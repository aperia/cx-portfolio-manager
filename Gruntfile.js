module.exports = function (grunt) {
  grunt.initConfig({
    maven_deploy: {
      options: {
        groupId: 'com.firstdata.fs',
        injectDestFolder: ''
      },
      snapshot: {
        options: {
          repositoryId: 'snapshots',
          url:
            'http://s1qvap952.1dc.com:8081/nexus/content/repositories/snapshots/',
          snapshot: 'true',
          goal: 'deploy'
        },
        files: [
          {
            src: ['**'],
            dest: '',
            cwd: 'build/',
            expand: true
          }
        ]
      },
      release: {
        options: {
          repositoryId: 'releases',
          url:
            'http://s1qvap952.1dc.com:8081/nexus/content/repositories/releases/',
          goal: 'deploy'
        },
        files: [
          {
            src: ['**'],
            dest: '',
            cwd: 'build/',
            expand: true
          }
        ]
      }
    },
    run: {
      lint: {
        cmd: 'eslint',
        args: ['*/**/*.{js,ts,tsx}', '--quiet', '--fix']
      },
      test: {
        cmd: 'react-scripts',
        args: [
          'test',
          '--coverage',
          '--silent',
          '--maxWorkers=50%',
          '--watchAll=false'
        ]
      },
      build_localization: {
        cmd: 'node',
        args: ['combineJson.ts']
      },
      build_react: {
        cmd: 'react-scripts',
        args: ['build']
      },
      prod_build: {
        cmd: 'react-scripts',
        args: ['build']
      }
    }
  });

  grunt.loadNpmTasks('grunt-maven-deploy');
  grunt.loadNpmTasks('grunt-run');
  grunt.registerTask('default', []);

  grunt.registerTask('release', [
    'run:build_localization',
    'run:prod_build',
    'maven_deploy:release'
  ]);
  grunt.registerTask('build', [
    'run:lint',
    'run:test',
    'run:build_localization',
    'run:build_react'
  ]);
};
