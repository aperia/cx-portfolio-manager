REM @echo off
cd build\static\js

set branchName=CXPM.Develop

set reactPath=..\..\..\..\%branchName%\CCT.CXPM.WebApp\wwwroot\resource
if not exist %reactPath% (
	mkdir %reactPath%
)

@RD /s /q %reactPath%

echo f | xcopy *.js %reactPath%\*.js /y /r
echo f | xcopy *.js.map %reactPath%\*.js.map /y /r

cd ..\..\css

set cssPath=..\..\..\%branchName%\CCT.CXPM.WebApp\wwwroot\css
if not exist %cssPath% (
	mkdir %cssPath%
)

echo f | xcopy styles.css %cssPath%\styles.css /y /r

pause
