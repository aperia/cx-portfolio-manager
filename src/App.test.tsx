import App from 'App';
import * as ApiServices from 'app/services';
import { renderWithMockStore } from 'app/utils';
import React from 'react';

const testId = 'app-container';

jest.mock('app/_libraries/_dof/core', () => ({
  App: ({ children }: any) => {
    return (
      <>
        <div>DOF App</div>
        <div>{children}</div>
      </>
    );
  },
  View: ({ formKey, descriptor, onClick }: any) => {
    return (
      <div id={formKey}>
        {formKey}
        <span>{descriptor}</span>
        <button onClick={onClick}>Click on {formKey}</button>
      </div>
    );
  }
}));
jest.mock('app/_libraries/_dof/layouts', () => {
  return {
    __esModule: true,
    default: jest.fn()
  };
});
jest.mock('registerControls', () => {
  return {
    __esModule: true,
    default: jest.fn()
  };
});
jest.mock('pages/_commons/ErrorBoundary', () => {
  return {
    __esModule: true,
    default: ({ children }: any) => {
      return (
        <>
          <div>Error Boundary</div>
          <div>{children}</div>
        </>
      );
    }
  };
});
jest.mock('pages/_commons/MappingProvider', () => {
  return {
    __esModule: true,
    default: ({ children }: any) => {
      return (
        <>
          <div>Mapping Provider</div>
          <div>{children}</div>
        </>
      );
    }
  };
});
jest.mock('pages/_commons/I18nextProvider', () => {
  return {
    __esModule: true,
    default: ({ children }: any) => {
      return (
        <>
          <div>I18next Provider</div>
          <div>{children}</div>
        </>
      );
    }
  };
});
jest.mock('AppContainer', () => {
  return {
    __esModule: true,
    default: () => {
      return (
        <>
          <div>App Container</div>
        </>
      );
    }
  };
});
jest.mock('app/services', () => {
  return {
    __esModule: true,
    default: () => {
      return {
        systemService: {
          getConfig: null,
          getReactSettingConfig: null
        }
      };
    }
  };
});

const mockService: any = ApiServices;

describe('App Container', () => {
  it('should render UI', async () => {
    const element = <App />;
    mockService.systemService = {
      getConfig: jest.fn().mockResolvedValue({
        data: { i18n: {}, api: {}, mappingUrl: 'mock url' }
      }),
      getReactSettingConfig: jest
        .fn()
        .mockResolvedValue({ data: { dofConfigs: [], dofDataSources: [] } })
    };

    const storeCofig = {
      initialState: {
        change: {
          changeDataExport: {
            loading: true
          }
        }
      }
    };

    const { findByText } = await renderWithMockStore(
      element,
      storeCofig,
      {},
      testId
    );

    expect(await findByText('App Container')).toBeInTheDocument();
  });
});
