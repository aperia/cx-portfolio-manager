import { renderComponent } from 'app/utils';
import 'app/_libraries/_dls/test-utils/mocks/mockChartjs';
import AppContainer from 'AppContainer';
import React from 'react';

const testId = 'app-container';

jest.mock('pages/_commons/NotiticationProvider', () => {
  return {
    __esModule: true,
    default: () => {
      return <div>Notification provider</div>;
    }
  };
});
jest.mock('pages/_commons/redux/WorkflowSetup', () => {
  return {
    useWorkflowSetup: () => {
      return { workflow: 'ok' };
    }
  };
});

describe('App Container', () => {
  it('should render UI', async () => {
    const element = <AppContainer />;

    const { getByText } = await renderComponent(testId, element);

    expect(getByText('Notification provider')).toBeInTheDocument();
  });
});
