// components
// dof
// api service
import { classnames } from 'app/helpers';
import { apiMapping, systemService } from 'app/services';
import { App as DOFApp, View } from 'app/_libraries/_dof/core';
import 'app/_libraries/_dof/layouts';
import AppContainer from 'AppContainer';
import ErrorBoundary from 'pages/_commons/ErrorBoundary';
import FallbackErrorComponent from 'pages/_commons/ErrorBoundary/FallbackErrorComponent';
import I18next from 'pages/_commons/I18nextProvider';
import MappingProvider from 'pages/_commons/MappingProvider';
// reducers
import { reducerMappingList } from 'pages/_commons/redux';
import { useSelectDataForExportLoading } from 'pages/_commons/redux/Change';
import { commonActions } from 'pages/_commons/redux/Common';
import 'pages/_commons/WorkflowSetup/CommonSteps/registerWorkflowSetupSteps';
import React, {
  useCallback,
  useEffect,
  useLayoutEffect,
  useState
} from 'react';
import { useDispatch, useSelector } from 'react-redux';
// middlewares
import thunk from 'redux-thunk';
import 'registerControls';

export const middlewares = [thunk.withExtraArgument({ ...apiMapping })];

window.debug = false;

interface AppProps {}

const App: React.FC<AppProps> = () => {
  const [isLoadingConfig, setLoadingConfig] = useState<boolean>(true);

  const handleGetConfig = async () => {
    try {
      setLoadingConfig(true);

      const result = await Promise.all([
        systemService.getConfig(),
        systemService.getReactSettingConfig()
      ]);
      const appConfig = result[0];
      window.appConfig = {
        i18n: appConfig?.data?.i18n,
        api: appConfig?.data?.api,
        mappingUrl: appConfig.data?.mappingUrl,
        workflowMappingUrl: appConfig.data?.workflowMappingUrl
      };

      const reactSetting: any = result.length > 1 && result[1];
      window.account = {
        ...reactSetting?.data
      };
    } finally {
      setLoadingConfig(false);
    }
  };

  useEffect(() => {
    handleGetConfig();
  }, []);

  return isLoadingConfig ? (
    <div className="loading vh-100"></div>
  ) : (
    <DOFApp
      customReducers={reducerMappingList}
      urlConfigs={window?.account?.dofConfigs as any}
      middleware={middlewares}
    >
      <ErrorBoundary fallbackComponent={FallbackErrorComponent}>
        <MappingProvider>
          <I18next>
            <Wrapper>
              <AppContainer />
            </Wrapper>
          </I18next>
        </MappingProvider>
      </ErrorBoundary>
    </DOFApp>
  );
};

const Wrapper: React.FC = ({ children }) => {
  const dispatch = useDispatch();
  const isDOFReady = useSelector<any, boolean>(
    state => !!state?.caches?.views['empty-view'],
    (pre, next) => pre === next
  );

  const exportLoading = useSelectDataForExportLoading();

  const updateWindowDemension = useCallback(() => {
    dispatch(
      commonActions.updateWindowDimension({
        width: window.innerWidth,
        height: window.innerHeight
      })
    );
  }, [dispatch]);

  useLayoutEffect(() => {
    window.addEventListener('resize', updateWindowDemension);
    updateWindowDemension();
    return () => window.removeEventListener('resize', updateWindowDemension);
  }, [updateWindowDemension]);

  return (
    <div
      className={classnames('h-100 d-flex flex-column', {
        'd-none': !isDOFReady,
        loading: exportLoading
      })}
    >
      <View id="empty-view" formKey="empty-view" descriptor="empty-view" />
      {children}
    </div>
  );
};

export default App;
