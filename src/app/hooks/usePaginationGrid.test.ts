import { renderHook } from '@testing-library/react-hooks';
import { usePaginationGrid } from 'app/hooks/usePaginationGrid';

describe('usePaginationGrid ', () => {
  const mockData = [
    { id: '1', name: 'testName', description: 'description' },
    { id: '2', name: 'testName2', description: 'description2' }
  ];
  it('should have data', () => {
    const { result } = renderHook(() => usePaginationGrid(mockData));

    expect(result.current.pageData).toEqual(mockData);
  });

  it('should have data with currentPage > 1', () => {
    const { result } = renderHook(() => usePaginationGrid(mockData));

    result.current.onPageChange(3);

    result.current.onPageSizeChange(3);

    expect(result.current.pageData).toEqual(mockData);
  });

  it('should have data with reset page to default', () => {
    const { result } = renderHook(() => usePaginationGrid(mockData));

    result.current.onPageChange(3);

    result.current.onResetToDefaultFilter();

    expect(result.current.currentPage).toEqual(1);
  });
});
