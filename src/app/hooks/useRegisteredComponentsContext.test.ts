import { renderHook } from '@testing-library/react-hooks';
import { useRegisteredComponentsContext } from 'app/hooks';

describe('useRegisteredComponentsContext ', () => {
  it('should return registered components in context', () => {
    const { result } = renderHook(() => useRegisteredComponentsContext());
    expect(result.current.registeredFields).toEqual({});
  });
});
