import { renderHook } from '@testing-library/react-hooks';
import { useUnsavedChangesWatchingChange } from 'app/hooks/useUnsavedChangesWatchingChange';
import * as unSaveHook from 'pages/_commons/redux/UnsavedChanges';

const mockAllFormChanges = jest.spyOn(unSaveHook, 'useAllFormChanges');

describe('Test Hooks > useUnsavedChangesWatchingChange ', () => {
  beforeEach(() => {
    mockAllFormChanges.mockReturnValue(['mockForm1', 'mockForm3']);
  });
  afterEach(() => {
    mockAllFormChanges.mockClear();
  });

  it('should render useUnsavedChangesWatchingChange hook > empty form watcher', () => {
    const { result } = renderHook(() => useUnsavedChangesWatchingChange());

    expect(result.current).toEqual([]);
  });

  it('should render useUnsavedChangesWatchingChange hook > empty form watcher', () => {
    const { result } = renderHook(() =>
      useUnsavedChangesWatchingChange(['mockForm1', 'mockForm2'])
    );

    expect(result.current).toEqual(['mockForm1']);
  });
});
