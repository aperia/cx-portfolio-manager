import {
  DEFAULT_PAGE_NUMBER,
  DEFAULT_PAGE_SIZE
} from 'app/_libraries/_dls/components/Pagination/constants';
import { useCallback, useEffect, useState } from 'react';

export const usePaginationGrid = <T>(list: Array<T>, curPage?: number) => {
  const [total, setTotal] = useState(list.length);
  const [currentPage, setCurrentPage] = useState(curPage ?? 1);
  const [currentPageSize, setCurrentPageSize] = useState(10);
  const [pageData, setPageData] = useState<T[]>([]);

  const onPageChange = (pageSelected: number) => {
    setCurrentPage(pageSelected);
  };

  const onPageSizeChange = (pageSizeNumber: any) => {
    setCurrentPage(1);
    setCurrentPageSize(pageSizeNumber);
  };

  const onResetToDefaultFilter = useCallback(() => {
    setCurrentPage(DEFAULT_PAGE_NUMBER);
    setCurrentPageSize(DEFAULT_PAGE_SIZE);
  }, []);

  useEffect(() => {
    const start = currentPageSize * (currentPage - 1);
    const end = currentPageSize * currentPage;
    const data = list.slice(start, end);
    if (list.length > 0 && data.length === 0 && currentPage > 1) {
      setCurrentPage(currentPage - 1);
      return;
    }

    setTotal(list.length);
    setPageData(data);
  }, [currentPage, currentPageSize, list]);

  return {
    total,
    currentPage,
    currentPageSize,
    pageData,
    onPageChange,
    onPageSizeChange,
    setCurrentPage,
    setCurrentPageSize,
    onResetToDefaultFilter
  };
};
