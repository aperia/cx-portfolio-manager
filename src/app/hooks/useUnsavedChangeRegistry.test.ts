import { renderHook } from '@testing-library/react-hooks';
import { useUnsavedChangeRegistry } from 'app/hooks/useUnsavedChangeRegistry';
import * as reactRedux from 'react-redux';

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');

describe('Test Hooks > useUnsavedChangeRegistry ', () => {
  beforeEach(() => {
    //mockUseDispatch.mockImplementation(() => (() => {}) as any);
  });
  afterEach(() => {});

  it('should render useUnsavedChangeRegistry hook > no change', () => {
    const dispatchFn = jest.fn();
    mockUseDispatch.mockReturnValue(dispatchFn);

    const { result } = renderHook(() =>
      useUnsavedChangeRegistry(
        {
          formName: 'mockForm',
          type: 'unsaved',
          confirmTitle: 'Mock title',
          confirmMessage: null,
          priority: 0
        },
        []
      )
    );
    expect(dispatchFn).toHaveBeenCalled();
    expect(result.current).toEqual(undefined);
  });

  it('should render useUnsavedChangeRegistry hook > with changes', () => {
    const dispatchFn = jest.fn();
    mockUseDispatch.mockReturnValue(dispatchFn);

    const { result } = renderHook(() =>
      useUnsavedChangeRegistry(
        {
          formName: 'mockForm',
          type: 'unsaved',
          confirmTitle: 'Mock title',
          confirmMessage: null,
          priority: 0
        },
        [true]
      )
    );
    expect(dispatchFn).toHaveBeenCalled();
    expect(result.current).toEqual(undefined);
  });
});
