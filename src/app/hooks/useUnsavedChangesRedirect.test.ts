import { renderHook } from '@testing-library/react-hooks';
import { useUnsavedChangesRedirect } from 'app/hooks/useUnsavedChangesRedirect';
import * as unSaveHook from 'pages/_commons/redux/UnsavedChanges';
import * as reactRedux from 'react-redux';

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');
const mockAllFormChanges = jest.spyOn(unSaveHook, 'useAllFormChanges');
const mockShowUnsavedConfirmation = jest.spyOn(
  unSaveHook,
  'useShowUnsavedConfirmation'
);

describe('Test Hooks > useUnsavedChangesRedirect ', () => {
  const { location } = window;

  beforeAll(() => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    delete window.location;
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    window.location = {
      href: ''
    };
  });
  afterAll(() => {
    window.location = location;
  });
  beforeEach(() => {
    mockAllFormChanges.mockReturnValue([]);
    mockShowUnsavedConfirmation.mockReturnValue(true);
  });
  afterEach(() => {
    mockAllFormChanges.mockClear();
    mockShowUnsavedConfirmation.mockClear();
  });

  it('should render useUnsavedChangesRedirect hook > empty form changes', () => {
    const onConfirm = jest.fn();
    const { result } = renderHook(() => useUnsavedChangesRedirect());
    result.current({ onConfirm });

    expect(onConfirm).toHaveBeenCalled();
  });

  it('should render useUnsavedChangesRedirect hook > with href to', () => {
    const onConfirm = jest.fn();
    const { result } = renderHook(() => useUnsavedChangesRedirect());
    result.current({ to: '/mock-url', onConfirm });

    expect(window.location.href).toEqual('/mock-url');
  });

  it('should render useUnsavedChangesRedirect hook > with form changes', () => {
    const dispatchFn = jest.fn();
    const onConfirm = jest.fn();
    mockUseDispatch.mockReturnValue(dispatchFn);
    mockAllFormChanges.mockReturnValue(['mockForm1']);

    const { result } = renderHook(() => useUnsavedChangesRedirect());

    result.current({
      to: 'mock-url',
      onConfirm,
      formsWatcher: ['mockForm1', 'mockForm2']
    });

    expect(dispatchFn).toHaveBeenCalled();
    expect(onConfirm).not.toHaveBeenCalled();
  });
});
