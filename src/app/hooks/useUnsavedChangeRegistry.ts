import { UnsavedChangesOptions } from 'app/types';
import { actionsUnsavedChanges } from 'pages/_commons/redux/UnsavedChanges';
import { useEffect, useMemo, useRef } from 'react';
import { useDispatch } from 'react-redux';

export const useUnsavedChangeRegistry = (
  options: Omit<
    UnsavedChangesOptions,
    'change' | 'onDiscard' | 'onClose' | 'onConfirm'
  >,
  changes: (string | boolean)[],
  onConfirm?: (apiCallSuccess?: boolean | void) => void,
  onClose?: () => void,
  onDiscard?: () => void
) => {
  const dispatch = useDispatch();

  const unsavedOptions = useRef({
    ...options,
    priority: options.priority || 0
  });
  unsavedOptions.current = useMemo(
    () => ({
      ...options,
      priority: options.priority || 0
    }),
    [options]
  );

  const changesMemo = useMemo(() => changes.some(v => !!v), [changes]);
  useEffect(() => {
    if (changesMemo) {
      dispatch(actionsUnsavedChanges.onFormChange(unsavedOptions.current));
    } else {
      dispatch(
        actionsUnsavedChanges.resetFormChange(unsavedOptions.current.formName)
      );
    }
  }, [dispatch, changesMemo]);

  useEffect(() => {
    dispatch(
      actionsUnsavedChanges.updateFormChangeActions({
        formName: unsavedOptions.current.formName,
        onClose,
        onConfirm,
        onDiscard
      })
    );
  }, [dispatch, onClose, onConfirm, onDiscard]);

  useEffect(() => {
    const formName = unsavedOptions.current.formName;
    return () => {
      dispatch(actionsUnsavedChanges.resetFormChange(formName));
    };
  }, [dispatch]);
};
