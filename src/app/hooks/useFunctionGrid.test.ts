import { renderHook } from '@testing-library/react-hooks';
import { useFunctionGrid } from 'app/hooks/useFunctionGrid';

describe('useFunctionGrid ', () => {
  it('should have data', () => {
    const { result } = renderHook(() =>
      useFunctionGrid({ defaultOrder: 'asc', defaultSortKey: 'name' })
    );
    result.current.onSetGridData([{ id: 'name', description: 'test' }]);
    expect(result.current.gridData).toEqual([
      { id: 'name', description: 'test' }
    ]);
  });

  it('should onClearData', () => {
    const { result } = renderHook(() =>
      useFunctionGrid({ defaultOrder: 'asc', defaultSortKey: 'name' })
    );
    result.current.onClearData();
    expect(result.current.onClearData()).toEqual(undefined);
  });

  it('should onSort', () => {
    const { result } = renderHook(() =>
      useFunctionGrid({ defaultOrder: 'asc', defaultSortKey: 'name' })
    );
    result.current.onSort({ id: 'example', order: 'desc' });
    expect(result.current.onSort).toBeTruthy();
  });

  it('should onSort without order', () => {
    const { result } = renderHook(() =>
      useFunctionGrid({ defaultOrder: 'asc', defaultSortKey: 'name' })
    );
    result.current.onSort({ id: 'example' });
    expect(result.current.onSort).toBeTruthy();
  });

  it('should onClearFilter', () => {
    const { result } = renderHook(() =>
      useFunctionGrid({ defaultOrder: 'asc', defaultSortKey: 'name' })
    );
    result.current.onClearFilter();
    expect(result.current.onClearFilter).toBeTruthy();
  });

  it('should onResetFilter', () => {
    const { result } = renderHook(() =>
      useFunctionGrid({ defaultOrder: 'asc', defaultSortKey: 'name' })
    );
    result.current.onResetFilter();
    expect(result.current.onResetFilter).toBeTruthy();
  });

  it('should onSetFilterData', () => {
    const { result } = renderHook(() =>
      useFunctionGrid({ defaultOrder: 'asc', defaultSortKey: 'name' })
    );
    result.current.onSetFilterData();
    expect(result.current.onSetFilterData).toBeTruthy();
  });

  it('should onExpand', () => {
    const { result } = renderHook(() =>
      useFunctionGrid({ defaultOrder: 'asc', defaultSortKey: 'name' })
    );
    result.current.onExpand({ id: 'key' });
    expect(result.current.onExpand).toBeTruthy();
  });

  it('should onCheck', () => {
    const { result } = renderHook(() =>
      useFunctionGrid({ defaultOrder: 'asc', defaultSortKey: 'name' })
    );
    result.current.onCheck(['test'], ['test', 'test2', 'test3']);
    expect(result.current.checkedList).toEqual(['test']);
  });

  it('should onSearch', () => {
    const { result } = renderHook(() =>
      useFunctionGrid({ defaultOrder: 'asc', defaultSortKey: 'name' })
    );
    result.current.onSearch('testSearchField');
    expect(result.current.searchValue).toEqual('testSearchField');
  });

  it('should onSearch with emptyValue', () => {
    const { result } = renderHook(() =>
      useFunctionGrid({ defaultOrder: 'asc', defaultSortKey: 'name' })
    );
    result.current.onSearch('');
    expect(result.current.searchValue).toEqual('');
  });

  it('should onSearch with searchValue with name filter', () => {
    const { result } = renderHook(() =>
      useFunctionGrid({ defaultOrder: 'asc', defaultSortKey: 'name' })
    );
    result.current.onSetGridData([
      { id: 'name', description: 'description', name: 'testName' }
    ]);

    result.current.onSearch('description');

    result.current.onSetFilterData();

    expect(result.current.searchValue).toEqual('description');
  });

  it('should onSearch with searchValue with description filter', () => {
    const { result } = renderHook(() =>
      useFunctionGrid({ defaultOrder: 'asc', defaultSortKey: 'name' })
    );
    result.current.onSetGridData([
      { id: 'name', description: 'test', name: 'hahaha' }
    ]);

    result.current.onSearch('hahaha');

    result.current.onSetFilterData();

    expect(result.current.searchValue).toEqual('hahaha');
  });

  it('should onExpand with empty data', () => {
    const { result } = renderHook(() =>
      useFunctionGrid({ defaultOrder: 'asc', defaultSortKey: 'name' })
    );
    result.current.onExpand([]);
    expect(result.current.onExpand).toBeTruthy();
  });

  it('should onSetIsCheckAll with true value', () => {
    const { result } = renderHook(() =>
      useFunctionGrid({ defaultOrder: 'asc', defaultSortKey: 'name' })
    );
    result.current.onSetIsCheckAll(true);
    expect(result.current.isCheckAll).toEqual(true);
  });

  it('should onSetIsCheckAll with false value', () => {
    const { result } = renderHook(() =>
      useFunctionGrid({ defaultOrder: 'asc', defaultSortKey: 'name' })
    );
    result.current.onSetIsCheckAll(false);
    expect(result.current.isCheckAll).toEqual(false);
  });

  it('should onSetPageCheckedList with empty data', () => {
    const { result } = renderHook(() =>
      useFunctionGrid({ defaultOrder: 'asc', defaultSortKey: 'name' })
    );
    result.current.onSetPageCheckedList(['testing']);
    expect(result.current.pageCheckedList).toEqual(['testing']);
  });

  it('should onSetCheckedList with data', () => {
    const { result } = renderHook(() =>
      useFunctionGrid({ defaultOrder: 'asc', defaultSortKey: 'name' })
    );
    result.current.onSetCheckedList(['testing']);
    expect(result.current.checkedList).toEqual(['testing']);
  });

  it('should onSetHasChange with empty data', () => {
    const { result } = renderHook(() =>
      useFunctionGrid({ defaultOrder: 'asc', defaultSortKey: 'name' })
    );
    result.current.onSetHasChange(true);
    expect(result.current.hasChange).toEqual(true);
  });

  it('should onClearExpand with empty data', () => {
    const { result } = renderHook(() =>
      useFunctionGrid({ defaultOrder: 'asc', defaultSortKey: 'name' })
    );
    result.current.onClearExpand();
    expect(result.current.expandedList).toEqual([]);
  });

  it('should onResetSort with empty data', () => {
    const { result } = renderHook(() =>
      useFunctionGrid({ defaultOrder: 'asc', defaultSortKey: 'name' })
    );
    result.current.onResetSort({ id: 'name', order: undefined });
    expect(result.current.expandedList).toEqual([]);
  });
});
