import { renderHook } from '@testing-library/react-hooks';
import { useElementSize } from 'app/hooks/useElementSize';
import { useRef } from 'react';

describe('useElementSize ', () => {
  it('should return size as 0', () => {
    const ref = renderHook<any, React.RefObject<HTMLElement>>(() =>
      useRef<HTMLElement | null>(null)
    );
    const { result } = renderHook(() => useElementSize(ref.result.current));

    expect(result.current.width).toEqual(0);
    expect(result.current.height).toEqual(0);
  });

  it('should return element size', () => {
    const element = document.createElement('div');
    element.getBoundingClientRect = jest
      .fn()
      .mockImplementation(() => ({ width: 400, height: 300 }));
    document.body.appendChild(element);

    const ref = renderHook(() => useRef(element));
    const { result } = renderHook(() => useElementSize(ref.result.current));

    expect(result.current.width).toEqual(400);
    expect(result.current.height).toEqual(300);
  });
});
