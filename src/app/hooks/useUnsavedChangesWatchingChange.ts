import { useAllFormChanges } from 'pages/_commons/redux/UnsavedChanges';
import { useMemo, useRef } from 'react';

export const useUnsavedChangesWatchingChange = (
  formsWatcher: string[] = []
) => {
  const allFormChanges = useAllFormChanges();
  const watcher = useRef(formsWatcher);

  const formsChange = useMemo(
    () => allFormChanges.filter(p => watcher.current?.includes(p)),
    [allFormChanges]
  );

  return formsChange;
};
