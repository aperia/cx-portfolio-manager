import userEvent from '@testing-library/user-event';
import { useCheckAllPagination } from 'app/hooks/useCheckAllPagination';
import { renderComponent } from 'app/utils';
import { Grid } from 'app/_libraries/_dls';
import React from 'react';

const get1MockData = (i: number) => `${i + 1}`;

const generateMockData = (length: number) => {
  const result = [];
  for (let i = 0; i < length; i++) {
    result.push(get1MockData(i));
  }

  return result;
};

describe('useCheckAllPagination ', () => {
  const Wrapper: React.FC<any> = props => {
    const { gridClassName, handleClickCheckAll } = useCheckAllPagination(
      props as any
    );

    return (
      <>
        <Grid
          className={gridClassName}
          columns={[
            {
              id: 'name',
              Header: 'name',
              accessor: 'name'
            }
          ]}
          data={[{ name: 'name 1' }, { name: 'name2' }]}
          dataItemKey="name"
          checkedList={[]}
          variant={{ id: 'id', type: 'checkbox' }}
        />
        <div onClick={handleClickCheckAll}>handleClickCheckAll</div>
      </>
    );
  };

  it('should check all with filter data', async () => {
    jest.useFakeTimers();

    const gridClassName = '';
    const allIds = generateMockData(4);
    const hasFilter = true;
    const filterIds = generateMockData(2);
    const checkedList = generateMockData(1);
    const setCheckAll = jest.fn();
    const setCheckList = jest.fn();

    const { getByText, rerender } = await renderComponent(
      '',
      <Wrapper
        gridClassName={gridClassName}
        allIds={allIds}
        hasFilter={hasFilter}
        filterIds={filterIds}
        checkedList={checkedList}
        setCheckAll={setCheckAll}
        setCheckList={setCheckList}
        checkableIds={['1']}
      />
    );

    userEvent.click(getByText('handleClickCheckAll'));
    expect(setCheckList).toBeCalledWith([]);

    rerender(
      <Wrapper
        gridClassName={gridClassName}
        allIds={allIds}
        hasFilter={hasFilter}
        filterIds={filterIds}
        checkedList={['1', '2']}
        setCheckAll={setCheckAll}
        setCheckList={setCheckList}
      />
    );
    userEvent.click(getByText('handleClickCheckAll'));
    jest.runAllTicks();
    expect(setCheckList).toBeCalledWith([]);

    jest.useRealTimers();
  });

  it('should check all without filter data', async () => {
    jest.useFakeTimers();

    const gridClassName = 'test';
    const allIds = generateMockData(4);
    const hasFilter = false;
    const filterIds = generateMockData(4);
    const checkedList = generateMockData(1);
    const setCheckAll = jest.fn();
    const setCheckList = jest.fn();

    const { getByText, rerender, container } = await renderComponent(
      '',
      <Wrapper
        gridClassName={gridClassName}
        allIds={allIds}
        hasFilter={hasFilter}
        filterIds={filterIds}
        checkedList={checkedList}
        setCheckAll={setCheckAll}
        setCheckList={setCheckList}
      />
    );

    userEvent.click(getByText('handleClickCheckAll'));
    expect(setCheckAll).toBeCalledWith(true);

    rerender(
      <Wrapper
        gridClassName={gridClassName}
        allIds={allIds}
        hasFilter={hasFilter}
        filterIds={filterIds}
        checkedList={allIds}
        setCheckAll={setCheckAll}
        setCheckList={setCheckList}
      />
    );
    userEvent.click(getByText('handleClickCheckAll'));
    jest.runAllTicks();
    expect(setCheckAll).toBeCalledWith(false);

    rerender(
      <Wrapper
        gridClassName={gridClassName}
        allIds={allIds}
        hasFilter={hasFilter}
        filterIds={filterIds}
        checkedList={generateMockData(3)}
        setCheckAll={setCheckAll}
        setCheckList={setCheckList}
      />
    );
    jest.runAllTicks();
    expect(container.querySelector('.indeterminate')).toBeInTheDocument();

    jest.useRealTimers();
  });
});
