import { renderHook } from '@testing-library/react-hooks';
import { useModalRegistry } from 'app/hooks/useModals';
import * as modalsStore from 'pages/_commons/redux/modal';
import * as reactRedux from 'react-redux';

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');

const mockActionsModalsRegister = jest.spyOn(
  modalsStore.actionsModals,
  'register'
);
const mockActionsModalsClose = jest.spyOn(modalsStore.actionsModals, 'close');
const mockUseSelectCurrentOpenedModal = jest.spyOn(
  modalsStore,
  'useSelectCurrentOpenedModal'
);

describe('useModals ', () => {
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => (() => {}) as any);

    mockActionsModalsRegister.mockImplementation();
    mockActionsModalsClose.mockImplementation();
  });
  afterEach(() => {
    mockUseSelectCurrentOpenedModal.mockClear();
  });

  it('should opene', () => {
    mockUseSelectCurrentOpenedModal.mockImplementation(() => [true, true]);

    const { result } = renderHook(() => useModalRegistry('id', true));
    expect(result.current).toEqual([true, true]);
  });

  it('should close', () => {
    mockUseSelectCurrentOpenedModal.mockImplementation(() => [false, false]);
    const { result } = renderHook(() => useModalRegistry('id', true));

    expect(result.current).toEqual([false, false]);
  });

  it('should close on force close', () => {
    mockUseSelectCurrentOpenedModal.mockImplementation(() => [false, false]);
    const { result } = renderHook(() => useModalRegistry('id', false));

    expect(result.current).toEqual([false, false]);
  });
});
