import { matchSearchValue } from 'app/helpers';
import { SortType } from 'app/_libraries/_dls';
import {
  DEFAULT_PAGE_NUMBER,
  DEFAULT_PAGE_SIZE
} from 'app/_libraries/_dls/components/Pagination/constants';
import { differenceWith, get, orderBy } from 'app/_libraries/_dls/lodash';
import _, { isEmpty, isUndefined } from 'lodash';
import { useCallback, useEffect, useMemo, useRef, useState } from 'react';

export const usePagination = <T>(
  list: Array<T>,
  searchField: string[] = [],
  sortIdDefault?: string,
  pageSize = DEFAULT_PAGE_SIZE,
  curPage = DEFAULT_PAGE_NUMBER
) => {
  const [total, setTotal] = useState(list.length);
  const [currentPage, setCurrentPage] = useState(curPage);
  const [currentPageSize, setCurrentPageSize] = useState(pageSize);
  const [searchValue, setSearchValue] = useState('');
  const [gridData, setGridData] = useState<T[]>([]);
  const [customData, setCustomData] = useState<T[]>([]);
  const [isSearchByCase, setIsSearchByCase] = useState<boolean>(false);
  const [allDataFilter, setAllDataFilter] = useState<T[]>([]);
  const [sort, setSort] = useState<SortType>({ id: sortIdDefault || '' });
  const [dataChecked, setDataChecked] = useState<any[]>([]);
  const [isCheckAll, setIsCheckAll] = useState<boolean>();
  const [dataCheckedView, setDataCheckedView] = useState<any[]>([]);

  const searchFieldRef = useRef(searchField);
  searchFieldRef.current = searchField;
  const pageSizeRef = useRef(pageSize);
  const sortRef = useRef({ id: sortIdDefault || '' });

  const allIds = useMemo(() => list.map(item => (item as any).id), [list]);

  const getDataFiltered = useCallback(() => {
    let dataFiltered = list;
    if (searchValue && !isEmpty(searchFieldRef.current)) {
      dataFiltered = list.filter((item: any) =>
        matchSearchValue(
          searchFieldRef.current.map((field: string) =>
            get(item, field.split('.'))
          ),
          searchValue
        )
      );
    }

    if (sort?.id) {
      dataFiltered = orderBy(dataFiltered, [sort?.id], [sort?.order || 'asc']);
    }

    const start = currentPageSize * (currentPage - 1);
    const end = currentPageSize * currentPage;
    return {
      data: dataFiltered.slice(start, end),
      dataFiltered
    };
  }, [currentPage, currentPageSize, list, searchValue, sort?.id, sort?.order]);

  const onSearchChange = useCallback((val: string) => {
    setCurrentPage(1);
    setSearchValue(val);
  }, []);

  const onPageChange = useCallback((pageSelected: number) => {
    setCurrentPage(pageSelected);
  }, []);

  const onPageSizeChange = useCallback((size: number) => {
    setCurrentPage(1);
    setCurrentPageSize(size);
  }, []);

  const onSortChange = useCallback((sort: SortType) => {
    if (!isUndefined(sort.order)) {
      setSort(sort);
    } else {
      setSort(sortRef.current);
    }
  }, []);

  const onCheckInPage = useCallback(
    dataCheckedIn1Page => {
      const { data } = getDataFiltered();
      const allIdInPage = data.map((item: any) => item.id);
      const dataOtherPage = differenceWith(dataChecked, allIdInPage);
      const newDataChecked = _.uniq(dataOtherPage.concat(dataCheckedIn1Page));
      setDataChecked(newDataChecked);
      setDataCheckedView(dataCheckedIn1Page);
      setIsCheckAll(newDataChecked.length === allIds.length);
    },
    [allIds.length, dataChecked, getDataFiltered]
  );

  const onSetDataChecked = useCallback(dataChecked => {
    setDataChecked(dataChecked);
  }, []);

  const onSetIsCheckAll = useCallback(
    isCheckAll => {
      setIsCheckAll(isCheckAll);
      setDataChecked(isCheckAll ? allIds : []);
    },
    [allIds]
  );

  const onResetToDefaultFilter = useCallback(
    ({ keepSort = false }: { keepSort?: boolean } = {}) => {
      !keepSort && setSort({ id: sortIdDefault || '' });
      setCurrentPage(DEFAULT_PAGE_NUMBER);
      setCurrentPageSize(pageSizeRef.current);
      setSearchValue('');
      setIsSearchByCase(false);
    },
    [sortIdDefault]
  );

  const onSearchByCase = useCallback(
    (val: string, searchCustomFields: string[]) => {
      if (!isEmpty(val)) {
        setCurrentPage(1);
        setIsSearchByCase(true);
        setSearchValue(val);
        const dataSearchField = list.filter((item: any) =>
          matchSearchValue(
            searchCustomFields.map((field: string) => get(item, field)),
            val
          )
        );

        setCustomData(dataSearchField);
      }
    },
    [setCustomData, list]
  );

  const navigateToItem = useCallback(
    (field: string, value: any) => {
      setSearchValue('');
      const { dataFiltered } = getDataFiltered();
      let idx = dataFiltered.findIndex((f: any) => f?.[`${field}`] === value);
      idx = idx !== -1 ? idx : 1;
      const currPage = Math.ceil((idx + 1) / currentPageSize);

      setCurrentPage(currPage);
    },
    [currentPageSize, getDataFiltered]
  );

  const navigateToItemAndResetDefault = useCallback(
    (field: string, value: any, keepSort?: boolean) => {
      setSearchValue('');
      const dataFiltered = orderBy(
        list,
        [keepSort ? sort.id : sortIdDefault ?? ''],
        keepSort ? sort.order : undefined
      );
      !keepSort && setSort({ id: sortIdDefault || '' });
      let idx = dataFiltered.findIndex((f: any) => f?.[`${field}`] === value);
      idx = idx !== -1 ? idx : 1;
      const currPage = Math.ceil((idx + 1) / currentPageSize);

      setCurrentPage(currPage);
    },
    [currentPageSize, list, sort, sortIdDefault]
  );

  useEffect(() => {
    const { data, dataFiltered } = getDataFiltered();
    if (data.length === 0 && currentPage > 1) {
      setCurrentPage(currentPage - 1);
      return;
    }

    const allIdInPage = data.map(item => (item as any).id);
    setDataCheckedView(isCheckAll ? allIdInPage : dataChecked);
    setTotal(dataFiltered.length);
    setGridData(data);
    setAllDataFilter(dataFiltered);
  }, [currentPage, dataChecked, getDataFiltered, isCheckAll, list.length]);

  useEffect(() => {
    if (!isSearchByCase) return;

    setTotal(customData.length);
    setGridData(orderBy(customData, [sort?.id], [sort?.order || 'asc']));
  }, [isSearchByCase, customData, sort?.id, sort?.order]);

  return {
    total,
    currentPage,
    pageSize,
    currentPageSize,
    gridData,
    allDataFilter,
    searchValue,
    sort,
    dataChecked,
    isCheckAll,
    dataCheckedView,
    onCheckInPage,
    onSetDataChecked,
    onSetIsCheckAll,
    onSortChange,
    onPageChange,
    onPageSizeChange,
    onSearchChange,
    onSearchByCase,
    setCurrentPage,
    setCurrentPageSize,
    onResetToDefaultFilter,
    navigateToItem,
    navigateToItemAndResetDefault
  };
};
