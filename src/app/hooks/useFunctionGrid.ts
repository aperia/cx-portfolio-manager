import { mappingArrayKey } from 'app/helpers';
import { SortType } from 'app/_libraries/_dls';
import { differenceWith, isEmpty } from 'app/_libraries/_dls/lodash';
import _, { orderBy } from 'lodash';
import { useCallback, useMemo, useState } from 'react';

interface OptionsType {
  defaultSortKey: string;
  defaultOrder: 'asc' | 'desc' | undefined;
}

export const useFunctionGrid = (options: OptionsType) => {
  const DEFAULT_SORT = useMemo(
    () => ({
      id: options.defaultSortKey,
      order: options.defaultOrder
    }),
    [options]
  );

  const [gridData, setGridData] = useState<any[]>([]);
  const [isCheckAll, setIsCheckAll] = useState<boolean>(false);
  const [filterData, setFilterData] = useState<any[]>([]);
  const [searchValue, setSearchValue] = useState<string>('');
  const [checkedList, setCheckedList] = useState<string[]>([]);
  const [pageCheckedList, setPageCheckedList] = useState<string[]>([]);
  const [expandedList, setExpandedList] = useState<string[]>([]);
  const [hasChange, setHasChange] = useState(false);
  const [sortBy, setSortBy] = useState<SortType>(DEFAULT_SORT);
  const allIds = useMemo(() => mappingArrayKey(gridData, 'id'), [gridData]);
  const hasFilter = !isEmpty(filterData) && !isEmpty(searchValue);

  const onClearData = useCallback(() => {
    setExpandedList([]);
    setSearchValue('');
    setFilterData([]);
  }, [setExpandedList, setSearchValue, setFilterData]);

  const onSetHasChange = useCallback(
    (value: boolean) => {
      setHasChange(value);
    },
    [setHasChange]
  );

  const onSetGridData = useCallback(
    (data: any[]) => {
      setGridData(orderBy(data, sortBy.id, sortBy.order));
    },
    [sortBy]
  );

  const onSort = useCallback(
    (newSort: SortType) => {
      const sort = newSort.order ? newSort : DEFAULT_SORT;
      setSortBy(sort);
      setGridData(orderBy(gridData, sort.id, sort.order));
    },
    [gridData, DEFAULT_SORT]
  );

  const onClearFilter = useCallback(() => {
    setFilterData([]);
  }, [setFilterData]);

  const onResetFilter = useCallback(() => {
    setSortBy(DEFAULT_SORT);
    setSearchValue('');
    setFilterData([]);
    setExpandedList([]);
  }, [setSearchValue, setFilterData, setSortBy, DEFAULT_SORT]);

  const onSetFilterData = useCallback(() => {
    const search = searchValue?.trim()?.toLowerCase();
    const filteredData = gridData.filter(
      (item: MagicKeyValue) =>
        item.id?.toString().includes(search) ||
        item.name?.toLocaleLowerCase().includes(search)
    );

    setFilterData(filteredData);
  }, [setFilterData, gridData, searchValue]);

  const onExpand = useCallback(
    (expandData: any) => {
      setExpandedList(isEmpty(expandData) ? [] : expandData);
    },
    [setExpandedList]
  );

  const onClearExpand = useCallback(() => {
    setExpandedList([]);
  }, [setExpandedList]);

  const onCheck = useCallback(
    (checkedData: string[], pageData: any[]) => {
      const allIdInPage = mappingArrayKey(pageData, 'id');
      const dataOtherPage = differenceWith(checkedList, allIdInPage);
      const newDataChecked = _.uniq(dataOtherPage.concat(checkedData));
      setHasChange(true);
      setCheckedList(newDataChecked);
      setPageCheckedList(checkedData);
      setIsCheckAll(newDataChecked.length === allIds.length);
    },
    [setCheckedList, checkedList, allIds]
  );

  const onSearch = useCallback(
    (val: string) => {
      setSearchValue(val);
    },
    [setSearchValue]
  );

  const onSetIsCheckAll = useCallback(
    (checked: boolean) => {
      setHasChange(true);
      setIsCheckAll(checked);
      setCheckedList(checked ? allIds : []);
    },
    [allIds]
  );

  const onSetCheckedList = useCallback(
    (list: string[]) => {
      setCheckedList(list);
    },
    [setCheckedList]
  );

  const onSetPageCheckedList = useCallback(
    (list: string[]) => {
      setPageCheckedList(list);
    },
    [setPageCheckedList]
  );

  const onResetSort = useCallback(
    ({ id, order }: SortType) => {
      setSortBy({ id, order });
    },
    [setSortBy]
  );

  return {
    filterData,
    checkedList,
    pageCheckedList,
    expandedList,
    searchValue,
    gridData,
    isCheckAll,
    sortBy,
    hasFilter,
    hasChange,
    onSort,
    onResetSort,
    onClearData,
    onClearFilter,
    onResetFilter,
    onSetFilterData,
    onExpand,
    onCheck,
    onSearch,
    onSetGridData,
    onSetIsCheckAll,
    onSetCheckedList,
    onSetPageCheckedList,
    onSetHasChange,
    onClearExpand
  };
};
