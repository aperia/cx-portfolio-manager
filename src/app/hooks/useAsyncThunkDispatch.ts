import { Action, ThunkDispatch } from '@reduxjs/toolkit';
import { useDispatch } from 'react-redux';

export const useAsyncThunkDispatch = () =>
  useDispatch<ThunkDispatch<RootState, void, Action>>();
