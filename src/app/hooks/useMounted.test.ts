import { renderHook } from '@testing-library/react-hooks';
import { useMounted } from 'app/hooks/useMounted';

describe('useMounted ', () => {
  it('should mount', () => {
    const { result } = renderHook(() => useMounted());
    expect(result.current.current).toEqual(true);
  });
});
