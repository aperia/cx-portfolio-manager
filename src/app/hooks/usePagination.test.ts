import { renderHook } from '@testing-library/react-hooks';
import { PAGE_SIZE } from 'app/constants/constants';
import { usePagination } from 'app/hooks/usePagination';
import { SortType } from 'app/_libraries/_dls';

const get1MockData = (i: number | string) => ({
  id: `${i}`,
  name: `testName${i}`,
  description: `description${i}`
});

const generateMockData = (length: number) => {
  const result = [];
  for (let i = 0; i < length; i++) {
    result.push(get1MockData(i));
  }

  return result;
};

describe('usePagination ', () => {
  it('should have data', () => {
    const mockData = generateMockData(2);
    const { result } = renderHook(() => usePagination(mockData));

    expect(result.current.pageSize).toEqual(10);
    expect(result.current.currentPage).toEqual(1);
  });

  it('onSearchChange', async () => {
    const mockData = generateMockData(20);
    const { result } = renderHook(() =>
      usePagination(mockData, ['name'], 'name')
    );
    const { onSearchChange } = result.current;
    const input = '3';
    onSearchChange(input);

    expect(result.current.currentPage).toEqual(1);
    expect(result.current.searchValue).toEqual(input);
    expect(result.current.gridData.length).toEqual(10);
  });

  it('onPageChange', async () => {
    const mockData = generateMockData(15);
    const { result } = renderHook(() =>
      usePagination(mockData, ['name'], 'name')
    );
    const { onPageChange } = result.current;
    const input = 2;
    onPageChange(input);

    expect(result.current.currentPage).toEqual(2);
    expect(result.current.gridData.length).toEqual(10);
  });

  it('onPageSizeChange', async () => {
    const mockData = generateMockData(15);
    const { result } = renderHook(() =>
      usePagination(mockData, ['name'], 'name')
    );
    const { onPageSizeChange } = result.current;
    const input = PAGE_SIZE[3];
    onPageSizeChange(input);

    expect(result.current.currentPage).toEqual(1);
    expect(result.current.gridData.length).toEqual(10);
  });

  it('onSortChange', async () => {
    const mockData = generateMockData(15);
    const { result } = renderHook(() =>
      usePagination(mockData, ['name'], 'name')
    );
    const { onSortChange } = result.current;
    const input: SortType = { id: 'name', order: 'desc' };
    onSortChange(input);

    expect(result.current.sort).toEqual(input);
    expect(result.current.gridData[0]).toEqual(get1MockData(0));
  });

  it('onSortChange without order', async () => {
    const mockData = generateMockData(15);
    const { result } = renderHook(() =>
      usePagination(mockData, ['name'], 'name')
    );
    const { onSortChange } = result.current;
    const input: SortType = { id: 'name' };
    onSortChange(input);

    expect(result.current.sort).toEqual(input);
    expect(result.current.gridData[0]).toEqual(get1MockData(0));
  });

  it('onCheckInPage', async () => {
    const mockData = generateMockData(20);
    const input = ['1', '2', '3', '4'];

    const { result } = renderHook(() =>
      usePagination(mockData, ['name'], 'name')
    );
    const { onCheckInPage } = result.current;
    onCheckInPage(input);

    expect(result.current.dataCheckedView).toEqual(input);
    expect(result.current.dataChecked).toEqual(input);
  });

  it('onSetDataChecked', () => {
    const mockData = generateMockData(20);
    const input = ['1', '2', '3', '4'];

    const { result } = renderHook(() =>
      usePagination(mockData, ['name'], 'name')
    );
    const { onSetDataChecked } = result.current;
    onSetDataChecked(input);

    expect(result.current.dataChecked).toEqual(input);
  });

  it('onSetIsCheckAll', () => {
    const mockData = generateMockData(20);

    const { result } = renderHook(() =>
      usePagination(mockData, ['name'], 'name')
    );
    const { onSetIsCheckAll } = result.current;
    onSetIsCheckAll(true);
    expect(result.current.isCheckAll).toEqual(true);
    expect(result.current.dataChecked).toEqual(mockData.map(i => i.id));

    onSetIsCheckAll(false);
    expect(result.current.dataChecked).toEqual([]);
  });

  it('bypass', () => {
    const mockData: any[] = [];
    const { result } = renderHook(() =>
      usePagination(mockData, ['name'], 'name')
    );
    const { onPageChange } = result.current;
    onPageChange(2);
    expect(result.current.currentPage).toEqual(2);
  });

  it('onResetToDefaultFilter', async () => {
    const mockData = generateMockData(15);
    const { result } = renderHook(() =>
      usePagination(mockData, ['name'], undefined)
    );
    const { onResetToDefaultFilter } = result.current;
    onResetToDefaultFilter();

    expect(result.current.pageSize).toEqual(10);
  });

  it('onSearchByCase', async () => {
    const mockData = generateMockData(15);
    const { result } = renderHook(() =>
      usePagination(mockData, ['name'], undefined)
    );
    const { onSearchByCase } = result.current;
    onSearchByCase('e', ['name']);

    expect(result.current.pageSize).toEqual(10);
  });

  it('navigateToItem', async () => {
    const mockData = generateMockData(15);
    const { result } = renderHook(() =>
      usePagination(mockData, ['name'], undefined)
    );
    const { navigateToItem } = result.current;
    navigateToItem('id', '13');
    expect(result.current.currentPage).toEqual(2);

    navigateToItem('field', '2');
    expect(result.current.currentPage).toEqual(1);
  });

  it('navigateToItemAndResetDefault', async () => {
    const mockData = generateMockData(15);
    const { result } = renderHook(() =>
      usePagination(mockData, ['name'], undefined)
    );
    const { navigateToItemAndResetDefault } = result.current;
    navigateToItemAndResetDefault('id', '13');
    expect(result.current.currentPage).toEqual(2);

    navigateToItemAndResetDefault('field', '2');
    expect(result.current.currentPage).toEqual(1);
  });
});
