import { UnsavedChangesRedirect } from 'app/types';
import {
  actionsUnsavedChanges,
  useAllFormChanges,
  useShowUnsavedConfirmation
} from 'pages/_commons/redux/UnsavedChanges';
import { useCallback, useRef } from 'react';
import { useDispatch } from 'react-redux';

export const useUnsavedChangesRedirect = () => {
  const dispatch = useDispatch();
  const showUnsavedConfirmation = useShowUnsavedConfirmation();
  const allFormChanges = useAllFormChanges();

  const keepRef = useRef({ showUnsavedConfirmation, allFormChanges });
  keepRef.current.showUnsavedConfirmation = showUnsavedConfirmation;
  keepRef.current.allFormChanges = allFormChanges;

  return useCallback(
    ({
      to,
      onDiscard,
      onClose,
      onConfirm,
      formsWatcher
    }: Omit<UnsavedChangesRedirect, 'completed'>) => {
      const {
        showUnsavedConfirmation: _showUnsavedConfirmation,
        allFormChanges: _allFormChanges
      } = keepRef.current;

      const showWithForm = !!_allFormChanges.find(
        p => !formsWatcher || formsWatcher.includes(p)
      );
      if (_showUnsavedConfirmation && showWithForm) {
        dispatch(
          actionsUnsavedChanges.redirectTo({
            to,
            onDiscard,
            onClose,
            onConfirm,
            formsWatcher
          })
        );
        return;
      }

      typeof onConfirm === 'function' && onConfirm();
      if (!!to) window.location.href = to;
    },
    [dispatch]
  );
};
