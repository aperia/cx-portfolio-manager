import { useAsyncThunkDispatch } from './useAsyncThunkDispatch';

const mockDispatch = jest.fn();

jest.mock('react-redux', () => ({
  useDispatch: () => mockDispatch
}));

describe('test hooks > useAsyncThunkDispatch', () => {
  it('test useAsyncThunkDispatch function', () => {
    const result = useAsyncThunkDispatch();
    expect(typeof result).toEqual('function');
  });
});
