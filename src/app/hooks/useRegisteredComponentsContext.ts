import RegisteredComponentsContext from 'app/_libraries/_dof/core/contexts/RegisteredComponentsContext';
import { useContext } from 'react';

export const useRegisteredComponentsContext = () =>
  useContext(RegisteredComponentsContext);
