import { isEqual } from 'lodash';
import { useCallback, useEffect, useRef, useState } from 'react';

interface ITouch {
  touched: boolean;
  blur: boolean;
}
export const useFormValidations = <TFields extends string | number>(
  formErrors: Record<TFields, IFormError>,
  changes = 0,
  ...defaultTouchesParam: TFields[]
): [
  Record<TFields, ITouch>,
  Record<TFields, IFormError>,
  (field: TFields, blur?: boolean) => () => void
] => {
  const [memoFormErrors, setMemoFormErrors] =
    useState<Record<TFields, IFormError>>(formErrors);
  const defaultTouches = useRef<TFields[]>(defaultTouchesParam);
  defaultTouches.current = defaultTouchesParam;

  const [touches, setTouches] = useState<Record<TFields, ITouch>>(
    defaultTouches.current.reduceRight(
      (pre, curr) => ({ ...pre, [curr]: { touched: true, blur: true } }),
      {} as Record<TFields, ITouch>
    )
  );
  const [errors, setErrors] = useState<Record<TFields, IFormError>>(
    {} as Record<TFields, IFormError>
  );

  useEffect(() => {
    setTouches(
      defaultTouches.current.reduceRight(
        (pre, curr) => ({ ...pre, [curr]: { touched: true, blur: true } }),
        {} as Record<TFields, ITouch>
      )
    );
    setErrors({} as Record<TFields, IFormError>);
  }, [changes]);

  useEffect(() => {
    if (isEqual(formErrors, memoFormErrors)) return;

    setMemoFormErrors(formErrors);
  }, [formErrors, memoFormErrors]);

  useEffect(() => {
    const touchesErrors = Object.keys(touches)
      .map(k => k as TFields)
      .filter(k => touches[k].blur)
      .reduceRight(
        (pre, curr) => ({ ...pre, [curr]: memoFormErrors[curr] }),
        {}
      ) as Record<TFields, IFormError>;

    setErrors(errs => ({ ...errs, ...touchesErrors }));
  }, [touches, memoFormErrors]);

  const setTouched = useCallback<
    (field: TFields, blur?: boolean) => () => void
  >((field, blur = false): (() => void) => {
    const handleTouched = () => {
      setTouches(currentTouches => {
        const touch = (currentTouches[field] && {
          ...currentTouches[field]
        }) || {
          touched: false,
          blur: false
        };
        if (blur && touch.blur === false) {
          touch.blur = true;
          touch.touched = true;
        }
        if (!blur && touch.blur === true) touch.blur = false;

        const isChange = currentTouches[field]?.blur !== touch.blur;
        return !isChange
          ? currentTouches
          : { ...currentTouches, [field]: touch };
      });
    };

    return handleTouched;
  }, []);

  return [touches, errors, setTouched];
};
