import { act } from '@testing-library/react';
import { renderHook } from '@testing-library/react-hooks';
import { useFormValidations } from 'app/hooks/useFormValidations';

describe('useFormValidations ', () => {
  it('should not show error', () => {
    const currentErrors = {
      usrName: {
        status: false,
        message: ''
      }
    } as Record<string, IFormError>;

    const { result } = renderHook(() => useFormValidations(currentErrors));

    act(() => {
      result.current[2]('usrName')();
    });
    expect(result.current[0]).toEqual({
      usrName: { touched: false, blur: false }
    });
    act(() => {
      result.current[2]('usrName', true)();
    });
    expect(result.current[0]).toEqual({
      usrName: { touched: true, blur: true }
    });
  });
  it('should show error', () => {
    const currentErrors = {
      usrName: { status: true, message: 'message' }
    } as Record<string, IFormError>;

    const { result, rerender } = renderHook(
      (errors: Record<string, IFormError>) => useFormValidations(errors),
      { initialProps: currentErrors }
    );

    act(() => result.current[2]('usrName')());
    expect(result.current[0]).toEqual({
      usrName: { touched: false, blur: false }
    });
    expect(result.current[1]).toEqual({ usrName: undefined });

    act(() => result.current[2]('usrName', true)());
    expect(result.current[0]).toEqual({
      usrName: { touched: true, blur: true }
    });
    expect(result.current[1]).toEqual({
      usrName: { status: true, message: 'message' }
    });

    act(() => result.current[2]('usrName')());
    expect(result.current[0]).toEqual({
      usrName: { touched: true, blur: false }
    });

    act(() => result.current[2]('usrName')());
    expect(result.current[0]).toEqual({
      usrName: { touched: true, blur: false }
    });

    rerender({});
    act(() => result.current[2]('usrName', true)());
    expect(result.current[1]).toEqual({
      usrName: undefined
    });
  });
  it('should show error on default', () => {
    const currentErrors = {
      usrName: { status: true, message: 'message' }
    } as Record<string, IFormError>;

    const { result } = renderHook(() =>
      useFormValidations(currentErrors, 0, 'usrName')
    );

    expect(result.current[0]).toEqual({
      usrName: { touched: true, blur: true }
    });
    expect(result.current[1]).toEqual({
      usrName: { status: true, message: 'message' }
    });
  });
});
