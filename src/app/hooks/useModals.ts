import {
  actionsModals as actions,
  useSelectCurrentOpenedModal
} from 'pages/_commons/redux/modal';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';

export const useModalRegistry = (
  name: string,
  opened: boolean,
  isModalFull = false
) => {
  const { register, close } = actions;
  const current = useSelectCurrentOpenedModal(name);
  const dispatch = useDispatch();

  useEffect(() => {
    opened && dispatch(register({ name, isModalFull }));
    !opened && dispatch(close(name));

    return () => {
      dispatch(close(name));
    };
  }, [name, opened, isModalFull, dispatch, register, close]);

  return current;
};
