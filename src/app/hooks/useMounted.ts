import { useEffect, useRef } from 'react';

export const useMounted = () => {
  const mount = useRef<boolean | null>(null);

  useEffect(() => {
    mount.current = true;

    return () => {
      mount.current = false;
    };
  });

  return mount;
};
