import { classnames } from 'app/helpers';
import { isFunction } from 'lodash';
import React, { useCallback, useEffect, useMemo } from 'react';

export const useCheckAllPagination = (
  {
    gridClassName,
    allIds,
    hasFilter,
    filterIds,
    checkedList,
    setCheckAll,
    setCheckList,
    checkableIds
  }: {
    gridClassName?: string;
    allIds: string[];
    hasFilter: boolean;
    filterIds: string[];
    checkedList: string[];
    setCheckAll: (isCheckAll: boolean) => void;
    setCheckList: (checkList: string[]) => void;
    checkableIds?: string[];
  },
  dependencies?: any[]
): {
  gridClassName?: string;
  handleClickCheckAll: (e: React.MouseEvent) => void;
} => {
  const filterIdsWithCheckable = checkableIds
    ? filterIds.filter(id => checkableIds.includes(id))
    : filterIds;
  const checkableList = checkableIds ?? allIds;

  const gridIds = useMemo(
    () => (hasFilter ? filterIdsWithCheckable : checkableList),
    [hasFilter, filterIdsWithCheckable, checkableList]
  );
  const checkedIds = useMemo(
    () =>
      hasFilter
        ? filterIdsWithCheckable.filter(id => checkedList.includes(id))
        : checkedList,
    [hasFilter, filterIdsWithCheckable, checkedList]
  );

  const isCheckAll = useMemo(
    () => gridIds.length === checkedIds.length && gridIds.length !== 0,
    [gridIds, checkedIds]
  );
  const isIndeterminate = useMemo(
    () => checkedIds.length > 0 && checkedIds.length < gridIds.length,
    [gridIds.length, checkedIds.length]
  );

  useEffect(() => {
    const classSelector = gridClassName ? `.${gridClassName} ` : '';
    const checkboxHeader: HTMLInputElement = document.querySelector(
      `${classSelector}.dls-grid-head input[type=checkbox]`
    ) as HTMLInputElement;

    if (!checkboxHeader) return;

    process.nextTick(() => {
      const checkboxClassList = checkboxHeader.classList;
      !checkboxClassList.contains('check-all--cus') &&
        checkboxClassList.add('check-all--cus');

      if (!isIndeterminate) {
        checkboxClassList.remove('indeterminate');
        checkboxClassList.remove('indeterminate-cus');
      } else {
        checkboxClassList.add('indeterminate');
        checkboxClassList.add('indeterminate-cus');
      }

      checkboxHeader.checked = isCheckAll || isIndeterminate;
    });
  }, [
    gridClassName,
    isCheckAll,
    isIndeterminate,
    checkedIds,
    gridIds,
    dependencies
  ]);

  const handleClickCheckAll = useCallback(
    (e: React.MouseEvent) => {
      e.preventDefault();

      if (hasFilter) {
        const _checkList = isCheckAll
          ? checkedList.filter(x => !filterIdsWithCheckable.includes(x))
          : Array.from(new Set([...checkedList, ...filterIdsWithCheckable]));

        isFunction(setCheckList) && setCheckList(_checkList);
      } else {
        isFunction(setCheckAll) && setCheckAll(!isCheckAll);
      }
    },
    [
      setCheckAll,
      setCheckList,
      hasFilter,
      checkedList,
      filterIdsWithCheckable,
      isCheckAll
    ]
  );

  const result = useMemo(
    () => ({
      gridClassName: classnames('grid-checkall--cus', gridClassName),
      handleClickCheckAll
    }),
    [handleClickCheckAll, gridClassName]
  );

  return result;
};
