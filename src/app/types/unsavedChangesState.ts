import { BtnVariants } from 'app/_libraries/_dls';
import React from 'react';

export interface UnsavedChangesRedirect {
  completed?: boolean;
  to?: string;
  formsWatcher?: string[];
  onDiscard?: (formName: string) => void;
  onClose?: (formName: string) => void;
  onConfirm?: (
    formName?: string
  ) => Promise<boolean | undefined | void> | boolean | undefined | void;
}
export interface UnsavedChangesButtonProps {
  className?: string;
  hide?: boolean;
  text?: string;
  variant?: BtnVariants;
}
export interface UnsavedChangesOptions {
  formName: string;
  change: boolean;
  priority: number;
  confirmFirst?: boolean;
  confirmTitle?: string;
  confirmMessage?: React.ReactNode;
  hideUnsavedConfirmation?: boolean;
  size?: 'sm' | 'md' | 'lg' | 'xl';
  footerClassName?: string;
  discardBtn?: UnsavedChangesButtonProps;
  closeBtn?: UnsavedChangesButtonProps;
  confirmBtn?: UnsavedChangesButtonProps;
  onDiscard?: () => void;
  onClose?: () => void;
  onConfirm?: (apiCallSuccess?: boolean | void) => void;
}
export interface UnsavedChangesState {
  redirect: UnsavedChangesRedirect;
  changes: Record<string, UnsavedChangesOptions>;
}
