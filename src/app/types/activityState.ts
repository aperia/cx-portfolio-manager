export interface ActivityState {
  lastUpdatedActivities: LastUpdatedActivitiesState;
  activitiesInChange: ActivitiesInChangeState;
}

export interface ActivitiesState {
  activities: IActivity[];
}
export interface LastUpdatedActivitiesState extends ActivitiesState {
  loading: boolean;
  error?: boolean;
}
export interface ActivitiesInChangeState extends ActivitiesState {
  loading: boolean;
  error?: boolean;
  dataFilter?: IDataFilter;
}
