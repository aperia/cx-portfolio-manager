export interface DMMTablesState {
  changeDMMTables: ChangeDMMTables;
  downloadDecisionElement: DownloadAttachment;
}

export interface ChangeDMMTables {
  dmmTablesList: IDMMData[];
  loading: boolean;
  error?: boolean;
  dataFilter?: IDataFilter;
}
export interface DownloadAttachment extends IFetching {
  attachment: IDownloadAttachment;
}
