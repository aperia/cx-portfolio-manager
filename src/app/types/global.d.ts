import {
  ElementsFieldParameterEnum,
  MethodFieldParameterEnum,
  RecordFieldParameterEnum,
  SendLetterParameterEnum,
  TableFieldParameterEnum
} from 'app/constants/enums';
import {
  ActivityState,
  ChangeState,
  CommonState,
  DMMTablesState,
  II18nextReducer,
  MappingState,
  ModalsState,
  NonWorkflowState,
  NotificationState,
  PortfolioState,
  PricingMethodsState,
  PricingStrategiesState,
  RefDataState,
  ToastState,
  UnsavedChangesState,
  WorkflowSetupRootState,
  WorkflowState
} from 'app/types';
import React from 'react';
import { Action, ActionCreator, Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { AuthState } from './commonState';

declare global {
  interface AppState {
    common?: CommonState;
    mapping?: MappingState;
    refData?: RefDataState;
    toastNotifications?: ToastState;
    change?: ChangeState;
    activity?: ActivityState;
    portfolio?: PortfolioState;
    nonWorkflow?: NonWorkflowState;
    workflow?: WorkflowState;
    workflowSetup?: WorkflowSetupRootState;
    pricingMethods?: PricingMethodsState;
    pricingStrategies?: PricingStrategiesState;
    i18next?: II18nextReducer;
    modals?: ModalsState;
    notification?: NotificationState;
    workflowTemplate?: WorkflowTemplateState;
    dmmTables?: DMMTablesState;
    auth?: AuthState;
    unsavedChanges?: UnsavedChangesState;
  }

  interface RootState extends AppState {
    form: Record<string, any>;
  }

  interface AppConfiguration {
    i18n?: {
      supplemental?: string;
      en?: {
        allComponents?: string;
        main?: string;
      };
      ja?: {
        allComponents?: string;
        main?: string;
      };
      vi?: {
        allComponents?: string;
        main?: string;
      };
    };
    api?: {
      system?: {
        login?: string;
        getUserInfo?: string;
      };
      refData?: {
        getStateRefData?: string;
      };
    };
    mappingUrl?: string;
    workflowMappingUrl?: string;
  }

  interface Window {
    account?: Account;

    debug?: boolean;
    appConfig?: AppConfiguration;
  }

  interface IFormError {
    status: boolean;
    message: string;
  }

  interface Account {
    allComponents?: Record<string, React.ReactType>;
    dofConfigs?: string[];
    dofDataSources?: string[];
    rootUrl?: string;
    apiUrl?: string;
    resourceUrl?: string;
    globalApplicationLayout?: Record<string, React.ReactType>;
    layoutConfigUrl?: string;
    sessionExpiring?: number;
    envWorkflowSteps?: string[];
  }

  interface ReactSetting {
    dofConfigs?: string[];
    dofDataSources?: string[];
    rootUrl?: string;
    apiUrl?: string;
    resourceUrl?: string;
    sessionExpiring?: number;
    envWorkflowSteps?: string[];
  }

  type AppThunk = ActionCreator<
    ThunkAction<void, AppState, unknown, Action<string>>
  >;

  type ThunkAPIConfig = {
    dispatch: Dispatch;
    rejectValue: {
      errorMessage: string;
    };
    state: RootState;
    extra: any;
  };

  interface StoreIdPayload {
    storeId: string;
  }

  interface ForceRefreshStoreIdPayload extends StoreIdPayload {
    isForceRefresh?: boolean;
  }

  interface MagicKeyValue {
    [key: string]: any;
  }

  interface OptionsData extends RefData {
    textDmm?: string;
  }

  interface RefData {
    code: string;
    text?: string;
    selected?: boolean;
  }

  interface IBaseRequestModel {
    selectFields?: string[];
    searchFields?: Record<string, any>;
  }

  interface RefDataString {
    code?: string;
    text?: string;
  }

  interface IPortfolioKpiDetails {
    portfolioId: string;
    kpi: string;
    subjectArea: string;
    submetric: string;
    name: string;
    period: string;
    daysPreviously: string;
  }

  interface IPortfolioKpiUpdate {
    id: number;
    name: string;
    period: string;
    daysPreviously: string;
  }

  interface IPortfolioModel {
    id: string;
    portfolioName: string;
    portfolioId: string;
    icon: string;
    cardType: string;
    cardTypeUrl: string;
    clientId: string;
    portfoliosImpacted: number;
    totalDelinquency: number;
    availableAccounts: number;
    netIncome: number;
    availableCredit: number;
    portfolioNew: number;
    spa: string[];
    activeAccounts: number;
    payments: number;
  }

  interface IPortfolioCardView extends IPortfolioModel {}

  interface IPortfoliosDetail {
    id?: string;
    portfolioCriteria?: IPortfolioCriteria[];
    portfolioStatistic?: IPortfolioStatistic;
    icon?: string;
    name?: string;
  }

  interface IPortfolioCriteria {
    fieldName: string;
    values: any[];
  }

  interface IPortfolioStatistic {
    kpiList: string[];
  }

  interface IKPIListForSetup {
    id: string;
    name: string;
    kpi: string;
    subjectArea: string;
    submetric: string;
  }

  interface IKPIDetails {
    id: string;
    name: string;
    kpi: string;
    subjectArea: string;
    submetric: string;
    period: string;
    daysPreviously: string;
    items: IItem[];
  }

  interface IItem {
    name: string;
    values: IValue[];
  }

  interface IValue {
    label: string;
    value: string;
  }

  interface IAddPortfolioKPI {
    id: string;
    kpiDetails: IPortfolioKpiDetails;
  }

  interface IUpdatePortfolioKPI {
    messageText: string;
    resultCode: string;
  }

  interface IDeletePortfolioKPI {
    kpiId: string;
  }

  interface IPricingStrategyModel {
    id: string;
    name: string;
    description: string;
    modeledFrom: string;
    methodsUpdated: number;
    versionCreating: string;
    methods: IPricingStrategyMethod[];
    impactingMethods: string[];
  }

  interface IPricingStrategyMethod {
    method: string;
    previousValue: string;
    newValue: string;
  }

  interface IWorkflowModel {
    id: string;
    isFavorite?: string;
    name: string;
    description: string;
    workflowNote: string;
    updatedDate: string;
    updatedBy: string;
    workflowType: string;
    status: string;
    firstRan: string;
    firstRanBy: string;
    countOfAttachments: number;
    validationMessages: IValidationMessage[];
    actions: string[];

    changeDetail: {
      name: string;
      changeSetId: string;
    };
    workflowTemplateId?: string;
  }

  interface IWorkflowTemplateModel {
    id: string;
    isFavorite?: boolean;
    name: string;
    description: string;
    lastExecuted: string;
    type: string;
    numberOfExcutions: number;
    accesible: boolean;
    iconName: string;
  }

  interface IKpiTemplateModel {
    id: string;
    name: string;
    subjectArea: string;
    submetric: string;
  }

  interface IWorkflowCardView extends IWorkflowModel {
    actionPayload?: IWorkflowModel;
    continuePayload?: IWorkflowModel;
    workflowError?: boolean;
    workflowWarning?: boolean;
  }

  interface IChangeDetail {
    changeId: string;
    changeName: string;
    changeOwner: string;
    changeType: string;
    changeStatus?: string;
    createdDate: string;
    effectiveDate: string;
    portfoliosImpacted?: number;
    cardholdersAccountsImpacted?: number;
    approvalDate?: string;
    lastApprovalBy?: string;
    changeInTermsStatus?: string;
    pricingStrategiesCreatedCount?: number;
    pricingStrategiesUpdatedCount?: number;
    pricingStrategiesImpacted?: number;
    pricingMethodsCreatedCount?: number;
    pricingMethodsUpdatedCount?: number;
    changeDescription?: string;
    noOfDMMTablesCreated?: number;
    noOfDMMTablesUpdated?: number;
    rejectedBy?: string;
    rejectedReason?: string;
    validationMessages?: IValidationMessage[];
    actions: string[];
    approvalRecords: IApprovalRecord[];
    pendingApprovalBy: IPendingApprovalBy[];
    lastChangedBy?: {
      id: number | string;
      name: string;
    };
    lastChangedDate: Date;
  }

  interface IApprovalRecord {
    actionTime: Date;
    group: {
      id: number | string;
      name: string;
      changeType: string;
      defaultgroup: boolean;
    };
    user: {
      id: number | string;
      name: string;
    };
    action: string;
    reason: string;
  }

  interface IPendingApprovalBy {
    id: string;
    name: string;
    changeType: string;
    nextApproverGroup?: IPendingApprovalBy;
    defaultgroup: boolean;
    users?: {
      id: number | string;
      name: string;
    }[];
    user?: any[];
  }

  interface IChangeSnapshot extends IChangeDetail {}

  interface IWorkflowChangeDetail extends IChangeDetail {
    workflowInstanceCount?: number;
  }

  interface IActivity {
    id: string;
    name: string;
    action: string;
    dateChanged: string;
    changeId?: string;
    changeName?: string;
    actionDetails?: IActivityDetail[];
  }

  interface IActivityDetail {
    field?: string;
    previousValue?: string;
    newValue?: string;
    workflowName?: string;
    workflowId?: string;
    canOpenWorkflow?: boolean;
  }

  interface IPricingMethod {
    id: string;
    name: string;
    methodType: string;
    versionCreating: string;
    modeledFrom: string;
    parametersUpdated: number;
    parameters: IPricingMethodParam[];
    newVersionOf: string;
    strategiesImpactedCount: number;
    strategiesImpacted: IStrategiesImpacted[];
  }

  interface IDMMData {
    id: string;
    serviceName: string;
    abbr: string;
    dmmTables: IDMMTable[];
    tableTypeFilter: ITableType[];
    totalItemFilter: number;
  }
  interface IDMMTable {
    id: number;
    parentId: number;
    parentTableId: string;
    parentTableName: string;
    effectiveDate: string;
    comment: string;
    status: string;
    tableId: string;
    tableName: string;
    tableType: ITableType;
    tableTypeDesc: string;
    tableModeled: IDMMTableModeled;
    tableControlParameters: TableContentType[];
    decisionElements: IDecisionElement[];
    modeledFrom: string | undefined;
    newVersionOf: string | undefined;
    actionPayload: any;
    totalDecisionElements: number;
  }

  interface ITableType {
    code: string;
    value: string;
    description: string;
  }
  interface IDMMTableModeled {
    id: string;
    tableId: string;
    tableName: string;
  }
  interface IDownloadAttachment {
    attachmentId: string;
    filename: string;
    mimeType: string;
    fileSize: number;
    fileStream: string;
    createdDate: string;
  }

  interface IDecisionElement {
    name: string;
    searchCode: string;
    immediateAllocation: string;
    value: string[];
  }
  interface IDecisionElement {
    id: string;
    tableName: string;
    changeType: string;
  }

  interface IPricingMethodParam {
    name: string;
    previousValue: string;
    newValue: string;
  }

  interface IStrategiesImpacted {
    id: string;
    name: string;
    description: string;
  }

  interface Window {
    account?: Account;
  }

  interface IChangeCardView extends IChangeDetail {
    actionPayload?: IChangeDetail;
  }

  interface IApprovalQueueCardView extends IChangeDetail {
    actionPayload?: IChangeDetail;
  }

  interface IFetching {
    loading?: boolean;
    error?: boolean;
  }

  type OrderByType = 'asc' | 'desc';

  interface IDataFilter {
    searchValue?: string;
    searchFields?: any;
    sortBy?: string[];
    orderBy?: OrderByType[];
    page?: number;
    pageSize?: number;
    totalItem?: number;
    filterField?: string;
    filterValue?: any;
  }

  interface IAttachment {
    id: string;
    filename: string;
    status: string;
    category: string;
    mimeType: string;
    fileSize: number;
    recordCount: number;
    uploadedDate: string;
    uploadedBy: string;
    workflowName: string;
  }

  interface IReport {
    id: string;
    reportFilename: string;
    status: string;
    mimeType: string;
    reportFileSize: number;
    recordCount: number;
    systemGeneratedDate: string;
    workflowName: string;
  }

  interface IWorkflowDetail {
    id: string;
    name: string;
    description: string;
    updatedDate: string;
    updatedBy: string;
    workflowType: string;
    status: string;
    firstRan: string;
    firstRanBy: string;
    workflowNote: string;

    downloadableAttachment: {
      isReady?: boolean;
      attachmentList: IAttachment[];
    };
    uploadedAttachment: IAttachment[];
    reports: IReport[];
    actions: string[];
    validationMessages: IValidationMessage[];
  }

  interface IValidationMessage {
    changeId?: string;
    workflowId?: string;

    source?: string;

    field: string;
    severity: 'error' | 'caution';
    message: string;

    isChangeFeedback?: boolean;
    actions: string[];
  }

  interface INotification {
    id: string;
    changeSetId: string;
    changeName: string;
    workflowTemplateId?: string;
    workflowName?: string;
    changeStatus: string;
    title: string;
    description: string;
    date: string;
    isRead: boolean;
    isNew: boolean;
    type: string;
    action: string;
    actionBy: string;
    teamName: string;
    approvalRecords?: any[];
    reminderDescription?: string;
  }

  interface RequestCommon {
    startSequence?: number;
    endSequence?: number;
    requestId?: string;
    userData?: string;
    txid?: string;
    user?: string;
    service?: string;
    auth?: string;
    agent?: string;
    cycle?: string;
    clientName?: string;
    clientNumber?: string;
    x500id?: string;
    extensions?: any;
    audience?: string;
    logo?: string;
    originHosts?: string[];
    sourceIp?: string;
    encoding?: string;
    key?: string;
    size?: number;
    retry?: number;
    timeout?: number;
    sessionId?: string;
    cid?: string;
    clientEmail?: string;
    clientId?: string;
    eventOrigin?: string;
    eventOriginInputName?: string;
  }

  interface ProcessorInfo {
    system?: string;
    cycle?: string;
    tor?: string;
    mode?: string;
  }

  type FieldType<T extends any> = {
    value?: T;
  };

  type TButtonActionFnc<T = any> = (
    value: T,
    dispatch: Dispatch<any>,
    event?: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => void;

  type TButtonActionAsyncFnc<T = any> = (
    value: T,
    dispatch: Dispatch<any>,
    event?: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => Promise;

  type Condition = boolean | ConditionWrapper | Comparation;
  type ConditionOperator = 'and' | 'or';
  interface ConditionWrapper {
    operator: ConditionOperator;
    childrens: Condition[];
  }
  type ComparationOperator = '>' | '<' | '=' | '!=' | '>=' | '<=';
  interface Comparation {
    operator: ComparationOperator;
    source: string;
    target?: any;
    targetPath?: string;
  }

  interface WorkflowSetupInfoConfig {
    title?: string;
    workflowInfo?: string;
  }
  interface WorkflowSetupConfig extends WorkflowSetupInfoConfig {
    steps: WorkflowSetupStepConfig[];
  }
  interface WorkflowSetupStepConfig {
    id: string;
    title: string;
    component: string;

    condition?: Condition;
    dataApi?: string;
    wrappedContent?: boolean;
    order?: number;
    envStep?: string;
  }

  interface WorkflowSetupStep {
    id: string;
    title: string;

    description?: string | any;
    error?: string | boolean;
    dataApi?: string;
    inprogress?: boolean;
  }
  interface WorkflowSetupContent {
    id: string;
    title: string;
    content: React.ReactElement;

    wrappedContent?: boolean;
  }

  interface SaveOptions {
    saveOnClose?: boolean;
    saveInBackground?: boolean;
  }
  type WorkflowSetupStepFormDataFunc<T = {}> = (
    workflowData: WorkflowSetupInstance,
    id: string,
    dispatch: Dispatch<any>
  ) => (WorkflowSetupStepFormData & T) | undefined;
  type WorkflowSetupStepValueFunc<T = {}, U = {}> = (params: {
    stepsForm: T;
    dependencies?: U;
    t: any;
  }) => string | React.ReactNode;
  type WorkflowSetupStepErrorFunc<T = {}, U = {}> = (
    stepsForm: T,
    dependencies?: U
  ) => string | boolean;
  type WorkflowSetupIsDefaultValueFunc<T = {}> = (
    formValues: T,
    defaultValues?: Partial<T>
  ) => boolean;
  type WorkflowSetupIsEqualFormValuesFunc<T = {}> = (f1: T, f2: T) => boolean;
  interface WorkflowSetupProps<T = {}, U = {}> {
    title?: string;
    stepId: string;
    selectedStep: string;
    isEdit: boolean;
    savedAt: number;
    formValues: T;
    setFormValues: (values: Partial<T>) => void;
    clearFormValues: (step: string) => void;
    handleSave: VoidFunction;

    workflow?: IWorkflowModel | IWorkflowTemplateModel;
    isStuck?: boolean;
    dependencies?: U;
    hasInstance?: boolean;
    ready?: boolean;
    type?: string;
  }
  interface WorkflowSetupSummaryStepProps<T = {}, U = {}>
    extends WorkflowSetupProps<T, U> {
    summaries: WorkflowSetupSummary[];
    handleSelectStep?: (stepId: string, moveNextLastStep?: boolean) => void;
  }
  interface WorkflowSetupSummary {
    stepId: string;
    title: string;
    element: React.ElementType<WorkflowSetupSummaryProps>;
    dependencies?: Record<string, any>;
  }
  interface WorkflowSetupSummaryProps<T = {}, U = {}>
    extends Partial<WorkflowSetupProps<T, U>> {
    originalStepConfig: any;
    formValues: T;
    dependencies?: U;
    onEditStep?: () => void;
  }
  interface WorkflowSetupInstance {
    workflowInstanceDetails: {
      id: string;
      workflowTemplate: IWorkflowTemplateModel;
      notes: string;
      status: string;
      changeSetId: string;
    };
    data: {
      workflowSetupData: WorkflowSetupInstanceStep[];
      configurations: {
        methodList?: WorkflowSetupMethod[];
        strategies?: StrategyListFormValues[];
        attachmentId?: string;
        transactionId?: string;
        parameters?: WorkflowSetupParamter[];
        changeDetail?: {
          id: string;
          name: string;
          description: string;
          effectiveDate: string;
        };
        sysPrinAgents?: MagicKeyValue[];
        ocsShells: MagicKeyValue[];
        merchants?: MagicKeyValue[];
        dataTableVersion?: MagicKeyValue;
        tableList: MagicKeyValue[];
        attachments: MagicKeyValue[];
        porfolioList: MagicKeyValue[];
        dataLinkElements?: MagicKeyValue[];
        shellProfile?: MagicKeyValue;
        profileList?: MagicKeyValue[];
        queueProfile?: MagicKeyValue;
        shellList?: MagicKeyValue[];
      };
    };
  }
  interface WorkflowSetupStepFormData {
    isSelected: boolean;
    isPass: boolean;
  }
  interface WorkflowSetupInstanceStep {
    id: string;
    status: 'DONE' | 'INPROGRESS' | 'READY';
    selected?: boolean;
    data?: any;
  }

  interface IMethodVersion {
    id: string;
    comment: string;
    effectiveDate: string;
    status: string;
    parameters: any;
  }

  interface IWorkflowStrategyModel {
    id: string;
    name: string;
    description: string;
    effectiveDate: string;
    numberOfCardHolders: number;
  }

  interface IStrategyModel {
    id: number;
    name: string;
    description: string;
    effectiveDate: string;
    status: string;
    processed: number;
    accounts: number;
    checked?: boolean;
  }

  interface IServiceSubjectSectionModel {
    id: number;
    value: string;
    currentAssignment: string;
    newAssignments: RefData[];
  }

  interface StrategyVersion {
    effectiveDate: string;
    accountsProcessedAA: string;
    accountsProcessedALP: string;
    status: string;
    cisDescription: string;
    commentArea: string;
    id: string;
    methods: StrategyMethod[];
  }

  interface StrategyMethod {
    currentAssignment: string;
    serviceSubjectSection: string;
  }

  interface Strategy {
    id: string;
    name: string;
    versions: StrategyVersion[];
  }

  interface IParameterModel {
    type: string;
    name: string;
    description: string;
  }

  interface IMethodModel {
    id: string;
    name: string;
    effectiveDate: string;
    comment: string;
    relatedPricingStrategies: number;
    numberOfVersions: number;
    versions: IMethodVersion[];
    pricingStrategies?: IWorkflowStrategyModel[];
    serviceSubjectSection?: string;
  }

  interface IStrategyModel {
    id: string;
    name: string;
    effectiveDate: string;
  }

  interface ITextAreaModel {
    id: string;
    name: string;
    effectiveDate: string;
    textType: string;
    languageCode: string;
  }

  interface ITableAreaModel {
    id: string;
    name: string;
    effectiveDate: string;
  }

  type WorkflowSetupMethodType = 'NEWMETHOD' | 'MODELEDMETHOD' | 'NEWVERSION';

  type WorkflowSetupRecordType =
    | 'New'
    | 'Updated'
    | 'Not Updated'
    | 'Marked for Deletion'
    | 'Marked for Removal';

  interface WorkflowSetupMethodModeledFrom {
    id: string;
    name?: string;
    versions: WorkflowSetupMethodModeledFromVersion[];
    index?: number;
    tableName?: string;
    tableId?: string;
  }
  interface WorkflowSetupMethodModeledFromVersion {
    id?: string;
    tableId?: string;
  }
  interface WorkflowSetupMethodParams {
    name: string;
    previousValue: string;
    newValue: string;
  }
  interface WorkflowSetupRecordParams extends WorkflowSetupMethodParams {}

  interface WorkflowSetupElementParams {
    name: string;
    value: string;
  }

  interface WorkflowSetupMethod {
    id: string;
    name: string;
    modeledFrom: WorkflowSetupMethodModeledFrom;
    methodType: WorkflowSetupMethodType;
    comment: string;
    serviceSubjectSection?: string;
    parameters: WorkflowSetupMethodParams[];
    tableName?: string;
    tableId?: string;
    versionParameters: WorkflowSetupMethodParams[];
    tableControlParameters: TableContentType[];
    decisionElements: DecisionType[];
    elementList: DecisionType[];
  }
  interface WorkflowSetupParamter {
    name: string;
    value: string;
  }

  interface WorkflowSetupRecord {
    id: string;
    name: string;
    status: WorkflowSetupRecordType;
    actionCode: string;
    parameters: WorkflowSetupRecordParams[];
    versionParameters?: WorkflowSetupMethodParams[];
  }

  interface WorkflowSetupRecordObjectParams {
    id: string;
    name: string;
    status: WorkflowSetupRecordType;
    actionCode: string;
    versionParameters?: WorkflowSetupMethodParams[];

    [RecordFieldParameterEnum.RecordType]?: string;
    [RecordFieldParameterEnum.GenerationMethod]?: string;
    [RecordFieldParameterEnum.RangeFrom]?: string;
    [RecordFieldParameterEnum.RangeTo]?: string;
    [RecordFieldParameterEnum.SerialNumber]?: string;
    [RecordFieldParameterEnum.WarningLevel]?: string;
    [RecordFieldParameterEnum.Prefix]?: string;
    [RecordFieldParameterEnum.AlternatePrefix]?: string;
    [RecordFieldParameterEnum.AccountIDOrPIID]?: string;
    [RecordFieldParameterEnum.NextFrom]?: string;
    [RecordFieldParameterEnum.NextTo]?: string;
    [RecordFieldParameterEnum.AvailableRecordNumber]?: string;
    [RecordFieldParameterEnum.UseAlternateAgent]?: string;
    [RecordFieldParameterEnum.AlternatePrincipal]?: string;
    [RecordFieldParameterEnum.AlternateAgent]?: string;
  }

  interface WorkflowSetupElement {
    elementName: string;
    elementId: string;
    description: string;
    parameters: WorkflowSetupElementParams[];
  }

  interface WorkflowSetupElementObjectParams {
    elementName: string;
    elementId: string;
    description: string;

    [ElementsFieldParameterEnum.ElementID]?: string;
    [ElementsFieldParameterEnum.ElementName]?: string;
    [ElementsFieldParameterEnum.TopName]?: string;
    [ElementsFieldParameterEnum.BottomName]?: string;
    [ElementsFieldParameterEnum.Reallocation]?: string;
    [ElementsFieldParameterEnum.DeltaType]?: string;
    [ElementsFieldParameterEnum.TransferTypes]?: string;
    [ElementsFieldParameterEnum.ElementType]?: string;
    [ElementsFieldParameterEnum.ElementLength]?: string;
    [ElementsFieldParameterEnum.NumberOfDecimals]?: string;
    [ElementsFieldParameterEnum.MinValue]?: string;
    [ElementsFieldParameterEnum.MaxValue]?: string;
    [ElementsFieldParameterEnum.ValidValues]?: string;
    [ElementsFieldParameterEnum.DefaultValue]?: string;
    [ElementsFieldParameterEnum.LetterFormat]?: string;
    [ElementsFieldParameterEnum.UseDefaultOnLetter]?: string;
    [ElementsFieldParameterEnum.OCSTran]?: string;
    [ElementsFieldParameterEnum.MaskText]?: string;
    [ElementsFieldParameterEnum.RealTimeUpdate]?: string;
    [ElementsFieldParameterEnum.RetainOnTransfer]?: string;
    [ElementsFieldParameterEnum.ElementBusinessDescription]?: string;
  }

  interface DecisionType {
    immediateAllocation: string;
    name: string;
    searchCode: string;
    value: string[];
  }
  interface TableContentType {
    businessName: string;
    greenScreenName: string;
    moreInfo: string;
    name: string;
    value: any | undefined;
  }

  interface TableControlParameters {
    // Manage Method Level Processing Decision Tables MLP
    [TableFieldParameterEnum.CAAllocationFlag]?: string;
    [TableFieldParameterEnum.AQAllocationFlag]?: string;
    [TableFieldParameterEnum.AllocationInterval]?: string;
    [TableFieldParameterEnum.AllocationBeforeAfterCycle]?: string;
    [TableFieldParameterEnum.ChangeCode]?: string;
    [TableFieldParameterEnum.ChangeInTermsMethodFlag]?: string;
    [TableFieldParameterEnum.ServiceSubjectSectionAreas]?: string;
  }

  interface WorkflowSetupMethodObjectParams {
    id: string;
    name: string;
    modeledFrom: WorkflowSetupMethodModeledFrom;
    methodType: WorkflowSetupMethodType;
    comment: string;

    tableId?: string;
    tableName?: string;
    effectiveDate?: string;
    status?: string;

    versionParameters?: WorkflowSetupMethodParams[];
    tableControlParameters?: TableContentType[];
    decisionElements?: DecisionType[];

    [MethodFieldParameterEnum.ReturnedCheckChargesOption]?: string;
    [MethodFieldParameterEnum.AmountAssessed]?: string;
    [MethodFieldParameterEnum.MinimumPayDueOptions]?: string;
    [MethodFieldParameterEnum.OneFeeParticipationCode]?: string;
    [MethodFieldParameterEnum.BatchIdentification]?: string;
    [MethodFieldParameterEnum.ComputerLetter]?: string;
    [MethodFieldParameterEnum.PostingTextID]?: string;
    [MethodFieldParameterEnum.ReversalTextID]?: string;
    [MethodFieldParameterEnum.FirstYrMaxManagement]?: string;
    [MethodFieldParameterEnum.OneFeePriorityCode]?: string;
    [MethodFieldParameterEnum.LateChargesOption]?: string;
    [MethodFieldParameterEnum.CalculationBase]?: string;
    [MethodFieldParameterEnum.FixedAmount]?: string;
    [MethodFieldParameterEnum.Percent]?: string;
    [MethodFieldParameterEnum.MinimumAmount]?: string;
    [MethodFieldParameterEnum.MaximumAmount]?: string;
    [MethodFieldParameterEnum.MaximumYearlyAmount]?: string;
    [MethodFieldParameterEnum.CyclesOfConsecutiveDelinquency]?: string;
    [MethodFieldParameterEnum.AssessedAccounts]?: string;
    [MethodFieldParameterEnum.ExclusionBalance]?: string;
    [MethodFieldParameterEnum.CurrentBalanceAssessment]?: string;
    [MethodFieldParameterEnum.CalculationDayControl]?: string;
    [MethodFieldParameterEnum.NumberOfDays]?: string;
    [MethodFieldParameterEnum.NonProcessingLateCharge]?: string;
    [MethodFieldParameterEnum.IncludeExcludeControl]?: string;
    [MethodFieldParameterEnum.Status1]?: string;
    [MethodFieldParameterEnum.Status2]?: string;
    [MethodFieldParameterEnum.Status3]?: string;
    [MethodFieldParameterEnum.Status4]?: string;
    [MethodFieldParameterEnum.Status5]?: string;
    [MethodFieldParameterEnum.AssessmentControl]?: string;
    [MethodFieldParameterEnum.BalanceIndicator]?: string;
    [MethodFieldParameterEnum.Tier2Balance]?: string;
    [MethodFieldParameterEnum.Tier2Amount]?: string;
    [MethodFieldParameterEnum.Tier3Balance]?: string;
    [MethodFieldParameterEnum.Tier3Amount]?: string;
    [MethodFieldParameterEnum.Tier4Balance]?: string;
    [MethodFieldParameterEnum.Tier4Amount]?: string;
    [MethodFieldParameterEnum.Tier5Balance]?: string;
    [MethodFieldParameterEnum.Tier5Amount]?: string;
    [MethodFieldParameterEnum.ThresholdToWaveLateCharge]?: string;
    [MethodFieldParameterEnum.LateChargeResetCounter]?: string;
    [MethodFieldParameterEnum.ReversalDetailsTextID]?: string;
    [MethodFieldParameterEnum.WaivedMessageTextID]?: string;
    [MethodFieldParameterEnum.LatePaymentWarningMessage]?: string;

    //design loan offers - Loan offer basic settings
    [MethodFieldParameterEnum.PromotionDescription]?: string;
    [MethodFieldParameterEnum.PromotionAlternateDescription]?: string;
    [MethodFieldParameterEnum.PromotionDescriptionDisplayOptions]?: string;
    [MethodFieldParameterEnum.PromotionStatementTextIDDTDD]?: string;
    [MethodFieldParameterEnum.PromotionStatementTextIDDTDX]?: string;
    [MethodFieldParameterEnum.PromotionStatementTextIDStandard]?: string;
    [MethodFieldParameterEnum.PromotionStatementTextControl]?: string;
    [MethodFieldParameterEnum.PromotionReturnApplicationOption]?: string;
    [MethodFieldParameterEnum.PromotionPayoffExceptionOption]?: string;
    [MethodFieldParameterEnum.PayoffExceptionMethod]?: string;
    [MethodFieldParameterEnum.CashAdvanceItemFeesMethod]?: string;
    [MethodFieldParameterEnum.MerchantItemFeesMethod]?: string;
    [MethodFieldParameterEnum.CreditApplicationGroup]?: string;
    [MethodFieldParameterEnum.PromotionGroupIdentifier]?: string;
    [MethodFieldParameterEnum.LoanBalanceClassification]?: string;
    //design loan offers - payment and pricing settings
    [MethodFieldParameterEnum.StandardMinimumPaymentCalculationCode]?: string;
    [MethodFieldParameterEnum.RulesMinimumPaymentDueMethod]?: string;
    [MethodFieldParameterEnum.StandardMinimumPaymentsRate]?: string;
    [MethodFieldParameterEnum.StandardMinimumPaymentRoundingOptions]?: string;
    [MethodFieldParameterEnum.AnnualInterestRate]?: string;
    [MethodFieldParameterEnum.PayoutPeriod]?: string;

    //design loan offers - merchant discount settings
    [MethodFieldParameterEnum.MerchantDiscount]?: string;
    [MethodFieldParameterEnum.MerchantDiscountCalculationCode]?: string;

    //design loan offers - advanced settings
    [MethodFieldParameterEnum.ReturnToRevolvingBaseInterest]?: string;
    [MethodFieldParameterEnum.ApplyPenaltyPricingOnLoanOffer]?: string;
    [MethodFieldParameterEnum.DefaultInterestRateMethodOverride]?: string;
    [MethodFieldParameterEnum.InterestCalculationMethodOverride]?: string;
    [MethodFieldParameterEnum.IntroductoryMessageDisplayControl]?: string;
    [MethodFieldParameterEnum.PositiveAmortizationUsageCode]?: string;

    //design loan offers - advanced introductory APR settings
    [MethodFieldParameterEnum.IntroductoryAnnualRate]?: string;
    [MethodFieldParameterEnum.IntroductoryAnnualRateFixedEndDate]?: string;
    [MethodFieldParameterEnum.IntroductoryRateNumberOfDays]?: string;
    [MethodFieldParameterEnum.IntroductoryRateNumberOfCycles]?: string;
    [MethodFieldParameterEnum.IntroductoryPeriodStatementTextID]?: string;

    //design loan offers - advanced promotional cash settings
    [MethodFieldParameterEnum.SameAsCashLoanOfferNumberOfCycles]?: string;
    [MethodFieldParameterEnum.AccrueInterestOnUnbilledInterest]?: string;
    //design loan offers - advanced payment delay settings
    [MethodFieldParameterEnum.DelayPaymentForStatementCycles]?: string;
    [MethodFieldParameterEnum.DelayPaymentForMonths]?: string;
    //design promotions basic
    [MethodFieldParameterEnum.DesignPromotionBasicInformation1]?: string;
    [MethodFieldParameterEnum.DesignPromotionBasicInformation2]?: string;
    [MethodFieldParameterEnum.DesignPromotionBasicInformation3]?: string;
    [MethodFieldParameterEnum.DesignPromotionBasicInformation4]?: string;
    [MethodFieldParameterEnum.DesignPromotionBasicInformation5]?: MagicKeyValue;
    [MethodFieldParameterEnum.DesignPromotionBasicInformation6]?: string;
    [MethodFieldParameterEnum.DesignPromotionBasicInformation7]?: string;
    [MethodFieldParameterEnum.DesignPromotionBasicInformation8]?: string;
    [MethodFieldParameterEnum.DesignPromotionBasicInformation9]?: string;
    [MethodFieldParameterEnum.DesignPromotionBasicInformation10]?: string;
    [MethodFieldParameterEnum.DesignPromotionBasicInformation11]?: string;
    [MethodFieldParameterEnum.DesignPromotionBasicInformation12]?: string;
    [MethodFieldParameterEnum.DesignPromotionBasicInformation13]?: string;
    //design promotions Interest Assessment and Fees
    [MethodFieldParameterEnum.DesignPromotionInterestAssessment1]?: string;
    [MethodFieldParameterEnum.DesignPromotionInterestAssessment2]?: string;
    [MethodFieldParameterEnum.DesignPromotionInterestAssessment3]?: string;
    [MethodFieldParameterEnum.DesignPromotionInterestAssessment4]?: string;
    [MethodFieldParameterEnum.DesignPromotionInterestAssessment5]?: string;
    [MethodFieldParameterEnum.DesignPromotionInterestAssessment6]?: string;
    [MethodFieldParameterEnum.DesignPromotionInterestAssessment7]?: string;
    [MethodFieldParameterEnum.DesignPromotionInterestAssessment8]?: string;
    [MethodFieldParameterEnum.DesignPromotionInterestAssessment9]?: string;
    [MethodFieldParameterEnum.DesignPromotionInterestAssessment10]?: string;
    [MethodFieldParameterEnum.DesignPromotionInterestAssessment11]?: string;
    [MethodFieldParameterEnum.DesignPromotionInterestAssessment12]?: string;
    [MethodFieldParameterEnum.DesignPromotionInterestAssessment13]?: string;
    [MethodFieldParameterEnum.DesignPromotionInterestAssessment14]?: string;
    [MethodFieldParameterEnum.DesignPromotionInterestAssessment15]?: string;
    [MethodFieldParameterEnum.DesignPromotionInterestAssessment16]?: string;
    [MethodFieldParameterEnum.DesignPromotionInterestAssessment17]?: string;
    [MethodFieldParameterEnum.DesignPromotionInterestAssessment18]?: string;
    //design promotions Interest Overrides
    [MethodFieldParameterEnum.DesignPromotionInterestOverrides1]?: string;
    [MethodFieldParameterEnum.DesignPromotionInterestOverrides2]?: string;
    [MethodFieldParameterEnum.DesignPromotionInterestOverrides3]?: string;
    [MethodFieldParameterEnum.DesignPromotionInterestOverrides4]?: string;
    [MethodFieldParameterEnum.DesignPromotionInterestOverrides5]?: string;
    [MethodFieldParameterEnum.DesignPromotionInterestOverrides6]?: string;
    [MethodFieldParameterEnum.DesignPromotionInterestOverrides7]?: string;
    [MethodFieldParameterEnum.DesignPromotionInterestOverrides8]?: string;
    //design promotions Retail Path
    [MethodFieldParameterEnum.DesignPromotionRetailPath1]?: string;
    [MethodFieldParameterEnum.DesignPromotionRetailPath2]?: string;
    [MethodFieldParameterEnum.DesignPromotionRetailPath3]?: string;
    [MethodFieldParameterEnum.DesignPromotionRetailPath4]?: string;
    //design promotions Return-to-Revolve
    [MethodFieldParameterEnum.DesignPromotionReturnRevolve1]?: string;
    [MethodFieldParameterEnum.DesignPromotionReturnRevolve2]?: string;
    [MethodFieldParameterEnum.DesignPromotionReturnRevolve3]?: string;
    [MethodFieldParameterEnum.DesignPromotionReturnRevolve4]?: string;
    [MethodFieldParameterEnum.DesignPromotionReturnRevolve5]?: string;
    [MethodFieldParameterEnum.DesignPromotionReturnRevolve6]?: string;
    //design promotions Statement Messaging
    [MethodFieldParameterEnum.DesignPromotionStatementMessaging1]?: string;
    [MethodFieldParameterEnum.DesignPromotionStatementMessaging2]?: string;
    [MethodFieldParameterEnum.DesignPromotionStatementMessaging3]?: string;
    [MethodFieldParameterEnum.DesignPromotionStatementMessaging4]?: string;
    [MethodFieldParameterEnum.DesignPromotionStatementMessaging5]?: string;
    //design promotion statement advanced parameter
    [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter1]?: string;
    [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter2]?: string;
    [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter3]?: string;
    [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter4]?: string;
    [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter5]?: string;
    [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter6]?: string;
    [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter7]?: string;
    [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter8]?: string;
    [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter9]?: string;
    [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter10]?: string;
    [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter11]?: string;
    [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter12]?: string;
    [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter13]?: string;
    [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter14]?: string;
    [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter15]?: string;
    [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter16]?: string;
    [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter17]?: string;
    [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter18]?: string;
    [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter19]?: string;
    [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter20]?: string;
    [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter21]?: string;
    [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter22]?: string;
    [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter23]?: string;
    [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter24]?: string;
    [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter25]?: string;
    [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter26]?: string;
    [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter27]?: string;
    [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter28]?: string;

    [MethodFieldParameterEnum.ChangeInTermsBasicSettingsCitPriorityOrder]?: string;
    [MethodFieldParameterEnum.ChangeInTermsBasicSettingsCisMemoSettings]?: string;
    [MethodFieldParameterEnum.ChangeInTermsBasicSettingsTermsConditionsTable]?: string;
    [MethodFieldParameterEnum.ChangeInTermsBasicSettingsCitPcs]?: string;
    [MethodFieldParameterEnum.ChangeInTermsBasicSettingsEnableCit]?: string;
    [MethodFieldParameterEnum.ChangeInTermsCommunicationLetterIdForActice]?: string;
    [MethodFieldParameterEnum.ChangeInTermsCommunicationLetterIdForInActice]?: string;

    [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcBp]?: string;
    [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcId]?: string;
    [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIi]?: string;
    [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIm]?: string;
    [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIp]?: string;
    [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIr]?: string;
    [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcMe]?: string;
    [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcMf]?: string;
    [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcVi]?: string;
    [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoAc]?: string;
    [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoCi]?: string;
    [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoMc]?: string;
    [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoMi]?: string;
    [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPfLc]?: string;
    [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPfOc]?: string;
    [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPfRc]?: string;
    [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPoMp]?: string;

    [MethodFieldParameterEnum.ChangeInTermsPenaltyPricingIndicator]?: string;

    [MethodFieldParameterEnum.ChangeInTermsProtectedBalancesMethodForCash]?: string;
    [MethodFieldParameterEnum.ChangeInTermsProtectedBalancesMethodForMerchandise]?: string;
    [MethodFieldParameterEnum.ChangeInTermsProtectedBalancesMethodForNonInterest]?: string;
    [MethodFieldParameterEnum.ChangeInTermsTimmingCitnumber]?: string;
    [MethodFieldParameterEnum.ChangeInTermsTimmingDelayPeriod]?: string;
    [MethodFieldParameterEnum.ChangeInTermsTimmingNumberOfMonthsOfDisclosureActiveAccounts]?: string;
    [MethodFieldParameterEnum.ChangeInTermsTimmingNumberOfMonthsOfDisclosureInActiveAccounts]?: string;
    //Advance
    [MethodFieldParameterEnum.ChangeInTermsReasonCode]?: string;
    [MethodFieldParameterEnum.ChangeInTermsProtectedBalanceDelayPeriod]?: string;
    [MethodFieldParameterEnum.ChangeInTermsDisclosedMethodOverridesCpIcIdInterestDefaults]?: string;
    [MethodFieldParameterEnum.ChangeInTermsDisclosedMethodOverridesCpIcIdInterestMethods]?: string;

    [MethodFieldParameterEnum.ProtectedBalancesAttributesCreditApplicationGroup]?: string;
    [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault1]?: string;
    [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault2]?: string;
    [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault3]?: string;
    [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod1]?: string;
    [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod2]?: string;
    [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod3]?: string;
    [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment1]?: string;
    [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment2]?: string;
    [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment3]?: string;

    [MethodFieldParameterEnum.ProtectedBalancesAttributesCreditApplicationGroup]?: string;
    [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault1]?: string;
    [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault2]?: string;
    [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault3]?: string;
    [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod1]?: string;
    [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod2]?: string;
    [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod3]?: string;
    [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment1]?: string;
    [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment2]?: string;
    [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment3]?: string;
    // ProtectedBalancesAttributes - Statement Display
    [MethodFieldParameterEnum.ProtectedBalancesAttributesDescription]?: string;
    [MethodFieldParameterEnum.ProtectedBalancesAttributesAlternateDescription]?: string;
    [MethodFieldParameterEnum.ProtectedBalancesAttributesDescriptionDisplayOption]?: string;
    [MethodFieldParameterEnum.ProtectedBalancesAttributesMessageText]?: string;
    [MethodFieldParameterEnum.ProtectedBalancesAttributesMessageOptions]?: string;
    [MethodFieldParameterEnum.ProtectedBalancesAttributesProtectBalancesForPenaltyPricing]?: string;
    [MethodFieldParameterEnum.ProtectedBalancesAttributesDelayIntroTextID]?: string;
    [MethodFieldParameterEnum.ProtectedBalancesAttributesPBBalanceTransfer]?: string;
    [MethodFieldParameterEnum.ProtectedBalancesAttributesProtectedBalanceOverrideCode]?: string;
    [MethodFieldParameterEnum.ProtectedBalancesAttributesGroupIdentifier]?: string;

    //Manage Income Option Methods
    //Mc
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsMcJoiningFeeAmount]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsMcJoiningFeeStatementDescription]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsMcJoiningFeeBatchId]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsMcCardReplacementFeeAmount]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsMcCardReplacementFeeBatchId]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsMcCardReplacementFeeStatementDescription]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsMcMiscellaneousFeeChargeId]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsMcFirstYearMaximumFeeManagement]?: string;
    //Ac - Statement and Notification Display
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualChargeStatementDisplayControl]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPreAnnualChargeNotifications]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcRegulationZNotificationMediaMethod]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcRegulationZnotificationGenerationPeriod]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPreNotificationMessageId]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine1]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine2Before]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine2After]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine3]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine4]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine5]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine6]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine7]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine8]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine9]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine10]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine11]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine12]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualChargeStatementDescription]?: string;
    //Ac - Assessment Calculations
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcBatchTypeCode]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcIncludeAnnualChargeInInterestCalculations]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPreventDelinquencyFromAnnualFeeBalance]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualChargeOption]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFirstYearMaximumFeeManagementOption]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPrenotificationAmountChangeOptions]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount1]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount2]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount3]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount4]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount5]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount6]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAdditionalPlasticCharge]?: string;
    //AC - Account Eligibility
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPlasticsRequirementCode]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcSpecialConditionsForExternalStatus]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcSpecialConditionsForExpiredAccounts]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualChargeIncludeTable]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFinanceChargeSuppressionCode]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcNoPrenotificationAssessmentOptions]?: string;
    //AC - Assessment Timing
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcExistingAccountStartMonthOption]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcNumberOfMonthsToFirstCharge]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPostingCycleOptions]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeTimingOptions]?: string;
    //AC - Advanced Parameters - Assessment Timing
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcSpecifiedMonthForAnnualCharge]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeWaiverMonths]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcSecondAnnualFeeNumberOfDays]?: string;
    //AC - Advanced Parameters - Assessment Calculations
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcDebitActivityOption]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcWaiverTotalDollarCalculationOption]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcRateChangePeriod]?: string;
    //AC - Advanced Parameters - Account Eligibility
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeWaiverOption]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcMinimumBalanceAndExternalStatusOption]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeWaiverAmount]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatusAMinimumBalance]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatusCMinimumBalance]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatusEMinimumBalance]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatusFMinimumBalance]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatusBlankMinimumBalance]?: string;
    //AC - Advanced Parameters -  Reversal Options
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcReversalStatementMessageWarningTextID]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcReversalNumberOfCycles]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcReversalReasonCode]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcReversalStatementMessageWarningOption]?: string;
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualFeeReversalBatchID]?: string;

    [MethodFieldParameterEnum.BulkExternalStatusManagementStatusCode]?: string;
    [MethodFieldParameterEnum.BulkExternalStatusManagementReasonCode]?: string;
    [MethodFieldParameterEnum.BulkExternalStatusManagementTransactionId]?: string;
    [MethodFieldParameterEnum.BulkExternalStatusManagementTransaction]?: string;
    [MethodFieldParameterEnum.BulkFraudTransaction]?: string;

    // Cycle Account
    [MethodFieldParameterEnum.CycleAccountsTestEnvironmentTransaction]?: string;
    [MethodFieldParameterEnum.CycleAccountsTestEnvironmentCode]?: string;

    // Manual Account Deletion
    [MethodFieldParameterEnum.ManualAccountDeletionTransaction]?: string;
    [MethodFieldParameterEnum.ManualAccountDeletionTransactionId]?: string;
    [MethodFieldParameterEnum.ManualAccountDeletionMN0TransactionId]?: string;
    [MethodFieldParameterEnum.ManualAccountDeletionMN0Transaction]?: string;
    [MethodFieldParameterEnum.ManualAccountDeletionExternalStatusCode]?: string;
    [MethodFieldParameterEnum.ManualAccountDeletionPropagate]?: string;

    //Action Card Compromise Event
    [MethodFieldParameterEnum.ActionCardCompromiseEventQuestion1]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventQuestion2SingleEntity]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventQuestion2SeparateEntity]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventQuestion3]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventQuestion4]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventTransaction]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventSingleEntityAccountTransferNoTransaction]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventSingleEntityAccountTransferYesTransaction]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventSingleEntityReportLostAccountTransaction]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventSingleEntityReportStolenAccountTransaction]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventSeparateEntityPIIDTransferNoTransaction]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventSeparateEntityPIIDTransferYesTransaction]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventSeparateEntityReportLostPIIDTransaction]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventSeparateEntityReportStolenPIIDTransaction]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventExternalStatus]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventDateOfEvent]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventPINLostIndicator]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventFraudLossOperator]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventPossibleFraudActivityCode]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventFraudTypeCode]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventTypeLossCode]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventLostLocationCode]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventAreaLost]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventMemoLine1]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventRushPlasticCode]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventFraudInvestigatorCode]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventFraudAreaCode]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventVisaWarningBulletinRegion]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventVisaResponseCode]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardStatus]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardWarningBulletinRegion]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardPurgeDate]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardINASReasonCode]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardAuthorizationLimitAmount]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventPiStatusCode]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventLostDate]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventStolenDate]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventLossOperatorId]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventProducePlasticIndicatorPLGEN]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventProducePlasticIndicatorPlastic]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventProducePlasticIndicatorSuppress]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventAccountTransferTypeCode]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventGenerateLetterIdentifier]?: string;
    [MethodFieldParameterEnum.ActionCardCompromiseEventRushMailCode]?: string;

    // Update Index Rate
    [MethodFieldParameterEnum.UpdateIndexRateCashIndexRate]?: string;
    [MethodFieldParameterEnum.UpdateIndexRateEffectiveDateNewCash]?: string;
    [MethodFieldParameterEnum.UpdateIndexRatePreviousCashIndexRate]?: string;
    [MethodFieldParameterEnum.UpdateIndexRateMerchandiseIndexRate]?: string;
    [MethodFieldParameterEnum.UpdateIndexRateEffectiveDateNewMerchandise]?: string;
    [MethodFieldParameterEnum.UpdateIndexRatePreviousMerchandiseIndexRate]?: string;
    [MethodFieldParameterEnum.UpdateIndexRateIndexType]?: string;
    [MethodFieldParameterEnum.UpdateIndexRateDeterminationDateDescription]?: string;
    [MethodFieldParameterEnum.UpdateIndexRateSourceDescription]?: string;
    [MethodFieldParameterEnum.UpdateIndexRateChangePeriod]?: string;

    // Manage Payoff Exception Methods
    [MethodFieldParameterEnum.PayoffExceptionCycleToDateCash]?: string;
    [MethodFieldParameterEnum.PayoffExceptionAmountToAvoidFinanceChargesSetting]?: string;
    [MethodFieldParameterEnum.PayoffExceptionCashOptionPromotionCode]?: string;
    [MethodFieldParameterEnum.PayoffExceptionCycleToDateCash]?: string;
    [MethodFieldParameterEnum.PayoffExceptionCheckExceptionsDaysAfterPaymentDue]?: string;
    [MethodFieldParameterEnum.PayoffExceptionCycleToDateMerchandise]?: string;
    [MethodFieldParameterEnum.PayoffExceptionCheckExceptionsDaysAfterPaymentDue]?: string;
    [MethodFieldParameterEnum.PayoffExceptionMinimumBalanceToAssessInterestOn]?: string;
    [MethodFieldParameterEnum.PayoffExceptionOldCash]?: string;
    [MethodFieldParameterEnum.PayoffExceptionPartialGraceSettings]?: string;
    [MethodFieldParameterEnum.PayoffExceptionTwoCycleOldMerchandise]?: string;
    [MethodFieldParameterEnum.PayoffExceptionStatementValue]?: string;
    [MethodFieldParameterEnum.PayoffExceptionOneCycleOldMerchandise]?: string;

    // Change Interest Rates
    [MethodFieldParameterEnum.DefaultCashBaseInterest]?: string;
    [MethodFieldParameterEnum.DefaultMerchandiseBaseInterest]?: string;
    [MethodFieldParameterEnum.DefaultMaximumCashInterest]?: string;
    [MethodFieldParameterEnum.DefaultMaximumMerchandiseInterest]?: string;
    [MethodFieldParameterEnum.DefaultMinimumCashInterest]?: string;
    [MethodFieldParameterEnum.DefaultMinimumMerchandiseInterest]?: string;
    [MethodFieldParameterEnum.IndexRateMethod]?: string;
    [MethodFieldParameterEnum.DefaultBaseInterestUsage]?: string;
    [MethodFieldParameterEnum.DefaultOverrideInterestLimit]?: string;
    [MethodFieldParameterEnum.DefaultMinimumUsage]?: string;
    [MethodFieldParameterEnum.DefaultMaximumUsage]?: string;
    [MethodFieldParameterEnum.DefaultMaximumInterestUsage]?: string;

    [MethodFieldParameterEnum.InterestRateRoundingCalculation]?: string;
    [MethodFieldParameterEnum.DailyAPRRoundingCalculation]?: string;
    [MethodFieldParameterEnum.StopCalculatingInterestAfterNumberofDaysDelinquent]?: string;
    [MethodFieldParameterEnum.IncludeAdditionalCreditBalanceInAverageDailyBalanceCalculation]?: string;
    [MethodFieldParameterEnum.CombineMerchandiseAndCashBalancesOnInterestCalculations]?: string;
    [MethodFieldParameterEnum.RestartInterestCalculationsAfterClearingDelinquency]?: string;
    [MethodFieldParameterEnum.CalculateCompoundInterest]?: string;
    [MethodFieldParameterEnum.OverridePromotionalRatesForPricingStrategies]?: string;
    [MethodFieldParameterEnum.MonthlyInterestCalculationOptions]?: string;
    [MethodFieldParameterEnum.DailyInterestRoundingOption]?: string;
    [MethodFieldParameterEnum.OverrideInterestRatesForProtectedBalance]?: string;
    [MethodFieldParameterEnum.DailyInterestAmountTruncationOption]?: string;

    [MethodFieldParameterEnum.IncentivePricingUsageCode]?: string;
    [MethodFieldParameterEnum.IncentiveBaseInterestUsageRate]?: string;
    [MethodFieldParameterEnum.DelinquencyCyclesForTermination]?: string;
    [MethodFieldParameterEnum.MinimumMaximumRateOverrideOptions]?: string;
    [MethodFieldParameterEnum.IncentivePricingOverrideOption]?: string;
    [MethodFieldParameterEnum.TerminateIncentivePricingOnPricingStrategyChange]?: string;
    [MethodFieldParameterEnum.MethodOverrideTerminationOption]?: string;
    [MethodFieldParameterEnum.IncentiveCashInterestRate]?: string;
    [MethodFieldParameterEnum.IncentiveMerchandiseInterestRate]?: string;
    [MethodFieldParameterEnum.EndofIncentiveWarningNotificationTiming]?: string;
    [MethodFieldParameterEnum.EndofIncentiveWarningNotificationStatementText]?: string;
    [MethodFieldParameterEnum.IncentivePricingNumberOfMonths]?: string;
    [MethodFieldParameterEnum.IndexRateMethodName]?: string;
    [MethodFieldParameterEnum.MinimumRateForCash]?: string;
    [MethodFieldParameterEnum.MaximumRateForCash]?: string;
    [MethodFieldParameterEnum.MinimumRateForMerchandise]?: string;
    [MethodFieldParameterEnum.MaximumRateForMerchandise]?: string;
    [MethodFieldParameterEnum.PreventTermsChangeDuringIncentivePricingPeriod]?: string;
    [MethodFieldParameterEnum.MessageForProtectedBalanceIncentivePricing]?: string;
    [MethodFieldParameterEnum.MinimumDaysForCycleCodeChange]?: string;
    [MethodFieldParameterEnum.RoundNumberOfDaysInCycleForInterestCalculations]?: string;
    [MethodFieldParameterEnum.CycleCodeChangeOptions]?: string;
    [MethodFieldParameterEnum.CyclePeriodChangeCISMemoOption]?: string;
    [MethodFieldParameterEnum.InterestMethodOldCash]?: string;
    [MethodFieldParameterEnum.InterestMethodCycleToDateCash]?: string;
    [MethodFieldParameterEnum.InterestMethodTwoCycleOldCash]?: string;
    [MethodFieldParameterEnum.InterestMethodOneCycleOldMerchandise]?: string;
    [MethodFieldParameterEnum.InterestMethodCycleToDateMerchandise]?: string;
    [MethodFieldParameterEnum.AdditionalInterestOnMerchandise1CyclesDelinquent]?: string;
    [MethodFieldParameterEnum.AdditionalInterestOnMerchandise2CyclesDelinquent]?: string;
    [MethodFieldParameterEnum.AdditionalInterestOnCash2CyclesDelinquent]?: string;
    [MethodFieldParameterEnum.AdditionalInterestOnMerchandise3CyclesDelinquent]?: string;
    [MethodFieldParameterEnum.AdditionalInterestOnCash3CyclesDelinquent]?: string;
    [MethodFieldParameterEnum.AutomatedMemosForAdditionalInterest]?: string;
    [MethodFieldParameterEnum.UseExternalStatusesForAdditionalInterest]?: string;
    [MethodFieldParameterEnum.ExternalStatus1ForAdditionalInterest]?: string;
    [MethodFieldParameterEnum.ExternalStatus2ForAdditionalInterest]?: string;
    [MethodFieldParameterEnum.ExternalStatus3ForAdditionalInterest]?: string;
    [MethodFieldParameterEnum.ExternalStatus4ForAdditionalInterest]?: string;
    [MethodFieldParameterEnum.ExternalStatus5ForAdditionalInterest]?: string;
    [MethodFieldParameterEnum.PenaltyPricingStatusReasonCodeTable]?: string;
    [MethodFieldParameterEnum.PenaltyPricingTimingOptions]?: string;
    [MethodFieldParameterEnum.PenaltyPricingStatementMessage1CyclesOldBalance]?: string;
    [MethodFieldParameterEnum.PenaltyPricingStatementMessage2CyclesOldBalance]?: string;
    [MethodFieldParameterEnum.PenaltyPricingStatementMessage3CyclesOldBalance]?: string;
    [MethodFieldParameterEnum.CureDelinquencyFor1CycleDelinquentBalances]?: string;
    [MethodFieldParameterEnum.CureDelinquencyFor2CycleDelinquentBalances]?: string;
    [MethodFieldParameterEnum.CureDelinquencyFor3CycleDelinquentBalances]?: string;
    [MethodFieldParameterEnum.PenaltyPricingOnPromotions]?: string;
    [MethodFieldParameterEnum.CITMethodForPenaltyPricing]?: string;
    [MethodFieldParameterEnum.IncludeZeroBalanceDaysInADBCalculation]?: string;

    // Manage Statement Production Settings
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditBalanceHoldOptions]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsWhereToSendRequestPayments]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPaymentDueDays]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsSameDayLostStatusOption]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsUseFixedMinimumPaymentDueDate]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsUseFixedMinimumPaymentDueDateTableID]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsAnnualInterestDisplay]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeLateChargesInAnnualInterestDisplay]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeCashItemInAnnualInterestDisplay]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeMerchandiseItemInAnnualInterestDisplay]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeOverLimitFeesInAnnualInterestDisplay]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeStatementProductionFeesInAnnualInterestDisplay]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsProduceStatementsForInactiveDeletedAccounts]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsUSInactiveAccountExternalStatusControl]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsNonUSInactiveAccountExternalStatusControl]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPricingStrategyChangeStatementProduction]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsMinimumAmountForInactiveDeletedAccounts]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsStatusAAuthorizationProhibited]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsStatusBBankrupt]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsStatusCClosed]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsStatusERevoked]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsStatusFFrozen]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsStatusIInterestProhibited]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsStatusLLost]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsStatusUStolen]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsStatusBlank]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance1]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance2]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance3]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance4]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance5]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance6]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance7]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance8]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance9]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCashAdvanceFinanceCharges]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCashItemCharges]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsMerchandiseFinanceCharges]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsMerchandiseItemCharges]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsLateCharge]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsOverlimitCharge]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsuranceRefunds]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsTurnOffPaper]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsTurnOnPaper]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsReturnToEDelivery]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsDeliquentAccountUnEnrollment]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsEbillLanguageOptions]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsFinanceChargeStatementDisplayOptions]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsDisplayAirlineTransactionDetailsOnStatement]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCardholderNamesOnStatement]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsReversalDisplayOption]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsLateChargeDateDisplay]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsOverlimitChargeDateDisplay]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsForeignCurrencyDisplayOptions]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsFeeRecordIndicatorOnCISScreens]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsOptionalIssuerFeeStatementDisplayCode]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsOptionalIssuerFeeMessageText]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICBP]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICID]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICII]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIM]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIP]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIR]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICMF]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICVI]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOAC]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOCI]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOMC]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOMI]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFLC]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFOC]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFRC]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPPOMP]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICME]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsSaleAmountAdjustmentText]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCashAdvanceAmountAdjustmentText]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsReturnAmountAdjustmentText]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPaymentAmountAdjustmentText]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffPrincipalAdjustmentTextLine1]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffFinanceChargesAdjustmentTextLine2]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsMiscellaneousSpecificCreditAdjustmentText]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffSmallBalanceAdjustmentText]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffCreditLifeInsuranceAdjustmentText]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffCashInterestRefundText]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffLateChargeRefundText]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffMerchandiseInterestRefundText]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffCashItemRefundText]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffMerchandiseItemRefundText]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffOverlimitFeeRefundText]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsSaleReversalText]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCashAdvanceReversalText]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsReturnReversalText]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPaymentReversalText]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditBalanceHoldCode]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPrintHardCopySecurityStatements]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsSCSStatementOverrideTableID]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsLateFeeWaiverText]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsGenericMessageAboveRevolvingBalance]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsGenericMessageBelowRevolvingBalance]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsNormalTermsDateID]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCrossCycleFeeDisplayOptions]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCrossCycleInterestDisplayOptions]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettings280TransactionsSeriesDisplayOptions]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycCashAdvance]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycCashItemCharge]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycMerchandise]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycMdseItemCharge]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsMinimumFinanceCharges]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsActivityFee]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycLateCharge]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycOverlimitCharge]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsuranceCharge]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsOtherDebitAdjustmentText]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsOtherCreditAdjustmentText]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCashAdvanceFinanceChargesText]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsMerchandiseItemChargeText]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCashItemChargesText]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsMerchandiseFinanceChargesText]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsLateChargesText]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsOverlimitChargesText]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsMinimumFinanceChargesText]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsActivityFeesText]?: string;
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsuranceText]?: string;

    // Create Portfolio Definition
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeFilterBy]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeOperator]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeSysPrinAgents]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeBin]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributePortfolioId]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeStateCode]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeSynsO]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributePrinO]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeAgentO]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeBinO]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeCompanyIdO]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeCompanyNameO]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeDatalinkElO]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributePortfolioIdO]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributePricingStrateryO]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeProductTypeCodeO]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeStateCodeO]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneousf1t10O]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneousf11t15O]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneousf16t20O]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneousf21t25O]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous26O]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous27]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous28]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous29]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous30]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous31]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous32]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous33]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous34]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous35]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous36]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous37]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous38]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous39]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous40]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous41]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous42]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous43]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous44]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous45]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous46]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous47]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous48]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous49]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous50]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributes]?: string;
    [MethodFieldParameterEnum.PortfolioDefinitionsIcon]?: string;
    //Manage Account Level Processing
    [MethodFieldParameterEnum.AllocationDate]?: string;
    [MethodFieldParameterEnum.DecisionElement]?: string;
    [MethodFieldParameterEnum.SearchCode]?: string;
    [MethodFieldParameterEnum.ImmediateAllocation]?: string;
    [MethodFieldParameterEnum.AllocationBeforeAfterCycle]?: string;
    [MethodFieldParameterEnum.AllocationFlag]?: string;
    [MethodFieldParameterEnum.AllocationInterval]?: string;
    [MethodFieldParameterEnum.DefaultStrategy]?: string;
    [MethodFieldParameterEnum.ChangeInTermsMethodFlag]?: string;

    [MethodFieldParameterEnum.MemosTypes]?: string;
    [MethodFieldParameterEnum.MemosActions]?: string;
    [MethodFieldParameterEnum.MemosTypesFormCIS]?: string;
    [MethodFieldParameterEnum.MemosTypesFormChronicle]?: string;
    [MethodFieldParameterEnum.MemosOperator]?: string;
    [MethodFieldParameterEnum.MemosEnteredIdOperator]?: string;
    [MethodFieldParameterEnum.MemosSysOperator]?: string;
    [MethodFieldParameterEnum.MemosPrinOperator]?: string;
    [MethodFieldParameterEnum.MemosAgentOperator]?: string;
    [MethodFieldParameterEnum.MemosDateOperator]?: string;
    [MethodFieldParameterEnum.MemosMemoTextOperator]?: string;
    [MethodFieldParameterEnum.MemosTypeOperator]?: string;
    [MethodFieldParameterEnum.MemosTypeOperatorValue]?: string;
    [MethodFieldParameterEnum.MemosSPAs]?: string;

    // Manage transaction TLP
    [MethodFieldParameterEnum.IncludeActivityAction]?: string;
    [MethodFieldParameterEnum.NextDebitDate]?: string;
    [MethodFieldParameterEnum.PosPromotionValidation]?: string;
  }

  interface CreateChangeSet {
    workflowTemplateId: string;
    changeSetDetails: {
      name: string;
      description: string;
      effectiveDate: string;
    };
  }

  interface CreateWorkflowInstance {
    workflowInstanceDetails: {
      note?: string;
    };
    workflowTemplateId?: string;
    changeSetId?: string;
  }

  interface UpdateflowInstanceDetail {
    workflowInstanceId?: string;
    changeSetId?: string;
    workflowInstanceDetails?: {
      note?: string;
    };
    changeSetDetail?: {
      name: string;
      description: string;
      effectiveDate: string;
    };
  }
  interface UpdateflowInstanceData {
    workflowInstanceId?: string;
    status?: string;
    data?: {
      workflowSetupData?: WorkflowSetupInstanceStep[];
      configurations?: any;
    };
  }

  interface IParametersLetter {
    id?: string;
    name?: string;
    type?: string;
    description?: string;
  }
  interface ILetter {
    id?: string;
    name?: string;
    description?: string;
    letterCode?: string;
    optionalNameAddressPermitted?: string;
    parameters?: IParametersLetter[];
  }
  interface ISupervisor {
    name?: string;
    level?: string;
  }
  interface IOCSShell {
    id?: string;
    shellName?: string;
    transactionProfile?: string;
    fieldProfile?: string;
    sysPrinProfile?: string;
    description?: string;
    accountProfile?: string;
    operatorCode?: string;
    singleSession?: string;
    employeeId?: string;
    supervisorShell?: string;
    reportManagementSystemId?: string;
    departmentId?: string;
    level?: string;
    masterSignOnOption?: string;
    pwdExpirationOption?: string;
    expireDate?: string;
    neverDisable?: string;
    comment?: string;
    initialPwd?: string;
    confirmPwd?: string;
    shellStatus?: string;
    status?: string;
    daysAllowed?: string[];
    timesAllowed?: string[];

    modelFromName?: string;
  }

  interface ElementMetaData {
    id: string;
    name: string;
    options?: {
      value: string;
      description;
      name;
      workFlow;
      type?: string;
      selected?: boolean;
    }[];
  }
  interface WorkflowSetupConfigureParameters {
    [SendLetterParameterEnum.LetterNumber]?: string;
    [SendLetterParameterEnum.Addressing]?: string;
  }

  interface ElementMetaDataAssignPricingStrategies {
    id: string;
    name: string;
    options?: (AssignPricingStrategiesMetaDataOption & {
      name: string;
      value: string;
      description;
    })[];
  }

  interface AssignPricingStrategiesMetaDataOption {
    value?: string;
    name?: string;
    description?: string;
    workFlow?: string;
  }

  interface ManageAccountDelinquencyMetaDataOption {
    value?: string;
    description?: string;
    workFlow?: string;
    selected?: boolean;
  }

  interface WorkflowSetupThunkArg {
    workflowSetupData?: WorkflowSetupInstanceStep[];
  }

  interface WorkflowElement {
    name: string;
    options: RefData[];
  }

  interface MerchantItem {
    merchantAccountNumber: string;
    merchantName: string;
    phoneNumber: string;
    address: string;
    city: string;
    stateProvinceTerritory: string;
    zipPostalCode: string;
    doingBusinessAs: string;
    IRSName: string;
    imprinterNumber: string;
    assessmentCode: string;
    informationIndicator: string;
    merchantCustomerServicePhone: string;
    aggregatorType: string;
    TINType: string;
    taxpayerId: string;
    sparkExclusionIndicator: string;
    transactionDecisionTableId: string;
    promotionDiscountRateTableId: string;
    inStorePayment: string;
    merchantPaymentDescription: string;
    businessCategoryCode: string;
    processFlag: string;
    discountMethodParticipation: string;

    rowId: string;
  }

  interface MerchantItemCreated extends MerchantItem {
    sysPrin?: string;
    modeledFrom?: MerchantItem;
  }
}

export {};
