import { PendingApprovalByGroup } from 'pages/_commons/redux/Change/types';

export interface ChangeState {
  lastUpdatedChanges: LastUpdatedChangesState;
  lastUpdatedApprovalQueues: LastUpdatedApprovalQueuesState;
  changeDetails: ChangeDetailState;
  changeDetail: DetailState;
  changeOwners: ChangeOwnerState;
  changeDateInfo: ChangeDateInfoState;
  changeUpdateStatus: ChangeUpdateStatusState;
  changeDataExport: ChangeDataExportState;
  approvalQueues: ApprovalQueuesState;
  changeApproveStatus: ChangeApproveStatusState;
  changeDemoteStatus: ChangeApproveStatusState;
  changeRejectStatus: ChangeRejectStatusState;
  changeAttachmentList: ChangeAttachmentListState;
  changeValidateStatus: ChangeApproveStatusState;
  changeSubmitStatus: ChangeSubmitStatusState;
  workflowDetailId: string;
  pendingApprovalByGroup: PendingApprovalByGroup;
  showChangeNotFoundModal?: boolean;
}
export interface IPendingApprovalsFilter extends IDataFilter {
  changeType: string;
}
export interface ApprovalQueuesState {
  data: IChangeDetail[];
  changeTypeList: string[];
  loading: boolean;
  error?: boolean;
  dataFilter?: IPendingApprovalsFilter;
  expandingApproval?: string;
}
export interface IInvalidDate {
  date: string;
  reason: string;
}
export interface ChangeDateInfoState {
  data: {
    minDate?: string;
    maxDate?: string;
    invalidDate?: IInvalidDate[];
  };
  loading: boolean;
  error?: boolean;
}
export interface ChangeUpdateStatusState {
  loading: boolean;
  error?: boolean;
}
export interface ChangeApproveStatusState extends IFetching {}
export interface ChangeDemoteStatusState extends IFetching {}
export interface ChangeRejectStatusState extends IFetching {}
export interface ChangeSubmitStatusState extends IFetching {}
export interface ChangeOwnerState {
  data: RefData[];
  loading: boolean;
  error?: boolean;
}
export interface IChangeManagementFilter extends IDataFilter {
  changeStatus?: string;
  changeType?: string;
}
export interface LastUpdatedChangesState {
  changes: IChangeDetail[];
  changeTypeList: string[];
  changeStatusList: string[];
  loading: boolean;
  error?: boolean;
  dataFilter?: IChangeManagementFilter;
  expandingChange: string;
}
export interface LastUpdatedApprovalQueuesState {
  approvalQueues: IChangeDetail[];
  changeTypeList: string[];
  loading: boolean;
  error?: boolean;
}
export interface ChangeDetailState {
  data: IChangeDetail;
  loading: boolean;
  error?: boolean;
}
export interface ChangeSnapshotState extends IFetching {
  data: IChangeDetail;
}
export interface ChangePortfoliosState extends IFetching {
  data: IPortfolioModel[];
  filter: IDataFilter;
}
export interface ChangePricingStrategiesState extends IFetching {
  data: IPricingStrategyModel[];
  filter: IDataFilter;
}
export interface ChangeAttachmentList {
  attachments: IAttachment[];
  reports: IReport[];
}
export interface ChangeAttachmentListState extends IFetching {
  data: ChangeAttachmentList;
}
export interface ChangeDetailsState {
  snapshot: ChangeSnapshotState;
  pricingStrategies: ChangePricingStrategiesState;
}
export interface DetailState {
  [changeId: string]: ChangeDetailsState;
}
export interface ChangeDetailActionPayload {
  changeId: string;
  filter?: IDataFilter;
}
export interface ChangeDataExportState {
  loading: boolean;
  error?: boolean;
}
export interface ChangeDataExport {
  snapshot?: IChangeDetail;
  activityList?: IActivity[];
  portfolioList?: IPortfolioModel[];
  workflowList?: IWorkflowModel[];
  pricingStrategyList?: IPricingStrategyModel[];
  pricingMethodList?: IPricingMethod[];
  attachmentList?: ChangeAttachmentList;
}
