export interface PricingMethodsState {
  changePricingMethods: ChangePricingMethods;
}

export interface ChangePricingMethods {
  pricingMethods: IPricingMethod[];
  loading: boolean;
  error?: boolean;
  dataFilter?: IDataFilter;
}

export interface PricingStrategiesState {
  pricingStrategyData: PricingStrategyData;
}

export interface PricingStrategyData extends IFetching {
  pricingStrategies: IPricingStrategyModel[];
  dataFilter: IDataFpricingStrategiesilter;
}
