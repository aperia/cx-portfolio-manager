export interface CommonState {
  window: WindowDimension;
}
export interface WindowDimension {
  width: number;
  height: number;
}

export interface RefDataState {
  error: boolean;
  loading: boolean;
  data: Record<string, any>[];
}

export interface ModalsState {
  registerModals: ModalsRegistryState[];
}
export interface ModalsRegistryState {
  name: string;
  isModalFull?: boolean;
}

export interface MappingState {
  loading: boolean;
  data: MappingStateData;
  workflowData: Record<string, string>;
}
export interface MappingStateData {
  downloadTemplateFile?: Record<string, string>;
  pricingMethods?: Record<string, string>;
  portfolioList?: Record<string, any>;
  changeManagementList?: Record<string, any>;
  changeDetailSnapshot?: Record<string, any>;
  changeWorkflow?: Record<string, any>;
  workflowTemplateList?: Record<string, any>;
  changeWorkflowDetail?: Record<string, any>;
  pricingStrategies?: Record<string, string>;
  activityList?: Record<string, any>;
  i18next?: Record<string, any>;
  changeOwner?: Record<string, any>;
  changeDateInfo?: Record<string, any>;
  attachmentList?: Record<string, any>;
  reports?: Record<string, any>;
  notification?: Record<string, any>;
  dmmList?: Record<string, any>;
  downloadAttachment?: Record<string, any>;
  pendingApprovals?: Record<string, any>;
  workflowMethodList?: Record<string, any>;
  workflowStrategyList?: Record<string, any>;
  workflowChangeSetList?: Record<string, any>;
  workflowStrategy?: Record<string, any>;
  workflowParameter?: Record<string, any>;
  serviceSubjectSection?: Record<string, any>;
  getLetters?: Record<string, any>;
  getKPIListForSetup?: Record<string, any>;
  getKPIDetails?: Record<string, any>;
  ocsShell?: Record<string, any>;
  workflowMerchant?: Record<string, any>;
  queueProfile?: Record<string, any>;
  ocsShellProfile?: Record<string, any>;
  profileTransactions?: Record<string, any>;
  supervisorList?: Record<string, any>;
  portfolioDetail?: Record<string, any>;
}

type TResource = Record<string, string>;
export interface ILang {
  lang: string;
}
export interface Resource {
  resource: TResource;
}
export interface IResource extends ILang {
  data: TResource;
}
export interface II18nextResponse extends ILang, Resource {}
export interface II18nextReturn extends ILang, Resource {}
export interface II18nextArg extends Partial<ILang> {}
export interface II18nextReducer extends ILang {
  resources: Record<string, TResource>;
  isLoading: boolean;
}

export interface ToastType {
  id?: string;
  show?: boolean;
  type?: 'attention' | 'success' | 'warning' | 'error';
  message?: string;
}
export interface ToastState {
  notifications: ToastType[];
}

export interface NotificationState extends IFetching {
  notificationList: NotificationList;
  updateNotification: UpdateNotification;
}

export interface NotificationList extends IFetching {
  notifications: INotification[];
  notificationsInApp: INotification[];
  filter: IDataFilter;
  loadingApi?: boolean;
}
export interface UpdateNotification extends IFetching {
  total: number;
}

export interface AuthState {
  currentUser: any;
}
