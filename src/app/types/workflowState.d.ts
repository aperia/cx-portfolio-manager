import { WorkflowSection } from 'app/constants/enums';
import { ReminderSet } from 'pages/_commons/redux/Workflow';

export interface WorkflowState {
  templateWorkflows: TemplateWorkflowState;
  changeWorkflows: ChangeWorkflows;
  changeWorkflowDetail: ChangeWorkflowDetail;
  workflowRemoval: WorkflowRemoval;
  favoriteWorkflowTemplate: FavoriteWorkflow;
  unfavoriteWorkflowTemplate: UnfavoriteWorkflow;
  reminderSet: ReminderSet;
  workflows: WorkflowsListState;
}

export interface WorkflowsListState extends IFetching {
  workflows: IWorkflowModel[];
  dataFilter?: IDataFilter;
}

export interface FavoriteWorkflow extends IFetching {}
export interface UnfavoriteWorkflow extends IFetching {}

export interface WorkflowRemoval {
  loading: boolean;
  error: boolean;
}

export interface ChangeWorkflowDetail {
  data: IWorkflowDetail;
  loading: boolean;
  error: boolean;
}

export interface ChangeWorkflows {
  workflows: IWorkflowModel[];
  deletedWorkflowIds?: string[];
  loading: boolean;
  error: boolean;
  dataFilter?: IDataFilter;
}

export interface TemplateWorkflows {
  workflows: IWorkflowTemplateModel[];
}

export interface IWorkflowPageFilter extends IDataFilter {
  myWorkflowFilterValue?: any;
  favoriteWorkflowFilterValue?: any;
  currentSection?: WorkflowSection;
  searchValues?: Record<string, string>;
  sortBys?: Record<string, string[]>;
  orderBys?: Record<string, OrderByType[]>;
}
export interface TemplateWorkflowState extends TemplateWorkflows {
  alreadyLoad?: boolean;
  loading: boolean;
  error?: boolean;
  dataFilter?: IDataFilter;
  pageFilter?: IWorkflowPageFilter;
  pageFetching?: IFetching;
}
