export interface PortfolioState {
  portfoliosData: LastUpdatedPortfoliosState;
  portfoliosDetails: PortfoliosDetailsState;
  getKPIListForSetup: getKPIListForSetupState;
  getKPIDetails: Record<any, any>;
  addPortfolioKPI: AddPortfolioKPIState;
  chooseKPI: chooseKPI;
}

export interface AddPortfolioKPIState {
  data: any;
}

export interface chooseKPI {
  openedChoosePopup: boolean;
  openedAddPopup: boolean;
  dataChooseKPI?: IKPIListForSetup;
}

export interface GetKPIDetailsState {
  data: IKPIDetails;
  loading: boolean;
  error?: boolean;
}

export interface getKPIListForSetupState {
  data: IKPIListForSetup[];
  loading: boolean;
  error?: boolean;
}

export interface PortfoliosDetailsState {
  data?: IPortfoliosDetail;
  loading: boolean;
  error?: boolean;
}

export interface LastUpdatedPortfoliosState {
  portfolios: IPortfolioModel[];
  loading?: boolean;
  error?: boolean;
  dataFilter?: IDataFilter;
  portfolioCriteriaFields?: string[];
}
