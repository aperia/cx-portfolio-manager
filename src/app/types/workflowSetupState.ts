import { WorkflowStrategyFlyoutProps } from 'app/components/WorkflowStrategyFlyout';
import { STATUS } from 'pages/WorkflowManageOCSShells/ConfigureParameters/constants';
import { IChangeManagementFilter } from 'pages/_commons/redux/Change';

export interface WorkflowSetupRootState {
  workflowSetup: WorkflowSetupState;
  workflowFormSteps: WorkflowFormStepsState;
  workflowMethods: WorkflowMethodsState;
  workflowStrategies: WorkflowStrategiesState;
  workflowTextAreas: WorkflowTextAreasState;
  workflowTableAreas: WorkflowTableAreasState;
  workflowInstance: WorkflowSetupInstanceState;
  createWorkflow: IFetching;
  createNewChange: IFetching;
  updateWorkflow: IFetching;
  elementMetadata: ElementMetaDataState;
  elementMetadataNonWorkflow: ElementMetaDataState;
  workflowChangeSets: ChangeSetListState;
  strategyData: StrategyDataState;
  parameterData: ParameterDataState;
  lettersData: LettersDataState;
  ocsShellData: OCSShellDataState;
  merchantData: MerchantDataState;
  exportMerchant: IFetching;
  queueProfile: IFetching;
  ocsShellProfile: IFetching;
  exportShellProfile: IFetching;
  profileTransactions: IFetching;
  shellList: IFetching;
  tableList: IFetching;
  decisionList: IFetching;
  decisionElementList: Record<any, decisionElementListState>;
  supervisorData: SupervisorDataState;
  decisionLevelData: MagicKeyValue[];
  strategiesListAQ: IFetching;
  services: IServices;
  tableListWorkflowSetup: ITableListWorkflowSetup;
  exportAnalyzer: IFetching;
}

export interface ITableListWorkflowSetup extends IFetching {
  data: ITableListItem[];
  total?: number;
  ProcessorInfo?: IProcessorInfo;
}

export interface IProcessorInfo {
  system: string;
  Cycle: string;
  Tor: string;
  Mode: string;
}

export interface IServices extends IFetching {
  data: IServicesData[];
}

export interface ITableListItem {
  tableId: string;
  tableName: string;
  versions: [];
}

export interface IServicesData {
  name: string;
  description: string;
  subjects: ISubjects[];
}

export interface ISubjects {
  name: string;
  description: string;
  sections: ISections[];
}

export interface ISections {
  name: string;
  description: string;
}

export interface WorkflowSetupInstanceState extends IFetching {
  instance?: WorkflowSetupInstance;
}

export interface WorkflowMethodsState extends IFetching {
  methods: IMethodModel[];
  dataFilter?: IDataFilter;
  expandingMethod?: string;
  strategyFlyoutData?: Omit<WorkflowStrategyFlyoutProps, 'onClose'> | null;
}

export interface WorkflowStrategiesState extends IFetching {
  strategies: IStrategyModel[];
  dataFilter?: IDataFilter;
  expandingStrategy?: string;
  strategyFlyoutData?: Omit<WorkflowStrategyFlyoutProps, 'onClose'> | null;
}

export interface WorkflowTextAreasState extends IFetching {
  textAreas: ITextAreaModel[];
  dataFilter?: IDataFilter;
  expandingTextArea?: string;
  textAreaFlyoutData?: Omit<WorkflowStrategyFlyoutProps, 'onClose'> | null;
}

export interface WorkflowTableAreasState extends IFetching {
  tableAreas: ITableAreaModel[];
  dataFilter?: IDataFilter;
  expandingTableArea?: string;
  tableAreaFlyoutData?: Omit<WorkflowStrategyFlyoutProps, 'onClose'> | null;
}

export interface WorkflowFormStepsState {
  workflowInfo: {
    changeSetId?: string;
    workflowTemplateId?: string;
    workflowInstanceId?: string;
    isCompletedWorkflow?: boolean;
  };
  selectedStep: string;
  lastStep: string;
  forms: Record<string, WorkflowSetupStepForm>;
  stepTracking: Record<string, WorkflowSetupStepTracking>;
  savedAt?: number;
  isStuck?: Record<string, boolean>;
  lastSavedInvalidAt?: number;
  haveBeenSaved?: boolean;
}

export interface ElementMetaDataState extends IFetching {
  elements: ElementMetaData[];
}

export interface WorkflowSetupStepTracking {
  inprogress?: boolean;
  error?: boolean;
  doneValidation?: number;
}

export interface WorkflowSetupStepForm extends Record<string, any> {
  isValid?: boolean;
}

export interface WorkflowSetupState extends IFetching {
  isTemplate?: boolean;
  workflow?: IWorkflowModel | IWorkflowTemplateModel;
  onSubmitted?: (params: { isDeleted?: boolean }) => boolean;
}
export interface ChangeSetListState extends IFetching {
  newChangeId?: string;
  dataFilter?: IChangeManagementFilter;
  changeStatusList?: string[];
  changeSetWithTemplate?: string[];
  changeSetList: IWorkflowChangeDetail[];
}
export interface StrategyDataState extends IFetching {
  strategyList: Strategy[];
  methods: Record<string, string[]>;
}

export interface ParameterDataState extends IFetching {
  parameterList: IParameterModel[];
}

export interface LettersDataState extends IFetching {
  letters: ILetter[];
}

export interface OCSShellDataState extends IFetching {
  ocsShells: IOCSShell[];
}

export interface MerchantDataState extends IFetching {
  merchants: MerchantItem[];
}

export interface ProfileRefData extends RefData {
  subSystemAdj: string;
}

export interface SupervisorDataState extends IFetching {
  supervisorList: ISupervisor[];
}

export interface decisionElementListState extends IFetching {
  decisionElementList?: any[];
}

export interface IShell {
  shellName?: string;
  transactionProfile: string;
  description: string;
  operatorCode: string;
  employeeId: string;
  supervisorShell?: string;
  departmentId?: string;
  level?: string;
  status: STATUS;
  rowId?: string;
  original?: IShell;
}

export interface QueueProfile {
  generalInformation: {
    queueOwner: string;
    queueOwnerId: string;
    managerId: string;
    terminalGroupId: string;
  };
  alternateAccessShellNames: { shellName: string }[];
}

export interface OcsShellProfile {
  id: string;
  shellName: string;
  generalInformation: {
    terminalGroupId: string;
    profile: string;
    departmentId: string;
    managerOperatorId: string;
    supervisorOperatorId: string;
    operatorId: string;
  };
  adjustmentLimitInformation: {
    batchCode: string;
    adjustmentTypeId: string;
    shellMaximumAmount: number;
    managerDepartmentMaximumAmount: number;
    actionCode: string;
  }[];
}

export interface ProfileTransactions {
  batchCode: string;
  transactionCode: string;
  actionCode: string;
  maximumAmount: string;

  status: WorkflowSetupRecordType;
  rowId?: string;

  originalProfile?: ProfileTransactions;
}
export interface ProfileDetail extends ProfileRefData {
  transactions: ProfileTransactions[];
}

export interface MethodTransactions {
  status: WorkflowSetupRecordType;
  rowId?: string;
  id: string;
  effectiveDate: string;
  'method.name': string;
  'effective.date': string;
  name: string;
  selected?: boolean;
}

export interface MethodDetail extends RefData {
  methodList: MethodTransactions[];
  code: string;
  value: string;
}

export interface StrategyTransactions {
  status: WorkflowSetupRecordType;
  rowId?: string;
  id: string;
  'strategy.name': string;
  'effective.date': string;
  effectiveDate: string;
  originalStrategy?: StrategyTransactions;
  strategyName: string;
  selected?: boolean;
}

export interface StrategyDetail extends RefData {
  methodList: StrategyTransactions[];
}

export interface TextAreaTransactions {
  status: WorkflowSetupRecordType;
  rowId?: string;
  id: string;
  effectiveDate: string;
  textId: string;
  textType: string;
  languageCode: string;
  selected?: boolean;
  'text.id': string;
  'text.type': string;
  'language.code': string;
  'effective.date': string;
}

export interface TextAreaDetail extends RefData {
  textIdList: TextAreaTransactions[];
}

export interface TableAreaTransactions {
  status: WorkflowSetupRecordType;
  rowId?: string;
  id: string;
  effectiveDate: string;
  tableId: string;
  selected?: boolean;
  'table.id': string;
  'effective.date': string;
}

export interface TableAreaDetail extends RefData {
  methodList: TableAreaTransactions[];
}
