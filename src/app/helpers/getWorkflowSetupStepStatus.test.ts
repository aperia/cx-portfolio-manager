import { getWorkflowSetupStepStatus } from 'app/helpers/getWorkflowSetupStepStatus';

describe('test Helpers > getWorkflowSetupStepStatus', () => {
  it('test getWorkflowSetupStepStatus function', () => {
    let result = getWorkflowSetupStepStatus({} as WorkflowSetupInstanceStep);
    expect(result).toEqual({
      isPass: false,
      isSelected: false
    });

    result = getWorkflowSetupStepStatus({
      status: 'INPROGRESS'
    } as WorkflowSetupInstanceStep);
    expect(result).toEqual({
      isPass: true,
      isSelected: false
    });

    result = getWorkflowSetupStepStatus({
      status: 'DONE',
      selected: true
    } as WorkflowSetupInstanceStep);
    expect(result).toEqual({
      isPass: true,
      isSelected: true
    });
  });
});
