import get from 'lodash.get';
import isArray from 'lodash.isarray';
import isEmpty from 'lodash.isempty';
import set from 'lodash.set';

/**
 * - ...
 * @param {object} val
 * @param {object} json
 * @returns {object}
 */
export const mappingDataFromObj = (
  val: any,
  json: object,
  selectedFields?: string[]
) => {
  const result: any = {};

  const dynamicFields = {} as any;
  if (isEmpty(val) || isEmpty(json)) return result;
  Object.entries(json).forEach(([key, keyVal]) => {
    if (typeof keyVal === 'string') {
      if (selectedFields && selectedFields.includes(key)) {
        set(dynamicFields, key, get(val, keyVal)!.toLowerCase());
      }
      set(result, key, get(val, keyVal));
    } else {
      if (keyVal.type === 'dynamic') {
        const containerProp = get(val, keyVal.containerProp) as any[];
        if (Array.isArray(containerProp)) {
          const item = containerProp.find(
            c => c[keyVal.key] === keyVal.fieldProp
          );
          const fieldValue = get(item, keyVal.value);

          fieldValue &&
            set(
              result,
              key,
              keyVal.mappingToType === 'array'
                ? (fieldValue as any[])
                : Array.isArray(fieldValue)
                ? fieldValue[0]
                : fieldValue
            );

          fieldValue && key && set(dynamicFields, 'dynamic', key);
        }
      } else if (keyVal.type === 'mappingDataFromArray') {
        set(
          result,
          key,
          mappingDataFromArray(
            val[keyVal.containerProp],
            keyVal[keyVal.containerProp]
          )
        );
      }
    }
  });
  if (selectedFields && !isEmpty(dynamicFields))
    set(result, 'dynamicFields', dynamicFields);
  return result;
};

/**
 * - ...
 * @param {array} vals
 * @param {object} json
 * @returns {array}
 */
export const mappingDataFromArray = (vals: any[], json: object) => {
  if (!isArray(vals) || isEmpty(vals) || isEmpty(json)) return [];
  return vals.map((val: any) => mappingDataFromObj(val, json));
};

/**
 * - ...
 * @param {array} vals
 * @param {object} json
 * @returns {object}
 */
export const mappingDataAndDynamicFieldsFromArray = (
  vals: any[],
  json: object,
  selectedFields: string[]
) => {
  const dynamicFields = {} as any;
  if (!isArray(vals) || isEmpty(vals) || isEmpty(json)) return {};

  const dataMapped = vals.map((val: any) => {
    const objectMapped = mappingDataFromObj(val, json, selectedFields);

    // set dynamic fields
    if (objectMapped.dynamicFields) {
      selectedFields.forEach(field => {
        if (objectMapped.dynamicFields[field]) {
          const currentDynamicFields: any[] = get(dynamicFields, field, []);
          currentDynamicFields.push(objectMapped.dynamicFields[field]);
          set(
            dynamicFields,
            field,
            currentDynamicFields.filter(
              (word, index, arr) => arr.indexOf(word) === index
            )
          );
        }
      });
    }

    return objectMapped;
  });

  return {
    data: dataMapped,
    dynamicFields
  };
};
