export const getChangeInTermsStatusColor = (
  state: string
): 'green' | 'red' | 'cyan' | 'orange' | 'purple' | 'grey' => {
  const status = (state || '').toLowerCase().replace(/ |-/g, '');

  if (status.includes('completed')) return 'green';
  if (status.includes('notstarted')) return 'orange';
  return 'grey';
};
