export const getCronExpression = (
  startDate: Date | undefined,
  recurrence: string
) => {
  const minutes = startDate?.getMinutes();
  const hours = startDate?.getHours();
  const date = startDate?.getDate();
  const month = startDate?.getMonth();
  const dayOfWeek = startDate?.getDay();
  const year = startDate?.getFullYear();
  switch (recurrence) {
    case 'noRecurrence':
      return `0 ${minutes} ${hours} ${date} ${month} ? ${year}`;
    case 'weekDay':
      return `0 ${minutes} ${hours} ? * MON-FRI`;
    case 'daily':
      return `0 ${minutes} ${hours} * * ?`;
    case 'weekly':
      return `0 ${minutes} ${hours} ? * ${dayOfWeek}`;
    case 'monthly':
      return `0 ${minutes} ${hours} ${date} * ?`;
    case 'yearly':
      return `0 ${minutes} ${hours} ${date} ${month} ? *`;
    default:
      return '';
  }
};
