import { getTableTypeColor } from 'app/helpers/getTableTypeColor';

describe('test Helpers > getTableTypeColor', () => {
  it('test getTableTypeColor function with table equal tablecreated', () => {
    const result = getTableTypeColor('tablecreated');
    expect(result).toEqual('purple');
  });
  it('test getTableTypeColor function with table equal tablemodeled', () => {
    const result = getTableTypeColor('tablemodeled');
    expect(result).toEqual('orange');
  });
  it('test getTableTypeColor function with table equal versioncreated', () => {
    const result = getTableTypeColor('versioncreated');
    expect(result).toEqual('cyan');
  });
  it('test getTableTypeColor function with table equal undefined', () => {
    const result = getTableTypeColor(undefined);
    expect(result).toEqual('grey');
  });
});
