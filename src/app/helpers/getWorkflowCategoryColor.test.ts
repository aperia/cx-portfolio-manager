import { getWorkflowCategoryColor } from 'app/helpers/getWorkflowCategoryColor';

describe('Test getWorkflowCategoryColor.test.ts', () => {
  describe('getWorkflowCategoryColor function ', () => {
    it('should green when contains "penaltypricing"', () => {
      const value = 'Penalty Pricing and some text';

      const result = getWorkflowCategoryColor(value);
      expect(result).toEqual(`green`);
    });

    it('should red when contains "interestprocessing"', () => {
      const value = 'interest Processing and some text';

      const result = getWorkflowCategoryColor(value);
      expect(result).toEqual(`red`);
    });

    it('should purple when contains "modifyaccounts"', () => {
      const value = 'Modify Accounts and some text';

      const result = getWorkflowCategoryColor(value);
      expect(result).toEqual(`purple`);
    });

    it('should grey as default', () => {
      const value = '';

      const result = getWorkflowCategoryColor(value);
      expect(result).toEqual(`grey`);
    });
  });
});
