const isScrollParent = (element: HTMLElement): boolean => {
  const { overflow, overflowX, overflowY } = window.getComputedStyle(element);
  return /auto|scroll|overlay/.test(overflow + overflowY + overflowX);
};

const getParentNode = (element: Node): Node | null => {
  return element.parentElement;
};

const isHTMLElement = (node: Node): boolean => {
  return node instanceof Element;
};

const getNodeName = (element: Node): string => {
  return element.nodeName.toLowerCase();
};

export const getScrollParent = (node: Node | null): HTMLElement | undefined => {
  if (!node) return undefined;
  if (['html', 'body', '#document'].indexOf(getNodeName(node)!) >= 0) {
    return node?.ownerDocument?.body;
  }
  if (isHTMLElement(node) && isScrollParent(node as HTMLElement)) {
    return node as HTMLElement;
  }
  return getScrollParent(getParentNode(node));
};
