export const getWorkflowTypeColor = (
  state: string
): 'green' | 'red' | 'cyan' | 'orange' | 'purple' | 'grey' => {
  const status = (state || '').toLowerCase().replace(/ |-/g, '');

  if (
    status.includes('accountmanagement') ||
    status.includes('productsetup') ||
    status.includes('filemanagement')
  ) {
    return 'cyan';
  }

  if (
    status.includes('correspondence') ||
    status.includes('reissuance') ||
    status.includes('settlement')
  )
    return 'red';

  if (
    status.includes('lending') ||
    status.includes('pricing') ||
    status === 'testinginternal'
  )
    return 'purple';

  if (
    status.includes('monetary') ||
    status.includes('security') ||
    status.includes('utility')
  )
    return 'green';

  if (
    status.includes('portfoliomanagement') ||
    status === 'testing' ||
    status.includes('reporting')
  )
    return 'orange';

  return 'grey';
};
