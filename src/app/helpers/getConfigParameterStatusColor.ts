import { TransactionStatus } from 'app/constants/enums';

export const getConfigParameterStatusColor = (
  state: WorkflowSetupRecordType
): 'green' | 'red' | 'purple' | 'grey' => {
  switch (state) {
    case TransactionStatus.New:
      return 'purple';
    case TransactionStatus.Updated:
      return 'green';
    case TransactionStatus.MarkedForDeletion:
    case TransactionStatus.MarkedForRemoval:
      return 'red';
    default:
      return 'grey';
  }
};
