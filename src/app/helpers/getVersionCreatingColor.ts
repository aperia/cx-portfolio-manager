export const getVersionCreatingColor = (
  state: string
): 'green' | 'red' | 'cyan' | 'orange' | 'purple' | 'grey' => {
  const status = (state || '').toLowerCase().replace(/ |-/g, '');

  if (status.includes('updated')) return 'cyan';
  if (status.includes('created')) return 'purple';
  if (status.includes('impacted')) return 'orange';

  return 'grey';
};

export const mapVersionCreatingValue = (
  value: string
): 'Created' | 'Updated' | 'Impacted' | '' => {
  const text = (value || '').toLowerCase().replace(/ |-/g, '');
  if (text.includes('new')) return 'Created';
  if (text.includes('cloned')) return 'Updated';
  if (text.includes('impacted')) return 'Impacted';
  return '';
};

export const getMethodVersionCreatingValue = (
  method: Omit<WorkflowSetupMethod, 'modeledFrom'> & { modeledFrom?: string }
): 'cloned' | 'new' => {
  return method?.modeledFrom && method?.modeledFrom === method?.name
    ? 'cloned'
    : 'new';
};
