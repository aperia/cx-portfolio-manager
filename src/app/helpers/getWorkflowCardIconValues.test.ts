import { getWorkflowCardIconValues } from 'app/helpers/getWorkflowCardIconValues';

describe('Test getWorkflowCardIconValues.test.ts', () => {
  describe('getWorkflowCardIconValues function', () => {
    it('accesible', () => {
      const value = {
        accesible: true
      } as IWorkflowTemplateModel;

      const result = getWorkflowCardIconValues(value);
      expect(result.iconProps?.name).toEqual(`rating-line`);
      expect(result.tooltipProps?.variant).toEqual(`primary`);
    });
    it('favorite', () => {
      const value = {
        isFavorite: true
      } as IWorkflowTemplateModel;

      const result = getWorkflowCardIconValues(value);
      expect(result.iconProps?.name).toEqual(`lock`);
      expect(result.tooltipProps?.variant).toEqual(`default`);
    });
    it('favorite and accesible', () => {
      const value = {
        isFavorite: true,
        accesible: true
      } as IWorkflowTemplateModel;

      const result = getWorkflowCardIconValues(value);
      expect(result.iconProps?.name).toEqual(`star-fill`);
      expect(result.tooltipProps?.variant).toEqual(`primary`);
    });
  });
});
