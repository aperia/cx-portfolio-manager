export const getTableTypeColor = (
  tableType: string
): 'orange' | 'purple' | 'cyan' | 'grey' => {
  const table = (tableType || '').toLowerCase().replace(/ |-/g, '');
  if (table.includes('tablecreated')) return 'purple';
  if (table.includes('tablemodeled')) return 'orange';
  if (table.includes('versioncreated')) return 'cyan';
  return 'grey';
};
