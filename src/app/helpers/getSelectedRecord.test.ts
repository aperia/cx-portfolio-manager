import { getSelectedRecord } from 'app/helpers/getSelectedRecord';

describe('test Helpers > getMethodTypeColor', () => {
  it('test getMethodTypeColor function ', () => {
    const result = getSelectedRecord({
      selected: 10,
      total: 10,
      text: 'text',
      text1: 'text1',
      textAll: 'textAll'
    });
    expect(result).toEqual('textAll');
  });
  it('test getMethodTypeColor function', () => {
    const result = getSelectedRecord({
      selected: 1,
      total: 10,
      text: 'text',
      text1: 'text1',
      textAll: 'textAll'
    });
    expect(result).toEqual('text1');
  });
  it('test getMethodTypeColor function', () => {
    const result = getSelectedRecord({
      selected: 2,
      total: 10,
      text: 'text',
      text1: 'text1',
      textAll: 'textAll'
    });
    expect(result).toEqual('text');
  });
});
