import { StrategyStatus } from 'app/constants/enums';

type Color =
  | 'green'
  | 'red'
  | 'cyan'
  | 'orange'
  | 'purple'
  | 'grey'
  | undefined;

function stringify(str: string) {
  return str.toLowerCase().replace(/ |-/g, '');
}

const COLORS = {
  green: [StrategyStatus.Production],
  grey: [StrategyStatus.Archived],
  cyan: [StrategyStatus.Scheduled],
  orange: [StrategyStatus.Previous, StrategyStatus.PendingApprovalA1],
  red: [
    StrategyStatus.LapsedDisapprovedA1,
    StrategyStatus.LapsedPendingA1,
    StrategyStatus.DisapprovedA1
  ]
};

function findColor(st: string) {
  let result: Color;
  Object.entries(COLORS).forEach(([color, dict]) => {
    if (dict.map(stringify).includes(stringify(st))) {
      result = color as Color;
    }
  });
  return result;
}

export const getStrategyStatusColor = (state: string) => findColor(state);
