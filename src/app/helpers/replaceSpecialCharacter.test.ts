import { replaceSpecialCharacter } from 'app/helpers/replaceSpecialCharacter';

describe('Test replaceSpecialCharacter.test.ts', () => {
  it('empty value ', () => {
    const value = undefined as any;

    const result = replaceSpecialCharacter(value);
    expect(result).toEqual('');
  });
  it('replace special value ', () => {
    const value = 'Test##%%1234';

    const result = replaceSpecialCharacter(value);
    expect(result).toEqual('Test1234');
  });
});
