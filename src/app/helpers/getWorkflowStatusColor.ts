export const getWorkflowStatusColor = (
  state: string
): 'green' | 'red' | 'cyan' | 'orange' | 'purple' | 'grey' => {
  const status = (state || '').toLowerCase().replace(/ |-/g, '');

  if (status.includes('incomplete')) {
    return 'cyan';
  }

  if (status.includes('complete')) {
    return 'green';
  }

  return 'grey';
};
