import { SortType } from 'app/_libraries/_dls';

export const sortGridData = (
  gridData: MagicKeyValue[],
  sortBy: SortType | undefined
) => {
  if (!sortBy?.order) return gridData;

  const result = [...gridData];

  return result.sort((a, b) => {
    const valueToCompareA = a[sortBy.id];
    const valueToCompareB = b[sortBy.id];

    if (sortBy.order === 'desc') {
      return valueToCompareB > valueToCompareA ? 1 : -1;
    } else return valueToCompareA > valueToCompareB ? 1 : -1;
  });
};
