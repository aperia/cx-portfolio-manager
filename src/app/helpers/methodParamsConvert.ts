export const methodParamsToObject = (
  method: WorkflowSetupMethod,
  useNewValue = true
): WorkflowSetupMethodObjectParams => {
  if (!method?.parameters) return method as WorkflowSetupMethodObjectParams;

  const { parameters, ...props } = method;

  if (!props.versionParameters) props.versionParameters = parameters;

  return {
    ...props,
    ...parameters.reduceRight((pre, curr) => {
      pre[curr.name] = (useNewValue ? curr.newValue : curr.previousValue) || '';
      return pre;
    }, {} as any)
  } as WorkflowSetupMethodObjectParams;
};

export const objectToMethodParams = (
  selectProperties: string[],
  method: WorkflowSetupMethodObjectParams
): WorkflowSetupMethod => {
  const result = { ...method } as WorkflowSetupMethod;
  if (!selectProperties) return result;

  selectProperties.forEach(prop => {
    delete result[prop as keyof WorkflowSetupMethod];
  });

  const preValues = methodParamsToObject({
    parameters: method?.versionParameters
  } as WorkflowSetupMethod);

  result.parameters = selectProperties.map(
    pro =>
      ({
        name: pro,
        newValue: method[pro as keyof WorkflowSetupMethodObjectParams] || '',
        previousValue:
          preValues[pro as keyof WorkflowSetupMethodObjectParams] || ''
      } as WorkflowSetupMethodParams)
  );

  return result;
};
