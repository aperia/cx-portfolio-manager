import { ElementsFieldParameterEnum } from 'app/constants/enums';

export const elementParamsToObject = (
  element: WorkflowSetupElement,
  useNewValue = true
): WorkflowSetupElementObjectParams => {
  if (!element?.parameters) return element as WorkflowSetupElementObjectParams;

  const { parameters, ...props } = element;

  return {
    ...props,
    ...parameters.reduceRight((pre, curr) => {
      pre[curr.name] = curr.value || '';
      return pre;
    }, {} as any)
  } as WorkflowSetupElementObjectParams;
};

export const objectToElementParams = (
  selectProperties: string[],
  element: WorkflowSetupElementObjectParams,
  dropdownElement: ElementsFieldParameterEnum[]
): WorkflowSetupElement => {
  const result = { ...element } as WorkflowSetupElement;

  if (!selectProperties) return result;

  selectProperties.forEach(prop => {
    delete result[prop as keyof WorkflowSetupElement];
  });

  result.parameters = selectProperties.map(pro => ({
    name: pro,
    value:
      (dropdownElement.includes(pro as ElementsFieldParameterEnum)
        ? element[pro as keyof WorkflowSetupElementObjectParams]
        : element[pro as keyof WorkflowSetupElementObjectParams]?.trim()) || ''
  }));

  return result;
};
