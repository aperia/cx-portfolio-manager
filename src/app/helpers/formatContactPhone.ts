import isNaN from 'lodash.isnan';

/**
 * - input: 0885864896
 * - output: (088) 586-4896
 * @param {string} contactPhone
 * @returns {string}
 */
export const formatContactPhone = (contactPhone: string | undefined) => {
  if (isNaN(Number(contactPhone)) || contactPhone?.length !== 10)
    return contactPhone;

  const pre = contactPhone?.slice(0, 3);
  const middle = contactPhone?.slice(3, 6);
  const last = contactPhone?.slice(6, contactPhone?.length);
  return `(${pre}) ${middle}-${last}`;
};
