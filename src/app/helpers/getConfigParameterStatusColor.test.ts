import { TransactionStatus } from 'app/constants/enums';
import { getConfigParameterStatusColor } from 'app/helpers/getConfigParameterStatusColor';

describe('test Helpers > getConfigParameterStatusColor', () => {
  it('test getMethodTypeColor function ', () => {
    const result = getConfigParameterStatusColor(TransactionStatus.New);
    expect(result).toEqual('purple');
  });

  it('test getMethodTypeColor function ', () => {
    const result = getConfigParameterStatusColor(TransactionStatus.Updated);
    expect(result).toEqual('green');
  });
  it('test getMethodTypeColor function ', () => {
    const result = getConfigParameterStatusColor(
      TransactionStatus.MarkedForDeletion
    );
    expect(result).toEqual('red');
  });
  it('test getMethodTypeColor function ', () => {
    const result = getConfigParameterStatusColor(TransactionStatus.NotUpdated);
    expect(result).toEqual('grey');
  });
});
