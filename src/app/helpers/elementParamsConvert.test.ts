import {
  elementParamsToObject,
  objectToElementParams
} from 'app/helpers/elementParamsConvert';

describe('Test elementParamsConvert.test.ts', () => {
  describe('elementParamsToObject function ', () => {
    it('element value undefined', () => {
      const element = {};

      const result = elementParamsToObject(element);
      expect(result).toEqual(element);
    });

    it('element value not undefined', () => {
      const element = {
        parameters: [
          {
            name: '',
            value: ''
          }
        ]
      };

      const result = elementParamsToObject(element);
      expect(result).toEqual({ '': '' });
    });
  });

  describe('objectToElementParams function ', () => {
    it('selectProperties is empty', () => {
      const element = {};

      const result = objectToElementParams('', element);
      expect(result).toEqual(element);
    });

    it('selectProperties is not empty', () => {
      const element = {
        'min.value': 'abc',
        'max.value': 'def'
      };

      const result = objectToElementParams(['min.value'], element, [
        'min.value'
      ]);
      expect(result).toEqual({
        'max.value': 'def',
        parameters: [
          {
            name: 'min.value',
            value: 'abc'
          }
        ]
      });
    });

    it('selectProperties is not empty and enum not exists selectProperties', () => {
      const element = {
        'min.value': 'abc',
        'max.value': 'def'
      };

      const result = objectToElementParams(['min.value'], element, ['123']);
      expect(result).toEqual({
        'max.value': 'def',
        parameters: [
          {
            name: 'min.value',
            value: 'abc'
          }
        ]
      });
    });

    it('selectProperties is not empty and enum not exists selectProperties', () => {
      const element = {
        'min.value': 'abc'
      };

      const result = objectToElementParams(['max.value'], element, ['123']);
      expect(result).toEqual({
        'min.value': 'abc',
        parameters: [
          {
            name: 'max.value',
            value: ''
          }
        ]
      });
    });
  });
});
