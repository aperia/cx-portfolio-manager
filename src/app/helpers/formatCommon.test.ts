import { formatCommon } from 'app/helpers/formatCommon';

jest.mock('app/helpers/formatContactPhone', () => ({
  formatContactPhone: jest.fn()
}));

jest.mock('app/helpers/formatTaxpayerId', () => ({
  formatTaxpayerId: jest.fn()
}));

jest.mock('app/helpers/formatTime', () => ({ formatTime: jest.fn() }));

describe('Test formatCommon.test.ts', () => {
  describe('formatCommon function ', () => {
    it('input invalid', () => {
      const value = {
        toString: ''
      } as any;

      const format = formatCommon(value);
      const resultpercent = format.percent();
      const resultphone = format.phone;
      const resultquantity = format.quantity;
      const resultcurrency = format.currency();
      const resultTaxpayerId = format.taxpayerId;

      expect(resultpercent).toEqual(undefined);
      expect(resultphone).toEqual(undefined);
      expect(resultquantity).toEqual(undefined);
      expect(resultcurrency).toEqual(undefined);
      expect(resultTaxpayerId).toEqual(undefined);
    });

    it('format quantity ', () => {
      const value = '123456';

      const format = formatCommon(value);
      const result = format.quantity;
      expect(result).toEqual('123,456');
    });

    it('format card number ', () => {
      const value = '123456';

      const format = formatCommon(value);
      const result = format.cardNumber;
      expect(result).toEqual('12-3456');
    });

    it('format card number is not number', () => {
      const value = '123 and some tesst 456';

      const format = formatCommon(value);
      const result = format.cardNumber;
      expect(result).toEqual(value);
    });

    it('format currency value is string number > 0 ', () => {
      const value = '123456';

      const format = formatCommon(value);
      const result = format.currency();
      expect(result).toEqual('$123,456.00');
      expect(result!.indexOf('$') >= 0).toBeTruthy();
      expect(result!.indexOf('.') >= 0).toBeTruthy();
    });

    it('format currency value is string number < 0', () => {
      const value = '-123456';

      const format = formatCommon(value);
      const result = format.currency();
      expect(result).toEqual('($123,456.00)');
      expect(result!.indexOf('$') >= 0).toBeTruthy();
      expect(result!.indexOf('.') >= 0).toBeTruthy();
      expect(result!.indexOf('(') >= 0).toBeTruthy();
      expect(result!.indexOf(')') >= 0).toBeTruthy();
    });

    it('format number ', () => {
      const value = '123456';

      const format = formatCommon(value);
      const result = format.number;
      expect(result).toEqual(123456);
    });

    it('format number is not number', () => {
      const value = '123 and some tesst 456';

      const format = formatCommon(value);
      const result = format.number;
      expect(result).toEqual(value);
    });

    it('format percent ', () => {
      const value = '99';

      const format = formatCommon(value);
      const result = format.percent();
      expect(result).toEqual('99.00%');
      expect(result!.indexOf('%') >= 0).toBeTruthy();
      expect(result!.indexOf('.') >= 0).toBeTruthy();

      const result1 = format.percent(null as any);
      expect(result1).toEqual('99%');
    });

    it('format method type', () => {
      const value = 'method';

      const format = formatCommon(value);
      const result = format.methodType;
      expect(result).toEqual('METHOD');
    });

    it('format time', () => {
      const value = '11/11/2000';

      const format = formatCommon(value);
      expect(format.time).toBeUndefined();
    });
  });
});
