import findIndex from 'lodash.findindex';
import findLastIndex from 'lodash.findlastindex';
import isNaN from 'lodash.isnan';

export const formatMaskNumber = (val: string) => {
  const arrNumber = val.split('');
  const firstIndex = findIndex(arrNumber, number =>
    isNaN(parseInt(number, 10))
  );
  const lastIndex = findLastIndex(arrNumber, number =>
    isNaN(parseInt(number, 10))
  );
  let maskText = '·';
  for (let i = 0; i < lastIndex - firstIndex; i++) {
    maskText = maskText + '·';
  }
  return {
    firstText: val.slice(0, firstIndex),
    lastText: val.slice(lastIndex + 1, val.length),
    maskText
  };
};
