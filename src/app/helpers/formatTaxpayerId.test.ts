import { formatTaxpayerId } from 'app/helpers/formatTaxpayerId';

describe('Test formatTaxpayerId.test.ts', () => {
  describe('formatTaxpayerId function ', () => {
    it('value have length difference 9', () => {
      const value = '45678';

      const result = formatTaxpayerId(value);
      expect(result).toEqual(value);
    });
    it('value have length equal 9', () => {
      const value = '123456789';

      const result = formatTaxpayerId(value);
      expect(result).toEqual('12-3456789');
    });
  });
});
