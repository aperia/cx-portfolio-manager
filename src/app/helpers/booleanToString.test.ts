import { booleanToString } from 'app/helpers/booleanToString';

describe('Test booleanToString.test.ts', () => {
  it('convert boolean to Yes', () => {
    const value = true;

    const result = booleanToString(value);
    expect(result).toEqual('Yes');
  });
  it('convert Y to Yes', () => {
    const value = 'Y';

    const result = booleanToString(value);
    expect(result).toEqual('Yes');
  });
});
