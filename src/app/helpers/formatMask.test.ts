import { formatMaskNumber } from 'app/helpers/formatMask';

describe('Test formatMask.test.ts', () => {
  describe('formatMaskNumber function ', () => {
    it('convert float number string to object', () => {
      const firstNumber = 100;
      const lastNumer = 3;
      const mask = '.testStringCharacter';
      const value = `${firstNumber}${mask}${lastNumer}`;

      const result = formatMaskNumber(value);
      expect(result.firstText).toEqual(`${firstNumber}`);
      expect(result.lastText).toEqual(`${lastNumer}`);
      expect(result.maskText.length).toEqual(mask.length);
    });
  });
});
