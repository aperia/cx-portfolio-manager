import {
  getWorkflowSetupMethodType,
  getWorkflowSetupTableType
} from 'app/helpers/getWorkflowSetupMethodType';

describe('test Helpers > getWorkflowSetupMethodType', () => {
  it('test getWorkflowSetupMethodType function', () => {
    let result = getWorkflowSetupMethodType({} as WorkflowSetupMethod);
    expect(result).toEqual('NEWMETHOD');

    result = getWorkflowSetupMethodType({
      name: 'Mock name',
      modeledFrom: {
        name: 'Mock name'
      }
    } as WorkflowSetupMethod);
    expect(result).toEqual('NEWVERSION');

    result = getWorkflowSetupMethodType({
      name: 'Mock name',
      modeledFrom: {
        name: 'Another name'
      }
    } as WorkflowSetupMethod);
    expect(result).toEqual('MODELEDMETHOD');
  });

  it('test getWorkflowSetupTableType function', () => {
    let result = getWorkflowSetupTableType({} as WorkflowSetupMethod);
    expect(result).toEqual('createNewTable');

    result = getWorkflowSetupTableType({
      tableId: 'Mock tableId',
      modeledFrom: {
        tableId: 'Mock tableId'
      }
    } as WorkflowSetupMethod);
    expect(result).toEqual('createNewVersion');

    result = getWorkflowSetupTableType({
      tableId: 'Mock tableId',
      modeledFrom: {
        tableId: 'Another tableId'
      }
    } as WorkflowSetupMethod);
    expect(result).toEqual('addTableToModel');
  });
});
