import isNaN from 'lodash.isnan';

/**
 * - input: 123456789
 * - output: 12-3456789
 * @param {string} formatTaxpayerId
 * @returns {string}
 */
export const formatTaxpayerId = (formatTaxpayerId: string | undefined) => {
  if (isNaN(Number(formatTaxpayerId)) || formatTaxpayerId?.length !== 9)
    return formatTaxpayerId;

  const pre = formatTaxpayerId?.slice(0, 2);
  const last = formatTaxpayerId?.slice(2, formatTaxpayerId?.length);
  return `${pre}-${last}`;
};
