export const getWorkflowCategoryColor = (
  state: string
): 'green' | 'red' | 'cyan' | 'orange' | 'purple' | 'grey' => {
  const status = (state || '').toLowerCase().replace(/ |-/g, '');

  if (status.includes('penaltypricing')) return 'green';
  if (status.includes('interestprocessing')) return 'red';
  if (status.includes('modifyaccounts')) return 'purple';
  return 'grey';
};
