import { getActionTakenStatusColor } from 'app/helpers/getActionTakenStatusColor';

describe('Test getActionTakenStatusColor.test.ts', () => {
  describe('getActionTakenStatusColor function ', () => {
    it('Version Created', () => {
      const result = getActionTakenStatusColor('Version Created');
      expect(result).toEqual('cyan');
    });
    it('Strategy Modeled', () => {
      const result = getActionTakenStatusColor('Strategy Modeled');
      expect(result).toEqual(`orange`);
    });
  });
});
