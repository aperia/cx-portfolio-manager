export const getMethodTypeColor = (
  methodType: string
): 'orange' | 'purple' | 'cyan' | 'grey' => {
  const method = (methodType || '').toLowerCase().replace(/ |-/g, '');

  if (method.includes('methodcreated')) return 'purple';
  if (method.includes('methodmodeled')) return 'orange';
  if (method.includes('versioncreated')) return 'cyan';
  return 'grey';
};
