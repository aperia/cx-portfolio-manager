import { deepEqual } from 'app/helpers/deepEqual';

describe('Test deepEqual.test.ts', () => {
  it('equal object', () => {
    const o1 = {
      p1: { p11: 'a' },
      p2: [{}],
      p3: 'p3',
      p4: { p41: '' }
    };
    const o2 = {
      p1: { p11: 'a' },
      p2: [{}],
      p3: 'p3',
      p4: undefined
    };

    const result = deepEqual(o1, o2);
    expect(result).toEqual(false);
  });
});
