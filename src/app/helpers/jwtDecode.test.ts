import { jwtDecode } from 'app/helpers/jwtDecode';

describe('Test jwtDecode.test.ts', () => {
  describe('jwtDecode function ', () => {
    it('should return empty when token is empty', () => {
      const token = '';

      const result = jwtDecode(token);

      expect(result).toEqual({});
    });
    it('should return object payload when have token', () => {
      const token =
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlc3NVc2VyTmFtZSI6IlRlc3QgTmFtZSIsImV4cCI6MTkwNTg5MTU5NCwiYXBwSWQiOiJjeENvbGxlY3Rpb25zIiwib3BlcmF0b3JJZCI6IkZERVNNQVNUIiwiT3JnSUQiOiJGREVTIiwidXNlcklkIjoiRkRFU01BU1QiLCJuYmYiOjE2MDU4OTA2OTQsImlzcyI6Ik5TQSIsIm9jc1NoZWxsIjoiRkRFU01BU1QiLCJwcml2aWxlZ2VzIjpbIkNYQ09MMSJdLCJ1c2VyTmFtZSI6InRlc3RuYW1lIiwieDUwMElkIjoiQUFBQTExMTQiLCJlbnRpdGxlbWVudCI6W10sImdyb3VwcyI6WyJITE5TQSJdfQ.EzRG8Kvc8kvL48TCBpaYaB7I6nN4JJ_bQNvvOHMPCYQ';

      const result = jwtDecode(token);

      expect(result.userId).toEqual('FDESMAST');
      expect(result.userName).toEqual('testname');
    });

    it('should return object header when have option header', () => {
      const token =
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlc3NVc2VyTmFtZSI6IlRlc3QgTmFtZSIsImV4cCI6MTkwNTg5MTU5NCwiYXBwSWQiOiJjeENvbGxlY3Rpb25zIiwib3BlcmF0b3JJZCI6IkZERVNNQVNUIiwiT3JnSUQiOiJGREVTIiwidXNlcklkIjoiRkRFU01BU1QiLCJuYmYiOjE2MDU4OTA2OTQsImlzcyI6Ik5TQSIsIm9jc1NoZWxsIjoiRkRFU01BU1QiLCJwcml2aWxlZ2VzIjpbIkNYQ09MMSJdLCJ1c2VyTmFtZSI6InRlc3RuYW1lIiwieDUwMElkIjoiQUFBQTExMTQiLCJlbnRpdGxlbWVudCI6W10sImdyb3VwcyI6WyJITE5TQSJdfQ.EzRG8Kvc8kvL48TCBpaYaB7I6nN4JJ_bQNvvOHMPCYQ';

      const result = jwtDecode(token, { header: true });
      expect(result.alg).toEqual('HS256');
      expect(result.typ).toEqual('JWT');
    });

    it('should return object', () => {
      const token =
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiIwMTIzNDU2Nzg5In0.unrJTzr0HlhdLxLq3wjc0B-cZqBLAKRMzFx_g2WyjnA';

      const result = jwtDecode(token);
      expect(result.userId).toEqual('0123456789');
    });
  });
});
