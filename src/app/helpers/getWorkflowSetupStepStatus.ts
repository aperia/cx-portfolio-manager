export const getWorkflowSetupStepStatus = (
  step: WorkflowSetupInstanceStep
): WorkflowSetupStepFormData => {
  const isPass = ['DONE', 'INPROGRESS'].includes(step.status);
  const isSelected = isPass && !!step.selected;

  return { isPass, isSelected };
};
