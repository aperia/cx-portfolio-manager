import {
  getMethodVersionCreatingValue,
  getVersionCreatingColor,
  mapVersionCreatingValue
} from 'app/helpers/getVersionCreatingColor';

describe('Test getVersionCreatingColor.test.ts', () => {
  describe('getVersionCreatingColor fnc', () => {
    it('should cyan when contains "updated"', () => {
      const value = 'updated aa';

      const result = getVersionCreatingColor(value);
      expect(result).toEqual(`cyan`);
    });
    it('should purple when contains "created"', () => {
      const value = 'created aa';

      const result = getVersionCreatingColor(value);
      expect(result).toEqual(`purple`);
    });
    it('should orange when contains "impacted"', () => {
      const value = 'impacted aa';

      const result = getVersionCreatingColor(value);
      expect(result).toEqual(`orange`);
    });
    it('should grey as default', () => {
      const value = '';

      const result = getVersionCreatingColor(value);
      expect(result).toEqual(`grey`);
    });
  });
  describe('mapVersionCreatingValue fnc', () => {
    it('should Created when contains "new"', () => {
      const value = 'new';

      const result = mapVersionCreatingValue(value);
      expect(result).toEqual(`Created`);
    });
    it('should Updated when contains "cloned"', () => {
      const value = 'cloned';

      const result = mapVersionCreatingValue(value);
      expect(result).toEqual(`Updated`);
    });
    it('should Impacted when contains "impacted"', () => {
      const value = 'Impacted';

      const result = mapVersionCreatingValue(value);
      expect(result).toEqual(`Impacted`);
    });
    it('should empty as default', () => {
      const value = '';

      const result = mapVersionCreatingValue(value);
      expect(result).toEqual('');
    });
  });

  describe('getMethodVersionCreatingValue fnc', () => {
    it('should cloned when the method is Create a new version', () => {
      const method = {
        modeledFrom: 'name',
        name: 'name'
      } as any;
      const result = getMethodVersionCreatingValue(method);
      expect(result).toEqual(`cloned`);
    });

    it('should new when the method is Modeled From', () => {
      const method = {
        modeledFrom: 'name',
        name: 'name 1'
      } as any;
      const result = getMethodVersionCreatingValue(method);
      expect(result).toEqual(`new`);
    });

    it('should new when the method is Create a new Method', () => {
      const method = {
        name: 'name'
      } as any;
      const result = getMethodVersionCreatingValue(method);
      expect(result).toEqual(`new`);
    });
  });
});
