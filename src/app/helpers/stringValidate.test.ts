import { stringValidate } from 'app/helpers/stringValidate';

describe('Test stringValidate.test.ts', () => {
  describe('stringValidate function ', () => {
    describe('isAlphabetical function ', () => {
      it('string alphabet vs number', () => {
        const value = 'abc123';

        const result = stringValidate(value);
        expect(result.isAlphabetical()).toBeFalsy();
      });
      it('string alphabet', () => {
        const value = 'abcdef';

        const result = stringValidate(value);

        expect(result.isAlphabetical()).toEqual(true);
      });
    });

    describe('isAlphanumericPunctuationSpecial function ', () => {
      it('string unicode', () => {
        const value = 'Kiểm tra các thứ khác';

        const result = stringValidate(value);
        expect(result.isAlphanumericPunctuationSpecial()).toBeFalsy();
      });
      it('string character display in keyboard', () => {
        const value = 'abc123!@#@$#';

        const result = stringValidate(value);

        expect(result.isAlphanumericPunctuationSpecial()).toEqual(true);
      });
    });

    describe('isSpecialAlphabetical function ', () => {
      it('string number', () => {
        const value = '1234567890';

        const result = stringValidate(value);
        expect(result.isSpecialAlphabetical()).toBeFalsy();
      });
      it('all string', () => {
        const value = 'abc~!@#%$^&&*';

        const result = stringValidate(value);

        expect(result.isSpecialAlphabetical()).toEqual(true);
      });
    });

    describe('isAlphanumeric function ', () => {
      it('string number', () => {
        const value = '!@#$abcdef';

        const result = stringValidate(value);
        expect(result.isAlphanumeric()).toBeFalsy();
      });
      it('all string', () => {
        const value = 'abcdef12331';

        const result = stringValidate(value);

        expect(result.isAlphanumeric()).toEqual(true);
      });
    });

    describe('isSpecialAlphanumeric function ', () => {
      it('string number', () => {
        const value = '~!@#$%^&*()';

        const result = stringValidate(value);
        expect(result.isSpecialAlphanumeric()).toBeFalsy();
      });
      it('all string', () => {
        const value = 'abcdef12331àá';

        const result = stringValidate(value);

        expect(result.isSpecialAlphanumeric()).toEqual(true);
      });
    });

    describe('isNumber function ', () => {
      it('alphabet unicode vs number', () => {
        const value = 'abcdef12331àá';

        const result = stringValidate(value);

        expect(result.isNumber()).toBeFalsy();
      });
      it('string number', () => {
        const value = '123456789';

        const result = stringValidate(value);
        expect(result.isNumber()).toBeTruthy();
      });
    });

    describe('maxLength function ', () => {
      it('string over max length', () => {
        const value = '123456';

        const result = stringValidate(value);

        expect(result.maxLength(5)).toBeFalsy();
      });
      it('under max length', () => {
        const value = '1234';

        const result = stringValidate(value);
        expect(result.maxLength(5)).toBeTruthy();
      });
    });

    describe('minLength function ', () => {
      it('string < min length', () => {
        const value = '123';

        const result = stringValidate(value);

        expect(result.minLength(4)).toBeFalsy();
      });
      it('under min length', () => {
        const value = '123456';

        const result = stringValidate(value);
        expect(result.minLength(5)).toBeTruthy();
      });
    });

    describe('isRequire function ', () => {
      it('empty value', () => {
        const value = '';

        const result = stringValidate(value);

        expect(result.isRequire()).toBeFalsy();
      });
      it('under min length', () => {
        const value = '123456';

        const result = stringValidate(value);
        expect(result.isRequire()).toBeTruthy();
      });
    });
  });
});
