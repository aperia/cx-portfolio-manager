import { mappingArrayKey } from 'app/helpers/mappingArrayKey';

describe('Test mappingArrayKey.test.ts', () => {
  const mockArray = [
    { id: '1', name: 'testName', description: 'description' },
    { id: '2', name: 'testName2', description: 'description2' },
    { id: '3', name: 'testName3', description: 'description3' }
  ];
  it('convertArray with key is id ', () => {
    const result = mappingArrayKey(mockArray, 'id');
    expect(result).toEqual(['1', '2', '3']);
  });

  it('convertArray with empty list', () => {
    const mockArray = [] as any[];

    const result = mappingArrayKey(mockArray, 'id');
    expect(result).toEqual([]);
  });

  it('convertArray with id not from array', () => {
    const result = mappingArrayKey(mockArray, 'test');
    expect(result).toEqual([]);
  });
});
