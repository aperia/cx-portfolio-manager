import { getCronExpression } from 'app/helpers/getCronExpression';

describe('test Helpers > getCronExpression', () => {
  const DateReal = global.Date;
  const mockDate = new Date('2022-01-01T10:10:00');
  let spy: any;
  beforeEach(() => {
    spy = jest.spyOn(global, 'Date').mockImplementation((...args): any => {
      if (args.length) {
        return new DateReal(...args);
      }
      return mockDate;
    });
  });

  afterEach(() => {
    spy.mockRestore();
  });

  it('test getCronExpression function > noRecurrence', () => {
    const rs = getCronExpression(new Date(), 'noRecurrence');

    expect(rs).toEqual('0 10 10 1 0 ? 2022');
  });

  it('test getCronExpression function > weekDay', () => {
    const rs = getCronExpression(new Date(), 'weekDay');

    expect(rs).toEqual('0 10 10 ? * MON-FRI');
  });

  it('test getCronExpression function > daily', () => {
    const rs = getCronExpression(new Date(), 'daily');

    expect(rs).toEqual('0 10 10 * * ?');
  });

  it('test getCronExpression function > weekly', () => {
    const rs = getCronExpression(new Date(), 'weekly');

    expect(rs).toEqual('0 10 10 ? * 6');
  });

  it('test getCronExpression function > monthly', () => {
    const rs = getCronExpression(new Date(), 'monthly');

    expect(rs).toEqual('0 10 10 1 * ?');
  });

  it('test getCronExpression function > yearly', () => {
    const rs = getCronExpression(new Date(), 'yearly');

    expect(rs).toEqual('0 10 10 1 0 ? *');
  });

  it('test getCronExpression function > emmpty recurrence', () => {
    const rs = getCronExpression(new Date(), '');

    expect(rs).toEqual('');
  });
});
