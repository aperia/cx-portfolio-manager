import { matchSearchValue } from 'app/helpers/matchSearchValue';

describe('Test matchSearchValue.test.ts', () => {
  it('match search value ', () => {
    const value = ['abcd', 'dddd'];

    const result = matchSearchValue(value, 'a');
    expect(result).toEqual(true);
  });
  it('NOT match search value ', () => {
    const value = ['abcd', 'dddd'];

    let result = matchSearchValue(value, 'bbb');
    expect(result).toEqual(false);

    result = matchSearchValue('' as any, 'bbb');
    expect(result).toEqual(false);

    result = matchSearchValue(value, '');
    expect(result).toEqual(true);
  });
});
