import { compare2Array } from './array';

describe('Test Helpers > array', () => {
  describe('compare2Array', () => {
    it('2 empty arry', () => {
      const arr1: any[] = [];
      const arr2: any[] = [];
      const result = compare2Array(arr1, arr2);
      expect(result).toBeTruthy();
    });

    it('arr1 empty, arr2 has value', () => {
      const arr1: any[] = [];
      const arr2: any[] = [1];
      const result = compare2Array(arr1, arr2);
      expect(result).toBeFalsy();
    });
    it('arr1 has value, arr2 empty', () => {
      const arr1: any[] = [1];
      const arr2: any[] = [];
      const result = compare2Array(arr1, arr2);
      expect(result).toBeFalsy();
    });
    it('2 array equal', () => {
      const arr1: any[] = [1];
      const arr2: any[] = [1];
      const result = compare2Array(arr1, arr2);
      expect(result).toBeTruthy();
    });
    it('2 array equal and suffer', () => {
      const arr1: any[] = [1, 2];
      const arr2: any[] = [2, 1];
      const result = compare2Array(arr1, arr2);
      expect(result).toBeTruthy();
    });

    it('2 array different', () => {
      const arr1: any[] = [1, 2];
      const arr2: any[] = [3, 1];
      const result = compare2Array(arr1, arr2);
      expect(result).toBeFalsy();
    });

    it('2 array not equal length', () => {
      const arr1: any[] = [1, 2, 3];
      const arr2: any[] = [3, 1];
      const result = compare2Array(arr1, arr2);
      expect(result).toBeFalsy();
    });
  });
});
