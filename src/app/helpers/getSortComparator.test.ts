import { getSortComparator } from 'app/helpers/getSortComparator';

describe('Test getSortComparator.test.ts', () => {
  const data = [
    { fieldA: 'c', fieldB: 2 },
    { fieldA: 'a', fieldB: 4 },
    { fieldA: 'b', fieldB: 3, fieldC: 'a' },
    { fieldA: 'd', fieldB: 1 },
    { fieldA: 'd', fieldB: 0 }
  ];
  it('sortBy string desc/asc ', () => {
    const comparatorAsc = getSortComparator(['fieldA'], ['asc']);
    const comparatorDesc = getSortComparator(['fieldA'], ['desc']);

    let result = [...data].sort(comparatorAsc).map(i => i.fieldA);
    expect(result).toEqual(['a', 'b', 'c', 'd', 'd']);

    result = [...data].sort(comparatorDesc).map(i => i.fieldA);
    expect(result).toEqual(['d', 'd', 'c', 'b', 'a']);
  });
  it('sortBy number desc/asc ', () => {
    const comparatorAsc = getSortComparator(['fieldB'], ['asc']);
    const comparatorDesc = getSortComparator(['fieldB'], ['desc']);

    let result = [...data].sort(comparatorAsc).map(i => i.fieldB);
    expect(result).toEqual([0, 1, 2, 3, 4]);

    result = [...data].sort(comparatorDesc).map(i => i.fieldB);
    expect(result).toEqual([4, 3, 2, 1, 0]);
  });
  it('sortBy undefined desc/asc ', () => {
    const comparatorAsc = getSortComparator(
      ['fieldC', 'fieldB'],
      ['asc', 'asc']
    );
    const comparatorDesc = getSortComparator(
      ['fieldC', 'fieldB'],
      ['desc', 'asc']
    );

    let result = [...data].sort(comparatorAsc).map(i => i.fieldB);
    expect(result).toEqual([3, 0, 1, 2, 4]);

    result = [...data].sort(comparatorDesc).map(i => i.fieldB);
    expect(result).toEqual([3, 0, 1, 2, 4]);
  });
  it('undefined sortBy params', () => {
    const comparatorAsc = getSortComparator(undefined, ['asc', 'asc']);

    const result = [...data].sort(comparatorAsc).map(i => i.fieldB);
    expect(result).toEqual([2, 4, 3, 1, 0]);
  });
});
