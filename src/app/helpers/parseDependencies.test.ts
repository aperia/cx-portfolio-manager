import { parseDependencies } from 'app/helpers/parseDependencies';

describe('test Helpers > parseDependencies', () => {
  it('test parseDependencies function', () => {
    const mockData = {
      data: {
        field1: 'value 1'
      }
    };
    let result = parseDependencies(mockData, ['data.field1']);
    expect(result).toEqual({
      field1: 'value 1'
    });

    result = parseDependencies(mockData, ['data']);
    expect(result).toEqual({});
  });
});
