import isFunction from 'lodash.isfunction';
import isNaN from 'lodash.isnan';
import { formatContactPhone } from './formatContactPhone';
import { formatTaxpayerId } from './formatTaxpayerId';
import { formatTime } from './formatTime';

/**
 * - input: 80.2135
 * - output: 80.21%
 * @param {string} text
 * @returns {string}
 */
const formatPercent =
  (text: string | undefined) =>
  (fractionDigits = 2) => {
    if (isNaN(Number(text))) return text;

    if (fractionDigits === null) {
      return `${Number(text)}%`;
    }

    return `${Number(text).toFixed(fractionDigits)}%`;
  };

/**
 * - input: 1000
 * - output: 1,000
 * @param {string} text
 * @returns {string}
 */
const formatQuantity = (text: string | undefined) => {
  if (isNaN(Number(text))) return text;
  return text
    ?.toString()
    .replace(/\d(?=(?:\d{3})+(?!\d))/g, (a: string) => a + ',');
};

const formatCardNumber = (text: string | undefined) => {
  if (isNaN(Number(text))) return text;

  return text
    ?.toString()
    .replace(/\d(?=(?:\d{4})+(?!\d))/g, (a: string) => a + '-');
};

const formatMethodType = (text: string | undefined) => {
  return text?.toUpperCase();
};

/**
 * - input: text: 1000, fractionDigits: 2
 * - output: 1,000.00
 * @param {string} text
 * @param {number} fractionDigits
 * @returns {string}
 */
const formatCurrency =
  (text: string | undefined) =>
  (fractionDigits = 2) => {
    if (isNaN(Number(text))) return text;

    let bracket = false;
    let amount = Number(text);

    if (amount < 0) {
      bracket = true;
      amount = Number(text) * -1;
    }

    const val = `$${amount
      .toFixed(fractionDigits)
      .replace(/(\d)(?=(\d\d\d)+(?=\.))/g, '$1,')}`;

    return bracket ? `(${val})` : val;
  };

/**
 * - input: text: 0000
 * - output: 0
 * @param {string} text
 * @returns {number}
 */
const formatNumber = (text: string | undefined) => {
  if (isNaN(Number(text))) return text;

  return Number(text);
};

/**
 * - Common format
 * @param {string | Date | number} input
 * @returns {object}
 */
export const formatCommon = (input: string | Date | number) => {
  const text = isFunction(input?.toString) ? input.toString() : undefined;

  return {
    get number() {
      return formatNumber(text);
    },
    get phone() {
      return formatContactPhone(text);
    },
    get quantity() {
      return formatQuantity(text);
    },
    get currency() {
      return formatCurrency(text);
    },
    get percent() {
      return formatPercent(text);
    },
    get time() {
      return formatTime(text);
    },
    get cardNumber() {
      return formatCardNumber(text);
    },
    get methodType() {
      return formatMethodType(text);
    },
    get taxpayerId() {
      return formatTaxpayerId(text);
    }
  };
};
