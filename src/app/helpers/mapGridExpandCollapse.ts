import { ToggleButtonConfig } from 'app/_libraries/_dls';
import { isDevice } from './isDevice';

export const mapGridExpandCollapse =
  (id: string, t: any) =>
  (item: any): ToggleButtonConfig =>
    ({
      id: item[id],
      collapsedTooltipProps: {
        element: t('txt_collapse'),
        opened: isDevice ? false : undefined
      },
      expandedTooltipProps: {
        element: t('txt_expand'),
        opened: isDevice ? false : undefined
      }
    } as ToggleButtonConfig);
