import { getMethodTypeColor } from 'app/helpers/getMethodTypeColor';

describe('test Helpers > getMethodTypeColor', () => {
  it('test getMethodTypeColor function > Method Created', () => {
    const result = getMethodTypeColor('Method Created');
    expect(result).toEqual('purple');
  });
  it('test getMethodTypeColor function > Method Modeled', () => {
    const result = getMethodTypeColor('Method Modeled');
    expect(result).toEqual('orange');
  });
  it('test getMethodTypeColor function > Version Created', () => {
    const result = getMethodTypeColor('Version Created');
    expect(result).toEqual('cyan');
  });
  it('test getMethodTypeColor function > empty input', () => {
    const result = getMethodTypeColor('');
    expect(result).toEqual('grey');
  });
});
