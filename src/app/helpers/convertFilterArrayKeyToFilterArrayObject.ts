import { isEmpty } from 'app/_libraries/_dls/lodash';

export const convertFilterArrayKeyToFilterArrayObject = (
  array: MagicKeyValue[],
  filters: Record<string, any>
) => {
  if (isEmpty(array) || isEmpty(filters)) return [];
  const filterKeys = Object.keys(filters);

  return array.filter((item: MagicKeyValue) => {
    return filterKeys.every((key: string) => {
      return filters[key].find((filter: string) => filter === item[key]);
    });
  });
};
