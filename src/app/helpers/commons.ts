import { getSortComparator } from 'app/helpers';
import { AttributeSearchValue } from 'app/_libraries/_dls';
import camelCase from 'lodash.camelcase';
import get from 'lodash.get';
import isEmpty from 'lodash.isempty';
import orderByFn from 'lodash.orderby';
import upperFirst from 'lodash.upperfirst';

export const getTabTitle = (searchParams: AttributeSearchValue[]): string => {
  let title = searchParams
    .map(item => {
      if (!item) return '';
      if (typeof item.value === 'string') {
        return item.value;
      }
      if (typeof item.value === 'object') {
        return get(item, ['value', 'keyword']);
      }
      return '';
    })
    .filter(e => e)
    .join(', ');
  title = `${title} - Search Results`;
  return title;
};

export const isArrayHasValue = (data: Array<MagicKeyValue>): boolean => {
  return data && data.length ? true : false;
};

export const camelToText = (camelValue: string) => {
  return upperFirst(
    camelValue?.toString().replace(/([a-z0-9])([A-Z0-9])/g, '$1 $2')
  );
};

export const textToCamel = (textValue: string | undefined) => {
  return camelCase(textValue);
};

export const techNameToText = (techName: string | undefined) => {
  return techName
    ? techName
        .split('.')
        .map(t => upperFirst(t))
        .join(' ')
    : '';
};

export const orderByNullLast = (
  list: any[],
  sortBy: string[],
  orderBy: OrderByType[]
) => {
  if (isEmpty(list) || isEmpty(sortBy)) return list;

  return [...list].sort(getSortComparator(sortBy, orderBy));
};

export const orderBySelectedField = (
  list: any[],
  sortBy: string[],
  orderBy: OrderByType[]
) => {
  if (isEmpty(list) || isEmpty(sortBy)) return list;

  const listHasSortField: any[] = [];
  const listHasNoSortField: any[] = [];
  list.forEach(item => {
    if (!!item[sortBy[0]]) {
      listHasSortField.push(item);
    } else {
      listHasNoSortField.push(item);
    }
  });

  return [
    ...orderByFn(
      listHasSortField,
      sortBy.map(sort => (item: any) => item[sort]?.toString().toLowerCase()),
      orderBy
    ),
    ...orderByFn(listHasNoSortField, sortBy, ['asc', ...orderBy.slice(1)])
  ];
};
