import { isEmpty } from 'app/_libraries/_dls/lodash';

export const mappingArrayKey = (array: MagicKeyValue[], key: string) => {
  if (isEmpty(array)) return [];

  const data = array.map((item: MagicKeyValue) => item[key]);

  if (data.includes(undefined)) return [];
  return data;
};
