export const matchSearchValue = (
  arr: string[],
  searchValue: string | undefined
) => {
  return (arr || []).some(
    i =>
      i &&
      i
        .toString()
        .toLowerCase()
        .includes(searchValue?.trim().toLowerCase() || '')
  );
};
