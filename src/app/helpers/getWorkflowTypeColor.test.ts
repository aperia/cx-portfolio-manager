import { getWorkflowTypeColor } from 'app/helpers/getWorkflowTypeColor';

describe('Test getWorkflowTypeColor.test.ts', () => {
  describe('getWorkflowTypeColor function ', () => {
    it('should green when contains "monetary"', () => {
      const value = 'Monetary';

      const result = getWorkflowTypeColor(value);
      expect(result).toEqual(`green`);
    });

    it('should red when contains "correspondence"', () => {
      const value = 'Correspondence';

      const result = getWorkflowTypeColor(value);
      expect(result).toEqual(`red`);
    });

    it('should cyan when contains "accountmanagement"', () => {
      const value = 'Account Management';

      const result = getWorkflowTypeColor(value);
      expect(result).toEqual(`cyan`);
    });

    it('should orange when contains "testing"', () => {
      const value = 'Portfolio Management';

      const result = getWorkflowTypeColor(value);
      expect(result).toEqual(`orange`);
    });

    it('should purple when contains "pricing"', () => {
      const value = 'Testing Internal';

      const result = getWorkflowTypeColor(value);
      expect(result).toEqual(`purple`);
    });

    it('should grey when contains "lending"', () => {
      const value = 'lending';

      const result = getWorkflowTypeColor(value);
      expect(result).toEqual(`purple`);
    });

    it('should grey when contains "reporting"', () => {
      const value = 'reporting';

      const result = getWorkflowTypeColor(value);
      expect(result).toEqual(`orange`);
    });

    it('should grey as default', () => {
      const value = '';

      const result = getWorkflowTypeColor(value);
      expect(result).toEqual(`grey`);
    });
  });
});
