import get from 'lodash.get';
import set from 'lodash.set';

export const parseDependencies = (
  data: Record<string, any>,
  dependencies?: string[]
) => {
  return dependencies?.reduceRight((pre, curr) => {
    const path = curr.split('.');
    if (path.length < 2) return pre;

    const value = get(data, path);
    path.shift();

    set(pre, path, value);
    return pre;
  }, {} as Record<string, any>);
};
