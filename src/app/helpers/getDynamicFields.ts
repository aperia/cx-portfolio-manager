import isArray from 'lodash.isarray';
import isEmpty from 'lodash.isempty';

/**
 * - ...
 * @param {array} vals
 * @param {object} json
 * @returns {array}
 */
export const getDynamicFieldNames = (vals: any[], fieldName: string) => {
  if (!isArray(vals) || isEmpty(vals) || isEmpty(fieldName)) return [];

  const dynamicFields = new Set([] as any[]);
  vals.forEach((val: any) => {
    (val[fieldName] || []).forEach((criteria: any) => {
      if (!isEmpty(criteria?.values)) {
        dynamicFields.add(criteria.fieldName);
      }
    });
  });
  return Array.from(dynamicFields);
};
