export const replaceSpecialCharacter = (value = '') => {
  return value.replace(/[^\w\s]/gi, '');
};
