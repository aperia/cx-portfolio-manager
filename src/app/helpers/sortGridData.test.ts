import { sortGridData } from 'app/helpers/sortGridData';
import { SortType } from 'app/_libraries/_dls/components';

const mockGridData = [
  {
    id: '2',
    postingDate: '2021-02-24',
    amountPaid: '120',
    description: 'description3'
  },
  {
    id: '1',
    postingDate: '2021-02-24',
    amountPaid: '120',
    description: 'description1'
  },
  {
    id: '3',
    postingDate: '2021-02-24',
    amountPaid: '120',
    description: 'description2'
  }
];

describe('Payment > helpers', () => {
  describe('sortGridData', () => {
    it('empty sortBy will use default sort description', () => {
      const result = sortGridData(mockGridData, {} as unknown as SortType);
      expect(result[0].description).toEqual('description3');
    });

    it('desc', () => {
      const result = sortGridData(mockGridData, {
        id: 'description',
        order: 'desc'
      });

      expect(result[0].description).toEqual('description3');
    });

    it('asc', () => {
      const result = sortGridData(mockGridData, {
        id: 'description',
        order: 'asc'
      });
      expect(result[0].description).toEqual('description1');
    });

    it('sortBy DueDate desc', () => {
      const result = sortGridData(mockGridData, {
        id: 'description',
        order: 'desc'
      });
      expect(result[0].description).toEqual('description3');
    });
  });
});
