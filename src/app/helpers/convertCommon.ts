export const convertStringToNumberOnly = (value: string) => {
  return value.toString().replace(/\D/g, '');
};

export const convertNameToCode = (value: string) => {
  return value?.toLowerCase()?.replace(/[^a-zA-Z0-9]/g, '');
};

export const convertRefValue = (
  refData: RefData[],
  refValue: string,
  mapping: Record<string, any> = { None: '' }
) => {
  const newValue = refData.find(o => o.code === refValue)?.text;
  const isInMapping = !!newValue && Object.keys(mapping).includes(newValue);

  return isInMapping ? mapping[newValue!] : newValue;
};
