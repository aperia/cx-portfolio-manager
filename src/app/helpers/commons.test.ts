import {
  camelToText,
  getTabTitle,
  isArrayHasValue,
  orderByNullLast,
  orderBySelectedField,
  techNameToText,
  textToCamel
} from 'app/helpers/commons';
import { AttributeSearchValue } from 'app/_libraries/_dls';

describe('Test common.test.ts', () => {
  describe('getTabTitle function', () => {
    it('params is list value string ', () => {
      const searchParams: AttributeSearchValue[] = [1, 2, 3, 4].map(
        (e, index) => ({ key: index.toString(), value: `${e}` })
      );

      const tabTitle = getTabTitle(searchParams);
      expect(tabTitle).toEqual(`1, 2, 3, 4 - Search Results`);
    });
    it('params is list value string and 1 item is empty ', () => {
      const searchParams: (AttributeSearchValue | undefined)[] = [
        1, 2, 3, 4
      ].map((e, index) => {
        if (e !== 4) return { key: index.toString(), value: `${e}` };

        return undefined;
      });

      const tabTitle = getTabTitle(searchParams as AttributeSearchValue[]);
      expect(tabTitle).toEqual(`1, 2, 3 - Search Results`);
    });
    it('params is list value string & object ', () => {
      const searchParams: AttributeSearchValue[] = [1, 2, 3, 4].map(
        (e, index) => {
          if (e === 1)
            return { key: index.toString(), value: { keyword: '1a' } };
          else if (e === 2) return { key: index.toString(), value: 2 };
          return { key: index.toString(), value: `${e}` };
        }
      );

      const result = getTabTitle(searchParams);
      expect(result).toEqual(`1a, 3, 4 - Search Results`);
    });
    it('params is list value string & object empty ', () => {
      const searchParams = ['', '', ''].map((e, index) => ({
        key: index.toString(),
        value: `${e}`
      }));

      const result = getTabTitle(searchParams);
      expect(result).toEqual(` - Search Results`);
    });
  });

  describe('isArrayHasValue function', () => {
    it('input is array 2 value', () => {
      const array = [1, 2].map((e, index) => ({
        key: index.toString(),
        value: `${e}`
      }));

      const result = isArrayHasValue(array);
      expect(result).toBeTruthy();
    });
    it('input is empty array', () => {
      const array = [] as any;

      const isArray = isArrayHasValue(array);
      expect(isArray).toBeFalsy();
    });
  });

  describe('camelToText/textToCamel/techNameToText function', () => {
    it('parse camel to text', () => {
      const result = camelToText('camelToText');
      expect(result).toEqual('Camel To Text');
    });
    it('parse text to camel', () => {
      const result = textToCamel('Camel To Text');
      expect(result).toEqual('camelToText');
    });
    it('parse tech name to text', () => {
      let result = techNameToText('mock.value');
      expect(result).toEqual('Mock Value');

      result = techNameToText(undefined);
      expect(result).toEqual('');
    });
  });

  describe('orderByNullLast function', () => {
    it('input is empty', () => {
      const array = [
        {
          id: 0,
          text: 'hi0'
        },
        {
          id: 1,
          value: 'xyz1',
          text: 'hi1'
        }
      ];

      const result = orderByNullLast(array, [], ['asc']);
      expect(result[0].value).toEqual(undefined);
    });
    it('input is array 4 value', () => {
      const array = [
        {
          id: 0,
          text: 'hi0'
        },
        {
          id: 1,
          value: 'xyz1',
          text: 'hi1'
        },
        {
          id: 2,
          value: 'xyz2',
          text: 'hi2'
        },
        {
          id: 3,
          text: 'hi3'
        },
        {
          id: 4,
          text: 'hi4'
        }
      ];

      let result = orderByNullLast(array, ['value'], ['asc']);
      expect(result[0].value).toEqual('xyz1');
      result = orderByNullLast(array, ['value'], ['desc']);
      expect(result[0].value).toEqual('xyz2');
    });

    describe('orderBySelectedField function', () => {
      it('input is empty', () => {
        const array = [
          {
            id: 0,
            text: 'hi0'
          },
          {
            id: 1,
            value: 'xyz1',
            text: 'hi1'
          }
        ];

        const result = orderBySelectedField(array, [], ['asc']);
        expect(result[0].value).toEqual(undefined);
      });
      it('input is array 4 value', () => {
        const array = [
          {
            id: 0,
            text: 'hi0'
          },
          {
            id: 1,
            value: 'xyz1',
            text: 'hi1'
          },
          {
            id: 2,
            value: 'xyz2',
            text: 'hi2'
          },
          {
            id: 3,
            text: 'hi3'
          },
          {
            id: 4,
            text: 'hi4'
          }
        ];

        let result = orderBySelectedField(array, ['value'], ['asc']);
        expect(result[0].value).toEqual('xyz1');
        result = orderBySelectedField(array, ['value'], ['desc']);
        expect(result[0].value).toEqual('xyz2');
      });
    });
  });
});
