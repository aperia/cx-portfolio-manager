import { FormatTime } from 'app/constants/enums';
import {
  addCurrentTime,
  compareTime,
  convertLocalDateToUTCDate,
  convertToGMTTimes,
  convertUTCDateToLocalDate,
  formatLogDate,
  formatTime,
  formatTimeConvert,
  formatTimeDefault,
  getCurrentTimePickerValue,
  isToday,
  isYesterday,
  parseDateWithoutTimezone
} from 'app/helpers/formatTime';
import datetime from 'date-and-time';

describe('Test formatTime.test.ts', () => {
  describe('isToday function ', () => {
    it('string newDate', () => {
      const value = new Date().toString();
      const result = isToday(value);

      expect(result).toBeTruthy();
    });
    it('string order date not is today', () => {
      const value = '11/11/2000';
      const result = isToday(value);

      expect(result).toBeFalsy();
    });
  });

  describe('isYesterday function ', () => {
    it('string yesterday', () => {
      const value = new Date();
      value.setDate(value.getDate() - 1);
      const result = isYesterday(value.toString());

      expect(result).toBeTruthy();
    });
    it('string order date not is yesterday', () => {
      const value = '11/11/2000';
      const result = isYesterday(value);

      expect(result).toBeFalsy();
    });
  });

  describe('convertToGMTTimes function ', () => {
    it('date time 11/11/2000', () => {
      const value = new Date('11/11/2000 09:09:09');
      const result = convertToGMTTimes(value);

      expect(result.getHours()).toEqual(0);
      expect(result.getMinutes()).toEqual(0);
      expect(result.getSeconds()).toEqual(0);
      expect(result.getMilliseconds()).toEqual(0);
    });
  });

  describe('formatTime function ', () => {
    it('format year', () => {
      const value = new Date().toString();
      const result = formatTime(value).year;

      expect(result).toEqual(formatTimeDefault(value, FormatTime.Year));
    });

    it('format date', () => {
      const value = new Date().toString();
      const result = formatTime(value).date;

      expect(result).toEqual(formatTimeDefault(value, FormatTime.Date));
    });

    it('format datetime', () => {
      const value = new Date().toString();
      const result = formatTime(value).datetime;

      expect(result).toEqual(formatTimeDefault(value, FormatTime.Datetime));
    });

    it('format datetime UTC', () => {
      const value = new Date().toString();
      const result = formatTime(value).dateUTC;

      expect(result).toEqual(
        formatTimeDefault(value, FormatTime.Date, undefined, true)
      );
    });

    it('format monthDate', () => {
      const value = new Date().toString();
      const result = formatTime(value).monthDate;

      expect(result).toEqual(formatTimeDefault(value, FormatTime.MonthDate));
    });

    it('format shortYearMonth', () => {
      const value = new Date().toString();
      const result = formatTime(value).shortYearMonth;

      expect(result).toEqual(
        formatTimeDefault(value, FormatTime.ShortYearMonth)
      );
    });

    it('format shortMonthShortYear', () => {
      const value = new Date().toString();
      const result = formatTime(value).shortMonthShortYear;

      expect(result).toEqual(
        formatTimeDefault(value, FormatTime.ShortMonthShortYear)
      );
    });

    it('format allDatetime', () => {
      const value = new Date().toString();
      const result = formatTime(value).allDatetime;

      expect(result).toEqual(formatTimeDefault(value, FormatTime.AllDatetime));
    });

    it('format fullTimeMeridiem', () => {
      const value = new Date().toString();
      const result = formatTime(value).fullTimeMeridiem;

      expect(result).toEqual(
        formatTimeDefault(value, FormatTime.FullTimeMeridiem)
      );
    });

    it('format monthYear', () => {
      const value = new Date().toString();
      const result = formatTime(value).monthYear;

      expect(result).toEqual(formatTimeDefault(value, FormatTime.MonthYear));
    });

    it('format shortMonthYear', () => {
      const value = new Date().toString();
      const result = formatTime(value).shortMonthYear;

      expect(result).toEqual(
        formatTimeDefault(value, FormatTime.ShortMonthYear)
      );
    });

    it('format dayOfWeekDate', () => {
      const value = new Date().toString();
      const result = formatTime(value).dayOfWeekDate;

      expect(result).toEqual(
        formatTimeDefault(value, FormatTime.DayOfWeekDate)
      );
    });

    it('format allDateTimeToUTC', () => {
      const value = new Date().toString();
      const result = formatTime(value).allDateTimeToUTC();
      const resultExpect = formatTimeConvert(
        value,
        FormatTime.AllDatetime,
        false,
        true
      )();

      expect(result).toEqual(resultExpect);
    });

    it('format fullTimeMeridiemToUTC', () => {
      const value = new Date().toString();
      const result = formatTime(value).fullTimeMeridiemToUTC();
      const resultExpect = formatTimeConvert(
        value,
        FormatTime.FullTimeMeridiem,
        false,
        true
      )();

      expect(result).toEqual(resultExpect);
    });
    it('format dateForLineChart', () => {
      const value = '2021-01-21T00:00:00';
      const result = formatTime(value).dateForLineChart;

      expect(result).not.toBeUndefined();
    });
    it('format monthShort', () => {
      const value = '2021-01-21T00:00:00';
      const result = formatTime(value).monthShort;
      const resultExpect = formatTimeDefault(
        `${value}Z`,
        FormatTime.MonthShort
      );

      expect(result).toEqual(resultExpect);
    });
    it('format monthShortYear', () => {
      const value = '2021-01-21T00:00:00';
      const result = formatTime(value).monthShortYear;
      const resultExpect = formatTimeDefault(
        `${value}Z`,
        FormatTime.MonthShortYear
      );

      expect(result).toEqual(resultExpect);
    });
    it('format monthShortDate', () => {
      const value = '2021-01-21T00:00:00';
      const result = formatTime(value).monthShortDate;

      expect(result).not.toBeUndefined();
    });
    it('format dateShort', () => {
      const value = '2021-01-21T00:00:00';
      const result = formatTime(value).dateShort;

      expect(result).not.toBeUndefined();
    });
    it('format dateMonthYear', () => {
      const value = '2021-01-21T00:00:00';
      const result = formatTime(value).dateMonthYear;

      expect(result).not.toBeUndefined();
    });
  });

  describe('formatTimeDefault function ', () => {
    it('convert to local', () => {
      const value = '2021-01-21T00:00:00';
      const result = formatTimeConvert(value, FormatTime.Datetime, true)();

      expect(result).not.toBeUndefined();
    });

    it('keep raw default format', () => {
      const value = '2021-01-21T00:00:00Z';
      const result = formatTimeConvert(value, FormatTime.Datetime)();

      const resultExpected = formatTimeDefault(value, FormatTime.Datetime);
      expect(result).toEqual(resultExpected);
    });

    it('invalid format date timezone', () => {
      const value = '';
      const result = formatTimeConvert(value, FormatTime.Datetime, true)();

      const resultExpected = formatTimeDefault(value, FormatTime.Datetime);
      expect(result).toEqual(resultExpected);
    });
  });

  describe('parseDateWithoutTimezone function ', () => {
    it('convert to local', () => {
      const value = new Date('2021-01-21T00:00:00');
      const result = parseDateWithoutTimezone(value);

      const resultExpected = convertUTCDateToLocalDate(value);
      expect(result).toEqual(new Date(resultExpected).toISOString());
    });

    it('convert to UTC', () => {
      const date = new Date();

      const resultExpected = convertLocalDateToUTCDate(date);
      expect(resultExpected).toEqual(
        new Date(date.getTime() + date.getTimezoneOffset() * 60000)
      );
    });
  });

  describe('formatTimeDefault function ', () => {
    it('string newDate', () => {
      const value = '05-2020';
      const result = formatTimeDefault(value, FormatTime.ShortMonthYear);

      expect(result).toEqual('05/2020');
    });

    it('string date time', () => {
      const value = new Date();
      const format = FormatTime.Date;

      const result = formatTimeDefault(value.toString(), format);
      const resultExpect = datetime.format(value, format);

      expect(result).toEqual(resultExpect);
    });

    it('string order format not string date time', () => {
      const value = 'test order format';
      const format = FormatTime.Date;
      const result = formatTimeDefault(value.toString(), format);

      expect(result).toEqual('');
    });

    it('string invalid date', () => {
      const value = '055-2020';
      const result = formatTimeDefault(value, FormatTime.ShortMonthYear);

      expect(result).toEqual('');
    });
  });

  describe('formatLogDate function ', () => {
    it('today', () => {
      const value = new Date();
      const result = formatLogDate(value.toString());

      expect(result).toEqual('Today');
    });
    it('yesterday', () => {
      const value = new Date();
      value.setDate(value.getDate() - 1);
      const result = formatLogDate(value.toString());

      expect(result).toEqual('Yesterday');
    });
    it('day of week', () => {
      const value = '11/11/2000';
      const result = formatLogDate(value);

      expect(result).toEqual('Sat 11/11/2000');
    });
  });

  describe('addCurrentTime function ', () => {
    it('today', () => {
      const value = new Date(2021, 1, 1);
      const result = addCurrentTime(value);

      expect(result).toBeTruthy();
    });
  });

  describe('Time picker format function ', () => {
    it('getCurrentTimePickerValue', () => {
      const DateReal = global.Date;

      let mockDate = new Date('2022-01-01T00:00:00');

      let spy = jest
        .spyOn(global, 'Date')
        .mockImplementation((...args): any => {
          if (args.length) {
            return new DateReal(...args);
          }
          return mockDate;
        });
      const result = getCurrentTimePickerValue();

      expect(result.hour).toEqual('00');
      expect(result.meridiem).toEqual('AM');

      spy.mockClear();

      mockDate = new Date('2022-01-01T22:15:00');
      spy = jest.spyOn(global, 'Date').mockImplementation((...args): any => {
        if (args.length) {
          return new DateReal(...args);
        }
        return mockDate;
      });

      const resultPM = getCurrentTimePickerValue();

      expect(resultPM.hour).toEqual('10');
      expect(resultPM.meridiem).toEqual('PM');
      spy.mockRestore();
    });

    it('compareTime funciton', () => {
      const result = compareTime(
        {
          hour: '01',
          minute: '10',
          meridiem: 'AM'
        },
        {
          hour: '01',
          minute: '10',
          meridiem: 'PM'
        }
      );

      expect(result).toEqual(-1);

      const resultUndefined = compareTime(undefined, undefined);

      expect(resultUndefined).toEqual(0);

      const result1 = compareTime(
        {
          hour: '01',
          minute: '11',
          meridiem: 'PM'
        },
        {
          hour: '01',
          minute: '10',
          meridiem: 'PM'
        }
      );

      expect(result1).toEqual(1);
    });
  });
});
