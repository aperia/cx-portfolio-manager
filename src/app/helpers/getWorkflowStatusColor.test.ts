import { getWorkflowStatusColor } from 'app/helpers/getWorkflowStatusColor';

describe('Test getWorkflowStatusColor.test.ts', () => {
  it('should cyan when contains "incomplete"', () => {
    const value = 'Incomplete and some text';

    const result = getWorkflowStatusColor(value);
    expect(result).toEqual(`cyan`);
  });

  it('should green when contains "complete"', () => {
    const value = 'Complete and some text';

    const result = getWorkflowStatusColor(value);
    expect(result).toEqual(`green`);
  });

  it('should grey as default', () => {
    const value = '';

    const result = getWorkflowStatusColor(value);
    expect(result).toEqual(`grey`);
  });
});
