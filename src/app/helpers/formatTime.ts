import { TimePickerValue } from 'app/_libraries/_dls/components/TimePicker';
import dateTime from 'date-and-time';
import isDate from 'lodash.isdate';
import { FormatTime } from '../constants/enums';

export const isValidDate = (date: string | Date | number | undefined) => {
  const newDate = new Date(date!);
  return isDate(newDate) && !isNaN(newDate.valueOf());
};

/**
 * - ...
 * - input: time: '10/16/2020', format: 'YYYY'
 * - output: '2020'
 * @param {string} time
 * @param {string} outputFormat
 * @param {string} inputFormat
 * @returns {Date}
 */
export const formatTimeDefault = (
  date: string,
  outputFormat: FormatTime,
  inputFormat?: string,
  isUTC?: boolean
) => {
  // Check date format is just have 0, ex: 000000
  const invalidFormatRegex = new RegExp('^[^1-9]+$');
  if (invalidFormatRegex.test(date)) {
    return '';
  }

  let newDate = new Date(date);

  // date: string Date
  //     || number string
  if (isValidDate(date)) {
    return dateTime.format(newDate, outputFormat, isUTC);
  }

  newDate = dateTime.parse(
    date,
    inputFormat || outputFormat.replace(/\//g, '-')
  );

  // date: MM-YYYY => MM/YYYY
  if (newDate && isValidDate(newDate)) {
    return dateTime.format(newDate, outputFormat, isUTC);
  }

  return '';
};

/**
 * ...
 * @param {string} time
 * @param {string} format
 * @param {boolean} isConvertLocal
 * @param {boolean} isConvertUTCToLocal
 * @returns {Date}
 */
export const formatTimeConvert =
  (
    time: string,
    format: FormatTime,
    isConvertLocal = false,
    isConvertUTCToLocal = false
  ) =>
  () => {
    if (isConvertLocal) time = time ? `${time}Z` : time;
    if (isConvertUTCToLocal) time = time.replace('Z', '');

    return formatTimeDefault(time, format);
  };

/**
 * - Common format time
 * @param {string} time
 * @returns {object}
 */
export const formatTime = (time: string | undefined) => {
  return {
    get year() {
      return formatTimeDefault(time!, FormatTime.Year);
    },
    get date() {
      return formatTimeDefault(time!, FormatTime.Date);
    },
    get dateUTC() {
      return formatTimeDefault(time!, FormatTime.Date, undefined, true);
    },
    get datetime() {
      return formatTimeDefault(time!, FormatTime.Datetime);
    },
    get monthDate() {
      return formatTimeDefault(time!, FormatTime.MonthDate);
    },
    get shortYearMonth() {
      return formatTimeDefault(time!, FormatTime.ShortYearMonth);
    },
    get shortMonthShortYear() {
      return formatTimeDefault(time!, FormatTime.ShortMonthShortYear);
    },
    get allDatetime() {
      return formatTimeDefault(time!, FormatTime.AllDatetime);
    },
    get fullTimeMeridiem() {
      return formatTimeDefault(time!, FormatTime.FullTimeMeridiem);
    },
    get monthYear() {
      return formatTimeDefault(time!, FormatTime.MonthYear);
    },
    get shortMonthYear() {
      return formatTimeDefault(time!, FormatTime.ShortMonthYear);
    },
    get dayOfWeekDate() {
      return formatTimeDefault(time!, FormatTime.DayOfWeekDate);
    },
    get allDateTimeToUTC() {
      return formatTimeConvert(time!, FormatTime.AllDatetime, false, true);
    },
    get fullTimeMeridiemToUTC() {
      return formatTimeConvert(time!, FormatTime.FullTimeMeridiem, false, true);
    },
    get dateForLineChart() {
      return formatTimeDefault(time!, FormatTime.DateForLineChart);
    },
    get monthShort() {
      return formatTimeDefault(time!, FormatTime.MonthShort);
    },
    get monthShortYear() {
      return formatTimeDefault(time!, FormatTime.MonthShortYear);
    },
    get monthShortDate() {
      return formatTimeDefault(time!, FormatTime.MonthShortDate);
    },
    get dateShort() {
      return formatTimeDefault(time!, FormatTime.DateShort);
    },
    get dateMonthYear() {
      return formatTimeDefault(time!, FormatTime.DateMonthYear);
    }
  };
};

/**
 * - Check if input date is the same as today
 * @param {string} date
 * @returns {boolean}
 */
export const isToday = (date: string) =>
  dateTime.isSameDay(new Date(), new Date(date));

/**
 * - Check if input date is the same as yesterday
 * @param {string} date
 * @returns {boolean}
 */
export const isYesterday = (date: string) =>
  dateTime.isSameDay(dateTime.addDays(new Date(), -1), new Date(date));

/**
 * - Convert UTC Date to Local Date
 * - input: Thu Oct 15 2020 00:00:00 GMT+0700 (Indochina Time)
 * - output: Wed Oct 14 2020 17:00:00 GMT+0700 (Indochina Time)
 * @param {Date} date
 * @returns {Date}
 */
export const convertUTCDateToLocalDate = (date: Date) => {
  return new Date(date.getTime() - date.getTimezoneOffset() * 60000);
};

/**
 * - Convert Local Date to UTC Date
 * @param {Date} date
 * @returns {Date}
 */
export const convertLocalDateToUTCDate = (date: Date) => {
  return new Date(date.getTime() + date.getTimezoneOffset() * 60000);
};

/**
 * ...
 * - input: Thu Oct 15 2020 00:00:00 GMT+0700 (Indochina Time)
 * - output: output: 2020-10-14T10:00:00.000Z
 * @param {Date} date
 * @returns {Date}
 */
export const parseDateWithoutTimezone = (date: Date) => {
  return new Date(convertUTCDateToLocalDate(date)).toISOString();
};

/**
 * - Reset to midnight
 * - input:  Wed Aug 05 2020 11:58:00 GMT+0700 (Indochina Time)
 * - output: Wed Aug 05 2020 00:00:00 GMT+0700 (Indochina Time)
 * @param {Date} date
 * @returns {Date}
 */
export const convertToGMTTimes = (date: Date) =>
  new Date(date.setHours(0, 0, 0, 0));

/**
 * - Format time in Log Screen
 * @param {string} date
 * @returns {string}
 */
export const formatLogDate = (date: string) => {
  return isToday(date)
    ? 'Today'
    : isYesterday(date)
    ? 'Yesterday'
    : formatTime(date).dayOfWeekDate;
};

/**
 * - Add currrent time to date
 * @param {Date} date
 * @returns {Date}
 */
export const addCurrentTime = (date: Date) => {
  const d = new Date();
  date.setHours(d.getHours());
  date.setMinutes(d.getMinutes());
  date.setSeconds(d.getSeconds());
  date.setMilliseconds(d.getMilliseconds());
  return date;
};

/**
 * - Get current time as TimePickerValue
 * @returns {Number}
 */

export const getCurrentTimePickerValue = (): TimePickerValue => {
  const today = new Date();
  const hour24 = today.getHours();
  const hour12 = hour24 > 12 ? hour24 - 12 : hour24;
  const hourStr = hour12 > 9 ? hour12.toString() : '0' + hour12.toString();
  const meridiem24 = hour24 > 12 ? 'PM' : 'AM';
  const minute24 = today.getMinutes();
  const minuteStr =
    minute24 > 9 ? minute24.toString() : '0' + minute24.toString();
  const currentTime = {
    hour: hourStr,
    minute: minuteStr,
    meridiem: meridiem24
  };
  return currentTime;
};

/**
 * - compare time
 * @param {TimePickerValue} a
 * @param {TimePickerValue} b
 * @returns {Number}
 */
export const compareTime = (
  a: TimePickerValue | undefined,
  b: TimePickerValue | undefined
): number => {
  const {
    hour: hourA = '00',
    minute: minuteA = '00',
    meridiem: meridiemA = 'AM'
  } = a || {};
  const {
    hour: hourB = '00',
    minute: minuteB = '00',
    meridiem: meridiemB = 'AM'
  } = b || {};
  const dateA = new Date();
  const hourANumber =
    meridiemA === 'AM' ? parseInt(hourA) : 12 + parseInt(hourA);
  const minuteANumber = parseInt(minuteA);
  dateA.setHours(hourANumber);
  dateA.setMinutes(minuteANumber);

  const dateB = new Date();
  const hourBNumber =
    meridiemB === 'AM' ? parseInt(hourB) : 12 + parseInt(hourB);
  const minuteBNumber = parseInt(minuteB);
  dateB.setHours(hourBNumber);
  dateB.setMinutes(minuteBNumber);
  const x: any = dateA > dateB;
  const y: any = dateA < dateB;
  return x - y;
};
