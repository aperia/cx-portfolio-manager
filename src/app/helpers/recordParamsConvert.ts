export const recordParamsToObject = (
  record: WorkflowSetupRecord,
  useNewValue = true
): WorkflowSetupRecordObjectParams => {
  if (!record?.parameters) return record as WorkflowSetupRecordObjectParams;

  const { parameters, ...props } = record;

  return {
    ...props,
    ...parameters.reduceRight((pre, curr) => {
      pre[curr.name] = useNewValue ? curr.newValue : curr.previousValue;
      return pre;
    }, {} as any)
  } as WorkflowSetupRecordObjectParams;
};

export const objectToRecordParams = (
  selectProperties: string[],
  record: WorkflowSetupRecordObjectParams,
  initRecord: WorkflowSetupRecordObjectParams
): WorkflowSetupRecord => {
  const result = { ...record } as WorkflowSetupRecord;

  if (!selectProperties) return result;

  selectProperties.forEach(prop => {
    delete result[prop as keyof WorkflowSetupRecord];
  });

  result.parameters = selectProperties.map(
    pro =>
      ({
        name: pro,
        newValue: record[pro as keyof WorkflowSetupRecordObjectParams] || '',
        previousValue: initRecord[pro as keyof WorkflowSetupRecordObjectParams]
      } as WorkflowSetupRecordParams)
  );

  return result;
};
