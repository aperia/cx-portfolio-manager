import { getChangeStatusColor } from 'app/helpers/getChangeStatusColor';

describe('Test getChangeStatusColor.test.ts', () => {
  describe('getChangeStatusColor function ', () => {
    it('should green when contains "production"', () => {
      const value = 'production and some text';

      const result = getChangeStatusColor(value);
      expect(result).toEqual(`green`);
    });
    it('should green when contains "validated"', () => {
      const value = 'validated and some text';

      const result = getChangeStatusColor(value);
      expect(result).toEqual(`green`);
    });

    it('should green when contains "scheduled"', () => {
      const value = 'scheduled and some text';

      const result = getChangeStatusColor(value);
      expect(result).toEqual(`orange`);
    });
    it('should orange when contains "pending"', () => {
      const value = 'clientpendinglevel';

      const result = getChangeStatusColor(value);
      expect(result).toEqual(`orange`);
    });

    it('should red when contains "lapsed"', () => {
      const value = 'lapsed and some text';

      const result = getChangeStatusColor(value);
      expect(result).toEqual(`red`);
    });
    it('should red when contains "rejected"', () => {
      const value = 'rejected and some text';

      const result = getChangeStatusColor(value);
      expect(result).toEqual(`red`);
    });

    it('should cyan when contains "work"', () => {
      const value = 'work and some text';

      const result = getChangeStatusColor(value);
      expect(result).toEqual(`cyan`);
    });

    it('should grey as default', () => {
      const value = '';

      const result = getChangeStatusColor(value);
      expect(result).toEqual(`grey`);
    });
  });
});
