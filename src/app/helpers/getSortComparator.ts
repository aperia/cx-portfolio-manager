import { isString } from 'lodash';

export const getSortComparator = (
  sortBy: string[] | undefined,
  orderBy: OrderByType[] | undefined,
  nilValuesDef: any[] = [undefined, '']
) => {
  const sortComparator = (a: any, b: any): number => {
    if (!sortBy || !orderBy) return 0;
    for (let i = 0; i < sortBy.length; i++) {
      const orderIdx: 1 | -1 = orderBy[i] === 'desc' ? -1 : 1;
      const k = sortBy[i];

      const valueA = isString(a[k]) ? a[k]?.trim()?.toLowerCase() : a[k];
      const valueB = isString(b[k]) ? b[k]?.trim()?.toLowerCase() : b[k];

      const isNillValA = nilValuesDef.includes(valueA);
      const isNillValB = nilValuesDef.includes(valueB);
      if (isNillValA && !isNillValB) {
        return 1;
      }

      if (isNillValB && !isNillValA) {
        return -1;
      }

      if (valueA > valueB) return orderIdx;

      if (valueA < valueB) return orderIdx * -1;
    }

    return 0;
  };

  return sortComparator;
};
