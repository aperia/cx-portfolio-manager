import { getScrollParent } from 'app/helpers/getScrollParent';

describe('Test isScrollParent.test.ts', () => {
  describe('isScrollParent function ', () => {
    it('should return undefined', () => {
      const result = getScrollParent(null);
      expect(result).toBeUndefined();
    });
    it('should return body', () => {
      const result = getScrollParent(document.body);
      expect(result).toEqual(document.body);
    });
    it('should return parent node as body', () => {
      const element = document.createElement('div');
      document.body.appendChild(element);

      const result = getScrollParent(element);
      expect(result).toEqual(document.body);
    });
    it('should return parent scroll', () => {
      const scrollElement = document.createElement('div');
      scrollElement.style.overflow = 'scroll';

      const node = document.createElement('div');

      scrollElement.appendChild(node);

      const result = getScrollParent(node);
      expect(result).toEqual(scrollElement);
    });
  });
});
