import {
  methodParamsToObject,
  objectToMethodParams
} from 'app/helpers/methodParamsConvert';

describe('Test methodParamsConvert', () => {
  it('methodParamsToObject function > useNewValue = true', () => {
    const parameters = [
      {
        name: 'returned.check.charge.option',
        previousValue: '1',
        newValue: '2'
      },
      {
        name: 'late.charges.option',
        previousValue: '0',
        newValue: '2'
      }
    ];

    let result = methodParamsToObject({
      parameters
    } as WorkflowSetupMethod);
    expect(result).toEqual({
      'late.charges.option': '2',
      'returned.check.charge.option': '2',
      versionParameters: parameters
    });

    result = methodParamsToObject({} as WorkflowSetupMethod);
    expect(result).toEqual({});

    result = methodParamsToObject({
      parameters,
      versionParameters: parameters
    } as WorkflowSetupMethod);
    expect(result).toEqual({
      'late.charges.option': '2',
      'returned.check.charge.option': '2',
      versionParameters: parameters
    });
  });

  it('methodParamsToObject function > useNewValue = false', () => {
    const parameters = [
      {
        name: 'returned.check.charge.option',
        previousValue: undefined,
        newValue: '2'
      },
      {
        name: 'late.charges.option',
        previousValue: '0',
        newValue: '2'
      }
    ];

    const result = methodParamsToObject(
      {
        parameters
      } as WorkflowSetupMethod,
      false
    );
    expect(result).toEqual({
      'late.charges.option': '0',
      'returned.check.charge.option': '',
      versionParameters: parameters
    });
  });

  it('objectToMethodParams function', () => {
    let result = objectToMethodParams(
      ['late.charges.option', 'returned.check.charge.option'],
      {
        'late.charges.option': '2',
        'returned.check.charge.option': '0'
      } as WorkflowSetupMethodObjectParams
    );
    expect(result.parameters[0]).toEqual({
      name: 'late.charges.option',
      previousValue: '',
      newValue: '2'
    });

    result = objectToMethodParams(
      undefined as any,
      {
        'late.charges.option': '2',
        'returned.check.charge.option': '0'
      } as WorkflowSetupMethodObjectParams
    );

    expect(result).toEqual({
      'late.charges.option': '2',
      'returned.check.charge.option': '0'
    });

    result = objectToMethodParams(
      ['late.charges.option', 'returned.check.charge.option'],
      {
        'late.charges.option': '',
        'returned.check.charge.option': ''
      } as WorkflowSetupMethodObjectParams
    );

    expect(result.parameters[1]).toEqual({
      name: 'returned.check.charge.option',
      previousValue: '',
      newValue: ''
    });
  });
});
