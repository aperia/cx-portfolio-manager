import { StrategyStatus } from 'app/constants/enums';
import { getStrategyStatusColor } from 'app/helpers/getStrategyStatusColor';

describe('Test getStrategyStatusColor.test.ts', () => {
  describe('getStrategyStatusColor function ', () => {
    it('Production', () => {
      const result = getStrategyStatusColor(StrategyStatus.Production);
      expect(result).toEqual('green');
    });

    it('Archived', () => {
      const result = getStrategyStatusColor(StrategyStatus.Archived);
      expect(result).toEqual(`grey`);
    });
    it('Scheduled', () => {
      const result = getStrategyStatusColor(StrategyStatus.Scheduled);
      expect(result).toEqual('cyan');
    });
    it('Previous', () => {
      const result = getStrategyStatusColor(StrategyStatus.Previous);
      expect(result).toEqual(`orange`);
    });
    it('LapsedDisapprovedA1', () => {
      const result = getStrategyStatusColor(StrategyStatus.LapsedDisapprovedA1);
      expect(result).toEqual(`red`);
    });
  });
});
