import { mapGridExpandCollapse } from 'app/helpers/mapGridExpandCollapse';
import mockDevice from 'app/utils/_mockHelperConstant/mockDevice';

describe('test Helpers > mapGridExpandCollapse with isDevice equal true', () => {
  beforeEach(() => {
    mockDevice.isDevice = true;
  });

  it('test getTableTypeColor function', () => {
    const result = mapGridExpandCollapse('id', jest.fn())({ id: 'test' });
    expect(result).toEqual({
      collapsedTooltipProps: {
        element: undefined,
        opened: false
      },
      expandedTooltipProps: {
        element: undefined,
        opened: false
      },
      id: 'test'
    });
  });
});

describe('test Helpers > mapGridExpandCollapse with isDevice equal false', () => {
  beforeEach(() => {
    mockDevice.isDevice = false;
  });

  it('test getTableTypeColor function', () => {
    const result = mapGridExpandCollapse('id', jest.fn())({ id: 'test' });
    expect(result).toEqual({
      collapsedTooltipProps: {
        element: undefined,
        opened: undefined
      },
      expandedTooltipProps: {
        element: undefined,
        opened: undefined
      },
      id: 'test'
    });
  });
});
