export const getWorkflowSetupMethodType = (
  method: WorkflowSetupMethod
): WorkflowSetupMethodType => {
  if (!method.modeledFrom) return 'NEWMETHOD';

  if (method.modeledFrom.name === method.name) return 'NEWVERSION';

  return 'MODELEDMETHOD';
};

export const getWorkflowSetupTableType = (
  table: WorkflowSetupMethod
): 'createNewVersion' | 'createNewTable' | 'addTableToModel' => {
  if (!table.modeledFrom) return 'createNewTable';

  if (table.modeledFrom.tableId === table.tableId) return 'createNewVersion';

  return 'addTableToModel';
};
