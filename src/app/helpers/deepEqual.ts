import { isArray, isEqual, isObject } from 'lodash';

export const deepEqual = (
  o1: any,
  o2: any,
  ignoreProps?: string[]
): boolean => {
  if (!isObject(o1) && !isArray(o1)) return isEqual(o1, o2);

  if (isArray(o1)) {
    return (o1 as any[]).every((i, idx) =>
      deepEqual(i, o2?.[idx], ignoreProps)
    );
  }

  const o1K = o1
    ? Object.keys(o1)
        .filter(k => !ignoreProps?.includes(k))
        .sort()
    : [];
  const o2K = o2
    ? Object.keys(o2)
        .filter(k => !ignoreProps?.includes(k))
        .sort()
    : [];
  if (!isEqual(o1K, o2K)) return false;

  return o1K.every((k: string) =>
    deepEqual((o1 as any)[k], o2[k], ignoreProps)
  );
};
