import {
  base64toBlob,
  bytesToFileSize,
  downloadBlobFile
} from 'app/helpers/fileUtilities';

describe('Test fileUtilities.test.ts', () => {
  describe('bytesToFileSize function ', () => {
    it('default params', () => {
      const result = bytesToFileSize();
      expect(result).toEqual(`0 Bytes`);
    });
    it('file size', () => {
      const result = bytesToFileSize(1050);
      expect(result).toEqual(`1.03 KB`);

      const resultNoDecimal = bytesToFileSize(1050, -1);
      expect(resultNoDecimal).toEqual(`1 KB`);
    });
  });
  describe('base64toBlob function ', () => {
    it('default params', () => {
      const result = base64toBlob();
      expect(result.size).toEqual(0);
    });
    it('parse to blob', () => {
      const base64 = btoa('aa');
      const result = base64toBlob(base64);
      expect(result.size).toEqual(2);
    });
  });
  describe('downloadBlobFile function ', () => {
    let windowSpy: jest.SpyInstance;

    beforeEach(() => {
      windowSpy = jest.spyOn(window, 'window', 'get');
    });

    afterEach(() => {
      windowSpy.mockRestore();
    });

    it('default download', () => {
      jest.useFakeTimers();
      const createObjectURLFn = jest.fn();

      windowSpy.mockImplementation(() => ({
        URL: { createObjectURL: createObjectURLFn }
      }));

      downloadBlobFile({} as Blob, '');
      jest.advanceTimersByTime(500);
      expect(createObjectURLFn).toHaveBeenCalled();
    });
    it('ms download', () => {
      const msSaveOrOpenBlobFn = jest.fn();

      windowSpy.mockImplementation(() => ({
        navigator: {
          msSaveOrOpenBlob: msSaveOrOpenBlobFn
        }
      }));

      downloadBlobFile({} as Blob, '');
      expect(msSaveOrOpenBlobFn).toHaveBeenCalled();
    });
  });
});
