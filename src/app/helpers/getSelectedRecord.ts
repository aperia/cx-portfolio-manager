export const getSelectedRecord = ({
  selected,
  total,
  text,
  text1,
  textAll
}: {
  selected: number;
  total: number;
  text: string;
  text1: string;
  textAll: string;
}) => {
  if (selected === total && selected !== 0) return textAll;

  if (selected === 1) return text1;

  return text;
};
