import { isEmpty, isEqual } from 'app/_libraries/_dls/lodash';

export const compare2Array = (arr1?: any[], arr2?: any[]) => {
  const isEmptyArr1 = isEmpty(arr1);
  const isEmptyArr2 = isEmpty(arr2);

  if (isEmptyArr1 && isEmptyArr2) return true;

  if ((isEmptyArr1 && !isEmptyArr2) || (!isEmptyArr1 && isEmptyArr2)) {
    return false;
  }

  const lengthArr1 = arr1!.length;
  const lengthArr2 = arr2!.length;

  if (lengthArr1 !== lengthArr2) return false;

  for (const item1 of arr1!) {
    const hasValue = arr2!.find(item2 => isEqual(item1, item2));
    if (!hasValue) return false;
  }

  return true;
};
