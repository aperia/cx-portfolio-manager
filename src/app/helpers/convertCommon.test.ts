import {
  convertNameToCode,
  convertRefValue,
  convertStringToNumberOnly
} from 'app/helpers/convertCommon';

describe('Test convertCommon.test.ts', () => {
  describe('convertStringToNumberOnly function ', () => {
    it('convert string to string only number ', () => {
      const value = 'test number 100%';

      const result = convertStringToNumberOnly(value);
      expect(result).toEqual(`100`);
    });
  });

  describe('convertNameToCode function ', () => {
    it('convert name to code', () => {
      const value = 'test number 100%';

      const result = convertNameToCode(value);
      expect(result).toEqual(`testnumber100`);
    });
  });

  describe('convertRefValue function ', () => {
    it('convert ref value with mapping empty', () => {
      const refData = [
        {
          code: 'code',
          text: 'text'
        }
      ];

      const result = convertRefValue(refData, 'code');
      expect(result).toEqual(`text`);
    });

    it('convert ref value with mapping not empty', () => {
      const refData = [
        {
          code: 'code',
          text: 'text'
        }
      ];

      const result = convertRefValue(refData, 'code', { text: 'text' });
      expect(result).toEqual(`text`);
    });
  });
});
