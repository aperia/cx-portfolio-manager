type Color =
  | 'green'
  | 'red'
  | 'cyan'
  | 'orange'
  | 'purple'
  | 'grey'
  | undefined;

function stringify(str: string) {
  return str.toLowerCase().replace(/ |-/g, '');
}

const COLORS = {
  cyan: ['Version Created'],
  orange: ['Strategy Modeled']
};

function findColor(st: string) {
  let result: Color;
  Object.entries(COLORS).forEach(([color, dict]) => {
    if (dict.map(stringify).includes(stringify(st))) {
      result = color as Color;
    }
  });
  return result;
}

export const getActionTakenStatusColor = (state: string) => findColor(state);
