import { ButtonActionType } from 'app/constants/button-action-types';
import {
  IconColors,
  IconNames,
  IconProps,
  TooltipProps
} from 'app/_libraries/_dls';

interface WorkflowCardIconValue {
  id?: string;
  actionName?: keyof typeof ButtonActionType;
  iconProps?: IconProps;
  tooltipProps?: TooltipProps;
}

export const getWorkflowCardIconValues = (
  value: IWorkflowTemplateModel
): WorkflowCardIconValue => {
  const { id, isFavorite, accesible } = value;
  const iconName: IconNames = !accesible
    ? ('lock' as any)
    : isFavorite
    ? 'star-fill'
    : 'rating-line';

  const iconColor = !accesible
    ? 'grey-l16'
    : isFavorite
    ? 'orange-d04'
    : 'grey-l16';

  const element = !accesible
    ? 'You do not have access.'
    : isFavorite
    ? 'Unfavorite Workflow'
    : 'Favorite Workflow';

  const variant = !accesible ? 'default' : 'primary';

  const iconProps: IconProps = {
    name: iconName,
    color: iconColor as IconColors
  };
  const tooltipProps: TooltipProps = { element, variant };

  const actionName = !accesible
    ? undefined
    : isFavorite
    ? 'unFavoriteWorkflow'
    : 'favoriteWorkflow';

  return {
    actionName,
    iconProps,
    tooltipProps,
    id: id
  };
};
