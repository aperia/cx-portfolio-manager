import { isBoolean } from 'lodash';

export const booleanToString = (booleanVal: any) => {
  if (isBoolean(booleanVal)) return booleanVal ? 'Yes' : 'No';

  return ['Yes', 'Y'].includes(booleanVal) ? 'Yes' : 'No';
};
