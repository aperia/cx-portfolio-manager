import { convertFilterArrayKeyToFilterArrayObject } from './convertFilterArrayKeyToFilterArrayObject';

describe('Test convertFilterArrayKeyToFilterArrayObject.test.ts', () => {
  describe('convertFilterArrayKeyToFilterArrayObject function ', () => {
    it('convertArray with multi filter ', () => {
      const mockArray = [
        { id: '1', name: 'testName', description: 'description' },
        { id: '2', name: 'testName2', description: 'description2' },
        { id: '3', name: 'testName3', description: 'description3' }
      ];

      const mockFilter = { id: ['1', '3'] };
      const result = convertFilterArrayKeyToFilterArrayObject(
        mockArray,
        mockFilter
      );
      expect(result).toEqual([
        { id: '1', name: 'testName', description: 'description' },
        { id: '3', name: 'testName3', description: 'description3' }
      ]);
    });

    it('convertArray with empty', () => {
      const mockArray = [] as any[];

      const mockFilter = { id: [] };
      const result = convertFilterArrayKeyToFilterArrayObject(
        mockArray,
        mockFilter
      );
      expect(result).toEqual([]);
    });
  });
});
