export const getChangeStatusColor = (
  state: string
): 'green' | 'red' | 'cyan' | 'orange' | 'purple' | 'grey' => {
  const status = (state || '').toLowerCase().replace(/ |-/g, '');

  if (status.includes('production') || status.includes('validated')) {
    return 'green';
  }

  if (status.includes('rejected') || status.includes('lapsed')) return 'red';
  if (
    status.includes('clientpendinglevel') ||
    status.includes('pendingapproval') ||
    status.includes('scheduled') ||
    status.includes('validating') ||
    status.includes('previous')
  )
    return 'orange';
  if (status.includes('work') || status.includes('demoted')) return 'cyan';

  return 'grey';
};
