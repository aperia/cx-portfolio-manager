import {
  canSetupWorkflow,
  getSetupWorkflowConfig
} from 'app/helpers/canSetupWorkflow';
import store from 'app/_libraries/_dof/core/redux/createAppStore';

jest.mock('app/_libraries/_dof/core/redux/createAppStore', () => ({
  __esModule: true,
  default: {
    dispatch: jest.fn(),
    getState: jest.fn(() => ({ mapping: null }))
  }
}));

describe('Test Helpers > canSetupWorkflow', () => {
  it('should return true in case workflow name match', () => {
    store.getState.mockImplementationOnce(() => ({
      mapping: {
        workflowData: {
          managepenaltyfeesworkflow: 'managepenaltyfeesworkflow'
        }
      }
    }));
    const result = canSetupWorkflow('Manage Penalty Fees Workflow');
    expect(result).toEqual(true);
  });

  it('should return false in case workflow name dont match', () => {
    store.getState.mockImplementationOnce(() => ({
      mapping: null
    }));
    const result = canSetupWorkflow('Mock workflow');
    expect(result).toEqual(false);
  });
});

describe('Test Helpers > getSetupWorkflowConfig', () => {
  it('should return true in case workflow name match', () => {
    store.getState.mockImplementationOnce(() => ({
      mapping: {
        workflowData: {
          managepenaltyfeesworkflow: 'managepenaltyfeesworkflow'
        }
      }
    }));
    const result = getSetupWorkflowConfig('Manage Penalty Fees Workflow');
    expect(!!result).toEqual(true);
  });

  it('should return false in case workflow name dont match', () => {
    store.getState.mockImplementationOnce(() => ({
      mapping: null
    }));
    const result = getSetupWorkflowConfig('Mock workflow');
    expect(result).toEqual(undefined);
  });
});
