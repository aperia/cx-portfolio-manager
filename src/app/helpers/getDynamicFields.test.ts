import { getDynamicFieldNames } from 'app/helpers/getDynamicFields';

describe('Test getDynamicFieldNames.test.ts', () => {
  it('getDynamicFieldNames correctly', () => {
    const value: any[] = [
      { dataField: [{ values: ['1234'], fieldName: 'field' }] }
    ];

    const result = getDynamicFieldNames(value, 'dataField');
    expect(result).toEqual(['field']);
  });
  it('getDynamicFieldNames empty result', () => {
    const value: any[] = [
      { dataField: [{ values: ['1234'], fieldName: 'field' }] }
    ];
    let result = getDynamicFieldNames(value, 'wrongName');
    expect(result).toEqual([]);

    result = getDynamicFieldNames(1234 as any, 'dataField');
    expect(result).toEqual([]);

    result = getDynamicFieldNames([] as any, 'dataField');
    expect(result).toEqual([]);

    result = getDynamicFieldNames(value, '');
    expect(result).toEqual([]);

    result = getDynamicFieldNames(
      [{ dataField: [{ values: [], fieldName: 'field' }] }],
      'dataField'
    );
    expect(result).toEqual([]);
  });
});
