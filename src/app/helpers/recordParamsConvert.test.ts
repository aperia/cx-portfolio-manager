import { RecordFieldParameterEnum } from 'app/constants/enums';
import {
  objectToRecordParams,
  recordParamsToObject
} from './recordParamsConvert';

describe('Test recordParamsConvert.test.ts', () => {
  it('recordParamsToObject', () => {
    const record = {
      id: 'id',
      parameters: [
        { name: 'name0', previousValue: '0', newValue: '1' },
        { name: 'name1', previousValue: '2', newValue: '3' }
      ]
    } as any;

    const resultNewValue = recordParamsToObject(record);
    expect(resultNewValue).toEqual({
      id: 'id',
      name0: '1',
      name1: '3'
    });

    const resultOldValue = recordParamsToObject(record, false);
    expect(resultOldValue).toEqual({
      id: 'id',
      name0: '0',
      name1: '2'
    });

    const resultNoParameters = recordParamsToObject({} as any, false);
    expect(resultNoParameters).toEqual({});
  });

  it('objectToRecordParams', () => {
    let selectProperties: any = undefined;
    let record: any = {};
    let initRecord: any = {};

    let result = objectToRecordParams(
      selectProperties as any,
      record,
      initRecord
    );
    expect(result).toEqual({});

    selectProperties = [RecordFieldParameterEnum.RecordType];
    record = {
      versionParameters: [
        {
          name: RecordFieldParameterEnum.RecordType,
          newValue: '1',
          previousValue: '0'
        },
        { name: 'name1', newValue: '3', previousValue: '2' }
      ]
    };
    initRecord = {
      [RecordFieldParameterEnum.RecordType]: 'prev'
    };

    result = objectToRecordParams(selectProperties as any, record, initRecord);
    expect(result).toEqual({
      parameters: [
        { name: 'record.type', newValue: '', previousValue: 'prev' }
      ],
      versionParameters: [
        { name: 'record.type', newValue: '1', previousValue: '0' },
        { name: 'name1', newValue: '3', previousValue: '2' }
      ]
    });
  });
});
