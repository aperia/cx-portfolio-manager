import { isIOS } from 'app/helpers/isIOS';

describe('Test isIOS.test.ts', () => {
  describe('isIOS function ', () => {
    let navigatorSpy: jest.SpyInstance;

    beforeEach(() => {
      navigatorSpy = jest.spyOn(global, 'navigator', 'get');
    });

    afterEach(() => {
      navigatorSpy.mockRestore();
    });

    it('should return true when platform is iPhone', () => {
      navigatorSpy.mockImplementation(() => ({
        platform: 'iPhone'
      }));

      expect(isIOS()).toEqual(true);
    });

    it('should return false when platform is not iPhone', () => {
      navigatorSpy.mockImplementation(() => ({
        platform: 'mock flatform',
        userAgent: 'mock agent'
      }));

      expect(isIOS()).toEqual(false);
    });

    it('should return true when userAgent is Mac', () => {
      navigatorSpy.mockImplementation(() => ({
        userAgent: 'Mac'
      }));
      document.ontouchend = () => {};

      expect(isIOS()).toEqual(true);
    });
  });
});
