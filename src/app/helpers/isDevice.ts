import { isIOS } from './isIOS';

const REGEX = /android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i;

export const isDevice =
  REGEX.test(navigator.userAgent.toLowerCase()) || isIOS();
