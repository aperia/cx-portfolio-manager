import { getChangeInTermsStatusColor } from 'app/helpers/getChangeInTermsStatusColor';

describe('Test getChangeInTermsStatusColor.test.ts', () => {
  describe('getChangeInTermsStatusColor function ', () => {
    it('should green when contains "completed"', () => {
      const value = 'Completed and some text';

      const result = getChangeInTermsStatusColor(value);
      expect(result).toEqual(`green`);
    });
    it('should orange when contains "notstarted"', () => {
      const value = 'Not started  and some text';

      const result = getChangeInTermsStatusColor(value);
      expect(result).toEqual(`orange`);
    });

    it('should grey as default', () => {
      const value = '';

      const result = getChangeInTermsStatusColor(value);
      expect(result).toEqual(`grey`);
    });
  });
});
