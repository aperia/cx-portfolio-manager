import store from 'app/_libraries/_dof/core/redux/createAppStore';
import { stepDefinition } from 'pages/_commons/WorkflowSetup/stepDefinition';
import { convertNameToCode } from './convertCommon';

const { getState } = store;

export const canSetupWorkflow = (workflowName: string) => {
  const { mapping } = getState() as RootState;
  const workflowMapping = mapping?.workflowData || {};

  const stepName = Object.keys(workflowMapping).find(
    k => workflowMapping[k] === convertNameToCode(workflowName)
  );
  if (!stepName) return false;

  return Object.keys(stepDefinition).some(k => k === stepName);
};

export const getSetupWorkflowConfig = (workflowName: string) => {
  const { mapping } = getState() as RootState;
  const workflowMapping = mapping?.workflowData || {};

  const stepName = Object.keys(workflowMapping).find(
    k => workflowMapping[k] === convertNameToCode(workflowName)
  );
  if (!stepName) return;

  return stepDefinition[stepName];
};
