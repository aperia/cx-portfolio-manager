const b64DecodeUnicode = (str: string) => {
  return decodeURIComponent(
    atob(str).replace(/(.)/g, function (m, p) {
      let code = p.charCodeAt(0).toString(16).toUpperCase();
      code = (code.length < 2 ? '0' : '') + code;
      return '%' + code;
    })
  );
};

const base64UrlDecode = (str: string) => {
  let output = str.replace(/-/g, '+').replace(/_/g, '/');
  switch (output.length % 4) {
    case 2:
      output += '==';
      break;
    case 3:
      output += '=';
      break;
    default:
      break;
  }
  return b64DecodeUnicode(output);
};

export const jwtDecode = (token: string, options?: any) => {
  if (!token) return {};
  const pos = options?.header === true ? 0 : 1;
  return JSON.parse(base64UrlDecode(token.split('.')[pos]));
};
