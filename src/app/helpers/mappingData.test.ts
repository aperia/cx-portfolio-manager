import {
  mappingDataAndDynamicFieldsFromArray,
  mappingDataFromArray,
  mappingDataFromObj
} from 'app/helpers/mappingData';

describe('Test mappingData.test.ts', () => {
  describe(' mappingDataFromObj function ', () => {
    it('value & json empty', () => {
      const value = '';
      const json = {};

      const result = mappingDataFromObj(value, json);
      expect(result).toEqual({});
    });

    it('mapping field name', () => {
      const value = {
        name: 'name 1',
        id: 'id 1'
      };
      const json = {
        nameKey: 'name',
        idKey: 'id'
      };

      const result = mappingDataFromObj(value, json);
      expect(result).toEqual({
        nameKey: value.name,
        idKey: value.id
      });
    });

    it('mapping field name with selectedFields', () => {
      const value = {
        name: 'name 1',
        id: 'id 1'
      };
      const json = {
        nameKey: 'name',
        idKey: 'id'
      };

      const result = mappingDataFromObj(value, json, ['nameKey']);
      expect(result).toEqual({
        dynamicFields: {
          nameKey: 'name 1'
        },
        idKey: 'id 1',
        nameKey: 'name 1'
      });
    });

    it('mappping with dynamic', () => {
      const value = {
        portfolioCriteria: [
          {
            fieldName: 'clientId',
            values: ['1234']
          }
        ]
      };
      const json = {
        name: {
          type: 'dynamic',
          containerProp: 'portfolioCriteria',
          fieldProp: 'clientId',
          mappingToType: 'string',
          key: 'fieldName',
          value: 'values'
        }
      };

      const result = mappingDataFromObj(value, json);
      expect(result).toEqual({
        name: '1234'
      });
    });

    it('mappping with dynamic but wrong value format', () => {
      const value = {
        portfolioCriteria: 1234
      };
      const json = {
        name: {
          type: 'dynamic',
          containerProp: 'portfolioCriteria',
          fieldProp: 'clientId',
          mappingToType: 'string',
          key: 'fieldName',
          value: 'values'
        }
      };

      const result = mappingDataFromObj(value, json);
      expect(result).toEqual({});
    });

    it('mappping with wrong mapping for dynamic mapping', () => {
      const value = {
        portfolioCriteria: [
          {
            fieldName: 'clientId',
            values: ['1234']
          }
        ]
      };
      const json = {
        name: {
          type: 'dynamic wrong',
          containerProp: 'portfolioCriteria',
          fieldProp: 'clientId',
          mappingToType: 'string',
          key: 'fieldName',
          value: 'values'
        }
      };

      const result = mappingDataFromObj(value, json);
      expect(result).toEqual({});
    });

    it('mappping with dynamic value', () => {
      const value = {
        portfolioCriteria: [
          {
            fieldName: 'clientId',
            values: '12345'
          }
        ]
      };
      const json = {
        name: {
          type: 'dynamic',
          containerProp: 'portfolioCriteria',
          fieldProp: 'clientId',
          key: 'fieldName',
          value: 'values'
        }
      };

      const result = mappingDataFromObj(value, json);
      expect(result).toEqual({
        name: '12345'
      });
    });

    it('mappping with dynamic array value', () => {
      const value = {
        portfolioCriteria: [
          {
            fieldName: 'clientId',
            values: ['1234', '12345']
          }
        ]
      };
      const json = {
        name: {
          type: 'dynamic',
          containerProp: 'portfolioCriteria',
          fieldProp: 'clientId',
          mappingToType: 'array',
          key: 'fieldName',
          value: 'values'
        }
      };

      const result = mappingDataFromObj(value, json);
      expect(result).toEqual({
        name: ['1234', '12345']
      });
    });

    it('mappping with deep array value', () => {
      const value = {
        dmmTables: [{ name: '1234' }, { name: '12345' }]
      };
      const json = {
        name: {
          type: 'mappingDataFromArray',
          containerProp: 'dmmTables',
          dmmTables: {
            name: 'name'
          }
        }
      };

      const result = mappingDataFromObj(value, json);
      expect(result).toEqual({
        name: [{ name: '1234' }, { name: '12345' }]
      });
    });
  });

  describe('mappingDataFromArray function ', () => {
    it('value is empty || json is empty', () => {
      const valueObj = {} as any[];
      const json = { name: 'name' };
      const jsonEmpty = {};

      const resultValueEmpty = mappingDataFromArray([], json);
      expect(resultValueEmpty).toEqual([]);

      const resultValueObj = mappingDataFromArray(valueObj, json);
      expect(resultValueObj).toEqual([]);

      const resultJsonEmpty = mappingDataFromArray(valueObj, jsonEmpty);
      expect(resultJsonEmpty).toEqual([]);
    });

    it('value is not empty && json is not empty', () => {
      const value = [1, 2, 3].map(e => ({ name: `name-${e}` }));
      const mapping = { nameKey: 'name' };

      const result = mappingDataFromArray(value, mapping);
      expect(result.length).toEqual(3);
      expect(result).toEqual([
        { nameKey: value[0].name },
        { nameKey: value[1].name },
        { nameKey: value[2].name }
      ]);
    });
  });

  describe('mappingDataAndDynamicFieldsFromArray function ', () => {
    it('json is empty', () => {
      const json = { name: 'name' };

      const resultValueEmpty = mappingDataAndDynamicFieldsFromArray(
        [],
        json,
        []
      );
      expect(resultValueEmpty).toEqual({});
    });

    it('value is not empty && json is not empty', () => {
      const value = [1, 2, 3].map(e => ({ name: `name-${e}` }));
      const mapping = { nameKey: 'name' };

      let result = mappingDataAndDynamicFieldsFromArray(value, mapping, []);
      expect(result.data?.length).toEqual(3);
      expect(result.data).toEqual([
        { nameKey: value[0].name },
        { nameKey: value[1].name },
        { nameKey: value[2].name }
      ]);

      const newValue = [
        {
          portfolioCriteria: [
            {
              fieldName: 'clientId',
              values: ['1234', '12345']
            }
          ]
        }
      ];
      const newMapping = {
        ...mapping,
        name: {
          type: 'dynamic',
          containerProp: 'portfolioCriteria',
          fieldProp: 'clientId',
          mappingToType: 'array',
          key: 'fieldName',
          value: 'values'
        }
      };
      result = mappingDataAndDynamicFieldsFromArray(newValue, newMapping, [
        'dynamic'
      ]);
      expect(result.data?.length).toEqual(1);
    });
  });
});
