import React from 'react';

// test
import '@testing-library/jest-dom';
import { fireEvent, render, RenderResult, act } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '../../test-utils/mocks/mockCanvas';
import {
  createMockMutationObserver,
  clearMockMutationObserver
} from '../../test-utils/mocks/mockMutationObserver';

// components
import Button from '../Button';
import ComboBox from './index';
import { ComboBoxProps } from './types';

const CBB_DATA = [
  {
    FieldID: 'ALL',
    FieldValue: 'All'
  },
  {
    FieldID: 'NER',
    FieldValue: 'NIGER'
  },
  {
    FieldID: 'GRL',
    FieldValue: 'GREENLAND'
  }
];

createMockMutationObserver([]);
afterAll(clearMockMutationObserver);

describe('Test ComboBox Component', () => {
  const cbbItems = () => {
    return CBB_DATA.map((item, index) => (
      <ComboBox.Item
        key={item.FieldID + index}
        label={item.FieldValue}
        value={item}
      />
    ));
  };

  const mockCbbProp = {
    id: 'cbbId',
    name: 'cbbName',
    textField: 'FieldValue',
    label: 'Mock ComboBox',
    placeholder: 'My ComboBox'
  };

  const renderCombobox = (
    props?: Omit<ComboBoxProps, 'children'>,
    renderChild?: Function
  ): RenderResult => {
    return render(
      <div>
        <ComboBox ref={() => {}} {...mockCbbProp} {...props}>
          {renderChild ? renderChild() : cbbItems()}
        </ComboBox>
        <Button autoBlur={false}>Test handleOnPopupClosed</Button>
      </div>
    );
  };

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('Check render with data', () => {
    it('Should render ComboBox UI with data', () => {
      jest.useFakeTimers();

      const focusFn = jest.fn();
      const changeFn = jest.fn();
      const wrapper = renderCombobox({
        onFocus: focusFn,
        onChange: changeFn
      });

      const { getByText } = wrapper;
      // Expect render Combobox Input with label
      expect(getByText(/Mock ComboBox/i)).toBeInTheDocument();

      const cbbIcon = wrapper.baseElement.querySelector(
        '.icon.icon-chevron-down'
      );
      // Mock click on chevron icon > input have no focus
      fireEvent.mouseDown(cbbIcon!);
      // Expect render Combobox Items
      expect(getByText(/NIGER/i)).toBeInTheDocument();
      expect(getByText(/GREENLAND/i)).toBeInTheDocument();

      act(() => {
        jest.runAllTimers();
      });

      expect(focusFn).toHaveBeenCalled();

      focusFn.mockClear();

      // Mock click on popup
      fireEvent.mouseDown(getByText(/GREENLAND/i).closest('.dls-popup')!);
      // Mock click on item
      fireEvent.click(getByText(/GREENLAND/i));

      expect(changeFn).toHaveBeenCalled();

      //Mock click on icon > input has focus
      const input: HTMLInputElement | null = wrapper.baseElement.querySelector(
        '.text-field-container input'
      );
      fireEvent.focus(input!);
      fireEvent.mouseDown(cbbIcon!);

      act(() => {
        jest.runAllTimers();
      });

      expect(focusFn).toHaveBeenCalled();
    });
    it('Should throw error when data is object and textField empty', () => {
      const error = new Error(
        'value is object so textField should be defined, please define your textField prop.'
      );

      const renderWrapper = () => {
        const wrapper = renderCombobox({
          textField: '',
          value: CBB_DATA[1],
          opened: true,
          readOnly: true
        });
        const input = wrapper.baseElement.querySelector(
          '.text-field-container input'
        );
        fireEvent.change(input!, { target: { value: 'green' } });
        return wrapper;
      };

      expect(renderWrapper).toThrow(error);
    });

    it('Should render ComboBox UI with data and error tooltip', () => {
      const wrapper = renderCombobox({
        error: {
          message: 'Mock error',
          status: true
        },
        readOnly: true,
        autoFocus: true
      });
      const { getByText } = wrapper;

      userEvent.click(document.querySelector('.text-field-container')!);

      // Expect render Combobox Input with label and error tooltip
      expect(getByText(/Mock ComboBox/i)).toBeInTheDocument();
      expect(getByText(/Mock error/i)).toBeInTheDocument();
    });

    it('Should render ComboBox UI with required', () => {
      const wrapper = renderCombobox({
        required: true,
        readOnly: false,
        disabled: false
      });
      const { getByText } = wrapper;

      // Expect render Combobox Input with label and error tooltip
      expect(getByText(/Mock ComboBox/i)).toBeInTheDocument();
    });

    it('Should render ComboBox UI with bubble', () => {
      renderCombobox({
        value: CBB_DATA[1],
        bubble: true,
        size: 'small'
      });

      // Expect render Combobox Input with label and error tooltip
      expect(document.querySelector('.dls-small')).toBeInTheDocument();
    });

    it('Should render ComboBox UI with loading', () => {
      renderCombobox({
        value: CBB_DATA[1],
        bubble: true,
        size: 'small',
        loading: true
      });

      // Expect render Combobox Input with label and error tooltip
      expect(document.querySelector('.dls-small')).toBeInTheDocument();
    });

    it('should floatingLabel if opened false and focus true', () => {
      const wrapper = renderCombobox({
        value: CBB_DATA[1],
        opened: false,
        error: {
          status: true
        }
      });
      const input = wrapper.baseElement.querySelector(
        '.text-field-container input'
      )!;
      fireEvent.focus(input);
      expect(true).toBeTruthy();
    });

    it('should preventDefault if enter on input filter', () => {
      const wrapper = renderCombobox({
        value: CBB_DATA[1]
      });
      const input = wrapper.baseElement.querySelector(
        '.text-field-container input'
      )!;
      fireEvent.submit(input);
      expect(true).toBeTruthy();
    });

    it('test on input filter change, with default obj value', () => {
      const filterChangeFn = jest.fn(),
        changeFn = jest.fn(),
        filter = 'green';
      const { baseElement } = renderCombobox(
        {
          value: CBB_DATA[1],
          onFilterChange: filterChangeFn,
          onChange: changeFn
        },
        () => {
          return CBB_DATA.map((item, index) => (
            <ComboBox.Item
              key={item.FieldID + index}
              label={item.FieldValue}
              value={item.FieldValue}
            />
          ));
        }
      );
      const input = baseElement.querySelector('.text-field-container input');
      fireEvent.change(input!, { target: { value: filter } });
      expect(filterChangeFn).toHaveBeenCalled();
      filterChangeFn.mockReset();
      fireEvent.change(input!, { target: { value: '' } });
      expect(changeFn).toHaveBeenCalled();
      changeFn.mockReset();
    });

    it('test on input filter change, with default obj value + group', () => {
      const filterChangeFn = jest.fn(),
        changeFn = jest.fn(),
        filter = 'NIGERR';
      const { baseElement } = renderCombobox(
        {
          value: CBB_DATA[1],
          onFilterChange: filterChangeFn,
          onChange: changeFn
        },
        () => {
          return (
            <ComboBox.Group key="ComboBoxGroup" label="ComboBoxGroup">
              {CBB_DATA.map((item, index) => (
                <ComboBox.Item
                  key={item.FieldID + index}
                  label={item.FieldValue}
                  value={item.FieldValue}
                />
              ))}
            </ComboBox.Group>
          );
        }
      );
      const input = baseElement.querySelector('.text-field-container input');
      fireEvent.change(input!, { target: { value: filter } });
      expect(filterChangeFn).toHaveBeenCalled();
      filterChangeFn.mockReset();
      fireEvent.change(input!, { target: { value: '' } });
      expect(changeFn).toHaveBeenCalled();
      changeFn.mockReset();
    });

    it('test on input filter change, with default obj value + group matched', () => {
      const filterChangeFn = jest.fn(),
        changeFn = jest.fn(),
        filter = 'NIGER';
      const { baseElement } = renderCombobox(
        {
          value: CBB_DATA[1],
          onFilterChange: filterChangeFn,
          onChange: changeFn
        },
        () => {
          return (
            <ComboBox.Group key="ComboBoxGroup" label="ComboBoxGroup">
              {CBB_DATA.map((item, index) => (
                <ComboBox.Item
                  key={item.FieldID + index}
                  label={item.FieldValue}
                  value={item.FieldValue}
                />
              ))}
            </ComboBox.Group>
          );
        }
      );
      const input = baseElement.querySelector('.text-field-container input');
      fireEvent.change(input!, { target: { value: filter } });
      expect(filterChangeFn).toHaveBeenCalled();
      filterChangeFn.mockReset();
      fireEvent.change(input!, { target: { value: '' } });
      expect(changeFn).toHaveBeenCalled();
      changeFn.mockReset();
    });

    it('test blur input with string value', () => {
      const blurFn = jest.fn(),
        filter = 'green';
      const wrapper = renderCombobox(
        {
          value: CBB_DATA[1].FieldValue,
          onBlur: blurFn
        },
        () => {
          return CBB_DATA.map((item, index) => (
            <ComboBox.Item
              key={item.FieldID + index}
              label={item.FieldValue}
              value={item.FieldValue}
            />
          ));
        }
      );

      const input = wrapper.baseElement.querySelector(
        '.text-field-container input'
      ) as HTMLInputElement;
      // Mock input focus in, then blur
      fireEvent.focus(input);
      expect(
        wrapper.baseElement.querySelector('.dls-popup')
      ).toBeInTheDocument();

      fireEvent.change(input, { target: { value: filter } });
      input.focus();
      fireEvent.blur(input);
      expect(blurFn).toHaveBeenCalled();
    });
    it('test blur input without value', () => {
      const blurFn = jest.fn(),
        visibilityChangeFn = jest.fn(),
        filterChangeFn = jest.fn(),
        filter = 'green';
      const wrapper = renderCombobox({
        opened: true,
        readOnly: true,
        onBlur: blurFn,
        onVisibilityChange: visibilityChangeFn,
        onFilterChange: filterChangeFn
      });

      const input = wrapper.baseElement.querySelector(
        '.text-field-container input'
      );
      // Mock input focus in, then blur
      fireEvent.change(input!, { target: { value: filter } });
      fireEvent.blur(input!);
      expect(blurFn).toHaveBeenCalled();
      expect(filterChangeFn).toHaveBeenCalled();

      fireEvent.change(input!, { target: { value: '' } });
      fireEvent.blur(input!);
      expect(visibilityChangeFn).toHaveBeenCalled();
    });
    it('Should render disabled ComboBox UI', () => {
      const focusFn = jest.fn();
      const wrapper = renderCombobox({
        disabled: true,
        onFocus: focusFn
      });

      const { queryByText } = wrapper;
      // Expect render Combobox Input with label
      expect(queryByText(/Mock ComboBox/i)).toBeInTheDocument();

      const cbbIcon = wrapper.baseElement.querySelector(
        '.icon.icon-chevron-down'
      );
      // Mock click on chevron icon,nothing happen
      fireEvent.mouseDown(cbbIcon!);
      expect(focusFn).not.toHaveBeenCalled();
    });
    it('Should render ComboBox UI with autofocus', () => {
      const focusFn = jest.fn();
      const wrapper = renderCombobox({
        autoFocus: true,
        readOnly: true,
        onFocus: focusFn
      });

      const { queryByText } = wrapper;
      // Expect render Combobox Input with label, and autofocus
      expect(queryByText(/Mock ComboBox/i)).toBeInTheDocument();
      expect(focusFn).toHaveBeenCalled();
    });
    it('should cover handlePopupClosed and apply next tabIndex is fakeFocusRef.current', () => {
      jest.useFakeTimers();
      const { container, queryByText } = renderCombobox({});

      const inputElement = container.querySelector(
        '.text-field-container input'
      )!;

      const buttonElement = queryByText('Test handleOnPopupClosed')!;
      userEvent.click(inputElement);
      jest.runAllTimers();

      userEvent.click(document.body);
      jest.runAllTimers();

      userEvent.click(inputElement);
      jest.runAllTimers();

      userEvent.click(buttonElement);
      jest.runAllTimers();

      expect(true).toBeTruthy();
    });
  });
});
