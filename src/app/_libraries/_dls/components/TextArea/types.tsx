import { TextareaHTMLAttributes } from 'react';
import { TooltipProps } from '../../components/Tooltip';

export interface TextAreaProps
  extends TextareaHTMLAttributes<HTMLTextAreaElement>,
    DLSId {
  label?: string;

  error?: {
    status: boolean;
    message: string;
  };
  errorTooltipProps?: TooltipProps;
}
