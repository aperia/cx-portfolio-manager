import React, { useContext } from 'react';

// components
import Button, { ButtonProps } from '../Button';

// utils
import classnames from 'classnames';
import { genAmtId } from '../../utils';

export interface GroupButtonProps extends DLSId {
  className?: string;
  children?:
    | React.ReactElement<ButtonProps, typeof Button>
    | React.ReactElement<ButtonProps, typeof Button>[];
}

const GroupButtonContext = React.createContext(false);

const useIsGroupButton = () => useContext(GroupButtonContext);

const GroupButton: React.FC<GroupButtonProps> = ({
  className,
  children,

  id,
  dataTestId
}) => {
  return (
    <GroupButtonContext.Provider value>
      <div
        className={classnames('dls-group-button', className)}
        id={id}
        data-testid={genAmtId(dataTestId, 'dls-group-button', 'GroupButton')}
      >
        {children}
      </div>
    </GroupButtonContext.Provider>
  );
};

export { useIsGroupButton, GroupButtonContext };
export default GroupButton;
