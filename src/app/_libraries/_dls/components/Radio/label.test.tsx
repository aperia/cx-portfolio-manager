import React from 'react';
import Label from './Label';
import { queryByText, render } from '@testing-library/react';
import '@testing-library/jest-dom';
import { queryBy } from '../../test-utils/queryHelpers';
describe('Test Label Radio Button', () => {
  it('Should have children', () => {
    const text = 'children';
    const { container } = render(<Label>{text}</Label>);
    const queryClass = queryBy(
      container,
      'class',
      'custom-control-label dls-radio-label'
    );
    const queryText = queryByText(container, text);
    expect(queryText?.textContent).toEqual(text);
    expect(queryClass).toBeInTheDocument();
  });

  it('Should haven"t children', () => {
    const { container } = render(<Label></Label>);
    const queryClass = queryBy(
      container,
      'class',
      'custom-control-label dls-radio-label'
    );
    expect(queryClass?.textContent).toEqual('');
    expect(queryClass).toBeInTheDocument();
  });

  it('Should haven classProps', () => {
    const className = 'className';
    const { container } = render(<Label className={className} />);
    const queryClass = queryBy(
      container,
      'class',
      `custom-control-label dls-radio-label ${className}`
    );
    expect(queryClass).toBeInTheDocument();
  });
});
