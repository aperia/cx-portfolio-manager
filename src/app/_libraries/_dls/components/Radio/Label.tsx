import classNames from 'classnames';
import { className } from '../../utils';
import React, { DetailedHTMLProps, LabelHTMLAttributes } from 'react';

export interface LabelProps
  extends React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLLabelElement>,
    HTMLLabelElement
  > {}

const Label: React.RefForwardingComponent<
  HTMLLabelElement,
  DetailedHTMLProps<LabelHTMLAttributes<HTMLLabelElement>, HTMLLabelElement>
> = ({ className: labelClassName, children, ...props }, ref) => {
  return (
    <label
      ref={ref}
      className={classNames(className.radio.LABEL, labelClassName)}
      {...props}
    >
      {children}
    </label>
  );
};

export default React.forwardRef<
  HTMLLabelElement,
  DetailedHTMLProps<LabelHTMLAttributes<HTMLLabelElement>, HTMLLabelElement>
>(Label);
