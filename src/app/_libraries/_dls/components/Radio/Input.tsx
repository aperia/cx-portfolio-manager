import React, { useEffect, useImperativeHandle, useRef, useState } from 'react';
import isBoolean from 'lodash.isboolean';
import isFunction from 'lodash.isfunction';
import classNames from 'classnames';
import { className } from '../../utils';

export interface InputProps
  extends React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  > {
  label?: string;
  indeterminate?: boolean;
  readOnly?: boolean;
  containerClassName?: string;
  inputClassName?: string;
  labelClassName?: string;
  disabled?: boolean;
  checked?: boolean;
  id?: string;
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const Input: React.ForwardRefRenderFunction<HTMLInputElement, InputProps> = (
  {
    onChange: onChangeProp,
    id,
    indeterminate,
    readOnly,
    disabled,
    checked: checkedProp,
    className: inputClassName,
    ...props
  },
  reference
) => {
  const ref = useRef<HTMLInputElement | null>(null);
  useImperativeHandle(reference, () => ref.current!);

  const [checked, setChecked] = useState(isBoolean(checkedProp) && checkedProp);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (readOnly) return;

    isFunction(onChangeProp) && onChangeProp(event);

    // uncontrolled
    !isBoolean(checkedProp) && setChecked((state) => !state);
  };

  useEffect(() => {
    if (!isBoolean(checkedProp)) return;

    setChecked(checkedProp);
  }, [checkedProp]);

  return (
    <input
      type="radio"
      id={id}
      disabled={disabled}
      readOnly={readOnly}
      className={classNames(
        className.radio.INPUT,
        { indeterminate },
        inputClassName
      )}
      checked={indeterminate ? false : checked}
      onChange={handleChange}
      ref={ref}
      {...props}
    />
  );
};

export default React.forwardRef<HTMLInputElement, InputProps>(Input);
