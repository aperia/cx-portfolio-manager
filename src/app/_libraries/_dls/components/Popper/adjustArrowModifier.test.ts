import adjustArrowModifier from './adjustArrowModifier';

describe('test error case', () => {
  it('should run with error case', () => {
    global.getComputedStyle = () => ({});
    global.DOMMatrix = class DOMMatrix {
      constructor() {}
    };

    const state1 = {
      placement: 'top-start',
      elements: {
        popper: {
          classList: {
            contains: () => true
          }
        },
        arrow: {
          style: {}
        }
      }
    };
    const stateResult1 = adjustArrowModifier.fn({ state: state1 });
    expect(stateResult1).not.toBeUndefined();

    const state2 = {
      ...state1,
      placement: 'top-end'
    };
    const stateResult2 = adjustArrowModifier.fn({ state: state2 });
    expect(stateResult2).not.toBeUndefined();
  });
});

describe('test top/bottom case', () => {
  it('should run with arrowX < distanceX', () => {
    global.getComputedStyle = () => ({});
    global.DOMMatrix = class DOMMatrix {
      constructor() {
        this.m41 = 0;
      }
    };

    const state1 = {
      placement: 'top',
      elements: {
        popper: {
          classList: {
            contains: () => false
          },
          style: {}
        },
        arrow: {
          style: {}
        },
        reference: {
          getBoundingClientRect: () => ({
            width: 0,
            offset: 0
          })
        }
      },
      rects: {
        popper: {},
        arrow: {}
      },
      modifiersData: {}
    };
    const stateResult1 = adjustArrowModifier.fn({ state: state1 });
    expect(stateResult1).not.toBeUndefined();

    global.DOMMatrix = class DOMMatrix {
      constructor() {
        this.m41 = 1;
      }
    };
    const stateResult2 = adjustArrowModifier.fn({ state: state1 });
    expect(stateResult2).not.toBeUndefined();
  });

  it('should run with widthPopper - arrowX < distanceX', () => {
    global.getComputedStyle = () => ({});
    global.DOMMatrix = class DOMMatrix {
      constructor() {
        this.m41 = 23;
      }
    };
    const state1 = {
      placement: 'top',
      elements: {
        popper: {
          classList: {
            contains: () => false
          },
          style: {}
        },
        arrow: {
          style: {}
        },
        reference: {
          getBoundingClientRect: () => ({
            width: 0,
            offset: 0
          })
        }
      },
      rects: {
        popper: {},
        arrow: {}
      },
      modifiersData: {}
    };
    const stateResult1 = adjustArrowModifier.fn({ state: state1 });
    expect(stateResult1).not.toBeUndefined();

    const state2 = {
      ...state1,
      elements: {
        popper: {
          classList: {
            contains: () => false
          },
          style: {}
        },
        arrow: {
          style: {}
        },
        reference: {
          getBoundingClientRect: () => ({
            width: 0,
            offset: 0
          })
        }
      },
      rects: {
        popper: {
          width: 26
        },
        arrow: {}
      },
      modifiersData: {}
    };
    const stateResult2 = adjustArrowModifier.fn({ state: state2 });
    expect(stateResult2).not.toBeUndefined();

    const state3 = {
      ...state1,
      elements: {
        popper: {
          classList: {
            contains: () => false
          },
          style: {}
        },
        arrow: {
          style: {}
        },
        reference: {
          getBoundingClientRect: () => ({
            width: 0,
            offset: 0
          })
        }
      },
      rects: {
        popper: {
          width: 260
        },
        arrow: {}
      },
      modifiersData: {}
    };
    const stateResult3 = adjustArrowModifier.fn({ state: state3 });
    expect(stateResult3).not.toBeUndefined();
  });
});

describe('test right/left case', () => {
  it('should run with arrowY < distanceY', () => {
    global.getComputedStyle = () => ({});
    global.DOMMatrix = class DOMMatrix {
      constructor() {
        this.m42 = 0;
      }
    };

    const state1 = {
      placement: 'right',
      elements: {
        popper: {
          classList: {
            contains: () => false
          },
          style: {}
        },
        arrow: {
          style: {}
        },
        reference: {
          getBoundingClientRect: () => ({
            width: 0,
            offset: 0
          })
        }
      },
      rects: {
        popper: {},
        arrow: {}
      },
      modifiersData: {}
    };
    const stateResult1 = adjustArrowModifier.fn({ state: state1 });
    expect(stateResult1).not.toBeUndefined();

    global.DOMMatrix = class DOMMatrix {
      constructor() {
        this.m42 = 1;
      }
    };
    const stateResult2 = adjustArrowModifier.fn({ state: state1 });
    expect(stateResult2).not.toBeUndefined();
  });

  it('should run with heightPopper - arrowY < distanceY', () => {
    global.getComputedStyle = () => ({});
    global.DOMMatrix = class DOMMatrix {
      constructor() {
        this.m42 = 23;
      }
    };
    const state1 = {
      placement: 'right',
      elements: {
        popper: {
          classList: {
            contains: () => false
          },
          style: {}
        },
        arrow: {
          style: {}
        },
        reference: {
          getBoundingClientRect: () => ({
            width: 0,
            offset: 0
          })
        }
      },
      rects: {
        popper: {},
        arrow: {}
      },
      modifiersData: {}
    };
    const stateResult1 = adjustArrowModifier.fn({ state: state1 });
    expect(stateResult1).not.toBeUndefined();

    const state2 = {
      ...state1,
      elements: {
        popper: {
          classList: {
            contains: () => false
          },
          style: {}
        },
        arrow: {
          style: {}
        },
        reference: {
          getBoundingClientRect: () => ({
            width: 0,
            offset: 0
          })
        }
      },
      rects: {
        popper: {
          height: 26
        },
        arrow: {}
      },
      modifiersData: {}
    };
    const stateResult2 = adjustArrowModifier.fn({ state: state2 });
    expect(stateResult2).not.toBeUndefined();

    const state3 = {
      ...state1,
      elements: {
        popper: {
          classList: {
            contains: () => false
          },
          style: {}
        },
        arrow: {
          style: {}
        },
        reference: {
          getBoundingClientRect: () => ({
            width: 0,
            offset: 0
          })
        }
      },
      rects: {
        popper: {
          height: 260
        },
        arrow: {}
      },
      modifiersData: {}
    };
    const stateResult3 = adjustArrowModifier.fn({ state: state3 });
    expect(stateResult3).not.toBeUndefined();
  });
});
