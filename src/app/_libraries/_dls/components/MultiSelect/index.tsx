import React, { useRef, useState, useEffect, useImperativeHandle } from 'react';

// components/types
import DropdownBase, {
  DropdownBaseChangeEvent,
  DropdownBaseRef,
  DropdownBaseGroup,
  DropdownBaseItem,
  HighLightWordsContextType
} from '../DropdownBase';
import PopupBase, { PopupBaseRef } from '../PopupBase';
import Tooltip from '../Tooltip';
import { MultiSelectRef, MultiSelectProps } from './types';

import { HighLightWordsContext } from '../DropdownBase';

// utils
import classnames from 'classnames';
import { classes, className, extendsEvent, genAmtId } from '../../utils';
import Group from './Group';
import Normal from './Normal';
import { isBoolean, isFunction, get, pick, isEmpty } from '../../lodash';

const MultiSelect: React.ForwardRefRenderFunction<
  MultiSelectRef,
  MultiSelectProps
> = (
  {
    value: valueProp,
    onChange,

    label,
    textField,

    onFocus,
    onBlur,
    opened: openedProp,
    onVisibilityChange,

    onFilterChange,
    maxLengthFilter,

    // Should show checkAll options or not
    checkAll,
    // Edit checkAll text
    checkAllLabel,
    // Should show searchBar or not
    searchBar,
    // Edit placeholder of the searchBar
    searchBarPlaceholder = 'Type to search',

    readOnly,
    disabled,
    placeholder,

    id,
    name,
    required,
    error,
    errorTooltipProps,

    variant,

    popupBaseProps,
    autoFocus = false,
    children: childrenProp,

    groupFormatInputText,

    dataTestId,
    ...props
  },
  ref
) => {
  // refs
  const coreRef = useRef<Record<string, any>>({});
  const popupBaseRef = useRef<PopupBaseRef | null>(null);
  const fakeFocusRef = useRef<HTMLDivElement | null>(null);

  // states
  const [dropdownBase, setDropdownBase] = useState<DropdownBaseRef>();
  const [containerElement, setContainerElement] =
    useState<HTMLDivElement | null>(null);
  const [opened, setOpened] = useState(!!openedProp);
  const [value, setValue] = useState(valueProp);
  const [filter, setFilter] = useState('');
  const [children, setChildren] = useState(childrenProp);
  const [highLightWordsContextValue, setHighLightWordsContextValue] =
    useState<HighLightWordsContextType>({
      highlightTextFilter: '',
      setHighlightTextFilter: (nextHighLightTextFilter) =>
        setHighLightWordsContextValue((state) => ({
          ...state,
          highlightTextFilter: nextHighLightTextFilter
        }))
    });

  // handle value uncontrolled/controlled
  const handleValueMode = (event: DropdownBaseChangeEvent) => {
    if (readOnly || disabled) return;

    onChange?.(event);

    // controlled
    if (valueProp) return;

    // uncontrolled
    setValue(event.target.value);
  };

  // handle opened uncontrolled/controlled
  const handleOpenedMode = (nextOpened: boolean) => {
    isFunction(onVisibilityChange) && onVisibilityChange(nextOpened);

    // controlled
    if (isBoolean(openedProp)) return;

    // uncontrolled
    setOpened(nextOpened);
  };

  // handle filter uncontrolled/controlled
  // for now, we removed controlled mode, just support uncontrolled mode
  const handleFilterMode = (nextFilter: string) => {
    if (isFunction(onFilterChange)) {
      let nextEvent = extendsEvent({} as any, 'target', {
        id,
        name,
        value: nextFilter
      });
      nextEvent = extendsEvent(nextEvent, null, { value: nextFilter });
      onFilterChange(nextEvent);
    }

    // uncontrolled (remaining code below)
    setFilter(nextFilter);

    // update current text filter to Context
    const { setHighlightTextFilter } = highLightWordsContextValue;
    setHighlightTextFilter!(nextFilter);

    const nextChildren = filterChildren(nextFilter);
    setChildren(nextChildren);
  };

  // basic filter algorithm
  const isMatched = (text: string, pattern: string) => {
    return text.toLowerCase().indexOf(pattern.toLowerCase()) !== -1;
  };

  // handle filter children
  // input: current filter
  // output: list child match with current filter
  const filterChildren = (nextFilter: string) => {
    const collector: any[] = [];

    React.Children.forEach(childrenProp, (child) => {
      const isGroup = get(child, 'type') === DropdownBaseGroup;

      if (isGroup) {
        const childrenInGroup = get(child, ['props', 'children']) as unknown[];
        const childrenMatchedInGroup = childrenInGroup.filter((element) => {
          const labelFilter = get(element, ['props', 'label']) as string;
          return isMatched(labelFilter, nextFilter);
        });

        if (childrenMatchedInGroup.length) {
          const nextChildProps = {
            ...get(child, 'props'),
            children: childrenMatchedInGroup
          };
          const childCloned = React.cloneElement(child as any, nextChildProps);
          collector.push(childCloned);
        }

        return;
      }

      const labelFilter = get(child, ['props', 'label']) as string;
      if (isMatched(labelFilter, nextFilter)) collector.push(child);
    });

    return collector as typeof children;
  };

  // handle onChange
  const handleOnChange = handleValueMode;

  // if the popup shown, we want the input element to stay in focus even if we
  // click on the popup, read more details in side this function
  const handleOnPopupShown = () => {
    const popupElement = popupBaseRef.current?.element;

    // handle mousedown on Popup's MultiSelect
    // mousedown on Popup's MultiSelect will trigger onBlur default of the MultiSelect
    // that is not the behavior we want, so we need to preventDefault it
    const handleMouseDown = (event: MouseEvent) => {
      // if searchBar is exists, searchBar will take care preventDefault on popupElement
      if (searchBar) return;

      // stop browser events
      event.preventDefault();
    };

    // no need to remove event later, because popup element will be remove from DOM
    popupElement?.addEventListener('mousedown', handleMouseDown);
  };

  coreRef.current.handleFilterMode = handleFilterMode;

  // NEED REFACTOR
  // handle clear timeout
  const timeoutIdRef = useRef<any>();
  const handleOnPopupClosed = () => {
    if (document?.activeElement === document?.body) {
      fakeFocusRef.current?.focus({ preventScroll: true });
    }

    if (filter && variant === 'group') {
      clearTimeout(timeoutIdRef.current);

      timeoutIdRef.current = setTimeout(() => {
        handleFilterMode('');
      }, 300);
    }
  };
  useEffect(() => {
    return () => clearTimeout(timeoutIdRef.current);
  }, []);

  // provide ref
  const publicRef = useRef<MultiSelectRef | null>({});
  useImperativeHandle(ref, () => {
    publicRef.current!.popupBaseRef = popupBaseRef;
    publicRef.current!.dropdownBaseRef = {
      current: dropdownBase!
    };

    return publicRef.current as MultiSelectRef;
  });

  // set props
  useEffect(() => setOpened(!!openedProp), [openedProp]);
  useEffect(() => setValue(valueProp), [valueProp]);
  useEffect(() => setChildren(childrenProp), [childrenProp]);

  // handle clear filter after value changed
  useEffect(() => {
    if (variant === 'group') return;

    coreRef.current.handleFilterMode('');
  }, [value, variant]);

  // check if variant is group or not
  const group = variant === 'group';

  // check if variant is no-border or not
  const noBorder = variant === 'no-border';

  // check if variant is normal
  const normal = !group && !noBorder;

  // check required
  const isRequired = required && !readOnly && !disabled && !noBorder;

  // check should show error or not
  // if variant is noBorder, we will ignore error
  const { message, status } = pick(error, 'message', 'status');
  const isError = status && !noBorder;
  const isOpenedTooltip = Boolean(isError && message && opened);

  // check should floating label or not
  const floating = (label && opened && !readOnly) || !isEmpty(value) || filter;

  // check no label
  const noLabel = !label;

  const { popupBaseClassName, ...popupProps } = popupBaseProps || {};

  return (
    <>
      <Tooltip
        opened={isOpenedTooltip}
        element={message}
        variant="error"
        placement="top-start"
        triggerClassName="d-block"
        dataTestId={`${dataTestId}-multi-select-error`}
        {...errorTooltipProps}
      >
        <div
          ref={setContainerElement}
          className={classnames({
            [classes.multiSelect.container]: true,
            [classes.multiSelect.variantGroup]: group,
            [classes.multiSelect.variantNoBorder]: noBorder,
            [classes.multiSelect.variantNormal]: normal,
            [classes.state.focused]: opened,
            [classes.state.readOnly]: readOnly,
            [classes.state.disabled]: disabled,
            [classes.state.error]: isError,
            [classes.state.floating]: floating,
            [classes.state.noLabel]: noLabel
          })}
          data-testid={genAmtId(dataTestId, 'dls-multiselect', 'MultiSelect')}
        >
          {group || noBorder ? (
            <Group
              label={label}
              placeholder={placeholder}
              value={value}
              textField={textField}
              disabled={disabled}
              required={isRequired}
              id={id}
              name={name}
              error={error}
              onFocus={onFocus}
              onBlur={onBlur}
              handleOpenedMode={handleOpenedMode}
              opened={opened}
              variant={variant}
              baseChildren={childrenProp}
              replacer={dropdownBase?.searchBarElement}
              autoFocus={autoFocus}
              openedControlled={isBoolean(openedProp)}
              groupFormatInputText={groupFormatInputText}
            >
              {children}
            </Group>
          ) : (
            <Normal
              label={label}
              placeholder={placeholder}
              readOnly={readOnly}
              disabled={disabled}
              required={isRequired}
              error={error}
              id={id}
              name={name}
              onFocus={onFocus}
              onBlur={onBlur}
              textField={textField}
              opened={opened}
              handleOpenedMode={handleOpenedMode}
              value={value}
              handleValueMode={handleValueMode}
              filter={filter}
              handleFilterMode={handleFilterMode}
            >
              {children}
            </Normal>
          )}
          <div ref={fakeFocusRef} tabIndex={-1} />
        </div>
      </Tooltip>
      <PopupBase
        ref={popupBaseRef}
        reference={containerElement!}
        opened={opened && !readOnly && !disabled}
        onVisibilityChange={handleOpenedMode}
        onPopupShown={handleOnPopupShown}
        onPopupClosed={handleOnPopupClosed}
        popupBaseClassName={classnames(
          className.popupBase.CONTAINER,
          popupBaseClassName
        )}
        fluid={noBorder ? false : true}
        dataTestId={`${dataTestId}-popup-multi-select`}
        {...popupProps}
      >
        <HighLightWordsContext.Provider value={highLightWordsContextValue}>
          <DropdownBase
            ref={setDropdownBase as any}
            allowKeydown={opened}
            value={value}
            onChange={handleOnChange}
            valueType="multiple"
            highlightTextItem
            id={id}
            name={name}
            //
            checkAll={checkAll}
            checkAllLabel={checkAllLabel}
            searchBar={searchBar}
            searchBarPlaceholder={searchBarPlaceholder}
            filter={filter}
            onFilter={handleFilterMode}
            maxLengthFilter={maxLengthFilter}
            baseChildren={childrenProp}
            onFocus={onFocus}
            onBlur={onBlur}
            handleOpenedMode={handleOpenedMode}
            dataTestId={dataTestId}
            {...props}
          >
            {children}
          </DropdownBase>
        </HighLightWordsContext.Provider>
      </PopupBase>
    </>
  );
};

const MultiSelectExtraStaticProp = React.forwardRef<
  MultiSelectRef,
  MultiSelectProps
>(MultiSelect) as React.ForwardRefExoticComponent<
  MultiSelectProps & React.RefAttributes<MultiSelectRef>
> & {
  Group: typeof DropdownBaseGroup;
  Item: typeof DropdownBaseItem;
};

MultiSelectExtraStaticProp.Group = DropdownBaseGroup;
MultiSelectExtraStaticProp.Item = DropdownBaseItem;

export * from './types';
export default MultiSelectExtraStaticProp;
