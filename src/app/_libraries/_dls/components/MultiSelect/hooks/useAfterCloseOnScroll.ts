import { useEffect, useRef } from 'react';
import { MultiSelectProps } from '../types';

export interface AfterCloseOnScrollHookArgs {
  opened?: MultiSelectProps['opened'];
  blurElement?: HTMLElement;
}

export interface AfterCloseOnScrollHook {
  (args: AfterCloseOnScrollHookArgs): void;
}

const useAfterCloseOnScroll: AfterCloseOnScrollHook = ({
  opened,
  blurElement
}) => {
  const blueElementRef = useRef<HTMLElement | null>(null);
  blueElementRef.current = blurElement!;

  useEffect(() => {
    if (opened) return;

    if (
      blueElementRef.current &&
      blueElementRef.current === document.activeElement
    ) {
      blueElementRef.current.blur();
    }
  }, [opened]);
};

export default useAfterCloseOnScroll;
