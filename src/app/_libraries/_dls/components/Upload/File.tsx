import React, {
  ReactText,
  useLayoutEffect,
  useMemo,
  useRef,
  useState
} from 'react';

// helpers
import ResizeObserver from 'resize-observer-polyfill';
import classNames from 'classnames';
import { useTranslation } from '../../hooks';
import { canvasTextWidth, genAmtId, getAvailableWidth } from '../../utils';
import {
  formatBytes,
  getFileExtension,
  getValid,
  STATUS,
  MAX_FILE,
  MIN_FILE,
  getAllWidthChildren
} from './helper';

// components
import Icon from '../Icon';
import Tooltip from '../Tooltip';
import Button from '../Button';
import Message from './Messages';

export interface StateFile extends FileList {
  idx: string;
  percentage?: number;
  status?: number;
  txtName?: string;
  txtDescription?: string;
  name?: string;
  size?: number;
  lastModified?: string;
  type?: string;
}

export interface Props extends DLSId {
  className?: string;
  file: StateFile;
  index: number;
  isEditFileDescription?: boolean;
  isEditFileName?: boolean;
  maxFile?: number;
  minFile?: number;
  typeFiles?: string[];
  isCancelUploadingFile?: boolean;
  onDownloadFile?: (index: number, idx: ReactText) => void;
  renderDownloadFile?: (params: {
    index: number;
    file: StateFile;
    elmDefault: any;
  }) => any;
  onRemoveFile: (index: number, idx: ReactText) => void;
}

const FileOnlyView: React.FC<Props> = ({
  className,
  file,
  index,
  onRemoveFile,
  onDownloadFile,
  renderDownloadFile,
  typeFiles,
  isCancelUploadingFile,
  // isEditFileDescription,
  isEditFileName,
  maxFile = MAX_FILE,
  minFile = MIN_FILE,

  dataTestId
}) => {
  const {
    idx,
    name,
    size,
    lastModified,
    percentage,
    status,
    txtName
    // txtDescription
  } = file;

  const { t } = useTranslation();

  const fileRef = useRef<HTMLDivElement | null>(null);
  const containerRef = useRef<HTMLDivElement | null>(null);
  const iconRef = useRef<HTMLDivElement | null>(null);
  const fileMessageRef = useRef<HTMLDivElement | null>(null);
  const fileSizeRef = useRef<HTMLDivElement | null>(null);

  const [fileNameRef, setFileNameRef] = useState<HTMLDivElement | null>(null);
  const [isTruncate, setIsTruncate] = useState<boolean>();
  const [isOneLine, setIsOneLine] = useState<boolean>();

  const fileExtension = getFileExtension(name);
  const valid = getValid(file, { typeFiles, maxFile, minFile });

  useLayoutEffect(() => {
    const resizeObserver = new ResizeObserver(() => {
      process.nextTick(() => {
        const widthParent = fileRef.current?.parentElement?.clientWidth;
        const widthFile = fileRef.current?.clientWidth;
        const isOneLine = widthFile === widthParent;

        setIsOneLine(isOneLine);
        const isDefault =
          fileRef.current?.parentElement?.classList.contains('default');

        if (isOneLine && !isDefault) {
          fileRef.current?.parentElement?.classList.add('default');
        }
        // ~ else if
        if (!(isOneLine && !isDefault) && !isOneLine) {
          fileRef.current?.parentElement?.classList.remove('default');
        }

        if (!fileNameRef) return;

        const widthContainer = getAvailableWidth(fileNameRef);
        const widthMessage = getAllWidthChildren(fileMessageRef.current);

        const computedStyles = window.getComputedStyle(fileNameRef);
        const textMetrics = canvasTextWidth(name!, computedStyles.font);

        const widthName = textMetrics?.width || 0;

        const isTruncate = widthContainer - widthName - widthMessage - 32 < 0;

        setIsTruncate(isTruncate);
      });
    });
    resizeObserver.observe(containerRef.current!);

    // clear ResizeObserver
    return () => resizeObserver.disconnect();
  }, [fileNameRef, name]);

  const isValid = status === STATUS.valid;
  const isUpload = status === STATUS.uploading;
  const isDone = status === STATUS.success;
  const isUploadFailed = status === STATUS.error;
  const isError = isUploadFailed || valid.isInvalid;

  const tooltipBtnRemoveFile = t('Remove file');
  const tooltipBtnDownloadFile = t('Download');
  const { isTypeSupport } = valid;
  const hasName =
    isEditFileName && isTypeSupport && !isUpload && !isDone && !isError;

  const placeholderName = t('Type to add name');

  const isDisplayDeleteBtn =
    (!isDone && !isUpload) || (isUpload && !!isCancelUploadingFile);

  const messageCpm = useMemo(() => {
    const statusMsg = {
      isDone,
      isUpload,
      isUploadFailed,
      maxFile,
      minFile
    };

    return (
      <div
        className={classNames('file-message', {
          error: isError,
          uploading: isUpload
        })}
        ref={fileMessageRef}
      >
        <Message valid={valid} status={statusMsg} isError={isError} />
      </div>
    );
  }, [isDone, isError, isUpload, isUploadFailed, maxFile, minFile, valid]);

  const renderBtnDownloadFile = () => {
    if (!onDownloadFile) return;

    const elmDefault = (
      <Tooltip
        triggerClassName="dls-upload-file-btn-upload"
        placement="top"
        variant="primary"
        element={tooltipBtnDownloadFile}
        data-testid={genAmtId(
          `${dataTestId}-${name}`,
          'dls-upload-file-name',
          'Upload.File.name'
        )}
      >
        <Button
          size="sm"
          variant="icon-secondary"
          onClick={() => onDownloadFile(index, idx)}
          data-testid={genAmtId(
            `${dataTestId}-${name}`,
            'dls-upload-file-download-button',
            'Upload.File.download'
          )}
        >
          <Icon name="download" />
        </Button>
      </Tooltip>
    );

    return renderDownloadFile
      ? renderDownloadFile({ index, file, elmDefault })
      : elmDefault;
  };

  return (
    <div
      ref={fileRef}
      className={classNames(
        'file-item',
        { truncate: isOneLine && isTruncate },
        className
      )}
      key={`${name}-${lastModified}`}
      data-testid={genAmtId(
        `${dataTestId}-${name}`,
        'dls-upload-file',
        'Upload.File'
      )}
    >
      <div ref={containerRef} className="file-item-content">
        <i ref={iconRef} className={`bg-file-type ${fileExtension}`} />
        <div ref={setFileNameRef} className="file-name">
          {hasName ? (
            <input
              className="text-truncate"
              placeholder={placeholderName}
              defaultValue={txtName || name}
              onChange={(event) => {
                file.txtName = event.target.value;
              }}
              data-testid={genAmtId(
                `${dataTestId}-${name}`,
                'dls-upload-file-input',
                'Upload.File.input'
              )}
            />
          ) : (
            <div
              className="text-truncate"
              title={name}
              data-testid={genAmtId(
                `${dataTestId}-${name}`,
                'dls-upload-file-name',
                'Upload.File.name'
              )}
            >
              {name}
            </div>
          )}
          {!isValid && !isTruncate && messageCpm}
        </div>
        {/* todo */}
        {/* {hasDescription ? (
          <input
            placeholder={placeholderDescription}
            defaultValue={txtDescription}
            onChange={(event) => {
              file.txtDescription = event.target.value;
            }}
          />
        ) : (
          !!txtDescription && (
            <span className="file-description">{txtDescription}</span>
          )
        )} */}
        {!isValid && isTruncate && messageCpm}

        <div
          className="file-size"
          ref={fileSizeRef}
          data-testid={genAmtId(
            `${dataTestId}-${name}`,
            'dls-upload-file-info',
            'Upload.File.info'
          )}
        >
          {!isUpload && (
            <span className="file-size-info">{formatBytes(size)}</span>
          )}
          {renderBtnDownloadFile()}
          {isDisplayDeleteBtn && (
            <Tooltip
              placement="top"
              variant="primary"
              triggerClassName="dls-upload-file-btn-remove"
              element={tooltipBtnRemoveFile}
              data-testid={genAmtId(
                `${dataTestId}-${name}`,
                'dls-upload-file-name',
                'Upload.File.name'
              )}
            >
              <Button
                size="sm"
                variant="icon-secondary"
                onClick={() => onRemoveFile(index, idx)}
                data-testid={genAmtId(
                  `${dataTestId}-${name}`,
                  'dls-upload-file-remove-button',
                  'Upload.File.remove'
                )}
              >
                <Icon name="close" />
              </Button>
            </Tooltip>
          )}
        </div>
        {isUpload && (
          <div
            className="file-progress uploading"
            style={{ width: `${percentage}%` }}
          />
        )}
      </div>
    </div>
  );
};

export default FileOnlyView;
