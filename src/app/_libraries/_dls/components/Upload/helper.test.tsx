import { render } from '@testing-library/react';
import React from 'react';
import {
  getAllWidthChildren,
  getTotalMarginPadding,
  formatBytes,
  getFileExtension,
  getValid,
  formatFiles,
  STATUS
} from './helper';
import { StateFile } from './type';

describe('Helper Upload', () => {
  describe('formatBytes', () => {
    it('default value', () => {
      const result = formatBytes();
      expect(result).toBe('0 Bytes');
    });

    it('has value', () => {
      const result = formatBytes(1024);
      expect(result).toBe('1 KB');
    });

    it('has value', () => {
      const result = formatBytes(10000000);
      expect(result).toBe('9.5 MB');
    });
  });

  describe('getFileExtension', () => {
    it('is not type support', () => {
      const result = getFileExtension();
      expect(result).toBe('bg-file-type-na');
    });

    it('is type support', () => {
      const result = getFileExtension('image.png');
      expect(result).toBe('bg-file-type-png');
    });
  });

  describe('getValid', () => {
    it('valid file', () => {
      const file = {
        name: 'image.png',
        size: 1024,
        idx: 'idx'
      } as StateFile;
      const options = {
        maxFile: 2048,
        minFile: 0,
        typeFiles: ['png']
      };
      const result = getValid(file, options);
      expect(result).toEqual({
        isInvalid: false,
        isTypeSupport: true,
        isOverSize: false,
        isUnderSize: false
      });
    });

    it('invalid file', () => {
      const file = {
        size: 1024,
        idx: 'idx'
      } as StateFile;
      const result = getValid(file, { minFile: 2048 });
      expect(result).toEqual({
        isInvalid: true,
        isTypeSupport: true,
        isOverSize: false,
        isUnderSize: true
      });
    });
    it('default option', () => {
      const file = {
        idx: 'idx'
      } as StateFile;
      const result = getValid(file);
      expect(result).toEqual({
        isInvalid: false,
        isTypeSupport: true,
        isOverSize: false,
        isUnderSize: false
      });
    });
  });

  describe('formatFiles', () => {
    it('format files', () => {
      const files = [
        {
          idx: '123',
          status: STATUS.valid
        },
        { name: 'image.png', size: 1024 },
        { name: 'text.txt', size: 1024 }
      ] as StateFile[];
      const options = { typeFiles: ['png'] };
      const result = formatFiles(files, options);

      expect(result.length).toEqual(3);
      expect(result[0]).toEqual(files[0]);

      expect(result[1].status).toEqual(STATUS.valid);

      expect(result[2].status).toEqual(STATUS.invalid);
    });

    it('invalid files', () => {
      const result = formatFiles(undefined, {});

      expect(result).toEqual([]);
    });
  });

  describe('getAllWidthChildren', () => {
    it('with element', () => {
      const { container } = render(<div />);
      window.getComputedStyle = () =>
        ({
          paddingRight: '12',
          paddingLeft: '12',
          marginRight: '12',
          marginLeft: '12'
        } as any);

      const result = getTotalMarginPadding(container);

      expect(result).toEqual(48);
    });
    it('with empty', () => {
      const result = getTotalMarginPadding(null);
      expect(result).toBe(0);
    });
  });

  describe('getAllWidthChildren', () => {
    it('with empty', () => {
      const result = getAllWidthChildren(null);
      expect(result).toBe(0);
    });

    it('with element', () => {
      const { container } = render(<div />);
      window.getComputedStyle = () =>
        ({
          paddingRight: '12',
          paddingLeft: '12',
          marginRight: '12',
          marginLeft: '12'
        } as any);

      let result = getAllWidthChildren(container);
      expect(result).toEqual(49);

      window.getComputedStyle = () =>
        ({
          paddingRight: '0',
          paddingLeft: '0',
          marginRight: '0',
          marginLeft: '0'
        } as any);
      result = getAllWidthChildren(container);
      expect(result).toEqual(0);
    });
  });
});
