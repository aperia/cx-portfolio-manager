import React, {
  forwardRef,
  useRef,
  useImperativeHandle,
  RefForwardingComponent
} from 'react';
import classnames from 'classnames';

// components
import UnassignedIcon from './icon-unassigned.svg';

// utils
import { genAmtId } from '../../utils';

export interface BubbleProps extends DLSId {
  name?: string;
  small?: boolean;
  isSystem?: boolean;
  isUnassigned?: boolean;
  disabled?: boolean;
}

export const getHashNumberFromString = (str: string) => {
  let hash = 1;
  let i = 0;

  do {
    hash = str.charCodeAt(i) + ((hash << 5) - hash);
    i++;
  } while (i < str.length);

  // Return a number in range [1-8]
  return (Math.abs(hash) % 8) + 1;
};

const Bubble: RefForwardingComponent<HTMLDivElement, BubbleProps> = (
  {
    name = 'anonymous',
    small,
    isSystem,
    isUnassigned = false,
    disabled = false,

    id,
    dataTestId
  },
  ref
) => {
  const containerRef = useRef<HTMLDivElement | null>(null);
  useImperativeHandle(ref, () => containerRef.current!);

  const getFirstLastByName = (nameInput: string) => {
    const words = nameInput.trim().split(' ');
    if (words.length === 1) return words;

    const first = words[0];
    const last = words[words.length - 1];
    return [first, last];
  };

  const shortName = name
    ? isSystem
      ? 'S'
      : getFirstLastByName(name)
          .map((n: string) => n.substr(0, 1).toUpperCase())
          .join('')
    : 'A';

  const rootClassName = classnames(
    'bubble',
    name
      ? isSystem
        ? 'system'
        : !isUnassigned && `bubble-color-${getHashNumberFromString(shortName)}`
      : 'anonymous',
    small && 'bb-sz-sm',
    disabled && 'disabled'
  );

  return (
    <div
      ref={containerRef}
      className={rootClassName}
      id={id}
      data-testid={genAmtId(dataTestId, 'dls-bubble', 'Bubble')}
    >
      {isUnassigned ? (
        <img src={UnassignedIcon} alt="" />
      ) : (
        <span>{shortName}</span>
      )}
    </div>
  );
};

export default forwardRef<HTMLDivElement, BubbleProps>(Bubble);
