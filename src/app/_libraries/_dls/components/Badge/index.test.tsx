import React from 'react';
import { render } from '@testing-library/react';
// component
import Badge from './index';

describe('Test Badge Component Style', () => {
  it('should render span with class "badge badge-green"', () => {
    const wrapper = render(<Badge color="green" />);
    const element = wrapper.container.querySelector(
      'span[class="badge badge-green"]'
    );
    expect(element).not.toBeNull();
    expect(
      wrapper.container.querySelector('span[class="badge badge-red"]')
    ).toBeNull();
  });

  it('should render span with class "badge badge-red"', () => {
    const wrapper = render(<Badge color="red" />);
    const element = wrapper.container.querySelector(
      'span[class="badge badge-red"]'
    );
    expect(element).not.toBeNull();
    expect(
      wrapper.container.querySelector('span[class="badge badge-green"]')
    ).toBeNull();
  });

  it('should render span with class "badge badge-cyan"', () => {
    const wrapper = render(<Badge color="cyan" />);
    const element = wrapper.container.querySelector(
      'span[class="badge badge-cyan"]'
    );
    expect(element).not.toBeNull();
    expect(
      wrapper.container.querySelector('span[class="badge badge-green"]')
    ).toBeNull();
  });

  it('should render span with class "badge badge-orange"', () => {
    const wrapper = render(<Badge color="orange" />);
    const element = wrapper.container.querySelector(
      'span[class="badge badge-orange"]'
    );
    expect(element).not.toBeNull();
    expect(
      wrapper.container.querySelector('span[class="badge badge-green"]')
    ).toBeNull();
  });

  it('should render span with class "badge badge-purple"', () => {
    const wrapper = render(<Badge color="purple" />);
    const element = wrapper.container.querySelector(
      'span[class="badge badge-purple"]'
    );
    expect(element).not.toBeNull();
    expect(
      wrapper.container.querySelector('span[class="badge badge-green"]')
    ).toBeNull();
  });

  it('should render span with class "badge badge-grey"', () => {
    const wrapper = render(<Badge color="grey" />);
    const element = wrapper.container.querySelector(
      'span[class="badge badge-grey"]'
    );
    expect(element).not.toBeNull();
    expect(
      wrapper.container.querySelector('span[class="badge badge-green"]')
    ).toBeNull();
  });

  it('should render span tag with class "badge badge-undefined disabled" when disabled is true', () => {
    const wrapper = render(<Badge disabled={true} />);
    const element = wrapper.container.querySelector(
      'span[class="badge badge-undefined disabled"]'
    );
    expect(element).not.toBeNull();
    expect(
      wrapper.container.querySelector('span[class="badge badge-green"]')
    ).toBeNull();
  });

  it('should render span tag with class "badge badge-undefined" when disabled is false', () => {
    const wrapper = render(<Badge disabled={false} />);
    const element = wrapper.container.querySelector(
      'span[class="badge badge-undefined"]'
    );
    expect(element).not.toBeNull();
    expect(
      wrapper.container.querySelector('span[class="badge badge-green"]')
    ).toBeNull();
  });

  it('should render span tag with class "badge badge-undefined" when textTransformNone props is false', () => {
    const wrapper = render(<Badge textTransformNone={false} />);
    const element = wrapper.container.querySelector(
      'span[class="badge badge-undefined"]'
    );
    expect(element).not.toBeNull();
    expect(
      wrapper.container.querySelector('span[class="badge badge-green"]')
    ).toBeNull();
  });

  it('should render span tag with class "badge badge-undefined text-transform-none" when textTransformNone props is true', () => {
    const wrapper = render(<Badge textTransformNone={true} />);
    const element = wrapper.container.querySelector(
      'span[class="badge badge-undefined text-transform-none"]'
    );
    expect(element).not.toBeNull();
    expect(
      wrapper.container.querySelector('span[class="badge badge-green"]')
    ).toBeNull();
  });
});

describe('Test Badge Component', () => {
  it('should render span with id is testId', () => {
    const wrapper = render(<Badge id="testId" />);
    expect(wrapper.container.querySelector('span[id="testId"]')).not.toBeNull();
    expect(wrapper.container.querySelector('span[id="wrongId"]')).toBeNull();
  });

  it('should render children', () => {
    const ref = React.createRef<HTMLSpanElement>();
    const children = <div>Children Element</div>;
    const wrapper = render(<Badge ref={ref}>{children}</Badge>);
    expect(wrapper.baseElement.querySelector('span')?.innerHTML).toEqual(
      '<div>Children Element</div>'
    );
  });
});
