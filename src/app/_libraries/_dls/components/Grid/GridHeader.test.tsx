import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';
import { GridProvider } from './GridContext';
import GridHeader from './GridHeader';

describe('test GridHeader', () => {
  it('render', () => {
    render(
      <GridProvider
        value={{
          state: {
            staticHeaderId: 'staticHeaderId',

            hasHorizontalScroll: true,
            gridInstance: {
              headerGroups: [
                {
                  headers: [
                    {
                      id: 'staticHeaderId',
                      placeholderOf: 'asd',
                      render: () => undefined,
                      getHeaderProps: () => ({}),
                      originalId: 'staticHeaderId',
                      totalFlexWidth: 1
                    },
                    {
                      id: 'staticHeaderId',
                      placeholderOf: 'asd',
                      render: () => undefined,
                      getHeaderProps: () => ({}),
                      originalId: 'staticHeaderId',
                      totalFlexWidth: 1
                    }
                  ],
                  getHeaderGroupProps: () => ({})
                },
                {
                  headers: [],
                  getHeaderGroupProps: () => ({})
                },
                ,
                {
                  headers: [],
                  getHeaderGroupProps: () => ({})
                }
              ]
            }
          },
          dispatch: () => undefined
        }}
      >
        <GridHeader />
      </GridProvider>
    );
  });
});
