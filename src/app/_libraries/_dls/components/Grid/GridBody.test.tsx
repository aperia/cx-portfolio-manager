import React from 'react';
import { createEvent, fireEvent, render } from '@testing-library/react';
import '@testing-library/jest-dom';
import { GridProvider } from './GridContext';
import GridBody from './GridBody';
import GridHeader from './GridHeader';

describe('test GridBody', () => {
  it('render with scrollable false', () => {
    const { baseElement } = render(
      <GridProvider
        value={{
          state: {
            scrollable: false,
            staticHeaderId: 'staticHeaderId',
            hasHorizontalScroll: true,
            gridInstance: {
              headerGroups: [
                {
                  getHeaderGroupProps: () => ({}),
                  headers: [
                    {
                      id: 'staticHeaderId',
                      placeholderOf: 'asd',
                      render: () => undefined,
                      getHeaderProps: () => ({}),
                      originalId: 'staticHeaderId',
                      totalFlexWidth: 1
                    },
                    {
                      id: 'staticHeaderId',
                      placeholderOf: 'asd',
                      render: () => undefined,
                      getHeaderProps: () => ({}),
                      originalId: 'staticHeaderId',
                      totalFlexWidth: 1
                    }
                  ]
                }
              ],
              rows: [],
              getTableBodyProps: () => ({})
            }
          },
          dispatch: () => undefined
        }}
      >
        <GridHeader />
        <GridBody className="dls-grid-body" />
      </GridProvider>
    );
    const bodyElement = baseElement.querySelector('tbody')!;

    bodyElement.closest = () => ({
      getElementsByClassName: () => [
        {
          scrollLeft: 1
        }
      ]
    });

    const event = createEvent('scroll', bodyElement, {
      scrollY: 100
    });

    fireEvent(bodyElement, event);
    expect(true).toBeTruthy();
  });

  it('render with scrollable true', () => {
    const { baseElement } = render(
      <GridProvider
        value={{
          state: {
            scrollable: true,
            staticHeaderId: 'staticHeaderId',
            hasHorizontalScroll: true,
            gridInstance: {
              headerGroups: [
                {
                  getHeaderGroupProps: () => ({}),
                  headers: [
                    {
                      id: 'staticHeaderId',
                      placeholderOf: 'asd',
                      render: () => undefined,
                      getHeaderProps: () => ({}),
                      originalId: 'staticHeaderId',
                      totalFlexWidth: 1
                    },
                    {
                      id: 'staticHeaderId',
                      placeholderOf: 'asd',
                      render: () => undefined,
                      getHeaderProps: () => ({}),
                      originalId: 'staticHeaderId',
                      totalFlexWidth: 1
                    }
                  ]
                }
              ],
              rows: [],
              getTableBodyProps: () => ({})
            }
          },
          dispatch: () => undefined
        }}
      >
        <GridHeader />
        <GridBody className="dls-grid-body" />
      </GridProvider>
    );
    const bodyElement = baseElement.querySelector('tbody')!;

    bodyElement.scrollTop = 100;
    Object.defineProperty(bodyElement, 'clientHeight', { value: 100 });
    Object.defineProperty(bodyElement, 'scrollheight', { value: 99 });

    bodyElement.closest = () => ({
      getElementsByClassName: () => [
        {
          scrollLeft: 1
        }
      ]
    });

    const event = createEvent('scroll', bodyElement, {
      scrollY: 100
    });

    fireEvent(bodyElement, event);
    expect(true).toBeTruthy();
  });

  it('render with scrollable true and onLoadMore', () => {
    const { baseElement } = render(
      <GridProvider
        value={{
          state: {
            scrollable: true,
            staticHeaderId: 'staticHeaderId',
            hasHorizontalScroll: true,
            onLoadMore: () => undefined,
            gridInstance: {
              headerGroups: [
                {
                  getHeaderGroupProps: () => ({}),
                  headers: [
                    {
                      id: 'staticHeaderId',
                      placeholderOf: 'asd',
                      render: () => undefined,
                      getHeaderProps: () => ({}),
                      originalId: 'staticHeaderId',
                      totalFlexWidth: 1
                    },
                    {
                      id: 'staticHeaderId',
                      placeholderOf: 'asd',
                      render: () => undefined,
                      getHeaderProps: () => ({}),
                      originalId: 'staticHeaderId',
                      totalFlexWidth: 1
                    }
                  ]
                }
              ],
              rows: [],
              getTableBodyProps: () => ({})
            }
          },
          dispatch: () => undefined
        }}
      >
        <GridHeader />
        <GridBody className="dls-grid-body" />
      </GridProvider>
    );
    const bodyElement = baseElement.querySelector('tbody')!;

    bodyElement.scrollTop = 100;
    Object.defineProperty(bodyElement, 'clientHeight', { value: 100 });
    Object.defineProperty(bodyElement, 'scrollheight', { value: 99 });

    bodyElement.closest = () => ({
      getElementsByClassName: () => [
        {
          scrollLeft: 1
        }
      ]
    });

    const event = createEvent('scroll', bodyElement, {
      scrollY: 100
    });

    fireEvent(bodyElement, event);
    expect(true).toBeTruthy();
  });
});
