export const data = [
  {
    rowHeader: 'KEPT',
    amount: '$3.000',
    number: 7
  },
  {
    rowHeader: 'BROKEN',
    amount: '$1.200',
    number: 11
  }
];

export const dataTotalFooter = {
  rowHeader: 'Total',
  amount: '$4.200',
  number: 18
};
