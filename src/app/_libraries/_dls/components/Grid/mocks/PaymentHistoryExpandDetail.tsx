/* eslint-disable react/display-name */
import React, { useState, useEffect } from 'react';
import isEmpty from 'lodash.isempty';

// Data
import { subTableData } from './data';

// type

export const SubGridNoHeader = ({
  isSetTimeOut = true,
  row: { original }
}: any) => {
  const { status } = original;

  const [data, setData] = useState<any[]>([]);

  useEffect(() => {
    if (!isSetTimeOut) return setData(subTableData);

    const time = setTimeout(() => {
      setData(subTableData);
    }, 500);

    return () => clearTimeout(time);
  }, [status, isSetTimeOut]);

  return isEmpty(data) ? (
    <div className="loading loading-custom" />
  ) : (
    <div className="sub-row overflow-hidden">
      {data.map(item => {
        const { postingDate, effectiveDate, amountPaid, description } = item;
        return (
          <div key={postingDate} className="row">
            <div className="col-2">
              <div className="cell-title">Posting Date</div>
              <div className="cell-value">{postingDate}</div>
            </div>
            <div className="col-2">
              <div className="cell-title">effective Date</div>
              <div className="cell-value">{effectiveDate}</div>
            </div>
            <div className="col-2">
              <div className="cell-title">amount Paid</div>
              <div className="cell-value">{amountPaid}</div>
            </div>
            <div className="col-6">
              <div className="cell-title">description</div>
              <div className="cell-value">{description}</div>
            </div>
          </div>
        );
      })}
    </div>
  );
};
