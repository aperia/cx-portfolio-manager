import React from 'react';

// Components
import Icon from '../../Icon';
import DropdownButton from '../../DropdownButton';

export const RenderDropdownActions = () => {
  const dropdownItem = [{ label: 'View' }, { label: 'Adjust' }];

  const dropdownListItem = dropdownItem.map(item => (
    <DropdownButton.Item key={item.label} label={item.label} value={item} />
  ));

  return (
    <DropdownButton
      buttonProps={{
        children: <Icon name="more" />,
        variant: 'icon-secondary'
      }}
      popupBaseProps={{ placement: 'bottom-end' }}
    >
      {dropdownListItem}
    </DropdownButton>
  );
};
