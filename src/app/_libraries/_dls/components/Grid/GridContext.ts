import { createContext } from 'react';
import { Row } from 'react-table';

// types
import { DispatchProps, GridContextArgs, GridStateProps } from './types';

export const initState = {
  data: [],
  dataHeader: undefined,
  dataFooter: undefined,
  columns: [],
  gridInstance: {
    rows: [],
    headerGroups: [],
    footerGroups: [],
    prepareRow: (row: Row) => row,
    getTableBodyProps: () => ({})
  },
  checkAll: false,
  checkedList: [],
  dataItemKey: 'id'
};

export const gridReducer = (state: GridStateProps, action: DispatchProps) => {
  const { type: actionType, ...payload } = action;

  switch (actionType) {
    case 'SET_GRID_INSTANCE':
      return { ...state, ...payload };
    case 'SET_PROPS':
      return { ...state, ...payload };
    default:
      return state;
  }
};

export const GridContext = createContext<GridContextArgs>({
  state: initState,
  dispatch: () => undefined
});

const { Provider: GridProvider } = GridContext;

export { GridProvider };
