import '@testing-library/jest-dom';
import { render } from '@testing-library/react';
import React from 'react';
import RowDnD from './RowDnD';

jest.mock('react-dnd', () => {
  return {
    useDrop: () => [undefined, () => undefined],
    useDrag: (options: any) => {
      options.end({}, { didDrop: () => false });
      return [{}, () => undefined, () => undefined];
    }
  };
});

describe('test RowDnD', () => {
  it('should call useDrag end with didProp false', () => {
    const wrapper = render(<RowDnD dndId="123" />);
    expect(wrapper.container.querySelector('tr')).toBeInTheDocument();
  });
});
