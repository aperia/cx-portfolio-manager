import React from 'react';
import { render, screen } from '@testing-library/react';
import CheckBox from './index';
import { mockUseRef } from '../../test-utils/hooks';
import { queryByFor, queryById } from '../../test-utils';
import '@testing-library/jest-dom';
describe('Test CheckBox', () => {
  const spyRef: any = mockUseRef();
  it('Render without input', () => {
    const textRender = 'CheckBox Label';
    const { container } = render(
      <CheckBox data-testid="checkbox1">
        <CheckBox.Label htmlFor="checkbox1">{textRender}</CheckBox.Label>
      </CheckBox>
    );

    const getLabelByTest = queryByFor(container, /dls-/)!;
    expect(getLabelByTest.textContent).toEqual(textRender);
  });

  it('Render CheckBox', () => {
    const textRender = 'CheckBox Label';
    const idCheckbox = 'checkbox1';
    render(
      <CheckBox ref={spyRef} data-testid="checkbox1">
        <CheckBox.Input id="checkbox1" />
        <CheckBox.Label htmlFor="checkbox1">{textRender}</CheckBox.Label>
      </CheckBox>
    );
    const getByTestId = screen.queryByTestId(idCheckbox);
    expect(spyRef).toHaveBeenCalled();
    expect(getByTestId?.textContent).toEqual(textRender);
  });

  it('Render CheckBox haven"t htmlFor', () => {
    const textRender = 'CheckBox Label';

    const { container } = render(
      <CheckBox data-testid="checkbox1">
        <CheckBox.Input />
        <CheckBox.Label>{textRender}</CheckBox.Label>
      </CheckBox>
    );
    const getInputByTest = queryById(container, /dls-/);
    const getLabelByTest = queryByFor(container, /dls-/);
    expect(getLabelByTest).toBeInTheDocument();
    expect(getInputByTest).toBeInTheDocument();
  });
});
