import React from 'react';
import Input from './Input';
import { render } from '@testing-library/react';
import { mockUseRef } from '../../test-utils/hooks';
import { queryByClass } from '../../test-utils/queryHelpers';

describe('Test Input CheckBox', () => {
  const spyRef: any = mockUseRef();
  const checkedValue = false;
  it('Render Input With indeterminate ', () => {
    const { container } = render(
      <Input ref={spyRef} indeterminate={true} checked={checkedValue} />
    );
    const queryCheckbox = queryByClass(container, /dls-checkbox-input/)!;
    queryCheckbox.click();
    expect(queryCheckbox.outerHTML.includes('checked=""')).toEqual(false);
  });
  it('Render Input With Props ReadOnly', () => {
    const { container } = render(
      <Input ref={spyRef} readOnly={true} checked={checkedValue} />
    );
    const queryCheckbox = queryByClass(container, /dls-checkbox-input/)!;
    queryCheckbox.click();
    expect(queryCheckbox.outerHTML.includes('checked=""')).toEqual(
      checkedValue
    );
  });

  it('Render Input With Props Checked is Undefined', () => {
    const { container } = render(<Input ref={spyRef} />);
    const queryCheckbox = queryByClass(container, /dls-checkbox-input/)!;
    queryCheckbox.click();
  });

  it('Render Input With Props Checked is Boolean', () => {
    const onChangeProp = jest.fn();
    const { container } = render(
      <Input ref={spyRef} checked={checkedValue} onChange={onChangeProp} />
    );
    const expandBtn = queryByClass(container, /dls-checkbox-input/)!;
    expandBtn.click();
    expect(onChangeProp).toHaveBeenCalled();
  });
});
