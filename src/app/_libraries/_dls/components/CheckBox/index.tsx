import React, {
  DetailedHTMLProps,
  ForwardRefExoticComponent,
  ForwardRefRenderFunction,
  HTMLAttributes,
  ReactElement,
  RefAttributes,
  useImperativeHandle,
  useRef
} from 'react';

// utils
import classNames from 'classnames';
import { className, genAmtId } from '../../utils';

// components
import Input, { InputProps } from './Input';
import Label from './Label';
import { Form } from 'react-bootstrap';

// hooks
import { useInputWithLabel } from '../../hooks';

export interface CheckBoxRef {
  ref?: HTMLInputElement;
}

export interface CheckBoxProps
  extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement>,
    DLSId {
  autoGenId?: boolean;
  children: InputProps | ReactElement[];
}

export interface ExtraStaticProp
  extends ForwardRefExoticComponent<
    CheckBoxProps & RefAttributes<HTMLDivElement>
  > {
  Input: typeof Input;
  Label: typeof Label;
}

const CheckBox: ForwardRefRenderFunction<HTMLDivElement, CheckBoxProps> = (
  { className: classNameProp, autoGenId, children, dataTestId, ...props },
  reference
) => {
  const containerRef = useRef<HTMLDivElement | null>(null);
  useImperativeHandle(reference, () => containerRef.current!);

  const testId = genAmtId(dataTestId, 'dls-checkbox', 'CheckBox');

  // Mapped children, add label if doesn't exist
  const { element } = useInputWithLabel({
    autoGenId,
    children,
    Input,
    Label,
    dataTestId: testId
  });

  return (
    <div
      className={classNames(className.checkbox.CONTAINER, classNameProp)}
      ref={containerRef}
      data-testid={testId}
      {...props}
    >
      {element}
    </div>
  );
};

const ExtraStaticCheckBox = React.forwardRef<HTMLDivElement, CheckBoxProps>(
  CheckBox
) as ExtraStaticProp;
ExtraStaticCheckBox.Input = Input;
ExtraStaticCheckBox.Label = Label;

Form.Check = ExtraStaticCheckBox as any;

export default ExtraStaticCheckBox;
