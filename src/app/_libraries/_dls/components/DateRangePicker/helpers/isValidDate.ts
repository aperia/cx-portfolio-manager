// utils
import { isDate } from '../../../lodash';

// types
import { IsValidDate } from '../types';

// Check a date is valid or not
const isValidDate = ((value: Date) => {
  // value maybe is Invalid Date, but Invalid Date is a Date object, so we need to !isNaN for this case
  const isValid = isDate(value) && !isNaN(value.valueOf());
  return isValid;
}) as IsValidDate;

export default isValidDate;
