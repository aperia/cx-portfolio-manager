import React from 'react';
import {
  render,
  screen,
  act,
  RenderResult,
  fireEvent
} from '@testing-library/react';
import '@testing-library/jest-dom';
import { queryByClass } from '../../test-utils';

import DropdownButton from './';
import Button from '../Button';

import { DropdownButtonProps } from './types';
import userEvent from '@testing-library/user-event';

const dataItem = [
  { value: 0, label: 'label 0' },
  { value: 1, label: 'label 1' },
  { value: 2, label: 'label 2' }
];

const dropdownButtonItems = dataItem.map((item, index) => (
  <DropdownButton.Item
    key={item.value + index}
    label={item.label}
    value={item}
  />
));

const mockDdbProp: DropdownButtonProps = {
  buttonProps: {
    id: 'btnId',
    children: 'btnName'
  },
  children: dropdownButtonItems
};

let wrapper: RenderResult;
let baseElement: HTMLElement;
let queryByText: any;

const renderComponent = (props: DropdownButtonProps) => {
  wrapper = render(
    <div>
      <DropdownButton ref={{ current: null }} {...props}></DropdownButton>
      <Button autoBlur={false}>Test handleOnPopupClosed</Button>
    </div>
  );

  baseElement = wrapper.baseElement as HTMLElement;
  queryByText = wrapper.queryByText;

  jest.runAllTimers();
};

let mockNavigatorPlatform: jest.SpyInstance;

beforeEach(() => {
  jest.useFakeTimers();
  mockNavigatorPlatform = jest.spyOn(window.navigator, 'platform', 'get');
});

const userClick = (element: HTMLElement) => {
  act(() => {
    userEvent.click(element);
  });

  act(() => {
    jest.runAllTimers();
  });
};

const getNode = {
  get container() {
    return queryByClass(baseElement, /dls-dropdown-button/)!;
  },
  get dlsPopup() {
    return baseElement.querySelector('.dls-popup')! as HTMLElement;
  }
};

describe('Render Component', () => {
  it('should render dropdownButton', () => {
    renderComponent(mockDdbProp);

    expect(getNode.container).toBeInTheDocument();
    expect(getNode.container.textContent).toEqual('btnName');
  });

  it('should render disabled dropdownButton', () => {
    renderComponent({
      ...mockDdbProp,
      buttonProps: { disabled: true }
    });

    expect(getNode.container).toBeInTheDocument();
    expect(getNode.container).toBeDisabled();
  });

  it('should render dropdownButton with data item', () => {
    renderComponent({
      children: dropdownButtonItems,
      opened: true
    });

    expect(getNode.container).toBeInTheDocument();

    expect(getNode.dlsPopup).toBeInTheDocument();
    expect(
      baseElement.querySelectorAll(
        '.dls-dropdown-base-scroll-container > .item'
      )
    ).toHaveLength(3);
  });

  it('should render dropdownButton without data item', () => {
    renderComponent({
      opened: true,
      children: []
    });

    expect(getNode.container).toBeInTheDocument();
    expect(getNode.dlsPopup).toBeInTheDocument();
    expect(
      baseElement.querySelector('.dls-no-result-found')
    ).toBeInTheDocument();
  });

  it('controlled with opened = true', () => {
    renderComponent({ ...mockDdbProp, opened: true });

    userClick(getNode.container);

    expect(getNode.container).toBeInTheDocument();
    expect(getNode.dlsPopup).toBeInTheDocument();
  });

  it('controlled with opened = false', () => {
    renderComponent({ ...mockDdbProp, opened: false });

    userClick(getNode.container);

    expect(getNode.container).toBeInTheDocument();
    expect(getNode.dlsPopup).toBeNull();
  });

  it('uncontrolled', () => {
    renderComponent(mockDdbProp);

    userClick(getNode.container);

    expect(getNode.container).toBeInTheDocument();
    expect(getNode.dlsPopup).toBeInTheDocument();

    userClick(document.body);

    expect(getNode.dlsPopup).toBeNull();
  });
});

describe('Test action', () => {
  it('test focus component', () => {
    const onFocus = jest.fn();
    renderComponent({ ...mockDdbProp, onFocus });

    userClick(getNode.container);

    expect(getNode.dlsPopup).toBeInTheDocument();
    expect(onFocus).toBeCalled();
  });

  it('test blur component', () => {
    const onBlur = jest.fn();

    renderComponent({ ...mockDdbProp, onBlur });

    userClick(getNode.container);
    expect(getNode.dlsPopup).toBeInTheDocument();

    act(() => {
      fireEvent.blur(getNode.container);
      jest.runAllTimers();
    });
    expect(getNode.dlsPopup).toBeNull();

    expect(onBlur).toBeCalled();
  });

  it('test mousedown component', () => {
    renderComponent({ ...mockDdbProp });

    userClick(getNode.container);
    expect(getNode.dlsPopup).toBeInTheDocument();
    userClick(getNode.container);
    expect(getNode.dlsPopup).toBeNull();
  });

  it('test mousedown component with macos', () => {
    mockNavigatorPlatform.mockReturnValue('Macintosh');

    renderComponent({ ...mockDdbProp, opened: false });
    userClick(getNode.container);
    expect(getNode.dlsPopup).not.toBeInTheDocument();
  });

  it('test mousedown popup', () => {
    renderComponent({ ...mockDdbProp });

    userClick(getNode.container);
    userClick(getNode.dlsPopup);
    expect(getNode.dlsPopup).toBeInTheDocument();

    userClick(document.body);
    expect(getNode.dlsPopup).not.toBeInTheDocument();
  });

  it('test auto focus component', () => {
    renderComponent({ ...mockDdbProp, autoFocus: true });

    expect(getNode.dlsPopup).toBeInTheDocument();

    userClick(document.body);

    expect(getNode.dlsPopup).toBeNull();
  });

  it('should close popup if click outside', () => {
    mockNavigatorPlatform.mockReturnValue('Macintosh');

    const onSelect = jest.fn();
    const onVisibilityChange = jest.fn();

    renderComponent({ ...mockDdbProp, onSelect, onVisibilityChange });

    userClick(getNode.container);
    expect(getNode.dlsPopup).toBeInTheDocument();
    expect(onVisibilityChange).toBeCalledWith(true);

    userClick(document.body);
    expect(getNode.dlsPopup).toBeNull();
    expect(onSelect).not.toBeCalledWith();
  });

  it('test select item dropdown with macos', () => {
    mockNavigatorPlatform.mockReturnValue('Macintosh');

    const onSelect = jest.fn();
    const onVisibilityChange = jest.fn();

    renderComponent({ ...mockDdbProp, onSelect, onVisibilityChange });

    userClick(getNode.container);

    expect(getNode.dlsPopup).toBeInTheDocument();
    expect(onVisibilityChange).toBeCalledWith(true);

    userClick(screen.getByText('label 1'));

    expect(getNode.dlsPopup).toBeNull();
    expect(onSelect).not.toBeCalledWith();
  });

  it('test select item dropdown', () => {
    mockNavigatorPlatform.mockReturnValue('');

    const onSelect = jest.fn();
    const onVisibilityChange = jest.fn();

    renderComponent({ ...mockDdbProp, onSelect, onVisibilityChange });

    userClick(getNode.container);

    expect(getNode.dlsPopup).toBeInTheDocument();
    expect(onVisibilityChange).toBeCalledWith(true);

    userClick(screen.getByText('label 1'));

    expect(getNode.dlsPopup).toBeNull();
    expect(onSelect).not.toBeCalledWith();
    expect(onSelect).toBeCalledWith(
      expect.objectContaining({ value: dataItem[1] })
    );
  });

  it('test select disabled item dropdown', () => {
    const onSelect = jest.fn();

    renderComponent({
      ...mockDdbProp,
      onSelect,
      children: dataItem.map((item, index) => (
        <DropdownButton.Item
          key={item.value + index}
          label={item.label}
          value={item}
          disabled={index === 2}
        />
      ))
    });

    userClick(getNode.container);

    expect(getNode.dlsPopup).toBeInTheDocument();

    const itemDisabled = queryByClass(baseElement, /item dls-disabled/)!;
    expect(itemDisabled).toBeInTheDocument();
    expect(itemDisabled.textContent).toEqual('label 2');

    userClick(itemDisabled);

    expect(getNode.dlsPopup).toBeInTheDocument();
    expect(onSelect).not.toBeCalled();
  });

  it('should cover handlePopupClosed and apply next tabIndex is fakeFocusRef.current', () => {
    jest.useFakeTimers();
    renderComponent({
      ...mockDdbProp,
      children: dataItem.map((item, index) => (
        <DropdownButton.Item
          key={item.value + index}
          label={item.label}
          value={item}
          disabled={index === 2}
        />
      ))
    });

    const buttonElement = queryByText('Test handleOnPopupClosed')!;
    userEvent.click(getNode.container);
    jest.runAllTimers();

    userEvent.click(getNode.container);
    jest.runAllTimers();

    userEvent.click(getNode.container);
    jest.runAllTimers();

    userEvent.click(buttonElement);
    jest.runAllTimers();

    expect(true).toBeTruthy();
  });
});
