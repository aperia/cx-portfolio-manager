import '@testing-library/jest-dom';
// testing library
import { render } from '@testing-library/react';
import React from 'react';
// mocks
import '../../../test-utils/mocks/mockCanvas';
import '../../../test-utils/mocks/mockChartjs';
// components
import BarChart from './';

describe('Test BarChart', () => {
  it('should render', () => {
    const { container } = render(<BarChart />);
    expect(container.querySelector('.dls-bar-chart')).toBeInTheDocument();
  });
});
