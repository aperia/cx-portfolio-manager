import React, { useEffect, useRef } from 'react';

import { Chart, ChartData, registerables } from 'chart.js';
import { useDeepValue } from '../../../hooks';
import classnames from 'classnames';
import { genAmtId } from '../../../utils';
import { ChartConfiguration } from 'chart.js';

Chart.register(...registerables);

export interface BarhartData extends ChartData<'bar', any> {}

export interface BarChartProps extends DLSId {
  config: ChartConfiguration;
  className?: string;
  height?: number;
}

const BarChart: React.RefForwardingComponent<HTMLDivElement, BarChartProps> = (
  {
    config: configProp,
    className,
    height = 190,

    id,
    dataTestId
  },
  ref
) => {
  // refs
  const canvasRef = useRef<HTMLCanvasElement | null>(null);

  // vars
  const config = useDeepValue(configProp);

  useEffect(() => {
    const canvas = canvasRef.current!;
    canvas.height = height;

    const ctx = canvas.getContext('2d')!;

    const lineChart = new Chart(ctx, config);

    return () => lineChart.destroy();
  }, [config, height]);

  return (
    <div
      ref={ref}
      className={classnames('dls-chart dls-bar-chart', className)}
      id={id}
      data-testid={genAmtId(dataTestId, 'dls-bar-chart', 'BarChart')}
    >
      <div className="dls-bar-chart-legend d-flex" />
      <canvas ref={canvasRef} />
    </div>
  );
};

export default React.forwardRef<HTMLDivElement, BarChartProps>(BarChart);
