import '@testing-library/jest-dom';
// testing library
import { render } from '@testing-library/react';
import React from 'react';
// mocks
import '../../../test-utils/mocks/mockCanvas';
import '../../../test-utils/mocks/mockChartjs';
// components
import LineChart from './';

describe('Test LineChart', () => {
  it('should render', () => {
    const { container } = render(<LineChart />);
    expect(container.querySelector('.dls-line-chart')).toBeInTheDocument();
  });
});
