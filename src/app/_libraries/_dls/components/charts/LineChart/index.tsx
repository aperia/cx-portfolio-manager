import { Chart, ChartConfiguration, ChartData, registerables } from 'chart.js';
import classnames from 'classnames';
import _ from 'lodash';
import React, { useEffect, useRef } from 'react';
import { useDeepValue } from '../../../hooks';
import { genAmtId } from '../../../utils';

Chart.register(...registerables);

export interface LineChartData extends ChartData<'line', any> {}

export interface LineChartProps extends DLSId {
  config: ChartConfiguration;
  className?: string;
  height?: number;
}

const LineChart: React.RefForwardingComponent<HTMLDivElement, LineChartProps> =
  (
    {
      config: configProp,
      className,
      height = 190,

      id,
      dataTestId
    },
    ref
  ) => {
    // refs
    const canvasRef = useRef<HTMLCanvasElement | null>(null);

    // vars
    const config = useDeepValue(configProp);

    useEffect(() => {
      const canvas = canvasRef.current!;
      canvas.height = height;

      const ctx = canvas.getContext('2d')!;
      const lineChart = new Chart(ctx, _.cloneDeep(config));
      return () => lineChart.destroy();
    }, [config, height]);

    return (
      <div
        ref={ref}
        className={classnames('dls-chart dls-line-chart', className)}
        id={id}
        data-testid={genAmtId(dataTestId, 'dls-line-chart', 'LineChart')}
      >
        <div className="dls-line-chart-legend d-flex" />
        <canvas ref={canvasRef} />
      </div>
    );
  };

export default React.forwardRef<HTMLDivElement, LineChartProps>(LineChart);
