import { render, screen } from '@testing-library/react';
import React from 'react';
import '@testing-library/jest-dom';

// components
import SimpleBar from '.';

// utils
import { queryByClass } from '../../test-utils/queryHelpers';

describe('Render', () => {
  it('should render', () => {
    const { baseElement } = render(
      <SimpleBar>
        <div>SimpleBar content</div>
      </SimpleBar>
    );

    expect(screen.getByText('SimpleBar content')).toBeInTheDocument();
    expect(
      queryByClass(baseElement as HTMLElement, /simplebar-wrapper/)
    ).toBeInTheDocument();
  });
});
