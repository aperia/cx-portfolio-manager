import {
  isFullValidTimePicker,
  combineTime,
  timeParser,
  format2DigitNumber,
  generateTimeData,
  checkValidAmPm
} from './helpers';

describe('Test TimePicker helpers', () => {
  describe('isFullValidTimePicker function', () => {
    it('returns true when value is empty object', () => {
      const value = isFullValidTimePicker({});

      expect(value).toEqual(true);
    });

    it('returns true when value has full data', () => {
      const value = isFullValidTimePicker({
        hour: '01',
        minute: '00',
        meridiem: 'AM'
      });

      expect(value).toEqual(true);
    });

    it('returns false > missing meridiem', () => {
      const value = isFullValidTimePicker({ hour: '01', minute: '00' });

      expect(value).toEqual(false);
    });

    it('returns false > missing minute', () => {
      const value = isFullValidTimePicker({ hour: '01', meridiem: 'AM' });

      expect(value).toEqual(false);
    });

    it('returns false > missing hour', () => {
      const value = isFullValidTimePicker({ minute: '01', meridiem: 'AM' });

      expect(value).toEqual(false);
    });
  });

  describe('combineTime function', () => {
    it('return empty', () => {
      const value = combineTime({});
      expect(value).toEqual('');
    });

    it('return empty', () => {
      const value = combineTime({
        hour: '__',
        minute: '__',
        meridiem: '__'
      });

      expect(value).toEqual('');
    });

    it('return empty', () => {
      const value = combineTime({
        hour: undefined,
        minute: undefined,
        meridiem: undefined
      });

      expect(value).toEqual('');
    });

    it('return ', () => {
      const value = combineTime({
        hour: '',
        minute: '',
        meridiem: ''
      });

      expect(value).toEqual('__:__ __');
    });

    it('return combine value when full data', () => {
      const value = combineTime({
        hour: '01',
        minute: '00',
        meridiem: 'AM'
      });

      expect(value).toEqual('01:00 AM');
    });
  });

  describe('timeParser function', () => {
    it('return data', () => {
      const value = timeParser('01:00 AM');

      expect(value).toEqual({
        hour: '01',
        minute: '00',
        meridiem: 'AM',
        isValid: true,
        maskedValue: '01:00 AM'
      });
    });

    it('return data with hour and minute is over size', () => {
      const value = timeParser('13:60 AM');

      expect(value).toEqual({
        hour: '01',
        minute: '59',
        meridiem: 'AM',
        isValid: true,
        maskedValue: '01:59 AM'
      });
    });

    it('return data with hour from 22 to 24 and meridiem just has _M', () => {
      const value = timeParser('23:00 _M');

      expect(value).toEqual({
        hour: '11',
        minute: '00',
        meridiem: undefined,
        isValid: false,
        maskedValue: '11:00 _M'
      });
    });

    it('return data with hour from 22 to 24 and meridiem just has _M', () => {
      const value = timeParser('aa:00 _M');

      expect(value).toEqual({
        hour: undefined,
        minute: '00',
        meridiem: undefined,
        isValid: false,
        maskedValue: 'aa:00 _M'
      });
    });

    it('return data with second', () => {
      const value = timeParser('10:00:00', true);

      expect(value).toEqual({
        hour: '10',
        minute: '00',
        meridiem: undefined,
        second: '00',
        isValid: true,
        maskedValue: '10:00:00'
      });
    });
  });

  describe('format2DigitNumber', () => {
    it('value <10', () => {
      const output = format2DigitNumber(3);
      expect(output).toEqual('03');
    });
    it('value > 10', () => {
      const output = format2DigitNumber(23);
      expect(output).toEqual('23');
    });
  });
  describe('generateTimeData', () => {
    it('from 0 to 10', () => {
      const output = generateTimeData(0, 10);
      expect(output.length).toEqual(11);
    });
  });

  describe('checkValidAmPm', () => {
    it('return false with undifined Value', () => {
      const output = checkValidAmPm();
      expect(output).toEqual(false);
    });
  });
});
