import React, {
  useRef,
  useMemo,
  useState,
  useEffect,
  useCallback
} from 'react';

import { TimeColumnProp, TimeName } from './types';
import classnames from 'classnames';

const rowHeight = 36;

/**
 * activeIndex start from 0 including for hyphen element
 * */

const TimeColumn: React.FC<TimeColumnProp> = ({
  label,
  data,
  value: valueProp,
  name,
  onChange: onChangeProp
}) => {
  const [scrollElement, setScrollElement] =
    useState<HTMLDivElement | null>(null);

  const previousScrollTop = useRef<number>(0);
  const currentActiveIndex = useRef<number>(0);

  const size = data.length;

  const activeControlledIndex = useMemo(() => {
    return data.findIndex((item) => item === valueProp) + 1;
  }, [data, valueProp]);

  const handleChange = useCallback(
    (index: number, shouldBlur?: boolean) => {
      onChangeProp({
        name,
        value: data[index],
        shouldBlur
      });
    },
    [data, name, onChangeProp]
  );

  const handleItemClick = (index: number) => {
    // if user click on meridiem, will blur the timePicker
    // if user select on meridiem , hours and minutes are undefined -> set minute and hours -> "00":"00"
    handleChange(index, name === TimeName.meridiem || name === TimeName.second);
  };

  useEffect(() => {
    if (!scrollElement) return;
    const handleScroll = () => {
      const currentTopPos = Math.round(scrollElement.scrollTop);
      const nextActive = Math.round(currentTopPos / rowHeight);
      //  direction = 1 -> Scroll down, direction = -1 scroll up
      const direction = Math.sign(currentTopPos - previousScrollTop.current);
      if (!direction || nextActive === currentActiveIndex.current) return;
      const nextActiveIndex = currentActiveIndex.current + direction;

      scrollElement.scrollTo({
        top: nextActiveIndex * rowHeight,
        behavior: 'auto'
      });

      previousScrollTop.current = nextActiveIndex * rowHeight;
      currentActiveIndex.current = nextActiveIndex;

      // due to we count the index of hyphen element -> we need to eliminate
      handleChange(nextActiveIndex - 1);
    };

    scrollElement.addEventListener('scroll', handleScroll);
    return () => {
      scrollElement.removeEventListener('scroll', handleScroll);
    };
  }, [handleChange, scrollElement]);

  // alway controlled mode
  useEffect(() => {
    if (!scrollElement) return;
    const nextScrollPos = activeControlledIndex * rowHeight;

    if (nextScrollPos === previousScrollTop.current) return;

    scrollElement.scrollTo({
      top: nextScrollPos,
      behavior: 'auto'
    });
    previousScrollTop.current = nextScrollPos;
    currentActiveIndex.current = activeControlledIndex;
  }, [activeControlledIndex, scrollElement]);

  return (
    <>
      <div className="dls-time-picker__column">
        <div className="dls-time-picker__label">{label}</div>
        <div
          ref={setScrollElement}
          className="dls-time-picker__scroll-container"
        >
          <div
            style={{
              height: `${size * rowHeight + 130}px`,
              transform: 'translateY(108px)'
            }}
          >
            <div
              className={classnames('time-element hyphen-item', {
                'hide-hyphen': activeControlledIndex > 0
              })}
            >
              --
            </div>
            {data.map((value, index) => {
              return (
                <div
                  key={`${value}_${index}`}
                  className={classnames('time-element', {
                    active: index === activeControlledIndex - 1
                  })}
                  onClick={() => handleItemClick(index)}
                >
                  {value}
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </>
  );
};

export default React.memo(TimeColumn);
