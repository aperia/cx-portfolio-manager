import { useEffect, useRef } from 'react';

import Hammer from '@egjs/hammerjs';

const useHammerPan = () => {
  const hammerRef = useRef<HTMLElement | null>(null);

  useEffect(() => {
    if (!hammerRef.current) return;

    const hammerElement = hammerRef.current;
    const hammer = new (Hammer as any)(hammerElement);

    hammer.add(
      new Hammer.Pan({ direction: Hammer.DIRECTION_VERTICAL, threshold: 50 })
    );

    const onPan = (ev: HammerInput) => {
      hammerElement.classList.add('panning');

      const direction = ev.direction === Hammer.DIRECTION_UP ? 1 : -1;
      hammerElement.scrollTop = hammerElement.scrollTop + 12 * direction;
    };

    const onPanEnd = () => {
      hammerElement.classList.remove('panning');
    };

    hammer.on('pan', onPan);
    hammer.on('panend pancancel', onPanEnd);

    return () => {
      hammer.off('pan', onPan);
      hammer.off('panend pancancel', onPanEnd);
    };
  }, []);

  return hammerRef;
};

export default useHammerPan;
