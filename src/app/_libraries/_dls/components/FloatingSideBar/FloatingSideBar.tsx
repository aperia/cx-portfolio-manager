import React, { useState, useEffect, useRef, useMemo } from 'react';

// components/types
import Icon from '../Icon';
import FloatingSideBarTabItem, {
  FloatingSideBarItem,
  SelectItemEvent,
  CloseItemEvent
} from './FloatingSideBarTabItem';

// hooks
import useHammerPan from './useHammerPan';

// utils
import groupBy from 'lodash.groupby';
import debounce from 'lodash.debounce';
import classnames from 'classnames';
import 'intersection-observer';
import perfectScrollIntoView from './perfectScrollIntoView';
import { genAmtId } from '../../utils';

export interface FloatingSideBarProps extends DLSId {
  content?: React.ReactNode;
  items?: FloatingSideBarItem[];
  zoom?: boolean;
  collapse?: boolean;
  // tooltipCloseAllProps?: PopperTooltipProps;
  onClose?: () => void;
  onSelectItem?: (event: SelectItemEvent) => void;
  onCloseItem?: (event: CloseItemEvent) => void;
}

const FloatingSideBar: React.FC<FloatingSideBarProps> = ({
  content,
  items,
  zoom: zoomProp,
  collapse: collapseProp,
  onClose,
  onSelectItem,
  onCloseItem,

  id,
  dataTestId
}) => {
  const [zoom, setZoom] = useState(zoomProp);
  const [collapse, setCollapse] = useState(collapseProp);
  const [hasPreviousButton, setHasPreviousButton] = useState<boolean>();
  const [hasNextButton, setHasNextButton] = useState<boolean>();

  const hammerRef =
    useHammerPan() as React.MutableRefObject<HTMLDivElement | null>;
  const itemsRef = useRef<HTMLDivElement[] | null>([]);
  const prevBtnRef = useRef<HTMLDivElement | null>(null);
  const nextBtnRef = useRef<HTMLDivElement | null>(null);
  const isRefresh = useRef<boolean>(true);

  useEffect(() => setZoom(zoomProp), [zoomProp]);
  useEffect(() => setCollapse(collapseProp), [collapseProp]);

  // check buttons disabled
  useEffect(() => {
    const hammerElement = hammerRef.current!;
    const itemElements = itemsRef.current!;
    let lastElement = itemElements[itemElements.length - 1];
    lastElement = lastElement.closest('.tab-item-wrap') as HTMLDivElement;

    const handleScroll = debounce(
      () => {
        setHasPreviousButton(!!hammerElement.scrollTop);

        const { top: containerTop, height: containerHeight } =
          hammerElement.getBoundingClientRect();
        const { top: elementTop, height: elementHeight } =
          lastElement.getBoundingClientRect();
        const endContainer = containerTop + containerHeight;
        const endElement = elementTop + elementHeight;

        const approx = endContainer - endElement;

        setHasNextButton(!(approx > 0 && approx < 5));
      },
      200,
      {
        leading: true,
        trailing: true
      }
    );

    handleScroll();
    isRefresh.current = false;

    hammerElement.addEventListener('scroll', handleScroll);
    return () => {
      isRefresh.current = true;
      hammerElement.removeEventListener('scroll', handleScroll);
    };
  }, [hammerRef, items]);

  // handle move to previous item
  const handlePreviousOnClick = () => {
    const prevBtnElement = prevBtnRef.current;
    const itemElements = itemsRef.current!;
    const { top: prevBtnTop, height: prevBtnHeight } =
      prevBtnElement!.getBoundingClientRect();
    const endPrevBtnTop = prevBtnTop + prevBtnHeight;

    for (let index = 0; index < itemElements.length; index++) {
      const itemElement = itemElements[index];
      const { top: itemTop, height: itemHeight } =
        itemElement.getBoundingClientRect();
      const endItem = Math.floor(itemTop + itemHeight);

      // half
      if (Math.ceil(itemTop) < endPrevBtnTop && endItem > endPrevBtnTop) {
        let prevElement: any;

        if (!itemElement.previousSibling) {
          // const parent = itemElement.closest(
          //   '.tab-item-wrap'
          // ) as HTMLDivElement;
          // const previousParent = parent.previousSibling as HTMLDivElement;
          const firstChild = null;
          prevElement = firstChild;
        } else {
          prevElement = itemElement.previousSibling as HTMLDivElement;
        }

        prevElement
          ? perfectScrollIntoView(
              prevElement,
              prevBtnElement!,
              nextBtnRef.current!,
              hammerRef.current!
            )
          : perfectScrollIntoView(
              itemElement,
              prevBtnElement!,
              nextBtnRef.current!,
              hammerRef.current!
            );
        break;
      }

      // full case
      const approx = endPrevBtnTop - Math.ceil(endItem);
      if (approx >= 0 && approx <= 6) {
        perfectScrollIntoView(
          itemElement,
          prevBtnElement!,
          nextBtnRef.current!,
          hammerRef.current!
        );
        break;
      }
    }
  };

  // handle move next previous item
  const handleNextOnClick = () => {
    const nextBtnElement = nextBtnRef.current!;
    const itemElements = itemsRef.current!;
    const { top: nextBtnTop } = nextBtnElement.getBoundingClientRect();

    for (let index = itemElements.length - 1; index >= 0; index--) {
      const itemElement = itemElements[index];
      const { top: itemTop, height: itemHeight } =
        itemElement.getBoundingClientRect();
      const endItem = itemTop + itemHeight;

      // half
      if (Math.ceil(itemTop) < nextBtnTop && endItem > nextBtnTop) {
        let nextItem: any;

        if (!itemElement.nextSibling) {
          // const parent = itemElement.closest(
          //   '.tab-item-wrap'
          // ) as HTMLDivElement;
          // const nextParent = parent.nextSibling as HTMLDivElement;
          const firstChild = null;
          nextItem = firstChild;
        } else {
          nextItem = itemElement.nextSibling as HTMLDivElement;
        }

        nextItem
          ? perfectScrollIntoView(
              nextItem,
              prevBtnRef.current!,
              nextBtnElement,
              hammerRef.current!
            )
          : perfectScrollIntoView(
              itemElement,
              prevBtnRef.current!,
              nextBtnElement,
              hammerRef.current!
            );

        break;
      }

      // full case
      const approx = Math.ceil(itemTop) - nextBtnTop;
      if (approx >= 0 && approx <= 6) {
        perfectScrollIntoView(
          itemElement,
          prevBtnRef.current!,
          nextBtnElement,
          hammerRef.current!
        );
        break;
      }
    }
  };

  const itemElements = useMemo(() => {
    // never happen
    // if items = undefined, ueEffect will throw error
    // if (!Array.isArray(items)) return;

    const itemsGrouped = groupBy(items, (item) => item.parent || 'parents');

    const parents = itemsGrouped['parents'] as FloatingSideBarItem[];
    // never happen
    // if (!Array.isArray(parents)) return;

    itemsRef.current = [];
    const elements = parents.map((parentItem) => {
      let groupdActived = parentItem.actived;

      const childs = itemsGrouped[parentItem.key];
      const childElements =
        Array.isArray(childs) &&
        childs.map((childItem) => {
          if (!groupdActived && childItem.actived) {
            groupdActived = childItem.actived;
          }

          return (
            <FloatingSideBarTabItem
              key={childItem.key}
              ref={(element: HTMLDivElement) => {
                if (
                  !itemsRef.current ||
                  !element ||
                  itemsRef.current.indexOf(element) !== -1
                ) {
                  return;
                }

                itemsRef.current.push(element);
              }}
              item={childItem}
              onSelectItem={onSelectItem}
              onCloseItem={onCloseItem}
              refs={{
                hammerRef,
                prevBtnRef,
                nextBtnRef
              }}
            />
          );
        });

      return (
        <div
          key={parentItem.key}
          className={classnames('tab-item-wrap', groupdActived && 'actived')}
        >
          <FloatingSideBarTabItem
            ref={(element: HTMLDivElement) => {
              if (
                !itemsRef.current ||
                !element ||
                itemsRef.current.indexOf(element) !== -1
              ) {
                return;
              }

              itemsRef.current.push(element);
            }}
            item={parentItem}
            onSelectItem={onSelectItem}
            onCloseItem={onCloseItem}
            groupActived={groupdActived}
            refs={{
              hammerRef,
              prevBtnRef,
              nextBtnRef
            }}
          />
          {childElements}
        </div>
      );
    });

    return elements;
  }, [items, onSelectItem, onCloseItem, hammerRef]);

  return (
    <aside
      id={id}
      data-testid={genAmtId(
        dataTestId,
        'dls-floating-sidebar',
        'FloatingSideBar'
      )}
      className={classnames('floating-sidebar', { zoom, collapse })}
    >
      <div className={classnames('floating-sidebar-left', { collapse })}>
        {content}
      </div>
      <div className="floating-sidebar-tab">
        {/* <PopperTooltip placement="left" {...tooltipCloseAllProps}> */}
        <div onClick={onClose} className="close-button">
          <Icon name="cross" size="3x" />
        </div>
        {/* </PopperTooltip> */}
        <div
          ref={prevBtnRef}
          onClick={handlePreviousOnClick}
          className={classnames(
            'previous-button',
            !hasPreviousButton && 'disabled'
          )}
        >
          <Icon name="arrow-up" size="3x" />
        </div>
        <div
          ref={hammerRef}
          className="ds-floating-sidebar-hammer"
          draggable={false}
        >
          <div className={classnames('list-scroll')}>{itemElements}</div>
        </div>
        <div
          ref={nextBtnRef}
          onClick={handleNextOnClick}
          className={classnames('next-button', !hasNextButton && 'disabled')}
        >
          <Icon name="arrow-down" size="3x" />
        </div>
      </div>
    </aside>
  );
};

export default FloatingSideBar;
