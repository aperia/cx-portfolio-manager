import React, {
  useImperativeHandle,
  useRef,
  useLayoutEffect,
  useState
} from 'react';

// components/types
import Icon from '../Icon';
import ActiveOutSideIcon from './active-outside.svg';
import ActiveIcon from './active.svg';
import InActiveIcon from './inactive.svg';

// utils
import classnames from 'classnames';

// import perfectScrollIntoView from './perfectScrollIntoView';

export interface SelectItemEvent {
  value: FloatingSideBarItem;
}

export interface CloseItemEvent {
  value: FloatingSideBarItem;
}

export interface FloatingSideBarItem {
  key: React.ReactText;
  text?: string;
  actived?: boolean;
  parent?: React.ReactText;
}

export interface FloatingSideBarTabItemProps {
  item: FloatingSideBarItem;
  onSelectItem?: (event: SelectItemEvent) => void;
  onCloseItem?: (event: CloseItemEvent) => void;
  className?: string;
  groupActived?: boolean;
  refs?: any;
}

const FloatingSideBarTabItem: React.RefForwardingComponent<
  HTMLDivElement,
  FloatingSideBarTabItemProps
> = (
  { item, onSelectItem, onCloseItem, className, groupActived, refs },
  ref
) => {
  const { key, text, actived, parent } = item || {};
  const [render, setRender] = useState(false);

  const divRef = useRef<HTMLDivElement | null>(null);
  useImperativeHandle(ref, () => divRef.current as any);

  const handleOnClickItem = () => {
    if (typeof onSelectItem !== 'function') return;

    onSelectItem({ value: item });
  };

  const handleOnClickIconClose = (
    event: React.MouseEvent<HTMLSpanElement, MouseEvent>
  ) => {
    event.stopPropagation();
    if (typeof onCloseItem !== 'function') return;

    onCloseItem({ value: item });
  };

  const parentIcon = groupActived
    ? actived
      ? ActiveIcon
      : ActiveOutSideIcon
    : InActiveIcon;

  useLayoutEffect(() => {
    setRender(true);
  }, []);

  useLayoutEffect(() => {
    if (!render) return;

    const element = divRef.current;

    if (!actived || !element || !refs['hammerRef']) {
      return;
    }

    // perfectScrollIntoView(
    //   element,
    //   refs['prevBtnRef'].current!,
    //   refs['nextBtnRef'].current!,
    //   refs['hammerRef'].current,
    //   true
    // );
  }, [actived, refs, render]);

  return (
    <div
      ref={divRef}
      key={key}
      onClick={handleOnClickItem}
      className={classnames('tab-item', actived && 'actived', className)}
    >
      {!parent && parentIcon && (
        <img src={parentIcon} alt="" draggable={false} />
      )}
      <span className="tab-item-id" title={text}>
        {text}
      </span>
      <span className="tab-item-wrap-icon" onClick={handleOnClickIconClose}>
        <Icon name="cross" size="3x" />
      </span>
    </div>
  );
};

export default React.forwardRef<HTMLDivElement, FloatingSideBarTabItemProps>(
  FloatingSideBarTabItem
);
