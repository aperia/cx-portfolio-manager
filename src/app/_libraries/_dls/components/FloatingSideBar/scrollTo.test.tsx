import scrollTo from './scrollTo';

describe('scrollTo', () => {
  const element = {
    scrollTop: 0,
    getBoundingClientRect: () => ({ height: 100, y: 100 })
  } as unknown as HTMLDivElement;

  it('call with 1 milisecond', () => {
    jest.useFakeTimers();

    scrollTo(element, 100, 20);

    jest.runAllTimers();

    expect(element.scrollTop).toEqual(100);
  });

  it('call with 1 second', () => {
    jest.useFakeTimers();

    scrollTo(element, 100, 1000);

    jest.runAllTimers();
    expect(element.scrollTop).toEqual(100);
  });
});
