import { MutableRefObject, useEffect, useMemo, useRef, useState } from 'react';

export interface NaiveVirtualScrollHook {
  (length: number, heightPerItem: number, numberItemsOnWindow: number): [
    /**
     * - Scroll container ref
     */
    MutableRefObject<HTMLDivElement | null>,
    /**
     * - Total height
     */
    number,
    /**
     * - From ... in list items
     */
    number,
    /**
     * - To ... in list items
     */
    number,
    /**
     * - TranslateY to simulate scroll space
     */
    number,
    /**
     * - A method to reset the from value
     */
    () => void
  ];
}

const useNaiveVirtualScroll: NaiveVirtualScrollHook = (
  length,
  heightPerItem,
  numberItemsOnWindow
) => {
  // refs
  const scrollableRef = useRef<HTMLDivElement | null>(null);
  const [from, setFrom] = useState(0);

  // reset from
  const resetFromRef = useRef(() => setFrom(0));

  const totalHeight = useMemo(
    () => length * heightPerItem,
    [heightPerItem, length]
  );

  const to = useMemo(
    () => from + numberItemsOnWindow,
    [from, numberItemsOnWindow]
  );

  const translateY = useMemo(() => from * heightPerItem, [from, heightPerItem]);

  useEffect(() => {
    if (!scrollableRef.current) return;

    const scrollableElement = scrollableRef.current;

    const handleScroll = (event: Event) => {
      event.stopPropagation();

      const { scrollTop } = event.target as HTMLElement;
      const nextFrom = Math.floor(scrollTop / heightPerItem);
      setFrom(nextFrom);
    };

    scrollableElement.addEventListener('scroll', handleScroll);
    return () => scrollableElement.removeEventListener('scroll', handleScroll);
  }, [heightPerItem]);

  return [
    scrollableRef,
    totalHeight,
    from,
    to,
    translateY,
    resetFromRef.current
  ];
};

export default useNaiveVirtualScroll;
