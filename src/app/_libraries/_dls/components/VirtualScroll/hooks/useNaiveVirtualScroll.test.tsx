import React, { useEffect } from 'react';

// testing library
import { fireEvent, render } from '@testing-library/react';
import '@testing-library/jest-dom';

// mocks
import useNaiveVirtualScroll from './useNaiveVirtualScroll';

const Wrapper: React.FC<any> = (props) => {
  const [ref, , , , , resetFrom] = useNaiveVirtualScroll(
    props.length || 1000,
    props.heightPerItem || 30,
    10
  );

  useEffect(() => {
    resetFrom();
  }, [resetFrom]);

  return (
    <div className="wrapper" ref={props.passRef ? ref : undefined}>
      Wrapper
    </div>
  );
};

describe('test useNaiveVirtualScroll', () => {
  it('should cover', () => {
    jest.useFakeTimers();
    const { container, rerender } = render(
      <Wrapper passRef={false} heightPerItem={30} />
    );

    rerender(<Wrapper passRef heightPerItem={31} />);

    fireEvent.scroll(document.querySelector('.wrapper')!, {
      target: { scrollLeft: 100, scrollTop: 0 }
    });

    expect(container.querySelector('.wrapper')).toBeInTheDocument();
  });
});
