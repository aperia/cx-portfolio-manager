import React, { MutableRefObject } from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';

// components
import ModalWrapper from './ModalWrapper';

// utils
import { queryByClass } from '../../test-utils/queryHelpers';

const mockRef: MutableRefObject<HTMLDivElement | null> = { current: null };

describe('Render', () => {
  it('should render', () => {
    render(<ModalWrapper ref={mockRef} show />);

    expect(screen.getByRole('dialog')).toBeInTheDocument();
    expect(screen.getByRole('dialog')).toBeInTheDocument();
  });

  it('should set animation duration to 0 when it smolder than 0', () => {
    render(<ModalWrapper ref={mockRef} show animationDuration={-1} />);

    expect(screen.getByRole('dialog')).toHaveStyle({
      'animation-duration': '0ms'
    });
  });

  it('should has className', () => {
    const { baseElement } = render(
      <ModalWrapper ref={mockRef} className={'class-name'} show />
    );

    expect(
      queryByClass(baseElement as HTMLElement, /class-name/)
    ).toBeInTheDocument();
  });
});

describe('Actions', () => {
  describe('keydown', () => {
    const { baseElement } = render(
      <ModalWrapper ref={mockRef} className={'class-name'} show>
        <button>button 1</button>
        <button>button 2</button>
      </ModalWrapper>
    );

    screen.getByText('button 2').focus();

    fireEvent.keyDown(baseElement, { keyCode: 9 });

    expect(document.activeElement?.innerHTML).toEqual('button 1');
  });

  it('keydown', () => {
    const { baseElement } = render(
      <ModalWrapper ref={mockRef} className={'class-name'} show={false}>
        <button>button 1</button>
        <button>button 2</button>
      </ModalWrapper>
    );

    document.body.focus();

    fireEvent.keyDown(baseElement, { keyCode: 9 });

    expect(document.activeElement?.innerHTML).not.toEqual('button 1');
  });
});
