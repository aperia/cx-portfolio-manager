import React, {
  useRef,
  useImperativeHandle,
  RefForwardingComponent,
  forwardRef
} from 'react';
import { ModalTitle as ModalTitleBase } from 'react-bootstrap';
import classnames from 'classnames';

import { className as classConst, genAmtId } from '../../utils';
import { ModalTitleProps } from './types';

const ModalTitle: RefForwardingComponent<HTMLDivElement, ModalTitleProps> = (
  { className, children, dataTestId, ...props },
  ref
) => {
  const containerRef = useRef<HTMLDivElement | null>(null);

  useImperativeHandle(ref, () => containerRef.current!);

  return (
    <ModalTitleBase
      ref={containerRef}
      className={classnames(classConst.modal.TITLE, className)}
      data-testid={genAmtId(dataTestId, 'dls-modal-title', 'Modal.Title')}
      {...props}
    >
      {children}
    </ModalTitleBase>
  );
};

export default forwardRef<HTMLDivElement, ModalTitleProps>(ModalTitle);
