import React, {
  useRef,
  useImperativeHandle,
  useEffect,
  RefForwardingComponent,
  forwardRef
} from 'react';
import ModalFooterBase from 'react-bootstrap/ModalFooter';
import classnames from 'classnames';

import { ModalFooterProps } from './types';
import { className as classConst, genAmtId } from '../../utils';
import Button from '../../components/Button';
import get from 'lodash.get';
import isEmpty from 'lodash.isempty';

const ModalFooter: RefForwardingComponent<HTMLDivElement, ModalFooterProps> = (
  {
    disabledOk,
    border = false,
    className,
    cancelButtonText,
    okButtonText,
    children,

    onCancel,
    onOk,

    dataTestId,
    ...props
  },
  ref
) => {
  const containerRef = useRef<HTMLDivElement | null>(null);

  useImperativeHandle(ref, () => containerRef.current!);

  useEffect(() => {
    const { BORDER_FOOTER, CONTENT } = classConst.modal;
    const parentElement = containerRef.current?.closest(`.${CONTENT}`);

    if (!parentElement) return;

    if (border) {
      parentElement.classList.add(BORDER_FOOTER);
    } else {
      parentElement.classList.remove(BORDER_FOOTER);
    }

    // watch the changing of modal-content className for adding BORDER_FOOTER
    const mutation = new MutationObserver((records) => {
      const target = get(records, ['0', 'target']) as typeof parentElement;

      if (!border || isEmpty(target)) return;
      // BORDER_FOOTER was existed
      if (border && target.classList.contains(BORDER_FOOTER)) return;

      parentElement.classList.add(BORDER_FOOTER);
    });

    // Modal will replace all class which was added before, Ex: add class 'loading'
    // We need to watch attribute class to add if BORDER_HEADER was replaced
    mutation.observe(parentElement!, { attributeFilter: ['class'] });

    return () => mutation.disconnect();
  }, [border]);

  const containerId = genAmtId(dataTestId, 'dls-modal-footer', 'Modal.Footer');

  return (
    <ModalFooterBase
      ref={containerRef}
      className={classnames(classConst.modal.FOOTER, className)}
      data-testid={containerId}
      {...props}
    >
      {children}
      {cancelButtonText && (
        <Button
          variant="secondary"
          onClick={onCancel}
          dataTestId={genAmtId(containerId, 'cancel-btn', '')}
        >
          {cancelButtonText}
        </Button>
      )}
      {okButtonText && (
        <Button
          variant="primary"
          onClick={onOk}
          disabled={disabledOk}
          dataTestId={genAmtId(containerId, 'ok-btn', '')}
        >
          {okButtonText}
        </Button>
      )}
    </ModalFooterBase>
  );
};
export default forwardRef<HTMLDivElement, ModalFooterProps>(ModalFooter);
