import React, {
  useEffect,
  useState,
  forwardRef,
  RefForwardingComponent,
  useLayoutEffect
} from 'react';

import Fade from '../../components/Animations/Fade';
import Modal from './Modal';
import { ModalWrapperProps } from './types';
import { ModalProvider } from './ModalContext';
import { keepFocusInside } from '../../utils';
import { useDeepValue } from '../../hooks';

const ModalWrapper: RefForwardingComponent<HTMLElement, ModalWrapperProps> = (
  {
    show: showProp,
    animationDuration = 350,
    animationComponent: AnimationComponent = Fade,
    animationComponentProps,
    ...props
  },
  ref
) => {
  const [duration, setDuration] = useState(0);

  useEffect(() => {
    setDuration(animationDuration > 0 ? animationDuration : 0);
  }, [animationDuration]);

  useLayoutEffect(() => {
    const handleKeyDownTab = (event: KeyboardEvent) => {
      const modals = document.getElementsByClassName('dls-modal-root');

      if (!modals.length) return;

      // when multible modals was opened, keep focus inside the last one
      const modal = modals[modals.length - 1] as HTMLDivElement;

      keepFocusInside(event, modal);
    };

    window.addEventListener('keydown', handleKeyDownTab);

    return () => window.removeEventListener('keydown', handleKeyDownTab);
  }, []);

  const contextValuePersisted = useDeepValue({ isInsideModal: true });

  // show={showProp || show}
  // -> modal is always show before animation is exited
  return (
    <ModalProvider value={contextValuePersisted}>
      <AnimationComponent
        in={showProp}
        duration={duration}
        {...animationComponentProps}
      >
        <Modal ref={ref} show animationDuration={duration} {...props} />
      </AnimationComponent>
    </ModalProvider>
  );
};

export default forwardRef<HTMLElement, ModalWrapperProps>(ModalWrapper);
