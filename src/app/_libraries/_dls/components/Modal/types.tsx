import { ReactType, ReactNode, ElementType } from 'react';

import { AnimationProps } from '../../components/Animations/types';

declare type DOMContainer<T extends HTMLElement = HTMLElement> =
  | T
  | React.RefObject<T>
  | null
  | (() => T | React.RefObject<T> | null);

interface ModalBaseProps extends DLSId {
  bsPrefix?: string;
  centered?: boolean;
  dialogAs?: React.ElementType;
  scrollable?: boolean;
  style?: React.CSSProperties;
  container?: DOMContainer;
  backdrop?: true | false | 'static';
  onEscapeKeyDown?: (e: KeyboardEvent) => void;
  onBackdropClick?: (e: React.SyntheticEvent) => void;
  keyboard?: boolean;
  autoFocus?: boolean;
  enforceFocus?: boolean;
  restoreFocus?: boolean;
  restoreFocusOptions?: {
    preventScroll: boolean;
  };
}

export interface ModalProps extends ModalBaseProps {
  placement?: 'top' | 'center';
  id?: string;
  xs?: boolean;
  sm?: boolean;
  md?: boolean;
  lg?: boolean;
  full?: boolean;
  rt?: boolean;
  lt?: boolean;
  show: boolean;
  loading?: boolean;
  animationDuration?: number;
  className?: string;
  classes?: {
    modal?: string;
    content?: string;
    dialog?: string;
    backdrop?: string;
  };
  children?: ReactNode;
  onShow?: () => void;
  onHide?: () => void;
}

export type ModalStateProps = {
  isInsideModal?: boolean;
};

export interface ModalWrapperProps extends ModalProps {
  animationComponent?: ReactType;
  animationComponentProps?: AnimationProps;
}

interface ModalHeaderBaseProps {
  closeButton?: boolean;
  closeLabel?: string;
  bsPrefix?: string;
  onHide?: () => void;
}

export interface ModalHeaderProps extends ModalHeaderBaseProps, DLSId {
  border?: boolean;
  className?: string;
  closeClassName?: string;
  children?: ReactNode;
}

interface ModalTitleBaseProps {
  as?: ElementType;
  bsPrefix?: string;
}

export interface ModalTitleProps extends ModalTitleBaseProps, DLSId {
  className?: string;
  children?: ReactNode;
}

interface ModalBodyBaseProps {
  as?: ElementType;
  bsPrefix?: string;
}

export interface ModalBodyProps extends ModalBodyBaseProps, DLSId {
  noPadding?: boolean;
  className?: string;
  children?: ReactNode;
}

interface ModalFooterBaseProps {
  as?: ElementType;
  bsPrefix?: string;
}

export interface ModalFooterProps extends ModalFooterBaseProps, DLSId {
  disabledOk?: boolean;
  border?: boolean;
  className?: string;
  cancelButtonText?: string;
  okButtonText?: string;
  children?: ReactNode;

  onCancel?: () => void;
  onOk?: () => void;
}
