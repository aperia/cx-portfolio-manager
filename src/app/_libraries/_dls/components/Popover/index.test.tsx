import React, { MutableRefObject, ReactElement } from 'react';
import '@testing-library/jest-dom';
import userEvent from '@testing-library/user-event';
import { act } from 'react-dom/test-utils';
import { fireEvent, render, RenderResult } from '@testing-library/react';
import { Matcher } from '@testing-library/react';

// components
import { Button, DropdownList, Icon, Popover } from '..';
import { PopperRef } from '../Popper';

// utils
import { queryByClass } from '../../test-utils/queryHelpers';

// mocks
import '../../test-utils/mocks/mockCanvas';

const mockRef: MutableRefObject<PopperRef | null> = { current: null };

let wrapper: RenderResult;

const dropdownListData = [
  { FieldID: 'ALL', FieldValue: 'All' },
  { FieldID: 'NER', FieldValue: 'NIGER' },
  { FieldID: 'BMU', FieldValue: 'BERMUDA' }
];

const innerElement = 'Inner Content of Popover';

const clickByClass = (id: Matcher) =>
  act(() =>
    userEvent.click(queryByClass(wrapper.baseElement as HTMLElement, id)!)
  );

const expectByClass = (id: Matcher) =>
  expect(queryByClass(wrapper.baseElement as HTMLElement, id));

const renderComponent = (element: ReactElement) => {
  wrapper = render(element);
};

describe('Render', () => {
  it('Render when popover is opened', () => {
    renderComponent(<Popover ref={mockRef} opened element={innerElement} />);

    expectByClass(/dls-popper-container/).toBeInTheDocument();
    expectByClass(/icon-button/).toBeNull();
    expect(wrapper.queryByText(innerElement)).toBeInTheDocument();

    // remove from DOM
    wrapper.rerender(<Popover opened={false}></Popover>);
  });

  it('Render with Icon Button when popover is opened', () => {
    renderComponent(
      <Popover opened>
        <Button>
          <Icon name="close" />
        </Button>
      </Popover>
    );

    expectByClass(/dls-popper-container/).toBeInTheDocument();
    expectByClass(/icon-button/).toBeInTheDocument();

    // remove from DOM
    wrapper.rerender(<Popover opened={false}></Popover>);
  });

  it('Render when popover not open', () => {
    renderComponent(<Popover ref={mockRef} opened={false} />);

    expectByClass(/dls-popper-container/).toBeNull();
    expectByClass(/icon-button/).toBeNull();

    // remove from DOM
    wrapper.rerender(<Popover opened={false}></Popover>);
  });
});

describe('Actions', () => {
  it('click outside when popover is opened', () => {
    const onVisibilityChange = jest.fn();

    renderComponent(<Popover opened onVisibilityChange={onVisibilityChange} />);

    fireEvent.mouseDown(wrapper.baseElement);
    expect(onVisibilityChange).toBeCalledWith(false);

    // remove from DOM
    wrapper.rerender(<Popover opened={false}></Popover>);
  });

  it('double click button when popover is not opened', () => {
    renderComponent(
      <Popover>
        <Button>
          <Icon name="close" />
        </Button>
      </Popover>
    );

    expectByClass(/dls-popper-container/).toBeNull();

    clickByClass(/dls-popper-trigger/);
    expectByClass(/dls-popper-container/).toBeInTheDocument();

    clickByClass(/dls-popper-trigger/);
    expectByClass(/dls-popper-container/).toBeNull();

    // remove from DOM
    wrapper.rerender(<Popover opened={false}></Popover>);
  });

  it('click dropdownList and click outside when popover is not opened', () => {
    renderComponent(
      <Popover
        element={
          <DropdownList
            opened
            name="user"
            value={dropdownListData[0]}
            textField="FieldValue"
            label="Controlled DropdownList"
            placeholder="My DropdownList"
          >
            {dropdownListData.map((item, index) => (
              <DropdownList.Item
                key={item.FieldID + index}
                label={item.FieldValue}
                value={item}
              />
            ))}
          </DropdownList>
        }
      >
        <Button>
          <Icon name="close" />
        </Button>
      </Popover>
    );

    expectByClass(/dls-popper-container/).toBeNull();

    // click to open popover
    clickByClass(/dls-popper-trigger/);
    expectByClass(/dls-popper-container/).toBeInTheDocument();

    // click on dropdownlist popupBase
    clickByClass(/dls-dropdown-base-container/);
    expectByClass(/dls-popper-container/).toBeInTheDocument();

    // click outsite
    fireEvent.mouseDown(wrapper.baseElement);
    expectByClass(/dls-popper-container/).toBeNull();

    // remove from DOM
    wrapper.rerender(<Popover opened={false}></Popover>);
  });

  it('click dropdownList and click inside popup element when popover is not opened', () => {
    renderComponent(
      <Popover
        element={
          <DropdownList
            opened
            name="user"
            value={dropdownListData[0]}
            textField="FieldValue"
            label="Controlled DropdownList"
            placeholder="My DropdownList"
          >
            {dropdownListData.map((item, index) => (
              <DropdownList.Item
                key={item.FieldID + index}
                label={item.FieldValue}
                value={item}
              />
            ))}
          </DropdownList>
        }
      >
        <Button>
          <Icon name="close" />
        </Button>
      </Popover>
    );

    expectByClass(/dls-popper-container/).toBeNull();

    // click to open popover
    clickByClass(/dls-popper-trigger/);
    expectByClass(/dls-popper-container/).toBeInTheDocument();

    // click on dropdownlist popupBase
    clickByClass(/dls-dropdown-base-container/);
    expectByClass(/dls-popper-container/).toBeInTheDocument();

    // click outsite
    fireEvent.mouseDown(wrapper.baseElement);
    expectByClass(/dls-popper-container/).toBeNull();

    // remove from DOM
    wrapper.rerender(<Popover opened={false}></Popover>);
  });
});
