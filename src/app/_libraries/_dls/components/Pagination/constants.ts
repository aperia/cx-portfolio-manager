export const DEFAULT_PAGE_SIZE = 10;
export const DEFAULT_PAGE_NUMBER = 1;
export const TABLET_WIDTH = 768;
export const CONTAINER_WITH_GTE_992 = {
  breakpoint: 992,
  numberDisplay: 5,
  onlyNumberDisplay: false,
  textInfo: 'Showing {showFrom}-{showTo} results of {totalItem}',
  textPerPage: 'Results per page'
};

export const CONTAINER_WITH_GTE_768 = {
  breakpoint: 768,
  numberDisplay: 3,
  onlyNumberDisplay: false,
  textInfo: '{totalItem} result{plural}',
  textPerPage: ''
};

export const CONTAINER_WITH_LT_768 = {
  breakpoint: 768,
  numberDisplay: 3,
  onlyNumberDisplay: true,
  textInfo: '',
  textPerPage: ''
};
