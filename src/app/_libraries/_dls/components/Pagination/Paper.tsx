import React, { Fragment } from 'react';

// components/types
import { Pagination } from 'react-bootstrap';
import { PaperProps } from './type';
import Icon from '../../components/Icon';
import PaginationItemCustom from './PaginationItem';

// utils
import { genAmtId } from '../../utils';

const Paper: React.FC<PaperProps> = ({
  totalPage,
  pageNumber,
  modeDisplay,
  onClick,

  dataTestId
}) => {
  const { numberDisplay, onlyNumberDisplay } = modeDisplay;

  const checkActivePage = (page: number) => pageNumber === page;

  const totalPath = Math.ceil(totalPage / numberDisplay) - 1;
  const currentPath = Math.ceil(pageNumber / numberDisplay) - 1;

  const handleRenderPageItem = () => {
    const items = [];
    for (let index = 0; index < numberDisplay; index++) {
      const displayStep = index + 1;
      const step = displayStep + currentPath * numberDisplay;
      if (step <= totalPage) {
        items.push(
          <Pagination.Item
            active={checkActivePage(step)}
            key={step}
            style={{
              pointerEvents: pageNumber === step ? 'none' : 'auto'
            }}
            data-testid={genAmtId(
              `${step}_${dataTestId || ''}`,
              'paper-step',
              'Paper.Item'
            )}
            onClick={() => {
              if (pageNumber === step) return;
              onClick(step);
            }}
          >
            {step}
          </Pagination.Item>
        );
      }
    }
    return items;
  };

  return (
    <Pagination>
      <PaginationItemCustom
        disabled={pageNumber === 1}
        onClick={() => onClick(pageNumber - 1)}
        dataTestId={genAmtId(
          `prev_${dataTestId || ''}`,
          'paper-step',
          'Paper.Item'
        )}
      >
        <Icon size="3x" name="arrow-left" />
      </PaginationItemCustom>
      {currentPath !== 0 && !onlyNumberDisplay && (
        <Fragment>
          <Pagination.Item
            active={checkActivePage(1)}
            onClick={() => onClick(1)}
            data-testid={genAmtId(
              `1_${dataTestId || ''}`,
              'paper-step',
              'Paper.Item'
            )}
          >
            {1}
          </Pagination.Item>
          <Pagination.Ellipsis
            onClick={() =>
              onClick((currentPath - 1) * numberDisplay + numberDisplay)
            }
            data-testid={genAmtId(
              `prev-ellipsis_${dataTestId || ''}`,
              'paper-step',
              'Paper.Item'
            )}
          />
        </Fragment>
      )}
      {handleRenderPageItem()}
      {currentPath !== totalPath && !onlyNumberDisplay && (
        <Fragment>
          <Pagination.Ellipsis
            onClick={() => onClick((currentPath + 1) * numberDisplay + 1)}
            data-testid={genAmtId(
              `next-ellipsis_${dataTestId || ''}`,
              'paper-step',
              'Paper.Item'
            )}
          />
          <Pagination.Item
            active={checkActivePage(totalPage)}
            onClick={() => onClick(totalPage)}
            data-testid={genAmtId(
              `${totalPage}_${dataTestId || ''}`,
              'paper-step',
              'Paper.Item'
            )}
          >
            {totalPage}
          </Pagination.Item>
        </Fragment>
      )}
      <PaginationItemCustom
        disabled={pageNumber === totalPage}
        onClick={() => onClick(pageNumber + 1)}
        dataTestId={genAmtId(
          `next_${dataTestId || ''}`,
          'paper-step',
          'Paper.Item'
        )}
      >
        <Icon size="3x" name="arrow-right" />
      </PaginationItemCustom>
    </Pagination>
  );
};

export default Paper;
