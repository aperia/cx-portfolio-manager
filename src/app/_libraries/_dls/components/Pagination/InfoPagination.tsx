import React from 'react';

import { InfoPaginationProps } from './type';
import { genAmtId } from '../../utils';

const InfoPagination: React.FC<InfoPaginationProps> = ({ textInfo, dataTestId }) => {
  return (
    <div 
      className="pagination-info pagination-text"
      data-testid={genAmtId(dataTestId, 'dls-info-pagination', 'InfoPagination')}
    >
      {textInfo}
    </div>
  );
};

export default InfoPagination;
