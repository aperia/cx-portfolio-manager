import React from 'react';
import { fireEvent, render } from '@testing-library/react';
import '@testing-library/jest-dom';
import Paper from '../Pagination/Paper';
import {
  queryAllByClass,
  queryByClass,
  queryByStyle
} from '../../test-utils/queryHelpers';
import { CONTAINER_WITH_GTE_992 } from './constants';
import userEvent from '@testing-library/user-event';
describe('Test Paper', () => {
  it('Should have Paper disabled left arrow', () => {
    const onClick = jest.fn();
    const { container } = render(
      <Paper
        totalPage={10}
        pageNumber={1}
        numberDisplay={2}
        onClick={onClick}
        modeDisplay={CONTAINER_WITH_GTE_992}
      />
    );
    const queryClass = queryAllByClass(container, /page-item/);
    expect(queryClass[0]).toHaveClass('disabled');
  });

  it('Should have Paper click left arrow', () => {
    let pageNumber = 2;
    const onClick = () => (pageNumber -= 1);
    const { container, rerender } = render(
      <Paper
        totalPage={10}
        pageNumber={pageNumber}
        numberDisplay={2}
        onClick={onClick}
        modeDisplay={CONTAINER_WITH_GTE_992}
      />
    );
    const queryIconArrow = queryAllByClass(container, /icon-arrow-left/);
    fireEvent.click(queryIconArrow[0]!);
    rerender(
      <Paper
        totalPage={10}
        pageNumber={pageNumber}
        numberDisplay={2}
        onClick={onClick}
        modeDisplay={CONTAINER_WITH_GTE_992}
      />
    );
    const dataPageNumber = queryByClass(container, /page-item active/);
    expect(dataPageNumber?.textContent).toContain('1');
  });

  it('Should have Paper click right arrow', () => {
    let pageNumber = 1;
    const onClick = () => (pageNumber += 1);
    const { container, rerender } = render(
      <Paper
        totalPage={10}
        pageNumber={pageNumber}
        numberDisplay={2}
        onClick={onClick}
        modeDisplay={CONTAINER_WITH_GTE_992}
      />
    );
    const queryIconArrow = queryAllByClass(container, /icon-arrow-right/);
    fireEvent.click(queryIconArrow[0]!);
    rerender(
      <Paper
        totalPage={10}
        pageNumber={pageNumber}
        numberDisplay={1}
        onClick={onClick}
        modeDisplay={CONTAINER_WITH_GTE_992}
      />
    );
    const dataPageNumber = queryByClass(container, /page-item active/);
    expect(dataPageNumber?.textContent).toContain('2');
  });

  it('Should have click ellipsis right', () => {
    const onClick = jest.fn();
    const { container, rerender } = render(
      <Paper
        totalPage={10}
        pageNumber={1}
        numberDisplay={2}
        onClick={onClick}
        modeDisplay={CONTAINER_WITH_GTE_992}
      />
    );
    const queryEllipsisLeft = queryAllByClass(container, /page-link/);
    fireEvent.click(queryEllipsisLeft[1]!);
    expect(onClick).not.toBeCalled();

    rerender(
      <Paper
        totalPage={10}
        pageNumber={1}
        numberDisplay={3}
        onClick={onClick}
        modeDisplay={CONTAINER_WITH_GTE_992}
      />
    );
    const queryPageLink = queryAllByClass(container, /page-link/);
    expect(queryPageLink.length).toEqual(9);
  });

  it('Should have click total page right', () => {
    const onClick = jest.fn();
    const { container, rerender } = render(
      <Paper
        totalPage={10}
        pageNumber={1}
        numberDisplay={2}
        onClick={onClick}
        modeDisplay={CONTAINER_WITH_GTE_992}
      />
    );
    const queryEllipsisLeft = queryAllByClass(container, /page-link/);
    fireEvent.click(queryEllipsisLeft[4]!);
    expect(onClick).toBeCalledWith(4);
    rerender(
      <Paper
        totalPage={10}
        pageNumber={10}
        numberDisplay={2}
        onClick={onClick}
        modeDisplay={CONTAINER_WITH_GTE_992}
      />
    );
    const dataPageNumber = queryByClass(container, /page-item active/);
    expect(dataPageNumber?.textContent).toContain('10');
  });

  it('Should have click ellipsis left', () => {
    const onClick = jest.fn();
    const { container } = render(
      <Paper
        totalPage={10}
        pageNumber={4}
        numberDisplay={2}
        onClick={onClick}
        modeDisplay={CONTAINER_WITH_GTE_992}
      />
    );
    const queryEllipsisLeft = queryAllByClass(container, /page-link/);
    fireEvent.click(queryEllipsisLeft[2]!);
    expect(onClick).toBeCalledWith(2);
  });

  it('Should have click left total ', () => {
    const onClick = jest.fn();
    const { container } = render(
      <Paper
        totalPage={10}
        pageNumber={4}
        numberDisplay={2}
        onClick={onClick}
        modeDisplay={CONTAINER_WITH_GTE_992}
      />
    );
    const queryEllipsisLeft = queryAllByClass(container, /page-link/);
    fireEvent.click(queryEllipsisLeft[1]!);
    expect(onClick).toBeCalledWith(1);
  });

  it('Should have step <= totalPage', () => {
    const onClick = jest.fn();
    const { container, getByTestId } = render(
      <Paper
        totalPage={10}
        pageNumber={11}
        numberDisplay={5}
        onClick={onClick}
        modeDisplay={CONTAINER_WITH_GTE_992}
      />
    );

    userEvent.click(getByTestId('next-ellipsis__paper-step'));
    userEvent.click(getByTestId('10__paper-step'));

    const queryEllipsisLeft = queryAllByClass(container, /page-link/);
    expect(queryEllipsisLeft.length).toEqual(6);
  });

  it('Should have pointerEvents none', () => {
    const onClick = jest.fn();
    const { container } = render(
      <Paper
        totalPage={12}
        pageNumber={12}
        numberDisplay={1}
        onClick={onClick}
        modeDisplay={CONTAINER_WITH_GTE_992}
      />
    );
    const queryStyle = queryByStyle(container, /pointer-events: none;/);
    expect(queryStyle).toBeInTheDocument();
  });

  it('Should have onClick pageNumber === step', () => {
    const onClick = jest.fn();
    const { container, queryByTestId } = render(
      <Paper
        totalPage={10}
        pageNumber={10}
        numberDisplay={1}
        onClick={onClick}
        modeDisplay={CONTAINER_WITH_GTE_992}
      />
    );

    userEvent.click(queryByTestId('prev-ellipsis__paper-step')!);

    const queryEllipsisLeft = queryAllByClass(container, /page-link/);
    fireEvent.click(queryEllipsisLeft[1]);
    const pageActive = queryByClass(container, /page-item active/);
    expect(pageActive?.textContent).toContain('10');
  });
});
