import { EndHandler } from 'react-transition-group/Transition';
import { Ref } from 'react';
import { CSSTransitionClassNames } from 'react-transition-group/CSSTransition';

export interface AnimationProps {
  in?: boolean;
  mountOnEnter?: boolean;
  unmountOnExit?: boolean;
  onEnter?: (element: HTMLElement) => void;
  onEntering?: (element: HTMLElement) => void;
  onEntered?: (element: HTMLElement) => void;
  onExit?: (element: HTMLElement) => void;
  onExiting?: (element: HTMLElement) => void;
  onExited?: (element: HTMLElement) => void;
  children?: any;
  nodeRef?: Ref<any>;
  addEndListener?: EndHandler<any>;
  classNames?: string | CSSTransitionClassNames;

  // custom
  anchor?: HTMLElement;
  animationCustomClassName?: string;
  duration?: number;
  isExitImmediately?: boolean;
  direction?: 'down' | 'up' | 'right' | 'left';
}
