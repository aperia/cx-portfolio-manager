import React, {
  forwardRef,
  RefForwardingComponent,
  useRef,
  useImperativeHandle
} from 'react';

import { className as classConst } from '../../utils';
import { AnimationProps } from './types';
import AnimationBase from './AnimationBase';

const Slide: RefForwardingComponent<HTMLDivElement, AnimationProps> = (
  { direction = 'right', animationCustomClassName, ...props },
  ref
) => {
  const containerRef = useRef<HTMLDivElement | null>(null);

  useImperativeHandle(ref, () => containerRef.current!);

  return (
    <AnimationBase
      ref={containerRef}
      animationCustomClassName={`${
        animationCustomClassName || classConst.animation.SLIDE
      }--${direction}`}
      {...props}
    />
  );
};

export default forwardRef<HTMLDivElement, AnimationProps>(Slide);
