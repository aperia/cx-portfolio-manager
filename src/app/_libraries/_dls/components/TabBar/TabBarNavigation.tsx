import React, {
  ReactElement,
  Children,
  ReactNode,
  useEffect,
  useState,
  useRef,
  useCallback,
  RefForwardingComponent,
  useImperativeHandle,
  forwardRef
} from 'react';

// components/types
import TabBarNavigationItem from './TabBarNavigationItem';
import { TabBarTabProps } from './types';
import classNames from 'classnames';
import Icon from '../Icon';

// utils
import debounce from 'lodash.debounce';
import { className } from '../../utils';

/**
 * @hidden
 */
const times = (count: any) => [...Array(count)];

/**
 * The props that are passed to the TabStripNavigation by the TabStrip.
 */

export interface TabStripNavigationProps {
  selected: number;
  tabIndex?: number;
  onKeyDown?: any;
  onSelect?(idx: number): void;
  children?: ReactElement | ReactElement<any>[];
}

interface ScrollParamsProps {
  scrollLeft?: boolean;
  scrollRight?: boolean;
}
const TabStripNavigation: RefForwardingComponent<
  HTMLDivElement,
  TabStripNavigationProps
> = (props: TabStripNavigationProps, ref) => {
  const { selected, children, onSelect, onKeyDown, tabIndex } = props;

  const containerRef = useRef<HTMLDivElement | null>(null);
  useImperativeHandle(ref, () => containerRef.current!);

  const tabsCount = Children.count(children);
  const childElements: ReactNode[] = Children.toArray(children);

  const [tabElement, setTabElement] = useState<HTMLUListElement | null>();
  const [showArrow, setShowArrow] = useState(false);
  const [scrollParams, setScrollParams] = useState<ScrollParamsProps>({});
  const [markActiveTab, setMarkActiveTab] = useState<{
    left: boolean;
    right: boolean;
  }>({ left: false, right: false });
  const arrowWidth = 34;
  const minAllowWidth = 88;
  const debounceFunLeft = useRef<any>();
  const debounceFunRight = useRef<any>();

  const handleLeftMove = (event: any) => {
    event.persist();
    event.stopPropagation();
    if (debounceFunRight.current) debounceFunRight.current.cancel();
    if (!debounceFunLeft.current) {
      debounceFunLeft.current = debounce(
        () => {
          const { scrollLeft } = tabElement as HTMLUListElement;
          const alignLeft = (scrollLeft + arrowWidth) / minAllowWidth;
          if (alignLeft < 2) {
            tabElement?.scrollTo({ left: 0, behavior: 'smooth' });
          } else {
            tabElement?.scrollTo({
              left: scrollLeft - ((alignLeft % 1) + 1) * minAllowWidth,
              behavior: 'smooth'
            });
          }
          setMarkActiveTab({ left: false, right: false });
        },
        200,
        { leading: true }
      );
    }
    debounceFunLeft.current();
  };
  const handleRightMove = (event: any) => {
    event.persist();
    event.stopPropagation();
    if (debounceFunLeft.current) debounceFunLeft.current.cancel();
    if (!debounceFunRight.current) {
      debounceFunRight.current = debounce(
        () => {
          const { scrollLeft, clientWidth } = tabElement as HTMLUListElement;
          const alignLeft =
            (scrollLeft + clientWidth - arrowWidth) / minAllowWidth;
          let newScrollLeftPos = 0;
          if (alignLeft % 1 === 0) {
            newScrollLeftPos = scrollLeft + minAllowWidth;
          } else {
            newScrollLeftPos =
              scrollLeft + (2 - (alignLeft % 1)) * minAllowWidth;
          }
          tabElement &&
            tabElement.scrollTo({
              left: newScrollLeftPos,
              behavior: 'smooth'
            });
          setMarkActiveTab({ left: false, right: false });
        },
        200,
        { leading: true }
      );
    }
    debounceFunRight.current();
  };
  const handleShowArrowNavigation = useCallback(() => {
    const {
      clientWidth,
      scrollWidth,
      scrollLeft
    } = tabElement as HTMLUListElement;

    if (clientWidth / (tabsCount - 1) < minAllowWidth) {
      // Show Arrow Navigation
      !showArrow && setShowArrow(true);
      setScrollParams(() => {
        return {
          scrollLeft: scrollLeft > 0,
          scrollRight: scrollWidth > clientWidth + scrollLeft
        };
      });
    } else {
      if (!showArrow) return;
      setShowArrow(false);
    }
  }, [tabElement, tabsCount, showArrow]);

  useEffect(() => {
    if (!tabElement) return;

    const handleResize = () => {
      handleShowArrowNavigation();
    };
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, [handleShowArrowNavigation, tabElement]);

  useEffect(() => {
    if (!showArrow || selected < 1 || !tabElement) return;
    const { clientWidth, scrollLeft } = tabElement as HTMLUListElement;
    const selectedTabPos = (selected - 1) * minAllowWidth;
    if (selectedTabPos <= scrollLeft + arrowWidth) {
      setMarkActiveTab({ left: true, right: false });
      tabElement.scrollTo({
        left: selectedTabPos - arrowWidth,
        behavior: 'smooth'
      });
    } else if (
      selectedTabPos + minAllowWidth >=
      scrollLeft + clientWidth - arrowWidth
    ) {
      // user selected a tab hidden on the right
      const distance =
        selectedTabPos + minAllowWidth - clientWidth + arrowWidth;
      setMarkActiveTab({ left: false, right: true });
      tabElement.scrollTo({ left: distance, behavior: 'smooth' });
    } else {
      setMarkActiveTab({ left: false, right: false });
    }
  }, [selected, minAllowWidth, showArrow, tabElement]);

  const handleOnScroll = () => {
    if (!showArrow) return;
    const {
      clientWidth,
      scrollWidth,
      scrollLeft
    } = tabElement as HTMLUListElement;
    setScrollParams({
      scrollLeft: scrollLeft > 0,
      scrollRight: scrollWidth > clientWidth + scrollLeft
    });
  };

  useEffect(() => {
    if (!tabElement) return;
    handleShowArrowNavigation();
  }, [handleShowArrowNavigation, tabElement]);

  const firstElement = (child: ReactElement<TabBarTabProps>) => {
    const tabProps = {
      active: selected === 0,
      disabled: child.props.disabled,
      index: 0,
      title: child.props.title,
      onSelect: onSelect,
      hoverTitle: child.props.hoverTitle
    };

    return (
      <TabBarNavigationItem
        key={child.key}
        tag="div"
        {...tabProps}
        className={className.tabBar.HOME}
        isTabFirst={selected === 1}
      />
    );
  };
  const navClasses = classNames(className.tabBar.TAB_STRIP_ITEMS);
  return (
    <div ref={containerRef} className={classNames(className.tabBar.NAV)}>
      {showArrow && scrollParams.scrollLeft && (
        <div
          className={classNames(className.tabBar.ARROW_LEFT, {
            [className.tabBar.MARK_ACTIVE]: markActiveTab.left
          })}
          onClick={handleLeftMove}
        >
          <Icon name="chevron-left" />
        </div>
      )}
      {firstElement(childElements[0] as ReactElement<TabBarTabProps>)}
      <div className={className.tabBar.NAV_BODY}>
        <ul
          className={navClasses}
          role={'tablist'}
          tabIndex={tabIndex}
          onKeyDown={onKeyDown}
          ref={setTabElement}
          onScroll={handleOnScroll}
        >
          {tabsCount &&
            tabsCount >= 1 &&
            times(tabsCount - 1).map((_, index) => {
              const tabProps = {
                active: selected === index + 1,
                disabled: (childElements[index + 1] as any).props.disabled,
                index: index + 1,
                title: (childElements[index + 1] as any).props.title,
                onSelect,
                hoverTitle: (childElements[index + 1] as any).props.hoverTitle
              };
              const key = (childElements[index + 1] as any).key;
              return <TabBarNavigationItem key={key} {...tabProps} />;
            })}
        </ul>
      </div>
      {showArrow && scrollParams.scrollRight && (
        <div
          className={classNames(className.tabBar.ARROW_RIGHT, {
            [className.tabBar.MARK_ACTIVE]: markActiveTab.right
          })}
          onClick={handleRightMove}
        >
          <Icon name="chevron-right" />
        </div>
      )}
    </div>
  );
};

export default forwardRef<HTMLDivElement, TabStripNavigationProps>(
  TabStripNavigation
);
