import React, {
  ReactElement,
  Children,
  CSSProperties,
  RefForwardingComponent,
  useRef,
  useImperativeHandle,
  forwardRef,
  useEffect,
  ReactNode
} from 'react';

// utils
import classNames from 'classnames';
import { className, genAmtId } from '../../utils';

// types
import { TabBarTabProps } from './types';

/**
 * The props that are passed to the TabStripContent by the TabStrip.
 */
export interface TabBarContentProps {
  animation?: boolean;
  selected?: number;
  style?: CSSProperties;
  index?: number;
  children?: ReactElement | ReactElement[];
}

const TabBarContent: RefForwardingComponent<
  HTMLDivElement,
  TabBarContentProps
> = (props, ref) => {
  const activatedTabs = useRef<Record<any, boolean>>({});
  const { children, selected } = props;

  const containerRef = useRef<HTMLDivElement | null>(null);
  useImperativeHandle(ref, () => containerRef.current!);

  const selectedTab =
    children &&
    typeof selected === 'number' &&
    (Array.from(children as ReactNode[])[
      selected
    ] as ReactElement<TabBarTabProps>);

  const contentClasses = classNames(
    className.tabBar.CONTENT,
    className.tabBar.DLS_STATE_ACTIVE,
    selectedTab && selectedTab.props.contentClassName
  );

  // save key of tab already mount to keep mount when the tab is inactive
  useEffect(() => {
    if (!selectedTab) return;
    const keyTab = selectedTab?.key || '';
    activatedTabs.current[keyTab] = true;
  }, [selectedTab]);

  const renderChild = (tab: ReactElement<TabBarTabProps>, idx: number) => {
    const visible = idx === props.selected;
    const contentProps = {
      role: 'tabpanel',
      'aria-expanded': true,
      style: {
        display: visible ? undefined : 'none'
      },
      'data-testid': visible
        ? genAmtId('tab-bar-content', 'active', 'TabBarContent')
        : undefined
    };
    const keyTab = tab.key || '';

    if (!activatedTabs.current[keyTab] && !visible) {
      return null;
    }

    return (
      <div {...contentProps} key={keyTab}>
        {tab.props.children}
      </div>
    );
  };

  return (
    <div ref={containerRef} className={contentClasses} style={props.style}>
      {Children.map(props.children, (tab, idx) => {
        return renderChild(tab as ReactElement<TabBarTabProps>, idx);
      })}
    </div>
  );
};

export default forwardRef<HTMLDivElement, TabBarContentProps>(TabBarContent);
