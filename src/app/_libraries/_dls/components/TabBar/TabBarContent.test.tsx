import React, { MutableRefObject, ReactElement } from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';

// components
import TabBarContent from './TabBarContent';
import TabBarTab from './TabBarTab';

// utils
import { queryByClass } from '../../test-utils/queryHelpers';

const mockRef: MutableRefObject<HTMLDivElement | null> = { current: null };

const tabs = [
  {
    // id: 'home',
    title: 'Home',
    children: 'Content Home Tab'
  },
  {
    id: 'tab1',
    title: 'Tab Name 1',
    children: 'Content Tab 01'
  },
  {
    id: 'tab2',
    title: 'Tab Name 2',
    children: 'Content Tab 02',
    disabled: true
  }
];

const renderComponent = (element: ReactElement) => {
  const wrapper = render(element);

  return {
    wrapper,
    baseElement: wrapper.baseElement as HTMLElement
  };
};

describe('Render', () => {
  it('should render single tab', () => {
    const { baseElement } = renderComponent(
      <TabBarContent ref={mockRef} selected={0}>
        <TabBarTab
          title={
            <React.Fragment>
              <span className="tab-title" id={'singleTab'}>
                Single Tab
              </span>
            </React.Fragment>
          }
          hoverTitle={'Single Tab'}
        >
          Single Tab Context
        </TabBarTab>
      </TabBarContent>
    );

    expect(queryByClass(baseElement, /dls-content/)).toBeInTheDocument();
    expect(screen.getByText('Single Tab Context')).toBeInTheDocument();
  });

  it('should render', () => {
    const { baseElement } = renderComponent(
      <TabBarContent ref={mockRef} selected={0}>
        {tabs.map(({ id, title, ...tabProps }) => {
          return (
            <TabBarTab
              key={id}
              title={
                <React.Fragment>
                  <span className="tab-title" id={id}>
                    {title}
                  </span>
                </React.Fragment>
              }
              hoverTitle={title}
              {...tabProps}
            />
          );
        })}
      </TabBarContent>
    );

    expect(queryByClass(baseElement, /dls-content/)).toBeInTheDocument();

    const homeTab = screen.getByText('Content Home Tab');
    expect(homeTab).toBeInTheDocument();
    expect(homeTab).not.toHaveStyle({ display: 'none' });

    const tab1 = screen.queryByText('Content Tab 01');
    expect(tab1).not.toBeInTheDocument();
  });
});
