import * as React from 'react';

/**
 * Represents the props of the TabStrip tabs.
 */
import { TabBarTabProps } from './types';

const TabBarTab: React.FC<TabBarTabProps> = () => {
  return null;
};

export default TabBarTab;
