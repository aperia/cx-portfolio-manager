import React, { ReactNode } from 'react';

// utils
import classNames from 'classnames';
import { className as classConst, genAmtId } from '../../utils';
import { isFunction } from 'lodash';

/**
 * The props that are passed by the TabStripNavigation to the TabStripNavigationItem.
 */
export interface TabBarNavigationItemProps {
  active?: boolean;
  disabled?: boolean;
  index: number;
  title?: ReactNode | string;
  onSelect?(idx: number): void;
  tag?: 'div' | 'li';
  className?: string;
  hoverTitle?: string;
  isTabFirst?: boolean;
}
const TabBarNavigationItem: React.FunctionComponent<TabBarNavigationItemProps> = (
  props
) => {
  const {
    active,
    disabled,
    title = 'Untitled',
    index,
    onSelect,
    tag: Tag = 'li',
    className,
    hoverTitle,
    isTabFirst
  } = props;

  const onClick = () => {
    isFunction(onSelect) && onSelect(index);
  };
  const itemProps = {
    'aria-selected': active,
    role: 'tab',
    onClick: !disabled ? onClick : undefined,
    'data-testid': active
        ? genAmtId('tab-bar-item', 'active', 'TabBarNavigationItem')
        : undefined
  };

  const itemClasses = classNames(
    classConst.tabBar.ITEM,
    { [classConst.tabBar.STATE_DISABLED]: disabled },
    { [classConst.tabBar.STATE_ACTIVE]: active },
    className
  );
  return (
    <Tag
      className={classNames(classConst.tabBar.ITEM_WRAP, {
        [classConst.tabBar.PRE_ACTIVE]: isTabFirst
      })}
      title={hoverTitle}
    >
      <span {...itemProps} className={itemClasses}>
        <span className={classConst.tabBar.TAB}>{title}</span>
      </span>
    </Tag>
  );
};

export default TabBarNavigationItem;
