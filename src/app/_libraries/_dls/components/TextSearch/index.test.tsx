import React from 'react';

// testing libs
import '@testing-library/jest-dom';
import userEvent from '@testing-library/user-event';
import { fireEvent, render, RenderResult } from '@testing-library/react';

// mocks
import '../../test-utils/mocks/mockCanvas';

// component
import TextSearch from '.';
import { TextSearchProps } from '.';

const renderTextSearch = (props: TextSearchProps): RenderResult => {
  return render(<TextSearch ref={() => {}} {...props} />);
};

describe('Test TextSearch Component', () => {
  it('Render component > uncontrol value', () => {
    const onSearchFn = jest.fn((value: string) => value);
    const wrapper = renderTextSearch({
      placeholder: 'Mock placeholder',
      clearTooltip: 'Clear Search Criteria',
      onSearch: onSearchFn
    });

    const inputElement = wrapper.baseElement.querySelector(
      '.dls-text-box > input'
    );
    fireEvent.change(inputElement!, { target: { value: 'Mocked value' } });
    // render with placeholder and value
    expect(inputElement).toBeInTheDocument();
    expect(inputElement?.getAttribute('placeholder')).toBe('Mock placeholder');
    expect(inputElement?.getAttribute('value')).toBe('Mocked value');

    const searchButton = wrapper.baseElement.querySelector(
      'button[type=submit].dls-text-search-search'
    );
    fireEvent.click(searchButton!);
    // search with input's value
    expect(searchButton).toBeInTheDocument();
    expect(onSearchFn).toHaveBeenCalledWith('Mocked value');

    const clearButton = wrapper.container.querySelector(
      '.dls-text-search-clear > div'
    );
    fireEvent.click(clearButton!);
    // clicking on clear button
    expect(clearButton).toBeInTheDocument();
    expect(inputElement?.getAttribute('value')).toBe('');
    // Test hint text clear button
    userEvent.hover(clearButton!);
    expect(wrapper.getByText(/Clear Search Criteria/i)).toBeInTheDocument();
  });
  it('Render component > control value', () => {
    const value = '';
    const onChangeFn = jest.fn();
    const onSearchFn = jest.fn();
    const onClearFn = jest.fn();
    const wrapper = renderTextSearch({
      value,
      onSearch: onSearchFn,
      onChange: onChangeFn,
      onClear: onClearFn
    });

    // user typing value
    const inputElement = wrapper.baseElement.querySelector(
      '.dls-text-box > input'
    ) as HTMLInputElement;
    userEvent.type(inputElement!, 'Test');
    expect(onChangeFn).toBeCalledTimes(4);

    // search with '' in controlled mode
    const searchButton = wrapper.baseElement.querySelector(
      'button[type=submit].dls-text-search-search'
    );
    fireEvent.click(searchButton!);
    expect(onSearchFn).not.toBeCalled();

    const clearButton = wrapper.container.querySelector(
      '.dls-text-search-clear > div'
    );
    fireEvent.click(clearButton!);
    // clicking on clear button
    expect(onClearFn).toBeCalled();
  });

  it('with popupElement uncontrolled', () => {
    const wrapper = renderTextSearch({
      popupElement: true
    });

    const inputElement = wrapper.container.querySelector('input')!;

    jest.useFakeTimers();
    userEvent.click(inputElement);
    jest.runAllTimers();

    const popupElement = wrapper.baseElement.querySelector('.dls-popup')!;
    userEvent.click(popupElement);
    expect(popupElement).toBeInTheDocument();

    jest.useFakeTimers();
    userEvent.click(document.body);
    jest.runAllTimers();

    expect(popupElement).not.toBeInTheDocument();
  });

  it('with popupElement controlled', () => {
    const wrapper = renderTextSearch({
      popupElement: true,
      opened: false
    });

    const inputElement = wrapper.container.querySelector('input')!;

    jest.useFakeTimers();
    userEvent.click(inputElement);
    jest.runAllTimers();

    const popupElement = wrapper.baseElement.querySelector('.dls-popup')!;
    expect(popupElement).not.toBeInTheDocument();
  });
});
