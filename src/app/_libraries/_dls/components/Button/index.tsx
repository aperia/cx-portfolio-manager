import React, {
  ButtonHTMLAttributes,
  useState,
  useEffect,
  useRef,
  useImperativeHandle
} from 'react';

// utils
import isFunction from 'lodash.isfunction';
import isBoolean from 'lodash.isboolean';
import pick from 'lodash.pick';
import classnames from 'classnames';
import { genAmtId } from '../../utils';
import { BtnVariants, BtnSizes } from './constant';

// components
import { useIsGroupButton } from '../GroupButton';
import Icon from '../Icon';

// hooks
import { useTooltipContext } from '../Tooltip';
import { usePopoverContext } from '../Popover';

export interface ButtonProps
  extends ButtonHTMLAttributes<HTMLButtonElement>,
    DLSId {
  variant?: BtnVariants;
  size?: BtnSizes;
  loading?: boolean;
  disabled?: boolean;
  selected?: boolean;
  autoBlur?: boolean;
}

const Button: React.RefForwardingComponent<HTMLButtonElement, ButtonProps> = (
  {
    variant = 'primary',
    size,
    loading,
    disabled,
    selected: selectedProp,
    autoBlur = true,
    children,
    onClick,
    className,

    id,
    dataTestId,
    ...props
  },
  ref
) => {
  // check is inside GroupButton or not
  const isGroupButton = useIsGroupButton();

  // states
  const [selected, setSelected] = useState(selectedProp);

  // refs
  const buttonRef = useRef<HTMLButtonElement | null>(null);

  // contexts
  const tooltipContextValue = useTooltipContext();
  const { setValue: tooltipSetValue } = pick(tooltipContextValue, ['setValue']);
  const popoverContextValue = usePopoverContext();
  const {
    value: popoverValue,
    opened: popoverOpened,
    setValue: popoverSetValue
  } = pick(popoverContextValue, ['value', 'setValue', 'opened']);

  const handleOnClick = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    // always trigger blur after click
    autoBlur && buttonRef.current!.blur();

    isFunction(onClick) && onClick(event);
    if (!isGroupButton) return;

    // controlled mode
    if (isBoolean(selectedProp)) return;

    // uncontrolled mode
    setSelected(!selected);
  };

  // provides ref
  useImperativeHandle(ref, () => buttonRef.current!);

  // set props
  useEffect(() => setSelected(selectedProp), [selectedProp]);

  // notify for TooltipContext/PopoverContext provider if Button is IconButton (add icon-button class)
  useEffect(() => {
    // ignore if Button is inside Popover element
    if (
      buttonRef.current?.closest('.dls-popper-container.icon-button') ||
      buttonRef.current?.closest('.dls-popup.dls-date-picker-popup')
    ) {
      return;
    }

    const childrenElement = children as React.ReactElement<unknown>;
    const isIconButton = childrenElement?.type === Icon;

    if (isFunction(tooltipSetValue)) {
      tooltipSetValue(isIconButton ? 'icon-button' : '');
    }

    if (isFunction(popoverSetValue)) {
      popoverSetValue(isIconButton ? 'icon-button' : '');
    }
  }, [children, tooltipSetValue, popoverSetValue]);

  let isActiveByPopoverOpened = false;
  if (buttonRef.current) {
    isActiveByPopoverOpened =
      !buttonRef.current.closest('.dls-popper-container.icon-button') &&
      popoverOpened &&
      popoverValue;
  }

  return (
    <button
      ref={buttonRef}
      disabled={loading || !!disabled}
      onClick={handleOnClick}
      className={classnames(
        `btn btn-${variant}`,
        size && `btn-${size}`,
        loading && 'loading loading-sm',
        selected || isActiveByPopoverOpened ? 'active' : '',
        className
      )}
      type="button"
      id={id}
      data-testid={genAmtId(dataTestId, 'dls-button', 'Button')}
      {...props}
    >
      {children}
    </button>
  );
};

export * from './constant';
export default React.forwardRef<HTMLButtonElement, ButtonProps>(Button);
