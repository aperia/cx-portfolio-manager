import React from 'react';
import {
  fireEvent,
  render,
  RenderResult,
  screen
} from '@testing-library/react';
import '@testing-library/jest-dom';

import AttributeSearchWrapper from './AttributeSearchWrapper';
import {
  AttributeSearchData,
  AttributeSearchProps,
  AttributeSearchValue,
  ChangeComponentEvent
} from '.';
import { MainSubConditionControl, MainSubConditionLevel } from './controls';

import { queryAllByClass, queryByClass } from '../../test-utils/queryHelpers';
import userEvent from '@testing-library/user-event';
import { act } from 'react-dom/test-utils';

jest.mock('../../hooks/useTranslation', () => () => ({
  t: (text: string) => text
}));

jest.mock('./hooks/useMouseTrap', () =>
  jest.requireActual('./mockUseMouseTrap')
);

Element.prototype.scroll = () => {};
Element.prototype.scrollBy = () => {};

let wrapper: RenderResult;

const mainSubConditionData: MainSubConditionLevel[] = [
  { key: '0', text: 'Moderate' },
  { key: '1', text: 'Low' },
  { key: '2', text: 'Exact' }
];

const lastNameField: AttributeSearchData = {
  key: 'lastNameCXC14',
  name: 'Last Name CXC-14',
  description: 'Last Name only',
  // eslint-disable-next-line react/display-name
  component: ({ value, ...props }: any) => {
    if (!value) {
      value = { level: mainSubConditionData[0], keyword: '' };
    }
    return (
      <MainSubConditionControl
        data={mainSubConditionData}
        inputProps={{ placeholder: 'Enter keyword' } as any}
        autoFocusDropdownList={false}
        dropdownListProps={{ placeholder: 'Select an option' } as any}
        autoFocusInput
        value={value}
        {...props}
      />
    );
  },
  groupName: 'CXC-14'
};

const renderComonent = (props: AttributeSearchProps) => {
  jest.useFakeTimers();

  wrapper = render(<AttributeSearchWrapper {...props} />);

  jest.runAllTimers();
};

const dataComponent = ({
  onSearch,
  onRemoveField,
  onNeedFocusInputFilter,
  onFocus,
  onBlur,
  onChange
}: any) => {
  return (
    <div data-testid="item" className="item">
      <input
        data-testid="item-onRemoveField"
        onChange={(e: any) => onRemoveField(e.target.prop)}
      />
      <div
        data-testid="item-onNeedFocusInputFilter"
        onClick={onNeedFocusInputFilter}
      />
      <input
        data-testid="item-input"
        onFocus={onFocus}
        onChange={(e: ChangeComponentEvent) => {
          onChange({ ...e, value: e.target?.value });
        }}
        onBlur={onBlur}
      />
      <div data-testid="item-onSearch" onClick={onSearch} />
    </div>
  );
};

const value = [
  {
    description: 'Max length 10 characters',
    groupName: 'Client Parameters',
    itemElement: null,
    key: 'companyName',
    name: 'Company Name',
    component: dataComponent
  }
];

const data = [
  {
    key: 'firstName',
    name: 'First Name',
    description: 'Max length 10 characters',
    component: dataComponent,
    groupName: 'Client Parameters'
  },
  {
    key: 'lastName',
    name: 'Last Name',
    description: 'Max length 10 characters',
    component: (props: any) => <div {...props} />,
    groupName: 'Client Parameters'
  }
];

const constraintFields = [
  {
    field: 'stateCXC14',
    requiredField: lastNameField,
    errorMessage: ''
  },
  {
    field: 'zipCodeCXC14',
    requiredField: lastNameField,
    errorMessage: ''
  }
];

const getNode = {
  get wrapper() {
    return queryAllByClass(
      wrapper.baseElement as HTMLElement,
      /dls-attribute-search/
    )[0]! as HTMLDivElement;
  },
  get inputFilter() {
    return queryByClass(
      wrapper.baseElement as HTMLElement,
      /dls-attribute-search-input-filter/
    )! as HTMLInputElement;
  },
  get clearBtn() {
    return queryByClass(
      wrapper.baseElement as HTMLElement,
      /ds-attr-clear-button/
    )! as HTMLInputElement;
  },
  get searchBtn() {
    return queryByClass(
      wrapper.baseElement as HTMLElement,
      /dls-attribute-search-button/
    )! as HTMLInputElement;
  },
  get searchItems() {
    return queryAllByClass(
      wrapper.baseElement as HTMLElement,
      /ds-attr-search-item(?!-)/
    )! as HTMLDivElement[];
  },
  get dropdown() {
    return queryByClass(
      wrapper.baseElement as HTMLElement,
      /dls-attribute-search-dropdown/
    )! as HTMLDivElement;
  },
  get searchFields() {
    return queryAllByClass(
      wrapper.baseElement as HTMLElement,
      /ds-attr-search-field/
    )! as HTMLDivElement[];
  },
  get inputElements() {
    return queryAllByClass(
      wrapper.baseElement as HTMLElement,
      /input-position(?!-)/
    )! as HTMLDivElement[];
  }
};

const userClick = (element: Element) => {
  act(() => {
    userEvent.click(element);
  });
  jest.runAllTimers();
};

describe('Render', () => {
  const value = [
    {
      key: 'firstName',
      name: 'First Name',
      description: 'Max length 10 characters',
      component: dataComponent,
      groupName: 'Client Parameters'
    },
    {
      key: 'lastName',
      name: 'Last Name',
      description: 'Max length 10 characters',
      component: (props: any) => <div {...props} />,
      groupName: 'Client Parameters'
    }
  ];

  it('should render with data', () => {
    const data = [
      {
        key: 'firstName',
        name: 'First Name',
        description: 'Max length 10 characters',
        component: dataComponent,
        groupName: 'Client Parameters'
      },
      {
        key: 'secondName',
        name: 'Second Name',
        description: 'Max length 10 characters',
        component: dataComponent,
        groupName: 'Client Parameters'
      },
      {
        key: 'lastName',
        name: 'Last Name',
        description: 'Max length 10 characters',
        component: (props: any) => <div {...props} />,
        groupName: 'Client Parameters',
        disabledFields: {
          firstName: 'disabled firstName',
          secondName: 'disabled secondName'
        }
      }
    ];

    renderComonent({
      constraintFields,
      value,
      data
    });

    // set data
    fireEvent.change(screen.getByTestId('ref-setValue'), {
      target: { value: 'value', optionalFields: value }
    });

    expect(getNode.wrapper).toBeInTheDocument();
  });

  it('should render with disabledFields', () => {
    renderComonent({
      constraintFields,
      value,
      data,
      disabledFields: [{ key: 'firstName', description: 'disabled fristName' }]
    });

    expect(getNode.wrapper).toBeInTheDocument();
  });

  it('should render without data', () => {
    renderComonent({ constraintFields });

    // set data
    fireEvent.change(screen.getByTestId('ref-setValue'), {
      target: { value: 'value', optionalFields: value }
    });

    expect(getNode.wrapper).toBeInTheDocument();
  });

  it('should render with value', () => {
    renderComonent({ constraintFields, value: [], data });

    // set data
    fireEvent.change(screen.getByTestId('ref-setValue'), {
      target: { value: 'value', optionalFields: value }
    });

    expect(getNode.wrapper).toBeInTheDocument();
  });

  it('should render with empty value', () => {
    renderComonent({ constraintFields, value: [], data });

    expect(getNode.wrapper).toBeInTheDocument();
  });

  it('should render and set show', () => {
    renderComonent({ constraintFields, value: [], data });

    // set show
    fireEvent.change(screen.getByTestId('ref-setShow'), {
      target: { value: 'value', show: true }
    });

    expect(getNode.wrapper).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('show dropdown and click outside', () => {
    const onFocusInputFilter = jest.fn();

    renderComonent({
      constraintFields,
      onFocusInputFilter,
      value: [],
      data
    });

    userClick(getNode.inputFilter);

    expect(getNode.dropdown).toBeInTheDocument();
    expect(onFocusInputFilter).toBeCalled();

    // click out side
    userClick(document.body);

    expect(getNode.dropdown).toBeNull();
  });

  describe('show dropdown, click items and search', () => {
    it('uncontrolled click on disabled item before click on item', () => {
      const onChange = jest.fn();
      renderComonent({
        constraintFields,
        // data,
        data: [
          data[0],
          {
            key: 'secondName',
            name: 'Second Name',
            description: 'Max length 10 characters',
            component: dataComponent,
            groupName: 'Client Parameters',
            disabledFields: { firstName: 'disabled' }
          },
          data[1]
        ],
        onChange,
        small: true
      });

      userClick(getNode.inputFilter);

      expect(getNode.dropdown).toBeInTheDocument();

      // click disabled item
      userClick(getNode.searchItems[1]);
      // click item [0]
      userClick(getNode.searchItems[0]);

      expect(onChange).toBeCalledTimes(1);

      expect(getNode.searchFields.length).toEqual(1);
    });

    it('uncontrolled', () => {
      const onChange = jest.fn();
      const onSearch = jest.fn();
      renderComonent({
        constraintFields,
        data,
        onChange,
        onSearch,
        small: true
      });

      userClick(getNode.inputFilter);

      expect(getNode.dropdown).toBeInTheDocument();

      // click item [0]
      userClick(getNode.searchItems[0]);
      // click item [1]
      userClick(getNode.searchItems[0]);

      expect(getNode.searchFields.length).toEqual(2);

      // coverage
      fireEvent.change(
        queryAllByClass(wrapper.container, /input-position mousetrap/)[1]!,
        { target: { value: 'value' } }
      );

      userClick(
        queryAllByClass(wrapper.container, /icon icon-close close-button/)[1]!
      );

      expect(getNode.searchFields.length).toEqual(1);
      expect(onChange).toBeCalled();
      onChange.mockClear();

      userClick(screen.getByTestId('item-onNeedFocusInputFilter'));

      // when dropdown is showed
      fireEvent.focus(screen.getByTestId('item-input'));
      userEvent.type(screen.getByTestId('item-input'), '123');
      userClick(screen.getByTestId('item-onSearch'));

      expect(onChange).toBeCalled();
      expect(onSearch).toBeCalled();

      // set error
      fireEvent.change(screen.getByTestId('ref-setError'), {
        target: { value: 'value', optionalError: { [data[0].key]: {} } }
      });

      // when dropdown is hided and error
      // click item [0]
      userClick(document.body);
      fireEvent.focus(screen.getByTestId('item-input'));
      userEvent.type(screen.getByTestId('item-input'), '123');

      expect(screen.queryByTestId('item-onSearch')).toBeNull();

      expect(onChange).toBeCalled();
    });

    describe('controlled', () => {
      it('when data is null', () => {
        const onChange = jest.fn();

        renderComonent({
          constraintFields,
          onChange,
          value: [],
          data
        });

        // set data
        fireEvent.change(screen.getByTestId('ref-setData'), {
          target: { value: 'value', optionalData: undefined }
        });

        userClick(getNode.inputFilter);

        expect(getNode.dropdown).toBeInTheDocument();

        expect(getNode.searchItems[0]).toBeUndefined();
      });

      it('when data is not empty', () => {
        const onChange = jest.fn();

        renderComonent({
          constraintFields,
          onChange,
          value: [],
          data
        });

        userClick(getNode.inputFilter);

        expect(getNode.dropdown).toBeInTheDocument();

        // click item
        userClick(getNode.searchItems[0]);

        expect(onChange).toBeCalled();
      });
    });
  });

  describe('handleAddField', () => {
    it('uncontrolled', () => {
      renderComonent({
        constraintFields,
        value,
        data
      });

      userClick(getNode.inputFilter);

      expect(getNode.dropdown).toBeInTheDocument();

      // click item
      userClick(getNode.searchItems[0]);

      expect(getNode.searchFields.length).toEqual(1);
    });

    it('controlled', () => {
      const onChange = jest.fn();

      renderComonent({
        constraintFields,
        onChange,
        value: [],
        data
      });

      userClick(getNode.inputFilter);

      expect(getNode.dropdown).toBeInTheDocument();

      // click item
      userClick(getNode.searchItems[0]);

      expect(getNode.searchFields.length).toEqual(1);

      jest.runAllTimers();
    });

    it('when has no value', () => {
      const onChange = jest.fn();

      renderComonent({
        constraintFields,
        onChange,
        data
      });

      userClick(getNode.inputFilter);

      expect(getNode.dropdown).toBeInTheDocument();

      // click item
      userClick(getNode.searchItems[0]);

      expect(getNode.searchFields.length).toEqual(1);

      jest.runAllTimers();
    });
  });

  describe('handleRemoveField', () => {
    it('uncontrolled', () => {
      renderComonent({
        constraintFields,
        value,
        data
      });

      // set data
      act(() => {
        fireEvent.change(screen.getByTestId('ref-setValue'), {
          target: { value: 'value', optionalFields: value }
        });
      });
      jest.runAllTimers();

      userClick(getNode.inputFilter);

      expect(getNode.searchFields.length).toEqual(1);

      // coverage
      fireEvent.change(screen.getByTestId('item-onRemoveField'), {
        target: { value: 'value', prop: undefined }
      });

      expect(getNode.searchFields.length).toEqual(1);
    });

    it('uncontrolled set empty value', () => {
      renderComonent({
        constraintFields,
        // onChange: () => {},
        value,
        data
      });

      // set data
      act(() => {
        fireEvent.change(screen.getByTestId('ref-setValue'), {
          target: { value: 'value', optionalFields: undefined }
        });
      });
      jest.runAllTimers();

      userClick(getNode.inputFilter);

      expect(getNode.searchFields.length).toEqual(0);
    });

    describe('controlled', () => {
      it('when params is undefined', () => {
        const onChange = jest.fn();

        renderComonent({
          constraintFields,
          onChange,
          value: [],
          data
        });

        userClick(getNode.inputFilter);

        expect(getNode.dropdown).toBeInTheDocument();

        // click item
        userClick(getNode.searchItems[0]);

        expect(getNode.searchFields.length).toEqual(1);

        // coverage
        fireEvent.change(screen.getByTestId('item-onRemoveField'), {
          target: { value: 'value', prop: undefined }
        });

        expect(getNode.searchFields.length).toEqual(0);

        jest.runAllTimers();
      });

      it('when params has position = 0', () => {
        const onChange = jest.fn();

        renderComonent({
          constraintFields,
          onChange,
          value: [],
          data
        });

        userClick(getNode.inputFilter);

        expect(getNode.dropdown).toBeInTheDocument();

        // click item
        userClick(getNode.searchItems[0]);

        expect(getNode.searchFields.length).toEqual(1);

        // coverage
        fireEvent.change(screen.getByTestId('item-onRemoveField'), {
          target: { value: 'value', prop: { byPosition: 0 } }
        });

        expect(getNode.searchFields.length).toEqual(0);

        jest.runAllTimers();
      });

      it('when params has right byFieldKey and has more than 1 error', () => {
        const onChange = jest.fn();

        renderComonent({
          constraintFields,
          validator: () => ({ [data[0].key]: 'error', [data[1].key]: 'error' }),
          onChange,
          // value: [],
          data
        });

        userClick(getNode.inputFilter);

        expect(getNode.dropdown).toBeInTheDocument();

        // click item
        userClick(getNode.searchItems[0]);

        expect(getNode.searchFields.length).toEqual(1);

        act(() => {
          // trigger validate
          fireEvent.focus(screen.queryByTestId('item-input')!);
          fireEvent.blur(screen.queryByTestId('item-input')!);
        });

        act(() => {
          // remove
          fireEvent.change(screen.getByTestId('item-onRemoveField'), {
            target: { value: 'value', prop: { byFieldKey: 'firstName' } }
          });
          jest.runAllTimers();
        });

        expect(getNode.searchFields.length).toEqual(0);
      });

      it('when params has right byFieldKey and has only 1 error', () => {
        const onChange = jest.fn();

        renderComonent({
          constraintFields,
          validator: () => ({ [data[0].key]: 'error' }),
          onChange,
          value: [],
          data
        });

        userClick(getNode.inputFilter);

        expect(getNode.dropdown).toBeInTheDocument();

        // click item
        userClick(getNode.searchItems[0]);

        expect(getNode.searchFields.length).toEqual(1);

        act(() => {
          // trigger validate
          fireEvent.focus(screen.queryByTestId('item-input')!);
          fireEvent.blur(screen.queryByTestId('item-input')!);
        });

        act(() => {
          // remove
          fireEvent.change(screen.getByTestId('item-onRemoveField'), {
            target: { value: 'value', prop: { byFieldKey: 'firstName' } }
          });
          jest.runAllTimers();
        });

        expect(getNode.searchFields.length).toEqual(0);
      });

      it('when params has wrong byFieldKey', () => {
        const onChange = jest.fn();

        renderComonent({
          constraintFields,
          onChange,
          value: [],
          data
        });

        userClick(getNode.inputFilter);

        expect(getNode.dropdown).toBeInTheDocument();

        // click item
        userClick(getNode.searchItems[0]);

        expect(getNode.searchFields.length).toEqual(1);

        // coverage
        fireEvent.change(screen.getByTestId('item-onRemoveField'), {
          target: { value: 'value', prop: { byFieldKey: 'firstName-wrong' } }
        });

        expect(getNode.searchFields.length).toEqual(0);

        jest.runAllTimers();
      });
    });
  });

  describe('handleClear', () => {
    it('controlled', () => {
      const onChange = jest.fn();
      const onClear = jest.fn();

      renderComonent({
        constraintFields,
        onChange,
        onClear,
        value,
        data
      });

      userClick(getNode.inputFilter);

      expect(getNode.dropdown).toBeInTheDocument();

      // click item
      userClick(getNode.searchItems[0]);
      userClick(getNode.clearBtn);

      expect(onChange).toBeCalled();
      expect(onClear).toBeCalled();
    });

    it('uncontrolled', () => {
      const onChange = jest.fn();
      const onClear = jest.fn();

      renderComonent({
        constraintFields,
        onChange,
        onClear,
        data
      });

      userClick(getNode.inputFilter);

      expect(getNode.dropdown).toBeInTheDocument();

      // click item
      userClick(getNode.searchItems[0]);
      userClick(getNode.clearBtn);

      expect(onChange).toBeCalled();
      expect(onClear).toBeCalled();
    });
  });

  describe('handleSearch', () => {
    it('uncontrolled', () => {
      const onChange = jest.fn();
      renderComonent({ constraintFields, data, onChange });

      userClick(getNode.inputFilter);

      expect(getNode.dropdown).toBeInTheDocument();

      // click item [0]
      userClick(getNode.searchItems[0]);

      userClick(screen.getByTestId('item-onSearch'));

      expect(onChange).toBeCalled();
    });

    it('controlled', () => {
      const onChange = jest.fn();
      const onSearch = jest.fn();
      renderComonent({ constraintFields, data, onChange, onSearch });

      userClick(getNode.inputFilter);

      expect(getNode.dropdown).toBeInTheDocument();

      // click item [0]
      userClick(getNode.searchItems[0]);

      userClick(screen.getByTestId('item-onSearch'));

      expect(onChange).toBeCalled();
      expect(onSearch).toBeCalled();
    });

    it('controlled when has error', () => {
      const onChange = jest.fn();
      const onSearch = jest.fn();
      renderComonent({
        constraintFields,
        data,
        onChange,
        onSearch,
        validator: () => ({ [data[0].key]: 'error' })
      });

      userClick(getNode.inputFilter);

      expect(getNode.dropdown).toBeInTheDocument();

      // click item [0]
      userClick(getNode.searchItems[0]);

      userClick(screen.getByTestId('item-onSearch'));

      expect(onChange).toBeCalled();
      expect(onSearch).toBeCalled();
    });
  });

  describe('mousetrap', () => {
    it('not focus on any item after select filter field', () => {
      const onChange = jest.fn();
      const onSearch = jest.fn();

      renderComonent({
        constraintFields,
        data,
        onChange,
        onSearch
      });

      userClick(getNode.inputFilter);

      userClick(getNode.searchItems[0]);

      // key down in wrapper
      userEvent.type(getNode.searchItems[0], '{arrowdown}');

      expect(queryByClass(wrapper.container, /selected/)).toBeNull();
    });

    it('press arrowdown when all items are enable', () => {
      const onChange = jest.fn();
      const onSearch = jest.fn();

      renderComonent({
        constraintFields,
        data,
        onChange,
        onSearch
      });

      userClick(getNode.inputFilter);

      expect(
        getNode.searchItems[0].classList.contains('selected')
      ).toBeTruthy();

      // key down in wrapper
      userEvent.type(getNode.wrapper, '{arrowdown}');

      expect(
        getNode.searchItems[0].classList.contains('selected')
      ).toBeTruthy();

      // key down in input filter
      userEvent.type(getNode.inputFilter, '{arrowdown}');

      expect(
        getNode.searchItems[1].classList.contains('selected')
      ).toBeTruthy();

      // key down in input filter
      userEvent.type(getNode.inputFilter, '{arrowdown}');

      expect(
        getNode.searchItems[1].classList.contains('selected')
      ).toBeTruthy();

      // userEvent.type(getNode.inputFilter, '{arrowup}');

      // userEvent.tab();
    });

    it('press arrowdown when has disabled item', () => {
      const onChange = jest.fn();
      const onSearch = jest.fn();

      const data = [
        {
          key: 'firstName',
          name: 'First Name',
          description: 'Max length 10 characters',
          component: dataComponent,
          groupName: 'Client Parameters'
        },
        {
          key: 'lastName',
          name: 'Last Name',
          description: 'Max length 10 characters',
          component: dataComponent,
          groupName: 'Client Parameters',
          itemElement: () => <div />
        },
        {
          key: 'secondName',
          name: 'Second Name',
          description: 'Max length 10 characters',
          component: dataComponent,
          groupName: 'Client Parameters',
          disabled: true
        }
      ];
      renderComonent({
        constraintFields,
        data,
        onChange,
        onSearch
      });

      userClick(getNode.inputFilter);

      expect(
        getNode.searchItems[0].classList.contains('selected')
      ).toBeTruthy();

      act(() => {
        fireEvent.focus(getNode.searchItems[1]);
        jest.runAllTimers();
      });

      // key down in input filter
      userEvent.type(getNode.inputFilter, '{arrowdown}');

      expect(
        getNode.searchItems[1].classList.contains('selected')
      ).toBeTruthy();

      // key down in input filter
      userEvent.type(getNode.inputFilter, '{arrowdown}');

      expect(
        getNode.searchItems[1].classList.contains('selected')
      ).toBeTruthy();
    });

    it('press up when all items are enable', () => {
      const onChange = jest.fn();
      const onSearch = jest.fn();

      renderComonent({
        constraintFields,
        data,
        onChange,
        onSearch
      });

      userClick(getNode.inputFilter);

      expect(
        getNode.searchItems[0].classList.contains('selected')
      ).toBeTruthy();

      // key up in wrapper
      userEvent.type(getNode.wrapper, '{arrowup}');

      expect(
        getNode.searchItems[0].classList.contains('selected')
      ).toBeTruthy();

      // select last item
      userEvent.type(getNode.inputFilter, '{arrowdown}');
      userEvent.type(getNode.inputFilter, '{arrowdown}');

      // key up in input
      userEvent.type(getNode.inputFilter, '{arrowup}');

      expect(
        getNode.searchItems[0].classList.contains('selected')
      ).toBeTruthy();
    });

    it('press arrowdown when has disabled item', () => {
      const onChange = jest.fn();
      const onSearch = jest.fn();

      const data = [
        {
          key: 'firstName',
          name: 'First Name',
          description: 'Max length 10 characters',
          component: dataComponent,
          groupName: 'Client Parameters',
          disabled: true
        },
        {
          key: 'lastName',
          name: 'Last Name',
          description: 'Max length 10 characters',
          component: dataComponent,
          groupName: 'Client Parameters',
          itemElement: () => <div />
        },
        {
          key: 'secondName',
          name: 'Second Name',
          description: 'Max length 10 characters',
          component: dataComponent,
          groupName: 'Client Parameters'
        }
      ];
      renderComonent({
        constraintFields,
        data,
        onChange,
        onSearch
      });

      userClick(getNode.inputFilter);

      expect(
        getNode.searchItems[0].classList.contains('selected')
      ).toBeTruthy();

      act(() => {
        fireEvent.focus(getNode.searchItems[1]);
        jest.runAllTimers();
      });

      // select last items
      userEvent.type(getNode.inputFilter, '{arrowdown}');
      userEvent.type(getNode.inputFilter, '{arrowdown}');
      userEvent.type(getNode.inputFilter, '{arrowdown}');

      // key down in input filter
      userEvent.type(getNode.inputFilter, '{arrowup}');

      expect(
        getNode.searchItems[1].classList.contains('selected')
      ).toBeTruthy();

      // key down in input filter
      userEvent.type(getNode.inputFilter, '{arrowup}');

      expect(
        getNode.searchItems[1].classList.contains('selected')
      ).toBeTruthy();
    });

    it('press up when all items are enable', () => {
      const onChange = jest.fn();
      const onSearch = jest.fn();

      renderComonent({
        constraintFields,
        data,
        onChange,
        onSearch
      });

      userClick(getNode.inputFilter);

      // select last item
      userEvent.type(getNode.inputFilter, '{arrowdown}');

      expect(getNode.searchFields.length).toEqual(0);

      // key tab
      userEvent.tab();

      expect(getNode.searchFields.length).toEqual(1);
    });

    it('press delete when all items are enable', () => {
      renderComonent({
        constraintFields,
        data,
        onChange: jest.fn(),
        onSearch: jest.fn()
      });

      userClick(getNode.inputFilter);

      userClick(getNode.searchItems[0]);

      expect(getNode.searchFields.length).toEqual(1);

      act(() => {
        userEvent.type(getNode.inputFilter, '{backspace}');
      });

      expect(getNode.searchFields.length).toEqual(0);

      userEvent.type(getNode.inputFilter, 'a');

      act(() => {
        userEvent.type(getNode.inputFilter, '{backspace}');
      });

      expect(getNode.searchFields.length).toEqual(0);
    });
  });

  describe('auto-scroll', () => {
    const mockGetBoundingClientRect = ({ step = 0 }) => {
      let increateNumber = 0;
      Element.prototype.getBoundingClientRect = () => {
        increateNumber += step;
        return {
          bottom: 0,
          height: 0,
          left: increateNumber,
          right: 0,
          top: 0,
          width: 0,
          x: 0,
          y: 0,
          toJSON: () => undefined
        };
      };
    };

    it('auto-scroll left to right when left position of focused input is out of attribute search', () => {
      const scrollBy = jest.fn();

      Element.prototype.scrollBy = scrollBy;

      // leftAttributeSearch > leftInput
      mockGetBoundingClientRect({ step: 1 });

      renderComonent({ constraintFields, data });

      userClick(getNode.inputFilter);

      userClick(getNode.searchItems[0]);

      fireEvent.focus(screen.getByTestId('item-input'));

      jest.runAllTimers();

      expect(scrollBy).toBeCalledWith({
        behavior: 'smooth',
        left: -10,
        top: 0
      });
    });

    it('auto-scroll right to left when right position of focused input is out of clear button', () => {
      const scrollBy = jest.fn();

      Element.prototype.scrollBy = scrollBy;

      // rightInput > leftClearBtn
      mockGetBoundingClientRect({ step: -1 });

      renderComonent({ constraintFields, data, onClear: jest.fn() });

      userClick(getNode.inputFilter);

      userClick(getNode.searchItems[0]);

      fireEvent.focus(screen.getByTestId('item-input'));

      jest.runAllTimers();

      expect(scrollBy).toBeCalledWith({
        behavior: 'smooth',
        left: 61,
        top: 0
      });
    });

    it('auto-scroll right to left when right position of focused input is out of search button', () => {
      const scrollBy = jest.fn();

      Element.prototype.scrollBy = scrollBy;

      // rightInput > leftSearchBtn
      mockGetBoundingClientRect({ step: -1 });

      renderComonent({ constraintFields, data });

      userClick(getNode.inputFilter);

      userClick(getNode.searchItems[0]);

      fireEvent.focus(screen.getByTestId('item-input'));

      jest.runAllTimers();

      expect(scrollBy).toBeCalledWith({
        behavior: 'smooth',
        left: 61,
        top: 0
      });
    });
  });

  describe('handleChangeComponent', () => {
    it('uncontrolled', () => {
      const onChange = jest.fn();
      renderComonent({
        constraintFields,
        data,
        value: [
          {
            key: 'firstName',
            name: 'First Name'
          } as unknown as AttributeSearchValue
        ]
      });

      fireEvent.change(screen.getByTestId('item-input'), {
        value: 'aaaa',
        target: { value: 'a' }
      });

      expect(onChange).not.toBeCalled();
    });

    describe('controlled', () => {
      it('without value', () => {
        const onChange = jest.fn();
        renderComonent({
          constraintFields,
          data,
          onChange
        });

        userClick(getNode.inputFilter);

        userClick(getNode.searchItems[0]);

        fireEvent.change(screen.getByTestId('item-input'), {
          value: 'aaaa',
          target: { value: 'a' }
        });
        fireEvent.change(screen.getByTestId('item-input'), {
          target: { value: '' }
        });

        expect(onChange).toBeCalled();
      });

      it('with value', () => {
        const onChange = jest.fn();
        renderComonent({
          constraintFields,
          data,
          value: [],
          onChange
        });

        userClick(getNode.inputFilter);

        userClick(getNode.searchItems[0]);

        fireEvent.change(screen.getByTestId('item-input'), {
          value: 'aaaa',
          target: { value: 'a' }
        });
        fireEvent.change(screen.getByTestId('item-input'), {
          target: { value: '' }
        });

        expect(onChange).toBeCalled();
      });
    });
  });
});
