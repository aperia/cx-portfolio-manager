import React from 'react';
import useDataMap from './useDataMap';

import { AttributeSearchData } from '..';
import { render, screen } from '@testing-library/react';

const Comp = ({ data }: { data: AttributeSearchData[] }) => {
  const result = useDataMap(data);

  return <div data-testid="useDataMap">{JSON.stringify(result)}</div>;
};

describe('useDataMap', () => {
  it('without data', () => {
    const data = undefined as unknown as AttributeSearchData[];

    render(<Comp data={data} />);

    const content = screen.getByTestId('useDataMap').textContent;

    expect(content).toEqual('');
  });

  it('with data', () => {
    const data = [
      { key: 'key_1', name: 'name_1' } as unknown as AttributeSearchData,
      { key: 'key_2', name: 'name_2' } as unknown as AttributeSearchData
    ];

    render(<Comp data={data} />);

    const content = screen.getByTestId('useDataMap').textContent!;

    expect(JSON.parse(content)).toEqual({
      [data[0].key]: data[0],
      [data[1].key]: data[1]
    });
  });
});
