import  { useEffect } from 'react';

export interface ObservePlaceholderHook {
  (
    observerElement: HTMLElement | null,
    observerClassName: string,

    placeholderElement: HTMLInputElement | null,
    placeholder: string
  ): void;
}

/**
 * Observe element to update placeholder
 * @param observerElement
 * @param placeholderElement
 * @param placeholder
 */
const useObserverPlaceholder: ObservePlaceholderHook = (
  observerElement,
  observerClassName,
  placeholderElement,
  placeholder
) => {
  useEffect(() => {
    if (!observerElement || !placeholderElement) return;

    const mutationCallback: MutationCallback = () => {
      const remainingItem = observerElement.querySelector(observerClassName);
      if (remainingItem) {
        placeholderElement.placeholder = placeholder;
        return;
      }
      placeholderElement.placeholder = '';
    };

    const mutationObserver = new MutationObserver(mutationCallback);
    mutationObserver.observe(observerElement, {
      childList: true
    });

    return () => mutationObserver.disconnect();
  }, [observerElement, observerClassName, placeholderElement, placeholder]);
};

export default useObserverPlaceholder;
