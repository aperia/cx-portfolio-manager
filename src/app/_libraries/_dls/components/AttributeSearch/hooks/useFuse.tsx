import { useMemo } from 'react';

import Fuse from 'fuse.js';

export interface FuseHook {
  <T>(data: T[]): Fuse<
    T,
    {
      shouldSort: true;
      threshold: number;
      location: number;
      distance: number;
      maxPatternLength: number;
      minMatchCharLength: number;
      keys: 'name'[];
    }
  >;
}

const useFuse: FuseHook = (data) => {
  const fuse = useMemo(() => {
    const instance = new Fuse(data, {
      shouldSort: true,
      threshold: 0.2,
      location: 0,
      distance: 100,
      maxPatternLength: 32,
      minMatchCharLength: 1,
      keys: ['name']
    });

    return instance;
  }, [data]);

  return fuse;
};

export default useFuse;
