import { useEffect, useRef } from 'react';

import get from 'lodash.get';
import Hammer from '@egjs/hammerjs';

const useHammerPan = () => {
  const hammerRef = useRef<HTMLElement | null>(null);

  useEffect(() => {
    if (!hammerRef.current) return;

    const hammerElement = hammerRef.current;
    const hammer = new (Hammer as any)(hammerElement);

    hammer.add(
      new Hammer.Pan({ direction: Hammer.DIRECTION_HORIZONTAL, threshold: 20 })
    );

    const handlePan = (event: HammerInput) => {
      hammerElement.classList.add('panning');

      // exception cases:
      //      - MultiSelect(element inside popup is focusing)
      const exceptionCase = document.activeElement?.closest(
        '.dls-attribute-search-popup'
      );

      if (
        document.activeElement &&
        (hammerElement.contains(document.activeElement) || exceptionCase)
      ) {
        const { left: leftHammer, width: widthHammer } =
          hammerElement.getBoundingClientRect();
        let { left: leftActive } =
          document.activeElement.getBoundingClientRect();
        const additionalEvent = get(event, 'additionalEvent');

        if (exceptionCase) {
          const popupId = exceptionCase.getAttribute('aria-owns');
          const wrapperElement = document.getElementById(popupId + '');
          const rect = wrapperElement?.getBoundingClientRect();
          rect && (leftActive = rect.left - 4);
        }

        // limit pan right
        if (
          leftHammer + widthHammer - 42 - 59 - 24 < leftActive &&
          additionalEvent !== 'panleft'
        ) {
          return;
        }

        // limit pan left
        if (leftHammer > leftActive && additionalEvent !== 'panright') {
          return;
        }
      }

      const direction = event.direction === Hammer.DIRECTION_LEFT ? 1 : -1;
      hammerElement.scrollLeft = hammerElement.scrollLeft + 12 * direction;
    };

    const handlePanEnd = () => {
      hammerElement.classList.remove('panning');
    };

    hammer.on('pan', handlePan);
    hammer.on('panend pancancel', handlePanEnd);

    return () => {
      hammer.off('pan', handlePan);
      hammer.off('panend pancancel', handlePanEnd);
    };
  }, []);

  return hammerRef;
};

export default useHammerPan;
