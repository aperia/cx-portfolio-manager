import React from 'react';
import { render, screen } from '@testing-library/react';

import useMouseTrap from './useMouseTrap';

const Comp = ({ element }: { element: HTMLElement }) => {
  const result = useMouseTrap(element);

  return <div data-testid="useMouseTrap">{JSON.stringify(result)}</div>;
};

it('useMouseTrap', () => {
  const element = document.createElement('div');

  render(<Comp element={element} />);

  const content = screen.getByTestId('useMouseTrap').textContent!;

  expect(JSON.parse(content)).toEqual({
    _callbacks: {},
    _directMap: {},
    target: {}
  });
});
