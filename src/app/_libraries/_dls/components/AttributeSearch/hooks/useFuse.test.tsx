import React from 'react';
import { render, screen } from '@testing-library/react';

import useFuse from './useFuse';

const Comp = ({ data }: { data: Record<string, string>[] }) => {
  const result = useFuse(data);

  return <div data-testid="useFuse">{JSON.stringify(result)}</div>;
};

describe('useFuse', () => {
  it('with data', () => {
    const data = [{ key: 'key_1', name: 'name_1' }];

    render(<Comp data={data} />);

    const content = screen.getByTestId('useFuse').textContent!;

    expect(JSON.parse(content)).toEqual(
      expect.objectContaining({
        list: data,
        options: {
          distance: 100,
          findAllMatches: false,
          id: null,
          includeMatches: false,
          includeScore: false,
          isCaseSensitive: false,
          keys: ['name'],
          location: 0,
          matchAllTokens: false,
          maxPatternLength: 32,
          minMatchCharLength: 1,
          shouldSort: true,
          threshold: 0.2,
          tokenSeparator: {},
          tokenize: false,
          verbose: false
        }
      })
    );
  });
});
