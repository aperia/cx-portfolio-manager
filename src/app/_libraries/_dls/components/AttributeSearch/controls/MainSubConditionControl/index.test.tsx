import '@testing-library/jest-dom';
import { act, fireEvent, render, RenderResult } from '@testing-library/react';
import UserEvent from '@testing-library/user-event';
import React from 'react';
import '../../../../test-utils/mocks/mockCanvas';
import MainSubConditionControlWraper from '../../../../test-utils/mocks/mockMainSubControlWrapper';
import { keycode } from '../../../../utils';
import MainSubConditionControl, {
  MainSubConditionChangeEvent,
  MainSubConditionLevel,
  MainSubConditionProps
} from './';

jest.mock('../../../../utils', () => {
  const actualModule = jest.requireActual('../../../../utils');
  return {
    ...actualModule,
    canvasTextWidth: (text: string) => {
      if (text === 'empty') return null;
      return { width: 10 } as TextMetrics;
    }
  };
});

const mockMainSubConditionData: MainSubConditionLevel[] = [
  { key: '0', text: 'Moderate' },
  { key: '1', text: 'Low' },
  { key: '2', text: 'empty' },
  { key: '3', text: 'Mock ' }
];

describe('Test AttributeSearch controls > MainSubConditionControl', () => {
  const mockMainSubConditionProps = {
    itemKey: 'mock-item-key',
    data: mockMainSubConditionData,
    inputProps: { placeholder: 'Enter value' },
    dropdownListProps: { placeholder: 'Select value' }
  } as MainSubConditionProps;

  const renderMainSubConditionControl = (
    props?: MainSubConditionProps
  ): RenderResult => {
    return render(
      <MainSubConditionControl {...mockMainSubConditionProps} {...props} />
    );
  };

  beforeEach(() => {
    jest.useFakeTimers();
  });

  afterEach(() => {
    jest.useRealTimers();
    jest.clearAllMocks();
  });

  it('should render MainSubConditionControl UI without default data', async () => {
    const wrapper = renderMainSubConditionControl({
      data: undefined,
      autoFocusDropdownList: undefined,
      autoFocusInput: undefined
    } as MainSubConditionProps);
    const { getByText } = wrapper;
    jest.runAllTimers();
    expect(getByText(/Select value/i)).toBeInTheDocument();
    const popupElement = wrapper.baseElement.querySelector(
      '.dls-popup'
    ) as HTMLDivElement;
    expect(popupElement).toBeInTheDocument();
  });

  it('should render MainSubConditionControl UI with default level data', async () => {
    const wrapper = renderMainSubConditionControl({
      data: undefined,
      value: {
        level: mockMainSubConditionData[0],
        keyword: ''
      },
      autoFocusDropdownList: true,
      autoFocusInput: true,
      allowFieldAutoFocus: false
    } as MainSubConditionProps);
    const { getByText } = wrapper;

    jest.runAllTimers();

    // default select for level is Moderate
    expect(getByText(/Moderate/i)).toBeInTheDocument();

    // open level popup
    const inputElement = wrapper.baseElement.querySelector(
      '.text-field-container'
    );
    fireEvent.mouseDown(inputElement!);
    act(() => {
      jest.runAllTimers();
    });

    // expect to see Low item
    expect(getByText('Low')).toBeInTheDocument();

    fireEvent.click(getByText('Low').parentElement!);

    const valueElement = wrapper.baseElement.querySelector(
      '.text-field-container > .input'
    );
    expect(valueElement?.innerHTML).toEqual(
      `<span class=\"dls-ellipsis\">${mockMainSubConditionData[0].text}</span>`
    );

    const inputControl = wrapper.baseElement.querySelector(
      '.input-control'
    ) as HTMLInputElement;
    fireEvent.change(inputControl, { target: { value: 'some keywork' } });
    expect(inputControl.value).toEqual('');
  });

  it('should render MainSubConditionControl UI > handle on change event', async () => {
    const changeFn = jest.fn();
    const wrapper = renderMainSubConditionControl({
      data: undefined,
      value: {
        level: mockMainSubConditionData[0],
        keyword: ''
      },
      autoFocusDropdownList: false,
      autoFocusInput: true,
      onChange: changeFn as MainSubConditionChangeEvent
    } as MainSubConditionProps);
    const { getByText } = wrapper;

    jest.runAllTimers();

    // default select for level is Moderate
    expect(getByText(/Moderate/i)).toBeInTheDocument();

    // open level popup
    const inputElement = wrapper.baseElement.querySelector(
      '.text-field-container'
    );
    fireEvent.mouseDown(inputElement!);
    act(() => {
      jest.runAllTimers();
    });

    // expect to see Low item
    expect(getByText('Low')).toBeInTheDocument();

    fireEvent.click(getByText('Low').parentElement!);

    expect(changeFn).toHaveBeenCalledWith({
      value: {
        level: mockMainSubConditionData.find((s) => s.text === 'Low'),
        keyword: ''
      }
    });

    changeFn.mockClear();

    const inputControl = wrapper.baseElement.querySelector(
      '.input-control'
    ) as HTMLInputElement;
    fireEvent.change(inputControl, { target: { value: 'some keywork' } });

    expect(changeFn).toHaveBeenCalledWith({
      value: {
        level: mockMainSubConditionData[0],
        keyword: 'some keywork'
      }
    });
  });

  it('should render MainSubConditionControl UI > handle dynamic width drowpdown', async () => {
    let wrapper = renderMainSubConditionControl({
      data: undefined,
      dropdownListProps: { placeholder: '' }
    } as MainSubConditionProps);

    jest.runAllTimers();
    // default select for level is empty
    let valueElement = wrapper.baseElement.querySelector(
      '.text-field-container > .input'
    );
    expect(valueElement?.innerHTML).toEqual(
      `<span class=\"dls-ellipsis\"></span>`
    );
    wrapper.unmount();

    wrapper = renderMainSubConditionControl({
      data: undefined,
      value: {
        level: mockMainSubConditionData[3],
        keyword: ''
      },
      dropdownListProps: { placeholder: '' }
    } as MainSubConditionProps);

    jest.runAllTimers();

    // default select for level is empty
    valueElement = wrapper.baseElement.querySelector(
      '.text-field-container > .input'
    );
    expect(valueElement?.innerHTML).toEqual(
      `<span class=\"dls-ellipsis\">Mock </span>`
    );

    wrapper.unmount();

    wrapper = renderMainSubConditionControl({
      data: undefined,
      value: {
        level: mockMainSubConditionData[2],
        keyword: ''
      },
      dropdownListProps: { placeholder: '' }
    } as MainSubConditionProps);

    jest.runAllTimers();

    // default select for level is empty
    valueElement = wrapper.baseElement.querySelector(
      '.text-field-container > .input'
    );
    expect(valueElement?.innerHTML).toEqual(
      `<span class=\"dls-ellipsis\">empty</span>`
    );
  });

  it('handle key press BACKSPACE or DELETE on input control', async () => {
    const removeFieldFn = jest.fn();

    let wrapper = renderMainSubConditionControl({
      value: {
        level: mockMainSubConditionData[0],
        keyword: ''
      },
      onRemoveField: removeFieldFn as any
    } as MainSubConditionProps);

    let inputControl = wrapper.baseElement.querySelector(
      'input.input-control'
    ) as HTMLInputElement;

    fireEvent.keyDown(inputControl, { keyCode: keycode.DELETE });
    expect(removeFieldFn).toHaveBeenCalled();

    removeFieldFn.mockClear();
    wrapper.unmount();

    wrapper = renderMainSubConditionControl({
      value: {
        level: mockMainSubConditionData[0],
        keyword: '123'
      },
      onRemoveField: removeFieldFn as any
    } as MainSubConditionProps);

    inputControl = wrapper.baseElement.querySelector(
      'input.input-control'
    ) as HTMLInputElement;

    fireEvent.keyDown(inputControl, { keyCode: keycode.BACKSPACE });

    expect(removeFieldFn).not.toHaveBeenCalled();
  });

  it('handle focus input control, by press ENTER to select dropdownlist', async () => {
    const wrapper = renderMainSubConditionControl({
      value: {
        level: mockMainSubConditionData[0],
        keyword: ''
      }
    } as MainSubConditionProps);

    const container = wrapper.baseElement.querySelector(
      '.text-field-container'
    ) as HTMLDivElement;

    fireEvent.keyDown(container, { keyCode: keycode.ENTER });

    const input = wrapper.baseElement.querySelector(
      '.input-control'
    ) as HTMLInputElement;
    expect(input).toHaveFocus();
  });

  it('prevent trigger internal behavior of dropdownlist', async () => {
    const wrapper = renderMainSubConditionControl({
      value: {
        level: mockMainSubConditionData[0],
        keyword: ''
      },
      allowFieldAutoFocus: true,
      autoFocusDropdownList: true
    } as MainSubConditionProps);

    act(() => {
      jest.runAllTimers();
    });

    fireEvent.mouseDown(wrapper.baseElement.querySelector('.dls-popup')!);
    fireEvent.mouseDown(wrapper.getByText('Low').parentElement!);

    const inputElement = wrapper.baseElement.querySelector(
      '.text-field-container'
    ) as HTMLDivElement;

    fireEvent.blur(inputElement);

    const valueElement = wrapper.baseElement.querySelector(
      '.text-field-container > .input'
    ) as HTMLDivElement;
    expect(valueElement.innerHTML).toEqual(
      `<span class=\"dls-ellipsis\">${mockMainSubConditionData[0].text}</span>`
    );
  });

  it('handle mouse down on DropdownList input', () => {
    const wrapper = renderMainSubConditionControl({
      value: {
        level: mockMainSubConditionData[0],
        keyword: ''
      },
      allowFieldAutoFocus: true,
      autoFocusDropdownList: true
    } as MainSubConditionProps);

    const inputElement = wrapper.baseElement.querySelector(
      '.text-field-container'
    ) as HTMLDivElement;

    fireEvent.mouseDown(inputElement);

    act(() => {
      jest.runAllTimers();
    });

    const input = wrapper.baseElement.querySelector(
      '.input-control'
    ) as HTMLInputElement;
    expect(input).toHaveFocus();
  });

  it('handle blur on both DropdownList and Input', async () => {
    const blurFn = jest.fn();

    const wrapper = renderMainSubConditionControl({
      value: {
        level: mockMainSubConditionData[0],
        keyword: ''
      },
      allowFieldAutoFocus: true,
      autoFocusDropdownList: false,
      autoFocusInput: true,
      onBlur: blurFn as () => void
    } as MainSubConditionProps);

    const input = wrapper.baseElement.querySelector(
      '.input-control'
    ) as HTMLInputElement;

    expect(input).toHaveFocus();

    UserEvent.tab();
    act(() => {
      jest.runAllTimers();
    });
    expect(blurFn).toHaveBeenCalled();
  });

  it('handle change keywork on Input', async () => {
    const props = {
      ...mockMainSubConditionProps,
      value: {
        level: mockMainSubConditionData[0],
        keyword: ''
      },
      allowFieldAutoFocus: true,
      autoFocusDropdownList: false,
      autoFocusInput: true
    };

    const wrapper: RenderResult = render(
      <MainSubConditionControlWraper {...props} />
    );

    const input = wrapper.baseElement.querySelector(
      '.input-control'
    ) as HTMLInputElement;

    expect(input).toHaveFocus();

    fireEvent.change(input, {
      target: { value: 'mock-keywork', selectionStart: 12 }
    });

    act(() => {
      jest.runAllTimers();
    });

    expect(input.value).toEqual('mock-keywork');
    expect(input.selectionStart).toEqual(12);
  });
});
