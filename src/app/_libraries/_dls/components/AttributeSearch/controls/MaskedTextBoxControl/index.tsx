import React, { useRef, useImperativeHandle, useLayoutEffect } from 'react';

// utils
import classnames from 'classnames';
import { canvasTextWidth, keycode } from '../../../../utils';
import isString from 'lodash.isstring';
import isNil from 'lodash.isnil';
import isFunction from 'lodash.isfunction';
import get from 'lodash.get';
import unset from 'lodash.unset';

// components
import MaskedTextBox, {
  MaskedTextBoxProps,
  MaskedTextBoxRef
} from '../../../MaskedTextBox';

export interface MaskedTextBoxControlProps extends MaskedTextBoxProps, DLSId {
  itemKey?: string;
  onRemoveField: (config: {
    byPosition?: number;
    byFieldKey?: string;
    byAction?: 'click' | 'keyboard';
  }) => void;
  onNeedFocusInputFilter: () => void;
  onSearch: () => void;
  /** 4 required properties (above) must be handled separately for each component type  */
  anchor?: Element;
  allowFieldAutoFocus?: boolean;
  placeholder?: string;
}

const MaskedTextBoxControl: React.RefForwardingComponent<
  MaskedTextBoxRef,
  MaskedTextBoxControlProps
> = (
  {
    itemKey,
    onRemoveField,
    /** 4 required properties (above) must be handled separately for each component type  */
    value,
    onChange,
    allowFieldAutoFocus,
    placeholder,
    className,
    dataTestId,
    ...props
  },
  ref
) => {
  // handle eslint no-unused-var, can't destructor params above
  unset(props, 'onNeedFocusInputFilter');
  unset(props, 'onSearch');
  unset(props, 'anchor');

  const maskedTextBoxRef = useRef<MaskedTextBoxRef | null>(null);
  value = isNil(value) ? '' : value;

  // handle attributesearch Input keyboard logic: Enter for search, BackSpace/Delete for remove field
  const handleOnKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    // mousetrap handled enter keyboard, not need to handle here

    // handle backspace/delete keyboard
    if (
      value ||
      (event.keyCode !== keycode.BACKSPACE && event.keyCode !== keycode.DELETE)
    ) {
      return;
    }

    isFunction(onRemoveField) && onRemoveField({ byFieldKey: itemKey });
  };

  useImperativeHandle(ref, () => maskedTextBoxRef.current!);

  // handle dynamic input width
  useLayoutEffect(() => {
    const inputElement = get(
      maskedTextBoxRef.current,
      'inputElement'
    ) as HTMLInputElement;

    const extraWidth =
      isString(value) && value[value.length - 1] === ' ' ? 16 : 4;
    const textMetrics = canvasTextWidth((value as string) || placeholder || '');
    if (!textMetrics) return;

    inputElement.style.width = textMetrics.width + extraWidth + 'px';
  }, [value, placeholder]);

  return (
    <MaskedTextBox
      ref={maskedTextBoxRef as any}
      value={value}
      onChange={onChange}
      onKeyDown={handleOnKeyDown}
      autoFocus={allowFieldAutoFocus}
      placeholder={placeholder}
      className={classnames(className, 'input-control')}
      dataTestId={dataTestId}
      {...props}
    ></MaskedTextBox>
  );
};

export default React.forwardRef<MaskedTextBoxRef, MaskedTextBoxControlProps>(
  MaskedTextBoxControl
);
