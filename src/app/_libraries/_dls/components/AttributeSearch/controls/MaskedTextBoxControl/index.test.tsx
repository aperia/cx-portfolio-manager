import React from 'react';
import '@testing-library/jest-dom';

// components
import MaskedTextBoxControl, { MaskedTextBoxControlProps } from '.';

import { queryByRole, render, RenderResult } from '@testing-library/react';
import { queryByClass } from '../../../../test-utils/queryHelpers';

// mocks
import '../../../../test-utils/mocks/mockCanvas';
import userEvent from '@testing-library/user-event';

// utils
import * as canvasTextWidth from '../../../../utils/canvasTextWidth';
import { act } from 'react-dom/test-utils';

let wrapper: RenderResult;

const mask = [/\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

const renderComponent = (props: Partial<MaskedTextBoxControlProps>) => {
  jest.useFakeTimers();

  wrapper = render(
    <MaskedTextBoxControl ref={{ current: null }} mask={mask} {...props} />
  );

  jest.runAllTimers();
};

const getNode = {
  get trigger() {
    return queryByClass(wrapper.container, /dls-popper-trigger/)!;
  },
  get input() {
    return queryByRole(wrapper.container, 'textbox')! as HTMLInputElement;
  }
};

describe('Render', () => {
  it('should render', () => {
    const value = '123-45-6789 ';
    renderComponent({ value });

    expect(getNode.trigger).toBeInTheDocument();
  });

  it('should render when text is null', () => {
    const spy = jest
      .spyOn(canvasTextWidth, 'default')
      .mockImplementation(() => null);

    renderComponent({});

    expect(getNode.trigger).toBeInTheDocument();

    spy.mockReset();
    spy.mockRestore();
  });
});

describe('Actions', () => {
  describe('handleOnKeyDown', () => {
    it('when press delete', () => {
      const itemKey = 'itemKey';
      const onRemoveField = jest.fn();

      renderComponent({ onRemoveField, itemKey });

      act(() => {
        userEvent.click(getNode.trigger);
        userEvent.type(getNode.input, `{delete}`);
      });

      expect(onRemoveField).toBeCalledWith({ byFieldKey: itemKey });
    });

    it('when press backspace', () => {
      const itemKey = 'itemKey';
      const onRemoveField = jest.fn();

      renderComponent({ onRemoveField, itemKey });

      act(() => {
        userEvent.click(getNode.trigger);
        userEvent.type(getNode.input, `{backspace}`);
      });

      expect(onRemoveField).toBeCalledWith({ byFieldKey: itemKey });
    });

    it('when press text', () => {
      const itemKey = 'itemKey';
      const onRemoveField = jest.fn();

      renderComponent({ onRemoveField, itemKey });

      act(() => {
        userEvent.click(getNode.trigger);
        userEvent.type(getNode.input, `text`);
      });

      expect(onRemoveField).not.toBeCalled();
    });
  });
});
