import isEqualGenerator from './isEqualGenerator';

it('isEqualGenerator', () => {
  const equalGenerator = isEqualGenerator('by');

  expect(
    equalGenerator({ by: '1' }, undefined as unknown as Record<string, any>)
  ).toEqual(false);

  expect(
    equalGenerator(undefined as unknown as Record<string, any>, {
      by: '1'
    })
  ).toEqual(false);

  expect(equalGenerator({ by: '1' }, { by: '1' })).toEqual(true);
});
