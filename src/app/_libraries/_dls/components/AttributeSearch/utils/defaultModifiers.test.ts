import defaultModifiers from './defaultModifiers';

describe('defaultModifiers', () => {
  it('when left reference greater than left container', () => {
    const container = {
      getBoundingClientRect: () => ({ left: 0 })
    } as unknown as HTMLDivElement;
    const modifiers = defaultModifiers(container);
    const popperConfig = {
      state: {
        elements: {
          reference: { getBoundingClientRect: () => ({ left: 10 }) }
        },
        modifiersData: { popperOffsets: { x: 0 } }
      }
    };

    modifiers[0].fn(popperConfig);

    expect(popperConfig.state.modifiersData.popperOffsets.x).toEqual(0);
  });

  it('when left reference less than left container', () => {
    const container = {
      getBoundingClientRect: () => ({ left: 100 })
    } as unknown as HTMLDivElement;
    const modifiers = defaultModifiers(container);
    const popperConfig = {
      state: {
        elements: {
          reference: { getBoundingClientRect: () => ({ left: 10 }) }
        },
        modifiersData: { popperOffsets: { x: 0 } }
      }
    };

    modifiers[0].fn(popperConfig);

    expect(popperConfig.state.modifiersData.popperOffsets.x).toEqual(100);
  });
});
