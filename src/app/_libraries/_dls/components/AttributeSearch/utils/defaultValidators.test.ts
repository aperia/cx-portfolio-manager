import defaultValidators from './defaultValidators';

describe('defaultValidators', () => {
  it('is number', () => {
    expect(defaultValidators.number('1')).toEqual(true);
    expect(defaultValidators.number('a')).toEqual(false);
  });

  it('is string', () => {
    expect(defaultValidators.string('1')).toEqual(true);
    expect(defaultValidators.string(1)).toEqual(false);
  });

  it('is datetime', () => {
    expect(defaultValidators.datetime(new Date())).toEqual(true);
    expect(defaultValidators.datetime('21/03/2021')).toEqual(false);
  });

  it('is boolean', () => {
    expect(defaultValidators.boolean(true)).toEqual(true);
    expect(defaultValidators.boolean('true')).toEqual(false);
  });

  it('is required', () => {
    expect(defaultValidators.required('required')).toEqual(false);
    expect(defaultValidators.required(null)).toEqual(true);
  });
});
