const defaultModifiers = (
  container: HTMLDivElement,
  haveBtnClear = false,
  isSmall = false
): any => {
  return [
    {
      name: 'hide',
      enabled: true,
      phase: 'main',
      fn: (popperConfig: any) => {
        const { reference } = popperConfig.state.elements;

        const { left: leftReference } = reference.getBoundingClientRect();
        const { left: leftContainer } = container.getBoundingClientRect();

        // console.log('leftReference...', leftReference, reference);
        // console.log('leftContainer...', leftContainer, container);

        if (leftReference < leftContainer) {
          popperConfig.state.modifiersData.popperOffsets.x = leftContainer;
        }
      }
    },
    {
      name: 'flip',
      options: {
        altBoundary: false
      }
    }
  ];
};

export default defaultModifiers;
