import React from 'react';
import '@testing-library/jest-dom';

// components
import Header from './Header';
import { render, screen } from '@testing-library/react';

describe('Render', () => {
  it('should render with children', () => {
    const children = 'header children';

    render(<Header>{children}</Header>);

    expect(screen.getByText(children)).toBeInTheDocument();
  });

  it('should render with name', () => {
    const name = 'header name';

    render(<Header name={name}></Header>);

    expect(screen.getByText(name)).toBeInTheDocument();
  });
});
