import { render } from '@testing-library/react';
import React from 'react';
import { act } from 'react-dom/test-utils';
import { queryByClass } from '../../test-utils';
import Toggle from './Toggle';

describe('InfoBar Toggle component', () => {
  it('should call onExpand', () => {
    const spyOnExpand = jest.fn();
    Object.defineProperty(window, 'innerWidth', {
      value: 1280
    });

    jest.useFakeTimers();

    act(() => {
      const wrapper = render(
        <Toggle expand={false} onCollapse={jest.fn()} onExpand={spyOnExpand} />
      );

      jest.runAllTimers();

      const ele = queryByClass(wrapper.container, 'infobar-nav-item');
      ele?.click();

      expect(spyOnExpand).toBeCalled();
    });
  });

  it('should call onCollapse', () => {
    const spyOnCollapse = jest.fn();
    Object.defineProperty(window, 'innerWidth', {
      value: 1280
    });

    jest.useFakeTimers();

    act(() => {
      const wrapper = render(
        <Toggle expand={true} onCollapse={spyOnCollapse} onExpand={jest.fn()} />
      );

      jest.runAllTimers();

      const ele = queryByClass(wrapper.container, 'infobar-nav-item');
      ele?.click();

      expect(spyOnCollapse).toBeCalled();
    });
  });
});
