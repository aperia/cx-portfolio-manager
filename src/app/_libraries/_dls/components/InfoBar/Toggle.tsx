import React from 'react';

// types
import Tooltip, { TooltipProps } from '../Tooltip';

// utils
import classnames from 'classnames';
import { className } from '../../utils';
import Icon from '../Icon';

// hooks
// import { useScreenType } from '../../hooks';
// import { ScreenType } from '../../hooks/useScreenType/types';

export interface ToggleProps extends TooltipProps, DLSId {
  expand?: boolean;
  onCollapse: () => false | void;
  onExpand: () => false | void;
  className?: string;
}

const Toggle: React.FC<ToggleProps> = (props) => {
  const {
    expand,
    onCollapse,
    onExpand,
    className: classNameProp,

    dataTestId,
    ...tooltipProps
  } = props;

  return (
    <Tooltip
      variant={tooltipProps?.variant || 'primary'}
      placement={tooltipProps?.placement || 'right'}
      element={tooltipProps?.element}
      triggerClassName={classnames(
        className.infoBar.MTA,
        className.infoBar.NAV_ITEM_TOGGLE
      )}
      dataTestId={`${dataTestId}-expand-collapse-tooltip`}
      {...tooltipProps}
    >
      <div
        className={classnames(className.infoBar.NAV_ITEM, classNameProp)}
        onClick={expand ? onCollapse : onExpand}
        data-testid={`${dataTestId}-expand-collapse-button`}
      >
        <Icon name={expand ? 'push-right' : 'push-left'} size="6x" />
      </div>
    </Tooltip>
  );
};

export default Toggle;
