import React, { useMemo, ReactType, ReactNode, MouseEvent } from 'react';
import classnames from 'classnames';
import isFunction from 'lodash.isfunction';

import { className, genAmtId } from '../../utils';
import Icon, { IconProps } from '../../components/Icon';
import Tooltip from '../../components/Tooltip';

export type InfoBarMode = 'static' | 'dropdown' | 'default' | 'floating';

export interface OnToggleEvent {
  (event: MouseEvent<HTMLDivElement> & { value: string }): void;
}

export interface MappedItems {
  navItems: ReactNode[];
  bodyItems: ReactNode[];
  dropdownItems: ReactNode[];
}

export interface ComponentProps {
  handlePin: () => void;
  handleUnpin: () => void;
  handleExpand: () => void;
  handleCollapse: () => void;
  itemSelected?: string;
  itemIdPinned?: string;
  id: string;
  showDropdown?: boolean;
}

export interface InfoBarItem {
  id: string;
  name: string;
  icon?: IconProps;
  onClickIcon?: (value?: any) => void;
  component?: ReactType<ComponentProps>;
  mode?: InfoBarMode;
  tooltipPlacement?: 'left' | 'right';
  showTooltip?: boolean;
  keepComponent?: boolean;
  separator?: boolean;
}

export interface UseInfoBarItems {
  (config: {
    items: InfoBarItem[];
    itemSelected?: string;
    itemIdPinned?: string;
    itemDropdown?: string;
    expand: boolean;
    onSelectedItem: (item: string) => void;
    onClickItem: (item: string) => void;
    handleExpand: () => void;
    handleCollapse: () => void;
    handlePin: (id: string) => void;
    handleUnpin: () => void;
    emptyElement?: ReactNode;
    dataTestId?: string;
  }): MappedItems;
}

const useInfoBarItems: UseInfoBarItems = ({
  items,
  itemSelected,
  itemIdPinned,
  itemDropdown,
  expand,
  onSelectedItem,
  onClickItem,
  handleExpand,
  handleCollapse,
  handlePin,
  handleUnpin,
  emptyElement,
  dataTestId
}) => {
  const { navItemElements, dropdownElements } = useMemo(() => {
    const navItemElements: ReactNode[] = [];
    const dropdownElements: ReactNode[] = [];

    for (let index = 0; index < items.length; index++) {
      const {
        id,
        name,
        icon,
        onClickIcon,
        mode = 'default',
        tooltipPlacement = 'right',
        showTooltip,
        component: Component,
        separator
      } = items[index];

      const active = id === itemSelected;
      const activeDropDown = id === itemDropdown;
      const isPinned = id === itemIdPinned;
      let anchor: HTMLElement | null;

      if (mode === 'floating') continue;

      if (mode === 'dropdown' && Component) {
        dropdownElements.push(
          <Component
            key={index}
            id={id}
            active={activeDropDown}
            onActive={onClickItem}
            {...({} as any)}
          />
        );
      }

      navItemElements.push(
        <Tooltip
          key={id}
          variant="primary"
          placement={tooltipPlacement}
          element={name}
          opened={showTooltip}
          dataTestId={genAmtId(dataTestId, `${name}-tooltip`, 'InfoBar.Item')}
        >
          <div
            className={classnames(className.infoBar.NAV_ITEM, {
              pinned: isPinned,
              active,
              [className.infoBar.ACTIVE_DROPDOWN]: activeDropDown
            })}
            key={index}
            id={id}
            data-testid={genAmtId(dataTestId, `${name}-info-bar-item`, 'InfoBar.Item')}
          >
            {icon && (
              <Icon
                key={id}
                name={icon.name}
                size={icon.size || '6x'}
                onClick={() => {
                  switch (mode) {
                    case 'default':
                      return onSelectedItem(id);
                    case 'dropdown':
                      return process.nextTick(
                        () => !activeDropDown && onClickItem(id)
                      );
                    case 'static':
                    default:
                      isFunction(onClickIcon) && onClickIcon(anchor);
                      break;
                  }
                }}
              />
            )}
          </div>
        </Tooltip>
      );

      separator &&
        navItemElements.push(
          <div
            key={`${id}-infobar-nav-item-separator`}
            className={className.infoBar.NAV_ITEM_SEPARATOR}
          />
        );
    }

    return { navItemElements, dropdownElements };
  }, [
    items,
    itemSelected,
    itemIdPinned,
    itemDropdown,
    onSelectedItem,
    onClickItem,
    dataTestId
  ]);

  const bodyItemElements = useMemo(() => {
    const bodyItemElements: ReactNode[] = [];

    let isShowEmpty = true;
    items.forEach(({ id, keepComponent, component: Component }) => {
      const active = id === itemSelected;
      const isPinned = id === itemIdPinned;
      if (isPinned) isShowEmpty = false;

      const bodyElement =
        Component && (isPinned || active) && !keepComponent ? (
          <Component
            id={id}
            handlePin={() => handlePin(id)}
            handleUnpin={handleUnpin}
            handleCollapse={handleCollapse}
            handleExpand={handleExpand}
            itemIdPinned={itemIdPinned}
            itemSelected={itemSelected}
          />
        ) : null;

      if (!keepComponent) {
        bodyItemElements.push(
          <div
            key={id}
            className={classnames(
              isPinned
                ? className.infoBar.CONTENT_PINNED
                : className.infoBar.CONTENT,
              active && className.infoBar.ACTIVE
            )}
            data-testid={genAmtId(
              `${dataTestId}-${id}${active ? '-active' : ''}${
                isPinned ? '-pinned' : ''
              }`,
              'dls-info-bar-body-item',
              'InfoBar.BodyItem'
            )}
          >
            {bodyElement}
          </div>
        );
      }

      if (keepComponent) {
        const active = id === itemSelected;
        const isPinned = id === itemIdPinned;
        bodyItemElements.push(
          <div
            key={id}
            className={classnames(
              isPinned
                ? className.infoBar.CONTENT_PINNED
                : className.infoBar.CONTENT_HIDE,
              active && className.infoBar.ACTIVE
            )}
            data-testid={genAmtId(
              `${dataTestId}-${id}${active ? '-active' : ''}${
                isPinned ? '-pinned' : ''
              }`,
              'dls-info-bar-body-item',
              'InfoBar.BodyItem'
            )}
          >
            {Component && (
              <Component
                id={id}
                handlePin={() => handlePin(id)}
                handleUnpin={handleUnpin}
                handleCollapse={handleCollapse}
                handleExpand={handleExpand}
                itemIdPinned={itemIdPinned}
                itemSelected={itemSelected}
              />
            )}
          </div>
        );
      }
    });

    bodyItemElements.push(
      <div
        key={'emptyElement'}
        className={classnames(
          className.infoBar.EMPTY,
          isShowEmpty && className.infoBar.ACTIVE
        )}
        data-testid={genAmtId(
          `${dataTestId}`,
          'dls-info-bar-body-empty',
          'InfoBar.BodyEmpty'
        )}
      >
        {emptyElement}
      </div>
    );

    return bodyItemElements;
  }, [
    items,
    itemSelected,
    itemIdPinned,
    handleExpand,
    handleCollapse,
    handlePin,
    handleUnpin,
    emptyElement,
    dataTestId
  ]);

  return {
    navItems: navItemElements,
    bodyItems: bodyItemElements,
    dropdownItems: dropdownElements
  };
};

export default useInfoBarItems;
