import { createContext } from 'react';

const InfoBarContext = createContext({ isInsideInfoBar: false });

const { Provider: InfoBarProvider } = InfoBarContext;

export { InfoBarProvider };

export default InfoBarContext;
