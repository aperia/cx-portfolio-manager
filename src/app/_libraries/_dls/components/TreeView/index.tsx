import React, { useEffect, useMemo, useRef, useState } from 'react';

// components/types
import {
  TreeViewItemProps,
  TreeViewOnChangeEvent,
  TreeViewProps,
  TreeViewRef,
  TreeViewValue
} from './types';
import Item from './Item';

// utils
import isEqual from 'react-fast-compare';
import { isFunction, set, isUndefined, get, isEmpty, pick } from '../../lodash';
import { genAmtId } from '../../utils';

const TreeView: React.RefForwardingComponent<TreeViewRef, TreeViewProps> = (
  {
    defaultValue,
    value: valueProp,
    onChange,
    variant = 'text',
    highlightText,
    id: idProp,
    dataTestId
  },
  ref
) => {
  // states
  const [value, setValue] = useState(() => {
    // decide which value to use (defaultValue or valueProp);
    const nextValueProp = !isUndefined(valueProp) ? valueProp : defaultValue;
    return nextValueProp;
  });

  // refs
  const valueRef = useRef(value);
  const isSecondRenderRef = useRef(false);
  const coreRef = useRef<Record<string, any>>({});

  // valueRef always is value (support flow set props from valueProp)
  valueRef.current = value;

  // handle remove verbose properties
  const removeVerboseProperties = (item: TreeViewItemProps): TreeViewValue => {
    return pick(item, [
      'id',
      'label',
      'items',
      'path',
      'expanded',
      'checked',
      'indeterminate',
      'readOnly',
      'disabled'
    ]);
  };

  const handleToggleExpand = (item: TreeViewItemProps) => {
    const itemRemovedVerboseProperties = JSON.parse(
      JSON.stringify(removeVerboseProperties(item))
    );

    let nextValue = JSON.parse(JSON.stringify(coreRef.current.value));
    nextValue = set(nextValue, item.path, {
      ...itemRemovedVerboseProperties,
      expanded: !item.expanded
    });

    const nextEventChange: TreeViewOnChangeEvent = {
      value: nextValue,
      activeItemId: item.id,
      activeItemPath: item.path
    };
    isFunction(onChange) && onChange(nextEventChange);

    // controlled
    if (coreRef.current.valueProp) return;

    // uncontrolled
    setValue(nextValue);
  };

  const handleToggleCheck = (item: TreeViewItemProps) => {
    const itemRemovedVerboseProperties = JSON.parse(
      JSON.stringify(removeVerboseProperties(item))
    );

    // handle check state
    let nextValue = JSON.parse(JSON.stringify(coreRef.current.value));
    nextValue = set(nextValue, item.path, {
      ...itemRemovedVerboseProperties,
      indeterminate: undefined,
      checked: !item.checked
    });

    // if item has items, and item is checked => all items will be checked
    // otherwise, all items will be uncheck
    // recursing to support unlimited levels nested
    const handleRecursiveCheckChildren = (
      nextItems: TreeViewValue[],
      nextSegments: string[]
    ) => {
      if (!Array.isArray(nextItems)) return;

      for (let index = 0; index < nextItems.length; index++) {
        const currentItem = nextItems[index];
        const currentSegments = [...nextSegments, 'items', `${index}`];
        handleRecursiveCheckChildren(currentItem.items!, currentSegments);

        nextValue = set(nextValue, currentSegments, {
          ...get(nextValue, currentSegments),
          indeterminate: undefined,
          checked: !item.checked
        });
      }
    };

    handleRecursiveCheckChildren(item.items!, item.path);

    let parentPath = item.path.slice(0, item.path.length - 2);
    let parentItem = get(nextValue, parentPath) as TreeViewValue;
    while (parentItem) {
      // if all childrens checked, indeterminate will true
      // otherwise, indeterminate will false
      const itemsChecked = [];
      let hasIndeterminate = false;

      for (let index = 0; index < parentItem.items!.length; index++) {
        const childrenItem = parentItem.items![index];
        if (childrenItem.checked) {
          itemsChecked.push(childrenItem);
        }
        if (childrenItem.indeterminate) hasIndeterminate = true;
      }

      const nextIndeterminate =
        isEmpty(itemsChecked) && !hasIndeterminate
          ? undefined
          : itemsChecked.length === parentItem.items!.length &&
            !hasIndeterminate
          ? false
          : true;

      nextValue = set(nextValue, parentPath, {
        ...parentItem,
        checked: nextIndeterminate === false,
        indeterminate: nextIndeterminate
      });

      parentPath = parentPath.slice(0, parentPath.length - 2);
      parentItem = get(nextValue, parentPath);
    }

    const nextEventChange: TreeViewOnChangeEvent = {
      value: nextValue,
      activeItemId: item.id,
      activeItemPath: item.path
    };
    isFunction(onChange) && onChange(nextEventChange);

    // controlled
    if (coreRef.current.valueProp) return;

    // uncontrolled
    setValue(nextValue);
  };

  // reference, optimize performance
  if (!isFunction(coreRef.current.handleToggleExpand)) {
    coreRef.current.handleToggleExpand = handleToggleExpand;
  }
  if (!isFunction(coreRef.current.handleToggleCheck)) {
    coreRef.current.handleToggleCheck = handleToggleCheck;
  }
  // we will use coreRef.current.value inside handleToggleExpand, handleToggleCheck
  coreRef.current.value = value;
  // we will use valueProp inside handleToggleExpand, handleToggleCheck
  coreRef.current.valueProp = valueProp;

  const treeElements = useMemo(() => {
    if (!Array.isArray(value)) return;

    const enhanceItems = (item: TreeViewValue, segments: string[]): any => {
      let nextItemElements;
      if (Array.isArray(item.items)) {
        nextItemElements = item.items.map((child, childIndex) => {
          return enhanceItems(child, [...segments, 'items', `${childIndex}`]);
        });
      }

      const nextProps: TreeViewItemProps = {
        ...item,
        indeterminate: Boolean(item.indeterminate),
        expanded: Boolean(item.expanded),
        checked: Boolean(item.checked),
        onToggleExpand: coreRef.current.handleToggleExpand,
        onToggleCheck: coreRef.current.handleToggleCheck,
        path: segments,
        variant,
        highlightText,
        children: nextItemElements,
        idProp,
        dataTestId
      };

      return <Item key={item.id} {...nextProps} />;
    };

    const result = value.map((item, itemIndex) => {
      return enhanceItems(item, [`${itemIndex}`]);
    });

    return result;
  }, [value, variant, highlightText, idProp, dataTestId]);

  // set props
  useEffect(() => {
    if (!isSecondRenderRef.current) {
      isSecondRenderRef.current = true;
      return;
    }

    if (isEqual(valueRef.current, valueProp)) return;

    setValue(valueProp);
  }, [valueProp]);

  return (
    <div
      className="dls-treeview"
      id={idProp}
      data-testid={genAmtId(dataTestId, 'dls-treeview', 'TreeView')}
    >
      {treeElements}
    </div>
  );
};

const TreeViewExtraStaticProp = React.forwardRef<TreeViewRef, TreeViewProps>(
  TreeView
) as React.ForwardRefExoticComponent<
  TreeViewProps & React.RefAttributes<TreeViewProps>
> & {
  Item: typeof Item;
};

export * from './types';
export default TreeViewExtraStaticProp;
