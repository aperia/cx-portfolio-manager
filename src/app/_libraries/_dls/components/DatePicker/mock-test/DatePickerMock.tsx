import React, { useCallback, useState } from 'react';

import DatePicker from '..';
import { DatePickerChangeEvent, DatePickerProps } from '../types';

const DatePickerMock = ({
  value: valueProp,
  onFocus,
  ...props
}: DatePickerProps) => {
  const [value, setValue] = useState(valueProp);

  const handleChange = ({ value }: DatePickerChangeEvent) => {
    setValue(value);
  };

  const handleFocus = useCallback(
    (e: React.FocusEvent<Element>) => {
      onFocus && onFocus(e);
    },
    [onFocus]
  );

  return (
    <div>
      <DatePicker
        ref={{ current: null }}
        onFocus={handleFocus}
        onChange={handleChange}
        value={value}
        {...props}
      />
    </div>
  );
};

export default DatePickerMock;
