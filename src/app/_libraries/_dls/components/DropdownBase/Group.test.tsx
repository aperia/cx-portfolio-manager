import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';

// components
import Group from './Group';

describe('Render', () => {
  it('should render', () => {
    const label = 'label';
    const text = 'text';

    render(
      <Group label={label}>
        <p>{text}</p>
      </Group>
    );

    expect(screen.getByText(label)).toBeInTheDocument();
    expect(screen.getByText(text)).toBeInTheDocument();
  });
});
