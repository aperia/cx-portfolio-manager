import React from 'react';

// testing library
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';

// hooks
import useInfinityScroll from './useInfinityScroll';

const entries1 = [{ isIntersecting: false }];
const entries2 = [{ isIntersecting: true }];
class IntersectionObserver {
  constructor(callback: Function) {
    this.callback = callback;
  }

  observe = () => {
    this.callback(entries1);
    this.callback(entries2);
  };
  unobserve = () => undefined;
  disconnect = () => undefined;
}

const Wrapper: React.FC<HTMLElement> = props => {
  useInfinityScroll(
    {},
    {
      current: {
        closest: () => ({
          style: {}
        }),
        ...props
      }
    }
  );

  return <div>Wrapper</div>;
};

describe('useInfinityScroll', () => {
  beforeAll(() => {
    window.IntersectionObserver = IntersectionObserver;
  });

  it('should cover 100%', () => {
    const { rerender, queryByText } = render(
      <Wrapper scrollHeight={1} clientHeight={2} />
    );
    expect(queryByText('Wrapper')).toBeInTheDocument();

    // lastElement undefined
    rerender(<Wrapper scrollHeight={2} clientHeight={1} />);

    rerender(<Wrapper scrollHeight={2} clientHeight={1} lastChild={{}} />);
  });
});
