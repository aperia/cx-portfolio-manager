import '@testing-library/jest-dom';
import { Matcher, render, screen } from '@testing-library/react';
import React, { MutableRefObject } from 'react';
// utils
import { queryByClass } from '../../test-utils/queryHelpers';
// components
import Item from './Item';
// types
import {
  DropdownBaseCore,
  DropdownBaseItemDetailProps,
  DropdownBaseValueMultiple
} from './types';

let baseElement: HTMLElement;

jest.mock('../../utils', () => ({
  ...jest.requireActual('../../utils'),
  truncateTooltip: () => undefined
}));

const renderComponent = (props: DropdownBaseItemDetailProps) => {
  const wrapper = render(<Item ref={{ current: null }} {...props} />);
  baseElement = wrapper.baseElement as HTMLElement;

  return wrapper;
};

const expectByClass = (className: Matcher) => {
  expect(queryByClass(baseElement, className)).toBeInTheDocument();
};

describe('Render', () => {
  it('should render disabled Item', () => {
    renderComponent({ variant: 'badge', disabled: true });

    expectByClass(/dls-disabled/);
  });

  it('should render Badge', () => {
    renderComponent({ variant: 'badge' });

    expectByClass(/badge badge-/);
  });

  it('should render text Badge', () => {
    renderComponent({ variant: 'text-badge' });

    expectByClass(/item-text-badge/);
  });

  it('should render Badge only', () => {
    renderComponent({ variant: 'badge' });

    expectByClass(/item-badge/);
  });

  it('should render Bubble', () => {
    renderComponent({ variant: 'bubble' });

    expectByClass(/item-bubble/);
  });

  it('should render text', () => {
    renderComponent({ variant: 'text' });

    expectByClass(/item-text/);
  });

  it('should render Checkbox', () => {
    renderComponent({ variant: 'checkbox' });

    const checkbox = screen.getByRole('checkbox');

    expect(checkbox).toBeInTheDocument();
  });

  it('should render custom element', () => {
    renderComponent({
      variant: 'custom',
      truncateTooltip: true,
      children: <div>children</div>
    });

    expect(screen.getByText('children')).toBeInTheDocument();
  });

  it('should render default element', () => {
    const label = 'label';
    renderComponent({ label, highlightTextItem: true, truncateTooltip: true });

    expect(screen.getByText('label')).toBeInTheDocument();
  });

  it('should render tooltip', () => {
    renderComponent({
      variant: 'custom',
      truncateTooltip: true,
      children: <div>children</div>,
      tooltipProps: { containerClassName: 'test-tooltip-props' }
    });

    expectByClass(/dls-popper-trigger/);
  });
});

describe('Actions', () => {
  describe('handleOnClickItem', () => {
    it('when disabled', () => {
      const handleOnChangeModeSingle = jest.fn();

      renderComponent({
        variant: 'badge',
        disabled: true,
        coreRef: {
          current: {
            handleOnChangeModeSingle,
            handleOnChangeModeMultiple: () => undefined,
            valueSingle: '',
            valueMultiple: [],
            valueType: 'single',
            items: [],
            setMaybeNextValue: () => undefined
          }
        } as unknown as MutableRefObject<DropdownBaseCore>
      });

      queryByClass(baseElement, /item-badge/)!.click();
      expect(handleOnChangeModeSingle).not.toBeCalled();
    });

    it('when single mode', () => {
      const value = 'value';
      const handleOnChangeModeSingle = jest.fn();

      renderComponent({
        variant: 'checkbox',
        value,
        coreRef: {
          current: {
            handleOnChangeModeSingle,
            handleOnChangeModeMultiple: () => undefined,
            valueSingle: '',
            valueMultiple: [],
            valueType: 'single',
            items: [],
            setMaybeNextValue: () => undefined
          }
        } as unknown as MutableRefObject<DropdownBaseCore>
      });

      screen.getByRole('checkbox').click();
      expect(handleOnChangeModeSingle).toBeCalledWith(value, 'click', {
        badgeProps: undefined,
        bubbleProps: undefined,
        textBadgeProps: undefined,
        textProps: undefined,
        variant: 'checkbox'
      });
    });

    it('when multiple mode and item was selected', () => {
      const value = 'value_0';
      const handleOnChangeModeMultiple = jest.fn();

      renderComponent({
        variant: 'checkbox',
        value,
        coreRef: {
          current: {
            handleOnChangeModeSingle: () => undefined,
            handleOnChangeModeMultiple,
            valueSingle: '',
            valueMultiple: ['value_0'],
            valueType: 'multiple',
            items: [],
            setHoverValue: () => undefined
          }
        } as unknown as MutableRefObject<DropdownBaseCore>
      });

      screen.getByRole('checkbox').click();
      expect(handleOnChangeModeMultiple).toBeCalledWith([]);
    });

    it('when multiple mode and item was not selected', () => {
      const value = 'value_1';
      const handleOnChangeModeMultiple = jest.fn();

      renderComponent({
        variant: 'checkbox',
        value,
        coreRef: {
          current: {
            handleOnChangeModeSingle: () => undefined,
            handleOnChangeModeMultiple,
            valueSingle: '',
            valueMultiple: undefined as unknown as DropdownBaseValueMultiple,
            valueType: 'multiple',
            items: [],
            baseChildren: [],
            setHoverValue: () => undefined
          }
        } as unknown as MutableRefObject<DropdownBaseCore>
      });

      screen.getByRole('checkbox').click();
      expect(handleOnChangeModeMultiple).toBeCalledWith([value]);
    });

    describe('when new-behavior mode', () => {
      it('should call handleOnChangeModeSingle and setHoverValue', () => {
        const handleOnChangeModeSingle = jest.fn();

        renderComponent({
          label: 'AAA',
          coreRef: {
            current: {
              handleOnChangeModeSingle,
              valueSingle: '',
              valueMultiple: undefined as unknown as DropdownBaseValueMultiple,
              valueType: 'new-behavior',
              items: [],
              setHoverValue: () => undefined
            }
          } as unknown as MutableRefObject<DropdownBaseCore>
        });

        screen.queryByText('AAA')!.click();
        expect(handleOnChangeModeSingle).toBeCalled();
      });
    });

    describe('when multiple mode and check all with value is PP-M-CheckAll', () => {
      it('without valueMultiple and baseChildren', () => {
        const value = 'PP-M-CheckAll';
        const handleOnChangeModeMultiple = jest.fn();

        renderComponent({
          variant: 'checkbox',
          value,
          coreRef: {
            current: {
              handleOnChangeModeSingle: () => undefined,
              handleOnChangeModeMultiple,
              valueSingle: '',
              valueMultiple: undefined as unknown as DropdownBaseValueMultiple,
              valueType: 'multiple',
              items: [],
              checkAll: true,
              setHoverValue: () => undefined
            }
          } as unknown as MutableRefObject<DropdownBaseCore>
        });

        screen.getByRole('checkbox').click();
        expect(handleOnChangeModeMultiple).toBeCalledWith(undefined);
      });

      it('with valueMultiple and baseChildren', () => {
        const value = 'PP-M-CheckAll';
        const valueMultiple = ['value_1', value];
        const handleOnChangeModeMultiple = jest.fn();

        renderComponent({
          variant: 'checkbox',
          value,
          coreRef: {
            current: {
              handleOnChangeModeSingle: () => undefined,
              handleOnChangeModeMultiple,
              valueSingle: '',
              valueMultiple,
              valueType: 'multiple',
              items: [],
              checkAll: true,
              baseChildren: valueMultiple.map((val, index) => (
                <div key={index}>{val}</div>
              )),
              setHoverValue: () => undefined
            }
          } as unknown as MutableRefObject<DropdownBaseCore>
        });

        screen.getByRole('checkbox').click();
        expect(handleOnChangeModeMultiple).toBeCalledWith([]);
      });
    });
  });
});
