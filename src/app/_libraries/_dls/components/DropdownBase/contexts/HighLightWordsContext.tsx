import React, { useContext } from 'react';

export interface HighLightWordsContextType {
  highlightTextFilter?: any;
  setHighlightTextFilter?: (nextHighLightTextFilter?: any) => void;
}

const HighLightWordsContext = React.createContext<HighLightWordsContextType>(
  {}
);
const useHighLightWordsContext = () => useContext(HighLightWordsContext);

export { useHighLightWordsContext, HighLightWordsContext };
export default HighLightWordsContext;
