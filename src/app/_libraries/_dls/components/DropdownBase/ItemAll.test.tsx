import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';

import ItemAll from './ItemAll';
import { GroupProps } from '../MultiSelect/Group';

jest.mock('./Item', () => () => <div data-testid="DropdownBase_Item"></div>);
jest.mock('./Group', () => ({ children }: GroupProps) => (
  <div data-testid="DropdownBase_Group">{children}</div>
));

describe('render', () => {
  it('should render', () => {
    render(<ItemAll />);

    expect(screen.getByTestId('DropdownBase_Item')).toBeInTheDocument();
    expect(screen.getByTestId('DropdownBase_Group')).toBeInTheDocument();
  });
});
