import React from 'react';

// components
import ReactHighLightWords, { HighlighterProps } from 'react-highlight-words';
import { get } from '../../lodash';

// utils
import { genAmtId, replaceSpecialCharacter } from '../../utils';

// hooks
import { useHighLightWordsContext } from '../DropdownBase/contexts/HighLightWordsContext';

export interface HighlightWordsProps
  extends Pick<HighlighterProps, 'textToHighlight' | 'className'> {
  dataTestId?: string;
}

const HighLightWords: React.FC<HighlightWordsProps> = ({
  textToHighlight,
  className,
  dataTestId
}) => {
  const highLightWordsContextValue = useHighLightWordsContext();
  const highlightTextFilter = get(
    highLightWordsContextValue,
    'highlightTextFilter'
  );

  return (
    <ReactHighLightWords
      searchWords={replaceSpecialCharacter(highlightTextFilter).split(' ')}
      textToHighlight={textToHighlight}
      highlightClassName="dls-mark"
      className={className}
      data-testid={genAmtId(
        dataTestId,
        'dls-high-light-words',
        'HighLightWords'
      )}
    />
  );
};

export default HighLightWords;
