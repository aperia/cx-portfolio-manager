import { render, screen } from '@testing-library/react';
import React from 'react';
import { HorizontalTabs } from '..';
import '@testing-library/jest-dom';

import * as react from 'react';
import { queryByClass } from '../../test-utils';

let spy: jest.SpyInstance;

const mockSetActiveKey = jest.fn();

const mockScroll = jest.fn();
window.HTMLElement.prototype.scroll = mockScroll;

const showLeftArrowButton = () => {
  const mockTabContainerRect = {
    left: 0,
    width: 800
  };
  const mockActiveElementRect = {
    left: 1000,
    width: 100
  };
  window.HTMLElement.prototype.getBoundingClientRect = jest
    .fn()
    .mockReturnValueOnce(mockTabContainerRect)
    .mockReturnValue(mockActiveElementRect);

  const tabElement = screen.queryAllByRole('tab')[1];

  tabElement?.click();
};

const showRightArrowButton = () => {
  const mockTabContainerRect = {
    left: 1000,
    width: 800
  };
  const mockActiveElementRect = {
    left: 800,
    width: 300
  };
  window.HTMLElement.prototype.getBoundingClientRect = jest
    .fn()
    .mockReturnValueOnce(mockTabContainerRect)
    .mockReturnValueOnce(mockActiveElementRect)
    .mockReturnValue({ left: 400, width: 100 });

  const tabElement = screen.queryAllByRole('tab')[1];

  tabElement?.click();
};

describe('Horizontal tabs', () => {
  beforeEach(() => {
    jest.spyOn(window, 'getComputedStyle').mockImplementation(
      () =>
        ({
          width: '100px'
        } as CSSStyleDeclaration)
    );
  });

  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });

  describe('render UI', () => {
    it('should render success uncontrolled onSelect', () => {
      render(
        <HorizontalTabs
          mountOnEnter
          unmountOnExit
          className="bg-light-l20"
          dataTestId="horizontal-tabs-test-id"
          ref={{ current: null }}
        >
          <HorizontalTabs.Tab key={`1`} eventKey={`1`} title="Overview">
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
          <HorizontalTabs.Tab key={`2`} eventKey={`2`} title="Statement">
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
          <HorizontalTabs.Tab
            key={`3`}
            eventKey={`3`}
            title="Pending Authorizations"
          >
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
        </HorizontalTabs>
      );

      expect(screen.getByText('Test content')).toBeInTheDocument();
    });
    it('should render success controlled onSelect', () => {
      const mockOnSelect = jest.fn();

      render(
        <HorizontalTabs
          mountOnEnter
          unmountOnExit
          className="bg-light-l20"
          activeKey="1"
          onSelect={mockOnSelect}
        >
          <HorizontalTabs.Tab key={`1`} eventKey={`1`} title="Overview">
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
          <HorizontalTabs.Tab key={`2`} eventKey={`2`} title="Statement">
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
          <HorizontalTabs.Tab
            key={`3`}
            eventKey={`3`}
            title="Pending Authorizations"
          >
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
        </HorizontalTabs>
      );

      expect(screen.getByText('Test content')).toBeInTheDocument();
    });
    it('should render scroll left scroll right success', () => {
      render(
        <HorizontalTabs
          mountOnEnter
          unmountOnExit
          className="bg-light-l20"
          ref={{ current: null }}
          activeKey="1"
        >
          <HorizontalTabs.Tab key={`1`} eventKey={`1`} title="Overview">
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
          <HorizontalTabs.Tab key={`2`} eventKey={`2`} title="Statement">
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
          <HorizontalTabs.Tab
            key={`3`}
            eventKey={`3`}
            title="Pending Authorizations"
          >
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
        </HorizontalTabs>
      );

      expect(screen.getByText('Test content')).toBeInTheDocument();
    });
  });

  describe('actions', () => {
    it('should passed handle scroll right ', () => {
      const mockOnSelect = jest.fn();
      const cpt = render(
        <HorizontalTabs
          mountOnEnter
          unmountOnExit
          className="bg-light-l20"
          activeKey="1"
          onSelect={mockOnSelect}
        >
          <HorizontalTabs.Tab key={`1`} eventKey={`1`} title="Overview">
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
          <HorizontalTabs.Tab key={`2`} eventKey={`2`} title="Statement">
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
          <HorizontalTabs.Tab
            key={`3`}
            eventKey={`3`}
            title="Pending Authorizations"
          >
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
        </HorizontalTabs>
      );

      showRightArrowButton();

      const scrollRightBtn = queryByClass(
        cpt.baseElement as HTMLElement,
        /icon-chevron-right/
      );

      scrollRightBtn?.click();

      expect(screen.getByText('Test content')).toBeInTheDocument();
      expect(mockScroll).toBeCalledTimes(2);
    });
    it('should passed handle scroll right, and run throw case undefined width', () => {
      const mockOnSelect = jest.fn();
      const cpt = render(
        <HorizontalTabs
          mountOnEnter
          unmountOnExit
          className="bg-light-l20"
          activeKey="1"
          onSelect={mockOnSelect}
        >
          <HorizontalTabs.Tab key={`1`} eventKey={`1`} title="Overview">
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
          <HorizontalTabs.Tab key={`2`} eventKey={`2`} title="Statement">
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
          <HorizontalTabs.Tab
            key={`3`}
            eventKey={`3`}
            title="Pending Authorizations"
          >
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
        </HorizontalTabs>
      );

      showRightArrowButton();

      window.HTMLElement.prototype.getBoundingClientRect = jest
        .fn()
        .mockReturnValueOnce({ left: 400, width: 100 })
        .mockReturnValueOnce({ left: 400, width: 100 })
        .mockReturnValue({ left: 400, width: 0 });

      const scrollRightBtn = queryByClass(
        cpt.baseElement as HTMLElement,
        /icon-chevron-right/
      );

      scrollRightBtn?.click();

      expect(screen.getByText('Test content')).toBeInTheDocument();
      expect(mockScroll).toBeCalledTimes(2);
    });
    it('should passed handle scroll right, and run throw case cutoff width < 0 (tab is not hidden)', () => {
      const mockOnSelect = jest.fn();
      const cpt = render(
        <HorizontalTabs
          mountOnEnter
          unmountOnExit
          className="bg-light-l20"
          activeKey="1"
          onSelect={mockOnSelect}
        >
          <HorizontalTabs.Tab key={`1`} eventKey={`1`} title="Overview">
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
          <HorizontalTabs.Tab key={`2`} eventKey={`2`} title="Statement">
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
          <HorizontalTabs.Tab
            key={`3`}
            eventKey={`3`}
            title="Pending Authorizations"
          >
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
        </HorizontalTabs>
      );

      showRightArrowButton();

      window.HTMLElement.prototype.getBoundingClientRect = jest
        .fn()
        .mockReturnValueOnce({ left: 400, width: 1001 })
        .mockReturnValueOnce({ left: 400, width: 100 })
        .mockReturnValue({ left: 400, width: 0 });

      const scrollRightBtn = queryByClass(
        cpt.baseElement as HTMLElement,
        /icon-chevron-right/
      );

      scrollRightBtn?.click();

      expect(screen.getByText('Test content')).toBeInTheDocument();
      expect(mockScroll).toBeCalledTimes(2);
    });
    it('should passed handle scroll left ', () => {
      const mockOnSelect = jest.fn();
      const cpt = render(
        <HorizontalTabs
          mountOnEnter
          unmountOnExit
          className="bg-light-l20"
          activeKey="1"
          onSelect={mockOnSelect}
        >
          <HorizontalTabs.Tab key={`1`} eventKey={`1`} title="Overview">
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
          <HorizontalTabs.Tab key={`2`} eventKey={`2`} title="Statement">
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
          <HorizontalTabs.Tab
            key={`3`}
            eventKey={`3`}
            title="Pending Authorizations"
          >
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
        </HorizontalTabs>
      );

      showLeftArrowButton();

      const scrollLeftBtn = queryByClass(
        cpt.baseElement as HTMLElement,
        /icon-chevron-left/
      );

      scrollLeftBtn?.click();

      expect(mockScroll).toBeCalledTimes(2);
    });
    it('should passed handle scroll left, when scrolled to the head', () => {
      const mockOnSelect = jest.fn();
      const cpt = render(
        <HorizontalTabs
          mountOnEnter
          unmountOnExit
          className="bg-light-l20"
          activeKey="1"
          onSelect={mockOnSelect}
        >
          <HorizontalTabs.Tab key={`1`} eventKey={`1`} title="Overview">
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
          <HorizontalTabs.Tab key={`2`} eventKey={`2`} title="Statement">
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
          <HorizontalTabs.Tab
            key={`3`}
            eventKey={`3`}
            title="Pending Authorizations"
          >
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
        </HorizontalTabs>
      );

      const mockTabContainerRect = {
        left: 0,
        width: 800
      };
      const mockActiveElementRect = {
        left: 1000,
        width: 100
      };
      window.HTMLElement.prototype.getBoundingClientRect = jest
        .fn()
        .mockReturnValueOnce(mockTabContainerRect)
        .mockReturnValueOnce(mockActiveElementRect);

      const tabElement = screen.queryAllByRole('tab')[1];

      tabElement?.click();

      const scrollLeftBtn = queryByClass(
        cpt.baseElement as HTMLElement,
        /icon-chevron-left/
      );

      window.HTMLElement.prototype.getBoundingClientRect = jest
        .fn()
        .mockReturnValueOnce({
          left: 400,
          width: 800
        })
        .mockReturnValueOnce({ left: 500, width: 800 })
        .mockReturnValue({ left: 400, width: 0 });

      scrollLeftBtn?.click();

      expect(mockScroll).toBeCalledTimes(2);
    });
    it('should passed handle onSelect in controlled mode', () => {
      const mockOnSelect = jest.fn();

      render(
        <HorizontalTabs
          mountOnEnter
          unmountOnExit
          className="bg-light-l20"
          activeKey="1"
          onSelect={mockOnSelect}
        >
          <HorizontalTabs.Tab key={`1`} eventKey={`1`} title="Overview">
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
          <HorizontalTabs.Tab key={`2`} eventKey={`2`} title="Statement">
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
          <HorizontalTabs.Tab
            key={`3`}
            eventKey={`3`}
            title="Pending Authorizations"
          >
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
        </HorizontalTabs>
      );

      window.HTMLElement.prototype.getBoundingClientRect = jest
        .fn()
        .mockReturnValueOnce({
          left: 400,
          width: 800
        })
        .mockReturnValueOnce({ left: 500, width: 800 })
        .mockReturnValue({ left: 400, width: 0 });

      const tabElement = screen.queryAllByRole('tab')[0];

      tabElement?.click();

      expect(mockOnSelect).toBeCalledTimes(1);
    });
    it('should passed handle onSelect in uncontrolled mode', () => {
      render(
        <HorizontalTabs mountOnEnter unmountOnExit className="bg-light-l20">
          <HorizontalTabs.Tab key={`1`} eventKey={`1`} title="Overview">
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
          <HorizontalTabs.Tab key={`2`} eventKey={`2`} title="Statement">
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
          <HorizontalTabs.Tab
            key={`3`}
            eventKey={`3`}
            title="Pending Authorizations"
          >
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
        </HorizontalTabs>
      );

      window.HTMLElement.prototype.getBoundingClientRect = jest
        .fn()
        .mockReturnValue({ left: 0, width: 100 });

      const tabElement = screen.queryAllByRole('tab')[0];

      tabElement?.click();
    });
    it('should passed handle onSelect in controlled mode, active tab is hidden on the right', () => {
      const mockOnSelect = jest.fn();

      render(
        <HorizontalTabs
          mountOnEnter
          unmountOnExit
          className="bg-light-l20"
          activeKey="1"
          onSelect={mockOnSelect}
        >
          <HorizontalTabs.Tab key={`1`} eventKey={`1`} title="Overview">
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
          <HorizontalTabs.Tab key={`2`} eventKey={`2`} title="Statement">
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
          <HorizontalTabs.Tab
            key={`3`}
            eventKey={`3`}
            title="Pending Authorizations"
          >
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
        </HorizontalTabs>
      );

      const mockTabContainerRect = {
        left: 0,
        width: 800
      };
      const mockActiveElementRect = {
        left: 1000,
        width: 100
      };
      window.HTMLElement.prototype.getBoundingClientRect = jest
        .fn()
        .mockReturnValueOnce(mockTabContainerRect)
        .mockReturnValueOnce(mockActiveElementRect)
        .mockReturnValueOnce(mockTabContainerRect)
        .mockReturnValue(mockActiveElementRect);

      const tabElement = screen.queryAllByRole('tab')[1];

      tabElement?.click();
      const tabElement2 = screen.queryAllByRole('tab')[0];
      tabElement2?.click();

      expect(mockOnSelect).toBeCalledTimes(2);
    });
    it('should passed handle onSelect in controlled mode, active tab is hidden on the right, scroll left arrow is not present', () => {
      const mockState: Record<string, unknown> = {
        arrowWidth: 60,
        paddingSize: 24,
        showScrollRight: true,
        showScrollLeft: false,
        activeKey: '1',
        isHavingIcon: true
      };

      spy = jest
        .spyOn(react, 'useState')
        .mockImplementationOnce(() => [mockState.arrowWidth, jest.fn()])
        .mockImplementationOnce(() => [mockState.paddingSize, jest.fn()])
        .mockImplementationOnce(() => [mockState.showScrollRight, jest.fn()])
        .mockImplementationOnce(() => [mockState.showScrollLeft, jest.fn()])
        .mockImplementationOnce(() => [mockState.activeKey, mockSetActiveKey])
        .mockImplementationOnce(() => [mockState.isHavingIcon, jest.fn()]);
      const mockOnSelect = jest.fn();

      render(
        <HorizontalTabs
          mountOnEnter
          unmountOnExit
          className="bg-light-l20"
          activeKey="1"
          onSelect={mockOnSelect}
        >
          <HorizontalTabs.Tab key={`1`} eventKey={`1`} title="Overview">
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
          <HorizontalTabs.Tab key={`2`} eventKey={`2`} title="Statement">
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
          <HorizontalTabs.Tab
            key={`3`}
            eventKey={`3`}
            title="Pending Authorizations"
          >
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
        </HorizontalTabs>
      );

      const mockTabContainerRect = {
        left: 0,
        width: 800
      };
      const mockActiveElementRect = {
        left: 1000,
        width: 100
      };
      window.HTMLElement.prototype.getBoundingClientRect = jest
        .fn()
        .mockReturnValueOnce(mockTabContainerRect)
        .mockReturnValueOnce(mockActiveElementRect);

      const tabElement = screen.queryAllByRole('tab')[1];

      tabElement?.click();

      expect(mockOnSelect).toBeCalledTimes(1);
    });
    it('should passed handle onSelect in controlled mode, active tab is hidden on the left', () => {
      const mockOnSelect = jest.fn();

      render(
        <HorizontalTabs
          mountOnEnter
          unmountOnExit
          className="bg-light-l20"
          activeKey="1"
          onSelect={mockOnSelect}
        >
          <HorizontalTabs.Tab key={`1`} eventKey={`1`} title="Overview">
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
          <HorizontalTabs.Tab key={`2`} eventKey={`2`} title="Statement">
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
          <HorizontalTabs.Tab
            key={`3`}
            eventKey={`3`}
            title="Pending Authorizations"
          >
            <h1>Test content</h1>
          </HorizontalTabs.Tab>
        </HorizontalTabs>
      );

      const mockTabContainerRect = {
        left: 1000,
        width: 800
      };
      const mockActiveElementRect = {
        left: 800,
        width: 300
      };
      window.HTMLElement.prototype.getBoundingClientRect = jest
        .fn()
        .mockReturnValueOnce(mockTabContainerRect)
        .mockReturnValueOnce(mockActiveElementRect);

      const tabElement = screen.queryAllByRole('tab')[1];

      tabElement?.click();

      expect(mockOnSelect).toBeCalledTimes(1);
    });
  });
});
