import React, { useMemo } from 'react';

// mock libs
import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '../../test-utils/mocks/mockCanvas';
import '@testing-library/jest-dom';
import FloatingDropdownButton from '.';

const data = [
  {
    FieldMetadataID: 5,
    FieldID: 'NEXT',
    FieldValue: 'Next Account',
    SequenceNumber: 0,
    RoleID: 0
  },
  {
    FieldMetadataID: 5,
    FieldID: 'QUEUE',
    FieldValue: 'Queue List',
    SequenceNumber: 0,
    RoleID: 0
  },
  {
    FieldMetadataID: 5,
    FieldID: 'SEARCH',
    FieldValue: 'Search Page',
    SequenceNumber: 0,
    RoleID: 0
  }
];

const MockFloatingDropdownButton: React.FC<any> = (props) => {
  const floatingDropdownButtonItem = useMemo(() => {
    return data.map((item, index) => (
      <FloatingDropdownButton.Item
        key={item.FieldID + index}
        label={item.FieldValue}
        value={item}
      />
    ));
  }, []);

  return (
    <FloatingDropdownButton textField="FieldValue">
      {floatingDropdownButtonItem}
    </FloatingDropdownButton>
  );
};

describe('FloatingDropdownButton', () => {
  it('should render', () => {
    const { container } = render(<MockFloatingDropdownButton />);

    expect(
      container.querySelector('.dls-floating-dropdown-button')
    ).toBeInTheDocument();
  });

  it('should opened', () => {
    const { baseElement, container } = render(
      <MockFloatingDropdownButton opened />
    );

    const containerElement = container.querySelector('.text-field-container')!;
    userEvent.click(containerElement);

    expect(baseElement.querySelector('.dls-popup')).toBeInTheDocument();
  });
});
