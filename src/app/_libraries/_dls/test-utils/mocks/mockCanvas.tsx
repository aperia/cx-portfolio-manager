import 'react';

HTMLCanvasElement.prototype.getContext = (() => ({
  measureText: (text: string) => ({ width: text.length * 50 })
})) as any;
