jest.mock('chart.js', () => {
  class Chart {
    constructor(ctx: any, options: any) {}

    public static register = (list: any) => undefined;

    update = jest.fn();

    destroy = () => undefined;
  }

  return {
    Chart,
    registerables: []
  };
});

export default {};
