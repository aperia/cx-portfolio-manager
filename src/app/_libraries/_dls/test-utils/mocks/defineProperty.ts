export const mockComputedPadding = (
  paddingTop = 0,
  paddingRight = 0,
  paddingBottom = 0,
  paddingLeft = 0
) =>
  Object.defineProperty(window, 'getComputedStyle', {
    value: () => ({
      paddingTop,
      paddingRight,
      paddingBottom,
      paddingLeft
    })
  });

export const mockPropertyValue = (value = 0) =>
  Object.defineProperty(window, 'getComputedStyle', {
    value: () => ({
      getPropertyValue: () => {
        return {
          value,
          replace: () => {
            return '';
          }
        };
      }
    })
  });

export const mockClientWidth = (clientWidth: number) => {
  return Object.defineProperty(HTMLElement.prototype, 'clientWidth', {
    writable: true,
    value: clientWidth
  });
};
