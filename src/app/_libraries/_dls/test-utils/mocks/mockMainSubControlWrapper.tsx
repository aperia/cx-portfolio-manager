
import React, { useState } from "react";
import { MainSubConditionProps, MainSubConditionValue, MainSubConditionControl } from "../../components/AttributeSearch/controls";

const MainSubConditionControlWraper: React.FC<MainSubConditionProps> = (props) => {
  const [value, setValue] = useState<MainSubConditionValue>(props.value);
  
  const handleChange = (event: { value?: MainSubConditionValue }) => {
    setValue(event.value!)
  }

  return <MainSubConditionControl {...props} value={value} onChange={handleChange} />
}

export default MainSubConditionControlWraper;