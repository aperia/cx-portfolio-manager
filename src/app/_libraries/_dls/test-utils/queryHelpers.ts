import { Matcher, queryHelpers } from '@testing-library/react';

export const queryByClass = (container: HTMLElement, id: Matcher) => {
  return queryHelpers.queryByAttribute('class', container, id);
};

export const queryAllByClass = (container: HTMLElement, id: Matcher) => {
  return queryHelpers.queryAllByAttribute('class', container, id);
};

export const queryByName = (container: HTMLElement, id: Matcher) => {
  return queryHelpers.queryByAttribute('name', container, id);
};

export const queryAllByName = (container: HTMLElement, id: Matcher) => {
  return queryHelpers.queryAllByAttribute('name', container, id);
};

export const queryById = (container: HTMLElement, id: Matcher) => {
  return queryHelpers.queryByAttribute('id', container, id);
};

export const queryAllById = (container: HTMLElement, id: Matcher) => {
  return queryHelpers.queryAllByAttribute('id', container, id);
};

export const queryByReadOnly = (container: HTMLElement, id: Matcher) => {
  return queryHelpers.queryByAttribute('readonly', container, id);
};

export const queryByFor = (container: HTMLElement, id: Matcher) => {
  return queryHelpers.queryByAttribute('for', container, id);
};

export const queryByStyle = (container: HTMLElement, id: Matcher) => {
  return queryHelpers.queryByAttribute('style', container, id);
};

export const queryBy = (
  container: HTMLElement,
  // queryBy -> queryByStr (sonar fix)
  queryByStr: string,
  id: Matcher
) => {
  return queryHelpers.queryByAttribute(queryByStr, container, id);
};
