import React from 'react';

export const mockUseRef = <T extends unknown>() => {
  return jest.spyOn(React, 'useRef').mockReturnValueOnce({ current: {} }) as T;
};
