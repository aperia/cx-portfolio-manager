import getIndexDiff from './getIndexDiff';

describe('test getIndexDiff', () => {
  it('should return -1 if previous is empty', () => {
    const result = getIndexDiff('', '153');
    expect(result).toEqual(-1);
  });

  it('should return index diff', () => {
    const result = getIndexDiff('123', '153');
    expect(result).toEqual(1);
  });
});
