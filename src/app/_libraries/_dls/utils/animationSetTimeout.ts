declare type CallbackType = (ratio: number) => any;

const animationSetTimeout = (
  fn: CallbackType,
  duration: number,
  frame = 60
) => {
  for (let index = 1; index < frame; index++) {
    setTimeout(() => fn(index / frame), (index / 60) * duration);
  }

  setTimeout(() => fn(1), duration);
};

export default animationSetTimeout;
