import genId from './genId';

describe('test genId', () => {
  it('should return random id', () => {
    const result = genId();
    expect(result).not.toBeUndefined();
  });
});
