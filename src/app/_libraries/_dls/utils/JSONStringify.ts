/**
 * - JSON.stringify try/catch version
 * - @param secure if true, this function will return empty string on production (default: true)
 * - @returns: JSON obj stringified
 */
const JSONStringify = (obj: any, secure = true) => {
  let result = '';
  
  if (process.env.NODE_ENV === 'production' && secure) return '';

  try {
    result = JSON.stringify(obj);
  } catch (error) {
    result = 'JSON.stringify has error';
  }

  return result;
}

export default JSONStringify;