import JSONStringify from './JSONStringify';

describe('test JSONStringify', () => {
  it('should run with secure is undefined', () => {
    const result = JSONStringify({});
    expect(result).toEqual('{}');
  });

  it('should run with secure is false', () => {
    const result = JSONStringify({}, false);
    expect(result).toEqual('{}');
  });

  it('should run with secure is true', () => {
    const temp = process.env.NODE_ENV;
    process.env.NODE_ENV = 'production';
    const result = JSONStringify({}, true);

    expect(result).toEqual('');
    process.env.NODE_ENV = temp;
  });

  it('should throw error with exception case', () => {
    const result = JSONStringify(
      {
        a: BigInt(99999999999999999999999999999999999999999999999999999999999)
      },
      true
    );
    expect(result).toEqual('JSON.stringify has error');
  });
});
