const focusableQuery =
  'a[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled],[tabindex="-1"]), iframe, object, embed, [tabindex="0"]';

const keepFocusInside = (event: KeyboardEvent, element: HTMLElement) => {
  if (event.keyCode !== 9) return;

  //List of html elements which can be focused by tabbing.
  const focusableElements = element.querySelectorAll(focusableQuery);

  if (!focusableElements.length) return;

  const firstElement = focusableElements[0] as HTMLElement;
  const lastElement = focusableElements[
    focusableElements.length - 1
  ] as HTMLElement;

  if (event.shiftKey) {
    // Check for Shift + Tab
    if (document.activeElement === firstElement) {
      event.preventDefault();
      lastElement.focus();
    }
  } else {
    // Check for Tab
    if (document.activeElement === lastElement) {
      event.preventDefault();
      firstElement.focus();
    }
  }
};

export default keepFocusInside;
