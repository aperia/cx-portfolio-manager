import { isNumber } from 'lodash';

const isProduction = process.env.NODE_ENV === 'production';
const isTest = isNumber(process.env.JEST_WORKER_ID);

/**
 * Generate automation test id
 */
const genAmtId = (
  id: string | undefined,
  suffix: string | undefined,
  componentName: string
) => {
  if (!id) {
    if (!isProduction && !isTest && window?.location?.hash === '#data-testid') {
      console.groupCollapsed('You are missing DLS id: ', componentName);
      console.trace();
      console.groupEnd();
    }
    return;
  }

  let nextId = id;
  if (suffix) nextId = `${id}_${suffix}`;

  return nextId.replace(/[^a-zA-Z0-9_-]/gi, '');
};

export default genAmtId;
