// Module generate and throw error

export default {
  /**
   * - Throw not found error
   */
  notFound: (key: string, value?: any) => {
    throw new Error(
      `[DLS] - Can't found value with ${key}=${value}, please define your ${key}`
    );
  },
  /**
   * - Throw can't less than error
   */
  cantLessThan: (key: string, value?: any) => {
    throw new Error(`[DLS] - ${key} can't be less than ${value}`);
  },
  /**
   * - Throw can't more than error
   */
  cantMoreThan: (key: string, value?: any) => {
    throw new Error(`[DLS] - ${key} can't be more than ${value}`);
  }
};
