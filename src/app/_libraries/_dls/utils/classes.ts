const classes = Object.freeze({
  state: {
    selected: 'dls-selected',
    focused: 'dls-focused',
    disabled: 'dls-disabled',
    readOnly: 'dls-readonly',
    required: 'dls-required',
    floating: 'dls-floating',
    error: 'dls-error',
    noLabel: 'dls-no-label',
    small: 'dls-small',
    hover: 'dls-hover',
    hidden: 'dls-hidden'
  },
  inputContainer: {
    container: 'text-field-container',
    input: 'input',
    placeholder: 'placeholder'
  },
  popupBase: {
    container: 'dls-popup'
  },
  numericTextBox: {
    onlySuffix: 'only-suffix',
    noSuffix: 'no-suffix'
  },
  dropdownList: {
    container: 'dls-dropdown-list',
    noBorder: 'no-border',
    bubble: 'dls-bubble',
    textBadge: 'dls-text-badge',
    badge: 'dls-badge'
  },
  multiSelect: {
    container: 'dls-multi-select',
    variantNormal: 'variant-normal',
    variantGroup: 'variant-group',
    variantNoBorder: 'variant-no-border'
  },
  floatingDropdownButton: {
    container: 'dls-floating-dropdown-button'
  },
  combobox: {
    container: 'dls-combobox',
    bubble: 'dls-bubble'
  },
  dropdownButton: {
    container: 'dls-dropdown-button',
    noBorder: 'no-border'
  },
  textarea: {
    container: 'dls-textarea'
  },
  popover: {
    inner: 'dls-popover-inner',
    arrow: 'dls-popover-arrow',
    container: 'dls-popover-container'
  },
  progressTracker: {
    container: 'dls-progress-tracker',
    stepProgress: 'dls-step-progress-tracker',
    stepItem: 'dls-step-progress-tracker-item'
  },
  datePicker: {
    container: 'dls-date-picker',
    control: 'date-picker-control'
  },
  dateRangePicker: {
    container: 'dls-date-range-picker'
  },
  textSearch: {
    container: 'dls-text-search',
    searchIcon: 'dls-text-search-icon',
    input: 'dls-text-search-input',
    clearButton: 'dls-text-search-clear',
    searchButton: 'dls-text-search-search'
  },
  colorPicker: {
    container: 'dls-color-picker',
    noColor: 'no-color',
    hasBorder: 'has-border'
  },
  utils: {
    ellipsis: 'dls-ellipsis'
  }
});

export default classes;
