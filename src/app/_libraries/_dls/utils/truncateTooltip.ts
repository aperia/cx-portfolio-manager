// utils
import canvasTextWidth from './canvasTextWidth';
import isNumber from 'lodash.isnumber';

const _truncateTooltip = (element: HTMLElement) => {
  const elementComputedStyle = window.getComputedStyle(element);
  const paddingLeft = +elementComputedStyle
    .getPropertyValue('padding-left')
    .replace('px', '');
  const paddingRight = +elementComputedStyle
    .getPropertyValue('padding-right')
    .replace('px', '');
  if (!isNumber(paddingLeft) || isNaN(paddingLeft)) return;
  if (!isNumber(paddingRight) || isNaN(paddingRight)) return;

  const textMetrics = canvasTextWidth(
    element.innerText,
    elementComputedStyle.font
  );
  if (!textMetrics) return;

  if (
    element.offsetWidth - paddingLeft - paddingRight <=
    Math.floor(textMetrics.width)
  ) {
    element.title = element.innerText?.replace('*', '');
  } else element.title = '';
};

/**
 * - Handle toggle show tooltip (title) for element if element truncated...
 * @param element Element need to check
 * @param awaitNextTick Should only run on the next tick
 */
const truncateTooltip = (element: HTMLElement, awaitNextTick = false) => {
  if (!element) return;

  // trong mot vai truong hop, width cua element se bi dieu chinh lai
  // vi du: popperjs tu dong canh chinh width theo reference cua no
  if (awaitNextTick) {
    process.nextTick(() => _truncateTooltip(element));
    return;
  }

  _truncateTooltip(element);
};

export default truncateTooltip;
