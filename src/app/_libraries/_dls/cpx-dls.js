const fs = require('fs-extra');
const path = require('path');

(async function () {
  console.log('[DLS] Trying to get the latest code...');
  try {
    const dlsBSPath = process.argv[2] || '';
    fs.lstatSync(dlsBSPath).isDirectory();

    const basePath = 'src';
    const pathList = [
      'components',
      'fonts',
      'hooks',
      'providers',
      'utils',
      'lodash',
      'global.d.ts',
      'test-utils'
    ];

    // remove and copy code
    for (let index = 0; index < pathList.length; index++) {
      const segment = pathList[index];
      const srcFullPath = path.resolve(dlsBSPath, basePath, segment);
      const destFullPath = path.resolve(__dirname, segment);

      await fs.remove(destFullPath);
      console.log(destFullPath, 'removed...');
      await fs.copy(srcFullPath, destFullPath);
      console.log(srcFullPath, 'is copied to', destFullPath);
    }

    // remove and copy main.css, fonts
    const mainCSSSegments = ['public', 'css'];
    const fontsSegments = ['public', 'fonts'];
    const mainCSSPath = path.resolve(__dirname, ...mainCSSSegments);
    const fontsPath = path.resolve(__dirnamem, ...fontsSegments);
    const destMainCSSPath = path.resolve(dlsBSPath, ...mainCSSSegments);
    const destFontsPath = path.resolve(dlsBSPath, ...fontsSegments);

    await fs.remove(destMainCSSPath);
    console.log(destMainCSSPath, 'removed...');
    await fs.copy(mainCSSPath, 'is copied to', destMainCSSPath);

    await fs.remove(destFontsPath);
    console.log(destFontsPath, 'removed...');
    await fs.copy(fontsPath, 'is copied to', destFontsPath);
  } catch (error) {
    if (error.code === 'ENOENT') {
      return console.error(
        '[DLS] You need to pass the aperia-dls-bs path folder to run this code.'
      );
    }
    console.log('[DLS] Get the latest code failed...', error);
  }
})();