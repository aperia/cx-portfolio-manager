import React, { Children } from 'react';
import { render } from '@testing-library/react';

import { useInputWithLabel } from '..';
import CheckBox from '../../components/CheckBox';
import Tooltip from '../../components/Tooltip';

const Wrapper = () => {
  useInputWithLabel(true, {}, {}, {});
  return <div>Wrapper</div>;
};

describe('usePopper', () => {
  Object.defineProperty(Children, 'toArray', {
    value: () => ({
      find: () => undefined
    })
  });

  it('should render Input', () => {
    const wrapper = render(<Wrapper />);
    expect(wrapper.queryByText('Wrapper')).toBeInTheDocument();
  });

  it('click fake label', () => {
    const { rerender } = render(
      <CheckBox>
        <Tooltip element="Tooltip for input element">
          <CheckBox.Input id="checkbox1" />
        </Tooltip>
        <CheckBox.Label htmlFor="checkbox1">
          Uncontrolled CheckBox (Tooltip hover on CheckBox)
        </CheckBox.Label>
      </CheckBox>
    );

    rerender(
      <CheckBox>
        <Tooltip element="Tooltip for input element">
          <CheckBox.Input id="checkbox1" />
        </Tooltip>
        <CheckBox.Label htmlFor="checkbox1">
          Uncontrolled CheckBox (Tooltip hover on CheckBox)
        </CheckBox.Label>
      </CheckBox>
    );
  });
});
