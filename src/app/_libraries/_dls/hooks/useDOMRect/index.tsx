import { useEffect, useRef, useState } from 'react';

// utils/observer
import ResizeObserver from 'resize-observer-polyfill';
import isEqual from 'lodash.isequal';
import pick from 'lodash.pick';

// types
import { UseDOMRectHook } from './types';

const useDOMRect: UseDOMRectHook = ({ element, watch = true, picks } = {}) => {
  const [DOMRect, setDOMRect] = useState<DOMRect>();
  const previousPicksRef = useRef(picks);

  // inline array prop will be not re-running useEffect below
  if (!isEqual(previousPicksRef.current, picks)) {
    previousPicksRef.current = picks;
  }
  const nextPicks = previousPicksRef.current;

  useEffect(() => {
    if (!watch || !element) return;

    const resizeObserverCallback = () => {
      const nextDOMRect = pick(
        element.getBoundingClientRect(),
        ...(nextPicks || [])
      );

      setDOMRect((currentDOMRect) => {
        if (isEqual(currentDOMRect, nextDOMRect)) return currentDOMRect;
        return element.getBoundingClientRect();
      });
    };

    const resizeObserver = new ResizeObserver(resizeObserverCallback);
    resizeObserver.observe(element);

    // run first time
    resizeObserverCallback();

    return () => resizeObserver.disconnect();
  }, [element, watch, nextPicks]);

  return (DOMRect || {}) as DOMRect;
};

export default useDOMRect;
