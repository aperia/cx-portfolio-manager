import {
  AnyAction,
  applyMiddleware,
  combineReducers,
  compose,
  createStore,
  Reducer,
  ReducersMapObject,
  Store
} from 'redux';
import { reducer as form } from 'redux-form';
import dynamicMiddleware from './dynamicMiddleware';
import { caches } from './reducers';
import { AppState } from './states';

export function createAppStore(): Store {
  const composeEnhancers =
    process.env.NODE_ENV === 'development' &&
    (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
      ? (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
          // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
        })
      : compose;
  const enhancers = composeEnhancers(
    applyMiddleware(dynamicMiddleware.enhancer)
  );

  return createStore<any, AnyAction, unknown, unknown>(
    createReducer(),
    enhancers
  );
}

export function createReducer(
  customReducers?: ReducersMapObject<any, AnyAction>
): Reducer {
  return combineReducers<AppState, AnyAction>({
    ...(customReducers || {}),
    form,
    caches
  });
}
export function addReducersToStore(
  appStore: Store,
  reducers?: ReducersMapObject<any, AnyAction>
) {
  reducers && appStore.replaceReducer(createReducer(reducers));
}

const store = createAppStore();
export default store;
