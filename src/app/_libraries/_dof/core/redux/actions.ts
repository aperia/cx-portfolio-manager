import React from 'react';
import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';

import { RESOLVE_CONTROL_FAILURE, RESOLVE_CONTROL_SUCCESS, RESOLVE_VIEW_FAILURE, RESOLVE_VIEW_SUCCESS } from './actionTypes';
import { Promising, isPromisingOf } from '..';
import { AppState } from './states';
import { ViewDescriptor } from '../interfaces';

export interface ResolveControlFailureAction extends Action {
    type: typeof RESOLVE_CONTROL_FAILURE;
    controlKey: string;
    error: any;
}

export interface ResolveControlFailureActionCreator {
    (controlKey: string, error: any): ResolveControlFailureAction;
}

export const resolveControlFailure: ResolveControlFailureActionCreator =
    (controlKey, error) => ({
        type: RESOLVE_CONTROL_FAILURE,
        controlKey,
        error
    });

export interface ResolveControlSuccessAction extends Action {
    type: typeof RESOLVE_CONTROL_SUCCESS;
    controlKey: string;
    controlType: React.ComponentType<any>;
}

export interface ResolveControlSuccessActionCreator {
    (controlKey: string, controlType: React.ComponentType<any>): ResolveControlSuccessAction;
}

export const resolveControlSuccess: ResolveControlSuccessActionCreator =
    (controlKey, controlType) => ({
        type: RESOLVE_CONTROL_SUCCESS,
        controlKey,
        controlType
    });

export interface ResolveControlActionCreator {
    (controlKey: string, registeredControl: React.ComponentType<any> | Promising<React.ComponentType<any>>)
        : ThunkAction<any, AppState, never, ResolveControlSuccessAction | ResolveControlFailureAction>;
}

export const resolveControl: ResolveControlActionCreator =
    (controlKey, registeredControl) => async dispatch => {
        if (isPromisingOf<React.ComponentType<any>>(registeredControl)) {
            try {
                const { promise } = registeredControl;
                const controlType = await promise();
                dispatch(resolveControlSuccess(controlKey, controlType));
            }
            catch (error) {
                dispatch(resolveControlFailure(controlKey, error));
            }
        } else {
            dispatch(resolveControlSuccess(controlKey, registeredControl));
        }
    };

export interface ResolveViewFailureAction extends Action {
    type: typeof RESOLVE_VIEW_FAILURE;
    viewId: string;
    error: any;
}

export interface ResolveViewFailureActionCreator {
    (viewId: string, error: any): ResolveViewFailureAction;
}

export const resolveViewFailure: ResolveViewFailureActionCreator = (viewId, error) => ({
    type: RESOLVE_VIEW_FAILURE,
    viewId,
    error
});

export interface ResolveViewSuccessAction extends Action {
    type: typeof RESOLVE_VIEW_SUCCESS;
    viewId: string;
    viewDescriptor: Omit<ViewDescriptor<any, any>, 'name'>;
}

export interface ResolveViewSuccessActionCreator {
    (viewId: string, viewDescriptor: Omit<ViewDescriptor<any, any>, 'name'>): ResolveViewSuccessAction;
}

export const resolveViewSuccess: ResolveViewSuccessActionCreator = (viewId, viewDescriptor) => ({
    type: RESOLVE_VIEW_SUCCESS,
    viewId,
    viewDescriptor
});

export interface ResolveViewActionCreator {
    (viewId: string, viewDescriptorPromise: Promise<Omit<ViewDescriptor<any, any>, 'name'>>)
        : ThunkAction<any, AppState, never, ResolveViewSuccessAction | ResolveViewFailureAction>;
}

export const resolveView: ResolveViewActionCreator = (viewId, viewDescriptorPromise) => async (dispatch) => {
    try {
        const viewDescriptor = await viewDescriptorPromise;
        dispatch(resolveViewSuccess(viewId, viewDescriptor));
    }
    catch (error) {
        dispatch(resolveViewFailure(viewId, error));
    }
};