import store, { addReducersToStore } from "./createAppStore";
import { ReducersMapObject, AnyAction, Reducer } from "redux";

describe("Text Redux > createAppStore", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it("test addReducersToStore function", () => {
    const reducerMock: Reducer<{ data: string }> = (
      state = { data: "mock-data" }
    ) => {
      return state;
    };

    addReducersToStore(store, { reducerMock } as ReducersMapObject<
      any,
      AnyAction
    >);

    expect(store.getState().reducerMock).toEqual({
      data: "mock-data",
    });
  });
});
