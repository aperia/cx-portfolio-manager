import DataApiContext from "./DataApiContext";
import { renderHook } from "@testing-library/react-hooks";
import { useContext } from "react";

describe("Test contexts > DataApiContext.ts", () => {
  it("should return default context data", () => {
    const {
      result: { current: mockContext },
    } = renderHook(() => useContext(DataApiContext));
    expect(mockContext).toBeUndefined();
  });
});
