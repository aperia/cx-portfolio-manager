import React from "react";
import {
  FieldDescriptor,
  ViewLayoutProps,
  ViewDescriptor,
  DataApiConfiguration,
} from "../interfaces/dynamic";
import { Promising } from "../interfaces/types";
import { HighOrderValidationFactory } from "../validation/interfaces";

export interface RegisteredControlsMap {
  [key: string]: React.ComponentType<any> | Promising<React.ComponentType<any>>;
}

export interface RegisteredFieldsMap {
  [name: string]: Omit<FieldDescriptor<any>, "name">;
}

export interface RegisteredHighOrderValidationFactoriesMap {
  [key: string]: HighOrderValidationFactory<any>;
}

export interface RegisteredLayoutsMap {
  [key: string]: React.ComponentType<ViewLayoutProps<any>>;
}

export interface RegisteredViewsMap {
  [name: string]:
    | Omit<ViewDescriptor<any, any>, "name">
    | Promise<Omit<ViewDescriptor<any, any>, "name">>;
}

export interface RegisteredDataSourceMap {
  [name: string]: Omit<DataApiConfiguration, "name">;
}

export interface RegisteredComponentsContextData {
  registeredControls: RegisteredControlsMap;
  registeredFields: RegisteredFieldsMap;
  registeredHighOrderValidationFactories: RegisteredHighOrderValidationFactoriesMap;
  registeredLayouts: RegisteredLayoutsMap;
  registeredViews: RegisteredViewsMap;
  registeredDataSource: RegisteredDataSourceMap;
  allViews: Array<string> | undefined;
}

const RegisteredComponentsContext = React.createContext<
  RegisteredComponentsContextData
>({
  registeredControls: {},
  registeredFields: {},
  registeredHighOrderValidationFactories: {},
  registeredLayouts: {},
  registeredViews: {},
  registeredDataSource: {},
  allViews: undefined,
});

export default RegisteredComponentsContext;
