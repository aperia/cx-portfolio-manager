import { isClass, isPromisingOf, Promising } from "./";

describe("Test interfaces > types.ts", () => {
  it("test isClass function", () => {
    expect(isClass("mock string test")).toEqual(false);
    class MockCls {}
    expect(isClass(MockCls)).toEqual(true);
  });
  it("test isPromisingOf function", () => {
    expect(isPromisingOf("mock string test")).toEqual(false);
    const mockPromising: Promising<string> = {
      promise: () => Promise.resolve("mock string"),
    };

    expect(isPromisingOf(mockPromising)).toEqual(true);
  });
});
