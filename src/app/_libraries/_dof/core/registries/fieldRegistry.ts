import {
  FieldsMap,
  FieldRegistry,
  FieldDescriptor,
} from "../interfaces/dynamic";

class DefaultFieldRegistry implements FieldRegistry {
  readonly fieldsMap: FieldsMap = {};

  registerField(fieldId: string, field: FieldDescriptor<any>): void {
    this.fieldsMap[fieldId] = Promise.resolve(field);
  }

  registerFields(fields: Array<FieldDescriptor<any>>): void {
    (async () => {
      const descriptors = await Promise.resolve(fields);
      descriptors.forEach(({ name, ...descriptor }) =>
        this.registerField(name, { name, ...descriptor }),
      );
    })();
  }

  unregisterField(fieldId: string): void {
    delete this.fieldsMap[fieldId];
  }
}

const fieldRegistry: FieldRegistry = new DefaultFieldRegistry();

export default fieldRegistry;
