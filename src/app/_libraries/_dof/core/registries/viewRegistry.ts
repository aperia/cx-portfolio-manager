import {
  ViewsMap,
  ViewRegistry,
  ViewDescriptor,
  ViewLayoutProps,
} from "../interfaces/dynamic";

class DefaultViewRegistry implements ViewRegistry {
  readonly viewsMap: ViewsMap = {};
  allViews: Array<string> = [];

  registerView<
    LayoutOptions extends ViewLayoutProps<LayoutProps> = ViewLayoutProps<any>,
    LayoutProps = unknown
  >(
    viewId: string,
    viewDescriptor:
      | Omit<ViewDescriptor<LayoutOptions, LayoutProps>, "name">
      | Promise<Omit<ViewDescriptor<LayoutOptions, LayoutProps>, "name">>,
  ): void {
    this.viewsMap[viewId] = viewDescriptor;
  }

  registerViews(
    viewDescriptors:
      | Array<ViewDescriptor<any, any>>
      | Promise<Array<ViewDescriptor<any, any>>>,
  ): void {
    (async () => {
      const descriptors = await Promise.resolve(viewDescriptors);
      descriptors.forEach(({ name, ...descriptor }) =>
        this.registerView(name, descriptor),
      );
    })();
  }

  registerAvailableViews(allViews: Array<string>): void {
    this.allViews = allViews;
  }

  unregisterView(viewId: string): void {
    delete this.viewsMap[viewId];
  }
}

const viewRegistry: ViewRegistry = new DefaultViewRegistry();

export default viewRegistry;
