import { ValidatorsMap, ValidatorRegistry } from '../interfaces/dynamic';
import { ValidatorConstructor, ValidationOptions } from '../validation';

class DefaultValidatorRegistry implements ValidatorRegistry {
    readonly validatorsMap: ValidatorsMap<any> = {};

    registerValidator<TOptions extends ValidationOptions<TError> = ValidationOptions<any>, TValue = any, TError = string>(
        validatorKey: string, 
        validatorClass: ValidatorConstructor<TOptions, TValue, TError>
    ): void {
        this.validatorsMap[validatorKey] = Promise.resolve(validatorClass);
    }

    unregisterValidator(controlKey: string): void {
        delete this.validatorsMap[controlKey];
    }
}

const validatorRegistry: ValidatorRegistry = new DefaultValidatorRegistry();

export default validatorRegistry;