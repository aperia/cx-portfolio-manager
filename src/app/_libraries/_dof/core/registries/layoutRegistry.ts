import React from 'react';
import { LayoutsMap, LayoutRegistry } from '../interfaces/dynamic';

class DefaultLayoutRegistry implements LayoutRegistry {
    readonly layoutsMap: LayoutsMap = {};

    registerLayout(layoutKey: string, layoutComponent: React.ComponentType<any>): void {
        this.layoutsMap[layoutKey] = Promise.resolve(layoutComponent);
    }

    unregisterLayout(layoutKey: string): void {
        delete this.layoutsMap[layoutKey];
    }
}

const layoutRegistry: LayoutRegistry = new DefaultLayoutRegistry();

export default layoutRegistry;