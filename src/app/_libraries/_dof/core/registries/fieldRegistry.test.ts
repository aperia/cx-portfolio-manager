import { fieldRegistry } from "./";
import { FieldDescriptor } from "..";

describe("Text Registries > fieldRegistry", () => {
  const mockFieldComponent = {
    name: "mock-field-com-1",
    type: "combobox",
    dataField: "mockDataField",
  } as FieldDescriptor<any>;

  afterEach(() => {
    jest.clearAllMocks();
  });

  it("test registerField function", () => {
    fieldRegistry.registerField("mock-field-1", mockFieldComponent);

    expect(fieldRegistry.fieldsMap["mock-field-1"]).toEqual(
      Promise.resolve(mockFieldComponent)
    );
  });

  it("test registerFields function", () => {
    const mockFieldComponent2 = {
      name: "mock-field-com-2",
      type: "label",
      dataField: "mockDataField",
    } as FieldDescriptor<any>;

    return Promise.resolve(
      fieldRegistry.registerFields([mockFieldComponent, mockFieldComponent2])
    ).then(() => {
      expect(fieldRegistry.fieldsMap["mock-field-com-1"]).toEqual(
        Promise.resolve(mockFieldComponent)
      );
      expect(fieldRegistry.fieldsMap["mock-field-com-2"]).toEqual(
        Promise.resolve(mockFieldComponent2)
      );
    });
  });

  it("test unregisterField function", () => {
    fieldRegistry.unregisterField("mock-field-1");

    expect(fieldRegistry.fieldsMap["mock-field-1"]).toBeUndefined();
  });
});
