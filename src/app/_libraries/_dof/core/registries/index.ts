export { default as controlRegistry } from "./controlRegistry";
export { default as fieldRegistry } from "./fieldRegistry";
export { default as layoutRegistry } from "./layoutRegistry";
export { default as validatorRegistry } from "./validatorRegistry";
export { default as viewRegistry } from "./viewRegistry";
export { default as valueChangeRegistry } from "./valueChangeRegistry";
