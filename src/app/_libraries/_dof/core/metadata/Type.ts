import 'reflect-metadata';
import { Class } from '../interfaces/types';

export type PropertyType<T> = 'string' | 'number' | 'boolean' | DateConstructor | Class<T>;

/**
 * When decorated on a property of a class, specifies the type of the property value at runtime.
 */
export interface Type {
    <T>(type: PropertyType<T>): PropertyDecorator;
}

const TypeMetadataKey = Symbol('TypeMetadataKey');

export const Type: Type = (type) => Reflect.metadata(TypeMetadataKey, type);

export const getType = <T>(target: Object, propertyKey: string | symbol): PropertyType<T> => 
    Reflect.getMetadata(TypeMetadataKey, target, propertyKey);
