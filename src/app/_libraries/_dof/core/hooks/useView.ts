import { useContext, useEffect, } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import RegisteredComponentsContext from '../contexts/RegisteredComponentsContext';

import { AppState } from '../redux/states';
import { resolveView, resolveViewSuccess } from '../redux/actions';
import { ViewDescriptor } from '../interfaces';

import { isPromise } from '../utils';

export interface UseViewHook {
    (viewIdOrDescriptor: string | ViewDescriptor<any, any> | Promise<ViewDescriptor<any, any>>)
        : Omit<ViewDescriptor<any, any>, 'name'> | undefined;
}

const useView: UseViewHook = (viewIdOrDescriptor: string | ViewDescriptor<any, any> | Promise<ViewDescriptor<any, any>>) => {
    const dispatch = useDispatch();
    const { registeredViews } = useContext(RegisteredComponentsContext);

    const cachedViewDescriptor = useSelector<AppState, Omit<ViewDescriptor<any, any>, 'name'> | undefined>(
        state => {
            if (isPromise(viewIdOrDescriptor)) {
                return undefined;
            }

            const viewName = typeof viewIdOrDescriptor === 'string' 
                ? viewIdOrDescriptor
                : viewIdOrDescriptor.name;

            // Check view can cache in redux. If not, return from context!
            const registeredView = registeredViews[viewName] as any;
            if (registeredView !== undefined) {
                const isCached = registeredView.isCached;
                if(isCached === false) return registeredView;
            }
            //==== END 20201111 ===
            return (state.caches.views && state.caches.views[viewName]) || undefined;
        },
        (prev, next) => (prev === undefined && next === undefined) || (prev === next)
    );

    useEffect(() => {
        if (cachedViewDescriptor === undefined) {
            if (typeof viewIdOrDescriptor === 'string') {
                const registeredView = registeredViews[viewIdOrDescriptor];

                if (registeredView !== undefined) {
                    
                    dispatch(resolveView(viewIdOrDescriptor, Promise.resolve(registeredView)));
                }
            } else {
                (async () => {
                    try {
                        const viewDescriptor = await Promise.resolve(viewIdOrDescriptor);
                        dispatch(resolveViewSuccess(viewDescriptor.name, viewDescriptor));
                    } 
                    catch (error) {
                        console.error(`Error while resolving view ${viewIdOrDescriptor} `, error);
                    }
                })();
            }
        }
    }, [dispatch, cachedViewDescriptor, registeredViews, viewIdOrDescriptor])

    return cachedViewDescriptor;
}

export default useView;