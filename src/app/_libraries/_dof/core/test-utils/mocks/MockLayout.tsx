import React from 'react';
import { ViewLayoutProps } from '../../interfaces';

const MockLayout: React.ComponentType<ViewLayoutProps<any>> = ({
  viewElements,
  viewName
}) => {
  return (
    <div role="dof-view" role-name={viewName}>
      <div>Layout container</div>
      {viewElements &&
        viewElements.map((v: any, index: number) => (
          <div key={index}>{v.renderedNode}</div>
        ))}
    </div>
  );
};

export default MockLayout;
