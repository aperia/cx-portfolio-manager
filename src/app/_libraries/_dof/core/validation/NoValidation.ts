import { ValidationFn } from './interfaces';

/**
 * A validation function which does nothing.
 */
const NoValidation: ValidationFn<any, any> = () => undefined;

export default NoValidation;