import { HighOrderValidationFactory, ValidationOptions } from '../interfaces';
import OfType from './OfType';

export interface MinDateValidationOptions<TError = string> extends ValidationOptions<TError> {
    minValue?: Date;
}

const MinDate: HighOrderValidationFactory<MinDateValidationOptions, Date> = ({ 
    minValue, 
    errorMsg = `Value must be a date that is equal to or greater than ${minValue}.`
} = {}) => (next) => 
    OfType<Date>(errorMsg, Date)((value, context) => 
        value !== null && value !== undefined && minValue !== undefined && value < minValue ? errorMsg : next(value, context));

export default MinDate;
