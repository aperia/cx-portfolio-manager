import { CrossFieldsValidationOptions, HighOrderValidationFactory } from '../interfaces';

const GreaterThan: HighOrderValidationFactory<CrossFieldsValidationOptions, any> = ({ 
    fieldToCompare, 
    errorMsg
} = {}) => (next) => (value, context) => 
    !!value && !!fieldToCompare && value <= context.valueOf(fieldToCompare) 
        ? (errorMsg || `Value must be greater than ${context.labelOf(fieldToCompare)}.`) 
        : next(value, context);

export default GreaterThan;
