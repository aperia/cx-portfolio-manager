import { HighOrderValidationFactory, ValidationOptions } from "../interfaces";
import OfType from "./OfType";

export interface RegExpValidationOptions<TError = string>
  extends ValidationOptions<TError> {
  regExp?: string;
}

const Pattern: HighOrderValidationFactory<RegExpValidationOptions, string> = ({
  regExp,
  errorMsg = "Invalid!",
} = {}) => (next) =>
  OfType<string>(
    errorMsg,
    "string",
  )((value, context) => {
    return value !== null &&
      value !== undefined &&
      value !== "" &&
      regExp != undefined &&
      !new RegExp(regExp).test(value)
      ? errorMsg
      : next(value, context);
  });

export default Pattern;
