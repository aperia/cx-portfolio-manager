import { HighOrderValidationFactory, ValidationOptions } from "../interfaces";
import OfType from "./OfType";

export interface MinLengthValidationOptions<TError = string>
  extends ValidationOptions<TError> {
  minLength?: number;
}

const MinLength: HighOrderValidationFactory<
  MinLengthValidationOptions,
  string
> = ({
  minLength,
  errorMsg = `Value must be a text that has at least ${minLength} letter(s).`,
} = {}) => (next) =>
  OfType<string>(
    errorMsg,
    "string"
  )((value, context) => {
    return value !== null &&
      value !== undefined &&
      value !== "" &&
      minLength !== undefined &&
      value.length < minLength
      ? errorMsg
      : next(value, context);
  });

export default MinLength;
