import MinDate from "./MinDate";
import NoValidation from "../NoValidation";
import { ValidationContext } from "../interfaces";

describe("MinDate(Date)", function () {
  const context: ValidationContext = {
    labelOf(): string | undefined {
      return undefined;
    },
    valueOf() {
      return undefined;
    },
  };

  it("should return a validation message when a date value is less than the given minimum value.", function () {
    const validationFn = MinDate({ minValue: new Date("2000-01-01") })(
      NoValidation
    );
    const message = validationFn(new Date("1999-12-31"), context);
    expect(message).not.toBeUndefined();
  });

  it("should NOT return a validation message when a date value is equal to the given minimum value.", function () {
    const validationFn = MinDate({ minValue: new Date("2000-01-01") })(
      NoValidation
    );
    const message = validationFn(new Date("2000-01-01"), context);
    expect(message).toBeUndefined();
  });

  it("should NOT return a validation message when a date value is greater than the given minimum value.", function () {
    const validationFn = MinDate({ minValue: new Date("2000-01-01") })(
      NoValidation
    );
    const message = validationFn(new Date("2000-12-31"), context);
    expect(message).toBeUndefined();
  });

  it("should return a validation message when options is undefined.", function () {
    const validationFn = MinDate(undefined as any)(NoValidation);
    const message = validationFn(new Date("1999-12-31"), context);
    expect(message).toBeUndefined();
  });
});
