import Phone from "./Phone";
import NoValidation from "../NoValidation";
import { ValidationContext } from "../interfaces";

describe("Phone(string)", function () {
  const context: ValidationContext = {
    labelOf(): string | undefined {
      return undefined;
    },
    valueOf() {
      return undefined;
    },
  };

  it("should return a validation message when a value has length that is different from given length.", function () {
    const validationFn = Phone({ length: 10 })(NoValidation);
    const message = validationFn("(1234)222-3333", context);
    expect(message).not.toBeUndefined();
  });

  it("should NOT return a validation message when a value has length that equal to given length.", function () {
    const validationFn = Phone({ length: 10 })(NoValidation);
    const message = validationFn("(123)222-3333", context);
    expect(message).toBeUndefined();
  });

  it("should NOT return a validation message when value and options are undefined", function () {
    const validationFn = Phone(undefined as any)(NoValidation);
    const message = validationFn(undefined, context);
    expect(message).toBeUndefined();
  });
});
