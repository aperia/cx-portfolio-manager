import Min from "./Min";
import NoValidation from "../NoValidation";
import { ValidationContext } from "../interfaces";

describe("Min(number)", function () {
  const context: ValidationContext = {
    labelOf(): string | undefined {
      return undefined;
    },
    valueOf() {
      return undefined;
    },
  };

  it("should return a validation message when a number value is less than the given minimum value.", function () {
    const validationFn = Min({ minValue: 1 })(NoValidation);
    const message = validationFn(0, context);
    expect(message).not.toBeUndefined();
  });

  it("should NOT return a validation message when a number value is equal to the given minimum value.", function () {
    const validationFn = Min({ minValue: 1 })(NoValidation);
    const message = validationFn(1, context);
    expect(message).toBeUndefined();
  });

  it("should NOT return a validation message when a number value is greater than the given minimum value.", function () {
    const validationFn = Min({ minValue: 1 })(NoValidation);
    const message = validationFn("10", context);
    expect(message).toBeUndefined();
  });

  it("should return a validation message when a STRING number value is less than the given minimum value.", function () {
    const validationFn = Min({ minValue: 1 })(NoValidation);
    const message = validationFn("0", context);
    expect(message).not.toBeUndefined();
  });

  it("should return a validation message when options is undefined.", function () {
    const validationFn = Min(undefined as any)(NoValidation);
    const message = validationFn(0, context);
    expect(message).toBeUndefined();
  });
});
