import { CrossFieldsValidationOptions, HighOrderValidationFactory } from '../interfaces';

const LessThan: HighOrderValidationFactory<CrossFieldsValidationOptions, any> = ({ 
    fieldToCompare, 
    errorMsg
} = {}) => (next) => (value, context) => 
    !!value && !!fieldToCompare && value >= context.valueOf(fieldToCompare) 
        ? (errorMsg || `Value must be less than ${context.labelOf(fieldToCompare)}.`) 
        : next(value, context);

export default LessThan;
