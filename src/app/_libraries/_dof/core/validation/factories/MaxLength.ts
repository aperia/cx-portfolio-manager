import { HighOrderValidationFactory, ValidationOptions } from "../interfaces";
import OfType from "./OfType";

export interface MaxLengthValidationOptions<TError = string>
  extends ValidationOptions<TError> {
  maxLength?: number;
}

const MaxLength: HighOrderValidationFactory<
  MaxLengthValidationOptions,
  string
> = ({
  maxLength,
  errorMsg = `Value must be a text that has at most ${maxLength} letter(s).`,
} = {}) => (next) =>
  OfType<string>(
    errorMsg,
    "string"
  )((value, context) =>
    value !== null &&
    value !== undefined &&
    maxLength !== undefined &&
    value !== "" &&
    value.length > maxLength
      ? errorMsg
      : next(value, context)
  );

export default MaxLength;
