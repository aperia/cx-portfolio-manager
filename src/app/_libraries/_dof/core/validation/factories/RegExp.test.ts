import RegExp from "./RegExp";
import NoValidation from "../NoValidation";
import { ValidationContext } from "../interfaces";

describe("Phone(string)", function () {
  const context: ValidationContext = {
    labelOf(): string | undefined {
      return undefined;
    },
    valueOf() {
      return undefined;
    },
  };

  it("should return a validation message when a value dont match input regex.", function () {
    const validationFn = RegExp({ regExp: "^mock" })(NoValidation);
    const message = validationFn("data-mock", context);
    expect(message).not.toBeUndefined();
  });

  it("should NOT return a validation message when value match input regex.", function () {
    const validationFn = RegExp({ regExp: "^mock" })(NoValidation);
    const message = validationFn("mock data", context);
    expect(message).toBeUndefined();
  });

  it("should NOT return a validation message when options is undefined", function () {
    const validationFn = RegExp(undefined as any)(NoValidation);
    const message = validationFn("data mock", context);
    expect(message).toBeUndefined();
  });
});
