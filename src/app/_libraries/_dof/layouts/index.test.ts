import { BootstrapGridLayout, HtmlTemplateLayout, SimpleLayout } from './index';

jest.mock('../core', () => {
  return {
    layoutRegistry: {
      registerLayout: jest.fn()
    }
  };
});

describe('Text Index', () => {
  it('Render SimpleLayout', () => {
    expect(typeof BootstrapGridLayout).toEqual('function');
    expect(typeof HtmlTemplateLayout).toEqual('function');
    expect(typeof SimpleLayout).toEqual('function');
  });
});
