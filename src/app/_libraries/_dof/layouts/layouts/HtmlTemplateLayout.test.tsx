import "@testing-library/jest-dom";
import { render, RenderResult, waitFor } from "@testing-library/react";
import React from "react";
import HtmlTemplateLayout from "./HtmlTemplateLayout";

describe("Text Layouts > SimpleLayout", () => {
  let originFetch: any;
  const fetchFn = jest.fn();

  const htmlTemplateLayoutProps = {
    viewName: "mock-view-name",
    viewElements: [],
    templateUrl: "",
  };

  beforeEach(() => {
    jest.resetModules();
    originFetch = window.fetch;
    window.fetch = fetchFn.mockResolvedValue({
      ok: true,
      text: () => `
      <div class="row listview-item mt-16 mb-16">
        <div class="col-3" data-element-ref="lblFirstName"></div>
        <div class="col-3" data-element-ref="lblLastName"></div>
      </div>`,
    });
  });

  afterEach(() => {
    window.fetch = originFetch;
    fetchFn.mockClear();
  });

  const renderHtmlTemplateLayout = (props?: any): RenderResult => {
    return render(
      <HtmlTemplateLayout {...htmlTemplateLayoutProps} {...props} />
    );
  };

  it("Render HtmlTemplateLayout UI, load template url failed", () => {
    fetchFn.mockClear();
    window.fetch = fetchFn.mockResolvedValue({
      ok: false,
    });

    const wrapper = renderHtmlTemplateLayout({
      templateUrl: "mock-url",
      viewElements: undefined,
    });
    expect(wrapper.getByText(/not found!$/i)).toBeInTheDocument();
  });

  it("Render HtmlTemplateLayout UI, load template url success, viewElements with elementRef", async () => {
    const wrapper = renderHtmlTemplateLayout({
      templateUrl: "mock-url",
      as: "span",
      viewElements: [
        {
          layoutProps: {
            elementRef: "lblFirstName",
          } as any,
          renderedNode: <div>Mock First Name</div>,
        },
        {
          layoutProps: {
            elementRef: "lblLastName",
          } as any,
          renderedNode: <div>Mock Last Name</div>,
        },
      ],
    });
    await waitFor(() => {
      expect(wrapper.getByRole("dof-view").tagName).toEqual("SPAN");
      expect(wrapper.getByText("Mock First Name")).toBeInTheDocument();
      expect(wrapper.getByText("Mock Last Name")).toBeInTheDocument();
    });
  });

  it("Render HtmlTemplateLayout UI, load template url success, viewElements with name", async () => {
    const wrapper = renderHtmlTemplateLayout({
      templateUrl: "mock-url",
      as: "article",
      viewElements: [
        {
          name: "lblFirstName",
          renderedNode: <div>Mock First Name</div>,
        },
        {
          name: "lblLastName",
          renderedNode: <div>Mock Last Name</div>,
        },
      ],
    });
    await waitFor(() => {
      expect(wrapper.getByRole("dof-view").tagName).toEqual("ARTICLE");
      expect(wrapper.getByText("Mock First Name")).toBeInTheDocument();
      expect(wrapper.getByText("Mock Last Name")).toBeInTheDocument();
    });
  });

  it("Render HtmlTemplateLayout UI, unmount before loading template url success", async () => {

    const wrapper = renderHtmlTemplateLayout({
      templateUrl: "mock-url",
      viewElements: [
        {
          name: "lblFirstName",
          renderedNode: <div>Mock First Name</div>,
        },
        {
          name: "lblLastName",
          renderedNode: <div>Mock Last Name</div>,
        },
      ],
    });
    wrapper.unmount();
    await waitFor(() => {
      expect(wrapper.queryByRole("dof-view")).toBeNull();
    });
  });
});
