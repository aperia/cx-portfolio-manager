import React from 'react';
import { ViewLayoutProps } from '../../core';

const SimpleLayout: React.FunctionComponent<ViewLayoutProps<never>> = ({
  viewElements
}) => {
  return (
    <React.Fragment>
      {viewElements.map((viewElement, index) => (
        <React.Fragment key={index}>{viewElement.renderedNode}</React.Fragment>
      ))}
    </React.Fragment>
  );
};

export default SimpleLayout;
