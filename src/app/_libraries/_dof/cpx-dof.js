const fs = require('fs-extra');
const path = require('path');

(async function () {
  const dlsBSPath = process.argv[2];

  if (!dlsBSPath) {
    return console.warn(
      '[DOF] You need to pass the dof path folder to run this code.'
    );
  }

  console.group('[DOF] Trying to get the latest code @aperia/core...');

  try {
    const basePath = 'packages/@aperia/core/src';
    const segment = 'core';

    const srcFullPath = path.resolve(dlsBSPath, basePath);
    const destFullPath = path.resolve(__dirname, segment);

    // remove old folder/file
    await fs.remove(destFullPath);
    console.log(destFullPath, 'removed...');

    // copy
    await fs.copy(srcFullPath, destFullPath);
    console.log(srcFullPath, 'is copied to', destFullPath);
  } catch (error) {
    console.log('[DOF] Get the latest code @aperia/core failed...', error);
  }

  console.group('[DOF] Trying to get the latest code @aperia/layouts...');

  try {
    const basePath = 'packages/@aperia/layouts/src';
    const segment = 'layouts';

    const srcFullPath = path.resolve(dlsBSPath, basePath);
    const destFullPath = path.resolve(__dirname, segment);

    // remove old folder/file
    await fs.remove(destFullPath);
    console.log(destFullPath, 'removed...');

    // copy
    await fs.copy(srcFullPath, destFullPath);
    console.log(srcFullPath, 'is copied to', destFullPath);
  } catch (error) {
    console.log('[DOF] Get the latest code @aperia/layouts failed...', error);
  }

  console.groupEnd();
})();
