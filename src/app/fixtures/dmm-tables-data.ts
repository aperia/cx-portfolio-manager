export const DMMTablesData = [
  {
    id: 1969580912,
    serviceName: 'MTI4',
    abbreviation: 'abbr',
    dmmTables: [
      {
        id: 'id1',
        name: 'abc1',
        comment: 'test',
        status: 'updated',
        tableType: {
          name: 'abc',
          abbreviation: 'abbr'
        },
        decisionElementMetaData: {
          decisionTables: [
            {
              id: 1,
              tableName: 'consequat diam et euismod sanctus vero consetetur',
              changeType: 'Removed'
            }
          ]
        }
      }
    ]
  },
  {
    id: 1622679248,
    serviceName: 'MTIC',
    abbreviation: 'KJC',
    dmmTables: [
      {
        id: 'id2',
        name: 'abc2',
        comment: 'test',
        status: 'created',
        tableType: {
          name: 'abc',
          abbreviation: 'abbr'
        },
        decisionElementMetaData: {
          decisionTables: [
            {
              id: 1,
              tableName: 'consequat diam et euismod sanctus vero consetetur',
              changeType: 'Removed'
            },
            {
              id: 1,
              tableName: 'consequat diam et euismod sanctus vero consetetur',
              changeType: 'Removed'
            },
            {
              id: 1,
              tableName: 'consequat diam et euismod sanctus vero consetetur',
              changeType: 'Removed'
            },
            {
              id: 1,
              tableName: 'consequat diam et euismod sanctus vero consetetur',
              changeType: 'Removed'
            },
            {
              id: 1,
              tableName: 'consequat diam et euismod sanctus vero consetetur',
              changeType: 'Removed'
            },
            {
              id: 1,
              tableName: 'consequat diam et euismod sanctus vero consetetur',
              changeType: 'Removed'
            },
            {
              id: 1,
              tableName: 'consequat diam et euismod sanctus vero consetetur',
              changeType: 'Removed'
            },
            {
              id: 1,
              tableName: 'consequat diam et euismod sanctus vero consetetur',
              changeType: 'Removed'
            },
            {
              id: 1,
              tableName: 'consequat diam et euismod sanctus vero consetetur',
              changeType: 'Removed'
            },
            {
              id: 1,
              tableName: 'consequat diam et euismod sanctus vero consetetur',
              changeType: 'Removed'
            },
            {
              id: 1,
              tableName: 'consequat diam et euismod sanctus vero consetetur',
              changeType: 'Removed'
            },
            {
              id: 1,
              tableName: 'consequat diam et euismod sanctus vero consetetur',
              changeType: 'Removed'
            },
            {
              id: 1,
              tableName: 'consequat diam et euismod sanctus vero consetetur',
              changeType: 'Removed'
            },
            {
              id: 1,
              tableName: 'consequat diam et euismod sanctus vero consetetur',
              changeType: 'Removed'
            },
            {
              id: 1,
              tableName: 'consequat diam et euismod sanctus vero consetetur',
              changeType: 'Removed'
            },
            {
              id: 1,
              tableName: 'consequat diam et euismod sanctus vero consetetur',
              changeType: 'Removed'
            },
            {
              id: 1,
              tableName: 'consequat diam et euismod sanctus vero consetetur',
              changeType: 'Removed'
            },
            {
              id: 1,
              tableName: 'consequat diam et euismod sanctus vero consetetur',
              changeType: 'Removed'
            },
            {
              id: 1,
              tableName: 'consequat diam et euismod sanctus vero consetetur',
              changeType: 'Removed'
            },
            {
              id: 1,
              tableName: 'consequat diam et euismod sanctus vero consetetur',
              changeType: 'Removed'
            },
            {
              id: 1,
              tableName: 'consequat diam et euismod sanctus vero consetetur',
              changeType: 'Removed'
            },
            {
              id: 1,
              tableName: 'consequat diam et euismod sanctus vero consetetur',
              changeType: 'Removed'
            },
            {
              id: 1,
              tableName: 'consequat diam et euismod sanctus vero consetetur',
              changeType: 'Removed'
            },
            {
              id: 1,
              tableName: 'consequat diam et euismod sanctus vero consetetur',
              changeType: 'Removed'
            },
            {
              id: 1,
              tableName: 'consequat diam et euismod sanctus vero consetetur',
              changeType: 'Removed'
            },
            {
              id: 1,
              tableName: 'consequat diam et euismod sanctus vero consetetur',
              changeType: 'Removed'
            },
            {
              id: 1,
              tableName: 'consequat diam et euismod sanctus vero consetetur',
              changeType: 'Removed'
            },
            {
              id: 1,
              tableName: 'consequat diam et euismod sanctus vero consetetur',
              changeType: 'Removed'
            },
            {
              id: 1,
              tableName: 'consequat diam et euismod sanctus vero consetetur',
              changeType: 'Removed'
            },
            {
              id: 1,
              tableName: 'consequat diam et euismod sanctus vero consetetur',
              changeType: 'Removed'
            },
            {
              id: 1,
              tableName: 'consequat diam et euismod sanctus vero consetetur',
              changeType: 'Removed'
            }
          ]
        }
      }
    ]
  }
];

export const DMMTablesdMapping = {
  id: 'id',
  serviceName: 'serviceName',
  abbr: 'abbreviation',
  dmmTables: {
    type: 'mappingDataFromArray',
    containerProp: 'dmmTables',
    dmmTables: {
      id: 'id',
      name: 'name',
      comment: 'comment',
      status: 'resultingAction',
      accessible: 'downloadAccessible',
      tableType: 'tableType',
      decisionElementChanges: 'decisionElementMetaData',
      allocationDate: {
        type: 'dynamic',
        containerProp: 'tableControlFields',
        fieldProp: 'allocationDate',
        mappingToType: 'string',
        key: 'fieldName',
        value: 'value'
      },
      allocationFlag: {
        type: 'dynamic',
        containerProp: 'tableControlFields',
        fieldProp: 'allocationFlag',
        mappingToType: 'string',
        key: 'fieldName',
        value: 'value'
      },
      allocationInterval: {
        type: 'dynamic',
        containerProp: 'tableControlFields',
        fieldProp: 'allocationInterval',
        mappingToType: 'string',
        key: 'fieldName',
        value: 'value'
      },
      allocationBACycle: {
        type: 'dynamic',
        containerProp: 'tableControlFields',
        fieldProp: 'allocationBACycle',
        mappingToType: 'string',
        key: 'fieldName',
        value: 'value'
      },
      changeCode: {
        type: 'dynamic',
        containerProp: 'tableControlFields',
        fieldProp: 'changeCode',
        mappingToType: 'string',
        key: 'fieldName',
        value: 'value'
      },
      citMethodFlag: {
        type: 'dynamic',
        containerProp: 'tableControlFields',
        fieldProp: 'citMethodFlag',
        mappingToType: 'string',
        key: 'fieldName',
        value: 'value'
      }
    }
  }
};
