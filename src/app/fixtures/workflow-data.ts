export const WorkflowData = [
  {
    id: '99033437',
    isCancel: true,
    state: 'modify',
    actions: ['edit'],
    workflowTemplate: {
      id: '61108510',
      name: 'Create Change in Terms Event',
      description: 'rebum veniam in luptatum zzril',
      type: '',
      firstExecuted: '2021-03-02T14:47:56.5951554+07:00',
      firstExecutedBy: 'Jesse Dunn',
      lastExecuted: '2021-03-02T14:47:56.595399+07:00',
      lastExecutedBy: 'Nate Nash',
      numberOfExecutions: 14,
      isFavorite: true,
      accesible: false
    },
    notes: 'nonumy at ipsum magna lorem',
    status: 'Incomplete',
    lastUpdated: '2021-03-02T14:47:56.5959464+07:00',
    lastUpdatedBy: 'Nate Nash',
    countOfAttachments: 72
  },
  {
    id: '99033437',
    isCancel: true,
    state: 'deleted',
    actions: ['edit'],
    workflowTemplate: {
      id: '61108510',
      name: 'Create Change in Terms Event',
      description: 'rebum veniam in luptatum zzril',
      type: 'Lending',
      firstExecuted: '2021-03-02T14:47:56.5951554+07:00',
      firstExecutedBy: 'Jesse Dunn',
      lastExecuted: '2021-03-02T14:47:56.595399+07:00',
      lastExecutedBy: 'Nate Nash',
      numberOfExecutions: 14,
      isFavorite: true,
      accesible: false
    },
    notes: 'nonumy at ipsum magna lorem',
    status: 'Incomplete',
    lastUpdated: '',
    lastUpdatedBy: 'Nate Nash',
    countOfAttachments: 72
  }
];

export const WorkflowDetailData = {
  id: '89865077',
  actions: ['edit'],
  workflowTemplate: {
    id: '14260323',
    name: 'Prime Interest Rate Change',
    description: 'est no dolore tempor dolore',
    type: 'Testing (Internal)',
    firstExecuted: '2021-03-01T14:45:13.7246931+07:00',
    firstExecutedBy: 'Mary Sue',
    lastExecuted: '2021-03-03T14:45:13.7247112+07:00',
    lastExecutedBy: 'Mary Sue',
    numberOfExecutions: 27,
    isFavorite: true,
    accesible: false
  },
  notes: 'est aliquam lorem elitr sanctus',
  status: 'Complete',
  lastUpdated: '2021-03-03T14:45:13.7249112+07:00',
  lastUpdatedBy: 'Darth Vader',
  attachments: [
    {
      id: '21722129',
      category: 'transaction.qualification.table',
      filename: 'Employment Agreement.pdf',
      mimeType: 'application/pdf',
      accessible: true,
      fileSize: 247500,
      recordCount: 6,
      uploadedDate: '2020-12-24T00:00:00',
      uploadedBy: 'Jonah Hill'
    }
  ],
  reports: [
    {
      reportId: '56710556',
      filename: 'August Releases_01112021.xlsx',
      mimeType: 'text/csv',
      accessible: false,
      fileSize: 2685287,
      recordCount: 47,
      systemGeneratedDate: '2021-02-11T00:00:00'
    }
  ]
};

// export const WorkflowTemplateData = {
//   id: '03208548',
//   name: 'Modify Incentive Pricing',
//   description: 'vero et exerci labore consetetur esse',
//   type: 'Security',
//   firstExecuted: '2021-03-01T14:45:23.2498397+07:00',
//   firstExecutedBy: 'Nate Nash',
//   lastExecuted: '2021-03-03T14:45:23.2498529+07:00',
//   lastExecutedBy: 'Freddy Kruger',
//   numberOfExecutions: 33,
//   isFavorite: true,
//   accesible: true
// };

export const WorkflowTemplateData = [
  {
    id: '03208548',
    name: 'Modify Incentive Pricing',
    description: 'vero et exerci labore consetetur esse',
    type: 'Security',
    firstExecuted: '2021-03-01T14:45:23.2498397+07:00',
    firstExecutedBy: 'Nate Nash',
    lastExecuted: '2021-03-03T14:45:23.2498529+07:00',
    lastExecutedBy: 'Freddy Kruger',
    numberOfExecutions: 33,
    isFavorite: true,
    accesible: true,
    filterValue: false
  }
];

export const WorkflowTemplateData1 = [
  {
    id: 'Security12',
    name: 'Modify Incentive Pricing',
    description: 'vero et exerci labore consetetur esse',
    type: 'Security1',
    firstExecuted: '2021-03-01T14:45:23.2498397+07:00',
    firstExecutedBy: 'Nate Nash',
    lastExecuted: '2021-03-03T14:45:23.2498529+07:00',
    lastExecutedBy: 'Freddy Kruger',
    numberOfExecutions: 33,
    isFavorite: true,
    accesible: true,
    filterValue: false
  }
];

export const WorkflowAttachmentData = {
  id: '21722129',
  category: 'transaction.qualification.table',
  filename: 'Employment Agreement.pdf',
  mimeType: 'application/pdf',
  accessible: true,
  fileSize: 247500,
  recordCount: 6,
  uploadedDate: '2020-12-24T00:00:00',
  uploadedBy: 'Jonah Hill'
};

export const WorkflowMapping = {
  Workflow: {
    state: 'state',
    actions: 'actions',
    isCancel: 'isCancel',
    id: 'id',
    name: 'workflowTemplate.name',
    description: 'workflowTemplate.description',
    workflowNote: 'notes',
    updatedDate: 'lastUpdated',
    updatedBy: 'lastUpdatedBy',
    workflowType: 'workflowTemplate.type',
    status: 'status',
    firstRan: 'workflowTemplate.firstExecuted',
    firstRanBy: 'workflowTemplate.firstExecutedBy',
    validationMessages: 'validationMessages',
    lastExecuted: 'workflowTemplate.lastExecuted',
    isFavorite: 'workflowTemplate.isFavorite',
    countOfAttachments: 'countOfAttachments'
  },
  WorkflowDetail: {
    id: 'id',
    actions: 'actions',
    name: 'workflowTemplate.name',
    description: 'workflowTemplate.description',
    updatedDate: 'lastUpdated',
    updatedBy: 'lastUpdatedBy',
    workflowType: 'workflowTemplate.type',
    status: 'status',
    firstRan: 'workflowTemplate.firstExecuted',
    firstRanBy: 'workflowTemplate.firstExecutedBy',
    workflowNote: 'notes',
    validationMessages: 'validationMessages',
    lastExecuted: 'workflowTemplate.lastExecuted',
    isFavorite: 'workflowTemplate.isFavorite',
    reports: 'reports',
    uploadedAttachment: 'attachments'
  },
  WorkflowAttachment: {
    id: 'id',
    category: 'category',
    filename: 'filename',
    mimeType: 'mimeType',
    accessible: 'accessible',
    fileSize: 'fileSize',
    recordCount: 'recordCount',
    uploadedDate: 'uploadedDate',
    uploadedBy: 'uploadedBy'
  },
  WorkflowTemplate: {
    id: 'id',
    name: 'name',
    description: 'description',
    type: 'type',
    firstExecuted: 'firstExecuted',
    firstExecutedBy: 'firstExecutedBy',
    lastExecuted: 'lastExecuted',
    lastExecutedBy: 'lastExecutedBy',
    isFavorite: 'isFavorite',
    numberOfExecutions: 'numberOfExecutions',
    accesible: 'accesible',
    filterValue: 'filterValue'
  }
};

export const MockWorkflowTemplate: IWorkflowTemplateModel = {
  accesible: true,
  description: 'sed amet dolores nisl aliquyam clita justo',
  id: '1943256820',
  isFavorite: true,
  lastExecuted: '2021-09-01T11:35:08.1865144+07:00',
  name: 'Manage Penalty Fees Workflow',
  numberOfExcutions: 50,
  type: 'pricing',
  iconName: 'fee-penalty'
};

export const MockWorkflowSetupSteps: WorkflowSetupStep[] = [
  {
    id: 'gettingStart',
    title: 'Get Started'
  },
  {
    id: 'changeInformation',
    title: 'Select a Change'
  },
  {
    id: 'returnedCheckCharges',
    title: 'Manage Penalty Fee Methods - Returned Check Charges'
  },
  {
    id: 'lateCharges',
    title: 'Manage Penalty Fee Methods - Late Charges'
  },
  {
    id: 'summaryInformation',
    title: 'Review and Submit'
  }
];
