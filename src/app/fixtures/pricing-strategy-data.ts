export const PricingStrategyData = [
  {
    id: 1385954068,
    name: 'KYI2',
    modeledFrom: null,
    description: 'sit kasd',
    methods: null
  },
  {
    id: 1618194724,
    name: 'KTI2',
    modeledFrom: 'KJC',
    description: 'dolores magna sanctus accusam elitr',
    methods: [
      {
        method: 'OPEPZU',
        previousValue: 'KLI3',
        newValue: 'KYI'
      },
      {
        method: 'ZVKUGZ',
        previousValue: 'KTI6',
        newValue: 'KTI'
      }
    ]
  },
  {
    id: 1618194724,
    name: 'KTI2',
    modeledFrom: 'KJC',
    description: 'dolores magna sanctus accusam elitr',
    methods: [
      {
        method: 'OPEPZU',
        previousValue: 'KLI3',
        newValue: 'KYI'
      },
      {
        method: 'ZVKUGZ',
        previousValue: 'KTI6',
        newValue: 'KTI'
      }
    ],
    impactingMethods: ['1']
  }
];

export const PricingStrategyMapping = {
  id: 'id',
  name: 'name',
  modeledFrom: 'modeledFrom',
  methods: 'methods',
  description: 'description',
  impactingMethods: 'impactingMethods'
};
