export const PortfolioData = [
  {
    id: '31823556',
    name: 'Omaha Bank Point Visa',
    icon: '',
    portfolioCriteria: [
      {
        fieldName: 'portfolioId',
        values: ['444743']
      },
      {
        fieldName: 'clientId',
        values: ['72449']
      },
      {
        fieldName: 'productType',
        values: ['Black']
      }
    ],
    portfolioStats: {
      delinquentAccounts: 7331,
      availableAccounts: 70232,
      activeAccounts: 4805,
      numberOfPricingStrategiesCount: 9866,
      newAccounts: 0.112742759339857,
      currency: 123.015299482744,
      availableCredit: 867070261.07786,
      netIncome: 523502604.940389,
      payments: 566654228.684745
    }
  },
  {
    id: '29455161',
    name: 'Omaha Bank Master Card',
    icon: 'abc',
    portfolioCriteria: [
      {
        fieldName: 'portfolioId',
        values: ['357925']
      },
      {
        fieldName: 'clientId',
        values: ['11478']
      },
      {
        fieldName: 'spa',
        values: ['0020/0200/0195']
      },
      {
        fieldName: 'productType',
        values: ['Black']
      }
    ],
    portfolioStats: {
      delinquentAccounts: 5650,
      availableAccounts: 88715,
      activeAccounts: 9456,
      numberOfPricingStrategiesCount: 5292,
      newAccounts: 0.736552050680179,
      currency: 997.673044545424,
      availableCredit: 49096512.1895208,
      netIncome: 150664456.229042,
      payments: 279214228.110584
    }
  }
];

export const PortfolioMapping = {
  id: 'id',
  portfolioName: 'name',
  icon: 'icon',
  portfolioId: {
    type: 'dynamic',
    containerProp: 'portfolioCriteria',
    fieldProp: 'portfolioId',
    mappingToType: 'string',
    key: 'fieldName',
    value: 'values'
  },
  clientId: {
    type: 'dynamic',
    containerProp: 'portfolioCriteria',
    fieldProp: 'clientId',
    mappingToType: 'string',
    key: 'fieldName',
    value: 'values'
  },
  spa: {
    type: 'dynamic',
    containerProp: 'portfolioCriteria',
    fieldProp: 'spa',
    mappingToType: 'array',
    key: 'fieldName',
    value: 'values'
  }
};
