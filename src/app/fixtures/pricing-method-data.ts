export const PricingMethodData = [
  {
    id: 1969580912,
    name: 'MTI4',
    modeledFrom: 'null',
    methodType: 'HCMOBA',
    parameters: null,
    strategiesImpacted: [
      {
        id: '123',
        name: 'abc',
        description: 'something'
      }
    ]
  },
  {
    id: 1969580988,
    name: 'MTI9',
    modeledFrom: 'null',
    methodType: 'HCMOBA',
    parameters: null,
    strategiesImpacted: [
      {
        id: '123',
        name: 'abc',
        description: 'something'
      }
    ]
  },
  {
    id: 1622679248,
    name: 'MTIC',
    modeledFrom: 'KJC',
    methodType: 'YUJZEW',
    parameters: [
      {
        name: 'incentive.pricing.override',
        previousValue: '8',
        newValue: '7'
      },
      {
        name: 'rate.override',
        previousValue: '3',
        newValue: '7'
      },
      {
        name: 'apply.time',
        previousValue: '3',
        newValue: '3'
      }
    ]
  }
];

export const PricingMethodMapping = {
  id: 'id',
  name: 'name',
  versionCreating: 'versionCreating',
  modeledFrom: 'modeledFrom.name',
  methodType: 'serviceSubjectSection',
  parametersUpdated: 'parametersUpdated',
  parameters: 'parameters',
  strategiesImpacted: 'modeledFrom.pricingStrategies'
};
