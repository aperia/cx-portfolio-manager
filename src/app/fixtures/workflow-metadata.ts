import { ConfigTableOptionsEnum } from 'app/constants/enums';

export const WorkflowMetadataOverride = {
  id: '1',
  workflowTemplateId: '123',
  name: 'temporary.credit.limit.override',
  options: [
    {
      value: '0',
      description: 'No'
    },
    {
      value: '1',
      description: 'Yes'
    }
  ]
};

export const WorkflowMetadata = {
  id: '1',
  workflowTemplateId: '123',
  name: 'minimum.pay.due.option',
  options: [
    {
      value: '0',
      description: 'Do not use reasonable amount fee processing'
    },
    {
      value: '1',
      description: 'Use the current MPD amount'
    }
  ]
};

export const WorkflowMetadataList = [
  {
    id: '118',
    name: 'promotion.expiration.text.id',
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      },
      {
        value: 'TEXTBRAE',
        description: ''
      },
      {
        value: 'TEXTBRAS',
        description: ''
      },
      {
        value: 'TEXTBRAR',
        description: ''
      }
    ]
  },
  {
    id: '1',
    name: 'minimum.pay.due.option',
    options: [
      {
        value: '0',
        description: 'Do not use reasonable amount fee processing.'
      },
      {
        value: '1',
        description: 'Use the current MPD amount.'
      },
      {
        value: '2',
        description: 'Use the current non-delinquent portion of the MPD amount.'
      },
      {
        value: '3',
        description: 'Use the historical last statement MPD amount.'
      },
      {
        value: '4',
        description:
          'Use the historical non-delinquent last statement MPD amount.'
      },
      {
        value: '5',
        description:
          'Use the lesser of the current or the historical last statement MPD amount.'
      }
    ]
  },
  {
    id: '2',
    name: 'returned.check.charge.option',
    options: [
      {
        value: '0',
        description:
          'No, do not charge for an insufficient fund check. Do not generate an automatic letter.'
      },
      {
        value: '2',
        description:
          'Yes, charge for an insufficient fund check.  The charge is a fixed amount. Generate an automatic letter.'
      }
    ]
  },
  {
    id: '3',
    name: 'one.fee.participation.code',
    options: [
      {
        value: '0',
        description: 'Do not use one fee processing.'
      },
      {
        value: '1',
        description:
          'Assess the first eligible penalty fee generated on the account.'
      }
    ]
  },
  {
    id: '4',
    name: 'reversal.text.id',
    options: [
      {
        value: '0',
        description: 'TXTRP002',
        selected: 'true'
      },
      {
        value: '1',
        description: 'RR0005'
      },
      {
        value: '2',
        description: 'RR0006'
      },
      {
        value: '3',
        description: 'RR0007'
      },
      {
        value: '4',
        description: 'RR0008'
      },
      {
        value: '5',
        description: 'RR0009'
      }
    ]
  },
  {
    id: '5',
    name: '1st.yr.max.management',
    options: [
      {
        value: '0',
        description: 'Exclude from First Year Maximum Fee.'
      },
      {
        value: '1',
        description:
          'Include the fee in first year maximum fee management, but do not allow fees that exceed the first year maximum fee allowance amount to post.'
      },
      {
        value: '2',
        description:
          'Include the fee in first year maximum fee management, but reduce fees that exceed the first year maximum fee allowance amount to the amount remaining in the allowance and then post.'
      }
    ]
  },
  {
    id: '6',
    name: 'one.fee.priority.code',
    options: [
      {
        value: '0',
        description: 'Do not use this feature.'
      },
      {
        value: '1',
        description:
          'Prioritize late and overlimit charges over returned check charges.'
      }
    ]
  },
  {
    id: '7',
    name: 'posting.text.id',
    options: [
      {
        value: '0',
        description: 'RR0003'
      },
      {
        value: '1',
        description: 'TXTRP005',
        selected: 'true'
      },
      {
        value: '2',
        description: 'RR0006'
      },
      {
        value: '3',
        description: 'RR0007'
      },
      {
        value: '4',
        description: 'RR0008'
      },
      {
        value: '5',
        description: 'RR0009'
      }
    ]
  },
  {
    id: '8',
    name: 'late.charges.option',
    options: [
      {
        value: '0',
        description: 'Do not assess late charges.'
      },
      {
        value: 'A',
        description: 'Assess late charges as a fixed amount.'
      },
      {
        value: 'G',
        description:
          'Assess late charges as either a percentage or a fixed amount, whichever is greater. This amount cannot be greater than that in the Maximum Yearly Amount parameter in this section.'
      },
      {
        value: 'P',
        description:
          'Assess late charges as a percentage. This amount cannot be greater than that in the Maximum Yearly Amount parameter. This amount is then checked to make sure it falls within the maximum and minimum late charge amounts set in the Maximum Amount parameter and the Minimum Amount parameter in this section. If it is less than the minimum, the minimum amount is assessed. If it is greater than the maximum, the maximum amount is assessed.'
      },
      {
        value: 'T',
        description:
          "Use tiered late charges based on the account's balance. You must set the Tiered Late Fees parameters in this section before using this option."
      }
    ]
  },
  {
    id: '9',
    name: 'calculation.base',
    options: [
      {
        value: '0',
        description: 'Use total one-cycle delinquent principal and interest.'
      },
      {
        value: '2',
        description: 'Use total one-cycle delinquent principal.'
      },
      {
        value: '4',
        description: 'Use total delinquent principal and interest.'
      },
      {
        value: '5',
        description: 'Use cycle-to-date minimum payment due.'
      },
      {
        value: '6',
        description: 'Use current balance.'
      },
      {
        value: '7',
        description: 'Use last statement balance.'
      }
    ]
  },
  {
    id: '10',
    name: 'assessment.control',
    options: [
      {
        value: '0',
        description:
          '"Continuous Delinquency" - The account must be continuously delinquent the number of cycles indicated in the Cycles of Consecutive Delinquency parameter in this section before the System assesses a late charge. For example, if you set this parameter to zero and set the Cycles of Consecutive Delinquency parameter to 3, the System does not assess the cardholder a late charge until the account cycles in a delinquent status for three consecutive months. The System assesses a late charge the third month. If you enter zero in this parameter and 1-9 in the Cycles of Consecutive Delinquency parameter, the System assesses a cardholder a late charge one time. If you enter zero in this parameter and zero in the Cycles of Consecutive Delinquency parameter, the System assesses late charges once when the cardholder becomes delinquent and once each month thereafter when the account cycles, as long as the cardholder remains delinquent.'
      },
      {
        value: '8',
        description:
          '"Every Cycle Until..." - The System assesses a late charge every month the account is delinquent, to a maximum delinquency level specified by the Cycles of Consecutive Delinquency parameter. For example, if you set this parameter to 8 and the Cycles of Consecutive Delinquency to 3, the System assesses the account a late charge every month the account is delinquent up to and including the third delinquency level. The late charges cease at the fourth delinquency level.'
      },
      {
        value: '9',
        description:
          '"Grace Period" - The account must be the number of cycles continuously delinquent set in the Cycles of Consecutive Delinquency parameter before the System assesses a late charge, and that assessment of the late charge continues each month until removal of the delinquency. If you set the Late Charge Option parameter in this section to a percent, the first late charge is based on the entire delinquent amount. The System calculates late charge in subsequent months based on the amount the account is one-cycle delinquent.'
      }
    ]
  },
  {
    id: '12',
    name: 'balance.indicator',
    options: [
      {
        value: '0',
        description:
          'Assess a late charge when the only balance on an account is an unpaid late charge.'
      },
      {
        value: '1',
        description:
          'Do not assess a late charge when the only balance on an account is an unpaid late charge.'
      }
    ]
  },
  {
    id: '13',
    name: 'assessed.accounts',
    options: [
      {
        value: '0',
        description: 'Calculate late charges without exception.'
      },
      {
        value: '2',
        description:
          "Assess late charges only if the customer's cycle-to-date payments have not reduced the minimum payment due to an amount less than that in the Minimum Delq Amount parameter in the Delinquency Settings section in the DO-DC-DS Method for this Pricing Strategy."
      },
      {
        value: '3',
        description:
          'Do not assess late charges if the balance is less than that in the Exclusion Balance parameter in this section (in this Method).'
      },
      {
        value: '6',
        description:
          "Assess late charges only if the payment is not sufficient to cover the nondelinquent minimum payment due. Do not assess late charges if the payment is sufficient to cover the nondelinquent minimum payment due. The System calculates the nondelinquent minimum payment due by subtracting the delinquent amount on the statement before last from the minimum payment due on the statement before last. The System calculates the payment amount by subtracting the last statement's delinquent amount from the minimum payment due on the statement before last."
      },
      {
        value: '7',
        description:
          "Do not assess late charges if the account meets one of the following conditions: The customer's cycle-to-date payments have reduced the minimum payment due to an amount less than that in the Minimum Delq Amount parameter. The beginning balance is less than that in the Minimum or Fixed Amount parameter in this section. If you use this option, you must also set the Exclusion Balance and Minimum or Fixed Amount parameters to values greater than zero."
      },
      {
        value: '8',
        description:
          'Do not assess late charges if the account meets one of the following conditions: The unpaid portion of the PCF-calculated MPD is less than or equal to the amount in the Threshold Waive Amount parameter in this section. The balance is less than that in the Exclusion Balance parameter. If you use this option, you must also set the Threshold Waive Amount parameter to a value greater than zero.'
      }
    ]
  },
  {
    id: '14',
    name: 'current.balance.assessment',
    options: [
      {
        value: '0',
        description: 'Current Balance'
      },
      {
        value: '1',
        description: 'Last Statement Balance'
      }
    ]
  },
  {
    id: '15',
    name: 'calculation.day.control',
    options: [
      {
        value: '0',
        description:
          'Calculate late charges based on a specified number of days after the billing cycle. If you set both this parameter and the Number of Days parameter to zero, the System will not assess late charges when the delinquent amount is less than the value you set in the Minimum Delq Amount parameter in the Delinquency Settings section.'
      },
      {
        value: '1',
        description:
          'Calculate late charges based on a specified number of days after the payment due date.'
      }
    ]
  },
  {
    id: '16',
    name: 'non.processing.late.charge',
    options: [
      {
        value: '0',
        description:
          'The assessed late fee is processed on the previous processing day.'
      },
      {
        value: '1',
        description:
          'The assessed late fee is processed on the next processing day.'
      }
    ]
  },
  {
    id: '17',
    name: 'include.exclude.control',
    options: [
      {
        value: 'I',
        description: 'Include'
      },
      {
        value: 'E',
        description: 'Exclude'
      },
      {
        value: 'Blank',
        description: 'Include all accounts (Regardless of external status).'
      }
    ]
  },
  {
    id: '18',
    name: 'status.1',
    options: [
      {
        value: 'A',
        description: 'Authorization prohibited.'
      },
      {
        value: 'B',
        description: 'Bankrupt'
      },
      {
        value: 'C',
        description: 'Closed'
      },
      {
        value: 'E',
        description: 'Revoked'
      },
      {
        value: 'F',
        description: 'Frozen'
      },
      {
        value: 'I',
        description: 'Interest accrual prohibited.'
      },
      {
        value: 'L',
        description: 'Lost'
      },
      {
        value: 'U',
        description: 'Stolen'
      },
      {
        value: 'Z',
        description: 'Charged off'
      },
      {
        value: '*',
        description: 'Unused parameters'
      },
      {
        value: 'Blank',
        description: 'Normal (blank external status).'
      }
    ]
  },
  {
    id: '19',
    name: 'late.charge.reset.counter',
    options: [
      {
        value: '0',
        description:
          "Reset the count of consecutive late fees to zero anytime that the delinquent portion of an account's minimum payment due is paid to an amount of zero."
      },
      {
        value: '1',
        description:
          'Reset the count of consecutive late fees to zero at statement cycle time for an account that is either non-delinquent at cycle time or had no late fee charged during the current billing period. This option prevents the System from assessing an additional consecutive late fee on an account with activity that cured a delinquency but became delinquent again within the same billing cycle.'
      }
    ]
  },
  {
    id: '20',
    name: 'reversal.details.text.id',
    options: [
      {
        value: 'DEFFR001',
        description: 'DEFFR001'
      },
      {
        value: 'RR0003',
        description: 'RR0003'
      }
    ]
  },
  {
    id: '21',
    name: 'waived.message.text.id',
    options: [
      {
        value: 'DEFFR001',
        description: 'DEFFR001'
      },
      {
        value: 'RR0003',
        description: 'RR0003'
      }
    ]
  },
  {
    id: '22',
    name: 'late.payment.warning.message',
    options: [
      {
        value: 'TEXTDFLW',
        description: 'TEXTDFLW'
      },
      {
        value: 'RR0003',
        description: 'RR0003'
      }
    ]
  },
  {
    id: '23',
    name: 'status.2',
    options: [
      {
        value: 'A',
        description: 'Authorization prohibited.'
      },
      {
        value: 'B',
        description: 'Bankrupt.'
      },
      {
        value: 'C',
        description: 'Closed.'
      },
      {
        value: 'E',
        description: 'Revoked.'
      },
      {
        value: 'F',
        description: 'Frozen.'
      },
      {
        value: 'I',
        description: 'Interest accrual prohibited.'
      },
      {
        value: 'L',
        description: 'Lost.'
      },
      {
        value: 'U',
        description: 'Stolen'
      },
      {
        value: 'Z',
        description: 'Charged off'
      },
      {
        value: '*',
        description: 'Unused parameters'
      },
      {
        value: 'Blank',
        description: 'Normal (blank external status).'
      }
    ]
  },
  {
    id: '24',
    name: 'status.3',
    options: [
      {
        value: 'A',
        description: 'Authorization prohibited'
      },
      {
        value: 'B',
        description: 'Bankrupt'
      },
      {
        value: 'C',
        description: 'Closed'
      },
      {
        value: 'E',
        description: 'Revoked'
      },
      {
        value: 'F',
        description: 'Frozen'
      },
      {
        value: 'I',
        description: 'Interest accrual prohibited.'
      },
      {
        value: 'L',
        description: 'Lost'
      },
      {
        value: 'U',
        description: 'Stolen'
      },
      {
        value: 'Z',
        description: 'Charged off'
      },
      {
        value: '*',
        description: 'Unused parameters'
      },
      {
        value: 'Blank',
        description: 'Normal (blank external status).'
      }
    ]
  },
  {
    id: '25',
    name: 'status.4',
    options: [
      {
        value: 'A',
        description: 'Authorization prohibited.'
      },
      {
        value: 'B',
        description: 'Bankrupt'
      },
      {
        value: 'C',
        description: 'Closed'
      },
      {
        value: 'E',
        description: 'Revoked'
      },
      {
        value: 'F',
        description: 'Frozen'
      },
      {
        value: 'I',
        description: 'Interest accrual prohibited.'
      },
      {
        value: 'L',
        description: 'Lost'
      },
      {
        value: 'U',
        description: 'Stolen'
      },
      {
        value: 'Z',
        description: 'Charged off'
      },
      {
        value: '*',
        description: 'Unused parameters'
      },
      {
        value: 'Blank',
        description: 'Normal (blank external status).'
      }
    ]
  },
  {
    id: '26',
    name: 'status.5',
    options: [
      {
        value: 'A',
        description: 'Authorization prohibited.'
      },
      {
        value: 'B',
        description: 'Bankrupt'
      },
      {
        value: 'C',
        description: 'Closed'
      },
      {
        value: 'E',
        description: 'Revoked'
      },
      {
        value: 'F',
        description: 'Frozen'
      },
      {
        value: 'I',
        description: 'Interest accrual prohibited.'
      },
      {
        value: 'L',
        description: 'Lost'
      },
      {
        value: 'U',
        description: 'Stolen'
      },
      {
        value: 'Z',
        description: 'Charged off'
      },
      {
        value: '*',
        description: 'Unused parameters'
      },
      {
        value: 'Blank',
        description: 'Normal (blank external status).'
      }
    ]
  },
  {
    id: '27',
    name: 'letter.number',
    options: [
      {
        value: 'A9B1',
        description: 'Promise to pay',
        optionalNameAddressPermitted: 'Y',
        variable_1: 'Annual charge',
        variable_2: 'Available credit',
        variable_3: 'Behavior score',
        variable_4: 'Agent name',
        variable_5: 'Amount of dispute',
        variable_6: 'Amount of last payment'
      },
      {
        value: 'A9Z1',
        description: 'Promise to pay',
        variable_1: 'Amount of last payment',
        optionalNameAddressPermitted: 'N',
        variable_2: 'Cardholder street'
      },
      {
        value: 'B5A9',
        description: 'Promise to pay',
        variable_1: 'Amount of last payment',
        variable_2: 'Cardholder street',
        variable_3: 'cardholder IpCode',
        variable_4: 'Cardholder balance',
        variable_5: 'Employer name'
      }
    ]
  },
  {
    id: '29',
    name: 'transaction.id',
    options: [
      {
        value: 'nm.168',
        description: 'Reallocate Strategy',
        workFlow: 'apsmoa'
      },
      {
        value: 'nm.169',
        description: 'Lock Strategy',
        workFlow: 'apsmoa'
      },
      {
        value: 'nm.170',
        description: 'Unlock Strategy',
        workFlow: 'apsmoa'
      },
      {
        value: 'nm.738',
        description: 'Override Strategy Using Method Level Override',
        workFlow: 'apsmoa'
      },
      {
        value: 'pid.194',
        description: 'Standard Plastic Request',
        workFlow: 'emboss'
      },
      {
        value: 'nm.65',
        description: 'Update Account Level Mail Code',
        workFlow: 'emboss'
      },
      {
        value: 'PID200Rush',
        description: 'Rush Plastic Request',
        workFlow: 'emboss'
      },
      {
        value: 'PID200Mass',
        description: 'Mass Plastic Request',
        workFlow: 'emboss'
      },
      {
        value: 'NM180Manual',
        description: 'Perform Manual Reage',
        workFlow: 'accountDelinquency'
      },
      {
        value: 'NM180Workout',
        description: 'Perform Workout Reage',
        workFlow: 'accountDelinquency'
      },
      {
        value: 'NM182',
        description: 'Recreate Account Delinquency',
        workFlow: 'accountDelinquency'
      },
      {
        value: 'NM183',
        description:
          'Increase or Descrease Delinquency on Already Delinquent Accounts',
        workFlow: 'accountDelinquency'
      },
      {
        value: 'NM181',
        description: 'Clear Account Delinquency and Update History',
        workFlow: 'accountDelinquency'
      }
    ]
  },
  {
    id: '57',
    name: 'nm.180.subtransaction.code',
    options: [
      {
        value: '00',
        description:
          'Clear the account delinquency and retain delinquency history. Reage the account if the value in the CODE field is N. Perform a customer service delinquency adjustment but do not reage the account if the value in the CODE field is Y.'
      }
    ]
  },
  {
    id: '56',
    name: 'nm.180.customer.service.reage',
    options: [
      {
        value: 'Y',
        description: 'Yes'
      },
      {
        value: 'N',
        description: 'No',
        selected: true
      }
    ]
  },
  {
    id: '54',
    name: 'nm.183.code',
    options: [
      {
        value: '01',
        description: "Add an amount to the account's total delinquency."
      }
    ]
  },
  {
    id: '55',
    name: 'nm.181.customer.service.adjustment',
    options: [
      {
        value: 'Y',
        description: 'Yes'
      },
      {
        value: 'N',
        description: 'No',
        selected: true
      }
    ]
  },
  {
    id: '149',
    name: 'cit.priority.order',
    options: [
      {
        value: '001',
        description: '',
        selected: true
      }
    ]
  },
  {
    id: '150',
    name: 'cit.number.of.days.before.terms.effective',
    options: [
      {
        value: '45',
        description: '',
        selected: true
      }
    ]
  }
];

export const WorkflowMetadataChangeInTermWithoutCode = [
  {
    id: '149',
    name: 'cit.priority.order',
    options: [
      {
        value: '001',
        description: '',
        selected: false
      }
    ]
  },
  {
    id: '150',
    name: 'cit.number.of.days.before.terms.effective',
    options: [
      {
        value: '45',
        description: '',
        selected: false
      }
    ]
  },
  {
    id: '241',
    name: 'joining.fee.statement.description',
    options: [
      {
        value: 'DEFJF001',
        description: '',
        selected: true
      },
      {
        value: 'DEFJF002',
        description: ''
      },
      {
        value: 'DEFJF003',
        description: ''
      },
      {
        value: 'DEFJF004',
        description: ''
      }
    ]
  }
];

export const WorkflowMetadataAll = [
  {
    id: '5126',
    name: 'allocation.before.after.cycle',
    options: [
      {
        value: 'AQ',
        description: 'Account Qualification'
      },
      {
        value: 'CA',
        description: 'Client Allocation'
      }
    ]
  },
  {
    id: '5127',
    name: 'change.code',
    options: [
      {
        value: 'AQ',
        description: 'Account Qualification'
      },
      {
        value: 'CA',
        description: 'Client Allocation'
      }
    ]
  },
  {
    id: '5128',
    name: 'service.subject.section.areas',
    options: [
      {
        value: 'AQ',
        description: 'Account Qualification'
      },
      {
        value: 'CA',
        description: 'Client Allocation'
      }
    ]
  },
  {
    id: '5126',
    name: 'change.in.terms.method.flag',
    options: [
      {
        value: 'AQ',
        description: 'Account Qualification'
      },
      {
        value: 'CA',
        description: 'Client Allocation'
      }
    ]
  },
  {
    id: '5126',
    name: 'ca.allocation.flag',
    options: [
      {
        value: 'AQ',
        description: 'Account Qualification'
      },
      {
        value: 'CA',
        description: 'Client Allocation'
      }
    ]
  },
  {
    id: '5124',
    name: 'aq.allocation.flag',
    options: [
      {
        value: 'AQ',
        description: 'Account Qualification'
      },
      {
        value: 'CA',
        description: 'Client Allocation'
      }
    ]
  },
  {
    id: '5123',
    name: 'text.area',
    options: [
      {
        value: 'AQ',
        description: 'Account Qualification'
      },
      {
        value: 'CA',
        description: 'Client Allocation'
      }
    ]
  },
  {
    id: '5125',
    name: 'table.area',
    options: [
      {
        value: 'AQ',
        description: 'Account Qualification'
      },
      {
        value: 'CA',
        description: 'Client Allocation'
      }
    ]
  },
  {
    id: '331',
    name: 'alp.table',
    options: [
      {
        value: 'AQ',
        description: 'Account Qualification'
      },
      {
        value: 'CA',
        description: 'Client Allocation'
      }
    ]
  },
  {
    id: '1',
    name: 'minimum.pay.due.option',
    options: [
      {
        value: '0',
        description: 'Do not use reasonable amount fee processing.'
      },
      {
        value: '1',
        description: 'Use the current MPD amount.'
      },
      {
        value: '2',
        description: 'Use the current non-delinquent portion of the MPD amount.'
      },
      {
        value: '3',
        description: 'Use the historical last statement MPD amount.'
      },
      {
        value: '4',
        description:
          'Use the historical non-delinquent last statement MPD amount.'
      },
      {
        value: '5',
        description:
          'Use the lesser of the current or the historical last statement MPD amount.'
      }
    ]
  },
  {
    id: '2',
    name: 'returned.check.charge.option',
    options: [
      {
        value: '0',
        description:
          'No, do not charge for an insufficient fund check. Do not generate an automatic letter.'
      },
      {
        value: '2',
        description:
          'Yes, charge for an insufficient fund check.  The charge is a fixed amount. Generate an automatic letter.'
      }
    ]
  },
  {
    id: '3',
    name: 'one.fee.participation.code',
    options: [
      {
        value: '0',
        description: 'Do not use one fee processing.'
      },
      {
        value: '1',
        description:
          'Assess the first eligible penalty fee generated on the account.'
      }
    ]
  },
  {
    id: '4',
    name: 'reversal.text.id',
    options: [
      {
        value: '0',
        description: 'TXTRP002',
        selected: 'true'
      },
      {
        value: '1',
        description: 'RR0005'
      },
      {
        value: '2',
        description: 'RR0006'
      },
      {
        value: '3',
        description: 'RR0007'
      },
      {
        value: '4',
        description: 'RR0008'
      },
      {
        value: '5',
        description: 'RR0009'
      }
    ]
  },
  {
    id: '5',
    name: '1st.yr.max.management',
    options: [
      {
        value: '0',
        description: 'Exclude from First Year Maximum Fee.'
      },
      {
        value: '1',
        description:
          'Include the fee in first year maximum fee management, but do not allow fees that exceed the first year maximum fee allowance amount to post.'
      },
      {
        value: '2',
        description:
          'Include the fee in first year maximum fee management, but reduce fees that exceed the first year maximum fee allowance amount to the amount remaining in the allowance and then post.'
      }
    ]
  },
  {
    id: '6',
    name: 'one.fee.priority.code',
    options: [
      {
        value: '0',
        description: 'Do not use this feature.'
      },
      {
        value: '1',
        description:
          'Prioritize late and overlimit charges over returned check charges.'
      }
    ]
  },
  {
    id: '7',
    name: 'posting.text.id',
    options: [
      {
        value: '0',
        description: 'RR0003'
      },
      {
        value: '1',
        description: 'TXTRP005',
        selected: 'true'
      },
      {
        value: '2',
        description: 'RR0006'
      },
      {
        value: '3',
        description: 'RR0007'
      },
      {
        value: '4',
        description: 'RR0008'
      },
      {
        value: '5',
        description: 'RR0009'
      }
    ]
  },
  {
    id: '8',
    name: 'late.charges.option',
    options: [
      {
        value: '0',
        description: 'Do not assess late charges.'
      },
      {
        value: 'A',
        description: 'Assess late charges as a fixed amount.'
      },
      {
        value: 'G',
        description:
          'Assess late charges as either a percentage or a fixed amount, whichever is greater. This amount cannot be greater than that in the Maximum Yearly Amount parameter in this section.'
      },
      {
        value: 'P',
        description:
          'Assess late charges as a percentage. This amount cannot be greater than that in the Maximum Yearly Amount parameter. This amount is then checked to make sure it falls within the maximum and minimum late charge amounts set in the Maximum Amount parameter and the Minimum Amount parameter in this section. If it is less than the minimum, the minimum amount is assessed. If it is greater than the maximum, the maximum amount is assessed.'
      },
      {
        value: 'T',
        description:
          "Use tiered late charges based on the account's balance. You must set the Tiered Late Fees parameters in this section before using this option."
      }
    ]
  },
  {
    id: '9',
    name: 'calculation.base',
    options: [
      {
        value: '0',
        description: 'Use total one-cycle delinquent principal and interest.'
      },
      {
        value: '2',
        description: 'Use total one-cycle delinquent principal.'
      },
      {
        value: '4',
        description: 'Use total delinquent principal and interest.'
      },
      {
        value: '5',
        description: 'Use cycle-to-date minimum payment due.'
      },
      {
        value: '6',
        description: 'Use current balance.'
      },
      {
        value: '7',
        description: 'Use last statement balance.'
      }
    ]
  },
  {
    id: '10',
    name: 'assessment.control',
    options: [
      {
        value: '0',
        description:
          '"Continuous Delinquency" - The account must be continuously delinquent the number of cycles indicated in the Cycles of Consecutive Delinquency parameter in this section before the System assesses a late charge. For example, if you set this parameter to zero and set the Cycles of Consecutive Delinquency parameter to 3, the System does not assess the cardholder a late charge until the account cycles in a delinquent status for three consecutive months. The System assesses a late charge the third month. If you enter zero in this parameter and 1-9 in the Cycles of Consecutive Delinquency parameter, the System assesses a cardholder a late charge one time. If you enter zero in this parameter and zero in the Cycles of Consecutive Delinquency parameter, the System assesses late charges once when the cardholder becomes delinquent and once each month thereafter when the account cycles, as long as the cardholder remains delinquent.'
      },
      {
        value: '8',
        description:
          '"Every Cycle Until..." - The System assesses a late charge every month the account is delinquent, to a maximum delinquency level specified by the Cycles of Consecutive Delinquency parameter. For example, if you set this parameter to 8 and the Cycles of Consecutive Delinquency to 3, the System assesses the account a late charge every month the account is delinquent up to and including the third delinquency level. The late charges cease at the fourth delinquency level.'
      },
      {
        value: '9',
        description:
          '"Grace Period" - The account must be the number of cycles continuously delinquent set in the Cycles of Consecutive Delinquency parameter before the System assesses a late charge, and that assessment of the late charge continues each month until removal of the delinquency. If you set the Late Charge Option parameter in this section to a percent, the first late charge is based on the entire delinquent amount. The System calculates late charge in subsequent months based on the amount the account is one-cycle delinquent.'
      }
    ]
  },
  {
    id: '12',
    name: 'balance.indicator',
    options: [
      {
        value: '0',
        description:
          'Assess a late charge when the only balance on an account is an unpaid late charge.'
      },
      {
        value: '1',
        description:
          'Do not assess a late charge when the only balance on an account is an unpaid late charge.'
      }
    ]
  },
  {
    id: '13',
    name: 'assessed.accounts',
    options: [
      {
        value: '0',
        description: 'Calculate late charges without exception.'
      },
      {
        value: '2',
        description:
          "Assess late charges only if the customer's cycle-to-date payments have not reduced the minimum payment due to an amount less than that in the Minimum Delq Amount parameter in the Delinquency Settings section in the DO-DC-DS Method for this Pricing Strategy."
      },
      {
        value: '3',
        description:
          'Do not assess late charges if the balance is less than that in the Exclusion Balance parameter in this section (in this Method).'
      },
      {
        value: '6',
        description:
          "Assess late charges only if the payment is not sufficient to cover the nondelinquent minimum payment due. Do not assess late charges if the payment is sufficient to cover the nondelinquent minimum payment due. The System calculates the nondelinquent minimum payment due by subtracting the delinquent amount on the statement before last from the minimum payment due on the statement before last. The System calculates the payment amount by subtracting the last statement's delinquent amount from the minimum payment due on the statement before last."
      },
      {
        value: '7',
        description:
          "Do not assess late charges if the account meets one of the following conditions: The customer's cycle-to-date payments have reduced the minimum payment due to an amount less than that in the Minimum Delq Amount parameter. The beginning balance is less than that in the Minimum or Fixed Amount parameter in this section. If you use this option, you must also set the Exclusion Balance and Minimum or Fixed Amount parameters to values greater than zero."
      },
      {
        value: '8',
        description:
          'Do not assess late charges if the account meets one of the following conditions: The unpaid portion of the PCF-calculated MPD is less than or equal to the amount in the Threshold Waive Amount parameter in this section. The balance is less than that in the Exclusion Balance parameter. If you use this option, you must also set the Threshold Waive Amount parameter to a value greater than zero.'
      }
    ]
  },
  {
    id: '14',
    name: 'current.balance.assessment',
    options: [
      {
        value: '0',
        description: 'Current Balance'
      },
      {
        value: '1',
        description: 'Last Statement Balance'
      }
    ]
  },
  {
    id: '15',
    name: 'calculation.day.control',
    options: [
      {
        value: '0',
        description:
          'Calculate late charges based on a specified number of days after the billing cycle. If you set both this parameter and the Number of Days parameter to zero, the System will not assess late charges when the delinquent amount is less than the value you set in the Minimum Delq Amount parameter in the Delinquency Settings section.'
      },
      {
        value: '1',
        description:
          'Calculate late charges based on a specified number of days after the payment due date.'
      }
    ]
  },
  {
    id: '16',
    name: 'non.processing.late.charge',
    options: [
      {
        value: '0',
        description:
          'The assessed late fee is processed on the previous processing day.'
      },
      {
        value: '1',
        description:
          'The assessed late fee is processed on the next processing day.'
      }
    ]
  },
  {
    id: '17',
    name: 'include.exclude.control',
    options: [
      {
        value: 'I',
        description: 'Include'
      },
      {
        value: 'E',
        description: 'Exclude'
      },
      {
        value: 'Blank',
        description: 'Include all accounts (Regardless of external status).'
      }
    ]
  },
  {
    id: '18',
    name: 'status.1',
    options: [
      {
        value: 'A',
        description: 'Authorization prohibited.'
      },
      {
        value: 'B',
        description: 'Bankrupt'
      },
      {
        value: 'C',
        description: 'Closed'
      },
      {
        value: 'E',
        description: 'Revoked'
      },
      {
        value: 'F',
        description: 'Frozen'
      },
      {
        value: 'I',
        description: 'Interest accrual prohibited.'
      },
      {
        value: 'L',
        description: 'Lost'
      },
      {
        value: 'U',
        description: 'Stolen'
      },
      {
        value: 'Z',
        description: 'Charged off'
      },
      {
        value: '*',
        description: 'Unused parameters'
      },
      {
        value: 'Blank',
        description: 'Normal (blank external status).'
      }
    ]
  },
  {
    id: '19',
    name: 'late.charge.reset.counter',
    options: [
      {
        value: '0',
        description:
          "Reset the count of consecutive late fees to zero anytime that the delinquent portion of an account's minimum payment due is paid to an amount of zero."
      },
      {
        value: '1',
        description:
          'Reset the count of consecutive late fees to zero at statement cycle time for an account that is either non-delinquent at cycle time or had no late fee charged during the current billing period. This option prevents the System from assessing an additional consecutive late fee on an account with activity that cured a delinquency but became delinquent again within the same billing cycle.'
      }
    ]
  },
  {
    id: '20',
    name: 'reversal.details.text.id',
    options: [
      {
        value: 'DEFFR001',
        description: 'DEFFR001'
      },
      {
        value: 'RR0003',
        description: 'RR0003'
      }
    ]
  },
  {
    id: '21',
    name: 'waived.message.text.id',
    options: [
      {
        value: 'DEFFR001',
        description: 'DEFFR001'
      },
      {
        value: 'RR0003',
        description: 'RR0003'
      }
    ]
  },
  {
    id: '22',
    name: 'late.payment.warning.message',
    options: [
      {
        value: 'TEXTDFLW',
        description: 'TEXTDFLW'
      },
      {
        value: 'RR0003',
        description: 'RR0003'
      }
    ]
  },
  {
    id: '23',
    name: 'status.2',
    options: [
      {
        value: 'A',
        description: 'Authorization prohibited.'
      },
      {
        value: 'B',
        description: 'Bankrupt.'
      },
      {
        value: 'C',
        description: 'Closed.'
      },
      {
        value: 'E',
        description: 'Revoked.'
      },
      {
        value: 'F',
        description: 'Frozen.'
      },
      {
        value: 'I',
        description: 'Interest accrual prohibited.'
      },
      {
        value: 'L',
        description: 'Lost.'
      },
      {
        value: 'U',
        description: 'Stolen'
      },
      {
        value: 'Z',
        description: 'Charged off'
      },
      {
        value: '*',
        description: 'Unused parameters'
      },
      {
        value: 'Blank',
        description: 'Normal (blank external status).'
      }
    ]
  },
  {
    id: '24',
    name: 'status.3',
    options: [
      {
        value: 'A',
        description: 'Authorization prohibited'
      },
      {
        value: 'B',
        description: 'Bankrupt'
      },
      {
        value: 'C',
        description: 'Closed'
      },
      {
        value: 'E',
        description: 'Revoked'
      },
      {
        value: 'F',
        description: 'Frozen'
      },
      {
        value: 'I',
        description: 'Interest accrual prohibited.'
      },
      {
        value: 'L',
        description: 'Lost'
      },
      {
        value: 'U',
        description: 'Stolen'
      },
      {
        value: 'Z',
        description: 'Charged off'
      },
      {
        value: '*',
        description: 'Unused parameters'
      },
      {
        value: 'Blank',
        description: 'Normal (blank external status).'
      }
    ]
  },
  {
    id: '25',
    name: 'status.4',
    options: [
      {
        value: 'A',
        description: 'Authorization prohibited.'
      },
      {
        value: 'B',
        description: 'Bankrupt'
      },
      {
        value: 'C',
        description: 'Closed'
      },
      {
        value: 'E',
        description: 'Revoked'
      },
      {
        value: 'F',
        description: 'Frozen'
      },
      {
        value: 'I',
        description: 'Interest accrual prohibited.'
      },
      {
        value: 'L',
        description: 'Lost'
      },
      {
        value: 'U',
        description: 'Stolen'
      },
      {
        value: 'Z',
        description: 'Charged off'
      },
      {
        value: '*',
        description: 'Unused parameters'
      },
      {
        value: 'Blank',
        description: 'Normal (blank external status).'
      }
    ]
  },
  {
    id: '26',
    name: 'status.5',
    options: [
      {
        value: 'A',
        description: 'Authorization prohibited.'
      },
      {
        value: 'B',
        description: 'Bankrupt'
      },
      {
        value: 'C',
        description: 'Closed'
      },
      {
        value: 'E',
        description: 'Revoked'
      },
      {
        value: 'F',
        description: 'Frozen'
      },
      {
        value: 'I',
        description: 'Interest accrual prohibited.'
      },
      {
        value: 'L',
        description: 'Lost'
      },
      {
        value: 'U',
        description: 'Stolen'
      },
      {
        value: 'Z',
        description: 'Charged off'
      },
      {
        value: '*',
        description: 'Unused parameters'
      },
      {
        value: 'Blank',
        description: 'Normal (blank external status).'
      }
    ]
  },
  {
    id: '27',
    name: 'letter.number',
    options: [
      {
        value: 'A9B1',
        description: 'Promise to pay',
        optionalNameAddressPermitted: 'Y',
        variable_1: 'Annual charge',
        variable_2: 'Available credit',
        variable_3: 'Behavior score',
        variable_4: 'Agent name',
        variable_5: 'Amount of dispute',
        variable_6: 'Amount of last payment',
        variable_7: 'Cardholder street',
        variable_8: 'Cardholder Ip Code',
        variable_9: 'Cardholder balance',
        variable_10: 'Employer name'
      },
      {
        value: 'A9Z1',
        description: 'Promise to pay',
        variable_1: 'Amount of last payment',
        optionalNameAddressPermitted: 'N',
        variable_2: 'Cardholder street'
      },
      {
        value: 'B5A9',
        description: 'Promise to pay',
        variable_1: 'Amount of last payment',
        variable_2: 'Cardholder street',
        variable_3: 'Cardholder Ip Code',
        variable_4: 'Cardholder balance',
        variable_5: 'Employer name'
      }
    ]
  },
  {
    id: '28',
    name: 'temporary.credit.limit.override',
    options: [
      {
        value: '1',
        description:
          "Yes, override the system's rejection of the temporary credit limit.",
        selected: true
      },
      {
        value: '0',
        description:
          "No, do not override the system's rejection of the temporary credit limit."
      }
    ]
  },
  {
    id: '29',
    name: 'override.configuration',
    options: [
      {
        name: 'Reallocate Strategy',
        value: 'nm.168',
        description:
          'NM*168 Strategy Reallocation - Use to change the grandfather type, pricing portfolio, and current pricing strategy for an account'
      },
      {
        name: 'Lock Strategy',
        value: 'nm.169',
        description: ''
      },
      {
        name: 'Unlock Strategy',
        value: 'nm.170',
        description: ''
      },
      {
        name: 'Override Strategy Using Method Level Override',
        value: 'nm.738',
        description: ''
      }
    ]
  },
  {
    id: '30',
    name: 'reallocate.strategy.entry.format',
    options: [
      {
        name: 'Cardholder account qualification table (CA)',
        value: 'CA',
        description: ''
      },
      {
        name: 'Cardholder pricing (CP)',
        value: 'CP',
        description: ''
      }
    ]
  },
  {
    id: '31',
    name: 'portfolio.id',
    options: [
      {
        name: '',
        value: '1234',
        description: ''
      },
      {
        name: '',
        value: '9864',
        description: ''
      },
      {
        name: '',
        value: '7658',
        description: ''
      }
    ]
  },
  {
    id: '32',
    name: 'strategy.id',
    options: [
      {
        name: '',
        value: '1234',
        description: ''
      },
      {
        name: '',
        value: '9864',
        description: ''
      },
      {
        name: '',
        value: '7658',
        description: ''
      }
    ]
  },
  {
    id: '32',
    name: 'strategy.id',
    options: [
      {
        name: '',
        value: '1234',
        description: ''
      },
      {
        name: '',
        value: '9864',
        description: ''
      },
      {
        name: '',
        value: '7658',
        description: ''
      }
    ]
  },
  {
    id: '33',
    name: 'status',
    options: [
      {
        name: '',
        value: 'L',
        description: 'Yes, lock the strategy onto the account.'
      },
      {
        name: '',
        value: 'Blank',
        description:
          'No, do not lock the strategy onto the account; the system assumes this is a U.'
      }
    ]
  },
  {
    id: '34',
    name: 'type',
    options: [
      {
        name: '',
        value: '0',
        description:
          'Process cash advance and merchandise principals using the new Product Control Fil'
      },
      {
        name: '',
        value: '1',
        description:
          'Cash advance balance. Process existing cash advance principals using the current'
      },
      {
        name: '',
        value: '2',
        description:
          'Merchandise balance. Process existing merchandise principals using the current'
      },
      {
        name: '',
        value: '3',
        description:
          'Cash advance and merchandise balances. Process existing merchandise and cash'
      }
    ]
  },
  {
    id: '35',
    name: 'nm738.subtraction.options',
    options: [
      {
        name: 'Subtransactions 01-31',
        value: '01-31',
        description: ''
      },
      {
        name: 'Subtransaction 60',
        value: '60',
        description: ''
      }
    ]
  },
  {
    id: '36',
    name: 'subtransaction.code',
    options: [
      {
        name: 'Break point interest (CP IC BP)',
        value: '01',
        description: ''
      },
      {
        name: 'Interest rate (CP IC ID)',
        value: '02',
        description: ''
      },
      {
        name: 'Interest method (CP IC IM)',
        value: '03',
        description: ''
      },
      {
        name: 'Incentive rate (CP IC IP)',
        value: '04',
        description: ''
      },
      {
        name: 'State Controls (CP IC SC)',
        value: '05',
        description: ''
      },
      {
        name: 'Annual charge (CP IO AC)',
        value: '06',
        description: ''
      },
      {
        name: 'Cash item (CP IO CI)',
        value: '07',
        description: ''
      },
      {
        name: 'Late fee (CP PF LC)',
        value: '08',
        description: ''
      },
      {
        name: 'Overlimit fee (CP PF OC)',
        value: '09',
        description: ''
      },
      {
        name: 'Return fee (CP PF RC)',
        value: '10',
        description: ''
      },
      {
        name: 'Minimum finance (CP IC MF)',
        value: '11',
        description: ''
      },
      {
        name: 'Payoff exception (CP IC PE)',
        value: '12',
        description: ''
      },
      {
        name: 'Credit application (CP PO CA)',
        value: '13',
        description: ''
      },
      {
        name: 'Minimum payment due (CP PO MP)',
        value: '14',
        description: ''
      },
      {
        name: 'Interest on interest (CP IC II)',
        value: '15',
        description: ''
      },
      {
        name: 'Securitization (CP IC SZ)',
        value: '16',
        description: ''
      },
      {
        name: 'Incentive pricing break point (CP IC IB)',
        value: '17',
        description: ''
      },
      {
        name: 'Incentive pricing variable (CP IC IV)',
        value: '18',
        description: ''
      },
      {
        name: 'Statement production (CP IC SP)',
        value: '19',
        description: ''
      },
      {
        name: 'Variable interest (CP IC VI)',
        value: '20',
        description: ''
      },
      {
        name: 'Merchandise item charges (CP IO MI)',
        value: '21',
        description: ''
      },
      {
        name: 'Miscellaneous charges (CP IO MC)',
        value: '22',
        description: ''
      },
      {
        name: 'Statement charges (CP IO SC)',
        value: '23',
        description: ''
      },
      {
        name: 'Debit ratification (CP OC DR)',
        value: '24',
        description: ''
      },
      {
        name: 'Declined authorization fee (CP PF DA)',
        value: '25',
        description: ''
      },
      {
        name: 'Skip payment (CP PO SP)',
        value: '26',
        description: ''
      },
      {
        name: 'Statement design (CP OC SD)',
        value: '27',
        description: ''
      },
      {
        name: 'MULTRAN processing (CP OC MP)',
        value: '28',
        description: ''
      },
      {
        name: 'Rules Minimum Payment (CP PO RM)',
        value: '29',
        description: ''
      },
      {
        name: 'Method level (CP IC ML)',
        value: '30',
        description: ''
      },
      {
        name: 'Max Cap EAPR (CP IC ME)',
        value: '31',
        description: ''
      }
    ]
  },
  {
    id: '37',
    name: 'method.override.id',
    options: [
      {
        name: 'Replace the current method override on this account with the last different method override.',
        value: 'LSTDIFF',
        description: ''
      },
      {
        name: 'Remove the current method override from this account.',
        value: 'REMOVE',
        description: ''
      },
      {
        name: 'Remove the current method override from this account.',
        value: 'Blank',
        description: ''
      }
    ]
  },
  {
    id: '38',
    name: 'lock.unlock',
    options: [
      {
        name: 'Lock the current method override for this account.',
        value: 'L',
        description: ''
      },
      {
        name: 'Unlock the current method override for this account.',
        value: 'U',
        description: ''
      }
    ]
  },
  {
    id: '39',
    name: 'allocate.immediately',
    options: [
      {
        name: 'Use the account qualification and client allocation tables to review and, if applicable, allocate the account during nightly processing.',
        value: 'X',
        description: ''
      },
      {
        name: 'No immediate allocation.',
        value: 'Blank',
        description: ''
      }
    ]
  },
  {
    id: '40',
    name: 'serviceSubjectSection',
    options: [
      {
        name: 'Method ID for the method override for the Break Points section (CP IC BP) of the Product Control File.',
        value: 'CPICBP',
        description: ''
      },
      {
        name: 'Method ID for the method override for the Incentive Pricing Break Points section (CP IC IB) of the Product Control File.',
        value: 'CPICIB',
        description: ''
      },
      {
        name: 'Method ID for the method override for the Interest Defaults section (CP IC ID) of the Product Control File.',
        value: 'CPICID',
        description: ''
      }
    ]
  },
  {
    id: '41',
    name: 'lock',
    options: [
      {
        name: 'Lock the current method override on this account.',
        value: 'L',
        description: ''
      },
      {
        name: 'The current method override will be locked according to the dates set in the LOCK DATE and UNLOCK fields.',
        value: 'U',
        description: ''
      }
    ]
  },
  {
    id: '42',
    name: 'reallocate',
    options: [
      {
        name: 'Review and, if applicable, reallocate the account on the date set in the UNLOCK field.',
        value: 'I',
        description: ''
      },
      {
        name: 'Do not reallocate the account based on the date set in the UNLOCK field.',
        value: 'Blank',
        description: ''
      }
    ]
  },
  {
    id: '43',
    name: 'aq.table',
    options: [
      {
        name: '',
        value: 'AQTABLE1',
        description: ''
      },
      {
        name: '',
        value: 'AQTABLE2',
        description: ''
      },
      {
        name: '',
        value: 'AQTABLE3',
        description: ''
      }
    ]
  },
  {
    id: '44',
    name: 'lock.strategy.start.date',
    options: [
      {
        minDate: '2021-09-20T12:26:23.9616406+07:00',
        maxDate: '2021-10-25T12:26:23.9616406+07:00'
      }
    ]
  },
  {
    id: '45',
    name: 'lock.strategy.end.date',
    options: [
      {
        minDate: '2021-09-20T12:26:23.9616406+07:00',
        maxDate: '2021-10-25T12:26:23.9616406+07:00'
      }
    ]
  },
  {
    id: '46',
    name: 'unlock.strategy.end.date',
    options: [
      {
        minDate: '2021-09-20T12:26:23.9616406+07:00',
        maxDate: '2021-10-25T12:26:23.9616406+07:00'
      }
    ]
  },
  {
    id: '47',
    name: 'emboss.configuration',
    options: [
      {
        name: 'Standard Plastic Request',
        value: 'pid.194',
        description: ''
      },
      {
        name: 'Update Account Level Mail Code',
        value: 'PID195',
        description: ''
      },
      {
        name: 'Rush Plastic Request',
        value: 'PID196',
        description: ''
      },
      {
        name: 'Mass Plastic Request',
        value: 'PID197',
        description: ''
      }
    ]
  },
  {
    id: '48',
    name: 'pid.194.code',
    options: [
      {
        description:
          'Emboss plastics for all active customer presentation instrument identifiers on a nondual account, or emboss plastics for all active customer presentation instrument identifiers on the drive side of a dual account',
        value: '0',
        name: ''
      },
      {
        description:
          'Emboss plastics for the principal customer on a nondual account, or for the principal customer on the drive side of a dual account',
        value: '1',
        name: ''
      },
      {
        description:
          'Emboss plastics for the secondary customer on a nondual account, or for the secondary customer on the drive side of a dual account',
        value: '2',
        name: ''
      },
      {
        description:
          'Emboss a plastic for the name entered in the NAME field, or for the name that corresponds with the identifier entered in the SEQUENCE field for the nondual account or for the drive side of a dual account',
        value: '3',
        name: ''
      }
    ]
  },
  {
    id: '49',
    name: 'emboss.standard.parameter.active',
    options: [
      {
        name: 'IDENTIFIER',
        value: 'Required',
        description: 'Presentation instrument identifier'
      },
      {
        name: 'COUNT',
        value: 'Required',
        description: 'Count of plastics to force emboss'
      },
      {
        name: 'NAME',
        value: 'Optional',
        description: 'Name to emboss on the plastic'
      },
      {
        name: 'SEQUENCE',
        value: 'Optional',
        description:
          'Member sequence identifier representing a specific customer on the account'
      },
      {
        name: 'FEE',
        value: 'Optional',
        description:
          'Code determining whether the customer is assessed a fee for a replacement plastic'
      },
      {
        name: 'ACTIVATION',
        value: 'Optional',
        description:
          'Code determining whether the system processes the plastic using existing card activation controls'
      }
    ]
  },
  {
    id: '50',
    name: 'emboss.standard.parameter.inActive',
    options: [
      {
        name: 'COUNT',
        value: 'Required',
        description: 'Count of plastics to force emboss'
      },
      {
        name: 'NAME',
        value: 'Optional',
        description: 'Name to emboss on the plastic'
      },
      {
        name: 'FEE',
        value: 'Optional',
        description:
          'Code determining whether the customer is assessed a fee for a replacement plastic'
      },
      {
        name: 'ACTIVATION',
        value: 'Optional',
        description:
          'Code determining whether the system processes the plastic using existing card activation controls'
      }
    ]
  },
  {
    id: '51',
    name: 'action',
    options: [
      {
        name: '',
        value: 'pid.194',
        description: 'Standard Plastic Request',
        workFlow: 'emboss'
      },
      {
        name: '',
        value: 'nm.65',
        description: 'Update Account Level Mail Code',
        workFlow: 'emboss'
      },
      {
        name: '',
        value: 'pid.200.rush',
        description: 'Rush Plastic Request',
        workFlow: 'emboss'
      },
      {
        name: '',
        value: 'pid.200.mass',
        description: 'Mass Plastic Request',
        workFlow: 'emboss'
      },
      {
        value: 'NM180Manual',
        description: 'Perform Manual Reage',
        workFlow: 'accountDelinquency'
      },
      {
        value: 'NM180Workout',
        description: 'Perform Workout Reage',
        workFlow: 'accountDelinquency'
      },
      {
        value: 'NM182',
        description: 'Recreate Account Delinquency',
        workFlow: 'accountDelinquency'
      },
      {
        value: 'NM183',
        description:
          'Increase or Descrease Delinquency on Already Delinquent Accounts',
        workFlow: 'accountDelinquency'
      },
      {
        value: 'NM181',
        description: 'Clear Account Delinquency and Update History',
        workFlow: 'accountDelinquency'
      }
    ]
  },
  {
    id: '52',
    name: 'pid.194.transaction.id',
    options: [
      {
        name: '',
        value: 'nm.65',
        description: 'NM*65 Mail Code Flag and Update'
      }
    ]
  },
  {
    id: '53',
    name: 'pid.200.code',
    options: [
      {
        description:
          'Emboss plastics for all active customer presentation instrument identifiers on a nondual account, or emboss plastics for all active customer presentation instrument identifiers on the drive side of a dual account',
        value: '0',
        name: ''
      },
      {
        description:
          'Emboss plastics for the principal customer on a nondual account, or for the principal customer on the drive side of a dual account',
        value: '1',
        name: ''
      },
      {
        description:
          'Emboss plastics for the secondary customer on a nondual account, or for the secondary customer on the drive side of a dual account',
        value: '2',
        name: ''
      },
      {
        description:
          'Emboss a plastic for the name entered in the NAME field, or for the name that corresponds with the identifier entered in the SEQUENCE field for the nondual account or for the drive side of a dual account',
        value: '3',
        name: ''
      }
    ]
  },
  {
    id: '54',
    name: 'nm.183.code',
    options: [
      {
        value: '01',
        description: "Add an amount to the account's total delinquency."
      },
      {
        value: '02',
        description:
          "Add an amount only to the cash advance portion of the account's delinquency."
      },
      {
        value: '03',
        description: "Subtract an amount from the account's total delinquency."
      },
      {
        value: '04',
        description:
          "Subtract an amount only from the cash advance portion of the account's delinquency."
      }
    ]
  },
  {
    id: '55',
    name: 'nm.181.customer.service.adjustment',
    options: [
      {
        value: 'Y',
        description: 'Yes'
      },
      {
        value: 'N',
        description: 'No',
        selected: true
      }
    ]
  },
  {
    id: '56',
    name: 'nm.180.customer.service.reage',
    options: [
      {
        value: 'Y',
        description: 'Yes'
      },
      {
        value: 'N',
        description: 'No',
        selected: true
      }
    ]
  },
  {
    id: '57',
    name: 'nm.180.subtransaction.code',
    options: [
      {
        value: '00',
        description:
          'Clear the account delinquency and retain delinquency history. Reage the account if the value in the CODE field is N. Perform a customer service delinquency adjustment but do not reage the account if the value in the CODE field is Y.'
      },
      {
        value: '03',
        description:
          'Reage Last Date. Update the last reage date with the value in the DATE field. You can also enter this information using the LAST REAGE DATE field of the formatted NM CL2, Account Collection - Level 2 transaction.'
      },
      {
        value: '04',
        description:
          'Reage Previous Date. Update the previous reage date with the value in the DATE field. You can also enter this information using the PREV REAGE DATE field of the formatted NM CL2, Account Collection - Level 2 transaction.'
      },
      {
        value: '05',
        description:
          'Reage Prior Date. Update the prior reage date with the value in the DATE field. You can also enter this information using the PRIOR REAGE DATE field of the formatted NM CL2, Account Collection - Level 2transaction.'
      },
      {
        value: '06',
        description:
          'Workout Last Date. Update the last workout reage date with the value in the DATE field. You can also enter this information using the LAST WRK DATE field of the formatted NM CL2, Account Collection - Level 2 transaction.'
      },
      {
        value: '07',
        description:
          'Workout Previous Date. Update the previous workout reage date with the value in the DATE field. You can also enter this information using the PREV WRKT DATE field of the formatted NM CL2, Account Collection - Level 2 transaction.'
      },
      {
        value: '50',
        description:
          'Update the workout plan end date with the value in the DATE field, update the workout plan type with the value in the TYPE field. To put an account on a workout plan, you must enter a value in the TYPE field. If you do not enter a value in the DATE field, the System automatically sets the workout plan end date to 99999999 to create a nonexpiring workout plan. If you want to specify an end date, set the DATE field to a valid future date. If the account is already on a workout plan, you can change the workout plan type code and/or the workout plan end date. If you want to end the workout plan early, set the workout plan end date to a date greater than the current date. If you want to change from an expiring workout plan to a nonexpiring workout plan, you must manually set the DATE field to 99999999. To remove an account from a workout plan, such as when an account was placed on a workout plan in error, you must enter zeros in the DATE field. Be aware that when you remove an account from a workout plan, the System resets all workout reage fields, making it appear as if the account was never on a workout plan. When you post subtransaction 50 to change the workout plan type code, the System automatically posts subtransaction 49 to set the workout plan start date and workout plan start balance.'
      },
      {
        value: '53',
        description:
          'Update the forbearance plan identifier with the value in the PLAN field and/or the forbearance plan start and end dates with the value in the DATE field. To put an account on a forbearance plan, you must enter a value in the PLAN field. If you do not set the DATE field, the System automatically sets the plan start date to the current date.'
      },
      {
        value: '54',
        description:
          'Update the forbearance plan end date with the value in the DATE field. If you do not set the forbearance plan end date, the System sets it to all nines, designating a nonexpiring plan60 - Clear account delinquency as a correction rehab and retain history by entering the last statement balance in the AMOUNT field.'
      },
      {
        value: '61',
        description:
          'Clear account delinquency as a courtesy rehab and retain history by entering the last statement balance in the AMOUNT field.'
      },
      {
        value: '62',
        description:
          'Clear account delinquency as a correction rehab and update history by entering the last statement balance in the AMOUNT field'
      },
      {
        value: '63',
        description:
          'Clear account delinquency as a courtesy rehab and update history by entering the last statement balance in the AMOUNT field.'
      }
    ]
  },
  {
    id: '58',
    name: 'transaction.id',
    options: [
      {
        value: 'NM180Manual',
        description: 'Perform Manual Reage'
      },
      {
        value: 'NM180Workout',
        description: 'Perform Workout Reage'
      },
      {
        value: 'NM182',
        description: 'Recreate Account Delinquency'
      },
      {
        value: 'NM183',
        description:
          'Increase or Descrease Delinquency on Already Delinquent Accounts'
      },
      {
        value: 'NM181',
        description: 'Clear Account Delinquency and Update History'
      }
    ]
  },
  {
    id: '59',
    name: 'promotion.description.display.options',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description: 'Do not display the promotion description.'
      },
      {
        value: '1',
        description: 'Display the description from the Description parameter.'
      },
      {
        value: '2',
        description:
          'Display the description from the Alternate Description parameter.'
      },
      {
        value: '3',
        description:
          'Display the description from the DD, Description Display, text type in the DT, Detail Descriptions, text area of the PCF Text Maintenance feature.'
      },
      {
        value: '4',
        description:
          'Display the description from the DX, Description Extended, text type in the DT, Detail Descriptions, text area of the PCF Text Maintenance feature.'
      }
    ]
  },
  {
    id: '60',
    name: 'promotion.statement.text.id.dt.dd',
    options: [
      {
        value: 'TEXTDFAD',
        description: ''
      },
      {
        value: 'TEXTDFAJ',
        description: ''
      },
      {
        value: 'TEXTDFAP',
        description: ''
      },
      {
        value: 'TEXTDFAR',
        description: ''
      },
      {
        value: 'TEXTDFCA',
        description: ''
      },
      {
        value: 'TEXTDFCC',
        description: ''
      },
      {
        value: 'TEXTDFCF',
        description: ''
      },
      {
        value: 'TEXTDFCI',
        description: ''
      },
      {
        value: 'TEXTDFCL',
        description: ''
      },
      {
        value: 'TEXTDFCP',
        description: ''
      }
    ]
  },
  {
    id: '61',
    name: 'promotion.statement.text.id.dt.dx',
    options: [
      {
        value: 'TEXTDFAD',
        description: ''
      },
      {
        value: 'TEXTDFAJ',
        description: ''
      },
      {
        value: 'TEXTDFAP',
        description: ''
      },
      {
        value: 'TEXTDFAR',
        description: ''
      },
      {
        value: 'TEXTDFCA',
        description: ''
      },
      {
        value: 'TEXTDFCC',
        description: ''
      },
      {
        value: 'TEXTDFCF',
        description: ''
      },
      {
        value: 'TEXTDFCI',
        description: ''
      },
      {
        value: 'TEXTDFCL',
        description: ''
      },
      {
        value: 'TEXTDFCP',
        description: ''
      }
    ]
  },
  {
    id: '62',
    name: 'promotion.statement.text.id',
    options: [
      {
        value: 'TEXTDFAD',
        description: '',
        workFlow: 'designLoanOffers'
      },
      {
        value: 'TEXTDFAJ',
        description: '',
        workFlow: 'designLoanOffers'
      },
      {
        value: 'TEXTDFAP',
        description: '',
        workFlow: 'designLoanOffers'
      },
      {
        value: 'TEXTDFAR',
        description: '',
        workFlow: 'designLoanOffers'
      },
      {
        value: 'TEXTDFCA',
        description: '',
        workFlow: 'designLoanOffers'
      },
      {
        value: 'TEXTDFCC',
        description: '',
        workFlow: 'designLoanOffers'
      },
      {
        value: 'TEXTDFCF',
        description: '',
        workFlow: 'designLoanOffers'
      },
      {
        value: 'TEXTDFCI',
        description: '',
        workFlow: 'designLoanOffers'
      },
      {
        value: 'TEXTDFCL',
        description: '',
        workFlow: 'designLoanOffers'
      },
      {
        value: 'TEXTDFCP',
        description: '',
        workFlow: 'designLoanOffers'
      },
      {
        value: 'TEXTDFAD',
        description: '',
        workFlow: 'designPromotion'
      },
      {
        value: 'TEXTDFAE',
        description: '',
        workFlow: 'designPromotion'
      },
      {
        value: 'TEXTDFAS',
        description: '',
        workFlow: 'designPromotion'
      },
      {
        value: 'TEXTDFAR',
        description: '',
        workFlow: 'designPromotion'
      }
    ]
  },
  {
    id: '63',
    name: 'promotion.statement.text.control',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description: 'Do not display the promotion description.'
      },
      {
        value: '1',
        description: 'Display text on statement and service screens.'
      },
      {
        value: '3',
        description: 'Display text only on service screens.'
      }
    ]
  },
  {
    id: '64',
    name: 'promotion.return.application.option',
    options: [
      {
        value: '',
        description: 'None',
        workFlow: 'designLoanOffers'
      },
      {
        value: '0',
        description: 'Option not used.',
        workFlow: 'designLoanOffers'
      },
      {
        value: '1',
        description:
          'Apply returns to the oldest balance ID of the promotion ID assigned by your TLP decision tables.',
        workFlow: 'designLoanOffers'
      },
      {
        value: '2',
        description:
          'Apply returns to the newest balance ID of the promotion ID assigned by your TLP decision tables.',
        workFlow: 'designLoanOffers'
      },
      {
        value: '',
        description: 'None',
        workFlow: 'designPromotion'
      },
      {
        value: '0',
        description:
          'Option not used. Returns follow the credit application process you have established with your payment application tables and Product Control File settings.',
        workFlow: 'designPromotion'
      },
      {
        value: '1',
        description:
          'Apply returns to the oldest balance ID of the promotion ID assigned by your TLP decision tables.',
        workFlow: 'designPromotion'
      },
      {
        value: '2',
        description: '',
        workFlow: 'designLoanOffers'
      }
    ]
  },
  {
    id: '65',
    name: 'promotion.payoff.exception.option',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description:
          'Exclude this promotional balance from the amount required to meet payoff exception processing.'
      },
      {
        value: '1',
        description:
          'Include this promotional balance in the amount required to meet payoff exception processing.'
      },
      {
        value: '2',
        description:
          'Exclude this promotional balance from the amount required to meet payoff exception processing when the delay period end date is greater than the last statement date.'
      },
      {
        value: '3',
        description:
          'Exclude this promotional balance from the amount required to meet payoff exception processing when the delay period end date is greater than the payoff exception date.'
      }
    ]
  },
  {
    id: '66',
    name: 'payoff.exception.method.cp.ic.pe',
    options: [
      {
        value: 'THDI1031',
        description: ''
      },
      {
        value: 'THDI1032',
        description: ''
      },
      {
        value: 'MTHD2921',
        description: ''
      },
      {
        value: 'MTHD1921',
        description: ''
      }
    ]
  },
  {
    id: '67',
    name: 'cash.advance.item.fees.method.cp.io.ci',
    options: [
      {
        value: 'THDI1031',
        description: ''
      },
      {
        value: 'THDI1032',
        description: ''
      },
      {
        value: 'METHOD2',
        description: ''
      },
      {
        value: 'MTHD1921',
        description: ''
      }
    ]
  },
  {
    id: '68',
    name: 'merchant.item.fees.method.cp.io.mi',
    options: [
      {
        value: 'THDI1031',
        description: ''
      },
      {
        value: 'THDI1032',
        description: ''
      },
      {
        value: 'METHOD2',
        description: ''
      },
      {
        value: 'MTHD1921',
        description: ''
      }
    ]
  },
  {
    id: '69',
    name: 'credit.application.group',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '1',
        description: '01'
      },
      {
        value: '2',
        description: '02'
      },
      {
        value: '3',
        description: '03'
      },
      {
        value: '4',
        description: '04'
      },
      {
        value: '5',
        description: '05'
      },
      {
        value: '6',
        description: '06'
      },
      {
        value: '7',
        description: '07'
      },
      {
        value: '8',
        description: '08'
      },
      {
        value: '9',
        description: '09'
      },
      {
        value: '10',
        description: '10'
      },
      {
        value: '11'
      }
    ]
  },
  {
    id: '70',
    name: 'promotion.group.identifier',
    options: [
      {
        value: 'THDI1031',
        description: ''
      },
      {
        value: 'THDI1032',
        description: ''
      },
      {
        value: 'MTHD2921',
        description: ''
      },
      {
        value: 'MTHD1921',
        description: ''
      }
    ]
  },
  {
    id: '71',
    name: 'standard.minimum.payment.calculation.code',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: 'D',
        description:
          'Reamortize the minimum payment due based on the current balance amount of the promotion.'
      },
      {
        value: 'F',
        description:
          'Reamortize the minimum payment due for the remaining payment term.'
      },
      {
        value: 'H',
        description:
          'Reamortize the minimum payment due amount based on the high balance amount of the promotion.'
      },
      {
        value: '0',
        description: 'Do not use this parameter to calculate minimum payment.'
      },
      {
        value: '1',
        description:
          'Calculate fixed minimum payment by dividing the amount at posting by the setting in the Payout Period parameter in this section. Round up to the next whole dollar.'
      },
      {
        value: '2',
        description:
          'Calculate fixed minimum payment by multiplying the amount at posting by the setting in the Standard Minimum Payments Rate parameter in this section. Round up to the next whole dollar.'
      },
      {
        value: '3',
        description:
          'Calculate decreasing minimum payment by multiplying the outstanding principal by the setting in the Standard Minimum Payments Rate parameter at cycle time. Round according to the setting in the Rounding parameter in the Minimum Payment Due section (CP PO MP) of the Product Control File.'
      },
      {
        value: '4',
        description:
          'Calculate decreasing minimum payment by multiplying the outstanding principal and interest by the setting in the Standard Minimum Payments Rate parameter at cycle time. Round according to the setting in the Rounding parameter in the Minimum Payment Due section (CP PO MP).'
      },
      {
        value: '5',
        description:
          'Calculate fixed minimum payment by amortizing the principal and interest over the life of the promotion. Use the monthly installment as the minimum payment due.'
      },
      {
        value: '6',
        description:
          'Calculate the minimum payment due (MPD) by using current total-amount-owed (TAO), the annual interest rate (INT), and the remaining payment period (N).'
      },
      {
        value: '7',
        description: 'Reserved for future use.'
      },
      {
        value: '8',
        description:
          'Combine all promotional balances assigned to this calculation method. Calculate the fixed minimum payment due by using the charge parameter in the Fixed Minimum Payment section (CP PO FM) of the Product Control File that corresponds to the first amount field greater than the combined promotional balance for the designated method to arrive at an amount.'
      },
      {
        value: '9',
        description:
          'Calculate fixed minimum payment due (MPD) by amortizing the principal, fees, and interest over the life of the promotion. Use the monthly installment as the minimum payment due.'
      }
    ]
  },
  {
    id: '72',
    name: 'rules.minimum.payment.due.method.pl.rt.mp',
    options: [
      {
        value: 'THDI1031',
        description: ''
      },
      {
        value: 'THDI1032',
        description: ''
      },
      {
        value: 'MTHD2921',
        description: ''
      },
      {
        value: 'MTHD1921',
        description: ''
      }
    ]
  },
  {
    id: '73',
    name: 'standard.minimum.payment.rounding.options',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: 'B',
        description: 'Round the calculation to the higher penny.'
      },
      {
        value: '5',
        description: 'Round to nearest penny.'
      }
    ]
  },
  {
    id: '74',
    name: 'merchant.discount.calculation.code',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description: 'Do not use this option.'
      },
      {
        value: '1',
        description:
          'Calculate discount based on the promotional discount rate only.'
      },
      {
        value: '2',
        description:
          'Calculate discount based on adding the promotional discount rate to the merchant qualifying rate.'
      }
    ]
  },
  {
    id: '75',
    name: 'return.to.revolving.base.interest',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description: 'Do not use this option.'
      },
      {
        value: '1',
        description:
          'If the promotional balance returns to revolving, use the interest rate at the next level up, the plan rate if one exists, or the revolving rate. Combine with other balances into a single average daily balance at the plan or revolving level. Maintain the promotion on the account record for the length of time specified in the Number of Retention Months parameter in this section.'
      }
    ]
  },
  {
    id: '76',
    name: 'apply.penalty.pricing.on.loan.offer',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description: 'Do not apply penalty pricing at the promotion level.'
      },
      {
        value: '1',
        description: 'Apply penalty pricing to the promotion.'
      },
      {
        value: '2',
        description:
          'Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
      }
    ]
  },
  {
    id: '77',
    name: 'default.interest.rate.method.override',
    options: [
      {
        value: 'THDI1031',
        description: ''
      },
      {
        value: 'THDI1032',
        description: ''
      },
      {
        value: 'MTHD2921',
        description: ''
      },
      {
        value: 'MTHD1921',
        description: ''
      }
    ]
  },
  {
    id: '78',
    name: 'interest.calculation.method.override',
    options: [
      {
        value: 'THDI1031',
        description: ''
      },
      {
        value: 'THDI1032',
        description: ''
      },
      {
        value: 'MTHD2921',
        description: ''
      },
      {
        value: 'MTHD1921',
        description: ''
      }
    ]
  },
  {
    id: '79',
    name: 'introductory.message.display.control',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '1',
        description:
          'Display the introductory promotional message both in CIS and on paper statements.'
      },
      {
        value: '2',
        description:
          'Display the introductory promotional message only on paper statements.'
      },
      {
        value: '3',
        description: 'Display the introductory promotional message only in CIS.'
      }
    ]
  },
  {
    id: '80',
    name: 'positive.amortization.usage.code',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description:
          'Include the promotional balance in the positive amortization calculation.'
      },
      {
        value: '1',
        description:
          'Exclude the promotional balance from the positive amortization calculation.'
      },
      {
        value: '2',
        description:
          'Exclude the promotional balance from the positive amortization calculation during an introductory minimum payment due period.'
      },
      {
        value: '3',
        description:
          'Exclude the promotional balance from the positive amortization calculation during an introductory interest period.'
      },
      {
        value: '4',
        description:
          'Exclude the promotional balance from the positive amortization calculation during an introductory cash option period.'
      },
      {
        value: '5',
        description:
          'Exclude the promotional balance from the positive amortization calculation during an introductory interest, minimum payment due, and/or cash option period.'
      }
    ]
  },
  {
    id: '81',
    name: 'introductory.period.statement.text.id',
    options: [
      {
        value: 'THDI1031',
        description: ''
      },
      {
        value: 'THDI1032',
        description: ''
      },
      {
        value: 'MTHD2921',
        description: ''
      },
      {
        value: 'MTHD1921',
        description: ''
      }
    ]
  },
  {
    id: '82',
    name: 'accrue.interest.on.unbilled.interest',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description: 'No, do not accrue interest on unbilled interest.'
      },
      {
        value: '1',
        description: 'Yes, accrue interest on unbilled interest.'
      }
    ]
  },
  {
    id: '83',
    name: 'accrue.interest.on.unbilled.interest',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description: 'Do not display the promotion description.'
      },
      {
        value: '1',
        description:
          'Display the description from the Description parameter. If you set this parameter to 1, the information set for the Description parameter in this section will appear as the second detail line of a transaction.'
      },
      {
        value: '2',
        description:
          'Display the description from the Alternate Description parameter. If you set this parameter to 2, the information set for the Alternate Description parameter in this section will appear as the second detail line of a transaction.'
      },
      {
        value: '3',
        description:
          'Display the description from the DD, Description Display, text type in the DT, Detail Descriptions, text area of the PCF Text Maintenance feature. If you set this parameter to 3, you must type a valid DT DD text identifier in the Description From Text ID - DT/DD parameter.'
      }
    ]
  },
  {
    id: '84',
    name: 'promotion.type',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '1',
        description: 'Single-ticket promotion.'
      },
      {
        value: '5',
        description: 'Multi-ticket promotion.'
      }
    ]
  },
  {
    id: '85',
    name: 'credit.application.options',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '1',
        description:
          'Do you use PCF Service / Subject / Section CP PO CA to control application of credits to accounts?'
      },
      {
        value: '2',
        description:
          'Do you use the Credit Application Service in Rules to control application of credits to accounts?'
      }
    ]
  },
  {
    id: '86',
    name: 'credit.application.group.id',
    options: [
      {
        value: 'GRPID001',
        description: ''
      },
      {
        value: 'GRPID002',
        description: ''
      },
      {
        value: 'GRPID003',
        description: ''
      }
    ]
  },
  {
    id: '87',
    name: 'multi.ticket.criteria',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description:
          'Post all qualifying transactions from multiple days to the same promotional balance. All qualifying transactions that post within the timeframe of the promotion, based on the end date established by the related fields, will post to the same promotional balance using the end date established by the initial transaction.'
      },
      {
        value: '1',
        description:
          'Post all qualifying transactions from the same promotional period to the same promotional balance. The "Promotional Cash Option," "Promotional Interest Delay," and "Introductory Min Pay Delay" parameters govern the Promotional Period to apply the balance.'
      }
    ]
  },
  {
    id: '88',
    name: 'multi.ticket.terms.creation',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description:
          'Post the transaction to the existing protected promotional balance.'
      },
      {
        value: '1',
        description:
          'Create and post to a new multi-ticket promotional balance.'
      }
    ]
  },
  {
    id: '89',
    name: 'balance.transfer.promotion.option',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description: 'Do not define the promotion as a balance transfer.'
      },
      {
        value: '1',
        description: 'Define the promotion as a balance transfer.'
      }
    ]
  },
  {
    id: '90',
    name: 'balance.transfer.promotion.option',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description: 'Do not define the promotion as a balance transfer.'
      },
      {
        value: '1',
        description: 'Define the promotion as a balance transfer.'
      }
    ]
  },
  {
    id: '91',
    name: 'promotion.payoff.exception.options',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description:
          'Exclude this promotional balance from the amount required to meet payoff exception processing.'
      },
      {
        value: '1',
        description:
          'Include this promotional balance in the amount required to meet payoff exception processing.'
      },
      {
        value: '2',
        description:
          'Exclude this promotional balance from the amount required to meet payoff exception processing when the delay period end date is greater than the last statement date.'
      }
    ]
  },
  {
    id: '92',
    name: 'merchandise.items.fees.cp.io.mi.set.1',
    options: [
      {
        value: 'MTHMTI1',
        description: ''
      },
      {
        value: 'MTHMTI2',
        description: ''
      },
      {
        value: 'MTHMTI3',
        description: ''
      }
    ]
  },
  {
    id: '93',
    name: 'merchandise.items.fees.cp.io.mi.set.2',
    options: [
      {
        value: 'MTHMTI1',
        description: ''
      },
      {
        value: 'MTHMTI2',
        description: ''
      },
      {
        value: 'MTHMTI3',
        description: ''
      }
    ]
  },
  {
    id: '94',
    name: 'merchandise.items.fees.cp.io.mi.set.3',
    options: [
      {
        value: 'MTHMTI1',
        description: ''
      },
      {
        value: 'MTHMTI2',
        description: ''
      },
      {
        value: 'MTHMTI3',
        description: ''
      }
    ]
  },
  {
    id: '95',
    name: 'cash.advance.item.fees.cp.io.ci.set.1',
    options: [
      {
        value: 'MTHMTI1',
        description: ''
      },
      {
        value: 'MTHMTI2',
        description: ''
      },
      {
        value: 'MTHMTI3',
        description: ''
      }
    ]
  },
  {
    id: '96',
    name: 'cash.advance.item.fees.cp.io.ci.set.2',
    options: [
      {
        value: 'MTHMTI1',
        description: ''
      },
      {
        value: 'MTHMTI2',
        description: ''
      },
      {
        value: 'MTHMTI3',
        description: ''
      }
    ]
  },
  {
    id: '97',
    name: 'cash.advance.item.fees.cp.io.ci.set.3',
    options: [
      {
        value: 'MTHMTI1',
        description: ''
      },
      {
        value: 'MTHMTI2',
        description: ''
      },
      {
        value: 'MTHMTI3',
        description: ''
      }
    ]
  },
  {
    id: '98',
    name: 'promotion.method.reload',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description:
          'Load the method name when the transaction posts to the account. If you choose this option and you set the Delay Terms parameter in this section to zero, the System uses the promotion’s existing method when a transaction associated with a promotion posts to the account. The existing pricing terms will stay in effect until the promotional balance is paid in full.'
      },
      {
        value: '1',
        description:
          'Reload the method name during nightly processing. If you choose this option, when a transaction associated with a promotion posts to the account, the System uses the promotion’s existing method. If a method change occurs, the System begins to use the promotion’s new pricing terms immediately.'
      }
    ]
  },
  {
    id: '99',
    name: 'promotion.interest.override.i.e',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '1',
        description:
          'Always allow the standard interest rate to override the promotional or protected promotional interest rate.'
      },
      {
        value: '2',
        description:
          'Never allow the standard interest rate to override the promotional or protected promotional interest rate.'
      },
      {
        value: '3',
        description:
          'Prevent the standard interest rate from overriding the promotional balance rate during the introductory interest period.'
      }
    ]
  },
  {
    id: '100',
    name: 'base.interest.set.1',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description:
          'Use the promotional interest rate determined by the values you set in the Base Rate for Regular Annual Rate parameter within this Method.'
      },
      {
        value: '1',
        description:
          'Use the Plan Rate or revolving rate. You will keep separate average daily balances (ADB). This value is only valid when the Base Rate for Regular Annual Rate parameter equals zero. After the introductory interest period, if any, the unique payoff exception, interest method, variable interest, and breakpoint methods set at the promotional level will be ignored.'
      },
      {
        value: '2',
        description:
          'Use the rate at the next level up, the plan rate if one exists, or the revolving rate. Same as 1, but you will keep single average daily balances (ADB). This value is only valid when the Base Rate for Regular Annual Rate parameter equals zero. With this option, the normal cardholder account strategy pricing terms will take effect after the introductory period, if any has been established. Information will be included with the standard ADB. After the introductory interest period, if any, the unique payoff exception, interest method, variable interest, and breakpoint methods set at the promotional level will be ignored.'
      }
    ]
  },
  {
    id: '101',
    name: 'base.interest.set.2',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description:
          'Use the promotional interest rate determined by the values you set in the Base Rate for Regular Annual Rate parameter within this Method.'
      },
      {
        value: '1',
        description:
          'Use the Plan Rate or revolving rate. You will keep separate average daily balances (ADB). This value is only valid when the Base Rate for Regular Annual Rate parameter equals zero. After the introductory interest period, if any, the unique payoff exception, interest method, variable interest, and breakpoint methods set at the promotional level will be ignored.'
      },
      {
        value: '2',
        description:
          'Use the rate at the next level up, the plan rate if one exists, or the revolving rate. Same as 1, but you will keep single average daily balances (ADB). This value is only valid when the Base Rate for Regular Annual Rate parameter equals zero. With this option, the normal cardholder account strategy pricing terms will take effect after the introductory period, if any has been established. Information will be included with the standard ADB. After the introductory interest period, if any, the unique payoff exception, interest method, variable interest, and breakpoint methods set at the promotional level will be ignored.'
      }
    ]
  },
  {
    id: '102',
    name: 'base.interest.set.3',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description:
          'Use the promotional interest rate determined by the values you set in the Base Rate for Regular Annual Rate parameter within this Method.'
      },
      {
        value: '1',
        description:
          'Use the Plan Rate or revolving rate. You will keep separate average daily balances (ADB). This value is only valid when the Base Rate for Regular Annual Rate parameter equals zero. After the introductory interest period, if any, the unique payoff exception, interest method, variable interest, and breakpoint methods set at the promotional level will be ignored.'
      },
      {
        value: '2',
        description:
          'Use the rate at the next level up, the plan rate if one exists, or the revolving rate. Same as 1, but you will keep single average daily balances (ADB). This value is only valid when the Base Rate for Regular Annual Rate parameter equals zero. With this option, the normal cardholder account strategy pricing terms will take effect after the introductory period, if any has been established. Information will be included with the standard ADB. After the introductory interest period, if any, the unique payoff exception, interest method, variable interest, and breakpoint methods set at the promotional level will be ignored.'
      }
    ]
  },
  {
    id: '103',
    name: 'payoff.exceptions.cp.ic.pe.set.1',
    options: [
      {
        value: 'MTHMTI1',
        description: ''
      },
      {
        value: 'MTHMTI2',
        description: ''
      },
      {
        value: 'MTHMTI3',
        description: ''
      }
    ]
  },
  {
    id: '104',
    name: 'payoff.exceptions.cp.ic.pe.set.2',
    options: [
      {
        value: 'MTHMTI1',
        description: ''
      },
      {
        value: 'MTHMTI2',
        description: ''
      },
      {
        value: 'MTHMTI3',
        description: ''
      }
    ]
  },
  {
    id: '105',
    name: 'payoff.exceptions.cp.ic.pe.set.3',
    options: [
      {
        value: 'MTHMTI1',
        description: ''
      },
      {
        value: 'MTHMTI2',
        description: ''
      },
      {
        value: 'MTHMTI3',
        description: ''
      }
    ]
  },
  {
    id: '106',
    name: 'introductory.interest.options',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description:
          'Use the "Base Interest Set 1" parameters in this section todetermine whether to apply the standard interest rate set at the customer account level or the introductory promotional rate set in the Intro Annual Rate parameter in this section.'
      },
      {
        value: '2',
        description:
          'Use the base rate set in either the Cash Advance Base Interest parameter or Merchandise Base Interest parameter in the promotion-only Interest Defaults (CP IC ID) method named on the Associated Product Control Files Sections screen in the Promotion Controls (PL RT PC) section of the Product Control File.'
      }
    ]
  },
  {
    id: '107',
    name: 'regular.annual.interest.options',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description:
          'Use the "Base Interest Set 1" parameters in this section todetermine whether to apply the standard interest rate set at the customer account level or the introductory promotional rate set in the Intro Annual Rate parameter in this section.'
      },
      {
        value: '1',
        description:
          'Use rate from the Base Interest Set 1 and Intro Annual Rate parameters in this section.'
      },
      {
        value: '2',
        description:
          'Use the base rate set in either the Cash Advance Base Interest parameter or Merchandise Base Interest parameter in the promotion-only Interest Defaults (CP IC ID) method named on the Associated Product Control Files Sections screen in the Promotion Controls (PL RT PC) section of the Product Control File.'
      }
    ]
  },
  {
    id: '108',
    name: 'r.path.promotion.return.indicator',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description: 'Use the promotion start and end dates.'
      },
      {
        value: '1',
        description: 'Use the return start and end dates.'
      }
    ]
  },
  {
    id: '109',
    name: 'promotion.discount.calculation.code',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description:
          'Do not use this option - use Merchant Discount Rate assigned to merchant only.'
      },
      {
        value: '1',
        description:
          'Calculate discount based on the promotional discount rate only.'
      },
      {
        value: '2',
        description:
          'Calculate discount based on adding the promotional discount rate to the merchant qualifying rate. If this parameter is set to 2 and the combined discount rate is greater than 100%, only the percentage amount over 100 is used as the combined discount rate. For example, if the promotional discount rate is 95% and the merchant qualifying rate is 25%, the System uses 20% as the combined discount rate. 95% + 25% = 120% 120% - 100 = 20%'
      }
    ]
  },
  {
    id: '110',
    name: 'collapse.return.to.revolving.option',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description: 'Not eligible to collapse.'
      },
      {
        value: '1',
        description:
          'Eligible to collapse when meets return to revolving criteria.'
      }
    ]
  },
  {
    id: '110',
    name: 'collapse.expiration.date.option',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description: 'Not eligible to collapse.'
      },
      {
        value: '1',
        description: 'Eligible to collapse when meets intro period expires.'
      }
    ]
  },
  {
    id: '111',
    name: 'delinquency.return.timing.code',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description: 'After cycle.'
      },
      {
        value: '1',
        description: 'Before cycle.'
      }
    ]
  },
  {
    id: '111',
    name: 'return.to.revolving.base.interest',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description:
          'Do not use this option. If the promotional balance returns to revolving, use the interest rate specified in the Base Interest Set 1.'
      },
      {
        value: '1',
        description:
          'If the promotional balance returns to revolving, use the interest rate at the next level up, the plan rate if one.'
      }
    ]
  },
  {
    id: '112',
    name: 'introductory.promotional.statement.text.id',
    options: [
      {
        value: 'TEXTDFVD',
        description: ''
      },
      {
        value: 'TEXTDFVE',
        description: ''
      },
      {
        value: 'TEXTDFVS',
        description: ''
      },
      {
        value: 'TEXTDFVR',
        description: ''
      }
    ]
  },
  {
    id: '113',
    name: 'introductory.message.control',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '1',
        description:
          'For promotions with introductory periods, display the introductory promotional message in both CIS and paper statements.'
      },
      {
        value: '3',
        description:
          'Display the introductory promotioal message in CIS statements.'
      }
    ]
  },
  {
    id: '114',
    name: 'regular.message.display.control',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '1',
        description:
          'Display the regular promotional message in both CIS and paper statements.'
      },
      {
        value: '3',
        description:
          'Display the regular promotional message in CIS statements.'
      }
    ]
  },
  {
    id: '115',
    name: 'description.from.text.id',
    options: [
      {
        value: 'TEXTDFQW',
        description: ''
      },
      {
        value: 'TEXTDFQA',
        description: ''
      },
      {
        value: 'TEXTDFQF',
        description: ''
      },
      {
        value: 'TEXTDFQG',
        description: ''
      }
    ]
  },
  {
    id: '116',
    name: 'delay.payment.start',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description:
          'Use the date the first promotional transaction posts to the account.'
      },
      {
        value: '1',
        description: 'Use the date the account was opened.'
      },
      {
        value: '2',
        description: 'Use the draw period end date.'
      },
      {
        value: '3',
        description:
          'Use the date an account was upgraded due to an account transfer.'
      }
    ]
  },
  {
    id: '117',
    name: 'return.to.revolve.internal.status',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: 'Blank',
        description:
          'Do not use delinquency and/or overlimit status as criteria to remove an account from promotional processing.'
      },
      {
        value: 'D',
        description:
          'If the date calculated in the Days Delinquent parameter equals the current date, the System begins processing the promotional balance based on the cardholder pricing strategy. If you set this parameter to D, you must set the Days Delinquentparameter in this section to a nonzero value.'
      }
    ]
  },
  {
    id: '118',
    name: 'promotion.expiration.text.id',
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      },
      {
        value: 'TEXTBRAE',
        description: ''
      },
      {
        value: 'TEXTBRAS',
        description: ''
      },
      {
        value: 'TEXTBRAR',
        description: ''
      }
    ]
  },
  {
    id: '119',
    name: 'expiring.promotion.notification.months',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '1',
        description: '1'
      },
      {
        value: '2',
        description: '2'
      },
      {
        value: '3',
        description: '3'
      },
      {
        value: '4',
        description: '4'
      },
      {
        value: '5',
        description: '5'
      },
      {
        value: '6',
        description: '6'
      },
      {
        value: '7',
        description: '7'
      },
      {
        value: '8',
        description: '8'
      },
      {
        value: '9',
        description: '9'
      },
      {
        value: '10',
        description: '10'
      },
      {
        value: '11',
        description: '11'
      },
      {
        value: '12',
        description: '12'
      },
      {
        value: '99',
        description: '99'
      }
    ]
  },
  {
    id: '120',
    name: 'interest.on.unbilled.interest',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description: 'No, do not accrue interest on unbilled interest.'
      },
      {
        value: '1',
        description: 'Yes, accrue interest on unbilled interest.'
      }
    ]
  },
  {
    id: '121',
    name: 'delay.interest.start.code',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description:
          'Calculate end date based on when the first transaction posts to this promotional balance, and your setting in "Promotional Interest Delay Number of Cycles."'
      },
      {
        value: '1',
        description:
          'Calculate end date based on account opening date, and your setting in "Promotional Interest Delay Number of Cycles."'
      }
    ]
  },
  {
    id: '122',
    name: 'exclude.cash.option.from.2.cycle.average.daily.balance',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description:
          'Do not exclude cash option promotion from 2-cycle average daily balance interest calculation.'
      },
      {
        value: '1',
        description:
          'Exclude cash option promotion from 2-cycle average daily balance interest calculation.'
      }
    ]
  },
  {
    id: '123',
    name: 'introductory.interest.default.cp.ic.id.set.1',
    options: [
      {
        value: 'MTHODFE1',
        description: ''
      },
      {
        value: 'MTHODFE2',
        description: ''
      },
      {
        value: 'MTHODFE3',
        description: ''
      }
    ]
  },
  {
    id: '124',
    name: 'introductory.interest.default.cp.ic.id.set.2',
    options: [
      {
        value: 'THIBOBE01',
        description: ''
      },
      {
        value: 'MTHMBE02',
        description: ''
      },
      {
        value: 'MTHMBE03',
        description: ''
      }
    ]
  },
  {
    id: '125',
    name: 'introductory.interest.default.cp.ic.id.set.3',
    options: [
      {
        value: 'NSAR3391',
        description: ''
      },
      {
        value: 'NSAR3392',
        description: ''
      },
      {
        value: 'MTHMQC2',
        description: ''
      }
    ]
  },
  {
    id: '126',
    name: 'interest.defaults.cp.ic.id.set.1',
    options: [
      {
        value: 'MANDRIL4',
        description: ''
      },
      {
        value: 'MANDRIL5',
        description: ''
      },
      {
        value: 'MTHMBA2',
        description: ''
      }
    ]
  },
  {
    id: '127',
    name: 'interest.defaults.cp.ic.id.set.2',
    options: [
      {
        value: 'RESHUS29',
        description: ''
      },
      {
        value: 'RESHUS30',
        description: ''
      },
      {
        value: 'MTHMSM2',
        description: ''
      }
    ]
  },
  {
    id: '128',
    name: 'interest.defaults.cp.ic.id.set.3',
    options: [
      {
        value: 'BELUG117',
        description: ''
      },
      {
        value: 'BELUG118',
        description: ''
      },
      {
        value: 'MTHMPO2',
        description: ''
      }
    ]
  },
  {
    id: '129',
    name: 'minimum.payment.pl.rt.mp.set.1',
    options: [
      {
        value: 'MTHMHR1',
        description: ''
      },
      {
        value: 'GOLIAT09',
        description: ''
      },
      {
        value: 'GOLIAT08',
        description: ''
      }
    ]
  },
  {
    id: '130',
    name: 'minimum.payment.pl.rt.mp.set.2',
    options: [
      {
        value: 'ORCARI78',
        description: ''
      },
      {
        value: 'ORCARI68',
        description: ''
      },
      {
        value: 'MTHMPM2',
        description: ''
      }
    ]
  },
  {
    id: '131',
    name: 'minimum.payment.pl.rt.mp.set.3',
    options: [
      {
        value: 'MTHMYT1',
        description: ''
      },
      {
        value: 'MTHMYT2',
        description: ''
      },
      {
        value: 'THERATI1',
        description: ''
      }
    ]
  },
  {
    id: '132',
    name: 'associate.with.plan.pl.rt.pa',
    options: [
      {
        value: 'ORANG890',
        description: ''
      },
      {
        value: 'ORANG892',
        description: ''
      },
      {
        value: 'MTHMBK2',
        description: ''
      }
    ]
  },
  {
    id: '133',
    name: 'zero.interest.extend.expiration.cd',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description:
          'End the zero percent interest rate on the promotion’s interest delay end date.'
      },
      {
        value: '1',
        description:
          'End the zero percent interest rate one day after the next billing cycle date following the promotion’s interest delay end date, update the promotion end date, and then extend the updated promotion expiration date one additional month, as illustrated in the following example: Assume a promotion interest delay end date of 6/19 and a statement cycle date of 6/25. With a valid code setting of 1, the System sets the interest delay end date to 6/26 and updates the promotion expiration date to 7/25.'
      },
      {
        value: '2',
        description:
          'End the zero percent interest rate one day after the next billing cycle date following the promotion’s interest delay end date and update the promotion end date (but do not extend the updated promotion expiration date one additional month) as illustrated in the following example: Assume a promotion interest delay end date of 6/19 and a statement cycle date of 6/25. With a valid code setting of 2, the System sets the interest delay end date to 6/26 and updates the promotion expiration date to 6/25.'
      }
    ]
  },
  {
    id: '134',
    name: 'promotion.payoff.exception.option',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description:
          'Exclude this promotional balance from the amount required to meet payoff exception processing.'
      },
      {
        value: '1',
        description:
          'Include this promotional balance in the amount required to meet payoff exception processing.'
      },
      {
        value: '2',
        description:
          'Exclude this promotional balance from the amount required to meet payoff exception processing when the delay period end date is greater than the last statement date.'
      },
      {
        value: '3',
        description:
          'Exclude this promotional balance from the amount required to meet payoff exception processing when the delay period end date is greater than the payoff exception date.'
      }
    ]
  },
  {
    id: '135',
    name: 'introductory.promotion.load.time',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description:
          'Load when a new pricing strategy or promotional method is created.'
      },
      {
        value: '1',
        description: 'Reload nightly.'
      }
    ]
  },
  {
    id: '136',
    name: 'regular.annual.load.time',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description:
          'Load when a new pricing strategy or promotional method is created.'
      },
      {
        value: '1',
        description: 'Reload nightly.'
      }
    ]
  },
  {
    id: '137',
    name: 'cpocpp.return.usage',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: 'N',
        description:
          'No, do not use the strategy-level delinquency days and timing parameters for return to revolving processing. If you select this option, the Optis system will incorporate thepromotional-level Days Delinquent and Dlnq Rtr Timing Code parameters in this section into return to revolving processing.'
      },
      {
        value: 'Y',
        description:
          'Yes, use the strategy-level delinquency days and timing parameters for return to revolving processing. If you select this option, the System will bypass the Days Delinquent and Dlnq Rtr Timing Code parameters and direct the strategy-level parameters to work in conjunction with other return to revolving parameters.'
      }
    ]
  },
  {
    id: '138',
    name: 'billed.pay.due.nm.code',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description:
          'Do not permit a zero billed payment due. If the promotional balance is not delinquent and the calculated minimum payment due is zero, use the remaining promotional balance as the billed payment due amount.'
      },
      {
        value: '1',
        description:
          'Permit a zero billed payment due. If the promotional balance is not delinquent and the calculated minimum payment due is zero, use zero as the billed payment due amount.'
      }
    ]
  },
  {
    id: '139',
    name: 'reinstate.mpd.terms.code',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description:
          'Calculate minimum payment due terms at the promotional balance level.'
      },
      {
        value: '1',
        description:
          'Calculate minimum payment due terms using next-available-level logic, where the sequence is plan balance level, protected balance level, and then the standard balance level. Be aware that a setting of F, 8, or 9 in the Standard Minimum Payments Calculation Method parameter will override your setting in this parameter.'
      }
    ]
  },
  {
    id: '140',
    name: 'promotion.billed.interest.code',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description:
          'Apply unpaid billed interest at the promotional balance level.'
      },
      {
        value: '1',
        description:
          'Apply unpaid billed interest using next-available-level logic, where the sequence is plan balance level, protected balance level, and then the standard balance level. If you set this parameter to 1, the System will take your setting in the base interest revolving switch on the *CM6 screen into account as follows:\n  *If the switch is set to the promotional balance level, the System will use next-available-level logic.\n   *If the switch is set to the standard balance level and the promotion is unexpired, the System will use next-available-level logic.\n   *If the switch is set to the standard balance level and the promotion has expired, the System will apply any unpaid billed interest at the promotional balance level.'
      }
    ]
  },
  {
    id: '141',
    name: 'statement.promotion.display.method.id.pl.rt.sd',
    options: [
      {
        value: 'RAPTOR1',
        description: ''
      },
      {
        value: 'RAPTOR2',
        description: ''
      },
      {
        value: 'MTHREI2',
        description: ''
      }
    ]
  },
  {
    id: '123',
    name: 'system.principal.agents',
    options: [
      {
        value: 'name',
        code: 'sysItem-prinItem'
      }
    ]
  },
  {
    id: '142',
    name: 'promotion.description.display',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description: 'Do not display the promotion description.'
      },
      {
        value: '1',
        description:
          'Display the description from the Description parameter. If you set this parameter to 1, the information set for the Description parameter in this section will appear as the second detail line of a transaction.'
      },
      {
        value: '2',
        description:
          'Display the description from the Alternate Description parameter. If you set this parameter to 2, the information set for the Alternate Description parameter in this section will appear as the second detail line of a transaction.'
      },
      {
        value: '3',
        description:
          'Display the description from the DD, Description Display, text type in the DT, Detail Descriptions, text area of the PCF Text Maintenance feature. If you set this parameter to 3, you must type a valid DT DD text identifier in the Description From Text ID - DT/DD parameter.'
      }
    ]
  },
  {
    id: '219',
    name: 'new.external.status.code',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: 'A',
        description: 'Authorization prohibited'
      },
      {
        value: 'Blank',
        description: 'Normal'
      }
    ]
  },
  {
    id: '215',
    name: 'transaction',
    options: [
      {
        id: 17,
        value: 'IDENTIFIER',
        type: 'Required',
        description: "Identifier of the customer's account",
        workFlow: 'besm'
      },
      {
        id: 17,
        value: 'IDENTIFIER',
        type: 'Required',
        description: "Identifier of the customer's account",
        workFlow: 'mad'
      },
      {
        id: 17,
        value: 'IDENTIFIER',
        type: 'Required',
        description: "Identifier of the customer's account",
        workFlow: 'bsfs'
      }
    ]
  },
  {
    id: '216',
    name: 'nm16.external.status.code',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: 'A',
        description: 'Authorization prohibited'
      },
      {
        value: 'B',
        description: 'Bankrupt'
      },
      {
        value: 'C',
        description: 'Closed'
      },
      {
        value: 'E',
        description: 'Revoked'
      },
      {
        value: 'F',
        description: 'Frozen'
      },
      {
        value: 'I',
        description: 'Interest accrual prohibited'
      },
      {
        value: 'L',
        description: 'Lost'
      },
      {
        value: 'U',
        description: 'Stolen'
      },
      {
        value: 'Z',
        description: 'Charge-off'
      },
      {
        value: 'Blank',
        description: 'Normal'
      },
      {
        value: 'Current Status',
        description: ''
      }
    ]
  },
  {
    id: '217',
    name: 'reason.code',
    options: [
      {
        value: '01',
        description: ''
      },
      {
        value: '03',
        description: ''
      },
      {
        value: '04',
        description: ''
      },
      {
        value: '05',
        description: ''
      }
    ]
  },
  {
    id: '218',
    name: 'nm0.transaction.id',
    options: [
      {
        value: 'nm.670',
        name: 'NM*670',
        description: 'Cardholder No Delete Flag'
      },
      {
        value: 'nm.54',
        name: 'NM*54',
        description: 'Remove External Status Code Z'
      }
    ]
  },
  {
    id: '219',
    name: 'new.external.status.code',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: 'A',
        description: 'Authorization prohibited'
      },
      {
        value: 'B',
        description: 'Bankrupt'
      },
      {
        value: 'C',
        description: 'Closed'
      },
      {
        value: 'E',
        description: 'Revoked'
      },
      {
        value: 'F',
        description: 'Frozen'
      },
      {
        value: 'I',
        description: 'Interest accrual prohibited'
      },
      {
        value: 'L',
        description: 'Lost'
      },
      {
        value: 'U',
        description: 'Stolen'
      },
      {
        value: 'Blank',
        description: 'Normal'
      }
    ]
  },
  {
    id: '220',
    name: 'propagate',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: 'N',
        description:
          'No, do not propagate new external status code to subaccounts.'
      },
      {
        value: 'Y',
        description: 'Yes, propagate new external status code to subaccounts.'
      }
    ]
  },
  {
    id: '241',
    name: 'joining.fee.statement.description',
    options: [
      {
        value: 'DEFJF001',
        description: '',
        selected: true
      },
      {
        value: 'DEFJF002',
        description: ''
      },
      {
        value: 'DEFJF003',
        description: ''
      },
      {
        value: 'DEFJF004',
        description: ''
      }
    ]
  },
  {
    id: '242',
    name: 'joining.fee.batch.id',
    options: [
      {
        value: 'Q1',
        description: ''
      },
      {
        value: 'Q2',
        description: ''
      },
      {
        value: 'Q3',
        description: ''
      },
      {
        value: 'Q4',
        description: ''
      },
      {
        value: 'Q5',
        description: ''
      },
      {
        value: 'Q6',
        description: ''
      },
      {
        value: 'Q7',
        description: ''
      },
      {
        value: 'Q8',
        description: ''
      },
      {
        value: 'Q9',
        description: ''
      },
      {
        value: 'QA',
        description: ''
      },
      {
        value: 'QB',
        description: ''
      },
      {
        value: 'QC',
        description: ''
      },
      {
        value: 'QD',
        description: ''
      },
      {
        value: 'QE',
        description: ''
      },
      {
        value: 'QF',
        description: ''
      },
      {
        value: 'QG',
        description: ''
      },
      {
        value: 'QH',
        description: ''
      },
      {
        value: 'QI',
        description: ''
      },
      {
        value: 'QJ',
        description: ''
      },
      {
        value: 'QK',
        description: ''
      },
      {
        value: 'QL',
        description: ''
      },
      {
        value: 'QM',
        description: ''
      },
      {
        value: 'QN',
        description: ''
      },
      {
        value: 'QO',
        description: ''
      },
      {
        value: 'QP',
        description: ''
      },
      {
        value: 'QQ',
        description: ''
      },
      {
        value: 'QR',
        description: ''
      },
      {
        value: 'QS',
        description: ''
      },
      {
        value: 'QT',
        description: ''
      },
      {
        value: 'QU',
        description: ''
      },
      {
        value: 'QV',
        description: ''
      },
      {
        value: 'QW',
        description: ''
      },
      {
        value: 'QX',
        description: ''
      },
      {
        value: 'QY',
        description: ''
      },
      {
        value: 'QZ',
        description: ''
      }
    ]
  },
  {
    id: '243',
    name: 'card.replacement.fee.batch.id',
    options: [
      {
        value: 'A1',
        description: ''
      },
      {
        value: 'A2',
        description: ''
      },
      {
        value: 'D1',
        description: ''
      }
    ]
  },
  {
    id: '244',
    name: 'card.replacement.fee.statement.description',
    options: [
      {
        value: 'DEFPA001',
        description: ''
      },
      {
        value: 'DEFPA002',
        description: ''
      },
      {
        value: 'DEFPA003',
        description: ''
      }
    ]
  },
  {
    id: '245',
    name: 'first.year.maximum.fee.management',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description: 'Exclude the fee from first year maximum fee management.'
      },
      {
        value: '1',
        description:
          'Include the fee in first year maximum fee management, but do not allow fees that exceed the first year maximum fee allowance amount to post.'
      },
      {
        value: '2',
        description:
          'Include the fee in first year maximum fee management, but reduce fees that exceed the first year maximum fee allowance amount to the amount remaining in the allowance and then post.'
      },
      {
        value: '3',
        description:
          'Include the fee in first year maximum fee management, but allow fees that exceed the first year maximum fee allowance amount to post.'
      }
    ]
  },
  {
    id: '246',
    name: 'index.rate.method.cp.ic.ir',
    options: [
      {
        value: 'HILTOP15',
        description: ''
      },
      {
        value: 'BOLERO22',
        description: ''
      },
      {
        value: 'SPRINT07',
        description: ''
      },
      {
        value: 'IKUMA621',
        description: ''
      }
    ]
  },
  {
    id: '247',
    name: 'default.base.interest.usage',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description:
          'Use base interest parameters only as new account defaults or for promotional purchases when a new promotion or plan is created. '
      },
      {
        value: '1',
        description:
          'Use base interest to reestablish customer or promotional or plan settings before each night’s processing.'
      },
      {
        value: '2',
        description:
          'Use base rate parameters to reestablish customer settings when the customer debit ratifies to this strategy.'
      },
      {
        value: '3',
        description:
          'Use base rate parameters to reestablish customer settings when the method set controlling the promotional pricing terms for the account changes. You can only use this option for a Promotion Only method established in the Promotion Controls (PL RT PC) or the Plan Attributes (PL RT PA) section when using Balance Administration. If you choose this option, you must also set a value in the Plan/Promo Method Set parameter. If you use Balance Administration and you set this parameter to 3, the System reestablishes base interest rate settings on the account based on the method set you enter in the Plan/Promo Method Set parameter in this section.'
      }
    ]
  },
  {
    id: '248',
    name: 'default.minimum.usage',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description:
          'Use the minimum interest rate one time only, at new account entry or when a new promotional balance is created.'
      },
      {
        value: '1',
        description:
          'Reestablish the minimum interest rate before each night’s processing. All accounts assigned to methods for which you use this code are reestablished on the effective production date you established for the method. This code applies to standard balances and to promotional balances when the promotional balance is being processed using a method which contains this setting.'
      },
      {
        value: '2',
        description:
          'Reestablish the minimum interest rate when the customer debit ratifies. You cannot use this code for promotions.'
      }
    ]
  },
  {
    id: '249',
    name: 'default.maximum.usage',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description:
          'Add additional interest to delinquent accounts before maximum interest rates are applied. If the interest rate, after adding the additional interest, exceeds the maximum interest rate, the maximum interest rate will be applied.'
      },
      {
        value: '1',
        description:
          'Add additional interest to delinquent accounts after maximum interest rates are applied. If the interest rate, after adding the additional interest, exceeds the maximum interest rate, the higher rate will be applied. Ensure that using this option is appropriate if the interest rate exceeds state usury rate limits.'
      }
    ]
  },
  {
    id: '250',
    name: 'default.maximum.interest.usage',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description:
          'Use the maximum interest rate one time only, at new account entry or when a new promotional balance is created.'
      },
      {
        value: '1',
        description:
          'Reestablish the maximum interest rate before each night’s processing. All accounts assigned to methods for which you use this code are reestablished on the effective production date you established for the method. This code applies to standard balances and to promotional balances when the promotional balance is being processed using a method which contains this setting.'
      }
    ]
  },
  {
    id: '251',
    name: 'end.of.incentive.warning.notification.timing',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '00',
        description: ''
      },
      {
        value: '01',
        description: ''
      },
      {
        value: '02',
        description: ''
      },
      {
        value: '03',
        description: ''
      },
      {
        value: '04',
        description: ''
      },
      {
        value: '05',
        description: ''
      },
      {
        value: '06',
        description: ''
      },
      {
        value: '07',
        description: ''
      },
      {
        value: '08',
        description: ''
      },
      {
        value: '09',
        description: ''
      },
      {
        value: '10',
        description: ''
      },
      {
        value: '11',
        description: ''
      },
      {
        value: '12',
        description: ''
      }
    ]
  },
  {
    id: '252',
    name: 'end.of.incentive.warning.notification.statement.text',
    options: [
      {
        value: 'TEXTID01',
        description: ''
      },
      {
        value: 'TEXTID02',
        description: ''
      },
      {
        value: 'TEXTID03',
        description: ''
      },
      {
        value: 'TEXTID04',
        description: ''
      }
    ]
  },
  {
    id: '253',
    name: 'incentive.pricing.number.of.months',
    options: [
      {
        value: '01',
        description: ''
      },
      {
        value: '23',
        description: ''
      },
      {
        value: '34',
        description: ''
      },
      {
        value: '80',
        description: ''
      }
    ]
  },
  {
    id: '254',
    name: 'incentive.pricing.usage.code',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description:
          'Do not apply incentive pricing parameters to either new or existing accounts undergoing a change in strategy assignment.'
      },
      {
        value: '1',
        description: 'Apply incentive pricing parameters to new accounts only.'
      },
      {
        value: '2',
        description:
          'Apply incentive pricing parameters to new accounts and to existing accounts when the existing account undergoes a change in strategy assignment. If you set this parameter to 2, be aware that the incentive pricing parameters will apply to the entire balance of existing accounts undergoing a change in strategy assignment.'
      }
    ]
  },
  {
    id: '255',
    name: 'incentive.base.interest.usage.rate',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description:
          'Use incentive pricing interest rates one time only, based on the setting in the Incentive Pricing Usage parameter in this section.'
      },
      {
        value: '1',
        description:
          'Use incentive pricing interest rates to reestablish cardholder settings before each night’s processing. If you change this parameter from 0 to 1, the System reestablishes cardholder settings only for active accounts still in the incentive pricing period.'
      }
    ]
  },
  {
    id: '256',
    name: 'delinquency.cycles.for.termination',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description: ''
      },
      {
        value: '1',
        description: ''
      },
      {
        value: '2',
        description: ''
      },
      {
        value: '3',
        description: ''
      },
      {
        value: '4',
        description: ''
      },
      {
        value: '5',
        description: ''
      },
      {
        value: '6',
        description: ''
      },
      {
        value: '7',
        description: ''
      },
      {
        value: '8',
        description: ''
      },
      {
        value: '9',
        description: ''
      }
    ]
  },
  {
    id: '257',
    name: 'minimum.maximum.rate.override.options',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description:
          'Minimum and maximum rates within the Incentive Pricing method are not used. Use the minimum and maximum rates set on the cardholder account record.'
      },
      {
        value: '1',
        description:
          'Minimum and maximum rates within the Incentive Pricing method are not used. Use the maximum rates set on the cardholder account record (CP-IC-ID). Minimum rates are not used.'
      },
      {
        value: '2',
        description:
          'Use the minimum and maximum rates within the Incentive Pricing method while the account is within the incentive pricing period.'
      }
    ]
  },
  {
    id: '258',
    name: 'incentive.pricing.override.option',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description: 'Do not override incentive pricing.'
      },
      {
        value: '1',
        description: 'Override incentive pricing.'
      }
    ]
  },
  {
    id: '259',
    name: 'terminate.incentive.pricing.on.pricing.strategy.change',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description:
          'Terminate incentive pricing by replacing the end date with the last statement date.'
      },
      {
        value: '1',
        description:
          'Do not terminate incentive pricing by replacing the end date with the last statement date.'
      }
    ]
  },
  {
    id: '260',
    name: 'method.override.termination.option',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description:
          'Terminate incentive pricing by replacing the end date with the last statement date.'
      },
      {
        value: '1',
        description:
          'Do not terminate incentive pricing by replacing the end date with the last statement date.'
      }
    ]
  },
  {
    id: '261',
    name: 'message.for.protected.balance.incentive.pricing',
    options: [
      {
        value: 'TEXTID11',
        description: ''
      },
      {
        value: 'TEXTID12',
        description: ''
      },
      {
        value: 'TEXTID13',
        description: ''
      },
      {
        value: 'TEXTID14',
        description: ''
      }
    ]
  },
  {
    id: '331',
    name: 'service.subject.section',
    options: [
      {
        value: 'PL RT PC ',
        description: 'Loan Offers'
      },
      {
        value: 'PL RT PC',
        description: 'Card Promotions'
      },
      {
        value: 'PL RT PB',
        description: 'Terms'
      },
      {
        value: 'CP IC BP',
        description: 'Break Points'
      },
      {
        value: 'CP IC IB',
        description: 'Incentive Pricing Break Points'
      },
      {
        value: 'CP IC ID',
        description: 'Interest Defaults'
      },
      {
        value: 'CP IC IF',
        description: 'Interest and Fees'
      },
      {
        value: 'CP IC II ',
        description: 'Interest on Interest'
      },
      {
        value: 'CP IC IM ',
        description: ' Interest Methods'
      },
      {
        value: 'CP IC IP',
        description: 'Incentive Pricing'
      },
      {
        value: 'CP IC IR ',
        description: 'Index Rate'
      },
      {
        value: 'CP IC IV ',
        description: 'Incentive Pricing Variable Interest'
      },
      {
        value: 'CP IC ME',
        description: 'Max Cap EAPR'
      },
      {
        value: 'CP IC MF',
        description: 'Minimum Finance Charges'
      },
      {
        value: 'CP IC ML ',
        description: 'Method Level'
      },
      {
        value: 'CP IC MS ',
        description: 'Money Sales'
      },
      {
        value: 'CP IC PE',
        description: 'Payoff Exceptions'
      },
      {
        value: 'CP IC SC',
        description: 'State Controls'
      },
      {
        value: 'CP IC SP',
        description: 'Statement Production'
      },
      {
        value: 'CP IC SZ ',
        description: 'Securitization Controls'
      },
      {
        value: 'CP IC TI',
        description: 'Tiered Interest'
      },
      {
        value: 'CP IC VI',
        description: 'Variable Interest'
      },
      {
        value: 'CP PF DA',
        description: 'Declined Authorization Charges'
      },
      {
        value: 'CP PF LC',
        description: 'Late Charges'
      },
      {
        value: 'CP PF OC',
        description: 'Overlimit Charges'
      },
      {
        value: 'CP PF RC',
        description: 'Returned Check Charge'
      },
      {
        value: 'CP OC AP ',
        description: 'Adjustment Pricing'
      },
      {
        value: 'CP OC BD',
        description: 'Backdating'
      },
      {
        value: 'CP OC CL',
        description: 'Credit Line Management'
      },
      {
        value: 'CP OC CP',
        description: 'Current Pricing Notification'
      },
      {
        value: 'CP OC DR',
        description: 'Debit Ratification'
      },
      {
        value: 'CP OC FP',
        description: 'Full Pricing Notification'
      },
      {
        value: 'CP OC GS',
        description: 'Group Statement Design'
      },
      {
        value: 'CP OC MO',
        description: 'Method Overrides'
      },
      {
        value: 'CP OC MP',
        description: 'MULTRAN Processing'
      },
      {
        value: 'CP OC PP',
        description: 'Promotional Purchases'
      },
      {
        value: 'CP OC RB',
        description: 'Rebate Option/Bonus Program'
      },
      {
        value: 'CP OC SA ',
        description: 'Special Accounts'
      },
      {
        value: 'CP OC SD',
        description: 'Statement Design'
      },
      {
        value: 'CP OC TI',
        description: 'Travel Insurance'
      },
      {
        value: 'CP PB CT',
        description: 'Change in Terms'
      },
      {
        value: 'CP IO AC',
        description: 'Annual Charges'
      },
      {
        value: 'CP IO CI',
        description: 'Cash Advance Item Charges'
      },
      {
        value: 'CP IO CL ',
        description: 'Credit Life'
      },
      {
        value: 'CP IO MC',
        description: 'Miscellaneous Charges'
      },
      {
        value: 'CP IO MI',
        description: 'Merchandise Item Charges'
      },
      {
        value: 'CP IO SC',
        description: 'Statement Charges'
      },
      {
        value: 'CP PO CA',
        description: 'Credit Application'
      },
      {
        value: 'CP PO FM',
        description: 'UUFixed Minimum Payment'
      },
      {
        value: 'CP PO MP ',
        description: 'Minimum Payment Due'
      },
      {
        value: 'CP PO RM',
        description: 'Rules Minimum Payment'
      },
      {
        value: 'CP PO SP',
        description: 'Skip Payment'
      }
    ]
  },

  {
    id: '262',
    name: 'search.code',
    options: [
      {
        value: 'E',
        description: 'Exact'
      },
      {
        value: 'R',
        description: 'Range'
      }
    ]
  },
  {
    id: '6123',
    name: 'include.activity.option',
    options: [
      {
        value: 'E',
        description: 'Exact'
      },
      {
        value: 'R',
        description: 'Range'
      },
      {
        value: '',
        description: 'Experience'
      }
    ]
  },
  {
    id: '61235',
    name: 'pos.promotion.validation',
    options: [
      {
        value: 'E',
        description: 'Exact'
      },
      {
        value: 'R',
        description: 'Range'
      },
      {
        value: '',
        description: 'Experience'
      }
    ]
  },
  {
    id: '262',
    name: 'nm.0.transaction.id',
    options: [
      {
        name: '',
        value: 'E',
        description: 'Exact'
      },
      {
        name: 'Example',
        value: 'R',
        description: 'Range'
      }
    ]
  },
  {
    id: '26255',
    name: ConfigTableOptionsEnum.tlpTable,
    options: [
      {
        name: ConfigTableOptionsEnum.tlpTable,
        value: 'E',
        description: 'Exact'
      },
      {
        name: ConfigTableOptionsEnum.tlpTable,
        value: 'R',
        description: 'Range'
      },
      {
        name: ConfigTableOptionsEnum.tlpTable,
        value: 'R',
        description: 'Range'
      }
    ]
  },
  {
    id: '5235',
    name: 'allocation.interval',
    options: [
      {
        value: 'E',
        description: 'Exact'
      },
      {
        value: 'R',
        description: 'Range'
      },
      {
        value: '',
        description: 'Number'
      }
    ]
  },
  {
    id: '331',
    name: 'strategy.area',
    options: [
      {
        value: 'AM',
        description: 'Account Marketing'
      },
      {
        value: 'AO',
        description: 'Agent Only'
      },
      {
        value: 'AU',
        description: 'Authorizations'
      },
      {
        value: 'CI',
        description: 'Card Issuance Unit'
      },
      {
        value: 'CO',
        description: 'Collections'
      },
      {
        value: 'CP',
        description: 'Card Holder Pricing'
      },
      {
        value: 'DA',
        description: 'Dependent Account'
      },
      {
        value: 'DO',
        description: 'Delinquency and Overlimit'
      },
      {
        value: 'MP',
        description: 'Merchant Processing'
      },
      {
        value: 'PD',
        description: 'Plastic Disclosure Inserting'
      },
      {
        value: 'PN',
        description: 'Penalty Fee'
      },
      {
        value: 'PM',
        description: 'Promotional Message'
      }
    ]
  }
];
