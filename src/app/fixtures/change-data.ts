export const ChangeList = [
  {
    id: 1708984056,
    name: 'Increase Fees for Black Card',
    changeOwner: {
      id: 1051691920,
      name: 'Freddy Kruger'
    },
    createdDate: '2021-06-05T13:27:52.0586093+07:00',
    effectiveDate: '2021-06-07T13:27:52.0586779+07:00',
    lastChangedDate: '2021-06-09T13:27:52.0586812+07:00',
    lastChangedBy: {
      id: 708119904,
      name: 'Elmer Hall'
    },
    changeType: 'PRICING',
    changeStatus: 'REJECTED',
    portfoliosImpactedCount: 3849,
    cardholderAccountsImpactedCount: 4449,
    pricingStrategiesCreatedCount: 8357,
    pricingStrategiesUpdatedCount: 2336,
    pricingMethodsCreatedCount: 7146,
    pricingMethodsUpdatedCount: 5299,
    dmmTablesCreated: 1378,
    dmmTablesUpdated: 3103,
    pendingApprovalBy: [],
    approvalRecords: [{ action: 'approve' }]
  },
  {
    workflowInstanceCount: 35,
    id: 1150226841,
    name: 'Card Compromise November',
    changeOwner: {
      id: 1160668354,
      name: 'Nate Nash'
    },
    createdDate: '2021-06-09T13:27:52.0588592+07:00',
    effectiveDate: '2021-06-09T13:27:52.0588627+07:00',
    lastChangedDate: '2021-06-07T13:27:52.0588641+07:00',
    lastChangedBy: {
      id: 2072692943,
      name: 'Nate Nash'
    },
    changeType: 'pricing',
    changeStatus: 'VALIDATED',
    portfoliosImpactedCount: 7894,
    cardholderAccountsImpactedCount: 4472,
    pricingStrategiesCreatedCount: 3281,
    pricingStrategiesUpdatedCount: 4361,
    pricingMethodsCreatedCount: 6787,
    pricingMethodsUpdatedCount: 3784,
    dmmTablesCreated: 7628,
    dmmTablesUpdated: 7236,
    pendingApprovalBy: [],
    approvalRecords: [{ action: 'approve' }]
  },
  {
    workflowInstanceCount: 35,
    id: 1150226841,
    name: 'Card Compromise November',
    changeOwner: {
      id: 1160668354,
      name: 'Nate Nash'
    },
    createdDate: '2021-06-09T13:27:52.0588592+07:00',
    effectiveDate: '2021-06-09T13:27:52.0588627+07:00',
    lastChangedDate: '2021-06-07T13:27:52.0588641+07:00',
    lastChangedBy: {
      id: 2072692943,
      name: 'Nate Nash'
    },
    changeType: 'ACCOUNTMANAGEMENT',
    changeStatus: 'WORK',
    portfoliosImpactedCount: 7894,
    cardholderAccountsImpactedCount: 4472,
    pricingStrategiesCreatedCount: 3281,
    pricingStrategiesUpdatedCount: 4361,
    pricingMethodsCreatedCount: 6787,
    pricingMethodsUpdatedCount: 3784,
    dmmTablesCreated: 7628,
    dmmTablesUpdated: 7236,
    pendingApprovalBy: [],
    approvalRecords: []
  }
];

export const pendingApproval = {
  id: 1150226841,
  name: 'name',
  changeType: 'pricing',
  defaultgroup: 'group a',
  user: 'user',
  nextApproverGroup: 'nextApproverGroup'
};

export const pendingApprovalMapping = {
  id: 'id',
  name: 'Nate Nash',
  changeType: 'spricing',
  defaultgroup: 'defaultgroup',
  user: 'user',
  nextApproverGroup: 'nextApproverGroup'
};

export const ChangeMapping = {
  changeId: 'id',
  changeName: 'name',
  changeOwner: 'changeOwner.name',
  createdDate: 'createdDate',
  effectiveDate: 'effectiveDate',
  approvalDate: 'lastChangedDate',
  changeType: 'changeType',
  changeStatus: 'changeStatus',
  pricingStrategiesCreatedCount: 'pricingStrategiesCreatedCount',
  pricingStrategiesUpdatedCount: 'pricingStrategiesUpdatedCount',
  pricingMethodsCreatedCount: 'pricingMethodsCreatedCount',
  pricingMethodsUpdatedCount: 'pricingMethodsUpdatedCount',
  portfoliosImpacted: 'portfoliosImpactedCount',
  cardholdersAccountsImpacted: 'cardholderAccountsImpactedCount',
  lastApprovalBy: 'lastChangedBy.name',
  noOfDMMTablesCreated: 'dmmTablesCreated',
  noOfDMMTablesUpdated: 'dmmTablesUpdated',
  approvalRecords: 'approvalRecords'
};

export const AttachmentData = {
  attachmentList: [
    {
      attachmentId: '85888544',
      category: 'client.allocation.table',
      filename: 'Development guilde.docx',
      workflowName: 'Create Private Label Card Promotion',
      mimeType: 'text/plain',
      accessible: false,
      fileSize: 3293583,
      recordCount: 48,
      uploadedDate: '2021-02-16T00:00:00',
      uploadedBy: 'Laura Bailey'
    },
    {
      attachmentId: '88456403',
      category: 'retail.qualification.table',
      filename: 'August Releases_01112021.xlsx',
      workflowName: 'Create Change in Terms Event',
      mimeType: 'image/png',
      accessible: true,
      fileSize: 4334937,
      recordCount: 85,
      uploadedDate: '2020-12-29T00:00:00',
      uploadedBy: 'Jesse Dunn'
    }
  ],
  reports: [
    {
      reportId: '64250976',
      filename: 'Employment Agreement.pdf',
      workflowName: 'Modify Incentive Pricing',
      mimeType: 'image/jpg',
      accessible: false,
      fileSize: 4647526,
      recordCount: 40,
      systemGeneratedDate: '2021-02-20T00:00:00'
    }
  ]
};

export const AttachmentMapping = {
  attachmentId: 'attachmentId',
  filename: 'filename',
  accessible: 'accessible',
  category: 'category',
  mimeType: 'mimeType',
  fileSize: 'fileSize',
  recordCount: 'recordCount',
  uploadedDate: 'uploadedDate',
  uploadedBy: 'uploadedBy',
  workflowName: 'workflowName'
};

export const ReportMapping = {
  reportId: 'reportId',
  filename: 'filename',
  accessible: 'accessible',
  category: 'category',
  mimeType: 'mimeType',
  fileSize: 'fileSize',
  recordCount: 'recordCount',
  workflowName: 'workflowName',
  systemGeneratedDate: 'systemGeneratedDate'
};

export const DataMaping = {
  pricingMethods: {
    id: 'id',
    name: 'name',
    versionCreating: 'versionCreating',
    modeledFrom: 'modeledFrom',
    methodType: 'methodType',
    parametersUpdated: 'parametersUpdated',
    parameters: 'parameters'
  },
  portfolioList: {
    id: 'id',
    portfolioName: 'name',
    icon: 'icon',
    portfolioId: {
      type: 'dynamic',
      containerProp: 'portfolioCriteria',
      fieldProp: 'portfolioId',
      mappingToType: 'string',
      key: 'fieldName',
      value: 'values'
    },
    clientId: {
      type: 'dynamic',
      containerProp: 'portfolioCriteria',
      fieldProp: 'clientId',
      mappingToType: 'string',
      key: 'fieldName',
      value: 'values'
    },
    spa: {
      type: 'dynamic',
      containerProp: 'portfolioCriteria',
      fieldProp: 'spa',
      mappingToType: 'array',
      key: 'fieldName',
      value: 'values'
    },
    NumberOfPricingStrategies: 'portfolioStats.numberOfPricingStrategiesCount',
    totalDelinquency: 'portfolioStats.delinquentAccounts',
    availableAccounts: 'portfolioStats.availableAccounts',
    netIncome: 'portfolioStats.netIncome',
    availableCredit: 'portfolioStats.availableCredit',
    portfolioNew: 'portfolioStats.newAccounts',
    activeAccounts: 'portfolioStats.activeAccounts',
    payments: 'portfolioStats.payments'
  },
  changeManagementList: {
    changeId: 'changeId',
    changeName: 'name',
    changeOwner: 'changeOwner',
    createdDate: 'createdDate',
    effectiveDate: 'effectiveDate',
    approvalDate: 'lastChangeDate',
    changeType: 'changeType',
    changeStatus: 'changeStatus',
    pricingStrategiesCreatedCount: 'pricingStrategiesCreatedCount',
    pricingStrategiesUpdatedCount: 'pricingStrategiesUpdatedCount',
    pricingMethodsCreatedCount: 'pricingMethodsCreatedCount',
    pricingMethodsUpdatedCount: 'pricingMethodsUpdatedCount',
    portfoliosImpacted: 'portfoliosImpactedCount',
    cardholdersAccountsImpacted: 'cardholderAccountsImpactedCount',
    lastApprovalBy: 'lastChangeBy',
    noOfDMMTablesCreated: 'numberOfDmmTablesCreatedCount',
    noOfDMMTablesUpdated: 'numberOfDmmTablesUpdatedCount'
  },
  changeDetailSnapshot: {
    actions: 'actions',
    changeId: 'changeId',
    createdDate: 'createdDate',
    effectiveDate: 'effectiveDate',
    approvalDate: 'lastChangeDate',
    lastApprovalBy: 'lastChangeBy',
    changeStatus: 'changeStatus',
    changeType: 'changeType',
    changeOwner: 'changeOwner',
    changeDescription: 'changeDescription',
    changeName: 'name',
    pricingStrategiesCreatedCount: 'pricingStrategiesCreatedCount',
    pricingStrategiesUpdatedCount: 'pricingStrategiesUpdatedCount',
    pricingMethodsCreatedCount: 'pricingMethodsCreatedCount',
    pricingMethodsUpdatedCount: 'pricingMethodsUpdatedCount',
    noOfDMMTablesCreated: 'numberOfDmmTablesCreatedCount',
    noOfDMMTablesUpdated: 'numberOfDmmTablesUpdatedCount',
    portfoliosImpacted: 'portfoliosImpactedCount',
    cardholdersAccountsImpacted: 'cardholderAccountsImpactedCount',
    rejectedBy: 'rejectedBy',
    rejectedReason: 'rejectedReason',
    approvalRecords: 'approvalRecords'
  },
  changeWorkflow: {
    id: 'id',
    name: 'workflowTemplate.name',
    description: 'workflowTemplate.description',
    updatedDate: 'lastUpdated',
    updatedBy: 'lastUpdatedBy',
    workflowType: 'workflowTemplate.type',
    status: 'status',
    firstRan: 'workflowTemplate.firstExecuted',
    firstRanBy: 'workflowTemplate.firstExecutedBy',
    validationMessages: 'validationMessages',
    lastExecuted: 'workflowTemplate.lastExecuted',
    isFavorite: 'workflowTemplate.isFavorite',
    countOfAttachments: 'countOfAttachments'
  },
  pricingStrategies: {
    id: 'id',
    name: 'name',
    modeledFrom: 'modeledFrom',
    methods: 'methods',
    description: 'description'
  },
  activityList: {
    id: 'id',
    name: 'name',
    dateChanged: 'dateChanged',
    changeId: 'changeId',
    changeName: 'changeName',
    action: 'action',
    actionDetails: 'actionDetails'
  }
};

export const ExportData: any = {
  changeDetailPricing: {
    changeDescription: 'dolor commodo duis et',
    rejectedBy: 'Luke Skywalker',
    rejectedReason: 'laoreet et takimata labore sed',
    validationMessages: [],
    changeId: '66190634',
    name: 'Increase Fees for Black Card',
    changeOwner: 'Elmer Hall',
    createdDate: '2021-03-02T14:49:23.9639217+07:00',
    effectiveDate: '2021-03-02T14:49:23.9639854+07:00',
    lastChangeDate: '2021-02-28T14:49:23.964029+07:00',
    lastChangeBy: 'Elmer Hall',
    changeType: 'Pricing',
    changeStatus: 'Work',
    portfoliosImpactedCount: 2757,
    cardholderAccountsImpactedCount: 2955,
    pricingStrategiesCreatedCount: 1185,
    pricingStrategiesUpdatedCount: 3057,
    pricingMethodsCreatedCount: 4972,
    pricingMethodsUpdatedCount: 4563,
    numberOfDmmTablesCreatedCount: 6658,
    numberOfDmmTablesUpdatedCount: 1194
  },
  changeDetailNonPricing: {
    actions: ['APPROVE'],
    changeDescription: 'sed ut tempor lorem eum vero',
    rejectedBy: 'Elmer Hall',
    rejectedReason: 'diam dolore rebum',
    validationMessages: [],
    changeId: '79565064',
    name: 'Increase Fees for Black Card',
    changeOwner: 'Jesse Dunn',
    createdDate: '2021-03-04T10:49:19.6591305+07:00',
    effectiveDate: '2021-03-04T10:49:19.6591412+07:00',
    lastChangeDate: '2021-03-02T10:49:19.6591461+07:00',
    lastChangeBy: 'Mary Sue',
    changeType: 'File Management',
    changeStatus: 'Work',
    portfoliosImpactedCount: 6249,
    cardholderAccountsImpactedCount: 6829,
    pricingStrategiesCreatedCount: 2337,
    pricingStrategiesUpdatedCount: 1054,
    pricingMethodsCreatedCount: 7351,
    pricingMethodsUpdatedCount: 5025,
    numberOfDmmTablesCreatedCount: 6005,
    numberOfDmmTablesUpdatedCount: 4372,
    approvalRecords: [{ action: 'approve' }]
  },
  portfolioList: [
    {
      id: '29455161',
      name: 'Omaha Bank Master Card',
      icon: '',
      portfolioCriteria: [
        {
          fieldName: 'portfolioId',
          values: ['357925']
        },
        {
          fieldName: 'clientId',
          values: ['11478']
        },
        {
          fieldName: 'spa',
          values: ['0020/0200/0195']
        },
        {
          fieldName: 'productType',
          values: ['Black']
        }
      ],
      portfolioStats: {
        delinquentAccounts: 5650,
        availableAccounts: 88715,
        activeAccounts: 9456,
        numberOfPricingStrategiesCount: 5292,
        newAccounts: 0.736552050680179,
        currency: 997.673044545424,
        availableCredit: 49096512.1895208,
        netIncome: 150664456.229042,
        payments: 279214228.110584
      }
    },
    {
      id: '31823556',
      name: 'Omaha Bank Point Visa',
      icon: 'abc',
      portfolioCriteria: [
        {
          fieldName: 'portfolioId',
          values: ['444743']
        },
        {
          fieldName: 'clientId',
          values: ['72449']
        },
        {
          fieldName: 'spa',
          values: ['0020/0200/0155']
        },
        {
          fieldName: 'productType',
          values: ['Black']
        }
      ],
      portfolioStats: {
        delinquentAccounts: 7331,
        availableAccounts: 70232,
        activeAccounts: 4805,
        numberOfPricingStrategiesCount: 9866,
        newAccounts: 0.112742759339857,
        currency: 123.015299482744,
        availableCredit: 867070261.07786,
        netIncome: 523502604.940389,
        payments: 566654228.684745
      }
    }
  ]
};
