export const NotificationList: INotification[] = [
  {
    id: '122',
    changeSetId: '456',
    changeName: 'changeSetDetail.name',
    changeStatus: 'changeSetDetail.changeStatus',
    title: 'title',
    description: 'description',
    date: 'createdTimestamp',
    isRead: true,
    isNew: false,
    type: 'type',
    action: 'changeSetSubmittedForApproval',
    actionBy: 'actionBy',
    teamName: 'changeSetDetail.teamName',
    approvalRecords: [
      {
        action: 'approve',
        reason: 'abc',
        actionTime: new Date('2021-05-03T10:27:25.0191871+07:00'),
        user: {
          id: 599207098,
          name: 'Darth Vader'
        },
        group: {
          name: undefined
        }
      }
    ]
  },
  {
    id: '123',
    changeSetId: '456',
    changeName: 'changeSetDetail.name',
    changeStatus: 'changeSetDetail.changeStatus',
    title: 'title',
    description: 'description',
    date: 'createdTimestamp',
    isRead: true,
    isNew: false,
    type: 'type',
    action: 'changeSetSubmittedForApproval',
    actionBy: 'actionBy',
    teamName: 'changeSetDetail.teamName',
    approvalRecords: [
      {
        action: 'approve',
        reason: 'abc',
        actionTime: new Date('2021-05-03T10:27:25.0191871+07:00'),
        user: {
          id: 599207098,
          name: 'Darth Vader'
        },
        group: {
          name: 'John Doe'
        }
      }
    ]
  },
  {
    id: '124',
    changeSetId: '457',
    changeName: 'changeSetDetail.name',
    changeStatus: 'changeSetDetail.changeStatus',
    title: 'title',
    description: 'description',
    date: 'createdTimestamp',
    isRead: false,
    isNew: true,
    type: 'type',
    action: 'changesetapproved',
    actionBy: 'actionBy',
    teamName: 'changeSetDetail.teamName',
    approvalRecords: [
      {
        action: 'approve',
        reason: 'abc',
        actionTime: new Date('2021-05-03T10:27:25.0191871+07:00'),
        user: {
          id: 599207098,
          name: 'Darth Vader'
        },
        group: {
          name: 'John Doe'
        }
      }
    ]
  },
  {
    id: '125',
    changeSetId: '458',
    changeName: 'changeSetDetail.name',
    changeStatus: 'changeSetDetail.changeStatus',
    title: 'title',
    description: 'description',
    date: 'createdTimestamp',
    isRead: false,
    isNew: true,
    type: 'type',
    action: 'changesetrejected',
    actionBy: 'actionBy',
    teamName: 'changeSetDetail.teamName',
    approvalRecords: [
      {
        action: 'approve',
        reason: 'abc',
        actionTime: new Date('2021-05-03T10:27:25.0191871+07:00'),
        user: {
          id: 599207098,
          name: 'Darth Vader'
        },
        group: {
          name: 'John Doe'
        }
      }
    ]
  },
  {
    id: '126',
    changeSetId: '456',
    changeName: 'changeSetDetail.name',
    changeStatus: 'changeSetDetail.changeStatus',
    title: 'title',
    description: 'description',
    date: 'createdTimestamp',
    isRead: true,
    isNew: false,
    type: 'type',
    action: 'changesetlapsed',
    actionBy: 'actionBy',
    teamName: 'changeSetDetail.teamName',
    approvalRecords: [
      {
        action: 'approve',
        reason: 'abc',
        actionTime: new Date('2021-05-03T10:27:25.0191871+07:00'),
        user: {
          id: 599207098,
          name: 'Darth Vader'
        },
        group: {
          name: 'John Doe'
        }
      }
    ]
  },
  {
    id: '127',
    changeSetId: '456',
    changeName: 'changeSetDetail.name',
    changeStatus: 'changeSetDetail.changeStatus',
    title: 'title',
    description: 'description',
    date: 'createdTimestamp',
    isRead: true,
    isNew: false,
    type: 'type',
    action: 'changesetsubmittedforvalidation',
    actionBy: 'actionBy',
    teamName: 'changeSetDetail.teamName',
    approvalRecords: [
      {
        action: 'approve',
        reason: 'abc',
        actionTime: new Date('2021-05-03T10:27:25.0191871+07:00'),
        user: {
          id: 599207098,
          name: 'Darth Vader'
        },
        group: {
          name: 'John Doe'
        }
      }
    ]
  },
  {
    id: '128',
    changeSetId: '456',
    changeName: 'changeSetDetail.name',
    changeStatus: 'changeSetDetail.changeStatus',
    title: 'title',
    description: 'description',
    date: 'createdTimestamp',
    isRead: true,
    isNew: false,
    type: 'type',
    action: 'addedreminder',
    actionBy: 'actionBy',
    teamName: 'changeSetDetail.teamName',
    approvalRecords: [
      {
        action: 'approve',
        reason: 'abc',
        actionTime: new Date('2021-05-03T10:27:25.0191871+07:00'),
        user: {
          id: 599207098,
          name: 'Darth Vader'
        },
        group: {
          name: 'John Doe'
        }
      }
    ]
  },
  {
    id: '129',
    changeSetId: '456',
    changeName: 'changeSetDetail.name',
    changeStatus: 'changeSetDetail.changeStatus',
    title: 'title',
    description: 'description',
    date: 'createdTimestamp',
    isRead: true,
    isNew: false,
    type: 'type',
    action: 'changeSetSubmittedForApproval',
    actionBy: 'actionBy',
    teamName: 'changeSetDetail.teamName',
    approvalRecords: []
  }
];

export const NotificationMapping = {
  id: 'id',
  changeSetId: 'changeSetDetail.changeId',
  changeName: 'changeSetDetail.name',
  changeStatus: 'changeSetDetail.changeStatus',
  title: 'title',
  description: 'description',
  date: 'createdTimestamp',
  isRead: 'isRead',
  isNew: 'isNew',
  type: 'type',
  action: 'action',
  actionBy: 'actionBy',
  teamName: 'changeSetDetail.teamName',
  approvalRecords: 'approvalRecords'
};

export const NotificationData = [
  {
    id: '1',
    title: 'go to change detail',
    changeSetId: '12345',
    description: 'desc',
    date: '03/04/2020',
    isRead: false
  },
  {
    id: '2',
    title: 'title 2',
    changeSetId: '123456',
    description: 'desc',
    date: '03/04/2020',
    isRead: false
  },
  {
    id: '3',
    title: 'title 3',
    changeSetId: '123456',
    description: 'desc',
    date: '03/05/2020',
    isRead: false,
    action: 'addedreminder'
  }
];
