export const ActivityList = [
  {
    id: '62079088',
    dateChanged: '2021-02-27T15:55:19.325294+07:00',
    changeName: 'August Release',
    changeId: '47797551',
    name: 'Jesse Dunn',
    action: 'rejectedChange',
    actionDetails: [
      {
        field: 'rejectReason',
        previousValue: null,
        newValue: 'sanctus sadipscing sit',
        workflowName: null,
        workflowId: null
      },
      {
        field: 'ChangeStatus',
        previousValue: 'erat et eos commodo nam ad',
        newValue: 'amet quod invidunt no augue doming diam',
        workflowName: null,
        workflowId: null
      },
      {
        field: 'ChangeOwner',
        previousValue: 'erat et eos commodo nam ad',
        newValue: 'amet quod invidunt no augue doming diam',
        workflowName: null,
        workflowId: null
      }
    ]
  },
  {
    id: '36797231',
    dateChanged: '2021-02-25T15:55:19.3275006+07:00',
    changeName: 'Card Compromise November',
    changeId: '63017106',
    name: 'John Doe',
    action: 'addedWorkflow',
    actionDetails: [
      {
        field: 'changeName',
        previousValue: 'erat et eos commodo nam ad',
        newValue: 'amet quod invidunt no augue doming diam',
        workflowName: 'Create Private Label Card Promotion',
        workflowId: '41492352'
      }
    ]
  },
  {
    id: '36797235',
    dateChanged: '2021-02-25T15:55:19.3275006+07:00',
    changeName: 'Card Compromise November',
    changeId: '63017106',
    name: 'John Doe',
    action: 'addedWorkflow',
    actionDetails: [
      {
        field: 'ChangeStatus',
        previousValue: 'erat et eos commodo nam ad',
        newValue: 'amet quod invidunt no augue doming diam',
        workflowName: 'Create Private Label Card Promotion',
        workflowId: '41492352'
      }
    ]
  }
];

export const ActivityMapping = {
  id: 'id',
  name: 'name',
  dateChanged: 'dateChanged',
  changeId: 'changeId',
  changeName: 'changeName',
  action: 'action',
  actionDetails: 'actionDetails'
};
