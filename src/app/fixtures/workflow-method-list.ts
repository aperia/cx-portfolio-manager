export const WorkflowMethodListData = [
  {
    id: '',
    name: 'KYI1',
    modeledFrom: null,
    comment: 'Add New Method',
    serviceSubjectSection: 'CP PF RC',
    parameters: [
      {
        name: 'returned.check.charge.option',
        previousValue: '',
        newValue: '2'
      },
      {
        name: 'number.of.days',
        previousValue: '',
        newValue: ''
      }
    ]
  },
  {
    id: '1234',
    name: 'KYI1',
    modeledFrom: {
      id: '1234',
      name: 'KYI1',
      version: {
        id: '456'
      }
    },
    comment: 'Create new version of method',
    serviceSubjectSection: 'CP PF RC',
    parameters: [
      {
        name: 'computer.letter',
        previousValue: '',
        newValue: ''
      },
      {
        name: 'number.of.days',
        previousValue: '',
        newValue: '5'
      }
    ]
  },
  {
    id: '12345',
    name: 'KYI12',
    modeledFrom: {
      id: '12345',
      name: 'KYI15',
      version: {
        id: '4567'
      }
    },
    comment: '',
    serviceSubjectSection: 'CP PF RC',
    parameters: [
      {
        name: 'computer.letter',
        previousValue: '',
        newValue: ''
      },
      {
        name: 'number.of.days',
        previousValue: '',
        newValue: '5'
      }
    ]
  }
];

export const GetMethodListData = [
  {
    id: '1',
    name: 'MTHMTCI',
    effectiveDate: '2021-05-26T15:58:48.1953131+07:00',
    versions: [
      {
        id: '93560569',
        comment: 'mock note',
        effectiveDate: '2021-05-28T15:58:48.195335+07:00',
        status: 'WORK',
        parameters: [
          {
            name: 'returned.check.charge.option',
            previousValue: '2',
            newValue: '0'
          }
        ]
      }
    ],
    pricingStrategies: [
      {
        id: 451076229,
        name: 'KTI6',
        description: 'magna diam exerci eu',
        effectiveDate: '2021-05-27T15:58:48.1954773+07:00',
        numberOfCardHolders: 56
      }
    ]
  },
  {
    id: '2',
    name: 'MTHMTAA',
    effectiveDate: '',
    versions: null,
    pricingStrategies: null
  }
];

export const GetMethodListDataMapping = {
  id: 'id',
  name: 'name',
  effectiveDate: 'effectiveDate',
  versions: 'versions',
  pricingStrategies: 'pricingStrategies'
};
