export const WorkflowChangeSetList = [
  {
    workflowInstanceCount: 31,
    changeSetId: 1708984056,
    name: 'Increase Fees for Black Card',
    changeOwner: {
      id: 1051691920,
      name: 'Freddy Kruger'
    },
    createdDate: '2021-06-05T13:27:52.0586093+07:00',
    effectiveDate: '2021-06-07T13:27:52.0586779+07:00',
    lastChangedDate: '2021-06-09T13:27:52.0586812+07:00',
    lastChangedBy: {
      id: 708119904,
      name: 'Elmer Hall'
    },
    changeType: 'PRICING',
    changeStatus: 'REJECTED',
    portfoliosImpactedCount: 3849,
    cardholderAccountsImpactedCount: 4449,
    pricingStrategiesCreatedCount: 8357,
    pricingStrategiesUpdatedCount: 2336,
    pricingMethodsCreatedCount: 7146,
    pricingMethodsUpdatedCount: 5299,
    dmmTablesCreated: 1378,
    dmmTablesUpdated: 3103,
    pendingApprovalBy: [],
    approvalRecords: []
  },
  {
    workflowInstanceCount: 35,
    changeSetId: 1150226841,
    name: 'Card Compromise November',
    changeOwner: {
      id: 1160668354,
      name: 'Nate Nash'
    },
    createdDate: '2021-06-09T13:27:52.0588592+07:00',
    effectiveDate: '2021-06-09T13:27:52.0588627+07:00',
    lastChangedDate: '2021-06-07T13:27:52.0588641+07:00',
    lastChangedBy: {
      id: 2072692943,
      name: 'Nate Nash'
    },
    changeType: 'ACCOUNTMANAGEMENT',
    changeStatus: 'VALIDATED',
    portfoliosImpactedCount: 7894,
    cardholderAccountsImpactedCount: 4472,
    pricingStrategiesCreatedCount: 3281,
    pricingStrategiesUpdatedCount: 4361,
    pricingMethodsCreatedCount: 6787,
    pricingMethodsUpdatedCount: 3784,
    dmmTablesCreated: 7628,
    dmmTablesUpdated: 7236,
    pendingApprovalBy: [],
    approvalRecords: []
  }
];

export const WorkflowChangeSetListMapping = {
  changeId: 'changeSetId',
  changeName: 'name',
  changeOwner: 'changeOwner.name',
  createdDate: 'createdDate',
  effectiveDate: 'effectiveDate',
  approvalDate: 'lastChangedDate',
  changeType: 'changeType',
  changeStatus: 'changeStatus',
  pricingStrategiesCreatedCount: 'pricingStrategiesCreatedCount',
  pricingStrategiesUpdatedCount: 'pricingStrategiesUpdatedCount',
  pricingStrategiesImpacted: 'pricingStrategiesImpacted',
  pricingMethodsCreatedCount: 'pricingMethodsCreatedCount',
  pricingMethodsUpdatedCount: 'pricingMethodsUpdatedCount',
  portfoliosImpacted: 'portfoliosImpactedCount',
  cardholdersAccountsImpacted: 'cardholderAccountsImpactedCount',
  lastApprovalBy: 'lastChangedBy.name',
  noOfDMMTablesCreated: 'dmmTablesCreated',
  noOfDMMTablesUpdated: 'dmmTablesUpdated',
  approvalRecords: 'approvalRecords',
  workflowInstanceCount: 'workflowInstanceCount'
};
