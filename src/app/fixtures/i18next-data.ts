export const I18NextMapping = {
  lang: 'lang',
  resource: 'resource'
};

export const I18NextData = {
  txt_activities: 'Activities',
  txt_portfolio_manager: 'Portfolio Manager'
};
