export const WorkflowStrategyListData = [
  {
    effectiveDate: '2022-01-17T00:00:00',
    strategyName: 'mockName1'
  },
  {
    effectiveDate: '2022-10-30T00:00:00',
    strategyName: 'mockName2'
  },
  {
    effectiveDate: '2022-02-09T00:00:00',
    strategyName: 'mockName3'
  },
  {
    effectiveDate: '2021-12-28T00:00:00',
    strategyName: 'mockName4'
  }
];
