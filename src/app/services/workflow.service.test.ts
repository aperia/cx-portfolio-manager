import * as apiService from 'app/services/api.service';
import { workflowService } from 'app/services/workflow.service';

const apiPost = jest.spyOn(apiService.apiService, 'post');

const workflowReminderTemp = {
  workflowTemplateId: 'string',
  cronStartDate: undefined,
  cronExpression: 'string',
  type: 'string',
  recipients: ['string[]'],
  description: 'string'
};

const createWorkflowInstanceTemp = {
  workflowInstanceDetails: {
    note: 'string'
  },
  workflowTemplateId: 'string',
  changeSetId: 'string'
};

const updateFlowInstanceDetailTemp = {
  workflowInstanceId: 'string',
  changeId: 'string',
  workflowInstanceDetails: {
    note: 'string'
  },
  changeSetDetail: {
    name: 'string',
    description: 'string',
    effectiveDate: 'string'
  }
};

const UpdateFlowInstanceDataTemp = {
  workflowInstanceId: 'string',
  status: 'string',
  data: {}
};

describe('workflow service ', () => {
  beforeEach(() => {
    apiPost.mockImplementation(
      () =>
        new Promise(resolve => {
          resolve({});
        })
    );
  });

  it('should getTemplateWorkflows success', async () => {
    const response = await workflowService.getTemplateWorkflows();
    expect(response).toEqual({});
  });
  it('should getWorkflowList success', async () => {
    await workflowService.getWorkflowList();
    const response = await workflowService.getWorkflowList();
    expect(response).toEqual({});
  });
  it('should getWorkflowDetails success', async () => {
    const response = await workflowService.getWorkflowDetails();
    expect(response).toEqual({});
  });
  it('should removeWorkflow success', async () => {
    const response = await workflowService.removeWorkflow('id');
    expect(response).toEqual({});
  });
  it('should favoriteWorkflowTemplate success', async () => {
    const response = await workflowService.favoriteWorkflowTemplate('id');
    expect(response).toEqual({});
  });
  it('should unfavoriteWorkflowTemplate success', async () => {
    const response = await workflowService.unfavoriteWorkflowTemplate('id');
    expect(response).toEqual({});
  });

  it('should setReminderWorkflow success', async () => {
    const response = await workflowService.setReminderWorkflow(
      workflowReminderTemp
    );
    expect(response).toEqual({});
  });
  it('should createWorkflowInstance success', async () => {
    const response = await workflowService.createWorkflowInstance(
      createWorkflowInstanceTemp
    );
    expect(response).toEqual({});
  });
  it('should updateWorkflowInstance success', async () => {
    const response = await workflowService.updateWorkflowInstance(
      updateFlowInstanceDetailTemp
    );
    expect(response).toEqual({});
  });
  it('should getWorkflowInstance success', async () => {
    const response = await workflowService.getWorkflowInstance('id');
    expect(response).toEqual({});
  });
  it('should updateWorkflowInstanceData success', async () => {
    const response = await workflowService.updateWorkflowInstanceData(
      UpdateFlowInstanceDataTemp
    );
    expect(response).toEqual({});
  });
  it('should getElementDataByName success', async () => {
    const response = await workflowService.getElementDataByName('id', ['id']);
    expect(response).toEqual({});
  });
  it('should getWorkflowMethods success', async () => {
    const response = await workflowService.getWorkflowMethods('id');
    expect(response).toEqual({});
  });
  it('should getWorkflowMethods success with array param', async () => {
    const response = await workflowService.getWorkflowMethods(['id']);
    expect(response).toEqual({});
  });
  it('should getInProgressWorkflow success', async () => {
    const response = await workflowService.getInProgressWorkflow();
    expect(response).toEqual({});
  });
  it('should getWorkflowEffectiveDateOptions success', async () => {
    const response = await workflowService.getWorkflowEffectiveDateOptions({});
    expect(response).toEqual({});
  });
  it('should getPerformMonetaryAdjustmentTransaction success', async () => {
    const response =
      await workflowService.getPerformMonetaryAdjustmentTransaction('type');
    expect(response).toEqual({});
  });
  it('should getWorkflowTransactionList success', async () => {
    const response = await workflowService.getWorkflowTransactionList({});
    expect(response).toEqual({});
  });
  it('should getWorkflowTransactionListCondition success', async () => {
    const response = await workflowService.getWorkflowTransactionListCondition(
      {}
    );
    expect(response).toEqual({});
  });
  it('should getWorkflowSPAList success', async () => {
    const response = await workflowService.getWorkflowSPAList({});
    expect(response).toEqual({});
  });

  it('should getOCSShellList success', async () => {
    const response = await workflowService.getOCSShellList({});
    expect(response).toEqual({});
  });

  it('should getWorkflowMerchantList success', async () => {
    const response = await workflowService.getWorkflowMerchantList('');
    expect(response).toEqual({});
  });

  it('should exportMerchants success', async () => {
    const response = await workflowService.exportMerchants('');
    expect(response).toEqual({});
  });

  it('should getProfileTransactions success', async () => {
    const response = await workflowService.getProfileTransactions('');
    expect(response).toEqual({});
  });

  it('should getWorkflowStrategies success', async () => {
    const response = await workflowService.getWorkflowStrategies('');
    expect(response).toEqual({});
  });
  it('should getWorkflowChangeList success', async () => {
    const response = await workflowService.getWorkflowChangeList({ id: '1' });
    expect(response).toEqual({});
  });
  it('should getQueueProfile success', async () => {
    const response = await workflowService.getQueueProfile('');
    expect(response).toEqual({});
  });
  it('should getOcsShellProfile success', async () => {
    const response = await workflowService.getOcsShellProfile('');
    expect(response).toEqual({});
  });
  it('should exportShellProfile success', async () => {
    const response = await workflowService.exportShellProfile('');
    expect(response).toEqual({});
  });
  it('should getSupervisorList success', async () => {
    const response = await workflowService.getSupervisorList({ id: '1' });
    expect(response).toEqual({});
  });
  it('should getServiceSubjectSectionOptions success', async () => {
    const response = await workflowService.getServiceSubjectSectionOptions();
    expect(response).toEqual({});
  });
  it('should getTableListForWorkflowSetup success', async () => {
    const response = await workflowService.getTableListForWorkflowSetup(
      'TableType'
    );
    expect(response).toEqual({});
  });
  it('should getTextIDListForArchive success', async () => {
    const response = await workflowService.getTextIDListForArchive('textArea');
    expect(response).toEqual({});
  });
  it('should  getPricingStrategyListForWorkflowSetup success', async () => {
    const response =
      await workflowService.getPricingStrategyListForWorkflowSetup(
        'strategyArea'
      );
    expect(response).toEqual({});
  });
  it('should  getTableListForArchive success', async () => {
    const response = await workflowService.getTableListForArchive('tableArea');
    expect(response).toEqual({});
  });
  it('should  exportAnalyzer success', async () => {
    const response = await workflowService.exportAnalyzer({ parameters: [] });
    expect(response).toEqual({});
  });
});
