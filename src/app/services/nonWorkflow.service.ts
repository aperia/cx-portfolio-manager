import {
  getElementDataByNameUrl,
  getServiceSubjectSectionOptionsUrl
} from 'app/constants/api-links';
import { apiService } from './api.service';

class NonWorkflowService {
  getServiceSubjectSectionOptions() {
    return apiService.post(getServiceSubjectSectionOptionsUrl, {});
  }

  getElementDataByName(workflowTemplateId: string, elementName: string[]) {
    const request = {
      searchFields: {
        workflowTemplateId,
        elementName
      }
    };
    return apiService.post(getElementDataByNameUrl, request);
  }
}

export const nonWorkflowService = new NonWorkflowService();
