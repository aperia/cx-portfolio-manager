import {
  changeNotFoundDetectEmptyResponseUrls,
  changeNotFoundDetectUrls,
  changeNotFoundSelectDeletedChangeUrls,
  MOCK_API_URL,
  updateWorkflowInstanceDataUrl
} from 'app/constants/api-links';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import Axios, {
  AxiosError,
  AxiosInstance,
  AxiosRequestConfig,
  AxiosResponse,
  CancelTokenStatic
} from 'axios';
import { isEmpty } from 'lodash';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import { Dispatch } from 'redux';
import { actionsChange } from '../../pages/_commons/redux/Change';
import { appSetting } from '../constants/configurations';

const { dispatch, getState } = store;

export interface ApiAxiosInstance extends AxiosInstance {
  CancelToken?: CancelTokenStatic;
  setupAxiosInterceptors?: (interceptors?: InterceptorsInstance) => void;
}

export interface InterceptorsInstance {
  request: {
    success?: (
      value: AxiosRequestConfig
    ) => AxiosRequestConfig | Promise<AxiosRequestConfig>;
    failure?: (error: any) => any;
  };
  response: {
    success?: (value: AxiosResponse) => AxiosResponse | Promise<AxiosResponse>;
    failure?: (error: any) => any;
  };
}

class ServiceSingleton {
  private static instance: ServiceSingleton;

  public axiosInstance: ApiAxiosInstance;
  public dispatch: Dispatch<any> = dispatch;
  public getState: () => RootState = getState;

  private interceptorsRequestNumber = 0;
  private interceptorsResponseNumber = 0;

  /**
   * Singleton's constructor must be private
   */
  private constructor() {
    this.axiosInstance = Axios.create({
      baseURL: appSetting.apiUrl
    });
  }

  public static getInstance(): ServiceSingleton {
    if (ServiceSingleton.instance) return ServiceSingleton.instance;

    ServiceSingleton.instance = new ServiceSingleton();
    const $this = ServiceSingleton.instance;
    // Setup interceptor by default
    $this.axiosInstance.setupAxiosInterceptors = $this.setupAxiosInterceptors;
    $this.setupAxiosInterceptors();

    return $this;
  }

  private setupAxiosInterceptors(interceptors?: InterceptorsInstance) {
    const $this = ServiceSingleton.getInstance();
    if (interceptors) {
      // Eject request/response interceptors
      $this.axiosInstance.interceptors.request.eject(
        $this.interceptorsRequestNumber
      );
      $this.axiosInstance.interceptors.response.eject(
        $this.interceptorsResponseNumber
      );

      /**
       * Try to set the handler to empty array,
       * cuz the handler will be set null after eject
       */
      ($this.axiosInstance.interceptors.request as any).handlers = [];
      ($this.axiosInstance.interceptors.response as any).handlers = [];

      // Inject request/response interceptors
      $this.interceptorsRequestNumber =
        $this.axiosInstance.interceptors.request.use(
          interceptors.request.success,
          interceptors.request.failure
        );
      $this.interceptorsResponseNumber =
        $this.axiosInstance.interceptors.response.use(
          interceptors.response.success,
          interceptors.response.failure
        );

      return;
    }

    const handleRequestSuccess = (
      config: AxiosRequestConfig
    ): Promise<AxiosRequestConfig> => {
      const token = appSetting.apiToken;
      config.headers.Authorization = token;
      config.maxBodyLength = Infinity;
      config.maxContentLength = Infinity;

      if (
        MOCK_API_URL.some(u => config.url?.indexOf(u) !== -1) &&
        process.env.REACT_APP_API_MOCK
      ) {
        config.baseURL = process.env.REACT_APP_API_MOCK;
      }

      const requestCommon: RequestCommon = {
        startSequence: 0,
        endSequence: 0,
        requestId: '',
        userData: '',
        txid: '',
        user: '',
        service: '',
        auth: '',
        agent: '',
        cycle: '',
        clientName: '',
        clientNumber: '',
        x500id: '',
        extensions: {},
        audience: '',
        logo: '',
        originHosts: [''],
        sourceIp: '',
        encoding: '',
        key: '',
        size: 0,
        retry: 0,
        timeout: 0,
        sessionId: '',
        cid: '',
        clientEmail: '',
        clientId: '',
        eventOrigin: '',
        eventOriginInputName: ''
      };

      if (config.data instanceof FormData) {
        Object.keys(requestCommon).forEach(key => {
          let value = (requestCommon as any)[key];

          if (typeof value === 'object') {
            value = JSON.stringify(value);
          } else {
            value += '';
          }

          config.data.append(key, value);
        });
      } else {
        config.data = {
          ...config.data,
          ...requestCommon
        };
      }

      return Promise.resolve(config);
    };

    const handleRequestFailure = (error: AxiosError): Promise<Error> => {
      return Promise.reject(error.request);
    };

    this.axiosInstance.interceptors.request.use(
      handleRequestSuccess,
      handleRequestFailure
    );

    const isSelectAChangeStep = () =>
      $this.getState().workflowSetup?.workflowFormSteps?.selectedStep ===
      'changeInformation';
    const currentSelectedChangeId = () =>
      $this.getState().workflowSetup?.workflowFormSteps?.forms?.[
        'changeInformation'
      ]?.changeSetId;

    const handleResponseSuccess = (
      response: AxiosResponse
    ): Promise<AxiosResponse> => {
      const url = response.config?.url;

      const isChangeNotFoundDetect =
        changeNotFoundDetectUrls.includes(url!) &&
        !(response.config as any).ignoreChangeNotFoundDetect;

      // TODO: should remove after FS API fix it
      const isChangeNotFoundForEmptyResponse =
        changeNotFoundDetectEmptyResponseUrls.includes(url!) &&
        isEmpty(response.data);

      const respStatus =
        response.data?.status || response.data?.messages?.[0]?.status;
      const isChangeNotFoundResponse =
        respStatus === 'NOT_FOUND' &&
        // TODO: should remove after FS API fix it
        response.data?.total !== 0;

      if (
        isChangeNotFoundDetect &&
        (isChangeNotFoundResponse || isChangeNotFoundForEmptyResponse)
      ) {
        $this.dispatch(actionsChange.setShowChangeNotFoundModal(true));

        return Promise.reject(response);
      }

      if (url === updateWorkflowInstanceDataUrl) {
        $this.dispatch(
          actionsWorkflowSetup.updateWorkflowInstance({
            ...response.data,
            workflowInstanceDetails: {
              ...response.data.workflowInstanceDetails,
              changeSetId: currentSelectedChangeId()
            }
          })
        );
      }

      return Promise.resolve(response);
    };

    const handleResponseFailure = (error: AxiosError): Promise<Error> => {
      if (Axios.isCancel(error)) {
        return Promise.resolve({ isCancel: true } as any);
      }
      const url = error.config?.url;

      const isChangeNotFoundDetect =
        changeNotFoundDetectUrls.includes(url!) &&
        !(error.config as any).ignoreChangeNotFoundDetect;

      const isSelectDeletedChange =
        changeNotFoundSelectDeletedChangeUrls.includes(url!) &&
        isSelectAChangeStep();
      const isChangeNotFoundResponse = error.response?.status === 500;

      if (
        isChangeNotFoundDetect &&
        (isChangeNotFoundResponse || isSelectDeletedChange)
      ) {
        if (isSelectDeletedChange) {
          $this.dispatch(
            actionsWorkflowSetup.updateWorkflowFormStep({
              name: 'changeInformation',
              values: {
                showChangeNoExistError: true,
                isValid: false
              }
            })
          );
          $this.dispatch(actionsChange.setShowChangeNotFoundModal(false));
        } else {
          $this.dispatch(actionsChange.setShowChangeNotFoundModal(true));
        }
      }
      return Promise.reject(error.response);
    };

    $this.axiosInstance.interceptors.response.use(
      handleResponseSuccess,
      handleResponseFailure
    );
  }
}

export const apiService = ServiceSingleton.getInstance().axiosInstance;
export const dispatchFnc = ServiceSingleton.getInstance().dispatch;
