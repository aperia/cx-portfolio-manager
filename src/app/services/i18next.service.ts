import axios from 'axios';
class I18nextService {
  async getMultipleLanguage(lang: string) {
    const baseURL = `i18n`;
    let returnData = { lang, resource: {} };
    const { data } = await axios.get<{ key: string }>(
      `${baseURL}/${lang}/main.json`
    );
    returnData = {
      ...returnData,
      resource: { ...returnData.resource, ...data }
    };
    return { data: returnData };
  }
}

export const i18nextService = new I18nextService();
