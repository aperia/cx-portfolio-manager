import * as apiService from 'app/services/api.service';
import { workflowTemplateService } from 'app/services/workflowTemplate.service';

const apiPost = jest.spyOn(apiService.apiService, 'post');

describe('workflow template service ', () => {
  beforeEach(() => {
    apiPost.mockImplementation(
      () =>
        new Promise(resolve => {
          resolve({});
        })
    );
  });

  it('should getTemplateWorkflows success', async () => {
    const response = await workflowTemplateService.getWorkflowTemplates();
    expect(response).toEqual({});
  });

  it('should getFileDetails success', async () => {
    const response = await workflowTemplateService.getFileDetails({});
    expect(response).toEqual({});
  });

  it('should deleteWorkflowTemplate success', async () => {
    const response = await workflowTemplateService.deleteWorkflowTemplate({});
    expect(response).toEqual({});
  });

  it('should uploadWorkflowTemplate success', async () => {
    const response = await workflowTemplateService.uploadWorkflowTemplate(
      {},
      {}
    );
    expect(response).toEqual({});
  });

  it('should downloadWorkflowTemplate success', async () => {
    const response = await workflowTemplateService.downloadWorkflowTemplate({});
    expect(response).toEqual({});
  });
});
