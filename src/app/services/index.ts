import { activityService } from './activity.service';
import { apiService } from './api.service';
import { changeService } from './change.service';
import { i18nextService } from './i18next.service';
import { nonWorkflowService } from './nonWorkflow.service';
import { notificationService } from './notification.service';
import { portfolioService } from './portfolio.service';
import { refDataService } from './refData.service';
import { systemService } from './system.service';
import { workflowService } from './workflow.service';
import { workflowTemplateService } from './workflowTemplate.service';

export const apiMapping = {
  apiService,
  refDataService,
  systemService,
  changeService,
  activityService,
  portfolioService,
  workflowService,
  i18nextService,
  notificationService,
  workflowTemplateService,
  nonWorkflowService
};

export {
  apiService,
  refDataService,
  systemService,
  changeService,
  activityService,
  portfolioService,
  workflowService,
  i18nextService,
  notificationService,
  workflowTemplateService,
  nonWorkflowService
};

export type APIMapping = typeof apiMapping;
