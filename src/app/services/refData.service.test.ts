import * as apiService from 'app/services/api.service';
import { refDataService } from 'app/services/refData.service';

const apiPost = jest.spyOn(apiService.apiService, 'post');

describe('refData service ', () => {
  it('should getHomePortfolio success', async () => {
    apiPost.mockImplementation(
      () =>
        new Promise((resolve, reject) => {
          resolve({});
        })
    );

    const response = await refDataService.getStateRefData();
    expect(response).toEqual({});
  });
});
