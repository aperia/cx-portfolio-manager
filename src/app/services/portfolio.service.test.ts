import * as apiService from 'app/services/api.service';
import { portfolioService } from 'app/services/portfolio.service';

const apiPost = jest.spyOn(apiService.apiService, 'post');

describe('portfolio service ', () => {
  it('should getHomePortfolio success', async () => {
    apiPost.mockImplementation(
      () =>
        new Promise((resolve, reject) => {
          resolve({});
        })
    );

    const response = await portfolioService.getHomePortfolio();
    expect(response).toEqual({});
  });
  it('should getPortfolioList success', async () => {
    apiPost.mockImplementation(
      () =>
        new Promise((resolve, reject) => {
          resolve({});
        })
    );

    const response = await portfolioService.getPortfolioList('id');
    expect(response).toEqual({});
  });
  it('should getPortfolioList without changeId success', async () => {
    apiPost.mockImplementation(
      () =>
        new Promise((resolve, reject) => {
          resolve({});
        })
    );

    const response = await portfolioService.getPortfolioList();
    expect(response).toEqual({});
  });

  it('should getPortfolioDetail without changeId success', async () => {
    apiPost.mockImplementation(
      () =>
        new Promise((resolve, reject) => {
          resolve({});
        })
    );

    const response = await portfolioService.getPortfolioDetail('id');
    expect(response).toEqual({});
  });

  it('should getKPIListForSetup success', async () => {
    apiPost.mockImplementation(
      () =>
        new Promise((resolve, reject) => {
          resolve({});
        })
    );

    const response = await portfolioService.getKPIListForSetup();
    expect(response).toEqual({});
  });

  it('should getKPIDetails', async () => {
    apiPost.mockImplementation(
      () =>
        new Promise((resolve, reject) => {
          resolve({});
        })
    );

    const response = await portfolioService.getKPIDetails('id');
    expect(response).toEqual({});
  });

  it('should addPortfolioKPI success', async () => {
    apiPost.mockImplementation(
      () =>
        new Promise((resolve, reject) => {
          resolve({});
        })
    );

    const response = await portfolioService.addPortfolioKPI({} as never);
    expect(response).toEqual({});
  });

  it('should updatePortfolioKPI success', async () => {
    apiPost.mockImplementation(
      () =>
        new Promise((resolve, reject) => {
          resolve({});
        })
    );

    const response = await portfolioService.updatePortfolioKPI({} as never);
    expect(response).toEqual({});
  });

  it('should deletePortfolioKPI success', async () => {
    apiPost.mockImplementation(
      () =>
        new Promise((resolve, reject) => {
          resolve({});
        })
    );

    const response = await portfolioService.deletePortfolioKPI({} as never);
    expect(response).toEqual({});
  });
});
