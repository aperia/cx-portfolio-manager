import { i18nextService } from 'app/services/i18next.service';
import axios from 'axios';

const apiGet = jest.spyOn(axios, 'get');

describe('i18next service ', () => {
  it('should getMultipleLanguage success', async () => {
    apiGet.mockImplementation(
      () =>
        new Promise((resolve, reject) => {
          resolve({});
        })
    );

    const response = await i18nextService.getMultipleLanguage('en');
    expect(response).toEqual({ data: { lang: 'en', resource: {} } });
  });
});
