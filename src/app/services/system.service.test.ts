import * as apiService from 'app/services/api.service';
import { systemService } from 'app/services/system.service';

const apiPost = jest.spyOn(apiService.apiService, 'get');

describe('system service ', () => {
  it('should getConfig success', async () => {
    apiPost.mockImplementation(
      () =>
        new Promise((resolve, reject) => {
          resolve({});
        })
    );

    const response = await systemService.getConfig();
    expect(response).toEqual({});
  });
  it('should getReactSettingConfig success', async () => {
    apiPost.mockImplementation(
      () =>
        new Promise((resolve, reject) => {
          resolve({});
        })
    );

    const response = await systemService.getReactSettingConfig();
    expect(response).toEqual({});
  });
  it('should getMapping success', async () => {
    apiPost.mockImplementation(
      () =>
        new Promise((resolve, reject) => {
          resolve({});
        })
    );

    const response = await systemService.getMapping();
    expect(response).toEqual({});
  });
  it('should getWorkflowMapping success', async () => {
    apiPost.mockImplementation(
      () =>
        new Promise((resolve, reject) => {
          resolve({});
        })
    );

    const response = await systemService.getWorkflowMapping();
    expect(response).toEqual({});
  });
});
