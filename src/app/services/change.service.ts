import {
  approvalQueueListUrl,
  changeAttachmentListUrl,
  changeDetailsUrl,
  changeOwnerListUrl,
  changeSetListUrl,
  CHANGE_ACTION,
  CHANGE_DETAIL_PRICING_METHODS_FIELDS,
  CHANGE_DETAIL_PRICING_STRATEGIES_FIELDS,
  CHANGE_DETAIL_SNAPSHOT_FIELDS,
  CHANGE_LIST_FIELDS,
  CHANGSET_ATTACHMENT_FIELDS,
  createChangeSetUrl,
  dateInformationUrl,
  deleteChangeUrl,
  dmmListUrl,
  DMM_LIST_FIELDS,
  downloadAttachmentUrl,
  downloadDecisionElementsUrl,
  DOWNLOAD_ATTACHMENT_FIELDS,
  exportChangeSetUrl,
  getDecisionElementUrl,
  getDecisionListUrl,
  getLettersUrl,
  getStrategiesListAQ,
  getTableListUrl,
  getWorkflowChangeSetListUrl,
  pendingApprovalGroupUrl,
  PENDING_APPROVAL_GROUP_FIELDS,
  portfolioListUrl,
  pricingMethodListlUrl,
  pricingStrategyListUrl,
  processChangeUrl,
  reScheduleChangeUrl,
  updateChangeDetailUrl
} from '../constants/api-links';
import { apiService } from './api.service';

class ChangeService {
  getLastUpdatedChanges() {
    return apiService.post<{ changeSetList: IChangeDetail[]; total: number }>(
      changeSetListUrl,
      {
        selectFields: CHANGE_LIST_FIELDS
      }
    );
  }
  getApprovalQueueList() {
    return apiService.post<{ changeSetList: IChangeDetail[]; total: number }>(
      approvalQueueListUrl,
      {
        selectFields: CHANGE_LIST_FIELDS
      }
    );
  }
  getChangeDetail(changeId: string) {
    return apiService.post(changeDetailsUrl, {
      selectFields: CHANGE_DETAIL_SNAPSHOT_FIELDS,
      searchFields: {
        changeSetId: changeId
      }
    });
  }

  getPricingMethods(changeId: string) {
    return apiService.post(pricingMethodListlUrl, {
      selectFields: CHANGE_DETAIL_PRICING_METHODS_FIELDS,
      searchFields: {
        changeSetId: changeId
      }
    });
  }

  getChangeDetailPortfolios() {
    return apiService.post(portfolioListUrl, {
      selectFields: [
        'id',
        'name',
        'icon',
        'portfolioCriteria',
        'portfolioStats'
      ]
    });
  }

  getChangePricingStrategies(changeId: string) {
    return apiService.post(pricingStrategyListUrl, {
      selectFields: CHANGE_DETAIL_PRICING_STRATEGIES_FIELDS,
      searchFields: {
        changeSetId: changeId
      }
    });
  }

  getChangeAttachmentList(changeId: string) {
    return apiService.post(changeAttachmentListUrl, {
      selectFields: CHANGSET_ATTACHMENT_FIELDS,
      searchFields: {
        changeSetId: changeId
      }
    });
  }

  getChangeOwners(filter?: IBaseRequestModel) {
    return apiService.post<{ users: RefData[]; total: number }>(
      changeOwnerListUrl,
      filter
    );
  }
  getDateInformation(filter?: IBaseRequestModel) {
    return apiService.post<Record<string, any>>(dateInformationUrl, filter);
  }
  updateChangeDetail(
    filter?: IBaseRequestModel & {
      changeSetId: string;
      changeName?: string;
      changeOwnerId?: string;
      effectiveDate?: string;
      description?: string;
    }
  ) {
    return apiService.post<Record<string, any>>(updateChangeDetailUrl, filter);
  }
  rescheduleChange(changeId: string, effectiveDate: string) {
    const body = {
      changeSetId: changeId,
      effectiveDate
    } as IBaseRequestModel;
    return apiService.post(reScheduleChangeUrl, body);
  }

  approveChange(changeId: string) {
    const body = {
      changeSetId: changeId,
      action: CHANGE_ACTION.Approve
    } as IBaseRequestModel;
    return apiService.post(processChangeUrl, body);
  }

  submitChange(changeId: string) {
    const body = {
      changeSetId: changeId,
      action: CHANGE_ACTION.Submit
    } as IBaseRequestModel;
    return apiService.post(processChangeUrl, body);
  }

  rejectChange(changeId: string, reason: string) {
    const body = {
      changeSetId: changeId,
      action: CHANGE_ACTION.Reject,
      reason
    } as IBaseRequestModel;
    return apiService.post(processChangeUrl, body);
  }

  demoteChange(changeId: string, reason: string) {
    const body = {
      changeSetId: changeId,
      action: CHANGE_ACTION.Demote,
      reason
    } as IBaseRequestModel;
    return apiService.post(processChangeUrl, body);
  }

  validateChange(changeId: string) {
    const body = {
      changeSetId: changeId,
      action: CHANGE_ACTION.Validate
    } as IBaseRequestModel;
    return apiService.post(processChangeUrl, body);
  }

  deleteChange(changeId: string) {
    return apiService.post(deleteChangeUrl, {
      changeSetId: changeId
    });
  }

  exportChangeSet(changeId: string) {
    return apiService.post(exportChangeSetUrl, {
      changeSetId: changeId
    });
  }

  getDMMTables(changeId: string) {
    return apiService.post(dmmListUrl, {
      selectFields: DMM_LIST_FIELDS,
      searchFields: {
        changeSetId: changeId
      }
    });
  }

  downloadDecisionElement(dmmTableId: string) {
    return apiService.post(downloadDecisionElementsUrl, {
      selectFields: DOWNLOAD_ATTACHMENT_FIELDS,
      searchFields: {
        dmmTableId
      }
    });
  }

  downloadAttachment(attachmentId: string, workflowInstanceId: string) {
    return apiService.post(downloadAttachmentUrl, {
      selectFields: DOWNLOAD_ATTACHMENT_FIELDS,
      searchFields: {
        fileId: attachmentId
      }
    });
  }

  pendingApprovalGroupList(changeSetId: string) {
    return apiService.post(pendingApprovalGroupUrl, {
      selectFields: PENDING_APPROVAL_GROUP_FIELDS,
      searchFields: {
        changeSetId
      }
    });
  }

  createChangeSet(request: CreateChangeSet) {
    return apiService.post(createChangeSetUrl, request);
  }

  getWorkflowChangeSetList(workflowTemplateId: string | undefined) {
    return apiService.post(getWorkflowChangeSetListUrl, {
      searchFields: {
        workflowTemplateId
      }
    });
  }

  getWorkflowChangeList(workflowTemplateId: string | undefined) {
    return apiService.post(changeSetListUrl, {
      searchFields: {
        workflowTemplateId
      }
    });
  }

  getTableList(tableType: string, serviceNameAbbreviation: string) {
    return apiService.post(getTableListUrl, {
      searchFields: {
        tableType,
        serviceNameAbbreviation
      }
    });
  }

  getDecisionList(tableType: string) {
    return apiService.post(getDecisionListUrl, {
      searchFields: {
        tableType,
        serviceNameAbbreviation: 'ALP'
      }
    });
  }

  getStrategiesListAQ() {
    return apiService.post(getStrategiesListAQ, {
      searchFields: {
        strategyArea: 'CP'
      }
    });
  }

  getDecisionElementList(tableType: string, serviceNameAbbreviation: string) {
    return apiService.post(getDecisionElementUrl, {
      searchFields: {
        tableType: tableType,
        serviceNameAbbreviation: serviceNameAbbreviation
      }
    });
  }

  getLetters() {
    return apiService.post(getLettersUrl, {
      selectFields: ['letter']
    });
  }
}

export const changeService = new ChangeService();
