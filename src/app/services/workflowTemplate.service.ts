import { AxiosRequestConfig } from 'axios';
import {
  deleteTemplateFileURL,
  downloadTemplateFileUrl,
  getFileDetailsURL,
  uploadTemplateFileURL,
  workflowTemplateList,
  WORKFLOW_TEMPLATE_FIELDS
} from '../constants/api-links';
import { apiService } from './api.service';

class WorkflowTemplateService {
  getFileDetails(postData: any) {
    return apiService.post(getFileDetailsURL, postData);
  }
  deleteWorkflowTemplate(postData: any) {
    return apiService.post(deleteTemplateFileURL, postData);
  }
  uploadWorkflowTemplate(postData: any, options: AxiosRequestConfig) {
    return apiService.post(uploadTemplateFileURL, postData, options);
  }
  downloadWorkflowTemplate(dataPost: any) {
    return apiService.post(downloadTemplateFileUrl, dataPost);
  }
  getWorkflowTemplates(filter?: IBaseRequestModel) {
    return apiService.post<{
      workflowTemplateList: IWorkflowTemplateModel[];
      total: number;
    }>(workflowTemplateList, {
      searchFields: filter?.searchFields,
      selectFields: WORKFLOW_TEMPLATE_FIELDS
    });
  }
}

export const workflowTemplateService = new WorkflowTemplateService();
