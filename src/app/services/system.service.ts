import { appSetting } from '../constants/configurations';
import { apiService } from './api.service';

const config = {
  baseURL: appSetting.publicUrl
};

class SystemService {
  getConfig() {
    const mainUrl = 'config/main.json';
    return apiService.get<AppConfiguration>(mainUrl, config);
  }
  getReactSettingConfig() {
    const mainUrl = 'config/react-settings.json';
    return apiService.get<ReactSetting>(mainUrl, config);
  }
  getMapping() {
    const url = window.appConfig?.mappingUrl || '';
    return apiService.get(url, config);
  }
  getWorkflowMapping() {
    const url = window.appConfig?.workflowMappingUrl || '';
    return apiService.get(url, config);
  }
}

export const systemService = new SystemService();
