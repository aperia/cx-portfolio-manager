import {
  addPortfolioKPIUrl,
  deletePortfolioKPIUrl,
  getKPIDetailsUrl,
  getKPIListForSetupUrl,
  portfolioDetailUrl,
  portfolioListUrl,
  PORTFOLIO_LIST_FIELDS,
  updatePortfolioKPIUrl
} from '../constants/api-links';
import { apiService } from './api.service';

class PortfolioService {
  getHomePortfolio() {
    return apiService.post<{ portfolioList: IPortfolioModel[]; total: number }>(
      portfolioListUrl,
      {
        selectFields: PORTFOLIO_LIST_FIELDS
      }
    );
  }

  getPortfolioList(changeId?: string) {
    let requestPayload: any = {
      selectFields: PORTFOLIO_LIST_FIELDS
    };
    requestPayload = changeId
      ? { ...requestPayload, searchFields: { changeSetId: changeId } }
      : { ...requestPayload };
    return apiService.post<{ portfolioList: IPortfolioModel[]; total: number }>(
      portfolioListUrl,
      requestPayload
    );
  }

  getPortfolioDetail(portfolioId: string) {
    return apiService.post(portfolioDetailUrl, {
      searchFields: {
        portfolioId
      }
    });
  }

  getKPIListForSetup() {
    return apiService.post(getKPIListForSetupUrl, {});
  }

  getKPIDetails(kpiId: number) {
    return apiService.post(getKPIDetailsUrl, {
      searchFields: {
        kpiId,
        timezoneOffset: -new Date().getTimezoneOffset() / 60
      }
    });
  }

  addPortfolioKPI(portfolioDetails: IPortfolioKpiDetails) {
    return apiService.post(addPortfolioKPIUrl, {
      portfolioKpiDetails: portfolioDetails
    });
  }

  updatePortfolioKPI(portfolioDetails: IPortfolioKpiUpdate) {
    return apiService.post(updatePortfolioKPIUrl, {
      portfolioKpiDetails: portfolioDetails
    });
  }

  deletePortfolioKPI(kpiId: number) {
    return apiService.post(deletePortfolioKPIUrl, {
      searchFields: {
        kpiId
      }
    });
  }
}

export const portfolioService = new PortfolioService();
