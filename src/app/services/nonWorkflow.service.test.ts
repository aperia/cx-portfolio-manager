import * as apiService from 'app/services/api.service';
import { nonWorkflowService } from './nonWorkflow.service';

const apiPost = jest.spyOn(apiService.apiService, 'post');

describe('activity service ', () => {
  beforeEach(() => {
    apiPost.mockImplementation(
      () =>
        new Promise(resolve => {
          resolve({});
        })
    );
  });

  it('should getServiceSubjectSectionOptions success', async () => {
    const response = await nonWorkflowService.getServiceSubjectSectionOptions();
    expect(response).toEqual({});
  });

  it('should getServiceSubjectSectionOptions success', async () => {
    const response = await nonWorkflowService.getElementDataByName(
      'workflowTemplateId',
      ['']
    );
    expect(response).toEqual({});
  });
});
