import * as apiService from 'app/services/api.service';
import { notificationService } from 'app/services/notification.service';

const apiPost = jest.spyOn(apiService.apiService, 'post');

describe('notification service ', () => {
  beforeEach(() => {
    apiPost.mockImplementation(
      () =>
        new Promise(resolve => {
          resolve({});
        })
    );
  });

  it('should getLastUpdatedChanges success', async () => {
    const response = await notificationService.getNotifications();
    expect(response).toEqual({});
  });
  it('should getLastUpdatedApprovalQueues success', async () => {
    const response = await notificationService.updateNotification([]);
    expect(response).toEqual({});
  });
});
