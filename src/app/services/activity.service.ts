import { ACTIVITY_LIST_FIELDS, getActivitiesUrl } from '../constants/api-links';
import { apiService } from './api.service';

class ActivityService {
  getActivitiesInChange(changeId: string) {
    return apiService.post<{ activityList: IActivity[]; total: number }>(
      getActivitiesUrl,
      {
        selectFields: ACTIVITY_LIST_FIELDS,
        searchFields: {
          changeSetId: changeId
        }
      }
    );
  }
}

export const activityService = new ActivityService();
