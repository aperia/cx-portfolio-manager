import {
  getNotificationsUrl,
  NOTIFICATION_LIST_FIELDS,
  updateNotificationUrl
} from 'app/constants/api-links';
import { apiService } from 'app/services';

class NotificationService {
  getNotifications() {
    return apiService.post<{ notifications: INotification[]; total: number }>(
      getNotificationsUrl,
      {
        selectFields: NOTIFICATION_LIST_FIELDS
      }
    );
  }

  updateNotification(notifications: any[]) {
    return apiService.post<{ total: number; messages: any[] }>(
      updateNotificationUrl,
      {
        notifications
      }
    );
  }
}

export const notificationService = new NotificationService();
