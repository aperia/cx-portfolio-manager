import { apiService, InterceptorsInstance } from 'app/services/api.service';
import mockUrl from 'app/utils/_mockHelperConstant/mockUrl';
import Axios from 'axios';

const mockIsCancel = jest.spyOn(Axios, 'isCancel');

const mockState = jest.fn();
jest.mock('app/_libraries/_dof/core/redux/createAppStore', () => ({
  dispatch: jest.fn(),
  getState: () => mockState()
}));

describe('api service ', () => {
  beforeEach(() => {
    mockState.mockReturnValue({ workflowSetup: { workflowFormSteps: {} } });
  });

  describe('default intercepttors', () => {
    const requestHandler = (apiService.interceptors.request as any).handlers[0];
    const responseHandler = (apiService.interceptors.response as any)
      .handlers[0];

    beforeEach(() => {
      mockUrl.MOCK_API_URL.length = 0;
    });

    it('should handle request success', async () => {
      process.env.REACT_APP_API_MOCK = 'baseUrl';

      // add Authorization
      let result = await requestHandler.fulfilled({
        headers: {},
        url: mockUrl.MOCK_API_URL
      });
      expect(result.headers).toEqual({ Authorization: '' });

      // change baseURL
      result = await requestHandler.fulfilled({
        headers: {},
        url: 'link'
      });
      expect(result.baseURL).toEqual(undefined);

      // add common request data
      result = await requestHandler.fulfilled({ headers: {}, data: {} });
      expect(Object.keys(result.data).length).toEqual(29);
    });

    it('should handle request success with form data', async () => {
      process.env.REACT_APP_API_MOCK = 'baseUrl';

      // add Authorization
      let result = await requestHandler.fulfilled({
        headers: {},
        url: 'link',
        data: new FormData()
      });
      expect(result.baseURL).toEqual(undefined);

      // add common request data
      result = await requestHandler.fulfilled({ headers: {}, data: {} });
      expect(Object.keys(result.data).length).toEqual(29);
    });

    it('should handle request success => MOCK_API_URL', async () => {
      mockUrl.MOCK_API_URL.push('link');
      process.env.REACT_APP_API_MOCK = 'baseUrl';

      // add Authorization
      let result = await requestHandler.fulfilled({
        headers: {},
        url: mockUrl.MOCK_API_URL
      });
      expect(result.headers).toEqual({ Authorization: '' });

      // add common request data
      result = await requestHandler.fulfilled({ headers: {}, data: {} });
      expect(Object.keys(result.data).length).toEqual(29);
    });

    it('should handle request failure', async () => {
      try {
        await requestHandler.rejected({ request: {} });
      } catch (result) {
        expect(result).toEqual({});
      }
    });

    it('should handle response success', async () => {
      const result = await responseHandler.fulfilled({ field: '' });
      expect(result).toEqual({ field: '' });
    });

    it('should handle show change not found error modal', async () => {
      try {
        await responseHandler.fulfilled({
          config: { url: mockUrl.updateWorkflowInstanceUrl },
          data: { status: 'NOT_FOUND' }
        });
      } catch (result) {
        expect(result.config.url).toEqual(mockUrl.updateWorkflowInstanceUrl);
      }
    });

    it('should auto update store of workflowInstanceDetails', async () => {
      const result = await responseHandler.fulfilled({
        config: { url: mockUrl.updateWorkflowInstanceDataUrl },
        data: { workflowInstanceDetails: {} }
      });
      expect(result.config.url).toEqual(mockUrl.updateWorkflowInstanceDataUrl);
    });

    it('should handle response failure', async () => {
      try {
        await responseHandler.rejected({ response: { error: 'message' } });
      } catch (result) {
        expect(result).toEqual({ error: 'message' });
      }

      mockIsCancel.mockImplementation(() => true);

      try {
        await responseHandler.rejected({
          response: { error: 'message' }
        });
      } catch (result) {
        expect(result).toEqual({ isCancel: true });
      }
    });

    it('should handle response failure', async () => {
      try {
        await responseHandler.rejected({
          config: { url: mockUrl.updateWorkflowInstanceUrl },
          response: { status: 500, error: 'message' }
        });
      } catch (result) {
        expect(result.status).toEqual(500);
      }

      mockState.mockReturnValue({
        workflowSetup: {
          workflowFormSteps: { selectedStep: 'changeInformation' }
        }
      });
      try {
        await responseHandler.rejected({
          config: { url: mockUrl.updateWorkflowInstanceUrl },
          response: { status: 500, error: 'message' }
        });
      } catch (result) {
        expect(result.status).toEqual(500);
      }
    });
  });

  describe('custom intercepttors', () => {
    it('should handle request/response success/failure', async () => {
      const interceptors: InterceptorsInstance = {
        request: {
          success: jest.fn(),
          failure: jest.fn()
        },
        response: {
          success: jest.fn(),
          failure: jest.fn()
        }
      };
      apiService.setupAxiosInterceptors!(interceptors);
      const requestHandler = (apiService.interceptors.request as any)
        .handlers[0];
      const responseHandler = (apiService.interceptors.response as any)
        .handlers[0];

      await requestHandler.fulfilled({});
      expect(interceptors.request.success).toBeCalled();

      try {
        await requestHandler.rejected({});
      } catch {
        expect(interceptors.request.failure).toBeCalled();
      }

      await responseHandler.fulfilled({});
      expect(interceptors.response.success).toBeCalled();

      try {
        await responseHandler.rejected({});
      } catch {
        expect(interceptors.response.failure).toBeCalled();
      }
    });
  });
});
