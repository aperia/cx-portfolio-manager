import { apiService } from './api.service';

class RefDataService {
  getStateRefData() {
    const url = window.appConfig?.api?.refData?.getStateRefData || '';
    return apiService.post(url, {});
  }
}

export const refDataService = new RefDataService();
