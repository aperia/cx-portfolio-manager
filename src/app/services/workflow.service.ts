import axios, { CancelTokenSource } from 'axios';
import { IWorkflowReminder } from 'pages/_commons/redux/Workflow/async-thunk';
import { ExportAnalyzerArg } from 'pages/_commons/redux/WorkflowSetup/async-thunk';
import {
  changeSetListUrl,
  createWorkflowInstanceUrl,
  exportAnalyzerUrl,
  exportMerchantsUrl,
  exportShellProfileUrl,
  favoriteWorkflowUrl,
  getElementDataByNameUrl,
  getMerchantListUrl,
  getOCSShellListUrl,
  getOcsShellProfileUrl,
  getPricingStrategyListForWorkflowSetupUrl,
  getProfileTransactionsUrl,
  getQueueProfileUrl,
  getServiceSubjectSectionOptionsUrl,
  getSPAListUrl,
  getSupervisorListUrl,
  getTableListForWorkflowSetupUrl,
  getTransactionListUrl,
  getWorkflowEffectiveDateOptionsURL,
  getWorkflowInstanceUrl,
  getWorkflowMethodsUrl,
  getWorkflowStrategiesUrl,
  getWorkflowTableAreaForArchiveUrl,
  getWorkflowTextAreaForArchiveUrl,
  inProgressWorkflowListUrl,
  performMonetaryAdjustmentTransactionUrl,
  removeWorkflowUrl,
  setReminderWorkflowUrl,
  updateWorkflowInstanceDataUrl,
  updateWorkflowInstanceUrl,
  workflowDetailsUrl,
  workflowListUrl,
  workflowTemplateList,
  WORKFLOW_INSTANCE_DETAIL_FIELDS,
  WORKFLOW_INSTANCE_FIELDS,
  WORKFLOW_TEMPLATE_FIELDS
} from '../constants/api-links';
import { apiService } from './api.service';

let cancelToken: CancelTokenSource | undefined;

class WorkflowService {
  getTemplateWorkflows() {
    return apiService.post<{
      workflowTemplateList: IWorkflowTemplateModel[];
      total: number;
    }>(workflowTemplateList, {
      searchFields: {},
      selectFields: WORKFLOW_TEMPLATE_FIELDS
    });
  }
  getWorkflowList(filter: IBaseRequestModel = {}) {
    if (cancelToken) {
      cancelToken.cancel('Operation canceled due to new request.');
    }

    cancelToken = axios.CancelToken?.source();

    const { searchFields, ...configParams } = filter;

    return apiService.post<{
      workflowInstanceList: IWorkflowModel[];
      total: number;
    }>(
      workflowListUrl,
      {
        searchFields: searchFields,
        selectFields: WORKFLOW_INSTANCE_FIELDS
      },
      {
        cancelToken: cancelToken?.token,
        ...configParams
      }
    );
  }
  getInProgressWorkflow() {
    return apiService.post<{
      workflowInstanceList: IWorkflowModel[];
      total: number;
    }>(inProgressWorkflowListUrl, {
      selectFields: WORKFLOW_INSTANCE_FIELDS
    });
  }
  getWorkflowDetails(filter?: IBaseRequestModel) {
    return apiService.post<{ workflowInstanceDetails: IWorkflowDetail }>(
      workflowDetailsUrl,
      {
        searchFields: filter?.searchFields,
        selectFields: WORKFLOW_INSTANCE_DETAIL_FIELDS
      }
    );
  }
  removeWorkflow(workflowId: string) {
    const body = {
      workflowInstanceId: workflowId
    } as IBaseRequestModel;
    return apiService.post<{ total: number; messages: any[] }>(
      removeWorkflowUrl,
      body
    );
  }
  favoriteWorkflowTemplate(workflowId: string) {
    const body = {
      workflowTemplateId: workflowId,
      isFavorite: true
    };
    return apiService.post<{ total: number; messages: any[] }>(
      favoriteWorkflowUrl,
      body
    );
  }
  unfavoriteWorkflowTemplate(workflowId: string) {
    const body = {
      workflowTemplateId: workflowId,
      isFavorite: false
    };
    return apiService.post<{ total: number; messages: any[] }>(
      favoriteWorkflowUrl,
      body
    );
  }
  setReminderWorkflow(reminder: IWorkflowReminder) {
    const { workflowTemplateId, ...rest } = reminder;
    const body = {
      workflowTemplateId,
      notification: rest
    };
    return apiService.post<{ total: number; messages: any[] }>(
      setReminderWorkflowUrl,
      body
    );
  }

  createWorkflowInstance(request: CreateWorkflowInstance) {
    return apiService.post(createWorkflowInstanceUrl, request);
  }

  updateWorkflowInstance(request: UpdateflowInstanceDetail) {
    return apiService.post(updateWorkflowInstanceUrl, request);
  }

  getWorkflowInstance(id: string) {
    return apiService.post(getWorkflowInstanceUrl, {
      searchFields: { workflowInstanceId: `${id}` }
    });
  }

  updateWorkflowInstanceData(request: UpdateflowInstanceData) {
    return apiService.post(updateWorkflowInstanceDataUrl, request);
  }

  getElementDataByName(workflowTemplateId: string, elementName: string[]) {
    const request = {
      searchFields: {
        workflowTemplateId,
        elementName
      }
    };
    return apiService.post(getElementDataByNameUrl, request);
  }

  getServiceSubjectSectionOptions() {
    return apiService.post(getServiceSubjectSectionOptionsUrl, {});
  }

  exportAnalyzer(searchFields: ExportAnalyzerArg) {
    return apiService.post(exportAnalyzerUrl, {
      searchFields
    });
  }

  getTableListForWorkflowSetup(TableType: string) {
    const request = {
      searchFields: {
        TableType
      }
    };
    return apiService.post(getTableListForWorkflowSetupUrl, request);
  }

  getWorkflowMethods(serviceSubjectSection: string | string[]) {
    const body = {
      searchFields: {
        serviceSubjectSection:
          typeof serviceSubjectSection === 'string'
            ? [serviceSubjectSection]
            : serviceSubjectSection
      }
    };

    return apiService.post<{
      methodList: IMethodModel[];
      total: number;
    }>(getWorkflowMethodsUrl, body);
  }

  getWorkflowStrategies(strategyArea: string) {
    const body = {
      searchFields: {
        strategyArea
      }
    };

    return apiService.post<{
      pricingStrategies: any[];
      total: number;
    }>(getWorkflowStrategiesUrl, body);
  }

  getTextIDListForArchive(textArea: string) {
    const body = {
      searchFields: {
        textArea
      }
    };

    return apiService.post<{
      textIDs: any[];
      total: number;
    }>(getWorkflowTextAreaForArchiveUrl, body);
  }

  getTableListForArchive(tableArea: string) {
    const body = {
      searchFields: {
        tableArea
      }
    };

    return apiService.post<{
      tables: any[];
      total: number;
    }>(getWorkflowTableAreaForArchiveUrl, body);
  }

  getPricingStrategyListForWorkflowSetup(strategyArea: string) {
    return apiService.post(getPricingStrategyListForWorkflowSetupUrl, {
      searchFields: {
        strategyArea
      }
    });
  }

  getWorkflowEffectiveDateOptions(dataPost: MagicKeyValue) {
    return apiService.post(getWorkflowEffectiveDateOptionsURL, dataPost);
  }

  getPerformMonetaryAdjustmentTransaction(type: string) {
    return apiService.post(performMonetaryAdjustmentTransactionUrl, { type });
  }

  getWorkflowTransactionList(dataPost: any) {
    return apiService.post(getTransactionListUrl, dataPost);
  }

  getWorkflowTransactionListCondition(dataPost: any) {
    return apiService.post(getTransactionListUrl, dataPost);
  }

  getWorkflowChangeList(dataPost: any) {
    return apiService.post(changeSetListUrl, dataPost);
  }

  getWorkflowSPAList(dataPost: any) {
    return apiService.post(getSPAListUrl, dataPost);
  }

  getOCSShellList(dataPost: any) {
    return apiService.post(getOCSShellListUrl, dataPost);
  }

  getWorkflowMerchantList(sysPrin: string) {
    return apiService.post(getMerchantListUrl, { searchFields: { sysPrin } });
  }

  exportMerchants(sysPrin: string) {
    return apiService.post(exportMerchantsUrl, { searchFields: { sysPrin } });
  }

  getQueueProfile(queueOwner: string) {
    return apiService.post(getQueueProfileUrl, {
      searchFields: { queueOwner }
    });
  }

  getOcsShellProfile(shellName: string) {
    return apiService.post(getOcsShellProfileUrl, {
      searchFields: { shellName }
    });
  }

  exportShellProfile(shellName: string) {
    return apiService.post(exportShellProfileUrl, {
      searchFields: { shellName }
    });
  }

  getProfileTransactions(profile: string) {
    return apiService.post(getProfileTransactionsUrl, {
      searchFields: { profile }
    });
  }

  getSupervisorList(dataPost: any) {
    return apiService.post(getSupervisorListUrl, dataPost);
  }
}

export const workflowService = new WorkflowService();
