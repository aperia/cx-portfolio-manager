import * as apiService from 'app/services/api.service';
import { changeService } from 'app/services/change.service';

const apiPost = jest.spyOn(apiService.apiService, 'post');

const createChangeSetTemp = {
  workflowTemplateId: 'string',
  changeSetDetails: {
    name: 'string',
    description: 'string',
    effectiveDate: 'string'
  }
};

describe('change service ', () => {
  beforeEach(() => {
    apiPost.mockImplementation(
      () =>
        new Promise(resolve => {
          resolve({});
        })
    );
  });

  it('should getLastUpdatedChanges success', async () => {
    const response = await changeService.getLastUpdatedChanges();
    expect(response).toEqual({});
  });
  it('should getLastUpdatedApprovalQueues success', async () => {
    const response = await changeService.getApprovalQueueList();
    expect(response).toEqual({});
  });
  it('should getChangeDetail success', async () => {
    const response = await changeService.getChangeDetail('id');
    expect(response).toEqual({});
  });
  it('should getPricingMethods success', async () => {
    const response = await changeService.getPricingMethods('id');
    expect(response).toEqual({});
  });
  it('should getChangeDetailPortfolios success', async () => {
    const response = await changeService.getChangeDetailPortfolios();
    expect(response).toEqual({});
  });
  it('should getChangePricingStrategies success', async () => {
    const response = await changeService.getChangePricingStrategies('id');
    expect(response).toEqual({});
  });
  it('should getChangeOwners success', async () => {
    const response = await changeService.getChangeOwners({});
    expect(response).toEqual({});
  });
  it('should getDateInformation success', async () => {
    const response = await changeService.getDateInformation({});
    expect(response).toEqual({});
  });
  it('should updateChangeDetail success', async () => {
    const response = await changeService.updateChangeDetail({} as any);
    expect(response).toEqual({});
  });
  it('should rescheduleChange success', async () => {
    const response = await changeService.rescheduleChange('id', 'date');
    expect(response).toEqual({});
  });
  it('should getChangeAttachmentList success', async () => {
    const response = await changeService.getChangeAttachmentList('id');
    expect(response).toEqual({});
  });
  it('should approveChange success', async () => {
    const response = await changeService.approveChange('id');
    expect(response).toEqual({});
  });
  it('should submitChange success', async () => {
    const response = await changeService.submitChange('id');
    expect(response).toEqual({});
  });
  it('should rejectChange success', async () => {
    const response = await changeService.rejectChange('id', 'reason');
    expect(response).toEqual({});
  });
  it('should demoteChange success', async () => {
    const response = await changeService.demoteChange('id', 'reason');
    expect(response).toEqual({});
  });
  it('should validateChange success', async () => {
    const response = await changeService.validateChange('id');
    expect(response).toEqual({});
  });
  it('should deleteChange success', async () => {
    const response = await changeService.deleteChange('id');
    expect(response).toEqual({});
  });
  it('should exportChangeSet success', async () => {
    const response = await changeService.exportChangeSet('id');
    expect(response).toEqual({});
  });
  it('should getDMMTables success', async () => {
    const response = await changeService.getDMMTables('id');
    expect(response).toEqual({});
  });
  it('should downloadDecisionElement success', async () => {
    const response = await changeService.downloadDecisionElement('id');
    expect(response).toEqual({});
  });
  it('should downloadAttachment success', async () => {
    const response = await changeService.downloadAttachment(
      'id',
      'workflowInstanceId'
    );
    expect(response).toEqual({});
  });
  it('should pendingApprovalGroupList success', async () => {
    const response = await changeService.pendingApprovalGroupList('id');
    expect(response).toEqual({});
  });

  it('should pendingApprovalGroupList success', async () => {
    const response = await changeService.createChangeSet(createChangeSetTemp);
    expect(response).toEqual({});
  });

  it('should pendingApprovalGroupList success', async () => {
    const response = await changeService.getWorkflowChangeSetList('id');
    expect(response).toEqual({});
  });

  it('should getTableList success', async () => {
    const response = await changeService.getTableList(
      'testType',
      'serviceNameAbbreviation'
    );
    expect(response).toEqual({});
  });

  it('should getLetters success', async () => {
    const response = await changeService.getLetters();
    expect(response).toEqual({});
  });

  it('should getDecisionList success', async () => {
    const response = await changeService.getDecisionList('tableType');
    expect(response).toEqual({});
  });

  it('should getDecisionElementList success', async () => {
    const response = await changeService.getDecisionElementList(
      'tableType',
      'serviceNameAbbreviation'
    );
    expect(response).toEqual({});
  });

  it('should getWorkflowChangeList success', async () => {
    const response = await changeService.getWorkflowChangeList('');
    expect(response).toEqual({});
  });

  it('should getStrategiesListAQ success', async () => {
    const response = await changeService.getStrategiesListAQ();
    expect(response).toEqual({});
  });
});
