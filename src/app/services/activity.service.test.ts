import { activityService } from 'app/services/activity.service';
import * as apiService from 'app/services/api.service';

const apiPost = jest.spyOn(apiService.apiService, 'post');

describe('activity service ', () => {
  beforeEach(() => {
    apiPost.mockImplementation(
      () =>
        new Promise(resolve => {
          resolve({});
        })
    );
  });

  it('should getLastUpdatedActivities success', async () => {
    const response = await activityService.getActivitiesInChange('10000001');
    expect(response).toEqual({});
  });
  it('should getActivitiesInChange success', async () => {
    const response = await activityService.getActivitiesInChange('changeId');
    expect(response).toEqual({});
  });
});
