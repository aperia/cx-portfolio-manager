import * as UseModalRegistry from 'app/hooks/useModals';
import * as ReactRedux from 'react-redux';

export const mockUseDispatchFnc = () => jest.spyOn(ReactRedux, 'useDispatch');

export const mockUseSelectorFnc = () => jest.spyOn(ReactRedux, 'useSelector');

export const mockReactRouterDOMFnc = () =>
  jest.mock('react-router-dom', () => ({
    useHistory: () => ({ push: jest.fn() }),
    useLocation: () => ({}),
    useParams: () => ({})
  }));

export const mockUseModalRegistryFnc = () =>
  jest.spyOn(UseModalRegistry, 'useModalRegistry');
