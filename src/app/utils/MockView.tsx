import React, {
  forwardRef,
  RefForwardingComponent,
  useImperativeHandle
} from 'react';

// types
import { ViewProps } from 'app/_libraries/_dof/core';
import { FormInstance } from 'redux-form';
import isFunction from 'lodash.isfunction';

export const storeIdViewNull = '123456789-view-ref-null';
export const storeIdViewNoValue = '123456789-view-no-value';
export const storeIdViewNotFound = '123456789-view-not-found';

const MockView: RefForwardingComponent<
  FormInstance<any, React.PropsWithChildren<any>, string>,
  ViewProps
> = ({ value, formKey, descriptor }, ref) => {
  const onFind = (str: string) => {
    if (formKey.includes(storeIdViewNotFound)) return null;

    const onFindProps = {
      props: {
        setData: () => undefined,
        setValue: () => undefined,
        setVisible: () => undefined,
        setEnable: () => undefined,
        setReadOnly: () => undefined,
        setRequired: () => undefined,
        setOthers: (param: any) => {
          if (isFunction(param)) return param({});
          return;
        }
      }
    };

    if (formKey.includes(storeIdViewNoValue))
      return {
        props: onFindProps
      };

    return {
      value,
      props: onFindProps
    };
  };

  const current = {
    props: { onFind },
    lastFieldValidatorKeys: ['lastFieldValidatorKeys']
  };

  const refResult = {
    ...current,
    ref: { current }
  };

  useImperativeHandle(ref, () => {
    return formKey.includes(storeIdViewNull)
      ? ((null as unknown) as FormInstance<
          any,
          React.PropsWithChildren<any>,
          string
        >)
      : ((refResult as unknown) as FormInstance<
          any,
          React.PropsWithChildren<any>,
          string
        >);
  });

  return <div data-testid={descriptor}>{JSON.stringify(value)}</div>;
};

export default forwardRef<
  FormInstance<any, React.PropsWithChildren<any>, string>,
  ViewProps
>(MockView);
