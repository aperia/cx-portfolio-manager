import {
  act,
  fireEvent,
  render,
  RenderOptions,
  RenderResult
} from '@testing-library/react';
import * as Modals from 'app/hooks/useModals';
import * as TranslationHook from 'app/_libraries/_dls/hooks/useTranslation';
import * as CommonRedux from 'pages/_commons/redux/Common/select-hooks';
import React from 'react';

export const renderComponent = (
  testId: string,
  element: React.ReactElement,
  renderOptions?: RenderOptions
): Promise<RenderResult> => {
  return new Promise(resolve => {
    const Wrapper: React.FC = ({ children, ...props }) => (
      <div data-testid={testId} {...props}>
        {children}
      </div>
    );

    act(() => {
      resolve(render(element, { wrapper: Wrapper, ...renderOptions }));
    });
  });
};

export const mockUseSelectWindowDimension = (width: number) => {
  jest
    .spyOn(CommonRedux, 'useSelectWindowDimension')
    .mockImplementation(() => ({ width } as any));
};

export const spyOnUseRef = (refPayload: any) => {
  jest.spyOn(React, 'useRef').mockReturnValue(refPayload);
};

export const spyOnTranslationHoook = () =>
  jest.spyOn(TranslationHook, 'default').mockImplementation(() => ({
    t: (val: string) => val
  }));

export const spyOnUseModalRegistry = (show: boolean) => {
  jest.spyOn(Modals, 'useModalRegistry').mockImplementation(() => [show, true]);
};

export const changeDlsDropdownMock = (
  filterIdx: number,
  itemIdx: number,
  wrapper: any
) => {
  const element = wrapper
    .querySelectorAll('.dls-dropdown-list .text-field-container')
    .item(filterIdx);
  fireEvent.focus(element);

  const pageElement = document
    .querySelectorAll('.dls-popup .item')
    .item(itemIdx);
  fireEvent.click(pageElement);
};
