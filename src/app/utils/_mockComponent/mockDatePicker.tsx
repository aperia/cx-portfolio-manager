import { DatePickerChangeEvent } from 'app/_libraries/_dls';
import React from 'react';

const mockDateTooltipParams = jest.fn();
jest.mock('app/_libraries/_dls/components/DatePicker', () => {
  const actualModule = jest.requireActual(
    'app/_libraries/_dls/components/DatePicker'
  );
  return {
    ...actualModule,
    __esModule: true,
    default: ({ onChange, dateTooltip, label, onBlur, ...props }: any) => (
      <div>
        <span>{label}</span>
        <input
          {...props}
          onClick={() => {
            const params = mockDateTooltipParams();

            !!dateTooltip && dateTooltip(...params);
          }}
          onChange={(e: any) => {
            onChange!({
              target: { value: new Date(e.target.value), name: e.target.name }
            } as DatePickerChangeEvent);
          }}
          onBlur={onBlur}
        />
      </div>
    )
  };
});

export default mockDateTooltipParams;
