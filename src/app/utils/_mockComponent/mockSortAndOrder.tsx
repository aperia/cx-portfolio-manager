import { SortAndOrderProps } from 'app/components/SortAndOrder';
import React from 'react';

jest.mock('app/components/SortAndOrder', () => {
  return {
    __esModule: true,
    default: ({
      sortByFields,
      sortByValue,
      orderByValue,
      onSortByChange,
      onOrderByChange
    }: SortAndOrderProps) => {
      return (
        <div>
          <ul className="sort-by-fields">
            {sortByFields ? (
              sortByFields.map(val => (
                <li
                  key={val.value}
                  className={`sort-by-field-item ${
                    val.value === sortByValue?.value ? 'active' : ''
                  }`}
                >
                  {val.description} - {val.value}
                </li>
              ))
            ) : (
              <></>
            )}
          </ul>
          <ul className="order-by-fields">
            <li
              className={`order-by-field-item ${
                orderByValue?.value === 'asc' ? 'active' : ''
              }`}
            >
              {'Ascending'} - {'Asc'}
            </li>
            <li
              className={`order-by-field-item ${
                orderByValue?.value === 'desc' ? 'active' : ''
              }`}
            >
              {'Descending'} - {'Des'}
            </li>
          </ul>
          <div>
            <button
              className="change-sort-by"
              onClick={() =>
                onSortByChange!({
                  description: 'Field 1',
                  value: 'field1'
                })
              }
            >
              Change to field1
            </button>
            <button
              className="change-order-by"
              onClick={() =>
                onOrderByChange!({
                  description: 'Ascending',
                  value: 'asc'
                })
              }
            >
              Change to asc
            </button>
          </div>
        </div>
      );
    }
  };
});
