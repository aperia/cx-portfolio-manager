import { PaginationWrapperProps } from 'app/_libraries/_dls';
import React from 'react';

jest.mock('app/_libraries/_dls', () => {
  return {
    Pagination: ({
      pageNumber,
      pageSizeValue,
      totalItem,
      onChangePage,
      onChangePageSize
    }: PaginationWrapperProps) => {
      return (
        <div>
          <ul>
            <li className="page-number">{pageNumber}</li>
            <li className="page-size-value">{pageSizeValue}</li>
            <li className="total-item">{totalItem}</li>
          </ul>
          <div>
            <button
              className="change-page-number"
              onClick={() => onChangePage!(2)}
            >
              Change to page 2
            </button>
            <button
              className="change-page-size"
              onClick={() => onChangePageSize!({ size: 25, page: 1 })}
            >
              Change to size 25
            </button>
          </div>
        </div>
      );
    }
  };
});
