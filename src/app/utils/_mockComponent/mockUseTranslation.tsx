import React from 'react';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return {
    __esModule: true,
    default: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

jest.mock('app/_libraries/_dls/components/i18next/Trans', () => {
  return {
    __esModule: true,
    default: ({ keyTranslation }: any) => <div>{keyTranslation}</div>
  };
});

export {};
