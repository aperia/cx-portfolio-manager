import React from 'react';

jest.mock('pages/_commons/Utils/SortOrder', () => {
  return {
    __esModule: true,
    default: function SortOrder({
      dataFilter,
      onChangeSortBy,
      onChangeOrderBy,
      onClearSearch
    }: any) {
      return (
        <div>
          <p>Mock Sort Order</p>
          <button
            className="sort-order--sort-btn"
            onClick={() => onChangeSortBy('test sort value')}
          >
            Sort By
          </button>
          <button
            className="sort-order--order-btn"
            onClick={() => onChangeOrderBy('test sort value')}
          >
            Order By
          </button>
          <button className="sort-order--clear-btn" onClick={onClearSearch}>
            Clear And Reset
          </button>
        </div>
      );
    }
  };
});
