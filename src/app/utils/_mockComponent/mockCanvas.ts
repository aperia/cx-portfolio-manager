import { isString } from 'lodash';

HTMLCanvasElement.prototype.getContext = (() => ({
  measureText: (text: string) => ({
    width: isString(text) ? text.length * 50 : 0
  })
})) as any;
