import React from 'react';

jest.mock('app/components/FailedApiReload', () => {
  return {
    __esModule: true,
    default: ({ id, onReload }: any) => {
      return (
        <div id={id}>
          Data load unsuccessful
          <button onClick={onReload}>Reload</button>
        </div>
      );
    }
  };
});
