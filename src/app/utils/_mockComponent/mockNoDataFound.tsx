import React from 'react';

jest.mock('app/components/NoDataFound', () => {
  return {
    __esModule: true,
    default: ({ id, onLinkClicked }: any) => {
      return (
        <div id={id} onClick={onLinkClicked}>
          No data found
        </div>
      );
    }
  };
});
