import {
  DropdownBaseChangeEvent,
  DropdownBaseGroupProps,
  DropdownBaseItemProps
} from 'app/_libraries/_dls/components';
import { ComboBoxProps } from 'app/_libraries/_dls/components/ComboBox';
import React from 'react';

jest.mock('app/_libraries/_dls/components/ComboBox', () => {
  const actualModule = jest.requireActual(
    'app/_libraries/_dls/components/ComboBox'
  );
  return {
    ...actualModule,
    __esModule: true,
    default: MockComboBox
  };
});
const ActualReact = jest.requireActual('react');
const MockComboBox = ActualReact.forwardRef(
  (props: ComboBoxProps, ref: React.Ref<any>) => {
    const scrollContainerElement = document.createElement('div');
    if (props.id === 'has-loading-div') {
      const loadingDiv = document.createElement('div');
      loadingDiv.id = 'combobox_loading-more-div';
      scrollContainerElement.appendChild(loadingDiv);
    }
    ActualReact.useImperativeHandle(ref, () => ({
      dropdownBaseRef: {
        current: {
          scrollContainerElement
        }
      }
    }));
    const { onChange, label, onBlur, error, children, onFilterChange } = props;

    return (
      <div className="dls-input-container">
        <input
          data-testid="ComboBox.Input_onChange"
          onChange={(e: any) => {
            const value: RefData = e.target.value;
            onChange!({
              target: { value: value }
            } as DropdownBaseChangeEvent);
          }}
          onBlur={onBlur}
        />
        <input
          data-testid="ComboBox.Input_onFilter"
          onChange={(e: any) => {
            onFilterChange && onFilterChange(e);
          }}
          onBlur={onBlur}
        />
        {!!error && <p data-testid="ComboBox.Error">{error?.message}</p>}
        <span>{label}</span>
        <div>{children}</div>
      </div>
    );
  }
);

MockComboBox.Item = ({ value, label }: DropdownBaseItemProps) => (
  <div key={`${value}-${label}`} data-testid={`ComboBox.Item-${label}`}>
    {label}
  </div>
);

MockComboBox.Group = ({ children, label }: DropdownBaseGroupProps) => (
  <div key={`${label}`} data-testid={`ComboBox.Group-${label}`}>
    {children}
  </div>
);

export default MockComboBox;
