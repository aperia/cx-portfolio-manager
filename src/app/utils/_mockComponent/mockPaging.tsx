import React from 'react';

jest.mock('pages/_commons/Utils/Paging', () => {
  return {
    __esModule: true,
    default: ({
      page,
      pageSize,
      totalItem,
      onChangePage,
      onChangePageSize
    }: any) => {
      return (
        <div>
          <ul>
            <li className="page-number">{page}</li>
            <li className="page-size-value">{pageSize}</li>
            <li className="total-item">{totalItem}</li>
          </ul>
          <div>
            <button
              className="change-page-number"
              onClick={() => onChangePage(2)}
            >
              Page 2
            </button>
            <button
              className="change-page-size"
              onClick={() => onChangePageSize(25)}
            >
              Page Size 25
            </button>
          </div>
        </div>
      );
    }
  };
});
