import { ISimpleSearchProps } from 'app/components/SimpleSearch';
import React from 'react';

jest.mock('app/components/SimpleSearch', () => {
  return {
    __esModule: true,
    default: ({
      defaultValue = '',
      placeholder = '',
      maxLength = 50,
      className = 'simple-search',
      clearTooltip = 'Clear Search Criteria',
      popperElement,
      onSearch
    }: ISimpleSearchProps) => {
      return (
        <div>
          <input />
          <div onClick={() => onSearch('test text')}>Simple Search</div>
        </div>
      );
    }
  };
});
