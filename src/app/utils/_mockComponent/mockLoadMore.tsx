import { LoadMoreProps } from 'app/components/LoadMore';
import React from 'react';

jest.mock('app/components/LoadMore', () => {
  return {
    __esModule: true,
    default: ({ loading, onLoadMore, isEnd }: LoadMoreProps) => {
      return (
        <div className={`load-more-cont ${loading ? 'loading' : 0}`}>
          {isEnd && <span>End.</span>}
          <button className="btn-load-more" onClick={onLoadMore}>
            Load more...
          </button>
        </div>
      );
    }
  };
});
