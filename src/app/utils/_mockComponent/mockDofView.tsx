import React from 'react';

jest.mock('app/_libraries/_dof/core', () => {
  return {
    View: ({ formKey, descriptor, onClick }: any) => {
      return (
        <div id={formKey}>
          {formKey}
          <span>{descriptor}</span>
          <button onClick={onClick}>Click on {formKey}</button>
        </div>
      );
    }
  };
});
