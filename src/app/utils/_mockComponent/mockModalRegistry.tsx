import { ModalRegistryProps } from 'app/components/ModalRegistry';
import React from 'react';

jest.mock('app/components/ModalRegistry', () => {
  return {
    __esModule: true,
    default: ({ id, show, children, onAutoClosedAll }: ModalRegistryProps) => {
      return show ? (
        <div id={id}>
          <button onClick={onAutoClosedAll}>AutoClose</button>
          {children}
        </div>
      ) : (
        <></>
      );
    }
  };
});
