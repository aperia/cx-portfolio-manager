import { TimePickerChangeEvent } from 'app/_libraries/_dls/components/TimePicker/types';
import React from 'react';

jest.mock('app/_libraries/_dls/components/TimePicker', () => {
  const actualModule = jest.requireActual(
    'app/_libraries/_dls/components/TimePicker'
  );
  return {
    ...actualModule,
    __esModule: true,
    default: ({ onChange, label, onBlur, ...props }: any) => (
      <div>
        <span>{label}</span>
        <input
          {...props}
          onChange={(e: any) => {
            // hh:mm A
            const value = e.target.value.toString();
            onChange({
              target: {
                value: {
                  hour: value.substr(0, 2),
                  minute: value.substr(3, 2),
                  meridiem: value.substr(6, 2)
                }
              }
            } as TimePickerChangeEvent);
          }}
          onBlur={onBlur}
        />
      </div>
    )
  };
});
