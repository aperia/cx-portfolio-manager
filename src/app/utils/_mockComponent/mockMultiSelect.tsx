import {
  DropdownBaseChangeEvent,
  DropdownBaseItemProps
} from 'app/_libraries/_dls/components';
import { MultiSelectProps } from 'app/_libraries/_dls/components/MultiSelect';
import React from 'react';

jest.mock('app/_libraries/_dls/components/MultiSelect', () => {
  const actualModule = jest.requireActual(
    'app/_libraries/_dls/components/MultiSelect'
  );
  return {
    ...actualModule,
    __esModule: true,
    default: MockMultiSelect
  };
});
const MockMultiSelect = (props: MultiSelectProps) => {
  const { onChange, label, onBlur, error, children, value } = props;

  return (
    <div className="dls-input-container">
      <input
        data-testid="MultiSelect.Input_onChange"
        onChange={(e: any) =>
          onChange!({
            target: { value: [...value, e.target.value] }
          } as DropdownBaseChangeEvent)
        }
        onBlur={onBlur}
      />
      {!!error && <p data-testid="MultiSelect.Error">{error?.message}</p>}
      <span>{label}</span>
      <div>{children}</div>
    </div>
  );
};

MockMultiSelect.Item = ({ value, label }: DropdownBaseItemProps) => (
  <div key={`${value}-${label}`} data-testid={`ComboBox.Item-${label}`}>
    {label}
  </div>
);
