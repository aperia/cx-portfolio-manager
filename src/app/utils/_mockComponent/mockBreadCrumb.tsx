import React from 'react';

jest.mock('app/components/BreadCrumb', () => {
  return {
    __esModule: true,
    default: ({ id }: any) => {
      return (
        <div id={id}>
          <p>Mock BreadCrumb</p>
        </div>
      );
    }
  };
});
