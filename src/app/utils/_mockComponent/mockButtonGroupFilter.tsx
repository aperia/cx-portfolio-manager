import React from 'react';

jest.mock('pages/_commons/Utils/ButtonGroupFilter', () => {
  return {
    __esModule: true,
    default: ({ id, defaultSelectedValue, onButtonSelectedChange }: any) => {
      return (
        <div id={id}>
          <p>Mock Button Group Filter</p>
          <div>{defaultSelectedValue}</div>
          <button
            id="btn-group-1"
            onClick={() => onButtonSelectedChange('test value')}
          >
            Filter 1
          </button>
          <button id="btn-group-1" onClick={() => onButtonSelectedChange('')}>
            Filter Empty
          </button>
        </div>
      );
    }
  };
});
