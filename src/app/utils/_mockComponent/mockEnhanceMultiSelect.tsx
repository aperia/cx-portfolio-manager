import {
  DropdownBaseChangeEvent,
  DropdownBaseItemProps
} from 'app/_libraries/_dls/components';
import { MultiSelectProps } from 'app/_libraries/_dls/components/MultiSelect';
import React from 'react';

jest.mock('app/components/EnhanceMultiSelect', () => {
  const actualModule = jest.requireActual('app/components/EnhanceMultiSelect');
  return {
    ...actualModule,
    __esModule: true,
    default: MockEnhanceMultiSelect
  };
});
const MockEnhanceMultiSelect = (props: MultiSelectProps) => {
  const { onChange, label, onBlur, error, children, value } = props;

  return (
    <div className="dls-input-container">
      <input
        data-testid="MultiSelect.Input_onChange"
        onChange={(e: any) =>
          onChange!({
            target: { value: [...value, e.target.value] }
          } as DropdownBaseChangeEvent)
        }
        onBlur={onBlur}
      />
      {!!error && <p data-testid="MultiSelect.Error">{error?.message}</p>}
      <span>{label}</span>
      <div>{children}</div>
    </div>
  );
};

MockEnhanceMultiSelect.Item = ({ value, label }: DropdownBaseItemProps) => (
  <div key={`${value}-${label}`} data-testid={`ComboBox.Item-${label}`}>
    {label}
  </div>
);
