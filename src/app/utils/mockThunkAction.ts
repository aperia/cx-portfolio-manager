import dynamicMiddleware from 'app/_libraries/_dof/core/redux/dynamicMiddleware';

interface Options {
  match?: boolean;
  type?: string;
  payload?: any;
  callback?: (...args: any) => void;
}
export const mockThunkAction = (actionObj: any) => {
  dynamicMiddleware.addMiddleware();

  return (
    action: string,
    { match = true, callback, payload }: Options | undefined = {}
  ) => {
    const mock = jest.fn();
    actionObj[action] = mock;
    mock.mockImplementation(args => {
      actionObj[action].fulfilled = {
        match: () => match
      };

      callback && callback(args);
      return () => payload;
    });

    return mock;
  };
};
