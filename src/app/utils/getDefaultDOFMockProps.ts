interface dataProps {
  value?: any;
  id?: string;
  name?: string;
  meta?: {
    active?: boolean;
    asyncValidating?: boolean;
    autofilled?: boolean;
    initial?: any;
    form?: string;
    dirty?: boolean;
    dispatch?: any;
    invalid?: boolean;
    pristine?: boolean;
    submitFailed?: boolean;
    submitting?: boolean;
    touched?: boolean;
    valid?: boolean;
    visited?: boolean;
  };
  onChange?: (e: any) => void;
  onBlur?: () => void;
  onFocus?: () => void;
  options?: any;
  readOnly?: boolean;
  required?: boolean;
  mask?: Array<{
    isRegex: boolean;
    pattern: string;
  }>;
}

type mockPropsDOFType = (data?: dataProps) => any;

export const getDefaultDOFMockProps: mockPropsDOFType = (data?: dataProps) => {
  const defaultFunc = () => {};

  return {
    input: {
      value: data?.value || '',
      onChange: data?.onChange || defaultFunc,
      onBlur: data?.onBlur || defaultFunc,
      onFocus: data?.onFocus || defaultFunc,
      onDrop: () => {},
      onDragStart: () => {},
      name: data?.name || ''
    },
    meta: {
      active: false,
      asyncValidating: false,
      autofilled: false,
      initial: undefined,
      form: '',
      dispatch: () => null,
      dirty: false,
      invalid: data?.meta?.invalid || false,
      pristine: true,
      submitFailed: false,
      submitting: false,
      touched: data?.meta?.touched || false,
      valid: true,
      visited: false
    },
    id: data?.id || '123456789',
    options: data?.options || undefined,
    required: data?.required || false,
    readOnly: data?.readOnly || false,
    enable: true,
    mask: data?.mask || undefined
  };
};
