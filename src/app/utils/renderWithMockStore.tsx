// dof
import { act, render, RenderOptions } from '@testing-library/react';
// api service
import { apiMapping } from 'app/services';
import 'app/_libraries/_dof/layouts';
// reducers
import { reducerMappingList } from 'pages/_commons/redux';
import React from 'react';
import { Provider } from 'react-redux';
import {
  AnyAction,
  applyMiddleware,
  combineReducers,
  createStore
} from 'redux';
// middlewares
import thunk, { ThunkMiddleware } from 'redux-thunk';

export const middlewares = [thunk.withExtraArgument({ ...apiMapping })];

interface StoreConfigProps {
  initialState?: MagicKeyValue;
  middleware?: ThunkMiddleware<{}, AnyAction, {}>[];
}

export const mockCreateStore = (
  initialState: MagicKeyValue,
  middleware = middlewares
) =>
  createStore(
    combineReducers({ ...reducerMappingList }),
    initialState,
    applyMiddleware(...middleware)
  );

export const renderWithMockStore = async (
  element?: React.ReactElement,
  storeConfig?: StoreConfigProps,
  renderOptions?: Omit<RenderOptions, 'queries'>,
  testId?: string
) => {
  const { initialState, middleware } = storeConfig || {};

  const store = mockCreateStore(initialState!, middleware);
  const Wrapper: React.FC = ({ children, ...props }) => (
    <div data-testid={testId} {...props}>
      <Provider store={store}>{children}</Provider>
    </div>
  );

  let component = render(<div />);
  await Promise.resolve(
    act(async () => {
      component = render(element!, { wrapper: Wrapper, ...renderOptions });
    })
  );
  return component;
};
