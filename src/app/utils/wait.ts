export const wait = (mis = 0) =>
  new Promise(resolve => {
    const id = setTimeout(() => {
      clearTimeout(id);
      resolve(true);
    }, mis);
  });
