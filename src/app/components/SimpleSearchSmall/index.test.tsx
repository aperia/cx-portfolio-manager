import { fireEvent } from '@testing-library/react';
import { ISimpleSearchProps } from 'app/components/SimpleSearch';
import SimpleSearchSmall from 'app/components/SimpleSearchSmall';
import { renderComponent } from 'app/utils';
import React from 'react';

(function mockDOMMatrix() {
  class DOMMatrixMock {
    scale = jest.fn();
    translate = jest.fn();
  }
  global.DOMMatrix = DOMMatrixMock as any;
})();

const testId = 'simpleSearchSmall';
describe('SimpleSearchSmall', () => {
  it('should show simple search without default props', async () => {
    const onSearchFn = jest.fn();
    const searchData: ISimpleSearchProps = {
      defaultValue: 'Simple search',
      maxLength: 50,
      onSearch: onSearchFn
    };
    const renderResult = await renderComponent(
      testId,
      <SimpleSearchSmall {...searchData} />
    );
    const wrapper = await renderResult.findByTestId(testId);
    const popupTriggerElement = wrapper.querySelector(
      `button`
    ) as HTMLButtonElement;
    fireEvent.click(popupTriggerElement);

    const inputElement = document.body.querySelector(
      `.dls-tooltip-container .dls-input-container > input`
    ) as HTMLInputElement;

    expect(inputElement.getAttribute('value')).toEqual('Simple search');
  });

  it('should show simple search with default props', async () => {
    const onSearchFn = jest.fn();
    const searchData: ISimpleSearchProps = {
      onSearch: onSearchFn
    };
    const renderResult = await renderComponent(
      testId,
      <SimpleSearchSmall {...searchData} />
    );
    const wrapper = await renderResult.findByTestId(testId);
    const popupTriggerElement = wrapper.querySelector(
      `button`
    ) as HTMLButtonElement;
    fireEvent.click(popupTriggerElement);

    const inputElement = document.body.querySelector(
      `.dls-tooltip-container .dls-input-container > input`
    ) as HTMLInputElement;

    expect(inputElement.getAttribute('value')).toEqual('');
    expect(inputElement.getAttribute('maxLength')).toEqual('50');
  });

  it('on change keydown clear simple search', async () => {
    const onSearchFn = jest.fn();
    const searchData: ISimpleSearchProps = {
      defaultValue: 'default value',
      onSearch: onSearchFn
    };
    const renderResult = await renderComponent(
      testId,
      <SimpleSearchSmall {...searchData} />
    );
    const wrapper = await renderResult.findByTestId(testId);
    const popupTriggerElement = wrapper.querySelector(
      `button`
    ) as HTMLButtonElement;
    fireEvent.click(popupTriggerElement);

    const inputElement = document.body.querySelector(
      `.dls-tooltip-container .dls-input-container > input`
    ) as HTMLInputElement;

    fireEvent.change(inputElement, { target: { value: '' } });
    expect(inputElement.getAttribute('value')).toEqual('');

    fireEvent.change(inputElement, { target: { value: 'hi there' } });
    expect(inputElement.getAttribute('value')).toEqual('hi there');

    fireEvent.keyDown(inputElement, { key: 'Shift' });
    expect(onSearchFn).not.toHaveBeenCalled();
    fireEvent.keyDown(inputElement, { key: 'Enter' });
    expect(onSearchFn).toHaveBeenCalled();

    // re-trigger to re-open popup
    fireEvent.click(popupTriggerElement);
    const searchBtnElement = document.body.querySelector(
      `.dls-tooltip-container button.btn`
    ) as HTMLButtonElement;
    fireEvent.click(searchBtnElement);
    expect(onSearchFn).toHaveBeenCalled();

    // close on click outside
    fireEvent.click(popupTriggerElement);
    fireEvent.mouseDown(document.body);
  });
});
