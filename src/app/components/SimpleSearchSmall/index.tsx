import { classnames, isDevice } from 'app/helpers';
import { Button, Icon, Popover, Tooltip } from 'app/_libraries/_dls';
import React, { useCallback, useState } from 'react';
import { ISimpleSearchProps } from '../SimpleSearch';
import { SearchInput } from './SearchInput';

interface ISimpleSearchSmallProps extends ISimpleSearchProps {
  insideInfobar?: boolean;
}

const SimpleSearchSmall: React.FC<ISimpleSearchSmallProps> = ({
  defaultValue = '',
  placeholder = '',
  maxLength = 50,
  className,

  insideInfobar,

  onSearch
}) => {
  const [isOpenPopover, setOpenPopover] = useState(false);

  const handleSearch = useCallback(
    (value: string) => {
      setOpenPopover(false);
      onSearch && onSearch(value);
    },
    [onSearch]
  );

  const handleVisibilityChange = useCallback(isOpened => {
    setOpenPopover(isOpened);
  }, []);

  const handleTogglePopper = useCallback(() => {
    setOpenPopover(isOpened => !isOpened);
  }, []);

  return (
    <span className="mr-16">
      <Popover
        size="md"
        placement="bottom-end"
        containerClassName={classnames('dls-tooltip-container', {
          'inside-infobar': insideInfobar
        })}
        opened={isOpenPopover}
        onVisibilityChange={handleVisibilityChange}
        keepMount
        element={
          <SearchInput
            defaultValue={defaultValue}
            className={className}
            placeholder={placeholder}
            maxLength={maxLength}
            isOpenPopover={isOpenPopover}
            onSearch={handleSearch}
          />
        }
      >
        <Tooltip
          placement="top"
          element={'Search'}
          variant="primary"
          opened={isDevice || isOpenPopover ? false : undefined}
        >
          <Button
            variant="icon-secondary"
            className={classnames({
              asterisk: defaultValue?.trim(),
              active: isOpenPopover
            })}
            onClick={handleTogglePopper}
          >
            <Icon name="search" />
          </Button>
        </Tooltip>
      </Popover>
    </span>
  );
};

export default SimpleSearchSmall;
