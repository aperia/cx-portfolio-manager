import {
  Button,
  TextBox,
  TextBoxRef,
  useTranslation
} from 'app/_libraries/_dls';
import React, { useCallback, useEffect, useRef, useState } from 'react';
import { ISimpleSearchProps } from '../SimpleSearch';

interface SearchInputProps extends ISimpleSearchProps {
  isOpenPopover?: boolean;
}
export const SearchInput: React.FC<SearchInputProps> = ({
  defaultValue = '',
  maxLength = 50,
  className,
  placeholder,
  isOpenPopover = false,
  onSearch
}) => {
  const { t } = useTranslation();
  const [searchValue, setSearchValue] = useState('');
  const ref = useRef<TextBoxRef>(null);

  useEffect(() => {
    setSearchValue(defaultValue);
  }, [defaultValue]);

  useEffect(() => {
    ref.current?.inputElement?.focus();
  }, [isOpenPopover]);

  const handleTextChanged = useCallback((e: any) => {
    setSearchValue(e?.target?.value || '');
  }, []);

  const handleOnKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    event.key === 'Enter' && onSearch && onSearch(searchValue);
  };

  const handleSearch = useCallback(() => {
    onSearch && onSearch(searchValue);
  }, [onSearch, searchValue]);

  return (
    <>
      <h4 className="mb-16">{t('txt_search')}</h4>
      <TextBox
        size={className}
        placeholder={placeholder}
        value={searchValue}
        onKeyDown={handleOnKeyDown}
        onChange={handleTextChanged}
        maxLength={maxLength}
        ref={ref}
      />
      <div className="d-flex mt-24 justify-content-end">
        <Button
          size="sm"
          onClick={handleSearch}
          variant="primary"
          disabled={!searchValue?.trim()}
        >
          {t('txt_apply')}
        </Button>
      </div>
    </>
  );
};
