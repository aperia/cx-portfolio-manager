import { RenderResult } from '@testing-library/react';
import { ISimpleSearchProps } from 'app/components/SimpleSearch';
import { SearchInput } from 'app/components/SimpleSearchSmall/SearchInput';
import { renderComponent } from 'app/utils';
import React from 'react';

const mockData: ISimpleSearchProps = {} as ISimpleSearchProps;

const testId = 'searchInput';
describe('SimpleSearchSmall > SearchInput', () => {
  let renderResult: RenderResult;

  const createWrapper = async (...props: ISimpleSearchProps[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    renderResult = await renderComponent(
      testId,
      <SearchInput {...(mergeProps as any)} />
    );
  };

  const it_should_render_search_input = async () => {
    const wrapper = await renderResult.findByTestId(testId);
    expect(wrapper.querySelector('.dls-input-container')).toBeTruthy();
  };

  it('should render with default props', async () => {
    await createWrapper(mockData);

    await it_should_render_search_input();
  });
});
