import { getWorkflowCardIconValues } from 'app/helpers/getWorkflowCardIconValues';
import { Icon, IconNames } from 'app/_libraries/_dls';
import { iconNames } from 'app/_libraries/_dls/components/Icon/constants';
import { View } from 'app/_libraries/_dof/core';
import React from 'react';

export interface WorkflowTemplateCardViewProps {
  id: string;
  value: IWorkflowTemplateModel;
  isStatusGroup?: boolean;
  isHomePage?: boolean;
  onClick?: () => void;
}
const WorkflowTemplateCardView: React.FC<WorkflowTemplateCardViewProps> = ({
  id,
  value,
  isStatusGroup = false,
  isHomePage = false,
  onClick
}) => {
  const descriptorSuffix =
    (isStatusGroup && '--status-group') || (isHomePage && '--home-page') || '';
  const iconValues = getWorkflowCardIconValues(value);
  const setUpValue = !value.accesible ? undefined : { ...value };

  return (
    <div
      id={`workflow-template-card-view-${id || 'empty'}`}
      className="list-view d-flex align-items-start mt-12 workflow-template-card-view"
      onClick={onClick}
    >
      <div className="flex-shrink-0 mr-16">
        {getWorkflowIcon(value?.iconName as IconNames)}
      </div>
      <View
        id={`workflow-template-card-view__${id || 'empty'}`}
        formKey={`workflow-template-card-view__${id || 'empty'}`}
        descriptor={`workflow-template-card-view${descriptorSuffix}`}
        value={{ ...value, iconValues, setUpValue }}
      />
    </div>
  );
};

const getWorkflowIcon = (iconName: IconNames) => {
  const icon: IconNames = iconNames.includes(iconName)
    ? iconName
    : 'action-list';

  return <Icon name={icon} size="11x" className="color-grey-l16" />;
};

export default WorkflowTemplateCardView;
