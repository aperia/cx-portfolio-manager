import WorkflowTemplateCardView, {
  WorkflowTemplateCardViewProps
} from 'app/components/WorkflowTemplateCardView';
import { renderComponent } from 'app/utils';
import { App as DOFApp } from 'app/_libraries/_dof/core';
import React from 'react';

const idDomTest = 'WorkflowTemplateCardView';
const defaultProps: WorkflowTemplateCardViewProps = {
  id: 'test',
  value: {
    id: '36447063',
    name: 'SendLetterToAccountsWorkflow',
    description: 'workflow description',
    type: 'Pricing',
    lastExecuted: '2021-01-22T16:17:39.0963846+07:00',
    isFavorite: false,
    numberOfExcutions: 1,
    accesible: true
  },
  onClick: jest.fn()
};

describe('WorkflowTemplateCardView', () => {
  let wrapper: HTMLElement;

  const createWrapper = async (...props: WorkflowTemplateCardViewProps[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );

    const renderResult = await renderComponent(
      idDomTest,
      <DOFApp urlConfigs={[]}>
        <WorkflowTemplateCardView {...defaultProps} {...mergeProps} />
      </DOFApp>
    );
    wrapper = await renderResult.findByTestId(idDomTest);
  };

  it('should show workflow card data', async () => {
    await createWrapper();
    expect(
      wrapper.querySelector(`#workflow-template-card-view-${defaultProps.id}`)
    ).toBeTruthy();
  });

  it('should show workflow card data has Status Group', async () => {
    const props = { ...defaultProps, isStatusGroup: true };
    props.value.name = 'workflow name';
    await createWrapper(props);
    expect(
      wrapper.querySelector(`#workflow-template-card-view-${defaultProps.id}`)
    ).toBeTruthy();
  });

  it('should show workflow card data has empty id', async () => {
    const props = { ...defaultProps, id: '' };
    await createWrapper(props);
    expect(
      wrapper.querySelector(`#workflow-template-card-view-empty`)
    ).toBeTruthy();
  });
});
