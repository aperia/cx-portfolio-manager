import { Button, Icon, Tooltip, useTranslation } from 'app/_libraries/_dls';
import { View } from 'app/_libraries/_dof/core';
import React, { useMemo } from 'react';

export interface WorkflowInProgressCardViewProps {
  id: string;
  value: IWorkflowCardView;
  small?: boolean;
  isExpand?: boolean;
  onClick?: () => void;
  onToggle?: (workflowId: string) => void;
}
const WorkflowInProgressCardView: React.FC<WorkflowInProgressCardViewProps> = ({
  id,
  value,
  small = false,
  isExpand = false,
  onClick,
  onToggle
}) => {
  const updateValue = useMemo(() => {
    const newVal = { ...value } as Record<string, any>;
    const editable = newVal.actions?.some((i: any) =>
      i?.toLowerCase().includes('edit')
    );

    if (editable) {
      newVal.continuePayload = { ...value };
    }
    newVal['divWithDot'] = 'divWithDot';
    newVal['emptyDiv'] = 'emptyDiv';
    return newVal;
  }, [value]);

  const { t } = useTranslation();

  const descriptorSuffix = useMemo(() => (small && '--small') || '', [small]);

  const handleToggleCard = (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    e.stopPropagation();
    onToggle && onToggle(isExpand ? '' : updateValue.id);
  };

  return (
    <div
      id={`workflow-in-progress-card-view-${id || 'empty'}`}
      className="list-view mt-12 workflow-card-view workflow-in-progress-card-view"
      onClick={onClick}
    >
      <View
        id={`workflow-in-progress-card-view-header__${id || 'empty'}`}
        formKey={`workflow-in-progress-card-view-header__${id || 'empty'}`}
        descriptor={`workflow-in-progress-card-view-header${descriptorSuffix}`}
        value={updateValue}
      />
      {isExpand && (
        <div className="bg-light-l16 mt-8 pt-4 mx-n16 mb-n8 rounded-bottom-8">
          <div className="mx-16 pb-8">
            <View
              id={`workflow-in-progress-card-view__${id || 'empty'}`}
              formKey={`workflow-in-progress-card-view__${id || 'empty'}`}
              descriptor={
                small
                  ? 'workflow-in-progress-card-view--small'
                  : 'workflow-in-progress-card-view'
              }
              value={updateValue}
            />
          </div>
        </div>
      )}
      <div className="workflow-card-expand">
        <Tooltip
          element={isExpand ? t('txt_collapse') : t('txt_expand')}
          variant="primary"
          placement="top"
        >
          <Button size="sm" variant="icon-secondary" onClick={handleToggleCard}>
            <Icon name={isExpand ? 'minus' : 'plus'} size="4x" />
          </Button>
        </Tooltip>
      </div>
    </div>
  );
};

export default WorkflowInProgressCardView;
