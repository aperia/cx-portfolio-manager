import { fireEvent } from '@testing-library/dom';
import WorkflowInProgressCardView, {
  WorkflowInProgressCardViewProps
} from 'app/components/WorkflowInProgressCardView';
import { renderComponent } from 'app/utils';
import { App as DOFApp } from 'app/_libraries/_dof/core';
import React from 'react';

const idDomTest = 'WorkflowInProgressCardView';
const defaultProps: WorkflowInProgressCardViewProps = {
  id: 'test',
  value: {} as IWorkflowModel,
  onClick: jest.fn()
};

describe('WorkflowInProgressCardView', () => {
  let wrapper: HTMLElement;
  let debug: any;

  const createWrapper = async (...props: WorkflowInProgressCardViewProps[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );

    const renderResult = await renderComponent(
      idDomTest,
      <DOFApp urlConfigs={[]}>
        <WorkflowInProgressCardView {...defaultProps} {...mergeProps} />
      </DOFApp>
    );
    debug = renderResult.debug;
    wrapper = await renderResult.findByTestId(idDomTest);
  };

  it('should show workflow card data', async () => {
    const toggleFn = jest.fn();

    await createWrapper({
      value: {
        id: '111',
        name: 'Test',
        actions: ['edit']
      } as IWorkflowModel,
      onToggle: toggleFn,
      id: 'mock-id'
    } as WorkflowInProgressCardViewProps);
    debug();
    expect(
      wrapper.querySelector(`#workflow-in-progress-card-view-mock-id`)
    ).toBeTruthy();
    const button = wrapper.querySelector(
      'button.btn-icon-secondary'
    ) as HTMLButtonElement;

    fireEvent.click(button);

    expect(toggleFn).toHaveBeenCalled();
  });

  it('should show workflow card data with small', async () => {
    await createWrapper({
      value: {
        id: '111',
        name: 'Test',
        actions: ['edit']
      } as IWorkflowModel,
      small: true
    } as WorkflowInProgressCardViewProps);
    debug();
    expect(
      wrapper.querySelector(
        `#workflow-in-progress-card-view-${defaultProps.id}`
      )
    ).toBeTruthy();
  });

  it('should show workflow card data with expand', async () => {
    const toggleFn = jest.fn();
    await createWrapper({
      value: {
        id: '111',
        name: 'Test',
        actions: ['edit']
      } as IWorkflowModel,
      isExpand: true,
      onToggle: toggleFn,
      id: 'mock-id'
    } as WorkflowInProgressCardViewProps);
    expect(
      wrapper.querySelector(`#workflow-in-progress-card-view-mock-id`)
    ).toBeTruthy();

    const button = wrapper.querySelector(
      'button.btn-icon-secondary'
    ) as HTMLButtonElement;

    fireEvent.click(button);

    expect(toggleFn).toHaveBeenCalled();
  });

  it('should show workflow card data with expand & empty id', async () => {
    await createWrapper({
      value: {
        id: '111',
        name: 'Test',
        actions: ['edit']
      } as IWorkflowModel,
      isExpand: true,
      id: ''
    } as WorkflowInProgressCardViewProps);
    expect(
      wrapper.querySelector(`#workflow-in-progress-card-view-empty`)
    ).toBeTruthy();
  });

  it('should show workflow card data has empty id', async () => {
    const props = { ...defaultProps, id: '' };
    await createWrapper(props);
    expect(
      wrapper.querySelector(`#workflow-in-progress-card-view-empty`)
    ).toBeTruthy();
  });
});
