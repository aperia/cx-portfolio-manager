import { fireEvent, render, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { TextAreaProps } from 'app/_libraries/_dls';
// import 'app/utils/_mockComponent/mockCanvas';
import React from 'react';
import TextAreaCountDown from './index';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

describe('Test TextAreaCountDown Component', () => {
  const mockTextAreaCountDownProps = {
    id: 'mock-id',
    placeholder: 'Mock placeholder'
  } as TextAreaProps;

  const renderTextAreaCountDown = (props?: TextAreaProps): RenderResult => {
    return render(
      <div>
        <TextAreaCountDown {...mockTextAreaCountDownProps} {...props} />
      </div>
    );
  };

  it('should render TextAreaCountDown UI with value', async () => {
    const changeFn = jest.fn();
    const blurFn = jest.fn();
    const wrapper = renderTextAreaCountDown({
      value: 'Mock textarea value',
      onChange: changeFn,
      onBlur: blurFn
    });

    const { queryByText } = wrapper;

    expect(queryByText(/Mock textarea value/i)).toBeInTheDocument();
    expect(queryByText(/txt_characters_left$/i)).toBeInTheDocument();

    fireEvent.change(queryByText(/Mock textarea value/i)!, {
      target: { value: 'some value' }
    });
    expect(changeFn).toHaveBeenCalled();

    userEvent.type(queryByText(/some value/i)!, '{enter}');
  });

  it('should render TextAreaCountDown UI with value & max length', async () => {
    const wrapper = renderTextAreaCountDown({
      value: 'Mock textarea value',
      maxLength: 20
    });

    const { queryByText } = wrapper;

    expect(queryByText(/Mock textarea value/i)).toBeInTheDocument();
    expect(queryByText(/1 txt_character_left$/i)).toBeInTheDocument();
    fireEvent.blur(queryByText(/Mock textarea value/i)!);
  });

  it('should render TextAreaCountDown UI without value', async () => {
    const changeFn = jest.fn();
    const wrapper = renderTextAreaCountDown({ onChange: changeFn });

    const { queryByText } = wrapper;

    fireEvent.change(wrapper.baseElement.querySelector('textarea')!, {
      target: { value: 'Another value' }
    });
    expect(queryByText(/Another value/i)).toBeInTheDocument();
  });
});
