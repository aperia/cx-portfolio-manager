import { classnames, formatCommon } from 'app/helpers';
import { TextArea, TextAreaProps, useTranslation } from 'app/_libraries/_dls';
import { extendsEvent } from 'app/_libraries/_dls/utils';
import { isFunction } from 'lodash';
import React, { useEffect, useMemo, useState } from 'react';

interface TextAreaCountDownProps extends TextAreaProps {
  resize?: boolean;
}
const TextAreaCountDown: React.FC<TextAreaCountDownProps> = ({
  value: valueProp,
  rows = 4,
  maxLength = 255,
  className: classNameProp,
  resize = false,
  onChange,
  ...rest
}) => {
  const { t } = useTranslation();

  const className = classnames(classNameProp, !resize && 'resize-none');

  const [value, setValue] = useState<string>((valueProp || '') as string);
  useEffect(() => setValue((valueProp || '') as string), [valueProp]);
  const countDown = useMemo(() => maxLength - value.length, [maxLength, value]);

  const handleChange: React.ChangeEventHandler<HTMLTextAreaElement> = e => {
    setValue(e.target.value.replace(/\n/g, ''));

    const nextEvent = extendsEvent(e, 'target', {
      value: e.target.value.replace(/\n/g, '')
    });
    isFunction(onChange) && onChange(nextEvent);
  };

  return (
    <div className="w-100">
      <TextArea
        value={value}
        rows={rows}
        maxLength={maxLength}
        onChange={handleChange}
        onKeyPress={e => e.key === 'Enter' && e.preventDefault()}
        className={className}
        {...rest}
      />
      <div className="mt-8">
        <span className="fs-12 color-grey-l24">
          {formatCommon(countDown).quantity}{' '}
          {countDown !== 1 ? t('txt_characters_left') : t('txt_character_left')}
        </span>
      </div>
    </div>
  );
};

export default TextAreaCountDown;
