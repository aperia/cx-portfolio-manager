import { Icon, TextBox } from 'app/_libraries/_dls';
import React, { useEffect, useState } from 'react';
export interface IProps {
  value?: string;
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  disabled?: boolean;
  readOnly?: boolean;
}
const SentitiveDataInput: React.FC<IProps> = ({
  value,
  onChange,
  disabled,
  readOnly,
  ...rest
}) => {
  const [show, setShow] = useState<boolean>(false);
  useEffect(() => {
    if (show) {
      const id = setTimeout(() => {
        setShow(false);
      }, 3000);
      return () => {
        clearTimeout(id);
      };
    }
  }, [show]);
  return (
    <div className="sentitive-data-input">
      <TextBox
        label="Password"
        onChange={onChange}
        size="sm"
        type={show ? 'input' : 'password'}
        value={value}
        disabled={disabled}
        readOnly={readOnly}
        {...rest}
        suffix={
          <span className="text-box-suffix" style={{ order: 2 }}>
            <Icon
              className={
                disabled || readOnly ? 'eye-icon-disabled' : 'eye-icon'
              }
              name={show ? 'eye-hide' : 'eye'}
              size="4x"
              onClick={() => {
                setShow(show => !show);
              }}
            />
          </span>
        }
      />
    </div>
  );
};

export default SentitiveDataInput;
