import { fireEvent, render } from '@testing-library/react';
import React from 'react';
import SentitiveDataInput, { IProps } from '.';

describe('App > Components > SentitiveDataInput', () => {
  const renderComponent = (props: IProps) => {
    return render(<SentitiveDataInput {...props} />);
  };

  it('Should render component with value', () => {
    jest.useFakeTimers();
    const wrapper = renderComponent({ value: 'abcd1234' });
    const eyeIcon = wrapper.container.querySelector('i.icon.icon-eye');
    fireEvent.click(eyeIcon!);
    jest.runAllTimers();
    expect(wrapper.queryByText('abcd1234')).not.toBeInTheDocument();
  });
});
