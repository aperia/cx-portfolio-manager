import { fireEvent } from '@testing-library/react';
import SimpleSearch, {
  ISimpleSearchProps,
  SimpleSearchPublic
} from 'app/components/SimpleSearch';
import { renderComponent } from 'app/utils';
import React from 'react';

const testId = 'simpleSearch';
describe('SimpleSearch', () => {
  it('should show simple search without default props', async () => {
    const ref = React.createRef<SimpleSearchPublic>();
    const onSearchFn = jest.fn();
    const searchData: ISimpleSearchProps = {
      defaultValue: 'Simple search',
      maxLength: 50,
      clearTooltip: '',
      onSearch: onSearchFn
    };
    const renderResult = await renderComponent(
      testId,
      <SimpleSearch ref={ref} {...searchData} />
    );
    const wrapper = await renderResult.findByTestId(testId);
    const inputElement = wrapper.querySelector(
      `.dls-input-container > input`
    ) as HTMLInputElement;

    expect(inputElement.getAttribute('value')).toEqual('Simple search');

    fireEvent.focus(inputElement);
    expect(
      document.body.querySelector('.dls-popper-container.size-custom')
    ).toBeTruthy();

    fireEvent.blur(inputElement);
    expect(
      document.body.querySelector('.dls-popper-container.size-custom')
    ).not.toBeTruthy();
  });

  it('should show simple search with default props', async () => {
    const ref = React.createRef<SimpleSearchPublic>();
    const onSearchFn = jest.fn();
    const searchData: ISimpleSearchProps = {
      onSearch: onSearchFn
    };
    const renderResult = await renderComponent(
      testId,
      <SimpleSearch ref={ref} {...searchData} />
    );
    const wrapper = await renderResult.findByTestId(testId);
    const inputElement = wrapper.querySelector(
      `.dls-input-container > input`
    ) as HTMLInputElement;

    expect(inputElement.getAttribute('value')).toEqual('');
    expect(inputElement.getAttribute('maxLength')).toEqual('50');
  });

  it('on change keydown clear simple search', async () => {
    const ref = React.createRef<SimpleSearchPublic>();
    const onSearchFn = jest.fn();
    const searchData: ISimpleSearchProps = {
      defaultValue: 'default value',
      onSearch: onSearchFn
    };
    const renderResult = await renderComponent(
      testId,
      <SimpleSearch ref={ref} {...searchData} />
    );
    const wrapper = await renderResult.findByTestId(testId);

    const inputElement = wrapper.querySelector(
      `.dls-input-container > input`
    ) as HTMLInputElement;

    fireEvent.change(inputElement, { target: { value: '' } });
    expect(inputElement.getAttribute('value')).toEqual('');

    fireEvent.change(inputElement, { target: { value: 'hi there' } });
    expect(inputElement.getAttribute('value')).toEqual('hi there');

    fireEvent.keyDown(inputElement, { key: 'Shift' });
    expect(onSearchFn).not.toHaveBeenCalled();
    fireEvent.keyDown(inputElement, { key: 'Enter' });
    expect(onSearchFn).toHaveBeenCalled();

    const iconSearchElement = wrapper.querySelector(
      `.dls-input-container i.icon.icon-search`
    ) as HTMLElement;
    fireEvent.click(iconSearchElement);
    expect(onSearchFn).toHaveBeenCalled();

    const iconCloseElement = wrapper.querySelector(
      `.dls-input-container i.icon.icon-close`
    ) as HTMLElement;
    fireEvent.change(inputElement, { target: { value: 'hi there' } });
    fireEvent.mouseDown(iconCloseElement);
    expect(inputElement.getAttribute('value')).toEqual('');

    ref.current!.clear();
    expect(inputElement.getAttribute('value')).toEqual('');
  });
});
