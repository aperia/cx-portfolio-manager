import { classnames, isDevice } from 'app/helpers';
import { Icon, Popover, TextBox, Tooltip } from 'app/_libraries/_dls';
import classNames from 'classnames';
import isEmpty from 'lodash.isempty';
import React, { useEffect, useImperativeHandle, useState } from 'react';

export interface SimpleSearchPublic {
  clear: () => void;
}
export interface ISimpleSearchProps {
  maxLength?: number;
  defaultValue?: string;
  placeholder?: string;
  className?: string;
  clearTooltip?: string;
  popperElement?: React.ReactNode;
  onSearch: (value: string) => void;
}

const SimpleSearch: React.RefForwardingComponent<
  SimpleSearchPublic,
  ISimpleSearchProps
> = (
  {
    defaultValue = '',
    placeholder = '',
    maxLength = 50,
    className = 'simple-search',
    clearTooltip = 'Clear Search Criteria',
    popperElement,
    onSearch,
    ...props
  },
  ref
) => {
  const [searchValue, setSearchValue] = useState(defaultValue);
  const [openedSearchPopover, setOpenedSearchPopover] =
    useState<boolean>(false);

  const handleSearch = () => {
    onSearch && onSearch(searchValue);
  };

  const handleOnFocus = () => {
    setOpenedSearchPopover(true);
  };

  const handleOnBlur = () => {
    setOpenedSearchPopover(false);
  };

  const handleClear = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    event?.preventDefault();
    setSearchValue('');
  };

  const handleOnKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === 'Enter') {
      onSearch && onSearch(searchValue);
      const target = event.target as HTMLElement;
      target.blur();
    }
  };

  useImperativeHandle(ref, () => ({
    clear: () => {
      setSearchValue('');
    }
  }));

  useEffect(() => {
    setSearchValue(defaultValue);
  }, [defaultValue]);

  return (
    <Popover
      element={popperElement ? popperElement : undefined}
      opened={openedSearchPopover}
      arrowClassName="invisible"
      placement="bottom-start"
      containerClassName={classNames(
        'simple-search-popover size-custom',
        !popperElement && 'd-none'
      )}
    >
      <TextBox
        size="sm"
        className="w-320"
        placeholder={placeholder}
        value={searchValue}
        onKeyDown={handleOnKeyDown}
        onChange={({ target }) => setSearchValue(target.value || '')}
        onFocus={handleOnFocus}
        onBlur={handleOnBlur}
        maxLength={maxLength}
        suffix={
          <div className="icon-simple-search">
            <div
              onMouseDown={handleClear}
              className={classnames(
                'pr-12',
                !isEmpty(searchValue) ? '' : 'd-none'
              )}
            >
              <Tooltip
                element={clearTooltip}
                opened={isDevice || !clearTooltip ? false : undefined}
                placement="top"
                variant={'primary'}
              >
                <Icon name={'close'} />
              </Tooltip>
            </div>
            <div onClick={handleSearch}>
              <Icon name={'search'} />
            </div>
          </div>
        }
        {...props}
      />
    </Popover>
  );
};

export default React.forwardRef<SimpleSearchPublic, ISimpleSearchProps>(
  SimpleSearch
);
