import { fireEvent, render } from '@testing-library/react';
import React from 'react';
import MaskedPassword, { IProps } from '.';

describe('App > Components > MaskPassword', () => {
  const renderComponent = (props: IProps) => {
    return render(<MaskedPassword {...props} />);
  };

  it('Should render component with value', () => {
    jest.useFakeTimers();
    const wrapper = renderComponent({ value: 'abcd1234' });
    const eyeIcon = wrapper.container.querySelector('i.icon.icon-eye');
    fireEvent.click(eyeIcon!);
    expect(wrapper.getByText('abcd1234')).toBeInTheDocument();
    jest.runAllTimers();
    expect(wrapper.queryByText('abcd1234')).not.toBeInTheDocument();
  });

  it('Should render component with value is null', () => {
    const wrapper = renderComponent({ value: '' });
    const eyeIcon = wrapper.container.querySelector('i.icon.icon-eye');
    expect(eyeIcon).not.toBeInTheDocument();
  });
});
