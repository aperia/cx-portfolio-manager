import { Icon } from 'app/_libraries/_dls';
import React, { useEffect, useState } from 'react';
export interface IProps {
  value?: string;
}
const MaskedPassword: React.FC<IProps> = ({ value }) => {
  const maskValue = '• • • • • • • • •';
  const [show, setShow] = useState<boolean>(false);
  useEffect(() => {
    if (show) {
      const id = setTimeout(() => {
        setShow(false);
      }, 3000);

      return () => clearTimeout(id);
    }
  }, [show]);
  if (!value) return null;
  return (
    <div className="sentitive-data-input">
      <span className="mr-8">{show ? value : maskValue}</span>
      <span onClick={() => setShow(show => !show)}>
        <Icon className="eye-icon" name={show ? 'eye-hide' : 'eye'} />
      </span>
    </div>
  );
};

export default MaskedPassword;
