import LoadMore, { LoadMoreProps } from 'app/components/LoadMore';
import { renderComponent } from 'app/utils';
import React from 'react';
const idDomTest = 'testId';
const defaultProps: LoadMoreProps = {
  loading: false,
  onLoadMore: jest.fn(),
  isEnd: true,
  onBackToTop: jest.fn(),
  className: 'class',
  loadingText: 'loading',
  endLoadMoreText: 'end',
  hideBackToTop: false
};
describe('LoadMore', () => {
  let wrapper: HTMLElement;
  const createWrapper = async (...props: LoadMoreProps[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    const renderResult = await renderComponent(
      idDomTest,
      <LoadMore {...defaultProps} {...mergeProps} />
    );
    wrapper = await renderResult.findByTestId(idDomTest);
  };

  const it_should_render_loadmore_with_loading = () => {
    expect(wrapper.querySelector(`span`)).toBeInTheDocument();
  };

  it('should show loadmore with loading', async () => {
    const observe = jest.fn();
    const unobserve = jest.fn();
    const disconnect = jest.fn();
    const mockIntersectionObserver = jest.fn();
    mockIntersectionObserver.mockReturnValue({
      observe,
      unobserve,
      disconnect
    });
    window.IntersectionObserver = mockIntersectionObserver;

    const onLoadMoreMock = jest.fn();
    const loadMoreProps: LoadMoreProps = {
      loading: true,
      className: 'load-more',
      isEnd: false,
      onLoadMore: onLoadMoreMock
    };
    await createWrapper(loadMoreProps);
    expect(observe).toHaveBeenCalled();
    it_should_render_loadmore_with_loading();
  });
});
