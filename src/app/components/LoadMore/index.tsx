/* istanbul ignore file */
import { getScrollParent } from 'app/helpers';
import classnames from 'classnames';
import get from 'lodash.get';
import isEmpty from 'lodash.isempty';
import React, { useCallback, useEffect, useRef, useState } from 'react';

export interface LoadMoreProps {
  loading: boolean;
  onLoadMore: () => void;
  isEnd: boolean;
  onBackToTop?: () => void;
  className?: string;
  loadingText?: string;
  endLoadMoreText?: string;
  hideBackToTop?: boolean;
}

const LoadMore: React.FC<LoadMoreProps> = ({
  loading,
  onLoadMore,
  onBackToTop: onBackToTopProp,
  isEnd,
  className: containerClassName,
  loadingText = 'Loading more results...',
  endLoadMoreText = 'End of Results List.',
  hideBackToTop = false,
  ...props
}) => {
  const [loadMoreElement, setLoadMoreElement] = useState<HTMLElement | null>(
    null
  );
  const [isShowLoadMore, setIsShowLoadMore] = useState<boolean>(false);
  const scrollParentRef = useRef<HTMLElement | undefined>(undefined);

  const handleIntersection = useCallback(
    entries => {
      const { isIntersecting } = get(entries, [0]);
      if (!isIntersecting || loading) return;

      // Do Not show LoadMore if they don't have scroll
      scrollParentRef.current = getScrollParent(loadMoreElement);
      const { scrollHeight, clientHeight } = scrollParentRef.current || {};
      const showLoadMore = scrollHeight! > clientHeight!;
      setIsShowLoadMore(showLoadMore);

      if (
        typeof onLoadMore === 'function' &&
        loading === false &&
        showLoadMore &&
        !isEnd
      ) {
        onLoadMore();
      }
    },
    [onLoadMore, loading, isEnd, loadMoreElement]
  );

  const handleBackToTop = () => {
    if (typeof onBackToTopProp === 'function') return onBackToTopProp();

    if (scrollParentRef && scrollParentRef.current) {
      scrollParentRef.current.scrollTo({ top: 0, behavior: 'smooth' });
    }
  };

  useEffect(() => {
    const options: IntersectionObserverInit = {
      root: null,
      rootMargin: '24px',
      threshold: 0.9
    };

    if (loadMoreElement) {
      const observer = new IntersectionObserver(handleIntersection, options);
      observer.observe(loadMoreElement);
      return () => observer.unobserve(loadMoreElement);
    }
  }, [loadMoreElement, handleIntersection]);

  return (
    <>
      {/* element to watch show on Window */}
      <span ref={setLoadMoreElement} />
      {isShowLoadMore && (
        <div
          className={classnames(
            !isEmpty(containerClassName) && containerClassName,
            (loading || isEnd) &&
              'd-flex align-items-center justify-content-center fs-14 mt-16'
          )}
        >
          {loading && (
            <>
              <span className="loading loading-sm mr-16" />
              <span>{loadingText}</span>
            </>
          )}
          {!loading && isEnd && !hideBackToTop && (
            <>
              <span className="mr-8">{endLoadMoreText}</span>
              <span>Click &nbsp;</span>
              <span
                className="link text-decoration-none"
                onClick={handleBackToTop}
              >
                Back to top.
              </span>
            </>
          )}
        </div>
      )}
    </>
  );
};

export default LoadMore;
