import { classnames } from 'app/helpers';
import { Button, Icon, Tooltip, useTranslation } from 'app/_libraries/_dls';
import { View } from 'app/_libraries/_dof/core';
import React from 'react';

export interface ChangeCardViewProps {
  id: string;
  value: IChangeCardView;
  isListViewMode?: boolean;
  isExpand?: boolean;
  onClick?: () => void;
  onToggle?: (changeId: string) => void;
}
const ChangeCardView: React.FC<ChangeCardViewProps> = ({
  id,
  value,
  isListViewMode = true,
  isExpand = false,
  onToggle,
  onClick
}) => {
  const { t } = useTranslation();
  const showChangeInTermsSetup =
    value?.changeInTermsStatus?.toLowerCase() === 'not started';

  const updateValue = { ...value };
  if (showChangeInTermsSetup) {
    updateValue.actionPayload = value;
  }
  const handleToggleCard = (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    e.stopPropagation();
    onToggle && onToggle(value.changeId);
  };

  return (
    <div
      className={classnames(
        'mt-12',
        isListViewMode ? 'col-12' : 'col-12 box-list-view masonry-item'
      )}
    >
      <div
        id={`change-card-data-${id || 'empty'}`}
        className="change-card-view list-view"
        onClick={onClick}
      >
        <View
          id={`change-card-view-header__${id || 'empty'}`}
          formKey={`change-card-view-header__${id || 'empty'}`}
          descriptor="change-card-view-header"
          value={updateValue}
        />
        {isExpand && (
          <div className="bg-light-l16 mt-8 pt-4 mx-n16 mb-n8 rounded-bottom-8">
            <div className="mx-16 pb-8">
              <View
                id={`change-card-view__${id || 'empty'}`}
                formKey={`change-card-view__${id || 'empty'}`}
                descriptor="change-card-view"
                value={updateValue}
              />
            </div>
          </div>
        )}
        <div className="change-card-expand">
          <Tooltip
            element={isExpand ? t('txt_collapse') : t('txt_expand')}
            variant="primary"
            placement="top"
          >
            <Button
              size="sm"
              variant="icon-secondary"
              onClick={handleToggleCard}
            >
              <Icon name={isExpand ? 'minus' : 'plus'} size="4x" />
            </Button>
          </Tooltip>
        </div>
      </div>
    </div>
  );
};

export default ChangeCardView;
