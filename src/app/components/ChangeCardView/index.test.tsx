import { fireEvent, RenderResult } from '@testing-library/react';
import ChangeCardView, {
  ChangeCardViewProps
} from 'app/components/ChangeCardView';
import { renderComponent } from 'app/utils';
import { App as DOFApp } from 'app/_libraries/_dof/core';
import React from 'react';

const mockData: IChangeCardView = {
  changeId: '48298468',
  changeName: 'Card Compromise November',
  changeOwner: 'Qozhowu Civusyla',
  createdDate: '2021-01-28T11:16:14.3536967+07:00',
  effectiveDate: '2021-01-28T11:16:14.3537965+07:00',
  approvalDate: '2021-01-28T11:16:14.3538515+07:00',
  changeType: 'Pricing',
  changeStatus: 'Lapsed',
  portfoliosImpacted: 6157,
  pricingStrategiesCreatedCount: 2172,
  pricingStrategiesUpdatedCount: 2172,
  pricingMethodsCreatedCount: 7674,
  pricingMethodsUpdatedCount: 7674,
  cardholdersAccountsImpacted: 7233,
  approvalRecords: [],
  pendingApprovalBy: [],
  lastChangedBy: {
    id: '1',
    name: 'mock name'
  },
  lastChangedDate: new Date('2021-01-01'),
  actions: []
};
const testId = 'changeCardView';
describe('ChangeCardView', () => {
  let renderResult: RenderResult;

  const createWrapper = async (...props: ChangeCardViewProps[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    renderResult = await renderComponent(
      testId,
      <DOFApp urlConfigs={[]}>
        <ChangeCardView {...(mergeProps as any)} />
      </DOFApp>
    );
  };

  const it_should_render_change = async (isListView: boolean) => {
    const wrapper = await renderResult.findByTestId(testId);

    expect(
      wrapper.querySelector(`#change-card-data-${mockData.changeId}`)
    ).toBeTruthy();

    const expectation = expect(wrapper.querySelector('.box-list-view'));
    isListView ? expectation.not.toBeTruthy() : expectation.toBeTruthy();
  };

  const it_should_render_empty_change = async () => {
    const wrapper = await renderResult.findByTestId(testId);

    expect(wrapper.querySelector('#change-card-data-empty')).toBeTruthy();
  };

  it('should show change as listView', async () => {
    await createWrapper({
      id: mockData.changeId,
      value: { ...mockData, changeInTermsStatus: 'Not Started' }
    });

    await it_should_render_change(true);
  });

  it('should show change as gridView', async () => {
    await createWrapper({
      id: mockData.changeId,
      value: mockData,
      isListViewMode: false
    });

    await it_should_render_change(false);
  });

  it('should show empty change', async () => {
    await createWrapper({} as any);

    await it_should_render_empty_change();
  });

  it('should show change with expand ', async () => {
    const toggleFn = jest.fn();

    await createWrapper({
      isExpand: true,
      onToggle: toggleFn,
      value: { changeId: '123' }
    } as any);

    const wrapper = await renderResult.findByTestId(testId);

    expect(wrapper.querySelector('.bg-light-l16')).toBeInTheDocument();

    fireEvent.click(
      wrapper.querySelector('.change-card-expand')!.querySelector('i.icon')!
    );
    expect(toggleFn).toHaveBeenCalled();
  });
});
