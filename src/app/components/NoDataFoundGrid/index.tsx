import { Icon, useTranslation } from 'app/_libraries/_dls';
import React from 'react';

const NoDataFoundGrid: React.FC = () => {
  const { t } = useTranslation();
  return (
    <div className="d-flex align-items-center p-16">
      <span className="mr-14">
        <Icon name={`file color-light-l12 fs-20` as any} />
      </span>
      <span className="color-grey">{t('txt_no_data_found_grid')}</span>
    </div>
  );
};

export default NoDataFoundGrid;
