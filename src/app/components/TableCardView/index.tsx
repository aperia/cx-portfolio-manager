import { formatCommon } from 'app/helpers';
import {
  Badge,
  Button,
  Icon,
  Radio,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import { View } from 'app/_libraries/_dof/core';
import React, { useCallback, useMemo } from 'react';

export interface ITableCardViewProps {
  id: string;
  value: any;
  selectedVersion?: any;
  isExpand?: boolean;
  onToggle?: (tableId: string) => void;
  onSelectVersion?: (version: any) => void;
  onViewVersionDetail?: (version: any) => void;
}

export const StatusName = {
  production: 'production',
  clientApproved: 'client approved',
  scheduled: 'scheduled',
  working: 'working',
  validating: 'validating',
  previous: 'previous',
  lapsed: 'lapsed'
};

export const TableCardViewTestId = {
  selectDivVersion: 'selectDivVersion',
  selectInputVersion: 'selectInputVersion',
  toggleCard: 'toggleCard',
  viewVersionDetail: 'viewVersionDetail'
};

export const mapColorBadge = (status: string) => {
  switch (status.toLocaleLowerCase()) {
    case StatusName.scheduled:
    case StatusName.working:
      return 'cyan';
    case StatusName.validating:
    case StatusName.previous:
      return 'orange';
    case StatusName.clientApproved:
    case StatusName.production:
      return 'green';
    case StatusName.lapsed:
      return 'red';
    default:
      return 'grey';
  }
};
const TableCardView: React.FC<ITableCardViewProps> = ({
  id,
  value: valueProp,
  selectedVersion,
  onSelectVersion,
  isExpand,
  onToggle,
  onViewVersionDetail
}) => {
  const { t } = useTranslation();
  const { versions, tableId } = valueProp;
  const text = useMemo(() => {
    const numberOfVersions = versions.length;
    return numberOfVersions === 1
      ? numberOfVersions + ' Version'
      : numberOfVersions + ' Versions';
  }, [versions.length]);
  const value = {
    name: tableId,
    numberOfVersions: text
  };

  const handleToggleCard = useCallback(
    (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
      e.stopPropagation();
      onToggle && onToggle(valueProp?.tableId);
    },
    [onToggle, valueProp?.tableId]
  );

  const handleSelectVersion = useCallback(
    (version: string) => {
      const onClick = (e: any) => {
        if (e.target.tagName === 'INPUT') return;
        onSelectVersion && onSelectVersion(version);
      };
      return onClick;
    },
    [onSelectVersion]
  );

  const handleViewVersionDetail = useCallback(
    (version: string) => {
      const onClick = (e: any) => {
        e.stopPropagation();
        onViewVersionDetail && onViewVersionDetail(version);
      };
      return onClick;
    },
    [onViewVersionDetail]
  );

  return (
    <div id={`table-card-data-${id || 'empty'}`} className="mt-12 col-12">
      <div className="list-view method-card-view py-12 overflow-hidden">
        <View
          id={`table-card-view-header__${id || 'empty'}`}
          formKey={`table-card-view-header__${id || 'empty'}`}
          descriptor={`table-card-view-header`}
          value={value}
        />
        {isExpand && (
          <div className="method-card-view-detail bg-light-l16 rounded-bottom-8 py-8 mx-n16 px-16 mb-n12 mt-10">
            <h6 className="pb-4">TABLE VERSIONS</h6>
            {versions.map((v: any, index: number) => (
              <div key={`${v.id}-${index}`} className="position-relative">
                <div
                  className="list-view py-12 mt-12 bg-white position-relative bg-checkbox-hover-light-l20 custom-control-root"
                  onClick={handleSelectVersion(v)}
                  data-testid={TableCardViewTestId.selectDivVersion}
                >
                  <div className="row align-items-center">
                    <Radio
                      className="select-version-radio"
                      onClick={handleSelectVersion(v)}
                      dataTestId={TableCardViewTestId.selectInputVersion}
                    >
                      <Radio.Input
                        className="checked-style"
                        checked={
                          v?.tableId === selectedVersion?.version?.tableId
                        }
                      />
                    </Radio>
                    <div className="col-4" role="dof-field">
                      <div className="form-group-static mt-0 d-flex align-items-center w-100 pl-18  ml-24">
                        <div className="color-grey text-nowrap mr-2">
                          Table Name:
                        </div>
                        <div className="form-group-static__text flex-fill fw-600">
                          {v?.tableName}
                        </div>
                      </div>
                    </div>
                    <div className="col-4" role="dof-field">
                      <div className="form-group-static mt-0 d-flex align-items-center w-100">
                        <div className="color-grey text-nowrap mr-2">
                          Effective Date:
                        </div>
                        <div className="form-group-static__text flex-fill">
                          {formatCommon(v?.effectiveDate).time.date}
                        </div>
                      </div>
                    </div>
                    <div className="col col-lg-3" role="dof-field">
                      <div className="form-group-static mt-0 d-flex align-items-center">
                        <Badge color={mapColorBadge(v?.status)}>
                          <span>{v?.status}</span>
                        </Badge>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="method-card-view-btn" role="dof-field">
                  <Button
                    size="sm"
                    variant="outline-primary"
                    onClick={handleViewVersionDetail(v)}
                    dataTestId={TableCardViewTestId.viewVersionDetail}
                  >
                    {t('txt_view')}
                  </Button>
                </div>
              </div>
            ))}
          </div>
        )}
        <div className="method-card-expand">
          <Tooltip
            element={isExpand ? t('txt_collapse') : t('txt_expand')}
            variant="primary"
            placement="top"
          >
            {' '}
            <Button
              variant="outline-primary"
              className="method-card-expand-btn"
              onClick={handleToggleCard}
              dataTestId={TableCardViewTestId.toggleCard}
            >
              <Icon
                name={isExpand ? 'minus' : 'plus'}
                className="mr-0 color-grey-l16"
                size="4x"
              />
            </Button>
          </Tooltip>
        </div>
      </div>
    </div>
  );
};

export default TableCardView;
