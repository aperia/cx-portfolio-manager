import { fireEvent, RenderResult } from '@testing-library/react';
import {
  mockUseDispatchFnc,
  mockUseSelectorFnc,
  renderComponent
} from 'app/utils';
import React from 'react';
import TableCardView, {
  ITableCardViewProps,
  StatusName,
  TableCardViewTestId
} from '.';

const mockUseDispatch = mockUseDispatchFnc();
const mockUseSelector = mockUseSelectorFnc();

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const idDomTest = 'test';
const statusNameValue = Object.values(StatusName);
const mockData: ITableCardViewProps = {
  id: 'testID',
  value: {
    tableId: 'tableId',
    tableName: 'tableName',
    versions: [...statusNameValue, ...['default status']]?.map(
      (status, index) => ({
        comment: 'comment1',
        decisionElements: [
          {
            immediateAllocation: '',
            name: 'decisionElement1',
            searchCode: 'searchCode1',
            value: '359'
          }
        ],
        effectiveDate: '2022-01-05T17:00:00Z',
        status,
        tableControlParameters: [],
        tableId: `versionTable${index}`,
        tableName: 'versionTableName1'
      })
    )
  },
  selectedVersion: { tableId: 'versionTable1' },
  isExpand: true,
  onToggle: jest.fn(),
  onSelectVersion: jest.fn(),
  onViewVersionDetail: jest.fn()
};
describe('Test NoDataFound', () => {
  let renderResult: RenderResult;
  const createWrapper = async (props: ITableCardViewProps) => {
    renderResult = await renderComponent(
      idDomTest,
      <TableCardView {...props} />
    );
  };

  beforeEach(() => {
    mockUseSelector.getMockImplementation();
    mockUseDispatch.mockImplementation(() => jest.fn());
  });

  it('test render and expand', async () => {
    await createWrapper(mockData);
    const { getAllByTestId, getByTestId } = renderResult;
    const btnSelectInputVersion = getAllByTestId(
      `${TableCardViewTestId.selectInputVersion}_dls-radio`
    )[0].querySelector('input');
    const btnSelectDivVersion = getAllByTestId(
      TableCardViewTestId.selectDivVersion
    )[0];
    const btnViewVersionDetail = getAllByTestId(
      `${TableCardViewTestId.viewVersionDetail}_dls-button`
    )[0];
    const btnToggleCard = getByTestId(
      `${TableCardViewTestId.toggleCard}_dls-button`
    );

    await fireEvent.click(btnSelectInputVersion!);
    await fireEvent.click(btnSelectDivVersion!);
    await fireEvent.click(btnViewVersionDetail!);
    await fireEvent.click(btnToggleCard!);

    expect(btnToggleCard).toBeInTheDocument();
  });

  it('test render not expand', async () => {
    await createWrapper({
      ...mockData,
      isExpand: false,
      id: '',
      value: { ...mockData.value, versions: [mockData.value.versions[0]] }
    });
    const { getByTestId } = renderResult;

    const btnToggleCard = getByTestId(
      `${TableCardViewTestId.toggleCard}_dls-button`
    );
    expect(btnToggleCard).toBeInTheDocument();
  });
});
