import { TruncateText, TruncateTextProps } from 'app/_libraries/_dls';
import React, { useState } from 'react';

interface HtmlTruncateTextProps extends TruncateTextProps {
  raw: string;
}

const HtmlTruncateText: React.FC<HtmlTruncateTextProps> = ({
  children,
  raw,
  ellipsisLessText,
  ellipsisMoreText,
  ...props
}) => {
  const [showAll, setShowAll] = useState(false);
  const [hasTruncated, setHasTruncated] = useState(true);

  const handleTruncate = (isTruncated: boolean, showAll: boolean) => {
    setHasTruncated(isTruncated);
    isTruncated && setShowAll(showAll);
  };

  if (showAll || !hasTruncated) {
    return (
      <div className="custom-tooltip">
        <span
          dangerouslySetInnerHTML={{
            __html: children as any
          }}
        />
        {hasTruncated && (
          <>
            {' '}
            <span
              onClick={() => setShowAll(false)}
              className="link text-decoration-none"
            >
              {ellipsisLessText}
            </span>
          </>
        )}
      </div>
    );
  }

  return (
    <TruncateText
      {...props}
      ellipsisMoreText={ellipsisMoreText}
      onTruncate={handleTruncate}
    >
      {raw}
    </TruncateText>
  );
};

export default HtmlTruncateText;
