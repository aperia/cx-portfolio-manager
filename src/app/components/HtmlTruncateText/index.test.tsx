import { fireEvent, render } from '@testing-library/react';
import HtmlTruncateText from 'app/components/HtmlTruncateText';
import React from 'react';

const mockOnTruncateParams = jest.fn();
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    TruncateText: ({ onTruncate, children }: any) => (
      <div>
        {children}
        <button
          onFocus={() => onTruncate && onTruncate(...mockOnTruncateParams())}
        >
          On Truncate
        </button>
      </div>
    )
  };
});

describe('Test app/HtmlTruncateText', () => {
  it('render to UI', async () => {
    mockOnTruncateParams.mockReturnValue([true, true]);
    const element = (
      <HtmlTruncateText
        raw={'HtmlTruncateText'}
        ellipsisLessText={'HtmlTruncateText'}
        ellipsisMoreText={'HtmlTruncateText'}
      />
    );

    const wrapper = await render(element);
    wrapper.queryAllByText(/On Truncate/).forEach(element => {
      fireEvent.focus(element!);
    });
    fireEvent.click(wrapper.queryByText(/HtmlTruncateText/));
  });
  it('render to UI', async () => {
    mockOnTruncateParams.mockReturnValue([true, false]);
    const element = (
      <HtmlTruncateText
        raw={'HtmlTruncateText'}
        ellipsisLessText={'HtmlTruncateText'}
        ellipsisMoreText={'HtmlTruncateText'}
      />
    );

    const wrapper = await render(element);
    wrapper.queryAllByText(/On Truncate/).forEach(element => {
      fireEvent.focus(element!);
    });
  });
});
