import { Button, Icon, IconNames, useTranslation } from 'app/_libraries/_dls';
import React, { useMemo } from 'react';
import SrcImg from './blank-page.svg';

export interface NoDataFoundProps {
  id: string;
  title: string;
  hasSearch?: boolean;
  hasFilter?: boolean;
  className?: string;
  iconName?: IconNames;
  linkTitle?: string;
  onLinkClicked?: () => void;
  isFullPage?: boolean;
}
const NoDataFound: React.FC<NoDataFoundProps> = ({
  id = 'No_Data_Found',
  title,
  hasSearch,
  hasFilter,
  className,
  iconName,
  linkTitle,
  onLinkClicked,
  isFullPage = false
}) => {
  const { t } = useTranslation();
  const notFoundText = isFullPage
    ? t('txt_no_results_found_without_dot')
    : t('txt_no_results_found');
  title = hasSearch || hasFilter ? notFoundText : title;
  const description =
    hasFilter && hasSearch
      ? t('txt_adjust_your_search_or_filter')
      : hasFilter
      ? t('txt_select_another_filter')
      : hasSearch
      ? t('txt_adjust_your_search_criteria')
      : '';

  const sectionNotFound = useMemo(
    () => (
      <>
        <div className="mb-16 text-center">
          <Icon name={`${iconName || 'file'} color-light-l12 fs-80` as any} />
        </div>
        <p className="text-center color-grey">{title + ' ' + description}</p>
      </>
    ),
    [title, description, iconName]
  );
  const fullPageNotFound = useMemo(
    () => (
      <>
        <img className="pt-16 pb-26" src={SrcImg} title={title} alt={title} />

        <h4 className="mt-24">{title}</h4>
        <p className="mt-4 color-grey">{description}</p>
      </>
    ),
    [title, description]
  );

  return (
    <div
      id={id}
      className={`d-flex flex-column justify-content-center align-items-center ${className}`}
    >
      {isFullPage ? fullPageNotFound : sectionNotFound}
      {linkTitle && onLinkClicked && (
        <Button
          id={`${id}__btn`}
          variant="outline-primary"
          size="sm"
          className="mt-24"
          onClick={onLinkClicked}
        >
          {linkTitle}
        </Button>
      )}
    </div>
  );
};

export default NoDataFound;
