import { RenderResult } from '@testing-library/react';
import { renderComponent } from 'app/utils';
import React from 'react';
import NoDataFound, { NoDataFoundProps } from '.';

const idDomTest = 'test';
const defaultProps: NoDataFoundProps = {
  id: '1',
  title: 'title',
  className: 'class',
  onLinkClicked: jest.fn()
};
describe('Test NoDataFound', () => {
  let renderResult: RenderResult;
  const createWrapper = async (...props: NoDataFoundProps[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    renderResult = await renderComponent(
      idDomTest,
      <NoDataFound {...defaultProps} {...mergeProps} />
    );
  };

  it('test render no data to display', async () => {
    await createWrapper(defaultProps);
    const { getByText, queryByText } = renderResult;
    expect(getByText(/title/i)).toBeInTheDocument();
    expect(queryByText(/link/i)).not.toBeInTheDocument();
  });

  it('test render no result found due to searching', async () => {
    const hasSearchProps = {
      id: undefined,
      title: 'title',
      hasSearch: true,
      linkTitle: 'clear and reset'
    } as any;
    await createWrapper({ ...hasSearchProps });
    const { getByText, queryByText } = renderResult;
    expect(getByText(/txt_adjust_your_search_criteria/i)).toBeInTheDocument();
    expect(queryByText(/clear and reset/i)).toBeInTheDocument();
  });

  it('test render no result found due to filtering', async () => {
    const hasSearchProps = {
      id: undefined,
      title: 'title',
      hasFilter: true,
      linkTitle: 'clear and reset'
    } as any;
    await createWrapper({ ...hasSearchProps });
    const { getByText, queryByText } = renderResult;
    expect(getByText(/txt_select_another_filter/i)).toBeInTheDocument();
    expect(queryByText(/clear and reset/i)).toBeInTheDocument();
  });

  it('test render no result found due to searching and filtering', async () => {
    const hasSearchProps = {
      id: undefined,
      title: 'title',
      hasSearch: true,
      hasFilter: true,
      linkTitle: 'clear and reset'
    } as any;
    await createWrapper({ ...hasSearchProps });
    const { getByText, queryByText } = renderResult;
    expect(getByText(/txt_adjust_your_search_or_filter/i)).toBeInTheDocument();
    expect(queryByText(/clear and reset/i)).toBeInTheDocument();
  });

  it('test render no result found on full page', async () => {
    const hasSearchProps = {
      id: undefined,
      title: 'title',
      hasSearch: true,
      hasFilter: true,
      linkTitle: 'clear and reset',
      isFullPage: true
    } as any;
    await createWrapper({ ...hasSearchProps });
    const { getByText, queryByText } = renderResult;
    expect(getByText(/txt_no_results_found_without_dot/i)).toBeInTheDocument();
    expect(getByText(/txt_adjust_your_search_or_filter/i)).toBeInTheDocument();
    expect(queryByText(/clear and reset/i)).toBeInTheDocument();
  });
});
