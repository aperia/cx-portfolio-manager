import { RenderResult } from '@testing-library/react';
import PricingStrategyCardView, {
  PricingStrategyCardViewProps
} from 'app/components/PricingStrategyCardView';
import { renderComponent } from 'app/utils';
import { App as DOFApp } from 'app/_libraries/_dof/core';
import * as CommonRedux from 'pages/_commons/redux/Common/select-hooks';
import React from 'react';

const mockUseSelectWindowDimension = jest.spyOn(
  CommonRedux,
  'useSelectWindowDimension'
);
const mockData: IPricingStrategyModel = {} as IPricingStrategyModel;
const testId = 'pricingStrategyCardView';

describe('PricingStrategyCardView', () => {
  let renderResult: RenderResult;

  const createWrapper = async (...props: PricingStrategyCardViewProps[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    renderResult = await renderComponent(
      testId,
      <DOFApp urlConfigs={[]}>
        <PricingStrategyCardView {...(mergeProps as any)} />
      </DOFApp>
    );
  };

  const mockUseSelectWindowDimensionImplementation = (width: number) => {
    mockUseSelectWindowDimension.mockImplementation(() => ({ width } as any));
  };

  afterEach(() => {
    mockUseSelectWindowDimension.mockClear();
  });

  const it_should_render_pricing_strategy = async () => {
    const wrapper = await renderResult.findByTestId(testId);

    expect(
      wrapper.querySelector(`#pricing-strategy-card-data-${testId}`)
    ).toBeTruthy();
  };

  const it_should_render_empty_pricing_strategy = async () => {
    const wrapper = await renderResult.findByTestId(testId);

    expect(
      wrapper.querySelector('#pricing-strategy-card-data-empty')
    ).toBeTruthy();
  };

  it('should show pricing strategy', async () => {
    mockUseSelectWindowDimensionImplementation(900);
    await createWrapper({
      id: testId,
      value: mockData
    });

    await it_should_render_pricing_strategy();
  });

  it('should show empty pricing strategy', async () => {
    mockUseSelectWindowDimensionImplementation(1400);
    await createWrapper({} as any);

    await it_should_render_empty_pricing_strategy();
  });
});
