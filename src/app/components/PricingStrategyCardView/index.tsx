import { MIN_WIDTH_PC } from 'app/constants/constants';
import { classnames } from 'app/helpers';
import { View } from 'app/_libraries/_dof/core';
import React from 'react';

export interface PricingStrategyCardViewProps {
  id: string;
  value: IPricingStrategyModel;
  width?: number;
  onClick?: () => void;
}
const PricingStrategyCardView: React.FC<PricingStrategyCardViewProps> = ({
  id,
  value,
  width = window.innerWidth,
  onClick
}) => {
  const viewName =
    width > MIN_WIDTH_PC
      ? 'change-pricing-strategy-pc-view'
      : 'change-pricing-strategy-tablet-view';
  return (
    <div className={classnames('mt-12 col-12')}>
      <div
        id={`pricing-strategy-card-data-${id || 'empty'}`}
        className="list-view"
        onClick={onClick}
      >
        <View
          id={`change-pricing-strategy-view__${id || 'empty'}`}
          formKey={`change-pricing-strategy-view__${id || 'empty'}`}
          descriptor={viewName}
          value={value}
        />
      </div>
    </div>
  );
};

export default React.memo(PricingStrategyCardView);
