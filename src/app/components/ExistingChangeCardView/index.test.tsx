import { fireEvent, render, RenderResult } from '@testing-library/react';
import { ChangeType } from 'app/constants/enums';
import {
  CHANGE_STATUS_MAPPING,
  CHANGE_TYPE_MAPPING
} from 'app/constants/mapping';
import {
  WorkflowChangeSetList,
  WorkflowChangeSetListMapping
} from 'app/fixtures/workflow-changeset-list';
import { mappingDataFromObj } from 'app/helpers';
import * as helper from 'app/helpers/isDevice';
import React from 'react';
import ExistingChangeCardView, { ExistingChangeCardViewProps } from './index';

// mock DOMMatrix
(function mockDOMMatrix() {
  class DOMMatrixMock {
    scale = jest.fn();
    translate = jest.fn();
  }
  global.DOMMatrix = DOMMatrixMock as any;
})();

jest.mock('app/helpers/isDevice', () => ({
  __esModule: true,
  isDevice: false
}));

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const mockHelper: any = helper;

describe('Test ExistingChangeCardView Component', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  const mapWorkflowChangeSet = (
    change: Record<string, any>,
    overrideData?: Record<string, any>
  ) => {
    const p = mappingDataFromObj(change, WorkflowChangeSetListMapping);
    p.changeStatus = CHANGE_STATUS_MAPPING[p.changeStatus!];
    p.changeType = CHANGE_TYPE_MAPPING[p.changeType];
    if (p.changeType !== ChangeType.Pricing) {
      delete p.pricingStrategiesCreatedCount;
      delete p.pricingStrategiesUpdatedCount;
      delete p.pricingMethodsCreatedCount;
      delete p.pricingMethodsUpdatedCount;
      delete p.noOfDMMTablesCreated;
      delete p.noOfDMMTablesUpdated;
    } else {
      delete p.cardholdersAccountsImpacted;
      delete p.portfoliosImpacted;
    }
    return { ...p, ...(overrideData || {}) };
  };

  const exitingChangeCardViewProps = {
    id: 'mock-existing-change',
    value: mapWorkflowChangeSet(WorkflowChangeSetList[0]),
    workflowNote: 'Mock note',
    onClickWorkflowIncluded: jest.fn()
  } as ExistingChangeCardViewProps;

  const renderCardView = (
    props?: ExistingChangeCardViewProps
  ): RenderResult => {
    return render(
      <div>
        <ExistingChangeCardView {...exitingChangeCardViewProps} {...props} />
      </div>
    );
  };

  it('should render ExistingChangeCardView UI with workflow changeset data', async () => {
    const onSelectedFn = jest.fn();
    const wrapper = renderCardView({
      ...exitingChangeCardViewProps,
      onSelected: onSelectedFn
    } as ExistingChangeCardViewProps);

    const { queryByText } = wrapper;
    expect(queryByText(/Increase Fees for Black Card/i)).toBeInTheDocument();

    // trigger collaspe/expand tooltip on desktop
    fireEvent.mouseEnter(
      wrapper.baseElement.querySelector('.dls-popper-trigger')!
    );
    expect(queryByText(/txt_expand/i)).toBeInTheDocument();

    // select on card view
    fireEvent.click(
      wrapper.baseElement.querySelector(
        '#existing-change-card-view-' + exitingChangeCardViewProps.id
      )!
    );
    expect(onSelectedFn).toHaveBeenCalledWith(true);

    fireEvent.click(queryByText('31')!);
  });
  it('should render ExistingChangeCardView UI with lock icon', async () => {
    const onSelectedFn = jest.fn();
    const wrapper = renderCardView({
      ...exitingChangeCardViewProps,
      id: undefined as any,
      isLocked: true,
      onSelected: onSelectedFn
    } as ExistingChangeCardViewProps);

    const { queryByText } = wrapper;
    expect(queryByText(/Increase Fees for Black Card/i)).toBeInTheDocument();
    expect(
      wrapper.baseElement.querySelector('.icon.icon-lock')
    ).toBeInTheDocument();

    // select on locked card view
    fireEvent.click(
      wrapper.baseElement.querySelector('#existing-change-card-view-empty')!
    );

    expect(onSelectedFn).not.toHaveBeenCalled();
  });
  it('should render ExistingChangeCardView UI > expand & selected > show workflow note', async () => {
    const onChangeWorkflowNoteFn = jest.fn();
    const wrapper = renderCardView({
      ...exitingChangeCardViewProps,
      isExpand: true,
      isSelected: true,
      onChangeWorkflowNote: onChangeWorkflowNoteFn
    } as ExistingChangeCardViewProps);

    const { queryByText } = wrapper;
    expect(queryByText(/Mock note/i)).toBeInTheDocument();

    // change workflow note
    fireEvent.change(queryByText(/Mock note/i)!, {
      target: { value: 'mock note update' }
    });

    expect(onChangeWorkflowNoteFn).toHaveBeenCalledWith('mock note update');
  });
  it('should render ExistingChangeCardView UI > disable collaspe/expand tooltip on device', async () => {
    mockHelper.isDevice = true;
    const onExpandFn = jest.fn();
    const wrapper = renderCardView({
      ...exitingChangeCardViewProps,
      value: mapWorkflowChangeSet(WorkflowChangeSetList[0], {
        pricingStrategiesCreatedCount: 0,
        pricingStrategiesUpdatedCount: 0,
        pricingMethodsCreatedCount: 0,
        pricingMethodsUpdatedCount: 0,
        noOfDMMTablesCreated: 0,
        noOfDMMTablesUpdated: 0,
        workflowInstanceCount: 0
      }),
      onExpand: onExpandFn
    } as ExistingChangeCardViewProps);

    const { queryByText } = wrapper;

    // trigger collaspe/expand tooltip on device > disable
    fireEvent.mouseEnter(
      wrapper.baseElement.querySelector('.dls-popper-trigger')!
    );
    expect(queryByText(/txt_expand/i)).toBeNull();

    // trigger click on collaspe/expand button
    fireEvent.click(
      wrapper.baseElement.querySelector('button.btn-icon-secondary')!
    );
    expect(onExpandFn).toHaveBeenCalled();
  });
});
