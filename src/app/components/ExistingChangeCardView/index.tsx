import {
  BadgeColorType,
  ChangeType,
  GroupTextFormat,
  GroupTextFormatType
} from 'app/constants/enums';
import { classnames, formatCommon, isDevice } from 'app/helpers';
import {
  Button,
  Icon,
  Radio,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import { isFunction } from 'lodash';
import React, { useMemo } from 'react';
import BadgeGroupTextControl from '../DofControl/BadgeGroupTextControl';
import BubbleGroupTextControl from '../DofControl/BubbleGroupTextControl';
import GroupTextControl from '../DofControl/GroupTextControl';
import HeadingControl from '../DofControl/HeadingControl';
import FieldTooltip from '../FieldTooltip';
import TextAreaCountDown from '../TextAreaCountDown';

export interface ExistingChangeCardViewProps {
  id: string;
  value: IWorkflowChangeDetail;
  workflowNote: string;
  isSelected?: boolean;
  isExpand?: boolean;
  isLocked?: boolean;

  onChangeWorkflowNote?: (value: string) => void;
  onSelected?: (isSelected: boolean) => void;
  onExpand?: (isExpand: boolean) => void;
  onClickWorkflowIncluded?: () => void;
}
const ExistingChangeCardView: React.FC<ExistingChangeCardViewProps> = ({
  id,
  value,
  workflowNote,
  isSelected = false,
  isExpand = false,
  isLocked = false,
  onChangeWorkflowNote,
  onSelected,
  onExpand,
  onClickWorkflowIncluded
}) => {
  const { t } = useTranslation();

  const cardId = useMemo(() => `change-card-${id}`, [id]);

  const isPricing = useMemo(
    () => value.changeType === ChangeType.Pricing,
    [value.changeType]
  );

  const handleSelect = () => {
    !isLocked && isFunction(onSelected) && onSelected(!isSelected);
  };
  const handleChangeWorkflowNote = (
    e: React.ChangeEvent<HTMLTextAreaElement>
  ) => {
    isFunction(onChangeWorkflowNote) && onChangeWorkflowNote(e.target.value);
  };
  const handleExpand = (e: React.MouseEvent) => {
    e.stopPropagation();

    isFunction(onExpand) && onExpand(!isExpand);
  };
  const handleClickWorkflowIncluded = (e: React.MouseEvent) => {
    e.stopPropagation();

    isFunction(onClickWorkflowIncluded) && onClickWorkflowIncluded();
  };

  return (
    <label
      id={`existing-change-card-view-${id || 'empty'}`}
      className={classnames(
        'list-view mt-12 custom-control-root d-block mb-0',
        {
          'bg-blue-l40 br-blue': isSelected,
          'cursor-default': isLocked
        }
      )}
      htmlFor={cardId}
    >
      <div className="row">
        <div className="col-5 col-lg-4">
          <div className="d-flex">
            <div className="d-flex pt-2">
              {isPricing && (
                <Tooltip
                  variant="primary"
                  opened={isDevice ? false : undefined}
                  element={t(!isExpand ? 'txt_expand' : 'txt_collapse')}
                  triggerClassName="mr-16"
                >
                  <Button
                    variant="icon-secondary"
                    size="sm"
                    onClick={handleExpand}
                  >
                    <Icon name={isExpand ? 'minus' : 'plus'} />
                  </Button>
                </Tooltip>
              )}
              {isLocked && (
                <Tooltip
                  triggerClassName="mr-16"
                  element={t('txt_existing_change_locked_message')}
                >
                  <Icon name="lock" size="5x" className="color-grey-l16" />
                </Tooltip>
              )}
              {!isLocked && (
                <Radio className="mr-16">
                  <Radio.Input
                    id={cardId}
                    onChange={handleSelect}
                    className="checked-style"
                    checked={isSelected}
                  />
                </Radio>
              )}
            </div>
            <div className="w-100">
              <HeadingControl
                id={`existingChange_${id}_changeName`}
                as="h5"
                input={{ value: value?.changeName } as any}
                meta={{} as any}
                truncate
              />
              <div className="pt-2">
                <BadgeGroupTextControl
                  id={`existingChange_${id}_changeStatus`}
                  input={{ value: value?.changeStatus } as any}
                  options={{ colorType: BadgeColorType.CHANGE_STATUS }}
                  meta={{} as any}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="col-7 col-lg-8">
          <div className="row mr-n12">
            <div className="col-12 col-lg-4">
              <div className="mt-n4">
                <GroupTextControl
                  id={`existingChange_${id}_changeId`}
                  input={{ value: value?.changeId } as any}
                  meta={{} as any}
                  label={t('txt_change_id') + ':'}
                />
              </div>
              <GroupTextControl
                id={`existingChange_${id}_effectiveDate`}
                input={{ value: value?.effectiveDate } as any}
                meta={{} as any}
                options={{ format: GroupTextFormat.DateUTC }}
                label={t('txt_effective_date') + ':'}
              />
            </div>
            <div className="col-12 mt-4 col-lg-4 mt-lg-0">
              <div className="mt-n4">
                <GroupTextControl
                  id={`existingChange_${id}_createdDate`}
                  input={{ value: value?.createdDate } as any}
                  meta={{} as any}
                  options={{ format: GroupTextFormat.Date }}
                  label={t('txt_created_date') + ':'}
                />
              </div>
              <BubbleGroupTextControl
                id={`existingChange_${id}_changeOwner`}
                input={{ value: value?.changeOwner } as any}
                meta={{} as any}
                label={t('txt_change_owner') + ':'}
              />
            </div>
            <div className="col-12 col-lg-4">
              <div className="mt-4 mt-lg-0">
                <span className="color-grey mr-2">
                  {t('txt_workflow_included')}:
                </span>
                {(value?.workflowInstanceCount || 0) > 0 ? (
                  <button
                    className="border-0 p-0 link text-decoration-none"
                    onClick={handleClickWorkflowIncluded}
                  >
                    {formatCommon(value.workflowInstanceCount!).quantity}
                  </button>
                ) : (
                  <span>0</span>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>

      <div
        className={classnames('mt-8 mb-0 mx-n16', { 'd-none': !isExpand })}
      />
      {isSelected && isExpand && <hr className="my-0 mx-n16" />}
      <div
        className={classnames('row mb-n8 py-8 mx-n16 px-4', {
          'br-bottom-left-radius-8 br-bottom-right-radius-8 bg-light-l16':
            !isSelected,
          'd-none': !isExpand
        })}
      >
        <div className="col-5 col-lg-4 pt-2">
          <HeadingControl
            id={`existingChange_${id}_pricingStrategies`}
            as="h6"
            headingClassName="text-uppercase mb-8 ls-5 fw-600"
            input={{ value: 'Pricing Strategies' } as any}
            meta={{} as any}
          />
          <HeadingControl
            id={`existingChange_${id}_pricingMethods`}
            as="h6"
            headingClassName="text-uppercase mb-8 ls-5 fw-600"
            input={{ value: 'Pricing methods' } as any}
            meta={{} as any}
          />
          <HeadingControl
            id={`existingChange_${id}_DMMTables`}
            as="h6"
            headingClassName="text-uppercase mb-8 ls-5 fw-600"
            input={{ value: 'DMM Tables' } as any}
            meta={{} as any}
          />
        </div>
        <div className="col-7 col-lg-8 mt-n4">
          <div className="row mx-n12">
            <div className="col-4 col-lg-4">
              <GroupTextControl
                id={`existingChange_${id}_pricingStrategiesCreatedCount`}
                input={
                  { value: value?.pricingStrategiesCreatedCount || 0 } as any
                }
                meta={{} as any}
                options={{
                  format: GroupTextFormat.Amount,
                  formatType: GroupTextFormatType.Quantity
                }}
                label={t('txt_created') + ':'}
              />
            </div>
            <div className="col-4 col-lg-4">
              <GroupTextControl
                id={`existingChange_${id}_pricingStrategiesUpdatedCount`}
                input={
                  { value: value?.pricingStrategiesUpdatedCount || 0 } as any
                }
                meta={{} as any}
                options={{
                  format: GroupTextFormat.Amount,
                  formatType: GroupTextFormatType.Quantity
                }}
                label={t('txt_updated') + ':'}
              />
            </div>
            <div className="col-4 col-lg-4">
              <GroupTextControl
                id={`existingChange_${id}_pricingStrategiesImpacted`}
                input={{ value: value?.pricingStrategiesImpacted || 0 } as any}
                meta={{} as any}
                options={{
                  format: GroupTextFormat.Amount,
                  formatType: GroupTextFormatType.Quantity
                }}
                label={t('txt_impacted') + ':'}
              />
            </div>
            <div className="col-4 col-lg-4">
              <GroupTextControl
                id={`existingChange_${id}_pricingMethodsCreatedCount`}
                input={{ value: value?.pricingMethodsCreatedCount || 0 } as any}
                meta={{} as any}
                options={{
                  format: GroupTextFormat.Amount,
                  formatType: GroupTextFormatType.Quantity
                }}
                label={t('txt_created') + ':'}
              />
            </div>
            <div className="col-8 col-lg-8">
              <GroupTextControl
                id={`existingChange_${id}_pricingMethodsUpdatedCount`}
                input={{ value: value?.pricingMethodsUpdatedCount || 0 } as any}
                meta={{} as any}
                options={{
                  format: GroupTextFormat.Amount,
                  formatType: GroupTextFormatType.Quantity
                }}
                label={t('txt_updated') + ':'}
              />
            </div>
            <div className="col-4 col-lg-4">
              <GroupTextControl
                id={`existingChange_${id}_noOfDMMTablesCreated`}
                input={{ value: value?.noOfDMMTablesCreated || 0 } as any}
                meta={{} as any}
                options={{
                  format: GroupTextFormat.Amount,
                  formatType: GroupTextFormatType.Quantity
                }}
                label={t('txt_created') + ':'}
              />
            </div>
            <div className="col-4 col-lg-4">
              <GroupTextControl
                id={`existingChange_${id}_noOfDMMTablesUpdated`}
                input={{ value: value?.noOfDMMTablesUpdated || 0 } as any}
                meta={{} as any}
                options={{
                  format: GroupTextFormat.Amount,
                  formatType: GroupTextFormatType.Quantity
                }}
                label={t('txt_updated') + ':'}
              />
            </div>
          </div>
        </div>
      </div>
      {isSelected && (
        <>
          <hr className="my-8 mx-n16" />
          <div>
            <FieldTooltip
              placement="top-start"
              element="This note will appear in the workflow list on the change."
            >
              <TextAreaCountDown
                label={t('txt_workflow_note')}
                id={`existingChange_${id}_workflowNote`}
                value={workflowNote}
                onChange={handleChangeWorkflowNote}
              />
            </FieldTooltip>
          </div>
        </>
      )}
    </label>
  );
};

export default ExistingChangeCardView;
