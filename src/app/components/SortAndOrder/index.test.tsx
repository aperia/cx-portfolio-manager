import { fireEvent } from '@testing-library/react';
import SortAndOrder from 'app/components/SortAndOrder';
// components
import { ORDER_BY_LIST } from 'app/constants/constants';
import { renderWithMockStore } from 'app/utils/renderWithMockStore';
import React from 'react';

const mockSortByField = [
  {
    value: 'value1',
    description: 'value 1'
  },
  {
    value: 'value2',
    description: 'value 2'
  }
];
const mockProps = {
  sortByFields: mockSortByField,
  sortByValueDefault: mockSortByField[1],
  sortByValue: mockSortByField[1],

  orderByValueDefault: ORDER_BY_LIST[1],
  orderByValue: ORDER_BY_LIST[1],

  onOrderByChange: () => {},
  onSortByChange: () => {}
};

const testId = 'sortAndOrder';
describe('Sort And Order RAC', () => {
  describe('PC', () => {
    it('render sort by and order by as dropdown list', async () => {
      const storeConfig = {
        initialState: {
          common: {
            window: {
              width: 1920,
              height: 1080
            }
          }
        }
      };
      const element = <SortAndOrder {...mockProps} />;
      const { getByText, findByTestId } = await renderWithMockStore(
        element,
        storeConfig,
        {},
        testId
      );
      expect(getByText('Sort by:')).not.toBeEmptyDOMElement();
      expect(getByText('Order by:')).not.toBeEmptyDOMElement();

      const wrapper = await findByTestId(testId);
      const allDropdown = wrapper.querySelectorAll(
        '.text-field-container > .input'
      );

      // Sort by
      const sortByDropdown = allDropdown.item(0);
      expect(sortByDropdown.textContent).toEqual('value 2');
      fireEvent.focus(sortByDropdown);
      fireEvent.click(
        document.body.querySelector('.dls-popup')!.querySelector('.item')!
      );

      // Order by
      const orderByDropdown = allDropdown.item(1);
      expect(orderByDropdown.textContent).toEqual(ORDER_BY_LIST[1].description);
      fireEvent.focus(orderByDropdown);
      fireEvent.click(
        document.body
          .querySelectorAll('.dls-popup')!
          .item(1)!
          .querySelector('.item')!
      );
    });
  });

  describe('Tablet', () => {
    it('render sort by and order by as icon popup', async () => {
      const storeConfig = {
        initialState: {
          common: {
            window: {
              width: 1024,
              height: 960
            }
          }
        }
      };
      const element = <SortAndOrder {...mockProps} />;
      const { findByTestId } = await renderWithMockStore(
        element,
        storeConfig,
        {},
        testId
      );

      const wrapper = await findByTestId(testId);
      const filterBtn = wrapper.querySelector('button.btn');

      // close by click filter button
      fireEvent.click(filterBtn!); // open
      fireEvent.click(filterBtn!); // close

      // close by click outside
      fireEvent.click(filterBtn!); // open
      fireEvent.mouseDown(document.body); // close

      fireEvent.click(filterBtn!); // open
      // change sort by
      const sortByDropdown = document.body.querySelector(
        '.dls-popper-container .dls-dropdown-list .input'
      );
      fireEvent.focus(sortByDropdown!); // select sort by
      const value1SortByItem = document.body.querySelector(
        '.dls-popup .dls-dropdown-base-container .item'
      );
      fireEvent.click(value1SortByItem!);
      expect(sortByDropdown!.textContent).toEqual('value 1');

      // change order by
      const ascInput: HTMLInputElement | null = document.body.querySelector(
        '.dls-popper-container .dls-radio > input[type="radio"]'
      );
      fireEvent.click(ascInput!); // select order by asc
      expect(ascInput!.checked).toEqual(true);

      // reset to default
      const resetToDefaultBtn = document.body.querySelector(
        '.dls-popper-container button.btn.btn-secondary'
      );
      fireEvent.click(resetToDefaultBtn!);
      expect(ascInput!.checked).toEqual(false);

      // apply sort
      const applyBtn = document.body.querySelector(
        '.dls-popper-container button.btn.btn-primary'
      );
      fireEvent.click(applyBtn!);
    });

    it('render order by only', async () => {
      const storeConfig = {
        initialState: {
          common: {
            window: {
              width: 1024,
              height: 960
            }
          }
        }
      };
      const element = (
        <SortAndOrder {...mockProps} sortByFields={[mockSortByField[1]]} />
      );
      const { findByTestId } = await renderWithMockStore(
        element,
        storeConfig,
        {},
        testId
      );

      const wrapper = await findByTestId(testId);
      const filterBtn = wrapper.querySelector('button.btn');

      fireEvent.click(filterBtn!); // open
      // NO sort by element
      const sortByDropdown = document.body.querySelector(
        '.dls-popper-container .dls-dropdown-list .input'
      );
      expect(sortByDropdown).not.toBeTruthy();
    });
  });
});
