import { ORDER_BY_LIST } from 'app/constants/constants';
import { DropdownList } from 'app/_libraries/_dls';
import React, { useMemo } from 'react';
import { SortAndOrderProps } from '..';

const SortAndOrder: React.FC<SortAndOrderProps> = ({
  sortByFields,
  sortByValue,
  orderByValue,
  onSortByChange,
  onOrderByChange,
  isHideSort
}) => {
  const sortByListItems = useMemo(() => {
    return sortByFields.map(item => (
      <DropdownList.Item
        key={item.value}
        label={item.description}
        value={item}
      />
    ));
  }, [sortByFields]);

  const orderByListItems = useMemo(() => {
    return ORDER_BY_LIST.map(item => (
      <DropdownList.Item
        key={item.value}
        label={item.description}
        value={item}
      />
    ));
  }, []);

  return (
    <div className="d-flex align-items-center">
      {!isHideSort && (
        <div className="d-flex align-items-center mr-24">
          <strong className="fs-14 color-grey mr-4">Sort by:</strong>
          <DropdownList
            name="sortBy"
            value={sortByValue}
            textField="description"
            onChange={event => {
              onSortByChange!(event.target.value);
            }}
            variant="no-border"
          >
            {sortByListItems}
          </DropdownList>
        </div>
      )}
      <div className="d-flex align-items-center">
        <strong className="fs-14 color-grey mr-4">Order by:</strong>
        <DropdownList
          name="orderBy"
          value={orderByValue}
          textField="description"
          onChange={event => {
            const orderBy = event.target.value;
            onOrderByChange!(orderBy);
          }}
          variant="no-border"
        >
          {orderByListItems}
        </DropdownList>
      </div>
    </div>
  );
};

export default SortAndOrder;
