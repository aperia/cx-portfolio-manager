import { MIN_WIDTH_PC } from 'app/constants/constants';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import React from 'react';
import SortAndOrderPC from './OnPC';
import SortAndOrderTablet from './OnTablet';

export interface SortByItemProps {
  description: string;
  value: string;
}
export interface OrderByItemProps {
  description: string;
  value: OrderByType;
}

export interface SortAndOrderProps {
  sortByFields: SortByItemProps[];
  small?: boolean;
  sortByValueDefault?: SortByItemProps;
  orderByValueDefault?: OrderByItemProps;
  sortByValue?: SortByItemProps;
  orderByValue?: OrderByItemProps;
  width?: number;
  onSortByChange?: (sortBy: SortByItemProps | undefined) => void;
  onOrderByChange?: (orderBy: OrderByItemProps | undefined) => void;
  isHideSort?: boolean;
}
const SortAndOrder: React.FC<SortAndOrderProps> = props => {
  const { width } = useSelectWindowDimension();
  const { small } = props;
  const Component =
    width <= MIN_WIDTH_PC || !!small ? SortAndOrderTablet : SortAndOrderPC;
  return (
    <React.Fragment>
      <div className="d-flex align-items-center">
        <Component {...props} />
      </div>
    </React.Fragment>
  );
};

export default SortAndOrder;
