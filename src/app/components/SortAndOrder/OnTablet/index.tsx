import { ORDER_BY_LIST } from 'app/constants/constants';
import { OrderBy } from 'app/constants/enums';
import { isDevice } from 'app/helpers';
import {
  Button,
  DropdownList,
  Icon,
  Popover,
  Radio,
  Tooltip
} from 'app/_libraries/_dls';
import classnames from 'classnames';
import find from 'lodash.find';
import isEqual from 'lodash.isequal';
import React, { useCallback, useMemo, useState } from 'react';
import { OrderByItemProps, SortAndOrderProps, SortByItemProps } from '..';

const SortAndOrderTablet: React.FC<SortAndOrderProps> = ({
  sortByFields,
  sortByValueDefault,
  orderByValueDefault,
  sortByValue: sortByValueProps,
  orderByValue: orderByValueProps,
  onSortByChange,
  onOrderByChange,
  isHideSort
}) => {
  const [sortByValue, setSortByValue] =
    useState<SortByItemProps | undefined>(sortByValueProps);
  const [orderByValue, setOrderByValue] =
    useState<OrderByItemProps | undefined>(orderByValueProps);

  const isDefaultSort =
    isEqual(orderByValueProps?.value, orderByValueDefault?.value) &&
    isEqual(sortByValueProps?.value, sortByValueDefault?.value);

  const [isOpenPopover, setOpenPopover] = useState(false);

  const handleTogglePopper = useCallback(() => {
    if (!isOpenPopover) {
      setSortByValue(sortByValueProps);
      setOrderByValue(orderByValueProps);
    }
    setOpenPopover(!isOpenPopover);
  }, [isOpenPopover, sortByValueProps, orderByValueProps]);

  const handleVisibilityChange = useCallback(isOpen => {
    setOpenPopover(isOpen);
  }, []);

  const handleChangeRadio = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      const { value } = event.target;
      setOrderByValue(find(ORDER_BY_LIST, ['value', value]));
    },
    []
  );

  const resetToDefault = useCallback(() => {
    setSortByValue(sortByValueDefault);
    setOrderByValue(orderByValueDefault);
  }, [sortByValueDefault, orderByValueDefault]);

  const handleApply = useCallback(() => {
    handleTogglePopper();
    onSortByChange!(sortByValue);
    onOrderByChange!(orderByValue);
  }, [
    handleTogglePopper,
    orderByValue,
    sortByValue,
    onOrderByChange,
    onSortByChange
  ]);

  const sortByListItems = useMemo(() => {
    return sortByFields ? (
      sortByFields.map(item => (
        <DropdownList.Item
          key={item.value}
          label={item.description}
          value={item}
        />
      ))
    ) : (
      <React.Fragment></React.Fragment>
    );
  }, [sortByFields]);

  const renderSortBy = useMemo(() => {
    return sortByFields?.length !== 1 ? (
      <DropdownList
        id="value"
        name="sortBy"
        textField="description"
        label="Sort by"
        value={sortByValue}
        onChange={event => {
          const sortBy = event.target.value;
          setSortByValue(sortBy);
        }}
        popupBaseProps={{
          popupBaseClassName: 'inside-infobar sortby-tablet-popup'
        }}
      >
        {sortByListItems}
      </DropdownList>
    ) : (
      <div className="d-flex mt-16">
        <div className="w-30 color-grey fs-14 fw-500">Sort by:</div>
        <div>{sortByFields[0].description}</div>
      </div>
    );
  }, [sortByValue, sortByListItems, sortByFields]);

  const renderOrderBy = useMemo(() => {
    return (
      <div className="d-flex mt-16">
        <p className="w-30 color-grey fs-14 fw-500">Order by:</p>
        <Radio className="mr-24">
          <Radio.Input
            onChange={handleChangeRadio}
            checked={orderByValue?.value === OrderBy.ASC}
            value={OrderBy.ASC}
            name="sortBy"
            id="ASC"
          />
          <Radio.Label>Ascending</Radio.Label>
        </Radio>
        <Radio>
          <Radio.Input
            onChange={handleChangeRadio}
            checked={orderByValue?.value === OrderBy.DESC}
            value={OrderBy.DESC}
            name="sortBy"
            id="DESC"
          />
          <Radio.Label>Descending</Radio.Label>
        </Radio>
      </div>
    );
  }, [orderByValue, handleChangeRadio]);

  const renderFooter = useMemo(() => {
    return (
      <div className="d-flex mt-24 justify-content-end">
        <Button onClick={resetToDefault} size="sm" variant="secondary">
          Reset to Default
        </Button>
        <Button size="sm" onClick={handleApply} variant="primary">
          Apply
        </Button>
      </div>
    );
  }, [resetToDefault, handleApply]);

  return (
    <Tooltip
      placement="top"
      element="Sort and Order"
      variant="primary"
      opened={isDevice || isOpenPopover ? false : undefined}
    >
      <Popover
        size="md"
        placement="bottom-end"
        containerClassName="dls-tooltip-container"
        opened={isOpenPopover}
        onVisibilityChange={handleVisibilityChange}
        element={
          <>
            <h4 className="mb-16">Sort and Order</h4>
            {!isHideSort && renderSortBy}
            {renderOrderBy}
            {renderFooter}
          </>
        }
      >
        <Button
          variant="icon-secondary"
          className={classnames({
            asterisk: !isDefaultSort,
            active: isOpenPopover
          })}
          onClick={handleTogglePopper}
        >
          <Icon name="sort-by" />
        </Button>
      </Popover>
    </Tooltip>
  );
};

export default SortAndOrderTablet;
