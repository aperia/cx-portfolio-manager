import { renderComponent } from 'app/utils';
import { App as DOFApp } from 'app/_libraries/_dof/core';
import React from 'react';
import PortfolioCardView, { PortfolioCardViewProps } from '.';

const idDomTest = 'testId';
const defaultProps: PortfolioCardViewProps = {
  id: '123',
  value: {
    id: '1',
    portfolioName: 'name',
    portfolioId: '2',
    icon: 'icon',
    cardType: 'type',
    cardTypeUrl: 'url',
    clientId: '3',
    portfoliosImpacted: 1000,
    totalDelinquency: 1000,
    availableAccounts: 1000,
    netIncome: 1000,
    availableCredit: 1000,
    portfolioNew: 1000,
    spa: ['spa1'],
    activeAccounts: 1000,
    payments: 1000
  },
  onClick: jest.fn()
};

describe('PortfolioCardView', () => {
  let wrapper: HTMLElement;

  const createWrapper = async (...props: PortfolioCardViewProps[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );

    const renderResult = await renderComponent(
      idDomTest,
      <DOFApp urlConfigs={[]}>
        <PortfolioCardView {...defaultProps} {...mergeProps} />
      </DOFApp>
    );
    wrapper = await renderResult.findByTestId(idDomTest);
  };

  it('should show portfolio card grid data', async () => {
    await createWrapper();
    expect(
      wrapper.querySelector(`#portfolio-card-data-${defaultProps.id}`)
    ).toBeTruthy();
  });

  it('should show portfolio card data', async () => {
    await createWrapper({ ...defaultProps, isListViewMode: false });
    expect(
      wrapper.querySelector(`#portfolio-card-data-${defaultProps.id}`)
    ).toBeTruthy();
  });

  it('should show portfolio card empty', async () => {
    await createWrapper({ ...defaultProps, id: '' });
    expect(wrapper.querySelector(`#portfolio-card-data-empty`)).toBeTruthy();
  });
});
