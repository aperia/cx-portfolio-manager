import { classnames } from 'app/helpers';
import { View } from 'app/_libraries/_dof/core';
import React from 'react';

export interface PortfolioCardViewProps {
  id: string;
  value: IPortfolioCardView;
  onClick?: () => void;
  isListViewMode?: boolean;
}
const PortfolioCardView: React.FC<PortfolioCardViewProps> = ({
  id,
  value,
  isListViewMode = true,
  onClick
}) => {
  return (
    <div
      className={classnames(
        'mt-12 ',
        isListViewMode ? 'col-12' : 'col-12 box-list-view'
      )}
    >
      <div
        id={`portfolio-card-data-${id || 'empty'}`}
        className="list-view"
        onClick={onClick}
      >
        <View
          id={`portfolio-card-view__${id || 'empty'}`}
          formKey={`portfolio-card-view__${id || 'empty'}`}
          descriptor="portfolio-card-view"
          value={value}
        />
      </div>
    </div>
  );
};

export default React.memo(PortfolioCardView);
