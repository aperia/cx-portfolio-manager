import { render, RenderResult } from '@testing-library/react';
import * as helper from 'app/helpers/isDevice';
import 'app/utils/_mockComponent/mockCanvas';
import { DatePickerProps } from 'app/_libraries/_dls';
import React from 'react';
import EnhanceDatePicker from './index';

jest.mock('app/helpers/isDevice', () => ({
  __esModule: true,
  isDevice: false
}));
const mockHelper: any = helper;

describe('Test EnhanceDatePicker Component', () => {
  const mockPickerProps = {
    value: new Date()
  } as DatePickerProps;

  const renderDatePicker = (props?: DatePickerProps): RenderResult => {
    return render(
      <div>
        <EnhanceDatePicker
          ref={() => {
            // useRef<DatePickerRef>(null);
          }}
          {...mockPickerProps}
          {...props}
        ></EnhanceDatePicker>
      </div>
    );
  };

  const wait = (timeout = 0) =>
    new Promise(resolve => {
      const id = setTimeout(() => {
        clearTimeout(id);
        resolve(true);
      }, timeout);
    });

  it('should render Enhance DatePicker on device', async () => {
    mockHelper.isDevice = true;
    renderDatePicker();

    const value = document.querySelector('.text-field-container input');
    await wait();
    expect(value).toBeInTheDocument();
  });
});
