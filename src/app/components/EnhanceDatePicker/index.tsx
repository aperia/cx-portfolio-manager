import { isDevice } from 'app/helpers';
import {
  DatePicker,
  DatePickerProps,
  DatePickerRef
} from 'app/_libraries/_dls';
import React, { useImperativeHandle, useRef } from 'react';

const EnhanceDatePicker: React.ForwardRefRenderFunction<
  DatePickerRef,
  DatePickerProps
> = (props, ref) => {
  const datePickerRef = useRef<DatePickerRef>(null);
  useImperativeHandle(ref, () => datePickerRef.current!);

  return <DatePicker ref={datePickerRef} {...props} canInput={!isDevice} />;
};

export default React.forwardRef(EnhanceDatePicker);
