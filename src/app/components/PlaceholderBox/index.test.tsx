import { RenderResult } from '@testing-library/react';
import PlaceholderBox, {
  PlaceholderBoxProps
} from 'app/components/PlaceholderBox';
import { renderComponent } from 'app/utils';
import React from 'react';

const idDomTest = 'placeholder-box';
const defaultProps: PlaceholderBoxProps = {
  id: 'id',
  title: 'title',
  className: 'className'
};

describe('PlaceholderBox', () => {
  let renderResult: RenderResult;

  const createWrapper = async (...props: PlaceholderBoxProps[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    renderResult = await renderComponent(
      idDomTest,
      <PlaceholderBox {...mergeProps} />
    );
  };
  it('should show place holder box', async () => {
    await createWrapper(defaultProps);
    const { getByText } = renderResult;
    expect(getByText(/title/i)).toBeInTheDocument();
  });
  it('should NOT show place holder box', async () => {
    await createWrapper();
    const { queryByText } = renderResult;
    expect(queryByText(/title/i)).toBeNull();
  });
});
