import React from 'react';

export interface PlaceholderBoxProps {
  id?: string;
  title?: string;
  className?: string;
}

const PlaceholderBox: React.FC<PlaceholderBoxProps> = ({
  id = 'placeholderBox',
  title = '',
  className
}) => {
  return (
    <div
      id={`${id}`}
      className={`d-flex justify-content-center align-items-center bg-grey-l32 placeholder-bg-color ${className}`}
    >
      <h5 className="text-uppercase color-grey">{title}</h5>
    </div>
  );
};

export default PlaceholderBox;
