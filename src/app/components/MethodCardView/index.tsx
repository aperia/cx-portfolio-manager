import {
  Button,
  Icon,
  Radio,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import { View } from 'app/_libraries/_dof/core';
import React, { useCallback, useMemo } from 'react';
import GroupTextControl from '../DofControl/GroupTextControl';

export interface IMethodCardViewProps {
  id: string;
  serviceSubjectSection?:
    | 'RC'
    | 'LC'
    | 'DLO'
    | 'CPICID'
    | 'CPICIP'
    | 'CPICIM'
    | 'MSPS'
    | 'CPIC'
    | 'MSPS'
    | 'UIR'
    | 'IOM'
    | 'PEM'
    | 'CIT'
    | 'PBA'
    | 'CIA';
  value: IMethodModel;
  isCreateNewVersion?: boolean;
  selectedVersion?: string;
  isExpand?: boolean;
  isHideCommentArea?: boolean;
  onToggle?: (methodId: string) => void;
  onSelectVersion?: (method: IMethodModel, versionId: string) => void;
}

export const PCF_WORKFLOW_SUBJECT_SECTION = [
  'PEM',
  'UIR',
  'IOM',
  'MSPS',
  'PBA',
  'CIT',
  'CPICID',
  'CPICIP',
  'CPICIM',
  'DLO',
  'LC',
  'RC',
  'CIA'
];
const MethodCardView: React.FC<IMethodCardViewProps> = ({
  id,
  serviceSubjectSection = 'RC',
  isCreateNewVersion,
  value: valueProp,
  selectedVersion,
  onSelectVersion,
  isExpand,
  isHideCommentArea = false,
  onToggle
}) => {
  const { t } = useTranslation();
  const {
    numberOfVersions,
    versions,
    name,
    relatedPricingStrategies = []
  } = valueProp;
  const text = useMemo(() => {
    return numberOfVersions === 1
      ? numberOfVersions + ' Version'
      : numberOfVersions + ' Versions';
  }, [numberOfVersions]);
  const value = {
    ...valueProp,
    numberOfVersions: text,
    relatedPricingStrategies: {
      id: valueProp.id,
      name,
      relatedPricingStrategies,
      isCreateNewVersion
    }
  };

  const isPCFWorkFlow = PCF_WORKFLOW_SUBJECT_SECTION.includes(
    serviceSubjectSection
  );

  const handleToggleCard = useCallback(
    (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
      e.stopPropagation();
      onToggle && onToggle(value.id);
    },
    [onToggle, value?.id]
  );

  const handleSelectVersion = useCallback(
    (versionId: string) => {
      const onClick = (e: any) => {
        if (e.target.tagName === 'INPUT') return;
        onSelectVersion && onSelectVersion(valueProp, versionId);
      };
      return onClick;
    },
    [onSelectVersion, valueProp]
  );

  return (
    <div id={`method-card-data-${id || 'empty'}`} className="mt-12 col-12">
      <div className="list-view method-card-view py-8">
        <View
          id={`method-card-view-header__${id || 'empty'}`}
          formKey={`method-card-view-header__${id || 'empty'}`}
          descriptor={`method-card-view-header-${serviceSubjectSection}`}
          value={value}
        />
        {isExpand && (
          <div className="bg-light-l16 rounded-bottom-8 py-8 mx-n16 px-16 mb-n8 mt-10">
            {!isHideCommentArea && (
              <div className="mb-24 mt-n16">
                <GroupTextControl
                  id={`id_${value.id}_comment`}
                  input={{ value: value.comment } as any}
                  label={t('txt_comment_area')}
                  meta={{} as any}
                  options={{
                    valueClass: 'pre-line',
                    inline: false,
                    truncate: true,
                    lineTruncate: 2,
                    ellipsisMoreText: 'more',
                    ellipsisLessText: 'less'
                  }}
                />
              </div>
            )}

            <h6 className="mb-4">{t('txt_method_versions')}</h6>
            {versions.map((v, index) => (
              <div
                key={`${v.id}-${index}`}
                className="list-view py-12 mt-12 bg-white position-relative bg-checkbox-hover-light-l20 custom-control-root"
                onClick={handleSelectVersion(v.id)}
              >
                <View
                  id={`method-card-view__${v.id || 'empty'}`}
                  formKey={`method-card-view__${v.id || 'empty'}`}
                  descriptor={
                    isPCFWorkFlow
                      ? 'method-card-view-extend'
                      : 'method-card-view'
                  }
                  value={v}
                />
                <Radio className="select-version-radio">
                  <Radio.Input
                    className="checked-style"
                    checked={selectedVersion === v.id}
                  />
                </Radio>
              </div>
            ))}
          </div>
        )}
        <div className="method-card-expand">
          <Tooltip
            element={isExpand ? t('txt_collapse') : t('txt_expand')}
            variant="primary"
            placement="top"
          >
            {' '}
            <Button
              variant="outline-primary"
              className="method-card-expand-btn"
              onClick={handleToggleCard}
            >
              <Icon
                name={isExpand ? 'minus' : 'plus'}
                className="mr-0 color-grey-l16"
                size="4x"
              />
            </Button>
          </Tooltip>
        </div>
      </div>
    </div>
  );
};

export default MethodCardView;
