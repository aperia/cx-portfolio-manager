import { fireEvent, render, RenderResult } from '@testing-library/react';
import {
  GetMethodListData,
  GetMethodListDataMapping
} from 'app/fixtures/workflow-method-list';
import { mappingDataFromObj } from 'app/helpers';
import React from 'react';
import MethodCardView, { IMethodCardViewProps } from './index';

jest.mock('app/_libraries/_dof/core', () => ({
  View: ({ formKey, descriptor, onClick, value }: any) => {
    return (
      <div id={formKey}>
        {formKey}
        <span>{descriptor}</span>
        <button onClick={onClick}>Click on {formKey}</button>
        <div>{value?.name}</div>
      </div>
    );
  }
}));

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const getMappingMethodData = (method: Record<string, any>): IMethodModel => {
  return mappingDataFromObj(method, GetMethodListDataMapping);
};

describe('Test MethodCardView Component', () => {
  const methodCardViewProps = {
    id: 'mock-id',
    key: 'mock-key',
    value: {} as IMethodModel
  } as IMethodCardViewProps;

  const renderMethodCardView = (props?: IMethodCardViewProps): RenderResult => {
    return render(
      <div>
        <MethodCardView {...methodCardViewProps} {...props} />
      </div>
    );
  };

  it('should render MethodCardView UI', async () => {
    const toggleCardFn = jest.fn();
    const value = getMappingMethodData(GetMethodListData[0]);
    value.numberOfVersions = 1;
    const wrapper = renderMethodCardView({
      ...methodCardViewProps,
      value,
      onToggle: toggleCardFn
    });

    const { queryByText } = wrapper;
    expect(
      wrapper.baseElement.querySelector('#method-card-view-header__mock-id')
    ).toBeInTheDocument();
    expect(queryByText(value.name)).toBeInTheDocument();

    // trigger toggle expand card view
    fireEvent.click(
      wrapper.baseElement.querySelector('button.method-card-expand-btn')!
    );
    expect(toggleCardFn).toHaveBeenCalledWith(value.id);
  });

  it('should render MethodCardView UI > expanded', async () => {
    const onSelectVersionFn = jest.fn();
    const value = getMappingMethodData(GetMethodListData[0]);
    value.numberOfVersions = 2;
    const wrapper = renderMethodCardView({
      ...methodCardViewProps,
      isExpand: true,
      onSelectVersion: onSelectVersionFn,
      value
    });

    expect(
      wrapper.baseElement.querySelector('#method-card-view-header__mock-id')
    ).toBeInTheDocument();
    expect(
      wrapper.baseElement.querySelector('#method-card-view__93560569')
    ).toBeInTheDocument();

    // trigger select version > click on container div
    fireEvent.click(
      wrapper.baseElement.querySelector('#method-card-view__93560569')!
        .parentElement!
    );
    expect(onSelectVersionFn).toHaveBeenCalled();

    onSelectVersionFn.mockClear();

    // trigger select version > click on radio input
    fireEvent.click(wrapper.baseElement.querySelector('.dls-radio-input')!);
    expect(onSelectVersionFn).not.toHaveBeenCalled();
  });

  it('should render MethodCardView UI > empty ID and expanded', async () => {
    const value = getMappingMethodData(GetMethodListData[0]);
    value.versions = value.versions.map(v => ({ ...v, id: undefined as any }));
    const wrapper = renderMethodCardView({
      ...methodCardViewProps,
      id: undefined as any,
      isExpand: true,
      value
    });

    expect(
      wrapper.baseElement.querySelector('#method-card-view-header__empty')
    ).toBeInTheDocument();
    expect(
      wrapper.baseElement.querySelector('#method-card-view__empty')
    ).toBeInTheDocument();
  });
});
