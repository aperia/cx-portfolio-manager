import { ChangeAction } from 'app/constants/enums';
import { formatTime } from 'app/helpers';
import { Bubble } from 'app/_libraries/_dls';
import { actionsChange } from 'pages/_commons/redux/Change';
import React from 'react';
import { useDispatch } from 'react-redux';

export interface IActivityComponent extends IActivity {
  deletedWorkflowIds?: string[];
}

const Activity: React.FC<IActivityComponent> = props => {
  const { id, name, action, dateChanged, actionDetails, deletedWorkflowIds } =
    props;
  const { fullTimeMeridiem: time } = formatTime(dateChanged);
  const dispatch = useDispatch();
  const handleOpenWorkflowDetail = (workflowId: string) => {
    dispatch(actionsChange.openWorkflowDetail(workflowId));
  };

  return (
    <div id={`activity-${id || 'empty'}`}>
      <div className="mb-4">
        <span>
          <Bubble name={name} small />
        </span>
        <span className="fw-500"> {name}</span> <span>{action}</span>
      </div>
      <div className="pl-24 fs-12 text-secondary">{time}</div>
      <ul className="mt-8 mb-16 ml-4">
        {actionDetails?.map((d: IActivityDetail, index: number) => (
          <li key={index} className="color-grey">
            {d.field}:&nbsp;
            {!d.previousValue ? (
              <>
                {d.workflowId &&
                d.workflowName &&
                ChangeAction.DELETE_WORKFLOW !== action &&
                !deletedWorkflowIds?.includes(d.workflowId) ? (
                  <span
                    className="fw-500 link text-decoration-underline-dotted"
                    onClick={() => {
                      handleOpenWorkflowDetail(d.workflowId!);
                    }}
                  >
                    {d.newValue}
                  </span>
                ) : (
                  <span className="fw-500 color-grey-d20">{d.newValue}</span>
                )}
              </>
            ) : (
              <>
                from{' '}
                <span className="fw-500 color-grey-d20">{d.previousValue}</span>{' '}
                to
                <span className="fw-500 color-grey-d20"> {d.newValue}</span>
              </>
            )}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Activity;
