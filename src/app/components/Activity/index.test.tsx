import { fireEvent } from '@testing-library/react';
import Activity from 'app/components/Activity';
import { renderWithMockStore } from 'app/utils';
import { actionsChange } from 'pages/_commons/redux/Change';
import React from 'react';

const mockChangeData: IActivity = {
  id: '03362240',
  name: 'Rehaevo Lekawylae',
  action: 'sed takimata diam',
  dateChanged: '2021-01-20T11:36:53.0899088+07:00',
  actionDetails: [
    {
      field: 'diam accusam eros eirmod',
      previousValue: 'vero enim labore lorem illum',
      newValue: 'invidunt velit diam kasd tempor molestie invidunt'
    }
  ]
} as IActivity;

const mockWorkflowChangeData: IActivity = {
  id: '',
  name: 'Rehaevo Lekawylae',
  action: 'editted workflow',
  dateChanged: '2021-01-20T11:36:53.0899088+07:00',
  actionDetails: [
    {
      field: 'workflow name',
      previousValue: '',
      newValue: 'Workflow Name',
      workflowId: '1234',
      workflowName: '3456'
    }
  ]
} as IActivity;

const mockWorkflowEmptyIdChangeData: IActivity = {
  id: '',
  name: 'Rehaevo Lekawylae',
  action: 'editted workflow',
  dateChanged: '2021-01-20T11:36:53.0899088+07:00',
  actionDetails: [
    {
      field: 'workflow name',
      previousValue: '',
      newValue: 'Workflow Name',
      workflowId: undefined,
      workflowName: '3456'
    }
  ]
} as IActivity;

const mockNoChangeData: IActivity = {
  id: '',
  name: 'Rehaevo Lekawylae',
  action: 'editted change',
  dateChanged: '2021-01-20T11:36:53.0899088+07:00',
  actionDetails: [
    {
      field: 'abc',
      previousValue: '',
      newValue: 'change abc'
    }
  ]
} as IActivity;

describe('RAC Activity', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  it('display activity component has change value', async () => {
    const element = <Activity {...mockChangeData} />;
    const storeCofig = {
      initialState: {}
    };
    const { getByText } = await renderWithMockStore(element, storeCofig);
    expect(getByText('Rehaevo Lekawylae')).not.toBeEmptyDOMElement();
  });

  it('display activity component has workflow change value', async () => {
    jest.spyOn(actionsChange, 'openWorkflowDetail');
    const element = <Activity {...mockWorkflowChangeData} />;
    const storeCofig = {
      initialState: {}
    };
    const { getByText } = await renderWithMockStore(element, storeCofig);
    expect(getByText('Rehaevo Lekawylae')).not.toBeEmptyDOMElement();
    fireEvent.click(getByText('Workflow Name'));
    expect(actionsChange.openWorkflowDetail).toHaveBeenCalledWith('1234');
  });

  it('display activity component has workflow empty id', async () => {
    const element = <Activity {...mockWorkflowEmptyIdChangeData} />;
    const storeCofig = {
      initialState: {}
    };
    const { getByText } = await renderWithMockStore(element, storeCofig);
    expect(getByText('Rehaevo Lekawylae')).not.toBeEmptyDOMElement();
  });

  it('display activity component has no change value', async () => {
    const element = <Activity {...mockNoChangeData} />;
    const storeCofig = {
      initialState: {}
    };
    const { getByText } = await renderWithMockStore(element, storeCofig);
    expect(getByText('Rehaevo Lekawylae')).not.toBeEmptyDOMElement();
  });
});
