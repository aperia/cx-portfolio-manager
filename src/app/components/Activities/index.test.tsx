import Activities from 'app/components/Activities';
import { ActivityList } from 'app/fixtures/activity-data';
import { renderComponent } from 'app/utils';
import React from 'react';

const testId = 'activity-list';

jest.mock('app/components/Activity', () => {
  return {
    __esModule: true,
    default: ({ action }: any) => {
      return <div>{action}</div>;
    }
  };
});

describe('Activities', () => {
  it('should show empty activities', async () => {
    const element = <Activities activities={[]} />;

    const { getByTestId } = await renderComponent(testId, element);

    expect(
      getByTestId(testId).querySelector('#activity-card-list')
    ).toBeEmptyDOMElement();
  });

  it('should show activities with data', async () => {
    ActivityList.push({
      id: '111',
      dateChanged: '2021-02-27T12:55:19.325294+07:00',
      changeName: 'August Release',
      changeId: '21232',
      name: 'Jesse Dunn',
      action: 'approvedChange',
      actionDetails: [
        {
          field: 'rejectReason',
          previousValue: null,
          newValue: 'sanctus sadipscing sit',
          workflowName: null,
          workflowId: null
        }
      ]
    });
    const element = <Activities activities={ActivityList as any} />;

    const setHeight = jest.fn();

    jest.spyOn(React, 'useState').mockImplementation(() => [1, setHeight]);

    const div = document.createElement('div');
    div.setAttribute('id', 'activity-fly-out-head');
    const container = document.body.appendChild(div);

    const { getByText, getAllByText } = await renderComponent(testId, element, {
      container
    });

    expect(getByText('rejectedChange')).not.toBeEmptyDOMElement();
    expect(getAllByText('addedWorkflow').length).toBeGreaterThan(0);
    expect(setHeight).toBeCalled();
  });
});
