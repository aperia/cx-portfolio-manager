import { formatLogDate, formatTime } from 'app/helpers/formatTime';
import { ActivitiesState } from 'app/types';
import React, { useLayoutEffect } from 'react';
import Activity from '../Activity';

export interface IActivitiesComponent extends ActivitiesState {
  deletedWorkflowIds?: string[];
}

const Activities: React.FC<IActivitiesComponent> = ({
  activities: activitiesProps,
  deletedWorkflowIds
}) => {
  const [headerHeight, setHeaderHeight] = React.useState<number | undefined>(0);
  const activities = [...activitiesProps];
  const activitiesByDate = activities.reduce((accum, current) => {
    const updatedDate = formatTime(current.dateChanged).date as string;
    if (!accum[updatedDate]) accum[updatedDate] = [];
    accum[updatedDate].push(current);
    return accum;
  }, {} as any);

  // don't remove the below comment
  // eslint-disable-next-line react-hooks/exhaustive-deps
  useLayoutEffect(() => {
    const header = document.querySelector(
      '#activity-fly-out-head'
    ) as HTMLDivElement;
    header &&
      header.clientHeight !== headerHeight &&
      setHeaderHeight(header?.offsetHeight);
  });

  return (
    <div id="activity-card-list">
      {Object.keys(activitiesByDate).map((k, index) => {
        const date = formatLogDate(k);
        const dateActivities = activitiesByDate[k];
        return (
          <div key={index}>
            <div
              className="text-center fs-12 pb-16 fw-500 color-grey text-uppercase sticky"
              style={{ top: headerHeight }}
            >
              {date}
            </div>
            <div>
              {dateActivities.map((activity: IActivity) => (
                <Activity
                  key={activity.id}
                  {...activity}
                  deletedWorkflowIds={deletedWorkflowIds}
                />
              ))}
            </div>
            {index < Object.keys(activitiesByDate).length - 1 && (
              <hr className="mb-24" />
            )}
          </div>
        );
      })}
    </div>
  );
};

export default Activities;
