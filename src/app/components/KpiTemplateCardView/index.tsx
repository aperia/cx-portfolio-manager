import { View } from 'app/_libraries/_dof/core';
import React from 'react';

export interface KpiTemplateCardViewProps {
  id: string;
  value: IKpiTemplateModel;
  onClick?: () => void;
}
const KpiTemplateCardView: React.FC<KpiTemplateCardViewProps> = ({
  id,
  value,
  onClick
}) => {
  const selectValue = { ...value };

  return (
    <div
      id={`kpi-template-card-view-${id || 'empty'}`}
      className="list-view d-flex align-items-start mt-12 kpi-template-card-view"
      onClick={onClick}
    >
      <View
        id={`kpi-template-card-view__${id || 'empty'}`}
        formKey={`kpi-template-card-view__${id || 'empty'}`}
        descriptor={`kpi-template-card-view`}
        value={{ ...value, selectValue }}
      />
    </div>
  );
};

export default KpiTemplateCardView;
