import KpiTemplateCardView, {
  KpiTemplateCardViewProps
} from 'app/components/KpiTemplateCardView';
import { renderComponent } from 'app/utils';
import { App as DOFApp } from 'app/_libraries/_dof/core';
import React from 'react';

const idDomTest = 'KpiTemplateCardView';
const defaultProps: KpiTemplateCardViewProps = {
  id: 'test',
  value: {
    id: '20',
    name: 'Utilization Rate (Calculation of Total utilization)',
    subjectArea: 'Balances',
    submetric: 'Rate'
  },
  onClick: jest.fn()
};

describe('KpiTemplateCardView', () => {
  let wrapper: HTMLElement;

  const createWrapper = async (...props: KpiTemplateCardViewProps[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );

    const renderResult = await renderComponent(
      idDomTest,
      <DOFApp urlConfigs={[]}>
        <KpiTemplateCardView {...defaultProps} {...mergeProps} />
      </DOFApp>
    );
    wrapper = await renderResult.findByTestId(idDomTest);
  };

  it('should show KPi card data', async () => {
    await createWrapper();
    const workflowTemplate = wrapper.querySelector(
      `#workflow-template-card-view-${defaultProps.id}`
    );
    expect(workflowTemplate).toBeNull;
  });

  it('should show KPi card data has Status Group', async () => {
    const props = { ...defaultProps, isStatusGroup: true };
    props.value.name = 'KPi name';
    await createWrapper(props);
    expect(
      wrapper.querySelector(`#workflow-template-card-view-${defaultProps.id}`)
    ).toBeNull();
  });

  it('should show KPi card data has empty id', async () => {
    const props = { ...defaultProps, id: '' };
    await createWrapper(props);
    expect(
      wrapper.querySelector(`#workflow-template-card-view-empty`)
    ).toBeNull();
  });
});
