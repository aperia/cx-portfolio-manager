import { MIME_TYPE } from 'app/constants/mine-type';
import { classnames } from 'app/helpers';
import React, { forwardRef, useImperativeHandle, useMemo, useRef } from 'react';

export interface AttachmentItemProps {
  id?: string;

  fileName: string;
  mimeType: string;
  suffix?: React.ReactNode;

  className?: string;
}
const AttachmentItem: React.ForwardRefRenderFunction<
  HTMLDivElement,
  AttachmentItemProps
> = ({ id, fileName = '', mimeType, suffix, className }, ref) => {
  const attachmentRef = useRef<HTMLDivElement | null>(null);
  const extentions = useMemo(() => {
    const exts = fileName.split('.');

    if (exts.length < 2) {
      return MIME_TYPE[mimeType]?.extensions[0] || '';
    }

    return exts[exts.length - 1];
  }, [fileName, mimeType]);

  useImperativeHandle(ref, () => attachmentRef.current!);

  return (
    <div
      ref={attachmentRef}
      id={id}
      className={classnames('attachment-item', className)}
    >
      <div className="file-name">
        <div className={`dls-item-icon dls-na dls-${extentions}`} />
        <span className="px-8 fs-12 color-grey">{fileName}</span>
      </div>
      <div className="attachment-suffix">{suffix}</div>
    </div>
  );
};

export default forwardRef<HTMLDivElement, AttachmentItemProps>(AttachmentItem);
