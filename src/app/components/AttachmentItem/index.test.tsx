import { RenderResult } from '@testing-library/react';
import AttachmentItem, {
  AttachmentItemProps
} from 'app/components/AttachmentItem';
import { renderComponent } from 'app/utils';
import { Button, Icon } from 'app/_libraries/_dls';
import React from 'react';

const mockData: AttachmentItemProps = {
  fileName: 'attachment.xlsx',
  mimeType: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  suffix: (
    <div className="mr-n4">
      <Button variant="icon-secondary" className="d-inline">
        <Icon name="information" color={'grey-l16' as any} size="4x" />
      </Button>
      <Button variant="icon-secondary" className="d-inline ml-0">
        <Icon name="download" color={'grey-l16' as any} size="4x" />
      </Button>
    </div>
  )
};

const testId = 'attachmentItem';
describe('AttachmentItem', () => {
  let renderResult: RenderResult;

  const createWrapper = async (...props: AttachmentItemProps[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    renderResult = await renderComponent(
      testId,
      <AttachmentItem {...(mergeProps as any)} />
    );
  };

  const it_should_render_attachment = async (
    value: string,
    hasSuffix?: boolean
  ) => {
    const wrapper = await renderResult.findByTestId(testId);
    expect(wrapper.querySelector('span')!.textContent).toEqual(value);
    hasSuffix
      ? expect(
          wrapper.querySelector('.attachment-suffix')!.childNodes.length
        ).toBeGreaterThan(0)
      : expect(
          wrapper.querySelector('.attachment-suffix')!.childNodes.length
        ).toEqual(0);
  };

  it('should show attachment with filename and suffix', async () => {
    await createWrapper(mockData);

    await it_should_render_attachment(mockData.fileName, true);
  });

  it('should show attachment with filename but not suffix', async () => {
    await createWrapper({
      ...mockData,
      fileName: 'attachment.xlsx',
      suffix: undefined,
      ref: () => {}
    } as any);

    await it_should_render_attachment(mockData.fileName, false);
  });

  it('should show attachment with filename without extension', async () => {
    await createWrapper({
      fileName: 'fileName',
      suffix: undefined,
      mimeType: 'test'
    } as any);

    await it_should_render_attachment('fileName', false);
  });

  it('should show attachment without filename & extension', async () => {
    await createWrapper({
      suffix: undefined,
      mimeType: 'test'
    } as any);

    await it_should_render_attachment('', false);
  });
});
