import { isDevice } from 'app/helpers';
import {
  DropdownButton,
  DropdownButtonProps,
  DropdownButtonRef
} from 'app/_libraries/_dls';
import React, { useImperativeHandle, useRef } from 'react';

const EnhanceDropdownButton: React.ForwardRefRenderFunction<
  DropdownButtonRef,
  DropdownButtonProps
> = ({ buttonProps, ...props }, ref) => {
  const dropdownRef = useRef<DropdownButtonRef>(null);

  useImperativeHandle(ref, () => dropdownRef.current!);

  // handle mousedown on DropdownButton element
  const handleClicked = () => {
    const buttonElement = dropdownRef.current
      ?.buttonElement as HTMLButtonElement;

    if (isDevice) {
      process.nextTick(() => buttonElement?.focus());
    }
  };

  return (
    <DropdownButton
      ref={dropdownRef}
      buttonProps={{ ...buttonProps, onClick: handleClicked }}
      {...props}
    />
  );
};

export default React.forwardRef(EnhanceDropdownButton);
