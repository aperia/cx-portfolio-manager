import { fireEvent, render, RenderResult } from '@testing-library/react';
import * as helper from 'app/helpers/isDevice';
import 'app/utils/_mockComponent/mockCanvas';
import { DropdownButton, DropdownButtonProps } from 'app/_libraries/_dls';
import React from 'react';
import EnhanceDropdownButton from './index';

jest.mock('app/helpers/isDevice', () => ({
  __esModule: true,
  isDevice: false
}));
const mockHelper: any = helper;

const BUTTONS = [
  {
    label: 'Mock btn 1',
    value: 'btn1'
  },
  {
    label: 'Mock btn 2',
    value: 'btn2'
  }
];

describe('Test EnhanceDropdownButton Component', () => {
  const mockDrdButtonProp = {
    buttonProps: {
      children: 'Mock Button',
      variant: 'outline-primary'
    },
    popupBaseProps: {
      placement: 'bottom-end'
    }
  } as DropdownButtonProps;

  const renderDropdownButton = (props?: DropdownButtonProps): RenderResult => {
    return render(
      <div>
        <EnhanceDropdownButton ref={() => {}} {...mockDrdButtonProp} {...props}>
          {BUTTONS.slice(2).map(buttonItem => {
            return (
              <DropdownButton.Item
                key={buttonItem.value}
                label={buttonItem.label}
                value={buttonItem.value}
              />
            );
          })}
        </EnhanceDropdownButton>
      </div>
    );
  };

  const wait = (timeout = 0) =>
    new Promise(resolve => {
      const id = setTimeout(() => {
        clearTimeout(id);
        resolve(true);
      }, timeout);
    });

  it('should render Enhance DropdownButton on device', async () => {
    mockHelper.isDevice = true;
    const wrapper = renderDropdownButton();

    const { getByText } = wrapper;
    await wait();
    expect(getByText(/Mock Button/i)).toBeInTheDocument();

    fireEvent.click(getByText(/Mock Button/i));
    await wait();
    expect(getByText(/Mock Button/i).classList).toContain('active');
  });

  it('should render Enhance DropdownButton on desktop', async () => {
    mockHelper.isDevice = false;
    const wrapper = renderDropdownButton();

    const { getByText } = wrapper;
    await wait();
    expect(getByText(/Mock Button/i)).toBeInTheDocument();
    fireEvent.click(getByText(/Mock Button/i));
    await wait();
    expect(getByText(/Mock Button/i).classList).not.toContain('active');
  });
});
