import { LOAD_MORE_PAGE_SIZE } from 'app/constants/constants';
import { usePagination } from 'app/hooks/usePagination';
import {
  ComboBox,
  ComboBoxProps,
  ComboBoxRef,
  useTranslation
} from 'app/_libraries/_dls';
import React, { useEffect, useMemo, useRef } from 'react';

const EnhanceComboBox: React.FC<
  Omit<
    ComboBoxProps,
    'children' | 'opened' | 'onFilterChange' | 'onVisibilityChange'
  > & { data: any[]; loadingMoreText?: string }
> = ({
  data,
  value,
  textField,
  onBlur,
  onFocus,
  loadingMoreText,
  ...props
}) => {
  const comboboxRef = useRef<ComboBoxRef | null>(null);
  const { t } = useTranslation();

  const {
    currentPage,
    currentPageSize,
    onPageChange,
    onSearchChange,
    allDataFilter,
    total
  } = usePagination(data, [textField!], undefined, LOAD_MORE_PAGE_SIZE, 1);

  const comboboxItems = useMemo(() => {
    const nextData = allDataFilter
      .slice(0, currentPage * currentPageSize)
      .map((item, index) => (
        <ComboBox.Item
          key={item.code || index}
          label={item[textField!]}
          value={item}
          variant="bubble"
        />
      ));
    return nextData;
  }, [allDataFilter, textField, currentPage, currentPageSize]);

  const isShowLoading =
    total > LOAD_MORE_PAGE_SIZE && comboboxItems.length < total;

  useEffect(() => {
    if (isShowLoading) {
      const dropdownBaseRef = comboboxRef.current?.dropdownBaseRef?.current;
      const scrollContainerElement = dropdownBaseRef?.scrollContainerElement;
      const el = scrollContainerElement?.querySelector(
        '#combobox_loading-more-div'
      );
      if (el) {
        const observer = new IntersectionObserver(
          ([e]) => {
            e.isIntersecting &&
              setTimeout(() => {
                onPageChange(currentPage + 1);
              }, 500);
          },
          {
            threshold: [0.9]
          }
        );
        observer.observe(el);
        return () => observer.unobserve(el);
      }
    }
  }, [currentPage, onPageChange, total, isShowLoading]);

  const showLoadMore = useMemo(() => {
    return (
      <div
        id="combobox_loading-more-div"
        className={
          isShowLoading ? 'd-flex justify-content-center mt-14 mb-8' : 'd-none'
        }
      >
        <span className="loading loading-md" />
        <p className="ml-26">{loadingMoreText || t('txt_loading_more')}</p>
      </div>
    );
  }, [isShowLoading, loadingMoreText, t]);

  const noResultsFound = useMemo(() => {
    return (
      <span className={total > 0 ? 'd-none' : 'dls-no-result-found'}>
        {t('txt_no_results_found')}
      </span>
    );
  }, [total, t]);

  return (
    <React.Fragment>
      <ComboBox
        ref={comboboxRef}
        onFilterChange={e => onSearchChange(e.target.value)}
        value={value}
        textField={textField}
        onBlur={onBlur}
        onFocus={onFocus}
        className="combobox-load-more"
        {...props}
      >
        <ComboBox.Group label="">{comboboxItems}</ComboBox.Group>
        <ComboBox.Group label="">{showLoadMore}</ComboBox.Group>
        <ComboBox.Group label="">{noResultsFound}</ComboBox.Group>
      </ComboBox>
    </React.Fragment>
  );
};

export default EnhanceComboBox;
