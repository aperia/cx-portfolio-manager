import {
  fireEvent,
  render,
  RenderResult,
  screen
} from '@testing-library/react';
import { LOAD_MORE_PAGE_SIZE } from 'app/constants/constants';
import 'app/utils/_mockComponent/mockCanvas';
import 'app/utils/_mockComponent/mockComboBox';
import { ComboBoxProps } from 'app/_libraries/_dls';
import React from 'react';
import EnhanceComboBox from './index';

const CBB_DATA = [
  {
    code: '',
    text: 'All'
  },
  {
    code: 'NER',
    text: 'NIGER'
  },
  {
    code: 'GRL',
    text: 'GREENLAND'
  }
];

const onBlurMock = jest.fn();
const onFocusMock = jest.fn();
const onChangeMock = jest.fn();

describe('Test EnhanceComboBox Component', () => {
  const mockCbbProp = {
    id: 'id',
    label: 'label',
    data: CBB_DATA,
    value: CBB_DATA[0],
    textField: 'text',
    onBlur: onBlurMock,
    onFocus: onFocusMock,
    onChange: onChangeMock
  };

  const renderCombobox = (
    props?: Omit<
      ComboBoxProps,
      'children' | 'opened' | 'onFilterChange' | 'onVisibilityChange'
    > & { data: any[]; loadingMoreText?: string }
  ): RenderResult => {
    return render(
      <div>
        <EnhanceComboBox {...mockCbbProp} {...props} />
      </div>
    );
  };

  beforeEach(() => {});

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Render UI not show loading', () => {
    const wrapper = renderCombobox();
    expect(wrapper.getByText('label')).toBeInTheDocument();
    const inputElement = screen.getByTestId('ComboBox.Input_onFilter');
    fireEvent.change(inputElement, {
      target: {
        value: 'xxx'
      }
    });
    expect(wrapper.getByText('txt_no_results_found')).toBeInTheDocument();
  });

  it('Render UI with data length > LOAD_MORE_PAGE_SIZE, not have loading div in the DOM', () => {
    const observe = jest.fn();
    const unobserve = jest.fn();
    const disconnect = jest.fn();
    const mockIntersectionObserver = jest.fn();
    mockIntersectionObserver.mockImplementation((callback, options) => {
      options && callback([{ isIntersecting: true }]);
      return {
        observe,
        unobserve,
        disconnect
      };
    });

    window.IntersectionObserver = mockIntersectionObserver;

    const bigArray: any = [];
    for (let i = 0; i < LOAD_MORE_PAGE_SIZE + 1; i++) {
      bigArray.push({ code: i, text: i.toString() });
    }
    jest.useFakeTimers();
    const wrapper = renderCombobox({
      id: 'no-loading-div',
      data: bigArray,
      value: bigArray[0],
      loadingMoreText: 'txt_loading_more_change_owners'
    });
    jest.runAllTimers();

    expect(wrapper.getByText('label')).toBeInTheDocument();
    expect(mockIntersectionObserver).not.toBeCalled();
    expect(observe).not.toBeCalled();
  });

  it('Render UI with data length > LOAD_MORE_PAGE_SIZE, has loading div in the DOM', () => {
    const observe = jest.fn();
    const unobserve = jest.fn();
    const disconnect = jest.fn();
    const mockIntersectionObserver = jest.fn();
    mockIntersectionObserver.mockImplementation((callback, options) => {
      options && callback([{ isIntersecting: true }]);
      return {
        observe,
        unobserve,
        disconnect
      };
    });

    window.IntersectionObserver = mockIntersectionObserver;

    const bigArray: any = [];
    for (let i = 0; i < LOAD_MORE_PAGE_SIZE + 1; i++) {
      bigArray.push({ code: i, text: i.toString() });
    }
    jest.useFakeTimers();
    const wrapper = renderCombobox({
      id: 'has-loading-div',
      data: bigArray,
      value: bigArray[0],
      loadingMoreText: 'txt_loading_more_change_owners'
    });
    jest.runAllTimers();

    expect(wrapper.getByText('label')).toBeInTheDocument();
    expect(mockIntersectionObserver).toBeCalled();
    expect(observe).toBeCalled();
  });
});
