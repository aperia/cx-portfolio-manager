import { useElementSize } from 'app/hooks';
import {
  DropdownButton,
  DropdownButtonSelectEvent,
  Icon
} from 'app/_libraries/_dls';
import React, {
  useCallback,
  useEffect,
  useImperativeHandle,
  useMemo,
  useRef,
  useState
} from 'react';
import EnhanceDropdownButton from '../EnhanceDropdownButton';

export interface BreadCrumbItem {
  title: string | React.ReactNode | (() => React.ReactNode | string);
  id: string;
  icon?: string;
}

export interface BreadCrumbAction {
  (items: BreadCrumbItem | BreadCrumbItem[]): void;
}

export interface BreadCrumbProps {
  id?: string;
  items: BreadCrumbItem[];
  onSelect?: (id: string) => void;
  width?: number;
}

export interface BreadCrumbActions {
  addItems: BreadCrumbAction;
  setItems: BreadCrumbAction;
}

const convertToArray = (items: any) => {
  if (!Array.isArray(items)) return [items];
  return items;
};

const lastArrayItem = (items: any) => {
  if (Array.isArray(items)) return items[items.length - 1];
  return items;
};

const BreadCrumb: React.RefForwardingComponent<
  BreadCrumbActions,
  BreadCrumbProps
> = ({ id, items, onSelect, width }, ref) => {
  const breadcrumbRef = useRef<HTMLDivElement>(null);
  const size = useElementSize(breadcrumbRef);
  const [breadCrumbList, setBreadCrumbList] = useState<BreadCrumbItem[]>(items);
  useEffect(() => setBreadCrumbList(items), [items]);

  const handleSelectRef = useRef(onSelect);
  useEffect(() => {
    handleSelectRef.current = onSelect || (() => {});
  }, [onSelect]);

  const ipadScreen = useMemo(
    () => ({
      breakPoint: 768,
      respItemsShown: 2
    }),
    []
  );

  const mobileScreen = useMemo(
    () => ({
      breakPoint: 320,
      respItemsShown: 1
    }),
    []
  );

  const largeDeviceScreen = useMemo(
    () => ({
      breakPoint: 992,
      respItemsShown: 4
    }),
    []
  );

  useImperativeHandle(ref, () => ({
    addItems,
    setItems
  }));

  const addItems: BreadCrumbAction = crumbItems => {
    if (!crumbItems) return;
    setBreadCrumbList([...breadCrumbList, ...convertToArray(crumbItems)]);
    handleSelectRef.current!(lastArrayItem(crumbItems).id);
  };

  const setItems: BreadCrumbAction = crumbItems => {
    if (!crumbItems) return;
    setBreadCrumbList([...convertToArray(crumbItems)]);
    handleSelectRef.current!(lastArrayItem(crumbItems).id);
  };

  const handleOnClickBreadCrumbItem = useCallback(
    (index: number, newArrLn?: number) => {
      if (newArrLn) {
        const itemIdx = breadCrumbList.length - newArrLn;
        setBreadCrumbList(breadCrumbList.slice(0, itemIdx + index + 1));
        handleSelectRef.current!(
          breadCrumbList[itemIdx + index] && breadCrumbList[itemIdx + index].id
        );
        return;
      }

      setBreadCrumbList(breadCrumbList.slice(0, index + 1));
      handleSelectRef.current!(
        breadCrumbList[index] && breadCrumbList[index].id
      );
    },
    [breadCrumbList]
  );

  const handleMenuItemClick = useCallback(
    (e: DropdownButtonSelectEvent) => {
      const _valueItem = e.target.value;
      handleOnClickBreadCrumbItem(_valueItem);
    },
    [handleOnClickBreadCrumbItem]
  );

  const itemText = useCallback((item: BreadCrumbItem) => {
    if (!item.title) return;
    if (typeof item.title === 'function') return item.title();
    return item.title;
  }, []);

  const itemTextFullScreen = useMemo(() => {
    const handleClickItem = (event: any, index: number) => {
      handleOnClickBreadCrumbItem(index);
    };

    return breadCrumbList.map((item, index) => (
      <div className="breadcrumb-item" key={index}>
        <div
          className="breadcrumb-item-text"
          onClick={event => handleClickItem(event, index)}
        >
          {itemText(item)}
        </div>
      </div>
    ));
  }, [breadCrumbList, handleOnClickBreadCrumbItem, itemText]);

  const itemTextResponsive = useMemo(() => {
    let _newArr: any = [...breadCrumbList];
    let _menuItems: any = [...breadCrumbList];
    let _respItemsShown = largeDeviceScreen.respItemsShown;

    if (!width) return;

    if (width <= ipadScreen.breakPoint) {
      _respItemsShown = ipadScreen.respItemsShown;
    }

    if (width <= mobileScreen.breakPoint) {
      _respItemsShown = mobileScreen.respItemsShown;
    }

    let _hideItems = [] as BreadCrumbItem[];

    if (breadCrumbList.length > _respItemsShown) {
      _newArr = [
        ...breadCrumbList.slice(
          breadCrumbList.length - _respItemsShown,
          breadCrumbList.length
        )
      ];

      _hideItems = [
        ...breadCrumbList.slice(0, breadCrumbList.length - _respItemsShown)
      ];
    }

    _menuItems = _hideItems.map((i, idx) => {
      return (
        <DropdownButton.Item
          key={`${i.id}_${idx}`}
          label={
            (
              <div className="breadcrumb-item">
                <div className="breadcrumb-item-text">{itemText(i)}</div>
              </div>
            ) as any
          }
          value={idx}
        />
      );
    });

    return (
      <div className="breadcrumb-item-resp">
        {breadCrumbList.length > _respItemsShown && (
          <div className="d-inline-block mr-12">
            <EnhanceDropdownButton
              popupBaseProps={{ popupBaseClassName: 'dls-popup-breadcrumb' }}
              buttonProps={{
                children: <Icon name="more" />,
                variant: 'icon-secondary',
                size: 'sm'
              }}
              onSelect={handleMenuItemClick}
            >
              {_menuItems}
            </EnhanceDropdownButton>
          </div>
        )}

        {_newArr.map((item: BreadCrumbItem, index: number) => (
          <div className="breadcrumb-item" key={index}>
            <div
              className="breadcrumb-item-text"
              onClick={event => {
                if (index === _newArr.length - 1) return;

                handleOnClickBreadCrumbItem(index, _newArr.length);
              }}
            >
              {itemText(item)}
            </div>
          </div>
        ))}
      </div>
    );
  }, [
    breadCrumbList,
    handleOnClickBreadCrumbItem,
    itemText,
    handleMenuItemClick,
    ipadScreen,
    width,
    mobileScreen,
    largeDeviceScreen
  ]);

  return (
    <div ref={breadcrumbRef} id={id} className="breadcrumb">
      <div className="breadcrumb-list">
        {size && size.width && size.width <= largeDeviceScreen.breakPoint
          ? itemTextResponsive
          : itemTextFullScreen}
      </div>
    </div>
  );
};

export default React.forwardRef<BreadCrumbActions, BreadCrumbProps>(BreadCrumb);
