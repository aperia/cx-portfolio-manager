import { fireEvent, RenderResult } from '@testing-library/react';
import BreadCrumb, {
  BreadCrumbActions,
  BreadCrumbProps
} from 'app/components/BreadCrumb';
import * as ElementSize from 'app/hooks/useElementSize';
import { renderComponent } from 'app/utils';
import React from 'react';

const mockUseElementSize = jest.spyOn(ElementSize, 'useElementSize');

const onSelect = jest.fn();
const mockData: BreadCrumbProps = {
  items: [
    { id: 'id1', title: () => 'title 1' },
    { id: 'id2', title: 'title 2' },
    { id: 'id3', title: 'title 3' }
  ],
  onSelect
};

const testId = 'breadcrumb';
describe('BreadCrumb', () => {
  let renderResult: RenderResult;

  const createWrapper = async (
    ref: { current: BreadCrumbActions | null },
    ...props: BreadCrumbProps[]
  ) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    renderResult = await renderComponent(
      testId,
      <BreadCrumb
        {...(mergeProps as any)}
        ref={r => {
          ref.current = r;
        }}
      />
    );
  };

  const mockUseElementSizeImplement = (size: { width: number }) => {
    mockUseElementSize.mockImplementation(
      () => size as { width: number; height: number }
    );
  };

  afterEach(() => {
    mockUseElementSize.mockClear();
  });

  const it_should_render_breadcrumb = async (value: number) => {
    const wrapper = await renderResult.findByTestId(testId);
    expect(wrapper.querySelectorAll('.breadcrumb-item').length).toEqual(value);
  };
  const it_should_render_dropdown_button = async (value: number) => {
    const wrapper = await renderResult.findByTestId(testId);
    expect(wrapper.querySelectorAll('.dls-dropdown-button').length).toEqual(
      value
    );
  };
  const it_should_call_onSelect = (times = 0) => {
    expect(onSelect).toHaveBeenCalledTimes(times);
  };

  const then_click_first_item = async () => {
    const wrapper = await renderResult.findByTestId(testId);

    const firstItemIlement = wrapper.querySelector('.breadcrumb-item-text');
    fireEvent.click(firstItemIlement!);
  };

  const then_click_first_item_in_dropdown_button = async () => {
    const wrapper = await renderResult.findByTestId(testId);

    const dropdown = wrapper.querySelector('.dls-dropdown-button');
    fireEvent.focus(dropdown!);

    const firstItemIlement = document.body
      .querySelector('.dls-popup-breadcrumb')!
      .querySelector('.item');
    fireEvent.click(firstItemIlement!);
  };

  it('should empty breadcrumb', async () => {
    mockUseElementSizeImplement({ width: 1400 });
    const ref: { current: BreadCrumbActions | null } = { current: null };
    await createWrapper(ref, { items: [] });

    await it_should_render_breadcrumb(0);
  });

  it('should render normal breadcrumb with all items', async () => {
    mockUseElementSizeImplement({ width: 1400 });
    const ref: { current: BreadCrumbActions | null } = { current: null };
    await createWrapper(ref, mockData);

    await it_should_render_breadcrumb(3);

    await then_click_first_item();
    it_should_call_onSelect(1);
  });

  it('should render breadcrumb with dropdown button when actual device width < largeDeviceScreen=992', async () => {
    mockUseElementSizeImplement({ width: 700 });
    const ref: { current: BreadCrumbActions | null } = { current: null };
    await createWrapper(ref, {
      width: 992,
      items: []
    });
    ref.current?.setItems(undefined as any);
    ref.current?.setItems(mockData.items);
    ref.current?.addItems(undefined as any);
    ref.current?.addItems({ id: 'id4' } as any);
    ref.current?.addItems([
      { id: 'id5', title: 'title 5' },
      { id: 'id6', title: 'title 6' }
    ]);

    await it_should_render_dropdown_button(1);
    await it_should_render_breadcrumb(4);

    it_should_call_onSelect(0); // because there are no onSelect prop
  });

  it('should render breadcrumb with dropdown button when actual device width < largeDeviceScreen=768', async () => {
    mockUseElementSizeImplement({ width: 700 });
    const ref: { current: BreadCrumbActions | null } = { current: null };
    await createWrapper(ref, {
      onSelect,
      width: 768,
      items: [...mockData.items, { id: 'id4', title: 'title 4' }]
    });

    await it_should_render_dropdown_button(1);
    await it_should_render_breadcrumb(2);

    await then_click_first_item();
    it_should_call_onSelect(1);

    await then_click_first_item_in_dropdown_button();
    it_should_call_onSelect(2);
  });

  it('should render breadcrumb with dropdown button when actual device width < mobileScreen=320', async () => {
    mockUseElementSizeImplement({ width: 500 });
    const ref: { current: BreadCrumbActions | null } = { current: null };
    await createWrapper(ref, {
      onSelect,
      width: 320,
      items: mockData.items
    });

    await it_should_render_dropdown_button(1);
    await it_should_render_breadcrumb(1);

    await then_click_first_item();
    it_should_call_onSelect(0); // because it's just one item not clickable
  });
});
