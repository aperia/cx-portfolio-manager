import { fireEvent, render, RenderResult } from '@testing-library/react';
import * as helper from 'app/helpers/isDevice';
import React from 'react';
import FieldTooltip, { FieldTooltipProps } from './index';

const mockHelper: any = helper;

jest.mock('app/helpers/isDevice', () => ({
  __esModule: true,
  isDevice: false
}));

describe('Test FieldTooltip Component', () => {
  const mockFieldTooltipProps = {
    placement: 'right-start',
    element: 'Mock tooltip data'
  } as FieldTooltipProps;

  const renderFieldTooltip = (
    children: React.ReactElement,
    props?: FieldTooltipProps
  ): RenderResult => {
    return render(
      <div>
        <FieldTooltip ref={() => {}} {...mockFieldTooltipProps} {...props}>
          {children}
        </FieldTooltip>
      </div>
    );
  };

  it('should render FieldTooltip UI', async () => {
    const focusFn = jest.fn();
    const blurFn = jest.fn();

    const wrapper = renderFieldTooltip(
      <div onFocus={focusFn} onBlur={blurFn}>
        Mock field content
      </div>
    );

    const { queryByText } = wrapper;

    expect(queryByText(/Mock field content/i)).toBeInTheDocument();

    // focus on field element
    fireEvent.focus(queryByText(/Mock field content/i)!);
    expect(focusFn).toHaveBeenCalled();
    expect(queryByText(/Mock tooltip data/i)).toBeInTheDocument();

    // blur on field element
    fireEvent.blur(queryByText(/Mock field content/i)!);
    expect(blurFn).toHaveBeenCalled();
    expect(queryByText(/Mock tooltip data/i)).toBeNull();
  });

  it('should render FieldTooltip UI on device', async () => {
    mockHelper.isDevice = true;
    const focusFn = jest.fn();

    const wrapper = renderFieldTooltip(
      <div onFocus={focusFn}>Mock field content</div>,
      {
        ...mockFieldTooltipProps,
        keepShowOnDevice: false
      }
    );

    const { queryByText } = wrapper;

    expect(queryByText(/Mock field content/i)).toBeInTheDocument();

    // focus on field element > not show tooltip
    fireEvent.focus(queryByText(/Mock field content/i)!);
    expect(focusFn).toHaveBeenCalled();
    expect(queryByText(/Mock tooltip data/i)).toBeNull();
  });
});
