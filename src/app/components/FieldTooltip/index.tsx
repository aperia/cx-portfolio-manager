import { classnames, isDevice } from 'app/helpers';
import { Popover, PopoverProps, PopperRef } from 'app/_libraries/_dls';
import { isFunction } from 'lodash';
import React, { useImperativeHandle, useMemo, useRef, useState } from 'react';

export interface ChildrenAccessProps {
  onFocus?: (e: React.FocusEvent<Element>) => void;
  onBlur?: (e: React.FocusEvent<Element>) => void;
}
export interface FieldTooltipProps extends Omit<PopoverProps, 'opened'> {
  keepShowOnDevice?: boolean;
  children: React.ReactElement<ChildrenAccessProps, any>;
}
const FieldTooltip: React.ForwardRefRenderFunction<
  PopperRef,
  FieldTooltipProps
> = (
  {
    children: childrenProp,
    keepShowOnDevice = true,
    triggerClassName: triggerClassNameProp,
    ...rest
  },
  ref
) => {
  const [opened, setOpened] = useState(false);
  const tooltipRef = useRef<PopperRef | null>(null);
  useImperativeHandle(ref, () => tooltipRef.current!);

  const triggerClassName = useMemo(
    () => classnames('w-100', triggerClassNameProp),
    [triggerClassNameProp]
  );

  const children = useMemo(() => {
    const { onFocus, onBlur } = childrenProp.props as ChildrenAccessProps;

    const customOnFocus = (e: React.FocusEvent<Element>) => {
      setOpened(true);

      isFunction(onFocus) && onFocus(e);
    };

    const customOnBlur = (e: React.FocusEvent<Element>) => {
      setOpened(false);

      isFunction(onBlur) && onBlur(e);
    };

    return React.cloneElement(childrenProp, {
      onFocus: customOnFocus,
      onBlur: customOnBlur
    });
  }, [childrenProp]);

  return (
    <Popover
      containerClassName="field-tooltip"
      {...rest}
      size="md"
      triggerClassName={triggerClassName}
      ref={tooltipRef}
      opened={isDevice && !keepShowOnDevice ? false : opened}
    >
      {children}
    </Popover>
  );
};

export default React.forwardRef(FieldTooltip);
