import {
  ComboBox,
  ComboBoxProps,
  DropdownBaseChangeEvent
} from 'app/_libraries/_dls';
import { CommonControlProps } from 'app/_libraries/_dof/core';
import React, { useEffect, useState } from 'react';

export interface ComboBoxControlProps
  extends CommonControlProps,
    Omit<ComboBoxProps, 'id' | 'label' | 'children'> {}

export const ComboBoxControl: React.FC<ComboBoxControlProps> = ({
  id,
  label,
  enable,
  data = [],
  input: { value: valueProp, onChange, onBlur },
  meta: { touched, error, invalid },
  textField = 'fieldValue',
  readOnly,
  required,
  ...props
}) => {
  const [value, setValue] = useState<MagicKeyValue>(valueProp);

  const handleOnChange = (event: DropdownBaseChangeEvent) => {
    const { value: cbValue } = event.target;
    setValue(cbValue);
  };

  const handleOnBlur = (event: React.FocusEvent<Element>) => {
    onChange(value);
    onBlur(value);
  };

  useEffect(() => {
    setValue(valueProp);
  }, [valueProp]);

  return (
    <ComboBox
      id={id}
      label={label}
      value={value}
      onChange={handleOnChange}
      onBlur={handleOnBlur}
      textField={textField}
      disabled={!enable}
      readOnly={readOnly}
      required={required}
      noResult={'No result founds'}
      popupBaseProps={{
        popupBaseClassName: 'inside-infobar'
      }}
      error={
        enable && touched && invalid && required
          ? {
              message: error,
              status: invalid
            }
          : undefined
      }
      {...props}
    >
      {data.map((item, index) => {
        return (
          <ComboBox.Item
            value={item}
            label={item[textField]}
            key={index}
            {...props}
          />
        );
      })}
    </ComboBox>
  );
};

export default ComboBoxControl;
