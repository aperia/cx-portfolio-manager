import { fireEvent, RenderResult } from '@testing-library/react';
import TextAreaControl, {
  TextAreaControlProps
} from 'app/components/DofControl/TextAreaControl';
import { getDefaultDOFMockProps, renderComponent } from 'app/utils';
import React from 'react';

const idDomTest = 'test';
const defaultProps = getDefaultDOFMockProps();

describe('unit test TextAreaControl', () => {
  let renderResult: RenderResult;
  const createWrapper = async (...props: TextAreaControlProps[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    renderResult = await renderComponent(
      idDomTest,
      <TextAreaControl {...defaultProps} {...mergeProps} />
    );
  };

  it('should have valueProp', async () => {
    const onBlurFunc = jest.fn();
    const dataTest = getDefaultDOFMockProps({
      value: 'john',
      onBlur: onBlurFunc,
      options: { maxLength: 6, countDown: true }
    });

    await createWrapper(dataTest);
    const { getByDisplayValue } = renderResult;
    expect(getByDisplayValue(/john/i)).toBeInTheDocument();
    fireEvent.blur(getByDisplayValue(/john/i));
    expect(onBlurFunc).toBeCalled();
  });

  it('value is null', async () => {
    const dataTest = getDefaultDOFMockProps({
      value: undefined
    });

    await createWrapper(dataTest);
    const { queryByDisplayValue } = renderResult;
    expect(queryByDisplayValue(/john/i)).not.toBeInTheDocument();
  });

  it('should contain 1 character left', async () => {
    const onBlurFunc = jest.fn();
    const dataTest = getDefaultDOFMockProps({
      value: 'john',
      onBlur: onBlurFunc,
      options: { maxLength: 5, countDown: true }
    });

    await createWrapper(dataTest);
    const { getByDisplayValue } = renderResult;
    expect(getByDisplayValue(/john/i)).toBeInTheDocument();
  });

  it('should have onChange', async () => {
    const onChangeFunc = jest.fn();
    const dataTest = getDefaultDOFMockProps({
      value: 'john',
      options: { maxLength: 200, countDown: true },
      onChange: onChangeFunc
    });
    await createWrapper(dataTest);
    const { getByDisplayValue } = renderResult;
    expect(getByDisplayValue(/john/i)).toBeInTheDocument();
    fireEvent.change(getByDisplayValue(/john/i), {
      target: { value: 'smith' }
    });
    expect(getByDisplayValue(/smith/i)).toBeInTheDocument();
  });
});
