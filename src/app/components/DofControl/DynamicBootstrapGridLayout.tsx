import { useRegisteredComponentsContext } from 'app/hooks';
import { ViewLayoutElement, ViewLayoutProps } from 'app/_libraries/_dof/core';
import React, { useMemo } from 'react';
import { useSelector } from 'react-redux';

interface ViewElementProps extends Partial<React.HTMLProps<HTMLDivElement>> {
  columnWidth?: number;
}
export interface DynamicBootstrapGridLayoutProps
  extends ViewLayoutProps<ViewElementProps> {
  className?: string;
}

const DEFAULT_VISIBLE_FIELDS = ['divider', 'empty'];

const DynamicBootstrapGridLayout: React.FunctionComponent<DynamicBootstrapGridLayoutProps> = ({
  viewElements,
  className = '',
  viewName
}) => {
  const registeredComponentsContext = useRegisteredComponentsContext();
  const registeredFields = useMemo(
    () => registeredComponentsContext.registeredFields,
    [registeredComponentsContext]
  );
  const formKey = useMemo(
    () =>
      viewElements && (viewElements[0]?.renderedNode as any)?.props?.formKey,
    [viewElements]
  );
  const formData = useSelector<RootState, any>(
    state => state.form[formKey]?.values
  );
  const formFields = useMemo(() => Object.keys(formData || {}), [formData]);
  const visibleFields = useMemo(
    () => [...DEFAULT_VISIBLE_FIELDS, ...formFields],
    [formFields]
  );

  if (formFields.length <= 0) return <React.Fragment />;

  return (
    <div
      className={`row ${className}`}
      {...{
        role: 'dof-view',
        'role-name': viewName
      }}
    >
      {viewElements
        .filter((f: ViewLayoutElement<ViewElementProps>) =>
          visibleFields.includes(registeredFields[f.name]?.dataField)
        )
        .map((viewElement, index) => {
          if (!viewElement.layoutProps) {
            return (
              <div
                key={index}
                {...{
                  role: 'dof-field'
                }}
              >
                {viewElement.renderedNode}
              </div>
            );
          }

          const {
            className: layoutClassName,
            columnWidth,
            ...others
          } = viewElement.layoutProps;
          const classNames: string[] = [];
          if (columnWidth) {
            classNames.push(`col-${columnWidth}`);
          }
          if (layoutClassName) {
            classNames.push(layoutClassName);
          }
          return (
            <div
              key={index}
              className={classNames.join(' ')}
              {...others}
              {...{
                role: 'dof-field'
              }}
            >
              {viewElement.renderedNode}
            </div>
          );
        })}
    </div>
  );
};

export default DynamicBootstrapGridLayout;
