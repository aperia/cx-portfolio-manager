import { fireEvent, render, RenderResult } from '@testing-library/react';
import React from 'react';
import * as ReactRedux from 'react-redux';
import { WrappedFieldInputProps } from 'redux-form';
import GroupMethodCardTextButtonControl, {
  GroupMethodCardTextButtonControlProps
} from './GroupMethodCardTextButtonControl';

const mockDispatch = jest.spyOn(ReactRedux, 'useDispatch');

const MethodName = 'Mock name';
const SubjectSection = 'Mock subject section';

describe('Test GroupMethodCardTextButtonControl DofControl', () => {
  const dispatchFn = jest.fn();

  const mockDrdButtonProp = {
    id: 'mock-id',
    options: {},
    input: {
      value: {
        name: MethodName,
        relatedPricingStrategies: '99',
        isCreateNewVersion: true
      }
    },
    label: 'Mock label'
  } as GroupMethodCardTextButtonControlProps;

  const renderGroupMethodCardTextButtonControl = (
    props?: GroupMethodCardTextButtonControlProps
  ): RenderResult => {
    return render(
      <div>
        <GroupMethodCardTextButtonControl {...mockDrdButtonProp} {...props} />
      </div>
    );
  };

  beforeEach(() => {
    mockDispatch.mockReturnValue(dispatchFn);
  });

  afterEach(() => {
    mockDispatch.mockClear();
  });

  it('should render GroupMethodCardTextButtonControl UI with Service SubjectSection', async () => {
    const wrapper = renderGroupMethodCardTextButtonControl({
      ...mockDrdButtonProp,
      options: {
        serviceSubjectSection: SubjectSection,
        actionName: 'openStrategyFlyout'
      } as Record<string, string>
    });

    const { queryByText } = wrapper;
    expect(queryByText(/Impacted Pricing Strategies:/i)).toBeInTheDocument();
    fireEvent.mouseEnter(
      wrapper.baseElement.querySelector('.dls-popper-trigger')!
    );

    expect(
      queryByText(
        `Pricing strategies that have ${MethodName} assigned as ${SubjectSection}.`
      )
    ).toBeInTheDocument();

    fireEvent.click(queryByText('txt_view')!);

    expect(dispatchFn).toHaveBeenCalled();
  });

  it('should render GroupMethodCardTextButtonControl UI with isCreateNewVersion = false', async () => {
    const wrapper = renderGroupMethodCardTextButtonControl({
      ...mockDrdButtonProp,
      options: undefined,
      input: {
        value: {
          name: MethodName,
          relatedPricingStrategies: '99',
          isCreateNewVersion: false
        }
      } as WrappedFieldInputProps
    });

    const { queryByText } = wrapper;
    expect(queryByText(/Related Pricing Strategies:/i)).toBeInTheDocument();
  });
});
