import { fireEvent, RenderResult } from '@testing-library/react';
import BadgeGroupTextControl, {
  BadgeGroupTextControlProps
} from 'app/components/DofControl/BadgeGroupTextControl';
import { BadgeColorType } from 'app/constants/enums';
import { getDefaultDOFMockProps, renderComponent } from 'app/utils';
import React from 'react';

const idDomTest = 'badge-group-text-inline-control-id';
const mockProps = getDefaultDOFMockProps({ id: idDomTest });

describe('BadgeGroupTextControl', () => {
  let wrapper: RenderResult;

  const createWrapper = async (...props: BadgeGroupTextControlProps[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    wrapper = await renderComponent(
      idDomTest,
      <BadgeGroupTextControl {...mockProps} {...mergeProps} />
    );
  };

  const mockValue = (
    value: any,
    options?: {
      colorType?: BadgeColorType;
      noBorder?: boolean;
      inline?: boolean;
      iconButton?: string;
    }
  ) => {
    return {
      input: { ...mockProps.input, value },
      options
    } as BadgeGroupTextControlProps;
  };
  const testWithLabelProps = {
    label: 'Mock Label'
  } as BadgeGroupTextControlProps;

  describe('badge with label', () => {
    it('should show N/A value when value = ""', async () => {
      await createWrapper(testWithLabelProps);
      const { queryByText } = wrapper;

      expect(queryByText('Mock Label')).toBeInTheDocument();
      expect(queryByText('N/A')).toBeInTheDocument();
    });

    it('should show value', async () => {
      await createWrapper(testWithLabelProps, mockValue('Mock Value'));
      const { queryByText } = wrapper;

      expect(queryByText('Mock Label')).toBeInTheDocument();
      expect(queryByText('Mock Value')).toBeInTheDocument();
    });

    it('should show color green when Change Status is Production', async () => {
      await createWrapper(
        testWithLabelProps,
        mockValue('Production', {
          colorType: BadgeColorType.CHANGE_STATUS
        })
      );
      const { queryByText } = wrapper;

      expect(queryByText('Production')).toBeInTheDocument();
      expect(queryByText('Production')?.parentElement!.className).toContain(
        'badge-green'
      );
    });

    it('should show color red when Change Status is Rejected', async () => {
      await createWrapper(
        testWithLabelProps,
        mockValue('Rejected', {
          colorType: BadgeColorType.CHANGE_STATUS,
          inline: false,
          iconButton: 'd-flex'
        })
      );
      const { queryByText } = wrapper;

      expect(queryByText('Rejected')).toBeInTheDocument();
      expect(queryByText('Rejected')?.parentElement!.className).toContain(
        'badge-red'
      );
    });

    it('should show color green when Workflow Category is Penalty Pricing', async () => {
      await createWrapper(
        testWithLabelProps,
        mockValue('Penalty Pricing', {
          colorType: BadgeColorType.WorkflowCategory,
          inline: false
        })
      );
      const { queryByText } = wrapper;

      expect(queryByText('Penalty Pricing')).toBeInTheDocument();
      expect(
        queryByText('Penalty Pricing')?.parentElement!.className
      ).toContain('badge-green');
    });

    it('should show color cyan when Workflow Type is File Management', async () => {
      await createWrapper(
        testWithLabelProps,
        mockValue('File Management', {
          colorType: BadgeColorType.WorkflowType
        })
      );
      const { queryByText } = wrapper;

      expect(queryByText('File Management')).toBeInTheDocument();
      expect(
        queryByText('File Management')?.parentElement!.className
      ).toContain('badge-cyan');
    });

    it('should show color cyan when Workflow Status is Incomplete', async () => {
      await createWrapper(
        testWithLabelProps,
        mockValue('Incomplete', {
          colorType: BadgeColorType.WorkflowStatus
        })
      );
      const { queryByText } = wrapper;

      expect(queryByText('Incomplete')).toBeInTheDocument();
      expect(queryByText('Incomplete')?.parentElement!.className).toContain(
        'badge-cyan'
      );
    });

    it('should show color cyan when Workflow Status is StrategyStatus', async () => {
      await createWrapper(
        testWithLabelProps,
        mockValue('Incomplete', {
          colorType: BadgeColorType.StrategyStatus
        })
      );
      const { queryByText } = wrapper;

      expect(queryByText('Incomplete')).toBeInTheDocument();
      expect(queryByText('Incomplete')?.parentElement!.className).toContain(
        'badge'
      );
    });

    it('should show color cyan when Workflow Status is ActionTaken', async () => {
      await createWrapper(
        testWithLabelProps,
        mockValue('Incomplete', {
          colorType: BadgeColorType.ActionTaken
        })
      );
      const { queryByText } = wrapper;

      expect(queryByText('Incomplete')).toBeInTheDocument();
      expect(queryByText('Incomplete')?.parentElement!.className).toContain(
        'badge'
      );
    });

    it('should show color cyan when Workflow Status is ConfigParameterStatus', async () => {
      await createWrapper(
        testWithLabelProps,
        mockValue('Incomplete', {
          colorType: BadgeColorType.ConfigParameterStatus
        })
      );
      const { queryByText } = wrapper;

      expect(queryByText('Incomplete')).toBeInTheDocument();
      expect(queryByText('Incomplete')?.parentElement!.className).toContain(
        'badge badge-grey'
      );
    });

    it('should show color orange when Workflow Status is Not Started', async () => {
      await createWrapper(
        testWithLabelProps,
        mockValue('Not Started', {
          colorType: BadgeColorType.ChangeInTermsStatus
        })
      );
      const { queryByText } = wrapper;

      expect(queryByText('Not Started')).toBeInTheDocument();
      expect(queryByText('Not Started')?.parentElement!.className).toContain(
        'badge-orange'
      );
    });

    it('should show color purple when Version Creating is Created', async () => {
      await createWrapper(
        testWithLabelProps,
        mockValue('Created', {
          colorType: BadgeColorType.VersionCreating
        })
      );
      const { queryByText } = wrapper;

      expect(queryByText('Created')).toBeInTheDocument();
      expect(queryByText('Created')?.parentElement!.className).toContain(
        'badge-purple'
      );
    });

    it('should show color grey when Version Creating is Created', async () => {
      await createWrapper(
        testWithLabelProps,
        mockValue('Created', {
          colorType: BadgeColorType.MethodType
        })
      );
      const { queryByText } = wrapper;

      expect(queryByText('Created')).toBeInTheDocument();
      expect(queryByText('Created')?.parentElement!.className).toContain(
        'badge-grey'
      );
    });

    it('should show color grey when Color Type is TableType', async () => {
      await createWrapper(
        testWithLabelProps,
        mockValue('Created', {
          colorType: BadgeColorType.TableType
        })
      );
      const { queryByText } = wrapper;

      expect(queryByText('Created')).toBeInTheDocument();
      expect(queryByText('Created')?.parentElement!.className).toContain(
        'badge-grey'
      );
    });

    it('should show color grey when Color Type is VersionCreated', async () => {
      await createWrapper(
        testWithLabelProps,
        mockValue('VersionCreated', {
          colorType: BadgeColorType.VersionCreated
        })
      );
      const { queryByText } = wrapper;

      expect(queryByText('VersionCreated')).toBeInTheDocument();
      expect(queryByText('VersionCreated')?.parentElement!.className).toContain(
        'badge-cyan'
      );
    });

    it('should add class "rounded" when noBorder is true', async () => {
      const withLabelProps = {
        label: 'Mock Label',
        input: null,
        options: { noBorder: true }
      } as any;

      await createWrapper(withLabelProps);
      const { queryByText } = wrapper;

      expect(queryByText('N/A')).toBeInTheDocument();
      expect(queryByText('N/A')?.parentElement!.className).toContain('rounded');
    });

    it('should show color grey when Version Creating is Created > value isObject', async () => {
      await createWrapper(
        testWithLabelProps,
        mockValue(
          { text: 'Created', callbackAction: jest.fn(), tooltip: '12' },
          {
            colorType: BadgeColorType.MethodType,
            inline: false,
            iconButton: 'd-flex'
          }
        )
      );
      const { queryByText } = wrapper;

      const btn = document.querySelector('.btn');
      btn && fireEvent.click(btn);

      expect(queryByText('Created')).toBeInTheDocument();
      expect(queryByText('Created')?.parentElement!.className).toContain(
        'badge-grey'
      );
    });
  });

  it('should show color grey when Version Creating is Created > value isObject no tooltip', async () => {
    await createWrapper(
      testWithLabelProps,
      mockValue(
        { text: 'Created', callbackAction: jest.fn() },
        {
          colorType: BadgeColorType.MethodType,
          inline: false,
          iconButton: 'd-flex'
        }
      )
    );
    const { queryByText } = wrapper;

    const btn = document.querySelector('.btn');
    btn && fireEvent.click(btn);

    expect(queryByText('Created')).toBeInTheDocument();
    expect(queryByText('Created')?.parentElement!.className).toContain(
      'badge-grey'
    );
  });
});
