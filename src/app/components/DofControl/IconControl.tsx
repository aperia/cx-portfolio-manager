import { ButtonActionType } from 'app/constants/button-action-types';
import {
  Icon,
  IconNames,
  IconProps,
  Tooltip,
  TooltipProps
} from 'app/_libraries/_dls';
import { CommonControlProps } from 'app/_libraries/_dof/core';
import React from 'react';
import { useDispatch } from 'react-redux';
import { getButtonControlAction } from './actions';

export interface IconControlProps extends CommonControlProps {
  id: any;
  className?: string;
  label?: string;
  options?: {
    actionName?: keyof typeof ButtonActionType;
    iconProps?: IconProps;
    tooltipProps?: TooltipProps;
  };
}

const IconControl: React.FC<IconControlProps> = ({
  id,
  input,
  className,
  options = {}
}) => {
  const dispatch = useDispatch();
  const { value } = input || {};

  if (!value) return <React.Fragment />;

  const { actionName, iconProps, tooltipProps } = options;

  const action = getButtonControlAction(actionName);

  const handleClick = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    action && action(value, dispatch, e);
  };

  return (
    <div className={className} id={id}>
      {tooltipProps ? (
        <Tooltip {...tooltipProps}>
          <div>
            <Icon
              name={iconProps?.name as IconNames}
              {...iconProps}
              onClick={handleClick}
            />
          </div>
        </Tooltip>
      ) : (
        <Icon
          name={iconProps?.name as IconNames}
          {...iconProps}
          onClick={handleClick}
        />
      )}
    </div>
  );
};

export default IconControl;
