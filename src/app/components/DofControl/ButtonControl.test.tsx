import { fireEvent, RenderResult } from '@testing-library/react';
import * as getButtonControlAction from 'app/components/DofControl/actions';
import ButtonControl, {
  ButtonControlProps
} from 'app/components/DofControl/ButtonControl';
import { ButtonActionType } from 'app/constants/button-action-types';
import { getDefaultDOFMockProps, renderComponent } from 'app/utils';
import React from 'react';
import * as reactRedux from 'react-redux';

const idDomTest = 'button-control-id';
const mockProps = getDefaultDOFMockProps({ id: idDomTest });
const useDispatchMock = jest.spyOn(reactRedux, 'useDispatch');

const getButtonControlActionMock = jest.spyOn(
  getButtonControlAction,
  'getButtonControlAction'
);

describe('ButtonControl', () => {
  let wrapper: RenderResult;
  const action = jest.fn();

  beforeEach(() => {
    getButtonControlActionMock.mockReturnValue(action);
  });

  afterEach(() => {
    useDispatchMock.mockClear();
  });

  const createWrapper = async (...props: ButtonControlProps[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    wrapper = await renderComponent(
      idDomTest,
      <ButtonControl {...mockProps} {...mergeProps} />
    );
  };

  const mockValue = (
    label: string,
    value: any,
    options?: {
      actionName?: string;
      tooltipText?: string;
    }
  ) => {
    return {
      input: { ...mockProps.input, value },
      label,
      options
    } as ButtonControlProps;
  };

  describe('Button Control', () => {
    it('should show nothing', async () => {
      await createWrapper({ input: null } as any);

      const { getByTestId } = wrapper;

      expect(getByTestId(idDomTest)).toBeEmptyDOMElement();
    });

    it('should show empty button', async () => {
      await createWrapper(mockValue('', 'Value', {}));

      const { getByTestId } = wrapper;

      expect(
        getByTestId(idDomTest).querySelector('button')!
      ).toBeEmptyDOMElement();
    });

    it('should show button with value & tooltip', async () => {
      await createWrapper(
        mockValue('Mock Button', 'Value', {
          tooltipText: 'Mock tooltip'
        })
      );

      const { getByText } = wrapper;

      expect(getByText('Mock Button')).toBeInTheDocument();
    });

    it('should show disabled button', async () => {
      await createWrapper(mockValue('Mock Button', 'disabled'));

      const { getByText } = wrapper;

      expect(getByText('Mock Button').className).toContain(
        'cursor-not-allowed'
      );
    });

    it('click on button', async () => {
      await createWrapper(
        mockValue('Mock Button', 'Value', {
          actionName: ButtonActionType.openWorkflow
        })
      );

      const { getByText } = wrapper;

      expect(getByText('Mock Button')).toBeInTheDocument();
      fireEvent.click(getByText('Mock Button'));
      expect(action).toBeCalled();
    });
  });
});
