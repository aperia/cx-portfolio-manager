import { CommonControlProps } from 'app/_libraries/_dof/core';
import React from 'react';

export interface DivControlProps extends CommonControlProps {}

const DivControl: React.FC<DivControlProps> = ({ ...props }) => {
  return <div id={props.id}>{props.label}</div>;
};

export default DivControl;
