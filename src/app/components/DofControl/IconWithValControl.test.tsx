import { RenderResult } from '@testing-library/react';
import IconWithValControl, {
  IconWithValControlProps
} from 'app/components/DofControl/IconWithValControl';
import { getDefaultDOFMockProps, renderComponent } from 'app/utils';
import React from 'react';

const idDomTest = 'icon-control-id';
const mockProps = getDefaultDOFMockProps({ id: idDomTest });

describe('IconWithValControl ', () => {
  let renderResult: RenderResult;

  const createWrapper = async (...props: IconWithValControlProps[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    renderResult = await renderComponent(
      idDomTest,
      <IconWithValControl {...mockProps} {...mergeProps} />
    );
  };

  const mockValue = (value: any) => {
    return {
      input: { ...mockProps.input, value }
    } as IconWithValControlProps;
  };

  it('should render without error', async () => {
    await createWrapper(mockValue('value'));
    const { getByText } = renderResult;
    expect(getByText(/value/i)).toBeInTheDocument();
  });

  it('should show icon when has iconProps', async () => {
    await createWrapper(mockValue('value'), {
      options: { iconProps: { name: 'file' } }
    } as any);
    const { getByText } = renderResult;
    expect(getByText(/value/i)).toBeInTheDocument();
  });
});
