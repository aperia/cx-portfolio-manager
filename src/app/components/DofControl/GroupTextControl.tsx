// Types
// Constant
import {
  GroupTextFormat as FormatEnum,
  GroupTextFormatType as FormatType
} from 'app/constants/enums';
// Helpers
import {
  camelToText,
  classnames,
  formatCommon,
  formatContactPhone,
  formatTaxpayerId,
  isDevice
} from 'app/helpers';
// Components
import { Tooltip, TruncateText } from 'app/_libraries/_dls';
import { CommonControlProps } from 'app/_libraries/_dof/core';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { Link } from 'react-router-dom';

export interface GroupTextControlProps extends CommonControlProps {
  options?: {
    format?: FormatEnum;
    fractionDigits?: number;
    formatType?: FormatType;
    zeroAsEmpty?: boolean;
    inline?: boolean;
    inlineFullText?: boolean;
    truncate?: boolean;
    lineTruncate?: number;
    ellipsisMoreText?: string;
    ellipsisLessText?: string;
    boldTextValue?: boolean;
    isCamel?: boolean;
    valueClass?: string;

    link?: string;
    linkDataField?: string;
    linkArgs?: string[];
    resizable?: boolean;
    handleTruncateProp?: (isTruncated: boolean, showAll: boolean) => void;
  };
}

const GroupTextControl: React.FC<GroupTextControlProps> = ({
  id,
  label,
  options = {},
  input: { value: valueProp },
  tooltip
}) => {
  const {
    format = FormatEnum.Text,
    fractionDigits = 2,
    formatType = FormatType.Currency,
    zeroAsEmpty = false,
    inline = true,
    inlineFullText = false,
    truncate = true,
    boldTextValue = false,
    isCamel = false,
    valueClass,
    lineTruncate = 1,
    ellipsisMoreText,
    ellipsisLessText,

    link = '',
    linkDataField = '',
    linkArgs = [],
    resizable = true,
    handleTruncateProp
  } = options;

  const value = useMemo(
    () => (link ? valueProp[linkDataField] : valueProp),
    [valueProp, link, linkDataField]
  );

  const followRef = useRef<HTMLDivElement | null>(null);

  const to = useMemo(
    () =>
      link.replace(/(\{\d\})/g, a => {
        const idx = Number.parseInt(a.replace(/[\{\}]/g, ''));
        if (linkArgs.length <= idx || !valueProp[linkArgs[idx]]) return a;

        return valueProp[linkArgs[idx]];
      }),
    [link, linkArgs, valueProp]
  );

  const handleTruncate = useCallback(
    (isTruncated: boolean, showAll: boolean) => {
      typeof handleTruncateProp === 'function' &&
        handleTruncateProp(isTruncated, showAll);
    },
    [handleTruncateProp]
  );

  const truncateText = useCallback(
    (val: any) =>
      truncate ? (
        <TruncateText
          id={`${id}__truncate-text`}
          className={classnames(
            format === FormatEnum.Heading && 'fs-16 fw-500 color-grey-d20',
            boldTextValue && 'fw-600'
          )}
          resizable={resizable}
          title={val}
          followRef={followRef}
          lines={lineTruncate}
          ellipsisMoreText={ellipsisMoreText}
          ellipsisLessText={ellipsisLessText}
          onTruncate={handleTruncate}
        >
          {val?.toString()}
        </TruncateText>
      ) : (
        val
      ),
    [
      truncate,
      id,
      format,
      boldTextValue,
      resizable,
      lineTruncate,
      ellipsisMoreText,
      ellipsisLessText,
      handleTruncate
    ]
  );

  const renderAmount = useCallback(
    (val: string) => {
      let content = val;
      switch (formatType) {
        case FormatType.Currency:
          content = formatCommon(val).currency(fractionDigits)!;
          break;
        case FormatType.Quantity:
          content = formatCommon(val).quantity!;
          break;
        case FormatType.Percent:
          content = formatCommon(val).percent(fractionDigits)!;
          break;
      }

      return truncateText(content);
    },
    [formatType, fractionDigits, truncateText]
  );

  const formatGeneration = useCallback(
    (val: any) => {
      switch (format) {
        case FormatEnum.Date:
          return formatCommon(val).time.date;
        case FormatEnum.DateUTC:
          return formatCommon(val).time.dateUTC;
        case FormatEnum.AllDatetime:
          return formatCommon(val).time.allDatetime;
        case FormatEnum.ShortMonthYear:
          return formatCommon(val).time.shortMonthYear;
        case FormatEnum.Amount:
          return renderAmount(val);
        case FormatEnum.PhoneNumber:
          return formatContactPhone(val);
        case FormatEnum.MethodType:
          return formatCommon(val).methodType;
        case FormatEnum.DateRange:
          const start = formatCommon(val?.start).time.date;
          const end = formatCommon(val?.end).time.date;

          return (start || end) && `${start} - ${end}`;
        case FormatEnum.TaxpayerId:
          return formatTaxpayerId(val);
        default:
          return truncateText(val);
      }
    },
    [format, renderAmount, truncateText]
  );

  const textValue = useMemo(
    () =>
      value || (!zeroAsEmpty && value === 0) ? formatGeneration(value) : '',
    [value, zeroAsEmpty, formatGeneration]
  );

  const defaultSize = useMemo(() => ({ width: 0, height: 0 }), []);
  const [tooltipTriggerSize, setTooltipTriggerSize] = useState(defaultSize);

  useEffect(() => {
    const { width: labelWidth, height: labelHeight } =
      document.getElementById(`${id}__label`)?.getBoundingClientRect() ||
      defaultSize;
    const { width: textWidth, height: textHeight } =
      document.querySelector(`#${id}__text > span`)?.getBoundingClientRect() ||
      defaultSize;

    setTooltipTriggerSize({
      width: labelWidth + textWidth,
      height: labelHeight || textHeight
    });
  }, [id, defaultSize]);

  const valueElement = useMemo(() => {
    const classNames = classnames('form-group-static__text', valueClass, {
      'flex-fill': inline
    });
    const text = isCamel ? camelToText(value) : textValue;

    return (
      <div id={`${id}__text`} className={classNames} ref={followRef}>
        {link && text ? (
          <Link className="link-abbr" to={to}>
            {text}
          </Link>
        ) : (
          text
        )}
      </div>
    );
  }, [id, isCamel, textValue, value, link, to, inline, valueClass]);

  const valueElementWithLabel = useMemo(() => {
    const classNames = classnames('form-group-static__text', valueClass, {
      'flex-fill': inline
    });

    return (
      <div id={`${id}__text`} className={classNames} ref={followRef}>
        {label && (
          <span id={`${id}__label`} className="color-grey text-nowrap mr-2">
            {label}
          </span>
        )}
        {textValue}
      </div>
    );
  }, [id, textValue, inline, valueClass, label]);

  if (tooltip && inline) {
    return (
      <div
        id={id}
        className="form-group-static mt-4 d-flex align-items-center position-relative"
      >
        <Tooltip
          triggerClassName="mt-4 position-absolute"
          element={tooltip}
          opened={isDevice ? false : undefined}
        >
          <div style={tooltipTriggerSize} className="invisible" />
        </Tooltip>
        {label && (
          <div id={`${id}__label`} className="color-grey text-nowrap mr-2">
            {label}
          </div>
        )}

        {valueElement}
      </div>
    );
  }

  return (
    <div
      id={id}
      ref={inline ? undefined : followRef}
      className={classnames('form-group-static', {
        'mt-4 d-flex align-items-center w-100': inline
      })}
    >
      {inlineFullText ? (
        valueElementWithLabel
      ) : (
        <>
          {inline ? (
            label && (
              <div id={`${id}__label`} className="color-grey text-nowrap mr-2">
                {label}
              </div>
            )
          ) : (
            <span id={`${id}__label`} className="form-group-static__label">
              <TruncateText resizable title={label} followRef={followRef}>
                {label}
              </TruncateText>
            </span>
          )}

          {valueElement}
        </>
      )}
    </div>
  );
};

export default GroupTextControl;
