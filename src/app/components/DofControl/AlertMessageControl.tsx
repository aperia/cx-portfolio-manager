import { classnames } from 'app/helpers';
import { Icon } from 'app/_libraries/_dls';
import { CommonControlProps } from 'app/_libraries/_dof/core';
import React from 'react';

export interface AlertMessageControlProps extends CommonControlProps {
  options?: {
    message?: string;
    multiLinesMessage?: boolean;
  };
}

const AlertMessage: React.FC<AlertMessageControlProps> = ({
  input: { value },
  id,
  options
}) => {
  if (value && value.length > 0 && options?.multiLinesMessage) {
    return (
      <div className="d-flex mb-16">
        <div className="color-orange-d08 mr-8 mt-i-4">
          <Icon name="warning" size="4x" />
        </div>
        <div>
          {value.map((message: string, index: number) => {
            const isLastIndex = index === value.length - 1;
            return (
              <p
                key={index}
                className={classnames('fs-12 color-grey-l16', {
                  'mb-16': !isLastIndex
                })}
              >
                {message}
              </p>
            );
          })}
        </div>
      </div>
    );
  }
  return !value || typeof value === 'string' ? (
    <div className="d-flex mb-16">
      <div className="color-orange-d08 mr-8 mt-i-4">
        <Icon name="warning" size="4x" />
      </div>
      <p id={id} className="color-grey fs-12">
        {value || options?.message}
      </p>
    </div>
  ) : (
    <React.Fragment></React.Fragment>
  );
};

export default AlertMessage;
