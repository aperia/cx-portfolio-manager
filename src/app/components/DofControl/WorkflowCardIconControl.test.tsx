import WorkflowCardIconControl, {
  WorkflowCardIconControlProps
} from 'app/components/DofControl/WorkflowCardIconControl';
import { getDefaultDOFMockProps, renderComponent } from 'app/utils';
import React from 'react';

jest.mock('app/components/DofControl/IconControl', () => ({
  __esModule: true,
  default: ({ input, options }: any) => {
    return (
      <div id={input.id}>
        <i className={`icon icon-${options.iconProps.name}`}></i>
      </div>
    );
  }
}));

const mockProps = getDefaultDOFMockProps();

const testId = 'workflow-card-icon-control';

describe('Test Workflow Card Icon Control', () => {
  it('render to UI empty', async () => {
    const { getByTestId } = await renderComponent(
      testId,
      <WorkflowCardIconControl {...mockProps} input={{ value: undefined }} />
    );
    expect(getByTestId(testId)).toBeEmptyDOMElement();
  });

  it('render to UI', async () => {
    const props: WorkflowCardIconControlProps = {
      id: testId,
      input: { ...mockProps.input, value: { id: 'test id' } },
      meta: mockProps.meta,
      options: {
        iconProps: {
          name: 'cross'
        }
      }
    };

    const { getByTestId, debug } = await renderComponent(
      testId,
      <WorkflowCardIconControl {...props} />
    );
    debug();
    expect(
      getByTestId(testId).querySelector('.icon.icon-cross')
    ).toBeInTheDocument();
  });

  it('render to UI lock', async () => {
    const props: WorkflowCardIconControlProps = {
      id: testId,
      input: { ...mockProps.input, value: { id: 'test id' } },
      meta: mockProps.meta,
      options: {
        iconProps: {
          name: 'lock'
        }
      }
    };

    const { getByTestId, debug } = await renderComponent(
      testId,
      <WorkflowCardIconControl {...props} />
    );
    debug();

    expect(
      getByTestId(testId).querySelector('.icon.icon-lock')
    ).toBeInTheDocument();
  });
});
