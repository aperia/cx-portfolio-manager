import { fireEvent, RenderResult } from '@testing-library/react';
import MaskedTextBoxControl, {
  MaskedTextBoxControlProps
} from 'app/components/DofControl/MaskedTextBoxControl';
import { renderComponent } from 'app/utils';
import { getDefaultDOFMockProps } from 'app/utils/getDefaultDOFMockProps';
import React from 'react';

const idDomTest = 'test';
const defaultProps = getDefaultDOFMockProps();

describe('test MaskedTextBoxControl', () => {
  let renderResult: RenderResult;
  const createWrapper = async (...props: MaskedTextBoxControlProps[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    renderResult = await renderComponent(
      idDomTest,
      <MaskedTextBoxControl {...defaultProps} {...mergeProps} />
    );
  };

  it('should have valueProp and mask', async () => {
    const mockData = getDefaultDOFMockProps({
      value: '1234',
      mask: [
        {
          isRegex: false,
          pattern: '('
        },
        {
          isRegex: true,
          pattern: '\\d'
        },
        {
          isRegex: true,
          pattern: '\\d'
        },
        {
          isRegex: true,
          pattern: '\\d'
        },
        {
          isRegex: false,
          pattern: ')'
        },
        {
          isRegex: false,
          pattern: ' '
        },
        {
          isRegex: true,
          pattern: '\\d'
        },
        {
          isRegex: true,
          pattern: '\\d'
        },
        {
          isRegex: true,
          pattern: '\\d'
        },
        {
          isRegex: false,
          pattern: '-'
        },
        {
          isRegex: true,
          pattern: '\\d'
        },
        {
          isRegex: true,
          pattern: '\\d'
        },
        {
          isRegex: true,
          pattern: '\\d'
        },
        {
          isRegex: true,
          pattern: '\\d'
        }
      ]
    });
    await createWrapper(mockData);
    const { getByDisplayValue } = renderResult;
    expect(getByDisplayValue(/123/i)).toBeInTheDocument();
  });

  it('should have onBlur', async () => {
    const onBlurFunc = jest.fn();
    const mockData = getDefaultDOFMockProps({
      onBlur: onBlurFunc
    });

    await createWrapper(mockData);
    const { getByRole } = renderResult;
    fireEvent.blur(getByRole('textbox'));
    expect(onBlurFunc).toBeCalled();
  });

  it('should use pattern as string if new RegExp(pattern) error', async () => {
    const mockData = getDefaultDOFMockProps({
      value: '1234',
      mask: [
        {
          isRegex: true,
          pattern: '('
        },
        {
          isRegex: true,
          pattern: '\\d'
        },
        {
          isRegex: true,
          pattern: '\\d'
        },
        {
          isRegex: true,
          pattern: '\\d'
        },
        {
          isRegex: false,
          pattern: ')'
        },
        {
          isRegex: false,
          pattern: ' '
        },
        {
          isRegex: true,
          pattern: '\\d'
        },
        {
          isRegex: true,
          pattern: '\\d'
        },
        {
          isRegex: true,
          pattern: '\\d'
        },
        {
          isRegex: false,
          pattern: '-'
        },
        {
          isRegex: true,
          pattern: '\\d'
        },
        {
          isRegex: true,
          pattern: '\\d'
        },
        {
          isRegex: true,
          pattern: '\\d'
        },
        {
          isRegex: true,
          pattern: ''
        }
      ]
    });

    await createWrapper(mockData);
    const { getByDisplayValue } = renderResult;
    expect(getByDisplayValue(/123/i)).toBeInTheDocument();
  });

  it('should have error', async () => {
    const mockData = getDefaultDOFMockProps({
      meta: { touched: true, invalid: true }
    });

    await createWrapper(mockData);
    const { getByRole } = renderResult;
    expect(getByRole('textbox')).toBeInTheDocument();
  });
});
