import { ButtonActionType } from 'app/constants/button-action-types';
import { MIME_TYPE } from 'app/constants/mine-type';
import { canSetupWorkflow } from 'app/helpers';
import { base64toBlob, downloadBlobFile } from 'app/helpers/fileUtilities';
import { actionsDMMTables } from 'pages/_commons/redux/DMMTables';
import { actionsPortfolio } from 'pages/_commons/redux/Portfolio';
import { actionsWorkflow } from 'pages/_commons/redux/Workflow';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import { actionsToast } from 'pages/_commons/ToastNotifications/redux';

const defaultFnc: TButtonActionFnc = () => {};

export const openWorkflow: TButtonActionFnc = (value, dispatch, event) => {
  if (canSetupWorkflow(value?.name)) {
    dispatch(
      actionsWorkflowSetup.setWorkflowSetup({
        isTemplate: true,
        workflow: value
      })
    );
  }
};

export const selectKpi: TButtonActionFnc = (value, dispatch, event) => {
  dispatch(
    actionsPortfolio.setOpenedChoosePopup({
      isChooseOpen: false,
      value: { ...value, mode: 'Add' },
      isAddOpen: true
    })
  );
};

export const unFavoriteWorkflow: TButtonActionFnc = async (
  workflowId,
  dispatch,
  event
) => {
  dispatch(actionsWorkflow.unfavoriteWorkflowTemplate(workflowId));
};

export const favoriteWorkflow: TButtonActionFnc = async (
  workflowId,
  dispatch,
  event
) => {
  dispatch(actionsWorkflow.favoriteWorkflowTemplate(workflowId));
};

export const editWorkflow: TButtonActionFnc = (value, dispatch, event) => {
  if (canSetupWorkflow(value?.name)) {
    dispatch(
      actionsWorkflowSetup.setWorkflowSetup({
        isTemplate: false,
        workflow: value
      })
    );
  }
};

export const downloadDMMTable: TButtonActionAsyncFnc = async (
  dmmTableId,
  dispatch,
  event
) => {
  event?.stopPropagation();
  if (dmmTableId === 'disabled') return;

  const result: any = await Promise.resolve(
    dispatch(actionsDMMTables.downloadDecisionElement(dmmTableId))
  );

  const isSuccess =
    actionsDMMTables.downloadDecisionElement.fulfilled.match(result);

  if (isSuccess) {
    const {
      fileStream = '',
      mimeType = '',
      filename = ''
    } = result?.payload as IDownloadAttachment;

    let fileName = filename;
    if (filename.split('.').length < 2) {
      const ext = MIME_TYPE[mimeType]?.extensions[0] || '';
      fileName = `${filename}.${ext}`;
    }
    const blol = base64toBlob(fileStream, mimeType);
    downloadBlobFile(blol, fileName);
  } else {
    dispatch(
      actionsToast.addToast({
        type: 'error',
        message: 'File failed to download.'
      })
    );
  }
};

export const openStrategyFlyout: TButtonActionFnc = (
  value,
  dispatch,
  event
) => {
  dispatch(actionsWorkflowSetup.setStrategyFlyout(value));
};

const actions: Record<
  keyof typeof ButtonActionType,
  TButtonActionFnc | TButtonActionAsyncFnc
> = {
  openWorkflow,
  unFavoriteWorkflow,
  favoriteWorkflow,
  editWorkflow,
  downloadDMMTable,
  openStrategyFlyout,
  selectKpi
};

export const getButtonControlAction = (
  actionName?: keyof typeof ButtonActionType
) => (actionName ? actions[actionName] : defaultFnc);
