import { fireEvent, RenderResult } from '@testing-library/react';
import GroupTextMoreControl, {
  GroupTextMoreControlProps
} from 'app/components/DofControl/GroupTextMoreControl';
import { GroupTextFormat, GroupTextFormatType } from 'app/constants/enums';
import { getDefaultDOFMockProps, renderComponent } from 'app/utils';
import React from 'react';

const idDomTest = 'group-text-more-control-id';
const mockProps = getDefaultDOFMockProps({ id: idDomTest });

describe('GroupTextControl', () => {
  let wrapper: RenderResult;

  const createWrapper = async (...props: GroupTextMoreControlProps[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    wrapper = await renderComponent(
      idDomTest,
      <GroupTextMoreControl {...mockProps} {...mergeProps} />
    );
  };

  const mockValue = (
    value: any,
    options?: {
      format?: GroupTextFormat;
      fractionDigits?: number;
      formatType?: GroupTextFormatType;
      zeroAsEmpty?: boolean;
      inline?: boolean;
      truncate?: boolean;
      boldTextValue?: boolean;
      isCamel?: boolean;
    }
  ) => {
    return {
      input: { ...mockProps.input, value },
      options
    } as GroupTextMoreControlProps;
  };

  describe('with label', () => {
    const testWithLabelProps = {
      label: 'Mock Label'
    } as GroupTextMoreControlProps;

    beforeEach(() => {
      jest.resetModules();
    });

    it('should show empty value when value = ""', async () => {
      await createWrapper({
        ...testWithLabelProps,
        options: { ...testWithLabelProps?.options, inline: false }
      });

      const { queryByText } = wrapper;

      expect(queryByText('Mock Label')).toBeInTheDocument();
    });

    it('should show tooltip with scrollbar in case more than 10 items', async () => {
      await createWrapper(
        testWithLabelProps,
        mockValue(
          [
            'Mock value 1',
            'Mock value 2',
            'Mock value 3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9',
            '10',
            '11',
            '12'
          ],
          {
            boldTextValue: true
          }
        )
      );

      const { queryByText } = wrapper;
      expect(queryByText('Mock Label')).toBeInTheDocument();
      expect(queryByText('Mock value 1')).toBeInTheDocument();
      expect(queryByText('Mock value 1')!.className).toContain('fw-600');

      const linkMore = queryByText('+11', { exact: false });
      fireEvent.click(linkMore!);
      expect(queryByText('Mock value 2')).toBeInTheDocument();
    });

    it('should show 0 value when format = "text" and zeroAsEmpty = false', async () => {
      await createWrapper(
        testWithLabelProps,
        mockValue([0], {
          zeroAsEmpty: false
        })
      );

      const { queryByText } = wrapper;
      expect(queryByText('Mock Label')).toBeInTheDocument();
      expect(queryByText('0')).toBeInTheDocument();
    });

    it('should show as date format when format = "date"', async () => {
      const dateValue = new Date('2020-12-25T10:00:00.000');
      await createWrapper(
        testWithLabelProps,
        mockValue([dateValue], { format: GroupTextFormat.Date })
      );
      const { queryByText } = wrapper;

      expect(queryByText('Mock Label')).toBeInTheDocument();
      expect(queryByText('12/25/2020')).toBeInTheDocument();
    });

    it('should show as shortMonthYear format when format = "shortMonthYear"', async () => {
      const dateValue = new Date('2020-12-25T10:00:00.000');
      await createWrapper(
        testWithLabelProps,
        mockValue([dateValue], { format: GroupTextFormat.ShortMonthYear })
      );

      const { queryByText } = wrapper;

      expect(queryByText('Mock Label')).toBeInTheDocument();
      expect(queryByText('12/2020')).toBeInTheDocument();
    });

    it('should show as phone number format when format = "phoneNumber"', async () => {
      await createWrapper(
        testWithLabelProps,
        mockValue(['0123456789'], { format: GroupTextFormat.PhoneNumber })
      );

      const { queryByText } = wrapper;

      expect(queryByText('Mock Label')).toBeInTheDocument();
      expect(queryByText('(012) 345-6789')).toBeInTheDocument();
    });

    it('should show as currency format when format = "amount" and format type = "currency"', async () => {
      await createWrapper(
        testWithLabelProps,
        mockValue([1234], { format: GroupTextFormat.Amount })
      );

      const { queryByText } = wrapper;

      expect(queryByText('Mock Label')).toBeInTheDocument();
      expect(queryByText('$1,234.00')).toBeInTheDocument();
    });

    it('should show as currency format with 3 fraction digits when format = "amount", format type = "currency" and fractionDigits = 3', async () => {
      await createWrapper(
        testWithLabelProps,
        mockValue([1234.123], {
          format: GroupTextFormat.Amount,
          fractionDigits: 3
        })
      );

      const { queryByText } = wrapper;

      expect(queryByText('Mock Label')).toBeInTheDocument();
      expect(queryByText('$1,234.123')).toBeInTheDocument();
    });

    it('should show as quantity format when format = "amount" and format type = "quantity"', async () => {
      await createWrapper(
        testWithLabelProps,
        mockValue([1234], {
          format: GroupTextFormat.Amount,
          formatType: GroupTextFormatType.Quantity
        })
      );

      const { queryByText } = wrapper;

      expect(queryByText('Mock Label')).toBeInTheDocument();
      expect(queryByText('1,234')).toBeInTheDocument();
    });

    it('should show as percent format when format = "amount" and format type = "percent"', async () => {
      await createWrapper(
        testWithLabelProps,
        mockValue([10], {
          format: GroupTextFormat.Amount,
          formatType: GroupTextFormatType.Percent
        })
      );

      const { queryByText } = wrapper;

      expect(queryByText('Mock Label')).toBeInTheDocument();
      expect(queryByText('10.00%')).toBeInTheDocument();
    });
  });

  describe('without label', () => {
    it('should show empty value when value = ""', async () => {
      await createWrapper();

      const { queryByText } = wrapper;

      expect(queryByText('Mock Label')).toBeNull();
    });
  });
});
