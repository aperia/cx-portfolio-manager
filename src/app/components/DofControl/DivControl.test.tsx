import DivControl from 'app/components/DofControl/DivControl';
import { getDefaultDOFMockProps, renderComponent } from 'app/utils';
import React from 'react';

const mockProps = getDefaultDOFMockProps();

const testId = 'div-control';

describe('Test Div Control', () => {
  it('render to UI', async () => {
    const { getByTestId } = await renderComponent(
      testId,
      <DivControl {...mockProps} />
    );
    expect(getByTestId(testId).querySelector('div')).toBeInTheDocument();
  });
});
