import { fireEvent } from '@testing-library/react';
import ComboBoxControl from 'app/components/DofControl/ComboBoxControl';
import { getDefaultDOFMockProps, renderComponent } from 'app/utils';
import 'app/utils/_mockComponent/mockCanvas';
import React from 'react';

const mockProps = getDefaultDOFMockProps();

const testId = 'combobox-control';

describe('ComboBoxControl', () => {
  const data = [
    {
      id: 1,
      value: 'value-item-1'
    },
    { id: 2, value: 'value-item-2' }
  ];

  beforeEach(() => {
    jest.useFakeTimers();
  });

  it('render without error when data is undefined and textField is undefined', async () => {
    const { queryByText } = await renderComponent(
      testId,
      <div>Mock label</div>
    );
    jest.runAllTimers();

    expect(queryByText('Mock label'));
  });

  it('should have error when required property is true and invalid is true', async () => {
    const testProps = {
      id: 'combobox-test-id',
      label: 'Mock label',
      textField: 'value'
    };
    const cbbMockProps = getDefaultDOFMockProps({
      meta: {
        invalid: true,
        touched: true
      }
    });

    const wrapper = await renderComponent(
      testId,
      <ComboBoxControl
        {...cbbMockProps}
        label={testProps.label}
        id={testProps.id}
        required
        enable
      />
    );
    jest.runAllTimers();

    expect(wrapper.queryByText('Mock label')).toBeInTheDocument();

    expect(
      wrapper
        .queryByText('Mock label')
        ?.parentElement?.querySelector('.icon-error')
    ).toBeInTheDocument();
  });

  it('render with default value', async () => {
    const testProps = {
      id: 'combobox-test-id',
      label: 'Mock label',
      textField: 'value'
    };

    const wrapper = await renderComponent(
      testId,
      <ComboBoxControl
        {...mockProps}
        data={data as []}
        textField={testProps.textField}
        label={testProps.label}
        id={testProps.id}
        value={data[0]}
      />
    );
    jest.runAllTimers();

    expect(wrapper.queryByText('Mock label')).toBeInTheDocument();

    const input = wrapper
      .queryByText('Mock label')!
      .parentElement?.parentElement?.querySelector('input');

    expect(input?.value).toEqual('value-item-1');
  });

  it('should render second value when onchange event to this item is called ', async () => {
    const testProps = {
      id: 'combobox-test-id',
      label: 'Mock label',
      textField: 'value'
    };

    const { queryByText } = await renderComponent(
      testId,
      <ComboBoxControl
        {...mockProps}
        data={data as []}
        textField={testProps.textField}
        label={testProps.label}
        id={testProps.id}
        opened
      />
    );
    jest.runAllTimers();

    expect(queryByText('Mock label')).toBeInTheDocument();
    const input =
      queryByText('Mock label')!.parentElement?.parentElement?.querySelector(
        'input'
      );
    const item = queryByText(data[1].value);
    fireEvent.click(item!);
    expect(input?.value).toEqual(data[1].value);
  });

  it('should still render current value when onblur event is called', async () => {
    const onChangeFn = jest.fn(),
      onBlurFn = jest.fn();
    const testProps = {
      id: 'combobox-test-id',
      label: 'Mock label',
      textField: 'value'
    };

    const wrapper = await renderComponent(
      testId,
      <ComboBoxControl
        {...mockProps}
        data={data as []}
        input={{ value: '', onChange: onChangeFn, onBlur: onBlurFn }}
        textField={testProps.textField}
        label={testProps.label}
        id={testProps.id}
      />
    );

    jest.runAllTimers();
    const { queryByText } = wrapper;

    expect(queryByText('Mock label')).toBeInTheDocument();

    const cbbIcon = wrapper.baseElement.querySelector(
      '.icon.icon-chevron-down'
    );
    // Mock click on chevron icon > input have no focus
    fireEvent.mouseDown(cbbIcon!);
    // Expect render Combobox Items
    expect(queryByText(/value-item-1/i)).toBeInTheDocument();

    // Mock click on popup
    fireEvent.mouseDown(queryByText(/value-item-1/i).closest('.dls-popup')!);
    // Mock click on item
    fireEvent.click(queryByText(/value-item-2/i));

    //Mock click on icon > input has focus
    const input: HTMLInputElement | null = wrapper.baseElement.querySelector(
      '.text-field-container input'
    );

    fireEvent.focus(input!);
    fireEvent.mouseDown(cbbIcon!);
    expect(input.value).toEqual('value-item-2');
  });
});
