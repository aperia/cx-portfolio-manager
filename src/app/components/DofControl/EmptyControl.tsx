import { CommonControlProps } from 'app/_libraries/_dof/core';
import React from 'react';

export interface EmptyControlProps extends CommonControlProps {}

const EmptyControl: React.FC<EmptyControlProps> = () => {
  return <React.Fragment />;
};

export default EmptyControl;
