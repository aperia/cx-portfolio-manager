import { RenderResult } from '@testing-library/react';
import AlertMessageControl, {
  AlertMessageControlProps
} from 'app/components/DofControl/AlertMessageControl';
import { getDefaultDOFMockProps, renderComponent } from 'app/utils';
import React from 'react';

const idDomTest = 'alert-message-control-id';
const mockProps = getDefaultDOFMockProps({ id: idDomTest });

describe('AlertMessageControl', () => {
  let wrapper: RenderResult;

  const createWrapper = async (...props: AlertMessageControlProps[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    wrapper = await renderComponent(
      idDomTest,
      <AlertMessageControl {...mockProps} {...mergeProps} />
    );
  };

  const mockValue = (
    value: any,
    options?: {
      message?: string;
      multiLinesMessage?: boolean;
    }
  ) => {
    return {
      input: { ...mockProps.input, value },
      options
    } as AlertMessageControlProps;
  };

  it("should show single message when it's string value", async () => {
    await createWrapper(mockValue('mock string mesage'));
    const { getByText } = wrapper;

    expect(getByText('mock string mesage')).not.toBeEmptyDOMElement();
  });

  it("should show options's message when the value is nill", async () => {
    await createWrapper(mockValue(null, { message: 'message' }));
    const { getByText } = wrapper;

    expect(getByText('message')).not.toBeEmptyDOMElement();
  });

  it("should show options's message when the value is empty", async () => {
    await createWrapper(mockValue('', { message: 'message' }));
    const { getByText } = wrapper;

    expect(getByText('message')).not.toBeEmptyDOMElement();
  });

  it("should show multiple line when it's array value and multiLinesMessage = true", () => {
    createWrapper(
      mockValue(['mock alert 1', 'mock alert 2'], { multiLinesMessage: true })
    );
    const { getByText } = wrapper;

    expect(getByText('mock alert 1')).not.toBeEmptyDOMElement();
    expect(getByText('mock alert 2')).not.toBeEmptyDOMElement();
  });

  it("should show nothing when it's array value", async () => {
    createWrapper(mockValue(['mock alert 1', 'mock alert 2']));
    const { queryByText } = wrapper;

    expect(queryByText('mock alert 1')).toBeNull();
  });

  it("should show nothing when it's empty array value", () => {
    createWrapper(mockValue([]));
    const { queryByText } = wrapper;

    expect(queryByText('mock alert 1')).toBeNull();
  });
});
