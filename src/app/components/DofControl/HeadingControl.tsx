import { classnames } from 'app/helpers';
import { Icon, Tooltip, TruncateText } from 'app/_libraries/_dls';
import { CommonControlProps } from 'app/_libraries/_dof/core';
import React from 'react';
export interface HeadingControlProps extends CommonControlProps {
  as?: React.ElementType;
  name?: string;
  layoutProps?: any;
  headingClassName?: string;
  tooltip?: string;
  className?: string;
  truncate?: boolean;
  truncateLines?: number;
}

const HeadingControl: React.FC<HeadingControlProps> = ({
  input: { value },
  as: Component = 'h5',
  label,
  children,
  tooltip,
  id,
  layoutProps,
  headingClassName = '',
  truncate = false,
  truncateLines = 1,
  name,
  ...props
}) => {
  if (truncate) {
    return (
      <div className="d-flex align-items-center">
        <TruncateText
          id={id}
          title={value || label || children}
          className={classnames(`${Component}`, headingClassName, {
            'mr-8': tooltip
          })}
          resizable
          lines={truncateLines}
        >
          {value || label || children}
        </TruncateText>
        {tooltip && (
          <Tooltip placement="top" element={tooltip}>
            <Icon name="information" size="4x" className="color-grey-l32" />
          </Tooltip>
        )}
      </div>
    );
  }

  return (
    <div className="d-flex align-items-center">
      <Component
        id={id}
        className={classnames(headingClassName, {
          'mr-8': tooltip
        })}
      >
        {value || label || children}
      </Component>
      {tooltip && (
        <Tooltip placement="top" element={tooltip}>
          <Icon name="information" size="4x" className="color-grey-l32" />
        </Tooltip>
      )}
    </div>
  );
};

export default HeadingControl;
