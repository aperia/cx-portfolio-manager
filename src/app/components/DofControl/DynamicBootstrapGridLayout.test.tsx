import { RenderResult } from '@testing-library/react';
import DynamicBootstrapGridLayout, {
  DynamicBootstrapGridLayoutProps
} from 'app/components/DofControl/DynamicBootstrapGridLayout';
import * as hooks from 'app/hooks/useRegisteredComponentsContext';
import { renderWithMockStore } from 'app/utils';
import {
  RegisteredComponentsContextData,
  RegisteredFieldsMap
} from 'app/_libraries/_dof/core/contexts/RegisteredComponentsContext';
import React from 'react';
import * as reactRedux from 'react-redux';

const useSelectorMock = jest.spyOn(reactRedux, 'useSelector');
const useRegisteredComponentsContextMock = jest.spyOn(
  hooks,
  'useRegisteredComponentsContext'
);
const mockRegisteredComponents = () => {
  useRegisteredComponentsContextMock.mockImplementation(
    () =>
      ({
        registeredFields: {
          component1_text: {
            type: 'component1',
            dataField: 'dataField1'
          },
          component2_text: {
            type: 'component2',
            dataField: 'dataField2'
          }
        } as RegisteredFieldsMap
      } as RegisteredComponentsContextData)
  );
};

const mockFormKey = 'formKey';
const MockControl: React.FC<{ formKey: string }> = ({ formKey, children }) => (
  <React.Fragment>{children}</React.Fragment>
);
const mockProps = {
  viewElements: [
    {
      name: 'component1_text',
      renderedNode: <MockControl formKey={mockFormKey}>value1</MockControl>
    },
    {
      name: 'component2_text',
      renderedNode: <MockControl formKey={mockFormKey}>value2</MockControl>
    }
  ]
} as DynamicBootstrapGridLayoutProps;

const testId = 'dynamic-bootstrap-grid-layout';

describe('DynamicBootstrapGridLayout', () => {
  let wrapper: RenderResult;

  beforeEach(() => {
    mockRegisteredComponents();
  });
  afterEach(() => {
    useRegisteredComponentsContextMock.mockClear();
    useSelectorMock.mockClear();
  });

  const mockReduxStoreFormData = (data?: any) => {
    useSelectorMock.mockImplementation(selector =>
      selector({ form: { [mockFormKey]: { values: data } } })
    );
  };

  const createWrapper = async (...props: DynamicBootstrapGridLayoutProps[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    const storeConfig = {
      initialState: {}
    };
    wrapper = await renderWithMockStore(
      <DynamicBootstrapGridLayout {...mockProps} {...mergeProps} />,
      storeConfig,
      {},
      testId
    );
  };

  it('should show empty when form data is empty', async () => {
    mockReduxStoreFormData();
    await createWrapper();
    const { getByTestId } = wrapper;

    expect(
      getByTestId(testId).querySelectorAll(`[role="dof-field"]`).length
    ).toEqual(0);
  });

  it('should show empty when form data does not includes any registered components dataField', async () => {
    mockReduxStoreFormData({ dataField_notMatch: 'not match' });
    await createWrapper();
    const { getByTestId } = wrapper;
    expect(
      getByTestId(testId).querySelectorAll(`[role="dof-field"]`).length
    ).toEqual(0);
  });

  it('should show expected FormControl when form data does includes registered component dataField', async () => {
    mockReduxStoreFormData({ dataField1: 'match' });
    await createWrapper();
    const { queryByText } = wrapper;

    expect(queryByText('value1')).toBeInTheDocument();
  });

  it('should show expected FormControl and custom layout className', () => {
    mockReduxStoreFormData({ dataField1: 'match' });

    const props = {
      ...mockProps,
      viewElements: mockProps.viewElements.map(e => ({
        ...e,
        layoutProps: { className: 'mock class' }
      }))
    };
    createWrapper(props);
    const { queryByText } = wrapper;

    expect(queryByText('value1')).toBeInTheDocument();
    expect(queryByText('value1')?.className).toContain('mock class');
  });

  it('should show expected FormControl and custom layout columnWidth', async () => {
    mockReduxStoreFormData({ dataField1: 'match' });

    const props = {
      ...mockProps,
      viewElements: mockProps.viewElements.map(e => ({
        ...e,
        layoutProps: { columnWidth: 2 }
      }))
    };
    await createWrapper(props);
    const { queryByText } = wrapper;

    expect(queryByText('value1')).toBeInTheDocument();
    expect(queryByText('value1')?.className).toContain('col-2');
  });

  it('should show expected FormControl and custom layout props', async () => {
    mockReduxStoreFormData({ dataField1: 'match' });

    const props = {
      ...mockProps,
      viewElements: mockProps.viewElements.map(e => ({
        ...e,
        layoutProps: { style: { width: '10%' } }
      }))
    };
    await createWrapper(props);
    const { queryByText } = wrapper;

    expect(queryByText('value1')).toBeInTheDocument();
    expect(queryByText('value1')?.style.width).toEqual('10%');
  });
});
