import { CommonControlProps } from 'app/_libraries/_dof/core';
import React from 'react';

export interface ImageControlProps extends CommonControlProps {
  className?: string;
  alt?: string;
}

const ImageControl: React.FC<ImageControlProps> = ({
  id,
  input: { value },
  className,
  alt
}) => {
  const imgSrc = value[0] === '/' ? '.' + value : value;
  return (
    <div id={id} className={className}>
      {value && <img id={`${id}__img`} src={imgSrc} alt={alt} />}
    </div>
  );
};

export default ImageControl;
