import { CommonControlProps } from 'app/_libraries/_dof/core';
import React from 'react';

export interface DividerControlProps extends CommonControlProps {}

const DividerControl: React.FC<DividerControlProps> = ({ ...props }) => {
  return <div className="divider-dashed" />;
};

export default DividerControl;
