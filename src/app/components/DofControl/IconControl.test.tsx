import { fireEvent } from '@testing-library/react';
import * as getButtonControlAction from 'app/components/DofControl/actions';
import IconControl, {
  IconControlProps
} from 'app/components/DofControl/IconControl';
import { ButtonActionType } from 'app/constants/button-action-types';
import { getDefaultDOFMockProps, renderComponent } from 'app/utils';
import React from 'react';
import * as reactRedux from 'react-redux';

const idDomTest = 'icon-control-id';
const mockProps = getDefaultDOFMockProps({ id: idDomTest });
const useDispatchMock = jest.spyOn(reactRedux, 'useDispatch');

const getButtonControlActionMock = jest.spyOn(
  getButtonControlAction,
  'getButtonControlAction'
);

describe('IconControl', () => {
  const action = jest.fn();

  beforeEach(() => {
    getButtonControlActionMock.mockReturnValue(action);
  });

  afterEach(() => {
    useDispatchMock.mockClear();
  });

  let wrapper: HTMLElement;

  const createWrapper = async (...props: IconControlProps[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    const renderResult = await renderComponent(
      idDomTest,
      <IconControl {...mockProps} {...mergeProps} />
    );
    wrapper = await renderResult.findByTestId(idDomTest);
  };

  const mockValue = (
    label: string,
    value: any,
    options?: {
      actionName?: string;
      iconProps?: any;
      tooltipProps?: any;
    }
  ) => {
    return {
      input: { ...mockProps.input, value },
      label,
      options
    } as IconControlProps;
  };

  describe('Icon Control', () => {
    it('should show with tooltip', async () => {
      await createWrapper(
        mockValue('', 'Value', {
          actionName: 'unFavoriteWorkflow',
          iconProps: {
            name: 'star-fill',
            size: '5x',
            color: 'orange-d04'
          },
          tooltipProps: {
            element: 'Favorite Workflow',
            placement: 'top',
            variant: 'primary'
          }
        })
      );
      expect(wrapper.querySelector('.icon.icon-star-fill')).toBeTruthy();
    });

    it('should show only icon without tooltip', async () => {
      await createWrapper(
        mockValue('', 'Value', {
          actionName: 'unFavoriteWorkflow',
          iconProps: {
            name: 'star-fill',
            size: '5x',
            color: 'orange-d04'
          }
        })
      );
      expect(wrapper.querySelector('.icon.icon-star-fill')).toBeTruthy();
    });

    it('should show nothing when value is empty', async () => {
      await createWrapper({ input: undefined } as any);
      expect(wrapper.querySelector('.icon.icon-star-fill')).toBeFalsy();
    });

    it('handle click', async () => {
      await createWrapper(
        mockValue('', 'Value', {
          actionName: ButtonActionType.unFavoriteWorkflow,
          iconProps: {
            name: 'star-fill',
            size: '5x',
            color: 'orange-d04'
          },
          tooltipProps: {
            element: 'Favorite Workflow',
            placement: 'top',
            variant: 'primary'
          }
        })
      );
      fireEvent.click(
        wrapper.querySelector('.icon.icon-star-fill') || document
      );
      expect(action).toBeCalled();
    });
  });
});
