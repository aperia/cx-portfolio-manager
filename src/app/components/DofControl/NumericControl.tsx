import {
  NumericProps,
  NumericTextBox,
  OnChangeNumericEvent
} from 'app/_libraries/_dls';
import { CommonControlProps } from 'app/_libraries/_dof/core';
import React, { useEffect, useState } from 'react';

export interface NumericControlProps
  extends CommonControlProps,
    Omit<NumericProps, 'id' | 'label'> {}

const NumericControl: React.FC<NumericControlProps> = ({
  input: { value: valueProp, onChange, onBlur, name },
  meta: { error, valid, invalid, touched },
  id,
  label,
  tooltip,
  enable,
  required,
  format,
  maxLength,
  readOnly,
  ...props
}) => {
  const [value, setValue] = useState<React.ReactText>(valueProp);

  const handleOnChange = (event: OnChangeNumericEvent) => {
    const { value: inputVal } = event.target;
    setValue(inputVal);
  };

  const handleOnBlur = (event: React.FocusEvent<HTMLInputElement>) => {
    const { value: inputVal } = event.target;
    onChange(inputVal);
    onBlur(inputVal);
  };

  useEffect(() => {
    setValue(valueProp);
  }, [valueProp]);

  return (
    <NumericTextBox
      id={id}
      label={label}
      value={value}
      format={format}
      maxLength={maxLength}
      required={required}
      error={
        enable && required && touched && invalid
          ? {
              message: error,
              status: invalid
            }
          : undefined
      }
      onChange={handleOnChange}
      onBlur={handleOnBlur}
    />
  );
};

export default NumericControl;
