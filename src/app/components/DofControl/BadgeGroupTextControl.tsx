import { SPECIAL_TYPE } from 'app/constants/constants';
import { BadgeColorType } from 'app/constants/enums';
import {
  camelToText,
  getActionTakenStatusColor,
  getChangeInTermsStatusColor,
  getChangeStatusColor,
  getConfigParameterStatusColor,
  getMethodTypeColor,
  getStrategyStatusColor,
  getTableTypeColor,
  getVersionCreatingColor,
  getWorkflowCategoryColor,
  getWorkflowStatusColor,
  getWorkflowTypeColor
} from 'app/helpers';
import {
  Badge,
  Button,
  Icon,
  IconNames,
  Tooltip,
  TruncateText
} from 'app/_libraries/_dls';
import { CommonControlProps } from 'app/_libraries/_dof/core';
import classNames from 'classnames';
import isFunction from 'lodash.isfunction';
import isObject from 'lodash.isobject';
import isString from 'lodash.isstring';
import React, { useEffect, useMemo, useRef } from 'react';

interface BadgeButtonAction {
  text: string;
  callbackAction: () => void;
  tooltip?: string;
}

export interface BadgeGroupTextControlProps extends CommonControlProps {
  id: any;
  label?: string;
  options?: {
    colorType?: BadgeColorType;
    noBorder?: boolean;
    inline?: boolean;
    iconButton?: string;
  };
}

const BadgeGroupTextControl: React.FC<BadgeGroupTextControlProps> = ({
  id,
  input,
  label,
  options = {}
}) => {
  const badgeRef = useRef<HTMLSpanElement | null>(null);

  const { colorType, noBorder, inline = true, iconButton } = options;
  const { value } = input || {};

  const valueText = isString(value) ? value : value?.text;

  const { callbackAction, tooltip } = (
    isObject(value) ? value : {}
  ) as BadgeButtonAction;

  const color = useMemo(() => {
    switch (colorType) {
      case BadgeColorType.WorkflowCategory:
        return getWorkflowCategoryColor(valueText);
      case BadgeColorType.WorkflowType:
        return getWorkflowTypeColor(valueText);
      case BadgeColorType.WorkflowStatus:
        return getWorkflowStatusColor(valueText);

      case BadgeColorType.CHANGE_STATUS:
        return getChangeStatusColor(valueText);
      case BadgeColorType.ChangeInTermsStatus:
        return getChangeInTermsStatusColor(valueText);
      case BadgeColorType.StrategyStatus:
        return getStrategyStatusColor(valueText);
      case BadgeColorType.ActionTaken:
        return getActionTakenStatusColor(valueText);
      case BadgeColorType.VersionCreating:
        return getVersionCreatingColor(valueText);
      case BadgeColorType.MethodType:
        return getMethodTypeColor(valueText);
      case BadgeColorType.TableType:
        return getTableTypeColor(valueText);

      case BadgeColorType.ConfigParameterStatus:
        return getConfigParameterStatusColor(valueText);

      case BadgeColorType.VersionCreated:
        return 'cyan';
      default:
        return 'grey';
    }
  }, [valueText, colorType]);

  const meanfulValue = useMemo(() => {
    switch (colorType) {
      case BadgeColorType.WorkflowType:
      case BadgeColorType.WorkflowStatus:
      case BadgeColorType.CHANGE_STATUS:
        return camelToText(SPECIAL_TYPE[`${valueText}`] || valueText);
      default:
        return valueText;
    }
  }, [valueText, colorType]);

  const handleClick = () => {
    isFunction(callbackAction) && callbackAction();
  };

  useEffect(() => {
    if (
      !badgeRef?.current ||
      !noBorder ||
      badgeRef.current.classList.contains('rounded')
    ) {
      return;
    }

    badgeRef.current.classList.add('rounded', 'border-0');
  }, [noBorder, valueText]);

  return (
    <div className={classNames('form-group-static', inline && 'mt-2')} id={id}>
      {inline ? (
        label && (
          <span id={`${id}__label`} className="color-grey d-inline">
            <TruncateText resizable title={label}>
              {label}
            </TruncateText>
          </span>
        )
      ) : (
        <span id={`${id}__label`} className="form-group-static__label">
          <TruncateText resizable title={label}>
            {label}
          </TruncateText>
        </span>
      )}
      <div
        id={`${id}__text`}
        className={classNames(
          'form-group-static__text',
          inline && 'd-inline',
          iconButton && 'd-flex'
        )}
      >
        <Badge id={`${id}__badge`} ref={badgeRef} color={color}>
          <span id={`${id}_value`}>{meanfulValue || 'N/A'}</span>
        </Badge>
        {iconButton && isObject(value) && (
          <div className="ml-8">
            <Tooltip
              element={tooltip}
              variant="primary"
              opened={!tooltip ? false : undefined}
              triggerClassName="d-flex"
            >
              <Button
                variant="icon-secondary"
                className="d-inline"
                size="sm"
                onClick={handleClick}
              >
                <Icon name={iconButton as IconNames} size="4x" />
              </Button>
            </Tooltip>
          </div>
        )}
      </div>
    </div>
  );
};

export default BadgeGroupTextControl;
