import { fireEvent, RenderResult } from '@testing-library/react';
import CheckBoxControl, {
  CheckBoxControlProps
} from 'app/components/DofControl/CheckBoxControl';
import { getDefaultDOFMockProps, renderComponent } from 'app/utils';
import React from 'react';

const idDomTest = 'check-box-control-id';
const onChange = jest.fn();
const mockProps = getDefaultDOFMockProps({ id: idDomTest, onChange });

jest.mock('app/_libraries/_dls/components/Tooltip', () => {
  return {
    __esModule: true,
    default: ({
      opened,
      element,
      onVisibilityChange,
      children
    }: {
      opened: boolean;
      element: JSX.Element;
      onVisibilityChange: () => void;
      children: JSX.Element;
    }) => {
      return (
        <div>
          <span>{opened ? 'Tooltip open' : 'Tooltip close'}</span>
          {element}
          {children}
          <button onClick={onVisibilityChange}>Visibility Change</button>
        </div>
      );
    }
  };
});

describe('CheckBoxControl', () => {
  let wrapper: RenderResult;

  const createWrapper = async (...props: CheckBoxControlProps[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    wrapper = await renderComponent(
      idDomTest,
      <CheckBoxControl {...mockProps} {...mergeProps} />
    );
  };

  const mockValue = (value: any) => {
    return {
      input: { ...mockProps.input, value }
    } as CheckBoxControlProps;
  };

  it('should on when value = true', async () => {
    await createWrapper(mockValue(true), {
      label: 'Mock Label'
    } as CheckBoxControlProps);

    const { queryByText } = wrapper;
    expect(queryByText('Mock Label')).toBeInTheDocument();
    expect(
      (queryByText('Mock Label')?.parentElement!.querySelector(
        'input'
      ) as HTMLInputElement).checked
    ).toEqual(true);
  });

  it('should off when have no input', async () => {
    await createWrapper({
      label: 'Mock Label',
      input: undefined
    } as any);

    const { queryByText } = wrapper;
    expect(queryByText('Mock Label')).toBeInTheDocument();
    expect(
      (queryByText('Mock Label')?.parentElement!.querySelector(
        'input'
      ) as HTMLInputElement).checked
    ).toEqual(false);
  });

  it('should show with tooltip', async () => {
    await createWrapper(mockValue(false), {
      label: 'Mock Label',
      message: 'Mock tooltip'
    } as CheckBoxControlProps);

    const { queryByText } = wrapper;

    expect(queryByText('Mock Label')).toBeInTheDocument();
    expect(queryByText('Mock tooltip')).toBeInTheDocument();
    expect(queryByText('Tooltip close')).toBeInTheDocument();

    fireEvent.click(queryByText('Visibility Change')!);
    expect(queryByText('Tooltip open')).toBeInTheDocument();
  });

  it('should not show tooltip if element empty', async () => {
    const { queryByText } = await renderComponent(
      idDomTest,
      <CheckBoxControl
        label={'Mock Label'}
        message={''}
        id={'test'}
        input={{} as any}
        meta={{} as any}
      />
    );

    expect(queryByText('Mock tooltip')).toBeNull();
    expect(queryByText('Tooltip close')).toBeInTheDocument();
  });

  it('should on when value = true', async () => {
    const onChangeFn = jest.fn();

    await createWrapper({
      label: 'Mock Label',
      input: { value: true, onChange: onChangeFn }
    } as any);

    const { queryByText } = wrapper;
    expect(queryByText('Mock Label')).toBeInTheDocument();

    const input = queryByText('Mock Label')?.parentElement!.querySelector(
      'input'
    ) as HTMLInputElement;

    expect(input.checked).toEqual(true);

    fireEvent.click(input);

    expect(onChangeFn).toBeCalled();
  });
});
