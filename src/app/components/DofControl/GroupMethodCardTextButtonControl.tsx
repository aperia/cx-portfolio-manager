// Types
// Constant
import { Button, useTranslation } from 'app/_libraries/_dls';
import React from 'react';
import { useDispatch } from 'react-redux';
import { getButtonControlAction } from './actions';
import GroupTextControl, { GroupTextControlProps } from './GroupTextControl';

export interface GroupMethodCardTextButtonControlProps
  extends GroupTextControlProps {}

const GroupMethodCardTextButtonControl: React.FC<GroupMethodCardTextButtonControlProps> =
  ({ id, options = {}, input, label: labelProps, ...rest }) => {
    const { t } = useTranslation();
    const dispatch = useDispatch();

    const {
      value: {
        id: methodId,
        name,
        relatedPricingStrategies,
        isCreateNewVersion
      }
    } = input;
    const { serviceSubjectSection, actionName } = options as Record<
      string,
      any
    >;
    const label = isCreateNewVersion
      ? 'Impacted Pricing Strategies: '
      : 'Related Pricing Strategies: ';
    const tooltip = `Pricing strategies that have ${name} assigned as ${serviceSubjectSection}.`;

    const action = getButtonControlAction(actionName);

    const handleClick = (
      e: React.MouseEvent<HTMLButtonElement, MouseEvent>
    ) => {
      action &&
        action({ id: methodId, isNewVersion: isCreateNewVersion }, dispatch, e);
    };

    return (
      <div className="d-flex align-items-start">
        <GroupTextControl
          id={id}
          label={label}
          options={options}
          input={{ ...input, value: relatedPricingStrategies }}
          tooltip={tooltip}
          {...rest}
        />
        {relatedPricingStrategies > 0 && (
          <Button
            variant="outline-primary"
            size="sm"
            className="ml-4"
            onClick={handleClick}
          >
            {t('txt_view')}
          </Button>
        )}
      </div>
    );
  };

export default GroupMethodCardTextButtonControl;
