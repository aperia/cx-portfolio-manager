// Helper
import { stringValidate } from 'app/helpers/stringValidate';
import { TextBox, TextBoxProps } from 'app/_libraries/_dls';
import { CommonControlProps } from 'app/_libraries/_dof/core';
import React, { useEffect, useState } from 'react';

export interface TextBoxControlProps
  extends CommonControlProps,
    Omit<TextBoxProps, 'id' | 'label'> {
  options?: {
    numberOnly?: boolean;
  };
}

export const TextBoxControl: React.FC<TextBoxControlProps> = ({
  input: { name, value: valueProp, onChange, onBlur },
  meta: { error: errorProp, valid, invalid, touched },
  id,
  label,
  enable,
  maxLength,
  options,
  ...props
}) => {
  const [value, setValue] = useState<string>(valueProp);

  useEffect(() => {
    setValue(valueProp);
  }, [valueProp]);

  const handleOnChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value: inputVal } = event.target;
    if (options?.numberOnly && !stringValidate(inputVal).isNumber()) {
      return;
    }
    setValue(inputVal);
  };

  const handleOnBlur = (event: React.FocusEvent<HTMLInputElement>) => {
    const { value: inputVal } = event.target;
    onChange(inputVal);
    onBlur(inputVal);
  };

  return (
    <TextBox
      id={id}
      label={label}
      disabled={!enable}
      value={value}
      onChange={handleOnChange}
      onBlur={handleOnBlur}
      name={name}
      maxLength={maxLength}
      error={
        enable && touched && invalid
          ? {
              message: errorProp,
              status: invalid
            }
          : undefined
      }
    />
  );
};

export default TextBoxControl;
