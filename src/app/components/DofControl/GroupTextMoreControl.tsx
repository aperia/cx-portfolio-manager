// Types
// Constant
import {
  GroupTextFormat as FormatEnum,
  GroupTextFormatType as FormatType
} from 'app/constants/enums';
// Helpers
import { formatCommon, formatContactPhone } from 'app/helpers';
// Components
import { Popover, TruncateText } from 'app/_libraries/_dls';
import { CommonControlProps } from 'app/_libraries/_dof/core';
import classNames from 'classnames';
import React, { useState } from 'react';

export interface GroupTextMoreControlProps extends CommonControlProps {
  options?: {
    format?: FormatEnum;
    fractionDigits?: number;
    formatType?: FormatType;
    zeroAsEmpty?: boolean;
    inline?: boolean;
    boldTextValue?: boolean;
    maximumItem?: number;
  };
}

const GroupTextMoreControl: React.FC<GroupTextMoreControlProps> = ({
  id,
  label,
  options = {},
  input: { value }
}) => {
  const {
    format = FormatEnum.Text,
    fractionDigits = 2,
    formatType = FormatType.Currency,
    zeroAsEmpty = true,
    inline = true,
    boldTextValue = false,
    maximumItem = 10
  } = options;

  const [maxHeight, setMaxHeight] = useState<number>(0);
  const [isOpen, setIsOpen] = useState(false);

  const handleTogglePopper = (value: boolean) => setIsOpen(value);

  const renderAmount = (val: string) => {
    let content = val;
    switch (formatType) {
      case FormatType.Currency:
        content = formatCommon(val).currency(fractionDigits)!;
        break;
      case FormatType.Quantity:
        content = formatCommon(val).quantity!;
        break;
      case FormatType.Percent:
        content = formatCommon(val).percent(fractionDigits)!;
        break;
    }

    return content;
  };

  const formatGeneration = (val: any) => {
    switch (format) {
      case FormatEnum.Date:
        return formatCommon(val).time.date;
      case FormatEnum.ShortMonthYear:
        return formatCommon(val).time.shortMonthYear;
      case FormatEnum.Amount:
        return renderAmount(val);
      case FormatEnum.PhoneNumber:
        return formatContactPhone(val);
      default:
        return val;
    }
  };

  const tooltipValues = [...(value || [])];
  const displayValue = tooltipValues.length > 0 && tooltipValues.shift();

  const textValue =
    displayValue || (!zeroAsEmpty && displayValue === 0)
      ? formatGeneration(displayValue)
      : '';

  const moreValuesElement = (
    <div
      style={
        tooltipValues.length > 10
          ? { maxHeight: `${maxHeight}px`, overflowY: 'scroll' }
          : undefined
      }
      className="mx-n16"
    >
      {tooltipValues.map((v, idx) => (
        <p
          ref={ref => {
            if (idx === 0) {
              setMaxHeight(
                (ref?.getBoundingClientRect()?.height || 0) * maximumItem
              );
            }
          }}
          key={idx}
          className={classNames(
            'px-16',
            idx < tooltipValues.length - 1 && 'pb-8'
          )}
        >
          {v}
        </p>
      ))}
    </div>
  );

  // just trigger to show tooltip, stop parents element to fire click event
  const handleClickAdditionInfo = (e: React.MouseEvent<HTMLSpanElement>) => {
    e.stopPropagation();
  };

  return (
    <div
      className={classNames('form-group-static', {
        'mt-4 d-flex align-items-center': inline
      })}
      id={id}
    >
      {inline ? (
        label && (
          <div id={`${id}__label`} className="color-grey text-nowrap mr-2">
            {label}
          </div>
        )
      ) : (
        <span id={`${id}__label`} className="form-group-static__label">
          <TruncateText resizable title={label}>
            {label}
          </TruncateText>
        </span>
      )}

      <div
        id={`${id}__text`}
        className={classNames('form-group-static__text flex-fill')}
      >
        {textValue && (
          <span className={classNames(boldTextValue && 'fw-600')}>
            {textValue}
          </span>
        )}

        {tooltipValues.length > 0 && (
          <Popover
            containerClassName="z-index-101"
            element={moreValuesElement}
            placement="top"
            size="auto"
            onVisibilityChange={handleTogglePopper}
          >
            <span
              className={classNames(
                'link text-decoration-none',
                isOpen && 'color-blue-d08'
              )}
              onClick={handleClickAdditionInfo}
            >
              &nbsp;+{tooltipValues.length}
            </span>
          </Popover>
        )}
      </div>
    </div>
  );
};

export default GroupTextMoreControl;
