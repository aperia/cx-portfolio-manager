import { ButtonActionType } from 'app/constants/button-action-types';
import * as FileUtils from 'app/helpers/fileUtilities';
import { actionsDMMTables } from 'pages/_commons/redux/DMMTables';
import { actionsWorkflow } from 'pages/_commons/redux/Workflow';
import {
  downloadDMMTable,
  editWorkflow,
  favoriteWorkflow,
  getButtonControlAction,
  openStrategyFlyout,
  openWorkflow,
  selectKpi,
  unFavoriteWorkflow
} from './actions';

jest.mock('app/_libraries/_dof/core/redux/createAppStore', () => ({
  __esModule: true,
  default: {
    dispatch: jest.fn(),
    getState: () => ({
      mapping: {
        workflowData: { managepenaltyfeesworkflow: 'managepenaltyfeesworkflow' }
      }
    })
  }
}));

jest.mock('pages/_commons/redux/Workflow', () => ({
  actionsWorkflow: {
    unfavoriteWorkflowTemplate: jest.fn(),
    favoriteWorkflowTemplate: jest.fn(),
    getTemplateWorkflows: jest.fn(),
    setWorkflowSetup: jest.fn(),
    getPageTemplateWorkflows: jest.fn()
  }
}));

jest.mock('pages/_commons/redux/DMMTables', () => ({
  actionsDMMTables: {
    downloadDecisionElement: jest.fn()
  }
}));

const downloadBlobFileMock = jest.fn();
jest
  .spyOn(FileUtils, 'downloadBlobFile')
  .mockImplementation(downloadBlobFileMock);

const actionsWorkflowMock: any = actionsWorkflow;
const actionsDMMTablesMock: any = actionsDMMTables;

describe('test action functions', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  it('function openWorkflow', () => {
    const mockDispatch = jest.fn();
    const workflowData = { id: '111', name: 'ManagePenaltyFeesWorkflow' };

    openWorkflow(workflowData, mockDispatch, {} as any);
    expect(mockDispatch).toHaveBeenCalledWith({
      payload: {
        isTemplate: true,
        workflow: workflowData
      },
      type: 'workflowSetup/setWorkflowSetup'
    });

    mockDispatch.mockClear();

    openWorkflow({}, mockDispatch, {} as any);
    expect(mockDispatch).not.toHaveBeenCalled();
  });

  it('function selectKpi', () => {
    const mockDispatch = jest.fn();
    const workflowData = {
      id: '111',
      name: 'ManagePenaltyFeesWorkflow',
      mode: 'Add'
    };

    selectKpi(workflowData, mockDispatch, {} as any);
    expect(mockDispatch).toHaveBeenCalledWith({
      payload: {
        isChooseOpen: false,
        value: workflowData,
        isAddOpen: true
      },
      type: 'portfolio/setOpenedChoosePopup'
    });

    mockDispatch.mockClear();

    selectKpi({}, mockDispatch, {} as any);
    expect(mockDispatch).toHaveBeenCalled();
  });

  it('function unFavoriteWorkflow success', async () => {
    const mockDispatch = jest.fn();
    actionsWorkflowMock.unfavoriteWorkflowTemplate = () => {
      actionsWorkflowMock.unfavoriteWorkflowTemplate = {
        fulfilled: {
          match: () => true
        }
      };
    };

    unFavoriteWorkflow('10000001', mockDispatch, {} as any);
    expect(mockDispatch).toHaveBeenCalled();
  });
  it('function unFavoriteWorkflow fail', async () => {
    const mockDispatch = jest.fn();
    actionsWorkflowMock.unfavoriteWorkflowTemplate = () => {
      actionsWorkflowMock.unfavoriteWorkflowTemplate = {
        fulfilled: {
          match: () => false
        }
      };
    };

    unFavoriteWorkflow('10000001', mockDispatch, {} as any);
    expect(mockDispatch).toHaveBeenCalled();
  });
  it('function favoriteWorkflow success', async () => {
    const mockDispatch = jest.fn();
    actionsWorkflowMock.favoriteWorkflowTemplate = () => {
      actionsWorkflowMock.favoriteWorkflowTemplate = {
        fulfilled: {
          match: () => true
        }
      };
    };

    favoriteWorkflow('10000001', mockDispatch, {} as any);
    expect(mockDispatch).toHaveBeenCalled();
  });
  it('function favoriteWorkflow fail', async () => {
    const mockDispatch = jest.fn();
    actionsWorkflowMock.favoriteWorkflowTemplate = () => {
      actionsWorkflowMock.favoriteWorkflowTemplate = {
        fulfilled: {
          match: () => false
        }
      };
    };

    favoriteWorkflow('10000001', mockDispatch, {} as any);
    expect(mockDispatch).toHaveBeenCalled();
  });

  it('function editWorkflow', () => {
    const mockDispatch = jest.fn();
    const workflowData = { id: '111', name: 'ManagePenaltyFeesWorkflow' };

    editWorkflow(workflowData, mockDispatch, {} as any);
    expect(mockDispatch).toHaveBeenCalledWith({
      payload: {
        isTemplate: false,
        workflow: workflowData
      },
      type: 'workflowSetup/setWorkflowSetup'
    });

    mockDispatch.mockClear();

    editWorkflow({}, mockDispatch, {} as any);
    expect(mockDispatch).not.toHaveBeenCalled();
  });

  describe('function downloadDMMTable', () => {
    it('disabled', async () => {
      const mockStopPropagation = jest.fn();
      await downloadDMMTable('disabled', jest.fn(), {
        stopPropagation: mockStopPropagation
      } as any);
      expect(mockStopPropagation).toHaveBeenCalled();
    });

    it('download success', async () => {
      actionsDMMTablesMock.downloadDecisionElement = () => {
        actionsDMMTablesMock.downloadDecisionElement = {
          fulfilled: {
            match: () => true
          }
        };
      };

      const mockStopPropagation = jest.fn();
      const mockDispatch = jest
        .fn()
        .mockImplementation(() => ({ payload: {} }));
      await downloadDMMTable('dmmTableId', mockDispatch, {
        stopPropagation: mockStopPropagation
      } as any);

      expect(mockStopPropagation).toHaveBeenCalled();
      expect(mockDispatch).toHaveBeenCalledTimes(1);
    });

    it('download success with filename', async () => {
      actionsDMMTablesMock.downloadDecisionElement = () => {
        actionsDMMTablesMock.downloadDecisionElement = {
          fulfilled: {
            match: () => true
          }
        };
      };

      const mockStopPropagation = jest.fn();
      const mockDispatch = jest
        .fn()
        .mockImplementation(() => ({ payload: { filename: 'filename.csv' } }));
      await downloadDMMTable('dmmTableId', mockDispatch, {
        stopPropagation: mockStopPropagation
      } as any);

      expect(mockStopPropagation).toHaveBeenCalled();
      expect(mockDispatch).toHaveBeenCalledTimes(1);
    });

    it('download fail', async () => {
      actionsDMMTablesMock.downloadDecisionElement = () => {
        actionsDMMTablesMock.downloadDecisionElement = {
          fulfilled: {
            match: () => false
          }
        };
      };

      const mockStopPropagation = jest.fn();
      const mockDispatch = jest
        .fn()
        .mockImplementation(() => ({ payload: {} }));
      await downloadDMMTable('dmmTableId', mockDispatch, {
        stopPropagation: mockStopPropagation
      } as any);
      expect(mockStopPropagation).toHaveBeenCalled();
      expect(mockDispatch).toHaveBeenCalledTimes(2);
    });
  });

  it('function openStrategyFlyout', () => {
    const mockDispatch = jest.fn();
    const strategyFlyoutData = { id: '111', isNewVersion: true };

    openStrategyFlyout(strategyFlyoutData, mockDispatch, {} as any);
    expect(mockDispatch).toHaveBeenCalledWith({
      payload: strategyFlyoutData,
      type: 'workflowSetup/setStrategyFlyout'
    });
  });

  it('function getButtonControlAction', () => {
    let actionName: keyof typeof ButtonActionType = 'openWorkflow';
    let result = getButtonControlAction(actionName);
    expect(result).toEqual(openWorkflow);

    actionName = 'unFavoriteWorkflow';
    result = getButtonControlAction(actionName);
    expect(result).toEqual(unFavoriteWorkflow);

    actionName = 'favoriteWorkflow';
    result = getButtonControlAction(actionName);
    expect(result).toEqual(favoriteWorkflow);

    actionName = 'editWorkflow';
    result = getButtonControlAction(actionName);
    expect(result).toEqual(editWorkflow);

    actionName = 'downloadDMMTable';
    result = getButtonControlAction(actionName);
    expect(result).toEqual(downloadDMMTable);

    actionName = 'openStrategyFlyout';
    result = getButtonControlAction(actionName);
    expect(result).toEqual(openStrategyFlyout);

    actionName = '' as any;
    result = getButtonControlAction(actionName);
    expect(result('Mock value', jest.fn(), {} as any)).toBeUndefined();
  });
});
