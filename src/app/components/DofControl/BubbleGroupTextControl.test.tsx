import { RenderResult } from '@testing-library/react';
import BubbleGroupTextControl, {
  BubbleGroupTextControlProps
} from 'app/components/DofControl/BubbleGroupTextControl';
import { getDefaultDOFMockProps, renderComponent } from 'app/utils';
import React from 'react';

const idDomTest = 'bubble-group-text-inline-control-id';
const mockProps = getDefaultDOFMockProps({ id: idDomTest });

describe('BubbleGroupTextControl', () => {
  let wrapper: RenderResult;

  const createWrapper = async (...props: BubbleGroupTextControlProps[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    wrapper = await renderComponent(
      idDomTest,
      <BubbleGroupTextControl {...mockProps} {...mergeProps} />
    );
  };

  const mockValue = (
    label: string,
    value: any,
    options?: {
      inline?: boolean;
      truncate?: boolean;
    }
  ) => {
    return {
      input: { ...mockProps.input, value },
      label,
      options
    } as BubbleGroupTextControlProps;
  };

  describe('Bubble group control test', () => {
    it('show label, no value', async () => {
      const testWithLabelProps = {
        id: idDomTest,
        label: 'Mock Label',
        input: undefined
      } as any;

      await createWrapper(testWithLabelProps);

      const { queryByText } = wrapper;

      expect(queryByText('Mock Label')).toBeInTheDocument();
    });

    it('show label, with value', async () => {
      await createWrapper(
        mockValue('Mock Label', 'Mock Value', { inline: true })
      );

      const { queryByText } = wrapper;

      expect(queryByText('Mock Label')).toBeInTheDocument();
      expect(queryByText('Mock Value')).toBeInTheDocument();
      expect(queryByText('Mock Value')!.parentElement!.className).toContain(
        'dls-truncate-text'
      );
    });

    it('show label, with value no truncate', async () => {
      await createWrapper(
        mockValue('Mock Label', 'Mock Value', {
          inline: false,
          truncate: false
        })
      );

      const { queryByText } = wrapper;

      expect(queryByText('Mock Label')).toBeInTheDocument();
      expect(queryByText('Mock Value')).toBeInTheDocument();
    });
  });
});
