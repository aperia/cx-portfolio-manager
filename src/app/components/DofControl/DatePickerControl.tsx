import { isValidDate } from 'app/helpers';
import { DatePickerChangeEvent, DatePickerProps } from 'app/_libraries/_dls';
import { CommonControlProps } from 'app/_libraries/_dof/core';
import isUndefined from 'lodash.isundefined';
import React from 'react';
import EnhanceDatePicker from '../EnhanceDatePicker';

export interface DatePickerControlProps
  extends CommonControlProps,
    Omit<DatePickerProps, 'id' | 'label'> {
  options?: {
    isMinToday?: boolean;
    isMaxToday: boolean;
    numberOverToday?: number;
    maxFromToday?: number;
  };
}

const DatePickerControl: React.FC<DatePickerControlProps> = ({
  input: { name, value, onChange, onBlur },
  meta: { error, valid, invalid, touched },
  id,
  label,
  enable,
  readOnly,
  options,
  required,
  ...props
}) => {
  const renderMinToDay = () => {
    const date = new Date();

    if (options?.numberOverToday) {
      return new Date(date.setDate(date.getDate() + options.numberOverToday));
    }
    if (options?.isMinToday) {
      return date;
    }

    return undefined;
  };

  const renderMaxToday = () => {
    const date = new Date();
    if (options?.isMaxToday) {
      return new Date();
    }

    if (options?.maxFromToday) {
      return new Date(date.setDate(date.getDate() + options.maxFromToday));
    }

    return undefined;
  };

  const handleOnChange = (event: DatePickerChangeEvent) => {
    const { value: dpValue } = event.target;
    onChange(isUndefined(dpValue) ? null : dpValue);
  };

  const handleOnBlur = (event: React.FocusEvent<Element>) => {
    const { value: dpValue } = event.target as any;
    onBlur(isUndefined(dpValue) ? null : dpValue);
  };

  return (
    <EnhanceDatePicker
      id={id}
      name={name}
      value={isValidDate(value) ? new Date(value) : undefined}
      minDate={renderMinToDay()}
      maxDate={renderMaxToday()}
      onChange={handleOnChange}
      onBlur={handleOnBlur}
      label={label}
      disabled={!enable}
      readOnly={readOnly}
      required={enable && !readOnly && required}
      error={
        required && touched && invalid
          ? {
              message: error,
              status: invalid
            }
          : undefined
      }
      popupBaseProps={{
        popupBaseClassName: 'inside-infobar'
      }}
    />
  );
};

export default DatePickerControl;
