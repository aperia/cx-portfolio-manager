// Helpers
import { convertStringToNumberOnly } from 'app/helpers';
import {
  conformToMask,
  MaskedTextBox,
  MaskedTextBoxProps
} from 'app/_libraries/_dls';
import { CommonControlProps } from 'app/_libraries/_dof/core';
import React, { useEffect, useState } from 'react';

export interface MaskPattern {
  isRegex?: boolean;
  pattern?: string;
}
export interface MaskedTextBoxControlProps
  extends CommonControlProps,
    Omit<MaskedTextBoxProps, 'id' | 'label' | 'data' | 'mask'> {
  mask?: Array<MaskPattern>;
}

const MaskedTextBoxControl: React.FC<MaskedTextBoxControlProps> = ({
  input: { name, value: valueProp, onChange, onBlur },
  meta: { error, valid, invalid, touched },
  id,
  placeholder,
  label,
  tooltip,
  readOnly,
  mask = [],
  required,
  enable,
  ...props
}) => {
  // custom masked pattern
  const maskedPattern: (string | RegExp)[] = mask.map(item => {
    try {
      // if the pattern is not regex, treat it as string
      if (!item.isRegex) return item.pattern!;
      const regexPattern = new RegExp(item.pattern || '');
      return regexPattern;
    } catch (e) {
      return item.pattern!;
    }
  });

  const [value, setValue] = useState<string>(valueProp);

  const handleOnChange = (event: any) => {
    const { value: inputVal } = event.target;
    setValue(convertStringToNumberOnly(inputVal));
  };

  const handleOnBlur = () => {
    onChange(value);
    onBlur(value);
  };

  const convertMaskedToRaw = (maskedValue: string) => {
    return conformToMask(maskedValue, maskedPattern).conformedValue as any;
  };

  useEffect(() => {
    setValue(valueProp);
  }, [valueProp]);

  return (
    <MaskedTextBox
      id={id}
      name={name}
      label={label}
      placeholder={placeholder}
      readOnly={readOnly}
      mask={maskedPattern}
      required={enable && required}
      value={value ? convertMaskedToRaw(value) : undefined}
      onChange={handleOnChange}
      onBlur={handleOnBlur}
      error={
        enable && touched && invalid
          ? {
              message: error,
              status: invalid
            }
          : undefined
      }
    />
  );
};

export default MaskedTextBoxControl;
