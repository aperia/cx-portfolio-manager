import { ButtonActionType } from 'app/constants/button-action-types';
import { classnames, isDevice } from 'app/helpers';
import { Button, ButtonProps, Popover, Tooltip } from 'app/_libraries/_dls';
import { CommonControlProps } from 'app/_libraries/_dof/core';
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { getButtonControlAction } from './actions';

export interface ButtonControlProps extends CommonControlProps {
  id: any;
  className?: string;
  label?: string;
  options?: {
    actionName?: keyof typeof ButtonActionType;
    buttonProps?: ButtonProps;
    tooltipText?: string;
    disabledTooltip?: string;
  };
}

const ButtonControl: React.FC<ButtonControlProps> = ({
  id,
  input,
  className,
  label,
  options = {}
}) => {
  const [tooltipOpened, setTooltipOpened] = useState(false);

  const dispatch = useDispatch();
  const { value } = input || {};

  if (!value) return <React.Fragment />;

  const {
    actionName,
    buttonProps,
    tooltipText = '',
    disabledTooltip = ''
  } = options;
  const isDisabled = value === 'disabled';
  const tooltip = isDisabled ? disabledTooltip : tooltipText;

  const action = getButtonControlAction(actionName);

  const handleClick = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    setTooltipOpened(opened => !opened);
    action && action(value, dispatch, e);
  };

  return (
    <div className={className} id={id}>
      <Tooltip
        element={tooltip}
        placement="top"
        opened={isDevice || !tooltip ? false : undefined}
      >
        <Popover
          placement="top-end"
          size="auto"
          element={tooltip}
          opened={isDevice && !!tooltip && tooltipOpened}
          onVisibilityChange={setTooltipOpened}
        >
          <Button
            id={`${id}__btn`}
            className={classnames(
              isDisabled && 'color-blue-l24 bg-transparent cursor-not-allowed'
            )}
            variant="outline-primary"
            size="sm"
            {...buttonProps}
            onClick={handleClick}
          >
            {label}
          </Button>
        </Popover>
      </Tooltip>
    </div>
  );
};

export default ButtonControl;
