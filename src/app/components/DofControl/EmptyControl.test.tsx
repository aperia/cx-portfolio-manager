import EmptyControl from 'app/components/DofControl/EmptyControl';
import { getDefaultDOFMockProps, renderComponent } from 'app/utils';
import React from 'react';

const testId = 'textbox-control';

describe('unit test TexBoxControl', () => {
  it('should render empty', async () => {
    const dataTest = getDefaultDOFMockProps({});
    const { getByTestId } = await renderComponent(
      testId,
      <EmptyControl {...dataTest} />
    );

    expect(getByTestId(testId)).toBeEmptyDOMElement();
  });
});
