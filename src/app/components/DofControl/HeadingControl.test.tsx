import { RenderResult } from '@testing-library/react';
import HeadingControl, {
  HeadingControlProps
} from 'app/components/DofControl/HeadingControl';
import { getDefaultDOFMockProps, renderComponent } from 'app/utils';
import React from 'react';

const idDomTest = 'heading-control-id';
const mockProps = getDefaultDOFMockProps({ id: idDomTest });

describe('HeadingControl', () => {
  let renderResult: RenderResult;
  const createWrapper = async (...props: HeadingControlProps[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    renderResult = await renderComponent(
      idDomTest,
      <HeadingControl {...mockProps} {...mergeProps} />
    );
  };

  it('render truncate + label', async () => {
    const testProps = {
      id: 'heading-test-id',
      label: 'heading-label',
      truncate: true
    } as any;
    await createWrapper(testProps);
    const { getByText } = renderResult;
    expect(getByText(/heading-label/i)).toBeTruthy();
  });

  it('render truncate without label', async () => {
    const testProps = {
      id: 'heading-test-id',
      truncate: true,
      tooltip: 'tooltip-test'
    } as any;
    await createWrapper(testProps);
    const { queryByText } = renderResult;
    expect(queryByText(/heading-label/i)).toBeFalsy();
  });

  it('render h3 tag', async () => {
    const props = {
      id: 'heading-test-id',
      label: 'heading-label',
      as: 'h3'
    } as any;
    await createWrapper(props);
    const { getByText } = renderResult;
    expect(getByText(/heading-label/i)).toBeTruthy();
  });

  it('render tooltip tag', async () => {
    const props = {
      id: 'heading-test-id',
      tooltip: 'tooltip-test'
    } as any;
    await createWrapper(props);
    const { queryByText } = renderResult;
    expect(queryByText(/heading-label/i)).toBeFalsy();
  });
});
