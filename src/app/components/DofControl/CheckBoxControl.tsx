import {
  CheckBox,
  CheckBoxProps,
  Tooltip,
  TooltipProps
} from 'app/_libraries/_dls';
import { CommonControlProps } from 'app/_libraries/_dof/core';
import React, { useState } from 'react';

export interface CheckBoxControlProps
  extends CommonControlProps,
    Omit<CheckBoxProps, 'id' | 'label' | 'children'> {
  tooltipProps?: TooltipProps;
  message?: string;
}

export const CheckBoxControl: React.FC<CheckBoxControlProps> = ({
  input: { name, value, onChange } = {},
  meta: { touched, error, valid, invalid },
  id,
  label,
  enable,
  readOnly,
  tooltipProps,
  message: elementProps = '',
  ...props
}) => {
  const [opened, setOpened] = useState<boolean>(false);
  const element = <p dangerouslySetInnerHTML={{ __html: elementProps }}></p>;

  const handleVisibilityChange = () => {
    setOpened(!opened);
  };

  return (
    <Tooltip
      opened={opened}
      element={element}
      variant="default"
      placement="top-start"
      triggerClassName="d-block"
      onVisibilityChange={handleVisibilityChange}
      {...tooltipProps}
    >
      <CheckBox>
        <CheckBox.Input
          id={id}
          name={name}
          readOnly={readOnly}
          disabled={!enable}
          checked={value}
          onChange={event => {
            onChange!(event.target.checked);
          }}
        />
        <CheckBox.Label>{label}</CheckBox.Label>
      </CheckBox>
    </Tooltip>
  );
};

export default CheckBoxControl;
