import {
  DropdownBaseChangeEvent,
  DropdownList,
  DropdownListProps
} from 'app/_libraries/_dls';
import { CommonControlProps } from 'app/_libraries/_dof/core';
import React, { useEffect, useState } from 'react';

export interface DropDownControlProps
  extends CommonControlProps,
    Omit<DropdownListProps, 'id' | 'label' | 'children'> {}

export const DropDownControl: React.FC<DropDownControlProps> = ({
  id,
  label,
  enable,
  data = [],
  input: { name, value: valueProp, onChange, onBlur },
  meta: { error, invalid, touched },
  readOnly,
  required,
  textField = 'fieldValue',
  ...props
}) => {
  const [value, setValue] = useState<MagicKeyValue>(valueProp);

  const handleOnChange = (event: DropdownBaseChangeEvent) => {
    const { value: ddValue } = event.target;
    setValue(ddValue);
  };

  const handleOnBlur = (event: React.FocusEvent<Element>) => {
    onChange(value);
    onBlur(value);
  };

  useEffect(() => {
    setValue(valueProp);
  }, [valueProp]);

  return (
    <DropdownList
      id={id}
      name={name}
      label={label}
      disabled={!enable}
      readOnly={readOnly}
      textField={textField}
      onChange={handleOnChange}
      onBlur={handleOnBlur}
      value={value}
      required={enable && required}
      popupBaseProps={{
        popupBaseClassName: 'inside-infobar'
      }}
      error={
        enable && required && touched && invalid
          ? {
              message: error,
              status: invalid
            }
          : undefined
      }
      {...props}
    >
      {data.map((item, index) => {
        return (
          <DropdownList.Item value={item} label={item[textField]} key={index} />
        );
      })}
    </DropdownList>
  );
};

export default DropDownControl;
