import { Bubble, TruncateText } from 'app/_libraries/_dls';
import { CommonControlProps } from 'app/_libraries/_dof/core';
import classNames from 'classnames';
import React, { useRef } from 'react';

export interface BubbleGroupTextControlProps extends CommonControlProps {
  id: any;
  label?: string;
  options?: {
    inline?: boolean;
    truncate?: boolean;
  };
}

const BubbleGroupTextControl: React.FC<BubbleGroupTextControlProps> = ({
  id,
  input,
  label,
  options = {}
}) => {
  const followRef = useRef<HTMLDivElement | null>(null);

  const { value } = input || {};
  const { inline = true, truncate = true } = options;

  return (
    <div
      className={classNames('form-group-static', {
        'mt-4 d-flex align-items-center': inline
      })}
      id={id}
    >
      {inline ? (
        label && (
          <div id={`${id}__label`} className="color-grey text-nowrap mr-2">
            {label}
          </div>
        )
      ) : (
        <span id={`${id}__label`} className="form-group-static__label">
          <TruncateText resizable title={label}>
            {label}
          </TruncateText>
        </span>
      )}

      <div
        id={`${id}__text`}
        className={classNames(
          'form-group-static__text flex-fill d-flex align-items-center'
        )}
      >
        {value && (
          <React.Fragment>
            <Bubble id={`${id}__value`} name={value} small />
            <div className="flex-fill ml-4" ref={followRef}>
              {truncate ? (
                <TruncateText resizable title={value} followRef={followRef}>
                  {value.toString()}
                </TruncateText>
              ) : (
                value
              )}
            </div>
          </React.Fragment>
        )}
      </div>
    </div>
  );
};

export default BubbleGroupTextControl;
