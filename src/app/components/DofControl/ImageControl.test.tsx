import { RenderResult } from '@testing-library/react';
import ImageControl, {
  ImageControlProps
} from 'app/components/DofControl/ImageControl';
import { getDefaultDOFMockProps, renderComponent } from 'app/utils';
import React from 'react';

const idDomTest = 'image-control-id';
const mockProps = getDefaultDOFMockProps({ id: idDomTest });

describe('ImageControl', () => {
  let renderResult: RenderResult;

  const createWrapper = async (...props: ImageControlProps[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    renderResult = await renderComponent(
      idDomTest,
      <ImageControl {...mockProps} {...mergeProps} />
    );
  };

  const mockValue = (value: any) => {
    return {
      input: { ...mockProps.input, value }
    } as ImageControlProps;
  };

  it('should render without error', async () => {
    await createWrapper(mockValue('value'));
    const { getByRole } = renderResult;
    expect(getByRole('img')).toBeInTheDocument();
  });

  it('should not show image when value is empty', async () => {
    await createWrapper(mockValue(''));
    const { queryByRole } = renderResult;
    expect(queryByRole('img')).not.toBeInTheDocument();
  });

  it('should replace image url start with "/"', async () => {
    await createWrapper(mockValue('/images/logo.svg'));
    const { getByRole } = renderResult;
    expect(getByRole('img')).toBeInTheDocument();
  });
});
