import { Icon, IconProps } from 'app/_libraries/_dls';
import { CommonControlProps } from 'app/_libraries/_dof/core';
import React from 'react';

export interface IconWithValControlProps extends CommonControlProps {
  options?: { iconProps?: IconProps };
}

const IconWithValControl: React.FC<IconWithValControlProps> = ({
  input: { value },
  id,
  options = {}
}) => {
  const { iconProps } = options;

  return (
    <div id={id} className="d-flex align-items-center">
      {iconProps && <Icon id={`${id}__icon`} size="4x" {...iconProps} />}
      <span id={`${id}__value`}>{value}</span>
    </div>
  );
};

export default IconWithValControl;
