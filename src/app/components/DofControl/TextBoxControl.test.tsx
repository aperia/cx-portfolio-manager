import { fireEvent, RenderResult } from '@testing-library/react';
import TextBoxControl, {
  TextBoxControlProps
} from 'app/components/DofControl/TextBoxControl';
import { getDefaultDOFMockProps, renderComponent } from 'app/utils';
import React from 'react';

const idDomTest = 'test';
const defaultProps = getDefaultDOFMockProps();

describe('unit test TexBoxControl', () => {
  let renderResult: RenderResult;
  const createWrapper = async (...props: TextBoxControlProps[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    renderResult = await renderComponent(
      idDomTest,
      <TextBoxControl {...defaultProps} {...mergeProps} />
    );
  };

  it('should have valueProp', async () => {
    const onBlurFunc = jest.fn();
    const onChangeFunc = jest.fn();
    const dataTest = getDefaultDOFMockProps({
      value: 'john',
      onBlur: onBlurFunc,
      onChange: onChangeFunc
    });
    await createWrapper(dataTest);
    const { getByDisplayValue } = renderResult;
    expect(getByDisplayValue(/john/i)).toBeInTheDocument();
    fireEvent.blur(getByDisplayValue(/john/i));
    expect(onBlurFunc).toBeCalled();
    fireEvent.change(getByDisplayValue(/john/i), {
      target: { value: 'smith' }
    });
    expect(onChangeFunc).toBeCalled();
    expect(getByDisplayValue(/smith/i)).toBeInTheDocument();
  });

  it('should have option.numberOnly', async () => {
    const onChangeFunc = jest.fn();
    const dataTest = getDefaultDOFMockProps({
      value: 'john',
      options: {
        numberOnly: true
      },
      onChange: onChangeFunc
    });

    await createWrapper(dataTest);
    const { getByDisplayValue } = renderResult;
    expect(getByDisplayValue(/john/i)).toBeInTheDocument();
    fireEvent.change(getByDisplayValue(/john/i), {
      target: { value: 'smith' }
    });
    expect(getByDisplayValue(/john/i)).toBeInTheDocument();
  });

  it('should have error', async () => {
    const dataTest = getDefaultDOFMockProps({
      value: 'john',
      meta: {
        invalid: true,
        touched: true
      },
      required: true
    });

    await createWrapper(dataTest);
    const { getByDisplayValue } = renderResult;
    expect(getByDisplayValue(/john/i)).toBeInTheDocument();
  });
});
