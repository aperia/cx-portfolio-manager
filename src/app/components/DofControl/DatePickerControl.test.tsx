import { fireEvent } from '@testing-library/react';
import DatePickerControl from 'app/components/DofControl/DatePickerControl';
import { getDefaultDOFMockProps, renderComponent } from 'app/utils';
import { DatePickerChangeEvent, DatePickerProps } from 'app/_libraries/_dls';
import React from 'react';

jest.mock('app/_libraries/_dls/components/DatePicker', () => {
  return {
    __esModule: true,
    default: ({
      id,
      value,
      minDate,
      maxDate,
      onChange,
      onBlur,
      label,
      error
    }: DatePickerProps) => {
      return (
        <div id={id}>
          <label htmlFor="value">
            {label}
            {error && <i className={'icon icon-error'}></i>}
          </label>
          <input
            type="text"
            name="value"
            value={value?.getUTCDate()}
            onBlur={onBlur}
          />
          <button
            onClick={() =>
              onChange!({
                target: {
                  value: new Date()
                }
              } as DatePickerChangeEvent)
            }
          >
            Change value
          </button>
          <button
            onClick={() =>
              onChange!({
                target: {
                  value: undefined
                }
              } as DatePickerChangeEvent)
            }
          >
            Change no value
          </button>
          <button
            onClick={() =>
              onBlur!({
                target: {
                  value: new Date()
                }
              } as any)
            }
          >
            Blur value
          </button>
          <button
            onClick={() =>
              onBlur!({
                target: {
                  value: undefined
                }
              } as any)
            }
          >
            Blur no value
          </button>
          <p>Min date {minDate?.getUTCDate() || 'not set'}</p>
          <p>Max date {maxDate?.getUTCDate() || 'not set'}</p>
        </div>
      );
    }
  };
});

const testId = 'date-picker-control';

describe('Test DatePickerControl', () => {
  it('test has numberOverToday option', async () => {
    const current = new Date();
    const valueData = getDefaultDOFMockProps({
      options: { numberOverToday: 4 },
      value: current.toISOString()
    });

    const { queryByText } = await renderComponent(
      testId,
      <DatePickerControl {...valueData} label={'Mock label'} />
    );
    const date = new Date(current);
    const result = new Date(date.setDate(date.getDate() + 4));
    const input = queryByText(`Mock label`)!.parentElement?.querySelector(
      'input'
    );

    expect(queryByText(`Mock label`)).toBeInTheDocument();
    expect(input?.value).toEqual(current.getUTCDate().toString());
    expect(queryByText(`Min date ${result.getUTCDate()}`)).toBeInTheDocument();
  });

  it('test has isMinToday option', async () => {
    const valueData = getDefaultDOFMockProps({
      options: { isMinToday: true }
    });

    const { queryByText } = await renderComponent(
      testId,
      <DatePickerControl {...valueData} />
    );

    const date = new Date();

    expect(queryByText(`Min date ${date.getUTCDate()}`)).toBeInTheDocument();
  });

  it('test has undefined option', async () => {
    const valueData = getDefaultDOFMockProps();

    const { queryByText } = await renderComponent(
      testId,
      <DatePickerControl {...valueData} />
    );

    expect(queryByText(`Min date not set`)).toBeInTheDocument();
  });

  it('test has  option isMaxToday', async () => {
    const valueData = getDefaultDOFMockProps({
      options: { isMaxToday: true }
    });

    const date = new Date();
    const { queryByText } = await renderComponent(
      testId,
      <DatePickerControl {...valueData} />
    );

    expect(queryByText(`Max date ${date.getUTCDate()}`)).toBeInTheDocument();
  });

  it('test has  option maxFromToday', async () => {
    const valueData = getDefaultDOFMockProps({
      options: { maxFromToday: 5 }
    });

    const { queryByText } = await renderComponent(
      testId,
      <DatePickerControl {...valueData} />
    );

    const date = new Date();
    const result = new Date(date.setDate(date.getDate() + 5));

    expect(queryByText(`Max date ${result.getUTCDate()}`)).toBeInTheDocument();
  });

  it('test onChange with value', async () => {
    const onChange = jest.fn();
    const valueData = getDefaultDOFMockProps({
      onChange
    });
    const { queryByText } = await renderComponent(
      testId,
      <DatePickerControl {...valueData} label={'Mock label'} />
    );

    const button = queryByText(`Change value`);

    fireEvent.click(button!);

    expect(onChange).toBeCalled();
  });

  it('test onChange with undefined value', async () => {
    const onChange = jest.fn();
    const valueData = getDefaultDOFMockProps({
      onChange
    });
    const { queryByText } = await renderComponent(
      testId,
      <DatePickerControl {...valueData} label={'Mock label'} />
    );

    const button = queryByText(`Change no value`);

    fireEvent.click(button!);

    expect(onChange).toBeCalled();
  });

  it('test onBlur with value', async () => {
    const onBlur = jest.fn();
    const valueData = getDefaultDOFMockProps({
      onBlur
    });
    const { queryByText } = await renderComponent(
      testId,
      <DatePickerControl {...valueData} label={'Mock label'} />
    );

    const button = queryByText(`Blur value`);

    fireEvent.click(button!);

    expect(onBlur).toBeCalled();
  });

  it('test onChange with undefined value', async () => {
    const onBlur = jest.fn();
    const valueData = getDefaultDOFMockProps({
      onBlur
    });
    const { queryByText } = await renderComponent(
      testId,
      <DatePickerControl {...valueData} label={'Mock label'} />
    );

    const button = queryByText(`Blur no value`);

    fireEvent.click(button!);

    expect(onBlur).toBeCalled();
  });

  it('test error', async () => {
    const valueData = getDefaultDOFMockProps({
      meta: {
        invalid: true,
        touched: true
      },
      required: true
    });

    const { queryByText } = await renderComponent(
      testId,
      <DatePickerControl {...valueData} label={'Mock label'} />
    );

    expect(
      queryByText('Mock label')?.querySelector('i.icon-error')
    ).toBeInTheDocument();
  });
});
