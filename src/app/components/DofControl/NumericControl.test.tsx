import { fireEvent, RenderResult } from '@testing-library/react';
import NumericControl, {
  NumericControlProps
} from 'app/components/DofControl/NumericControl';
import { getDefaultDOFMockProps, renderComponent } from 'app/utils';
import React from 'react';

const idDomTest = 'test';
const defaultProps = getDefaultDOFMockProps();

describe('Test NumericTextbox', () => {
  let renderResult: RenderResult;
  const createWrapper = async (...props: NumericControlProps[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    renderResult = await renderComponent(
      idDomTest,
      <NumericControl {...defaultProps} {...mergeProps} />
    );
  };

  it('test onblur', async () => {
    const onBlurFunc = jest.fn();
    const valueData = getDefaultDOFMockProps({
      value: '10',
      onBlur: onBlurFunc
    });
    await createWrapper(valueData);
    const { getByDisplayValue } = renderResult;
    expect(getByDisplayValue(/10.00/i)).toBeInTheDocument();
    fireEvent.blur(getByDisplayValue(/10/i), { target: { value: '20' } });
    expect(onBlurFunc).toBeCalled();
  });

  it('test onchange', async () => {
    const valueData = getDefaultDOFMockProps({ value: '10' });
    await createWrapper(valueData);
    const { getByDisplayValue } = renderResult;
    expect(getByDisplayValue(/10.00/i)).toBeInTheDocument();
    fireEvent.change(getByDisplayValue(/10/i), {
      target: { value: '20' }
    });
    expect(getByDisplayValue(/20.00/i)).toBeInTheDocument();
  });

  it('test error on field', async () => {
    const valueData = getDefaultDOFMockProps({
      required: true,
      meta: { touched: true, invalid: true },
      value: '10'
    });
    await createWrapper(valueData);
    const { getByDisplayValue } = renderResult;
    expect(getByDisplayValue(/10.00/i)).toBeInTheDocument();
  });
});
