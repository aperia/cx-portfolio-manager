import { formatCommon } from 'app/helpers';
import { TextArea, TextAreaProps } from 'app/_libraries/_dls';
import { CommonControlProps } from 'app/_libraries/_dof/core';
import React, { useEffect, useState } from 'react';

// helper func
const formatCharacterLeft = (restChar: number) => {
  if (restChar === 1) return `${restChar} character left`;
  return `${formatCommon(restChar).quantity} characters left`;
};

export interface TextAreaControlProps
  extends CommonControlProps,
    Omit<TextAreaProps, 'id' | 'label'> {
  options?: {
    countDown?: boolean;
    maxLength?: number;
    className?: string;
  };
}

export const TextAreaControl: React.FC<TextAreaControlProps> = ({
  input: { name, value: valueProp, onChange, onBlur },
  meta: { error: errorProp, valid, invalid, touched },
  id,
  enable,
  placeholder,
  options,
  ...props
}) => {
  const [value, setValue] = useState<string>(valueProp);

  useEffect(() => {
    setValue(valueProp || '');
  }, [valueProp]);

  const handleOnChange = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    const { value: inputVal } = event.target;
    setValue(inputVal);
  };

  const handleOnBlur = (event: React.FocusEvent<HTMLTextAreaElement>) => {
    const { value: inputVal } = event.target;
    onChange(inputVal);
    onBlur(inputVal);
  };

  const restCharacter =
    options?.maxLength && options?.countDown
      ? options?.maxLength - value.length
      : undefined;

  return (
    <>
      <TextArea
        id={id}
        disabled={!enable}
        value={value}
        placeholder={placeholder}
        className={options?.className}
        onChange={handleOnChange}
        onBlur={handleOnBlur}
        name={name}
        maxLength={options?.maxLength}
      />
      {restCharacter !== undefined && (
        <p className="color-grey-l24 fs-12 mt-8">
          {formatCharacterLeft(restCharacter)}
        </p>
      )}
    </>
  );
};

export default TextAreaControl;
