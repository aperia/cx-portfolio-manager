import { fireEvent } from '@testing-library/react';
import DropDownControl from 'app/components/DofControl/DropDownControl';
import { getDefaultDOFMockProps, renderComponent } from 'app/utils';
import 'app/utils/_mockComponent/mockCanvas';
import React from 'react';

const mockProps = getDefaultDOFMockProps();

const testId = 'dropdown-control';

describe('DropDownControl', () => {
  const data = [
    {
      id: 1,
      value: 'value-item-1'
    },
    { id: 2, value: 'value-item-2' }
  ];

  it('render without error when data is undefined and textField is undefined', async () => {
    const testProps = {
      id: 'combobox-test-id',
      label: 'Mock label',
      textField: 'value'
    };

    const { queryByText } = await renderComponent(
      testId,
      <DropDownControl
        {...mockProps}
        label={testProps.label}
        id={testProps.id}
      />
    );

    expect(queryByText('Mock label'));
  });

  it('should have error when required property is true and invalid is true', async () => {
    const testProps = {
      id: 'combobox-test-id',
      label: 'Mock label',
      textField: 'value'
    };
    const cbbMockProps = getDefaultDOFMockProps({
      meta: {
        invalid: true,
        touched: true
      }
    });

    const { queryByText } = await renderComponent(
      testId,
      <DropDownControl
        {...cbbMockProps}
        label={testProps.label}
        id={testProps.id}
        required
        enable
      />
    );

    expect(queryByText('Mock label')).toBeInTheDocument();
    expect(
      queryByText('Mock label')?.parentElement?.querySelector('.icon-error')
    ).toBeInTheDocument();
  });

  it('render with default value', async () => {
    const testProps = {
      id: 'combobox-test-id',
      label: 'Mock label',
      textField: 'value'
    };

    const { queryByText } = await renderComponent(
      testId,
      <DropDownControl
        {...mockProps}
        data={data as []}
        textField={testProps.textField}
        label={testProps.label}
        id={testProps.id}
        value={data[0]}
      />
    );

    expect(queryByText('Mock label')).toBeInTheDocument();

    const inputDiv = queryByText(data[0].value);

    expect(inputDiv).toBeInTheDocument();
  });

  it('should render second value when onchange event to this item is called ', async () => {
    const testProps = {
      id: 'combobox-test-id',
      label: 'Mock label',
      textField: 'value'
    };

    const { queryByText, getByTestId } = await renderComponent(
      testId,
      <DropDownControl
        {...mockProps}
        data={data as []}
        textField={testProps.textField}
        label={testProps.label}
        id={testProps.id}
        placeholder="Select an item"
        opened
      />
    );

    expect(queryByText('Mock label')).toBeInTheDocument();
    const item = queryByText(data[1].value);
    fireEvent.click(item!);
    expect(
      getByTestId(testId)
        .querySelector('.input')
        ?.querySelector('.dls-ellipsis')?.innerHTML
    ).toEqual(data[1].value);
  });

  it('should still render current value when onblur event is called', async () => {
    const onChangeFn = jest.fn(),
      onBlurFn = jest.fn();
    const testProps = {
      id: 'combobox-test-id',
      label: 'Mock label',
      textField: 'value'
    };

    const { queryByText } = await renderComponent(
      testId,
      <DropDownControl
        {...mockProps}
        data={data as []}
        input={{ value: '', onChange: onChangeFn, onBlur: onBlurFn }}
        textField={testProps.textField}
        label={testProps.label}
        id={testProps.id}
        placeholder="Select an item"
      />
    );

    expect(queryByText('Mock label')).toBeInTheDocument();

    const input = queryByText('Select an item');

    fireEvent.blur(input!.parentElement!);

    expect(onChangeFn).toBeCalled();
    expect(onBlurFn).toBeCalled();
  });
});
