import DividerControl from 'app/components/DofControl/DividerControl';
import { getDefaultDOFMockProps, renderComponent } from 'app/utils';
import React from 'react';

const mockProps = getDefaultDOFMockProps();

const testId = 'divider-control';

describe('Test Divider Control', () => {
  it('render to UI', async () => {
    const { getByTestId } = await renderComponent(
      testId,
      <DividerControl {...mockProps} />
    );
    expect(getByTestId(testId).querySelector('div')?.className).toContain(
      'divider-dashed'
    );
  });
});
