import React from 'react';
import IconControl, { IconControlProps } from './IconControl';

export interface WorkflowCardIconControlProps extends IconControlProps {}

const WorkflowCardIconControl: React.FC<WorkflowCardIconControlProps> = ({
  input,
  options = {},
  ...rest
}) => {
  const { value } = input;

  if (!value) return <React.Fragment />;

  const { id: workflowId, actionName, iconProps, tooltipProps } = value;
  const _options = {
    ...options,
    actionName,
    iconProps: { ...options.iconProps, ...iconProps },
    tooltipProps: { ...options.tooltipProps, ...tooltipProps }
  };

  if (_options.iconProps.name === 'lock') {
    rest.className = '';
  }

  return (
    <IconControl
      input={{ ...input, value: workflowId }}
      options={_options}
      {...rest}
    />
  );
};

export default WorkflowCardIconControl;
