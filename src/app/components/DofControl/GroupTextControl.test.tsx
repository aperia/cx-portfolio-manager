import { RenderResult } from '@testing-library/react';
import GroupTextControl, {
  GroupTextControlProps
} from 'app/components/DofControl/GroupTextControl';
import { GroupTextFormat, GroupTextFormatType } from 'app/constants/enums';
import * as helper from 'app/helpers/isDevice';
import { getDefaultDOFMockProps, renderComponent } from 'app/utils';
import { mockClientWidth } from 'app/_libraries/_dls/test-utils/mocks/defineProperty';
import * as getElementAttribute from 'app/_libraries/_dls/utils/getElementAttribute';
import React from 'react';
import { HashRouter } from 'react-router-dom';

const idDomTest = 'group-text-inline-control-id';
const mockProps = getDefaultDOFMockProps({ id: idDomTest });

let spyGetElementAttribute: jest.SpyInstance;

beforeEach(() => {
  spyGetElementAttribute = jest
    .spyOn(getElementAttribute, 'getAvailableWidth')
    .mockImplementation(() => 1205);
});

afterEach(() => {
  spyGetElementAttribute?.mockReset();
  spyGetElementAttribute?.mockRestore();
});

jest.mock('app/helpers/isDevice', () => ({
  __esModule: true,
  isDevice: null
}));

const mockHelper: any = helper;

describe('GroupTextControl', () => {
  let wrapper: RenderResult;

  const createWrapper = async (...props: GroupTextControlProps[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    wrapper = await renderComponent(
      idDomTest,
      <GroupTextControl {...mockProps} {...mergeProps} />
    );
  };

  const createWrapperWithRouter = async (...props: GroupTextControlProps[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    wrapper = await renderComponent(
      idDomTest,
      <HashRouter>
        <GroupTextControl {...mockProps} {...mergeProps} />
      </HashRouter>
    );
  };

  const mockValue = (
    value: any,
    options?: {
      format?: GroupTextFormat;
      fractionDigits?: number;
      formatType?: GroupTextFormatType;
      zeroAsEmpty?: boolean;
      inline?: boolean;
      truncate?: boolean;
      boldTextValue?: boolean;
      isCamel?: boolean;
      inlineFullText?: boolean;
      link?: string;
      linkDataField?: string;
      linkArgs?: string[];
    }
  ) => {
    return {
      input: { ...mockProps.input, value },
      options
    } as GroupTextControlProps;
  };

  describe('with label', () => {
    const testWithLabelProps = {
      label: 'Mock Label'
    } as GroupTextControlProps;

    beforeEach(() => {
      jest.resetModules();
    });

    it('should show empty value when value = ""', async () => {
      await createWrapper({
        ...testWithLabelProps,
        options: { ...testWithLabelProps?.options, inline: false }
      });

      const { queryByText } = wrapper;

      expect(queryByText('Mock Label')).toBeInTheDocument();
    });

    it('should show as text when format = "text"', async () => {
      await createWrapper(
        testWithLabelProps,
        mockValue('accountManagement', { isCamel: true })
      );
      const { queryByText } = wrapper;
      expect(queryByText('Mock Label')).toBeInTheDocument();
      expect(queryByText('Account Management')).toBeInTheDocument();
    });

    it('should show as text when format = "text" and truncated', async () => {
      spyGetElementAttribute = jest
        .spyOn(getElementAttribute, 'getAvailableWidth')
        .mockImplementation(() => 300);

      spyGetElementAttribute = jest
        .spyOn(getElementAttribute, 'getPadding')
        .mockImplementation(() => ({
          top: 0,
          left: 0,
          right: 0,
          bottom: 0
        }));

      spyGetElementAttribute = jest
        .spyOn(getElementAttribute, 'getMargin')
        .mockImplementation(() => ({
          top: 0,
          left: 0,
          right: 0,
          bottom: 0
        }));

      mockClientWidth(300);
      const handleTruncate = jest.fn();
      await createWrapper(
        testWithLabelProps,
        mockValue(
          'The workflow will be deployed on a Fiserv issuer platform by a change.\nYou can configure a new change to deploy this workflow or you can add the workflow to an existing change.\nTo use an existing change, select a change that is not yet scheduled or approved and that only has other workflows of the Account Management type associated with it.',
          {
            truncate: true,
            lineTruncate: 1,
            resizable: false,
            handleTruncateProp: handleTruncate,
            ellipsisMoreText: 'More',
            ellipsisLessText: 'Less'
          }
        )
      );
      const { queryByText } = wrapper;
      expect(queryByText('Mock Label')).toBeInTheDocument();
    });

    it('should show as text when format = "text" and no truncate', async () => {
      await createWrapper(
        testWithLabelProps,
        mockValue('Mock value', { truncate: false })
      );
      const { queryByText } = wrapper;
      expect(queryByText('Mock Label')).toBeInTheDocument();
      expect(queryByText('Mock value')).toBeInTheDocument();
      expect(queryByText('Mock value')!.parentElement?.className).not.toContain(
        'dls-truncate-text'
      );
    });

    it('should show as Heading text when format = "Heading"', async () => {
      await createWrapper(
        testWithLabelProps,
        mockValue('Mock value', {
          boldTextValue: true,
          format: GroupTextFormat.Heading
        })
      );
      const { queryByText } = wrapper;
      expect(queryByText('Mock Label')).toBeInTheDocument();
      expect(queryByText('Mock value')!.parentElement?.className).toContain(
        'dls-truncate-text'
      );
    });

    it('should show as date format when format = "date"', async () => {
      const dateValue = new Date('2020-12-25T10:00:00.000');
      await createWrapper(
        testWithLabelProps,
        mockValue(dateValue, { format: GroupTextFormat.Date })
      );
      const { queryByText } = wrapper;

      expect(queryByText('Mock Label')).toBeInTheDocument();
      expect(queryByText('12/25/2020')).toBeInTheDocument();
    });

    it('should show as shortMonthYear format when format = "shortMonthYear"', async () => {
      const dateValue = new Date('2020-12-25T10:00:00.000');
      await createWrapper(
        testWithLabelProps,
        mockValue(dateValue, { format: GroupTextFormat.ShortMonthYear })
      );

      const { queryByText } = wrapper;

      expect(queryByText('Mock Label')).toBeInTheDocument();
      expect(queryByText('12/2020')).toBeInTheDocument();
    });

    it('should show as shortMonthYear format when format = "dateUTC"', async () => {
      const dateValue = new Date('2020-12-25T11:30:01.000');
      await createWrapper(
        testWithLabelProps,
        mockValue(dateValue, { format: GroupTextFormat.DateUTC })
      );

      const { queryByText } = wrapper;

      expect(queryByText('Mock Label')).toBeInTheDocument();
      expect(queryByText('12/25/2020')).toBeInTheDocument();
    });

    it('should show as shortMonthYear format when format = "allDatetime"', async () => {
      const dateValue = new Date('2020-12-25T11:30:01.000');
      await createWrapper(
        testWithLabelProps,
        mockValue(dateValue, { format: GroupTextFormat.AllDatetime })
      );

      const { queryByText } = wrapper;

      expect(queryByText('Mock Label')).toBeInTheDocument();
      expect(queryByText('12/25/2020 11:30:01 AM')).toBeInTheDocument();
    });

    it('should show as phone number format when format = "phoneNumber"', async () => {
      await createWrapper(
        testWithLabelProps,
        mockValue('0123456789', { format: GroupTextFormat.PhoneNumber })
      );

      const { queryByText } = wrapper;

      expect(queryByText('Mock Label')).toBeInTheDocument();
      expect(queryByText('(012) 345-6789')).toBeInTheDocument();
    });

    it('should show as currency format when format = "amount" and format type = "currency"', async () => {
      await createWrapper(
        testWithLabelProps,
        mockValue(1234, { format: GroupTextFormat.Amount })
      );

      const { queryByText } = wrapper;

      expect(queryByText('Mock Label')).toBeInTheDocument();
      expect(queryByText('$1,234.00')).toBeInTheDocument();
    });

    it('should show as currency format with 3 fraction digits when format = "amount", format type = "currency" and fractionDigits = 3', async () => {
      await createWrapper(
        testWithLabelProps,
        mockValue(1234.123, {
          format: GroupTextFormat.Amount,
          fractionDigits: 3
        })
      );

      const { queryByText } = wrapper;

      expect(queryByText('Mock Label')).toBeInTheDocument();
      expect(queryByText('$1,234.123')).toBeInTheDocument();
    });

    it('should show as quantity format when format = "amount" and format type = "quantity"', async () => {
      await createWrapper(
        testWithLabelProps,
        mockValue(1234, {
          format: GroupTextFormat.Amount,
          formatType: GroupTextFormatType.Quantity
        })
      );

      const { queryByText } = wrapper;

      expect(queryByText('Mock Label')).toBeInTheDocument();
      expect(queryByText('1,234')).toBeInTheDocument();
    });

    it('should show as percent format when format = "amount" and format type = "percent"', async () => {
      await createWrapper(
        testWithLabelProps,
        mockValue(10, {
          format: GroupTextFormat.Amount,
          formatType: GroupTextFormatType.Percent
        })
      );

      const { queryByText } = wrapper;

      expect(queryByText('Mock Label')).toBeInTheDocument();
      expect(queryByText('10.00%')).toBeInTheDocument();
    });

    it('should show 0.00% when value = 0, zeroAsEmpty = false, format = "amount" and format type = "percent"', async () => {
      await createWrapper(
        testWithLabelProps,
        mockValue(0, {
          format: GroupTextFormat.Amount,
          formatType: GroupTextFormatType.Percent,
          zeroAsEmpty: false
        })
      );

      const { queryByText } = wrapper;

      expect(queryByText('Mock Label')).toBeInTheDocument();
      expect(queryByText('0.00%')).toBeInTheDocument();
    });

    it('should show as empty when value = 0, format = "amount" and format type = "percent"', async () => {
      await createWrapper(
        testWithLabelProps,
        mockValue(0, {
          format: GroupTextFormat.Amount,
          formatType: GroupTextFormatType.Percent,
          inline: false
        })
      );

      const { queryByText } = wrapper;

      expect(queryByText('Mock Label')).toBeInTheDocument();
      expect(queryByText('0.00%')).toBeInTheDocument();
    });

    it('should show as methodType format when format = "methodType"', async () => {
      await createWrapper(
        testWithLabelProps,
        mockValue('updated', { format: GroupTextFormat.MethodType })
      );

      const { queryByText } = wrapper;

      expect(queryByText('Mock Label')).toBeInTheDocument();
      expect(queryByText('UPDATED')).toBeInTheDocument();
    });

    it('should show as methodType format when format = "dateRange"', async () => {
      await createWrapper(
        testWithLabelProps,
        mockValue(
          { start: new Date('1/1/2020'), end: new Date('2/2/2020') },
          { format: GroupTextFormat.DateRange }
        )
      );

      const { queryByText } = wrapper;

      expect(queryByText('Mock Label')).toBeInTheDocument();
      expect(queryByText('01/01/2020 - 02/02/2020')).toBeInTheDocument();
    });

    it('should show as methodType format when format = "dateRange" with start is null', async () => {
      await createWrapper(
        testWithLabelProps,
        mockValue(
          { start: null, end: new Date('2/2/2020') },
          { format: GroupTextFormat.DateRange }
        )
      );

      const { queryByText } = wrapper;

      expect(queryByText('Mock Label')).toBeInTheDocument();
      expect(queryByText('- 02/02/2020')).toBeInTheDocument();
    });

    it('should show value with tooltip & isDevice true & format TaxpayerId', async () => {
      mockHelper.isDevice = true;
      await createWrapper(
        testWithLabelProps,
        mockValue('mock with tooltip', { format: GroupTextFormat.TaxpayerId }),
        {
          tooltip: 'mock tooltip'
        } as any
      );

      const { queryByText } = wrapper;

      expect(queryByText('Mock Label')).toBeInTheDocument();
      expect(queryByText('mock with tooltip')).toBeInTheDocument();
    });

    it('should show value with tooltip & isDevice true', async () => {
      mockHelper.isDevice = true;
      await createWrapper(
        testWithLabelProps,
        mockValue('mock with tooltip', { format: GroupTextFormat.Text }),
        {
          tooltip: 'mock tooltip'
        } as any
      );

      const { queryByText } = wrapper;

      expect(queryByText('Mock Label')).toBeInTheDocument();
      expect(queryByText('mock with tooltip')).toBeInTheDocument();
    });

    it('should show value with tooltip & isDevice false', async () => {
      mockHelper.isDevice = false;
      await createWrapper(
        testWithLabelProps,
        mockValue('mockValue', {
          format: GroupTextFormat.Text,
          isCamel: true
        }),
        {
          tooltip: 'mock tooltip'
        } as any
      );

      const { queryByText } = wrapper;

      expect(queryByText('Mock Label')).toBeInTheDocument();
      expect(queryByText('Mock Value')).toBeInTheDocument();
    });

    it('should show fulltext inline value', async () => {
      await createWrapper(
        testWithLabelProps,
        mockValue('mockValue', {
          truncate: false,
          inlineFullText: true
        })
      );

      const { queryByText } = wrapper;
      expect(queryByText('Mock Label')).toBeInTheDocument();
      expect(queryByText('mockValue')).toBeInTheDocument();
    });

    it('should show group text control with link & args', async () => {
      const testWithLabelAndValueProps = {
        label: 'Mock Label',
        input: {
          value: {
            id: '111',
            name: 'Mock Name'
          }
        },
        options: {
          link: '/change-management/detail/{0}',
          linkArgs: ['id'],
          linkDataField: 'name'
        }
      } as GroupTextControlProps;
      await createWrapperWithRouter(testWithLabelAndValueProps);

      const { queryByText } = wrapper;
      expect(queryByText('Mock Label')).toBeInTheDocument();
      expect(queryByText('Mock Name')).toBeInTheDocument();
      expect(
        wrapper.baseElement.querySelector('a.link-abbr')?.getAttribute('href')
      ).toContain('/change-management/detail/111');
    });

    it('should show group text control with link only', async () => {
      const testWithLabelAndValueProps = {
        label: 'Mock Label',
        input: {
          value: {
            id: '111',
            name: 'Mock Name'
          }
        },
        options: {
          link: '/change-management/{0}',
          linkDataField: 'name'
        }
      } as GroupTextControlProps;
      await createWrapperWithRouter(testWithLabelAndValueProps);

      const { queryByText } = wrapper;
      expect(queryByText('Mock Label')).toBeInTheDocument();
      expect(queryByText('Mock Name')).toBeInTheDocument();
      expect(
        wrapper.baseElement.querySelector('a.link-abbr')?.getAttribute('href')
      ).toContain('/change-management');
    });
  });

  describe('without label', () => {
    it('should show empty value when value = ""', async () => {
      await createWrapper();

      const { queryByText } = wrapper;

      expect(queryByText('Mock Label')).toBeNull();
    });
  });
});
