import { act, fireEvent, render, RenderResult } from '@testing-library/react';
import * as helper from 'app/helpers/isDevice';
import { mockUseDispatchFnc, mockUseModalRegistryFnc } from 'app/utils';
import 'app/utils/_mockComponent/mockLoadMore';
import React from 'react';
import WorkflowStrategyFlyout, { WorkflowStrategyFlyoutProps } from './index';

const mockUseModalRegistry = mockUseModalRegistryFnc();
const mockUseDispatch = mockUseDispatchFnc();

const generateMockData = (records: number) => {
  const result = [];
  for (let i = 0; i <= records; i++) {
    result.push({
      id: `449381282${i}`,
      name: `KLIC5${i}`,
      description: 'erat lorem labore nulla eos nihil sed',
      effectiveDate: '2021-06-07T15:02:31.185241+07:00',
      numberOfCardHolders: 20 + i
    });
  }
  return result;
};

jest.mock('app/helpers/isDevice', () => ({
  __esModule: true,
  isDevice: false
}));

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const mockHelper: any = helper;

describe('Test WorkflowStrategyFlyout Component', () => {
  const mockDispatch = jest.fn();
  const workflowStrategyFlyoutProps = {
    id: 'mock-id',
    show: true,
    methodName: 'Mock name',
    strategyList: [] as IWorkflowStrategyModel[]
  } as WorkflowStrategyFlyoutProps;

  const renderWorkflowStrategyFlyout = (
    props?: WorkflowStrategyFlyoutProps
  ): RenderResult => {
    return render(
      <div>
        <WorkflowStrategyFlyout {...workflowStrategyFlyoutProps} {...props} />
      </div>
    );
  };

  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);

    mockUseModalRegistry.mockImplementation(() => [true, false]);
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
    mockUseModalRegistry.mockClear();
  });

  it('should render WorkflowStrategyFlyout UI > empty data', async () => {
    let wrapper = renderWorkflowStrategyFlyout();

    expect(
      wrapper.queryByText('Mock name - txt_related_pricing_strategies')
    ).toBeInTheDocument();
    expect(
      wrapper.queryByText('txt_list_of_related_pricing_strategies')
    ).toBeInTheDocument();
    expect(
      wrapper.baseElement.querySelector('.dls-grid-body')!.childElementCount
    ).toEqual(0);

    wrapper.unmount();

    wrapper = renderWorkflowStrategyFlyout({
      ...workflowStrategyFlyoutProps,
      isNewVersion: true
    });

    expect(
      wrapper.queryByText('Mock name - txt_impacted_pricing_strategies')
    ).toBeInTheDocument();
    expect(
      wrapper.queryByText('txt_list_of_impacted_pricing_strategies')
    ).toBeInTheDocument();

    fireEvent.mouseEnter(
      wrapper.baseElement.querySelector('.dls-popper-trigger')!
    );

    expect(
      wrapper.queryByText('txt_number_of_cardholders_tooltip')
    ).toBeInTheDocument();
  });

  it('should render WorkflowStrategyFlyout UI > with data', async () => {
    const wrapper = renderWorkflowStrategyFlyout({
      ...workflowStrategyFlyoutProps,
      strategyList: [
        {
          id: '449381282',
          name: 'KLIC5',
          description: 'erat lorem labore nulla eos nihil sed',
          effectiveDate: '2021-06-07T15:02:31.185241+07:00',
          numberOfCardHolders: 51
        },
        {
          id: '1630775754',
          name: 'KYI1',
          description: 'aliquyam feugiat ut et dolore ex',
          effectiveDate: '2021-06-05T15:02:31.1852822+07:00',
          numberOfCardHolders: 23
        }
      ]
    });
    expect(
      wrapper.baseElement.querySelector('.dls-grid-body')!.childElementCount
    ).toEqual(2);
  });

  it('should render WorkflowStrategyFlyout UI > with data and show infinity scroll', async () => {
    mockHelper.isDevice = true;
    jest.useFakeTimers();

    const wrapper = renderWorkflowStrategyFlyout({
      ...workflowStrategyFlyoutProps,
      strategyList: generateMockData(40)
    });

    fireEvent.click(wrapper.getByText('Load more...'));

    expect(
      wrapper.getByText('Load more...').parentElement?.className
    ).toContain('loading');

    act(() => {
      jest.advanceTimersByTime(300);
    });

    expect(
      wrapper.getByText('Load more...').parentElement?.className
    ).not.toContain('loading');
  });
});
