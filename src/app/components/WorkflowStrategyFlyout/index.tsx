import ModalRegistry from 'app/components/ModalRegistry';
import { LOAD_MORE_PAGE_SIZE } from 'app/constants/constants';
import { isDevice } from 'app/helpers';
import {
  Grid,
  Icon,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  SimpleBar,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import _orderBy from 'lodash.orderby';
import {
  formatText,
  formatTruncate
} from 'pages/_commons/Utils/formatGridField';
import React, { useMemo, useRef, useState } from 'react';
import LoadMore from '../LoadMore';
export interface WorkflowStrategyFlyoutProps {
  id: string;
  show: boolean;
  methodName: string;
  isNewVersion?: boolean;
  strategyList: IWorkflowStrategyModel[];
  onClose: () => void;
}

const WorkflowStrategyFlyout: React.FC<WorkflowStrategyFlyoutProps> = ({
  id,
  show,
  methodName,
  isNewVersion,
  strategyList,
  onClose
}) => {
  const total = useRef<number>(strategyList.length);
  const [loadingMore, setLoadingMore] = useState<boolean>(false);
  const [page, setPage] = useState<number>(1);
  const { t } = useTranslation();

  const sortData = _orderBy(strategyList, 'name', 'asc');

  const modalHeaderTitle = `${methodName} - ${
    isNewVersion
      ? t('txt_impacted_pricing_strategies')
      : t('txt_related_pricing_strategies')
  }`;
  const modalBodyTitle = `${
    isNewVersion
      ? t('txt_list_of_impacted_pricing_strategies')
      : t('txt_list_of_related_pricing_strategies')
  }`;
  const endLoadMoreText = `${t('txt_end_of')} ${
    isNewVersion
      ? t('txt_impacted_pricing_strategies')
      : t('txt_related_pricing_strategies')
  }.`;

  const columns = useMemo(
    () => [
      {
        id: 'name',
        Header: t('txt_pricing_methods_strategyName'),
        accessor: formatTruncate(['name']),
        width: 110
      },
      {
        id: 'description',
        Header: t('txt_cis_description'),
        accessor: formatTruncate(['description']),
        width: 198
      },
      {
        id: 'effectiveDate',
        Header: t('txt_effective_date'),
        accessor: formatText(['effectiveDate'], { format: 'date' }),
        width: 115
      },
      {
        id: 'numberOfCardHolders',
        Header: () => (
          <div>
            {t('txt_number_of_cardholders')}
            <Tooltip
              element={t('txt_number_of_cardholders_tooltip')}
              placement="top"
              opened={isDevice ? false : undefined}
            >
              <Icon className="ml-4" name={'information'} />
            </Tooltip>
          </div>
        ),
        accessor: formatText(['numberOfCardHolders'], {
          format: 'amount',
          formatType: 'quantity',
          truncate: false
        }),
        cellHeaderProps: { className: 'text-right' },
        cellBodyProps: { className: 'text-right' }
      }
    ],
    [t]
  );

  const handleOnChangePage = () => {
    setLoadingMore(true);
    setTimeout(() => {
      setPage(page + 1);
      setLoadingMore(false);
    }, 300);
  };

  return (
    <ModalRegistry
      id={`pricing-strategy-detail-${id}`}
      xs
      rt
      enforceFocus={false}
      scrollable={false}
      show={show}
      animationDuration={500}
      animationComponentProps={{ direction: 'right' }}
      classes={{
        dialog: 'window-sm'
      }}
      onAutoClosedAll={onClose}
    >
      <ModalHeader border closeButton onHide={onClose}>
        <ModalTitle>{modalHeaderTitle}</ModalTitle>
      </ModalHeader>
      <ModalBody className="p-0 overflow-auto">
        <SimpleBar>
          <div className="p-24">
            <h5 className="pb-16">{modalBodyTitle}</h5>
            <Grid
              columns={columns}
              data={sortData.slice(0, page * LOAD_MORE_PAGE_SIZE)}
            />
            {total.current > LOAD_MORE_PAGE_SIZE && (
              <LoadMore
                loading={loadingMore}
                onLoadMore={handleOnChangePage}
                isEnd={page * LOAD_MORE_PAGE_SIZE >= total.current}
                className="mt-24"
                endLoadMoreText={endLoadMoreText}
              />
            )}
          </div>
        </SimpleBar>
      </ModalBody>
      <ModalFooter border cancelButtonText="Close" onCancel={onClose} />
    </ModalRegistry>
  );
};

export default WorkflowStrategyFlyout;
