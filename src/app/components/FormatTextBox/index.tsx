import { TextBox, TextBoxProps, TextBoxRef } from 'app/_libraries/_dls';
import { isFunction, isUndefined } from 'lodash';
import React, {
  forwardRef,
  useCallback,
  useEffect,
  useImperativeHandle,
  useRef,
  useState
} from 'react';

export interface FormatTextBoxProps extends Omit<TextBoxProps, 'value'> {
  value?: string;
  parse?: (value: string) => string;
  format?: (value: string) => string;
}
const FormatTextBox: React.ForwardRefRenderFunction<
  TextBoxRef,
  FormatTextBoxProps
> = (
  {
    pattern,
    value: valueProp,
    parse,
    format,
    onFocus,
    onBlur,
    onChange,

    ...rest
  },
  ref
) => {
  const textBoxRef = useRef<TextBoxRef>(null);
  useImperativeHandle(ref, () => textBoxRef.current!);

  const keepRef = useRef({ parse, format });
  keepRef.current.parse = parse;
  keepRef.current.format = format;

  const [focused, setFocused] = useState(false);

  const validateValue = useCallback(
    (value: string | undefined) => {
      return !pattern || new RegExp(pattern).test(value || '');
    },
    [pattern]
  );

  const handleFormat = useCallback(
    (value?: string) => {
      if (!value) return '';
      const _format = keepRef.current.format;

      const newValue = validateValue(value) ? value : '';

      return isFunction(_format) && !focused ? _format(newValue) : newValue;
    },
    [validateValue, focused]
  );

  const [value, setValue] = useState(handleFormat(valueProp));
  useEffect(() => setValue(handleFormat(valueProp)), [handleFormat, valueProp]);

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value;
    if (!validateValue(value)) return;

    if (isUndefined(valueProp)) {
      setValue(value);
    } else {
      isFunction(onChange) && onChange(e);
    }
  };

  const handleFocus = (e: React.FocusEvent<HTMLInputElement>) => {
    const _parse = keepRef.current.parse;
    setValue(_value => (isFunction(_parse) ? _parse(_value) : _value));
    setFocused(true);

    isFunction(onFocus) && onFocus(e);
  };

  const handleBlur = (e: React.FocusEvent<HTMLInputElement>) => {
    const _format = keepRef.current.format;
    setValue(_value => (isFunction(_format) ? _format(_value) : _value));
    setFocused(false);

    isFunction(onBlur) && onBlur(e);
  };

  return (
    <TextBox
      ref={textBoxRef}
      {...rest}
      value={value}
      onChange={handleChange}
      onFocus={handleFocus}
      onBlur={handleBlur}
    />
  );
};
export default forwardRef(FormatTextBox);
