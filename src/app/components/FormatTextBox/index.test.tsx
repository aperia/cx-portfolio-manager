import { fireEvent, render, RenderResult } from '@testing-library/react';
import React from 'react';
import FormatTextBox, { FormatTextBoxProps } from '.';

describe('Test FormatTextBox Component', () => {
  const focusFn = jest.fn();
  const blurFn = jest.fn();
  const changeFn = jest.fn();
  const renderFormatTextBox = (props?: FormatTextBoxProps): RenderResult => {
    return render(
      <div>
        <FormatTextBox
          ref={() => {}}
          onFocus={focusFn}
          onBlur={blurFn}
          onChange={changeFn}
          {...props}
        />
      </div>
    );
  };

  it('should render FormatTextBox with pattern and format & parse is Function', async () => {
    const formatFn = jest.fn();
    const parseFn = jest.fn();
    const wrapper = renderFormatTextBox({
      pattern: '\\d',
      value: '123',
      format: formatFn,
      parse: parseFn
    });

    const { getByDisplayValue } = wrapper;
    expect(getByDisplayValue('')).toBeInTheDocument();
    // focus on field element
    fireEvent.focus(getByDisplayValue('')!);
    expect(getByDisplayValue('123')).toBeInTheDocument();

    // change value
    fireEvent.change(getByDisplayValue('123'), { target: { value: '456' } });

    // blur on field element
    fireEvent.blur(getByDisplayValue('123')!);
    expect(getByDisplayValue('123')).toBeInTheDocument();
  });

  it('should render FormatTextBox with pattern and format & parse is string', async () => {
    const wrapper = renderFormatTextBox({
      pattern: '\\d',
      value: '123',
      format: '#',
      parse: '#'
    });

    const { getByDisplayValue } = wrapper;
    expect(getByDisplayValue(/123/i)).toBeInTheDocument();
    // focus on field element
    fireEvent.focus(getByDisplayValue(/123/i)!);
    expect(getByDisplayValue('123')).toBeInTheDocument();

    // change value
    fireEvent.change(getByDisplayValue(/123/i), { target: { value: '456' } });

    // blur on field element
    fireEvent.blur(getByDisplayValue(/123/i)!);
    expect(getByDisplayValue(/123/i)).toBeInTheDocument();
  });

  it('should render FormatTextBox with pattern and value undefined', async () => {
    const wrapper = renderFormatTextBox({
      value: undefined,
      format: '#',
      parse: '#'
    });

    const { getByDisplayValue } = wrapper;
    expect(getByDisplayValue('')).toBeInTheDocument();
    // focus on field element
    fireEvent.focus(getByDisplayValue('')!);
    expect(getByDisplayValue('')).toBeInTheDocument();

    // change value on field element
    fireEvent.change(getByDisplayValue(''), { target: { value: '456' } });

    // blur on field element
    fireEvent.blur(getByDisplayValue('456')!);
    expect(getByDisplayValue('')).toBeInTheDocument();
  });

  it('should render FormatTextBox with pattern undefined and value not empty', async () => {
    const wrapper = renderFormatTextBox({
      pattern: '\\d',
      value: 'abc',
      format: '#',
      parse: '#'
    });

    const { getByDisplayValue } = wrapper;
    expect(getByDisplayValue('')).toBeInTheDocument();
    // focus on field element
    fireEvent.focus(getByDisplayValue('')!);
    expect(getByDisplayValue('')).toBeInTheDocument();

    // change value
    fireEvent.change(getByDisplayValue(''), { target: { value: '456' } });

    // blur on field element
    fireEvent.blur(getByDisplayValue('')!);
    expect(getByDisplayValue('')).toBeInTheDocument();
  });

  it('should render FormatTextBox without format and parse', async () => {
    const wrapper = renderFormatTextBox({
      pattern: '\\d',
      value: '123'
    });

    const { getByDisplayValue } = wrapper;
    expect(getByDisplayValue('123')).toBeInTheDocument();
    // focus on field element
    fireEvent.focus(getByDisplayValue('123')!);
    expect(getByDisplayValue('123')).toBeInTheDocument();

    //change value
    fireEvent.change(getByDisplayValue('123'), { target: { value: '' } });

    // blur on field element
    fireEvent.blur(getByDisplayValue('123')!);
    expect(getByDisplayValue('123')).toBeInTheDocument();
  });
});
