import { fireEvent } from '@testing-library/react';
import Header from 'app/components/Header';
import { renderWithMockStore } from 'app/utils';
import { defaultDataFilter } from 'pages/_commons/redux/Notification/reducers';
import React from 'react';

const mockDefault = {
  onClickLogo: jest.fn()
};
const testId = 'RAC/Header';

describe('Test RAC/Header', () => {
  it('render to UI', async () => {
    const storeConfig = {
      initialState: {
        auth: {
          currentUser: {
            essUserName: 'Test Name 01',
            userId: 'FDESMAST',
            userName: 'testname',
            groups: ['HLNSA'],
            timeZone: -5
          }
        },
        notification: {
          notificationList: {
            notifications: [
              {
                id: 'id',
                changeSetId: '12345',
                description: 'desc',
                date: '03/04/2020',
                isRead: false
              },
              {
                id: '',
                changeSetId: '123456',
                description: 'desc',
                date: '03/04/2020',
                isRead: true
              }
            ],
            loading: false,
            error: false,
            filter: defaultDataFilter
          },
          updateNotification: {
            loading: false,
            error: false,
            total: 0
          }
        }
      }
    };
    const element = <Header {...mockDefault} />;
    const renderResult = await renderWithMockStore(
      element,
      storeConfig,
      {},
      testId
    );

    const { getByText } = renderResult;

    expect(
      getByText(storeConfig.initialState.auth.currentUser.essUserName)
    ).toBeInTheDocument();

    const wrapper = await renderResult.findByTestId(testId);

    const logoElement = wrapper.querySelector('.header-logo__link');
    fireEvent.click(logoElement!);

    const notificationBtn = wrapper.querySelector('.notification-button');
    fireEvent.click(notificationBtn!);

    const closeBtn = document.querySelector('.dls-modal-footer .btn');
    fireEvent.click(closeBtn!);
  });
});
