import { classnames } from 'app/helpers';
import {
  Bubble,
  Button,
  DropdownButton,
  Icon,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import isFunction from 'lodash.isfunction';
import { useSelectCurrentUser } from 'pages/_commons/Header/redux/selector';
import { useSelectUnReadNotificationIds } from 'pages/_commons/redux/Notification';
import React, { useState } from 'react';
import EnhanceDropdownButton from '../EnhanceDropdownButton';
import Notifications from './Notifications';

export interface HeaderProps {
  logo?: string;
  logoHref?: string;
  onClickLogo?: () => void;
}

const Header: React.FC<HeaderProps> = ({
  logo = '',
  logoHref = '/',
  onClickLogo,
  ...props
}) => {
  const { t } = useTranslation();
  const [showNotification, setShowNotification] = useState<boolean>(false);
  const { isAllRead } = useSelectUnReadNotificationIds();

  const dropdownBubbleData = [
    { code: 'profile', text: 'My Profile' },
    { code: 'logout', text: 'Logout' }
  ];
  const textField = 'text';

  const userPayload = useSelectCurrentUser();

  const handleOnClickLogo = (
    event: React.MouseEvent<HTMLAnchorElement, MouseEvent>
  ) => {
    event.preventDefault();
    isFunction(onClickLogo) && onClickLogo();
  };

  return (
    <header className="header border-bottom" id="header-component">
      <div className="d-flex align-items-center w-100">
        <div className="header-logo">
          <a
            href={logoHref}
            onClick={handleOnClickLogo}
            className="header-logo__link"
          >
            <img src={logo} alt="logo" />
          </a>
          <div className="ml-24 mr-24 border-right br-light-l12" />
        </div>

        <div className="ml-auto d-flex justify-content-end mr-n6">
          <Tooltip
            element={t('txt_notifications')}
            placement="bottom"
            variant="primary"
            triggerClassName="d-flex"
          >
            <Button
              variant="icon-secondary"
              className={classnames(
                'notification-button align-self-center',
                !isAllRead && 'asterisk'
              )}
              onClick={() => setShowNotification(true)}
            >
              <Icon
                name={'notification' as any}
                className="color-grey-l16"
                size="5x"
              />
            </Button>
            <Notifications
              show={showNotification}
              onCloseNotifications={() => setShowNotification(false)}
            />
          </Tooltip>

          <div className="ml-24 mr-24 border-right br-light-l12" />
          <div className="d-flex align-items-center">
            <EnhanceDropdownButton
              buttonProps={{
                className: 'd-flex align-items-center',
                children: (
                  <>
                    <div className="d-flex align-items-center">
                      <Bubble name={userPayload?.essUserName || ''} small />
                      <p className="ml-4">{userPayload?.essUserName}</p>
                    </div>
                  </>
                ),
                variant: 'outline-secondary'
              }}
              popupBaseProps={{
                popupBaseClassName: 'inside-infobar'
              }}
              className="d-flex align-items-center"
            >
              {dropdownBubbleData.map((item, index) => (
                <DropdownButton.Item
                  key={index}
                  label={item[textField]}
                  value={item}
                  className="d-flex align-items-center"
                />
              ))}
            </EnhanceDropdownButton>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
