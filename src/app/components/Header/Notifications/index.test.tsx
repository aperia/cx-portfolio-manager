import { fireEvent } from '@testing-library/react';
import { NotificationData } from 'app/fixtures/notification-data';
import { renderWithMockStore } from 'app/utils';
import 'app/utils/_mockComponent/mockFailedApiReload';
import 'app/utils/_mockComponent/mockLoadMore';
import 'app/utils/_mockComponent/mockModalRegistry';
import { defaultDataFilter } from 'pages/_commons/redux/Notification/reducers';
import * as NotificationRedux from 'pages/_commons/redux/Notification/select-hooks';
import React from 'react';
import * as reactRedux from 'react-redux';
import { HashRouter } from 'react-router-dom';
import Notifications from './index';

const mockSelectNotification = jest.spyOn(
  NotificationRedux,
  'useSelectNotifications'
);
const mockUnReadNotificationIds = jest.spyOn(
  NotificationRedux,
  'useSelectUnReadNotificationIds'
);
const mockSelectUpdateNotification = jest.spyOn(
  NotificationRedux,
  'useSelectUpdateNotification'
);
const mockSelectNotificationFilter = jest.spyOn(
  NotificationRedux,
  'useSelectNotificationFilter'
);

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');

const mockDefault = {
  show: true,
  onCloseNotifications: jest.fn()
};
const testId = 'Notifications';

// jest.mock('pages/_commons/Utils/ClearAndResetButton', () => ({
//   __esModule: true,
//   default: ({ onClearAndReset }: any) => (
//     <button onClick={() => onClearAndReset}>{'Clear And Reset'}</button>
//   )
// }));

describe('Test Notifications', () => {
  const storeConfig = {
    initialState: {}
  };

  afterEach(() => {
    jest.clearAllMocks();
    jest.useRealTimers();
  });

  it('render loading & error UI', async () => {
    const dispatchFn = jest.fn();

    const element = (
      <HashRouter>
        <Notifications {...mockDefault} />
      </HashRouter>
    );
    mockSelectNotification.mockReturnValue({
      notifications: [],
      loading: false,
      error: false,
      total: 0
    });
    mockUnReadNotificationIds.mockReturnValue({
      notificationIds: [],
      isAllRead: true
    });
    mockSelectUpdateNotification.mockReturnValue({
      loading: true,
      total: 0
    });
    mockSelectNotificationFilter.mockReturnValue({
      ...defaultDataFilter
    });
    mockUseDispatch.mockReturnValue(dispatchFn);
    const { getByTestId } = await renderWithMockStore(
      element,
      storeConfig,
      {},
      testId
    );

    expect(
      getByTestId(testId).querySelector('.dls-modal-body')?.className
    ).toContain('loading');
    mockSelectUpdateNotification.mockReturnValue({
      loading: false,
      total: 0
    });
    mockSelectNotification.mockReturnValue({
      notifications: [],
      loading: false,
      error: true,
      total: 0
    });

    const { getByText } = await renderWithMockStore(
      element,
      storeConfig,
      {},
      testId
    );

    expect(getByText(/Data load unsuccessful/i)).toBeInTheDocument();
    fireEvent.click(getByText(/Reload/i));
    expect(dispatchFn).toHaveBeenCalled();
  });

  it('render notification flyout with data', async () => {
    const dispatchFn = jest.fn();

    const element = (
      <HashRouter>
        <Notifications {...mockDefault} />
      </HashRouter>
    );
    mockSelectNotification.mockReturnValue({
      notifications: NotificationData as any,
      loading: false,
      error: false,
      total: 40
    });
    mockUnReadNotificationIds.mockReturnValue({
      notificationIds: [],
      isAllRead: false
    });
    mockSelectUpdateNotification.mockReturnValue({
      loading: false,
      total: 0
    });
    mockSelectNotificationFilter.mockReturnValue({
      ...defaultDataFilter,
      searchFields: 'isRead',
      searchValue: 'true'
    });
    mockUseDispatch.mockReturnValue(dispatchFn);
    const { getByText } = await renderWithMockStore(
      element,
      storeConfig,
      {},
      testId
    );

    expect(getByText(/Wed 03\/04\/2020/i)).toBeInTheDocument();
    fireEvent.click(
      getByText(/go to change detail/i).parentElement!.parentElement!
    );
    expect(dispatchFn).toHaveBeenCalled();

    fireEvent.click(getByText(/txt_unread/i));
    fireEvent.click(getByText(/txt_read/i));

    fireEvent.click(getByText(/txt_clear_and_reset/i));
    expect(dispatchFn).toHaveBeenCalled();

    fireEvent.click(getByText(/txt_mark_all_as_read/i));
    expect(dispatchFn).toHaveBeenCalled();
    jest.useFakeTimers();
    fireEvent.click(getByText(/Load more.../i));
    expect(document.body.querySelector('.load-more-cont')!.className).toContain(
      'loading'
    );
    jest.advanceTimersByTime(300);
    expect(
      document.body.querySelector('.load-more-cont')!.className
    ).not.toContain('loading');

    mockSelectNotification.mockReturnValue({
      notifications: [],
      loading: false,
      error: false,
      total: 0
    });
    mockSelectNotificationFilter.mockReturnValue({
      ...defaultDataFilter
    });

    const { getByText: getByText1 } = await renderWithMockStore(
      element,
      storeConfig,
      {},
      testId
    );

    expect(getByText1(/txt_no_notifications_to_display/i)).toBeInTheDocument();

    mockSelectNotificationFilter.mockReturnValue({
      ...defaultDataFilter,
      searchFields: 'isRead',
      searchValue: 'false'
    });

    const { getByText: getByText2 } = await renderWithMockStore(
      element,
      storeConfig,
      {},
      testId
    );

    expect(getByText2(/txt_no_notifications_filter/i)).toBeInTheDocument();
  });

  it('render notification flyout with data > searchValue: false', async () => {
    const dispatchFn = jest.fn();

    const element = (
      <HashRouter>
        <Notifications {...mockDefault} />
      </HashRouter>
    );
    mockSelectNotification.mockReturnValue({
      notifications: NotificationData as any,
      loading: false,
      error: false,
      total: 40
    });
    mockUnReadNotificationIds.mockReturnValue({
      notificationIds: [],
      isAllRead: false
    });
    mockSelectUpdateNotification.mockReturnValue({
      loading: false,
      total: 0
    });
    mockSelectNotificationFilter.mockReturnValue({
      ...defaultDataFilter,
      searchFields: 'isRead',
      searchValue: 'false'
    });
    mockUseDispatch.mockReturnValue(dispatchFn);
    const { getByText } = await renderWithMockStore(
      element,
      storeConfig,
      {},
      testId
    );

    expect(getByText(/Wed 03\/04\/2020/i)).toBeInTheDocument();
    fireEvent.click(
      getByText(/go to change detail/i).parentElement!.parentElement!
    );
    expect(dispatchFn).toHaveBeenCalled();

    fireEvent.click(getByText(/txt_unread/i));
    fireEvent.click(getByText(/txt_read/i));

    fireEvent.click(getByText(/txt_clear_and_reset/i));
    expect(dispatchFn).toHaveBeenCalled();

    fireEvent.click(getByText(/txt_mark_all_as_read/i));
    expect(dispatchFn).toHaveBeenCalled();
    jest.useFakeTimers();
    fireEvent.click(getByText(/Load more.../i));
    expect(document.body.querySelector('.load-more-cont')!.className).toContain(
      'loading'
    );
    jest.advanceTimersByTime(300);
    expect(
      document.body.querySelector('.load-more-cont')!.className
    ).not.toContain('loading');

    mockSelectNotification.mockReturnValue({
      notifications: [],
      loading: false,
      error: false,
      total: 0
    });
    mockSelectNotificationFilter.mockReturnValue({
      ...defaultDataFilter
    });

    const { getByText: getByText1 } = await renderWithMockStore(
      element,
      storeConfig,
      {},
      testId
    );

    expect(getByText1(/txt_no_notifications_to_display/i)).toBeInTheDocument();

    mockSelectNotificationFilter.mockReturnValue({
      ...defaultDataFilter,
      searchFields: 'isRead',
      searchValue: 'false'
    });

    const { getByText: getByText2 } = await renderWithMockStore(
      element,
      storeConfig,
      {},
      testId
    );

    expect(getByText2(/txt_no_notifications_filter/i)).toBeInTheDocument();
  });

  it('render onClearAndReset with no notifications', async () => {
    const dispatchFn = jest.fn();

    const element = (
      <HashRouter>
        <Notifications {...mockDefault} />
      </HashRouter>
    );
    mockSelectNotification.mockReturnValue({
      notifications: [],
      loading: false,
      error: false,
      total: 40
    });
    mockUnReadNotificationIds.mockReturnValue({
      notificationIds: [],
      isAllRead: false
    });
    mockSelectUpdateNotification.mockReturnValue({
      loading: false,
      total: 0
    });
    mockSelectNotificationFilter.mockReturnValue({
      ...defaultDataFilter,
      searchFields: 'isRead',
      searchValue: 'true'
    });
    mockUseDispatch.mockReturnValue(dispatchFn);
    const { getByText } = await renderWithMockStore(
      element,
      storeConfig,
      {},
      testId
    );

    fireEvent.click(getByText(/txt_clear_and_reset/i));
    expect(dispatchFn).toHaveBeenCalled();

    fireEvent.click(getByText(/txt_mark_all_as_read/i));
    expect(dispatchFn).toHaveBeenCalled();
  });

  it('render notification flyout with reminder data, click to show popup', async () => {
    const dispatchFn = jest.fn();

    const element = (
      <HashRouter>
        <Notifications {...mockDefault} />
      </HashRouter>
    );
    mockSelectNotification.mockReturnValue({
      notifications: NotificationData as any,
      loading: false,
      error: false,
      total: 4
    });
    mockUnReadNotificationIds.mockReturnValue({
      notificationIds: [],
      isAllRead: false
    });
    mockSelectUpdateNotification.mockReturnValue({
      loading: false,
      total: 0
    });
    mockSelectNotificationFilter.mockReturnValue({
      ...defaultDataFilter
    });
    mockUseDispatch.mockReturnValue(dispatchFn);
    const { getByText, queryByText } = await renderWithMockStore(
      element,
      storeConfig,
      {},
      testId
    );

    expect(getByText(/title 3/i)).toBeInTheDocument();
    fireEvent.click(getByText(/title 3/i).parentElement!.parentElement!);
    expect(getByText(/txt_reminder_note/i)).toBeInTheDocument();
    fireEvent.click(getByText(/txt_skip/i));
    expect(queryByText(/txt_reminder_note/i)).toBeNull();
    fireEvent.click(getByText(/txt_mark_all_as_read/i));
    expect(dispatchFn).toHaveBeenCalledTimes(3);
  });
});
