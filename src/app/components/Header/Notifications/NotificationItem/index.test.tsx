import { NotificationData } from 'app/fixtures/notification-data';
import { renderComponent } from 'app/utils';
import 'app/utils/_mockComponent/mockFailedApiReload';
import 'app/utils/_mockComponent/mockLoadMore';
import 'app/utils/_mockComponent/mockModalRegistry';
import React from 'react';
import NotificationItem from './index';

const mockDefault = {
  id: NotificationData[0].id,
  title: NotificationData[0].title,
  description: NotificationData[0].description,
  date: NotificationData[0].date,
  isRead: NotificationData[0].isRead
};
const testId = 'NotificationsItem';

describe('Test NotificationItem', () => {
  it('render notification item UI', async () => {
    const element = <NotificationItem {...(mockDefault as any)} />;
    const { getByText } = await renderComponent(testId, element);
    expect(getByText(/go to change detail/i)).toBeInTheDocument();
  });

  it('render notification item UI and greyout', async () => {
    const element = (
      <NotificationItem {...(mockDefault as any)} id={''} isRead={true} />
    );
    const { getByText } = await renderComponent(testId, element);
    expect(getByText(/go to change detail/i)).toBeInTheDocument();
    expect(getByText(/go to change detail/i).className).toContain(
      'text-secondary'
    );
  });
});
