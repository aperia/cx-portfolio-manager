import { classnames, formatTime } from 'app/helpers';
import React from 'react';

const NotificationItem: React.FC<INotification> = ({
  id,
  title,
  description,
  date,
  isRead
}) => {
  const { fullTimeMeridiem: time } = formatTime(date);
  return (
    <div
      id={`NotificationItem-${id || 'empty'}`}
      className={classnames('notification-item', !isRead && 'unread')}
    >
      <div
        className={classnames(
          'notification-item-title fw-500 mb-4',
          isRead && 'text-secondary'
        )}
      >
        {title}
      </div>
      <div className="fs-12 text-secondary">{time}</div>
      {description && (
        <div className="fs-14 mt-8 text-secondary">{description}</div>
      )}
    </div>
  );
};

export default NotificationItem;
