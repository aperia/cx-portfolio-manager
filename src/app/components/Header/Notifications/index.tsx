import FailedApiReload from 'app/components/FailedApiReload';
import LoadMore from 'app/components/LoadMore';
import ModalRegistry from 'app/components/ModalRegistry';
import ReminderModal from 'app/components/ReminderModal';
import { changeDetailLink } from 'app/constants/links';
import { NOTIFICATION_REMINDER_ACTIONS } from 'app/constants/notification-types';
import { classnames, formatLogDate, formatTime } from 'app/helpers';
import {
  Button,
  GroupButton,
  Icon,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  SimpleBar,
  useTranslation
} from 'app/_libraries/_dls';
import { isFunction } from 'lodash';
import {
  actionsNotification,
  useSelectNotificationFilter,
  useSelectNotifications,
  useSelectUnReadNotificationIds,
  useSelectUpdateNotification
} from 'pages/_commons/redux/Notification';
import ClearAndResetButton from 'pages/_commons/Utils/ClearAndResetButton';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
import NotificationItem from './NotificationItem';

enum NotificationStatus {
  All,
  Read,
  UnRead
}

interface INotificationsProps {
  show: boolean;
  onCloseNotifications: () => void;
}

const Notifications: React.FC<INotificationsProps> = ({
  show,
  onCloseNotifications
}) => {
  const { t } = useTranslation();
  const buttons = [
    {
      id: NotificationStatus.All,
      label: t('txt_all')
    },
    {
      id: NotificationStatus.Read,
      label: t('txt_read')
    },
    {
      id: NotificationStatus.UnRead,
      label: t('txt_unread')
    }
  ];

  const [reminderItem, setReminderItem] = useState<INotification>();
  const [loadingMore, setLoadingMore] = useState(false);

  const dispatch = useDispatch();
  const history = useHistory();
  const contentRef = useRef<HTMLDivElement>(null);
  const keepRef = useRef({ onCloseNotifications });
  keepRef.current.onCloseNotifications = onCloseNotifications;

  const { notifications, loading, error, total } = useSelectNotifications();

  const { notificationIds, isAllRead } = useSelectUnReadNotificationIds();
  const { loading: updating } = useSelectUpdateNotification();

  const groupNotifications =
    notifications &&
    notifications.length > 0 &&
    notifications.reduce((accum, current) => {
      const updatedDate = formatTime(current.date).date;
      if (!accum[updatedDate!]) accum[updatedDate!] = [];
      accum[updatedDate!].push(current);
      return accum;
    }, {} as any);

  const { page, pageSize, searchFields, searchValue } =
    useSelectNotificationFilter();

  const buttonSelected = useMemo(() => {
    if (!searchFields) return NotificationStatus.All;
    if (searchValue?.toLowerCase() === 'true') {
      return NotificationStatus.Read;
    }
    return NotificationStatus.UnRead;
  }, [searchFields, searchValue]);

  const hasFilter = buttonSelected !== NotificationStatus.All;

  const haveLoadMore = useMemo(() => {
    return pageSize && pageSize < total;
  }, [pageSize, total]);
  // pagination
  const handleOnChangePage = useCallback(() => {
    setLoadingMore(true);
    setTimeout(() => {
      dispatch(
        actionsNotification.updateNotificationListFilter({ page: page! + 1 })
      );
      setLoadingMore(false);
    }, 300);
  }, [dispatch, page]);

  const handleFilterChange = useCallback(
    (id: NotificationStatus) => {
      const searchFields = id !== NotificationStatus.All ? 'isRead' : '';
      const searchValue = id === NotificationStatus.Read;
      dispatch(
        actionsNotification.updateNotificationListFilter({
          searchFields,
          searchValue: searchValue.toString()
        })
      );
    },
    [dispatch]
  );

  const handleMarkAllAsRead = useCallback(() => {
    dispatch(
      actionsNotification.markNotificationsAsRead({
        notificationIds,
        date: new Date()
      })
    );
  }, [dispatch, notificationIds]);

  const handleApiReload = useCallback(() => {
    dispatch(actionsNotification.getNotifications({ showLoading: true }));
  }, [dispatch]);

  const navigateToChangeDetail = useCallback(
    changeSetId => {
      history.push(changeDetailLink(changeSetId));
    },
    [history]
  );

  const handleClickNotification = useCallback(
    (item: INotification) => {
      const { id, changeSetId, action, isRead } = item;
      const onClick = () => {
        const { onCloseNotifications: onClose } = keepRef.current;

        !isRead &&
          dispatch(
            actionsNotification.markNotificationsAsRead({
              notificationIds: [id],
              date: new Date()
            })
          );

        const actionName = action?.toLowerCase();

        const isReminder = NOTIFICATION_REMINDER_ACTIONS.includes(actionName);
        if (isReminder) {
          setReminderItem(item);
          return;
        }

        isFunction(onClose) && onClose();
        navigateToChangeDetail(changeSetId);
      };

      return onClick;
    },
    [dispatch, navigateToChangeDetail]
  );

  useEffect(() => {
    const el = contentRef.current?.querySelector('.check-intersection');
    if (el) {
      const observer = new IntersectionObserver(
        ([e]) => {
          const flyoutHead = contentRef.current?.querySelector('.flyout-head');
          flyoutHead &&
            flyoutHead.classList.toggle('has-scroll', e.intersectionRatio < 1);
        },
        {
          threshold: [0.1]
        }
      );
      observer.observe(el);
      return () => observer.unobserve(el);
    }
  });

  useEffect(() => {
    show &&
      dispatch(actionsNotification.getNotifications({ showLoading: true }));
    return () => {
      dispatch(actionsNotification.clearAndResetNotificationListFilter());
    };
  }, [dispatch, show]);

  return (
    <React.Fragment>
      <ModalRegistry
        id={`notifications`}
        xs
        rt
        enforceFocus={false}
        scrollable={false}
        show={show}
        animationDuration={500}
        animationComponentProps={{ direction: 'right' }}
        onAutoClosedAll={onCloseNotifications}
        classes={{
          content: 'custom-modal'
        }}
      >
        <ModalHeader border closeButton onHide={onCloseNotifications}>
          <ModalTitle>{t('txt_notifications')}</ModalTitle>
        </ModalHeader>
        <ModalBody
          className={classnames(
            'p-0 overflow-hidden',
            (loading || updating) && 'loading'
          )}
        >
          {!(loading || updating) && (
            <SimpleBar>
              <div className="d-flex flex-column" ref={contentRef}>
                <div className="check-intersection" />
                <div className="flyout-head sticky pb-0 pt-24 px-24">
                  <div className="d-flex align-items-center justify-content-between">
                    <h5>{t('txt_notification_list')}</h5>
                    <Button
                      className="mr-i-8"
                      size="sm"
                      variant={'outline-primary'}
                      onClick={handleMarkAllAsRead}
                      disabled={
                        buttonSelected === NotificationStatus.Read || isAllRead
                      }
                    >
                      {t('txt_mark_all_as_read')}
                    </Button>
                  </div>
                  <div className="d-flex justify-content-between pt-16 pb-24">
                    <GroupButton>
                      {buttons.map(({ id, label }) => {
                        return (
                          <Button
                            key={id}
                            selected={buttonSelected === id}
                            onClick={() => handleFilterChange(id)}
                            size="sm"
                            variant="secondary"
                          >
                            {label}
                          </Button>
                        );
                      })}
                    </GroupButton>
                    {hasFilter && Object.keys(groupNotifications).length > 0 && (
                      <div className="mr-n8">
                        <ClearAndResetButton
                          small
                          onClearAndReset={() =>
                            handleFilterChange(NotificationStatus.All)
                          }
                        />
                      </div>
                    )}
                  </div>
                </div>
                <div className="flyout-body pt-0 pb-24 px-24">
                  <div id="activity-card-list">
                    {error ? (
                      <FailedApiReload
                        id="notification-flyout-error"
                        onReload={handleApiReload}
                        className="mt-40 pt-40 d-flex flex-column justify-content-center align-items-center"
                      />
                    ) : Object.keys(groupNotifications).length > 0 ? (
                      Object.keys(groupNotifications).map((k, index) => {
                        const date = formatLogDate(k);
                        const dateNotifications = groupNotifications[k];
                        return (
                          <div key={index}>
                            <div className="group-date text-center fs-12 pb-8 fw-500 color-grey text-uppercase sticky mx-n24">
                              {date}
                            </div>
                            {dateNotifications.map(
                              (notification: INotification, dIndex: number) => (
                                <div
                                  key={notification.id}
                                  onClick={handleClickNotification(
                                    notification
                                  )}
                                  className={classnames({
                                    'mt-n8': dIndex === 0,
                                    'last-item':
                                      dIndex === dateNotifications.length - 1
                                  })}
                                >
                                  <NotificationItem {...notification} />
                                  {dIndex < dateNotifications.length - 1 && (
                                    <div
                                      style={{ border: '1px dashed #E3E3E8' }}
                                    />
                                  )}
                                </div>
                              )
                            )}
                            {index <
                              Object.keys(groupNotifications).length - 1 && (
                              <hr className="mt-0 mb-24 mx-n24" />
                            )}
                          </div>
                        );
                      })
                    ) : (
                      <div className="pt-28 text-center">
                        <Icon
                          name={`notification color-light-l12 fs-80` as any}
                          className="pb-20"
                        />
                        <div className="mb-24">
                          {hasFilter
                            ? t('txt_no_notifications_filter')
                            : t('txt_no_notifications_to_display')}
                        </div>
                        <ClearAndResetButton
                          onClearAndReset={() =>
                            handleFilterChange(NotificationStatus.All)
                          }
                        />
                      </div>
                    )}
                  </div>
                  {haveLoadMore && (
                    <LoadMore
                      loading={loadingMore}
                      onLoadMore={handleOnChangePage}
                      isEnd={page! * pageSize! > total}
                      className="mt-24"
                      endLoadMoreText={t('txt_end_of_notifications')}
                    />
                  )}
                </div>
              </div>
            </SimpleBar>
          )}
        </ModalBody>
        <ModalFooter
          border
          cancelButtonText="Close"
          onCancel={onCloseNotifications}
        />
      </ModalRegistry>
      {reminderItem && (
        <ReminderModal
          id={`ChangeSetReminderModal_${reminderItem.id}`}
          show
          workflowTemplateId={reminderItem.workflowTemplateId!}
          workflowName={reminderItem.workflowName!}
          description={reminderItem.description}
          onClose={() => setReminderItem(undefined)}
        />
      )}
    </React.Fragment>
  );
};

export default Notifications;
