import { DropdownBaseChangeEvent } from 'app/_libraries/_dls';
import MultiSelect, {
  MultiSelectProps
} from 'app/_libraries/_dls/components/MultiSelect';
import { classes } from 'app/_libraries/_dls/utils';
import { isFunction } from 'lodash';
import isEmpty from 'lodash.isempty';
import React, {
  useCallback,
  useEffect,
  useLayoutEffect,
  useRef,
  useState
} from 'react';

const EnhanceMultiSelect: React.FC<
  Omit<
    MultiSelectProps,
    'children' | 'opened' | 'onFilterChange' | 'onVisibilityChange'
  > & { data: any[]; minFilterLength?: number }
> = ({
  data,
  minFilterLength = 2,

  value: valueProp,
  textField: textFieldProp,
  placeholder,
  label,
  onChange,
  onFocus,
  onBlur,
  error,
  ...props
}) => {
  const multiSelectRef = useRef<HTMLDivElement>(null);

  const [opened, setOpened] = useState(false);
  const [focus, setFocus] = useState(false);
  const [filter, setFilter] = useState('');
  const [multiSelectItems, setMultiSelectItems] = useState<any[]>([]);

  // Currently, focused and floating in dls work for opened state only.
  // This custom multiselect still close after focus and type at least 2 characters
  // So, need manual changing class to support focused/floating state
  const updateFocusedAndFloatingState = useCallback(
    (isFocus: boolean) => {
      const containerElement = multiSelectRef.current?.querySelector(
        `.${classes.multiSelect.container}`
      ) as HTMLDivElement;
      const inputElement = multiSelectRef.current?.querySelector(
        `.filter input`
      ) as HTMLInputElement;

      if (isFocus && multiSelectRef.current?.contains(document.activeElement)) {
        !containerElement?.classList?.contains(classes.state.focused) &&
          containerElement?.classList?.add(classes.state.focused);
        !containerElement?.classList.contains(classes.state.floating) &&
          containerElement?.classList?.add(classes.state.floating);
      } else {
        containerElement?.classList?.contains(classes.state.focused) &&
          containerElement?.classList?.remove(classes.state.focused);

        if (valueProp.length === 0) {
          containerElement?.classList?.contains(classes.state.floating) &&
            containerElement?.classList?.remove(classes.state.floating);
        }
      }
      inputElement && (inputElement.style.width = '100%');
    },
    [valueProp.length]
  );

  useLayoutEffect(() => {
    const inputElement = multiSelectRef.current?.querySelector(
      `.filter input`
    ) as HTMLInputElement;
    valueProp.length === 0 && placeholder && (!label || focus)
      ? inputElement.setAttribute('placeholder', placeholder)
      : inputElement.setAttribute('placeholder', '');
  }, [valueProp.length, placeholder, label, focus]);

  useEffect(() => {
    if (filter.length < minFilterLength) {
      if (opened && filter.length !== 0) return;

      setOpened(false);
      setMultiSelectItems([]);

      filter.length > 0 && updateFocusedAndFloatingState(true);
      return;
    }

    if (data.some(item => item[textFieldProp!] === filter)) return;

    const nextData = data
      .filter(
        (item: any) =>
          item[textFieldProp!]?.toLowerCase()?.indexOf(filter.toLowerCase()) !==
          -1
      )
      .map((item, index) => (
        <MultiSelect.Item
          key={item.code || index}
          label={item[textFieldProp!]}
          value={item}
          variant="checkbox"
        />
      ));

    setMultiSelectItems(nextData);
    setOpened(true);
  }, [
    updateFocusedAndFloatingState,
    filter,
    opened,
    data,
    textFieldProp,
    minFilterLength
  ]);

  useEffect(() => {
    if (valueProp.length > 0 && !opened) {
      setMultiSelectItems([]);
    }
  }, [opened, valueProp]);

  useEffect(() => {
    if (!opened) {
      setMultiSelectItems([]);
    }
  }, [opened]);

  // it's blur after change, need manual focus to continue search
  useEffect(() => {
    if (opened || !multiSelectRef.current?.contains(document.activeElement))
      return;

    const timeoutId = setTimeout(() => {
      const inputElement = multiSelectRef.current?.querySelector(
        `.filter input`
      ) as HTMLInputElement;
      inputElement?.focus();
      updateFocusedAndFloatingState(true);

      clearTimeout(timeoutId);
    }, 100);
    return () => {
      clearTimeout(timeoutId);
    };
  }, [opened, updateFocusedAndFloatingState]);

  const handleFocus = (e: React.FocusEvent<Element>) => {
    setFocus(true);
    isFunction(onFocus) && onFocus(e);

    updateFocusedAndFloatingState(true);
  };

  const handleBlur = (e: React.FocusEvent<Element>) => {
    setFocus(false);
    isFunction(onBlur) && onBlur(e);

    updateFocusedAndFloatingState(false);
  };

  const handleChange = (e: DropdownBaseChangeEvent) => {
    isFunction(onChange) && onChange(e);
  };

  const handleFilterChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setFilter(e.target.value);

    process.nextTick(() => updateFocusedAndFloatingState(true));
  };

  return (
    <div ref={multiSelectRef}>
      <MultiSelect
        opened={opened}
        onFilterChange={handleFilterChange}
        onVisibilityChange={nextOpened => {
          if (isEmpty(multiSelectItems) && !filter) {
            return setOpened(false);
          }

          setOpened(nextOpened);
        }}
        onChange={handleChange}
        onFocus={handleFocus}
        onBlur={handleBlur}
        value={valueProp}
        textField={textFieldProp}
        error={error}
        // fix error not show when focus
        errorTooltipProps={{
          opened: Boolean(error && error.status && focus)
        }}
        {...props}
      >
        {multiSelectItems}
      </MultiSelect>
    </div>
  );
};

export default EnhanceMultiSelect;
