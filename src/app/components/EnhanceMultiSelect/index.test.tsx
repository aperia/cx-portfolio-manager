import { fireEvent, render, RenderResult } from '@testing-library/react';
import 'app/utils/_mockComponent/mockCanvas';
import { MultiSelectProps } from 'app/_libraries/_dls/components/MultiSelect';
import React from 'react';
import EnhanceMultiSelect from './index';

const MS_DATA = [
  {
    code: '',
    text: 'All'
  },
  {
    code: 'NER',
    text: 'NIGER'
  },
  {
    code: 'GRL',
    text: 'GREENLAND'
  },
  {
    code: '',
    text: 'GREENLAND1'
  }
];

describe('Test EnhanceMultiSelect Component', () => {
  const mockMultiSelectProp = {
    id: 'cbbId',
    name: 'cbbName',
    textField: 'text',
    label: 'Mock Multi Select',
    placeholder: 'My Multi Select',
    data: [],
    value: [],
    error: {
      status: true
    }
  };

  const renderCombobox = (
    props?: Omit<
      MultiSelectProps,
      'children' | 'opened' | 'onFilterChange' | 'onVisibilityChange'
    > & { data: any[]; minFilterLength?: number }
  ): RenderResult => {
    return render(
      <div>
        <EnhanceMultiSelect {...mockMultiSelectProp} {...props} />
      </div>
    );
  };

  beforeEach(() => {});

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render Enhance Multi select UI', () => {
    jest.useFakeTimers();

    const wrapper = renderCombobox({
      data: MS_DATA
    });

    // change filter to 'g'
    const input = wrapper.baseElement.querySelector(
      '.text-field-container .filter > input'
    );
    fireEvent.change(input!, { target: { value: 'g' } });
    expect(
      wrapper.baseElement.querySelector('.dls-popup')
    ).not.toBeInTheDocument();

    // change filter to ''
    fireEvent.change(input!, { target: { value: 'gr' } });
    jest.runAllTimers();

    expect(wrapper.baseElement.querySelector('.dls-popup')).toBeInTheDocument();

    expect(wrapper.getAllByText('GR').length).toEqual(2);
  });

  it('should render Enhance Multi UI, with init data & no open', () => {
    const blurFn = jest.fn();

    const wrapper = renderCombobox({
      data: MS_DATA,
      value: [MS_DATA[0]],
      onBlur: blurFn
    });

    // Expect not open popup
    expect(
      wrapper.baseElement.querySelector('.dls-popup')
    ).not.toBeInTheDocument();

    const input = wrapper.baseElement.querySelector(
      '.text-field-container .filter > input'
    );
    fireEvent.blur(input!);
    expect(blurFn).toHaveBeenCalled();
  });

  it('should render Enhance Multi UI, update focus & floating state', () => {
    jest.useFakeTimers();
    const wrapper = renderCombobox({
      data: MS_DATA
    });
    Object.defineProperty(document, 'activeElement', {
      value: wrapper.getByRole('textbox')
    });

    // change filter to 'GREENLAND'
    const input = wrapper.baseElement.querySelector(
      '.text-field-container .filter > input'
    );
    fireEvent.change(input!, { target: { value: 'GREENLAND' } });

    expect(
      wrapper.baseElement.querySelector('.dls-popup')
    ).not.toBeInTheDocument();

    // change filter to 'gr'
    fireEvent.change(input!, { target: { value: 'gr' } });
    expect(wrapper.baseElement.querySelector('.dls-popup')).toBeInTheDocument();
    expect(wrapper.getAllByText('GR').length).toEqual(2);

    // change filter to ''
    fireEvent.change(input!, { target: { value: '' } });
    jest.runAllTimers();

    fireEvent.click(document.body);
    expect(
      wrapper.baseElement.querySelector('.dls-popup')
    ).not.toBeInTheDocument();
  });

  it('should call Multi select events: focus, blur', () => {
    const focusFn = jest.fn();
    const blurFn = jest.fn();
    const changeFn = jest.fn();

    const wrapper = renderCombobox({
      data: MS_DATA,
      onFocus: focusFn,
      onBlur: blurFn,
      onChange: changeFn
    });

    const input = wrapper.baseElement.querySelector(
      '.text-field-container .filter > input'
    );
    fireEvent.focus(input!);
    expect(focusFn).toHaveBeenCalled();

    fireEvent.blur(input!);
    expect(blurFn).toHaveBeenCalled();

    blurFn.mockClear();

    fireEvent.change(input!, { target: { value: 'gr' } });
    fireEvent.change(input!, { target: { value: 'g' } });

    fireEvent.blur(input!);
    expect(blurFn).toHaveBeenCalled();
  });

  it('should call Multi select events: change', () => {
    jest.useFakeTimers();
    const changeFn = jest.fn();

    const wrapper = renderCombobox({
      data: MS_DATA,
      onChange: changeFn
    });

    const input = wrapper.baseElement.querySelector(
      '.text-field-container .filter > input'
    );

    fireEvent.change(input!, { target: { value: 'gr' } });

    fireEvent.click(wrapper.getAllByText('GR')[0].closest('.item-checkbox')!);
    jest.runAllTimers();

    expect(changeFn).toHaveBeenCalled();
  });
});
