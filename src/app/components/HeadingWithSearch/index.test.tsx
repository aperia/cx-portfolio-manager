import { render } from '@testing-library/react';
import 'app/utils/_mockComponent/mockUseTranslation';
import React from 'react';
import { HeadingWithSearch } from '.';

const factory = (searchValue = '', preview = false) => {
  const defaultProps = {
    txtTitle: 'heading',
    searchValue,
    onSearchChange: jest.fn(),
    isDataEmpty: false,
    onClearSearch: jest.fn(),
    searchDescription: ['description'],
    preview
  };
  const { getByText } = render(<HeadingWithSearch {...defaultProps} />);

  return {
    title: () => getByText(defaultProps.txtTitle),
    clearAndReset: () => getByText('txt_clear_and_reset')
  };
};

describe('test HeadingWithSearch component', () => {
  it('should be render', () => {
    const { title } = factory();
    expect(title()).toBeInTheDocument();
  });

  it('should be render with search', () => {
    const { clearAndReset } = factory('search any thing');
    expect(clearAndReset()).toBeInTheDocument();
  });
});
