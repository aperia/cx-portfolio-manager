import SimpleSearch, { SimpleSearchPublic } from 'app/components/SimpleSearch';
import { useTranslation } from 'app/_libraries/_dls';
import ClearAndResetButton from 'pages/_commons/Utils/ClearAndResetButton';
import React, { useEffect, useRef } from 'react';

interface HeadingWithSearchProps {
  txtTitle: string;
  searchDescription: string[];
  searchDescriptionString?: string;
  searchValue: string;
  onSearchChange: (s: string) => void;
  isDataEmpty: boolean;
  onClearSearch: () => void;
  preview?: boolean;
}

const HeadingWithSearch: React.FC<HeadingWithSearchProps> = ({
  txtTitle,
  searchValue,
  onSearchChange,
  isDataEmpty,
  onClearSearch,
  searchDescription,
  searchDescriptionString,
  preview
}) => {
  const { t } = useTranslation();

  const simpleSearchRef = useRef<SimpleSearchPublic>(null);

  useEffect(() => {
    if (!searchValue && simpleSearchRef?.current) {
      simpleSearchRef.current.clear();
    }
  }, [searchValue]);

  return (
    <>
      <div className="d-flex align-items-center justify-content-between mb-16">
        <h5>{t(txtTitle)}</h5>
        {!preview && (
          <SimpleSearch
            ref={simpleSearchRef}
            placeholder={t(searchDescriptionString ?? 'txt_type_to_search')}
            onSearch={onSearchChange}
            defaultValue={searchValue}
            popperElement={
              searchDescription.length ? (
                <>
                  <>
                    <p className="color-grey">{t('txt_search_description')}</p>
                    <ul className="search-field-item list-unstyled">
                      {searchDescription.map(item => (
                        <li key={item} className="mt-16">
                          {t(item)}
                        </li>
                      ))}
                    </ul>
                  </>
                </>
              ) : undefined
            }
          />
        )}
      </div>
      {searchValue && !isDataEmpty && (
        <div className="d-flex justify-content-end mb-16 mr-n8">
          <ClearAndResetButton small onClearAndReset={onClearSearch} />
        </div>
      )}
    </>
  );
};

export { HeadingWithSearch };
