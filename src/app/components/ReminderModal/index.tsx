import ModalRegistry from 'app/components/ModalRegistry';
import { canSetupWorkflow } from 'app/helpers';
import {
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import isFunction from 'lodash.isfunction';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import React from 'react';
import { useDispatch } from 'react-redux';
import GroupTextControl from '../DofControl/GroupTextControl';

export interface IReminderModalProps {
  id: string;
  show: boolean;
  workflowTemplateId: string;
  workflowName: string;
  description: string;

  onClose?: () => void;
}

const ReminderModal: React.FC<IReminderModalProps> = ({
  id,
  show,
  workflowTemplateId,
  workflowName,
  description,

  onClose
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const handleClose = () => {
    isFunction(onClose) && onClose();
  };

  const handleRunWorkflow = () => {
    if (canSetupWorkflow(workflowName)) {
      dispatch(
        actionsWorkflowSetup.setWorkflowSetup({
          isTemplate: true,
          workflow: { name: workflowName, id: workflowTemplateId } as any
        })
      );

      handleClose();
    }
  };

  return (
    <ModalRegistry id={id} show={show} onAutoClosedAll={handleClose}>
      <ModalHeader border closeButton onHide={handleClose}>
        <ModalTitle>{t('txt_reminder_note')}</ModalTitle>
      </ModalHeader>
      <ModalBody>
        <div className="mt-n16">
          <GroupTextControl
            id={`${id}_workflowName`}
            input={{ value: workflowName } as any}
            meta={{} as any}
            options={{ truncate: false, inline: false }}
            label={t('txt_workflow_name_short')}
          />
        </div>
        <GroupTextControl
          id={`${id}_workflowDescription`}
          input={{ value: description } as any}
          meta={{} as any}
          options={{ truncate: false, inline: false, valueClass: 'pre-line' }}
          label={t('txt_description')}
        />
      </ModalBody>
      <ModalFooter
        cancelButtonText={t('txt_skip')}
        okButtonText={t('txt_set_up_workflow')}
        onCancel={handleClose}
        onOk={handleRunWorkflow}
      />
    </ModalRegistry>
  );
};

export default ReminderModal;
