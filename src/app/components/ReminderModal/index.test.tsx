import { fireEvent } from '@testing-library/react';
import ReminderModal, {
  IReminderModalProps
} from 'app/components/ReminderModal';
import * as canSetupWorkflow from 'app/helpers/canSetupWorkflow';
import {
  mockUseDispatchFnc,
  mockUseSelectorFnc,
  renderComponent
} from 'app/utils';
import React from 'react';

const mockUseSelector = mockUseSelectorFnc();
const mockUseDispatch = mockUseDispatchFnc();
const mockCanSetupWorkflow = jest.spyOn(canSetupWorkflow, 'canSetupWorkflow');

const mockProps = {
  id: 'ReminderModal',
  show: true,
  onClose: jest.fn() as any
} as IReminderModalProps;
const testId = 'ReminderModal';
describe('ReminderModal', () => {
  const createWrapper = async (...props: IReminderModalProps[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    await renderComponent(
      testId,
      <ReminderModal {...(mergeProps as IReminderModalProps)} />
    );
  };

  const it_should_render_modal = () => {
    const id = 'ReminderModal_';

    expect(
      document.getElementById(`${id}workflowName`)
    ).not.toBeEmptyDOMElement();
    expect(
      document.getElementById(`${id}workflowDescription`)
    ).not.toBeEmptyDOMElement();
  };
  const it_should_close_modal = () => {
    expect(mockProps.onClose).toBeCalled();
  };

  const then_run_workflow = () => {
    const dropdown = document.querySelector(
      '.dls-modal-footer .btn.btn-primary'
    );
    fireEvent.click(dropdown!);
  };

  const mockOpened = (opened = true) => {
    mockUseSelector.mockImplementation((selector: any) =>
      selector({
        modals: {
          registerModals: opened ? [{ name: 'ReminderModal' }] : []
        }
      })
    );
  };

  beforeEach(() => {
    mockOpened();
    mockUseDispatch.mockImplementation(() => jest.fn());
    mockCanSetupWorkflow.mockReturnValue(true);
  });

  it('should render ReminderModal', async () => {
    await createWrapper(mockProps);

    it_should_render_modal();

    then_run_workflow();
    it_should_close_modal();
  });

  it('should not close modal', async () => {
    mockCanSetupWorkflow.mockReturnValue(false);
    await createWrapper(mockProps);

    it_should_render_modal();

    then_run_workflow();
    expect(mockProps.onClose).not.toBeCalled();
  });
});
