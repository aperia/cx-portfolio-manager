import {
  fireEvent,
  render,
  RenderResult,
  waitFor
} from '@testing-library/react';
import * as helper from 'app/helpers/isDevice';
import 'app/utils/_mockComponent/mockCanvas';
import { TruncateTextProps } from 'app/_libraries/_dls';
import React from 'react';
import EnhanceDropdownList, { EnhanceDropdownListProps } from './index';

jest.mock('app/helpers/isDevice', () => ({
  __esModule: true,
  isDevice: false
}));

(function mockDOMMatrix() {
  class DOMMatrixMock {
    scale = jest.fn();
    translate = jest.fn();
  }
  global.DOMMatrix = DOMMatrixMock as any;
})();

const mockHelper: Record<string, any> = helper;

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    TruncateText: ({ children, onTruncate }: TruncateTextProps) => {
      return (
        <div>
          {children}
          <button
            onFocus={() =>
              onTruncate && onTruncate(children !== 'Mock Option 2', false)
            }
          >
            On Truncate
          </button>
        </div>
      );
    }
  };
});

// const mockHelper: any = helper;

const OPTIONS = [
  {
    text: 'Mock Option 1',
    code: 'opt1'
  },
  {
    text: 'Mock Option 2',
    code: 'opt2'
  },
  {
    text: 'Mock Option 3',
    code: 'opt3'
  }
];

describe('Test EnhanceDropdownList Component', () => {
  const mockDropdownList = {
    id: 'mock-drd-id',
    label: 'Mock label',
    options: OPTIONS
  } as EnhanceDropdownListProps;

  const renderDropdownList = (
    props?: EnhanceDropdownListProps
  ): RenderResult => {
    return render(
      <div>
        <EnhanceDropdownList
          {...mockDropdownList}
          {...props}
          tooltipElement={<div>Mock tooltip</div>}
        />
      </div>
    );
  };

  const showTooltipOnItem = (
    wrapper: RenderResult,
    itemValue: string
  ): HTMLDivElement => {
    // simulate on truncate for all item
    wrapper.queryAllByText(/On Truncate/).forEach(element => {
      fireEvent.focus(element!);
    });

    const popperTrigger = wrapper
      .queryByText(itemValue)
      ?.closest('.dls-popper-trigger') as HTMLDivElement;

    fireEvent.mouseEnter(popperTrigger);
    jest.runAllTimers();
    return popperTrigger;
  };

  it('should render Enhance DropdownList UI', () => {
    jest.useFakeTimers();
    const wrapper = renderDropdownList({} as EnhanceDropdownListProps);

    const { queryByText } = wrapper;

    expect(queryByText(/Mock label/i)).toBeInTheDocument();
    const inputDiv = wrapper.baseElement.querySelector(
      '.text-field-container > .input'
    ) as HTMLDivElement;

    fireEvent.focus(inputDiv);
    jest.runAllTimers();

    expect(queryByText(/Mock Option 1/i)).toBeInTheDocument();
    expect(queryByText(/Mock tooltip/i)).toBeInTheDocument();
  });

  it('should render Enhance DropdownList UI on desktop with custom item tooltip', () => {
    jest.useFakeTimers();
    mockHelper.isDevice = false;
    const wrapper = renderDropdownList({
      tooltipItemCustom: [
        {
          code: 'opt1',
          tooltipItem: <div>Mock Option 1 tooltip</div>
        }
      ]
    } as EnhanceDropdownListProps);

    const { queryByText } = wrapper;

    const inputDiv = wrapper.baseElement.querySelector(
      '.text-field-container > .input'
    ) as HTMLDivElement;

    fireEvent.focus(inputDiv);
    jest.runAllTimers();

    expect(queryByText(/Mock tooltip/i)).toBeInTheDocument();

    const popperTrigger = showTooltipOnItem(wrapper, 'Mock Option 1');

    expect(queryByText(/Mock Option 1 tooltip/i)).toBeInTheDocument();

    fireEvent.mouseLeave(popperTrigger);
    expect(queryByText(/Mock Option 1 tooltip/i)).toBeNull();
  });

  it('should render Enhance DropdownList UI on mobile > disable item tooltip', () => {
    jest.useFakeTimers();
    mockHelper.isDevice = true;
    const wrapper = renderDropdownList({
      tooltipItemCustom: [
        {
          code: 'opt1',
          tooltipItem: <div>Mock Option 1 tooltip</div>
        }
      ]
    } as EnhanceDropdownListProps);

    const { queryByText } = wrapper;

    const inputDiv = wrapper.baseElement.querySelector(
      '.text-field-container > .input'
    ) as HTMLDivElement;

    fireEvent.focus(inputDiv);
    jest.runAllTimers();

    expect(queryByText(/Mock tooltip/i)).toBeNull();

    showTooltipOnItem(wrapper, 'Mock Option 1');

    expect(queryByText(/Mock Option 1 tooltip/i)).toBeInTheDocument();
  });

  it('should render Enhance DropdownList UI on mobile > disable item tooltip', async () => {
    jest.useFakeTimers();
    mockHelper.isDevice = true;
    const wrapper = renderDropdownList({
      popupBaseProps: {
        placement: 'top-start'
      }
    } as EnhanceDropdownListProps);

    const inputDiv = wrapper.baseElement.querySelector(
      '.text-field-container > .input'
    ) as HTMLDivElement;

    fireEvent.focus(inputDiv);
    jest.runAllTimers();

    await waitFor(() => {
      const popupDiv = wrapper.baseElement.querySelector(
        '.dls-popup'
      ) as HTMLDivElement;
      expect(popupDiv).toBeInTheDocument();
      expect(popupDiv.getAttribute('data-popper-placement')).toContain('top');
    });

    showTooltipOnItem(wrapper, 'Mock Option 1');
  });

  it('should render Enhance DropdownList UI on mobile > disable item tooltip bottom-start', async () => {
    jest.useFakeTimers();
    mockHelper.isDevice = true;
    const wrapper = renderDropdownList({
      popupBaseProps: {
        placement: 'bottom-start'
      }
    } as EnhanceDropdownListProps);

    const inputDiv = wrapper.baseElement.querySelector(
      '.text-field-container > .input'
    ) as HTMLDivElement;

    fireEvent.focus(inputDiv);
    jest.runAllTimers();

    await waitFor(() => {
      const popupDiv = wrapper.baseElement.querySelector(
        '.dls-popup'
      ) as HTMLDivElement;
      expect(popupDiv).toBeInTheDocument();
    });

    showTooltipOnItem(wrapper, 'Mock Option 2');
    showTooltipOnItem(wrapper, 'Mock Option 2');
  });
});
