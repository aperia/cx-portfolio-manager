import FieldTooltip from 'app/components/FieldTooltip';
import { classnames, isDevice } from 'app/helpers';
import {
  DropdownList,
  DropdownListProps,
  DropdownListRef,
  TruncateText
} from 'app/_libraries/_dls';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import React, { useEffect, useMemo, useRef, useState } from 'react';

const NONE_OPTION = 'none';

export interface ItemCustomProps {
  code: string;
  tooltipItem: React.ReactNode;
}
export interface EnhanceDropdownListProps
  extends Omit<
    DropdownListProps,
    'children' | 'textField' | 'opened' | 'onVisibilityChange'
  > {
  options: RefData[];
  tooltipElement?: React.ReactNode;
  tooltipItemCustom?: ItemCustomProps[];
  tooltipPlacementSide?: 'left' | 'right';
}
const EnhanceDropdownList: React.FC<EnhanceDropdownListProps> = ({
  options: optionsProp,
  tooltipPlacementSide = 'right',
  tooltipElement,
  tooltipItemCustom = [],
  value: valueProp,
  ...rest
}) => {
  const dropdownRef = useRef<DropdownListRef>(null);

  const [showTooltipForItems, setShowTooltipForItems] = useState(
    {} as Record<string, boolean>
  );

  const hasNoneOption = useMemo(
    () => optionsProp?.some(o => o.text?.toLowerCase() === NONE_OPTION),
    [optionsProp]
  );
  const [hideNoneValue, setHideNoneValue] = useState(
    hasNoneOption && !valueProp?.code
  );
  useEffect(() => {
    return () => {
      setHideNoneValue(false);
    };
  }, [valueProp]);

  const value = useMemo(
    () => (hideNoneValue ? undefined : valueProp),
    [hideNoneValue, valueProp]
  );

  const showFieldTooltip = useMemo(() => !!tooltipElement, [tooltipElement]);
  const dropdownItemSide = useMemo(
    () => (tooltipPlacementSide === 'left' ? 'right' : 'left'),
    [tooltipPlacementSide]
  );

  const options = useMemo(
    () =>
      optionsProp.map(itemValue => {
        const { code, text } = itemValue;
        const tooltipCustom = tooltipItemCustom.find(i => i.code === code);
        const tooltip = tooltipCustom?.tooltipItem || text;

        return (
          <DropdownList.Item
            key={code}
            variant="custom"
            tooltipProps={{
              opened: !showTooltipForItems[code] ? false : undefined,
              placement: dropdownItemSide,
              triggerClassName: 'DropdownList__tooltip-item-trigger d-block',
              tooltipInnerClassName: 'DropdownList__tooltip-item',
              containerClassName: classnames(
                'DropdownList__tooltip-container',
                showFieldTooltip &&
                  'DropdownList__tooltip-container--with-field-tooltip'
              ),
              displayOnClick: isDevice,
              element: tooltip
            }}
            value={itemValue}
          >
            <TruncateText
              lines={2}
              onTruncate={isTruncated =>
                setShowTooltipForItems(values => {
                  if (values[code] === isTruncated) return values;

                  return {
                    ...values,
                    [code]: isTruncated
                  };
                })
              }
              resizable
            >
              {text}
            </TruncateText>
          </DropdownList.Item>
        );
      }),
    [
      optionsProp,
      tooltipItemCustom,
      showFieldTooltip,
      dropdownItemSide,
      showTooltipForItems
    ]
  );

  if (!showFieldTooltip)
    return (
      <DropdownList
        {...rest}
        value={value}
        ref={dropdownRef}
        textFieldRender={value => (
          <span
            className={classnames('text-truncate', {
              'form-group-static__text': isEmpty(value.text)
            })}
          >
            {value.text}
          </span>
        )}
      >
        {options}
      </DropdownList>
    );

  return (
    <FieldTooltip
      keepShowOnDevice={false}
      popoverInnerClassName="DropdownList__tooltip"
      placement={tooltipPlacementSide}
      element={tooltipElement}
    >
      <DropdownList
        {...rest}
        value={value}
        ref={dropdownRef}
        textFieldRender={value => (
          <span
            className={classnames('text-truncate', {
              'form-group-static__text': isEmpty(value.text)
            })}
          >
            {value.text}
          </span>
        )}
      >
        {options}
      </DropdownList>
    </FieldTooltip>
  );
};

export default EnhanceDropdownList;
