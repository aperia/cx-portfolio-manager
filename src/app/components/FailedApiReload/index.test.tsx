import { fireEvent, RenderResult } from '@testing-library/react';
import FailedApiReload, {
  FailedApiReloadProps
} from 'app/components/FailedApiReload';
import { renderComponent } from 'app/utils';
import React from 'react';

describe('Test FailedApiReload component', () => {
  const idDomTest = 'test';
  const defaultProps: FailedApiReloadProps = {
    id: '1',
    className: 'class',
    dataLoadText: undefined,
    onReload: jest.fn()
  };

  let renderResult: RenderResult;
  const createWrapper = async (...props: FailedApiReloadProps[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    renderResult = await renderComponent(
      idDomTest,
      <FailedApiReload {...defaultProps} {...mergeProps} />
    );
  };

  it('Render component', async () => {
    await createWrapper();
    const { getByText } = renderResult;
    expect(
      getByText(/Data load unsuccessful. Click Reload to try again./i)
    ).toBeInTheDocument();
    expect(getByText(/Failure ocurred on/i)).toBeInTheDocument();
  });

  it('test on reload btn clicked', async () => {
    const props: any = { dataLoadText: 'loadText', className: undefined };
    await createWrapper({ ...props });
    const { getByText } = renderResult;
    fireEvent.click(getByText(/Reload/i));
    expect(defaultProps.onReload).toHaveBeenCalled();
  });
});
