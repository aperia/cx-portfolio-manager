import { Button } from 'app/_libraries/_dls';
import classNames from 'classnames';
import { format } from 'date-and-time';
import React, { FC } from 'react';

export interface FailedApiReloadProps {
  id: string;
  className?: string;
  dataLoadText?: string;
  onReload: () => void;
}
const FailedApiReload: FC<FailedApiReloadProps> = ({
  id,
  className = 'mt-24',
  onReload,
  dataLoadText = 'Data load unsuccessful. Click Reload to try again.'
}) => {
  return (
    <div className={classNames('text-center', className)}>
      <p className="color-grey-d20">{dataLoadText}</p>
      <p className="fs-12 color-grey mt-4">
        Failure ocurred on {format(new Date(), 'MM/DD/YYYY hh:mm:ss A')}
      </p>
      <Button
        id={`${id}_Button`}
        onClick={onReload}
        className="mt-24"
        size="sm"
      >
        Reload
      </Button>
    </div>
  );
};

export default FailedApiReload;
