import { fireEvent } from '@testing-library/dom';
import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React from 'react';
import MoreAction, { MoreActionProps } from '.';

const renderComponent = (props: MoreActionProps) => {
  return render(<MoreAction {...props} />);
};

describe('MoreAction', () => {
  it('Render Null', () => {
    const wrapper = renderComponent({ dataTestId: 'null', actions: [] });
    expect(
      wrapper.queryByTestId('actionId_dls-button')
    ).not.toBeInTheDocument();
  });
  it('Render 1 Action', () => {
    const onSelectMock = jest.fn();
    const actionsMock = [{ label: 'label', uniqueId: 'id', disabled: false }];
    const wrapper = renderComponent({
      dataTestId: 'actionId',
      actions: actionsMock,
      onSelect: onSelectMock
    });
    expect(wrapper.getByTestId('actionId_dls-button')).toBeInTheDocument();
    fireEvent.click(wrapper.getByTestId('actionId_dls-button'));
    expect(onSelectMock).toBeCalledWith(actionsMock[0]);
  });
  it('Render more than 1 Action', async () => {
    const onSelectMock = jest.fn();
    const actionsMock = [
      { label: 'label1', uniqueId: 'id1', disabled: false },
      { label: 'label2', uniqueId: 'id2', disabled: false }
    ];
    const wrapper = renderComponent({
      dataTestId: 'moreActions',
      actions: actionsMock,
      onSelect: onSelectMock
    });

    expect(wrapper.getByTestId('moreActions_dls-button')).toBeInTheDocument();
    userEvent.click(wrapper.getByTestId('moreActions_dls-button'));
    expect(wrapper.getByText('label1')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('label1'));
    expect(onSelectMock).toBeCalledWith(actionsMock[0]);
  });
});
