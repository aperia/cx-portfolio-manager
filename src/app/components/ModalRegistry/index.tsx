import { useModalRegistry } from 'app/hooks';
import { Modal, ModalProps } from 'app/_libraries/_dls';
import { AnimationProps } from 'app/_libraries/_dls/components/Animations';
import isFunction from 'lodash.isfunction';
import React, { useEffect, useRef } from 'react';

export interface ModalRegistryProps extends ModalProps {
  id: string;
  keepMount?: boolean;
  onAutoClosed?: () => void;
  onAutoOpened?: () => void;
  onAutoClosedAll?: () => void;
  animationComponent?: React.ElementType;
  animationComponentProps?: AnimationProps;
}

const ModalRegistry: React.FC<ModalRegistryProps> = ({
  id,
  show,
  keepMount = true,
  onAutoClosed,
  onAutoOpened,
  onAutoClosedAll,
  full,
  ...props
}) => {
  const [isCurrentOpened, isAllClosed] = useModalRegistry(id, show, full);
  const firstRenderRef = useRef<boolean>(true);
  const funcRef = useRef<Record<string, any>>({ preOpened: isCurrentOpened });
  const preOpenedRef = useRef<boolean>(isCurrentOpened);

  funcRef.current.handleAutoClosed = onAutoClosed;
  funcRef.current.handleAutoOpened = onAutoOpened;
  funcRef.current.handleAutoClosedAll = onAutoClosedAll;

  useEffect(
    () => () => {
      preOpenedRef.current = isCurrentOpened;
    },
    [isCurrentOpened]
  );

  useEffect(() => {
    if (firstRenderRef.current && !isCurrentOpened) {
      firstRenderRef.current = false;
      return;
    }

    const { handleAutoClosed, handleAutoOpened } = funcRef.current;

    if (isCurrentOpened) {
      isFunction(handleAutoClosed) && handleAutoClosed();
    } else {
      isFunction(handleAutoOpened) && handleAutoOpened();
    }
  }, [isCurrentOpened]);

  useEffect(() => {
    if (!(isAllClosed && preOpenedRef.current)) {
      return;
    }

    const { handleAutoClosedAll } = funcRef.current;
    isFunction(handleAutoClosedAll) && handleAutoClosedAll();
  }, [isAllClosed]);

  if (!keepMount && !isCurrentOpened) {
    return <React.Fragment />;
  }

  return <Modal show={isCurrentOpened} full={full} {...props} />;
};

export default ModalRegistry;
