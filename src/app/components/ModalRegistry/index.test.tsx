import ModalRegistry, {
  ModalRegistryProps
} from 'app/components/ModalRegistry';
import * as Modals from 'app/hooks/useModals';
import { renderComponent } from 'app/utils';
import React from 'react';

const mockUseModalRegistry = jest.spyOn(Modals, 'useModalRegistry');

const onAutoOpened = jest.fn();
const onAutoClosed = jest.fn();
const mockData: ModalRegistryProps = {
  id: 'modalRegistryId',
  show: true,
  onAutoClosed,
  onAutoOpened
};

const testId = 'modalRegistry';
describe('ModalRegistry', () => {
  const createWrapper = async (...props: ModalRegistryProps[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    await renderComponent(testId, <ModalRegistry {...(mergeProps as any)} />);
  };

  const mockUseModalRegistryImplement = (show: boolean) => {
    mockUseModalRegistry.mockImplementation(() => [show, true]);
  };

  afterEach(() => {
    mockUseModalRegistry.mockClear();
  });

  const it_should_show_Modal = (show = true) => {
    expect(document.body.querySelectorAll('.dls-modal-root').length).toEqual(
      show ? 1 : 0
    );
  };

  it('should show Modal', async () => {
    mockUseModalRegistryImplement(true);
    await createWrapper(mockData);

    it_should_show_Modal();
  });

  it('should NOT show Modal', async () => {
    mockUseModalRegistryImplement(false);
    await createWrapper(mockData, { keepMount: false } as any);

    it_should_show_Modal(false);
  });
});
