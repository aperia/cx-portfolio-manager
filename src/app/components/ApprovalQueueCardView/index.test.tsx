import { fireEvent, RenderResult } from '@testing-library/react';
import ApprovalQueueCardView, {
  ApprovalQueueCardViewProps
} from 'app/components/ApprovalQueueCardView';
import { renderComponent } from 'app/utils';
import { App as DOFApp } from 'app/_libraries/_dof/core';
import React from 'react';

const mockData: any = {
  id: '56267021',
  changeId: '56267021',
  changeName: 'Sepynedu Vymashevaepyzhe',
  changeOwner: 'Hujykoxyhaewu Renecyli',
  effectiveDate: '2021-01-22T13:46:44.7501061+07:00',
  approvalDate: '2021-01-22T13:46:44.7501766+07:00',
  changeType: 'Pricing',
  changeStatus: 'Draft',
  portfoliosImpacted: 1757008739,
  pricingStrategiesCreated: 341252882,
  pricingMethodsCreated: 1896957364,
  cardholdersAccountsImpacted: 938822807,
  changeInTermsStatus: 'Not Started'
};

const testId = 'approvalQueueCardView';
describe('ApprovalQueueCardView', () => {
  let renderResult: RenderResult;

  const createWrapper = async (...props: ApprovalQueueCardViewProps[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    renderResult = await renderComponent(
      testId,
      <DOFApp urlConfigs={[]}>
        <ApprovalQueueCardView {...(mergeProps as any)} />
      </DOFApp>
    );
  };

  const it_should_render_pending_approval = async (isListView: boolean) => {
    const wrapper = await renderResult.findByTestId(testId);

    expect(
      wrapper.querySelector(`#approval-queue-card-data-${mockData.id}`)
    ).toBeTruthy();

    const expectation = expect(wrapper.querySelector('.box-list-view'));
    isListView ? expectation.not.toBeTruthy() : expectation.toBeTruthy();
  };

  const it_should_render_empty_pending_approval = async () => {
    const wrapper = await renderResult.findByTestId(testId);

    expect(
      wrapper.querySelector('#approval-queue-card-data-empty')
    ).toBeTruthy();
  };

  it('should show pending approval as listView', async () => {
    await createWrapper({
      id: mockData.id,
      value: mockData
    });

    await it_should_render_pending_approval(true);
  });

  it('should show pending approval as gridView', async () => {
    await createWrapper({
      id: mockData.id,
      value: mockData,
      isListViewMode: false
    });

    await it_should_render_pending_approval(false);
  });

  it('should show empty pending approval', async () => {
    await createWrapper({} as any);

    await it_should_render_empty_pending_approval();
  });

  it('should show pending approval as expand > click button to collaspe', async () => {
    const toggleFn = jest.fn();

    await createWrapper({
      id: mockData.id,
      value: mockData,
      isExpand: true,
      onToggle: toggleFn
    });

    const wrapper = await renderResult.findByTestId(testId);
    expect(wrapper.querySelector(`.bg-light-l16`)).toBeInTheDocument();

    fireEvent.click(
      wrapper.querySelector('.change-card-expand')!.querySelector('i.icon')!
    );
    expect(toggleFn).toHaveBeenCalled();
  });

  it('should show pending approval as expand with undefined ID', async () => {
    const toggleFn = jest.fn();

    await createWrapper({
      id: undefined as any,
      value: mockData,
      isExpand: true,
      onToggle: toggleFn
    });

    const wrapper = await renderResult.findByTestId(testId);
    expect(
      wrapper.querySelector(`#approval-queue-card-data-empty`)
    ).toBeInTheDocument();
    expect(wrapper.querySelector(`.bg-light-l16`)).toBeInTheDocument();
  });
});
