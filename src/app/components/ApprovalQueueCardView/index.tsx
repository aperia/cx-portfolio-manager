import { classnames } from 'app/helpers';
import { Button, Icon, Tooltip, useTranslation } from 'app/_libraries/_dls';
import { View } from 'app/_libraries/_dof/core';
import React, { useCallback, useMemo } from 'react';

export interface ApprovalQueueCardViewProps {
  id: string;
  value: IApprovalQueueCardView;
  isListViewMode?: boolean;
  isExpand?: boolean;
  onClick?: () => void;
  onToggle?: (changeId: string) => void;
}
const ApprovalQueueCardView: React.FC<ApprovalQueueCardViewProps> = ({
  id,
  value,
  isListViewMode = true,
  isExpand,
  onClick,
  onToggle
}) => {
  const { t } = useTranslation();

  const handleToggleCard = useCallback(
    (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
      e.stopPropagation();
      onToggle && onToggle(value.changeId);
    },
    [onToggle, value?.changeId]
  );

  const updateValue = useMemo(
    () => ({ ...value, actionPayload: value }),
    [value]
  );

  const viewElement = useMemo(
    () => (
      <div className="list-view">
        <View
          id={`approval-queue-card-view-header__${id || 'empty'}`}
          formKey={`approval-queue-card-view-header__${id || 'empty'}`}
          descriptor="change-card-view-header"
          value={updateValue}
        />
        {isExpand && (
          <div className="bg-light-l16 mt-8 pt-4 mx-n16 mb-n8 rounded-bottom-8">
            <div className="mx-16 pb-8">
              <View
                id={`approval-queue-card-view__${id || 'empty'}`}
                formKey={`approval-queue-card-view__${id || 'empty'}`}
                descriptor="change-card-view"
                value={updateValue}
              />
            </div>
          </div>
        )}
        <div className="change-card-expand">
          <Tooltip
            element={isExpand ? t('txt_collapse') : t('txt_expand')}
            variant="primary"
            placement="top"
          >
            <Button
              size="sm"
              variant="icon-secondary"
              onClick={handleToggleCard}
            >
              <Icon name={isExpand ? 'minus' : 'plus'} size="4x" />
            </Button>
          </Tooltip>
        </div>
      </div>
    ),
    [id, updateValue, isExpand, handleToggleCard, t]
  );

  return (
    <div
      id={`approval-queue-card-data-${id || 'empty'}`}
      className={classnames(
        'mt-12 ',
        isListViewMode ? 'col-12' : 'col-12 box-list-view masonry-item'
      )}
      onClick={onClick}
    >
      {viewElement}
    </div>
  );
};

export default ApprovalQueueCardView;
