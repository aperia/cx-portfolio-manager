import { fireEvent, render, RenderResult } from '@testing-library/react';
import { mockUseDispatchFnc, mockUseModalRegistryFnc } from 'app/utils';
import React from 'react';
import ConfirmationModal, { ConfirmationModalProps } from './index';

const mockUseModalRegistry = mockUseModalRegistryFnc();
const mockUseDispatch = mockUseDispatchFnc();

describe('Test ConfirmationModal Component', () => {
  const mockDispatch = jest.fn();
  const confirmationModalProps = {
    id: 'mock-id',
    show: true,
    modalTitle: 'Mock title',
    modalContent: <div>Mock content</div>
  } as ConfirmationModalProps;

  const renderConfirmationModal = (
    props?: ConfirmationModalProps
  ): RenderResult => {
    return render(
      <div>
        <ConfirmationModal {...confirmationModalProps} {...props} />
      </div>
    );
  };

  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);

    mockUseModalRegistry.mockImplementation(() => [true, false]);
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
    mockUseModalRegistry.mockClear();
  });

  it('should render ConfirmationModal UI > empty button, with loading', async () => {
    const onCancelFn = jest.fn();
    const wrapper = renderConfirmationModal({
      ...confirmationModalProps,
      loading: true,
      onCancel: onCancelFn
    });

    expect(wrapper.queryByText('Mock title')).toBeInTheDocument();
    expect(wrapper.queryByText('Mock content')).toBeInTheDocument();
    expect(wrapper.queryByText('txt_close')).toBeInTheDocument();
    expect(
      wrapper.baseElement.querySelector('.modal-content')?.className
    ).toContain('loading');

    fireEvent.click(wrapper.queryByText('txt_close')!);

    expect(onCancelFn).toHaveBeenCalled();
  });

  it('should render ConfirmationModal UI > with buttons config', async () => {
    const onCancelFn = jest.fn();
    const onConfirmFn = jest.fn();
    const wrapper = renderConfirmationModal({
      ...confirmationModalProps,
      modalButtons: {
        confirmText: 'Confirm mock',
        confirmVariant: 'secondary',
        cancelText: 'Cancel mock',
        cancelVariant: 'danger'
      },
      onCancel: onCancelFn,
      onConfirm: onConfirmFn
    });

    expect(wrapper.queryByText('Confirm mock')).toBeInTheDocument();
    expect(wrapper.queryByText('Cancel mock')).toBeInTheDocument();
    expect(
      wrapper.baseElement.querySelector('.modal-content')?.className
    ).not.toContain('loading');

    fireEvent.click(wrapper.queryByText('Confirm mock')!);

    expect(onConfirmFn).toHaveBeenCalled();

    fireEvent.click(wrapper.queryByText('Cancel mock')!);

    expect(onCancelFn).toHaveBeenCalled();
  });
});
