import ModalRegistry from 'app/components/ModalRegistry';
import {
  BtnVariants,
  Button,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import React from 'react';

export interface ConfirmationModalProps {
  id: string;
  show: boolean;
  loading?: boolean;
  modalTitle: string;
  modalContent: string | React.ReactNode;
  modalButtons?: {
    confirmText: string;
    confirmVariant: BtnVariants;
    cancelText: string;
    cancelVariant: BtnVariants;
  };
  onCancel?: () => void;
  onConfirm?: () => void;
}

const ConfirmationModal: React.FC<ConfirmationModalProps> = ({
  id,
  show,
  loading,
  modalTitle,
  modalContent,
  modalButtons,
  onCancel,
  onConfirm
}) => {
  const { t } = useTranslation();

  return (
    <ModalRegistry
      id={`confirmation-modal-${id}`}
      show={show}
      classes={{ content: loading ? 'loading' : '' }}
      onAutoClosedAll={onCancel}
    >
      <ModalHeader border closeButton onHide={onCancel}>
        <ModalTitle>{modalTitle}</ModalTitle>
      </ModalHeader>
      <ModalBody>
        <div>{modalContent}</div>
      </ModalBody>
      {modalButtons ? (
        <div className="dls-modal-footer modal-footer">
          <Button variant={modalButtons.cancelVariant} onClick={onCancel}>
            {modalButtons.cancelText}
          </Button>
          <Button variant={modalButtons.confirmVariant} onClick={onConfirm}>
            {modalButtons.confirmText}
          </Button>
        </div>
      ) : (
        <ModalFooter
          border
          cancelButtonText={t('txt_close')}
          onCancel={onCancel}
        />
      )}
    </ModalRegistry>
  );
};

export default ConfirmationModal;
