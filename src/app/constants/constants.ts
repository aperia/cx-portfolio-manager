import { SortType } from 'app/_libraries/_dls';
import orderBy from 'lodash.orderby';
import {
  ActivitySortByFields,
  AddAttributeButtons,
  AddMethodButtons,
  AddTableButtons,
  ChangeManagementSortByFields,
  DMMTablesStateSortByFields,
  OrderBy,
  PendingApprovalQueuesSortByFields,
  PortfoliosSortByFields,
  PricingMethodsSortByFields,
  PricingStrategiesSortByFields,
  WorkflowMethodsSortByFields,
  WorkflowSortByFields,
  WorkflowTemplateSortByFields,
  WorkflowTemplateType,
  WorkFlowTLPCASortByFields
} from './enums';
export const MIN_WIDTH_PC = 1024;
export const PAGE_SIZE = [10, 25, 50];
export const LOAD_MORE_PAGE_SIZE = 30;
export const FIRST_PAGE = 1;
export const DEFAULT_FILTER_VALUE: IDataFilter = {
  page: FIRST_PAGE,
  pageSize: PAGE_SIZE[0],
  searchValue: '',
  searchFields: '',
  sortBy: [],
  orderBy: []
};

export const SORT_PRICING_METHODS: SortType = {
  id: 'name',
  order: 'asc'
};

export const PORTFOLIOS_CRITERIA_FIELDS = [
  PortfoliosSortByFields.CLIENT_ID,
  PortfoliosSortByFields.SPA,
  PortfoliosSortByFields.PORTFOLIO_ID
];

export const PORTFOLIOS_SORT_BY_LIST = orderBy(
  [
    {
      value: PortfoliosSortByFields.ACTIVE_ACCOUNTS,
      description: 'Active Accounts'
    },
    {
      value: PortfoliosSortByFields.AVAILABLE_ACCOUNTS,
      description: 'Available Accounts'
    },
    {
      value: PortfoliosSortByFields.AVAILABLE_CREDIT,
      description: 'Available Credit'
    },
    {
      value: PortfoliosSortByFields.CLIENT_ID,
      description: 'Client ID'
    },
    {
      value: PortfoliosSortByFields.DELINQUENCY,
      description: 'Delq Accounts'
    },
    {
      value: PortfoliosSortByFields.NET_INCOME,
      description: 'Net Income'
    },
    {
      value: PortfoliosSortByFields.PORTFOLIO_NEW,
      description: 'New Acct Growth'
    },
    {
      value: PortfoliosSortByFields.NUMBER_OF_PRICING_STRATEGIES,
      description: 'Number of Pricing Strategies'
    },
    {
      value: PortfoliosSortByFields.PAYMENT,
      description: 'Payments'
    },
    {
      value: PortfoliosSortByFields.PORTFOLIO_ID,
      description: 'Portfolio ID'
    },
    {
      value: PortfoliosSortByFields.PORTFOLIO_NAME,
      description: 'Portfolio Name'
    },
    {
      value: PortfoliosSortByFields.SPA,
      description: 'SPA'
    }
  ],
  ['description'],
  ['asc']
);

export const CHANGE_MANAGEMENT_SORT_BY_LIST = orderBy(
  [
    {
      value: ChangeManagementSortByFields.CHANGE_ID,
      description: 'Change ID'
    },
    {
      value: ChangeManagementSortByFields.CHANGE_NAME,
      description: 'Change Name'
    },
    {
      value: ChangeManagementSortByFields.CHANGE_OWNER,
      description: 'Change Owner'
    },
    {
      value: ChangeManagementSortByFields.CHANGE_STATUS,
      description: 'Change Status'
    },
    {
      value: ChangeManagementSortByFields.CHANGE_TYPE,
      description: 'Change Type'
    },
    {
      value: ChangeManagementSortByFields.CREATED_DATE,
      description: 'Created Date'
    },
    {
      value: ChangeManagementSortByFields.EFFECTIVE_DATE,
      description: 'Effective Date'
    },
    {
      value: ChangeManagementSortByFields.LAST_APPROVAL_BY,
      description: 'Last Approved By'
    },
    {
      value: ChangeManagementSortByFields.LAST_APPROVAL_DAY,
      description: 'Last Approved Date'
    }
  ],
  ['description'],
  ['asc']
);

export const EXISTING_CHANGE_SORT_BY_LIST = orderBy(
  [
    {
      value: ChangeManagementSortByFields.CHANGE_ID,
      description: 'Change ID'
    },
    {
      value: ChangeManagementSortByFields.CHANGE_NAME,
      description: 'Change Name'
    },
    {
      value: ChangeManagementSortByFields.CHANGE_OWNER,
      description: 'Change Owner'
    },
    {
      value: ChangeManagementSortByFields.CREATED_DATE,
      description: 'Created Date'
    },
    {
      value: ChangeManagementSortByFields.EFFECTIVE_DATE,
      description: 'Effective Date'
    }
  ],
  ['description'],
  ['asc']
);

export const PRICING_STRATEGIES_SORT_BY_LIST = [
  {
    value: PricingStrategiesSortByFields.METHOD_UPDATED,
    description: 'Methods Updated'
  },
  {
    value: PricingStrategiesSortByFields.MODELED_FROM,
    description: 'Modeled From'
  },
  {
    value: PricingStrategiesSortByFields.PRICING_STRATEGY_NAME,
    description: 'Pricing Strategy Name'
  }
];

export const WORKFLOWS_SORT_BY_LIST = [
  {
    value: WorkflowSortByFields.FIRST_RAN,
    description: 'First Executed'
  },
  {
    value: WorkflowSortByFields.FIRST_RAN_BY,
    description: 'First Executed By'
  },
  {
    value: WorkflowSortByFields.LAST_UPDATED,
    description: 'Last Updated'
  },
  {
    value: WorkflowSortByFields.LAST_UPDATED_BY,
    description: 'Last Updated By'
  },
  {
    value: WorkflowSortByFields.STATUS,
    description: 'Status'
  },
  {
    value: WorkflowSortByFields.WORKFLOW_NAME,
    description: 'Workflow Name'
  }
];

export const WORKFLOWS_TEMPLATE_SORT_BY_LIST = [
  {
    value: WorkflowTemplateSortByFields.LAST_EXECUTED,
    description: 'Last Executed'
  },
  {
    value: WorkflowTemplateSortByFields.NAME,
    description: 'Name'
  },
  {
    value: WorkflowTemplateSortByFields.NUMBER_OF_EXCUTIONS,
    description: 'Number of Executions'
  },
  {
    value: WorkflowTemplateSortByFields.TYPE,
    description: 'Type'
  }
];

export const INPROGRESS_WORKFLOWS_SORT_BY_LIST = [
  {
    value: WorkflowSortByFields.CHANGE_ID,
    description: 'Change ID'
  },
  {
    value: WorkflowSortByFields.CHANGE_NAME,
    description: 'Change Name'
  },
  {
    value: WorkflowSortByFields.FIRST_RAN_BY,
    description: 'Created By'
  },
  {
    value: WorkflowSortByFields.FIRST_RAN,
    description: 'Created Date'
  },
  {
    value: WorkflowSortByFields.LAST_UPDATED,
    description: 'Last Updated'
  },
  {
    value: WorkflowSortByFields.LAST_UPDATED_BY,
    description: 'Last Updated By'
  },
  {
    value: WorkflowSortByFields.NUMBER_OF_ATTACHMENTS,
    description: 'Number of Attachments'
  },
  {
    value: WorkflowSortByFields.TYPE,
    description: 'Type'
  },
  {
    value: WorkflowSortByFields.WORKFLOW_NAME,
    description: 'Workflow Name'
  }
];

export const PENDING_APPROVAL_QUEUES_SORT_BY_LIST = [
  {
    value: PendingApprovalQueuesSortByFields.CHANGE_ID,
    description: 'Change ID'
  },
  {
    value: PendingApprovalQueuesSortByFields.CHANGE_NAME,
    description: 'Change Name'
  },
  {
    value: PendingApprovalQueuesSortByFields.CHANGE_OWNER,
    description: 'Change Owner'
  },
  {
    value: PendingApprovalQueuesSortByFields.CHANGE_STATUS,
    description: 'Change Status'
  },
  {
    value: PendingApprovalQueuesSortByFields.CHANGE_TYPE,
    description: 'Change Type'
  },
  {
    value: PendingApprovalQueuesSortByFields.CREATED_DATE,
    description: 'Created Date'
  },
  {
    value: PendingApprovalQueuesSortByFields.EFFECTIVE_DATE,
    description: 'Effective Date'
  },
  {
    value: PendingApprovalQueuesSortByFields.LAST_APPROVAL_BY,
    description: 'Last Approved By'
  },
  {
    value: PendingApprovalQueuesSortByFields.LAST_APPROVAL_DATE,
    description: 'Last Approved Date '
  }
];

export const PRICING_METHODS_SORT_BY_LIST = orderBy(
  [
    {
      value: PricingMethodsSortByFields.METHOD,
      description: 'Method'
    },
    {
      value: PricingMethodsSortByFields.MODELED_FROM,
      description: 'Modeled From'
    },
    {
      value: PricingMethodsSortByFields.PARAMETERS_UPDATED,
      description: 'Parameters Updated'
    },
    {
      value: PricingMethodsSortByFields.PRICING_METHOD_NAME,
      description: 'Pricing Method Name'
    },
    {
      value: PricingMethodsSortByFields.NEW_VERSION_OF,
      description: 'New Version Of'
    },
    {
      value: PricingMethodsSortByFields.STRATEGIES_IMPACTED_COUNT,
      description: 'Strategies Impacted'
    }
  ],
  ['description'],
  ['asc']
);

export const DMM_TABLES_SORT_BY_LIST = orderBy(
  [
    {
      value: DMMTablesStateSortByFields.TABLE_NAME,
      description: 'Table Name'
    },
    {
      value: DMMTablesStateSortByFields.TABLE_ID,
      description: 'Table ID'
    },
    {
      value: DMMTablesStateSortByFields.TYPE,
      description: 'Type'
    }
  ],
  ['description'],
  ['asc']
);

export const WORKFLOW_METHODS_SORT_BY_LIST_IMPACTED = orderBy(
  [
    {
      value: WorkflowMethodsSortByFields.EFFECTIVE_DATE,
      description: 'Effective Date'
    },
    {
      value: WorkflowMethodsSortByFields.NAME,
      description: 'Method Name'
    },
    {
      value: WorkflowMethodsSortByFields.IMPACTEDPRICINGSTRATEGY,
      description: 'Impacted Pricing Strategies'
    },
    {
      value: WorkflowMethodsSortByFields.NUMBEROFVERSION,
      description: 'Number of Versions'
    }
  ],
  ['description'],
  ['asc']
);

export const WORKFLOW_METHODS_SORT_BY_LIST_RELATED = orderBy(
  [
    {
      value: WorkflowMethodsSortByFields.EFFECTIVE_DATE,
      description: 'Effective Date'
    },
    {
      value: WorkflowMethodsSortByFields.NAME,
      description: 'Method Name'
    },
    {
      value: WorkflowMethodsSortByFields.RELATEDPRICINGSTRATEGY,
      description: 'Related Pricing Strategies'
    },
    {
      value: WorkflowMethodsSortByFields.NUMBEROFVERSION,
      description: 'Number of Versions'
    }
  ],
  ['description'],
  ['asc']
);

export const WORKFLOW_METHODS_SORT_BY_LIST_FOR_DESIGN_LOAN_OFFERS = orderBy(
  [
    {
      value: WorkflowMethodsSortByFields.EFFECTIVE_DATE,
      description: 'Effective Date'
    },
    {
      value: WorkflowMethodsSortByFields.NAME,
      description: 'Method Name'
    },
    {
      value: WorkflowMethodsSortByFields.NUMBEROFVERSION,
      description: 'Number of Versions'
    }
  ],
  ['description'],
  ['asc']
);

export const WORKFLOW_TABLE_SORT_BY_MANAGE_ACCOUNT_LEVEL = orderBy(
  [
    {
      value: WorkflowMethodsSortByFields.TABLE_ID,
      description: 'Table ID'
    },
    {
      value: WorkflowMethodsSortByFields.EFFECTIVE_DATE,
      description: 'Effective Date'
    }
  ],
  ['description'],
  ['asc']
);

export const WORKFLOW_TLP_CA_SORT_BY_LIST = orderBy(
  [
    {
      value: WorkFlowTLPCASortByFields.TABLE_NAME,
      description: 'Table Name'
    },
    {
      value: WorkFlowTLPCASortByFields.EFFECTIVE_DATE,
      description: 'Effective Date'
    }
  ],
  ['description'],
  ['asc']
);

export const WORKFLOW_TLP_ST_SORT_BY_LIST = orderBy(
  [
    {
      value: WorkFlowTLPCASortByFields.TABLE_NAME,
      description: 'Table Name'
    },
    {
      value: WorkFlowTLPCASortByFields.EFFECTIVE_DATE,
      description: 'Effective Date'
    }
  ],
  ['description'],
  ['asc']
);
export const ORDER_BY_LIST = [
  { value: OrderBy.ASC, description: 'Ascending' },
  { value: OrderBy.DESC, description: 'Descending ' }
];

export const DEFAULT_PORFOLIOS_FILTER_VALUE = {
  ...DEFAULT_FILTER_VALUE,
  sortBy: PORTFOLIOS_SORT_BY_LIST.map(p => p.value),
  orderBy: ['desc', 'asc', 'asc']
};

export const DEFAULT_PRICING_STRATEGIES_FILTER_VALUE = {
  ...DEFAULT_FILTER_VALUE,
  sortBy: [PRICING_STRATEGIES_SORT_BY_LIST[2].value],
  orderBy: [ORDER_BY_LIST[0].value]
};

export const ACTIVITIES_SORT_BY_LIST = [
  {
    value: ActivitySortByFields.DATE_CHANGED,
    description: 'Activity Date Timestamp'
  }
];

export const CHANGE_STATUS = orderBy([
  'All',
  'Work',
  'Validated',
  'Validating',
  'Pending Approval',
  'Scheduled',
  'Production',
  'Lapsed',
  'Rejected'
]);

export const CHANGE_TYPE = [
  'All',
  ...orderBy([
    'Pricing',
    'Account Management',
    'Correspondence',
    'File Management',
    'Lending',
    'Monetary',
    'Portfolio Management',
    'Product Setup',
    'Re-Issue',
    'Reporting',
    'Security',
    'Testing',
    'Testing (Internal)'
  ])
];

export const SPECIAL_TYPE: Record<string, string> = {
  reIssue: 'Re-Issue',
  testingInternal: 'Testing (Internal)'
};

export const IN_APP_NOTIFICATION_DELAY = 10000;

export const CHANGE_FILTER_BY = [
  {
    value: ChangeManagementSortByFields.CREATED_DATE,
    description: 'Created Date'
  },
  {
    value: ChangeManagementSortByFields.EFFECTIVE_DATE,
    description: 'Effective Date'
  },
  {
    value: ChangeManagementSortByFields.LAST_APPROVAL_DAY,
    description: 'Last Approved Date'
  }
];

export const CHANGE_FILTER_VALUES = [
  'All',
  'Today',
  'Last 7 days',
  'Last 30 days'
];

export const WORKFLOW_FILTER_TYPE_FIELDS = [
  {
    value: WorkflowTemplateType.ALL,
    description: 'All'
  },
  {
    value: WorkflowTemplateType.ACCOUNT_MANAGEMENT,
    description: 'Account Management'
  },
  {
    value: WorkflowTemplateType.CORRESPONDENCE,
    description: 'Correspondence'
  },
  {
    value: WorkflowTemplateType.FILE_MANAGEMENT,
    description: 'File Management'
  },
  {
    value: WorkflowTemplateType.LENDING,
    description: 'Lending'
  },
  {
    value: WorkflowTemplateType.MONETARY,
    description: 'Monetary'
  },
  {
    value: WorkflowTemplateType.PORTFOLIOS_MANAGEMENT,
    description: 'Portfolio Management'
  },
  {
    value: WorkflowTemplateType.PRICING,
    description: 'Pricing'
  },
  {
    value: WorkflowTemplateType.PRODUCT_SETUP,
    description: 'Product Setup'
  },
  {
    value: WorkflowTemplateType.RE_ISSUE,
    description: 'Re-Issue'
  },
  {
    value: WorkflowTemplateType.REPORTING,
    description: 'Reporting'
  },
  {
    value: WorkflowTemplateType.SECURITY,
    description: 'Security'
  },
  {
    value: WorkflowTemplateType.TESTING,
    description: 'Testing'
  },
  {
    value: WorkflowTemplateType.TESTING_INTERNAL,
    description: 'Testing (Internal)'
  }
];

export const ADD_METHOD_BUTTONS = [
  {
    value: AddMethodButtons.CREATE_NEW_VERSION,
    description: 'txt_manage_penalty_fee_create_new_version'
  },
  {
    value: AddMethodButtons.CHOOSE_METHOD_TO_MODEL,
    description: 'txt_manage_penalty_fee_choose_method_to_model'
  },
  {
    value: AddMethodButtons.ADD_NEW_METHOD,
    description: 'txt_manage_penalty_fee_create_new_method'
  }
];

export const ADD_TABLE_BUTTONS = [
  {
    value: AddTableButtons.CREATE_NEW_VERSION,
    description: 'txt_manage_penalty_fee_create_new_version'
  },
  {
    value: AddTableButtons.CHOOSE_TABLE_TO_MODEL,
    description: 'txt_manage_transaction_level_tlp_choose_table_to_model'
  },
  {
    value: AddTableButtons.ADD_NEW_TABLE,
    description: 'txt_manage_transaction_level_tlp_create_new_table'
  }
];

export const ADD_ATTRIBUTE_BUTTONS = [
  {
    value: AddAttributeButtons.ADD_ATTRIBUTE,
    description: 'txt_portfolio_definitions_add_attribute'
  },
  {
    value: AddAttributeButtons.ADD_ATTRIBUTE_GROUP,
    description: 'txt_portfolio_definitions_add_attribute_group'
  }
];
