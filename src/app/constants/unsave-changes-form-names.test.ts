import { addSuffixFormName } from './unsave-changes-form-names';

describe('test addSuffixFormName', () => {
  it('should call callback fn', () => {
    jest.useFakeTimers();
    const value = addSuffixFormName('name', 'suffix');
    jest.runAllTimers();

    expect(`name_suffix`).toEqual(value);
  });
});
