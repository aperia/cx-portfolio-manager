import {
  ElementsFieldParameterEnum,
  ElementTypeEnum,
  MethodFieldParameterEnum,
  RecordFieldParameterEnum,
  TableFieldParameterEnum
} from './enums';

export const CHANGE_TYPE_MAPPING: Record<string, string> = {
  PRICING: 'pricing',
  ACCOUNTMANAGEMENT: 'accountManagement',
  CORRESPONDENCE: 'correspondence',
  FILEMANAGEMENT: 'fileManagement',
  LENDING: 'lending',
  MONETARY: 'monetary',
  PORTFOLIOMANAGEMENT: 'portfolioManagement',
  PRODUCTSETUP: 'productSetup',
  REISSUE: 'Re-Issuance',
  REPORTING: 'reporting',
  SECURITY: 'security',
  TESTING: 'testing',
  TESTINGINTERNAL: 'testingInternal',
  SETTLEMENT: 'settlement',
  UTILITY: 'utility',
  IMPLEMENTATION: 'implementation'
};

export const CHANGE_STATUS_MAPPING: Record<string, string> = {
  WORK: 'work',
  VALIDATED: 'validated',
  VALIDATING: 'validating',
  PENDINGAPPROVAL: 'pendingApproval',
  SCHEDULED: 'scheduled',
  PRODUCTION: 'production',
  LAPSED: 'lapsed',
  REJECTED: 'rejected',
  DEMOTED: 'demoted',
  PREVIOUS: 'previous'
};

export const CHANGE_ACTION_MAPPING: Record<string, string> = {
  APPROVE: 'approve',
  REJECT: 'reject',
  DEMOTE: 'demote',
  SUBMITFORAPPROVAL: 'submit',
  VALIDATE: 'validate',
  EDIT: 'edit',
  RESCHEDULE: 'reschedule',
  DELETE: 'delete'
};

export const WORKFLOW_STATUS_MAPPING: Record<string, string> = {
  COMPLETE: 'complete',
  INCOMPLETE: 'incomplete'
};

export const WORKFLOW_ACTION_MAPPING: Record<string, string> = {
  EDIT: 'edit',
  DELETE: 'delete'
};

export const DMM_RESULTING_ACTION_MAPPING: Record<string, string> = {
  UPDATED: 'Updated',
  CREATED: 'Created'
};

export const ACTIVITY_ACTION: Record<string, string> = {
  ADDWORKFLOW: 'set up workflow',
  EDITWORKFLOW: 'edited workflow',
  CHANGEFIELDS: 'edited the change',
  REMOVEWORKFLOW: 'deleted workflow',
  CREATEDCHANGE: 'created the change',
  CHANGESETSUBMITTEDFORAPPROVAL: 'submitted the change for approval',
  CHANGESETSUBMITTEDFORVALIDATION: 'validated the change',
  CHANGESETDEMOTED: 'demoted the change',
  CHANGESETREJECTED: 'rejected the change',
  CHANGESETAPPROVED: 'approved the change'
};

export const ACTIVITY_FIELD: Record<string, string> = {
  ChangeName: 'Change Name',
  ChangeDescription: 'Change Description',
  ChangeOwner: 'Change Owner',
  ChangeEffectiveDate: 'Effective Date',
  ChangeStatus: 'Change Status'
};

export const METHOD_RC_FIELDS = [
  MethodFieldParameterEnum.ReturnedCheckChargesOption,
  MethodFieldParameterEnum.AmountAssessed,
  MethodFieldParameterEnum.MinimumPayDueOptions,
  MethodFieldParameterEnum.OneFeeParticipationCode,
  MethodFieldParameterEnum.BatchIdentification,
  MethodFieldParameterEnum.ComputerLetter,
  MethodFieldParameterEnum.PostingTextID,
  MethodFieldParameterEnum.ReversalTextID,
  MethodFieldParameterEnum.FirstYrMaxManagement,
  MethodFieldParameterEnum.OneFeePriorityCode
];

export const METHOD_LC_FIELDS = [
  MethodFieldParameterEnum.LateChargesOption,
  MethodFieldParameterEnum.CalculationBase,
  MethodFieldParameterEnum.MinimumPayDueOptions,
  MethodFieldParameterEnum.FixedAmount,
  MethodFieldParameterEnum.Percent,
  MethodFieldParameterEnum.MaximumAmount,
  MethodFieldParameterEnum.MaximumYearlyAmount,
  MethodFieldParameterEnum.AssessmentControl,
  MethodFieldParameterEnum.CyclesOfConsecutiveDelinquency,
  MethodFieldParameterEnum.BalanceIndicator,
  MethodFieldParameterEnum.AssessedAccounts,
  MethodFieldParameterEnum.ExclusionBalance,
  MethodFieldParameterEnum.CurrentBalanceAssessment,
  MethodFieldParameterEnum.CalculationDayControl,
  MethodFieldParameterEnum.NumberOfDays,
  MethodFieldParameterEnum.NonProcessingLateCharge,
  MethodFieldParameterEnum.IncludeExcludeControl,
  MethodFieldParameterEnum.Status1,
  MethodFieldParameterEnum.Status2,
  MethodFieldParameterEnum.Status3,
  MethodFieldParameterEnum.Status4,
  MethodFieldParameterEnum.Status5,
  MethodFieldParameterEnum.Tier2Balance,
  MethodFieldParameterEnum.Tier3Balance,
  MethodFieldParameterEnum.Tier4Balance,
  MethodFieldParameterEnum.Tier5Balance,
  MethodFieldParameterEnum.Tier2Amount,
  MethodFieldParameterEnum.Tier3Amount,
  MethodFieldParameterEnum.Tier4Amount,
  MethodFieldParameterEnum.Tier5Amount,
  MethodFieldParameterEnum.ThresholdToWaveLateCharge,
  MethodFieldParameterEnum.LateChargeResetCounter,
  MethodFieldParameterEnum.ReversalDetailsTextID,
  MethodFieldParameterEnum.WaivedMessageTextID,
  MethodFieldParameterEnum.LatePaymentWarningMessage
];

export const METHOD_ACCOUNT_LEVEL_PROCESSING = [
  MethodFieldParameterEnum.AllocationDate,
  MethodFieldParameterEnum.DecisionElement,
  MethodFieldParameterEnum.SearchCode,
  MethodFieldParameterEnum.ImmediateAllocation
];

export const METHOD_CHANGES_INTEREST_RATES_FIELDS = [
  MethodFieldParameterEnum.DefaultCashBaseInterest,
  MethodFieldParameterEnum.DefaultMerchandiseBaseInterest,
  MethodFieldParameterEnum.DefaultMaximumCashInterest,
  MethodFieldParameterEnum.DefaultMaximumMerchandiseInterest,
  MethodFieldParameterEnum.DefaultMinimumCashInterest,
  MethodFieldParameterEnum.DefaultMinimumMerchandiseInterest,
  MethodFieldParameterEnum.IndexRateMethod,
  MethodFieldParameterEnum.DefaultBaseInterestUsage,
  MethodFieldParameterEnum.DefaultOverrideInterestLimit,
  MethodFieldParameterEnum.DefaultMinimumUsage,
  MethodFieldParameterEnum.DefaultMaximumUsage,
  MethodFieldParameterEnum.DefaultMaximumInterestUsage,
  MethodFieldParameterEnum.IncentivePricingUsageCode,
  MethodFieldParameterEnum.IncentiveBaseInterestUsageRate,
  MethodFieldParameterEnum.DelinquencyCyclesForTermination,
  MethodFieldParameterEnum.MinimumMaximumRateOverrideOptions,
  MethodFieldParameterEnum.IncentivePricingOverrideOption,
  MethodFieldParameterEnum.TerminateIncentivePricingOnPricingStrategyChange,
  MethodFieldParameterEnum.MethodOverrideTerminationOption,
  MethodFieldParameterEnum.IncentiveCashInterestRate,
  MethodFieldParameterEnum.IndexRateMethodName,
  MethodFieldParameterEnum.IncentiveMerchandiseInterestRate,
  MethodFieldParameterEnum.EndofIncentiveWarningNotificationTiming,
  MethodFieldParameterEnum.EndofIncentiveWarningNotificationStatementText,
  MethodFieldParameterEnum.IncentivePricingNumberOfMonths,
  MethodFieldParameterEnum.MinimumRateForCash,
  MethodFieldParameterEnum.MaximumRateForCash,
  MethodFieldParameterEnum.MinimumRateForMerchandise,
  MethodFieldParameterEnum.MaximumRateForMerchandise,
  MethodFieldParameterEnum.PreventTermsChangeDuringIncentivePricingPeriod,
  MethodFieldParameterEnum.MessageForProtectedBalanceIncentivePricing,
  MethodFieldParameterEnum.InterestRateRoundingCalculation,
  MethodFieldParameterEnum.DailyAPRRoundingCalculation,
  MethodFieldParameterEnum.StopCalculatingInterestAfterNumberofDaysDelinquent,
  MethodFieldParameterEnum.IncludeAdditionalCreditBalanceInAverageDailyBalanceCalculation,
  MethodFieldParameterEnum.CombineMerchandiseAndCashBalancesOnInterestCalculations,
  MethodFieldParameterEnum.RestartInterestCalculationsAfterClearingDelinquency,
  MethodFieldParameterEnum.CalculateCompoundInterest,
  MethodFieldParameterEnum.OverridePromotionalRatesForPricingStrategies,
  MethodFieldParameterEnum.MonthlyInterestCalculationOptions,
  MethodFieldParameterEnum.DailyInterestRoundingOption,
  MethodFieldParameterEnum.OverrideInterestRatesForProtectedBalance,
  MethodFieldParameterEnum.MinimumDaysForCycleCodeChange,
  MethodFieldParameterEnum.RoundNumberOfDaysInCycleForInterestCalculations,
  MethodFieldParameterEnum.CycleCodeChangeOptions,
  MethodFieldParameterEnum.CyclePeriodChangeCISMemoOption,
  MethodFieldParameterEnum.InterestMethodOldCash,
  MethodFieldParameterEnum.InterestMethodCycleToDateCash,
  MethodFieldParameterEnum.InterestMethodTwoCycleOldCash,
  MethodFieldParameterEnum.InterestMethodOneCycleOldMerchandise,
  MethodFieldParameterEnum.InterestMethodCycleToDateMerchandise,
  MethodFieldParameterEnum.AdditionalInterestOnMerchandise1CyclesDelinquent,
  MethodFieldParameterEnum.AdditionalInterestOnMerchandise2CyclesDelinquent,
  MethodFieldParameterEnum.AdditionalInterestOnCash2CyclesDelinquent,
  MethodFieldParameterEnum.AdditionalInterestOnMerchandise3CyclesDelinquent,
  MethodFieldParameterEnum.AdditionalInterestOnCash3CyclesDelinquent,
  MethodFieldParameterEnum.AutomatedMemosForAdditionalInterest,
  MethodFieldParameterEnum.UseExternalStatusesForAdditionalInterest,
  MethodFieldParameterEnum.ExternalStatus1ForAdditionalInterest,
  MethodFieldParameterEnum.ExternalStatus2ForAdditionalInterest,
  MethodFieldParameterEnum.ExternalStatus3ForAdditionalInterest,
  MethodFieldParameterEnum.ExternalStatus4ForAdditionalInterest,
  MethodFieldParameterEnum.ExternalStatus5ForAdditionalInterest,
  MethodFieldParameterEnum.PenaltyPricingStatusReasonCodeTable,
  MethodFieldParameterEnum.PenaltyPricingTimingOptions,
  MethodFieldParameterEnum.PenaltyPricingStatementMessage1CyclesOldBalance,
  MethodFieldParameterEnum.PenaltyPricingStatementMessage2CyclesOldBalance,
  MethodFieldParameterEnum.PenaltyPricingStatementMessage3CyclesOldBalance,
  MethodFieldParameterEnum.CureDelinquencyFor1CycleDelinquentBalances,
  MethodFieldParameterEnum.CureDelinquencyFor2CycleDelinquentBalances,
  MethodFieldParameterEnum.CureDelinquencyFor3CycleDelinquentBalances,
  MethodFieldParameterEnum.PenaltyPricingOnPromotions,
  MethodFieldParameterEnum.CITMethodForPenaltyPricing,
  MethodFieldParameterEnum.IncludeZeroBalanceDaysInADBCalculation
];

export const METHOD_DLO_FIELDS = [
  MethodFieldParameterEnum.PromotionDescription,
  MethodFieldParameterEnum.PromotionAlternateDescription,
  MethodFieldParameterEnum.PromotionDescriptionDisplayOptions,
  MethodFieldParameterEnum.PromotionStatementTextIDDTDD,
  MethodFieldParameterEnum.PromotionStatementTextIDDTDX,
  MethodFieldParameterEnum.PromotionStatementTextIDStandard,
  MethodFieldParameterEnum.PromotionStatementTextControl,
  MethodFieldParameterEnum.PromotionReturnApplicationOption,
  MethodFieldParameterEnum.PromotionPayoffExceptionOption,
  MethodFieldParameterEnum.PayoffExceptionMethod,
  MethodFieldParameterEnum.CashAdvanceItemFeesMethod,
  MethodFieldParameterEnum.MerchantItemFeesMethod,
  MethodFieldParameterEnum.CreditApplicationGroup,
  MethodFieldParameterEnum.PromotionGroupIdentifier,
  MethodFieldParameterEnum.LoanBalanceClassification,
  //design loan offers - payment and pricing settings
  MethodFieldParameterEnum.StandardMinimumPaymentCalculationCode,
  MethodFieldParameterEnum.RulesMinimumPaymentDueMethod,
  MethodFieldParameterEnum.StandardMinimumPaymentsRate,
  MethodFieldParameterEnum.StandardMinimumPaymentRoundingOptions,
  MethodFieldParameterEnum.AnnualInterestRate,
  MethodFieldParameterEnum.PayoutPeriod,

  //design loan offers - merchant discount settings
  MethodFieldParameterEnum.MerchantDiscount,
  MethodFieldParameterEnum.MerchantDiscountCalculationCode,

  //design loan offers - advanced settings
  MethodFieldParameterEnum.ReturnToRevolvingBaseInterest,
  MethodFieldParameterEnum.ApplyPenaltyPricingOnLoanOffer,
  MethodFieldParameterEnum.DefaultInterestRateMethodOverride,
  MethodFieldParameterEnum.InterestCalculationMethodOverride,
  MethodFieldParameterEnum.IntroductoryMessageDisplayControl,
  MethodFieldParameterEnum.PositiveAmortizationUsageCode,

  //design loan offers - advanced introductory APR settings
  MethodFieldParameterEnum.IntroductoryAnnualRate,
  MethodFieldParameterEnum.IntroductoryAnnualRateFixedEndDate,
  MethodFieldParameterEnum.IntroductoryRateNumberOfDays,
  MethodFieldParameterEnum.IntroductoryRateNumberOfCycles,
  MethodFieldParameterEnum.IntroductoryPeriodStatementTextID,

  //design loan offers - advanced promotional cash settings
  MethodFieldParameterEnum.SameAsCashLoanOfferNumberOfCycles,
  MethodFieldParameterEnum.AccrueInterestOnUnbilledInterest,
  //design loan offers - advanced payment delay settings
  MethodFieldParameterEnum.DelayPaymentForStatementCycles,
  MethodFieldParameterEnum.DelayPaymentForMonths,
  //design promotions basic
  MethodFieldParameterEnum.DesignPromotionBasicInformation1,
  MethodFieldParameterEnum.DesignPromotionBasicInformation2,
  MethodFieldParameterEnum.DesignPromotionBasicInformation3,
  MethodFieldParameterEnum.DesignPromotionBasicInformation4,
  MethodFieldParameterEnum.DesignPromotionBasicInformation5,
  MethodFieldParameterEnum.DesignPromotionBasicInformation6,
  MethodFieldParameterEnum.DesignPromotionBasicInformation7,
  MethodFieldParameterEnum.DesignPromotionBasicInformation8,
  MethodFieldParameterEnum.DesignPromotionBasicInformation9,
  MethodFieldParameterEnum.DesignPromotionBasicInformation10,
  MethodFieldParameterEnum.DesignPromotionBasicInformation11,
  MethodFieldParameterEnum.DesignPromotionBasicInformation12,
  MethodFieldParameterEnum.DesignPromotionBasicInformation13,
  //design promotions Interest Assessment and Fees
  MethodFieldParameterEnum.DesignPromotionInterestAssessment1,
  MethodFieldParameterEnum.DesignPromotionInterestAssessment2,
  MethodFieldParameterEnum.DesignPromotionInterestAssessment3,
  MethodFieldParameterEnum.DesignPromotionInterestAssessment4,
  MethodFieldParameterEnum.DesignPromotionInterestAssessment5,
  MethodFieldParameterEnum.DesignPromotionInterestAssessment6,
  MethodFieldParameterEnum.DesignPromotionInterestAssessment7,
  MethodFieldParameterEnum.DesignPromotionInterestAssessment8,
  MethodFieldParameterEnum.DesignPromotionInterestAssessment9,
  MethodFieldParameterEnum.DesignPromotionInterestAssessment10,
  MethodFieldParameterEnum.DesignPromotionInterestAssessment11,
  MethodFieldParameterEnum.DesignPromotionInterestAssessment12,
  MethodFieldParameterEnum.DesignPromotionInterestAssessment13,
  MethodFieldParameterEnum.DesignPromotionInterestAssessment14,
  MethodFieldParameterEnum.DesignPromotionInterestAssessment15,
  MethodFieldParameterEnum.DesignPromotionInterestAssessment16,
  MethodFieldParameterEnum.DesignPromotionInterestAssessment17,
  MethodFieldParameterEnum.DesignPromotionInterestAssessment18,
  //design promotions Interest Overrides
  MethodFieldParameterEnum.DesignPromotionInterestOverrides1,
  MethodFieldParameterEnum.DesignPromotionInterestOverrides2,
  MethodFieldParameterEnum.DesignPromotionInterestOverrides3,
  MethodFieldParameterEnum.DesignPromotionInterestOverrides4,
  MethodFieldParameterEnum.DesignPromotionInterestOverrides5,
  MethodFieldParameterEnum.DesignPromotionInterestOverrides6,
  MethodFieldParameterEnum.DesignPromotionInterestOverrides7,
  MethodFieldParameterEnum.DesignPromotionInterestOverrides8,
  //design promotions Retail Path
  MethodFieldParameterEnum.DesignPromotionRetailPath1,
  MethodFieldParameterEnum.DesignPromotionRetailPath2,
  MethodFieldParameterEnum.DesignPromotionRetailPath3,
  MethodFieldParameterEnum.DesignPromotionRetailPath4,
  //design promotions Return-to-Revolve
  MethodFieldParameterEnum.DesignPromotionReturnRevolve1,
  MethodFieldParameterEnum.DesignPromotionReturnRevolve2,
  MethodFieldParameterEnum.DesignPromotionReturnRevolve3,
  MethodFieldParameterEnum.DesignPromotionReturnRevolve4,
  MethodFieldParameterEnum.DesignPromotionReturnRevolve5,
  MethodFieldParameterEnum.DesignPromotionReturnRevolve6,
  //design promotions Statement Messaging
  MethodFieldParameterEnum.DesignPromotionStatementMessaging1,
  MethodFieldParameterEnum.DesignPromotionStatementMessaging2,
  MethodFieldParameterEnum.DesignPromotionStatementMessaging3,
  MethodFieldParameterEnum.DesignPromotionStatementMessaging4,
  MethodFieldParameterEnum.DesignPromotionStatementMessaging5,
  // Design Promotion Statement Advanced
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter1,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter2,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter3,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter4,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter5,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter6,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter7,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter8,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter9,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter10,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter11,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter12,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter13,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter14,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter15,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter16,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter17,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter18,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter19,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter20,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter21,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter22,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter23,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter24,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter25,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter26,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter27,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter28
];

export const METHOD_DLO_P_FIELDS = [
  //design promotions basic
  MethodFieldParameterEnum.DesignPromotionBasicInformation1,
  MethodFieldParameterEnum.DesignPromotionBasicInformation2,
  MethodFieldParameterEnum.DesignPromotionBasicInformation3,
  MethodFieldParameterEnum.DesignPromotionBasicInformation4,
  MethodFieldParameterEnum.DesignPromotionBasicInformation5,
  MethodFieldParameterEnum.DesignPromotionBasicInformation6,
  MethodFieldParameterEnum.DesignPromotionBasicInformation7,
  MethodFieldParameterEnum.DesignPromotionBasicInformation8,
  MethodFieldParameterEnum.DesignPromotionBasicInformation9,
  MethodFieldParameterEnum.DesignPromotionBasicInformation10,
  MethodFieldParameterEnum.DesignPromotionBasicInformation11,
  MethodFieldParameterEnum.DesignPromotionBasicInformation12,
  MethodFieldParameterEnum.DesignPromotionBasicInformation13,
  //design promotions Interest Assessment and Fees
  MethodFieldParameterEnum.DesignPromotionInterestAssessment1,
  MethodFieldParameterEnum.DesignPromotionInterestAssessment2,
  MethodFieldParameterEnum.DesignPromotionInterestAssessment3,
  MethodFieldParameterEnum.DesignPromotionInterestAssessment4,
  MethodFieldParameterEnum.DesignPromotionInterestAssessment5,
  MethodFieldParameterEnum.DesignPromotionInterestAssessment6,
  MethodFieldParameterEnum.DesignPromotionInterestAssessment7,
  MethodFieldParameterEnum.DesignPromotionInterestAssessment8,
  MethodFieldParameterEnum.DesignPromotionInterestAssessment9,
  MethodFieldParameterEnum.DesignPromotionInterestAssessment10,
  MethodFieldParameterEnum.DesignPromotionInterestAssessment11,
  MethodFieldParameterEnum.DesignPromotionInterestAssessment12,
  MethodFieldParameterEnum.DesignPromotionInterestAssessment13,
  MethodFieldParameterEnum.DesignPromotionInterestAssessment14,
  MethodFieldParameterEnum.DesignPromotionInterestAssessment15,
  MethodFieldParameterEnum.DesignPromotionInterestAssessment16,
  MethodFieldParameterEnum.DesignPromotionInterestAssessment17,
  MethodFieldParameterEnum.DesignPromotionInterestAssessment18,
  //design promotions Interest Overrides
  MethodFieldParameterEnum.DesignPromotionInterestOverrides1,
  MethodFieldParameterEnum.DesignPromotionInterestOverrides2,
  MethodFieldParameterEnum.DesignPromotionInterestOverrides3,
  MethodFieldParameterEnum.DesignPromotionInterestOverrides4,
  MethodFieldParameterEnum.DesignPromotionInterestOverrides5,
  MethodFieldParameterEnum.DesignPromotionInterestOverrides6,
  MethodFieldParameterEnum.DesignPromotionInterestOverrides7,
  MethodFieldParameterEnum.DesignPromotionInterestOverrides8,
  //design promotions Retail Path
  MethodFieldParameterEnum.DesignPromotionRetailPath1,
  MethodFieldParameterEnum.DesignPromotionRetailPath2,
  MethodFieldParameterEnum.DesignPromotionRetailPath3,
  MethodFieldParameterEnum.DesignPromotionRetailPath4,
  //design promotions Return-to-Revolve
  MethodFieldParameterEnum.DesignPromotionReturnRevolve1,
  MethodFieldParameterEnum.DesignPromotionReturnRevolve2,
  MethodFieldParameterEnum.DesignPromotionReturnRevolve3,
  MethodFieldParameterEnum.DesignPromotionReturnRevolve4,
  MethodFieldParameterEnum.DesignPromotionReturnRevolve5,
  MethodFieldParameterEnum.DesignPromotionReturnRevolve6,
  //design promotions Statement Messaging
  MethodFieldParameterEnum.DesignPromotionStatementMessaging1,
  MethodFieldParameterEnum.DesignPromotionStatementMessaging2,
  MethodFieldParameterEnum.DesignPromotionStatementMessaging3,
  MethodFieldParameterEnum.DesignPromotionStatementMessaging4,
  MethodFieldParameterEnum.DesignPromotionStatementMessaging5,

  // Design Promotion Statement Advanced
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter1,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter2,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter3,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter4,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter5,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter6,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter7,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter8,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter9,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter10,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter11,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter12,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter13,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter14,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter15,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter16,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter17,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter18,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter19,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter20,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter21,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter22,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter23,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter24,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter25,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter26,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter27,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter28
];

export const METHOD_UPDATE_INDEX_RATE_FIELDS = [
  MethodFieldParameterEnum.UpdateIndexRateCashIndexRate,
  MethodFieldParameterEnum.UpdateIndexRateEffectiveDateNewCash,
  MethodFieldParameterEnum.UpdateIndexRatePreviousCashIndexRate,
  MethodFieldParameterEnum.UpdateIndexRateMerchandiseIndexRate,
  MethodFieldParameterEnum.UpdateIndexRateEffectiveDateNewMerchandise,
  MethodFieldParameterEnum.UpdateIndexRatePreviousMerchandiseIndexRate,
  MethodFieldParameterEnum.UpdateIndexRateIndexType,
  MethodFieldParameterEnum.UpdateIndexRateDeterminationDateDescription,
  MethodFieldParameterEnum.UpdateIndexRateSourceDescription,
  MethodFieldParameterEnum.UpdateIndexRateChangePeriod
];

export const RECORD_MANAGE_ACCOUNT_SERIALIZATION_FIELDS = [
  RecordFieldParameterEnum.RecordType,
  RecordFieldParameterEnum.GenerationMethod,
  RecordFieldParameterEnum.RangeFrom,
  RecordFieldParameterEnum.RangeTo,
  RecordFieldParameterEnum.SerialNumber,
  RecordFieldParameterEnum.WarningLevel,
  RecordFieldParameterEnum.Prefix,
  RecordFieldParameterEnum.AlternatePrefix,
  RecordFieldParameterEnum.AccountIDOrPIID,
  RecordFieldParameterEnum.NextFrom,
  RecordFieldParameterEnum.NextTo,
  RecordFieldParameterEnum.AvailableRecordNumber,
  RecordFieldParameterEnum.UseAlternateAgent,
  RecordFieldParameterEnum.AlternatePrincipal,
  RecordFieldParameterEnum.AlternateAgent
];

export const RECORD_MANAGE_ACCOUNT_SERIALIZATION_DEFAULT: Record<
  string,
  string
> = {
  [RecordFieldParameterEnum.RecordType]: ' ',
  [RecordFieldParameterEnum.GenerationMethod]: 'S',
  [RecordFieldParameterEnum.RangeFrom]: '',
  [RecordFieldParameterEnum.RangeTo]: '',
  [RecordFieldParameterEnum.SerialNumber]: '',
  [RecordFieldParameterEnum.WarningLevel]: '',
  [RecordFieldParameterEnum.Prefix]: '',
  [RecordFieldParameterEnum.AlternatePrefix]: '',
  [RecordFieldParameterEnum.AccountIDOrPIID]: '',
  [RecordFieldParameterEnum.NextFrom]: '',
  [RecordFieldParameterEnum.NextTo]: '',
  [RecordFieldParameterEnum.AvailableRecordNumber]: '',
  [RecordFieldParameterEnum.UseAlternateAgent]: 'N',
  [RecordFieldParameterEnum.AlternatePrincipal]: '',
  [RecordFieldParameterEnum.AlternateAgent]: ''
};

export const METHOD_TYPE: Record<string, string> = {
  NEWMETHOD: 'New Method',
  MODELEDMETHOD: 'Modeled Method',
  NEWVERSION: 'New Version'
};

export const ELEMENT_FIELDS = [
  ElementsFieldParameterEnum.ElementID,
  ElementsFieldParameterEnum.ElementName,
  ElementsFieldParameterEnum.TopName,
  ElementsFieldParameterEnum.BottomName,
  ElementsFieldParameterEnum.Reallocation,
  ElementsFieldParameterEnum.DeltaType,
  ElementsFieldParameterEnum.TransferTypes,
  ElementsFieldParameterEnum.ElementType,
  ElementsFieldParameterEnum.ElementLength,
  ElementsFieldParameterEnum.NumberOfDecimals,
  ElementsFieldParameterEnum.MinValue,
  ElementsFieldParameterEnum.MaxValue,
  ElementsFieldParameterEnum.ValidValues,
  ElementsFieldParameterEnum.DefaultValue,
  ElementsFieldParameterEnum.LetterFormat,
  ElementsFieldParameterEnum.UseDefaultOnLetter,
  ElementsFieldParameterEnum.OCSTran,
  ElementsFieldParameterEnum.MaskText,
  ElementsFieldParameterEnum.RealTimeUpdate,
  ElementsFieldParameterEnum.RetainOnTransfer,
  ElementsFieldParameterEnum.ElementBusinessDescription
];

export const ELEMENT_FIELDS_DEFAULT: Record<string, string> = {
  [ElementsFieldParameterEnum.ElementID]: '',
  [ElementsFieldParameterEnum.ElementName]: '',
  [ElementsFieldParameterEnum.TopName]: '',
  [ElementsFieldParameterEnum.BottomName]: '',
  [ElementsFieldParameterEnum.Reallocation]: ' ',
  [ElementsFieldParameterEnum.DeltaType]: ' ',
  [ElementsFieldParameterEnum.TransferTypes]: ' ',
  [ElementsFieldParameterEnum.ElementType]: ' ',
  [ElementsFieldParameterEnum.ElementLength]: '',
  [ElementsFieldParameterEnum.NumberOfDecimals]: '',
  [ElementsFieldParameterEnum.MinValue]: '',
  [ElementsFieldParameterEnum.MaxValue]: '',
  [ElementsFieldParameterEnum.ValidValues]: '',
  [ElementsFieldParameterEnum.DefaultValue]: '',
  [ElementsFieldParameterEnum.LetterFormat]: ' ',
  [ElementsFieldParameterEnum.UseDefaultOnLetter]: ' ',
  [ElementsFieldParameterEnum.OCSTran]: '+D0001',
  [ElementsFieldParameterEnum.MaskText]: '',
  [ElementsFieldParameterEnum.RealTimeUpdate]: ' ',
  [ElementsFieldParameterEnum.RetainOnTransfer]: ' ',
  [ElementsFieldParameterEnum.ElementBusinessDescription]: ''
};

export const CHANGE_INTEREST_RATES_FIELD = [
  [MethodFieldParameterEnum.DefaultCashBaseInterest],
  [MethodFieldParameterEnum.DefaultMerchandiseBaseInterest],
  [MethodFieldParameterEnum.DefaultMaximumCashInterest],
  [MethodFieldParameterEnum.DefaultMaximumMerchandiseInterest],
  [MethodFieldParameterEnum.DefaultMinimumCashInterest],
  [MethodFieldParameterEnum.DefaultMinimumMerchandiseInterest],
  [MethodFieldParameterEnum.IndexRateMethod],
  [MethodFieldParameterEnum.DefaultBaseInterestUsage],
  [MethodFieldParameterEnum.DefaultOverrideInterestLimit],
  [MethodFieldParameterEnum.DefaultMinimumUsage],
  [MethodFieldParameterEnum.DefaultMaximumUsage],
  [MethodFieldParameterEnum.DefaultMaximumInterestUsage],
  [MethodFieldParameterEnum.IncentivePricingUsageCode],
  [MethodFieldParameterEnum.IncentiveBaseInterestUsageRate],
  [MethodFieldParameterEnum.IndexRateMethodName],
  [MethodFieldParameterEnum.DelinquencyCyclesForTermination],
  [MethodFieldParameterEnum.MinimumMaximumRateOverrideOptions],
  [MethodFieldParameterEnum.IncentivePricingOverrideOption],
  [MethodFieldParameterEnum.TerminateIncentivePricingOnPricingStrategyChange],
  [MethodFieldParameterEnum.MethodOverrideTerminationOption],
  [MethodFieldParameterEnum.IncentiveCashInterestRate],
  [MethodFieldParameterEnum.IncentiveMerchandiseInterestRate],
  [MethodFieldParameterEnum.EndofIncentiveWarningNotificationTiming],
  [MethodFieldParameterEnum.EndofIncentiveWarningNotificationStatementText],
  [MethodFieldParameterEnum.IncentivePricingNumberOfMonths],
  [MethodFieldParameterEnum.MinimumRateForCash],
  [MethodFieldParameterEnum.MaximumRateForCash],
  [MethodFieldParameterEnum.MinimumRateForMerchandise],
  [MethodFieldParameterEnum.MaximumRateForMerchandise],
  [MethodFieldParameterEnum.PreventTermsChangeDuringIncentivePricingPeriod],
  [MethodFieldParameterEnum.MessageForProtectedBalanceIncentivePricing]
];

export const METHOD_CHANGES_INTEREST_RATES_DEFAULT: Record<string, string> = {
  [MethodFieldParameterEnum.StopCalculatingInterestAfterNumberofDaysDelinquent]:
    '990'
};

export const METHOD_LEVEL_PROCESSING_DEFAULT: Record<string, string> = {
  [TableFieldParameterEnum.AllocationBeforeAfterCycle]: 'B',
  [TableFieldParameterEnum.ChangeInTermsMethodFlag]: 'N'
};

export const ELEMENT_TYPE = [
  ElementTypeEnum.Required,
  ElementTypeEnum.Optional
];

export const METHOD_LC_DEFAULT: Record<string, string> = {
  [MethodFieldParameterEnum.LateChargesOption]: 'P',
  [MethodFieldParameterEnum.CalculationBase]: '2',
  [MethodFieldParameterEnum.MinimumPayDueOptions]: '0',
  [MethodFieldParameterEnum.FixedAmount]: '462.98',
  [MethodFieldParameterEnum.Percent]: '18.932',
  [MethodFieldParameterEnum.MaximumAmount]: '129',
  [MethodFieldParameterEnum.MaximumYearlyAmount]: '231.96',
  [MethodFieldParameterEnum.AssessmentControl]: '0',
  [MethodFieldParameterEnum.CyclesOfConsecutiveDelinquency]: '0',
  [MethodFieldParameterEnum.BalanceIndicator]: '0',
  [MethodFieldParameterEnum.ExclusionBalance]: '137',
  [MethodFieldParameterEnum.CurrentBalanceAssessment]: '0',
  [MethodFieldParameterEnum.CalculationDayControl]: '1',
  [MethodFieldParameterEnum.NumberOfDays]: '00',
  [MethodFieldParameterEnum.NonProcessingLateCharge]: '0',
  [MethodFieldParameterEnum.IncludeExcludeControl]: 'E',
  [MethodFieldParameterEnum.Status1]: 'Z',
  [MethodFieldParameterEnum.Status2]: 'U',
  [MethodFieldParameterEnum.Status3]: 'L',
  [MethodFieldParameterEnum.Status4]: 'I',
  [MethodFieldParameterEnum.Status5]: '*',
  [MethodFieldParameterEnum.Tier2Balance]: '0.00',
  [MethodFieldParameterEnum.Tier2Amount]: '0.00',
  [MethodFieldParameterEnum.Tier3Balance]: '0.00',
  [MethodFieldParameterEnum.Tier3Amount]: '0.00',
  [MethodFieldParameterEnum.Tier4Balance]: '0.00',
  [MethodFieldParameterEnum.Tier4Amount]: '0.00',
  [MethodFieldParameterEnum.Tier5Balance]: '0.00',
  [MethodFieldParameterEnum.Tier5Amount]: '0.00',
  [MethodFieldParameterEnum.ThresholdToWaveLateCharge]: '0.00',
  [MethodFieldParameterEnum.LateChargeResetCounter]: '0',
  [MethodFieldParameterEnum.ReversalDetailsTextID]: 'DEFFR001'
};

export const METHOD_RC_DEFAULT: Record<string, string> = {
  [MethodFieldParameterEnum.ReturnedCheckChargesOption]: '0',
  [MethodFieldParameterEnum.AmountAssessed]: '0.00',
  [MethodFieldParameterEnum.MinimumPayDueOptions]: '0',
  [MethodFieldParameterEnum.OneFeeParticipationCode]: '0',
  [MethodFieldParameterEnum.BatchIdentification]: 'Q1',
  [MethodFieldParameterEnum.ComputerLetter]: '0000',
  [MethodFieldParameterEnum.PostingTextID]: '1',
  [MethodFieldParameterEnum.ReversalTextID]: '0',
  [MethodFieldParameterEnum.FirstYrMaxManagement]: '0',
  [MethodFieldParameterEnum.OneFeePriorityCode]: '0'
};

export const METHOD_DLO_DEFAULT: Record<string, string> = {
  [MethodFieldParameterEnum.PromotionDescription]: '',
  [MethodFieldParameterEnum.PromotionAlternateDescription]: '',
  [MethodFieldParameterEnum.PromotionDescriptionDisplayOptions]: ' ',
  [MethodFieldParameterEnum.PromotionStatementTextIDDTDD]: '',
  [MethodFieldParameterEnum.PromotionStatementTextIDDTDX]: '',
  [MethodFieldParameterEnum.PromotionStatementTextIDStandard]: '',
  [MethodFieldParameterEnum.PromotionStatementTextControl]: '1',
  [MethodFieldParameterEnum.PromotionReturnApplicationOption]: ' ',
  [MethodFieldParameterEnum.PromotionPayoffExceptionOption]: ' ',
  [MethodFieldParameterEnum.PayoffExceptionMethod]: '',
  [MethodFieldParameterEnum.CashAdvanceItemFeesMethod]: '',
  [MethodFieldParameterEnum.MerchantItemFeesMethod]: '',
  [MethodFieldParameterEnum.CreditApplicationGroup]: ' ',
  [MethodFieldParameterEnum.PromotionGroupIdentifier]: '',
  [MethodFieldParameterEnum.LoanBalanceClassification]: ' ',
  //design loan offers - payment and pricing settings
  [MethodFieldParameterEnum.StandardMinimumPaymentCalculationCode]: ' ',
  [MethodFieldParameterEnum.RulesMinimumPaymentDueMethod]: '',
  [MethodFieldParameterEnum.StandardMinimumPaymentsRate]: '',
  [MethodFieldParameterEnum.StandardMinimumPaymentRoundingOptions]: ' ',
  [MethodFieldParameterEnum.AnnualInterestRate]: '',
  [MethodFieldParameterEnum.PayoutPeriod]: '',

  //design loan offers - merchant discount settings
  [MethodFieldParameterEnum.MerchantDiscount]: '',
  [MethodFieldParameterEnum.MerchantDiscountCalculationCode]: ' ',

  //design loan offers - advanced settings
  [MethodFieldParameterEnum.ReturnToRevolvingBaseInterest]: ' ',
  [MethodFieldParameterEnum.ApplyPenaltyPricingOnLoanOffer]: ' ',
  [MethodFieldParameterEnum.DefaultInterestRateMethodOverride]: '',
  [MethodFieldParameterEnum.InterestCalculationMethodOverride]: '',
  [MethodFieldParameterEnum.IntroductoryMessageDisplayControl]: ' ',
  [MethodFieldParameterEnum.PositiveAmortizationUsageCode]: ' ',

  //design loan offers - advanced introductory APR settings
  [MethodFieldParameterEnum.IntroductoryAnnualRate]: '',
  [MethodFieldParameterEnum.IntroductoryAnnualRateFixedEndDate]: '',
  [MethodFieldParameterEnum.IntroductoryRateNumberOfDays]: '',
  [MethodFieldParameterEnum.IntroductoryRateNumberOfCycles]: '',
  [MethodFieldParameterEnum.IntroductoryPeriodStatementTextID]: '',

  //design loan offers - advanced promotional cash settings
  [MethodFieldParameterEnum.SameAsCashLoanOfferNumberOfCycles]: '',
  [MethodFieldParameterEnum.AccrueInterestOnUnbilledInterest]: ' ',
  //design loan offers - advanced payment delay settings
  [MethodFieldParameterEnum.DelayPaymentForStatementCycles]: '',
  [MethodFieldParameterEnum.DelayPaymentForMonths]: ''
};

export const METHOD_PAYOFF_EXCEPTION_FIELDS = [
  MethodFieldParameterEnum.PayoffExceptionAmountToAvoidFinanceChargesSetting,
  MethodFieldParameterEnum.PayoffExceptionCashOptionPromotionCode,
  MethodFieldParameterEnum.PayoffExceptionCheckExceptionsDaysAfterPaymentDue,
  MethodFieldParameterEnum.PayoffExceptionCycleToDateCash,
  MethodFieldParameterEnum.PayoffExceptionCycleToDateMerchandise,
  MethodFieldParameterEnum.PayoffExceptionMethod,
  MethodFieldParameterEnum.PayoffExceptionMinimumBalanceToAssessInterestOn,
  MethodFieldParameterEnum.PayoffExceptionOldCash,
  MethodFieldParameterEnum.PayoffExceptionOneCycleOldMerchandise,
  MethodFieldParameterEnum.PayoffExceptionPartialGraceSettings,
  MethodFieldParameterEnum.PayoffExceptionStatementValue,
  MethodFieldParameterEnum.PayoffExceptionTwoCycleOldMerchandise
];

export const METHOD_PAYOFF_EXCEPTION_DEFAULT: Record<string, string> = {
  [MethodFieldParameterEnum.PayoffExceptionAmountToAvoidFinanceChargesSetting]:
    '',
  [MethodFieldParameterEnum.PayoffExceptionCashOptionPromotionCode]: '',
  [MethodFieldParameterEnum.PayoffExceptionCheckExceptionsDaysAfterPaymentDue]:
    '',
  [MethodFieldParameterEnum.PayoffExceptionCycleToDateCash]: '',
  [MethodFieldParameterEnum.PayoffExceptionCycleToDateMerchandise]: '',
  [MethodFieldParameterEnum.PayoffExceptionMethod]: '',
  [MethodFieldParameterEnum.PayoffExceptionMinimumBalanceToAssessInterestOn]:
    '',
  [MethodFieldParameterEnum.PayoffExceptionOldCash]: '',
  [MethodFieldParameterEnum.PayoffExceptionOneCycleOldMerchandise]: '',
  [MethodFieldParameterEnum.PayoffExceptionPartialGraceSettings]: '',
  [MethodFieldParameterEnum.PayoffExceptionStatementValue]: '',
  [MethodFieldParameterEnum.PayoffExceptionTwoCycleOldMerchandise]: ''
};

export const METHOD_DLO_P_DEFAULT: Record<string, string> = {
  [MethodFieldParameterEnum.DesignPromotionBasicInformation1]: '',
  [MethodFieldParameterEnum.DesignPromotionBasicInformation2]: '',
  [MethodFieldParameterEnum.DesignPromotionBasicInformation3]: ' ',
  [MethodFieldParameterEnum.DesignPromotionBasicInformation4]: ' ',
  [MethodFieldParameterEnum.DesignPromotionBasicInformation5]: ' ',
  [MethodFieldParameterEnum.DesignPromotionBasicInformation6]: ' ',
  [MethodFieldParameterEnum.DesignPromotionBasicInformation7]: ' ',
  [MethodFieldParameterEnum.DesignPromotionBasicInformation8]: ' ',
  [MethodFieldParameterEnum.DesignPromotionBasicInformation9]: ' ',
  [MethodFieldParameterEnum.DesignPromotionBasicInformation10]: ' ',
  [MethodFieldParameterEnum.DesignPromotionBasicInformation11]: ' ',
  [MethodFieldParameterEnum.DesignPromotionBasicInformation12]: ' ',
  [MethodFieldParameterEnum.DesignPromotionBasicInformation13]: '2',
  [MethodFieldParameterEnum.DesignPromotionInterestAssessment1]: '',
  [MethodFieldParameterEnum.DesignPromotionInterestAssessment2]: '',
  [MethodFieldParameterEnum.DesignPromotionInterestAssessment3]: '',
  [MethodFieldParameterEnum.DesignPromotionInterestAssessment4]: '',
  [MethodFieldParameterEnum.DesignPromotionInterestAssessment5]: '',
  [MethodFieldParameterEnum.DesignPromotionInterestAssessment6]: '',
  [MethodFieldParameterEnum.DesignPromotionInterestAssessment7]: '',
  [MethodFieldParameterEnum.DesignPromotionInterestAssessment8]: '',
  [MethodFieldParameterEnum.DesignPromotionInterestAssessment9]: ' ',
  [MethodFieldParameterEnum.DesignPromotionInterestAssessment10]: ' ',
  [MethodFieldParameterEnum.DesignPromotionInterestAssessment11]: ' ',
  [MethodFieldParameterEnum.DesignPromotionInterestAssessment12]: ' ',
  [MethodFieldParameterEnum.DesignPromotionInterestAssessment13]: ' ',
  [MethodFieldParameterEnum.DesignPromotionInterestAssessment14]: ' ',
  [MethodFieldParameterEnum.DesignPromotionInterestAssessment15]: ' ',
  [MethodFieldParameterEnum.DesignPromotionInterestAssessment16]: ' ',
  [MethodFieldParameterEnum.DesignPromotionInterestAssessment17]: ' ',
  [MethodFieldParameterEnum.DesignPromotionInterestAssessment18]: '',
  [MethodFieldParameterEnum.DesignPromotionInterestOverrides1]: ' ',
  [MethodFieldParameterEnum.DesignPromotionInterestOverrides2]: ' ',
  [MethodFieldParameterEnum.DesignPromotionInterestOverrides3]: ' ',
  [MethodFieldParameterEnum.DesignPromotionInterestOverrides4]: ' ',
  [MethodFieldParameterEnum.DesignPromotionInterestOverrides5]: ' ',
  [MethodFieldParameterEnum.DesignPromotionInterestOverrides6]: ' ',
  [MethodFieldParameterEnum.DesignPromotionInterestOverrides7]: ' ',
  [MethodFieldParameterEnum.DesignPromotionInterestOverrides8]: ' ',
  [MethodFieldParameterEnum.DesignPromotionRetailPath1]: ' ',
  [MethodFieldParameterEnum.DesignPromotionRetailPath2]: '',
  [MethodFieldParameterEnum.DesignPromotionRetailPath3]: '',
  [MethodFieldParameterEnum.DesignPromotionRetailPath4]: ' ',
  [MethodFieldParameterEnum.DesignPromotionReturnRevolve1]: '',
  [MethodFieldParameterEnum.DesignPromotionReturnRevolve2]: ' ',
  [MethodFieldParameterEnum.DesignPromotionReturnRevolve3]: ' ',
  [MethodFieldParameterEnum.DesignPromotionReturnRevolve4]: ' ',
  [MethodFieldParameterEnum.DesignPromotionReturnRevolve5]: '',
  [MethodFieldParameterEnum.DesignPromotionReturnRevolve6]: ' ',
  [MethodFieldParameterEnum.DesignPromotionStatementMessaging1]: ' ',
  [MethodFieldParameterEnum.DesignPromotionStatementMessaging2]: ' ',
  [MethodFieldParameterEnum.DesignPromotionStatementMessaging3]: ' ',
  [MethodFieldParameterEnum.DesignPromotionStatementMessaging4]: ' ',
  [MethodFieldParameterEnum.DesignPromotionStatementMessaging5]: ' ',

  [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter1]: ' ',
  [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter2]: '',
  [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter3]: ' ',
  [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter4]: ' ',
  [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter5]: ' ',
  [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter6]: '',
  [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter7]: ' ',
  [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter8]: ' ',
  [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter9]: ' ',
  [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter10]: ' ',
  [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter11]: ' ',
  [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter12]: ' ',
  [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter13]: ' ',
  [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter14]: ' ',
  [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter15]: ' ',
  [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter16]: ' ',
  [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter17]: ' ',
  [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter18]: ' ',
  [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter19]: ' ',
  [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter20]: ' ',
  [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter21]: ' ',
  [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter22]: ' ',
  [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter23]: ' ',
  [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter24]: ' ',
  [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter25]: ' ',
  [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter26]: ' ',
  [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter27]: ' ',
  [MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter28]: ' '
};

export const METHOD_UPDATE_INDEX_RATE_DEFAULT: Record<string, string> = {
  [MethodFieldParameterEnum.UpdateIndexRateCashIndexRate]: '',
  [MethodFieldParameterEnum.UpdateIndexRateEffectiveDateNewCash]: '',
  [MethodFieldParameterEnum.UpdateIndexRatePreviousCashIndexRate]: '',
  [MethodFieldParameterEnum.UpdateIndexRateMerchandiseIndexRate]: '',
  [MethodFieldParameterEnum.UpdateIndexRateEffectiveDateNewMerchandise]: '',
  [MethodFieldParameterEnum.UpdateIndexRatePreviousMerchandiseIndexRate]: '',
  [MethodFieldParameterEnum.UpdateIndexRateIndexType]: '',
  [MethodFieldParameterEnum.UpdateIndexRateDeterminationDateDescription]: '',
  [MethodFieldParameterEnum.UpdateIndexRateSourceDescription]: '',
  [MethodFieldParameterEnum.UpdateIndexRateChangePeriod]: ''
};

export const METHOD_CIT_FIELDS = [
  MethodFieldParameterEnum.ChangeInTermsBasicSettingsCitPriorityOrder,
  MethodFieldParameterEnum.ChangeInTermsBasicSettingsCisMemoSettings,
  MethodFieldParameterEnum.ChangeInTermsBasicSettingsTermsConditionsTable,
  MethodFieldParameterEnum.ChangeInTermsBasicSettingsCitPcs,
  MethodFieldParameterEnum.ChangeInTermsBasicSettingsEnableCit,
  MethodFieldParameterEnum.ChangeInTermsCommunicationLetterIdForActice,
  MethodFieldParameterEnum.ChangeInTermsCommunicationLetterIdForInActice,
  MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcBp,
  MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcId,
  MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIi,
  MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIm,
  MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIp,
  MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIr,
  MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcMe,
  MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcMf,
  MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcVi,
  MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoAc,
  MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoCi,
  MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoMc,
  MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoMi,
  MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPfLc,
  MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPfOc,
  MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPfRc,
  MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPoMp,
  MethodFieldParameterEnum.ChangeInTermsPenaltyPricingIndicator,
  MethodFieldParameterEnum.ChangeInTermsProtectedBalancesMethodForCash,
  MethodFieldParameterEnum.ChangeInTermsProtectedBalancesMethodForMerchandise,
  MethodFieldParameterEnum.ChangeInTermsProtectedBalancesMethodForNonInterest,
  MethodFieldParameterEnum.ChangeInTermsTimmingCitnumber,
  MethodFieldParameterEnum.ChangeInTermsTimmingDelayPeriod,
  MethodFieldParameterEnum.ChangeInTermsTimmingNumberOfMonthsOfDisclosureActiveAccounts,
  MethodFieldParameterEnum.ChangeInTermsTimmingNumberOfMonthsOfDisclosureInActiveAccounts,
  MethodFieldParameterEnum.ChangeInTermsProtectedBalanceDelayPeriod,
  MethodFieldParameterEnum.ChangeInTermsDisclosedMethodOverridesCpIcIdInterestDefaults,
  MethodFieldParameterEnum.ChangeInTermsDisclosedMethodOverridesCpIcIdInterestMethods,
  MethodFieldParameterEnum.ChangeInTermsReasonCode
];
export const METHOD_PBA_FIELDS = [
  MethodFieldParameterEnum.ProtectedBalancesAttributesCreditApplicationGroup,
  MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault1,
  MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault2,
  MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault3,
  MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod1,
  MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod2,
  MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod3,
  MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment1,
  MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment2,
  MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment3,
  MethodFieldParameterEnum.ProtectedBalancesAttributesCreditApplicationGroup,
  MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault1,
  MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault2,
  MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault3,
  MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod1,
  MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod2,
  MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod3,
  MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment1,
  MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment2,
  MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment3,
  MethodFieldParameterEnum.ProtectedBalancesAttributesDescription,
  MethodFieldParameterEnum.ProtectedBalancesAttributesAlternateDescription,
  MethodFieldParameterEnum.ProtectedBalancesAttributesDescriptionDisplayOption,
  MethodFieldParameterEnum.ProtectedBalancesAttributesMessageText,
  MethodFieldParameterEnum.ProtectedBalancesAttributesMessageOptions,
  MethodFieldParameterEnum.ProtectedBalancesAttributesProtectBalancesForPenaltyPricing,
  MethodFieldParameterEnum.ProtectedBalancesAttributesDelayIntroTextID,
  MethodFieldParameterEnum.ProtectedBalancesAttributesPBBalanceTransfer,
  MethodFieldParameterEnum.ProtectedBalancesAttributesProtectedBalanceOverrideCode,
  MethodFieldParameterEnum.ProtectedBalancesAttributesGroupIdentifier
];

export const METHOD_FRAUD_FIELDS = [
  MethodFieldParameterEnum.BulkFraudTransaction
];
export const METHOD_CYCLE_ACCOUNTS_FIELDS = [
  MethodFieldParameterEnum.CycleAccountsTestEnvironmentTransaction,
  MethodFieldParameterEnum.CycleAccountsTestEnvironmentCode
];

export const METHOD_MC_FIELDS = [
  MethodFieldParameterEnum.ManageIncomeOptionMethodsMcJoiningFeeAmount,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsMcJoiningFeeStatementDescription,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsMcJoiningFeeBatchId,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsMcCardReplacementFeeAmount,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsMcCardReplacementFeeBatchId,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsMcCardReplacementFeeStatementDescription,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsMcMiscellaneousFeeChargeId,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsMcFirstYearMaximumFeeManagement
];

export const METHOD_AC_FIELDS = [
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualChargeStatementDisplayControl,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPreAnnualChargeNotifications,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcRegulationZNotificationMediaMethod,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcRegulationZnotificationGenerationPeriod,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPreNotificationMessageId,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine1,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine2Before,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine2After,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine3,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine4,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine5,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine6,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine7,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine8,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine9,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine10,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine11,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine12,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualChargeStatementDescription,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcBatchTypeCode,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcIncludeAnnualChargeInInterestCalculations,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPreventDelinquencyFromAnnualFeeBalance,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualChargeOption,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFirstYearMaximumFeeManagementOption,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPrenotificationAmountChangeOptions,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount1,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount2,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount3,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount4,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount5,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount6,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAdditionalPlasticCharge,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPlasticsRequirementCode,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcSpecialConditionsForExternalStatus,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcSpecialConditionsForExpiredAccounts,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualChargeIncludeTable,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFinanceChargeSuppressionCode,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcNoPrenotificationAssessmentOptions,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcExistingAccountStartMonthOption,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcNumberOfMonthsToFirstCharge,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPostingCycleOptions,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeTimingOptions,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcSpecifiedMonthForAnnualCharge,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeWaiverMonths,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcSecondAnnualFeeNumberOfDays,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcDebitActivityOption,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcWaiverTotalDollarCalculationOption,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcRateChangePeriod,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeWaiverOption,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcMinimumBalanceAndExternalStatusOption,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeWaiverAmount,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatusAMinimumBalance,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatusCMinimumBalance,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatusEMinimumBalance,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatusFMinimumBalance,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatusBlankMinimumBalance,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcReversalStatementMessageWarningTextID,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcReversalNumberOfCycles,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcReversalReasonCode,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcReversalStatementMessageWarningOption,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualFeeReversalBatchID
];

export const METHOD_INCOME_DEFAULT: Record<string, string> = {
  //Ac - Statement and Notification Display
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualChargeStatementDisplayControl]:
    '',
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPreAnnualChargeNotifications]:
    '',
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcRegulationZNotificationMediaMethod]:
    '',
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcRegulationZnotificationGenerationPeriod]:
    '',
  // //Ac - Assessment Calculations
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcBatchTypeCode]: '',
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPreventDelinquencyFromAnnualFeeBalance]:
    '',
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualChargeOption]: '',
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFirstYearMaximumFeeManagementOption]:
    '',
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPrenotificationAmountChangeOptions]:
    '',
  //AC - Account Eligibility
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPlasticsRequirementCode]:
    '',
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcSpecialConditionsForExternalStatus]:
    '',
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcSpecialConditionsForExpiredAccounts]:
    '',
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualChargeIncludeTable]:
    '',
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFinanceChargeSuppressionCode]:
    '',
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcNoPrenotificationAssessmentOptions]:
    '',
  //AC - Assessment Timing
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcExistingAccountStartMonthOption]:
    '',
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcNumberOfMonthsToFirstCharge]:
    '',
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPostingCycleOptions]: '',
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeTimingOptions]: '',
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcSpecifiedMonthForAnnualCharge]:
    '',
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeWaiverMonths]: '',
  //AC - Advanced Parameters - Assessment Calculations
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcDebitActivityOption]: '',
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcWaiverTotalDollarCalculationOption]:
    '',
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcRateChangePeriod]: '',
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeWaiverOption]: '',
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcMinimumBalanceAndExternalStatusOption]:
    ''
};

export const PORTFOLIO_ATTRIBUTE_FIELDS: string[] = [
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeFilterBy,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeOperator,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeSysPrinAgents,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeBin,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributePortfolioId,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeStateCode,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeSynsO,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributePrinO,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeAgentO,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeBinO,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeCompanyIdO,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeCompanyNameO,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeDatalinkElO,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributePortfolioIdO,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributePricingStrateryO,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeProductTypeCodeO,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeStateCodeO,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneousf1t10O,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneousf11t15O,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneousf16t20O,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneousf21t25O,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous26O,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous27,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous28,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous29,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous30,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous31,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous32,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous33,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous34,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous35,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous36,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous37,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous38,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous39,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous40,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous41,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous42,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous43,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous44,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous45,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous46,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous47,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous48,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous49,
  MethodFieldParameterEnum.PortfolioDefinitionsAttributeMiscellaneous50
];
