export const ButtonActionType = {
  openWorkflow: 'openWorkflow',
  unFavoriteWorkflow: 'unFavoriteWorkflow',
  favoriteWorkflow: 'favoriteWorkflow',
  editWorkflow: 'editWorkflow',
  downloadDMMTable: 'downloadDMMTable',
  openStrategyFlyout: 'openStrategyFlyout',
  selectKpi: 'selectKpi'
};
