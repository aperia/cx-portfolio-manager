export const appSetting = {
  apiUrl: process.env.REACT_APP_API || '/api',
  publicUrl: process.env.PUBLIC_URL || '/',
  apiToken: process.env.REACT_APP_API_TOKEN || '',
  notificationInterval: process.env.REACT_APP_NOTIFICATION_INTERVAL || 20,
  fileVerifiedStatuses:
    process.env.REACT_APP_FILE_VERIFIED_STATUSES || 'Valid,Invalid'
};

export const appFeatureFlag = {
  notificationOff: Boolean(process.env.REACT_APP_NOTIFICATION_OFF)
};
