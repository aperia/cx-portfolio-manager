export const NOTIFICATION_DETAILS_ACTIONS = [
  'changesetsubmittedforapproval',
  'changesetapproved',
  'changesetrejected',
  'changesetlapsed',
  'changesetsubmittedforvalidation',
  'validationcompleted'
];
export const NOTIFICATION_REMINDER_ACTIONS = ['addedreminder'];

export const NOTIFICATION_ACTIONS = [
  ...NOTIFICATION_DETAILS_ACTIONS,
  ...NOTIFICATION_REMINDER_ACTIONS
];
