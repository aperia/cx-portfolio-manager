import { isProduction } from 'app/_libraries/_dls/utils';
import { appSetting } from './configurations';

// Links controlled by the application
export const BASE_URL = '/';
export const CHANGE_MANAGEMENT_URL = `${BASE_URL}change-management`;
export const CHANGE_DETAIL_URL = `${BASE_URL}change-management/detail/:id`;
export const PENDING_APPROVALS_URL = `${BASE_URL}pending-approvals`;
export const WORKFLOWS_URL = `${BASE_URL}workflows`;
export const MY_IN_PROGRESS_WORKFLOWS_URL = `${BASE_URL}my-in-progress-workflows`;
export const CURRENT_HREF =
  window.location.href.slice(-1) !== `${BASE_URL}`
    ? `${window.location.href}${BASE_URL}`
    : `${window.location.href}`;
export const CURRENT_HREF_FULL =
  CURRENT_HREF.slice(-2) !== `#${BASE_URL}`
    ? `${CURRENT_HREF}#${BASE_URL}`
    : `${CURRENT_HREF}`;

export const changeDetailLink = (id: string) =>
  `${BASE_URL}change-management/detail/${id}`;
export const changeDetailLinkFromImportExportWF = (id: string) =>
  `${
    isProduction() ? appSetting.publicUrl : BASE_URL
  }#/change-management/detail/${id}?notifyFrom=import-export-workflow`;
export const MY_PORTFOLIOS_URL = `${BASE_URL}my-portfolios`;
export const MY_PORTFOLIOS_DETAIL_URL = `${BASE_URL}my-portfolios/detail/:id`;
export const myPortfoliosDetail = (id: string) =>
  `${BASE_URL}my-portfolios/detail/${id}`;
