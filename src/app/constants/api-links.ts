export const getFileDetailsURL = 'fs/portfolio/v1/changeSets/getFileDetails';
export const deleteTemplateFileURL = 'fs/portfolio/v1/changeSets/deleteFile';
export const uploadTemplateFileURL = 'fs/portfolio/v1/changeSets/uploadFile';
export const downloadTemplateFileUrl =
  'fs/portfolio/v1/changeSets/downloadTemplateFile';
export const getActivitiesUrl = 'fs/portfolio/v1/changeSets/getActivityList';
export const getNotificationsUrl =
  'fs/portfolio/v1/changeSets/getNotificationList';
export const updateNotificationUrl =
  'fs/portfolio/v1/changeSets/updateNotification';

export const changeSetListUrl = 'fs/portfolio/v1/changeSets/getChangeSetList';
export const approvalQueueListUrl =
  'fs/portfolio/v1/changeSets/getPendingApprovalQueueList';
export const changeDetailsUrl =
  'fs/portfolio/v1/changeSets/getChangeSetDetails';
export const pricingMethodListlUrl =
  'fs/portfolio/v1/changeSets/getPricingMethodList';
export const pricingStrategyListUrl =
  'fs/portfolio/v1/changeSets/getPricingStrategyList';
export const changeAttachmentListUrl =
  'fs/portfolio/v1/changeSets/getChangeSetAttachmentList';
export const changeOwnerListUrl =
  'fs/portfolio/v1/changeSets/getPossibleOwnersList';
export const dateInformationUrl =
  'fs/portfolio/v1/changeSets/getChangeSetEffectiveDateOptions';
export const updateChangeDetailUrl =
  'fs/portfolio/v1/changeSets/updateChangeSet';
export const reScheduleChangeUrl =
  'fs/portfolio/v1/changeSets/rescheduleChangeSet';
export const processChangeUrl = 'fs/portfolio/v1/changeSets/processChangeSet';
export const deleteChangeUrl = 'fs/portfolio/v1/changeSets/deleteChangeSet';
export const exportChangeSetUrl = 'fs/portfolio/v1/changeSets/exportChangeSet';

export const portfolioListUrl = 'fs/portfolio/v1/portfolios/getPortfolioList';
export const portfolioDetailUrl =
  'fs/portfolio/v1/changeSets/getPortfolioDetails';
export const getKPIListForSetupUrl =
  'fs/portfolio/v1/changeSets/getKPISubjectAreaList';
export const getKPIDetailsUrl = 'fs/portfolio/v1/changeSets/getKPIDetails';
export const addPortfolioKPIUrl = 'fs/portfolio/v1/changeSets/addPortfolioKPI';
export const updatePortfolioKPIUrl =
  'fs/portfolio/v1/changeSets/updatePortfolioKPI';
export const deletePortfolioKPIUrl =
  'fs/portfolio/v1/changeSets/deletePortfolioKPI';

export const getServiceSubjectSectionOptionsUrl =
  'fs/portfolio/v1/changeSets/getServiceSubjectSectionOptions';
export const getTableListForWorkflowSetupUrl =
  'fs/portfolio/v1/changeSets/getTableListForWorkflowSetup';
export const exportAnalyzerUrl = 'fs/portfolio/v1/changeSets/exportAnalyzer';

export const getWorkflowEffectiveDateOptionsURL =
  'fs/portfolio/v1/changeSets/getWorkflowEffectiveDateOptions';
export const workflowTCLOURL = 'fs/portfolio/v1/workflows/tclo';
export const workflowTemplateList =
  'fs/portfolio/v1/workflows/getWorkflowTemplateList';
export const workflowListUrl = 'fs/portfolio/v1/changeSets/getWorkflowList';
export const inProgressWorkflowListUrl =
  'fs/portfolio/v1/changeSets/getInProgressWorkflowList';
export const workflowDetailsUrl =
  'fs/portfolio/v1/changeSets/getWorkflowDetails';
export const removeWorkflowUrl =
  'fs/portfolio/v1/changeSets/deleteWorkflowInstance';
export const favoriteWorkflowUrl = 'fs/portfolio/v1/workflows/favoriteWorkflow';
export const unfavoriteWorkflowUrl =
  'fs/portfolio/v1/workflows/unfavoriteWorkflowTemplate';
export const setReminderWorkflowUrl = 'fs/portfolio/v1/workflows/addReminder';
export const getWorkflowChangeSetListUrl =
  'fs/portfolio/v1/changeSets/getChangeSetListForWorkflowSetup';
export const getWorkflowMethodsUrl =
  'fs/portfolio/v1/changeSets/getMethodListForWorkflowSetup';
export const getWorkflowStrategiesUrl =
  'fs/portfolio/v1/changeSets/getPricingStrategyListForArchive';
export const getWorkflowTextAreaForArchiveUrl =
  'fs/portfolio/v1/changeSets/getTextIDListForArchive';
export const getWorkflowTableAreaForArchiveUrl =
  'fs/portfolio/v1/changeSets/getTableListForArchive';
export const getElementDataByNameUrl =
  'fs/portfolio/v1/changeSets/getElementDataByName';
export const createChangeSetUrl = 'fs/portfolio/v1/changeSets/createChangeSet';
export const createWorkflowInstanceUrl =
  'fs/portfolio/v1/changeSets/createWorkflowInstance';
export const updateWorkflowInstanceUrl =
  'fs/portfolio/v1/changeSets/updateWorkflowInstance';
export const getWorkflowInstanceUrl =
  'fs/portfolio/v1/changeSets/getWorkflowInstanceData';
export const updateWorkflowInstanceDataUrl =
  'fs/portfolio/v1/changeSets/updateWorkflowInstanceData';

export const dmmListUrl = 'fs/portfolio/v1/changeSets/getChangeSetDmmList';

export const downloadDecisionElementsUrl =
  'fs/portfolio/v1/changeSets/downloadDecisionElement';

export const downloadAttachmentUrl = 'fs/portfolio/v1/changeSets/downloadFile';

export const pendingApprovalGroupUrl =
  'fs/portfolio/v1/changeSets/getPendingApprovalGroupList';

export const getPricingStrategyListForWorkflowSetupUrl =
  'fs/portfolio/v1/changeSets/getPricingStrategyListForWorkflowSetup';

export const getTableListUrl =
  'fs/portfolio/v1/changeSets/getTableListForWorkflowSetup';

export const getDecisionListUrl =
  'fs/portfolio/v1/changeSets/getDecisionElementList';

export const getStrategiesListAQ =
  'fs/portfolio/v1/changeSets/getPricingStrategyListForArchive';

export const getDecisionElementUrl =
  'fs/portfolio/v1/changeSets/getDecisionElementList';

export const performMonetaryAdjustmentTransactionUrl =
  'fs/portfolio/v1/changeSets/getTransactionList';

//flexible monetary transactions
export const getTransactionListUrl =
  'fs/portfolio/v1/changeSets/getTransactionList';

export const getSPAListUrl =
  'fs/portfolio/v1/changeSets/getSysPrinAgentRecordTypeList';

export const getOCSShellListUrl = '/fs/portfolio/v1/changeSets/getOcsShellList';

export const getMerchantListUrl = 'fs/portfolio/v1/changeSets/getMerchantList';

export const exportMerchantsUrl = 'fs/portfolio/v1/changeSets/exportMerchants';

export const getSPAsUrl = 'fs/portfolio/v1/changeSets/getSPAs';

export const getLettersUrl = 'fs/portfolio/v1/changeSets/getLetters';

export const getQueueProfileUrl = 'fs/portfolio/v1/changeSets/getQueueProfile';

export const getOcsShellProfileUrl =
  'fs/portfolio/v1/changeSets/getOcsShellProfile';

export const exportShellProfileUrl =
  'fs/portfolio/v1/changeSets/exportShellProfile';

export const getProfileTransactionsUrl =
  'fs/portfolio/v1/changeSets/getOcsTransactionActions';

export const getSupervisorListUrl =
  '/fs/portfolio/v1/changeSets/getSupervisorList';

export const MOCK_API_URL = [];

export const ACTIVITY_LIST_FIELDS = ['activityList', 'total'];

export const NOTIFICATION_LIST_FIELDS = ['notifications', 'total'];

export const CHANGE_LIST_FIELDS = ['changeSetList', 'total'];
export const CHANGE_DETAIL_SNAPSHOT_FIELDS = ['changeSetDetail'];
export const CHANGE_DETAIL_PRICING_METHODS_FIELDS = [
  'pricingMethodList',
  'total'
];
export const CHANGE_OWNER_LIST_FIELDS = ['users', 'total'];
export const CHANGE_DATE_INFORMATION_FIELDS = [
  'minDate',
  'maxDate',
  'invalidDates'
];
export const CHANGE_DETAIL_PRICING_STRATEGIES_FIELDS = [
  'pricingStrategyList',
  'total'
];
export const CHANGE_ACTION = {
  Approve: 'APPROVE',
  Reject: 'REJECT',
  Demote: 'DEMOTE',
  Validate: 'VALIDATE',
  Delete: 'DELETE',
  Submit: 'SUBMITFORAPPROVAL'
};

export const PORTFOLIO_LIST_FIELDS = ['portfolioList', 'total'];

export const WORKFLOW_TEMPLATE_FIELDS = ['workflowTemplateList', 'total'];
export const WORKFLOW_INSTANCE_FIELDS = ['workflowInstanceList', 'total'];
export const WORKFLOW_INSTANCE_DETAIL_FIELDS = ['workflowInstanceDetails'];

export const CHANGSET_ATTACHMENT_FIELDS = ['changeSetAttachment', 'reports'];
export const DMM_LIST_FIELDS = ['dmmList', 'total'];
export const DOWNLOAD_ATTACHMENT_FIELDS = ['attachment'];
export const PENDING_APPROVAL_GROUP_FIELDS = ['pendingApprovals'];

export const changeNotFoundDetectUrls = [
  // change details
  changeDetailsUrl,
  processChangeUrl,
  deleteChangeUrl,
  workflowListUrl,
  exportChangeSetUrl,
  getActivitiesUrl,
  changeAttachmentListUrl,
  pendingApprovalGroupUrl,
  pricingStrategyListUrl,
  pricingMethodListlUrl,
  dmmListUrl,
  downloadDecisionElementsUrl,
  dateInformationUrl,
  changeOwnerListUrl,
  updateChangeDetailUrl,
  reScheduleChangeUrl,
  workflowDetailsUrl,
  // workflow setup/edit
  downloadAttachmentUrl,
  deleteTemplateFileURL,
  // TODO: need response error
  // uploadTemplateFileURL,
  getFileDetailsURL,
  removeWorkflowUrl,
  getWorkflowInstanceUrl,
  updateWorkflowInstanceDataUrl,
  getWorkflowEffectiveDateOptionsURL,
  downloadTemplateFileUrl,

  createWorkflowInstanceUrl,
  updateWorkflowInstanceUrl
];
export const changeNotFoundDetectEmptyResponseUrls = [
  getWorkflowInstanceUrl,
  updateWorkflowInstanceUrl,
  updateWorkflowInstanceDataUrl
];
export const changeNotFoundSelectDeletedChangeUrls = [
  updateWorkflowInstanceUrl,
  updateWorkflowInstanceDataUrl
];
