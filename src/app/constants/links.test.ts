import {
  BASE_URL,
  changeDetailLink,
  changeDetailLinkFromImportExportWF,
  myPortfoliosDetail
} from './links';

describe('test links', () => {
  it('test function changeDetailLink', () => {
    const value = changeDetailLink('name');
    expect(`${BASE_URL}change-management/detail/name`).toEqual(value);
  });
  it('test function changeDetailLinkFromImportExportWF', () => {
    const value = changeDetailLinkFromImportExportWF('id');
    expect(
      `${BASE_URL}#/change-management/detail/id?notifyFrom=import-export-workflow`
    ).toEqual(value);
  });
  it('test function myPortfoliosDetail', () => {
    const value = myPortfoliosDetail('name');
    expect(`${BASE_URL}my-portfolios/detail/name`).toEqual(value);
  });
});
