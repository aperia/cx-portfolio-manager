import { render } from '@testing-library/react';
import React from 'react';
import {
  dynamicActionCardCompromiseParameterList,
  generateActionCardCompromiseTitle,
  METHOD_ACCE_FIELDS_ACCOUNT_TRANSFER,
  METHOD_ACCE_FIELDS_LOST_DEFAULT,
  METHOD_ACCE_FIELDS_MASTERCARD_NEGATIVE_DEFAULT,
  METHOD_ACCE_FIELDS_MASTERCARD_WARNING_DEFAULT,
  METHOD_ACCE_FIELDS_PIIDS_LOST_DEFAULT,
  METHOD_ACCE_FIELDS_PIIDS_STOLEN_DEFAULT,
  METHOD_ACCE_FIELDS_PIIDS_TRANSFER,
  METHOD_ACCE_FIELDS_STOLEN_DEFAULT,
  METHOD_ACCE_FIELDS_VISA_DEFAULT,
  renderMappingForm,
  templateACCE
} from './helper';
import {
  accountTransfer,
  masterCardNegativeBulletinsParameterList,
  masterCardWarningBulletinsParameterList,
  piidTransfer,
  reportLostPIIDs,
  reportStolenAccountsParameterList,
  reportStolenPIIDs,
  visaWarningBulletinsParameterList
} from './newParameterList';

describe('templateACCE function', () => {
  it('should run templateACCE function with dynamic data', () => {
    const example = templateACCE(
      'Report Lost Accounts using the SL, Lost/Stolen Report transaction.',
      'Visa exception file using the WC, Visa Warning Bulletins transaction.'
    );

    expect(example).toEqual({
      template1: METHOD_ACCE_FIELDS_LOST_DEFAULT,
      template2: METHOD_ACCE_FIELDS_VISA_DEFAULT
    });
  });
  it('should run templateACCE function with dynamic data', () => {
    const example = templateACCE(
      'Report Stolen Accounts using the SL, Lost/Stolen Report transaction.',
      'MasterCard negative file using the WMA, Add Cardholder Account transaction.'
    );

    expect(example).toEqual({
      template1: METHOD_ACCE_FIELDS_STOLEN_DEFAULT,
      template2: METHOD_ACCE_FIELDS_MASTERCARD_NEGATIVE_DEFAULT
    });
  });
  it('should run templateACCE function with dynamic data', () => {
    const example = templateACCE(
      'Report Lost PIIDs using the SLP, Lost/Stolen Report transaction.',
      'MasterCard warning bulletin using the WB, MasterCard Warning Bulletins transaction.'
    );

    expect(example).toEqual({
      template1: METHOD_ACCE_FIELDS_PIIDS_LOST_DEFAULT,
      template2: METHOD_ACCE_FIELDS_MASTERCARD_WARNING_DEFAULT
    });
  });
  it('should run templateACCE function with dynamic data', () => {
    const example = templateACCE(
      'Report Stolen PIIDs using the SLP, Lost/Stolen Report transaction.',
      ''
    );

    expect(example).toEqual({
      template1: METHOD_ACCE_FIELDS_PIIDS_STOLEN_DEFAULT,
      template2: {}
    });
  });

  it('should run templateACCE function with dynamic data', () => {
    const example = templateACCE(
      'Account Transfer using the AT, Account Transfers transaction.',
      ''
    );

    expect(example).toEqual({
      template1: METHOD_ACCE_FIELDS_ACCOUNT_TRANSFER,
      template2: {}
    });
  });

  it('should run templateACCE function with dynamic data', () => {
    const example = templateACCE(
      'PIID Transfer using the PIT, Presentation Instrument Transfer transaction.',
      ''
    );

    expect(example).toEqual({
      template1: METHOD_ACCE_FIELDS_PIIDS_TRANSFER,
      template2: {}
    });
  });
});

describe('generateActionCardCompromiseTitle function', () => {
  it('should render ui with dynamic parameters', () => {
    const example = generateActionCardCompromiseTitle(
      'Visa exception file using the WC, Visa Warning Bulletins transaction.'
    );
    expect(example).toEqual('WC, Visa Warning Bulletins');
  });
  it('should render ui with dynamic parameters', () => {
    const example = generateActionCardCompromiseTitle(
      'MasterCard negative file using the WMA, Add Cardholder Account transaction.'
    );
    expect(example).toEqual('WMA, Add Cardholder Account');
  });
  it('should render ui with dynamic parameters', () => {
    const example = generateActionCardCompromiseTitle(
      'MasterCard warning bulletin using the WB, MasterCard Warning Bulletins transaction.'
    );
    expect(example).toEqual('WB, MasterCard Warning Bulletins');
  });
  it('should render ui with dynamic parameters', () => {
    const example = generateActionCardCompromiseTitle(
      'Report Lost Accounts using the SL, Lost/Stolen Report transaction.'
    );
    expect(example).toEqual('SL, Lost/Stolen Report');
  });
  it('should render ui with dynamic parameters', () => {
    const example = generateActionCardCompromiseTitle(
      'Report Stolen Accounts using the SL, Lost/Stolen Report transaction.'
    );
    expect(example).toEqual('SL, Lost/Stolen Report');
  });
  it('should render ui with dynamic parameters', () => {
    const example = generateActionCardCompromiseTitle(
      'Report Lost PIIDs using the SLP, Lost/Stolen Report transaction.'
    );
    expect(example).toEqual('SLP, Lost/Stolen Report');
  });
  it('should render ui with dynamic parameters', () => {
    const example = generateActionCardCompromiseTitle(
      'Report Stolen PIIDs using the SLP, Lost/Stolen Report transaction.'
    );
    expect(example).toEqual('SLP, Lost/Stolen Report');
  });
  it('should render ui with dynamic parameters', () => {
    const example = generateActionCardCompromiseTitle(
      'Account Transfer using the AT, Account Transfers transaction.'
    );
    expect(example).toEqual('AT, Account Transfers');
  });
  it('should render ui with dynamic parameters', () => {
    const example = generateActionCardCompromiseTitle(
      'PIID Transfer using the PIT, Presentation Instrument Transfer transaction.'
    );
    expect(example).toEqual('PIT, Presentation Instrument Transfer');
  });
});

describe('dynamicActionCardCompromiseParameterList function', () => {
  it('should render ui with dynamic parameters', () => {
    const example = dynamicActionCardCompromiseParameterList(
      'Visa exception file using the WC, Visa Warning Bulletins transaction.'
    );
    expect(example).toEqual(visaWarningBulletinsParameterList);
  });
  it('should render ui with dynamic parameters', () => {
    const example = dynamicActionCardCompromiseParameterList(
      'MasterCard negative file using the WMA, Add Cardholder Account transaction.'
    );
    expect(example).toEqual(masterCardNegativeBulletinsParameterList);
  });
  it('should render ui with dynamic parameters', () => {
    const example = dynamicActionCardCompromiseParameterList(
      'MasterCard warning bulletin using the WB, MasterCard Warning Bulletins transaction.'
    );
    expect(example).toEqual(masterCardWarningBulletinsParameterList);
  });
  it('should render ui with dynamic parameters', () => {
    const example = dynamicActionCardCompromiseParameterList(
      'Report Lost Accounts using the SL, Lost/Stolen Report transaction.'
    );
    expect(example).toEqual(reportStolenAccountsParameterList.slice(0, 10));
  });
  it('should render ui with dynamic parameters', () => {
    const example = dynamicActionCardCompromiseParameterList(
      'Report Stolen Accounts using the SL, Lost/Stolen Report transaction.'
    );
    expect(example).toEqual(reportStolenAccountsParameterList);
  });
  it('should render ui with dynamic parameters', () => {
    const example = dynamicActionCardCompromiseParameterList(
      'Report Lost PIIDs using the SLP, Lost/Stolen Report transaction.'
    );
    expect(example).toEqual(reportLostPIIDs);
  });
  it('should render ui with dynamic parameters', () => {
    const example = dynamicActionCardCompromiseParameterList(
      'Report Stolen PIIDs using the SLP, Lost/Stolen Report transaction.'
    );
    expect(example).toEqual(reportStolenPIIDs);
  });
  it('should render ui with dynamic parameters', () => {
    const example = dynamicActionCardCompromiseParameterList(
      'Account Transfer using the AT, Account Transfers transaction.'
    );
    expect(example).toEqual(accountTransfer);
  });
  it('should render ui with dynamic parameters', () => {
    const example = dynamicActionCardCompromiseParameterList(
      'PIID Transfer using the PIT, Presentation Instrument Transfer transaction.'
    );
    expect(example).toEqual(piidTransfer);
  });
});

describe('renderMappingForm render ', () => {
  const data = { id: 'purge.date' };
  const t = jest.fn();
  const metadata = {
    externalStatus: [{ code: 'name', description: 'type' }],
    pinLostIndicator: [{ code: 'name', description: 'type' }],
    fraudLossOperator: [{ code: 'name', description: 'type' }],
    possibleFraudActivityCode: [{ code: 'name', description: 'type' }],
    fraudTypeCode: [{ code: 'name', description: 'type' }],
    lostLocationCode: [{ code: 'name', description: 'type' }],
    rushPlasticCode: [{ code: 'name', description: 'type' }],
    visaWarningBulletinRegion: [{ code: 'name', description: 'type' }],
    visaResponseCode: [{ code: 'name', description: 'type' }],
    producePlasticIndicatorSuppress: [{ code: 'name', description: 'type' }],
    producePlasticIndicatorPlastic: [{ code: 'name', description: 'type' }],
    piStatusCode: [{ code: 'name', description: 'type' }],
    inasReasonCode: [{ code: 'name', description: 'type' }],
    masterCardStatus: [{ code: 'name', description: 'type' }],
    masterCardWarningBulletinRegion: [{ code: 'name', description: 'type' }],
    accountTransferTypeCode: [{ code: 'name', description: 'type' }],
    letterIdentifier: [{ code: 'name', description: 'type' }],
    rushMailCode: [{ code: 'name', description: 'type' }],
    lossOperatorId: [{ code: 'name', description: 'type' }],
    typeLossCode: [{ code: 'name', description: 'type' }],
    producePlasticIndicatorPLGEN: [{ code: 'name', description: 'type' }]
  };
  const ACCE = {
    'purge.date': '12/12/2022',
    'stolen.date': '12/12/2022',
    'lost.date': '12/12/2022',
    'date.of.event': '12/12/2022'
  };
  const minDate = new Date();
  const maxDate = new Date();
  const handleFormChange = jest.fn();

  it('should render purge date', () => {
    const Component = () => {
      return renderMappingForm(
        data,
        t,
        metadata,
        ACCE,
        minDate,
        maxDate,
        handleFormChange
      );
    };
    const wrapper = render(<Component />);

    expect(wrapper.getByRole('textbox')).toBeInTheDocument();
  });
  it('should render stolen date', () => {
    const Component = () => {
      return renderMappingForm(
        { ...data, id: 'stolen.date' },
        t,
        metadata,
        ACCE,
        minDate,
        maxDate,
        handleFormChange
      );
    };
    const wrapper = render(<Component />);

    expect(wrapper.getByRole('textbox')).toBeInTheDocument();
  });
  it('should render lost date', () => {
    const Component = () => {
      return renderMappingForm(
        { ...data, id: 'lost.date' },
        t,
        metadata,
        ACCE,
        minDate,
        maxDate,
        handleFormChange
      );
    };
    const wrapper = render(<Component />);

    expect(wrapper.getByRole('textbox')).toBeInTheDocument();
  });
  it('should render date of event', () => {
    const Component = () => {
      return renderMappingForm(
        { ...data, id: 'date.of.event' },
        t,
        metadata,
        ACCE,
        minDate,
        maxDate,
        handleFormChange
      );
    };
    const wrapper = render(<Component />);

    expect(wrapper.getByRole('textbox')).toBeInTheDocument();
  });
});
