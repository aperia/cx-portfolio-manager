import userEvent from '@testing-library/user-event';
import { renderWithMockStore } from 'app/utils';
import 'app/_libraries/_dls/test-utils/mocks/mockCanvas';
import * as CommonRedux from 'pages/_commons/redux/Common/select-hooks';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowActionCardCompromiseEvent';
import React from 'react';
import * as ReactRedux from 'react-redux';
import DynamicGrid from './DynamicGrid';
const mockUseDispatch = jest.spyOn(ReactRedux, 'useDispatch');
const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({ t })
  };
});

const mockUseSelectWindowDimension = jest.spyOn(
  CommonRedux,
  'useSelectWindowDimension'
);

const useSelectElementMetadataForActionCardCompromiseEvent = jest.spyOn(
  WorkflowSetup,
  'useSelectElementMetadataForActionCardCompromiseEvent'
);

const mockUseSelectWindowDimensionImplementation = (width: number) => {
  mockUseSelectWindowDimension.mockImplementation(() => ({ width } as any));
};

describe('Action Card Compromise Event > DynamicGrid', () => {
  beforeEach(() => {
    mockUseDispatch.mockReturnValue(jest.fn());
    mockUseSelectWindowDimensionImplementation(120);
    useSelectElementMetadataForActionCardCompromiseEvent.mockReturnValue({
      externalStatus: [{ code: 'name', description: 'type' }],
      pinLostIndicator: [{ code: 'name', description: 'type' }],
      fraudLossOperator: [{ code: 'name', description: 'type' }],
      possibleFraudActivityCode: [{ code: 'name', description: 'type' }],
      fraudTypeCode: [{ code: 'name', description: 'type' }],
      lostLocationCode: [{ code: 'name', description: 'type' }],
      rushPlasticCode: [{ code: 'name', description: 'type' }],
      visaWarningBulletinRegion: [{ code: 'name', description: 'type' }],
      visaResponseCode: [{ code: 'name', description: 'type' }],
      producePlasticIndicatorSuppress: [{ code: 'name', description: 'type' }],
      producePlasticIndicatorPlastic: [{ code: 'name', description: 'type' }],
      piStatusCode: [{ code: 'name', description: 'type' }],
      inasReasonCode: [{ code: 'name', description: 'type' }],
      masterCardStatus: [{ code: 'name', description: 'type' }],
      masterCardWarningBulletinRegion: [{ code: 'name', description: 'type' }],
      accountTransferTypeCode: [{ code: 'name', description: 'type' }],
      letterIdentifier: [{ code: 'name', description: 'type' }],
      rushMailCode: [{ code: 'name', description: 'type' }],
      lossOperatorId: [{ code: 'name', description: 'type' }],
      typeLossCode: [{ code: 'name', description: 'type' }],
      producePlasticIndicatorPLGEN: [{ code: 'name', description: 'type' }]
    } as any);
  });
  afterEach(() => {
    mockUseSelectWindowDimension.mockClear();
    jest.clearAllMocks();
  });
  const props = { setFormValues: jest.fn() } as any;
  it('should render DynamicGrid', async () => {
    const wrapper = await renderWithMockStore(<DynamicGrid {...props} />, {});

    expect(
      wrapper.getByPlaceholderText('txt_type_to_search')
    ).toBeInTheDocument();
  });

  it('should render DynamicGrid with dynamic dependencies', async () => {
    const newProps = {
      ...props,
      selectedQuestion:
        'Report Lost Accounts using the SL, Lost/Stolen Report transaction.',
      gridTitle: 'SL, Lost/Stolen Report'
    };
    const wrapper = await renderWithMockStore(
      <DynamicGrid {...newProps} />,
      {}
    );

    expect(wrapper.getByText('SL, Lost/Stolen Report')).toBeInTheDocument();
  });

  it('should render DynamicGrid with dynamic dependencies', async () => {
    const newProps = {
      ...props,
      stepId: '1',
      selectedStep: '2',
      selectedQuestion:
        'Report Lost Accounts using the SL, Lost/Stolen Report transaction.',
      gridTitle: 'SL, Lost/Stolen Report'
    };
    const wrapper = await renderWithMockStore(
      <DynamicGrid {...newProps} />,
      {}
    );

    expect(wrapper.getByText('SL, Lost/Stolen Report')).toBeInTheDocument();
  });

  it('should render DynamicGrid with dynamic dependencies', async () => {
    const newProps = {
      ...props,
      stepId: '1',
      selectedStep: '2',
      selectedQuestion:
        'Report Stolen Accounts using the SL, Lost/Stolen Report transaction.',
      gridTitle: 'SL, Lost/Stolen Report',
      formValues: { parameters: { field: 'abc' } }
    };
    const wrapper = await renderWithMockStore(
      <DynamicGrid {...newProps} />,
      {}
    );

    const txt = wrapper.getByTestId(
      'actionCardCompromiseEvent__actionCardCompromiseEventAreaLost_dls-text-box_input'
    );
    const txtArea = wrapper.getByTestId(
      'actionCardCompromiseEvent__actionCardCompromiseEventMemoLine1_dls-text-area_input'
    );

    const dropdown = wrapper.getByTestId(
      'actionCardCompromiseEvent__actionCardCompromiseEventExternalStatus_dls-dropdown-list'
    );

    const textAlphabet = wrapper.getByTestId(
      'actionCardCompromiseEvent__actionCardCompromiseEventFraudInvestigatorCode_dls-text-box_input'
    );

    userEvent.type(txt, 'asdadas {enter}');
    userEvent.type(txtArea, '=213-2-=');
    userEvent.click(dropdown);
    userEvent.type(textAlphabet, '12-3-as=**(0');

    expect(wrapper.getByText('SL, Lost/Stolen Report')).toBeInTheDocument();
  });

  it('should render DynamicGrid with dynamic search', async () => {
    const newProps = {
      ...props,
      stepId: '1',
      selectedStep: '2',
      selectedQuestion:
        'Report Stolen Accounts using the SL, Lost/Stolen Report transaction.',
      gridTitle: 'SL, Lost/Stolen Report',
      formValues: {}
    };
    const wrapper = await renderWithMockStore(
      <DynamicGrid {...newProps} />,
      {}
    );

    const search = wrapper.getByPlaceholderText('txt_type_to_search');

    userEvent.type(search, 'aaaa {enter}');

    userEvent.click(wrapper.getByText('txt_clear_and_reset'));

    expect(wrapper.getByText('SL, Lost/Stolen Report')).toBeInTheDocument();
  });

  it('should render DynamicGrid with dynamic search', async () => {
    const newProps = {
      ...props,
      stepId: '1',
      selectedStep: '2',
      selectedQuestion:
        'Report Stolen Accounts using the SL, Lost/Stolen Report transaction.',
      gridTitle: 'SL, Lost/Stolen Report',
      formValues: {}
    };
    const wrapper = await renderWithMockStore(
      <DynamicGrid {...newProps} />,
      {}
    );

    const search = wrapper.getByPlaceholderText('txt_type_to_search');

    userEvent.type(search, 'External Status {enter}');

    expect(wrapper.getByText('SL, Lost/Stolen Report')).toBeInTheDocument();
  });

  it('should render DynamicGrid with  optional props and dynamic dependencies', async () => {
    const newProps = {
      ...props,
      stepId: '1',
      selectedStep: '2',
      selectedQuestion:
        'Report Lost Accounts using the SL, Lost/Stolen Report transaction.',
      isOptional: true,
      gridTitle: 'SL, Lost/Stolen Report'
    };
    const wrapper = await renderWithMockStore(
      <DynamicGrid {...newProps} />,
      {}
    );

    expect(wrapper.getByText('SL, Lost/Stolen Report')).toBeInTheDocument();
  });

  it('should render DynamicGrid with optional props and  dynamic dependencies', async () => {
    const newProps = {
      ...props,
      stepId: '1',
      selectedStep: '2',
      selectedQuestion:
        'Report Stolen Accounts using the SL, Lost/Stolen Report transaction.',
      gridTitle: 'SL, Lost/Stolen Report',
      formValues: { parameters: { field: 'abc' } },
      isOptional: true
    };
    const wrapper = await renderWithMockStore(
      <DynamicGrid {...newProps} />,
      {}
    );

    const txt = wrapper.getByTestId(
      'actionCardCompromiseEvent__actionCardCompromiseEventAreaLost_dls-text-box_input'
    );
    const txtArea = wrapper.getByTestId(
      'actionCardCompromiseEvent__actionCardCompromiseEventMemoLine1_dls-text-area_input'
    );

    const dropdown = wrapper.getByTestId(
      'actionCardCompromiseEvent__actionCardCompromiseEventExternalStatus_dls-dropdown-list'
    );

    const textAlphabet = wrapper.getByTestId(
      'actionCardCompromiseEvent__actionCardCompromiseEventFraudInvestigatorCode_dls-text-box_input'
    );

    userEvent.type(txt, 'asdadas {enter}');
    userEvent.type(txtArea, '=213-2-=');
    userEvent.click(dropdown);
    userEvent.type(textAlphabet, '12-3-as=**(0');

    expect(wrapper.getByText('SL, Lost/Stolen Report')).toBeInTheDocument();
  });

  it('should render DynamicGrid with optional props and  dynamic dependencies', async () => {
    const newProps = {
      ...props,
      stepId: '1',
      selectedStep: '2',
      selectedQuestion:
        'Report Lost PIIDs using the SLP, Lost/Stolen Report transaction.',
      gridTitle: 'Report Lost PIIDs',
      formValues: { parameters: { field: 'abc' } }
    };
    const wrapper = await renderWithMockStore(
      <DynamicGrid {...newProps} />,
      {}
    );

    expect(
      wrapper.getByText(
        'Report Lost PIIDs using the SLP, Lost/Stolen Report transaction.'
      )
    ).toBeInTheDocument();
  });
  it('should render DynamicGrid with optional props and  dynamic dependencies', async () => {
    const newProps = {
      ...props,
      stepId: '1',
      selectedStep: '2',
      selectedQuestion:
        'Report Stolen PIIDs using the SLP, Lost/Stolen Report transaction.',
      gridTitle: 'Report Stolen PIIDs',
      formValues: { parameters: { field: 'abc' } }
    };
    const wrapper = await renderWithMockStore(
      <DynamicGrid {...newProps} />,
      {}
    );

    expect(
      wrapper.getByText(
        'Report Stolen PIIDs using the SLP, Lost/Stolen Report transaction.'
      )
    ).toBeInTheDocument();
  });
  it('should render DynamicGrid with optional props and  dynamic dependencies', async () => {
    const newProps = {
      ...props,
      stepId: '1',
      selectedStep: '2',
      selectedQuestion:
        'Account Transfer using the AT, Account Transfers transaction.',
      gridTitle: 'Account Transfer',
      formValues: { parameters: { field: 'abc' } }
    };
    const wrapper = await renderWithMockStore(
      <DynamicGrid {...newProps} />,
      {}
    );

    expect(
      wrapper.getByText(
        'Account Transfer using the AT, Account Transfers transaction.'
      )
    ).toBeInTheDocument();
  });
  it('should render DynamicGrid with optional props and  dynamic dependencies', async () => {
    const newProps = {
      ...props,
      stepId: '1',
      selectedStep: '2',
      selectedQuestion:
        'PIID Transfer using the PIT, Presentation Instrument Transfer transaction.',
      gridTitle: 'PIID Transfer',
      formValues: { parameters: { field: 'abc' } }
    };
    const wrapper = await renderWithMockStore(
      <DynamicGrid {...newProps} />,
      {}
    );

    expect(
      wrapper.getByText(
        'PIID Transfer using the PIT, Presentation Instrument Transfer transaction.'
      )
    ).toBeInTheDocument();
  });
  it('should render DynamicGrid with optional props and  dynamic dependencies', async () => {
    const newProps = {
      ...props,
      stepId: '1',
      selectedStep: '2',
      selectedQuestion:
        'MasterCard negative file using the WMA, Add Cardholder Account transaction.',
      gridTitle: 'MasterCard negative file',
      formValues: { parameters: { field: 'abc' } }
    };
    const wrapper = await renderWithMockStore(
      <DynamicGrid {...newProps} />,
      {}
    );

    expect(wrapper.getByText('MasterCard negative file')).toBeInTheDocument();
  });
  it('should render DynamicGrid with optional props and  dynamic dependencies', async () => {
    const newProps = {
      ...props,
      stepId: '1',
      selectedStep: '2',
      selectedQuestion:
        'Visa exception file using the WC, Visa Warning Bulletins transaction.',
      gridTitle: 'Visa exception file',
      formValues: { parameters: { field: 'abc' } }
    };
    const wrapper = await renderWithMockStore(
      <DynamicGrid {...newProps} />,
      {}
    );

    expect(wrapper.getByText('Visa exception file')).toBeInTheDocument();
  });
  it('should render DynamicGrid with optional props and  dynamic dependencies', async () => {
    const newProps = {
      ...props,
      stepId: '1',
      selectedStep: '2',
      selectedQuestion:
        'MasterCard warning bulletin using the WB, MasterCard Warning Bulletins transaction.',
      gridTitle: 'MasterCard warning bulletin',
      formValues: { parameters: { field: 'abc' } }
    };
    const wrapper = await renderWithMockStore(
      <DynamicGrid {...newProps} />,
      {}
    );

    expect(
      wrapper.getByText('MasterCard warning bulletin')
    ).toBeInTheDocument();
  });
  it('should render DynamicGrid with optional props and  dynamic dependencies', async () => {
    const newProps = {
      ...props,
      stepId: '1',
      selectedStep: '2',
      selectedQuestion:
        'MasterCard warning bulletin using the WB, MasterCard Warning Bulletins transaction.',
      gridTitle: 'MasterCard warning bulletin',
      formValues: {
        parameters: {
          'purge.date': '12/12/2022',
          'stolen.date': '12/12/2022',
          'lost.date': '12/12/2022',
          'date.of.event': '12/12/2022'
        },
        optionalParameters: {
          'purge.date': '12/12/2022',
          'stolen.date': '12/12/2022',
          'lost.date': '12/12/2022',
          'date.of.event': '12/12/2022'
        }
      }
    };
    const wrapper = await renderWithMockStore(
      <DynamicGrid {...newProps} />,
      {}
    );

    expect(
      wrapper.getByText('MasterCard warning bulletin')
    ).toBeInTheDocument();
  });
});
