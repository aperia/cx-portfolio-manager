import { FormatTime, MethodFieldParameterEnum } from 'app/constants/enums';
import { formatCommon, formatTimeDefault, matchSearchValue } from 'app/helpers';
import {
  Button,
  Grid,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty, isFunction } from 'app/_libraries/_dls/lodash';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import { useSelectElementMetadataForActionCardCompromiseEvent } from 'pages/_commons/redux/WorkflowSetup/select-hooks';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React, { useCallback, useMemo, useState } from 'react';
import { ACCEConfigureParameterValue } from '.';
import { ACCESelectActionsValue } from '../ActionCardCompromiseEventSelectActionsStep';
import {
  dynamicActionCardCompromiseParameterList,
  generateActionCardCompromiseTitle
} from './helper';

const Summary: React.FC<
  WorkflowSetupSummaryProps<ACCEConfigureParameterValue, ACCESelectActionsValue>
> = ({ formValues, dependencies, onEditStep }) => {
  const { width } = useSelectWindowDimension();

  const { t } = useTranslation();
  const metadata = useSelectElementMetadataForActionCardCompromiseEvent();
  const {
    externalStatus,
    pinLostIndicator,
    fraudLossOperator,
    possibleFraudActivityCode,
    fraudTypeCode,
    lostLocationCode,
    rushPlasticCode,
    lossOperatorId,
    piStatusCode,
    producePlasticIndicatorPLGEN,
    producePlasticIndicatorPlastic,
    producePlasticIndicatorSuppress,
    typeLossCode,
    visaResponseCode,
    visaWarningBulletinRegion,
    inasReasonCode,
    masterCardStatus,
    masterCardWarningBulletinRegion,
    accountTransferTypeCode,
    letterIdentifier,
    rushMailCode
  } = metadata;

  const modifiedData: MagicKeyValue = useMemo(() => {
    return {
      [MethodFieldParameterEnum.ActionCardCompromiseEventExternalStatus]:
        externalStatus.find(
          (o: MagicKeyValue) =>
            o.code ===
            formValues.parameters[
              MethodFieldParameterEnum.ActionCardCompromiseEventExternalStatus
            ]
        )?.text || '',
      [MethodFieldParameterEnum.ActionCardCompromiseEventPINLostIndicator]:
        pinLostIndicator.find(
          (o: MagicKeyValue) =>
            o.code ===
            formValues.parameters[
              MethodFieldParameterEnum.ActionCardCompromiseEventPINLostIndicator
            ]
        )?.text || '',
      [MethodFieldParameterEnum.ActionCardCompromiseEventPossibleFraudActivityCode]:
        possibleFraudActivityCode.find(
          (o: MagicKeyValue) =>
            o.code ===
            formValues.parameters[
              MethodFieldParameterEnum
                .ActionCardCompromiseEventPossibleFraudActivityCode
            ]
        )?.text || '',
      [MethodFieldParameterEnum.ActionCardCompromiseEventFraudLossOperator]:
        fraudLossOperator.find(
          (o: MagicKeyValue) =>
            o.code ===
            formValues.parameters[
              MethodFieldParameterEnum
                .ActionCardCompromiseEventFraudLossOperator
            ]
        )?.text || '',
      [MethodFieldParameterEnum.ActionCardCompromiseEventFraudTypeCode]:
        fraudTypeCode.find(
          (o: MagicKeyValue) =>
            o.code ===
            formValues.parameters[
              MethodFieldParameterEnum.ActionCardCompromiseEventFraudTypeCode
            ]
        )?.text || '',
      [MethodFieldParameterEnum.ActionCardCompromiseEventLostLocationCode]:
        lostLocationCode.find(
          (o: MagicKeyValue) =>
            o.code ===
            formValues.parameters[
              MethodFieldParameterEnum.ActionCardCompromiseEventLostLocationCode
            ]
        )?.text || '',
      [MethodFieldParameterEnum.ActionCardCompromiseEventRushPlasticCode]:
        rushPlasticCode.find(
          (o: MagicKeyValue) =>
            o.code ===
            formValues.parameters[
              MethodFieldParameterEnum.ActionCardCompromiseEventRushPlasticCode
            ]
        )?.text || '',
      [MethodFieldParameterEnum.ActionCardCompromiseEventLossOperatorId]:
        lossOperatorId.find(
          (o: MagicKeyValue) =>
            o.code ===
            formValues.parameters[
              MethodFieldParameterEnum.ActionCardCompromiseEventLossOperatorId
            ]
        )?.text || '',
      [MethodFieldParameterEnum.ActionCardCompromiseEventPiStatusCode]:
        piStatusCode.find(
          (o: MagicKeyValue) =>
            o.code ===
            formValues.parameters[
              MethodFieldParameterEnum.ActionCardCompromiseEventPiStatusCode
            ]
        )?.text || '',
      [MethodFieldParameterEnum.ActionCardCompromiseEventProducePlasticIndicatorPLGEN]:
        producePlasticIndicatorPLGEN.find(
          (o: MagicKeyValue) =>
            o.code ===
            formValues.parameters[
              MethodFieldParameterEnum
                .ActionCardCompromiseEventProducePlasticIndicatorPLGEN
            ]
        )?.text || '',
      [MethodFieldParameterEnum.ActionCardCompromiseEventProducePlasticIndicatorPlastic]:
        producePlasticIndicatorPlastic.find(
          (o: MagicKeyValue) =>
            o.code ===
            formValues.parameters[
              MethodFieldParameterEnum
                .ActionCardCompromiseEventProducePlasticIndicatorPlastic
            ]
        )?.text || '',
      [MethodFieldParameterEnum.ActionCardCompromiseEventTypeLossCode]:
        typeLossCode.find(
          (o: MagicKeyValue) =>
            o.code ===
            formValues.parameters[
              MethodFieldParameterEnum.ActionCardCompromiseEventTypeLossCode
            ]
        )?.text || '',
      [MethodFieldParameterEnum.ActionCardCompromiseEventProducePlasticIndicatorSuppress]:
        producePlasticIndicatorSuppress.find(
          (o: MagicKeyValue) =>
            o.code ===
            formValues.parameters[
              MethodFieldParameterEnum
                .ActionCardCompromiseEventProducePlasticIndicatorSuppress
            ]
        )?.text || '',
      [MethodFieldParameterEnum.ActionCardCompromiseEventVisaResponseCode]:
        visaResponseCode.find(
          (o: MagicKeyValue) =>
            o.code ===
            formValues.optionalParameters[
              MethodFieldParameterEnum.ActionCardCompromiseEventVisaResponseCode
            ]
        )?.text || '',
      [MethodFieldParameterEnum.ActionCardCompromiseEventVisaWarningBulletinRegion]:
        visaWarningBulletinRegion.find(
          (o: MagicKeyValue) =>
            o.code ===
            formValues.optionalParameters[
              MethodFieldParameterEnum
                .ActionCardCompromiseEventVisaWarningBulletinRegion
            ]
        )?.text || '',
      [MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardINASReasonCode]:
        inasReasonCode.find(
          (o: MagicKeyValue) =>
            o.code ===
            formValues.optionalParameters[
              MethodFieldParameterEnum
                .ActionCardCompromiseEventMasterCardINASReasonCode
            ]
        )?.text || '',
      [MethodFieldParameterEnum.ActionCardCompromiseEventRushMailCode]:
        rushMailCode.find(
          (o: MagicKeyValue) =>
            o.code ===
            formValues.parameters[
              MethodFieldParameterEnum.ActionCardCompromiseEventRushMailCode
            ]
        )?.text || '',
      [MethodFieldParameterEnum.ActionCardCompromiseEventGenerateLetterIdentifier]:
        letterIdentifier.find(
          (o: MagicKeyValue) =>
            o.code ===
            formValues.parameters[
              MethodFieldParameterEnum
                .ActionCardCompromiseEventGenerateLetterIdentifier
            ]
        )?.text || '',
      [MethodFieldParameterEnum.ActionCardCompromiseEventAccountTransferTypeCode]:
        accountTransferTypeCode.find(
          (o: MagicKeyValue) =>
            o.code ===
            formValues.parameters[
              MethodFieldParameterEnum
                .ActionCardCompromiseEventAccountTransferTypeCode
            ]
        )?.text || '',
      [MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardWarningBulletinRegion]:
        masterCardWarningBulletinRegion.find(
          (o: MagicKeyValue) =>
            o.code ===
            formValues.optionalParameters[
              MethodFieldParameterEnum
                .ActionCardCompromiseEventMasterCardWarningBulletinRegion
            ]
        )?.text || '',
      [MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardStatus]:
        masterCardStatus.find(
          (o: MagicKeyValue) =>
            o.code ===
            formValues.optionalParameters[
              MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardStatus
            ]
        )?.text || '',
      [MethodFieldParameterEnum.ActionCardCompromiseEventAreaLost]:
        formValues.parameters[
          MethodFieldParameterEnum.ActionCardCompromiseEventAreaLost
        ],
      [MethodFieldParameterEnum.ActionCardCompromiseEventMemoLine1]:
        formValues.parameters[
          MethodFieldParameterEnum.ActionCardCompromiseEventMemoLine1
        ],
      [MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardAuthorizationLimitAmount]:
        formValues.optionalParameters[
          MethodFieldParameterEnum
            .ActionCardCompromiseEventMasterCardAuthorizationLimitAmount
        ],
      [MethodFieldParameterEnum.ActionCardCompromiseEventStolenDate]:
        formatTimeDefault(
          new Date(
            formValues.parameters[
              MethodFieldParameterEnum.ActionCardCompromiseEventStolenDate
            ]
          ).toString(),
          FormatTime.Date
        ) || '',
      [MethodFieldParameterEnum.ActionCardCompromiseEventLostDate]:
        formatTimeDefault(
          new Date(
            formValues.parameters[
              MethodFieldParameterEnum.ActionCardCompromiseEventLostDate
            ]
          ).toString(),
          FormatTime.Date
        ) || '',
      [MethodFieldParameterEnum.ActionCardCompromiseEventDateOfEvent]:
        formatTimeDefault(
          new Date(
            formValues.parameters[
              MethodFieldParameterEnum.ActionCardCompromiseEventDateOfEvent
            ]
          ).toString(),
          FormatTime.Date
        ) || '',
      [MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardPurgeDate]:
        formatTimeDefault(
          new Date(
            formValues.optionalParameters[
              MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardPurgeDate
            ]
          ).toString(),
          FormatTime.Date
        ) || '',
      [MethodFieldParameterEnum.ActionCardCompromiseEventFraudAreaCode]:
        formValues.parameters[
          MethodFieldParameterEnum.ActionCardCompromiseEventFraudAreaCode
        ],
      [MethodFieldParameterEnum.ActionCardCompromiseEventFraudInvestigatorCode]:
        formValues.parameters[
          MethodFieldParameterEnum
            .ActionCardCompromiseEventFraudInvestigatorCode
        ]
    };
  }, [
    formValues,
    externalStatus,
    pinLostIndicator,
    fraudLossOperator,
    possibleFraudActivityCode,
    fraudTypeCode,
    lostLocationCode,
    rushPlasticCode,
    lossOperatorId,
    piStatusCode,
    producePlasticIndicatorPLGEN,
    producePlasticIndicatorPlastic,
    producePlasticIndicatorSuppress,
    typeLossCode,
    visaResponseCode,
    visaWarningBulletinRegion,
    inasReasonCode,
    masterCardStatus,
    masterCardWarningBulletinRegion,
    accountTransferTypeCode,
    letterIdentifier,
    rushMailCode
  ]);

  const handleEdit = () => {
    isFunction(onEditStep) && onEditStep();
  };

  const question3Txt = dependencies?.action?.question3;
  const question2Txt = dependencies?.action?.question2;

  const gridData = useMemo(() => {
    return dynamicActionCardCompromiseParameterList(question2Txt);
  }, [question2Txt]);

  const gridData2 = useMemo(() => {
    return dynamicActionCardCompromiseParameterList(question3Txt);
  }, [question3Txt]);

  const isOptional =
    !isEmpty(question3Txt) &&
    !['yes', 'no'].includes(question3Txt!.toLowerCase());

  const valueColumn = useCallback(
    (item: MagicKeyValue) => {
      let valueTruncate = '';

      switch (item.id) {
        case MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardAuthorizationLimitAmount:
          valueTruncate = modifiedData?.[item.id]
            ? `$${formatCommon(modifiedData?.[item.id]).quantity}`
            : '';
          break;
        default:
          valueTruncate =
            modifiedData?.[item.id] === 'None' ? '' : modifiedData?.[item.id];
          break;
      }

      return valueTruncate;
    },
    [modifiedData]
  );

  const columns = useMemo(
    () => [
      {
        id: 'fieldName',
        Header: t('txt_business_name'),
        accessor: 'fieldName',
        cellBodyProps: { className: 'pb-8' }
      },
      {
        id: 'greenScreenName',
        Header: t('txt_green_screen_name'),
        accessor: 'greenScreenName',
        cellBodyProps: { className: 'pb-8' }
      },
      {
        id: 'value',
        Header: t('txt_value'),
        accessor: (item: MagicKeyValue) => {
          return (
            <TruncateText
              resizable
              lines={2}
              ellipsisLessText={t('txt_less')}
              ellipsisMoreText={t('txt_more')}
            >
              {valueColumn(item)}
            </TruncateText>
          );
        },
        cellBodyProps: { className: 'pb-8' },
        width: width < 1280 ? 220 : undefined
      },
      {
        id: 'moreInfo',
        Header: t('txt_more_info'),
        className: 'text-center',
        accessor: viewMoreInfo(['moreInfo'], t),
        width: 105,
        cellBodyProps: { className: 'pb-8' }
      }
    ],
    [t, width, valueColumn]
  );
  const [searchValue] = useState('');

  const data2 = useMemo(
    () =>
      gridData2.filter((g: MagicKeyValue) =>
        matchSearchValue(
          [g.fieldName, g.moreInfoText, g.greenScreenName],
          searchValue.trim()
        )
      ),
    [searchValue, gridData2]
  );

  const data = useMemo(
    () =>
      gridData.filter((g: MagicKeyValue) =>
        matchSearchValue(
          [g.fieldName, g.moreInfoText, g.greenScreenName],
          searchValue.trim()
        )
      ),
    [searchValue, gridData]
  );

  return (
    <div className="position-relative">
      <div className="absolute-top-right mt-n26 mr-n8">
        <Button variant="outline-primary" size="sm" onClick={handleEdit}>
          {t('txt_edit')}
        </Button>
      </div>

      <div className="d-flex justify-content-between pt-16">
        <p className="fw-600">
          {generateActionCardCompromiseTitle(question2Txt!)}
        </p>
      </div>
      <div className="pt-16">
        <Grid columns={columns} data={data} />
      </div>

      {isOptional && (
        <>
          <div className="d-flex justify-content-between pt-24">
            <p className="fw-600">
              {generateActionCardCompromiseTitle(question3Txt!)}
            </p>
          </div>
          <div className=" pt-16">
            <Grid columns={columns} data={data2} />
          </div>
        </>
      )}
    </div>
  );
};

export default Summary;
