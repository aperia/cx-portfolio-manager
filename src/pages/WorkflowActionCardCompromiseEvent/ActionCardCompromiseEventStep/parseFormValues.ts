import { MethodFieldParameterEnum as ParameterEnum } from 'app/constants/enums';
import { getWorkflowSetupStepStatus } from 'app/helpers';
import { ACCEConfigureParameterValue } from '.';
import { templateACCE } from './helper';

const parseFormValues: WorkflowSetupStepFormDataFunc<ACCEConfigureParameterValue> =
  (data, id) => {
    const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);
    if (!stepInfo) return;
    const allParameters = ((data?.data as any)?.configurations?.parameters ||
      []) as WorkflowSetupElementParams[];

    const question2 =
      allParameters.find(
        p =>
          p.name ===
          ParameterEnum.ActionCardCompromiseEventQuestion2SingleEntity
      )?.value ||
      allParameters.find(
        p =>
          p.name ===
          ParameterEnum.ActionCardCompromiseEventQuestion2SeparateEntity
      )?.value;
    const question3 =
      allParameters.find(
        p => p.name === ParameterEnum.ActionCardCompromiseEventQuestion3
      )?.value ||
      allParameters.find(
        p => p.name === ParameterEnum.ActionCardCompromiseEventQuestion4
      )?.value;
    const paramConfig = templateACCE(question2!, question3!);

    const parameters = getParameters(paramConfig.template1, allParameters);
    const optionalParameters = getParameters(
      paramConfig.template2,
      allParameters
    );

    return {
      ...getWorkflowSetupStepStatus(stepInfo),
      parameters,
      optionalParameters,
      isValid: true
    };
  };

const getParameters = (
  paramConfig: any,
  allParameters: WorkflowSetupElementParams[]
) => {
  const parameters = {} as any;
  Object.keys(paramConfig).forEach(
    k => (parameters[k] = allParameters.find(p => p.name === k)?.value)
  );
  return parameters;
};

export default parseFormValues;
