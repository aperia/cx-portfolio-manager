import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import * as CommonRedux from 'pages/_commons/redux/Common/select-hooks';
import * as workflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowActionCardCompromiseEvent';
import React from 'react';
import Summary from './Summary';
const mockUseSelectWindowDimension = jest.spyOn(
  CommonRedux,
  'useSelectWindowDimension'
);
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const mockUseSelectElementMetadataForActionCardCompromiseEvent = jest.spyOn(
  workflowSetup,
  'useSelectElementMetadataForActionCardCompromiseEvent'
);

describe('Summary ConfigureParameter', () => {
  const mockUseSelectWindowDimensionImplementation = (width: number) => {
    mockUseSelectWindowDimension.mockImplementation(() => ({ width } as any));
  };

  beforeEach(() => {
    const metadata: any = {
      externalStatus: [{ code: 'None', text: 'None' }],
      pinLostIndicator: [{ code: 'code', text: 'text' }],
      fraudLossOperator: [{ code: 'code', text: 'text' }],
      possibleFraudActivityCode: [{ code: 'code', text: 'text' }],
      fraudTypeCode: [
        { code: 'code', text: 'text' },
        { code: 'code1', text: 'text1' }
      ],
      lostLocationCode: [
        { code: 'code', text: 'text' },
        { code: 'code1', text: 'text1' }
      ],
      rushPlasticCode: [
        { code: 'code', text: 'text' },
        { code: 'code1', text: 'text1' }
      ],
      lossOperatorId: [
        { code: 'code', text: 'text' },
        { code: 'code1', text: 'text1' }
      ],
      piStatusCode: [
        { code: 'code', text: 'text' },
        { code: 'code1', text: 'text1' }
      ],
      producePlasticIndicatorPLGEN: [
        { code: 'code', text: 'text' },
        { code: 'code1', text: 'text1' }
      ],
      producePlasticIndicatorPlastic: [
        { code: 'code', text: 'text' },
        { code: 'code1', text: 'text1' }
      ],
      producePlasticIndicatorSuppress: [
        { code: 'code', text: 'text' },
        { code: 'code1', text: 'text1' }
      ],
      typeLossCode: [
        { code: 'code', text: 'text' },
        { code: 'code1', text: 'text1' }
      ],
      visaResponseCode: [
        { code: 'code', text: 'text' },
        { code: 'None', text: 'text1' }
      ],
      visaWarningBulletinRegion: [
        { code: 'code', text: 'text' },
        { code: 'None', text: 'text1' }
      ],
      inasReasonCode: [
        { code: 'code', text: 'text' },
        { code: 'code1', text: 'text1' }
      ],
      masterCardStatus: [
        { code: 'code', text: 'text' },
        { code: 'code1', text: 'text1' }
      ],
      masterCardWarningBulletinRegion: [
        { code: 'code', text: 'text' },
        { code: 'code1', text: 'text1' }
      ],
      accountTransferTypeCode: [
        { code: 'code', text: 'text' },
        { code: 'code1', text: 'text1' }
      ],
      letterIdentifier: [
        { code: 'code', text: 'text' },
        { code: 'code1', text: 'text1' }
      ],
      rushMailCode: [
        { code: 'code', text: 'text' },
        { code: 'code1', text: 'text1' }
      ]
    };
    mockUseSelectElementMetadataForActionCardCompromiseEvent.mockReturnValue(
      metadata
    );
  });

  afterEach(() => {
    mockUseSelectWindowDimension.mockClear();
  });

  it('Should render a grid with content', () => {
    mockUseSelectWindowDimensionImplementation(90);
    const props = {
      formValues: {
        parameters: {},
        optionalParameters: {}
      },
      dependencies: {
        action: {
          question2:
            'Report Lost Accounts using the SL, Lost/Stolen Report transaction.',
          question3:
            'Visa exception file using the WC, Visa Warning Bulletins transaction.'
        }
      },
      onEditStep: jest.fn(),
      stepId: '1',
      selectedStep: '1'
    };

    const wrapper = render(<Summary {...props} />);
    expect(wrapper.getByText('Visa Response Code')).toBeInTheDocument();
  });

  it('Should render and click edit', () => {
    mockUseSelectWindowDimensionImplementation(1300);
    const props = {
      formValues: {
        parameters: [],
        optionalParameters: {
          'purge.date': '10/03/2020',
          'visa.response.code': 'None',
          'visa.warning.bulletin.region': 'None',
          'inas.reason.code': 'None',
          'mastercard.warning.bulletin.region': 'None',
          status: 'None',
          'authorization.limit.amount': 'None'
        }
      },
      dependencies: {
        action: {
          question2:
            'Report Lost Accounts using the SL, Lost/Stolen Report transaction.',
          question3:
            'Visa exception file using the WC, Visa Warning Bulletins transaction.'
        }
      },
      onEditStep: jest.fn(),
      stepId: '1',
      selectedStep: '1'
    };

    const wrapper = render(<Summary {...props} />);
    const text = wrapper.getByText('txt_edit');
    userEvent.click(text);
    expect(wrapper.getByText('Visa Response Code')).toBeInTheDocument();
  });

  it('Should render and click edit with deference dependencies', () => {
    mockUseSelectWindowDimensionImplementation(1300);
    const props = {
      formValues: {
        parameters: {},
        optionalParameters: {}
      },
      dependencies: {
        action: {
          question2:
            'Report Lost Accounts using the SL, Lost/Stolen Report transaction.',
          question3:
            'Visa exception file using the WC, Visa Warning Bulletins transaction.'
        }
      },
      onEditStep: jest.fn(),
      stepId: '1',
      selectedStep: '1'
    };

    const wrapper = render(<Summary {...props} />);
    const text = wrapper.getByText('txt_edit');
    userEvent.click(text);
    expect(wrapper.getByText('Visa Response Code')).toBeInTheDocument();
  });

  it('Should render and click edit with deference dependencies', () => {
    mockUseSelectWindowDimensionImplementation(1300);
    const props = {
      formValues: {
        parameters: {},
        optionalParameters: {
          'authorization.limit.amount': '12'
        }
      },
      dependencies: {
        action: {
          question2:
            'Report Lost Accounts using the SL, Lost/Stolen Report transaction.',
          question3:
            'MasterCard negative file using the WMA, Add Cardholder Account transaction.'
        }
      },
      onEditStep: jest.fn(),
      stepId: '1',
      selectedStep: '1'
    };

    const wrapper = render(<Summary {...props} />);
    const text = wrapper.getByText('txt_edit');
    userEvent.click(text);
    expect(wrapper.getByText('Authorization Limit Amount')).toBeInTheDocument();
  });
  it('Should render and click edit with deference dependencies', () => {
    mockUseSelectWindowDimensionImplementation(1300);
    const props = {
      formValues: {
        parameters: {},
        optionalParameters: {
          'authorization.limit.amount': '12'
        }
      },
      dependencies: {
        action: {
          question2:
            'Report Lost Accounts using the SL, Lost/Stolen Report transaction.',
          question3:
            'MasterCard warning bulletin using the WB, MasterCard Warning Bulletins transaction.'
        }
      },
      onEditStep: jest.fn(),
      stepId: '1',
      selectedStep: '1'
    };

    const wrapper = render(<Summary {...props} />);
    const text = wrapper.getByText('txt_edit');
    userEvent.click(text);
    expect(wrapper.getByText('Purge Date')).toBeInTheDocument();
  });
  it('Should render and click edit with deference dependencies', () => {
    mockUseSelectWindowDimensionImplementation(1300);
    const props = {
      formValues: {
        parameters: {
          'external.status': 'None'
        },
        optionalParameters: {}
      },
      dependencies: {
        action: {
          question2:
            'Report Lost Accounts using the SL, Lost/Stolen Report transaction.',
          question3:
            'MasterCard negative file using the WMA, Add Cardholder Account transaction.'
        }
      },
      onEditStep: jest.fn(),
      stepId: '1',
      selectedStep: '1'
    };

    const wrapper = render(<Summary {...props} />);
    const text = wrapper.getByText('txt_edit');
    userEvent.click(text);
    expect(wrapper.getByText('Limit')).toBeInTheDocument();
  });
});
