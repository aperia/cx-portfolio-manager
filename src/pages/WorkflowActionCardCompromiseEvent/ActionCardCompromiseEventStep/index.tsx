import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React from 'react';
import ConfigureParameters from './ConfigureParameters';
import parseFormValues from './parseFormValues';
import ActionCardCompromiseEventSummary from './Summary';

export interface ACCEConfigureParameterValue {
  isValid?: boolean;
  parameters?: any;
  optionalParameters?: any;
}

const ActionCardCompromiseEvent: React.FC<
  WorkflowSetupProps<ACCEConfigureParameterValue>
> = props => {
  return (
    <div className="mt-24">
      <ConfigureParameters {...props} />
    </div>
  );
};

const ExtraStaticACCEConfigureParameterStep =
  ActionCardCompromiseEvent as WorkflowSetupStaticProp<ACCEConfigureParameterValue>;

ExtraStaticACCEConfigureParameterStep.summaryComponent =
  ActionCardCompromiseEventSummary;
ExtraStaticACCEConfigureParameterStep.defaultValues = {
  isValid: true,
  parameters: [],
  optionalParameters: []
};

ExtraStaticACCEConfigureParameterStep.parseFormValues = parseFormValues;

export default ExtraStaticACCEConfigureParameterStep;
