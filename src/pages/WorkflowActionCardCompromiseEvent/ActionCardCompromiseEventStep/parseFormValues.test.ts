import { MethodFieldParameterEnum as ParameterEnum } from 'app/constants/enums';
import parseFormValues from './parseFormValues';

describe('pages > WorkflowActionCardCompromise > ActionCardCompromiseStep > parseFormValues', () => {
  it('parseFormValues', () => {
    let configurations: any = {};
    const id = 'id';
    const input: any = {
      data: {
        configurations,
        workflowSetupData: [{ id, status: 'INPROGRESS' }]
      }
    };

    // isvalid
    let result = parseFormValues(input, id, jest.fn);
    expect(result?.isValid).toEqual(true);

    // other step
    result = parseFormValues(input, 'id1', jest.fn);
    expect(result).toBeUndefined();

    configurations = {
      parameters: [
        {
          name: ParameterEnum.ActionCardCompromiseEventQuestion2SingleEntity,
          value:
            'Report Lost Accounts using the SL, Lost/Stolen Report transaction.'
        },
        {
          name: ParameterEnum.ActionCardCompromiseEventQuestion2SeparateEntity,
          value: 'value'
        },
        {
          name: ParameterEnum.ActionCardCompromiseEventQuestion3,
          value: 'value'
        },
        {
          name: ParameterEnum.ActionCardCompromiseEventQuestion4,
          value: 'value'
        }
      ]
    };
    input.data.configurations = configurations;

    result = parseFormValues(input, id, jest.fn);
    expect(result?.isValid).toEqual(true);

    configurations = {
      parameters: [
        {
          name: ParameterEnum.ActionCardCompromiseEventQuestion2SeparateEntity,
          value:
            'Visa exception file using the WC, Visa Warning Bulletins transaction.'
        },
        {
          name: ParameterEnum.ActionCardCompromiseEventQuestion3,
          value: 'value'
        },
        {
          name: ParameterEnum.ActionCardCompromiseEventQuestion4,
          value: 'value'
        }
      ]
    };
    input.data.configurations = configurations;

    result = parseFormValues(input, id, jest.fn);
    expect(result?.isValid).toEqual(true);

    configurations = {
      parameters: [
        {
          name: ParameterEnum.ActionCardCompromiseEventQuestion3,
          value: 'value'
        },
        {
          name: ParameterEnum.ActionCardCompromiseEventQuestion4,
          value: 'value'
        }
      ]
    };
    input.data.configurations = configurations;

    result = parseFormValues(input, id, jest.fn);
    expect(result?.isValid).toEqual(true);

    configurations = {
      parameters: [
        {
          name: ParameterEnum.ActionCardCompromiseEventQuestion4,
          value: 'value'
        }
      ]
    };
    input.data.configurations = configurations;

    result = parseFormValues(input, id, jest.fn);
    expect(result?.isValid).toEqual(true);
  });
});
