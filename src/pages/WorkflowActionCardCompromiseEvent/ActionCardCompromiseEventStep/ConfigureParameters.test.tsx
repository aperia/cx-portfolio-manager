import { renderWithMockStore } from 'app/utils';
import 'app/_libraries/_dls/test-utils/mocks/mockCanvas';
import React from 'react';
import * as ReactRedux from 'react-redux';
import ConfigureParameters from './ConfigureParameters';
const mockUseDispatch = jest.spyOn(ReactRedux, 'useDispatch');
describe('Action Card Compromise Event > ConfigureParameters', () => {
  beforeEach(() => {
    mockUseDispatch.mockReturnValue(jest.fn());
  });
  afterEach(() => {
    jest.clearAllMocks();
  });
  const props = {} as any;
  it('should render ConfigureParameters', async () => {
    const wrapper = await renderWithMockStore(
      <ConfigureParameters {...props} />,
      {}
    );

    expect(
      wrapper.getByPlaceholderText('txt_type_to_search')
    ).toBeInTheDocument();
  });

  it('should render ConfigureParameters with dynamic dependencies', async () => {
    const props = {
      dependencies: {
        action: {
          question2:
            'Report Lost Accounts using the SL, Lost/Stolen Report transaction.'
        }
      },
      setFormValues: jest.fn()
    } as any;
    const wrapper = await renderWithMockStore(
      <ConfigureParameters {...props} />,
      {}
    );

    expect(wrapper.getByText('SL, Lost/Stolen Report')).toBeInTheDocument();
  });

  it('should render ConfigureParameters with dynamic dependencies', async () => {
    const props = {
      dependencies: {
        action: {
          question2:
            'Report Stolen Accounts using the SL, Lost/Stolen Report transaction.'
        }
      },
      setFormValues: jest.fn()
    } as any;
    const wrapper = await renderWithMockStore(
      <ConfigureParameters {...props} />,
      {}
    );

    expect(wrapper.getByText('SL, Lost/Stolen Report')).toBeInTheDocument();
  });

  it('should render ConfigureParameters with dynamic dependencies', async () => {
    const props = {
      dependencies: {
        action: {
          question3:
            'Visa exception file using the WC, Visa Warning Bulletins transaction.'
        }
      },
      setFormValues: jest.fn()
    } as any;
    const wrapper = await renderWithMockStore(
      <ConfigureParameters {...props} />,
      {}
    );

    expect(wrapper.getByText('WC, Visa Warning Bulletins')).toBeInTheDocument();
  });
});
