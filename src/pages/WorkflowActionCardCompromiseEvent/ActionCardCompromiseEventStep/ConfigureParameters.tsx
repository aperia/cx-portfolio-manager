import { FormatTime } from 'app/constants/enums';
import { formatTimeDefault } from 'app/helpers';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch } from 'react-redux';
import { ACCEConfigureParameterValue } from '.';
import { ACCESelectActionsValue } from '../ActionCardCompromiseEventSelectActionsStep';
import DynamicGrid from './DynamicGrid';
import { generateActionCardCompromiseTitle, templateACCE } from './helper';

const ConfigureParameters: React.FC<
  WorkflowSetupProps<ACCEConfigureParameterValue, ACCESelectActionsValue>
> = props => {
  const { dependencies } = props;
  const dispatch = useDispatch();
  const [minDateSetting, setMinDateSetting] = useState();
  const [maxDateSetting, setMaxDateSetting] = useState();

  const question3Txt = dependencies?.action?.question3;

  const question2Txt = dependencies?.action?.question2;

  const minDate = useMemo(() => {
    return new Date(formatTimeDefault(minDateSetting!, FormatTime.Date));
  }, [minDateSetting]);

  const maxDate = useMemo(() => {
    return new Date(formatTimeDefault(maxDateSetting!, FormatTime.Date));
  }, [maxDateSetting]);

  const isOptional =
    !isEmpty(question3Txt) &&
    !['yes', 'no'].includes(question3Txt!.toLowerCase());

  const initFields = useCallback(async () => {
    const responseDateRange = await Promise.resolve(
      dispatch(actionsWorkflowSetup.getWorkflowEffectiveDateOptions({}))
    );

    const { maxDate: maxDateRes, minDate: minDateRes } =
      (responseDateRange as any)?.payload?.data || {};

    setMinDateSetting(minDateRes);
    setMaxDateSetting(maxDateRes);
  }, [dispatch]);

  useEffect(() => {
    initFields();
  }, [initFields]);

  return (
    <>
      <DynamicGrid
        selectedQuestion={question2Txt}
        minDate={minDate}
        maxDate={maxDate}
        draftData={templateACCE(question2Txt!, question3Txt!)}
        gridTitle={generateActionCardCompromiseTitle(question2Txt!)}
        {...props}
      />
      {isOptional && (
        <div className="border-top mt-24 pt-24">
          <DynamicGrid
            isOptional
            selectedQuestion={question3Txt}
            draftData={templateACCE(question2Txt!, question3Txt!)}
            gridTitle={generateActionCardCompromiseTitle(question3Txt!)}
            {...props}
          />
        </div>
      )}
    </>
  );
};

export default ConfigureParameters;
