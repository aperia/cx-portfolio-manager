import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import {
  unsavedChangesProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { matchSearchValue, stringValidate } from 'app/helpers';
import { useUnsavedChangeRegistry } from 'app/hooks';
import { Grid, useTranslation } from 'app/_libraries/_dls';
import { isEmpty, isEqual } from 'app/_libraries/_dls/lodash';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import { useSelectElementMetadataForActionCardCompromiseEvent } from 'pages/_commons/redux/WorkflowSetup';
import ClearAndResetButton from 'pages/_commons/Utils/ClearAndResetButton';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { ACCEConfigureParameterValue } from '.';
import { ACCESelectActionsValue } from '../ActionCardCompromiseEventSelectActionsStep';
import {
  dynamicActionCardCompromiseParameterList,
  generateSubTitleActionCardCompromise,
  renderMappingForm
} from './helper';

const DynamicGrid: React.FC<
  WorkflowSetupProps<ACCEConfigureParameterValue, ACCESelectActionsValue> & {
    selectedQuestion?: string;
    gridTitle?: string;
    minDate?: Date;
    maxDate?: Date;
    draftData: Record<string, any>;
    isOptional?: boolean;
  }
> = ({
  gridTitle,
  selectedQuestion,
  draftData,
  isOptional = false,
  formValues,
  minDate,
  maxDate,
  setFormValues,
  savedAt,
  selectedStep,
  stepId
}) => {
  const { t } = useTranslation();

  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;
  const simpleSearchRef = useRef<any>(null);
  const metadata = useSelectElementMetadataForActionCardCompromiseEvent();

  const [searchValue, setSearchValue] = useState('');

  const formValuesDynamic = useMemo(
    () =>
      isOptional ? formValues?.optionalParameters : formValues?.parameters,
    [isOptional, formValues?.optionalParameters, formValues?.parameters]
  );
  const [initialACCE, setInitialACCE] =
    useState<Record<string, string>>(formValuesDynamic);

  const { width } = useSelectWindowDimension();

  const draftTemplate = useMemo(() => {
    if (isOptional) return draftData?.template2;
    return draftData?.template1;
  }, [draftData, isOptional]);

  useUnsavedChangeRegistry(
    {
      ...unsavedChangesProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__ACTION_CARD_COMPROMISE_EVENT__CONFIGURE_PARAMETERS,
      priority: 1
    },
    [!isEqual(formValuesDynamic, initialACCE)]
  );

  const gridData = useMemo(() => {
    return dynamicActionCardCompromiseParameterList(selectedQuestion);
  }, [selectedQuestion]);

  const data = useMemo(
    () =>
      gridData.filter((g: MagicKeyValue) =>
        matchSearchValue(
          [g.fieldName, g.moreInfoText, g.greenScreenName],
          searchValue.trim()
        )
      ),
    [searchValue, gridData]
  );

  useEffect(() => {
    let defaultParamObj: Record<string, string> = {};

    if (!isEmpty(draftTemplate) && isEmpty(initialACCE)) {
      const paramMapping = Object.keys(draftTemplate).reduce((pre, next) => {
        pre[next] = draftTemplate[next];
        return pre;
      }, {} as Record<string, string>);
      defaultParamObj = Object.assign({}, paramMapping);

      if (isOptional) {
        setFormValues({ optionalParameters: defaultParamObj });
      } else {
        setFormValues({ parameters: defaultParamObj });
      }
    }
  }, [isOptional, setFormValues, initialACCE, draftTemplate]);

  useEffect(() => {
    if (isEmpty(formValuesDynamic)) {
      setInitialACCE({});
    }
  }, [setInitialACCE, formValuesDynamic]);

  useEffect(() => {
    setInitialACCE(formValuesDynamic);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [savedAt]);

  useEffect(() => {
    if (selectedStep !== stepId) {
      setSearchValue('');
    }
  }, [selectedStep, stepId]);

  const handleFormChange = useCallback(
    (field: keyof WorkflowSetupMethodObjectParams, isRefData?: boolean) =>
      (e: any) => {
        const value = isRefData ? e.target?.value?.code : e.target?.value;

        if (
          [
            MethodFieldParameterEnum.ActionCardCompromiseEventFraudInvestigatorCode,
            MethodFieldParameterEnum.ActionCardCompromiseEventFraudAreaCode
          ].includes(field as MethodFieldParameterEnum) &&
          !stringValidate(value).isAlphabetical()
        ) {
          return false;
        }

        if (isOptional) {
          setFormValues({
            optionalParameters: {
              ...formValues.optionalParameters,
              [field]: value
            }
          });
        } else {
          setFormValues({
            parameters: {
              ...formValues.parameters,
              [field]: value
            }
          });
        }
      },
    [formValues, setFormValues, isOptional]
  );

  const handleClearFilter = () => {
    setSearchValue('');
  };

  const columns = useMemo(
    () => [
      {
        id: 'fieldName',
        Header: t('txt_business_name'),
        accessor: 'fieldName',
        cellBodyProps: { className: 'pt-12 pb-8' }
      },
      {
        id: 'greenScreenName',
        Header: t('txt_green_screen_name'),
        accessor: 'greenScreenName',
        cellBodyProps: { className: 'pt-12 pb-8' }
      },
      {
        id: 'value',
        Header: t('txt_value'),
        accessor: (item: MagicKeyValue) =>
          renderMappingForm(
            item,
            t,
            metadata,
            formValuesDynamic,
            minDate,
            maxDate,
            handleFormChange
          ),
        cellBodyProps: { className: 'py-8' },
        width: width < 1280 ? 220 : undefined
      },
      {
        id: 'moreInfo',
        Header: t('txt_more_info'),
        className: 'text-center',
        accessor: viewMoreInfo(['moreInfo'], t),
        width: 105,
        cellBodyProps: { className: 'pt-12 pb-8' }
      }
    ],
    [t, width, handleFormChange, maxDate, minDate, formValuesDynamic, metadata]
  );

  useEffect(() => {
    if (!searchValue && simpleSearchRef?.current) {
      simpleSearchRef.current.clear();
    }
  }, [searchValue]);

  return (
    <>
      <div className="d-flex justify-content-between">
        <h5>{gridTitle}</h5>
        <SimpleSearch
          ref={simpleSearchRef}
          placeholder={t('txt_type_to_search')}
          onSearch={e => setSearchValue(e)}
          popperElement={
            <>
              <p className="color-grey">{t('txt_search_description')}</p>
              <ul className="search-field-item list-unstyled">
                <li className="mt-16">{t('txt_business_name')}</li>
                <li className="mt-16">{t('txt_green_screen_name')}</li>
                <li className="mt-16">{t('txt_more_info')}</li>
              </ul>
            </>
          }
        />
      </div>
      <div
        className={`d-flex justify-content-between align-items-center color-grey ${
          searchValue && !isEmpty(data) ? 'mt-16' : 'mt-8'
        }`}
      >
        <p>{generateSubTitleActionCardCompromise(selectedQuestion!)}</p>
        {searchValue && !isEmpty(data) && (
          <div className="d-flex justify-content-end mr-n8">
            <ClearAndResetButton small onClearAndReset={handleClearFilter} />
          </div>
        )}
      </div>

      {isEmpty(data) && searchValue && (
        <div className="d-flex flex-column justify-content-center mt-40 mb-24">
          <NoDataFound
            id="newMethod_notfound"
            hasSearch
            title={t('txt_no_results_found')}
            linkTitle={t('txt_clear_and_reset')}
            onLinkClicked={handleClearFilter}
          />
        </div>
      )}
      {!isEmpty(data) && (
        <div className="mt-16">
          <Grid columns={columns} data={data} />
        </div>
      )}
    </>
  );
};

export default DynamicGrid;
