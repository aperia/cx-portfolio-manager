import {
  MethodFieldNameEnum,
  MethodFieldParameterEnum
} from 'app/constants/enums';
import React from 'react';

export interface ParameterListProps {
  id: MethodFieldParameterEnum;
  fieldName: MethodFieldNameEnum;
  greenScreenName: string;
  moreInfoText: string;
  moreInfo: React.ReactNode;
}
export const reportStolenAccountsParameterList: ParameterListProps[] = [
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventExternalStatus,
    fieldName: MethodFieldNameEnum.ActionCardCompromiseEventExternalStatus,
    greenScreenName: 'ST',
    moreInfoText:
      'External Status Code - Issuer-assigned code recognized by Fiserv determining specific account processing.',
    moreInfo: (
      <p>
        External Status Code - Issuer-assigned code recognized by Fiserv
        determining specific account processing.
      </p>
    )
  },
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventDateOfEvent,
    fieldName: MethodFieldNameEnum.ActionCardCompromiseEventDateOfEvent,
    greenScreenName: 'DL',
    moreInfoText:
      'Lost Date - Date the customer’s presentation instrument was lost or stolen.',
    moreInfo: (
      <p>
        Lost Date - Date the customer’s presentation instrument was lost or
        stolen.
      </p>
    )
  },
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventPINLostIndicator,
    fieldName: MethodFieldNameEnum.ActionCardCompromiseEventPINLostIndicator,
    greenScreenName: 'PN',
    moreInfoText:
      'PIN Indicator - Indicator designating whether the PIN was lost with the presentation instrument.',
    moreInfo:
      'PIN Indicator - Indicator designating whether the PIN was lost with the presentation instrument.'
  },
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventFraudLossOperator,
    fieldName: MethodFieldNameEnum.ActionCardCompromiseEventFraudLossOperator,
    greenScreenName: 'TB',
    moreInfoText:
      'Taken By Identifier - Identifier of the Fraud Loss Operation clerk taking the lost or stolen report from the customer.',
    moreInfo:
      'Taken By Identifier - Identifier of the Fraud Loss Operation clerk taking the lost or stolen report from the customer.'
  },
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventPossibleFraudActivityCode,
    fieldName:
      MethodFieldNameEnum.ActionCardCompromiseEventPossibleFraudActivityCode,
    greenScreenName: 'PF',
    moreInfoText:
      'Possible Fraud Code - Code representing whether the customer suspects fraudulent activity when the plastic is first reported as lost or stolen.',
    moreInfo:
      'Possible Fraud Code - Code representing whether the customer suspects fraudulent activity when the plastic is first reported as lost or stolen.'
  },
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventFraudTypeCode,
    fieldName: MethodFieldNameEnum.ActionCardCompromiseEventFraudTypeCode,
    greenScreenName: 'TL',
    moreInfoText:
      'Fraud Type Code - Code representing the type of fraud on the account.',
    moreInfo:
      'Fraud Type Code - Code representing the type of fraud on the account.'
  },
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventLostLocationCode,
    fieldName: MethodFieldNameEnum.ActionCardCompromiseEventLostLocationCode,
    greenScreenName: 'LL',
    moreInfoText:
      'Loss Location Code - Code representing the location where the loss or theft occurred.',
    moreInfo:
      'Loss Location Code - Code representing the location where the loss or theft occurred.'
  },
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventAreaLost,
    fieldName: MethodFieldNameEnum.ActionCardCompromiseEventAreaLost,
    greenScreenName: 'AL',
    moreInfoText:
      'Loss Area Code - Code representing where the plastic was lost or stolen.',
    moreInfo:
      'Loss Area Code - Code representing where the plastic was lost or stolen.'
  },
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventMemoLine1,
    fieldName: MethodFieldNameEnum.ActionCardCompromiseEventMemoLine1,
    greenScreenName: '',
    moreInfoText: 'Memo Line 1 Text - Free-form message text line.',
    moreInfo: 'Memo Line 1 Text - Free-form message text line.'
  },
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventRushPlasticCode,
    fieldName: MethodFieldNameEnum.ActionCardCompromiseEventRushPlasticCode,
    greenScreenName: 'RI',
    moreInfoText:
      'Rush Plastics Code - Code representing the shipping method for rush plastics.',
    moreInfo:
      'Rush Plastics Code - Code representing the shipping method for rush plastics.'
  },
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventFraudInvestigatorCode,
    fieldName:
      MethodFieldNameEnum.ActionCardCompromiseEventFraudInvestigatorCode,
    greenScreenName: 'FR',
    moreInfoText:
      'Fraud Investigator Code - Issuer-defined code representing the investigator assigned to this case.',
    moreInfo:
      'Fraud Investigator Code - Issuer-defined code representing the investigator assigned to this case.'
  },
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventFraudAreaCode,
    fieldName: MethodFieldNameEnum.ActionCardCompromiseEventFraudAreaCode,
    greenScreenName: 'FR',
    moreInfoText:
      'Fraud Area Code - Code representing the warning bulletin region in which fraudulent activity first occurred on the account.',
    moreInfo:
      'Fraud Area Code - Code representing the warning bulletin region in which fraudulent activity first occurred on the account.'
  }
];

export const reportLostPIIDs: ParameterListProps[] = [
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventPiStatusCode,
    fieldName: MethodFieldNameEnum.ActionCardCompromiseEventPiStatusCode,
    greenScreenName: 'ST',
    moreInfoText:
      'External Status Code - Issuer-assigned code recognized by Fiserv determining specific account processing.',
    moreInfo:
      'External Status Code - Issuer-assigned code recognized by Fiserv determining specific account processing.'
  },
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventLostDate,
    fieldName: MethodFieldNameEnum.ActionCardCompromiseEventLostDate,
    greenScreenName: 'DL',
    moreInfoText:
      'Lost Date - Date the customer’s presentation instrument was lost or stolen.',
    moreInfo:
      'Lost Date - Date the customer’s presentation instrument was lost or stolen.'
  },
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventPINLostIndicator,
    fieldName: MethodFieldNameEnum.ActionCardCompromiseEventPINLostIndicator,
    greenScreenName: 'PN',
    moreInfoText:
      'PIN Indicator - Indicator designating whether the PIN was lost with the presentation instrument.',
    moreInfo:
      'PIN Indicator - Indicator designating whether the PIN was lost with the presentation instrument.'
  },
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventLossOperatorId,
    fieldName: MethodFieldNameEnum.ActionCardCompromiseEventLossOperatorId,
    greenScreenName: 'TB',
    moreInfoText:
      'Taken By Identifier - Identifier of the Fraud Loss Operation clerk taking the lost or stolen report from the customer.',
    moreInfo:
      'Taken By Identifier - Identifier of the Fraud Loss Operation clerk taking the lost or stolen report from the customer.'
  },
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventPossibleFraudActivityCode,
    fieldName:
      MethodFieldNameEnum.ActionCardCompromiseEventPossibleFraudActivityCode,
    greenScreenName: 'PF',
    moreInfoText:
      'Possible Fraud Code - Code representing whether the customer suspects fraudulent activity when the plastic is first reported as lost or stolen.',
    moreInfo:
      'Possible Fraud Code - Code representing whether the customer suspects fraudulent activity when the plastic is first reported as lost or stolen.'
  },
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventTypeLossCode,
    fieldName: MethodFieldNameEnum.ActionCardCompromiseEventTypeLossCode,
    greenScreenName: 'TL',
    moreInfoText:
      'Fraud Type Code - Code representing the type of fraud on the account.',
    moreInfo:
      'Fraud Type Code - Code representing the type of fraud on the account.'
  },
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventAreaLost,
    fieldName: MethodFieldNameEnum.ActionCardCompromiseEventAreaLost,
    greenScreenName: 'AL',
    moreInfoText:
      'Loss Area Code - Code representing where the plastic was lost or stolen.',
    moreInfo:
      'Loss Area Code - Code representing where the plastic was lost or stolen.'
  },
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventMemoLine1,
    fieldName: MethodFieldNameEnum.ActionCardCompromiseEventMemoLine1,
    greenScreenName: '',
    moreInfoText: 'Memo Line 1 Text - Free-form message text line.',
    moreInfo: 'Memo Line 1 Text - Free-form message text line.'
  },
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventProducePlasticIndicatorPLGEN,
    fieldName:
      MethodFieldNameEnum.ActionCardCompromiseEventProducePlasticIndicator,
    greenScreenName: 'PL GEN',
    moreInfoText:
      'Code determining whether to build a new presentation instrument identifier and whether to issue a plastic.',
    moreInfo:
      'Code determining whether to build a new presentation instrument identifier and whether to issue a plastic.'
  }
];

export const reportStolenPIIDs: ParameterListProps[] = [
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventPiStatusCode,
    fieldName: MethodFieldNameEnum.ActionCardCompromiseEventPiStatusCode,
    greenScreenName: 'ST',
    moreInfoText:
      'External Status Code - Issuer-assigned code recognized by Fiserv determining specific account processing.',
    moreInfo:
      'External Status Code - Issuer-assigned code recognized by Fiserv determining specific account processing.'
  },
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventStolenDate,
    fieldName: MethodFieldNameEnum.ActionCardCompromiseEventStolenDate,
    greenScreenName: 'DL',
    moreInfoText:
      'Stolen Date - Date the customer’s presentation instrument was lost or stolen.',
    moreInfo:
      'Stolen Date - Date the customer’s presentation instrument was lost or stolen.'
  },
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventPINLostIndicator,
    fieldName: MethodFieldNameEnum.ActionCardCompromiseEventPINLostIndicator,
    greenScreenName: 'PN',
    moreInfoText:
      'PIN Indicator - Indicator designating whether the PIN was lost with the presentation instrument.',
    moreInfo:
      'PIN Indicator - Indicator designating whether the PIN was lost with the presentation instrument.'
  },
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventLossOperatorId,
    fieldName: MethodFieldNameEnum.ActionCardCompromiseEventLossOperatorId,
    greenScreenName: 'TB',
    moreInfoText:
      'Taken By Identifier - Identifier of the Fraud Loss Operation clerk taking the lost or stolen report from the customer.',
    moreInfo:
      'Taken By Identifier - Identifier of the Fraud Loss Operation clerk taking the lost or stolen report from the customer.'
  },
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventPossibleFraudActivityCode,
    fieldName:
      MethodFieldNameEnum.ActionCardCompromiseEventPossibleFraudActivityCode,
    greenScreenName: 'PF',
    moreInfoText:
      'Possible Fraud Code - Code representing whether the customer suspects fraudulent activity when the plastic is first reported as lost or stolen.',
    moreInfo:
      'Possible Fraud Code - Code representing whether the customer suspects fraudulent activity when the plastic is first reported as lost or stolen.'
  },
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventTypeLossCode,
    fieldName: MethodFieldNameEnum.ActionCardCompromiseEventTypeLossCode,
    greenScreenName: 'TL',
    moreInfoText:
      'Fraud Type Code - Code representing the type of fraud on the account.',
    moreInfo:
      'Fraud Type Code - Code representing the type of fraud on the account.'
  },
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventAreaLost,
    fieldName: MethodFieldNameEnum.ActionCardCompromiseEventAreaLost,
    greenScreenName: 'AL',
    moreInfoText:
      'Loss Area Code - Code representing where the plastic was lost or stolen.',
    moreInfo:
      'Loss Area Code - Code representing where the plastic was lost or stolen.'
  },
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventMemoLine1,
    fieldName: MethodFieldNameEnum.ActionCardCompromiseEventMemoLine1,
    greenScreenName: '',
    moreInfoText: 'Memo Line 1 Text - Free-form message text line.',
    moreInfo: 'Memo Line 1 Text - Free-form message text line.'
  },
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventProducePlasticIndicatorPLGEN,
    fieldName:
      MethodFieldNameEnum.ActionCardCompromiseEventProducePlasticIndicator,
    greenScreenName: 'PL GEN',
    moreInfoText:
      'Code determining whether to build a new presentation instrument identifier and whether to issue a plastic.',
    moreInfo:
      'Code determining whether to build a new presentation instrument identifier and whether to issue a plastic.'
  }
];

export const visaWarningBulletinsParameterList: ParameterListProps[] = [
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventVisaWarningBulletinRegion,
    fieldName:
      MethodFieldNameEnum.ActionCardCompromiseEventVisaWarningBulletinRegion,
    greenScreenName: 'Region',
    moreInfoText:
      'Warning Bulletin Region Code - Code representing the warning bulletin region where the transaction occurred.',
    moreInfo:
      'Warning Bulletin Region Code - Code representing the warning bulletin region where the transaction occurred.'
  },
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventVisaResponseCode,
    fieldName: MethodFieldNameEnum.ActionCardCompromiseEventVisaResponseCode,
    greenScreenName: 'Response',
    moreInfoText:
      'Action Code - Code representing the merchant response or action taken.',
    moreInfo:
      'Action Code - Code representing the merchant response or action taken.'
  }
];

export const masterCardWarningBulletinsParameterList: ParameterListProps[] = [
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardStatus,
    fieldName: MethodFieldNameEnum.ActionCardCompromiseEventMasterCardStatus,
    greenScreenName:
      MethodFieldNameEnum.ActionCardCompromiseEventMasterCardStatus,
    moreInfoText:
      'Warning Bulletin Reason Code - Code representing the reason the customer account is being placed on the warning bulletin.',
    moreInfo:
      'Warning Bulletin Reason Code - Code representing the reason the customer account is being placed on the warning bulletin.'
  },
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardWarningBulletinRegion,
    fieldName:
      MethodFieldNameEnum.ActionCardCompromiseEventMasterCardWarningBulletinRegion,
    greenScreenName: 'Regions',
    moreInfoText:
      'Region Code - Code representing the warning bulletin region where the Mastercard® account is listed.',
    moreInfo:
      'Region Code - Code representing the warning bulletin region where the Mastercard® account is listed.'
  },
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardPurgeDate,
    fieldName: MethodFieldNameEnum.ActionCardCompromiseEventMasterCardPurgeDate,
    greenScreenName: '',
    moreInfoText:
      'Purge Date - Date that the account is scheduled to be removed from the warning bulletin files.',
    moreInfo:
      'Purge Date - Date that the account is scheduled to be removed from the warning bulletin files.'
  }
];

export const masterCardNegativeBulletinsParameterList: ParameterListProps[] = [
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardINASReasonCode,
    fieldName:
      MethodFieldNameEnum.ActionCardCompromiseEventMasterCardINASReasonCode,
    greenScreenName: 'Code',
    moreInfoText:
      'INAS Reason Code - Code representing the reason the account is being placed on the INAS exception files listing.',
    moreInfo:
      'INAS Reason Code - Code representing the reason the account is being placed on the INAS exception files listing.'
  },
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardAuthorizationLimitAmount,
    fieldName:
      MethodFieldNameEnum.ActionCardCompromiseEventMasterCardAuthorizationLimitAmount,
    greenScreenName: 'Limit',
    moreInfoText:
      'Authorization Limit Amount - Amount of the authorization limit.',
    moreInfo: 'Authorization Limit Amount - Amount of the authorization limit.'
  }
];

export const accountTransfer: ParameterListProps[] = [
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventAccountTransferTypeCode,
    fieldName:
      MethodFieldNameEnum.ActionCardCompromiseEventAccountTransferTypeCode,
    greenScreenName: 'Type',
    moreInfoText: 'Type of account transfer.',
    moreInfo: 'Type of account transfer.'
  },
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventProducePlasticIndicatorPlastic,
    fieldName:
      MethodFieldNameEnum.ActionCardCompromiseEventProducePlasticIndicator,
    greenScreenName: 'Plastic',
    moreInfoText:
      'Indicator designating whether a new plastic will be produced.',
    moreInfo: 'Indicator designating whether a new plastic will be produced.'
  },
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventGenerateLetterIdentifier,
    fieldName:
      MethodFieldNameEnum.ActionCardCompromiseEventGenerateLetterIdentifier,
    greenScreenName: 'Letter',
    moreInfoText: 'Letter identifier.',
    moreInfo: 'Letter identifier.'
  },
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventRushMailCode,
    fieldName: MethodFieldNameEnum.ActionCardCompromiseEventRushMailCode,
    greenScreenName: 'Rush',
    moreInfoText:
      'Code representing the shipping method for rush plastics or code representing the shipping method for a bulk shipment of rush plastics.',
    moreInfo:
      'Code representing the shipping method for rush plastics or code representing the shipping method for a bulk shipment of rush plastics.'
  }
];

export const piidTransfer: ParameterListProps[] = [
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventProducePlasticIndicatorSuppress,
    fieldName:
      MethodFieldNameEnum.ActionCardCompromiseEventProducePlasticIndicator,
    greenScreenName: 'Suppress',
    moreInfoText:
      'Indicator designating whether the System produces a new plastic.',
    moreInfo: 'Indicator designating whether the System produces a new plastic.'
  },
  {
    id: MethodFieldParameterEnum.ActionCardCompromiseEventRushPlasticCode,
    fieldName: MethodFieldNameEnum.ActionCardCompromiseEventRushPlasticCode,
    greenScreenName: 'RushCD',
    moreInfoText:
      'Code representing the shipping method for the replacement plastic.',
    moreInfo:
      'Code representing the shipping method for the replacement plastic.'
  }
];
