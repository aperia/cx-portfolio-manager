import EnhanceDatePicker from 'app/components/EnhanceDatePicker';
import EnhanceDropdownList from 'app/components/EnhanceDropdownList';
import TextAreaCountDown from 'app/components/TextAreaCountDown';
import {
  MethodFieldNameEnum,
  MethodFieldParameterEnum
} from 'app/constants/enums';
import { isValidDate } from 'app/helpers';
import {
  ChangeComponentEvent,
  ComboBox,
  NumericTextBox,
  TextBox
} from 'app/_libraries/_dls';
import { ParameterListProps } from 'pages/WorkflowCustomDataLinkElements/CustomDataLinkElementsStep/newElementParameterList';
import React from 'react';
import {
  accountTransfer,
  masterCardNegativeBulletinsParameterList,
  masterCardWarningBulletinsParameterList,
  piidTransfer,
  reportLostPIIDs,
  reportStolenAccountsParameterList,
  reportStolenPIIDs,
  visaWarningBulletinsParameterList
} from './newParameterList';

export interface ActionCardCompromiseMetadata {
  question1: RefData[];
  question2SingleEntity: RefData[];
  question2SeparateEntity: RefData[];
  question3: RefData[];
  question4: RefData[];
  transaction: RefData[];
  singleEntityNoTrans: RefData[];
  singleEntityYesTrans: RefData[];
  singleEntityReportLost: RefData[];
  singleEntityReportStolen: RefData[];
  separateEntityNoTrans: RefData[];
  separateEntityYesTrans: RefData[];
  separateEntityReportStolen: RefData[];
  separateEntityReportLost: RefData[];
  externalStatus: RefData[];
  piStatusCode: RefData[];
  lossOperatorId: RefData[];
  producePlasticIndicatorPLGEN: RefData[];
  producePlasticIndicatorPlastic: RefData[];
  producePlasticIndicatorSuppress: RefData[];
  typeLossCode: RefData[];
  pinLostIndicator: RefData[];
  fraudLossOperator: RefData[];
  possibleFraudActivityCode: RefData[];
  fraudTypeCode: RefData[];
  lostLocationCode: RefData[];
  rushPlasticCode: RefData[];
  visaWarningBulletinRegion: RefData[];
  visaResponseCode: RefData[];
  inasReasonCode: RefData[];
  masterCardStatus: RefData[];
  masterCardWarningBulletinRegion: RefData[];
  accountTransferTypeCode: RefData[];
  letterIdentifier: RefData[];
  rushMailCode: RefData[];
}

export const METHOD_ACCE_FIELDS_LOST_DEFAULT: Record<string, string> = {
  [MethodFieldParameterEnum.ActionCardCompromiseEventExternalStatus]: ' ',
  [MethodFieldParameterEnum.ActionCardCompromiseEventDateOfEvent]: '',
  [MethodFieldParameterEnum.ActionCardCompromiseEventPINLostIndicator]: ' ',
  [MethodFieldParameterEnum.ActionCardCompromiseEventFraudLossOperator]: ' ',
  [MethodFieldParameterEnum.ActionCardCompromiseEventPossibleFraudActivityCode]:
    ' ',
  [MethodFieldParameterEnum.ActionCardCompromiseEventFraudTypeCode]: ' ',
  [MethodFieldParameterEnum.ActionCardCompromiseEventLostLocationCode]: ' ',
  [MethodFieldParameterEnum.ActionCardCompromiseEventAreaLost]: '',
  [MethodFieldParameterEnum.ActionCardCompromiseEventMemoLine1]: '',
  [MethodFieldParameterEnum.ActionCardCompromiseEventRushPlasticCode]: ' '
};

export const METHOD_ACCE_FIELDS_STOLEN_DEFAULT: Record<string, string> = {
  [MethodFieldParameterEnum.ActionCardCompromiseEventExternalStatus]: ' ',
  [MethodFieldParameterEnum.ActionCardCompromiseEventDateOfEvent]: '',
  [MethodFieldParameterEnum.ActionCardCompromiseEventPINLostIndicator]: ' ',
  [MethodFieldParameterEnum.ActionCardCompromiseEventFraudLossOperator]: ' ',
  [MethodFieldParameterEnum.ActionCardCompromiseEventPossibleFraudActivityCode]:
    ' ',
  [MethodFieldParameterEnum.ActionCardCompromiseEventFraudTypeCode]: ' ',
  [MethodFieldParameterEnum.ActionCardCompromiseEventLostLocationCode]: ' ',
  [MethodFieldParameterEnum.ActionCardCompromiseEventAreaLost]: '',
  [MethodFieldParameterEnum.ActionCardCompromiseEventMemoLine1]: '',
  [MethodFieldParameterEnum.ActionCardCompromiseEventRushPlasticCode]: ' ',
  [MethodFieldParameterEnum.ActionCardCompromiseEventFraudInvestigatorCode]: '',
  [MethodFieldParameterEnum.ActionCardCompromiseEventFraudAreaCode]: ''
};

export const METHOD_ACCE_FIELDS_PIIDS_LOST_DEFAULT: Record<string, string> = {
  [MethodFieldParameterEnum.ActionCardCompromiseEventPiStatusCode]: ' ',
  [MethodFieldParameterEnum.ActionCardCompromiseEventLostDate]: '',
  [MethodFieldParameterEnum.ActionCardCompromiseEventPINLostIndicator]: ' ',
  [MethodFieldParameterEnum.ActionCardCompromiseEventLossOperatorId]: ' ',
  [MethodFieldParameterEnum.ActionCardCompromiseEventPossibleFraudActivityCode]:
    ' ',
  [MethodFieldParameterEnum.ActionCardCompromiseEventTypeLossCode]: ' ',
  [MethodFieldParameterEnum.ActionCardCompromiseEventAreaLost]: '',
  [MethodFieldParameterEnum.ActionCardCompromiseEventMemoLine1]: '',
  [MethodFieldParameterEnum.ActionCardCompromiseEventProducePlasticIndicatorPLGEN]:
    'Y'
};

export const METHOD_ACCE_FIELDS_PIIDS_STOLEN_DEFAULT: Record<string, string> = {
  [MethodFieldParameterEnum.ActionCardCompromiseEventPiStatusCode]: ' ',
  [MethodFieldParameterEnum.ActionCardCompromiseEventStolenDate]: '',
  [MethodFieldParameterEnum.ActionCardCompromiseEventPINLostIndicator]: ' ',
  [MethodFieldParameterEnum.ActionCardCompromiseEventLossOperatorId]: ' ',
  [MethodFieldParameterEnum.ActionCardCompromiseEventPossibleFraudActivityCode]:
    ' ',
  [MethodFieldParameterEnum.ActionCardCompromiseEventTypeLossCode]: ' ',
  [MethodFieldParameterEnum.ActionCardCompromiseEventAreaLost]: '',
  [MethodFieldParameterEnum.ActionCardCompromiseEventMemoLine1]: '',
  [MethodFieldParameterEnum.ActionCardCompromiseEventProducePlasticIndicatorPLGEN]:
    'Y'
};

export const METHOD_ACCE_FIELDS_VISA_DEFAULT: Record<string, string> = {
  [MethodFieldParameterEnum.ActionCardCompromiseEventVisaWarningBulletinRegion]:
    ' ',
  [MethodFieldParameterEnum.ActionCardCompromiseEventVisaResponseCode]: ' '
};

export const METHOD_ACCE_FIELDS_MASTERCARD_WARNING_DEFAULT: Record<
  string,
  string
> = {
  [MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardStatus]: ' ',
  [MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardWarningBulletinRegion]:
    ' ',
  [MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardPurgeDate]: ''
};

export const METHOD_ACCE_FIELDS_MASTERCARD_NEGATIVE_DEFAULT: Record<
  string,
  string
> = {
  [MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardINASReasonCode]:
    ' ',
  [MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardAuthorizationLimitAmount]:
    ''
};

export const METHOD_ACCE_FIELDS_ACCOUNT_TRANSFER: Record<string, string> = {
  [MethodFieldParameterEnum.ActionCardCompromiseEventAccountTransferTypeCode]:
    ' ',
  [MethodFieldParameterEnum.ActionCardCompromiseEventProducePlasticIndicatorPlastic]:
    ' ',
  [MethodFieldParameterEnum.ActionCardCompromiseEventGenerateLetterIdentifier]:
    '',
  [MethodFieldParameterEnum.ActionCardCompromiseEventRushMailCode]: ' '
};

export const METHOD_ACCE_FIELDS_PIIDS_TRANSFER: Record<string, string> = {
  [MethodFieldParameterEnum.ActionCardCompromiseEventProducePlasticIndicatorSuppress]:
    ' ',
  [MethodFieldParameterEnum.ActionCardCompromiseEventRushPlasticCode]: ' '
};

export const templateACCE = (qs1: string, qs2: string) => {
  let template1 = {};
  let template2 = {};

  switch (qs1) {
    case 'Report Lost Accounts using the SL, Lost/Stolen Report transaction.':
      template1 = METHOD_ACCE_FIELDS_LOST_DEFAULT;
      break;
    case 'Report Stolen Accounts using the SL, Lost/Stolen Report transaction.':
      template1 = METHOD_ACCE_FIELDS_STOLEN_DEFAULT;
      break;
    case 'Report Lost PIIDs using the SLP, Lost/Stolen Report transaction.':
      template1 = METHOD_ACCE_FIELDS_PIIDS_LOST_DEFAULT;
      break;
    case 'Report Stolen PIIDs using the SLP, Lost/Stolen Report transaction.':
      template1 = METHOD_ACCE_FIELDS_PIIDS_STOLEN_DEFAULT;
      break;
    case 'Account Transfer using the AT, Account Transfers transaction.':
      template1 = METHOD_ACCE_FIELDS_ACCOUNT_TRANSFER;
      break;
    case 'PIID Transfer using the PIT, Presentation Instrument Transfer transaction.':
      template1 = METHOD_ACCE_FIELDS_PIIDS_TRANSFER;
      break;
  }

  switch (qs2) {
    case 'Visa exception file using the WC, Visa Warning Bulletins transaction.':
      template2 = METHOD_ACCE_FIELDS_VISA_DEFAULT;
      break;
    case 'MasterCard negative file using the WMA, Add Cardholder Account transaction.':
      template2 = METHOD_ACCE_FIELDS_MASTERCARD_NEGATIVE_DEFAULT;
      break;
    case 'MasterCard warning bulletin using the WB, MasterCard Warning Bulletins transaction.':
      template2 = METHOD_ACCE_FIELDS_MASTERCARD_WARNING_DEFAULT;
      break;
  }

  return { template1, template2 };
};

export const generateSubTitleActionCardCompromise = (txt: string) => {
  let text = '';
  switch (txt) {
    case 'Visa exception file using the WC, Visa Warning Bulletins transaction.':
      text =
        'Visa Exception File using the WC, Visa Warning Bulletins transaction.';
      break;
    case 'MasterCard negative file using the WMA, Add Cardholder Account transaction.':
      text =
        'MasterCard Negative File using the WMA, Add Cardholder Account transaction.';
      break;
    case 'MasterCard warning bulletin using the WB, MasterCard Warning Bulletins transaction.':
      text =
        'MasterCard Warning Bulletin using the WB, MasterCard Warning Bulletins transaction.';
      break;
    default:
      text = txt;
      break;
  }

  return text;
};

export const generateActionCardCompromiseTitle = (txt: string) => {
  let title = '';

  switch (txt) {
    case 'Visa exception file using the WC, Visa Warning Bulletins transaction.':
      title = 'WC, Visa Warning Bulletins';
      break;
    case 'MasterCard negative file using the WMA, Add Cardholder Account transaction.':
      title = 'WMA, Add Cardholder Account';
      break;
    case 'MasterCard warning bulletin using the WB, MasterCard Warning Bulletins transaction.':
      title = 'WB, MasterCard Warning Bulletins';
      break;
    case 'Report Lost Accounts using the SL, Lost/Stolen Report transaction.':
      title = 'SL, Lost/Stolen Report';
      break;
    case 'Report Stolen Accounts using the SL, Lost/Stolen Report transaction.':
      title = 'SL, Lost/Stolen Report';
      break;
    case 'Report Lost PIIDs using the SLP, Lost/Stolen Report transaction.':
      title = 'SLP, Lost/Stolen Report';
      break;
    case 'Report Stolen PIIDs using the SLP, Lost/Stolen Report transaction.':
      title = 'SLP, Lost/Stolen Report';
      break;
    case 'Account Transfer using the AT, Account Transfers transaction.':
      title = 'AT, Account Transfers';
      break;
    case 'PIID Transfer using the PIT, Presentation Instrument Transfer transaction.':
      title = 'PIT, Presentation Instrument Transfer';
      break;
  }

  return title;
};

export const dynamicActionCardCompromiseParameterList = (
  question: string | undefined
) => {
  let value = [] as ParameterListProps[] | MagicKeyValue;
  switch (question) {
    case 'Visa exception file using the WC, Visa Warning Bulletins transaction.':
      value = visaWarningBulletinsParameterList;
      break;
    case 'MasterCard negative file using the WMA, Add Cardholder Account transaction.':
      value = masterCardNegativeBulletinsParameterList;
      break;
    case 'MasterCard warning bulletin using the WB, MasterCard Warning Bulletins transaction.':
      value = masterCardWarningBulletinsParameterList;
      break;
    case 'Report Stolen Accounts using the SL, Lost/Stolen Report transaction.':
      value = reportStolenAccountsParameterList;
      break;
    case 'Report Lost Accounts using the SL, Lost/Stolen Report transaction.':
      value = reportStolenAccountsParameterList.slice(0, 10);
      break;
    case 'Report Lost PIIDs using the SLP, Lost/Stolen Report transaction.':
      value = reportLostPIIDs;
      break;
    case 'Report Stolen PIIDs using the SLP, Lost/Stolen Report transaction.':
      value = reportStolenPIIDs;
      break;
    case 'Account Transfer using the AT, Account Transfers transaction.':
      value = accountTransfer;
      break;
    case 'PIID Transfer using the PIT, Presentation Instrument Transfer transaction.':
      value = piidTransfer;
      break;
  }
  return value;
};

export const renderMappingForm: any = (
  data: Record<string, any>,
  t: any,
  metadata: ActionCardCompromiseMetadata,
  ACCE: WorkflowSetupMethodObjectParams,
  minDate: Date,
  maxDate: Date,
  handleFormChange: (
    field: keyof WorkflowSetupMethodObjectParams,
    isRefData?: boolean
  ) => (e: ChangeComponentEvent) => void
) => {
  const {
    externalStatus,
    pinLostIndicator,
    fraudLossOperator,
    possibleFraudActivityCode,
    fraudTypeCode,
    lostLocationCode,
    rushPlasticCode,
    lossOperatorId,
    piStatusCode,
    producePlasticIndicatorPLGEN,
    producePlasticIndicatorPlastic,
    producePlasticIndicatorSuppress,
    typeLossCode,
    visaResponseCode,
    visaWarningBulletinRegion,
    inasReasonCode,
    masterCardStatus,
    masterCardWarningBulletinRegion,
    accountTransferTypeCode,
    letterIdentifier,
    rushMailCode
  } = metadata;
  const paramId = data.id;
  switch (paramId) {
    case MethodFieldParameterEnum.ActionCardCompromiseEventExternalStatus: {
      const value = externalStatus.find(
        o =>
          o.code ===
          ACCE?.[
            MethodFieldParameterEnum.ActionCardCompromiseEventExternalStatus
          ]
      );

      return (
        <EnhanceDropdownList
          placeholder={t('txt_select_an_option')}
          dataTestId="actionCardCompromiseEvent__actionCardCompromiseEventExternalStatus"
          size="small"
          value={value}
          onChange={handleFormChange(
            MethodFieldParameterEnum.ActionCardCompromiseEventExternalStatus,
            true
          )}
          options={externalStatus}
        />
      );
    }

    case MethodFieldParameterEnum.ActionCardCompromiseEventDateOfEvent: {
      const _val =
        ACCE?.[MethodFieldParameterEnum.ActionCardCompromiseEventDateOfEvent];
      const val = isValidDate(_val) ? new Date(_val!) : undefined;
      return (
        <div>
          <EnhanceDatePicker
            size="small"
            id="actionCardCompromiseEvent__actionCardCompromiseEventDateOfEventFormat"
            placeholder="mm/dd/yyyy"
            value={val}
            minDate={minDate}
            maxDate={maxDate}
            name={MethodFieldNameEnum.ActionCardCompromiseEventDateOfEvent}
            onChange={handleFormChange(
              MethodFieldParameterEnum.ActionCardCompromiseEventDateOfEvent
            )}
          />
        </div>
      );
    }

    case MethodFieldParameterEnum.ActionCardCompromiseEventPINLostIndicator: {
      const value = pinLostIndicator.find(
        o =>
          o.code ===
          ACCE?.[
            MethodFieldParameterEnum.ActionCardCompromiseEventPINLostIndicator
          ]
      );

      return (
        <EnhanceDropdownList
          placeholder={t('txt_select_an_option')}
          dataTestId="actionCardCompromiseEvent__actionCardCompromiseEventPINLostIndicator"
          size="small"
          value={value}
          onChange={handleFormChange(
            MethodFieldParameterEnum.ActionCardCompromiseEventPINLostIndicator,
            true
          )}
          options={pinLostIndicator}
        />
      );
    }

    case MethodFieldParameterEnum.ActionCardCompromiseEventFraudLossOperator: {
      const value = fraudLossOperator.find(
        o =>
          o.code ===
          ACCE?.[
            MethodFieldParameterEnum.ActionCardCompromiseEventFraudLossOperator
          ]
      );

      return (
        <ComboBox
          placeholder={t('txt_select_an_option')}
          textField="text"
          value={value}
          size="small"
          onChange={handleFormChange(
            MethodFieldParameterEnum.ActionCardCompromiseEventFraudLossOperator,
            true
          )}
          noResult={t('txt_no_results_found_without_dot')}
        >
          {fraudLossOperator.map(item => (
            <ComboBox.Item key={item.code} value={item} label={item.text} />
          ))}
        </ComboBox>
      );
    }

    case MethodFieldParameterEnum.ActionCardCompromiseEventFraudTypeCode: {
      const value = fraudTypeCode.find(
        o =>
          o.code ===
          ACCE?.[
            MethodFieldParameterEnum.ActionCardCompromiseEventFraudTypeCode
          ]
      );

      return (
        <EnhanceDropdownList
          placeholder={t('txt_select_an_option')}
          dataTestId="actionCardCompromiseEvent__actionCardCompromiseEventFraudTypeCode"
          size="small"
          value={value}
          onChange={handleFormChange(
            MethodFieldParameterEnum.ActionCardCompromiseEventFraudTypeCode,
            true
          )}
          options={fraudTypeCode}
        />
      );
    }

    case MethodFieldParameterEnum.ActionCardCompromiseEventLostLocationCode: {
      const value = lostLocationCode.find(
        o =>
          o.code ===
          ACCE?.[
            MethodFieldParameterEnum.ActionCardCompromiseEventLostLocationCode
          ]
      );

      return (
        <EnhanceDropdownList
          placeholder={t('txt_select_an_option')}
          dataTestId="actionCardCompromiseEvent__actionCardCompromiseEventLostLocationCode"
          size="small"
          value={value}
          onChange={handleFormChange(
            MethodFieldParameterEnum.ActionCardCompromiseEventLostLocationCode,
            true
          )}
          options={lostLocationCode}
        />
      );
    }

    case MethodFieldParameterEnum.ActionCardCompromiseEventPossibleFraudActivityCode: {
      const value = possibleFraudActivityCode.find(
        o =>
          o.code ===
          ACCE?.[
            MethodFieldParameterEnum
              .ActionCardCompromiseEventPossibleFraudActivityCode
          ]
      );

      return (
        <EnhanceDropdownList
          placeholder={t('txt_select_an_option')}
          dataTestId="actionCardCompromiseEvent__actionCardCompromiseEventPossibleFraudActivityCode"
          size="small"
          value={value}
          onChange={handleFormChange(
            MethodFieldParameterEnum.ActionCardCompromiseEventPossibleFraudActivityCode,
            true
          )}
          options={possibleFraudActivityCode}
        />
      );
    }

    case MethodFieldParameterEnum.ActionCardCompromiseEventRushPlasticCode: {
      const value = rushPlasticCode.find(
        o =>
          o.code ===
          ACCE?.[
            MethodFieldParameterEnum.ActionCardCompromiseEventRushPlasticCode
          ]
      );

      return (
        <EnhanceDropdownList
          placeholder={t('txt_select_an_option')}
          dataTestId="actionCardCompromiseEvent__actionCardCompromiseEventRushPlasticCode"
          size="small"
          value={value}
          onChange={handleFormChange(
            MethodFieldParameterEnum.ActionCardCompromiseEventRushPlasticCode,
            true
          )}
          options={rushPlasticCode}
        />
      );
    }
    case MethodFieldParameterEnum.ActionCardCompromiseEventAreaLost: {
      return (
        <TextBox
          placeholder={t('txt_enter_a_value')}
          dataTestId="actionCardCompromiseEvent__actionCardCompromiseEventAreaLost"
          size="sm"
          value={
            ACCE?.[MethodFieldParameterEnum.ActionCardCompromiseEventAreaLost]
          }
          onChange={handleFormChange(
            MethodFieldParameterEnum.ActionCardCompromiseEventAreaLost
          )}
          maxLength={2}
        />
      );
    }

    case MethodFieldParameterEnum.ActionCardCompromiseEventMemoLine1: {
      return (
        <TextAreaCountDown
          placeholder={t('txt_enter_a_value')}
          dataTestId="actionCardCompromiseEvent__actionCardCompromiseEventMemoLine1"
          value={
            ACCE?.[MethodFieldParameterEnum.ActionCardCompromiseEventMemoLine1]
          }
          onChange={handleFormChange(
            MethodFieldParameterEnum.ActionCardCompromiseEventMemoLine1
          )}
          className="resize-none"
          rows={4}
          maxLength={75}
        />
      );
    }

    case MethodFieldParameterEnum.ActionCardCompromiseEventFraudInvestigatorCode: {
      return (
        <TextBox
          placeholder={t('txt_enter_a_value')}
          dataTestId="actionCardCompromiseEvent__actionCardCompromiseEventFraudInvestigatorCode"
          size="sm"
          value={
            ACCE?.[
              MethodFieldParameterEnum
                .ActionCardCompromiseEventFraudInvestigatorCode
            ]
          }
          onChange={handleFormChange(
            MethodFieldParameterEnum.ActionCardCompromiseEventFraudInvestigatorCode
          )}
          maxLength={2}
        />
      );
    }

    case MethodFieldParameterEnum.ActionCardCompromiseEventFraudAreaCode: {
      return (
        <TextBox
          placeholder={t('txt_enter_a_value')}
          dataTestId="actionCardCompromiseEvent__actionCardCompromiseEventFraudAreaCode"
          size="sm"
          value={
            ACCE?.[
              MethodFieldParameterEnum.ActionCardCompromiseEventFraudAreaCode
            ]
          }
          onChange={handleFormChange(
            MethodFieldParameterEnum.ActionCardCompromiseEventFraudAreaCode
          )}
          maxLength={2}
        />
      );
    }

    case MethodFieldParameterEnum.ActionCardCompromiseEventLostDate: {
      const _val =
        ACCE?.[MethodFieldParameterEnum.ActionCardCompromiseEventLostDate];
      const val = isValidDate(_val) ? new Date(_val!) : undefined;
      return (
        <div>
          <EnhanceDatePicker
            size="small"
            id="actionCardCompromiseEvent__actionCardCompromiseEventLostDateFormat"
            placeholder="mm/dd/yyyy"
            value={val}
            minDate={minDate}
            maxDate={maxDate}
            name={MethodFieldNameEnum.ActionCardCompromiseEventLostDate}
            onChange={handleFormChange(
              MethodFieldParameterEnum.ActionCardCompromiseEventLostDate
            )}
          />
        </div>
      );
    }

    case MethodFieldParameterEnum.ActionCardCompromiseEventStolenDate: {
      const _val =
        ACCE?.[MethodFieldParameterEnum.ActionCardCompromiseEventStolenDate];
      const val = isValidDate(_val) ? new Date(_val!) : undefined;
      return (
        <div>
          <EnhanceDatePicker
            size="small"
            id="actionCardCompromiseEvent__actionCardCompromiseEventStolenDateFormat"
            placeholder="mm/dd/yyyy"
            value={val}
            minDate={minDate}
            maxDate={maxDate}
            name={MethodFieldNameEnum.ActionCardCompromiseEventStolenDate}
            onChange={handleFormChange(
              MethodFieldParameterEnum.ActionCardCompromiseEventStolenDate
            )}
          />
        </div>
      );
    }

    case MethodFieldParameterEnum.ActionCardCompromiseEventPiStatusCode: {
      const value = piStatusCode.find(
        o =>
          o.code ===
          ACCE?.[MethodFieldParameterEnum.ActionCardCompromiseEventPiStatusCode]
      );

      return (
        <EnhanceDropdownList
          placeholder={t('txt_select_an_option')}
          dataTestId="actionCardCompromiseEvent__actionCardCompromiseEventPiStatusCode"
          size="small"
          value={value}
          onChange={handleFormChange(
            MethodFieldParameterEnum.ActionCardCompromiseEventPiStatusCode,
            true
          )}
          options={piStatusCode}
        />
      );
    }

    case MethodFieldParameterEnum.ActionCardCompromiseEventLossOperatorId: {
      const value = lossOperatorId.find(
        o =>
          o.code ===
          ACCE?.[
            MethodFieldParameterEnum.ActionCardCompromiseEventLossOperatorId
          ]
      );

      return (
        <ComboBox
          placeholder={t('txt_select_an_option')}
          textField="text"
          value={value}
          size="small"
          onChange={handleFormChange(
            MethodFieldParameterEnum.ActionCardCompromiseEventLossOperatorId,
            true
          )}
          noResult={t('txt_no_results_found_without_dot')}
        >
          {lossOperatorId.map(item => (
            <ComboBox.Item key={item.code} value={item} label={item.text} />
          ))}
        </ComboBox>
      );
    }

    case MethodFieldParameterEnum.ActionCardCompromiseEventProducePlasticIndicatorPLGEN: {
      const value = producePlasticIndicatorPLGEN.find(
        o =>
          o.code ===
          ACCE?.[
            MethodFieldParameterEnum
              .ActionCardCompromiseEventProducePlasticIndicatorPLGEN
          ]
      );

      return (
        <EnhanceDropdownList
          placeholder={t('txt_select_an_option')}
          dataTestId="actionCardCompromiseEvent__actionCardCompromiseEventProducePlasticIndicatorPLGEN"
          size="small"
          value={value}
          onChange={handleFormChange(
            MethodFieldParameterEnum.ActionCardCompromiseEventProducePlasticIndicatorPLGEN,
            true
          )}
          options={producePlasticIndicatorPLGEN}
        />
      );
    }

    case MethodFieldParameterEnum.ActionCardCompromiseEventProducePlasticIndicatorPlastic: {
      const value = producePlasticIndicatorPlastic.find(
        o =>
          o.code ===
          ACCE?.[
            MethodFieldParameterEnum
              .ActionCardCompromiseEventProducePlasticIndicatorPlastic
          ]
      );

      return (
        <EnhanceDropdownList
          placeholder={t('txt_select_an_option')}
          dataTestId="actionCardCompromiseEvent__ActionCardCompromiseEventProducePlasticIndicatorPlastic"
          size="small"
          value={value}
          onChange={handleFormChange(
            MethodFieldParameterEnum.ActionCardCompromiseEventProducePlasticIndicatorPlastic,
            true
          )}
          options={producePlasticIndicatorPlastic}
        />
      );
    }

    case MethodFieldParameterEnum.ActionCardCompromiseEventProducePlasticIndicatorSuppress: {
      const value = producePlasticIndicatorSuppress.find(
        o =>
          o.code ===
          ACCE?.[
            MethodFieldParameterEnum
              .ActionCardCompromiseEventProducePlasticIndicatorSuppress
          ]
      );

      return (
        <EnhanceDropdownList
          placeholder={t('txt_select_an_option')}
          dataTestId="actionCardCompromiseEvent__actionCardCompromiseEventProducePlasticIndicatorSuppress"
          size="small"
          value={value}
          onChange={handleFormChange(
            MethodFieldParameterEnum.ActionCardCompromiseEventProducePlasticIndicatorSuppress,
            true
          )}
          options={producePlasticIndicatorSuppress}
        />
      );
    }

    case MethodFieldParameterEnum.ActionCardCompromiseEventTypeLossCode: {
      const value = typeLossCode.find(
        o =>
          o.code ===
          ACCE?.[MethodFieldParameterEnum.ActionCardCompromiseEventTypeLossCode]
      );

      return (
        <EnhanceDropdownList
          placeholder={t('txt_select_an_option')}
          dataTestId="actionCardCompromiseEvent__actionCardCompromiseEventTypeLossCode"
          size="small"
          value={value}
          onChange={handleFormChange(
            MethodFieldParameterEnum.ActionCardCompromiseEventTypeLossCode,
            true
          )}
          options={typeLossCode}
        />
      );
    }

    case MethodFieldParameterEnum.ActionCardCompromiseEventVisaResponseCode: {
      const value = visaResponseCode.find(
        o =>
          o.code ===
          ACCE?.[
            MethodFieldParameterEnum.ActionCardCompromiseEventVisaResponseCode
          ]
      );

      return (
        <EnhanceDropdownList
          placeholder={t('txt_select_an_option')}
          dataTestId="actionCardCompromiseEvent__actionCardCompromiseEventVisaResponseCode"
          size="small"
          value={value}
          onChange={handleFormChange(
            MethodFieldParameterEnum.ActionCardCompromiseEventVisaResponseCode,
            true
          )}
          options={visaResponseCode}
        />
      );
    }

    case MethodFieldParameterEnum.ActionCardCompromiseEventVisaWarningBulletinRegion: {
      const value = visaWarningBulletinRegion.find(
        o =>
          o.code ===
          ACCE?.[
            MethodFieldParameterEnum
              .ActionCardCompromiseEventVisaWarningBulletinRegion
          ]
      );

      return (
        <ComboBox
          placeholder={t('txt_select_an_option')}
          textField="text"
          value={value}
          size="small"
          onChange={handleFormChange(
            MethodFieldParameterEnum.ActionCardCompromiseEventVisaWarningBulletinRegion,
            true
          )}
          noResult={t('txt_no_results_found_without_dot')}
        >
          {visaWarningBulletinRegion.map(item => (
            <ComboBox.Item key={item.code} value={item} label={item.text} />
          ))}
        </ComboBox>
      );
    }

    case MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardINASReasonCode: {
      const value = inasReasonCode.find(
        o =>
          o.code ===
          ACCE?.[
            MethodFieldParameterEnum
              .ActionCardCompromiseEventMasterCardINASReasonCode
          ]
      );

      return (
        <EnhanceDropdownList
          placeholder={t('txt_select_an_option')}
          dataTestId="actionCardCompromiseEvent__actionCardCompromiseEventMasterCardINASReasonCode"
          size="small"
          value={value}
          onChange={handleFormChange(
            MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardINASReasonCode,
            true
          )}
          options={inasReasonCode}
        />
      );
    }

    case MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardStatus: {
      const value = masterCardStatus.find(
        o =>
          o.code ===
          ACCE?.[
            MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardStatus
          ]
      );

      return (
        <EnhanceDropdownList
          placeholder={t('txt_select_an_option')}
          dataTestId="actionCardCompromiseEvent__actionCardCompromiseEventMasterCardStatus"
          size="small"
          value={value}
          onChange={handleFormChange(
            MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardStatus,
            true
          )}
          options={masterCardStatus}
        />
      );
    }

    case MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardWarningBulletinRegion: {
      const value = masterCardWarningBulletinRegion.find(
        o =>
          o.code ===
          ACCE?.[
            MethodFieldParameterEnum
              .ActionCardCompromiseEventMasterCardWarningBulletinRegion
          ]
      );

      return (
        <ComboBox
          placeholder={t('txt_select_an_option')}
          textField="text"
          value={value}
          size="small"
          onChange={handleFormChange(
            MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardWarningBulletinRegion,
            true
          )}
          noResult={t('txt_no_results_found_without_dot')}
        >
          {masterCardWarningBulletinRegion.map(item => (
            <ComboBox.Item key={item.code} value={item} label={item.text} />
          ))}
        </ComboBox>
      );
    }

    case MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardPurgeDate: {
      const _val =
        ACCE?.[
          MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardPurgeDate
        ];
      const val = isValidDate(_val) ? new Date(_val!) : undefined;
      return (
        <div>
          <EnhanceDatePicker
            size="small"
            id="actionCardCompromiseEvent__actionCardCompromiseEventMasterCardPurgeDateFormat"
            placeholder="mm/dd/yyyy"
            value={val}
            minDate={minDate}
            maxDate={maxDate}
            name={
              MethodFieldNameEnum.ActionCardCompromiseEventMasterCardPurgeDate
            }
            onChange={handleFormChange(
              MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardPurgeDate
            )}
          />
        </div>
      );
    }

    case MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardAuthorizationLimitAmount: {
      return (
        <NumericTextBox
          dataTestId="addNewMethod__actionCardCompromiseEventMasterCardAuthorizationLimitAmount"
          size="sm"
          format="c0"
          showStep={false}
          maxLength={5}
          autoFocus={false}
          value={
            ACCE?.[
              MethodFieldParameterEnum
                .ActionCardCompromiseEventMasterCardAuthorizationLimitAmount
            ] || ''
          }
          onChange={handleFormChange(
            MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardAuthorizationLimitAmount
          )}
        />
      );
    }

    case MethodFieldParameterEnum.ActionCardCompromiseEventGenerateLetterIdentifier: {
      const value = letterIdentifier.find(
        o =>
          o.code ===
          ACCE?.[
            MethodFieldParameterEnum
              .ActionCardCompromiseEventGenerateLetterIdentifier
          ]
      );

      return (
        <ComboBox
          placeholder={t('txt_select_an_option')}
          textField="text"
          value={value}
          size="small"
          onChange={handleFormChange(
            MethodFieldParameterEnum.ActionCardCompromiseEventGenerateLetterIdentifier,
            true
          )}
          noResult={t('txt_no_results_found_without_dot')}
        >
          {letterIdentifier.map((item: MagicKeyValue) => (
            <ComboBox.Item key={item.code} value={item} label={item.text} />
          ))}
        </ComboBox>
      );
    }

    case MethodFieldParameterEnum.ActionCardCompromiseEventAccountTransferTypeCode: {
      const value = accountTransferTypeCode.find(
        o =>
          o.code ===
          ACCE?.[
            MethodFieldParameterEnum
              .ActionCardCompromiseEventAccountTransferTypeCode
          ]
      );

      return (
        <EnhanceDropdownList
          placeholder={t('txt_select_an_option')}
          dataTestId="actionCardCompromiseEvent__actionCardCompromiseEventAccountTransferTypeCode"
          size="small"
          value={value}
          onChange={handleFormChange(
            MethodFieldParameterEnum.ActionCardCompromiseEventAccountTransferTypeCode,
            true
          )}
          options={accountTransferTypeCode}
        />
      );
    }

    case MethodFieldParameterEnum.ActionCardCompromiseEventRushMailCode: {
      const value = rushMailCode.find(
        o =>
          o.code ===
          ACCE?.[MethodFieldParameterEnum.ActionCardCompromiseEventRushMailCode]
      );

      return (
        <EnhanceDropdownList
          placeholder={t('txt_select_an_option')}
          dataTestId="actionCardCompromiseEvent__actionCardCompromiseEventRushMailCode"
          size="small"
          value={value}
          onChange={handleFormChange(
            MethodFieldParameterEnum.ActionCardCompromiseEventRushMailCode,
            true
          )}
          options={rushMailCode}
        />
      );
    }
  }
};
