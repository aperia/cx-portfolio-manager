import { render, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { queryAllByClass } from 'app/_libraries/_dls/test-utils/queryHelpers';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowActionCardCompromiseEvent';
import React from 'react';
import UploadParameterList from '.';
import { ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME } from '../ActionCardCompromiseEventSelectActionsStep/helper';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const useSelectElementMetadataForActionCardCompromiseEvent = jest.spyOn(
  WorkflowSetup,
  'useSelectElementMetadataForActionCardCompromiseEvent'
);

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <UploadParameterList {...props} />
    </div>
  );
};

describe('WorkflowBulkExternalStatusManagement > UploadParameterList', () => {
  beforeEach(() => {
    useSelectElementMetadataForActionCardCompromiseEvent.mockReturnValue({
      singleEntityYesTrans: [{ name: 'name', type: 'type' }],
      singleEntityNoTrans: [],
      singleEntityReportLost: [],
      singleEntityReportStolen: [],
      separateEntityYesTrans: [],
      separateEntityNoTrans: [],
      separateEntityReportLost: [],
      separateEntityReportStolen: []
    } as any);
  });

  it('Should render a grid with content', async () => {
    const props = {
      stepId: '1',
      selectedStep: '2',
      clearFormName: '',
      dependencies: {
        action: {
          radio: ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SINGLE_ENTITY,
          question2:
            'Report Lost Accounts using the SL, Lost/Stolen Report transaction.'
        }
      },
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = renderComponent(props);

    const expandBtn = queryAllByClass(wrapper.container, /icon icon-plus/)[0];
    userEvent.click(expandBtn);

    expect(
      wrapper.getByText('txt_action_card_compromise_parameter_list_desc')
    ).toBeInTheDocument();
    const minusBtn = queryAllByClass(wrapper.container, /icon icon-minus/)[0];
    userEvent.click(minusBtn);
  });

  it('Should render a grid with content', async () => {
    const props = {
      stepId: '1',
      selectedStep: '2',
      clearFormName: '',
      dependencies: {
        action: {
          radio: ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SINGLE_ENTITY,
          question2:
            'Report Stolen Accounts using the SL, Lost/Stolen Report transaction.'
        }
      },
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = renderComponent(props);

    const expandBtn = queryAllByClass(wrapper.container, /icon icon-plus/)[0];
    userEvent.click(expandBtn);

    expect(
      wrapper.getByText('txt_action_card_compromise_parameter_list_desc')
    ).toBeInTheDocument();
    const minusBtn = queryAllByClass(wrapper.container, /icon icon-minus/)[0];
    userEvent.click(minusBtn);
  });

  it('Should render a grid with content', async () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      clearFormName: '',
      dependencies: {
        action: {
          radio: ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SINGLE_ENTITY,
          question3: 'No'
        }
      },
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = renderComponent(props);

    const expandBtn = queryAllByClass(wrapper.container, /icon icon-plus/)[0];
    userEvent.click(expandBtn);

    expect(
      wrapper.getByText('txt_action_card_compromise_parameter_list_desc')
    ).toBeInTheDocument();
    const minusBtn = queryAllByClass(wrapper.container, /icon icon-minus/)[0];
    userEvent.click(minusBtn);
  });

  it('Should render a grid with content', async () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      clearFormName: '',
      dependencies: {
        action: {
          radio: ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SINGLE_ENTITY,
          question3: 'Yes'
        }
      },
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = renderComponent(props);

    const expandBtn = queryAllByClass(wrapper.container, /icon icon-plus/)[0];
    userEvent.click(expandBtn);

    expect(
      wrapper.getByText('txt_action_card_compromise_parameter_list_desc')
    ).toBeInTheDocument();
    const minusBtn = queryAllByClass(wrapper.container, /icon icon-minus/)[0];
    userEvent.click(minusBtn);
  });

  it('Should render a grid with content', async () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      clearFormName: '',
      dependencies: {
        action: {
          radio: ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SEPARATE_ENTITY,
          question3: 'Yes'
        }
      },
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = renderComponent(props);

    const expandBtn = queryAllByClass(wrapper.container, /icon icon-plus/)[0];
    userEvent.click(expandBtn);

    expect(
      wrapper.getByText('txt_action_card_compromise_parameter_list_desc')
    ).toBeInTheDocument();
    const minusBtn = queryAllByClass(wrapper.container, /icon icon-minus/)[0];
    userEvent.click(minusBtn);
  });

  it('Should render a grid with content', async () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      clearFormName: '',
      dependencies: {
        action: {
          radio: ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SEPARATE_ENTITY,
          question2:
            'Report Stolen PIIDs using the SLP, Lost/Stolen Report transaction.'
        }
      },
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = renderComponent(props);

    const expandBtn = queryAllByClass(wrapper.container, /icon icon-plus/)[0];
    userEvent.click(expandBtn);

    expect(
      wrapper.getByText('txt_action_card_compromise_parameter_list_desc')
    ).toBeInTheDocument();
    const minusBtn = queryAllByClass(wrapper.container, /icon icon-minus/)[0];
    userEvent.click(minusBtn);
  });

  it('Should render a grid with content', async () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      clearFormName: '',
      dependencies: {
        action: {
          radio: ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SEPARATE_ENTITY,
          question3: 'No'
        }
      },
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = renderComponent(props);

    const expandBtn = queryAllByClass(wrapper.container, /icon icon-plus/)[0];
    userEvent.click(expandBtn);

    expect(
      wrapper.getByText('txt_action_card_compromise_parameter_list_desc')
    ).toBeInTheDocument();
    const minusBtn = queryAllByClass(wrapper.container, /icon icon-minus/)[0];
    userEvent.click(minusBtn);
  });

  it('Should render a grid with content', async () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      clearFormName: '',
      dependencies: {
        action: {
          radio: ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SEPARATE_ENTITY,
          question2:
            'Report Lost PIIDs using the SLP, Lost/Stolen Report transaction.'
        }
      },
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = renderComponent(props);

    const expandBtn = queryAllByClass(wrapper.container, /icon icon-plus/)[0];
    userEvent.click(expandBtn);

    expect(
      wrapper.getByText('txt_action_card_compromise_parameter_list_desc')
    ).toBeInTheDocument();
    const minusBtn = queryAllByClass(wrapper.container, /icon icon-minus/)[0];
    userEvent.click(minusBtn);
  });

  it('Should render a grid with content', async () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      clearFormName: '',
      dependencies: {
        action: {
          radio: ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SEPARATE_ENTITY,
          question2: {
            text: 'Report Stolen PIIDs using the SLP, Lost/Stolen Report transaction.'
          }
        }
      },
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = renderComponent(props);

    const expandBtn = queryAllByClass(wrapper.container, /icon icon-plus/)[0];
    userEvent.click(expandBtn);

    expect(
      wrapper.getByText('txt_action_card_compromise_parameter_list_desc')
    ).toBeInTheDocument();
    const minusBtn = queryAllByClass(wrapper.container, /icon icon-minus/)[0];
    userEvent.click(minusBtn);
  });

  it('Should render a grid with content', async () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      clearFormName: '',
      dependencies: {
        action: {
          radio: ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SEPARATE_ENTITY,
          question2: {
            text: 'text'
          }
        }
      },
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = renderComponent(props);

    const expandBtn = queryAllByClass(wrapper.container, /icon icon-plus/)[0];
    userEvent.click(expandBtn);

    expect(
      wrapper.getByText('txt_action_card_compromise_parameter_list_desc')
    ).toBeInTheDocument();
    const minusBtn = queryAllByClass(wrapper.container, /icon icon-minus/)[0];
    userEvent.click(minusBtn);
  });

  it('Should render a grid with content', async () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      clearFormName: '',
      dependencies: {
        action: {
          radio: ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SINGLE_ENTITY,
          question2: 'text'
        }
      },
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = renderComponent(props);

    const expandBtn = queryAllByClass(wrapper.container, /icon icon-plus/)[0];
    userEvent.click(expandBtn);

    expect(
      wrapper.getByText('txt_action_card_compromise_parameter_list_desc')
    ).toBeInTheDocument();
    const minusBtn = queryAllByClass(wrapper.container, /icon icon-minus/)[0];
    userEvent.click(minusBtn);
  });

  it('Should render a grid with content', async () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      clearFormName: '',
      dependencies: {
        action: {
          radio: 'text',
          question2: 'text'
        }
      },
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = renderComponent(props);

    const expandBtn = queryAllByClass(wrapper.container, /icon icon-plus/)[0];
    userEvent.click(expandBtn);

    expect(
      wrapper.getByText('txt_action_card_compromise_parameter_list_desc')
    ).toBeInTheDocument();
    const minusBtn = queryAllByClass(wrapper.container, /icon icon-minus/)[0];
    userEvent.click(minusBtn);
  });
});
