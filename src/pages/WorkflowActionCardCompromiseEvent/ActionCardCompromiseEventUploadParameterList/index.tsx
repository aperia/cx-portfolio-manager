import {
  Button,
  ColumnType,
  Grid,
  Icon,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import { useSelectElementMetadataForActionCardCompromiseEvent } from 'pages/_commons/redux/WorkflowSetup';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, { useEffect, useMemo, useState } from 'react';
import { ACCESelectActionsValue } from '../ActionCardCompromiseEventSelectActionsStep';
import { ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME } from '../ActionCardCompromiseEventSelectActionsStep/helper';

export interface ParameterListProp {
  isValid?: boolean;
}

const ParameterList: React.FC<
  WorkflowSetupProps<ParameterListProp, ACCESelectActionsValue>
> = ({ dependencies, stepId, selectedStep }) => {
  const { t } = useTranslation();
  const [isExpand, setIsExpand] = useState(false);

  const { action } = dependencies as ACCESelectActionsValue;

  const {
    singleEntityYesTrans,
    singleEntityNoTrans,
    singleEntityReportLost,
    singleEntityReportStolen,
    separateEntityYesTrans,
    separateEntityNoTrans,
    separateEntityReportLost,
    separateEntityReportStolen
  } = useSelectElementMetadataForActionCardCompromiseEvent();

  const gridData = useMemo(() => {
    if (
      action?.radio === ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SINGLE_ENTITY
    ) {
      if (action?.question3 === 'Yes') {
        return singleEntityYesTrans;
      }
      if (action?.question3 === 'No') {
        return singleEntityNoTrans;
      }
      if (
        action?.question2 ===
        'Report Lost Accounts using the SL, Lost/Stolen Report transaction.'
      ) {
        return singleEntityReportLost;
      }
      if (
        action?.question2 ===
        'Report Stolen Accounts using the SL, Lost/Stolen Report transaction.'
      ) {
        return singleEntityReportStolen;
      }
      return [];
    }
    if (
      action?.radio === ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SEPARATE_ENTITY
    ) {
      if (action?.question3 === 'Yes') {
        return separateEntityYesTrans;
      }
      if (action?.question3 === 'No') {
        return separateEntityNoTrans;
      }
      if (
        action?.question2 ===
        'Report Lost PIIDs using the SLP, Lost/Stolen Report transaction.'
      ) {
        return separateEntityReportLost;
      }
      if (
        action?.question2 ===
        'Report Stolen PIIDs using the SLP, Lost/Stolen Report transaction.'
      ) {
        return separateEntityReportStolen;
      }
      return [];
    }
    return [];
  }, [
    action,
    singleEntityNoTrans,
    singleEntityYesTrans,
    singleEntityReportLost,
    singleEntityReportStolen,
    separateEntityYesTrans,
    separateEntityNoTrans,
    separateEntityReportLost,
    separateEntityReportStolen
  ]);

  const columns: ColumnType[] = useMemo(
    () => [
      {
        id: 'name',
        Header: t('txt_field_name'),
        accessor: 'code',
        width: 211
      },
      {
        id: 'type',
        Header: t('txt_field_type'),
        accessor: 'type',
        width: 103
      },
      {
        id: 'description',
        Header: t('txt_description'),
        accessor: 'text'
      }
    ],
    [t]
  );

  useEffect(() => {
    if (stepId !== selectedStep) {
      setIsExpand(false);
    }
  }, [stepId, selectedStep]);

  return (
    <div className="border-bottom py-24">
      <div className="d-flex align-items-center ">
        <div className="flex-shrink-0 ml-n2 mr-8">
          <Tooltip
            element={isExpand ? t('txt_collapse') : t('txt_expand')}
            variant="primary"
            placement="top"
          >
            <Button
              variant="icon-secondary"
              size="sm"
              onClick={() => setIsExpand(!isExpand)}
            >
              <Icon name={isExpand ? 'minus' : 'plus'} />
            </Button>
          </Tooltip>
        </div>
        <h5>{t('txt_parameter_list')}</h5>
      </div>
      {isExpand && (
        <div className="pt-8">
          <p className="color-grey">
            {t('txt_action_card_compromise_parameter_list_desc')}
          </p>
          {!isEmpty(gridData) && (
            <div className="mt-16">
              <Grid columns={columns} data={gridData} />
            </div>
          )}
        </div>
      )}
    </div>
  );
};

const ExtraStaticParameterList =
  ParameterList as WorkflowSetupStaticProp<ParameterListProp>;

ExtraStaticParameterList.defaultValues = { isValid: true };

export default ExtraStaticParameterList;
