import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import {
  stepRegistry,
  uploadFileStepRegistry
} from '../_commons/WorkflowSetup/registries';
import ActionCardCompromiseEventSelectActionsStep from './ActionCardCompromiseEventSelectActionsStep';
import ActionCardCompromiseEventStep from './ActionCardCompromiseEventStep';
import ParameterList from './ActionCardCompromiseEventUploadParameterList';
import GettingStartStep from './GettingStartStep';

stepRegistry.registerStep(
  'GetStartedActionCardCompromiseEvent',
  GettingStartStep
);

stepRegistry.registerStep(
  'ActionCardCompromiseEvent',
  ActionCardCompromiseEventStep,
  [
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__ACTION_CARD_COMPROMISE_EVENT__CONFIGURE_PARAMETERS
  ]
);

stepRegistry.registerStep(
  'ActionCardCompromiseEventSelectActions',
  ActionCardCompromiseEventSelectActionsStep,
  [
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__ACTION_CARD_COMPROMISE_EVENT_SELECT_ACTIONS
  ]
);

uploadFileStepRegistry.registerComponent(
  'ActionCardCompromiseEventParameterList',
  ParameterList
);
