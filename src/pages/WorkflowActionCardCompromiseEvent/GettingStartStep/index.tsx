import { WORKFLOW_SETUP } from 'app/constants/local-storage';
import { Button, Icon, TransDLS, useTranslation } from 'app/_libraries/_dls';
import OverviewModal from 'pages/_commons/OverviewModal';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, { useRef, useState } from 'react';
import GettingStartSummary from './GettingStartSummary';

export interface FormGettingStart {
  isValid?: boolean;
  alreadyShown?: boolean;
}
const GettingStartStep: React.FC<WorkflowSetupProps<FormGettingStart>> = ({
  formValues,
  setFormValues
}) => {
  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;

  const { t } = useTranslation();
  const [showOverview, setShowOverview] = useState(
    formValues.alreadyShown
      ? false
      : localStorage.getItem(WORKFLOW_SETUP.SHOW_OVERVIEW_AGAIN) !== 'false'
  );

  return (
    <React.Fragment>
      <div className="position-relative">
        <div className="absolute-top-right mt-n28">
          <Button
            className="mr-n8"
            variant="outline-primary"
            size="sm"
            onClick={() => setShowOverview(true)}
          >
            {t('txt_view_overview')}
          </Button>
        </div>
        <div className="row">
          <div className="col-12 col-md-6 mt-24">
            <div className="bg-light-l20 rounded-lg p-16">
              <div className="text-center">
                <Icon name="request" size="12x" className="color-grey-l16" />
              </div>
              <p className="mt-8 color-grey">
                {t('txt_action_card_compromise_event_desc_1')}
              </p>
              <p className="mt-8 color-grey">
                {t('txt_action_card_compromise_event_desc_2')}
              </p>
              <p className="mt-8 color-grey">
                {t('txt_action_card_compromise_event_desc_3')}
              </p>
              <p className="mt-8 color-grey">
                {t('txt_action_card_compromise_event_desc_4')}
              </p>
              <p className="mt-8 color-grey">
                {t('txt_action_card_compromise_event_desc_5')}
              </p>
              <p className="mt-16 fw-600">
                {t('txt_action_card_compromise_event_desc_6_bold')}
              </p>
              <ul className="list-dot color-grey">
                <li>{t('txt_action_card_compromise_event_desc_7')}</li>
                <li>{t('txt_action_card_compromise_event_desc_8')}</li>
                <li>{t('txt_action_card_compromise_event_desc_9')}</li>
                <li>{t('txt_action_card_compromise_event_desc_10')}</li>
                <li>{t('txt_action_card_compromise_event_desc_11')}</li>
                <li>{t('txt_action_card_compromise_event_desc_12')}</li>
              </ul>
              <p className="mt-16 fw-600">
                {t('txt_action_card_compromise_event_desc_13_bold')}
              </p>
              <p className="mt-8 color-grey">
                {t('txt_action_card_compromise_event_desc_14')}
              </p>
              <ul className="list-dot color-grey">
                <li>{t('txt_action_card_compromise_event_desc_15')}</li>
                <li>{t('txt_action_card_compromise_event_desc_16')}</li>
                <li>{t('txt_action_card_compromise_event_desc_17')}</li>
                <li>{t('txt_action_card_compromise_event_desc_18')}</li>
                <li>{t('txt_action_card_compromise_event_desc_19')}</li>
              </ul>
              <p className="mt-16 fw-600">
                {t('txt_action_card_compromise_event_desc_20_bold')}
              </p>
              <ul className="list-dot color-grey">
                <li>{t('txt_action_card_compromise_event_desc_21')}</li>
              </ul>
              <p className="mt-16 fw-600">
                {t('txt_action_card_compromise_event_desc_22_bold')}
              </p>
              <ul className="list-dot color-grey">
                <li>{t('txt_action_card_compromise_event_desc_23')}</li>
                <li>{t('txt_action_card_compromise_event_desc_24')}</li>
              </ul>
              <p className="mt-16 fw-600">
                {t('txt_action_card_compromise_event_desc_25_bold')}
              </p>
              <ul className="list-dot color-grey">
                <li>{t('txt_action_card_compromise_event_desc_26')}</li>
                <li>{t('txt_action_card_compromise_event_desc_27')}</li>
                <li>{t('txt_action_card_compromise_event_desc_28')}</li>
                <li>{t('txt_action_card_compromise_event_desc_29')}</li>
              </ul>
            </div>
          </div>
          <div className="col-12 col-md-6 mt-24 color-grey">
            <p>{t('txt_action_card_compromise_event_on_right_1')}</p>
            <p className="mt-8">
              <TransDLS keyTranslation="txt_action_card_compromise_event_on_right_2">
                <strong className="color-grey-d20" />
              </TransDLS>
            </p>
            <p className="mt-8">
              <TransDLS keyTranslation="txt_action_card_compromise_event_on_right_3">
                <strong className="color-grey-d20" />
              </TransDLS>
            </p>
            <p className="mt-8">
              <TransDLS keyTranslation="txt_action_card_compromise_event_on_right_4">
                <strong className="color-grey-d20" />
              </TransDLS>
            </p>
            <p className="mt-8">
              <TransDLS keyTranslation="txt_action_card_compromise_event_on_right_5">
                <strong className="color-grey-d20" />
              </TransDLS>
            </p>
            <p className="mt-8">
              <TransDLS keyTranslation="txt_action_card_compromise_event_on_right_6">
                <strong className="color-grey-d20" />
              </TransDLS>
            </p>
          </div>
        </div>
      </div>
      {showOverview && (
        <OverviewModal
          id="overviewWorkflowSetup"
          show
          onClose={() => {
            setShowOverview(false);
            keepRef.current.setFormValues({ alreadyShown: true });
          }}
        />
      )}
    </React.Fragment>
  );
};

const ExtraStaticGettingStartStep =
  GettingStartStep as WorkflowSetupStaticProp<FormGettingStart>;

ExtraStaticGettingStartStep.summaryComponent = GettingStartSummary;

ExtraStaticGettingStartStep.defaultValues = {
  isValid: true
};

export default ExtraStaticGettingStartStep;
