import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { default as stepRegistry } from '../_commons/WorkflowSetup/registries/stepRegistry';
import { default as uploadFileStepRegistry } from '../_commons/WorkflowSetup/registries/uploadFileStepRegistry';

const registerFunc = jest.spyOn(stepRegistry, 'registerStep');

const registerUploadFunc = jest.spyOn(
  uploadFileStepRegistry,
  'registerComponent'
);

describe('CustomDataLinkElements > registerSteps', () => {
  it('Should register components', async () => {
    await import('./registerSteps');

    expect(registerFunc).toBeCalledWith(
      'ActionCardCompromiseEvent',
      expect.anything(),
      [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__ACTION_CARD_COMPROMISE_EVENT__CONFIGURE_PARAMETERS
      ]
    );

    expect(registerFunc).toBeCalledWith(
      'ActionCardCompromiseEventSelectActions',
      expect.anything(),
      [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__ACTION_CARD_COMPROMISE_EVENT_SELECT_ACTIONS
      ]
    );

    expect(registerUploadFunc).toBeCalledWith(
      'ActionCardCompromiseEventParameterList',
      expect.anything()
    );
  });
});
