import { MethodFieldParameterEnum as ParameterEnum } from 'app/constants/enums';
import { ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME } from './helper';
import parseFormValues from './parseFormValues';

describe('pages > WorkflowActionCardCompromise > ActionCardCompromiseStep > parseFormValues', () => {
  it('parseFormValues', () => {
    let configurations: any = {};
    const id = 'id';
    const input: any = {
      data: {
        configurations,
        workflowSetupData: [{ id, status: 'INPROGRESS' }]
      }
    };

    // isvalid
    let result = parseFormValues(input, id, jest.fn);
    expect(result?.isValid).toBeFalsy();

    // other step
    result = parseFormValues(input, 'id1', jest.fn);
    expect(result).toBeUndefined();

    configurations = {
      parameters: [
        {
          name: ParameterEnum.ActionCardCompromiseEventQuestion2SingleEntity,
          value:
            'Report Lost Accounts using the SL, Lost/Stolen Report transaction.'
        },
        {
          name: ParameterEnum.ActionCardCompromiseEventQuestion2SeparateEntity,
          value: 'value'
        },
        {
          name: ParameterEnum.ActionCardCompromiseEventQuestion3,
          value: 'value'
        },
        {
          name: ParameterEnum.ActionCardCompromiseEventQuestion4,
          value: 'value'
        }
      ]
    };
    input.data.configurations = configurations;

    result = parseFormValues(input, id, jest.fn);
    expect(result?.isValid).toEqual(false);

    configurations = {
      parameters: [
        {
          name: ParameterEnum.ActionCardCompromiseEventQuestion2SeparateEntity,
          value:
            'Visa exception file using the WC, Visa Warning Bulletins transaction.'
        },
        {
          name: ParameterEnum.ActionCardCompromiseEventQuestion3,
          value: 'value'
        },
        {
          name: ParameterEnum.ActionCardCompromiseEventQuestion4,
          value: 'value'
        }
      ]
    };
    input.data.configurations = configurations;

    result = parseFormValues(input, id, jest.fn);
    expect(result?.isValid).toEqual(false);

    configurations = {
      parameters: [
        {
          name: ParameterEnum.ActionCardCompromiseEventQuestion3,
          value: 'value'
        },
        {
          name: ParameterEnum.ActionCardCompromiseEventQuestion4,
          value: 'value'
        }
      ]
    };
    input.data.configurations = configurations;

    result = parseFormValues(input, id, jest.fn);
    expect(result?.isValid).toEqual(false);

    configurations = {
      parameters: [
        {
          name: ParameterEnum.ActionCardCompromiseEventQuestion4,
          value: 'value'
        }
      ]
    };
    input.data.configurations = configurations;

    result = parseFormValues(input, id, jest.fn);
    expect(result?.isValid).toEqual(false);

    configurations = {
      parameters: [
        {
          name: ParameterEnum.ActionCardCompromiseEventQuestion1,
          value: ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SINGLE_ENTITY
        },
        {
          name: ParameterEnum.ActionCardCompromiseEventQuestion2SingleEntity,
          value: 'Account Transfer using the AT, Account Transfers transaction.'
        }
      ]
    };
    input.data.configurations = configurations;

    result = parseFormValues(input, id, jest.fn);
    expect(result?.isValid).toEqual(false);

    configurations = {
      parameters: [
        {
          name: ParameterEnum.ActionCardCompromiseEventQuestion1,
          value: ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SEPARATE_ENTITY
        },
        {
          name: ParameterEnum.ActionCardCompromiseEventQuestion2SingleEntity,
          value:
            'PIID Transfer using the PIT, Presentation Instrument Transfer transaction.'
        }
      ]
    };
    input.data.configurations = configurations;

    result = parseFormValues(input, id, jest.fn);
    expect(result?.isValid).toEqual(false);
  });
});
