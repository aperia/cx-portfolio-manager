import EnhanceDropdownList from 'app/components/EnhanceDropdownList';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import {
  confirmEditStepProps,
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { useUnsavedChangeRegistry } from 'app/hooks';
import { Icon, Radio, Tooltip, useTranslation } from 'app/_libraries/_dls';
import { STATUS } from 'app/_libraries/_dls/components/Upload/helper';
import { isEmpty, isEqual } from 'lodash';
import {
  actionsWorkflowSetup,
  useSelectElementMetadataForActionCardCompromiseEvent
} from 'pages/_commons/redux/WorkflowSetup';
import React, { useEffect, useMemo, useState } from 'react';
import { useDispatch } from 'react-redux';
import { ACCESelectActionsValue } from '.';
import { ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME } from './helper';

const SelectActions: React.FC<
  WorkflowSetupProps<ACCESelectActionsValue> & {
    clearFormNames: string[];
  }
> = ({
  setFormValues,
  formValues,
  savedAt,
  clearFormValues,
  clearFormNames,
  dependencies
}) => {
  const { t } = useTranslation();

  const dispatch = useDispatch();

  const [initialValues, setInitialValues] = useState(formValues);
  const { action } = formValues;

  const metadata = useSelectElementMetadataForActionCardCompromiseEvent();

  const {
    question1,
    question2SingleEntity,
    question2SeparateEntity,
    question3,
    question4
  } = metadata;

  const question2 = useMemo(
    () =>
      action?.radio === ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SINGLE_ENTITY
        ? question2SingleEntity
        : question2SeparateEntity,
    [action, question2SingleEntity, question2SeparateEntity]
  );

  const isYesNoQuestion =
    (action?.radio === ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SINGLE_ENTITY &&
      action?.question2 ===
        'Account Transfer using the AT, Account Transfers transaction.') ||
    (action?.radio ===
      ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SEPARATE_ENTITY &&
      action?.question2 ===
        'PIID Transfer using the PIT, Presentation Instrument Transfer transaction.');

  useEffect(() => {
    dispatch(
      actionsWorkflowSetup.getWorkflowMetadata([
        MethodFieldParameterEnum.ActionCardCompromiseEventQuestion1,
        MethodFieldParameterEnum.ActionCardCompromiseEventQuestion2SingleEntity,
        MethodFieldParameterEnum.ActionCardCompromiseEventQuestion2SeparateEntity,
        MethodFieldParameterEnum.ActionCardCompromiseEventQuestion3,
        MethodFieldParameterEnum.ActionCardCompromiseEventQuestion4,
        MethodFieldParameterEnum.ActionCardCompromiseEventTransaction,
        MethodFieldParameterEnum.ActionCardCompromiseEventSingleEntityAccountTransferYesTransaction,
        MethodFieldParameterEnum.ActionCardCompromiseEventSingleEntityAccountTransferNoTransaction,
        MethodFieldParameterEnum.ActionCardCompromiseEventSingleEntityReportStolenAccountTransaction,
        MethodFieldParameterEnum.ActionCardCompromiseEventSingleEntityReportLostAccountTransaction,
        MethodFieldParameterEnum.ActionCardCompromiseEventSeparateEntityPIIDTransferYesTransaction,
        MethodFieldParameterEnum.ActionCardCompromiseEventSeparateEntityPIIDTransferNoTransaction,
        MethodFieldParameterEnum.ActionCardCompromiseEventSeparateEntityReportStolenPIIDTransaction,
        MethodFieldParameterEnum.ActionCardCompromiseEventSeparateEntityReportLostPIIDTransaction,
        MethodFieldParameterEnum.ActionCardCompromiseEventExternalStatus,
        MethodFieldParameterEnum.ActionCardCompromiseEventPINLostIndicator,
        MethodFieldParameterEnum.ActionCardCompromiseEventFraudLossOperator,
        MethodFieldParameterEnum.ActionCardCompromiseEventPossibleFraudActivityCode,
        MethodFieldParameterEnum.ActionCardCompromiseEventFraudTypeCode,
        MethodFieldParameterEnum.ActionCardCompromiseEventLostLocationCode,
        MethodFieldParameterEnum.ActionCardCompromiseEventRushPlasticCode,
        MethodFieldParameterEnum.ActionCardCompromiseEventPiStatusCode,
        MethodFieldParameterEnum.ActionCardCompromiseEventLossOperatorId,
        MethodFieldParameterEnum.ActionCardCompromiseEventProducePlasticIndicatorPLGEN,
        MethodFieldParameterEnum.ActionCardCompromiseEventProducePlasticIndicatorSuppress,
        MethodFieldParameterEnum.ActionCardCompromiseEventProducePlasticIndicatorPlastic,
        MethodFieldParameterEnum.ActionCardCompromiseEventTypeLossCode,
        MethodFieldParameterEnum.ActionCardCompromiseEventVisaResponseCode,
        MethodFieldParameterEnum.ActionCardCompromiseEventVisaWarningBulletinRegion,
        MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardINASReasonCode,
        MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardStatus,
        MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardWarningBulletinRegion,
        MethodFieldParameterEnum.ActionCardCompromiseEventAccountTransferTypeCode,
        MethodFieldParameterEnum.ActionCardCompromiseEventGenerateLetterIdentifier,
        MethodFieldParameterEnum.ActionCardCompromiseEventRushMailCode
      ])
    );
  }, [dispatch]);

  useEffect(() => {
    setInitialValues(formValues);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [savedAt]);

  const isValid = useMemo(
    () =>
      isYesNoQuestion
        ? !isEmpty(action?.radio) &&
          !isEmpty(action?.question2) &&
          !isEmpty(action?.question3)
        : !isEmpty(action?.radio) && !isEmpty(action?.question2),
    [action, isYesNoQuestion]
  );

  const onChangeRadio = (e: any) => {
    const value = e.target.value;
    setFormValues({
      ...formValues,
      action: {
        radio: value
      }
    });
  };

  const onChangeQuestion2 = (e: any) => {
    const value = e.target.value;
    setFormValues({
      action: {
        ...formValues.action,
        question2: value?.code,
        question3: ''
      }
    });
  };

  const onChangeQuestion3 =
    (isRefData = false) =>
    (e: any) => {
      const value = e.target.value;
      setFormValues({
        action: {
          ...formValues.action,
          question3: isRefData ? value?.code : value
        }
      });
    };

  useEffect(() => {
    if (formValues.isValid === isValid) return;
    setFormValues({ isValid });
  }, [isValid, setFormValues, formValues]);

  const handleResetUploadStep = () => {
    const compareForm = handleCompareForm();

    if (compareForm) {
      const file = (dependencies as any)?.files?.[0];
      const attachmentId = file?.idx;
      const isValidFile = file?.status === STATUS.valid;
      clearFormNames?.forEach(clearFormValues);
      isValidFile &&
        dispatch(actionsWorkflowSetup.deleteTemplateFile({ attachmentId }));
    }
  };

  const handleCompareForm = () => {
    const init = initialValues?.action;
    const form = formValues?.action;

    switch (form?.radio) {
      case ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SINGLE_ENTITY:
        if (
          form?.question2 ===
            'Account Transfer using the AT, Account Transfers transaction.' ||
          form?.question2 !== init?.question2
        )
          return true;
        break;

      case ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SEPARATE_ENTITY:
        if (
          form?.question2 ===
            'PIID Transfer using the PIT, Presentation Instrument Transfer transaction.' ||
          form?.question2 !== init?.question2
        )
          return true;
        break;
    }

    return false;
  };

  const newDepend = useMemo(() => {
    return initialValues?.action?.radio ===
      ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SEPARATE_ENTITY
      ? {
          ...dependencies,
          parameters: {
            ...(dependencies as any)?.parameters,
            'produce.plastic.indicator.pl.gen': ''
          }
        }
      : dependencies;
  }, [dependencies, initialValues]);

  const isEditStep5 =
    Object.values((newDepend as any)?.parameters || {}).some(
      (el: any) => el?.toString().trim() !== ''
    ) ||
    Object.values((newDepend as any)?.optionalParameters || {}).some(
      (el: any) => el?.toString().trim() !== ''
    );

  const changeFormOptional =
    !isYesNoQuestion &&
    !isEqual(initialValues?.action?.question3, action?.question3) &&
    isEditStep5;

  const formChanged =
    initialValues?.action?.radio !== action?.radio ||
    !isEqual(initialValues?.action?.question2, action?.question2) ||
    (isYesNoQuestion &&
      !isEqual(initialValues?.action?.question3, action?.question3));

  useEffect(() => {
    const formEventStep = clearFormNames?.[1];

    if (
      !isYesNoQuestion &&
      !isEqual(initialValues?.action?.question3, action?.question3)
    ) {
      formEventStep && clearFormValues(formEventStep);
    }
  }, [isYesNoQuestion, clearFormValues, clearFormNames, initialValues, action]);

  const unsaveOptions = useMemo(
    () => ({
      ...confirmEditStepProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__ACTION_CARD_COMPROMISE_EVENT_SELECT_ACTIONS_CONFIRM_EDIT,
      confirmFirst: true,
      priority: 1
    }),
    []
  );

  useUnsavedChangeRegistry(
    unsaveOptions,
    [
      (!isEmpty((dependencies as any)?.files) && formChanged) ||
        changeFormOptional
    ],
    handleResetUploadStep
  );

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__ACTION_CARD_COMPROMISE_EVENT_SELECT_ACTIONS,
      priority: 1
    },
    [formChanged]
  );

  return (
    <div className="group-card">
      <div className="group-card-item">
        <div className="row">
          <div className="col-12 col-lg-8">
            <p className="title">{t('txt_action_card_compromise_radio_1')}</p>
          </div>
          <div className="col-12 col-lg-4 mt-8 mt-lg-0">
            <div className="group-control justify-content-lg-end">
              {question1.map((item: RefData, i) => (
                <div key={item.code} className="group-control-item">
                  <Radio className="custom-control-root">
                    <Radio.Input
                      onChange={onChangeRadio}
                      checked={item.code === action?.radio}
                      value={item.code}
                      id={item.code}
                      name={item.code}
                      className="checked-style"
                    />
                  </Radio>
                  <label htmlFor={item.code} className="dls-radio-label">
                    {item.code}
                  </label>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
      <div className="group-card-item">
        <div className="row align-items-center">
          <div className="col-12 col-lg-8 col-xxl-10">
            <p className="title">{t('txt_action_card_compromise_radio_2')}</p>
          </div>
          <div className="col-6 col-lg-4 col-xxl-2 mt-8 mt-lg-0">
            <EnhanceDropdownList
              required
              value={question2.find(q => q.code == action?.question2)}
              disabled={isEmpty(action?.radio)}
              onChange={onChangeQuestion2}
              label={t('txt_action_card_compromise_dropdown_name')}
              name="CardCompromiseAction"
              options={question2}
            />
          </div>
        </div>
      </div>
      {action?.question2 && (
        <div
          className={`group-card-item ${
            isYesNoQuestion ? 'py-16 pl-16 pr-10 pr-lg-16' : 'p-16'
          }`}
        >
          <div
            className={`row ${!isYesNoQuestion ? 'align-items-center' : ''}`}
          >
            <div
              className={
                isYesNoQuestion
                  ? 'col-12 col-lg-9'
                  : 'col-12 col-lg-8 col-xxl-10'
              }
            >
              <div className="mt-n10 mb-n16">
                <p className="title">
                  {isYesNoQuestion
                    ? t('txt_action_card_compromise_radio_4')
                    : t('txt_action_card_compromise_radio_3')}
                  {isYesNoQuestion && (
                    <Tooltip
                      element={
                        <>
                          <p>{t('txt_action_card_compromise_tooltip_1')}</p>
                          <p className="mt-16">
                            {t('txt_action_card_compromise_tooltip_2')}
                          </p>
                        </>
                      }
                      displayOnClick={false}
                      placement="left-start"
                      triggerClassName="align-sub ml-8 py-12 cursor-default"
                    >
                      <Icon
                        name="information"
                        size="5x"
                        className="color-grey-l16"
                      />
                    </Tooltip>
                  )}
                </p>
              </div>
            </div>

            {isYesNoQuestion ? (
              <div className="col-12 col-lg-3 mt-8 mt-lg-0">
                <div className="group-control justify-content-lg-end">
                  {question4.map((item: RefData, i) => (
                    <div key={item.code} className="group-control-item">
                      <Radio className="custom-control-root">
                        <Radio.Input
                          onChange={onChangeQuestion3()}
                          checked={item.code === action?.question3}
                          value={item.code}
                          id={item.code}
                          name={item.code}
                          className="checked-style"
                        />
                      </Radio>
                      <label htmlFor={item.code} className="dls-radio-label">
                        {item.code}
                      </label>
                    </div>
                  ))}
                </div>
              </div>
            ) : (
              <div className="col-6 col-lg-4 col-xxl-2 mt-8 mt-lg-0">
                <EnhanceDropdownList
                  value={question3.find(q => q.code == action?.question3)}
                  onChange={onChangeQuestion3(true)}
                  label={t('txt_action_card_compromise_option')}
                  name="Option"
                  options={question3}
                />
              </div>
            )}
          </div>
        </div>
      )}
    </div>
  );
};

export default SelectActions;
