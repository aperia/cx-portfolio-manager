import { act } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { renderWithMockStore } from 'app/utils';
import { STATUS } from 'app/_libraries/_dls/components/Upload/helper';
import * as workflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowActionCardCompromiseEvent';
import React from 'react';
import SelectActions from '.';

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({ t })
  };
});

let confirmFunc: jest.fn;
jest.mock('app/hooks/useUnsavedChangeRegistry', () => {
  return {
    __esModule: true,
    useUnsavedChangeRegistry: (options: any, changes: any, onConfirm: any) => {
      onConfirm && onConfirm();
    }
  };
});

jest.mock('app/components/EnhanceDropdownList', () => {
  return {
    __esModule: true,
    default: ({
      placeholder,
      dataTestId,
      value,
      onChange,
      options,
      ...rest
    }: any) => (
      <div data-testid={`${dataTestId}_dls-dropdown-list`}>
        <div>EnhanceDropdownList</div>
        <div>{placeholder}</div>
        <div>{(value || '').toString()}</div>
        <div>
          {options.map((option: RefData) => (
            <div
              key={option.code}
              className="item"
              onClick={() => onChange({ target: { value: option } })}
            >
              {option.text}
            </div>
          ))}
        </div>
      </div>
    )
  };
});

const renderComponent = (props: any, initValues: any = {}) => {
  return renderWithMockStore(
    <>
      <SelectActions {...props} />
      <div onClick={confirmFunc}>Confirm</div>
    </>,
    initValues
  );
};

const mockUseSelectElementMetadataForActionCardCompromiseEvent = jest.spyOn(
  workflowSetup,
  'useSelectElementMetadataForActionCardCompromiseEvent'
);
describe('Name of the group', () => {
  beforeEach(() => {
    const metadata: any = {
      question1: [
        { code: 'Single Entity', text: 'Single Entity' },
        { code: 'Separate Entity', text: 'Separate Entity' }
      ],
      question2SingleEntity: [{ code: 'code', text: 'text' }],
      question2SeparateEntity: [{ code: 'code', text: 'text' }],
      question4: [{ code: 'code', text: 'text' }],
      question3: [
        { code: 'code', text: 'text' },
        { code: 'code1', text: 'text1' }
      ]
    };
    mockUseSelectElementMetadataForActionCardCompromiseEvent.mockReturnValue(
      metadata
    );
  });
  it('Should render SelectActions ', async () => {
    const props = {
      formValues: {
        isValid: true,
        action: {
          radio: 'Single Entity',
          question2: {
            code: 'code',
            text: 'text'
          }
        }
      },
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = await renderComponent(props, {});

    expect(wrapper.getByText('txt_action_card_compromise_event_title'))
      .toBeInTheDocument;
  });
});

describe('SelectActions', () => {
  beforeEach(() => {
    const metadata: any = {
      question1: [
        { code: 'Single Entity', text: 'Single Entity' },
        { code: 'Separate Entity', text: 'Separate Entity' }
      ],
      question2SingleEntity: [{ code: 'code', text: 'text' }],
      question2SeparateEntity: [{ code: 'code', text: 'text' }],
      question4: [{ code: 'code', text: 'text' }],
      question3: [
        { code: 'code', text: 'text' },
        { code: 'code1', text: 'text1' }
      ]
    };
    mockUseSelectElementMetadataForActionCardCompromiseEvent.mockReturnValue(
      metadata
    );
  });
  it('Should render SelectActions ', async () => {
    const props = {
      formValues: {},
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = await renderComponent(props, {});

    expect(wrapper.getByText('txt_action_card_compromise_event_title'))
      .toBeInTheDocument;
  });
  it('Should render SelectActions with question 1 ', async () => {
    const props = {
      formValues: {
        action: {
          radio: 'Single Entity',
          question2: {
            code: 'Account Transfer using the AT, Account Transfers transaction.',
            text: 'Account Transfer using the AT, Account Transfers transaction.'
          }
        }
      },
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = await renderComponent(props, {});

    expect(wrapper.getByText('txt_action_card_compromise_event_title'))
      .toBeInTheDocument;
  });

  it('Should render SelectActions with custom action ', async () => {
    const props = {
      formValues: {
        isValid: true,
        action: {
          radio: 'Single Entity',
          question2: {
            code: 'Account Transfer using the AT, Account Transfers transaction.',
            text: 'Account Transfer using the AT, Account Transfers transaction.'
          }
        }
      },
      dependencies: {
        files: [{ status: STATUS.valid, idx: '1' }],
        optionalParameters: { question3: 'YES' }
      },
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = await renderComponent(props, {});

    expect(wrapper.getByText('txt_action_card_compromise_event_title'))
      .toBeInTheDocument;
  });
  it('Should render SelectActions with custom action ', async () => {
    const props = {
      formValues: {
        action: {
          radio: 'Separate Entity',
          question1: [
            { code: 'code', text: 'text' },
            { code: 'code1', text: 'text1' }
          ],
          question2:
            'PIID Transfer using the PIT, Presentation Instrument Transfer transaction.',
          question3: [
            { code: 'code', text: 'text' },
            { code: 'code1', text: 'text1' }
          ],
          question4: [
            { code: 'code', text: 'text' },
            { code: 'code1', text: 'text1' }
          ]
        }
      },
      dependencies: { files: [{ status: STATUS.valid, idx: '1' }] },
      clearFormNames: ['text', 'text2'],
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = await renderComponent(props, {});

    expect(wrapper.getByText('txt_action_card_compromise_event_title'))
      .toBeInTheDocument;

    wrapper.rerender(
      <SelectActions
        formValues={{
          action: {
            radio: 'Separate Entity',
            question1: [
              { code: 'code', text: 'text' },
              { code: 'code1', text: 'text1' }
            ],
            question2: 'test.',
            question3:
              'PIID Transfer using the PIT, Presentation Instrument Transfer transaction.',
            question4: [
              { code: 'code', text: 'text' },
              { code: 'code1', text: 'text1' }
            ]
          }
        }}
        dependencies={{ files: [{ status: STATUS.valid, idx: '1' }] }}
        clearFormNames={['text', 'text2']}
        setFormValues={jest.fn()}
        clearFormValues={jest.fn()}
      />
    );
  });
  it('Should render SelectActions with custom action ', async () => {
    const props = {
      formValues: {
        action: {
          radio: 'Separate Entity',
          question1: [
            { code: 'code', text: 'text' },
            { code: 'code1', text: 'text1' }
          ],
          question2: {
            code: 'PIID Transfer using the PIT, Presentation Instrument Transfer transaction.',
            text: 'PIID Transfer using the PIT, Presentation Instrument Transfer transaction.'
          },
          question3: 'YES',
          question4: [
            { code: 'code', text: 'text' },
            { code: 'code1', text: 'text1' }
          ]
        }
      },
      dependencies: {
        files: [{ status: STATUS.valid, idx: '1' }],
        optionalParameters: { question3: 'YES' }
      },
      clearFormNames: ['text', 'text2'],
      clearFormValues: jest.fn(),
      setFormValues: jest.fn()
    };
    const wrapper = await renderComponent(props, {});

    expect(wrapper.getByText('txt_action_card_compromise_event_title'))
      .toBeInTheDocument;
  });
  it('Should render SelectActions with custom action ', async () => {
    const props = {
      isValid: true,
      clearFormNames: ['text', 'text2'],
      dependencies: {
        files: [{ status: STATUS.valid, idx: '1' }],
        parameters: { question3: 'YES' },
        optionalParameters: { question3: 'YES' }
      },
      formValues: {
        action: {
          radio: 'Separate Entity',
          question1: [
            { code: 'code', text: 'text' },
            { code: 'code1', text: 'text1' }
          ],
          question2: {
            code: 'PIID Transfer using the PIT, Presentation Instrument Transfer transaction.',
            text: 'PIID Transfer using the PIT, Presentation Instrument Transfer transaction.'
          },
          question3: 'YES',
          question4: [
            { code: 'code', text: 'text' },
            { code: 'code1', text: 'text1' }
          ]
        }
      },
      clearFormValues: jest.fn(),
      setFormValues: jest.fn()
    };
    const wrapper = await renderComponent(props, {});

    expect(wrapper.getByText('txt_action_card_compromise_event_title'))
      .toBeInTheDocument;
  });
  it('Should render SelectActions with custom action ', async () => {
    const props = {
      isValid: true,
      clearFormNames: ['text', 'text2'],
      dependencies: {
        files: [{ status: STATUS.valid, idx: '1' }],
        parameters: { question3: 'YES' },
        optionalParameters: { question3: 'YES' }
      },
      formValues: {
        action: {
          radio: 'Separate Entity',
          question1: [
            { code: 'code', text: 'text' },
            { code: 'code1', text: 'text1' }
          ],
          question2: {
            code: 'PIID',
            text: 'PIID'
          },
          question3: 'YES',
          question4: [
            { code: 'code', text: 'text' },
            { code: 'code1', text: 'text1' }
          ]
        }
      },
      clearFormValues: jest.fn(),
      setFormValues: jest.fn()
    };
    const wrapper = await renderComponent(props, {});

    expect(wrapper.getByText('txt_action_card_compromise_event_title'))
      .toBeInTheDocument;
  });

  it('Should render SelectActions with onChange radio ', async () => {
    const props = {
      isValid: true,
      clearFormNames: ['text', 'text2'],
      dependencies: {
        files: [{ status: STATUS.valid, idx: '1' }],
        parameters: { question3: 'YES' },
        optionalParameters: { question3: 'YES' }
      },
      formValues: {
        action: {
          radio: 'Separate Entity',
          question1: [
            { code: 'code', text: 'text' },
            { code: 'code1', text: 'text1' }
          ],
          question2: {
            code: 'PIID',
            text: 'PIID'
          },
          question3: 'YES',
          question4: [
            { code: 'code', text: 'text' },
            { code: 'code1', text: 'text1' }
          ]
        }
      },
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = await renderComponent(props, {});

    const text = wrapper.getByText('Single Entity');

    userEvent.click(text);

    expect(wrapper.getByText('txt_action_card_compromise_event_title'))
      .toBeInTheDocument;
  });
});

describe('Name of the group', () => {
  beforeEach(() => {
    const metadata: any = {
      question1: [
        { code: 'Single Entity', text: 'Single Entity' },
        { code: 'Separate Entity', text: 'Separate Entity' }
      ],
      question2SingleEntity: [
        {
          code: 'Report Stolen Accounts using the SL, Lost/Stolen Report transaction.',
          text: 'Report Stolen Accounts using the SL, Lost/Stolen Report transaction.'
        },
        {
          code: 'Account Transfer using the AT, Account Transfers transaction.',
          text: 'Account Transfer using the AT, Account Transfers transaction.'
        }
      ],
      question2SeparateEntity: [
        {
          code: 'Report Lost PIIDs using the SLP, Lost/Stolen Report transaction.',
          text: 'Report Lost PIIDs using the SLP, Lost/Stolen Report transaction.'
        },
        {
          code: 'PIID Transfer using the PIT, Presentation Instrument Transfer transaction.',
          text: 'PIID Transfer using the PIT, Presentation Instrument Transfer transaction.'
        }
      ],
      question4: [
        { code: 'Yes', text: 'Yes' },
        { code: 'No', text: 'No' }
      ],
      question3: [
        {
          code: 'Visa exception file using the WC, Visa Warning Bulletins transaction.',
          text: 'Visa exception file using the WC, Visa Warning Bulletins transaction.'
        },
        {
          code: 'MasterCard negative file using the WMA, Add Cardholder Account transaction.',
          text: 'MasterCard negative file using the WMA, Add Cardholder Account transaction.'
        },
        {
          code: 'MasterCard warning bulletin using the WB, MasterCard Warning Bulletins transaction.',
          text: 'MasterCard warning bulletin using the WB, MasterCard Warning Bulletins transaction.'
        }
      ]
    };
    mockUseSelectElementMetadataForActionCardCompromiseEvent.mockReturnValue(
      metadata
    );
  });
  it('Should render SelectActions', async () => {
    const props = {
      isValid: false,
      formValues: {
        action: {
          radio: 'Single Entity',
          question2: {
            code: ''
          }
        }
      },
      dependencies: {
        files: [{ status: STATUS.valid, idx: '1' }],
        parameters: { question3: 'YES' },
        optionalParameters: { question3: 'YES' }
      },
      clearFormNames: ['UploadFile', 'ActionCardCompromiseEvent'],
      clearFormValues: jest.fn(),
      setFormValues: jest.fn()
    };

    const wrapper = await renderComponent(props, {});

    const radio = wrapper.getByText('Single Entity');

    const dropdown1 = wrapper.getByText(
      'Account Transfer using the AT, Account Transfers transaction.'
    );
    const dropdown2 = wrapper.getByText(
      'MasterCard warning bulletin using the WB, MasterCard Warning Bulletins transaction.'
    );
    const confirmTxt = wrapper.getByText('Confirm');

    act(() => {
      userEvent.click(radio);
      userEvent.click(dropdown1);
      userEvent.click(dropdown2);
      userEvent.click(confirmTxt);
    });
    wrapper.rerender(<SelectActions {...(props as any)} />);
    wrapper.debug();
    expect(wrapper.getByText('txt_action_card_compromise_event_title'))
      .toBeInTheDocument;
  });
  it('Should render SelectActions', async () => {
    const props = {
      isValid: true,
      clearFormNames: ['text', 'text2'],
      dependencies: {
        files: [{ status: STATUS.valid, idx: '1' }],
        parameters: { question3: 'YES' },
        optionalParameters: { question3: 'YES' }
      },
      formValues: {
        action: {
          radio: 'Single Entity',
          question1: [
            { code: 'code', text: 'text' },
            { code: 'code1', text: 'text1' }
          ],
          question2: {
            code: 'Report Stolen Accounts using the SL, Lost/Stolen Report transaction.',
            text: 'Report Stolen Accounts using the SL, Lost/Stolen Report transaction.'
          },
          question3: {
            code: 'MasterCard warning bulletin using the WB, MasterCard Warning Bulletins transaction.',
            text: 'MasterCard warning bulletin using the WB, MasterCard Warning Bulletins transaction.'
          }
        }
      },
      clearFormValues: jest.fn(),
      setFormValues: jest.fn()
    };

    const wrapper = await renderComponent(props, {});

    const radio = wrapper.getByRole('radio', { name: 'Separate Entity' });

    const dropdown1 = wrapper.getByText(
      'Report Stolen Accounts using the SL, Lost/Stolen Report transaction.'
    );
    const dropdown2 = wrapper.getByText(
      'MasterCard warning bulletin using the WB, MasterCard Warning Bulletins transaction.'
    );
    const confirmTxt = wrapper.getByText('Confirm');

    act(() => {
      userEvent.click(radio);
      userEvent.click(dropdown1);
      userEvent.click(dropdown2);
      userEvent.click(confirmTxt);
    });

    expect(wrapper.getByText('txt_action_card_compromise_event_title'))
      .toBeInTheDocument;
  });
  it('Should render SelectActions', async () => {
    const props = {
      isValid: true,
      clearFormNames: ['text', 'text2'],
      dependencies: {
        files: [{ status: STATUS.valid, idx: '1' }],
        parameters: { question3: 'YES' },
        optionalParameters: { question3: 'YES' }
      },
      formValues: {
        action: {
          radio: 'Separate Entity',
          question1: [
            { code: 'code', text: 'text' },
            { code: 'code1', text: 'text1' }
          ],
          question2: {
            code: 'Report Stolen Accounts using the SL, Lost/Stolen Report transaction.',
            text: 'Report Stolen Accounts using the SL, Lost/Stolen Report transaction.'
          },
          question3: {
            code: 'MasterCard warning bulletin using the WB, MasterCard Warning Bulletins transaction.',
            text: 'MasterCard warning bulletin using the WB, MasterCard Warning Bulletins transaction.'
          }
        }
      },
      clearFormValues: jest.fn(),
      setFormValues: jest.fn()
    };

    const wrapper = await renderComponent(props, {});

    const radio = wrapper.getByRole('radio', { name: 'Single Entity' });

    const dropdown1 = wrapper.getByText(
      'PIID Transfer using the PIT, Presentation Instrument Transfer transaction.'
    );
    const dropdown2 = wrapper.getByText(
      'MasterCard warning bulletin using the WB, MasterCard Warning Bulletins transaction.'
    );
    const confirmTxt = wrapper.getByText('Confirm');

    act(() => {
      userEvent.click(radio);
      userEvent.click(dropdown1);
      userEvent.click(dropdown2);
      userEvent.click(confirmTxt);
    });

    expect(wrapper.getByText('txt_action_card_compromise_event_title'))
      .toBeInTheDocument;
  });
  it('Should render SelectActions', async () => {
    const props = {
      isValid: true,
      clearFormNames: ['text', 'text2'],
      dependencies: {
        files: [{ status: STATUS.valid, idx: '1' }],
        parameters: { question3: 'YES' },
        optionalParameters: { question3: 'YES' }
      },
      formValues: {
        action: {
          radio: 'Separate Entity',
          question1: [
            { code: 'code', text: 'text' },
            { code: 'code1', text: 'text1' }
          ],
          question2: {
            code: 'PIID Transfer using the PIT, Presentation Instrument Transfer transaction.',
            text: 'PIID Transfer using the PIT, Presentation Instrument Transfer transaction.'
          }
        }
      },
      clearFormValues: jest.fn(),
      setFormValues: jest.fn()
    };

    const wrapper = await renderComponent(props, {});

    wrapper.rerender(
      <>
        <SelectActions
          {...({
            ...props,
            formValues: { action: { radio: 'Single Entity' } }
          } as any)}
        />
        <div onClick={confirmFunc}>Confirm</div>
      </>
    );

    const confirmTxt = wrapper.getByText('Confirm');

    act(() => {
      userEvent.click(confirmTxt);
    });

    expect(wrapper.getByText('txt_action_card_compromise_event_title'))
      .toBeInTheDocument;
  });
  it('Should render SelectActions', async () => {
    const props = {
      isValid: true,
      clearFormNames: ['text', 'text2'],
      dependencies: {
        files: [{ status: STATUS.valid, idx: '1' }],
        parameters: { question3: 'YES' },
        optionalParameters: { question3: 'YES' }
      },
      formValues: {
        action: {
          radio: 'Single Entity',
          question1: [
            { code: 'code', text: 'text' },
            { code: 'code1', text: 'text1' }
          ],
          question2: {
            code: 'Account Transfer using the AT, Account Transfers transaction.',
            text: 'Account Transfer using the AT, Account Transfers transaction.'
          }
        }
      },
      clearFormValues: jest.fn(),
      setFormValues: jest.fn()
    };

    const wrapper = await renderComponent(props, {});

    wrapper.rerender(
      <>
        <SelectActions
          {...({
            ...props,
            formValues: { action: { radio: 'Single Entity' } }
          } as any)}
        />
        <div onClick={confirmFunc}>Confirm</div>
      </>
    );

    const confirmTxt = wrapper.getByText('Confirm');

    act(() => {
      userEvent.click(confirmTxt);
    });

    expect(wrapper.getByText('txt_action_card_compromise_event_title'))
      .toBeInTheDocument;
  });
});
