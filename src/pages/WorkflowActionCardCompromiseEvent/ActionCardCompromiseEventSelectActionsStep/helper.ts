import { isEmpty } from 'app/_libraries/_dls/lodash';
import { IActionCard } from '.';
import { generateActionCardCompromiseTitle } from '../ActionCardCompromiseEventStep/helper';

export enum ACTION_CARD_COMPROMISE_EVENT_RADIO_ENUM {
  SINGLE_ENTITY = 'single_entity',
  SEPARATE_ENTITY = 'separate_entity'
}

export enum ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME {
  SINGLE_ENTITY = 'Single Entity',
  SEPARATE_ENTITY = 'Separate Entity'
}

export const generateActionCardCompromiseQuestion3 = (data: MagicKeyValue) => {
  let title = '';

  switch (data.question3) {
    case 'Visa exception file using the WC, Visa Warning Bulletins transaction.':
      title = ' - WC, Visa Warning Bulletins';
      break;
    case 'MasterCard negative file using the WMA, Add Cardholder Account transaction.':
      title = ' - WMA, Add Cardholder Account';
      break;
    case 'MasterCard warning bulletin using the WB, MasterCard Warning Bulletins transaction.':
      title = ' - WB, MasterCard Warning Bulletins';
      break;
    case 'Yes':
      title =
        data.radio === ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SINGLE_ENTITY
          ? ' - Auto Account Number Assignment'
          : ' - Auto PIID Assignment';
      break;
    case 'No':
      title =
        data.radio === ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SINGLE_ENTITY
          ? ' - No Auto Account Number Assignment'
          : ' - No Auto PIID Assignment';
      break;
  }

  return title;
};

export const generateRadioTxt = (data: string) => {
  let radioTxt = '';
  switch (data) {
    case ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SINGLE_ENTITY:
      radioTxt = `${ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SINGLE_ENTITY} - `;
      break;

    case ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SEPARATE_ENTITY:
      radioTxt = `${ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SEPARATE_ENTITY} - `;
      break;
  }

  return radioTxt;
};

export const parseContent = (data: IActionCard) => {
  if (isEmpty(data)) return '';

  const radioTxt = generateRadioTxt(data.radio!);

  const question2Text = generateActionCardCompromiseTitle(data.question2!);

  const question3Text = generateActionCardCompromiseQuestion3(data);

  return `${radioTxt}${question2Text}${question3Text}`;
};
