import { MethodFieldParameterEnum as ParameterEnum } from 'app/constants/enums';
import { getWorkflowSetupStepStatus } from 'app/helpers';
import { isEmpty } from 'lodash';
import { ACCESelectActionsValue } from '.';
import { ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME } from './helper';

const parseFormValues: WorkflowSetupStepFormDataFunc<ACCESelectActionsValue> = (
  data,
  id
) => {
  const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);
  if (!stepInfo) return;

  const parameters = ((data?.data as any)?.configurations?.parameters ||
    []) as WorkflowSetupElementParams[];

  const radio = parameters.find(
    p => p.name === ParameterEnum.ActionCardCompromiseEventQuestion1
  )?.value;
  const question2 =
    parameters.find(
      p =>
        p.name === ParameterEnum.ActionCardCompromiseEventQuestion2SingleEntity
    )?.value ||
    parameters.find(
      p =>
        p.name ===
        ParameterEnum.ActionCardCompromiseEventQuestion2SeparateEntity
    )?.value;
  const question3 =
    parameters.find(
      p => p.name === ParameterEnum.ActionCardCompromiseEventQuestion3
    )?.value ||
    parameters.find(
      p => p.name === ParameterEnum.ActionCardCompromiseEventQuestion4
    )?.value;

  const isYesNoQuestion =
    (radio === ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SINGLE_ENTITY &&
      question2 ===
        'Account Transfer using the AT, Account Transfers transaction.') ||
    (radio === ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SEPARATE_ENTITY &&
      question2 ===
        'PIID Transfer using the PIT, Presentation Instrument Transfer transaction.');

  const isValid =
    !isEmpty(radio) &&
    !isEmpty(question2) &&
    (!isYesNoQuestion || !isEmpty(question3));

  return {
    ...getWorkflowSetupStepStatus(stepInfo),
    isValid,
    action: { radio, question2, question3 }
  };
};

export default parseFormValues;
