import { InlineMessage, useTranslation } from 'app/_libraries/_dls';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React from 'react';
import parseFormValues from './parseFormValues';
import SelectActions from './SelectActions';
import stepValueFunc from './stepValueFunc';
import Summary from './Summary';

export interface IActionCard {
  radio?: string;
  question2?: string;
  question3?: string;
}
export interface ACCESelectActionsValue {
  action?: IActionCard;
  isValid?: boolean;
}

const ActionCardCompromiseEventSelectActions: React.FC<
  WorkflowSetupProps<ACCESelectActionsValue> & { clearFormNames: string[] }
> = props => {
  const { t } = useTranslation();
  const { isStuck } = props;

  return (
    <div className="mt-24">
      {isStuck && (
        <InlineMessage className="mb-24" variant="danger" withIcon>
          {t('txt_step_stuck_move_forward_message')}
        </InlineMessage>
      )}
      <h5>{t('txt_action_card_compromise_event_title')}</h5>
      <div className="mt-16">
        <SelectActions {...props} />
      </div>
    </div>
  );
};

const ExtraStaticACCESelectActionsStep =
  ActionCardCompromiseEventSelectActions as WorkflowSetupStaticProp<ACCESelectActionsValue>;

ExtraStaticACCESelectActionsStep.summaryComponent = Summary;
ExtraStaticACCESelectActionsStep.defaultValues = {
  isValid: false,
  action: { question2: undefined, question3: undefined, radio: undefined }
};

ExtraStaticACCESelectActionsStep.stepValue = stepValueFunc;

ExtraStaticACCESelectActionsStep.parseFormValues = parseFormValues;

export default ExtraStaticACCESelectActionsStep;
