import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import * as CommonRedux from 'pages/_commons/redux/Common/select-hooks';
import React from 'react';
import Summary from './Summary';
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const mockUseSelectWindowDimension = jest.spyOn(
  CommonRedux,
  'useSelectWindowDimension'
);

describe('Select Action Step 3', () => {
  const mockUseSelectWindowDimensionImplementation = (width: number) => {
    mockUseSelectWindowDimension.mockImplementation(() => ({ width } as any));
  };

  beforeEach(() => {
    mockUseSelectWindowDimensionImplementation(90);
  });
  afterEach(() => {
    mockUseSelectWindowDimension.mockClear();
  });
  it('Should render a grid with content', () => {
    const props = {
      formValues: {
        action: {
          question2: 'test'
        }
      },
      onEditStep: jest.fn()
    };

    const wrapper = render(<Summary {...props} />);
    expect(
      wrapper.getByText('txt_action_card_selected_summary_2')
    ).toBeInTheDocument();
  });

  it('Should render a grid with Account Transfer using the AT, Account Transfers transaction.', () => {
    const props = {
      formValues: {
        action: {
          question2:
            'Account Transfer using the AT, Account Transfers transaction.'
        }
      },
      onEditStep: jest.fn()
    };

    const wrapper = render(<Summary {...props} />);
    expect(
      wrapper.getByText('txt_action_card_selected_summary_2')
    ).toBeInTheDocument();
  });
  it('Should render and click edit', () => {
    const props = {
      formValues: {
        action: {
          question2:
            'PIID Transfer using the PIT, Presentation Instrument Transfer transaction.'
        }
      },
      onEditStep: jest.fn(),
      stepId: '1',
      selectedStep: '1'
    };

    const wrapper = render(<Summary {...props} />);
    const text = wrapper.getByText('txt_edit');
    userEvent.click(text);
    expect(
      wrapper.getByText('txt_action_card_selected_summary_2')
    ).toBeInTheDocument();
  });
});
