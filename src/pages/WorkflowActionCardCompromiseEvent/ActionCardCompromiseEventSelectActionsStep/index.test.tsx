import { renderWithMockStore } from 'app/utils';
import React from 'react';
import SelectActionStep from '.';

jest.mock('./SelectActions', () => ({
  __esModule: true,
  default: () => <div>SelectActions</div>
}));

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

const renderComponent = (props: any, initValues: any = {}) => {
  return renderWithMockStore(<SelectActionStep {...props} />, initValues);
};

describe('SelectActionStep', () => {
  it('Should render SelectActionStep contains Completed', async () => {
    const props = {
      isStuck: true
    };
    const wrapper = await renderComponent(props, {});

    expect(wrapper.getByText('txt_action_card_compromise_event_title'))
      .toBeInTheDocument;
  });
});
