import { render } from '@testing-library/react';
import { ACCESelectActionsValue } from '.';
import stepValueFunc from './stepValueFunc';

const t = (value: string) => {
  return value;
};

describe('WorkflowActionCardCompromiseEvent > SelectActionCardCompromiseEventStep > stepValueFunc', () => {
  it('Should Return with XXX Transaction Selected', () => {
    const stepsForm = {
      action: {
        radio: 'Single Entity',
        question2:
          'Report Stolen Accounts using the SL, Lost/Stolen Report transaction.',
        question3:
          'Visa exception file using the WC, Visa Warning Bulletins transaction.'
      }
    } as ACCESelectActionsValue;

    const response: React.ReactNode = stepValueFunc({ stepsForm, t });
    const wrapper = render(response as any);
    expect(wrapper.queryByText('Single Entity')).toBeInTheDocument;
  });

  it('Should Return with content', () => {
    const stepsForm = {
      action: {
        radio: 'Single Entity',
        question2:
          'Report Stolen Accounts using the SL, Lost/Stolen Report transaction.',
        question3:
          'Visa exception file using the WC, Visa Warning Bulletins transaction.'
      }
    } as ACCESelectActionsValue;

    const response: React.ReactNode = stepValueFunc({ stepsForm, t });
    const wrapper = render(response as any);
    expect(
      wrapper.queryByText(
        'Report Stolen Accounts using the SL, Lost/Stolen Report transaction.'
      )
    ).toBeInTheDocument;
  });

  it('Should Return with empty string', () => {
    const stepsForm = {} as ACCESelectActionsValue;

    const response = stepValueFunc({ stepsForm, t });
    expect(response).toEqual('');
  });

  it('should return changeName with all transaction selected', () => {
    const stepsForm = {
      action: {
        radio: 'Single Entity',
        question2:
          'Report Stolen Accounts using the SL, Lost/Stolen Report transaction.',
        question3:
          'Visa exception file using the WC, Visa Warning Bulletins transaction.'
      }
    };

    const result = stepValueFunc({ stepsForm, t });
    expect(result).toBeTruthy();
  });
});
