import {
  ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME,
  generateActionCardCompromiseQuestion3,
  generateRadioTxt,
  parseContent
} from './helper';

describe('helper', () => {
  it('generateActionCardCompromiseQuestion3', () => {
    generateActionCardCompromiseQuestion3({
      question3:
        'Visa exception file using the WC, Visa Warning Bulletins transaction.'
    });
    generateActionCardCompromiseQuestion3({
      question3:
        'MasterCard negative file using the WMA, Add Cardholder Account transaction.'
    });
    generateActionCardCompromiseQuestion3({
      question3:
        'MasterCard warning bulletin using the WB, MasterCard Warning Bulletins transaction.'
    });
    generateActionCardCompromiseQuestion3({
      question3: 'Yes'
    });
    generateActionCardCompromiseQuestion3({
      question3: 'Yes',
      radio: ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SINGLE_ENTITY
    });
    generateActionCardCompromiseQuestion3({
      question3: 'No'
    });
    generateActionCardCompromiseQuestion3({
      question3: 'No',
      radio: ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SINGLE_ENTITY
    });
  });

  it('generateRadioTxt', () => {
    generateRadioTxt(ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SINGLE_ENTITY);
    generateRadioTxt(ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SEPARATE_ENTITY);
  });

  it('parseContent', () => {
    parseContent({});
    parseContent({
      radio: '123',
      question2: '123',
      question3: '123'
    });
  });
});
