import { isEmpty } from 'app/_libraries/_dls/lodash';
import { ACCESelectActionsValue } from '.';
import { parseContent } from './helper';

const stepValueFunc: WorkflowSetupStepValueFunc<ACCESelectActionsValue> = ({
  stepsForm: formValues,
  t
}) => {
  if (isEmpty(formValues?.action)) return '';

  const content = parseContent(formValues.action!);

  return content;
};

export default stepValueFunc;
