import { Button, Icon, Tooltip, useTranslation } from 'app/_libraries/_dls';
import { isFunction } from 'app/_libraries/_dls/lodash';
import React from 'react';
import { ACCESelectActionsValue } from '.';

const Summary: React.FC<WorkflowSetupSummaryProps<ACCESelectActionsValue>> = ({
  formValues,
  onEditStep
}) => {
  const values = formValues?.action;
  const { t } = useTranslation();

  const isNotListingOption =
    values?.question2 ===
      'Account Transfer using the AT, Account Transfers transaction.' ||
    values?.question2 ===
      'PIID Transfer using the PIT, Presentation Instrument Transfer transaction.';

  const handleEdit = () => {
    isFunction(onEditStep) && onEditStep();
  };

  return (
    <div className="position-relative">
      <div className="absolute-top-right mt-n26 mr-n8">
        <Button variant="outline-primary" size="sm" onClick={handleEdit}>
          {t('txt_edit')}
        </Button>
      </div>

      <div className="pt-16">
        <div className="group-card">
          {/* Row 1*/}
          <div className="group-card-item cursor-default">
            <div className="row">
              <div className="col-12 col-lg-8">
                <p className="title">
                  {t('txt_action_card_selected_summary_1')}
                </p>
              </div>
              <div className="col-12 col-lg-4 mt-8 mt-lg-0">
                <div className="group-control justify-content-lg-end text-right">
                  <div className="group-control-item">{values?.radio}</div>
                </div>
              </div>
            </div>
          </div>

          {/* Row 2*/}
          <div className="group-card-item cursor-default">
            <div className="row">
              <div className="col-12 col-lg-8">
                <p className="title">
                  {t('txt_action_card_selected_summary_2')}
                </p>
              </div>
              <div className="col-12 col-lg-4 mt-8 mt-lg-0">
                <div className="group-control justify-content-lg-end text-right">
                  <div className="group-control-item">{values?.question2}</div>
                </div>
              </div>
            </div>
          </div>

          {/* Row 3 */}
          <div className="group-card-item cursor-default">
            <div className="row">
              <div className="col-12 col-lg-8">
                {!isNotListingOption ? (
                  <p className="title">
                    {t('txt_action_card_selected_summary_3_2')}
                  </p>
                ) : (
                  <div className="mt-n10 mb-n16">
                    <p className="title">
                      {t('txt_action_card_selected_summary_3_1')}
                      <Tooltip
                        element={
                          <>
                            <p>{t('txt_action_card_compromise_tooltip_1')}</p>
                            <p className="mt-16">
                              {t('txt_action_card_compromise_tooltip_2')}
                            </p>
                          </>
                        }
                        displayOnClick={false}
                        placement="left-start"
                        triggerClassName="align-sub ml-8 py-12 cursor-default"
                      >
                        <Icon
                          name="information"
                          size="5x"
                          className="color-grey-l16"
                        />
                      </Tooltip>
                    </p>
                  </div>
                )}
              </div>
              <div className="col-12 col-lg-4 mt-8 mt-lg-0">
                <div className="group-control justify-content-lg-end text-right">
                  <div className="group-control-item">{values?.question3}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Summary;
