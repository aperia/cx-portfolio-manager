import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React from 'react';
import ConfigureParameters, {
  ConfigureParametersFormValue
} from '../ConfigureParameters';
import { TableType } from '../ConfigureParameters/constant';
import parseFormValues from '../ConfigureParameters/parseFormValues';
import Summary from '../ConfigureParameters/Summary';

const ConfigureParametersAQ: React.FC<
  WorkflowSetupProps<ConfigureParametersFormValue>
> = props => {
  return (
    <div>
      <ConfigureParameters {...props} tableType={TableType.tlpaq} />
    </div>
  );
};

const ExtraStaticConfigureParametersAQ =
  ConfigureParametersAQ as WorkflowSetupStaticProp;

ExtraStaticConfigureParametersAQ.summaryComponent = Summary;
ExtraStaticConfigureParametersAQ.defaultValues = {
  isValid: false,
  tables: [],
  tableType: TableType.tlpaq
};
ExtraStaticConfigureParametersAQ.parseFormValues = parseFormValues;

export default ExtraStaticConfigureParametersAQ;
