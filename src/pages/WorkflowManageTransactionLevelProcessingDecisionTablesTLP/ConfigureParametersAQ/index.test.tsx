import { render, RenderResult } from '@testing-library/react';
import 'app/utils/_mockComponent/mockUseTranslation';
import React from 'react';
import { TableType } from '../ConfigureParameters/constant';
import { ConfigTableItem } from '../ConfigureParameters/type';
import ConfigureParametersAQ from './';

jest.mock('../ConfigureParameters', () => ({
  __esModule: true,
  default: () => <div>ConfigureParameters_Component</div>
}));

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <ConfigureParametersAQ {...props} />
    </div>
  );
};

describe('WorkflowManageTransactionLevelProcessingDecisionTablesTLP > ConfigureParametersAQ > index', () => {
  const defaultProps = {
    isValid: true,
    tableType: TableType.tlpaq,
    tables: [] as ConfigTableItem[]
  };

  it('It should render ConfigureParametersAQ component', () => {
    const wrapper = renderComponent(defaultProps);
    expect(
      wrapper.getByText('ConfigureParameters_Component')
    ).toBeInTheDocument();
  });
});
