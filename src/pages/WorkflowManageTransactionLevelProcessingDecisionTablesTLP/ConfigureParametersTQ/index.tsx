import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React from 'react';
import ConfigureParameters, {
  ConfigureParametersFormValue
} from '../ConfigureParameters';
import { TableType } from '../ConfigureParameters/constant';
import parseFormValues from '../ConfigureParameters/parseFormValues';
import Summary from '../ConfigureParameters/Summary';

const ConfigureParametersTQ: React.FC<
  WorkflowSetupProps<ConfigureParametersFormValue>
> = props => {
  return (
    <div>
      <ConfigureParameters {...props} tableType={TableType.tlptq} />
    </div>
  );
};

const ExtraStaticConfigureParametersTQ =
  ConfigureParametersTQ as WorkflowSetupStaticProp;

ExtraStaticConfigureParametersTQ.summaryComponent = Summary;

ExtraStaticConfigureParametersTQ.defaultValues = {
  isValid: false,
  tables: [],
  tableType: TableType.tlptq
};
ExtraStaticConfigureParametersTQ.parseFormValues = parseFormValues;

export default ExtraStaticConfigureParametersTQ;
