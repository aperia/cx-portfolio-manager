import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React from 'react';
import ConfigureParameters, {
  ConfigureParametersFormValue
} from '../ConfigureParameters';
import { TableType } from '../ConfigureParameters/constant';
import parseFormValues from '../ConfigureParameters/parseFormValues';
import Summary from '../ConfigureParameters/Summary';

const ConfigureParametersRQ: React.FC<
  WorkflowSetupProps<ConfigureParametersFormValue>
> = props => {
  return (
    <div>
      <ConfigureParameters {...props} tableType={TableType.tlprq} />
    </div>
  );
};

const ExtraStaticConfigureParametersRQ =
  ConfigureParametersRQ as WorkflowSetupStaticProp;

ExtraStaticConfigureParametersRQ.summaryComponent = Summary;
ExtraStaticConfigureParametersRQ.defaultValues = {
  isValid: false,
  tables: [],
  tableType: TableType.tlprq
};
ExtraStaticConfigureParametersRQ.parseFormValues = parseFormValues;

export default ExtraStaticConfigureParametersRQ;
