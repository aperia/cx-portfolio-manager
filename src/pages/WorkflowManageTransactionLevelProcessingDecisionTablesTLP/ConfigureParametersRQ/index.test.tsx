import { render, RenderResult } from '@testing-library/react';
import 'app/utils/_mockComponent/mockUseTranslation';
import React from 'react';
import { TableType } from '../ConfigureParameters/constant';
import { ConfigTableItem } from '../ConfigureParameters/type';
import ConfigureParametersRQ from './';

jest.mock('../ConfigureParameters', () => ({
  __esModule: true,
  default: () => <div>ConfigureParameters_Component</div>
}));

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <ConfigureParametersRQ {...props} />
    </div>
  );
};

describe('WorkflowManageTransactionLevelProcessingDecisionTablesTLP > ConfigureParametersRQ > index', () => {
  const defaultProps = {
    isValid: true,
    tableType: TableType.tlpaq,
    tables: [] as ConfigTableItem[]
  };

  it('It should render ConfigureParametersRQ component', () => {
    const wrapper = renderComponent(defaultProps);
    expect(
      wrapper.getByText('ConfigureParameters_Component')
    ).toBeInTheDocument();
  });
});
