import { fireEvent, render, RenderResult } from '@testing-library/react';
import React from 'react';
import { FormGettingStart } from '.';
import GettingStartSummary from './Summary';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <GettingStartSummary {...props} />
    </div>
  );
};

describe('WorkflowManageTransactionLevelProcessingDecisionTablesTLP > GettingStartStep > Summary', () => {
  it('Should render summary contains txt_config_manage_transaction_level_processing_decision_tables_tlp_config_tq', () => {
    const wrapper = renderComponent({
      formValues: {
        TQ: true
      }
    } as WorkflowSetupSummaryProps<FormGettingStart>);

    expect(
      wrapper.getByText(
        'txt_config_manage_transaction_level_processing_decision_tables_tlp_config_tq'
      )
    ).toBeInTheDocument();
  });

  it('Should click on edit', () => {
    const mockFn = jest.fn();
    const wrapper = renderComponent({
      formValues: {
        TQ: true
      },
      onEditStep: mockFn
    } as WorkflowSetupSummaryProps<FormGettingStart>);

    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(mockFn).toHaveBeenCalled();
  });
});
