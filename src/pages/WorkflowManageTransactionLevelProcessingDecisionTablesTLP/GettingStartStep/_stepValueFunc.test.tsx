import { FormGettingStart } from '.';
import stepValueFunc from './_stepValueFunc';

describe('WorkflowManageTransactionLevelProcessingDecisionTablesTLP > GettingStartStep > stepValueFunc', () => {
  const t = jest.fn().mockImplementation(v => v);
  beforeEach(() => {
    t.mockImplementation(v => v);
  });

  it('should return tq text', () => {
    const stepsForm: FormGettingStart = { TQ: true };

    const response = stepValueFunc({ stepsForm, t });
    expect(response).toEqual(
      'txt_config_manage_transaction_level_processing_decision_tables_tlp_config_tq'
    );
  });

  it('should return aq text', () => {
    const stepsForm: FormGettingStart = { AQ: true };

    const response = stepValueFunc({ stepsForm, t });
    expect(response).toEqual(
      'txt_config_manage_transaction_level_processing_decision_tables_tlp_config_aq'
    );
  });

  it('should return ca text', () => {
    const stepsForm: FormGettingStart = { CA: true };

    const response = stepValueFunc({ stepsForm, t });
    expect(response).toEqual(
      'txt_config_manage_transaction_level_processing_decision_tables_tlp_config_ca'
    );
  });

  it('should return rq text', () => {
    const stepsForm: FormGettingStart = { RQ: true };

    const response = stepValueFunc({ stepsForm, t });
    expect(response).toEqual(
      'txt_config_manage_transaction_level_processing_decision_tables_tlp_config_rq'
    );
  });

  it('should return all tables type text', () => {
    const stepsForm: FormGettingStart = {
      AQ: true,
      TQ: true,
      CA: true,
      RQ: true
    };

    const response = stepValueFunc({ stepsForm, t });
    expect(response).toEqual(
      'txt_manage_transaction_level_tlp_all_tables_type_selected'
    );
  });

  it('should return empty', () => {
    const stepsForm: any = undefined;

    const response = stepValueFunc({ stepsForm, t });
    expect(response).toEqual('');
  });
});
