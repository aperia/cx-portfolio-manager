import { ConfigTableOptionsEnum } from 'app/constants/enums';
import { WORKFLOW_SETUP } from 'app/constants/local-storage';
import {
  unsavedExistedWorkflowSetupDataOnStuckProps,
  unsavedExistedWorkflowSetupDataProps,
  unsavedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { useUnsavedChangeRegistry } from 'app/hooks';
import {
  Button,
  CheckBox,
  Icon,
  InlineMessage,
  TransDLS,
  useTranslation
} from 'app/_libraries/_dls';
import ContentExpand from 'pages/_commons/ContentExpand';
import OverviewModal from 'pages/_commons/OverviewModal';
import {
  actionsWorkflowSetup,
  useSelectElementMetadataManageTransactionTLP
} from 'pages/_commons/redux/WorkflowSetup';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';
import GettingStartSummary from './Summary';
import parseFormValues from './_parseFormValues';
import stepValueFunc from './_stepValueFunc';

export interface FormGettingStart {
  isValid?: boolean;
  CA?: boolean;
  AQ?: boolean;
  TQ?: boolean;
  RQ?: boolean;
  alreadyShown?: boolean;
}
export interface GettingStartProps
  extends WorkflowSetupProps<FormGettingStart> {
  AQFormId?: string;
  AQUploadFileFormId?: string;
  CAFormId?: string;
  CAUploadFileFormId?: string;
  RQFormId?: string;
  RQUploadFileFormId?: string;
  TQFormId?: string;
  TQUploadFileFormId?: string;
}
const GettingStartStep: React.FC<GettingStartProps> = ({
  title,
  stepId,
  selectedStep,
  savedAt,
  formValues,
  hasInstance,
  isStuck,

  AQFormId,
  AQUploadFileFormId,
  CAFormId,
  CAUploadFileFormId,
  RQFormId,
  RQUploadFileFormId,
  TQFormId,
  TQUploadFileFormId,

  setFormValues,
  clearFormValues
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;

  useEffect(() => {
    dispatch(
      actionsWorkflowSetup.getWorkflowMetadata([
        ConfigTableOptionsEnum.tlpTable
      ])
    );
  }, [dispatch]);

  const metadata = useSelectElementMetadataManageTransactionTLP();

  const { tlpTableConfigOptions } = metadata;
  const [initialValues, setInitialValues] = useState(formValues);
  const [showOverview, setShowOverview] = useState(
    formValues.alreadyShown
      ? false
      : localStorage.getItem(WORKFLOW_SETUP.SHOW_OVERVIEW_AGAIN) !== 'false'
  );

  const hasSelectedAnOption = useMemo(
    () =>
      !!formValues.TQ || !!formValues.CA || !!formValues.RQ || !!formValues.AQ,
    [formValues.TQ, formValues.CA, formValues.RQ, formValues.AQ]
  );

  useEffect(() => {
    const isValid = hasSelectedAnOption;
    if (formValues.isValid === isValid) return;

    keepRef.current.setFormValues({ isValid });
  }, [formValues, hasSelectedAnOption]);

  useEffect(() => {
    setInitialValues(formValues);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [savedAt]);

  useUnsavedChangeRegistry(
    {
      ...unsavedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_TRANSACTION_LEVEL_PROCESSING_DECISION_TABLES_TLP__GET_STARTED,
      priority: 1
    },
    [!hasInstance && hasSelectedAnOption]
  );

  const handleConfirmChangeDependencies = useCallback(() => {
    AQFormId && !formValues.AQ && clearFormValues(AQFormId);
    AQUploadFileFormId && !formValues.AQ && clearFormValues(AQUploadFileFormId);

    CAFormId && !formValues.CA && clearFormValues(CAFormId);
    CAUploadFileFormId && !formValues.CA && clearFormValues(CAUploadFileFormId);

    RQFormId && !formValues.RQ && clearFormValues(RQFormId);
    RQUploadFileFormId && !formValues.RQ && clearFormValues(RQUploadFileFormId);

    TQFormId && !formValues.TQ && clearFormValues(TQFormId);
    TQUploadFileFormId && !formValues.TQ && clearFormValues(TQUploadFileFormId);
  }, [
    clearFormValues,
    formValues.AQ,
    formValues.CA,
    formValues.RQ,
    formValues.TQ,
    AQFormId,
    AQUploadFileFormId,
    CAFormId,
    CAUploadFileFormId,
    RQFormId,
    RQUploadFileFormId,
    TQFormId,
    TQUploadFileFormId
  ]);

  const isUncheckAll = useMemo(
    () => !formValues.AQ && !formValues.CA && !formValues.RQ && !formValues.TQ,
    [formValues.AQ, formValues.CA, formValues.RQ, formValues.TQ]
  );
  const hasChanged = useMemo(
    () =>
      initialValues.AQ !== formValues.AQ ||
      initialValues.CA !== formValues.CA ||
      initialValues.RQ !== formValues.RQ ||
      initialValues.TQ !== formValues.TQ,
    [initialValues, formValues]
  );

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_TRANSACTION_LEVEL_PROCESSING_DECISION_TABLES_TLP__GET_STARTED__EXISTED_WORKFLOW,
      priority: 1,
      confirmFirst: true
    },
    [!isUncheckAll && !!hasInstance && hasChanged],
    handleConfirmChangeDependencies
  );
  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataOnStuckProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_TRANSACTION_LEVEL_PROCESSING_DECISION_TABLES_TLP__GET_STARTED__EXISTED_WORKFLOW__STUCK,
      priority: 1,
      confirmFirst: true
    },
    [isUncheckAll && !!hasInstance && hasChanged]
  );

  const handleChangeOption = (option: keyof FormGettingStart) => {
    const onChange = (e: any) => {
      setFormValues({ [option]: !formValues[option] });
    };

    return onChange;
  };

  return (
    <React.Fragment>
      <ContentExpand
        isSelectedStep={stepId === selectedStep}
        change={savedAt}
        instruction={
          <div className="pt-24 px-24">
            <div className="d-flex align-items-center justify-content-between">
              <h4>{title}</h4>
              <Button
                className="mr-n8"
                variant="outline-primary"
                size="sm"
                onClick={() => setShowOverview(true)}
              >
                {t('txt_view_overview')}
              </Button>
            </div>
            <div className="pb-24">
              {isStuck && (
                <InlineMessage className="mb-0 mt-24" variant="danger" withIcon>
                  {t('txt_step_stuck_move_forward_message')}
                </InlineMessage>
              )}

              <div className="row">
                <div className="col-6 mt-24">
                  <div className="bg-light-l20 rounded-lg p-16">
                    <div className="text-center">
                      <Icon
                        name="request"
                        size="12x"
                        className="color-grey-l16"
                      />
                    </div>
                    <p className="mt-8 color-grey">
                      {t(
                        'txt_config_manage_transaction_level_processing_decision_tables_tlp_desc_1'
                      )}
                    </p>
                    <p className="mt-16 fw-600">
                      {t(
                        'txt_config_manage_transaction_level_processing_decision_tables_tlp_desc_2'
                      )}
                    </p>
                    <p className="mt-8 color-grey">
                      {t(
                        'txt_config_manage_transaction_level_processing_decision_tables_tlp_desc_3'
                      )}
                    </p>

                    <ul className="list-dot color-grey">
                      <li>
                        {t(
                          'txt_config_manage_transaction_level_processing_decision_tables_tlp_desc_4'
                        )}
                      </li>
                      <li>
                        {t(
                          'txt_config_manage_transaction_level_processing_decision_tables_tlp_desc_5'
                        )}
                      </li>
                    </ul>

                    <p className="mt-8 color-grey">
                      {t(
                        'txt_config_manage_transaction_level_processing_decision_tables_tlp_desc_6'
                      )}
                    </p>
                    <ul className="list-dot color-grey">
                      <li>
                        {t(
                          'txt_config_manage_transaction_level_processing_decision_tables_tlp_desc_7'
                        )}
                      </li>
                      <li>
                        {t(
                          'txt_config_manage_transaction_level_processing_decision_tables_tlp_desc_8'
                        )}
                      </li>
                      <li>
                        {t(
                          'txt_config_manage_transaction_level_processing_decision_tables_tlp_desc_9'
                        )}
                      </li>
                      <li>
                        {t(
                          'txt_config_manage_transaction_level_processing_decision_tables_tlp_desc_10'
                        )}
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="col-6 mt-24 color-grey">
                  <p>
                    {t(
                      'txt_config_manage_adjustments_system_get_started_guides_title'
                    )}
                  </p>
                  <p className="mt-8">
                    <TransDLS keyTranslation="txt_config_manage_transaction_level_processing_decision_tables_tlp_guides_1">
                      <strong className="color-grey-d20" />
                    </TransDLS>
                  </p>
                  <p className="mt-8">
                    <TransDLS keyTranslation="txt_config_manage_transaction_level_processing_decision_tables_tlp_guides_2">
                      <strong className="color-grey-d20" />
                    </TransDLS>
                  </p>
                  <p className="mt-8">
                    <TransDLS keyTranslation="txt_config_manage_transaction_level_processing_decision_tables_tlp_guides_3">
                      <strong className="color-grey-d20" />
                    </TransDLS>
                  </p>
                  <p className="mt-8">
                    <TransDLS keyTranslation="txt_config_manage_transaction_level_processing_decision_tables_tlp_guides_4">
                      <strong className="color-grey-d20" />
                    </TransDLS>
                  </p>
                  <p className="mt-8">
                    <TransDLS keyTranslation="txt_config_manage_transaction_level_processing_decision_tables_tlp_guides_5">
                      <strong className="color-grey-d20" />
                    </TransDLS>
                  </p>
                </div>
              </div>
            </div>
          </div>
        }
      >
        <div className="p-24">
          <h5>
            {t(
              'txt_config_manage_transaction_level_processing_decision_tables_tlp_config_guide'
            )}
          </h5>
          <div className="list-cards list-cards--selectable list-cards--multi mt-16">
            {Array.isArray(tlpTableConfigOptions) &&
              tlpTableConfigOptions.map(o => (
                <label
                  key={o.code}
                  className="list-cards__item custom-control-root"
                  htmlFor={o.code}
                >
                  <span className="d-flex align-items-center">
                    <span className="pr-8">{t(o.text)}</span>
                  </span>
                  <CheckBox className="mr-n4">
                    <CheckBox.Input
                      id={o.code}
                      checked={formValues[o.code as keyof FormGettingStart]}
                      onChange={handleChangeOption(o.code as any)}
                    />
                  </CheckBox>
                </label>
              ))}
          </div>
        </div>
      </ContentExpand>
      {showOverview && (
        <OverviewModal
          id="overviewWorkflowSetup"
          show
          onClose={() => {
            setShowOverview(false);
            keepRef.current.setFormValues({ alreadyShown: true });
          }}
        />
      )}
    </React.Fragment>
  );
};

const ExtraStaticGettingStartStep =
  GettingStartStep as WorkflowSetupStaticProp<FormGettingStart>;

ExtraStaticGettingStartStep.summaryComponent = GettingStartSummary;
ExtraStaticGettingStartStep.stepValue = stepValueFunc;
ExtraStaticGettingStartStep.defaultValues = {
  isValid: false,
  CA: false,
  TQ: false,
  RQ: false,
  AQ: false
};
ExtraStaticGettingStartStep.parseFormValues = parseFormValues;

export default ExtraStaticGettingStartStep;
