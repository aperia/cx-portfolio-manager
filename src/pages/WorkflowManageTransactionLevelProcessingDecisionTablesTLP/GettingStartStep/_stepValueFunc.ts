import { FormGettingStart } from '.';

const stepValueFunc: WorkflowSetupStepValueFunc<FormGettingStart> = ({
  stepsForm: formValues,
  t
}) => {
  const { CA, AQ, RQ, TQ } = formValues || {};
  const isSelectedAll = TQ && AQ && CA && RQ;

  const options = [];
  CA &&
    options.push(
      t(
        'txt_config_manage_transaction_level_processing_decision_tables_tlp_config_ca'
      )
    );
  AQ &&
    options.push(
      t(
        'txt_config_manage_transaction_level_processing_decision_tables_tlp_config_aq'
      )
    );
  RQ &&
    options.push(
      t(
        'txt_config_manage_transaction_level_processing_decision_tables_tlp_config_rq'
      )
    );
  TQ &&
    options.push(
      t(
        'txt_config_manage_transaction_level_processing_decision_tables_tlp_config_tq'
      )
    );

  return isSelectedAll
    ? t('txt_manage_transaction_level_tlp_all_tables_type_selected')
    : options.join(', ');
};

export default stepValueFunc;
