import { fireEvent, render, RenderResult } from '@testing-library/react';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageTransactionLevelProcessingDecisionTLP';
import React from 'react';
import * as reactRedux from 'react-redux';
import GettingStartStep, { FormGettingStart } from './index';

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');

const useSelectElementMetadataManageTransactionTLP = jest.spyOn(
  WorkflowSetup,
  'useSelectElementMetadataManageTransactionTLP'
);

const MockTableConfigOptions = [
  {
    code: 'TQ',
    text: 'Transaction Qualification'
  },
  {
    code: 'AQ',
    text: 'Account Qualification'
  },
  {
    code: 'CA',
    text: 'Client Allocation'
  },
  {
    code: 'RQ',
    text: 'Retail Qualification'
  }
];

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

jest.mock('app/hooks/useUnsavedChangeRegistry', () => {
  return {
    __esModule: true,
    useUnsavedChangeRegistry: (options: any, changes: any, onConfirm: any) => {
      onConfirm && onConfirm();
    }
  };
});

jest.mock('pages/_commons/OverviewModal', () => ({
  __esModule: true,
  default: ({ onClose }: any) => {
    return (
      <div>
        <div>OverviewModal</div>
        <button onClick={onClose}>OverviewButtonCloseModal</button>
      </div>
    );
  }
}));

jest.mock('pages/_commons/ContentExpand', () => ({
  __esModule: true,
  default: ({ instruction, children }: any) => (
    <div>
      <div>ContentExpand</div>
      <div>{instruction}</div>
      <div>{children}</div>
    </div>
  )
}));

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <GettingStartStep {...props} />
    </div>
  );
};

describe('WorkflowManageTransactionLevelProcessingDecisionTablesTLP > GettingStartStep > Index', () => {
  const mockDispatch = jest.fn();
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
    useSelectElementMetadataManageTransactionTLP.mockReturnValue({
      searchCode: [],
      includeActivityOptions: [],
      posPromoValidationOptions: [],
      tlpTableConfigOptions: MockTableConfigOptions
    });
  });

  it('Should render without Overview modal > Open overview modal', () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: {},
      setFormValues: mockFn
    } as WorkflowSetupProps<FormGettingStart>;

    const wrapper = renderComponent(props);
    expect(wrapper.queryByText(/OverviewModal/)).toBeInTheDocument();
    wrapper.rerender(
      <div>
        <GettingStartStep
          {...props}
          stepId="1"
          formValues={{ alreadyShown: true, CA: true }}
        />
      </div>
    );

    fireEvent.click(wrapper.getByText('txt_view_overview'));
    expect(wrapper.queryByText(/OverviewModal/)).toBeInTheDocument();
  });

  it('Should render Overview modal > Close overview modal', () => {
    const props = {
      hasInstance: true,
      formValues: {
        CA: true,
        isValid: true
      },
      AQFormId: 'AQ',
      AQUploadFileFormId: 'AQ',
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    } as unknown as WorkflowSetupProps<FormGettingStart>;

    const wrapper = renderComponent(props);
    expect(wrapper.queryByText(/OverviewModal/)).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('OverviewButtonCloseModal'));
    expect(wrapper.queryByText(/OverviewModal/)).not.toBeInTheDocument();

    const props2 = {
      hasInstance: true,
      formValues: {
        CA: false,
        isValid: true
      },
      AQFormId: 'NAQ',
      RQFormId: 'NAQ',
      TQFormId: 'NAQ',
      CAFormId: 'NAC',
      AQUploadFileFormId: 'NAQ',
      CAUploadFileFormId: 'NAQ',
      TQUploadFileFormId: 'NAQ',
      RQUploadFileFormId: 'NAQ',
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    } as unknown as WorkflowSetupProps<FormGettingStart>;

    wrapper.rerender(<GettingStartStep {...props2} />);
  });

  it('change transaction', () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: {
        CA: true,
        isValid: true
      },
      setFormValues: mockFn
    } as WorkflowSetupProps<FormGettingStart>;

    const wrapper = renderComponent(props);
    const checkbox = wrapper.baseElement.querySelector(
      'input[class="custom-control-input dls-checkbox-input"]'
    ) as Element;
    const _checkbox = checkbox as HTMLInputElement;
    expect(_checkbox.checked).toEqual(false);

    fireEvent.click(checkbox);
    expect(_checkbox.checked).toEqual(true);

    fireEvent.click(wrapper.getByText('Transaction Qualification'));
  });

  it('should render stuck message', () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: {
        CA: true,
        isValid: true
      },
      setFormValues: mockFn,
      isStuck: true
    } as WorkflowSetupProps<FormGettingStart>;

    const wrapper = renderComponent(props);
    expect(
      wrapper.getByText('txt_step_stuck_move_forward_message')
    ).toBeInTheDocument();
  });
});
