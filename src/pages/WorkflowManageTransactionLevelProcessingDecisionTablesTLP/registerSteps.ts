import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import {
  stepRegistry,
  uploadFileStepRegistry
} from '../_commons/WorkflowSetup/registries';
import ConfigureParametersAQ from './ConfigureParametersAQ';
import ConfigureParametersCA from './ConfigureParametersCA';
import ConfigureParametersRQ from './ConfigureParametersRQ';
import ConfigureParametersTQ from './ConfigureParametersTQ';
import GettingStartStep from './GettingStartStep';
import TLPTableList from './TLPTableList';

stepRegistry.registerStep(
  'GetStartedManageTransactionLevelProcessing',
  GettingStartStep,
  [
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_TRANSACTION_LEVEL_PROCESSING_DECISION_TABLES_TLP__GET_STARTED__EXISTED_WORKFLOW,
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_TRANSACTION_LEVEL_PROCESSING_DECISION_TABLES_TLP__GET_STARTED__EXISTED_WORKFLOW__STUCK
  ]
);

stepRegistry.registerStep(
  'ConfigureParametersTQManageTransactionLevelProcessing',
  ConfigureParametersTQ,
  [
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_TRANSACTION_LEVEL_PROCESSING_DECISION_TABLES_TLP__CONFIG_TQ
  ]
);

stepRegistry.registerStep(
  'ConfigureParametersAQManageTransactionLevelProcessing',
  ConfigureParametersAQ,
  [
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_TRANSACTION_LEVEL_PROCESSING_DECISION_TABLES_TLP__CONFIG_AQ
  ]
);

stepRegistry.registerStep(
  'ConfigureParametersCAManageTransactionLevelProcessing',
  ConfigureParametersCA,
  [
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_TRANSACTION_LEVEL_PROCESSING_DECISION_TABLES_TLP__CONFIG_CA
  ]
);

stepRegistry.registerStep(
  'ConfigureParametersRQManageTransactionLevelProcessing',
  ConfigureParametersRQ,
  [
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_TRANSACTION_LEVEL_PROCESSING_DECISION_TABLES_TLP__CONFIG_RQ
  ]
);

uploadFileStepRegistry.registerComponent('TLPTableList', TLPTableList);
