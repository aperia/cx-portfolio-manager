import { render, RenderResult } from '@testing-library/react';
import 'app/utils/_mockComponent/mockUseTranslation';
import * as CommonSelectHooks from 'pages/_commons/redux/Common/select-hooks';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageTransactionLevelProcessingDecisionTLP';
import React from 'react';
import TableListSubGrid from './TLPTableListSubGrid';

const mockUseSelectWindowDimension = jest.spyOn(
  CommonSelectHooks,
  'useSelectWindowDimension'
);

const useSelectDecisionElementListData = jest.spyOn(
  WorkflowSetup,
  'useSelectDecisionElementListData'
);

const MockElementListOptions = [
  {
    name: 'ACS CURR PORT',
    moreInfo:
      'Adaptive Control System portfolio currently in effect for the customer account; variable-length, 4 positions, numeric.',
    immediateAllocation: 'No',
    configurable: true
  },
  {
    name: 'ACS PORT CHG DATE',
    moreInfo:
      'Date (MMDDYYYY) the current Adaptive Control System portfolio identification was last changed; variable-length, 10 positions, numeric.\n Example: 031404 or 03142004 = March 14, 2004',
    immediateAllocation: 'No',
    configurable: true
  },
  {
    name: 'ADVERTISING GROUP',
    moreInfo:
      'Issuer-defined advertising group code; variable-length, 3 positions, numeric.',
    immediateAllocation: 'No',
    configurable: true
  },
  {
    name: 'AGE',
    moreInfo:
      'Principal cardholder’s age expressed in whole number of years; variable-length, 3 positions, numeric.\n To determine whether a cardholder account matches a value, the System subtracts the year of birth from the current year. The System adds one year if the month of birth has already passed. If the date of birth consists of zeros, the age is 0.',
    immediateAllocation: 'No',
    configurable: true
  }
];

const MockElementList = [
  { name: 'AGE', moreInfo: 'test' },
  { name: '1', moreInfo: '2' }
] as any[];

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <TableListSubGrid {...props} />
    </div>
  );
};

describe('WorkflowManageTransactionLevelProcessingDecisionTablesTLP > TLPTableList > TLPTableListSubGrid', () => {
  beforeEach(() => {
    useSelectDecisionElementListData.mockReturnValue({
      decisionElementList: MockElementListOptions
    });
  });

  afterEach(() => {
    useSelectDecisionElementListData.mockClear();
  });

  it('Should render Table List SubGrid ', () => {
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1920,
      height: 1080
    }));
    const wrapper = renderComponent({
      elementList: MockElementList
    });

    expect(wrapper.getByText('txt_description')).toBeInTheDocument();
  });

  it('Should render Table List SubGrid ', () => {
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1200,
      height: 1080
    }));
    const wrapper = renderComponent({});

    expect(wrapper.getByText('txt_description')).toBeInTheDocument();
  });
});
