import { queryByText, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { renderWithMockStore } from 'app/utils';
import 'app/utils/_mockComponent/mockUseTranslation';
import { queryAllByClass } from 'app/_libraries/_dls/test-utils/queryHelpers';
import * as getElementAttribute from 'app/_libraries/_dls/utils/getElementAttribute';
import * as CommonSelectHooks from 'pages/_commons/redux/Common/select-hooks';
import * as selectHooks from 'pages/_commons/redux/WorkflowSetup/select-hooks/_common';
import React from 'react';
import TLPTableList from '.';

const useWorkflowFormStepMock = jest.spyOn(selectHooks, 'useWorkflowFormStep');

const mockUseSelectWindowDimension = jest.spyOn(
  CommonSelectHooks,
  'useSelectWindowDimension'
);

const renderComponent = (props: any): RenderResult => {
  return renderWithMockStore(
    <div>
      <TLPTableList {...props} />
    </div>
  ) as any;
};

jest.mock('resize-observer-polyfill', () =>
  jest.requireActual('app/_libraries/_dls/test-utils/mocks/MockResizeObserver')
);
jest.mock('app/_libraries/_dls/utils/canvasTextWidth.ts');

beforeEach(() => {
  jest
    .spyOn(getElementAttribute, 'getAvailableWidth')
    .mockImplementation(() => 1000);
  mockUseSelectWindowDimension.mockImplementation(() => ({
    width: 1920,
    height: 1080
  }));
  useWorkflowFormStepMock.mockImplementation(() => {
    return {
      tables: [
        { tableId: '1', original: { elementList: [{}] } },
        { tableId: '1', original: { elementList: [{}] } },
        { tableId: '2', original: { elementList: [{}] } },
        { tableId: '3', original: { elementList: [{}] } },
        { tableId: '4', original: { elementList: [{}] } },
        { tableId: '5', original: { elementList: [{}] } },
        { tableId: '6', original: { elementList: [{}] } },
        { tableId: '7', original: { elementList: [{}] } },
        { tableId: '8', original: { elementList: [{}] } },
        { tableId: '9', original: { elementList: [{}] } },
        { tableId: '10', original: { elementList: [{}] } },
        { tableId: '11', original: { elementList: [{}] } },
        { tableId: '12', original: { elementList: [{}] } },
        { tableId: '13', original: { elementList: [{}] } },
        { tableId: '14', original: { elementList: [{}] } },
        { tableId: '15', original: { elementList: [{}] } },
        { tableId: '16', original: { elementList: [{}] } },
        { tableId: '17', original: { elementList: [{}] } },
        { tableId: '18', original: { elementList: [{}] } },
        { tableId: '19', original: { elementList: [{}] } },
        { tableId: '20', original: { elementList: [{}] } }
      ]
    } as any;
  });
});

afterEach(() => {
  mockUseSelectWindowDimension.mockClear();
  useWorkflowFormStepMock.mockClear();
});

describe('WorkflowBulkExternalStatusManagement > ParameterList', () => {
  it('Should render a grid with content', async () => {
    const props = { stepId: 'uploadFileAccountQualification' };
    const wrapper = await renderComponent(props);
    let expandBtn = queryAllByClass(wrapper.container, /icon icon-plus/)[0];
    userEvent.click(expandBtn);

    expandBtn = queryAllByClass(wrapper.container, /icon icon-plus/)[0];
    userEvent.click(expandBtn);

    expect(
      wrapper.getByText('txt_manage_transaction_level_tlp_config_tq_tableList')
    ).toBeInTheDocument();

    // handle change page
    const page = wrapper.getByTestId('2__paper-step');
    userEvent.click(page);

    // handle change page size
    const dropdownListElement =
      wrapper.container.querySelector('.dls-dropdown-list');
    const iconElement = dropdownListElement?.querySelector('.icon');
    userEvent.click(iconElement!);

    const popupElement = wrapper.baseElement.querySelector('.dls-popup');
    expect(popupElement).toBeInTheDocument();

    const item = queryByText(popupElement as HTMLElement, /25/);
    userEvent.click(item!);
  });

  it('Should render a grid with content and stepId uploadFileTq selectedStep', async () => {
    const props = {
      stepId: 'uploadFileTransactionQualification',
      selectedStep: '3'
    };
    const wrapper = await renderComponent(props);

    const expandBtn = queryAllByClass(wrapper.container, /icon icon-plus/)[0];
    userEvent.click(expandBtn);

    expect(
      wrapper.getByText('txt_manage_transaction_level_tlp_config_tq_tableList')
    ).toBeInTheDocument();
    const minusBtn = queryAllByClass(wrapper.container, /icon icon-minus/)[0];
    userEvent.click(minusBtn);
  });

  it('Should render a grid with content and stepId uploadFileCa selectedStep', async () => {
    const props = { stepId: 'uploadFileClientAllocation', selectedStep: '3' };
    const wrapper = await renderComponent(props);

    const expandBtn = queryAllByClass(wrapper.container, /icon icon-plus/)[0];
    userEvent.click(expandBtn);

    expect(
      wrapper.getByText('txt_manage_account_level_table_decision_element_list')
    ).toBeInTheDocument();
    const minusBtn = queryAllByClass(wrapper.container, /icon icon-minus/)[0];
    userEvent.click(minusBtn);
  });

  it('Should render a grid with content and stepId uploadFileRq different selectedStep', async () => {
    const props = {
      stepId: 'uploadFileRetailQualification',
      selectedStep: '3'
    };
    const wrapper = await renderComponent(props);

    const expandBtn = queryAllByClass(wrapper.container, /icon icon-plus/)[0];
    userEvent.click(expandBtn);

    expect(
      wrapper.getByText('txt_manage_transaction_level_tlp_config_tq_tableList')
    ).toBeInTheDocument();
    const minusBtn = queryAllByClass(wrapper.container, /icon icon-minus/)[0];
    userEvent.click(minusBtn);
  });

  it('Should render a grid with content and default formvalues', async () => {
    useWorkflowFormStepMock.mockImplementation(() => {
      return {} as any;
    });
    const props = { stepId: '', selectedStep: '3' };
    await renderComponent(props);
  });

  it('Should render a grid with content and default useSelectWorkFlowData', async () => {
    useWorkflowFormStepMock.mockImplementation(() => {
      return undefined as any;
    });
    const props = { stepId: '', selectedStep: '3' };
    await renderComponent(props);
  });
});
