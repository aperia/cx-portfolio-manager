import { MethodFieldParameterEnum } from 'app/constants/enums';
import { formatCommon } from 'app/helpers';
import {
  ColumnType,
  Grid,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import { useSelectElementMetadataManageTransactionTLP } from 'pages/_commons/redux/WorkflowSetup';
import { formatText, viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React from 'react';
import { ControlParameterItem } from './type';

interface IControlParametersDataGrid {
  controlParameters: ControlParameterItem[];
}

const ControlParametersDataGrid: React.FC<IControlParametersDataGrid> = ({
  controlParameters = []
}) => {
  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();
  const metadata = useSelectElementMetadataManageTransactionTLP();
  const { includeActivityOptions, posPromoValidationOptions } = metadata;

  const renderParameterValues = (item: any) => {
    const { id, value } = item;
    if (!value) return null;
    let valueShow;
    switch (id) {
      case MethodFieldParameterEnum.NextDebitDate:
        valueShow = formatCommon(value as any).time.date;
        break;
      case MethodFieldParameterEnum.IncludeActivityAction:
        const activityMatchOption = includeActivityOptions.find(
          op => op.code === value
        );
        valueShow = activityMatchOption?.text;
        break;
      case MethodFieldParameterEnum.PosPromotionValidation:
        const posMatchOption = posPromoValidationOptions.find(
          op => op.code === value
        );
        valueShow = posMatchOption?.text;
        break;
      default:
        valueShow = value;
    }
    return (
      <TruncateText
        resizable
        lines={2}
        ellipsisLessText={t('txt_less')}
        ellipsisMoreText={t('txt_more')}
      >
        {valueShow}
      </TruncateText>
    );
  };

  const columnsControlParameters: ColumnType[] = [
    {
      id: 'businessName',
      Header: t('txt_manage_account_level_table_decision_business_name'),
      accessor: formatText(['businessName']),
      cellBodyProps: { className: 'pt-12 pb-8' },
      width: 233
    },
    {
      id: 'greenScreenName',
      Header: t('txt_manage_account_level_table_decision_green_name'),
      accessor: formatText(['greenScreenName']),
      cellBodyProps: { className: 'pt-12 pb-8' },
      width: 244
    },
    {
      id: 'value',
      Header: t('txt_manage_account_level_table_decision_value'),
      width: width < 1280 ? 320 : 360,
      cellBodyProps: { className: 'py-8' },
      accessor: renderParameterValues
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      width: 106,
      cellBodyProps: { className: 'pt-12 pb-8' },
      accessor: viewMoreInfo(['moreInfo'], t)
    }
  ];

  return <Grid columns={columnsControlParameters} data={controlParameters} />;
};

export default ControlParametersDataGrid;
