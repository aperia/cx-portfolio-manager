import { render, RenderResult } from '@testing-library/react';
import 'app/utils/_mockComponent/mockUseTranslation';
import * as CommonSelectHooks from 'pages/_commons/redux/Common/select-hooks';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageTransactionLevelProcessingDecisionTLP';
import React from 'react';
import * as constant from './constant';
import { getControlParameters, TableType } from './constant';
import GridControlParametersTableDetailsFlyout from './GridControlParametersTableDetailsFlyout';

const useSelectElementMetadataManageTransactionTLP = jest.spyOn(
  WorkflowSetup,
  'useSelectElementMetadataManageTransactionTLP'
);

const mockUseSelectWindowDimension = jest.spyOn(
  CommonSelectHooks,
  'useSelectWindowDimension'
);

HTMLCanvasElement.prototype.getContext = jest.fn();

const MockIncludeActivityOptions = [
  {
    code: '',
    text: 'None',
    description: 'None'
  },
  {
    code: 'A',
    text: 'A - Use both cash advance transactions and merchandise transactions in the table.',
    description:
      'Use both cash advance transactions and merchandise transactions in the table.'
  },
  {
    code: 'C',
    text: 'C - Use only cash advance transactions in the table.',
    description: 'Use only cash advance transactions in the table.'
  },
  {
    code: 'M',
    text: 'M - Use only merchandise transactions in the table.',
    description: 'Use only merchandise transactions in the table.'
  }
];
const MockPosPromoValidationOptions = [
  {
    code: '',
    text: 'None',
    description: 'None'
  },
  {
    code: 'N',
    text: 'N - Do not use POS promotion validation',
    description: 'Do not use POS promotion validation'
  },
  {
    code: 'Y',
    text: 'Y - Use POS promotion validation',
    description: 'Use POS promotion validation'
  }
];
const MockTLPAQControlParameters = [
  {
    name: 'next.debit.date',
    businessName: 'Next Debit Date',
    greenScreenName: 'Next Debit Date',
    moreInfo:
      'The Next Debit Date parameter defines the date (MM/DD/YYYY) when the next debit will post to the account.',
    value: '2022-03-28T17:00:00.000Z'
  },
  {
    name: 'include.activity.option',
    businessName: 'Include Activity Option',
    greenScreenName: 'INCL Activity Option',
    moreInfo:
      'The Include Activity Option parameter indicates whether the System includes the cash advance transaction type, merchandise transaction type, or both transaction types in the table.',
    value: 'M'
  },
  {
    name: 'test',
    businessName: 'POS Promotion Validation',
    greenScreenName: 'POS PROMO Validation',
    value: 'N',
    moreInfo:
      'The POS Promotion Validation parameter designates whether you want to set the A/D/R IND and RESN CD fields on the Table Detail screen to indicate the result of the point-of-sale validation and provide a reason code.'
  }
];

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <GridControlParametersTableDetailsFlyout {...props} />
    </div>
  );
};

describe('WorkflowManageTransactionLevelProcessingDecisionTablesTLP > ConfigureParameters > ControlParametersDataGrid', () => {
  const controlParameters = getControlParameters(TableType.tlpaq);
  const defaultProps = {
    data: controlParameters,
    tableType: TableType.tlpaq
  };

  beforeEach(() => {
    useSelectElementMetadataManageTransactionTLP.mockImplementation(() => ({
      searchCode: [],
      includeActivityOptions: MockIncludeActivityOptions,
      posPromoValidationOptions: MockPosPromoValidationOptions
    }));
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1920,
      height: 1080
    }));
  });

  afterEach(() => {
    mockUseSelectWindowDimension.mockClear();
    useSelectElementMetadataManageTransactionTLP.mockClear();
  });

  it('Should render Control parameters grid data with no value', () => {
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1920,
      height: 1080
    }));

    const wrapper = renderComponent({ ...defaultProps, isShowFlyout: true });
    expect(
      wrapper.getByText('txt_manage_account_level_table_decision_business_name')
    ).toBeInTheDocument();
  });

  it('Should render Control parameters grid data with TLP-AQ Values', () => {
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1000,
      height: 1080
    }));

    const wrapper = renderComponent({
      ...defaultProps,
      data: MockTLPAQControlParameters,
      isShowFlyout: true
    });
    expect(
      wrapper.getByText('txt_manage_account_level_table_decision_business_name')
    ).toBeInTheDocument();
  });

  it('Should render Control parameters grid data with TLP-RQ and value is empty', () => {
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1000,
      height: 1080
    }));

    const wrapper = renderComponent({
      ...defaultProps,
      data: [
        {
          name: 'pos.promotion.validation',
          value: ''
        },
        {
          name: 'pos.promotion.validation',
          value: 'N'
        }
      ],
      tableType: TableType.tlprq
    });
    expect(
      wrapper.getByText('txt_manage_account_level_table_decision_business_name')
    ).toBeInTheDocument();
  });

  it('Should render Control parameters grid data with TLP-RQ and Has Value', () => {
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1000,
      height: 1080
    }));

    const wrapper = renderComponent({
      ...defaultProps,
      data: [
        {
          name: 'pos.promotion.validation',
          value: 'N'
        }
      ],
      tableType: TableType.tlprq
    });
    expect(
      wrapper.getByText('txt_manage_account_level_table_decision_business_name')
    ).toBeInTheDocument();
  });

  it('Should render Control parameters with empty parameters', () => {
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1000,
      height: 1080
    }));

    renderComponent({
      ...defaultProps,
      data: undefined
    });
  });

  it('Should render Value of control parameter with default value', () => {
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 2000,
      height: 1080
    }));

    jest.spyOn(constant, 'getControlParameters').mockImplementation(() => {
      return [
        {
          name: 'test',
          businessName: 'POS Promotion Validation',
          greenScreenName: 'POS PROMO Validation',
          value: 'N',
          moreInfo:
            'The POS Promotion Validation parameter designates whether you want to set the A/D/R IND and RESN CD fields on the Table Detail screen to indicate the result of the point-of-sale validation and provide a reason code.'
        }
      ] as any;
    });

    renderComponent({
      ...defaultProps,
      data: undefined
    });
  });
});
