import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import 'app/utils/_mockComponent/mockUseTranslation';
import React from 'react';
import { TableType } from './constant';
import Summary from './Summary';

jest.mock('./TableListSummary', () => ({
  __esModule: true,
  default: () => {
    return <div>TableList_Summary_component</div>;
  }
}));

jest.mock('./ViewNewVersionSummary', () => ({
  __esModule: true,
  default: () => {
    return <div>ViewNewVersionSummary</div>;
  }
}));

const renderComponent = (props: any) => {
  return render(
    <div>
      <Summary {...props} />
    </div>
  );
};

describe('WorkflowManageTransactionLevelProcessingDecisionTablesTLP > ConfigureParameters > Summary', () => {
  const onEditStep = jest.fn();
  const defaultProps = {
    stepId: 'config',
    selectedStep: 'config',
    setFormValues: jest.fn(),
    onEditStep
  };

  it('Should render Table List Summary', () => {
    const wrapper = renderComponent({
      ...defaultProps,
      formValues: {
        tableType: TableType.tlpaq,
        tables: [1, 2, 3]
      }
    });
    expect(
      wrapper.getByText('TableList_Summary_component')
    ).toBeInTheDocument();
  });

  it('Should render View new Version summary', () => {
    const wrapper = renderComponent({
      ...defaultProps,
      formValues: {
        tableType: TableType.tlpca,
        tables: [1]
      }
    });
    expect(wrapper.getByText('ViewNewVersionSummary')).toBeInTheDocument();
  });

  it('Should Call Function OnEdit', () => {
    const wrapper = renderComponent({
      ...defaultProps,
      formValues: {
        tableType: TableType.tlpca,
        tables: [1]
      }
    });
    const editBtn = wrapper.getByText('txt_edit');
    userEvent.click(editBtn);
    expect(onEditStep).toBeCalled();
  });

  it('Should Render with no formValues', () => {
    renderComponent({
      ...defaultProps,
      formValues: undefined
    });
  });
});
