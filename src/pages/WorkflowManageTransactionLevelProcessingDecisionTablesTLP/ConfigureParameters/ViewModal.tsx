import GroupTextControl from 'app/components/DofControl/GroupTextControl';
import ModalRegistry from 'app/components/ModalRegistry';
import { PAGE_SIZE } from 'app/constants/constants';
import { formatCommon } from 'app/helpers';
import {
  Badge,
  ColumnType,
  Grid,
  HorizontalTabs,
  Icon,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import {
  DEFAULT_PAGE_NUMBER,
  DEFAULT_PAGE_SIZE
} from 'app/_libraries/_dls/components/Pagination/constants';
import {
  mapColorBadge,
  mappingData
} from 'pages/WorkflowManageAccountLevel/ManageAccountLevelCAStep/helper';
import { useSelectElementMetadataManageAcountLevel } from 'pages/_commons/redux/WorkflowSetup';
import Paging from 'pages/_commons/Utils/Paging';
import React, { useMemo, useState } from 'react';
import { TableType } from './constant';
import GridControlParameters from './GridControlParametersTableDetailsFlyout';

interface ViewProps {
  tableId: string;
  data?: any;
  onCancel: () => void;
  onOK: () => void;
  tableType: TableType;
}

const ViewModal: React.FC<ViewProps> = ({
  tableId,
  data,
  onCancel,
  onOK,
  tableType
}) => {
  const { t } = useTranslation();
  const {
    tableName,
    effectiveDate,
    status,
    comment,
    tableControlParameters,
    decisionElements
  } = data;
  const [activeTab, setActiveTab] = useState<string | null>('table');
  const [page, setPage] = useState<number>(DEFAULT_PAGE_NUMBER);
  const [pageSize, setPageSize] = useState<number>(PAGE_SIZE[0]);

  const metadata = useSelectElementMetadataManageAcountLevel();

  const { searchCode } = metadata;

  const handleSelectTab = (
    eventKey: string | null,
    e: React.SyntheticEvent<unknown>
  ) => {
    setActiveTab(eventKey);
  };

  const onlyShowElementList = useMemo(() => {
    return tableType === TableType.tlptq || tableType === TableType.tlpca;
  }, [tableType]);

  const filterData = decisionElements || [];

  const total = filterData[0]?.value?.length;

  const dataPage = filterData?.map((item: DecisionType) => {
    return {
      ...item,
      value: item.value.slice((page - 1) * pageSize, page * pageSize)
    };
  });

  const columns = dataPage?.map((clm: any, index: number) => ({
    id: clm?.name + index,
    Header: (
      <div className="d-flex align-items-end">
        <p>{clm?.name}</p>
        {!!clm?.searchCode && (
          <Tooltip
            triggerClassName="ml-8 d-flex"
            element={
              <div>
                <p>
                  {t('txt_manage_account_level_table_decision_search')}:{' '}
                  {
                    searchCode.filter(val => val.code === clm?.searchCode)[0]
                      ?.text
                  }
                </p>
                {!!clm?.immediateAllocation && (
                  <p>
                    {t(
                      'txt_manage_account_level_table_decision_immediate_allocation'
                    )}
                    : {clm?.immediateAllocation}
                  </p>
                )}
              </div>
            }
          >
            <Icon name="information" size="4x" className="color-grey-l16" />
          </Tooltip>
        )}
      </div>
    ),
    accessor: (data: any) => data![clm?.name],
    width: 180,
    minWidth: 180
  })) as ColumnType[];

  const handleChangePage = (page: number) => {
    setPage(page);
  };

  const handleChangePageSize = (pageSize: number) => {
    setPageSize(pageSize);
  };

  return (
    <ModalRegistry
      id={`table-view-${tableId}`}
      xs
      rt
      enforceFocus={false}
      scrollable={false}
      show
      animationDuration={500}
      animationComponentProps={{ direction: 'right' }}
      classes={{
        dialog: 'window-sm'
      }}
      onAutoClosedAll={onCancel}
    >
      <ModalHeader border closeButton onHide={onCancel}>
        <ModalTitle>{t('txt_manage_account_level_table_details')}</ModalTitle>
      </ModalHeader>
      <ModalBody className="p-0 overflow-auto">
        <div className="bg-light-l20">
          <div className="p-24">
            <div className="row">
              <div className="col-12">
                <h5>{tableName}</h5>
              </div>
              <div className="col-4">
                <GroupTextControl
                  id="tableId"
                  input={{ value: tableId } as any}
                  meta={{} as any}
                  label={t('txt_manage_account_level_table_id')}
                  options={{ inline: false }}
                />
              </div>
              <div className="col-4">
                <GroupTextControl
                  id="tableId"
                  input={
                    { value: formatCommon(effectiveDate).time.date } as any
                  }
                  meta={{} as any}
                  label={t('txt_effective_date')}
                  options={{ inline: false }}
                />
              </div>
              <div className="col-4">
                <div className="form-group-static align-items-center">
                  <span className="form-group-static__label">
                    <span className="color-grey text-nowrap mr-2">
                      {t('txt_manage_account_level_table_status')}
                    </span>
                  </span>
                  <Badge color={mapColorBadge(status)}>
                    <span>{status}</span>
                  </Badge>
                </div>
              </div>
              <div className="col-12">
                <GroupTextControl
                  id="tableId"
                  input={{ value: comment } as any}
                  meta={{} as any}
                  label={t('txt_comment_area')}
                  options={{ inline: false }}
                />
              </div>
            </div>
          </div>
          {!onlyShowElementList && (
            <HorizontalTabs
              id="tab"
              activeKey={activeTab!}
              onSelect={handleSelectTab}
              defaultActiveKey="table"
              level2
            >
              <HorizontalTabs.Tab
                key="table"
                eventKey="table"
                title={t('txt_manage_account_level_table_control')}
              >
                <div className="bg-white p-24">
                  <h5 className="mb-16">
                    {t('txt_manage_account_level_table_parameters')}
                  </h5>
                  <GridControlParameters
                    tableType={tableType}
                    data={tableControlParameters}
                    isShowFlyout
                  />
                </div>
              </HorizontalTabs.Tab>
              <HorizontalTabs.Tab
                key="decision"
                eventKey="decision"
                title={t('txt_manage_account_level_table_decision_elements')}
              >
                <div className="bg-white p-24">
                  <h5 className="mb-16">
                    {t('txt_manage_account_level_table_decision_elements')}
                  </h5>
                  <Grid columns={columns} data={mappingData(dataPage)} />
                  {total > DEFAULT_PAGE_SIZE && (
                    <div className="mt-16">
                      <Paging
                        page={page}
                        pageSize={pageSize}
                        totalItem={total}
                        onChangePage={handleChangePage}
                        onChangePageSize={handleChangePageSize}
                      />
                    </div>
                  )}
                </div>
              </HorizontalTabs.Tab>
            </HorizontalTabs>
          )}
          {onlyShowElementList && (
            <div className="bg-white p-24 border-top">
              <h5 className="mb-16">
                {t('txt_manage_account_level_table_decision_elements')}
              </h5>
              <Grid columns={columns} data={mappingData(dataPage)} />
              {total > DEFAULT_PAGE_SIZE && (
                <div className="mt-16">
                  <Paging
                    page={page}
                    pageSize={pageSize}
                    totalItem={total}
                    onChangePage={handleChangePage}
                    onChangePageSize={handleChangePageSize}
                  />
                </div>
              )}
            </div>
          )}
        </div>
      </ModalBody>
      <ModalFooter
        border
        cancelButtonText="Close"
        onCancel={onCancel}
        okButtonText="Select This Version"
        onOk={onOK}
      />
    </ModalRegistry>
  );
};

export default ViewModal;
