import {
  confirmEditWhenChangeEffectToUploadProps,
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { useUnsavedChangeRegistry } from 'app/hooks';
import { InlineMessage, useTranslation } from 'app/_libraries/_dls';
import { StateFile } from 'app/_libraries/_dls/components/Upload/File';
import { STATUS } from 'app/_libraries/_dls/components/Upload/helper';
import { omit } from 'lodash';
import get from 'lodash.get';
import isEmpty from 'lodash.isempty';
import isEqual from 'lodash.isequal';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import React, { useEffect, useMemo, useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import ActionButtons from './ActionButtons';
import AddNewTableModal from './AddNewTableModal';
import { TableType } from './constant';
import RemoveTableModal from './RemoveTableModal';
import TableList from './TableList';
import TableListModal from './TableListModal';
import { ConfigTableItem, ElementItem } from './type';
import {
  CONFIG_ACTIONS_MODAL,
  EXPANDED_LIST,
  EXPAND_ACTION,
  useConfigAction
} from './useConfigActions';
import ViewNewVersion from './ViewNewVersion';

export interface ConfigureParametersFormValue {
  isValid?: boolean;
  tables?: ConfigTableItem[];
  tableType?: TableType;
}

interface IDependencies {
  files?: StateFile[];
}

const ConfigureParameters: React.FC<
  WorkflowSetupProps<ConfigureParametersFormValue, IDependencies> & {
    tableType: TableType;
  }
> = ({
  formValues,
  setFormValues,
  stepId,
  selectedStep,
  tableType,
  isStuck,
  ...props
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const tables = formValues.tables || [];
  const [originTable, setOriginTable] = useState(tables);
  const [hasChange, setHasChange] = useState(false);
  const isSelectedStep = stepId === selectedStep;

  const {
    openedModal,
    handleOpenConfigModal,
    selectedVersion,
    handleSetExpandedList,
    handleSetSelectedVersion,
    expandedList,
    isCreateNewVersion,
    handleSetIsCreateNewVersion,
    handleSetDraftNewTable,
    draftNewTable,
    handleAddNewTable,
    tableList,
    handleSetSelectedTable,
    selectedTable,
    handleEditTable,
    handleDeleteTable,
    handleCheckDuplicateTableId,
    tableIndexChanged,
    handleSetTableIndexChanged
  } = useConfigAction(tables, tableType);

  const [isShowConfirmAffectFile, setShowConfirmAffectFile] = useState(false);
  const onlyShowCreateNewVersion = useMemo(() => {
    return tableType === TableType.tlpca;
  }, [tableType]);

  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;

  useEffect(() => {
    if (isEqual(tableList, formValues.tables)) return;
    setFormValues({ tables: tableList });
    setHasChange(true);
    if (isEmpty(props?.dependencies?.files)) return;
    if (originTable.length !== tableList.length) {
      setShowConfirmAffectFile(true);
      return;
    }
    const isChange = originTable?.some((table, index) => {
      return !isEqual(
        {
          tableId: table?.tableId,
          elementList: table?.elementList?.map((element: ElementItem) =>
            omit(element, 'rowId')
          )
        },
        {
          tableId: tableList[index]?.tableId,
          elementList: tableList[index]?.elementList?.map(
            (element: ElementItem) => omit(element, 'rowId')
          )
        }
      );
    });
    setShowConfirmAffectFile(isChange);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tableList, setFormValues, formValues]);

  useEffect(() => {
    if (isEmpty(props?.dependencies?.files)) return;

    setOriginTable(tableList);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props?.dependencies?.files]);

  useEffect(() => {
    const isValid = (formValues?.tables?.length || 0) > 0;
    if (formValues.isValid === isValid) return;

    keepRef.current.setFormValues({ isValid });
  }, [formValues]);

  useEffect(() => {
    if (isSelectedStep) return;
    handleSetExpandedList(EXPANDED_LIST.tableListIndex)({
      expandAction: EXPAND_ACTION.replace,
      expandedList: {}
    });
    setHasChange(false);
  }, [handleSetExpandedList, isSelectedStep]);

  const handleResetUploadStep = () => {
    const file = props?.dependencies?.files?.[0];
    const attachmentId = file?.idx;
    const isValid = file?.status === STATUS.valid;
    props?.clearFormValues(get(props, 'clearFormName', ''));
    isValid &&
      dispatch(actionsWorkflowSetup.deleteTemplateFile({ attachmentId }));
    setShowConfirmAffectFile(false);
  };

  const confirmUnSavedChangeFormName = useMemo(() => {
    switch (tableType) {
      case TableType.tlptq:
        return UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_TRANSACTION_LEVEL_PROCESSING_DECISION_TABLES_TLP__CONFIG_TQ;
      case TableType.tlpaq:
        return UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_TRANSACTION_LEVEL_PROCESSING_DECISION_TABLES_TLP__CONFIG_AQ;
      case TableType.tlpca:
        return UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_TRANSACTION_LEVEL_PROCESSING_DECISION_TABLES_TLP__CONFIG_CA;
      case TableType.tlprq:
        return UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_TRANSACTION_LEVEL_PROCESSING_DECISION_TABLES_TLP__CONFIG_RQ;
      default:
        return '';
    }
  }, [tableType]);

  const confirmEditFormName = useMemo(() => {
    switch (tableType) {
      case TableType.tlptq:
        return UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_TRANSACTION_LEVEL_PROCESSING_DECISION_TABLES_TLP__CONFIG_TQ_CONFIRM_EDIT;
      case TableType.tlpaq:
        return UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_TRANSACTION_LEVEL_PROCESSING_DECISION_TABLES_TLP__CONFIG_AQ_CONFIRM_EDIT;
      case TableType.tlpca:
        return UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_TRANSACTION_LEVEL_PROCESSING_DECISION_TABLES_TLP__CONFIG_CA_CONFIRM_EDIT;
      case TableType.tlprq:
        return UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_TRANSACTION_LEVEL_PROCESSING_DECISION_TABLES_TLP__CONFIG_RQ_CONFIRM_EDIT;
      default:
        return '';
    }
  }, [tableType]);

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName: confirmUnSavedChangeFormName,
      priority: 1
    },
    [hasChange]
  );

  useUnsavedChangeRegistry(
    {
      ...confirmEditWhenChangeEffectToUploadProps,
      formName: confirmEditFormName,
      priority: 1
    },
    [!isEmpty(props?.dependencies?.files) && isShowConfirmAffectFile],
    handleResetUploadStep
  );

  return (
    <div>
      {isStuck && (
        <InlineMessage className="mb-0 mt-24" variant="danger" withIcon>
          {t('txt_step_stuck_move_forward_message')}
        </InlineMessage>
      )}
      <div className="mt-24">
        {!(tableList.length === 1 && onlyShowCreateNewVersion) && (
          <ActionButtons
            small={!isEmpty(tableList)}
            title="Table List"
            onlyShowCreateNewVersion={onlyShowCreateNewVersion}
            onClickAddNewTable={() =>
              handleOpenConfigModal(CONFIG_ACTIONS_MODAL.addNewTableModal)
            }
            onClickTableToModel={() =>
              handleOpenConfigModal(CONFIG_ACTIONS_MODAL.tableListModal)
            }
            onClickCreateNewVersion={() => {
              handleOpenConfigModal(CONFIG_ACTIONS_MODAL.tableListModal);
              handleSetIsCreateNewVersion(true);
            }}
          />
        )}
      </div>
      {tableList &&
        tableList.length === 1 &&
        onlyShowCreateNewVersion &&
        isSelectedStep && (
          <ViewNewVersion
            dataTableVersion={tableList[0]}
            handleSetSelectedTable={handleSetSelectedTable}
            handleOpenConfigModal={handleOpenConfigModal}
          />
        )}
      {tableList &&
        tableList.length > 0 &&
        !onlyShowCreateNewVersion &&
        isSelectedStep && (
          <TableList
            tableList={tableList}
            handleSetSelectedTable={handleSetSelectedTable}
            handleOpenConfigModal={handleOpenConfigModal}
            handleSetExpandedList={handleSetExpandedList}
            expandedList={expandedList[EXPANDED_LIST.tableListIndex]}
            tableIndexChanged={tableIndexChanged}
            handleSetTableIndexChanged={handleSetTableIndexChanged}
          />
        )}

      {openedModal === CONFIG_ACTIONS_MODAL.deleteTableModal && (
        <RemoveTableModal
          id="removeMethod"
          show
          onOk={() => handleDeleteTable(selectedTable?.rowId as string)}
          onClose={() => {
            handleOpenConfigModal(null);
            handleSetSelectedTable(null);
          }}
        />
      )}
      {openedModal === CONFIG_ACTIONS_MODAL.addNewTableModal && (
        <AddNewTableModal
          id="addNewMethod"
          show
          handleClose={() => handleOpenConfigModal(null)}
          selectedVersion={selectedVersion}
          handleOpenConfigModal={handleOpenConfigModal}
          handleSelectVersion={handleSetSelectedVersion}
          handleSetDraftNewTable={handleSetDraftNewTable}
          draftNewTable={draftNewTable}
          isCreateNewVersion={isCreateNewVersion}
          handleAddNewTable={handleAddNewTable}
          handleSetIsCreateNewVersion={handleSetIsCreateNewVersion}
          selectedTable={selectedTable}
          handleEditTable={handleEditTable}
          handleSetSelectedTable={handleSetSelectedTable}
          handleSetExpandedList={handleSetExpandedList}
          handleCheckDuplicateTableId={handleCheckDuplicateTableId}
          tableType={tableType}
        />
      )}
      {openedModal === CONFIG_ACTIONS_MODAL.tableListModal && (
        <TableListModal
          id="editMethodFromModel"
          show
          handleClose={() => handleOpenConfigModal(null)}
          selectedVersion={selectedVersion}
          handleSetExpandedList={handleSetExpandedList}
          handleSelectVersion={handleSetSelectedVersion}
          expandedList={expandedList[EXPANDED_LIST.tableListModal]}
          handleOpenConfigModal={handleOpenConfigModal}
          isCreateNewVersion={isCreateNewVersion}
          handleSetIsCreateNewVersion={handleSetIsCreateNewVersion}
          handleSetDraftNewTable={handleSetDraftNewTable}
          draftNewTable={draftNewTable}
          tableType={tableType}
        />
      )}
    </div>
  );
};

export default ConfigureParameters;
