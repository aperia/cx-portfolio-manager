import { PAGE_SIZE } from 'app/constants/constants';
import { BadgeColorType } from 'app/constants/enums';
import { mapGridExpandCollapse } from 'app/helpers';
import {
  ColumnType,
  Grid,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import {
  DEFAULT_PAGE_NUMBER,
  DEFAULT_PAGE_SIZE
} from 'app/_libraries/_dls/components/Pagination/constants';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import { formatBadge, formatText } from 'pages/_commons/Utils/formatGridField';
import Paging from 'pages/_commons/Utils/Paging';
import React, { useCallback, useMemo, useState } from 'react';
import { AddNewTableAction } from './AddNewTableModal';
import TableListSubGrid from './TableListSubGrid';
import { ConfigTableItem } from './type';

interface ITableListSummary {
  tableList: ConfigTableItem[];
}

const TableListSummary: React.FC<ITableListSummary> = ({ tableList }) => {
  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();
  const [page, setPage] = useState<number>(DEFAULT_PAGE_NUMBER);
  const [pageSize, setPageSize] = useState<number>(PAGE_SIZE[0]);
  const [expandedList, setExpandedList] = useState<string[]>([]);

  const dataTablePage = useMemo(() => {
    return tableList?.slice(pageSize * (page - 1), page * pageSize);
  }, [tableList, pageSize, page]);

  const handleChangePage = (page: number) => setPage(page);
  const handleChangePageSize = (pageSize: number) => setPageSize(pageSize);

  const methodTypeToText = useCallback((type: WorkflowSetupMethodType) => {
    switch (type) {
      case AddNewTableAction.createNewVersion:
        return 'Version Created';
      case AddNewTableAction.createNewTable:
        return 'Table Created';
      default:
        return 'Table Modeled';
    }
  }, []);
  const columns: ColumnType[] = useMemo(
    () => [
      {
        id: 'tableId',
        Header: t('txt_manage_transaction_level_tlp_config_tq_table_id'),
        accessor: formatText(['tableId']),
        width: 120
      },
      {
        id: 'tableName',
        Header: t('txt_manage_transaction_level_tlp_config_tq_table_name'),
        accessor: formatText(['tableName']),
        width: 120
      },
      {
        id: 'modelOrCreateFrom',
        Header: t('txt_change_interest_rates_model_create'),
        accessor: formatText(['selectedTableId']),
        width: 128
      },
      {
        id: 'comment',
        Header: t('txt_comment_area'),
        accessor: (data: any) => {
          return (
            <TruncateText
              resizable
              lines={2}
              ellipsisLessText={t('txt_less')}
              ellipsisMoreText={t('txt_more')}
            >
              {data?.comment}
            </TruncateText>
          );
        },
        width: width > 1280 ? 360 : 300
      },
      {
        id: 'methodType',
        Header: t('txt_manage_penalty_fee_action_taken'),
        accessor: (data, idx) =>
          formatBadge(['tableType'], {
            colorType: BadgeColorType.TableType,
            noBorder: true
          })(
            {
              ...data,
              tableType: methodTypeToText(data.actionTaken)
            },
            idx
          ),
        width: 153
      }
    ],
    [t, methodTypeToText, width]
  );

  return (
    <div className="pt-16">
      <Grid
        columns={columns}
        data={dataTablePage}
        subRow={(data: any) => {
          return (
            <TableListSubGrid
              controlParameters={data?.original?.controlParameters}
              elementList={data?.original?.elementList}
            />
          );
        }}
        togglable
        toggleButtonConfigList={tableList.map(
          mapGridExpandCollapse('rowId', t)
        )}
        expandedItemKey={'rowId'}
        expandedList={expandedList}
        dataItemKey="rowId"
        onExpand={setExpandedList}
      />
      {tableList?.length > DEFAULT_PAGE_SIZE && (
        <div className="mt-16">
          <Paging
            page={page}
            pageSize={pageSize}
            totalItem={tableList?.length}
            onChangePage={handleChangePage}
            onChangePageSize={handleChangePageSize}
          />
        </div>
      )}
    </div>
  );
};

export default TableListSummary;
