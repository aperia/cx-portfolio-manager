import FailedApiReload from 'app/components/FailedApiReload';
import ModalRegistry from 'app/components/ModalRegistry';
import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import TableCardView from 'app/components/TableCardView';
import {
  ORDER_BY_LIST,
  WORKFLOW_TLP_CA_SORT_BY_LIST
} from 'app/constants/constants';
import { classnames } from 'app/helpers';
import {
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  TransDLS,
  useTranslation
} from 'app/_libraries/_dls';
import DropdownList from 'app/_libraries/_dls/components/DropdownList';
import isEmpty from 'lodash.isempty';
import { defaultWorkflowMethodsFilter } from 'pages/_commons/redux/WorkflowSetup/reducers';
import ClearAndResetButton from 'pages/_commons/Utils/ClearAndResetButton';
import Paging from 'pages/_commons/Utils/Paging';
import SortOrder from 'pages/_commons/Utils/SortOrder';
import React, { useMemo, useRef } from 'react';
import { TableType } from './constant';
import {
  DraftNewTable,
  SelectedVersion,
  TableItem,
  TableVersion
} from './type';
import { CONFIG_ACTIONS_MODAL, EXPANDED_LIST } from './useConfigActions';
import { useTableListModal } from './useTableListModal';
import VersionCardView from './VersionCardView';
import ViewVersionDetail from './ViewModal';

export interface TableListModalProps {
  id: string;
  show: boolean;
  handleClose: () => void;
  selectedVersion: SelectedVersion | null;
  handleSetExpandedList: (
    tableListName: EXPANDED_LIST
  ) => (expandItem: Record<string, any>) => void;
  handleSelectVersion: (version: any) => void;
  expandedList: Record<string, any>;
  handleOpenConfigModal: (tableName: CONFIG_ACTIONS_MODAL | null) => void;
  isCreateNewVersion: boolean;
  handleSetIsCreateNewVersion: (isCreateNewVersion: boolean) => void;
  handleSetDraftNewTable: (draftNewTable: DraftNewTable | null) => void;
  draftNewTable: DraftNewTable | null;
  tableType: TableType;
}

const TableListModal: React.FC<TableListModalProps> = props => {
  const { t } = useTranslation();
  const { id, show, expandedList, selectedVersion, tableType } = props;
  const simpleSearchRef = useRef<any>(null);
  const {
    dataListShow,
    total,
    page,
    pageSize,
    handleChangePage,
    handleChangePageSize,
    handleSearch,
    orderBy,
    searchValue,
    handleChangeOrderBy,
    loading,
    error,
    handleGetTableList,
    handleClearAndReset,
    versionDetail,
    title,
    helpText1,
    handleGoToNextStep,
    handleToggleExpandedTableListModal,
    handleSelectTableVersion,
    handleCloseModalVersionDetail,
    showListVersion,
    listVersionTableId,
    handleChangeSortBy,
    dataFilter,
    handleCloseModal
  } = useTableListModal(props);

  const orderByListItems = useMemo(() => {
    return ORDER_BY_LIST.map(item => (
      <DropdownList.Item
        key={item.value}
        label={item.description}
        value={item}
      />
    ));
  }, []);

  const noDataFound = useMemo(
    () =>
      !loading &&
      !error &&
      total === 0 && (
        <div className="d-flex flex-column justify-content-center mt-40">
          <NoDataFound
            id="method-list-NoDataFound"
            title={t('txt_no_tables_to_display')}
            hasSearch={!!searchValue}
            linkTitle={!!searchValue && t('txt_clear_and_reset')}
            onLinkClicked={handleClearAndReset}
          />
        </div>
      ),
    [t, loading, error, total, searchValue, handleClearAndReset]
  );
  const failedReload = useMemo(
    () => (
      <div>
        {error && (
          <FailedApiReload
            id="method-list-error"
            onReload={handleGetTableList}
            className="mt-40 pt-40 d-flex flex-column justify-content-center align-items-center"
          />
        )}
      </div>
    ),
    [error, handleGetTableList]
  );

  const tableList = useMemo(
    () => (
      <div>
        {total > 0 && (
          <React.Fragment>
            {!showListVersion && (
              <div className="mt-16 mx-n12">
                {(dataListShow as TableItem[]).map(table => (
                  <TableCardView
                    id={table.tableId}
                    key={table.tableId}
                    value={table}
                    selectedVersion={selectedVersion}
                    onSelectVersion={handleSelectTableVersion(
                      table.tableId,
                      table.tableName
                    )}
                    onViewVersionDetail={handleSelectTableVersion(
                      table.tableId,
                      table.tableName,
                      true
                    )}
                    isExpand={expandedList[table?.tableId]}
                    onToggle={handleToggleExpandedTableListModal}
                  />
                ))}
              </div>
            )}
            {showListVersion && (
              <div>
                {(dataListShow as TableVersion[]).map(version => (
                  <VersionCardView
                    key={version?.tableId}
                    onSelectVersion={handleSelectTableVersion(
                      listVersionTableId,
                      ''
                    )}
                    onViewVersionDetail={handleSelectTableVersion(
                      listVersionTableId,
                      '',
                      true
                    )}
                    selectedVersion={selectedVersion}
                    version={version}
                  />
                ))}
              </div>
            )}
            <div className="mt-8">
              <Paging
                page={page}
                pageSize={pageSize}
                totalItem={total}
                onChangePage={handleChangePage}
                onChangePageSize={handleChangePageSize}
              />
            </div>
          </React.Fragment>
        )}
      </div>
    ),
    [
      handleChangePage,
      handleChangePageSize,
      total,
      page,
      pageSize,
      dataListShow,
      selectedVersion,
      handleToggleExpandedTableListModal,
      handleSelectTableVersion,
      expandedList,
      showListVersion,
      listVersionTableId
    ]
  );

  return (
    <React.Fragment>
      <ModalRegistry id={id} show={show} lg onAutoClosedAll={handleCloseModal}>
        <ModalHeader border closeButton onHide={handleCloseModal}>
          <ModalTitle>{title}</ModalTitle>
        </ModalHeader>
        <ModalBody
          className={classnames('px-24 pt-24 pb-24 pb-lg-0', {
            loading
          })}
        >
          <div className="color-grey">
            {!showListVersion && (
              <p>
                <TransDLS keyTranslation={helpText1}>
                  <sup></sup>
                  <span className="fw-500 color-grey-d20"></span>
                </TransDLS>
              </p>
            )}
            {showListVersion && (
              <p>
                <TransDLS keyTranslation={helpText1}>
                  <sup></sup>
                  <span className="fw-500 color-grey-d20">
                    {{ tableId: listVersionTableId }}
                  </span>
                </TransDLS>
              </p>
            )}
          </div>
          <div className="mt-24">
            <div className="d-flex justify-content-between align-items-center mb-16">
              <h5>
                {t(
                  showListVersion
                    ? 'txt_manage_account_level_table_versions'
                    : 'txt_manage_transaction_level_tlp_config_tq_tableList'
                )}
              </h5>
              {(total > 0 || searchValue) && (
                <div className="method-list-simple-search">
                  <SimpleSearch
                    defaultValue={searchValue}
                    ref={simpleSearchRef}
                    clearTooltip={t('txt_clear_search_criteria')}
                    onSearch={handleSearch}
                    placeholder={t(
                      showListVersion
                        ? 'txt_manage_transaction_level_tlp_config_ca_simpleSearch_placeholder'
                        : 'txt_manage_transaction_level_tlp_config_tq_tableList_search_placeholder'
                    )}
                  />
                </div>
              )}
            </div>
            {total > 0 &&
              searchValue &&
              !isEmpty(dataListShow) &&
              !showListVersion && (
                <div className="d-flex justify-content-end mb-16 mr-n8">
                  <ClearAndResetButton
                    small
                    onClearAndReset={handleClearAndReset}
                  />
                </div>
              )}
            {total > 0 && showListVersion && (
              <div className="d-flex justify-content-end align-items-center mt-16">
                <SortOrder
                  dataFilter={dataFilter}
                  defaultDataFilter={defaultWorkflowMethodsFilter}
                  sortByFields={WORKFLOW_TLP_CA_SORT_BY_LIST}
                  onChangeOrderBy={handleChangeOrderBy}
                  onChangeSortBy={handleChangeSortBy}
                  onClearSearch={handleClearAndReset}
                  hasFilter={!!searchValue}
                />
              </div>
            )}
            {total > 0 && !showListVersion && (
              <div className="d-flex justify-content-end align-items-center">
                <div className="d-flex align-items-center">
                  <div className="d-flex align-items-center mr-n8">
                    <strong className="fs-14 color-grey mr-4">Order by:</strong>
                    <DropdownList
                      name="orderBy"
                      value={orderBy}
                      textField="description"
                      onChange={event => {
                        const orderBy = event.target.value;
                        handleChangeOrderBy(orderBy?.value);
                      }}
                      variant="no-border"
                    >
                      {orderByListItems}
                    </DropdownList>
                  </div>
                </div>
              </div>
            )}
          </div>
          {tableList}
          {noDataFound}
          {failedReload}
        </ModalBody>

        <ModalFooter
          cancelButtonText={t('txt_cancel')}
          okButtonText={t('txt_continue')}
          onCancel={handleCloseModal}
          disabledOk={!selectedVersion}
          onOk={handleGoToNextStep()}
        />
      </ModalRegistry>

      {versionDetail && (
        <ViewVersionDetail
          onCancel={handleCloseModalVersionDetail}
          tableId={versionDetail?.tableId}
          onOK={handleGoToNextStep(versionDetail)}
          tableType={tableType}
          data={versionDetail?.version}
        />
      )}
    </React.Fragment>
  );
};

export default TableListModal;
