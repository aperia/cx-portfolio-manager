import { nanoid } from 'nanoid';
import { AddNewTableAction } from './AddNewTableModal';
import { getControlParameters, TableType } from './constant';
import { ElementItem } from './type';

export const getTableActionType = (
  modeledFrom: Record<string, any> | undefined,
  tableId: string
) => {
  if (!modeledFrom) return AddNewTableAction.createNewTable;
  if (tableId === modeledFrom?.tableId)
    return AddNewTableAction.createNewVersion;
  return AddNewTableAction.addTableToModel;
};

export const mappingControlParameterValues = (
  tableType: TableType,
  values = [] as Record<string, any>[]
) => {
  const defaultControlParameters = getControlParameters(tableType);
  return defaultControlParameters.map((parameter: any) => {
    const matchItem = values.find((item: any) => item.name === parameter.id);
    return matchItem ? { ...parameter, value: matchItem?.value } : parameter;
  });
};

export const mappingElementListInfo = (
  elementList: ElementItem[],
  elementMetaData: any
) => {
  return elementList
    .map(el => {
      const matchElement =
        elementMetaData.find((element: any) => element.name === el.name) || {};
      return {
        ...matchElement,
        ...el,
        rowId: nanoid()
      };
    })
    .filter(el => el.configurable);
};
