import { render, RenderResult } from '@testing-library/react';
import 'app/utils/_mockComponent/mockUseTranslation';
import React from 'react';
import TableListSubGrid from './TableListSubGrid';

jest.mock('./ElementListDataGrid', () => ({
  __esModule: true,
  default: () => <div>ElementListDataGrid_Component</div>
}));

jest.mock('./ControlParametersDataGrid', () => ({
  __esModule: true,
  default: () => <div>ControlParametersDataGrid_Component</div>
}));

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <TableListSubGrid {...props} />
    </div>
  );
};

describe('WorkflowManageTransactionLevelProcessingDecisionTablesTLP > ConfigureParameters > TableListSubGrid', () => {
  const elementList = [] as any[];
  const controlParameters = [] as any[];

  const defaultProps = {
    elementList,
    controlParameters
  };
  it('Should render Table List SubGrid ', () => {
    const wrapper = renderComponent(defaultProps);

    expect(
      wrapper.getByText('txt_manage_transaction_level_tlp_element_list')
    ).toBeInTheDocument();
  });

  it('Should render Table List SubGrid with controlparameters values ', () => {
    const wrapper = renderComponent({
      ...defaultProps,
      controlParameters: ['1', '2']
    });

    expect(
      wrapper.getByText('txt_manage_account_level_table_parameters')
    ).toBeInTheDocument();
  });

  it('Should render Table List SubGrid with default data ', () => {
    const wrapper = renderComponent({});

    expect(
      wrapper.getByText('txt_manage_transaction_level_tlp_element_list')
    ).toBeInTheDocument();
  });
});
