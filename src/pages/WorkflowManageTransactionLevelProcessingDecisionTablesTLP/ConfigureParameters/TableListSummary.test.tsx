import { render, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import 'app/utils/_mockComponent/mockPaging';
import 'app/utils/_mockComponent/mockUseTranslation';
import * as CommonSelectHooks from 'pages/_commons/redux/Common/select-hooks';
import React from 'react';
import TableListSummary from './TableListSummary';

const mockUseSelectWindowDimension = jest.spyOn(
  CommonSelectHooks,
  'useSelectWindowDimension'
);

jest.mock('./TableListSubGrid', () => ({
  __esModule: true,
  default: () => <div>TableList_SubRowGrid_component</div>
}));

const MockTableList = [
  {
    rowId: 'gTHW7hyGq2ubw826Kkdka',
    tableId: '1',
    tableName: 'dsadas',
    elementList: [],
    comment: '',
    actionTaken: 'createNewTable',
    selectedVersionId: '',
    selectedTableId: ''
  },
  {
    rowId: '84j6GYkMJrJWo983pyRsZ',
    tableId: '30D66C93',
    tableName: '2',
    elementList: [],
    comment: 'dsada',
    actionTaken: 'createNewVersion',
    selectedVersionId: 'I5IIY2P8',
    selectedTableId: '30D66C93',
    selectedVersion: {
      tableId: '30D66C93',
      version: {
        tableId: 'I5IIY2P8',
        tableName: '5885E54F',
        effectiveDate: '2021-08-10T17:00:00Z',
        status: 'Lapsed',
        comment: 'velit dignissim',
        tableControlParameters: [],
        decisionElements: []
      }
    }
  },
  {
    rowId: 'l0LqFVvrRYpK0zEZyXPeI',
    tableId: '3',
    tableName: '3',
    elementList: [],
    comment: 'dsadas',
    actionTaken: 'addTableToModel',
    selectedVersionId: 'VSJQCLTR',
    selectedTableId: '30D66C93',
    selectedVersion: {
      tableId: '30D66C93',
      version: {
        tableId: 'VSJQCLTR',
        tableName: 'J45SZ6B6',
        effectiveDate: '2021-04-20T17:00:00Z',
        status: 'Scheduled',
        comment: 'tempor dolor',
        tableControlParameters: [],
        decisionElements: []
      }
    }
  },
  {
    rowId: 'MxLtWDrMqRMr3uMgZwzlT',
    tableId: '4',
    tableName: '4',
    elementList: [],
    comment: '',
    actionTaken: 'createNewTable',
    selectedVersionId: '',
    selectedTableId: '',
    selectedVersion: null
  },
  {
    rowId: 'sRGC9hflC7uLYYpY7Gpnv',
    tableId: '5',
    tableName: '5',
    elementList: [],
    comment: '',
    actionTaken: 'createNewTable',
    selectedVersionId: '',
    selectedTableId: '',
    selectedVersion: null
  },
  {
    rowId: 'FLuu2Uike6m_-D2Kk1CLU',
    tableId: '6',
    tableName: '6',
    elementList: [],
    comment: 'dsadsa',
    actionTaken: 'createNewTable',
    selectedVersionId: '',
    selectedTableId: '',
    selectedVersion: null
  },
  {
    rowId: 'mZnE9oVKlnJsRg6OmL7qJ',
    tableId: '7',
    tableName: '7',
    elementList: [],
    comment: 'dsadsa',
    actionTaken: 'createNewTable',
    selectedVersionId: '',
    selectedTableId: '',
    selectedVersion: null
  },
  {
    rowId: 'wjULM3uigGe_M3wQglXEY',
    tableId: '8',
    tableName: '8',
    elementList: [],
    comment: 'sadsadas',
    actionTaken: 'createNewTable',
    selectedVersionId: '',
    selectedTableId: '',
    selectedVersion: null
  },
  {
    rowId: '5YThH6o-JHRbrU7uG-n_C',
    tableId: '9',
    tableName: '9',
    elementList: [],
    comment: 'dsadsadsa',
    actionTaken: 'createNewTable',
    selectedVersionId: '',
    selectedTableId: '',
    selectedVersion: null
  },
  {
    rowId: '6C732qqSD-nSKt_oNpkFz',
    tableId: 'ewqewq',
    tableName: 'sadsa',
    elementList: [],
    comment: '',
    actionTaken: 'createNewTable',
    selectedVersionId: '',
    selectedTableId: '',
    selectedVersion: null
  },
  {
    rowId: 'U_7PTpR6yBbgUQbhqXod7',
    tableId: 'sadsa',
    tableName: 'dsada',
    elementList: [],
    comment: '',
    actionTaken: 'createNewTable',
    selectedVersionId: '',
    selectedTableId: '',
    selectedVersion: null
  }
] as any[];

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <TableListSummary {...props} />
    </div>
  );
};

describe('WorkflowManageTransactionLevelProcessingDecisionTablesTLP > ConfigureParameters > TableListSummary', () => {
  const tableList = [...MockTableList];

  const defaultProps = {
    tableList
  };

  it('Should render Table List Summary', () => {
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1920,
      height: 1080
    }));

    const { rerender, baseElement } = render(
      <TableListSummary tableList={MockTableList} />
    );

    const body = baseElement.querySelector('tbody[class="dls-grid-body"]');
    //SHOW Add new method modal
    const expandBtn = body?.children[0].querySelector(
      'button[class="btn btn-icon-secondary btn-sm"]'
    ) as Element;

    userEvent.click(expandBtn);

    rerender(<TableListSummary tableList={MockTableList} />);
  });

  it('should render Table List Summary with subGrid', () => {
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1200,
      height: 1080
    }));
    const wrapper = renderComponent({
      ...defaultProps,
      tableIndexChanged: 2,
      expandedList: { '84j6GYkMJrJWo983pyRsZ': true }
    });
    userEvent.click(
      wrapper.baseElement.querySelector(
        'button[class="change-page-number"]'
      ) as Element
    );

    userEvent.click(
      wrapper.baseElement.querySelector(
        'button[class="change-page-size"]'
      ) as Element
    );
  });

  it('Should render Table List SubGrid ', () => {
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1920,
      height: 1080
    }));
    render(<TableListSummary tableList={MockTableList} />);
  });
});
