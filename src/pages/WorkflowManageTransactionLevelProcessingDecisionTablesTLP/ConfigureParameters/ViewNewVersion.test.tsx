import { render, RenderResult } from '@testing-library/react';
import { act } from '@testing-library/react-hooks';
import userEvent from '@testing-library/user-event';
import 'app/utils/_mockComponent/mockUseTranslation';
import React from 'react';
import ViewNewVersion from './ViewNewVersion';

jest.mock('./ElementListDataGrid', () => ({
  __esModule: true,
  default: () => <div>ElementListDataGrid_Component</div>
}));

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <ViewNewVersion {...props} />
    </div>
  );
};

const DataTable = {
  rowId: 'X9zKlAjSfnALKnIv1CGoq',
  tableId: 'HUW2UWGL',
  tableName: 'DSADA',
  elementList: [],
  comment: '',
  actionTaken: 'createNewVersion',
  selectedVersionId: 'XLUB8XPN',
  selectedTableId: 'HUW2UWGL',
  selectedVersion: {
    tableId: 'HUW2UWGL',
    version: {
      tableId: 'XLUB8XPN',
      tableName: 'F1EGSCUZ',
      effectiveDate: '2022-01-31T17:00:00Z',
      status: 'Validating',
      comment: 'nostrud aliquyam',
      tableControlParameters: [],
      decisionElements: []
    }
  },
  controlParameters: []
};

describe('WorkflowManageTransactionLevelProcessingDecisionTablesTLP > ConfigureParameters > ViewNewVersion', () => {
  const handleSetSelectedTable = jest.fn();
  const handleOpenConfigModal = jest.fn();

  const defaultProps = {
    handleSetSelectedTable,
    handleOpenConfigModal,
    dataTableVersion: DataTable
  };

  it('It should render ViewNewVersion', () => {
    const wrapper = renderComponent(defaultProps);
    const editBtn = wrapper.getByText('txt_edit');
    const deleteBtn = wrapper.getByText('txt_delete');

    expect(
      wrapper.getByText('txt_manage_account_level_table_decision_element_list')
    ).toBeInTheDocument();
    act(() => {
      userEvent.click(editBtn);
    });
    expect(handleSetSelectedTable).toBeCalled();

    act(() => {
      userEvent.click(deleteBtn);
    });

    expect(handleSetSelectedTable).toBeCalled();
  });
});
