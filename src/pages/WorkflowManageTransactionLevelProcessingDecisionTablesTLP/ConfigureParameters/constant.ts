import { MethodFieldParameterEnum } from 'app/constants/enums';

export enum TableType {
  tlptq = 'TQ',
  tlpaq = 'AQ',
  tlpca = 'CA',
  tlprq = 'RQ'
}

enum BusinessName {
  nextDebitDate = 'Next Debit Date',
  includeActivityOptions = 'Include Activity Option',
  posPromotionValidation = 'POS Promotion Validation'
}

enum GreenScreenName {
  nextDebitDate = 'Next Debit Date',
  includeActivityOptions = 'INCL Activity Option',
  posPromoValidation = 'POS PROMO Validation'
}

export const getControlParameters = (type: TableType) => {
  switch (type) {
    case TableType.tlpaq:
      return [
        {
          id: MethodFieldParameterEnum.NextDebitDate,
          businessName: BusinessName.nextDebitDate,
          greenScreenName: GreenScreenName.nextDebitDate,
          moreInfo:
            'The Next Debit Date parameter defines the date (MM/DD/YYYY) when the next debit will post to the account.'
        },
        {
          id: MethodFieldParameterEnum.IncludeActivityAction,
          businessName: BusinessName.includeActivityOptions,
          greenScreenName: GreenScreenName.includeActivityOptions,
          moreInfo:
            'The Include Activity Option parameter indicates whether the System includes the cash advance transaction type, merchandise transaction type, or both transaction types in the table.'
        }
      ];
    case TableType.tlprq:
      return [
        {
          id: MethodFieldParameterEnum.PosPromotionValidation,
          businessName: BusinessName.posPromotionValidation,
          greenScreenName: GreenScreenName.posPromoValidation,
          value: 'N',
          moreInfo:
            'The POS Promotion Validation parameter designates whether you want to set the A/D/R IND and RESN CD fields on the Table Detail screen to indicate the result of the point-of-sale validation and provide a reason code.'
        }
      ];
    default:
      return [];
  }
};
