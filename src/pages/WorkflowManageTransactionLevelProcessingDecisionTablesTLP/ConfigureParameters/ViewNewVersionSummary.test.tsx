import { render, RenderResult } from '@testing-library/react';
import 'app/utils/_mockComponent/mockUseTranslation';
import React from 'react';
import ViewNewVersionSummary from './ViewNewVersionSummary';

jest.mock('./ElementListDataGrid', () => ({
  __esModule: true,
  default: () => <div>ElementListDataGrid_Component</div>
}));

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <ViewNewVersionSummary {...props} />
    </div>
  );
};

const DataTable = {
  rowId: 'X9zKlAjSfnALKnIv1CGoq',
  tableId: 'HUW2UWGL',
  tableName: 'DSADA',
  elementList: [],
  comment: '',
  actionTaken: 'createNewVersion',
  selectedVersionId: 'XLUB8XPN',
  selectedTableId: 'HUW2UWGL',
  selectedVersion: {
    tableId: 'HUW2UWGL',
    version: {
      tableId: 'XLUB8XPN',
      tableName: 'F1EGSCUZ',
      effectiveDate: '2022-01-31T17:00:00Z',
      status: 'Validating',
      comment: 'nostrud aliquyam',
      tableControlParameters: [],
      decisionElements: []
    }
  },
  controlParameters: []
};

describe('WorkflowManageTransactionLevelProcessingDecisionTablesTLP > ConfigureParameters > ViewNewVersionSummary', () => {
  const defaultProps = {
    dataTableVersion: DataTable
  };

  it('It should render ViewNewVersion', () => {
    const wrapper = renderComponent(defaultProps);

    expect(
      wrapper.getByText('txt_manage_account_level_table_decision_element_list')
    ).toBeInTheDocument();
  });
});
