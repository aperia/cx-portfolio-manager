import { render, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import 'app/utils/_mockComponent/mockNoDataFound';
import 'app/utils/_mockComponent/mockPaging';
import 'app/utils/_mockComponent/mockUseTranslation';
import * as CommonSelectHooks from 'pages/_commons/redux/Common/select-hooks';
import React from 'react';
import ElementList from './ElementList';
import * as hooks from './useElementList';

const mockUseSelectWindowDimension = jest.spyOn(
  CommonSelectHooks,
  'useSelectWindowDimension'
);

HTMLCanvasElement.prototype.getContext = jest.fn();
jest.mock('pages/_commons/Utils/ClearAndResetButton', () => ({
  __esModule: true,
  default: ({ onClearAndReset }: any) => (
    <button onClick={onClearAndReset}>{'Clear_And_Reset'}</button>
  )
}));

const SearchCodeOptions = [
  {
    code: 'E',
    text: 'E - Exact'
  },
  {
    code: 'R',
    text: 'R - Range'
  }
];

const ElementOptions = [
  {
    code: 'Test',
    text: 'Test'
  },
  {
    code: 'Test-1',
    text: 'Test-1'
  }
];

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <ElementList {...props} />
    </div>
  );
};

describe('WorkflowManageTransactionLevelProcessingDecisionTablesTLP > ConfigureParametersTQ > ElementList', () => {
  const handleSetTouchField = () => jest.fn();
  const handleChangeElementValue = () => jest.fn();
  const elementList = [{ name: 'text', searchCode: '123' }] as any[];
  const handleSetElementList = jest.fn();
  const handleSetElementListHasError = jest.fn();
  const handleDeleteElement = () => jest.fn();
  const handleChangeSearchValue = jest.fn();
  beforeEach(() => {
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1920,
      height: 1080
    }));
  });

  afterEach(() => {
    mockUseSelectWindowDimension.mockClear();
  });

  const mockUseElementData = {
    elements: elementList,
    searchCodeOptions: SearchCodeOptions,
    elementListNameOptionsCanSelect: ElementOptions,
    elementListNameOptions: ElementOptions,
    handleSetTouchField,
    handleChangeElementValue,
    handleDeleteElement
  } as any;

  it('Should render Element List With 1 element', () => {
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1000,
      height: 1080
    }));
    jest.spyOn(hooks, 'useElementList').mockImplementation(() => {
      return mockUseElementData;
    });
    const wrapper = renderComponent({
      elementList,
      handleSetElementList,
      handleSetElementListHasError
    });
    expect(
      wrapper.getByText(
        'txt_manage_transaction_level_tlp_config_tq_decision_element_list'
      )
    ).toBeInTheDocument();
  });

  it('Should render Element List With 2 element', () => {
    jest.spyOn(hooks, 'useElementList').mockImplementation(() => {
      return {
        elements: [
          {
            name: '',
            searchCode: '',
            rowId: '1',
            isNewElement: true
          },
          {
            name: 'test',
            searchCode: 'E',
            rowId: '2'
          }
        ],
        maximumElementCanAdd: 20,
        totalElement: 2,
        searchCodeOptions: SearchCodeOptions,
        elementListNameOptions: ElementOptions,
        handleSetTouchField,
        handleChangeElementValue,
        elementListNameOptionsCanSelect: ElementOptions,
        touches: { 2: { name: true, searchCode: true } },
        errors: { 2: { name: 'error', searchCode: 'error' } },
        handleDeleteElement,
        searchValue: 'search',
        handleChangeSearchValue
      } as any;
    });
    const wrapper = renderComponent({
      elementList,
      handleSetElementList,
      handleSetElementListHasError
    });
    expect(
      wrapper.getByText(
        'txt_manage_transaction_level_tlp_config_tq_decision_element_list'
      )
    ).toBeInTheDocument();
    const clearAndResetBtn = wrapper.getByText('Clear_And_Reset');
    userEvent.click(clearAndResetBtn);
    expect(handleChangeSearchValue).toBeCalled();
  });

  it('Should render No Data found', () => {
    jest.spyOn(hooks, 'useElementList').mockImplementation(() => {
      return {
        elements: [],
        searchValue: 'search',
        handleChangeSearchValue
      } as any;
    });
    const wrapper = renderComponent({
      elementList,
      handleSetElementList,
      handleSetElementListHasError
    });
    const noData = wrapper.getByText('No data found');
    expect(noData).toBeInTheDocument();
    userEvent.click(noData);
    expect(handleChangeSearchValue).toBeCalled();
  });

  it('Should render Element List with 20 Elements', () => {
    jest.spyOn(hooks, 'useElementList').mockImplementation(() => {
      return {
        elements: [
          {
            name: 'test',
            searchCode: 'E',
            rowId: '1'
          }
        ],
        totalElement: 20,
        maximumElementCanAdd: 20,
        searchValue: 'search',
        handleChangeSearchValue,
        elementListNameOptionsCanSelect: ElementOptions,
        searchCodeOptions: SearchCodeOptions,
        elementListNameOptions: ElementOptions,
        handleSetTouchField,
        handleChangeElementValue,
        touches: { 2: { name: true, searchCode: true } },
        errors: { 2: { name: 'error', searchCode: 'error' } },
        handleDeleteElement
      } as any;
    });
    const wrapper = renderComponent({
      elementList,
      handleSetElementList,
      handleSetElementListHasError
    });
    expect(
      wrapper.getByText(
        'txt_manage_transaction_level_tlp_config_tq_decision_element_list'
      )
    ).toBeInTheDocument();
  });

  it('Should render Element List with hide delete button', () => {
    jest.spyOn(hooks, 'useElementList').mockImplementation(() => {
      return {
        elements: [
          {
            name: 'test',
            searchCode: 'E',
            rowId: '1'
          }
        ],
        totalElement: 20,
        maximumElementCanAdd: 20,
        searchValue: 'search',
        handleChangeSearchValue,
        elementListNameOptionsCanSelect: ElementOptions,
        searchCodeOptions: SearchCodeOptions,
        elementListNameOptions: ElementOptions,
        handleSetTouchField,
        handleChangeElementValue,
        touches: { 2: { name: true, searchCode: true } },
        errors: { 2: { name: 'error', searchCode: 'error' } },
        handleDeleteElement,
        isHideDeleteBtn: true
      } as any;
    });
    const wrapper = renderComponent({
      elementList,
      handleSetElementList,
      handleSetElementListHasError
    });
    expect(
      wrapper.getByText(
        'txt_manage_transaction_level_tlp_config_tq_decision_element_list'
      )
    ).toBeInTheDocument();
  });
});
