import { render, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import 'app/utils/_mockComponent/mockNoDataFound';
import 'app/utils/_mockComponent/mockPaging';
import 'app/utils/_mockComponent/mockSimpleSearch';
import 'app/utils/_mockComponent/mockUseTranslation';
import * as CommonSelectHooks from 'pages/_commons/redux/Common/select-hooks';
import React from 'react';
import { act } from 'react-dom/test-utils';
import { getControlParameters, TableType } from './constant';
import ControlParameters from './ControlParameters';
import * as hooks from './useControlParameters';

const mockUseSelectWindowDimension = jest.spyOn(
  CommonSelectHooks,
  'useSelectWindowDimension'
);

HTMLCanvasElement.prototype.getContext = jest.fn();
jest.mock('pages/_commons/Utils/ClearAndResetButton', () => ({
  __esModule: true,
  default: ({ onClearAndReset }: any) => (
    <button onClick={onClearAndReset}>{'Clear_And_Reset'}</button>
  )
}));

const MockIncludeActivityOptions = [{ code: 'N', text: 'test' }];
const MockPosPromoValidationOptions = [{ code: 'N', text: 'test' }];

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <ControlParameters {...props} />
    </div>
  );
};

describe('WorkflowManageTransactionLevelProcessingDecisionTablesTLP > ConfigureParameters > ControlParameters', () => {
  const handleSetTouchField = () => jest.fn();
  const handleChangeElementValue = () => jest.fn();
  const handleChangeSearchValue = jest.fn();
  const handleChangeParametersValue = () => jest.fn();

  const controlParameters = getControlParameters(TableType.tlpaq);

  beforeEach(() => {
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1920,
      height: 1080
    }));
  });

  afterEach(() => {
    mockUseSelectWindowDimension.mockClear();
  });

  it('Should render TLP-AQ Control parameters list', () => {
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1920,
      height: 1080
    }));
    jest.spyOn(hooks, 'useControlParameters').mockImplementation(() => {
      return {
        handleSetTouchField,
        handleChangeElementValue,
        isHideSearchBox: false,
        includeActivityOptions: MockIncludeActivityOptions,
        posPromoValidationOptions: MockPosPromoValidationOptions,
        parameters: controlParameters,
        handleChangeParametersValue,
        handleChangeSearchValue,
        searchValue: '123'
      } as any;
    });
    const wrapper = renderComponent({});
    expect(
      wrapper.getByText('txt_manage_account_level_table_parameters')
    ).toBeInTheDocument();

    expect(wrapper.getByText('Simple Search')).toBeInTheDocument();
    const clearAndResetBtn = wrapper.getByText('Clear_And_Reset');

    act(() => {
      userEvent.click(clearAndResetBtn);
    });
    expect(handleChangeSearchValue).toBeCalled();
  });

  it('Should render TLP-RQ Control parameters list', () => {
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1000,
      height: 1080
    }));
    jest.spyOn(hooks, 'useControlParameters').mockImplementation(() => {
      return {
        handleSetTouchField,
        handleChangeElementValue,
        isHideSearchBox: false,
        includeActivityOptions: MockIncludeActivityOptions,
        posPromoValidationOptions: MockPosPromoValidationOptions,
        parameters: getControlParameters(TableType.tlprq),
        handleChangeParametersValue,
        searchValue: '123'
      } as any;
    });
    const wrapper = renderComponent({});
    expect(
      wrapper.getByText('txt_manage_account_level_table_parameters')
    ).toBeInTheDocument();

    expect(wrapper.getByText('Simple Search')).toBeInTheDocument();
  });

  it('Should render Empty Control parameters list', () => {
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1000,
      height: 1080
    }));
    jest.spyOn(hooks, 'useControlParameters').mockImplementation(() => {
      return {
        handleSetTouchField,
        handleChangeElementValue,
        isHideSearchBox: false,
        includeActivityOptions: MockIncludeActivityOptions,
        posPromoValidationOptions: MockPosPromoValidationOptions,
        parameters: [],
        handleChangeParametersValue,
        searchValue: '123',
        handleChangeSearchValue
      } as any;
    });
    const wrapper = renderComponent({});
    const noDataFound = wrapper.getByText('No data found');
    expect(noDataFound).toBeInTheDocument();

    act(() => {
      userEvent.click(noDataFound);
    });
    expect(handleChangeSearchValue).toBeCalled();
  });

  it('Should render TLP-AQ Control parameters list with date value', () => {
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1000,
      height: 1080
    }));
    jest.spyOn(hooks, 'useControlParameters').mockImplementation(() => {
      return {
        handleSetTouchField,
        handleChangeElementValue,
        isHideSearchBox: false,
        includeActivityOptions: MockIncludeActivityOptions,
        posPromoValidationOptions: MockPosPromoValidationOptions,
        parameters: [...getControlParameters(TableType.tlpaq)].map(control => ({
          ...control,
          value: '22/11/2022'
        })),
        handleChangeParametersValue
      } as any;
    });
    const wrapper = renderComponent({});
    expect(
      wrapper.getByText('txt_manage_account_level_table_parameters')
    ).toBeInTheDocument();
  });
});
