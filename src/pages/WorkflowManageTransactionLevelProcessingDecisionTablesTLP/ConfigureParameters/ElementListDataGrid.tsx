import { ColumnType, Grid, useTranslation } from 'app/_libraries/_dls';
import { useSelectElementMetadataManageTransactionTLP } from 'pages/_commons/redux/WorkflowSetup';
import { formatText, viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React from 'react';
import { ElementItem } from './type';

interface IElementListDataGrid {
  elementList: ElementItem[];
}

const ElementListDataGrid: React.FC<IElementListDataGrid> = ({
  elementList = []
}) => {
  const { t } = useTranslation();
  const metadata = useSelectElementMetadataManageTransactionTLP();

  const { searchCode: searchCodeOptions } = metadata;

  const renderSearchCode = (item: any) => {
    const { searchCode } = item;
    const matchOptions = searchCodeOptions.find(
      (op: any) => op.code === searchCode
    );
    return matchOptions?.text;
  };

  const columnsElementList: ColumnType[] = [
    {
      id: 'name',
      Header: t('txt_manage_transaction_level_tlp_decision_element'),
      accessor: formatText(['name'])
    },
    {
      id: 'searchCode',
      Header: t('txt_manage_transaction_level_tlp_config_tq_search_code'),
      accessor: renderSearchCode,
      width: 210
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      accessor: viewMoreInfo(['moreInfo'], t),
      width: 106
    }
  ];

  return <Grid columns={columnsElementList} data={elementList} />;
};

export default ElementListDataGrid;
