import EnhanceDropdownList from 'app/components/EnhanceDropdownList';
import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import {
  Button,
  ColumnType,
  ComboBox,
  Grid,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import get from 'lodash.get';
import isEmpty from 'lodash.isempty';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import ClearAndResetButton from 'pages/_commons/Utils/ClearAndResetButton';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React, { useCallback, useEffect, useRef } from 'react';
import { TableType } from './constant';
import { ElementItem } from './type';
import { useElementList } from './useElementList';

export interface IElementList {
  elementList: ElementItem[];
  handleSetElementList: (elementList: ElementItem[]) => void;
  handleSetElementListHasError: (isElementListHasError: boolean) => void;
  tableType: TableType;
}

const ElementList: React.FC<IElementList> = props => {
  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();
  const {
    elements,
    handleChangeElementValue,
    touches,
    errors,
    handleSetTouchField,
    elementListNameOptions,
    handleDeleteElement,
    handleAddNewElement,
    searchValue,
    handleChangeSearchValue,
    totalElement,
    handleDnDDataChange,
    searchCodeOptions,
    tooltipDragList,
    elementListNameOptionsCanSelect,
    maximumElementCanAdd,
    isHideDeleteBtn
  } = useElementList(props);

  const simpleSearchRef = useRef<any>(null);
  useEffect(() => {
    if (!searchValue && simpleSearchRef?.current) {
      simpleSearchRef.current.clear();
    }
  }, [searchValue]);

  const renderCombobox = useCallback(
    (fieldName: string) => (item: any) => {
      const options = elementListNameOptions;
      const value = options.find(
        (option: any) => option.code === item[fieldName]
      );
      const rowId = item?.rowId;

      return (
        <ComboBox
          placeholder={t(
            'txt_workflow_manage_change_in_terms_select_an_option'
          )}
          size="small"
          textField="text"
          onBlur={handleSetTouchField(fieldName, rowId)}
          error={{
            status: Boolean(
              get(touches, [rowId, fieldName]) &&
                get(errors, [rowId, fieldName])
            ),
            message: get(errors, [rowId, fieldName], '')
          }}
          value={value}
          onChange={handleChangeElementValue(fieldName, item?.rowId)}
          noResult={t('txt_no_results_found_without_dot')}
        >
          {elementListNameOptionsCanSelect.map((item: any) => (
            <ComboBox.Item key={item.code} value={item} label={item.text} />
          ))}
        </ComboBox>
      );
    },
    [
      t,
      handleChangeElementValue,
      handleSetTouchField,
      errors,
      touches,
      elementListNameOptions,
      elementListNameOptionsCanSelect
    ]
  );

  const renderDropDownItem = useCallback(
    fieldName => (item: any) => {
      if (item.isNewElement) return <span />;
      const options = searchCodeOptions;
      const value = options.find(option => option.code === item[fieldName]);
      const rowId = item?.rowId;
      return (
        <EnhanceDropdownList
          placeholder={t(
            'txt_workflow_manage_change_in_terms_select_an_option'
          )}
          dataTestId={`addNewElement__`}
          size="small"
          value={value}
          required
          onBlur={handleSetTouchField(fieldName, rowId)}
          error={{
            status: Boolean(
              get(touches, [rowId, fieldName]) &&
                get(errors, [rowId, fieldName])
            ),
            message: get(errors, [rowId, fieldName], '')
          }}
          onChange={handleChangeElementValue(fieldName, item?.rowId)}
          options={options}
        />
      );
    },
    [
      t,
      handleChangeElementValue,
      handleSetTouchField,
      errors,
      touches,
      searchCodeOptions
    ]
  );

  const columns: ColumnType[] = [
    {
      id: 'name',
      Header: t('txt_manage_transaction_level_tlp_config_tq_decision_element'),
      accessor: renderCombobox('name'),
      cellBodyProps: { className: 'py-8' },
      width: width > 1280 ? 420 : 280,
      required: true
    },
    {
      id: 'searchCode',
      Header: t('txt_manage_transaction_level_tlp_config_tq_search_code'),
      accessor: renderDropDownItem('searchCode'),
      cellBodyProps: { className: 'py-8' },
      width: width > 1280 ? 240 : 210,
      required: true
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      accessor: viewMoreInfo(['moreInfo'], t),
      width: 106,
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'action',
      Header: 'Action',
      className: 'text-center',
      accessor: (item: any) => {
        return isHideDeleteBtn ? (
          <span />
        ) : (
          <Button
            size="sm"
            variant="outline-danger"
            onClick={handleDeleteElement(item.rowId)}
          >
            {t('txt_remove')}
          </Button>
        );
      },
      width: 110,
      cellBodyProps: { className: 'py-8' }
    }
  ];
  return (
    <div className="mt-24">
      <div className="d-flex align-items-center justify-content-between mb-16">
        <h5>
          {t(
            'txt_manage_transaction_level_tlp_config_tq_decision_element_list'
          )}
        </h5>

        <SimpleSearch
          ref={simpleSearchRef}
          placeholder={t(
            'txt_manage_transaction_level_tlp_config_tq_search_element_placeholder'
          )}
          onSearch={handleChangeSearchValue}
          defaultValue={searchValue}
        />
      </div>
      <div className="d-flex justify-content-end mt-16 mr-n8">
        {totalElement === maximumElementCanAdd ? (
          <Tooltip
            element={t(
              'txt_manage_transaction_level_tlp_config_tq_maximum_element'
            )}
          >
            <Button size="sm" variant="outline-primary" disabled>
              {t('txt_manage_transaction_level_tlp_config_tq_add_new_element')}
            </Button>
          </Tooltip>
        ) : (
          <Button
            size="sm"
            variant="outline-primary"
            onClick={handleAddNewElement}
          >
            {t('txt_manage_transaction_level_tlp_config_tq_add_new_element')}
          </Button>
        )}
        {searchValue && !isEmpty(elements) && (
          <div className="ml-8">
            <ClearAndResetButton
              small
              onClearAndReset={() => handleChangeSearchValue('')}
            />
          </div>
        )}
      </div>
      {isEmpty(elements) && searchValue && (
        <div className="d-flex flex-column justify-content-center mt-40 mb-32 mb-xl-0">
          <NoDataFound
            id="newMethod_notfound"
            hasSearch
            title={t('txt_no_results_found')}
            linkTitle={t('txt_clear_and_reset')}
            onLinkClicked={() => handleChangeSearchValue('')}
          />
        </div>
      )}
      {!isEmpty(elements) && (
        <div className="mt-16 mb-24 mb-xl-0">
          <Grid
            columns={columns}
            data={elements}
            variant={{
              type: 'dnd',
              id: 'rowId',
              cellBodyProps: { className: 'text-center' }
            }}
            onDataChange={handleDnDDataChange}
            dnd
            dataItemKey={'rowId'}
            dndReadOnly={Boolean(searchValue)}
            dndButtonConfigList={tooltipDragList}
          />
        </div>
      )}
    </div>
  );
};

export default ElementList;
