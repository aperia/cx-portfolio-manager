import { getWorkflowSetupStepStatus } from 'app/helpers';
import get from 'lodash.get';
import { nanoid } from 'nanoid';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import { TableType } from './constant';
import {
  getTableActionType,
  mappingControlParameterValues,
  mappingElementListInfo
} from './helper';
import { ConfigTableItem } from './type';

const parseFormValues: WorkflowSetupStepFormDataFunc<any> = async (
  data,
  id,
  dispatch
) => {
  const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);
  if (!stepInfo) return;
  let tableType: TableType;
  switch (id) {
    case 'configTlpTq':
      tableType = TableType.tlptq;
      break;
    case 'configTlpAq':
      tableType = TableType.tlpaq;
      break;
    case 'configTlpRq':
      tableType = TableType.tlprq;
      break;
    default:
      tableType = TableType.tlpca;
  }

  const result = (await Promise.resolve(
    dispatch(
      actionsWorkflowSetup.getDecisionElementList({
        tableType,
        serviceNameAbbreviation: 'TLP'
      })
    )
  )) as any;

  const decisionElements = get(result, ['payload', 'decisionElementList'], []);

  const tables = (data.data.configurations?.tableList || [])
    .filter((table: any) => table.tableType === tableType)
    .map(
      (table: any) =>
        ({
          ...table,
          id: '',
          rowId: nanoid(),
          selectedVersion: table.modeledFrom
            ? {
                tableId: table?.modeledFrom?.tableId,
                tableName: table?.modeledFrom?.tableName,
                version: { tableId: table?.modeledFrom?.version?.tableId }
              }
            : null,
          selectedTableId: table?.modeledFrom
            ? table?.modeledFrom?.tableId
            : null,
          selectedVersionId: table?.modeledFrom
            ? table?.modeledFrom?.version?.tableId
            : null,
          elementList: mappingElementListInfo(
            table?.decisionElements ?? [],
            decisionElements
          ),
          actionTaken: getTableActionType(table?.modeledFrom, table?.tableId),
          controlParameters: mappingControlParameterValues(
            tableType,
            table?.tableControlParameters
          )
        } as ConfigTableItem)
    );

  return {
    ...getWorkflowSetupStepStatus(stepInfo),
    isValid: tables.length > 0,
    tables,
    tableType: tableType
  };
};

export default parseFormValues;
