import { render, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { mockUseModalRegistryFnc } from 'app/utils';
import 'app/utils/_mockComponent/mockFailedApiReload';
import 'app/utils/_mockComponent/mockNoDataFound';
import 'app/utils/_mockComponent/mockPaging';
import 'app/utils/_mockComponent/mockSortOrder';
import React from 'react';
import TableListModal from './TableListModal';
import * as hooks from './useTableListModal';

const mockUseModalRegistry = mockUseModalRegistryFnc();

jest.mock('app/_libraries/_dls/components/i18next/Trans', () => {
  return {
    __esModule: true,
    default: (props: any) => {
      const { keyTranslation } = props || {};
      return <div>{keyTranslation}</div>;
    }
  };
});

jest.mock('app/_libraries/_dls/components/DropdownList', () => {
  const MockDropDown = ({ onChange }: any) => {
    return (
      <div
        onClick={() =>
          onChange({
            target: {
              value: {
                value: 'value',
                code: 'code'
              }
            }
          })
        }
      >
        MockDropDown
      </div>
    );
  };

  MockDropDown.Item = () => <div>Mock Drop down item</div>;
  return {
    __esModule: true,
    default: MockDropDown
  };
});

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const TableListMockData = [
  {
    tableId: 'LUMEJ598',
    tableName: '8A41RDTB',
    versions: [
      {
        tableId: 'IFAA8XEE',
        tableName: 'WFRKLMZB',
        effectiveDate: '2021-05-31T17:00:00Z',
        status: 'Production',
        comment: 'at tempor accusam',
        tableControlParameters: [],
        decisionElements: []
      },
      {
        tableId: 'K1TIBA4C',
        tableName: 'ZHFMK7K2',
        effectiveDate: '2021-05-28T17:00:00Z',
        status: 'Client Approved',
        comment: 'eu labore congue ea',
        tableControlParameters: [],
        decisionElements: []
      },
      {
        tableId: 'WMAEVM7G',
        tableName: '91IDUIVL',
        effectiveDate: '2021-10-17T17:00:00Z',
        status: 'Validating',
        comment: 'nulla aliquip cum magna iriure ex velit',
        tableControlParameters: [],
        decisionElements: [{ name: 'Name', searchCode: 'searchCode' }]
      }
    ]
  },
  {
    tableId: 'LUMEJ5F',
    tableName: '8A41RDTH',
    versions: [
      {
        tableId: 'IFAA8XEE',
        tableName: 'WFRKLMZB',
        effectiveDate: '2021-05-31T17:00:00Z',
        status: 'Production',
        comment: 'at tempor accusam',
        tableControlParameters: [],
        decisionElements: []
      },
      {
        tableId: 'K1TIBA4DD',
        tableName: 'ZHFMK7EE',
        effectiveDate: '2021-05-28T17:00:00Z',
        status: 'Client Approved',
        comment: 'eu labore congue ea',
        tableControlParameters: [],
        decisionElements: []
      },
      {
        tableId: 'WMAEVMKK',
        tableName: '91IDUIVL',
        effectiveDate: '2021-10-17T17:00:00Z',
        status: 'Validating',
        comment: 'nulla aliquip cum magna iriure ex velit',
        tableControlParameters: [],
        decisionElements: []
      }
    ]
  }
];

const SelectedVersion = {
  tableId: 'LUMEJ598',
  version: {
    tableId: 'IFAA8XEE',
    tableName: 'WFRKLMZB',
    effectiveDate: '2021-05-31T17:00:00Z',
    status: 'Production',
    comment: 'at tempor accusam',
    tableControlParameters: [],
    decisionElements: [{ name: 'Name', searchCode: 'searchCode' }]
  }
};

jest.mock('./ViewModal', () => ({
  __esModule: true,
  default: ({ onCancel, onOK }: any) => {
    return (
      <div>
        <h1>ViewModal_component</h1>
        <button onClick={onCancel}>Close_Modal_View_detail</button>
        <button onClick={onOK}>Go_To_Next_Step</button>
      </div>
    );
  }
}));

jest.mock('app/components/TableCardView', () => ({
  __esModule: true,
  default: ({ onSelectVersion, onViewVersionDetail, onToggle }: any) => {
    return (
      <div>
        <h1>Table Card View</h1>
        <button onClick={() => onSelectVersion(SelectedVersion)}>
          Select Version
        </button>
        <button onClick={() => onViewVersionDetail(SelectedVersion)}>
          View Version
        </button>
        <button onClick={onToggle}>Toggle View</button>
      </div>
    );
  }
}));

jest.mock('./VersionCardView', () => ({
  __esModule: true,
  default: ({ onSelectVersion, onViewVersionDetail, onToggle }: any) => {
    return (
      <div>
        <h1>Version Card View</h1>
        <button onClick={() => onSelectVersion(SelectedVersion)}>
          Select Version
        </button>
        <button onClick={() => onViewVersionDetail(SelectedVersion)}>
          View Version
        </button>
        <button onClick={onToggle}>Toggle View</button>
      </div>
    );
  }
}));

HTMLCanvasElement.prototype.getContext = jest.fn();
jest.mock('pages/_commons/Utils/ClearAndResetButton', () => ({
  __esModule: true,
  default: ({ onClearAndReset }: any) => (
    <button onClick={onClearAndReset}>{'Clear_And_Reset'}</button>
  )
}));

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <TableListModal {...props} />
    </div>
  );
};

describe('WorkflowManageTransactionLevelProcessingDecisionTablesTLP > ConfigureParameters > TableListModal', () => {
  const id = 'id';
  const show = true;
  const selectedVersion = null;
  const expandedList = {};
  const handleSetShowVersionDetail = jest.fn();
  const handleChangeOrderBy = jest.fn();
  const defaultProps = {
    id,
    show,
    selectedVersion,
    expandedList
  };

  beforeEach(() => {
    mockUseModalRegistry.mockImplementation(() => [true, false]);
  });

  afterEach(() => {
    mockUseModalRegistry.mockClear();
  });

  it('Should render Table List Modal with no data found', () => {
    jest.spyOn(hooks, 'useTableListModal').mockImplementation(() => {
      return {
        total: 0,
        searchValue: 'abc',
        handleGoToNextStep: () => jest.fn()
      } as any;
    });
    const wrapper = renderComponent(defaultProps);
    expect(
      wrapper.getByText('txt_manage_transaction_level_tlp_config_tq_tableList')
    ).toBeInTheDocument();

    const noDataFound = wrapper.getByText('No data found');
    expect(noDataFound).toBeInTheDocument();
  });

  it('Should render Table List Modal with APi fail component', () => {
    jest.spyOn(hooks, 'useTableListModal').mockImplementation(() => {
      return {
        total: 0,
        searchValue: 'abc',
        error: {},
        handleGoToNextStep: () => jest.fn()
      } as any;
    });
    const wrapper = renderComponent(defaultProps);
    const apiFailed = wrapper.getByText('Data load unsuccessful');
    expect(apiFailed).toBeInTheDocument();
  });

  it('Should render Table List', () => {
    jest.spyOn(hooks, 'useTableListModal').mockImplementation(() => {
      return {
        total: 2,
        searchValue: 'abc',
        dataListShow: TableListMockData,
        handleSetShowVersionDetail,
        handleSelectTableVersion: () => jest.fn(),
        handleGoToNextStep: () => jest.fn()
      } as any;
    });
    const wrapper = renderComponent({
      ...defaultProps,
      selectedVersion: SelectedVersion,
      isCreateNewVersion: true
    });
    const cardViewList = wrapper.getAllByText('Table Card View');
    expect(cardViewList.length).toBeGreaterThan(0);
  });

  it('Should render version List', () => {
    jest.spyOn(hooks, 'useTableListModal').mockImplementation(() => {
      return {
        total: 3,
        dataListShow: TableListMockData[0].versions,
        handleSetShowVersionDetail,
        listVersionTableId: '123',
        showListVersion: true,
        handleSelectTableVersion: () => jest.fn(),
        handleGoToNextStep: () => jest.fn()
      } as any;
    });
    const wrapper = renderComponent({
      ...defaultProps
    });
    const cardViewList = wrapper.getAllByText('Version Card View');
    expect(cardViewList.length).toBeGreaterThan(0);
  });

  it('Should Show modal version detail', () => {
    jest.spyOn(hooks, 'useTableListModal').mockImplementation(() => {
      return {
        total: 2,
        dataListShow: TableListMockData,
        versionDetail: SelectedVersion,
        handleSelectTableVersion: () => jest.fn(),
        handleGoToNextStep: () => jest.fn(),
        handleChangeOrderBy,
        showListVersion: false
      } as any;
    });
    const wrapper = renderComponent({
      ...defaultProps
    });
    const modalDetail = wrapper.getByText('ViewModal_component');
    expect(modalDetail).toBeInTheDocument();

    const mockDropDown = wrapper.getByText('MockDropDown');
    userEvent.click(mockDropDown);
    expect(handleChangeOrderBy).toBeCalled();
  });
});
