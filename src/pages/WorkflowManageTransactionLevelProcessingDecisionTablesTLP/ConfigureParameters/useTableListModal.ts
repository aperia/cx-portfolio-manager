import { ORDER_BY_LIST, PAGE_SIZE } from 'app/constants/constants';
import { OrderBy, WorkflowMethodsSortByFields } from 'app/constants/enums';
import {
  unsavedChangesProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { matchSearchValue } from 'app/helpers';
import { useUnsavedChangeRegistry, useUnsavedChangesRedirect } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls';
import { DEFAULT_PAGE_NUMBER } from 'app/_libraries/_dls/components/Pagination/constants';
import get from 'lodash.get';
import _orderBy from 'lodash.orderby';
import {
  actionsWorkflowSetup,
  useSelectDecisionElementListData,
  useSelectTableListData
} from 'pages/_commons/redux/WorkflowSetup';
import { useCallback, useEffect, useMemo, useReducer } from 'react';
import { useDispatch } from 'react-redux';
import { TableType } from './constant';
import {
  mappingControlParameterValues,
  mappingElementListInfo
} from './helper';
import { TableListModalProps } from './TableListModal';
import {
  ControlParameterItem,
  ElementItem,
  SelectedVersion,
  TableItem,
  TableVersion
} from './type';
import {
  CONFIG_ACTIONS_MODAL,
  EXPANDED_LIST,
  EXPAND_ACTION
} from './useConfigActions';

interface ITableListStateProps {
  page: number;
  pageSize: number;
  searchValue: string;
  orderBy: string;
  versionDetail: Record<string, any> | null;
  sortBy: string;
}

const TABLE_LIST_INITIAL_STATE = {
  page: DEFAULT_PAGE_NUMBER,
  pageSize: PAGE_SIZE[0],
  searchValue: '',
  orderBy: OrderBy.ASC,
  sortBy: WorkflowMethodsSortByFields.EFFECTIVE_DATE
} as ITableListStateProps;

const ACTION_TYPES = {
  CHANGE_PAGE: 'CHANGE_PAGE',
  CHANGE_PAGE_SIZE: 'CHANGE_PAGE_SIZE',
  CHANGE_SEARCH_VALUE: 'CHANGE_SEARCH_VALUE',
  CHANGE_ORDER_BY: 'CHANGE_ORDER_BY',
  SET_VERSION_DETAIL: 'SET_VERSION_DETAIL',
  CHANGE_SORT_BY: 'CHANGE_SORT_BY'
};

const tableListReducer = (state: ITableListStateProps, action: any) => {
  switch (action.type) {
    case ACTION_TYPES.CHANGE_PAGE:
      return { ...state, page: action.page };
    case ACTION_TYPES.CHANGE_PAGE_SIZE:
      return { ...state, pageSize: action.pageSize };
    case ACTION_TYPES.CHANGE_SEARCH_VALUE:
      return {
        ...state,
        searchValue: action.searchValue,
        page: DEFAULT_PAGE_NUMBER
      };
    case ACTION_TYPES.CHANGE_ORDER_BY:
      return {
        ...state,
        orderBy: action.orderBy,
        page: DEFAULT_PAGE_NUMBER
      };
    case ACTION_TYPES.SET_VERSION_DETAIL:
      return {
        ...state,
        versionDetail: action.versionDetail
      };
    case ACTION_TYPES.CHANGE_SORT_BY:
      return {
        ...state,
        sortBy: action.sortBy,
        page: DEFAULT_PAGE_NUMBER
      };
    default:
      return state;
  }
};

export const useTableListModal = (tableListProps: TableListModalProps) => {
  const {
    handleClose,
    selectedVersion,
    handleSetExpandedList,
    handleSelectVersion,
    handleOpenConfigModal,
    isCreateNewVersion,
    handleSetIsCreateNewVersion,
    draftNewTable,
    handleSetDraftNewTable,
    tableType
  } = tableListProps;

  const hasControlParameters = useMemo(() => {
    return tableType === TableType.tlpaq;
  }, [tableType]);

  const showListVersion = useMemo(() => {
    return tableType === TableType.tlpca;
  }, [tableType]);

  const dispatch = useDispatch();
  const { t } = useTranslation();
  const redirect = useUnsavedChangesRedirect();

  const data = useSelectDecisionElementListData(`TLP${tableType}`);
  const decisionElementList: ElementItem[] =
    data?.decisionElementList || ([] as ElementItem[]);

  const isFormChanging = useMemo(() => {
    if (
      !draftNewTable ||
      (draftNewTable &&
        draftNewTable.selectedVersionId !== selectedVersion?.version?.tableId)
    ) {
      return false;
    }
    return draftNewTable.isFormChanging;
  }, [selectedVersion, draftNewTable]);

  const initialState = useMemo(() => {
    return showListVersion
      ? { ...TABLE_LIST_INITIAL_STATE, orderBy: OrderBy.DESC }
      : { ...TABLE_LIST_INITIAL_STATE };
  }, [showListVersion]);

  const [state, reactDispatch] = useReducer(tableListReducer, initialState);

  const { page, pageSize, searchValue, orderBy, versionDetail, sortBy } = state;

  const {
    tableList = [] as TableItem[],
    loading,
    error
  } = useSelectTableListData() || {};

  const dataList = showListVersion
    ? (tableList[0]?.versions as TableVersion[]) || ([] as TableVersion[])
    : (tableList as TableItem[]);
  const listVersionTableId = showListVersion ? tableList[0]?.tableId : null;

  const handleGetTableList = useCallback(() => {
    dispatch(
      actionsWorkflowSetup.getTableList({
        tableType,
        serviceNameAbbreviation: 'TLP'
      })
    );
  }, [dispatch, tableType]);

  useEffect(() => {
    handleGetTableList();
  }, [handleGetTableList]);

  const handleChangePage = useCallback((page: number) => {
    reactDispatch({ type: ACTION_TYPES.CHANGE_PAGE, page });
  }, []);

  const handleChangePageSize = useCallback(
    (pageSize: number) => {
      reactDispatch({ type: ACTION_TYPES.CHANGE_PAGE_SIZE, pageSize });
      handleChangePage(DEFAULT_PAGE_NUMBER);
    },
    [handleChangePage]
  );

  const handleSearch = useCallback(
    (val: string) => {
      if (searchValue === val) return;
      reactDispatch({
        type: ACTION_TYPES.CHANGE_SEARCH_VALUE,
        searchValue: val
      });
    },
    [searchValue]
  );

  const handleChangeOrderBy = useCallback((orderBy: string) => {
    reactDispatch({
      type: ACTION_TYPES.CHANGE_ORDER_BY,
      orderBy
    });
  }, []);

  const handleChangeSortBy = useCallback((sortBy: string) => {
    reactDispatch({
      type: ACTION_TYPES.CHANGE_SORT_BY,
      sortBy
    });
  }, []);

  const _searchValue = searchValue.toLowerCase() || '';
  const _dataOrdered = showListVersion
    ? (_orderBy(dataList, [sortBy], [orderBy]) as TableVersion[])
    : (_orderBy(dataList, 'tableId', orderBy) as TableItem[]);

  const _dataFiltered = showListVersion
    ? (_dataOrdered as TableVersion[]).filter(w =>
        matchSearchValue([w?.tableName], _searchValue)
      )
    : (_dataOrdered as TableItem[])
        .filter(w => {
          const listVersionName = Array.isArray(w?.versions)
            ? w?.versions.map(v => v?.tableName)
            : [];
          return matchSearchValue(
            [w?.tableId, ...listVersionName],
            _searchValue
          );
        })
        .map(w => {
          const sortedVersions = _orderBy(w.versions, 'effectiveDate', 'desc');
          return { ...w, versions: sortedVersions } as TableItem;
        });

  const total = _dataFiltered.length;
  const dataListShow = _dataFiltered.slice(
    (page! - 1) * pageSize!,
    page! * pageSize!
  );

  const oderByItem = ORDER_BY_LIST.find(i => i.value === orderBy);

  const handleChangePageToSelectedVersion = useCallback(() => {
    if (!Array.isArray(tableList) || tableList.length === 0 || !selectedVersion)
      return;
    const selectedVersionIndex = showListVersion
      ? (_dataOrdered as TableVersion[]).findIndex(
          version => version?.tableId === selectedVersion?.version?.tableId
        )
      : (_dataOrdered as TableItem[]).findIndex(
          table => table?.tableId === selectedVersion?.tableId
        );
    if (selectedVersionIndex === -1) return;
    const selectedVersionPage = Math.ceil(
      (selectedVersionIndex + 1) / pageSize
    );
    handleChangePage(selectedVersionPage);

    !showListVersion &&
      handleSetExpandedList(EXPANDED_LIST.tableListModal)({
        expandAction: EXPAND_ACTION.replace,
        expandedList: { [selectedVersion?.tableId]: true }
      });

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tableList, selectedVersion, _dataOrdered]);

  useEffect(() => {
    handleChangePageToSelectedVersion();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tableList]);

  const handleSetVersionDetail = useCallback((versionDetail: any) => {
    reactDispatch({
      type: ACTION_TYPES.SET_VERSION_DETAIL,
      versionDetail
    });
  }, []);

  const handleClearAndReset = () => {
    handleSearch('');
    if (selectedVersion) {
      handleChangePageToSelectedVersion();
      return;
    }
    handleSetExpandedList(EXPANDED_LIST.tableListModal)({
      expandAction: EXPAND_ACTION.replace,
      expandedList: {}
    });
    handleChangePageSize(PAGE_SIZE[0]);
  };

  const [title, helpText1] = useMemo(() => {
    let helpText1;
    const title = isCreateNewVersion
      ? t('txt_workflow_manage_change_in_terms_new_version')
      : t('txt_manage_transaction_level_tlp_config_tq_choose_table_to_model');
    switch (tableType) {
      case TableType.tlptq:
        helpText1 = isCreateNewVersion
          ? 'txt_manage_transaction_level_tlp_config_tq_tableList_help_text'
          : 'txt_manage_transaction_level_tlp_config_tq_choose_table_to_model_help_text';
        break;
      case TableType.tlpaq:
        helpText1 = isCreateNewVersion
          ? 'txt_manage_transaction_level_tlp_config_aq_tableList_help_text'
          : 'txt_manage_transaction_level_tlp_config_aq_choose_table_to_model_help_text';
        break;
      case TableType.tlprq:
        helpText1 = isCreateNewVersion
          ? 'txt_manage_transaction_level_tlp_config_rq_tableList_help_text'
          : 'txt_manage_transaction_level_tlp_config_rq_choose_table_to_model_help_text';
        break;
      case TableType.tlpca:
        helpText1 =
          'txt_manage_transaction_level_tlp_config_ca_tableList_help_text';
        break;
      default:
        helpText1 = '';
    }
    return [title, helpText1];
  }, [isCreateNewVersion, t, tableType]);

  const handleGoToNextStep = (version?: SelectedVersion) => () => {
    handleOpenConfigModal(CONFIG_ACTIONS_MODAL.addNewTableModal);
    if (version) handleSelectVersion(version);
    const versionSelect = version || selectedVersion;
    if (
      !draftNewTable ||
      (draftNewTable &&
        draftNewTable.selectedVersionId !== versionSelect?.version?.tableId)
    ) {
      const elementListSelected = get(
        versionSelect,
        ['version', 'decisionElements'],
        [] as ElementItem[]
      );

      const controlParametersSelected = get(
        versionSelect,
        ['version', 'tableControlParameters'],
        [] as ControlParameterItem[]
      );

      const newControlParameters = mappingControlParameterValues(
        tableType,
        controlParametersSelected
      );

      const newElementList = mappingElementListInfo(
        elementListSelected,
        decisionElementList
      );

      handleSetDraftNewTable({
        selectedVersionId: versionSelect?.version?.tableId,
        elementList: newElementList,
        selectedTableId: versionSelect?.tableId as string,
        controlParameters: newControlParameters
      });
    }
  };

  const handleToggleExpandedTableListModal = useCallback(
    (tableId: string) => {
      return handleSetExpandedList(EXPANDED_LIST.tableListModal)({
        expandAction: EXPAND_ACTION.toggle,
        expandKey: tableId
      });
    },
    [handleSetExpandedList]
  );

  const handleSelectTableVersion = useCallback(
    (tableId: string, tableName: string, isViewDetail?: boolean) =>
      (version: TableVersion) => {
        isViewDetail
          ? handleSetVersionDetail({ tableId, tableName, version })
          : handleSelectVersion({ tableId, tableName, version });
      },
    [handleSelectVersion, handleSetVersionDetail]
  );

  const handleCloseModalVersionDetail = () => {
    handleSetVersionDetail(null);
  };

  useEffect(() => {
    return () => {
      handleSetExpandedList(EXPANDED_LIST.tableListModal)({
        expandAction: EXPAND_ACTION.replace,
        expandedList: {}
      });
    };
  }, [handleSetExpandedList]);

  const handleCloseAndClearState = () => {
    handleClose();
    handleSetExpandedList(EXPANDED_LIST.tableListModal)({
      expandedList: {},
      expandAction: EXPAND_ACTION.replace
    });
    handleSelectVersion(null);
    handleSetIsCreateNewVersion(false);
    handleSetDraftNewTable(null);
  };

  const handleCloseModal = () => {
    redirect({
      onConfirm: () => handleCloseAndClearState(),
      formsWatcher: [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_TRANSACTION_LEVEL_PROCESSING_DECISION_TABLES_TLP__CONFIG_TQ_DETAILS
      ]
    });
  };

  useUnsavedChangeRegistry(
    {
      ...unsavedChangesProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_TRANSACTION_LEVEL_PROCESSING_DECISION_TABLES_TLP__CONFIG_TQ_DETAILS,
      priority: 1
    },
    [Boolean(isFormChanging)]
  );

  const dataFilter: IDataFilter = useMemo(() => {
    return {
      sortBy: [sortBy],
      orderBy: [orderBy],
      searchValue
    };
  }, [orderBy, sortBy, searchValue]);

  return {
    dataListShow,
    total,
    page,
    pageSize,
    handleChangePage,
    handleChangePageSize,
    handleSearch,
    orderBy: oderByItem,
    searchValue,
    handleChangeOrderBy,
    loading,
    error,
    handleGetTableList,
    handleClearAndReset,
    versionDetail,
    dispatch: reactDispatch,
    title,
    helpText1,
    handleGoToNextStep,
    handleToggleExpandedTableListModal,
    handleSelectTableVersion,
    handleCloseModalVersionDetail,
    hasControlParameters,
    showListVersion,
    listVersionTableId,
    handleChangeSortBy,
    dataFilter,
    handleCloseModal
  };
};
