import { useTranslation } from 'app/_libraries/_dls';
import isEmpty from 'lodash.isempty';
import React from 'react';
import ControlParametersDataGrid from './ControlParametersDataGrid';
import ElementListDataGrid from './ElementListDataGrid';
import { ControlParameterItem, ElementItem } from './type';

interface ITableListSubGrid {
  elementList: ElementItem[];
  controlParameters: ControlParameterItem[];
}

const TableListSubGrid: React.FC<ITableListSubGrid> = ({
  elementList = [],
  controlParameters = []
}) => {
  const { t } = useTranslation();
  return (
    <div className="p-20">
      {!isEmpty(controlParameters) && (
        <div className="mb-24">
          <h5 className="mb-16 fs-14">
            {t('txt_manage_account_level_table_parameters')}
          </h5>
          <ControlParametersDataGrid controlParameters={controlParameters} />
        </div>
      )}
      <div>
        <h5 className="mb-16 fs-14">
          {t('txt_manage_transaction_level_tlp_element_list')}
        </h5>
        <ElementListDataGrid elementList={elementList} />
      </div>
    </div>
  );
};

export default TableListSubGrid;
