import { MethodFieldParameterEnum } from 'app/constants/enums';
import get from 'lodash.get';
import _orderBy from 'lodash.orderby';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import { useCallback, useEffect, useMemo, useReducer } from 'react';
import { useDispatch } from 'react-redux';
import { TableType } from './constant';
import { ConfigTableItem, DraftNewTable, SelectedVersion } from './type';

interface ConfigActionState {
  openedModal: null | CONFIG_ACTIONS_MODAL;
  expandedList: Record<EXPANDED_LIST, Record<string, any>>;
  selectedVersion: SelectedVersion | null;
  selectedTable: ConfigTableItem | null;
  isCreateNewVersion: boolean;
  draftNewTable: DraftNewTable | null;
  tableList: ConfigTableItem[];
  tableIndexChanged: number | null;
}

export enum CONFIG_ACTIONS_MODAL {
  addNewTableModal = 'addNewTableModal',
  tableListModal = 'tableListModal',
  deleteTableModal = 'deleteTableModal'
}

export enum EXPANDED_LIST {
  tableListModal = 'tableListModal',
  tableListIndex = 'tableListIndex'
}
export enum EXPAND_ACTION {
  toggle = 'toggle',
  replace = 'replace'
}

const ACTION_TYPE = {
  SET_OPENED_MODAL: 'SET_OPENED_MODAL',
  SET_EXPANDED_LIST: 'SET_EXPANDED_LIST',
  SET_SELECTED_VERSION: 'SET_SELECTED_VERSION',
  SET_SELECTED_TABLE: 'SET_SELECTED_TABLE',
  SET_IS_CREATE_NEW_VERSION: 'SET_IS_CREATE_NEW_VERSION',
  SET_DRAFT_NEW_TABLE: 'SET_DRAFT_NEW_TABLE',
  SET_TABLE_LIST: 'SET_TABLE_LIST',
  SET_TABLE_INDEX_CHANGED: 'SET_TABLE_INDEX_CHANGED'
};

export const INITIAL_STATE: ConfigActionState = {
  openedModal: null,
  expandedList: {
    [EXPANDED_LIST.tableListModal]: {},
    [EXPANDED_LIST.tableListIndex]: {}
  },
  draftNewTable: null,
  tableList: [] as ConfigTableItem[],
  tableIndexChanged: null,
  selectedVersion: null,
  selectedTable: null,
  isCreateNewVersion: false
};

const configActionsReducer = (state: ConfigActionState, action: any) => {
  switch (action.type) {
    case ACTION_TYPE.SET_OPENED_MODAL:
      return {
        ...state,
        openedModal: action.modalName
      };
    case ACTION_TYPE.SET_EXPANDED_LIST:
      let newExpandEdList = {
        ...state.expandedList[action.expandedListName as EXPANDED_LIST]
      };
      if (action.expandAction === EXPAND_ACTION.toggle) {
        newExpandEdList[action.expandKey]
          ? delete newExpandEdList[action.expandKey]
          : (newExpandEdList[action.expandKey] = true);
      }
      if (action.expandAction === EXPAND_ACTION.replace) {
        newExpandEdList = action.expandedList || {};
      }
      return {
        ...state,
        expandedList: {
          ...state.expandedList,
          [action.expandedListName]: newExpandEdList
        }
      };
    case ACTION_TYPE.SET_SELECTED_VERSION:
      return {
        ...state,
        selectedVersion: action.selectedVersion
      };
    case ACTION_TYPE.SET_SELECTED_TABLE:
      return {
        ...state,
        selectedTable: action.selectedTable
      };
    case ACTION_TYPE.SET_IS_CREATE_NEW_VERSION:
      return {
        ...state,
        isCreateNewVersion: action.isCreateNewVersion
      };
    case ACTION_TYPE.SET_DRAFT_NEW_TABLE:
      return {
        ...state,
        draftNewTable: action.draftNewTable
      };
    case ACTION_TYPE.SET_TABLE_LIST:
      return {
        ...state,
        tableList: action.tableList
      };
    case ACTION_TYPE.SET_TABLE_INDEX_CHANGED:
      return {
        ...state,
        tableIndexChanged: action.tableIndexChanged
      };
    default:
      return state;
  }
};

export const useConfigAction = (
  tables: ConfigTableItem[],
  tableType: TableType
) => {
  const initialState = useMemo(() => {
    return { ...INITIAL_STATE, tableList: tables } as ConfigActionState;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const [state, dispatch] = useReducer(configActionsReducer, initialState);
  const {
    openedModal,
    expandedList,
    selectedVersion,
    selectedTable,
    isCreateNewVersion,
    draftNewTable,
    tableList,
    tableIndexChanged
  } = state as ConfigActionState;

  const reduxDispatch = useDispatch();

  useEffect(() => {
    reduxDispatch(
      actionsWorkflowSetup.getWorkflowMetadata([
        MethodFieldParameterEnum.SearchCode,
        MethodFieldParameterEnum.IncludeActivityAction,
        MethodFieldParameterEnum.PosPromotionValidation
      ])
    );
  }, [reduxDispatch]);

  useEffect(() => {
    reduxDispatch(
      actionsWorkflowSetup.getDecisionElementList({
        tableType,
        serviceNameAbbreviation: 'TLP'
      })
    );
  }, [reduxDispatch, tableType]);

  const handleOpenConfigModal = useCallback(
    (modalName: CONFIG_ACTIONS_MODAL | null) =>
      dispatch({
        type: ACTION_TYPE.SET_OPENED_MODAL,
        modalName
      }),
    []
  );

  const handleCheckDuplicateTableId = useCallback(
    (tableId: string) => {
      const selectedTableId = get(selectedVersion, 'tableId', '');
      const versionTableId = get(selectedVersion, ['version', 'tableId'], '');
      const tableListIds = tableList.map((table: any) => table?.tableId);
      const arrayCheck = selectedVersion
        ? [...tableListIds, selectedTableId, versionTableId]
        : tableListIds;
      return arrayCheck.some(
        (existedTableId: string) => tableId === existedTableId
      );
    },
    [selectedVersion, tableList]
  );

  const handleSetTableIndexChanged = useCallback(
    (tableIndexChanged: number | null) => {
      dispatch({
        type: ACTION_TYPE.SET_TABLE_INDEX_CHANGED,
        tableIndexChanged
      });
    },
    []
  );

  const handleSetExpandedList = useCallback(
    (expandedListName: EXPANDED_LIST) =>
      ({ expandAction, expandKey, expandedList }: Record<string, any>) => {
        dispatch({
          type: ACTION_TYPE.SET_EXPANDED_LIST,
          expandedList,
          expandedListName,
          expandKey,
          expandAction
        });
      },
    []
  );

  const handleSetSelectedVersion = useCallback(
    (selectedVersion: SelectedVersion | null) => {
      dispatch({
        type: ACTION_TYPE.SET_SELECTED_VERSION,
        selectedVersion
      });
    },
    []
  );

  const handleSetIsCreateNewVersion = useCallback(
    (isCreateNewVersion: boolean) => {
      dispatch({
        type: ACTION_TYPE.SET_IS_CREATE_NEW_VERSION,
        isCreateNewVersion
      });
    },
    []
  );

  const handleSetDraftNewTable = useCallback(
    (draftNewTable: DraftNewTable | null) => {
      dispatch({
        type: ACTION_TYPE.SET_DRAFT_NEW_TABLE,
        draftNewTable
      });
    },
    []
  );

  const handleAddNewTable = useCallback(
    (table: ConfigTableItem) => {
      const newTables = _orderBy(
        [...tableList, table],
        'tableId',
        'asc'
      ) as ConfigTableItem[];
      const tableIndexChanged = newTables.findIndex(
        t => t?.tableId === table?.tableId
      );
      dispatch({
        type: ACTION_TYPE.SET_TABLE_LIST,
        tableList: newTables
      });
      handleSetTableIndexChanged(tableIndexChanged);
    },
    [tableList, handleSetTableIndexChanged]
  );

  const handleSetSelectedTable = useCallback(
    (selectedTable: ConfigTableItem | null) => {
      dispatch({
        type: ACTION_TYPE.SET_SELECTED_TABLE,
        selectedTable
      });
    },
    []
  );

  const handleEditTable = useCallback(
    (newTable: ConfigTableItem) => {
      const tableIndex = (tableList as ConfigTableItem[]).findIndex(
        table => table.rowId === newTable.rowId
      );
      const newTableList = [...tableList];
      newTableList[tableIndex] = newTable;
      const newTablesOrdered = _orderBy(newTableList, 'tableId', 'asc');
      const tableIndexChanged = newTablesOrdered.findIndex(
        (t: any) => t?.tableId === newTable?.tableId
      );

      dispatch({
        type: ACTION_TYPE.SET_TABLE_LIST,
        tableList: newTablesOrdered
      });
      handleSetTableIndexChanged(tableIndexChanged);
    },
    [tableList, handleSetTableIndexChanged]
  );

  const handleDeleteTable = useCallback(
    (rowId: string) => {
      const newTableList = tableList.filter(
        (table: any) => table.rowId !== rowId
      );
      dispatch({
        type: ACTION_TYPE.SET_TABLE_LIST,
        tableList: newTableList
      });
    },
    [tableList]
  );

  return {
    handleOpenConfigModal,
    openedModal,
    selectedTable,
    expandedList,
    selectedVersion,
    handleSetExpandedList,
    handleSetSelectedVersion,
    handleSetIsCreateNewVersion,
    isCreateNewVersion,
    handleSetDraftNewTable,
    draftNewTable,
    handleAddNewTable,
    tableList,
    handleSetSelectedTable,
    handleEditTable,
    handleDeleteTable,
    handleCheckDuplicateTableId,
    dispatch,
    tableIndexChanged,
    handleSetTableIndexChanged,
    initialState
  };
};
