import { MethodFieldParameterEnum } from 'app/constants/enums';
import { formatCommon } from 'app/helpers';
import {
  ColumnType,
  Grid,
  Icon,
  Tooltip,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import { useSelectElementMetadataManageTransactionTLP } from 'pages/_commons/redux/WorkflowSetup';
import React from 'react';
import { getControlParameters, TableType } from './constant';

interface IProps {
  isShowFlyout?: boolean;
  data: TableContentType[];
  tableType: TableType;
}

const GridTableDetail: React.FC<IProps> = ({
  isShowFlyout = false,
  data = [],
  tableType
}) => {
  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();
  const metadata = useSelectElementMetadataManageTransactionTLP();
  const { includeActivityOptions, posPromoValidationOptions } = metadata;

  const controlParameters = getControlParameters(tableType);
  const renderParameterValues = (item: any) => {
    const { id } = item;
    const matchItem = data.find(d => d?.name === id);
    let value;
    switch (id) {
      case MethodFieldParameterEnum.NextDebitDate:
        value = formatCommon(matchItem?.value as any)?.time?.date;
        break;
      case MethodFieldParameterEnum.IncludeActivityAction:
        const activityMatchOption = includeActivityOptions.find(
          op => op?.code === matchItem?.value
        );
        value = activityMatchOption?.code ? activityMatchOption?.text : null;
        break;
      case MethodFieldParameterEnum.PosPromotionValidation:
        const posMatchOption = posPromoValidationOptions.find(
          op => op?.code === matchItem?.value
        );
        value = posMatchOption?.code ? posMatchOption?.text : null;
        break;
      default:
        value = matchItem?.value;
    }

    return (
      <TruncateText
        resizable
        lines={2}
        ellipsisLessText={t('txt_less')}
        ellipsisMoreText={t('txt_more')}
      >
        {value}
      </TruncateText>
    );
  };

  const columns: ColumnType[] = [
    {
      id: 'businessName',
      Header: t('txt_manage_account_level_table_decision_business_name'),
      accessor: ({ businessName }) => t(businessName),
      width: isShowFlyout ? 230 : 233
    },
    {
      id: 'greenScreenName',
      Header: t('txt_manage_account_level_table_decision_green_name'),
      accessor: ({ greenScreenName }) => t(greenScreenName),
      width: isShowFlyout ? 200 : 244
    },
    {
      id: 'value',
      Header: t('txt_manage_account_level_table_decision_value'),
      accessor: renderParameterValues,
      width: isShowFlyout ? 260 : width < 1280 ? 320 : 360
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      cellBodyProps: { className: 'pt-12 pb-8' },
      width: 106,
      accessor: ({ moreInfo }) => (
        <Tooltip element={t(moreInfo)}>
          <Icon name="information" size="5x" className="color-grey-l16" />
        </Tooltip>
      )
    }
  ];

  return <Grid columns={columns} data={controlParameters} />;
};

export default GridTableDetail;
