import { mapColorBadge } from 'app/components/TableCardView';
import { formatCommon } from 'app/helpers';
import { Badge, Button, Radio, useTranslation } from 'app/_libraries/_dls';
import React from 'react';
import { SelectedVersion, TableVersion } from './type';

interface VersionCardViewProps {
  version: TableVersion;
  onSelectVersion: (version: TableVersion) => void;
  onViewVersionDetail: (version: TableVersion) => void;
  selectedVersion: SelectedVersion | null;
}

const VersionCardView: React.FC<VersionCardViewProps> = ({
  version,
  selectedVersion,
  onSelectVersion,
  onViewVersionDetail
}) => {
  const { t } = useTranslation();
  const handleSelectVersion = (version: TableVersion) => () => {
    onSelectVersion(version);
  };
  const handleViewVersionDetail = (version: TableVersion) => (e: any) => {
    e.stopPropagation();
    onViewVersionDetail(version);
  };
  return (
    <div key={version?.tableId} className="position-relative">
      <div
        className="list-view method-card-view py-12 mt-12 workflow-card-view bg-checkbox-hover-light-l20 custom-control-root"
        data-testid="versionCardView"
        onClick={handleSelectVersion(version)}
      >
        <div className="row align-items-center">
          <Radio
            className="select-version-radio"
            onClick={handleSelectVersion(version)}
          >
            <Radio.Input
              className="checked-style"
              checked={version?.tableId === selectedVersion?.version?.tableId}
            />
          </Radio>
          <div className="col-4 mt-n4" role="dof-field">
            <div className="form-group-static mt-4 d-flex align-items-center w-100 pl-24 ml-24">
              <div className="color-grey text-nowrap mr-2">Table Name:</div>
              <div className="form-group-static__text flex-fill fw-600">
                {version?.tableName}
              </div>
            </div>
          </div>
          <div className="col-4 mt-n4" role="dof-field">
            <div className="form-group-static mt-4 d-flex align-items-center w-100">
              <div className="color-grey text-nowrap mr-2">Effective Date:</div>
              <div className="form-group-static__text flex-fill">
                {formatCommon(version?.effectiveDate).time.date}
              </div>
            </div>
          </div>
          <div className="col-3 pl-0 pl-lg-12" role="dof-field">
            <div className="form-group-static mt-0 d-flex align-items-center">
              <Badge color={mapColorBadge(version?.status)}>
                <span>{version?.status}</span>
              </Badge>
            </div>
          </div>
        </div>
      </div>
      <div className="method-card-view-btn" role="dof-field">
        <Button
          size="sm"
          variant="outline-primary"
          onClick={handleViewVersionDetail(version)}
        >
          {t('txt_view')}
        </Button>
      </div>
    </div>
  );
};

export default VersionCardView;
