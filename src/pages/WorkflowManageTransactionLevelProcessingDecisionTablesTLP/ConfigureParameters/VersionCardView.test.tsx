import { render, RenderResult } from '@testing-library/react';
import { act } from '@testing-library/react-hooks';
import userEvent from '@testing-library/user-event';
import 'app/utils/_mockComponent/mockUseTranslation';
import React from 'react';
import VersionCardView from './VersionCardView';

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <VersionCardView {...props} />
    </div>
  );
};

const SelectedVersion = {
  tableId: 'HUW2UWGL',
  version: {
    tableId: 'XLUB8XPN',
    tableName: 'F1EGSCUZ',
    effectiveDate: '2022-01-31T17:00:00Z',
    status: 'Validating',
    comment: 'nostrud aliquyam',
    tableControlParameters: [],
    decisionElements: []
  }
};

const Version = {
  tableId: '78QI7ROK',
  tableName: 'GP3AU2MD',
  effectiveDate: '2021-09-18T17:00:00Z',
  status: 'Validating',
  comment: 'diam lorem justo dignissim kasd',
  tableControlParameters: [],
  decisionElements: []
};

describe('WorkflowManageTransactionLevelProcessingDecisionTablesTLP > ConfigureParameters > versionCardView', () => {
  const version = Version;
  const onSelectVersion = jest.fn();
  const onViewVersionDetail = jest.fn();
  const selectedVersion = SelectedVersion;

  const defaultProps = {
    version,
    onSelectVersion,
    onViewVersionDetail,
    selectedVersion
  };

  it('It should render and Click select and view detail card view', () => {
    const wrapper = renderComponent(defaultProps);
    const cardView = wrapper.getByTestId('versionCardView');
    const btnView = wrapper.getByText('txt_view');

    expect(wrapper.getByTestId('versionCardView')).toBeInTheDocument();
    act(() => {
      userEvent.click(cardView);
    });
    expect(onSelectVersion).toBeCalled();

    act(() => {
      userEvent.click(btnView);
    });

    expect(onViewVersionDetail).toBeCalled();
  });
});
