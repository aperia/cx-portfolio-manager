import { render, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { mockUseModalRegistryFnc } from 'app/utils';
import 'app/utils/_mockComponent/mockNoDataFound';
import 'app/utils/_mockComponent/mockPaging';
import 'app/utils/_mockComponent/mockUseTranslation';
import * as mockMetaData from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageAccountLevel';
import React from 'react';
import * as reactRedux from 'react-redux';
import { TableType } from './constant';
import ViewModal from './ViewModal';

const mockMeta = jest.spyOn(
  mockMetaData,
  'useSelectElementMetadataManageAcountLevel'
);

jest.mock('./GridControlParametersTableDetailsFlyout', () => {
  const MethodListModal = () => {
    return (
      <div>
        <div>GridControlParameters_component</div>
      </div>
    );
  };

  const StatusName = {
    production: 'production',
    clientApproved: 'client approved',
    scheduled: 'scheduled',
    working: 'working',
    validating: 'validating',
    previous: 'previous',
    lapsed: 'lapsed'
  };

  return {
    __esModule: true,
    default: MethodListModal,
    StatusName
  };
});

const mockUseModalRegistry = mockUseModalRegistryFnc();
const mockUseSelector = jest.spyOn(reactRedux, 'useSelector');

const defaultProps = {
  tableId: '123',
  data: {},
  onCancel: jest.fn(),
  onOK: jest.fn()
};

const renderModal = (props: any): RenderResult => {
  return render(
    <div>
      <ViewModal {...props} />
    </div>
  );
};

const mockDecisionElements = [
  {
    name: 'APPLICATION SCORE',
    searchCode: 'E',
    immediateAllocation: '2342',
    value: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
  },
  {
    name: 'APPLICATION SCORE',
    searchCode: 'E',
    immediateAllocation: '3242342',
    value: []
  },
  {
    name: 'APPLICATION SCORE',
    searchCode: 'E',
    immediateAllocation: '',
    value: []
  },
  {
    name: 'APPLICATION SCORE',
    searchCode: 'E',
    immediateAllocation: '',
    value: []
  },
  {
    name: 'APPLICATION SCORE',
    searchCode: 'E',
    immediateAllocation: '',
    value: []
  },
  {
    name: 'APPLICATION SCORE',
    searchCode: 'E',
    immediateAllocation: '',
    value: []
  },
  {
    name: 'APPLICATION SCORE',
    searchCode: 'E',
    immediateAllocation: '',
    value: []
  },
  {
    name: 'APPLICATION SCORE',
    searchCode: 'E',
    immediateAllocation: '',
    value: []
  },
  {
    name: 'APPLICATION SCORE',
    searchCode: 'E',
    immediateAllocation: '',
    value: []
  },
  {
    name: 'APPLICATION SCORE',
    searchCode: 'E',
    immediateAllocation: '',
    value: []
  },
  {
    name: 'APPLICATION SCORE',
    searchCode: 'E',
    immediateAllocation: '',
    value: []
  },
  {
    name: 'RESULT ID',
    searchCode: '',
    immediateAllocation: '',
    value: []
  }
];

const { getComputedStyle } = window;
window.getComputedStyle = (elt, pseudoElt) => getComputedStyle(elt, pseudoElt);

describe('WorkflowManageTransactionLevelProcessingDecisionTablesTLP > ConfigureParameters > ViewModal', () => {
  beforeEach(() => {
    mockUseSelector.mockImplementation(selector =>
      selector({
        caches: {}
      })
    );
    mockMeta.mockReturnValue({
      searchCode: [
        {
          value: 'E',
          description: 'E - Exact'
        },
        {
          value: 'R',
          description: 'R - Ranger'
        }
      ]
    } as any);
    mockUseModalRegistry.mockImplementation(() => [true, false]);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Should render modal with no data', () => {
    renderModal({
      ...defaultProps,
      data: { status: '' }
    });
  });

  it('Should render modal with data', () => {
    const wrapper = renderModal({
      ...defaultProps,
      data: { decisionElements: mockDecisionElements, status: '' }
    });
    userEvent.click(
      wrapper.getByText('txt_manage_account_level_table_decision_elements')
    );
    userEvent.click(
      wrapper.baseElement.querySelector(
        'button[class="change-page-number"]'
      ) as Element
    );
    userEvent.click(
      wrapper.baseElement.querySelector(
        'button[class="change-page-size"]'
      ) as Element
    );
  });

  it('Should render modal with data when onlyShowElementList', () => {
    renderModal({
      ...defaultProps,
      data: { decisionElements: mockDecisionElements, status: '' },
      tableType: TableType.tlptq
    });
  });
});
