import { fireEvent, render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React from 'react';
import ActionButtons from './ActionButtons';

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

describe('WorkflowManageTransactionLevelProcessingDecisionTablesTLP > ConfigureParametersTQ > ActionButtons', () => {
  it('Should render with small buttons', () => {
    const props = {
      small: true,
      title: 'example',
      onClickCreateNewVersion: jest.fn(),
      onClickTableToModel: jest.fn(),
      onClickAddNewTable: jest.fn()
    };
    const wrapper = render(<ActionButtons {...props} />);
    const dropdownButton = wrapper.container.querySelector(
      'button.dls-dropdown-button'
    ) as Element;
    expect(dropdownButton).not.toBeNull();

    userEvent.click(dropdownButton);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    ).toBeInTheDocument();
    expect(wrapper.getByText('example')).toBeInTheDocument();
    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    );
    expect(props.onClickCreateNewVersion).toHaveBeenCalled();

    userEvent.click(dropdownButton);
    expect(
      wrapper.getByText(
        'txt_manage_transaction_level_tlp_choose_table_to_model'
      )
    ).toBeInTheDocument();
    fireEvent.click(
      wrapper.getByText(
        'txt_manage_transaction_level_tlp_choose_table_to_model'
      )
    );
    expect(props.onClickTableToModel).toHaveBeenCalled();

    userEvent.click(dropdownButton);
    expect(
      wrapper.getByText('txt_manage_transaction_level_tlp_create_new_table')
    ).toBeInTheDocument();
    fireEvent.click(
      wrapper.getByText('txt_manage_transaction_level_tlp_create_new_table')
    );
    expect(props.onClickAddNewTable).toHaveBeenCalled();
  });

  it('Should render with large buttons', () => {
    const props = {
      onClickCreateNewVersion: jest.fn(),
      onClickTableToModel: jest.fn(),
      onClickAddNewTable: jest.fn()
    };
    const wrapper = render(<ActionButtons {...props} />);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_select_an_option')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    );
    expect(props.onClickCreateNewVersion).toHaveBeenCalled();

    fireEvent.click(
      wrapper.getByText(
        'txt_manage_transaction_level_tlp_choose_table_to_model'
      )
    );
    expect(props.onClickTableToModel).toHaveBeenCalled();

    fireEvent.click(
      wrapper.getByText('txt_manage_transaction_level_tlp_create_new_table')
    );
    expect(props.onClickAddNewTable).toHaveBeenCalled();
  });

  it('Should render with only create new version button', () => {
    const props = {
      onClickCreateNewVersion: jest.fn(),
      onClickTableToModel: jest.fn(),
      onClickAddNewTable: jest.fn(),
      onlyShowCreateNewVersion: true
    };
    const wrapper = render(<ActionButtons {...props} />);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_select_an_option')
    ).toBeInTheDocument();
  });
});
