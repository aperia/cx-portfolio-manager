import { Button, useTranslation } from 'app/_libraries/_dls';
import isFunction from 'lodash.isfunction';
import React, { useMemo } from 'react';
import { ConfigureParametersFormValue } from '../ConfigureParameters';
import { TableType } from './constant';
import TableListSummary from './TableListSummary';
import ViewNewVersionSummary from './ViewNewVersionSummary';

const Summary: React.FC<
  WorkflowSetupSummaryProps<ConfigureParametersFormValue>
> = ({ formValues, selectedStep, stepId, onEditStep }) => {
  const { tables, tableType } = formValues || {};

  const onlyShowCreateNewVersion = useMemo(() => {
    return tableType === TableType.tlpca;
  }, [tableType]);

  const { t } = useTranslation();

  const handleEdit = () => {
    isFunction(onEditStep) && onEditStep();
  };
  const isStepSelected = selectedStep === stepId;
  return (
    <div className="position-relative">
      <div className="absolute-top-right mt-n26 mr-n8">
        <Button variant="outline-primary" size="sm" onClick={handleEdit}>
          {t('txt_edit')}
        </Button>
      </div>
      {tables &&
        tables.length === 1 &&
        onlyShowCreateNewVersion &&
        isStepSelected && (
          <ViewNewVersionSummary dataTableVersion={tables[0]} />
        )}
      {tables &&
        tables.length > 0 &&
        !onlyShowCreateNewVersion &&
        isStepSelected && <TableListSummary tableList={tables} />}
    </div>
  );
};

export default Summary;
