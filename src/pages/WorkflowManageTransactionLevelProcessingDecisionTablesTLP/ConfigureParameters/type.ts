export interface ElementItem {
  name: string;
  searchCode?: string;
  moreInfo?: string;
  immediateAllocation?: string;
  configurable?: boolean;
  isNewElement?: boolean;
  rowId: string;
  value?: number[];
}

export interface ControlParameterItem {
  [index: string]: string;
}

export interface TableVersion {
  tableId: string;
  tableName: string;
  effectiveDate: string;
  status: string;
  comment: string;
  decisionElements: ElementItem[];
  tableControlParameters: ControlParameterItem[];
}

export interface TableItem {
  tableId: string;
  tableName: string;
  versions: TableVersion[];
}

export interface SelectedVersion {
  tableId: string;
  version: TableVersion;
  tableName?: string;
}

export interface DraftNewTable {
  comment?: string;
  tableId?: string;
  tableName?: string;
  selectedTableId: string;
  elementList: ElementItem[];
  selectedVersionId?: string;
  controlParameters?: ControlParameterItem[];
  isFormChanging?: boolean;
}

export interface ConfigTableItem {
  rowId: string;
  tableId: string;
  tableName: string;
  elementList: ElementItem[];
  comment: string;
  actionTaken: string;
  selectedVersionId: string;
  selectedTableId: string;
  selectedVersion: SelectedVersion;
  controlParameters: ControlParameterItem[];
}
