import { mockThunkAction } from 'app/utils';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import parseFormValues from './parseFormValues';

const mockActionsWorkflowSetup = mockThunkAction(actionsWorkflowSetup);

beforeEach(() => {
  mockActionsWorkflowSetup('getDecisionElementList');
});

describe('pages > WorkflowManageTransactionLevelProcessingDecisionTablesTLP > ConfigureParameters > parseFormValues', () => {
  it('should return undefined with stepInfo is undefined', async () => {
    const response = await parseFormValues(
      {
        data: {
          configurations: {
            tableList: [
              { tableType: 'AQ', modeledFrom: '' },
              { tableType: 'AQ', modeledFrom: 'Modeled' }
            ]
          },
          workflowSetupData: [{ id: '1' }]
        }
      } as any,
      'configTlpTq',
      jest.fn()
    );
    expect(response).toEqual(undefined);
  });

  it('should return valid data with id is configTlpTq', async () => {
    const response = await parseFormValues(
      {
        data: {
          workflowSetupData: [
            {
              id: 'configTlpTq'
            }
          ],
          configurations: {
            tableList: [
              { tableType: 'TQ', modeledFrom: '' },
              { tableType: 'TQ', modeledFrom: 'Modeled' }
            ]
          }
        }
      } as any,
      'configTlpTq',
      jest.fn()
    );

    expect(response).toEqual(
      expect.objectContaining({
        isPass: false,
        isSelected: false,
        isValid: true,
        tableType: 'TQ'
      })
    );
  });

  it('should return valid data with id is configTlpAq', async () => {
    const response = await parseFormValues(
      {
        data: {
          workflowSetupData: [
            {
              id: 'configTlpAq'
            }
          ],
          configurations: {
            tableList: [
              { tableType: 'AQ', modeledFrom: '' },
              { tableType: 'AQ', modeledFrom: 'Modeled' }
            ]
          }
        }
      } as any,
      'configTlpAq',
      jest.fn()
    );

    expect(response).toEqual(
      expect.objectContaining({
        isPass: false,
        isSelected: false,
        isValid: true,
        tableType: 'AQ'
      })
    );
  });

  it('should return valid data with id is configTlpRq', async () => {
    const response = await parseFormValues(
      {
        data: {
          workflowSetupData: [
            {
              id: 'configTlpRq'
            }
          ],
          configurations: {
            tableList: [
              { tableType: 'RQ', modeledFrom: '' },
              { tableType: 'RQ', modeledFrom: 'Modeled' }
            ]
          }
        }
      } as any,
      'configTlpRq',
      jest.fn()
    );

    expect(response).toEqual(
      expect.objectContaining({
        isPass: false,
        isSelected: false,
        isValid: true,
        tableType: 'RQ'
      })
    );
  });

  it('should return valid data with id is tlpca', async () => {
    const response = await parseFormValues(
      {
        data: {
          workflowSetupData: [
            {
              id: 'tlpca'
            }
          ],
          configurations: {}
        }
      } as any,
      'tlpca',
      jest.fn()
    );

    expect(response).toEqual(
      expect.objectContaining({
        isPass: false,
        isSelected: false,
        isValid: false,
        tableType: 'CA'
      })
    );
  });
});
