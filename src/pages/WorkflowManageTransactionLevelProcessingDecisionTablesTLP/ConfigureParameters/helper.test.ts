import { getTableActionType } from './helper';

describe('test getTableActionType helper', () => {
  it('should be return addTableToModel', () => {
    const actual = getTableActionType({ tableId: '1' }, '');
    expect(actual).toEqual('addTableToModel');
  });
});
