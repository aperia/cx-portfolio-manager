import { matchSearchValue } from 'app/helpers';
import { useSelectElementMetadataManageTransactionTLP } from 'pages/_commons/redux/WorkflowSetup';
import { useCallback, useEffect, useMemo, useReducer, useRef } from 'react';
import { TableType } from './constant';
import { ControlParametersProps } from './ControlParameters';

export enum FieldType {
  datePicker = 'datePicker',
  textBox = 'textBox',
  dropDown = 'dropDown'
}
interface ControlParametersState {
  searchValue: string;
}

const INITIAL_STATE: ControlParametersState = {
  searchValue: ''
};

const ACTION_TYPES = {
  CHANGE_SEARCH_VALUE: 'CHANGE_SEARCH_VALUE'
};

const controlParametersReducer = (
  state: ControlParametersState,
  action: any
) => {
  switch (action.type) {
    case ACTION_TYPES.CHANGE_SEARCH_VALUE:
      return { ...state, searchValue: action.searchValue };
    default:
      return state;
  }
};

export const useControlParameters = (props: ControlParametersProps) => {
  const { controlParameterList, handleChangeControlParameters, tableType } =
    props;
  const [state, reactDispatch] = useReducer(
    controlParametersReducer,
    INITIAL_STATE
  );
  const { searchValue } = state;

  const simpleSearchRef = useRef<any>(null);

  useEffect(() => {
    if (!searchValue && simpleSearchRef?.current) {
      simpleSearchRef.current.clear();
    }
  }, [searchValue]);

  const metadata = useSelectElementMetadataManageTransactionTLP();

  const { includeActivityOptions, posPromoValidationOptions } = metadata;

  const handleChangeSearchValue = (searchValue: string) => {
    reactDispatch({ type: ACTION_TYPES.CHANGE_SEARCH_VALUE, searchValue });
  };

  const isHideSearchBox = useMemo(() => {
    return tableType === TableType.tlprq;
  }, [tableType]);

  const handleChangeParametersValue = useCallback(
    (id: string, fieldType: FieldType) => (e: any) => {
      let value = {} as any;
      switch (fieldType) {
        case FieldType.datePicker:
          value.value = e?.target?.value;
          break;
        case FieldType.dropDown:
          value.value = e?.target?.value?.code;
          break;
        default:
          value = e?.target?.value;
      }
      const rowIndex = controlParameterList.findIndex(row => row.id === id);

      if (rowIndex === -1) return;

      const newControlParameters = [...controlParameterList];
      newControlParameters[rowIndex] = {
        ...controlParameterList[rowIndex],
        ...value
      };
      handleChangeControlParameters(newControlParameters);
    },
    [controlParameterList, handleChangeControlParameters]
  );

  const _searchValue = searchValue ? searchValue.toLowerCase() : '';

  const _controlParametersSearched = controlParameterList.filter((w: any) => {
    if (!_searchValue) return true;
    return matchSearchValue(
      [w?.businessName, w?.greenScreenName, w?.moreInfo],
      _searchValue
    );
  });

  return {
    searchValue,
    handleChangeSearchValue,
    parameters: _controlParametersSearched,
    simpleSearchRef,
    handleChangeParametersValue,
    includeActivityOptions,
    posPromoValidationOptions,
    isHideSearchBox,
    dispatch: reactDispatch
  };
};
