import { render, RenderResult } from '@testing-library/react';
import 'app/utils/_mockComponent/mockUseTranslation';
import * as CommonSelectHooks from 'pages/_commons/redux/Common/select-hooks';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageTransactionLevelProcessingDecisionTLP';
import React from 'react';
import ElementListDataGrid from './ElementListDataGrid';

const useSelectElementMetadataManageTransactionTLP = jest.spyOn(
  WorkflowSetup,
  'useSelectElementMetadataManageTransactionTLP'
);

const mockUseSelectWindowDimension = jest.spyOn(
  CommonSelectHooks,
  'useSelectWindowDimension'
);

HTMLCanvasElement.prototype.getContext = jest.fn();

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <ElementListDataGrid {...props} />
    </div>
  );
};

const SearchCodeOptions = [
  {
    code: 'E',
    text: 'E - Exact'
  },
  {
    code: 'R',
    text: 'R - Range'
  }
];

const MockElementListData = [
  {
    name: 'Test',
    searchCode: 'E'
  },
  {
    name: 'Test-1',
    searchCode: 'R'
  }
];

describe('WorkflowManageTransactionLevelProcessingDecisionTablesTLP > ConfigureParameters > ElementListDataGrid', () => {
  const defaultProps = {
    elementList: undefined
  };

  beforeEach(() => {
    useSelectElementMetadataManageTransactionTLP.mockImplementation(() => ({
      searchCode: SearchCodeOptions,
      includeActivityOptions: [],
      posPromoValidationOptions: []
    }));
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1920,
      height: 1080
    }));
  });

  afterEach(() => {
    mockUseSelectWindowDimension.mockClear();
    useSelectElementMetadataManageTransactionTLP.mockClear();
  });

  it('Should render Element list grid data with no value', () => {
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1920,
      height: 1080
    }));

    renderComponent(defaultProps);
  });

  it('Should render Element list grid data with Values', () => {
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1000,
      height: 1080
    }));

    const wrapper = renderComponent({
      ...defaultProps,
      elementList: MockElementListData
    });
    expect(
      wrapper.getByText('txt_manage_transaction_level_tlp_decision_element')
    ).toBeInTheDocument();
  });
});
