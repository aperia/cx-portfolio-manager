import { matchSearchValue } from 'app/helpers';
import { useTranslation } from 'app/_libraries/_dls';
import get from 'lodash.get';
import isEmpty from 'lodash.isempty';
import isEqual from 'lodash.isequal';
import _orderBy from 'lodash.orderby';
import set from 'lodash.set';
import { nanoid } from 'nanoid';
import {
  useSelectDecisionElementListData,
  useSelectElementMetadataManageTransactionTLP
} from 'pages/_commons/redux/WorkflowSetup';
import { useCallback, useEffect, useMemo, useReducer } from 'react';
import { TableType } from './constant';
import { IElementList } from './ElementList';

interface ElementListState {
  errors: Record<string, Record<string, string>>;
  touches: Record<string, Record<string, boolean>>;
  searchValue: string;
}

const INITIAL_STATE: ElementListState = {
  errors: {},
  touches: {},
  searchValue: ''
};

const ACTION_TYPES = {
  CHANGE_SEARCH_VALUE: 'CHANGE_SEARCH_VALUE',
  SET_TOUCHES_FIELD: 'SET_TOUCHES_FIELD',
  SET_ERRORS: 'SET_ERRORS'
};

const elementListReducer = (state: ElementListState, action: any) => {
  switch (action.type) {
    case ACTION_TYPES.CHANGE_SEARCH_VALUE:
      return { ...state, searchValue: action.searchValue };
    case ACTION_TYPES.SET_TOUCHES_FIELD:
      const newStateTouch = { ...state, touches: { ...state.touches } };
      set(
        newStateTouch,
        ['touches', action.rowId, action.fieldName],
        action.value
      );
      return { ...newStateTouch };
    case ACTION_TYPES.SET_ERRORS:
      return { ...state, errors: action.errors };
    default:
      return state;
  }
};

export const useElementList = (props: IElementList) => {
  const {
    elementList,
    handleSetElementList,
    handleSetElementListHasError,
    tableType
  } = props;
  const { t } = useTranslation();
  const [state, reactDispatch] = useReducer(elementListReducer, INITIAL_STATE);
  const { searchValue, errors, touches } = state;
  const data = useSelectDecisionElementListData(`TLP${tableType}`);
  const decisionElementList = useMemo(
    () => data?.decisionElementList || [],
    [data]
  );
  const metadata = useSelectElementMetadataManageTransactionTLP();

  const { searchCode } = metadata;

  const elementListNameOptions = useMemo(() => {
    return _orderBy(decisionElementList, 'name', 'asc')
      .map((el: any) => ({
        code: el.name,
        text: el.name,
        ...el
      }))
      .filter((el: any) => el.configurable);
  }, [decisionElementList]);

  const elementListNameOptionsCanSelect = useMemo(() => {
    return elementListNameOptions.filter(el => {
      const isOptionChosen = elementList.some(
        elementItem => elementItem?.name === el?.name
      );
      return !isOptionChosen;
    });
  }, [elementListNameOptions, elementList]);

  const handleChangeSearchValue = (searchValue: string) => {
    reactDispatch({ type: ACTION_TYPES.CHANGE_SEARCH_VALUE, searchValue });
  };

  const maximumElementCanAdd = useMemo(() => {
    switch (tableType) {
      case TableType.tlpca:
        return 6;
      default:
        return 20;
    }
  }, [tableType]);

  const isHideDeleteBtn = useMemo(() => {
    return elementList.length === 1 && !elementList[0]?.name;
  }, [elementList]);

  const handleChangeElementValue = useCallback(
    (fieldName: string, rowId: string) => (e: any) => {
      const value = e?.target?.value;
      const rowIndex = elementList.findIndex(row => row.rowId === rowId);
      if (rowIndex === -1) return;
      const newElementList = [...elementList];
      newElementList[rowIndex] = {
        ...elementList[rowIndex],
        [fieldName]: value?.code || ''
      };
      if (fieldName === 'name' && value?.code) {
        newElementList[rowIndex] = {
          ...newElementList[rowIndex],
          moreInfo: value?.moreInfo,
          immediateAllocation: value?.immediateAllocation,
          configurable: value?.configurable,
          isNewElement: false,
          searchCode: ''
        };
        reactDispatch({
          type: ACTION_TYPES.SET_TOUCHES_FIELD,
          fieldName: 'searchCode',
          rowId,
          value: false
        });
      }
      handleSetElementList(newElementList);
    },
    [elementList, handleSetElementList]
  );

  const handleClearElementName = useCallback(
    (rowId: string, clearType?: string) => {
      const rowIndex = elementList.findIndex(row => row.rowId === rowId);
      if (rowIndex === -1) return;
      const newElementList = [...elementList];
      newElementList[rowIndex] =
        clearType === 'removeButton'
          ? {
              name: '',
              isNewElement: true,
              rowId: nanoid()
            }
          : {
              name: '',
              isNewElement: true,
              rowId: newElementList[rowIndex]?.rowId
            };
      handleSetElementList(newElementList);
    },

    [elementList, handleSetElementList]
  );

  const handleDeleteElement = useCallback(
    rowId => () => {
      // remove
      if (elementList.length === 1 && elementList[0]?.name) {
        handleClearElementName(rowId, 'removeButton');
        return;
      }
      const newElementList = elementList.filter(el => el.rowId !== rowId);
      handleSetElementList(newElementList);
    },
    [handleSetElementList, elementList, handleClearElementName]
  );

  const handleSetTouchField = useCallback(
    (fieldName: string, rowId: string) => (e: any) => {
      const value = e?.target?.value;
      if (fieldName === 'name' && !value?.code) {
        handleClearElementName(rowId);
      }
      if (get(touches, [rowId, fieldName])) return;
      reactDispatch({
        type: ACTION_TYPES.SET_TOUCHES_FIELD,
        fieldName,
        rowId,
        value: true
      });
    },
    [touches, handleClearElementName]
  );

  const _searchValue = searchValue ? searchValue.toLowerCase() : '';

  const _elementListSearched = elementList.filter(w => {
    if (!_searchValue && !w.name) return true;
    return matchSearchValue([w?.name, w?.moreInfo as string], _searchValue);
  });

  useEffect(() => {
    const newErrors = {};
    if (elementList.length === 0) return;

    elementList.forEach(el => {
      if (!el.name) {
        set(
          newErrors,
          [el.rowId, 'name'],
          t(
            'txt_manage_transaction_level_tlp_config_tq_require_decision_element'
          )
        );
      }
      if (!el.searchCode) {
        set(
          newErrors,
          [el.rowId, 'searchCode'],
          t('txt_manage_transaction_level_tlp_config_tq_require_search_code')
        );
      }
    });

    !isEmpty(newErrors)
      ? handleSetElementListHasError(true)
      : handleSetElementListHasError(false);

    if (isEqual(errors, newErrors)) return;
    reactDispatch({
      type: ACTION_TYPES.SET_ERRORS,
      errors: newErrors
    });
  }, [elementList, t, handleSetElementListHasError, errors]);

  const totalElement = elementList.length;

  const handleDnDDataChange = (newElementList: any) => {
    if (!searchValue) {
      handleSetElementList(newElementList);
      return;
    }
  };

  const handleAddNewElement = useCallback(() => {
    if (totalElement === maximumElementCanAdd) return;
    const newElement = { name: '', isNewElement: true, rowId: nanoid() };
    const newElementList = [...elementList, newElement];
    handleSetElementList(newElementList);
    handleChangeSearchValue('');
  }, [handleSetElementList, elementList, totalElement, maximumElementCanAdd]);

  const tooltipDragList = useMemo(() => {
    if (!searchValue) return [];
    return elementList?.map(item => ({
      id: item?.rowId,
      tooltipProps: {
        element: t(
          'txt_manage_account_level_table_decision_tooltip_disabled_dnd'
        )
      }
    }));
  }, [searchValue, elementList, t]);

  return {
    searchValue,
    handleChangeSearchValue,
    handleChangeElementValue,
    elements: _elementListSearched,
    handleDeleteElement,
    handleSetTouchField,
    errors,
    touches,
    elementListNameOptions,
    handleAddNewElement,
    totalElement,
    handleDnDDataChange,
    searchCodeOptions: searchCode,
    dispatch: reactDispatch,
    tooltipDragList,
    elementListNameOptionsCanSelect,
    maximumElementCanAdd,
    isHideDeleteBtn
  };
};
