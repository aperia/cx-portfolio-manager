import { act, renderHook } from '@testing-library/react-hooks';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageTransactionLevelProcessingDecisionTLP';
import * as reactRedux from 'react-redux';
import { TableType } from './constant';
import { useElementList } from './useElementList';

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');

const useSelectElementMetadataManageTransactionTLP = jest.spyOn(
  WorkflowSetup,
  'useSelectElementMetadataManageTransactionTLP'
);
const useSelectDecisionElementListData = jest.spyOn(
  WorkflowSetup,
  'useSelectDecisionElementListData'
);

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

const MockSearchCode = [
  {
    code: 'E',
    text: 'E - Exact'
  },
  {
    code: 'R',
    text: 'R - Range'
  }
];

const MockElementListOptions = [
  {
    name: 'ACS CURR PORT',
    moreInfo:
      'Adaptive Control System portfolio currently in effect for the customer account; variable-length, 4 positions, numeric.',
    immediateAllocation: 'No',
    configurable: true
  },
  {
    name: 'ACS PORT CHG DATE',
    moreInfo:
      'Date (MMDDYYYY) the current Adaptive Control System portfolio identification was last changed; variable-length, 10 positions, numeric.\n Example: 031404 or 03142004 = March 14, 2004',
    immediateAllocation: 'No',
    configurable: true
  },
  {
    name: 'ADVERTISING GROUP',
    moreInfo:
      'Issuer-defined advertising group code; variable-length, 3 positions, numeric.',
    immediateAllocation: 'No',
    configurable: true
  },
  {
    name: 'AGE',
    moreInfo:
      'Principal cardholder’s age expressed in whole number of years; variable-length, 3 positions, numeric.\n To determine whether a cardholder account matches a value, the System subtracts the year of birth from the current year. The System adds one year if the month of birth has already passed. If the date of birth consists of zeros, the age is 0.',
    immediateAllocation: 'No',
    configurable: true
  },
  {
    name: 'ALT FIN RPT BRCH/P',
    moreInfo:
      'Issuer-defined alternate financial reporting branch/portfolio field; fixed-length, 5 positions, numeric.',
    immediateAllocation: 'No',
    configurable: true
  },
  {
    name: 'ALT FIN RPT C/NO C',
    moreInfo:
      'Issuer-defined alternate financial reporting card/no card field; fixed-length, 5 positions, numeric.',
    immediateAllocation: 'No',
    configurable: true
  },
  {
    name: 'ALT FIN RPT ENTITY',
    moreInfo:
      'Issuer-defined alternate financial reporting entity field; fixed-length, 5 positions, numeric.',
    immediateAllocation: 'No',
    configurable: true
  },
  {
    name: 'ALT FIN RPT OTHER1',
    moreInfo:
      'Issuer-defined alternate financial reporting other 1 field; fixed-length, 5 positions, numeric.',
    immediateAllocation: 'No',
    configurable: true
  },
  {
    name: 'ANN CASH INT RATE',
    moreInfo:
      'Annual cash advance interest rate on cardholder account record; fixed-length, 6 positions, numeric.\nYou must enter two positions, a decimal point, and three positions to the right of the decimal point.\nExamples:\n18.550 = 18.55%\n09.990 = 9.99%',
    immediateAllocation: 'No',
    configurable: true
  },
  {
    name: 'ANN MRCH INT RATE',
    moreInfo:
      'Annual merchandise interest rate on cardholder account record; fixed-length, 6 positions, numeric.\nYou must enter two positions, a decimal point, and three positions to the right of the decimal point.\nExamples:\n18.550 = 18.55%\n09.990 = 9.99%',
    immediateAllocation: 'No',
    configurable: true
  },
  {
    name: 'ANNUAL CHARGE FLAG',
    moreInfo:
      'Value indicating the annual charge rate for a cardholder account; valid codes are A-F, 0-6, 9.',
    immediateAllocation: 'No',
    configurable: true
  },
  {
    name: 'ANNUAL CHG DATE',
    moreInfo:
      'Next date (MMYYYY) that a cardholder account will be reviewed and assessed an annual charge; fixed-length, 6 positions, numeric.',
    immediateAllocation: 'No',
    configurable: true
  },
  {
    name: 'APPLICATION SCORE',
    moreInfo:
      'Application Processing System (EAPS) first-pass score, based on the information provided on the new cardholder account application; variable-length, 5 positions, numeric.\nThis score does not include information provided by the credit bureau.',
    immediateAllocation: 'No',
    configurable: true
  },
  {
    name: 'APPROVAL OFFICER',
    moreInfo:
      'Issuer-defined code representing the person at your institution who approved this account; variable-length, 7 positions, numeric.',
    immediateAllocation: 'No',
    configurable: true
  },
  {
    name: 'ATTRITION INDEX',
    moreInfo:
      'Issuer-defined identifier of the attrition index; variable-length, 3 positions, numeric.',
    immediateAllocation: 'No',
    configurable: true
  },
  {
    name: 'AUTHORIZATION FLAG',
    moreInfo:
      'Value used to control authorizations or correspondence; fixed-length, 1 position, alphanumeric.',
    immediateAllocation: 'No',
    configurable: true
  },
  {
    name: 'AVG END BAL LST 03',
    moreInfo:
      'Dollar-and-cent amount of the customer account’s average ending balance for the last three cycles; variable-length, 15 positions, numeric.\nExample: 100019 = $1,000.19\nTo determine whether a customer account matches a value, the System totals the ending balances for the last three cycles and divides the result by three. The result is the correct average if the customer account has cycled three times in the Fiserv System.',
    immediateAllocation: 'No',
    configurable: true
  },
  {
    name: 'AVG END BAL LST 06',
    moreInfo:
      'Dollar-and-cent amount of the customer account’s average ending balance for the last six cycles; variable-length, 15 positions, numeric.\nExample: 100019 = $1,000.19\nTo determine whether a customer account matches a value, the System totals the ending balances for the last six cycles and divides the result by six. The result is the correct average if the customer account has cycled six times in the Fiserv System.',
    immediateAllocation: 'No',
    configurable: true
  },
  {
    name: 'AVG END BAL LST 12',
    moreInfo:
      'Dollar-and-cent amount of the customer account’s average ending balance for the last 12 cycles; variable-length, 15 positions, numeric.\nExample: 100019 = $1,000.19\nTo determine whether a customer account matches a value, the System totals the ending balances for the last 12 cycles and divides the result by 12. The result is the correct average if the customer account has cycled 12 times in the Fiserv System.',
    immediateAllocation: 'No',
    configurable: true
  },
  {
    name: 'BEHAVIOR SCORE',
    moreInfo:
      'Number representing the likelihood that an account will go three or more cycles delinquent in the next six months; variable-length, 3 positions, numeric.',
    immediateAllocation: 'No',
    configurable: true
  },
  {
    name: 'BILLING CYCLE CODE',
    moreInfo:
      'Code determining when the customer statement is generated; variable-length, 2 positions, numeric.',
    immediateAllocation: 'No',
    configurable: true
  },
  {
    name: 'BONUS STRATEGY',
    moreInfo:
      'Identifier of the bonus strategy established for the Rewards product; variable-length, 8 positions, alphanumeric.\nThis field must match an existing bonus strategy. Refer to the Rewards manual for more information about bonus strategies.',
    immediateAllocation: 'No',
    configurable: true
  },
  {
    name: 'ACCT OPEN DAYS',
    moreInfo: null,
    immediateAllocation: 'No',
    configurable: true
  },
  {
    name: 'ACQUIRING BIN',
    moreInfo: null,
    immediateAllocation: 'No',
    configurable: true
  },
  {
    name: 'ACQUIRING MC ICA',
    moreInfo: null,
    immediateAllocation: 'No',
    configurable: true
  },
  {
    name: 'ACQUIRING VISA BIN',
    moreInfo: null,
    immediateAllocation: 'No',
    configurable: true
  },
  {
    name: 'RESULT ID',
    moreInfo:
      'Enter the Promotion Identifier in the RESULT ID field for each criteria row to point cardholder accounts to Product Control File settings.\nFor the transaction qualification tables, the Promotion Identifier is a variable-length, 8-position code that identifies a group of Product Control File methods. The methods include parameter settings that control processing options.',
    immediateAllocation: 'No',
    configurable: false
  }
];

const MockElementList = [
  {
    name: 'APPLICATION SCORE',
    searchCode: 'E',
    immediateAllocation: 'No',
    value: [],
    rowId: 'SFElrs-dzBc04ytSziVKZ',
    moreInfo:
      'Application Processing System (EAPS) first-pass score, based on the information provided on the new cardholder account application; variable-length, 5 positions, numeric.\nThis score does not include information provided by the credit bureau.',
    configurable: true,
    searchCodeText: 'E - Exact'
  },
  {
    name: 'Testing not have in options list',
    searchCode: 'Not have in options',
    immediateAllocation: 'No',
    value: [],
    rowId: 'SFElrs-dzBc04ytSEEEE',
    moreInfo:
      'Application Processing System (EAPS) first-pass score, based on the information provided on the new cardholder account application; variable-length, 5 positions, numeric.\nThis score does not include information provided by the credit bureau.',
    configurable: true,
    searchCodeText: 'E - Exact'
  },
  {
    name: '',
    searchCode: '',
    isNewElement: true,
    rowId: 'SFElrs-dzBc04ytSziret'
  }
];

describe('WorkflowManageTransactionLevelProcessingDecisionTablesTLP > ConfigureParameters > useElementList', () => {
  const mockDispatch = jest.fn();
  const handleSetElementList = jest.fn();
  const handleSetElementListHasError = jest.fn();

  const tableType = 'Test';
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
    useSelectElementMetadataManageTransactionTLP.mockReturnValue({
      searchCode: MockSearchCode,
      includeActivityOptions: [],
      posPromoValidationOptions: []
    });
  });

  const defaultProps = {
    elementList: MockElementList,
    handleSetElementList,
    handleSetElementListHasError,
    tableType
  } as any;

  it('Run Default reducer function', () => {
    useSelectDecisionElementListData.mockReturnValue({});
    const { result } = renderHook(() => useElementList(defaultProps));
    act(() => {
      result.current.dispatch({ type: 'Test Action' });
    });
  });

  it('Run useElement List normal', () => {
    useSelectDecisionElementListData.mockReturnValue({
      decisionElementList: MockElementListOptions
    });
    const { result } = renderHook(() => useElementList(defaultProps));

    act(() => {
      result.current.handleChangeSearchValue('ABC');
    });
    act(() => {
      result.current.handleDnDDataChange([]);
    });
    expect(result.current.searchValue).toEqual('ABC');

    act(() => {
      result.current.handleChangeSearchValue('');
      result.current.handleChangeElementValue(
        'name',
        'SFElrs-dzBc04ytSziret'
      )({
        target: {
          value: {
            code: 'BEHAVIOR SCORE',
            text: 'BEHAVIOR SCORE',
            name: 'BEHAVIOR SCORE',
            moreInfo:
              'Number representing the likelihood that an account will go three or more cycles delinquent in the next six months; variable-length, 3 positions, numeric.',
            immediateAllocation: 'No',
            configurable: true
          }
        }
      });
      result.current.handleChangeElementValue(
        'searchCode',
        'SFElrs-dzBc04ytSziret'
      )({
        target: {
          value: {
            code: 'E',
            text: 'E - Exact'
          }
        }
      });
      result.current.handleChangeElementValue(
        'searchCode',
        'SFElrs-test'
      )({
        target: {
          value: {
            code: 'E',
            text: 'E - Exact'
          }
        }
      });
      result.current.handleChangeElementValue(
        'searchCode',
        'SFElrs-dzBc04ytSEEEE'
      )({
        target: {
          value: {
            abc: 'E',
            text: 'E - Exact'
          }
        }
      });
    });

    act(() => {
      result.current.handleSetTouchField('name', 'SFElrs-dzBc04ytSziret')({});
      result.current.handleSetTouchField(
        'searchCode',
        'SFElrs-dzBc04ytSziret'
      )({});
    });
    expect(result.current.touches['SFElrs-dzBc04ytSziret'].name).toEqual(true);

    act(() => {
      result.current.handleDnDDataChange([]);
    });
    expect(handleSetElementList).toHaveBeenCalled();

    act(() => {
      result.current.handleSetTouchField('name', 'SFElrs-dzBc04ytSziret')({});
      result.current.handleAddNewElement();
    });
    expect(handleSetElementList).toHaveBeenCalled();

    act(() => {
      result.current.handleDeleteElement('SFElrs-dzBc04ytSziret')();
    });
    expect(handleSetElementList).toHaveBeenCalled();
  });

  it('Render with no erros', () => {
    useSelectDecisionElementListData.mockReturnValue({
      decisionElementList: MockElementListOptions
    });
    const { result } = renderHook(() =>
      useElementList({
        ...defaultProps,
        elementList: [
          { name: 'name', searchCode: 'dsa' },
          { name: 'name', searchCode: 'dsa' },
          { name: 'name', searchCode: 'dsa' },
          { name: 'name', searchCode: 'dsa' },
          { name: 'name', searchCode: 'dsa' },
          { name: 'name', searchCode: 'dsa' }
        ],
        tableType: TableType.tlpca
      })
    );
    expect(result.current.errors).toEqual({});

    act(() => {
      result.current.handleAddNewElement();
    });
  });

  it('Render with no element list', () => {
    useSelectDecisionElementListData.mockReturnValue({
      decisionElementList: MockElementListOptions
    });
    const { result } = renderHook(() =>
      useElementList({
        ...defaultProps,
        elementList: [],
        tableType: TableType.tlpca
      })
    );
    expect(result.current.errors).toEqual({});
  });

  it('Render with one new element', () => {
    useSelectDecisionElementListData.mockReturnValue({
      decisionElementList: MockElementListOptions
    });
    const { result } = renderHook(() =>
      useElementList({
        ...defaultProps,
        elementList: [{ name: '', isNewElement: true, rowId: '12' }],
        tableType: TableType.tlptq
      })
    );
    expect(result.current.isHideDeleteBtn).toEqual(true);
  });

  it('Delete one element', () => {
    useSelectDecisionElementListData.mockReturnValue({
      decisionElementList: MockElementListOptions
    });
    const { result } = renderHook(() =>
      useElementList({
        ...defaultProps,
        elementList: [{ name: 'Test', isNewElement: true, rowId: '12' }],
        tableType: TableType.tlptq
      })
    );
    act(() => {
      result.current.handleDeleteElement('12')();
    });
    expect(result.current.elements.length).toEqual(1);
  });

  it('Delete one element with incorrect row id', () => {
    useSelectDecisionElementListData.mockReturnValue({
      decisionElementList: MockElementListOptions
    });
    const { result } = renderHook(() =>
      useElementList({
        ...defaultProps,
        elementList: [{ name: 'Test', isNewElement: true, rowId: '12' }],
        tableType: TableType.tlptq
      })
    );
    act(() => {
      result.current.handleDeleteElement('123')();
    });

    expect(result.current.elements.length).toEqual(1);
  });
});
