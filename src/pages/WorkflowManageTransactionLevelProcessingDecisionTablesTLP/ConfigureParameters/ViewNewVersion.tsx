import BadgeGroupTextControl from 'app/components/DofControl/BadgeGroupTextControl';
import GroupTextControl from 'app/components/DofControl/GroupTextControl';
import { BadgeColorType } from 'app/constants/enums';
import { Button, useTranslation } from 'app/_libraries/_dls';
import React, { useCallback } from 'react';
import ElementListDataGrid from './ElementListDataGrid';
import { ConfigTableItem } from './type';
import { CONFIG_ACTIONS_MODAL } from './useConfigActions';

export interface ViewNewVersionProps {
  dataTableVersion: ConfigTableItem;
  handleSetSelectedTable: (selectedTable: ConfigTableItem) => void;
  handleOpenConfigModal: (tableName: CONFIG_ACTIONS_MODAL) => void;
}

const ViewNewVersion: React.FC<ViewNewVersionProps> = ({
  dataTableVersion,
  handleSetSelectedTable,
  handleOpenConfigModal
}) => {
  const { t } = useTranslation();

  const handleEditTable = useCallback(
    (table: any) => () => {
      handleSetSelectedTable(table);
      handleOpenConfigModal(CONFIG_ACTIONS_MODAL.addNewTableModal);
    },
    [handleSetSelectedTable, handleOpenConfigModal]
  );

  const handleDeleteTable = useCallback(
    table => () => {
      handleSetSelectedTable(table);
      handleOpenConfigModal(CONFIG_ACTIONS_MODAL.deleteTableModal);
    },
    [handleOpenConfigModal, handleSetSelectedTable]
  );
  return (
    <div>
      <div className="d-flex justify-content-between align-items-center">
        <h5>{dataTableVersion?.tableName}</h5>
        <div className="mr-n8">
          <Button
            size="sm"
            variant="outline-primary"
            onClick={handleEditTable(dataTableVersion)}
          >
            {t('txt_edit')}
          </Button>
          <Button
            size="sm"
            variant="outline-danger"
            onClick={handleDeleteTable(dataTableVersion)}
          >
            {t('txt_delete')}
          </Button>
        </div>
      </div>

      <div className="row">
        <div className="col-6 col-xl-3">
          <GroupTextControl
            id="tableId"
            input={{ value: dataTableVersion?.tableId } as any}
            meta={{} as any}
            label={t('txt_manage_account_level_table_id')}
            options={{ inline: false }}
          />
        </div>
        <div className="col-6 col-xl-3">
          <div className="form-group-static align-items-center">
            <span className="form-group-static__label">
              <span className="color-grey text-nowrap mr-2">
                {t('txt_manage_account_level_table_action_taken')}
              </span>
            </span>
            <BadgeGroupTextControl
              id="manage_account_level_table_version_created"
              input={
                {
                  value: t('txt_manage_account_level_table_version_created')
                } as any
              }
              meta={{} as any}
              options={{
                colorType: BadgeColorType.VersionCreated,
                noBorder: true
              }}
            />
          </div>
        </div>
        <div className="col-12 col-xl-6">
          <GroupTextControl
            id="comment"
            input={{ value: dataTableVersion?.comment } as any}
            meta={{} as any}
            label={t('Comment Area')}
            options={{
              inline: false,
              lineTruncate: 2,
              ellipsisLessText: t('txt_less'),
              ellipsisMoreText: t('txt_more')
            }}
          />
        </div>
      </div>
      <div className="divider-dashed my-24" />
      <div className="d-flex align-items-center justify-content-between">
        <h5 className="mb-16">
          {t('txt_manage_account_level_table_decision_element_list')}
        </h5>
      </div>
      <ElementListDataGrid elementList={dataTableVersion?.elementList} />
    </div>
  );
};

export default ViewNewVersion;
