import EnhanceDatePicker from 'app/components/EnhanceDatePicker';
import EnhanceDropdownList from 'app/components/EnhanceDropdownList';
import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import {
  ColumnType,
  Grid,
  Icon,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import isEmpty from 'lodash.isempty';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import ClearAndResetButton from 'pages/_commons/Utils/ClearAndResetButton';
import React from 'react';
import { TableType } from './constant';
import { ControlParameterItem } from './type';
import { FieldType, useControlParameters } from './useControlParameters';

export interface ControlParametersProps {
  controlParameterList: ControlParameterItem[];
  handleChangeControlParameters: (
    controlParameters: ControlParameterItem[]
  ) => void;
  tableType: TableType;
}

const ControlParameters: React.FC<ControlParametersProps> = props => {
  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();

  const {
    searchValue,
    handleChangeSearchValue,
    parameters,
    simpleSearchRef,
    handleChangeParametersValue,
    includeActivityOptions,
    posPromoValidationOptions,
    isHideSearchBox
  } = useControlParameters(props);

  const renderValuesField = (data: any) => {
    const { id } = data;
    switch (id) {
      case MethodFieldParameterEnum.NextDebitDate:
        const dateValue = data.value ? new Date(data?.value) : undefined;
        return (
          <EnhanceDatePicker
            placeholder="mm/dd/yyyy"
            required
            size="small"
            value={dateValue}
            onChange={handleChangeParametersValue(id, FieldType.datePicker)}
          />
        );
      case MethodFieldParameterEnum.IncludeActivityAction:
        const valueActivity = includeActivityOptions.find(
          option => option?.code === data?.value
        );
        return (
          <EnhanceDropdownList
            size="small"
            value={valueActivity}
            placeholder={t('txt_select_an_option')}
            onChange={handleChangeParametersValue(id, FieldType.dropDown)}
            options={includeActivityOptions}
          />
        );
      case MethodFieldParameterEnum.PosPromotionValidation:
        const valuePos = posPromoValidationOptions.find(
          option => option?.code === data?.value
        );
        return (
          <EnhanceDropdownList
            size="small"
            value={valuePos}
            placeholder={t('txt_select_an_option')}
            onChange={handleChangeParametersValue(id, FieldType.dropDown)}
            options={posPromoValidationOptions}
          />
        );
    }
  };

  const columns: ColumnType[] = [
    {
      id: 'businessName',
      Header: t('txt_manage_account_level_table_decision_business_name'),
      accessor: data => data.businessName,
      cellBodyProps: { className: 'pt-12 pb-8' },
      width: 233
    },
    {
      id: 'greenScreenName',
      Header: t('txt_manage_account_level_table_decision_green_name'),
      accessor: data => data.greenScreenName,
      cellBodyProps: { className: 'pt-12 pb-8' },
      width: 244
    },
    {
      id: 'value',
      Header: t('txt_manage_account_level_table_decision_value'),
      width: width < 1280 ? 320 : 360,
      cellBodyProps: { className: 'py-8' },
      accessor: renderValuesField
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      width: 106,
      cellBodyProps: { className: 'pt-12 pb-8' },
      accessor: data => (
        <Tooltip element={data.moreInfo}>
          <Icon name="information" size="5x" className="color-grey-l16" />
        </Tooltip>
      )
    }
  ];

  return (
    <div className="mt-24">
      {
        <div className="d-flex align-items-center justify-content-between mb-16">
          <h5>{t('txt_manage_account_level_table_parameters')}</h5>
          {!isHideSearchBox && (
            <SimpleSearch
              ref={simpleSearchRef}
              placeholder={t('txt_type_to_search')}
              defaultValue={searchValue}
              onSearch={handleChangeSearchValue}
              popperElement={
                <>
                  <p className="color-grey">{t('txt_search_description')}</p>
                  <ul className="search-field-item list-unstyled">
                    <li className="mt-16">{t('txt_business_name')}</li>
                    <li className="mt-16">{t('txt_green_screen_name')}</li>
                    <li className="mt-16">{t('txt_more_info')}</li>
                  </ul>
                </>
              }
            />
          )}
        </div>
      }
      {searchValue && !isEmpty(parameters) && (
        <div className="d-flex justify-content-end mb-16 mr-n8">
          <ClearAndResetButton
            small
            onClearAndReset={() => {
              handleChangeSearchValue('');
            }}
          />
        </div>
      )}
      {isEmpty(parameters) && (
        <div className="d-flex flex-column justify-content-center mt-40 mb-24">
          <NoDataFound
            id="newMethod_notfound"
            hasSearch
            title={t('txt_no_results_found')}
            linkTitle={t('txt_clear_and_reset')}
            onLinkClicked={() => {
              handleChangeSearchValue('');
            }}
          />
        </div>
      )}
      {!isEmpty(parameters) && <Grid columns={columns} data={parameters} />}
    </div>
  );
};

export default ControlParameters;
