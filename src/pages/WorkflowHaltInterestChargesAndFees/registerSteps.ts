import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { stepRegistry } from '../_commons/WorkflowSetup/registries';
import ConfigureParametersStep from './ConfigureParametersStep';
import GettingStartStep from './GettingStartStep';

stepRegistry.registerStep(
  'GetStartedHaltInterestChargesAndFees',
  GettingStartStep
);
stepRegistry.registerStep(
  'ConfigureParametersHaltInterestChargesAndFees',
  ConfigureParametersStep,
  [
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__HALT_INTEREST_CHARGES_AND_FEES_CONFIGURE_PARAMETERS
  ]
);
