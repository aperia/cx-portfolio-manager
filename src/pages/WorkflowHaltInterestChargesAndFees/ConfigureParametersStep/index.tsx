import {
  DateRangePickerValue,
  InlineMessage,
  useTranslation
} from 'app/_libraries/_dls';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React from 'react';
import ConfigureParametersEditStep from './EditForm';
import parseFormValues from './parseFormValues';
import ConfigureParametersSummary from './Summary';

export interface ConfigureParametersFormValues {
  isValid?: boolean;
  dateRange?: DateRangePickerValue;
}

const ConfigureParametersStep: React.FC<
  WorkflowSetupProps<ConfigureParametersFormValues>
> = props => {
  const { t } = useTranslation();
  const { isStuck } = props;

  return (
    <>
      {isStuck && (
        <InlineMessage className="mb-16 mt-24" variant="danger" withIcon>
          {t('txt_step_stuck_move_forward_message')}
        </InlineMessage>
      )}
      <div className="mt-8">
        <p className="color-grey">
          {t('txt_halt_interest_charges_and_fees_enter_the_time_period')}
        </p>
        <ConfigureParametersEditStep {...props} />
      </div>
    </>
  );
};

const ExtraStaticConfigureParameters =
  ConfigureParametersStep as WorkflowSetupStaticProp<ConfigureParametersFormValues>;

ExtraStaticConfigureParameters.parseFormValues = parseFormValues;
ExtraStaticConfigureParameters.summaryComponent = ConfigureParametersSummary;
ExtraStaticConfigureParameters.defaultValues = {
  isValid: false,
  dateRange: { start: undefined, end: undefined }
};

export default ExtraStaticConfigureParameters;
