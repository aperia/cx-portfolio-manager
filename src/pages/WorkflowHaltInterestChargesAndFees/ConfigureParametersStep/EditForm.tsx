import {
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { isDevice } from 'app/helpers';
import { useFormValidations, useUnsavedChangeRegistry } from 'app/hooks';
import {
  DateRangePicker,
  DateRangePickerValue,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import { isEqual } from 'lodash';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch } from 'react-redux';
import { ConfigureParametersFormValues } from '.';
interface ChangeInfo {
  dateRange?: DateRangePickerValue;
}

const ConfigureParametersEditStep: React.FC<
  WorkflowSetupProps<ConfigureParametersFormValues>
> = ({ setFormValues, formValues, stepId, selectedStep, savedAt }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [initialValues, setInitialValues] = useState(formValues);

  const [rangeActive, setRangeActive] = useState<{ min?: Date; max?: Date }>({
    min: undefined,
    max: undefined
  });

  const [change, setChange] = useState<ChangeInfo>({
    dateRange: {
      start: undefined,
      end: undefined
    }
  });

  const handleFieldChange = (field: keyof ChangeInfo) => (e: any) => {
    setChange(change => {
      const newFormValue = {
        ...formValues,
        ...change,
        [field]: e.target?.value
      };
      const isValid =
        !!newFormValue.dateRange?.start && !!newFormValue.dateRange?.end;

      setFormValues({ ...newFormValue, isValid });
      return { ...change, [field]: e.target?.value };
    });
  };

  const currentErrors = useMemo(() => {
    const inValidDateRange =
      !change?.dateRange || !change.dateRange.start || !change.dateRange.end;

    return {
      dateRange: inValidDateRange && {
        status: true,
        message: t('txt_required_validation', {
          fieldName: t('txt_date_range')
        })
      }
    } as Record<keyof ChangeInfo, IFormError>;
  }, [change, t]);
  const [, errors, setTouched] =
    useFormValidations<keyof ChangeInfo>(currentErrors);

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__HALT_INTEREST_CHARGES_AND_FEES_CONFIGURE_PARAMETERS,
      priority: 1
    },
    [initialValues !== formValues]
  );

  const initFields = useCallback(async () => {
    const responseDateRange = await Promise.resolve(
      dispatch(actionsWorkflowSetup.getWorkflowEffectiveDateOptions({}))
    );
    const { maxDate, minDate } =
      (responseDateRange as any)?.payload?.data || {};

    setRangeActive({ max: new Date(maxDate), min: new Date(minDate) });
  }, [dispatch]);

  const handleDateTooltip = (date: Date) => {
    if (
      (rangeActive?.min && rangeActive.min > date) ||
      (rangeActive?.max && rangeActive.max < date)
    ) {
      return <Tooltip element={<span>Non-processing day</span>} />;
    }
  };

  useEffect(() => {
    setInitialValues(formValues);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [savedAt]);

  useEffect(() => {
    initFields();
  }, [initFields]);

  useEffect(() => {
    if (stepId !== selectedStep) return;

    const isUpdateDateRange =
      isEqual(change.dateRange?.start, formValues.dateRange?.start) &&
      isEqual(change.dateRange?.end, formValues.dateRange?.end);

    if (isUpdateDateRange) return;

    setChange({
      dateRange: {
        start: formValues.dateRange?.start,
        end: formValues.dateRange?.end
      }
    });
  }, [change.dateRange, formValues.dateRange, selectedStep, stepId]);

  return (
    <div className="row mt-24">
      <div className="col-6 col-xl-4">
        <DateRangePicker
          error={errors.dateRange}
          onChange={handleFieldChange('dateRange')}
          onFocus={setTouched('dateRange')}
          onBlur={setTouched('dateRange', true)}
          label={t('txt_date_range')}
          maxDate={rangeActive.max}
          minDate={rangeActive.min}
          dateTooltip={handleDateTooltip}
          popupBaseProps={{
            popupBaseClassName: 'my-4'
          }}
          required
          value={change.dateRange}
          canInput={!isDevice}
        />
      </div>
    </div>
  );
};

export default ConfigureParametersEditStep;
