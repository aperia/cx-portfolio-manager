import { getWorkflowSetupStepStatus, isValidDate } from 'app/helpers';
import { isEmpty } from 'lodash';

const parseFormValues: WorkflowSetupStepFormDataFunc<any> = (data, id) => {
  const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);

  const { date } = (data?.data as any)?.configurations || {};
  if (isEmpty(date) || !stepInfo) return;

  const dateRange = {
    start: isValidDate(date.minDate) ? new Date(date.minDate) : undefined,
    end: isValidDate(date.maxDate) ? new Date(date.maxDate) : undefined
  };

  const isValid = !!dateRange?.end && !!dateRange?.start;
  return {
    ...getWorkflowSetupStepStatus(stepInfo),
    isValid,
    dateRange
  };
};

export default parseFormValues;
