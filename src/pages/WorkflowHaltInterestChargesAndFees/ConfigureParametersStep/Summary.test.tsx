import { fireEvent, render, RenderResult } from '@testing-library/react';
import React from 'react';
import Summary from './Summary';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <Summary {...props} />
    </div>
  );
};

describe('HaltInterestChargesAndFees > ConfigureParametersStep > Summary', () => {
  it('Should render summary contains Completed', async () => {
    const onEditStepMock = jest.fn();
    const props = {
      onEditStep: onEditStepMock,
      formValues: {
        dateRange: {
          start: new Date(),
          end: new Date()
        }
      }
    } as any;
    const wrapper = await renderComponent(props);

    expect(wrapper.getByText('txt_date_range')).toBeInTheDocument;
    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(onEditStepMock).toBeCalled();
  });
});
