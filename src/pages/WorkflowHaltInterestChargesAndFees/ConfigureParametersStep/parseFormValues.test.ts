import parseFormValues from './parseFormValues';

describe('HaltInterestChargesAndFees > parseFormValues', () => {
  it('Has stepId and date data', () => {
    const data = {
      data: {
        workflowSetupData: [
          {
            id: 'step',
            status: 'INPROGRESS',
            selected: true
          }
        ],
        configurations: {
          date: {
            minDate: '10/04/2021',
            maxDate: '10/14/2021'
          }
        }
      }
    } as any;
    const id = 'step';
    const { isPass, isSelected } = parseFormValues(data, id);
    expect(isPass).toEqual(true);
    expect(isSelected).toEqual(true);
  });

  it('Has NO date data', () => {
    const data = {
      data: {
        workflowSetupData: [
          {
            id: 'step',
            status: 'INPROGRESS',
            selected: true
          }
        ]
      }
    } as any;
    const id = 'step';
    const output = parseFormValues(data, id);
    expect(output).toBeUndefined();
  });
});
