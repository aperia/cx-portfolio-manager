import { render, RenderResult } from '@testing-library/react';
import React from 'react';
import ConfigureParametersStep from '.';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

jest.mock('./EditForm', () => {
  return {
    __esModule: true,
    default: () => <div>ConfigureParametersForm</div>
  };
});

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <ConfigureParametersStep {...props} />
    </div>
  );
};

describe('HaltInterestChargesAndFees > ConfigureParametersStep', () => {
  it('Should render ConfigureParametersStep contains Completed', () => {
    const wrapper = renderComponent({
      formValues: {}
    });

    expect(
      wrapper.getByText(
        'txt_halt_interest_charges_and_fees_enter_the_time_period'
      )
    ).toBeInTheDocument;
  });
});
