import { fireEvent } from '@testing-library/dom';
import { renderComponent } from 'app/utils';
import { DateRangePickerProps } from 'app/_libraries/_dls';
import React from 'react';
import * as ReactRedux from 'react-redux';
import { ConfigureParametersFormValues } from '.';
import EditForm from './EditForm';

const mockUseDispatch = jest.spyOn(ReactRedux, 'useDispatch');

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

jest.mock('app/_libraries/_dls/components/DateRangePicker', () => {
  return {
    __esModule: true,
    default: ({ onChange, dateTooltip }: DateRangePickerProps) => (
      <>
        <input
          data-testid="DateRangePicker_onChange"
          onChange={(e: any) => {
            onChange!({
              target: {
                value: { start: e.target.date, end: e.target.date },
                name: e.target.name
              }
            } as any);
            dateTooltip!(e.target.date as Date, e.target.view);
          }}
        />
      </>
    )
  };
});

describe('HaltInterestChargesAndFees > ConfigureParametersSteps', () => {
  const testId = 'testId';
  beforeEach(() => {
    mockUseDispatch.mockReturnValue(jest.fn());
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Render component', async () => {
    mockUseDispatch.mockReturnValue(
      jest.fn().mockReturnValue({
        payload: { data: { maxDate: '2021/02/04', minDate: '2021/02/01' } }
      })
    );
    const props: WorkflowSetupProps<ConfigureParametersFormValues> = {
      setFormValues: jest.fn(),
      formValues: {
        dateRange: {
          start: new Date(),
          end: new Date()
        }
      },
      selectedStep: 'step',
      stepId: 'step',
      savedAt: 123,
      isEdit: false,
      clearFormValues: function (step: string): void {
        throw new Error('Function not implemented.');
      },
      handleSave: jest.fn()
    };
    const { getByTestId } = await renderComponent(
      testId,
      <EditForm {...props} />
    );
    const inputElement = getByTestId('DateRangePicker_onChange');
    expect(inputElement).toBeInTheDocument();

    fireEvent.change(inputElement, {
      target: {
        value: 'undefined',
        name: 'dateValue',
        date: new Date('2021/02/03'),
        view: 'month'
      }
    });
    expect(inputElement).toBeInTheDocument();
  });

  it('Render component when selected', async () => {
    const props: WorkflowSetupProps<ConfigureParametersFormValues> = {
      setFormValues: jest.fn(),
      formValues: {
        dateRange: {
          start: new Date(),
          end: new Date()
        }
      },
      selectedStep: 'step',
      stepId: 'step',
      savedAt: 123,
      isEdit: false,
      clearFormValues: function (step: string): void {
        throw new Error('Function not implemented.');
      },
      handleSave: jest.fn()
    };
    const { getByTestId } = await renderComponent(
      testId,
      <EditForm {...props} />
    );
    const inputElement = getByTestId('DateRangePicker_onChange');
    expect(inputElement).toBeInTheDocument();
  });

  it('Render component NOT selected', async () => {
    mockUseDispatch.mockReturnValue(
      jest.fn().mockReturnValue({
        payload: { data: { maxDate: '2021/02/02', minDate: '2021/02/01' } }
      })
    );
    const props: WorkflowSetupProps<ConfigureParametersFormValues> = {
      setFormValues: jest.fn(),
      formValues: {},
      selectedStep: 'step1',
      stepId: 'step2',
      savedAt: 123,
      isEdit: false,
      clearFormValues: function (step: string): void {
        throw new Error('Function not implemented.');
      },
      handleSave: jest.fn()
    };
    const { getByTestId } = await renderComponent(
      testId,
      <EditForm {...props} />
    );
    const inputElement = getByTestId('DateRangePicker_onChange');
    expect(inputElement).toBeInTheDocument();

    fireEvent.change(inputElement, {
      target: {
        value: 'undefined',
        name: 'dateValue',
        date: new Date('2021/02/03'),
        view: 'month'
      }
    });
    expect(inputElement).toBeInTheDocument();
  });
});
