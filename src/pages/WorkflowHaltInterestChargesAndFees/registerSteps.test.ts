import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { default as stepRegistry } from '../_commons/WorkflowSetup/registries/stepRegistry';

const registerFunc = jest.spyOn(stepRegistry, 'registerStep');

describe('ManageCreditLimitWorkflow > registerSteps', () => {
  it('Should register components', async () => {
    await import('./registerSteps');

    expect(registerFunc).toBeCalledWith(
      'GetStartedHaltInterestChargesAndFees',
      expect.anything()
    );
    expect(registerFunc).toBeCalledWith(
      'ConfigureParametersHaltInterestChargesAndFees',
      expect.anything(),
      [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__HALT_INTEREST_CHARGES_AND_FEES_CONFIGURE_PARAMETERS
      ]
    );
  });
});
