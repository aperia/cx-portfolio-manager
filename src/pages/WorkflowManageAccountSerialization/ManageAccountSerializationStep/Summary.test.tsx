import { fireEvent, render, RenderResult } from '@testing-library/react';
import React from 'react';
import ManageAccountSerializationSummary from './Summary';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

jest.mock('./SummaryGrid', () => {
  return {
    __esModule: true,
    default: () => <div>SummaryGrid</div>
  };
});

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <ManageAccountSerializationSummary {...props} />
    </div>
  );
};

describe('ManageAccountSerializationSummary', () => {
  it('Should render summary ', () => {
    const onEditStepMock = jest.fn();
    const props = {
      onEditStep: onEditStepMock,
      formValues: {
        sysPrinAgents: undefined
      }
    } as any;
    const wrapper = renderComponent(props);

    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(onEditStepMock).toBeCalled();
  });

  it('Should render summary with paging ', () => {
    const mockData = [
      {
        sysPrinAgent: '0000-0200-3456',
        recordTypes: [
          {
            status: 'Marked for Deletion',
            id: 1018971639,
            actionCode: 'R',
            parameters: [
              {
                name: 'record.type',
                previousValue: 'S',
                newValue: ''
              },
              {
                name: 'generation.method',
                previousValue: 'R',
                newValue: ''
              },
              {
                name: 'range.from',
                previousValue: '15831',
                newValue: ''
              },
              {
                name: 'range.to',
                previousValue: '78332',
                newValue: ''
              },
              {
                name: 'serial.number',
                previousValue: '37428',
                newValue: ''
              },
              {
                name: 'warning.level',
                previousValue: '47',
                newValue: ''
              },
              {
                name: 'prefix',
                previousValue: '7720',
                newValue: ''
              },
              {
                name: 'alternate.prefix',
                previousValue: '88103',
                newValue: ''
              },
              {
                name: 'account.id.or.piid',
                previousValue: 'AC',
                newValue: ''
              },
              {
                name: 'next.from',
                previousValue: '20698',
                newValue: ''
              },
              {
                name: 'next.to',
                previousValue: '66089',
                newValue: ''
              },
              {
                name: 'available.record.number',
                previousValue: '1641635672',
                newValue: ''
              },
              {
                name: 'use.alternate.agent',
                previousValue: 'Y',
                newValue: ''
              },
              {
                name: 'alternate.principal',
                previousValue: '0020',
                newValue: ''
              },
              {
                name: 'alternate.agent',
                previousValue: '0011',
                newValue: ''
              }
            ]
          }
        ]
      }
    ];
    const onEditStepMock = jest.fn();
    const props = {
      onEditStep: onEditStepMock,
      formValues: {
        sysPrinAgents: mockData
      }
    } as any;
    const wrapper = renderComponent(props);
    expect(wrapper.getByText('SummaryGrid')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(onEditStepMock).toBeCalled();
  });
});
