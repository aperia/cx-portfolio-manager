import { RecordFieldParameterEnum } from 'app/constants/enums';
import { renderWithMockStore } from 'app/utils';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageAccountSerialization';
import React from 'react';
import { SubGrid } from './SubGrid';

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

const metadata = {
  accountIDOrPIID: [{ code: 'code', text: 'text' }],
  recordType: [{ code: 'code', text: 'text' }],
  generationMethod: [{ code: 'code', text: 'text' }],
  useAlternateAgent: [{ code: 'code', text: 'text' }]
};
const _props = { metadata, onChange: jest.fn(), searchValue: '' };

const useSelectElementMetadataForSPA = jest.spyOn(
  WorkflowSetup,
  'useSelectElementMetadataForSPA'
);

describe('pages > WorkflowManageAccountSerialization > ManageAccountSerializationStep > SubGrid', () => {
  beforeEach(() => {
    useSelectElementMetadataForSPA.mockReturnValue(metadata as any);
  });
  it('grid with status not updated', async () => {
    const props = {
      ..._props,
      row: {
        original: {
          parameters: [
            { name: 'record.type', previousValue: 'S', newValue: '' },
            { name: 'generation.method', previousValue: 'G', newValue: '' },
            { name: 'warning.level', previousValue: '93', newValue: '' },
            { name: 'account.id.or.piid', previousValue: 'PI', newValue: '' },
            {
              name: 'available.record.number',
              previousValue: '1663565961',
              newValue: ''
            }
          ],
          status: 'Not Updated',
          id: 1
        }
      }
    };
    const wrapper = await renderWithMockStore(<SubGrid {...props} />, {});

    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(wrapper.getByText('txt_green_screen_name')).toBeInTheDocument();
    expect(wrapper.getByText('txt_value')).toBeInTheDocument();
    expect(wrapper.getByText('txt_more_info')).toBeInTheDocument();
  });

  it('grid with status New', async () => {
    const props = {
      ..._props,
      row: {
        original: {
          parameters: [
            { name: 'record.type', previousValue: '', newValue: 'S' },
            { name: 'generation.method', previousValue: '', newValue: 'G' },
            { name: 'warning.level', previousValue: '', newValue: '93' },
            {
              name: RecordFieldParameterEnum.AccountIDOrPIID,
              previousValue: '',
              newValue: 'PI'
            },
            {
              name: RecordFieldParameterEnum.AvailableRecordNumber,
              previousValue: '',
              newValue: '1663565961'
            }
          ],
          status: 'New',
          id: 1
        }
      }
    };
    const wrapper = await renderWithMockStore(<SubGrid {...props} />, {});

    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(wrapper.getByText('txt_green_screen_name')).toBeInTheDocument();
    expect(wrapper.getByText('txt_value')).toBeInTheDocument();
    expect(wrapper.getByText('txt_more_info')).toBeInTheDocument();
  });

  it('grid with status New with empty data', async () => {
    const props = {
      ..._props,
      row: {
        original: {
          parameters: [
            {},
            { name: 'record.type', previousValue: '', newValue: '' },
            { name: 'generation.method', previousValue: '', newValue: 'T' },
            { name: 'warning.level', previousValue: '', newValue: '' },
            {
              name: RecordFieldParameterEnum.AccountIDOrPIID,
              previousValue: '',
              newValue: 'PI'
            },
            {
              name: RecordFieldParameterEnum.AvailableRecordNumber,
              previousValue: '',
              newValue: '1663565961'
            }
          ],
          status: 'New',
          id: 1
        }
      }
    };
    const wrapper = await renderWithMockStore(<SubGrid {...props} />, {});

    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(wrapper.getByText('txt_green_screen_name')).toBeInTheDocument();
    expect(wrapper.getByText('txt_value')).toBeInTheDocument();
    expect(wrapper.getByText('txt_more_info')).toBeInTheDocument();
  });

  it('grid with status Updated', async () => {
    const props = {
      ..._props,
      row: {
        original: {
          parameters: [
            { name: 'record.type', previousValue: '', newValue: 'S' },
            { name: 'generation.method', previousValue: '', newValue: 'G' },
            { name: 'warning.level', previousValue: '', newValue: '93' },
            {
              name: RecordFieldParameterEnum.AccountIDOrPIID,
              previousValue: '',
              newValue: 'PI'
            },
            {
              name: RecordFieldParameterEnum.AvailableRecordNumber,
              previousValue: '',
              newValue: '1663565961'
            }
          ],
          status: 'Updated',
          id: 1
        }
      }
    };
    const wrapper = await renderWithMockStore(<SubGrid {...props} />, {});

    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(wrapper.getByText('txt_green_screen_name')).toBeInTheDocument();
    expect(wrapper.getByText('txt_new_value')).toBeInTheDocument();
    expect(wrapper.getByText('txt_previous_value')).toBeInTheDocument();
    expect(wrapper.getByText('txt_more_info')).toBeInTheDocument();
  });

  it('grid with empty data', async () => {
    const props = {
      ..._props,
      row: {
        original: {}
      }
    };
    await renderWithMockStore(<SubGrid {...props} />, {});
  });
});
