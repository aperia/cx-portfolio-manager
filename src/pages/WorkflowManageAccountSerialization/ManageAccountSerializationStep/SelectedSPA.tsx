import { InlineMessage, useTranslation } from 'app/_libraries/_dls';
import React, { useMemo } from 'react';
import { ManageAccountSerializationFormValue } from '.';
import SPA from './SPAForm';
import SPAList from './SPAList';

export interface SelectedSPAProps
  extends WorkflowSetupProps<ManageAccountSerializationFormValue> {
  data: { sys: string[]; prin: MagicKeyValue; agent: MagicKeyValue };
}

const SelectedSPA: React.FC<SelectedSPAProps> = ({ data, ...props }) => {
  const { t } = useTranslation();
  const { formValues, setFormValues, isStuck } = props;

  const showSPAList = useMemo(() => {
    return formValues?.sysPrinAgents.length > 0;
  }, [formValues?.sysPrinAgents]);

  const handleContinue = (data: any) => {
    setFormValues({
      sysPrinAgents: [
        {
          sysPrinAgent: `${data.sys}-${data.prin}-${data.agent}`,
          recordTypes: []
        }
      ]
    });
  };

  return (
    <>
      {isStuck && (
        <InlineMessage className="mb-0 mt-24" variant="danger" withIcon>
          {t('txt_step_stuck_move_forward_message')}
        </InlineMessage>
      )}
      {!showSPAList && <SPA data={data} handleContinue={handleContinue} />}
      {showSPAList && <SPAList {...props} SPAs={data} />}
    </>
  );
};

export default SelectedSPA;
