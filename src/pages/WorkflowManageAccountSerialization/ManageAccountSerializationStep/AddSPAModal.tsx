import FieldTooltip from 'app/components/FieldTooltip';
import ModalRegistry from 'app/components/ModalRegistry';
import { useFormValidations } from 'app/hooks';
import {
  Button,
  ComboBox,
  DropdownBaseChangeEvent,
  InlineMessage,
  ModalBody,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty, orderBy } from 'app/_libraries/_dls/lodash';
import isFunction from 'lodash.isfunction';
import React, { useEffect, useMemo, useState } from 'react';
import { SPA_FORM_NAME } from './constants';

interface IProps {
  id: string;
  show: boolean;
  onClose?: () => void;
  handleAdd: (data: { sys: string; prin: string; agent: string }) => void;
  SPAs: { sys: string[]; prin: MagicKeyValue; agent: MagicKeyValue };
  arrSPAs: { sys: string; prin: string; agent: string }[];
}

const AddSPAModal: React.FC<IProps> = ({
  id,
  show,
  onClose,
  handleAdd,
  SPAs,
  arrSPAs
}) => {
  const { t } = useTranslation();
  const [dataForm, setDataForm] = useState({
    sys: '',
    prin: '',
    agent: ''
  });
  const [isExistSPA, setIsExistSPA] = useState<boolean>(false);
  const [resetTouch, setResetTouch] = useState(0);
  useEffect(() => {
    process.nextTick(() => {
      setResetTouch(t => t + 1);
    });
  }, [dataForm.sys, dataForm.prin]);

  const currentErrors = useMemo(
    () =>
      ({
        sys: isEmpty(dataForm.sys) && {
          status: true,
          message: t('txt_required_validation', {
            fieldName: 'Sys'
          })
        },
        prin: isEmpty(dataForm.prin) &&
          !isEmpty(dataForm.sys) && {
            status: true,
            message: t('txt_required_validation', {
              fieldName: 'Prin'
            })
          },
        agent: isEmpty(dataForm.agent) &&
          !isEmpty(dataForm.prin) &&
          !isEmpty(dataForm.sys) && {
            status: true,
            message: t('txt_required_validation', {
              fieldName: 'Agent'
            })
          }
      } as Record<any, IFormError>),
    [dataForm.sys, dataForm.prin, dataForm.agent, t]
  );

  const [, errors, setTouched] = useFormValidations<any>(
    currentErrors,
    resetTouch
  );

  const isValid = useMemo(() => {
    const props = ['sys', 'prin', 'agent'];
    return props.every(p => !errors[p]?.status);
  }, [errors]);

  const handleClose = () => {
    isFunction(onClose) && onClose();
  };

  const handleChange = (
    e: DropdownBaseChangeEvent,
    name: 'sys' | 'prin' | 'agent'
  ) => {
    const prevValue = dataForm[name];
    const nextValue = e.target.value;

    if (prevValue === nextValue) return;

    switch (name) {
      case SPA_FORM_NAME.AGENT:
        setDataForm({ ...dataForm, [name]: nextValue });
        break;
      case SPA_FORM_NAME.PRIN:
        setDataForm({
          ...dataForm,
          [name]: nextValue,
          [SPA_FORM_NAME.AGENT]: ''
        });
        break;
      case SPA_FORM_NAME.SYS:
        setDataForm({
          ...dataForm,
          [name]: nextValue,
          [SPA_FORM_NAME.PRIN]: '',
          [SPA_FORM_NAME.AGENT]: ''
        });
        break;
    }
  };

  const checkSPAs = () => {
    const _isExistSPA = arrSPAs.some(
      spa =>
        spa.sys === dataForm.sys &&
        spa.agent === dataForm.agent &&
        spa.prin === dataForm.prin
    );

    if (_isExistSPA) {
      setIsExistSPA(_isExistSPA);
    } else {
      handleAdd(dataForm);
    }
  };

  const sysItems = useMemo(() => {
    return SPAs.sys.map(w => <ComboBox.Item key={w} label={w} value={w} />);
  }, [SPAs]);

  const prinItems = useMemo(() => {
    const prinData = SPAs.prin[dataForm[SPA_FORM_NAME.SYS]] as string[];
    if (prinData) {
      return prinData.map(w => <ComboBox.Item key={w} label={w} value={w} />);
    }
    return <ComboBox.Item />;
  }, [SPAs, dataForm]);

  const agentItems = useMemo(() => {
    const agentData = SPAs.agent[
      `${dataForm[SPA_FORM_NAME.SYS]}${dataForm[SPA_FORM_NAME.PRIN]}`
    ] as string[];
    if (agentData) {
      return orderBy(agentData).map(w => (
        <ComboBox.Item key={w} label={w} value={w} />
      ));
    }
    return <ComboBox.Item />;
  }, [SPAs, dataForm]);

  return (
    <ModalRegistry id={id} show={show} onAutoClosedAll={handleClose} md>
      <ModalHeader border closeButton onHide={handleClose}>
        <ModalTitle>{t('txt_spa_sys_prin_agent_add')}</ModalTitle>
      </ModalHeader>
      <ModalBody>
        {isExistSPA && (
          <>
            <InlineMessage variant="danger" withIcon className="mb-16">
              {t('txt_spa_mgs_add_spa')}
            </InlineMessage>
          </>
        )}
        <div className="row">
          <div className="col-4">
            <FieldTooltip
              placement="right-start"
              element={
                <div>
                  <p className="pb-8">{t('txt_tooltip_sys')}</p>
                  <p>{t('txt_tooltip_sys_body')}</p>
                </div>
              }
            >
              <ComboBox
                id={`spa__workflow_sys`}
                label={t('txt_spa_sys')}
                required
                textField="text"
                noResult={t('txt_no_results_found')}
                value={dataForm[SPA_FORM_NAME.SYS]}
                onChange={e => handleChange(e, SPA_FORM_NAME.SYS)}
                onFocus={setTouched(SPA_FORM_NAME.SYS)}
                onBlur={setTouched(SPA_FORM_NAME.SYS, true)}
                error={errors[SPA_FORM_NAME.SYS]}
              >
                {sysItems}
              </ComboBox>
            </FieldTooltip>
          </div>
          <div className="col-4">
            <FieldTooltip
              placement="right-start"
              element={
                <div>
                  <p className="pb-8">{t('txt_tooltip_prin')}</p>
                  <p>{t('txt_tooltip_prin_body')}</p>
                </div>
              }
            >
              <ComboBox
                id={`spa__workflow_prin`}
                label={t('txt_spa_prin')}
                required
                textField="text"
                noResult={t('txt_no_results_found')}
                value={dataForm[SPA_FORM_NAME.PRIN]}
                onChange={e => handleChange(e, SPA_FORM_NAME.PRIN)}
                disabled={isEmpty(dataForm[SPA_FORM_NAME.SYS])}
                onFocus={setTouched(SPA_FORM_NAME.PRIN)}
                onBlur={setTouched(SPA_FORM_NAME.PRIN, true)}
                error={errors[SPA_FORM_NAME.PRIN]}
              >
                {prinItems}
              </ComboBox>
            </FieldTooltip>
          </div>
          <div className="col-4">
            <FieldTooltip
              placement="right-start"
              element={
                <div>
                  <p className="pb-8">{t('txt_tooltip_agent')}</p>
                  <p>{t('txt_tooltip_agent_body')}</p>
                </div>
              }
            >
              <ComboBox
                id={`spa__workflow_agent`}
                label={t('txt_spa_agent')}
                required
                textField="text"
                noResult={t('txt_no_results_found')}
                value={dataForm[SPA_FORM_NAME.AGENT]}
                onChange={e => handleChange(e, SPA_FORM_NAME.AGENT)}
                disabled={isEmpty(dataForm[SPA_FORM_NAME.PRIN])}
                onFocus={setTouched(SPA_FORM_NAME.AGENT)}
                onBlur={setTouched(SPA_FORM_NAME.AGENT, true)}
                error={errors[SPA_FORM_NAME.AGENT]}
              >
                {agentItems}
              </ComboBox>
            </FieldTooltip>
          </div>
        </div>
      </ModalBody>
      <div className="dls-modal-footer modal-footer">
        <Button variant="secondary" onClick={handleClose}>
          {t('txt_cancel')}
        </Button>
        <Button
          variant="primary"
          onClick={checkSPAs}
          disabled={isEmpty(dataForm[SPA_FORM_NAME.AGENT]) || !isValid}
        >
          {t('txt_add')}
        </Button>
      </div>
    </ModalRegistry>
  );
};

export default AddSPAModal;
