import ModalRegistry from 'app/components/ModalRegistry';
import { RecordFieldParameterEnum } from 'app/constants/enums';
import {
  Button,
  ModalBody,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty } from 'lodash';
import isFunction from 'lodash.isfunction';
import React from 'react';
import { STATUS_SPA } from './helper';

interface IProps {
  id: string;
  show: boolean;
  onClose?: (reload?: boolean) => void;
  infoSpa: MagicKeyValue;
  checkedList: string[];
  data: MagicKeyValue[];
  isBulk: boolean;
}

const RemoveRecordModal: React.FC<IProps> = ({
  id,
  show,
  onClose,
  infoSpa,
  checkedList,
  data,
  isBulk
}) => {
  const { t } = useTranslation();

  const isMarkDelete = infoSpa?.status === STATUS_SPA.MarkedForDeletion;

  const isUpdated = infoSpa?.status === STATUS_SPA.Updated;

  const isHavingGenerateMethod = data
    ?.filter((item: MagicKeyValue) => checkedList?.includes(item.id))
    ?.some((item: MagicKeyValue) => item.generationMethod === 'G');

  const isGeneratedGenerationMethod =
    ((infoSpa?.status === STATUS_SPA.NotUpdated &&
      infoSpa?.parameters?.find(
        (el: MagicKeyValue) =>
          el.name === RecordFieldParameterEnum.GenerationMethod
      )?.previousValue) ||
      (isUpdated &&
        infoSpa?.parameters?.find(
          (el: MagicKeyValue) =>
            el.name === RecordFieldParameterEnum.GenerationMethod
        )?.newValue) ||
      (infoSpa?.status === STATUS_SPA.New &&
        infoSpa?.parameters?.find(
          (el: MagicKeyValue) =>
            el.name === RecordFieldParameterEnum.GenerationMethod
        )?.newValue &&
        isEmpty(
          infoSpa?.parameters?.find(
            (el: MagicKeyValue) =>
              el.name === RecordFieldParameterEnum.GenerationMethod
          )?.previousValue
        ))) === 'G';

  const onRemove = async () => {
    isFunction(onClose) && onClose(true);
  };

  const handleCloseWithoutReload = () => {
    isFunction(onClose) && onClose(false);
  };

  const dynamicText = () => {
    if (isGeneratedGenerationMethod && !isBulk && !isUpdated)
      return (
        <div>
          {t('txt_delete_body_msg_generated_generation_method')}
          <p className="mt-8">{t('txt_delete_body_msg')}</p>
        </div>
      );

    if (isMarkDelete && !isBulk)
      return <span>{t('txt_cancel_delete_modal_body')}</span>;

    if (checkedList?.length > 1 && !isHavingGenerateMethod && isBulk)
      return <span>{t('txt_delete_body_msg_plural')}</span>;

    if (checkedList?.length > 1 && isHavingGenerateMethod && isBulk)
      return (
        <div>
          {t('txt_delete_body_msg_generated_generation_method')}
          <p className="mt-8">{t('txt_delete_body_msg_plural')}</p>
        </div>
      );

    if (isUpdated) {
      return <span>{t('txt_delete_cancel_msg')}</span>;
    }

    return <span>{t('txt_delete_body_msg')}</span>;
  };

  return (
    <ModalRegistry
      id={id}
      show={show}
      onAutoClosedAll={handleCloseWithoutReload}
      classes={{ content: '' }}
    >
      <ModalHeader border closeButton onHide={handleCloseWithoutReload}>
        <ModalTitle>
          {(isMarkDelete && !isBulk) || isUpdated
            ? t('txt_confirm_cancel')
            : t('txt_confirm_deletion')}
        </ModalTitle>
      </ModalHeader>
      <ModalBody>{dynamicText()}</ModalBody>
      <div className="dls-modal-footer modal-footer">
        <Button variant="secondary" onClick={handleCloseWithoutReload}>
          {isUpdated ? t('txt_close') : t('txt_cancel')}
        </Button>
        <Button variant="danger" onClick={onRemove}>
          {isMarkDelete || isUpdated ? t('txt_confirm') : t('txt_delete')}
        </Button>
      </div>
    </ModalRegistry>
  );
};

export default RemoveRecordModal;
