import {
  RecordFieldParameterEnum,
  SPAFieldParameterEnum
} from 'app/constants/enums';
import { isEqual } from 'app/_libraries/_dls/lodash';

export const parseDefaultRecordType = (
  data: MagicKeyValue,
  initData?: MagicKeyValue
) => {
  return {
    ...initData,
    [RecordFieldParameterEnum.RecordType]:
      data.status === STATUS_SPA.New
        ? data?.parameters?.find(
            (o: MagicKeyValue) => o.name === RecordFieldParameterEnum.RecordType
          )?.newValue
        : data.status === STATUS_SPA.Updated
        ? data?.parameters?.find(
            (o: MagicKeyValue) => o.name === RecordFieldParameterEnum.RecordType
          )?.newValue
        : data?.parameters?.find(
            (o: MagicKeyValue) => o.name === RecordFieldParameterEnum.RecordType
          )?.previousValue,
    [RecordFieldParameterEnum.AccountIDOrPIID]:
      data.status === STATUS_SPA.New
        ? data?.parameters?.find(
            (o: MagicKeyValue) =>
              o.name === RecordFieldParameterEnum.AccountIDOrPIID
          )?.newValue
        : data.status === STATUS_SPA.Updated
        ? data?.parameters?.find(
            (o: MagicKeyValue) =>
              o.name === RecordFieldParameterEnum.AccountIDOrPIID
          )?.newValue
        : data?.parameters?.find(
            (o: MagicKeyValue) =>
              o.name === RecordFieldParameterEnum.AccountIDOrPIID
          )?.previousValue,
    [RecordFieldParameterEnum.AlternatePrefix]:
      data.status === STATUS_SPA.New
        ? data?.parameters?.find(
            (o: MagicKeyValue) =>
              o.name === RecordFieldParameterEnum.AlternatePrefix
          )?.newValue
        : data.status === STATUS_SPA.Updated
        ? data?.parameters?.find(
            (o: MagicKeyValue) =>
              o.name === RecordFieldParameterEnum.AlternatePrefix
          )?.newValue
        : data?.parameters?.find(
            (o: MagicKeyValue) =>
              o.name === RecordFieldParameterEnum.AlternatePrefix
          )?.previousValue,
    [RecordFieldParameterEnum.AvailableRecordNumber]:
      data.status === STATUS_SPA.New
        ? data?.parameters?.find(
            (o: MagicKeyValue) =>
              o.name === RecordFieldParameterEnum.AvailableRecordNumber
          )?.newValue
        : data.status === STATUS_SPA.Updated
        ? data?.parameters?.find(
            (o: MagicKeyValue) =>
              o.name === RecordFieldParameterEnum.AvailableRecordNumber
          )?.newValue
        : data?.parameters?.find(
            (o: MagicKeyValue) =>
              o.name === RecordFieldParameterEnum.AvailableRecordNumber
          )?.previousValue,
    [RecordFieldParameterEnum.GenerationMethod]:
      data.status === STATUS_SPA.New
        ? data?.parameters?.find(
            (o: MagicKeyValue) =>
              o.name === RecordFieldParameterEnum.GenerationMethod
          )?.newValue
        : data.status === STATUS_SPA.Updated
        ? data?.parameters?.find(
            (o: MagicKeyValue) =>
              o.name === RecordFieldParameterEnum.GenerationMethod
          )?.newValue
        : data?.parameters?.find(
            (o: MagicKeyValue) =>
              o.name === RecordFieldParameterEnum.GenerationMethod
          )?.previousValue,
    [RecordFieldParameterEnum.NextFrom]:
      data.status === STATUS_SPA.New
        ? data?.parameters?.find(
            (o: MagicKeyValue) => o.name === RecordFieldParameterEnum.NextFrom
          )?.newValue
        : data.status === STATUS_SPA.Updated
        ? data?.parameters?.find(
            (o: MagicKeyValue) => o.name === RecordFieldParameterEnum.NextFrom
          )?.newValue
        : data?.parameters?.find(
            (o: MagicKeyValue) => o.name === RecordFieldParameterEnum.NextFrom
          )?.previousValue,
    [RecordFieldParameterEnum.NextTo]:
      data.status === STATUS_SPA.New
        ? data?.parameters?.find(
            (o: MagicKeyValue) => o.name === RecordFieldParameterEnum.NextTo
          )?.newValue
        : data.status === STATUS_SPA.Updated
        ? data?.parameters?.find(
            (o: MagicKeyValue) => o.name === RecordFieldParameterEnum.NextTo
          )?.newValue
        : data?.parameters?.find(
            (o: MagicKeyValue) => o.name === RecordFieldParameterEnum.NextTo
          )?.previousValue,
    [RecordFieldParameterEnum.Prefix]:
      data.status === STATUS_SPA.New
        ? data?.parameters?.find(
            (o: MagicKeyValue) => o.name === RecordFieldParameterEnum.Prefix
          )?.newValue
        : data.status === STATUS_SPA.Updated
        ? data?.parameters?.find(
            (o: MagicKeyValue) => o.name === RecordFieldParameterEnum.Prefix
          )?.newValue
        : data?.parameters?.find(
            (o: MagicKeyValue) => o.name === RecordFieldParameterEnum.Prefix
          )?.previousValue,
    [RecordFieldParameterEnum.RangeFrom]:
      data.status === STATUS_SPA.New
        ? data?.parameters?.find(
            (o: MagicKeyValue) => o.name === RecordFieldParameterEnum.RangeFrom
          )?.newValue
        : data.status === STATUS_SPA.Updated
        ? data?.parameters?.find(
            (o: MagicKeyValue) => o.name === RecordFieldParameterEnum.RangeFrom
          )?.newValue
        : data?.parameters?.find(
            (o: MagicKeyValue) => o.name === RecordFieldParameterEnum.RangeFrom
          )?.previousValue,
    [RecordFieldParameterEnum.RangeTo]:
      data.status === STATUS_SPA.New
        ? data?.parameters?.find(
            (o: MagicKeyValue) => o.name === RecordFieldParameterEnum.RangeTo
          )?.newValue
        : data.status === STATUS_SPA.Updated
        ? data?.parameters?.find(
            (o: MagicKeyValue) => o.name === RecordFieldParameterEnum.RangeTo
          )?.newValue
        : data?.parameters?.find(
            (o: MagicKeyValue) => o.name === RecordFieldParameterEnum.RangeTo
          )?.previousValue,
    [RecordFieldParameterEnum.SerialNumber]:
      data.status === STATUS_SPA.New
        ? data?.parameters?.find(
            (o: MagicKeyValue) =>
              o.name === RecordFieldParameterEnum.SerialNumber
          )?.newValue
        : data.status === STATUS_SPA.Updated
        ? data?.parameters?.find(
            (o: MagicKeyValue) =>
              o.name === RecordFieldParameterEnum.SerialNumber
          )?.newValue
        : data?.parameters?.find(
            (o: MagicKeyValue) =>
              o.name === RecordFieldParameterEnum.SerialNumber
          )?.previousValue,
    [RecordFieldParameterEnum.UseAlternateAgent]:
      data.status === STATUS_SPA.New
        ? data?.parameters?.find(
            (o: MagicKeyValue) =>
              o.name === RecordFieldParameterEnum.UseAlternateAgent
          )?.newValue
        : data.status === STATUS_SPA.Updated
        ? data?.parameters?.find(
            (o: MagicKeyValue) =>
              o.name === RecordFieldParameterEnum.UseAlternateAgent
          )?.newValue
        : data?.parameters?.find(
            (o: MagicKeyValue) =>
              o.name === RecordFieldParameterEnum.UseAlternateAgent
          )?.previousValue,
    [RecordFieldParameterEnum.WarningLevel]:
      data.status === STATUS_SPA.New
        ? data?.parameters?.find(
            (o: MagicKeyValue) =>
              o.name === RecordFieldParameterEnum.WarningLevel
          )?.newValue
        : data.status === STATUS_SPA.Updated
        ? data?.parameters?.find(
            (o: MagicKeyValue) =>
              o.name === RecordFieldParameterEnum.WarningLevel
          )?.newValue
        : data?.parameters?.find(
            (o: MagicKeyValue) =>
              o.name === RecordFieldParameterEnum.WarningLevel
          )?.previousValue,
    [RecordFieldParameterEnum.AlternatePrincipal]:
      data.status === STATUS_SPA.New
        ? data?.parameters?.find(
            (o: MagicKeyValue) =>
              o.name === RecordFieldParameterEnum.AlternatePrincipal
          )?.newValue
        : data.status === STATUS_SPA.Updated
        ? data?.parameters?.find(
            (o: MagicKeyValue) =>
              o.name === RecordFieldParameterEnum.AlternatePrincipal
          )?.newValue
        : data?.parameters?.find(
            (o: MagicKeyValue) =>
              o.name === RecordFieldParameterEnum.AlternatePrincipal
          )?.previousValue,
    [RecordFieldParameterEnum.AlternateAgent]:
      data.status === STATUS_SPA.New
        ? data?.parameters?.find(
            (o: MagicKeyValue) =>
              o.name === RecordFieldParameterEnum.AlternateAgent
          )?.newValue
        : data.status === STATUS_SPA.Updated
        ? data?.parameters?.find(
            (o: MagicKeyValue) =>
              o.name === RecordFieldParameterEnum.AlternateAgent
          )?.newValue
        : data?.parameters?.find(
            (o: MagicKeyValue) =>
              o.name === RecordFieldParameterEnum.AlternateAgent
          )?.previousValue
  };
};

export const uniqueArr = (arr: any, key: any) => {
  return [...(new Map(arr.map((el: any) => [key(el), el])).values() as any)];
};

export const mapParameter = (data: MagicKeyValue) => {
  const _object = data;
  let parameter: MagicKeyValue[] = [];
  for (const [key, value] of Object.entries(_object)) {
    switch (key) {
      case SPAFieldParameterEnum.RecordType:
        parameter = [
          ...parameter,
          {
            name: RecordFieldParameterEnum.RecordType,
            previousValue: value.toString(),
            newValue: ''
          }
        ];
        break;
      case SPAFieldParameterEnum.GenerationMethod:
        parameter = [
          ...parameter,
          {
            name: RecordFieldParameterEnum.GenerationMethod,
            previousValue: value.toString(),
            newValue: ''
          }
        ];
        break;
      case SPAFieldParameterEnum.RangeFrom:
        parameter = [
          ...parameter,
          {
            name: RecordFieldParameterEnum.RangeFrom,
            previousValue: value.toString(),
            newValue: ''
          }
        ];
        break;
      case SPAFieldParameterEnum.RangeTo:
        parameter = [
          ...parameter,
          {
            name: RecordFieldParameterEnum.RangeTo,
            previousValue: value.toString(),
            newValue: ''
          }
        ];
        break;
      case SPAFieldParameterEnum.SerialNumber:
        parameter = [
          ...parameter,
          {
            name: RecordFieldParameterEnum.SerialNumber,
            previousValue: value.toString(),
            newValue: ''
          }
        ];
        break;
      case SPAFieldParameterEnum.WarningLevel:
        parameter = [
          ...parameter,
          {
            name: RecordFieldParameterEnum.WarningLevel,
            previousValue: value.toString(),
            newValue: ''
          }
        ];
        break;
      case SPAFieldParameterEnum.Prefix:
        parameter = [
          ...parameter,
          {
            name: RecordFieldParameterEnum.Prefix,
            previousValue: value.toString(),
            newValue: ''
          }
        ];
        break;
      case SPAFieldParameterEnum.AlternatePrefix:
        parameter = [
          ...parameter,
          {
            name: RecordFieldParameterEnum.AlternatePrefix,
            previousValue: value.toString(),
            newValue: ''
          }
        ];
        break;
      case SPAFieldParameterEnum.AccountIDOrPIID:
        parameter = [
          ...parameter,
          {
            name: RecordFieldParameterEnum.AccountIDOrPIID,
            previousValue: value.toString(),
            newValue: ''
          }
        ];
        break;
      case SPAFieldParameterEnum.NextFrom:
        parameter = [
          ...parameter,
          {
            name: RecordFieldParameterEnum.NextFrom,
            previousValue: value.toString(),
            newValue: ''
          }
        ];
        break;
      case SPAFieldParameterEnum.NextTo:
        parameter = [
          ...parameter,
          {
            name: RecordFieldParameterEnum.NextTo,
            previousValue: value.toString(),
            newValue: ''
          }
        ];
        break;
      case SPAFieldParameterEnum.AvailableRecordNumber:
        parameter = [
          ...parameter,
          {
            name: RecordFieldParameterEnum.AvailableRecordNumber,
            previousValue: value.toString(),
            newValue: ''
          }
        ];
        break;
      case SPAFieldParameterEnum.UseAlternateAgent:
        parameter = [
          ...parameter,
          {
            name: RecordFieldParameterEnum.UseAlternateAgent,
            previousValue: value.toString(),
            newValue: ''
          }
        ];
        break;
      case SPAFieldParameterEnum.AlternatePrincipal:
        parameter = [
          ...parameter,
          {
            name: RecordFieldParameterEnum.AlternatePrincipal,
            previousValue: value.toString(),
            newValue: ''
          }
        ];
        break;
      case SPAFieldParameterEnum.AlternateAgent:
        parameter = [
          ...parameter,
          {
            name: RecordFieldParameterEnum.AlternateAgent,
            previousValue: value.toString(),
            newValue: ''
          }
        ];
        break;
      default:
        break;
    }
  }
  return parameter;
};

export const mapDataToFormSPA = (data: MagicKeyValue[]) => {
  return data?.map(record => {
    const parameters = mapParameter(record);
    return {
      status: 'Not Updated',
      id: record.id,
      actionCode: '',
      parameters
    };
  });
};

export const convertParameter = (data?: MagicKeyValue[] | undefined) => {
  if (!data) return [];
  return mapParameter(data);
};

export enum STATUS_SPA {
  New = 'New',
  Updated = 'Updated',
  NotUpdated = 'Not Updated',
  MarkedForDeletion = 'Marked for Deletion'
}

export const mapColorBadge = (status: string) => {
  switch (status) {
    case STATUS_SPA.New:
      return 'purple';
    case STATUS_SPA.Updated:
      return 'green';
    case STATUS_SPA.MarkedForDeletion:
      return 'red';
    case STATUS_SPA.NotUpdated:
    default:
      return 'grey';
  }
};

export const formatSysPrinAgentObjectArr = (data: MagicKeyValue) => {
  if (!data) return [];
  return data.map((item: MagicKeyValue) => ({
    code: item,
    text: item
  }));
};

export const formatDropdownValue = (data: MagicKeyValue) => {
  if (!data) return [];
  return data.map((item: MagicKeyValue) => ({
    code: item.value,
    text: `${item.value ? item.value + ' - ' : ''}${item.description}`
  }));
};

export const isEqualOriginalData = (
  originData: MagicKeyValue,
  newData: MagicKeyValue
) => {
  const origin = originData?.map((item: MagicKeyValue) => item.previousValue);
  const update = newData?.parameters?.map(
    (item: MagicKeyValue) => item.newValue
  );
  return isEqual(origin, update);
};
