import { DEFAULT_FILTER_VALUE } from 'app/constants/constants';
import { RecordFieldParameterEnum } from 'app/constants/enums';
import { mapGridExpandCollapse } from 'app/helpers';
import {
  Badge,
  Button,
  ColumnType,
  Grid,
  Icon,
  SortType,
  Tooltip,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import { differenceWith, orderBy } from 'app/_libraries/_dls/lodash';
import classNames from 'classnames';
import _, { isEmpty } from 'lodash';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import {
  mapCodeAndTextToText,
  mapValueAndDescToRefItem
} from 'pages/_commons/redux/WorkflowSetup/select-hooks/_helper';
import Paging from 'pages/_commons/Utils/Paging';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';
import { ManageAccountSerializationFormValue } from '.';
import AddNewRecordModal from './AddNewRecordModal';
import {
  convertParameter,
  isEqualOriginalData,
  mapColorBadge,
  mapDataToFormSPA,
  STATUS_SPA
} from './helper';
import RemoveRecordModal from './RemoveRecordModal';
import { SubGrid } from './SubGrid';

export interface SPAGridProps
  extends WorkflowSetupProps<ManageAccountSerializationFormValue> {
  dataForm: { sys: string; prin: string; agent: string };
  SPAs: { sys: string[]; prin: MagicKeyValue; agent: MagicKeyValue };
  searchValue: string;
  handleHideSPA: (spa: { sys: string; prin: string; agent: string }) => void;
  isClearAndReset: boolean;
  hasSearch: boolean;
  resetDefaultFilter: () => void;
  isResetSort: boolean;
  resetSort: () => void;
  resetClearAndReset: () => void;
  order: number;
}

const SPAGrid: React.FC<SPAGridProps> = ({
  dataForm,
  SPAs,
  setFormValues,
  formValues,
  searchValue,
  handleHideSPA,
  isClearAndReset,
  hasSearch,
  resetDefaultFilter,
  isResetSort,
  resetSort,
  selectedStep,
  stepId,
  resetClearAndReset,
  order
}) => {
  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();
  const dispatch = useDispatch();

  const id = `spa_${dataForm?.sys}_${dataForm?.prin}_${dataForm?.agent}`;

  const sysPrinAgents = useMemo(
    () => formValues.sysPrinAgents,
    [formValues.sysPrinAgents]
  );
  const currentId = useMemo(
    () => `${dataForm?.sys}-${dataForm?.prin}-${dataForm?.agent}`,
    [dataForm]
  );
  const currentSysPrinAgent = useMemo(
    () => sysPrinAgents?.find(item => currentId === item.sysPrinAgent),
    [currentId, sysPrinAgents]
  );
  const originalData = useMemo(
    () => currentSysPrinAgent?.originalData,
    [currentSysPrinAgent?.originalData]
  );

  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;

  const [data, setData] = useState<MagicKeyValue[]>([]);

  const [columns, setColumns] = useState<ColumnType[]>([]);
  const [expandedList, setExpandedList] = useState<string[]>([]);
  const [isExpand, setIsExpand] = useState<boolean>(true);
  const [checkedList, setCheckedList] = useState<string[]>([]);
  const [isBulk, setIsBulk] = useState<boolean>(false);
  const [currentPageSize, setCurrentPageSize] = useState(
    DEFAULT_FILTER_VALUE.pageSize as number
  );
  const [currentPage, setCurrentPage] = useState(1);
  const [sort, setSort] = useState<SortType>({
    id: RecordFieldParameterEnum.RangeFrom,
    order: undefined
  });

  const [dataGrid, setDataGrid] = useState<any>([]);
  const [dataSearch, setDataSearch] = useState<any>([]);

  const [openAddRecordModal, setOpenAddRecordModal] = useState<boolean>(false);
  const [openEditRecordModal, setOpenEditRecordModal] =
    useState<boolean>(false);
  const [openDeleteRecordModal, setOpenDeleteRecordModal] =
    useState<boolean>(false);
  const [selectedRecord, setSelectedRecord] = useState<MagicKeyValue>({});
  const [dataChecked, setDataChecked] = useState<string[]>([]);
  const [isCheckedAll, setIsCheckedAll] = useState<boolean>(false);

  const handleBulkDelete = () => {
    setOpenDeleteRecordModal(true);
    setIsBulk(true);
  };

  const formatDataToForm = useCallback(
    (data: MagicKeyValue[], allData: MagicKeyValue) => {
      const recordTypes = mapDataToFormSPA(data);
      keepRef.current.setFormValues({
        sysPrinAgents: [
          ...sysPrinAgents.filter(item => item.sysPrinAgent !== currentId),
          {
            sysPrinAgent: currentId,
            recordTypes,
            originalData: allData
          }
        ],
        isValid: true
      });
      setDataGrid(recordTypes);
    },
    [currentId, sysPrinAgents]
  );

  const fetchData = useCallback(async () => {
    const response: MagicKeyValue = await Promise.resolve(
      dispatch(actionsWorkflowSetup.getSPAListWorkflow({ dataForm }))
    );
    const dataGridTemp = response?.payload?.data?.recordTypes;
    //get default data
    const _allData = response?.payload?.data;
    setData(dataGridTemp);

    const allData = {
      recordTypes: _allData?.recordTypes,
      defaultPrefix: _allData?.defaultPrefix,
      generationMethods: _allData?.defaultGenerationMethods
        ?.map(mapValueAndDescToRefItem)
        ?.map(mapCodeAndTextToText),
      types: _allData?.defaultTypes
        ?.map(mapValueAndDescToRefItem)
        ?.map(mapCodeAndTextToText),
      useAlternateAgents: _allData?.defaultUseAlternateAgents
        ?.map(mapValueAndDescToRefItem)
        ?.map(mapCodeAndTextToText)
    };
    // mapping Data
    formatDataToForm(dataGridTemp, allData);
  }, [dataForm, dispatch, formatDataToForm]);

  const handleExpand = (dataKeyList: string[]) => {
    setExpandedList(dataKeyList);
  };

  const renderSubRow = (row: any) => (
    <SubGrid row={row} originalData={originalData} />
  );

  const handleAddRecord = () => {
    setOpenAddRecordModal(true);
  };

  const handleEditRecord = (selectData: MagicKeyValue) => {
    setSelectedRecord(selectData);
    setOpenEditRecordModal(true);
  };

  const handleDeleteRecord = (selectData: MagicKeyValue) => {
    setSelectedRecord(selectData);
    setOpenDeleteRecordModal(true);
  };

  const handleCancelRecord = useCallback((selectData: MagicKeyValue) => {
    setSelectedRecord(selectData);
    setOpenDeleteRecordModal(true);
  }, []);

  const onPageChange = useCallback((pageSelected: number) => {
    setCurrentPage(pageSelected);
  }, []);

  const onPageSizeChange = useCallback((size: number) => {
    setCurrentPage(1);
    setCurrentPageSize(size);
  }, []);

  const onSortChange = useCallback((sort: SortType) => {
    setSort(sort);
  }, []);

  const handleUpdateExpand = (id: string) => {
    setExpandedList([...expandedList, id]);
  };

  const handleCloseDeleteModal = (reload: boolean | undefined) => {
    const newDataGrid = dataGrid.slice() as MagicKeyValue[];
    setIsBulk(false);
    if (reload && isBulk) {
      checkedList.map(id => {
        const index = newDataGrid.findIndex(
          (item: MagicKeyValue) => item.id === id
        );

        if (newDataGrid[index]?.status === STATUS_SPA.New) {
          newDataGrid.splice(index, 1);
        } else {
          const originalSelectedRecordType = originalData?.recordTypes?.[index];
          newDataGrid[index] = {
            ...newDataGrid[index],
            actionCode: 'R',
            status: STATUS_SPA.MarkedForDeletion,
            parameters: convertParameter(originalSelectedRecordType)
          };
        }

        setDataGrid(newDataGrid);
      });
      const cloneSysPrinAgents = sysPrinAgents.slice();
      const selectedSysPrinAgentsIndex = cloneSysPrinAgents.findIndex(
        (item: MagicKeyValue) =>
          item.sysPrinAgent ===
          `${dataForm?.sys}-${dataForm?.prin}-${dataForm?.agent}`
      );

      cloneSysPrinAgents[selectedSysPrinAgentsIndex] = {
        ...cloneSysPrinAgents[selectedSysPrinAgentsIndex],
        recordTypes: newDataGrid
      };

      keepRef.current.setFormValues({
        sysPrinAgents: cloneSysPrinAgents,
        isValid: true
      });
      setDataChecked([]);
      setCheckedList([]);

      isCheckedAll && setIsCheckedAll(false);
    }
    if (reload && selectedRecord && selectedRecord.status === STATUS_SPA.New) {
      const removeGrid = newDataGrid.filter(
        (item: MagicKeyValue) => item.id !== selectedRecord.id
      );

      setDataGrid(removeGrid);

      const indexChecked = checkedList.indexOf(selectedRecord.id);
      const indexCheckedData = dataChecked.indexOf(selectedRecord.id);
      if (indexChecked !== -1) {
        const temp = [...checkedList];
        temp.splice(indexChecked, 1);
        setCheckedList(temp);
        const tempData = [...dataChecked];
        tempData.splice(indexCheckedData, 1);
        setDataChecked(tempData);
      }

      const cloneSysPrinAgents = sysPrinAgents.slice();
      const selectedSysPrinAgentsIndex = cloneSysPrinAgents.findIndex(
        (item: MagicKeyValue) =>
          item.sysPrinAgent ===
          `${dataForm?.sys}-${dataForm?.prin}-${dataForm?.agent}`
      );

      cloneSysPrinAgents[selectedSysPrinAgentsIndex] = {
        ...cloneSysPrinAgents[selectedSysPrinAgentsIndex],
        recordTypes: removeGrid
      };

      keepRef.current.setFormValues({
        sysPrinAgents: cloneSysPrinAgents,
        isValid: true
      });
    }
    if (
      reload &&
      selectedRecord &&
      STATUS_SPA.NotUpdated === selectedRecord.status &&
      !isBulk
    ) {
      const selectedIndex = newDataGrid.findIndex(
        (item: MagicKeyValue) => item.id === selectedRecord.id
      );

      const indexChecked = checkedList.indexOf(selectedRecord.id);
      const indexCheckedData = dataChecked.indexOf(selectedRecord.id);
      if (indexChecked !== -1) {
        const temp = [...checkedList];
        temp.splice(indexChecked, 1);
        setCheckedList(temp);
        const tempData = [...dataChecked];
        tempData.splice(indexCheckedData, 1);
        setDataChecked(tempData);
      }

      newDataGrid[selectedIndex] = {
        ...newDataGrid[selectedIndex],
        actionCode: 'R',
        status: STATUS_SPA.MarkedForDeletion
      };

      setDataGrid(newDataGrid);

      const cloneSysPrinAgents = sysPrinAgents.slice();
      const selectedSysPrinAgentsIndex = cloneSysPrinAgents.findIndex(
        (item: MagicKeyValue) =>
          item.sysPrinAgent ===
          `${dataForm?.sys}-${dataForm?.prin}-${dataForm?.agent}`
      );

      cloneSysPrinAgents[selectedSysPrinAgentsIndex] = {
        ...cloneSysPrinAgents[selectedSysPrinAgentsIndex],
        recordTypes: newDataGrid
      };

      keepRef.current.setFormValues({
        sysPrinAgents: cloneSysPrinAgents,
        isValid: true
      });
      setSelectedRecord({});
      setOpenDeleteRecordModal(false);
      return;
    }
    if (
      reload &&
      selectedRecord &&
      [STATUS_SPA.MarkedForDeletion, STATUS_SPA.Updated].includes(
        selectedRecord.status
      ) &&
      !isBulk
    ) {
      const selectedIndex = newDataGrid.findIndex(
        (item: MagicKeyValue) => item.id === selectedRecord.id
      );

      const originalSelectedRecordType =
        originalData?.recordTypes?.[selectedIndex];

      newDataGrid[selectedIndex] = {
        ...newDataGrid[selectedIndex],
        actionCode: '',
        status: STATUS_SPA.NotUpdated,
        parameters: convertParameter(originalSelectedRecordType)
      };

      setDataGrid(newDataGrid);

      const cloneSysPrinAgents = sysPrinAgents.slice();
      const selectedSysPrinAgentsIndex = cloneSysPrinAgents.findIndex(
        (item: MagicKeyValue) =>
          item.sysPrinAgent ===
          `${dataForm?.sys}-${dataForm?.prin}-${dataForm?.agent}`
      );

      cloneSysPrinAgents[selectedSysPrinAgentsIndex] = {
        ...cloneSysPrinAgents[selectedSysPrinAgentsIndex],
        recordTypes: newDataGrid
      };

      keepRef.current.setFormValues({
        sysPrinAgents: cloneSysPrinAgents,
        isValid: true
      });
      setSelectedRecord({});
    }
    setOpenDeleteRecordModal(false);
  };

  useEffect(() => {
    const sysPrinAgentsData = sysPrinAgents.find(
      (item: MagicKeyValue) =>
        item.sysPrinAgent ===
        `${dataForm?.sys}-${dataForm?.prin}-${dataForm?.agent}`
    )?.recordTypes;
    if (isEmpty(data) && isEmpty(sysPrinAgentsData)) {
      fetchData();
    }
  }, [
    data,
    dataForm?.agent,
    dataForm?.prin,
    dataForm?.sys,
    fetchData,
    sysPrinAgents
  ]);

  useEffect(() => {
    if (isResetSort) {
      setSort({
        id: RecordFieldParameterEnum.RangeFrom,
        order: undefined
      });
      resetSort();
    }
  }, [isResetSort, resetSort]);

  useEffect(() => {
    const isSPA = `${dataForm?.sys} ${dataForm?.prin} ${dataForm?.agent}`
      .toLowerCase()
      .includes(searchValue?.trim().toLowerCase() || '');
    if (isSPA) {
      setDataSearch(dataGrid);
    } else {
      /* istanbul ignore next */
      const dataFilter = dataGrid?.filter((spaItem: MagicKeyValue) => {
        const param = spaItem?.parameters as MagicKeyValue[];

        const recordType = param?.find(
          (o: MagicKeyValue) => o.name === RecordFieldParameterEnum.RecordType
        );
        const valRecordType =
          spaItem?.status === STATUS_SPA.NotUpdated
            ? recordType?.previousValue
            : recordType?.newValue || recordType?.previousValue;
        return valRecordType
          .toString()
          .toLowerCase()
          .includes(searchValue?.trim().toLowerCase() || '');
      });
      setDataSearch(dataFilter);
      setIsExpand(true);
    }
  }, [
    dataForm?.agent,
    dataForm?.prin,
    dataForm?.sys,
    dataGrid,
    hasSearch,
    searchValue
  ]);

  useEffect(() => {
    if (hasSearch) {
      setCurrentPage(1);
    }
  }, [hasSearch, searchValue]);

  useEffect(() => {
    if (isClearAndReset) {
      resetClearAndReset();
      setIsExpand(true);
      setExpandedList([]);
      setCurrentPage(1);
      setCurrentPageSize(DEFAULT_FILTER_VALUE.pageSize as number);
      const sysPrinAgentsData = sysPrinAgents.find(
        (item: MagicKeyValue) =>
          item.sysPrinAgent ===
          `${dataForm?.sys}-${dataForm?.prin}-${dataForm?.agent}`
      )?.recordTypes;

      sysPrinAgentsData && setDataGrid(sysPrinAgentsData);
    }
  }, [
    dataForm?.agent,
    dataForm?.prin,
    dataForm?.sys,
    isClearAndReset,
    resetClearAndReset,
    sysPrinAgents
  ]);

  useEffect(() => {
    !!searchValue && setExpandedList([]);
  }, [searchValue]);

  useEffect(() => {
    setColumns([
      {
        id: RecordFieldParameterEnum.RecordType,
        Header: t('txt_spa_record_type'),
        isSort: true,
        width: width < 1280 ? 90 : undefined,
        accessor: (row: MagicKeyValue) => {
          const val =
            row.status === STATUS_SPA.MarkedForDeletion
              ? row?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.RecordType
                )?.newValue ||
                row?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.RecordType
                )?.previousValue
              : row.status !== STATUS_SPA.NotUpdated
              ? row?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.RecordType
                )?.newValue
              : row?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.RecordType
                )?.previousValue;
          const valueTruncate: string = !isEmpty(val?.trim()) ? val : '';
          return (
            <TruncateText
              resizable
              lines={2}
              ellipsisLessText={t('txt_less')}
              ellipsisMoreText={t('txt_more')}
            >
              {valueTruncate}
            </TruncateText>
          );
        }
      },
      {
        id: RecordFieldParameterEnum.GenerationMethod,
        Header: t('txt_spa_generation_method'),
        isSort: true,
        width: width < 1280 ? 165 : undefined,
        accessor: (row: MagicKeyValue) => {
          const val =
            row.status === STATUS_SPA.MarkedForDeletion
              ? row?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.GenerationMethod
                )?.newValue ||
                row?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.GenerationMethod
                )?.previousValue
              : row.status !== STATUS_SPA.NotUpdated
              ? row?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.GenerationMethod
                )?.newValue
              : row?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.GenerationMethod
                )?.previousValue;
          const valueTruncate = originalData?.generationMethods?.find(
            (o: RefData) => o.code === val
          )?.text;
          return (
            <TruncateText
              resizable
              lines={2}
              ellipsisLessText={t('txt_less')}
              ellipsisMoreText={t('txt_more')}
            >
              {valueTruncate}
            </TruncateText>
          );
        }
      },
      {
        id: RecordFieldParameterEnum.RangeFrom,
        isSort: true,
        width: width < 1280 ? 115 : undefined,
        Header: t('txt_spa_range_from'),
        accessor: (row: MagicKeyValue) => {
          const val =
            row.status === STATUS_SPA.MarkedForDeletion
              ? row?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.RangeFrom
                )?.newValue ||
                row?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.RangeFrom
                )?.previousValue
              : row.status !== STATUS_SPA.NotUpdated
              ? row?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.RangeFrom
                )?.newValue
              : row?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.RangeFrom
                )?.previousValue;
          return (
            <TruncateText
              resizable
              lines={2}
              ellipsisLessText={t('txt_less')}
              ellipsisMoreText={t('txt_more')}
            >
              {val}
            </TruncateText>
          );
        }
      },
      {
        id: RecordFieldParameterEnum.RangeTo,
        isSort: true,
        width: width < 1280 ? 115 : undefined,
        Header: t('txt_spa_range_to'),
        accessor: (row: MagicKeyValue) => {
          const val =
            row.status === STATUS_SPA.MarkedForDeletion
              ? row?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.RangeTo
                )?.newValue ||
                row?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.RangeTo
                )?.previousValue
              : row.status !== STATUS_SPA.NotUpdated
              ? row?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.RangeTo
                )?.newValue
              : row?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.RangeTo
                )?.previousValue;
          return (
            <TruncateText
              resizable
              lines={2}
              ellipsisLessText={t('txt_less')}
              ellipsisMoreText={t('txt_more')}
            >
              {val}
            </TruncateText>
          );
        }
      },
      {
        id: RecordFieldParameterEnum.Status,
        isSort: true,
        width: width < 1280 ? 150 : undefined,
        Header: t('txt_spa_status'),
        accessor: (row: MagicKeyValue) => {
          return (
            <Badge textTransformNone color={mapColorBadge(row?.status)}>
              {row?.status}
            </Badge>
          );
        }
      },
      {
        id: 'action',
        Header: t('txt_actions'),
        width: 133,
        cellHeaderProps: {
          className: 'text-center'
        },
        cellBodyProps: {
          className: 'py-8'
        },
        accessor: data => {
          const method =
            data.status === STATUS_SPA.MarkedForDeletion
              ? data?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.GenerationMethod
                )?.newValue ||
                data?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.GenerationMethod
                )?.previousValue
              : data.status !== STATUS_SPA.NotUpdated
              ? data?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.GenerationMethod
                )?.newValue
              : data?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.GenerationMethod
                )?.previousValue;
          if (method === 'D') {
            return <div />;
          }
          return (
            <div className="d-flex justify-content-center">
              {data.status === STATUS_SPA.MarkedForDeletion ? (
                <>
                  <Button
                    variant="outline-danger"
                    size="sm"
                    onClick={() => handleCancelRecord(data)}
                  >
                    {t('txt_cancel')}
                  </Button>
                </>
              ) : (
                <>
                  <Button
                    variant="outline-primary"
                    size="sm"
                    onClick={() => handleEditRecord(data)}
                  >
                    {t('txt_edit')}
                  </Button>

                  {data.status === STATUS_SPA.Updated ? (
                    <Button
                      variant="outline-danger"
                      size="sm"
                      onClick={() => handleCancelRecord(data)}
                    >
                      {t('txt_cancel')}
                    </Button>
                  ) : (
                    <Button
                      variant="outline-danger"
                      size="sm"
                      onClick={() => handleDeleteRecord(data)}
                      className="ml-8"
                    >
                      {t('txt_delete')}
                    </Button>
                  )}
                </>
              )}
            </div>
          );
        }
      }
    ]);
  }, [t, handleCancelRecord, originalData, width]);

  const readOnlyCheckbox = dataGrid
    ?.filter((e: MagicKeyValue) => {
      const generation =
        e.status === STATUS_SPA.MarkedForDeletion
          ? e?.parameters?.find(
              (o: MagicKeyValue) =>
                o.name === RecordFieldParameterEnum.GenerationMethod
            )?.newValue ||
            e?.parameters?.find(
              (o: MagicKeyValue) =>
                o.name === RecordFieldParameterEnum.GenerationMethod
            )?.previousValue
          : e.status !== STATUS_SPA.NotUpdated
          ? e?.parameters?.find(
              (o: MagicKeyValue) =>
                o.name === RecordFieldParameterEnum.GenerationMethod
            )?.newValue
          : e?.parameters?.find(
              (o: MagicKeyValue) =>
                o.name === RecordFieldParameterEnum.GenerationMethod
            )?.previousValue;
      return (
        [STATUS_SPA.MarkedForDeletion, STATUS_SPA.Updated].includes(e.status) ||
        generation === 'D'
      );
    })
    .map((e: MagicKeyValue) => e.id);

  const recordSelected = useMemo(() => {
    const checkListLength = dataChecked?.length;
    const content = isCheckedAll
      ? t('txt_all_record_types_selected')
      : checkListLength !== 1
      ? t('txt_spa_record_types_selected', { checkListLength })
      : t('txt_spa_record_type_selected', { checkListLength });

    return <>{content}</>;
  }, [dataChecked, t, isCheckedAll]);

  const dataFilter = useMemo(() => {
    if (sort.id === RecordFieldParameterEnum.Status) {
      return orderBy(
        dataSearch,
        [
          o => {
            return o?.status;
          }
        ],
        [sort?.order || 'asc']
      ).slice(
        currentPageSize * (currentPage - 1),
        currentPage * currentPageSize
      );
    }

    if (
      sort.id === RecordFieldParameterEnum.RangeFrom ||
      sort.id === RecordFieldParameterEnum.RangeTo
    ) {
      return orderBy(
        dataSearch,
        [
          (o: MagicKeyValue) => {
            const param = o?.parameters as MagicKeyValue[];
            const val =
              o.status === STATUS_SPA.MarkedForDeletion
                ? param?.find((o: MagicKeyValue) => o.name === sort.id)
                    ?.newValue ||
                  param?.find((o: MagicKeyValue) => o.name === sort.id)
                    ?.previousValue
                : o.status !== STATUS_SPA.NotUpdated
                ? param?.find((o: MagicKeyValue) => o.name === sort.id)
                    ?.newValue
                : param?.find((o: MagicKeyValue) => o.name === sort.id)
                    ?.previousValue;
            return parseFloat(val);
          }
        ],
        [sort?.order || 'asc']
      ).slice(
        currentPageSize * (currentPage - 1),
        currentPage * currentPageSize
      );
    }

    return orderBy(
      dataSearch,
      [
        (o: MagicKeyValue) => {
          const param = o?.parameters as MagicKeyValue[];
          const val =
            o.status === STATUS_SPA.MarkedForDeletion
              ? param?.find((o: MagicKeyValue) => o.name === sort.id)
                  ?.newValue ||
                param?.find((o: MagicKeyValue) => o.name === sort.id)
                  ?.previousValue
              : o.status !== STATUS_SPA.NotUpdated
              ? param?.find((o: MagicKeyValue) => o.name === sort.id)?.newValue
              : param?.find((o: MagicKeyValue) => o.name === sort.id)
                  ?.previousValue;
          return val;
        }
      ],
      [sort?.order || 'asc']
    ).slice(currentPageSize * (currentPage - 1), currentPage * currentPageSize);
  }, [currentPage, currentPageSize, dataSearch, sort.id, sort?.order]);

  let isHide = false;

  dataGrid?.forEach((o: MagicKeyValue) => {
    if (o.status !== STATUS_SPA.NotUpdated) {
      isHide = true;
      return;
    }
  });

  const checkedAllPage = useMemo(
    () =>
      dataGrid
        ?.filter((e: MagicKeyValue) => {
          const generation =
            e.status === STATUS_SPA.MarkedForDeletion
              ? e?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.GenerationMethod
                )?.newValue ||
                e?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.GenerationMethod
                )?.previousValue
              : e.status !== STATUS_SPA.NotUpdated
              ? e?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.GenerationMethod
                )?.newValue
              : e?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.GenerationMethod
                )?.previousValue;
          return (
            [STATUS_SPA.NotUpdated, STATUS_SPA.New].includes(e.status) &&
            generation !== 'D'
          );
        })
        .map((e: MagicKeyValue) => e.id),
    [dataGrid]
  );

  const checkedAllInPage = useMemo(
    () =>
      dataFilter
        ?.filter((e: MagicKeyValue) => {
          const generation =
            e.status === STATUS_SPA.MarkedForDeletion
              ? e?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.GenerationMethod
                )?.newValue ||
                e?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.GenerationMethod
                )?.previousValue
              : e.status !== STATUS_SPA.NotUpdated
              ? e?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.GenerationMethod
                )?.newValue
              : e?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.GenerationMethod
                )?.previousValue;
          return (
            [STATUS_SPA.NotUpdated, STATUS_SPA.New].includes(e.status) &&
            generation !== 'D'
          );
        })
        .map((e: MagicKeyValue) => e.id),
    [dataFilter]
  );

  const handleOnChecked = (dataKeyList: string[]) => {
    const dataOtherPage = differenceWith(dataChecked, checkedAllInPage);
    const newDataChecked = _.uniq(dataOtherPage.concat(dataKeyList));
    setCheckedList(dataKeyList);
    setDataChecked(newDataChecked);
    const _isCheckedAll =
      newDataChecked.length === checkedAllPage.length &&
      checkedAllPage.length > 0;
    setIsCheckedAll(_isCheckedAll);
  };

  /* istanbul ignore next */
  const onSetIsCheckAll = useCallback(
    isCheckAll => {
      setIsCheckedAll(isCheckAll);
      setDataChecked(isCheckAll ? checkedAllPage : []);
    },
    [checkedAllPage]
  );
  /* istanbul ignore next */
  const handleCheckAll = useCallback(
    event => {
      onSetIsCheckAll(event.target.checked);
    },
    [onSetIsCheckAll]
  );

  // fake layout
  // remove className indeterminate && set checked for checkbox header in HTML
  useEffect(() => {
    if (selectedStep !== stepId || dataGrid?.length <= 0) {
      return;
    }

    const _grid = document.querySelector(`.${id}`);

    if (!_grid) return;

    const checkboxHeader = _grid.querySelector(
      '.dls-grid-head input[type=checkbox]'
    ) as HTMLInputElement;

    const isIndeterminate =
      dataChecked.length > 0 && dataChecked.length < checkedAllPage.length;

    if (!!checkboxHeader) {
      process.nextTick(() => {
        checkboxHeader.classList.remove('indeterminate');
        isIndeterminate && checkboxHeader.classList.add('indeterminate');

        checkboxHeader.checked =
          (isCheckedAll || isIndeterminate) && checkedAllPage.length > 0;
      });
    }
  }, [
    data?.length,
    dataChecked,
    selectedStep,
    stepId,
    isCheckedAll,
    dataGrid?.length,
    checkedAllPage?.length,
    id,
    currentPage,
    currentPageSize
  ]);

  // addEventListener for checkbox header
  useEffect(() => {
    if (selectedStep !== stepId || data?.length <= 0) {
      return;
    }
    process.nextTick(() => {
      const _grid = document.querySelector(`.${id}`);

      if (!_grid) return;

      const checkboxHeader: any = _grid.querySelector(
        '.dls-grid-head input[type=checkbox]'
      );

      checkboxHeader?.addEventListener('change', handleCheckAll);
    });

    return () => {
      const _grid = document.querySelector(`.${id}`);

      if (!_grid) return;

      const checkboxHeader: any = _grid.querySelector(
        '.dls-grid-head input[type=checkbox]'
      );
      checkboxHeader?.removeEventListener('change', handleCheckAll);
    };
  }, [data?.length, handleCheckAll, id, selectedStep, stepId]);

  useEffect(() => {
    setCheckedList(isCheckedAll ? checkedAllInPage : dataChecked);
  }, [checkedAllInPage, dataChecked, isCheckedAll]);

  if (dataSearch?.length < 1) {
    return <></>;
  }

  return (
    <>
      {order > 0 && <hr className="my-24" />}
      <div className="mt-16">
        <div className="d-flex align-items-center">
          <Tooltip
            element={isExpand ? t('txt_collapse') : t('txt_expand')}
            variant="primary"
            placement="top"
            triggerClassName="d-flex ml-n2"
          >
            <Button
              size="sm"
              variant="icon-secondary"
              onClick={() => setIsExpand(!isExpand)}
            >
              <Icon name={isExpand ? 'minus' : 'plus'} size="4x" />
            </Button>
          </Tooltip>
          <p className="fw-600 ml-8">{`${dataForm?.sys} ${dataForm?.prin} ${dataForm?.agent}`}</p>
          <div className="ml-auto d-flex ">
            {!isHide ? (
              <Button
                variant="outline-primary"
                size="sm"
                onClick={() => handleHideSPA(dataForm)}
              >
                {t('txt_hide_spa_sys_prin_agent_add')}
              </Button>
            ) : (
              <div className="mr-8">
                <Tooltip
                  element={t('txt_spa_hide_record_list')}
                  placement="top"
                >
                  <Button variant="outline-primary" size="sm" disabled={true}>
                    {t('txt_hide_spa_sys_prin_agent_add')}
                  </Button>
                </Tooltip>
              </div>
            )}
            <Button
              variant="outline-primary"
              size="sm"
              onClick={handleAddRecord}
              className="mr-n8"
            >
              {t('txt_add_record_type')}
            </Button>
          </div>
        </div>
        {isExpand && (
          <>
            <div className="d-flex align-items-center my-12">
              <p className="py-4">{recordSelected}</p>
              {checkedList.length > 1 && (
                <Button
                  variant="outline-danger"
                  size="sm"
                  className="ml-auto mr-n8"
                  onClick={handleBulkDelete}
                >
                  {t('txt_spa_bulk_deleted')}
                </Button>
              )}
            </div>
            <Grid
              togglable
              className={classNames(
                'grid-has-tooltip center grid-checkall--cus',
                checkedAllPage?.length === dataChecked?.length && 'check-all',
                id
              )}
              data={dataFilter}
              columns={columns}
              dataItemKey="id"
              toggleButtonConfigList={dataFilter.map(
                mapGridExpandCollapse('id', t)
              )}
              expandedItemKey="id"
              variant={{
                id: 'id',
                type: 'checkbox'
              }}
              expandedList={expandedList}
              onExpand={handleExpand}
              subRow={renderSubRow}
              onCheck={handleOnChecked}
              checkedList={checkedList}
              readOnlyCheckbox={readOnlyCheckbox}
              checkboxTooltipPropsList={readOnlyCheckbox?.map((id: string) => ({
                id,
                element: t('txt_manage_account_serialization_disabled_tooltip')
              }))}
              onSortChange={onSortChange}
              sortBy={[sort]}
            />
            {dataSearch?.length > 10 && (
              <div className="mt-16">
                <Paging
                  page={currentPage}
                  pageSize={currentPageSize}
                  totalItem={dataSearch?.length}
                  onChangePage={onPageChange}
                  onChangePageSize={onPageSizeChange}
                />
              </div>
            )}
          </>
        )}
      </div>
      {openAddRecordModal && (
        <AddNewRecordModal
          id="addNewRecord"
          show={openAddRecordModal}
          isCreate={true}
          title={'txt_add_record_type'}
          SPAs={SPAs}
          dataForm={dataForm}
          originalData={originalData}
          handleUpdateExpand={handleUpdateExpand}
          resetDefaultFilter={resetDefaultFilter}
          onClose={newRecord => {
            if (newRecord) {
              setDataGrid([...dataGrid, newRecord]);
              if (isCheckedAll) {
                setIsCheckedAll(false);
              }
              const cloneSysPrinAgents = sysPrinAgents.slice();
              const selectedSysPrinAgentsIndex = cloneSysPrinAgents.findIndex(
                (item: MagicKeyValue) =>
                  item.sysPrinAgent ===
                  `${dataForm?.sys}-${dataForm?.prin}-${dataForm?.agent}`
              );

              cloneSysPrinAgents[selectedSysPrinAgentsIndex] = {
                ...cloneSysPrinAgents[selectedSysPrinAgentsIndex],
                recordTypes: [...dataGrid, newRecord]
              };

              keepRef.current.setFormValues({
                sysPrinAgents: cloneSysPrinAgents,
                isValid: true
              });
              setOpenAddRecordModal(false);
            }
            setOpenAddRecordModal(false);
          }}
        />
      )}

      {openEditRecordModal && (
        <AddNewRecordModal
          id="addNewRecord"
          show={openEditRecordModal}
          isCreate={false}
          SPAs={SPAs}
          dataForm={dataForm}
          originalData={originalData}
          title={'txt_edit_record_type'}
          infoSpa={selectedRecord}
          handleUpdateExpand={handleUpdateExpand}
          resetDefaultFilter={resetDefaultFilter}
          onClose={editRecord => {
            const newDataGrid = dataGrid.slice();

            const selectedIndex = newDataGrid.findIndex(
              (item: MagicKeyValue) => item.id === selectedRecord.id
            );

            if (!editRecord) return setOpenEditRecordModal(false);

            if (
              editRecord &&
              editRecord.status !== STATUS_SPA.New &&
              isEqualOriginalData(
                convertParameter(originalData?.recordTypes?.[selectedIndex]),
                editRecord
              )
            ) {
              editRecord.status = STATUS_SPA.NotUpdated;
              editRecord.parameters = convertParameter(
                originalData?.recordTypes?.[selectedIndex]
              ) as any;

              newDataGrid[selectedIndex] = editRecord;

              setDataGrid(newDataGrid);

              const cloneSysPrinAgents = sysPrinAgents.slice();
              const selectedSysPrinAgentsIndex = cloneSysPrinAgents.findIndex(
                (item: MagicKeyValue) =>
                  item.sysPrinAgent ===
                  `${dataForm?.sys}-${dataForm?.prin}-${dataForm?.agent}`
              );

              cloneSysPrinAgents[selectedSysPrinAgentsIndex] = {
                ...cloneSysPrinAgents[selectedSysPrinAgentsIndex],
                recordTypes: newDataGrid
              };

              keepRef.current.setFormValues({
                sysPrinAgents: cloneSysPrinAgents,
                isValid: true
              });

              setExpandedList(prev => prev.concat(selectedRecord.id));
              setOpenEditRecordModal(false);
              return;
            }
            newDataGrid[selectedIndex] = editRecord;

            setDataGrid(newDataGrid);

            const cloneSysPrinAgents = sysPrinAgents.slice();
            const selectedSysPrinAgentsIndex = cloneSysPrinAgents.findIndex(
              (item: MagicKeyValue) =>
                item.sysPrinAgent ===
                `${dataForm?.sys}-${dataForm?.prin}-${dataForm?.agent}`
            );

            cloneSysPrinAgents[selectedSysPrinAgentsIndex] = {
              ...cloneSysPrinAgents[selectedSysPrinAgentsIndex],
              recordTypes: newDataGrid
            };

            keepRef.current.setFormValues({
              sysPrinAgents: cloneSysPrinAgents,
              isValid: true
            });
            /* istanbul ignore next */
            const filterCheckedList = checkedList.filter(
              o => o !== selectedRecord.id
            );
            setCheckedList(filterCheckedList);
            /* istanbul ignore next */
            const filterCheckedListData = dataChecked.filter(
              o => o !== selectedRecord.id
            );
            setDataChecked(filterCheckedListData);
            setExpandedList(prev => prev.concat(selectedRecord.id));
            setOpenEditRecordModal(false);
          }}
        />
      )}
      {openDeleteRecordModal && (
        <RemoveRecordModal
          id="addNewRecord"
          show={openDeleteRecordModal}
          onClose={handleCloseDeleteModal}
          infoSpa={selectedRecord}
          checkedList={checkedList}
          data={data}
          isBulk={isBulk}
        />
      )}
    </>
  );
};

export default SPAGrid;
