import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { mockUseModalRegistryFnc } from 'app/utils';
import React from 'react';
import AddSPAModal from './AddSPAModal';

const mockUseModalRegistry = mockUseModalRegistryFnc();

beforeEach(() => {
  mockUseModalRegistry.mockImplementation(() => [true, false]);
});

describe('ExtraStaticManageAccountSerializationStep', () => {
  const mockSpas = {
    sys: ['001', '002', '003'],
    prin: {
      '001': ['011', '012', '013'],
      '002': ['021', '022', '023'],
      '003': ['031', '032', '033']
    },
    agent: {
      '001011': ['111', '112', '113'],
      '001012': ['121', '122', '123'],
      '001013': ['131', '132', '133'],
      '002021': ['211', '212', '213'],
      '002022': ['221', '222', '223'],
      '002023': ['231', '232', '233'],
      '003031': ['311', '312', '313'],
      '003032': ['321', '322', '323'],
      '003033': ['331', '332', '333']
    }
  };

  it('render', () => {
    jest.useFakeTimers();
    const props = {
      onClose: jest.fn(),
      SPAs: { sys: [], prin: {}, agent: {} }
    } as any;
    const wrapper = render(
      <div>
        <AddSPAModal {...props} />
      </div>
    );
    jest.runAllTicks();

    expect(wrapper.getByText('txt_spa_sys_prin_agent_add')).toBeInTheDocument();

    userEvent.click(wrapper.getByText('txt_cancel'));
    expect(props.onClose).toBeCalled();
  });

  it('should add SPA success', () => {
    jest.useFakeTimers();
    const props = {
      handleAdd: jest.fn(),
      SPAs: mockSpas,
      arrSPAs: []
    } as any;
    const wrapper = render(
      <div>
        <AddSPAModal {...props} />
      </div>
    );
    jest.runAllTicks();

    expect(wrapper.getByText('txt_spa_sys_prin_agent_add')).toBeInTheDocument();

    const sysOptions = wrapper.getByText('txt_spa_sys');
    userEvent.click(sysOptions);
    userEvent.click(wrapper.getByText('001'));
    userEvent.click(sysOptions);
    userEvent.click(wrapper.getByText('001'));

    const prinOptions = wrapper.getByText('txt_spa_prin');
    userEvent.click(prinOptions);
    userEvent.click(wrapper.getByText('011'));

    const agentOptions = wrapper.getByText('txt_spa_agent');
    userEvent.click(agentOptions);
    userEvent.click(wrapper.getByText('111'));

    userEvent.click(wrapper.getByText('txt_add'));
    expect(props.handleAdd).toBeCalled();
  });

  it('should add SPA with exist SPA', () => {
    jest.useFakeTimers();
    const props = {
      handleAdd: jest.fn(),
      SPAs: mockSpas,
      arrSPAs: [{ sys: '001', prin: '011', agent: '111' }]
    } as any;
    const wrapper = render(
      <div>
        <AddSPAModal {...props} />
      </div>
    );
    jest.runAllTicks();

    expect(wrapper.getByText('txt_spa_sys_prin_agent_add')).toBeInTheDocument();

    const sysOptions = wrapper.getByText('txt_spa_sys');
    userEvent.click(sysOptions);
    userEvent.click(wrapper.getByText('001'));

    const prinOptions = wrapper.getByText('txt_spa_prin');
    userEvent.click(prinOptions);
    userEvent.click(wrapper.getByText('011'));

    const agentOptions = wrapper.getByText('txt_spa_agent');
    userEvent.click(agentOptions);
    userEvent.click(wrapper.getByText('111'));

    userEvent.click(wrapper.getByText('txt_add'));
    expect(props.handleAdd).not.toBeCalled();
  });
});
