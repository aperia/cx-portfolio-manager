import { render } from '@testing-library/react';
import ExtraStaticManageAccountSerializationStep from '.';
import React from 'react';

jest.mock('./ConfigureParameters', () => ({
  __esModule: true,
  default: () => <div>ConfigureParameters</div>
}));

describe('ExtraStaticManageAccountSerializationStep', () => {
  it('render', () => {
    const props = {} as any;
    const wrapper = render(
      <div>
        <ExtraStaticManageAccountSerializationStep {...props} />
      </div>
    );

    expect(wrapper.getByText('ConfigureParameters')).toBeInTheDocument();
  });
});
