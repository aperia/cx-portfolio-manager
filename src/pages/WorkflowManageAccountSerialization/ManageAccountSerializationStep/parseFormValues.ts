import { getWorkflowSetupStepStatus } from 'app/helpers';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import {
  mapCodeAndTextToText,
  mapValueAndDescToRefItem
} from 'pages/_commons/redux/WorkflowSetup/select-hooks/_helper';

const parseFormValues: WorkflowSetupStepFormDataFunc<any> = async (
  data,
  id,
  dispatch
) => {
  const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);
  if (!stepInfo) return;

  const _sysPrinAgents = data.data.configurations?.sysPrinAgents || [];
  const sysPrinAgents: any[] = [];
  for (let i = 0; i < _sysPrinAgents.length; i++) {
    const item = { ..._sysPrinAgents[i] };
    const sysPrinAgentSplit = item?.sysPrinAgent?.split('-');
    if (sysPrinAgentSplit?.length !== 3) continue;

    const dataForm = {
      sys: sysPrinAgentSplit[0],
      prin: sysPrinAgentSplit[1],
      agent: sysPrinAgentSplit[2]
    };

    const response: MagicKeyValue = await Promise.resolve(
      dispatch(actionsWorkflowSetup.getSPAListWorkflow({ dataForm }))
    );
    const _allData = response?.payload?.data;
    const allData = {
      recordTypes: _allData?.recordTypes,
      defaultPrefix: _allData?.defaultPrefix,
      generationMethods: _allData?.defaultGenerationMethods
        ?.map(mapValueAndDescToRefItem)
        ?.map(mapCodeAndTextToText),
      types: _allData?.defaultTypes
        ?.map(mapValueAndDescToRefItem)
        ?.map(mapCodeAndTextToText),
      useAlternateAgents: _allData?.defaultUseAlternateAgents
        ?.map(mapValueAndDescToRefItem)
        ?.map(mapCodeAndTextToText)
    };

    item.originalData = allData;

    sysPrinAgents.push(item);
  }

  const isValid = !isEmpty(sysPrinAgents);

  return {
    ...getWorkflowSetupStepStatus(stepInfo),
    isValid,
    sysPrinAgents: sysPrinAgents
  };
};

export default parseFormValues;
