export enum SPA_FORM_NAME {
  SYS = 'sys',
  PRIN = 'prin',
  AGENT = 'agent'
}
