import FieldTooltip from 'app/components/FieldTooltip';
import { useFormValidations } from 'app/hooks';
import {
  Button,
  ComboBox,
  DropdownBaseChangeEvent,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty, orderBy } from 'lodash';
import React, { useEffect, useMemo, useState } from 'react';
import { SPA_FORM_NAME } from './constants';

export interface SPAProps {
  data: { sys: string[]; prin: MagicKeyValue; agent: MagicKeyValue };
  handleContinue: (data: { sys: string; prin: string; agent: string }) => void;
}

const SPA: React.FC<SPAProps> = ({ data, handleContinue }) => {
  const { t } = useTranslation();
  const [dataForm, setDataForm] = useState({
    sys: '',
    prin: '',
    agent: ''
  });
  const [resetTouch, setResetTouch] = useState(0);
  useEffect(() => {
    process.nextTick(() => {
      setResetTouch(t => t + 1);
    });
  }, [dataForm.sys, dataForm.prin]);

  const currentErrors = useMemo(
    () =>
      ({
        sys: isEmpty(dataForm.sys) && {
          status: true,
          message: t('txt_required_validation', {
            fieldName: 'Sys'
          })
        },
        prin: isEmpty(dataForm.prin) &&
          !isEmpty(dataForm.sys) && {
            status: true,
            message: t('txt_required_validation', {
              fieldName: 'Prin'
            })
          },
        agent: isEmpty(dataForm.agent) &&
          !isEmpty(dataForm.prin) &&
          !isEmpty(dataForm.sys) && {
            status: true,
            message: t('txt_required_validation', {
              fieldName: 'Agent'
            })
          }
      } as Record<any, IFormError>),
    [dataForm.sys, dataForm.prin, dataForm.agent, t]
  );

  const [, errors, setTouched] = useFormValidations<any>(
    currentErrors,
    resetTouch
  );

  const isValid = useMemo(() => {
    const props = ['sys', 'prin', 'agent'];
    return props.every(p => !errors[p]?.status);
  }, [errors]);

  const handleChange = (
    e: DropdownBaseChangeEvent,
    name: 'sys' | 'prin' | 'agent'
  ) => {
    const prevValue = dataForm[name];
    const nextValue = e.target.value;

    if (prevValue === nextValue) return;

    if (name === SPA_FORM_NAME.AGENT) {
      setDataForm({ ...dataForm, [name]: nextValue });
    }
    if (name === SPA_FORM_NAME.PRIN) {
      setDataForm({
        ...dataForm,
        [name]: nextValue,
        [SPA_FORM_NAME.AGENT]: ''
      });
    }
    if (name === SPA_FORM_NAME.SYS) {
      setDataForm({
        ...dataForm,
        [name]: nextValue,
        [SPA_FORM_NAME.PRIN]: '',
        [SPA_FORM_NAME.AGENT]: ''
      });
    }
  };

  const sysItems = useMemo(() => {
    return data.sys.map(w => <ComboBox.Item key={w} label={w} value={w} />);
  }, [data]);

  const prinItems = useMemo(() => {
    const prinData = data.prin[dataForm[SPA_FORM_NAME.SYS]] as string[];
    if (prinData) {
      return prinData.map(w => <ComboBox.Item key={w} label={w} value={w} />);
    }
    return <ComboBox.Item />;
  }, [data, dataForm]);

  const agentItems = useMemo(() => {
    const agentData = data.agent[
      `${dataForm[SPA_FORM_NAME.SYS]}${dataForm[SPA_FORM_NAME.PRIN]}`
    ] as string[];
    if (agentData) {
      return orderBy(agentData).map(w => (
        <ComboBox.Item key={w} label={w} value={w} />
      ));
    }
    return <ComboBox.Item />;
  }, [data, dataForm]);

  return (
    <div>
      <h5 className="mt-24 mb-16">{t('txt_spa_title')}</h5>
      <div className="row">
        <div className="col-4">
          <FieldTooltip
            placement="right-start"
            element={
              <div>
                <p className="pb-8">{t('txt_tooltip_sys')}</p>
                <p>{t('txt_tooltip_sys_body')}</p>
              </div>
            }
          >
            <ComboBox
              id={`spa__workflow_sys`}
              label={t('txt_spa_sys')}
              required
              textField="text"
              noResult={t('txt_no_results_found')}
              value={dataForm[SPA_FORM_NAME.SYS]}
              onChange={e => handleChange(e, SPA_FORM_NAME.SYS)}
              onFocus={setTouched(SPA_FORM_NAME.SYS)}
              onBlur={setTouched(SPA_FORM_NAME.SYS, true)}
              error={errors[SPA_FORM_NAME.SYS]}
            >
              {sysItems}
            </ComboBox>
          </FieldTooltip>
        </div>
        <div className="col-4">
          <FieldTooltip
            placement="right-start"
            element={
              <div>
                <p className="pb-8">{t('txt_tooltip_prin')}</p>
                <p>{t('txt_tooltip_prin_body')}</p>
              </div>
            }
          >
            <ComboBox
              id={`spa__workflow_prin`}
              label={t('txt_spa_prin')}
              required
              textField="text"
              noResult={t('txt_no_results_found')}
              value={dataForm[SPA_FORM_NAME.PRIN]}
              onChange={e => handleChange(e, SPA_FORM_NAME.PRIN)}
              disabled={isEmpty(dataForm[SPA_FORM_NAME.SYS])}
              onFocus={setTouched(SPA_FORM_NAME.PRIN)}
              onBlur={setTouched(SPA_FORM_NAME.PRIN, true)}
              error={errors[SPA_FORM_NAME.PRIN]}
            >
              {prinItems}
            </ComboBox>
          </FieldTooltip>
        </div>
        <div className="col-4">
          <FieldTooltip
            placement="right-start"
            element={
              <div>
                <p className="pb-8">{t('txt_tooltip_agent')}</p>
                <p>{t('txt_tooltip_agent_body')}</p>
              </div>
            }
          >
            <ComboBox
              id={`spa__workflow_agent`}
              label={t('txt_spa_agent')}
              required
              textField="text"
              noResult={t('txt_no_results_found')}
              value={dataForm[SPA_FORM_NAME.AGENT]}
              onChange={e => handleChange(e, SPA_FORM_NAME.AGENT)}
              disabled={isEmpty(dataForm[SPA_FORM_NAME.PRIN])}
              onFocus={setTouched(SPA_FORM_NAME.AGENT)}
              onBlur={setTouched(SPA_FORM_NAME.AGENT, true)}
              error={errors[SPA_FORM_NAME.AGENT]}
            >
              {agentItems}
            </ComboBox>
          </FieldTooltip>
        </div>
      </div>
      <div className="text-right mt-24">
        <Button
          size="sm"
          onClick={() => handleContinue(dataForm)}
          variant="primary"
          disabled={isEmpty(dataForm[SPA_FORM_NAME.AGENT]) || !isValid}
        >
          {t('txt_continue')}
        </Button>
      </div>
    </div>
  );
};

export default SPA;
