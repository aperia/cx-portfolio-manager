import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import { RecordFieldParameterEnum } from 'app/constants/enums';
import { classnames } from 'app/helpers';
import { Button, useTranslation } from 'app/_libraries/_dls';
import { isEmpty, orderBy } from 'app/_libraries/_dls/lodash';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { ManageAccountSerializationFormValue } from '.';
import AddSPAModal from './AddSPAModal';
import { STATUS_SPA } from './helper';
import SPAGrid from './SPAGrid';

export interface SPAListProps
  extends WorkflowSetupProps<ManageAccountSerializationFormValue> {
  SPAs: { sys: string[]; prin: MagicKeyValue; agent: MagicKeyValue };
}

const SPAList: React.FC<SPAListProps> = ({ SPAs, ...props }) => {
  const { t } = useTranslation();
  const simpleSearchRef = useRef<any>(null);
  const { setFormValues, selectedStep, stepId } = props;
  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;

  const [addSPAModal, setAddSPAModal] = useState<boolean>(false);
  const [searchValue, setSearchValue] = useState<string>('');
  const [isResult, setIsResult] = useState<boolean>(true);
  const [isSearch, setIsSearch] = useState<boolean>(false);
  const [isClearAndReset, setIsClearAndRest] = useState<boolean>(false);
  const [isResetSort, setIsResetSort] = useState<boolean>(false);

  const { sysPrinAgents } = props.formValues;

  const handleFilter = (_search: string, _sysPrinAgents: any[]) => {
    if (isEmpty(_search)) return;
    setSearchValue(_search);
    setIsClearAndRest(false);

    let isSpa = false;
    let isRecordType = false;

    _sysPrinAgents.forEach((o: MagicKeyValue) => {
      const spa = o?.sysPrinAgent.split('-').join(' ');
      if (spa.includes(_search?.trim().toLowerCase())) {
        isSpa = true;
        return;
      }
      o?.recordTypes?.forEach((_record: MagicKeyValue) => {
        const param = _record?.parameters as MagicKeyValue[];
        const recordType = param.find(
          o => o.name === RecordFieldParameterEnum.RecordType
        );

        const valRecordType =
          _record?.status === STATUS_SPA.NotUpdated
            ? recordType?.previousValue
            : recordType?.newValue || recordType?.previousValue;
        if (
          valRecordType
            .toString()
            .toLowerCase()
            .includes(_search?.trim().toLowerCase())
        ) {
          isRecordType = true;
        }
      });
    });
    if (!isSearch) {
      setIsSearch(true);
    }
    if (isSpa || isRecordType) {
      setIsResult(true);
    } else {
      setIsResult(false);
    }
  };

  const handleSearch = (val: string) => {
    handleFilter(val, sysPrinAgents);
  };

  const resetSort = () => setIsResetSort(false);

  const handleAddSPA = () => setAddSPAModal(true);

  const resetClearAndReset = () => setIsClearAndRest(false);

  const handleAdd = (data: { sys: string; prin: string; agent: string }) => {
    setAddSPAModal(false);
    setFormValues({
      sysPrinAgents: [
        ...sysPrinAgents,
        {
          sysPrinAgent: `${data.sys}-${data.prin}-${data.agent}`,
          recordTypes: []
        }
      ]
    });
    handleClearAndReset();
  };

  const handleClose = () => setAddSPAModal(false);

  const resetDefaultFilter = useCallback(() => {
    setIsResetSort(true);
    setSearchValue('');
    setIsResult(true);
    setIsSearch(false);
  }, []);

  const handleClearAndReset = useCallback(() => {
    setSearchValue('');
    setIsResult(true);
    setIsClearAndRest(true);
    setIsSearch(false);
  }, []);

  useEffect(() => {
    if (selectedStep === stepId) {
      handleClearAndReset();
    }
  }, [selectedStep, stepId, handleClearAndReset]);

  const handleHideSPA = (spa: { sys: string; prin: string; agent: string }) => {
    const newSysPrinAgents = sysPrinAgents?.filter(
      (o: MagicKeyValue) =>
        o.sysPrinAgent !== `${spa.sys}-${spa.prin}-${spa.agent}`
    );
    keepRef.current.setFormValues({
      sysPrinAgents: newSysPrinAgents,
      isValid: !isEmpty(newSysPrinAgents)
    });
    handleFilter(searchValue, newSysPrinAgents);
  };

  useEffect(() => {
    if (selectedStep === stepId) {
      setIsResetSort(true);
    }
  }, [selectedStep, stepId]);

  const arrSPA = useMemo(
    () =>
      sysPrinAgents.map(({ sysPrinAgent }) => {
        const sysPrinAgentSplit = sysPrinAgent?.split('-');
        const dataForm = {
          sys: sysPrinAgentSplit[0],
          prin: sysPrinAgentSplit[1],
          agent: sysPrinAgentSplit[2]
        };
        return dataForm;
      }),
    [sysPrinAgents]
  );
  const arrOrderSPA = useMemo(
    () => orderBy(arrSPA, ['sys', 'prin', 'agent'], ['asc']),
    [arrSPA]
  );

  return (
    <div className="position-relative pt-24">
      <div className="absolute-top-right mt-n28">
        <Button
          className="mr-n8"
          variant="outline-primary"
          size="sm"
          onClick={handleAddSPA}
        >
          {t('txt_spa_sys_prin_agent_add')}
        </Button>
      </div>
      <div className="d-flex align-items-center">
        <h5>{t('txt_spa_sys_prin_agent_list')}</h5>

        <div className="ml-auto">
          <SimpleSearch
            ref={simpleSearchRef}
            defaultValue={searchValue}
            onSearch={handleSearch}
            placeholder={t('txt_type_to_search_for_spa')}
          />
        </div>
      </div>
      {isResult && isSearch && (
        <div className="text-right mt-16">
          <Button
            className="mr-n8"
            variant="outline-primary"
            size="sm"
            onClick={handleClearAndReset}
          >
            {t('txt_clear_and_reset')}
          </Button>
        </div>
      )}
      {!isResult && (
        <div className="d-flex flex-column justify-content-center mt-40 mb-32">
          <NoDataFound
            id="no_record_type_found"
            title={t('txt_pricing_strategies_and_assignments_no_results_found')}
            hasSearch={!!searchValue}
            linkTitle={searchValue && t('txt_clear_and_reset')}
            onLinkClicked={handleClearAndReset}
          />
        </div>
      )}
      {arrOrderSPA.map((data, index) => {
        return (
          <div
            key={`${data?.sys}${data?.prin}${data?.agent}`}
            className={classnames(!isResult && 'd-none')}
          >
            <SPAGrid
              {...props}
              dataForm={data}
              SPAs={SPAs}
              searchValue={searchValue}
              handleHideSPA={handleHideSPA}
              isClearAndReset={isClearAndReset}
              hasSearch={isSearch}
              isResetSort={isResetSort}
              resetDefaultFilter={resetDefaultFilter}
              resetSort={resetSort}
              resetClearAndReset={resetClearAndReset}
              order={index}
            />
          </div>
        );
      })}
      {addSPAModal && (
        <AddSPAModal
          id="addSPA"
          show={addSPAModal}
          onClose={handleClose}
          handleAdd={handleAdd}
          SPAs={SPAs}
          arrSPAs={arrSPA}
        />
      )}
    </div>
  );
};

export default SPAList;
