import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { mockUseModalRegistryFnc } from 'app/utils';
import React from 'react';
import { STATUS_SPA } from './helper';
import RemoveRecordModal from './RemoveRecordModal';

const mockUseModalRegistry = mockUseModalRegistryFnc();

describe('ManageAccountSerializationStep > RemoveRecordModal', () => {
  beforeEach(() => {
    jest.useFakeTimers();
    mockUseModalRegistry.mockImplementation(() => [true, false]);
  });

  afterEach(() => {
    jest.runAllTicks();
  });

  const props = {
    onClose: jest.fn(),
    id: '12',
    show: true
  } as any;

  const data = [
    {
      accountIDOrPIID: 'PI',
      alternateAgent: '0103',
      alternatePrefix: 58248,
      alternatePrincipal: '0032',
      availableRecordNumber: 567734642,
      generationMethod: 'S',
      generationMethods: [
        { value: 'S', description: 'Sequential' },
        { value: 'R', description: 'Random' }
      ],
      id: '1',
      nextFrom: 13667,
      nextTo: 84446,
      prefix: 8973,
      rangeFrom: 32229,
      rangeTo: 84213,
      recordType: 'A',
      serialNumber: 54367,
      useAlternateAgent: 'N',
      warningLevel: 81
    },
    {
      accountIDOrPIID: 'PI',
      alternateAgent: '0103',
      alternatePrefix: 58248,
      alternatePrincipal: '0032',
      availableRecordNumber: 567734642,
      generationMethod: 'S',
      generationMethods: [
        { value: 'S', description: 'Sequential' },
        { value: 'R', description: 'Random' }
      ],
      id: '2',
      nextFrom: 13667,
      nextTo: 84446,
      prefix: 8973,
      rangeFrom: 32229,
      rangeTo: 84213,
      recordType: 'A',
      serialNumber: 54367,
      useAlternateAgent: 'N',
      warningLevel: 81
    }
  ];

  const infoSpa = {
    status: STATUS_SPA.NotUpdated,
    id: '1',
    parameters: [
      { name: 'record.type', previousValue: 'T', newValue: '' },
      { name: 'generation.method', previousValue: 'G', newValue: '' }
    ]
  };

  it('render and close modal', () => {
    const wrapper = render(
      <div>
        <RemoveRecordModal {...props} infoSpa={infoSpa} data={data} />
      </div>
    );

    userEvent.click(wrapper.getByText('txt_cancel'));

    expect(wrapper.getByText('txt_confirm_deletion')).toBeInTheDocument();
  });

  it('render and isHavingGenerateMethod ', () => {
    const wrapper = render(
      <div>
        <RemoveRecordModal
          {...props}
          infoSpa={infoSpa}
          data={data}
          checkedList={['1']}
          isBulk={true}
        />
      </div>
    );

    userEvent.click(wrapper.getByText('txt_cancel'));

    expect(wrapper.getByText('txt_confirm_deletion')).toBeInTheDocument();
  });

  it('render and isHavingGenerateMethod and updated status', () => {
    const wrapper = render(
      <div>
        <RemoveRecordModal
          {...props}
          infoSpa={{ ...infoSpa, status: STATUS_SPA.Updated }}
          data={data}
          checkedList={['1']}
          isBulk={true}
        />
      </div>
    );

    userEvent.click(wrapper.getByText('txt_close'));

    expect(wrapper.getByText('txt_confirm_cancel')).toBeInTheDocument();
  });

  it('render and isHavingGenerateMethod and new status', () => {
    const wrapper = render(
      <div>
        <RemoveRecordModal
          {...props}
          infoSpa={{
            ...infoSpa,
            status: STATUS_SPA.New,
            parameters: [
              { name: 'record.type', previousValue: 'T', newValue: '' },
              { name: 'generation.method', previousValue: '', newValue: 'G' }
            ]
          }}
          data={data}
          checkedList={['1']}
          isBulk={true}
        />
      </div>
    );

    userEvent.click(wrapper.getByText('txt_cancel'));

    expect(wrapper.getByText('txt_confirm_deletion')).toBeInTheDocument();
  });

  it('render and !isHavingGenerateMethod and isBulk and checkedList ', () => {
    const wrapper = render(
      <div>
        <RemoveRecordModal
          {...props}
          infoSpa={infoSpa}
          data={data}
          checkedList={['1', '2']}
          isBulk={true}
        />
      </div>
    );

    userEvent.click(wrapper.getByText('txt_cancel'));

    expect(wrapper.getByText('txt_confirm_deletion')).toBeInTheDocument();
  });

  it('render and isHavingGenerateMethod and isBulk and checkedList ', () => {
    const wrapper = render(
      <div>
        <RemoveRecordModal
          {...props}
          infoSpa={infoSpa}
          data={[
            {
              accountIDOrPIID: 'PI',
              alternateAgent: '0103',
              alternatePrefix: 58248,
              alternatePrincipal: '0032',
              availableRecordNumber: 567734642,
              generationMethod: 'G',
              generationMethods: [
                { value: 'S', description: 'Sequential' },
                { value: 'R', description: 'Random' }
              ],
              id: '1',
              nextFrom: 13667,
              nextTo: 84446,
              prefix: 8973,
              rangeFrom: 32229,
              rangeTo: 84213,
              recordType: 'A',
              serialNumber: 54367,
              useAlternateAgent: 'N',
              warningLevel: 81
            },
            {
              accountIDOrPIID: 'PI',
              alternateAgent: '0103',
              alternatePrefix: 58248,
              alternatePrincipal: '0032',
              availableRecordNumber: 567734642,
              generationMethod: 'S',
              generationMethods: [
                { value: 'S', description: 'Sequential' },
                { value: 'R', description: 'Random' }
              ],
              id: '2',
              nextFrom: 13667,
              nextTo: 84446,
              prefix: 8973,
              rangeFrom: 32229,
              rangeTo: 84213,
              recordType: 'A',
              serialNumber: 54367,
              useAlternateAgent: 'N',
              warningLevel: 81
            }
          ]}
          checkedList={['1', '2']}
          isBulk={true}
        />
      </div>
    );

    userEvent.click(wrapper.getByText('txt_delete'));

    expect(wrapper.getByText('txt_confirm_deletion')).toBeInTheDocument();
  });

  it('render and isMarkDelete and isBulk', () => {
    const wrapper = render(
      <div>
        <RemoveRecordModal
          {...props}
          infoSpa={{ ...infoSpa, status: STATUS_SPA.MarkedForDeletion }}
          data={data}
          checkedList={['1', '2']}
          isBulk={true}
        />
      </div>
    );

    userEvent.click(wrapper.getByText('txt_cancel'));

    expect(wrapper.getByText('txt_confirm_deletion')).toBeInTheDocument();
  });

  it('render and isMarkDelete and isBulk', () => {
    const wrapper = render(
      <div>
        <RemoveRecordModal
          {...props}
          infoSpa={{ ...infoSpa, status: STATUS_SPA.MarkedForDeletion }}
          data={data}
          checkedList={['1', '2']}
          isBulk={false}
        />
      </div>
    );

    userEvent.click(wrapper.getByText('txt_cancel'));

    expect(wrapper.getByText('txt_confirm_cancel')).toBeInTheDocument();
  });
});
