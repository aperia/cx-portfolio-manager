import NoDataFound from 'app/components/NoDataFound';
import { Button, useTranslation } from 'app/_libraries/_dls';
import { isFunction, orderBy } from 'app/_libraries/_dls/lodash';
import React from 'react';
import { ManageAccountSerializationFormValue } from '.';
import { STATUS_SPA } from './helper';
import SummaryGrid from './SummaryGrid';

const Summary: React.FC<
  WorkflowSetupSummaryProps<ManageAccountSerializationFormValue>
> = props => {
  const { t } = useTranslation();

  const { formValues, onEditStep } = props;

  const { sysPrinAgents } = formValues || {};

  const handleEdit = () => {
    isFunction(onEditStep) && onEditStep();
  };

  const arrGrid = sysPrinAgents
    ?.map((sys: MagicKeyValue) => {
      const recordTypes = sys?.recordTypes.filter(
        (record: MagicKeyValue) => record?.status !== STATUS_SPA.NotUpdated
      );
      return {
        ...sys,
        recordTypes
      };
    })
    ?.filter((sys: MagicKeyValue) => sys?.recordTypes?.length > 0);

  const isEmptyArr = arrGrid?.length > 0;

  const arrOrderSPA = orderBy(arrGrid, ['sysPrinAgent'], ['asc']);

  if (!isEmptyArr)
    return (
      <div className="position-relative">
        <div className="absolute-top-right mt-n26 mr-n8">
          <Button variant="outline-primary" size="sm" onClick={handleEdit}>
            {t('txt_edit')}
          </Button>
        </div>
        <div className="pt-16">
          <div className="d-flex flex-column justify-content-center mt-40 mb-32">
            <NoDataFound
              id="no_record_type_found"
              title={t('txt_not_found_sys')}
              hasSearch={false}
            />
          </div>
        </div>
      </div>
    );

  return (
    <div className="position-relative pt-16">
      <div className="absolute-top-right mt-n26 mr-n8">
        <Button variant="outline-primary" size="sm" onClick={handleEdit}>
          {t('txt_edit')}
        </Button>
      </div>
      <div className="list-summary-grid">
        {arrOrderSPA.map((sys: MagicKeyValue) => (
          <div key={sys?.sysPrinAgent} className="summary-grid">
            <SummaryGrid
              dataForm={sys?.sysPrinAgent}
              dataGrid={sys?.recordTypes}
              originalData={sys?.originalData}
              {...props}
            />
          </div>
        ))}
      </div>
    </div>
  );
};

export default Summary;
