import { fireEvent, render, screen } from '@testing-library/react';
import {
  ComboBoxProps,
  DropdownBaseChangeEvent,
  DropdownBaseGroupProps,
  DropdownBaseItemProps
} from 'app/_libraries/_dls';
import React from 'react';
import SPA, { SPAProps } from './SPAForm';

// mock area
jest.mock('app/_libraries/_dls/components/ComboBox', () => {
  const actualModule = jest.requireActual(
    'app/_libraries/_dls/components/ComboBox'
  );
  const MockComboBox = (props: ComboBoxProps) => {
    const { id, onChange, label, error, children } = props;

    return (
      <div className="dls-input-container">
        <input
          data-testid={`ComboBox.Input_${id}`}
          onBlur={e => {
            const value = e.target.value;
            onChange!({
              target: { value: value }
            } as DropdownBaseChangeEvent);
          }}
        />
        {!!error && <p data-testid="ComboBox.Error">{error?.message}</p>}
        <span>{label}</span>
        <div>{children}</div>
      </div>
    );
  };

  MockComboBox.Item = ({ value, label }: DropdownBaseItemProps) => (
    <div key={`${value}-${label}`} data-testid={`ComboBox.Item-${label}`}>
      {label}
    </div>
  );

  MockComboBox.Group = ({ children, label }: DropdownBaseGroupProps) => (
    <div key={`${label}`} data-testid={`ComboBox.Group-${label}`}>
      {children}
    </div>
  );
  return {
    ...actualModule,
    __esModule: true,
    default: MockComboBox
  };
});

const handleContinueMock = jest.fn();
const renderComponent = (props: SPAProps) => render(<SPA {...props} />);
const wait = (timeout = 0) =>
  new Promise(resolve => {
    const id = setTimeout(() => {
      clearTimeout(id);
      resolve(true);
    }, timeout);
  });

describe('WorkflowManageAccountSerialization > ManageAccountSerializationStep > SPAForm', () => {
  it('Should render component', async () => {
    renderComponent({
      data: {
        sys: ['0000', '0010', '0020'],
        prin: {
          '0000': ['0200', '0220', '0230'],
          '0010': ['0300', '0330', '0304'],
          '0020': ['0400', '1400', '2400']
        },
        agent: {
          '00000200': ['3456', '0001', '0002'],
          '00000220': ['3451', '0002', '0003'],
          '00000230': ['3221', '0008', '0004'],
          '00100300': ['0003', '0004', '0005'],
          '00100330': ['0003', '0004', '0005'],
          '00100304': ['0003', '0004', '0005'],
          '00200400': ['0006', '0007', '0008'],
          '00201400': ['0006', '0007', '0008'],
          '00202400': ['0006', '0007', '0008']
        }
      },
      handleContinue: handleContinueMock
    });

    await wait();

    // Select sys
    const sysInput = screen.getByTestId('ComboBox.Input_spa__workflow_sys');
    fireEvent.change(sysInput, {
      target: {
        value: '0000'
      }
    });
    fireEvent.blur(sysInput);
    expect(screen.getByText('0000')).toBeInTheDocument();

    // Select prin
    const prinInput = screen.getByTestId('ComboBox.Input_spa__workflow_prin');
    fireEvent.change(prinInput, {
      target: {
        value: '0200'
      }
    });
    fireEvent.blur(prinInput);
    expect(screen.getByText('0200')).toBeInTheDocument();

    // Select agent
    const agentInput = screen.getByTestId(
      'ComboBox.Input_spa__workflow_agent'
    ) as HTMLInputElement;
    fireEvent.change(agentInput, {
      target: {
        value: '3456'
      }
    });
    fireEvent.blur(agentInput);

    // Change the same value
    fireEvent.change(agentInput, {
      target: {
        value: '3456'
      }
    });
    fireEvent.blur(agentInput);
    expect(screen.getByText('3456')).toBeInTheDocument();

    // Handle continue
    fireEvent.click(screen.getByText('txt_continue'));
    expect(handleContinueMock).toBeCalled();
  });
});
