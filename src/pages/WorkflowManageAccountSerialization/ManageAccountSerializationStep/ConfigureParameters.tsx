import { RecordFieldParameterEnum } from 'app/constants/enums';
import {
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { useUnsavedChangeRegistry } from 'app/hooks';
import { isEqual } from 'lodash';
import {
  actionsWorkflowSetup,
  useSelectElementMetadataForSPA
} from 'pages/_commons/redux/WorkflowSetup';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { ManageAccountSerializationFormValue } from '.';
import SelectedSPA from './SelectedSPA';

const ConfigureParameters: React.FC<
  WorkflowSetupProps<ManageAccountSerializationFormValue>
> = props => {
  const { sys, prin, agent } = useSelectElementMetadataForSPA();
  const { formValues, savedAt } = props;

  const [preValues, setPreValues] = useState(formValues);

  const dispatch = useDispatch();

  useEffect(() => {
    setPreValues(formValues);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [savedAt]);

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_ACCOUNT_SERIALIZATION_PARAMETERS_CONFIRM_EDIT,
      priority: 1
    },
    [!isEqual(preValues.sysPrinAgents, formValues.sysPrinAgents)]
  );

  useEffect(() => {
    dispatch(
      actionsWorkflowSetup.getWorkflowMetadata([
        RecordFieldParameterEnum.SPAs,
        RecordFieldParameterEnum.AccountIDOrPIID
      ])
    );
  }, [dispatch]);

  return <SelectedSPA {...props} data={{ sys, prin, agent }} />;
};

export default ConfigureParameters;
