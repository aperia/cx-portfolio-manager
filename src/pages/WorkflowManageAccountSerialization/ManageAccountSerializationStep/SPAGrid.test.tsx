import { act, fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { RecordFieldParameterEnum } from 'app/constants/enums';
import { WorkflowMetadataList } from 'app/fixtures/workflow-metadata';
import { mockThunkAction, renderWithMockStore } from 'app/utils';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import React from 'react';
import * as ReactRedux from 'react-redux';
import { STATUS_SPA } from './helper';
import SPAGrid from './SPAGrid';

const mockRecord = jest.fn();

const mockOnCloseWithoutRecord = jest.fn();
const mockOnCloseWithRecordEmptyArray = jest.fn();
jest.mock('./AddNewRecordModal', () => ({
  __esModule: true,
  default: ({ onClose, handleUpdateExpand, resetDefaultFilter }: any) => {
    const handleOnClose = () => {
      if (mockOnCloseWithoutRecord()) {
        onClose();
        return;
      }

      if (mockOnCloseWithRecordEmptyArray()) {
        onClose(
          {
            status: 'Not Updated',
            id: 601254652,
            actionCode: '',
            parameters: []
          },
          true
        );
        return;
      }

      onClose(
        {
          status: 'Not Updated',
          id: 601254652,
          actionCode: '',
          parameters: [
            { name: 'record.type', previousValue: 'A', newValue: '' }
          ]
        },
        true
      );
    };
    return (
      <div>
        <div onClick={handleOnClose}>AddNewRecordModal_component</div>
        <div onClick={handleUpdateExpand}>
          AddNewRecordModal_handleUpdateExpand
        </div>
        <div onClick={resetDefaultFilter}>
          AddNewRecordModal_resetDefaultFilter
        </div>
      </div>
    );
  }
}));
const mockOnCloseWithoutMethod = jest.fn();
jest.mock('./RemoveRecordModal', () => ({
  __esModule: true,
  default: ({ onClose }: any) => {
    const handleOnClose = () => {
      if (mockOnCloseWithoutMethod()) {
        onClose(true);
        return;
      }

      onClose(false);
    };
    return <div onClick={handleOnClose}>RemoveRecordModal_component</div>;
  }
}));

jest.mock('pages/_commons/Utils/Paging', () => ({
  __esModule: true,
  default: ({ onChangePage, onChangePageSize }: any) => {
    const handleOnChangePage = () => {
      onChangePage();
    };

    const handleOnChangePageSize = () => {
      onChangePageSize();
    };
    return (
      <div>
        <div onClick={handleOnChangePage}>Paging_component_onChangePage</div>
        <div onClick={handleOnChangePageSize}>
          Paging_component_handleOnChangePageSize
        </div>
      </div>
    );
  }
}));

const payload = {
  payload: {
    data: {
      generationMethods: [
        { value: '', description: 'None', selected: false },
        { value: 'R', description: 'Random', selected: false },
        { value: 'S', description: 'Sequential', selected: true }
      ],
      defaultPrefix: 2810,
      types: [
        {
          value: 'A',
          description:
            'Account identifiers for new separate-entity accoun…cord type A to generate presentation instruments.'
        }
      ],
      useAlternateAgents: [
        {
          value: 'N',
          description: 'No, do not use an alternate agent.',
          selected: true
        },
        {
          value: 'Y',
          description: 'Yes, use an alternate agent.',
          selected: false
        }
      ],
      recordTypes: [
        {
          accountIDOrPIID: 'PI',
          alternateAgent: '0011',
          alternatePrefix: 79015,
          alternatePrincipal: '0203',
          availableRecordNumber: 607038562,
          generationMethod: 'R',
          id: 200579830,
          generationMethods: [{ value: 'R', description: 'Random' }],
          nextFrom: 23665,
          nextTo: 80068,
          prefix: 2473,
          rangeFrom: 26312,
          rangeTo: 68188,
          recordType: 'B',
          serialNumber: 36105,
          useAlternateAgent: 'N',
          warningLevel: 82
        }
      ]
    }
  }
};

const recordTypes = [
  {
    status: STATUS_SPA.NotUpdated,
    id: 601254652,
    actionCode: '',
    parameters: [{ name: 'record.type', previousValue: 'A', newValue: '' }]
  },
  {
    status: STATUS_SPA.New,
    id: 601254652,
    actionCode: '',
    parameters: [{ name: 'record.type', previousValue: 'A', newValue: '' }]
  },
  {
    status: STATUS_SPA.NotUpdated,
    id: 601254652,
    actionCode: '',
    parameters: [
      { name: 'record.type', previousValue: 'A', newValue: '' },
      {
        name: RecordFieldParameterEnum.GenerationMethod,
        previousValue: 'G',
        newValue: ''
      }
    ]
  },
  {
    status: STATUS_SPA.NotUpdated,
    id: 601254652,
    actionCode: '',
    parameters: [
      { name: 'record.type', previousValue: 'A', newValue: '' },
      {
        name: RecordFieldParameterEnum.GenerationMethod,
        previousValue: 'D',
        newValue: ''
      }
    ]
  },
  {
    status: STATUS_SPA.Updated,
    id: 601254652,
    actionCode: '',
    parameters: [
      { name: 'record.type', previousValue: 'A', newValue: '' },
      {
        name: RecordFieldParameterEnum.GenerationMethod,
        previousValue: 'D',
        newValue: ''
      }
    ]
  },
  {
    status: STATUS_SPA.MarkedForDeletion,
    id: 601254652,
    actionCode: '',
    parameters: [
      { name: 'record.type', previousValue: 'A', newValue: '' },
      {
        name: RecordFieldParameterEnum.GenerationMethod,
        previousValue: 'G',
        newValue: ''
      }
    ]
  }
];

const mockActionsWorkflowSetup = mockThunkAction(actionsWorkflowSetup);

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

const getInitialState = ({ elements = WorkflowMetadataList }: any = {}) => ({
  workflowSetup: { elementMetadata: { elements } }
});

const mockHandleHideSPA = jest.fn();
const setFormValues = jest.fn();
const mockUseDispatch = jest.spyOn(ReactRedux, 'useDispatch');

describe('SPAGrid', () => {
  beforeEach(() => {
    mockOnCloseWithoutRecord.mockReturnValue(false);
    mockOnCloseWithoutMethod.mockReturnValue(false);
    mockOnCloseWithRecordEmptyArray.mockReturnValue(false);
    mockUseDispatch.mockReturnValue(jest.fn());
    jest.useFakeTimers();
  });

  afterEach(() => {
    jest.clearAllMocks();
    jest.runAllTimers();
  });

  it('render with empty data', async () => {
    const props = {
      isResetSort: true,
      resetClearAndReset: jest.fn(),
      resetSort: jest.fn(),
      dataForm: { sys: '0001', prin: '0002', agent: '0003' },
      formValues: {
        sysPrinAgents: [
          {
            originalData: payload.payload.data,
            sysPrinAgent: '0001-0002-0003'
          }
        ]
      },
      SPAs: [],
      searchValue: '',
      handleHideSPA: mockHandleHideSPA,
      isClearAndReset: false,
      hasSearch: false,
      setFormValues
    } as any;

    await renderWithMockStore(<SPAGrid {...props} />, {
      initialState: getInitialState()
    });

    expect(props.resetSort).toBeCalled();
  });

  it('render with empty data and fetch data', async () => {
    mockActionsWorkflowSetup('getSPAListWorkflow', {
      match: true,
      payload
    });

    const props = {
      dataForm: { sys: '0001', prin: '0002', agent: '0003' },
      formValues: {
        sysPrinAgents: []
      },
      SPAs: [],
      searchValue: '999',
      handleHideSPA: mockHandleHideSPA,
      isClearAndReset: false,
      hasSearch: false,
      resetClearAndReset: jest.fn(),
      setFormValues
    } as any;

    const wrapper = await renderWithMockStore(<SPAGrid {...props} />, {
      initialState: getInitialState()
    });

    wrapper.container
      .querySelectorAll('.btn .icon.icon-minus')
      .forEach(element => userEvent.click(element));

    wrapper.rerender(
      <SPAGrid {...props} selectedStep="step1" stepId="step1" />
    );
    expect(wrapper.container.querySelector('table')).not.toBeInTheDocument();
  });

  it('render with empty data and fetch data with props is all true', async () => {
    mockActionsWorkflowSetup('getSPAListWorkflow', {
      match: true,
      payload
    });

    const props = {
      dataForm: { sys: '0001', prin: '0002', agent: '0003' },
      formValues: {
        sysPrinAgents: [
          {
            originalData: payload.payload.data,
            sysPrinAgent: '0001-0002-0003',
            recordTypes: [
              {
                status: 'Not Updated',
                id: 601254652,
                actionCode: '',
                parameters: [
                  { name: 'record.type', previousValue: 'A', newValue: '' }
                ]
              }
            ]
          }
        ]
      },
      SPAs: [],
      searchValue: '0001 0002 0003',
      handleHideSPA: mockHandleHideSPA,
      isClearAndReset: true,
      hasSearch: true,
      resetClearAndReset: jest.fn(),
      setFormValues
    } as any;

    const wrapper = await renderWithMockStore(<SPAGrid {...props} />, {
      initialState: getInitialState()
    });

    expect(
      wrapper.getByText('txt_hide_spa_sys_prin_agent_add')
    ).toBeInTheDocument();
  });

  it('render with empty data and fetch data with props is all true and with sysPrinAgentData', async () => {
    mockActionsWorkflowSetup('getSPAListWorkflow', {
      match: true,
      payload
    });

    const props = {
      dataForm: { sys: '0001', prin: '0002', agent: '0003' },
      formValues: {
        sysPrinAgents: [
          {
            originalData: payload.payload.data,
            sysPrinAgent: '0001-0002-0003',
            recordTypes
          }
        ]
      },
      SPAs: [],
      searchValue: '0001 0002 0003',
      handleHideSPA: mockHandleHideSPA,
      isClearAndReset: true,
      hasSearch: true,
      resetClearAndReset: jest.fn(),
      setFormValues
    } as any;

    const wrapper = await renderWithMockStore(<SPAGrid {...props} />, {
      initialState: getInitialState()
    });

    userEvent.click(wrapper.getByText('txt_spa_status'));
    userEvent.click(wrapper.getByText('txt_spa_status'));
    userEvent.click(wrapper.getByText('txt_spa_status'));
    userEvent.click(wrapper.getByText('txt_spa_range_from'));
    userEvent.click(wrapper.getByText('txt_spa_range_to'));
    userEvent.click(wrapper.getByText('txt_spa_generation_method'));

    expect(
      wrapper.getByText('txt_hide_spa_sys_prin_agent_add')
    ).toBeInTheDocument();
  });

  it('render with empty data and fetch data with props is all true and with sysPrinAgentData with pagination', async () => {
    mockActionsWorkflowSetup('getSPAListWorkflow', {
      match: true,
      payload
    });

    const props = {
      dataForm: { sys: '0001', prin: '0002', agent: '0003' },
      formValues: {
        sysPrinAgents: [
          {
            originalData: payload.payload.data,
            sysPrinAgent: '0001-0002-0003',
            recordTypes: [
              ...recordTypes,
              {
                status: STATUS_SPA.NotUpdated,
                id: 1,
                actionCode: '',
                parameters: [
                  { name: 'record.type', previousValue: 'A', newValue: '' }
                ]
              },
              {
                status: STATUS_SPA.New,
                id: 2,
                actionCode: '',
                parameters: [
                  { name: 'record.type', previousValue: 'A', newValue: '' }
                ]
              },
              {
                status: STATUS_SPA.NotUpdated,
                id: 3,
                actionCode: '',
                parameters: [
                  { name: 'record.type', previousValue: 'A', newValue: '' },
                  {
                    name: RecordFieldParameterEnum.GenerationMethod,
                    previousValue: 'G',
                    newValue: ''
                  }
                ]
              },
              {
                status: STATUS_SPA.NotUpdated,
                id: 4,
                actionCode: '',
                parameters: [
                  { name: 'record.type', previousValue: 'A', newValue: '' },
                  {
                    name: RecordFieldParameterEnum.GenerationMethod,
                    previousValue: 'D',
                    newValue: ''
                  }
                ]
              },
              {
                status: STATUS_SPA.Updated,
                id: 5,
                actionCode: '',
                parameters: [
                  { name: 'record.type', previousValue: 'A', newValue: '' },
                  {
                    name: RecordFieldParameterEnum.GenerationMethod,
                    previousValue: 'D',
                    newValue: ''
                  }
                ]
              },
              {
                status: STATUS_SPA.MarkedForDeletion,
                id: 6,
                actionCode: '',
                parameters: [
                  { name: 'record.type', previousValue: 'A', newValue: '' },
                  {
                    name: RecordFieldParameterEnum.GenerationMethod,
                    previousValue: 'G',
                    newValue: ''
                  }
                ]
              }
            ]
          }
        ]
      },
      SPAs: [],
      searchValue: '0001 0002 0003',
      handleHideSPA: mockHandleHideSPA,
      isClearAndReset: true,
      hasSearch: true,
      resetClearAndReset: jest.fn(),
      setFormValues
    } as any;

    const wrapper = await renderWithMockStore(<SPAGrid {...props} />, {
      initialState: getInitialState()
    });

    userEvent.click(wrapper.getByText('Paging_component_onChangePage'));

    userEvent.click(
      wrapper.getByText('Paging_component_handleOnChangePageSize')
    );

    expect(
      wrapper.getByText('txt_hide_spa_sys_prin_agent_add')
    ).toBeInTheDocument();
  });

  it('render with data and click txt_hide_spa_sys_prin_agent_add', async () => {
    mockActionsWorkflowSetup('getSPAListWorkflow', {
      match: true,
      payload
    });

    const props = {
      dataForm: { sys: '0001', prin: '0002', agent: '0003' },
      formValues: {
        sysPrinAgents: [
          {
            originalData: payload.payload.data,
            sysPrinAgent: '0001-0002-0003',
            recordTypes: [
              {
                status: STATUS_SPA.NotUpdated,
                id: 601254652,
                actionCode: '',
                parameters: [
                  { name: 'record.type', previousValue: 'A', newValue: '' }
                ]
              }
            ]
          }
        ]
      },
      SPAs: [],
      searchValue: '0001 0002 0003',
      handleHideSPA: mockHandleHideSPA,
      isClearAndReset: true,
      hasSearch: true,
      resetClearAndReset: jest.fn(),
      setFormValues
    } as any;

    const wrapper = await renderWithMockStore(<SPAGrid {...props} />, {
      initialState: getInitialState()
    });

    const button = wrapper.getByText('txt_hide_spa_sys_prin_agent_add');

    userEvent.click(button);

    expect(button).toBeInTheDocument();
  });

  it('render with data and click txt_add_record_type', async () => {
    mockActionsWorkflowSetup('getSPAListWorkflow', {
      match: true,
      payload
    });

    const props = {
      dataForm: { sys: '0001', prin: '0002', agent: '0003' },
      formValues: {
        sysPrinAgents: [
          {
            originalData: payload.payload.data,
            sysPrinAgent: '0001-0002-0003',
            recordTypes
          }
        ]
      },
      SPAs: { sys: ['0001, 0002, 0003'], prin: '0002', agent: '0003' },
      searchValue: '0001 0002 0003',
      handleHideSPA: mockHandleHideSPA,
      isClearAndReset: true,
      hasSearch: true,
      resetClearAndReset: jest.fn(),
      setFormValues
    } as any;

    const wrapper = await renderWithMockStore(<SPAGrid {...props} />, {
      initialState: getInitialState()
    });

    const button = wrapper.getByText('txt_add_record_type');

    userEvent.click(button);

    expect(button).toBeInTheDocument();

    act(() => {
      userEvent.click(
        wrapper.getByText('AddNewRecordModal_handleUpdateExpand')
      );
      userEvent.click(
        wrapper.getByText('AddNewRecordModal_resetDefaultFilter')
      );
      userEvent.click(wrapper.getByText('AddNewRecordModal_component'));
    });
  });

  it('render with data and click edit record type with value', async () => {
    mockRecord.mockReturnValue({ status: 'status', parameters: [] });
    mockActionsWorkflowSetup('getSPAListWorkflow', {
      match: true,
      payload
    });

    const props = {
      dataForm: { sys: '0001', prin: '0002', agent: '0003' },
      formValues: {
        sysPrinAgents: [
          {
            originalData: {
              ...payload.payload.data,
              recordTypes: [{ recordType: '' }]
            },
            sysPrinAgent: '0001-0002-0003',
            recordTypes
          }
        ]
      },
      SPAs: { sys: ['0001, 0002, 0003'], prin: '0002', agent: '0003' },
      searchValue: '0001 0002 0003',
      handleHideSPA: mockHandleHideSPA,
      isClearAndReset: true,
      hasSearch: true,
      resetClearAndReset: jest.fn(),
      setFormValues
    } as any;

    const wrapper = await renderWithMockStore(<SPAGrid {...props} />, {
      initialState: getInitialState()
    });

    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    expect(body?.children.length).toEqual(6);

    //SHOW Add new method modal //txt_edit
    const editButton = body?.children[1].querySelector(
      'button[class="btn btn-outline-primary btn-sm"]'
    ) as Element;

    fireEvent.click(editButton);

    userEvent.click(wrapper.getByText('AddNewRecordModal_component'));
  });

  it('render with data and click edit record type without value', async () => {
    mockRecord.mockReturnValue({ status: 'status', parameters: [] });
    mockActionsWorkflowSetup('getSPAListWorkflow', {
      match: true,
      payload
    });

    const props = {
      dataForm: { sys: '0001', prin: '0002', agent: '0003' },
      formValues: {
        sysPrinAgents: [
          {
            originalData: payload.payload.data,
            sysPrinAgent: '0001-0002-0003',
            recordTypes
          }
        ]
      },
      SPAs: { sys: ['0001, 0002, 0003'], prin: '0002', agent: '0003' },
      searchValue: '0001 0002 0003',
      handleHideSPA: mockHandleHideSPA,
      isClearAndReset: true,
      hasSearch: true,
      resetClearAndReset: jest.fn(),
      setFormValues
    } as any;

    const wrapper = await renderWithMockStore(<SPAGrid {...props} />, {
      initialState: getInitialState()
    });

    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    expect(body?.children.length).toEqual(6);

    //SHOW Add new method modal //txt_edit
    const editButton = body?.children[1].querySelector(
      'button[class="btn btn-outline-primary btn-sm"]'
    ) as Element;

    fireEvent.click(editButton);

    mockOnCloseWithoutRecord.mockReturnValue(true);
    userEvent.click(wrapper.getByText('AddNewRecordModal_component'));
  });

  it('render with data and click edit record type with empty parameters', async () => {
    mockRecord.mockReturnValue({});
    mockActionsWorkflowSetup('getSPAListWorkflow', {
      match: true,
      payload
    });

    const props = {
      dataForm: { sys: '0001', prin: '0002', agent: '0003' },
      formValues: {
        sysPrinAgents: [
          {
            originalData: payload.payload.data,
            sysPrinAgent: '0001-0002-0003',
            recordTypes
          }
        ]
      },
      SPAs: { sys: ['0001, 0002, 0003'], prin: '0002', agent: '0003' },
      searchValue: '0001 0002 0003',
      handleHideSPA: mockHandleHideSPA,
      isClearAndReset: true,
      hasSearch: true,
      resetClearAndReset: jest.fn(),
      setFormValues
    } as any;

    const wrapper = await renderWithMockStore(<SPAGrid {...props} />, {
      initialState: getInitialState()
    });

    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    expect(body?.children.length).toEqual(6);

    //SHOW Add new method modal //txt_edit
    const editButton = body?.children[1].querySelector(
      'button[class="btn btn-outline-primary btn-sm"]'
    ) as Element;

    fireEvent.click(editButton);

    mockOnCloseWithRecordEmptyArray.mockReturnValue(true);
    userEvent.click(wrapper.getByText('AddNewRecordModal_component'));
  });

  it('render with data and click delete record type  with onClose is true', async () => {
    mockOnCloseWithoutMethod.mockReturnValue(true);
    mockActionsWorkflowSetup('getSPAListWorkflow', {
      match: true,
      payload
    });

    const props = {
      dataForm: { sys: '0001', prin: '0002', agent: '0003' },
      formValues: {
        sysPrinAgents: [
          {
            originalData: payload.payload.data,
            sysPrinAgent: '0001-0002-0003',
            recordTypes
          }
        ]
      },
      SPAs: { sys: ['0001, 0002, 0003'], prin: '0002', agent: '0003' },
      searchValue: '0001 0002 0003',
      handleHideSPA: mockHandleHideSPA,
      isClearAndReset: true,
      hasSearch: true,
      resetClearAndReset: jest.fn(),
      setFormValues
    } as any;
    mockOnCloseWithoutMethod.mockReturnValue(true);

    const wrapper = await renderWithMockStore(<SPAGrid {...props} />, {
      initialState: getInitialState()
    });

    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );

    const removeButton = body?.children[0].querySelector(
      'button[class="btn btn-outline-danger btn-sm ml-8"]'
    ) as Element;

    fireEvent.click(removeButton);
    expect(
      wrapper.getByText('RemoveRecordModal_component')
    ).toBeInTheDocument();
    userEvent.click(wrapper.getByText('RemoveRecordModal_component'));
    expect(
      wrapper.queryByText('RemoveRecordModal_component')
    ).not.toBeInTheDocument();
  });

  it('render with data and click delete record type with onClose is false', async () => {
    mockOnCloseWithoutMethod.mockReturnValue(false);
    mockActionsWorkflowSetup('getSPAListWorkflow', {
      match: true,
      payload
    });

    const props = {
      dataForm: { sys: '0001', prin: '0002', agent: '0003' },
      formValues: {
        sysPrinAgents: [
          {
            originalData: payload.payload.data,
            sysPrinAgent: '0001-0002-0003',
            recordTypes
          }
        ]
      },
      SPAs: { sys: ['0001, 0002, 0003'], prin: '0002', agent: '0003' },
      searchValue: '0001 0002 0003',
      handleHideSPA: mockHandleHideSPA,
      isClearAndReset: true,
      hasSearch: true,
      resetClearAndReset: jest.fn(),
      setFormValues
    } as any;

    const wrapper = await renderWithMockStore(<SPAGrid {...props} />, {
      initialState: getInitialState()
    });

    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );

    const removeButton = body?.children[0].querySelector(
      'button[class="btn btn-outline-danger btn-sm ml-8"]'
    ) as Element;

    fireEvent.click(removeButton);
    expect(
      wrapper.getByText('RemoveRecordModal_component')
    ).toBeInTheDocument();
  });

  it('render with data and click checkbox', async () => {
    mockActionsWorkflowSetup('getSPAListWorkflow', {
      match: true,
      payload
    });

    const props = {
      dataForm: { sys: '0001', prin: '0002', agent: '0003' },
      formValues: {
        sysPrinAgents: [
          {
            originalData: payload.payload.data,
            sysPrinAgent: '0001-0002-0003',
            recordTypes: [
              {
                status: STATUS_SPA.NotUpdated,
                id: 601254651,
                actionCode: '',
                parameters: [
                  { name: 'record.type', previousValue: 'S', newValue: '' }
                ]
              },
              {
                status: STATUS_SPA.NotUpdated,
                id: 601254652,
                actionCode: '',
                parameters: [
                  { name: 'record.type', previousValue: 'A', newValue: '' }
                ]
              },
              {
                status: STATUS_SPA.New,
                id: 601254653,
                actionCode: '',
                parameters: [
                  { name: 'record.type', previousValue: 'X', newValue: '' }
                ]
              }
            ]
          }
        ]
      },
      SPAs: { sys: ['0001, 0002, 0003'], prin: '0002', agent: '0003' },
      searchValue: '0001 0002 0003',
      handleHideSPA: mockHandleHideSPA,
      isClearAndReset: true,
      hasSearch: true,
      resetClearAndReset: jest.fn(),
      setFormValues
    } as any;

    const wrapper = await renderWithMockStore(<SPAGrid {...props} />, {
      initialState: getInitialState()
    });

    const checkbox = wrapper.getAllByRole('checkbox');

    act(() => {
      userEvent.click(checkbox[0]);
    });

    userEvent.click(wrapper.getByText('txt_spa_bulk_deleted'));
    setTimeout(() => {
      expect(wrapper.getByText('txt_spa_bulk_deleted')).toBeInTheDocument();
      mockOnCloseWithoutMethod.mockReturnValue(true);
      userEvent.click(wrapper.getByText('RemoveRecordModal_component'));
    }, 2);
  });

  it('render with data and click expand outside', async () => {
    mockActionsWorkflowSetup('getSPAListWorkflow', {
      match: true,
      payload
    });

    const props = {
      dataForm: { sys: '0001', prin: '0002', agent: '0003' },
      formValues: {
        sysPrinAgents: [
          {
            originalData: payload.payload.data,
            sysPrinAgent: '0001-0002-0003',
            recordTypes: [
              {
                status: STATUS_SPA.NotUpdated,
                id: 601254651,
                actionCode: '',
                parameters: [
                  { name: 'record.type', previousValue: 'S', newValue: '' }
                ]
              },
              {
                status: STATUS_SPA.NotUpdated,
                id: 601254652,
                actionCode: '',
                parameters: [
                  { name: 'record.type', previousValue: 'A', newValue: '' }
                ]
              },
              {
                status: STATUS_SPA.NotUpdated,
                id: 601254653,
                actionCode: '',
                parameters: [
                  { name: 'record.type', previousValue: 'X', newValue: '' }
                ]
              }
            ]
          }
        ]
      },
      SPAs: { sys: ['0001, 0002, 0003'], prin: '0002', agent: '0003' },
      searchValue: '0001 0002 0003',
      handleHideSPA: mockHandleHideSPA,
      isClearAndReset: true,
      hasSearch: true,
      resetClearAndReset: jest.fn(),
      setFormValues
    } as any;

    const wrapper = await renderWithMockStore(<SPAGrid {...props} />, {
      initialState: getInitialState()
    });

    const buttons = wrapper.getAllByRole('button');

    act(() => {
      userEvent.click(buttons[0]);
    });

    setTimeout(() => {
      expect(wrapper.getByText('txt_add_record_type')).toBeInTheDocument();
    }, 2);
  });

  it('render with data and click expand inside', async () => {
    mockActionsWorkflowSetup('getSPAListWorkflow', {
      match: true,
      payload
    });

    const props = {
      dataForm: { sys: '0001', prin: '0002', agent: '0003' },
      formValues: {
        sysPrinAgents: [
          {
            originalData: payload.payload.data,
            sysPrinAgent: '0001-0002-0003',
            recordTypes: [
              {
                status: STATUS_SPA.NotUpdated,
                id: 601254651,
                actionCode: '',
                parameters: [
                  { name: 'record.type', previousValue: 'S', newValue: '' }
                ]
              },
              {
                status: STATUS_SPA.NotUpdated,
                id: 601254652,
                actionCode: '',
                parameters: [
                  { name: 'record.type', previousValue: 'A', newValue: '' }
                ]
              },
              {
                status: STATUS_SPA.NotUpdated,
                id: 601254653,
                actionCode: '',
                parameters: [
                  { name: 'record.type', previousValue: 'X', newValue: '' }
                ]
              }
            ]
          }
        ]
      },
      SPAs: { sys: ['0001, 0002, 0003'], prin: '0002', agent: '0003' },
      searchValue: '0001 0002 0003',
      handleHideSPA: mockHandleHideSPA,
      isClearAndReset: true,
      hasSearch: true,
      resetClearAndReset: jest.fn(),
      setFormValues
    } as any;

    const wrapper = await renderWithMockStore(<SPAGrid {...props} />, {
      initialState: getInitialState()
    });

    const buttons = wrapper.getAllByRole('button');

    act(() => {
      userEvent.click(buttons[1]);
      userEvent.click(buttons[2]);
      userEvent.click(buttons[3]);
    });

    setTimeout(() => {
      expect(wrapper.getByText('txt_add_record_type')).toBeInTheDocument();
    }, 2);
  });

  it('render with data and click cancel record type', async () => {
    mockOnCloseWithoutMethod.mockReturnValue(false);
    mockActionsWorkflowSetup('getSPAListWorkflow', {
      match: true,
      payload
    });

    const props = {
      dataForm: { sys: '0001', prin: '0002', agent: '0003' },
      formValues: {
        sysPrinAgents: [
          {
            originalData: payload.payload.data,
            sysPrinAgent: '0001-0002-0003',
            recordTypes: [
              {
                status: STATUS_SPA.MarkedForDeletion,
                id: 601254651,
                actionCode: '',
                parameters: [
                  { name: 'record.type', previousValue: 'S', newValue: '' }
                ]
              }
            ]
          }
        ]
      },
      SPAs: { sys: ['0001, 0002, 0003'], prin: '0002', agent: '0003' },
      searchValue: '0001 0002 0003',
      handleHideSPA: mockHandleHideSPA,
      isClearAndReset: true,
      hasSearch: true,
      resetClearAndReset: jest.fn(),
      setFormValues
    } as any;

    const wrapper = await renderWithMockStore(<SPAGrid {...props} />, {
      initialState: getInitialState()
    });

    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );

    const cancelButton = body?.children[0]?.querySelector(
      'button[class="btn btn-outline-danger btn-sm"]'
    ) as Element;

    fireEvent.click(cancelButton);
    expect(
      wrapper.getByText('RemoveRecordModal_component')
    ).toBeInTheDocument();
  });

  it('render with data and click MarkedForDeletion record type', async () => {
    mockOnCloseWithoutMethod.mockReturnValue(true);
    mockActionsWorkflowSetup('getSPAListWorkflow', {
      match: true,
      payload
    });

    const props = {
      dataForm: { sys: '0001', prin: '0002', agent: '0003' },
      formValues: {
        sysPrinAgents: [
          {
            originalData: payload.payload.data,
            sysPrinAgent: '0001-0002-0003',
            recordTypes: [
              {
                status: STATUS_SPA.MarkedForDeletion,
                id: 601254651,
                actionCode: '',
                parameters: [
                  { name: 'record.type', previousValue: 'S', newValue: '' }
                ]
              }
            ]
          }
        ]
      },
      SPAs: { sys: ['0001, 0002, 0003'], prin: '0002', agent: '0003' },
      searchValue: '0001 0002 0003',
      handleHideSPA: mockHandleHideSPA,
      isClearAndReset: true,
      hasSearch: true,
      resetClearAndReset: jest.fn(),
      setFormValues
    } as any;

    const wrapper = await renderWithMockStore(<SPAGrid {...props} />, {
      initialState: getInitialState()
    });

    userEvent.click(
      wrapper.container.querySelector('td input[type="checkbox"]')!
    );

    userEvent.click(wrapper.getByText('txt_cancel'));
    expect(
      wrapper.getByText('RemoveRecordModal_component')
    ).toBeInTheDocument();

    userEvent.click(wrapper.getByText('RemoveRecordModal_component'));
    expect(
      wrapper.queryByText('RemoveRecordModal_component')
    ).not.toBeInTheDocument();
  });

  it('render with data and click new record type', async () => {
    mockOnCloseWithoutMethod.mockReturnValue(true);
    mockActionsWorkflowSetup('getSPAListWorkflow', {
      match: true,
      payload
    });

    const props = {
      dataForm: { sys: '0001', prin: '0002', agent: '0003' },
      formValues: {
        sysPrinAgents: [
          {
            originalData: payload.payload.data,
            sysPrinAgent: '0001-0002-0003',
            recordTypes: [
              {
                status: STATUS_SPA.New,
                id: 601254651,
                actionCode: '',
                parameters: [
                  { name: 'record.type', previousValue: 'S', newValue: '' }
                ]
              }
            ]
          }
        ]
      },
      SPAs: { sys: ['0001, 0002, 0003'], prin: '0002', agent: '0003' },
      searchValue: '0001 0002 0003',
      handleHideSPA: mockHandleHideSPA,
      isClearAndReset: true,
      hasSearch: true,
      resetClearAndReset: jest.fn(),
      setFormValues
    } as any;

    const wrapper = await renderWithMockStore(<SPAGrid {...props} />, {
      initialState: getInitialState()
    });

    userEvent.click(
      wrapper.container.querySelector('td input[type="checkbox"]')!
    );
    userEvent.click(wrapper.getByText('txt_delete'));
    expect(
      wrapper.getByText('RemoveRecordModal_component')
    ).toBeInTheDocument();

    userEvent.click(wrapper.getByText('RemoveRecordModal_component'));
    expect(
      wrapper.queryByText('RemoveRecordModal_component')
    ).not.toBeInTheDocument();
  });

  it('render with data and click NotUpdated record type', async () => {
    mockOnCloseWithoutMethod.mockReturnValue(true);
    mockActionsWorkflowSetup('getSPAListWorkflow', {
      match: true,
      payload
    });

    const props = {
      dataForm: { sys: '0001', prin: '0002', agent: '0003' },
      formValues: {
        sysPrinAgents: [
          {
            originalData: payload.payload.data,
            sysPrinAgent: '0001-0002-0003',
            recordTypes: [
              {
                status: STATUS_SPA.NotUpdated,
                id: 601254651,
                actionCode: '',
                parameters: [
                  { name: 'record.type', previousValue: 'S', newValue: '' }
                ]
              }
            ]
          }
        ]
      },
      SPAs: { sys: ['0001, 0002, 0003'], prin: '0002', agent: '0003' },
      searchValue: '0001 0002 0003',
      handleHideSPA: mockHandleHideSPA,
      isClearAndReset: true,
      hasSearch: true,
      resetClearAndReset: jest.fn(),
      setFormValues
    } as any;

    const wrapper = await renderWithMockStore(<SPAGrid {...props} />, {
      initialState: getInitialState()
    });

    userEvent.click(
      wrapper.container.querySelector('td input[type="checkbox"]')!
    );
    userEvent.click(wrapper.getByText('txt_delete'));
    expect(
      wrapper.getByText('RemoveRecordModal_component')
    ).toBeInTheDocument();

    userEvent.click(wrapper.getByText('RemoveRecordModal_component'));
    expect(
      wrapper.queryByText('RemoveRecordModal_component')
    ).not.toBeInTheDocument();
  });

  it('render with data and click Updated record type', async () => {
    mockOnCloseWithoutMethod.mockReturnValue(true);
    mockActionsWorkflowSetup('getSPAListWorkflow', {
      match: true,
      payload
    });

    const props = {
      dataForm: { sys: '0001', prin: '0002', agent: '0003' },
      formValues: {
        sysPrinAgents: [
          {
            originalData: payload.payload.data,
            sysPrinAgent: '0001-0002-0003',
            recordTypes: [
              {
                status: STATUS_SPA.Updated,
                id: 601254651,
                actionCode: '',
                parameters: [
                  { name: 'record.type', previousValue: 'S', newValue: '' }
                ]
              }
            ]
          }
        ]
      },
      SPAs: { sys: ['0001, 0002, 0003'], prin: '0002', agent: '0003' },
      searchValue: '0001 0002 0003',
      handleHideSPA: mockHandleHideSPA,
      isClearAndReset: true,
      hasSearch: true,
      resetClearAndReset: jest.fn(),
      setFormValues
    } as any;

    const wrapper = await renderWithMockStore(<SPAGrid {...props} />, {
      initialState: getInitialState()
    });
    userEvent.click(
      wrapper.container.querySelector('td input[type="checkbox"]')!
    );
    userEvent.click(wrapper.getByText('txt_cancel'));
    expect(
      wrapper.getByText('RemoveRecordModal_component')
    ).toBeInTheDocument();

    userEvent.click(wrapper.getByText('RemoveRecordModal_component'));
    expect(
      wrapper.queryByText('RemoveRecordModal_component')
    ).not.toBeInTheDocument();
  });

  it('render with data and click txt_hide_spa_sys_prin_agent_add', async () => {
    mockActionsWorkflowSetup('getSPAListWorkflow', {
      match: true,
      payload
    });

    const props = {
      dataForm: { sys: '0001', prin: '0002', agent: '0003' },
      formValues: {
        sysPrinAgents: [
          {
            originalData: payload.payload.data,
            sysPrinAgent: '0001-0002-0003',
            recordTypes: [
              {
                status: STATUS_SPA.NotUpdated,
                id: 601254652,
                actionCode: '',
                parameters: [
                  { name: 'record.type', previousValue: 'A', newValue: '' }
                ]
              }
            ]
          }
        ]
      },
      SPAs: [],
      searchValue: '0001 0002 0003',
      handleHideSPA: mockHandleHideSPA,
      isClearAndReset: true,
      hasSearch: true,
      resetClearAndReset: jest.fn(),
      setFormValues
    } as any;

    const wrapper = await renderWithMockStore(<SPAGrid {...props} />, {
      initialState: getInitialState()
    });

    const checkbox = wrapper.getAllByRole('checkbox');

    userEvent.click(checkbox[0]);

    const button = wrapper.getByText('txt_add_record_type');

    userEvent.click(button);

    userEvent.click(wrapper.getByText('AddNewRecordModal_component'));

    wrapper.debug();
  });
});
