import { ServiceSubjectSection } from 'app/constants/enums';
import parseFormValues from './parseFormValues';

describe('pages > WorkflowManageAccountSerialization > ManageAccountSerializationStep > parseFormValues', () => {
  it('parseFormValues', async () => {
    let configurations: any = {
      sysPrinAgents: [{ sysPrinAgent: '001-002-003' }, {}]
    };
    const id = 'id';
    const input: any = {
      data: {
        configurations,
        workflowSetupData: [{ id, status: 'INPROGRESS' }]
      }
    };

    // isvalid
    let result = await parseFormValues(input, id, jest.fn());
    expect(result?.isValid).toEqual(true);

    // other step
    result = await parseFormValues(input, 'id1', jest.fn());
    expect(result).toBeUndefined();

    configurations = {
      spas: [
        {
          serviceSubjectSection: ServiceSubjectSection.DLO,
          name: 'name'
        }
      ]
    };
    input.data.configurations = configurations;

    result = await parseFormValues(input, id, jest.fn());
    expect(result?.isValid).toBeFalsy();
  });
});
