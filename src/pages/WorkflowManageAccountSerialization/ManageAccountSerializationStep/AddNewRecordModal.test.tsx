import { fireEvent } from '@testing-library/dom';
import { renderWithMockStore } from 'app/utils';
import {
  ComboBoxProps,
  DropdownBaseChangeEvent,
  DropdownBaseGroupProps,
  DropdownBaseItemProps
} from 'app/_libraries/_dls';
import React from 'react';
import { act } from 'react-dom/test-utils';
import AddNewRecordModal, { IProps } from './AddNewRecordModal';
import { STATUS_SPA } from './helper';

// mock area
jest.mock('app/components/SimpleSearch', () => {
  const ActualReact = jest.requireActual('react');
  return {
    __esModule: true,
    default: ActualReact.forwardRef(
      ({ onSearch }: any, ref: React.Ref<HTMLDivElement>) => {
        ActualReact.useImperativeHandle(ref, () => ({
          clear: () => {}
        }));
        return (
          <div>
            <div>SimpleSearch</div>
            <div onClick={() => onSearch('a')}>Search a</div>
            <div onClick={() => onSearch('xxx')}>Search xxx</div>
          </div>
        );
      }
    )
  };
});

jest.mock('app/components/EnhanceDropdownList', () => {
  return {
    __esModule: true,
    default: ({
      placeholder,
      dataTestId,
      value,
      onChange,
      options,
      ...rest
    }: any) => (
      <div data-testid={`${dataTestId}_dls-dropdown-list`}>
        <div>EnhanceDropdownList</div>
        <div>{placeholder}</div>
        <div>{(value || '').toString()}</div>
        <div>
          {options?.map((option: RefData) => (
            <div
              key={option.code}
              className="item"
              onClick={() => onChange({ target: { value: option } })}
            >
              {option.text}
            </div>
          ))}
        </div>
      </div>
    )
  };
});

jest.mock('app/_libraries/_dls/components/ComboBox', () => {
  const actualModule = jest.requireActual(
    'app/_libraries/_dls/components/ComboBox'
  );
  const MockComboBox = (props: ComboBoxProps) => {
    const { onChange, label, onBlur, error, children, dataTestId } = props;

    return (
      <div className="dls-input-container">
        <input
          data-testid={`ComboBox.Input_onChange_${dataTestId}`}
          onChange={(e: any) => {
            const value: RefData = e.target.value;
            onChange!({
              target: { value: value }
            } as DropdownBaseChangeEvent);
          }}
          onBlur={onBlur}
        />
        {!!error && <p data-testid="ComboBox.Error">{error?.message}</p>}
        <span>{label}</span>
        <div>{children}</div>
      </div>
    );
  };

  MockComboBox.Item = ({ value, label }: DropdownBaseItemProps) => (
    <div key={`${value}-${label}`} data-testid={`ComboBox.Item-${label}`}>
      {label}
    </div>
  );

  MockComboBox.Group = ({ children, label }: DropdownBaseGroupProps) => (
    <div key={`${label}`} data-testid={`ComboBox.Group-${label}`}>
      {children}
    </div>
  );
  return {
    ...actualModule,
    __esModule: true,
    default: MockComboBox
  };
});

// Test area
describe('pages > WorkflowManageAccountSerialization >  ManageAccountSerializationStep> AddNewRecordModal', () => {
  const testId = 'testId';
  const storeConfig = {
    initialState: {
      workflowSetup: {
        elementMetadata: {
          elements: [
            {
              id: '145',
              name: 'record.type',
              options: [
                {
                  value: '',
                  description: 'None'
                },
                {
                  value: 'A',
                  description:
                    'Account identifiers for new separate-entity accounts when the PI ID/Account Equal parameter in the Basic Controls (AO PA BC) section of the Product Control File is set to 1 or 2. Record type O is the default for accounts and presentation instruments unless you define a record type A to generate presentation instruments.'
                },
                {
                  value: 'B',
                  description:
                    'Presentation instrument identifiers for separate-entity accounts with additional customer name adds entered via batch transmission.'
                },
                {
                  value: 'O',
                  description:
                    'Account identifiers for new single-entity accounts, or account and customer presentation instrument identifiers for new separate-entity accounts. Record type O is the default for accounts and presentation instruments unless you define a record type A to generate presentation instruments.'
                },
                {
                  value: 'P',
                  description:
                    'Presentation instrument identifiers for accounts that have the virtual indicator set at the account level. Record type P is used to enable MasterCard Virtual Card presentation instruments.'
                },
                {
                  value: 'S',
                  description:
                    'Account identifiers for single-entity accounts, or separate-entity security suspense record (SSR) accounts generated as part of a lost/stolen action.'
                },
                {
                  value: 'T',
                  description:
                    'Presentation instrument identifiers for all types of accounts that are used for adding a new token presentation instrument or a virtual/digital ID using PI type code 016. If you do not establish a serial number range for the value of T, the system defaults to the range established for value S.'
                }
              ]
            },
            {
              id: '147',
              name: 'use.alternate.agent',
              options: [
                {
                  value: '',
                  description: 'None'
                },
                {
                  value: 'N',
                  description: 'No, do not use an alternate agent.',
                  selected: true
                },
                {
                  value: 'Y',
                  description: 'Yes, use an alternate agent.'
                }
              ]
            },
            {
              id: '148',
              name: 'account.id.or.piid',
              options: [
                {
                  value: 'AC',
                  description: 'The System uses the account identifier.'
                },
                {
                  value: 'PI',
                  description:
                    'The System uses the presentation instrument identifier.'
                }
              ]
            }
          ]
        }
      }
    }
  };

  const onCloseMock = jest.fn();
  const handleUpdateExpandMock = jest.fn();
  const resetDefaultFilterMock = jest.fn();

  const defaultProps: IProps = {
    id: 'testId',
    show: true,
    title: 'title',
    dataForm: { sys: '0000', prin: '0200', agent: '3456' },
    SPAs: {
      sys: ['0000', '0010', '0020'],
      prin: {
        '0000': ['0200', '0220', '0230'],
        '0010': ['0300', '0330', '0304'],
        '0020': ['0400', '1400', '2400']
      },
      agent: {
        '00000200': ['3456', '0001', '0002'],
        '00000220': ['3451', '0002', '0003'],
        '00000230': ['3221', '0008', '0004'],
        '00100300': ['0003', '0004', '0005'],
        '00100330': ['0003', '0004', '0005'],
        '00100304': ['0003', '0004', '0005'],
        '00200400': ['0006', '0007', '0008'],
        '00201400': ['0006', '0007', '0008'],
        '00202400': ['0006', '0007', '0008']
      }
    },
    originalData: {
      defaultPrefix: 'minim et amet et',
      types: [
        {
          value: '',
          description: 'None'
        },
        {
          value: 'A',
          description:
            'Account identifiers for new separate-entity accounts when the PI ID/Account Equal parameter in the Basic Controls (AO PA BC) section of the Product Control File is set to 1 or 2. Record type O is the default for accounts and presentation instruments unless you define a record type A to generate presentation instruments.'
        },
        {
          value: 'B',
          description:
            'Presentation instrument identifiers for separate-entity accounts with additional customer name adds entered via batch transmission.'
        },
        {
          value: 'O',
          description:
            'Account identifiers for new single-entity accounts, or account and customer presentation instrument identifiers for new separate-entity accounts. Record type O is the default for accounts and presentation instruments unless you define a record type A to generate presentation instruments.'
        },
        {
          value: 'P',
          description:
            'Presentation instrument identifiers for accounts that have the virtual indicator set at the account level. Record type P is used to enable MasterCard Virtual Card presentation instruments.'
        },
        {
          value: 'S',
          description:
            'Account identifiers for single-entity accounts, or separate-entity security suspense record (SSR) accounts generated as part of a lost/stolen action.'
        },
        {
          value: 'T',
          description:
            'Presentation instrument identifiers for all types of accounts that are used for adding a new token presentation instrument or a virtual/digital ID using PI type code 016. If you do not establish a serial number range for the value of T, the system defaults to the range established for value S.'
        }
      ],
      generationMethods: [
        {
          value: '',
          description: 'None',
          selected: false
        },
        {
          value: 'R',
          description: 'Random',
          selected: false
        },
        {
          value: 'S',
          description: 'Sequential',
          selected: true
        }
      ],
      useAlternateAgents: [
        {
          value: '',
          description: 'None',
          selected: false
        },
        {
          value: 'N',
          description: 'No, do not use an alternate agent.',
          selected: true
        },
        {
          value: 'Y',
          description: 'Yes, use an alternate agent.',
          selected: false
        }
      ],
      recordTypes: [
        {
          id: 171810128,
          recordType: 'A',
          generationMethod: 'S',
          rangeFrom: 6877,
          rangeTo: 86970,
          serialNumber: 49453,
          warningLevel: 46,
          prefix: 'vero ut vel',
          alternatePrefix: 55478,
          accountIDOrPIID: 'AC',
          nextFrom: 29176,
          nextTo: 80204,
          availableRecordNumber: 782703326,
          useAlternateAgent: 'N',
          alternatePrincipal: '0020',
          alternateAgent: '0103',
          generationMethods: [
            {
              value: 'R',
              description: 'Random'
            },
            {
              value: 'S',
              description: 'Sequential'
            }
          ]
        },
        {
          id: 1291936407,
          recordType: 'B',
          generationMethod: 'R',
          rangeFrom: 12837,
          rangeTo: 93277,
          serialNumber: 47628,
          warningLevel: 91,
          prefix: 'duo duis',
          alternatePrefix: 81313,
          accountIDOrPIID: 'PI',
          nextFrom: 43107,
          nextTo: 88793,
          availableRecordNumber: 2047769768,
          useAlternateAgent: 'N',
          alternatePrincipal: '0020',
          alternateAgent: '0022',
          generationMethods: [
            {
              value: 'R',
              description: 'Random'
            }
          ]
        },
        {
          id: 601142489,
          recordType: 'O',
          generationMethod: 'S',
          rangeFrom: 14244,
          rangeTo: 83616,
          serialNumber: 50341,
          warningLevel: 27,
          prefix: 'dolor eirmod invidunt sanctus erat erat enim',
          alternatePrefix: 28803,
          accountIDOrPIID: 'PI',
          nextFrom: 12378,
          nextTo: 93434,
          availableRecordNumber: 955716221,
          useAlternateAgent: 'N',
          alternatePrincipal: '0020',
          alternateAgent: '0010',
          generationMethods: [
            {
              value: 'R',
              description: 'Random'
            },
            {
              value: 'S',
              description: 'Sequential'
            }
          ]
        },
        {
          id: 505048193,
          recordType: 'P',
          generationMethod: 'G',
          rangeFrom: 4836,
          rangeTo: 91717,
          serialNumber: 46478,
          warningLevel: 62,
          prefix: 'aliquam nam nam',
          alternatePrefix: 83167,
          accountIDOrPIID: 'PI',
          nextFrom: 41339,
          nextTo: 94731,
          availableRecordNumber: 87776829,
          useAlternateAgent: 'Y',
          alternatePrincipal: '0020',
          alternateAgent: '0010',
          generationMethods: [
            {
              value: 'G',
              description: 'Generated'
            }
          ]
        },
        {
          id: 1399397413,
          recordType: 'S',
          generationMethod: 'S',
          rangeFrom: 30598,
          rangeTo: 90033,
          serialNumber: 39373,
          warningLevel: 6,
          prefix: 'magna et aliquyam lorem aliquip ipsum',
          alternatePrefix: 89759,
          accountIDOrPIID: 'PI',
          nextFrom: 49339,
          nextTo: 66007,
          availableRecordNumber: 1725301649,
          useAlternateAgent: 'Y',
          alternatePrincipal: '0020',
          alternateAgent: '0010',
          generationMethods: [
            {
              value: 'R',
              description: 'Random'
            },
            {
              value: 'S',
              description: 'Sequential'
            }
          ]
        },
        {
          id: 38772461,
          recordType: 'T',
          generationMethod: 'D',
          rangeFrom: 469,
          rangeTo: 89018,
          serialNumber: 43840,
          warningLevel: 79,
          prefix: 'nonumy nostrud clita',
          alternatePrefix: 70775,
          accountIDOrPIID: 'PI',
          nextFrom: 5801,
          nextTo: 56111,
          availableRecordNumber: 392053860,
          useAlternateAgent: 'N',
          alternatePrincipal: '0020',
          alternateAgent: '0011',
          generationMethods: []
        }
      ]
    },
    onClose: onCloseMock,
    handleUpdateExpand: handleUpdateExpandMock,
    resetDefaultFilter: resetDefaultFilterMock
  };

  const renderComponent = async (props?: IProps) => {
    const element = <AddNewRecordModal {...defaultProps} {...props} />;
    const wrapper = await renderWithMockStore(element, storeConfig, {}, testId);
    return wrapper;
  };

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render component create', async () => {
    const props = {
      draftRecord: {
        status: 'Not Updated',
        id: 1291936407,
        actionCode: '',
        parameters: [
          {
            name: 'record.type',
            previousValue: 'B',
            newValue: ''
          },
          {
            name: 'generation.method',
            previousValue: 'R',
            newValue: ''
          },
          {
            name: 'range.from',
            previousValue: '12837',
            newValue: ''
          },
          {
            name: 'range.to',
            previousValue: '93277',
            newValue: ''
          },
          {
            name: 'serial.number',
            previousValue: '47628',
            newValue: ''
          },
          {
            name: 'warning.level',
            previousValue: '91',
            newValue: ''
          },
          {
            name: 'prefix',
            previousValue: 'duo duis',
            newValue: ''
          },
          {
            name: 'alternate.prefix',
            previousValue: '81313',
            newValue: ''
          },
          {
            name: 'account.id.or.piid',
            previousValue: 'PI',
            newValue: ''
          },
          {
            name: 'next.from',
            previousValue: '43107',
            newValue: ''
          },
          {
            name: 'next.to',
            previousValue: '88793',
            newValue: ''
          },
          {
            name: 'available.record.number',
            previousValue: '2047769768',
            newValue: ''
          },
          {
            name: 'use.alternate.agent',
            previousValue: 'N',
            newValue: ''
          },
          {
            name: 'alternate.principal',
            previousValue: '0020',
            newValue: ''
          },
          {
            name: 'alternate.agent',
            previousValue: '0022',
            newValue: ''
          }
        ]
      }
    } as any;
    const wrapper = await renderComponent(props);
    const { getByText, getAllByText } = wrapper;
    expect(getByText('txt_parameter_list')).toBeInTheDocument();
    expect(getByText('SimpleSearch')).toBeInTheDocument();
    expect(getAllByText('EnhanceDropdownList')).toHaveLength(3);

    act(() => {
      fireEvent.click(getByText('Search a'));
    });
    act(() => {
      fireEvent.click(getByText('Search xxx'));
    });
    fireEvent.click(getByText('txt_clear_and_reset'));
    fireEvent.click(getByText('txt_add'));

    expect(handleUpdateExpandMock).toBeCalled();
    expect(onCloseMock).toBeCalled();
    expect(resetDefaultFilterMock).toBeCalled();
    // last
    fireEvent.click(getByText('txt_cancel'));
    expect(onCloseMock).toBeCalled();
  });

  it('should render component edit Status Not Updated', async () => {
    const props = {
      isCreate: false,
      infoSpa: {
        status: STATUS_SPA.NotUpdated,
        id: 1291936407,
        actionCode: '',
        parameters: [
          {
            name: 'record.type',
            previousValue: 'B',
            newValue: ''
          },
          {
            name: 'generation.method',
            previousValue: 'R',
            newValue: ''
          },
          {
            name: 'range.from',
            previousValue: '12837999',
            newValue: ''
          },
          {
            name: 'range.to',
            previousValue: '93277',
            newValue: ''
          },
          {
            name: 'serial.number',
            previousValue: '47628',
            newValue: ''
          },
          {
            name: 'warning.level',
            previousValue: '91',
            newValue: ''
          },
          {
            name: 'prefix',
            previousValue: 'duo duis',
            newValue: ''
          },
          {
            name: 'alternate.prefix',
            previousValue: '81313',
            newValue: ''
          },
          {
            name: 'account.id.or.piid',
            previousValue: 'PI',
            newValue: ''
          },
          {
            name: 'next.from',
            previousValue: '88793',
            newValue: ''
          },
          {
            name: 'next.to',
            previousValue: '48793',
            newValue: ''
          },
          {
            name: 'available.record.number',
            previousValue: '2047769768',
            newValue: ''
          },
          {
            name: 'use.alternate.agent',
            previousValue: 'N',
            newValue: ''
          },
          {
            name: 'alternate.principal',
            previousValue: '0200',
            newValue: '0200'
          },
          {
            name: 'alternate.agent',
            previousValue: '3456',
            newValue: '3456'
          }
        ]
      }
    } as any;
    const wrapper = await renderComponent(props);
    const { getByText, getByTestId, getAllByText } = wrapper;
    expect(getByText('txt_parameter_list')).toBeInTheDocument();
    expect(getByText('SimpleSearch')).toBeInTheDocument();
    expect(getAllByText('EnhanceDropdownList')).toHaveLength(4);

    act(() => {
      fireEvent.click(getByText('txt_save'));
    });

    // Input value trigger errors
    const rangeFromInput = getByTestId(
      'addNewRecord__rangeFrom_dls-text-box_input'
    );
    const rangeToInput = getByTestId(
      'addNewRecord__rangeTo_dls-text-box_input'
    );
    const serialNumberInput = getByTestId(
      'addNewRecord__serialNumber_dls-text-box_input'
    );

    const nextFromInput = getByTestId(
      'addNewRecord__nextFrom_dls-text-box_input'
    );
    const nextToInput = getByTestId('addNewRecord__nextTo_dls-text-box_input');

    expect(rangeFromInput).toBeInTheDocument();
    fireEvent.change(rangeFromInput, { target: { value: 'abcd' } });
    fireEvent.change(rangeFromInput, { target: { value: '1234' } });
    fireEvent.change(rangeToInput, { target: { value: '1234' } });
    fireEvent.change(serialNumberInput, { target: { value: '' } });
    fireEvent.click(getByText('txt_save'));
    fireEvent.change(serialNumberInput, { target: { value: '1235' } });
    fireEvent.click(getByText('txt_save'));

    // Clear errors
    fireEvent.change(rangeFromInput, { target: { value: '2345' } });
    fireEvent.change(rangeToInput, { target: { value: '5678' } });
    fireEvent.change(serialNumberInput, { target: { value: '3456' } });
    fireEvent.change(nextFromInput, { target: { value: '1234' } });
    fireEvent.change(nextToInput, { target: { value: '3456' } });

    // Change dropdownlist Record Type
    const recordTypeDropdown = wrapper.getByTestId(
      'addNewRecord__recordType_dls-dropdown-list'
    );
    const item = recordTypeDropdown.querySelectorAll('.item').item(1);
    fireEvent.click(item);

    // Change combobox AlternatePrincipal
    const alternatePrincipalCombobox = wrapper.getByTestId(
      'ComboBox.Input_onChange_addNewRecord__alternatePrincipal'
    );
    expect(alternatePrincipalCombobox).toBeInTheDocument();

    fireEvent.change(alternatePrincipalCombobox!, {
      target: { value: { code: '0300', text: '0300' } }
    });

    // Save
    fireEvent.click(getByText('txt_save'));
    expect(handleUpdateExpandMock).toBeCalled();
    expect(onCloseMock).toBeCalled();
    expect(resetDefaultFilterMock).toBeCalled();
  });

  it('should render component edit Status Update', async () => {
    const props = {
      isCreate: false,
      title: 'Edit record type',
      infoSpa: {
        status: STATUS_SPA.Updated,
        id: 1291936407,
        actionCode: '',
        parameters: [
          {
            name: 'record.type',
            previousValue: 'B',
            newValue: ''
          },
          {
            name: 'generation.method',
            previousValue: 'R',
            newValue: ''
          },
          {
            name: 'range.from',
            previousValue: '1234',
            newValue: ''
          },
          {
            name: 'range.to',
            previousValue: '4567',
            newValue: ''
          },
          {
            name: 'serial.number',
            previousValue: '2345',
            newValue: ''
          },
          {
            name: 'warning.level',
            previousValue: '91',
            newValue: ''
          },
          {
            name: 'prefix',
            previousValue: 'duo duis',
            newValue: ''
          },
          {
            name: 'alternate.prefix',
            previousValue: '81313',
            newValue: ''
          },
          {
            name: 'account.id.or.piid',
            previousValue: 'PI',
            newValue: ''
          },
          {
            name: 'next.from',
            previousValue: '1234',
            newValue: ''
          },
          {
            name: 'next.to',
            previousValue: '2345',
            newValue: ''
          },
          {
            name: 'available.record.number',
            previousValue: '2047769768',
            newValue: ''
          },
          {
            name: 'use.alternate.agent',
            previousValue: 'N',
            newValue: ''
          },
          {
            name: 'alternate.principal',
            previousValue: '0020',
            newValue: ''
          },
          {
            name: 'alternate.agent',
            previousValue: '0022',
            newValue: ''
          }
        ]
      }
    } as any;
    const { getByText, getAllByText } = await renderComponent(props);
    expect(getByText('txt_parameter_list')).toBeInTheDocument();
    expect(getByText('SimpleSearch')).toBeInTheDocument();
    expect(getAllByText('EnhanceDropdownList')).toHaveLength(4);

    act(() => {
      fireEvent.click(getByText('txt_save'));
    });

    expect(handleUpdateExpandMock).toBeCalled();
    expect(onCloseMock).toBeCalled();
    expect(resetDefaultFilterMock).toBeCalled();
  });

  it('should render component edit Status New', async () => {
    const props = {
      isCreate: false,
      SPAs: undefined,
      infoSpa: {
        status: STATUS_SPA.New,
        id: 1291936407,
        actionCode: '',
        parameters: [
          {
            name: 'record.type',
            previousValue: 'B',
            newValue: ''
          },
          {
            name: 'generation.method',
            previousValue: 'R',
            newValue: ''
          },
          {
            name: 'range.from',
            previousValue: '1234',
            newValue: ''
          },
          {
            name: 'range.to',
            previousValue: '3456',
            newValue: ''
          },
          {
            name: 'serial.number',
            previousValue: '2345',
            newValue: ''
          },
          {
            name: 'warning.level',
            previousValue: '91',
            newValue: ''
          },
          {
            name: 'prefix',
            previousValue: 'duo duis',
            newValue: ''
          },
          {
            name: 'alternate.prefix',
            previousValue: '81313',
            newValue: ''
          },
          {
            name: 'account.id.or.piid',
            previousValue: 'PI',
            newValue: ''
          },
          {
            name: 'next.from',
            previousValue: '1234',
            newValue: ''
          },
          {
            name: 'next.to',
            previousValue: '2345',
            newValue: ''
          },
          {
            name: 'available.record.number',
            previousValue: '2047769768',
            newValue: ''
          },
          {
            name: 'use.alternate.agent',
            previousValue: 'N',
            newValue: ''
          },
          {
            name: 'alternate.principal',
            previousValue: '0020',
            newValue: ''
          },
          {
            name: 'alternate.agent',
            previousValue: '0022',
            newValue: ''
          }
        ]
      }
    } as any;
    const { getByText, getAllByText } = await renderComponent(props);
    expect(getByText('txt_parameter_list')).toBeInTheDocument();
    expect(getByText('SimpleSearch')).toBeInTheDocument();
    expect(getAllByText('EnhanceDropdownList')).toHaveLength(3);

    act(() => {
      fireEvent.click(getByText('txt_save'));
    });

    expect(handleUpdateExpandMock).toBeCalled();
    expect(onCloseMock).toBeCalled();
    expect(resetDefaultFilterMock).toBeCalled();
  });
});
