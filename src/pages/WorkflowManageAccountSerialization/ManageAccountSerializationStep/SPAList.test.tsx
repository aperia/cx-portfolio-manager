import { act } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { WorkflowMetadataList } from 'app/fixtures/workflow-metadata';
import { renderWithMockStore } from 'app/utils';
import React from 'react';
import * as ReactRedux from 'react-redux';
import SPAList from './SPAList';
const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});
const mockOnCloseWithoutRecord = jest.fn();
jest.mock('./AddNewRecordModal', () => ({
  __esModule: true,
  default: ({ onClose, handleUpdateExpand, resetDefaultFilter }: any) => {
    const handleOnClose = () => {
      if (mockOnCloseWithoutRecord()) {
        onClose();
        return;
      }
      onClose(
        {
          status: 'Not Updated',
          id: 601254652,
          actionCode: '',
          parameters: [
            { name: 'record.type', previousValue: 'A', newValue: '' }
          ]
        },
        true
      );
    };
    return (
      <div>
        <div onClick={handleOnClose}>AddNewRecordModal_component</div>
        <div onClick={handleUpdateExpand}>
          AddNewRecordModal_handleUpdateExpand
        </div>
        <div onClick={resetDefaultFilter}>
          AddNewRecordModal_resetDefaultFilter
        </div>
      </div>
    );
  }
}));
jest.mock('./AddSPAModal', () => ({
  __esModule: true,
  default: ({ onClose, handleAdd }: any) => {
    const handleOnClose = () => {
      if (mockOnCloseWithoutRecord()) {
        onClose();
        return;
      }
      onClose(
        {
          id: '1',
          name: 'Record-1',
          versions: [
            {
              id: 'version 1.0'
            }
          ]
        },
        true
      );
    };
    const onHandleAdd = () => {
      handleAdd({});
    };
    return (
      <div>
        <div onClick={handleOnClose}>AddSPAModal_component_close</div>;
        <div onClick={onHandleAdd}>AddSPAModal_component_add</div>;
      </div>
    );
  }
}));
const getInitialState = ({ elements = WorkflowMetadataList }: any = {}) => ({
  workflowSetup: { elementMetadata: { elements } }
});
// const mockHandleHideSPA = jest.fn();
const setFormValues = jest.fn();
const mockUseDispatch = jest.spyOn(ReactRedux, 'useDispatch');
const handleShow = jest.fn();
describe('SPAList', () => {
  beforeEach(() => {
    mockUseDispatch.mockReturnValue(jest.fn());
    jest.useFakeTimers();
  });
  afterEach(() => {
    jest.clearAllMocks();
    jest.runAllTimers();
  });
  it('render with data', async () => {
    const props = {
      SPAs: { sys: ['0001', '0002', '0003'], prin: '0002', agent: '0003' },
      formValues: {
        sysPrinAgents: [
          {
            sysPrinAgent: '0005-0006-0008',
            recordTypes: [
              {
                status: 'Not Updated',
                id: 601254652,
                actionCode: '',
                parameters: [
                  { name: 'record.type', previousValue: 'A', newValue: '' }
                ]
              }
            ]
          }
        ]
      },
      handleShow,
      setFormValues
    } as any;
    const wrapper = await renderWithMockStore(<SPAList {...props} />, {
      initialState: getInitialState()
    });
    expect(
      wrapper.getByText('txt_hide_spa_sys_prin_agent_add')
    ).toBeInTheDocument();
    const button = wrapper.getByText('txt_add_record_type');
    userEvent.click(button);
    expect(button).toBeInTheDocument();
    userEvent.click(wrapper.getByText('AddNewRecordModal_resetDefaultFilter'));
  });
  it('render with  data', async () => {
    const props = {
      SPAs: { sys: ['0001', '0002', '0003'], prin: '0002', agent: '0003' },
      formValues: {
        sysPrinAgents: [
          {
            sysPrinAgent: '0005-0006-0008',
            recordTypes: [
              {
                status: 'Not Updated',
                id: 601254652,
                actionCode: '',
                parameters: [
                  { name: 'record.type', previousValue: 'A', newValue: '' }
                ]
              }
            ]
          }
        ]
      },
      handleShow,
      setFormValues
    } as any;
    const wrapper = await renderWithMockStore(<SPAList {...props} />, {
      initialState: getInitialState()
    });
    expect(
      wrapper.getByText('txt_hide_spa_sys_prin_agent_add')
    ).toBeInTheDocument();
  });
  it('render with trigger open modal spa', async () => {
    const props = {
      SPAs: { sys: ['0001', '0002', '0003'], prin: '0002', agent: '0003' },
      formValues: {
        sysPrinAgents: [
          {
            sysPrinAgent: '0005-0006-0008',
            recordTypes: [
              {
                status: 'Not Updated',
                id: 601254652,
                actionCode: '',
                parameters: [
                  { name: 'record.type', previousValue: 'A', newValue: '' }
                ]
              }
            ]
          }
        ]
      },
      handleShow,
      setFormValues
    } as any;
    const wrapper = await renderWithMockStore(<SPAList {...props} />, {
      initialState: getInitialState()
    });
    const button = wrapper.getByText('txt_spa_sys_prin_agent_add');
    act(() => {
      userEvent.click(button);
    });
    expect(
      wrapper.getByText('txt_hide_spa_sys_prin_agent_add')
    ).toBeInTheDocument();
  });
  it('render with trigger open modal spa and trigger close spa', async () => {
    const props = {
      SPAs: { sys: ['0001', '0002', '0003'], prin: '0002', agent: '0003' },
      formValues: {
        sysPrinAgents: [
          {
            sysPrinAgent: '0005-0006-0008',
            recordTypes: [
              {
                status: 'Not Updated',
                id: 601254652,
                actionCode: '',
                parameters: [
                  { name: 'record.type', previousValue: 'A', newValue: '' }
                ]
              }
            ]
          }
        ]
      },
      handleShow,
      setFormValues
    } as any;
    const wrapper = await renderWithMockStore(<SPAList {...props} />, {
      initialState: getInitialState()
    });
    const button = wrapper.getByText('txt_spa_sys_prin_agent_add');
    act(() => {
      userEvent.click(button);
    });
    expect(
      wrapper.getByText('txt_hide_spa_sys_prin_agent_add')
    ).toBeInTheDocument();
    const closeBtn = wrapper.getByText('AddSPAModal_component_close');
    act(() => {
      userEvent.click(closeBtn);
    });
  });
  it('render with trigger open modal spa and trigger add spa', async () => {
    const props = {
      SPAs: { sys: ['0001', '0002', '0003'], prin: '0002', agent: '0003' },
      formValues: {
        sysPrinAgents: [
          {
            sysPrinAgent: '0005-0006-0008',
            recordTypes: [
              {
                status: 'Not Updated',
                id: 601254652,
                actionCode: '',
                parameters: [
                  { name: 'record.type', previousValue: 'A', newValue: '' }
                ]
              }
            ]
          }
        ]
      },
      handleShow,
      setFormValues
    } as any;
    const wrapper = await renderWithMockStore(<SPAList {...props} />, {
      initialState: getInitialState()
    });
    const button = wrapper.getByText('txt_spa_sys_prin_agent_add');
    act(() => {
      userEvent.click(button);
    });
    expect(
      wrapper.getByText('txt_hide_spa_sys_prin_agent_add')
    ).toBeInTheDocument();
    const addBtn = wrapper.getByText('AddSPAModal_component_add');
    act(() => {
      userEvent.click(addBtn);
    });
  });
  it('render with trigger hide spa', async () => {
    const props = {
      SPAs: { sys: ['0001', '0002', '0003'], prin: '0002', agent: '0003' },
      formValues: {
        sysPrinAgents: [
          {
            sysPrinAgent: '0005-0006-0008',
            recordTypes: [
              {
                status: 'Not Updated',
                id: 601254652,
                actionCode: '',
                parameters: [
                  { name: 'record.type', previousValue: 'A', newValue: '' }
                ]
              }
            ]
          }
        ]
      },
      handleShow,
      setFormValues
    } as any;
    const wrapper = await renderWithMockStore(<SPAList {...props} />, {
      initialState: getInitialState()
    });
    const button = wrapper.getByText('txt_hide_spa_sys_prin_agent_add');
    act(() => {
      userEvent.click(button);
    });
    expect(
      wrapper.getByText('txt_spa_sys_prin_agent_list')
    ).toBeInTheDocument();
  });
  it('render with search value', async () => {
    const props = {
      SPAs: { sys: ['0001', '0002', '0003'], prin: '0002', agent: '0003' },
      formValues: {
        sysPrinAgents: [
          {
            sysPrinAgent: '0001-0002-0003',
            recordTypes: [
              {
                status: 'Not Updated',
                id: 601254652,
                actionCode: '',
                parameters: [
                  { name: 'record.type', previousValue: 'A', newValue: '' }
                ]
              }
            ]
          }
        ]
      },
      handleShow,
      setFormValues
    } as any;
    const wrapper = await renderWithMockStore(<SPAList {...props} />, {
      initialState: getInitialState()
    });
    const search = wrapper.getByPlaceholderText('txt_type_to_search_for_spa');
    userEvent.type(search, '0001 0002 0003');
    userEvent.click(
      search.parentElement!.querySelector(
        '.icon-simple-search .icon.icon-search'
      )!
    );
    userEvent.type(search, 'notfound');
    userEvent.click(
      search.parentElement!.querySelector(
        '.icon-simple-search .icon.icon-search'
      )!
    );
    expect(
      wrapper.getByText('txt_spa_sys_prin_agent_list')
    ).toBeInTheDocument();
  });
  it('render with no search value', async () => {
    const props = {
      SPAs: { sys: ['0001', '0002', '0003'], prin: '0002', agent: '0003' },
      formValues: {
        sysPrinAgents: [
          {
            sysPrinAgent: '0005-0006-0008',
            recordTypes: [
              {
                status: 'Not Updated',
                id: 601254652,
                actionCode: '',
                parameters: [
                  { name: 'record.type', previousValue: 'A', newValue: '' }
                ]
              }
            ]
          }
        ]
      },
      handleShow,
      setFormValues
    } as any;
    const wrapper = await renderWithMockStore(<SPAList {...props} />, {
      initialState: getInitialState()
    });
    const search = wrapper.getByPlaceholderText('txt_type_to_search_for_spa');
    userEvent.type(search, '');
    userEvent.click(
      search.parentElement!.querySelector(
        '.icon-simple-search .icon.icon-search'
      )!
    );
    userEvent.type(search, '');
    userEvent.click(
      search.parentElement!.querySelector(
        '.icon-simple-search .icon.icon-search'
      )!
    );
    expect(
      wrapper.getByText('txt_spa_sys_prin_agent_list')
    ).toBeInTheDocument();
  });
  it('render with search value and have clear&reset button', async () => {
    const props = {
      SPAs: { sys: ['0001', '0002', '0003'], prin: '0002', agent: '0003' },
      formValues: {
        sysPrinAgents: [
          {
            sysPrinAgent: '0005-0006-0008',
            recordTypes: [
              {
                status: 'Not Updated',
                id: 601254652,
                actionCode: '',
                parameters: [
                  { name: 'record.type', previousValue: 'A', newValue: '' }
                ]
              }
            ]
          }
        ]
      },
      handleShow,
      setFormValues
    } as any;
    const wrapper = await renderWithMockStore(<SPAList {...props} />, {
      initialState: getInitialState()
    });
    const search = wrapper.getByPlaceholderText('txt_type_to_search_for_spa');
    userEvent.type(search, 'a');
    userEvent.click(
      search.parentElement!.querySelector(
        '.icon-simple-search .icon.icon-search'
      )!
    );
    const clearResetBtn = wrapper.getByText('txt_clear_and_reset');
    expect(clearResetBtn).toBeInTheDocument();
    userEvent.click(clearResetBtn);
    expect(
      wrapper.getByText('txt_spa_sys_prin_agent_list')
    ).toBeInTheDocument();
  });

  it('render when selected step is not current step', async () => {
    const props = {
      SPAs: { sys: ['0001', '0002', '0003'], prin: '0002', agent: '0003' },
      formValues: {
        sysPrinAgents: [
          {
            sysPrinAgent: '0005-0006-0008',
            recordTypes: [
              {
                status: 'Not Updated',
                id: 601254652,
                actionCode: '',
                parameters: [
                  { name: 'record.type', previousValue: 'A', newValue: '' }
                ]
              }
            ]
          }
        ]
      },
      handleShow,
      setFormValues,
      selectedStep: 'step1',
      stepId: 'step2'
    } as any;
    const wrapper = await renderWithMockStore(<SPAList {...props} />, {
      initialState: getInitialState()
    });
    expect(
      wrapper.getByText('txt_spa_sys_prin_agent_list')
    ).toBeInTheDocument();
  });
});
