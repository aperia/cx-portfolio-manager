import { render, screen } from '@testing-library/react';
import * as SelectHooks from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageAccountSerialization';
import React from 'react';
import * as ReactRedux from 'react-redux';
import ConfigureParameters from './ConfigureParameters';

jest.mock('./SelectedSPA', () => ({
  __esModule: true,
  default: () => <div>SelectedSPA</div>
}));

const useSelectElementMetadataForSPAMock = jest.spyOn(
  SelectHooks,
  'useSelectElementMetadataForSPA'
);
const useDispatchMock = jest.spyOn(ReactRedux, 'useDispatch');
const dispatchMock = jest.fn();

const renderComponent = (props?: any) =>
  render(<ConfigureParameters {...props} />);

describe('WorkflowManageAccountSerialization > ManageAccountSerializationStep > ConfigureParameters', () => {
  beforeEach(() => {
    useDispatchMock.mockReturnValue(dispatchMock);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Should render component', () => {
    useSelectElementMetadataForSPAMock.mockReturnValue({
      sys: [],
      prin: {},
      agent: {}
    } as any);
    renderComponent({ formValues: {} });
    expect(dispatchMock).toBeCalled();
    expect(screen.getByText('SelectedSPA')).toBeInTheDocument();
  });
});
