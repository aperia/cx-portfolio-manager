import { fireEvent } from '@testing-library/react';
import { renderWithMockStore } from 'app/utils';
import 'app/utils/_mockComponent/mockPaging';
import * as WorkflowSetupSelectHooks from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageAccountSerialization';
import React from 'react';
import SummaryGrid, { SummaryGridProps } from './SummaryGrid';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  const useTranslationMock = {
    t: (value: string) => {
      return value;
    }
  };
  return {
    ...actualModule,
    useTranslation: () => useTranslationMock
  };
});

const metadata = {
  SVGUnitTypes: [
    {
      code: '',
      text: ''
    },
    {
      code: 'A',
      text: 'A - Account identifiers for new separate-entity accounts when the PI ID/Account Equal parameter in the Basic Controls (AO PA BC) section of the Product Control File is set to 1 or 2. Record type O is the default for accounts and presentation instruments unless you define a record type A to generate presentation instruments.'
    },
    {
      code: 'B',
      text: 'B - Presentation instrument identifiers for separate-entity accounts with additional customer name adds entered via batch transmission.'
    },
    {
      code: 'O',
      text: 'O - Account identifiers for new single-entity accounts, or account and customer presentation instrument identifiers for new separate-entity accounts. Record type O is the default for accounts and presentation instruments unless you define a record type A to generate presentation instruments.'
    },
    {
      code: 'P',
      text: 'P - Presentation instrument identifiers for accounts that have the virtual indicator set at the account level. Record type P is used to enable MasterCard Virtual Card presentation instruments.'
    },
    {
      code: 'S',
      text: 'S - Account identifiers for single-entity accounts, or separate-entity security suspense record (SSR) accounts generated as part of a lost/stolen action.'
    },
    {
      code: 'T',
      text: 'T - Presentation instrument identifiers for all types of accounts that are used for adding a new token presentation instrument or a virtual/digital ID using PI type code 016. If you do not establish a serial number range for the value of T, the system defaults to the range established for value S.'
    }
  ],
  spas: [
    {
      code: '0000-0200-3456',
      text: '0000-0200-3456 - system principle agent numbers'
    },
    {
      code: '0000-0200-0001',
      text: '0000-0200-0001 - system principle agent numbers'
    },
    {
      code: '0000-0200-0002',
      text: '0000-0200-0002 - system principle agent numbers'
    },
    {
      code: '0000-0220-3451',
      text: '0000-0220-3451 - system principle agent numbers'
    },
    {
      code: '0000-0220-0002',
      text: '0000-0220-0002 - system principle agent numbers'
    },
    {
      code: '0000-0220-0003',
      text: '0000-0220-0003 - system principle agent numbers'
    },
    {
      code: '0000-0230-3221',
      text: '0000-0230-3221 - system principle agent numbers'
    },
    {
      code: '0000-0230-0008',
      text: '0000-0230-0008 - system principle agent numbers'
    },
    {
      code: '0000-0230-0004',
      text: '0000-0230-0004 - system principle agent numbers'
    },
    {
      code: '0010-0300-0003',
      text: '0010-0300-0003 - system principle agent numbers'
    },
    {
      code: '0010-0300-0004',
      text: '0010-0300-0004 - system principle agent numbers'
    },
    {
      code: '0010-0300-0005',
      text: '0010-0300-0005 - system principle agent numbers'
    },
    {
      code: '0010-0330-0003',
      text: '0010-0330-0003 - system principle agent numbers'
    },
    {
      code: '0010-0330-0004',
      text: '0010-0330-0004 - system principle agent numbers'
    },
    {
      code: '0010-0330-0005',
      text: '0010-0330-0005 - system principle agent numbers'
    },
    {
      code: '0010-0304-0003',
      text: '0010-0304-0003 - system principle agent numbers'
    },
    {
      code: '0010-0304-0004',
      text: '0010-0304-0004 - system principle agent numbers'
    },
    {
      code: '0010-0304-0005',
      text: '0010-0304-0005 - system principle agent numbers'
    },
    {
      code: '0020-0400-0006',
      text: '0020-0400-0006 - system principle agent numbers'
    },
    {
      code: '0020-0400-0007',
      text: '0020-0400-0007 - system principle agent numbers'
    },
    {
      code: '0020-0400-0008',
      text: '0020-0400-0008 - system principle agent numbers'
    },
    {
      code: '0020-1400-0006',
      text: '0020-1400-0006 - system principle agent numbers'
    },
    {
      code: '0020-1400-0007',
      text: '0020-1400-0007 - system principle agent numbers'
    },
    {
      code: '0020-1400-0008',
      text: '0020-1400-0008 - system principle agent numbers'
    },
    {
      code: '0020-2400-0006',
      text: '0020-2400-0006 - system principle agent numbers'
    },
    {
      code: '0020-2400-0007',
      text: '0020-2400-0007 - system principle agent numbers'
    },
    {
      code: '0020-2400-0008',
      text: '0020-2400-0008 - system principle agent numbers'
    }
  ],
  sys: ['0000', '0010', '0020'],
  prin: {
    '0000': ['0200', '0220', '0230'],
    '0010': ['0300', '0330', '0304'],
    '0020': ['0400', '1400', '2400']
  },
  agent: {
    '00000200': ['3456', '0001', '0002'],
    '00000220': ['3451', '0002', '0003'],
    '00000230': ['3221', '0008', '0004'],
    '00100300': ['0003', '0004', '0005'],
    '00100330': ['0003', '0004', '0005'],
    '00100304': ['0003', '0004', '0005'],
    '00200400': ['0006', '0007', '0008'],
    '00201400': ['0006', '0007', '0008'],
    '00202400': ['0006', '0007', '0008']
  },
  generationMethods: [
    {
      code: '',
      text: ''
    },
    {
      code: 'R',
      text: 'R - Random'
    },
    {
      code: 'S',
      text: 'S - Sequential'
    }
  ],
  useAlternateAgents: [
    {
      code: '',
      text: ''
    },
    {
      code: 'N',
      text: 'N - No, do not use an alternate agent.'
    },
    {
      code: 'Y',
      text: 'Y - Yes, use an alternate agent.'
    }
  ],
  accountIDOrPIID: [
    {
      code: '',
      text: ''
    },
    {
      code: 'N',
      text: 'N - No, do not use an alternate agent.'
    },
    {
      code: 'Y',
      text: 'Y - Yes, use an alternate agent.'
    }
  ]
};

const useSelectElementMetadataForSPA = jest.spyOn(
  WorkflowSetupSelectHooks,
  'useSelectElementMetadataForSPA'
);

const getDataGridMock = (i?: number) => {
  const mod = (i || 0) % 3;
  return {
    status:
      mod === 0 ? 'Marked for Deletion' : mod === 1 ? 'Updated' : 'Not Updated',
    id: 2021828824,
    actionCode: 'R',
    parameters: [
      {
        name: 'record.type',
        previousValue: 'B',
        newValue: ''
      },
      {
        name: 'generation.method',
        previousValue: 'G',
        newValue: ''
      },
      {
        name: 'range.from',
        previousValue: '14862',
        newValue: ''
      },
      {
        name: 'range.to',
        previousValue: '67163',
        newValue: ''
      },
      {
        name: 'serial.number',
        previousValue: '44097',
        newValue: ''
      },
      {
        name: 'warning.level',
        previousValue: '95',
        newValue: ''
      },
      {
        name: 'prefix',
        previousValue: '1796',
        newValue: ''
      },
      {
        name: 'alternate.prefix',
        previousValue: '82039',
        newValue: ''
      },
      {
        name: 'account.id.or.piid',
        previousValue: 'AC',
        newValue: ''
      },
      {
        name: 'next.from',
        previousValue: '2302',
        newValue: ''
      },
      {
        name: 'next.to',
        previousValue: '64151',
        newValue: ''
      },
      {
        name: 'available.record.number',
        previousValue: '649181885',
        newValue: ''
      },
      {
        name: 'use.alternate.agent',
        previousValue: 'Y',
        newValue: ''
      },
      {
        name: 'alternate.principal',
        previousValue: '0020',
        newValue: ''
      },
      {
        name: 'alternate.agent',
        previousValue: '0010',
        newValue: ''
      }
    ]
  };
};

const generateDataGridMock = (count: number) => {
  const dataMock: any = [];
  for (let i = 0; i < count; i++) {
    dataMock.push(getDataGridMock(i));
  }
  return dataMock;
};

describe('WorkflowManageAccountSerialization > ManageAccountSerializationStep > SummaryGrid', () => {
  const renderComponent = (props: SummaryGridProps) => {
    return renderWithMockStore(
      <div>
        <SummaryGrid {...props} />
      </div>
    );
  };
  beforeEach(() => {
    useSelectElementMetadataForSPA.mockReturnValue(metadata as any);
  });
  it('Should render component', async () => {
    const props: SummaryGridProps = {
      dataForm: '0000-0200-3456',
      dataGrid: [getDataGridMock()],
      originalData: metadata
    } as any;
    const wrapper = await renderComponent({ ...props });

    // expand grid row
    const expandIcon = wrapper.container.querySelector('tbody .icon.icon-plus');
    fireEvent.click(expandIcon!);

    // collapse section
    const minusSectionIcon =
      wrapper.container.querySelector('.icon.icon-minus');
    fireEvent.click(minusSectionIcon!);
    const expandSectionIcon =
      wrapper.container.querySelector('.icon.icon-plus');
    expect(expandSectionIcon).toBeInTheDocument();
  });

  it('Should render component with more than 10 record types', async () => {
    const props: SummaryGridProps = {
      dataForm: '0000-0200-3456',
      dataGrid: generateDataGridMock(11),
      originalData: metadata
    } as any;
    const wrapper = await renderComponent({ ...props });

    // change page
    fireEvent.click(wrapper.getByText('Page 2'));

    // change page size
    fireEvent.click(wrapper.getByText('Page Size 25'));
  });
});
