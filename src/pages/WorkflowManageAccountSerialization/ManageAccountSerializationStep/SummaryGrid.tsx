import { DEFAULT_FILTER_VALUE } from 'app/constants/constants';
import { RecordFieldParameterEnum } from 'app/constants/enums';
import { mapGridExpandCollapse } from 'app/helpers';
import {
  Badge,
  Button,
  ColumnType,
  Grid,
  Icon,
  Tooltip,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import { orderBy } from 'app/_libraries/_dls/lodash';
import { isEmpty } from 'lodash';
import Paging from 'pages/_commons/Utils/Paging';
import React, { useCallback, useEffect, useState } from 'react';
import { ManageAccountSerializationFormValue } from '.';
import { mapColorBadge, STATUS_SPA } from './helper';
import { SubGrid } from './SubGrid';

export interface SummaryGridProps
  extends WorkflowSetupSummaryProps<ManageAccountSerializationFormValue> {
  dataForm: string;
  dataGrid: MagicKeyValue[];
  originalData: MagicKeyValue;
}

const SummaryGrid: React.FC<SummaryGridProps> = ({
  dataForm,
  dataGrid,
  originalData,
  stepId,
  selectedStep
}) => {
  const { t } = useTranslation();

  const [columns, setColumns] = useState<ColumnType[]>([]);
  const [expandedList, setExpandedList] = useState<string[]>([]);
  const [isExpand, setIsExpand] = useState<boolean>(true);
  const [currentPageSize, setCurrentPageSize] = useState(
    DEFAULT_FILTER_VALUE.pageSize as number
  );
  const [currentPage, setCurrentPage] = useState(1);

  const handleExpand = (dataKeyList: string[]) => {
    setExpandedList(dataKeyList);
  };

  const renderSubRow = (row: any) => (
    <SubGrid row={row} originalData={originalData} />
  );

  const onPageChange = useCallback((pageSelected: number) => {
    setCurrentPage(pageSelected);
  }, []);

  const onPageSizeChange = useCallback((size: number) => {
    setCurrentPage(1);
    setCurrentPageSize(size);
  }, []);

  useEffect(() => {
    setColumns([
      {
        id: RecordFieldParameterEnum.RecordType,
        Header: t('txt_spa_record_type'),
        accessor: (row: MagicKeyValue) => {
          const val =
            row.status === STATUS_SPA.MarkedForDeletion
              ? row?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.RecordType
                )?.newValue ||
                row?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.RecordType
                )?.previousValue
              : row.status !== STATUS_SPA.NotUpdated
              ? row?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.RecordType
                )?.newValue
              : row?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.RecordType
                )?.previousValue;
          const valueTruncate: string = !isEmpty(val?.trim()) ? val : '';
          return (
            <TruncateText
              resizable
              lines={2}
              ellipsisLessText={t('txt_less')}
              ellipsisMoreText={t('txt_more')}
            >
              {valueTruncate}
            </TruncateText>
          );
        }
      },
      {
        id: RecordFieldParameterEnum.GenerationMethod,
        Header: t('txt_spa_generation_method'),
        accessor: (row: MagicKeyValue) => {
          const val =
            row.status === STATUS_SPA.MarkedForDeletion
              ? row?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.GenerationMethod
                )?.newValue ||
                row?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.GenerationMethod
                )?.previousValue
              : row.status !== STATUS_SPA.NotUpdated
              ? row?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.GenerationMethod
                )?.newValue
              : row?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.GenerationMethod
                )?.previousValue;
          const valueTruncate = originalData?.generationMethods?.find(
            (o: RefData) => o.code === val
          )?.text;
          return (
            <TruncateText
              resizable
              lines={2}
              ellipsisLessText={t('txt_less')}
              ellipsisMoreText={t('txt_more')}
            >
              {valueTruncate}
            </TruncateText>
          );
        }
      },
      {
        id: RecordFieldParameterEnum.RangeFrom,
        Header: t('txt_spa_range_from'),
        accessor: (row: MagicKeyValue) => {
          const val =
            row.status === STATUS_SPA.MarkedForDeletion
              ? row?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.RangeFrom
                )?.newValue ||
                row?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.RangeFrom
                )?.previousValue
              : row.status !== STATUS_SPA.NotUpdated
              ? row?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.RangeFrom
                )?.newValue
              : row?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.RangeFrom
                )?.previousValue;
          return (
            <TruncateText
              resizable
              lines={2}
              ellipsisLessText={t('txt_less')}
              ellipsisMoreText={t('txt_more')}
            >
              {val}
            </TruncateText>
          );
        }
      },
      {
        id: RecordFieldParameterEnum.RangeTo,
        Header: t('txt_spa_range_to'),
        accessor: (row: MagicKeyValue) => {
          const val =
            row.status === STATUS_SPA.MarkedForDeletion
              ? row?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.RangeTo
                )?.newValue ||
                row?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.RangeTo
                )?.previousValue
              : row.status !== STATUS_SPA.NotUpdated
              ? row?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.RangeTo
                )?.newValue
              : row?.parameters?.find(
                  (o: MagicKeyValue) =>
                    o.name === RecordFieldParameterEnum.RangeTo
                )?.previousValue;
          return (
            <TruncateText
              resizable
              lines={2}
              ellipsisLessText={t('txt_less')}
              ellipsisMoreText={t('txt_more')}
            >
              {val}
            </TruncateText>
          );
        }
      },
      {
        id: RecordFieldParameterEnum.Status,
        Header: t('txt_spa_status'),
        accessor: (row: MagicKeyValue) => {
          return (
            <Badge textTransformNone color={mapColorBadge(row?.status)}>
              {row?.status}
            </Badge>
          );
        }
      }
    ]);
  }, [t, originalData?.generationMethods]);

  const defaultSort = orderBy(
    dataGrid,
    [
      (o: MagicKeyValue) => {
        const param = o?.parameters as MagicKeyValue[];
        const val =
          o.status === STATUS_SPA.MarkedForDeletion
            ? param?.find(
                (o: MagicKeyValue) =>
                  o.name === RecordFieldParameterEnum.RangeFrom
              )?.newValue ||
              param?.find(
                (o: MagicKeyValue) =>
                  o.name === RecordFieldParameterEnum.RangeFrom
              )?.previousValue
            : o.status !== STATUS_SPA.NotUpdated
            ? param?.find(
                (o: MagicKeyValue) =>
                  o.name === RecordFieldParameterEnum.RangeFrom
              )?.newValue
            : param?.find(
                (o: MagicKeyValue) =>
                  o.name === RecordFieldParameterEnum.RangeFrom
              )?.previousValue;
        return parseFloat(val);
      }
    ],
    ['asc']
  );

  useEffect(() => {
    if (selectedStep === stepId) {
      setExpandedList([]);
      setIsExpand(true);
    }
  }, [selectedStep, stepId]);

  const data = defaultSort?.slice(
    currentPageSize * (currentPage - 1),
    currentPage * currentPageSize
  );

  return (
    <>
      <div className="d-flex align-items-center pb-16">
        <Tooltip
          element={isExpand ? t('txt_collapse') : t('txt_expand')}
          variant="primary"
          placement="top"
          triggerClassName="d-flex ml-n2"
        >
          <Button
            size="sm"
            variant="icon-secondary"
            onClick={() => setIsExpand(!isExpand)}
          >
            <Icon name={isExpand ? 'minus' : 'plus'} size="4x" />
          </Button>
        </Tooltip>
        <p className="fw-600 ml-8">{`${dataForm.split('-')[0]} ${
          dataForm.split('-')[1]
        } ${dataForm.split('-')[2]}`}</p>
      </div>
      {isExpand && (
        <>
          <Grid
            className="grid-has-tooltip center"
            togglable
            data={data}
            columns={columns}
            dataItemKey="id"
            toggleButtonConfigList={data.map(mapGridExpandCollapse('id', t))}
            expandedItemKey="id"
            expandedList={expandedList}
            onExpand={handleExpand}
            subRow={renderSubRow}
          />
          {dataGrid?.length > 10 && (
            <div className="mt-16">
              <Paging
                page={currentPage}
                pageSize={currentPageSize}
                totalItem={dataGrid?.length}
                onChangePage={onPageChange}
                onChangePageSize={onPageSizeChange}
              />
            </div>
          )}
        </>
      )}
    </>
  );
};

export default SummaryGrid;
