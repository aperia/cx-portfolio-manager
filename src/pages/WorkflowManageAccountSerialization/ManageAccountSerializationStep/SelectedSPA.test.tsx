import { fireEvent, render, screen } from '@testing-library/react';
import React from 'react';
import SelectedSPA from './SelectedSPA';

// mock area
jest.mock('./SPAForm', () => {
  return {
    __esModule: true,
    default: ({ handleContinue }: any) => (
      <div onClick={() => handleContinue({})}>SPA</div>
    )
  };
});

jest.mock('./SPAList', () => ({
  __esModule: true,
  default: ({ handleShow }: any) => (
    <div onClick={() => handleShow()}>SPAList</div>
  )
}));

const renderComponent = (props?: any) => render(<SelectedSPA {...props} />);

describe('WorkflowManageAccountSerialization > ManageAccountSerializationStep > SelectedSPA', () => {
  it('Should render component', () => {
    const setFormValues = jest.fn();

    renderComponent({ setFormValues });
    expect(screen.getByText('SPA')).toBeInTheDocument();

    fireEvent.click(screen.getByText('SPA'));
    expect(setFormValues).toHaveBeenCalled();
  });

  it('Should render component with item sysPrinAgents', () => {
    renderComponent({ formValues: { sysPrinAgents: [{}] } });
    expect(screen.getByText('SPAList')).toBeInTheDocument();
  });
});
