import EnhanceDropdownList from 'app/components/EnhanceDropdownList';
import ModalRegistry from 'app/components/ModalRegistry';
import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import {
  RecordFieldNameEnum,
  RecordFieldParameterEnum
} from 'app/constants/enums';
import {
  RECORD_MANAGE_ACCOUNT_SERIALIZATION_DEFAULT,
  RECORD_MANAGE_ACCOUNT_SERIALIZATION_FIELDS
} from 'app/constants/mapping';
import {
  unsavedChangesProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import {
  matchSearchValue,
  objectToRecordParams,
  recordParamsToObject,
  stringValidate
} from 'app/helpers';
import { useUnsavedChangeRegistry, useUnsavedChangesRedirect } from 'app/hooks';
import {
  ColumnType,
  ComboBox,
  Grid,
  InlineMessage,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  NumericTextBox,
  TextBox,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty, orderBy } from 'app/_libraries/_dls/lodash';
import isEqual from 'lodash.isequal';
import isFunction from 'lodash.isfunction';
import { useSelectElementMetadataForSPA } from 'pages/_commons/redux/WorkflowSetup';
import ClearAndResetButton from 'pages/_commons/Utils/ClearAndResetButton';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React, { useEffect, useMemo, useRef, useState } from 'react';
import {
  formatSysPrinAgentObjectArr,
  parseDefaultRecordType,
  STATUS_SPA
} from './helper';
import { recordParameterMAS } from './newRecordParameterList';

export interface IProps {
  id: string;
  show: boolean;
  isCreate?: boolean;
  record?: WorkflowSetupRecord;
  draftRecord?: WorkflowSetupRecord;
  title: string;
  infoSpa?: MagicKeyValue;
  dataForm: { sys: string; prin: string; agent: string };
  SPAs: { sys: string[]; prin: MagicKeyValue; agent: MagicKeyValue };
  originalData: MagicKeyValue;
  onClose?: (record?: WorkflowSetupRecord, isBack?: boolean) => void;
  handleUpdateExpand: (id: string) => void;
  resetDefaultFilter: () => void;
}

export interface IFieldValidate {
  rangeFrom: string;
  rangeTo: string;
  serialNumber: string;
  nextFrom: string;
  nextTo: string;
}

const AddNewRecordModal: React.FC<IProps> = ({
  id,
  show,
  title,
  isCreate = true,
  record: recordProp,
  draftRecord,
  dataForm,
  SPAs,
  infoSpa,
  originalData,
  handleUpdateExpand,
  onClose,
  resetDefaultFilter
}) => {
  const [prinList] = useState(
    SPAs ? formatSysPrinAgentObjectArr(SPAs.prin[dataForm.sys]) : []
  );
  const [agentList, setAgentList] = useState([]);

  const [selectedGeneratedMethod, setSelectedGeneratedMethod] = useState([]);
  const { accountIDOrPIID } = useSelectElementMetadataForSPA();

  const { t } = useTranslation();

  const isNewStatus = infoSpa?.status === STATUS_SPA.New;

  const isUpdated = infoSpa?.status === STATUS_SPA.Updated;

  const redirect = useUnsavedChangesRedirect();

  const [initialRecord, setInitialRecord] = useState<
    WorkflowSetupRecordObjectParams & { rowId?: number }
  >(recordParamsToObject(recordProp as WorkflowSetupRecord));

  const [record, setRecord] = useState(
    draftRecord ? recordParamsToObject(draftRecord) : initialRecord
  );

  const [errors, setErrors] = useState<MagicKeyValue>();

  const [searchValue, setSearchValue] = useState('');

  const parameters = useMemo(
    () =>
      recordParameterMAS.filter(g =>
        matchSearchValue(
          [g.fieldName, g.moreInfoText, g.greenScreenName],
          searchValue
        )
      ),
    [searchValue]
  );

  const newParameters = useMemo(
    () =>
      recordParameterMAS.filter(
        g =>
          ![
            RecordFieldParameterEnum.AccountIDOrPIID,
            RecordFieldParameterEnum.AvailableRecordNumber
          ].includes(g.id) &&
          matchSearchValue(
            [g.fieldName, g.moreInfoText, g.greenScreenName],
            searchValue
          )
      ),
    [searchValue]
  );

  useUnsavedChangeRegistry(
    {
      ...unsavedChangesProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_ACCOUNT_SERIALIZATION_ADD_RECORD_TYPE,
      priority: 1
    },
    [!isEqual(initialRecord, record)]
  );

  useEffect(() => {
    let defaultParamObj: Record<string, string> = {};

    const paramMapping = Object.keys(
      RECORD_MANAGE_ACCOUNT_SERIALIZATION_DEFAULT
    ).reduce((pre, next) => {
      pre[next] = RECORD_MANAGE_ACCOUNT_SERIALIZATION_DEFAULT[next];
      return pre;
    }, {} as Record<string, string>);
    if (infoSpa) {
      defaultParamObj = parseDefaultRecordType(infoSpa, paramMapping);

      setRecord(defaultParamObj as any);
      setInitialRecord(defaultParamObj as any);
    } else {
      defaultParamObj = Object.assign({}, paramMapping);
      defaultParamObj.prefix = originalData?.defaultPrefix;
      defaultParamObj.status = STATUS_SPA.New;
      setRecord(defaultParamObj as any);
      setInitialRecord(defaultParamObj as any);
    }
  }, [infoSpa, originalData?.defaultPrefix]);

  useEffect(() => {
    if (record?.[RecordFieldParameterEnum.AlternatePrincipal]) {
      setAgentList(
        formatSysPrinAgentObjectArr(
          SPAs.agent[
            `${dataForm['sys']}${
              record?.[RecordFieldParameterEnum.AlternatePrincipal]
            }`
          ]
        )
      );
    }
  }, [record, SPAs, dataForm]);

  useEffect(() => {
    if (originalData && infoSpa && !isNewStatus) {
      const selectedGeneratedMethodList = originalData?.recordTypes?.find(
        (item: MagicKeyValue) => item.id === infoSpa.id
      );

      setSelectedGeneratedMethod(
        selectedGeneratedMethodList?.generationMethods
      );
    }
  }, [originalData, infoSpa, selectedGeneratedMethod, isNewStatus]);

  const dynamicParameter = isCreate || isNewStatus ? newParameters : parameters;

  const dynamicGenerationMethods = useMemo(() => {
    if (isNewStatus || isCreate) {
      return originalData?.generationMethods;
    }
    return selectedGeneratedMethod.map((item: MagicKeyValue) => ({
      code: item.value,
      text: `${item.value} - ${item.description}`
    }));
  }, [
    isCreate,
    isNewStatus,
    originalData?.generationMethods,
    selectedGeneratedMethod
  ]);

  const dynamicPrefix = !isCreate
    ? record?.[RecordFieldParameterEnum.Prefix]
    : originalData?.defaultPrefix;

  const simpleSearchRef = useRef<any>(null);

  useEffect(() => {
    if (!searchValue && simpleSearchRef?.current) {
      simpleSearchRef.current.clear();
    }
  }, [searchValue]);

  const handleClose = () => {
    redirect({
      onConfirm: () => isFunction(onClose) && onClose(),
      formsWatcher: [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_ACCOUNT_SERIALIZATION_ADD_RECORD_TYPE
      ]
    });
  };
  const checkError = (record: MagicKeyValue) => {
    let currentErrors = {};

    const rangeFrom = record[RecordFieldParameterEnum.RangeFrom];
    const rangeTo = record[RecordFieldParameterEnum.RangeTo];
    const serialNumber = record[RecordFieldParameterEnum.SerialNumber];
    const nextFrom = record[RecordFieldParameterEnum.NextFrom];
    const nextTo = record[RecordFieldParameterEnum.NextTo];

    if (rangeFrom && rangeTo && isEmpty(serialNumber)) {
      currentErrors = {
        ...currentErrors,
        rangeFrom: Number(rangeFrom) >= Number(rangeTo) && {
          status: true,
          message: t('txt_manage_account_serialization_error_1')
        }
      };
    }
    if (rangeFrom && rangeTo && serialNumber) {
      const numRangeFrom = Number(rangeFrom);
      const numRangeTo = Number(rangeTo);

      const numSerialNumber = Number(serialNumber);

      const isSerialNumberInRange =
        // range to < range from
        (numSerialNumber >= numRangeTo && numSerialNumber <= numRangeFrom) ||
        // range to > range from
        (numSerialNumber >= numRangeFrom && numSerialNumber <= numRangeTo);

      currentErrors = {
        ...currentErrors,
        rangeFrom: numRangeFrom >= numRangeTo && {
          status: true,
          message: t('txt_manage_account_serialization_error_1')
        },
        serialNumber: !isSerialNumberInRange && {
          status: true,
          message: t('txt_manage_account_serialization_error_3')
        }
      };
    }
    if (nextFrom && nextTo) {
      currentErrors = {
        ...currentErrors,
        nextFrom: Number(nextFrom) >= Number(nextTo) && {
          status: true,
          message: t('txt_manage_account_serialization_error_4')
        }
      };
    }

    return currentErrors;
  };

  const handleAddRecord = () => {
    const errors = checkError(record);

    setErrors(errors);

    if (
      !isEmpty(errors) &&
      Object.values(errors).some(item => typeof item === 'object')
    )
      return;

    const recordForm = objectToRecordParams(
      [...RECORD_MANAGE_ACCOUNT_SERIALIZATION_FIELDS],
      record,
      initialRecord
    );
    recordForm.status = isNewStatus
      ? STATUS_SPA.New
      : isUpdated
      ? STATUS_SPA.Updated
      : STATUS_SPA.NotUpdated;
    recordForm.actionCode = isUpdated ? 'C' : '';
    recordForm.id = infoSpa?.id || '';
    if (
      infoSpa?.status === STATUS_SPA.NotUpdated &&
      !isEqual(record, initialRecord)
    ) {
      recordForm.status = STATUS_SPA.Updated;
    }

    if (isCreate) {
      recordForm.status = STATUS_SPA.New;
      recordForm.id = `${Date.now()}`;
    }
    handleUpdateExpand(recordForm.id);
    resetDefaultFilter();
    isFunction(onClose) && onClose(recordForm);
  };

  const handleFormChange =
    (
      field: keyof WorkflowSetupRecordObjectParams & { optionalTiers?: string },
      isRefData?: boolean
    ) =>
    (e: any) => {
      const value = isRefData ? e.target?.value?.code : e.target?.value;

      if (
        [
          RecordFieldParameterEnum.NextFrom,
          RecordFieldParameterEnum.NextTo,
          RecordFieldParameterEnum.RangeFrom,
          RecordFieldParameterEnum.RangeTo,
          RecordFieldParameterEnum.SerialNumber,
          RecordFieldParameterEnum.AlternatePrefix
        ].includes(field as any) &&
        !stringValidate(value).isNumber()
      ) {
        return false;
      }

      if (field === RecordFieldParameterEnum.AlternatePrincipal) {
        setRecord(records => ({
          ...records,
          [RecordFieldParameterEnum.AlternateAgent]: ''
        }));
      }

      setRecord(records => ({ ...records, [field]: value }));
    };

  const handleSearch = (val: string) => {
    setSearchValue(val);
  };

  const handleClearFilter = () => {
    setSearchValue('');
  };

  const valueAccessor = (data: Record<string, any>) => {
    const paramId = data.id as RecordFieldParameterEnum;

    switch (paramId) {
      case RecordFieldParameterEnum.RecordType: {
        const value = originalData?.types?.find(
          (o: RefData) =>
            o.code === record?.[RecordFieldParameterEnum.RecordType]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewRecord__recordType"
            size="small"
            value={value}
            disabled={!isCreate && !isNewStatus}
            onChange={handleFormChange(
              RecordFieldParameterEnum.RecordType,
              true
            )}
            options={originalData?.types}
          />
        );
      }
      case RecordFieldParameterEnum.GenerationMethod: {
        const value = dynamicGenerationMethods?.find(
          (o: RefData) =>
            o.code === record?.[RecordFieldParameterEnum.GenerationMethod]
        );

        return (
          <EnhanceDropdownList
            placeholder={RecordFieldNameEnum.GenerationMethod}
            dataTestId="addNewRecord__generationMethod"
            size="small"
            value={
              (dynamicGenerationMethods?.length === 1 &&
                dynamicGenerationMethods[0]) ||
              value
            }
            onChange={handleFormChange(
              RecordFieldParameterEnum.GenerationMethod,
              true
            )}
            disabled={dynamicGenerationMethods?.length === 1}
            options={dynamicGenerationMethods}
          />
        );
      }

      case RecordFieldParameterEnum.RangeFrom: {
        return (
          <TextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewRecord__rangeFrom"
            size="sm"
            maxLength={13}
            autoFocus={false}
            value={record?.[RecordFieldParameterEnum.RangeFrom] || ''}
            onChange={handleFormChange(RecordFieldParameterEnum.RangeFrom)}
            error={errors?.rangeFrom}
          />
        );
      }
      case RecordFieldParameterEnum.RangeTo: {
        return (
          <TextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewRecord__rangeTo"
            size="sm"
            maxLength={13}
            autoFocus={false}
            value={record?.[RecordFieldParameterEnum.RangeTo] || ''}
            onChange={handleFormChange(RecordFieldParameterEnum.RangeTo)}
            error={errors?.rangeFrom}
          />
        );
      }

      case RecordFieldParameterEnum.SerialNumber: {
        return (
          <TextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewRecord__serialNumber"
            size="sm"
            maxLength={13}
            autoFocus={false}
            value={record?.[RecordFieldParameterEnum.SerialNumber] || ''}
            onChange={handleFormChange(RecordFieldParameterEnum.SerialNumber)}
            error={errors?.serialNumber}
          />
        );
      }

      case RecordFieldParameterEnum.WarningLevel: {
        return (
          <NumericTextBox
            dataTestId="addNewRecord__warningLevel"
            size="sm"
            format="p0"
            showStep={false}
            maxLength={2}
            autoFocus={false}
            value={record?.[RecordFieldParameterEnum.WarningLevel] || ''}
            onChange={handleFormChange(RecordFieldParameterEnum.WarningLevel)}
          />
        );
      }
      case RecordFieldParameterEnum.Prefix: {
        return (
          <TextBox
            placeholder={RecordFieldNameEnum.Prefix}
            dataTestId="addNewRecord__prefix"
            size="sm"
            disabled={true}
            maxLength={13}
            autoFocus={false}
            value={dynamicPrefix}
            onChange={handleFormChange(RecordFieldParameterEnum.Prefix)}
          />
        );
      }

      case RecordFieldParameterEnum.AlternatePrefix: {
        return (
          <TextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewRecord__alternatePrefix"
            size="sm"
            maxLength={8}
            autoFocus={false}
            value={record?.[RecordFieldParameterEnum.AlternatePrefix] || ''}
            onChange={handleFormChange(
              RecordFieldParameterEnum.AlternatePrefix
            )}
          />
        );
      }

      case RecordFieldParameterEnum.AccountIDOrPIID: {
        const value = accountIDOrPIID?.find(
          o => o.code === record?.[RecordFieldParameterEnum.AccountIDOrPIID]
        );

        return (
          <EnhanceDropdownList
            placeholder={RecordFieldNameEnum.AccountIDOrPIID}
            dataTestId="addNewRecord__accountIDOrPIID"
            size="small"
            value={value}
            disabled={!isCreate}
            onChange={handleFormChange(
              RecordFieldParameterEnum.AccountIDOrPIID,
              true
            )}
            options={accountIDOrPIID}
          />
        );
      }

      case RecordFieldParameterEnum.NextFrom: {
        return (
          <TextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewRecord__nextFrom"
            size="sm"
            maxLength={13}
            autoFocus={false}
            value={record?.[RecordFieldParameterEnum.NextFrom] || ''}
            onChange={handleFormChange(RecordFieldParameterEnum.NextFrom)}
            error={errors?.nextFrom}
          />
        );
      }

      case RecordFieldParameterEnum.NextTo: {
        return (
          <TextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewRecord__nextTo"
            size="sm"
            maxLength={13}
            autoFocus={false}
            value={record?.[RecordFieldParameterEnum.NextTo] || ''}
            onChange={handleFormChange(RecordFieldParameterEnum.NextTo)}
            error={errors?.nextFrom}
          />
        );
      }

      case RecordFieldParameterEnum.AvailableRecordNumber: {
        return (
          <TextBox
            placeholder={RecordFieldNameEnum.AvailableRecordNumber}
            dataTestId="addNewRecord__availableRecordNumber"
            size="sm"
            maxLength={13}
            disabled={true}
            autoFocus={false}
            value={
              record?.[RecordFieldParameterEnum.AvailableRecordNumber] || ''
            }
            onChange={handleFormChange(
              RecordFieldParameterEnum.AvailableRecordNumber
            )}
          />
        );
      }

      case RecordFieldParameterEnum.UseAlternateAgent: {
        const value = originalData?.useAlternateAgents?.find(
          (o: RefData) =>
            o.code === record?.[RecordFieldParameterEnum.UseAlternateAgent]
        );

        return (
          <EnhanceDropdownList
            placeholder={RecordFieldNameEnum.UseAlternateAgent}
            dataTestId="addNewRecord__useAlternateAgent"
            size="small"
            value={value}
            onChange={handleFormChange(
              RecordFieldParameterEnum.UseAlternateAgent,
              true
            )}
            options={originalData?.useAlternateAgents}
          />
        );
      }

      case RecordFieldParameterEnum.AlternatePrincipal: {
        const value = prinList?.find(
          (o: any) =>
            o.code === record?.[RecordFieldParameterEnum.AlternatePrincipal]
        );
        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            size="small"
            dataTestId="addNewRecord__alternatePrincipal"
            textField="text"
            value={
              isEmpty(value)
                ? record?.[RecordFieldParameterEnum.AlternatePrincipal]
                : value
            }
            onChange={handleFormChange(
              RecordFieldParameterEnum.AlternatePrincipal,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {prinList.map((item: MagicKeyValue) => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }

      case RecordFieldParameterEnum.AlternateAgent: {
        const value = agentList?.find(
          (o: any) =>
            o.code === record?.[RecordFieldParameterEnum.AlternateAgent]
        );
        return (
          <ComboBox
            dataTestId="addNewRecord__alternateAgent"
            size="small"
            placeholder={t('txt_select_an_option')}
            textField="text"
            value={
              isEmpty(value)
                ? record?.[RecordFieldParameterEnum.AlternateAgent]
                : value
            }
            onChange={handleFormChange(
              RecordFieldParameterEnum.AlternateAgent,
              true
            )}
            disabled={isEmpty(
              record?.[RecordFieldParameterEnum.AlternatePrincipal]
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {orderBy(agentList, ['code'], ['asc']).map(
              (item: MagicKeyValue) => (
                <ComboBox.Item key={item.code} value={item} label={item.text} />
              )
            )}
          </ComboBox>
        );
      }
    }
  };

  const columns: ColumnType[] = [
    {
      id: 'fieldName',
      Header: t('txt_business_name'),
      accessor: 'fieldName'
    },
    {
      id: 'greenScreenName',
      Header: t('txt_green_screen_name'),
      accessor: 'greenScreenName'
    },
    {
      id: 'value',
      Header: t('txt_value'),
      accessor: valueAccessor,
      cellBodyProps: { className: 'control' }
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      accessor: viewMoreInfo(['moreInfo'], t),
      width: 105
    }
  ];

  const listErrorMsg = Object.entries(errors || {})
    .map((el: any) => el[1])
    .filter(item => typeof item === 'object');

  return (
    <ModalRegistry lg id={id} show={show} onAutoClosedAll={handleClose}>
      <ModalHeader border closeButton onHide={handleClose}>
        <ModalTitle>{t(title)}</ModalTitle>
      </ModalHeader>
      <ModalBody>
        <>
          <div className="mb-16">
            <div className="d-flex align-items-center justify-content-between">
              <h5>{t('txt_parameter_list')}</h5>

              <SimpleSearch
                ref={simpleSearchRef}
                placeholder={t('txt_type_to_search')}
                onSearch={handleSearch}
                popperElement={
                  <>
                    <p className="color-grey">{t('txt_search_description')}</p>
                    <ul className="search-field-item list-unstyled">
                      <li className="mt-16">{t('txt_business_name')}</li>
                      <li className="mt-16">{t('txt_more_info')}</li>
                      <li className="mt-16">{t('txt_green_screen_name')}</li>
                    </ul>
                  </>
                }
              />
            </div>

            {searchValue && !isEmpty(dynamicParameter) && (
              <div className="d-flex justify-content-end mt-16 mr-n8">
                <ClearAndResetButton
                  small
                  onClearAndReset={handleClearFilter}
                />
              </div>
            )}
            {!isEmpty(listErrorMsg) && (
              <div className="mt-8">
                {listErrorMsg.map(item => (
                  <InlineMessage
                    key={item.message}
                    variant="danger"
                    className="mb-0 mt-8 mr-8"
                  >
                    <div className="d-flex">
                      <div className="flex-1">
                        <TruncateText lines={1}>{item.message}</TruncateText>
                      </div>
                    </div>
                  </InlineMessage>
                ))}
              </div>
            )}
          </div>
          {isEmpty(dynamicParameter) && (
            <div className="d-flex flex-column justify-content-center mt-40">
              <NoDataFound
                id="newRecord_notfound"
                hasSearch
                title={t('txt_no_results_found')}
                linkTitle={t('txt_clear_and_reset')}
                onLinkClicked={handleClearFilter}
              />
            </div>
          )}
          {!isEmpty(dynamicParameter) && (
            <Grid
              className="grid-has-control grid-has-tooltip center"
              columns={columns}
              data={dynamicParameter}
            />
          )}
        </>
      </ModalBody>
      <ModalFooter
        cancelButtonText={t('txt_cancel')}
        okButtonText={isCreate ? t('txt_add') : t('txt_save')}
        onCancel={handleClose}
        onOk={handleAddRecord}
      ></ModalFooter>
    </ModalRegistry>
  );
};

export default AddNewRecordModal;
