import {
  RecordFieldParameterEnum,
  SPAFieldParameterEnum
} from 'app/constants/enums';
import {
  convertParameter,
  formatDropdownValue,
  formatSysPrinAgentObjectArr,
  isEqualOriginalData,
  mapColorBadge,
  mapDataToFormSPA,
  mapParameter,
  parseDefaultRecordType,
  STATUS_SPA,
  uniqueArr
} from './helper';

describe('WorkflowManageAccountSerialization > ManageAccountSerializationStep > helper > parseDefaultRecordType', () => {
  const parameters = [
    {
      name: RecordFieldParameterEnum.RecordType,
      newValue: 'newValue',
      previousValue: 'previousValue'
    },
    { name: RecordFieldParameterEnum.AccountIDOrPIID },
    { name: RecordFieldParameterEnum.AlternatePrefix },
    { name: RecordFieldParameterEnum.AvailableRecordNumber },
    { name: RecordFieldParameterEnum.GenerationMethod },
    { name: RecordFieldParameterEnum.NextFrom },
    { name: RecordFieldParameterEnum.NextTo },
    { name: RecordFieldParameterEnum.Prefix },
    { name: RecordFieldParameterEnum.RangeFrom },
    { name: RecordFieldParameterEnum.RangeTo },
    { name: RecordFieldParameterEnum.SerialNumber },
    { name: RecordFieldParameterEnum.UseAlternateAgent },
    { name: RecordFieldParameterEnum.WarningLevel },
    { name: RecordFieldParameterEnum.AlternatePrincipal },
    { name: RecordFieldParameterEnum.AlternateAgent }
  ];
  it('parseDefaultRecordType > status New', () => {
    const data = {
      status: STATUS_SPA.New,
      parameters
    };
    const initData = {};
    const result = parseDefaultRecordType(data, initData) as any;
    expect(result[RecordFieldParameterEnum.RecordType]).toBe('newValue');
  });

  it('parseDefaultRecordType > status Updated', () => {
    const data = {
      status: STATUS_SPA.Updated,
      parameters
    };
    const initData = {};
    const result = parseDefaultRecordType(data, initData);
    expect(result[RecordFieldParameterEnum.RecordType]).toBe('newValue');
  });

  it('parseDefaultRecordType > status NotUpdated', () => {
    const data = {
      status: STATUS_SPA.NotUpdated,
      parameters
    };
    const initData = {};
    const result = parseDefaultRecordType(data, initData);
    expect(result[RecordFieldParameterEnum.RecordType]).toBe('previousValue');
  });
});

describe('WorkflowManageAccountSerialization > ManageAccountSerializationStep > helper > isEqualOriginalData', () => {
  it('isEqualOriginalData', () => {
    const array = [{ newValue: 'value' }];
    const key = (el: any) => el.toString();
    const result = uniqueArr(array, key);
    expect(result).toEqual([{ newValue: 'value' }]);
  });
});

describe('WorkflowManageAccountSerialization > ManageAccountSerializationStep > helper > mapParameter', () => {
  const data = {
    [SPAFieldParameterEnum.RecordType]: 'value',
    [SPAFieldParameterEnum.GenerationMethod]: 'value',
    [SPAFieldParameterEnum.RangeFrom]: 'value',
    [SPAFieldParameterEnum.RangeTo]: 'value',
    [SPAFieldParameterEnum.SerialNumber]: 'value',
    [SPAFieldParameterEnum.WarningLevel]: 'value',
    [SPAFieldParameterEnum.Prefix]: 'value',
    [SPAFieldParameterEnum.AlternatePrefix]: 'value',
    [SPAFieldParameterEnum.AccountIDOrPIID]: 'value',
    [SPAFieldParameterEnum.NextFrom]: 'value',
    [SPAFieldParameterEnum.NextTo]: 'value',
    [SPAFieldParameterEnum.AvailableRecordNumber]: 'value',
    [SPAFieldParameterEnum.UseAlternateAgent]: 'value',
    [SPAFieldParameterEnum.AlternatePrincipal]: 'value',
    [SPAFieldParameterEnum.AlternateAgent]: 'value',
    strange: 'value'
  };
  test('mapParameter', () => {
    const result = mapParameter(data);
    expect(result).toHaveLength(15);
  });

  test('mapDataToFormSPA', () => {
    const result = mapDataToFormSPA([data]);
    expect(result).toHaveLength(1);
  });

  test('convertParameter', () => {
    const result = convertParameter([data]);
    expect(result).toHaveLength(0);
  });
});

describe('WorkflowManageAccountSerialization > ManageAccountSerializationStep > helper > mapColorBadge', () => {
  it('mapColorBadge > status new', () => {
    const result = mapColorBadge(STATUS_SPA.New);
    expect(result).toBe('purple');
  });

  it('mapColorBadge > status new', () => {
    const result = mapColorBadge(STATUS_SPA.New);
    expect(result).toBe('purple');
  });

  it('mapColorBadge > status update', () => {
    const result = mapColorBadge(STATUS_SPA.Updated);
    expect(result).toBe('green');
  });

  it('mapColorBadge > status marked for deletion', () => {
    const result = mapColorBadge(STATUS_SPA.MarkedForDeletion);
    expect(result).toBe('red');
  });

  it('mapColorBadge > status not updated', () => {
    const result = mapColorBadge(STATUS_SPA.NotUpdated);
    expect(result).toBe('grey');
  });

  it('mapColorBadge > status not updated', () => {
    const result = mapColorBadge('default');
    expect(result).toBe('grey');
  });
});

describe('WorkflowManageAccountSerialization > ManageAccountSerializationStep > helper > formatSysPrinAgentObjectArr', () => {
  it('formatSysPrinAgentObjectArr > data is null', () => {
    const data = undefined as any;
    const result = formatSysPrinAgentObjectArr(data);
    expect(result).toHaveLength(0);
  });

  it('formatSysPrinAgentObjectArr > has data', () => {
    const data = ['mane'];
    const result = formatSysPrinAgentObjectArr(data);
    expect(result).toHaveLength(1);
  });
});

describe('WorkflowManageAccountSerialization > ManageAccountSerializationStep > helper > formatDropdownValue', () => {
  it('formatDropdownValue > data is null', () => {
    const data = undefined as any;
    const result = formatDropdownValue(data);
    expect(result).toHaveLength(0);
  });

  it('formatDropdownValue > has data', () => {
    const data = [{ value: 'Mane' }, { value: '' }];
    const result = formatDropdownValue(data);
    expect(result).toHaveLength(2);
  });
});

describe('WorkflowManageAccountSerialization > ManageAccountSerializationStep > helper > isEqualOriginalData', () => {
  it('isEqualOriginalData', () => {
    const originData = [{ newValue: 'value' }];
    const newData = {
      parameters: [{ previousValue: 'value' }]
    };
    const result = isEqualOriginalData(originData, newData);
    expect(result).toBe(true);
  });
});
