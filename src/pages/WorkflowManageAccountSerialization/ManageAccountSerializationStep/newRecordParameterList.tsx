import {
  RecordFieldNameEnum,
  RecordFieldParameterEnum
} from 'app/constants/enums';
import React from 'react';

export interface RecordParameterMAS {
  id: RecordFieldParameterEnum;
  fieldName: RecordFieldNameEnum;
  greenScreenName: string;
  moreInfoText: string;
  moreInfo: React.ReactNode;
}

export const recordParameterMASNewStatus: any[] = [
  {
    id: RecordFieldParameterEnum.RecordType,
    fieldName: RecordFieldNameEnum.RecordType,
    greenScreenName: 'RT',
    moreInfoText:
      'Code representing the record type to which these automatic number assignment settings apply.',
    moreInfo:
      'Code representing the record type to which these automatic number assignment settings apply.'
  },
  {
    id: RecordFieldParameterEnum.GenerationMethod,
    fieldName: RecordFieldNameEnum.GenerationMethod,
    greenScreenName: 'GM',
    moreInfoText:
      'Code representing the method used to generate these serial numbers.',
    moreInfo:
      'Code representing the method used to generate these serial numbers.'
  },
  {
    id: RecordFieldParameterEnum.RangeFrom,
    fieldName: RecordFieldNameEnum.RangeFrom,
    greenScreenName: RecordFieldNameEnum.RangeFrom,
    moreInfoText:
      'The Range From parameter defines the beginning serial number in the range for this record type.',
    moreInfo:
      'The Range From parameter defines the beginning serial number in the range for this record type.'
  },
  {
    id: RecordFieldParameterEnum.RangeTo,
    fieldName: RecordFieldNameEnum.RangeTo,
    greenScreenName: RecordFieldNameEnum.RangeTo,
    moreInfoText:
      'The Range To parameter defines the ending serial number in the range of serial numbers for this record type.',
    moreInfo:
      'The Range To parameter defines the ending serial number in the range of serial numbers for this record type.'
  },
  {
    id: RecordFieldParameterEnum.SerialNumber,
    fieldName: RecordFieldNameEnum.SerialNumber,
    greenScreenName: RecordFieldNameEnum.SerialNumber,
    moreInfoText:
      'The Serial Number parameter defines the next available serial number for this record type.',
    moreInfo:
      'The Serial Number parameter defines the next available serial number for this record type.'
  },
  {
    id: RecordFieldParameterEnum.WarningLevel,
    fieldName: RecordFieldNameEnum.WarningLevel,
    greenScreenName: 'WL',
    moreInfoText:
      'The Warning Level parameter defines the percentage of serial numbers used at which a warning message is generated from the range of serial numbers you established for this record type.',
    moreInfo:
      'The Warning Level parameter defines the percentage of serial numbers used at which a warning message is generated from the range of serial numbers you established for this record type.'
  },
  {
    id: RecordFieldParameterEnum.Prefix,
    fieldName: RecordFieldNameEnum.Prefix,
    greenScreenName: RecordFieldNameEnum.Prefix,
    moreInfoText:
      'The Prefix parameter defines BIN or ICA number assigned by Visa or Mastercard to the agent you entered.',
    moreInfo:
      'The Prefix parameter defines BIN or ICA number assigned by Visa or Mastercard to the agent you entered.'
  },
  {
    id: RecordFieldParameterEnum.AlternatePrefix,
    fieldName: RecordFieldNameEnum.AlternatePrefix,
    greenScreenName: 'Alt Prfx',
    moreInfoText:
      'The Alternate Prefix parameter defines the alternate prefix to be used for automatic number assignment.',
    moreInfo:
      'The Alternate Prefix parameter defines the alternate prefix to be used for automatic number assignment.'
  },
  {
    id: RecordFieldParameterEnum.NextFrom,
    fieldName: RecordFieldNameEnum.NextFrom,
    greenScreenName: RecordFieldNameEnum.NextFrom,
    moreInfoText:
      'The Next From parameter defines the beginning serial number in the next range for this record type.',
    moreInfo:
      'The Next From parameter defines the beginning serial number in the next range for this record type.'
  },
  {
    id: RecordFieldParameterEnum.NextTo,
    fieldName: RecordFieldNameEnum.NextTo,
    greenScreenName: RecordFieldNameEnum.NextTo,
    moreInfoText:
      'The Next To parameter defines the ending serial number in the next range of serial numbers for this record type.',
    moreInfo:
      'The Next To parameter defines the ending serial number in the next range of serial numbers for this record type.'
  },
  {
    id: RecordFieldParameterEnum.UseAlternateAgent,
    fieldName: RecordFieldNameEnum.UseAlternateAgent,
    greenScreenName: 'UA',
    moreInfoText:
      'The Use Alternate Agent parameter indicates whether you want to use an alternate agent when generating numbers for record type S accounts.',
    moreInfo:
      'The Use Alternate Agent parameter indicates whether you want to use an alternate agent when generating numbers for record type S accounts.'
  },
  {
    id: RecordFieldParameterEnum.AlternatePrincipal,
    fieldName: RecordFieldNameEnum.AlternatePrincipal,
    greenScreenName: 'Alt Prin',
    moreInfoText:
      'The Alternate Principal parameter defines the identifier of the principal to be used to generate account numbers if you set the UA field to Y, or when the supply of account numbers within the range has been depleted regardless of your setting in the UA field.',
    moreInfo:
      'The Alternate Principal parameter defines the identifier of the principal to be used to generate account numbers if you set the UA field to Y, or when the supply of account numbers within the range has been depleted regardless of your setting in the UA field.'
  },
  {
    id: RecordFieldParameterEnum.AlternateAgent,
    fieldName: RecordFieldNameEnum.AlternateAgent,
    greenScreenName: 'Alt Agnt',
    moreInfoText:
      '	The Alternate Agent parameter defines the identifier of the agent to be used to generate account numbers if you set the UA field to Y, or when the supply of account numbers within the range has been depleted regardless of your setting in the UA field.',
    moreInfo:
      '	The Alternate Agent parameter defines the identifier of the agent to be used to generate account numbers if you set the UA field to Y, or when the supply of account numbers within the range has been depleted regardless of your setting in the UA field.'
  }
];

export const recordParameterMAS: any[] = [
  {
    id: RecordFieldParameterEnum.RecordType,
    fieldName: RecordFieldNameEnum.RecordType,
    greenScreenName: 'RT',
    moreInfoText:
      'Code representing the record type to which these automatic number assignment settings apply.',
    moreInfo:
      'Code representing the record type to which these automatic number assignment settings apply.'
  },
  {
    id: RecordFieldParameterEnum.GenerationMethod,
    fieldName: RecordFieldNameEnum.GenerationMethod,
    greenScreenName: 'GM',
    moreInfoText:
      'Code representing the method used to generate these serial numbers.',
    moreInfo:
      'Code representing the method used to generate these serial numbers.'
  },
  {
    id: RecordFieldParameterEnum.RangeFrom,
    fieldName: RecordFieldNameEnum.RangeFrom,
    greenScreenName: RecordFieldNameEnum.RangeFrom,
    moreInfoText:
      'The Range From parameter defines the beginning serial number in the range for this record type.',
    moreInfo:
      'The Range From parameter defines the beginning serial number in the range for this record type.'
  },
  {
    id: RecordFieldParameterEnum.RangeTo,
    fieldName: RecordFieldNameEnum.RangeTo,
    greenScreenName: RecordFieldNameEnum.RangeTo,
    moreInfoText:
      'The Range To parameter defines the ending serial number in the range of serial numbers for this record type.',
    moreInfo:
      'The Range To parameter defines the ending serial number in the range of serial numbers for this record type.'
  },
  {
    id: RecordFieldParameterEnum.SerialNumber,
    fieldName: RecordFieldNameEnum.SerialNumber,
    greenScreenName: RecordFieldNameEnum.SerialNumber,
    moreInfoText:
      'The Serial Number parameter defines the next available serial number for this record type.',
    moreInfo:
      'The Serial Number parameter defines the next available serial number for this record type.'
  },
  {
    id: RecordFieldParameterEnum.WarningLevel,
    fieldName: RecordFieldNameEnum.WarningLevel,
    greenScreenName: 'WL',
    moreInfoText:
      'The Warning Level parameter defines the percentage of serial numbers used at which a warning message is generated from the range of serial numbers you established for this record type.',
    moreInfo:
      'The Warning Level parameter defines the percentage of serial numbers used at which a warning message is generated from the range of serial numbers you established for this record type.'
  },
  {
    id: RecordFieldParameterEnum.Prefix,
    fieldName: RecordFieldNameEnum.Prefix,
    greenScreenName: RecordFieldNameEnum.Prefix,
    moreInfoText:
      'The Prefix parameter defines BIN or ICA number assigned by Visa or Mastercard to the agent you entered.',
    moreInfo:
      'The Prefix parameter defines BIN or ICA number assigned by Visa or Mastercard to the agent you entered.'
  },
  {
    id: RecordFieldParameterEnum.AlternatePrefix,
    fieldName: RecordFieldNameEnum.AlternatePrefix,
    greenScreenName: 'Alt Prfx',
    moreInfoText:
      'The Alternate Prefix parameter defines the alternate prefix to be used for automatic number assignment.',
    moreInfo:
      'The Alternate Prefix parameter defines the alternate prefix to be used for automatic number assignment.'
  },
  {
    id: RecordFieldParameterEnum.AccountIDOrPIID,
    fieldName: RecordFieldNameEnum.AccountIDOrPIID,
    greenScreenName: 'PF',
    moreInfoText:
      'The Account ID or PIID parameter represents whether the System creates the new SSR account identifier using the PREFIX from the original account identifier or the presentation instrument identifier.',
    moreInfo:
      'The Account ID or PIID parameter represents whether the System creates the new SSR account identifier using the PREFIX from the original account identifier or the presentation instrument identifier.'
  },
  {
    id: RecordFieldParameterEnum.NextFrom,
    fieldName: RecordFieldNameEnum.NextFrom,
    greenScreenName: RecordFieldNameEnum.NextFrom,
    moreInfoText:
      'The Next From parameter defines the beginning serial number in the next range for this record type.',
    moreInfo:
      'The Next From parameter defines the beginning serial number in the next range for this record type.'
  },
  {
    id: RecordFieldParameterEnum.NextTo,
    fieldName: RecordFieldNameEnum.NextTo,
    greenScreenName: RecordFieldNameEnum.NextTo,
    moreInfoText:
      'The Next To parameter defines the ending serial number in the next range of serial numbers for this record type.',
    moreInfo:
      'The Next To parameter defines the ending serial number in the next range of serial numbers for this record type.'
  },
  {
    id: RecordFieldParameterEnum.AvailableRecordNumber,
    fieldName: RecordFieldNameEnum.AvailableRecordNumber,
    greenScreenName: 'Available RN',
    moreInfoText:
      '	The Available Record Number parameter define the identifier of the serial number that is available right now for this record type.',
    moreInfo:
      '	The Available Record Number parameter define the identifier of the serial number that is available right now for this record type.'
  },
  {
    id: RecordFieldParameterEnum.UseAlternateAgent,
    fieldName: RecordFieldNameEnum.UseAlternateAgent,
    greenScreenName: 'UA',
    moreInfoText:
      'The Use Alternate Agent parameter indicates whether you want to use an alternate agent when generating numbers for record type S accounts.',
    moreInfo:
      'The Use Alternate Agent parameter indicates whether you want to use an alternate agent when generating numbers for record type S accounts.'
  },
  {
    id: RecordFieldParameterEnum.AlternatePrincipal,
    fieldName: RecordFieldNameEnum.AlternatePrincipal,
    greenScreenName: 'Alt Prin',
    moreInfoText:
      'The Alternate Principal parameter defines the identifier of the principal to be used to generate account numbers if you set the UA field to Y, or when the supply of account numbers within the range has been depleted regardless of your setting in the UA field.',
    moreInfo:
      'The Alternate Principal parameter defines the identifier of the principal to be used to generate account numbers if you set the UA field to Y, or when the supply of account numbers within the range has been depleted regardless of your setting in the UA field.'
  },
  {
    id: RecordFieldParameterEnum.AlternateAgent,
    fieldName: RecordFieldNameEnum.AlternateAgent,
    greenScreenName: 'Alt Agnt',
    moreInfoText:
      '	The Alternate Agent parameter defines the identifier of the agent to be used to generate account numbers if you set the UA field to Y, or when the supply of account numbers within the range has been depleted regardless of your setting in the UA field.',
    moreInfo:
      '	The Alternate Agent parameter defines the identifier of the agent to be used to generate account numbers if you set the UA field to Y, or when the supply of account numbers within the range has been depleted regardless of your setting in the UA field.'
  }
];
