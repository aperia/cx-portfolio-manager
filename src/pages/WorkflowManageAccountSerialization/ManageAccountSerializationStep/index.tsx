import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React from 'react';
import ConfigureParameters from './ConfigureParameters';
import parseFormValues from './parseFormValues';
import ManageAccountSerializationSummary from './Summary';

export interface ManageAccountSerializationFormValue {
  isValid?: boolean;
  sysPrinAgents: any[];
}

const ManageAccountSerialization: React.FC<
  WorkflowSetupProps<ManageAccountSerializationFormValue>
> = props => {
  return <ConfigureParameters {...props} />;
};

const ExtraStaticManageAccountSerializationStep =
  ManageAccountSerialization as WorkflowSetupStaticProp<ManageAccountSerializationFormValue>;

ExtraStaticManageAccountSerializationStep.summaryComponent =
  ManageAccountSerializationSummary;
ExtraStaticManageAccountSerializationStep.defaultValues = {
  isValid: false,
  sysPrinAgents: []
};

ExtraStaticManageAccountSerializationStep.parseFormValues = parseFormValues;

export default ExtraStaticManageAccountSerializationStep;
