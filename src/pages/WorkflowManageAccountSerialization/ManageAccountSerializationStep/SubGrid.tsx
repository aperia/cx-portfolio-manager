import { RecordFieldParameterEnum } from 'app/constants/enums';
import { Grid, TruncateText, useTranslation } from 'app/_libraries/_dls';
import get from 'lodash.get';
import isEmpty from 'lodash.isempty';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import { useSelectElementMetadataForSPA } from 'pages/_commons/redux/WorkflowSetup';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React from 'react';
import { STATUS_SPA } from './helper';
import {
  recordParameterMAS,
  recordParameterMASNewStatus
} from './newRecordParameterList';

export const SubGrid = (props: any) => {
  const data = get(props, 'row.original', {});
  const originalData = get(props, 'originalData', {});
  const { accountIDOrPIID } = useSelectElementMetadataForSPA();
  const { width } = useSelectWindowDimension();

  const { t } = useTranslation();

  const valueAccessor = (path: string) => (row: MagicKeyValue) => {
    let valueTruncate: any = '';
    const paramId = row.id as RecordFieldParameterEnum;
    switch (paramId) {
      case RecordFieldParameterEnum.AlternateAgent: {
        valueTruncate = data?.parameters?.find(
          (o: MagicKeyValue) =>
            o.name === RecordFieldParameterEnum.AlternateAgent
        )?.[path];
        break;
      }
      case RecordFieldParameterEnum.AlternatePrefix: {
        valueTruncate = data?.parameters?.find(
          (o: MagicKeyValue) =>
            o.name === RecordFieldParameterEnum.AlternatePrefix
        )?.[path];
        break;
      }
      case RecordFieldParameterEnum.AccountIDOrPIID: {
        const _id = data?.parameters?.find(
          (o: MagicKeyValue) =>
            o.name === RecordFieldParameterEnum.AccountIDOrPIID
        )?.[path];

        valueTruncate = accountIDOrPIID.find(v => v.code === _id)?.text;
        break;
      }
      case RecordFieldParameterEnum.AlternatePrincipal: {
        valueTruncate = data?.parameters?.find(
          (o: MagicKeyValue) =>
            o.name === RecordFieldParameterEnum.AlternatePrincipal
        )?.[path];
        break;
      }
      case RecordFieldParameterEnum.GenerationMethod: {
        const val = data?.parameters?.find(
          (o: MagicKeyValue) =>
            o.name === RecordFieldParameterEnum.GenerationMethod
        )?.[path];

        valueTruncate = !isEmpty(val)
          ? originalData?.generationMethods?.find(
              (o: RefData) => o.code === val
            )?.text
          : '';
        break;
      }
      case RecordFieldParameterEnum.NextFrom: {
        valueTruncate = data?.parameters?.find(
          (o: MagicKeyValue) => o.name === RecordFieldParameterEnum.NextFrom
        )?.[path];
        break;
      }
      case RecordFieldParameterEnum.NextTo: {
        valueTruncate = data?.parameters?.find(
          (o: MagicKeyValue) => o.name === RecordFieldParameterEnum.NextTo
        )?.[path];
        break;
      }
      case RecordFieldParameterEnum.AvailableRecordNumber: {
        valueTruncate = data?.parameters?.find(
          (o: MagicKeyValue) =>
            o.name === RecordFieldParameterEnum.AvailableRecordNumber
        )?.[path];
        break;
      }
      case RecordFieldParameterEnum.Prefix: {
        valueTruncate = data?.parameters?.find(
          (o: MagicKeyValue) => o.name === RecordFieldParameterEnum.Prefix
        )?.[path];
        break;
      }
      case RecordFieldParameterEnum.RangeFrom: {
        valueTruncate = data?.parameters?.find(
          (o: MagicKeyValue) => o.name === RecordFieldParameterEnum.RangeFrom
        )?.[path];
        break;
      }
      case RecordFieldParameterEnum.RangeTo: {
        valueTruncate = data?.parameters?.find(
          (o: MagicKeyValue) => o.name === RecordFieldParameterEnum.RangeTo
        )?.[path];
        break;
      }
      case RecordFieldParameterEnum.RecordType: {
        const type = data?.parameters?.find(
          (o: MagicKeyValue) => o.name === RecordFieldParameterEnum.RecordType
        )?.[path];
        valueTruncate = !isEmpty(type?.trim())
          ? originalData?.types?.find((o: RefData) => o.code === type)?.text
          : '';
        break;
      }
      case RecordFieldParameterEnum.SerialNumber: {
        valueTruncate = data?.parameters?.find(
          (o: MagicKeyValue) => o.name === RecordFieldParameterEnum.SerialNumber
        )?.[path];
        break;
      }
      case RecordFieldParameterEnum.UseAlternateAgent: {
        const val = data?.parameters?.find(
          (o: MagicKeyValue) =>
            o.name === RecordFieldParameterEnum.UseAlternateAgent
        )?.[path];
        valueTruncate = originalData?.useAlternateAgents?.find(
          (o: RefData) => o.code === val
        )?.text;
        break;
      }
      case RecordFieldParameterEnum.WarningLevel: {
        const warning = data?.parameters?.find(
          (o: MagicKeyValue) => o.name === RecordFieldParameterEnum.WarningLevel
        )?.[path];
        valueTruncate = !isEmpty(`${warning}`) ? `${warning}%` : '';
        break;
      }
    }
    return (
      <TruncateText
        resizable
        lines={2}
        ellipsisLessText={t('txt_less')}
        ellipsisMoreText={t('txt_more')}
      >
        {valueTruncate}
      </TruncateText>
    );
  };

  const columns =
    data?.status !== STATUS_SPA.Updated
      ? [
          {
            id: 'fieldName',
            Header: t('txt_business_name'),
            accessor: 'fieldName'
          },
          {
            id: 'greenScreenName',
            Header: t('txt_green_screen_name'),
            accessor: 'greenScreenName'
          },
          {
            id: 'value',
            Header: t('txt_value'),
            width: width < 1280 ? 300 : 460,
            accessor:
              data?.status === STATUS_SPA.New
                ? valueAccessor('newValue')
                : valueAccessor('previousValue')
          },
          {
            id: 'moreInfo',
            Header: t('txt_more_info'),
            className: 'text-center',
            accessor: viewMoreInfo(['moreInfo'], t),
            width: 105
          }
        ]
      : [
          {
            id: 'fieldName',
            Header: t('txt_business_name'),
            accessor: 'fieldName'
          },
          {
            id: 'greenScreenName',
            Header: t('txt_green_screen_name'),
            accessor: 'greenScreenName'
          },
          {
            id: 'previousValue',
            Header: t('txt_previous_value'),
            accessor: valueAccessor('previousValue')
          },
          {
            id: 'newValue',
            Header: t('txt_new_value'),
            accessor: valueAccessor('newValue'),
            width: width < 1280 ? 300 : 460
          },
          {
            id: 'moreInfo',
            Header: t('txt_more_info'),
            className: 'text-center',
            accessor: viewMoreInfo(['moreInfo'], t),
            width: 105
          }
        ];

  const rows =
    data?.status === STATUS_SPA.New
      ? recordParameterMASNewStatus
      : recordParameterMAS;
  return isEmpty(data) ? (
    <div style={{ height: '4.8rem' }} />
  ) : (
    <div style={{ padding: '2rem' }}>
      <Grid className="grid-has-tooltip center" columns={columns} data={rows} />
    </div>
  );
};
