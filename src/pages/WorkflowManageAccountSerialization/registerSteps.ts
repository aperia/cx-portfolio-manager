import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { stepRegistry } from '../_commons/WorkflowSetup/registries';
import GettingStartStep from './GettingStartStep';
import AccountSerializationStep from './ManageAccountSerializationStep';

stepRegistry.registerStep(
  'GetStartedManageAccountSerialization',
  GettingStartStep
);

stepRegistry.registerStep(
  'ManageAccountSerialization',
  AccountSerializationStep,
  [
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_ACCOUNT_SERIALIZATION_PARAMETERS_CONFIRM_EDIT
  ]
);
