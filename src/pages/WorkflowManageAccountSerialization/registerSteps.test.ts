import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { default as stepRegistry } from '../_commons/WorkflowSetup/registries/stepRegistry';

const registerFunc = jest.spyOn(stepRegistry, 'registerStep');

describe('FlexibleMonetaryTransactions > registerSteps', () => {
  it('Should register components', async () => {
    await import('./registerSteps');

    expect(registerFunc).toBeCalledWith(
      'GetStartedManageAccountSerialization',
      expect.anything()
    );

    expect(registerFunc).toBeCalledWith(
      'ManageAccountSerialization',
      expect.anything(),
      [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_ACCOUNT_SERIALIZATION_PARAMETERS_CONFIRM_EDIT
      ]
    );
  });
});
