import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { stepRegistry } from '../_commons/WorkflowSetup/registries';
import ConfigureParametersStep from './ConfigureParametersStep';
import GettingStartStep from './GettingStartStep';

stepRegistry.registerStep(
  'GetStartedForceEmbossReplacementCardsWorkflow',
  GettingStartStep
);

stepRegistry.registerStep(
  'ConfigureParametersForceEmbossReplacementCardsWorkflow',
  ConfigureParametersStep,
  [UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__FORCE_EMBOSS_REPLACEMENT_CARDS]
);
