import { WorkflowMetadataList } from 'app/fixtures/workflow-metadata';
import * as helper from 'app/helpers/isDevice';
import { mockUseDispatchFnc, renderWithMockStore } from 'app/utils';
import 'app/_libraries/_dls/test-utils/mocks/mockCanvas';
import React from 'react';
import ConfigureParametersStepGrid from './ConfigureParametersStepGrid';
jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/utils/MockView')
);
const mockUseDispatch = mockUseDispatchFnc();
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderComponent = (props: any) => {
  return renderWithMockStore(<ConfigureParametersStepGrid {...props} />, {
    initialState: {
      workflowSetup: {
        elementMetadata: {
          elements: WorkflowMetadataList
        }
      }
    } as AppState
  });
};
const mockHelper: any = helper;
describe('ConfigureParametersStepSummary ', () => {
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => (() => {}) as any);
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
  });

  it('Should render ConfigureParametersStepGrid > value', async () => {
    const props = {
      value: [
        {
          value: {
            name: 'IDENTIFIER',
            value: 'Required',
            description: 'Presentation instrument identifier'
          }
        }
      ]
    } as any;

    const wrapper = renderComponent(props);
    expect(wrapper).toEqual(Promise.resolve({}));
  });

  it('Should render ConfigureParametersStepGrid > value > isNM = true', async () => {
    const props = {
      isNM: true,
      value: [
        {
          value: {
            name: 'IDENTIFIER',
            value: 'Required',
            description: 'Presentation instrument identifier'
          }
        },
        {
          value: {
            name: 'FEFE',
            value: 'Required',
            description: 'Presentation instrument identifier'
          }
        }
      ],
      subValue: [
        {
          value: {
            name: 'IDENTIFIER',
            value: 'Required',
            description: 'Presentation instrument identifier'
          }
        },
        {
          value: {
            name: 'KAKA',
            value: 'Required',
            description: 'Presentation instrument identifier'
          }
        }
      ]
    } as any;

    const wrapper = renderComponent(props);
    expect(wrapper).toEqual(Promise.resolve({}));
  });

  it('Should render ConfigureParametersStepGrid > value > isNM = false', async () => {
    const props = {
      isNM: false,
      value: [
        {
          value: {
            name: 'IDENTIFIER',
            value: 'Required',
            description: 'Presentation instrument identifier'
          }
        },
        {
          value: {
            name: 'FEFE',
            value: 'Required',
            description: 'Presentation instrument identifier'
          }
        }
      ],
      subValue: [
        {
          value: {
            name: 'IDENTIFIER',
            value: 'Required',
            description: 'Presentation instrument identifier'
          }
        },
        {
          value: {
            name: 'KAKA',
            value: 'Required',
            description: 'Presentation instrument identifier'
          }
        }
      ]
    } as any;

    const wrapper = renderComponent(props);
    expect(wrapper).toEqual(Promise.resolve({}));
  });

  it('Should render ConfigureParametersStepGrid > isDevice', async () => {
    mockHelper.isDevice = true;
  });

  it('Should render ConfigureParametersStepGrid > isDevice false', async () => {
    mockHelper.isDevice = false;
  });
});
