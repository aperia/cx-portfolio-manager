import { fireEvent } from '@testing-library/dom';
import { renderWithMockStore } from 'app/utils';
import 'app/utils/_mockComponent/mockComboBox';
import 'app/utils/_mockComponent/mockDatePicker';
import mockDateTooltipParams from 'app/utils/_mockComponent/mockDatePicker';
import { STATUS } from 'app/_libraries/_dls/components/Upload/helper';
import 'app/_libraries/_dls/test-utils/mocks/mockCanvas';
import React from 'react';
import * as ReactRedux from 'react-redux';
import { default as NM } from './index';

const mockUseDispatch = jest.spyOn(ReactRedux, 'useDispatch');

(function mockDOMMatrix() {
  class DOMMatrixMock {
    scale = jest.fn();
    translate = jest.fn();
  }
  global.DOMMatrix = DOMMatrixMock as any;
})();

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});
jest.mock('app/hooks/useUnsavedChangeRegistry', () => {
  return {
    __esModule: true,
    useUnsavedChangeRegistry: () => {}
  };
});

jest.mock('app/hooks/useUnsavedChangeRegistry', () => {
  return {
    __esModule: true,
    useUnsavedChangeRegistry: () => {}
  };
});

const elementMetaData = [
  {
    id: '47',
    workflowTemplateId: '964929713',
    name: 'action',
    options: [
      {
        name: '',
        value: 'pid.194',
        description: 'Standard Plastic Request',
        workFlow: 'emboss'
      },
      {
        name: '',
        value: 'nm.65',
        description: 'Update Account Level Mail Code',
        workFlow: 'emboss'
      },
      {
        name: '',
        value: 'pid.200.rush',
        description: 'Rush Plastic Request',
        workFlow: 'emboss'
      },
      {
        name: '',
        value: 'pid.200.mass',
        description: 'Mass Plastic Request',
        workFlow: 'emboss'
      }
    ]
  },
  {
    id: '48',
    workflowTemplateId: '964929713',
    name: 'pid.194.code',
    options: [
      {
        name: 'Emboss plastics for all active customer presentation instrument identifiers on a nondual account, or emboss plastics for all active customer presentation instrument identifiers on the drive side of a dual account',
        value: '0',
        description:
          'Emboss plastics for all active customer presentation instrument identifiers on a nondual account, or emboss plastics for all active customer presentation instrument identifiers on the drive side of a dual account'
      },
      {
        name: 'Emboss plastics for the principal customer on a nondual account, or for the principal customer on the drive side of a dual account',
        value: '1',
        description:
          'Emboss plastics for the principal customer on a nondual account, or for the principal customer on the drive side of a dual account'
      }
    ]
  },
  {
    id: '49',
    workflowTemplateId: '964929713',
    name: 'pid.194.transaction.id',
    options: [
      {
        name: '',
        value: 'nm.65',
        description: 'NM*65 Mail Code Flag and Update'
      }
    ]
  },
  {
    id: '50',
    workflowTemplateId: '964929713',
    name: 'pid.200.code',
    options: [
      {
        name: 'Emboss plastics for all active customer presentation instrument identifiers on a nondual account, or emboss plastics for all active customer presentation instrument identifiers on the drive side of a dual account',
        value: '0',
        description:
          'Emboss plastics for all active customer presentation instrument identifiers on a nondual account, or emboss plastics for all active customer presentation instrument identifiers on the drive side of a dual account'
      },
      {
        name: 'Emboss plastics for the principal customer on a nondual account, or for the principal customer on the drive side of a dual account',
        value: '1',
        description:
          'Emboss plastics for the principal customer on a nondual account, or for the principal customer on the drive side of a dual account'
      }
    ]
  },
  {
    id: '51',
    workflowTemplateId: '964929713',
    name: 'transaction.id',
    options: [
      {
        name: '',
        workFlow: 'emboss',
        value: 'nm.65',
        description: 'NM*65 Mail Code Flag and Update'
      },
      {
        name: '',
        workFlow: 'emboss',
        value: 'nm.65',
        description: 'Mass Plastic Request'
      },
      {
        name: '',
        workFlow: 'emboss',
        value: 'nm.65',
        description: 'Rush Plastic Request'
      },
      {
        name: '',
        workFlow: 'emboss',
        value: 'nm.65',
        description: 'Standard Plastic Request'
      }
    ]
  }
];

const parameterData = [
  {
    type: 'Optional',
    name: 'SEQUENCE',
    description: 'sed dolore magna mazim duo kasd',
    check: false
  },
  {
    type: 'Optional',
    name: 'SEQUENCE',
    description: 'stet sed est sanctus sadipscing',
    check: false
  }
];

const renderComponent = (props: any) => {
  return renderWithMockStore(<NM {...props} />, {
    initialState: {
      workflowSetup: {
        elementMetadata: {
          elements: elementMetaData as any
        },
        parameterData: {
          parameterList: parameterData as any
        }
      }
    } as AppState
  });
};

const setFormValuesMock = jest.fn();
const clearFormValuesMock = jest.fn();

jest.mock('app/hooks/useUnsavedChangeRegistry', () => {
  return {
    __esModule: true,

    useUnsavedChangeRegistry: (options: any, changes: any, onConfirm: any) => {
      onConfirm && onConfirm();
    }
  };
});

describe('WorkflowForceEmbossReplacementCards > GettingStartStep > Index', () => {
  beforeEach(() => {
    mockUseDispatch.mockReturnValue(jest.fn());
    mockDateTooltipParams.mockReturnValue([new Date('01/01/2021')]);
  });
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Render Component with initial formValues with value PID194', async () => {
    const props = {
      formValues: {
        config: {
          parameters: {
            transactionId: {
              value: 'pid.194'
            }
          },
          parametersPID194: 'parametersPID194'
        }
      },
      clearFormValues: clearFormValuesMock,
      setFormValues: setFormValuesMock
    } as any;

    const wrapper = await renderComponent(props);
    expect(
      wrapper.getByText(
        /txt_force_emboss_replacement_cards_configuration_select_action/
      )
    ).toBeInTheDocument();
    fireEvent.click(
      wrapper.getByText(
        /txt_force_emboss_replacement_cards_configuration_select_action/
      )
    );
    expect(setFormValuesMock).toBeCalled();
  });

  it('Render Component with initial formValues with value PID200RUSH', async () => {
    const props = {
      formValues: {
        config: {
          parameters: {
            transactionId: {
              value: 'pid.200.rush'
            }
          },
          parametersPID200RUSH: 'parametersPID200RUSH'
        }
      },
      clearFormValues: clearFormValuesMock,
      setFormValues: setFormValuesMock
    } as any;

    const wrapper = await renderComponent(props);
    expect(
      wrapper.getByText(
        /txt_force_emboss_replacement_cards_configuration_select_action/
      )
    ).toBeInTheDocument();
    fireEvent.click(
      wrapper.getByText(
        /txt_force_emboss_replacement_cards_configuration_select_action/
      )
    );
    expect(setFormValuesMock).toBeCalled();
  });

  it('Render Component with initial formValues with value PID200MASS', async () => {
    const props = {
      formValues: {
        config: {
          parameters: {
            transactionId: {
              value: 'pid.200.mass'
            }
          },
          parametersPID200MASS: 'parametersPID200MASS'
        }
      },
      clearFormValues: clearFormValuesMock,
      setFormValues: setFormValuesMock
    } as any;

    const wrapper = await renderComponent(props);
    expect(
      wrapper.getByText(
        /txt_force_emboss_replacement_cards_configuration_select_action/
      )
    ).toBeInTheDocument();
    fireEvent.click(
      wrapper.getByText(
        /txt_force_emboss_replacement_cards_configuration_select_action/
      )
    );
    expect(setFormValuesMock).toBeCalled();
  });

  it('Render Component with initial formValues with transactionId is string', async () => {
    const props = {
      formValues: {
        config: {
          parameters: {
            transactionId: 'pid.200.mass'
          },
          parametersPID200MASS: 'parametersPID200MASS'
        }
      },
      clearFormValues: clearFormValuesMock,
      setFormValues: setFormValuesMock
    } as any;

    const wrapper = await renderComponent(props);
    expect(
      wrapper.getByText(
        /txt_force_emboss_replacement_cards_configuration_select_action/
      )
    ).toBeInTheDocument();
    fireEvent.click(
      wrapper.getByText(
        /txt_force_emboss_replacement_cards_configuration_select_action/
      )
    );
    expect(setFormValuesMock).toBeCalled();
  });

  it('Render PID194 > isNM65MailCode true', async () => {
    //let wrapper1: HTMLElement;

    const props = {
      formValues: {
        isValid: true,
        config: {
          parametersPID194: {
            value: 'pid.194',
            action: {
              value: '',
              param: {
                isNM65MailCode: true,
                standardCode: {
                  code: '0',
                  text: '0 - Text'
                }
              }
            }
          },
          parameters: {
            value: 'pid.194',
            action: {
              value: '',
              param: {
                isNM65MailCode: true,
                standardCode: {
                  code: '0',
                  text: '0 - Text'
                }
              }
            }
          }
        }
      },
      clearFormValues: clearFormValuesMock,
      setFormValues: setFormValuesMock,
      hasInstance: true
    } as any;

    const wrapper = await renderComponent(props);

    expect(
      wrapper.getByText(
        /txt_force_emboss_replacement_cards_configuration_title_1/
      )
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('Standard Plastic Request'));
    expect(setFormValuesMock).toBeCalled();

    fireEvent.click(wrapper.getAllByText('NM*65 Mail Code Flag and Update')[0]);
    expect(setFormValuesMock).toBeCalled();

    fireEvent.click(wrapper.getAllByText('NM*65 Mail Code Flag and Update')[1]);
    expect(setFormValuesMock).toBeCalled();
  });

  it('Render PID194 > isNM65MailCode false', async () => {
    const props = {
      formValues: {
        isValid: true,
        config: {
          parametersPID194: {
            value: 'pid.194',
            action: {
              value: '',
              param: {
                isNM65MailCode: false,
                standardCode: {
                  code: '0',
                  text: '0 - Text'
                }
              }
            }
          },
          parameters: {
            value: 'pid.194',
            action: {
              value: '',
              param: {
                isNM65MailCode: false,
                standardCode: {
                  code: '0',
                  text: '0 - Text'
                }
              }
            }
          }
        }
      },
      clearFormValues: clearFormValuesMock,
      setFormValues: setFormValuesMock,
      hasInstance: true,
      dependencies: {
        attachmentId: '123',
        files: [
          {
            idx: 123,
            status: STATUS.valid
          }
        ]
      },
      initialValues: {
        isValid: true,
        config: {
          parametersPID194: {
            value: 'pid.194',
            action: {
              value: '',
              param: {
                isNM65MailCode: false,
                standardCode: {
                  code: '1',
                  text: '1 - Text'
                }
              }
            }
          },
          parameters: {
            value: 'pid.194',
            action: {
              value: '',
              param: {
                isNM65MailCode: false,
                standardCode: {
                  code: '1',
                  text: '1 - Text'
                }
              }
            }
          }
        }
      }
    } as any;

    const wrapper = await renderComponent(props);

    expect(
      wrapper.getByText(
        /txt_force_emboss_replacement_cards_configuration_title_1/
      )
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText(/Standard Plastic Request/));
    expect(setFormValuesMock).toBeCalled();

    fireEvent.click(wrapper.getAllByText('NM*65 Mail Code Flag and Update')[0]);
    expect(setFormValuesMock).toBeCalled();

    const element = wrapper.baseElement
      .querySelectorAll('.dls-dropdown-list .text-field-container')
      .item(0);

    fireEvent.focus(element);

    const pageElement = document.querySelectorAll('.dls-popup .item').item(1);
    fireEvent.click(pageElement);
  });

  it('Render PID196', async () => {
    const props = {
      formValues: {
        config: {
          parametersPID200RUSH: {
            value: 'pid.200.rush',
            action: {
              value: '',
              param: {
                standardCode: {
                  code: '0',
                  text: '0 - Text'
                }
              }
            }
          },
          parameters: {
            value: 'pid.200.rush',
            action: {
              value: '',
              param: {
                standardCode: {
                  code: '0',
                  text: '0 - Text'
                }
              }
            }
          }
        }
      },
      clearFormValues: clearFormValuesMock,
      setFormValues: setFormValuesMock
    } as any;

    const wrapper = await renderComponent(props);
    expect(wrapper.getByText('Rush Plastic Request')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText(/Rush Plastic Request/));
    expect(setFormValuesMock).toBeCalled();

    const element = wrapper.baseElement
      .querySelectorAll('.dls-dropdown-list .text-field-container')
      .item(0);

    fireEvent.focus(element);

    const pageElement = document.querySelectorAll('.dls-popup .item').item(1);
    fireEvent.click(pageElement);
  });

  it('Render PID197', async () => {
    const props = {
      formValues: {
        config: {
          parametersPID200MASS: {
            value: 'pid.200.mass',
            action: {
              value: '',
              param: {
                standardCode: {
                  code: '0',
                  text: '0 - Text'
                }
              }
            }
          },
          parameters: {
            value: 'pid.200.mass',
            action: {
              value: '',
              param: {
                isNM65MailCode: true,
                standardCode: {
                  code: '0',
                  text: '0 - Text'
                }
              }
            }
          }
        }
      },
      clearFormValues: clearFormValuesMock,
      setFormValues: setFormValuesMock
    } as any;

    const wrapper = await renderComponent(props);
    expect(wrapper.getByText('Mass Plastic Request')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('Mass Plastic Request'));
    expect(setFormValuesMock).toBeCalled();

    const element = wrapper.baseElement
      .querySelectorAll('.dls-dropdown-list .text-field-container')
      .item(0);

    fireEvent.focus(element);

    const pageElement = document.querySelectorAll('.dls-popup .item').item(1);
    fireEvent.click(pageElement);
  });

  it('Render PID195', async () => {
    const props = {
      formValues: {
        config: {
          parameters: {
            value: 'nm.65',
            action: {
              value: '',
              param: {
                standardCode: {
                  code: '0',
                  text: '0 - Text'
                }
              }
            }
          }
        }
      },
      clearFormValues: clearFormValuesMock,
      setFormValues: setFormValuesMock
    } as any;

    const wrapper = await renderComponent(props);

    expect(
      wrapper.getByText('NM*65 Mail Code Flag and Update')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('NM*65 Mail Code Flag and Update'));
    expect(setFormValuesMock).toBeCalled();
  });

  it('Render PID200MASS', async () => {
    const props = {
      formValues: {
        config: {
          parametersPID200MASS: {
            value: 'pid.200.mass',
            action: {
              value: '',
              param: {
                standardCode: {
                  code: '0',
                  text: '0 - Text'
                }
              }
            }
          },
          parameters: {
            value: 'pid.200.mass',
            action: {
              value: '',
              param: {
                isNM65MailCode: true,
                standardCode: {
                  code: '0',
                  text: '0 - Text'
                }
              }
            }
          }
        }
      },
      clearFormValues: clearFormValuesMock,
      setFormValues: setFormValuesMock
    } as any;

    const wrapper = await renderComponent(props);
    expect(wrapper.getByText('Mass Plastic Request')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('Mass Plastic Request'));
    expect(setFormValuesMock).toBeCalled();

    const element = wrapper.baseElement
      .querySelectorAll('.dls-dropdown-list .text-field-container')
      .item(0);

    fireEvent.focus(element);

    const pageElement = document.querySelectorAll('.dls-popup .item').item(1);
    fireEvent.click(pageElement);
  });

  it('Render PID200RUSH', async () => {
    const props = {
      formValues: {
        config: {
          parametersPID200RUSH: {
            value: 'pid.200.rush',
            action: {
              value: '',
              param: {
                standardCode: {
                  code: '0',
                  text: '0 - Text'
                }
              }
            }
          },
          parameters: {
            value: 'pid.200.rush',
            action: {
              value: '',
              param: {
                standardCode: {
                  code: '0',
                  text: '0 - Text'
                }
              }
            }
          }
        }
      },
      clearFormValues: clearFormValuesMock,
      setFormValues: setFormValuesMock
    } as any;

    const wrapper = await renderComponent(props);
    expect(wrapper.getByText('Rush Plastic Request')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText(/Rush Plastic Request/));
    expect(setFormValuesMock).toBeCalled();

    const element = wrapper.baseElement
      .querySelectorAll('.dls-dropdown-list .text-field-container')
      .item(0);

    fireEvent.focus(element);

    const pageElement = document.querySelectorAll('.dls-popup .item').item(1);
    fireEvent.click(pageElement);
  });
});
