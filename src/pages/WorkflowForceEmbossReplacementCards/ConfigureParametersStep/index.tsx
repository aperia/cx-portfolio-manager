import {
  confirmEditWhenChangeEffectToUploadProps,
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { useUnsavedChangeRegistry } from 'app/hooks';
import {
  CheckBox,
  DropdownBaseChangeEvent,
  Icon,
  InlineMessage,
  Radio,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import { StateFile } from 'app/_libraries/_dls/components/Upload/File';
import { STATUS } from 'app/_libraries/_dls/components/Upload/helper';
import { isEmpty } from 'lodash';
import {
  actionsWorkflowSetup,
  useSelectElementMetadataForForceEmbossReplacementCards
} from 'pages/_commons/redux/WorkflowSetup';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';
import ConfigureParametersStepGrid from './ConfigureParametersStepGrid';
import ConfigureParametersStepSummary from './ConfigureParametersStepSummary';
import parseFormValues from './parseFormValues';
import PID194 from './PID194';
import PID200MASS from './PID200MASS';
import PID200RUSH from './PID200RUSH';
import StepValueFunc from './stepValueFunc';

export interface ConfigureParametersStepProps {
  isValid?: boolean;
  config?: {
    parameters?: {
      transactionId?: string;
      value?: string;
      dropdownValue?: string;
      action?: {
        value?: string;
        param?: {
          isNM65MailCode?: boolean;
          standardCode?: {
            code?: string;
            text?: string;
          };
        };
      };
    };
    parametersPID194?: {
      value?: string;
      action?: {
        value?: string;
        param?: {
          isNM65MailCode?: boolean;
          standardCode?: {
            code?: string;
            text?: string;
          };
        };
      };
    };
    parametersPID200RUSH?: {
      value?: string;
      action?: {
        value?: string;
        param?: {
          standardCode?: {
            code?: string;
            text?: string;
          };
        };
      };
    };
    parametersPID200MASS?: {
      value?: string;
      action?: {
        value?: string;
        param?: {
          standardCode?: {
            code?: string;
            text?: string;
          };
        };
      };
    };
  };
}

export interface IFieldValidate {
  standardCode: string;
}

export interface IDependencies {
  files?: StateFile[];
}
export interface ConfigureParametersProps
  extends WorkflowSetupProps<ConfigureParametersStepProps, IDependencies> {
  clearFormName: string;
}

const ConfigureParametersStep: React.FC<ConfigureParametersProps> = ({
  savedAt,
  dependencies,
  hasInstance,
  formValues,
  isStuck,
  setFormValues,
  clearFormValues,
  clearFormName
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;

  const [initialValues, setInitialValues] = useState(formValues);

  const [titleCode, setTitleCode] = useState(
    'txt_force_emboss_replacement_cards_configuration_PID194'
  );

  useEffect(() => {
    dispatch(
      actionsWorkflowSetup.getWorkflowMetadata([
        'action',
        'transaction.id',
        'pid.194.transaction.id',
        'pid.194.code',
        'pid.194.transaction',
        'pid.200.code',
        'pid.200.transaction',
        'nm.65.transaction'
      ])
    );
  }, [dispatch]);

  const {
    configuration,
    transaction,
    PID194Code,
    PID200Code,
    PID194Transaction,
    PID200Transaction,
    NM65Transaction
  } = useSelectElementMetadataForForceEmbossReplacementCards();

  const handleOnSelectAction = useCallback(
    (name: string, item?: any, initValue?: string) => {
      const value = item?.value || initValue;
      const formPID194 = formValues?.config?.parametersPID194;
      const formPID200RUSH = formValues?.config?.parametersPID200RUSH;
      const formPID200MASS = formValues?.config?.parametersPID200MASS;

      switch (name) {
        case 'configure':
          setFormValues({
            config: {
              parameters:
                formPID194 !== undefined && value === 'pid.194'
                  ? formPID194
                  : formPID200RUSH !== undefined && value === 'pid.200.rush'
                  ? formPID200RUSH
                  : formPID200MASS !== undefined && value === 'pid.200.mass'
                  ? formPID200MASS
                  : { value: value },
              parametersPID194: formPID194,
              parametersPID200RUSH: formPID200RUSH,
              parametersPID200MASS: formPID200MASS
            }
          });

          break;
      }
    },
    [
      formValues?.config?.parametersPID194,
      formValues?.config?.parametersPID200RUSH,
      formValues?.config?.parametersPID200MASS,
      setFormValues
    ]
  );

  const handleCheckBoxChange = (e: any) => {
    const value = e.target?.checked;
    const action = formValues.config?.parameters?.value;
    const valueAction = '';

    setValue(action, valueAction, e.target?.name, value);
  };

  const setValue = (
    action: any,
    valueAction: any,
    name: string,
    value: any
  ) => {
    setFormValues({
      config: {
        parametersPID194:
          action === 'pid.194'
            ? {
                value: action,
                action: {
                  value: valueAction,
                  param: {
                    ...formValues.config?.parameters?.action?.param,
                    [name]: value
                  }
                }
              }
            : formValues?.config?.parametersPID194,
        parametersPID200RUSH:
          action === 'pid.200.rush'
            ? {
                value: action,
                action: {
                  value: valueAction,
                  param: {
                    ...formValues.config?.parameters?.action?.param,
                    [name]: value
                  }
                }
              }
            : formValues?.config?.parametersPID200RUSH,
        parametersPID200MASS:
          action === 'pid.200.mass'
            ? {
                value: action,
                action: {
                  value: valueAction,
                  param: {
                    ...formValues.config?.parameters?.action?.param,
                    [name]: value
                  }
                }
              }
            : formValues?.config?.parametersPID200MASS,
        parameters: {
          dropdownValue: value?.code,
          value: action,
          action: {
            value: valueAction,
            param: {
              ...formValues.config?.parameters?.action?.param,
              [name]: value
            }
          }
        }
      }
    });
  };

  const handleOnChangeDropdown = (event: DropdownBaseChangeEvent) => {
    const { name, value } = event.target;

    const action = formValues.config?.parameters?.value;
    const valueAction = formValues.config?.parameters?.action?.value;

    setValue(action, valueAction, name, value);
  };

  const isSelectedStandardCode =
    formValues?.config?.parameters?.action?.param?.standardCode?.code !==
      undefined &&
    formValues?.config?.parameters?.action?.param?.standardCode?.code !== '';
  const isPID194 = formValues.config?.parameters?.value === 'pid.194';
  const isNM65 = formValues.config?.parameters?.value === 'nm.65';
  const isPID200RUSH = formValues.config?.parameters?.value === 'pid.200.rush';
  const isPID200MAS = formValues.config?.parameters?.value === 'pid.200.mass';
  const activeMailCode =
    formValues?.config?.parameters?.action?.param?.isNM65MailCode === true;
  const isMailForm =
    formValues?.config?.parameters?.action?.param?.isNM65MailCode;
  const isMailInit =
    initialValues?.config?.parameters?.action?.param?.isNM65MailCode;

  useEffect(() => {
    setTitleCode(
      activeMailCode
        ? 'txt_force_emboss_replacement_cards_configuration_PID194_Active_Mail'
        : 'txt_force_emboss_replacement_cards_configuration_PID194'
    );
  }, [activeMailCode]);

  useEffect(() => {
    if (initialValues.config?.parameters?.transactionId) {
      handleOnSelectAction(
        'configure',
        initialValues.config?.parameters?.transactionId
      );
    }
  }, [initialValues, handleOnSelectAction]);

  useEffect(() => {
    let isValid = false;
    const config = formValues?.config?.parameters?.value;

    if (
      config === 'pid.194' ||
      config === 'pid.200.rush' ||
      config === 'pid.200.mass'
    ) {
      isValid =
        formValues?.config?.parameters?.action?.param?.standardCode?.code !==
        undefined;
    }

    if (formValues?.config?.parameters?.value === 'nm.65') {
      isValid = true;
    }

    if (formValues.isValid === isValid) return;
    keepRef.current.setFormValues({ isValid });
  }, [formValues]);

  useEffect(() => {
    setInitialValues(formValues);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [savedAt]);

  const handleCompareValues = () => {
    let isSame =
      formValues?.config?.parameters?.value !==
        initialValues?.config?.parameters?.value ||
      formValues?.config?.parameters?.dropdownValue !==
        initialValues?.config?.parameters?.dropdownValue;

    if (isPID194 && isMailInit !== undefined) {
      isSame = isMailForm !== isMailInit;
    }

    return isSame;
  };

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__FORCE_EMBOSS_REPLACEMENT_CARDS,
      priority: 1
    },
    [!!hasInstance && handleCompareValues()]
  );

  const unsaveOptions = useMemo(
    () => ({
      ...confirmEditWhenChangeEffectToUploadProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__FORCE_EMBOSS_REPLACEMENT_CARDS_CONFIRM_EDIT,
      priority: 1
    }),
    []
  );

  const handleResetUploadStep = () => {
    const file = dependencies?.files?.[0];
    const attachmentId = file?.idx;
    const isValid = file?.status === STATUS.valid;

    clearFormValues(clearFormName);
    isValid &&
      dispatch(actionsWorkflowSetup.deleteTemplateFile({ attachmentId }));
  };
  useUnsavedChangeRegistry(
    unsaveOptions,
    [!isEmpty(dependencies?.files) && handleCompareValues()],
    handleResetUploadStep
  );

  return (
    <div className="position-relative">
      <p className="mt-8 color-grey">
        {t('txt_force_emboss_replacement_cards_configuration_title_1')}
      </p>
      <p className="mt-8 color-grey">
        {t('txt_force_emboss_replacement_cards_configuration_title_2')}
      </p>
      <div className="pt-24 mt-24 border-top">
        {isStuck && (
          <InlineMessage className="mb-24" variant="danger" withIcon>
            {t('txt_step_stuck_move_forward_message')}
          </InlineMessage>
        )}
        <h5 className="mb-16">
          {t('txt_force_emboss_replacement_cards_configuration_select_action')}
        </h5>
        <div className="row">
          <div className="col-md-6">
            <div className="group-card">
              {configuration?.map((item: any, index: number) => {
                return (
                  <div
                    key={index}
                    className="group-card-item d-flex align-items-center px-16 py-18 custom-control-root"
                    onClick={() => handleOnSelectAction('configure', item)}
                  >
                    <span className="pr-8">{item.description}</span>
                    <Radio className="ml-auto mr-n4">
                      <Radio.Input
                        checked={
                          item.value ===
                          (formValues.config?.parameters?.value ||
                            initialValues?.config?.parameters?.transactionId)
                        }
                        className="checked-style"
                      />
                    </Radio>
                  </div>
                );
              })}
            </div>
          </div>

          {/* action right */}
          {isPID194 && (
            <div className="col-md-6">
              <div className="bg-light-l20 rounded-lg p-16 h-100 ">
                <InlineMessage variant="warning" withIcon className="mb-16">
                  <p className="color-orange-d16">
                    {t(
                      'txt_force_emboss_replacement_cards_configuration_option1_warning'
                    )}
                  </p>
                </InlineMessage>
                <p className="fw-600">
                  {t('txt_force_emboss_replacement_cards_configuration_PID194')}
                </p>
                <div className="border-dashed my-8"></div>
                <div className="d-flex align-items-center">
                  <p className="color-grey mr-8 fw-600">
                    {t(
                      'txt_force_emboss_replacement_cards_configuration_transaction'
                    )}
                  </p>
                  <Tooltip
                    element={
                      <div className="my-8">
                        <p>
                          {t(
                            'txt_force_emboss_replacement_cards_configuration_transaction_tooltip'
                          )}
                        </p>
                      </div>
                    }
                    triggerClassName="d-flex"
                  >
                    <Icon
                      name="information"
                      size="5x"
                      className="color-grey-l16"
                    />
                  </Tooltip>
                </div>
                <div className="d-flex mt-8">
                  {transaction?.map((item: any, index: number) => {
                    return (
                      <CheckBox key={index}>
                        <CheckBox.Input
                          id={'isNM65MailCode_id'}
                          name={'isNM65MailCode'}
                          disabled={false}
                          checked={
                            formValues?.config?.parameters?.action?.param
                              ?.isNM65MailCode
                          }
                          onChange={handleCheckBoxChange}
                        />
                        <CheckBox.Label>{item.description}</CheckBox.Label>
                      </CheckBox>
                    );
                  })}
                </div>
              </div>
            </div>
          )}

          {isNM65 && (
            <div className="col-md-6">
              <div className="bg-light-l20 rounded-lg p-16 h-100 ">
                <InlineMessage variant="warning" withIcon className="mb-16">
                  <p className="color-orange-d16">
                    {t(
                      'txt_force_emboss_replacement_cards_configuration_option2_warning'
                    )}
                  </p>
                </InlineMessage>
                <p className="fw-600">
                  {t('txt_force_emboss_replacement_cards_configuration_NM65')}
                </p>
              </div>
            </div>
          )}

          {isPID200RUSH && (
            <div className="col-md-6">
              <div className="bg-light-l20 rounded-lg p-16 h-100 ">
                <InlineMessage variant="info" withIcon className="mb-16">
                  <p className="color-cyan-d16">
                    {t(
                      'txt_force_emboss_replacement_cards_configuration_option3_warning'
                    )}
                  </p>
                </InlineMessage>
                <p className="fw-600">
                  {t(
                    'txt_force_emboss_replacement_cards_configuration_PID200_Rush'
                  )}
                </p>
              </div>
            </div>
          )}

          {isPID200MAS && (
            <div className="col-md-6">
              <div className="bg-light-l20 rounded-lg p-16 h-100 ">
                <InlineMessage variant="warning" withIcon className="mb-16">
                  <p className="color-orange-d16">
                    {t(
                      'txt_force_emboss_replacement_cards_configuration_option4_warning'
                    )}
                  </p>
                </InlineMessage>
                <p className="fw-600 color-grey-d20">
                  {t(
                    'txt_force_emboss_replacement_cards_configuration_PID200_Mass'
                  )}
                </p>
                <p className="pt-8">
                  {t(
                    'txt_force_emboss_replacement_cards_configuration_PID200_Mass_description'
                  )}
                </p>
              </div>
            </div>
          )}
        </div>
      </div>

      {isPID194 && (
        <PID194
          value={PID194Code}
          formValue={formValues}
          onchange={handleOnChangeDropdown}
          title={titleCode}
        />
      )}

      {isPID200RUSH && (
        <PID200RUSH
          value={PID200Code}
          formValue={formValues}
          onchange={handleOnChangeDropdown}
        />
      )}

      {isPID200MAS && (
        <PID200MASS
          value={PID200Code}
          formValue={formValues}
          onchange={handleOnChangeDropdown}
        />
      )}

      {isSelectedStandardCode && (
        <div className="pt-24 mt-24 border-top">
          <h5 className="mb-12">{t('txt_parameter_list')}</h5>
          <p className="color-grey">
            {t(
              'txt_force_emboss_replacement_cards_configuration_parameter_description'
            )}
          </p>
          <div className="mt-16">
            <ConfigureParametersStepGrid
              value={
                isPID200RUSH || isPID200MAS
                  ? PID200Transaction
                  : PID194Transaction
              }
              subValue={NM65Transaction}
              isNM={activeMailCode}
            />
          </div>
        </div>
      )}

      {isNM65 && (
        <div className="pt-24 mt-24 border-top">
          <h5 className="mb-12">{t('txt_parameter_list')}</h5>
          <p className="color-grey">
            {t(
              'txt_force_emboss_replacement_cards_configuration_parameter_description'
            )}
          </p>
          <div className="mt-16">
            <ConfigureParametersStepGrid value={NM65Transaction} />
          </div>
        </div>
      )}
    </div>
  );
};

const ExtraStaticConfigureParametersStep =
  ConfigureParametersStep as WorkflowSetupStaticProp<ConfigureParametersStepProps>;

ExtraStaticConfigureParametersStep.summaryComponent =
  ConfigureParametersStepSummary;
ExtraStaticConfigureParametersStep.defaultValues = {
  isValid: false,
  config: {}
};

ExtraStaticConfigureParametersStep.stepValue = StepValueFunc;
ExtraStaticConfigureParametersStep.parseFormValues = parseFormValues;

export default ExtraStaticConfigureParametersStep;
