import parseFormValues from './parseFormValues';

describe('WorkflowForceEmbossReplacementCards > ConfigureParametersStep > parseFormValues', () => {
  it('Should return undefined', () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '2'
            }
          ] as any
        }
      } as WorkflowSetupInstance,
      id: '1'
    };

    const response = parseFormValues(props.data, props.id, jest.fn);
    expect(response).toEqual(undefined);
  });

  it('Should return data >  value: PID194', () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '1'
            }
          ] as any,
          configurations: {
            value: 'pid.194'
          } as any
        }
      } as any,
      id: '1'
    };

    const response = parseFormValues(props.data, props.id, jest.fn);
    expect(response).toBeTruthy();
  });

  it('Should return data >  value: PID195', () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '1'
            }
          ] as any,
          configurations: {
            value: 'PID195'
          } as any
        }
      } as any,
      id: '1'
    };

    const response = parseFormValues(props.data, props.id, jest.fn);
    expect(response)?.toBeTruthy();
  });

  it('Should return data > have param', () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '1'
            }
          ] as any,
          configurations: {
            action: {
              value: '',
              param: {
                standardCode: {
                  code: '0',
                  text: '0 - Text'
                }
              }
            }
          } as any
        }
      } as any,
      id: '1'
    };

    const response = parseFormValues(props.data, props.id, jest.fn);
    expect(response)?.toBeTruthy();
  });
});
