import { WorkflowMetadataList } from 'app/fixtures/workflow-metadata';
import * as helper from 'app/helpers/isDevice';
import { mockUseDispatchFnc, renderWithMockStore } from 'app/utils';
import 'app/_libraries/_dls/test-utils/mocks/mockCanvas';
import React from 'react';
import PID200MASS from './PID200MASS';
jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/utils/MockView')
);
const mockUseDispatch = mockUseDispatchFnc();
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderComponent = (props: any) => {
  return renderWithMockStore(<PID200MASS {...props} />, {
    initialState: {
      workflowSetup: {
        elementMetadata: {
          elements: WorkflowMetadataList
        }
      }
    } as AppState
  });
};
const mockHelper: any = helper;
describe('ConfigureParametersStepSummary ', () => {
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => (() => {}) as any);
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
  });

  it('Should render PID200MASS > value', async () => {
    const props = {
      value: [
        {
          value: {
            code: '0',
            text: '0 - text'
          }
        }
      ]
    } as any;
    const wrapper = renderComponent(props);
    const text = (await wrapper).getByText('Code');
    expect(text).toBeInTheDocument;
  });

  it('Should render PID200_MASS > isDevice', async () => {
    mockHelper.isDevice = true;
  });

  it('Should render PID200_MASS > isDevice false', async () => {
    mockHelper.isDevice = false;
  });
});
