import { getWorkflowSetupStepStatus } from 'app/helpers';

const parseFormValues: WorkflowSetupStepFormDataFunc<any> = (data, id) => {
  const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);

  if (!stepInfo) return;

  return {
    ...getWorkflowSetupStepStatus(stepInfo),
    ...stepInfo.data
  };
};

export default parseFormValues;
