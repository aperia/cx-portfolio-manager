import EnhanceDropdownList from 'app/components/EnhanceDropdownList';
import { useFormValidations } from 'app/hooks';
import { DropdownBaseChangeEvent, useTranslation } from 'app/_libraries/_dls';
import React, { useMemo } from 'react';
import { ConfigureParametersStepProps, IFieldValidate } from '.';

interface PID200MASSProps {
  value?: any;
  formValue?: ConfigureParametersStepProps;
  onchange?: (event: DropdownBaseChangeEvent) => void;
}
const PID200MASS: React.FC<PID200MASSProps> = ({
  value,
  formValue,
  onchange
}) => {
  const { t } = useTranslation();
  const standardCodeVal =
    formValue?.config?.parameters?.action?.param?.standardCode?.code;
  const currentErrors = useMemo(
    () =>
      ({
        standardCode: !standardCodeVal?.trim() && {
          status: true,
          message: t('txt_required_validation', {
            fieldName: t('txt_force_emboss_replacement_cards_code')
          })
        }
      } as Record<keyof IFieldValidate, IFormError>),
    [standardCodeVal, t]
  );

  const [, errors, setTouched] =
    useFormValidations<keyof IFieldValidate>(currentErrors);

  const oldVal = value?.find((x: any) => x.value === standardCodeVal);

  return (
    <div className="pt-24 mt-24 border-top">
      <h5 className="mb-16">
        {t('txt_force_emboss_replacement_cards_configuration_PID200_Mass')}
      </h5>
      <div className="row mt-16">
        <div className="col-lg-4 col-md-6">
          <EnhanceDropdownList
            value={oldVal}
            onChange={onchange}
            label="Code"
            name="standardCode"
            required
            onFocus={setTouched('standardCode')}
            onBlur={setTouched('standardCode', true)}
            error={errors.standardCode}
            options={value}
            tooltipElement={
              <div>
                <div className="text-uppercase">
                  {t('txt_force_emboss_replacement_cards_code')}
                </div>
                <p className="mt-8">
                  {t(
                    'txt_force_emboss_replacement_cards_configuration_standardCode_tooltip'
                  )}
                </p>
              </div>
            }
          />
        </div>
      </div>
    </div>
  );
};

export default PID200MASS;
