import { ConfigureParametersStepProps } from '.';

const stepValueFunc: WorkflowSetupStepValueFunc<ConfigureParametersStepProps> =
  ({ stepsForm: formValues }) => {
    const { config } = formValues || {};
    const param = config?.parameters?.value || '';

    return param === 'pid.194'
      ? 'Standard Plastic Request (PID*194)'
      : param === 'nm.65'
      ? 'Update Account Level Mail Code (NM*65)'
      : param === 'pid.200.rush'
      ? 'Rush Plastic Request (PID*200)'
      : param === 'pid.200.mass'
      ? 'Mass Plastic Request (PID*200)'
      : '';
  };

export default stepValueFunc;
