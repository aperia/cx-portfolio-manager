/* eslint-disable react/display-name */
import { ColumnType, Grid } from 'app/_libraries/_dls';
import _ from 'lodash';
import { formatTruncate } from 'pages/_commons/Utils/formatGridField';
import React, { useEffect, useState } from 'react';
const columnData: ColumnType[] = [
  {
    id: 'fieldName',
    Header: 'Field Name',
    accessor: 'value',
    width: 211
  },
  {
    id: 'fieldType',
    Header: 'Field Type',
    accessor: 'type',
    width: 103
  },
  {
    id: 'description',
    Header: 'Description',
    accessor: formatTruncate(['description'])
  }
];

interface ConfigureParametersStepGridProps {
  value?: any;
  subValue?: any;
  isNM?: boolean;
}

const ConfigureParametersStepGrid: React.FC<ConfigureParametersStepGridProps> =
  ({ value, subValue = [], isNM }) => {
    const [columns, setColumns] = useState<ColumnType[]>([]);
    const newValue = value?.map((item: any) => ({
      ...item,
      value: `PID*194 - ${item.value}`
    }));
    const newSubValue = subValue?.map((item: any) => ({
      ...item,
      value: `NM*65 - ${item.value}`
    }));
    const allData = [...newValue, ...newSubValue];
    const lookup = allData.reduce((acc: any, curr: any) => {
      acc[curr.code] = ++acc[curr.code] || 0;
      return acc;
    }, {});

    const dupData = _.uniqBy(
      [...value, ...subValue].filter((item: any) => lookup[item.code]),
      'code'
    );

    const dupCode = dupData.map((item: any) => item.code);

    const uniqData: any[] = _.uniqBy(
      allData.filter((item: any) => !dupCode?.includes(item.code)),
      'code'
    );

    const gridData = isNM ? [...dupData, ...uniqData] : value;

    useEffect(() => {
      setColumns(columnData);
    }, []);

    return <Grid columns={columns} data={gridData} />;
  };

export default ConfigureParametersStepGrid;
