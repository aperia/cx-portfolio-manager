import { ConfigureParametersStepProps } from '.';
import stepValueFunc from './stepValueFunc';

describe('WorkflowForceEmbossReplacementCards > ConfigureParametersStep > stepValueFunc', () => {
  it('Should Return Check param PID194', () => {
    const stepsForm = {
      config: {
        parameters: {
          value: 'pid.194',
          action: {
            value: ''
          }
        }
      }
    } as ConfigureParametersStepProps;

    const response = stepValueFunc({ stepsForm });
    expect(response).toEqual('Standard Plastic Request (PID*194)');
  });

  it('Should Return Check param NM65', () => {
    const stepsForm = {
      config: {
        parameters: {
          value: 'nm.65',
          action: {
            value: ''
          }
        }
      }
    } as ConfigureParametersStepProps;

    const response = stepValueFunc({ stepsForm });
    expect(response).toEqual('Update Account Level Mail Code (NM*65)');
  });

  it('Should Return Check param PID200RUSH', () => {
    const stepsForm = {
      config: {
        parameters: {
          value: 'pid.200.rush',
          action: {
            value: ''
          }
        }
      }
    } as ConfigureParametersStepProps;

    const response = stepValueFunc({ stepsForm });
    expect(response).toEqual('Rush Plastic Request (PID*200)');
  });

  it('Should Return Check param PID200MASS', () => {
    const stepsForm = {
      config: {
        parameters: {
          value: 'pid.200.mass',
          action: {
            value: ''
          }
        }
      }
    } as ConfigureParametersStepProps;

    const response = stepValueFunc({ stepsForm });
    expect(response).toEqual('Mass Plastic Request (PID*200)');
  });

  it('Should Return Check param empty', () => {
    const stepsForm = {
      config: {
        parameters: {
          value: '',
          action: {
            value: ''
          }
        }
      }
    } as ConfigureParametersStepProps;

    const response = stepValueFunc({ stepsForm });
    expect(response).toEqual('');
  });
});
