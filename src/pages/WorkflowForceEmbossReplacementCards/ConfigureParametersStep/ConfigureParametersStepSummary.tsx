import GroupTextControl from 'app/components/DofControl/GroupTextControl';
import { Button, useTranslation } from 'app/_libraries/_dls';
import { isFunction } from 'app/_libraries/_dls/lodash';
import React from 'react';
import { ConfigureParametersStepProps } from '.';

const ConfigureParametersStepSummary: React.FC<
  WorkflowSetupSummaryProps<ConfigureParametersStepProps>
> = ({ formValues, onEditStep }) => {
  const { t } = useTranslation();
  const handleEdit = () => {
    isFunction(onEditStep) && onEditStep();
  };
  const isExistTransactionPlus =
    formValues.config?.parameters?.action?.param?.isNM65MailCode === true;
  const param = formValues.config?.parameters?.value;

  let actionText = '';
  let transactionText = '';
  let transactionTextPlus = '';
  let standardCode = '';

  switch (param) {
    case 'pid.194':
      actionText = t('txt_force_emboss_replacement_cards_name_PID194');
      transactionText = t(
        'txt_force_emboss_replacement_cards_configuration_PID194'
      );
      transactionTextPlus = t(
        'txt_force_emboss_replacement_cards_configuration_NM65'
      );

      standardCode =
        formValues.config?.parameters?.action?.param?.standardCode?.text || '';
      break;

    case 'nm.65':
      actionText = t('txt_force_emboss_replacement_cards_name_PID195');
      transactionText = t(
        'txt_force_emboss_replacement_cards_configuration_NM65'
      );
      break;

    case 'pid.200.rush':
      actionText = t('txt_force_emboss_replacement_cards_name_PID196');
      transactionText = t(
        'txt_force_emboss_replacement_cards_configuration_PID200_Rush'
      );
      standardCode =
        formValues.config?.parameters?.action?.param?.standardCode?.text || '';
      break;

    case 'pid.200.mass':
      actionText = t('txt_force_emboss_replacement_cards_name_PID197');
      transactionText = t(
        'txt_force_emboss_replacement_cards_configuration_PID200_Mass'
      );
      standardCode =
        formValues.config?.parameters?.action?.param?.standardCode?.text || '';
      break;
  }
  return (
    <div className="position-relative">
      <div className="absolute-top-right mt-n26 mr-n8">
        <Button variant="outline-primary" size="sm" onClick={handleEdit}>
          {t('txt_edit')}
        </Button>
      </div>
      <div className="row">
        <div className="col-12 col-lg-3">
          <GroupTextControl
            id="summary_actionText"
            input={{ value: actionText } as any}
            meta={{} as any}
            label="Action"
            options={{
              inline: false,
              lineTruncate: 2,
              ellipsisLessText: t('txt_less'),
              ellipsisMoreText: t('txt_more')
            }}
          />
        </div>
        <div className="col-12 col-lg-3">
          <GroupTextControl
            id="summary_transactionText"
            input={{ value: transactionText } as any}
            meta={{} as any}
            label="Transaction"
            options={{
              inline: false,
              lineTruncate: 2,
              ellipsisLessText: t('txt_less'),
              ellipsisMoreText: t('txt_more')
            }}
          />
        </div>
        {isExistTransactionPlus && param === 'pid.194' && (
          <div className="col-12 col-lg-3">
            <GroupTextControl
              id="summary_transactionTextPlus"
              input={{ value: transactionTextPlus } as any}
              meta={{} as any}
              label="Transaction"
              options={{
                inline: false,
                lineTruncate: 2,
                ellipsisLessText: t('txt_less'),
                ellipsisMoreText: t('txt_more')
              }}
            />
          </div>
        )}

        {(param === 'pid.194' ||
          param === 'pid.200.rush' ||
          param === 'pid.200.mass') && (
          <div className="col-12 col-lg-3">
            <GroupTextControl
              id="summary_standardCode"
              input={{ value: standardCode } as any}
              meta={{} as any}
              label="Code"
              options={{
                inline: false,
                lineTruncate: 2,
                ellipsisLessText: t('txt_less'),
                ellipsisMoreText: t('txt_more')
              }}
            />
          </div>
        )}
      </div>
    </div>
  );
};

export default ConfigureParametersStepSummary;
