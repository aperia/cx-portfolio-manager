import { fireEvent } from '@testing-library/react';
import { WorkflowMetadataList } from 'app/fixtures/workflow-metadata';
import { mockUseDispatchFnc, renderWithMockStore } from 'app/utils';
import React from 'react';
import ConfigureParametersStepSummary from './ConfigureParametersStepSummary';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/utils/MockView')
);
const mockUseDispatch = mockUseDispatchFnc();
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderComponent = (props: any) => {
  return renderWithMockStore(<ConfigureParametersStepSummary {...props} />, {
    initialState: {
      workflowSetup: {
        elementMetadata: {
          elements: WorkflowMetadataList
        }
      }
    } as AppState
  });
};

describe('ConfigureParametersStepSummary ', () => {
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => (() => {}) as any);
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
  });

  it('Should render ConfigureParametersStepSummary > PID194', async () => {
    const mockFunction = jest.fn();

    const props = {
      formValues: {
        config: {
          parameters: {
            value: 'pid.194',
            action: {
              value: '',
              param: ''
            }
          }
        }
      },
      onEditStep: mockFunction
    };
    const wrapper = await renderComponent(props);
    const editButton = wrapper.getByText('txt_edit');
    expect(editButton).toBeInTheDocument;
    fireEvent.click(editButton);
    expect(mockFunction).toHaveBeenCalled();
  });

  it('Should render ConfigureParametersStepSummary > PID194 and isNM65MailCode', async () => {
    const mockFunction = jest.fn();

    const props = {
      formValues: {
        config: {
          parameters: {
            value: 'pid.194',
            action: {
              value: '',
              param: {
                isNM65MailCode: true
              }
            }
          }
        }
      },
      onEditStep: mockFunction
    };
    const wrapper = await renderComponent(props);
    const editButton = wrapper.getByText('txt_edit');
    expect(editButton).toBeInTheDocument;
    fireEvent.click(editButton);
    expect(mockFunction).toHaveBeenCalled();
  });

  it('Should render ConfigureParametersStepSummary > NM65', async () => {
    const mockFunction = jest.fn();

    const props = {
      formValues: {
        config: {
          parameters: {
            value: 'nm.65',
            action: {
              value: '',
              param: ''
            }
          }
        }
      },
      onEditStep: mockFunction
    };
    const wrapper = await renderComponent(props);
    const editButton = wrapper.getByText('txt_edit');
    expect(editButton).toBeInTheDocument;
    fireEvent.click(editButton);
    expect(mockFunction).toHaveBeenCalled();
  });

  it('Should render ConfigureParametersStepSummary > default', async () => {
    const mockFunction = jest.fn();

    const props = {
      formValues: {
        config: {
          parameters: {
            value: '',
            action: {
              value: '',
              param: ''
            }
          }
        }
      },
      onEditStep: mockFunction
    };
    const wrapper = await renderComponent(props);
    const editButton = wrapper.getByText('txt_edit');
    expect(editButton).toBeInTheDocument;
    fireEvent.click(editButton);
    expect(mockFunction).toHaveBeenCalled();
  });

  it('Should render ConfigureParametersStepSummary > PID200RUSH', async () => {
    const mockFunction = jest.fn();

    const props = {
      formValues: {
        config: {
          parameters: {
            value: 'pid.200.rush',
            action: {
              value: '',
              param: ''
            }
          }
        }
      },
      onEditStep: mockFunction
    };
    const wrapper = await renderComponent(props);
    const editButton = wrapper.getByText('txt_edit');
    expect(editButton).toBeInTheDocument;
    fireEvent.click(editButton);
    expect(mockFunction).toHaveBeenCalled();
  });

  it('Should render ConfigureParametersStepSummary > PID200MASS', async () => {
    const mockFunction = jest.fn();

    const props = {
      formValues: {
        config: {
          parameters: {
            value: 'pid.200.mass',
            action: {
              value: '',
              param: ''
            }
          }
        }
      },
      onEditStep: mockFunction
    };
    const wrapper = await renderComponent(props);
    const editButton = wrapper.getByText('txt_edit');
    expect(editButton).toBeInTheDocument;
    fireEvent.click(editButton);
    expect(mockFunction).toHaveBeenCalled();
  });
});
