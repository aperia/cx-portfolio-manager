import { render, RenderResult } from '@testing-library/react';
import React from 'react';
import GettingStartSummary from './GettingStartSummary';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <GettingStartSummary {...props} />
    </div>
  );
};

describe('WorkflowForceEmbossReplacementCards > GettingStartStep > GettingStartSummary', () => {
  it('Should render summary contains Completed', () => {
    const wrapper = renderComponent({
      formValues: {
        returnedCheckCharges: true
      }
    } as WorkflowSetupSummaryProps);

    expect(wrapper.getByText('txt_force_emboss_replacement_cards_complete'))
      .toBeInTheDocument;
  });
});
