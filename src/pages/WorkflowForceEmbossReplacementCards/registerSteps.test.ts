import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { default as stepRegistry } from '../_commons/WorkflowSetup/registries/stepRegistry';

const registerFunc = jest.spyOn(stepRegistry, 'registerStep');

describe('WorkflowForceEmbossReplacementCards > registerSteps', () => {
  it('Should register components', async () => {
    await import('./registerSteps');

    expect(registerFunc).toBeCalledWith(
      'GetStartedForceEmbossReplacementCardsWorkflow',
      expect.anything()
    );

    expect(registerFunc).toBeCalledWith(
      'ConfigureParametersForceEmbossReplacementCardsWorkflow',
      expect.anything(),
      [UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__FORCE_EMBOSS_REPLACEMENT_CARDS]
    );
  });
});
