import { fireEvent } from '@testing-library/react';
import {
  WorkflowSection,
  WorkflowTemplateFilterByFields,
  WorkflowTemplateType
} from 'app/constants/enums';
import { changeDlsDropdownMock, renderComponent } from 'app/utils';
import 'app/utils/_mockComponent/mockBreadCrumb';
import 'app/utils/_mockComponent/mockFailedApiReload';
import 'app/utils/_mockComponent/mockNoDataFound';
import 'app/utils/_mockComponent/mockPaging';
import 'app/utils/_mockComponent/mockSortAndOrder';
import * as CommonRedux from 'pages/_commons/redux/Common/select-hooks';
import { actionsWorkflow } from 'pages/_commons/redux/Workflow';
import { defaultTemplateDataFilter } from 'pages/_commons/redux/Workflow/reducers';
import * as WorkflowRedux from 'pages/_commons/redux/Workflow/select-hooks';
import React from 'react';
import * as ReactRedux from 'react-redux';
import * as ReactRouterDom from 'react-router-dom';
import TemplateWorkflows from '.';

jest.mock('app/components/SimpleSearch', () => ({
  __esModule: true,
  default: ({ onSearch }: any) => (
    <div onClick={() => onSearch('Simple search value')}>Simple search</div>
  )
}));
jest.mock('app/components/SimpleSearchSmall', () => ({
  __esModule: true,
  default: ({ onSearch }: any) => (
    <div onClick={() => onSearch('Simple search small value')}>
      Simple search small
    </div>
  )
}));
jest.mock('app/components/WorkflowTemplateCardView', () => ({
  __esModule: true,
  default: ({ value }: any) => <div>{value.name}</div>
}));
jest.mock('pages/_commons/Utils/ClearAndResetButton', () => ({
  __esModule: true,
  default: ({ onClearAndReset }: any) => (
    <button onClick={onClearAndReset}>{'Clear And Reset'}</button>
  )
}));
jest.mock('app/_libraries/_dls', () => {
  const dlsActual = jest.requireActual('app/_libraries/_dls');
  return {
    ...dlsActual,
    Button: ({ onClick, children }: any) => (
      <button onClick={onClick}>{children}</button>
    ),
    SimpleBar: ({ children }: any) => (
      <div id="mock-simple-bar">{children}</div>
    ),
    useTranslation: () => ({
      t: (key: string) => key
    })
  };
});
jest.mock('react-router-dom', () => {
  return {
    __esModule: true,
    useLocation: null
  };
});

jest.mock('pages/ChangeDetail/Modals', () => {
  const actualModule = jest.requireActual('pages/ChangeDetail/Modals');
  return {
    __esModule: true,
    ...actualModule,
    SetReminderModal: ({ onClose }: any) => {
      return <div onClick={() => onClose()}>On Close Button</div>;
    }
  };
});

const mockReactRouter: any = ReactRouterDom;

const mockSelectWindow = jest.spyOn(CommonRedux, 'useSelectWindowDimension');
const mockSelectFavorite = jest.spyOn(
  WorkflowRedux,
  'useSelectFavoriteWorkflowTemplate'
);
const mockSelectPageData = jest.spyOn(
  WorkflowRedux,
  'useSelectPageWorkflowTemplates'
);
const mockSelectFilter = jest.spyOn(
  WorkflowRedux,
  'useSelectPageWorkflowTemplatesFilter'
);
const mockSelectType = jest.spyOn(
  WorkflowRedux,
  'useSelectPageWorkflowTemplateTypes'
);
const mockSelectUnfavorite = jest.spyOn(
  WorkflowRedux,
  'useSelectUnfavoriteWorkflowTemplate'
);
const mockUseDispatch = jest.spyOn(ReactRedux, 'useDispatch');
const testId = 'workflows';

describe('Page > Workflows', () => {
  const dispatchFn = jest.fn();
  beforeEach(() => {
    mockSelectWindow.mockReturnValue({ width: 1200, height: 1200 });
    mockSelectFavorite.mockReturnValue({ loading: false });
    mockSelectUnfavorite.mockReturnValue({ loading: false });
    mockSelectPageData.mockReturnValue({
      workflowTemplates: [
        { name: 'Mock template 1' },
        { name: 'Mock template 2' }
      ] as any[],
      changeTypeList: [
        WorkflowTemplateType.ACCOUNT_MANAGEMENT,
        WorkflowTemplateType.CORRESPONDENCE,
        WorkflowTemplateType.FILE_MANAGEMENT
      ],
      loading: false,
      error: false,
      total: 25
    });
    mockSelectFilter.mockReturnValue(defaultTemplateDataFilter);
    mockSelectType.mockReturnValue([
      {
        id: '1',
        label: 'Mock type 1',
        count: 10,
        description: 'Mock type 1 description'
      },
      {
        id: '2',
        label: 'Mock type 2',
        count: 15,
        description: 'Mock type 2 description'
      }
    ]);
    mockUseDispatch.mockReturnValue(dispatchFn);
    mockReactRouter.useLocation = () => ({
      pathname: '/workflows/favorite'
    });
  });
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Should render Workflow template UI with data, filter and paging', async () => {
    const { getByText } = await renderComponent(testId, <TemplateWorkflows />);
    expect(getByText('Workflows')).toBeInTheDocument();
    expect(getByText('Mock BreadCrumb')).toBeInTheDocument();
    expect(getByText('Mock type 1')).toBeInTheDocument();
    expect(getByText('Simple search')).toBeInTheDocument();
    expect(getByText('Page Size 25')).toBeInTheDocument();
  });
  it('Should render Workflow template UI loading', async () => {
    mockSelectPageData.mockReturnValue({
      workflowTemplates: [] as any[],
      changeTypeList: [],
      loading: true,
      error: false,
      total: 0
    });
    mockSelectFilter.mockReturnValue({});
    mockReactRouter.useLocation = () => ({
      pathname: '/workflows'
    });
    const { getByTestId } = await renderComponent(
      testId,
      <TemplateWorkflows />
    );

    expect(
      getByTestId(testId).querySelector('#mock-simple-bar > div.loading')
    ).toBeInTheDocument();
  });

  it('Should render Workflow template UI > No data', async () => {
    mockSelectPageData.mockReturnValue({
      workflowTemplates: [] as any[],
      changeTypeList: [],
      loading: false,
      error: false,
      total: 0
    });
    mockSelectFilter.mockReturnValue({
      ...defaultTemplateDataFilter,
      currentSection: WorkflowSection.FAVORITE_WORKFLOWS,
      searchValues: {
        favoriteWorkflows: 'mock value'
      }
    });
    const { getByText } = await renderComponent(testId, <TemplateWorkflows />);

    expect(getByText('No data found')).toBeInTheDocument();
    fireEvent.click(getByText('No data found'));
    expect(dispatchFn).toHaveBeenCalled();
  });

  it('Should render Workflow template UI > Error api', async () => {
    mockSelectPageData.mockReturnValue({
      workflowTemplates: [] as any[],
      changeTypeList: [],
      loading: false,
      error: true,
      total: 0
    });
    mockSelectFilter.mockReturnValue({
      ...defaultTemplateDataFilter,
      currentSection: 'mock section not exist' as any
    });
    const { getByText } = await renderComponent(testId, <TemplateWorkflows />);
    expect(getByText('Data load unsuccessful')).toBeInTheDocument();
    fireEvent.click(getByText('Reload'));
    expect(dispatchFn).toHaveBeenCalled();
  });

  it('Should render Workflow template UI > Title And Filter ', async () => {
    // mock screen width = 786
    mockSelectWindow.mockReturnValue({ width: 768, height: 1200 });
    // mock filter currentSection to myWorkflows
    mockSelectFilter.mockReturnValue({
      ...defaultTemplateDataFilter,
      currentSection: WorkflowSection.MY_WORKFLOWS,
      searchValues: {
        myWorkflows: 'mock value'
      }
    });
    const { getByText } = await renderComponent(testId, <TemplateWorkflows />);
    expect(getByText('Simple search small')).toBeInTheDocument();

    // Mock simple search change value
    fireEvent.click(getByText('Simple search small'));
    expect(dispatchFn).toHaveBeenCalled();

    // Mock clear and reset button clicked
    fireEvent.click(getByText('Clear And Reset'));
    expect(dispatchFn).toHaveBeenCalled();
  });

  it('Should render Workflow template UI > My Workflow > change type ', async () => {
    // mock filter currentSection to myWorkflows
    mockSelectFilter.mockReturnValue({
      ...defaultTemplateDataFilter,
      currentSection: WorkflowSection.MY_WORKFLOWS,
      searchValues: {
        myWorkflows: 'mock value'
      }
    });
    const { getByTestId } = await renderComponent(
      testId,
      <TemplateWorkflows />
    );

    // Mock change type dropdown
    changeDlsDropdownMock(0, 2, getByTestId(testId));
    expect(dispatchFn).toHaveBeenCalled();
  });

  it('Should render Workflow template UI > Favorite Workflow > change type ', async () => {
    // mock filter currentSection to myWorkflows
    mockSelectFilter.mockReturnValue({
      ...defaultTemplateDataFilter,
      currentSection: WorkflowSection.FAVORITE_WORKFLOWS,
      searchValues: {
        favoriteWorkflows: 'mock value'
      }
    });
    const { getByTestId } = await renderComponent(
      testId,
      <TemplateWorkflows />
    );

    // Mock change type dropdown
    changeDlsDropdownMock(0, 2, getByTestId(testId));
    expect(dispatchFn).toHaveBeenCalled();
  });

  it('Should render Workflow template UI > Menu filter clicked ', async () => {
    const { getByText, getAllByText } = await renderComponent(
      testId,
      <TemplateWorkflows />
    );
    // Mock click on All Workflows filter
    fireEvent.click(getAllByText('txt_all_workflows')[0]);
    expect(dispatchFn).toHaveBeenCalledWith(
      actionsWorkflow.updatePageWorkflowsFilter({
        page: 1,
        filterValue: {},
        currentSection: WorkflowSection.ALL
      })
    );
    // Mock click on My Workflows filter
    fireEvent.click(getByText('txt_my_workflows'));
    expect(dispatchFn).toHaveBeenCalledWith(
      actionsWorkflow.updatePageWorkflowsFilter({
        page: 1,
        myWorkflowFilterValue: {
          [WorkflowTemplateFilterByFields.ACCESSIBLE]: true
        },
        currentSection: WorkflowSection.MY_WORKFLOWS
      })
    );
    // Mock click on Favorite Workflows filter
    fireEvent.click(getByText('txt_favorite_workflows'));
    expect(dispatchFn).toHaveBeenCalledWith(
      actionsWorkflow.updatePageWorkflowsFilter({
        page: 1,
        favoriteWorkflowFilterValue: {
          [WorkflowTemplateFilterByFields.ACCESSIBLE]: true,
          [WorkflowTemplateFilterByFields.IS_FAVORITE]: true
        },
        currentSection: WorkflowSection.FAVORITE_WORKFLOWS
      })
    );
    // Mock click on Workflows type filter = Mock type 2
    fireEvent.click(getByText('Mock type 2'));
    expect(dispatchFn).toHaveBeenCalled();
  });

  it('Should render Workflow template UI > sort by, order by, pagination', async () => {
    const { getByText } = await renderComponent(testId, <TemplateWorkflows />);
    // Mock sort by
    fireEvent.click(getByText('Change to field1'));
    const { sortBys, orderBys, currentSection } = defaultTemplateDataFilter;
    expect(dispatchFn).toHaveBeenCalledWith(
      actionsWorkflow.updatePageWorkflowsFilter({
        sortBys: { ...sortBys, [currentSection as any]: ['field1'] }
      })
    );
    // Mock order by
    fireEvent.click(getByText('Change to asc'));
    expect(dispatchFn).toHaveBeenCalledWith(
      actionsWorkflow.updatePageWorkflowsFilter({
        orderBys: { ...orderBys, [currentSection as any]: ['asc'] }
      })
    );
    // Mock pagination
    fireEvent.click(getByText('Page 2'));
    expect(dispatchFn).toHaveBeenCalledWith(
      actionsWorkflow.updatePageWorkflowsFilter({
        page: 2
      })
    );
    fireEvent.click(getByText('Page Size 25'));
    expect(dispatchFn).toHaveBeenCalledWith(
      actionsWorkflow.updatePageWorkflowsFilter({
        pageSize: 25,
        page: 1
      })
    );

    fireEvent.click(getByText('txt_workflow_set_reminder'));
    fireEvent.click(getByText('On Close Button'));
  });
});
