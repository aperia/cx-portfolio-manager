import BreadCrumb from 'app/components/BreadCrumb';
import FailedApiReload from 'app/components/FailedApiReload';
import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import SimpleSearchSmall from 'app/components/SimpleSearchSmall';
import WorkflowTemplateCardView from 'app/components/WorkflowTemplateCardView';
import {
  WORKFLOWS_TEMPLATE_SORT_BY_LIST,
  WORKFLOW_FILTER_TYPE_FIELDS
} from 'app/constants/constants';
import {
  WorkflowSection,
  WorkflowTemplateFilterByFields
} from 'app/constants/enums';
import { BASE_URL } from 'app/constants/links';
import { classnames } from 'app/helpers';
import { IWorkflowPageFilter } from 'app/types';
import {
  Button,
  DropdownBaseChangeEvent,
  DropdownList,
  SimpleBar,
  useTranslation
} from 'app/_libraries/_dls';
import includes from 'lodash.includes';
import { SetReminderModal } from 'pages/ChangeDetail/Modals';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import {
  actionsWorkflow,
  useSelectFavoriteWorkflowTemplate,
  useSelectPageWorkflowTemplates,
  useSelectPageWorkflowTemplatesFilter,
  useSelectPageWorkflowTemplateTypes,
  useSelectUnfavoriteWorkflowTemplate,
  WorkflowTemplateType
} from 'pages/_commons/redux/Workflow';
import { defaultTemplateDataFilter } from 'pages/_commons/redux/Workflow/reducers';
import Paging from 'pages/_commons/Utils/Paging';
import SortOrder from 'pages/_commons/Utils/SortOrder';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch } from 'react-redux';
import { Link, useLocation } from 'react-router-dom';

const breadCrmbItems = [
  { id: 'home', title: <Link to={BASE_URL}>Home</Link> },
  { id: 'workflows', title: 'Workflows' }
];

interface IWorkflowsProps {}
const TemplateWorkflows: React.FC<IWorkflowsProps> = () => {
  const { width } = useSelectWindowDimension();
  const { pathname } = useLocation();
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const defaultSection = pathname.includes('favorite')
    ? WorkflowSection.FAVORITE_WORKFLOWS
    : WorkflowSection.ALL;
  const { workflowTemplates, loading, error, total, changeTypeList } =
    useSelectPageWorkflowTemplates();
  const {
    page = 1,
    pageSize = 10,
    sortBy: sortByDefault,
    orderBy: orderByDefault,
    myWorkflowFilterValue = {},
    favoriteWorkflowFilterValue = {},
    currentSection = WorkflowSection.ALL,
    searchValues = {},
    orderBys = {},
    sortBys = {}
  } = useSelectPageWorkflowTemplatesFilter();
  const sortBy = useMemo(
    () => sortBys[currentSection] ?? sortByDefault,
    [sortBys, currentSection, sortByDefault]
  );
  const orderBy = useMemo(
    () => orderBys[currentSection] ?? orderByDefault,
    [orderBys, currentSection, orderByDefault]
  );
  const dataFilter = useMemo(() => ({ sortBy, orderBy }), [sortBy, orderBy]);

  const searchValue = searchValues[currentSection];
  const filterDropdownValue =
    currentSection === WorkflowSection.FAVORITE_WORKFLOWS
      ? favoriteWorkflowFilterValue[WorkflowTemplateFilterByFields.TYPE]
      : currentSection === WorkflowSection.MY_WORKFLOWS
      ? myWorkflowFilterValue[WorkflowTemplateFilterByFields.TYPE]
      : null;
  const hasFilterToClear = searchValue || filterDropdownValue;
  const workflowTemplateTypes = useSelectPageWorkflowTemplateTypes();
  const filterWorkflowCategoryButtons: WorkflowTemplateType[] = useMemo(
    () => [
      {
        id: WorkflowSection.ALL,
        label: t('txt_all_workflows'),
        description: t('txt_all_available_workflows')
      },
      {
        id: WorkflowSection.MY_WORKFLOWS,
        label: t('txt_my_workflows'),
        description: t('txt_workflows_i_have_access_to')
      },
      {
        id: WorkflowSection.FAVORITE_WORKFLOWS,
        label: t('txt_favorite_workflows'),
        description: t('txt_workflows_I_have_marked_as_my_favorites')
      }
    ],
    [t]
  );
  const filterWorkflow = [
    ...filterWorkflowCategoryButtons,
    ...workflowTemplateTypes
  ].find(w => w.id === currentSection);
  const { label, description } = filterWorkflow || {};

  const { loading: favoriting } = useSelectFavoriteWorkflowTemplate();
  const { loading: unfavoriting } = useSelectUnfavoriteWorkflowTemplate();

  const [reminderModalOpen, setReminderModalOpen] = useState<boolean>(false);

  // sort by
  const handleChangeSortBy = useCallback(
    (sortByValue: string) => {
      dispatch(
        actionsWorkflow.updatePageWorkflowsFilter({
          sortBys: { ...sortBys, [currentSection]: [sortByValue] }
        })
      );
    },
    [dispatch, currentSection, sortBys]
  );

  // order by
  const handleChangeOrderBy = useCallback(
    (orderByValue: string) => {
      dispatch(
        actionsWorkflow.updatePageWorkflowsFilter({
          orderBys: {
            ...orderBys,
            [currentSection]: [orderByValue as OrderByType]
          }
        })
      );
    },
    [dispatch, orderBys, currentSection]
  );

  // pagination
  const handleChangePage = useCallback(
    (pageNumber: number) => {
      dispatch(
        actionsWorkflow.updatePageWorkflowsFilter({
          page: pageNumber
        })
      );
    },
    [dispatch]
  );

  const handleChangePageSize = useCallback(
    (pageSizeNumber: number) => {
      dispatch(
        actionsWorkflow.updatePageWorkflowsFilter({
          pageSize: pageSizeNumber,
          page: 1
        })
      );
    },
    [dispatch]
  );

  const handleSearch = useCallback(
    (val: string) => {
      dispatch(
        actionsWorkflow.updatePageWorkflowsFilter({
          page: 1,
          searchValues: { ...searchValues, [currentSection]: val }
        })
      );
    },
    [dispatch, currentSection, searchValues]
  );

  const handleFilterWorkflowMenu = useCallback(
    item => {
      const { id } = item;
      switch (id) {
        case WorkflowSection.ALL:
          dispatch(
            actionsWorkflow.updatePageWorkflowsFilter({
              page: 1,
              filterValue: {},
              currentSection: id
            })
          );
          break;
        case WorkflowSection.MY_WORKFLOWS:
          dispatch(
            actionsWorkflow.updatePageWorkflowsFilter({
              page: 1,
              myWorkflowFilterValue: {
                ...myWorkflowFilterValue,
                [WorkflowTemplateFilterByFields.ACCESSIBLE]: true
              },
              currentSection: id
            })
          );
          break;
        case WorkflowSection.FAVORITE_WORKFLOWS:
          dispatch(
            actionsWorkflow.updatePageWorkflowsFilter({
              page: 1,
              favoriteWorkflowFilterValue: {
                ...favoriteWorkflowFilterValue,
                [WorkflowTemplateFilterByFields.ACCESSIBLE]: true,
                [WorkflowTemplateFilterByFields.IS_FAVORITE]: true
              },
              currentSection: id
            })
          );
          break;
        default:
          dispatch(
            actionsWorkflow.updatePageWorkflowsFilter({
              page: 1,
              filterValue: { [WorkflowTemplateFilterByFields.TYPE]: id },
              currentSection: id
            })
          );
      }
    },
    [dispatch, myWorkflowFilterValue, favoriteWorkflowFilterValue]
  );

  const handleFilterWorkflowType = useCallback(
    (e: DropdownBaseChangeEvent) => {
      const typeFilter = {
        [WorkflowTemplateFilterByFields.TYPE]: e.target.value.value
      };
      let _pageFilter: IWorkflowPageFilter = {};
      switch (currentSection) {
        case WorkflowSection.MY_WORKFLOWS:
          _pageFilter = {
            myWorkflowFilterValue: { ...myWorkflowFilterValue, ...typeFilter }
          };
          break;
        case WorkflowSection.FAVORITE_WORKFLOWS:
          _pageFilter = {
            favoriteWorkflowFilterValue: {
              ...favoriteWorkflowFilterValue,
              ...typeFilter
            }
          };
          break;
      }
      dispatch(
        actionsWorkflow.updatePageWorkflowsFilter({
          page: 1,
          ..._pageFilter
        })
      );
    },
    [
      dispatch,
      myWorkflowFilterValue,
      favoriteWorkflowFilterValue,
      currentSection
    ]
  );

  // clear and reset
  const handleClearAndReset = useCallback(() => {
    let _pageFilter: IWorkflowPageFilter = {};
    switch (currentSection) {
      case WorkflowSection.MY_WORKFLOWS:
        _pageFilter = {
          myWorkflowFilterValue: {
            ...myWorkflowFilterValue,
            [WorkflowTemplateFilterByFields.TYPE]: null
          }
        };
        break;
      case WorkflowSection.FAVORITE_WORKFLOWS:
        _pageFilter = {
          favoriteWorkflowFilterValue: {
            ...favoriteWorkflowFilterValue,
            [WorkflowTemplateFilterByFields.TYPE]: null
          }
        };
        break;
    }
    dispatch(
      actionsWorkflow.updatePageWorkflowsFilter({
        page: 1,
        ..._pageFilter,
        searchValues: { ...searchValues, [currentSection]: '' }
      })
    );
  }, [
    dispatch,
    searchValues,
    currentSection,
    favoriteWorkflowFilterValue,
    myWorkflowFilterValue
  ]);

  const handleApiReload = useCallback(() => {
    dispatch(actionsWorkflow.getPageTemplateWorkflows());
  }, [dispatch]);

  useEffect(() => {
    dispatch(actionsWorkflow.getPageTemplateWorkflows());
    return () => {
      dispatch(actionsWorkflow.resetPageWorkflowsFilterToDefault());
    };
  }, [dispatch]);

  useEffect(() => {
    defaultSection === WorkflowSection.FAVORITE_WORKFLOWS &&
      dispatch(
        actionsWorkflow.updatePageWorkflowsFilter({
          currentSection: WorkflowSection.FAVORITE_WORKFLOWS,
          favoriteWorkflowFilterValue: {
            [WorkflowTemplateFilterByFields.ACCESSIBLE]: true,
            [WorkflowTemplateFilterByFields.IS_FAVORITE]: true
          }
        })
      );
  }, [defaultSection, dispatch]);

  const changeTypeListDynamic = useMemo(() => {
    const list = WORKFLOW_FILTER_TYPE_FIELDS.filter(i =>
      includes(changeTypeList, i.value)
    );
    return [WORKFLOW_FILTER_TYPE_FIELDS[0], ...list];
  }, [changeTypeList]);

  const header = useMemo(
    () => (
      <div className="workflows-header py-24 pl-24 pr-16 bg-light-l20 border-bottom">
        <div className="mb-16">
          <BreadCrumb
            id={'workflows__breadcrumb'}
            items={breadCrmbItems}
            width={992}
          />
        </div>
        <div className="d-flex d-flex justify-content-between  align-items-center">
          <h4>Workflows</h4>
          <Button
            variant="outline-primary"
            size="sm"
            onClick={() => setReminderModalOpen(true)}
          >
            {t('txt_workflow_set_reminder')}
          </Button>
        </div>
      </div>
    ),
    [t]
  );

  const menuItem = useCallback(
    (button: WorkflowTemplateType) => (
      <div className="workflow-menu-item py-4" key={button.id}>
        <Button
          variant={currentSection === button.id ? 'primary' : 'outline-primary'}
          className={classnames(
            'w-100 br-radius-8 text-left px-16 py-8 fs-14 fw-600',
            currentSection !== button.id && 'color-secondary'
          )}
          onClick={() => handleFilterWorkflowMenu(button)}
        >
          <div className="d-flex justify-content-between">
            <span>{button.label}</span>
            <span className="ml-4">{button.count}</span>
          </div>
        </Button>
      </div>
    ),
    [currentSection, handleFilterWorkflowMenu]
  );

  const sidebar = useMemo(
    () => (
      <div className="workflows-sidebar bg-light-l20 border-right px-8 py-24">
        <div className="mb-24">
          {filterWorkflowCategoryButtons.map(button => menuItem(button))}
        </div>
        <h5 className="px-16 mb-8">Workflow Type</h5>
        <div>{workflowTemplateTypes.map(button => menuItem(button))}</div>
      </div>
    ),
    [workflowTemplateTypes, menuItem, filterWorkflowCategoryButtons]
  );

  const titleAndFilter = useMemo(
    () => (
      <div className="content-head">
        <div className="d-flex justify-content-between align-items-center pb-16">
          <h4>{label}</h4>
          {(total > 0 || hasFilterToClear) && (
            <div>
              {width > 1040 ? (
                <SimpleSearch
                  placeholder={t(
                    'txt_type_to_search_by_workflow_name_and_desc'
                  )}
                  onSearch={handleSearch}
                  defaultValue={searchValue}
                />
              ) : (
                <div className="mr-n16">
                  <SimpleSearchSmall
                    placeholder={t(
                      'txt_type_to_search_by_workflow_name_and_desc'
                    )}
                    maxLength={200}
                    onSearch={handleSearch}
                    defaultValue={searchValue}
                  />
                </div>
              )}
            </div>
          )}
        </div>
        <div className="d-flex justify-content-between align-items-center">
          <div className="d-flex flex-column">
            <div>{description}</div>
            {(currentSection === WorkflowSection.FAVORITE_WORKFLOWS ||
              currentSection === WorkflowSection.MY_WORKFLOWS) &&
              (total > 0 || hasFilterToClear) && (
                <div className="d-flex align-items-center pr-24 mt-16">
                  <strong className="fs-14 color-grey mr-4">Type:</strong>
                  <DropdownList
                    name="type"
                    textField="description"
                    value={
                      WORKFLOW_FILTER_TYPE_FIELDS.find(
                        item => item.value === filterDropdownValue
                      ) || WORKFLOW_FILTER_TYPE_FIELDS[0]
                    }
                    onChange={handleFilterWorkflowType}
                    variant="no-border"
                  >
                    {changeTypeListDynamic.map((item, idx) => (
                      <DropdownList.Item
                        key={idx}
                        label={item.description}
                        value={item}
                      />
                    ))}
                  </DropdownList>
                </div>
              )}
          </div>
          {total > 0 && (
            <div className="d-flex align-self-end">
              <SortOrder
                small={
                  searchValue || hasFilterToClear
                    ? width <= 1175
                    : width <= 1040
                }
                hasFilter={!!searchValue || hasFilterToClear}
                dataFilter={dataFilter}
                defaultDataFilter={defaultTemplateDataFilter}
                sortByFields={WORKFLOWS_TEMPLATE_SORT_BY_LIST}
                onChangeOrderBy={handleChangeOrderBy}
                onChangeSortBy={handleChangeSortBy}
                onClearSearch={handleClearAndReset}
              />
            </div>
          )}
        </div>
      </div>
    ),
    [
      t,
      handleChangeOrderBy,
      handleChangeSortBy,
      handleSearch,
      searchValue,
      handleClearAndReset,
      total,
      description,
      label,
      handleFilterWorkflowType,
      currentSection,
      width,
      filterDropdownValue,
      hasFilterToClear,
      changeTypeListDynamic,
      dataFilter
    ]
  );

  const workflowTemplateCardList = useMemo(
    () =>
      !loading &&
      total > 0 && (
        <React.Fragment>
          <div className="row mt-4">
            {workflowTemplates.map(item => (
              <div key={item.id} className="col-lg-12 col-xl-6">
                <WorkflowTemplateCardView
                  key={item.id}
                  id={`page_${item.id}`}
                  value={item}
                />
              </div>
            ))}
          </div>
          <Paging
            page={page}
            pageSize={pageSize}
            totalItem={total}
            onChangePage={handleChangePage}
            onChangePageSize={handleChangePageSize}
          />
        </React.Fragment>
      ),
    [
      workflowTemplates,
      page,
      pageSize,
      total,
      handleChangePage,
      handleChangePageSize,
      loading
    ]
  );

  const noDataFound = useMemo(
    () =>
      !loading &&
      !error &&
      total === 0 && (
        <div className="d-flex flex-column justify-content-center mt-80 mb-24">
          <NoDataFound
            id="all-workflow-NoDataFound"
            title={t('txt_no_workflows_to_display')}
            hasSearch={!!searchValue}
            hasFilter={filterDropdownValue}
            linkTitle={hasFilterToClear && t('txt_clear_and_reset')}
            onLinkClicked={handleClearAndReset}
          />
        </div>
      ),
    [
      handleClearAndReset,
      t,
      loading,
      error,
      total,
      hasFilterToClear,
      filterDropdownValue,
      searchValue
    ]
  );
  const failedReload = useMemo(
    () => (
      <div>
        {error && (
          <FailedApiReload
            id="all-workflows-error"
            onReload={handleApiReload}
            className="mt-40 pt-40 d-flex flex-column justify-content-center align-items-center"
          />
        )}
      </div>
    ),
    [error, handleApiReload]
  );

  return (
    <SimpleBar className="h-100 content-h-100">
      <div
        className={classnames(
          'h-100 d-flex flex-column',
          (loading || favoriting || unfavoriting) && 'loading'
        )}
      >
        {header}
        <div className="d-flex flex-fill">
          {sidebar}
          <div className="workflow-content flex-fill px-24 pt-24 pb-16 h-100">
            {titleAndFilter}
            {workflowTemplateCardList}
            {noDataFound}
            {failedReload}
          </div>
        </div>
      </div>
      {reminderModalOpen && (
        <SetReminderModal
          id="setReminderModal"
          show={reminderModalOpen}
          onClose={() => setReminderModalOpen(false)}
        />
      )}
    </SimpleBar>
  );
};

export default TemplateWorkflows;
