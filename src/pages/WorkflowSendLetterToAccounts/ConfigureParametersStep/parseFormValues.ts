import { SendLetterParameterEnum } from 'app/constants/enums';
import { getWorkflowSetupStepStatus } from 'app/helpers';

const parseFormValues: WorkflowSetupStepFormDataFunc<any> = (data, id) => {
  const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);

  const configureParameters = ((data?.data as any)?.configurations ||
    {}) as WorkflowSetupConfigureParameters;
  if (!stepInfo) return;

  const isValid =
    !!configureParameters?.[SendLetterParameterEnum.LetterNumber] &&
    !!configureParameters?.[SendLetterParameterEnum.Addressing];

  return {
    ...getWorkflowSetupStepStatus(stepInfo),
    ...stepInfo.data,
    isValid,
    configureParameters
  };
};

export default parseFormValues;
