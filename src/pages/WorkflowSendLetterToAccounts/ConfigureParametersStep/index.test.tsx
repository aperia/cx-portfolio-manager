import { act, fireEvent, RenderResult, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { SendLetterParameterEnum } from 'app/constants/enums';
import { LettersDataState } from 'app/types';
import { mockUseDispatchFnc, renderWithMockStore } from 'app/utils';
import React from 'react';
import ConfigureParametersStep from '.';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/utils/MockView')
);
const mockUseDispatch = mockUseDispatchFnc();
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});
jest.mock('app/hooks/useUnsavedChangeRegistry', () => {
  return {
    __esModule: true,
    useUnsavedChangeRegistry: (options: any, changes: any, onConfirm: any) => {
      onConfirm && onConfirm();
    }
  };
});
const renderComponent = (props: any) => {
  return renderWithMockStore(<ConfigureParametersStep {...props} />, {
    initialState: {
      workflowSetup: {
        lettersData: {
          letters: [
            {
              id: '1',
              name: 'Promise to pay',
              description: 'description',
              letterCode: 'A9Z1',
              optionalNameAddressPermitted: 'N',
              parameters: [
                { id: '1', name: 'Annual charge', description: 'variable 1' }
              ]
            },
            {
              id: '2',
              name: 'Promise to pay',
              description: 'description',
              letterCode: 'A9B1',
              optionalNameAddressPermitted: 'Y',
              parameters: [
                { id: '1', name: 'Annual charge', description: 'variable 1' }
              ]
            }
          ]
        } as LettersDataState
      }
    } as AppState
  });
};
const queryInputFromLabel = (tierLabel: HTMLElement) => {
  return tierLabel
    .closest(
      '.dls-input-container, .dls-numreric-container, .text-field-container'
    )
    ?.querySelector('input, .input') as HTMLInputElement;
};
const then_change_letter_number_dropdown = (
  wrapper: RenderResult,
  value: string
) => {
  const setRecurrenceOptions = queryInputFromLabel(
    wrapper.getByText('txt_letter_number')
  );
  act(() => {
    userEvent.click(setRecurrenceOptions);
  });

  act(() => {
    userEvent.click(screen.getByText(value));
  });

  fireEvent.blur(setRecurrenceOptions);
};

describe('ConfigureParametersStep > ConfigureParametersStep', () => {
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => (() => {}) as any);
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
  });

  it('Should render with Alert Message', async () => {
    const props = {
      stepId: '1',
      selectedStep: '2',
      clearFormName: '',
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = await renderComponent({
      ...props,
      formValues: {
        isValid: false,
        configureParameters: {
          [SendLetterParameterEnum.LetterNumber]: 'A9Z1',
          [SendLetterParameterEnum.Addressing]: '1'
        }
      },
      dependencies: {
        attachmentId: '123'
      }
    });

    expect(wrapper.getByText('txt_letter_number')).toBeInTheDocument;
  });

  it('Should have error', async () => {
    const props = {
      stepId: '1',
      selectedStep: '2',
      clearFormName: '',
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = await renderComponent({
      ...props,
      formValues: {
        isValid: false,
        configureParameters: {}
      }
    });

    expect(
      wrapper.getByText(
        'txt_send_letter_to_accounts_workflow_configure_parameters_step_send_letter_title'
      )
    ).toBeInTheDocument;
  });

  it('Should render ConfigureParametersStep contains Completed', async () => {
    const props = {
      stepId: '2',
      selectedStep: '2',
      clearFormName: '',
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = await renderComponent({
      ...props,
      formValues: {
        isValid: true,
        configureParameters: {
          [SendLetterParameterEnum.LetterNumber]: 'A9B1',
          [SendLetterParameterEnum.Addressing]: '0'
        }
      },
      dependencies: {
        attachmentId: '123'
      }
    });
    userEvent.click(screen.queryAllByRole('radio')[0]);
    userEvent.click(screen.queryAllByRole('radio')[1]);
    then_change_letter_number_dropdown(wrapper, 'A9Z1');
    expect(
      wrapper.getByText(
        'txt_send_letter_to_accounts_workflow_configure_parameters_step_send_letter_title'
      )
    ).toBeInTheDocument;
  });
});
