import { SendLetterParameterEnum } from 'app/constants/enums';
import { Button, useTranslation } from 'app/_libraries/_dls';
import { isFunction } from 'app/_libraries/_dls/lodash';
import { View } from 'app/_libraries/_dof/core';
import { useSelectLetters } from 'pages/_commons/redux/WorkflowSetup';
import React, { useMemo } from 'react';
import { ConfigureParametersFormValues } from '.';

const ConfigureParametersStepSummary: React.FC<
  WorkflowSetupSummaryProps<ConfigureParametersFormValues>
> = ({ formValues, onEditStep }) => {
  const { t } = useTranslation();
  const letters = useSelectLetters();
  const sendLetterViewData = useMemo(() => {
    const selected = letters?.find(
      o =>
        o.letterCode ===
        formValues?.configureParameters?.[SendLetterParameterEnum.LetterNumber]
    );
    const parameters = selected?.parameters?.reduce(
      (pre, cur: any) => ({
        ...pre,
        [cur.name.replace(' ', '_')]: cur.description
      }),
      {} as Record<string, string>
    );
    return { ...selected, ...parameters };
  }, [formValues?.configureParameters, letters]);

  const handleEdit = () => {
    isFunction(onEditStep) && onEditStep();
  };

  return (
    <div className="position-relative">
      <div className="absolute-top-right mt-n26 mr-n8">
        <Button variant="outline-primary" size="sm" onClick={handleEdit}>
          {t('txt_edit')}
        </Button>
      </div>
      <View
        id={`send-letter-configure-parameters-summary-${sendLetterViewData?.letterCode}`}
        formKey={`send-letter-configure-parameters-summary-${sendLetterViewData?.letterCode}`}
        descriptor="send-letter-configure-parameters-summary"
        value={{
          ...sendLetterViewData,
          letterNumber: sendLetterViewData?.letterCode,
          expansionAddressCode:
            formValues?.configureParameters?.addressingType === '0'
              ? 'Standard Addressing'
              : 'Universal Addressing'
        }}
      />
    </div>
  );
};

export default ConfigureParametersStepSummary;
