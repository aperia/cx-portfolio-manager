import { SendLetterParameterEnum } from 'app/constants/enums';
import parseFormValues from './parseFormValues';

describe('AssignPricingStrategiesWorkFlow > ConfigureParametersStep > parseFormValues', () => {
  it('Should return undefined', () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '2'
            }
          ] as any
        }
      } as WorkflowSetupInstance,
      id: '1'
    };

    const response = parseFormValues(props.data, props.id);
    expect(response).toEqual(undefined);
  });

  it('Should return data', () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '1'
            }
          ] as any,
          configurations: {
            [SendLetterParameterEnum.LetterNumber]: 'A9B1',
            [SendLetterParameterEnum.Addressing]: '10'
          } as any
        }
      } as any,
      id: '1'
    };

    const response = parseFormValues(props.data, props.id);

    expect(response)?.toEqual({
      configureParameters: {
        [SendLetterParameterEnum.LetterNumber]: 'A9B1',
        [SendLetterParameterEnum.Addressing]: '10'
      },
      isPass: false,
      isSelected: false,
      isValid: true
    });
  });
});
