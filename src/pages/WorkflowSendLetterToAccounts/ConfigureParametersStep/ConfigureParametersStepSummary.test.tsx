import { fireEvent } from '@testing-library/react';
import { SendLetterParameterEnum } from 'app/constants/enums';
import { LettersDataState } from 'app/types';
import { mockUseDispatchFnc, renderWithMockStore } from 'app/utils';
import React from 'react';
import ConfigureParametersStepSummary from './ConfigureParametersStepSummary';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/utils/MockView')
);
const mockUseDispatch = mockUseDispatchFnc();
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderComponent = (props: any) => {
  return renderWithMockStore(<ConfigureParametersStepSummary {...props} />, {
    initialState: {
      workflowSetup: {
        lettersData: {
          letters: [
            {
              id: '1',
              name: 'Promise to pay',
              description: 'description',
              letterCode: 'A9Z1',
              optionalNameAddressPermitted: 'N',
              parameters: [
                { id: '1', name: 'Annual charge', description: 'variable 1' }
              ]
            },
            {
              id: '2',
              name: 'Promise to pay',
              description: 'description',
              letterCode: 'A9B1',
              optionalNameAddressPermitted: 'Y',
              parameters: [
                { id: '1', name: 'Annual charge', description: 'variable 1' }
              ]
            }
          ]
        } as LettersDataState
      }
    } as AppState
  });
};

describe('ConfigureParametersStepSummary ', () => {
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => (() => {}) as any);
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
  });

  it('Should render ConfigureParametersStepSummary with Addressing is 0', async () => {
    const mockFunction = jest.fn();

    const props = {
      formValues: {
        configureParameters: {
          [SendLetterParameterEnum.LetterNumber]: 'A9B1',
          [SendLetterParameterEnum.Addressing]: '0'
        }
      },
      onEditStep: mockFunction
    };
    const wrapper = await renderComponent(props);
    const editButon = wrapper.getByText('txt_edit');
    expect(editButon).toBeInTheDocument;
    fireEvent.click(editButon);
    expect(mockFunction).toHaveBeenCalled();
  });
  it('Should render ConfigureParametersStepSummary  with Addressing is 1', async () => {
    const mockFunction = jest.fn();

    const props = {
      formValues: {
        configureParameters: {
          [SendLetterParameterEnum.LetterNumber]: 'A9B1',
          [SendLetterParameterEnum.Addressing]: '1'
        }
      },
      onEditStep: mockFunction
    };
    const wrapper = await renderComponent(props);
    const editButon = wrapper.getByText('txt_edit');
    expect(editButon).toBeInTheDocument;
    fireEvent.click(editButon);
    expect(mockFunction).toHaveBeenCalled();
  });
});
