import FieldTooltip from 'app/components/FieldTooltip';
import { AddressingEnum, SendLetterParameterEnum } from 'app/constants/enums';
import {
  confirmEditWhenChangeEffectToUploadProps,
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { useFormValidations, useUnsavedChangeRegistry } from 'app/hooks';
import {
  ComboBox,
  InlineMessage,
  Radio,
  useTranslation
} from 'app/_libraries/_dls';
import { StateFile } from 'app/_libraries/_dls/components/Upload/File';
import { STATUS } from 'app/_libraries/_dls/components/Upload/helper';
import { View } from 'app/_libraries/_dof/core';
import { isEmpty } from 'lodash';
import {
  actionsWorkflowSetup,
  useSelectLetters
} from 'pages/_commons/redux/WorkflowSetup';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch } from 'react-redux';
import ConfigureParametersStepSummary from './ConfigureParametersStepSummary';
import parseFormValues from './parseFormValues';

export interface ConfigureParametersFormValues {
  isValid?: boolean;
  configureParameters?: WorkflowSetupConfigureParameters;
}
interface IDependencies {
  files?: StateFile[];
}

const ConfigureParametersStep: React.FC<
  WorkflowSetupProps<ConfigureParametersFormValues, IDependencies> & {
    clearFormName: string;
  }
> = ({
  formValues,
  setFormValues,
  clearFormValues,
  clearFormName,
  savedAt,
  isStuck,
  dependencies
}) => {
  const dispatch = useDispatch();
  const [initialValues, setInitialValues] = useState(formValues);

  const { configureParameters } = formValues;

  const letters = useSelectLetters();

  const { t } = useTranslation();

  useEffect(() => {
    setInitialValues(formValues);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [savedAt]);

  const formChanged =
    initialValues?.configureParameters?.[SendLetterParameterEnum.Addressing] !==
      configureParameters?.[SendLetterParameterEnum.Addressing] ||
    initialValues?.configureParameters?.[
      SendLetterParameterEnum.LetterNumber
    ] !== configureParameters?.[SendLetterParameterEnum.LetterNumber];

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__SEND_LETTER_TO_ACCOUNTS__CONFIGURE_PARAMETERS,
      priority: 1
    },
    [formChanged]
  );

  const handleResetUploadStep = () => {
    const file = dependencies?.files?.[0];
    const attachmentId = file?.idx;
    const isValid = file?.status === STATUS.valid;

    clearFormValues(clearFormName);
    isValid &&
      dispatch(actionsWorkflowSetup.deleteTemplateFile({ attachmentId }));
  };
  useUnsavedChangeRegistry(
    {
      ...confirmEditWhenChangeEffectToUploadProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__SEND_LETTER_TO_ACCOUNTS__CONFIGURE_PARAMETERS_CONFIRM_EDIT,
      priority: 1
    },
    [!isEmpty(dependencies?.files) && formChanged],
    handleResetUploadStep
  );

  const validateFields = useMemo(() => {
    return [
      SendLetterParameterEnum.LetterNumber,
      SendLetterParameterEnum.Addressing
    ] as (keyof WorkflowSetupConfigureParameters)[];
  }, []);

  const currentErrors = useMemo(
    () =>
      ({
        [SendLetterParameterEnum.LetterNumber]: !configureParameters?.[
          SendLetterParameterEnum.LetterNumber
        ] && {
          status: true,
          message: t('txt_required_validation', {
            fieldName: t('txt_letter_number')
          })
        },
        [SendLetterParameterEnum.Addressing]: !configureParameters?.[
          SendLetterParameterEnum.Addressing
        ] && {
          status: true
        }
      } as Record<keyof WorkflowSetupConfigureParameters, IFormError>),
    [t, configureParameters]
  );

  const [touches, errors, setTouched] =
    useFormValidations<keyof WorkflowSetupConfigureParameters>(currentErrors);

  const isValid = useMemo(
    () =>
      !Object.keys(errors).some(
        k => errors[k as keyof WorkflowSetupConfigureParameters]
      ) &&
      validateFields.every(
        k =>
          !currentErrors[k]?.status ||
          touches[k as keyof WorkflowSetupConfigureParameters]?.touched
      ),
    [validateFields, errors, touches, currentErrors]
  );

  useEffect(() => {
    if (formValues.isValid === isValid) return;
    setFormValues({
      ...formValues,
      isValid
    });
  }, [isValid, setFormValues, formValues]);

  useEffect(() => {
    dispatch(actionsWorkflowSetup.getLetters());
  }, [dispatch]);

  const handleChangeRadio = useCallback(
    (value: string) => {
      setFormValues({
        ...formValues,
        configureParameters: {
          ...formValues.configureParameters,
          [SendLetterParameterEnum.Addressing]: value
        }
      });
    },
    [formValues, setFormValues]
  );

  const handleFormChange = useCallback(
    (field: keyof WorkflowSetupConfigureParameters) => (e: any) => {
      const value = e.target?.value;

      setFormValues({
        ...formValues,
        configureParameters: {
          ...formValues.configureParameters,
          [field]: value
        }
      });
    },
    [formValues, setFormValues]
  );

  const sendLetterViewData = useMemo(() => {
    const selected = letters?.find(
      o =>
        o.letterCode ===
        configureParameters?.[SendLetterParameterEnum.LetterNumber]
    );
    const parameters = selected?.parameters?.reduce(
      (pre, cur: any) => ({
        ...pre,
        [cur.name.replace(' ', '_')]: cur.description
      }),
      {} as Record<string, string>
    );
    return { ...selected, ...parameters };
  }, [configureParameters, letters]);

  const hasAlertMessage = useMemo(() => {
    if (
      configureParameters?.[SendLetterParameterEnum.Addressing] ===
        AddressingEnum.UNIVERSAL &&
      sendLetterViewData?.optionalNameAddressPermitted === 'N'
    )
      return true;
    return false;
  }, [configureParameters, sendLetterViewData]);

  const comboBoxItems = useMemo(
    () =>
      letters.map(item => (
        <ComboBox.Item
          key={item.letterCode}
          label={item.letterCode}
          value={item.letterCode}
        />
      )),
    [letters]
  );

  return (
    <>
      {isStuck && (
        <InlineMessage className="mb-0 mt-24" variant="danger" withIcon>
          {t('txt_step_stuck_move_forward_message')}
        </InlineMessage>
      )}
      {hasAlertMessage && (
        <InlineMessage variant="warning" withIcon className="mt-24 mb-0">
          <p className="color-orange-d16">
            {t(
              'txt_send_letter_to_accounts_workflow_configure_parameters_step_inline_alert'
            )}
          </p>
        </InlineMessage>
      )}

      <h5 className="mt-24">
        {t(
          'txt_send_letter_to_accounts_workflow_configure_parameters_step_send_letter_title'
        )}
      </h5>
      <div className="pb-24 mb-24 border-bottom">
        <div className="row mt-16">
          <div className="d-flex col-lg-4 col-md-6">
            <FieldTooltip
              keepShowOnDevice={false}
              placement="right"
              element={
                <div className="my-n8">
                  <div className="text-uppercase">
                    {t(
                      'txt_send_letter_to_accounts_workflow_configure_parameters_step_tooltip_header'
                    )}
                  </div>
                  <p className="mt-8">
                    {t(
                      'txt_send_letter_to_accounts_workflow_configure_parameters_step_tooltip'
                    )}
                  </p>
                </div>
              }
            >
              <ComboBox
                label={t('txt_letter_number')}
                required
                textField="code"
                noResult={t('txt_no_results_found')}
                value={
                  configureParameters?.[SendLetterParameterEnum.LetterNumber]
                }
                onChange={handleFormChange(
                  SendLetterParameterEnum.LetterNumber
                )}
                onFocus={setTouched(SendLetterParameterEnum.LetterNumber)}
                onBlur={setTouched(SendLetterParameterEnum.LetterNumber, true)}
                error={errors[SendLetterParameterEnum.LetterNumber]}
              >
                {comboBoxItems}
              </ComboBox>
            </FieldTooltip>
          </div>
        </div>

        {isEmpty(sendLetterViewData) ? (
          <div className="row">
            <div className="col-6 col-lg-4" role="dof-field">
              <div className="form-group-static">
                <span className="form-group-static__label">
                  {t('txt_description')}
                </span>
                <div className="form-group-static__text"></div>
              </div>
            </div>

            <div className="col-6 col-lg-4" role="dof-field">
              <div className="form-group-static">
                <span className="form-group-static__label">
                  {t(
                    'txt_send_letter_to_accounts_workflow_configure_parameters_option_name'
                  )}
                </span>
                <div className="form-group-static__text"></div>
              </div>
            </div>
          </div>
        ) : (
          <View
            id={`send-letter-configure-parameters-${sendLetterViewData?.letterCode}`}
            formKey={`send-letter-configure-parameters-${sendLetterViewData?.letterCode}`}
            descriptor="send-letter-configure-parameters"
            value={sendLetterViewData}
          />
        )}
      </div>
      <h5 className="mb-8">
        {t(
          'txt_send_letter_to_accounts_workflow_configure_parameters_select_addressing'
        )}
      </h5>
      <p className="mb-16 color-grey">
        {t(
          'txt_send_letter_to_accounts_workflow_configure_parameters_select_addressing_des'
        )}
      </p>
      <div className="row my-16">
        <div className="col-md-6">
          <div className="group-card">
            <div
              className="group-card-item d-flex align-items-center px-16 py-18 custom-control-root"
              onClick={() => handleChangeRadio(AddressingEnum.STANDARD)}
            >
              <span className="pr-8">
                {t(
                  'txt_send_letter_to_accounts_workflow_configure_parameters_select_standard_addressing'
                )}
              </span>
              <Radio className="ml-auto mr-n4">
                <Radio.Input
                  checked={
                    configureParameters?.[
                      SendLetterParameterEnum.Addressing
                    ] === AddressingEnum.STANDARD
                  }
                  value={AddressingEnum.STANDARD}
                  id="standard"
                  name="standard"
                  className="checked-style"
                />
              </Radio>
            </div>
            <div
              className="group-card-item d-flex align-items-center px-16 py-18 custom-control-root"
              onClick={() => handleChangeRadio(AddressingEnum.UNIVERSAL)}
            >
              <span className="pr-8">
                {t(
                  'txt_send_letter_to_accounts_workflow_configure_parameters_select_universal_addressing'
                )}
              </span>
              <Radio className="ml-auto mr-n4">
                <Radio.Input
                  checked={
                    configureParameters?.[
                      SendLetterParameterEnum.Addressing
                    ] === AddressingEnum.UNIVERSAL
                  }
                  value={AddressingEnum.UNIVERSAL}
                  id="universal"
                  name="universal"
                  className="checked-style"
                />
              </Radio>
            </div>
          </div>
        </div>
        {!!configureParameters?.[SendLetterParameterEnum.Addressing] && (
          <div className="col-md-6">
            <div className="bg-light-l20 rounded-lg p-16 h-100">
              <p className="color-grey">
                {configureParameters?.[SendLetterParameterEnum.Addressing] ===
                  AddressingEnum.UNIVERSAL &&
                  '1 - Yes, access the universal addressing database.'}
                {configureParameters?.[SendLetterParameterEnum.Addressing] ===
                  AddressingEnum.STANDARD &&
                  '0 - No, do not access the universal addressing database.'}
              </p>
            </div>
          </div>
        )}
      </div>
    </>
  );
};

const ExtraStaticConfigureParameters =
  ConfigureParametersStep as WorkflowSetupStaticProp;
ExtraStaticConfigureParameters.summaryComponent =
  ConfigureParametersStepSummary;

ExtraStaticConfigureParameters.defaultValues = {
  isValid: false,
  configureParameters: {}
};
ExtraStaticConfigureParameters.parseFormValues = parseFormValues;

export default ExtraStaticConfigureParameters;
