import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { stepRegistry } from '../_commons/WorkflowSetup/registries';
import ConfigureParametersStep from './ConfigureParametersStep';
import GettingStartStep from './GettingStartStep';

stepRegistry.registerStep(
  'GetStartedSendLetterToAccountsWorkflow',
  GettingStartStep
);

stepRegistry.registerStep(
  'ConfigureParametersSendLetterToAccountsWorkflow',
  ConfigureParametersStep,
  [
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__SEND_LETTER_TO_ACCOUNTS__CONFIGURE_PARAMETERS
  ]
);
