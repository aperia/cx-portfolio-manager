import { renderComponent } from 'app/utils';
import React from 'react';
import MyPortfolios from './';

jest.mock('app/_libraries/_dls', () => ({
  SimpleBar: ({ children }: any) => <div>{children}</div>,
  useTranslation: () => ({
    t: (key: string) => key
  })
}));
jest.mock('pages/_commons/PortfolioList', () => ({
  __esModule: true,
  default: ({ title }: any) => <div>{title}</div>
}));

const testId = 'my-portfolios';

describe('Page > My Portfolios', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  it('Should render My Portfolios UI', async () => {
    const { getByText } = await renderComponent(testId, <MyPortfolios />);

    expect(getByText('txt_my_portfolios')).toBeInTheDocument();
  });
});
