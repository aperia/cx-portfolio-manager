import { SimpleBar, useTranslation } from 'app/_libraries/_dls';
import PortfolioList from 'pages/_commons/PortfolioList';
import React from 'react';

interface IMyPortfoliosProps {}
const MyPortfolios: React.FC<IMyPortfoliosProps> = () => {
  const { t } = useTranslation();

  return (
    <SimpleBar>
      <PortfolioList
        containerClassName={'p-24'}
        title={t('txt_my_portfolios')}
        isFullPage={true}
      />
    </SimpleBar>
  );
};

export default MyPortfolios;
