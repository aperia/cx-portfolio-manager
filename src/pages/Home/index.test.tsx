import { renderComponent } from 'app/utils';
import React from 'react';
import Home from './';

jest.mock('./MyInProgressWorkflows', () => ({
  __esModule: true,
  default: () => <div>InProgress</div>
}));
jest.mock('./ApprovalQueue', () => ({
  __esModule: true,
  default: () => <div>Approval Queue</div>
}));
jest.mock('./ChangeManagement', () => ({
  __esModule: true,
  default: () => <div>Change Management</div>
}));
jest.mock('./MyFavoriteWorkflows', () => ({
  __esModule: true,
  default: () => <div>My Favorite Workflows</div>
}));
jest.mock('./MyPortolios', () => ({
  __esModule: true,
  default: () => <div>My Portolios</div>
}));
jest.mock('app/_libraries/_dls', () => ({
  SimpleBar: ({ children }: any) => <div>{children}</div>,
  useTranslation: () => ({
    t: (key: string) => key
  })
}));

const testId = 'home-page';

describe('Page > Home page', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  it('Should render Home page UI', async () => {
    const { getByText } = await renderComponent(testId, <Home />);

    expect(getByText('My Portolios')).toBeInTheDocument();
    expect(getByText('My Favorite Workflows')).toBeInTheDocument();
    expect(getByText('InProgress')).toBeInTheDocument();
    expect(getByText('Change Management')).toBeInTheDocument();
    expect(getByText('Approval Queue')).toBeInTheDocument();
  });
});
