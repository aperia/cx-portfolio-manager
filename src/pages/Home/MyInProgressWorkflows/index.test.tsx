import { renderComponent } from 'app/utils';
import * as WorkflowRedux from 'pages/_commons/redux/Workflow/select-hooks';
import React from 'react';
import { HashRouter } from 'react-router-dom';
import MyInProgressWorkflows from './';

jest.mock('pages/_commons/WorkflowInProgressList', () => ({
  __esModule: true,
  default: () => <div>Workflow In Progress List</div>
}));
jest.mock('app/_libraries/_dls', () => ({
  useTranslation: () => ({
    t: (key: string) => key
  })
}));
jest.mock('react-router-dom', () => {
  const actualModule = jest.requireActual('react-router-dom');
  return {
    ...actualModule,
    useHistory: () => ({
      push: jest.fn()
    })
  };
});
const mockSelectWorkflows = jest.spyOn(
  WorkflowRedux,
  'useSelectLastUpdatedInProgressWorkflows'
);
const mockSelectWorkflowsFetchingStatus = jest.spyOn(
  WorkflowRedux,
  'useSelectWorkflowsFetchingStatus'
);

const testId = 'my-in-process-workflow';

describe('Page > Home > My InProgress Workflow', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Should render Home My InProgress Workflow list UI with view all link', async () => {
    mockSelectWorkflowsFetchingStatus.mockReturnValue({
      loading: false,
      error: false
    } as any);
    mockSelectWorkflows.mockReturnValue({
      workflows: [],
      total: 6
    } as any);

    const { getByText } = await renderComponent(
      testId,
      <HashRouter>
        <MyInProgressWorkflows />
      </HashRouter>
    );

    expect(getByText('txt_view_all_in_progress_workflows')).toBeInTheDocument();
  });

  it('Should render Home My InProgress Workflow list UI without view all link', async () => {
    mockSelectWorkflowsFetchingStatus.mockReturnValue({
      loading: false,
      error: true
    } as any);
    mockSelectWorkflows.mockReturnValue({
      workflows: [],
      total: 0
    } as any);

    const { queryByText } = await renderComponent(
      testId,
      <HashRouter>
        <MyInProgressWorkflows />
      </HashRouter>
    );

    expect(queryByText('txt_view_all_in_progress_workflows')).toBeNull();
  });
});
