import { MY_IN_PROGRESS_WORKFLOWS_URL } from 'app/constants/links';
import { useTranslation } from 'app/_libraries/_dls';
import {
  useSelectLastUpdatedInProgressWorkflows,
  useSelectWorkflowsFetchingStatus
} from 'pages/_commons/redux/Workflow';
import WorkflowInProgressList from 'pages/_commons/WorkflowInProgressList';
import React from 'react';
import { Link } from 'react-router-dom';

const MyInProgressWorkflows: React.FC = () => {
  const { t } = useTranslation();
  const { error } = useSelectWorkflowsFetchingStatus();
  const { workflows, total } = useSelectLastUpdatedInProgressWorkflows();

  return (
    <div className="h-100 home-inprogress-workflow">
      <div className="d-flex align-items-center">
        <h4>{t('txt_my_in_progress_workflows')}</h4>
        {total > 5 && !error && (
          <Link to={MY_IN_PROGRESS_WORKFLOWS_URL} className="ml-auto link">
            {t('txt_view_all_in_progress_workflows')}
          </Link>
        )}
      </div>
      <div className="mt-16">
        <span>{t('txt_last_5_in_progress_workflows')}</span>
      </div>
      <div className="section">
        <WorkflowInProgressList workflows={workflows} small />
      </div>
    </div>
  );
};

export default MyInProgressWorkflows;
