import { CHANGE_MANAGEMENT_URL } from 'app/constants/links';
import { useTranslation } from 'app/_libraries/_dls';
import ChangeList from 'pages/_commons/ChangeList';
import { useSelectChangesManagement } from 'pages/_commons/redux/Change';
import React from 'react';
import { Link } from 'react-router-dom';

const ChangeManagement: React.FC = () => {
  const selectChanges = useSelectChangesManagement();

  const { error, totalItem } = selectChanges;

  const { t } = useTranslation();

  return (
    <div>
      <div className="d-flex">
        <h4>{t('txt_change_management')}</h4>
        {totalItem > 5 && !error && (
          <Link to={CHANGE_MANAGEMENT_URL} className="link ml-auto">
            {t('txt_view_all_changes')}
          </Link>
        )}
      </div>

      <div className="mt-16 pb-4">
        <span>{t('txt_changes_by_effective_date')}</span>
      </div>
      <div className="section mx-n12">
        <ChangeList
          containerClassName={'px-12'}
          hideBreadCrumb
          hideHeader
          hideViewMode
          hideFilter
          maxDataNumber={5}
          isFullPage={false}
        />
      </div>
    </div>
  );
};

export default ChangeManagement;
