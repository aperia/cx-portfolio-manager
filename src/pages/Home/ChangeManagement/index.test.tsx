import { renderComponent } from 'app/utils';
import * as ChangeRedux from 'pages/_commons/redux/Change/select-hooks';
import React from 'react';
import { HashRouter } from 'react-router-dom';
import ChangeManagement from './';

jest.mock('pages/_commons/ChangeList', () => ({
  __esModule: true,
  default: () => <div>Change Management List</div>
}));
jest.mock('app/_libraries/_dls', () => ({
  useTranslation: () => ({
    t: (key: string) => key
  })
}));
const mockSelectChangemanagement = jest.spyOn(
  ChangeRedux,
  'useSelectChangesManagement'
);

const testId = 'my-change-management';

describe('Page > Home > Change management list', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Should render Home Change management UI with view all link', async () => {
    mockSelectChangemanagement.mockReturnValue({
      loading: false,
      error: false,
      totalItem: 6
    } as any);

    const { getByText } = await renderComponent(
      testId,
      <HashRouter>
        <ChangeManagement />
      </HashRouter>
    );

    expect(getByText('Change Management List')).toBeInTheDocument();
    expect(getByText('txt_view_all_changes')).toBeInTheDocument();
  });

  it('Should render Home Change management UI without view all link', async () => {
    mockSelectChangemanagement.mockReturnValue({
      loading: false,
      error: true,
      totalItem: 0
    } as any);

    const { queryByText } = await renderComponent(
      testId,
      <HashRouter>
        <ChangeManagement />
      </HashRouter>
    );

    expect(queryByText('txt_view_all_changes')).toBeNull();
  });
});
