import { PENDING_APPROVALS_URL } from 'app/constants/links';
import { useTranslation } from 'app/_libraries/_dls';
import PendingApprovalList from 'pages/_commons/PendingApprovalList';
import {
  useSelectApprovalQueues,
  useSelectApprovalQueuesStatus
} from 'pages/_commons/redux/Change';
import React from 'react';
import { Link } from 'react-router-dom';

const ApprovalQueue: React.FC = () => {
  const { t } = useTranslation();

  const { total } = useSelectApprovalQueues();
  const { error } = useSelectApprovalQueuesStatus();

  return (
    <div>
      <div className="d-flex justify-content-between">
        <h4>{t('txt_pending_approval_queue')}</h4>
        {total > 5 && !error && (
          <Link to={PENDING_APPROVALS_URL} className="link ml-auto">
            {t('txt_view_all_approval_queue')}
          </Link>
        )}
      </div>
      <div className="mt-16 pb-4">
        <span>{t('txt_home_pending_approval_queue_desc')}</span>
      </div>
      <div className="section mx-n12">
        <PendingApprovalList
          containerClassName={'px-12'}
          hideBreadCrumb
          hideHeader
          hideViewMode
          hideFilter
          maxDataNumber={5}
        />
      </div>
    </div>
  );
};

export default ApprovalQueue;
