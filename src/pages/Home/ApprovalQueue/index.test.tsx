import { renderComponent } from 'app/utils';
import * as ChangeRedux from 'pages/_commons/redux/Change/select-hooks';
import React from 'react';
import { HashRouter } from 'react-router-dom';
import ApprovalQueue from './';

jest.mock('pages/_commons/PendingApprovalList', () => ({
  __esModule: true,
  default: () => <div>Pending Approval List</div>
}));
jest.mock('app/_libraries/_dls', () => ({
  useTranslation: () => ({
    t: (key: string) => key
  })
}));
const mockSelectApprovalQueues = jest.spyOn(
  ChangeRedux,
  'useSelectApprovalQueues'
);
const mockSelectApprovalQueuesStatus = jest.spyOn(
  ChangeRedux,
  'useSelectApprovalQueuesStatus'
);

const testId = 'pending-approval';

describe('Page > Home > Approval queue list', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Should render Home Approval queue UI with view all link', async () => {
    mockSelectApprovalQueues.mockReturnValue({
      total: 6
    } as any);
    mockSelectApprovalQueuesStatus.mockReturnValue({
      loading: false,
      error: false
    });

    const { getByText } = await renderComponent(
      testId,
      <HashRouter>
        <ApprovalQueue />
      </HashRouter>
    );

    expect(getByText('Pending Approval List')).toBeInTheDocument();
    expect(getByText('txt_view_all_approval_queue')).toBeInTheDocument();
  });

  it('Should render Home Approval queue UI without view all link', async () => {
    mockSelectApprovalQueues.mockReturnValue({
      total: 0
    } as any);
    mockSelectApprovalQueuesStatus.mockReturnValue({
      loading: false,
      error: true
    });

    const { queryByText } = await renderComponent(
      testId,
      <HashRouter>
        <ApprovalQueue />
      </HashRouter>
    );

    expect(queryByText('txt_view_all_approval_queue')).toBeNull();
  });
});
