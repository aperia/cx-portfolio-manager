import { WorkflowSection } from 'app/constants/enums';
import { WORKFLOWS_URL } from 'app/constants/links';
import { useTranslation } from 'app/_libraries/_dls';
import {
  actionsWorkflow,
  useSelectLastUpdatedFavoriteWorkflows
} from 'pages/_commons/redux/Workflow';
import WorkflowTemplateList from 'pages/_commons/WorkflowTemplateList';
import React, { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';

const MyFavoriteWorkflows: React.FC = () => {
  const { t } = useTranslation();
  const history = useHistory();
  const dispatch = useDispatch();

  const { error, total } = useSelectLastUpdatedFavoriteWorkflows();

  const gotoFavoriteWorkflows = useCallback(() => {
    history.push(WORKFLOWS_URL);
    dispatch(
      actionsWorkflow.updatePageWorkflowsFilter({
        currentSection: WorkflowSection.FAVORITE_WORKFLOWS
      })
    );
  }, [history, dispatch]);

  return (
    <div className="h-100 home-favorite-workflow pr-xl-24">
      <div className="d-flex align-items-center">
        <h4>{t('txt_my_favorite_workflows')}</h4>
        {total > 5 && !error && (
          <Link
            to={WORKFLOWS_URL}
            className="ml-auto link"
            onClick={gotoFavoriteWorkflows}
          >
            {t('txt_view_all_favorite_workflows')}
          </Link>
        )}
      </div>
      <div className="mt-16">
        <span>{t('txt_last_5_favorite_workflows')}</span>
      </div>
      <div className="section">
        <WorkflowTemplateList />
      </div>
    </div>
  );
};

export default MyFavoriteWorkflows;
