import { fireEvent } from '@testing-library/react';
import { renderComponent } from 'app/utils';
import * as WorkflowRedux from 'pages/_commons/redux/Workflow/select-hooks';
import React from 'react';
import * as ReactRedux from 'react-redux';
import { HashRouter } from 'react-router-dom';
import MyFavoriteWorkflows from './';

jest.mock('pages/_commons/WorkflowTemplateList', () => ({
  __esModule: true,
  default: () => <div>Workflow Template List</div>
}));
jest.mock('app/_libraries/_dls', () => ({
  useTranslation: () => ({
    t: (key: string) => key
  })
}));
jest.mock('react-router-dom', () => {
  const actualModule = jest.requireActual('react-router-dom');
  return {
    ...actualModule,
    useHistory: () => ({
      push: jest.fn()
    })
  };
});
const mockSelectFavoriteWorkflows = jest.spyOn(
  WorkflowRedux,
  'useSelectLastUpdatedFavoriteWorkflows'
);

const mockDispatch = jest.spyOn(ReactRedux, 'useDispatch');

const testId = 'my-favorite-workflow';

describe('Page > Home > My Favorite Workflow', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Should render Home My Favorite Workflow list UI with view all link', async () => {
    mockSelectFavoriteWorkflows.mockReturnValue({
      loading: false,
      error: false,
      total: 6
    } as any);

    const { getByText } = await renderComponent(
      testId,
      <HashRouter>
        <MyFavoriteWorkflows />
      </HashRouter>
    );

    expect(getByText('Workflow Template List')).toBeInTheDocument();
    expect(getByText('txt_view_all_favorite_workflows')).toBeInTheDocument();
  });

  it('test click on link view all', async () => {
    const dispatchFn = jest.fn();
    mockDispatch.mockReturnValue(dispatchFn);

    mockSelectFavoriteWorkflows.mockReturnValue({
      loading: false,
      error: false,
      total: 6
    } as any);

    const { getByText } = await renderComponent(
      testId,
      <HashRouter>
        <MyFavoriteWorkflows />
      </HashRouter>
    );

    fireEvent.click(getByText('txt_view_all_favorite_workflows'));

    expect(dispatchFn).toHaveBeenCalled();
  });

  it('Should render Home My Favorite Workflow list UI without view all link', async () => {
    mockSelectFavoriteWorkflows.mockReturnValue({
      loading: false,
      error: true,
      total: 0
    } as any);

    const { queryByText } = await renderComponent(
      testId,
      <HashRouter>
        <MyFavoriteWorkflows />
      </HashRouter>
    );

    expect(queryByText('txt_view_all_favorite_workflows')).toBeNull();
  });
});
