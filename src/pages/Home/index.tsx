import { SimpleBar, useTranslation } from 'app/_libraries/_dls';
import React from 'react';
import ApprovalQueue from './ApprovalQueue';
import ChangeManagement from './ChangeManagement';
import MyFavoriteWorkflows from './MyFavoriteWorkflows';
import MyInProgressWorkflows from './MyInProgressWorkflows';
import MyPortfolios from './MyPortolios';

const Home: React.FC = () => {
  const { t } = useTranslation();

  return (
    <SimpleBar className="p-24">
      <h3>{t('txt_portfolio_manager')}</h3>
      <div className="mt-24">
        <MyPortfolios />
      </div>
      <hr className="my-24" />

      <div className="row">
        <div className="col-xl-6 col-12">
          <MyFavoriteWorkflows />
        </div>
        <div className="col-xl-6 col-12 mt-xl-0 mt-24">
          <MyInProgressWorkflows />
        </div>
      </div>
      <hr className="my-24" />

      <div>
        <ChangeManagement />
      </div>
      <hr className="my-24" />

      <div>
        <ApprovalQueue />
      </div>
    </SimpleBar>
  );
};

export default Home;
