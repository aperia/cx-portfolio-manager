import { renderComponent } from 'app/utils';
import * as PortfolioRedux from 'pages/_commons/redux/Portfolio/select-hooks';
import React from 'react';
import { HashRouter } from 'react-router-dom';
import MyPortfolios from './';

jest.mock('pages/_commons/PortfolioList', () => ({
  __esModule: true,
  default: () => <div>Portfolio List</div>
}));
jest.mock('app/_libraries/_dls', () => ({
  useTranslation: () => ({
    t: (key: string) => key
  })
}));
const mockSelectPortfolios = jest.spyOn(PortfolioRedux, 'useSelectPortfolios');

const testId = 'my-portfolios';

describe('Page > Home > Portfolio list', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Should render Home Portfolio list UI with view all link', async () => {
    mockSelectPortfolios.mockReturnValue({
      portfolios: [{}, {}, {}, {}, {}, {}] as any[]
    });

    const { getByText } = await renderComponent(
      testId,
      <HashRouter>
        <MyPortfolios />
      </HashRouter>
    );

    expect(getByText('Portfolio List')).toBeInTheDocument();
    expect(getByText('txt_view_all_portfolios')).toBeInTheDocument();
  });

  it('Should render Home Portfolio list UI without view all link', async () => {
    mockSelectPortfolios.mockReturnValue({
      portfolios: [],
      error: true
    });

    const { queryByText } = await renderComponent(
      testId,
      <HashRouter>
        <MyPortfolios />
      </HashRouter>
    );

    expect(queryByText('txt_view_all_portfolios')).toBeNull();
  });
});
