import { MY_PORTFOLIOS_URL } from 'app/constants/links';
import { useTranslation } from 'app/_libraries/_dls';
import PortfolioList from 'pages/_commons/PortfolioList';
import { useSelectPortfolios } from 'pages/_commons/redux/Portfolio';
import React from 'react';
import { Link } from 'react-router-dom';

const MyPortfolios: React.FC = () => {
  const selectPortfolios = useSelectPortfolios();

  const { error, portfolios } = selectPortfolios;

  const { t } = useTranslation();

  return (
    <>
      <div className="d-flex align-items-center">
        <h4>{t('txt_my_portfolios')}</h4>
        {portfolios?.length > 5 && !error && (
          <Link to={MY_PORTFOLIOS_URL} className="link ml-auto">
            {t('txt_view_all_portfolios')}
          </Link>
        )}
      </div>

      <div className="mt-16 pb-4">
        <span>{t('txt_first_5_portfolios')}</span>
      </div>
      <div className="section mx-n12">
        <PortfolioList
          containerClassName={'px-12'}
          hideBreadCrumb
          hideHeader
          hideViewMode
          hideFilter
          isTopFive
          maxDataNumber={5}
        />
      </div>
    </>
  );
};

export default MyPortfolios;
