import ModalRegistry from 'app/components/ModalRegistry';
import TextAreaCountDown from 'app/components/TextAreaCountDown';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import {
  METHOD_UPDATE_INDEX_RATE_DEFAULT,
  METHOD_UPDATE_INDEX_RATE_FIELDS
} from 'app/constants/mapping';
import {
  unsavedChangesProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { methodParamsToObject, objectToMethodParams } from 'app/helpers';
import {
  useFormValidations,
  useUnsavedChangeRegistry,
  useUnsavedChangesRedirect
} from 'app/hooks';
import {
  Button,
  InlineMessage,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  TextBox,
  TransDLS,
  useTranslation
} from 'app/_libraries/_dls';
import isEqual from 'lodash.isequal';
import isFunction from 'lodash.isfunction';
import React, { useEffect, useMemo, useRef, useState } from 'react';
import ParameterGrid from './ParameterGrid';

export interface IAddNewMethodModalProps {
  id: string;
  show: boolean;
  isCreate?: boolean;
  method?: WorkflowSetupMethod;
  draftMethod?: WorkflowSetupMethod;
  methodNames?: string[];
  onClose?: (method?: WorkflowSetupMethod, isBack?: boolean) => void;
}

const AddNewMethodModal: React.FC<IAddNewMethodModalProps> = ({
  id,
  show,
  isCreate = true,
  method: methodProp = { methodType: 'NEWMETHOD' } as WorkflowSetupMethod,
  draftMethod,
  methodNames = [],
  onClose
}) => {
  const { t } = useTranslation();
  const redirect = useUnsavedChangesRedirect();

  const isChangeFieldName = useRef(false);

  const [initialMethod, setInitialMethod] = useState<
    WorkflowSetupMethodObjectParams & { rowId?: number }
  >(methodParamsToObject(methodProp));
  const [method, setMethod] = useState(
    draftMethod ? methodParamsToObject(draftMethod) : initialMethod
  );

  const [isDuplicateName, setIsDuplicateName] = useState(false);

  const isNewVersion = useMemo(
    () => method.methodType === 'NEWVERSION',
    [method.methodType]
  );

  const isNewMethod = useMemo(
    () => method.methodType === 'NEWMETHOD',
    [method.methodType]
  );

  const errorDuplicateName = useMemo(
    () => ({
      status: true,
      message: t(
        'txt_update_index_rate_step_3_method_details_unique_name_error'
      )
    }),
    [t]
  );

  useUnsavedChangeRegistry(
    {
      ...unsavedChangesProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__UPDATE_INDEX_RATE_METHOD_DETAILS,
      priority: 1
    },
    [
      !isEqual(initialMethod.name?.trim() || '', method.name?.trim() || ''),
      !isEqual(
        initialMethod.comment?.trim() || '',
        method.comment?.trim() || ''
      ),
      !isEqual(initialMethod, method)
    ]
  );

  const currentErrors = useMemo(
    () =>
      ({
        name: !method?.name?.trim() && {
          status: true,
          message: t('txt_required_validation', {
            fieldName: t('txt_method_name')
          })
        }
      } as Record<keyof WorkflowSetupMethodObjectParams, IFormError>),
    [method?.name, t]
  );
  const [touches, errors, setTouched] =
    useFormValidations<keyof WorkflowSetupMethodObjectParams>(currentErrors);

  const isValid = useMemo(
    () =>
      !errors.name?.status &&
      !isDuplicateName &&
      (!currentErrors.name?.status || touches.name?.touched),
    [
      errors.name?.status,
      isDuplicateName,
      currentErrors.name?.status,
      touches.name?.touched
    ]
  );

  useEffect(() => {
    let defaultParamObj: Record<string, string> = {};
    if (
      initialMethod.methodType === 'NEWMETHOD' &&
      initialMethod.rowId === undefined
    ) {
      const paramMapping = Object.keys(METHOD_UPDATE_INDEX_RATE_DEFAULT).reduce(
        (pre, next) => {
          pre[next] = METHOD_UPDATE_INDEX_RATE_DEFAULT[next];
          return pre;
        },
        {} as Record<string, string>
      );
      defaultParamObj = Object.assign({}, paramMapping);
      defaultParamObj.methodType = 'NEWMETHOD';
      defaultParamObj.comment = '';
      setMethod(defaultParamObj as any);
      setInitialMethod(defaultParamObj as any);
    }

    setMethod(method => ({ ...method, comment: method.comment || '' }));
  }, [initialMethod.methodType, initialMethod.rowId]);

  const handleBack = () => {
    const methodForm = objectToMethodParams(
      METHOD_UPDATE_INDEX_RATE_FIELDS,
      method
    );
    methodForm.versionParameters = !!methodForm.versionParameters
      ? methodForm.versionParameters
      : methodForm.parameters;

    isFunction(onClose) && onClose(methodForm, true);
  };

  const handleClose = () => {
    redirect({
      onConfirm: () => isFunction(onClose) && onClose(),
      formsWatcher: [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__UPDATE_INDEX_RATE_METHOD_DETAILS
      ]
    });
  };

  const handleAddMethod = () => {
    const isEdit = !isCreate;
    const isChangeMethodName = initialMethod.name !== method.name;
    const isCheckName = isCreate || (isEdit && isChangeMethodName);

    const isExistNameInCurrentMethods = methodNames.includes(
      method?.name?.trim()
    );
    const isChooseMethod = !isNewMethod && !isNewVersion;

    const isDuplicateName =
      (isCheckName && isExistNameInCurrentMethods) ||
      (isChooseMethod && method?.name?.trim() === method.modeledFrom?.name);
    setIsDuplicateName(isDuplicateName);
    if (isDuplicateName) return;

    const fields = METHOD_UPDATE_INDEX_RATE_FIELDS.filter(
      f => !excludeTierFields.includes(f)
    );

    const methodForm = objectToMethodParams([...fields], method);
    methodForm.versionParameters = !!methodForm.versionParameters
      ? methodForm.versionParameters
      : methodForm.parameters;

    isFunction(onClose) && onClose(methodForm);
  };

  const handleFormChange =
    (
      field: keyof WorkflowSetupMethodObjectParams & { optionalTiers?: string }
    ) =>
    (e: any) => {
      let value = e.target?.value;
      if (field === 'name') {
        // flag to check is change for Event onBlur
        isChangeFieldName.current = true;

        value = value?.toUpperCase();
        const isValidValue = /[^a-zA-Z0-9]/.test(value);
        if (isValidValue) return;
      }
      setMethod(method => ({ ...method, [field]: value }));
    };

  const handleBlurName = () => {
    if (isChangeFieldName.current) {
      setIsDuplicateName(false);
    }
    isChangeFieldName.current = false;

    setTouched('name', true)();
  };

  return (
    <ModalRegistry
      className="add-update-index-rate-modal"
      lg
      id={id}
      show={show}
      onAutoClosedAll={handleClose}
    >
      <ModalHeader border closeButton onHide={handleClose}>
        <ModalTitle>
          {t('txt_update_index_rate_step_3_method_details_title')}
        </ModalTitle>
      </ModalHeader>
      <ModalBody>
        <div className="color-grey">
          {isNewMethod && (
            <p>
              <TransDLS keyTranslation="txt_update_index_rate_step_3_method_detail_new_method_desc">
                <strong />
              </TransDLS>
            </p>
          )}
          {isNewVersion && (
            <p>
              <TransDLS keyTranslation="txt_update_index_rate_step_3_method_detail_new_version_desc">
                <strong className="color-grey-d20" />
              </TransDLS>
            </p>
          )}
          {!isNewMethod && !isNewVersion && (
            <p>
              <TransDLS keyTranslation="txt_update_index_rate_step_3_method_detail_model_from_desc">
                <strong className="color-grey-d20" />
              </TransDLS>
            </p>
          )}
        </div>
        <div>
          {isDuplicateName && (
            <InlineMessage variant="danger" withIcon className="mb-8 mt-16">
              {t(
                'txt_update_index_rate_step_3_method_details_unique_name_error'
              )}
            </InlineMessage>
          )}
          <div className="row mt-16">
            {!isNewMethod && (
              <div className="col-6 col-lg-4">
                <TextBox
                  id="addNewMethod__selectedMethod"
                  readOnly
                  value={method?.modeledFrom?.name || ''}
                  maxLength={8}
                  label={t('txt_selected_method')}
                  dataTestId="addNewMethod__selectedMethod"
                />
              </div>
            )}
            <div className="col-6 col-lg-4 mr-lg-4">
              <TextBox
                id="addNewMethod__methodName"
                dataTestId="addNewMethod__methodName"
                readOnly={isNewVersion}
                value={method?.name || ''}
                onChange={handleFormChange('name')}
                required
                maxLength={8}
                label={t('txt_method_name')}
                onFocus={setTouched('name')}
                onBlur={handleBlurName}
                error={errors.name || (isDuplicateName && errorDuplicateName)}
              />
            </div>
            <div className="mt-16 col-12 col-lg-8">
              <TextAreaCountDown
                id="addNewMethod__comment"
                dataTestId="addNewMethod__comment"
                label={t('txt_comment_area')}
                value={method?.comment || ''}
                onChange={handleFormChange('comment')}
                rows={4}
                maxLength={240}
              />
            </div>
          </div>
          <div className="mt-24">
            <ParameterGrid method={method} onChange={handleFormChange} />
          </div>
        </div>
      </ModalBody>
      <ModalFooter
        cancelButtonText={t('txt_cancel')}
        okButtonText={t('txt_save')}
        onCancel={handleClose}
        onOk={handleAddMethod}
        disabledOk={!isValid}
      >
        {!isNewMethod && (
          <Button
            className="mr-auto ml-n8"
            variant="outline-primary"
            onClick={handleBack}
          >
            {t('txt_back')}
          </Button>
        )}
      </ModalFooter>
    </ModalRegistry>
  );
};
const excludeTierFields = [
  MethodFieldParameterEnum.Tier3Balance,
  MethodFieldParameterEnum.Tier3Amount,
  MethodFieldParameterEnum.Tier4Balance,
  MethodFieldParameterEnum.Tier4Amount,
  MethodFieldParameterEnum.Tier5Balance,
  MethodFieldParameterEnum.Tier5Amount
];

export default AddNewMethodModal;
