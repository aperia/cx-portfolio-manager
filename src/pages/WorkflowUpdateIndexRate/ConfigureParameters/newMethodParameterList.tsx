import {
  MethodFieldNameEnum,
  MethodFieldParameterEnum
} from 'app/constants/enums';
import { useTranslation } from 'app/_libraries/_dls';
import React from 'react';

const CashIndexRateMoreInfo: React.FC = () => {
  const { t } = useTranslation();
  return (
    <div>
      <p>
        {t('txt_update_index_rate_step_3_cash_index_rate_more_info_desc_1')}
      </p>
      <p>
        {t('txt_update_index_rate_step_3_cash_index_rate_more_info_desc_2')}
      </p>
      <ul className="list-dot">
        <li>
          {t('txt_update_index_rate_step_3_cash_index_rate_more_info_li_1')}
        </li>
        <li>
          {t('txt_update_index_rate_step_3_cash_index_rate_more_info_li_2')}
        </li>
        <li>
          {t('txt_update_index_rate_step_3_cash_index_rate_more_info_li_3')}
        </li>
        <li>
          {t('txt_update_index_rate_step_3_cash_index_rate_more_info_li_4')}
        </li>
      </ul>
    </div>
  );
};

export const ParameterList = [
  {
    id: MethodFieldParameterEnum.UpdateIndexRateCashIndexRate,
    fieldName: MethodFieldNameEnum.UpdateIndexRateCashIndexRate,
    onlinePCF: MethodFieldNameEnum.UpdateIndexRateCashIndexRate,
    moreInfoText: 'txt_update_index_rate_step_3_cash_index_rate_more_info_desc',
    moreInfo: <CashIndexRateMoreInfo />
  },
  {
    id: MethodFieldParameterEnum.UpdateIndexRateEffectiveDateNewCash,
    fieldName: MethodFieldNameEnum.UpdateIndexRateEffectiveDateNewCash,
    onlinePCF: 'txt_update_index_rate_step_3_effective_date_new_cash_short',
    moreInfoText:
      'txt_update_index_rate_step_3_effective_date_new_cash_more_info'
  },
  {
    id: MethodFieldParameterEnum.UpdateIndexRatePreviousCashIndexRate,
    fieldName: MethodFieldNameEnum.UpdateIndexRatePreviousCashIndexRate,
    onlinePCF: 'txt_update_index_rate_step_3_previous_cash_index_rate_short',
    moreInfoText:
      'txt_update_index_rate_step_3_previous_cash_index_rate_more_info'
  },
  {
    id: MethodFieldParameterEnum.UpdateIndexRateMerchandiseIndexRate,
    fieldName: MethodFieldNameEnum.UpdateIndexRateMerchandiseIndexRate,
    onlinePCF: 'txt_update_index_rate_step_3_merchandise_index_rate_short',
    moreInfoText:
      'txt_update_index_rate_step_3_merchandise_index_rate_more_info'
  },
  {
    id: MethodFieldParameterEnum.UpdateIndexRateEffectiveDateNewMerchandise,
    fieldName: MethodFieldNameEnum.UpdateIndexRateEffectiveDateNewMerchandise,
    onlinePCF:
      'txt_update_index_rate_step_3_effective_date_new_merchandise_short',
    moreInfoText:
      'txt_update_index_rate_step_3_effective_date_new_merchandise_more_info'
  },
  {
    id: MethodFieldParameterEnum.UpdateIndexRatePreviousMerchandiseIndexRate,
    fieldName: MethodFieldNameEnum.UpdateIndexRatePreviousMerchandiseIndexRate,
    onlinePCF:
      'txt_update_index_rate_step_3_previous_merchandise_index_rate_short',
    moreInfoText:
      'txt_update_index_rate_step_3_previous_merchandise_index_rate_more_info'
  },
  {
    id: MethodFieldParameterEnum.UpdateIndexRateIndexType,
    fieldName: MethodFieldNameEnum.UpdateIndexRateIndexType,
    onlinePCF: 'txt_update_index_rate_step_3_index_type_short',
    moreInfoText: 'txt_update_index_rate_step_3_index_type_more_info'
  },
  {
    id: MethodFieldParameterEnum.UpdateIndexRateDeterminationDateDescription,
    fieldName: MethodFieldNameEnum.UpdateIndexRateDeterminationDateDescription,
    onlinePCF:
      'txt_update_index_rate_step_3_determination_date_description_short',
    moreInfoText:
      'txt_update_index_rate_step_3_determination_date_description_more_info'
  },
  {
    id: MethodFieldParameterEnum.UpdateIndexRateSourceDescription,
    fieldName: MethodFieldNameEnum.UpdateIndexRateSourceDescription,
    onlinePCF: 'txt_update_index_rate_step_3_source_description_short',
    moreInfoText: 'txt_update_index_rate_step_3_source_description_more_info'
  },
  {
    id: MethodFieldParameterEnum.UpdateIndexRateChangePeriod,
    fieldName: MethodFieldNameEnum.UpdateIndexRateChangePeriod,
    onlinePCF: 'txt_update_index_rate_step_3_change_period_short',
    moreInfoText: 'txt_update_index_rate_step_3_change_period_more_info'
  }
];
