import { fireEvent } from '@testing-library/dom';
import { renderWithMockStore } from 'app/utils';
import 'app/_libraries/_dls/test-utils/mocks/mockCanvas';
import React from 'react';
import AddNewMethodModal, {
  IAddNewMethodModalProps
} from './AddNewMethodModal';
const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

jest.mock('app/hooks/useUnsavedChangesRedirect', () => {
  return {
    __esModule: true,
    useUnsavedChangesRedirect:
      () =>
      ({ onConfirm, onDiscard, onCancel }: any) => {
        onDiscard && onDiscard();
        onCancel && onCancel();
        onConfirm && onConfirm(jest.fn());
      }
  };
});
describe('pages > WorkflowUpdateIndexRate > ConfigureParameters > AddNewMethodModal', () => {
  const renderComponent = async (props: IAddNewMethodModalProps) => {
    return await renderWithMockStore(<AddNewMethodModal {...props} />, {});
  };

  it('Should render component when create new method', async () => {
    const props: IAddNewMethodModalProps = {
      id: 'id',
      show: true,
      isCreate: undefined,
      method: undefined,
      draftMethod: undefined,
      methodNames: ['METHOD1'],
      onClose: jest.fn()
    };
    const wrapper = await renderComponent(props);
    expect(
      wrapper.getByText('txt_update_index_rate_step_3_method_details_title')
    ).toBeInTheDocument();

    // input a name
    const methodNameTextBox = wrapper.getByTestId(
      'addNewMethod__methodName_dls-text-box_input'
    );
    expect(methodNameTextBox).toBeInTheDocument();
    fireEvent.focus(methodNameTextBox);
    fireEvent.blur(methodNameTextBox);
    fireEvent.change(methodNameTextBox, { target: { value: 'METHOD@' } });
    fireEvent.change(methodNameTextBox, { target: { value: 'METHOD1' } });
    // input a comment
    const commentTextarea = wrapper.getByTestId(
      'addNewMethod__comment_dls-text-area_input'
    );
    expect(methodNameTextBox).toBeInTheDocument();
    fireEvent.change(commentTextarea, { target: { value: 'comment' } });

    // save method - dupplicate method name
    fireEvent.click(wrapper.getByText('txt_save'));
    expect(
      wrapper.getByText(
        'txt_update_index_rate_step_3_method_details_unique_name_error'
      )
    ).toBeInTheDocument();

    // Re-input method name and save
    fireEvent.change(methodNameTextBox, { target: { value: 'METHOD2' } });
    fireEvent.blur(methodNameTextBox);
    fireEvent.click(wrapper.getByText('txt_save'));
    expect(props.onClose).toBeCalled();
  });

  it('Should render component when create new version', async () => {
    const props: IAddNewMethodModalProps = {
      id: 'id',
      show: true,
      isCreate: true,
      method: {
        id: 'id',
        name: 'METHOD1',
        methodType: 'NEWVERSION',
        versionParameters: []
      } as any,
      draftMethod: undefined,
      methodNames: undefined,
      onClose: jest.fn()
    };
    const wrapper = await renderComponent(props);
    expect(
      wrapper.getByText('txt_update_index_rate_step_3_method_details_title')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('txt_back'));
    expect(props.onClose).toBeCalled();

    fireEvent.click(wrapper.getByText('txt_cancel'));
    expect(props.onClose).toBeCalled();
  });

  it('Should render component when choose method to model', async () => {
    const props: IAddNewMethodModalProps = {
      id: 'id',
      show: true,
      isCreate: false,
      method: {
        id: 'id',
        name: 'METHOD2',
        methodType: 'MODELEDMETHOD',
        modeledFrom: {
          name: 'METHOD1'
        } as any
      } as any,
      draftMethod: {
        id: 'id',
        name: 'METHOD2',
        methodType: 'MODELEDMETHOD',
        modeledFrom: {
          name: 'METHOD1'
        } as any
      } as any,
      methodNames: undefined,
      onClose: jest.fn()
    };
    const wrapper = await renderComponent(props);
    expect(
      wrapper.getByText('txt_update_index_rate_step_3_method_details_title')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('txt_save'));
    expect(props.onClose).toBeCalled();

    fireEvent.click(wrapper.getByText('txt_back'));
    expect(props.onClose).toBeCalled();

    fireEvent.click(wrapper.getByText('txt_cancel'));
    expect(props.onClose).toBeCalled();
  });

  it('Should render component when choose method to model with versionParameters', async () => {
    const props: IAddNewMethodModalProps = {
      id: 'id',
      show: true,
      isCreate: false,
      method: {
        id: 'id',
        name: 'METHOD2',
        versionParameters: '123',
        methodType: 'MODELEDMETHOD',
        modeledFrom: {
          name: 'METHOD1'
        } as any
      } as any,
      draftMethod: {
        id: 'id',
        name: 'METHOD2',
        methodType: 'MODELEDMETHOD',
        versionParameters: [],
        modeledFrom: {
          name: 'METHOD1'
        } as any
      } as any,
      methodNames: undefined,
      onClose: jest.fn()
    };
    const wrapper = await renderComponent(props);
    expect(
      wrapper.getByText('txt_update_index_rate_step_3_method_details_title')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('txt_save'));
    expect(props.onClose).toBeCalled();

    fireEvent.click(wrapper.getByText('txt_back'));
    expect(props.onClose).toBeCalled();

    fireEvent.click(wrapper.getByText('txt_cancel'));
    expect(props.onClose).toBeCalled();
  });
});
