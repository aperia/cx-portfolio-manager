import { fireEvent, render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import * as CommonSelectHooks from 'pages/_commons/redux/Common/select-hooks';
import React from 'react';
import ParameterGrid, { IParameterGridProps } from './ParameterGrid';

// mock Simple Search Component
jest.mock('app/components/SimpleSearch', () => {
  const ActualReact = jest.requireActual('react');
  return {
    __esModule: true,
    default: ActualReact.forwardRef(
      ({ onSearch }: any, ref: React.Ref<HTMLDivElement>) => {
        ActualReact.useImperativeHandle(ref, () => ({
          clear: () => {}
        }));
        return (
          <div>
            <div>SimpleSearch</div>
            <div onClick={() => onSearch('a')}>Search a</div>
            <div onClick={() => onSearch('xxx')}>Search xxx</div>
          </div>
        );
      }
    )
  };
});

const useSelectWindowDimensionMock = jest.spyOn(
  CommonSelectHooks,
  'useSelectWindowDimension'
);

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

describe('pages > WorkflowUpdateIndexRate > ConfigureParameters > ParameterGrid', () => {
  const renderComponent = (props: IParameterGridProps) => {
    return render(<ParameterGrid {...props} />);
  };
  it('Should render component with default values', () => {
    useSelectWindowDimensionMock.mockReturnValue({ width: 1024, height: 768 });
    const props: IParameterGridProps = {
      method: {} as any,
      onChange: jest.fn()
    };
    const { getByText } = renderComponent(props);
    expect(getByText('txt_parameter_list')).toBeInTheDocument();
  });

  it('Should render component with method values', () => {
    useSelectWindowDimensionMock.mockReturnValue({ width: 1280, height: 768 });
    const props: IParameterGridProps = {
      method: {
        id: 'id',
        [MethodFieldParameterEnum.UpdateIndexRateEffectiveDateNewCash]:
          '02/09/2022',
        [MethodFieldParameterEnum.UpdateIndexRateEffectiveDateNewMerchandise]:
          '02/08/2022'
      } as any,
      onChange: jest.fn()
    };
    const { getByText } = renderComponent(props);
    expect(getByText('txt_parameter_list')).toBeInTheDocument();

    // hover to more info icon
    const firstMoreInfoIcon = document.querySelector('.icon.icon-information');
    userEvent.hover(firstMoreInfoIcon!);
    expect(
      getByText('txt_update_index_rate_step_3_cash_index_rate_more_info_desc_1')
    ).toBeInTheDocument();
  });

  it('Should render component with search value has data', () => {
    useSelectWindowDimensionMock.mockReturnValue({ width: 1280, height: 768 });
    const props: IParameterGridProps = {
      method: {
        id: 'id',
        [MethodFieldParameterEnum.UpdateIndexRateEffectiveDateNewCash]:
          '02/09/2022',
        [MethodFieldParameterEnum.UpdateIndexRateEffectiveDateNewMerchandise]:
          '02/08/2022'
      } as any,
      onChange: jest.fn()
    };
    const { getByText } = renderComponent(props);
    expect(getByText('txt_parameter_list')).toBeInTheDocument();
    expect(getByText('SimpleSearch')).toBeInTheDocument();

    // search has data
    fireEvent.click(getByText('Search a'));
    expect(getByText('txt_clear_and_reset')).toBeInTheDocument();
    fireEvent.click(getByText('txt_clear_and_reset'));

    // search has NO data
    fireEvent.click(getByText('Search xxx'));
    expect(getByText('txt_clear_and_reset')).toBeInTheDocument();
    fireEvent.click(getByText('txt_clear_and_reset'));
  });
});
