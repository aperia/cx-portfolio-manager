import EnhanceDatePicker from 'app/components/EnhanceDatePicker';
import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch, { SimpleSearchPublic } from 'app/components/SimpleSearch';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import { isValidDate, matchSearchValue } from 'app/helpers';
import {
  ColumnType,
  Grid,
  NumericTextBox,
  TextBox,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import ClearAndResetButton from 'pages/_commons/Utils/ClearAndResetButton';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React, { useEffect, useMemo, useRef, useState } from 'react';
import { ParameterList } from './newMethodParameterList';

export interface IParameterGridProps {
  method: WorkflowSetupMethodObjectParams;
  onChange: (
    field: keyof WorkflowSetupMethodObjectParams,
    isRefData?: boolean
  ) => (e: any) => void;
}
const ParameterGrid: React.FC<IParameterGridProps> = ({ method, onChange }) => {
  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();

  const simpleSearchRef = useRef<SimpleSearchPublic>(null);
  const [searchValue, setSearchValue] = useState<string>('');

  useEffect(() => {
    !searchValue && simpleSearchRef.current?.clear();
  }, [searchValue]);

  const data = useMemo(
    () =>
      ParameterList.filter(p =>
        matchSearchValue(
          [t(p.fieldName), t(p.moreInfoText), t(p.onlinePCF)],
          searchValue
        )
      ),
    [searchValue, t]
  );

  const valueAccessor = (data: Record<string, any>) => {
    const id = data.id as MethodFieldParameterEnum;
    const value = method[id as keyof WorkflowSetupMethodObjectParams] as any;
    switch (id) {
      case MethodFieldParameterEnum.UpdateIndexRateCashIndexRate:
        return (
          <NumericTextBox
            dataTestId={id}
            value={value}
            onChange={onChange(id)}
            onBlur={onChange(id)}
            size="sm"
            format="p3"
            maxLength={6}
            max={99.999}
            negative={true}
            showStep={false}
          />
        );

      case MethodFieldParameterEnum.UpdateIndexRateEffectiveDateNewCash:
        return (
          <EnhanceDatePicker
            size="small"
            dataTestId={id}
            value={isValidDate(value) ? new Date(value!) : undefined}
            readOnly
          />
        );

      case MethodFieldParameterEnum.UpdateIndexRatePreviousCashIndexRate:
        return (
          <NumericTextBox
            dataTestId={id}
            value={value}
            onChange={onChange(id)}
            size="sm"
            format="p3"
            max={99.999}
            showStep={false}
            readOnly
          />
        );

      case MethodFieldParameterEnum.UpdateIndexRateMerchandiseIndexRate:
        return (
          <NumericTextBox
            dataTestId={id}
            value={value}
            onChange={onChange(id)}
            onBlur={onChange(id)}
            size="sm"
            format="p3"
            maxLength={6}
            max={99.999}
            negative={true}
            showStep={false}
          />
        );

      case MethodFieldParameterEnum.UpdateIndexRateEffectiveDateNewMerchandise:
        return (
          <EnhanceDatePicker
            size="small"
            dataTestId={id}
            value={isValidDate(value) ? new Date(value!) : undefined}
            readOnly
          />
        );

      case MethodFieldParameterEnum.UpdateIndexRatePreviousMerchandiseIndexRate:
        return (
          <NumericTextBox
            dataTestId={id}
            value={value}
            onChange={onChange(id)}
            size="sm"
            format="p3"
            max={99.999}
            showStep={false}
            readOnly
          />
        );

      case MethodFieldParameterEnum.UpdateIndexRateIndexType:
        return (
          <TextBox
            dataTestId={id}
            size="sm"
            value={value}
            onChange={onChange(id)}
            maxLength={15}
            placeholder={t('txt_enter_a_value')}
          />
        );

      case MethodFieldParameterEnum.UpdateIndexRateDeterminationDateDescription:
        return (
          <TextBox
            dataTestId={id}
            size="sm"
            value={value}
            onChange={onChange(id)}
            maxLength={20}
            placeholder={t('txt_enter_a_value')}
          />
        );

      case MethodFieldParameterEnum.UpdateIndexRateSourceDescription:
        return (
          <TextBox
            dataTestId={id}
            size="sm"
            value={value}
            onChange={onChange(id)}
            maxLength={20}
            placeholder={t('txt_enter_a_value')}
          />
        );

      case MethodFieldParameterEnum.UpdateIndexRateChangePeriod:
        return (
          <TextBox
            dataTestId={id}
            size="sm"
            value={value}
            onChange={onChange(id)}
            maxLength={10}
            placeholder={t('txt_enter_a_value')}
          />
        );
    }
  };

  const columns: ColumnType[] = [
    {
      id: 'fieldName',
      Header: t('txt_business_name'),
      accessor: row => t(row.fieldName)
    },
    {
      id: 'onlinePCF',
      Header: t('txt_manage_penalty_fee_online_PCF'),
      accessor: row => t(row.onlinePCF)
    },
    {
      id: 'value',
      Header: t('txt_value'),
      accessor: valueAccessor,
      width: width < 1280 ? 240 : undefined,
      cellBodyProps: { className: 'control' }
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      accessor: rowData =>
        rowData.moreInfo
          ? viewMoreInfo(['moreInfo'], t)(rowData)
          : viewMoreInfo(['moreInfoText'], t)(rowData),
      width: 105
    }
  ];

  const handleSearch = (searchValue: string) => {
    setSearchValue(searchValue);
  };

  const handleClearFilter = () => {
    setSearchValue('');
  };

  return (
    <>
      <div className="d-flex align-items-center justify-content-between mb-16">
        <h5>{t('txt_parameter_list')}</h5>
        <SimpleSearch
          ref={simpleSearchRef}
          placeholder={t('txt_type_to_search')}
          onSearch={handleSearch}
          popperElement={
            <>
              <p className="color-grey">{t('txt_search_description')}</p>
              <ul className="search-field-item list-unstyled">
                <li className="mt-16">{t('txt_business_name')}</li>
                <li className="mt-16">{t('txt_more_info')}</li>
                <li className="mt-16">{t('txt_online_pcf')}</li>
              </ul>
            </>
          }
        />
      </div>
      {searchValue && !isEmpty(data) && (
        <div className="d-flex justify-content-end mb-16 mr-n8">
          <ClearAndResetButton small onClearAndReset={handleClearFilter} />
        </div>
      )}
      {isEmpty(data) ? (
        <div className="d-flex flex-column justify-content-center mt-40">
          <NoDataFound
            id="newMethod_notfound"
            hasSearch
            title={t('txt_no_results_found')}
            linkTitle={t('txt_clear_and_reset')}
            onLinkClicked={handleClearFilter}
          />
        </div>
      ) : (
        <Grid
          className="grid-has-control grid-has-tooltip center"
          columns={columns}
          data={data}
        />
      )}
    </>
  );
};

export default ParameterGrid;
