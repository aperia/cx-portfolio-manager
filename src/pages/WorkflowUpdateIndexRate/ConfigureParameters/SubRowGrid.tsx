import { FormatTime, MethodFieldParameterEnum } from 'app/constants/enums';
import { formatTimeDefault } from 'app/helpers';
import {
  ColumnType,
  Grid,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React, { useCallback, useMemo } from 'react';
import { ParameterList as data } from './newMethodParameterList';

export interface SubGridProps {
  original: any;
}
const SubRowGrid: React.FC<SubGridProps> = ({ original }) => {
  const { t } = useTranslation();

  const formatPercent = (
    text: string | number | undefined,
    format = 3 as number
  ) => {
    if (!text) return '';
    return `${parseFloat(`${text}`)
      .toFixed(format)
      .replace(/\d(?=(\d{3})+\.)/g, '$&,')}%`;
  };

  const valueAccessor = useCallback(
    (data: Record<string, any>) => {
      let valueTruncate: any = '';

      const paramId = data.id as MethodFieldParameterEnum;
      const value = original?.parameters?.find(
        (p: any) => p.name === paramId
      )?.newValue;

      switch (paramId) {
        case MethodFieldParameterEnum.UpdateIndexRateCashIndexRate: {
          valueTruncate = formatPercent(value);
          break;
        }
        case MethodFieldParameterEnum.UpdateIndexRateEffectiveDateNewCash: {
          valueTruncate = value
            ? formatTimeDefault(new Date(value).toString(), FormatTime.Date)
            : '';
          break;
        }
        case MethodFieldParameterEnum.UpdateIndexRatePreviousCashIndexRate: {
          valueTruncate = formatPercent(value);
          break;
        }
        case MethodFieldParameterEnum.UpdateIndexRateMerchandiseIndexRate: {
          valueTruncate = formatPercent(value);
          break;
        }
        case MethodFieldParameterEnum.UpdateIndexRateEffectiveDateNewMerchandise: {
          valueTruncate = value
            ? formatTimeDefault(new Date(value).toString(), FormatTime.Date)
            : '';
          break;
        }
        case MethodFieldParameterEnum.UpdateIndexRatePreviousMerchandiseIndexRate: {
          valueTruncate = formatPercent(value);
          break;
        }
        default:
          valueTruncate = value;
          break;
      }
      return (
        <TruncateText
          lines={2}
          ellipsisLessText={t('txt_less')}
          ellipsisMoreText={t('txt_more')}
        >
          {valueTruncate}
        </TruncateText>
      );
    },
    [t, original]
  );

  const columns: ColumnType[] = useMemo(
    () => [
      {
        id: 'fieldName',
        Header: t('txt_business_name'),
        accessor: ({ fieldName }) => (
          <TruncateText
            resizable
            lines={2}
            title={t(fieldName)}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {t(fieldName)}
          </TruncateText>
        )
      },
      {
        id: 'onlinePCF',
        Header: t('txt_online_pcf'),
        accessor: ({ onlinePCF }) => (
          <TruncateText
            resizable
            lines={2}
            title={t(onlinePCF)}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {t(onlinePCF)}
          </TruncateText>
        )
      },
      {
        id: 'value',
        Header: t('txt_value'),
        accessor: valueAccessor
      },
      {
        id: 'moreInfo',
        Header: t('txt_more_info'),
        className: 'text-center',
        accessor: rowData =>
          rowData.moreInfo
            ? viewMoreInfo(['moreInfo'], t)(rowData)
            : viewMoreInfo(['moreInfoText'], t)(rowData),
        width: 105
      }
    ],
    [t, valueAccessor]
  );

  return (
    <div className="p-20 overflow-hidden">
      <Grid className="grid-has-tooltip center" columns={columns} data={data} />
    </div>
  );
};

export default SubRowGrid;
