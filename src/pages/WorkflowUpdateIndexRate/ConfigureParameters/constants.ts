import { AddMethodButtons } from 'app/constants/enums';

export const ACTION_BUTTONS = [
  {
    value: AddMethodButtons.CREATE_NEW_VERSION,
    description: 'txt_update_index_rate_step_3_option_1'
  },
  {
    value: AddMethodButtons.CHOOSE_METHOD_TO_MODEL,
    description: 'txt_update_index_rate_step_3_option_2'
  },
  {
    value: AddMethodButtons.ADD_NEW_METHOD,
    description: 'txt_update_index_rate_step_3_option_3'
  }
];
