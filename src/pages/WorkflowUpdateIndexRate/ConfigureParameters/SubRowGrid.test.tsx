import { renderComponent } from 'app/utils';
import React from 'react';
import SubRowGrid from './SubRowGrid';

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

describe('pages > WorkflowUpdateIndexRate > ConfigureParameters > SubRowGrids', () => {
  it('Should render with percent text and date text', async () => {
    const original = {
      parameters: [
        {
          name: 'cash.index.rate',
          id: 'cash.index.rate',
          newValue: 10.2
        },
        {
          name: 'effective.date.for.new.cash.index.rate',
          id: 'effective.date.for.new.cash.index.rate',
          newValue: '09/01/2021'
        }
      ]
    };
    const wrapper = await renderComponent(
      'testId',
      <SubRowGrid original={original} />
    );
    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
  });

  it('Should render with normal text', async () => {
    const original = {
      parameters: [
        {
          name: 'previous.cash.index.rate',
          id: 'previous.cash.index.rate',
          newValue: 'aaaa'
        },
        {
          name: 'merchandise.index.rate',
          id: 'merchandise.index.rate',
          newValue: 'aaaa'
        },
        {
          name: 'effective.date.for.new.merchandise.index.rate',
          id: 'effective.date.for.new.merchandise.index.rate',
          newValue: 'aaaa'
        },
        { name: 'index.type', id: 'index.type', newValue: 'aaaa' },
        {
          name: 'determination.date.description',
          id: 'determination.date.description',
          newValue: 'aaaa'
        },
        {
          name: 'source.description',
          id: 'source.description',
          newValue: 'aaaa'
        },
        { name: 'change.period', id: 'change.period', newValue: 'aaaa' }
      ]
    };
    const wrapper = await renderComponent(
      'testId',
      <SubRowGrid original={original} />
    );
    expect(wrapper.getByText('txt_online_pcf')).toBeInTheDocument();
  });

  it('Should render with normal text', async () => {
    const original = {
      parameters: [
        {
          name: 'previous.cash.index.rate',
          id: 'previous.cash.index.rate',
          newValue: 'aaaa'
        },
        {
          name: 'merchandise.index.rate',
          id: 'merchandise.index.rate',
          newValue: 'aaaa'
        },
        {
          name: 'effective.date.for.new.merchandise.index.rate',
          id: 'effective.date.for.new.merchandise.index.rate',
          newValue: '12/12/2022'
        },
        { name: 'index.type', id: 'index.type', newValue: '12/12/2022' },
        {
          name: 'effective.date.for.new.cash.index.rate',
          id: 'effective.date.for.new.cash.index.rate',
          newValue: '12/12/2022'
        },
        {
          name: 'determination.date.description',
          id: 'determination.date.description',
          newValue: 'aaaa'
        },
        {
          name: 'source.description',
          id: 'source.description',
          newValue: 'aaaa'
        },
        { name: 'change.period', id: 'change.period', newValue: 'aaaa' }
      ]
    };

    const wrapper = await renderComponent(
      'testId',
      <SubRowGrid original={original} />
    );
    expect(wrapper.getByText('txt_online_pcf')).toBeInTheDocument();
  });
});
