import EnhanceDropdownButton from 'app/components/EnhanceDropdownButton';
import {
  AddMethodButtons,
  BadgeColorType,
  ServiceSubjectSection
} from 'app/constants/enums';
import {
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { mapGridExpandCollapse, mappingArrayKey } from 'app/helpers';
import { usePaginationGrid, useUnsavedChangeRegistry } from 'app/hooks';
import {
  Button,
  ColumnType,
  DropdownButton,
  DropdownButtonSelectEvent,
  Grid,
  Icon,
  InlineMessage,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty, orderBy } from 'lodash';
import {
  formatBadge,
  formatText,
  formatTruncate
} from 'pages/_commons/Utils/formatGridField';
import Paging from 'pages/_commons/Utils/Paging';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, {
  Fragment,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import AddNewMethodModal from './AddNewMethodModal';
import { ACTION_BUTTONS } from './constants';
import MethodListModal from './MethodListModal';
import parseFormValues from './parseFormValues';
import RemoveMethodModal from './RemoveMethodModal';
import SubRowGrid from './SubRowGrid';
import Summary from './Summary';

export interface UpdateIndexRateFormValue {
  isValid?: boolean;
  methods?: any;
}

const UpdateIndexRate: React.FC<WorkflowSetupProps<UpdateIndexRateFormValue>> =
  ({ stepId, selectedStep, savedAt, formValues, isStuck, setFormValues }) => {
    const { t } = useTranslation();

    const keepRef = useRef({ setFormValues });
    keepRef.current.setFormValues = setFormValues;

    const [expandedList, setExpandedList] = useState<string[]>([]);
    const [editMethodRowId, setEditMethodRowId] =
      useState<undefined | number>(undefined);

    const [openCreateNewVersion, setOpenCreateNewVersion] =
      useState<boolean>(false);

    const [removeMethodRowId, setRemoveMethodRowId] =
      useState<undefined | number>(undefined);
    const [openMethodToModel, setOpenMethodToModel] = useState<boolean>(false);
    const [openAddNewMethod, setOpenAddNewMethod] = useState<boolean>(false);
    const [editMethodBackId, setEditMethodBackId] =
      useState<undefined | number>(undefined);
    const [hasChange, setHasChange] = useState(false);

    const prevExpandedList = useRef<string[]>();

    useEffect(() => {
      prevExpandedList.current = expandedList;
    }, [expandedList]);

    useEffect(() => {
      const isValid = (formValues?.methods?.length || 0) > 0;
      if (formValues.isValid === isValid) return;

      keepRef.current.setFormValues({ isValid });
    }, [formValues]);

    useUnsavedChangeRegistry(
      {
        ...unsavedExistedWorkflowSetupDataProps,
        formName: UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__UPDATE_INDEX_RATE,
        priority: 1
      },
      [hasChange]
    );

    const methods = useMemo(
      () => orderBy(formValues?.methods || [], ['name'], ['asc']),
      [formValues?.methods]
    );

    const onEditMethod = useCallback((idx: number) => {
      setEditMethodRowId(idx);
    }, []);

    const handleSelectAction = useCallback(
      (event: DropdownButtonSelectEvent) => {
        const { value } = event.target.value;
        switch (value) {
          case AddMethodButtons.CREATE_NEW_VERSION:
            handleCreateNewVersion();
            break;
          case AddMethodButtons.ADD_NEW_METHOD:
            handleAddNewMethod();
            break;
          case AddMethodButtons.CHOOSE_METHOD_TO_MODEL:
            handleMethodToModel();
            break;
        }
      },
      []
    );

    const dropdownListItem = useMemo(() => {
      return ACTION_BUTTONS.map(item => (
        <DropdownButton.Item
          key={item.value}
          label={t(item.description)}
          value={item}
        />
      ));
    }, [t]);

    const methodNames = useMemo(
      () => mappingArrayKey(methods, 'name'),
      [methods]
    );

    const methodTypeToText = useCallback(
      (type: WorkflowSetupMethodType) => {
        switch (type) {
          case 'NEWVERSION':
            return t('txt_manage_penalty_fee_version_created');
          case 'MODELEDMETHOD':
            return t('txt_manage_penalty_fee_method_modeled');
        }
        return t('txt_manage_penalty_fee_method_created');
      },
      [t]
    );

    const handleAddNewMethod = () => {
      setOpenAddNewMethod(true);
    };
    const handleCreateNewVersion = () => {
      setOpenCreateNewVersion(true);
    };
    const handleMethodToModel = () => {
      setOpenMethodToModel(true);
    };
    const closeMethodListModalModal = () => {
      setOpenCreateNewVersion(false);
      setOpenMethodToModel(false);
    };

    const onRemoveMethod = useCallback((idx: number) => {
      setRemoveMethodRowId(idx);
    }, []);

    const handleOnExpand = (dataKeyList: string[]) => {
      setExpandedList(isEmpty(dataKeyList) ? [] : dataKeyList);
    };

    const {
      total,
      currentPage,
      currentPageSize,
      pageData,
      onPageChange,
      onPageSizeChange,
      onResetToDefaultFilter
    } = usePaginationGrid(methods);

    useEffect(() => {
      if (stepId !== selectedStep) {
        setExpandedList([]);
        onResetToDefaultFilter();
      }
      setHasChange(false);
    }, [onResetToDefaultFilter, stepId, selectedStep, savedAt]);

    const columns: ColumnType[] = useMemo(
      () => [
        {
          id: 'methodName',
          Header: t('txt_method_name'),
          accessor: formatText(['name']),
          width: 120
        },
        {
          id: 'modelOrCreateFrom',
          Header: t('txt_manage_penalty_fee_modeled_or_create_from'),
          accessor: formatText(['modeledFrom', 'name']),
          width: 120
        },
        {
          id: 'comment',
          Header: t('txt_comment_area'),
          accessor: formatTruncate(['comment']),
          width: 312
        },
        {
          id: 'methodType',
          Header: t('txt_manage_penalty_fee_action_taken'),
          accessor: (data, idx) =>
            formatBadge(['methodType'], {
              colorType: BadgeColorType.MethodType,
              noBorder: true
            })(
              {
                ...data,
                methodType: methodTypeToText(data.methodType)
              },
              idx
            ),
          width: 153
        },
        {
          id: 'actions',
          Header: t('txt_actions'),
          accessor: (data: any) => (
            <div className="d-flex justify-content-center">
              <Button
                size="sm"
                variant="outline-primary"
                onClick={() => onEditMethod(data.rowId)}
              >
                {t('txt_edit')}
              </Button>
              <Button
                size="sm"
                variant="outline-danger"
                className="ml-8"
                onClick={() => onRemoveMethod(data.rowId)}
              >
                {t('txt_delete')}
              </Button>
            </div>
          ),
          className: 'text-center',
          cellBodyProps: { className: 'py-8' },
          width: 132
        }
      ],
      [t, methodTypeToText, onEditMethod, onRemoveMethod]
    );

    const renderHasMethod = useMemo(() => {
      if (isEmpty(pageData)) return;

      return (
        <div>
          <div className="d-flex align-items-center">
            <h5>{t('txt_update_index_rate_step_3_method_list')}</h5>
            <div className="d-flex ml-auto mr-n8">
              <EnhanceDropdownButton
                buttonProps={{
                  size: 'sm',
                  children: t('txt_update_index_rate_step_3_add_method'),
                  variant: 'outline-primary'
                }}
                onSelect={handleSelectAction}
              >
                {dropdownListItem}
              </EnhanceDropdownButton>
            </div>
          </div>
          <div className="pt-16">
            <Grid
              togglable
              columns={columns}
              data={pageData}
              subRow={({ original }: any) => <SubRowGrid original={original} />}
              dataItemKey="rowId"
              toggleButtonConfigList={pageData.map(
                mapGridExpandCollapse('rowId', t)
              )}
              expandedItemKey={'rowId'}
              expandedList={expandedList}
              onExpand={handleOnExpand}
              scrollable
              className="grid-has-tooltip center"
            />
            {total > 10 && (
              <Paging
                totalItem={total}
                pageSize={currentPageSize}
                page={currentPage}
                onChangePage={onPageChange}
                onChangePageSize={onPageSizeChange}
              />
            )}
          </div>
        </div>
      );
    }, [
      dropdownListItem,
      handleSelectAction,
      columns,
      currentPage,
      currentPageSize,
      expandedList,
      onPageChange,
      onPageSizeChange,
      pageData,
      t,
      total
    ]);

    const renderNoMethod = useMemo(() => {
      return (
        <Fragment>
          <h5>{t('txt_update_index_rate_step_3_select_option')}</h5>
          <div className="row mt-16">
            <div className="col-xl col-6">
              <div
                className="rcc-btn d-flex justify-content-between border rounded-lg py-10"
                onClick={handleCreateNewVersion}
              >
                <span className="d-flex align-items-center ml-16">
                  <Icon
                    name="method-create"
                    size="9x"
                    className="color-grey-l16"
                  />
                  <span className="ml-12 mr-8">
                    {t('txt_update_index_rate_step_3_option_1')}
                  </span>
                  <Tooltip
                    element={t('txt_update_index_rate_step_3_option_1_tooltip')}
                    triggerClassName="d-flex"
                  >
                    <Icon
                      name="information"
                      className="color-grey-l16"
                      size="5x"
                    />
                  </Tooltip>
                </span>
              </div>
            </div>
            <div className="col-xl col-6">
              <div
                className="rcc-btn d-flex justify-content-between border rounded-lg py-10"
                onClick={handleMethodToModel}
              >
                <span className="d-flex align-items-center ml-16">
                  <Icon
                    name="method-clone"
                    size="9x"
                    className="color-grey-l16"
                  />
                  <span className="ml-12 mr-8">
                    {t('txt_update_index_rate_step_3_option_2')}
                  </span>
                  <Tooltip
                    element={t('txt_update_index_rate_step_3_option_2_tooltip')}
                    triggerClassName="d-flex"
                  >
                    <Icon
                      name="information"
                      className="color-grey-l16"
                      size="5x"
                    />
                  </Tooltip>
                </span>
              </div>
            </div>
            <div className="col-xl col-6 mt-xl-0 mt-16">
              <div
                className="rcc-btn d-flex justify-content-between border rounded-lg py-10"
                onClick={handleAddNewMethod}
              >
                <span className="d-flex align-items-center ml-16">
                  <Icon
                    name="method-add"
                    size="9x"
                    className="color-grey-l16"
                  />
                  <span className="ml-12 mr-8">
                    {t('txt_update_index_rate_step_3_option_3')}
                  </span>
                  <Tooltip
                    element={t('txt_update_index_rate_step_3_option_3_tooltip')}
                    triggerClassName="d-flex"
                  >
                    <Icon
                      name="information"
                      className="color-grey-l16"
                      size="5x"
                    />
                  </Tooltip>
                </span>
              </div>
            </div>
          </div>
        </Fragment>
      );
    }, [t]);

    return (
      <div>
        <div className="mt-8 pb-24 border-bottom color-grey">
          <p>{t('txt_update_index_rate_step_3_intro_text')}</p>
        </div>

        {isStuck && (
          <InlineMessage className="mb-0 mt-24" variant="danger" withIcon>
            {t('txt_step_stuck_move_forward_message')}
          </InlineMessage>
        )}
        <div className="mt-24">
          {isEmpty(methods) ? renderNoMethod : renderHasMethod}
        </div>
        {removeMethodRowId !== undefined && (
          <RemoveMethodModal
            id="removeMethod"
            show
            methodName={methods.find(i => i.rowId === removeMethodRowId)?.name}
            onClose={method => {
              if (method) {
                setFormValues({
                  methods: methods.filter(i => i.rowId !== removeMethodRowId)
                });
                setHasChange(true);
              }
              setRemoveMethodRowId(undefined);
            }}
          />
        )}
        {editMethodRowId !== undefined && (
          <AddNewMethodModal
            id="addNewMethod"
            show
            isCreate={false}
            method={methods.find(i => i.rowId === editMethodRowId)}
            methodNames={methodNames}
            onClose={(method, isBack) => {
              if (method) {
                method.serviceSubjectSection = ServiceSubjectSection.UIR;
                const newMethds = methods.map(item => {
                  if (item.rowId === editMethodRowId) return method;
                  return item;
                });
                setFormValues({ methods: newMethds });
                setHasChange(true);
                isBack && setEditMethodBackId(editMethodRowId);
              }
              setEditMethodRowId(undefined);
            }}
          />
        )}
        {editMethodBackId && (
          <MethodListModal
            id="editMethodFromModel"
            show
            methodNames={methodNames}
            isCreateNewVersion={
              methods.find(i => i.rowId === editMethodBackId)!.methodType ===
              'NEWVERSION'
            }
            defaultMethod={methods.find(i => i.rowId === editMethodBackId)}
            onClose={method => {
              if (method) {
                method.serviceSubjectSection = ServiceSubjectSection.UIR;
                const newMethds = methods.map(item => {
                  if (item.rowId === editMethodBackId) return method;
                  return item;
                });
                setFormValues({ methods: newMethds });
                setHasChange(true);
              }
              setEditMethodBackId(undefined);
            }}
          />
        )}
        {(openCreateNewVersion || openMethodToModel) && (
          <MethodListModal
            id="addMethodFromModel"
            show
            methodNames={methodNames}
            isCreateNewVersion={openCreateNewVersion}
            onClose={newMethod => {
              if (newMethod) {
                const rowId = Date.now();
                setFormValues({
                  methods: [
                    {
                      ...newMethod,
                      rowId,
                      serviceSubjectSection: ServiceSubjectSection.UIR
                    },
                    ...methods
                  ]
                });
                setExpandedList([rowId] as any);
                setHasChange(true);
              }
              closeMethodListModalModal();
            }}
          />
        )}
        {openAddNewMethod && (
          <AddNewMethodModal
            id="addNewMethod"
            show
            methodNames={methodNames}
            onClose={method => {
              if (method) {
                const rowId = Date.now();
                setFormValues({
                  methods: [
                    {
                      ...method,
                      rowId,
                      serviceSubjectSection: ServiceSubjectSection.UIR
                    },
                    ...methods
                  ]
                });
                setExpandedList([rowId] as any);
                setHasChange(true);
              }
              setOpenAddNewMethod(false);
            }}
          />
        )}
      </div>
    );
  };

const ExtraStaticUpdateIndexRateStep =
  UpdateIndexRate as WorkflowSetupStaticProp;

ExtraStaticUpdateIndexRateStep.summaryComponent = Summary;
ExtraStaticUpdateIndexRateStep.defaultValues = {
  isValid: false,
  methods: []
};

ExtraStaticUpdateIndexRateStep.parseFormValues = parseFormValues;

export default ExtraStaticUpdateIndexRateStep;
