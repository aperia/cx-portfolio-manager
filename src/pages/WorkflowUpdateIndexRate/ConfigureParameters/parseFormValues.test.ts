import { ServiceSubjectSection } from 'app/constants/enums';
import parseFormValues from './parseFormValues';

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

describe('pages > WorkflowUpdateIndexRate > ConfigureParameters > parseFormValues', () => {
  it('should parse Data has step values', () => {
    const configurations: any = {
      methodList: [
        { id: 'id', serviceSubjectSection: ServiceSubjectSection.UIR }
      ]
    };
    const id = 'step3';
    const input: any = {
      data: {
        configurations,
        workflowSetupData: [{ id, status: 'DONE' }]
      }
    };

    const result = parseFormValues(input, id, jest.fn);
    expect(result).toBeTruthy();
  });
  it('should parse Data has step values with empty', () => {
    const configurations: any = {};
    const id = 'step3';
    const input: any = {
      data: {
        configurations,
        workflowSetupData: [{ id, status: 'DONE' }]
      }
    };

    const result = parseFormValues(input, id, jest.fn);
    expect(result).toEqual({
      isPass: true,
      isSelected: false,
      isValid: false,
      methods: []
    });
  });

  it('should parse Data is undefined', () => {
    const configurations: any = {};
    const id = 'step';
    const input: any = {
      data: {
        configurations,
        workflowSetupData: [
          {
            id: 'stop',
            status: 'DONE'
          }
        ]
      }
    };
    const result = parseFormValues(input, id, jest.fn);
    expect(result).toEqual(undefined);
  });
});
