import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { default as stepRegistry } from '../_commons/WorkflowSetup/registries/stepRegistry';

const registerFunc = jest.spyOn(stepRegistry, 'registerStep');

describe('pages > WorkflowUpdateIndexRate > registerSteps', () => {
  it('Should register components', async () => {
    await import('./registerSteps');

    expect(registerFunc).toBeCalledWith(
      'GetStartedUpdateIndexRateWorkflow',
      expect.anything()
    );

    expect(registerFunc).toBeCalledWith(
      'ConfigureParameterUpdateIndexRateWorkflow',
      expect.anything(),
      [UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__UPDATE_INDEX_RATE]
    );
  });
});
