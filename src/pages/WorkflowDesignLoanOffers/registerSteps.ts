import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { stepRegistry } from '../_commons/WorkflowSetup/registries';
import DesignLoanOffersStep from './DesignLoanOffersStep';
import GettingStartStep from './GettingStartStep';

stepRegistry.registerStep('GetStartedDesignLoanOffers', GettingStartStep);

stepRegistry.registerStep('DesignLoanOffers', DesignLoanOffersStep, [
  UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__DESIGN_LOAN_OFFERS__PARAMETERS
]);
