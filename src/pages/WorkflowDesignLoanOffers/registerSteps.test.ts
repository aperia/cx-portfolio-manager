import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { default as stepRegistry } from '../_commons/WorkflowSetup/registries/stepRegistry';

const registerFunc = jest.spyOn(stepRegistry, 'registerStep');

describe('FlexibleMonetaryTransactions > registerSteps', () => {
  it('Should register components', async () => {
    await import('./registerSteps');

    expect(registerFunc).toBeCalledWith(
      'GetStartedDesignLoanOffers',
      expect.anything()
    );

    expect(registerFunc).toBeCalledWith('DesignLoanOffers', expect.anything(), [
      UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__DESIGN_LOAN_OFFERS__PARAMETERS
    ]);
  });
});
