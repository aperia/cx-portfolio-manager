import {
  MethodFieldNameEnum,
  MethodFieldParameterEnum
} from 'app/constants/enums';
import React from 'react';

export type ParameterListIds =
  | 'Info_LoanOfferBasicSettings'
  | 'Info_PaymentAndPricingSettings'
  | 'Info_MerchantDiscount'
  | 'Adv_MiscellaneousControls'
  | 'Adv_IntroductoryAprSettings'
  | 'Adv_PromotionalCashSettings'
  | 'Adv_PaymentDelaySettings';

export interface ParameterList {
  id: ParameterListIds;
  section:
    | 'txt_design_loan_offers_standard_parameters'
    | 'txt_design_loan_offers_advanced_parameters';
  parameterGroup?:
    | 'txt_design_loan_offers_loan_offer_basic_settings'
    | 'txt_design_loan_offers_payment_and_pricing_settings'
    | 'txt_design_loan_offers_merchant_discount'
    | 'txt_design_loan_offers_miscellaneous_controls'
    | 'txt_design_loan_offers_introductory_apr_settings'
    | 'txt_design_loan_offers_promotional_cash_settings'
    | 'txt_design_loan_offers_payment_delay_settings';
  moreInfo?: React.ReactNode;
}

export const parameterList: ParameterList[] = [
  {
    id: 'Info_LoanOfferBasicSettings',
    section: 'txt_design_loan_offers_standard_parameters',
    parameterGroup: 'txt_design_loan_offers_loan_offer_basic_settings'
  },
  {
    id: 'Info_PaymentAndPricingSettings',
    section: 'txt_design_loan_offers_standard_parameters',
    parameterGroup: 'txt_design_loan_offers_payment_and_pricing_settings'
  },
  {
    id: 'Info_MerchantDiscount',
    section: 'txt_design_loan_offers_standard_parameters',
    parameterGroup: 'txt_design_loan_offers_merchant_discount',
    moreInfo: 'txt_design_loan_offer_merchant_discount_tooltip'
  },
  {
    id: 'Adv_MiscellaneousControls',
    section: 'txt_design_loan_offers_advanced_parameters',
    parameterGroup: 'txt_design_loan_offers_miscellaneous_controls'
  },
  {
    id: 'Adv_IntroductoryAprSettings',
    section: 'txt_design_loan_offers_advanced_parameters',
    parameterGroup: 'txt_design_loan_offers_introductory_apr_settings'
  },
  {
    id: 'Adv_PromotionalCashSettings',
    section: 'txt_design_loan_offers_advanced_parameters',
    parameterGroup: 'txt_design_loan_offers_promotional_cash_settings'
  },
  {
    id: 'Adv_PaymentDelaySettings',
    section: 'txt_design_loan_offers_advanced_parameters',
    parameterGroup: 'txt_design_loan_offers_payment_delay_settings'
  }
];

export interface ParameterGroup {
  id: MethodFieldParameterEnum;
  fieldName: MethodFieldNameEnum;
  onlinePCF: string;
  moreInfoText: string;
  moreInfo: React.ReactNode;
}
export const parameterGroup: Record<ParameterListIds, ParameterGroup[]> = {
  Info_LoanOfferBasicSettings: [
    {
      id: MethodFieldParameterEnum.PromotionDescription,
      fieldName: MethodFieldNameEnum.PromotionDescription,
      onlinePCF: 'Description',
      moreInfoText:
        'The Promotion Description parameter provides a description of a specific Transaction Level ProcessingSM service (TLPSM service) promotion. This 30-character, free-form text will appear as the second detail line of a transaction if you set the Description Display parameter in this section to 1. Information entered in the Description parameter appears on TLP Promotional Purchase screens.',
      moreInfo: (
        <div>
          The Promotion Description parameter provides a description of a
          specific Transaction Level Processing<sup>SM</sup> service (TLP
          <sup>SM</sup> service) promotion. This 30-character, free-form text
          will appear as the second detail line of a transaction if you set the
          Description Display parameter in this section to 1. Information
          entered in the Description parameter appears on TLP Promotional
          Purchase screens.
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.PromotionAlternateDescription,
      fieldName: MethodFieldNameEnum.PromotionAlternateDescription,
      onlinePCF: 'Alternate Description',
      moreInfoText:
        'The Promotion Alternate Description parameter provides a description of a specific TLP promotion like the Description parameter in this section. This 30-character, free-form text will appear as the second detail line of a transaction if you set the Description Display parameter in this section to 2. You can pull information from this parameter into an Enterprise Presentation statement.',
      moreInfo:
        'The Promotion Alternate Description parameter provides a description of a specific TLP promotion like the Description parameter in this section. This 30-character, free-form text will appear as the second detail line of a transaction if you set the Description Display parameter in this section to 2. You can pull information from this parameter into an Enterprise Presentation statement.'
    },
    {
      id: MethodFieldParameterEnum.PromotionDescriptionDisplayOptions,
      fieldName: MethodFieldNameEnum.PromotionDescriptionDisplayOptions,
      onlinePCF: 'Description Display',
      moreInfoText:
        'The Promotion Description Display Options parameter determines whether a description of the promotion will appear as a second transaction detail line on the customer’s statement and, if so, which description will display.',
      moreInfo:
        'The Promotion Description Display Options parameter determines whether a description of the promotion will appear as a second transaction detail line on the customer’s statement and, if so, which description will display.'
    },
    {
      id: MethodFieldParameterEnum.PromotionStatementTextIDDTDD,
      fieldName: MethodFieldNameEnum.PromotionStatementTextIDDTDD,
      onlinePCF: 'Description from Text ID - DT/DD',
      moreInfoText:
        'The Promotion Statement Text ID (DT/DD) parameter identifies the client-defined promotion description that displays on customer statements in the customer’s preferred language.',
      moreInfo: (
        <div>
          <p>Text Area = DT, Text Type = DD.</p>
          <br />
          The Promotion Statement Text ID (DT/DD) parameter identifies the
          client-defined promotion description that displays on customer
          statements in the customer’s preferred language.
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.PromotionStatementTextIDDTDX,
      fieldName: MethodFieldNameEnum.PromotionStatementTextIDDTDX,
      onlinePCF: 'Description from Text ID - DT/DX',
      moreInfoText:
        'The Promotion Statement Text ID (DT/DX) parameter determines the promotional message that is displayed in the body of the customer statement after the introductory/delay period expires. This message is also used throughout the life of a promotion if an introductory period has not been established.',
      moreInfo: (
        <div>
          <p>Text Area = DT, Text Type = DX.</p>
          <br />
          The Promotion Statement Text ID (DT/DX) parameter determines the
          promotional message that is displayed in the body of the customer
          statement after the introductory/delay period expires. This message is
          also used throughout the life of a promotion if an introductory period
          has not been established.
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.PromotionStatementTextIDStandard,
      fieldName: MethodFieldNameEnum.PromotionStatementTextIDStandard,
      onlinePCF: 'Regular Text ID',
      moreInfoText:
        'The Promotion Statement Text ID (Standard) parameter determines the promotional message that is displayed in the body of the customer statement after the introductory/delay period expires. This message is also used throughout the life of a promotion if an introductory period has not been established.',
      moreInfo: (
        <div>
          <p>Text Area = MT, Text Type = PT.</p>
          <br />
          The Promotion Statement Text ID (Standard) parameter determines the
          promotional message that is displayed in the body of the customer
          statement after the introductory/delay period expires. This message is
          also used throughout the life of a promotion if an introductory period
          has not been established.
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.PromotionStatementTextControl,
      fieldName: MethodFieldNameEnum.PromotionStatementTextControl,
      onlinePCF: 'Regular Message Display Control',
      moreInfoText:
        'The Promotion Statement Text Control parameter controls the display of the regular promotional message on the statement and/or customer service screen.',
      moreInfo:
        'The Promotion Statement Text Control parameter controls the display of the regular promotional message on the statement and/or customer service screen.'
    },
    {
      id: MethodFieldParameterEnum.PromotionReturnApplicationOption,
      fieldName: MethodFieldNameEnum.PromotionReturnApplicationOption,
      onlinePCF: MethodFieldNameEnum.PromotionReturnApplicationOption,
      moreInfoText:
        'The Promotion Return Application Option parameter determines whether returns for promotional transactions processed on the T retail path and cardholder path for the Transaction Level ProcessingSM service are applied to the promotional balance.',
      moreInfo:
        'The Promotion Return Application Option parameter determines whether returns for promotional transactions processed on the T retail path and cardholder path for the Transaction Level ProcessingSM service are applied to the promotional balance.'
    },
    {
      id: MethodFieldParameterEnum.PromotionPayoffExceptionOption,
      fieldName: MethodFieldNameEnum.PromotionPayoffExceptionOption,
      onlinePCF: MethodFieldNameEnum.PromotionPayoffExceptionOption,
      moreInfoText:
        'The Promotion Payoff Exception Option parameter determines whether to flag promotional balances for exclusion from the required payoff balance.',
      moreInfo:
        'The Promotion Payoff Exception Option parameter determines whether to flag promotional balances for exclusion from the required payoff balance.'
    },
    {
      id: MethodFieldParameterEnum.PayoffExceptionMethod,
      fieldName: MethodFieldNameEnum.PayoffExceptionMethod,
      onlinePCF: 'Payoff Exceptions (CP/IC/PE)',
      moreInfoText:
        'The Payoff Exception Method (CP IC PE) parameter contain the method name for the Payoff Exceptions section (CP IC PE) of the Product Control File that determines promotional payoff exception processing.',
      moreInfo:
        'The Payoff Exception Method (CP IC PE) parameter contain the method name for the Payoff Exceptions section (CP IC PE) of the Product Control File that determines promotional payoff exception processing.'
    },
    {
      id: MethodFieldParameterEnum.CashAdvanceItemFeesMethod,
      fieldName: MethodFieldNameEnum.CashAdvanceItemFeesMethod,
      onlinePCF: 'Cash Adv Item Fees (CP/IO/CI)',
      moreInfoText:
        'The Cash Advance Item Fees Method (CP IO CI) parameter contain the method name for the Cash Advance Item Charges (CP/IO/CI) section of the Product Control File that determines the cash advance item charge used for transactions that qualify for this promotion.',
      moreInfo:
        'The Cash Advance Item Fees Method (CP IO CI) parameter contain the method name for the Cash Advance Item Charges (CP/IO/CI) section of the Product Control File that determines the cash advance item charge used for transactions that qualify for this promotion.'
    },
    {
      id: MethodFieldParameterEnum.MerchantItemFeesMethod,
      fieldName: MethodFieldNameEnum.MerchantItemFeesMethod,
      onlinePCF: 'Merchant Item Fees (CP/IO/MI)',
      moreInfoText:
        'The Merchant Item Fees Method (CP IO MI) parameter contain the method name for the Merchandise Item Charges (CP/IO/MI) section of the Product Control File that determines the merchandise item charge used for transactions that qualify for this promotion.',
      moreInfo:
        'The Merchant Item Fees Method (CP IO MI) parameter contain the method name for the Merchandise Item Charges (CP/IO/MI) section of the Product Control File that determines the merchandise item charge used for transactions that qualify for this promotion.'
    },
    {
      id: MethodFieldParameterEnum.CreditApplicationGroup,
      fieldName: MethodFieldNameEnum.CreditApplicationGroup,
      onlinePCF: MethodFieldNameEnum.CreditApplicationGroup,
      moreInfoText:
        'The Credit Application Group parameter identifies a payment group, which determines how payments are applied to promotional balances that have not been assigned a unique minimum payment. Establish your credit application groups using the payment application sequence parameters in the Credit Application section (CP PO CA) of the Product Control File.',
      moreInfo:
        'The Credit Application Group parameter identifies a payment group, which determines how payments are applied to promotional balances that have not been assigned a unique minimum payment. Establish your credit application groups using the payment application sequence parameters in the Credit Application section (CP PO CA) of the Product Control File.'
    },
    {
      id: MethodFieldParameterEnum.PromotionGroupIdentifier,
      fieldName: MethodFieldNameEnum.PromotionGroupIdentifier,
      onlinePCF: 'Group Identifier',
      moreInfoText:
        'The Promotion Group Identifier parameter identifies the client-defined name assigned to the plan balances you wish to group together for processing using Rules Management processing.',
      moreInfo:
        'The Promotion Group Identifier parameter identifies the client-defined name assigned to the plan balances you wish to group together for processing using Rules Management processing.'
    },
    {
      id: MethodFieldParameterEnum.LoanBalanceClassification,
      fieldName: MethodFieldNameEnum.LoanBalanceClassification,
      onlinePCF: 'Loan Balance',
      moreInfoText:
        'The Loan Balance Classification parameter determines whether or not these promotional balances will display the LN1 (through LN6) screens of the Customer Promotional Purchase Display Screen transaction.',
      moreInfo:
        'The Loan Balance Classification parameter determines whether or not these promotional balances will display the LN1 (through LN6) screens of the Customer Promotional Purchase Display Screen transaction.'
    }
  ],

  Info_PaymentAndPricingSettings: [
    {
      id: MethodFieldParameterEnum.StandardMinimumPaymentCalculationCode,
      fieldName: MethodFieldNameEnum.StandardMinimumPaymentCalculationCode,
      onlinePCF: 'Standard Minimum Payments Calculation Method',
      moreInfoText:
        'The Standard Minimum Payment Calculation Code parameter determines how the System calculates a minimum payment for a promotional balance.',
      moreInfo:
        'The Standard Minimum Payment Calculation Code parameter determines how the System calculates a minimum payment for a promotional balance.'
    },
    {
      id: MethodFieldParameterEnum.RulesMinimumPaymentDueMethod,
      fieldName: MethodFieldNameEnum.RulesMinimumPaymentDueMethod,
      onlinePCF: 'Minimum Payment (PL/RT/MP)',
      moreInfoText:
        'The Rules Minimum Payment Due Method (PL RT MP) parameter identify the Promotional Level minimum payment due method to use for a promotion.',
      moreInfo:
        'The Rules Minimum Payment Due Method (PL RT MP) parameter identify the Promotional Level minimum payment due method to use for a promotion.'
    },
    {
      id: MethodFieldParameterEnum.StandardMinimumPaymentsRate,
      fieldName: MethodFieldNameEnum.StandardMinimumPaymentsRate,
      onlinePCF: MethodFieldNameEnum.StandardMinimumPaymentsRate,
      moreInfoText:
        'The Standard Minimum Payments Rate parameter determines the rate used to calculate the minimum payment due as a percentage of the original transaction, principal balance, or principal plus interest, based on your setting in the Standard Minimum Payments Calculation Method parameter in this section.',
      moreInfo:
        'The Standard Minimum Payments Rate parameter determines the rate used to calculate the minimum payment due as a percentage of the original transaction, principal balance, or principal plus interest, based on your setting in the Standard Minimum Payments Calculation Method parameter in this section.'
    },
    {
      id: MethodFieldParameterEnum.StandardMinimumPaymentRoundingOptions,
      fieldName: MethodFieldNameEnum.StandardMinimumPaymentRoundingOptions,
      onlinePCF: 'Standard Minimum Payments MPD Rounding',
      moreInfoText:
        'The Standard Minimum Payment Rounding Options parameter controls the rounding of minimum payment calculations.',
      moreInfo:
        'The Standard Minimum Payment Rounding Options parameter controls the rounding of minimum payment calculations.'
    },
    {
      id: MethodFieldParameterEnum.AnnualInterestRate,
      fieldName: MethodFieldNameEnum.AnnualInterestRate,
      onlinePCF: 'Regular Annual Rate',
      moreInfoText:
        'The Annual Interest Rate parameter determines the interest rate for transactions after the introductory period has expired or during a cash option period.',
      moreInfo:
        'The Annual Interest Rate parameter determines the interest rate for transactions after the introductory period has expired or during a cash option period.'
    },
    {
      id: MethodFieldParameterEnum.PayoutPeriod,
      fieldName: MethodFieldNameEnum.PayoutPeriod,
      onlinePCF: MethodFieldNameEnum.PayoutPeriod,
      moreInfoText:
        'The Payout Period parameter determines the number of months over which to extend customer payments.',
      moreInfo:
        'The Payout Period parameter determines the number of months over which to extend customer payments.'
    }
  ],
  Info_MerchantDiscount: [
    {
      id: MethodFieldParameterEnum.MerchantDiscount,
      fieldName: MethodFieldNameEnum.MerchantDiscount,
      onlinePCF: MethodFieldNameEnum.MerchantDiscount,
      moreInfoText:
        'The Merchant Discount parameter identifies the discount percentage charged to the merchant for transactions that qualify for the promotion if the promotion is processed through the Retail path.',
      moreInfo:
        'The Merchant Discount parameter identifies the discount percentage charged to the merchant for transactions that qualify for the promotion if the promotion is processed through the Retail path.'
    },
    {
      id: MethodFieldParameterEnum.MerchantDiscountCalculationCode,
      fieldName: MethodFieldNameEnum.MerchantDiscountCalculationCode,
      onlinePCF: 'Promo Discount Calculation Code',
      moreInfoText:
        'The Merchant Discount Calculation Code parameter determines whether to calculate merchant discount based solely on the promotional discount rate or on the promotional discount rate plus the merchant qualifying discount rate.',
      moreInfo:
        'The Merchant Discount Calculation Code parameter determines whether to calculate merchant discount based solely on the promotional discount rate or on the promotional discount rate plus the merchant qualifying discount rate.'
    }
  ],
  Adv_MiscellaneousControls: [
    {
      id: MethodFieldParameterEnum.ReturnToRevolvingBaseInterest,
      fieldName: MethodFieldNameEnum.ReturnToRevolvingBaseInterest,
      onlinePCF: 'Return to Revolving Base Interest',
      moreInfoText:
        'The Return to Revolving Base Interest parameter determines the interest rate the System uses for a promotional balance that has returned to revolving.',
      moreInfo:
        'The Return to Revolving Base Interest parameter determines the interest rate the System uses for a promotional balance that has returned to revolving.'
    },
    {
      id: MethodFieldParameterEnum.ApplyPenaltyPricingOnLoanOffer,
      fieldName: MethodFieldNameEnum.ApplyPenaltyPricingOnLoanOffer,
      onlinePCF: 'Penalty On Promotion Override',
      moreInfoText:
        'The Apply Penalty Pricing on Loan Offer parameter controls whether to apply penalty pricing to delinquent accounts with a specific promotion.',
      moreInfo:
        'The Apply Penalty Pricing on Loan Offer parameter controls whether to apply penalty pricing to delinquent accounts with a specific promotion.'
    },
    {
      id: MethodFieldParameterEnum.DefaultInterestRateMethodOverride,
      fieldName: MethodFieldNameEnum.DefaultInterestRateMethodOverride,
      onlinePCF: 'Interest Defaults (CP/IC/ID)',
      moreInfoText:
        'The Default Interest Rate Method Override parameter identifies the method names for the Cardholder Pricing interest default method to use following the end of the promotion’s introductory period.',
      moreInfo:
        'The Default Interest Rate Method Override parameter identifies the method names for the Cardholder Pricing interest default method to use following the end of the promotion’s introductory period.'
    },
    {
      id: MethodFieldParameterEnum.InterestCalculationMethodOverride,
      fieldName: MethodFieldNameEnum.InterestCalculationMethodOverride,
      onlinePCF: 'Interest Methods (CP/IC/IM)',
      moreInfoText:
        'The Interest Calculation Method Override parameter contains the method name for the Interest Methods section (CP IC IM) of the Product Control File that determines interest processing.',
      moreInfo:
        'The Interest Calculation Method Override parameter contains the method name for the Interest Methods section (CP IC IM) of the Product Control File that determines interest processing.'
    },
    {
      id: MethodFieldParameterEnum.PositiveAmortizationUsageCode,
      fieldName: MethodFieldNameEnum.PositiveAmortizationUsageCode,
      onlinePCF: MethodFieldNameEnum.PositiveAmortizationUsageCode,
      moreInfoText:
        'The Positive Amortization Usage Code parameter determines whether the System excludes a promotional balance from a positive amortization calculation. This parameter also can determine whether the System excludes the promotional balance from the positive amortization calculation only during any of the following promotional periods.',
      moreInfo: (
        <div>
          <p>
            The Positive Amortization Usage Code parameter determines whether
            the System excludes a promotional balance from a positive
            amortization calculation. This parameter also can determine whether
            the System excludes the promotional balance from the positive
            amortization calculation only during any of the following
            promotional periods.
          </p>
          <ul>
            <li>Introductory MPD</li>
            <li>Introductory interest</li>
            <li>Introductory cash option</li>
            <li>Any or all of the above</li>
          </ul>
        </div>
      )
    }
  ],
  Adv_IntroductoryAprSettings: [
    {
      id: MethodFieldParameterEnum.IntroductoryAnnualRate,
      fieldName: MethodFieldNameEnum.IntroductoryAnnualRate,
      onlinePCF: 'Intro Annual Rate',
      moreInfoText:
        'The Introductory Annual Interest Rate parameter determines the interest rate for transactions during a time period you specify in the Promotional Interest Delay Number of Days, Promotional Interest Delay Number of Months, or Promotional Interest Delay Number of Cycles parameter. To waive interest for the established delay period, set this parameter to zero. Introductory annual rates greater than zero will be used through the cycle following the delay end date.',
      moreInfo:
        'The Introductory Annual Interest Rate parameter determines the interest rate for transactions during a time period you specify in the Promotional Interest Delay Number of Days, Promotional Interest Delay Number of Months, or Promotional Interest Delay Number of Cycles parameter. To waive interest for the established delay period, set this parameter to zero. Introductory annual rates greater than zero will be used through the cycle following the delay end date.'
    },
    {
      id: MethodFieldParameterEnum.IntroductoryAnnualRateFixedEndDate,
      fieldName: MethodFieldNameEnum.IntroductoryAnnualRateFixedEndDate,
      onlinePCF: 'Promotional Interest Delay Date',
      moreInfoText:
        'The Introductory Annual Rate - Fixed End Date parameter sets the date (MM/DD/YY) the introductory interest rate expires.',
      moreInfo:
        'The Introductory Annual Rate - Fixed End Date parameter sets the date (MM/DD/YY) the introductory interest rate expires.'
    },
    {
      id: MethodFieldParameterEnum.IntroductoryRateNumberOfDays,
      fieldName: MethodFieldNameEnum.IntroductoryRateNumberOfDays,
      onlinePCF: 'Promotional Interest Delay Number of Days',
      moreInfoText:
        'The Introductory Rate – Number of Days parameter determines the number of days the introductory interest rate for the promotion will be in effect. Your setting in the Delay Interest Start parameter determines whether the System begins counting the number of days from the post date of the first promotional transaction or from the account open date.',
      moreInfo:
        'The Introductory Rate – Number of Days parameter determines the number of days the introductory interest rate for the promotion will be in effect. Your setting in the Delay Interest Start parameter determines whether the System begins counting the number of days from the post date of the first promotional transaction or from the account open date.'
    },
    {
      id: MethodFieldParameterEnum.IntroductoryRateNumberOfCycles,
      fieldName: MethodFieldNameEnum.IntroductoryRateNumberOfCycles,
      onlinePCF: 'Promotional Interest Delay Number of Cycles',
      moreInfoText:
        'The Introductory Rate – Number of Cycles parameter determines the number of cycles the introductory interest rate for the promotion will be in effect. Your setting in the Delay Interest Start parameter determines whether the System begins counting the number of cycles from the post date of the first promotional transaction or from the account open date.',
      moreInfo:
        'The Introductory Rate – Number of Cycles parameter determines the number of cycles the introductory interest rate for the promotion will be in effect. Your setting in the Delay Interest Start parameter determines whether the System begins counting the number of cycles from the post date of the first promotional transaction or from the account open date.'
    },
    {
      id: MethodFieldParameterEnum.IntroductoryPeriodStatementTextID,
      fieldName: MethodFieldNameEnum.IntroductoryPeriodStatementTextID,
      onlinePCF: 'Delay/Intro Text ID',
      moreInfoText:
        'Determines when the consecutive late fee count is set back to zero. There is a dependency on what the issuer entered into the LATE CHARGE CYCLES OF CONSECUTIVE DELINQUENCY.',
      moreInfo: (
        <div>
          <p>Text Area = MT, Text Type = PT.</p>
          <br />
          <p>
            The Introductory Period Statement Text ID parameter determines which
            of your promotion messages appears in the body of a cardholder
            statement during the delay/introductory interest period. Your
            settings in the following parameters determine whether a promotion
            is in a delay/introductory interest period.
          </p>
          <ul>
            <li>Promotional Interest Delay Date</li>
            <li>Promotional Interest Delay Number of Days</li>
            <li>Promotional Interest Delay Number of Cycles</li>
            <li>Introductory Minimum Payments Delay Date</li>
            <li>Introductory Minimum Payments Delay Number of Days</li>
            <li>Introductory Minimum Payments Delay Number of Cycles</li>
            <li>Introductory Minimum Payments Delay Number of Months</li>
            <li>Promotional Cash Option End Date</li>
            <li>Promotional Cash Option Number of Days</li>
            <li>Promotional Cash Option Number of Cycles</li>
            <li>Promotional Cash Option Number of Months</li>
          </ul>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.IntroductoryMessageDisplayControl,
      fieldName: MethodFieldNameEnum.IntroductoryMessageDisplayControl,
      onlinePCF: MethodFieldNameEnum.IntroductoryMessageDisplayControl,
      moreInfoText:
        'The Introductory Message Display Control parameter controls the display of an introductory promotional message on the cardholder’s paper statement, the online statement in the Customer Inquiry System (CIS), or both.',
      moreInfo:
        'The Introductory Message Display Control parameter controls the display of an introductory promotional message on the cardholder’s paper statement, the online statement in the Customer Inquiry System (CIS), or both.'
    }
  ],
  Adv_PromotionalCashSettings: [
    {
      id: MethodFieldParameterEnum.SameAsCashLoanOfferNumberOfCycles,
      fieldName: MethodFieldNameEnum.SameAsCashLoanOfferNumberOfCycles,
      onlinePCF: 'Promotional Cash Option Number of Cycles',
      moreInfoText:
        'The Same-as-Cash Loan Offer - Number of Cycles parameter determines the number of cycles from the last statement date that a cardholder has to pay off a promotional balance to avoid interest charges. The valid values for this parameter range from 00 to 99. This parameter facilitates the consolidation of subaccount promotional balances in a commercial card environment.',
      moreInfo:
        'The Same-as-Cash Loan Offer - Number of Cycles parameter determines the number of cycles from the last statement date that a cardholder has to pay off a promotional balance to avoid interest charges. The valid values for this parameter range from 00 to 99. This parameter facilitates the consolidation of subaccount promotional balances in a commercial card environment.'
    },
    {
      id: MethodFieldParameterEnum.AccrueInterestOnUnbilledInterest,
      fieldName: MethodFieldNameEnum.AccrueInterestOnUnbilledInterest,
      onlinePCF: 'Interest on Unbilled Interest',
      moreInfoText:
        'The Accrue Interest on Unbilled Interest parameter determines whether promotional balances that have unbilled interest will accrue interest.',
      moreInfo:
        'The Accrue Interest on Unbilled Interest parameter determines whether promotional balances that have unbilled interest will accrue interest.'
    }
  ],
  Adv_PaymentDelaySettings: [
    {
      id: MethodFieldParameterEnum.DelayPaymentForStatementCycles,
      fieldName: MethodFieldNameEnum.DelayPaymentForStatementCycles,
      onlinePCF: 'Introductory Minimum Payments Delay Number of Cycles',
      moreInfoText:
        'The Delay Payment for Statement Cycles parameter determines the number of cycles that the introductory minimum payment period is in effect. Your setting in the Delay Payment Start parameter determines whether the System begins counting the number of cycles from the last statement date prior to the post date of the first promotional transaction.	',
      moreInfo:
        'The Delay Payment for Statement Cycles parameter determines the number of cycles that the introductory minimum payment period is in effect. Your setting in the Delay Payment Start parameter determines whether the System begins counting the number of cycles from the last statement date prior to the post date of the first promotional transaction.	'
    },
    {
      id: MethodFieldParameterEnum.DelayPaymentForMonths,
      fieldName: MethodFieldNameEnum.DelayPaymentForMonths,
      onlinePCF: 'Introductory Minimum Payments Delay Number of Months',
      moreInfoText:
        'The Delay Payment for Months parameter determines the number of months that the introductory minimum payment period is in effect. Your setting in the Delay Payment Start parameter determines whether the System begins counting the number of months from the post date of the first promotional transaction or from the account open date.',
      moreInfo:
        'The Delay Payment for Months parameter determines the number of months that the introductory minimum payment period is in effect. Your setting in the Delay Payment Start parameter determines whether the System begins counting the number of months from the post date of the first promotional transaction or from the account open date.'
    }
  ]
};
