import { ColumnType } from 'app/_libraries/_dls';
import {
  valueTranslation,
  viewMoreInfo
} from 'pages/_commons/Utils/formatGridField';

const newMethodColumns = (t: any): ColumnType[] => [
  {
    id: 'section',
    Header: t('txt_section'),
    width: 356,
    accessor: valueTranslation(['section'], t)
  },
  {
    id: 'parameterGroup',
    Header: t('txt_parameter_group'),
    accessor: valueTranslation(['parameterGroup'], t)
  },
  {
    id: 'moreInfo',
    Header: t('txt_more_info'),
    className: 'text-center',
    accessor: viewMoreInfo(['moreInfo'], t),
    width: 105
  }
];

export default newMethodColumns;
