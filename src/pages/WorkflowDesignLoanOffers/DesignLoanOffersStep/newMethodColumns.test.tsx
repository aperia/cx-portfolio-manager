import newMethodColumns from './newMethodColumns';

describe('pages > WorkflowDesignLoanOffers > DesignLoanOffersStep > newMethodColumns', () => {
  it('newMethodColumns', () => {
    const t = value => value;

    const result = newMethodColumns(t);
    expect(result[0].id).toEqual('section');
    expect(result[0].Header).toEqual('txt_section');

    expect(result[1].id).toEqual('parameterGroup');
    expect(result[1].Header).toEqual('txt_parameter_group');
  });
});
