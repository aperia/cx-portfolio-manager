import EnhanceDatePicker from 'app/components/EnhanceDatePicker';
import EnhanceDropdownList from 'app/components/EnhanceDropdownList';
import {
  MethodFieldNameEnum,
  MethodFieldParameterEnum
} from 'app/constants/enums';
import { isValidDate, matchSearchValue } from 'app/helpers';
import {
  ColumnType,
  ComboBox,
  Grid,
  NumericTextBox,
  TextBox,
  useTranslation
} from 'app/_libraries/_dls';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React, { useMemo } from 'react';
import { parameterGroup, ParameterList } from './newMethodParameterList';

interface IProps {
  searchValue: string;
  method: WorkflowSetupMethodObjectParams;
  original: ParameterList;
  minDate?: Date;
  maxDate?: Date;
  metadata: {
    promotionDescription: RefData[];
    promotionAlternateDescription: RefData[];
    promotionDescriptionDisplayOptions: RefData[];
    promotionStatementTextIDDTDD: RefData[];
    promotionStatementTextIDDTDX: RefData[];
    promotionStatementTextIDStandard: RefData[];
    promotionStatementTextControl: RefData[];
    promotionReturnApplicationOption: RefData[];
    promotionPayoffExceptionOption: RefData[];
    loanBalanceClassification: RefData[];
    payoffExceptionMethod: RefData[];
    cashAdvanceItemFeesMethod: RefData[];
    merchantItemFeesMethod: RefData[];
    creditApplicationGroup: RefData[];
    promotionGroupIdentifier: RefData[];
    standardMinimumPaymentCalculationCode: RefData[];
    rulesMinimumPaymentDueMethod: RefData[];
    standardMinimumPaymentsRate: RefData[];
    standardMinimumPaymentRoundingOptions: RefData[];
    annualInterestRate: RefData[];
    payoutPeriod: RefData[];
    merchantDiscount: RefData[];
    merchantDiscountCalculationCode: RefData[];
    returnToRevolvingBaseInterest: RefData[];
    applyPenaltyPricingOnLoanOffer: RefData[];
    defaultInterestRateMethodOverride: RefData[];
    interestCalculationMethodOverride: RefData[];
    introductoryMessageDisplayControl: RefData[];
    positiveAmortizationUsageCode: RefData[];
    introductoryAnnualRate: RefData[];
    introductoryAnnualRateFixedEndDate: RefData[];
    introductoryRateNumberOfDays: RefData[];
    introductoryRateNumberOfCycles: RefData[];
    introductoryPeriodStatementTextID: RefData[];
    sameAsCashLoanOfferNumberOfCycles: RefData[];
    accrueInterestOnUnbilledInterest: RefData[];
    delayPaymentForStatementCycles: RefData[];
    delayPaymentForMonths: RefData[];
  };
  onChange: (
    field: keyof WorkflowSetupMethodObjectParams,
    isRefData?: boolean
  ) => (e: any) => void;
}
const SubGridNewMethod: React.FC<IProps> = ({
  searchValue,
  method,
  original,
  metadata,
  onChange,
  minDate,
  maxDate
}) => {
  const { t } = useTranslation();

  const { width } = useSelectWindowDimension();

  const {
    promotionDescriptionDisplayOptions,
    promotionStatementTextIDDTDD,
    promotionStatementTextIDDTDX,
    promotionStatementTextIDStandard,
    promotionStatementTextControl,
    promotionReturnApplicationOption,
    promotionPayoffExceptionOption,
    payoffExceptionMethod,
    cashAdvanceItemFeesMethod,
    merchantItemFeesMethod,
    creditApplicationGroup,
    promotionGroupIdentifier,
    loanBalanceClassification,
    standardMinimumPaymentCalculationCode,
    rulesMinimumPaymentDueMethod,
    standardMinimumPaymentRoundingOptions,
    merchantDiscountCalculationCode,
    returnToRevolvingBaseInterest,
    applyPenaltyPricingOnLoanOffer,
    defaultInterestRateMethodOverride,
    interestCalculationMethodOverride,
    introductoryMessageDisplayControl,
    positiveAmortizationUsageCode,
    introductoryPeriodStatementTextID,
    accrueInterestOnUnbilledInterest
  } = metadata;

  const data = useMemo(
    () =>
      parameterGroup[original.id].filter(p =>
        matchSearchValue(
          [p.fieldName, p.moreInfoText, p.onlinePCF],
          searchValue
        )
      ),
    [searchValue, original.id]
  );

  const valueAccessor = (data: Record<string, any>) => {
    const paramId = data.id as MethodFieldParameterEnum;

    switch (paramId) {
      //standard parameters - Loan offer basic settings
      case MethodFieldParameterEnum.PromotionDescription: {
        return (
          <TextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__promotionDescription"
            size="sm"
            value={method?.[MethodFieldParameterEnum.PromotionDescription]}
            onChange={onChange(MethodFieldParameterEnum.PromotionDescription)}
            maxLength={30}
          />
        );
      }
      case MethodFieldParameterEnum.PromotionAlternateDescription: {
        return (
          <TextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__promotionAlternateDescription"
            size="sm"
            value={
              method?.[MethodFieldParameterEnum.PromotionAlternateDescription]
            }
            onChange={onChange(
              MethodFieldParameterEnum.PromotionAlternateDescription
            )}
            maxLength={30}
          />
        );
      }
      case MethodFieldParameterEnum.PromotionDescriptionDisplayOptions: {
        const value = promotionDescriptionDisplayOptions.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum.PromotionDescriptionDisplayOptions
            ]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__promotionDescriptionDisplayOptions"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.PromotionDescriptionDisplayOptions,
              true
            )}
            options={promotionDescriptionDisplayOptions}
          />
        );
      }

      case MethodFieldParameterEnum.PromotionStatementTextIDDTDD: {
        const value = promotionStatementTextIDDTDD.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.PromotionStatementTextIDDTDD]
        );

        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            size="small"
            textField="text"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.PromotionStatementTextIDDTDD,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {promotionStatementTextIDDTDD.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }

      case MethodFieldParameterEnum.PromotionStatementTextIDDTDX: {
        const value = promotionStatementTextIDDTDX.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.PromotionStatementTextIDDTDX]
        );

        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            size="small"
            textField="text"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.PromotionStatementTextIDDTDX,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {promotionStatementTextIDDTDX.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }

      case MethodFieldParameterEnum.PromotionStatementTextIDStandard: {
        const value = promotionStatementTextIDStandard.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.PromotionStatementTextIDStandard]
        );

        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            size="small"
            textField="text"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.PromotionStatementTextIDStandard,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {promotionStatementTextIDStandard.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }

      case MethodFieldParameterEnum.PromotionStatementTextControl: {
        const value = promotionStatementTextControl.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.PromotionStatementTextControl]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__promotionStatementTextControl"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.PromotionStatementTextControl,
              true
            )}
            options={promotionStatementTextControl}
          />
        );
      }

      case MethodFieldParameterEnum.PromotionReturnApplicationOption: {
        const value = promotionReturnApplicationOption.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.PromotionReturnApplicationOption]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__promotionReturnApplicationOption"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.PromotionReturnApplicationOption,
              true
            )}
            options={promotionReturnApplicationOption}
          />
        );
      }

      case MethodFieldParameterEnum.PromotionPayoffExceptionOption: {
        const value = promotionPayoffExceptionOption.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.PromotionPayoffExceptionOption]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__promotionPayoffExceptionOption"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.PromotionPayoffExceptionOption,
              true
            )}
            options={promotionPayoffExceptionOption}
          />
        );
      }

      case MethodFieldParameterEnum.PayoffExceptionMethod: {
        const value = payoffExceptionMethod.find(
          o =>
            o.code === method?.[MethodFieldParameterEnum.PayoffExceptionMethod]
        );

        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            textField="text"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.PayoffExceptionMethod,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {payoffExceptionMethod.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }

      case MethodFieldParameterEnum.CashAdvanceItemFeesMethod: {
        const value = cashAdvanceItemFeesMethod.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.CashAdvanceItemFeesMethod]
        );

        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            size="small"
            textField="text"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.CashAdvanceItemFeesMethod,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {cashAdvanceItemFeesMethod.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }

      case MethodFieldParameterEnum.MerchantItemFeesMethod: {
        const value = merchantItemFeesMethod.find(
          o =>
            o.code === method?.[MethodFieldParameterEnum.MerchantItemFeesMethod]
        );

        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            size="small"
            textField="text"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.MerchantItemFeesMethod,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {merchantItemFeesMethod.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }

      case MethodFieldParameterEnum.CreditApplicationGroup: {
        const value = creditApplicationGroup.find(
          o =>
            o.code === method?.[MethodFieldParameterEnum.CreditApplicationGroup]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__creditApplicationGroup"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.CreditApplicationGroup,
              true
            )}
            options={creditApplicationGroup}
          />
        );
      }

      case MethodFieldParameterEnum.PromotionGroupIdentifier: {
        const value = promotionGroupIdentifier.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.PromotionGroupIdentifier]
        );

        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            size="small"
            textField="text"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.PromotionGroupIdentifier,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {promotionGroupIdentifier.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }

      case MethodFieldParameterEnum.LoanBalanceClassification: {
        const value = loanBalanceClassification.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.LoanBalanceClassification]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__loanBalanceClassification"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.LoanBalanceClassification,
              true
            )}
            options={loanBalanceClassification}
          />
        );
      }
      //standard parameters - Payment and Pricing settings
      case MethodFieldParameterEnum.StandardMinimumPaymentCalculationCode: {
        const value = standardMinimumPaymentCalculationCode.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum.StandardMinimumPaymentCalculationCode
            ]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__standardMinimumPaymentCalculationCode"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.StandardMinimumPaymentCalculationCode,
              true
            )}
            options={standardMinimumPaymentCalculationCode}
          />
        );
      }

      case MethodFieldParameterEnum.RulesMinimumPaymentDueMethod: {
        const value = rulesMinimumPaymentDueMethod.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.RulesMinimumPaymentDueMethod]
        );

        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            size="small"
            textField="text"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.RulesMinimumPaymentDueMethod,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {rulesMinimumPaymentDueMethod.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }

      case MethodFieldParameterEnum.StandardMinimumPaymentsRate: {
        return (
          <NumericTextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__standardMinimumPaymentsRate"
            size="sm"
            format="p4"
            showStep={false}
            maxLength={5}
            autoFocus={false}
            value={
              method?.[MethodFieldParameterEnum.StandardMinimumPaymentsRate] ||
              ''
            }
            onChange={onChange(
              MethodFieldParameterEnum.StandardMinimumPaymentsRate
            )}
          />
        );
      }

      case MethodFieldParameterEnum.StandardMinimumPaymentRoundingOptions: {
        const value = standardMinimumPaymentRoundingOptions.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum.StandardMinimumPaymentRoundingOptions
            ]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__standardMinimumPaymentRoundingOptions"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.StandardMinimumPaymentRoundingOptions,
              true
            )}
            options={standardMinimumPaymentRoundingOptions}
          />
        );
      }

      case MethodFieldParameterEnum.AnnualInterestRate: {
        return (
          <NumericTextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__annualInterestRate"
            size="sm"
            format="p3"
            showStep={false}
            maxLength={5}
            autoFocus={false}
            value={method?.[MethodFieldParameterEnum.AnnualInterestRate] || ''}
            onChange={onChange(MethodFieldParameterEnum.AnnualInterestRate)}
          />
        );
      }

      case MethodFieldParameterEnum.PayoutPeriod: {
        return (
          <NumericTextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__payoutPeriod"
            size="sm"
            format="i0"
            maxLength={3}
            value={method?.[MethodFieldParameterEnum.PayoutPeriod] || ''}
            onChange={onChange(MethodFieldParameterEnum.PayoutPeriod)}
          />
        );
      }

      case MethodFieldParameterEnum.MerchantDiscount: {
        return (
          <NumericTextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__merchantDiscount"
            size="sm"
            format="p3"
            showStep={false}
            maxLength={5}
            autoFocus={false}
            value={method?.[MethodFieldParameterEnum.MerchantDiscount] || ''}
            onChange={onChange(MethodFieldParameterEnum.MerchantDiscount)}
          />
        );
      }

      case MethodFieldParameterEnum.MerchantDiscountCalculationCode: {
        const value = merchantDiscountCalculationCode.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.MerchantDiscountCalculationCode]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__merchantDiscountCalculationCode"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.MerchantDiscountCalculationCode,
              true
            )}
            options={merchantDiscountCalculationCode}
          />
        );
      }

      //advance parameters introductory APR Settings

      case MethodFieldParameterEnum.ReturnToRevolvingBaseInterest: {
        const value = returnToRevolvingBaseInterest.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.ReturnToRevolvingBaseInterest]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__returnToRevolvingBaseInterest"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.ReturnToRevolvingBaseInterest,
              true
            )}
            options={returnToRevolvingBaseInterest}
          />
        );
      }

      case MethodFieldParameterEnum.PositiveAmortizationUsageCode: {
        const value = positiveAmortizationUsageCode.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.PositiveAmortizationUsageCode]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__positiveAmortizationUsageCode"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.PositiveAmortizationUsageCode,
              true
            )}
            options={positiveAmortizationUsageCode}
          />
        );
      }

      case MethodFieldParameterEnum.DefaultInterestRateMethodOverride: {
        const value = defaultInterestRateMethodOverride.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.DefaultInterestRateMethodOverride]
        );

        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            size="small"
            textField="text"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DefaultInterestRateMethodOverride,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {defaultInterestRateMethodOverride.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }

      case MethodFieldParameterEnum.InterestCalculationMethodOverride: {
        const value = interestCalculationMethodOverride.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.InterestCalculationMethodOverride]
        );

        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            size="small"
            textField="text"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.InterestCalculationMethodOverride,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {interestCalculationMethodOverride.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }

      case MethodFieldParameterEnum.IntroductoryMessageDisplayControl: {
        const value = introductoryMessageDisplayControl.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.IntroductoryMessageDisplayControl]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__introductoryMessageDisplayControl"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.IntroductoryMessageDisplayControl,
              true
            )}
            options={introductoryMessageDisplayControl}
          />
        );
      }

      case MethodFieldParameterEnum.ApplyPenaltyPricingOnLoanOffer: {
        const value = applyPenaltyPricingOnLoanOffer.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.ApplyPenaltyPricingOnLoanOffer]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__applyPenaltyPricingOnLoanOffer"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.ApplyPenaltyPricingOnLoanOffer,
              true
            )}
            options={applyPenaltyPricingOnLoanOffer}
          />
        );
      }

      case MethodFieldParameterEnum.IntroductoryAnnualRate: {
        return (
          <NumericTextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__introductoryAnnualRate"
            size="sm"
            format="p3"
            showStep={false}
            maxLength={5}
            autoFocus={false}
            value={
              method?.[MethodFieldParameterEnum.IntroductoryAnnualRate] || ''
            }
            onChange={onChange(MethodFieldParameterEnum.IntroductoryAnnualRate)}
          />
        );
      }

      case MethodFieldParameterEnum.IntroductoryAnnualRateFixedEndDate: {
        const _val =
          method?.[MethodFieldParameterEnum.IntroductoryAnnualRateFixedEndDate];
        const val = isValidDate(_val) ? new Date(_val!) : undefined;
        return (
          <div>
            <EnhanceDatePicker
              size="small"
              id="addNewMethod__introductoryAnnualRateFixedEndDateFormat"
              placeholder={t('txt_date_format')}
              value={val}
              minDate={minDate}
              maxDate={maxDate}
              name={MethodFieldNameEnum.IntroductoryAnnualRateFixedEndDate}
              onChange={onChange(
                MethodFieldParameterEnum.IntroductoryAnnualRateFixedEndDate
              )}
            />
          </div>
        );
      }

      case MethodFieldParameterEnum.IntroductoryRateNumberOfDays: {
        return (
          <NumericTextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__introductoryRateNumberOfDays"
            size="sm"
            format="i0"
            maxLength={5}
            autoFocus={false}
            value={
              method?.[MethodFieldParameterEnum.IntroductoryRateNumberOfDays] ||
              ''
            }
            onChange={onChange(
              MethodFieldParameterEnum.IntroductoryRateNumberOfDays
            )}
          />
        );
      }

      case MethodFieldParameterEnum.IntroductoryRateNumberOfCycles: {
        return (
          <NumericTextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__introductoryRateNumberOfCycles"
            size="sm"
            format="i0"
            maxLength={2}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum.IntroductoryRateNumberOfCycles
              ] || ''
            }
            onChange={onChange(
              MethodFieldParameterEnum.IntroductoryRateNumberOfCycles
            )}
          />
        );
      }

      case MethodFieldParameterEnum.IntroductoryPeriodStatementTextID: {
        const value = introductoryPeriodStatementTextID.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.IntroductoryPeriodStatementTextID]
        );

        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            size="small"
            textField="text"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.IntroductoryPeriodStatementTextID,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {introductoryPeriodStatementTextID.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }

      case MethodFieldParameterEnum.SameAsCashLoanOfferNumberOfCycles: {
        return (
          <NumericTextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__sameAsCashLoanOfferNumberOfCycles"
            size="sm"
            format="i0"
            maxLength={2}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum.SameAsCashLoanOfferNumberOfCycles
              ] || ''
            }
            onChange={onChange(
              MethodFieldParameterEnum.SameAsCashLoanOfferNumberOfCycles
            )}
          />
        );
      }

      case MethodFieldParameterEnum.AccrueInterestOnUnbilledInterest: {
        const value = accrueInterestOnUnbilledInterest.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.AccrueInterestOnUnbilledInterest]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__applyPenaltyPricingOnLoanOffer"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.AccrueInterestOnUnbilledInterest,
              true
            )}
            options={accrueInterestOnUnbilledInterest}
          />
        );
      }

      case MethodFieldParameterEnum.DelayPaymentForStatementCycles: {
        return (
          <NumericTextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__delayPaymentForStatementCycles"
            size="sm"
            format="i0"
            maxLength={2}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum.DelayPaymentForStatementCycles
              ] || ''
            }
            onChange={onChange(
              MethodFieldParameterEnum.DelayPaymentForStatementCycles
            )}
          />
        );
      }

      case MethodFieldParameterEnum.DelayPaymentForMonths: {
        return (
          <NumericTextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__delayPaymentForMonths"
            size="sm"
            format="i0"
            maxLength={2}
            autoFocus={false}
            value={
              method?.[MethodFieldParameterEnum.DelayPaymentForMonths] || ''
            }
            onChange={onChange(MethodFieldParameterEnum.DelayPaymentForMonths)}
          />
        );
      }
    }
  };

  const columns: ColumnType[] = [
    {
      id: 'fieldName',
      Header: t('txt_business_name'),
      accessor: 'fieldName',
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'onlinePCF',
      Header: t('txt_manage_penalty_fee_online_PCF'),
      accessor: 'onlinePCF',
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'value',
      Header: t('txt_value'),
      accessor: valueAccessor,
      cellBodyProps: { className: 'py-8' },
      width: width < 1280 ? 240 : undefined
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      accessor: viewMoreInfo(['moreInfo'], t),
      width: 105,
      cellBodyProps: { className: 'pt-12 pb-8' }
    }
  ];

  return (
    <div className="p-20">
      <Grid columns={columns} data={data} />
    </div>
  );
};

export default SubGridNewMethod;
