import {
  fireEvent,
  queryByText,
  render,
  RenderResult
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { ServiceSubjectSection } from 'app/constants/enums';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowDesignLoanOffer';
import React from 'react';
import * as reactRedux from 'react-redux';
import ConfigureParameters from './ConfigureParameters';

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');
const MockSelectElementMetadataForDLO = jest.spyOn(
  WorkflowSetup,
  'useSelectElementMetadataForDLO'
);

const mockOnCloseWithoutMethod = jest.fn();
jest.mock('./MethodListModal', () => ({
  __esModule: true,
  default: ({ onClose }: any) => {
    const handleOnClose = () => {
      if (mockOnCloseWithoutMethod()) {
        onClose();
        return;
      }

      onClose({
        id: '1',
        name: 'method-1',
        versions: [
          {
            id: 'version 1.0'
          }
        ]
      });
    };

    return <div onClick={handleOnClose}>MethodListModal_component</div>;
  }
}));

jest.mock('./AddNewMethodModal', () => ({
  __esModule: true,
  default: ({ onClose }: any) => {
    const handleOnClose = () => {
      if (mockOnCloseWithoutMethod()) {
        onClose();
        return;
      }

      onClose(
        {
          id: '1',
          name: 'method-1',
          versions: [
            {
              id: 'version 1.0'
            }
          ]
        },
        true
      );
    };
    return <div onClick={handleOnClose}>AddNewMethodModal_component</div>;
  }
}));

jest.mock(
  'pages/WorkflowManagePenaltyFee/LateChargesStep/RemoveMethodModal',
  () => ({
    __esModule: true,
    default: ({ onClose }: any) => {
      const handleOnClose = () => {
        if (mockOnCloseWithoutMethod()) {
          onClose();
          return;
        }

        onClose({
          id: '1',
          name: 'method-1',
          versions: [
            {
              id: 'version 1.0'
            }
          ]
        });
      };
      return <div onClick={handleOnClose}>RemoveMethodModal_component</div>;
    }
  })
);

jest.mock('./SubRowGrid', () => ({
  __esModule: true,
  default: () => <div>SubRowGrid_component</div>
}));

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <ConfigureParameters {...props} />
    </div>
  );
};
const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

describe('pages > WorkflowDesignLoanOffers > DesignLoanOffersStep > ConfigureParameters', () => {
  const mockDispatch = jest.fn();
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
    MockSelectElementMetadataForDLO.mockReturnValue({
      promotionDescription: [{ code: 'code', text: 'text' }],
      promotionAlternateDescription: [{ code: 'code', text: 'text' }],
      promotionDescriptionDisplayOptions: [{ code: 'code', text: 'text' }],
      promotionStatementTextIDDTDD: [{ code: 'code', text: 'text' }],
      promotionStatementTextIDDTDX: [{ code: 'code', text: 'text' }],
      promotionStatementTextIDStandard: [{ code: 'code', text: 'text' }],
      promotionStatementTextControl: [{ code: 'code', text: 'text' }],
      promotionReturnApplicationOption: [{ code: 'code', text: 'text' }],
      promotionPayoffExceptionOption: [{ code: 'code', text: 'text' }],
      loanBalanceClassification: [{ code: 'code', text: 'text' }],
      payoffExceptionMethod: [{ code: 'code', text: 'text' }],
      cashAdvanceItemFeesMethod: [{ code: 'code', text: 'text' }],
      merchantItemFeesMethod: [{ code: 'code', text: 'text' }],
      creditApplicationGroup: [{ code: 'code', text: 'text' }],
      promotionGroupIdentifier: [{ code: 'code', text: 'text' }],
      standardMinimumPaymentCalculationCode: [{ code: 'code', text: 'text' }],
      rulesMinimumPaymentDueMethod: [{ code: 'code', text: 'text' }],
      standardMinimumPaymentsRate: [{ code: 'code', text: 'text' }],
      standardMinimumPaymentRoundingOptions: [{ code: 'code', text: 'text' }],
      annualInterestRate: [{ code: 'code', text: 'text' }],
      payoutPeriod: [{ code: 'code', text: 'text' }],
      merchantDiscount: [{ code: 'code', text: 'text' }],
      merchantDiscountCalculationCode: [{ code: 'code', text: 'text' }],
      returnToRevolvingBaseInterest: [{ code: 'code', text: 'text' }],
      applyPenaltyPricingOnLoanOffer: [{ code: 'code', text: 'text' }],
      defaultInterestRateMethodOverride: [{ code: 'code', text: 'text' }],
      interestCalculationMethodOverride: [{ code: 'code', text: 'text' }],
      introductoryMessageDisplayControl: [{ code: 'code', text: 'text' }],
      positiveAmortizationUsageCode: [{ code: 'code', text: 'text' }],
      introductoryAnnualRate: [{ code: 'code', text: 'text' }],
      introductoryAnnualRateFixedEndDate: [{ code: 'code', text: 'text' }],
      introductoryRateNumberOfDays: [{ code: 'code', text: 'text' }],
      introductoryRateNumberOfCycles: [{ code: 'code', text: 'text' }],
      introductoryPeriodStatementTextID: [{ code: 'code', text: 'text' }],
      sameAsCashLoanOfferNumberOfCycles: [{ code: 'code', text: 'text' }],
      accrueInterestOnUnbilledInterest: [{ code: 'code', text: 'text' }],
      delayPaymentForStatementCycles: [{ code: 'code', text: 'text' }],
      delayPaymentForMonths: [{ code: 'code', text: 'text' }],
      designPromotionBasicInformation3: [{ code: 'code', text: 'text' }],
      designPromotionBasicInformation4: [{ code: 'code', text: 'text' }],
      designPromotionBasicInformation5: [{ code: 'code', text: 'text' }],
      designPromotionBasicInformation6: [{ code: 'code', text: 'text' }],
      designPromotionBasicInformation7: [{ code: 'code', text: 'text' }],
      designPromotionBasicInformation8: [{ code: 'code', text: 'text' }],
      designPromotionBasicInformation9: [{ code: 'code', text: 'text' }],
      designPromotionBasicInformation10: [{ code: 'code', text: 'text' }],
      designPromotionBasicInformation11: [{ code: 'code', text: 'text' }],
      designPromotionBasicInformation12: [{ code: 'code', text: 'text' }],
      designPromotionInterestAssessment9: [{ code: 'code', text: 'text' }],
      designPromotionInterestAssessment10: [{ code: 'code', text: 'text' }],
      designPromotionInterestAssessment11: [{ code: 'code', text: 'text' }],
      designPromotionInterestAssessment12: [{ code: 'code', text: 'text' }],
      designPromotionInterestAssessment13: [{ code: 'code', text: 'text' }],
      designPromotionInterestAssessment14: [{ code: 'code', text: 'text' }],
      designPromotionInterestAssessment15: [{ code: 'code', text: 'text' }],
      designPromotionInterestAssessment16: [{ code: 'code', text: 'text' }],
      designPromotionInterestAssessment17: [{ code: 'code', text: 'text' }],
      designPromotionInterestOverrides1: [{ code: 'code', text: 'text' }],
      designPromotionInterestOverrides2: [{ code: 'code', text: 'text' }],
      designPromotionInterestOverrides3: [{ code: 'code', text: 'text' }],
      designPromotionInterestOverrides4: [{ code: 'code', text: 'text' }],
      designPromotionInterestOverrides5: [{ code: 'code', text: 'text' }],
      designPromotionInterestOverrides6: [{ code: 'code', text: 'text' }],
      designPromotionInterestOverrides7: [{ code: 'code', text: 'text' }],
      designPromotionInterestOverrides8: [{ code: 'code', text: 'text' }],
      designPromotionRetailPath1: [{ code: 'code', text: 'text' }],
      designPromotionRetailPath4: [{ code: 'code', text: 'text' }],
      designPromotionReturnRevolve2: [{ code: 'code', text: 'text' }],
      designPromotionReturnRevolve3: [{ code: 'code', text: 'text' }],
      designPromotionReturnRevolve4: [{ code: 'code', text: 'text' }],
      designPromotionReturnRevolve6: [{ code: 'code', text: 'text' }],
      designPromotionStatementMessaging1: [{ code: 'code', text: 'text' }],
      designPromotionStatementMessaging2: [{ code: 'code', text: 'text' }],
      designPromotionStatementMessaging3: [{ code: 'code', text: 'text' }],
      designPromotionStatementMessaging4: [{ code: 'code', text: 'text' }],
      designPromotionStatementMessaging5: [{ code: 'code', text: 'text' }]
    });

    mockOnCloseWithoutMethod.mockReturnValue(false);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Should render with no method > Create new version', () => {
    const props = {
      stepId: '1',
      selectedStep: 'selectedStep',
      formValues: {},
      saveAt: 0,
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_select_an_option')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    );
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('MethodListModal_component'));
    expect(
      queryByText(wrapper.container, /MethodListModal_component/i)
    ).not.toBeInTheDocument();

    mockOnCloseWithoutMethod.mockReturnValue(true);
    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    );
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('MethodListModal_component'));
    expect(
      queryByText(wrapper.container, /MethodListModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with no method > Choose Method to Model', () => {
    const props = {
      stepId: '1',
      selectedStep: 'selectedStep',
      formValues: {},
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_select_an_option')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_choose_method_to_model')
    );
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('MethodListModal_component'));
    expect(
      queryByText(wrapper.container, /MethodListModal_component/i)
    ).not.toBeInTheDocument();

    mockOnCloseWithoutMethod.mockReturnValue(true);
    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_choose_method_to_model')
    );

    fireEvent.click(wrapper.getByText('MethodListModal_component'));
    expect(
      queryByText(wrapper.container, /MethodListModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with no method > Add New Method', () => {
    const props = {
      stepId: '1',
      selectedStep: 'selectedStep',
      formValues: {},
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_select_an_option')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_method')
    );
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();

    mockOnCloseWithoutMethod.mockReturnValue(true);
    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_method')
    );
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with DLO Method > Edit method', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    expect(body?.children.length).toEqual(3);

    //SHOW Add new method modal
    const editButton = body?.children[1].querySelector(
      'button[class="btn btn-outline-primary btn-sm"]'
    ) as Element;
    fireEvent.click(editButton);
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
    //End

    mockOnCloseWithoutMethod.mockReturnValue(true);
    fireEvent.click(editButton);
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with DLO Method > Add new method', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );

    //SHOW AddNewMethodModal
    const editButton = body?.children[0].querySelector(
      'button[class="btn btn-outline-primary btn-sm"]'
    ) as Element;
    fireEvent.click(editButton);
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
    //End

    mockOnCloseWithoutMethod.mockReturnValue(true);
    fireEvent.click(editButton);
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with DLO Method > Remove method', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );

    //SHOW RemoveMethodModal
    const removeButton = body?.children[0].querySelector(
      'button[class="btn btn-outline-danger btn-sm ml-8"]'
    ) as Element;

    fireEvent.click(removeButton);
    expect(
      wrapper.getByText('RemoveMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('RemoveMethodModal_component'));
    expect(
      queryByText(wrapper.container, /RemoveMethodModal_component/i)
    ).not.toBeInTheDocument();
    //End

    mockOnCloseWithoutMethod.mockReturnValue(true);
    fireEvent.click(removeButton);
    expect(
      wrapper.getByText('RemoveMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('RemoveMethodModal_component'));
    expect(
      queryByText(wrapper.container, /RemoveMethodModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with DLO Method > click Add New Method on dropdown button > show paging', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWVERSION',
            rowId: 3
          },
          {
            id: '4',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '5',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '6',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWVERSION',
            rowId: 3
          },
          {
            id: '7',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '8',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '9',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWVERSION',
            rowId: 3
          },
          {
            id: '10',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '11',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '12',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const dropdownButton = wrapper.container.querySelector(
      'button.dls-dropdown-button'
    ) as Element;
    userEvent.click(dropdownButton);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_create_new_method')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_method')
    );
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with DLO Method > click Add New Method on dropdown button', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const dropdownButton = wrapper.container.querySelector(
      'button.dls-dropdown-button'
    ) as Element;
    userEvent.click(dropdownButton);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_create_new_method')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_method')
    );
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with DLO Method > click Create New Version on dropdown button', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const dropdownButton = wrapper.container.querySelector(
      'button.dls-dropdown-button'
    ) as Element;
    userEvent.click(dropdownButton);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    );
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('MethodListModal_component'));
    expect(
      queryByText(wrapper.container, /MethodListModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with DLO Method > click Choose Method to Model on dropdown button', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const dropdownButton = wrapper.container.querySelector(
      'button.dls-dropdown-button'
    ) as Element;
    userEvent.click(dropdownButton);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_choose_method_to_model')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_choose_method_to_model')
    );
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('MethodListModal_component'));
    expect(
      queryByText(wrapper.container, /MethodListModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with DLO Method > Expand method', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    const firstChild = body?.children[0].querySelector(
      'button[class="btn btn-icon-secondary btn-sm"]'
    );
    fireEvent.click(firstChild as Element);
    expect(
      queryByText(wrapper.container, /SubRowGrid_component/i)
    ).toBeInTheDocument();
    fireEvent.click(firstChild as Element);
    expect(
      queryByText(wrapper.container, /SubRowGrid_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with DLO Method > Edit method to list with method', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    expect(body?.children.length).toEqual(3);

    //SHOW Add new method modal
    const editButton = body?.children[1].querySelector(
      'button[class="btn btn-outline-primary btn-sm"]'
    ) as Element;

    //WITHOUT METHOD
    fireEvent.click(editButton);
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
    //End
    mockOnCloseWithoutMethod.mockReturnValue(true);
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('MethodListModal_component'));
  });

  it('Should render with DLO Method > Edit method to list without method', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    expect(body?.children.length).toEqual(3);

    //SHOW Add new method modal
    const editButton = body?.children[1].querySelector(
      'button[class="btn btn-outline-primary btn-sm"]'
    ) as Element;

    //WITH METHOD
    fireEvent.click(editButton);
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
    //End
    mockOnCloseWithoutMethod.mockReturnValue(false);
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('MethodListModal_component'));
  });
});
