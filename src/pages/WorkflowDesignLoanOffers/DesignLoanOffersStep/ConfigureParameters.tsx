import {
  BadgeColorType,
  FormatTime,
  MethodFieldParameterEnum,
  ServiceSubjectSection
} from 'app/constants/enums';
import {
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import {
  formatTimeDefault,
  mapGridExpandCollapse,
  mappingArrayKey
} from 'app/helpers';
import { usePaginationGrid, useUnsavedChangeRegistry } from 'app/hooks';
import { Button, ColumnType, Grid, useTranslation } from 'app/_libraries/_dls';
import { isEmpty, orderBy } from 'app/_libraries/_dls/lodash';
import ActionButtons from 'pages/WorkflowManagePenaltyFee/LateChargesStep/ActionButtons';
import RemoveMethodModal from 'pages/WorkflowManagePenaltyFee/LateChargesStep/RemoveMethodModal';
import {
  actionsWorkflowSetup,
  useSelectElementMetadataForDLO
} from 'pages/_commons/redux/WorkflowSetup';
import {
  formatBadge,
  formatText,
  formatTruncate
} from 'pages/_commons/Utils/formatGridField';
import Paging from 'pages/_commons/Utils/Paging';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';
import { DesignLoanOffersFormValue } from '.';
import AddNewMethodModal from './AddNewMethodModal';
import MethodListModal from './MethodListModal';
import SubRowGrid from './SubRowGrid';

const ConfigureParameters: React.FC<
  WorkflowSetupProps<DesignLoanOffersFormValue>
> = ({ setFormValues, formValues, stepId, selectedStep, savedAt }) => {
  const { t } = useTranslation();

  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;

  const dispatch = useDispatch();

  const [expandedList, setExpandedList] = useState<string[]>([]);
  const [openAddNewMethod, setOpenAddNewMethod] = useState<boolean>(false);
  const [openMethodToModel, setOpenMethodToModel] = useState<boolean>(false);
  const [openCreateNewVersion, setOpenCreateNewVersion] =
    useState<boolean>(false);
  const [removeMethodRowId, setRemoveMethodRowId] =
    useState<undefined | number>(undefined);
  const [editMethodRowId, setEditMethodRowId] =
    useState<undefined | number>(undefined);
  const [editMethodBackId, setEditMethodBackId] =
    useState<undefined | number>(undefined);
  const [hasChange, setHasChange] = useState(false);
  const [keepCreateMethodState, setKeepCreateMethodState] =
    useState<any | undefined>(undefined);

  const metadata = useSelectElementMetadataForDLO();
  const prevExpandedList = useRef<string[]>();
  const methods = useMemo(
    () => orderBy(formValues?.methods || [], ['name'], ['asc']),
    [formValues?.methods]
  );

  useEffect(() => {
    prevExpandedList.current = expandedList;
  }, [expandedList]);

  const methodNames = useMemo(
    () => mappingArrayKey(methods, 'name'),
    [methods]
  );

  const onRemoveMethod = useCallback((idx: number) => {
    setRemoveMethodRowId(idx);
  }, []);

  const onEditMethod = useCallback((idx: number) => {
    setEditMethodRowId(idx);
  }, []);

  const onClickAddNewMethod = () => {
    setOpenAddNewMethod(true);
  };

  const onClickMethodToModel = () => {
    setOpenMethodToModel(true);
  };

  const onClickCreateNewVersion = () => {
    setOpenCreateNewVersion(true);
  };

  const closeMethodListModalModal = () => {
    setOpenCreateNewVersion(false);
    setOpenMethodToModel(false);
  };

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__DESIGN_LOAN_OFFERS__PARAMETERS,
      priority: 1
    },
    [hasChange]
  );

  const methodTypeToText = useCallback(
    (type: WorkflowSetupMethodType) => {
      switch (type) {
        case 'NEWVERSION':
          return t('txt_manage_penalty_fee_version_created');
        case 'MODELEDMETHOD':
          return t('txt_manage_penalty_fee_method_modeled');
      }
      return t('txt_manage_penalty_fee_method_created');
    },
    [t]
  );

  const columns: ColumnType[] = useMemo(
    () => [
      {
        id: 'methodName',
        Header: t('txt_method_name'),
        accessor: formatText(['name']),
        width: 105
      },
      {
        id: 'modelOrCreateFrom',
        Header: t('txt_manage_penalty_fee_modeled_or_create_from'),
        accessor: formatText(['modeledFrom', 'name']),
        width: 120
      },
      {
        id: 'comment',
        Header: t('txt_comment_area'),
        accessor: formatTruncate(['comment']),
        width: 312
      },
      {
        id: 'methodType',
        Header: t('txt_manage_penalty_fee_action_taken'),
        accessor: (data, idx) =>
          formatBadge(['methodType'], {
            colorType: BadgeColorType.MethodType,
            noBorder: true
          })(
            {
              ...data,
              methodType: methodTypeToText(data.methodType)
            },
            idx
          ),
        width: 153
      },
      {
        id: 'actions',
        Header: t('txt_actions'),
        accessor: (data: any) => (
          <div className="d-flex justify-content-center">
            <Button
              size="sm"
              variant="outline-primary"
              onClick={() => onEditMethod(data.rowId)}
            >
              {t('txt_edit')}
            </Button>
            <Button
              size="sm"
              variant="outline-danger"
              className="ml-8"
              onClick={() => onRemoveMethod(data.rowId)}
            >
              {t('txt_delete')}
            </Button>
          </div>
        ),
        className: 'text-center',
        cellBodyProps: { className: 'py-8' },
        width: 142
      }
    ],
    [t, methodTypeToText, onEditMethod, onRemoveMethod]
  );

  const [minDateSetting, setMinDateSetting] = useState();
  const [maxDateSetting, setMaxDateSetting] = useState();

  const minDate = useMemo(() => {
    return new Date(formatTimeDefault(minDateSetting!, FormatTime.Date));
  }, [minDateSetting]);

  const maxDate = useMemo(() => {
    return new Date(formatTimeDefault(maxDateSetting!, FormatTime.Date));
  }, [maxDateSetting]);

  const initFields = useCallback(async () => {
    const responseDateRange = await Promise.resolve(
      dispatch(actionsWorkflowSetup.getWorkflowEffectiveDateOptions({}))
    );

    const { maxDate: maxDateRes, minDate: minDateRes } =
      (responseDateRange as any)?.payload?.data || {};

    setMinDateSetting(minDateRes);
    setMaxDateSetting(maxDateRes);
  }, [dispatch]);

  useEffect(() => {
    initFields();
  }, [initFields]);

  useEffect(() => {
    dispatch(
      actionsWorkflowSetup.getWorkflowMetadata([
        MethodFieldParameterEnum.PromotionDescription,
        MethodFieldParameterEnum.PromotionAlternateDescription,
        MethodFieldParameterEnum.PromotionDescriptionDisplayOptions,
        MethodFieldParameterEnum.PromotionStatementTextIDDTDD,
        MethodFieldParameterEnum.PromotionStatementTextIDDTDX,
        MethodFieldParameterEnum.PromotionStatementTextIDStandard,
        MethodFieldParameterEnum.PromotionStatementTextControl,
        MethodFieldParameterEnum.PromotionReturnApplicationOption,
        MethodFieldParameterEnum.PromotionPayoffExceptionOption,
        MethodFieldParameterEnum.LoanBalanceClassification,
        MethodFieldParameterEnum.PayoffExceptionMethod,
        MethodFieldParameterEnum.CashAdvanceItemFeesMethod,
        MethodFieldParameterEnum.MerchantItemFeesMethod,
        MethodFieldParameterEnum.CreditApplicationGroup,
        MethodFieldParameterEnum.PromotionGroupIdentifier,
        MethodFieldParameterEnum.StandardMinimumPaymentCalculationCode,
        MethodFieldParameterEnum.RulesMinimumPaymentDueMethod,
        MethodFieldParameterEnum.StandardMinimumPaymentsRate,
        MethodFieldParameterEnum.StandardMinimumPaymentRoundingOptions,
        MethodFieldParameterEnum.AnnualInterestRate,
        MethodFieldParameterEnum.PayoutPeriod,
        MethodFieldParameterEnum.MerchantDiscount,
        MethodFieldParameterEnum.MerchantDiscountCalculationCode,
        MethodFieldParameterEnum.ReturnToRevolvingBaseInterest,
        MethodFieldParameterEnum.ApplyPenaltyPricingOnLoanOffer,
        MethodFieldParameterEnum.DefaultInterestRateMethodOverride,
        MethodFieldParameterEnum.InterestCalculationMethodOverride,
        MethodFieldParameterEnum.IntroductoryMessageDisplayControl,
        MethodFieldParameterEnum.PositiveAmortizationUsageCode,
        MethodFieldParameterEnum.IntroductoryAnnualRate,
        MethodFieldParameterEnum.IntroductoryAnnualRateFixedEndDate,
        MethodFieldParameterEnum.IntroductoryRateNumberOfDays,
        MethodFieldParameterEnum.IntroductoryRateNumberOfCycles,
        MethodFieldParameterEnum.IntroductoryPeriodStatementTextID,
        MethodFieldParameterEnum.SameAsCashLoanOfferNumberOfCycles,
        MethodFieldParameterEnum.AccrueInterestOnUnbilledInterest,
        MethodFieldParameterEnum.DelayPaymentForStatementCycles,
        MethodFieldParameterEnum.DelayPaymentForMonths
      ])
    );
  }, [dispatch]);

  const {
    total,
    currentPage,
    currentPageSize,
    pageData,
    onPageChange,
    onPageSizeChange
  } = usePaginationGrid(methods);

  useEffect(() => {
    const isValid = (formValues?.methods?.length || 0) > 0;
    if (formValues?.isValid === isValid) return;

    keepRef?.current?.setFormValues({ isValid });
  }, [formValues]);

  useEffect(() => {
    if (stepId !== selectedStep) {
      setExpandedList([]);
    }
    setHasChange(false);
  }, [stepId, selectedStep, savedAt]);

  const handleOnExpand = (dataKeyList: string[]) => {
    setExpandedList(isEmpty(dataKeyList) ? [] : dataKeyList);
  };

  const renderGrid = useMemo(() => {
    if (isEmpty(pageData)) return;
    return (
      <div className="pt-16">
        <Grid
          togglable
          columns={columns}
          data={pageData}
          subRow={({ original }: any) => (
            <SubRowGrid original={original} metadata={metadata} />
          )}
          dataItemKey="rowId"
          toggleButtonConfigList={pageData.map(
            mapGridExpandCollapse('rowId', t)
          )}
          expandedItemKey={'rowId'}
          expandedList={expandedList}
          onExpand={handleOnExpand}
          scrollable
        />
        {total > 10 && (
          <Paging
            totalItem={total}
            pageSize={currentPageSize}
            page={currentPage}
            onChangePage={onPageChange}
            onChangePageSize={onPageSizeChange}
          />
        )}
      </div>
    );
  }, [
    columns,
    metadata,
    expandedList,
    t,
    total,
    currentPageSize,
    currentPage,
    pageData,
    onPageChange,
    onPageSizeChange
  ]);

  return (
    <>
      <ActionButtons
        small={!isEmpty(methods)}
        title="txt_method_list"
        onClickAddNewMethod={onClickAddNewMethod}
        onClickMethodToModel={onClickMethodToModel}
        onClickCreateNewVersion={onClickCreateNewVersion}
      />
      {renderGrid}
      {removeMethodRowId !== undefined && (
        <RemoveMethodModal
          id="removeMethod"
          show
          methodName={methods.find(i => i.rowId === removeMethodRowId)?.name}
          onClose={method => {
            if (method) {
              setFormValues({
                methods: methods.filter(i => i.rowId !== removeMethodRowId)
              });
              setHasChange(true);
            }
            setRemoveMethodRowId(undefined);
          }}
        />
      )}
      {editMethodRowId !== undefined && (
        <AddNewMethodModal
          id="addNewMethod"
          show
          isCreate={false}
          method={methods.find(i => i.rowId === editMethodRowId)}
          methodNames={methodNames}
          minDate={minDate}
          maxDate={maxDate}
          onClose={(method, isBack, keepState) => {
            if (method) {
              method.serviceSubjectSection = ServiceSubjectSection.DLO;
              const newMethds = methods.map(item => {
                if (item.rowId === editMethodRowId) return method;
                return item;
              });
              setFormValues({ methods: newMethds });
              setHasChange(true);

              if (isBack) {
                setEditMethodBackId(editMethodRowId);
                setKeepCreateMethodState(keepState);
              }
            }
            setEditMethodRowId(undefined);
          }}
        />
      )}
      {editMethodBackId && (
        <MethodListModal
          id="editMethodFromModel"
          show
          keepState={keepCreateMethodState}
          methodNames={methodNames}
          isCreateNewVersion={
            methods.find(i => i.rowId === editMethodBackId)!.methodType ===
            'NEWVERSION'
          }
          defaultMethod={methods.find(i => i.rowId === editMethodBackId)}
          onClose={method => {
            if (method) {
              method.serviceSubjectSection = ServiceSubjectSection.DLO;
              const newMethds = methods.map(item => {
                if (item.rowId === editMethodBackId) return method;
                return item;
              });
              setFormValues({ methods: newMethds });
              setHasChange(true);
            }
            setEditMethodBackId(undefined);
            setKeepCreateMethodState(undefined);
          }}
        />
      )}
      {(openCreateNewVersion || openMethodToModel) && (
        <MethodListModal
          id="addMethodFromModel"
          show
          methodNames={methodNames}
          isCreateNewVersion={openCreateNewVersion}
          onClose={newMethod => {
            if (newMethod) {
              const rowId = Date.now();
              setFormValues({
                methods: [
                  {
                    ...newMethod,
                    rowId,
                    serviceSubjectSection: ServiceSubjectSection.DLO
                  },
                  ...methods
                ]
              });
              setExpandedList(
                prevExpandedList?.current?.concat([rowId as any]) as any
              );
              setHasChange(true);
            }
            closeMethodListModalModal();
          }}
        />
      )}
      {openAddNewMethod && (
        <AddNewMethodModal
          id="addNewMethod"
          show
          methodNames={methodNames}
          minDate={minDate}
          maxDate={maxDate}
          onClose={method => {
            if (method) {
              const rowId = Date.now();
              setFormValues({
                methods: [
                  {
                    ...method,
                    rowId,
                    serviceSubjectSection: ServiceSubjectSection.DLO
                  },
                  ...methods
                ]
              });

              setExpandedList(
                prevExpandedList?.current?.concat([rowId as any]) as any
              );
              setHasChange(true);
            }
            setOpenAddNewMethod(false);
          }}
        />
      )}
    </>
  );
};

export default ConfigureParameters;
