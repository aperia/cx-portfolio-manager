import { parameterGroup, parameterList } from './newMethodParameterList';

describe('pages > WorkflowDesignLoanOffers > DesignLoanOffersStep > newMethodParameterList', () => {
  it('parameterList', () => {
    expect(parameterList).toEqual([
      {
        id: 'Info_LoanOfferBasicSettings',
        section: 'txt_design_loan_offers_standard_parameters',
        parameterGroup: 'txt_design_loan_offers_loan_offer_basic_settings'
      },
      {
        id: 'Info_PaymentAndPricingSettings',
        section: 'txt_design_loan_offers_standard_parameters',
        parameterGroup: 'txt_design_loan_offers_payment_and_pricing_settings'
      },
      {
        id: 'Info_MerchantDiscount',
        section: 'txt_design_loan_offers_standard_parameters',
        parameterGroup: 'txt_design_loan_offers_merchant_discount',
        moreInfo: 'txt_design_loan_offer_merchant_discount_tooltip'
      },
      {
        id: 'Adv_MiscellaneousControls',
        section: 'txt_design_loan_offers_advanced_parameters',
        parameterGroup: 'txt_design_loan_offers_miscellaneous_controls'
      },
      {
        id: 'Adv_IntroductoryAprSettings',
        section: 'txt_design_loan_offers_advanced_parameters',
        parameterGroup: 'txt_design_loan_offers_introductory_apr_settings'
      },
      {
        id: 'Adv_PromotionalCashSettings',
        section: 'txt_design_loan_offers_advanced_parameters',
        parameterGroup: 'txt_design_loan_offers_promotional_cash_settings'
      },
      {
        id: 'Adv_PaymentDelaySettings',
        section: 'txt_design_loan_offers_advanced_parameters',
        parameterGroup: 'txt_design_loan_offers_payment_delay_settings'
      }
    ]);
  });
  it('parameterGroup', () => {
    const result = parameterGroup;

    expect(Object.keys(result).length).toEqual(7);
  });
});
