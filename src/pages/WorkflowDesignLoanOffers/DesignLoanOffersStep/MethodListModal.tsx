import FailedApiReload from 'app/components/FailedApiReload';
import MethodCardView from 'app/components/MethodCardView';
import ModalRegistry from 'app/components/ModalRegistry';
import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import WorkflowStrategyFlyout from 'app/components/WorkflowStrategyFlyout';
import { WORKFLOW_METHODS_SORT_BY_LIST_FOR_DESIGN_LOAN_OFFERS } from 'app/constants/constants';
import { ServiceSubjectSection } from 'app/constants/enums';
import { classnames } from 'app/helpers';
import {
  InlineMessage,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  TransDLS,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty } from 'lodash';
import isFunction from 'lodash.isfunction';
import set from 'lodash.set';
import {
  actionsWorkflowSetup,
  useSelectExpandingMethod,
  useSelectStrategyFlyoutSelector,
  useSelectWorkflowMethodsDLOSelector,
  useSelectWorkflowMethodsFilterSelector
} from 'pages/_commons/redux/WorkflowSetup';
import { defaultWorkflowPCFMethodsFilter } from 'pages/_commons/redux/WorkflowSetup/reducers';
import Paging from 'pages/_commons/Utils/Paging';
import SortOrder from 'pages/_commons/Utils/SortOrder';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';
import AddNewMethodModal from './AddNewMethodModal';
import { ParameterListIds } from './newMethodParameterList';

interface IKeepState {
  searchValue: string;
  expandedList: ParameterListIds[];
}
interface IMethodListModalProps {
  id: string;
  show: boolean;
  isCreateNewVersion?: boolean;
  keepState?: IKeepState;
  methodNames: string[];
  defaultMethod?: WorkflowSetupMethod & { rowId?: number };
  onClose?: (method?: WorkflowSetupMethod & { rowId?: number }) => void;
}

const MethodListModal: React.FC<IMethodListModalProps> = ({
  id,
  show = false,
  isCreateNewVersion = false,
  keepState,
  defaultMethod,
  methodNames = [],
  onClose
}) => {
  const [draftMethod, setDraftMethod] =
    useState<(WorkflowSetupMethod & { rowId?: number }) | undefined>(undefined);
  const [keepCreateMethodState, setKeepCreateMethodState] =
    useState<IKeepState | undefined>(keepState);
  const defaultVersionId =
    defaultMethod && defaultMethod!.modeledFrom?.versions[0]?.id;
  const defaultMethodId = defaultMethod && defaultMethod!.modeledFrom?.id;

  const [showCreateMethodModal, setShowCreateMethodModal] = useState(false);
  const [selectedModel, setSelectedModel] = useState<IMethodModel>();
  const [selectedVersion, setSelectedVersion] = useState(defaultVersionId);

  const strategyFlyout = useSelectStrategyFlyoutSelector();

  const [title, helpText1, announcement] = useMemo(() => {
    let title, helpText1, announcement;
    if (isCreateNewVersion) {
      title = 'Create New Version';
      helpText1 = 'txt_design_loan_offers_step_3_create_new_version_help_text';
      announcement = `When you create a new version of a PL RT PC method, existing promotional balances will be impacted.`;
    } else {
      title = 'Choose Method to Model';
      helpText1 =
        'txt_design_loan_offers_step_3_create_choose_version_help_text';
    }
    return [title, helpText1, announcement];
  }, [isCreateNewVersion]);
  const { methods, total, loading, error } =
    useSelectWorkflowMethodsDLOSelector();
  const dataFilter = useSelectWorkflowMethodsFilterSelector();
  const { page, pageSize, searchValue } = dataFilter;
  const expandingMethod = useSelectExpandingMethod();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const simpleSearchRef = useRef<any>(null);
  const handleSearch = useCallback(
    (val: string) => {
      dispatch(
        actionsWorkflowSetup.updateWorkflowMethodFilter({
          page: 1,
          searchValue: val
        })
      );
    },
    [dispatch]
  );
  const handleToggle = useCallback(
    (methodId: string) => {
      dispatch(actionsWorkflowSetup.updateExpandingMethod(methodId));
    },
    [dispatch]
  );

  const handlePageChange = useCallback(
    (page: number) => {
      dispatch(actionsWorkflowSetup.updateWorkflowMethodFilter({ page }));
    },
    [dispatch]
  );

  const handlePageSizeChange = useCallback(
    (pageSize: number) => {
      dispatch(actionsWorkflowSetup.updateWorkflowMethodFilter({ pageSize }));
    },
    [dispatch]
  );

  const handleChangeOrderBy = useCallback(
    (orderBy: string) => {
      dispatch(
        actionsWorkflowSetup.updateWorkflowMethodFilter({
          orderBy: [orderBy as OrderByType]
        })
      );
    },
    [dispatch]
  );

  const handleClearAndReset = useCallback(() => {
    dispatch(actionsWorkflowSetup.clearAndResetWorkflowPCFMethodFilter());
  }, [dispatch]);

  const handleClearAndResetAll = useCallback(() => {
    dispatch(
      actionsWorkflowSetup.clearAndResetWorkflowPCFMethodFilter({
        resetSort: true
      })
    );
  }, [dispatch]);

  const handleApiReload = useCallback(() => {
    dispatch(
      actionsWorkflowSetup.getManagePenaltyFeeWorkflowMethods({
        serviceSubjectSection: ServiceSubjectSection.DLO,
        defaultMethodId: defaultMethodId
      })
    );
  }, [dispatch, defaultMethodId]);

  useEffect(() => {
    !searchValue && simpleSearchRef?.current?.clear();
  }, [searchValue]);

  useEffect(() => {
    dispatch(
      actionsWorkflowSetup.getManagePenaltyFeeWorkflowMethods({
        serviceSubjectSection: ServiceSubjectSection.DLO,
        defaultMethodId: defaultMethodId
      })
    );
    return () => {
      dispatch(
        actionsWorkflowSetup.updateWorkflowMethodFilter({
          ...defaultWorkflowPCFMethodsFilter
        })
      );
    };
  }, [dispatch, defaultMethodId]);

  const isSelectedBeforeVersion = useMemo(
    () =>
      (!isEmpty(defaultMethod) && selectedVersion === defaultVersionId) ||
      (!isEmpty(draftMethod) &&
        selectedVersion === draftMethod?.modeledFrom?.versions[0]?.id),
    [defaultMethod, selectedVersion, draftMethod, defaultVersionId]
  );

  const methodList = useMemo(
    () => (
      <div>
        {!loading && !error && total > 0 && (
          <React.Fragment>
            <div className="mt-16 mx-n12">
              {methods.map((method: any) => (
                <MethodCardView
                  id={method.id}
                  key={method.id}
                  value={method}
                  serviceSubjectSection="DLO"
                  isCreateNewVersion={isCreateNewVersion}
                  selectedVersion={selectedVersion}
                  onSelectVersion={(model, versionId) => {
                    setSelectedModel(model);
                    setSelectedVersion(versionId);
                  }}
                  isHideCommentArea={true}
                  isExpand={expandingMethod === method.id}
                  onToggle={handleToggle}
                />
              ))}
            </div>
            <div className="mt-8">
              <Paging
                page={page}
                pageSize={pageSize}
                totalItem={total}
                onChangePage={handlePageChange}
                onChangePageSize={handlePageSizeChange}
              />
            </div>
          </React.Fragment>
        )}
      </div>
    ),
    [
      error,
      expandingMethod,
      handlePageChange,
      handlePageSizeChange,
      handleToggle,
      isCreateNewVersion,
      total,
      page,
      pageSize,
      loading,
      methods,
      selectedVersion
    ]
  );

  const noDataFound = useMemo(
    () =>
      !loading &&
      !error &&
      total === 0 && (
        <div className="d-flex flex-column justify-content-center mt-40">
          <NoDataFound
            id="method-list-NoDataFound"
            title={t('No methods to display')}
            hasSearch={!!searchValue}
            linkTitle={!!searchValue && t('txt_clear_and_reset')}
            onLinkClicked={handleClearAndReset}
          />
        </div>
      ),
    [handleClearAndReset, t, loading, error, total, searchValue]
  );
  const failedReload = useMemo(
    () => (
      <div>
        {error && (
          <FailedApiReload
            id="method-list-error"
            onReload={handleApiReload}
            className="mt-40 pt-40 d-flex flex-column justify-content-center align-items-center"
          />
        )}
      </div>
    ),
    [error, handleApiReload]
  );

  const methodSelected = useMemo<WorkflowSetupMethod>(() => {
    const version = selectedModel?.versions.find(v => v.id === selectedVersion);
    const model = {
      id: '',
      name: isCreateNewVersion ? selectedModel?.name : '',
      comment: '',
      methodType: isCreateNewVersion ? 'NEWVERSION' : 'MODELEDMETHOD',
      parameters: version?.parameters,
      modeledFrom: {
        id: selectedModel?.id,
        name: selectedModel?.name,
        versions: [{ id: version?.id }]
      }
    } as WorkflowSetupMethod;
    return model;
  }, [isCreateNewVersion, selectedModel, selectedVersion]);
  const handleCloseAndAdd = (
    method: WorkflowSetupMethod & { rowId?: number }
  ) => {
    isFunction(onClose) && onClose(method);
  };
  const handleClose = () => {
    isFunction(onClose) && onClose();
  };
  const handleStrategyFlyoutClosed = useCallback(() => {
    dispatch(actionsWorkflowSetup.setStrategyFlyout(null));
  }, [dispatch]);

  const sortByList = WORKFLOW_METHODS_SORT_BY_LIST_FOR_DESIGN_LOAN_OFFERS;

  return (
    <React.Fragment>
      <ModalRegistry
        id={id}
        show={show}
        lg
        onAutoClosedAll={handleClose}
        onAutoClosed={handleClearAndResetAll}
      >
        <ModalHeader border closeButton onHide={handleClose}>
          <ModalTitle>{title}</ModalTitle>
        </ModalHeader>
        <ModalBody
          className={classnames(
            'px-24 pt-24 pb-24 pb-lg-0 overflow-auto',
            loading && 'loading'
          )}
        >
          <div>
            {announcement && (
              <InlineMessage variant="info" withIcon className="mb-24">
                {announcement}
              </InlineMessage>
            )}
            <p className="color-grey">
              <TransDLS keyTranslation={helpText1}>
                <strong className="color-grey-d20" />
              </TransDLS>
            </p>
          </div>
          <div className="mt-24">
            <div className="d-flex justify-content-between align-items-center">
              <h5 className="title">{t('txt_method_list')}</h5>
              {(total > 0 || searchValue) && (
                <div className="method-list-simple-search">
                  <SimpleSearch
                    defaultValue={searchValue}
                    ref={simpleSearchRef}
                    clearTooltip={t('txt_clear_search_criteria')}
                    onSearch={handleSearch}
                    placeholder="Type to search by method name"
                  />
                </div>
              )}
            </div>
            {total > 0 && (
              <div className="d-flex justify-content-end align-items-center mt-16">
                <SortOrder
                  hasFilter={!!searchValue}
                  dataFilter={dataFilter}
                  defaultDataFilter={defaultWorkflowPCFMethodsFilter}
                  sortByFields={sortByList}
                  isHideSort={true}
                  onChangeOrderBy={handleChangeOrderBy}
                  onClearSearch={handleClearAndReset}
                />
              </div>
            )}
          </div>
          {methodList}
          {noDataFound}
          {failedReload}
        </ModalBody>

        <ModalFooter
          cancelButtonText={t('txt_cancel')}
          okButtonText={t('txt_continue')}
          onCancel={handleClose}
          onOk={() => setShowCreateMethodModal(true)}
          disabledOk={!selectedVersion}
        />
      </ModalRegistry>
      {showCreateMethodModal && (
        <AddNewMethodModal
          id={
            isCreateNewVersion
              ? 'addNewMethod_createNewVersion'
              : 'addNewMethod_cloneFromModel'
          }
          show
          keepState={
            isSelectedBeforeVersion ? keepCreateMethodState : undefined
          }
          methodNames={methodNames}
          draftMethod={isSelectedBeforeVersion ? draftMethod : undefined}
          method={
            isSelectedBeforeVersion && defaultMethod
              ? defaultMethod
              : methodSelected
          }
          onClose={(method, isBack, keepState) => {
            setShowCreateMethodModal(false);

            if (isBack && method) {
              draftMethod && set(method, 'rowId', draftMethod.rowId);
              setDraftMethod(method);
              setKeepCreateMethodState(keepState);
              return;
            }
            if (method) {
              defaultMethod && set(method, 'rowId', defaultMethod.rowId);
              handleCloseAndAdd(method);
              return;
            }

            !isBack && handleClose();
          }}
        />
      )}
      {strategyFlyout != null && (
        <WorkflowStrategyFlyout
          {...strategyFlyout}
          onClose={handleStrategyFlyoutClosed}
        />
      )}
    </React.Fragment>
  );
};

export default MethodListModal;
