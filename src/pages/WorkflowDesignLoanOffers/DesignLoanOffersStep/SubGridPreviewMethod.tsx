import { FormatTime, MethodFieldParameterEnum } from 'app/constants/enums';
import { formatCommon, formatTimeDefault, matchSearchValue } from 'app/helpers';
import {
  ColumnType,
  Grid,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React, { useMemo } from 'react';
import { parameterGroup, ParameterList } from './newMethodParameterList';
import { ParameterValueType } from './SubRowGrid';

interface IProps {
  original: ParameterList;
  metadata: ParameterValueType;
}
const SubGridPreviewMethod: React.FC<IProps> = ({ original, metadata }) => {
  const { t } = useTranslation();

  const {
    promotionDescription,
    promotionAlternateDescription,
    promotionDescriptionDisplayOptions,
    promotionStatementTextIDDTDD,
    promotionStatementTextIDDTDX,
    promotionStatementTextIDStandard,
    promotionStatementTextControl,
    promotionReturnApplicationOption,
    promotionPayoffExceptionOption,
    payoffExceptionMethod,
    cashAdvanceItemFeesMethod,
    merchantItemFeesMethod,
    creditApplicationGroup,
    promotionGroupIdentifier,
    loanBalanceClassification,
    standardMinimumPaymentCalculationCode,
    rulesMinimumPaymentDueMethod,
    standardMinimumPaymentsRate,
    standardMinimumPaymentRoundingOptions,
    annualInterestRate,
    payoutPeriod,
    merchantDiscount,
    merchantDiscountCalculationCode,
    returnToRevolvingBaseInterest,
    applyPenaltyPricingOnLoanOffer,
    defaultInterestRateMethodOverride,
    interestCalculationMethodOverride,
    introductoryMessageDisplayControl,
    positiveAmortizationUsageCode,
    introductoryAnnualRate,
    introductoryAnnualRateFixedEndDate,
    introductoryRateNumberOfDays,
    introductoryRateNumberOfCycles,
    introductoryPeriodStatementTextID,
    sameAsCashLoanOfferNumberOfCycles,
    accrueInterestOnUnbilledInterest,
    delayPaymentForStatementCycles,
    delayPaymentForMonths
  } = metadata;

  const data = useMemo(
    () =>
      parameterGroup[original.id].filter(p =>
        matchSearchValue([p.fieldName, p.moreInfoText, p.onlinePCF], undefined)
      ),
    [original.id]
  );

  const formatPercent = (text: string | number | undefined) => {
    if (!text) return '';
    return `${text}%`;
  };

  const valueAccessor = (data: Record<string, any>) => {
    const paramId = data.id as MethodFieldParameterEnum;

    switch (paramId) {
      case MethodFieldParameterEnum.PromotionDescription: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {promotionDescription}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.PromotionAlternateDescription: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {promotionAlternateDescription}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.PromotionDescriptionDisplayOptions: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {promotionDescriptionDisplayOptions}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.PromotionStatementTextIDDTDD: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {promotionStatementTextIDDTDD}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.PromotionStatementTextIDDTDX: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {promotionStatementTextIDDTDX}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.PromotionStatementTextIDStandard: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {promotionStatementTextIDStandard}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.PromotionStatementTextControl: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {promotionStatementTextControl}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.PromotionReturnApplicationOption: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {promotionReturnApplicationOption}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.PromotionPayoffExceptionOption: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {promotionPayoffExceptionOption}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.PayoffExceptionMethod: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {payoffExceptionMethod}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.CashAdvanceItemFeesMethod: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {cashAdvanceItemFeesMethod}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.MerchantItemFeesMethod: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {merchantItemFeesMethod}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.CreditApplicationGroup: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {creditApplicationGroup}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.PromotionGroupIdentifier: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {promotionGroupIdentifier}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.LoanBalanceClassification: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {loanBalanceClassification}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.StandardMinimumPaymentCalculationCode: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {standardMinimumPaymentCalculationCode}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.RulesMinimumPaymentDueMethod: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {rulesMinimumPaymentDueMethod}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.StandardMinimumPaymentsRate: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {formatPercent(standardMinimumPaymentsRate)}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.StandardMinimumPaymentRoundingOptions: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {standardMinimumPaymentRoundingOptions}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.AnnualInterestRate: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {formatPercent(annualInterestRate)}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.PayoutPeriod: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {payoutPeriod}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.MerchantDiscount: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {formatPercent(merchantDiscount)}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.MerchantDiscountCalculationCode: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {merchantDiscountCalculationCode}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.ReturnToRevolvingBaseInterest: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {returnToRevolvingBaseInterest}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.ApplyPenaltyPricingOnLoanOffer: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {applyPenaltyPricingOnLoanOffer}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.DefaultInterestRateMethodOverride: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {defaultInterestRateMethodOverride}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.InterestCalculationMethodOverride: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {interestCalculationMethodOverride}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.IntroductoryMessageDisplayControl: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {introductoryMessageDisplayControl}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.PositiveAmortizationUsageCode: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {positiveAmortizationUsageCode}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.IntroductoryAnnualRate: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {formatPercent(introductoryAnnualRate)}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.IntroductoryAnnualRateFixedEndDate: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {introductoryAnnualRateFixedEndDate
              ? formatTimeDefault(
                  new Date(introductoryAnnualRateFixedEndDate).toString(),
                  FormatTime.Date
                )
              : ''}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.IntroductoryRateNumberOfDays: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {formatCommon(introductoryRateNumberOfDays!).quantity}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.IntroductoryRateNumberOfCycles: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {introductoryRateNumberOfCycles}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.IntroductoryPeriodStatementTextID: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {introductoryPeriodStatementTextID}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.SameAsCashLoanOfferNumberOfCycles: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {sameAsCashLoanOfferNumberOfCycles}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.AccrueInterestOnUnbilledInterest: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {accrueInterestOnUnbilledInterest}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.DelayPaymentForStatementCycles: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {delayPaymentForStatementCycles}
          </TruncateText>
        );
      }
      case MethodFieldParameterEnum.DelayPaymentForMonths: {
        return (
          <TruncateText
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {delayPaymentForMonths}
          </TruncateText>
        );
      }
    }
  };

  const columns: ColumnType[] = [
    {
      id: 'fieldName',
      Header: t('txt_business_name'),
      accessor: 'fieldName'
    },
    {
      id: 'onlinePCF',
      Header: t('txt_manage_penalty_fee_online_PCF'),
      accessor: 'onlinePCF'
    },
    {
      id: 'value',
      Header: t('txt_value'),
      accessor: valueAccessor
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      accessor: viewMoreInfo(['moreInfo'], t),
      width: 105
    }
  ];

  return (
    <div className="p-20">
      <Grid columns={columns} data={data} />
    </div>
  );
};

export default SubGridPreviewMethod;
