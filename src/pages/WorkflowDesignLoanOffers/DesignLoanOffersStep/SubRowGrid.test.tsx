import { renderComponent } from 'app/utils';
import React from 'react';
import { parameterList } from './newMethodParameterList';
import SubRowGrid from './SubRowGrid';

const t = value => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

jest.mock('./SubGridPreviewMethod', () => ({
  __esModule: true,
  default: () => <div>SubGridPreviewMethod</div>
}));

describe('pages > WorkflowDesignLoanOffers > DesignLoanOffersStep > SubRowGrid', () => {
  it('render', async () => {
    const original = {
      id: '',
      name: 'aaaa',
      comment: '',
      methodType: 'MODELEDMETHOD',
      modeledFrom: {
        id: '312689',
        name: 'at consetetur',
        versions: [{ id: '1032937' }]
      },
      versionParameters: [
        {
          name: 'promotion.description',
          previousValue: 'dignissim ex tempor velit',
          newValue: 'dolor magna erat illum'
        },
        {
          name: 'promotion.alternate.description',
          previousValue: 'nam sadipscing tation lorem labore et',
          newValue: 'voluptua diam labore kasd erat'
        },
        {
          name: 'promotion.description.display.options',
          previousValue: '2',
          newValue: '0'
        },
        {
          name: 'promotion.statement.text.id.dt.dd',
          previousValue: 'TEXTDFCI',
          newValue: 'TEXTDFAR'
        },
        {
          name: 'promotion.statement.text.id.dt.dx',
          previousValue: 'TEXTDFCP',
          newValue: ''
        },
        {
          name: 'promotion.statement.text.id',
          previousValue: 'TEXTDFCP',
          newValue: 'TEXTDFAR'
        },
        {
          name: 'promotion.statement.text.control',
          previousValue: '0',
          newValue: '1'
        },
        {
          name: 'promotion.return.application.option',
          previousValue: '0',
          newValue: ''
        },
        {
          name: 'promotion.payoff.exception.option',
          previousValue: '1',
          newValue: '1'
        },
        {
          name: 'payoff.exception.method.cp.ic.pe',
          previousValue: 'THDI1031',
          newValue: 'THDI1031'
        },
        {
          name: 'cash.advance.item.fees.method.cp.io.ci',
          previousValue: '',
          newValue: 'THDI1031'
        },
        {
          name: 'merchant.item.fees.method.cp.io.mi',
          previousValue: 'MTHD1921',
          newValue: 'MTHD2921'
        },
        {
          name: 'credit.application.group',
          previousValue: '10',
          newValue: '02'
        },
        {
          name: 'promotion.group.identifier',
          previousValue: '',
          newValue: 'THDI1031'
        },
        {
          name: 'loan.balance.classification',
          previousValue: '',
          newValue: 'loanNewValue'
        },
        {
          name: 'standard.minimum.payment.calculation.code',
          previousValue: '',
          newValue: '8'
        },
        {
          name: 'rules.minimum.payment.due.method.pl.rt.mp',
          previousValue: 'MTHD1921',
          newValue: 'MTHD1921'
        },
        {
          name: 'standard.minimum.payments.rate',
          previousValue: '94',
          newValue: '62'
        },
        {
          name: 'standard.minimum.payment.rounding.options',
          previousValue: 'B',
          newValue: 'B'
        },
        { name: 'annual.interest.rate', previousValue: '57', newValue: '21' },
        { name: 'payout.period', previousValue: '137', newValue: '873' },
        { name: 'merchant.discount', previousValue: '65', newValue: '77' },
        {
          name: 'merchant.discount.calculation.code',
          previousValue: '',
          newValue: ''
        },
        {
          name: 'return.to.revolving.base.interest',
          previousValue: '0',
          newValue: '1'
        },
        {
          name: 'apply.penalty.pricing.on.loan.offer',
          previousValue: '0',
          newValue: ''
        },
        {
          name: 'default.interest.rate.method.override',
          previousValue: 'THDI1032',
          newValue: 'MTHD1921'
        },
        {
          name: 'interest.calculation.method.override',
          previousValue: 'THDI1032',
          newValue: 'THDI1031'
        },
        {
          name: 'introductory.message.display.control',
          previousValue: '1',
          newValue: '1'
        },
        {
          name: 'positive.amortization.usage.code',
          previousValue: '',
          newValue: '4'
        },
        {
          name: 'introductory.annual.rate',
          previousValue: '38',
          newValue: '33'
        },
        {
          name: 'introductory.annual.rate.fixed.end.date',
          previousValue: '1/15/2021 5:00:00 PM',
          newValue: '3/18/2021 5:00:00 PM'
        },
        {
          name: 'introductory.rate.number.of.days',
          previousValue: '7197',
          newValue: '90113'
        },
        {
          name: 'introductory.rate.number.of.cycles',
          previousValue: '74',
          newValue: '57'
        },
        {
          name: 'introductory.period.statement.text.id',
          previousValue: '',
          newValue: 'MTHD1921'
        },
        {
          name: 'same.as.cash.loan.offer.number.of.cycles',
          previousValue: '43',
          newValue: '1'
        },
        {
          name: 'accrue.interest.on.unbilled.interest',
          previousValue: '',
          newValue: '0'
        },
        {
          name: 'delay.payment.for.statement.cycles',
          previousValue: '67',
          newValue: '98'
        },
        {
          name: 'delay.payment.for.months',
          previousValue: '90',
          newValue: '43'
        },
        { name: 'delay.payment.start', previousValue: '0', newValue: '0' },
        {
          name: 'introductory.minimum.payments.delay.number.of.days',
          previousValue: '26193',
          newValue: '73919'
        },
        {
          name: 'return.to.revolve.internal.status',
          previousValue: 'Blank',
          newValue: 'D'
        },
        {
          name: 'promotion.expiration.text.id',
          previousValue: 'TEXTBRAS',
          newValue: 'TEXTBRAE'
        },
        {
          name: 'expiring.promotion.notification.months',
          previousValue: '3',
          newValue: '99'
        },
        {
          name: 'payment.delay.number.of.cycles',
          previousValue: '94',
          newValue: '40'
        },
        {
          name: 'interest.on.unbilled.interest',
          previousValue: '',
          newValue: ''
        },
        { name: 'delay.interest.start.code', previousValue: '', newValue: '2' },
        {
          name: 'exclude.cash.option.from.2.cycle.average.daily.balance',
          previousValue: '1',
          newValue: '2'
        },
        {
          name: 'introductory.interest.default.cp.ic.id.set.1',
          previousValue: 'MTHODFE2',
          newValue: ''
        },
        {
          name: 'introductory.interest.default.cp.ic.id.set.2',
          previousValue: 'THIBOBE01',
          newValue: 'MTHMBE03'
        },
        {
          name: 'introductory.interest.default.cp.ic.id.set.3',
          previousValue: 'MTHMQC2',
          newValue: 'MTHMQC2'
        },
        {
          name: 'interest.defaults.cp.ic.id.set.1',
          previousValue: 'MANDRIL5',
          newValue: ''
        },
        {
          name: 'interest.defaults.cp.ic.id.set.2',
          previousValue: 'RESHUS29',
          newValue: 'RESHUS29'
        },
        {
          name: 'interest.defaults.cp.ic.id.set.3',
          previousValue: 'BELUG117',
          newValue: 'BELUG117'
        },
        {
          name: 'minimum.payment.pl.rt.mp.set.1',
          previousValue: 'MTHMHR1',
          newValue: ''
        },
        {
          name: 'minimum.payment.pl.rt.mp.set.2',
          previousValue: '',
          newValue: 'MTHMPM2'
        },
        {
          name: 'minimum.payment.pl.rt.mp.set.3',
          previousValue: '',
          newValue: 'THERATI1'
        },
        {
          name: 'associate.with.plan.pl.rt.pa',
          previousValue: 'MTHMBK2',
          newValue: ''
        },
        {
          name: 'zero.interest.extend.expiration.cd',
          previousValue: '1',
          newValue: '0'
        },
        {
          name: 'promotion.payoff.exception.option',
          previousValue: '3',
          newValue: '0'
        },
        {
          name: 'introductory.promotion.load.time',
          previousValue: '',
          newValue: '1'
        },
        { name: 'regular.annual.load.time', previousValue: '0', newValue: '' },
        { name: 'cpocpp.return.usage', previousValue: 'Y', newValue: 'N' },
        { name: 'billed.pay.due.nm.code', previousValue: '', newValue: '' },
        { name: 'reinstate.mpd.terms.code', previousValue: '', newValue: '1' },
        {
          name: 'promotion.billed.interest.code',
          previousValue: '0',
          newValue: '0'
        },
        {
          name: 'statement.promotion.display.method.id.pl.rt.sd',
          previousValue: 'RAPTOR2',
          newValue: 'RAPTOR2'
        },
        {
          name: 'alternate.promotion.description',
          previousValue: 'vero illum quis sea',
          newValue: 'ipsum duis diam'
        },
        {
          name: 'promotion.description.display',
          previousValue: '3',
          newValue: '3'
        },
        { name: 'promotion.type', previousValue: '5', newValue: '5' },
        {
          name: 'promotion.date.range',
          previousValue: '3/22/2021 5:00:00 PM',
          newValue: '10/27/2021 5:00:00 PM'
        },
        {
          name: 'credit.application.options',
          previousValue: '2',
          newValue: '2'
        },
        {
          name: 'credit.application.group.id',
          previousValue: 'GRPID003',
          newValue: 'GRPID003'
        },
        { name: 'multi.ticket.criteria', previousValue: '0', newValue: '1' },
        {
          name: 'multi.ticket.terms.creation',
          previousValue: '1',
          newValue: '1'
        },
        {
          name: 'balance.transfer.promotion.option',
          previousValue: '',
          newValue: '1'
        },
        {
          name: 'number.of.retentin.months',
          previousValue: '79',
          newValue: '97'
        },
        {
          name: 'promotional.interest.rate',
          previousValue: '46',
          newValue: '30'
        },
        {
          name: 'introductory.promotional.interest.rate',
          previousValue: '17',
          newValue: '64'
        },
        {
          name: 'promotional.interest.delay.date',
          previousValue: '3/21/2021 5:00:00 PM',
          newValue: '8/9/2021 5:00:00 PM'
        },
        {
          name: 'promotional.interest.delay.number.of.days',
          previousValue: '3475',
          newValue: '97796'
        },
        {
          name: 'promotional.interest.delay.number.of.cycles',
          previousValue: '67',
          newValue: '15'
        },
        {
          name: 'promotional.cash.option.number.of.days',
          previousValue: '968',
          newValue: '781'
        },
        {
          name: 'promotional.cash.option.number.of.cycles',
          previousValue: '89',
          newValue: '23'
        },
        { name: 'payoff.amount', previousValue: '32682', newValue: '73301' },
        {
          name: 'promotion.payoff.exception.options',
          previousValue: '',
          newValue: '1'
        },
        {
          name: 'merchandise.items.fees.cp.io.mi.set.1',
          previousValue: 'MTHMTI1',
          newValue: 'MTHMTI1'
        },
        {
          name: 'merchandise.items.fees.cp.io.mi.set.2',
          previousValue: 'MTHMTI1',
          newValue: 'MTHMTI2'
        },
        {
          name: 'merchandise.items.fees.cp.io.mi.set.3',
          previousValue: '',
          newValue: 'MTHMTI3'
        },
        {
          name: 'cash.advance.item.fees.cp.io.ci.set.1',
          previousValue: 'MTHMTI1',
          newValue: 'MTHMTI2'
        },
        {
          name: 'cash.advance.item.fees.cp.io.ci.set.2',
          previousValue: 'MTHMTI2',
          newValue: 'MTHMTI1'
        },
        {
          name: 'cash.advance.item.fees.cp.io.ci.set.3',
          previousValue: 'MTHMTI3',
          newValue: ''
        },
        { name: 'promotion.method.reload', previousValue: '1', newValue: '' },
        {
          name: 'promotion.interest.override.i.e',
          previousValue: '2',
          newValue: '1'
        },
        {
          name: 'reset.date',
          previousValue: '5/16/2021 5:00:00 PM',
          newValue: '1/2/2021 5:00:00 PM'
        },
        { name: 'base.interest.set.1', previousValue: '0', newValue: '1' },
        { name: 'base.interest.set.2', previousValue: '0', newValue: '2' },
        { name: 'base.interest.set.3', previousValue: '1', newValue: '0' },
        {
          name: 'payoff.exceptions.cp.ic.pe.set.1',
          previousValue: 'MTHMTI3',
          newValue: 'MTHMTI3'
        },
        {
          name: 'payoff.exceptions.cp.ic.pe.set.2',
          previousValue: 'MTHMTI2',
          newValue: 'MTHMTI2'
        },
        {
          name: 'payoff.exceptions.cp.ic.pe.set.3',
          previousValue: 'MTHMTI3',
          newValue: 'MTHMTI1'
        },
        {
          name: 'introductory.interest.options',
          previousValue: '',
          newValue: '0'
        },
        {
          name: 'regular.annual.interest.options',
          previousValue: '0',
          newValue: '2'
        },
        {
          name: 'r.path.promotion.return.indicator',
          previousValue: '0',
          newValue: '0'
        },
        {
          name: 'r.path.promotion.return.date.range',
          previousValue: '8/8/2021 5:00:00 PM',
          newValue: '10/4/2021 5:00:00 PM'
        },
        { name: 'merchant.discount.rate', previousValue: '31', newValue: '24' },
        {
          name: 'promotion.discount.calculation.code',
          previousValue: '',
          newValue: '0'
        },
        {
          name: 'number.of.days.delinquent',
          previousValue: '17',
          newValue: '49'
        },
        {
          name: 'collapse.return.to.revolving.option',
          previousValue: '',
          newValue: '0'
        },
        {
          name: 'collapse.expiration.date.option',
          previousValue: '0',
          newValue: '0'
        },
        {
          name: 'delinquency.return.timing.code',
          previousValue: '0',
          newValue: '0'
        },
        {
          name: 'return.to.revolve.delay.number.of.days',
          previousValue: '1',
          newValue: '61'
        },
        {
          name: 'return.to.revolving.base.interest',
          previousValue: '1',
          newValue: '0'
        },
        {
          name: 'promotion.statement.text.id',
          previousValue: 'TEXTDFAS',
          newValue: 'TEXTDFAR'
        },
        {
          name: 'introductory.promotional.statement.text.id',
          previousValue: 'TEXTDFVS',
          newValue: 'TEXTDFVE'
        },
        {
          name: 'introductory.message.control',
          previousValue: '3',
          newValue: ''
        },
        {
          name: 'regular.message.display.control',
          previousValue: '3',
          newValue: ''
        },
        {
          name: 'description.from.text.id',
          previousValue: 'TEXTDFQA',
          newValue: 'TEXTDFQF'
        }
      ],
      'description.from.text.id': 'TEXTDFQF',
      'regular.message.display.control': '',
      'introductory.message.control': '',
      'introductory.promotional.statement.text.id': 'TEXTDFVE',
      'promotion.statement.text.id': 'TEXTDFAR',
      'return.to.revolving.base.interest': '1',
      'return.to.revolve.delay.number.of.days': '61',
      'delinquency.return.timing.code': '0',
      'collapse.expiration.date.option': '0',
      'collapse.return.to.revolving.option': '0',
      'number.of.days.delinquent': '49',
      'promotion.discount.calculation.code': '0',
      'merchant.discount.rate': '24',
      'r.path.promotion.return.date.range': '10/4/2021 5:00:00 PM',
      'r.path.promotion.return.indicator': '0',
      'regular.annual.interest.options': '2',
      'introductory.interest.options': '0',
      'payoff.exceptions.cp.ic.pe.set.3': 'MTHMTI1',
      'payoff.exceptions.cp.ic.pe.set.2': 'MTHMTI2',
      'payoff.exceptions.cp.ic.pe.set.1': 'MTHMTI3',
      'base.interest.set.3': '0',
      'base.interest.set.2': '2',
      'base.interest.set.1': '1',
      'reset.date': '1/2/2021 5:00:00 PM',
      'promotion.interest.override.i.e': '1',
      'promotion.method.reload': '',
      'cash.advance.item.fees.cp.io.ci.set.3': '',
      'cash.advance.item.fees.cp.io.ci.set.2': 'MTHMTI1',
      'cash.advance.item.fees.cp.io.ci.set.1': 'MTHMTI2',
      'merchandise.items.fees.cp.io.mi.set.3': 'MTHMTI3',
      'merchandise.items.fees.cp.io.mi.set.2': 'MTHMTI2',
      'merchandise.items.fees.cp.io.mi.set.1': 'MTHMTI1',
      'promotion.payoff.exception.options': '1',
      'payoff.amount': '73301',
      'promotional.cash.option.number.of.cycles': '23',
      'promotional.cash.option.number.of.days': '781',
      'promotional.interest.delay.number.of.cycles': '15',
      'promotional.interest.delay.number.of.days': '97796',
      'promotional.interest.delay.date': '8/9/2021 5:00:00 PM',
      'introductory.promotional.interest.rate': '64',
      'promotional.interest.rate': '30',
      'number.of.retentin.months': '97',
      'balance.transfer.promotion.option': '1',
      'multi.ticket.terms.creation': '1',
      'multi.ticket.criteria': '1',
      'credit.application.group.id': 'GRPID003',
      'credit.application.options': '2',
      'promotion.date.range': '10/27/2021 5:00:00 PM',
      'promotion.type': '5',
      'promotion.description.display': '3',
      'alternate.promotion.description': 'ipsum duis diam',
      'statement.promotion.display.method.id.pl.rt.sd': 'RAPTOR2',
      'promotion.billed.interest.code': '0',
      'reinstate.mpd.terms.code': '1',
      'billed.pay.due.nm.code': '',
      'cpocpp.return.usage': 'N',
      'regular.annual.load.time': '',
      'introductory.promotion.load.time': '1',
      'promotion.payoff.exception.option': '1',
      'zero.interest.extend.expiration.cd': '0',
      'associate.with.plan.pl.rt.pa': '',
      'minimum.payment.pl.rt.mp.set.3': 'THERATI1',
      'minimum.payment.pl.rt.mp.set.2': 'MTHMPM2',
      'minimum.payment.pl.rt.mp.set.1': '',
      'interest.defaults.cp.ic.id.set.3': 'BELUG117',
      'interest.defaults.cp.ic.id.set.2': 'RESHUS29',
      'interest.defaults.cp.ic.id.set.1': '',
      'introductory.interest.default.cp.ic.id.set.3': 'MTHMQC2',
      'introductory.interest.default.cp.ic.id.set.2': 'MTHMBE03',
      'introductory.interest.default.cp.ic.id.set.1': '',
      'exclude.cash.option.from.2.cycle.average.daily.balance': '2',
      'delay.interest.start.code': '2',
      'interest.on.unbilled.interest': '',
      'payment.delay.number.of.cycles': '40',
      'expiring.promotion.notification.months': '99',
      'promotion.expiration.text.id': 'TEXTBRAE',
      'return.to.revolve.internal.status': 'D',
      'introductory.minimum.payments.delay.number.of.days': '73919',
      'delay.payment.start': '0',
      'delay.payment.for.months': '43',
      'delay.payment.for.statement.cycles': '98',
      'accrue.interest.on.unbilled.interest': '0',
      'same.as.cash.loan.offer.number.of.cycles': '1',
      'introductory.period.statement.text.id': 'MTHD1921',
      'introductory.rate.number.of.cycles': '57',
      'introductory.rate.number.of.days': '90113',
      'introductory.annual.rate.fixed.end.date': '3/18/2021 5:00:00 PM',
      'introductory.annual.rate': '33',
      'positive.amortization.usage.code': '4',
      'introductory.message.display.control': '1',
      'interest.calculation.method.override': 'THDI1031',
      'default.interest.rate.method.override': 'MTHD1921',
      'apply.penalty.pricing.on.loan.offer': '',
      'merchant.discount.calculation.code': '',
      'merchant.discount': '77',
      'payout.period': '873',
      'annual.interest.rate': '21',
      'standard.minimum.payment.rounding.options': 'B',
      'standard.minimum.payments.rate': '62',
      'rules.minimum.payment.due.method.pl.rt.mp': 'MTHD1921',
      'standard.minimum.payment.calculation.code': '8',
      'promotion.group.identifier': 'THDI1031',
      'loan.balance.classification': 'loanNewValue',
      'credit.application.group': '02',
      'merchant.item.fees.method.cp.io.mi': 'MTHD2921',
      'cash.advance.item.fees.method.cp.io.ci': 'THDI1031',
      'payoff.exception.method.cp.ic.pe': 'THDI1031',
      'promotion.return.application.option': '',
      'promotion.statement.text.control': '1',
      'promotion.statement.text.id.dt.dx': '',
      'promotion.statement.text.id.dt.dd': 'TEXTDFAR',
      'promotion.description.display.options': '0',
      'promotion.alternate.description': 'voluptua diam labore kasd erat',
      'promotion.description': 'dolor magna erat illum',
      parameters: [
        { name: 'late.charges.option', newValue: '', previousValue: '' },
        { name: 'calculation.base', newValue: '', previousValue: '' },
        { name: 'minimum.pay.due.option', newValue: '', previousValue: '' },
        { name: 'fixed.amount', newValue: '', previousValue: '' },
        { name: 'percent', newValue: '', previousValue: '' },
        { name: 'maximum.amount', newValue: '', previousValue: '' },
        { name: 'maximum.yearly.amount', newValue: '', previousValue: '' },
        { name: 'assessment.control', newValue: '', previousValue: '' },
        {
          name: 'cycles.of.consecutive.delinquency',
          newValue: '',
          previousValue: ''
        },
        { name: 'balance.indicator', newValue: '', previousValue: '' },
        { name: 'assessed.accounts', newValue: '', previousValue: '' },
        { name: 'exclusion.balance', newValue: '', previousValue: '' },
        { name: 'current.balance.assessment', newValue: '', previousValue: '' },
        { name: 'calculation.day.control', newValue: '', previousValue: '' },
        { name: 'number.of.days', newValue: '', previousValue: '' },
        { name: 'non.processing.late.charge', newValue: '', previousValue: '' },
        { name: 'include.exclude.control', newValue: '', previousValue: '' },
        { name: 'status.1', newValue: '', previousValue: '' },
        { name: 'status.2', newValue: '', previousValue: '' },
        { name: 'status.3', newValue: '', previousValue: '' },
        { name: 'status.4', newValue: '', previousValue: '' },
        { name: 'status.5', newValue: '', previousValue: '' },
        { name: 'tier.2.balance', newValue: '', previousValue: '' },
        { name: 'tier.2.amount', newValue: '', previousValue: '' },
        {
          name: 'threshold.to.wave.late.charge',
          newValue: '',
          previousValue: ''
        },
        { name: 'late.charge.reset.counter', newValue: '', previousValue: '' },
        { name: 'reversal.details.text.id', newValue: '', previousValue: '' },
        { name: 'waived.message.text.id', newValue: '', previousValue: '' },
        {
          name: 'late.payment.warning.message',
          newValue: '',
          previousValue: ''
        }
      ],
      rowId: 1636346098104,
      serviceSubjectSection: 'PL RT PC'
    };

    const metadata = {
      promotionDescription: 'example_txt',
      promotionAlternateDescription: 'example_txt',
      promotionDescriptionDisplayOptions: [
        { code: '', text: ' - None' },
        { code: '0', text: '0 - Do not display the promotion description.' },
        {
          code: '1',
          text: '1 - Display the description from the Description parameter.'
        },
        {
          code: '2',
          text: '2 - Display the description from the Alternate Description parameter.'
        },
        {
          code: '3',
          text: '3 - Display the description from the DD, Description Display, text type in the DT, Detail Descriptions, text area of the PCF Text Maintenance feature.'
        },
        {
          code: '4',
          text: '4 - Display the description from the DX, Description Extended, text type in the DT, Detail Descriptions, text area of the PCF Text Maintenance feature.'
        }
      ],
      promotionStatementTextIDDTDD: [
        { code: 'TEXTDFAD', text: 'TEXTDFAD - ' },
        { code: 'TEXTDFAJ', text: 'TEXTDFAJ - ' },
        { code: 'TEXTDFAP', text: 'TEXTDFAP - ' },
        { code: 'TEXTDFAR', text: 'TEXTDFAR - ' },
        { code: 'TEXTDFCA', text: 'TEXTDFCA - ' },
        { code: 'TEXTDFCC', text: 'TEXTDFCC - ' },
        { code: 'TEXTDFCF', text: 'TEXTDFCF - ' },
        { code: 'TEXTDFCI', text: 'TEXTDFCI - ' },
        { code: 'TEXTDFCL', text: 'TEXTDFCL - ' },
        { code: 'TEXTDFCP', text: 'TEXTDFCP - ' }
      ],
      promotionStatementTextIDDTDX: [
        { code: 'TEXTDFAD', text: 'TEXTDFAD - ' },
        { code: 'TEXTDFAJ', text: 'TEXTDFAJ - ' },
        { code: 'TEXTDFAP', text: 'TEXTDFAP - ' },
        { code: 'TEXTDFAR', text: 'TEXTDFAR - ' },
        { code: 'TEXTDFCA', text: 'TEXTDFCA - ' },
        { code: 'TEXTDFCC', text: 'TEXTDFCC - ' },
        { code: 'TEXTDFCF', text: 'TEXTDFCF - ' },
        { code: 'TEXTDFCI', text: 'TEXTDFCI - ' },
        { code: 'TEXTDFCL', text: 'TEXTDFCL - ' },
        { code: 'TEXTDFCP', text: 'TEXTDFCP - ' }
      ],
      promotionStatementTextIDStandard: [
        { code: 'TEXTDFAD', text: 'TEXTDFAD - ' },
        { code: 'TEXTDFAJ', text: 'TEXTDFAJ - ' },
        { code: 'TEXTDFAP', text: 'TEXTDFAP - ' },
        { code: 'TEXTDFAR', text: 'TEXTDFAR - ' },
        { code: 'TEXTDFCA', text: 'TEXTDFCA - ' },
        { code: 'TEXTDFCC', text: 'TEXTDFCC - ' },
        { code: 'TEXTDFCF', text: 'TEXTDFCF - ' },
        { code: 'TEXTDFCI', text: 'TEXTDFCI - ' },
        { code: 'TEXTDFCL', text: 'TEXTDFCL - ' },
        { code: 'TEXTDFCP', text: 'TEXTDFCP - ' },
        { code: 'TEXTDFAD', text: 'TEXTDFAD - ' },
        { code: 'TEXTDFAE', text: 'TEXTDFAE - ' },
        { code: 'TEXTDFAS', text: 'TEXTDFAS - ' },
        { code: 'TEXTDFAR', text: 'TEXTDFAR - ' }
      ],
      promotionStatementTextControl: [
        { code: '', text: ' - None' },
        { code: '0', text: '0 - Do not display the promotion description.' },
        {
          code: '1',
          text: '1 - Display text on statement and service screens.'
        },
        { code: '3', text: '3 - Display text only on service screens.' }
      ],
      promotionReturnApplicationOption: [
        { code: '', text: ' - None' },
        { code: '0', text: '0 - Option not used.' },
        {
          code: '1',
          text: '1 - Apply returns to the oldest balance ID of the promotion ID assigned by your TLP decision tables.'
        },
        {
          code: '2',
          text: '2 - Apply returns to the newest balance ID of the promotion ID assigned by your TLP decision tables.'
        },
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Option not used. Returns follow the credit application process you have established with your payment application tables and Product Control File settings.'
        },
        {
          code: '1',
          text: '1 - Apply returns to the oldest balance ID of the promotion ID assigned by your TLP decision tables.'
        }
      ],
      promotionPayoffExceptionOption: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Exclude this promotional balance from the amount required to meet payoff exception processing.'
        },
        {
          code: '1',
          text: '1 - Include this promotional balance in the amount required to meet payoff exception processing.'
        },
        {
          code: '2',
          text: '2 - Exclude this promotional balance from the amount required to meet payoff exception processing when the delay period end date is greater than the last statement date.'
        },
        {
          code: '3',
          text: '3 - Exclude this promotional balance from the amount required to meet payoff exception processing when the delay period end date is greater than the payoff exception date.'
        }
      ],
      payoffExceptionMethod: [
        { code: 'THDI1031', text: 'THDI1031 - ' },
        { code: 'THDI1032', text: 'THDI1032 - ' },
        { code: 'MTHD2921', text: 'MTHD2921 - ' },
        { code: 'MTHD1921', text: 'MTHD1921 - ' }
      ],
      cashAdvanceItemFeesMethod: [
        { code: 'THDI1031', text: 'THDI1031 - ' },
        { code: 'THDI1032', text: 'THDI1032 - ' },
        { code: 'METHOD2', text: 'METHOD2 - ' },
        { code: 'MTHD1921', text: 'MTHD1921 - ' }
      ],
      merchantItemFeesMethod: [
        { code: 'THDI1031', text: 'THDI1031 - ' },
        { code: 'THDI1032', text: 'THDI1032 - ' },
        { code: 'METHOD2', text: 'METHOD2 - ' },
        { code: 'MTHD1921', text: 'MTHD1921 - ' }
      ],
      creditApplicationGroup: [
        { code: '', text: ' - None' },
        { code: '1', text: '1 - 01' },
        { code: '2', text: '2 - 02' },
        { code: '3', text: '3 - 03' },
        { code: '4', text: '4 - 04' },
        { code: '5', text: '5 - 05' },
        { code: '6', text: '6 - 06' },
        { code: '7', text: '7 - 07' },
        { code: '8', text: '8 - 08' },
        { code: '9', text: '9 - 09' },
        { code: '10', text: '10 - 10' }
      ],
      promotionGroupIdentifier: [
        { code: 'THDI1031', text: 'THDI1031 - ' },
        { code: 'THDI1032', text: 'THDI1032 - ' },
        { code: 'MTHD2921', text: 'MTHD2921 - ' },
        { code: 'MTHD1921', text: 'MTHD1921 - ' }
      ],
      standardMinimumPaymentCalculationCode: [
        { code: '', text: ' - None' },
        {
          code: 'D',
          text: 'D - Reamortize the minimum payment due based on the current balance amount of the promotion.'
        },
        {
          code: 'F',
          text: 'F - Reamortize the minimum payment due for the remaining payment term.'
        },
        {
          code: 'H',
          text: 'H - Reamortize the minimum payment due amount based on the high balance amount of the promotion.'
        },
        {
          code: '0',
          text: '0 - Do not use this parameter to calculate minimum payment.'
        },
        {
          code: '1',
          text: '1 - Calculate fixed minimum payment by dividing the amount at posting by the setting in the Payout Period parameter in this section. Round up to the next whole dollar.'
        },
        {
          code: '2',
          text: '2 - Calculate fixed minimum payment by multiplying the amount at posting by the setting in the Standard Minimum Payments Rate parameter in this section. Round up to the next whole dollar.'
        },
        {
          code: '3',
          text: '3 - Calculate decreasing minimum payment by multiplying the outstanding principal by the setting in the Standard Minimum Payments Rate parameter at cycle time. Round according to the setting in the Rounding parameter in the Minimum Payment Due section (CP PO MP) of the Product Control File.'
        },
        {
          code: '4',
          text: '4 - Calculate decreasing minimum payment by multiplying the outstanding principal and interest by the setting in the Standard Minimum Payments Rate parameter at cycle time. Round according to the setting in the Rounding parameter in the Minimum Payment Due section (CP PO MP).'
        },
        {
          code: '5',
          text: '5 - Calculate fixed minimum payment by amortizing the principal and interest over the life of the promotion. Use the monthly installment as the minimum payment due.'
        },
        {
          code: '6',
          text: '6 - Calculate the minimum payment due (MPD) by using current total-amount-owed (TAO), the annual interest rate (INT), and the remaining payment period (N).'
        },
        { code: '7', text: '7 - Reserved for future use.' },
        {
          code: '8',
          text: '8 - Combine all promotional balances assigned to this calculation method. Calculate the fixed minimum payment due by using the charge parameter in the Fixed Minimum Payment section (CP PO FM) of the Product Control File that corresponds to the first amount field greater than the combined promotional balance for the designated method to arrive at an amount.'
        },
        {
          code: '9',
          text: '9 - Calculate fixed minimum payment due (MPD) by amortizing the principal, fees, and interest over the life of the promotion. Use the monthly installment as the minimum payment due.'
        }
      ],
      rulesMinimumPaymentDueMethod: [
        { code: 'THDI1031', text: 'THDI1031 - ' },
        { code: 'THDI1032', text: 'THDI1032 - ' },
        { code: 'MTHD2921', text: 'MTHD2921 - ' },
        { code: 'MTHD1921', text: 'MTHD1921 - ' }
      ],
      loanBalanceClassification: [
        { code: 'loanNewValue', text: 'THDI1031 - ' }
      ],
      standardMinimumPaymentsRate: [{ code: '21', text: '21' }],
      standardMinimumPaymentRoundingOptions: [
        { code: '', text: ' - None' },
        { code: 'B', text: 'B - Round the calculation to the higher penny.' },
        { code: '5', text: '5 - Round to nearest penny.' }
      ],
      annualInterestRate: [
        { code: '873', text: '873' },
        { code: '21', text: '21' }
      ],
      payoutPeriod: [{ code: '873', text: '873' }],
      merchantDiscount: [{ code: '77', text: '77' }],
      merchantDiscountCalculationCode: [
        { code: '', text: ' - None' },
        { code: '0', text: '0 - Do not use this option.' },
        {
          code: '1',
          text: '1 - Calculate discount based on the promotional discount rate only.'
        },
        {
          code: '2',
          text: '2 - Calculate discount based on adding the promotional discount rate to the merchant qualifying rate.'
        },
        { code: '77', text: '77' }
      ],
      returnToRevolvingBaseInterest: [
        { code: '', text: ' - None' },
        { code: '0', text: '0 - Do not use this option.' },
        {
          code: '1',
          text: '1 - If the promotional balance returns to revolving, use the interest rate at the next level up, the plan rate if one exists, or the revolving rate. Combine with other balances into a single average daily balance at the plan or revolving level. Maintain the promotion on the account record for the length of time specified in the Number of Retention Months parameter in this section.'
        }
      ],
      applyPenaltyPricingOnLoanOffer: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      defaultInterestRateMethodOverride: [
        { code: 'THDI1031', text: 'THDI1031 - ' },
        { code: 'THDI1032', text: 'THDI1032 - ' },
        { code: 'MTHD2921', text: 'MTHD2921 - ' },
        { code: 'MTHD1921', text: 'MTHD1921 - ' }
      ],
      interestCalculationMethodOverride: [
        { code: 'THDI1031', text: 'THDI1031 - ' },
        { code: 'THDI1032', text: 'THDI1032 - ' },
        { code: 'MTHD2921', text: 'MTHD2921 - ' },
        { code: 'MTHD1921', text: 'MTHD1921 - ' }
      ],
      introductoryMessageDisplayControl: [
        { code: '', text: ' - None' },
        {
          code: '1',
          text: '1 - Display the introductory promotional message both in CIS and on paper statements.'
        },
        {
          code: '2',
          text: '2 - Display the introductory promotional message only on paper statements.'
        },
        {
          code: '3',
          text: '3 - Display the introductory promotional message only in CIS.'
        }
      ],
      positiveAmortizationUsageCode: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Include the promotional balance in the positive amortization calculation.'
        },
        {
          code: '1',
          text: '1 - Exclude the promotional balance from the positive amortization calculation.'
        },
        {
          code: '2',
          text: '2 - Exclude the promotional balance from the positive amortization calculation during an introductory minimum payment due period.'
        },
        {
          code: '3',
          text: '3 - Exclude the promotional balance from the positive amortization calculation during an introductory interest period.'
        },
        {
          code: '4',
          text: '4 - Exclude the promotional balance from the positive amortization calculation during an introductory cash option period.'
        },
        {
          code: '5',
          text: '5 - Exclude the promotional balance from the positive amortization calculation during an introductory interest, minimum payment due, and/or cash option period.'
        }
      ],
      introductoryAnnualRate: [{ code: '33', text: '33' }],
      introductoryAnnualRateFixedEndDate: [
        { code: '3/18/2021 5:00:00 PM', text: '3/18/2021 5:00:00 PM' }
      ],
      introductoryRateNumberOfDays: [{ code: '90113', text: '90113' }],
      introductoryRateNumberOfCycles: [
        {
          code: '57',
          text: '57'
        },
        {
          code: '1',
          text: '1'
        }
      ],
      introductoryPeriodStatementTextID: [
        { code: 'THDI1031', text: 'THDI1031 - ' },
        { code: 'THDI1032', text: 'THDI1032 - ' },
        { code: 'MTHD2921', text: 'MTHD2921 - ' },
        { code: 'MTHD1921', text: 'MTHD1921 - ' }
      ],
      sameAsCashLoanOfferNumberOfCycles: [{ code: '1', text: '1' }],
      accrueInterestOnUnbilledInterest: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - No, do not accrue interest on unbilled interest.'
        },
        { code: '1', text: '1 - Yes, accrue interest on unbilled interest.' }
      ],
      delayPaymentForStatementCycles: [{ code: '98', text: '98' }],
      delayPaymentForMonths: [{ code: '43', text: '43' }]
    };

    const wrapper = await renderComponent(
      'testId',
      <SubRowGrid metadata={metadata} original={original} />
    );

    expect(
      wrapper.getByText(parameterList[0].parameterGroup)
    ).toBeInTheDocument();
    expect(
      wrapper.getByText(parameterList[1].parameterGroup)
    ).toBeInTheDocument();
    expect(
      wrapper.getByText(parameterList[2].parameterGroup)
    ).toBeInTheDocument();
  });
});
