import { MethodFieldParameterEnum } from 'app/constants/enums';
import { mapGridExpandCollapse, methodParamsToObject } from 'app/helpers';
import { Grid, useTranslation } from 'app/_libraries/_dls';
import React, { useState } from 'react';
import newMethodColumns from './newMethodColumns';
import { parameterList, ParameterListIds } from './newMethodParameterList';
import SubGridPreviewMethod from './SubGridPreviewMethod';

export interface ParameterValueType {
  promotionDescription: string | number | undefined;
  promotionAlternateDescription: string | number | undefined;
  promotionDescriptionDisplayOptions: string | number | undefined;
  promotionStatementTextIDDTDD: string | number | undefined;
  promotionStatementTextIDDTDX: string | number | undefined;
  promotionStatementTextIDStandard: string | number | undefined;
  promotionStatementTextControl: string | number | undefined;
  promotionReturnApplicationOption: string | number | undefined;
  promotionPayoffExceptionOption: string | number | undefined;
  payoffExceptionMethod: string | number | undefined;
  cashAdvanceItemFeesMethod: string | number | undefined;
  merchantItemFeesMethod: string | number | undefined;
  creditApplicationGroup: string | number | undefined;
  promotionGroupIdentifier: string | number | undefined;
  loanBalanceClassification: string | number | undefined;
  standardMinimumPaymentCalculationCode: string | number | undefined;
  rulesMinimumPaymentDueMethod: string | number | undefined;
  standardMinimumPaymentsRate: string | number | undefined;
  standardMinimumPaymentRoundingOptions: string | number | undefined;
  annualInterestRate: string | number | undefined;
  payoutPeriod: string | number | undefined;
  merchantDiscount: string | number | undefined;
  merchantDiscountCalculationCode: string | number | undefined;
  returnToRevolvingBaseInterest: string | number | undefined;
  applyPenaltyPricingOnLoanOffer: string | number | undefined;
  defaultInterestRateMethodOverride: string | number | undefined;
  interestCalculationMethodOverride: string | number | undefined;
  introductoryMessageDisplayControl: string | number | undefined;
  positiveAmortizationUsageCode: string | number | undefined;
  introductoryAnnualRate: string | number | undefined;
  introductoryAnnualRateFixedEndDate: string | number | undefined;
  introductoryRateNumberOfDays: string | number | undefined;
  introductoryRateNumberOfCycles: string | number | undefined;
  introductoryPeriodStatementTextID: string | number | undefined;
  sameAsCashLoanOfferNumberOfCycles: string | number | undefined;
  accrueInterestOnUnbilledInterest: string | number | undefined;
  delayPaymentForStatementCycles: string | number | undefined;
  delayPaymentForMonths: string | number | undefined;
}

export interface SubGridProps {
  original: any;
  metadata: {
    promotionDescription: RefData[];
    promotionAlternateDescription: RefData[];
    promotionDescriptionDisplayOptions: RefData[];
    promotionStatementTextIDDTDD: RefData[];
    promotionStatementTextIDDTDX: RefData[];
    promotionStatementTextIDStandard: RefData[];
    promotionStatementTextControl: RefData[];
    promotionReturnApplicationOption: RefData[];
    promotionPayoffExceptionOption: RefData[];
    payoffExceptionMethod: RefData[];
    cashAdvanceItemFeesMethod: RefData[];
    merchantItemFeesMethod: RefData[];
    creditApplicationGroup: RefData[];
    promotionGroupIdentifier: RefData[];
    loanBalanceClassification: RefData[];
    standardMinimumPaymentCalculationCode: RefData[];
    rulesMinimumPaymentDueMethod: RefData[];
    standardMinimumPaymentsRate: RefData[];
    standardMinimumPaymentRoundingOptions: RefData[];
    annualInterestRate: RefData[];
    payoutPeriod: RefData[];
    merchantDiscount: RefData[];
    merchantDiscountCalculationCode: RefData[];
    returnToRevolvingBaseInterest: RefData[];
    applyPenaltyPricingOnLoanOffer: RefData[];
    defaultInterestRateMethodOverride: RefData[];
    interestCalculationMethodOverride: RefData[];
    introductoryMessageDisplayControl: RefData[];
    positiveAmortizationUsageCode: RefData[];
    introductoryAnnualRate: RefData[];
    introductoryAnnualRateFixedEndDate: RefData[];
    introductoryRateNumberOfDays: RefData[];
    introductoryRateNumberOfCycles: RefData[];
    introductoryPeriodStatementTextID: RefData[];
    sameAsCashLoanOfferNumberOfCycles: RefData[];
    accrueInterestOnUnbilledInterest: RefData[];
    delayPaymentForStatementCycles: RefData[];
    delayPaymentForMonths: RefData[];
  };
}
const SubRowGrid: React.FC<SubGridProps> = ({ original, metadata }) => {
  const {
    promotionDescriptionDisplayOptions,
    promotionStatementTextIDDTDD,
    promotionStatementTextIDDTDX,
    promotionStatementTextIDStandard,
    promotionStatementTextControl,
    promotionReturnApplicationOption,
    promotionPayoffExceptionOption,
    payoffExceptionMethod,
    cashAdvanceItemFeesMethod,
    merchantItemFeesMethod,
    creditApplicationGroup,
    promotionGroupIdentifier,
    loanBalanceClassification,
    standardMinimumPaymentCalculationCode,
    rulesMinimumPaymentDueMethod,
    standardMinimumPaymentRoundingOptions,
    merchantDiscountCalculationCode,
    returnToRevolvingBaseInterest,
    applyPenaltyPricingOnLoanOffer,
    defaultInterestRateMethodOverride,
    interestCalculationMethodOverride,
    introductoryMessageDisplayControl,
    positiveAmortizationUsageCode,
    introductoryPeriodStatementTextID,
    accrueInterestOnUnbilledInterest
  } = metadata;

  const convertedObject = methodParamsToObject(original);

  const { t } = useTranslation();

  const data: ParameterValueType = {
    promotionDescription:
      convertedObject[MethodFieldParameterEnum.PromotionDescription],
    promotionAlternateDescription:
      convertedObject[MethodFieldParameterEnum.PromotionAlternateDescription],
    promotionDescriptionDisplayOptions: promotionDescriptionDisplayOptions.find(
      o =>
        o.code ===
        convertedObject[
          MethodFieldParameterEnum.PromotionDescriptionDisplayOptions
        ]
    )?.text,
    promotionStatementTextIDDTDD: promotionStatementTextIDDTDD.find(
      o =>
        o.code ===
        convertedObject[MethodFieldParameterEnum.PromotionStatementTextIDDTDD]
    )?.text,
    promotionStatementTextIDDTDX: promotionStatementTextIDDTDX.find(
      o =>
        o.code ===
        convertedObject[MethodFieldParameterEnum.PromotionStatementTextIDDTDX]
    )?.text,
    promotionStatementTextIDStandard: promotionStatementTextIDStandard.find(
      o =>
        o.code ===
        convertedObject[
          MethodFieldParameterEnum.PromotionStatementTextIDStandard
        ]
    )?.text,
    promotionStatementTextControl: promotionStatementTextControl.find(
      o =>
        o.code ===
        convertedObject[MethodFieldParameterEnum.PromotionStatementTextControl]
    )?.text,
    promotionReturnApplicationOption: promotionReturnApplicationOption.find(
      o =>
        o.code ===
        convertedObject[
          MethodFieldParameterEnum.PromotionReturnApplicationOption
        ]
    )?.text,
    promotionPayoffExceptionOption: promotionPayoffExceptionOption.find(
      o =>
        o.code ===
        convertedObject[MethodFieldParameterEnum.PromotionPayoffExceptionOption]
    )?.text,
    payoffExceptionMethod: payoffExceptionMethod.find(
      o =>
        o.code ===
        convertedObject[MethodFieldParameterEnum.PayoffExceptionMethod]
    )?.text,
    cashAdvanceItemFeesMethod: cashAdvanceItemFeesMethod.find(
      o =>
        o.code ===
        convertedObject[MethodFieldParameterEnum.CashAdvanceItemFeesMethod]
    )?.text,
    merchantItemFeesMethod: merchantItemFeesMethod.find(
      o =>
        o.code ===
        convertedObject[MethodFieldParameterEnum.MerchantItemFeesMethod]
    )?.text,
    creditApplicationGroup: creditApplicationGroup.find(
      o =>
        o.code ===
        convertedObject[MethodFieldParameterEnum.CreditApplicationGroup]
    )?.text,
    promotionGroupIdentifier: promotionGroupIdentifier.find(
      o =>
        o.code ===
        convertedObject[MethodFieldParameterEnum.PromotionGroupIdentifier]
    )?.text,
    loanBalanceClassification: loanBalanceClassification.find(
      o =>
        o.code ===
        convertedObject[MethodFieldParameterEnum.LoanBalanceClassification]
    )?.text,
    standardMinimumPaymentCalculationCode:
      standardMinimumPaymentCalculationCode.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.StandardMinimumPaymentCalculationCode
          ]
      )?.text,
    rulesMinimumPaymentDueMethod: rulesMinimumPaymentDueMethod.find(
      o =>
        o.code ===
        convertedObject[MethodFieldParameterEnum.RulesMinimumPaymentDueMethod]
    )?.text,
    standardMinimumPaymentsRate:
      convertedObject[MethodFieldParameterEnum.StandardMinimumPaymentsRate],
    standardMinimumPaymentRoundingOptions:
      standardMinimumPaymentRoundingOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.StandardMinimumPaymentRoundingOptions
          ]
      )?.text,
    annualInterestRate:
      convertedObject[MethodFieldParameterEnum.AnnualInterestRate],
    payoutPeriod: convertedObject[MethodFieldParameterEnum.PayoutPeriod],
    merchantDiscount:
      convertedObject[MethodFieldParameterEnum.MerchantDiscount],
    merchantDiscountCalculationCode: merchantDiscountCalculationCode.find(
      o =>
        o.code ===
        convertedObject[
          MethodFieldParameterEnum.MerchantDiscountCalculationCode
        ]
    )?.text,
    returnToRevolvingBaseInterest: returnToRevolvingBaseInterest.find(
      o =>
        o.code ===
        convertedObject[MethodFieldParameterEnum.ReturnToRevolvingBaseInterest]
    )?.text,
    applyPenaltyPricingOnLoanOffer: applyPenaltyPricingOnLoanOffer.find(
      o =>
        o.code ===
        convertedObject[MethodFieldParameterEnum.ApplyPenaltyPricingOnLoanOffer]
    )?.text,
    defaultInterestRateMethodOverride: defaultInterestRateMethodOverride.find(
      o =>
        o.code ===
        convertedObject[
          MethodFieldParameterEnum.DefaultInterestRateMethodOverride
        ]
    )?.text,
    interestCalculationMethodOverride: interestCalculationMethodOverride.find(
      o =>
        o.code ===
        convertedObject[
          MethodFieldParameterEnum.InterestCalculationMethodOverride
        ]
    )?.text,
    introductoryMessageDisplayControl: introductoryMessageDisplayControl.find(
      o =>
        o.code ===
        convertedObject[
          MethodFieldParameterEnum.IntroductoryMessageDisplayControl
        ]
    )?.text,
    positiveAmortizationUsageCode: positiveAmortizationUsageCode.find(
      o =>
        o.code ===
        convertedObject[MethodFieldParameterEnum.PositiveAmortizationUsageCode]
    )?.text,
    introductoryAnnualRate:
      convertedObject[MethodFieldParameterEnum.IntroductoryAnnualRate],
    introductoryAnnualRateFixedEndDate:
      convertedObject[
        MethodFieldParameterEnum.IntroductoryAnnualRateFixedEndDate
      ],
    introductoryRateNumberOfDays:
      convertedObject[MethodFieldParameterEnum.IntroductoryRateNumberOfDays],
    introductoryRateNumberOfCycles:
      convertedObject[MethodFieldParameterEnum.IntroductoryRateNumberOfCycles],
    introductoryPeriodStatementTextID: introductoryPeriodStatementTextID.find(
      o =>
        o.code ===
        convertedObject[
          MethodFieldParameterEnum.IntroductoryPeriodStatementTextID
        ]
    )?.text,
    sameAsCashLoanOfferNumberOfCycles:
      convertedObject[
        MethodFieldParameterEnum.SameAsCashLoanOfferNumberOfCycles
      ],
    accrueInterestOnUnbilledInterest: accrueInterestOnUnbilledInterest.find(
      o =>
        o.code ===
        convertedObject[
          MethodFieldParameterEnum.AccrueInterestOnUnbilledInterest
        ]
    )?.text,
    delayPaymentForStatementCycles:
      convertedObject[MethodFieldParameterEnum.DelayPaymentForStatementCycles],
    delayPaymentForMonths:
      convertedObject[MethodFieldParameterEnum.DelayPaymentForMonths]
  };

  const [expandedList, setExpandedList] = useState<ParameterListIds[]>([
    'Info_LoanOfferBasicSettings'
  ]);

  return (
    <div className="p-20 overflow-hidden">
      <Grid
        togglable
        columns={newMethodColumns(t)}
        data={parameterList}
        subRow={({ original }: MagicKeyValue) => (
          <SubGridPreviewMethod original={original} metadata={data} />
        )}
        dataItemKey="id"
        expandedItemKey="id"
        expandedList={expandedList}
        onExpand={setExpandedList as any}
        toggleButtonConfigList={parameterList.map(
          mapGridExpandCollapse('id', t)
        )}
      />
    </div>
  );
};

export default SubRowGrid;
