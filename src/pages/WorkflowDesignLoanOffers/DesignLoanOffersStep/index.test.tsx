import { renderWithMockStore } from 'app/utils';
import React from 'react';
import DesignLoanOffersStep from '.';

jest.mock('./ConfigureParameters', () => ({
  __esModule: true,
  default: () => <div>ConfigureParameters</div>
}));

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

const renderComponent = (props: any, initValues: any = {}) => {
  return renderWithMockStore(<DesignLoanOffersStep {...props} />, initValues);
};

describe('DesignLoanOffersStep', () => {
  it('Should render DesignLoanOffersStep contains Completed', async () => {
    const props = {};
    const wrapper = await renderComponent(props, {});

    expect(wrapper.getByText('txt_design_loan_offers_desc_1'))
      .toBeInTheDocument;
  });
});
