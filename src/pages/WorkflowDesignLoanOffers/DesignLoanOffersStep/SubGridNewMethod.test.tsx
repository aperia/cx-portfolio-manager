import { renderWithMockStore } from 'app/utils';
import React from 'react';
import SubGridNewMethod from './SubGridNewMethod';

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

const metadata = {
  promotionDescription: [{ code: 'code', text: 'text' }],
  promotionAlternateDescription: [{ code: 'code', text: 'text' }],
  promotionDescriptionDisplayOptions: [{ code: 'code', text: 'text' }],
  promotionStatementTextIDDTDD: [{ code: 'code', text: 'text' }],
  promotionStatementTextIDDTDX: [{ code: 'code', text: 'text' }],
  promotionStatementTextIDStandard: [{ code: 'code', text: 'text' }],
  promotionStatementTextControl: [{ code: 'code', text: 'text' }],
  promotionReturnApplicationOption: [{ code: 'code', text: 'text' }],
  promotionPayoffExceptionOption: [{ code: 'code', text: 'text' }],
  loanBalanceClassification: [{ code: 'code', text: 'text' }],
  payoffExceptionMethod: [{ code: 'code', text: 'text' }],
  cashAdvanceItemFeesMethod: [{ code: 'code', text: 'text' }],
  merchantItemFeesMethod: [{ code: 'code', text: 'text' }],
  creditApplicationGroup: [{ code: 'code', text: 'text' }],
  promotionGroupIdentifier: [{ code: 'code', text: 'text' }],
  standardMinimumPaymentCalculationCode: [{ code: 'code', text: 'text' }],
  rulesMinimumPaymentDueMethod: [{ code: 'code', text: 'text' }],
  standardMinimumPaymentsRate: [{ code: 'code', text: 'text' }],
  standardMinimumPaymentRoundingOptions: [{ code: 'code', text: 'text' }],
  annualInterestRate: [{ code: 'code', text: 'text' }],
  payoutPeriod: [{ code: 'code', text: 'text' }],
  merchantDiscount: [{ code: 'code', text: 'text' }],
  merchantDiscountCalculationCode: [{ code: 'code', text: 'text' }],
  returnToRevolvingBaseInterest: [{ code: 'code', text: 'text' }],
  applyPenaltyPricingOnLoanOffer: [{ code: 'code', text: 'text' }],
  defaultInterestRateMethodOverride: [{ code: 'code', text: 'text' }],
  interestCalculationMethodOverride: [{ code: 'code', text: 'text' }],
  introductoryMessageDisplayControl: [{ code: 'code', text: 'text' }],
  positiveAmortizationUsageCode: [{ code: 'code', text: 'text' }],
  introductoryAnnualRate: [{ code: 'code', text: 'text' }],
  introductoryAnnualRateFixedEndDate: [{ code: 'code', text: 'text' }],
  introductoryRateNumberOfDays: [{ code: 'code', text: 'text' }],
  introductoryRateNumberOfCycles: [{ code: 'code', text: 'text' }],
  introductoryPeriodStatementTextID: [{ code: 'code', text: 'text' }],
  sameAsCashLoanOfferNumberOfCycles: [{ code: 'code', text: 'text' }],
  accrueInterestOnUnbilledInterest: [{ code: 'code', text: 'text' }],
  delayPaymentForStatementCycles: [{ code: 'code', text: 'text' }],
  delayPaymentForMonths: [{ code: 'code', text: 'text' }]
};
const _props = { metadata, onChange: jest.fn(), searchValue: '' };

describe('pages > WorkflowDesignPromotions > DesignPromotionsStep > SubGridNewMethod', () => {
  it('Info_LoanOfferBasicSettings', async () => {
    const props = {
      ..._props,
      original: { id: 'Info_LoanOfferBasicSettings' }
    };
    const wrapper = await renderWithMockStore(
      <SubGridNewMethod {...props} />,
      {}
    );

    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
    expect(wrapper.getByText('txt_value')).toBeInTheDocument();
    expect(wrapper.getByText('txt_more_info')).toBeInTheDocument();
  });

  it('Info_PaymentAndPricingSettings', async () => {
    const props = {
      ..._props,
      original: { id: 'Info_PaymentAndPricingSettings' }
    };
    const wrapper = await renderWithMockStore(
      <SubGridNewMethod {...props} />,
      {}
    );

    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
    expect(wrapper.getByText('txt_value')).toBeInTheDocument();
    expect(wrapper.getByText('txt_more_info')).toBeInTheDocument();
  });

  it('Info_MerchantDiscount', async () => {
    const props = { ..._props, original: { id: 'Info_MerchantDiscount' } };
    const wrapper = await renderWithMockStore(
      <SubGridNewMethod {...props} />,
      {}
    );

    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
    expect(wrapper.getByText('txt_value')).toBeInTheDocument();
    expect(wrapper.getByText('txt_more_info')).toBeInTheDocument();
  });

  it('Adv_MiscellaneousControls', async () => {
    const props = {
      ..._props,
      original: { id: 'Adv_MiscellaneousControls' }
    };
    const wrapper = await renderWithMockStore(
      <SubGridNewMethod {...props} />,
      {}
    );

    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
    expect(wrapper.getByText('txt_value')).toBeInTheDocument();
    expect(wrapper.getByText('txt_more_info')).toBeInTheDocument();
  });

  it('Adv_IntroductoryAprSettings', async () => {
    const props = {
      ..._props,
      original: { id: 'Adv_IntroductoryAprSettings' }
    };
    const wrapper = await renderWithMockStore(
      <SubGridNewMethod {...props} />,
      {}
    );

    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
    expect(wrapper.getByText('txt_value')).toBeInTheDocument();
    expect(wrapper.getByText('txt_more_info')).toBeInTheDocument();
  });

  it('Adv_PromotionalCashSettings', async () => {
    const props = {
      ..._props,
      original: { id: 'Adv_PromotionalCashSettings' }
    };
    const wrapper = await renderWithMockStore(
      <SubGridNewMethod {...props} />,
      {}
    );

    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
    expect(wrapper.getByText('txt_value')).toBeInTheDocument();
    expect(wrapper.getByText('txt_more_info')).toBeInTheDocument();
  });

  it('Adv_PaymentDelaySettings', async () => {
    const props = { ..._props, original: { id: 'Adv_PaymentDelaySettings' } };
    const wrapper = await renderWithMockStore(
      <SubGridNewMethod {...props} />,
      {}
    );

    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
    expect(wrapper.getByText('txt_value')).toBeInTheDocument();
    expect(wrapper.getByText('txt_more_info')).toBeInTheDocument();
  });
});
