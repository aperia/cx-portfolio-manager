import { ServiceSubjectSection } from 'app/constants/enums';
import mockDate from 'app/_libraries/_dls/test-utils/mockDate';
import parseFormValues from './_parseFormValues';

const todayString = '2022-02-23';
const today = new Date(todayString);

describe('pages > WorkflowDesignLoanOffers > DesignLoanOffersStep > parseFormValues', () => {
  beforeEach(() => {
    mockDate.set(today);
  });

  it('parseFormValues', () => {
    let configurations: any = {};
    const id = 'id';
    const input: any = {
      data: {
        configurations,
        workflowSetupData: [{ id, status: 'INPROGRESS' }]
      }
    };

    // isvalid
    let result = parseFormValues(input, id, jest.fn);
    expect(result?.isValid).toBeFalsy();

    // other step
    result = parseFormValues(input, 'id1', jest.fn);
    expect(result).toBeUndefined();

    configurations = {
      methodList: [
        {
          serviceSubjectSection: ServiceSubjectSection.DLO,
          name: 'name'
        }
      ]
    };
    input.data.configurations = configurations;

    result = parseFormValues(input, id, jest.fn);
    expect(result?.isValid).toBeTruthy();
    expect(result?.methods?.[0]?.methodType).toEqual('NEWMETHOD');
  });
});
