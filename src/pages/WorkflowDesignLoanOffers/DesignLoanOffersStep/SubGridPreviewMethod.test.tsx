import { render } from '@testing-library/react';
import React from 'react';
import SubGridPreviewMethod from './SubGridPreviewMethod';

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

const metadata = {
  promotionDescription: 'promotionDescription',
  promotionAlternateDescription: 'promotionAlternateDescription',
  promotionDescriptionDisplayOptions: 'promotionDescriptionDisplayOptions',
  promotionStatementTextIDDTDD: 'promotionStatementTextIDDTDD',
  promotionStatementTextIDDTDX: 'promotionStatementTextIDDTDX',
  promotionStatementTextIDStandard: 'promotionStatementTextIDStandard',
  promotionStatementTextControl: 'promotionStatementTextControl',
  promotionReturnApplicationOption: 'promotionReturnApplicationOption',
  promotionPayoffExceptionOption: 'promotionPayoffExceptionOption',
  payoffExceptionMethod: 'payoffExceptionMethod',
  cashAdvanceItemFeesMethod: 'cashAdvanceItemFeesMethod',
  merchantItemFeesMethod: 'merchantItemFeesMethod',
  creditApplicationGroup: 'creditApplicationGroup',
  promotionGroupIdentifier: 'promotionGroupIdentifier',
  loanBalanceClassification: 'loanBalanceClassification',
  standardMinimumPaymentCalculationCode:
    'standardMinimumPaymentCalculationCode',
  rulesMinimumPaymentDueMethod: 'rulesMinimumPaymentDueMethod',
  standardMinimumPaymentsRate: 'standardMinimumPaymentsRate',
  standardMinimumPaymentRoundingOptions:
    'standardMinimumPaymentRoundingOptions',
  annualInterestRate: 'annualInterestRate',
  payoutPeriod: 'payoutPeriod',
  merchantDiscount: 'merchantDiscount',
  merchantDiscountCalculationCode: 'merchantDiscountCalculationCode',
  returnToRevolvingBaseInterest: 'returnToRevolvingBaseInterest',
  applyPenaltyPricingOnLoanOffer: 'applyPenaltyPricingOnLoanOffer',
  defaultInterestRateMethodOverride: 'defaultInterestRateMethodOverride',
  interestCalculationMethodOverride: 'interestCalculationMethodOverride',
  introductoryMessageDisplayControl: 'introductoryMessageDisplayControl',
  positiveAmortizationUsageCode: 'positiveAmortizationUsageCode',
  introductoryAnnualRate: 'introductoryAnnualRate',
  introductoryAnnualRateFixedEndDate: '11/11/2021',
  introductoryRateNumberOfDays: 'introductoryRateNumberOfDays',
  introductoryRateNumberOfCycles: 'introductoryRateNumberOfCycles',
  introductoryPeriodStatementTextID: 'introductoryPeriodStatementTextID',
  sameAsCashLoanOfferNumberOfCycles: 'sameAsCashLoanOfferNumberOfCycles',
  accrueInterestOnUnbilledInterest: 'accrueInterestOnUnbilledInterest',
  delayPaymentForStatementCycles: 'delayPaymentForStatementCycles',
  delayPaymentForMonths: 'delayPaymentForMonths'
};

describe('pages > WorkflowDesignPromotions > DesignPromotionsStep > SubGridPreviewMethod', () => {
  it('Info_LoanOfferBasicSettings', () => {
    const original = { id: 'Info_LoanOfferBasicSettings' };
    const props = { metadata, original };
    const wrapper = render(<SubGridPreviewMethod {...props} />);

    expect(wrapper.getByText('promotionDescription')).toBeInTheDocument();
    expect(
      wrapper.getByText('promotionAlternateDescription')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('promotionDescriptionDisplayOptions')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('promotionStatementTextIDDTDD')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('promotionStatementTextIDDTDX')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('promotionStatementTextIDStandard')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('promotionStatementTextControl')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('promotionReturnApplicationOption')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('promotionPayoffExceptionOption')
    ).toBeInTheDocument();
    expect(wrapper.getByText('payoffExceptionMethod')).toBeInTheDocument();
    expect(wrapper.getByText('cashAdvanceItemFeesMethod')).toBeInTheDocument();
    expect(wrapper.getByText('merchantItemFeesMethod')).toBeInTheDocument();
    expect(wrapper.getByText('creditApplicationGroup')).toBeInTheDocument();
    expect(wrapper.getByText('promotionGroupIdentifier')).toBeInTheDocument();
    expect(wrapper.getByText('loanBalanceClassification')).toBeInTheDocument();
  });

  it('Info_PaymentAndPricingSettings', () => {
    const original = { id: 'Info_PaymentAndPricingSettings' };
    const props = { metadata, original };
    const wrapper = render(<SubGridPreviewMethod {...props} />);

    expect(
      wrapper.getByText('standardMinimumPaymentCalculationCode')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('rulesMinimumPaymentDueMethod')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('standardMinimumPaymentsRate%')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('standardMinimumPaymentRoundingOptions')
    ).toBeInTheDocument();
    expect(wrapper.getByText('annualInterestRate%')).toBeInTheDocument();
    expect(wrapper.getByText('payoutPeriod')).toBeInTheDocument();
  });

  it('Info_MerchantDiscount', () => {
    const original = { id: 'Info_MerchantDiscount' };
    const props = { metadata, original };
    const wrapper = render(<SubGridPreviewMethod {...props} />);

    expect(wrapper.getByText('merchantDiscount%')).toBeInTheDocument();
    expect(
      wrapper.getByText('merchantDiscountCalculationCode')
    ).toBeInTheDocument();
  });

  it('Adv_IntroductoryAprSettings with empty value', () => {
    const original = { id: 'Adv_IntroductoryAprSettings' };
    const metadata = {
      promotionDescription: 'promotionDescription',
      promotionAlternateDescription: 'promotionAlternateDescription',
      promotionDescriptionDisplayOptions: 'promotionDescriptionDisplayOptions',
      promotionStatementTextIDDTDD: 'promotionStatementTextIDDTDD',
      promotionStatementTextIDDTDX: 'promotionStatementTextIDDTDX',
      promotionStatementTextIDStandard: 'promotionStatementTextIDStandard',
      promotionStatementTextControl: 'promotionStatementTextControl',
      promotionReturnApplicationOption: 'promotionReturnApplicationOption',
      promotionPayoffExceptionOption: 'promotionPayoffExceptionOption',
      payoffExceptionMethod: 'payoffExceptionMethod',
      cashAdvanceItemFeesMethod: 'cashAdvanceItemFeesMethod',
      merchantItemFeesMethod: 'merchantItemFeesMethod',
      creditApplicationGroup: 'creditApplicationGroup',
      promotionGroupIdentifier: 'promotionGroupIdentifier',
      loanBalanceClassification: 'loanBalanceClassification',
      standardMinimumPaymentCalculationCode:
        'standardMinimumPaymentCalculationCode',
      rulesMinimumPaymentDueMethod: 'rulesMinimumPaymentDueMethod',
      standardMinimumPaymentsRate: 'standardMinimumPaymentsRate',
      standardMinimumPaymentRoundingOptions:
        'standardMinimumPaymentRoundingOptions',
      annualInterestRate: 'annualInterestRate',
      payoutPeriod: 'payoutPeriod',
      merchantDiscount: undefined,
      merchantDiscountCalculationCode: 'merchantDiscountCalculationCode',
      returnToRevolvingBaseInterest: 'returnToRevolvingBaseInterest',
      applyPenaltyPricingOnLoanOffer: 'applyPenaltyPricingOnLoanOffer',
      defaultInterestRateMethodOverride: 'defaultInterestRateMethodOverride',
      interestCalculationMethodOverride: 'interestCalculationMethodOverride',
      introductoryMessageDisplayControl: 'introductoryMessageDisplayControl',
      positiveAmortizationUsageCode: 'positiveAmortizationUsageCode',
      introductoryAnnualRate: undefined,
      introductoryAnnualRateFixedEndDate: false,
      introductoryRateNumberOfDays: 'introductoryRateNumberOfDays',
      introductoryRateNumberOfCycles: 'introductoryRateNumberOfCycles',
      introductoryPeriodStatementTextID: 'introductoryPeriodStatementTextID',
      sameAsCashLoanOfferNumberOfCycles: 'sameAsCashLoanOfferNumberOfCycles',
      accrueInterestOnUnbilledInterest: 'accrueInterestOnUnbilledInterest',
      delayPaymentForStatementCycles: 'delayPaymentForStatementCycles',
      delayPaymentForMonths: 'delayPaymentForMonths'
    };

    const props = { metadata, original };
    render(<SubGridPreviewMethod {...props} />);
  });

  it('Info_MerchantDiscount with empty value', () => {
    const original = { id: 'Info_MerchantDiscount' };

    const metadata = {
      promotionDescription: 'promotionDescription',
      promotionAlternateDescription: 'promotionAlternateDescription',
      promotionDescriptionDisplayOptions: 'promotionDescriptionDisplayOptions',
      promotionStatementTextIDDTDD: 'promotionStatementTextIDDTDD',
      promotionStatementTextIDDTDX: 'promotionStatementTextIDDTDX',
      promotionStatementTextIDStandard: 'promotionStatementTextIDStandard',
      promotionStatementTextControl: 'promotionStatementTextControl',
      promotionReturnApplicationOption: 'promotionReturnApplicationOption',
      promotionPayoffExceptionOption: 'promotionPayoffExceptionOption',
      payoffExceptionMethod: 'payoffExceptionMethod',
      cashAdvanceItemFeesMethod: 'cashAdvanceItemFeesMethod',
      merchantItemFeesMethod: 'merchantItemFeesMethod',
      creditApplicationGroup: 'creditApplicationGroup',
      promotionGroupIdentifier: 'promotionGroupIdentifier',
      loanBalanceClassification: 'loanBalanceClassification',
      standardMinimumPaymentCalculationCode:
        'standardMinimumPaymentCalculationCode',
      rulesMinimumPaymentDueMethod: 'rulesMinimumPaymentDueMethod',
      standardMinimumPaymentsRate: 'standardMinimumPaymentsRate',
      standardMinimumPaymentRoundingOptions:
        'standardMinimumPaymentRoundingOptions',
      annualInterestRate: 'annualInterestRate',
      payoutPeriod: 'payoutPeriod',
      merchantDiscount: undefined,
      merchantDiscountCalculationCode: 'merchantDiscountCalculationCode',
      returnToRevolvingBaseInterest: 'returnToRevolvingBaseInterest',
      applyPenaltyPricingOnLoanOffer: 'applyPenaltyPricingOnLoanOffer',
      defaultInterestRateMethodOverride: 'defaultInterestRateMethodOverride',
      interestCalculationMethodOverride: 'interestCalculationMethodOverride',
      introductoryMessageDisplayControl: 'introductoryMessageDisplayControl',
      positiveAmortizationUsageCode: 'positiveAmortizationUsageCode',
      introductoryAnnualRate: undefined,
      introductoryAnnualRateFixedEndDate: false,
      introductoryRateNumberOfDays: 'introductoryRateNumberOfDays',
      introductoryRateNumberOfCycles: 'introductoryRateNumberOfCycles',
      introductoryPeriodStatementTextID: 'introductoryPeriodStatementTextID',
      sameAsCashLoanOfferNumberOfCycles: 'sameAsCashLoanOfferNumberOfCycles',
      accrueInterestOnUnbilledInterest: 'accrueInterestOnUnbilledInterest',
      delayPaymentForStatementCycles: 'delayPaymentForStatementCycles',
      delayPaymentForMonths: 'delayPaymentForMonths'
    };
    const props = { metadata, original };

    render(<SubGridPreviewMethod {...props} />);
  });

  it('Adv_MiscellaneousControls', () => {
    const original = { id: 'Adv_MiscellaneousControls' };
    const props = { metadata, original };
    const wrapper = render(<SubGridPreviewMethod {...props} />);

    expect(
      wrapper.getByText('returnToRevolvingBaseInterest')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('applyPenaltyPricingOnLoanOffer')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('defaultInterestRateMethodOverride')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('interestCalculationMethodOverride')
    ).toBeInTheDocument();
  });

  it('Adv_IntroductoryAprSettings', () => {
    const original = { id: 'Adv_IntroductoryAprSettings' };
    const props = { metadata, original };
    const wrapper = render(<SubGridPreviewMethod {...props} />);

    expect(wrapper.getAllByText('11/11/2021')[0]).toBeInTheDocument();
    expect(
      wrapper.getByText('introductoryMessageDisplayControl')
    ).toBeInTheDocument();
  });

  it('Adv_PromotionalCashSettings', () => {
    const original = { id: 'Adv_PromotionalCashSettings' };
    const props = { metadata, original };
    const wrapper = render(<SubGridPreviewMethod {...props} />);

    expect(
      wrapper.getByText('sameAsCashLoanOfferNumberOfCycles')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('accrueInterestOnUnbilledInterest')
    ).toBeInTheDocument();
  });

  it('Adv_PaymentDelaySettings', () => {
    const original = { id: 'Adv_PaymentDelaySettings' };
    const props = { metadata, original };
    const wrapper = render(<SubGridPreviewMethod {...props} />);

    expect(
      wrapper.getByText('delayPaymentForStatementCycles')
    ).toBeInTheDocument();
    expect(wrapper.getByText('delayPaymentForMonths')).toBeInTheDocument();
  });
});
