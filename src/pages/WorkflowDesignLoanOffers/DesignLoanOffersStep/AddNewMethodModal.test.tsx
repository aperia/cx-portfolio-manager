import userEvent from '@testing-library/user-event';
import { WorkflowMetadataList } from 'app/fixtures/workflow-metadata';
import { renderWithMockStore } from 'app/utils';
import 'app/utils/_mockComponent/mockModalRegistry';
import 'app/utils/_mockComponent/mockUseTranslation';
import mockDevice from 'app/utils/_mockHelperConstant/mockDevice';
import React from 'react';
import AddNewMethodModal from './AddNewMethodModal';

const mockOnConfirmParams = jest.fn();
jest.mock('app/hooks/useUnsavedChangesRedirect', () => {
  return {
    __esModule: true,
    useUnsavedChangesRedirect:
      () =>
      ({ onConfirm, onDiscard, onCancel }: any) => {
        onDiscard && onDiscard();
        onCancel && onCancel();
        onConfirm && onConfirm(mockOnConfirmParams());
      }
  };
});

describe('pages > WorkflowDesignLoanOffers > DesignLoanOffersStep > AddNewMethodModal', () => {
  const onClose = jest.fn();
  const defaultProps = {
    id: 'AddNewMethodModal',
    show: true,
    onClose
  };

  const method: WorkflowSetupMethod = {
    id: 'id',
    name: 'MTHMTCI',
    modeledFrom: {
      id: 'id1',
      name: 'MTHMTCI',
      versions: [{ id: 'id' }]
    },
    methodType: 'NEWVERSION',
    comment: '',
    parameters: [
      {
        name: 'late.charges.option',
        previousValue: '0',
        newValue: 'T'
      },
      {
        name: 'calculation.base',
        previousValue: '0',
        newValue: '0'
      },
      {
        name: 'minimum.pay.due.option',
        previousValue: '4',
        newValue: '3'
      },
      {
        name: 'fixed.amount',
        previousValue: '0',
        newValue: '3'
      },
      {
        name: 'percent',
        previousValue: '0',
        newValue: '8'
      },
      {
        name: 'maximum.amount',
        previousValue: '0',
        newValue: '1000'
      },
      {
        name: 'maximum.yearly.amount',
        previousValue: '0',
        newValue: '10'
      },
      {
        name: 'assessment.control',
        previousValue: '0',
        newValue: '8'
      },
      {
        name: 'cycles.of.consecutive.delinquency',
        previousValue: '4',
        newValue: '8'
      },
      {
        name: 'balance.indicator',
        previousValue: '0',
        newValue: '1'
      },
      {
        name: 'assessed.accounts',
        previousValue: '8',
        newValue: '8'
      },
      {
        name: 'current.balance.assessment',
        previousValue: '1',
        newValue: '1'
      },
      {
        name: 'calculation.day.control',
        previousValue: '0',
        newValue: '0'
      },
      {
        name: 'exclusion.balance',
        previousValue: '0',
        newValue: '1000'
      },
      {
        name: 'number.of.days',
        previousValue: '2',
        newValue: '3'
      },
      {
        name: 'non.processing.late.charge',
        previousValue: '1',
        newValue: '0'
      },
      {
        name: 'include.exclude.control',
        previousValue: 'E',
        newValue: 'I'
      },
      {
        name: 'status.1',
        previousValue: 'B',
        newValue: 'E'
      },
      {
        name: 'status.2',
        previousValue: 'U',
        newValue: 'I'
      },
      {
        name: 'status.3',
        previousValue: 'F',
        newValue: 'L'
      },
      {
        name: 'status.4',
        previousValue: 'F',
        newValue: 'Z'
      },
      {
        name: 'status.5',
        previousValue: 'B',
        newValue: 'F'
      }
    ]
  };

  const getInitialState = ({ elements = WorkflowMetadataList }: any = {}) => ({
    workflowSetup: { elementMetadata: { elements } }
  });
  const queryInputFromLabel = (tierLabel: HTMLElement) => {
    return tierLabel
      .closest(
        '.dls-input-container, .dls-numreric-container, .text-field-container'
      )
      ?.querySelector('input, .input, textarea') as HTMLInputElement;
  };

  beforeEach(() => {
    mockDevice.isDevice = false;
    mockOnConfirmParams.mockReturnValue('');
  });

  afterEach(() => {
    onClose.mockClear();
  });

  it('should render AddNewMethodModal', async () => {
    mockDevice.isDevice = true;
    const wrapper = await renderWithMockStore(
      <AddNewMethodModal {...defaultProps} />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByText('txt_design_loan_offers_method_details')
    ).toBeInTheDocument();

    const description = queryInputFromLabel(
      wrapper.getByText('txt_comment_area')
    );
    userEvent.type(description, new Array(239).fill(0).join(''));
    expect(description.textContent?.length).toEqual(239);

    userEvent.click(wrapper.getByText('txt_cancel'));
    expect(onClose).toBeCalled();
  });

  it('should render AddNewMethodModal form new version', async () => {
    const wrapper = await renderWithMockStore(
      <AddNewMethodModal {...defaultProps} method={method} />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByText('txt_design_loan_offers_method_details')
    ).toBeInTheDocument();

    userEvent.click(wrapper.getByText('txt_back'));
    expect(onClose).toBeCalled();
  });

  it('should render AddNewMethodModal form modeled method', async () => {
    const wrapper = await renderWithMockStore(
      <AddNewMethodModal
        {...defaultProps}
        methodNames={['MTHMTCI1']}
        draftMethod={{
          ...method,
          modeledFrom: { ...method.modeledFrom, name: '' },
          methodType: 'MODELEDMETHOD',
          parameters: undefined as any
        }}
        method={{
          ...method,
          modeledFrom: { ...method.modeledFrom, name: '' },
          methodType: 'MODELEDMETHOD',
          parameters: undefined as any
        }}
      />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByText('txt_design_loan_offers_method_details')
    ).toBeInTheDocument();

    const methodName = queryInputFromLabel(
      wrapper.getByText('txt_method_name')
    );
    userEvent.clear(methodName);
    userEvent.type(methodName, '@');
    userEvent.type(methodName, 'MTHMTCI1');
    expect(methodName?.value).toEqual('MTHMTCI1');
    userEvent.click(document.body);
    userEvent.click(methodName);

    userEvent.click(wrapper.getByText('txt_back'));
    expect(onClose).toBeCalled();
  });

  it('should save method form modeled method', async () => {
    const wrapper = await renderWithMockStore(
      <AddNewMethodModal
        methodNames={['MTHMTCI']}
        {...defaultProps}
        method={{ ...method, methodType: 'MODELEDMETHOD' }}
      />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByText('txt_design_loan_offers_method_details')
    ).toBeInTheDocument();

    const methodName = queryInputFromLabel(
      wrapper.getByText('txt_method_name')
    );
    userEvent.clear(methodName);
    userEvent.type(methodName, 'MTHMTCI1');
    expect(methodName.value).toEqual('MTHMTCI1');

    userEvent.click(wrapper.getByText('txt_save'));
    expect(onClose).toBeCalled();
  });

  it('should render no data filter', async () => {
    const wrapper = await renderWithMockStore(
      <AddNewMethodModal {...defaultProps} method={undefined} />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByText('txt_design_loan_offers_method_details')
    ).toBeInTheDocument();

    const searchBox = wrapper.getByPlaceholderText('txt_type_to_search');

    userEvent.type(searchBox, 'a');
    userEvent.click(
      searchBox.parentElement!.querySelector(
        '.icon-simple-search .icon.icon-search'
      )!
    );

    userEvent.type(searchBox, 'notfound');
    userEvent.click(
      searchBox.parentElement!.querySelector(
        '.icon-simple-search .icon.icon-search'
      )!
    );

    expect(
      wrapper.getByText('txt_no_results_found txt_adjust_your_search_criteria')
    ).toBeInTheDocument();

    userEvent.click(wrapper.getByText('txt_clear_and_reset'));
    expect(
      wrapper.queryByText(
        'txt_no_results_found txt_adjust_your_search_criteria'
      )
    ).not.toBeInTheDocument();
  });

  it('Stop Add new method when name duplicate', async () => {
    const wrapper = await renderWithMockStore(
      <AddNewMethodModal
        {...defaultProps}
        isCreate
        methodNames={['MTHMTCI']}
        draftMethod={{
          ...method,
          modeledFrom: { ...method.modeledFrom, name: 'MTHMTCI' },
          methodType: 'MODELEDMETHOD',
          parameters: undefined as any
        }}
        method={{
          ...method,
          modeledFrom: { ...method.modeledFrom, name: '' },
          methodType: 'MODELEDMETHOD',
          parameters: undefined as any
        }}
      />,
      { initialState: getInitialState() }
    );

    userEvent.click(wrapper.getByText('txt_save'));

    expect(onClose).not.toBeCalled();
  });
});
