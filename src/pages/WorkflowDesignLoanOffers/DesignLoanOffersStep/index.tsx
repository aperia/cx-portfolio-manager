import { InlineMessage, useTranslation } from 'app/_libraries/_dls';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React from 'react';
import ConfigureParameters from './ConfigureParameters';
import DesignLoanOffersSummary from './Summary';
import parseFormValues from './_parseFormValues';

export interface DesignLoanOffersFormValue {
  isValid?: boolean;
  methods?: any[];
}

const DesignLoanOffers: React.FC<
  WorkflowSetupProps<DesignLoanOffersFormValue>
> = props => {
  const { t } = useTranslation();
  const { isStuck } = props;

  return (
    <>
      {isStuck && (
        <InlineMessage className="mb-12 mt-24" variant="danger" withIcon>
          {t('txt_step_stuck_move_forward_message')}
        </InlineMessage>
      )}
      <p className="mt-12 color-grey">{t('txt_design_loan_offers_desc_1')}</p>
      <div className="pt-24 mt-24 border-top">
        <ConfigureParameters {...props} />
      </div>
    </>
  );
};

const ExtraStaticDesignLoanOffersStep =
  DesignLoanOffers as WorkflowSetupStaticProp;

ExtraStaticDesignLoanOffersStep.summaryComponent = DesignLoanOffersSummary;
ExtraStaticDesignLoanOffersStep.defaultValues = {
  isValid: false,
  methods: []
};

ExtraStaticDesignLoanOffersStep.parseFormValues = parseFormValues;

export default ExtraStaticDesignLoanOffersStep;
