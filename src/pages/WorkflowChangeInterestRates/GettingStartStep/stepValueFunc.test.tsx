import { FormGettingStart } from '.';
import stepValueFunc from './stepValueFunc';

describe('WorkflowChangeInterestRates > GettingStartStep > stepValueFunc', () => {
  it('Should interestDefault & interestMethod & inventivePricing', () => {
    const stepsForm = {
      interestDefault: true,
      interestMethod: true,
      inventivePricing: true
    } as FormGettingStart;

    const response = stepValueFunc({ stepsForm, t: jest.fn });
    expect(response).toEqual(
      'Interest Default (CP IC ID), Interest Methods (CP IC IM), Incentive Pricing (CP IC IP)'
    );
  });

  it('Should interestDefault & interestMethod', () => {
    const stepsForm = {
      interestDefault: true,
      interestMethod: true,
      inventivePricing: false
    } as FormGettingStart;

    const response = stepValueFunc({ stepsForm, t: jest.fn });
    expect(response).toEqual(
      'Interest Default (CP IC ID) and Interest Methods (CP IC IM)'
    );
  });

  it('Should interestDefault & inventivePricing', () => {
    const stepsForm = {
      interestDefault: true,
      interestMethod: false,
      inventivePricing: true
    } as FormGettingStart;

    const response = stepValueFunc({ stepsForm, t: jest.fn });
    expect(response).toEqual(
      'Interest Default (CP IC ID) and Incentive Pricing (CP IC IP)'
    );
  });

  it('Should interestMethod & inventivePricing', () => {
    const stepsForm = {
      interestDefault: false,
      interestMethod: true,
      inventivePricing: true
    } as FormGettingStart;

    const response = stepValueFunc({ stepsForm, t: jest.fn });
    expect(response).toEqual(
      'Interest Methods (CP IC IM) and Incentive Pricing (CP IC IP)'
    );
  });

  it('Should interestDefault', () => {
    const stepsForm = {
      interestDefault: true,
      interestMethod: false,
      inventivePricing: false
    } as FormGettingStart;

    const response = stepValueFunc({ stepsForm, t: jest.fn });
    expect(response).toEqual('Interest Default (CP IC ID)');
  });

  it('Should interestMethod', () => {
    const stepsForm = {
      interestDefault: false,
      interestMethod: true,
      inventivePricing: false
    } as FormGettingStart;

    const response = stepValueFunc({ stepsForm, t: jest.fn });
    expect(response).toEqual('Interest Methods (CP IC IM)');
  });

  it('Should inventivePricing', () => {
    const stepsForm = {
      interestDefault: false,
      interestMethod: false,
      inventivePricing: true
    } as FormGettingStart;

    const response = stepValueFunc({ stepsForm, t: jest.fn });
    expect(response).toEqual('Incentive Pricing (CP IC IP)');
  });

  it('Should default', () => {
    const stepsForm = {
      interestDefault: false,
      interestMethod: false,
      inventivePricing: false
    } as FormGettingStart;

    const response = stepValueFunc({ stepsForm, t: jest.fn });
    expect(response).toEqual('');
  });

  it('Should error', () => {
    const stepsForm = '' as FormGettingStart;

    const response = stepValueFunc({ stepsForm, t: jest.fn });
    expect(response).toEqual('');
  });
});
