import { fireEvent, render, RenderResult } from '@testing-library/react';
import React from 'react';
import GettingStartStep, { FormGettingStart } from './index';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});
jest.mock('app/hooks/useUnsavedChangeRegistry', () => {
  return {
    __esModule: true,
    useUnsavedChangeRegistry: (o: any, deps: any, onConfirm?: () => void) => {
      onConfirm && onConfirm();
    }
  };
});

jest.mock('pages/_commons/OverviewModal', () => ({
  __esModule: true,
  default: ({ onClose }: any) => {
    return (
      <div>
        <div>OverviewModal</div>
        <button onClick={onClose}>OverviewButtonCloseModal</button>
      </div>
    );
  }
}));

jest.mock('pages/_commons/ContentExpand', () => ({
  __esModule: true,
  default: ({ instruction, children }: any) => (
    <div>
      <div>ContentExpand</div>
      <div>{instruction}</div>
      <div>{children}</div>
    </div>
  )
}));

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <GettingStartStep {...props} />
    </div>
  );
};

describe('WorkflowChangeInterestRates > GettingStartStep > Index', () => {
  it('Should render without Overview modal > Open overview modal', () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: {
        inventivePricing: false
      },
      setFormValues: mockFn
    } as WorkflowSetupProps<FormGettingStart>;

    const wrapper = renderComponent(props);
    expect(wrapper.queryByText(/OverviewModal/)).not.toBeInTheDocument;
    wrapper.rerender(
      <div>
        <GettingStartStep
          {...props}
          stepId="1"
          formValues={{
            alreadyShown: true,
            inventivePricing: true
          }}
          hasInstance={true}
        />
      </div>
    );

    fireEvent.click(wrapper.getByText('txt_view_overview'));
    expect(wrapper.queryByText(/OverviewModal/)).toBeInTheDocument;
  });

  it('Should render Overview modal > Close overview modal', () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: {
        interestDefault: true,
        isValid: true
      },
      setFormValues: mockFn
    } as WorkflowSetupProps<FormGettingStart>;

    const wrapper = renderComponent(props);
    expect(wrapper.queryByText(/OverviewModal/)).toBeInTheDocument;

    fireEvent.click(wrapper.getByText('OverviewButtonCloseModal'));
    expect(wrapper.queryByText(/OverviewModal/)).not.toBeInTheDocument;
  });

  it('Select Change option', () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: {
        interestDefault: true,
        isValid: true
      },
      setFormValues: mockFn
    } as WorkflowSetupProps<FormGettingStart>;

    const wrapper = renderComponent(props);
    const checkbox = wrapper.baseElement.querySelector(
      'input[class="custom-control-input dls-checkbox-input"]'
    ) as Element;
    const _checkbox = checkbox as HTMLInputElement;
    expect(_checkbox.checked).toEqual(true);

    fireEvent.click(checkbox);
    expect(_checkbox.checked).toEqual(true);

    fireEvent.click(
      wrapper.getByText('txt_change_interest_rates_interest_default')
    );
  });

  it('should be render stuck message', () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: {
        isValid: true
      },
      isStuck: true,
      hasInstance: true,
      interestDefaultFormId: true,
      interestMethodFormId: true,
      inventivePricingFormId: true,
      setFormValues: mockFn,
      clearFormValues: mockFn
    } as unknown as WorkflowSetupProps<FormGettingStart>;

    const wrapper = renderComponent(props);

    expect(
      wrapper.getByText('txt_step_stuck_move_forward_message')
    ).toBeInTheDocument();
  });
});
