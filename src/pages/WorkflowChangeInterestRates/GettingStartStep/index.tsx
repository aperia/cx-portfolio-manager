import { WORKFLOW_SETUP } from 'app/constants/local-storage';
import {
  unsavedExistedWorkflowSetupDataOnStuckProps,
  unsavedExistedWorkflowSetupDataProps,
  unsavedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { useUnsavedChangeRegistry } from 'app/hooks';
import {
  Button,
  CheckBox,
  Icon,
  InlineMessage,
  TransDLS,
  useTranslation
} from 'app/_libraries/_dls';
import ContentExpand from 'pages/_commons/ContentExpand';
import OverviewModal from 'pages/_commons/OverviewModal';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import GettingStartSummary from './GettingStartSummary';
import parseFormValues from './parseFormValues';
import stepValueFunc from './stepValueFunc';

export interface FormGettingStart {
  isValid?: boolean;

  interestDefault?: boolean;
  interestMethod?: boolean;
  inventivePricing?: boolean;

  alreadyShown?: boolean;
}

export interface GettingStartProps
  extends WorkflowSetupProps<FormGettingStart> {
  interestDefaultFormId?: string;
  interestMethodFormId?: string;
  inventivePricingFormId?: string;
}
const GettingStartStep: React.FC<GettingStartProps> = ({
  title,
  stepId,
  selectedStep,
  savedAt,
  formValues,
  hasInstance,
  isStuck,

  interestDefaultFormId,
  interestMethodFormId,
  inventivePricingFormId,

  setFormValues,
  clearFormValues
}) => {
  const { t } = useTranslation();
  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;

  const [initialValues, setInitialValues] = useState(formValues);
  const [showOverview, setShowOverview] = useState(
    formValues.alreadyShown
      ? false
      : localStorage.getItem(WORKFLOW_SETUP.SHOW_OVERVIEW_AGAIN) !== 'false'
  );

  useEffect(() => {
    const isValid =
      !!formValues.interestDefault ||
      !!formValues.interestMethod ||
      !!formValues.inventivePricing;
    if (formValues.isValid === isValid) return;

    keepRef.current.setFormValues({ isValid });
  }, [formValues]);

  useEffect(() => {
    setInitialValues(formValues);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [savedAt]);

  useUnsavedChangeRegistry(
    {
      ...unsavedWorkflowSetupDataProps,
      formName: UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CHANGE_INTEREST_RATES,
      priority: 1
    },
    [
      !hasInstance &&
        (!!formValues.interestDefault ||
          !!formValues.interestMethod ||
          !!formValues.inventivePricing)
    ]
  );

  const handleConfirmChangeDependencies = useCallback(() => {
    interestDefaultFormId &&
      !formValues.interestDefault &&
      clearFormValues(interestDefaultFormId);

    interestMethodFormId &&
      !formValues.interestMethod &&
      clearFormValues(interestMethodFormId);

    inventivePricingFormId &&
      !formValues.inventivePricing &&
      clearFormValues(inventivePricingFormId);
  }, [
    clearFormValues,
    formValues.interestDefault,
    formValues.interestMethod,
    formValues.inventivePricing,
    interestDefaultFormId,
    interestMethodFormId,
    inventivePricingFormId
  ]);

  const isUncheckAll = useMemo(
    () =>
      !formValues.interestDefault &&
      !formValues.interestMethod &&
      !formValues.inventivePricing,
    [
      formValues.interestDefault,
      formValues.interestMethod,
      formValues.inventivePricing
    ]
  );
  const hasChanged = useMemo(
    () =>
      initialValues.interestDefault !== formValues.interestDefault ||
      initialValues.interestMethod !== formValues.interestMethod ||
      initialValues.inventivePricing !== formValues.inventivePricing,
    [initialValues, formValues]
  );

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CHANGE_INTEREST_RATES__GET_STARTED__EXISTED_WORKFLOW,
      priority: 1,
      confirmFirst: true
    },
    [!isUncheckAll && !!hasInstance && hasChanged],
    handleConfirmChangeDependencies
  );
  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataOnStuckProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CHANGE_INTEREST_RATES__GET_STARTED__EXISTED_WORKFLOW__STUCK,
      priority: 1,
      confirmFirst: true
    },
    [isUncheckAll && !!hasInstance && hasChanged]
  );

  const handleChangeOption = (option: keyof FormGettingStart) => {
    const onChange = (e: any) => {
      setFormValues({ [option]: !formValues[option] });
    };

    return onChange;
  };

  return (
    <React.Fragment>
      <ContentExpand
        isSelectedStep={stepId === selectedStep}
        change={savedAt}
        instruction={
          <div className="pt-24 px-24">
            <div className="d-flex align-items-center justify-content-between">
              <h4>{title}</h4>
              <Button
                className="mr-n8"
                variant="outline-primary"
                size="sm"
                onClick={() => setShowOverview(true)}
              >
                {t('txt_view_overview')}
              </Button>
            </div>
            <div className="pb-24">
              {isStuck && (
                <InlineMessage className="mb-0 mt-24" variant="danger" withIcon>
                  {t('txt_step_stuck_move_forward_message')}
                </InlineMessage>
              )}
              <div className="row">
                <div className="col-6 mt-24">
                  <div className="bg-light-l20 rounded-lg p-16">
                    <div className="text-center">
                      <Icon
                        name="request"
                        size="12x"
                        className="color-grey-l16"
                      />
                    </div>
                    <p className="mt-8 color-grey">
                      {t('txt_change_interest_rates_get_started_desc_1')}
                    </p>
                    <p className="mt-16 fw-600">
                      {t(
                        'txt_change_interest_rates_get_started_desc_highlight_1'
                      )}
                    </p>
                    <p className="mt-8 fw-600">
                      {t(
                        'txt_change_interest_rates_get_started_desc_highlight_2'
                      )}
                    </p>
                    <p className="mt-8 color-grey">
                      {t('txt_change_interest_rates_get_started_desc_2_1')}
                    </p>
                    <p className="mt-8 color-grey">
                      {t('txt_change_interest_rates_get_started_desc_2_2')}
                    </p>
                    <p className="mt-16 fw-600">
                      {t(
                        'txt_change_interest_rates_get_started_desc_highlight_3'
                      )}
                    </p>
                    <p className="mt-8 color-grey">
                      {t('txt_change_interest_rates_get_started_desc_3_1')}
                    </p>
                    <p className="mt-8 color-grey">
                      {t('txt_change_interest_rates_get_started_desc_3_2')}
                    </p>
                    <p className="mt-8 color-grey">
                      {t('txt_change_interest_rates_get_started_desc_3_3')}
                    </p>
                    <p className="mt-8 color-grey">
                      {t('txt_change_interest_rates_get_started_desc_3_4')}
                    </p>
                    <p className="mt-8 color-grey">
                      {t('txt_change_interest_rates_get_started_desc_3_5')}
                    </p>
                    <p className="mt-8 color-grey">
                      {t('txt_change_interest_rates_get_started_desc_3_6')}
                    </p>
                  </div>
                </div>
                <div className="col-6 mt-24 color-grey">
                  <p>
                    {t(
                      'txt_change_interest_rates_get_started_steps_guides_title'
                    )}
                  </p>
                  <p className="mt-8">
                    <TransDLS keyTranslation="txt_change_interest_rates_get_started_steps_guides_1">
                      <strong className="color-grey-d20" />
                    </TransDLS>
                  </p>
                  <p className="mt-8">
                    <TransDLS keyTranslation="txt_change_interest_rates_get_started_steps_guides_2">
                      <strong className="color-grey-d20" />
                    </TransDLS>
                  </p>
                  <p className="mt-8">
                    <TransDLS keyTranslation="txt_change_interest_rates_get_started_steps_guides_3">
                      <strong className="color-grey-d20" />
                    </TransDLS>
                  </p>
                  <p className="mt-8">
                    <TransDLS keyTranslation="txt_change_interest_rates_get_started_steps_guides_4">
                      <strong className="color-grey-d20" />
                    </TransDLS>
                  </p>
                  <p className="mt-8">
                    {t(
                      'txt_change_interest_rates_get_started_steps_guides_desc'
                    )}
                  </p>
                </div>
              </div>
            </div>
          </div>
        }
      >
        <div className="p-24">
          <h5>
            <TransDLS keyTranslation="txt_change_interest_rates_get_started_select_the_interest">
              <strong className="color-red" />
            </TransDLS>
          </h5>
          <div className="row mt-16 list-cards list-cards--selectable list-cards--single">
            <div className="col-6 col-lg-4">
              <label
                className="list-cards__item custom-control-root"
                htmlFor="interestDefault"
              >
                <span className="d-flex align-items-center">
                  <span className="pr-8">
                    {t('txt_change_interest_rates_interest_default')}
                  </span>
                </span>
                <CheckBox className="mr-n4">
                  <CheckBox.Input
                    id="interestDefault"
                    checked={formValues.interestDefault}
                    onChange={handleChangeOption('interestDefault')}
                  />
                </CheckBox>
              </label>
            </div>
            <div className="col-6 col-lg-4">
              <label
                className="list-cards__item custom-control-root"
                htmlFor="interestMethod"
              >
                <span className="d-flex align-items-center">
                  <span className="pr-8">
                    {t('txt_change_interest_rates_interest_method')}
                  </span>
                </span>
                <CheckBox className="mr-n4">
                  <CheckBox.Input
                    id="interestMethod"
                    checked={formValues.interestMethod}
                    onChange={handleChangeOption('interestMethod')}
                  />
                </CheckBox>
              </label>
            </div>
            <div className="col-6 col-lg-4">
              <label
                className="list-cards__item custom-control-root"
                htmlFor="inventivePricing"
              >
                <span className="d-flex align-items-center">
                  <span className="pr-8">
                    {t('txt_change_interest_rates_incentive_pricing')}
                  </span>
                </span>
                <CheckBox className="mr-n4">
                  <CheckBox.Input
                    id="inventivePricing"
                    checked={formValues.inventivePricing}
                    onChange={handleChangeOption('inventivePricing')}
                  />
                </CheckBox>
              </label>
            </div>
          </div>
        </div>
      </ContentExpand>
      {showOverview && (
        <OverviewModal
          id="overviewWorkflowSetup"
          show
          onClose={() => {
            setShowOverview(false);
            keepRef.current.setFormValues({ alreadyShown: true });
          }}
        />
      )}
    </React.Fragment>
  );
};

const ExtraStaticGettingStartStep =
  GettingStartStep as WorkflowSetupStaticProp<FormGettingStart>;

ExtraStaticGettingStartStep.stepValue = stepValueFunc;
ExtraStaticGettingStartStep.summaryComponent = GettingStartSummary;
ExtraStaticGettingStartStep.defaultValues = {
  isValid: false,
  interestDefault: false,
  interestMethod: false,
  inventivePricing: false
};
ExtraStaticGettingStartStep.parseFormValues = parseFormValues;

export default ExtraStaticGettingStartStep;
