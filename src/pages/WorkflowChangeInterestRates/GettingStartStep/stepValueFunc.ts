import { FormGettingStart } from '.';

const stepValueFunc: WorkflowSetupStepValueFunc<FormGettingStart> = ({
  stepsForm: formValues
}) => {
  const { interestDefault, interestMethod, inventivePricing } =
    formValues || {};

  if (interestDefault && interestMethod && inventivePricing) {
    return 'Interest Default (CP IC ID), Interest Methods (CP IC IM), Incentive Pricing (CP IC IP)';
  }

  if (interestMethod && inventivePricing) {
    return 'Interest Methods (CP IC IM) and Incentive Pricing (CP IC IP)';
  }

  if (interestDefault && interestMethod) {
    return 'Interest Default (CP IC ID) and Interest Methods (CP IC IM)';
  }

  if (interestDefault && inventivePricing) {
    return 'Interest Default (CP IC ID) and Incentive Pricing (CP IC IP)';
  }

  return interestDefault
    ? 'Interest Default (CP IC ID)'
    : interestMethod
    ? 'Interest Methods (CP IC IM)'
    : inventivePricing
    ? 'Incentive Pricing (CP IC IP)'
    : '';
};

export default stepValueFunc;
