import {
  MethodFieldNameEnum,
  MethodFieldParameterEnum
} from 'app/constants/enums';
import React from 'react';

export type ParameterListIds =
  | 'Info_BaseInterestDefaults'
  | 'Info_BaseInterestUsage'
  | 'Info_IncentivePricingUsage'
  | 'Info_IncentiveInterestCalculation'
  | 'Info_IncentiveNotifications'
  | 'Info_IncentivePricingTiming'
  | 'Info_InterestRateMinMax'
  | 'Info_MiscellaneousAdvancedParameters'
  | 'Info_InterestCalculation'
  | 'Info_CycleManagement'
  | 'Info_InterestMethods'
  | 'Info_PenaltyPricingParameters'
  | 'Info_MiscellaneousAdvancedParametersIM';

export interface ParameterList {
  id: ParameterListIds;
  section:
    | 'txt_change_interest_rates_standard_parameters'
    | 'txt_change_interest_rates_advanced_parameters';
  parameterGroup:
    | 'txt_change_interest_rates_group_base_interest_defaults'
    | 'txt_change_interest_rates_group_base_interest_usage'
    | 'txt_change_interest_rates_group_incentive_pricing_usage'
    | 'txt_change_interest_rates_group_incentive_interest_calculation'
    | 'txt_change_interest_rates_group_incentive_notifications'
    | 'txt_change_interest_rates_group_incentive_pricing_timing'
    | 'txt_change_interest_rates_group_interest_rate_min_max'
    | 'txt_change_interest_rates_group_interest_calculation'
    | 'txt_change_interest_rates_group_cycle_management'
    | 'txt_change_interest_rates_group_interest_method'
    | 'txt_change_interest_rates_group_penalty_pricing_parameters'
    | 'txt_change_interest_rates_group_miscellaneous_advanced_parameters'
    | 'txt_change_interest_rates_group_miscellaneous_advanced_parameters_im';
}

export const parameterList: (type?: string) => ParameterList[] = type => {
  switch (type) {
    case 'IM':
      return [
        {
          id: 'Info_InterestCalculation',
          section: 'txt_change_interest_rates_standard_parameters',
          parameterGroup: 'txt_change_interest_rates_group_interest_calculation'
        },
        {
          id: 'Info_CycleManagement',
          section: 'txt_change_interest_rates_standard_parameters',
          parameterGroup: 'txt_change_interest_rates_group_cycle_management'
        },
        {
          id: 'Info_InterestMethods',
          section: 'txt_change_interest_rates_standard_parameters',
          parameterGroup: 'txt_change_interest_rates_group_interest_method'
        },
        {
          id: 'Info_PenaltyPricingParameters',
          section: 'txt_change_interest_rates_advanced_parameters',
          parameterGroup:
            'txt_change_interest_rates_group_penalty_pricing_parameters'
        },
        {
          id: 'Info_MiscellaneousAdvancedParametersIM',
          section: 'txt_change_interest_rates_advanced_parameters',
          parameterGroup:
            'txt_change_interest_rates_group_miscellaneous_advanced_parameters_im'
        }
      ];
    case 'IP':
      return [
        {
          id: 'Info_IncentivePricingUsage',
          section: 'txt_change_interest_rates_standard_parameters',
          parameterGroup:
            'txt_change_interest_rates_group_incentive_pricing_usage'
        },
        {
          id: 'Info_IncentiveInterestCalculation',
          section: 'txt_change_interest_rates_standard_parameters',
          parameterGroup:
            'txt_change_interest_rates_group_incentive_interest_calculation'
        },
        {
          id: 'Info_IncentiveNotifications',
          section: 'txt_change_interest_rates_standard_parameters',
          parameterGroup:
            'txt_change_interest_rates_group_incentive_notifications'
        },
        {
          id: 'Info_IncentivePricingTiming',
          section: 'txt_change_interest_rates_standard_parameters',
          parameterGroup:
            'txt_change_interest_rates_group_incentive_pricing_timing'
        },
        {
          id: 'Info_InterestRateMinMax',
          section: 'txt_change_interest_rates_advanced_parameters',
          parameterGroup:
            'txt_change_interest_rates_group_interest_rate_min_max'
        },
        {
          id: 'Info_MiscellaneousAdvancedParameters',
          section: 'txt_change_interest_rates_advanced_parameters',
          parameterGroup:
            'txt_change_interest_rates_group_miscellaneous_advanced_parameters'
        }
      ];
    default:
      return [
        {
          id: 'Info_BaseInterestDefaults',
          section: 'txt_change_interest_rates_standard_parameters',
          parameterGroup:
            'txt_change_interest_rates_group_base_interest_defaults'
        },
        {
          id: 'Info_BaseInterestUsage',
          section: 'txt_change_interest_rates_standard_parameters',
          parameterGroup: 'txt_change_interest_rates_group_base_interest_usage'
        }
      ];
  }
};

export interface ParameterGroup {
  id: MethodFieldParameterEnum;
  fieldName: MethodFieldNameEnum;
  onlinePCF: string;
  moreInfoText: string;
  moreInfo: React.ReactNode;
}
export const parameterGroup: Record<ParameterListIds, ParameterGroup[]> = {
  Info_BaseInterestDefaults: [
    {
      id: MethodFieldParameterEnum.DefaultCashBaseInterest,
      fieldName: MethodFieldNameEnum.DefaultCashBaseInterest,
      onlinePCF: 'Cash Advance and Merchandise Base Interest',
      moreInfoText:
        'The Default Cash Base Interest parameters determine the interest rate values set on the cardholder account record. If you do not enter a merchandise or cash advance interest rate for a new account, this interest rate is automatically placed on file for the account. These rates may also be used to reset the cardholder account record during nightly processing. This function depends on the Override Interest Limit parameter in this section.',
      moreInfo: (
        <div>
          <p>
            The Default Cash Base Interest parameters determine the interest
            rate values set on the cardholder account record. If you do not
            enter a merchandise or cash advance interest rate for a new account,
            this interest rate is automatically placed on file for the account.
          </p>
          <p>
            These rates may also be used to reset the cardholder account record
            during nightly processing. This function depends on the Override
            Interest Limit parameter in this section.
          </p>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.DefaultMerchandiseBaseInterest,
      fieldName: MethodFieldNameEnum.DefaultMerchandiseBaseInterest,
      onlinePCF: 'Cash Advance and Merchandise Base Interest',
      moreInfoText:
        'The Default Merchandise Base Interest determine the interest rate values set on the cardholder account record. If you do not enter a merchandise or cash advance interest rate for a new account, this interest rate is automatically placed on file for the account. These rates may also be used to reset the cardholder account record during nightly processing. This function depends on the Override Interest Limit parameter in this section.',
      moreInfo: (
        <div>
          <p>
            The Default Merchandise Base Interest determine the interest rate
            values set on the cardholder account record. If you do not enter a
            merchandise or cash advance interest rate for a new account, this
            interest rate is automatically placed on file for the account.
          </p>
          <p>
            These rates may also be used to reset the cardholder account record
            during nightly processing. This function depends on the Override
            Interest Limit parameter in this section.
          </p>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.DefaultMaximumCashInterest,
      fieldName: MethodFieldNameEnum.DefaultMaximumCashInterest,
      onlinePCF: 'Maximum Interest Cash Advances and Merchandise',
      moreInfoText:
        'The Default Maximum Cash  Interest parameters determine the maximum annual interest rate the System sets on new cardholder account records for cash advances and merchandise.',
      moreInfo:
        'The Default Maximum Cash  Interest parameters determine the maximum annual interest rate the System sets on new cardholder account records for cash advances and merchandise.'
    },
    {
      id: MethodFieldParameterEnum.DefaultMaximumMerchandiseInterest,
      fieldName: MethodFieldNameEnum.DefaultMaximumMerchandiseInterest,
      onlinePCF: 'Maximum Interest Cash Advances and Merchandise',
      moreInfoText:
        'The Default Maximum Merchandise Interest  parameters determine the maximum annual interest rate the System sets on new cardholder account records for cash advances and merchandise.',
      moreInfo:
        'The Default Maximum Merchandise Interest  parameters determine the maximum annual interest rate the System sets on new cardholder account records for cash advances and merchandise.'
    },
    {
      id: MethodFieldParameterEnum.DefaultMinimumCashInterest,
      fieldName: MethodFieldNameEnum.DefaultMinimumCashInterest,
      onlinePCF: 'Minimum Interest Cash Advances and Merchandise',
      moreInfoText:
        'The Default Minimum Cash Interest parameters determine the minimum annual interest rate the System sets on new cardholder account records for cash advances and merchandise. The numeric values of these parameters cannot exceed the value of the Override Interest Limit parameter in this section. The value in these fields also cannot be greater than the value in the Maximum Interest Cash Advances and Maximum Interest Merchandise parameters in this section.',
      moreInfo: (
        <div>
          <p>
            The Default Minimum Cash Interest parameters determine the minimum
            annual interest rate the System sets on new cardholder account
            records for cash advances and merchandise.
          </p>
          <p>
            The numeric values of these parameters cannot exceed the value of
            the Override Interest Limit parameter in this section.
          </p>
          <p>
            The value in these fields also cannot be greater than the value in
            the Maximum Interest Cash Advances and Maximum Interest Merchandise
            parameters in this section.
          </p>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.DefaultMinimumMerchandiseInterest,
      fieldName: MethodFieldNameEnum.DefaultMinimumMerchandiseInterest,
      onlinePCF: 'Minimum Interest Cash Advances and Merchandise',
      moreInfoText:
        'The Default Minimum Merchandise Interest parameters determine the minimum annual interest rate the System sets on new cardholder account records for cash advances and merchandise. The numeric values of these parameters cannot exceed the value of the Override Interest Limit parameter in this section. The value in these fields also cannot be greater than the value in the Maximum Interest Cash Advances and Maximum Interest Merchandise parameters in this section.',
      moreInfo: (
        <div>
          <p>
            The Default Minimum Merchandise Interest parameters determine the
            minimum annual interest rate the System sets on new cardholder
            account records for cash advances and merchandise.
          </p>
          <p>
            The numeric values of these parameters cannot exceed the value of
            the Override Interest Limit parameter in this section.
          </p>
          <p>
            The value in these fields also cannot be greater than the value in
            the Maximum Interest Cash Advances and Maximum Interest Merchandise
            parameters in this section.
          </p>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.IndexRateMethod,
      fieldName: MethodFieldNameEnum.IndexRateMethod,
      onlinePCF: 'Index Rate Method',
      moreInfoText:
        'The Index Rate Method (CP IC IR) parameter contains the method name for the Index Rate section of the Product Control File. You must set the Base Interest Usage parameter in this section to 1 to use this parameter. In addition, the method name must have a scheduled or production status.',
      moreInfo: (
        <div>
          <p>
            The Index Rate Method (CP IC IR) parameter contains the method name
            for the Index Rate section of the Product Control File.
          </p>
          <p>
            You must set the Base Interest Usage parameter in this section to 1
            to use this parameter.
          </p>
          <p>
            In addition, the method name must have a scheduled or production
            status.
          </p>
        </div>
      )
    }
  ],
  Info_BaseInterestUsage: [
    {
      id: MethodFieldParameterEnum.DefaultBaseInterestUsage,
      fieldName: MethodFieldNameEnum.DefaultBaseInterestUsage,
      onlinePCF: 'Base Interest Usage',
      moreInfoText:
        'The Default Base Interest Usage parameter controls the other base interest parameters in this section.',
      moreInfo:
        'The Default Base Interest Usage parameter controls the other base interest parameters in this section.'
    },
    {
      id: MethodFieldParameterEnum.DefaultOverrideInterestLimit,
      fieldName: MethodFieldNameEnum.DefaultOverrideInterestLimit,
      onlinePCF: 'Override Interest Limit',
      moreInfoText:
        'The Default Override Interest Limit parameter determines the maximum interest rate for all other interest rate calculations (including the promotional maximum interest rate) in each strategy, except the state control and maximum interest rate tables.',
      moreInfo:
        'The Default Override Interest Limit parameter determines the maximum interest rate for all other interest rate calculations (including the promotional maximum interest rate) in each strategy, except the state control and maximum interest rate tables.'
    },
    {
      id: MethodFieldParameterEnum.DefaultMinimumUsage,
      fieldName: MethodFieldNameEnum.DefaultMinimumUsage,
      onlinePCF: 'Minimum Usage',
      moreInfoText:
        'The Default Minimum Usage parameter controls whether to change minimum annual interest rates when the interest rate for an existing standard or promotional balance is changed.',
      moreInfo:
        'The Default Minimum Usage parameter controls whether to change minimum annual interest rates when the interest rate for an existing standard or promotional balance is changed.'
    },
    {
      id: MethodFieldParameterEnum.DefaultMaximumUsage,
      fieldName: MethodFieldNameEnum.DefaultMaximumUsage,
      onlinePCF: 'Maximum Usage',
      moreInfoText:
        'The Default Maximum Usage parameter controls whether to change maximum annual interest rates when the interest rate for an existing standard or promotional balance is changed.',
      moreInfo:
        'The Default Maximum Usage parameter controls whether to change maximum annual interest rates when the interest rate for an existing standard or promotional balance is changed.'
    },
    {
      id: MethodFieldParameterEnum.DefaultMaximumInterestUsage,
      fieldName: MethodFieldNameEnum.DefaultMaximumInterestUsage,
      onlinePCF: 'Maximum Interest  Usage',
      moreInfoText:
        'The Default  Maximum Interest Usage parameter controls whether to charge additional interest to accounts that are one, two, or three cycles delinquent, either before or after the maximum interest rate has been applied.',
      moreInfo:
        'The Default  Maximum Interest Usage parameter controls whether to charge additional interest to accounts that are one, two, or three cycles delinquent, either before or after the maximum interest rate has been applied.'
    }
  ],
  Info_IncentivePricingUsage: [
    {
      id: MethodFieldParameterEnum.IncentivePricingUsageCode,
      fieldName: MethodFieldNameEnum.IncentivePricingUsageCode,
      onlinePCF: 'Incentive Pricing Usage',
      moreInfoText:
        'The Incentive Pricing Usage Code parameter specifies whether to apply incentive pricing parameters to new and/or existing accounts undergoing a change in strategy assignment.',
      moreInfo:
        'The Incentive Pricing Usage Code parameter specifies whether to apply incentive pricing parameters to new and/or existing accounts undergoing a change in strategy assignment.'
    },
    {
      id: MethodFieldParameterEnum.IncentiveBaseInterestUsageRate,
      fieldName: MethodFieldNameEnum.IncentiveBaseInterestUsageRate,
      onlinePCF: 'Base Interest Usage',
      moreInfoText:
        'The Incentive Base Interest Usage Rate parameter specifies when to use incentive pricing interest rates.',
      moreInfo:
        'The Incentive Base Interest Usage Rate parameter specifies when to use incentive pricing interest rates.'
    },
    {
      id: MethodFieldParameterEnum.DelinquencyCyclesForTermination,
      fieldName: MethodFieldNameEnum.DelinquencyCyclesForTermination,
      onlinePCF: 'Delinquency Cycles for Termination',
      moreInfoText:
        'The Delinquency Cycles for Termination parameter controls whether to automatically terminate the incentive period when an account becomes delinquent based on the number of cycles you specified. Incentive pricing rates apply through the end of the specified number of cycles. After termination, the standard rate applies for all subsequent cycles. Values 1-9 identify the specified number of cycles of delinquency the account must reach to automatically terminate the incentive period. Set this parameter to zero if you do not want to automatically terminate the incentive period.',
      moreInfo:
        'The Delinquency Cycles for Termination parameter controls whether to automatically terminate the incentive period when an account becomes delinquent based on the number of cycles you specified. Incentive pricing rates apply through the end of the specified number of cycles. After termination, the standard rate applies for all subsequent cycles. Values 1-9 identify the specified number of cycles of delinquency the account must reach to automatically terminate the incentive period. Set this parameter to zero if you do not want to automatically terminate the incentive period.'
    },
    {
      id: MethodFieldParameterEnum.MinimumMaximumRateOverrideOptions,
      fieldName: MethodFieldNameEnum.MinimumMaximumRateOverrideOptions,
      onlinePCF: 'Minimum/Maximum Rate Override',
      moreInfoText:
        'The Minimum/Maximum Rate Override Options parameter controls whether the System uses the minimum and maximum interest rates set on the cardholder account record or the minimum and maximum interest rates established within the Incentive Pricing method.',
      moreInfo:
        'The Minimum/Maximum Rate Override Options parameter controls whether the System uses the minimum and maximum interest rates set on the cardholder account record or the minimum and maximum interest rates established within the Incentive Pricing method.'
    },
    {
      id: MethodFieldParameterEnum.IncentivePricingOverrideOption,
      fieldName: MethodFieldNameEnum.IncentivePricingOverrideOption,
      onlinePCF: 'Incentive Pricing Override',
      moreInfoText:
        'The Incentive Pricing Override Option parameter specifies whether to override incentive pricing when cardholder accounts move into other strategies.',
      moreInfo:
        'The Incentive Pricing Override Option parameter specifies whether to override incentive pricing when cardholder accounts move into other strategies.'
    },
    {
      id: MethodFieldParameterEnum.TerminateIncentivePricingOnPricingStrategyChange,
      fieldName:
        MethodFieldNameEnum.TerminateIncentivePricingOnPricingStrategyChange,
      onlinePCF: 'Terminate Incentive Pricing',
      moreInfoText:
        'The Terminate Incentive Pricing on Pricing Strategy Change parameter controls whether during strategy changes the System retains the incentive pricing end date on a cardholder account or replaces the date with the last statement date. If the date on the account is retained, incentive pricing continues in the new strategy. This retention option depends on this parameter setting for the incentive pricing strategy into which the account is moving. The System checks this parameter each time an account with incentive pricing changes strategies.  Keep in mind that when an account changes to a strategy that has a setting of 2 in the Incentive Pricing Usage parameter in this section, the incentive pricing settings for that strategy apply to the account regardless of the value you set in the Terminate Incentive Pricing parameter. Additionally, the Terminate Incentive Pricing parameter has no effect on the Delinquency Cycles For Termination parameter in this section.',
      moreInfo:
        'The Terminate Incentive Pricing on Pricing Strategy Change parameter controls whether during strategy changes the System retains the incentive pricing end date on a cardholder account or replaces the date with the last statement date. If the date on the account is retained, incentive pricing continues in the new strategy. This retention option depends on this parameter setting for the incentive pricing strategy into which the account is moving. The System checks this parameter each time an account with incentive pricing changes strategies.  Keep in mind that when an account changes to a strategy that has a setting of 2 in the Incentive Pricing Usage parameter in this section, the incentive pricing settings for that strategy apply to the account regardless of the value you set in the Terminate Incentive Pricing parameter. Additionally, the Terminate Incentive Pricing parameter has no effect on the Delinquency Cycles For Termination parameter in this section.'
    },
    {
      id: MethodFieldParameterEnum.MethodOverrideTerminationOption,
      fieldName: MethodFieldNameEnum.MethodOverrideTerminationOption,
      onlinePCF: 'Method Override Terminate',
      moreInfoText:
        'The Method Override Termination Option parameter controls whether the System retains or terminates an incentive program when a CP IC IP method override is added, changed, or removed. The System checks this parameter each time a method override changes. To retain incentive pricing, set the parameter to 1. This retains the incentive pricing end date on the cardholder account record. To terminate incentive pricing, set the parameter to zero. This terminates incentive pricing by replacing the incentive pricing end date on the cardholder account record with the last statement date. Keep in mind that when the terminate incentive pricing field on the cardholder account record is set to a non-zero value, it will override the Method Override Terminate parameter. Additionally, when the Incentive Pricing Usage parameter is set to 2, the incentive pricing settings for the method or strategy apply to the account regardless of the value you set in the Method Override Terminate parameter.',
      moreInfo: (
        <div>
          <p>
            The Method Override Termination Option parameter controls whether
            the System retains or terminates an incentive program when a CP IC
            IP method override is added, changed, or removed. The System checks
            this parameter each time a method override changes. To retain
            incentive pricing, set the parameter to 1. This retains the
            incentive pricing end date on the cardholder account record. To
            terminate incentive pricing, set the parameter to zero. This
            terminates incentive pricing by replacing the incentive pricing end
            date on the cardholder account record with the last statement date.
          </p>
          <p>
            Keep in mind that when the terminate incentive pricing field on the
            cardholder account record is set to a non-zero value, it will
            override the Method Override Terminate parameter. Additionally, when
            the Incentive Pricing Usage parameter is set to 2, the incentive
            pricing settings for the method or strategy apply to the account
            regardless of the value you set in the Method Override Terminate
            parameter.
          </p>
        </div>
      )
    }
  ],
  Info_IncentiveInterestCalculation: [
    {
      id: MethodFieldParameterEnum.IncentiveCashInterestRate,
      fieldName: MethodFieldNameEnum.IncentiveCashInterestRate,
      onlinePCF: 'Cash Advance Rate',
      moreInfoText:
        'The Incentive Cash Interest Rate parameter specifies the cash advance interest rate for the incentive period. The value of this parameter cannot exceed the value of the Cash Advance Maximum Rate parameter in this section. If you set the Index Rate Method parameter in this section, you can set this parameter to a negative value.',
      moreInfo:
        'The Incentive Cash Interest Rate parameter specifies the cash advance interest rate for the incentive period. The value of this parameter cannot exceed the value of the Cash Advance Maximum Rate parameter in this section. If you set the Index Rate Method parameter in this section, you can set this parameter to a negative value.'
    },
    {
      id: MethodFieldParameterEnum.IncentiveMerchandiseInterestRate,
      fieldName: MethodFieldNameEnum.IncentiveMerchandiseInterestRate,
      onlinePCF: 'Merchandise Rate',
      moreInfoText:
        'The Incentive Merchandise Interest Rate parameter specifies the merchandise interest rate for the incentive period. The value of this parameter cannot exceed the value of the Merchandise Maximum Rate parameter in this section. If you set the Index Rate Method parameter in this section, you can set this parameter to a negative value.',
      moreInfo:
        'The Incentive Merchandise Interest Rate parameter specifies the merchandise interest rate for the incentive period. The value of this parameter cannot exceed the value of the Merchandise Maximum Rate parameter in this section. If you set the Index Rate Method parameter in this section, you can set this parameter to a negative value.'
    },
    {
      id: MethodFieldParameterEnum.IndexRateMethodName,
      fieldName: MethodFieldNameEnum.IndexRateMethodName,
      onlinePCF: 'Index Rate Method',
      moreInfoText:
        'This is the prime index rate Method (CP IC IR) that will get added to the cash and merchandise interest rate values set within this Method.',
      moreInfo:
        'This is the prime index rate Method (CP IC IR) that will get added to the cash and merchandise interest rate values set within this Method.'
    }
  ],
  Info_IncentiveNotifications: [
    {
      id: MethodFieldParameterEnum.EndofIncentiveWarningNotificationTiming,
      fieldName: MethodFieldNameEnum.EndofIncentiveWarningNotificationTiming,
      onlinePCF: 'Ending Incentive Notification Months',
      moreInfoText:
        'Choose how many months prior to the incentive pricing end-date that you would like to send a message to the cardholder warning them of the upcoming change.',
      moreInfo:
        'Choose how many months prior to the incentive pricing end-date that you would like to send a message to the cardholder warning them of the upcoming change.'
    },
    {
      id: MethodFieldParameterEnum.EndofIncentiveWarningNotificationStatementText,
      fieldName:
        MethodFieldNameEnum.EndofIncentiveWarningNotificationStatementText,
      onlinePCF: 'Revl Text ID',
      moreInfoText:
        'For the incentive pricing end-date, choose the Text ID for the statement message that will warn the customer that this special pricing is coming to an end.',
      moreInfo: (
        <div>
          <p>Text Area = MT, Text Type = CC</p>
          <br />
          <p>
            For the incentive pricing end-date, choose the Text ID for the
            statement message that will warn the customer that this special
            pricing is coming to an end.
          </p>
        </div>
      )
    }
  ],
  Info_IncentivePricingTiming: [
    {
      id: MethodFieldParameterEnum.IncentivePricingNumberOfMonths,
      fieldName: MethodFieldNameEnum.IncentivePricingNumberOfMonths,
      onlinePCF: 'Number of Months',
      moreInfoText:
        'The Incentive Pricing Number of Months parameter specifies the number of months the incentive pricing period remains in effect. The System edits this parameter for numeric values. Values 01-98 identify the number of months incentive pricing is to be in effect. Set this parameter to 99 if you do not want to specify a number of months.',
      moreInfo: (
        <div>
          <p>
            The Incentive Pricing Number of Months parameter specifies the
            number of months the incentive pricing period remains in effect.
          </p>
          <p>
            The System edits this parameter for numeric values. Values 01-98
            identify the number of months incentive pricing is to be in effect.
            Set this parameter to 99 if you do not want to specify a number of
            months.
          </p>
        </div>
      )
    }
  ],
  Info_InterestRateMinMax: [
    {
      id: MethodFieldParameterEnum.MinimumRateForCash,
      fieldName: MethodFieldNameEnum.MinimumRateForCash,
      onlinePCF: 'Cash Advance Minimum Rate',
      moreInfoText:
        'The Minimum Rate for Cash parameter determines the minimum annual interest rate the System sets on cardholder account records for incentive interest rates for cash advances. The value of this parameter must be less than or equal to the value of the Minimum Interest Cash Advances parameter in the Interest Defaults section (CP IC ID). It must also be less than or equal to the value of the Cash Advance Maximum Rate parameter in this section.',
      moreInfo:
        'The Minimum Rate for Cash parameter determines the minimum annual interest rate the System sets on cardholder account records for incentive interest rates for cash advances. The value of this parameter must be less than or equal to the value of the Minimum Interest Cash Advances parameter in the Interest Defaults section (CP IC ID). It must also be less than or equal to the value of the Cash Advance Maximum Rate parameter in this section.'
    },
    {
      id: MethodFieldParameterEnum.MaximumRateForCash,
      fieldName: MethodFieldNameEnum.MaximumRateForCash,
      onlinePCF: 'Cash Advance Maximum Rate',
      moreInfoText:
        'The Maximum Rate for Cash parameter determines the maximum annual interest rate the System sets on cardholder account records for incentive interest rates for cash advances. If you set the Minimum/Maximum Rate Override parameter in this section to 2, the rate you set in the Cash Advance Maximum Rate parameter may override your promotional rate settings in the Promotion Controls section (PL RT PC) of the Product Control File during the incentive pricing period. The value of this parameter must be less than or equal to the value of the Maximum Interest Cash Advances parameter in the Interest Defaults section (CP IC ID). It must also be greater than or equal to the value of the Cash Advance Minimum Rate parameter in this section.',
      moreInfo:
        'The Maximum Rate for Cash parameter determines the maximum annual interest rate the System sets on cardholder account records for incentive interest rates for cash advances. If you set the Minimum/Maximum Rate Override parameter in this section to 2, the rate you set in the Cash Advance Maximum Rate parameter may override your promotional rate settings in the Promotion Controls section (PL RT PC) of the Product Control File during the incentive pricing period. The value of this parameter must be less than or equal to the value of the Maximum Interest Cash Advances parameter in the Interest Defaults section (CP IC ID). It must also be greater than or equal to the value of the Cash Advance Minimum Rate parameter in this section.'
    },
    {
      id: MethodFieldParameterEnum.MinimumRateForMerchandise,
      fieldName: MethodFieldNameEnum.MinimumRateForMerchandise,
      onlinePCF: 'Merchandise Minimum Rate',
      moreInfoText:
        'The Minimum Rate for Merchandise parameter determines the minimum annual interest rate the System sets on cardholder account records for incentive interest rates for merchandise. The value of this parameter must be less than or equal to the value of the Maximum Interest Merchandise parameter in the Interest Defaults section (CP IC ID). It must also be less than or equal to the value of the Merchandise Maximum Rate parameter.',
      moreInfo:
        'The Minimum Rate for Merchandise parameter determines the minimum annual interest rate the System sets on cardholder account records for incentive interest rates for merchandise. The value of this parameter must be less than or equal to the value of the Maximum Interest Merchandise parameter in the Interest Defaults section (CP IC ID). It must also be less than or equal to the value of the Merchandise Maximum Rate parameter.'
    },
    {
      id: MethodFieldParameterEnum.MaximumRateForMerchandise,
      fieldName: MethodFieldNameEnum.MaximumRateForMerchandise,
      onlinePCF: 'Merchandise Maximum Rate',
      moreInfoText:
        'The Maximum Rate for Merchandise parameter determines the maximum annual interest rate the System sets on cardholder account records for incentive interest rates for merchandise. If you set the Minimum/Maximum Rate Override parameter in this section to 2, the rate you set in the Merchandise Maximum Rate parameter may override your promotional rate settings in the Promotion Controls section (PL RT PC) of the Product Control File during the incentive pricing period.',
      moreInfo:
        'The Maximum Rate for Merchandise parameter determines the maximum annual interest rate the System sets on cardholder account records for incentive interest rates for merchandise. If you set the Minimum/Maximum Rate Override parameter in this section to 2, the rate you set in the Merchandise Maximum Rate parameter may override your promotional rate settings in the Promotion Controls section (PL RT PC) of the Product Control File during the incentive pricing period.'
    }
  ],
  Info_MiscellaneousAdvancedParameters: [
    {
      id: MethodFieldParameterEnum.PreventTermsChangeDuringIncentivePricingPeriod,
      fieldName:
        MethodFieldNameEnum.PreventTermsChangeDuringIncentivePricingPeriod,
      onlinePCF: 'Terms Minimum Number of Months',
      moreInfoText:
        'The Prevent Terms Change During Incentive Pricing Period parameter establishes a period of time during which the System will prevent terms changes for all balances on an account that has entered an incentive pricing period. The System will use your setting in this parameter to calculate the date until which terms changes will be prevented. Set this parameter to a value of zero to 999. The System will add the number of months you specify to the incentive pricing start date to determine an end date. The System will prevent the termination of incentive pricing terms until the first cycle following the calculated end date.',
      moreInfo:
        'The Prevent Terms Change During Incentive Pricing Period parameter establishes a period of time during which the System will prevent terms changes for all balances on an account that has entered an incentive pricing period. The System will use your setting in this parameter to calculate the date until which terms changes will be prevented. Set this parameter to a value of zero to 999. The System will add the number of months you specify to the incentive pricing start date to determine an end date. The System will prevent the termination of incentive pricing terms until the first cycle following the calculated end date.'
    },
    {
      id: MethodFieldParameterEnum.MessageForProtectedBalanceIncentivePricing,
      fieldName: MethodFieldNameEnum.MessageForProtectedBalanceIncentivePricing,
      onlinePCF: 'PB Text ID',
      moreInfoText:
        'Text Area = MT, Text Type = PM. The Message for Protected Balance Incentive Pricing parameter identifies the message for ending incentive pricing periods for balances that have been protected and now exist as a protected balance. Use the PM, Promotional message text type in the MT, Messages text area to create these messages and assign text identifiers for use in this parameter.',
      moreInfo: (
        <div>
          <p>Text Area = MT, Text Type = PM</p>
          <br />
          <p>
            The Message for Protected Balance Incentive Pricing parameter
            identifies the message for ending incentive pricing periods for
            balances that have been protected and now exist as a protected
            balance. Use the PM, Promotional message text type in the MT,
            Messages text area to create these messages and assign text
            identifiers for use in this parameter.
          </p>
        </div>
      )
    }
  ],
  Info_InterestCalculation: [
    {
      id: MethodFieldParameterEnum.InterestRateRoundingCalculation,
      fieldName: MethodFieldNameEnum.InterestRateRoundingCalculation,
      onlinePCF: 'Interest Rounding',
      moreInfoText:
        'The Interest Rate Rounding Calculation parameter determines when cardholder interest is rounded off during calculation. It affects only those processors who calculate interest based on average daily balance. Interest is calculated separately on each cash advance/merchandise principal. These interest figures are then combined to create the interest amount to be billed. Interest is calculated to the hundredth of a cent. This flag controls when in the process these hundredths of a cent are truncated or rounded.',
      moreInfo:
        'The Interest Rate Rounding Calculation parameter determines when cardholder interest is rounded off during calculation. It affects only those processors who calculate interest based on average daily balance. Interest is calculated separately on each cash advance/merchandise principal. These interest figures are then combined to create the interest amount to be billed. Interest is calculated to the hundredth of a cent. This flag controls when in the process these hundredths of a cent are truncated or rounded.'
    },
    {
      id: MethodFieldParameterEnum.DailyAPRRoundingCalculation,
      fieldName: MethodFieldNameEnum.DailyAPRRoundingCalculation,
      onlinePCF: 'Daily Interest Rate Calculation Method',
      moreInfoText:
        'The Daily APR Rounding Calculation parameter determines how the daily interest rate is calculated from the annual interest percentage rate set on the cardholder account record.',
      moreInfo:
        'The Daily APR Rounding Calculation parameter determines how the daily interest rate is calculated from the annual interest percentage rate set on the cardholder account record.'
    },
    {
      id: MethodFieldParameterEnum.StopCalculatingInterestAfterNumberofDaysDelinquent,
      fieldName:
        MethodFieldNameEnum.StopCalculatingInterestAfterNumberofDaysDelinquent,
      onlinePCF: 'Stop Interest Delinquency Days',
      moreInfoText:
        'The Stop Calculating Interest After Number of Days Delinquent parameter specifies the number of days an account must be delinquent to stop interest accrual. When this day is reached, interest accrual stops for the following cycle. The system edits this parameter for numeric values divisible by 30.',
      moreInfo: (
        <div>
          <p>
            The Stop Calculating Interest After Number of Days Delinquent
            parameter specifies the number of days an account must be delinquent
            to stop interest accrual. When this day is reached, interest accrual
            stops for the following cycle.
          </p>
          <p>
            The system edits this parameter for numeric values divisible by 30.
          </p>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.IncludeAdditionalCreditBalanceInAverageDailyBalanceCalculation,
      fieldName:
        MethodFieldNameEnum.IncludeAdditionalCreditBalanceInAverageDailyBalanceCalculation,
      onlinePCF: 'Credit Balance Included In ADB',
      moreInfoText:
        'The Include Additional Credit Balance in Average Daily Balance Calculation parameter determines whether to treat credit balances as a credit or as zero when calculating the average daily balance. The Include Additional Credit Balance in Average Daily Balance Calculation parameter determines whether to treat credit balances as a credit or as zero when calculating the average daily balance: - Credit Balance Included In ADB parameter in this section to Y. - Credit Interest Method parameter in the MULTRAN Processing section (CP OC MP) to 02',
      moreInfo: (
        <div>
          <p>
            The Include Additional Credit Balance in Average Daily Balance
            Calculation parameter determines whether to treat credit balances as
            a credit or as zero when calculating the average daily balance
          </p>
          <p>
            The Include Additional Credit Balance in Average Daily Balance
            Calculation parameter determines whether to treat credit balances as
            a credit or as zero when calculating the average daily balance:
          </p>
          <ul>
            <li>
              Credit Balance Included In ADB parameter in this section to Y
            </li>
            <li>
              Credit Interest Method parameter in the MULTRAN Processing section
              (CP OC MP) to 02
            </li>
          </ul>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.CombineMerchandiseAndCashBalancesOnInterestCalculations,
      fieldName:
        MethodFieldNameEnum.CombineMerchandiseAndCashBalancesOnInterestCalculations,
      onlinePCF: 'Combine Aggregates',
      moreInfoText:
        'The Combine Merchandise and Cash Balances on Interest Calculations parameter determines whether to combine the following principal balances on cardholder accounts into one cash advance aggregate amount and one merchandise aggregate amount before calculating interest for cardholder statements. You cannot set this parameter to combine the principal balances unless the System calculates interest the same way for each principal. The interest calculations and payoff exceptions for all cash advance principals must be the same. The interest calculations and payoff exceptions for all merchandise principals also must be the same.',
      moreInfo: (
        <div>
          <p>
            The Combine Merchandise and Cash Balances on Interest Calculations
            parameter determines whether to combine the following principal
            balances on cardholder accounts into one cash advance aggregate
            amount and one merchandise aggregate amount before calculating
            interest for cardholder statements.
          </p>
          <p>
            You cannot set this parameter to combine the principal balances
            unless the System calculates interest the same way for each
            principal. The interest calculations and payoff exceptions for all
            cash advance principals must be the same. The interest calculations
            and payoff exceptions for all merchandise principals also must be
            the same.
          </p>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.RestartInterestCalculationsAfterClearingDelinquency,
      fieldName:
        MethodFieldNameEnum.RestartInterestCalculationsAfterClearingDelinquency,
      onlinePCF: 'Restart Interest',
      moreInfoText:
        'The Restart Interest Calculations After Clearing Delinquency parameter controls when you want to restart accrual of interest and the assessment of fees on delinquent accounts.',
      moreInfo:
        'The Restart Interest Calculations After Clearing Delinquency parameter controls when you want to restart accrual of interest and the assessment of fees on delinquent accounts.'
    },
    {
      id: MethodFieldParameterEnum.CalculateCompoundInterest,
      fieldName: MethodFieldNameEnum.CalculateCompoundInterest,
      onlinePCF: 'Compound Interest',
      moreInfoText:
        'The Calculate Compound Interest parameter determines whether the System calculates simple or compound interest. This option is for use only with interest methods 05 and 08.',
      moreInfo:
        'The Calculate Compound Interest parameter determines whether the System calculates simple or compound interest. This option is for use only with interest methods 05 and 08.'
    },
    {
      id: MethodFieldParameterEnum.OverridePromotionalRatesForPricingStrategies,
      fieldName:
        MethodFieldNameEnum.OverridePromotionalRatesForPricingStrategies,
      onlinePCF: 'Promotion Interest Override',
      moreInfoText:
        'Use the Override Promotional  Rates for Pricing Strategies parameter to override all plan or promotional interest rates at the pricing strategy level, or at the plan or promotional balance level.',
      moreInfo:
        'Use the Override Promotional  Rates for Pricing Strategies parameter to override all plan or promotional interest rates at the pricing strategy level, or at the plan or promotional balance level.'
    },
    {
      id: MethodFieldParameterEnum.MonthlyInterestCalculationOptions,
      fieldName: MethodFieldNameEnum.MonthlyInterestCalculationOptions,
      onlinePCF: 'Monthly Interest Rate Calc Method',
      moreInfoText:
        "The Monthly Interest Calculation Options parameter determines whether a cardholder's monthly percentage rate is rounded or truncated to five decimal places.",
      moreInfo:
        "The Monthly Interest Calculation Options parameter determines whether a cardholder's monthly percentage rate is rounded or truncated to five decimal places."
    },
    {
      id: MethodFieldParameterEnum.DailyInterestRoundingOption,
      fieldName: MethodFieldNameEnum.DailyInterestRoundingOption,
      onlinePCF: 'Daily Interest Amount Rounding',
      moreInfoText:
        'The Daily Interest Rounding Option parameter controls whether the System rounds the interest amount to five decimal positions when calculating daily compounded interest.',
      moreInfo:
        'The Daily Interest Rounding Option parameter controls whether the System rounds the interest amount to five decimal positions when calculating daily compounded interest.'
    },
    {
      id: MethodFieldParameterEnum.DailyInterestAmountTruncationOption,
      fieldName: MethodFieldNameEnum.DailyInterestAmountTruncationOption,
      onlinePCF: 'Daily Interest Amount Truncation',
      moreInfoText:
        'The Daily Interest Amount Truncation Option parameter controls the way the System truncates the daily interest amount. The setting in this parameter affects both revolving balances and cash and merchandise promotional balances. This option is for use only with interest methods 05, 08, or 11. The System checks your setting in this parameter to ensure the following parameters are set: - If you set the Daily Interest Amount Truncation parameter to 1, set the Daily Interest Amount Rounding parameter to 1. - If you set the Daily Interest Amount Truncation parameter to 1, set the Daily Interest Amount Rounding parameter to 1.',
      moreInfo: (
        <div>
          <p>
            The Daily Interest Amount Truncation Option parameter controls the
            way the System truncates the daily interest amount. The setting in
            this parameter affects both revolving balances and cash and
            merchandise promotional balances. This option is for use only with
            interest methods 05, 08, or 11. The System checks your setting in
            this parameter to ensure the following parameters are set:
          </p>
          <ul>
            <li>
              If you set the Daily Interest Amount Truncation parameter to 1,
              set the Daily Interest Amount Rounding parameter to 1.
            </li>
            <li>
              If you set the Daily Interest Amount Truncation parameter to 2,
              set the Daily Interest Amount Rounding parameter to zero.
            </li>
          </ul>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.OverrideInterestRatesForProtectedBalance,
      fieldName: MethodFieldNameEnum.OverrideInterestRatesForProtectedBalance,
      onlinePCF: 'Protected Balance Interest Override',
      moreInfoText:
        'The Override Interest Rates for Protected Balance parameter determines whether the System will override the strategy-level interest rate or the protected balance-level interest rate for every protected balance on an account.',
      moreInfo:
        'The Override Interest Rates for Protected Balance parameter determines whether the System will override the strategy-level interest rate or the protected balance-level interest rate for every protected balance on an account.'
    }
  ],
  Info_CycleManagement: [
    {
      id: MethodFieldParameterEnum.MinimumDaysForCycleCodeChange,
      fieldName: MethodFieldNameEnum.MinimumDaysForCycleCodeChange,
      onlinePCF: 'Minimum Days for Cycle Code Change',
      moreInfoText:
        'The Minimum Days for Cycle Code Change parameter determines the minimum number of days allowed between the last and next statement date if billing cycle code changes are attempted through the NM*22, Billing Cycle Code transaction. If a billing cycle code change would result in a shorter billing cycle than you specify in this parameter, the System will reject or suspend the transaction, based on your setting in the Cycle Code Change Option parameter.',
      moreInfo: (
        <div>
          <p>
            The Minimum Days for Cycle Code Change parameter determines the
            minimum number of days allowed between the last and next statement
            date if billing cycle code changes are attempted through the NM*22,
            Billing Cycle Code transaction.
          </p>
          <p>
            If a billing cycle code change would result in a shorter billing
            cycle than you specify in this parameter, the System will reject or
            suspend the transaction, based on your setting in the Cycle Code
            Change Option parameter.
          </p>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.RoundNumberOfDaysInCycleForInterestCalculations,
      fieldName:
        MethodFieldNameEnum.RoundNumberOfDaysInCycleForInterestCalculations,
      onlinePCF: 'Cycle Days Rounding',
      moreInfoText:
        'The Round Number of Days in Cycle for Interest Calculations parameter controls the number of days in the cycle when calculating interest.',
      moreInfo:
        'The Round Number of Days in Cycle for Interest Calculations parameter controls the number of days in the cycle when calculating interest.'
    },
    {
      id: MethodFieldParameterEnum.CycleCodeChangeOptions,
      fieldName: MethodFieldNameEnum.CycleCodeChangeOptions,
      onlinePCF: 'Cycle Code Change Option',
      moreInfoText:
        'The Cycle Code Change Option parameter indicates whether the System should reject a NM*22, Billing Cycle Code transaction, or suspend a cycle code change until after the original cycle is past if changing the cycle code would result in a shorter cycle than allowed by your setting in the Minimum Days For Cycle Code Change parameter in this section.',
      moreInfo:
        'The Cycle Code Change Option parameter indicates whether the System should reject a NM*22, Billing Cycle Code transaction, or suspend a cycle code change until after the original cycle is past if changing the cycle code would result in a shorter cycle than allowed by your setting in the Minimum Days For Cycle Code Change parameter in this section.'
    },
    {
      id: MethodFieldParameterEnum.CyclePeriodChangeCISMemoOption,
      fieldName: MethodFieldNameEnum.CyclePeriodChangeCISMemoOption,
      onlinePCF: 'Cycle Code CIS Memo Option',
      moreInfoText:
        'The Cycle Period Change CIS Memo Option parameter indicates whether the System creates a CIS memo when suspending a cycle code change.',
      moreInfo:
        'The Cycle Period Change CIS Memo Option parameter indicates whether the System creates a CIS memo when suspending a cycle code change.'
    }
  ],
  Info_InterestMethods: [
    {
      id: MethodFieldParameterEnum.InterestMethodOldCash,
      fieldName: MethodFieldNameEnum.InterestMethodOldCash,
      onlinePCF: 'Cash Advance and Merchandise Interest Methods Parameters',
      moreInfoText:
        'Use the following cash advance and merchandise interest methods parameters to set the interest method for each principal balance. Each parameter controls a different principal balance.',
      moreInfo: (
        <div>
          <p>
            Use the following cash advance and merchandise interest methods
            parameters to set the interest method for each principal balance.
          </p>
          <p>Each parameter controls a different principal balance.</p>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.InterestMethodCycleToDateCash,
      fieldName: MethodFieldNameEnum.InterestMethodCycleToDateCash,
      onlinePCF: 'Cash Advance and Merchandise Interest Methods Parameters',
      moreInfoText:
        'Use the following cash advance and merchandise interest methods parameters to set the interest method for each principal balance. Each parameter controls a different principal balance.',
      moreInfo: (
        <div>
          <p>
            Use the following cash advance and merchandise interest methods
            parameters to set the interest method for each principal balance.
          </p>
          <p>Each parameter controls a different principal balance.</p>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.InterestMethodTwoCycleOldCash,
      fieldName: MethodFieldNameEnum.InterestMethodTwoCycleOldCash,
      onlinePCF: 'Cash Advance and Merchandise Interest Methods Parameters',
      moreInfoText:
        'Use the following cash advance and merchandise interest methods parameters to set the interest method for each principal balance. Each parameter controls a different principal balance.',
      moreInfo: (
        <div>
          <p>
            Use the following cash advance and merchandise interest methods
            parameters to set the interest method for each principal balance.
          </p>
          <p>Each parameter controls a different principal balance.</p>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.InterestMethodOneCycleOldMerchandise,
      fieldName: MethodFieldNameEnum.InterestMethodOneCycleOldMerchandise,
      onlinePCF: 'Cash Advance and Merchandise Interest Methods Parameters',
      moreInfoText:
        'Use the following cash advance and merchandise interest methods parameters to set the interest method for each principal balance. Each parameter controls a different principal balance.',
      moreInfo: (
        <div>
          <p>
            Use the following cash advance and merchandise interest methods
            parameters to set the interest method for each principal balance.
          </p>
          <p>Each parameter controls a different principal balance.</p>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.InterestMethodCycleToDateMerchandise,
      fieldName: MethodFieldNameEnum.InterestMethodCycleToDateMerchandise,
      onlinePCF: 'Cash Advance and Merchandise Interest Methods Parameters',
      moreInfoText:
        'Use the following cash advance and merchandise interest methods parameters to set the interest method for each principal balance. Each parameter controls a different principal balance.',
      moreInfo: (
        <div>
          <p>
            Use the following cash advance and merchandise interest methods
            parameters to set the interest method for each principal balance.
          </p>
          <p>Each parameter controls a different principal balance.</p>
        </div>
      )
    }
  ],
  Info_PenaltyPricingParameters: [
    {
      id: MethodFieldParameterEnum.AdditionalInterestOnMerchandise1CyclesDelinquent,
      fieldName:
        MethodFieldNameEnum.AdditionalInterestOnMerchandise1CyclesDelinquent,
      onlinePCF: '1 Cycle Merchandise',
      moreInfoText:
        'This parameter applies additional interest on merchandise that is one cycle delinquent, as a method to apply penalty pricing.',
      moreInfo:
        'This parameter applies additional interest on merchandise that is one cycle delinquent, as a method to apply penalty pricing.'
    },
    {
      id: MethodFieldParameterEnum.AdditionalInterestOnMerchandise2CyclesDelinquent,
      fieldName:
        MethodFieldNameEnum.AdditionalInterestOnMerchandise2CyclesDelinquent,
      onlinePCF: '2 Cycle Merchandise',
      moreInfoText:
        'This parameter applies additional interest on merchandise that is two cycles delinquent, as a method to apply penalty pricing.',
      moreInfo:
        'This parameter applies additional interest on merchandise that is two cycles delinquent, as a method to apply penalty pricing.'
    },
    {
      id: MethodFieldParameterEnum.AdditionalInterestOnCash2CyclesDelinquent,
      fieldName: MethodFieldNameEnum.AdditionalInterestOnCash2CyclesDelinquent,
      onlinePCF: '2 Cycles Cash',
      moreInfoText:
        'This parameter applies additional interest on cash that is two cycles delinquent, as a method to apply penalty pricing.',
      moreInfo:
        'This parameter applies additional interest on cash that is two cycles delinquent, as a method to apply penalty pricing.'
    },
    {
      id: MethodFieldParameterEnum.AdditionalInterestOnMerchandise3CyclesDelinquent,
      fieldName:
        MethodFieldNameEnum.AdditionalInterestOnMerchandise3CyclesDelinquent,
      onlinePCF: '3+ Cycles Merchandise',
      moreInfoText:
        'This parameter applies additional interest on merchandise that is three cycles delinquent, as a method to apply penalty pricing.',
      moreInfo:
        'This parameter applies additional interest on merchandise that is three cycles delinquent, as a method to apply penalty pricing.'
    },
    {
      id: MethodFieldParameterEnum.AdditionalInterestOnCash3CyclesDelinquent,
      fieldName: MethodFieldNameEnum.AdditionalInterestOnCash3CyclesDelinquent,
      onlinePCF: '3+ Cash Cycles',
      moreInfoText:
        'This parameter applies additional interest on cash that is three cycles delinquent, as a method to apply penalty pricing.',
      moreInfo:
        'This parameter applies additional interest on cash that is three cycles delinquent, as a method to apply penalty pricing.'
    },
    {
      id: MethodFieldParameterEnum.AutomatedMemosForAdditionalInterest,
      fieldName: MethodFieldNameEnum.AutomatedMemosForAdditionalInterest,
      onlinePCF: 'Automatic CIS Memo',
      moreInfoText:
        'The Automated Memos for Additional Interest parameter controls whether the System creates a Customer Inquiry System (CIS) memo when the interest additive feature is used.',
      moreInfo:
        'The Automated Memos for Additional Interest parameter controls whether the System creates a Customer Inquiry System (CIS) memo when the interest additive feature is used.'
    },
    {
      id: MethodFieldParameterEnum.UseExternalStatusesForAdditionalInterest,
      fieldName: MethodFieldNameEnum.UseExternalStatusesForAdditionalInterest,
      onlinePCF: 'Include/Exclude Control',
      moreInfoText:
        'The Use External Statuses for Additional Interest parameter determines whether you include or exclude delinquent accounts with a specific external status from having additional interest added. You determine the external status to be included or excluded in the Status 1 through Status 5 parameters in this section.',
      moreInfo:
        'The Use External Statuses for Additional Interest parameter determines whether you include or exclude delinquent accounts with a specific external status from having additional interest added. You determine the external status to be included or excluded in the Status 1 through Status 5 parameters in this section.'
    },
    {
      id: MethodFieldParameterEnum.ExternalStatus1ForAdditionalInterest,
      fieldName: MethodFieldNameEnum.ExternalStatus1ForAdditionalInterest,
      onlinePCF: 'Status 1-5',
      moreInfoText:
        'The External Status 1 for Additional Interest through External Status 5 for Additional Interest parameters determine up to five external statuses to include or exclude from having additional interest added to delinquent accounts. You determine whether the accounts are included or excluded using the Include/Exclude Control parameter.',
      moreInfo:
        'The External Status 1 for Additional Interest through External Status 5 for Additional Interest parameters determine up to five external statuses to include or exclude from having additional interest added to delinquent accounts. You determine whether the accounts are included or excluded using the Include/Exclude Control parameter.'
    },
    {
      id: MethodFieldParameterEnum.ExternalStatus2ForAdditionalInterest,
      fieldName: MethodFieldNameEnum.ExternalStatus2ForAdditionalInterest,
      onlinePCF: 'Status 1-5',
      moreInfoText:
        'The External Status 1 for Additional Interest through External Status 5 for Additional Interest parameters determine up to five external statuses to include or exclude from having additional interest added to delinquent accounts. You determine whether the accounts are included or excluded using the Include/Exclude Control parameter.',
      moreInfo:
        'The External Status 1 for Additional Interest through External Status 5 for Additional Interest parameters determine up to five external statuses to include or exclude from having additional interest added to delinquent accounts. You determine whether the accounts are included or excluded using the Include/Exclude Control parameter.'
    },
    {
      id: MethodFieldParameterEnum.ExternalStatus3ForAdditionalInterest,
      fieldName: MethodFieldNameEnum.ExternalStatus3ForAdditionalInterest,
      onlinePCF: 'Status 1-5',
      moreInfoText:
        'The External Status 1 for Additional Interest through External Status 5 for Additional Interest parameters determine up to five external statuses to include or exclude from having additional interest added to delinquent accounts. You determine whether the accounts are included or excluded using the Include/Exclude Control parameter.',
      moreInfo:
        'The External Status 1 for Additional Interest through External Status 5 for Additional Interest parameters determine up to five external statuses to include or exclude from having additional interest added to delinquent accounts. You determine whether the accounts are included or excluded using the Include/Exclude Control parameter.'
    },
    {
      id: MethodFieldParameterEnum.ExternalStatus4ForAdditionalInterest,
      fieldName: MethodFieldNameEnum.ExternalStatus4ForAdditionalInterest,
      onlinePCF: 'Status 1-5',
      moreInfoText:
        'The External Status 1 for Additional Interest through External Status 5 for Additional Interest parameters determine up to five external statuses to include or exclude from having additional interest added to delinquent accounts. You determine whether the accounts are included or excluded using the Include/Exclude Control parameter.',
      moreInfo:
        'The External Status 1 for Additional Interest through External Status 5 for Additional Interest parameters determine up to five external statuses to include or exclude from having additional interest added to delinquent accounts. You determine whether the accounts are included or excluded using the Include/Exclude Control parameter.'
    },
    {
      id: MethodFieldParameterEnum.ExternalStatus5ForAdditionalInterest,
      fieldName: MethodFieldNameEnum.ExternalStatus5ForAdditionalInterest,
      onlinePCF: 'Status 1-5',
      moreInfoText:
        'The External Status 1 for Additional Interest through External Status 5 for Additional Interest parameters determine up to five external statuses to include or exclude from having additional interest added to delinquent accounts. You determine whether the accounts are included or excluded using the Include/Exclude Control parameter.',
      moreInfo:
        'The External Status 1 for Additional Interest through External Status 5 for Additional Interest parameters determine up to five external statuses to include or exclude from having additional interest added to delinquent accounts. You determine whether the accounts are included or excluded using the Include/Exclude Control parameter.'
    },
    {
      id: MethodFieldParameterEnum.PenaltyPricingStatusReasonCodeTable,
      fieldName: MethodFieldNameEnum.PenaltyPricingStatusReasonCodeTable,
      onlinePCF: 'External Status Reason Table',
      moreInfoText:
        'The Penalty Pricing Status Reason Code Table parameter specifies the external status reason code table identification used to determine whether to include or exclude an account from penalty pricing.',
      moreInfo:
        'The Penalty Pricing Status Reason Code Table parameter specifies the external status reason code table identification used to determine whether to include or exclude an account from penalty pricing.'
    },
    {
      id: MethodFieldParameterEnum.PenaltyPricingTimingOptions,
      fieldName: MethodFieldNameEnum.PenaltyPricingTimingOptions,
      onlinePCF: 'Addition Timing Option',
      moreInfoText:
        'Warning! Before choosing to apply add-on interest in conjunction with the daily accrual interest method, ensure that this action complies with the disclosed terms and conditions of your cardholder agreement and is not prohibited by applicable state laws. Warning! Before choosing to apply add-on interest in conjunction with the daily accrual interest method, ensure that this action complies with the disclosed terms and conditions of your cardholder agreement and is not prohibited by applicable state laws.',
      moreInfo: (
        <div>
          <p>
            The Penalty Pricing Timing Options parameter controls whether to
            apply add-on interest rates to the account during the current cycle
            or during the next cycle.
          </p>
          <p>
            Warning! Before choosing to apply add-on interest in conjunction
            with the daily accrual interest method, ensure that this action
            complies with the disclosed terms and conditions of your cardholder
            agreement and is not prohibited by applicable state laws.
          </p>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.PenaltyPricingStatementMessage1CyclesOldBalance,
      fieldName:
        MethodFieldNameEnum.PenaltyPricingStatementMessage1CyclesOldBalance,
      onlinePCF: '1-3 Cycle Message IDs',
      moreInfoText:
        'The Penalty Pricing Statement Message - 1-Cycle-Old Balance through Penalty Pricing Statement Message - 3-Cycle-Old Balance parameters indicate the text identification name for the message text to print on the cardholder statement when the account becomes one, two, or three cycles delinquent.',
      moreInfo:
        'The Penalty Pricing Statement Message - 1-Cycle-Old Balance through Penalty Pricing Statement Message - 3-Cycle-Old Balance parameters indicate the text identification name for the message text to print on the cardholder statement when the account becomes one, two, or three cycles delinquent.'
    },
    {
      id: MethodFieldParameterEnum.PenaltyPricingStatementMessage2CyclesOldBalance,
      fieldName:
        MethodFieldNameEnum.PenaltyPricingStatementMessage2CyclesOldBalance,
      onlinePCF: '1-3 Cycle Message IDs',
      moreInfoText:
        'The Penalty Pricing Statement Message - 1-Cycle-Old Balance through Penalty Pricing Statement Message - 3-Cycle-Old Balance parameters indicate the text identification name for the message text to print on the cardholder statement when the account becomes one, two, or three cycles delinquent.',
      moreInfo:
        'The Penalty Pricing Statement Message - 1-Cycle-Old Balance through Penalty Pricing Statement Message - 3-Cycle-Old Balance parameters indicate the text identification name for the message text to print on the cardholder statement when the account becomes one, two, or three cycles delinquent.'
    },
    {
      id: MethodFieldParameterEnum.PenaltyPricingStatementMessage3CyclesOldBalance,
      fieldName:
        MethodFieldNameEnum.PenaltyPricingStatementMessage3CyclesOldBalance,
      onlinePCF: '1-3 Cycle Message IDs',
      moreInfoText:
        'The Penalty Pricing Statement Message - 1-Cycle-Old Balance through Penalty Pricing Statement Message - 3-Cycle-Old Balance parameters indicate the text identification name for the message text to print on the cardholder statement when the account becomes one, two, or three cycles delinquent.',
      moreInfo:
        'The Penalty Pricing Statement Message - 1-Cycle-Old Balance through Penalty Pricing Statement Message - 3-Cycle-Old Balance parameters indicate the text identification name for the message text to print on the cardholder statement when the account becomes one, two, or three cycles delinquent.'
    },
    {
      id: MethodFieldParameterEnum.CureDelinquencyFor1CycleDelinquentBalances,
      fieldName: MethodFieldNameEnum.CureDelinquencyFor1CycleDelinquentBalances,
      onlinePCF: 'Cure 1-3 Cycle Delinquency',
      moreInfoText:
        'The Cure Delinquency for 1-Cycle Delinquent Balances through Cure Delinquency for 3-Cycle Delinquent Balances parameters specify the number of consecutive cycles an account that is one, two, or three cycles delinquent must be in a nondelinquent status to be returned from penalty pricing.',
      moreInfo:
        'The Cure Delinquency for 1-Cycle Delinquent Balances through Cure Delinquency for 3-Cycle Delinquent Balances parameters specify the number of consecutive cycles an account that is one, two, or three cycles delinquent must be in a nondelinquent status to be returned from penalty pricing.'
    },
    {
      id: MethodFieldParameterEnum.CureDelinquencyFor2CycleDelinquentBalances,
      fieldName: MethodFieldNameEnum.CureDelinquencyFor2CycleDelinquentBalances,
      onlinePCF: 'Cure 1-3 Cycle Delinquency',
      moreInfoText:
        'The Cure Delinquency for 1-Cycle Delinquent Balances through Cure Delinquency for 3-Cycle Delinquent Balances parameters specify the number of consecutive cycles an account that is one, two, or three cycles delinquent must be in a nondelinquent status to be returned from penalty pricing.',
      moreInfo:
        'The Cure Delinquency for 1-Cycle Delinquent Balances through Cure Delinquency for 3-Cycle Delinquent Balances parameters specify the number of consecutive cycles an account that is one, two, or three cycles delinquent must be in a nondelinquent status to be returned from penalty pricing.'
    },
    {
      id: MethodFieldParameterEnum.CureDelinquencyFor3CycleDelinquentBalances,
      fieldName: MethodFieldNameEnum.CureDelinquencyFor3CycleDelinquentBalances,
      onlinePCF: 'Cure 1-3 Cycle Delinquency',
      moreInfoText:
        'The Cure Delinquency for 1-Cycle Delinquent Balances through Cure Delinquency for 3-Cycle Delinquent Balances parameters specify the number of consecutive cycles an account that is one, two, or three cycles delinquent must be in a nondelinquent status to be returned from penalty pricing.',
      moreInfo:
        'The Cure Delinquency for 1-Cycle Delinquent Balances through Cure Delinquency for 3-Cycle Delinquent Balances parameters specify the number of consecutive cycles an account that is one, two, or three cycles delinquent must be in a nondelinquent status to be returned from penalty pricing.'
    },
    {
      id: MethodFieldParameterEnum.PenaltyPricingOnPromotions,
      fieldName: MethodFieldNameEnum.PenaltyPricingOnPromotions,
      onlinePCF: 'Penalty on Promotion Option',
      moreInfoText:
        'The Penalty Pricing on Promotions parameter adds the penalty rate set in the 1 Cycle Cash through 3+ Cash Cycles and 1 Cycle Merchandise through 3+ Cycles Merchandise parameters in this section to Transaction Level ProcessingSM promotional balances. The System uses the pricing strategy on the cardholder account to determine which standard or promotional balances receive a penalty add-on rate.',
      moreInfo:
        'The Penalty Pricing on Promotions parameter adds the penalty rate set in the 1 Cycle Cash through 3+ Cash Cycles and 1 Cycle Merchandise through 3+ Cycles Merchandise parameters in this section to Transaction Level ProcessingSM promotional balances. The System uses the pricing strategy on the cardholder account to determine which standard or promotional balances receive a penalty add-on rate.'
    },
    {
      id: MethodFieldParameterEnum.CITMethodForPenaltyPricing,
      fieldName: MethodFieldNameEnum.CITMethodForPenaltyPricing,
      onlinePCF: 'Add On CIT Method (CP/PB/CT)',
      moreInfoText:
        'The CIT Method for Penalty Pricing parameter identifies the method the System uses to establish the timing for and specific identifier of a letter notifying active or inactive customers about penalty pricing add-on rates for accounts that are three or more cycles delinquent. This method is not used for notification for accounts that are one or two cycles delinquent. Establish this method in the Change in Terms section (CP PB CT) of the Product Control File.',
      moreInfo:
        'The CIT Method for Penalty Pricing parameter identifies the method the System uses to establish the timing for and specific identifier of a letter notifying active or inactive customers about penalty pricing add-on rates for accounts that are three or more cycles delinquent. This method is not used for notification for accounts that are one or two cycles delinquent. Establish this method in the Change in Terms section (CP PB CT) of the Product Control File.'
    }
  ],
  Info_MiscellaneousAdvancedParametersIM: [
    {
      id: MethodFieldParameterEnum.IncludeZeroBalanceDaysInADBCalculation,
      fieldName: MethodFieldNameEnum.IncludeZeroBalanceDaysInADBCalculation,
      onlinePCF: 'ADB Balance Calculation',
      moreInfoText:
        'The Include Zero Balance Days in ADB Calculation parameter determines whether to include the standard balance when calculating the average daily balance, if the standard balance is zero.',
      moreInfo:
        'The Include Zero Balance Days in ADB Calculation parameter determines whether to include the standard balance when calculating the average daily balance, if the standard balance is zero.'
    }
  ]
};
