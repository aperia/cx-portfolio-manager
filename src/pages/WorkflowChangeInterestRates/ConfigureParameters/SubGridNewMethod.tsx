import EnhanceDropdownList from 'app/components/EnhanceDropdownList';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import { matchSearchValue } from 'app/helpers';
import {
  ColumnType,
  ComboBox,
  Grid,
  NumericTextBox,
  useTranslation
} from 'app/_libraries/_dls';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React, { useMemo } from 'react';
import { parameterGroup, ParameterList } from './newMethodParameterList';

interface IProps {
  searchValue: string;
  method: WorkflowSetupMethodObjectParams;
  original: ParameterList;
  metadata: {
    defaultCashBaseInterest: RefData[];
    defaultMerchandiseBaseInterest: RefData[];
    defaultMaximumCashInterest: RefData[];
    defaultMaximumMerchandiseInterest: RefData[];
    defaultMinimumCashInterest: RefData[];
    defaultMinimumMerchandiseInterest: RefData[];
    indexRateMethod: RefData[];
    defaultBaseInterestUsage: RefData[];
    defaultOverrideInterestLimit: RefData[];
    defaultMinimumUsage: RefData[];
    defaultMaximumUsage: RefData[];
    defaultMaximumInterestUsage: RefData[];
    incentivePricingUsageCode: RefData[];
    incentiveBaseInterestUsageRate: RefData[];
    delinquencyCyclesForTermination: RefData[];
    minimumMaximumRateOverrideOptions: RefData[];
    incentivePricingOverrideOption: RefData[];
    terminateIncentivePricingOnPricingStrategyChange: RefData[];
    methodOverrideTerminationOption: RefData[];
    endofIncentiveWarningNotificationTiming: RefData[];
    endofIncentiveWarningNotificationStatementText: RefData[];
    incentivePricingNumberOfMonths: RefData[];
    messageForProtectedBalanceIncentivePricing: RefData[];
    interestRateRoundingCalculation: RefData[];
    dailyAPRRoundingCalculation: RefData[];
    stopCalculatingInterestAfterNumberofDaysDelinquent: RefData[];
    includeAdditionalCreditBalanceInAverageDailyBalanceCalculation: RefData[];
    combineMerchandiseAndCashBalancesOnInterestCalculations: RefData[];
    restartInterestCalculationsAfterClearingDelinquency: RefData[];
    calculateCompoundInterest: RefData[];
    overridePromotionalRatesForPricingStrategies: RefData[];
    monthlyInterestCalculationOptions: RefData[];
    dailyInterestRoundingOption: RefData[];
    dailyInterestAmountTruncationOption: RefData[];
    overrideInterestRatesForProtectedBalance: RefData[];
    minimumDaysForCycleCodeChange: RefData[];
    roundNumberOfDaysInCycleForInterestCalculations: RefData[];
    cycleCodeChangeOptions: RefData[];
    cyclePeriodChangeCISMemoOption: RefData[];
    interestMethodOldCash: RefData[];
    interestMethodCycleToDateCash: RefData[];
    interestMethodTwoCycleOldCash: RefData[];
    interestMethodOneCycleOldMerchandise: RefData[];
    interestMethodCycleToDateMerchandise: RefData[];
    additionalInterestOnMerchandise1CyclesDelinquent: RefData[];
    additionalInterestOnMerchandise2CyclesDelinquent: RefData[];
    additionalInterestOnCash2CyclesDelinquent: RefData[];
    additionalInterestOnMerchandise3CyclesDelinquent: RefData[];
    additionalInterestOnCash3CyclesDelinquent: RefData[];
    automatedMemosForAdditionalInterest: RefData[];
    useExternalStatusesForAdditionalInterest: RefData[];
    externalStatus1ForAdditionalInterest: RefData[];
    externalStatus2ForAdditionalInterest: RefData[];
    externalStatus3ForAdditionalInterest: RefData[];
    externalStatus4ForAdditionalInterest: RefData[];
    externalStatus5ForAdditionalInterest: RefData[];
    penaltyPricingStatusReasonCodeTable: RefData[];
    penaltyPricingTimingOptions: RefData[];
    penaltyPricingStatementMessage1CyclesOldBalance: RefData[];
    penaltyPricingStatementMessage2CyclesOldBalance: RefData[];
    penaltyPricingStatementMessage3CyclesOldBalance: RefData[];
    cureDelinquencyFor1CycleDelinquentBalances: RefData[];
    cureDelinquencyFor2CycleDelinquentBalances: RefData[];
    cureDelinquencyFor3CycleDelinquentBalances: RefData[];
    penaltyPricingOnPromotions: RefData[];
    citMethodForPenaltyPricing: RefData[];
    includeZeroBalanceDaysInADBCalculation: RefData[];
    indexRateMethodName: RefData[];
  };
  onChange: (
    field: keyof WorkflowSetupMethodObjectParams,
    isRefData?: boolean
  ) => (e: any) => void;
}
const SubGridNewMethod: React.FC<IProps> = ({
  searchValue,
  method,
  original,
  metadata,
  onChange
}) => {
  const { t } = useTranslation();

  const isNewMethod = method?.methodType === 'NEWMETHOD';

  const {
    indexRateMethod,
    indexRateMethodName,
    defaultBaseInterestUsage,
    defaultMinimumUsage,
    defaultMaximumUsage,
    defaultMaximumInterestUsage,
    incentivePricingUsageCode,
    incentiveBaseInterestUsageRate,
    delinquencyCyclesForTermination,
    minimumMaximumRateOverrideOptions,
    incentivePricingOverrideOption,
    terminateIncentivePricingOnPricingStrategyChange,
    methodOverrideTerminationOption,
    endofIncentiveWarningNotificationTiming,
    endofIncentiveWarningNotificationStatementText,
    messageForProtectedBalanceIncentivePricing,
    interestRateRoundingCalculation,
    dailyAPRRoundingCalculation,
    includeAdditionalCreditBalanceInAverageDailyBalanceCalculation,
    combineMerchandiseAndCashBalancesOnInterestCalculations,
    restartInterestCalculationsAfterClearingDelinquency,
    calculateCompoundInterest,
    overridePromotionalRatesForPricingStrategies,
    monthlyInterestCalculationOptions,
    dailyInterestRoundingOption,
    dailyInterestAmountTruncationOption,
    overrideInterestRatesForProtectedBalance,
    roundNumberOfDaysInCycleForInterestCalculations,
    cycleCodeChangeOptions,
    cyclePeriodChangeCISMemoOption,
    interestMethodOldCash,
    interestMethodCycleToDateCash,
    interestMethodTwoCycleOldCash,
    interestMethodOneCycleOldMerchandise,
    interestMethodCycleToDateMerchandise,
    automatedMemosForAdditionalInterest,
    useExternalStatusesForAdditionalInterest,
    externalStatus1ForAdditionalInterest,
    externalStatus2ForAdditionalInterest,
    externalStatus3ForAdditionalInterest,
    externalStatus4ForAdditionalInterest,
    externalStatus5ForAdditionalInterest,
    penaltyPricingStatusReasonCodeTable,
    penaltyPricingTimingOptions,
    penaltyPricingStatementMessage1CyclesOldBalance,
    penaltyPricingStatementMessage2CyclesOldBalance,
    penaltyPricingStatementMessage3CyclesOldBalance,
    cureDelinquencyFor1CycleDelinquentBalances,
    cureDelinquencyFor2CycleDelinquentBalances,
    cureDelinquencyFor3CycleDelinquentBalances,
    penaltyPricingOnPromotions,
    citMethodForPenaltyPricing,
    includeZeroBalanceDaysInADBCalculation
  } = metadata;

  const data = useMemo(
    () =>
      parameterGroup[original.id].filter(p =>
        matchSearchValue(
          [p.fieldName, p.moreInfoText, p.onlinePCF],
          searchValue
        )
      ),
    [searchValue, original.id]
  );

  const valueAccessor = (data: Record<string, any>) => {
    const paramId = data.id as MethodFieldParameterEnum;

    switch (paramId) {
      case MethodFieldParameterEnum.DefaultCashBaseInterest: {
        return (
          <NumericTextBox
            size="sm"
            format="p3"
            showStep={false}
            maxLength={5}
            autoFocus={false}
            value={method?.[MethodFieldParameterEnum.DefaultCashBaseInterest]}
            onChange={onChange(
              MethodFieldParameterEnum.DefaultCashBaseInterest
            )}
          />
        );
      }
      case MethodFieldParameterEnum.DefaultMerchandiseBaseInterest: {
        return (
          <NumericTextBox
            size="sm"
            format="p3"
            showStep={false}
            maxLength={5}
            autoFocus={false}
            value={
              method?.[MethodFieldParameterEnum.DefaultMerchandiseBaseInterest]
            }
            onChange={onChange(
              MethodFieldParameterEnum.DefaultMerchandiseBaseInterest
            )}
          />
        );
      }
      case MethodFieldParameterEnum.DefaultMaximumCashInterest: {
        return (
          <NumericTextBox
            size="sm"
            format="p3"
            showStep={false}
            maxLength={5}
            autoFocus={false}
            value={
              method?.[MethodFieldParameterEnum.DefaultMaximumCashInterest]
            }
            onChange={onChange(
              MethodFieldParameterEnum.DefaultMaximumCashInterest
            )}
          />
        );
      }
      case MethodFieldParameterEnum.DefaultMaximumMerchandiseInterest: {
        return (
          <NumericTextBox
            size="sm"
            format="p3"
            showStep={false}
            maxLength={5}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum.DefaultMaximumMerchandiseInterest
              ]
            }
            onChange={onChange(
              MethodFieldParameterEnum.DefaultMaximumMerchandiseInterest
            )}
          />
        );
      }
      case MethodFieldParameterEnum.DefaultMinimumCashInterest: {
        return (
          <NumericTextBox
            size="sm"
            format="p3"
            showStep={false}
            maxLength={5}
            autoFocus={false}
            value={
              method?.[MethodFieldParameterEnum.DefaultMinimumCashInterest]
            }
            onChange={onChange(
              MethodFieldParameterEnum.DefaultMinimumCashInterest
            )}
          />
        );
      }
      case MethodFieldParameterEnum.DefaultMinimumMerchandiseInterest: {
        return (
          <NumericTextBox
            size="sm"
            format="p3"
            showStep={false}
            maxLength={5}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum.DefaultMinimumMerchandiseInterest
              ]
            }
            onChange={onChange(
              MethodFieldParameterEnum.DefaultMinimumMerchandiseInterest
            )}
          />
        );
      }
      case MethodFieldParameterEnum.IndexRateMethod: {
        const value = indexRateMethod?.find(
          o => o.code === method?.[MethodFieldParameterEnum.IndexRateMethod]
        );

        return (
          <ComboBox
            textField="text"
            value={value}
            size="small"
            placeholder={t('txt_select_an_option')}
            onChange={onChange(MethodFieldParameterEnum.IndexRateMethod, true)}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {indexRateMethod.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.IndexRateMethodName: {
        const value = indexRateMethodName?.find(
          o => o.code === method?.[MethodFieldParameterEnum.IndexRateMethodName]
        );

        return (
          <ComboBox
            textField="text"
            value={value}
            size="small"
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.IndexRateMethodName,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {indexRateMethodName.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.DefaultBaseInterestUsage: {
        const value = defaultBaseInterestUsage?.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.DefaultBaseInterestUsage]
        );

        return (
          <EnhanceDropdownList
            id={MethodFieldParameterEnum.DefaultBaseInterestUsage}
            size="small"
            value={isNewMethod ? '' : value}
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.DefaultBaseInterestUsage,
              true
            )}
            options={defaultBaseInterestUsage}
          />
        );
      }
      case MethodFieldParameterEnum.DefaultOverrideInterestLimit: {
        return (
          <NumericTextBox
            size="sm"
            format="p3"
            showStep={false}
            maxLength={5}
            autoFocus={false}
            value={
              method?.[MethodFieldParameterEnum.DefaultOverrideInterestLimit]
            }
            onChange={onChange(
              MethodFieldParameterEnum.DefaultOverrideInterestLimit
            )}
          />
        );
      }
      case MethodFieldParameterEnum.DefaultMinimumUsage: {
        const value = defaultMinimumUsage?.find(
          o => o.code === method?.[MethodFieldParameterEnum.DefaultMinimumUsage]
        );

        return (
          <EnhanceDropdownList
            id={MethodFieldParameterEnum.DefaultMinimumUsage}
            size="small"
            value={isNewMethod ? '' : value}
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.DefaultMinimumUsage,
              true
            )}
            options={defaultMinimumUsage}
          />
        );
      }
      case MethodFieldParameterEnum.DefaultMaximumUsage: {
        const value = defaultMaximumUsage?.find(
          o => o.code === method?.[MethodFieldParameterEnum.DefaultMaximumUsage]
        );

        return (
          <EnhanceDropdownList
            id={MethodFieldParameterEnum.DefaultMaximumUsage}
            size="small"
            value={isNewMethod ? '' : value}
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.DefaultMaximumUsage,
              true
            )}
            options={defaultMaximumUsage}
          />
        );
      }
      case MethodFieldParameterEnum.DefaultMaximumInterestUsage: {
        const value = defaultMaximumInterestUsage?.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.DefaultMaximumInterestUsage]
        );

        return (
          <EnhanceDropdownList
            id={MethodFieldParameterEnum.DefaultMaximumInterestUsage}
            size="small"
            value={isNewMethod ? '' : value}
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.DefaultMaximumInterestUsage,
              true
            )}
            options={defaultMaximumInterestUsage}
          />
        );
      }
      case MethodFieldParameterEnum.IncentivePricingUsageCode: {
        const value = incentivePricingUsageCode?.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.IncentivePricingUsageCode]
        );

        return (
          <EnhanceDropdownList
            id={MethodFieldParameterEnum.IncentivePricingUsageCode}
            size="small"
            value={isNewMethod ? '' : value}
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.IncentivePricingUsageCode,
              true
            )}
            options={incentivePricingUsageCode}
          />
        );
      }
      case MethodFieldParameterEnum.IncentiveBaseInterestUsageRate: {
        const value = incentiveBaseInterestUsageRate?.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.IncentiveBaseInterestUsageRate]
        );

        return (
          <EnhanceDropdownList
            id={MethodFieldParameterEnum.IncentiveBaseInterestUsageRate}
            size="small"
            value={isNewMethod ? '' : value}
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.IncentiveBaseInterestUsageRate,
              true
            )}
            options={incentiveBaseInterestUsageRate}
          />
        );
      }
      case MethodFieldParameterEnum.DelinquencyCyclesForTermination: {
        const value = delinquencyCyclesForTermination?.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.DelinquencyCyclesForTermination]
        );

        return (
          <EnhanceDropdownList
            id={MethodFieldParameterEnum.DelinquencyCyclesForTermination}
            size="small"
            value={isNewMethod ? '' : value}
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.DelinquencyCyclesForTermination,
              true
            )}
            options={delinquencyCyclesForTermination}
          />
        );
      }
      case MethodFieldParameterEnum.MinimumMaximumRateOverrideOptions: {
        const value = minimumMaximumRateOverrideOptions?.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.MinimumMaximumRateOverrideOptions]
        );

        return (
          <EnhanceDropdownList
            id={MethodFieldParameterEnum.MinimumMaximumRateOverrideOptions}
            size="small"
            value={isNewMethod ? '' : value}
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.MinimumMaximumRateOverrideOptions,
              true
            )}
            options={minimumMaximumRateOverrideOptions}
          />
        );
      }
      case MethodFieldParameterEnum.IncentivePricingOverrideOption: {
        const value = incentivePricingOverrideOption?.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.IncentivePricingOverrideOption]
        );

        return (
          <EnhanceDropdownList
            id={MethodFieldParameterEnum.IncentivePricingOverrideOption}
            size="small"
            value={isNewMethod ? '' : value}
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.IncentivePricingOverrideOption,
              true
            )}
            options={incentivePricingOverrideOption}
          />
        );
      }
      case MethodFieldParameterEnum.TerminateIncentivePricingOnPricingStrategyChange: {
        const value = terminateIncentivePricingOnPricingStrategyChange?.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum
                .TerminateIncentivePricingOnPricingStrategyChange
            ]
        );

        return (
          <EnhanceDropdownList
            id={
              MethodFieldParameterEnum.TerminateIncentivePricingOnPricingStrategyChange
            }
            size="small"
            value={isNewMethod ? '' : value}
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.TerminateIncentivePricingOnPricingStrategyChange,
              true
            )}
            options={terminateIncentivePricingOnPricingStrategyChange}
          />
        );
      }
      case MethodFieldParameterEnum.MethodOverrideTerminationOption: {
        const value = methodOverrideTerminationOption?.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.MethodOverrideTerminationOption]
        );

        return (
          <EnhanceDropdownList
            id={MethodFieldParameterEnum.MethodOverrideTerminationOption}
            size="small"
            value={isNewMethod ? '' : value}
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.MethodOverrideTerminationOption,
              true
            )}
            options={methodOverrideTerminationOption}
          />
        );
      }
      case MethodFieldParameterEnum.IncentiveCashInterestRate: {
        return (
          <NumericTextBox
            size="sm"
            format="p3"
            showStep={false}
            maxLength={5}
            autoFocus={false}
            value={method?.[MethodFieldParameterEnum.IncentiveCashInterestRate]}
            onChange={onChange(
              MethodFieldParameterEnum.IncentiveCashInterestRate
            )}
          />
        );
      }
      case MethodFieldParameterEnum.IncentiveMerchandiseInterestRate: {
        return (
          <NumericTextBox
            size="sm"
            format="p3"
            showStep={false}
            maxLength={5}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum.IncentiveMerchandiseInterestRate
              ]
            }
            onChange={onChange(
              MethodFieldParameterEnum.IncentiveMerchandiseInterestRate
            )}
          />
        );
      }
      case MethodFieldParameterEnum.EndofIncentiveWarningNotificationTiming: {
        const value = endofIncentiveWarningNotificationTiming?.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum.EndofIncentiveWarningNotificationTiming
            ]
        );

        return (
          <EnhanceDropdownList
            id={
              MethodFieldParameterEnum.EndofIncentiveWarningNotificationTiming
            }
            size="small"
            value={isNewMethod ? '' : value}
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.EndofIncentiveWarningNotificationTiming,
              true
            )}
            options={endofIncentiveWarningNotificationTiming}
          />
        );
      }
      case MethodFieldParameterEnum.EndofIncentiveWarningNotificationStatementText: {
        const value = endofIncentiveWarningNotificationStatementText?.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum
                .EndofIncentiveWarningNotificationStatementText
            ]
        );

        return (
          <ComboBox
            textField="text"
            value={isNewMethod ? '' : value}
            size="small"
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.EndofIncentiveWarningNotificationStatementText,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {endofIncentiveWarningNotificationStatementText.map((item: any) => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.IncentivePricingNumberOfMonths: {
        return (
          <NumericTextBox
            size="sm"
            format="n0"
            showStep={false}
            maxLength={2}
            autoFocus={false}
            placeholder={t('txt_enter_a_value')}
            value={
              method?.[MethodFieldParameterEnum.IncentivePricingNumberOfMonths]
            }
            onChange={onChange(
              MethodFieldParameterEnum.IncentivePricingNumberOfMonths
            )}
          />
        );
      }
      case MethodFieldParameterEnum.MinimumRateForCash: {
        return (
          <NumericTextBox
            size="sm"
            format="p3"
            showStep={false}
            maxLength={5}
            autoFocus={false}
            value={method?.[MethodFieldParameterEnum.MinimumRateForCash]}
            onChange={onChange(MethodFieldParameterEnum.MinimumRateForCash)}
          />
        );
      }
      case MethodFieldParameterEnum.MaximumRateForCash: {
        return (
          <NumericTextBox
            size="sm"
            format="p3"
            showStep={false}
            maxLength={5}
            autoFocus={false}
            value={method?.[MethodFieldParameterEnum.MaximumRateForCash]}
            onChange={onChange(MethodFieldParameterEnum.MaximumRateForCash)}
          />
        );
      }
      case MethodFieldParameterEnum.MinimumRateForMerchandise: {
        return (
          <NumericTextBox
            size="sm"
            format="p3"
            showStep={false}
            maxLength={5}
            autoFocus={false}
            value={method?.[MethodFieldParameterEnum.MinimumRateForMerchandise]}
            onChange={onChange(
              MethodFieldParameterEnum.MinimumRateForMerchandise
            )}
          />
        );
      }
      case MethodFieldParameterEnum.MaximumRateForMerchandise: {
        return (
          <NumericTextBox
            size="sm"
            format="p3"
            showStep={false}
            maxLength={5}
            autoFocus={false}
            value={method?.[MethodFieldParameterEnum.MaximumRateForMerchandise]}
            onChange={onChange(
              MethodFieldParameterEnum.MaximumRateForMerchandise
            )}
          />
        );
      }
      case MethodFieldParameterEnum.PreventTermsChangeDuringIncentivePricingPeriod: {
        return (
          <NumericTextBox
            size="sm"
            format="n0"
            showStep={false}
            maxLength={3}
            autoFocus={false}
            placeholder={t('txt_enter_a_value')}
            value={
              method?.[
                MethodFieldParameterEnum
                  .PreventTermsChangeDuringIncentivePricingPeriod
              ]
            }
            onChange={onChange(
              MethodFieldParameterEnum.PreventTermsChangeDuringIncentivePricingPeriod
            )}
          />
        );
      }
      case MethodFieldParameterEnum.MessageForProtectedBalanceIncentivePricing: {
        const value = messageForProtectedBalanceIncentivePricing?.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum
                .MessageForProtectedBalanceIncentivePricing
            ]
        );

        return (
          <ComboBox
            textField="text"
            value={isNewMethod ? '' : value}
            size="small"
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.MessageForProtectedBalanceIncentivePricing,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {messageForProtectedBalanceIncentivePricing.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.InterestRateRoundingCalculation: {
        const value = interestRateRoundingCalculation.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.InterestRateRoundingCalculation]
        );

        return (
          <ComboBox
            textField="text"
            value={isNewMethod ? '' : value}
            size="small"
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.InterestRateRoundingCalculation,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {interestRateRoundingCalculation.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.DailyAPRRoundingCalculation: {
        const value = dailyAPRRoundingCalculation.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.DailyAPRRoundingCalculation]
        );

        return (
          <ComboBox
            textField="text"
            value={isNewMethod ? '' : value}
            size="small"
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.DailyAPRRoundingCalculation,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {dailyAPRRoundingCalculation.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.StopCalculatingInterestAfterNumberofDaysDelinquent: {
        return (
          <NumericTextBox
            size="sm"
            format="n0"
            showStep={false}
            maxLength={3}
            autoFocus={false}
            placeholder={t('txt_enter_a_value')}
            value={
              method?.[
                MethodFieldParameterEnum
                  .StopCalculatingInterestAfterNumberofDaysDelinquent
              ]
            }
            onChange={onChange(
              MethodFieldParameterEnum.StopCalculatingInterestAfterNumberofDaysDelinquent
            )}
          />
        );
      }
      case MethodFieldParameterEnum.IncludeAdditionalCreditBalanceInAverageDailyBalanceCalculation: {
        const value =
          includeAdditionalCreditBalanceInAverageDailyBalanceCalculation.find(
            o =>
              o.code ===
              method?.[
                MethodFieldParameterEnum
                  .IncludeAdditionalCreditBalanceInAverageDailyBalanceCalculation
              ]
          );

        return (
          <ComboBox
            textField="text"
            value={isNewMethod ? '' : value}
            size="small"
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.IncludeAdditionalCreditBalanceInAverageDailyBalanceCalculation,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {includeAdditionalCreditBalanceInAverageDailyBalanceCalculation.map(
              item => (
                <ComboBox.Item key={item.code} value={item} label={item.text} />
              )
            )}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.CombineMerchandiseAndCashBalancesOnInterestCalculations: {
        const value =
          combineMerchandiseAndCashBalancesOnInterestCalculations.find(
            o =>
              o.code ===
              method?.[
                MethodFieldParameterEnum
                  .CombineMerchandiseAndCashBalancesOnInterestCalculations
              ]
          );

        return (
          <ComboBox
            textField="text"
            value={isNewMethod ? '' : value}
            size="small"
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.CombineMerchandiseAndCashBalancesOnInterestCalculations,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {combineMerchandiseAndCashBalancesOnInterestCalculations.map(
              item => (
                <ComboBox.Item key={item.code} value={item} label={item.text} />
              )
            )}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.RestartInterestCalculationsAfterClearingDelinquency: {
        const value = restartInterestCalculationsAfterClearingDelinquency.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum
                .RestartInterestCalculationsAfterClearingDelinquency
            ]
        );

        return (
          <ComboBox
            textField="text"
            value={isNewMethod ? '' : value}
            size="small"
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.RestartInterestCalculationsAfterClearingDelinquency,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {restartInterestCalculationsAfterClearingDelinquency.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.CalculateCompoundInterest: {
        const value = calculateCompoundInterest.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.CalculateCompoundInterest]
        );

        return (
          <ComboBox
            textField="text"
            value={isNewMethod ? '' : value}
            size="small"
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.CalculateCompoundInterest,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {calculateCompoundInterest.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.OverridePromotionalRatesForPricingStrategies: {
        const value = overridePromotionalRatesForPricingStrategies.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum
                .OverridePromotionalRatesForPricingStrategies
            ]
        );

        return (
          <ComboBox
            textField="text"
            value={isNewMethod ? '' : value}
            size="small"
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.OverridePromotionalRatesForPricingStrategies,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {overridePromotionalRatesForPricingStrategies.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.MonthlyInterestCalculationOptions: {
        const value = monthlyInterestCalculationOptions.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.MonthlyInterestCalculationOptions]
        );

        return (
          <ComboBox
            textField="text"
            value={isNewMethod ? '' : value}
            size="small"
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.MonthlyInterestCalculationOptions,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {monthlyInterestCalculationOptions.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.DailyInterestRoundingOption: {
        const value = dailyInterestRoundingOption.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.DailyInterestRoundingOption]
        );

        return (
          <ComboBox
            textField="text"
            value={isNewMethod ? '' : value}
            size="small"
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.DailyInterestRoundingOption,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {dailyInterestRoundingOption.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.DailyInterestAmountTruncationOption: {
        const value = dailyInterestAmountTruncationOption.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum.DailyInterestAmountTruncationOption
            ]
        );

        return (
          <ComboBox
            textField="text"
            value={isNewMethod ? '' : value}
            size="small"
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.DailyInterestAmountTruncationOption,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {dailyInterestAmountTruncationOption.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.OverrideInterestRatesForProtectedBalance: {
        const value = overrideInterestRatesForProtectedBalance.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum.OverrideInterestRatesForProtectedBalance
            ]
        );

        return (
          <ComboBox
            textField="text"
            value={isNewMethod ? '' : value}
            size="small"
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.OverrideInterestRatesForProtectedBalance,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {overrideInterestRatesForProtectedBalance.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.MinimumDaysForCycleCodeChange: {
        return (
          <NumericTextBox
            size="sm"
            format="n0"
            showStep={false}
            maxLength={2}
            autoFocus={false}
            placeholder={t('txt_enter_a_value')}
            value={
              method?.[MethodFieldParameterEnum.MinimumDaysForCycleCodeChange]
            }
            onChange={onChange(
              MethodFieldParameterEnum.MinimumDaysForCycleCodeChange
            )}
          />
        );
      }
      case MethodFieldParameterEnum.RoundNumberOfDaysInCycleForInterestCalculations: {
        const value = roundNumberOfDaysInCycleForInterestCalculations?.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum
                .RoundNumberOfDaysInCycleForInterestCalculations
            ]
        );

        return (
          <EnhanceDropdownList
            id={
              MethodFieldParameterEnum.RoundNumberOfDaysInCycleForInterestCalculations
            }
            size="small"
            value={isNewMethod ? '' : value}
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.RoundNumberOfDaysInCycleForInterestCalculations,
              true
            )}
            tooltipPlacementSide="left"
            options={roundNumberOfDaysInCycleForInterestCalculations}
            tooltipItemCustom={roundNumberOfDaysInCycleForInterestCalculations.map(
              o => ({
                code: o.code,
                tooltipItem: (
                  <div
                    className="custom-tooltip"
                    dangerouslySetInnerHTML={{
                      __html: o.text
                        ?.replace(/\n/g, '</br>')
                        ?.replace(/\t/g, '&emsp;') as any
                    }}
                  ></div>
                )
              })
            )}
          />
        );
      }
      case MethodFieldParameterEnum.CycleCodeChangeOptions: {
        const value = cycleCodeChangeOptions.find(
          o =>
            o.code === method?.[MethodFieldParameterEnum.CycleCodeChangeOptions]
        );

        return (
          <EnhanceDropdownList
            id={MethodFieldParameterEnum.CycleCodeChangeOptions}
            size="small"
            value={isNewMethod ? '' : value}
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.CycleCodeChangeOptions,
              true
            )}
            options={cycleCodeChangeOptions}
          />
        );
      }
      case MethodFieldParameterEnum.CyclePeriodChangeCISMemoOption: {
        const value = cyclePeriodChangeCISMemoOption.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.CyclePeriodChangeCISMemoOption]
        );

        return (
          <EnhanceDropdownList
            id={MethodFieldParameterEnum.CyclePeriodChangeCISMemoOption}
            size="small"
            value={isNewMethod ? '' : value}
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.CyclePeriodChangeCISMemoOption,
              true
            )}
            options={cyclePeriodChangeCISMemoOption}
          />
        );
      }
      case MethodFieldParameterEnum.InterestMethodOldCash: {
        const value = interestMethodOldCash?.find(
          o =>
            o.code === method?.[MethodFieldParameterEnum.InterestMethodOldCash]
        );

        return (
          <EnhanceDropdownList
            id={MethodFieldParameterEnum.InterestMethodOldCash}
            size="small"
            value={isNewMethod ? '' : value}
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.InterestMethodOldCash,
              true
            )}
            options={interestMethodOldCash}
          />
        );
      }
      case MethodFieldParameterEnum.InterestMethodCycleToDateCash: {
        const value = interestMethodCycleToDateCash?.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.InterestMethodCycleToDateCash]
        );

        return (
          <EnhanceDropdownList
            id={MethodFieldParameterEnum.InterestMethodCycleToDateCash}
            size="small"
            value={isNewMethod ? '' : value}
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.InterestMethodCycleToDateCash,
              true
            )}
            options={interestMethodCycleToDateCash}
          />
        );
      }
      case MethodFieldParameterEnum.InterestMethodTwoCycleOldCash: {
        const value = interestMethodTwoCycleOldCash?.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.InterestMethodTwoCycleOldCash]
        );

        return (
          <EnhanceDropdownList
            id={MethodFieldParameterEnum.InterestMethodTwoCycleOldCash}
            size="small"
            value={isNewMethod ? '' : value}
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.InterestMethodTwoCycleOldCash,
              true
            )}
            options={interestMethodTwoCycleOldCash}
          />
        );
      }
      case MethodFieldParameterEnum.InterestMethodOneCycleOldMerchandise: {
        const value = interestMethodOneCycleOldMerchandise?.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum.InterestMethodOneCycleOldMerchandise
            ]
        );

        return (
          <EnhanceDropdownList
            id={MethodFieldParameterEnum.InterestMethodOneCycleOldMerchandise}
            size="small"
            value={isNewMethod ? '' : value}
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.InterestMethodOneCycleOldMerchandise,
              true
            )}
            options={interestMethodOneCycleOldMerchandise}
          />
        );
      }
      case MethodFieldParameterEnum.InterestMethodCycleToDateMerchandise: {
        const value = interestMethodCycleToDateMerchandise?.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum.InterestMethodCycleToDateMerchandise
            ]
        );

        return (
          <EnhanceDropdownList
            id={MethodFieldParameterEnum.InterestMethodCycleToDateMerchandise}
            size="small"
            value={isNewMethod ? '' : value}
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.InterestMethodCycleToDateMerchandise,
              true
            )}
            options={interestMethodCycleToDateMerchandise}
          />
        );
      }
      case MethodFieldParameterEnum.AdditionalInterestOnMerchandise1CyclesDelinquent: {
        return (
          <NumericTextBox
            size="sm"
            format="p3"
            showStep={false}
            maxLength={5}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum
                  .AdditionalInterestOnMerchandise1CyclesDelinquent
              ]
            }
            onChange={onChange(
              MethodFieldParameterEnum.AdditionalInterestOnMerchandise1CyclesDelinquent
            )}
          />
        );
      }
      case MethodFieldParameterEnum.AdditionalInterestOnMerchandise2CyclesDelinquent: {
        return (
          <NumericTextBox
            size="sm"
            format="p3"
            showStep={false}
            maxLength={5}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum
                  .AdditionalInterestOnMerchandise2CyclesDelinquent
              ]
            }
            onChange={onChange(
              MethodFieldParameterEnum.AdditionalInterestOnMerchandise2CyclesDelinquent
            )}
          />
        );
      }
      case MethodFieldParameterEnum.AdditionalInterestOnCash2CyclesDelinquent: {
        return (
          <NumericTextBox
            size="sm"
            format="p3"
            showStep={false}
            maxLength={5}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum
                  .AdditionalInterestOnCash2CyclesDelinquent
              ]
            }
            onChange={onChange(
              MethodFieldParameterEnum.AdditionalInterestOnCash2CyclesDelinquent
            )}
          />
        );
      }
      case MethodFieldParameterEnum.AdditionalInterestOnMerchandise3CyclesDelinquent: {
        return (
          <NumericTextBox
            size="sm"
            format="p3"
            showStep={false}
            maxLength={5}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum
                  .AdditionalInterestOnMerchandise3CyclesDelinquent
              ]
            }
            onChange={onChange(
              MethodFieldParameterEnum.AdditionalInterestOnMerchandise3CyclesDelinquent
            )}
          />
        );
      }
      case MethodFieldParameterEnum.AdditionalInterestOnCash3CyclesDelinquent: {
        return (
          <NumericTextBox
            size="sm"
            format="p3"
            showStep={false}
            maxLength={5}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum
                  .AdditionalInterestOnCash3CyclesDelinquent
              ]
            }
            onChange={onChange(
              MethodFieldParameterEnum.AdditionalInterestOnCash3CyclesDelinquent
            )}
          />
        );
      }
      case MethodFieldParameterEnum.AutomatedMemosForAdditionalInterest: {
        const value = automatedMemosForAdditionalInterest?.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum.AutomatedMemosForAdditionalInterest
            ]
        );

        return (
          <EnhanceDropdownList
            size="small"
            value={isNewMethod ? '' : value}
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.AutomatedMemosForAdditionalInterest,
              true
            )}
            options={automatedMemosForAdditionalInterest}
          />
        );
      }
      case MethodFieldParameterEnum.UseExternalStatusesForAdditionalInterest: {
        const value = useExternalStatusesForAdditionalInterest?.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum.UseExternalStatusesForAdditionalInterest
            ]
        );

        return (
          <EnhanceDropdownList
            size="small"
            value={isNewMethod ? '' : value}
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.UseExternalStatusesForAdditionalInterest,
              true
            )}
            options={useExternalStatusesForAdditionalInterest}
          />
        );
      }
      case MethodFieldParameterEnum.ExternalStatus1ForAdditionalInterest: {
        const value = externalStatus1ForAdditionalInterest?.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum.ExternalStatus1ForAdditionalInterest
            ]
        );

        return (
          <EnhanceDropdownList
            size="small"
            value={isNewMethod ? '' : value}
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.ExternalStatus1ForAdditionalInterest,
              true
            )}
            options={externalStatus1ForAdditionalInterest}
          />
        );
      }
      case MethodFieldParameterEnum.ExternalStatus2ForAdditionalInterest: {
        const value = externalStatus2ForAdditionalInterest?.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum.ExternalStatus2ForAdditionalInterest
            ]
        );

        return (
          <EnhanceDropdownList
            size="small"
            value={isNewMethod ? '' : value}
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.ExternalStatus2ForAdditionalInterest,
              true
            )}
            options={externalStatus2ForAdditionalInterest}
          />
        );
      }
      case MethodFieldParameterEnum.ExternalStatus3ForAdditionalInterest: {
        const value = externalStatus3ForAdditionalInterest?.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum.ExternalStatus3ForAdditionalInterest
            ]
        );

        return (
          <EnhanceDropdownList
            size="small"
            value={isNewMethod ? '' : value}
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.ExternalStatus3ForAdditionalInterest,
              true
            )}
            options={externalStatus3ForAdditionalInterest}
          />
        );
      }
      case MethodFieldParameterEnum.ExternalStatus4ForAdditionalInterest: {
        const value = externalStatus4ForAdditionalInterest?.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum.ExternalStatus4ForAdditionalInterest
            ]
        );

        return (
          <EnhanceDropdownList
            size="small"
            value={isNewMethod ? '' : value}
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.ExternalStatus4ForAdditionalInterest,
              true
            )}
            options={externalStatus4ForAdditionalInterest}
          />
        );
      }
      case MethodFieldParameterEnum.ExternalStatus5ForAdditionalInterest: {
        const value = externalStatus5ForAdditionalInterest?.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum.ExternalStatus5ForAdditionalInterest
            ]
        );

        return (
          <EnhanceDropdownList
            size="small"
            value={isNewMethod ? '' : value}
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.ExternalStatus5ForAdditionalInterest,
              true
            )}
            options={externalStatus5ForAdditionalInterest}
          />
        );
      }
      case MethodFieldParameterEnum.PenaltyPricingStatusReasonCodeTable: {
        const value = penaltyPricingStatusReasonCodeTable.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum.PenaltyPricingStatusReasonCodeTable
            ]
        );

        return (
          <ComboBox
            textField="text"
            value={isNewMethod ? '' : value}
            size="small"
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.PenaltyPricingStatusReasonCodeTable,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {penaltyPricingStatusReasonCodeTable.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.PenaltyPricingTimingOptions: {
        const value = penaltyPricingTimingOptions?.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.PenaltyPricingTimingOptions]
        );

        return (
          <EnhanceDropdownList
            size="small"
            value={isNewMethod ? '' : value}
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.PenaltyPricingTimingOptions,
              true
            )}
            options={penaltyPricingTimingOptions}
          />
        );
      }
      case MethodFieldParameterEnum.PenaltyPricingStatementMessage1CyclesOldBalance: {
        const value = penaltyPricingStatementMessage1CyclesOldBalance.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum
                .PenaltyPricingStatementMessage1CyclesOldBalance
            ]
        );

        return (
          <ComboBox
            textField="text"
            value={isNewMethod ? '' : value}
            size="small"
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.PenaltyPricingStatementMessage1CyclesOldBalance,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {penaltyPricingStatementMessage1CyclesOldBalance.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.PenaltyPricingStatementMessage2CyclesOldBalance: {
        const value = penaltyPricingStatementMessage2CyclesOldBalance.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum
                .PenaltyPricingStatementMessage2CyclesOldBalance
            ]
        );

        return (
          <ComboBox
            textField="text"
            value={isNewMethod ? '' : value}
            size="small"
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.PenaltyPricingStatementMessage2CyclesOldBalance,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {penaltyPricingStatementMessage2CyclesOldBalance.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.PenaltyPricingStatementMessage3CyclesOldBalance: {
        const value = penaltyPricingStatementMessage3CyclesOldBalance.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum
                .PenaltyPricingStatementMessage3CyclesOldBalance
            ]
        );

        return (
          <ComboBox
            textField="text"
            value={isNewMethod ? '' : value}
            size="small"
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.PenaltyPricingStatementMessage3CyclesOldBalance,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {penaltyPricingStatementMessage3CyclesOldBalance.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.CureDelinquencyFor1CycleDelinquentBalances: {
        const value = cureDelinquencyFor1CycleDelinquentBalances?.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum
                .CureDelinquencyFor1CycleDelinquentBalances
            ]
        );

        return (
          <EnhanceDropdownList
            size="small"
            value={isNewMethod ? '' : value}
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.CureDelinquencyFor1CycleDelinquentBalances,
              true
            )}
            options={cureDelinquencyFor1CycleDelinquentBalances}
          />
        );
      }
      case MethodFieldParameterEnum.CureDelinquencyFor2CycleDelinquentBalances: {
        const value = cureDelinquencyFor2CycleDelinquentBalances?.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum
                .CureDelinquencyFor2CycleDelinquentBalances
            ]
        );

        return (
          <EnhanceDropdownList
            size="small"
            value={isNewMethod ? '' : value}
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.CureDelinquencyFor2CycleDelinquentBalances,
              true
            )}
            options={cureDelinquencyFor2CycleDelinquentBalances}
          />
        );
      }
      case MethodFieldParameterEnum.CureDelinquencyFor3CycleDelinquentBalances: {
        const value = cureDelinquencyFor3CycleDelinquentBalances?.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum
                .CureDelinquencyFor3CycleDelinquentBalances
            ]
        );

        return (
          <EnhanceDropdownList
            size="small"
            value={isNewMethod ? '' : value}
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.CureDelinquencyFor3CycleDelinquentBalances,
              true
            )}
            options={cureDelinquencyFor3CycleDelinquentBalances}
          />
        );
      }
      case MethodFieldParameterEnum.PenaltyPricingOnPromotions: {
        const value = penaltyPricingOnPromotions?.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.PenaltyPricingOnPromotions]
        );

        return (
          <EnhanceDropdownList
            size="small"
            value={isNewMethod ? '' : value}
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.PenaltyPricingOnPromotions,
              true
            )}
            options={penaltyPricingOnPromotions}
          />
        );
      }
      case MethodFieldParameterEnum.CITMethodForPenaltyPricing: {
        const value = citMethodForPenaltyPricing.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.CITMethodForPenaltyPricing]
        );

        return (
          <ComboBox
            textField="text"
            value={isNewMethod ? '' : value}
            size="small"
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.CITMethodForPenaltyPricing,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {citMethodForPenaltyPricing.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.IncludeZeroBalanceDaysInADBCalculation: {
        const value = includeZeroBalanceDaysInADBCalculation?.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum.IncludeZeroBalanceDaysInADBCalculation
            ]
        );

        return (
          <EnhanceDropdownList
            size="small"
            value={isNewMethod ? '' : value}
            placeholder={t('txt_select_an_option')}
            onChange={onChange(
              MethodFieldParameterEnum.IncludeZeroBalanceDaysInADBCalculation,
              true
            )}
            options={includeZeroBalanceDaysInADBCalculation}
          />
        );
      }
    }
  };

  const columns: ColumnType[] = [
    {
      id: 'fieldName',
      Header: t('txt_business_name'),
      accessor: 'fieldName',
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'onlinePCF',
      Header: t('txt_manage_penalty_fee_online_PCF'),
      accessor: 'onlinePCF',
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'value',
      Header: t('txt_value'),
      accessor: valueAccessor,
      cellBodyProps: { className: 'py-8' }
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      accessor: viewMoreInfo(['moreInfo'], t),
      width: 105,
      cellBodyProps: { className: 'pt-12 pb-8' }
    }
  ];

  return (
    <div className="p-20">
      <Grid columns={columns} data={data} />
    </div>
  );
};

export default SubGridNewMethod;
