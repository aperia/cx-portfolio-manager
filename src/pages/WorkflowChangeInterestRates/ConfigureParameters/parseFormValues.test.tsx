import { ServiceSubjectSection } from 'app/constants/enums';
import mockDate from 'app/_libraries/_dls/test-utils/mockDate';
import parseFormValues from './parseFormValues';

const todayString = '02/23/2022';
const today = new Date(todayString);
describe('ManagePenaltyFeeWorkflow > LateChargesStep > parseFormValues', () => {
  beforeEach(() => {
    mockDate.set(today);
  });

  const dispatchMock = jest.fn();

  it('should return undefined with methodList is undefined', () => {
    const response = parseFormValues(
      { data: { workflowSetupData: [{ id: '1' }] } } as any,
      '1',
      dispatchMock
    );
    expect(response).toEqual({
      isPass: false,
      isSelected: false,
      isValid: false,
      methods: []
    });
  });

  it('should return undefined with stepInfo is undefined', () => {
    const response = parseFormValues(
      {
        data: {
          workflowSetupData: [
            {
              id: '2'
            }
          ],
          configurations: {
            methodList: []
          }
        }
      } as any,
      '1',
      dispatchMock
    );
    expect(response).toEqual(undefined);
  });

  it('should return undefined with methodList does not have ServiceSubjectSection is LC', () => {
    const response = parseFormValues(
      {
        data: {
          workflowSetupData: [
            {
              id: '1'
            }
          ],
          configurations: {
            methodList: [
              {
                id: '123',
                name: '123',
                serviceSubjectSection: ServiceSubjectSection.UIM
              }
            ]
          }
        }
      } as any,
      '1',
      dispatchMock
    );

    expect(response?.methods?.[0]?.serviceSubjectSection).toEqual(undefined);
  });

  it('should return valid data', () => {
    const response = parseFormValues(
      {
        data: {
          workflowSetupData: [
            {
              id: 'configureParametersInterestMethod'
            }
          ],
          configurations: {
            methodList: [
              {
                id: 'configureParametersInterestMethod',
                name: '123',
                serviceSubjectSection: ServiceSubjectSection.UIM
              },
              {
                id: 'configureParametersInterestInventive',
                name: '123',
                serviceSubjectSection: ServiceSubjectSection.UIP
              }
            ]
          }
        }
      } as any,
      'configureParametersInterestMethod',
      dispatchMock
    );

    expect(response?.isValid).toEqual(true);
  });

  it('should return valid data', () => {
    const response = parseFormValues(
      {
        data: {
          workflowSetupData: [
            {
              id: 'configureParametersInterestInventive'
            }
          ],
          configurations: {
            methodList: [
              {
                id: 'configureParametersInterestMethod',
                name: '123',
                serviceSubjectSection: ServiceSubjectSection.UIM
              },
              {
                id: 'configureParametersInterestInventive',
                name: '123',
                serviceSubjectSection: ServiceSubjectSection.UIP
              }
            ]
          }
        }
      } as any,
      'configureParametersInterestInventive',
      dispatchMock
    );

    expect(response?.isValid).toEqual(true);
  });
});
