import { fireEvent, render } from '@testing-library/react';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import React from 'react';
import SubGridNewMethod from './SubGridNewMethod';

jest.mock('app/_libraries/_dof/core', () => ({
  View: ({ formKey, descriptor, onClick }: any) => {
    return (
      <div id={formKey}>
        {formKey}
        <span>{descriptor}</span>
      </div>
    );
  }
}));

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

describe('ManagePenaltyFeeWorkflow > LateChargesStep > SubGridNewMethod', () => {
  const props: any = {
    metadata: {
      defaultCashBaseInterest: [
        {
          code: 'none',
          text: 'none'
        }
      ],
      defaultMerchandiseBaseInterest: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      defaultMaximumCashInterest: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      defaultMaximumMerchandiseInterest: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      defaultMinimumCashInterest: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      defaultMinimumMerchandiseInterest: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      indexRateMethod: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      defaultBaseInterestUsage: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      defaultOverrideInterestLimit: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      defaultMinimumUsage: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      defaultMaximumUsage: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      defaultMaximumInterestUsage: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      incentivePricingUsageCode: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      incentiveBaseInterestUsageRate: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      delinquencyCyclesForTermination: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      minimumMaximumRateOverrideOptions: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      incentivePricingOverrideOption: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      terminateIncentivePricingOnPricingStrategyChange: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      methodOverrideTerminationOption: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      endofIncentiveWarningNotificationTiming: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      endofIncentiveWarningNotificationStatementText: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      messageForProtectedBalanceIncentivePricing: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      indexRateMethodName: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      interestRateRoundingCalculation: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      dailyAPRRoundingCalculation: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      stopCalculatingInterestAfterNumberofDaysDelinquent: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      includeAdditionalCreditBalanceInAverageDailyBalanceCalculation: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      combineMerchandiseAndCashBalancesOnInterestCalculations: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      restartInterestCalculationsAfterClearingDelinquency: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      calculateCompoundInterest: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      overridePromotionalRatesForPricingStrategies: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      monthlyInterestCalculationOptions: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      dailyInterestRoundingOption: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      dailyInterestAmountTruncationOption: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      overrideInterestRatesForProtectedBalance: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      roundNumberOfDaysInCycleForInterestCalculations: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      cycleCodeChangeOptions: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      cyclePeriodChangeCISMemoOption: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      interestMethodOldCash: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      interestMethodCycleToDateCash: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      interestMethodTwoCycleOldCash: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      interestMethodOneCycleOldMerchandise: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      interestMethodCycleToDateMerchandise: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      additionalInterestOnMerchandise1CyclesDelinquent: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      additionalInterestOnMerchandise2CyclesDelinquent: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      additionalInterestOnCash2CyclesDelinquent: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      additionalInterestOnMerchandise3CyclesDelinquent: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      additionalInterestOnCash3CyclesDelinquent: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      automatedMemosForAdditionalInterest: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      useExternalStatusesForAdditionalInterest: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      externalStatus1ForAdditionalInterest: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      externalStatus2ForAdditionalInterest: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      externalStatus3ForAdditionalInterest: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      externalStatus4ForAdditionalInterest: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      externalStatus5ForAdditionalInterest: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      penaltyPricingStatusReasonCodeTable: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      penaltyPricingTimingOptions: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      penaltyPricingStatementMessage1CyclesOldBalance: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      penaltyPricingStatementMessage2CyclesOldBalance: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      penaltyPricingStatementMessage3CyclesOldBalance: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      cureDelinquencyFor1CycleDelinquentBalances: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      cureDelinquencyFor2CycleDelinquentBalances: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      cureDelinquencyFor3CycleDelinquentBalances: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      penaltyPricingOnPromotions: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      citMethodForPenaltyPricing: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      includeZeroBalanceDaysInADBCalculation: [
        {
          code: 'text',
          text: 'text'
        }
      ]
    },
    original: {
      id: 'Info_CycleManagement',
      parameterGroup: 'txt_change_interest_rates_group_base_interest_defaults',
      section: 'txt_change_interest_rates_advanced_parameters'
    },
    method: {
      id: 'id',
      name: 'name',
      modeledFrom: { id: 'modeledForm', versions: [] },
      methodType: 'MODELEDMETHOD',
      comment: 'comment',
      serviceSubjectSection: 'CP IC SP',
      parameters: [
        {
          name: MethodFieldParameterEnum.AdditionalInterestOnCash3CyclesDelinquent,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.AdditionalInterestOnCash3CyclesDelinquent,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.AdditionalInterestOnMerchandise3CyclesDelinquent,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.AdditionalInterestOnCash2CyclesDelinquent,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.AdditionalInterestOnMerchandise2CyclesDelinquent,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.AdditionalInterestOnMerchandise1CyclesDelinquent,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.MaximumRateForMerchandise,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.MinimumRateForMerchandise,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.MaximumRateForCash,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.MinimumRateForCash,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.IncentiveMerchandiseInterestRate,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.IncentiveCashInterestRate,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.DefaultMinimumMerchandiseInterest,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.DefaultMinimumCashInterest,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.DefaultMaximumMerchandiseInterest,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.DefaultMaximumCashInterest,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.DefaultCashBaseInterest,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.DefaultMerchandiseBaseInterest,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.InterestRateRoundingCalculation,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.DailyAPRRoundingCalculation,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.StopCalculatingInterestAfterNumberofDaysDelinquent,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.IncludeAdditionalCreditBalanceInAverageDailyBalanceCalculation,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.CombineMerchandiseAndCashBalancesOnInterestCalculations,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.RestartInterestCalculationsAfterClearingDelinquency,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.CalculateCompoundInterest,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.OverridePromotionalRatesForPricingStrategies,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.MonthlyInterestCalculationOptions,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.DailyInterestRoundingOption,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.DailyInterestAmountTruncationOption,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.OverrideInterestRatesForProtectedBalance,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.MinimumDaysForCycleCodeChange,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.RoundNumberOfDaysInCycleForInterestCalculations,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.CycleCodeChangeOptions,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.CyclePeriodChangeCISMemoOption,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.InterestMethodOldCash,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.InterestMethodCycleToDateMerchandise,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.InterestMethodTwoCycleOldCash,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.InterestMethodOneCycleOldMerchandise,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.InterestMethodCycleToDateCash,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.AutomatedMemosForAdditionalInterest,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.UseExternalStatusesForAdditionalInterest,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.ExternalStatus1ForAdditionalInterest,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.ExternalStatus2ForAdditionalInterest,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.ExternalStatus3ForAdditionalInterest,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.ExternalStatus4ForAdditionalInterest,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.ExternalStatus5ForAdditionalInterest,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.PenaltyPricingTimingOptions,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.PenaltyPricingStatementMessage1CyclesOldBalance,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.PenaltyPricingStatementMessage2CyclesOldBalance,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.PenaltyPricingStatementMessage3CyclesOldBalance,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.CureDelinquencyFor1CycleDelinquentBalances,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.CureDelinquencyFor2CycleDelinquentBalances,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.CureDelinquencyFor3CycleDelinquentBalances,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.PenaltyPricingOnPromotions,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.IncludeZeroBalanceDaysInADBCalculation,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.CITMethodForPenaltyPricing,
          newValue: '12',
          previousValue: ''
        },
        {
          name: MethodFieldParameterEnum.PenaltyPricingStatusReasonCodeTable,
          newValue: '12',
          previousValue: ''
        }
      ],
      // tableName?: string,
      // tableId?: string,
      versionParameters: [],
      tableControlParameters: [],
      decisionElements: []
    },
    onChange: jest.fn()
  };

  it('should render ', () => {
    const wrapper = render(<SubGridNewMethod {...props} />);

    wrapper.container.querySelectorAll('.icon.icon-plus').forEach(element => {
      fireEvent.click(element!);
    });
  });

  it('should render with case default ', () => {
    const newProps = {
      ...props,
      method: { ...props.method, methodType: 'NEWVERSION' }
    };
    render(<SubGridNewMethod {...newProps} />);
  });
  it('should render with case default ', () => {
    const newProps = {
      ...props,
      method: { ...props.method, methodType: 'NEWMETHOD' }
    };
    render(<SubGridNewMethod {...newProps} />);
  });

  it('should render with case default Info_BaseInterestDefaults ', () => {
    const newProps = {
      ...props,
      method: { ...props.method, methodType: 'NEWVERSION' },
      original: { ...props.original, id: 'Info_BaseInterestDefaults' }
    };
    render(<SubGridNewMethod {...newProps} />);
  });
  it('should render with case default Info_BaseInterestUsage ', () => {
    const newProps = {
      ...props,
      method: { ...props.method, methodType: 'NEWVERSION' },
      original: { ...props.original, id: 'Info_BaseInterestUsage' }
    };
    render(<SubGridNewMethod {...newProps} />);
  });
  it('should render with case default Info_IncentivePricingUsage ', () => {
    const newProps = {
      ...props,
      method: { ...props.method, methodType: 'NEWVERSION' },
      original: { ...props.original, id: 'Info_IncentivePricingUsage' }
    };
    render(<SubGridNewMethod {...newProps} />);
  });
  it('should render with case default Info_IncentiveInterestCalculation ', () => {
    const newProps = {
      ...props,
      method: { ...props.method, methodType: 'NEWVERSION' },
      original: { ...props.original, id: 'Info_IncentiveInterestCalculation' }
    };
    render(<SubGridNewMethod {...newProps} />);
  });
  it('should render with case default Info_IncentiveNotifications ', () => {
    const newProps = {
      ...props,
      method: { ...props.method, methodType: 'NEWVERSION' },
      original: { ...props.original, id: 'Info_IncentiveNotifications' }
    };
    render(<SubGridNewMethod {...newProps} />);
  });
  it('should render with case default Info_IncentivePricingTiming ', () => {
    const newProps = {
      ...props,
      method: { ...props.method, methodType: 'NEWVERSION' },
      original: { ...props.original, id: 'Info_IncentivePricingTiming' }
    };
    render(<SubGridNewMethod {...newProps} />);
  });
  it('should render with case default Info_InterestRateMinMax ', () => {
    const newProps = {
      ...props,
      method: { ...props.method, methodType: 'NEWVERSION' },
      original: { ...props.original, id: 'Info_InterestRateMinMax' }
    };
    render(<SubGridNewMethod {...newProps} />);
  });
  it('should render with case default Info_MiscellaneousAdvancedParameters ', () => {
    const newProps = {
      ...props,
      method: { ...props.method, methodType: 'NEWVERSION' },
      original: {
        ...props.original,
        id: 'Info_MiscellaneousAdvancedParameters'
      }
    };
    render(<SubGridNewMethod {...newProps} />);
  });
  it('should render with case default Info_InterestCalculation ', () => {
    const newProps = {
      ...props,
      method: { ...props.method, methodType: 'NEWVERSION' },
      original: { ...props.original, id: 'Info_InterestCalculation' }
    };
    render(<SubGridNewMethod {...newProps} />);
  });
  it('should render with case default Info_InterestMethods ', () => {
    const newProps = {
      ...props,
      method: { ...props.method, methodType: 'NEWVERSION' },
      original: { ...props.original, id: 'Info_InterestMethods' }
    };
    render(<SubGridNewMethod {...newProps} />);
  });
  it('should render with case default Info_PenaltyPricingParameters ', () => {
    const newProps = {
      ...props,
      method: { ...props.method, methodType: 'NEWVERSION' },
      original: { ...props.original, id: 'Info_PenaltyPricingParameters' }
    };
    render(<SubGridNewMethod {...newProps} />);
  });
  it('should render with case default Info_MiscellaneousAdvancedParametersIM ', () => {
    const newProps = {
      ...props,
      method: { ...props.method, methodType: 'NEWVERSION' },
      original: {
        ...props.original,
        id: 'Info_MiscellaneousAdvancedParametersIM'
      }
    };
    render(<SubGridNewMethod {...newProps} />);
  });

  // with new method
  it('should render with case default Info_BaseInterestDefaults ', () => {
    const newProps = {
      ...props,
      method: { ...props.method, methodType: 'NEWMETHOD' },
      original: { ...props.original, id: 'Info_BaseInterestDefaults' }
    };
    render(<SubGridNewMethod {...newProps} />);
  });
  it('should render with case default Info_BaseInterestUsage ', () => {
    const newProps = {
      ...props,
      method: { ...props.method, methodType: 'NEWMETHOD' },
      original: { ...props.original, id: 'Info_BaseInterestUsage' }
    };
    render(<SubGridNewMethod {...newProps} />);
  });
  it('should render with case default Info_IncentivePricingUsage ', () => {
    const newProps = {
      ...props,
      method: { ...props.method, methodType: 'NEWMETHOD' },
      original: { ...props.original, id: 'Info_IncentivePricingUsage' }
    };
    render(<SubGridNewMethod {...newProps} />);
  });
  it('should render with case default Info_IncentiveInterestCalculation ', () => {
    const newProps = {
      ...props,
      method: { ...props.method, methodType: 'NEWMETHOD' },
      original: { ...props.original, id: 'Info_IncentiveInterestCalculation' }
    };
    render(<SubGridNewMethod {...newProps} />);
  });
  it('should render with case default Info_IncentiveNotifications ', () => {
    const newProps = {
      ...props,
      method: { ...props.method, methodType: 'NEWMETHOD' },
      original: { ...props.original, id: 'Info_IncentiveNotifications' }
    };
    render(<SubGridNewMethod {...newProps} />);
  });
  it('should render with case default Info_IncentivePricingTiming ', () => {
    const newProps = {
      ...props,
      method: { ...props.method, methodType: 'NEWMETHOD' },
      original: { ...props.original, id: 'Info_IncentivePricingTiming' }
    };
    render(<SubGridNewMethod {...newProps} />);
  });
  it('should render with case default Info_InterestRateMinMax ', () => {
    const newProps = {
      ...props,
      method: { ...props.method, methodType: 'NEWMETHOD' },
      original: { ...props.original, id: 'Info_InterestRateMinMax' }
    };
    render(<SubGridNewMethod {...newProps} />);
  });
  it('should render with case default Info_MiscellaneousAdvancedParameters ', () => {
    const newProps = {
      ...props,
      method: { ...props.method, methodType: 'NEWMETHOD' },
      original: {
        ...props.original,
        id: 'Info_MiscellaneousAdvancedParameters'
      }
    };
    render(<SubGridNewMethod {...newProps} />);
  });
  it('should render with case default Info_InterestCalculation ', () => {
    const newProps = {
      ...props,
      method: { ...props.method, methodType: 'NEWMETHOD' },
      original: { ...props.original, id: 'Info_InterestCalculation' }
    };
    render(<SubGridNewMethod {...newProps} />);
  });
  it('should render with case default Info_InterestMethods ', () => {
    const newProps = {
      ...props,
      method: { ...props.method, methodType: 'NEWMETHOD' },
      original: { ...props.original, id: 'Info_InterestMethods' }
    };
    render(<SubGridNewMethod {...newProps} />);
  });
  it('should render with case default Info_PenaltyPricingParameters ', () => {
    const newProps = {
      ...props,
      method: { ...props.method, methodType: 'NEWMETHOD' },
      original: { ...props.original, id: 'Info_PenaltyPricingParameters' }
    };
    render(<SubGridNewMethod {...newProps} />);
  });
  it('should render with case default Info_MiscellaneousAdvancedParametersIM ', () => {
    const newProps = {
      ...props,
      method: { ...props.method, methodType: 'NEWMETHOD' },
      original: {
        ...props.original,
        id: 'Info_MiscellaneousAdvancedParametersIM'
      }
    };
    render(<SubGridNewMethod {...newProps} />);
  });
});
