import {
  BadgeColorType,
  MethodFieldParameterEnum,
  ServiceSubjectSection
} from 'app/constants/enums';
import {
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { mapGridExpandCollapse } from 'app/helpers';
import { usePaginationGrid, useUnsavedChangeRegistry } from 'app/hooks';
import {
  Button,
  ColumnType,
  Grid,
  InlineMessage,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty, orderBy } from 'lodash';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import { useSelectElementMetadataForChangeInterest } from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowChangeInterestRates';
import {
  formatBadge,
  formatText,
  formatTruncate
} from 'pages/_commons/Utils/formatGridField';
import Paging from 'pages/_commons/Utils/Paging';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';
import ActionButtons from './ActionButtons';
import AddNewMethodModal from './AddNewMethodModal';
import ConfigSummary from './ConfigSummary';
import MethodListModal from './MethodListModal';
import parseFormValues from './parseFormValues';
import RemoveMethodModal from './RemoveMethodModal';
import SubRowGrid from './SubRowGrid';

export interface ConfigFormValues {
  isValid?: boolean;
  methods?: (WorkflowSetupMethod & { rowId?: number })[];
}
const ConfigureStep: React.FC<WorkflowSetupProps<ConfigFormValues>> = ({
  type: typeMethod,
  stepId,
  selectedStep,
  savedAt,
  formValues,
  setFormValues,
  isStuck
}) => {
  const { t } = useTranslation();
  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;

  const dispatch = useDispatch();
  const [expandedList, setExpandedList] = useState<string[]>([]);
  const [showAddNewMethodModal, setShowAddNewMethodModal] = useState(false);
  const [isCreateNewVersion, setIsCreateNewVersion] = useState(false);
  const [isMethodToModel, setIsMethodToModel] = useState(false);
  const [removeMethodRowId, setRemoveMethodRowId] =
    useState<undefined | number>(undefined);
  const [editMethodRowId, setEditMethodRowId] =
    useState<undefined | number>(undefined);
  const [editMethodBackId, setEditMethodBackId] =
    useState<undefined | number>(undefined);
  const [hasChange, setHasChange] = useState(false);
  const [keepCreateMethodState, setKeepCreateMethodState] =
    useState<any | undefined>(undefined);

  const metadata = useSelectElementMetadataForChangeInterest();

  const methods = useMemo(
    () => orderBy(formValues?.methods || [], ['name'], ['asc']),
    [formValues?.methods]
  );
  const methodNames = useMemo(() => methods.map(i => i.name), [methods]);

  const {
    total,
    currentPage,
    currentPageSize,
    pageData,
    onPageChange,
    onPageSizeChange,
    onResetToDefaultFilter
  } = usePaginationGrid(methods);

  const onRemoveMethod = useCallback((idx: number) => {
    setRemoveMethodRowId(idx);
  }, []);

  const onEditMethod = useCallback((idx: number) => {
    setEditMethodRowId(idx);
  }, []);

  const onClickAddNewMethod = () => {
    setShowAddNewMethodModal(true);
  };
  const onClickMethodToModel = () => {
    setIsMethodToModel(true);
  };
  const onClickCreateNewVersion = () => {
    setIsCreateNewVersion(true);
  };
  const closeMethodListModalModal = () => {
    setIsCreateNewVersion(false);
    setIsMethodToModel(false);
  };

  const unSavedConfigureParameters = () => {
    switch (typeMethod) {
      case 'IP':
        return UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CHANGE_INTEREST_RATES_IP;
      case 'IM':
        return UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CHANGE_INTEREST_RATES_IM;
      default:
        return UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CHANGE_INTEREST_RATES_ID;
    }
  };

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName: unSavedConfigureParameters(),
      priority: 1
    },
    [hasChange]
  );

  const serviceSubjectSection = useCallback(() => {
    switch (typeMethod) {
      case 'IP':
        return ServiceSubjectSection.UIP;

      case 'IM':
        return ServiceSubjectSection.UIM;

      default:
        return ServiceSubjectSection.UID;
    }
  }, [typeMethod]);

  const methodTypeToText = useCallback(
    (type: WorkflowSetupMethodType) => {
      switch (type) {
        case 'NEWVERSION':
          return t('txt_change_interest_rates_version_created');
        case 'MODELEDMETHOD':
          return t('txt_change_interest_rates_method_modeled');
      }
      return t('txt_change_interest_rates_method_created');
    },
    [t]
  );

  const columns: ColumnType[] = useMemo(
    () => [
      {
        id: 'methodName',
        Header: t('txt_method_name'),
        accessor: formatText(['name']),
        width: 105
      },
      {
        id: 'modelOrCreateFrom',
        Header: t('txt_change_interest_rates_modeled_or_create_from'),
        accessor: formatText(['modeledFrom', 'name']),
        width: 120
      },
      {
        id: 'comment',
        Header: t('txt_comment_area'),
        accessor: formatTruncate(['comment']),
        width: 312
      },
      {
        id: 'methodType',
        Header: t('txt_change_interest_rates_action_taken'),
        accessor: (data, idx) =>
          formatBadge(['methodType'], {
            colorType: BadgeColorType.MethodType,
            noBorder: true
          })(
            {
              ...data,
              methodType: methodTypeToText(data.methodType)
            },
            idx
          ),
        width: 153
      },
      {
        id: 'actions',
        Header: t('txt_actions'),
        accessor: (data: any) => (
          <div className="d-flex justify-content-center">
            <Button
              size="sm"
              variant="outline-primary"
              onClick={() => onEditMethod(data.rowId)}
            >
              {t('txt_edit')}
            </Button>
            <Button
              size="sm"
              variant="outline-danger"
              className="ml-8"
              onClick={() => onRemoveMethod(data.rowId)}
            >
              {t('txt_delete')}
            </Button>
          </div>
        ),
        className: 'text-center',
        cellBodyProps: { className: 'py-8' },
        width: 142
      }
    ],
    [t, methodTypeToText, onEditMethod, onRemoveMethod]
  );

  useEffect(() => {
    switch (typeMethod) {
      case 'IM':
        dispatch(
          actionsWorkflowSetup.getWorkflowMetadata([
            MethodFieldParameterEnum.InterestRateRoundingCalculation,
            MethodFieldParameterEnum.DailyAPRRoundingCalculation,
            MethodFieldParameterEnum.StopCalculatingInterestAfterNumberofDaysDelinquent,
            MethodFieldParameterEnum.IncludeAdditionalCreditBalanceInAverageDailyBalanceCalculation,
            MethodFieldParameterEnum.CombineMerchandiseAndCashBalancesOnInterestCalculations,
            MethodFieldParameterEnum.RestartInterestCalculationsAfterClearingDelinquency,
            MethodFieldParameterEnum.CalculateCompoundInterest,
            MethodFieldParameterEnum.OverridePromotionalRatesForPricingStrategies,
            MethodFieldParameterEnum.MonthlyInterestCalculationOptions,
            MethodFieldParameterEnum.DailyInterestRoundingOption,
            MethodFieldParameterEnum.DailyInterestAmountTruncationOption,
            MethodFieldParameterEnum.OverrideInterestRatesForProtectedBalance,
            MethodFieldParameterEnum.MinimumDaysForCycleCodeChange,
            MethodFieldParameterEnum.RoundNumberOfDaysInCycleForInterestCalculations,
            MethodFieldParameterEnum.CycleCodeChangeOptions,
            MethodFieldParameterEnum.CyclePeriodChangeCISMemoOption,
            MethodFieldParameterEnum.InterestMethodOldCash,
            MethodFieldParameterEnum.InterestMethodCycleToDateMerchandise,
            MethodFieldParameterEnum.InterestMethodTwoCycleOldCash,
            MethodFieldParameterEnum.InterestMethodOneCycleOldMerchandise,
            MethodFieldParameterEnum.InterestMethodCycleToDateCash,
            MethodFieldParameterEnum.AutomatedMemosForAdditionalInterest,
            MethodFieldParameterEnum.UseExternalStatusesForAdditionalInterest,
            MethodFieldParameterEnum.ExternalStatus1ForAdditionalInterest,
            MethodFieldParameterEnum.ExternalStatus2ForAdditionalInterest,
            MethodFieldParameterEnum.ExternalStatus3ForAdditionalInterest,
            MethodFieldParameterEnum.ExternalStatus4ForAdditionalInterest,
            MethodFieldParameterEnum.ExternalStatus5ForAdditionalInterest,
            MethodFieldParameterEnum.PenaltyPricingTimingOptions,
            MethodFieldParameterEnum.PenaltyPricingStatementMessage1CyclesOldBalance,
            MethodFieldParameterEnum.PenaltyPricingStatementMessage2CyclesOldBalance,
            MethodFieldParameterEnum.PenaltyPricingStatementMessage3CyclesOldBalance,
            MethodFieldParameterEnum.CureDelinquencyFor1CycleDelinquentBalances,
            MethodFieldParameterEnum.CureDelinquencyFor2CycleDelinquentBalances,
            MethodFieldParameterEnum.CureDelinquencyFor3CycleDelinquentBalances,
            MethodFieldParameterEnum.PenaltyPricingOnPromotions,
            MethodFieldParameterEnum.CITMethodForPenaltyPricing,
            MethodFieldParameterEnum.IncludeZeroBalanceDaysInADBCalculation,
            MethodFieldParameterEnum.PenaltyPricingStatusReasonCodeTable
          ])
        );
        break;
      case 'IP':
        dispatch(
          actionsWorkflowSetup.getWorkflowMetadata([
            MethodFieldParameterEnum.IndexRateMethodName,
            MethodFieldParameterEnum.IncentiveBaseInterestUsageRate,
            MethodFieldParameterEnum.IncentivePricingUsageCode,
            MethodFieldParameterEnum.DelinquencyCyclesForTermination,
            MethodFieldParameterEnum.MinimumMaximumRateOverrideOptions,
            MethodFieldParameterEnum.IncentivePricingOverrideOption,
            MethodFieldParameterEnum.TerminateIncentivePricingOnPricingStrategyChange,
            MethodFieldParameterEnum.MethodOverrideTerminationOption,
            MethodFieldParameterEnum.EndofIncentiveWarningNotificationStatementText,
            MethodFieldParameterEnum.EndofIncentiveWarningNotificationTiming,
            MethodFieldParameterEnum.IncentivePricingNumberOfMonths,
            MethodFieldParameterEnum.MessageForProtectedBalanceIncentivePricing
          ])
        );
        break;
      default:
        dispatch(
          actionsWorkflowSetup.getWorkflowMetadata([
            MethodFieldParameterEnum.IndexRateMethod,
            MethodFieldParameterEnum.DefaultBaseInterestUsage,
            MethodFieldParameterEnum.DefaultMaximumUsage,
            MethodFieldParameterEnum.DefaultMinimumUsage,
            MethodFieldParameterEnum.DefaultMaximumInterestUsage
          ])
        );
        break;
    }
  }, [dispatch, typeMethod]);

  useEffect(() => {
    const isValid = (formValues?.methods?.length || 0) > 0;
    if (formValues.isValid === isValid) return;

    keepRef.current.setFormValues({ isValid });
  }, [formValues]);

  useEffect(() => {
    if (stepId !== selectedStep) {
      setExpandedList([]);
    }
    setHasChange(false);
  }, [stepId, selectedStep, savedAt]);

  useEffect(() => {
    if (stepId !== selectedStep) {
      onResetToDefaultFilter();
      return;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [stepId, selectedStep, savedAt]);

  const handleOnExpand = (dataKeyList: string[]) => {
    setExpandedList(isEmpty(dataKeyList) ? [] : dataKeyList);
  };

  const renderGrid = useMemo(() => {
    if (isEmpty(methods)) return;
    return (
      <div className="pt-16">
        <Grid
          columns={columns}
          data={pageData}
          subRow={({ original }: any) => (
            <SubRowGrid
              original={original}
              metadata={metadata}
              type={typeMethod}
            />
          )}
          togglable
          dataItemKey="rowId"
          toggleButtonConfigList={methods.map(
            mapGridExpandCollapse('rowId', t)
          )}
          expandedItemKey={'rowId'}
          expandedList={expandedList}
          onExpand={handleOnExpand}
          scrollable
        />
        {total > 10 && (
          <Paging
            totalItem={total}
            pageSize={currentPageSize}
            page={currentPage}
            onChangePage={onPageChange}
            onChangePageSize={onPageSizeChange}
          />
        )}
      </div>
    );
  }, [
    columns,
    currentPage,
    currentPageSize,
    expandedList,
    metadata,
    methods,
    onPageChange,
    onPageSizeChange,
    pageData,
    t,
    total,
    typeMethod
  ]);

  return (
    <div>
      <div className="mt-8 pb-24 border-bottom color-grey">
        <p>{t('txt_change_interest_rates_configure')}</p>
      </div>
      {isStuck && (
        <InlineMessage className="mb-0 mt-24" variant="danger" withIcon>
          {t('txt_step_stuck_move_forward_message')}
        </InlineMessage>
      )}
      <div className="mt-24">
        <ActionButtons
          small={!isEmpty(methods)}
          title="Method List"
          onClickAddNewMethod={onClickAddNewMethod}
          onClickMethodToModel={onClickMethodToModel}
          onClickCreateNewVersion={onClickCreateNewVersion}
        />
      </div>
      {renderGrid}
      {removeMethodRowId !== undefined && (
        <RemoveMethodModal
          id="removeMethod"
          show
          methodName={methods.find(i => i.rowId === removeMethodRowId)?.name}
          onClose={method => {
            if (method) {
              setFormValues({
                methods: methods.filter(i => i.rowId !== removeMethodRowId)
              });
              setHasChange(true);
            }
            setRemoveMethodRowId(undefined);
          }}
        />
      )}
      {editMethodRowId !== undefined && (
        <AddNewMethodModal
          id="addNewMethod"
          show
          method={methods.find(i => i.rowId === editMethodRowId)}
          methodNames={methodNames}
          type={typeMethod}
          onClose={(method, isBack, keepState) => {
            if (method) {
              method.serviceSubjectSection = serviceSubjectSection();
              const newMethds = methods.map(item => {
                if (item.rowId === editMethodRowId) return method;
                return item;
              });
              setFormValues({ methods: newMethds });
              setHasChange(true);

              /* istanbul ignore else */
              if (isBack) {
                setEditMethodBackId(editMethodRowId);
                setKeepCreateMethodState(keepState);
              }
            }
            setEditMethodRowId(undefined);
          }}
        />
      )}
      {editMethodBackId && (
        <MethodListModal
          id="editMethodFromModel"
          show
          methodNames={methodNames}
          isCreateNewVersion={
            methods.find(i => i.rowId === editMethodBackId)!.methodType ===
            'NEWVERSION'
          }
          defaultMethod={methods.find(i => i.rowId === editMethodBackId)}
          keepState={keepCreateMethodState}
          onClose={method => {
            if (method) {
              method.serviceSubjectSection = serviceSubjectSection();
              const newMethds = methods.map(item => {
                if (item.rowId === editMethodBackId) return method;
                return item;
              });
              setFormValues({ methods: newMethds });
              setHasChange(true);
            }
            setEditMethodBackId(undefined);
            setKeepCreateMethodState(undefined);
          }}
          type={typeMethod}
        />
      )}
      {(isCreateNewVersion || isMethodToModel) && (
        <MethodListModal
          id="addMethodFromModel"
          show
          methodNames={methodNames}
          isCreateNewVersion={isCreateNewVersion}
          onClose={newMethod => {
            if (newMethod) {
              const rowId = Date.now();
              setFormValues({
                methods: [
                  {
                    ...newMethod,
                    rowId,
                    serviceSubjectSection: serviceSubjectSection()
                  },
                  ...methods
                ]
              });
              setExpandedList([rowId] as any);
              setHasChange(true);
            }
            closeMethodListModalModal();
          }}
          type={typeMethod}
        />
      )}
      {showAddNewMethodModal && (
        <AddNewMethodModal
          id="addNewMethod"
          show
          methodNames={methodNames}
          onClose={method => {
            if (method) {
              const rowId = Date.now();
              setFormValues({
                methods: [
                  {
                    ...method,
                    rowId,
                    serviceSubjectSection: serviceSubjectSection()
                  },
                  ...methods
                ]
              });
              setExpandedList([rowId] as any);
              setHasChange(true);
            }
            setShowAddNewMethodModal(false);
          }}
          type={typeMethod}
        />
      )}
    </div>
  );
};

const ExtraStaticConfigureStep = ConfigureStep as WorkflowSetupStaticProp;

ExtraStaticConfigureStep.summaryComponent = ConfigSummary;
ExtraStaticConfigureStep.defaultValues = {
  isValid: false,
  methods: []
};
ExtraStaticConfigureStep.parseFormValues = parseFormValues;

export default ExtraStaticConfigureStep;
