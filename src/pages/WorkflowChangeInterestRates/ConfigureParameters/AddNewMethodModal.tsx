import ModalRegistry from 'app/components/ModalRegistry';
import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import TextAreaCountDown from 'app/components/TextAreaCountDown';
import {
  METHOD_CHANGES_INTEREST_RATES_DEFAULT,
  METHOD_CHANGES_INTEREST_RATES_FIELDS
} from 'app/constants/mapping';
import {
  unsavedChangesProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import {
  mapGridExpandCollapse,
  matchSearchValue,
  methodParamsToObject,
  objectToMethodParams
} from 'app/helpers';
import {
  useFormValidations,
  useUnsavedChangeRegistry,
  useUnsavedChangesRedirect
} from 'app/hooks';
import {
  Button,
  Grid,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  TextBox,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty } from 'lodash';
import isEqual from 'lodash.isequal';
import isFunction from 'lodash.isfunction';
import { useSelectElementMetadataForChangeInterest } from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowChangeInterestRates';
import ClearAndResetButton from 'pages/_commons/Utils/ClearAndResetButton';
import React, { useEffect, useMemo, useRef, useState } from 'react';
import newMethodColumns from './newMethodColumns';
import {
  parameterGroup,
  parameterList,
  ParameterListIds
} from './newMethodParameterList';
import SubGridNewMethod from './SubGridNewMethod';

interface IKeepState {
  searchValue: string;
  expandedList: ParameterListIds[];
}
interface IProps {
  id: string;
  show: boolean;
  method?: WorkflowSetupMethod;
  draftMethod?: WorkflowSetupMethod;
  keepState?: IKeepState;
  methodNames?: string[];
  onClose?: (
    method?: WorkflowSetupMethod,
    isBack?: boolean,
    keepState?: IKeepState
  ) => void;
  type?: string;
}

const AddNewMethodModal: React.FC<IProps> = ({
  id,
  show,
  method: methodProp = { methodType: 'NEWMETHOD' } as WorkflowSetupMethod,
  draftMethod,
  methodNames = [],
  keepState,
  onClose,
  type
}) => {
  const { t } = useTranslation();
  const redirect = useUnsavedChangesRedirect();
  const metadata = useSelectElementMetadataForChangeInterest();

  const [initialMethod, setInitialMethod] = useState<
    WorkflowSetupMethodObjectParams & { rowId?: number }
  >(methodParamsToObject(methodProp));
  const [method, setMethod] = useState(
    draftMethod ? methodParamsToObject(draftMethod) : initialMethod
  );
  const [searchValue, setSearchValue] = useState(keepState?.searchValue || '');

  const [expandedList, setExpandedList] = useState<ParameterListIds[]>(
    keepState?.expandedList || []
  );

  const expandedDefault: () => ParameterListIds = () => {
    switch (type) {
      case 'IM':
        return 'Info_InterestCalculation';
      case 'IP':
        return 'Info_IncentivePricingUsage';
      default:
        return 'Info_BaseInterestDefaults';
    }
  };

  useEffect(() => {
    if (!keepState?.expandedList) {
      setExpandedList([expandedDefault()]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [type]);

  const isNewVersion = useMemo(
    () => method.methodType === 'NEWVERSION',
    [method.methodType]
  );

  const isNewMethod = useMemo(
    () => method.methodType === 'NEWMETHOD',
    [method.methodType]
  );

  const parameters = useMemo(
    () =>
      parameterList(type).filter(p =>
        parameterGroup[p.id].some(g =>
          matchSearchValue(
            [g.fieldName, g.moreInfoText, g.onlinePCF],
            searchValue
          )
        )
      ),
    [searchValue, type]
  );

  const unSavedConfigureParameters = () => {
    switch (type) {
      case 'IM':
        return UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CHANGE_INTEREST_RATES_IM__METHOD_DETAILS;
      case 'IP':
        return UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CHANGE_INTEREST_RATES_IP__METHOD_DETAILS;
      default:
        return UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CHANGE_INTEREST_RATES_ID__METHOD_DETAILS;
    }
  };

  useUnsavedChangeRegistry(
    {
      ...unsavedChangesProps,
      formName: unSavedConfigureParameters(),
      priority: 1
    },
    [
      !isEqual(initialMethod.name?.trim() || '', method.name?.trim() || ''),
      !isEqual(
        initialMethod.comment?.trim() || '',
        method.comment?.trim() || ''
      ),
      METHOD_CHANGES_INTEREST_RATES_FIELDS.some(f => {
        return !isEqual(
          initialMethod[f]?.trim() || '',
          method[f]?.trim() || ''
        );
      })
    ]
  );

  const currentErrors = useMemo(
    () =>
      ({
        name:
          (!method?.name?.trim() && {
            status: true,
            message: t('txt_required_validation', {
              fieldName: t('txt_method_name')
            })
          }) ||
          (initialMethod.name?.trim() !== method?.name?.trim() &&
            methodNames.includes(method?.name?.trim()) && {
              status: true,
              message: t(
                'txt_manage_penalty_fee_validation_method_name_must_be_unique'
              )
            }) ||
          (!isNewMethod &&
            !isNewVersion &&
            method?.name?.trim() === method.modeledFrom?.name && {
              status: true,
              message: t(
                'txt_manage_penalty_fee_validation_method_name_must_be_unique'
              )
            })
      } as Record<keyof WorkflowSetupMethodObjectParams, IFormError>),
    [method, t, initialMethod, methodNames, isNewMethod, isNewVersion]
  );
  const [touches, errors, setTouched] =
    useFormValidations<keyof WorkflowSetupMethodObjectParams>(currentErrors);

  const isValid = useMemo(
    () =>
      !errors.name?.status &&
      (!currentErrors.name?.status || touches.name?.touched),
    [errors, touches, currentErrors]
  );

  useEffect(() => {
    let defaultParamObj: Record<string, string> = {};
    if (
      initialMethod.methodType === 'NEWMETHOD' &&
      initialMethod.rowId === undefined
    ) {
      const paramMapping = Object.keys(
        METHOD_CHANGES_INTEREST_RATES_DEFAULT
      ).reduce((pre, next) => {
        pre[next] = METHOD_CHANGES_INTEREST_RATES_DEFAULT[next];
        return pre;
      }, {} as Record<string, string>);
      defaultParamObj = Object.assign({}, paramMapping);
      defaultParamObj.methodType = 'NEWMETHOD';
      setMethod(defaultParamObj as any);
      setInitialMethod(defaultParamObj as any);
    }
  }, [initialMethod.methodType, initialMethod.rowId]);

  const simpleSearchRef = useRef<any>(null);
  useEffect(() => {
    if (!searchValue && simpleSearchRef?.current) {
      simpleSearchRef.current.clear();
    }
  }, [searchValue]);

  const handleBack = () => {
    const methodForm = objectToMethodParams(
      [...METHOD_CHANGES_INTEREST_RATES_FIELDS],
      method
    );
    methodForm.versionParameters = !!methodForm.versionParameters
      ? methodForm.versionParameters
      : methodForm.parameters;

    isFunction(onClose) &&
      onClose(methodForm, true, { searchValue, expandedList });
  };

  const handleClose = () => {
    redirect({
      onConfirm: () => isFunction(onClose) && onClose(),
      formsWatcher: [unSavedConfigureParameters()]
    });
  };

  const handleAddMethod = () => {
    const methodForm = objectToMethodParams(
      [...METHOD_CHANGES_INTEREST_RATES_FIELDS],
      method
    );
    methodForm.versionParameters = !!methodForm.versionParameters
      ? methodForm.versionParameters
      : methodForm.parameters;

    isFunction(onClose) && onClose(methodForm);
  };

  const handleFormChange =
    (
      field: keyof WorkflowSetupMethodObjectParams & { optionalTiers?: string },
      isRefData?: boolean
    ) =>
    (e: any) => {
      let value = isRefData ? e.target?.value?.code : e.target?.value;
      value = field === 'name' ? value?.toUpperCase() : value;
      setMethod(method => ({ ...method, [field]: value }));
    };

  const handleSearch = (val: string) => {
    setSearchValue(val);
    setExpandedList(parameters.map(p => p.id));
  };

  const handleClearFilter = () => {
    setSearchValue('');
    setExpandedList([expandedDefault()]);
  };

  return (
    <ModalRegistry
      lg
      id={id}
      show={show}
      onAutoClosedAll={handleClose}
      onAutoClosed={handleClearFilter}
    >
      <ModalHeader border closeButton onHide={handleClose}>
        <ModalTitle>{t('txt_manage_penalty_fee_method_details')}</ModalTitle>
      </ModalHeader>
      <ModalBody>
        <div>
          <p>
            To create a new CP IC {type} method in the online Product Control
            File, enter a unique name for the method and a comment. Then, enter
            values into the parameter list. When finished, click <b>Save</b> to
            continue.
          </p>
        </div>
        <div>
          <div className="row mt-16">
            {!isNewMethod && (
              <div className="col-6 col-lg-4">
                <TextBox
                  id="addNewMethod__selectedMethod"
                  readOnly
                  value={method?.modeledFrom?.name || ''}
                  maxLength={8}
                  label={t('txt_manage_penalty_fee_selected_method')}
                />
              </div>
            )}
            <div className="col-6 col-lg-4 mr-lg-4">
              <TextBox
                id="addNewMethod__methodName"
                readOnly={isNewVersion}
                value={method?.name || ''}
                onChange={handleFormChange('name')}
                required
                maxLength={8}
                label={t('txt_method_name')}
                onFocus={setTouched('name')}
                onBlur={setTouched('name', true)}
                error={errors.name}
              />
            </div>
            <div className="mt-16 col-12 col-lg-8">
              <TextAreaCountDown
                id="addNewMethod__comment"
                label={t('txt_comment_area')}
                value={method?.comment || ''}
                onChange={handleFormChange('comment')}
                rows={4}
                maxLength={240}
              />
            </div>
          </div>
          <div className="mt-24">
            <div className="d-flex align-items-center justify-content-between mb-16">
              <h5>{t('txt_parameter_list')}</h5>
              <SimpleSearch
                ref={simpleSearchRef}
                placeholder={t('txt_type_to_search')}
                defaultValue={searchValue}
                onSearch={handleSearch}
                popperElement={
                  <>
                    <p className="color-grey">{t('txt_search_description')}</p>
                    <ul className="search-field-item list-unstyled">
                      <li className="mt-16">{t('txt_business_name')}</li>
                      <li className="mt-16">{t('txt_more_info')}</li>
                      <li className="mt-16">
                        {t('txt_change_interest_rates_online_PCF')}
                      </li>
                    </ul>
                  </>
                }
              />
            </div>
            {searchValue && !isEmpty(parameters) && (
              <div className="d-flex justify-content-end mb-16 mr-n8">
                <ClearAndResetButton
                  small
                  onClearAndReset={handleClearFilter}
                />
              </div>
            )}
            {isEmpty(parameters) && (
              <div className="d-flex flex-column justify-content-center mt-40 mb-32">
                <NoDataFound
                  id="newMethod_notfound"
                  hasSearch
                  title={t('txt_no_results_found')}
                  linkTitle={t('txt_clear_and_reset')}
                  onLinkClicked={handleClearFilter}
                />
              </div>
            )}
            {!isEmpty(parameters) && (
              <Grid
                columns={newMethodColumns(t)}
                data={parameters}
                subRow={({ original }: MagicKeyValue) => (
                  <SubGridNewMethod
                    original={original}
                    method={method}
                    searchValue={searchValue}
                    onChange={handleFormChange}
                    metadata={metadata}
                  />
                )}
                dataItemKey="id"
                togglable
                expandedItemKey="id"
                expandedList={expandedList}
                onExpand={setExpandedList as any}
                toggleButtonConfigList={parameters.map(
                  mapGridExpandCollapse('id', t)
                )}
              />
            )}
          </div>
        </div>
      </ModalBody>
      <ModalFooter
        cancelButtonText={t('txt_cancel')}
        okButtonText={t('txt_save')}
        onCancel={handleClose}
        onOk={handleAddMethod}
        disabledOk={!isValid}
      >
        {!isNewMethod && (
          <Button
            className="mr-auto ml-n8"
            variant="outline-primary"
            onClick={handleBack}
          >
            {t('txt_back')}
          </Button>
        )}
      </ModalFooter>
    </ModalRegistry>
  );
};

export default AddNewMethodModal;
