import { mapGridExpandCollapse } from 'app/helpers';
import { Grid, useTranslation } from 'app/_libraries/_dls';
import React, { useEffect, useMemo, useState } from 'react';
import newMethodColumns from './newMethodColumns';
import { parameterList, ParameterListIds } from './newMethodParameterList';
import SubRowGridInfo from './SubRowGridInfo';

export interface SubGridProps {
  original: any;
  metadata: {
    defaultCashBaseInterest: RefData[];
    defaultMerchandiseBaseInterest: RefData[];
    defaultMaximumCashInterest: RefData[];
    defaultMaximumMerchandiseInterest: RefData[];
    defaultMinimumCashInterest: RefData[];
    defaultMinimumMerchandiseInterest: RefData[];
    indexRateMethod: RefData[];
    defaultBaseInterestUsage: RefData[];
    defaultOverrideInterestLimit: RefData[];
    defaultMinimumUsage: RefData[];
    defaultMaximumUsage: RefData[];
    defaultMaximumInterestUsage: RefData[];
    incentivePricingUsageCode: RefData[];
    incentiveBaseInterestUsageRate: RefData[];
    delinquencyCyclesForTermination: RefData[];
    minimumMaximumRateOverrideOptions: RefData[];
    incentivePricingOverrideOption: RefData[];
    terminateIncentivePricingOnPricingStrategyChange: RefData[];
    methodOverrideTerminationOption: RefData[];
    endofIncentiveWarningNotificationTiming: RefData[];
    endofIncentiveWarningNotificationStatementText: RefData[];
    incentivePricingNumberOfMonths: RefData[];
    messageForProtectedBalanceIncentivePricing: RefData[];
    indexRateMethodName: RefData[];
    interestRateRoundingCalculation: RefData[];
    dailyAPRRoundingCalculation: RefData[];
    stopCalculatingInterestAfterNumberofDaysDelinquent: RefData[];
    includeAdditionalCreditBalanceInAverageDailyBalanceCalculation: RefData[];
    combineMerchandiseAndCashBalancesOnInterestCalculations: RefData[];
    restartInterestCalculationsAfterClearingDelinquency: RefData[];
    calculateCompoundInterest: RefData[];
    overridePromotionalRatesForPricingStrategies: RefData[];
    monthlyInterestCalculationOptions: RefData[];
    dailyInterestRoundingOption: RefData[];
    dailyInterestAmountTruncationOption: RefData[];
    overrideInterestRatesForProtectedBalance: RefData[];
    minimumDaysForCycleCodeChange: RefData[];
    roundNumberOfDaysInCycleForInterestCalculations: RefData[];
    cycleCodeChangeOptions: RefData[];
    cyclePeriodChangeCISMemoOption: RefData[];
    interestMethodOldCash: RefData[];
    interestMethodCycleToDateCash: RefData[];
    interestMethodTwoCycleOldCash: RefData[];
    interestMethodOneCycleOldMerchandise: RefData[];
    interestMethodCycleToDateMerchandise: RefData[];
    additionalInterestOnMerchandise1CyclesDelinquent: RefData[];
    additionalInterestOnMerchandise2CyclesDelinquent: RefData[];
    additionalInterestOnCash2CyclesDelinquent: RefData[];
    additionalInterestOnMerchandise3CyclesDelinquent: RefData[];
    additionalInterestOnCash3CyclesDelinquent: RefData[];
    automatedMemosForAdditionalInterest: RefData[];
    useExternalStatusesForAdditionalInterest: RefData[];
    externalStatus1ForAdditionalInterest: RefData[];
    externalStatus2ForAdditionalInterest: RefData[];
    externalStatus3ForAdditionalInterest: RefData[];
    externalStatus4ForAdditionalInterest: RefData[];
    externalStatus5ForAdditionalInterest: RefData[];
    penaltyPricingStatusReasonCodeTable: RefData[];
    penaltyPricingTimingOptions: RefData[];
    penaltyPricingStatementMessage1CyclesOldBalance: RefData[];
    penaltyPricingStatementMessage2CyclesOldBalance: RefData[];
    penaltyPricingStatementMessage3CyclesOldBalance: RefData[];
    cureDelinquencyFor1CycleDelinquentBalances: RefData[];
    cureDelinquencyFor2CycleDelinquentBalances: RefData[];
    cureDelinquencyFor3CycleDelinquentBalances: RefData[];
    penaltyPricingOnPromotions: RefData[];
    citMethodForPenaltyPricing: RefData[];
    includeZeroBalanceDaysInADBCalculation: RefData[];
  };
  type?: string;
}
const SubRowGrid: React.FC<SubGridProps> = ({
  original: method,
  metadata,
  type
}) => {
  const { t } = useTranslation();
  const [expandedList, setExpandedList] = useState<ParameterListIds[]>([]);

  useEffect(() => {
    let expanded: ParameterListIds;
    switch (type) {
      case 'IM':
        expanded = 'Info_InterestCalculation';
        break;
      case 'IP':
        expanded = 'Info_IncentivePricingUsage';
        break;
      default:
        expanded = 'Info_BaseInterestDefaults';
        break;
    }
    setExpandedList([expanded]);
  }, [type]);

  const dataParameterList = useMemo(() => parameterList(type), [type]);

  return (
    <div className="p-20 overflow-hidden">
      <Grid
        columns={newMethodColumns(t)}
        data={dataParameterList}
        subRow={({ original }: MagicKeyValue) => (
          <SubRowGridInfo
            method={method}
            original={original}
            metadata={metadata}
          />
        )}
        dataItemKey="id"
        togglable
        expandedItemKey="id"
        expandedList={expandedList}
        onExpand={setExpandedList as any}
        toggleButtonConfigList={dataParameterList.map(
          mapGridExpandCollapse('id', t)
        )}
      />
    </div>
  );
};

export default SubRowGrid;
