import { fireEvent, render, RenderResult } from '@testing-library/react';
import { queryAllByClass } from 'app/_libraries/_dls/test-utils';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowChangeInterestRates';
import React from 'react';
import ConfigSummary from './ConfigSummary';

jest.mock('./SubRowGrid', () => ({
  __esModule: true,
  default: () => <div>SubRowGrid_component</div>
}));

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const MockSelectWorkflowMethodsSelector = jest.spyOn(
  WorkflowSetup,
  'useSelectElementMetadataForChangeInterest'
);

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <ConfigSummary {...props} />
    </div>
  );
};

describe('WorkflowChangeInterestRates > ConfigSummary', () => {
  beforeEach(() => {
    MockSelectWorkflowMethodsSelector.mockReturnValue({} as any);
  });

  afterEach(() => {
    MockSelectWorkflowMethodsSelector.mockReset();
  });

  it('Should render a grid with no content', () => {
    const props = {
      stepId: '',
      formValues: {}
    } as any;

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    expect(body?.children.length).toEqual(0);
  });

  it('Should render a grid with content', () => {
    const props = {
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: 'CP IC IP',
            methodType: 'NEWMETHOD'
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: 'CP IC IM',
            methodType: 'MODELEDMETHOD'
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: 'CP IC ID',
            methodType: 'NEWVERSION'
          }
        ]
      }
    } as any;

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    expect(body?.children.length).toEqual(3);
  });

  it('Should call edit function', () => {
    const mockFn = jest.fn();
    const props = {
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: 'CP IC IP',
            methodType: 'NEWMETHOD'
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: 'CP IC IM',
            methodType: 'MODELEDMETHOD'
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: 'CP IC ID',
            methodType: 'NEWVERSION'
          }
        ]
      },
      onEditStep: mockFn
    } as any;

    const wrapper = renderComponent(props);
    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(mockFn).toHaveBeenCalled();
  });

  it('Should show sub row', () => {
    const mockFn = jest.fn();
    const props = {
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: 'CP IC IP',
            methodType: 'NEWMETHOD',
            rowId: 123
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: 'CP IC IM',
            methodType: 'MODELEDMETHOD',
            rowId: 125
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: 'CP IC ID',
            methodType: 'NEWVERSION',
            rowId: 234
          }
        ]
      },
      onEditStep: mockFn
    } as any;

    const wrapper = renderComponent(props);

    const icon = queryAllByClass(wrapper.container, /icon-plus/);
    fireEvent.click(icon[0] as Element);
    //Re-click
    fireEvent.click(icon[0] as Element);
    fireEvent.click(icon[1] as Element);
    fireEvent.click(icon[2] as Element);
  });
});
