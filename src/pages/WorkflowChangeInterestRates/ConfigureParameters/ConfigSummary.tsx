import { BadgeColorType } from 'app/constants/enums';
import { mapGridExpandCollapse } from 'app/helpers';
import { usePaginationGrid } from 'app/hooks';
import { Button, ColumnType, Grid, useTranslation } from 'app/_libraries/_dls';
import { isFunction, orderBy } from 'lodash';
import isEmpty from 'lodash.isempty';
import { useSelectElementMetadataForChangeInterest } from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowChangeInterestRates';
import {
  formatBadge,
  formatText,
  formatTruncate
} from 'pages/_commons/Utils/formatGridField';
import Paging from 'pages/_commons/Utils/Paging';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { ConfigFormValues } from '.';
import SubRowGrid from './SubRowGrid';

const ConfigSummary: React.FC<WorkflowSetupSummaryProps<ConfigFormValues>> = ({
  onEditStep,
  formValues,
  stepId,
  selectedStep
}) => {
  const [expandedList, setExpandedList] = useState<string[]>([]);
  const { t } = useTranslation();
  const metadata = useSelectElementMetadataForChangeInterest();

  const methods = useMemo(
    () => orderBy(formValues?.methods || [], ['name'], ['asc']),
    [formValues?.methods]
  );

  const {
    total,
    currentPage,
    currentPageSize,
    pageData,
    onPageChange,
    onPageSizeChange,
    onResetToDefaultFilter
  } = usePaginationGrid(methods);

  const typeMethod = (original: any) => {
    switch (original?.serviceSubjectSection) {
      case 'CP IC IP':
        return 'IP';
      case 'CP IC IM':
        return 'IM';
      default:
        return 'ID';
    }
  };

  const methodTypeToText = useCallback(
    (type: WorkflowSetupMethodType) => {
      switch (type) {
        case 'NEWVERSION':
          return t('txt_change_interest_rates_version_created');
        case 'MODELEDMETHOD':
          return t('txt_change_interest_rates_method_modeled');
      }
      return t('txt_change_interest_rates_method_created');
    },
    [t]
  );

  const columns: ColumnType[] = useMemo(
    () => [
      {
        id: 'methodName',
        Header: t('txt_method_name'),
        accessor: formatText(['name']),
        width: 105
      },
      {
        id: 'modelOrCreateFrom',
        Header: t('txt_manage_penalty_fee_modeled_or_create_from'),
        accessor: formatText(['modeledFrom', 'name']),
        width: 120
      },
      {
        id: 'comment',
        Header: t('txt_comment'),
        accessor: formatTruncate(['comment']),
        width: 312
      },
      {
        id: 'methodType',
        Header: t('txt_change_interest_rates_action'),
        accessor: (data, idx) =>
          formatBadge(['methodType'], {
            colorType: BadgeColorType.MethodType,
            noBorder: true
          })(
            {
              ...data,
              methodType: methodTypeToText(data.methodType)
            },
            idx
          ),
        width: 153
      }
    ],
    [t, methodTypeToText]
  );

  const handleEdit = () => {
    isFunction(onEditStep) && onEditStep();
  };

  const handleOnExpand = (dataKeyList: string[]) => {
    setExpandedList(
      isEmpty(dataKeyList) ? [] : [dataKeyList[dataKeyList.length - 1]]
    );
  };

  useEffect(() => {
    if (stepId !== selectedStep) {
      onResetToDefaultFilter();
      return;
    }

    setExpandedList([]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedStep]);

  return (
    <div className="position-relative">
      <div className="absolute-top-right mt-n26 mr-n8">
        <Button variant="outline-primary" size="sm" onClick={handleEdit}>
          {t('txt_edit')}
        </Button>
      </div>
      <div className="pt-16">
        <Grid
          columns={columns}
          data={pageData}
          subRow={({ original }: any) => {
            return (
              <SubRowGrid
                original={original}
                metadata={metadata}
                type={typeMethod(original)}
              />
            );
          }}
          togglable
          dataItemKey="rowId"
          toggleButtonConfigList={methods.map(
            mapGridExpandCollapse('rowId', t)
          )}
          expandedItemKey={'rowId'}
          expandedList={expandedList}
          onExpand={handleOnExpand}
          scrollable
        />
        {total > 10 && (
          <div className="mt-16">
            <Paging
              page={currentPage}
              pageSize={currentPageSize}
              totalItem={total}
              onChangePage={onPageChange}
              onChangePageSize={onPageSizeChange}
            />
          </div>
        )}
      </div>
    </div>
  );
};

export default ConfigSummary;
