import HtmlTruncateText from 'app/components/HtmlTruncateText';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import { formatCommon, methodParamsToObject } from 'app/helpers';
import {
  ColumnType,
  Grid,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React, { useCallback, useMemo } from 'react';
import { parameterGroup, ParameterList } from './newMethodParameterList';

export interface SubRowGridInfoProps {
  original: ParameterList;
  method: WorkflowSetupMethod;
  metadata: {
    defaultCashBaseInterest: RefData[];
    defaultMerchandiseBaseInterest: RefData[];
    defaultMaximumCashInterest: RefData[];
    defaultMaximumMerchandiseInterest: RefData[];
    defaultMinimumCashInterest: RefData[];
    defaultMinimumMerchandiseInterest: RefData[];
    indexRateMethod: RefData[];
    indexRateMethodName: RefData[];
    defaultBaseInterestUsage: RefData[];
    defaultOverrideInterestLimit: RefData[];
    defaultMinimumUsage: RefData[];
    defaultMaximumUsage: RefData[];
    defaultMaximumInterestUsage: RefData[];
    incentivePricingUsageCode: RefData[];
    incentiveBaseInterestUsageRate: RefData[];
    delinquencyCyclesForTermination: RefData[];
    minimumMaximumRateOverrideOptions: RefData[];
    incentivePricingOverrideOption: RefData[];
    terminateIncentivePricingOnPricingStrategyChange: RefData[];
    methodOverrideTerminationOption: RefData[];
    endofIncentiveWarningNotificationTiming: RefData[];
    endofIncentiveWarningNotificationStatementText: RefData[];
    messageForProtectedBalanceIncentivePricing: RefData[];
    interestRateRoundingCalculation: RefData[];
    dailyAPRRoundingCalculation: RefData[];
    stopCalculatingInterestAfterNumberofDaysDelinquent: RefData[];
    includeAdditionalCreditBalanceInAverageDailyBalanceCalculation: RefData[];
    combineMerchandiseAndCashBalancesOnInterestCalculations: RefData[];
    restartInterestCalculationsAfterClearingDelinquency: RefData[];
    calculateCompoundInterest: RefData[];
    overridePromotionalRatesForPricingStrategies: RefData[];
    monthlyInterestCalculationOptions: RefData[];
    dailyInterestRoundingOption: RefData[];
    dailyInterestAmountTruncationOption: RefData[];
    overrideInterestRatesForProtectedBalance: RefData[];
    roundNumberOfDaysInCycleForInterestCalculations: RefData[];
    cycleCodeChangeOptions: RefData[];
    cyclePeriodChangeCISMemoOption: RefData[];
    interestMethodOldCash: RefData[];
    interestMethodCycleToDateCash: RefData[];
    interestMethodTwoCycleOldCash: RefData[];
    interestMethodOneCycleOldMerchandise: RefData[];
    interestMethodCycleToDateMerchandise: RefData[];
    additionalInterestOnMerchandise1CyclesDelinquent: RefData[];
    additionalInterestOnMerchandise2CyclesDelinquent: RefData[];
    additionalInterestOnCash2CyclesDelinquent: RefData[];
    additionalInterestOnMerchandise3CyclesDelinquent: RefData[];
    additionalInterestOnCash3CyclesDelinquent: RefData[];
    automatedMemosForAdditionalInterest: RefData[];
    useExternalStatusesForAdditionalInterest: RefData[];
    externalStatus1ForAdditionalInterest: RefData[];
    externalStatus2ForAdditionalInterest: RefData[];
    externalStatus3ForAdditionalInterest: RefData[];
    externalStatus4ForAdditionalInterest: RefData[];
    externalStatus5ForAdditionalInterest: RefData[];
    penaltyPricingStatusReasonCodeTable: RefData[];
    penaltyPricingTimingOptions: RefData[];
    penaltyPricingStatementMessage1CyclesOldBalance: RefData[];
    penaltyPricingStatementMessage2CyclesOldBalance: RefData[];
    penaltyPricingStatementMessage3CyclesOldBalance: RefData[];
    cureDelinquencyFor1CycleDelinquentBalances: RefData[];
    cureDelinquencyFor2CycleDelinquentBalances: RefData[];
    cureDelinquencyFor3CycleDelinquentBalances: RefData[];
    penaltyPricingOnPromotions: RefData[];
    citMethodForPenaltyPricing: RefData[];
    includeZeroBalanceDaysInADBCalculation: RefData[];
  };
}
const SubRowGridInfo: React.FC<SubRowGridInfoProps> = ({
  original,
  method: methodProp,
  metadata
}) => {
  const { t } = useTranslation();

  const method = useMemo(() => methodParamsToObject(methodProp), [methodProp]);
  const data = useMemo(() => parameterGroup[original.id], [original.id]);

  const values = useMemo<Record<string, string | undefined>>(() => {
    const {
      indexRateMethod,
      indexRateMethodName,
      defaultBaseInterestUsage,
      defaultMinimumUsage,
      defaultMaximumUsage,
      defaultMaximumInterestUsage,
      incentivePricingUsageCode,
      incentiveBaseInterestUsageRate,
      delinquencyCyclesForTermination,
      minimumMaximumRateOverrideOptions,
      incentivePricingOverrideOption,
      terminateIncentivePricingOnPricingStrategyChange,
      methodOverrideTerminationOption,
      endofIncentiveWarningNotificationTiming,
      endofIncentiveWarningNotificationStatementText,
      messageForProtectedBalanceIncentivePricing,
      interestRateRoundingCalculation,
      dailyAPRRoundingCalculation,
      includeAdditionalCreditBalanceInAverageDailyBalanceCalculation,
      combineMerchandiseAndCashBalancesOnInterestCalculations,
      restartInterestCalculationsAfterClearingDelinquency,
      calculateCompoundInterest,
      overridePromotionalRatesForPricingStrategies,
      monthlyInterestCalculationOptions,
      dailyInterestRoundingOption,
      dailyInterestAmountTruncationOption,
      overrideInterestRatesForProtectedBalance,
      roundNumberOfDaysInCycleForInterestCalculations,
      cycleCodeChangeOptions,
      cyclePeriodChangeCISMemoOption,
      interestMethodOldCash,
      interestMethodCycleToDateCash,
      interestMethodTwoCycleOldCash,
      interestMethodOneCycleOldMerchandise,
      interestMethodCycleToDateMerchandise,
      automatedMemosForAdditionalInterest,
      useExternalStatusesForAdditionalInterest,
      externalStatus1ForAdditionalInterest,
      externalStatus2ForAdditionalInterest,
      externalStatus3ForAdditionalInterest,
      externalStatus4ForAdditionalInterest,
      externalStatus5ForAdditionalInterest,
      penaltyPricingStatusReasonCodeTable,
      penaltyPricingTimingOptions,
      penaltyPricingStatementMessage1CyclesOldBalance,
      penaltyPricingStatementMessage2CyclesOldBalance,
      penaltyPricingStatementMessage3CyclesOldBalance,
      cureDelinquencyFor1CycleDelinquentBalances,
      cureDelinquencyFor2CycleDelinquentBalances,
      cureDelinquencyFor3CycleDelinquentBalances,
      penaltyPricingOnPromotions,
      citMethodForPenaltyPricing,
      includeZeroBalanceDaysInADBCalculation
    } = metadata;
    return {
      [MethodFieldParameterEnum.DefaultCashBaseInterest]:
        method[MethodFieldParameterEnum.DefaultCashBaseInterest] &&
        formatCommon(
          method[MethodFieldParameterEnum.DefaultCashBaseInterest]!
        ).percent(3),
      [MethodFieldParameterEnum.DefaultMerchandiseBaseInterest]:
        method[MethodFieldParameterEnum.DefaultMerchandiseBaseInterest] &&
        formatCommon(
          method[MethodFieldParameterEnum.DefaultMerchandiseBaseInterest]!
        ).percent(3),
      [MethodFieldParameterEnum.DefaultMaximumCashInterest]:
        method[MethodFieldParameterEnum.DefaultMaximumCashInterest] &&
        formatCommon(
          method[MethodFieldParameterEnum.DefaultMaximumCashInterest]!
        ).percent(3),
      [MethodFieldParameterEnum.DefaultMaximumMerchandiseInterest]:
        method[MethodFieldParameterEnum.DefaultMaximumMerchandiseInterest] &&
        formatCommon(
          method[MethodFieldParameterEnum.DefaultMaximumMerchandiseInterest]!
        ).percent(3),
      [MethodFieldParameterEnum.DefaultMinimumCashInterest]:
        method[MethodFieldParameterEnum.DefaultMinimumCashInterest] &&
        formatCommon(
          method[MethodFieldParameterEnum.DefaultMinimumCashInterest]!
        ).percent(3),
      [MethodFieldParameterEnum.DefaultMinimumMerchandiseInterest]:
        method[MethodFieldParameterEnum.DefaultMinimumMerchandiseInterest] &&
        formatCommon(
          method[MethodFieldParameterEnum.DefaultMinimumMerchandiseInterest]!
        ).percent(3),
      [MethodFieldParameterEnum.IndexRateMethod]: indexRateMethod.find(
        o => o.code === method[MethodFieldParameterEnum.IndexRateMethod]
      )?.text,
      [MethodFieldParameterEnum.IndexRateMethodName]: indexRateMethodName.find(
        o => o.code === method[MethodFieldParameterEnum.IndexRateMethodName]
      )?.text,
      [MethodFieldParameterEnum.DefaultBaseInterestUsage]:
        defaultBaseInterestUsage.find(
          o =>
            o.code === method[MethodFieldParameterEnum.DefaultBaseInterestUsage]
        )?.text,
      [MethodFieldParameterEnum.DefaultOverrideInterestLimit]:
        method[MethodFieldParameterEnum.DefaultOverrideInterestLimit] + '%',
      [MethodFieldParameterEnum.DefaultMinimumUsage]: defaultMinimumUsage.find(
        o => o.code === method[MethodFieldParameterEnum.DefaultMinimumUsage]
      )?.text,
      [MethodFieldParameterEnum.DefaultMaximumUsage]: defaultMaximumUsage.find(
        o => o.code === method[MethodFieldParameterEnum.DefaultMaximumUsage]
      )?.text,
      [MethodFieldParameterEnum.DefaultMaximumInterestUsage]:
        defaultMaximumInterestUsage.find(
          o =>
            o.code ===
            method[MethodFieldParameterEnum.DefaultMaximumInterestUsage]
        )?.text,

      [MethodFieldParameterEnum.IncentivePricingUsageCode]:
        incentivePricingUsageCode.find(
          o =>
            o.code ===
            method[MethodFieldParameterEnum.IncentivePricingUsageCode]
        )?.text,
      [MethodFieldParameterEnum.IncentiveBaseInterestUsageRate]:
        incentiveBaseInterestUsageRate.find(
          o =>
            o.code ===
            method[MethodFieldParameterEnum.IncentiveBaseInterestUsageRate]
        )?.text,
      [MethodFieldParameterEnum.DelinquencyCyclesForTermination]:
        delinquencyCyclesForTermination.find(
          o =>
            o.code ===
            method[MethodFieldParameterEnum.DelinquencyCyclesForTermination]
        )?.text,
      [MethodFieldParameterEnum.MinimumMaximumRateOverrideOptions]:
        minimumMaximumRateOverrideOptions.find(
          o =>
            o.code ===
            method[MethodFieldParameterEnum.MinimumMaximumRateOverrideOptions]
        )?.text,
      [MethodFieldParameterEnum.IncentivePricingOverrideOption]:
        incentivePricingOverrideOption.find(
          o =>
            o.code ===
            method[MethodFieldParameterEnum.IncentivePricingOverrideOption]
        )?.text,
      [MethodFieldParameterEnum.TerminateIncentivePricingOnPricingStrategyChange]:
        terminateIncentivePricingOnPricingStrategyChange.find(
          o =>
            o.code ===
            method[
              MethodFieldParameterEnum
                .TerminateIncentivePricingOnPricingStrategyChange
            ]
        )?.text,
      [MethodFieldParameterEnum.MethodOverrideTerminationOption]:
        methodOverrideTerminationOption.find(
          o =>
            o.code ===
            method[MethodFieldParameterEnum.MethodOverrideTerminationOption]
        )?.text,
      [MethodFieldParameterEnum.EndofIncentiveWarningNotificationTiming]:
        endofIncentiveWarningNotificationTiming.find(
          o =>
            o.code ===
            method[
              MethodFieldParameterEnum.EndofIncentiveWarningNotificationTiming
            ]
        )?.text,
      [MethodFieldParameterEnum.EndofIncentiveWarningNotificationStatementText]:
        endofIncentiveWarningNotificationStatementText.find(
          o =>
            o.code ===
            method[
              MethodFieldParameterEnum
                .EndofIncentiveWarningNotificationStatementText
            ]
        )?.text,
      [MethodFieldParameterEnum.IncentivePricingNumberOfMonths]:
        method[MethodFieldParameterEnum.IncentivePricingNumberOfMonths],
      [MethodFieldParameterEnum.MessageForProtectedBalanceIncentivePricing]:
        messageForProtectedBalanceIncentivePricing.find(
          o =>
            o.code ===
            method[
              MethodFieldParameterEnum
                .MessageForProtectedBalanceIncentivePricing
            ]
        )?.text,
      [MethodFieldParameterEnum.IncentiveCashInterestRate]:
        method[MethodFieldParameterEnum.IncentiveCashInterestRate] &&
        formatCommon(
          method[MethodFieldParameterEnum.IncentiveCashInterestRate]!
        ).percent(3),
      [MethodFieldParameterEnum.IncentiveMerchandiseInterestRate]:
        method[MethodFieldParameterEnum.IncentiveMerchandiseInterestRate] &&
        formatCommon(
          method[MethodFieldParameterEnum.IncentiveMerchandiseInterestRate]!
        ).percent(3),
      [MethodFieldParameterEnum.MinimumRateForCash]:
        method[MethodFieldParameterEnum.MinimumRateForCash] &&
        formatCommon(
          method[MethodFieldParameterEnum.MinimumRateForCash]!
        ).percent(3),
      [MethodFieldParameterEnum.MaximumRateForCash]:
        method[MethodFieldParameterEnum.MaximumRateForCash] &&
        formatCommon(
          method[MethodFieldParameterEnum.MaximumRateForCash]!
        ).percent(3),
      [MethodFieldParameterEnum.MinimumRateForMerchandise]:
        method[MethodFieldParameterEnum.MinimumRateForMerchandise] &&
        formatCommon(
          method[MethodFieldParameterEnum.MinimumRateForMerchandise]!
        ).percent(3),
      [MethodFieldParameterEnum.MaximumRateForMerchandise]:
        method[MethodFieldParameterEnum.MaximumRateForMerchandise] &&
        formatCommon(
          method[MethodFieldParameterEnum.MaximumRateForMerchandise]!
        ).percent(3),
      [MethodFieldParameterEnum.PreventTermsChangeDuringIncentivePricingPeriod]:
        method[
          MethodFieldParameterEnum
            .PreventTermsChangeDuringIncentivePricingPeriod
        ],
      [MethodFieldParameterEnum.InterestRateRoundingCalculation]:
        interestRateRoundingCalculation.find(
          o =>
            o.code ===
            method[MethodFieldParameterEnum.InterestRateRoundingCalculation]
        )?.text,
      [MethodFieldParameterEnum.DailyAPRRoundingCalculation]:
        dailyAPRRoundingCalculation.find(
          o =>
            o.code ===
            method[MethodFieldParameterEnum.DailyAPRRoundingCalculation]
        )?.text,
      [MethodFieldParameterEnum.StopCalculatingInterestAfterNumberofDaysDelinquent]:
        method[
          MethodFieldParameterEnum
            .StopCalculatingInterestAfterNumberofDaysDelinquent
        ],
      [MethodFieldParameterEnum.IncludeAdditionalCreditBalanceInAverageDailyBalanceCalculation]:
        includeAdditionalCreditBalanceInAverageDailyBalanceCalculation.find(
          o =>
            o.code ===
            method[
              MethodFieldParameterEnum
                .IncludeAdditionalCreditBalanceInAverageDailyBalanceCalculation
            ]
        )?.text,
      [MethodFieldParameterEnum.CombineMerchandiseAndCashBalancesOnInterestCalculations]:
        combineMerchandiseAndCashBalancesOnInterestCalculations.find(
          o =>
            o.code ===
            method[
              MethodFieldParameterEnum
                .CombineMerchandiseAndCashBalancesOnInterestCalculations
            ]
        )?.text,
      [MethodFieldParameterEnum.RestartInterestCalculationsAfterClearingDelinquency]:
        restartInterestCalculationsAfterClearingDelinquency.find(
          o =>
            o.code ===
            method[
              MethodFieldParameterEnum
                .RestartInterestCalculationsAfterClearingDelinquency
            ]
        )?.text,
      [MethodFieldParameterEnum.CalculateCompoundInterest]:
        calculateCompoundInterest.find(
          o =>
            o.code ===
            method[MethodFieldParameterEnum.CalculateCompoundInterest]
        )?.text,
      [MethodFieldParameterEnum.OverridePromotionalRatesForPricingStrategies]:
        overridePromotionalRatesForPricingStrategies.find(
          o =>
            o.code ===
            method[
              MethodFieldParameterEnum
                .OverridePromotionalRatesForPricingStrategies
            ]
        )?.text,
      [MethodFieldParameterEnum.MonthlyInterestCalculationOptions]:
        monthlyInterestCalculationOptions.find(
          o =>
            o.code ===
            method[MethodFieldParameterEnum.MonthlyInterestCalculationOptions]
        )?.text,
      [MethodFieldParameterEnum.DailyInterestRoundingOption]:
        dailyInterestRoundingOption.find(
          o =>
            o.code ===
            method[MethodFieldParameterEnum.DailyInterestRoundingOption]
        )?.text,
      [MethodFieldParameterEnum.DailyInterestAmountTruncationOption]:
        dailyInterestAmountTruncationOption.find(
          o =>
            o.code ===
            method[MethodFieldParameterEnum.DailyInterestAmountTruncationOption]
        )?.text,
      [MethodFieldParameterEnum.OverrideInterestRatesForProtectedBalance]:
        overrideInterestRatesForProtectedBalance.find(
          o =>
            o.code ===
            method[
              MethodFieldParameterEnum.OverrideInterestRatesForProtectedBalance
            ]
        )?.text,
      [MethodFieldParameterEnum.MinimumDaysForCycleCodeChange]:
        method[MethodFieldParameterEnum.MinimumDaysForCycleCodeChange],
      [MethodFieldParameterEnum.RoundNumberOfDaysInCycleForInterestCalculations]:
        roundNumberOfDaysInCycleForInterestCalculations.find(
          o =>
            o.code ===
            method[
              MethodFieldParameterEnum
                .RoundNumberOfDaysInCycleForInterestCalculations
            ]
        )?.text,
      [MethodFieldParameterEnum.CycleCodeChangeOptions]:
        cycleCodeChangeOptions.find(
          o =>
            o.code === method[MethodFieldParameterEnum.CycleCodeChangeOptions]
        )?.text,
      [MethodFieldParameterEnum.CyclePeriodChangeCISMemoOption]:
        cyclePeriodChangeCISMemoOption.find(
          o =>
            o.code ===
            method[MethodFieldParameterEnum.CyclePeriodChangeCISMemoOption]
        )?.text,
      [MethodFieldParameterEnum.InterestMethodOldCash]:
        interestMethodOldCash.find(
          o => o.code === method[MethodFieldParameterEnum.InterestMethodOldCash]
        )?.text,
      [MethodFieldParameterEnum.InterestMethodCycleToDateCash]:
        interestMethodCycleToDateCash.find(
          o =>
            o.code ===
            method[MethodFieldParameterEnum.InterestMethodCycleToDateCash]
        )?.text,
      [MethodFieldParameterEnum.InterestMethodTwoCycleOldCash]:
        interestMethodTwoCycleOldCash.find(
          o =>
            o.code ===
            method[MethodFieldParameterEnum.InterestMethodTwoCycleOldCash]
        )?.text,
      [MethodFieldParameterEnum.InterestMethodOneCycleOldMerchandise]:
        interestMethodOneCycleOldMerchandise.find(
          o =>
            o.code ===
            method[
              MethodFieldParameterEnum.InterestMethodOneCycleOldMerchandise
            ]
        )?.text,
      [MethodFieldParameterEnum.InterestMethodCycleToDateMerchandise]:
        interestMethodCycleToDateMerchandise.find(
          o =>
            o.code ===
            method[
              MethodFieldParameterEnum.InterestMethodCycleToDateMerchandise
            ]
        )?.text,
      [MethodFieldParameterEnum.AdditionalInterestOnMerchandise1CyclesDelinquent]:
        method[
          MethodFieldParameterEnum
            .AdditionalInterestOnMerchandise1CyclesDelinquent
        ] &&
        formatCommon(
          method[
            MethodFieldParameterEnum
              .AdditionalInterestOnMerchandise1CyclesDelinquent
          ]!
        ).percent(3),
      [MethodFieldParameterEnum.AdditionalInterestOnMerchandise2CyclesDelinquent]:
        method[
          MethodFieldParameterEnum
            .AdditionalInterestOnMerchandise2CyclesDelinquent
        ] &&
        formatCommon(
          method[
            MethodFieldParameterEnum
              .AdditionalInterestOnMerchandise2CyclesDelinquent
          ]!
        ).percent(3),
      [MethodFieldParameterEnum.AdditionalInterestOnCash2CyclesDelinquent]:
        method[
          MethodFieldParameterEnum.AdditionalInterestOnCash2CyclesDelinquent
        ] &&
        formatCommon(
          method[
            MethodFieldParameterEnum.AdditionalInterestOnCash2CyclesDelinquent
          ]!
        ).percent(3),
      [MethodFieldParameterEnum.AdditionalInterestOnMerchandise3CyclesDelinquent]:
        method[
          MethodFieldParameterEnum
            .AdditionalInterestOnMerchandise3CyclesDelinquent
        ] &&
        formatCommon(
          method[
            MethodFieldParameterEnum
              .AdditionalInterestOnMerchandise3CyclesDelinquent
          ]!
        ).percent(3),
      [MethodFieldParameterEnum.AdditionalInterestOnCash3CyclesDelinquent]:
        method[
          MethodFieldParameterEnum.AdditionalInterestOnCash3CyclesDelinquent
        ] &&
        formatCommon(
          method[
            MethodFieldParameterEnum.AdditionalInterestOnCash3CyclesDelinquent
          ]!
        ).percent(3),
      [MethodFieldParameterEnum.AutomatedMemosForAdditionalInterest]:
        automatedMemosForAdditionalInterest.find(
          o =>
            o.code ===
            method[MethodFieldParameterEnum.AutomatedMemosForAdditionalInterest]
        )?.text,
      [MethodFieldParameterEnum.UseExternalStatusesForAdditionalInterest]:
        useExternalStatusesForAdditionalInterest.find(
          o =>
            o.code ===
            method[
              MethodFieldParameterEnum.UseExternalStatusesForAdditionalInterest
            ]
        )?.text,
      [MethodFieldParameterEnum.ExternalStatus1ForAdditionalInterest]:
        externalStatus1ForAdditionalInterest.find(
          o =>
            o.code ===
            method[
              MethodFieldParameterEnum.ExternalStatus1ForAdditionalInterest
            ]
        )?.text,
      [MethodFieldParameterEnum.ExternalStatus2ForAdditionalInterest]:
        externalStatus2ForAdditionalInterest.find(
          o =>
            o.code ===
            method[
              MethodFieldParameterEnum.ExternalStatus2ForAdditionalInterest
            ]
        )?.text,
      [MethodFieldParameterEnum.ExternalStatus3ForAdditionalInterest]:
        externalStatus3ForAdditionalInterest.find(
          o =>
            o.code ===
            method[
              MethodFieldParameterEnum.ExternalStatus3ForAdditionalInterest
            ]
        )?.text,
      [MethodFieldParameterEnum.ExternalStatus4ForAdditionalInterest]:
        externalStatus4ForAdditionalInterest.find(
          o =>
            o.code ===
            method[
              MethodFieldParameterEnum.ExternalStatus4ForAdditionalInterest
            ]
        )?.text,
      [MethodFieldParameterEnum.ExternalStatus5ForAdditionalInterest]:
        externalStatus5ForAdditionalInterest.find(
          o =>
            o.code ===
            method[
              MethodFieldParameterEnum.ExternalStatus5ForAdditionalInterest
            ]
        )?.text,
      [MethodFieldParameterEnum.PenaltyPricingStatusReasonCodeTable]:
        penaltyPricingStatusReasonCodeTable.find(
          o =>
            o.code ===
            method[MethodFieldParameterEnum.PenaltyPricingStatusReasonCodeTable]
        )?.text,
      [MethodFieldParameterEnum.PenaltyPricingTimingOptions]:
        penaltyPricingTimingOptions.find(
          o =>
            o.code ===
            method[MethodFieldParameterEnum.PenaltyPricingTimingOptions]
        )?.text,
      [MethodFieldParameterEnum.PenaltyPricingStatementMessage1CyclesOldBalance]:
        penaltyPricingStatementMessage1CyclesOldBalance.find(
          o =>
            o.code ===
            method[
              MethodFieldParameterEnum
                .PenaltyPricingStatementMessage1CyclesOldBalance
            ]
        )?.text,
      [MethodFieldParameterEnum.PenaltyPricingStatementMessage2CyclesOldBalance]:
        penaltyPricingStatementMessage2CyclesOldBalance.find(
          o =>
            o.code ===
            method[
              MethodFieldParameterEnum
                .PenaltyPricingStatementMessage2CyclesOldBalance
            ]
        )?.text,
      [MethodFieldParameterEnum.PenaltyPricingStatementMessage3CyclesOldBalance]:
        penaltyPricingStatementMessage3CyclesOldBalance.find(
          o =>
            o.code ===
            method[
              MethodFieldParameterEnum
                .PenaltyPricingStatementMessage3CyclesOldBalance
            ]
        )?.text,
      [MethodFieldParameterEnum.CureDelinquencyFor1CycleDelinquentBalances]:
        cureDelinquencyFor1CycleDelinquentBalances.find(
          o =>
            o.code ===
            method[
              MethodFieldParameterEnum
                .CureDelinquencyFor1CycleDelinquentBalances
            ]
        )?.text,
      [MethodFieldParameterEnum.CureDelinquencyFor2CycleDelinquentBalances]:
        cureDelinquencyFor2CycleDelinquentBalances.find(
          o =>
            o.code ===
            method[
              MethodFieldParameterEnum
                .CureDelinquencyFor2CycleDelinquentBalances
            ]
        )?.text,
      [MethodFieldParameterEnum.CureDelinquencyFor3CycleDelinquentBalances]:
        cureDelinquencyFor3CycleDelinquentBalances.find(
          o =>
            o.code ===
            method[
              MethodFieldParameterEnum
                .CureDelinquencyFor3CycleDelinquentBalances
            ]
        )?.text,
      [MethodFieldParameterEnum.PenaltyPricingOnPromotions]:
        penaltyPricingOnPromotions.find(
          o =>
            o.code ===
            method[MethodFieldParameterEnum.PenaltyPricingOnPromotions]
        )?.text,
      [MethodFieldParameterEnum.CITMethodForPenaltyPricing]:
        citMethodForPenaltyPricing.find(
          o =>
            o.code ===
            method[MethodFieldParameterEnum.CITMethodForPenaltyPricing]
        )?.text,
      [MethodFieldParameterEnum.IncludeZeroBalanceDaysInADBCalculation]:
        includeZeroBalanceDaysInADBCalculation.find(
          o =>
            o.code ===
            method[
              MethodFieldParameterEnum.IncludeZeroBalanceDaysInADBCalculation
            ]
        )?.text
    };
  }, [metadata, method]);

  const valueAccessor = useCallback(
    data => {
      const valueTruncate =
        values[data.id]?.toString().toLowerCase() !== 'none'
          ? values[data.id]
          : '';

      if (
        data.id ===
        MethodFieldParameterEnum.RoundNumberOfDaysInCycleForInterestCalculations
      ) {
        return (
          <HtmlTruncateText
            resizable
            lines={2}
            raw={valueTruncate!}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {valueTruncate?.replace(/\n/g, '</br>')?.replace(/\t/g, '&emsp;')}
          </HtmlTruncateText>
        );
      }

      return (
        <TruncateText
          resizable
          lines={2}
          ellipsisLessText={t('txt_less')}
          ellipsisMoreText={t('txt_more')}
        >
          {valueTruncate}
        </TruncateText>
      );
    },
    [t, values]
  );

  const columns: ColumnType[] = [
    {
      id: 'fieldName',
      Header: t('txt_business_name'),
      accessor: 'fieldName'
    },
    {
      id: 'onlinePCF',
      Header: t('txt_change_interest_rates_online_PCF'),
      accessor: 'onlinePCF'
    },
    {
      id: 'id',
      Header: t('txt_value'),
      accessor: valueAccessor
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      accessor: viewMoreInfo(['moreInfo'], t),
      width: 105
    }
  ];

  return (
    <div className="p-20 overflow-hidden">
      <Grid columns={columns} data={data} />
    </div>
  );
};

export default SubRowGridInfo;
