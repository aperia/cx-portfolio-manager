import { ServiceSubjectSection } from 'app/constants/enums';
import {
  getWorkflowSetupMethodType,
  getWorkflowSetupStepStatus
} from 'app/helpers';
import { ConfigFormValues } from '.';

const parseFormValues: WorkflowSetupStepFormDataFunc<ConfigFormValues> = (
  data,
  id
) => {
  const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);
  if (!stepInfo) return;

  let checkType = '';
  switch (id) {
    case 'configureParametersInterestMethod':
      checkType = ServiceSubjectSection.UIM;
      break;
    case 'configureParametersInterestInventive':
      checkType = ServiceSubjectSection.UIP;
      break;
    default:
      checkType = ServiceSubjectSection.UID;
  }

  const now = Date.now();
  const methods = (data.data.configurations?.methodList || [])
    .filter((method: any) => checkType === method.serviceSubjectSection)
    .map(
      (method: any, idx: number) =>
        ({
          ...method,
          id: '', // Remove Id due to error 500 when update with methodId
          methodType: getWorkflowSetupMethodType(method),
          rowId: now + idx
        } as WorkflowSetupMethod & { rowId?: number })
    );

  return {
    ...getWorkflowSetupStepStatus(stepInfo),
    isValid: methods.length > 0,
    methods
  };
};

export default parseFormValues;
