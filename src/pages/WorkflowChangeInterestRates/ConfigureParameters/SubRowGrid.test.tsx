import { fireEvent, render } from '@testing-library/react';
import React from 'react';
import SubRowGrid from './SubRowGrid';

jest.mock('app/_libraries/_dof/core', () => ({
  View: ({ formKey, descriptor, onClick }: any) => {
    return (
      <div id={formKey}>
        {formKey}
        <span>{descriptor}</span>
      </div>
    );
  }
}));

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

describe('ManagePenaltyFeeWorkflow > LateChargesStep > SubRowGrid', () => {
  const props = {
    metadata: {
      defaultCashBaseInterest: [
        {
          code: 'none',
          text: 'none'
        }
      ],
      defaultMerchandiseBaseInterest: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      defaultMaximumCashInterest: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      defaultMaximumMerchandiseInterest: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      defaultMinimumCashInterest: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      defaultMinimumMerchandiseInterest: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      indexRateMethod: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      defaultBaseInterestUsage: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      defaultOverrideInterestLimit: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      defaultMinimumUsage: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      defaultMaximumUsage: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      defaultMaximumInterestUsage: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      incentivePricingUsageCode: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      incentiveBaseInterestUsageRate: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      delinquencyCyclesForTermination: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      minimumMaximumRateOverrideOptions: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      incentivePricingOverrideOption: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      terminateIncentivePricingOnPricingStrategyChange: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      methodOverrideTerminationOption: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      endofIncentiveWarningNotificationTiming: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      endofIncentiveWarningNotificationStatementText: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      incentivePricingNumberOfMonths: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      messageForProtectedBalanceIncentivePricing: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      indexRateMethodName: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      interestRateRoundingCalculation: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      dailyAPRRoundingCalculation: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      stopCalculatingInterestAfterNumberofDaysDelinquent: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      includeAdditionalCreditBalanceInAverageDailyBalanceCalculation: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      combineMerchandiseAndCashBalancesOnInterestCalculations: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      restartInterestCalculationsAfterClearingDelinquency: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      calculateCompoundInterest: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      overridePromotionalRatesForPricingStrategies: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      monthlyInterestCalculationOptions: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      dailyInterestRoundingOption: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      dailyInterestAmountTruncationOption: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      overrideInterestRatesForProtectedBalance: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      minimumDaysForCycleCodeChange: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      roundNumberOfDaysInCycleForInterestCalculations: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      cycleCodeChangeOptions: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      cyclePeriodChangeCISMemoOption: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      interestMethodOldCash: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      interestMethodCycleToDateCash: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      interestMethodTwoCycleOldCash: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      interestMethodOneCycleOldMerchandise: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      interestMethodCycleToDateMerchandise: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      additionalInterestOnMerchandise1CyclesDelinquent: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      additionalInterestOnMerchandise2CyclesDelinquent: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      additionalInterestOnCash2CyclesDelinquent: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      additionalInterestOnMerchandise3CyclesDelinquent: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      additionalInterestOnCash3CyclesDelinquent: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      automatedMemosForAdditionalInterest: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      useExternalStatusesForAdditionalInterest: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      externalStatus1ForAdditionalInterest: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      externalStatus2ForAdditionalInterest: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      externalStatus3ForAdditionalInterest: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      externalStatus4ForAdditionalInterest: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      externalStatus5ForAdditionalInterest: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      penaltyPricingStatusReasonCodeTable: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      penaltyPricingTimingOptions: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      penaltyPricingStatementMessage1CyclesOldBalance: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      penaltyPricingStatementMessage2CyclesOldBalance: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      penaltyPricingStatementMessage3CyclesOldBalance: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      cureDelinquencyFor1CycleDelinquentBalances: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      cureDelinquencyFor2CycleDelinquentBalances: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      cureDelinquencyFor3CycleDelinquentBalances: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      penaltyPricingOnPromotions: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      citMethodForPenaltyPricing: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      includeZeroBalanceDaysInADBCalculation: [
        {
          code: 'text',
          text: 'text'
        }
      ],
      roundNumberOfDaysInCycleForInterestCalculations: [
        {
          code: 'round.number.of.days.in.cycle.for.interest.calculations',
          text: 'text'
        }
      ]
    },
    original: {
      rowId: 1
    }
  };

  it('should render ', () => {
    const wrapper = render(<SubRowGrid {...props} type="IM" />);
    expect(
      wrapper.getAllByText('txt_change_interest_rates_advanced_parameters')[0]
    ).toBeInTheDocument();
    wrapper.container.querySelectorAll('.icon.icon-plus').forEach(element => {
      fireEvent.click(element!);
    });
  });

  it('should render with case default ', () => {
    const wrapper = render(<SubRowGrid {...props} type="ID" />);
    expect(wrapper.getByText('Index Rate Method')).toBeInTheDocument();
  });

  it('should render within tier', () => {
    const wrapper = render(
      <SubRowGrid
        {...props}
        type="IP"
        original={
          {
            'tier.3.balance': '1',
            'tier.3.amount': '10',
            'tier.4.balance': '1',
            'tier.4.amount': '10',
            'tier.5.balance': '1',
            'tier.5.amount': '10',
            'round.number.of.days.in.cycle.for.interest.calculations': '1',
            rowId: 1
          } as any
        }
      />
    );
    expect(
      wrapper.getAllByText('txt_change_interest_rates_advanced_parameters')[0]
    ).toBeInTheDocument();
  });
});
