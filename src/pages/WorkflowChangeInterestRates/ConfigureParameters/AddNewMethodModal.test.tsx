import userEvent from '@testing-library/user-event';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import { renderWithMockStore } from 'app/utils';
import 'app/utils/_mockComponent/mockCanvas';
import 'app/utils/_mockComponent/mockModalRegistry';
import 'app/utils/_mockComponent/mockUseTranslation';
import mockDevice from 'app/utils/_mockHelperConstant/mockDevice';
import React from 'react';
import AddNewMethodModal from './AddNewMethodModal';
const methodData = [
  {
    id: '1',
    name: MethodFieldParameterEnum.IndexRateMethod,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '100',
    name: MethodFieldParameterEnum.IndexRateMethodName,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '2',
    name: MethodFieldParameterEnum.DefaultBaseInterestUsage,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '3',
    name: MethodFieldParameterEnum.DefaultMinimumUsage,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '4',
    name: MethodFieldParameterEnum.DefaultMaximumUsage,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '5',
    name: MethodFieldParameterEnum.DefaultMaximumInterestUsage,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '6',
    name: MethodFieldParameterEnum.IncentivePricingUsageCode,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '7',
    name: MethodFieldParameterEnum.IncentiveBaseInterestUsageRate,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '8',
    name: MethodFieldParameterEnum.DelinquencyCyclesForTermination,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '9',
    name: MethodFieldParameterEnum.MinimumMaximumRateOverrideOptions,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '10',
    name: MethodFieldParameterEnum.IncentivePricingOverrideOption,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '11',
    name: MethodFieldParameterEnum.TerminateIncentivePricingOnPricingStrategyChange,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '12',
    name: MethodFieldParameterEnum.MethodOverrideTerminationOption,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '13',
    name: MethodFieldParameterEnum.IncentiveCashInterestRate,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '14',
    name: MethodFieldParameterEnum.IncentiveMerchandiseInterestRate,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '16',
    name: MethodFieldParameterEnum.EndofIncentiveWarningNotificationTiming,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '17',
    name: MethodFieldParameterEnum.EndofIncentiveWarningNotificationStatementText,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '18',
    name: MethodFieldParameterEnum.IncentivePricingNumberOfMonths,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '19',
    name: MethodFieldParameterEnum.MinimumRateForCash,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '20',
    name: MethodFieldParameterEnum.MaximumRateForCash,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '21',
    name: MethodFieldParameterEnum.MinimumRateForMerchandise,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '22',
    name: MethodFieldParameterEnum.MaximumRateForMerchandise,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '23',
    name: MethodFieldParameterEnum.PreventTermsChangeDuringIncentivePricingPeriod,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '24',
    name: MethodFieldParameterEnum.MessageForProtectedBalanceIncentivePricing,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '25',
    name: MethodFieldParameterEnum.InterestRateRoundingCalculation,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '26',
    name: MethodFieldParameterEnum.DailyAPRRoundingCalculation,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '27',
    name: MethodFieldParameterEnum.StopCalculatingInterestAfterNumberofDaysDelinquent,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '28',
    name: MethodFieldParameterEnum.IncludeAdditionalCreditBalanceInAverageDailyBalanceCalculation,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '29',
    name: MethodFieldParameterEnum.CombineMerchandiseAndCashBalancesOnInterestCalculations,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '30',
    name: MethodFieldParameterEnum.RestartInterestCalculationsAfterClearingDelinquency,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '31',
    name: MethodFieldParameterEnum.CalculateCompoundInterest,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '32',
    name: MethodFieldParameterEnum.OverridePromotionalRatesForPricingStrategies,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '33',
    name: MethodFieldParameterEnum.MonthlyInterestCalculationOptions,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '34',
    name: MethodFieldParameterEnum.DailyInterestRoundingOption,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '35',
    name: MethodFieldParameterEnum.DailyInterestAmountTruncationOption,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '36',
    name: MethodFieldParameterEnum.OverrideInterestRatesForProtectedBalance,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '37',
    name: MethodFieldParameterEnum.MinimumDaysForCycleCodeChange,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '38',
    name: MethodFieldParameterEnum.RoundNumberOfDaysInCycleForInterestCalculations,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '39',
    name: MethodFieldParameterEnum.CycleCodeChangeOptions,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '40',
    name: MethodFieldParameterEnum.CyclePeriodChangeCISMemoOption,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '41',
    name: MethodFieldParameterEnum.InterestMethodOldCash,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '42',
    name: MethodFieldParameterEnum.InterestMethodCycleToDateCash,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '42',
    name: MethodFieldParameterEnum.InterestMethodTwoCycleOldCash,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '43',
    name: MethodFieldParameterEnum.InterestMethodOneCycleOldMerchandise,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '44',
    name: MethodFieldParameterEnum.InterestMethodCycleToDateMerchandise,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '45',
    name: MethodFieldParameterEnum.AutomatedMemosForAdditionalInterest,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '46',
    name: MethodFieldParameterEnum.UseExternalStatusesForAdditionalInterest,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '47',
    name: MethodFieldParameterEnum.ExternalStatus1ForAdditionalInterest,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '48',
    name: MethodFieldParameterEnum.ExternalStatus2ForAdditionalInterest,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '49',
    name: MethodFieldParameterEnum.ExternalStatus3ForAdditionalInterest,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '50',
    name: MethodFieldParameterEnum.ExternalStatus4ForAdditionalInterest,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '51',
    name: MethodFieldParameterEnum.ExternalStatus5ForAdditionalInterest,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '52',
    name: MethodFieldParameterEnum.PenaltyPricingStatusReasonCodeTable,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '53',
    name: MethodFieldParameterEnum.PenaltyPricingTimingOptions,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '54',
    name: MethodFieldParameterEnum.PenaltyPricingStatementMessage1CyclesOldBalance,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '55',
    name: MethodFieldParameterEnum.PenaltyPricingStatementMessage2CyclesOldBalance,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '56',
    name: MethodFieldParameterEnum.PenaltyPricingStatementMessage3CyclesOldBalance,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '57',
    name: MethodFieldParameterEnum.CureDelinquencyFor1CycleDelinquentBalances,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '58',
    name: MethodFieldParameterEnum.CureDelinquencyFor2CycleDelinquentBalances,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '59',
    name: MethodFieldParameterEnum.CureDelinquencyFor3CycleDelinquentBalances,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '60',
    name: MethodFieldParameterEnum.PenaltyPricingOnPromotions,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '61',
    name: MethodFieldParameterEnum.CITMethodForPenaltyPricing,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '62',
    name: MethodFieldParameterEnum.IncludeZeroBalanceDaysInADBCalculation,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  },
  {
    id: '63',
    name: MethodFieldParameterEnum.IncludeZeroBalanceDaysInADBCalculation,
    options: [
      {
        value: 'TEXTBRAD',
        description: ''
      }
    ]
  }
];

const mockOnConfirmParams = jest.fn();
jest.mock('app/hooks/useUnsavedChangesRedirect', () => {
  return {
    __esModule: true,
    useUnsavedChangesRedirect:
      () =>
      ({ onConfirm, onDiscard, onCancel }: any) => {
        onDiscard && onDiscard();
        onCancel && onCancel();
        onConfirm && onConfirm(mockOnConfirmParams());
      }
  };
});

describe('WorkflowChangeInterestRates > ConfigureParameters > AddNewMethodModal', () => {
  const onClose = jest.fn();
  const defaultProps = {
    id: 'AddNewMethodModal',
    show: true,
    onClose
  };

  const method: WorkflowSetupMethod = {
    id: 'id',
    name: 'MTHMTCI',
    modeledFrom: {
      id: 'id1',
      name: 'MTHMTCI',
      versions: [{ id: 'id' }]
    },
    methodType: 'NEWVERSION',
    comment: '',
    parameters: [
      {
        name: MethodFieldParameterEnum.DefaultCashBaseInterest,
        previousValue: '0',
        newValue: 'T'
      }
    ]
  };

  const getInitialState = ({ elements = methodData }: any = {}) => ({
    workflowSetup: { elementMetadata: { elements } }
  });

  const queryInputFromLabel = (tierLabel: HTMLElement) => {
    return tierLabel
      .closest(
        '.dls-input-container, .dls-numreric-container, .text-field-container'
      )
      ?.querySelector('input, .input, textarea') as HTMLInputElement;
  };

  beforeEach(() => {
    mockDevice.isDevice = false;
    mockOnConfirmParams.mockReturnValue('');
  });

  afterEach(() => {
    onClose.mockClear();
    jest.clearAllMocks();
  });

  it('should render AddNewMethodModal', async () => {
    mockDevice.isDevice = true;
    const wrapper = await renderWithMockStore(
      <AddNewMethodModal {...defaultProps} type="IP" />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByText('txt_manage_penalty_fee_method_details')
    ).toBeInTheDocument();

    const description = queryInputFromLabel(
      wrapper.getByText('txt_comment_area')
    );
    userEvent.type(description, new Array(239).fill(0).join(''));
    expect(description.textContent?.length).toEqual(239);

    userEvent.click(wrapper.getByText('txt_cancel'));
    expect(onClose).toBeCalled();
  });

  it('should render AddNewMethodModal', async () => {
    mockDevice.isDevice = true;
    const wrapper = await renderWithMockStore(
      <AddNewMethodModal {...defaultProps} type="IP" />,
      { initialState: getInitialState() }
    );

    expect(
      wrapper.getByText('txt_manage_penalty_fee_method_details')
    ).toBeInTheDocument();

    const description = queryInputFromLabel(
      wrapper.getByText('txt_comment_area')
    );
    userEvent.type(description, new Array(239).fill(0).join(''));
    expect(description.textContent?.length).toEqual(239);

    userEvent.click(wrapper.getByText('txt_cancel'));
    expect(onClose).toBeCalled();
  });

  it('should render AddNewMethodModal form new version', async () => {
    const wrapper = await renderWithMockStore(
      <AddNewMethodModal {...defaultProps} method={method} type="IP" />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByText('txt_manage_penalty_fee_method_details')
    ).toBeInTheDocument();

    userEvent.click(wrapper.getByText('txt_back'));
    expect(onClose).toBeCalled();
  });

  it('should render AddNewMethodModal form modeled method', async () => {
    const wrapper = await renderWithMockStore(
      <AddNewMethodModal
        {...defaultProps}
        methodNames={['MTHMTCI1']}
        draftMethod={{
          ...method,
          modeledFrom: { ...method.modeledFrom, name: '' },
          methodType: 'MODELEDMETHOD',
          parameters: undefined as any
        }}
        method={{
          ...method,
          modeledFrom: { ...method.modeledFrom, name: '' },
          methodType: 'MODELEDMETHOD',
          parameters: undefined as any
        }}
      />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByText('txt_manage_penalty_fee_method_details')
    ).toBeInTheDocument();

    const methodName = queryInputFromLabel(
      wrapper.getByText('txt_method_name')
    );
    userEvent.clear(methodName);
    userEvent.type(methodName, 'MTHMTCI1');
    userEvent.click(document.body);
    userEvent.click(methodName);

    expect(
      wrapper.getByText(
        'txt_manage_penalty_fee_validation_method_name_must_be_unique'
      )
    ).toBeInTheDocument();

    userEvent.click(wrapper.getByText('txt_back'));
    expect(onClose).toBeCalled();
  });

  it('should save method form modeled method', async () => {
    const wrapper = await renderWithMockStore(
      <AddNewMethodModal
        methodNames={['MTHMTCI']}
        {...defaultProps}
        method={{ ...method, methodType: 'MODELEDMETHOD' }}
      />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByText('txt_manage_penalty_fee_method_details')
    ).toBeInTheDocument();

    const methodName = queryInputFromLabel(
      wrapper.getByText('txt_method_name')
    );
    userEvent.clear(methodName);
    userEvent.type(methodName, 'MTHMTCI1');
    userEvent.click(wrapper.getByText('txt_save'));
    expect(onClose).toBeCalled();
  });

  it('should save new method', async () => {
    const wrapper = await renderWithMockStore(
      <AddNewMethodModal {...defaultProps} method={undefined} type="IM" />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByText('txt_manage_penalty_fee_method_details')
    ).toBeInTheDocument();

    const methodName = queryInputFromLabel(
      wrapper.getByText('txt_method_name')
    );
    userEvent.type(methodName, 'MTHMTCI1');

    const expandedButtons = wrapper.container.querySelectorAll(
      'table tr td .btn.btn-icon-secondary.btn-sm .icon.icon-plus'
    );
    expandedButtons.forEach(element => userEvent.click(element));

    userEvent.click(wrapper.getByText('txt_save'));
    expect(onClose).toBeCalled();
  });

  it('should save new method', async () => {
    const wrapper = await renderWithMockStore(
      <AddNewMethodModal {...defaultProps} method={undefined} type="IP" />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByText('txt_manage_penalty_fee_method_details')
    ).toBeInTheDocument();

    const methodName = queryInputFromLabel(
      wrapper.getByText('txt_method_name')
    );
    userEvent.type(methodName, 'MTHMTCI1');

    const expandedButtons = wrapper.container.querySelectorAll(
      'table tr td .btn.btn-icon-secondary.btn-sm .icon.icon-plus'
    );
    expandedButtons.forEach(element => userEvent.click(element));

    userEvent.click(wrapper.getByText('txt_save'));
    expect(onClose).toBeCalled();
  });

  it('should render no data filter', async () => {
    const wrapper = await renderWithMockStore(
      <AddNewMethodModal {...defaultProps} method={undefined} />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByText('txt_manage_penalty_fee_method_details')
    ).toBeInTheDocument();

    const searchBox = wrapper.getByPlaceholderText('txt_type_to_search');

    userEvent.type(searchBox, 'a');
    userEvent.click(
      searchBox.parentElement!.querySelector(
        '.icon-simple-search .icon.icon-search'
      )!
    );

    userEvent.type(searchBox, 'notfound');
    userEvent.click(
      searchBox.parentElement!.querySelector(
        '.icon-simple-search .icon.icon-search'
      )!
    );

    expect(wrapper.getByText('txt_parameter_list')).toBeInTheDocument();

    userEvent.click(wrapper.getByText('txt_clear_and_reset'));
    expect(
      wrapper.queryByText(
        'txt_no_results_found txt_adjust_your_search_criteria'
      )
    ).not.toBeInTheDocument();
  });
});
