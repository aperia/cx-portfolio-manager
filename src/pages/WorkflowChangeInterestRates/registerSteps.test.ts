import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { default as stepRegistry } from '../_commons/WorkflowSetup/registries/stepRegistry';

const registerFunc = jest.spyOn(stepRegistry, 'registerStep');

describe('pages > WorkflowChangeInterestRates > registerSteps', () => {
  it('Should register components', async () => {
    await import('./registerSteps');

    expect(registerFunc).toBeCalledWith(
      'GetStartedChangeInterestRatesWorkflow',
      expect.anything(),
      [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CHANGE_INTEREST_RATES__GET_STARTED__EXISTED_WORKFLOW,
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CHANGE_INTEREST_RATES__GET_STARTED__EXISTED_WORKFLOW__STUCK
      ]
    );
    expect(registerFunc).toBeCalledWith(
      'ConfigureParameterChangeInterestRatesDefaultWorkflow',
      expect.anything(),
      [UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CHANGE_INTEREST_RATES_ID]
    );
    expect(registerFunc).toBeCalledWith(
      'ConfigureParameterChangeInterestRatesMethodWorkflow',
      expect.anything(),
      [UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CHANGE_INTEREST_RATES_IM]
    );
    expect(registerFunc).toBeCalledWith(
      'ConfigureParameterChangeInterestRatesInventiveWorkflow',
      expect.anything(),
      [UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CHANGE_INTEREST_RATES_IP]
    );
  });
});
