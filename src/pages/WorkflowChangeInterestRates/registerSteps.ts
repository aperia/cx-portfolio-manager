import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { stepRegistry } from '../_commons/WorkflowSetup/registries';
import ConfigureParameters from './ConfigureParameters';
import GettingStartStep from './GettingStartStep';

stepRegistry.registerStep(
  'GetStartedChangeInterestRatesWorkflow',
  GettingStartStep,
  [
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CHANGE_INTEREST_RATES__GET_STARTED__EXISTED_WORKFLOW,
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CHANGE_INTEREST_RATES__GET_STARTED__EXISTED_WORKFLOW__STUCK
  ]
);

stepRegistry.registerStep(
  'ConfigureParameterChangeInterestRatesDefaultWorkflow',
  ConfigureParameters,
  [UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CHANGE_INTEREST_RATES_ID]
);

stepRegistry.registerStep(
  'ConfigureParameterChangeInterestRatesMethodWorkflow',
  ConfigureParameters,
  [UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CHANGE_INTEREST_RATES_IM]
);

stepRegistry.registerStep(
  'ConfigureParameterChangeInterestRatesInventiveWorkflow',
  ConfigureParameters,
  [UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CHANGE_INTEREST_RATES_IP]
);
