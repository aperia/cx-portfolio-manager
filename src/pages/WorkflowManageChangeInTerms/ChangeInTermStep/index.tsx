import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React from 'react';
import ConfigureParameters, {
  ConfigureParametersFormValue
} from '../ConfigureParameters';
import Summary from '../ConfigureParameters/Summary';
import parseFormValues from './parseFormValues';

const ChangeInTermStep: React.FC<
  WorkflowSetupProps<ConfigureParametersFormValue>
> = prop => {
  return <ConfigureParameters {...prop} />;
};

const ExtraStaticChangeInTermStep = ChangeInTermStep as WorkflowSetupStaticProp;

ExtraStaticChangeInTermStep.summaryComponent = Summary;
ExtraStaticChangeInTermStep.defaultValues = {
  isValid: false,
  isChangeInTermStep: true,
  methods: []
};
ExtraStaticChangeInTermStep.parseFormValues = parseFormValues;

export default ExtraStaticChangeInTermStep;
