import { render } from '@testing-library/react';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import React from 'react';
import SubGridPreviewMethod from './SubGridPreviewMethod';

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

const metadata = {
  [MethodFieldParameterEnum.ChangeInTermsBasicSettingsCitPriorityOrder]:
    'test2',
  [MethodFieldParameterEnum.ChangeInTermsBasicSettingsCisMemoSettings]: 'test',
  [MethodFieldParameterEnum.ChangeInTermsBasicSettingsTermsConditionsTable]:
    'test',
  [MethodFieldParameterEnum.ChangeInTermsBasicSettingsCitPcs]: 'test',
  [MethodFieldParameterEnum.ChangeInTermsBasicSettingsEnableCit]: 'test',
  [MethodFieldParameterEnum.ChangeInTermsCommunicationLetterIdForActice]:
    'test',
  [MethodFieldParameterEnum.ChangeInTermsCommunicationLetterIdForInActice]:
    'test',

  [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcBp]: 'test',
  [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcId]: 'test',
  [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIi]: 'test',
  [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIm]: 'test',
  [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIp]: 'test',
  [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIr]: 'test',
  [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcMe]: 'test',
  [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcMf]: 'test',
  [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcVi]: 'test',
  [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoAc]: 'test',
  [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoCi]: 'test',
  [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoMc]: 'test',
  [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoMi]: 'test',
  [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPfLc]: 'test',
  [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPfOc]: 'test',
  [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPfRc]: 'test',
  [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPoMp]: 'none',

  [MethodFieldParameterEnum.ChangeInTermsPenaltyPricingIndicator]: 'test',

  [MethodFieldParameterEnum.ChangeInTermsProtectedBalancesMethodForCash]:
    'test',
  [MethodFieldParameterEnum.ChangeInTermsProtectedBalancesMethodForMerchandise]:
    'test',
  [MethodFieldParameterEnum.ChangeInTermsProtectedBalancesMethodForNonInterest]:
    'test',
  [MethodFieldParameterEnum.ChangeInTermsTimmingCitnumber]: 'test',
  [MethodFieldParameterEnum.ChangeInTermsTimmingDelayPeriod]: 'test',
  [MethodFieldParameterEnum.ChangeInTermsTimmingNumberOfMonthsOfDisclosureActiveAccounts]:
    'test',
  [MethodFieldParameterEnum.ChangeInTermsTimmingNumberOfMonthsOfDisclosureInActiveAccounts]:
    'test',
  //Advance
  [MethodFieldParameterEnum.ChangeInTermsReasonCode]: 'test1',
  [MethodFieldParameterEnum.ChangeInTermsProtectedBalanceDelayPeriod]: 'test',
  [MethodFieldParameterEnum.ChangeInTermsDisclosedMethodOverridesCpIcIdInterestDefaults]:
    'test',
  [MethodFieldParameterEnum.ChangeInTermsDisclosedMethodOverridesCpIcIdInterestMethods]:
    'none'
};

describe('pages > WorkflowManageChangeInTerms > ChangeInTermStep > SubGridPreviewMethod', () => {
  it('Info_Basic', () => {
    const original = { id: 'Info_Settings' };
    const props = {
      metadata,
      original
    };
    const wrapper = render(<SubGridPreviewMethod {...props} />);
    expect(wrapper.getByText('test2')).toBeInTheDocument();
  });
  it('Info_AdvancedParameters ', () => {
    const original = { id: 'Info_AdvancedParameters' };
    const props = {
      metadata,
      original
    };
    const wrapper = render(<SubGridPreviewMethod {...props} />);
    expect(wrapper.getByText('test1')).toBeInTheDocument();
  });
});
