import { renderComponent } from 'app/utils';
import React from 'react';
import { parameterChangeInTermList } from './constant';
import SubRowGrid from './SubRowGrid';

const t = value => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

jest.mock('./SubGridPreviewMethod', () => ({
  __esModule: true,
  default: () => <div>SubGridPreviewMethod</div>
}));

describe('pages > WorkflowManageChangeInTerms > ChangeInTermStep > SubRowGrid', () => {
  it('render', async () => {
    const original = [
      { newValue: '0', name: 'cit.priority.order' },
      { newValue: '0', name: 'cis.memo.settings' },
      { newValue: '0', name: 'terms.and.conditions.table' },
      { newValue: '0', name: 'cit.pcs.messaging.table' },
      { newValue: '0', name: 'enable.cit.manager.review.process' },

      { newValue: '0', name: 'letter.id.for.active.accounts' },
      { newValue: '0', name: 'letter.id.for.inactive.accounts' },

      { newValue: '0', name: 'disclosed.methods.cp.ic.bp.break.points' },
      { newValue: '0', name: 'disclosed.methods.cp.ic.id.interest.defaults' },
      {
        newValue: '0',
        name: 'disclosed.methods.cp.ic.ii.interest.on.interest'
      },
      { newValue: '0', name: 'disclosed.methods.cp.ic.im.interest.methods' },
      { newValue: '0', name: 'disclosed.methods.cp.ic.ip.incentive.pricing' },
      { newValue: '0', name: 'disclosed.methods.cp.ic.ir.index.rate' },
      { newValue: '0', name: 'disclosed.methods.cp.ic.me.max.cap.eapr' },
      {
        newValue: '0',
        name: 'disclosed.methods.cp.ic.mf.minimum.finance.charge'
      },
      {
        newValue: '0',
        name: 'disclosed.methods.cp.ic.vi.incentive.pricing.variable.interest'
      },
      { newValue: '0', name: 'disclosed.methods.cp.io.ac.annual.charges' },
      {
        newValue: '0',
        name: 'disclosed.methods.cp.io.ci.cash.advance.item.charges'
      },
      {
        newValue: '0',
        name: 'disclosed.methods.cp.io.mc.miscellaneous.charges'
      },
      {
        newValue: '0',
        name: 'disclosed.methods.cp.io.mi.merchandise.item.charges'
      },
      { newValue: '0', name: 'disclosed.methods.cp.pf.lc.late.charges' },
      { newValue: '0', name: 'disclosed.methods.cp.pf.oc.overlimit.charges' },
      {
        newValue: '0',
        name: 'disclosed.methods.cp.pf.rc.return.check.charges'
      },
      { newValue: '0', name: 'disclosed.methods.cp.po.mp.minimum.payment.due' },

      { newValue: '0', name: 'penalty.pricing.change.in.terms.indicator' },

      { newValue: '0', name: 'protected.balance.method.for.cash' },
      { newValue: '0', name: 'protected.balance.method.for.merchandise' },
      {
        newValue: '0',
        name: 'protected.balance.method.for.non.interest.bearing.balance'
      },

      { newValue: '0', name: 'cit.number.of.days.before.terms.effective' },
      { newValue: '0', name: 'delay.period.after.account.opening.date' },
      {
        newValue: '0',
        name: 'number.of.months.of.disclosure.displays.for.active.accounts'
      },
      {
        newValue: '0',
        name: 'number.of.months.of.disclosure.displays.for.inactive.accounts'
      },

      { newValue: '0', name: 'credit.application.group' },
      {
        newValue: '0',
        name: 'choose.the.interest.default.cp.ic.id.methods.to.protect.set.1'
      },
      {
        newValue: '0',
        name: 'choose.the.interest.default.cp.ic.id.methods.to.protect.set.2'
      },
      {
        newValue: '0',
        name: 'choose.the.interest.default.cp.ic.id.methods.to.protect.set.3'
      },
      {
        newValue: '0',
        name: 'choose.the.interest.method.cp.ic.im.methods.to.protect.set.1'
      },
      {
        newValue: '0',
        name: 'choose.the.interest.method.cp.ic.im.methods.to.protect.set.2'
      },
      {
        newValue: '0',
        name: 'choose.the.interest.method.cp.ic.im.methods.to.protect.set.3'
      },
      {
        newValue: '0',
        name: 'choose.the.minimum.payment.pl.rt.mp.methods.to.protect.set.1'
      },
      {
        newValue: '0',
        name: 'choose.the.minimum.payment.pl.rt.mp.methods.to.protect.set.2'
      },
      {
        newValue: '0',
        name: 'choose.the.minimum.payment.pl.rt.mp.methods.to.protect.set.3'
      },
      { newValue: '0', name: 'protected.balance.description' },
      { newValue: '0', name: 'protected.balance.alternate.description' },
      { newValue: '0', name: 'description.display.option' },
      { newValue: '0', name: 'protected.balance.statement.message.text' },
      { newValue: '0', name: 'protected.balance.statement.message.options' },

      { newValue: '0', name: 'protect.balances.for.penalty.pricing' },
      { newValue: '0', name: 'delay.intro.text.id' },
      { newValue: '0', name: 'pb.balance.transfer' },
      { newValue: '0', name: 'protected.balance.override.code' },
      { newValue: '0', name: 'group.identifier' },
      //Advance
      { newValue: '0', name: 'change.in.terms.reason.code' },
      { newValue: '0', name: 'protected.balance.delay.period' },
      {
        newValue: '0',
        name: 'disclosed.method.overrides.cp.ic.id.interest.defaults'
      },
      {
        newValue: '0',
        name: 'disclosed.method.overrides.cp.ic.im.interest.methods'
      }
    ];

    const metadata = {
      changeInTermsBasicSettingsCisMemoSettings: [
        { code: 'code', text: 'text' }
      ],
      changeInTermsBasicSettingsTermsConditionsTable: [
        { code: 'code', text: 'text' }
      ],
      changeInTermsBasicSettingsCitPcs: [{ code: 'code', text: 'text' }],
      changeInTermsBasicSettingsEnableCit: [{ code: 'code', text: 'text' }],
      changeInTermsCommunicationLetterIdForActice: [
        { code: 'code', text: 'text' }
      ],
      changeInTermsCommunicationLetterIdForInActice: [
        { code: 'code', text: 'text' }
      ],
      changeInTermsMethodDisclosureCpIcBp: [{ code: 'code', text: 'text' }],
      changeInTermsMethodDisclosureCpIcId: [{ code: 'code', text: 'text' }],
      changeInTermsMethodDisclosureCpIcIi: [{ code: 'code', text: 'text' }],
      changeInTermsMethodDisclosureCpIcIm: [{ code: 'code', text: 'text' }],
      changeInTermsMethodDisclosureCpIcIp: [{ code: 'code', text: 'text' }],
      changeInTermsMethodDisclosureCpIcIr: [{ code: 'code', text: 'text' }],
      changeInTermsMethodDisclosureCpIcMe: [{ code: 'code', text: 'text' }],
      changeInTermsMethodDisclosureCpIcMf: [{ code: 'code', text: 'text' }],
      changeInTermsMethodDisclosureCpIcVi: [{ code: 'code', text: 'text' }],
      changeInTermsMethodDisclosureCpIoAc: [{ code: 'code', text: 'text' }],
      changeInTermsMethodDisclosureCpIoCi: [{ code: 'code', text: 'text' }],
      changeInTermsMethodDisclosureCpIoMc: [{ code: 'code', text: 'text' }],
      changeInTermsMethodDisclosureCpIoMi: [{ code: 'code', text: 'text' }],
      changeInTermsMethodDisclosureCpPfLc: [{ code: 'code', text: 'text' }],
      changeInTermsMethodDisclosureCpPfOc: [{ code: 'code', text: 'text' }],
      changeInTermsMethodDisclosureCpPfRc: [{ code: 'code', text: 'text' }],
      changeInTermsMethodDisclosureCpPoMp: [{ code: 'code', text: 'text' }],
      changeInTermsPenaltyPricingIndicator: [{ code: 'code', text: 'text' }],
      changeInTermsProtectedBalancesMethodForCash: [
        { code: 'code', text: 'text' }
      ],
      changeInTermsProtectedBalancesMethodForMerchandise: [
        { code: 'code', text: 'text' }
      ],
      changeInTermsProtectedBalancesMethodForNonInterest: [
        { code: 'code', text: 'text' }
      ],
      changeInTermsTimmingCitnumber: [{ code: 'code', text: 'text' }],
      changeInTermsBasicSettingsCitPriorityOrder: [
        { code: 'code', text: 'text' }
      ],
      changeInTermsReasonCode: [{ code: 'code', text: 'text' }],
      changeInTermsDisclosedMethodOverridesCpIcIdInterestDefaultsOptions: [
        { code: 'code', text: 'text' }
      ],
      changeInTermsDisclosedMethodOverridesCpIcIdInterestMethodsOptions: [
        { code: 'code', text: 'text' }
      ]
    };
    const wrapper = await renderComponent(
      'testId',
      <SubRowGrid metadata={metadata} original={original} />
    );

    expect(
      wrapper.getByText(parameterChangeInTermList[0].parameterGroup)
    ).toBeInTheDocument();
    expect(
      wrapper.getByText(parameterChangeInTermList[1].parameterGroup)
    ).toBeInTheDocument();
    expect(
      wrapper.getByText(parameterChangeInTermList[2].parameterGroup)
    ).toBeInTheDocument();
  });
});
