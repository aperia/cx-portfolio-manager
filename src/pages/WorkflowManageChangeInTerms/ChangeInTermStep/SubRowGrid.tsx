import { MethodFieldParameterEnum } from 'app/constants/enums';
import { mapGridExpandCollapse, methodParamsToObject } from 'app/helpers';
import { Grid, useTranslation } from 'app/_libraries/_dls';
import React, { useState } from 'react';
import newMethodColumns from '../ConfigureParameters/newMethodColumns';
import { parameterChangeInTermList } from './constant';
import SubGridPreviewMethod from './SubGridPreviewMethod';
import { ChangeInTermMeta, ParameterChangeInTermListIds } from './types';

export interface SubGridProps {
  original: WorkflowSetupMethod;
  metadata: ChangeInTermMeta;
}
const SubRowGrid: React.FC<SubGridProps> = ({ original, metadata }) => {
  const {
    changeInTermsBasicSettingsCisMemoSettings,
    changeInTermsBasicSettingsTermsConditionsTable,
    changeInTermsBasicSettingsCitPcs,
    changeInTermsBasicSettingsEnableCit,
    changeInTermsCommunicationLetterIdForActice,
    changeInTermsCommunicationLetterIdForInActice,
    changeInTermsMethodDisclosureCpIcBp,
    changeInTermsMethodDisclosureCpIcId,
    changeInTermsMethodDisclosureCpIcIi,
    changeInTermsMethodDisclosureCpIcIm,
    changeInTermsMethodDisclosureCpIcIp,
    changeInTermsMethodDisclosureCpIcIr,
    changeInTermsMethodDisclosureCpIcMe,
    changeInTermsMethodDisclosureCpIcMf,
    changeInTermsMethodDisclosureCpIcVi,
    changeInTermsMethodDisclosureCpIoAc,
    changeInTermsMethodDisclosureCpIoCi,
    changeInTermsMethodDisclosureCpIoMc,
    changeInTermsMethodDisclosureCpIoMi,
    changeInTermsMethodDisclosureCpPfLc,
    changeInTermsMethodDisclosureCpPfOc,
    changeInTermsMethodDisclosureCpPfRc,
    changeInTermsMethodDisclosureCpPoMp,
    changeInTermsPenaltyPricingIndicator,
    changeInTermsProtectedBalancesMethodForCash,
    changeInTermsProtectedBalancesMethodForMerchandise,
    changeInTermsProtectedBalancesMethodForNonInterest,
    changeInTermsDisclosedMethodOverridesCpIcIdInterestDefaultsOptions,
    changeInTermsDisclosedMethodOverridesCpIcIdInterestMethodsOptions,
    changeInTermsReasonCode
  } = metadata;

  const convertedObject = methodParamsToObject(original);

  const { t } = useTranslation();

  const data: Record<MethodFieldParameterEnum, any> = {
    [MethodFieldParameterEnum.ChangeInTermsTimmingCitnumber]:
      convertedObject[MethodFieldParameterEnum.ChangeInTermsTimmingCitnumber],
    [MethodFieldParameterEnum.ChangeInTermsTimmingDelayPeriod]:
      convertedObject[MethodFieldParameterEnum.ChangeInTermsTimmingDelayPeriod],
    [MethodFieldParameterEnum.ChangeInTermsTimmingNumberOfMonthsOfDisclosureActiveAccounts]:
      convertedObject[
        MethodFieldParameterEnum
          .ChangeInTermsTimmingNumberOfMonthsOfDisclosureActiveAccounts
      ],
    [MethodFieldParameterEnum.ChangeInTermsTimmingNumberOfMonthsOfDisclosureInActiveAccounts]:
      convertedObject[
        MethodFieldParameterEnum
          .ChangeInTermsTimmingNumberOfMonthsOfDisclosureInActiveAccounts
      ],
    [MethodFieldParameterEnum.ChangeInTermsBasicSettingsCitPriorityOrder]:
      convertedObject[
        MethodFieldParameterEnum.ChangeInTermsBasicSettingsCitPriorityOrder
      ],
    [MethodFieldParameterEnum.ChangeInTermsBasicSettingsCisMemoSettings]:
      changeInTermsBasicSettingsCisMemoSettings.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.ChangeInTermsBasicSettingsCisMemoSettings
          ]
      )?.text,
    [MethodFieldParameterEnum.ChangeInTermsBasicSettingsTermsConditionsTable]:
      changeInTermsBasicSettingsTermsConditionsTable.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ChangeInTermsBasicSettingsTermsConditionsTable
          ]
      )?.text,
    [MethodFieldParameterEnum.ChangeInTermsBasicSettingsCitPcs]:
      changeInTermsBasicSettingsCitPcs.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.ChangeInTermsBasicSettingsCitPcs
          ]
      )?.text,
    [MethodFieldParameterEnum.ChangeInTermsBasicSettingsEnableCit]:
      changeInTermsBasicSettingsEnableCit.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.ChangeInTermsBasicSettingsEnableCit
          ]
      )?.text,
    [MethodFieldParameterEnum.ChangeInTermsCommunicationLetterIdForActice]:
      changeInTermsCommunicationLetterIdForActice.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.ChangeInTermsCommunicationLetterIdForActice
          ]
      )?.text,
    [MethodFieldParameterEnum.ChangeInTermsCommunicationLetterIdForActice]:
      changeInTermsCommunicationLetterIdForActice.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.ChangeInTermsCommunicationLetterIdForActice
          ]
      )?.text,
    [MethodFieldParameterEnum.ChangeInTermsCommunicationLetterIdForInActice]:
      changeInTermsCommunicationLetterIdForInActice.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ChangeInTermsCommunicationLetterIdForInActice
          ]
      )?.text,
    [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcBp]:
      changeInTermsMethodDisclosureCpIcBp.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcBp
          ]
      )?.text,
    [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcId]:
      changeInTermsMethodDisclosureCpIcId.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcId
          ]
      )?.text,
    [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIi]:
      changeInTermsMethodDisclosureCpIcIi.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIi
          ]
      )?.text,
    [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIm]:
      changeInTermsMethodDisclosureCpIcIm.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIm
          ]
      )?.text,
    [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIp]:
      changeInTermsMethodDisclosureCpIcIp.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIp
          ]
      )?.text,
    [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIr]:
      changeInTermsMethodDisclosureCpIcIr.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIr
          ]
      )?.text,
    [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcMe]:
      changeInTermsMethodDisclosureCpIcMe.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcMe
          ]
      )?.text,

    [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcMf]:
      changeInTermsMethodDisclosureCpIcMf.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcMf
          ]
      )?.text,
    [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcVi]:
      changeInTermsMethodDisclosureCpIcVi.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcVi
          ]
      )?.text,
    [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoAc]:
      changeInTermsMethodDisclosureCpIoAc.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoAc
          ]
      )?.text,
    [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoCi]:
      changeInTermsMethodDisclosureCpIoCi.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoCi
          ]
      )?.text,
    [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoMc]:
      changeInTermsMethodDisclosureCpIoMc.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoMc
          ]
      )?.text,
    [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoMi]:
      changeInTermsMethodDisclosureCpIoMi.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoMi
          ]
      )?.text,
    [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPfLc]:
      changeInTermsMethodDisclosureCpPfLc.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPfLc
          ]
      )?.text,
    [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPfOc]:
      changeInTermsMethodDisclosureCpPfOc.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPfOc
          ]
      )?.text,
    [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPfRc]:
      changeInTermsMethodDisclosureCpPfRc.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPfRc
          ]
      )?.text,
    [MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPoMp]:
      changeInTermsMethodDisclosureCpPoMp.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPoMp
          ]
      )?.text,
    [MethodFieldParameterEnum.ChangeInTermsPenaltyPricingIndicator]:
      changeInTermsPenaltyPricingIndicator.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.ChangeInTermsPenaltyPricingIndicator
          ]
      )?.text,
    [MethodFieldParameterEnum.ChangeInTermsProtectedBalancesMethodForCash]:
      changeInTermsProtectedBalancesMethodForCash.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.ChangeInTermsProtectedBalancesMethodForCash
          ]
      )?.text,
    [MethodFieldParameterEnum.ChangeInTermsProtectedBalancesMethodForMerchandise]:
      changeInTermsProtectedBalancesMethodForMerchandise.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ChangeInTermsProtectedBalancesMethodForMerchandise
          ]
      )?.text,
    [MethodFieldParameterEnum.ChangeInTermsProtectedBalancesMethodForNonInterest]:
      changeInTermsProtectedBalancesMethodForNonInterest.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ChangeInTermsProtectedBalancesMethodForNonInterest
          ]
      )?.text,
    [MethodFieldParameterEnum.ChangeInTermsDisclosedMethodOverridesCpIcIdInterestDefaults]:
      changeInTermsDisclosedMethodOverridesCpIcIdInterestDefaultsOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ChangeInTermsDisclosedMethodOverridesCpIcIdInterestDefaults
          ]
      )?.text,
    [MethodFieldParameterEnum.ChangeInTermsDisclosedMethodOverridesCpIcIdInterestMethods]:
      changeInTermsDisclosedMethodOverridesCpIcIdInterestMethodsOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ChangeInTermsDisclosedMethodOverridesCpIcIdInterestMethods
          ]
      )?.text,
    [MethodFieldParameterEnum.ChangeInTermsReasonCode]:
      changeInTermsReasonCode.find(
        o =>
          o.code ===
          convertedObject[MethodFieldParameterEnum.ChangeInTermsReasonCode]
      )?.text,
    [MethodFieldParameterEnum.ChangeInTermsProtectedBalanceDelayPeriod]:
      convertedObject[
        MethodFieldParameterEnum.ChangeInTermsProtectedBalanceDelayPeriod
      ]
  } as Record<MethodFieldParameterEnum, any>;

  const [expandedList, setExpandedList] = useState<
    ParameterChangeInTermListIds[]
  >(['Info_Settings']);

  return (
    <div className="p-20 overflow-hidden">
      <Grid
        togglable
        columns={newMethodColumns(t)}
        data={parameterChangeInTermList}
        subRow={({ original: _original }: MagicKeyValue) => (
          <SubGridPreviewMethod
            original={_original}
            metadata={data}
            methodType={original.methodType}
          />
        )}
        dataItemKey="id"
        expandedItemKey="id"
        expandedList={expandedList}
        onExpand={setExpandedList as any}
        toggleButtonConfigList={parameterChangeInTermList.map(
          mapGridExpandCollapse('id', t)
        )}
      />
    </div>
  );
};

export default SubRowGrid;
