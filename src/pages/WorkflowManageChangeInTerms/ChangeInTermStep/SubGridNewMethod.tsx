import EnhanceDropdownList from 'app/components/EnhanceDropdownList';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import { matchSearchValue, stringValidate } from 'app/helpers';
import {
  ColumnType,
  ComboBox,
  Grid,
  NumericTextBox,
  TextBox,
  useTranslation
} from 'app/_libraries/_dls';
import { isFunction } from 'app/_libraries/_dls/lodash';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React, { useMemo } from 'react';
import { parameterChangeInTermGroup } from './constant';
import { ChangeInTermMeta, ParameterChangeInTermList } from './types';

interface IProps {
  initialMethod: string;
  searchValue: string;
  method: WorkflowSetupMethodObjectParams;
  original: ParameterChangeInTermList;
  minDate?: Date;
  maxDate?: Date;
  metadata: ChangeInTermMeta;
  onChange: (
    field: keyof WorkflowSetupMethodObjectParams,
    isRefData?: boolean
  ) => (e: any) => void;
}
const SubGridNewMethod: React.FC<IProps> = ({
  searchValue,
  method,
  original,
  metadata,
  onChange,
  initialMethod,
  minDate,
  maxDate
}) => {
  const { t } = useTranslation();

  const {
    changeInTermsBasicSettingsCisMemoSettings,
    changeInTermsBasicSettingsTermsConditionsTable,
    changeInTermsBasicSettingsCitPcs,
    changeInTermsBasicSettingsEnableCit,
    changeInTermsCommunicationLetterIdForActice,
    changeInTermsCommunicationLetterIdForInActice,
    changeInTermsMethodDisclosureCpIcBp,
    changeInTermsMethodDisclosureCpIcId,
    changeInTermsMethodDisclosureCpIcIi,
    changeInTermsMethodDisclosureCpIcIm,
    changeInTermsMethodDisclosureCpIcIp,
    changeInTermsMethodDisclosureCpIcIr,
    changeInTermsMethodDisclosureCpIcMe,
    changeInTermsMethodDisclosureCpIcMf,
    changeInTermsMethodDisclosureCpIcVi,
    changeInTermsMethodDisclosureCpIoAc,
    changeInTermsMethodDisclosureCpIoCi,
    changeInTermsMethodDisclosureCpIoMc,
    changeInTermsMethodDisclosureCpIoMi,
    changeInTermsMethodDisclosureCpPfLc,
    changeInTermsMethodDisclosureCpPfOc,
    changeInTermsMethodDisclosureCpPfRc,
    changeInTermsMethodDisclosureCpPoMp,
    changeInTermsPenaltyPricingIndicator,
    changeInTermsProtectedBalancesMethodForCash,
    changeInTermsProtectedBalancesMethodForMerchandise,
    changeInTermsProtectedBalancesMethodForNonInterest,
    changeInTermsDisclosedMethodOverridesCpIcIdInterestDefaultsOptions,
    changeInTermsDisclosedMethodOverridesCpIcIdInterestMethodsOptions,
    changeInTermsReasonCode
  } = metadata;

  const data = useMemo(
    () =>
      parameterChangeInTermGroup[original.id].filter(p =>
        matchSearchValue(
          [p.fieldName, p.moreInfoText, p.onlinePCF],
          searchValue
        )
      ),
    [original.id, searchValue]
  );

  const renderDropdown = (
    data: RefData[],
    field: keyof WorkflowSetupMethodObjectParams
  ) => {
    const value = data.find(o => o.code === method?.[field]);

    return (
      <EnhanceDropdownList
        placeholder={t('txt_workflow_manage_change_in_terms_select_an_option')}
        dataTestId={`addNewMethod__${field}`}
        size="small"
        value={value}
        onChange={onChange(field, true)}
        options={data}
      />
    );
  };

  const renderCombobox = (
    data: RefData[],
    field: keyof WorkflowSetupMethodObjectParams
  ) => {
    const value = data.find(o => o.code === method?.[field]);

    return (
      <ComboBox
        placeholder={t('txt_workflow_manage_change_in_terms_select_an_option')}
        size="small"
        textField="text"
        value={value}
        onChange={onChange(field, true)}
        noResult={t('txt_no_results_found_without_dot')}
      >
        {data.map(item => (
          <ComboBox.Item key={item.code} value={item} label={item.text} />
        ))}
      </ComboBox>
    );
  };

  const valueAccessor = (data: Record<string, any>) => {
    const paramId = data.id as MethodFieldParameterEnum;

    switch (paramId) {
      case MethodFieldParameterEnum.ChangeInTermsTimmingCitnumber: {
        return (
          <NumericTextBox
            placeholder={t('txt_workflow_manage_change_in_terms_Enter_a_value')}
            dataTestId="addNewMethod__changeInTermsTimmingCitnumber"
            size="sm"
            format="n"
            maxLength={3}
            autoFocus={false}
            value={
              method?.[MethodFieldParameterEnum.ChangeInTermsTimmingCitnumber]
            }
            onChange={onChange(
              MethodFieldParameterEnum.ChangeInTermsTimmingCitnumber
            )}
          />
        );
      }
      case MethodFieldParameterEnum.ChangeInTermsTimmingDelayPeriod: {
        return (
          <NumericTextBox
            placeholder={t('txt_workflow_manage_change_in_terms_Enter_a_value')}
            dataTestId="addNewMethod__changeInTermsTimmingDelayPeriod"
            size="sm"
            format="n"
            maxLength={3}
            autoFocus={false}
            value={
              method?.[MethodFieldParameterEnum.ChangeInTermsTimmingDelayPeriod]
            }
            onChange={onChange(
              MethodFieldParameterEnum.ChangeInTermsTimmingDelayPeriod
            )}
          />
        );
      }
      case MethodFieldParameterEnum.ChangeInTermsTimmingNumberOfMonthsOfDisclosureActiveAccounts: {
        return (
          <NumericTextBox
            placeholder={t('txt_workflow_manage_change_in_terms_Enter_a_value')}
            dataTestId="addNewMethod__ChangeInTermsTimmingNumberOfMonthsOfDisclosureActiveAccounts"
            size="sm"
            format="n"
            maxLength={2}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum
                  .ChangeInTermsTimmingNumberOfMonthsOfDisclosureActiveAccounts
              ]
            }
            onChange={onChange(
              MethodFieldParameterEnum.ChangeInTermsTimmingNumberOfMonthsOfDisclosureActiveAccounts
            )}
          />
        );
      }
      case MethodFieldParameterEnum.ChangeInTermsTimmingNumberOfMonthsOfDisclosureInActiveAccounts: {
        return (
          <NumericTextBox
            placeholder={t('txt_workflow_manage_change_in_terms_Enter_a_value')}
            dataTestId="addNewMethod__changeInTermsTimmingNumberOfMonthsOfDisclosureInActiveAccounts"
            size="sm"
            format="n"
            maxLength={2}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum
                  .ChangeInTermsTimmingNumberOfMonthsOfDisclosureInActiveAccounts
              ]
            }
            onChange={onChange(
              MethodFieldParameterEnum.ChangeInTermsTimmingNumberOfMonthsOfDisclosureInActiveAccounts
            )}
          />
        );
      }
      case MethodFieldParameterEnum.ChangeInTermsBasicSettingsCitPriorityOrder: {
        return (
          <TextBox
            placeholder={t('txt_workflow_manage_change_in_terms_Enter_a_value')}
            dataTestId="addNewMethod__changeInTermsBasicSettingsCitPriorityOrder"
            size="sm"
            maxLength={3}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum
                  .ChangeInTermsBasicSettingsCitPriorityOrder
              ]
            }
            onChange={e => {
              const value = e.target.value;
              if (!stringValidate(value).isNumber()) {
                return false;
              }
              isFunction(onChange) &&
                onChange(
                  MethodFieldParameterEnum.ChangeInTermsBasicSettingsCitPriorityOrder
                )(e);
            }}
          />
        );
      }
      case MethodFieldParameterEnum.ChangeInTermsBasicSettingsCisMemoSettings: {
        return renderDropdown(
          changeInTermsBasicSettingsCisMemoSettings,
          MethodFieldParameterEnum.ChangeInTermsBasicSettingsCisMemoSettings
        );
      }

      case MethodFieldParameterEnum.ChangeInTermsBasicSettingsTermsConditionsTable: {
        return renderCombobox(
          changeInTermsBasicSettingsTermsConditionsTable,
          MethodFieldParameterEnum.ChangeInTermsBasicSettingsTermsConditionsTable
        );
      }

      case MethodFieldParameterEnum.ChangeInTermsBasicSettingsCitPcs: {
        return renderDropdown(
          changeInTermsBasicSettingsCitPcs,
          MethodFieldParameterEnum.ChangeInTermsBasicSettingsCitPcs
        );
      }

      case MethodFieldParameterEnum.ChangeInTermsBasicSettingsEnableCit: {
        return renderDropdown(
          changeInTermsBasicSettingsEnableCit,
          MethodFieldParameterEnum.ChangeInTermsBasicSettingsEnableCit
        );
      }

      case MethodFieldParameterEnum.ChangeInTermsCommunicationLetterIdForActice: {
        return renderCombobox(
          changeInTermsCommunicationLetterIdForActice,
          MethodFieldParameterEnum.ChangeInTermsCommunicationLetterIdForActice
        );
      }

      case MethodFieldParameterEnum.ChangeInTermsCommunicationLetterIdForInActice: {
        return renderCombobox(
          changeInTermsCommunicationLetterIdForInActice,
          MethodFieldParameterEnum.ChangeInTermsCommunicationLetterIdForInActice
        );
      }

      case MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcBp: {
        return renderDropdown(
          changeInTermsMethodDisclosureCpIcBp,
          MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcBp
        );
      }
      case MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcId: {
        return renderDropdown(
          changeInTermsMethodDisclosureCpIcId,
          MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcId
        );
      }
      case MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIi: {
        return renderDropdown(
          changeInTermsMethodDisclosureCpIcIi,
          MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIi
        );
      }
      case MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIm: {
        return renderDropdown(
          changeInTermsMethodDisclosureCpIcIm,
          MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIm
        );
      }
      case MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIp: {
        return renderDropdown(
          changeInTermsMethodDisclosureCpIcIp,
          MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIp
        );
      }
      case MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIr: {
        return renderDropdown(
          changeInTermsMethodDisclosureCpIcIr,
          MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIr
        );
      }
      case MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcMe: {
        return renderDropdown(
          changeInTermsMethodDisclosureCpIcMe,
          MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcMe
        );
      }
      case MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcMf: {
        return renderDropdown(
          changeInTermsMethodDisclosureCpIcMf,
          MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcMf
        );
      }
      case MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcVi: {
        return renderDropdown(
          changeInTermsMethodDisclosureCpIcVi,
          MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcVi
        );
      }
      case MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoAc: {
        return renderDropdown(
          changeInTermsMethodDisclosureCpIoAc,
          MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoAc
        );
      }
      case MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoCi: {
        return renderDropdown(
          changeInTermsMethodDisclosureCpIoCi,
          MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoCi
        );
      }
      case MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoMc: {
        return renderDropdown(
          changeInTermsMethodDisclosureCpIoMc,
          MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoMc
        );
      }
      case MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoMi: {
        return renderDropdown(
          changeInTermsMethodDisclosureCpIoMi,
          MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoMi
        );
      }
      case MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPfLc: {
        return renderDropdown(
          changeInTermsMethodDisclosureCpPfLc,
          MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPfLc
        );
      }
      case MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPfOc: {
        return renderDropdown(
          changeInTermsMethodDisclosureCpPfOc,
          MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPfOc
        );
      }
      case MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPfRc: {
        return renderDropdown(
          changeInTermsMethodDisclosureCpPfRc,
          MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPfRc
        );
      }
      case MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPoMp: {
        return renderDropdown(
          changeInTermsMethodDisclosureCpPoMp,
          MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPoMp
        );
      }
      case MethodFieldParameterEnum.ChangeInTermsPenaltyPricingIndicator: {
        return renderDropdown(
          changeInTermsPenaltyPricingIndicator,
          MethodFieldParameterEnum.ChangeInTermsPenaltyPricingIndicator
        );
      }

      case MethodFieldParameterEnum.ChangeInTermsProtectedBalancesMethodForCash: {
        return renderCombobox(
          changeInTermsProtectedBalancesMethodForCash,
          MethodFieldParameterEnum.ChangeInTermsProtectedBalancesMethodForCash
        );
      }

      case MethodFieldParameterEnum.ChangeInTermsProtectedBalancesMethodForMerchandise: {
        return renderCombobox(
          changeInTermsProtectedBalancesMethodForMerchandise,
          MethodFieldParameterEnum.ChangeInTermsProtectedBalancesMethodForMerchandise
        );
      }

      case MethodFieldParameterEnum.ChangeInTermsProtectedBalancesMethodForNonInterest: {
        return renderCombobox(
          changeInTermsProtectedBalancesMethodForNonInterest,
          MethodFieldParameterEnum.ChangeInTermsProtectedBalancesMethodForNonInterest
        );
      }
      //advance
      case MethodFieldParameterEnum.ChangeInTermsReasonCode: {
        return renderCombobox(
          changeInTermsReasonCode,
          MethodFieldParameterEnum.ChangeInTermsReasonCode
        );
      }
      case MethodFieldParameterEnum.ChangeInTermsProtectedBalanceDelayPeriod: {
        return (
          <NumericTextBox
            placeholder={t('txt_workflow_manage_change_in_terms_Enter_a_value')}
            dataTestId="addNewMethod__ChangeInTermsProtectedBalanceDelayPeriod"
            size="sm"
            format="n"
            maxLength={3}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum
                  .ChangeInTermsProtectedBalanceDelayPeriod
              ]
            }
            onChange={onChange(
              MethodFieldParameterEnum.ChangeInTermsProtectedBalanceDelayPeriod
            )}
          />
        );
      }

      case MethodFieldParameterEnum.ChangeInTermsDisclosedMethodOverridesCpIcIdInterestDefaults: {
        return renderDropdown(
          changeInTermsDisclosedMethodOverridesCpIcIdInterestDefaultsOptions,
          MethodFieldParameterEnum.ChangeInTermsDisclosedMethodOverridesCpIcIdInterestDefaults
        );
      }

      case MethodFieldParameterEnum.ChangeInTermsDisclosedMethodOverridesCpIcIdInterestMethods: {
        return renderDropdown(
          changeInTermsDisclosedMethodOverridesCpIcIdInterestMethodsOptions,
          MethodFieldParameterEnum.ChangeInTermsDisclosedMethodOverridesCpIcIdInterestMethods
        );
      }
    }
  };

  const columns: ColumnType[] = [
    {
      id: 'fieldName',
      Header: t('txt_business_name'),
      accessor: 'fieldName',
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'onlinePCF',
      Header: t('txt_manage_penalty_fee_online_PCF'),
      accessor: 'onlinePCF',
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'value',
      Header: t('txt_value'),
      cellBodyProps: { className: 'py-8' },
      accessor: valueAccessor
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      accessor: viewMoreInfo(['moreInfo'], t),
      width: 105,
      cellBodyProps: { className: 'pt-12 pb-8' }
    }
  ];

  return (
    <div className="p-20">
      <Grid columns={columns} data={data} />
    </div>
  );
};

export default SubGridNewMethod;
