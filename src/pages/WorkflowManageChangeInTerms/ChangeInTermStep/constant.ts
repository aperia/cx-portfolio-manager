import {
  MethodFieldNameEnum,
  MethodFieldParameterEnum
} from 'app/constants/enums';
import {
  ParameterChangeInTermGroup,
  ParameterChangeInTermList,
  ParameterChangeInTermListIds
} from './types';

export const ONLINE_PCF = {
  //

  ChangeInTermsBasicSettingsCitPriorityOrder: 'Priority',
  ChangeInTermsBasicSettingsCisMemoSettings: 'CIS Memo',
  ChangeInTermsBasicSettingsTermsConditionsTable: 'Disclosure Table ID',
  ChangeInTermsBasicSettingsCitPcs: 'PCS CIT Table ID',
  ChangeInTermsBasicSettingsEnableCit: 'CIT Review Indicator',

  ChangeInTermsCommunicationLetterIdForActice:
    'Cycle Time Letter ID Active Accts',
  ChangeInTermsCommunicationLetterIdForInActice:
    'Cycle Time Letter ID Inactive Accts',

  ChangeInTermsMethodDisclosureCpIcBp: 'CP IC BP Disclosure Flag',
  ChangeInTermsMethodDisclosureCpIcId: 'CP IC ID Disclosure Flag',
  ChangeInTermsMethodDisclosureCpIcIi: 'CP IC II Disclosure Flag',
  ChangeInTermsMethodDisclosureCpIcIm: 'CP IC IM Disclosure Flag',
  ChangeInTermsMethodDisclosureCpIcIp: 'CP IC IP Disclosure Flag',
  ChangeInTermsMethodDisclosureCpIcIr: 'CP IC IR Disclosure Flag',
  ChangeInTermsMethodDisclosureCpIcMe: 'CP IC ME Disclosure Flag',
  ChangeInTermsMethodDisclosureCpIcMf: 'CP IC MF Disclosure Flag',
  ChangeInTermsMethodDisclosureCpIcVi: 'CP IC VI Disclosure Flag',
  ChangeInTermsMethodDisclosureCpIoAc: 'CP IO AC Disclosure Flag',
  ChangeInTermsMethodDisclosureCpIoCi: 'CP IO CI Disclosure Flag',
  ChangeInTermsMethodDisclosureCpIoMc: 'CP IO MC Disclosure Flag',
  ChangeInTermsMethodDisclosureCpIoMi: 'CP IO MI Disclosure Flag',
  ChangeInTermsMethodDisclosureCpPfLc: 'CP PF LC Disclosure Flag',
  ChangeInTermsMethodDisclosureCpPfOc: 'CP PF OC Disclosure Flag',
  ChangeInTermsMethodDisclosureCpPfRc: 'CP PF RC Disclosure Flag',
  ChangeInTermsMethodDisclosureCpPoMp: 'CP PO MP Disclosure Flag',

  ChangeInTermsPenaltyPricingIndicator: 'Penalty Priority',

  ChangeInTermsProtectedBalancesMethodForCash: 'Cash Balance',
  ChangeInTermsProtectedBalancesMethodForMerchandise: 'Merch Balance',
  ChangeInTermsProtectedBalancesMethodForNonInterest: 'Non-Int Bearing Balance',
  ChangeInTermsTimmingCitnumber: 'Change Effective Number of Days',
  ChangeInTermsTimmingDelayPeriod: 'New Account Delay Number of Days',
  ChangeInTermsTimmingNumberOfMonthsOfDisclosureActiveAccounts:
    'Notification Cycles Active Accts',
  ChangeInTermsTimmingNumberOfMonthsOfDisclosureInActiveAccounts:
    'Notification Cycles Inactive Accts',

  //Advance
  ChangeInTermsReasonCode: 'CIT Reason Code',
  ChangeInTermsProtectedBalanceDelayPeriod: 'Protect Balance Number of Days',
  ChangeInTermsDisclosedMethodOverridesCpIcIdInterestDefaults:
    'CP IC ID Disc MO Excl',
  ChangeInTermsDisclosedMethodOverridesCpIcIdInterestMethods:
    'CP IC IM Disc MO Excl'
};

export const MORE_INFO = {
  ChangeInTermsBasicSettingsCitPriorityOrder:
    'Use the CIT Priority Order parameter to assign a priority number to a change in terms (CIT) method',
  ChangeInTermsBasicSettingsCisMemoSettings:
    'The CIS Memo Settings parameter determines whether a change in terms triggers a Customer Inquiry System (CIS) memo as well as whether the CIS memo created is temporary or permanent.',
  ChangeInTermsBasicSettingsTermsConditionsTable: `The Terms and Conditions Table parameter contains the identifier of the table in the TC, Terms and Conditions table area, that provides the disclosure messages for
    disclosure notices. The System uses this parameter in conjunction with the SRVC/SBJC/SCTN Disclosure Flag parameters to identify the text it must retrieve for disclosure letters or statement messages.`,
  ChangeInTermsBasicSettingsCitPcs: `The CIT PCS Messaging Table parameter contains the identifier of the Parameter Control System (PCS) table that contains the disclosure messages for changes in terms. The System uses this parameter in conjunction with the SRVC/SBJC/SCTN Disclosure Flag parameters to identify the text it must retrieve for disclosure letters or statement messages.`,
  ChangeInTermsBasicSettingsEnableCit: `The Enable CIT Manager Review Process parameter determines whether the System runs the change in terms (CIT) review process available via the Change In Terms ManagerSM service.`,

  ChangeInTermsCommunicationLetterIdForActice:
    'Use the Letter ID for Active Accounts parameter to specify the identifier of the CIT notification letter you want to send to customers with active accounts. The System will generate the letter on the first statement cycle date after the CIT effective date.',
  ChangeInTermsCommunicationLetterIdForInActice:
    'Use the Letter ID for Inactive Accounts parameter to specify the identifier of the CIT notification letter you want to send to customers with inactive accounts. The System will generate the letter on the first statement cycle date after the CIT effective date.',
  ChangeInTermsMethodDisclosureCpIcBp:
    'This parameter identifies the service/subject/section containing the terms for which you want to notify customers.',
  ChangeInTermsMethodDisclosureCpIcId:
    'This parameter identifies the service/subject/section containing the terms for which you want to notify customers.',
  ChangeInTermsMethodDisclosureCpIcIi:
    'This parameter identifies the service/subject/section containing the terms for which you want to notify customers.',
  ChangeInTermsMethodDisclosureCpIcIm:
    'This parameter identifies the service/subject/section containing the terms for which you want to notify customers.',
  ChangeInTermsMethodDisclosureCpIcIp:
    'This parameter identifies the service/subject/section containing the terms for which you want to notify customers.',
  ChangeInTermsMethodDisclosureCpIcIr:
    'This parameter identifies the service/subject/section containing the terms for which you want to notify customers.',
  ChangeInTermsMethodDisclosureCpIcMe:
    'This parameter identifies the service/subject/section containing the terms for which you want to notify customers.',
  ChangeInTermsMethodDisclosureCpIcMf:
    'This parameter identifies the service/subject/section containing the terms for which you want to notify customers.',
  ChangeInTermsMethodDisclosureCpIcVi:
    'This parameter identifies the service/subject/section containing the terms for which you want to notify customers.',
  ChangeInTermsMethodDisclosureCpIoAc:
    'This parameter identifies the service/subject/section containing the terms for which you want to notify customers.',
  ChangeInTermsMethodDisclosureCpIoCi:
    'This parameter identifies the service/subject/section containing the terms for which you want to notify customers.',
  ChangeInTermsMethodDisclosureCpIoMc:
    'This parameter identifies the service/subject/section containing the terms for which you want to notify customers.',
  ChangeInTermsMethodDisclosureCpIoMi:
    'This parameter identifies the service/subject/section containing the terms for which you want to notify customers.',
  ChangeInTermsMethodDisclosureCpPfLc:
    'This parameter identifies the service/subject/section containing the terms for which you want to notify customers.',
  ChangeInTermsMethodDisclosureCpPfOc:
    'This parameter identifies the service/subject/section containing the terms for which you want to notify customers.',
  ChangeInTermsMethodDisclosureCpPfRc:
    'This parameter identifies the service/subject/section containing the terms for which you want to notify customers.',
  ChangeInTermsMethodDisclosureCpPoMp:
    'This parameter identifies the service/subject/section containing the terms for which you want to notify customers.',
  ChangeInTermsPenaltyPricingIndicator:
    'The Penalty Pricing Change in Terms Indicator parameter identifies the CIT method as one directing a penalty pricing change in terms.',
  ChangeInTermsProtectedBalancesMethodForCash:
    'Use the Protected Balance Method for Cash parameter to specify the identifier of the protected balance method you want applied to cash balances',
  ChangeInTermsProtectedBalancesMethodForMerchandise:
    'Use the Protected Balance Method for Merchandise parameter to specify the identifier of the protected balance method you want applied to merchandise balances',
  ChangeInTermsProtectedBalancesMethodForNonInterest:
    'Use the Protected Balance Method for Non-Interest Bearing Balance parameter to specify the identifier of the protected balance method you want to apply to non-interest bearing balances',
  ChangeInTermsTimmingCitnumber: `The CIT Number of Days Before Terms Effective parameter contains the delay period in days after the account allocation date that the strategy associated with the
    CIT method becomes effective for existing accounts.`,
  ChangeInTermsTimmingDelayPeriod:
    'The Delay Period After Account Opening Date parameter contains the delay period in days after the account open date that the strategy associated with the CIT method becomes effective for accounts that have been open for less than a year',
  ChangeInTermsTimmingNumberOfMonthsOfDisclosureActiveAccounts:
    'The Number of Months of Disclosure Displays for Active Accounts parameter determines the number of times the System will place change in terms (CIT) disclosure messages on statements for active accounts.',
  ChangeInTermsTimmingNumberOfMonthsOfDisclosureInActiveAccounts:
    'The Number of Months of Disclosure Displays for Inactive Accounts parameter determines the number of times the System will place change in terms disclosure messages on statements for inactive accounts.',
  //Advance
  ChangeInTermsReasonCode:
    'Use the Change in Terms Reason Code parameter to establish a client-defined code representing the reason for the change in terms directed by the CIT method.',
  ChangeInTermsProtectedBalanceDelayPeriod:
    'The Protected Balance Delay Period (days) parameter specifies the delay period, in days, that the System will protect the balances. The delay period begins when the System generates the change in terms (CIT) disclosure, which is determined by the CIT effective date.',
  ChangeInTermsDisclosedMethodOverridesCpIcIdInterestDefaults:
    'This parameter determines if a method override for the service/subject/section provides terms for disclosure message variables or if the strategy provides the terms.',
  ChangeInTermsDisclosedMethodOverridesCpIcIdInterestMethods:
    'This parameter determines if a method override for the service/subject/section provides terms for disclosure message variables or if the strategy provides the terms.'
};

export const parameterChangeInTermList: ParameterChangeInTermList[] = [
  {
    id: 'Info_Settings',
    section: 'txt_manage_penalty_fee_standard_parameters',
    parameterGroup: 'Basic Settings'
  },
  {
    id: 'Info_Timing',
    section: 'txt_manage_penalty_fee_standard_parameters',
    parameterGroup: 'Timing'
  },
  {
    id: 'Info_ProtectedBalances',
    section: 'txt_manage_penalty_fee_standard_parameters',
    parameterGroup: 'Protected Balances'
  },

  {
    id: 'Info_Communication',
    section: 'txt_manage_penalty_fee_standard_parameters',
    parameterGroup: 'Communication'
  },

  {
    id: 'Info_MethodDisclosureDecisions',
    section: 'txt_manage_penalty_fee_standard_parameters',
    parameterGroup: 'Method Disclosure Decisions'
  },
  {
    id: 'Info_PenaltyPricing',
    section: 'txt_manage_penalty_fee_standard_parameters',
    parameterGroup: 'Penalty Pricing'
  },
  {
    id: 'Info_AdvancedParameters',
    section: 'txt_manage_penalty_fee_advanced_parameters',
    parameterGroup: 'Miscellaneous Parameters'
  }
];

export const parameterChangeInTermGroup: Record<
  ParameterChangeInTermListIds,
  ParameterChangeInTermGroup[]
> = {
  Info_Settings: [
    {
      id: MethodFieldParameterEnum.ChangeInTermsBasicSettingsCitPriorityOrder,
      fieldName: MethodFieldNameEnum.ChangeInTermsBasicSettingsCitPriorityOrder,
      onlinePCF: ONLINE_PCF.ChangeInTermsBasicSettingsCitPriorityOrder,
      moreInfoText: MORE_INFO.ChangeInTermsBasicSettingsCitPriorityOrder,
      moreInfo: MORE_INFO.ChangeInTermsBasicSettingsCitPriorityOrder
    },
    {
      id: MethodFieldParameterEnum.ChangeInTermsBasicSettingsCisMemoSettings,
      fieldName: MethodFieldNameEnum.ChangeInTermsBasicSettingsCisMemoSettings,
      onlinePCF: ONLINE_PCF.ChangeInTermsBasicSettingsCisMemoSettings,
      moreInfoText: MORE_INFO.ChangeInTermsBasicSettingsCisMemoSettings,
      moreInfo: MORE_INFO.ChangeInTermsBasicSettingsCisMemoSettings
    },
    {
      id: MethodFieldParameterEnum.ChangeInTermsBasicSettingsTermsConditionsTable,
      fieldName:
        MethodFieldNameEnum.ChangeInTermsBasicSettingsTermsConditionsTable,
      onlinePCF: ONLINE_PCF.ChangeInTermsBasicSettingsTermsConditionsTable,
      moreInfoText: MORE_INFO.ChangeInTermsBasicSettingsTermsConditionsTable,
      moreInfo: MORE_INFO.ChangeInTermsBasicSettingsTermsConditionsTable
    },
    {
      id: MethodFieldParameterEnum.ChangeInTermsBasicSettingsCitPcs,
      fieldName: MethodFieldNameEnum.ChangeInTermsBasicSettingsCitPcs,
      onlinePCF: ONLINE_PCF.ChangeInTermsBasicSettingsCitPcs,
      moreInfoText: MORE_INFO.ChangeInTermsBasicSettingsCitPcs,
      moreInfo: MORE_INFO.ChangeInTermsBasicSettingsCitPcs
    },
    {
      id: MethodFieldParameterEnum.ChangeInTermsBasicSettingsEnableCit,
      fieldName: MethodFieldNameEnum.ChangeInTermsBasicSettingsEnableCit,
      onlinePCF: ONLINE_PCF.ChangeInTermsBasicSettingsEnableCit,
      moreInfoText: MORE_INFO.ChangeInTermsBasicSettingsEnableCit,
      moreInfo: MORE_INFO.ChangeInTermsBasicSettingsEnableCit
    }
  ],
  Info_Communication: [
    {
      id: MethodFieldParameterEnum.ChangeInTermsCommunicationLetterIdForActice,
      fieldName:
        MethodFieldNameEnum.ChangeInTermsCommunicationLetterIdForActice,
      onlinePCF: ONLINE_PCF.ChangeInTermsCommunicationLetterIdForActice,
      moreInfoText: MORE_INFO.ChangeInTermsCommunicationLetterIdForActice,
      moreInfo: MORE_INFO.ChangeInTermsCommunicationLetterIdForActice
    },
    {
      id: MethodFieldParameterEnum.ChangeInTermsCommunicationLetterIdForInActice,
      fieldName:
        MethodFieldNameEnum.ChangeInTermsCommunicationLetterIdForInActice,
      onlinePCF: ONLINE_PCF.ChangeInTermsCommunicationLetterIdForInActice,
      moreInfoText: MORE_INFO.ChangeInTermsCommunicationLetterIdForInActice,
      moreInfo: MORE_INFO.ChangeInTermsCommunicationLetterIdForInActice
    }
  ],
  Info_Timing: [
    {
      id: MethodFieldParameterEnum.ChangeInTermsTimmingCitnumber,
      fieldName: MethodFieldNameEnum.ChangeInTermsTimmingCitnumber,
      onlinePCF: ONLINE_PCF.ChangeInTermsTimmingCitnumber,
      moreInfoText: MORE_INFO.ChangeInTermsTimmingCitnumber,
      moreInfo: MORE_INFO.ChangeInTermsTimmingCitnumber
    },
    {
      id: MethodFieldParameterEnum.ChangeInTermsTimmingDelayPeriod,
      fieldName: MethodFieldNameEnum.ChangeInTermsTimmingDelayPeriod,
      onlinePCF: ONLINE_PCF.ChangeInTermsTimmingDelayPeriod,
      moreInfoText: MORE_INFO.ChangeInTermsTimmingDelayPeriod,
      moreInfo: MORE_INFO.ChangeInTermsTimmingDelayPeriod
    },
    {
      id: MethodFieldParameterEnum.ChangeInTermsTimmingNumberOfMonthsOfDisclosureActiveAccounts,
      fieldName:
        MethodFieldNameEnum.ChangeInTermsTimmingNumberOfMonthsOfDisclosureActiveAccounts,
      onlinePCF:
        ONLINE_PCF.ChangeInTermsTimmingNumberOfMonthsOfDisclosureActiveAccounts,
      moreInfoText:
        MORE_INFO.ChangeInTermsTimmingNumberOfMonthsOfDisclosureActiveAccounts,
      moreInfo:
        MORE_INFO.ChangeInTermsTimmingNumberOfMonthsOfDisclosureActiveAccounts
    },
    {
      id: MethodFieldParameterEnum.ChangeInTermsTimmingNumberOfMonthsOfDisclosureInActiveAccounts,
      fieldName:
        MethodFieldNameEnum.ChangeInTermsTimmingNumberOfMonthsOfDisclosureInActiveAccounts,
      onlinePCF:
        ONLINE_PCF.ChangeInTermsTimmingNumberOfMonthsOfDisclosureInActiveAccounts,
      moreInfoText:
        MORE_INFO.ChangeInTermsTimmingNumberOfMonthsOfDisclosureInActiveAccounts,
      moreInfo:
        MORE_INFO.ChangeInTermsTimmingNumberOfMonthsOfDisclosureInActiveAccounts
    }
  ],
  Info_ProtectedBalances: [
    {
      id: MethodFieldParameterEnum.ChangeInTermsProtectedBalancesMethodForCash,
      fieldName:
        MethodFieldNameEnum.ChangeInTermsProtectedBalancesMethodForCash,
      onlinePCF: ONLINE_PCF.ChangeInTermsProtectedBalancesMethodForCash,
      moreInfoText: MORE_INFO.ChangeInTermsProtectedBalancesMethodForCash,
      moreInfo: MORE_INFO.ChangeInTermsProtectedBalancesMethodForCash
    },
    {
      id: MethodFieldParameterEnum.ChangeInTermsProtectedBalancesMethodForMerchandise,
      fieldName:
        MethodFieldNameEnum.ChangeInTermsProtectedBalancesMethodForMerchandise,
      onlinePCF: ONLINE_PCF.ChangeInTermsProtectedBalancesMethodForMerchandise,
      moreInfoText:
        MORE_INFO.ChangeInTermsProtectedBalancesMethodForMerchandise,
      moreInfo: MORE_INFO.ChangeInTermsProtectedBalancesMethodForMerchandise
    },
    {
      id: MethodFieldParameterEnum.ChangeInTermsProtectedBalancesMethodForNonInterest,
      fieldName:
        MethodFieldNameEnum.ChangeInTermsProtectedBalancesMethodForNonInterest,
      onlinePCF: ONLINE_PCF.ChangeInTermsProtectedBalancesMethodForNonInterest,
      moreInfoText:
        MORE_INFO.ChangeInTermsProtectedBalancesMethodForNonInterest,
      moreInfo: MORE_INFO.ChangeInTermsProtectedBalancesMethodForNonInterest
    }
  ],
  Info_MethodDisclosureDecisions: [
    {
      id: MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcBp,
      fieldName: MethodFieldNameEnum.ChangeInTermsMethodDisclosureCpIcBp,
      onlinePCF: ONLINE_PCF.ChangeInTermsMethodDisclosureCpIcBp,
      moreInfoText: MORE_INFO.ChangeInTermsMethodDisclosureCpIcBp,
      moreInfo: MORE_INFO.ChangeInTermsMethodDisclosureCpIcBp
    },
    {
      id: MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcId,
      fieldName: MethodFieldNameEnum.ChangeInTermsMethodDisclosureCpIcId,
      onlinePCF: ONLINE_PCF.ChangeInTermsMethodDisclosureCpIcId,
      moreInfoText: MORE_INFO.ChangeInTermsMethodDisclosureCpIcId,
      moreInfo: MORE_INFO.ChangeInTermsMethodDisclosureCpIcId
    },
    {
      id: MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIi,
      fieldName: MethodFieldNameEnum.ChangeInTermsMethodDisclosureCpIcIi,
      onlinePCF: ONLINE_PCF.ChangeInTermsMethodDisclosureCpIcIi,
      moreInfoText: MORE_INFO.ChangeInTermsMethodDisclosureCpIcIi,
      moreInfo: MORE_INFO.ChangeInTermsMethodDisclosureCpIcIi
    },
    {
      id: MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIm,
      fieldName: MethodFieldNameEnum.ChangeInTermsMethodDisclosureCpIcIm,
      onlinePCF: ONLINE_PCF.ChangeInTermsMethodDisclosureCpIcIm,
      moreInfoText: MORE_INFO.ChangeInTermsMethodDisclosureCpIcIm,
      moreInfo: MORE_INFO.ChangeInTermsMethodDisclosureCpIcIm
    },
    {
      id: MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIp,
      fieldName: MethodFieldNameEnum.ChangeInTermsMethodDisclosureCpIcIp,
      onlinePCF: ONLINE_PCF.ChangeInTermsMethodDisclosureCpIcIp,
      moreInfoText: MORE_INFO.ChangeInTermsMethodDisclosureCpIcIp,
      moreInfo: MORE_INFO.ChangeInTermsMethodDisclosureCpIcIp
    },
    {
      id: MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIr,
      fieldName: MethodFieldNameEnum.ChangeInTermsMethodDisclosureCpIcIr,
      onlinePCF: ONLINE_PCF.ChangeInTermsMethodDisclosureCpIcIr,
      moreInfoText: MORE_INFO.ChangeInTermsMethodDisclosureCpIcIr,
      moreInfo: MORE_INFO.ChangeInTermsMethodDisclosureCpIcIr
    },
    {
      id: MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcMe,
      fieldName: MethodFieldNameEnum.ChangeInTermsMethodDisclosureCpIcMe,
      onlinePCF: ONLINE_PCF.ChangeInTermsMethodDisclosureCpIcMe,
      moreInfoText: MORE_INFO.ChangeInTermsMethodDisclosureCpIcMe,
      moreInfo: MORE_INFO.ChangeInTermsMethodDisclosureCpIcMe
    },
    {
      id: MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcMf,
      fieldName: MethodFieldNameEnum.ChangeInTermsMethodDisclosureCpIcMf,
      onlinePCF: ONLINE_PCF.ChangeInTermsMethodDisclosureCpIcMf,
      moreInfoText: MORE_INFO.ChangeInTermsMethodDisclosureCpIcMf,
      moreInfo: MORE_INFO.ChangeInTermsMethodDisclosureCpIcMf
    },
    {
      id: MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcVi,
      fieldName: MethodFieldNameEnum.ChangeInTermsMethodDisclosureCpIcVi,
      onlinePCF: ONLINE_PCF.ChangeInTermsMethodDisclosureCpIcVi,
      moreInfoText: MORE_INFO.ChangeInTermsMethodDisclosureCpIcVi,
      moreInfo: MORE_INFO.ChangeInTermsMethodDisclosureCpIcVi
    },
    {
      id: MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoAc,
      fieldName: MethodFieldNameEnum.ChangeInTermsMethodDisclosureCpIoAc,
      onlinePCF: ONLINE_PCF.ChangeInTermsMethodDisclosureCpIoAc,
      moreInfoText: MORE_INFO.ChangeInTermsMethodDisclosureCpIoAc,
      moreInfo: MORE_INFO.ChangeInTermsMethodDisclosureCpIoAc
    },
    {
      id: MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoCi,
      fieldName: MethodFieldNameEnum.ChangeInTermsMethodDisclosureCpIoCi,
      onlinePCF: ONLINE_PCF.ChangeInTermsMethodDisclosureCpIoCi,
      moreInfoText: MORE_INFO.ChangeInTermsMethodDisclosureCpIoCi,
      moreInfo: MORE_INFO.ChangeInTermsMethodDisclosureCpIoCi
    },
    {
      id: MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoMc,
      fieldName: MethodFieldNameEnum.ChangeInTermsMethodDisclosureCpIoMc,
      onlinePCF: ONLINE_PCF.ChangeInTermsMethodDisclosureCpIoMc,
      moreInfoText: MORE_INFO.ChangeInTermsMethodDisclosureCpIoMc,
      moreInfo: MORE_INFO.ChangeInTermsMethodDisclosureCpIoMc
    },
    {
      id: MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoMi,
      fieldName: MethodFieldNameEnum.ChangeInTermsMethodDisclosureCpIoMi,
      onlinePCF: ONLINE_PCF.ChangeInTermsMethodDisclosureCpIoMi,
      moreInfoText: MORE_INFO.ChangeInTermsMethodDisclosureCpIoMi,
      moreInfo: MORE_INFO.ChangeInTermsMethodDisclosureCpIoMi
    },
    {
      id: MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPfLc,
      fieldName: MethodFieldNameEnum.ChangeInTermsMethodDisclosureCpPfLc,
      onlinePCF: ONLINE_PCF.ChangeInTermsMethodDisclosureCpPfLc,
      moreInfoText: MORE_INFO.ChangeInTermsMethodDisclosureCpPfLc,
      moreInfo: MORE_INFO.ChangeInTermsMethodDisclosureCpPfLc
    },
    {
      id: MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPfOc,
      fieldName: MethodFieldNameEnum.ChangeInTermsMethodDisclosureCpPfOc,
      onlinePCF: ONLINE_PCF.ChangeInTermsMethodDisclosureCpPfOc,
      moreInfoText: MORE_INFO.ChangeInTermsMethodDisclosureCpPfOc,
      moreInfo: MORE_INFO.ChangeInTermsMethodDisclosureCpPfOc
    },
    {
      id: MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPfRc,
      fieldName: MethodFieldNameEnum.ChangeInTermsMethodDisclosureCpPfRc,
      onlinePCF: ONLINE_PCF.ChangeInTermsMethodDisclosureCpPfRc,
      moreInfoText: MORE_INFO.ChangeInTermsMethodDisclosureCpPfRc,
      moreInfo: MORE_INFO.ChangeInTermsMethodDisclosureCpPfRc
    },
    {
      id: MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPoMp,
      fieldName: MethodFieldNameEnum.ChangeInTermsMethodDisclosureCpPoMp,
      onlinePCF: ONLINE_PCF.ChangeInTermsMethodDisclosureCpPoMp,
      moreInfoText: MORE_INFO.ChangeInTermsMethodDisclosureCpPoMp,
      moreInfo: MORE_INFO.ChangeInTermsMethodDisclosureCpPoMp
    }
  ],
  Info_PenaltyPricing: [
    {
      id: MethodFieldParameterEnum.ChangeInTermsPenaltyPricingIndicator,
      fieldName: MethodFieldNameEnum.ChangeInTermsPenaltyPricingIndicator,
      onlinePCF: ONLINE_PCF.ChangeInTermsPenaltyPricingIndicator,
      moreInfoText: MORE_INFO.ChangeInTermsPenaltyPricingIndicator,
      moreInfo: MORE_INFO.ChangeInTermsPenaltyPricingIndicator
    }
  ],
  Info_AdvancedParameters: [
    {
      id: MethodFieldParameterEnum.ChangeInTermsReasonCode,
      fieldName: MethodFieldNameEnum.ChangeInTermsReasonCode,
      onlinePCF: ONLINE_PCF.ChangeInTermsReasonCode,
      moreInfoText: MORE_INFO.ChangeInTermsReasonCode,
      moreInfo: MORE_INFO.ChangeInTermsReasonCode
    },
    {
      id: MethodFieldParameterEnum.ChangeInTermsProtectedBalanceDelayPeriod,
      fieldName: MethodFieldNameEnum.ChangeInTermsProtectedBalanceDelayPeriod,
      onlinePCF: ONLINE_PCF.ChangeInTermsProtectedBalanceDelayPeriod,
      moreInfoText: MORE_INFO.ChangeInTermsProtectedBalanceDelayPeriod,
      moreInfo: MORE_INFO.ChangeInTermsProtectedBalanceDelayPeriod
    },
    {
      id: MethodFieldParameterEnum.ChangeInTermsDisclosedMethodOverridesCpIcIdInterestDefaults,
      fieldName:
        MethodFieldNameEnum.ChangeInTermsDisclosedMethodOverridesCpIcIdInterestDefaults,
      onlinePCF:
        ONLINE_PCF.ChangeInTermsDisclosedMethodOverridesCpIcIdInterestDefaults,
      moreInfoText:
        MORE_INFO.ChangeInTermsDisclosedMethodOverridesCpIcIdInterestDefaults,
      moreInfo:
        MORE_INFO.ChangeInTermsDisclosedMethodOverridesCpIcIdInterestDefaults
    },
    {
      id: MethodFieldParameterEnum.ChangeInTermsDisclosedMethodOverridesCpIcIdInterestMethods,
      fieldName:
        MethodFieldNameEnum.ChangeInTermsDisclosedMethodOverridesCpIcIdInterestMethods,
      onlinePCF:
        ONLINE_PCF.ChangeInTermsDisclosedMethodOverridesCpIcIdInterestMethods,
      moreInfoText:
        MORE_INFO.ChangeInTermsDisclosedMethodOverridesCpIcIdInterestMethods,
      moreInfo:
        MORE_INFO.ChangeInTermsDisclosedMethodOverridesCpIcIdInterestMethods
    }
  ]
};

export const META_PARAMETER_CHANGE_IN_TERM = [
  MethodFieldParameterEnum.ChangeInTermsBasicSettingsCisMemoSettings,
  MethodFieldParameterEnum.ChangeInTermsBasicSettingsTermsConditionsTable,
  MethodFieldParameterEnum.ChangeInTermsBasicSettingsCitPcs,
  MethodFieldParameterEnum.ChangeInTermsBasicSettingsEnableCit,
  MethodFieldParameterEnum.ChangeInTermsProtectedBalancesMethodForCash,
  MethodFieldParameterEnum.ChangeInTermsProtectedBalancesMethodForMerchandise,
  MethodFieldParameterEnum.ChangeInTermsProtectedBalancesMethodForNonInterest,
  MethodFieldParameterEnum.ChangeInTermsCommunicationLetterIdForActice,
  MethodFieldParameterEnum.ChangeInTermsCommunicationLetterIdForInActice,
  MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcBp,
  MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcId,
  MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIi,
  MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIm,
  MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIp,
  MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIr,
  MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcMe,
  MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcMf,
  MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcVi,
  MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoAc,
  MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoCi,
  MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoMc,
  MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoMi,
  MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPfLc,
  MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPfOc,
  MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPfRc,
  MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPoMp,
  MethodFieldParameterEnum.ChangeInTermsTimmingCitnumber,
  MethodFieldParameterEnum.ChangeInTermsBasicSettingsCitPriorityOrder,
  MethodFieldParameterEnum.ChangeInTermsPenaltyPricingIndicator,
  MethodFieldParameterEnum.ChangeInTermsReasonCode,
  MethodFieldParameterEnum.ChangeInTermsProtectedBalanceDelayPeriod,
  MethodFieldParameterEnum.ChangeInTermsDisclosedMethodOverridesCpIcIdInterestDefaults,
  MethodFieldParameterEnum.ChangeInTermsDisclosedMethodOverridesCpIcIdInterestMethods
];
