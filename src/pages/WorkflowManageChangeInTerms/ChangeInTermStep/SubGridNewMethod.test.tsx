import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React from 'react';
import SubGridNewMethod from './SubGridNewMethod';

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

const metadata = {
  changeInTermsBasicSettingsCisMemoSettings: [{ code: 'code', text: 'text' }],
  changeInTermsBasicSettingsTermsConditionsTable: [
    { code: 'code', text: 'text' }
  ],
  changeInTermsBasicSettingsCitPcs: [{ code: 'code', text: 'text' }],
  changeInTermsBasicSettingsEnableCit: [{ code: 'code', text: 'text' }],
  changeInTermsCommunicationLetterIdForActice: [{ code: 'code', text: 'text' }],
  changeInTermsCommunicationLetterIdForInActice: [
    { code: 'code', text: 'text' }
  ],
  changeInTermsMethodDisclosureCpIcBp: [{ code: 'code', text: 'text' }],
  changeInTermsMethodDisclosureCpIcId: [{ code: 'code', text: 'text' }],
  changeInTermsMethodDisclosureCpIcIi: [{ code: 'code', text: 'text' }],
  changeInTermsMethodDisclosureCpIcIm: [{ code: 'code', text: 'text' }],
  changeInTermsMethodDisclosureCpIcIp: [{ code: 'code', text: 'text' }],
  changeInTermsMethodDisclosureCpIcIr: [{ code: 'code', text: 'text' }],
  changeInTermsMethodDisclosureCpIcMe: [{ code: 'code', text: 'text' }],
  changeInTermsMethodDisclosureCpIcMf: [{ code: 'code', text: 'text' }],
  changeInTermsMethodDisclosureCpIcVi: [{ code: 'code', text: 'text' }],
  changeInTermsMethodDisclosureCpIoAc: [{ code: 'code', text: 'text' }],
  changeInTermsMethodDisclosureCpIoCi: [{ code: 'code', text: 'text' }],
  changeInTermsMethodDisclosureCpIoMc: [{ code: 'code', text: 'text' }],
  changeInTermsMethodDisclosureCpIoMi: [{ code: 'code', text: 'text' }],
  changeInTermsMethodDisclosureCpPfLc: [{ code: 'code', text: 'text' }],
  changeInTermsMethodDisclosureCpPfOc: [{ code: 'code', text: 'text' }],
  changeInTermsMethodDisclosureCpPfRc: [{ code: 'code', text: 'text' }],
  changeInTermsMethodDisclosureCpPoMp: [{ code: 'code', text: 'text' }],
  changeInTermsPenaltyPricingIndicator: [{ code: 'code', text: 'text' }],
  changeInTermsProtectedBalancesMethodForCash: [{ code: 'code', text: 'text' }],
  changeInTermsProtectedBalancesMethodForMerchandise: [
    { code: 'code', text: 'text' }
  ],
  changeInTermsProtectedBalancesMethodForNonInterest: [
    { code: 'code', text: 'text' }
  ],
  changeInTermsTimmingCitnumber: [{ code: 'code', text: 'text' }],
  changeInTermsBasicSettingsCitPriorityOrder: [{ code: 'code', text: 'text' }],
  changeInTermsReasonCode: [{ code: 'code', text: 'text' }],
  changeInTermsDisclosedMethodOverridesCpIcIdInterestDefaultsOptions: [
    { code: 'code', text: 'text' }
  ],
  changeInTermsDisclosedMethodOverridesCpIcIdInterestMethodsOptions: [
    { code: 'code', text: 'text' }
  ]
};
const mockOnChange =
  (field: keyof WorkflowSetupMethodObjectParams, isRefData?: boolean) =>
  (e: any) =>
    jest.fn();
const _props = {
  metadata,
  onChange: mockOnChange,
  searchValue: ''
};

describe('pages > WorkflowManageChangeInTerms > ChangeInTermStep > SubGridNewMethod', () => {
  it('Info_Basic ', () => {
    const props = { ..._props, original: { id: 'Info_Settings' } };
    jest.useFakeTimers();
    const wrapper = render(
      <SubGridNewMethod initialMethod={'NEWVERSION'} {...props} />
    );
    jest.runAllTimers();
    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
    const citPriorityOrderInput = wrapper.getByTestId(
      'addNewMethod__changeInTermsBasicSettingsCitPriorityOrder_dls-text-box_input'
    ) as HTMLInputElement;
    userEvent.type(citPriorityOrderInput, '12');

    userEvent.clear(citPriorityOrderInput);
    userEvent.type(citPriorityOrderInput, 'cc');

    //
  });
  it('Info_Timing  ', () => {
    const props = { ..._props, original: { id: 'Info_Timing' } };
    const wrapper = render(
      <SubGridNewMethod initialMethod={'NEWVERSION'} {...props} />
    );
    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
  });
  it('Info_ProtectedBalances  ', () => {
    const props = { ..._props, original: { id: 'Info_ProtectedBalances' } };
    const wrapper = render(
      <SubGridNewMethod initialMethod={'NEWVERSION'} {...props} />
    );
    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
  });
  it('Info_Communication  ', () => {
    const props = { ..._props, original: { id: 'Info_Communication' } };
    const wrapper = render(
      <SubGridNewMethod initialMethod={'NEWVERSION'} {...props} />
    );
    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
  });
  it('Info_MethodDisclosureDecisions  ', () => {
    const props = {
      ..._props,
      original: { id: 'Info_MethodDisclosureDecisions' }
    };
    const wrapper = render(
      <SubGridNewMethod initialMethod={'NEWVERSION'} {...props} />
    );
    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
  });
  it('Info_PenaltyPricing  ', () => {
    const props = {
      ..._props,
      original: { id: 'Info_PenaltyPricing' }
    };
    const wrapper = render(
      <SubGridNewMethod initialMethod={'NEWVERSION'} {...props} />
    );
    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
  });
  it('Info_AdvancedParameters  ', () => {
    const props = {
      ..._props,
      original: { id: 'Info_AdvancedParameters' }
    };
    const wrapper = render(
      <SubGridNewMethod initialMethod={'NEWVERSION'} {...props} />
    );
    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
  });
});
