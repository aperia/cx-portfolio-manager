import {
  MethodFieldNameEnum,
  MethodFieldParameterEnum
} from 'app/constants/enums';
import React from 'react';

export type ParameterChangeInTermListIds =
  | 'Info_Settings'
  | 'Info_Timing'
  | 'Info_ProtectedBalances'
  | 'Info_Communication'
  | 'Info_MethodDisclosureDecisions'
  | 'Info_PenaltyPricing'
  | 'Info_AdvancedParameters';

export interface ParameterChangeInTermList {
  id: ParameterChangeInTermListIds;
  section:
    | 'txt_manage_penalty_fee_standard_parameters'
    | 'txt_manage_penalty_fee_advanced_parameters';
  parameterGroup:
    | 'Basic Settings'
    | 'Timing'
    | 'Protected Balances'
    | 'Communication'
    | 'Method Disclosure Decisions'
    | 'Penalty Pricing'
    | 'Miscellaneous Parameters';
  moreInfo?: React.ReactNode;
}

export interface ParameterChangeInTermGroup {
  id: MethodFieldParameterEnum;
  fieldName: MethodFieldNameEnum;
  onlinePCF: string;
  moreInfoText: string;
  moreInfo: React.ReactNode;
}

export interface ChangeInTermMeta {
  changeInTermsBasicSettingsCisMemoSettings: RefData[];
  changeInTermsBasicSettingsTermsConditionsTable: RefData[];
  changeInTermsBasicSettingsCitPcs: RefData[];
  changeInTermsBasicSettingsEnableCit: RefData[];
  changeInTermsCommunicationLetterIdForActice: RefData[];
  changeInTermsCommunicationLetterIdForInActice: RefData[];
  changeInTermsMethodDisclosureCpIcBp: RefData[];
  changeInTermsMethodDisclosureCpIcId: RefData[];
  changeInTermsMethodDisclosureCpIcIi: RefData[];
  changeInTermsMethodDisclosureCpIcIm: RefData[];
  changeInTermsMethodDisclosureCpIcIp: RefData[];
  changeInTermsMethodDisclosureCpIcIr: RefData[];
  changeInTermsMethodDisclosureCpIcMe: RefData[];
  changeInTermsMethodDisclosureCpIcMf: RefData[];
  changeInTermsMethodDisclosureCpIcVi: RefData[];
  changeInTermsMethodDisclosureCpIoAc: RefData[];
  changeInTermsMethodDisclosureCpIoCi: RefData[];
  changeInTermsMethodDisclosureCpIoMc: RefData[];
  changeInTermsMethodDisclosureCpIoMi: RefData[];
  changeInTermsMethodDisclosureCpPfLc: RefData[];
  changeInTermsMethodDisclosureCpPfOc: RefData[];
  changeInTermsMethodDisclosureCpPfRc: RefData[];
  changeInTermsMethodDisclosureCpPoMp: RefData[];
  changeInTermsPenaltyPricingIndicator: RefData[];
  changeInTermsProtectedBalancesMethodForCash: RefData[];
  changeInTermsProtectedBalancesMethodForMerchandise: RefData[];
  changeInTermsProtectedBalancesMethodForNonInterest: RefData[];
  changeInTermsReasonCode: RefData[];
  changeInTermsDisclosedMethodOverridesCpIcIdInterestDefaultsOptions: RefData[];
  changeInTermsDisclosedMethodOverridesCpIcIdInterestMethodsOptions: RefData[];
}
