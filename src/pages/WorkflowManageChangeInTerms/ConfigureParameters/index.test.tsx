import {
  fireEvent,
  queryByText,
  render,
  RenderResult
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { ServiceSubjectSection } from 'app/constants/enums';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageChangeInTerms';
import React from 'react';
import * as reactRedux from 'react-redux';
import ConfigureParameters from '.';

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');
const useSelectElementMetadataForChangeInTerm = jest.spyOn(
  WorkflowSetup,
  'useSelectElementMetadataForChangeInTerm'
);

const mockOnCloseWithoutMethod = jest.fn();
jest.mock('./MethodListModal', () => ({
  __esModule: true,
  default: ({ onClose }: any) => {
    const handleOnClose = () => {
      if (mockOnCloseWithoutMethod()) {
        onClose();
        return;
      }

      onClose({
        id: '1',
        name: 'method-1',
        versions: [
          {
            id: 'version 1.0'
          }
        ]
      });
    };

    return <div onClick={handleOnClose}>MethodListModal_component</div>;
  }
}));

jest.mock('./AddNewMethodModal', () => ({
  __esModule: true,
  default: ({ onClose }: any) => {
    const handleOnClose = () => {
      if (mockOnCloseWithoutMethod()) {
        onClose();
        return;
      }

      onClose(
        {
          id: '1',
          name: 'method-1',
          versions: [
            {
              id: 'version 1.0'
            }
          ]
        },
        true
      );
    };
    return <div onClick={handleOnClose}>AddNewMethodModal_component</div>;
  }
}));

jest.mock('./RemoveMethodModal', () => ({
  __esModule: true,
  default: ({ onClose }: any) => {
    const handleOnClose = () => {
      if (mockOnCloseWithoutMethod()) {
        onClose();
        return;
      }

      onClose({
        id: '1',
        name: 'method-1',
        versions: [
          {
            id: 'version 1.0'
          }
        ]
      });
    };
    return <div onClick={handleOnClose}>RemoveMethodModal_component</div>;
  }
}));

jest.mock('../ChangeInTermStep/SubRowGrid', () => ({
  __esModule: true,
  default: () => <div>ChangeInTermStep_SubRowGrid_component</div>
}));

jest.mock('../ProtectedBalancesAttributesStep/SubRowGrid', () => ({
  __esModule: true,
  default: () => <div>ProtectedBalancesAttributesStep_SubRowGrid_component</div>
}));

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <ConfigureParameters {...props} />
    </div>
  );
};
const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

describe('pages > WorkflowDesignPromotions > ConfigureParameters > index', () => {
  const mockDispatch = jest.fn();
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
    useSelectElementMetadataForChangeInTerm.mockReturnValue({
      changeInTermsBasicSettingsCisMemoSettings: [
        { code: 'code', text: 'text' }
      ],
      changeInTermsBasicSettingsTermsConditionsTable: [
        { code: 'code', text: 'text' }
      ],
      changeInTermsBasicSettingsCitPcs: [{ code: 'code', text: 'text' }],
      changeInTermsBasicSettingsEnableCit: [{ code: 'code', text: 'text' }],
      changeInTermsCommunicationLetterIdForActice: [
        { code: 'code', text: 'text' }
      ],
      changeInTermsCommunicationLetterIdForInActice: [
        { code: 'code', text: 'text' }
      ],
      changeInTermsMethodDisclosureCpIcBp: [{ code: 'code', text: 'text' }],
      changeInTermsMethodDisclosureCpIcId: [{ code: 'code', text: 'text' }],
      changeInTermsMethodDisclosureCpIcIi: [{ code: 'code', text: 'text' }],
      changeInTermsMethodDisclosureCpIcIm: [{ code: 'code', text: 'text' }],
      changeInTermsMethodDisclosureCpIcIp: [{ code: 'code', text: 'text' }],
      changeInTermsMethodDisclosureCpIcIr: [{ code: 'code', text: 'text' }],
      changeInTermsMethodDisclosureCpIcMe: [{ code: 'code', text: 'text' }],
      changeInTermsMethodDisclosureCpIcMf: [{ code: 'code', text: 'text' }],
      changeInTermsMethodDisclosureCpIcVi: [{ code: 'code', text: 'text' }],
      changeInTermsMethodDisclosureCpIoAc: [{ code: 'code', text: 'text' }],
      changeInTermsMethodDisclosureCpIoCi: [{ code: 'code', text: 'text' }],
      changeInTermsMethodDisclosureCpIoMc: [{ code: 'code', text: 'text' }],
      changeInTermsMethodDisclosureCpIoMi: [{ code: 'code', text: 'text' }],
      changeInTermsMethodDisclosureCpPfLc: [{ code: 'code', text: 'text' }],
      changeInTermsMethodDisclosureCpPfOc: [{ code: 'code', text: 'text' }],
      changeInTermsMethodDisclosureCpPfRc: [{ code: 'code', text: 'text' }],
      changeInTermsMethodDisclosureCpPoMp: [{ code: 'code', text: 'text' }],
      changeInTermsPenaltyPricingIndicator: [{ code: 'code', text: 'text' }],
      changeInTermsProtectedBalancesMethodForCash: [
        { code: 'code', text: 'text' }
      ],
      changeInTermsProtectedBalancesMethodForMerchandise: [
        { code: 'code', text: 'text' }
      ],
      changeInTermsProtectedBalancesMethodForNonInterest: [
        { code: 'code', text: 'text' }
      ],
      changeInTermsTimmingCitnumber: [{ code: 'code', text: 'text' }],
      changeInTermsBasicSettingsCitPriorityOrder: [
        { code: 'code', text: 'text' }
      ],
      protectedBalancesAttributesCreditApplicationGroup: [
        { code: 'code', text: 'text' }
      ],
      protectedBalancesAttributesMethodToProtectInterestDefault1: [
        { code: 'code', text: 'text' }
      ],
      protectedBalancesAttributesMethodToProtectInterestDefault2: [
        { code: 'code', text: 'text' }
      ],
      protectedBalancesAttributesMethodToProtectInterestDefault3: [
        { code: 'code', text: 'text' }
      ],
      protectedBalancesAttributesMethodToProtectInterestMethod1: [
        { code: 'code', text: 'text' }
      ],
      protectedBalancesAttributesMethodToProtectInterestMethod2: [
        { code: 'code', text: 'text' }
      ],
      protectedBalancesAttributesMethodToProtectInterestMethod3: [
        { code: 'code', text: 'text' }
      ],
      protectedBalancesAttributesMethodToProtectMinimumPayment1: [
        { code: 'code', text: 'text' }
      ],
      protectedBalancesAttributesMethodToProtectMinimumPayment2: [
        { code: 'code', text: 'text' }
      ],
      protectedBalancesAttributesMethodToProtectMinimumPayment3: [
        { code: 'code', text: 'text' }
      ],
      protectedBalancesAttributesDescriptionDisplayOption: [
        { code: 'code', text: 'text' }
      ],
      protectedBalancesAttributesMessageText: [{ code: 'code', text: 'text' }],
      protectedBalancesAttributesMessageOptions: [
        { code: 'code', text: 'text' }
      ],
      //Advance
      changeInTermsReasonCode: [{ code: 'code', text: 'text' }],
      changeInTermsDisclosedMethodOverridesCpIcIdInterestDefaultsOptions: [
        { code: 'code', text: 'text' }
      ],
      changeInTermsDisclosedMethodOverridesCpIcIdInterestMethodsOptions: [
        { code: 'code', text: 'text' }
      ],
      protectedBalancesAttributesProtectBalancesForPenaltyPricingOptions: [
        { code: 'code', text: 'text' }
      ],
      protectedBalancesAttributesDelayIntroTextIDOptions: [
        { code: 'code', text: 'text' }
      ],
      protectedBalancesAttributesPBBalanceTransferOptions: [
        { code: 'code', text: 'text' }
      ],
      protectedBalancesAttributesProtectedBalanceOverrideCodeOptions: [
        { code: 'code', text: 'text' }
      ],
      protectedBalancesAttributesGroupIdentifierOptions: [
        { code: 'code', text: 'text' }
      ]
    });

    mockOnCloseWithoutMethod.mockReturnValue(false);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Should render with no method > Create new version', () => {
    const props = {
      stepId: '1',
      selectedStep: 'selectedStep',
      formValues: {},
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_select_an_option')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    );
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('MethodListModal_component'));
    expect(
      queryByText(wrapper.container, /MethodListModal_component/i)
    ).not.toBeInTheDocument();

    mockOnCloseWithoutMethod.mockReturnValue(true);
    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    );
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('MethodListModal_component'));
    expect(
      queryByText(wrapper.container, /MethodListModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with no method > Choose Method to Model', () => {
    const props = {
      stepId: '1',
      selectedStep: 'selectedStep',
      formValues: {},
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_select_an_option')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_choose_method_to_model')
    );
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('MethodListModal_component'));
    expect(
      queryByText(wrapper.container, /MethodListModal_component/i)
    ).not.toBeInTheDocument();

    mockOnCloseWithoutMethod.mockReturnValue(true);
    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_choose_method_to_model')
    );

    fireEvent.click(wrapper.getByText('MethodListModal_component'));
    expect(
      queryByText(wrapper.container, /MethodListModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with no method > Add New Method', () => {
    const props = {
      stepId: '1',
      selectedStep: 'selectedStep',
      formValues: {},
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_select_an_option')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_method')
    );
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();

    mockOnCloseWithoutMethod.mockReturnValue(true);
    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_method')
    );
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with DLO Method > Edit method', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    expect(body?.children.length).toEqual(3);

    //SHOW Add new method modal
    const editButton = body?.children[1].querySelector(
      'button[class="btn btn-outline-primary btn-sm"]'
    ) as Element;
    fireEvent.click(editButton);
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
    //End

    mockOnCloseWithoutMethod.mockReturnValue(true);
    fireEvent.click(editButton);
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with DLO Method > Add new method', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );

    //SHOW AddNewMethodModal
    const editButton = body?.children[0].querySelector(
      'button[class="btn btn-outline-primary btn-sm"]'
    ) as Element;
    fireEvent.click(editButton);
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
    //End

    mockOnCloseWithoutMethod.mockReturnValue(true);
    fireEvent.click(editButton);
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with DLO Method > Remove method', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );

    //SHOW RemoveMethodModal
    const removeButton = body?.children[0].querySelector(
      'button[class="btn btn-outline-danger btn-sm ml-8"]'
    ) as Element;

    fireEvent.click(removeButton);
    expect(
      wrapper.getByText('RemoveMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('RemoveMethodModal_component'));
    expect(
      queryByText(wrapper.container, /RemoveMethodModal_component/i)
    ).not.toBeInTheDocument();
    //End

    mockOnCloseWithoutMethod.mockReturnValue(true);
    fireEvent.click(removeButton);
    expect(
      wrapper.getByText('RemoveMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('RemoveMethodModal_component'));
    expect(
      queryByText(wrapper.container, /RemoveMethodModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with DLO Method > click Add New Method on dropdown button', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const dropdownButton = wrapper.container.querySelector(
      'button.dls-dropdown-button'
    ) as Element;
    userEvent.click(dropdownButton);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_create_new_method')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_method')
    );
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with DLO Method > click Create New Version on dropdown button', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const dropdownButton = wrapper.container.querySelector(
      'button.dls-dropdown-button'
    ) as Element;
    userEvent.click(dropdownButton);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    );
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('MethodListModal_component'));
    expect(
      queryByText(wrapper.container, /MethodListModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with DLO Method > click Choose Method to Model on dropdown button', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const dropdownButton = wrapper.container.querySelector(
      'button.dls-dropdown-button'
    ) as Element;
    userEvent.click(dropdownButton);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_choose_method_to_model')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_choose_method_to_model')
    );
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('MethodListModal_component'));
    expect(
      queryByText(wrapper.container, /MethodListModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with DLO Method > Expand method', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    const firstChild = body?.children[0].querySelector(
      'button[class="btn btn-icon-secondary btn-sm"]'
    );
    fireEvent.click(firstChild as Element);
    expect(
      queryByText(wrapper.container, /SubRowGrid_component/i)
    ).toBeInTheDocument();
    fireEvent.click(firstChild as Element);
    expect(
      queryByText(wrapper.container, /SubRowGrid_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with DLO Method > Expand method', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        isChangeInTermStep: true,
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWVERSION',
            rowId: 3
          },
          {
            id: '4',
            name: 'name 4',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWVERSION',
            rowId: 4
          },
          {
            id: '5',
            name: 'name 5',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWVERSION',
            rowId: 5
          },
          {
            id: '6',
            name: 'name 6',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWVERSION',
            rowId: 6
          },
          {
            id: '7',
            name: 'name 7',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWVERSION',
            rowId: 7
          },
          {
            id: '8',
            name: 'name 8',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWVERSION',
            rowId: 8
          },
          {
            id: '9',
            name: 'name 9',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWVERSION',
            rowId: 9
          },
          {
            id: '10',
            name: 'name 10',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWVERSION',
            rowId: 10
          },
          {
            id: '11',
            name: 'name 11',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWVERSION',
            rowId: 11
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    const firstChild = body?.children[0].querySelector(
      'button[class="btn btn-icon-secondary btn-sm"]'
    );
    fireEvent.click(firstChild as Element);
    expect(
      queryByText(wrapper.container, /SubRowGrid_component/i)
    ).toBeInTheDocument();
    fireEvent.click(firstChild as Element);
    expect(
      queryByText(wrapper.container, /SubRowGrid_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with DLO Method > Edit method to list with method', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    expect(body?.children.length).toEqual(3);

    //SHOW Add new method modal
    const editButton = body?.children[1].querySelector(
      'button[class="btn btn-outline-primary btn-sm"]'
    ) as Element;

    //WITHOUT METHOD
    fireEvent.click(editButton);
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
    //End
    mockOnCloseWithoutMethod.mockReturnValue(true);
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('MethodListModal_component'));
  });

  it('Should render with DLO Method > Edit method to list with method and isChangeInTermStep', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        isChangeInTermStep: true,
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    expect(body?.children.length).toEqual(3);

    //SHOW Add new method modal
    const editButton = body?.children[1].querySelector(
      'button[class="btn btn-outline-primary btn-sm"]'
    ) as Element;

    //WITHOUT METHOD
    fireEvent.click(editButton);
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
    //End
    mockOnCloseWithoutMethod.mockReturnValue(true);
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('MethodListModal_component'));
  });

  it('Should render with DLO Method > Edit method to list without method', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    expect(body?.children.length).toEqual(3);

    //SHOW Add new method modal
    const editButton = body?.children[1].querySelector(
      'button[class="btn btn-outline-primary btn-sm"]'
    ) as Element;

    //WITH METHOD
    fireEvent.click(editButton);
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
    //End
    mockOnCloseWithoutMethod.mockReturnValue(false);
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('MethodListModal_component'));
  });
});
