import {
  fireEvent,
  queryByText,
  render,
  RenderResult
} from '@testing-library/react';
import { ServiceSubjectSection } from 'app/constants/enums';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageChangeInTerms';
import React from 'react';
import Summary from './Summary';

jest.mock('../ChangeInTermStep/SubRowGrid', () => ({
  __esModule: true,
  default: () => <div>ChangeInTermStep_SubRowGrid_component</div>
}));

jest.mock('../ProtectedBalancesAttributesStep/SubRowGrid', () => ({
  __esModule: true,
  default: () => <div>ProtectedBalancesAttributesStep_SubRowGrid_component</div>
}));

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const useSelectElementMetadataForChangeInTerm = jest.spyOn(
  WorkflowSetup,
  'useSelectElementMetadataForChangeInTerm'
);

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <Summary {...props} />
    </div>
  );
};

describe('ManagePenaltyFeeWorkflow > LateChargesStep > LateChargesSummary', () => {
  beforeEach(() => {
    useSelectElementMetadataForChangeInTerm.mockReturnValue({} as any);
  });

  afterEach(() => {
    useSelectElementMetadataForChangeInTerm.mockReset();
  });

  it('Should render a grid with no content', () => {
    const props = {
      stepId: '',
      formValues: {}
    } as any;

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    expect(body?.children.length).toEqual(0);
  });

  it('Should render a grid with content', () => {
    const props = {
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'NEWMETHOD'
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'MODELEDMETHOD'
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'NEWVERSION'
          }
        ]
      }
    } as any;

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    expect(body?.children.length).toEqual(3);
  });

  it('Should call edit function', () => {
    const mockFn = jest.fn();
    const props = {
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'NEWMETHOD'
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'MODELEDMETHOD'
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'NEWVERSION'
          }
        ]
      },
      onEditStep: mockFn
    } as any;

    const wrapper = renderComponent(props);
    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(mockFn).toHaveBeenCalled();
  });

  it('Should show sub row', () => {
    const mockFn = jest.fn();
    const props = {
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ]
      },
      onEditStep: mockFn
    } as any;

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    const firstChild = body?.children[0].querySelector(
      'button[class="btn btn-icon-secondary btn-sm"]'
    );
    fireEvent.click(firstChild as Element);
    expect(
      queryByText(wrapper.container, /SubRowGrid_component/i)
    ).toBeInTheDocument();
    fireEvent.click(firstChild as Element);
    expect(
      queryByText(wrapper.container, /SubRowGrid_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should show sub row', () => {
    const mockFn = jest.fn();
    const props = {
      formValues: {
        isChangeInTermStep: true,
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'NEWVERSION',
            rowId: 3
          },
          {
            id: '4',
            name: 'name 4',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'NEWVERSION',
            rowId: 4
          },
          {
            id: '5',
            name: 'name 5',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'NEWVERSION',
            rowId: 5
          },
          {
            id: '6',
            name: 'name 6',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'NEWVERSION',
            rowId: 6
          },
          {
            id: '7',
            name: 'name 7',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'NEWVERSION',
            rowId: 7
          },
          {
            id: '8',
            name: 'name 8',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'NEWVERSION',
            rowId: 8
          },
          {
            id: '9',
            name: 'name 9',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'NEWVERSION',
            rowId: 9
          },
          {
            id: '10',
            name: 'name 10',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'NEWVERSION',
            rowId: 10
          },
          {
            id: '11',
            name: 'name 11',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'NEWVERSION',
            rowId: 11
          }
        ]
      },
      onEditStep: mockFn
    } as any;

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    const firstChild = body?.children[0].querySelector(
      'button[class="btn btn-icon-secondary btn-sm"]'
    );
    fireEvent.click(firstChild as Element);
    expect(
      queryByText(wrapper.container, /SubRowGrid_component/i)
    ).toBeInTheDocument();
    fireEvent.click(firstChild as Element);
    expect(
      queryByText(wrapper.container, /SubRowGrid_component/i)
    ).not.toBeInTheDocument();
  });
});
