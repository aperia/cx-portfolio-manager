import {
  BadgeColorType,
  FormatTime,
  ServiceSubjectSection
} from 'app/constants/enums';
import {
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { formatTimeDefault, mapGridExpandCollapse } from 'app/helpers';
import { usePaginationGrid, useUnsavedChangeRegistry } from 'app/hooks';
import {
  Button,
  ColumnType,
  Grid,
  InlineMessage,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty, orderBy } from 'lodash';
import {
  actionsWorkflowSetup,
  useSelectElementMetadataForChangeInTerm
} from 'pages/_commons/redux/WorkflowSetup';
import {
  formatBadge,
  formatText,
  formatTruncate
} from 'pages/_commons/Utils/formatGridField';
import Paging from 'pages/_commons/Utils/Paging';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';
import { META_PARAMETER_CHANGE_IN_TERM } from '../ChangeInTermStep/constant';
import SubRowGridChangeInTerm from '../ChangeInTermStep/SubRowGrid';
import { META_PARAMETER_PROTECTED_BALANCES_ATTRIBUTES } from '../ProtectedBalancesAttributesStep/constant';
import SubRowGridProtectedBalancesAttributes from '../ProtectedBalancesAttributesStep/SubRowGrid';
import ActionButtons from './ActionButtons';
import AddNewMethodModal from './AddNewMethodModal';
import MethodListModal from './MethodListModal';
import RemoveMethodModal from './RemoveMethodModal';

export interface ConfigureParametersFormValue {
  isValid?: boolean;
  isChangeInTermStep?: boolean;
  methods?: (WorkflowSetupMethod & { rowId?: number })[];
}

const ConfigureParameters: React.FC<
  WorkflowSetupProps<ConfigureParametersFormValue>
> = ({
  type: serviceSubjectSectionType,
  stepId,
  selectedStep,
  savedAt,
  isStuck,
  formValues,
  setFormValues
}) => {
  const { t } = useTranslation();

  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;

  const { isChangeInTermStep } = formValues;

  const dispatch = useDispatch();
  const [expandedList, setExpandedList] = useState<string[]>([]);
  const [showAddNewMethodModal, setShowAddNewMethodModal] = useState(false);
  const [isCreateNewVersion, setIsCreateNewVersion] = useState(false);
  const [isMethodToModel, setIsMethodToModel] = useState(false);
  const [removeMethodRowId, setRemoveMethodRowId] =
    useState<undefined | number>(undefined);
  const [editMethodRowId, setEditMethodRowId] =
    useState<undefined | number>(undefined);
  const [editMethodBackId, setEditMethodBackId] =
    useState<undefined | number>(undefined);
  const [hasChange, setHasChange] = useState(false);
  const [keepCreateMethodState, setKeepCreateMethodState] =
    useState<any | undefined>(undefined);

  const metadata = useSelectElementMetadataForChangeInTerm();
  const methods = useMemo(
    () => orderBy(formValues?.methods || [], ['name'], ['asc']),
    [formValues?.methods]
  );

  const methodNames = useMemo(() => methods.map(i => i.name), [methods]);

  const {
    total,
    currentPage,
    currentPageSize,
    pageData,
    onPageChange,
    onPageSizeChange
  } = usePaginationGrid(methods);

  const prevExpandedList = useRef<string[]>();

  const serviceSubjectSection = isChangeInTermStep
    ? ServiceSubjectSection.CIT
    : ServiceSubjectSection.PBA;

  useEffect(() => {
    prevExpandedList.current = expandedList;
  }, [expandedList]);

  const onRemoveMethod = useCallback((idx: number) => {
    setRemoveMethodRowId(idx);
  }, []);

  const onEditMethod = useCallback((idx: number) => {
    setEditMethodRowId(idx);
  }, []);

  const onClickAddNewMethod = () => {
    setShowAddNewMethodModal(true);
  };
  const onClickMethodToModel = () => {
    setIsMethodToModel(true);
  };
  const onClickCreateNewVersion = () => {
    setIsCreateNewVersion(true);
  };
  const closeMethodListModalModal = () => {
    setIsCreateNewVersion(false);
    setIsMethodToModel(false);
  };

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName: UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CHANGE_IN_TERM__PARAMETERS,
      priority: 1
    },
    [hasChange]
  );

  const methodTypeToText = useCallback(
    (type: WorkflowSetupMethodType) => {
      switch (type) {
        case 'NEWVERSION':
          return t('txt_manage_penalty_fee_version_created');
        case 'MODELEDMETHOD':
          return t('txt_manage_penalty_fee_method_modeled');
      }
      return t('txt_manage_penalty_fee_method_created');
    },
    [t]
  );

  const columns: ColumnType[] = useMemo(
    () => [
      {
        id: 'methodName',
        Header: t('txt_method_name'),
        accessor: formatText(['name']),
        width: 105
      },
      {
        id: 'modelOrCreateFrom',
        Header: t('txt_manage_penalty_fee_modeled_or_create_from'),
        accessor: formatText(['modeledFrom', 'name']),
        width: 120
      },
      {
        id: 'comment',
        Header: t('txt_comment_area'),
        accessor: formatTruncate(['comment']),
        width: 312
      },
      {
        id: 'methodType',
        Header: t('txt_manage_penalty_fee_action_taken'),
        accessor: (data, idx) =>
          formatBadge(['methodType'], {
            colorType: BadgeColorType.MethodType,
            noBorder: true
          })(
            {
              ...data,
              methodType: methodTypeToText(data.methodType)
            },
            idx
          ),
        width: 153
      },
      {
        id: 'actions',
        Header: t('txt_actions'),
        accessor: (data: any) => (
          <div className="d-flex justify-content-center">
            <Button
              size="sm"
              variant="outline-primary"
              onClick={() => onEditMethod(data.rowId)}
            >
              {t('txt_edit')}
            </Button>
            <Button
              size="sm"
              variant="outline-danger"
              className="ml-8"
              onClick={() => onRemoveMethod(data.rowId)}
            >
              {t('txt_delete')}
            </Button>
          </div>
        ),
        cellProps: { className: 'text-center py-8' },
        width: 142
      }
    ],
    [t, methodTypeToText, onEditMethod, onRemoveMethod]
  );

  const [minDateSetting, setMinDateSetting] = useState();
  const [maxDateSetting, setMaxDateSetting] = useState();
  // const [invalidDate, setInvalidDate] = useState([]);

  const minDate = useMemo(() => {
    return new Date(formatTimeDefault(minDateSetting!, FormatTime.Date));
  }, [minDateSetting]);

  const maxDate = useMemo(() => {
    return new Date(formatTimeDefault(maxDateSetting!, FormatTime.Date));
  }, [maxDateSetting]);

  const initFields = useCallback(async () => {
    const responseDateRange = await Promise.resolve(
      dispatch(actionsWorkflowSetup.getWorkflowEffectiveDateOptions({}))
    );

    const { maxDate: maxDateRes, minDate: minDateRes } =
      (responseDateRange as any)?.payload?.data || {};

    setMinDateSetting(minDateRes);
    setMaxDateSetting(maxDateRes);
    // setInvalidDate(invalidDates);
  }, [dispatch]);

  useEffect(() => {
    initFields();
  }, [initFields]);

  useEffect(() => {
    dispatch(
      actionsWorkflowSetup.getWorkflowMetadata(
        isChangeInTermStep
          ? META_PARAMETER_CHANGE_IN_TERM
          : META_PARAMETER_PROTECTED_BALANCES_ATTRIBUTES
      )
    );
  }, [dispatch, isChangeInTermStep]);
  useEffect(() => {
    const isValid = (formValues?.methods?.length || 0) > 0;
    if (formValues.isValid === isValid) return;

    keepRef.current.setFormValues({ isValid });
  }, [formValues]);

  useEffect(() => {
    if (stepId !== selectedStep) {
      setExpandedList([]);
    }
    setHasChange(false);
  }, [stepId, selectedStep, savedAt]);

  const renderGrid = useMemo(() => {
    if (isEmpty(pageData)) return;
    return (
      <div className="pt-16">
        <Grid
          columns={columns}
          data={pageData}
          subRow={({ original }: any) =>
            isChangeInTermStep ? (
              <SubRowGridChangeInTerm original={original} metadata={metadata} />
            ) : (
              <SubRowGridProtectedBalancesAttributes
                original={original}
                metadata={metadata}
              />
            )
          }
          dataItemKey="rowId"
          togglable
          toggleButtonConfigList={pageData.map(
            mapGridExpandCollapse('rowId', t)
          )}
          expandedItemKey={'rowId'}
          expandedList={expandedList}
          onExpand={setExpandedList}
          scrollable
        />
        {total > 10 && (
          <Paging
            totalItem={total}
            pageSize={currentPageSize}
            page={currentPage}
            onChangePage={onPageChange}
            onChangePageSize={onPageSizeChange}
          />
        )}
      </div>
    );
  }, [
    columns,
    expandedList,
    metadata,
    t,
    isChangeInTermStep,
    currentPage,
    currentPageSize,
    total,
    pageData,
    onPageChange,
    onPageSizeChange
  ]);

  return (
    <div>
      <div className="mt-8 pb-24 border-bottom color-grey">
        <p>
          {isChangeInTermStep
            ? t('txt_workflow_manage_change_in_terms_change_in_terms_configure')
            : t(
                'txt_workflow_manage_change_in_terms_protected_balances_attributes_configure'
              )}
        </p>
      </div>
      {isStuck && (
        <InlineMessage className="mb-0 mt-24" variant="danger" withIcon>
          {t('txt_step_stuck_move_forward_message')}
        </InlineMessage>
      )}
      <div className="mt-24">
        <ActionButtons
          small={!isEmpty(methods)}
          title="txt_method_list"
          onClickAddNewMethod={onClickAddNewMethod}
          onClickMethodToModel={onClickMethodToModel}
          onClickCreateNewVersion={onClickCreateNewVersion}
        />
      </div>
      {renderGrid}
      {removeMethodRowId !== undefined && (
        <RemoveMethodModal
          id="removeMethod"
          show
          methodName={methods.find(i => i.rowId === removeMethodRowId)?.name}
          onClose={method => {
            if (method) {
              setFormValues({
                methods: methods.filter(i => i.rowId !== removeMethodRowId)
              });
              setHasChange(true);
            }
            setRemoveMethodRowId(undefined);
          }}
        />
      )}
      {editMethodRowId !== undefined && (
        <AddNewMethodModal
          id="addNewMethod"
          show
          isChangeInTermStep={isChangeInTermStep}
          isCreate={false}
          method={methods.find(i => i.rowId === editMethodRowId)}
          methodNames={methodNames}
          minDate={minDate}
          maxDate={maxDate}
          onClose={(method, isBack, keepState) => {
            if (method) {
              method.serviceSubjectSection = serviceSubjectSection;
              const newMethds = methods.map(item => {
                if (item.rowId === editMethodRowId) return method;
                return item;
              });
              setFormValues({ methods: newMethds });
              setHasChange(true);

              if (isBack) {
                setEditMethodBackId(editMethodRowId);
                setKeepCreateMethodState(keepState);
              }
            }
            setEditMethodRowId(undefined);
          }}
        />
      )}
      {editMethodBackId && (
        <MethodListModal
          id="editMethodFromModel"
          show
          isChangeInTermStep={isChangeInTermStep}
          keepState={keepCreateMethodState}
          methodNames={methodNames}
          isCreateNewVersion={
            methods.find(i => i.rowId === editMethodBackId)!.methodType ===
            'NEWVERSION'
          }
          defaultMethod={methods.find(i => i.rowId === editMethodBackId)}
          onClose={method => {
            if (method) {
              method.serviceSubjectSection = serviceSubjectSection;
              const newMethds = methods.map(item => {
                if (item.rowId === editMethodBackId) return method;
                return item;
              });
              setFormValues({ methods: newMethds });
              setHasChange(true);
            }
            setEditMethodBackId(undefined);
            setKeepCreateMethodState(undefined);
          }}
        />
      )}
      {(isCreateNewVersion || isMethodToModel) && (
        <MethodListModal
          id="addMethodFromModel"
          show
          isChangeInTermStep={isChangeInTermStep}
          methodNames={methodNames}
          isCreateNewVersion={isCreateNewVersion}
          serviceSubjectSectionType={serviceSubjectSectionType}
          onClose={newMethod => {
            if (newMethod) {
              const rowId = Date.now();
              setFormValues({
                methods: [
                  {
                    ...newMethod,
                    rowId,
                    serviceSubjectSection: serviceSubjectSection
                  },
                  ...methods
                ]
              });
              setExpandedList(
                prevExpandedList?.current?.concat([rowId as any]) as any
              );
              setHasChange(true);
            }
            closeMethodListModalModal();
          }}
        />
      )}
      {showAddNewMethodModal && (
        <AddNewMethodModal
          id="addNewMethod"
          show
          isChangeInTermStep={isChangeInTermStep}
          methodNames={methodNames}
          minDate={minDate}
          maxDate={maxDate}
          onClose={method => {
            if (method) {
              const rowId = Date.now();
              setFormValues({
                methods: [
                  {
                    ...method,
                    rowId,
                    serviceSubjectSection: serviceSubjectSection
                  },
                  ...methods
                ]
              });
              setExpandedList(
                prevExpandedList?.current?.concat([rowId as any]) as any
              );
              setHasChange(true);
            }
            setShowAddNewMethodModal(false);
          }}
        />
      )}
    </div>
  );
};

export default ConfigureParameters;
