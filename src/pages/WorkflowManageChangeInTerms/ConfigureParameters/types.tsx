import { ParameterChangeInTermListIds } from '../ChangeInTermStep/types';
import { ParameterProtectedBalancesAttributesListIds } from '../ProtectedBalancesAttributesStep/types';

export type ParameterListIds = ParameterChangeInTermListIds &
  ParameterProtectedBalancesAttributesListIds;
