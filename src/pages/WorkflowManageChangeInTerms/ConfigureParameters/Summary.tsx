import { BadgeColorType } from 'app/constants/enums';
import { mapGridExpandCollapse } from 'app/helpers';
import { usePaginationGrid } from 'app/hooks/usePaginationGrid';
import { Button, ColumnType, Grid, useTranslation } from 'app/_libraries/_dls';
import { isFunction, orderBy } from 'lodash';
import isEmpty from 'lodash.isempty';
import { useSelectElementMetadataForChangeInTerm } from 'pages/_commons/redux/WorkflowSetup';
import {
  formatBadge,
  formatText,
  formatTruncate
} from 'pages/_commons/Utils/formatGridField';
import Paging from 'pages/_commons/Utils/Paging';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { ConfigureParametersFormValue } from '.';
import SubRowGridChangeInTerm from '../ChangeInTermStep/SubRowGrid';
import SubRowGridProtectedBalancesAttributes from '../ProtectedBalancesAttributesStep/SubRowGrid';

const Summary: React.FC<
  WorkflowSetupSummaryProps<ConfigureParametersFormValue>
> = ({ onEditStep, formValues, stepId, selectedStep }) => {
  const [expandedList, setExpandedList] = useState<string[]>([]);
  const { t } = useTranslation();
  const metadata = useSelectElementMetadataForChangeInTerm();

  useEffect(() => {
    if (stepId !== selectedStep) return;

    setExpandedList([]);
  }, [stepId, selectedStep]);

  const methods = useMemo(
    () => orderBy(formValues?.methods || [], ['name'], ['asc']),
    [formValues?.methods]
  );
  const isChangeInTermStep = formValues?.isChangeInTermStep;
  const {
    total,
    currentPage,
    currentPageSize,
    pageData,
    onPageChange,
    onPageSizeChange
  } = usePaginationGrid(methods);

  const methodTypeToText = useCallback(
    (type: WorkflowSetupMethodType) => {
      switch (type) {
        case 'NEWVERSION':
          return t('txt_manage_penalty_fee_version_created');
        case 'MODELEDMETHOD':
          return t('txt_manage_penalty_fee_method_modeled');
      }
      return t('txt_manage_penalty_fee_method_created');
    },
    [t]
  );

  const columns: ColumnType[] = useMemo(
    () => [
      {
        id: 'methodName',
        Header: t('txt_method_name'),
        accessor: formatText(['name']),
        width: 105
      },
      {
        id: 'modelOrCreateFrom',
        Header: t('txt_manage_penalty_fee_modeled_or_create_from'),
        accessor: formatText(['modeledFrom', 'name']),
        width: 120
      },
      {
        id: 'comment',
        Header: t('txt_comment_area'),
        accessor: formatTruncate(['comment']),
        width: 312
      },
      {
        id: 'methodType',
        Header: t('txt_manage_penalty_fee_action_taken'),
        accessor: (data, idx) =>
          formatBadge(['methodType'], {
            colorType: BadgeColorType.MethodType,
            noBorder: true
          })(
            {
              ...data,
              methodType: methodTypeToText(data.methodType)
            },
            idx
          ),
        width: 153
      }
    ],
    [t, methodTypeToText]
  );

  const handleEdit = () => {
    isFunction(onEditStep) && onEditStep();
  };

  const handleOnExpand = (dataKeyList: string[]) => {
    setExpandedList(
      isEmpty(dataKeyList) ? [] : [dataKeyList[dataKeyList.length - 1]]
    );
  };

  return (
    <div className="position-relative">
      <div className="absolute-top-right mt-n26 mr-n8">
        <Button variant="outline-primary" size="sm" onClick={handleEdit}>
          {t('txt_edit')}
        </Button>
      </div>
      <div className="pt-16">
        <Grid
          columns={columns}
          data={pageData}
          subRow={({ original }: any) =>
            isChangeInTermStep ? (
              <SubRowGridChangeInTerm original={original} metadata={metadata} />
            ) : (
              <SubRowGridProtectedBalancesAttributes
                original={original}
                metadata={metadata}
              />
            )
          }
          dataItemKey="rowId"
          togglable
          toggleButtonConfigList={pageData.map(
            mapGridExpandCollapse('rowId', t)
          )}
          expandedItemKey={'rowId'}
          expandedList={expandedList}
          onExpand={handleOnExpand}
          scrollable
        />
        {total > 10 && (
          <Paging
            totalItem={total}
            pageSize={currentPageSize}
            page={currentPage}
            onChangePage={onPageChange}
            onChangePageSize={onPageSizeChange}
          />
        )}
      </div>
    </div>
  );
};

export default Summary;
