import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { default as stepRegistry } from '../_commons/WorkflowSetup/registries/stepRegistry';

const registerFunc = jest.spyOn(stepRegistry, 'registerStep');

describe('WorkflowManageChangeInTerms > registerSteps', () => {
  it('Should register components', async () => {
    await import('./registerSteps');

    expect(registerFunc).toBeCalledWith(
      'ConfigureParametersProtectedBalancesAttributesStep',
      expect.anything(),
      [UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CHANGE_IN_TERM__PARAMETERS]
    );
    expect(registerFunc).toBeCalledWith(
      'ConfigureParametersChangeInTermStep',
      expect.anything(),
      [UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CHANGE_IN_TERM__PARAMETERS]
    );
  });
});
