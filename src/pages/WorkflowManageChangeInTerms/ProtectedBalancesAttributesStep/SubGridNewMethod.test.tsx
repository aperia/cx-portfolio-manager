import { render } from '@testing-library/react';
import React from 'react';
import SubGridNewMethod from './SubGridNewMethod';

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

const metadata = {
  protectedBalancesAttributesCreditApplicationGroup: [
    { code: 'code', text: 'text' }
  ],
  protectedBalancesAttributesMethodToProtectInterestDefault1: [
    { code: 'code', text: 'text' }
  ],
  protectedBalancesAttributesMethodToProtectInterestDefault2: [
    { code: 'code', text: 'text' }
  ],
  protectedBalancesAttributesMethodToProtectInterestDefault3: [
    { code: 'code', text: 'text' }
  ],
  protectedBalancesAttributesMethodToProtectInterestMethod1: [
    { code: 'code', text: 'text' }
  ],
  protectedBalancesAttributesMethodToProtectInterestMethod2: [
    { code: 'code', text: 'text' }
  ],
  protectedBalancesAttributesMethodToProtectInterestMethod3: [
    { code: 'code', text: 'text' }
  ],
  protectedBalancesAttributesMethodToProtectMinimumPayment1: [
    { code: 'code', text: 'text' }
  ],
  protectedBalancesAttributesMethodToProtectMinimumPayment2: [
    { code: 'code', text: 'text' }
  ],
  protectedBalancesAttributesMethodToProtectMinimumPayment3: [
    { code: 'code', text: 'text' }
  ],
  protectedBalancesAttributesDescriptionDisplayOption: [
    { code: 'code', text: 'text' }
  ],
  protectedBalancesAttributesMessageText: [{ code: 'code', text: 'text' }],
  protectedBalancesAttributesMessageOptions: [{ code: 'code', text: 'text' }],
  protectedBalancesAttributesProtectBalancesForPenaltyPricingOptions: [
    { code: 'code', text: 'text' }
  ],
  protectedBalancesAttributesDelayIntroTextIDOptions: [
    { code: 'code', text: 'text' }
  ],
  protectedBalancesAttributesPBBalanceTransferOptions: [
    { code: 'code', text: 'text' }
  ],
  protectedBalancesAttributesProtectedBalanceOverrideCodeOptions: [
    { code: 'code', text: 'text' }
  ],
  protectedBalancesAttributesGroupIdentifierOptions: [
    { code: 'code', text: 'text' }
  ]
};
const mockOnChange =
  (field: keyof WorkflowSetupMethodObjectParams, isRefData?: boolean) =>
  (e: any) =>
    jest.fn();
const _props = {
  metadata,
  onChange: mockOnChange,
  searchValue: ''
};

describe('pages > WorkflowManageChangeInTerms > ChangeInTermStep > SubGridNewMethod', () => {
  it('Info_Basic ', () => {
    const props = { ..._props, original: { id: 'Info_Settings' } };
    jest.useFakeTimers();
    const wrapper = render(
      <SubGridNewMethod initialMethod={'NEWVERSION'} {...props} />
    );
    jest.runAllTimers();
    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();

    //
  });
  it('Info_StatementDisplay  ', () => {
    const props = { ..._props, original: { id: 'Info_StatementDisplay' } };
    const wrapper = render(
      <SubGridNewMethod initialMethod={'NEWVERSION'} {...props} />
    );
    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
  });
  it('Info_MethodsToProtect  ', () => {
    const props = { ..._props, original: { id: 'Info_MethodsToProtect' } };
    const wrapper = render(
      <SubGridNewMethod initialMethod={'NEWVERSION'} {...props} />
    );
    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
  });
  it('Info_PenaltyPricing  ', () => {
    const props = { ..._props, original: { id: 'Info_PenaltyPricing' } };
    const wrapper = render(
      <SubGridNewMethod initialMethod={'NEWVERSION'} {...props} />
    );
    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
  });
  it('Info_MiscellaneousParameters  ', () => {
    const props = {
      ..._props,
      original: { id: 'Info_MiscellaneousParameters' }
    };
    const wrapper = render(
      <SubGridNewMethod initialMethod={'NEWVERSION'} {...props} />
    );
    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
  });
});
