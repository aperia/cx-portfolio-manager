import EnhanceDropdownList from 'app/components/EnhanceDropdownList';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import { matchSearchValue } from 'app/helpers';
import {
  ColumnType,
  ComboBox,
  Grid,
  TextBox,
  useTranslation
} from 'app/_libraries/_dls';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React, { useMemo } from 'react';
import { parameterProtectedBalancesAttributesGroup } from './constant';
import {
  ParameterProtectedBalancesAttributesList,
  ProtectedBalancesAttributesMeta
} from './types';

interface IProps {
  initialMethod: string;
  searchValue: string;
  method: WorkflowSetupMethodObjectParams;
  original: ParameterProtectedBalancesAttributesList;
  minDate?: Date;
  maxDate?: Date;
  metadata: ProtectedBalancesAttributesMeta;
  onChange: (
    field: keyof WorkflowSetupMethodObjectParams,
    isRefData?: boolean
  ) => (e: any) => void;
}
const SubGridNewMethod: React.FC<IProps> = ({
  searchValue,
  method,
  original,
  metadata,
  onChange,
  initialMethod,
  minDate,
  maxDate
}) => {
  const { t } = useTranslation();

  const {
    protectedBalancesAttributesCreditApplicationGroup,
    protectedBalancesAttributesMethodToProtectInterestDefault1,
    protectedBalancesAttributesMethodToProtectInterestDefault2,
    protectedBalancesAttributesMethodToProtectInterestDefault3,
    protectedBalancesAttributesMethodToProtectInterestMethod1,
    protectedBalancesAttributesMethodToProtectInterestMethod2,
    protectedBalancesAttributesMethodToProtectInterestMethod3,
    protectedBalancesAttributesMethodToProtectMinimumPayment1,
    protectedBalancesAttributesMethodToProtectMinimumPayment2,
    protectedBalancesAttributesMethodToProtectMinimumPayment3,
    protectedBalancesAttributesDescriptionDisplayOption,
    protectedBalancesAttributesMessageText,
    protectedBalancesAttributesMessageOptions,
    protectedBalancesAttributesProtectBalancesForPenaltyPricingOptions,
    protectedBalancesAttributesDelayIntroTextIDOptions,
    protectedBalancesAttributesPBBalanceTransferOptions,
    protectedBalancesAttributesProtectedBalanceOverrideCodeOptions,
    protectedBalancesAttributesGroupIdentifierOptions
  } = metadata;
  const dataColumn =
    initialMethod === 'NEWVERSION'
      ? parameterProtectedBalancesAttributesGroup
      : parameterProtectedBalancesAttributesGroup;

  const data = useMemo(
    () =>
      dataColumn[original.id].filter(p =>
        matchSearchValue(
          [p.fieldName, p.moreInfoText, p.onlinePCF],
          searchValue
        )
      ),
    [dataColumn, original.id, searchValue]
  );

  const renderDropdown = (
    data: RefData[],
    field: keyof WorkflowSetupMethodObjectParams
  ) => {
    const value = data.find(o => o.code === method?.[field]);

    return (
      <EnhanceDropdownList
        placeholder={t('txt_workflow_manage_change_in_terms_select_an_option')}
        dataTestId={`addNewMethod__${field}`}
        size="small"
        value={value}
        onChange={onChange(field, true)}
        options={data}
      />
    );
  };

  const renderCombobox = (
    data: RefData[],
    field: keyof WorkflowSetupMethodObjectParams
  ) => {
    const value = data.find(o => o.code === method?.[field]);

    return (
      <ComboBox
        placeholder={t('txt_workflow_manage_change_in_terms_select_an_option')}
        size="small"
        textField="text"
        value={value}
        onChange={onChange(field, true)}
        noResult={t('txt_no_results_found_without_dot')}
      >
        {data.map(item => (
          <ComboBox.Item key={item.code} value={item} label={item.text} />
        ))}
      </ComboBox>
    );
  };

  const valueAccessor = (data: Record<string, any>) => {
    const paramId = data.id as MethodFieldParameterEnum;

    switch (paramId) {
      case MethodFieldParameterEnum.ProtectedBalancesAttributesCreditApplicationGroup: {
        return renderDropdown(
          protectedBalancesAttributesCreditApplicationGroup,
          MethodFieldParameterEnum.ProtectedBalancesAttributesCreditApplicationGroup
        );
      }

      case MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault1: {
        return renderCombobox(
          protectedBalancesAttributesMethodToProtectInterestDefault1,
          MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault1
        );
      }

      case MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault2: {
        return renderCombobox(
          protectedBalancesAttributesMethodToProtectInterestDefault2,
          MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault2
        );
      }
      case MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault3: {
        return renderCombobox(
          protectedBalancesAttributesMethodToProtectInterestDefault3,
          MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault3
        );
      }
      case MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod1: {
        return renderCombobox(
          protectedBalancesAttributesMethodToProtectInterestMethod1,
          MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod1
        );
      }
      case MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod2: {
        return renderCombobox(
          protectedBalancesAttributesMethodToProtectInterestMethod2,
          MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod2
        );
      }
      case MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod3: {
        return renderCombobox(
          protectedBalancesAttributesMethodToProtectInterestMethod3,
          MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod3
        );
      }
      case MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment1: {
        return renderCombobox(
          protectedBalancesAttributesMethodToProtectMinimumPayment1,
          MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment1
        );
      }
      case MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment2: {
        return renderCombobox(
          protectedBalancesAttributesMethodToProtectMinimumPayment2,
          MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment2
        );
      }
      case MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment3: {
        return renderCombobox(
          protectedBalancesAttributesMethodToProtectMinimumPayment3,
          MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment3
        );
      }
      case MethodFieldParameterEnum.ProtectedBalancesAttributesDescription: {
        return (
          <TextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__protectedBalancesAttributesDescription"
            value={
              method?.[
                MethodFieldParameterEnum.ProtectedBalancesAttributesDescription
              ] || ''
            }
            size="sm"
            maxLength={30}
            onChange={onChange(
              MethodFieldParameterEnum.ProtectedBalancesAttributesDescription
            )}
          />
        );
      }
      case MethodFieldParameterEnum.ProtectedBalancesAttributesAlternateDescription: {
        return (
          <TextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__protectedBalancesAttributesAlternateDescription"
            value={
              method?.[
                MethodFieldParameterEnum
                  .ProtectedBalancesAttributesAlternateDescription
              ] || ''
            }
            size="sm"
            maxLength={30}
            onChange={onChange(
              MethodFieldParameterEnum.ProtectedBalancesAttributesAlternateDescription
            )}
          />
        );
      }
      case MethodFieldParameterEnum.ProtectedBalancesAttributesDescriptionDisplayOption: {
        return renderDropdown(
          protectedBalancesAttributesDescriptionDisplayOption,
          MethodFieldParameterEnum.ProtectedBalancesAttributesDescriptionDisplayOption
        );
      }
      case MethodFieldParameterEnum.ProtectedBalancesAttributesMessageText: {
        return renderCombobox(
          protectedBalancesAttributesMessageText,
          MethodFieldParameterEnum.ProtectedBalancesAttributesMessageText
        );
      }
      case MethodFieldParameterEnum.ProtectedBalancesAttributesMessageOptions: {
        return renderDropdown(
          protectedBalancesAttributesMessageOptions,
          MethodFieldParameterEnum.ProtectedBalancesAttributesMessageOptions
        );
      }

      case MethodFieldParameterEnum.ProtectedBalancesAttributesProtectBalancesForPenaltyPricing: {
        return renderDropdown(
          protectedBalancesAttributesProtectBalancesForPenaltyPricingOptions,
          MethodFieldParameterEnum.ProtectedBalancesAttributesProtectBalancesForPenaltyPricing
        );
      }

      case MethodFieldParameterEnum.ProtectedBalancesAttributesDelayIntroTextID: {
        return renderCombobox(
          protectedBalancesAttributesDelayIntroTextIDOptions,
          MethodFieldParameterEnum.ProtectedBalancesAttributesDelayIntroTextID
        );
      }

      case MethodFieldParameterEnum.ProtectedBalancesAttributesPBBalanceTransfer: {
        return renderDropdown(
          protectedBalancesAttributesPBBalanceTransferOptions,
          MethodFieldParameterEnum.ProtectedBalancesAttributesPBBalanceTransfer
        );
      }

      case MethodFieldParameterEnum.ProtectedBalancesAttributesProtectedBalanceOverrideCode: {
        return renderDropdown(
          protectedBalancesAttributesProtectedBalanceOverrideCodeOptions,
          MethodFieldParameterEnum.ProtectedBalancesAttributesProtectedBalanceOverrideCode
        );
      }

      case MethodFieldParameterEnum.ProtectedBalancesAttributesGroupIdentifier: {
        return renderCombobox(
          protectedBalancesAttributesGroupIdentifierOptions,
          MethodFieldParameterEnum.ProtectedBalancesAttributesGroupIdentifier
        );
      }
    }
  };

  const columns: ColumnType[] = [
    {
      id: 'fieldName',
      Header: t('txt_business_name'),
      accessor: 'fieldName',
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'onlinePCF',
      Header: t('txt_manage_penalty_fee_online_PCF'),
      accessor: 'onlinePCF',
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'value',
      Header: t('txt_value'),
      cellBodyProps: { className: 'py-8' },
      accessor: valueAccessor
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      accessor: viewMoreInfo(['moreInfo'], t),
      width: 105,
      cellBodyProps: { className: 'pt-12 pb-8' }
    }
  ];

  return (
    <div className="p-20">
      <Grid columns={columns} data={data} />
    </div>
  );
};

export default SubGridNewMethod;
