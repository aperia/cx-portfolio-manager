import {
  MethodFieldNameEnum,
  MethodFieldParameterEnum
} from 'app/constants/enums';
import React from 'react';

export type ParameterProtectedBalancesAttributesListIds =
  | 'Info_Settings'
  | 'Info_MethodsToProtect'
  | 'Info_StatementDisplay'
  | 'Info_PenaltyPricing'
  | 'Info_MiscellaneousParameters';

export interface ParameterProtectedBalancesAttributesList {
  id: ParameterProtectedBalancesAttributesListIds;
  section:
    | 'txt_manage_penalty_fee_standard_parameters'
    | 'txt_manage_penalty_fee_advanced_parameters';
  parameterGroup:
    | 'Basic Settings'
    | 'Methods to Protect'
    | 'Statement Display'
    | 'Penalty Pricing'
    | 'Miscellaneous Parameters';

  moreInfo?: React.ReactNode;
}

export interface ParameterProtectedBalancesAttributesGroup {
  id: MethodFieldParameterEnum;
  fieldName: MethodFieldNameEnum;
  onlinePCF: string;
  moreInfoText: string;
  moreInfo: React.ReactNode;
}

export interface ProtectedBalancesAttributesMeta {
  protectedBalancesAttributesCreditApplicationGroup: RefData[];
  protectedBalancesAttributesMethodToProtectInterestDefault1: RefData[];
  protectedBalancesAttributesMethodToProtectInterestDefault2: RefData[];
  protectedBalancesAttributesMethodToProtectInterestDefault3: RefData[];
  protectedBalancesAttributesMethodToProtectInterestMethod1: RefData[];
  protectedBalancesAttributesMethodToProtectInterestMethod2: RefData[];
  protectedBalancesAttributesMethodToProtectInterestMethod3: RefData[];
  protectedBalancesAttributesMethodToProtectMinimumPayment1: RefData[];
  protectedBalancesAttributesMethodToProtectMinimumPayment2: RefData[];
  protectedBalancesAttributesMethodToProtectMinimumPayment3: RefData[];
  protectedBalancesAttributesDescriptionDisplayOption: RefData[];
  protectedBalancesAttributesMessageText: RefData[];
  protectedBalancesAttributesMessageOptions: RefData[];
  protectedBalancesAttributesProtectBalancesForPenaltyPricingOptions: RefData[];
  protectedBalancesAttributesDelayIntroTextIDOptions: RefData[];
  protectedBalancesAttributesPBBalanceTransferOptions: RefData[];
  protectedBalancesAttributesProtectedBalanceOverrideCodeOptions: RefData[];
  protectedBalancesAttributesGroupIdentifierOptions: RefData[];
}
