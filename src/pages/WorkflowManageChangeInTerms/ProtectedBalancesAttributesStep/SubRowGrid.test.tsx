import { renderComponent } from 'app/utils';
import React from 'react';
import { parameterProtectedBalancesAttributesList } from './constant';
import SubRowGrid from './SubRowGrid';

const t = value => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

jest.mock('./SubGridPreviewMethod', () => ({
  __esModule: true,
  default: () => <div>SubGridPreviewMethod</div>
}));

describe('pages > WorkflowManageChangeInTerms > ProtectedBalancesAttributesStep > SubRowGrid', () => {
  it('render', async () => {
    const original = [
      { newValue: '0', name: 'cit.priority.order' },
      { newValue: '0', name: 'cis.memo.settings' },
      { newValue: '0', name: 'terms.and.conditions.table' },
      { newValue: '0', name: 'cit.pcs.messaging.table' },
      { newValue: '0', name: 'enable.cit.manager.review.process' },

      { newValue: '0', name: 'letter.id.for.active.accounts' },
      { newValue: '0', name: 'letter.id.for.inactive.accounts' },

      { newValue: '0', name: 'disclosed.methods.cp.ic.bp.break.points' },
      { newValue: '0', name: 'disclosed.methods.cp.ic.id.interest.defaults' },
      {
        newValue: '0',
        name: 'disclosed.methods.cp.ic.ii.interest.on.interest'
      },
      { newValue: '0', name: 'disclosed.methods.cp.ic.im.interest.methods' },
      { newValue: '0', name: 'disclosed.methods.cp.ic.ip.incentive.pricing' },
      { newValue: '0', name: 'disclosed.methods.cp.ic.ir.index.rate' },
      { newValue: '0', name: 'disclosed.methods.cp.ic.me.max.cap.eapr' },
      {
        newValue: '0',
        name: 'disclosed.methods.cp.ic.mf.minimum.finance.charge'
      },
      {
        newValue: '0',
        name: 'disclosed.methods.cp.ic.vi.incentive.pricing.variable.interest'
      },
      { newValue: '0', name: 'disclosed.methods.cp.io.ac.annual.charges' },
      {
        newValue: '0',
        name: 'disclosed.methods.cp.io.ci.cash.advance.item.charges'
      },
      {
        newValue: '0',
        name: 'disclosed.methods.cp.io.mc.miscellaneous.charges'
      },
      {
        newValue: '0',
        name: 'disclosed.methods.cp.io.mi.merchandise.item.charges'
      },
      { newValue: '0', name: 'disclosed.methods.cp.pf.lc.late.charges' },
      { newValue: '0', name: 'disclosed.methods.cp.pf.oc.overlimit.charges' },
      {
        newValue: '0',
        name: 'disclosed.methods.cp.pf.rc.return.check.charges'
      },
      { newValue: '0', name: 'disclosed.methods.cp.po.mp.minimum.payment.due' },

      { newValue: '0', name: 'penalty.pricing.change.in.terms.indicator' },

      { newValue: '0', name: 'protected.balance.method.for.cash' },
      { newValue: '0', name: 'protected.balance.method.for.merchandise' },
      {
        newValue: '0',
        name: 'protected.balance.method.for.non.interest.bearing.balance'
      },

      { newValue: '0', name: 'cit.number.of.days.before.terms.effective' },
      { newValue: '0', name: 'delay.period.after.account.opening.date' },
      {
        newValue: '0',
        name: 'number.of.months.of.disclosure.displays.for.active.accounts'
      },
      {
        newValue: '0',
        name: 'number.of.months.of.disclosure.displays.for.inactive.accounts'
      },

      { newValue: '0', name: 'credit.application.group' },
      {
        newValue: '0',
        name: 'choose.the.interest.default.cp.ic.id.methods.to.protect.set.1'
      },
      {
        newValue: '0',
        name: 'choose.the.interest.default.cp.ic.id.methods.to.protect.set.2'
      },
      {
        newValue: '0',
        name: 'choose.the.interest.default.cp.ic.id.methods.to.protect.set.3'
      },
      {
        newValue: '0',
        name: 'choose.the.interest.method.cp.ic.im.methods.to.protect.set.1'
      },
      {
        newValue: '0',
        name: 'choose.the.interest.method.cp.ic.im.methods.to.protect.set.2'
      },
      {
        newValue: '0',
        name: 'choose.the.interest.method.cp.ic.im.methods.to.protect.set.3'
      },
      {
        newValue: '0',
        name: 'choose.the.minimum.payment.pl.rt.mp.methods.to.protect.set.1'
      },
      {
        newValue: '0',
        name: 'choose.the.minimum.payment.pl.rt.mp.methods.to.protect.set.2'
      },
      {
        newValue: '0',
        name: 'choose.the.minimum.payment.pl.rt.mp.methods.to.protect.set.3'
      },
      { newValue: '0', name: 'protected.balance.description' },
      { newValue: '0', name: 'protected.balance.alternate.description' },
      { newValue: '0', name: 'description.display.option' },
      { newValue: '0', name: 'protected.balance.statement.message.text' },
      { newValue: '0', name: 'protected.balance.statement.message.options' },

      { newValue: '0', name: 'protect.balances.for.penalty.pricing' },
      { newValue: '0', name: 'delay.intro.text.id' },
      { newValue: '0', name: 'pb.balance.transfer' },
      { newValue: '0', name: 'protected.balance.override.code' },
      { newValue: '0', name: 'group.identifier' },
      //Advance
      { newValue: '0', name: 'change.in.terms.reason.code' },
      { newValue: '0', name: 'protected.balance.delay.period' },
      {
        newValue: '0',
        name: 'disclosed.method.overrides.cp.ic.id.interest.defaults'
      },
      {
        newValue: '0',
        name: 'disclosed.method.overrides.cp.ic.im.interest.methods'
      }
    ];

    const metadata = {
      protectedBalancesAttributesCreditApplicationGroup: [
        { code: 'code', text: 'text' }
      ],
      protectedBalancesAttributesMethodToProtectInterestDefault1: [
        { code: 'code', text: 'text' }
      ],
      protectedBalancesAttributesMethodToProtectInterestDefault2: [
        { code: 'code', text: 'text' }
      ],
      protectedBalancesAttributesMethodToProtectInterestDefault3: [
        { code: 'code', text: 'text' }
      ],
      protectedBalancesAttributesMethodToProtectInterestMethod1: [
        { code: 'code', text: 'text' }
      ],
      protectedBalancesAttributesMethodToProtectInterestMethod2: [
        { code: 'code', text: 'text' }
      ],
      protectedBalancesAttributesMethodToProtectInterestMethod3: [
        { code: 'code', text: 'text' }
      ],
      protectedBalancesAttributesMethodToProtectMinimumPayment1: [
        { code: 'code', text: 'text' }
      ],
      protectedBalancesAttributesMethodToProtectMinimumPayment2: [
        { code: 'code', text: 'text' }
      ],
      protectedBalancesAttributesMethodToProtectMinimumPayment3: [
        { code: 'code', text: 'text' }
      ],
      protectedBalancesAttributesDescriptionDisplayOption: [
        { code: 'code', text: 'text' }
      ],
      protectedBalancesAttributesMessageText: [{ code: 'code', text: 'text' }],
      protectedBalancesAttributesMessageOptions: [
        { code: 'code', text: 'text' }
      ],
      protectedBalancesAttributesProtectBalancesForPenaltyPricingOptions: [
        { code: 'code', text: 'text' }
      ],
      protectedBalancesAttributesDelayIntroTextIDOptions: [
        { code: 'code', text: 'text' }
      ],
      protectedBalancesAttributesPBBalanceTransferOptions: [
        { code: 'code', text: 'text' }
      ],
      protectedBalancesAttributesProtectedBalanceOverrideCodeOptions: [
        { code: 'code', text: 'text' }
      ],
      protectedBalancesAttributesGroupIdentifierOptions: [
        { code: 'code', text: 'text' }
      ]
    };
    const wrapper = await renderComponent(
      'testId',
      <SubRowGrid metadata={metadata} original={original} />
    );

    expect(
      wrapper.getByText(
        parameterProtectedBalancesAttributesList[0].parameterGroup
      )
    ).toBeInTheDocument();
    expect(
      wrapper.getByText(
        parameterProtectedBalancesAttributesList[1].parameterGroup
      )
    ).toBeInTheDocument();
    expect(
      wrapper.getByText(
        parameterProtectedBalancesAttributesList[2].parameterGroup
      )
    ).toBeInTheDocument();
  });
});
