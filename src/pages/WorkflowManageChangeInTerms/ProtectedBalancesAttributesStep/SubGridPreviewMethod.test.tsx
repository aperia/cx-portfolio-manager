import { render } from '@testing-library/react';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import React from 'react';
import SubGridPreviewMethod from './SubGridPreviewMethod';

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

const metadata = {
  [MethodFieldParameterEnum.ProtectedBalancesAttributesCreditApplicationGroup]:
    'test',
  [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault1]:
    'none',
  [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault2]:
    'test',
  [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault3]:
    'test',
  [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod1]:
    'test',
  [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod2]:
    'test',
  [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod3]:
    'test',
  [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment1]:
    'test',
  [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment2]:
    'test',
  [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment3]:
    'test',

  [MethodFieldParameterEnum.ProtectedBalancesAttributesCreditApplicationGroup]:
    'test',
  [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault1]:
    'test',
  [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault2]:
    'test',
  [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault3]:
    'test',
  [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod1]:
    'test',
  [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod2]:
    'test',
  [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod3]:
    'test',
  [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment1]:
    'test',
  [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment2]:
    'test',
  [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment3]:
    'test',
  // ProtectedBalancesAttributes - Statement Display
  [MethodFieldParameterEnum.ProtectedBalancesAttributesDescription]: 'test',
  [MethodFieldParameterEnum.ProtectedBalancesAttributesAlternateDescription]:
    'test',
  [MethodFieldParameterEnum.ProtectedBalancesAttributesDescriptionDisplayOption]:
    'test',
  [MethodFieldParameterEnum.ProtectedBalancesAttributesMessageText]: 'test',
  [MethodFieldParameterEnum.ProtectedBalancesAttributesMessageOptions]: 'test',
  [MethodFieldParameterEnum.ProtectedBalancesAttributesProtectBalancesForPenaltyPricing]:
    'test',
  [MethodFieldParameterEnum.ProtectedBalancesAttributesDelayIntroTextID]:
    'test',
  [MethodFieldParameterEnum.ProtectedBalancesAttributesPBBalanceTransfer]:
    'test',
  [MethodFieldParameterEnum.ProtectedBalancesAttributesProtectedBalanceOverrideCode]:
    'test',
  [MethodFieldParameterEnum.ProtectedBalancesAttributesGroupIdentifier]: 'none'
};

describe('pages > WorkflowManageChangeInTerms > ProtectedBalancesAttributesStep > SubGridPreviewMethod', () => {
  it('Info_Basic > empty designPromotionBasicInformation5', () => {
    const original = { id: 'Info_Settings' };
    const props = {
      metadata,
      original
    };
    const wrapper = render(<SubGridPreviewMethod {...props} />);
    expect(wrapper.getByText('test')).toBeInTheDocument();
  });
});
