import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React from 'react';
import ConfigureParameters, {
  ConfigureParametersFormValue
} from '../ConfigureParameters';
import Summary from '../ConfigureParameters/Summary';
import parseFormValues from './parseFormValues';

const ProtectedBalancesAttributesStep: React.FC<
  WorkflowSetupProps<ConfigureParametersFormValue>
> = prop => {
  return <ConfigureParameters {...prop} />;
};

const ExtraStaticProtectedBalancesAttributesStep =
  ProtectedBalancesAttributesStep as WorkflowSetupStaticProp;

ExtraStaticProtectedBalancesAttributesStep.summaryComponent = Summary;
ExtraStaticProtectedBalancesAttributesStep.defaultValues = {
  isValid: false,
  methods: []
};
ExtraStaticProtectedBalancesAttributesStep.parseFormValues = parseFormValues;

export default ExtraStaticProtectedBalancesAttributesStep;
