import {
  MethodFieldNameEnum,
  MethodFieldParameterEnum
} from 'app/constants/enums';
import React from 'react';
import {
  ParameterProtectedBalancesAttributesGroup,
  ParameterProtectedBalancesAttributesList,
  ParameterProtectedBalancesAttributesListIds
} from './types';

export const ONLINE_PCF = {
  ProtectedBalancesAttributesCreditApplicationGroup: 'Credit Application Group',
  ProtectedBalancesAttributesMethodToProtectInterestDefault1:
    'Interest Defaults (CP/IC/ID) Set 1',
  ProtectedBalancesAttributesMethodToProtectInterestDefault2:
    'Interest Defaults (CP/IC/ID) Set 2',
  ProtectedBalancesAttributesMethodToProtectInterestDefault3:
    'Interest Defaults (CP/IC/ID) Set 3',
  ProtectedBalancesAttributesMethodToProtectInterestMethod1:
    'Interest Methods (CP/IC/IM) Set 1',
  ProtectedBalancesAttributesMethodToProtectInterestMethod2:
    'Interest Methods (CP/IC/IM) Set 2',
  ProtectedBalancesAttributesMethodToProtectInterestMethod3:
    'Interest Methods (CP/IC/IM) Set 3',
  ProtectedBalancesAttributesMethodToProtectMinimumPayment1:
    'Minimum Payment (PL/RT/MP) Set 1',
  ProtectedBalancesAttributesMethodToProtectMinimumPayment2:
    'Minimum Payment (PL/RT/MP) Set 2',
  ProtectedBalancesAttributesMethodToProtectMinimumPayment3:
    'Minimum Payment (PL/RT/MP) Set 3',
  ProtectedBalancesAttributesDescription: 'Description',
  ProtectedBalancesAttributesAlternateDescription: 'Alternate Description',
  ProtectedBalancesAttributesDescriptionDisplayOption: 'Description Display',
  ProtectedBalancesAttributesMessageText: 'Statement Message Text ID',
  ProtectedBalancesAttributesMessageOptions: 'Stmt Msg Display Code',

  ProtectedBalancesAttributesProtectBalancesForPenaltyPricing:
    'Penalty on Protected Bal Override',
  ProtectedBalancesAttributesDelayIntroTextID: 'Delay/Intro Text ID',
  ProtectedBalancesAttributesPBBalanceTransfer: 'PB Balance Transfer',
  ProtectedBalancesAttributesProtectedBalanceOverrideCode:
    'Protected Balance Override I/E',
  ProtectedBalancesAttributesGroupIdentifier: 'Group Identifier'
};

export const MORE_INFO = {
  ProtectedBalancesAttributesCreditApplicationGroup:
    'The Credit Application Group parameter identifies a payment group that determines how payments are applied to protected balances to which unique minimum payment terms have not been assigned.',
  ProtectedBalancesAttributesMethodToProtectInterestDefault1:
    'The Choose the Interest Default (CP IC ID) Methods to Protect Set 1 parameter contains the interest default method names you want the System to use for the protected balance.',
  ProtectedBalancesAttributesMethodToProtectInterestDefault2:
    'The Choose the Interest Default (CP IC ID) Methods to Protect Set 2 parameter contains the interest default method names you want the System to use for the protected balance.',
  ProtectedBalancesAttributesMethodToProtectInterestDefault3:
    'The Choose the Interest Default (CP IC ID) Methods to Protect Set 3 parameter contains the interest default method names you want the System to use for the protected balance.',
  ProtectedBalancesAttributesMethodToProtectInterestMethod1:
    'The Choose the Interest Method (CP IC IM) Methods to Protect Set 1 parameter contains the method names you want the System to use for interest processing for the protected balance.',
  ProtectedBalancesAttributesMethodToProtectInterestMethod2:
    'The Choose the Interest Method (CP IC IM) Methods to Protect Set 2 parameter contains the method names you want the System to use for interest processing for the protected balance.',
  ProtectedBalancesAttributesMethodToProtectInterestMethod3:
    'The Choose the Interest Method (CP IC IM) Methods to Protect Set 3 parameter contains the method names you want the System to use for interest processing for the protected balance.',
  ProtectedBalancesAttributesMethodToProtectMinimumPayment1:
    'The Choose the Minimum Payment (PL/RT/MP) Methods to Protect Set 1 parameter contains the method names you want the System to use for minimum payment due processing for the protected balance.',
  ProtectedBalancesAttributesMethodToProtectMinimumPayment2:
    'The Choose the Minimum Payment (PL RT MP) Methods to Protect Set 2 parameter contains the method names you want the System to use for minimum payment due processing for the protected balance.',
  ProtectedBalancesAttributesMethodToProtectMinimumPayment3:
    'The Choose the Minimum Payment (PL RT MP) Methods to Protect Set 3 parameter contains the method names you want the System to use for minimum payment due processing for the protected balance.',
  ProtectedBalancesAttributesDescription:
    'The Protected Balance Description parameter provides a description for a specific protected balance. This free-form text appears as the second statement detail line of a transaction if you set the Description Display parameter in this section to 1. Text you type for this parameter can display on an Enterprise Presentation statement.',
  ProtectedBalancesAttributesAlternateDescription:
    'The Protected Balance Alternate Description parameter provides an alternate description for a specific protected balance. This free-form text appears as the second statement detail line of a transaction if you set the Description Display parameter in this section to 2. Text you type for this parameter can display on an Enterprise Presentation statement.',
  ProtectedBalancesAttributesDescriptionDisplayOption:
    'The Description Display Option parameter instructs the System whether to include a description of the protected balance in the second transaction detail line on a customer statement and, if so, which description to include.',
  ProtectedBalancesAttributesMessageText:
    'The Protected Balance Statement Message Text parameter identifies the message that will appear in the body of customer statements. This message will be used throughout the life of a protected balance. If you set the Base Interest Set 1-3 parameters in this section to zero or 1, you can use this parameter for a text message disclosing interest rates and average daily balance information. If you use this parameter to specify a message for protected balances, this message will appear for a protected balance that includes any balance, even if the balance includes only non-interest bearing amounts.',
  ProtectedBalancesAttributesMessageOptions:
    'The Protected Balance Statement Message Options parameter controls the display of the protected balance-level message on the customer’s paper statement, the online statement in the Customer Inquiry System (CIS), or both.',

  ProtectedBalancesAttributesProtectBalancesForPenaltyPricing:
    'The Protect Balances for Penalty Pricing parameter instructs the System whether to apply penalty pricing to delinquent accounts with a specific protected balance.',
  ProtectedBalancesAttributesDelayIntroTextID:
    'The Delay/Intro Text ID parameter determines which of your promotional messages appears in the body of a cardholder statement during the incentive pricing period of a protected balance.',
  ProtectedBalancesAttributesPBBalanceTransfer:
    'The PB Balance Transfer parameter determines whether the System defines the protected balance as a balance transfer.',
  ProtectedBalancesAttributesProtectedBalanceOverrideCode:
    'The Protected Balance Override Code parameter controls whether the System allows the standard interest rate to override the protected standard balance interest rate.',
  ProtectedBalancesAttributesGroupIdentifier:
    'The Group Identifier parameter specifies the client-defined name assigned to any protected balances you want to group for processing in Rules Management.',
  protectedBalancesAtributesGroupTextType_mt_pm:
    'Text Area = MT, Text Type = PM.'
};

export const parameterProtectedBalancesAttributesList: ParameterProtectedBalancesAttributesList[] =
  [
    {
      id: 'Info_Settings',
      section: 'txt_manage_penalty_fee_standard_parameters',
      parameterGroup: 'Basic Settings'
    },
    {
      id: 'Info_StatementDisplay',
      section: 'txt_manage_penalty_fee_standard_parameters',
      parameterGroup: 'Statement Display'
    },
    {
      id: 'Info_MethodsToProtect',
      section: 'txt_manage_penalty_fee_standard_parameters',
      parameterGroup: 'Methods to Protect'
    },
    {
      id: 'Info_PenaltyPricing',
      section: 'txt_manage_penalty_fee_advanced_parameters',
      parameterGroup: 'Penalty Pricing'
    },
    {
      id: 'Info_MiscellaneousParameters',
      section: 'txt_manage_penalty_fee_advanced_parameters',
      parameterGroup: 'Miscellaneous Parameters'
    }
  ];

export const parameterProtectedBalancesAttributesGroup: Record<
  ParameterProtectedBalancesAttributesListIds,
  ParameterProtectedBalancesAttributesGroup[]
> = {
  Info_Settings: [
    {
      id: MethodFieldParameterEnum.ProtectedBalancesAttributesCreditApplicationGroup,
      fieldName:
        MethodFieldNameEnum.ProtectedBalancesAttributesCreditApplicationGroup,
      onlinePCF: ONLINE_PCF.ProtectedBalancesAttributesCreditApplicationGroup,
      moreInfoText: MORE_INFO.ProtectedBalancesAttributesCreditApplicationGroup,
      moreInfo: MORE_INFO.ProtectedBalancesAttributesCreditApplicationGroup
    }
  ],
  Info_MethodsToProtect: [
    {
      id: MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault1,
      fieldName:
        MethodFieldNameEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault1,
      onlinePCF:
        ONLINE_PCF.ProtectedBalancesAttributesMethodToProtectInterestDefault1,
      moreInfoText:
        MORE_INFO.ProtectedBalancesAttributesMethodToProtectInterestDefault1,
      moreInfo:
        MORE_INFO.ProtectedBalancesAttributesMethodToProtectInterestDefault1
    },
    {
      id: MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault2,
      fieldName:
        MethodFieldNameEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault2,
      onlinePCF:
        ONLINE_PCF.ProtectedBalancesAttributesMethodToProtectInterestDefault2,
      moreInfoText:
        MORE_INFO.ProtectedBalancesAttributesMethodToProtectInterestDefault2,
      moreInfo:
        MORE_INFO.ProtectedBalancesAttributesMethodToProtectInterestDefault2
    },
    {
      id: MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault3,
      fieldName:
        MethodFieldNameEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault3,
      onlinePCF:
        ONLINE_PCF.ProtectedBalancesAttributesMethodToProtectInterestDefault3,
      moreInfoText:
        MORE_INFO.ProtectedBalancesAttributesMethodToProtectInterestDefault3,
      moreInfo:
        MORE_INFO.ProtectedBalancesAttributesMethodToProtectInterestDefault3
    },
    {
      id: MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod1,
      fieldName:
        MethodFieldNameEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod1,
      onlinePCF:
        ONLINE_PCF.ProtectedBalancesAttributesMethodToProtectInterestMethod1,
      moreInfoText:
        MORE_INFO.ProtectedBalancesAttributesMethodToProtectInterestMethod1,
      moreInfo:
        MORE_INFO.ProtectedBalancesAttributesMethodToProtectInterestMethod1
    },
    {
      id: MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod2,
      fieldName:
        MethodFieldNameEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod2,
      onlinePCF:
        ONLINE_PCF.ProtectedBalancesAttributesMethodToProtectInterestMethod2,
      moreInfoText:
        MORE_INFO.ProtectedBalancesAttributesMethodToProtectInterestMethod2,
      moreInfo:
        MORE_INFO.ProtectedBalancesAttributesMethodToProtectInterestMethod2
    },
    {
      id: MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod3,
      fieldName:
        MethodFieldNameEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod3,
      onlinePCF:
        ONLINE_PCF.ProtectedBalancesAttributesMethodToProtectInterestMethod3,
      moreInfoText:
        MORE_INFO.ProtectedBalancesAttributesMethodToProtectInterestMethod3,
      moreInfo:
        MORE_INFO.ProtectedBalancesAttributesMethodToProtectInterestMethod3
    },
    {
      id: MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment1,
      fieldName:
        MethodFieldNameEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment1,
      onlinePCF:
        ONLINE_PCF.ProtectedBalancesAttributesMethodToProtectMinimumPayment1,
      moreInfoText:
        MORE_INFO.ProtectedBalancesAttributesMethodToProtectMinimumPayment1,
      moreInfo:
        MORE_INFO.ProtectedBalancesAttributesMethodToProtectMinimumPayment1
    },
    {
      id: MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment2,
      fieldName:
        MethodFieldNameEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment2,
      onlinePCF:
        ONLINE_PCF.ProtectedBalancesAttributesMethodToProtectMinimumPayment2,
      moreInfoText:
        MORE_INFO.ProtectedBalancesAttributesMethodToProtectMinimumPayment2,
      moreInfo:
        MORE_INFO.ProtectedBalancesAttributesMethodToProtectMinimumPayment2
    },
    {
      id: MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment3,
      fieldName:
        MethodFieldNameEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment3,
      onlinePCF:
        ONLINE_PCF.ProtectedBalancesAttributesMethodToProtectMinimumPayment3,
      moreInfoText:
        MORE_INFO.ProtectedBalancesAttributesMethodToProtectMinimumPayment3,
      moreInfo:
        MORE_INFO.ProtectedBalancesAttributesMethodToProtectMinimumPayment3
    }
  ],
  Info_StatementDisplay: [
    {
      id: MethodFieldParameterEnum.ProtectedBalancesAttributesDescription,
      fieldName: MethodFieldNameEnum.ProtectedBalancesAttributesDescription,
      onlinePCF: ONLINE_PCF.ProtectedBalancesAttributesDescription,
      moreInfoText: MORE_INFO.ProtectedBalancesAttributesDescription,
      moreInfo: MORE_INFO.ProtectedBalancesAttributesDescription
    },
    {
      id: MethodFieldParameterEnum.ProtectedBalancesAttributesAlternateDescription,
      fieldName:
        MethodFieldNameEnum.ProtectedBalancesAttributesAlternateDescription,
      onlinePCF: ONLINE_PCF.ProtectedBalancesAttributesAlternateDescription,
      moreInfoText: MORE_INFO.ProtectedBalancesAttributesAlternateDescription,
      moreInfo: MORE_INFO.ProtectedBalancesAttributesAlternateDescription
    },
    {
      id: MethodFieldParameterEnum.ProtectedBalancesAttributesDescriptionDisplayOption,
      fieldName:
        MethodFieldNameEnum.ProtectedBalancesAttributesDescriptionDisplayOption,
      onlinePCF: ONLINE_PCF.ProtectedBalancesAttributesDescriptionDisplayOption,
      moreInfoText:
        MORE_INFO.ProtectedBalancesAttributesDescriptionDisplayOption,
      moreInfo: MORE_INFO.ProtectedBalancesAttributesDescriptionDisplayOption
    },
    {
      id: MethodFieldParameterEnum.ProtectedBalancesAttributesMessageText,
      fieldName: MethodFieldNameEnum.ProtectedBalancesAttributesMessageText,
      onlinePCF: ONLINE_PCF.ProtectedBalancesAttributesMessageText,
      moreInfoText: MORE_INFO.ProtectedBalancesAttributesMessageText,
      moreInfo: (
        <div>
          <p>{MORE_INFO.protectedBalancesAtributesGroupTextType_mt_pm}</p>
          <br />
          {MORE_INFO.ProtectedBalancesAttributesMessageText}
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.ProtectedBalancesAttributesMessageOptions,
      fieldName: MethodFieldNameEnum.ProtectedBalancesAttributesMessageOptions,
      onlinePCF: ONLINE_PCF.ProtectedBalancesAttributesMessageOptions,
      moreInfoText: MORE_INFO.ProtectedBalancesAttributesMessageOptions,
      moreInfo: MORE_INFO.ProtectedBalancesAttributesMessageOptions
    }
  ],
  Info_PenaltyPricing: [
    {
      id: MethodFieldParameterEnum.ProtectedBalancesAttributesProtectBalancesForPenaltyPricing,
      fieldName:
        MethodFieldNameEnum.ProtectedBalancesAttributesProtectBalancesForPenaltyPricing,
      onlinePCF:
        ONLINE_PCF.ProtectedBalancesAttributesProtectBalancesForPenaltyPricing,
      moreInfoText:
        MORE_INFO.ProtectedBalancesAttributesProtectBalancesForPenaltyPricing,
      moreInfo:
        MORE_INFO.ProtectedBalancesAttributesProtectBalancesForPenaltyPricing
    }
  ],
  Info_MiscellaneousParameters: [
    {
      id: MethodFieldParameterEnum.ProtectedBalancesAttributesDelayIntroTextID,
      fieldName:
        MethodFieldNameEnum.ProtectedBalancesAttributesDelayIntroTextID,
      onlinePCF: ONLINE_PCF.ProtectedBalancesAttributesDelayIntroTextID,
      moreInfoText: MORE_INFO.ProtectedBalancesAttributesDelayIntroTextID,
      moreInfo: (
        <div>
          <p>{MORE_INFO.protectedBalancesAtributesGroupTextType_mt_pm}</p>
          <br />
          {MORE_INFO.ProtectedBalancesAttributesDelayIntroTextID}
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.ProtectedBalancesAttributesPBBalanceTransfer,
      fieldName:
        MethodFieldNameEnum.ProtectedBalancesAttributesPBBalanceTransfer,
      onlinePCF: ONLINE_PCF.ProtectedBalancesAttributesPBBalanceTransfer,
      moreInfoText: MORE_INFO.ProtectedBalancesAttributesPBBalanceTransfer,
      moreInfo: MORE_INFO.ProtectedBalancesAttributesPBBalanceTransfer
    },
    {
      id: MethodFieldParameterEnum.ProtectedBalancesAttributesProtectedBalanceOverrideCode,
      fieldName:
        MethodFieldNameEnum.ProtectedBalancesAttributesProtectedBalanceOverrideCode,
      onlinePCF:
        ONLINE_PCF.ProtectedBalancesAttributesProtectedBalanceOverrideCode,
      moreInfoText:
        MORE_INFO.ProtectedBalancesAttributesProtectedBalanceOverrideCode,
      moreInfo:
        MORE_INFO.ProtectedBalancesAttributesProtectedBalanceOverrideCode
    },
    {
      id: MethodFieldParameterEnum.ProtectedBalancesAttributesGroupIdentifier,
      fieldName: MethodFieldNameEnum.ProtectedBalancesAttributesGroupIdentifier,
      onlinePCF: ONLINE_PCF.ProtectedBalancesAttributesGroupIdentifier,
      moreInfoText: MORE_INFO.ProtectedBalancesAttributesGroupIdentifier,
      moreInfo: MORE_INFO.ProtectedBalancesAttributesGroupIdentifier
    }
  ]
};

export const META_PARAMETER_PROTECTED_BALANCES_ATTRIBUTES = [
  MethodFieldParameterEnum.ProtectedBalancesAttributesCreditApplicationGroup,
  MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault1,
  MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault2,
  MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault3,
  MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod1,
  MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod2,
  MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod3,
  MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment1,
  MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment2,
  MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment3,
  MethodFieldParameterEnum.ProtectedBalancesAttributesDescriptionDisplayOption,
  MethodFieldParameterEnum.ProtectedBalancesAttributesMessageText,
  MethodFieldParameterEnum.ProtectedBalancesAttributesMessageOptions,
  MethodFieldParameterEnum.ProtectedBalancesAttributesProtectBalancesForPenaltyPricing,
  MethodFieldParameterEnum.ProtectedBalancesAttributesDelayIntroTextID,
  MethodFieldParameterEnum.ProtectedBalancesAttributesPBBalanceTransfer,
  MethodFieldParameterEnum.ProtectedBalancesAttributesProtectedBalanceOverrideCode,
  MethodFieldParameterEnum.ProtectedBalancesAttributesGroupIdentifier
];
