import { MethodFieldParameterEnum } from 'app/constants/enums';
import { mapGridExpandCollapse, methodParamsToObject } from 'app/helpers';
import { Grid, useTranslation } from 'app/_libraries/_dls';
import React, { useState } from 'react';
import newMethodColumns from '../ConfigureParameters/newMethodColumns';
import { parameterProtectedBalancesAttributesList } from './constant';
import SubGridPreviewMethod from './SubGridPreviewMethod';
import {
  ParameterProtectedBalancesAttributesListIds,
  ProtectedBalancesAttributesMeta
} from './types';

export interface SubGridProps {
  original: WorkflowSetupMethod;
  metadata: ProtectedBalancesAttributesMeta;
}
const SubRowGrid: React.FC<SubGridProps> = ({ original, metadata }) => {
  const {
    protectedBalancesAttributesCreditApplicationGroup,
    protectedBalancesAttributesMethodToProtectInterestDefault1,
    protectedBalancesAttributesMethodToProtectInterestDefault2,
    protectedBalancesAttributesMethodToProtectInterestDefault3,
    protectedBalancesAttributesMethodToProtectInterestMethod1,
    protectedBalancesAttributesMethodToProtectInterestMethod2,
    protectedBalancesAttributesMethodToProtectInterestMethod3,
    protectedBalancesAttributesMethodToProtectMinimumPayment1,
    protectedBalancesAttributesMethodToProtectMinimumPayment2,
    protectedBalancesAttributesMethodToProtectMinimumPayment3,
    protectedBalancesAttributesDescriptionDisplayOption,
    protectedBalancesAttributesMessageText,
    protectedBalancesAttributesMessageOptions,
    protectedBalancesAttributesProtectBalancesForPenaltyPricingOptions,
    protectedBalancesAttributesDelayIntroTextIDOptions,
    protectedBalancesAttributesPBBalanceTransferOptions,
    protectedBalancesAttributesProtectedBalanceOverrideCodeOptions,
    protectedBalancesAttributesGroupIdentifierOptions
  } = metadata;

  const convertedObject = methodParamsToObject(original);

  const { t } = useTranslation();

  const data: Record<MethodFieldParameterEnum, any> = {
    [MethodFieldParameterEnum.ProtectedBalancesAttributesCreditApplicationGroup]:
      protectedBalancesAttributesCreditApplicationGroup.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ProtectedBalancesAttributesCreditApplicationGroup
          ]
      )?.text,
    [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault1]:
      protectedBalancesAttributesMethodToProtectInterestDefault1.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ProtectedBalancesAttributesMethodToProtectInterestDefault1
          ]
      )?.text,
    [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault2]:
      protectedBalancesAttributesMethodToProtectInterestDefault2.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ProtectedBalancesAttributesMethodToProtectInterestDefault2
          ]
      )?.text,
    [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault3]:
      protectedBalancesAttributesMethodToProtectInterestDefault3.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ProtectedBalancesAttributesMethodToProtectInterestDefault3
          ]
      )?.text,
    [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod1]:
      protectedBalancesAttributesMethodToProtectInterestMethod1.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ProtectedBalancesAttributesMethodToProtectInterestMethod1
          ]
      )?.text,
    [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod2]:
      protectedBalancesAttributesMethodToProtectInterestMethod2.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ProtectedBalancesAttributesMethodToProtectInterestMethod2
          ]
      )?.text,
    [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod3]:
      protectedBalancesAttributesMethodToProtectInterestMethod3.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ProtectedBalancesAttributesMethodToProtectInterestMethod3
          ]
      )?.text,
    [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment1]:
      protectedBalancesAttributesMethodToProtectMinimumPayment1.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ProtectedBalancesAttributesMethodToProtectMinimumPayment1
          ]
      )?.text,
    [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment2]:
      protectedBalancesAttributesMethodToProtectMinimumPayment2.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ProtectedBalancesAttributesMethodToProtectMinimumPayment2
          ]
      )?.text,
    [MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment3]:
      protectedBalancesAttributesMethodToProtectMinimumPayment3.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ProtectedBalancesAttributesMethodToProtectMinimumPayment3
          ]
      )?.text,
    [MethodFieldParameterEnum.ProtectedBalancesAttributesDescription]:
      convertedObject[
        MethodFieldParameterEnum.ProtectedBalancesAttributesDescription
      ],
    [MethodFieldParameterEnum.ProtectedBalancesAttributesAlternateDescription]:
      convertedObject[
        MethodFieldParameterEnum.ProtectedBalancesAttributesAlternateDescription
      ],
    [MethodFieldParameterEnum.ProtectedBalancesAttributesDescriptionDisplayOption]:
      protectedBalancesAttributesDescriptionDisplayOption.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ProtectedBalancesAttributesDescriptionDisplayOption
          ]
      )?.text,
    [MethodFieldParameterEnum.ProtectedBalancesAttributesMessageText]:
      protectedBalancesAttributesMessageText.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.ProtectedBalancesAttributesMessageText
          ]
      )?.text,
    [MethodFieldParameterEnum.ProtectedBalancesAttributesMessageOptions]:
      protectedBalancesAttributesMessageOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.ProtectedBalancesAttributesMessageOptions
          ]
      )?.text,
    [MethodFieldParameterEnum.ProtectedBalancesAttributesProtectBalancesForPenaltyPricing]:
      protectedBalancesAttributesProtectBalancesForPenaltyPricingOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ProtectedBalancesAttributesProtectBalancesForPenaltyPricing
          ]
      )?.text,
    [MethodFieldParameterEnum.ProtectedBalancesAttributesDelayIntroTextID]:
      protectedBalancesAttributesDelayIntroTextIDOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.ProtectedBalancesAttributesDelayIntroTextID
          ]
      )?.text,
    [MethodFieldParameterEnum.ProtectedBalancesAttributesPBBalanceTransfer]:
      protectedBalancesAttributesPBBalanceTransferOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ProtectedBalancesAttributesPBBalanceTransfer
          ]
      )?.text,
    [MethodFieldParameterEnum.ProtectedBalancesAttributesProtectedBalanceOverrideCode]:
      protectedBalancesAttributesProtectedBalanceOverrideCodeOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ProtectedBalancesAttributesProtectedBalanceOverrideCode
          ]
      )?.text,
    [MethodFieldParameterEnum.ProtectedBalancesAttributesGroupIdentifier]:
      protectedBalancesAttributesGroupIdentifierOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.ProtectedBalancesAttributesGroupIdentifier
          ]
      )?.text
  } as Record<MethodFieldParameterEnum, any>;

  const [expandedList, setExpandedList] = useState<
    ParameterProtectedBalancesAttributesListIds[]
  >(['Info_Settings']);

  return (
    <div className="p-20 overflow-hidden">
      <Grid
        columns={newMethodColumns(t)}
        data={parameterProtectedBalancesAttributesList}
        subRow={({ original: _original }: MagicKeyValue) => (
          <SubGridPreviewMethod
            original={_original}
            metadata={data}
            methodType={original.methodType}
          />
        )}
        dataItemKey="id"
        togglable
        expandedItemKey="id"
        expandedList={expandedList}
        onExpand={setExpandedList as any}
        toggleButtonConfigList={parameterProtectedBalancesAttributesList.map(
          mapGridExpandCollapse('id', t)
        )}
      />
    </div>
  );
};

export default SubRowGrid;
