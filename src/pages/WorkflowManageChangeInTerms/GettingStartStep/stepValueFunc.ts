import { FormGettingStart } from '.';

const stepValueFunc: WorkflowSetupStepValueFunc<FormGettingStart> = ({
  stepsForm: formValues
}) => {
  const { changeInTerms, protectedBalancesAttributes } = formValues || {};

  if (changeInTerms && protectedBalancesAttributes) {
    return 'Change in Terms (CP PB CT) and Protected Balances Attributes (PL RT PB)';
  }

  return changeInTerms
    ? 'Change in Terms (CP PB CT)'
    : protectedBalancesAttributes
    ? 'Protected Balances Attributes (PL RT PB)'
    : '';
};

export default stepValueFunc;
