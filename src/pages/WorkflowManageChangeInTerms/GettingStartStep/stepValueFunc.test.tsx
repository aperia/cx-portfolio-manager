import { FormGettingStart } from '.';
import stepValueFunc from './stepValueFunc';

describe('ManagePenaltyFeeWorkflow > GettingStartStep > stepValueFunc', () => {
  it('Should Return Check Charges and Late Charges', () => {
    const stepsForm = {
      protectedBalancesAttributes: true,
      changeInTerms: true
    } as FormGettingStart;

    const response = stepValueFunc({ stepsForm });
    expect(response).toEqual(
      'Change in Terms (CP PB CT) and Protected Balances Attributes (PL RT PB)'
    );
  });

  it('Should Return Check Charges', () => {
    const stepsForm = {
      changeInTerms: true
    } as FormGettingStart;

    const response = stepValueFunc({ stepsForm });
    expect(response).toEqual('Change in Terms (CP PB CT)');
  });

  it('Should Return Late Charges', () => {
    const stepsForm = {
      protectedBalancesAttributes: true
    } as FormGettingStart;

    const response = stepValueFunc({ stepsForm });
    expect(response).toEqual('Protected Balances Attributes (PL RT PB)');
  });

  it('Should Return empty', () => {
    const stepsForm: any = undefined;

    const response = stepValueFunc({ stepsForm });
    expect(response).toEqual('');
  });
});
