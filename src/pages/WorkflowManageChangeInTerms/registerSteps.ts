import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { stepRegistry } from '../_commons/WorkflowSetup/registries';
import ChangeInTermStep from './ChangeInTermStep';
import GettingStartStep from './GettingStartStep';
import ProtectedBalancesAttributesStep from './ProtectedBalancesAttributesStep';

stepRegistry.registerStep('GetStartedManageChangeInTerms', GettingStartStep, [
  UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CHANGE_IN_TERM__GET_STARTED__EXISTED_WORKFLOW,
  UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CHANGE_IN_TERM__GET_STARTED__EXISTED_WORKFLOW__STUCK
]);

stepRegistry.registerStep(
  'ConfigureParametersChangeInTermStep',
  ChangeInTermStep,
  [UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CHANGE_IN_TERM__PARAMETERS]
);

stepRegistry.registerStep(
  'ConfigureParametersProtectedBalancesAttributesStep',
  ProtectedBalancesAttributesStep,
  [UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CHANGE_IN_TERM__PARAMETERS]
);
