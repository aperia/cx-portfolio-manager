import { fireEvent, render, RenderResult } from '@testing-library/react';
import React from 'react';
import GettingStartStep, { FormGettingStart } from './index';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');

  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      },
      i18n: { changeLanguage: jest.fn() }
    }),
    TransDLS: ({ keyTranslation }: any) => {
      return keyTranslation;
    }
  };
});

jest.mock('app/hooks/useUnsavedChangeRegistry', () => {
  return {
    __esModule: true,
    useUnsavedChangeRegistry: () => {}
  };
});

jest.mock('pages/_commons/OverviewModal', () => ({
  __esModule: true,
  default: ({ onClose }: any) => {
    return (
      <div>
        <div>OverviewModal</div>
        <button onClick={onClose}>OverviewButtonCloseModal</button>
      </div>
    );
  }
}));

const renderComponent = (props: any): RenderResult => {
  return render(<GettingStartStep {...props} />);
};

describe('ManagePayoffExceptionMethodsWorkflow > GettingStartStep', () => {
  it('Should render with Overview Button', () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: {},
      setFormValues: mockFn
    } as WorkflowSetupProps<FormGettingStart>;

    const wrapper = renderComponent(props);
    const overviewBtn = wrapper.getByText('txt_view_overview');
    expect(overviewBtn).toHaveAttribute('type', 'button');
  });
  it('Should render without Overview modal > Open overview modal', () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: {},
      setFormValues: mockFn
    } as WorkflowSetupProps<FormGettingStart>;

    const wrapper = renderComponent(props);
    expect(wrapper.queryByText(/OverviewModal/)).not.toBeInTheDocument;
    wrapper.rerender(
      <div>
        <GettingStartStep
          {...props}
          stepId="1"
          formValues={{ alreadyShown: true }}
        />
      </div>
    );

    const overviewBtn = wrapper.getByText('txt_view_overview');
    fireEvent.click(overviewBtn);
    expect(wrapper.queryByText(/OverviewModal/)).toBeInTheDocument;
  });

  it('Should render Overview modal > Close overview modal', () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: {
        isValid: true
      },
      setFormValues: mockFn
    } as WorkflowSetupProps<FormGettingStart>;

    const wrapper = renderComponent(props);
    expect(wrapper.queryByText(/OverviewModal/)).toBeInTheDocument;

    fireEvent.click(wrapper.getByText('OverviewButtonCloseModal'));
    expect(wrapper.queryByText(/OverviewModal/)).not.toBeInTheDocument;
  });

  it('Should render description paragraphs', () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: {
        isValid: true
      },
      setFormValues: mockFn
    } as WorkflowSetupProps<FormGettingStart>;

    const wrapper = renderComponent(props);
    const row = wrapper.baseElement.querySelector('.row') as Element;
    const Icon = wrapper.baseElement.querySelector('.icon') as Element;

    expect(row).toBeTruthy();
    expect(Icon).toBeTruthy();

    const desc1 = wrapper.getByText(
      'txt_manage_payoff_exception_methods_desc_1'
    );
    expect(desc1).toBeInTheDocument;
    expect(desc1.className).toBe('mt-8 color-grey');

    const desc2 = wrapper.getByText(
      'txt_manage_payoff_exception_methods_desc_2_bold'
    );
    expect(desc2).toBeInTheDocument;
    expect(desc2.className).toBe('mt-16 fw-600');

    const desc3 = wrapper.getByText(
      'txt_manage_payoff_exception_methods_desc_3_bold'
    );
    expect(desc3).toBeInTheDocument;
    expect(desc3.className).toBe('mt-8 fw-600');

    const desc4 = wrapper.getByText(
      'txt_manage_payoff_exception_methods_desc_4'
    );
    expect(desc4).toBeInTheDocument;
    expect(desc4.className).toBe('mt-8 color-grey');

    const desc5 = wrapper.getByText(
      'txt_manage_payoff_exception_methods_desc_5'
    );
    expect(desc5).toBeInTheDocument;
    expect(desc5.className).toBe('mt-8 color-grey');

    const desc6 = wrapper.getByText(
      'txt_manage_payoff_exception_methods_desc_6_bold'
    );
    expect(desc6).toBeInTheDocument;
    expect(desc6.className).toBe('mt-16 fw-600');

    const desc7 = wrapper.getByText(
      'txt_manage_payoff_exception_methods_desc_7'
    );
    expect(desc7).toBeInTheDocument;
    expect(desc7.className).toBe('mt-8 color-grey');

    const desc8 = wrapper.getByText(
      'txt_manage_payoff_exception_methods_desc_8'
    );
    expect(desc8).toBeInTheDocument;
    expect(desc8.className).toBe('mt-8 color-grey');

    const desc9 = wrapper.getByText(
      'txt_manage_payoff_exception_methods_desc_9'
    );
    expect(desc9).toBeInTheDocument;
    expect(desc9.className).toBe('mt-8 color-grey');

    const desc10 = wrapper.getByText(
      'txt_manage_payoff_exception_methods_desc_10'
    );
    expect(desc10).toBeInTheDocument;
    expect(desc10.className).toBe('mt-8 color-grey');

    const desc11 = wrapper.getByText(
      'txt_manage_payoff_exception_methods_desc_11'
    );
    expect(desc11).toBeInTheDocument;
    expect(desc11.className).toBe('mt-8 color-grey');

    const desc12 = wrapper.getByText(
      'txt_manage_payoff_exception_methods_desc_12'
    );
    expect(desc12).toBeInTheDocument;
    expect(desc12.className).toBe('mt-8 color-grey');

    const right1 = wrapper.getByText(
      'txt_manage_payoff_exception_methods_on_right_1'
    );
    expect(right1).toBeInTheDocument;

    const right2 = wrapper.getByText(
      'txt_manage_payoff_exception_methods_on_right_2'
    );
    expect(right2).toBeInTheDocument();
    expect(right2.className).toBe('mt-8 color-grey');

    const right3 = wrapper.getByText(
      'txt_manage_payoff_exception_methods_on_right_3'
    );
    expect(right3).toBeInTheDocument();
    expect(right3.className).toBe('mt-8 color-grey');

    const right4 = wrapper.getByText(
      'txt_manage_payoff_exception_methods_on_right_4'
    );
    expect(right4).toBeInTheDocument();
    expect(right4.className).toBe('mt-8 color-grey');

    const right5 = wrapper.getByText(
      'txt_manage_payoff_exception_methods_on_right_5'
    );
    expect(right5).toBeInTheDocument();
    expect(right5.className).toBe('mt-8 color-grey');
  });
});
