import { render, RenderResult } from '@testing-library/react';
import React from 'react';
import GettingStartSummary from './GettingStartSummary';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule
  };
});

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <GettingStartSummary {...props} />
    </div>
  );
};

describe('WorkflowManagePayoffExceptionMethods > GettingStartStep > GettingStartSummary', () => {
  it('Should render summary contains complete Icon', () => {
    const wrapper = renderComponent({
      formValues: {}
    } as WorkflowSetupSummaryProps);

    const Icon = wrapper.baseElement.querySelector('.icon');
    expect(Icon).toBeInTheDocument();
  });

  it('Should render summary contains Completed', () => {
    const wrapper = renderComponent({
      formValues: {}
    } as WorkflowSetupSummaryProps);

    expect(wrapper.getByText('txt_completed')).toBeInTheDocument;
  });
});
