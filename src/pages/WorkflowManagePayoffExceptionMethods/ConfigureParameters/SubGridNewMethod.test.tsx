import { MethodFieldParameterEnum } from 'app/constants/enums';
import { renderWithMockStore } from 'app/utils';
import React from 'react';
import SubGridNewMethod from './SubGridNewMethod';

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

const metadata = {
  [MethodFieldParameterEnum.PayoffExceptionCashOptionPromotionCode]: [
    { code: '0', text: 'None' },
    { code: '1', text: 'Test' }
  ],
  [MethodFieldParameterEnum.PayoffExceptionCashOptionPromotionCode]: [
    { code: '0', text: 'None' },
    { code: '1', text: 'Test' }
  ],
  [MethodFieldParameterEnum.PayoffExceptionCheckExceptionsDaysAfterPaymentDue]:
    [
      { code: '0', text: 'None' },
      { code: '1', text: 'Test' }
    ],
  [MethodFieldParameterEnum.PayoffExceptionPartialGraceSettings]: [
    { code: '0', text: 'None' },
    { code: '1', text: 'Test' }
  ],
  [MethodFieldParameterEnum.PayoffExceptionAmountToAvoidFinanceChargesSetting]:
    [
      { code: '0', text: 'None' },
      { code: '1', text: 'Test' }
    ],
  [MethodFieldParameterEnum.PayoffExceptionOldCash]: [
    { code: '0', text: 'None' },
    { code: '1', text: 'Test' }
  ],
  [MethodFieldParameterEnum.PayoffExceptionCycleToDateCash]: [
    { code: '0', text: 'None' },
    { code: '1', text: 'Test' }
  ],
  [MethodFieldParameterEnum.PayoffExceptionTwoCycleOldMerchandise]: [
    { code: '0', text: 'None' },
    { code: '1', text: 'Test' }
  ],
  [MethodFieldParameterEnum.PayoffExceptionOneCycleOldMerchandise]: [
    { code: '0', text: 'None' },
    { code: '1', text: 'Test' }
  ],
  [MethodFieldParameterEnum.PayoffExceptionCycleToDateMerchandise]: [
    { code: '0', text: 'None' },
    { code: '1', text: 'Test' }
  ]
};

const _props = { metadata, onChange: jest.fn(), searchValue: '' };

describe('pages > PayoffExceptionMethods > PayoffExceptionMethodsStep > SubGridNewMethod', () => {
  it('Info_Payoff_Exception_Usage', async () => {
    const props = {
      ..._props,
      original: { id: 'Info_Payoff_Exception_Usage' }
    };
    const wrapper = await renderWithMockStore(
      <SubGridNewMethod {...props} />,
      {}
    );

    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
    expect(wrapper.getByText('txt_value')).toBeInTheDocument();
    expect(wrapper.getByText('txt_more_info')).toBeInTheDocument();
  });
});
