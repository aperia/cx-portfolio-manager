import { ColumnType } from 'app/_libraries/_dls';
import { valueTranslation } from 'pages/_commons/Utils/formatGridField';

const newMethodColumns = (t: any): ColumnType[] => [
  {
    id: 'section',
    Header: t('txt_section'),
    width: 270,
    accessor: valueTranslation(['section'], t)
  },
  {
    id: 'parameterGroup',
    Header: t('txt_parameter_group'),
    width: 390,
    accessor: valueTranslation(['parameterGroup'], t)
  }
];

export default newMethodColumns;
