import HtmlTruncateText from 'app/components/HtmlTruncateText';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import { matchSearchValue } from 'app/helpers';
import {
  ColumnType,
  Grid,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React, { useMemo } from 'react';
import { parameterGroup, ParameterList } from './newMethodParameterList';

interface IProps {
  original: ParameterList;
  metadata: Record<string, string | number | undefined>;
}
const SubGridPreviewMethod: React.FC<IProps> = ({ original, metadata }) => {
  const { t } = useTranslation();

  const allParameters = useMemo(() => parameterGroup, []);
  const data = useMemo(
    () =>
      allParameters[original.id].filter(p =>
        matchSearchValue([p.fieldName, p.moreInfoText, p.onlinePCF], undefined)
      ),
    [original.id, allParameters]
  );

  const formatNumber = (text: string | number | undefined) => {
    if (!text) return '';
    return `$${new Intl.NumberFormat().format(parseFloat(`${text}`))}`;
  };

  const valueAccessor = (data: Record<string, any>) => {
    let valueTruncate: any = '';
    const paramId = data.id as MethodFieldParameterEnum;
    switch (data.type) {
      case 'numeric': {
        valueTruncate = formatNumber(metadata[paramId]);
        break;
      }
      default:
        valueTruncate =
          metadata[paramId]?.toString().toLowerCase() !== 'none'
            ? metadata[paramId]
            : '';
        break;
    }

    if (data.type === 'dropdown') {
      return (
        <HtmlTruncateText
          resizable
          lines={2}
          raw={valueTruncate}
          ellipsisLessText={t('txt_less')}
          ellipsisMoreText={t('txt_more')}
        >
          {valueTruncate?.replace(/\n/g, '</br>')?.replace(/\t/g, '&emsp;')}
        </HtmlTruncateText>
      );
    }

    return (
      <TruncateText
        resizable
        lines={2}
        ellipsisLessText={t('txt_less')}
        ellipsisMoreText={t('txt_more')}
      >
        {valueTruncate}
      </TruncateText>
    );
  };

  const columns: ColumnType[] = [
    {
      id: 'fieldName',
      Header: t('txt_business_name'),
      accessor: ({ fieldName }) => (
        <div>
          <TruncateText
            resizable
            lines={2}
            title={fieldName}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {fieldName}
          </TruncateText>
        </div>
      )
    },
    {
      id: 'onlinePCF',
      Header: t('txt_manage_penalty_fee_online_PCF'),
      accessor: ({ onlinePCF }) => (
        <div>
          <TruncateText
            resizable
            lines={2}
            title={onlinePCF}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {onlinePCF}
          </TruncateText>
        </div>
      )
    },
    {
      id: 'value',
      Header: t('txt_value'),
      accessor: valueAccessor
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      accessor: viewMoreInfo(['moreInfo'], t),
      width: 105
    }
  ];

  return (
    <div className="p-20">
      <Grid columns={columns} data={data} />
    </div>
  );
};

export default SubGridPreviewMethod;
