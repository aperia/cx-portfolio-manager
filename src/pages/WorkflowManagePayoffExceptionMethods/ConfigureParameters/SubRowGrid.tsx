import { mapGridExpandCollapse, methodParamsToObject } from 'app/helpers';
import { Grid, useTranslation } from 'app/_libraries/_dls';
import get from 'lodash.get';
import React, { useState } from 'react';
import {
  PAYOFF_DROPDOWN_FIELDS,
  PAYOFF_NUMERIC_TEXTBOX_FIELDS
} from './constant';
import newMethodColumns from './newMethodColumns';
import { parameterList, ParameterListIds } from './newMethodParameterList';
import SubGridPreviewMethod from './SubGridPreviewMethod';

export interface ParameterValueType {}

export interface SubGridProps {
  original: WorkflowSetupMethod;
  metadata: Record<string, RefData[]>;
}
const SubRowGrid: React.FC<SubGridProps> = ({ original, metadata }) => {
  const convertedObject = methodParamsToObject(original);

  const { t } = useTranslation();

  const getData = () => {
    const data: Record<string, string | number | undefined> = {};

    PAYOFF_DROPDOWN_FIELDS.forEach(field => {
      const value: string | number | undefined = get(convertedObject, [field]);
      Object.assign(data, {
        [field]: metadata[field].find(o => o.code === value)?.text
      });
    });

    PAYOFF_NUMERIC_TEXTBOX_FIELDS.forEach((field: string) => {
      const value: string | number | undefined = get(convertedObject, [field]);
      Object.assign(data, {
        [field]: value
      });
    });

    return data;
  };

  const [expandedList, setExpandedList] = useState<ParameterListIds[]>([
    'Info_Payoff_Exception_Usage'
  ]);

  return (
    <div className="p-20 overflow-hidden">
      <Grid
        togglable
        columns={newMethodColumns(t)}
        data={parameterList}
        subRow={({ original: _original }: MagicKeyValue) => (
          <SubGridPreviewMethod original={_original} metadata={getData()} />
        )}
        dataItemKey="id"
        expandedItemKey="id"
        expandedList={expandedList}
        onExpand={setExpandedList as any}
        toggleButtonConfigList={parameterList.map(
          mapGridExpandCollapse('id', t)
        )}
      />
    </div>
  );
};

export default SubRowGrid;
