import EnhanceDropdownList from 'app/components/EnhanceDropdownList';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import { matchSearchValue } from 'app/helpers';
import {
  ColumnType,
  Grid,
  NumericTextBox,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React, { useMemo } from 'react';
import { parameterGroup, ParameterList } from './newMethodParameterList';

interface IProps {
  searchValue: string;
  method: WorkflowSetupMethodObjectParams;
  original: ParameterList;
  metadata: Record<string, RefData[]>;
  onChange: (
    field: keyof WorkflowSetupMethodObjectParams,
    isRefData?: boolean
  ) => (e: any) => void;
}
const SubGridNewMethod: React.FC<IProps> = ({
  searchValue,
  method,
  original,
  metadata,
  onChange
}) => {
  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();
  const dataColumn = parameterGroup;

  const data = useMemo(
    () =>
      dataColumn[original.id].filter(p =>
        matchSearchValue(
          [p.fieldName, p.moreInfoText, p.onlinePCF],
          searchValue
        )
      ),
    [dataColumn, original.id, searchValue]
  );

  const valueAccessor = (data: Record<string, any>) => {
    const paramId = data.id as MethodFieldParameterEnum;

    if (data.type === 'dropdown') {
      const options: RefData[] = metadata[paramId];
      const value = options.find(o => o.code === method?.[paramId]);

      return (
        <EnhanceDropdownList
          placeholder={t('txt_select_an_option')}
          dataTestId={`addNewMethod__${paramId}`}
          size="small"
          value={value}
          onChange={onChange(paramId, true)}
          options={options}
          tooltipItemCustom={options.map(o => ({
            code: o.code,
            tooltipItem: (
              <div
                className="custom-tooltip"
                dangerouslySetInnerHTML={{
                  __html: o.text
                    ?.replace(/\n/g, '</br>')
                    ?.replace(/\t/g, '&emsp;') as any
                }}
              ></div>
            )
          }))}
        />
      );
    }

    return (
      <NumericTextBox
        dataTestId={`addNewMethod__${paramId}`}
        size="sm"
        format="c2"
        maxLength={13}
        autoFocus={false}
        value={method?.[paramId] as string}
        onChange={onChange(paramId)}
        showStep={false}
      />
    );
  };

  const columns: ColumnType[] = [
    {
      id: 'fieldName',
      Header: t('txt_business_name'),
      accessor: ({ fieldName }) => (
        <TruncateText
          resizable
          lines={2}
          title={fieldName}
          ellipsisLessText={t('txt_less')}
          ellipsisMoreText={t('txt_more')}
        >
          {fieldName}
        </TruncateText>
      ),
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'onlinePCF',
      Header: t('txt_manage_penalty_fee_online_PCF'),
      accessor: ({ onlinePCF }) => (
        <TruncateText
          resizable
          lines={2}
          title={onlinePCF}
          ellipsisLessText={t('txt_less')}
          ellipsisMoreText={t('txt_more')}
        >
          {onlinePCF}
        </TruncateText>
      ),
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'value',
      Header: t('txt_value'),
      cellBodyProps: { className: 'py-8' },
      accessor: valueAccessor,
      width: width < 1280 ? 280 : undefined
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      accessor: viewMoreInfo(['moreInfo'], t),
      width: 105,
      cellBodyProps: { className: 'pt-12 pb-8' }
    }
  ];

  return (
    <div className="p-20">
      <Grid columns={columns} data={data} />
    </div>
  );
};

export default SubGridNewMethod;
