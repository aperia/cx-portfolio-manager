import { BadgeColorType, ServiceSubjectSection } from 'app/constants/enums';
import { METHOD_PAYOFF_EXCEPTION_FIELDS } from 'app/constants/mapping';
import {
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { mapGridExpandCollapse } from 'app/helpers';
import { usePaginationGrid, useUnsavedChangeRegistry } from 'app/hooks';
import {
  Button,
  ColumnType,
  Grid,
  InlineMessage,
  useTranslation
} from 'app/_libraries/_dls';
import { DEFAULT_PAGE_SIZE } from 'app/_libraries/_dls/components/Pagination/constants';
import { isEmpty, orderBy } from 'lodash';
import {
  actionsWorkflowSetup,
  useSelectElementMetadataForPayoffMethods
} from 'pages/_commons/redux/WorkflowSetup';
import {
  formatBadge,
  formatText,
  formatTruncate
} from 'pages/_commons/Utils/formatGridField';
import Paging from 'pages/_commons/Utils/Paging';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';
import ActionButtons from './ActionButtons';
import AddNewMethodModal from './AddNewMethodModal';
import MethodListModal from './MethodListModal';
import parseFormValues from './parseFormValues';
import RemoveMethodModal from './RemoveMethodModal';
import SubRowGrid from './SubRowGrid';
import Summary from './Summary';

export interface PayoffExceptionFormValue {
  isValid?: boolean;

  methods?: (WorkflowSetupMethod & { rowId?: number })[];
}
const LateChargesStep: React.FC<WorkflowSetupProps<PayoffExceptionFormValue>> =
  ({ stepId, selectedStep, isStuck, savedAt, formValues, setFormValues }) => {
    const { t } = useTranslation();

    const keepRef = useRef({ setFormValues });
    keepRef.current.setFormValues = setFormValues;

    const dispatch = useDispatch();
    const [expandedList, setExpandedList] = useState<string[]>([]);
    const [showAddNewMethodModal, setShowAddNewMethodModal] = useState(false);
    const [isCreateNewVersion, setIsCreateNewVersion] = useState(false);
    const [isMethodToModel, setIsMethodToModel] = useState(false);
    const [removeMethodRowId, setRemoveMethodRowId] =
      useState<undefined | number>(undefined);
    const [editMethodRowId, setEditMethodRowId] =
      useState<undefined | number>(undefined);
    const [editMethodBackId, setEditMethodBackId] =
      useState<undefined | number>(undefined);
    const [hasChange, setHasChange] = useState(false);

    const metadata = useSelectElementMetadataForPayoffMethods();
    const methods = useMemo(
      () => orderBy(formValues?.methods || [], ['name'], ['asc']),
      [formValues?.methods]
    );
    const methodNames = useMemo(() => methods.map(i => i.name), [methods]);

    const prevExpandedList = useRef<string[]>();

    useEffect(() => {
      prevExpandedList.current = expandedList;
    }, [expandedList]);

    const onRemoveMethod = useCallback((idx: number) => {
      setRemoveMethodRowId(idx);
    }, []);

    const onEditMethod = useCallback((idx: number) => {
      setEditMethodRowId(idx);
    }, []);

    const onClickAddNewMethod = () => {
      setShowAddNewMethodModal(true);
    };
    const onClickMethodToModel = () => {
      setIsMethodToModel(true);
    };
    const onClickCreateNewVersion = () => {
      setIsCreateNewVersion(true);
    };
    const closeMethodListModalModal = () => {
      setIsCreateNewVersion(false);
      setIsMethodToModel(false);
    };

    useUnsavedChangeRegistry(
      {
        ...unsavedExistedWorkflowSetupDataProps,
        formName:
          UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_PAYOFF_EXCEPTION_METHODS__CONFIGURE_PARAMETERS,
        priority: 1
      },
      [hasChange]
    );

    const methodTypeToText = useCallback(
      (type: WorkflowSetupMethodType) => {
        switch (type) {
          case 'NEWVERSION':
            return t('txt_manage_penalty_fee_version_created');
          case 'MODELEDMETHOD':
            return t('txt_manage_penalty_fee_method_modeled');
        }
        return t('txt_manage_penalty_fee_method_created');
      },
      [t]
    );

    const columns: ColumnType[] = useMemo(
      () => [
        {
          id: 'methodName',
          Header: t('txt_method_name'),
          accessor: formatText(['name']),
          width: 105
        },
        {
          id: 'modelOrCreateFrom',
          Header: t('txt_manage_penalty_fee_modeled_or_create_from'),
          accessor: formatText(['modeledFrom', 'name']),
          width: 120
        },
        {
          id: 'comment',
          Header: t('txt_comment_area'),
          accessor: formatTruncate(['comment']),
          width: 312
        },
        {
          id: 'methodType',
          Header: t('txt_manage_penalty_fee_action_taken'),
          accessor: (data, idx) =>
            formatBadge(['methodType'], {
              colorType: BadgeColorType.MethodType,
              noBorder: true
            })(
              {
                ...data,
                methodType: methodTypeToText(data.methodType)
              },
              idx
            ),
          width: 153
        },
        {
          id: 'actions',
          Header: t('txt_actions'),
          accessor: (data: any) => (
            <div className="d-flex justify-content-center">
              <Button
                size="sm"
                variant="outline-primary"
                onClick={() => onEditMethod(data.rowId)}
              >
                {t('txt_edit')}
              </Button>
              <Button
                size="sm"
                variant="outline-danger"
                className="ml-8"
                onClick={() => onRemoveMethod(data.rowId)}
              >
                {t('txt_delete')}
              </Button>
            </div>
          ),
          className: 'text-center',
          cellBodyProps: { className: 'py-8' },
          width: 142
        }
      ],
      [t, methodTypeToText, onEditMethod, onRemoveMethod]
    );

    useEffect(() => {
      dispatch(
        actionsWorkflowSetup.getWorkflowMetadata(METHOD_PAYOFF_EXCEPTION_FIELDS)
      );
    }, [dispatch]);

    const {
      total,
      currentPage,
      currentPageSize,
      pageData,
      onPageChange,
      onPageSizeChange
    } = usePaginationGrid(methods);

    useEffect(() => {
      const isValid = (formValues?.methods?.length || 0) > 0;
      if (formValues.isValid === isValid) return;

      keepRef.current.setFormValues({ isValid });
    }, [formValues]);

    useEffect(() => {
      if (stepId !== selectedStep) {
        setExpandedList([]);
        onPageChange(1);
        onPageSizeChange(DEFAULT_PAGE_SIZE);
      }
      setHasChange(false);
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [stepId, selectedStep, savedAt]);

    const handleOnExpand = (dataKeyList: string[]) => {
      setExpandedList(isEmpty(dataKeyList) ? [] : dataKeyList);
    };

    const renderGrid = useMemo(() => {
      if (isEmpty(pageData)) return;
      return (
        <div className="pt-16">
          <Grid
            togglable
            columns={columns}
            data={pageData}
            subRow={({ original }: any) => (
              <SubRowGrid original={original} metadata={metadata} />
            )}
            dataItemKey="rowId"
            toggleButtonConfigList={pageData.map(
              mapGridExpandCollapse('rowId', t)
            )}
            expandedItemKey={'rowId'}
            expandedList={expandedList}
            onExpand={handleOnExpand}
            scrollable
          />
          {total > 10 && (
            <Paging
              totalItem={total}
              pageSize={currentPageSize}
              page={currentPage}
              onChangePage={onPageChange}
              onChangePageSize={onPageSizeChange}
            />
          )}
        </div>
      );
    }, [
      columns,
      currentPage,
      currentPageSize,
      expandedList,
      metadata,
      onPageChange,
      onPageSizeChange,
      pageData,
      t,
      total
    ]);

    return (
      <div>
        <div className="mt-8 pb-24 border-bottom color-grey">
          <p>{t('txt_manage_payoff_exception_methods_parameters_1')}</p>
        </div>
        {isStuck && (
          <InlineMessage className="mb-0 mt-24" variant="danger" withIcon>
            {t('txt_step_stuck_move_forward_message')}
          </InlineMessage>
        )}
        <div className="mt-24">
          <ActionButtons
            small={!isEmpty(methods)}
            title="txt_method_list"
            onClickAddNewMethod={onClickAddNewMethod}
            onClickMethodToModel={onClickMethodToModel}
            onClickCreateNewVersion={onClickCreateNewVersion}
          />
        </div>
        {renderGrid}
        {removeMethodRowId !== undefined && (
          <RemoveMethodModal
            id="removeMethod"
            show
            methodName={methods.find(i => i.rowId === removeMethodRowId)?.name}
            onClose={method => {
              if (method) {
                setFormValues({
                  methods: methods.filter(i => i.rowId !== removeMethodRowId)
                });
                setHasChange(true);
              }
              setRemoveMethodRowId(undefined);
            }}
          />
        )}
        {editMethodRowId !== undefined && (
          <AddNewMethodModal
            id="addNewMethod"
            show
            isCreate={false}
            method={methods.find(i => i.rowId === editMethodRowId)}
            methodNames={methodNames}
            onClose={(method, isBack) => {
              if (method) {
                method.serviceSubjectSection = ServiceSubjectSection.PEM;
                const newMethds = methods.map(item => {
                  if (item.rowId === editMethodRowId) return method;
                  return item;
                });
                setFormValues({ methods: newMethds });
                setHasChange(true);
                isBack && setEditMethodBackId(editMethodRowId);
              }
              setEditMethodRowId(undefined);
            }}
          />
        )}
        {editMethodBackId && (
          <MethodListModal
            id="editMethodFromModel"
            show
            methodNames={methodNames}
            isCreateNewVersion={
              methods.find(i => i.rowId === editMethodBackId)!.methodType ===
              'NEWVERSION'
            }
            defaultMethod={methods.find(i => i.rowId === editMethodBackId)}
            onClose={method => {
              if (method) {
                method.serviceSubjectSection = ServiceSubjectSection.PEM;
                const newMethds = methods.map(item => {
                  if (item.rowId === editMethodBackId) return method;
                  return item;
                });
                setFormValues({ methods: newMethds });
                setHasChange(true);
              }
              setEditMethodBackId(undefined);
            }}
          />
        )}
        {(isCreateNewVersion || isMethodToModel) && (
          <MethodListModal
            id="addMethodFromModel"
            show
            methodNames={methodNames}
            isCreateNewVersion={isCreateNewVersion}
            onClose={newMethod => {
              if (newMethod) {
                const rowId = Date.now();
                setFormValues({
                  methods: [
                    {
                      ...newMethod,
                      rowId,
                      serviceSubjectSection: ServiceSubjectSection.PEM
                    },
                    ...methods
                  ]
                });
                setExpandedList([rowId as any]);
                setHasChange(true);
              }
              closeMethodListModalModal();
            }}
          />
        )}
        {showAddNewMethodModal && (
          <AddNewMethodModal
            id="addNewMethod"
            show
            methodNames={methodNames}
            onClose={method => {
              if (method) {
                const rowId = Date.now();
                const _methods = orderBy(
                  [
                    {
                      ...method,
                      rowId,
                      serviceSubjectSection: ServiceSubjectSection.PEM
                    },
                    ...methods
                  ],
                  ['name'],
                  ['asc']
                );

                setFormValues({ methods: _methods });
                setExpandedList([rowId as any]);
                setHasChange(true);

                const newMethodIdx = _methods.findIndex(m => m.rowId === rowId);
                onPageChange(Math.ceil((newMethodIdx + 1) / currentPageSize));
              }
              setShowAddNewMethodModal(false);
            }}
          />
        )}
      </div>
    );
  };

const ExtraStaticLateChargesStep = LateChargesStep as WorkflowSetupStaticProp;

ExtraStaticLateChargesStep.defaultValues = { isValid: false, methods: [] };
ExtraStaticLateChargesStep.summaryComponent = Summary;

ExtraStaticLateChargesStep.parseFormValues = parseFormValues;

export default ExtraStaticLateChargesStep;
