import { parameterGroup, parameterList } from './newMethodParameterList';

describe('pages > PayoffExceptionMethods > PayoffExceptionMethodsStep > newMethodParameterList', () => {
  it('parameterList', () => {
    expect(Object.keys(parameterList).length).toEqual(2);
  });

  it('parameterGroup', () => {
    expect(Object.keys(parameterGroup).length).toEqual(2);
  });
});
