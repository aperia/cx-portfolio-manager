import EnhanceDropdownButton from 'app/components/EnhanceDropdownButton';
import { ADD_METHOD_BUTTONS } from 'app/constants/constants';
import { AddMethodButtons } from 'app/constants/enums';
import { useTranslation } from 'app/_libraries/_dls';
import {
  DropdownButton,
  DropdownButtonSelectEvent,
  Icon,
  Tooltip
} from 'app/_libraries/_dls/components';
import React, { useCallback, useMemo } from 'react';

interface ActionButtonsProps {
  small?: boolean;
  title?: string;
  onClickCreateNewVersion?: () => void;
  onClickMethodToModel?: () => void;
  onClickAddNewMethod?: () => void;
}

const ActionButtons: React.FC<ActionButtonsProps> = ({
  small = false,
  title = '',
  onClickCreateNewVersion,
  onClickMethodToModel,
  onClickAddNewMethod
}) => {
  const { t } = useTranslation();

  const dropdownListItem = useMemo(() => {
    return ADD_METHOD_BUTTONS.map(item => (
      <DropdownButton.Item
        key={item.value}
        label={t(item.description)}
        value={item}
      />
    ));
  }, [t]);

  const onSelectAddMethod = useCallback(
    (event: DropdownButtonSelectEvent) => {
      const { value } = event.target.value;
      switch (value) {
        case AddMethodButtons.CREATE_NEW_VERSION:
          onClickCreateNewVersion && onClickCreateNewVersion();
          break;
        case AddMethodButtons.ADD_NEW_METHOD:
          onClickAddNewMethod && onClickAddNewMethod();
          break;
        case AddMethodButtons.CHOOSE_METHOD_TO_MODEL:
          onClickMethodToModel && onClickMethodToModel();
          break;
      }
    },
    [onClickAddNewMethod, onClickMethodToModel, onClickCreateNewVersion]
  );

  if (small) {
    return (
      <div className="d-flex align-items-center">
        <h5>{t(title)}</h5>
        <div className="d-flex ml-auto mr-n8">
          <EnhanceDropdownButton
            buttonProps={{
              size: 'sm',
              children: t('txt_manage_penalty_fee_add_method'),
              variant: 'outline-primary'
            }}
            onSelect={onSelectAddMethod}
          >
            {dropdownListItem}
          </EnhanceDropdownButton>
        </div>
      </div>
    );
  }

  return (
    <div>
      <h5>{t('txt_manage_penalty_fee_select_an_option')}</h5>
      <div className="row mt-16">
        <div className="col-xl col-6">
          <div
            className="rcc-btn d-flex justify-content-between border rounded-lg py-10"
            onClick={onClickCreateNewVersion}
          >
            <span className="d-flex align-items-center ml-16">
              <Icon name="method-create" size="9x" className="color-grey-l16" />
              <span className="ml-12 mr-8">
                {t('txt_manage_penalty_fee_create_new_version')}
              </span>
              <Tooltip
                element={t('txt_manage_penalty_fee_create_new_version_desc')}
                triggerClassName="d-flex"
              >
                <Icon name="information" className="color-grey-l16" size="5x" />
              </Tooltip>
            </span>
          </div>
        </div>
        <div className="col-xl col-6">
          <div
            className="rcc-btn d-flex justify-content-between border rounded-lg py-10"
            onClick={onClickMethodToModel}
          >
            <span className="d-flex align-items-center ml-16">
              <Icon name="method-clone" size="9x" className="color-grey-l16" />
              <span className="ml-12 mr-8">
                {t('txt_manage_penalty_fee_choose_method_to_model')}
              </span>
              <Tooltip
                element={t(
                  'txt_manage_penalty_fee_choose_method_to_model_desc'
                )}
                triggerClassName="d-flex"
              >
                <Icon name="information" className="color-grey-l16" size="5x" />
              </Tooltip>
            </span>
          </div>
        </div>
        <div className="col-xl col-6 mt-xl-0 mt-16">
          <div
            className="rcc-btn d-flex justify-content-between border rounded-lg py-10"
            onClick={onClickAddNewMethod}
          >
            <span className="d-flex align-items-center ml-16">
              <Icon name="method-add" size="9x" className="color-grey-l16" />
              <span className="ml-12 mr-8">
                {t('txt_manage_penalty_fee_create_new_method')}
              </span>
              <Tooltip
                element={t('txt_manage_penalty_fee_create_new_method_desc')}
                triggerClassName="d-flex"
              >
                <Icon name="information" className="color-grey-l16" size="5x" />
              </Tooltip>
            </span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ActionButtons;
