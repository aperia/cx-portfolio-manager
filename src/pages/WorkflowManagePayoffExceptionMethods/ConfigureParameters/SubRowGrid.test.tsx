import { MethodFieldParameterEnum } from 'app/constants/enums';
import { renderComponent } from 'app/utils';
import React from 'react';
import { parameterList } from './newMethodParameterList';
import SubRowGrid from './SubRowGrid';

const t = (value: any) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

jest.mock('./SubGridPreviewMethod', () => ({
  __esModule: true,
  default: () => <div>SubGridPreviewMethod</div>
}));

jest.mock('./constant', () => {
  const constants = jest.requireActual('./constant');

  return {
    ...constants,
    PAYOFF_DROPDOWN_FIELDS: ['cash.option.promotion.code'],
    PAYOFF_NUMERIC_TEXTBOX_FIELDS: ['statement.value.for.payoff.exception']
  };
});

describe('pages > PayoffExceptionMethods > PayoffExceptionMethodsStep > SubRowGrid', () => {
  it('render', async () => {
    const method: WorkflowSetupMethod = {
      id: 'id',
      name: 'MTHMTCI',
      modeledFrom: {
        id: 'id1',
        name: 'MTHMTCI',
        versions: [{ id: 'id' }]
      },
      methodType: 'NEWVERSION',
      comment: '',
      parameters: [
        {
          name: MethodFieldParameterEnum.PayoffExceptionCashOptionPromotionCode,
          previousValue: '0',
          newValue: '0'
        }
      ]
    };
    const metadata = {
      [MethodFieldParameterEnum.PayoffExceptionCashOptionPromotionCode]: [
        { code: '0', text: 'None' },
        { code: '1', text: 'dataSource1' }
      ]
    };

    const wrapper = await renderComponent(
      'testId',
      <SubRowGrid metadata={metadata} original={method} />
    );

    expect(
      wrapper.getByText(parameterList[0].parameterGroup)
    ).toBeInTheDocument();
    expect(
      wrapper.getByText(parameterList[1].parameterGroup)
    ).toBeInTheDocument();
  });
});
