import { ServiceSubjectSection } from 'app/constants/enums';
import parseFormValues from './parseFormValues';

describe('pages > PayoffExceptionMethods > PayoffExceptionMethodsStep > parseFormValues', () => {
  it('should return undefined with methodList is undefined', () => {
    const response = parseFormValues(
      { data: { workflowSetupData: [{ id: '1' }] } } as any,
      '1',
      jest.fn()
    );
    expect(response).toEqual({
      isPass: false,
      isSelected: false,
      isValid: false,
      methods: []
    });
  });

  it('should return undefined with stepInfo is undefined', () => {
    const response = parseFormValues(
      {
        data: {
          workflowSetupData: [
            {
              id: '2'
            }
          ],
          configurations: {
            methodList: []
          }
        }
      } as any,
      '1',
      jest.fn()
    );
    expect(response).toEqual(undefined);
  });

  it('should return undefined with methodList does not have ServiceSubjectSection is LC', () => {
    const response = parseFormValues(
      {
        data: {
          workflowSetupData: [
            {
              id: '1'
            }
          ],
          configurations: {
            methodList: [
              {
                id: '123',
                name: '123',
                serviceSubjectSection: ServiceSubjectSection.RC
              }
            ]
          }
        }
      } as any,
      '1',
      jest.fn()
    );
    expect(response).toEqual({
      isPass: false,
      isSelected: false,
      isValid: false,
      methods: []
    });
  });

  it('should return valid data', () => {
    const response = parseFormValues(
      {
        data: {
          workflowSetupData: [
            {
              id: '1'
            }
          ],
          configurations: {
            methodList: [
              {
                id: '123',
                name: '123',
                serviceSubjectSection: ServiceSubjectSection.PEM
              }
            ]
          }
        }
      } as any,
      '1',
      jest.fn()
    );
    expect(response).toEqual({
      isPass: false,
      isSelected: false,
      isValid: true,
      methods: [
        {
          id: '',
          name: '123',
          methodType: 'NEWMETHOD',
          rowId: 0,
          serviceSubjectSection: ServiceSubjectSection.PEM
        }
      ]
    });
  });
});
