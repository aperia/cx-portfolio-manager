import {
  MethodFieldNameEnum,
  MethodFieldParameterEnum
} from 'app/constants/enums';
import React from 'react';
import { MORE_INFO, ONLINE_PCF } from './constant';

export type ParameterListIds =
  | 'Info_Payoff_Exception_Usage'
  | 'Info_Payoff_Exception_Options';

export interface ParameterList {
  id: ParameterListIds;
  section: 'txt_manage_payoff_exception_parameters';
  parameterGroup: 'Payoff Exception Usage' | 'Payoff Exception Options';
  moreInfo?: React.ReactNode;
}

export const parameterList: ParameterList[] = [
  {
    id: 'Info_Payoff_Exception_Usage',
    section: 'txt_manage_payoff_exception_parameters',
    parameterGroup: 'Payoff Exception Usage'
  },
  {
    id: 'Info_Payoff_Exception_Options',
    section: 'txt_manage_payoff_exception_parameters',
    parameterGroup: 'Payoff Exception Options'
  }
];

type FieldType = 'dropdown' | 'numeric';
export interface ParameterGroup {
  id: MethodFieldParameterEnum;
  fieldName: MethodFieldNameEnum;
  onlinePCF: string;
  moreInfoText: string;
  moreInfo: React.ReactNode;
  type: FieldType;
}

export const parameterGroup: Record<ParameterListIds, ParameterGroup[]> = {
  Info_Payoff_Exception_Usage: [
    {
      id: MethodFieldParameterEnum.PayoffExceptionStatementValue,
      fieldName: MethodFieldNameEnum.PayoffExceptionStatementValue,
      onlinePCF: ONLINE_PCF.PayoffExceptionUsageStatment,
      moreInfoText: MORE_INFO.PayoffExceptionUsageStatment,
      moreInfo: MORE_INFO.PayoffExceptionUsageStatment,
      type: 'numeric'
    },
    {
      id: MethodFieldParameterEnum.PayoffExceptionCashOptionPromotionCode,
      fieldName: MethodFieldNameEnum.PayoffExceptionCashOptionPromotionCode,
      onlinePCF: ONLINE_PCF.PayoffExceptionCashOptionPromotionCode,
      moreInfoText: MORE_INFO.PayoffExceptionCashOptionPromotionCode,
      moreInfo: MORE_INFO.PayoffExceptionCashOptionPromotionCode,
      type: 'dropdown'
    },
    {
      id: MethodFieldParameterEnum.PayoffExceptionMinimumBalanceToAssessInterestOn,
      fieldName:
        MethodFieldNameEnum.PayoffExceptionMinimumBalanceToAssessInterestOn,
      onlinePCF: ONLINE_PCF.PayoffExceptionMinimumBalanceToAssessInterestOn,
      moreInfoText: MORE_INFO.PayoffExceptionMinimumBalanceToAssessInterestOn,
      moreInfo: MORE_INFO.PayoffExceptionMinimumBalanceToAssessInterestOn,
      type: 'numeric'
    },
    {
      id: MethodFieldParameterEnum.PayoffExceptionCheckExceptionsDaysAfterPaymentDue,
      fieldName:
        MethodFieldNameEnum.PayoffExceptionCheckExceptionsDaysAfterPaymentDue,
      onlinePCF: ONLINE_PCF.PayoffExceptionCheckExceptionsDaysAfterPaymentDue,
      moreInfoText: MORE_INFO.PayoffExceptionCheckExceptionsDaysAfterPaymentDue,
      moreInfo: MORE_INFO.PayoffExceptionCheckExceptionsDaysAfterPaymentDue,
      type: 'dropdown'
    },
    {
      id: MethodFieldParameterEnum.PayoffExceptionPartialGraceSettings,
      fieldName: MethodFieldNameEnum.PayoffExceptionPartialGraceSettings,
      onlinePCF: ONLINE_PCF.PayoffExceptionPartialGraceSettings,
      moreInfoText: MORE_INFO.PayoffExceptionPartialGraceSettings,
      moreInfo: MORE_INFO.PayoffExceptionPartialGraceSettings,
      type: 'dropdown'
    },
    {
      id: MethodFieldParameterEnum.PayoffExceptionAmountToAvoidFinanceChargesSetting,
      fieldName:
        MethodFieldNameEnum.PayoffExceptionAmountToAvoidFinanceChargesSetting,
      onlinePCF: ONLINE_PCF.PayoffExceptionAmountToAvoidFinanceChargesSetting,
      moreInfoText: MORE_INFO.PayoffExceptionAmountToAvoidFinanceChargesSetting,
      moreInfo: MORE_INFO.PayoffExceptionAmountToAvoidFinanceChargesSetting,
      type: 'dropdown'
    }
  ],
  Info_Payoff_Exception_Options: [
    {
      id: MethodFieldParameterEnum.PayoffExceptionOldCash,
      fieldName: MethodFieldNameEnum.PayoffExceptionOldCash,
      onlinePCF: ONLINE_PCF.PayoffExceptionOldCash,
      moreInfoText: MORE_INFO.PayoffExceptionOptions,
      moreInfo: MORE_INFO.PayoffExceptionOptions,
      type: 'dropdown'
    },
    {
      id: MethodFieldParameterEnum.PayoffExceptionCycleToDateCash,
      fieldName: MethodFieldNameEnum.PayoffExceptionCycleToDateCash,
      onlinePCF: ONLINE_PCF.PayoffExceptionCycleToDateCash,
      moreInfoText: MORE_INFO.PayoffExceptionOptions,
      moreInfo: MORE_INFO.PayoffExceptionOptions,
      type: 'dropdown'
    },
    {
      id: MethodFieldParameterEnum.PayoffExceptionTwoCycleOldMerchandise,
      fieldName: MethodFieldNameEnum.PayoffExceptionTwoCycleOldMerchandise,
      onlinePCF: ONLINE_PCF.PayoffExceptionTwoCycleOldMerchandise,
      moreInfoText: MORE_INFO.PayoffExceptionOptions,
      moreInfo: MORE_INFO.PayoffExceptionOptions,
      type: 'dropdown'
    },
    {
      id: MethodFieldParameterEnum.PayoffExceptionOneCycleOldMerchandise,
      fieldName: MethodFieldNameEnum.PayoffExceptionOneCycleOldMerchandise,
      onlinePCF: ONLINE_PCF.PayoffExceptionOneCycleOldMerchandise,
      moreInfoText: MORE_INFO.PayoffExceptionOptions,
      moreInfo: MORE_INFO.PayoffExceptionOptions,
      type: 'dropdown'
    },
    {
      id: MethodFieldParameterEnum.PayoffExceptionCycleToDateMerchandise,
      fieldName: MethodFieldNameEnum.PayoffExceptionCycleToDateMerchandise,
      onlinePCF: ONLINE_PCF.PayoffExceptionCycleToDateMerchandise,
      moreInfoText: MORE_INFO.PayoffExceptionOptions,
      moreInfo: MORE_INFO.PayoffExceptionOptions,
      type: 'dropdown'
    }
  ]
};
