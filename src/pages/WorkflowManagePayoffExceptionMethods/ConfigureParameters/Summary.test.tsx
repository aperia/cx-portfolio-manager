import {
  fireEvent,
  queryByText,
  render,
  RenderResult
} from '@testing-library/react';
import { ServiceSubjectSection } from 'app/constants/enums';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManagePayoffExceptionMethods';
import React from 'react';
import Summary from './Summary';

jest.mock('./SubRowGrid', () => ({
  __esModule: true,
  default: () => <div>SubRowGrid_component</div>
}));

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const MockSelectElementMetadataForPEM = jest.spyOn(
  WorkflowSetup,
  'useSelectElementMetadataForPayoffMethods'
);

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <Summary {...props} />
    </div>
  );
};

const get1MockData = (i: number | string) => ({
  id: `${i}`,
  name: `name ${i}`,
  serviceSubjectSection: ServiceSubjectSection.DLO,
  methodType: 'NEWMETHOD'
});

const generateMockData = (length: number) => {
  const result = [];
  for (let i = 0; i < length; i++) {
    result.push(get1MockData(i));
  }

  return result;
};

describe('PayoffExceptionMethods > PayoffExceptionMethodsStep > Summary', () => {
  beforeEach(() => {
    MockSelectElementMetadataForPEM.mockReturnValue({} as any);
  });

  afterEach(() => {
    MockSelectElementMetadataForPEM.mockReset();
  });

  it('Should render a grid with no content', () => {
    const props = {
      formValues: {}
    } as any;

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    expect(body?.children.length).toEqual(undefined);
  });

  it('Should render a grid with content', () => {
    const props = {
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWMETHOD'
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'MODELEDMETHOD'
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWVERSION'
          }
        ]
      }
    } as any;

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    expect(body?.children.length).toEqual(3);
  });

  it('Should render a grid with pagination', () => {
    const props = {
      formValues: {
        methods: generateMockData(11)
      }
    };

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    expect(body?.children.length).toEqual(10);
  });

  it('Should call edit function', () => {
    const mockFn = jest.fn();
    const props = {
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWMETHOD'
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'MODELEDMETHOD'
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWVERSION'
          }
        ]
      },
      onEditStep: mockFn
    } as any;

    const wrapper = renderComponent(props);
    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(mockFn).toHaveBeenCalled();
  });

  it('Should show sub row', () => {
    const mockFn = jest.fn();
    const props = {
      stepId: '',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.DLO,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ]
      },
      onEditStep: mockFn
    } as any;

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    const firstChild = body?.children[0].querySelector(
      'button[class="btn btn-icon-secondary btn-sm"]'
    );
    fireEvent.click(firstChild as Element);
    expect(
      queryByText(wrapper.container, /SubRowGrid_component/i)
    ).toBeInTheDocument();
    fireEvent.click(firstChild as Element);
    expect(
      queryByText(wrapper.container, /SubRowGrid_component/i)
    ).not.toBeInTheDocument();
  });
});
