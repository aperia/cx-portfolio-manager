import {
  fireEvent,
  queryByText,
  render,
  RenderResult
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import {
  MethodFieldParameterEnum,
  ServiceSubjectSection
} from 'app/constants/enums';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManagePayoffExceptionMethods';
import React from 'react';
import * as reactRedux from 'react-redux';
import LateChargesStep from './index';

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');
const MockSelectElementMetadataForLC = jest.spyOn(
  WorkflowSetup,
  'useSelectElementMetadataForPayoffMethods'
);

const mockOnCloseWithoutMethod = jest.fn();
jest.mock('./MethodListModal', () => ({
  __esModule: true,
  default: ({ onClose }: any) => {
    const handleOnClose = () => {
      if (mockOnCloseWithoutMethod()) {
        onClose();
        return;
      }

      onClose({
        id: '1',
        name: 'method-1',
        versions: [
          {
            id: 'version 1.0'
          }
        ]
      });
    };

    return <div onClick={handleOnClose}>MethodListModal_component</div>;
  }
}));

jest.mock('./AddNewMethodModal', () => ({
  __esModule: true,
  default: ({ onClose }: any) => {
    const handleOnClose = () => {
      if (mockOnCloseWithoutMethod()) {
        onClose();
        return;
      }

      onClose(
        {
          id: '1',
          name: 'method-1',
          versions: [
            {
              id: 'version 1.0'
            }
          ]
        },
        true
      );
    };
    return <div onClick={handleOnClose}>AddNewMethodModal_component</div>;
  }
}));

jest.mock('./RemoveMethodModal', () => ({
  __esModule: true,
  default: ({ onClose }: any) => {
    const handleOnClose = () => {
      if (mockOnCloseWithoutMethod()) {
        onClose();
        return;
      }

      onClose({
        id: '1',
        name: 'method-1',
        versions: [
          {
            id: 'version 1.0'
          }
        ]
      });
    };
    return <div onClick={handleOnClose}>RemoveMethodModal_component</div>;
  }
}));

jest.mock('./SubRowGrid', () => ({
  __esModule: true,
  default: () => <div>SubRowGrid_component</div>
}));

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <LateChargesStep {...props} />
    </div>
  );
};

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

describe('PayoffExceptionMethods > ConfigureParameters > Index', () => {
  const mockDispatch = jest.fn();
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
    MockSelectElementMetadataForLC.mockReturnValue({
      [MethodFieldParameterEnum.PayoffExceptionCashOptionPromotionCode]: [
        { code: '0', text: 'None' },
        { code: '1', text: 'Test' }
      ],
      [MethodFieldParameterEnum.PayoffExceptionCashOptionPromotionCode]: [
        { code: '0', text: 'None' },
        { code: '1', text: 'Test' }
      ],
      [MethodFieldParameterEnum.PayoffExceptionCheckExceptionsDaysAfterPaymentDue]:
        [
          { code: '0', text: 'None' },
          { code: '1', text: 'Test' }
        ],
      [MethodFieldParameterEnum.PayoffExceptionPartialGraceSettings]: [
        { code: '0', text: 'None' },
        { code: '1', text: 'Test' }
      ],
      [MethodFieldParameterEnum.PayoffExceptionAmountToAvoidFinanceChargesSetting]:
        [
          { code: '0', text: 'None' },
          { code: '1', text: 'Test' }
        ],
      [MethodFieldParameterEnum.PayoffExceptionOldCash]: [
        { code: '0', text: 'None' },
        { code: '1', text: 'Test' }
      ],
      [MethodFieldParameterEnum.PayoffExceptionCycleToDateCash]: [
        { code: '0', text: 'None' },
        { code: '1', text: 'Test' }
      ],
      [MethodFieldParameterEnum.PayoffExceptionTwoCycleOldMerchandise]: [
        { code: '0', text: 'None' },
        { code: '1', text: 'Test' }
      ],
      [MethodFieldParameterEnum.PayoffExceptionOneCycleOldMerchandise]: [
        { code: '0', text: 'None' },
        { code: '1', text: 'Test' }
      ],
      [MethodFieldParameterEnum.PayoffExceptionCycleToDateMerchandise]: [
        { code: '0', text: 'None' },
        { code: '1', text: 'Test' }
      ]
    });

    mockOnCloseWithoutMethod.mockReturnValue(false);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Should render with no method > Create new version', () => {
    const props = {
      stepId: '1',
      selectedStep: 'selectedStep',
      formValues: {},
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_select_an_option')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    );
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('MethodListModal_component'));
    expect(
      queryByText(wrapper.container, /MethodListModal_component/i)
    ).not.toBeInTheDocument();

    mockOnCloseWithoutMethod.mockReturnValue(true);
    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    );
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('MethodListModal_component'));
    expect(
      queryByText(wrapper.container, /MethodListModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with no method > Choose Method to Model', () => {
    const props = {
      stepId: '1',
      selectedStep: 'selectedStep',
      formValues: {},
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_select_an_option')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_choose_method_to_model')
    );
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('MethodListModal_component'));
    expect(
      queryByText(wrapper.container, /MethodListModal_component/i)
    ).not.toBeInTheDocument();

    mockOnCloseWithoutMethod.mockReturnValue(true);
    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_choose_method_to_model')
    );

    fireEvent.click(wrapper.getByText('MethodListModal_component'));
    expect(
      queryByText(wrapper.container, /MethodListModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with no method > Add New Method', () => {
    const props = {
      stepId: '1',
      selectedStep: 'selectedStep',
      formValues: {},
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_select_an_option')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_method')
    );
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();

    mockOnCloseWithoutMethod.mockReturnValue(true);
    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_method')
    );
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with PayoffExceptionMethods > Edit method', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    expect(body?.children.length).toEqual(3);

    //SHOW Add new method modal
    const editButton = body?.children[1].querySelector(
      'button[class="btn btn-outline-primary btn-sm"]'
    ) as Element;
    fireEvent.click(editButton);
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
    //End

    mockOnCloseWithoutMethod.mockReturnValue(true);
    fireEvent.click(editButton);
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with PayoffExceptionMethods > Add new method', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );

    //SHOW AddNewMethodModal
    const editButton = body?.children[0].querySelector(
      'button[class="btn btn-outline-primary btn-sm"]'
    ) as Element;
    fireEvent.click(editButton);
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
    //End

    mockOnCloseWithoutMethod.mockReturnValue(true);
    fireEvent.click(editButton);
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with PayoffExceptionMethods > Remove method', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );

    //SHOW RemoveMethodModal
    const removeButton = body?.children[0].querySelector(
      'button[class="btn btn-outline-danger btn-sm ml-8"]'
    ) as Element;

    fireEvent.click(removeButton);
    expect(
      wrapper.getByText('RemoveMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('RemoveMethodModal_component'));
    expect(
      queryByText(wrapper.container, /RemoveMethodModal_component/i)
    ).not.toBeInTheDocument();
    //End

    mockOnCloseWithoutMethod.mockReturnValue(true);
    fireEvent.click(removeButton);
    expect(
      wrapper.getByText('RemoveMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('RemoveMethodModal_component'));
    expect(
      queryByText(wrapper.container, /RemoveMethodModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with PayoffExceptionMethods > click Add New Method on dropdown button', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const dropdownButton = wrapper.container.querySelector(
      'button.dls-dropdown-button'
    ) as Element;
    userEvent.click(dropdownButton);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_create_new_method')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_method')
    );
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with PayoffExceptionMethods > click Create New Version on dropdown button', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const dropdownButton = wrapper.container.querySelector(
      'button.dls-dropdown-button'
    ) as Element;
    userEvent.click(dropdownButton);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    );
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('MethodListModal_component'));
    expect(
      queryByText(wrapper.container, /MethodListModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with PayoffExceptionMethods > click Choose Method to Model on dropdown button', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const dropdownButton = wrapper.container.querySelector(
      'button.dls-dropdown-button'
    ) as Element;
    userEvent.click(dropdownButton);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_choose_method_to_model')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_choose_method_to_model')
    );
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('MethodListModal_component'));
    expect(
      queryByText(wrapper.container, /MethodListModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with PayoffExceptionMethods > Expand method', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    const firstChild = body?.children[0].querySelector(
      'button[class="btn btn-icon-secondary btn-sm"]'
    );
    fireEvent.click(firstChild as Element);
    expect(
      queryByText(wrapper.container, /SubRowGrid_component/i)
    ).toBeInTheDocument();
    fireEvent.click(firstChild as Element);
    expect(
      queryByText(wrapper.container, /SubRowGrid_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with PayoffExceptionMethods > Edit method to list with method', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    expect(body?.children.length).toEqual(3);

    //SHOW Add new method modal
    const editButton = body?.children[1].querySelector(
      'button[class="btn btn-outline-primary btn-sm"]'
    ) as Element;

    //WITHOUT METHOD
    fireEvent.click(editButton);
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
    //End
    mockOnCloseWithoutMethod.mockReturnValue(true);
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('MethodListModal_component'));
  });

  it('Should render with PayoffExceptionMethods > Edit method to list without method', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.LC,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    expect(body?.children.length).toEqual(3);

    //SHOW Add new method modal
    const editButton = body?.children[1].querySelector(
      'button[class="btn btn-outline-primary btn-sm"]'
    ) as Element;

    //WITH METHOD
    fireEvent.click(editButton);
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
    //End
    mockOnCloseWithoutMethod.mockReturnValue(false);
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('MethodListModal_component'));
  });
});
