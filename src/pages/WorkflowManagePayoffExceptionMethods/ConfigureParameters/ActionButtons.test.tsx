import { fireEvent, render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React from 'react';
import ActionButtons from './ActionButtons';

describe('ManagePayoffExceptionMethodsWorkflow > ConfigureParameters > ActionButtons', () => {
  it('Should render with small buttons', () => {
    const props = {
      small: true,
      title: 'example',
      onClickCreateNewVersion: jest.fn(),
      onClickMethodToModel: jest.fn(),
      onClickAddNewMethod: jest.fn()
    };
    const wrapper = render(<ActionButtons {...props} />);
    const dropdownButton = wrapper.container.querySelector(
      'button.dls-dropdown-button'
    ) as Element;
    expect(dropdownButton).not.toBeNull();

    userEvent.click(dropdownButton);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    ).toBeInTheDocument();
    expect(wrapper.getByText('example')).toBeInTheDocument();
    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    );
    expect(props.onClickCreateNewVersion).toHaveBeenCalled();

    userEvent.click(dropdownButton);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_choose_method_to_model')
    ).toBeInTheDocument();
    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_choose_method_to_model')
    );
    expect(props.onClickMethodToModel).toHaveBeenCalled();

    userEvent.click(dropdownButton);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_create_new_method')
    ).toBeInTheDocument();
    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_method')
    );
    expect(props.onClickAddNewMethod).toHaveBeenCalled();
  });

  it('Should render with large buttons', () => {
    const props = {
      onClickCreateNewVersion: jest.fn(),
      onClickMethodToModel: jest.fn(),
      onClickAddNewMethod: jest.fn()
    };
    const wrapper = render(<ActionButtons {...props} />);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_select_an_option')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    );
    expect(props.onClickCreateNewVersion).toHaveBeenCalled();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_choose_method_to_model')
    );
    expect(props.onClickMethodToModel).toHaveBeenCalled();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_method')
    );
    expect(props.onClickAddNewMethod).toHaveBeenCalled();
  });
});
