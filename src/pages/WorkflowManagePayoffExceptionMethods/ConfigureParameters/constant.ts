import { MethodFieldParameterEnum } from 'app/constants/enums';

export const ONLINE_PCF = {
  // Payoff Exception Usage
  PayoffExceptionUsageStatment: 'Statement Payoff Exception Value',
  PayoffExceptionCashOptionPromotionCode: 'Cash Option Promotion',
  PayoffExceptionMinimumBalanceToAssessInterestOn:
    'Assess Interest Minimum Balance',
  PayoffExceptionCheckExceptionsDaysAfterPaymentDue:
    'Check Exception Days After Pymt Due',
  PayoffExceptionPartialGraceSettings: 'Partial Grace',
  PayoffExceptionAmountToAvoidFinanceChargesSetting: 'Interest Free Code',
  PayoffExceptionOldCash: 'Old Cash',
  PayoffExceptionCycleToDateCash: 'Cycle-To-Date Cash',
  PayoffExceptionTwoCycleOldMerchandise: 'Two-Cycle-Old Merchandise',
  PayoffExceptionOneCycleOldMerchandise: 'One-Cycle-Old Merchandise',
  PayoffExceptionCycleToDateMerchandise: 'Cycle-To-Date Merchandise'
};

export const MORE_INFO = {
  // Payoff Exception Usage
  PayoffExceptionUsageStatment:
    'The Statement Value for Payoff Exception parameter is the dollar-and-cent amount used with a payoff exception value of 2, 4, 11, 16, or 17 as described under the Payoff Exception Options heading.',
  PayoffExceptionCashOptionPromotionCode:
    'The Cash Option Promotion Code parameter indicates whether cash option promotion purchases will be eligible for payoff exceptions.',
  PayoffExceptionMinimumBalanceToAssessInterestOn:
    'The Minimum Balance to Assess Interest On parameter identifies a balance below which finance charges will not be assessed.',
  PayoffExceptionCheckExceptionsDaysAfterPaymentDue:
    'The Check Exceptions Days After Payment Due parameter enables you to have the System check for payoff exceptions on payments that post after the payment due date. If the balance is paid in full, interest accrues until the posting date.',
  PayoffExceptionPartialGraceSettings: `The Partial Grace Settings parameter determines whether the System applies partial grace when the following statements about the principal balance and the account are all true.`,
  PayoffExceptionAmountToAvoidFinanceChargesSetting:
    'The Amount to Avoid Finance Charges Setting parameter determines which of the following options to use to avoid paying interest or other finance charges.',
  PayoffExceptionOptions:
    'Use the following parameters to control payoff exceptions for individual balances. Each parameter controls the payoff exceptions for a different principal balance. Each valid code corresponds to a payoff exception option.'
};

export const PAYOFF_DROPDOWN_FIELDS: string[] = [
  MethodFieldParameterEnum.PayoffExceptionCashOptionPromotionCode,
  MethodFieldParameterEnum.PayoffExceptionCheckExceptionsDaysAfterPaymentDue,
  MethodFieldParameterEnum.PayoffExceptionPartialGraceSettings,
  MethodFieldParameterEnum.PayoffExceptionAmountToAvoidFinanceChargesSetting,
  MethodFieldParameterEnum.PayoffExceptionOldCash,
  MethodFieldParameterEnum.PayoffExceptionCycleToDateCash,
  MethodFieldParameterEnum.PayoffExceptionTwoCycleOldMerchandise,
  MethodFieldParameterEnum.PayoffExceptionOneCycleOldMerchandise,
  MethodFieldParameterEnum.PayoffExceptionCycleToDateMerchandise
];

export const PAYOFF_NUMERIC_TEXTBOX_FIELDS: string[] = [
  MethodFieldParameterEnum.PayoffExceptionStatementValue,
  MethodFieldParameterEnum.PayoffExceptionMinimumBalanceToAssessInterestOn
];
