import FailedApiReload from 'app/components/FailedApiReload';
import MethodCardView from 'app/components/MethodCardView';
import ModalRegistry from 'app/components/ModalRegistry';
import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import WorkflowStrategyFlyout from 'app/components/WorkflowStrategyFlyout';
import { WORKFLOW_METHODS_SORT_BY_LIST_FOR_DESIGN_LOAN_OFFERS } from 'app/constants/constants';
import { ServiceSubjectSection } from 'app/constants/enums';
import { classnames } from 'app/helpers';
import {
  InlineMessage,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  TransDLS,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty, isFunction } from 'lodash';
import set from 'lodash.set';
import {
  actionsWorkflowSetup,
  useSelectExpandingPayoffExceptionMethod,
  useSelectStrategyFlyoutChangeInterestRatesSelector,
  useSelectWorkflowPayoffExceptionMethodsFilterSelector,
  useSelectWorkflowPayoffExceptionMethodsSelector
} from 'pages/_commons/redux/WorkflowSetup';
import { defaultWorkflowMethodsFilter } from 'pages/_commons/redux/WorkflowSetup/reducers';
import Paging from 'pages/_commons/Utils/Paging';
import SortOrder from 'pages/_commons/Utils/SortOrder';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';
import AddNewMethodModal from './AddNewMethodModal';

interface IMethodListModalProps {
  id: string;
  show: boolean;
  isCreateNewVersion?: boolean;
  methodNames: string[];
  defaultMethod?: WorkflowSetupMethod & { rowId?: number };
  onClose?: (method?: WorkflowSetupMethod & { rowId?: number }) => void;
}

interface IMethodModelWithIndex extends IMethodModel {
  index?: number;
}

const MethodListModal: React.FC<IMethodListModalProps> = ({
  id,
  show = false,
  isCreateNewVersion = false,
  defaultMethod,
  methodNames = [],
  onClose
}) => {
  const [draftMethod, setDraftMethod] =
    useState<(WorkflowSetupMethod & { rowId?: number }) | undefined>(undefined);
  const defaultVersionId =
    defaultMethod && defaultMethod!.modeledFrom?.versions[0]?.id;
  const defaultMethodId = defaultMethod && defaultMethod!.modeledFrom?.id;

  const [showCreateMethodModal, setShowCreateMethodModal] = useState(false);
  const [selectedModel, setSelectedModel] = useState<IMethodModelWithIndex>();
  const [selectedVersion, setSelectedVersion] = useState(defaultVersionId);
  const { methods, total, loading, error } =
    useSelectWorkflowPayoffExceptionMethodsSelector();
  const dataFilter = useSelectWorkflowPayoffExceptionMethodsFilterSelector();
  const { page, pageSize, searchValue } = dataFilter;
  const expandingMethod = useSelectExpandingPayoffExceptionMethod();

  const { t } = useTranslation();
  const dispatch = useDispatch();
  const simpleSearchRef = useRef<any>(null);

  const strategyFlyout = useSelectStrategyFlyoutChangeInterestRatesSelector();

  const [title, description, anouncement] = useMemo(() => {
    if (isCreateNewVersion) {
      return [
        t('txt_create_new_version'),
        <>
          <p className="color-grey">
            <TransDLS keyTranslation="txt_manage_payoff_exception_step_3_create_new_version_help_text">
              <strong className="color-grey-d20" />
            </TransDLS>
          </p>
          <p className="mt-8 color-grey">
            <TransDLS keyTranslation="txt_manage_payoff_exception_step_3_create_new_version_help_text_2">
              <strong className="color-grey-d20" />
            </TransDLS>
          </p>
        </>,
        `When you create a new Version of an existing Method, the new Version will impact all Pricing Strategies assigned to the Method, which changes the pricing for all customers that are assigned to this Pricing Strategy.`
      ];
    }

    return [
      t('txt_choose_method'),
      <>
        <p className="color-grey">
          <TransDLS keyTranslation="txt_manage_payoff_exception_methods_choose_method">
            <strong className="color-grey-d20" />
          </TransDLS>
        </p>
        <p className="mt-8 color-grey">
          <TransDLS keyTranslation="txt_manage_payoff_exception_step_3_create_new_version_help_text_2">
            <strong className="color-grey-d20" />
          </TransDLS>
        </p>
      </>
    ];
  }, [isCreateNewVersion, t]);

  const handleSearch = useCallback(
    (val: string) => {
      dispatch(
        actionsWorkflowSetup.updateWorkflowMethodFilter({
          page: 1,
          searchValue: val
        })
      );
    },
    [dispatch]
  );
  const handleToggle = useCallback(
    (methodId: string) => {
      dispatch(actionsWorkflowSetup.updateExpandingMethod(methodId));
    },
    [dispatch]
  );

  const handlePageChange = useCallback(
    (page: number) => {
      dispatch(actionsWorkflowSetup.updateWorkflowMethodFilter({ page }));
    },
    [dispatch]
  );

  const handlePageSizeChange = useCallback(
    (pageSize: number) => {
      dispatch(
        actionsWorkflowSetup.updateWorkflowMethodFilter({ pageSize, page: 1 })
      );
    },
    [dispatch]
  );

  const handleChangeOrderBy = useCallback(
    (orderBy: string) => {
      dispatch(
        actionsWorkflowSetup.updateWorkflowMethodFilter({
          orderBy: [orderBy as OrderByType]
        })
      );
    },
    [dispatch]
  );

  const handleResetSort = useCallback(() => {
    dispatch(
      actionsWorkflowSetup.updateWorkflowMethodFilter({
        sortBy: ['name'],
        orderBy: ['asc']
      })
    );
  }, [dispatch]);

  useEffect(() => {
    handleResetSort();
  }, [handleResetSort]);

  const handleClearAndReset = useCallback(() => {
    if (!selectedModel) {
      dispatch(actionsWorkflowSetup.clearAndResetWorkflowMethodFilter());
      return;
    }

    const nextPage = Math.ceil((selectedModel?.index || 1) / 10);
    dispatch(
      actionsWorkflowSetup.updateWorkflowMethodFilter({
        ...defaultWorkflowMethodsFilter,
        page: nextPage
      })
    );

    expandingMethod !== selectedModel.id &&
      dispatch(actionsWorkflowSetup.updateExpandingMethod(selectedModel.id));
  }, [selectedModel, dispatch, expandingMethod]);

  const handleApiReload = useCallback(() => {
    dispatch(
      actionsWorkflowSetup.getManagePayoffExceptionMethodsWorkflow({
        serviceSubjectSection: ServiceSubjectSection.PEM,
        defaultMethodId
      })
    );
  }, [dispatch, defaultMethodId]);

  useEffect(() => {
    !searchValue && simpleSearchRef?.current?.clear();
  }, [searchValue]);

  useEffect(() => {
    dispatch(
      actionsWorkflowSetup.getManagePayoffExceptionMethodsWorkflow({
        serviceSubjectSection: ServiceSubjectSection.PEM,
        defaultMethodId: defaultMethodId
      })
    );
    return () => {
      dispatch(
        actionsWorkflowSetup.updateWorkflowMethodFilter({
          ...defaultWorkflowMethodsFilter
        })
      );
    };
  }, [dispatch, defaultMethodId]);

  const isSelectedBeforeVersion = useMemo(
    () =>
      (!isEmpty(defaultMethod) && selectedVersion === defaultVersionId) ||
      (!isEmpty(draftMethod) &&
        selectedVersion === draftMethod?.modeledFrom?.versions[0]?.id),
    [defaultMethod, selectedVersion, draftMethod, defaultVersionId]
  );

  const methodList = useMemo(
    () => (
      <div>
        {!loading && !error && total > 0 && (
          <React.Fragment>
            <div className="mt-16 mx-n12">
              {methods.map((method: any) => (
                <MethodCardView
                  id={method.id}
                  key={method.id}
                  value={method}
                  serviceSubjectSection="PEM"
                  isCreateNewVersion={isCreateNewVersion}
                  selectedVersion={selectedVersion}
                  onSelectVersion={(model, versionId) => {
                    setSelectedModel(model);
                    setSelectedVersion(versionId);
                  }}
                  isExpand={expandingMethod === method.id}
                  onToggle={handleToggle}
                  isHideCommentArea={true}
                />
              ))}
            </div>
            <div className="mt-8">
              <Paging
                page={page}
                pageSize={pageSize}
                totalItem={total}
                onChangePage={handlePageChange}
                onChangePageSize={handlePageSizeChange}
              />
            </div>
          </React.Fragment>
        )}
      </div>
    ),
    [
      error,
      expandingMethod,
      handlePageChange,
      handlePageSizeChange,
      handleToggle,
      isCreateNewVersion,
      total,
      page,
      pageSize,
      loading,
      methods,
      selectedVersion
    ]
  );

  const noDataFound = useMemo(
    () =>
      !loading &&
      !error &&
      total === 0 && (
        <div className="d-flex flex-column justify-content-center mt-80">
          <NoDataFound
            id="method-list-NoDataFound"
            title={t('No methods to display')}
            hasSearch={!!searchValue}
            linkTitle={!!searchValue && t('txt_clear_and_reset')}
            onLinkClicked={handleClearAndReset}
          />
        </div>
      ),
    [handleClearAndReset, t, loading, error, total, searchValue]
  );
  const failedReload = useMemo(
    () => (
      <div>
        {error && (
          <FailedApiReload
            id="method-list-error"
            onReload={handleApiReload}
            className="mt-40 pt-40 d-flex flex-column justify-content-center align-items-center"
          />
        )}
      </div>
    ),
    [error, handleApiReload]
  );

  const methodSelected = useMemo<WorkflowSetupMethod>(() => {
    const version = selectedModel?.versions.find(v => v.id === selectedVersion);
    const model = {
      id: '',
      name: isCreateNewVersion ? selectedModel?.name : '',
      comment: isCreateNewVersion ? version?.comment : '',
      methodType: isCreateNewVersion ? 'NEWVERSION' : 'MODELEDMETHOD',
      parameters: version?.parameters,
      modeledFrom: {
        id: selectedModel?.id,
        name: selectedModel?.name,
        versions: [{ id: version?.id }],
        index: selectedModel?.index
      }
    } as WorkflowSetupMethod;
    return model;
  }, [isCreateNewVersion, selectedModel, selectedVersion]);
  const handleCloseAndAdd = (
    method: WorkflowSetupMethod & { rowId?: number }
  ) => {
    isFunction(onClose) && onClose(method);
  };
  const handleClose = () => {
    isFunction(onClose) && onClose();
  };
  const handleStrategyFlyoutClosed = useCallback(() => {
    dispatch(actionsWorkflowSetup.setStrategyFlyout(null));
  }, [dispatch]);

  const sortByList = WORKFLOW_METHODS_SORT_BY_LIST_FOR_DESIGN_LOAN_OFFERS;

  return (
    <React.Fragment>
      <ModalRegistry id={id} show={show} lg onAutoClosedAll={handleClose}>
        <ModalHeader border closeButton onHide={handleClose}>
          <ModalTitle>{title}</ModalTitle>
        </ModalHeader>
        <ModalBody
          className={classnames('px-24 pt-24 pb-24 pb-lg-0', {
            loading
          })}
        >
          {anouncement && (
            <InlineMessage variant="info" withIcon className="mb-24">
              {anouncement}
            </InlineMessage>
          )}
          <div className="color-grey">{description}</div>
          <div className="mt-24">
            <div className="d-flex justify-content-between align-items-center">
              <h5>{t('txt_method_list')}</h5>
              {(total > 0 || searchValue) && (
                <div className="method-list-simple-search">
                  <SimpleSearch
                    defaultValue={searchValue}
                    ref={simpleSearchRef}
                    clearTooltip={t('txt_clear_search_criteria')}
                    onSearch={handleSearch}
                    placeholder={t(
                      'txt_workflow_design_promotions_search_title'
                    )}
                  />
                </div>
              )}
            </div>
            {total > 0 && (
              <div className="mt-16 d-flex justify-content-end align-items-center">
                <SortOrder
                  hasFilter={!!searchValue}
                  dataFilter={dataFilter}
                  defaultDataFilter={defaultWorkflowMethodsFilter}
                  sortByFields={sortByList}
                  onChangeOrderBy={handleChangeOrderBy}
                  onClearSearch={handleClearAndReset}
                  isHideSort={true}
                />
              </div>
            )}
          </div>
          {methodList}
          {noDataFound}
          {failedReload}
        </ModalBody>

        <ModalFooter
          cancelButtonText={t('txt_cancel')}
          okButtonText={t('txt_continue')}
          onCancel={handleClose}
          onOk={() => setShowCreateMethodModal(true)}
          disabledOk={!selectedVersion}
        />
      </ModalRegistry>
      {showCreateMethodModal && (
        <AddNewMethodModal
          id={
            isCreateNewVersion
              ? 'addNewMethod_createNewVersion'
              : 'addNewMethod_cloneFromModel'
          }
          show
          methodNames={methodNames}
          draftMethod={isSelectedBeforeVersion ? draftMethod : undefined}
          method={
            isSelectedBeforeVersion && defaultMethod
              ? defaultMethod
              : methodSelected
          }
          onClose={(method, isBack) => {
            setShowCreateMethodModal(false);
            if (!method) {
              !isBack && handleClose();
              return;
            }

            if (isBack) {
              draftMethod && set(method, 'rowId', draftMethod.rowId);
              setDraftMethod(method);
              return;
            }

            defaultMethod && set(method, 'rowId', defaultMethod.rowId);
            handleCloseAndAdd(method);
            return;
          }}
        />
      )}
      {strategyFlyout !== null && (
        <WorkflowStrategyFlyout
          {...strategyFlyout}
          onClose={handleStrategyFlyoutClosed}
        />
      )}
    </React.Fragment>
  );
};

export default MethodListModal;
