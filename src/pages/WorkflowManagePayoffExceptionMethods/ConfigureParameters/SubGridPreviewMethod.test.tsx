import { render } from '@testing-library/react';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import React from 'react';
import SubGridPreviewMethod from './SubGridPreviewMethod';

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

const metadata = {
  [MethodFieldParameterEnum.PayoffExceptionCashOptionPromotionCode]: '0',
  [MethodFieldParameterEnum.PayoffExceptionStatementValue]: '12'
};

describe('pages > PayoffExceptionMethods > PayoffExceptionMethodsStep > SubGridPreviewMethod', () => {
  it('Info_Payoff_Exception_Usage', () => {
    const original = {
      id: 'Info_Payoff_Exception_Usage',
      section: 'txt_manage_payoff_exception_parameters',
      parameterGroup: 'Payoff Exception Usage'
    };
    const props = { metadata, original };
    const wrapper = render(<SubGridPreviewMethod {...props} />);
    expect(
      wrapper.getByText('Check Exceptions Days After Payment Due')
    ).toBeInTheDocument();
  });
});
