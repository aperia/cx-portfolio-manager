import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { stepRegistry } from '../_commons/WorkflowSetup/registries';
import ConfigureParameters from './ConfigureParameters';
import GettingStartStep from './GettingStartStep';

stepRegistry.registerStep(
  'GetStartedManagePayoffExceptionMethods',
  GettingStartStep,
  []
);

stepRegistry.registerStep('ConfigureParameters', ConfigureParameters, [
  UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_PAYOFF_EXCEPTION_METHODS__CONFIGURE_PARAMETERS
]);
