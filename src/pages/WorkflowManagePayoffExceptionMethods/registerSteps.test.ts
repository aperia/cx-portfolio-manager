import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { default as stepRegistry } from '../_commons/WorkflowSetup/registries/stepRegistry';

const registerFunc = jest.spyOn(stepRegistry, 'registerStep');

describe('ManagePayoffExceptionMethodsWorkflow > registerSteps', () => {
  it('Should register components', async () => {
    await import('./registerSteps');

    expect(registerFunc).toBeCalledWith(
      'GetStartedManagePayoffExceptionMethods',
      expect.anything(),
      []
    );

    expect(registerFunc).toBeCalledWith(
      'ConfigureParameters',
      expect.anything(),
      [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_PAYOFF_EXCEPTION_METHODS__CONFIGURE_PARAMETERS
      ]
    );
  });
});
