import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { uploadFileStepRegistry } from 'pages/_commons/WorkflowSetup/registries';
import { default as stepRegistry } from '../_commons/WorkflowSetup/registries/stepRegistry';

const registerFunc = jest.spyOn(stepRegistry, 'registerStep');
const registerCom = jest.spyOn(uploadFileStepRegistry, 'registerComponent');

describe('WorkflowBulkSuspendFraudStrategy > registerSteps', () => {
  it('Should register components', async () => {
    await import('./registerSteps');

    expect(registerFunc).toBeCalledWith(
      'GetStartedBulkSuspendFraudStrategy',
      expect.anything()
    );

    expect(registerCom).toBeCalledWith(
      'SubContentBulkSuspendFraudStrategy',
      expect.anything()
    );

    expect(registerFunc).toBeCalledWith(
      'ConfigureParametersBulkFraud',
      expect.anything(),
      [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__BULK_SUSPEND_FRAUD_STRATEGY__CONFIGURE_PARAMETERS
      ]
    );
  });
});
