import {
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { useUnsavedChangeRegistry } from 'app/hooks';
import { DateRangePickerValue } from 'app/_libraries/_dls';
import { isEqual } from 'lodash';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, { useEffect, useState } from 'react';
import ParameterGrid from './ParameterGrid';
import parseFormValues from './parseFormValues';
import ConfigureParametersSummary from './Summary';

export interface ConfigureParametersFormValue {
  isValid?: boolean;
  parameters?: {
    dateRange?: DateRangePickerValue;
    strategy?: string;
  };
}

const ConfigureParameters: React.FC<
  WorkflowSetupProps<ConfigureParametersFormValue>
> = props => {
  const { formValues, savedAt } = props;
  const [preFormValues, setPreFormValues] = useState(formValues);

  useEffect(() => {
    setPreFormValues(formValues);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [savedAt]);

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__BULK_SUSPEND_FRAUD_STRATEGY__CONFIGURE_PARAMETERS,
      priority: 1
    },
    [
      !isEqual(
        preFormValues?.parameters?.dateRange,
        formValues?.parameters?.dateRange
      ),
      !isEqual(
        preFormValues?.parameters?.strategy,
        formValues?.parameters?.strategy
      )
    ]
  );

  return <ParameterGrid {...props} />;
};

const ExtraStaticConfigureParametersStep =
  ConfigureParameters as WorkflowSetupStaticProp<ConfigureParametersFormValue>;

ExtraStaticConfigureParametersStep.summaryComponent =
  ConfigureParametersSummary;
ExtraStaticConfigureParametersStep.defaultValues = {
  isValid: true,
  parameters: {}
};

ExtraStaticConfigureParametersStep.parseFormValues = parseFormValues;

export default ExtraStaticConfigureParametersStep;
