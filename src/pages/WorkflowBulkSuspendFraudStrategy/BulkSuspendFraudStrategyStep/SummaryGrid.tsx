import { FormatTime, FraudFieldParameterEnum } from 'app/constants/enums';
import { formatTimeDefault } from 'app/helpers';
import { Grid, TruncateText, useTranslation } from 'app/_libraries/_dls';
import isUndefined from 'lodash.isundefined';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React from 'react';
import { ConfigureParametersFormValue } from '.';
import { FraudParameterMAS } from './parameterList';

const SummaryGrid: React.FC<
  WorkflowSetupSummaryProps<ConfigureParametersFormValue>
> = ({ formValues }) => {
  const { t } = useTranslation();

  const valueAccessor = (row: MagicKeyValue) => {
    if (row?.id === FraudFieldParameterEnum.Strategy) {
      return (
        <TruncateText
          resizable
          lines={2}
          ellipsisLessText={t('txt_less')}
          ellipsisMoreText={t('txt_more')}
        >
          {formValues?.parameters?.strategy}
        </TruncateText>
      );
    }

    if (row?.id === FraudFieldParameterEnum.DateRange) {
      const isRangeDate =
        isUndefined(formValues?.parameters?.dateRange?.end) &&
        isUndefined(formValues?.parameters?.dateRange?.start);
      const val = isRangeDate
        ? ''
        : !isUndefined(formValues?.parameters?.dateRange?.end)
        ? `${formatTimeDefault(
            new Date(
              formValues?.parameters?.dateRange?.start as any
            ).toString(),
            FormatTime.Date
          )} - ${formatTimeDefault(
            new Date(formValues?.parameters?.dateRange?.end as any).toString(),
            FormatTime.Date
          )}`
        : `${formatTimeDefault(
            new Date(
              formValues?.parameters?.dateRange?.start as any
            ).toString(),
            FormatTime.Date
          )}`;
      return (
        <TruncateText
          resizable
          lines={2}
          ellipsisLessText={t('txt_less')}
          ellipsisMoreText={t('txt_more')}
        >
          {val}
        </TruncateText>
      );
    }
  };

  const columns = [
    {
      id: 'fieldName',
      Header: t('txt_business_name'),
      accessor: 'fieldName',
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'greenScreenName',
      Header: t('txt_green_screen_name'),
      accessor: 'greenScreenName',
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'value',
      Header: t('txt_value'),
      accessor: valueAccessor,
      cellBodyProps: { className: 'py-8' }
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      accessor: viewMoreInfo(['moreInfo'], t),
      width: 105,
      cellBodyProps: { className: 'pt-12 pb-8' }
    }
  ];

  return (
    <div>
      <Grid columns={columns} data={FraudParameterMAS} />
    </div>
  );
};

export default SummaryGrid;
