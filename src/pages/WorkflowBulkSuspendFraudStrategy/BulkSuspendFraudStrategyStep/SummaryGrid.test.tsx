import { render, RenderResult } from '@testing-library/react';
import { mockUseDispatchFnc } from 'app/utils';
import * as workflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowBulkSuspendFraudStrategy';
import React from 'react';
import SummaryGrid from './SummaryGrid';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const mockUseDispatch = mockUseDispatchFnc();

const mockUseSelectElementMetadataBulkFraud = jest.spyOn(
  workflowSetup,
  'useSelectElementMetadataBulkFraud'
);

const renderComponent = (props: any): RenderResult => {
  mockUseSelectElementMetadataBulkFraud.mockReturnValue({
    transaction: [{ text: '', code: '' }]
  });
  return render(
    <div>
      <SummaryGrid {...props} />
    </div>
  );
};

describe('WorkflowBulkSuspendFraudStrategy > SummaryGrid', () => {
  const mockDispatch = jest.fn();
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
    mockUseSelectElementMetadataBulkFraud.mockReturnValue({} as any);
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
    mockUseSelectElementMetadataBulkFraud.mockReset();
  });
  it('Should render SummaryGrid contains Completed', () => {
    const wrapper = renderComponent({
      formValues: {}
    });

    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument;
  });
});
