import {
  FraudFieldNameEnum,
  FraudFieldParameterEnum
} from 'app/constants/enums';
import { FraudParameterMAS } from './parameterList';

describe('pages > WorkflowManageAccountSerialization > ManageAccountSerializationStep ', () => {
  it('> recordParameterMAS', () => {
    expect(FraudParameterMAS).toEqual([
      {
        id: FraudFieldParameterEnum.Strategy,
        fieldName: FraudFieldNameEnum.Strategy,
        greenScreenName: 'Strategy',
        moreInfoText:
          'The Strategy represents whether you want to restrict this account from fraud authorization decision processing or send this account to a new strategy until the date specified in the END field',
        moreInfo:
          'The Strategy represents whether you want to restrict this account from fraud authorization decision processing or send this account to a new strategy until the date specified in the END field'
      },
      {
        id: FraudFieldParameterEnum.DateRange,
        fieldName: FraudFieldNameEnum.DateRange,
        greenScreenName: 'Start - End',
        moreInfoText:
          'The Date Range parameter defines the period you want the System to begin and end the suspension of normal fraud authorization decision processing or send this account to a new strategy.',
        moreInfo:
          'The Date Range parameter defines the period you want the System to begin and end the suspension of normal fraud authorization decision processing or send this account to a new strategy.'
      }
    ]);
  });
});
