import {
  FraudFieldNameEnum,
  FraudFieldParameterEnum
} from 'app/constants/enums';
import React from 'react';

export interface FraudParameterMAS {
  id: FraudFieldParameterEnum;
  fieldName: FraudFieldNameEnum;
  greenScreenName: string;
  moreInfoText: string;
  moreInfo: React.ReactNode;
}

export const FraudParameterMAS: FraudParameterMAS[] = [
  {
    id: FraudFieldParameterEnum.Strategy,
    fieldName: FraudFieldNameEnum.Strategy,
    greenScreenName: 'Strategy',
    moreInfoText:
      'The Strategy represents whether you want to restrict this account from fraud authorization decision processing or send this account to a new strategy until the date specified in the END field',
    moreInfo:
      'The Strategy represents whether you want to restrict this account from fraud authorization decision processing or send this account to a new strategy until the date specified in the END field'
  },
  {
    id: FraudFieldParameterEnum.DateRange,
    fieldName: FraudFieldNameEnum.DateRange,
    greenScreenName: 'Start - End',
    moreInfoText:
      'The Date Range parameter defines the period you want the System to begin and end the suspension of normal fraud authorization decision processing or send this account to a new strategy.',
    moreInfo:
      'The Date Range parameter defines the period you want the System to begin and end the suspension of normal fraud authorization decision processing or send this account to a new strategy.'
  }
];
