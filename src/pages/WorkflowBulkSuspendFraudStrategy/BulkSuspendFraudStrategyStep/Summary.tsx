import { Button, useTranslation } from 'app/_libraries/_dls';
import { isFunction } from 'app/_libraries/_dls/lodash';
import React from 'react';
import { ConfigureParametersFormValue } from '.';
import SummaryGrid from './SummaryGrid';

const Summary: React.FC<
  WorkflowSetupSummaryProps<ConfigureParametersFormValue>
> = props => {
  const { t } = useTranslation();

  const { onEditStep } = props;

  const handleEdit = () => {
    isFunction(onEditStep) && onEditStep();
  };

  return (
    <div className="position-relative">
      <div className="absolute-top-right mt-n26 mr-n8">
        <Button variant="outline-primary" size="sm" onClick={handleEdit}>
          {t('txt_edit')}
        </Button>
      </div>
      <div className="pt-16">
        <SummaryGrid {...props} />
      </div>
    </div>
  );
};

export default Summary;
