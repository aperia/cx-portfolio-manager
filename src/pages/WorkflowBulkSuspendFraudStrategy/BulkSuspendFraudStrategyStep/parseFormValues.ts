import { getWorkflowSetupStepStatus, isValidDate } from 'app/helpers';
import { ConfigureParametersFormValue } from '.';

const parseFormValues: WorkflowSetupStepFormDataFunc<ConfigureParametersFormValue> =
  (data, id) => {
    const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);
    if (!stepInfo) return;

    const config = data?.data?.configurations as any;

    const parameters = {
      dateRange: {
        start: isValidDate(config?.date?.minDate)
          ? new Date(config?.date?.minDate)
          : undefined,
        end: isValidDate(config?.date?.maxDate)
          ? new Date(config?.date?.maxDate)
          : undefined
      },
      strategy: config?.parameters?.find((i: any) => i.name === 'strategy')
        ?.value
    };

    return {
      ...getWorkflowSetupStepStatus(stepInfo),
      isValid: true,
      parameters
    };
  };

export default parseFormValues;
