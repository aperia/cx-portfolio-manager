import { RenderResult } from '@testing-library/react';
import { renderWithMockStore } from 'app/utils';
import React from 'react';
import ConfigureParametersStep from '.';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

jest.mock('./ParameterGrid', () => {
  return {
    __esModule: true,
    default: () => <div>ConfigureParametersForm</div>
  };
});

const renderComponent = (props: any): RenderResult => {
  return renderWithMockStore(
    <div>
      <ConfigureParametersStep {...props} />
    </div>
  );
};

describe('WorkflowBulkSuspendFraudStrategy > ConfigureParametersStep', () => {
  it('Should render ConfigureParametersStep contains Completed', async () => {
    const wrapper = await renderComponent({
      formValues: {
        checked: 'r1'
      }
    });

    expect(wrapper.getByText('ConfigureParametersForm')).toBeInTheDocument;
  });
});
