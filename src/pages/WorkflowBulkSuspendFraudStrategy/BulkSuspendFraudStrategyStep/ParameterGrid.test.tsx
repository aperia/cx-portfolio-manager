import { fireEvent } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';
import { WorkflowMetadataOverride } from 'app/fixtures/workflow-metadata';
import { mockUseDispatchFnc, renderWithMockStore } from 'app/utils';
import 'app/utils/_mockComponent/mockModalRegistry';
import 'app/_libraries/_dls/test-utils/mocks/mockCanvas';
import React from 'react';
import ParameterGrid from './ParameterGrid';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

jest.mock('app/_libraries/_dls/components/DateRangePicker', () => {
  const actualModule = jest.requireActual(
    'app/_libraries/_dls/components/DateRangePicker'
  );
  return {
    ...actualModule,
    __esModule: true,
    default: ({ onChange }: any) => (
      <div onClick={onChange}>DateRangePicker</div>
    )
  };
});

const mockUseDispatch = mockUseDispatchFnc();

const getInitialState = ({
  elements = WorkflowMetadataOverride
}: any = {}) => ({
  workflowSetup: { elementMetadata: { elements } }
});

const renderComponent = (props: any) => {
  return renderWithMockStore(<ParameterGrid {...props} />, {
    initialState: getInitialState()
  });
};

describe('WorkflowBulkSuspendFraudStrategy > SummaryGrid', () => {
  const mockDispatch = jest.fn();
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
  });
  it('Should render SummaryGrid contains Completed', async () => {
    const wrapper = await renderComponent({
      formValues: {}
    });

    expect(wrapper.getByText('txt_bulk_suspend_fraud_strategy_search_title'))
      .toBeInTheDocument;
  });

  it('Should render search bar', async () => {
    const wrapper = await renderComponent({
      formValues: {},
      setFormValues: jest.fn()
    });

    const dateRange = wrapper.getAllByText('DateRangePicker');
    userEvent.click(dateRange[0]);
    userEvent.type(dateRange[0], '123 {enter}');
    expect(wrapper.getByText('txt_bulk_suspend_fraud_strategy_search_title'))
      .toBeInTheDocument;
  });

  it('Should render search bar', async () => {
    const wrapper = await renderComponent({
      formValues: {}
    });

    const searchBar = wrapper.getByPlaceholderText('txt_type_to_search');
    userEvent.type(searchBar, '123 {enter}');
    const resetButton = wrapper.getByText('txt_clear_and_reset');
    userEvent.click(resetButton);
    expect(wrapper.getByText('txt_bulk_suspend_fraud_strategy_search_title'))
      .toBeInTheDocument;
  });

  it('Should render text box', async () => {
    const wrapper = await renderComponent({
      formValues: {
        parameters: {
          strategy: 'a'
        }
      },
      setFormValues: jest.fn(),
      stepId: '2',
      selectedStep: ''
    });

    const textBox = wrapper.getByPlaceholderText('txt_enter_a_value');
    userEvent.type(textBox, 'qwe');
    fireEvent.blur(textBox);
    userEvent.type(textBox, 'mmm');
    fireEvent.blur(textBox);

    expect(wrapper.getByText('txt_bulk_suspend_fraud_strategy_search_title'))
      .toBeInTheDocument;
  });

  it('Should render search text box 2', async () => {
    const props = {
      formValues: {
        parameters: {
          strategy: '1'
        }
      },
      setFormValues: jest.fn(),
      stepId: '2',
      selectedStep: ''
    };

    const { getByPlaceholderText, rerender } = await renderComponent({
      ...props
    });

    const textBox = getByPlaceholderText('txt_enter_a_value');
    userEvent.type(textBox, 'abc');
    fireEvent.blur(textBox);
    userEvent.type(textBox, '1234');
    fireEvent.blur(textBox);

    rerender(
      <ParameterGrid
        {...props}
        formValues={{
          parameters: {
            strategy: '123'
          }
        }}
      />
    );
    const textBox2 = getByPlaceholderText('txt_enter_a_value');
    userEvent.type(textBox2, 'abc');
    fireEvent.blur(textBox2);
  });
});
