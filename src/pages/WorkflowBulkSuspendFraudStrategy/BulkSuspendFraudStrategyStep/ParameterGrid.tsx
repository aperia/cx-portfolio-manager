import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import { FraudFieldParameterEnum } from 'app/constants/enums';
import { matchSearchValue } from 'app/helpers';
import {
  DateRangePicker,
  Grid,
  TextBox,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import ClearAndResetButton from 'pages/_commons/Utils/ClearAndResetButton';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React, { useEffect, useMemo, useRef, useState } from 'react';
import { ConfigureParametersFormValue } from '.';
import { FraudParameterMAS } from './parameterList';

const ParameterGrid: React.FC<
  WorkflowSetupProps<ConfigureParametersFormValue>
> = ({ setFormValues, formValues, selectedStep, stepId }) => {
  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();
  const simpleSearchRef = useRef<any>(null);

  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;

  const [searchValue, setSearchValue] = useState('');
  const [isErrorStrategy, setIsErrorStrategy] = useState<boolean>(false);

  const handleSearch = (val: string) => setSearchValue(val);

  const handleClearFilter = () => setSearchValue('');

  const handleChange = (e: any, name: string) => {
    keepRef.current.setFormValues({
      parameters: { ...formValues.parameters, [name]: e?.target?.value }
    });
  };

  const handleBlur = () => {
    if (formValues?.parameters?.strategy?.length === 1) {
      setIsErrorStrategy(true);
      keepRef.current.setFormValues({
        ...formValues,
        isValid: false
      });
    }

    if (isErrorStrategy && !(formValues?.parameters?.strategy?.length === 1)) {
      keepRef.current.setFormValues({
        isValid: true
      });
      setIsErrorStrategy(false);
    }
  };

  useEffect(() => {
    if (!searchValue && simpleSearchRef?.current) {
      simpleSearchRef.current.clear();
    }
  }, [searchValue]);

  useEffect(() => {
    if (stepId !== selectedStep) {
      setSearchValue('');
    }
  }, [stepId, selectedStep]);

  const parameters = useMemo(
    () =>
      FraudParameterMAS.filter(g =>
        matchSearchValue(
          [g.fieldName, g.moreInfoText, g.greenScreenName],
          searchValue
        )
      ),
    [searchValue]
  );

  const valueAccessor = (data: MagicKeyValue) => {
    const paramId = data.id as FraudFieldParameterEnum;
    if (paramId === FraudFieldParameterEnum.Strategy) {
      return (
        <TextBox
          size="sm"
          id="fraud__strategy"
          placeholder={t('txt_enter_a_value')}
          value={formValues?.parameters?.strategy}
          autoFocus={false}
          error={{
            status: isErrorStrategy,
            message: t('txt_invalid_format')
          }}
          maxLength={2}
          onChange={e => handleChange(e, 'strategy')}
          onBlur={handleBlur}
        />
      );
    }
    if (paramId === FraudFieldParameterEnum.DateRange) {
      return (
        <DateRangePicker
          size="small"
          id="parameter_date_range__bulk_fraud"
          onChange={e => handleChange(e, 'dateRange')}
          value={formValues?.parameters?.dateRange}
        />
      );
    }
  };

  const columns = [
    {
      id: 'fieldName',
      Header: t('txt_business_name'),
      accessor: 'fieldName',
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'greenScreenName',
      Header: t('txt_green_screen_name'),
      accessor: 'greenScreenName',
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'value',
      Header: t('txt_value'),
      accessor: valueAccessor,
      cellBodyProps: { className: 'py-8' },
      width: width < 1280 ? 280 : undefined
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      accessor: viewMoreInfo(['moreInfo'], t),
      width: 105,
      cellBodyProps: { className: 'pt-12 pb-8' }
    }
  ];

  return (
    <>
      <div className="d-flex align-items-center justify-content-between mt-24 mb-16">
        <h5>{t('txt_bulk_suspend_fraud_strategy_search_title')}</h5>
        <SimpleSearch
          ref={simpleSearchRef}
          placeholder={t('txt_type_to_search')}
          onSearch={handleSearch}
          popperElement={
            <>
              <p className="color-grey">{t('txt_search_description')}</p>
              <ul className="search-field-item list-unstyled">
                <li className="mt-16">{t('txt_business_name')}</li>
                <li className="mt-16">{t('txt_green_screen_name')}</li>
                <li className="mt-16">{t('txt_more_info')}</li>
              </ul>
            </>
          }
        />
      </div>
      {searchValue && !isEmpty(parameters) && (
        <div className="d-flex justify-content-end mb-16 mr-n8">
          <ClearAndResetButton small onClearAndReset={handleClearFilter} />
        </div>
      )}

      {!isEmpty(parameters) && <Grid columns={columns} data={parameters} />}

      {isEmpty(parameters) && (
        <div className="d-flex flex-column justify-content-center mt-40 mb-32">
          <NoDataFound
            id="newRecord_notfound"
            hasSearch
            title={t('txt_no_results_found')}
            linkTitle={t('txt_clear_and_reset')}
            onLinkClicked={handleClearFilter}
          />
        </div>
      )}
    </>
  );
};

export default ParameterGrid;
