import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import {
  stepRegistry,
  uploadFileStepRegistry
} from '../_commons/WorkflowSetup/registries';
import ConfigureParameters from './BulkSuspendFraudStrategyStep';
import SubContent from './BulkSuspendFraudStrategyStep/SubContent';
import GettingStartStep from './GettingStartStep';

stepRegistry.registerStep(
  'GetStartedBulkSuspendFraudStrategy',
  GettingStartStep
);

uploadFileStepRegistry.registerComponent(
  'SubContentBulkSuspendFraudStrategy',
  SubContent
);

stepRegistry.registerStep('ConfigureParametersBulkFraud', ConfigureParameters, [
  UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__BULK_SUSPEND_FRAUD_STRATEGY__CONFIGURE_PARAMETERS
]);
