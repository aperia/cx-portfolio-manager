import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { stepRegistry } from '../_commons/WorkflowSetup/registries';
import GettingStartStep from './GettingStartStep';
import SelectNonMonetaryTransactionsStep from './SelectNonMonetaryTransactionsStep';

stepRegistry.registerStep(
  'GetStartedFlexibleNonMonetaryTransactionsWorkflow',
  GettingStartStep
);

stepRegistry.registerStep(
  'SelectNonMonetaryTransactionsWorkflow',
  SelectNonMonetaryTransactionsStep,
  [
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__FLEXIBLE_NON_MONETARY_TRANSACTIONS__MONETARY_TRANSACTIONS
  ]
);
