import { WORKFLOW_SETUP } from 'app/constants/local-storage';
import { Button, Icon, TransDLS, useTranslation } from 'app/_libraries/_dls';
import OverviewModal from 'pages/_commons/OverviewModal';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, { useRef, useState } from 'react';
import GettingStartSummary from './GettingStartSummary';

export interface FormGettingStart {
  isValid?: boolean;

  returnedCheckCharges?: boolean;
  lateCharges?: boolean;

  alreadyShown?: boolean;
}

const GettingStartStep: React.FC<WorkflowSetupProps<FormGettingStart>> = ({
  stepId,
  selectedStep,
  title,
  formValues,
  setFormValues
}) => {
  const { t } = useTranslation();
  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;

  const [showOverview, setShowOverview] = useState(
    formValues.alreadyShown
      ? false
      : localStorage.getItem(WORKFLOW_SETUP.SHOW_OVERVIEW_AGAIN) !== 'false'
  );

  return (
    <React.Fragment>
      <div className="position-relative">
        <div className="absolute-top-right mt-n28">
          <Button
            className="mr-n8"
            variant="outline-primary"
            size="sm"
            onClick={() => setShowOverview(true)}
          >
            {t('txt_view_overview')}
          </Button>
        </div>
        <div className="row">
          <div className="col-6 mt-24">
            <div className="bg-light-l20 rounded-lg p-16">
              <div className="text-center">
                <Icon name="request" size="12x" className="color-grey-l16" />
              </div>
              <p className="mt-8 color-grey">
                {t(
                  'txt_workflow_flexible_non_monetary_transactions__getting_start_1'
                )}
              </p>
              <p className="mt-16 fw-600">
                {t(
                  'txt_workflow_flexible_non_monetary_transactions__getting_start_2'
                )}
              </p>
              <p className="mt-8 color-grey">
                {t(
                  'txt_workflow_flexible_non_monetary_transactions__getting_start_3'
                )}{' '}
              </p>
              <ul className="list-dot color-grey">
                <li>
                  {t(
                    'txt_workflow_flexible_non_monetary_transactions__getting_start_3_1'
                  )}
                </li>
                <li>
                  {t(
                    'txt_workflow_flexible_non_monetary_transactions__getting_start_3_2'
                  )}
                </li>
                <li>
                  {t(
                    'txt_workflow_flexible_non_monetary_transactions__getting_start_3_3'
                  )}
                </li>
              </ul>
            </div>
          </div>
          <div className="col-6 mt-24 color-grey">
            <p>
              {t(
                'txt_workflow_flexible_non_monetary_transactions__getting_start_4'
              )}
            </p>
            <p className="mt-8">
              <TransDLS keyTranslation="txt_workflow_flexible_non_monetary_transactions__getting_start_4_1">
                <strong className="color-grey-d20" />
              </TransDLS>
            </p>
            <p className="mt-8">
              <TransDLS keyTranslation="txt_workflow_flexible_non_monetary_transactions__getting_start_4_2">
                <strong className="color-grey-d20" />
              </TransDLS>
            </p>
            <p className="mt-8">
              <TransDLS keyTranslation="txt_workflow_flexible_non_monetary_transactions__getting_start_4_3">
                <strong className="color-grey-d20" />
              </TransDLS>
            </p>
            <p className="mt-8">
              <TransDLS keyTranslation="txt_workflow_flexible_non_monetary_transactions__getting_start_4_4">
                <strong className="color-grey-d20" />
              </TransDLS>
            </p>
          </div>
        </div>
      </div>
      {showOverview && (
        <OverviewModal
          id="overviewWorkflowSetup"
          show
          onClose={() => {
            setShowOverview(false);
            keepRef.current.setFormValues({ alreadyShown: true });
          }}
        />
      )}
    </React.Fragment>
  );
};

const ExtraStaticGettingStartStep =
  GettingStartStep as WorkflowSetupStaticProp<FormGettingStart>;

ExtraStaticGettingStartStep.summaryComponent = GettingStartSummary;
ExtraStaticGettingStartStep.defaultValues = { isValid: true };

export default ExtraStaticGettingStartStep;
