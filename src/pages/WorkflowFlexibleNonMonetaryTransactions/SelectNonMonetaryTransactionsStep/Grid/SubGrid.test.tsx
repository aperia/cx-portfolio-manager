import { render, RenderResult } from '@testing-library/react';
import React from 'react';
import { SubGrid } from './SubGrid';

const t = (value: string) => {
  return value;
};

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({ t })
  };
});

const renderComponent = (props: any): RenderResult => {
  return render(<SubGrid {...props} />);
};

describe('WorkflowFlexibleNonMonetaryTransactions > SelectNonMonetaryTransactionsStep > Grid > SubGrid', () => {
  it('Should render with content', () => {
    const parameters = [
      { id: '1', value: 'name', description: 'description', type: 'type' },
      { id: '2', value: 'name2', description: 'description2', type: 'type2' }
    ];
    const props = { row: { original: { parameters } } };

    const wrapper = renderComponent(props);

    expect(wrapper.getByText('name')).toBeInTheDocument();
  });
});
