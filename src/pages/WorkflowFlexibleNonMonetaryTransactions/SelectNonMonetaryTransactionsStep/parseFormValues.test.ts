import parseFormValues from './parseFormValues';

describe('WorkflowFlexibleNonMonetaryTransactions > SelectNonMonetaryTransactionsStep > parseFormValues', () => {
  it('selected step', () => {
    const data = {
      transactions: ['1', '2']
    };
    const input: any = {
      data: {
        configurations: data,
        workflowSetupData: [
          {
            id: 'id',
            status: 'DONE'
          }
        ]
      }
    };
    const id = 'id';
    const result = parseFormValues(input, id);
    expect(result.transactionsProp).toEqual(data.transactions);
  });

  it('select other step', () => {
    const input: any = {
      data: {
        workflowSetupData: [
          {
            id: 'id',
            status: 'DONE'
          }
        ]
      }
    };
    const id = 'id1';
    const result = parseFormValues(input, id);
    expect(result).toBeUndefined();
  });
});
