export enum ParameterCode {
  TransactionId = 'transaction.id',

  TransactionCU956 = 'CU.956.transaction',
  TransactionNM102 = 'NM.102.transaction',
  TransactionNM146 = 'NM.146.transaction',
  TransactionNM150 = 'NM.150.transaction'
}
