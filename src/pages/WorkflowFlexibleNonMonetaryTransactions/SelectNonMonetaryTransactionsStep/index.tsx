import { InlineMessage, useTranslation } from 'app/_libraries/_dls';
import { StateFile } from 'app/_libraries/_dls/components/Upload/File';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React from 'react';
import Grid from './Grid';
import parseFormValues from './parseFormValues';
import stepValueFunc from './stepValueFunc';
import Summary from './Summary';

export interface Form {
  isValid?: boolean;
  transactions?: string[];
  transactionsProp?: string[];
  transactionsData?: MagicKeyValue[];
  listLength?: number;
}

export interface IDependencies {
  files?: StateFile[];
}

export interface SelectNonMonetaryTransactionsProps
  extends WorkflowSetupProps<Form, IDependencies> {
  clearFormName: string;
}

const SelectNonMonetaryTransactionsStep: React.FC<SelectNonMonetaryTransactionsProps> =
  props => {
    const { t } = useTranslation();
    const { isStuck } = props;

    return (
      <>
        <p className="mt-12 color-grey">
          {t('txt_select_one_or_more_transactions')}
        </p>
        {isStuck && (
          <InlineMessage className="mb-0 mt-24" variant="danger" withIcon>
            {t('txt_step_stuck_move_forward_message')}
          </InlineMessage>
        )}
        <Grid {...props} />
      </>
    );
  };

const ExtraStaticConfigureParameters =
  SelectNonMonetaryTransactionsStep as WorkflowSetupStaticProp;
ExtraStaticConfigureParameters.summaryComponent = Summary;
ExtraStaticConfigureParameters.parseFormValues = parseFormValues;
ExtraStaticConfigureParameters.stepValue = stepValueFunc as any;

ExtraStaticConfigureParameters.defaultValues = { isValid: false };

export default ExtraStaticConfigureParameters;
