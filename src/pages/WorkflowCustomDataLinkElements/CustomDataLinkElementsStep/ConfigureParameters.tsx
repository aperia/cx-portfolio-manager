import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import { ELEMENT_FIELDS } from 'app/constants/mapping';
import {
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { mapGridExpandCollapse } from 'app/helpers';
import { useUnsavedChangeRegistry } from 'app/hooks';
import { usePagination } from 'app/hooks/usePagination';
import {
  Button,
  ColumnType,
  Grid,
  InlineMessage,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty, isUndefined, orderBy } from 'app/_libraries/_dls/lodash';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import ClearAndResetButton from 'pages/_commons/Utils/ClearAndResetButton';
import {
  formatText,
  formatTruncate
} from 'pages/_commons/Utils/formatGridField';
import Paging from 'pages/_commons/Utils/Paging';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';
import { CustomDataLinkElementsFormValue } from '.';
import ActionButton from './ActionButton';
import AddNewElementModal from './AddNewElementModal';
import RemoveElementModal from './RemoveElementModal';
import SubGrid from './SubGrid';

const ConfigureParameters: React.FC<
  WorkflowSetupProps<CustomDataLinkElementsFormValue>
> = ({ setFormValues, formValues, stepId, selectedStep, savedAt, isStuck }) => {
  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();

  const dispatch = useDispatch();

  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;

  const [expandedList, setExpandedList] = useState<string[]>([]);
  const [openAddNewElement, setOpenAddNewElement] = useState<boolean>(false);

  const [removeElementId, setRemoveElementId] =
    useState<undefined | number>(undefined);
  const [editElementId, setEditElementId] =
    useState<undefined | number>(undefined);

  const [hasChange, setHasChange] = useState(false);
  const simpleSearchRef = useRef<any>(null);
  const prevExpandedList = useRef<string[]>();

  const elements = useMemo(
    () => orderBy(formValues?.dataLinkElements || [], ['elementId'], ['asc']),
    [formValues?.dataLinkElements]
  );

  const {
    total,
    currentPage,
    currentPageSize,
    gridData,
    sort,
    searchValue,
    onSortChange,
    onSearchByCase,
    onResetToDefaultFilter,
    onPageChange,
    onPageSizeChange
  } = usePagination(elements, [], 'elementId');

  useEffect(() => {
    prevExpandedList.current = expandedList;
  }, [expandedList]);

  useEffect(() => {
    if (!searchValue && simpleSearchRef?.current) {
      simpleSearchRef.current.clear();
    }
  }, [searchValue]);

  const onRemoveElement = useCallback((idx: number) => {
    setRemoveElementId(idx);
  }, []);

  const onEditElement = useCallback((idx: number) => {
    setEditElementId(idx);
  }, []);

  const onClickAddNewElement = () => {
    setOpenAddNewElement(true);
  };

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CUSTOM_DATA_LINK_ELEMENTS__PARAMETERS,
      priority: 1
    },
    [hasChange]
  );

  const columns: ColumnType[] = useMemo(
    () => [
      {
        id: 'elementId',
        Header: t('txt_custom_data_link_elements_element_id'),
        accessor: formatText(['elementId']),
        width: width < 1280 ? 136 : undefined,
        isSort: true
      },
      {
        id: 'elementName',
        Header: t('txt_custom_data_link_elements_element_name'),
        accessor: formatText(['elementName']),
        width: width < 1280 ? 185 : undefined,
        isSort: true
      },
      {
        id: 'elementBusinessDescription',
        Header: t('txt_custom_data_link_elements_element_business_desc'),
        accessor: formatTruncate(['description']),
        width: width < 1280 ? 240 : undefined
      },
      {
        id: 'actions',
        Header: t('txt_actions'),
        accessor: (data: any) => (
          <div className="d-flex justify-content-center">
            <Button
              size="sm"
              variant="outline-primary"
              onClick={() => onEditElement(data.id)}
            >
              {t('txt_edit')}
            </Button>
            <Button
              size="sm"
              variant="outline-danger"
              className="ml-8"
              onClick={() => onRemoveElement(data.id)}
            >
              {t('txt_delete')}
            </Button>
          </div>
        ),
        className: 'text-center',
        width: 133,
        cellBodyProps: { className: 'py-8' }
      }
    ],
    [t, onEditElement, onRemoveElement, width]
  );

  useEffect(() => {
    const isValid = (formValues?.dataLinkElements?.length || 0) > 0;
    if (formValues?.isValid === isValid) return;

    keepRef?.current?.setFormValues({ isValid });
  }, [formValues]);

  useEffect(() => {
    if (stepId !== selectedStep) {
      onResetToDefaultFilter();
      setExpandedList([]);
    }
    setHasChange(false);
  }, [stepId, selectedStep, savedAt, onResetToDefaultFilter]);

  useEffect(() => {
    dispatch(actionsWorkflowSetup.getWorkflowMetadata(ELEMENT_FIELDS));
  }, [dispatch]);

  const handleOnExpand = (dataKeyList: string[]) => {
    setExpandedList(isEmpty(dataKeyList) ? [] : dataKeyList);
  };

  const handleCloseAddModal = (
    element?: WorkflowSetupElement & { id?: number }
  ) => {
    if (element) {
      setFormValues({ dataLinkElements: [...elements, element] });
      setExpandedList([element.id as any]);
      onResetToDefaultFilter();
      setHasChange(true);
    }
    setOpenAddNewElement(false);
  };

  const handleCloseEditModal = (element?: WorkflowSetupElement) => {
    if (element) {
      const newEls = elements.map(item => {
        if (item.id === editElementId) return element;
        return item;
      });

      setFormValues({ dataLinkElements: newEls });
      setExpandedList(prev => [...prev, prevExpandedList?.current] as any);
      setHasChange(true);
    }
    setEditElementId(undefined);
  };

  const handleCloseDeleteModal = (reload?: boolean) => {
    if (reload) {
      const selectedEl = elements.filter(i => i.id !== removeElementId);
      setFormValues({
        dataLinkElements: selectedEl
      });
      setHasChange(true);
    }
    setRemoveElementId(undefined);
  };

  const handleClearAndReset = useCallback(() => {
    onResetToDefaultFilter({ keepSort: true });
    setExpandedList([]);
  }, [onResetToDefaultFilter]);

  const subRow = useCallback((row: any) => {
    return <SubGrid row={row} />;
  }, []);

  const renderGrid = useMemo(() => {
    if (isEmpty(gridData) && !searchValue) return;
    return (
      <div className="mt-16">
        {isEmpty(gridData) && searchValue ? (
          <div className="d-flex flex-column justify-content-center mt-40 mb-32">
            <NoDataFound
              id="newMethod_notfound"
              hasSearch
              title={t('txt_no_results_found')}
              linkTitle={t('txt_clear_and_reset')}
              onLinkClicked={handleClearAndReset}
            />
          </div>
        ) : (
          <>
            <Grid
              togglable
              columns={columns}
              data={gridData}
              dataItemKey="id"
              subRow={subRow}
              toggleButtonConfigList={gridData.map(
                mapGridExpandCollapse('id', t)
              )}
              expandedItemKey={'id'}
              expandedList={expandedList}
              onExpand={handleOnExpand}
              sortBy={[sort]}
              onSortChange={onSortChange}
            />
            {total > 10 && (
              <Paging
                totalItem={total}
                pageSize={currentPageSize}
                page={currentPage}
                onChangePage={onPageChange}
                onChangePageSize={onPageSizeChange}
              />
            )}
          </>
        )}
      </div>
    );
  }, [
    columns,
    expandedList,
    t,
    total,
    currentPageSize,
    currentPage,
    gridData,
    onPageChange,
    subRow,
    sort,
    searchValue,
    onSortChange,
    onPageSizeChange,
    handleClearAndReset
  ]);

  return (
    <div className="pt-24 border-top mt-24">
      {isStuck && (
        <InlineMessage className="mb-24" variant="danger" withIcon>
          {t('txt_step_stuck_move_forward_message')}
        </InlineMessage>
      )}
      {!isEmpty(elements) && (
        <div className="d-flex align-items-center justify-content-between mb-16">
          <h5>{t('txt_dataLink_element_list')}</h5>

          <SimpleSearch
            ref={simpleSearchRef}
            placeholder={t('txt_type_to_search')}
            onSearch={e =>
              onSearchByCase(e, [
                'elementName',
                'elementId',
                'description',
                'topName',
                'bottomName'
              ])
            }
            popperElement={
              <>
                <p className="color-grey">{t('txt_search_description')}</p>
                <ul className="search-field-item list-unstyled">
                  <li className="mt-16">{t('txt_bottom_name')}</li>
                  <li className="mt-16">
                    {t('txt_element_business_description')}
                  </li>
                  <li className="mt-16">{t('txt_element_id')}</li>
                  <li className="mt-16">{t('txt_element_name')}</li>
                  <li className="mt-16">{t('txt_top_name')}</li>
                </ul>
              </>
            }
          />
        </div>
      )}

      <div
        className={`row ${
          !isEmpty(gridData) || !isEmpty(elements)
            ? 'justify-content-end mr-n8'
            : 'mt-16'
        }`}
      >
        <ActionButton
          data={elements}
          small={!isEmpty(elements)}
          handleClickBtn={onClickAddNewElement}
          title="txt_custom_data_link_elements_action_button_description"
        />
        {searchValue && !isEmpty(gridData) && (
          <div className="ml-8">
            <ClearAndResetButton small onClearAndReset={handleClearAndReset} />
          </div>
        )}
      </div>

      {renderGrid}

      {!isUndefined(removeElementId) && (
        <RemoveElementModal
          id="removeElementModal"
          show
          onClose={handleCloseDeleteModal}
        />
      )}

      {openAddNewElement && (
        <AddNewElementModal
          id="removeElement"
          show
          onClose={handleCloseAddModal}
        />
      )}

      {!isUndefined(editElementId) && (
        <AddNewElementModal
          id="editSelectedElement"
          show
          isCreate={false}
          element={elements.find(i => i.id === editElementId)}
          onClose={handleCloseEditModal}
        />
      )}
    </div>
  );
};

export default ConfigureParameters;
