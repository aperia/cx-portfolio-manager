import EnhanceDropdownList from 'app/components/EnhanceDropdownList';
import TextAreaCountDown from 'app/components/TextAreaCountDown';
import { ElementsFieldParameterEnum } from 'app/constants/enums';
import { ChangeComponentEvent, TextArea, TextBox } from 'app/_libraries/_dls';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import React from 'react';

export interface MetadataProps {
  elementId: RefData[];
  elementName: RefData[];
  topName: RefData[];
  bottomName: RefData[];
  reallocation: RefData[];
  deltaType: RefData[];
  transferTypes: RefData[];
  elementType: RefData[];
  elementLength: RefData[];
  numberOfDecimals: RefData[];
  minValue: RefData[];
  maxValue: RefData[];
  validValues: RefData[];
  defaultValue: RefData[];
  letterFormat: RefData[];
  useDefaultOnLetter: RefData[];
  ocsTran: RefData[];
  maskText: RefData[];
  realTimeUpdate: RefData[];
  retainOnTransfer: RefData[];
  elementBusinessDescription: RefData[];
}

export const DROPDOWN_ELEMENT = [
  ElementsFieldParameterEnum.Reallocation,
  ElementsFieldParameterEnum.DeltaType,
  ElementsFieldParameterEnum.TransferTypes,
  ElementsFieldParameterEnum.ElementType,
  ElementsFieldParameterEnum.LetterFormat,
  ElementsFieldParameterEnum.UseDefaultOnLetter,
  ElementsFieldParameterEnum.RealTimeUpdate,
  ElementsFieldParameterEnum.RetainOnTransfer
];

export const NUMBER_ELEMENT = [
  ElementsFieldParameterEnum.ElementLength,
  ElementsFieldParameterEnum.NumberOfDecimals
];

export const ALPHA_NUMBER_ELEMENT = [
  ElementsFieldParameterEnum.ElementID,
  ElementsFieldParameterEnum.MaskText
];

export const countMultiValues = (item: any) => {
  if (isEmpty(item)) return;
  return item!.split(';').filter((el: any) => !isEmpty(el.trim())).length;
};
export const valueAccessor = (
  data: Record<string, any>,
  metadata: MetadataProps,
  elements: WorkflowSetupElementObjectParams,
  onChange: (
    field: keyof WorkflowSetupElementObjectParams,
    isRefData?: boolean
  ) => (e: ChangeComponentEvent) => void,
  t: any,
  errors: MagicKeyValue
) => {
  const {
    elementType,
    deltaType,
    useDefaultOnLetter,
    realTimeUpdate,
    reallocation,
    retainOnTransfer,
    transferTypes,
    letterFormat
  } = metadata;
  const paramId = data.id as ElementsFieldParameterEnum;
  const textPlaceHolder = t('txt_enter_a_value');
  const dropdownPlaceHolder = t('txt_select_an_option');

  switch (paramId) {
    case ElementsFieldParameterEnum.ElementID: {
      return (
        <TextBox
          placeholder={textPlaceHolder}
          dataTestId="addNewElement__elementID"
          size="sm"
          value={elements?.[ElementsFieldParameterEnum.ElementID]}
          onChange={onChange(ElementsFieldParameterEnum.ElementID)}
          maxLength={8}
        />
      );
    }
    case ElementsFieldParameterEnum.ElementName: {
      return (
        <TextBox
          placeholder={textPlaceHolder}
          dataTestId="addNewElement__elementName"
          size="sm"
          value={elements?.[ElementsFieldParameterEnum.ElementName]}
          onChange={onChange(ElementsFieldParameterEnum.ElementName)}
          maxLength={8}
        />
      );
    }
    case ElementsFieldParameterEnum.TopName: {
      return (
        <TextBox
          placeholder={textPlaceHolder}
          dataTestId="addNewElement__topName"
          size="sm"
          value={elements?.[ElementsFieldParameterEnum.TopName]}
          onChange={onChange(ElementsFieldParameterEnum.TopName)}
          maxLength={8}
        />
      );
    }
    case ElementsFieldParameterEnum.BottomName: {
      return (
        <TextBox
          placeholder={textPlaceHolder}
          dataTestId="addNewElement__bottomName"
          size="sm"
          value={elements?.[ElementsFieldParameterEnum.BottomName]}
          onChange={onChange(ElementsFieldParameterEnum.BottomName)}
          maxLength={8}
        />
      );
    }
    case ElementsFieldParameterEnum.Reallocation: {
      const value = reallocation.find(
        o => o.code === elements?.[ElementsFieldParameterEnum.Reallocation]
      );

      return (
        <EnhanceDropdownList
          placeholder={dropdownPlaceHolder}
          dataTestId="addNewElement__reallocation"
          size="small"
          value={value}
          onChange={onChange(ElementsFieldParameterEnum.Reallocation, true)}
          options={reallocation}
        />
      );
    }
    case ElementsFieldParameterEnum.DeltaType: {
      const value = deltaType.find(
        o => o.code === elements?.[ElementsFieldParameterEnum.DeltaType]
      );

      return (
        <EnhanceDropdownList
          placeholder={dropdownPlaceHolder}
          dataTestId="addNewElement__deltaType"
          size="small"
          value={value}
          onChange={onChange(ElementsFieldParameterEnum.DeltaType, true)}
          options={deltaType}
        />
      );
    }
    case ElementsFieldParameterEnum.TransferTypes: {
      const value = transferTypes.find(
        o => o.code === elements?.[ElementsFieldParameterEnum.TransferTypes]
      );

      return (
        <EnhanceDropdownList
          placeholder={dropdownPlaceHolder}
          dataTestId="addNewElement__transferTypes"
          size="small"
          value={value}
          onChange={onChange(ElementsFieldParameterEnum.TransferTypes, true)}
          options={transferTypes}
        />
      );
    }
    case ElementsFieldParameterEnum.ElementType: {
      const value = elementType.find(
        o => o.code === elements?.[ElementsFieldParameterEnum.ElementType]
      );

      return (
        <EnhanceDropdownList
          placeholder={dropdownPlaceHolder}
          dataTestId="addNewElement__elementType"
          size="small"
          value={value}
          onChange={onChange(ElementsFieldParameterEnum.ElementType, true)}
          options={elementType}
        />
      );
    }
    case ElementsFieldParameterEnum.ElementLength: {
      return (
        <TextBox
          placeholder={textPlaceHolder}
          dataTestId="addNewElement__elementLength"
          size="sm"
          maxLength={2}
          autoFocus={false}
          value={elements?.[ElementsFieldParameterEnum.ElementLength] || ''}
          onChange={onChange(ElementsFieldParameterEnum.ElementLength)}
        />
      );
    }
    case ElementsFieldParameterEnum.NumberOfDecimals: {
      return (
        <TextBox
          placeholder={textPlaceHolder}
          dataTestId="addNewElement__numberOfDecimals"
          size="sm"
          maxLength={2}
          autoFocus={false}
          value={elements?.[ElementsFieldParameterEnum.NumberOfDecimals] || ''}
          onChange={onChange(ElementsFieldParameterEnum.NumberOfDecimals)}
        />
      );
    }
    case ElementsFieldParameterEnum.MinValue: {
      return (
        <TextBox
          placeholder={textPlaceHolder}
          dataTestId="addNewElement__minValue"
          size="sm"
          value={elements?.[ElementsFieldParameterEnum.MinValue]}
          onChange={onChange(ElementsFieldParameterEnum.MinValue)}
          maxLength={12}
        />
      );
    }
    case ElementsFieldParameterEnum.MaxValue: {
      return (
        <TextBox
          placeholder={textPlaceHolder}
          dataTestId="addNewElement__maxValue"
          size="sm"
          value={elements?.[ElementsFieldParameterEnum.MaxValue]}
          onChange={onChange(ElementsFieldParameterEnum.MaxValue)}
          maxLength={12}
        />
      );
    }

    case ElementsFieldParameterEnum.ValidValues: {
      return (
        <div className="w-100">
          <TextArea
            placeholder={t('txt_enter_values')}
            dataTestId="addNewElement__validValues"
            rows={4}
            value={elements?.[ElementsFieldParameterEnum.ValidValues]}
            onChange={onChange(ElementsFieldParameterEnum.ValidValues)}
            className="resize-none"
            error={errors?.validValues}
          />
          <div className="mt-8">
            <span className="fs-12 color-grey-l24">
              {t('txt_custom_data_link_elements_sub_text_area')}
            </span>
          </div>
        </div>
      );
    }
    case ElementsFieldParameterEnum.DefaultValue: {
      return (
        <TextBox
          placeholder={textPlaceHolder}
          dataTestId="addNewElement__defaultValue"
          size="sm"
          value={elements?.[ElementsFieldParameterEnum.DefaultValue]}
          onChange={onChange(ElementsFieldParameterEnum.DefaultValue)}
          maxLength={12}
        />
      );
    }
    case ElementsFieldParameterEnum.LetterFormat: {
      const value = letterFormat.find(
        o => o.code === elements?.[ElementsFieldParameterEnum.LetterFormat]
      );

      return (
        <EnhanceDropdownList
          placeholder={dropdownPlaceHolder}
          dataTestId="addNewElement__letterFormat"
          size="small"
          value={value}
          onChange={onChange(ElementsFieldParameterEnum.LetterFormat, true)}
          options={letterFormat}
        />
      );
    }
    case ElementsFieldParameterEnum.UseDefaultOnLetter: {
      const value = useDefaultOnLetter.find(
        o =>
          o.code === elements?.[ElementsFieldParameterEnum.UseDefaultOnLetter]
      );

      return (
        <EnhanceDropdownList
          placeholder={dropdownPlaceHolder}
          dataTestId="addNewElement__useDefaultOnLetter"
          size="small"
          value={value}
          onChange={onChange(
            ElementsFieldParameterEnum.UseDefaultOnLetter,
            true
          )}
          options={useDefaultOnLetter}
        />
      );
    }
    case ElementsFieldParameterEnum.OCSTran: {
      return (
        <TextBox
          placeholder={textPlaceHolder}
          dataTestId="addNewElement__OCSTran"
          size="sm"
          value={elements?.[ElementsFieldParameterEnum.OCSTran]}
          onChange={onChange(ElementsFieldParameterEnum.OCSTran)}
          maxLength={6}
        />
      );
    }
    case ElementsFieldParameterEnum.MaskText: {
      return (
        <TextBox
          placeholder={textPlaceHolder}
          dataTestId="addNewElement__maskText"
          size="sm"
          value={elements?.[ElementsFieldParameterEnum.MaskText]}
          onChange={onChange(ElementsFieldParameterEnum.MaskText)}
          maxLength={12}
        />
      );
    }
    case ElementsFieldParameterEnum.RealTimeUpdate: {
      const value = realTimeUpdate.find(
        o => o.code === elements?.[ElementsFieldParameterEnum.RealTimeUpdate]
      );

      return (
        <EnhanceDropdownList
          placeholder={dropdownPlaceHolder}
          dataTestId="addNewElement__realTimeUpdate"
          size="small"
          value={value}
          onChange={onChange(ElementsFieldParameterEnum.RealTimeUpdate, true)}
          options={realTimeUpdate}
        />
      );
    }
    case ElementsFieldParameterEnum.RetainOnTransfer: {
      const value = retainOnTransfer.find(
        o => o.code === elements?.[ElementsFieldParameterEnum.RetainOnTransfer]
      );

      return (
        <EnhanceDropdownList
          placeholder={dropdownPlaceHolder}
          dataTestId="addNewElement__retainOnTransfer"
          size="small"
          value={value}
          onChange={onChange(ElementsFieldParameterEnum.RetainOnTransfer, true)}
          options={retainOnTransfer}
        />
      );
    }
    case ElementsFieldParameterEnum.ElementBusinessDescription: {
      return (
        <TextAreaCountDown
          placeholder={textPlaceHolder}
          dataTestId="addNewElement__elementBusinessDescription"
          rows={4}
          value={
            elements?.[ElementsFieldParameterEnum.ElementBusinessDescription]
          }
          onChange={onChange(
            ElementsFieldParameterEnum.ElementBusinessDescription
          )}
          maxLength={70}
        />
      );
    }
  }
};
