import ModalRegistry from 'app/components/ModalRegistry';
import {
  Button,
  ModalBody,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import { isFunction } from 'app/_libraries/_dls/lodash';
import React from 'react';

interface IProps {
  id: string;
  show: boolean;
  onClose?: (reload?: boolean) => void;
}

const RemoveElementModal: React.FC<IProps> = ({ id, show, onClose }) => {
  const { t } = useTranslation();
  const onRemove = async () => {
    isFunction(onClose) && onClose(true);
  };

  const handleCloseWithoutReload = () => {
    isFunction(onClose) && onClose();
  };

  return (
    <ModalRegistry
      id={id}
      show={show}
      onAutoClosedAll={handleCloseWithoutReload}
      classes={{ content: '' }}
    >
      <ModalHeader border closeButton onHide={handleCloseWithoutReload}>
        <ModalTitle>{t('txt_confirm_deletion')}</ModalTitle>
      </ModalHeader>
      <ModalBody>
        <span>{t('txt_custom_data_link_elements_delete_msg')}</span>
      </ModalBody>
      <div className="dls-modal-footer modal-footer">
        <Button variant="secondary" onClick={handleCloseWithoutReload}>
          {t('txt_cancel')}
        </Button>
        <Button variant="danger" onClick={onRemove}>
          {t('txt_delete')}
        </Button>
      </div>
    </ModalRegistry>
  );
};

export default RemoveElementModal;
