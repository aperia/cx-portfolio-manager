import { useTranslation } from 'app/_libraries/_dls';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React from 'react';
import ConfigureParameters from './ConfigureParameters';
import parseFormValues from './parseFormValues';
import CustomDataLinkElementsSummary from './Summary';

export interface CustomDataLinkElementsFormValue {
  isValid?: boolean;
  dataLinkElements?: any[];
}

const CustomDataLinkElements: React.FC<
  WorkflowSetupProps<CustomDataLinkElementsFormValue>
> = props => {
  const { t } = useTranslation();

  return (
    <>
      <p className="mt-8 color-grey">
        {t('txt_custom_data_link_elements_header_config_parameters')}
      </p>
      <ConfigureParameters {...props} />
    </>
  );
};

const ExtraStaticCustomDataLinkElementsStep =
  CustomDataLinkElements as WorkflowSetupStaticProp;

ExtraStaticCustomDataLinkElementsStep.summaryComponent =
  CustomDataLinkElementsSummary;
ExtraStaticCustomDataLinkElementsStep.defaultValues = {
  isValid: false,
  dataLinkElements: []
};

ExtraStaticCustomDataLinkElementsStep.parseFormValues = parseFormValues;

export default ExtraStaticCustomDataLinkElementsStep;
