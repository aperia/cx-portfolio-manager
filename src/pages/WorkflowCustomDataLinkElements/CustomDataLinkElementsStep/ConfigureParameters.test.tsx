import { act, fireEvent, queryByText } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { ElementsFieldParameterEnum } from 'app/constants/enums';
import { WorkflowMetadataList } from 'app/fixtures/workflow-metadata';
import * as helper from 'app/helpers/isDevice';
import { mockUseDispatchFnc, renderWithMockStore } from 'app/utils';
import * as CommonRedux from 'pages/_commons/redux/Common/select-hooks';
import React from 'react';
import ConfigureParameters from './ConfigureParameters';
const mockUseDispatch = mockUseDispatchFnc();
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

jest.mock('./SubGrid', () => ({
  __esModule: true,
  default: () => <div>SubGrid_component</div>
}));
const mockOnCloseWithoutElement = jest.fn();
const mockOnCloseReload = jest.fn();
jest.mock('./RemoveElementModal', () => ({
  __esModule: true,
  default: ({ onClose }: any) => {
    const handleOnClose = () => {
      if (mockOnCloseReload()) {
        onClose();
        return;
      }

      onClose({
        id: 1,
        elementName: 'CA',
        elementId: 'AA',
        bottomName: 'BB',
        parameters: [{ name: 'element.name', value: 'CA' }]
      });
    };
    return <div onClick={handleOnClose}>RemoveElementModal_component</div>;
  }
}));

jest.mock('./AddNewElementModal', () => ({
  __esModule: true,
  default: ({ onClose }: any) => {
    const handleOnClose = () => {
      if (mockOnCloseWithoutElement()) {
        onClose();
        return;
      }

      onClose({
        id: 1,
        elementName: 'CA',
        elementId: 'AA',
        bottomName: 'BB',
        parameters: [{ name: 'element.name', value: 'CA' }]
      });
    };
    return <div onClick={handleOnClose}>AddNewElementModal_component</div>;
  }
}));

const mockUseSelectWindowDimension = jest.spyOn(
  CommonRedux,
  'useSelectWindowDimension'
);

const mockUseSelectWindowDimensionImplementation = (width: number) => {
  mockUseSelectWindowDimension.mockImplementation(() => ({ width } as any));
};

const renderComponent = (props: any) => {
  return renderWithMockStore(<ConfigureParameters {...props} />, {
    initialState: {
      workflowSetup: {
        elementMetadata: {
          elements: WorkflowMetadataList
        }
      }
    } as AppState
  });
};
const mockHelper: any = helper;

describe('Custom Data Link Elements > ConfigureParameters', () => {
  const props = {
    formValues: {
      dataLinkElements: [
        {
          id: 1,
          elementName: 'CA',
          elementId: 'AA',
          bottomName: 'BB',
          parameters: [
            { name: ElementsFieldParameterEnum.ElementName, value: 'CA' }
          ]
        },
        {
          id: 2,
          elementName: 'AC',
          elementId: 'AA',
          bottomName: 'BB',
          parameters: [
            { name: ElementsFieldParameterEnum.ElementName, value: 'AC' }
          ]
        }
      ]
    },
    setFormValues: jest.fn()
  } as any;
  beforeEach(() => {
    mockUseSelectWindowDimensionImplementation(120);
    mockUseDispatch.mockImplementation(() => (() => {}) as any);
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
  });

  it('Should render ConfigureParameters > isDevice', async () => {
    mockHelper.isDevice = true;
  });

  it('Should render ConfigureParameters > isDevice false', async () => {
    mockHelper.isDevice = false;
  });

  it('should render ConfigureParameters empty props', async () => {
    const wrapper = await renderComponent({
      ...props,
      formValues: {}
    });
    const text = wrapper.getByText(
      'txt_custom_data_link_elements_action_button_description'
    );
    expect(text).toBeInTheDocument;
  });

  it('should render ConfigureParameters', async () => {
    mockUseSelectWindowDimensionImplementation(120);
    const wrapper = await renderComponent(props);
    const text = wrapper.getByText(
      'txt_custom_data_link_elements_element_name'
    );
    expect(text).toBeInTheDocument;
  });

  it('should render ConfigureParameters with stepId and selectedStep', async () => {
    mockUseSelectWindowDimensionImplementation(1400);
    const wrapper = await renderComponent({
      ...props,
      stepId: 1,
      selectedStep: 2
    });
    const text = wrapper.getByText(
      'txt_custom_data_link_elements_element_name'
    );
    expect(text).toBeInTheDocument;
  });

  it('should render ConfigureParameters with stepId and selectedStep and isStuck', async () => {
    mockUseSelectWindowDimensionImplementation(1400);
    const wrapper = await renderComponent({
      ...props,
      stepId: 1,
      isStuck: true,
      selectedStep: 2
    });
    const text = wrapper.getByText('txt_step_stuck_move_forward_message');
    expect(text).toBeInTheDocument;
  });

  it('should render ConfigureParameters empty data', async () => {
    const wrapper = await renderComponent({
      ...props,
      formValues: { dataLinkElements: [], isValid: false }
    });
    const text = wrapper.getByText(
      'txt_custom_data_link_elements_action_button_description'
    );
    expect(text).toBeInTheDocument;
  });

  it('should render ConfigureParameters and add new element', async () => {
    const wrapper = await renderComponent(props);
    const text = wrapper.getByText(
      'txt_custom_data_link_elements_action_button_description'
    );

    userEvent.click(text);
    expect(text).toBeInTheDocument;
  });
  it('should render ConfigureParameters and edit element', async () => {
    const wrapper = await renderComponent(props);
    const text = wrapper.getAllByText('txt_edit');

    userEvent.click(text[1]);
    expect(text).toBeInTheDocument;
  });
  it('should render ConfigureParameters and delete element', async () => {
    const wrapper = await renderComponent(props);
    const text = wrapper.getAllByText('txt_delete');

    userEvent.click(text[0]);
    expect(text).toBeInTheDocument;
  });

  it('should render ConfigureParameters and with paging', async () => {
    const newProps = {
      formValues: {
        dataLinkElements: [
          {
            id: 1,
            elementName: 'CA',
            elementId: 'AA',
            bottomName: 'BB',
            parameters: [
              { name: ElementsFieldParameterEnum.ElementName, value: 'CA' }
            ]
          },
          {
            id: 2,
            elementName: 'AC',
            elementId: 'AA',
            bottomName: 'BB',
            parameters: [
              { name: ElementsFieldParameterEnum.ElementName, value: 'AC' }
            ]
          },
          {
            id: 3,
            elementName: 'CA',
            elementId: 'AA',
            bottomName: 'BB',
            parameters: [
              { name: ElementsFieldParameterEnum.ElementName, value: 'CA' }
            ]
          },
          {
            id: 4,
            elementName: 'AC',
            elementId: 'AA',
            bottomName: 'BB',
            parameters: [
              { name: ElementsFieldParameterEnum.ElementName, value: 'AC' }
            ]
          },
          {
            id: 5,
            elementName: 'CA',
            elementId: 'AA',
            bottomName: 'BB',
            parameters: [
              { name: ElementsFieldParameterEnum.ElementName, value: 'CA' }
            ]
          },
          {
            id: 6,
            elementName: 'AC',
            elementId: 'AA',
            bottomName: 'BB',
            parameters: [
              { name: ElementsFieldParameterEnum.ElementName, value: 'AC' }
            ]
          },
          {
            id: 7,
            elementName: 'CA',
            elementId: 'AA',
            bottomName: 'BB',
            parameters: [
              { name: ElementsFieldParameterEnum.ElementName, value: 'CA' }
            ]
          },
          {
            id: 8,
            elementName: 'AC',
            elementId: 'AA',
            bottomName: 'BB',
            parameters: [
              { name: ElementsFieldParameterEnum.ElementName, value: 'AC' }
            ]
          },
          {
            id: 9,
            elementName: 'CA',
            elementId: 'AA',
            bottomName: 'BB',
            parameters: [
              { name: ElementsFieldParameterEnum.ElementName, value: 'CA' }
            ]
          },
          {
            id: 10,
            elementName: 'AC',
            elementId: 'AA',
            bottomName: 'BB',
            parameters: [
              { name: ElementsFieldParameterEnum.ElementName, value: 'AC' }
            ]
          },
          {
            id: 11,
            elementName: 'CA',
            elementId: 'AA',
            bottomName: 'BB',
            parameters: [
              { name: ElementsFieldParameterEnum.ElementName, value: 'CA' }
            ]
          },
          {
            id: 12,
            elementName: 'AC',
            elementId: 'AA',
            bottomName: 'BB',
            parameters: [
              { name: ElementsFieldParameterEnum.ElementName, value: 'AC' }
            ]
          }
        ]
      },
      setFormValues: jest.fn()
    } as any;
    const wrapper = await renderComponent(newProps);
    const text = wrapper.getAllByText('txt_delete');

    expect(text).toHaveLength(10);
  });

  it('should render ConfigureParameters with delete and close modal and data is empty', async () => {
    mockOnCloseWithoutElement.mockReturnValue(true);
    const wrapper = await renderComponent(props);
    const text = wrapper.getAllByText('txt_edit');

    userEvent.click(text[1]);

    const closeBtn = wrapper.getByText('AddNewElementModal_component');

    userEvent.click(closeBtn);

    expect(closeBtn).toBeInTheDocument;
  });
  it('should render ConfigureParameters with delete and close modal and data is !empty', async () => {
    mockOnCloseWithoutElement.mockReturnValue(false);
    const wrapper = await renderComponent(props);
    const text = wrapper.getAllByText('txt_edit');

    userEvent.click(text[1]);

    const closeBtn = wrapper.getByText('AddNewElementModal_component');

    userEvent.click(closeBtn);

    expect(closeBtn).toBeInTheDocument;
  });

  it('should render ConfigureParameters with delete and close modal and reload = false', async () => {
    mockOnCloseReload.mockReturnValue(false);
    const wrapper = await renderComponent(props);
    const text = wrapper.getAllByText('txt_delete');

    userEvent.click(text[0]);

    const closeBtn = wrapper.getByText('RemoveElementModal_component');

    userEvent.click(closeBtn);

    expect(closeBtn).toBeInTheDocument;
  });
  it('should render ConfigureParameters with delete and close modal and reload = true', async () => {
    mockOnCloseReload.mockReturnValue(true);
    const wrapper = await renderComponent(props);
    const text = wrapper.getAllByText('txt_delete');

    userEvent.click(text[0]);

    const closeBtn = wrapper.getByText('RemoveElementModal_component');

    userEvent.click(closeBtn);

    expect(closeBtn).toBeInTheDocument;
  });

  it('should render ConfigureParameters with add and close modal', async () => {
    mockOnCloseWithoutElement.mockReturnValue(false);
    const wrapper = await renderComponent(props);
    const text = wrapper.getByText(
      'txt_custom_data_link_elements_action_button_description'
    );

    userEvent.click(text);

    const closeBtn = wrapper.getByText('AddNewElementModal_component');

    userEvent.click(closeBtn);

    expect(closeBtn).toBeInTheDocument;
  });
  it('should render ConfigureParameters with add and close modal', async () => {
    mockOnCloseWithoutElement.mockReturnValue(true);
    const wrapper = await renderComponent(props);
    const text = wrapper.getByText(
      'txt_custom_data_link_elements_action_button_description'
    );

    userEvent.click(text);

    const closeBtn = wrapper.getByText('AddNewElementModal_component');

    userEvent.click(closeBtn);

    expect(closeBtn).toBeInTheDocument;
  });

  it('should render ConfigureParameters and click search bar', async () => {
    mockOnCloseWithoutElement.mockReturnValue(true);
    const wrapper = await renderComponent(props);
    const text = wrapper.getByPlaceholderText('txt_type_to_search');

    userEvent.type(text, 'hehe {enter}');
    expect(text).toBeInTheDocument;

    const resetBtn = wrapper.getByText('txt_clear_and_reset');

    expect(resetBtn).toBeInTheDocument();
    userEvent.click(resetBtn);
  });

  it('should render ConfigureParameters and click expand', async () => {
    const wrapper = await renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    const firstChild = body?.children[0].querySelector(
      'button[class="btn btn-icon-secondary btn-sm"]'
    );
    fireEvent.click(firstChild as Element);
    expect(
      queryByText(wrapper.container, /SubGrid_component/i)
    ).toBeInTheDocument();
    fireEvent.click(firstChild as Element);
    expect(
      queryByText(wrapper.container, /SubGrid_component/i)
    ).not.toBeInTheDocument();
  });

  it('should render ConfigureParameters and click search bar and clear ref', async () => {
    const wrapper = await renderComponent(props);
    const text = wrapper.getByPlaceholderText('txt_type_to_search');

    userEvent.type(text, 'A {enter}');

    expect(text).toBeInTheDocument;

    const resetBtn = wrapper.getByText('txt_clear_and_reset');
    expect(resetBtn).toBeInTheDocument();

    act(() => {
      userEvent.click(resetBtn);
    });
  });
});
