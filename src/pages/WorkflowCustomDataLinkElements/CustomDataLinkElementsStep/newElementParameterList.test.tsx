import {
  ElementsFieldNameEnum,
  ElementsFieldParameterEnum,
  ElementTypeEnum
} from 'app/constants/enums';
import React from 'react';
import { parameterList } from './newElementParameterList';

describe('pages > WorkflowCustomDataLinkElements > CustomDataLinkElementsStep ', () => {
  it('> parameterList', () => {
    expect(parameterList).toEqual([
      {
        id: ElementsFieldParameterEnum.ElementID,
        fieldName: ElementsFieldNameEnum.ElementID,
        fieldType: ElementTypeEnum.Required,
        moreInfoText:
          'Client defined identifier representing an existing or new element. Length: variable length, eight positions.',
        moreInfo: (
          <p>
            Client defined identifier representing an existing or new element.
            Length: variable length, eight positions.
          </p>
        )
      },
      {
        id: ElementsFieldParameterEnum.ElementName,
        fieldName: ElementsFieldNameEnum.ElementName,
        fieldType: ElementTypeEnum.Required,
        moreInfoText:
          'Name describing the element. Length: variable length, eight positions.',
        moreInfo: (
          <p>
            Name describing the element. Length: variable length, eight
            positions.
          </p>
        )
      },
      {
        id: ElementsFieldParameterEnum.ElementBusinessDescription,
        fieldName: ElementsFieldNameEnum.ElementBusinessDescription,
        fieldType: ElementTypeEnum.Required,
        moreInfoText:
          'Text describing the use and purpose of the decision element.',
        moreInfo: 'Text describing the use and purpose of the decision element.'
      },
      {
        id: ElementsFieldParameterEnum.TopName,
        fieldName: ElementsFieldNameEnum.TopName,
        fieldType: ElementTypeEnum.Optional,
        moreInfoText:
          'Element name as it appears on the top line of a client table or strategy. Length: Variable length, eight positions.',
        moreInfo: (
          <div>
            Element name as it appears on the top line of a client table or
            strategy.
            <p>Length: Variable length, eight positions.</p>
          </div>
        )
      },
      {
        id: ElementsFieldParameterEnum.BottomName,
        fieldName: ElementsFieldNameEnum.BottomName,
        fieldType: ElementTypeEnum.Optional,
        moreInfoText:
          'Element name as it appears on the bottom line of a client table or strategy. Length: Variable length, eight positions.',
        moreInfo: (
          <div>
            Element name as it appears on the bottom line of a client table or
            strategy.
            <p>Length: Variable length, eight positions.</p>
          </div>
        )
      },
      {
        id: ElementsFieldParameterEnum.Reallocation,
        fieldName: ElementsFieldNameEnum.Reallocation,
        fieldType: ElementTypeEnum.Optional,
        moreInfoText:
          'Indicator designating whether the decision element is eligible for use in the Data Link attribute change area of Disclosure Design Manager service or Data Link element reallocation process area of the risk service.',
        moreInfo:
          'Indicator designating whether the decision element is eligible for use in the Data Link attribute change area of Disclosure Design Manager service or Data Link element reallocation process area of the risk service.'
      },
      {
        id: ElementsFieldParameterEnum.DeltaType,
        fieldName: ElementsFieldNameEnum.DeltaType,
        fieldType: ElementTypeEnum.Optional,
        moreInfoText:
          'Code representing the type of comparison the System should make when processing this decision element for the Datalink element reallocation process area in the Risk service of Rules Management. The System compares an account’s old value and new value for the decision element using the method you indicate in this field to determine whether the account should be sent to the Adaptive Control System for mid-cycle credit line processing.',
        moreInfo:
          'Code representing the type of comparison the System should make when processing this decision element for the Datalink element reallocation process area in the Risk service of Rules Management. The System compares an account’s old value and new value for the decision element using the method you indicate in this field to determine whether the account should be sent to the Adaptive Control System for mid-cycle credit line processing.'
      },
      {
        id: ElementsFieldParameterEnum.TransferTypes,
        fieldName: ElementsFieldNameEnum.TransferTypes,
        fieldType: ElementTypeEnum.Optional,
        moreInfoText:
          'Code determining whether to transfer the element when a type C, N, or U account transfer takes place.',
        moreInfo:
          'Code determining whether to transfer the element when a type C, N, or U account transfer takes place.'
      },
      {
        id: ElementsFieldParameterEnum.ElementType,
        fieldName: ElementsFieldNameEnum.ElementType,
        fieldType: ElementTypeEnum.Required,
        moreInfoText:
          'Code representing the type of element being added, changed, or deleted.',
        moreInfo:
          'Code representing the type of element being added, changed, or deleted.'
      },
      {
        id: ElementsFieldParameterEnum.ElementLength,
        fieldName: ElementsFieldNameEnum.ElementLength,
        fieldType: ElementTypeEnum.Required,
        moreInfoText:
          'Length of element type. Alphanumeric and numeric elements can be a maximum of 12 characters in length. Date elements must be 8 characters in length and must follow the YYYYMMDD format.',
        moreInfo:
          'Length of element type. Alphanumeric and numeric elements can be a maximum of 12 characters in length. Date elements must be 8 characters in length and must follow the YYYYMMDD format.'
      },
      {
        id: ElementsFieldParameterEnum.NumberOfDecimals,
        fieldName: ElementsFieldNameEnum.NumberOfDecimals,
        fieldType: ElementTypeEnum.Optional,
        moreInfoText:
          'Count of decimals used for numeric element types. If you use this field, the number entered must be at least 3 characters less than the element length.',
        moreInfo:
          'Count of decimals used for numeric element types. If you use this field, the number entered must be at least 3 characters less than the element length.'
      },
      {
        id: ElementsFieldParameterEnum.MinValue,
        fieldName: ElementsFieldNameEnum.MinValue,
        fieldType: ElementTypeEnum.Required,
        moreInfoText:
          'Client-defined minimum value allowed for this element. The value entered in this field must be less than the value entered in the Max Value field. If you want spaces to be a valid value, you must leave this field blank or write the word spaces in this field.',
        moreInfo:
          'Client-defined minimum value allowed for this element. The value entered in this field must be less than the value entered in the Max Value field. If you want spaces to be a valid value, you must leave this field blank or write the word spaces in this field.'
      },
      {
        id: ElementsFieldParameterEnum.MaxValue,
        fieldName: ElementsFieldNameEnum.MaxValue,
        fieldType: ElementTypeEnum.Required,
        moreInfoText:
          'Client-defined maximum value allowed for this element. The value entered in this field must be greater than the value entered in the Min Value field.',
        moreInfo:
          'Client-defined maximum value allowed for this element. The value entered in this field must be greater than the value entered in the Min Value field.'
      },
      {
        id: ElementsFieldParameterEnum.ValidValues,
        fieldName: ElementsFieldNameEnum.ValidValues,
        fieldType: ElementTypeEnum.Optional,
        moreInfoText:
          'Exact values allowed for this element. If you use this field, valid values must be within the range entered in the Min Value and Max Value fields. You may enter a maximum of 24 valid values, separated by semicolons.',
        moreInfo:
          'Exact values allowed for this element. If you use this field, valid values must be within the range entered in the Min Value and Max Value fields. You may enter a maximum of 24 valid values, separated by semicolons.'
      },
      {
        id: ElementsFieldParameterEnum.DefaultValue,
        fieldName: ElementsFieldNameEnum.DefaultValue,
        fieldType: ElementTypeEnum.Required,
        moreInfoText:
          'Client-defined default value for an element. The default value must be between the minimum and maximum value. If you have defined values in the Valid Values field, the default value must be a valid value. If you want spaces to be a valid value, you must leave this field blank or write the word spaces in this field.',
        moreInfo:
          'Client-defined default value for an element. The default value must be between the minimum and maximum value. If you have defined values in the Valid Values field, the default value must be a valid value. If you want spaces to be a valid value, you must leave this field blank or write the word spaces in this field.'
      },
      {
        id: ElementsFieldParameterEnum.LetterFormat,
        fieldName: ElementsFieldNameEnum.LetterFormat,
        fieldType: ElementTypeEnum.Optional,
        moreInfoText:
          'Code representing the presentation layout of the data element value within the DataLink and the Fiserv® Cardholder Letters System when &DV is used. This field defaults to A101 for numeric elements, C101 for alphanumeric elements, or D101 for date elements, depending on the element type.',
        moreInfo:
          'Code representing the presentation layout of the data element value within the DataLink and the Fiserv® Cardholder Letters System when &DV is used. This field defaults to A101 for numeric elements, C101 for alphanumeric elements, or D101 for date elements, depending on the element type.'
      },
      {
        id: ElementsFieldParameterEnum.UseDefaultOnLetter,
        fieldName: ElementsFieldNameEnum.UseDefaultOnLetter,
        fieldType: ElementTypeEnum.Optional,
        moreInfoText:
          'Indicator designating whether to use the default value on the cardholder letter if a DataLink value is not present for the account.',
        moreInfo:
          'Indicator designating whether to use the default value on the cardholder letter if a DataLink value is not present for the account.'
      },
      {
        id: ElementsFieldParameterEnum.OCSTran,
        fieldName: ElementsFieldNameEnum.OCSTran,
        fieldType: ElementTypeEnum.Optional,
        moreInfoText:
          'Client-defined identifier of the transaction class to which you want to assign this element. You must enter +D before the 4-position class codes. You cannot enter value +D0000 in this field.',
        moreInfo:
          'Client-defined identifier of the transaction class to which you want to assign this element. You must enter +D before the 4-position class codes. You cannot enter value +D0000 in this field.'
      },
      {
        id: ElementsFieldParameterEnum.MaskText,
        fieldName: ElementsFieldNameEnum.MaskText,
        fieldType: ElementTypeEnum.Optional,
        moreInfoText:
          'Text describing how a DataLink element value will appear on the HDV and HDI screens.',
        moreInfo:
          'Text describing how a DataLink element value will appear on the HDV and HDI screens.'
      },
      {
        id: ElementsFieldParameterEnum.RealTimeUpdate,
        fieldName: ElementsFieldNameEnum.RealTimeUpdate,
        fieldType: ElementTypeEnum.Optional,
        moreInfoText:
          'Indicator designating whether a DataLink element is available for online real time updating.',
        moreInfo:
          'Indicator designating whether a DataLink element is available for online real time updating.'
      },
      {
        id: ElementsFieldParameterEnum.RetainOnTransfer,
        fieldName: ElementsFieldNameEnum.RetainOnTransfer,
        fieldType: ElementTypeEnum.Optional,
        moreInfoText:
          'Indicator designating whether you want to retain the value for the displayed element on the From account during an account transfer.',
        moreInfo:
          'Indicator designating whether you want to retain the value for the displayed element on the From account during an account transfer.'
      }
    ]);
  });
});
