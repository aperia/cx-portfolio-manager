import userEvent from '@testing-library/user-event';
import { WorkflowMetadataList } from 'app/fixtures/workflow-metadata';
import { renderWithMockStore } from 'app/utils';
import 'app/utils/_mockComponent/mockModalRegistry';
import 'app/utils/_mockComponent/mockUseTranslation';
import mockDevice from 'app/utils/_mockHelperConstant/mockDevice';
import 'app/_libraries/_dls/test-utils/mocks/mockCanvas';
import * as CommonRedux from 'pages/_commons/redux/Common/select-hooks';
import * as workflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowCustomDataLinkElements';
import React from 'react';
import AddNewElementModal from './AddNewElementModal';
const mockOnConfirmParams = jest.fn();

const mockUseSelectWindowDimension = jest.spyOn(
  CommonRedux,
  'useSelectWindowDimension'
);
jest.mock('app/hooks/useUnsavedChangesRedirect', () => {
  return {
    __esModule: true,
    useUnsavedChangesRedirect:
      () =>
      ({ onConfirm, onDiscard, onCancel }: any) => {
        onDiscard && onDiscard();
        onCancel && onCancel();
        onConfirm && onConfirm(mockOnConfirmParams());
      }
  };
});

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const mockUseSelectMetadataForCustomDataLinkElements = jest.spyOn(
  workflowSetup,
  'useSelectMetadataForCustomDataLinkElements'
);
const mockUseSelectWindowDimensionImplementation = (width: number) => {
  mockUseSelectWindowDimension.mockImplementation(() => ({ width } as any));
};

describe('pages > WorkflowCustomDataLinkElements > CustomDataLinkElementsStep > AddNewElementModal', () => {
  const onClose = jest.fn();
  const defaultProps = {
    id: 'AddNewElementModal',
    show: true,
    onClose
  };

  beforeEach(() => {
    mockUseSelectWindowDimensionImplementation(120);
  });

  afterEach(() => {
    mockUseSelectWindowDimension.mockClear();
  });

  const element = {
    id: 1,
    elementName: 'CA',
    elementId: 'AA',
    bottomName: 'BB',
    parameters: [{ name: 'element.name', value: 'CA' }]
  };

  const getInitialState = ({ elements = WorkflowMetadataList }: any = {}) => ({
    workflowSetup: { elementMetadata: { elements } }
  });

  beforeEach(() => {
    const metadata: any = {
      elementType: [{ code: 'code', text: 'None' }],
      deltaType: [{ code: 'code', text: 'None' }],
      useDefaultOnLetter: [{ code: 'code', text: 'None' }],
      realTimeUpdate: [{ code: 'code', text: 'None' }],
      reallocation: [{ code: 'code', text: 'None' }],
      retainOnTransfer: [{ code: 'code', text: 'None' }],
      transferTypes: [{ code: 'code', text: 'None' }],
      letterFormat: [{ code: 'code', text: 'None' }]
    };
    mockUseSelectMetadataForCustomDataLinkElements.mockReturnValue(metadata);
    mockDevice.isDevice = false;
    mockOnConfirmParams.mockReturnValue('');
  });

  afterEach(() => {
    onClose.mockClear();
  });

  it('should render AddNewElementModal with add modal with draft element', async () => {
    mockDevice.isDevice = true;
    const wrapper = await renderWithMockStore(
      <AddNewElementModal {...defaultProps} draftElement={element as any} />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByText('txt_custom_data_link_elements_add_modal_name')
    ).toBeInTheDocument();

    userEvent.click(wrapper.getByText('txt_cancel'));
    expect(onClose).toBeCalled();
  });

  it('should render AddNewElementModal with add modal', async () => {
    mockDevice.isDevice = true;
    const wrapper = await renderWithMockStore(
      <AddNewElementModal {...defaultProps} />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByText('txt_custom_data_link_elements_add_modal_name')
    ).toBeInTheDocument();

    userEvent.click(wrapper.getByText('txt_cancel'));
    expect(onClose).toBeCalled();
  });

  it('should render AddNewElementModal with edit modal', async () => {
    mockDevice.isDevice = true;
    const wrapper = await renderWithMockStore(
      <AddNewElementModal {...defaultProps} isCreate={false} />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByText('txt_custom_data_link_elements_edit_modal_name')
    ).toBeInTheDocument();

    userEvent.click(wrapper.getByText('txt_cancel'));
    expect(onClose).toBeCalled();
  });

  it('should render ConfigureParameters and click search bar', async () => {
    mockUseSelectWindowDimensionImplementation(1400);
    const wrapper = await renderWithMockStore(
      <AddNewElementModal {...defaultProps} />,
      { initialState: getInitialState() }
    );
    const text = wrapper.getByPlaceholderText('txt_type_to_search');

    userEvent.type(text, 'A {enter}');
    expect(text).toBeInTheDocument;

    const resetBtn = wrapper.getByText('txt_clear_and_reset');

    expect(resetBtn).toBeInTheDocument();
    userEvent.click(resetBtn);
  });

  it('should render ConfigureParameters and click search bar and have empty data', async () => {
    const wrapper = await renderWithMockStore(
      <AddNewElementModal {...defaultProps} isCreate={true} />,
      { initialState: getInitialState() }
    );
    const text = wrapper.getByPlaceholderText('txt_type_to_search');

    userEvent.type(text, 'asqwefs {enter}');
    expect(text).toBeInTheDocument;

    const resetBtn = wrapper.getByText('txt_clear_and_reset');

    expect(resetBtn).toBeInTheDocument();
    userEvent.click(resetBtn);
  });

  it('should render AddNewElementModal with add modal and click add btn', async () => {
    mockDevice.isDevice = true;
    const wrapper = await renderWithMockStore(
      <AddNewElementModal {...defaultProps} />,
      { initialState: getInitialState() }
    );
    expect(wrapper.getByText('txt_add')).toBeInTheDocument();

    userEvent.click(wrapper.getByText('txt_add'));
    expect(wrapper.getByText('txt_add')).toBeInTheDocument();
  });
  it('should render AddNewElementModal with add modal and click edit btn', async () => {
    mockDevice.isDevice = true;
    const wrapper = await renderWithMockStore(
      <AddNewElementModal {...defaultProps} isCreate={false} />,
      { initialState: getInitialState() }
    );
    expect(wrapper.getByText('txt_save')).toBeInTheDocument();

    userEvent.click(wrapper.getByText('txt_save'));
    expect(wrapper.getByText('txt_save')).toBeInTheDocument();
  });

  it('should render AddNewElementModal with add modal and input fields ElementID', async () => {
    mockDevice.isDevice = true;
    const wrapper = await renderWithMockStore(
      <AddNewElementModal {...defaultProps} />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByTestId('addNewElement__elementID_dls-text-box_input')
    ).toBeInTheDocument();

    userEvent.type(
      wrapper.getByTestId('addNewElement__elementID_dls-text-box_input'),
      'ab'
    );
    expect(
      wrapper.getByTestId('addNewElement__elementID_dls-text-box_input')
    ).toBeInTheDocument();
  });

  it('should render AddNewElementModal with add modal and input fields ElementID and invalid', async () => {
    mockDevice.isDevice = true;
    const wrapper = await renderWithMockStore(
      <AddNewElementModal {...defaultProps} />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByTestId('addNewElement__elementID_dls-text-box_input')
    ).toBeInTheDocument();

    userEvent.type(
      wrapper.getByTestId('addNewElement__elementID_dls-text-box_input'),
      '@#@31'
    );
    expect(
      wrapper.getByTestId('addNewElement__elementID_dls-text-box_input')
    ).toBeInTheDocument();
  });

  it('should render AddNewElementModal with add modal and input fields elementName', async () => {
    mockDevice.isDevice = true;
    const wrapper = await renderWithMockStore(
      <AddNewElementModal {...defaultProps} />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByTestId('addNewElement__elementName_dls-text-box_input')
    ).toBeInTheDocument();

    userEvent.type(
      wrapper.getByTestId('addNewElement__elementName_dls-text-box_input'),
      'ab'
    );
    expect(
      wrapper.getByTestId('addNewElement__elementName_dls-text-box_input')
    ).toBeInTheDocument();
  });

  it('should render AddNewElementModal with add modal and input fields elementLength', async () => {
    mockDevice.isDevice = true;
    const wrapper = await renderWithMockStore(
      <AddNewElementModal {...defaultProps} />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByTestId('addNewElement__elementLength_dls-text-box_input')
    ).toBeInTheDocument();

    userEvent.type(
      wrapper.getByTestId('addNewElement__elementLength_dls-text-box_input'),
      '1'
    );
    expect(
      wrapper.getByTestId('addNewElement__elementLength_dls-text-box_input')
    ).toBeInTheDocument();
  });
  it('should render AddNewElementModal with add modal and input fields elementLength and input invalid', async () => {
    mockDevice.isDevice = true;
    const wrapper = await renderWithMockStore(
      <AddNewElementModal {...defaultProps} />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByTestId('addNewElement__elementLength_dls-text-box_input')
    ).toBeInTheDocument();

    userEvent.type(
      wrapper.getByTestId('addNewElement__elementLength_dls-text-box_input'),
      'AAA'
    );
    expect(
      wrapper.getByTestId('addNewElement__elementLength_dls-text-box_input')
    ).toBeInTheDocument();
  });
  it('should render AddNewElementModal with add modal and input fields topName', async () => {
    mockDevice.isDevice = true;
    const wrapper = await renderWithMockStore(
      <AddNewElementModal {...defaultProps} />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByTestId('addNewElement__topName_dls-text-box_input')
    ).toBeInTheDocument();

    userEvent.type(
      wrapper.getByTestId('addNewElement__topName_dls-text-box_input'),
      '1'
    );
    expect(
      wrapper.getByTestId('addNewElement__topName_dls-text-box_input')
    ).toBeInTheDocument();
  });

  it('should render AddNewElementModal with add modal and onChange fields elementType', async () => {
    mockDevice.isDevice = true;

    const wrapper = await renderWithMockStore(
      <AddNewElementModal {...defaultProps} />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByTestId('addNewElement__elementType_dls-dropdown-list')
    ).toBeInTheDocument();
  });

  it('should render AddNewElementModal with add modal and onChange fields validValues', async () => {
    mockDevice.isDevice = true;

    const wrapper = await renderWithMockStore(
      <AddNewElementModal {...defaultProps} />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByTestId('addNewElement__validValues_dls-text-area_input')
    ).toBeInTheDocument();

    userEvent.type(
      wrapper.getByTestId('addNewElement__validValues_dls-text-area_input'),
      '1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;'
    );
    expect(
      wrapper.getByTestId('addNewElement__validValues_dls-text-area_input')
    ).toBeInTheDocument();
    expect(wrapper.getByText('txt_add')).toBeInTheDocument();

    userEvent.click(wrapper.getByText('txt_add'));
  });
});
