import { ElementsFieldParameterEnum } from 'app/constants/enums';
import { getWorkflowSetupStepStatus } from 'app/helpers';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import { CustomDataLinkElementsFormValue } from '.';

const parseFormValues: WorkflowSetupStepFormDataFunc<CustomDataLinkElementsFormValue> =
  (data, id) => {
    const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);
    if (!stepInfo) return;

    const now = Date.now();

    const dataLinkElements = (
      data?.data?.configurations?.dataLinkElements || []
    ).map((el: MagicKeyValue, index) => ({
      ...el,
      id: now + index,
      bottomName: el.parameters.find(
        (el: MagicKeyValue) => el.name === ElementsFieldParameterEnum.BottomName
      ).value,
      description: el.parameters.find(
        (el: MagicKeyValue) =>
          el.name === ElementsFieldParameterEnum.ElementBusinessDescription
      ).value,
      elementId: el.parameters.find(
        (el: MagicKeyValue) => el.name === ElementsFieldParameterEnum.ElementID
      ).value,
      elementName: el.parameters.find(
        (el: MagicKeyValue) =>
          el.name === ElementsFieldParameterEnum.ElementName
      ).value,
      topName: el.parameters.find(
        (el: MagicKeyValue) => el.name === ElementsFieldParameterEnum.TopName
      ).value
    }));

    const isValid = !isEmpty(dataLinkElements);

    return {
      ...getWorkflowSetupStepStatus(stepInfo),
      isValid,
      dataLinkElements
    };
  };

export default parseFormValues;
