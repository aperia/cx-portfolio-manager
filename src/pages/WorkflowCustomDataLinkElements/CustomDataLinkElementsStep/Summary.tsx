import { mapGridExpandCollapse } from 'app/helpers';
import { usePagination } from 'app/hooks/usePagination';
import { Button, ColumnType, Grid, useTranslation } from 'app/_libraries/_dls';
import { isEmpty, isFunction, orderBy } from 'app/_libraries/_dls/lodash';
import {
  formatText,
  formatTruncate
} from 'pages/_commons/Utils/formatGridField';
import Paging from 'pages/_commons/Utils/Paging';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { CustomDataLinkElementsFormValue } from '.';
import SubGrid from './SubGrid';

const Summary: React.FC<
  WorkflowSetupSummaryProps<CustomDataLinkElementsFormValue>
> = ({ formValues, onEditStep, stepId, selectedStep }) => {
  const { t } = useTranslation();

  const elements = useMemo(
    () => orderBy(formValues?.dataLinkElements || [], ['elementId'], ['asc']),
    [formValues?.dataLinkElements]
  );

  const [expandedList, setExpandedList] = useState<string[]>([]);

  const columns: ColumnType[] = useMemo(
    () => [
      {
        id: 'elementId',
        Header: t('txt_custom_data_link_elements_element_id'),
        accessor: formatText(['elementId']),
        width: 176
      },
      {
        id: 'elementName',
        Header: t('txt_custom_data_link_elements_element_name'),
        accessor: formatText(['elementName']),
        width: 175
      },
      {
        id: 'elementBusinessDescription',
        Header: t('txt_custom_data_link_elements_element_business_desc'),
        accessor: formatTruncate(['description'])
      }
    ],
    [t]
  );

  const {
    total,
    currentPage,
    currentPageSize,
    gridData,
    sort,
    onSortChange,
    onPageChange,
    onResetToDefaultFilter,
    onPageSizeChange
  } = usePagination(elements, [], 'elementId');

  useEffect(() => {
    if (stepId !== selectedStep) {
      onResetToDefaultFilter();
      setExpandedList([]);
    }
  }, [stepId, selectedStep, onResetToDefaultFilter]);

  const handleEdit = () => {
    isFunction(onEditStep) && onEditStep();
  };

  const handleOnExpand = (dataKeyList: string[]) => {
    setExpandedList(isEmpty(dataKeyList) ? [] : dataKeyList);
  };

  const subRow = useCallback((row: any) => {
    return <SubGrid row={row} />;
  }, []);

  return (
    <div className="position-relative">
      <div className="absolute-top-right mt-n26 mr-n8">
        <Button variant="outline-primary" size="sm" onClick={handleEdit}>
          {t('txt_edit')}
        </Button>
      </div>
      <div className="pt-16">
        <Grid
          togglable
          columns={columns}
          data={gridData}
          dataItemKey="id"
          subRow={subRow}
          toggleButtonConfigList={gridData.map(mapGridExpandCollapse('id', t))}
          expandedItemKey={'id'}
          expandedList={expandedList}
          onExpand={handleOnExpand}
          sortBy={[sort]}
          onSortChange={onSortChange}
        />
        {total > 10 && (
          <Paging
            totalItem={total}
            pageSize={currentPageSize}
            page={currentPage}
            onChangePage={onPageChange}
            onChangePageSize={onPageSizeChange}
          />
        )}
      </div>
    </div>
  );
};

export default Summary;
