import { Button, Icon, Tooltip, useTranslation } from 'app/_libraries/_dls';
import React from 'react';

interface ActionButtonProps {
  handleClickBtn: () => void;
  title: string;
  small: boolean;
  data: MagicKeyValue;
}

const ActionButton: React.FC<ActionButtonProps> = ({
  handleClickBtn,
  title,
  small,
  data
}) => {
  const { t } = useTranslation();
  const disableAddBtn = data?.length === 60;
  return (
    <>
      {small ? (
        disableAddBtn ? (
          <Tooltip
            element={t('txt_custom_data_link_elements_tool_tip_max_element')}
            placement="top"
          >
            <Button
              size="sm"
              variant="outline-primary"
              disabled={disableAddBtn}
              onClick={handleClickBtn}
            >
              {t(title)}
            </Button>
          </Tooltip>
        ) : (
          <Button
            size="sm"
            variant="outline-primary"
            disabled={disableAddBtn}
            onClick={handleClickBtn}
          >
            {t(title)}
          </Button>
        )
      ) : (
        <div className="col-4">
          <div
            className="d-flex align-items-center rcc-btn border rounded-lg px-16 py-12"
            onClick={handleClickBtn}
          >
            <Icon name="method-add" size="9x" className="color-grey-l16" />
            <span className="ml-12 mr-8">{t(title)}</span>
          </div>
        </div>
      )}
    </>
  );
};

export default ActionButton;
