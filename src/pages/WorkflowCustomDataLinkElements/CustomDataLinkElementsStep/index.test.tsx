import { renderWithMockStore } from 'app/utils';
import React from 'react';
import CustomDataLinkElementsStep from '.';

jest.mock('./ConfigureParameters', () => ({
  __esModule: true,
  default: () => <div>ConfigureParameters</div>
}));

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

const renderComponent = (props: any, initValues: any = {}) => {
  return renderWithMockStore(
    <CustomDataLinkElementsStep {...props} />,
    initValues
  );
};

describe('CustomDataLinkElementsStep', () => {
  it('Should render CustomDataLinkElementsStep contains Completed', async () => {
    const props = {};
    const wrapper = await renderComponent(props, {});

    expect(
      wrapper.getByText(
        'txt_custom_data_link_elements_header_config_parameters'
      )
    ).toBeInTheDocument;
  });
});
