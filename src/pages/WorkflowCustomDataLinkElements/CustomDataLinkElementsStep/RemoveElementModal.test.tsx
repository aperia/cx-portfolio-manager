import { fireEvent, render, RenderResult } from '@testing-library/react';
import { mockUseModalRegistryFnc } from 'app/utils';
import React from 'react';
import RemoveElementModal from './RemoveElementModal';

const mockUseModalRegistry = mockUseModalRegistryFnc();

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderModal = (props: any): RenderResult => {
  return render(
    <div>
      <RemoveElementModal {...props} />
    </div>
  );
};

describe('CustomDataLinkElementsWorkFlow > RemoveElementModal', () => {
  beforeEach(() => {
    mockUseModalRegistry.mockImplementation(() => [true, false]);
  });

  afterEach(() => {
    mockUseModalRegistry.mockClear();
  });

  it('Should render modal', () => {
    const props = {
      id: '1',
      show: true,
      onClose: jest.fn()
    };

    const wrapper = renderModal(props);
    expect(
      wrapper.getByText('txt_custom_data_link_elements_delete_msg')
    ).toBeInTheDocument();
  });

  it('Should close modal without delete', () => {
    const onClose = jest.fn(isDeleted => isDeleted);

    const props = {
      id: '1',
      show: true,
      onClose: onClose
    };

    const wrapper = renderModal(props);
    wrapper.debug();
    fireEvent.click(
      wrapper.baseElement.querySelector(
        'button[class="btn btn-secondary"]'
      ) as Element
    );
    expect(onClose.mock.results[0].value).toEqual(undefined);
  });

  it('Should close modal with deleted', () => {
    const onClose = jest.fn(isDeleted => isDeleted);

    const props = {
      id: '1',
      show: true,
      onClose: onClose
    };

    const wrapper = renderModal(props);
    wrapper.debug();
    fireEvent.click(
      wrapper.baseElement.querySelector(
        'button[class="btn btn-danger"]'
      ) as Element
    );
    expect(onClose.mock.results[0].value).toEqual(true);
  });
});
