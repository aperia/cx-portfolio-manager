import {
  ElementsFieldParameterEnum,
  ServiceSubjectSection
} from 'app/constants/enums';
import parseFormValues from './parseFormValues';

describe('pages > WorkflowCustomDataLinkElement > CustomDataLinkElementStep > parseFormValues', () => {
  it('parseFormValues', () => {
    let configurations: any = {
      dataLinkElements: [
        {
          parameters: [
            { name: ElementsFieldParameterEnum.BottomName, value: 'example' },
            {
              name: ElementsFieldParameterEnum.ElementBusinessDescription,
              value: 'example'
            },
            { name: ElementsFieldParameterEnum.ElementID, value: 'example' },
            { name: ElementsFieldParameterEnum.ElementName, value: 'example' },
            { name: ElementsFieldParameterEnum.TopName, value: 'example' }
          ]
        }
      ]
    };
    const id = 'id';
    const input: any = {
      data: {
        configurations,
        workflowSetupData: [{ id, status: 'INPROGRESS' }]
      }
    };

    // isvalid
    let result = parseFormValues(input, id, jest.fn);
    expect(result?.isValid).toEqual(true);

    // other step
    result = parseFormValues(input, 'id1', jest.fn);
    expect(result).toBeUndefined();

    configurations = {
      methodList: [
        {
          serviceSubjectSection: ServiceSubjectSection.DLO,
          name: 'name'
        }
      ]
    };
    input.data.configurations = configurations;

    result = parseFormValues(input, id, jest.fn);
    expect(result?.isValid).toEqual(false);
    expect(result?.dataLinkElements?.[0]).toEqual(undefined);
  });

  it('parseFormValues', () => {
    let configurations: any = {};
    const id = 'id';
    const input: any = {
      data: {
        configurations,
        workflowSetupData: [{ id, status: 'INPROGRESS' }]
      }
    };

    // isvalid
    let result = parseFormValues(input, id, jest.fn);
    expect(result?.isValid).toEqual(false);

    // other step
    result = parseFormValues(input, 'id1', jest.fn);
    expect(result).toBeUndefined();

    configurations = {
      methodList: [
        {
          serviceSubjectSection: ServiceSubjectSection.DLO,
          name: 'name'
        }
      ]
    };
    input.data.configurations = configurations;

    result = parseFormValues(input, id, jest.fn);
    expect(result?.isValid).toEqual(false);
    expect(result?.dataLinkElements?.[0]).toEqual(undefined);
  });

  it('parseFormValues', () => {
    let configurations: any = {};
    const id = 'id';
    const input: any = {
      data: {
        configurations,
        workflowSetupData: [{ id, status: 'INPROGRESS' }]
      }
    };

    // isvalid
    let result = parseFormValues(input, 'id2', jest.fn);
    expect(result?.isValid).toBeUndefined();

    // other step
    result = parseFormValues(input, 'id1', jest.fn);
    expect(result).toBeUndefined();

    configurations = {
      methodList: [
        {
          serviceSubjectSection: ServiceSubjectSection.DLO,
          name: 'name'
        }
      ]
    };
    input.data.configurations = configurations;

    result = parseFormValues(input, 'id2', jest.fn);
    expect(result?.isValid).toEqual(undefined);
    expect(result?.dataLinkElements?.[0]).toEqual(undefined);
  });
});
