import { render } from '@testing-library/react';
import { ElementsFieldParameterEnum } from 'app/constants/enums';
import { countMultiValues, valueAccessor } from './helper';
const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});
describe('helper CustomDataLinkElements', () => {
  it('should run countMultiValues function with parameters', () => {
    const value = countMultiValues('1;1;1;1;');

    expect(value).toEqual(4);
  });

  it('should run countMultiValues function without parameters', () => {
    const value = countMultiValues('');

    expect(value).toEqual(undefined);
  });
});

describe('should run valueAccessor', () => {
  const metadata = {
    elementType: [{ code: 'code', text: 'text' }],
    deltaType: [{ code: 'code', text: 'text' }],
    useDefaultOnLetter: [{ code: 'code', text: 'text' }],
    realTimeUpdate: [{ code: 'code', text: 'text' }],
    reallocation: [{ code: 'code', text: 'text' }],
    retainOnTransfer: [{ code: 'code', text: 'text' }],
    transferTypes: [{ code: 'code', text: 'text' }],
    letterFormat: [{ code: 'code', text: 'text' }]
  };
  const onChange = jest.fn();
  const elements = {};
  const error = {};

  it('should render ElementID', () => {
    const data = { id: ElementsFieldParameterEnum.ElementID };
    const element = valueAccessor(
      data,
      metadata as any,
      elements as any,
      onChange,
      t,
      error
    );

    const wrapper = render(element);

    expect(wrapper).toBeTruthy();
  });
  it('should render ElementName', () => {
    const data = { id: ElementsFieldParameterEnum.ElementName };
    const element = valueAccessor(
      data,
      metadata as any,
      elements as any,
      onChange,
      t,
      error
    );

    const wrapper = render(element);

    expect(wrapper).toBeTruthy();
  });
  it('should render TopName', () => {
    const data = { id: ElementsFieldParameterEnum.TopName };
    const element = valueAccessor(
      data,
      metadata as any,
      elements as any,
      onChange,
      t,
      error
    );

    const wrapper = render(element);

    expect(wrapper).toBeTruthy();
  });
  it('should render BottomName', () => {
    const data = { id: ElementsFieldParameterEnum.BottomName };
    const element = valueAccessor(
      data,
      metadata as any,
      elements as any,
      onChange,
      t,
      error
    );

    const wrapper = render(element);

    expect(wrapper).toBeTruthy();
  });
  it('should render Reallocation', () => {
    const data = { id: ElementsFieldParameterEnum.Reallocation };
    const element = valueAccessor(
      data,
      metadata as any,
      elements as any,
      onChange,
      t,
      error
    );

    const wrapper = render(element);

    expect(wrapper).toBeTruthy();
  });
  it('should render DeltaType', () => {
    const data = { id: ElementsFieldParameterEnum.DeltaType };
    const element = valueAccessor(
      data,
      metadata as any,
      elements as any,
      onChange,
      t,
      error
    );

    const wrapper = render(element);

    expect(wrapper).toBeTruthy();
  });
  it('should render TransferTypes', () => {
    const data = { id: ElementsFieldParameterEnum.TransferTypes };
    const element = valueAccessor(
      data,
      metadata as any,
      elements as any,
      onChange,
      t,
      error
    );

    const wrapper = render(element);

    expect(wrapper).toBeTruthy();
  });
  it('should render ElementType', () => {
    const data = { id: ElementsFieldParameterEnum.ElementType };
    const element = valueAccessor(
      data,
      metadata as any,
      elements as any,
      onChange,
      t,
      error
    );

    const wrapper = render(element);

    expect(wrapper).toBeTruthy();
  });
  it('should render ElementLength', () => {
    const data = { id: ElementsFieldParameterEnum.ElementLength };
    const element = valueAccessor(
      data,
      metadata as any,
      elements as any,
      onChange,
      t,
      error
    );

    const wrapper = render(element);

    expect(wrapper).toBeTruthy();
  });
  it('should render NumberOfDecimals', () => {
    const data = { id: ElementsFieldParameterEnum.NumberOfDecimals };
    const element = valueAccessor(
      data,
      metadata as any,
      elements as any,
      onChange,
      t,
      error
    );

    const wrapper = render(element);

    expect(wrapper).toBeTruthy();
  });
  it('should render MinValue', () => {
    const data = { id: ElementsFieldParameterEnum.MinValue };
    const element = valueAccessor(
      data,
      metadata as any,
      elements as any,
      onChange,
      t,
      error
    );

    const wrapper = render(element);

    expect(wrapper).toBeTruthy();
  });
  it('should render MaxValue', () => {
    const data = { id: ElementsFieldParameterEnum.MaxValue };
    const element = valueAccessor(
      data,
      metadata as any,
      elements as any,
      onChange,
      t,
      error
    );

    const wrapper = render(element);

    expect(wrapper).toBeTruthy();
  });
  it('should render ValidValues', () => {
    const data = { id: ElementsFieldParameterEnum.ValidValues };
    const element = valueAccessor(
      data,
      metadata as any,
      elements as any,
      onChange,
      t,
      error
    );

    const wrapper = render(element);

    expect(wrapper).toBeTruthy();
  });
  it('should render DefaultValue', () => {
    const data = { id: ElementsFieldParameterEnum.DefaultValue };
    const element = valueAccessor(
      data,
      metadata as any,
      elements as any,
      onChange,
      t,
      error
    );

    const wrapper = render(element);

    expect(wrapper).toBeTruthy();
  });
  it('should render LetterFormat', () => {
    const data = { id: ElementsFieldParameterEnum.LetterFormat };
    const element = valueAccessor(
      data,
      metadata as any,
      elements as any,
      onChange,
      t,
      error
    );

    const wrapper = render(element);

    expect(wrapper).toBeTruthy();
  });
  it('should render UseDefaultOnLetter', () => {
    const data = { id: ElementsFieldParameterEnum.UseDefaultOnLetter };
    const element = valueAccessor(
      data,
      metadata as any,
      elements as any,
      onChange,
      t,
      error
    );

    const wrapper = render(element);

    expect(wrapper).toBeTruthy();
  });
  it('should render OCSTran', () => {
    const data = { id: ElementsFieldParameterEnum.OCSTran };
    const element = valueAccessor(
      data,
      metadata as any,
      elements as any,
      onChange,
      t,
      error
    );

    const wrapper = render(element);

    expect(wrapper).toBeTruthy();
  });
  it('should render MaskText', () => {
    const data = { id: ElementsFieldParameterEnum.MaskText };
    const element = valueAccessor(
      data,
      metadata as any,
      elements as any,
      onChange,
      t,
      error
    );

    const wrapper = render(element);

    expect(wrapper).toBeTruthy();
  });
  it('should render RealTimeUpdate', () => {
    const data = { id: ElementsFieldParameterEnum.RealTimeUpdate };
    const element = valueAccessor(
      data,
      metadata as any,
      elements as any,
      onChange,
      t,
      error
    );

    const wrapper = render(element);

    expect(wrapper).toBeTruthy();
  });
  it('should render RetainOnTransfer', () => {
    const data = { id: ElementsFieldParameterEnum.RetainOnTransfer };
    const element = valueAccessor(
      data,
      metadata as any,
      elements as any,
      onChange,
      t,
      error
    );

    const wrapper = render(element);

    expect(wrapper).toBeTruthy();
  });
  it('should render ElementBusinessDescription', () => {
    const data = { id: ElementsFieldParameterEnum.ElementBusinessDescription };
    const element = valueAccessor(
      data,
      metadata as any,
      elements as any,
      onChange,
      t,
      error
    );

    const wrapper = render(element);

    expect(wrapper).toBeTruthy();
  });
});
