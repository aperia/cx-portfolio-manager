import { ElementsFieldParameterEnum } from 'app/constants/enums';
import { matchSearchValue } from 'app/helpers';
import {
  ColumnType,
  Grid,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import { isUndefined } from 'lodash';
import { useSelectMetadataForCustomDataLinkElements } from 'pages/_commons/redux/WorkflowSetup';
import {
  valueTranslation,
  viewMoreInfo
} from 'pages/_commons/Utils/formatGridField';
import React, { useCallback, useMemo } from 'react';
import { MetadataProps } from './helper';
import { parameterList } from './newElementParameterList';

interface IProps {
  row: any;
  searchValue?: string;
}
const SubGrid: React.FC<IProps> = ({ row: { original }, searchValue }) => {
  const { t } = useTranslation();

  const metadata: MetadataProps = useSelectMetadataForCustomDataLinkElements();

  const {
    elementType,
    deltaType,
    useDefaultOnLetter,
    realTimeUpdate,
    reallocation,
    retainOnTransfer,
    transferTypes,
    letterFormat
  } = metadata;

  const dropdownMetadata: any = useMemo(
    () => ({
      [ElementsFieldParameterEnum.ElementType]: elementType,
      [ElementsFieldParameterEnum.DeltaType]: deltaType,
      [ElementsFieldParameterEnum.UseDefaultOnLetter]: useDefaultOnLetter,
      [ElementsFieldParameterEnum.RealTimeUpdate]: realTimeUpdate,
      [ElementsFieldParameterEnum.Reallocation]: reallocation,
      [ElementsFieldParameterEnum.RetainOnTransfer]: retainOnTransfer,
      [ElementsFieldParameterEnum.TransferTypes]: transferTypes,
      [ElementsFieldParameterEnum.LetterFormat]: letterFormat
    }),
    [
      elementType,
      deltaType,
      useDefaultOnLetter,
      realTimeUpdate,
      reallocation,
      retainOnTransfer,
      transferTypes,
      letterFormat
    ]
  );

  const data = useMemo(
    () =>
      parameterList.filter(g =>
        matchSearchValue([g.fieldName, g.moreInfoText], searchValue)
      ),
    [searchValue]
  );

  const parameters = original?.parameters;

  const getValue = useCallback(
    (rowData: Record<string, any>) => {
      const selectData = parameters.find(
        (param: MagicKeyValue) => param?.name === rowData?.id
      );

      const value = selectData?.value;

      const dropdownValue = dropdownMetadata?.[rowData.id]?.find(
        (el: MagicKeyValue) => el?.code === value
      )?.text;

      if (selectData.name === ElementsFieldParameterEnum.ValidValues) {
        return value
          .split(';')
          .filter((el: string) => el.trim())
          .join('; ');
      }

      if (!isUndefined(dropdownMetadata?.[rowData.id])) {
        if (dropdownValue === 'None') return '';
        return dropdownValue;
      }

      return value;
    },
    [parameters, dropdownMetadata]
  );

  const valueAccessor = useCallback(
    (rowData: Record<string, any>) => {
      return (
        <TruncateText
          lines={2}
          resizable
          ellipsisLessText={t('txt_less')}
          ellipsisMoreText={t('txt_more')}
        >
          {getValue(rowData)}
        </TruncateText>
      );
    },
    [t, getValue]
  );

  const columns: ColumnType[] = useMemo(
    () => [
      {
        id: 'fieldName',
        Header: t('txt_field_name'),
        accessor: valueTranslation(['fieldName'], t)
      },
      {
        id: 'value',
        Header: t('txt_value'),
        accessor: valueAccessor
      },
      {
        id: 'moreInfo',
        Header: t('txt_more_info'),
        className: 'text-center',
        accessor: viewMoreInfo(['moreInfo'], t),
        width: 106
      }
    ],
    [t, valueAccessor]
  );

  return (
    <div className="p-20">
      <Grid columns={columns} data={data} />
    </div>
  );
};

export default SubGrid;
