import { render } from '@testing-library/react';
import React from 'react';
import ActionButton from './ActionButton';

const handleClickBtn = jest.fn();
describe('ActionButton', () => {
  it('should render ActionButton with props and small is true', () => {
    const wrapper = render(
      <ActionButton
        title="example"
        small={true}
        data={{}}
        handleClickBtn={handleClickBtn}
      />
    );

    expect(wrapper.getByText('example')).toBeInTheDocument();
  });

  it('should render ActionButton with props and small is false', () => {
    const wrapper = render(
      <ActionButton
        title="example"
        small={false}
        data={{}}
        handleClickBtn={handleClickBtn}
      />
    );

    expect(wrapper.getByText('example')).toBeInTheDocument();
  });
});
