import ModalRegistry from 'app/components/ModalRegistry';
import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import { ElementsFieldParameterEnum } from 'app/constants/enums';
import { ELEMENT_FIELDS, ELEMENT_FIELDS_DEFAULT } from 'app/constants/mapping';
import {
  unsavedChangesProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { matchSearchValue, stringValidate } from 'app/helpers';
import {
  elementParamsToObject,
  objectToElementParams
} from 'app/helpers/elementParamsConvert';
import { useUnsavedChangeRegistry, useUnsavedChangesRedirect } from 'app/hooks';
import {
  ColumnType,
  Grid,
  InlineMessage,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty, isEqual, isFunction } from 'app/_libraries/_dls/lodash';
import { isValid } from 'pages/WorkflowManageAccountDelinquency/ConfigureParametersStep/parseFormValues';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import { useSelectMetadataForCustomDataLinkElements } from 'pages/_commons/redux/WorkflowSetup';
import ClearAndResetButton from 'pages/_commons/Utils/ClearAndResetButton';
import {
  valueTranslation,
  viewMoreInfo
} from 'pages/_commons/Utils/formatGridField';
import React, { useEffect, useMemo, useRef, useState } from 'react';
import {
  ALPHA_NUMBER_ELEMENT,
  countMultiValues,
  DROPDOWN_ELEMENT,
  MetadataProps,
  NUMBER_ELEMENT,
  valueAccessor
} from './helper';
import { parameterList } from './newElementParameterList';

interface AddNewElementModalProps {
  id: string;
  isCreate?: boolean;
  show: boolean;
  onClose?: (element?: WorkflowSetupElement) => void;
  draftElement?: WorkflowSetupElement & { id?: number };
  element?: WorkflowSetupElement & { id?: number };
}

const AddNewElementModal: React.FC<AddNewElementModalProps> = ({
  id,
  show,
  isCreate = true,
  element: elementProp = {} as WorkflowSetupElement,
  draftElement,
  onClose
}) => {
  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();

  const metadata: MetadataProps = useSelectMetadataForCustomDataLinkElements();

  const redirect = useUnsavedChangesRedirect();

  const [searchValue, setSearchValue] = useState('');

  const [errors, setErrors] = useState<MagicKeyValue>({});

  const [initialElement, setInitialElement] = useState<
    WorkflowSetupElementObjectParams & { id?: number }
  >(elementParamsToObject(elementProp));

  const [elements, setElements] = useState<WorkflowSetupElementObjectParams>(
    draftElement ? elementParamsToObject(draftElement) : initialElement
  );

  const modalTitle = isCreate
    ? t('txt_custom_data_link_elements_add_modal_name')
    : t('txt_custom_data_link_elements_edit_modal_name');

  const buttonName = isCreate ? t('txt_add') : t('txt_save');

  const parameters = useMemo(
    () =>
      parameterList.filter(g =>
        matchSearchValue(
          [g.fieldName, g.moreInfoText, g.fieldType],
          searchValue
        )
      ),
    [searchValue]
  );

  const errorValidation = () => {
    let currentError = {};

    currentError = {
      validValues: countMultiValues(
        elements[ElementsFieldParameterEnum.ValidValues] || ''
      ) > 24 && {
        message: t('txt_valid_values_err_msg'),
        status: true
      }
    };

    return currentError;
  };

  useUnsavedChangeRegistry(
    {
      ...unsavedChangesProps,
      formName: UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CUSTOM_DATA_LINK_ELEMENTS,
      priority: 1
    },
    [!isEqual(initialElement, elements)]
  );

  useEffect(() => {
    let defaultParamObj: Record<string, string> = {};

    if (!isCreate) return;

    const paramMapping = Object.keys(ELEMENT_FIELDS_DEFAULT).reduce(
      (pre, next) => {
        pre[next] = ELEMENT_FIELDS_DEFAULT[next];
        return pre;
      },
      {} as Record<string, string>
    );
    defaultParamObj = Object.assign({}, paramMapping);
    setElements(defaultParamObj as any);
    setInitialElement(defaultParamObj as any);
  }, [isCreate]);

  const handleFormChange =
    (field: keyof WorkflowSetupElementObjectParams, isRefData?: boolean) =>
    (e: any) => {
      let value = isRefData ? e.target?.value?.code : e.target?.value;

      if (field === ElementsFieldParameterEnum.ElementID) {
        value = value.toUpperCase();
      }

      if (
        NUMBER_ELEMENT.includes(field as ElementsFieldParameterEnum) &&
        !stringValidate(value).isNumber()
      ) {
        return false;
      }

      if (
        ALPHA_NUMBER_ELEMENT.includes(field as ElementsFieldParameterEnum) &&
        !stringValidate(value).isAlphanumeric()
      ) {
        return false;
      }

      setElements(element => ({ ...element, [field]: value }));
    };

  const columns: ColumnType[] = useMemo(
    () => [
      {
        id: 'fieldName',
        Header: t('txt_field_name'),
        accessor: valueTranslation(['fieldName'], t),
        cellBodyProps: { className: 'pt-12 pb-8' }
      },
      {
        id: 'fieldType',
        Header: t('txt_field_type'),
        accessor: valueTranslation(['fieldType'], t),
        cellBodyProps: { className: 'pt-12 pb-8' }
      },
      {
        id: 'value',
        Header: t('txt_value'),
        accessor: data =>
          valueAccessor(data, metadata, elements, handleFormChange, t, errors),
        cellBodyProps: { className: 'py-8' },
        width: width < 1280 ? 260 : undefined
      },
      {
        id: 'moreInfo',
        Header: t('txt_more_info'),
        className: 'text-center',
        accessor: viewMoreInfo(['moreInfo'], t),
        width: 106,
        cellBodyProps: { className: 'pt-12 pb-8' }
      }
    ],
    [t, metadata, elements, errors, width]
  );

  const simpleSearchRef = useRef<any>(null);
  useEffect(() => {
    if (!searchValue && simpleSearchRef?.current) {
      simpleSearchRef.current.clear();
    }
  }, [searchValue]);

  const handleClose = () => {
    redirect({
      onConfirm: () => isFunction(onClose) && onClose(),
      formsWatcher: [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CUSTOM_DATA_LINK_ELEMENTS
      ]
    });
  };

  const handleAddElement = () => {
    const validate = errorValidation();

    setErrors(validate);

    if (
      !isEmpty(validate) &&
      Object.values(validate).some(err => typeof err === 'object')
    )
      return;

    const elementForm = objectToElementParams(
      [...ELEMENT_FIELDS],
      elements,
      DROPDOWN_ELEMENT
    ) as WorkflowSetupElement & {
      id?: number;
      topName?: string;
      bottomName?: string;
    };

    if (isCreate) {
      elementForm.id = Date.now();
    }
    elementForm.elementId =
      elements?.[ElementsFieldParameterEnum.ElementID] || '';
    elementForm.elementName =
      elements?.[ElementsFieldParameterEnum.ElementName] || '';
    elementForm.description =
      elements?.[ElementsFieldParameterEnum.ElementBusinessDescription] || '';
    elementForm.topName = elements?.[ElementsFieldParameterEnum.TopName] || '';
    elementForm.bottomName =
      elements?.[ElementsFieldParameterEnum.BottomName] || '';

    isFunction(onClose) && onClose(elementForm);
  };

  const handleSearch = (val: string) => {
    setSearchValue(val);
  };

  const handleClearFilter = () => {
    setSearchValue('');
  };

  return (
    <ModalRegistry lg id={id} show={show} onAutoClosedAll={handleClose}>
      <ModalHeader border closeButton onHide={handleClose}>
        <ModalTitle>{modalTitle}</ModalTitle>
      </ModalHeader>
      <ModalBody>
        <div>
          <div className="d-flex align-items-center justify-content-between mb-16">
            <h5>{t('txt_parameter_list')}</h5>
            <SimpleSearch
              ref={simpleSearchRef}
              placeholder={t('txt_type_to_search')}
              onSearch={handleSearch}
              popperElement={
                <>
                  <p className="color-grey">{t('txt_search_description')}</p>
                  <ul className="search-field-item list-unstyled">
                    <li className="mt-16">{t('txt_field_name')}</li>
                    <li className="mt-16">{t('txt_field_type')}</li>
                    <li className="mt-16">{t('txt_more_info')}</li>
                  </ul>
                </>
              }
            />
          </div>
          {!isEmpty(errors) && (
            <div className="mb-16">
              <InlineMessage variant="danger" className="mb-0 mt-8 mr-8">
                <TruncateText lines={2}>
                  {t('txt_valid_values_err_msg')}
                </TruncateText>
              </InlineMessage>
            </div>
          )}
          {searchValue && !isEmpty(parameters) && (
            <div className="d-flex justify-content-end mb-16 mr-n8">
              <ClearAndResetButton small onClearAndReset={handleClearFilter} />
            </div>
          )}
          {isEmpty(parameters) && (
            <div className="d-flex flex-column justify-content-center mt-40">
              <NoDataFound
                id="newMethod_notfound"
                hasSearch
                title={t('txt_no_results_found')}
                linkTitle={t('txt_clear_and_reset')}
                onLinkClicked={handleClearFilter}
              />
            </div>
          )}
          {!isEmpty(parameters) && <Grid columns={columns} data={parameters} />}
        </div>
      </ModalBody>
      <ModalFooter
        cancelButtonText={t('txt_cancel')}
        okButtonText={buttonName}
        onCancel={handleClose}
        onOk={handleAddElement}
        disabledOk={!isValid}
      ></ModalFooter>
    </ModalRegistry>
  );
};

export default AddNewElementModal;
