import { renderComponent } from 'app/utils';
import * as workflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowCustomDataLinkElements';
import React from 'react';
import { MetadataProps } from './helper';
import SubGrid from './SubGrid';
const t = (value: any) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

const mockUseSelectMetadataForCustomDataLinkElements = jest.spyOn(
  workflowSetup,
  'useSelectMetadataForCustomDataLinkElements'
);

describe('pages > WorkflowCustomDataLinkElements > CustomDataLinkElementsStep > SubGrid', () => {
  it('render', async () => {
    const row = {
      original: {
        parameters: [
          { name: 'element.type', value: '' },
          { name: 'element.id', value: 'A' },
          { name: 'element.name', value: 's' },
          { name: 'top.name', value: 'c' },
          { name: 'bottom.name', value: 'v' },
          { name: 'reallocation', value: ' ' },
          { name: 'delta.type', value: ' ' },
          { name: 'transfer.types', value: ' ' },
          { name: 'element.type', value: ' ' },
          { name: 'element.length', value: '' },
          { name: 'number.of.decimals', value: '' },
          { name: 'min.value', value: '' },
          { name: 'max.value', value: '' },
          { name: 'valid.values', value: '' },
          { name: 'default.value', value: '' },
          { name: 'letter.format', value: ' ' },
          { name: 'use.default.on.letter', value: ' ' },
          { name: 'ocs.tran', value: '+D0001' },
          { name: 'mask.text', value: '' },
          { name: 'real.time.update', value: ' ' },
          { name: 'retain.on.transfer', value: ' ' },
          { name: 'element.business.description', value: '' }
        ]
      }
    };

    const metadata: MetadataProps = {
      elementId: [{ code: 'code', text: 'text' }],
      elementBusinessDescription: [
        { code: 'code', text: 'text' },
        { code: '', text: '' }
      ],
      elementLength: [{ code: 'None', text: 'None' }],
      elementName: [{ code: 'code', text: 'text' }],
      elementType: [{ code: '', text: 'None' }],
      bottomName: [{ code: 'code', text: 'text' }],
      topName: [{ code: 'code', text: 'text' }],
      transferTypes: [{ code: 'code', text: 'None' }],
      defaultValue: [{ code: 'code', text: 'text' }],
      useDefaultOnLetter: [{ code: 'code', text: 'text' }],
      numberOfDecimals: [{ code: 'code', text: 'text' }],
      realTimeUpdate: [{ code: 'code', text: 'text' }],
      reallocation: [{ code: 'code', text: 'None' }],
      retainOnTransfer: [{ code: 'code', text: 'None' }],
      maskText: [{ code: 'code', text: 'None' }],
      minValue: [{ code: 'code', text: 'None' }],
      maxValue: [{ code: 'code', text: 'None' }],
      validValues: [{ code: 'code', text: 'None' }],
      ocsTran: [{ code: 'code', text: 'None' }],
      deltaType: [{ code: 'code', text: 'None' }],
      letterFormat: [{ code: 'code', text: 'None' }]
    };
    mockUseSelectMetadataForCustomDataLinkElements.mockReturnValue(metadata);

    const wrapper = await renderComponent('testId', <SubGrid row={row} />);

    expect(
      wrapper.getByText('Element Business Description')
    ).toBeInTheDocument();
  });

  it('render with empty dropdownValue', async () => {
    const row = {
      original: {
        parameters: [
          { name: 'element.type', value: '' },
          { name: 'element.id', value: 'A' },
          { name: 'element.name', value: 's' },
          { name: 'top.name', value: 'c' },
          { name: 'bottom.name', value: 'v' },
          { name: 'reallocation', value: ' ' },
          { name: 'delta.type', value: ' ' },
          { name: 'transfer.types', value: ' ' },
          { name: 'element.type', value: ' ' },
          { name: 'element.length', value: '' },
          { name: 'number.of.decimals', value: '' },
          { name: 'min.value', value: '' },
          { name: 'max.value', value: '' },
          { name: 'valid.values', value: '' },
          { name: 'default.value', value: '' },
          { name: 'letter.format', value: ' ' },
          { name: 'use.default.on.letter', value: ' ' },
          { name: 'ocs.tran', value: '+D0001' },
          { name: 'mask.text', value: '' },
          { name: 'real.time.update', value: ' ' },
          { name: 'retain.on.transfer', value: ' ' },
          { name: 'element.business.description', value: '' }
        ]
      }
    };

    const metadata: MetadataProps = {
      elementId: [{ code: 'code', text: 'text' }],
      elementBusinessDescription: [
        { code: 'code', text: 'text' },
        { code: '', text: '' }
      ],
      elementLength: [{ code: 'None', text: 'None' }],
      elementName: [{ code: 'code', text: 'text' }],
      elementType: [{ code: '', text: 'None' }],
      bottomName: [{ code: 'code', text: 'text' }],
      topName: [{ code: 'code', text: 'text' }],
      transferTypes: [{ code: 'code', text: 'None' }],
      defaultValue: [{ code: 'code', text: 'text' }],
      useDefaultOnLetter: [{ code: 'code', text: 'text' }],
      numberOfDecimals: [{ code: 'code', text: 'text' }],
      realTimeUpdate: [{ code: 'code', text: 'text' }],
      reallocation: [{ code: 'code', text: 'None' }],
      retainOnTransfer: [{ code: 'code', text: 'None' }],
      maskText: [{ code: 'code', text: 'None' }],
      minValue: [{ code: 'code', text: 'None' }],
      maxValue: [{ code: 'code', text: 'None' }],
      validValues: [{ code: 'code', text: 'None' }],
      ocsTran: [{ code: 'code', text: 'None' }],
      deltaType: [{ code: 'code', text: 'None' }],
      letterFormat: [{ code: 'code', text: 'None' }]
    };
    mockUseSelectMetadataForCustomDataLinkElements.mockReturnValue(metadata);

    const wrapper = await renderComponent('testId', <SubGrid row={row} />);

    expect(
      wrapper.getByText('Element Business Description')
    ).toBeInTheDocument();
  });
});
