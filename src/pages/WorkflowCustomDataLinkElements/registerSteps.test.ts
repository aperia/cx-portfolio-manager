import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { default as stepRegistry } from '../_commons/WorkflowSetup/registries/stepRegistry';

const registerFunc = jest.spyOn(stepRegistry, 'registerStep');

describe('CustomDataLinkElements > registerSteps', () => {
  it('Should register components', async () => {
    await import('./registerSteps');

    expect(registerFunc).toBeCalledWith(
      'GetStartedCustomDataLinkElements',
      expect.anything()
    );

    expect(registerFunc).toBeCalledWith(
      'CustomDataLinkElements',
      expect.anything(),
      [UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CUSTOM_DATA_LINK_ELEMENTS__PARAMETERS]
    );
  });
});
