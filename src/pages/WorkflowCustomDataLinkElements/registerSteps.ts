import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { stepRegistry } from '../_commons/WorkflowSetup/registries';
import CustomDataLinkElementsStep from './CustomDataLinkElementsStep';
import GettingStartStep from './GettingStartStep';

stepRegistry.registerStep('GetStartedCustomDataLinkElements', GettingStartStep);

stepRegistry.registerStep(
  'CustomDataLinkElements',
  CustomDataLinkElementsStep,
  [UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CUSTOM_DATA_LINK_ELEMENTS__PARAMETERS]
);
