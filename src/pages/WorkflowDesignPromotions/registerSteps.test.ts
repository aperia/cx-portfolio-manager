import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { default as stepRegistry } from '../_commons/WorkflowSetup/registries/stepRegistry';

const registerFunc = jest.spyOn(stepRegistry, 'registerStep');

describe('PerformMonetaryAdjustmentsWorkflow > registerSteps', () => {
  it('Should register components', async () => {
    await import('./registerSteps');

    expect(registerFunc).toBeCalledWith(
      'GetStartedDesignPromotions',
      expect.anything()
    );
    expect(registerFunc).toBeCalledWith(
      'DesignPromotionsMethod',
      expect.anything(),
      [UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__DESIGN_PROMOTIONS__PARAMETERS]
    );
  });
});
