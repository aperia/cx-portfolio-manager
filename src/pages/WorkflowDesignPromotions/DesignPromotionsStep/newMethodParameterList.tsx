import {
  MethodFieldNameEnum,
  MethodFieldParameterEnum
} from 'app/constants/enums';
import React from 'react';
import { MORE_INFO, ONLINE_PCF } from './constant';

export type ParameterListIds =
  | 'Info_Basic'
  | 'Info_InterestAssessmentFees'
  | 'Info_InterestOverrides'
  | 'Info_ReturnRevolveSettings'
  | 'Info_RetailPathRelatedSettings'
  | 'Info_StatementMessaging'
  | 'Info_AdvancedParameters';

export interface ParameterList {
  id: ParameterListIds;
  section:
    | 'txt_manage_penalty_fee_standard_parameters'
    | 'txt_manage_penalty_fee_advanced_parameters';
  parameterGroup:
    | ''
    | 'Basic Information'
    | 'Interest Assessment and Fees'
    | 'Interest Overrides'
    | 'Retail Path Related Settings'
    | 'Return-to-Revolve Settings'
    | 'Statement Messaging';
  moreInfo?: React.ReactNode;
}

export const parameterList: ParameterList[] = [
  {
    id: 'Info_Basic',
    section: 'txt_manage_penalty_fee_standard_parameters',
    parameterGroup: 'Basic Information'
  },
  {
    id: 'Info_InterestAssessmentFees',
    section: 'txt_manage_penalty_fee_standard_parameters',
    parameterGroup: 'Interest Assessment and Fees'
  },
  {
    id: 'Info_InterestOverrides',
    section: 'txt_manage_penalty_fee_standard_parameters',
    parameterGroup: 'Interest Overrides'
  },
  {
    id: 'Info_RetailPathRelatedSettings',
    section: 'txt_manage_penalty_fee_standard_parameters',
    parameterGroup: 'Retail Path Related Settings'
  },
  {
    id: 'Info_ReturnRevolveSettings',
    section: 'txt_manage_penalty_fee_standard_parameters',
    parameterGroup: 'Return-to-Revolve Settings'
  },
  {
    id: 'Info_StatementMessaging',
    section: 'txt_manage_penalty_fee_standard_parameters',
    parameterGroup: 'Statement Messaging'
  },
  {
    id: 'Info_AdvancedParameters',
    section: 'txt_manage_penalty_fee_advanced_parameters',
    parameterGroup: ''
  }
];

export interface ParameterGroup {
  id: MethodFieldParameterEnum;
  fieldName: MethodFieldNameEnum;
  onlinePCF: string;
  moreInfoText: string;
  moreInfo: React.ReactNode;
}

export const parameterGroup: Record<ParameterListIds, ParameterGroup[]> = {
  Info_Basic: [
    {
      id: MethodFieldParameterEnum.DesignPromotionBasicInformation1,
      fieldName: MethodFieldNameEnum.DesignPromotionBasicInformation1,
      onlinePCF: ONLINE_PCF.DesignPromotionBasicInformation1,
      moreInfoText: MORE_INFO.DesignPromotionBasicInformation1,
      moreInfo: MORE_INFO.DesignPromotionBasicInformation1
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionBasicInformation2,
      fieldName: MethodFieldNameEnum.DesignPromotionBasicInformation2,
      onlinePCF: ONLINE_PCF.DesignPromotionBasicInformation2,
      moreInfoText: MORE_INFO.DesignPromotionBasicInformation2,
      moreInfo: MORE_INFO.DesignPromotionBasicInformation2
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionBasicInformation3,
      fieldName: MethodFieldNameEnum.DesignPromotionBasicInformation3,
      onlinePCF: ONLINE_PCF.DesignPromotionBasicInformation3,
      moreInfoText: MORE_INFO.DesignPromotionBasicInformation3,
      moreInfo: MORE_INFO.DesignPromotionBasicInformation3
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionBasicInformation4,
      fieldName: MethodFieldNameEnum.DesignPromotionBasicInformation4,
      onlinePCF: ONLINE_PCF.DesignPromotionBasicInformation4,
      moreInfoText: MORE_INFO.DesignPromotionBasicInformation4,
      moreInfo: MORE_INFO.DesignPromotionBasicInformation4
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionBasicInformation5,
      fieldName: MethodFieldNameEnum.DesignPromotionBasicInformation5,
      onlinePCF: ONLINE_PCF.DesignPromotionBasicInformation5,
      moreInfoText: MORE_INFO.DesignPromotionBasicInformation5,
      moreInfo: MORE_INFO.DesignPromotionBasicInformation5
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionBasicInformation6,
      fieldName: MethodFieldNameEnum.DesignPromotionBasicInformation6,
      onlinePCF: ONLINE_PCF.DesignPromotionBasicInformation6,
      moreInfoText: MORE_INFO.DesignPromotionBasicInformation6,
      moreInfo: MORE_INFO.DesignPromotionBasicInformation6
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionBasicInformation7,
      fieldName: MethodFieldNameEnum.DesignPromotionBasicInformation7,
      onlinePCF: ONLINE_PCF.DesignPromotionBasicInformation7,
      moreInfoText: MORE_INFO.DesignPromotionBasicInformation7,
      moreInfo: MORE_INFO.DesignPromotionBasicInformation7
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionBasicInformation8,
      fieldName: MethodFieldNameEnum.DesignPromotionBasicInformation8,
      onlinePCF: ONLINE_PCF.DesignPromotionBasicInformation8,
      moreInfoText: MORE_INFO.DesignPromotionBasicInformation8,
      moreInfo: MORE_INFO.DesignPromotionBasicInformation8
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionBasicInformation9,
      fieldName: MethodFieldNameEnum.DesignPromotionBasicInformation9,
      onlinePCF: ONLINE_PCF.DesignPromotionBasicInformation9,
      moreInfoText: MORE_INFO.DesignPromotionBasicInformation9,
      moreInfo: (
        <div>
          <p>
            {`The Multi-Ticket Criteria parameter determines whether qualifying sales transactions from multiple days or promotional periods will post to the same promotional balance for the duration of a promotion. Only enter this field if Multi-ticket promotion is chosen in Promotion Type field.
When you hover over "related fields," you will see the following fields and the values in said fields:`}
          </p>
          <ul>
            <li>Promotional Cash Option Number of Days</li>
            <li>Promotional Cash Option Number of Months</li>
            <li>Promotional Cash Option Number of Cycles</li>
            <li>
              Promotional Cash Option End Date (do not display for choice zero)
            </li>
            <li>Promotional Interest Delay Number of Days</li>
            <li>Promotional Interest Delay Number of Months</li>
            <li>Promotional Interest Delay Number of Cycles</li>
            <li>
              Promotional Interest Delay End Date (do not display for choice
              zero)
            </li>
            <li>Promotional Minimum Delay Number of Days</li>
            <li>Promotional Minimum Delay Number of Months</li>
            <li>Promotional Minimum Delay Number of Cycles</li>
            <li>
              Promotional Minimum Delay End Date (do not display for choice
              zero)
            </li>
          </ul>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionBasicInformation10,
      fieldName: MethodFieldNameEnum.DesignPromotionBasicInformation10,
      onlinePCF: ONLINE_PCF.DesignPromotionBasicInformation10,
      moreInfoText: MORE_INFO.DesignPromotionBasicInformation10,
      moreInfo: MORE_INFO.DesignPromotionBasicInformation10
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionBasicInformation11,
      fieldName: MethodFieldNameEnum.DesignPromotionBasicInformation11,
      onlinePCF: ONLINE_PCF.DesignPromotionBasicInformation11,
      moreInfoText: MORE_INFO.DesignPromotionBasicInformation11,
      moreInfo: (
        <div>
          <p>
            {`The Balance Transfer Promotion Option parameter determines whether the System defines the promotion is defined as a balance transfer. IE: "if you transfer your balance over, we'll give you %% APR for X Months!"`}
          </p>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionBasicInformation12,
      fieldName: MethodFieldNameEnum.DesignPromotionBasicInformation12,
      onlinePCF: ONLINE_PCF.DesignPromotionBasicInformation12,
      moreInfoText: MORE_INFO.DesignPromotionBasicInformation12,
      moreInfo: MORE_INFO.DesignPromotionBasicInformation12
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionBasicInformation13,
      fieldName: MethodFieldNameEnum.DesignPromotionBasicInformation13,
      onlinePCF: ONLINE_PCF.DesignPromotionBasicInformation13,
      moreInfoText: MORE_INFO.DesignPromotionBasicInformation13,
      moreInfo: MORE_INFO.DesignPromotionBasicInformation13
    }
  ],
  Info_InterestAssessmentFees: [
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestAssessment1,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestAssessment1,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestAssessment1,
      moreInfoText: MORE_INFO.DesignPromotionInterestAssessment1,
      moreInfo: MORE_INFO.DesignPromotionInterestAssessment1
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestAssessment2,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestAssessment2,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestAssessment2,
      moreInfoText: MORE_INFO.DesignPromotionInterestAssessment2,
      moreInfo: (
        <div>
          <p>
            {`The Introductory Promotional Interest Rate parameter determines the interest rate for an introductory period - "for the first six months of spend within the cardholder's promotional terms, the interest on the balance is 8%."
The Intro Annual Rate parameter determines the interest rate for transactions during a time period you specify in:`}
          </p>
          <ul>
            <li>Promotional Interest Delay Number of Days</li>
            <li>Promotional Interest Delay Number of Cycles</li>
          </ul>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestAssessment3,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestAssessment3,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestAssessment3,
      moreInfoText: MORE_INFO.DesignPromotionInterestAssessment3,
      moreInfo: (
        <div>
          <p>
            {`The Promotional Interest Delay Date sets the date when the introductory interest rate expires. If you set this field, you cannot enter a value in:`}
          </p>
          <ul>
            <li>Promotional Interest Delay Number of Days</li>
            <li>Promotional Interest Delay Number of Cycles</li>
            <li>Promotional Cash Option Number of Days</li>
            <li>Promotional Cash Option Number of Cycles</li>
          </ul>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestAssessment4,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestAssessment4,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestAssessment4,
      moreInfoText: MORE_INFO.DesignPromotionInterestAssessment4,
      moreInfo: (
        <div>
          <p>
            {`The Promotional Interest Delay Number of Days parameter determines the number of days the introductory interest rate for the promotion will be in effect. If you set this parameter to a nonzero value, set the following parameters to zeros:`}
          </p>
          <ul>
            <li>Promotional Interest Delay Date</li>
            <li>Promotional Interest Delay Number of Cycles</li>
            <li>Promotional Cash Option Number of Days</li>
            <li>Promotional Cash Option Number of Cycles</li>
          </ul>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestAssessment5,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestAssessment5,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestAssessment5,
      moreInfoText: MORE_INFO.DesignPromotionInterestAssessment5,
      moreInfo: (
        <div>
          <p>
            {`The Promotional Interest Delay Number of Days parameter determines the number of days the introductory interest rate for the promotion will be in effect. If you set this parameter to a nonzero value, set the following parameters to zeros:`}
          </p>
          <ul>
            <li>Promotional Interest Delay Date</li>
            <li>Promotional Interest Delay Number of Cycles</li>
            <li>Promotional Cash Option Number of Days</li>
            <li>Promotional Cash Option Number of Cycles</li>
          </ul>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestAssessment6,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestAssessment6,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestAssessment6,
      moreInfoText: MORE_INFO.DesignPromotionInterestAssessment6,
      moreInfo: MORE_INFO.DesignPromotionInterestAssessment6
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestAssessment7,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestAssessment7,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestAssessment7,
      moreInfoText: MORE_INFO.DesignPromotionInterestAssessment7,
      moreInfo: (
        <div>
          <p>
            {`The Promotional Cash Option Number of Cycles parameter determines the number of cycles for the Promotional Cash Option Number of Cycles parameter which determines the number of cycles from the last statement date that a cardholder has to pay off a promotional balance to avoid interest charges.For deferred interest - no interest as long as you pay off before the end date - but interest is accruing over that time and all the interest accrued is assessed if you don't pay down the full balance by end-of-promotion period defined in this field.If you set this parameter to a value other than zeros, set the following parameters to zeros:`}
          </p>
          <ul>
            <li>Promotional Interest Delay Date</li>
            <li>Promotional Interest Delay Number of Days</li>
            <li>Promotional Interest Delay Number of Months</li>
            <li>Promotional Cash Option Number of Days</li>
          </ul>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestAssessment8,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestAssessment8,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestAssessment8,
      moreInfoText: MORE_INFO.DesignPromotionInterestAssessment8,
      moreInfo: MORE_INFO.DesignPromotionInterestAssessment8
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestAssessment9,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestAssessment9,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestAssessment9,
      moreInfoText: MORE_INFO.DesignPromotionInterestAssessment9,
      moreInfo: MORE_INFO.DesignPromotionInterestAssessment9
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestAssessment10,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestAssessment10,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestAssessment10,
      moreInfoText: MORE_INFO.DesignPromotionInterestAssessment10,
      moreInfo: MORE_INFO.DesignPromotionInterestAssessment10
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestAssessment11,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestAssessment11,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestAssessment11,
      moreInfoText: MORE_INFO.DesignPromotionInterestAssessment11,
      moreInfo: MORE_INFO.DesignPromotionInterestAssessment11
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestAssessment12,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestAssessment12,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestAssessment12,
      moreInfoText: MORE_INFO.DesignPromotionInterestAssessment12,
      moreInfo: MORE_INFO.DesignPromotionInterestAssessment12
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestAssessment13,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestAssessment13,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestAssessment13,
      moreInfoText: MORE_INFO.DesignPromotionInterestAssessment13,
      moreInfo: MORE_INFO.DesignPromotionInterestAssessment13
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestAssessment14,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestAssessment14,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestAssessment14,
      moreInfoText: MORE_INFO.DesignPromotionInterestAssessment14,
      moreInfo: MORE_INFO.DesignPromotionInterestAssessment14
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestAssessment15,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestAssessment15,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestAssessment15,
      moreInfoText: MORE_INFO.DesignPromotionInterestAssessment15,
      moreInfo: MORE_INFO.DesignPromotionInterestAssessment15
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestAssessment16,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestAssessment16,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestAssessment16,
      moreInfoText: MORE_INFO.DesignPromotionInterestAssessment16,
      moreInfo: MORE_INFO.DesignPromotionInterestAssessment16
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestAssessment17,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestAssessment17,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestAssessment17,
      moreInfoText: MORE_INFO.DesignPromotionInterestAssessment17,
      moreInfo: MORE_INFO.DesignPromotionInterestAssessment17
    }
  ],
  Info_InterestOverrides: [
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestOverrides1,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestOverrides1,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestOverrides1,
      moreInfoText: MORE_INFO.DesignPromotionInterestOverrides1,
      moreInfo: MORE_INFO.DesignPromotionInterestOverrides1
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestOverrides2,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestOverrides2,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestOverrides2,
      moreInfoText: MORE_INFO.DesignPromotionInterestOverrides2,
      moreInfo: MORE_INFO.DesignPromotionInterestOverrides2
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestOverrides3,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestOverrides3,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestOverrides3,
      moreInfoText: MORE_INFO.DesignPromotionInterestOverrides3,
      moreInfo: MORE_INFO.DesignPromotionInterestOverrides3
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestOverrides4,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestOverrides4,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestOverrides4,
      moreInfoText: MORE_INFO.DesignPromotionInterestOverrides4,
      moreInfo: MORE_INFO.DesignPromotionInterestOverrides4
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestOverrides5,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestOverrides5,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestOverrides5,
      moreInfoText: MORE_INFO.DesignPromotionInterestOverrides5,
      moreInfo: MORE_INFO.DesignPromotionInterestOverrides5
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestOverrides6,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestOverrides6,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestOverrides6,
      moreInfoText: MORE_INFO.DesignPromotionInterestOverrides6,
      moreInfo: MORE_INFO.DesignPromotionInterestOverrides6
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestOverrides7,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestOverrides7,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestOverrides7,
      moreInfoText: MORE_INFO.DesignPromotionInterestOverrides7,
      moreInfo: (
        <div>
          <p>
            {`The Introductory Interest Options parameter determines the source of the introductory interest rate used for the promotion.If you set this parameter to 1 or 2, you must set at least one of the Intro Interest Dflt (CP IC ID) Set 1 parameters to a promotional method name. If you have set at least one of the Intro Interest Dflt (CP IC ID) Set 1 parameters to a promotional method name, you cannot set this parameter to zero.`}
          </p>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestOverrides8,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestOverrides8,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestOverrides8,
      moreInfoText: MORE_INFO.DesignPromotionInterestOverrides8,
      moreInfo: (
        <div>
          <p>
            {`The Regular Annual Interest Options parameter determines the source of the base interest rate you want to use for your promotion.If you set this parameter to 1 or 2, you must set at least one of the Intro Interest Dflt (CP IC ID) Set 1 parameters to a promotional method name. If you have set at least one of the Intro Interest Dflt (CP IC ID) Set 1 parameters to a promotional method name, you cannot set this parameter to
zero.`}
          </p>
        </div>
      )
    }
  ],
  Info_ReturnRevolveSettings: [
    {
      id: MethodFieldParameterEnum.DesignPromotionReturnRevolve1,
      fieldName: MethodFieldNameEnum.DesignPromotionReturnRevolve1,
      onlinePCF: ONLINE_PCF.DesignPromotionReturnRevolve1,
      moreInfoText: MORE_INFO.DesignPromotionReturnRevolve1,
      moreInfo: MORE_INFO.DesignPromotionReturnRevolve1
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionReturnRevolve2,
      fieldName: MethodFieldNameEnum.DesignPromotionReturnRevolve2,
      onlinePCF: ONLINE_PCF.DesignPromotionReturnRevolve2,
      moreInfoText: MORE_INFO.DesignPromotionReturnRevolve2,
      moreInfo: MORE_INFO.DesignPromotionReturnRevolve2
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionReturnRevolve3,
      fieldName: MethodFieldNameEnum.DesignPromotionReturnRevolve3,
      onlinePCF: ONLINE_PCF.DesignPromotionReturnRevolve3,
      moreInfoText: MORE_INFO.DesignPromotionReturnRevolve3,
      moreInfo: MORE_INFO.DesignPromotionReturnRevolve3
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionReturnRevolve4,
      fieldName: MethodFieldNameEnum.DesignPromotionReturnRevolve4,
      onlinePCF: ONLINE_PCF.DesignPromotionReturnRevolve4,
      moreInfoText: MORE_INFO.DesignPromotionReturnRevolve4,
      moreInfo: MORE_INFO.DesignPromotionReturnRevolve4
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionReturnRevolve5,
      fieldName: MethodFieldNameEnum.DesignPromotionReturnRevolve5,
      onlinePCF: ONLINE_PCF.DesignPromotionReturnRevolve5,
      moreInfoText: MORE_INFO.DesignPromotionReturnRevolve5,
      moreInfo: MORE_INFO.DesignPromotionReturnRevolve5
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionReturnRevolve6,
      fieldName: MethodFieldNameEnum.DesignPromotionReturnRevolve6,
      onlinePCF: ONLINE_PCF.DesignPromotionReturnRevolve6,
      moreInfoText: MORE_INFO.DesignPromotionReturnRevolve6,
      moreInfo: MORE_INFO.DesignPromotionReturnRevolve6
    }
  ],
  Info_RetailPathRelatedSettings: [
    {
      id: MethodFieldParameterEnum.DesignPromotionRetailPath1,
      fieldName: MethodFieldNameEnum.DesignPromotionRetailPath1,
      onlinePCF: ONLINE_PCF.DesignPromotionRetailPath1,
      moreInfoText: MORE_INFO.DesignPromotionRetailPath1,
      moreInfo: (
        <div>
          <p>
            {`The R Path Promotion Return Indicator parameter specifies whether you want to accept promotional return transactions through the "R Path" and apply them to the promotional balance after the end of a promotion. Your setting determines whether the System uses the return start and end dates instead of the promotion start and end dates.This parameter is used only by TLPSM transactions within the R path.The R path transactions have a merchant with a merchant department code switch of R.`}
          </p>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionRetailPath2,
      fieldName: MethodFieldNameEnum.DesignPromotionRetailPath2,
      onlinePCF: ONLINE_PCF.DesignPromotionRetailPath2,
      moreInfoText: MORE_INFO.DesignPromotionRetailPath2,
      moreInfo: MORE_INFO.DesignPromotionRetailPath2
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionRetailPath3,
      fieldName: MethodFieldNameEnum.DesignPromotionRetailPath3,
      onlinePCF: ONLINE_PCF.DesignPromotionRetailPath3,
      moreInfoText: MORE_INFO.DesignPromotionRetailPath3,
      moreInfo: MORE_INFO.DesignPromotionRetailPath3
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionRetailPath4,
      fieldName: MethodFieldNameEnum.DesignPromotionRetailPath4,
      onlinePCF: ONLINE_PCF.DesignPromotionRetailPath4,
      moreInfoText: MORE_INFO.DesignPromotionRetailPath4,
      moreInfo: MORE_INFO.DesignPromotionRetailPath4
    }
  ],
  Info_StatementMessaging: [
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementMessaging1,
      fieldName: MethodFieldNameEnum.DesignPromotionStatementMessaging1,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementMessaging1,
      moreInfoText: MORE_INFO.DesignPromotionStatementMessaging1,
      moreInfo: (
        <div>
          <p>{MORE_INFO.DesignPromotionStatementAdvancedTextType_mt_pt}</p>
          <br />
          {MORE_INFO.DesignPromotionStatementMessaging1}
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementMessaging2,
      fieldName: MethodFieldNameEnum.DesignPromotionStatementMessaging2,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementMessaging2,
      moreInfoText: MORE_INFO.DesignPromotionStatementMessaging2,
      moreInfo: (
        <div>
          <p>{MORE_INFO.DesignPromotionStatementAdvancedTextType_mt_pt}</p>
          <br />
          {MORE_INFO.DesignPromotionStatementMessaging2}
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementMessaging3,
      fieldName: MethodFieldNameEnum.DesignPromotionStatementMessaging3,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementMessaging3,
      moreInfoText: MORE_INFO.DesignPromotionStatementMessaging3,
      moreInfo: MORE_INFO.DesignPromotionStatementMessaging3
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementMessaging4,
      fieldName: MethodFieldNameEnum.DesignPromotionStatementMessaging4,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementMessaging4,
      moreInfoText: MORE_INFO.DesignPromotionStatementMessaging4,
      moreInfo: MORE_INFO.DesignPromotionStatementMessaging4
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementMessaging5,
      fieldName: MethodFieldNameEnum.DesignPromotionStatementMessaging5,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementMessaging5,
      moreInfoText: MORE_INFO.DesignPromotionStatementMessaging5,
      moreInfo: MORE_INFO.DesignPromotionStatementMessaging5
    }
  ],
  Info_AdvancedParameters: [
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter1,
      fieldName: MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter1,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter1,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter1,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter1
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter2,
      fieldName: MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter2,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter2,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter2,
      moreInfo: (
        <div>
          <p>
            The Introductory Minimum Payments Delay Number of Days parameter
            determines the number of days that the introductory minimum payment
            period is in effect. Your setting in the Delay Payment Start
            parameter determines whether the System begins counting the number
            of days from the post date of the first promotional transaction or
            from the account open date. The System edits the Introductory
            Minimum Payments Delay Number of Days parameter for numeric values.
            If you set this parameter, you must also set the following
            parameters in this section to zeros.
          </p>
          <ul>
            <li>Introductory Minimum Payments Delay Date</li>
            <li>Introductory Minimum Payments Delay Number of Cycles</li>
            <li>Introductory Minimum Payments Delay Number of Months</li>
            <li>Introductory Minimum Payments MPD End Date Ext</li>
          </ul>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter3,
      fieldName: MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter3,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter3,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter3,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter3
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter4,
      fieldName: MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter4,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter4,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter4,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter4
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter5,
      fieldName: MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter5,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter5,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter5,
      moreInfo: (
        <div>
          <p>
            The Expiring Promotion Notification Months parameter determines the
            number of months prior to the promotional expiration date that a
            notification message is included on statements. The System edits
            this parameter for values 00 through 12 and 99.
          </p>
          <p>
            Set this parameter from 1 to 12 as the number of months prior to the
            promotional expiration date that a notification message is included
            on statements. For example, if you enter 03 in this parameter, and
            the promotion expires on October 31 and the account statement cycles
            on the 15th of the month, the notification message appears on
            statements for August 15, September 15, and October 15
          </p>
          <p>
            Set this parameter to 99 to have the System include a message about
            the promotion expiration date on each statement throughout the
            duration of the promotion.
          </p>
          <p>
            Set this parameter to zeros to not provide a notification message.
          </p>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter6,
      fieldName: MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter6,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter6,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter6,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter6
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter7,
      fieldName: MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter7,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter7,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter7,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter7
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter8,
      fieldName: MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter8,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter8,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter8,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter8
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter9,
      fieldName: MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter9,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter9,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter9,
      moreInfo: (
        <div>
          <p>
            The Exclude Cash Option from 2-Cycle Average Daily Balance parameter
            determines whether to exclude a cash option promotion from the two
            cycle average daily balance interest calculation method for cash
            advance or merchandise transactions
          </p>
          <p>
            If you set this parameter to 1, you must also set the Interest
            Methods (CP IC IM) parameter in this section to blank.
          </p>
          <p>
            Setting this parameter to 1 overrides the Cycle-To-Date Cash
            interest method in the Interest Methods section (CP IC IM) from the
            pricing strategy or method override with a nondeferring interest
            method for cash option promotions as described in the following
            paragraphs.
          </p>
          <ul>
            <li>
              Accounts with cash option promotional balances that use interest
              methods 03, Deferred Interest, or 09, Revolving Variable, will
              instead use interest method 02, Average Daily Balance, to
              determine the interest on the cash option promotional balance.
            </li>
            <li>
              Accounts with cash option promotional balances that use interest
              methods 08, Deferred at Daily Rate, or 11, Daily Revolving
              Variable, will instead use interest method 05, Daily Rate Average
              Daily Balance, to determine the interest on the cash option
              promotional balance.
            </li>
          </ul>
          <p>
            Since interest methods 02 and 05 are not interest deferring methods,
            cycle-to-date interest is determined and billed on the account’s
            promotional balances. The interest is displayed on the cardholder’s
            first statement immediately following statement cycle in which the
            cash option transaction posts to the account.
          </p>
          <p>
            Accounts with cash option promotional balances that use interest
            deferring methods 03, 08, 09, or 11 will still use the interest
            deferring method to determine interest on the revolving side of the
            account, as well as for noncash option promotional purchases.
          </p>
          <p>
            You have to have 2-cycle ADB set in CP IC IM or else this Method
            will not work effectively.
          </p>
          <p>
            <b>Warning!</b>
          </p>
          <p>
            <b>
              Use this feature with new promotions only. Changing the setting of
              this parameter from zero to 1 on an existing promotion may cause
              the following results.
            </b>
          </p>
          <p>
            <b>
              If you initially set this parameter to zero then change it to 1,
              the cardholder’s two-cycle average daily balance will be lost. If
              you initially set this parameter to zero on a multi-ticket
              promotion, the System will generate a deferred average daily
              balance and will not generate unbilled interest. If you change the
              setting to 1 in the next statement cycle and backdate a
              transaction across cycle, the System will generate last statement
              unbilled interest and will not generate a last statement deferred
              average daily balance.
            </b>
          </p>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter10,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter10,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter10,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter10,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter10
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter11,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter11,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter11,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter11,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter11
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter12,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter12,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter12,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter12,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter12
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter13,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter13,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter13,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter13,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter13
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter14,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter14,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter14,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter14,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter14
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter15,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter15,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter15,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter15,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter15
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter16,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter16,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter16,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter16,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter16
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter17,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter17,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter17,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter17,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter17
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter18,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter18,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter18,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter18,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter18
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter19,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter19,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter19,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter19,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter19
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter20,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter20,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter20,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter20,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter20
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter21,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter21,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter21,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter21,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter21
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter22,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter22,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter22,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter22,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter22
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter23,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter23,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter23,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter23,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter23
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter24,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter24,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter24,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter24,
      moreInfo: (
        <div>
          <p>
            The CPOCPP Return Usage parameter determines whether you use the
            following pricing strategy-level parameters in the Promotional
            Purchases section (CP OC PP) for return to revolving processing of a
            delinquent account.
          </p>
          <ul>
            <li>Return to Revolving Days Delinquent</li>
            <li>Return to Revolving Dlnq Rtr Timing Code</li>
          </ul>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter25,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter25,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter25,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter25,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter25
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter26,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter26,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter26,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter26,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter26
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter27,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter27,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter27,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter27,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter27
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter28,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter28,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter28,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter28,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter28
    }
  ]
};

export const parameterGroupNewVersion: Record<
  ParameterListIds,
  ParameterGroup[]
> = {
  Info_Basic: [
    {
      id: MethodFieldParameterEnum.DesignPromotionBasicInformation1,
      fieldName: MethodFieldNameEnum.DesignPromotionBasicInformation1,
      onlinePCF: ONLINE_PCF.DesignPromotionBasicInformation1,
      moreInfoText: MORE_INFO.DesignPromotionBasicInformation1,
      moreInfo: MORE_INFO.DesignPromotionBasicInformation1
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionBasicInformation2,
      fieldName: MethodFieldNameEnum.DesignPromotionBasicInformation2,
      onlinePCF: ONLINE_PCF.DesignPromotionBasicInformation2,
      moreInfoText: MORE_INFO.DesignPromotionBasicInformation2,
      moreInfo: MORE_INFO.DesignPromotionBasicInformation2
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionBasicInformation3,
      fieldName: MethodFieldNameEnum.DesignPromotionBasicInformation3,
      onlinePCF: ONLINE_PCF.DesignPromotionBasicInformation3,
      moreInfoText: MORE_INFO.DesignPromotionBasicInformation3,
      moreInfo: MORE_INFO.DesignPromotionBasicInformation3
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionBasicInformation4,
      fieldName: MethodFieldNameEnum.DesignPromotionBasicInformation4,
      onlinePCF: ONLINE_PCF.DesignPromotionBasicInformation4,
      moreInfoText: MORE_INFO.DesignPromotionBasicInformation4,
      moreInfo: MORE_INFO.DesignPromotionBasicInformation4
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionBasicInformation5,
      fieldName: MethodFieldNameEnum.DesignPromotionBasicInformation5,
      onlinePCF: ONLINE_PCF.DesignPromotionBasicInformation5,
      moreInfoText: MORE_INFO.DesignPromotionBasicInformation5,
      moreInfo: MORE_INFO.DesignPromotionBasicInformation5
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionBasicInformation6,
      fieldName: MethodFieldNameEnum.DesignPromotionBasicInformation6,
      onlinePCF: ONLINE_PCF.DesignPromotionBasicInformation6,
      moreInfoText: MORE_INFO.DesignPromotionBasicInformation6,
      moreInfo: MORE_INFO.DesignPromotionBasicInformation6
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionBasicInformation7,
      fieldName: MethodFieldNameEnum.DesignPromotionBasicInformation7,
      onlinePCF: ONLINE_PCF.DesignPromotionBasicInformation7,
      moreInfoText: MORE_INFO.DesignPromotionBasicInformation7,
      moreInfo: MORE_INFO.DesignPromotionBasicInformation7
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionBasicInformation8,
      fieldName: MethodFieldNameEnum.DesignPromotionBasicInformation8,
      onlinePCF: ONLINE_PCF.DesignPromotionBasicInformation8,
      moreInfoText: MORE_INFO.DesignPromotionBasicInformation8,
      moreInfo: MORE_INFO.DesignPromotionBasicInformation8
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionBasicInformation9,
      fieldName: MethodFieldNameEnum.DesignPromotionBasicInformation9,
      onlinePCF: ONLINE_PCF.DesignPromotionBasicInformation9,
      moreInfoText: MORE_INFO.DesignPromotionBasicInformation9,
      moreInfo: (
        <div>
          <p>
            {`The Multi-Ticket Criteria parameter determines whether qualifying sales transactions from multiple days or promotional periods will post to the same promotional balance for the duration of a promotion. Only enter this field if Multi-ticket promotion is chosen in Promotion Type field.
When you hover over "related fields," you will see the following fields and the values in said fields:`}
          </p>
          <ul>
            <li>Promotional Cash Option Number of Days</li>
            <li>Promotional Cash Option Number of Months</li>
            <li>Promotional Cash Option Number of Cycles</li>
            <li>
              Promotional Cash Option End Date (do not display for choice zero)
            </li>
            <li>Promotional Interest Delay Number of Days</li>
            <li>Promotional Interest Delay Number of Months</li>
            <li>Promotional Interest Delay Number of Cycles</li>
            <li>
              Promotional Interest Delay End Date (do not display for choice
              zero)
            </li>
            <li>Promotional Minimum Delay Number of Days</li>
            <li>Promotional Minimum Delay Number of Months</li>
            <li>Promotional Minimum Delay Number of Cycles</li>
            <li>
              Promotional Minimum Delay End Date (do not display for choice
              zero)
            </li>
          </ul>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionBasicInformation10,
      fieldName: MethodFieldNameEnum.DesignPromotionBasicInformation10,
      onlinePCF: ONLINE_PCF.DesignPromotionBasicInformation10,
      moreInfoText: MORE_INFO.DesignPromotionBasicInformation10,
      moreInfo: MORE_INFO.DesignPromotionBasicInformation10
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionBasicInformation11,
      fieldName: MethodFieldNameEnum.DesignPromotionBasicInformation11,
      onlinePCF: ONLINE_PCF.DesignPromotionBasicInformation11,
      moreInfoText: MORE_INFO.DesignPromotionBasicInformation11,
      moreInfo: (
        <div>
          <p>
            {`The Balance Transfer Promotion Option parameter determines whether the System defines the promotion is defined as a balance transfer. IE: "if you transfer your balance over, we'll give you %% APR for X Months!"`}
          </p>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionBasicInformation12,
      fieldName: MethodFieldNameEnum.DesignPromotionBasicInformation12,
      onlinePCF: ONLINE_PCF.DesignPromotionBasicInformation12,
      moreInfoText: MORE_INFO.DesignPromotionBasicInformation12,
      moreInfo: MORE_INFO.DesignPromotionBasicInformation12
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionBasicInformation13,
      fieldName: MethodFieldNameEnum.DesignPromotionBasicInformation13,
      onlinePCF: ONLINE_PCF.DesignPromotionBasicInformation13,
      moreInfoText: MORE_INFO.DesignPromotionBasicInformation13,
      moreInfo: MORE_INFO.DesignPromotionBasicInformation13
    }
  ],
  Info_InterestAssessmentFees: [
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestAssessment1,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestAssessment1,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestAssessment1,
      moreInfoText: MORE_INFO.DesignPromotionInterestAssessment1,
      moreInfo: MORE_INFO.DesignPromotionInterestAssessment1
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestAssessment2,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestAssessment2,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestAssessment2,
      moreInfoText: MORE_INFO.DesignPromotionInterestAssessment2,
      moreInfo: (
        <div>
          <p>
            {`The Introductory Promotional Interest Rate parameter determines the interest rate for an introductory period - "for the first six months of spend within the cardholder's promotional terms, the interest on the balance is 8%."
The Intro Annual Rate parameter determines the interest rate for transactions during a time period you specify in:`}
          </p>
          <ul>
            <li>Promotional Interest Delay Number of Days</li>
            <li>Promotional Interest Delay Number of Cycles</li>
          </ul>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestAssessment3,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestAssessment3,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestAssessment3,
      moreInfoText: MORE_INFO.DesignPromotionInterestAssessment3,
      moreInfo: (
        <div>
          <p>
            {`The Promotional Interest Delay Date sets the date when the introductory interest rate expires. If you set this field, you cannot enter a value in:`}
          </p>
          <ul>
            <li>Promotional Interest Delay Number of Days</li>
            <li>Promotional Interest Delay Number of Cycles</li>
            <li>Promotional Cash Option Number of Days</li>
            <li>Promotional Cash Option Number of Cycles</li>
          </ul>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestAssessment4,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestAssessment4,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestAssessment4,
      moreInfoText: MORE_INFO.DesignPromotionInterestAssessment4,
      moreInfo: (
        <div>
          <p>
            {`The Promotional Interest Delay Number of Days parameter determines the number of days the introductory interest rate for the promotion will be in effect. If you set this parameter to a nonzero value, set the following parameters to zeros:`}
          </p>
          <ul>
            <li>Promotional Interest Delay Date</li>
            <li>Promotional Interest Delay Number of Cycles</li>
            <li>Promotional Cash Option Number of Days</li>
            <li>Promotional Cash Option Number of Cycles</li>
          </ul>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestAssessment5,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestAssessment5,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestAssessment5,
      moreInfoText: MORE_INFO.DesignPromotionInterestAssessment5,
      moreInfo: (
        <div>
          <p>
            {`The Promotional Interest Delay Number of Days parameter determines the number of days the introductory interest rate for the promotion will be in effect. If you set this parameter to a nonzero value, set the following parameters to zeros:`}
          </p>
          <ul>
            <li>Promotional Interest Delay Date</li>
            <li>Promotional Interest Delay Number of Cycles</li>
            <li>Promotional Cash Option Number of Days</li>
            <li>Promotional Cash Option Number of Cycles</li>
          </ul>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestAssessment6,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestAssessment6,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestAssessment6,
      moreInfoText: MORE_INFO.DesignPromotionInterestAssessment6,
      moreInfo: MORE_INFO.DesignPromotionInterestAssessment6
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestAssessment7,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestAssessment7,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestAssessment7,
      moreInfoText: MORE_INFO.DesignPromotionInterestAssessment7,
      moreInfo: (
        <div>
          <p>
            {`The Promotional Cash Option Number of Cycles parameter determines the number of cycles for the Promotional Cash Option Number of Cycles parameter which determines the number of cycles from the last statement date that a cardholder has to pay off a promotional balance to avoid interest charges.For deferred interest - no interest as long as you pay off before the end date - but interest is accruing over that time and all the interest accrued is assessed if you don't pay down the full balance by end-of-promotion period defined in this field.If you set this parameter to a value other than zeros, set the following parameters to zeros:`}
          </p>
          <ul>
            <li>Promotional Interest Delay Date</li>
            <li>Promotional Interest Delay Number of Days</li>
            <li>Promotional Interest Delay Number of Months</li>
            <li>Promotional Cash Option Number of Days</li>
          </ul>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestAssessment8,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestAssessment8,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestAssessment8,
      moreInfoText: MORE_INFO.DesignPromotionInterestAssessment8,
      moreInfo: MORE_INFO.DesignPromotionInterestAssessment8
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestAssessment9,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestAssessment9,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestAssessment9,
      moreInfoText: MORE_INFO.DesignPromotionInterestAssessment9,
      moreInfo: MORE_INFO.DesignPromotionInterestAssessment9
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestAssessment10,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestAssessment10,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestAssessment10,
      moreInfoText: MORE_INFO.DesignPromotionInterestAssessment10,
      moreInfo: MORE_INFO.DesignPromotionInterestAssessment10
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestAssessment11,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestAssessment11,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestAssessment11,
      moreInfoText: MORE_INFO.DesignPromotionInterestAssessment11,
      moreInfo: MORE_INFO.DesignPromotionInterestAssessment11
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestAssessment12,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestAssessment12,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestAssessment12,
      moreInfoText: MORE_INFO.DesignPromotionInterestAssessment12,
      moreInfo: MORE_INFO.DesignPromotionInterestAssessment12
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestAssessment13,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestAssessment13,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestAssessment13,
      moreInfoText: MORE_INFO.DesignPromotionInterestAssessment13,
      moreInfo: MORE_INFO.DesignPromotionInterestAssessment13
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestAssessment14,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestAssessment14,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestAssessment14,
      moreInfoText: MORE_INFO.DesignPromotionInterestAssessment14,
      moreInfo: MORE_INFO.DesignPromotionInterestAssessment14
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestAssessment15,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestAssessment15,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestAssessment15,
      moreInfoText: MORE_INFO.DesignPromotionInterestAssessment15,
      moreInfo: MORE_INFO.DesignPromotionInterestAssessment15
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestAssessment16,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestAssessment16,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestAssessment16,
      moreInfoText: MORE_INFO.DesignPromotionInterestAssessment16,
      moreInfo: MORE_INFO.DesignPromotionInterestAssessment16
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestAssessment17,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestAssessment17,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestAssessment17,
      moreInfoText: MORE_INFO.DesignPromotionInterestAssessment17,
      moreInfo: MORE_INFO.DesignPromotionInterestAssessment17
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestAssessment18,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestAssessment18,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestAssessment18,
      moreInfoText: MORE_INFO.DesignPromotionInterestAssessment18,
      moreInfo: MORE_INFO.DesignPromotionInterestAssessment18
    }
  ],
  Info_InterestOverrides: [
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestOverrides1,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestOverrides1,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestOverrides1,
      moreInfoText: MORE_INFO.DesignPromotionInterestOverrides1,
      moreInfo: MORE_INFO.DesignPromotionInterestOverrides1
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestOverrides2,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestOverrides2,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestOverrides2,
      moreInfoText: MORE_INFO.DesignPromotionInterestOverrides2,
      moreInfo: MORE_INFO.DesignPromotionInterestOverrides2
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestOverrides3,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestOverrides3,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestOverrides3,
      moreInfoText: MORE_INFO.DesignPromotionInterestOverrides3,
      moreInfo: MORE_INFO.DesignPromotionInterestOverrides3
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestOverrides4,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestOverrides4,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestOverrides4,
      moreInfoText: MORE_INFO.DesignPromotionInterestOverrides4,
      moreInfo: MORE_INFO.DesignPromotionInterestOverrides4
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestOverrides5,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestOverrides5,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestOverrides5,
      moreInfoText: MORE_INFO.DesignPromotionInterestOverrides5,
      moreInfo: MORE_INFO.DesignPromotionInterestOverrides5
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestOverrides6,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestOverrides6,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestOverrides6,
      moreInfoText: MORE_INFO.DesignPromotionInterestOverrides6,
      moreInfo: MORE_INFO.DesignPromotionInterestOverrides6
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestOverrides7,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestOverrides7,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestOverrides7,
      moreInfoText: MORE_INFO.DesignPromotionInterestOverrides7,
      moreInfo: (
        <div>
          <p>
            {`The Introductory Interest Options parameter determines the source of the introductory interest rate used for the promotion.If you set this parameter to 1 or 2, you must set at least one of the Intro Interest Dflt (CP IC ID) Set 1 parameters to a promotional method name. If you have set at least one of the Intro Interest Dflt (CP IC ID) Set 1 parameters to a promotional method name, you cannot set this parameter to zero.`}
          </p>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionInterestOverrides8,
      fieldName: MethodFieldNameEnum.DesignPromotionInterestOverrides8,
      onlinePCF: ONLINE_PCF.DesignPromotionInterestOverrides8,
      moreInfoText: MORE_INFO.DesignPromotionInterestOverrides8,
      moreInfo: (
        <div>
          <p>
            {`The Regular Annual Interest Options parameter determines the source of the base interest rate you want to use for your promotion.If you set this parameter to 1 or 2, you must set at least one of the Intro Interest Dflt (CP IC ID) Set 1 parameters to a promotional method name. If you have set at least one of the Intro Interest Dflt (CP IC ID) Set 1 parameters to a promotional method name, you cannot set this parameter to zero.`}
          </p>
        </div>
      )
    }
  ],
  Info_ReturnRevolveSettings: [
    {
      id: MethodFieldParameterEnum.DesignPromotionReturnRevolve1,
      fieldName: MethodFieldNameEnum.DesignPromotionReturnRevolve1,
      onlinePCF: ONLINE_PCF.DesignPromotionReturnRevolve1,
      moreInfoText: MORE_INFO.DesignPromotionReturnRevolve1,
      moreInfo: MORE_INFO.DesignPromotionReturnRevolve1
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionReturnRevolve2,
      fieldName: MethodFieldNameEnum.DesignPromotionReturnRevolve2,
      onlinePCF: ONLINE_PCF.DesignPromotionReturnRevolve2,
      moreInfoText: MORE_INFO.DesignPromotionReturnRevolve2,
      moreInfo: MORE_INFO.DesignPromotionReturnRevolve2
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionReturnRevolve3,
      fieldName: MethodFieldNameEnum.DesignPromotionReturnRevolve3,
      onlinePCF: ONLINE_PCF.DesignPromotionReturnRevolve3,
      moreInfoText: MORE_INFO.DesignPromotionReturnRevolve3,
      moreInfo: MORE_INFO.DesignPromotionReturnRevolve3
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionReturnRevolve4,
      fieldName: MethodFieldNameEnum.DesignPromotionReturnRevolve4,
      onlinePCF: ONLINE_PCF.DesignPromotionReturnRevolve4,
      moreInfoText: MORE_INFO.DesignPromotionReturnRevolve4,
      moreInfo: MORE_INFO.DesignPromotionReturnRevolve4
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionReturnRevolve5,
      fieldName: MethodFieldNameEnum.DesignPromotionReturnRevolve5,
      onlinePCF: ONLINE_PCF.DesignPromotionReturnRevolve5,
      moreInfoText: MORE_INFO.DesignPromotionReturnRevolve5,
      moreInfo: MORE_INFO.DesignPromotionReturnRevolve5
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionReturnRevolve6,
      fieldName: MethodFieldNameEnum.DesignPromotionReturnRevolve6,
      onlinePCF: ONLINE_PCF.DesignPromotionReturnRevolve6,
      moreInfoText: MORE_INFO.DesignPromotionReturnRevolve6,
      moreInfo: MORE_INFO.DesignPromotionReturnRevolve6
    }
  ],
  Info_RetailPathRelatedSettings: [
    {
      id: MethodFieldParameterEnum.DesignPromotionRetailPath1,
      fieldName: MethodFieldNameEnum.DesignPromotionRetailPath1,
      onlinePCF: ONLINE_PCF.DesignPromotionRetailPath1,
      moreInfoText: MORE_INFO.DesignPromotionRetailPath1,
      moreInfo: (
        <div>
          <p>
            {`The R Path Promotion Return Indicator parameter specifies whether you want to accept promotional return transactions through the "R Path" and apply them to the promotional balance after the end of a promotion. Your setting determines whether the System uses the return start and end dates instead of the promotion start and end dates.This parameter is used only by TLPSM transactions within the R path. The R path transactions have a merchant with a merchant department code switch of R.`}
          </p>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionRetailPath2,
      fieldName: MethodFieldNameEnum.DesignPromotionRetailPath2,
      onlinePCF: ONLINE_PCF.DesignPromotionRetailPath2,
      moreInfoText: MORE_INFO.DesignPromotionRetailPath2,
      moreInfo: MORE_INFO.DesignPromotionRetailPath2
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionRetailPath3,
      fieldName: MethodFieldNameEnum.DesignPromotionRetailPath3,
      onlinePCF: ONLINE_PCF.DesignPromotionRetailPath3,
      moreInfoText: MORE_INFO.DesignPromotionRetailPath3,
      moreInfo: MORE_INFO.DesignPromotionRetailPath3
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionRetailPath4,
      fieldName: MethodFieldNameEnum.DesignPromotionRetailPath4,
      onlinePCF: ONLINE_PCF.DesignPromotionRetailPath4,
      moreInfoText: MORE_INFO.DesignPromotionRetailPath4,
      moreInfo: MORE_INFO.DesignPromotionRetailPath4
    }
  ],
  Info_StatementMessaging: [
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementMessaging1,
      fieldName: MethodFieldNameEnum.DesignPromotionStatementMessaging1,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementMessaging1,
      moreInfoText: MORE_INFO.DesignPromotionStatementMessaging1,
      moreInfo: (
        <div>
          <p>{MORE_INFO.DesignPromotionStatementAdvancedTextType_mt_pt}</p>
          <br />
          {MORE_INFO.DesignPromotionStatementMessaging1}
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementMessaging2,
      fieldName: MethodFieldNameEnum.DesignPromotionStatementMessaging2,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementMessaging2,
      moreInfoText: MORE_INFO.DesignPromotionStatementMessaging2,
      moreInfo: (
        <div>
          <p>{MORE_INFO.DesignPromotionStatementAdvancedTextType_mt_pt}</p>
          <br />
          {MORE_INFO.DesignPromotionStatementMessaging2}
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementMessaging3,
      fieldName: MethodFieldNameEnum.DesignPromotionStatementMessaging3,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementMessaging3,
      moreInfoText: MORE_INFO.DesignPromotionStatementMessaging3,
      moreInfo: MORE_INFO.DesignPromotionStatementMessaging3
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementMessaging4,
      fieldName: MethodFieldNameEnum.DesignPromotionStatementMessaging4,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementMessaging4,
      moreInfoText: MORE_INFO.DesignPromotionStatementMessaging4,
      moreInfo: MORE_INFO.DesignPromotionStatementMessaging4
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementMessaging5,
      fieldName: MethodFieldNameEnum.DesignPromotionStatementMessaging5,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementMessaging5,
      moreInfoText: MORE_INFO.DesignPromotionStatementMessaging5,
      moreInfo: MORE_INFO.DesignPromotionStatementMessaging5
    }
  ],

  Info_AdvancedParameters: [
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter1,
      fieldName: MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter1,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter1,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter1,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter1
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter2,
      fieldName: MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter2,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter2,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter2,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter2
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter3,
      fieldName: MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter3,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter3,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter3,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter3
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter4,
      fieldName: MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter4,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter4,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter4,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter4
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter5,
      fieldName: MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter5,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter5,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter5,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter5
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter6,
      fieldName: MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter6,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter6,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter6,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter6
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter7,
      fieldName: MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter7,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter7,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter7,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter7
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter8,
      fieldName: MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter8,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter8,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter8,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter8
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter9,
      fieldName: MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter9,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter9,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter9,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter9
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter10,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter10,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter10,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter10,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter10
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter11,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter11,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter11,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter11,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter11
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter12,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter12,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter12,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter12,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter12
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter13,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter13,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter13,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter13,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter13
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter14,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter14,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter14,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter14,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter14
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter15,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter15,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter15,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter15,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter15
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter16,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter16,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter16,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter16,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter16
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter17,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter17,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter17,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter17,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter17
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter18,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter18,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter18,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter18,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter18
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter19,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter19,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter19,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter19,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter19
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter20,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter20,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter20,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter20,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter20
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter21,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter21,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter21,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter21,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter21
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter22,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter22,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter22,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter22,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter22
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter23,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter23,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter23,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter23,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter23
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter24,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter24,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter24,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter24,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter24
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter25,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter25,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter25,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter25,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter25
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter26,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter26,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter26,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter26,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter26
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter27,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter27,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter27,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter27,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter27
    },
    {
      id: MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter28,
      fieldName:
        MethodFieldNameEnum.DesignPromotionStatementAdvancedParameter28,
      onlinePCF: ONLINE_PCF.DesignPromotionStatementAdvancedParameter28,
      moreInfoText: MORE_INFO.DesignPromotionStatementAdvancedParameter28,
      moreInfo: MORE_INFO.DesignPromotionStatementAdvancedParameter28
    }
  ]
};
