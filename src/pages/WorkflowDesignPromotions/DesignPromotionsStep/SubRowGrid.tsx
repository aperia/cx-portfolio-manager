import { MethodFieldParameterEnum } from 'app/constants/enums';
import { mapGridExpandCollapse, methodParamsToObject } from 'app/helpers';
import { Grid, useTranslation } from 'app/_libraries/_dls';
import React, { useState } from 'react';
import newMethodColumns from './newMethodColumns';
import { parameterList, ParameterListIds } from './newMethodParameterList';
import SubGridPreviewMethod from './SubGridPreviewMethod';

export interface ParameterValueType {
  // design promotions basic
  designPromotionBasicInformation1: number | string | undefined;
  designPromotionBasicInformation2: number | string | undefined;
  designPromotionBasicInformation3: number | string | undefined;
  designPromotionBasicInformation4: number | string | undefined;
  designPromotionBasicInformation5: MagicKeyValue | undefined;
  designPromotionBasicInformation6: number | string | undefined;
  designPromotionBasicInformation7: number | string | undefined;
  designPromotionBasicInformation8: number | string | undefined;
  designPromotionBasicInformation9: number | string | undefined;
  designPromotionBasicInformation10: number | string | undefined;
  designPromotionBasicInformation11: number | string | undefined;
  designPromotionBasicInformation12: number | string | undefined;
  designPromotionBasicInformation13: number | string | undefined;
  // design promotions Interest Assessment and Fees
  designPromotionInterestAssessment1: number | string | undefined;
  designPromotionInterestAssessment2: number | string | undefined;
  designPromotionInterestAssessment3: number | string | undefined;
  designPromotionInterestAssessment4: number | string | undefined;
  designPromotionInterestAssessment5: number | string | undefined;
  designPromotionInterestAssessment6: number | string | undefined;
  designPromotionInterestAssessment7: number | string | undefined;
  designPromotionInterestAssessment8: number | string | undefined;
  designPromotionInterestAssessment9: number | string | undefined;
  designPromotionInterestAssessment10: number | string | undefined;
  designPromotionInterestAssessment11: number | string | undefined;
  designPromotionInterestAssessment12: number | string | undefined;
  designPromotionInterestAssessment13: number | string | undefined;
  designPromotionInterestAssessment14: number | string | undefined;
  designPromotionInterestAssessment15: number | string | undefined;
  designPromotionInterestAssessment16: number | string | undefined;
  designPromotionInterestAssessment17: number | string | undefined;
  designPromotionInterestAssessment18: number | string | undefined;
  // design promotions Interest Overrides
  designPromotionInterestOverrides1: number | string | undefined;
  designPromotionInterestOverrides2: number | string | undefined;
  designPromotionInterestOverrides3: number | string | undefined;
  designPromotionInterestOverrides4: number | string | undefined;
  designPromotionInterestOverrides5: number | string | undefined;
  designPromotionInterestOverrides6: number | string | undefined;
  designPromotionInterestOverrides7: number | string | undefined;
  designPromotionInterestOverrides8: number | string | undefined;
  // design promotions Retail Path
  designPromotionRetailPath1: number | string | undefined;
  designPromotionRetailPath2: number | string | undefined;
  designPromotionRetailPath3: number | string | undefined;
  designPromotionRetailPath4: number | string | undefined;
  //design promotions Return-to-Revolve
  designPromotionReturnRevolve1: number | string | undefined;
  designPromotionReturnRevolve2: number | string | undefined;
  designPromotionReturnRevolve3: number | string | undefined;
  designPromotionReturnRevolve4: number | string | undefined;
  designPromotionReturnRevolve5: number | string | undefined;
  designPromotionReturnRevolve6: number | string | undefined;
  //design promotions Statement Messaging
  designPromotionStatementMessaging1: number | string | undefined;
  designPromotionStatementMessaging2: number | string | undefined;
  designPromotionStatementMessaging3: number | string | undefined;
  designPromotionStatementMessaging4: number | string | undefined;
  designPromotionStatementMessaging5: number | string | undefined;
  // design Promotion Statement Advanced Parameter
  designPromotionStatementAdvancedParameter1?: number | string;
  designPromotionStatementAdvancedParameter2?: number | string;
  designPromotionStatementAdvancedParameter3?: number | string;
  designPromotionStatementAdvancedParameter4?: number | string;
  designPromotionStatementAdvancedParameter5?: number | string;
  designPromotionStatementAdvancedParameter6?: number | string;
  designPromotionStatementAdvancedParameter7?: number | string;
  designPromotionStatementAdvancedParameter8?: number | string;
  designPromotionStatementAdvancedParameter9?: number | string;
  designPromotionStatementAdvancedParameter10?: number | string;
  designPromotionStatementAdvancedParameter11?: number | string;
  designPromotionStatementAdvancedParameter12?: number | string;
  designPromotionStatementAdvancedParameter13?: number | string;
  designPromotionStatementAdvancedParameter14?: number | string;
  designPromotionStatementAdvancedParameter15?: number | string;
  designPromotionStatementAdvancedParameter16?: number | string;
  designPromotionStatementAdvancedParameter17?: number | string;
  designPromotionStatementAdvancedParameter18?: number | string;
  designPromotionStatementAdvancedParameter19?: number | string;
  designPromotionStatementAdvancedParameter20?: number | string;
  designPromotionStatementAdvancedParameter21?: number | string;
  designPromotionStatementAdvancedParameter22?: number | string;
  designPromotionStatementAdvancedParameter23?: number | string;
  designPromotionStatementAdvancedParameter24?: number | string;
  designPromotionStatementAdvancedParameter25?: number | string;
  designPromotionStatementAdvancedParameter26?: number | string;
  designPromotionStatementAdvancedParameter27?: number | string;
  designPromotionStatementAdvancedParameter28?: number | string;
}

export interface SubGridProps {
  original: WorkflowSetupMethod;
  metadata: {
    designPromotionBasicInformation3: RefData[];
    designPromotionBasicInformation4: RefData[];
    designPromotionBasicInformation6: RefData[];
    designPromotionBasicInformation7: RefData[];
    designPromotionBasicInformation8: RefData[];
    designPromotionBasicInformation9: RefData[];
    designPromotionBasicInformation10: RefData[];
    designPromotionBasicInformation11: RefData[];
    designPromotionBasicInformation12: RefData[];
    designPromotionInterestAssessment9: RefData[];
    designPromotionInterestAssessment10: RefData[];
    designPromotionInterestAssessment11: RefData[];
    designPromotionInterestAssessment12: RefData[];
    designPromotionInterestAssessment13: RefData[];
    designPromotionInterestAssessment14: RefData[];
    designPromotionInterestAssessment15: RefData[];
    designPromotionInterestAssessment16: RefData[];
    designPromotionInterestAssessment17: RefData[];
    designPromotionInterestOverrides1: RefData[];
    designPromotionInterestOverrides2: RefData[];
    designPromotionInterestOverrides3: RefData[];
    designPromotionInterestOverrides4: RefData[];
    designPromotionInterestOverrides5: RefData[];
    designPromotionInterestOverrides6: RefData[];
    designPromotionInterestOverrides7: RefData[];
    designPromotionInterestOverrides8: RefData[];
    designPromotionRetailPath1: RefData[];
    designPromotionRetailPath4: RefData[];
    designPromotionReturnRevolve2: RefData[];
    designPromotionReturnRevolve3: RefData[];
    designPromotionReturnRevolve4: RefData[];
    designPromotionReturnRevolve6: RefData[];
    designPromotionStatementMessaging1: RefData[];
    designPromotionStatementMessaging2: RefData[];
    designPromotionStatementMessaging3: RefData[];
    designPromotionStatementMessaging4: RefData[];
    designPromotionStatementMessaging5: RefData[];
    // design promotion statement advanced parameter
    designPromotionStatementAdvancedParameter1: RefData[];
    designPromotionStatementAdvancedParameter3: RefData[];
    designPromotionStatementAdvancedParameter4: RefData[];
    designPromotionStatementAdvancedParameter5: RefData[];
    designPromotionStatementAdvancedParameter7: RefData[];
    designPromotionStatementAdvancedParameter8: RefData[];
    designPromotionStatementAdvancedParameter9: RefData[];
    designPromotionStatementAdvancedParameter10: RefData[];
    designPromotionStatementAdvancedParameter11: RefData[];
    designPromotionStatementAdvancedParameter12: RefData[];
    designPromotionStatementAdvancedParameter13: RefData[];
    designPromotionStatementAdvancedParameter14: RefData[];
    designPromotionStatementAdvancedParameter15: RefData[];
    designPromotionStatementAdvancedParameter16: RefData[];
    designPromotionStatementAdvancedParameter17: RefData[];
    designPromotionStatementAdvancedParameter18: RefData[];
    designPromotionStatementAdvancedParameter19: RefData[];
    designPromotionStatementAdvancedParameter20: RefData[];
    designPromotionStatementAdvancedParameter21: RefData[];
    designPromotionStatementAdvancedParameter22: RefData[];
    designPromotionStatementAdvancedParameter23: RefData[];
    designPromotionStatementAdvancedParameter24: RefData[];
    designPromotionStatementAdvancedParameter25: RefData[];
    designPromotionStatementAdvancedParameter26: RefData[];
    designPromotionStatementAdvancedParameter27: RefData[];
    designPromotionStatementAdvancedParameter28: RefData[];
  };
}
const SubRowGrid: React.FC<SubGridProps> = ({ original, metadata }) => {
  const {
    designPromotionBasicInformation3,
    designPromotionBasicInformation4,
    designPromotionBasicInformation6,
    designPromotionBasicInformation7,
    designPromotionBasicInformation8,
    designPromotionBasicInformation9,
    designPromotionBasicInformation10,
    designPromotionBasicInformation11,
    designPromotionBasicInformation12,
    designPromotionInterestAssessment9,
    designPromotionInterestAssessment10,
    designPromotionInterestAssessment11,
    designPromotionInterestAssessment12,
    designPromotionInterestAssessment13,
    designPromotionInterestAssessment14,
    designPromotionInterestAssessment15,
    designPromotionInterestAssessment16,
    designPromotionInterestAssessment17,
    designPromotionInterestOverrides1,
    designPromotionInterestOverrides2,
    designPromotionInterestOverrides3,
    designPromotionInterestOverrides4,
    designPromotionInterestOverrides5,
    designPromotionInterestOverrides6,
    designPromotionInterestOverrides7,
    designPromotionInterestOverrides8,
    designPromotionRetailPath1,
    designPromotionRetailPath4,
    designPromotionReturnRevolve2,
    designPromotionReturnRevolve3,
    designPromotionReturnRevolve4,
    designPromotionReturnRevolve6,
    designPromotionStatementMessaging1,
    designPromotionStatementMessaging2,
    designPromotionStatementMessaging3,
    designPromotionStatementMessaging4,
    designPromotionStatementMessaging5,
    designPromotionStatementAdvancedParameter1,
    designPromotionStatementAdvancedParameter3,
    designPromotionStatementAdvancedParameter4,
    designPromotionStatementAdvancedParameter5,
    designPromotionStatementAdvancedParameter7,
    designPromotionStatementAdvancedParameter8,
    designPromotionStatementAdvancedParameter9,
    designPromotionStatementAdvancedParameter10,
    designPromotionStatementAdvancedParameter11,
    designPromotionStatementAdvancedParameter12,
    designPromotionStatementAdvancedParameter13,
    designPromotionStatementAdvancedParameter14,
    designPromotionStatementAdvancedParameter15,
    designPromotionStatementAdvancedParameter16,
    designPromotionStatementAdvancedParameter17,
    designPromotionStatementAdvancedParameter18,
    designPromotionStatementAdvancedParameter19,
    designPromotionStatementAdvancedParameter20,
    designPromotionStatementAdvancedParameter21,
    designPromotionStatementAdvancedParameter22,
    designPromotionStatementAdvancedParameter23,
    designPromotionStatementAdvancedParameter24,
    designPromotionStatementAdvancedParameter25,
    designPromotionStatementAdvancedParameter26,
    designPromotionStatementAdvancedParameter27,
    designPromotionStatementAdvancedParameter28
  } = metadata;

  const convertedObject = methodParamsToObject(original);

  const { t } = useTranslation();

  const data: ParameterValueType = {
    designPromotionStatementMessaging1: designPromotionStatementMessaging1.find(
      o =>
        o.code ===
        convertedObject[
          MethodFieldParameterEnum.DesignPromotionStatementMessaging1
        ]
    )?.text,
    designPromotionStatementMessaging2: designPromotionStatementMessaging2.find(
      o =>
        o.code ===
        convertedObject[
          MethodFieldParameterEnum.DesignPromotionStatementMessaging2
        ]
    )?.text,
    designPromotionStatementMessaging3: designPromotionStatementMessaging3.find(
      o =>
        o.code ===
        convertedObject[
          MethodFieldParameterEnum.DesignPromotionStatementMessaging3
        ]
    )?.text,
    designPromotionStatementMessaging4: designPromotionStatementMessaging4.find(
      o =>
        o.code ===
        convertedObject[
          MethodFieldParameterEnum.DesignPromotionStatementMessaging4
        ]
    )?.text,
    designPromotionStatementMessaging5: designPromotionStatementMessaging5.find(
      o =>
        o.code ===
        convertedObject[
          MethodFieldParameterEnum.DesignPromotionStatementMessaging5
        ]
    )?.text,
    designPromotionReturnRevolve2: designPromotionReturnRevolve2.find(
      o =>
        o.code ===
        convertedObject[MethodFieldParameterEnum.DesignPromotionReturnRevolve2]
    )?.text,
    designPromotionReturnRevolve3: designPromotionReturnRevolve3.find(
      o =>
        o.code ===
        convertedObject[MethodFieldParameterEnum.DesignPromotionReturnRevolve3]
    )?.text,
    designPromotionReturnRevolve4: designPromotionReturnRevolve4.find(
      o =>
        o.code ===
        convertedObject[MethodFieldParameterEnum.DesignPromotionReturnRevolve4]
    )?.text,
    designPromotionReturnRevolve6: designPromotionReturnRevolve6.find(
      o =>
        o.code ===
        convertedObject[MethodFieldParameterEnum.DesignPromotionReturnRevolve6]
    )?.text,
    designPromotionReturnRevolve1:
      convertedObject[MethodFieldParameterEnum.DesignPromotionReturnRevolve1],
    designPromotionReturnRevolve5:
      convertedObject[MethodFieldParameterEnum.DesignPromotionReturnRevolve5],
    designPromotionRetailPath2:
      convertedObject[MethodFieldParameterEnum.DesignPromotionRetailPath2],
    designPromotionRetailPath3:
      convertedObject[MethodFieldParameterEnum.DesignPromotionRetailPath3],
    designPromotionRetailPath1: designPromotionRetailPath1.find(
      o =>
        o.code ===
        convertedObject[MethodFieldParameterEnum.DesignPromotionRetailPath1]
    )?.text,
    designPromotionRetailPath4: designPromotionRetailPath4.find(
      o =>
        o.code ===
        convertedObject[MethodFieldParameterEnum.DesignPromotionRetailPath4]
    )?.text,
    designPromotionInterestOverrides1: designPromotionInterestOverrides1.find(
      o =>
        o.code ===
        convertedObject[
          MethodFieldParameterEnum.DesignPromotionInterestOverrides1
        ]
    )?.text,
    designPromotionInterestOverrides2: designPromotionInterestOverrides2.find(
      o =>
        o.code ===
        convertedObject[
          MethodFieldParameterEnum.DesignPromotionInterestOverrides2
        ]
    )?.text,
    designPromotionInterestOverrides3: designPromotionInterestOverrides3.find(
      o =>
        o.code ===
        convertedObject[
          MethodFieldParameterEnum.DesignPromotionInterestOverrides3
        ]
    )?.text,
    designPromotionInterestOverrides4: designPromotionInterestOverrides4.find(
      o =>
        o.code ===
        convertedObject[
          MethodFieldParameterEnum.DesignPromotionInterestOverrides4
        ]
    )?.text,
    designPromotionInterestOverrides5: designPromotionInterestOverrides5.find(
      o =>
        o.code ===
        convertedObject[
          MethodFieldParameterEnum.DesignPromotionInterestOverrides5
        ]
    )?.text,
    designPromotionInterestOverrides6: designPromotionInterestOverrides6.find(
      o =>
        o.code ===
        convertedObject[
          MethodFieldParameterEnum.DesignPromotionInterestOverrides6
        ]
    )?.text,
    designPromotionInterestOverrides7: designPromotionInterestOverrides7.find(
      o =>
        o.code ===
        convertedObject[
          MethodFieldParameterEnum.DesignPromotionInterestOverrides7
        ]
    )?.text,
    designPromotionInterestOverrides8: designPromotionInterestOverrides8.find(
      o =>
        o.code ===
        convertedObject[
          MethodFieldParameterEnum.DesignPromotionInterestOverrides8
        ]
    )?.text,
    designPromotionInterestAssessment1:
      convertedObject[
        MethodFieldParameterEnum.DesignPromotionInterestAssessment1
      ],
    designPromotionInterestAssessment2:
      convertedObject[
        MethodFieldParameterEnum.DesignPromotionInterestAssessment2
      ],
    designPromotionInterestAssessment3:
      convertedObject[
        MethodFieldParameterEnum.DesignPromotionInterestAssessment3
      ],
    designPromotionInterestAssessment4:
      convertedObject[
        MethodFieldParameterEnum.DesignPromotionInterestAssessment4
      ],
    designPromotionInterestAssessment5:
      convertedObject[
        MethodFieldParameterEnum.DesignPromotionInterestAssessment5
      ],
    designPromotionInterestAssessment6:
      convertedObject[
        MethodFieldParameterEnum.DesignPromotionInterestAssessment6
      ],
    designPromotionInterestAssessment7:
      convertedObject[
        MethodFieldParameterEnum.DesignPromotionInterestAssessment7
      ],
    designPromotionInterestAssessment8:
      convertedObject[
        MethodFieldParameterEnum.DesignPromotionInterestAssessment8
      ],
    designPromotionInterestAssessment18:
      convertedObject[
        MethodFieldParameterEnum.DesignPromotionInterestAssessment18
      ],
    designPromotionBasicInformation1:
      convertedObject[
        MethodFieldParameterEnum.DesignPromotionBasicInformation1
      ],
    designPromotionBasicInformation2:
      convertedObject[
        MethodFieldParameterEnum.DesignPromotionBasicInformation2
      ],
    designPromotionBasicInformation5:
      convertedObject[
        MethodFieldParameterEnum.DesignPromotionBasicInformation5
      ],
    designPromotionBasicInformation13:
      convertedObject[
        MethodFieldParameterEnum.DesignPromotionBasicInformation13
      ],
    designPromotionBasicInformation3: designPromotionBasicInformation3.find(
      o =>
        o.code ===
        convertedObject[
          MethodFieldParameterEnum.DesignPromotionBasicInformation3
        ]
    )?.text,
    designPromotionBasicInformation4: designPromotionBasicInformation4.find(
      o =>
        o.code ===
        convertedObject[
          MethodFieldParameterEnum.DesignPromotionBasicInformation4
        ]
    )?.text,
    designPromotionBasicInformation6: designPromotionBasicInformation6.find(
      o =>
        o.code ===
        convertedObject[
          MethodFieldParameterEnum.DesignPromotionBasicInformation6
        ]
    )?.text,
    designPromotionBasicInformation7: designPromotionBasicInformation7.find(
      o =>
        o.code ===
        convertedObject[
          MethodFieldParameterEnum.DesignPromotionBasicInformation7
        ]
    )?.text,
    designPromotionBasicInformation8: designPromotionBasicInformation8.find(
      o =>
        o.code ===
        convertedObject[
          MethodFieldParameterEnum.DesignPromotionBasicInformation8
        ]
    )?.text,
    designPromotionBasicInformation9: designPromotionBasicInformation9.find(
      o =>
        o.code ===
        convertedObject[
          MethodFieldParameterEnum.DesignPromotionBasicInformation9
        ]
    )?.text,
    designPromotionBasicInformation10: designPromotionBasicInformation10.find(
      o =>
        o.code ===
        convertedObject[
          MethodFieldParameterEnum.DesignPromotionBasicInformation10
        ]
    )?.text,
    designPromotionBasicInformation11: designPromotionBasicInformation11.find(
      o =>
        o.code ===
        convertedObject[
          MethodFieldParameterEnum.DesignPromotionBasicInformation11
        ]
    )?.text,
    designPromotionBasicInformation12: designPromotionBasicInformation12.find(
      o =>
        o.code ===
        convertedObject[
          MethodFieldParameterEnum.DesignPromotionBasicInformation12
        ]
    )?.text,
    designPromotionInterestAssessment9: designPromotionInterestAssessment9.find(
      o =>
        o.code ===
        convertedObject[
          MethodFieldParameterEnum.DesignPromotionInterestAssessment9
        ]
    )?.text,
    designPromotionInterestAssessment11:
      designPromotionInterestAssessment11.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.DesignPromotionInterestAssessment11
          ]
      )?.text,
    designPromotionInterestAssessment10:
      designPromotionInterestAssessment10.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.DesignPromotionInterestAssessment10
          ]
      )?.text,
    designPromotionInterestAssessment12:
      designPromotionInterestAssessment12.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.DesignPromotionInterestAssessment12
          ]
      )?.text,
    designPromotionInterestAssessment13:
      designPromotionInterestAssessment13.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.DesignPromotionInterestAssessment13
          ]
      )?.text,
    designPromotionInterestAssessment14:
      designPromotionInterestAssessment14.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.DesignPromotionInterestAssessment14
          ]
      )?.text,
    designPromotionInterestAssessment15:
      designPromotionInterestAssessment15.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.DesignPromotionInterestAssessment15
          ]
      )?.text,
    designPromotionInterestAssessment16:
      designPromotionInterestAssessment16.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.DesignPromotionInterestAssessment16
          ]
      )?.text,
    designPromotionInterestAssessment17:
      designPromotionInterestAssessment17.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.DesignPromotionInterestAssessment17
          ]
      )?.text,
    designPromotionStatementAdvancedParameter1:
      designPromotionStatementAdvancedParameter1.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter1
          ]
      )?.text,
    designPromotionStatementAdvancedParameter2:
      convertedObject[
        MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter2
      ],
    designPromotionStatementAdvancedParameter3:
      designPromotionStatementAdvancedParameter3.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter3
          ]
      )?.text,
    designPromotionStatementAdvancedParameter4:
      designPromotionStatementAdvancedParameter4.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter4
          ]
      )?.text,
    designPromotionStatementAdvancedParameter5:
      designPromotionStatementAdvancedParameter5.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter5
          ]
      )?.text,
    designPromotionStatementAdvancedParameter6:
      convertedObject[
        MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter6
      ],
    designPromotionStatementAdvancedParameter7:
      designPromotionStatementAdvancedParameter7.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter7
          ]
      )?.text,
    designPromotionStatementAdvancedParameter8:
      designPromotionStatementAdvancedParameter8.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter8
          ]
      )?.text,
    designPromotionStatementAdvancedParameter9:
      designPromotionStatementAdvancedParameter9.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter9
          ]
      )?.text,
    designPromotionStatementAdvancedParameter10:
      designPromotionStatementAdvancedParameter10.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter10
          ]
      )?.text,
    designPromotionStatementAdvancedParameter11:
      designPromotionStatementAdvancedParameter11.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter11
          ]
      )?.text,
    designPromotionStatementAdvancedParameter12:
      designPromotionStatementAdvancedParameter12.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter12
          ]
      )?.text,
    designPromotionStatementAdvancedParameter13:
      designPromotionStatementAdvancedParameter13.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter13
          ]
      )?.text,
    designPromotionStatementAdvancedParameter14:
      designPromotionStatementAdvancedParameter14.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter14
          ]
      )?.text,
    designPromotionStatementAdvancedParameter15:
      designPromotionStatementAdvancedParameter15.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter15
          ]
      )?.text,
    designPromotionStatementAdvancedParameter16:
      designPromotionStatementAdvancedParameter16.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter16
          ]
      )?.text,
    designPromotionStatementAdvancedParameter17:
      designPromotionStatementAdvancedParameter17.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter17
          ]
      )?.text,
    designPromotionStatementAdvancedParameter18:
      designPromotionStatementAdvancedParameter18.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter18
          ]
      )?.text,
    designPromotionStatementAdvancedParameter19:
      designPromotionStatementAdvancedParameter19.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter19
          ]
      )?.text,
    designPromotionStatementAdvancedParameter20:
      designPromotionStatementAdvancedParameter20.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter20
          ]
      )?.text,
    designPromotionStatementAdvancedParameter21:
      designPromotionStatementAdvancedParameter21.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter21
          ]
      )?.text,
    designPromotionStatementAdvancedParameter22:
      designPromotionStatementAdvancedParameter22.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter22
          ]
      )?.text,
    designPromotionStatementAdvancedParameter23:
      designPromotionStatementAdvancedParameter23.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter23
          ]
      )?.text,
    designPromotionStatementAdvancedParameter24:
      designPromotionStatementAdvancedParameter24.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter24
          ]
      )?.text,
    designPromotionStatementAdvancedParameter25:
      designPromotionStatementAdvancedParameter25.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter25
          ]
      )?.text,
    designPromotionStatementAdvancedParameter26:
      designPromotionStatementAdvancedParameter26.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter26
          ]
      )?.text,
    designPromotionStatementAdvancedParameter27:
      designPromotionStatementAdvancedParameter27.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter27
          ]
      )?.text,
    designPromotionStatementAdvancedParameter28:
      designPromotionStatementAdvancedParameter28.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter28
          ]
      )?.text
  };

  const [expandedList, setExpandedList] = useState<ParameterListIds[]>([
    'Info_Basic'
  ]);

  return (
    <div className="p-20 overflow-hidden">
      <Grid
        togglable
        columns={newMethodColumns(t)}
        data={parameterList}
        subRow={({ original: _original }: MagicKeyValue) => (
          <SubGridPreviewMethod
            original={_original}
            metadata={data}
            methodType={original.methodType}
          />
        )}
        dataItemKey="id"
        expandedItemKey="id"
        expandedList={expandedList}
        onExpand={setExpandedList as any}
        toggleButtonConfigList={parameterList.map(
          mapGridExpandCollapse('id', t)
        )}
      />
    </div>
  );
};

export default SubRowGrid;
