import { BadgeColorType } from 'app/constants/enums';
import { mapGridExpandCollapse } from 'app/helpers';
import { Button, ColumnType, Grid, useTranslation } from 'app/_libraries/_dls';
import { isFunction, orderBy } from 'lodash';
import isEmpty from 'lodash.isempty';
import { useSelectElementMetadataForLC } from 'pages/_commons/redux/WorkflowSetup';
import {
  formatBadge,
  formatText,
  formatTruncate
} from 'pages/_commons/Utils/formatGridField';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { DesignPromotionsFormValue } from '.';
import SubRowGrid from './SubRowGrid';

const LateChargesSummary: React.FC<
  WorkflowSetupSummaryProps<DesignPromotionsFormValue>
> = ({ onEditStep, formValues, stepId, selectedStep }) => {
  const [expandedList, setExpandedList] = useState<string[]>([]);
  const { t } = useTranslation();
  const metadata = useSelectElementMetadataForLC();

  useEffect(() => {
    if (stepId !== selectedStep) return;

    setExpandedList([]);
  }, [stepId, selectedStep]);

  const methods = useMemo(
    () => orderBy(formValues?.methods || [], ['name'], ['asc']),
    [formValues?.methods]
  );

  const methodTypeToText = useCallback(
    (type: WorkflowSetupMethodType) => {
      switch (type) {
        case 'NEWVERSION':
          return t('txt_manage_penalty_fee_version_created');
        case 'MODELEDMETHOD':
          return t('txt_manage_penalty_fee_method_modeled');
      }
      return t('txt_manage_penalty_fee_method_created');
    },
    [t]
  );

  const columns: ColumnType[] = useMemo(
    () => [
      {
        id: 'methodName',
        Header: t('txt_method_name'),
        accessor: formatText(['name']),
        width: 105
      },
      {
        id: 'modelOrCreateFrom',
        Header: t('txt_manage_penalty_fee_modeled_or_create_from'),
        accessor: formatText(['modeledFrom', 'name']),
        width: 120
      },
      {
        id: 'comment',
        Header: t('txt_comment_area'),
        accessor: formatTruncate(['comment']),
        width: 312
      },
      {
        id: 'methodType',
        Header: t('txt_manage_penalty_fee_action_taken'),
        accessor: (data, idx) =>
          formatBadge(['methodType'], {
            colorType: BadgeColorType.MethodType,
            noBorder: true
          })(
            {
              ...data,
              methodType: methodTypeToText(data.methodType)
            },
            idx
          ),
        width: 153
      }
    ],
    [t, methodTypeToText]
  );

  const handleEdit = () => {
    isFunction(onEditStep) && onEditStep();
  };

  const handleOnExpand = (dataKeyList: string[]) => {
    setExpandedList(
      isEmpty(dataKeyList) ? [] : [dataKeyList[dataKeyList.length - 1]]
    );
  };

  return (
    <div className="position-relative">
      <div className="absolute-top-right mt-n26 mr-n8">
        <Button variant="outline-primary" size="sm" onClick={handleEdit}>
          {t('txt_edit')}
        </Button>
      </div>
      <div className="pt-16">
        <Grid
          togglable
          columns={columns}
          data={methods}
          subRow={({ original }: any) => (
            <SubRowGrid original={original} metadata={metadata} />
          )}
          dataItemKey="rowId"
          toggleButtonConfigList={methods.map(
            mapGridExpandCollapse('rowId', t)
          )}
          expandedItemKey={'rowId'}
          expandedList={expandedList}
          onExpand={handleOnExpand}
          scrollable
        />
      </div>
    </div>
  );
};

export default LateChargesSummary;
