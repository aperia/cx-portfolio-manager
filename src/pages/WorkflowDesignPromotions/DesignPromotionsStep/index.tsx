import {
  BadgeColorType,
  FormatTime,
  MethodFieldParameterEnum,
  ServiceSubjectSection
} from 'app/constants/enums';
import {
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { formatTimeDefault, mapGridExpandCollapse } from 'app/helpers';
import { usePaginationGrid, useUnsavedChangeRegistry } from 'app/hooks';
import {
  Button,
  ColumnType,
  Grid,
  InlineMessage,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty, orderBy } from 'lodash';
import {
  actionsWorkflowSetup,
  useSelectElementMetadataForPW
} from 'pages/_commons/redux/WorkflowSetup';
import {
  formatBadge,
  formatText,
  formatTruncate
} from 'pages/_commons/Utils/formatGridField';
import Paging from 'pages/_commons/Utils/Paging';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';
import ActionButtons from './ActionButtons';
import AddNewMethodModal from './AddNewMethodModal';
import MethodListModal from './MethodListModal';
import RemoveMethodModal from './RemoveMethodModal';
import SubRowGrid from './SubRowGrid';
import Summary from './Summary';
import parseFormValues from './_parseFormValues';

export interface DesignPromotionsFormValue {
  isValid?: boolean;

  methods?: (WorkflowSetupMethod & { rowId?: number })[];
}
const ConfigParametersStep: React.FC<
  WorkflowSetupProps<DesignPromotionsFormValue>
> = ({
  stepId,
  selectedStep,
  savedAt,
  isStuck,

  formValues,
  setFormValues
}) => {
  const { t } = useTranslation();

  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;

  const dispatch = useDispatch();
  const [expandedList, setExpandedList] = useState<string[]>([]);
  const [showAddNewMethodModal, setShowAddNewMethodModal] = useState(false);
  const [isCreateNewVersion, setIsCreateNewVersion] = useState(false);
  const [isMethodToModel, setIsMethodToModel] = useState(false);
  const [removeMethodRowId, setRemoveMethodRowId] =
    useState<undefined | number>(undefined);
  const [editMethodRowId, setEditMethodRowId] =
    useState<undefined | number>(undefined);
  const [editMethodBackId, setEditMethodBackId] =
    useState<undefined | number>(undefined);
  const [hasChange, setHasChange] = useState(false);
  const [keepCreateMethodState, setKeepCreateMethodState] =
    useState<any | undefined>(undefined);

  const metadata = useSelectElementMetadataForPW();
  const methods = useMemo(
    () => orderBy(formValues?.methods || [], ['name'], ['asc']),
    [formValues?.methods]
  );
  const methodNames = useMemo(() => methods.map(i => i.name), [methods]);

  const prevExpandedList = useRef<string[]>();

  useEffect(() => {
    prevExpandedList.current = expandedList;
  }, [expandedList]);

  const onRemoveMethod = useCallback((idx: number) => {
    setRemoveMethodRowId(idx);
  }, []);

  const onEditMethod = useCallback((idx: number) => {
    setEditMethodRowId(idx);
  }, []);

  const onClickAddNewMethod = () => {
    setShowAddNewMethodModal(true);
  };
  const onClickMethodToModel = () => {
    setIsMethodToModel(true);
  };
  const onClickCreateNewVersion = () => {
    setIsCreateNewVersion(true);
  };
  const closeMethodListModalModal = () => {
    setIsCreateNewVersion(false);
    setIsMethodToModel(false);
  };

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__DESIGN_PROMOTIONS__PARAMETERS,
      priority: 1
    },
    [hasChange]
  );

  const methodTypeToText = useCallback(
    (type: WorkflowSetupMethodType) => {
      switch (type) {
        case 'NEWVERSION':
          return t('txt_manage_penalty_fee_version_created');
        case 'MODELEDMETHOD':
          return t('txt_manage_penalty_fee_method_modeled');
      }
      return t('txt_manage_penalty_fee_method_created');
    },
    [t]
  );

  const columns: ColumnType[] = useMemo(
    () => [
      {
        id: 'methodName',
        Header: t('txt_method_name'),
        accessor: formatText(['name']),
        width: 105
      },
      {
        id: 'modelOrCreateFrom',
        Header: t('txt_manage_penalty_fee_modeled_or_create_from'),
        accessor: formatText(['modeledFrom', 'name']),
        width: 120
      },
      {
        id: 'comment',
        Header: t('txt_comment_area'),
        accessor: formatTruncate(['comment']),
        width: 312
      },
      {
        id: 'methodType',
        Header: t('txt_manage_penalty_fee_action_taken'),
        accessor: (data, idx) =>
          formatBadge(['methodType'], {
            colorType: BadgeColorType.MethodType,
            noBorder: true
          })(
            {
              ...data,
              methodType: methodTypeToText(data.methodType)
            },
            idx
          ),
        width: 153
      },
      {
        id: 'actions',
        Header: t('txt_actions'),
        accessor: (data: any) => (
          <div className="d-flex justify-content-center">
            <Button
              size="sm"
              variant="outline-primary"
              onClick={() => onEditMethod(data.rowId)}
            >
              {t('txt_edit')}
            </Button>
            <Button
              size="sm"
              variant="outline-danger"
              className="ml-8"
              onClick={() => onRemoveMethod(data.rowId)}
            >
              {t('txt_delete')}
            </Button>
          </div>
        ),
        className: 'text-center',
        cellBodyProps: { className: 'py-8' },
        width: 142
      }
    ],
    [t, methodTypeToText, onEditMethod, onRemoveMethod]
  );

  const [minDateSetting, setMinDateSetting] = useState();
  const [maxDateSetting, setMaxDateSetting] = useState();

  const minDate = useMemo(() => {
    return new Date(formatTimeDefault(minDateSetting!, FormatTime.Date));
  }, [minDateSetting]);

  const maxDate = useMemo(() => {
    return new Date(formatTimeDefault(maxDateSetting!, FormatTime.Date));
  }, [maxDateSetting]);

  const initFields = useCallback(async () => {
    const responseDateRange = await Promise.resolve(
      dispatch(actionsWorkflowSetup.getWorkflowEffectiveDateOptions({}))
    );

    const { maxDate: maxDateRes, minDate: minDateRes } =
      (responseDateRange as any)?.payload?.data || {};

    setMinDateSetting(minDateRes);
    setMaxDateSetting(maxDateRes);
  }, [dispatch]);

  useEffect(() => {
    initFields();
  }, [initFields]);

  useEffect(() => {
    dispatch(
      actionsWorkflowSetup.getWorkflowMetadata([
        MethodFieldParameterEnum.DesignPromotionBasicInformation3,
        MethodFieldParameterEnum.DesignPromotionBasicInformation4,
        MethodFieldParameterEnum.DesignPromotionBasicInformation6,
        MethodFieldParameterEnum.DesignPromotionBasicInformation7,
        MethodFieldParameterEnum.DesignPromotionBasicInformation8,
        MethodFieldParameterEnum.DesignPromotionBasicInformation9,
        MethodFieldParameterEnum.DesignPromotionBasicInformation10,
        MethodFieldParameterEnum.DesignPromotionBasicInformation11,
        MethodFieldParameterEnum.DesignPromotionBasicInformation12,
        MethodFieldParameterEnum.DesignPromotionInterestAssessment9,
        MethodFieldParameterEnum.DesignPromotionInterestAssessment10,
        MethodFieldParameterEnum.DesignPromotionInterestAssessment11,
        MethodFieldParameterEnum.DesignPromotionInterestAssessment12,
        MethodFieldParameterEnum.DesignPromotionInterestAssessment13,
        MethodFieldParameterEnum.DesignPromotionInterestAssessment14,
        MethodFieldParameterEnum.DesignPromotionInterestAssessment15,
        MethodFieldParameterEnum.DesignPromotionInterestAssessment16,
        MethodFieldParameterEnum.DesignPromotionInterestAssessment17,
        MethodFieldParameterEnum.DesignPromotionInterestOverrides1,
        MethodFieldParameterEnum.DesignPromotionInterestOverrides2,
        MethodFieldParameterEnum.DesignPromotionInterestOverrides3,
        MethodFieldParameterEnum.DesignPromotionInterestOverrides4,
        MethodFieldParameterEnum.DesignPromotionInterestOverrides5,
        MethodFieldParameterEnum.DesignPromotionInterestOverrides6,
        MethodFieldParameterEnum.DesignPromotionInterestOverrides7,
        MethodFieldParameterEnum.DesignPromotionInterestOverrides8,
        MethodFieldParameterEnum.DesignPromotionRetailPath1,
        MethodFieldParameterEnum.DesignPromotionRetailPath4,
        MethodFieldParameterEnum.DesignPromotionReturnRevolve2,
        MethodFieldParameterEnum.DesignPromotionReturnRevolve3,
        MethodFieldParameterEnum.DesignPromotionReturnRevolve4,
        MethodFieldParameterEnum.DesignPromotionReturnRevolve6,
        MethodFieldParameterEnum.DesignPromotionStatementMessaging1,
        MethodFieldParameterEnum.DesignPromotionStatementMessaging2,
        MethodFieldParameterEnum.DesignPromotionStatementMessaging3,
        MethodFieldParameterEnum.DesignPromotionStatementMessaging4,
        MethodFieldParameterEnum.DesignPromotionStatementMessaging5,
        //design promotion statement advanced parameter
        MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter1,
        MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter2,
        MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter3,
        MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter4,
        MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter5,
        MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter6,
        MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter7,
        MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter8,
        MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter9,
        MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter10,
        MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter11,
        MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter12,
        MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter13,
        MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter14,
        MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter15,
        MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter16,
        MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter17,
        MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter18,
        MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter19,
        MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter20,
        MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter21,
        MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter22,
        MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter23,
        MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter24,
        MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter25,
        MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter26,
        MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter27,
        MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter28
      ])
    );
  }, [dispatch]);

  const {
    total,
    currentPage,
    currentPageSize,
    pageData,
    onPageChange,
    onPageSizeChange
  } = usePaginationGrid(methods);

  useEffect(() => {
    const isValid = (formValues?.methods?.length || 0) > 0;
    if (formValues.isValid === isValid) return;

    keepRef.current.setFormValues({ isValid });
  }, [formValues]);

  useEffect(() => {
    if (stepId !== selectedStep) {
      setExpandedList([]);
    }
    setHasChange(false);
  }, [stepId, selectedStep, savedAt]);

  const handleOnExpand = (dataKeyList: string[]) => {
    setExpandedList(isEmpty(dataKeyList) ? [] : dataKeyList);
  };

  const renderGrid = useMemo(() => {
    if (isEmpty(pageData)) return;
    return (
      <div className="pt-16">
        <Grid
          togglable
          columns={columns}
          data={pageData}
          subRow={({ original }: any) => (
            <SubRowGrid original={original} metadata={metadata} />
          )}
          dataItemKey="rowId"
          toggleButtonConfigList={pageData.map(
            mapGridExpandCollapse('rowId', t)
          )}
          expandedItemKey={'rowId'}
          expandedList={expandedList}
          onExpand={handleOnExpand}
          scrollable
        />
        {total > 10 && (
          <Paging
            totalItem={total}
            pageSize={currentPageSize}
            page={currentPage}
            onChangePage={onPageChange}
            onChangePageSize={onPageSizeChange}
          />
        )}
      </div>
    );
  }, [
    columns,
    currentPage,
    currentPageSize,
    expandedList,
    metadata,
    onPageChange,
    onPageSizeChange,
    pageData,
    t,
    total
  ]);

  return (
    <div>
      {isStuck && (
        <InlineMessage className="mb-16 mt-24" variant="danger" withIcon>
          {t('txt_step_stuck_move_forward_message')}
        </InlineMessage>
      )}
      <div className="mt-8 pb-24 border-bottom color-grey">
        <p>{t('txt_workflow_design_promotions_configure_parameters_1')}</p>
      </div>
      <div className="mt-24">
        <ActionButtons
          small={!isEmpty(methods)}
          title="txt_method_list"
          onClickAddNewMethod={onClickAddNewMethod}
          onClickMethodToModel={onClickMethodToModel}
          onClickCreateNewVersion={onClickCreateNewVersion}
        />
      </div>
      {renderGrid}
      {removeMethodRowId !== undefined && (
        <RemoveMethodModal
          id="removeMethod"
          show
          methodName={methods.find(i => i.rowId === removeMethodRowId)?.name}
          onClose={method => {
            if (method) {
              setFormValues({
                methods: methods.filter(i => i.rowId !== removeMethodRowId)
              });
              setHasChange(true);
            }
            setRemoveMethodRowId(undefined);
          }}
        />
      )}
      {editMethodRowId !== undefined && (
        <AddNewMethodModal
          id="addNewMethod"
          show
          isCreate={false}
          method={methods.find(i => i.rowId === editMethodRowId)}
          methodNames={methodNames}
          minDate={minDate}
          maxDate={maxDate}
          onClose={(method, isBack, keepState) => {
            if (method) {
              method.serviceSubjectSection = ServiceSubjectSection.DLO;
              const newMethds = methods.map(item => {
                if (item.rowId === editMethodRowId) return method;
                return item;
              });
              setFormValues({ methods: newMethds });
              setHasChange(true);

              if (isBack) {
                setEditMethodBackId(editMethodRowId);
                setKeepCreateMethodState(keepState);
              }
            }
            setEditMethodRowId(undefined);
          }}
        />
      )}
      {editMethodBackId && (
        <MethodListModal
          id="editMethodFromModel"
          show
          methodNames={methodNames}
          keepState={keepCreateMethodState}
          isCreateNewVersion={
            methods.find(i => i.rowId === editMethodBackId)!.methodType ===
            'NEWVERSION'
          }
          defaultMethod={methods.find(i => i.rowId === editMethodBackId)}
          onClose={method => {
            if (method) {
              method.serviceSubjectSection = ServiceSubjectSection.DLO;
              const newMethds = methods.map(item => {
                if (item.rowId === editMethodBackId) return method;
                return item;
              });
              setFormValues({ methods: newMethds });
              setHasChange(true);
            }
            setEditMethodBackId(undefined);
            setKeepCreateMethodState(undefined);
          }}
        />
      )}
      {(isCreateNewVersion || isMethodToModel) && (
        <MethodListModal
          id="addMethodFromModel"
          show
          methodNames={methodNames}
          isCreateNewVersion={isCreateNewVersion}
          onClose={newMethod => {
            if (newMethod) {
              const rowId = Date.now();
              setFormValues({
                methods: [
                  {
                    ...newMethod,
                    rowId,
                    serviceSubjectSection: ServiceSubjectSection.DLO
                  },
                  ...methods
                ]
              });
              setExpandedList(
                prevExpandedList?.current?.concat([rowId as any]) as any
              );
              setHasChange(true);
            }
            closeMethodListModalModal();
          }}
        />
      )}
      {showAddNewMethodModal && (
        <AddNewMethodModal
          id="addNewMethod"
          show
          methodNames={methodNames}
          minDate={minDate}
          maxDate={maxDate}
          onClose={method => {
            if (method) {
              const rowId = Date.now();
              setFormValues({
                methods: [
                  {
                    ...method,
                    rowId,
                    serviceSubjectSection: ServiceSubjectSection.DLO
                  },
                  ...methods
                ]
              });
              setExpandedList(
                prevExpandedList?.current?.concat([rowId as any]) as any
              );
              setHasChange(true);
            }
            setShowAddNewMethodModal(false);
          }}
        />
      )}
    </div>
  );
};

const ExtraStaticConfigParametersStep =
  ConfigParametersStep as WorkflowSetupStaticProp;

ExtraStaticConfigParametersStep.summaryComponent = Summary;
ExtraStaticConfigParametersStep.defaultValues = { isValid: false, methods: [] };
ExtraStaticConfigParametersStep.parseFormValues = parseFormValues;

export default ExtraStaticConfigParametersStep;
