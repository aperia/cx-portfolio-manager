export const ONLINE_PCF = {
  // design promotions basic
  DesignPromotionBasicInformation1: 'Description',
  DesignPromotionBasicInformation2: 'Alternate Description',
  DesignPromotionBasicInformation3: 'Description Display',
  DesignPromotionBasicInformation4: 'Type',
  DesignPromotionBasicInformation5: 'Start Date - End Date',
  DesignPromotionBasicInformation6: '',
  DesignPromotionBasicInformation7: 'Credit Application Group',
  DesignPromotionBasicInformation8: 'Group Identifier',
  DesignPromotionBasicInformation9: 'Multi Ticket Criteria',
  DesignPromotionBasicInformation10: 'Multi Ticket Terms Creation',
  DesignPromotionBasicInformation11: 'Promo Balance Transfer',
  DesignPromotionBasicInformation12: 'Promotion Return Application Option',
  DesignPromotionBasicInformation13: 'Number of Retention Months',
  // design promotions Interest Assessment and Fees
  DesignPromotionInterestAssessment1: 'Regular Annual Rate',
  DesignPromotionInterestAssessment2: 'Intro Annual Rate',
  DesignPromotionInterestAssessment3: 'Promotional Interest Delay Date',
  DesignPromotionInterestAssessment4:
    'Promotional Interest Delay Number of Days',
  DesignPromotionInterestAssessment5:
    'Promotional Interest Delay Number of Cycles',
  DesignPromotionInterestAssessment6: 'Promotional Cash Option Number of Days',
  DesignPromotionInterestAssessment7:
    'Promotional Cash Option Number of Cycles',
  DesignPromotionInterestAssessment8: 'Payoff Amount',
  DesignPromotionInterestAssessment9: 'Promotion Payoff Exception Option',
  DesignPromotionInterestAssessment10: 'Merchand Item Fees (CP/IO/MI) Set 1',
  DesignPromotionInterestAssessment11: `Merchand Item Fees (CP/IO/MI) Set 2`,
  DesignPromotionInterestAssessment12: `Merchand Item Fees (CP/IO/MI) Set 3`,
  DesignPromotionInterestAssessment13: `Cash Adv Item Fees (CP/IO/CI) Set 1`,
  DesignPromotionInterestAssessment14: `Cash Adv Item Fees (CP/IO/CI) Set 2`,
  DesignPromotionInterestAssessment15: `Cash Adv Item Fees (CP/IO/CI) Set 3`,
  DesignPromotionInterestAssessment16: 'Promo Method Reload',
  DesignPromotionInterestAssessment17: 'Promotion Interest Override I/E',
  DesignPromotionInterestAssessment18: 'Reset Date',
  // design promotions Interest Overrides
  DesignPromotionInterestOverrides1: 'Base Interest Set 1',
  DesignPromotionInterestOverrides2: 'Base Interest Set 2',
  DesignPromotionInterestOverrides3: 'Base Interest Set 3',
  DesignPromotionInterestOverrides4: `Payoff Exceptions (CP/IC/PE) Set 1`,
  DesignPromotionInterestOverrides5: `Payoff Exceptions (CP/IC/PE) Set 2`,
  DesignPromotionInterestOverrides6: `Payoff Exceptions (CP/IC/PE) Set 3`,
  DesignPromotionInterestOverrides7: 'Base Rate for Intro Promotion',
  DesignPromotionInterestOverrides8: 'Base Rate for Regular Annual Rate',
  // design promotions Retail Path
  DesignPromotionRetailPath1: 'R Path Promotion Return Indicator',
  DesignPromotionRetailPath2:
    'R Path Return Start Date and R Path Return End Date',
  DesignPromotionRetailPath3: 'Merchant Discount',
  DesignPromotionRetailPath4: 'Promo Discount Calculation Code',
  // design promotions Return-to-Revolve
  DesignPromotionReturnRevolve1: 'Days Delinquent',
  DesignPromotionReturnRevolve2: 'Collapse Return to Revolving Option',
  DesignPromotionReturnRevolve3: 'Collapse Expiration Date Option',
  DesignPromotionReturnRevolve4: 'Dlnq Rtr Timing Code',
  DesignPromotionReturnRevolve5: 'RTR Delay Number of Days',
  DesignPromotionReturnRevolve6: 'Return to Revolving Base Interest',
  // design promotions Statement Messaging
  DesignPromotionStatementMessaging1: 'Regular Text ID',
  DesignPromotionStatementMessaging2: 'Delay/Intro Text ID',
  DesignPromotionStatementMessaging3: 'Introductory Message Display Control',
  DesignPromotionStatementMessaging4: 'Regular Message Display Control',
  DesignPromotionStatementMessaging5: 'Description From Text ID - DT/DD',
  // DesignPromotionStatementAdvanced
  DesignPromotionStatementAdvancedParameter1: 'Delay Payment Start',
  DesignPromotionStatementAdvancedParameter2:
    'Introductory Minimum Payments Delay Number of Days',
  DesignPromotionStatementAdvancedParameter3: 'Internal Status Include Control',
  DesignPromotionStatementAdvancedParameter4: 'Promo Expiration Message',
  DesignPromotionStatementAdvancedParameter5:
    'Expiring Promotion Notification Months',
  DesignPromotionStatementAdvancedParameter6:
    'Introductory Minimum Payments Delay Number of Cycles',
  DesignPromotionStatementAdvancedParameter7: 'Interest on Unbilled Interest',
  DesignPromotionStatementAdvancedParameter8: 'Delay Interst Start',
  DesignPromotionStatementAdvancedParameter9: 'Excl Cash Opt from 2Cyc ADB',
  DesignPromotionStatementAdvancedParameter10:
    'Intro Interest Dflt (CP/IC/ID) Set 1',
  DesignPromotionStatementAdvancedParameter11:
    'Intro Interest Dflt (CP/IC/ID) Set 2',
  DesignPromotionStatementAdvancedParameter12:
    'Intro Interest Dflt (CP/IC/ID) Set 3',
  DesignPromotionStatementAdvancedParameter13:
    'Interest Defaults (CP/IC/ID) Set 1',
  DesignPromotionStatementAdvancedParameter14:
    'Interest Defaults (CP/IC/ID) Set 2',
  DesignPromotionStatementAdvancedParameter15:
    'Interest Defaults (CP/IC/ID) Set 3',
  DesignPromotionStatementAdvancedParameter16:
    'Minimum Payment (PL/RT/MP) Set 1',
  DesignPromotionStatementAdvancedParameter17:
    'Minimum Payment (PL/RT/MP) Set 2',
  DesignPromotionStatementAdvancedParameter18:
    'Minimum Payment (PL/RT/MP) Set 3',
  DesignPromotionStatementAdvancedParameter19: 'Associate w/Plan (PL/RT/PA)',
  DesignPromotionStatementAdvancedParameter20:
    'Zero Interest Extend Expiration CD',
  DesignPromotionStatementAdvancedParameter21:
    'Payoff Exception 17 Include/Exclude',
  DesignPromotionStatementAdvancedParameter22: 'Intro Promotion Load Time',
  DesignPromotionStatementAdvancedParameter23: 'Regular Annual Load Time',
  DesignPromotionStatementAdvancedParameter24: 'CPOCPP Rtr Usage',
  DesignPromotionStatementAdvancedParameter25: 'Billed Pay Due NM Code',
  DesignPromotionStatementAdvancedParameter26: 'Reinstate MPD Terms CD',
  DesignPromotionStatementAdvancedParameter27: 'Promo Billed Interest CD',
  DesignPromotionStatementAdvancedParameter28:
    'Stmt Promo Disp Mthd ID (PL RT SD)'
};

export const MORE_INFO = {
  // design promotions basic
  DesignPromotionBasicInformation1:
    'The Promotion Description parameter provides a description of a specific Transaction Level Processing service (TLP service) promotion. This 30-character, free-form text will appear as the second detail line of a transaction if you set the Description Display parameter in this section to 1.',
  DesignPromotionBasicInformation2:
    'The Alternate Description parameter provides a description of a specific TLP promotion like the Promotion Description parameter. This will appear as the second detail line of a transaction if you set the Promotion Description Display parameter in this section to 2.',
  DesignPromotionBasicInformation3:
    'The Promotion Description Display parameter determines what appears on your statement - Alternate Description or regular Promotion Description.',
  DesignPromotionBasicInformation4:
    'The Promotion Type parameter determines whether the promotional transaction posts to a promotion as an individual balance (single-ticket) or whether it posts with transactions that qualify for the same promotional balance (multi-ticket). This parameter also determines whether the promotional balance remains separate from the standard balance until the promotion is paid off or is added to the standard balance after the promotion period expires.',
  DesignPromotionBasicInformation5: `The date range to start and end the promotion. If you're using DMM tables to assign promotional terms, you set these values in the table`,
  DesignPromotionBasicInformation6:
    'The Credit Application Options parameter determines whether or not CREDIT_APPL_GROUP PROMOTION CREDIT APPLICATION GROUP and GROUP IDENTIFIER appears in the UI.',
  DesignPromotionBasicInformation7:
    'The Credit Application Group parameter identifies the payment group, which determines how payments are applied to promotional balances that have not been assigned a unique minimum payment. Establish your credit application groups using the payment application sequence parameters in the Credit Application section (CP PO CA) of the Product Control File.',
  DesignPromotionBasicInformation8:
    'The Credit Application Group ID parameter identifies the  client-defined name assigned to the plan balances you wish to group together for processing using Rules Management processing',
  DesignPromotionBasicInformation9: `The Multi-Ticket Criteria parameter determines whether qualifying sales transactions from multiple days or promotional periods will post to the same promotional balance for the duration of a promotion. Only enter this field if Multi-ticket promotion is chosen in Promotion Type field.
  When you hover over "related fields," you will see the following fields and the values in said fields:
  Promotional Cash Option Number of Days
  Promotional Cash Option Number of Months
  Promotional Cash Option Number of Cycles
  Promotional Cash Option End Date (do not display for choice zero)
  Promotional Interest Delay Number of Days
  Promotional Interest Delay Number of Months
  Promotional Interest Delay Number of Cycles
  Promotional Interest Delay End Date (do not display for choice zero)
  Introductory Minimum Payments Delay Number of Days
  Introductory Minimum Payments Delay Number of Months
  Introductory Minimum Payments Delay Number of Cycles
  Introductory Minimum Payments Delay End Date (do not display for choice zero)`,
  DesignPromotionBasicInformation10: `The Multi-Ticket Terms Creation parameter determines how the System will post a transaction that qualifies for an existing multi-ticket promotion when the promotional balance is in protected status.
  If you choose option 1, the newly created multi-ticket promotion will retain the introductory rates and dates associated with the original multi-ticket promotional balance, but will apply the current terms in the Promotion Controls section. You need to choose an option here if this is a multi-ticket promotion.`,
  DesignPromotionBasicInformation11: `The Balance Transfer Promotion Option parameter determines whether the System defines the promotion is defined as a balance transfer. IE: "if you transfer your balance over, we'll give you %% APR for X Months!"`,
  DesignPromotionBasicInformation12: `The Promotion Return Application Option parameter determines whether returns for promotional transactions processed on the T retail path and cardholder path for the Transaction Level Processing service are applied to the promotional balance.`,
  DesignPromotionBasicInformation13: `The Number of Retention Months parameter determines the length of time you retain zero-balance promotional balances at the promotional level. Set this parameter from 02 to 99 to establish the number of months you want to retain a zero-balance promotional balance for a particular promotion.`,
  // design promotions Interest Assessment and Fees
  DesignPromotionInterestAssessment1:
    'The Promotional Interest Rate parameter determines the interest rate for transactions after the introductory period has expired or during a cash option period. Example promotional terms would be, "you will get 8% interest for the next 12 months.',
  DesignPromotionInterestAssessment2: `The Introductory Promotional Interest Rate parameter determines the interest rate for an introductory period - "for the first six months of spend within the cardholder's promotional terms, the interest on the balance is 8%."
  The Intro Annual Rate parameter determines the interest rate for transactions during a time period you specify in:
  Promotional Interest Delay Number of Days
  Promotional Interest Delay Number of Cycles`,
  DesignPromotionInterestAssessment3: `The Promotional Interest Delay Date sets the date when the introductory interest rate expires. If you set this field, you cannot enter a value in:
  Promotional Interest Delay Number of Days
  Promotional Interest Delay Number of Cycles
  Promotional Cash Option Number of Days
  Promotional Cash Option Number of Cycles`,
  DesignPromotionInterestAssessment4: `The Promotional Interest Delay Number of Days parameter determines the number of days the introductory interest rate for the promotion will be in effect. If you set this parameter to a nonzero value, set the following parameters to zeros:
    Promotional Interest Delay Date
    Promotional Interest Delay Number of Cycles
    Promotional Cash Option Number of Days
    Promotional Cash Option Number of Cycles`,
  DesignPromotionInterestAssessment5: `The Promotional Interest Delay Number of Cycles parameter determines the number of cycles the introductory interest rate for the promotion will be in effect. If you set this parameter to a value other than zeros, set the following parameters to zeros.
    Promotional Interest Delay Date
    Promotional Interest Delay Number of Days
    Promotional Cash Option Number of Days
    Promotional Cash Option Number of Cycles`,
  DesignPromotionInterestAssessment6:
    'The Promotional Cash Option Number of Days parameter determines the number of days from the promotional transaction date during which a cardholder must pay off a promotional balance to avoid interest charges. If the promotional balance is not paid in full during the cycle following the original transaction post date, the cardholder is billed for all interest that accrued during the life of the promotion. The System edits this parameter for numeric values. This must be a single-ticket promotion in order for cash options to to be applied.',
  DesignPromotionInterestAssessment7: `The Promotional Cash Option Number of Cycles parameter determines the number of cycles for the Promotional Cash Option Number of Cycles parameter which determines the number of cycles from the last statement date that a cardholder has to pay off a promotional balance to avoid interest charges.
    For deferred interest - no interest as long as you pay off before the end date - but interest is accruing over that time and all the interest accrued is assessed if you don't pay down the full balance by end-of-promotion period defined in this field.
    If you set this parameter to a value other than zeros, set the following parameters to zeros:
    Promotional Interest Delay Date
    Promotional Interest Delay Number of Days
    Promotional Interest Delay Number of Months
    Promotional Cash Option Number of Days`,
  DesignPromotionInterestAssessment8: `The Payoff Amount parameter determines the fixed amount to be considered payoff of a promotion. The System edits this parameter for numeric values.`,
  DesignPromotionInterestAssessment9:
    'The Promotion Payoff Exception Options parameter determines whether to flag promotional balances for exclusion from the required payoff balance.',
  DesignPromotionInterestAssessment10: `The Merchand Item Fees (CP IO MI) Set 1 parameter contains the method name for the Merchandise Item Charges (CP IO MI) section of the PCF that determines the merchandise item charge used for transactions that qualify for this promotion.`,
  DesignPromotionInterestAssessment11: `The Merchand Item Fees (CP IO MI) Set 2 parameter contains the method name for the Merchandise Item Charges (CP IO MI) section of the PCF that determines the merchandise item charge used for transactions that qualify for this promotion.`,
  DesignPromotionInterestAssessment12: `The Merchand Item Fees (CP IO MI) Set 3 parameter contains the method name for the Merchandise Item Charges (CP/IO/MI) section of the PCF that determines the merchandise item charge used for transactions that qualify for this promotion.`,
  DesignPromotionInterestAssessment13: `The Cash Adv Item Fees (CP IO CI) Set 1 parameter contains the method name for the Cash Advance Item Charges (CP IO CI) section of the Product Control File that determines the cash advance item charge used for transactions that qualify for this promotion`,
  DesignPromotionInterestAssessment14: `The Cash Adv Item Fees (CP IO CI) Set 2 parameter contains the method name for the Cash Advance Item Charges (CP IO CI) section of the Product Control File that determines the cash advance item charge used for transactions that qualify for this promotion`,
  DesignPromotionInterestAssessment15: `The Cash Adv Item Fees (CP IO CI) Set 3 parameter contains the method name for the Cash Advance Item Charges (CP IO CI) section of the Product Control File that determines the cash advance item charge used for transactions that qualify for this promotion`,
  DesignPromotionInterestAssessment16: `The Promotion Method Reload parameter determines the pricing terms used to process the account’s promotional balance`,
  DesignPromotionInterestAssessment17: `The Promotion Interest Override I/E parameter controls whether the System allows the standard interest rate to override the promotional or protected promotional interest rate.This parameter works in conjunction with options 4 and 5 in the Promotion Interest Override parameter in the Interest Methods section (CP IC IM). If you set the Promotion Interest Override parameter to an option other than 4 or 5, this parameter does not control interest overrides.`,
  DesignPromotionInterestAssessment18: `The Reset Date parameter identifies the date (MM/DD/YY) the System uses to reset to current promotional terms on all transactions that have posted to this promotion`,
  // design promotions Interest Overrides
  DesignPromotionInterestOverrides1: `The Base Interest Set 1 parameter defines interest after the introductory rate of your promotion. If there is no introductory rate, this will be the applied interest rate method overrides from the start of the promotion.`,
  DesignPromotionInterestOverrides2:
    'The Base Interest Set 2 parameter defines interest after the introductory rate of your promotion. If there is no introductory rate, this will be the applied interest rate method overrides from the start of the promotion.',
  DesignPromotionInterestOverrides3:
    'The Base Interest Set 3 parameter defines interest after the introductory rate of your promotion. If there is no introductory rate, this will be the applied interest rate method overrides from the start of the promotion.',
  DesignPromotionInterestOverrides4:
    'The Payoff Exceptions (CP IC PE) Set 1 parameter contains a method name for the Payoff Exceptions section (CP IC PE) of the Product Control File that determines promotional payoff exception processing.',
  DesignPromotionInterestOverrides5:
    'The Payoff Exceptions (CP IC PE) Set 2 parameter contains a method name for the Payoff Exceptions section (CP IC PE) of the Product Control File that determines promotional payoff exception processing.',
  DesignPromotionInterestOverrides6: `The Payoff Exceptions (CP IC PE) Set 3 parameter contains a method name for the Payoff Exceptions section (CP IC PE) of the Product Control File that determines promotional payoff exception processing.`,
  DesignPromotionInterestOverrides7: `The Introductory Interest Options parameter determines the source of the introductory interest rate used for the promotion.
  If you set this parameter to 1 or 2, you must set at least one of the Intro Interest Dflt (CP IC ID) Set 1 parameters to a promotional method name. If you have set at least one of the Intro Interest Dflt (CP IC ID) Set 1 parameters to a promotional method name, you cannot set this parameter to zero.`,
  DesignPromotionInterestOverrides8: `The Regular Annual Interest Options parameter determines the source of the base interest rate you want to use for your promotion.
  If you set this parameter to 1 or 2, you must set at least one of the Intro Interest Dflt (CP IC ID) Set 1 parameters to a promotional method name. If you have set at least one of the Intro Interest Dflt (CP IC ID) Set 1 parameters to a promotional method name, you cannot set this parameter to
  zero.`,
  // design promotions Retail Path
  DesignPromotionRetailPath1: `The R Path Promotion Return Indicator parameter specifies whether you want to accept promotional return transactions through the "R Path" and apply them to the promotional balance after the end of a promotion. Your setting determines whether the System uses the return start and end dates instead of the promotion start and end dates.
  This parameter is used only by TLPSM transactions within the R path. The R path transactions have a merchant with a merchant department code switch of R.`,
  DesignPromotionRetailPath2: `The date range for a period after the end of a promotion during which promotional return transactions can be accepted and applied to a promotional balance`,
  DesignPromotionRetailPath3: `The Merchant Discount Rate parameter identifies the discount percentage charged to the merchant for transactions that qualify for the promotion if the promotion is processed through the Retail path.`,
  DesignPromotionRetailPath4: `The Promo Discount Calculation Code parameter determines whether to calculate merchant discount based solely on the promotional discount rate or on the promotional discount rate plus the merchant qualifying discount rate.`,
  // design promotions Return-to-Revolve
  DesignPromotionReturnRevolve1: `The Number of Days Delinquent parameter determines the number of days an account’s principal may be delinquent before the System begins processing the promotional balance based on the cardholder pricing strategy.`,
  DesignPromotionReturnRevolve2: `The Collapse Return to Revolving Option parameter indicates whetherthe promotion is eligible to automatically collapse when it meets return to revolving criteria or when it has been on the account for a specified length of time and meets return to revolving criteria and additional requirements`,
  DesignPromotionReturnRevolve3: `The Collapse Expiration Date Option parameter indicates whether the promotional balance is eligible to automatically collapse when the account meets all the promotion’s introductory period expiration dates.`,
  DesignPromotionReturnRevolve4: `The Delinquency Return Timing Code parameter determines the timing during cycle processing when a delinquent account is returned to revolving terms due to delinquency. Your setting in this parameter determines whether the System calculates interest for such an account before or after statement cycle time. This parameter has no effect on MPD calculation.`,
  DesignPromotionReturnRevolve5: `The Return-to-Revolve Delay Number of Days parameter specifies the count of days after which a promotional balance reverts to standard balance terms when an account meets your return-to-revolving criteria.`,
  DesignPromotionReturnRevolve6: `The Return to Revolving Base Interest parameter determines the interest rate the System uses for a promotional balance that has returned to revolving.`,
  // design promotions Statement Messaging
  DesignPromotionStatementMessaging1: `The Promotion Statement Text ID parameter determines the promotional message that is displayed in the body of the customer statement after the introductory/delay period expires. This message is also used throughout the life of a promotion if an introductory period has not been established.`,
  DesignPromotionStatementMessaging2: `The Introductory Promotional Statement Text ID parameter determines which of your promotion messages appears in the body of a cardholder statement during the delay / introductory interest period. Only required for promotions with an intro/delay period.`,
  DesignPromotionStatementMessaging3: `The Introductory Message Control parameter controls the display of an introductory promotional message on the cardholder’s paper statement, the online statement in the Customer Inquiry System (CIS), or both.`,
  DesignPromotionStatementMessaging4: `The Regular Message Display Control parameter controls the display of a routine promotional message on the cardholder’s paper statement, the online statement in the Customer Inquiry System (CIS), or both.`,
  DesignPromotionStatementMessaging5: `The Description From Text ID parameter identifies the expanded text descriptions in the transaction detail, promotional balance, and interest charge sections of the customer statement at the promotional and plan balance level.`,
  DesignPromotionStatementAdvancedParameter1: `The Delay Payment Start parameter determines the value the System uses with the following parameters in this section to calculate the end date for the promotion’s introductory minimum payment period.`,
  DesignPromotionStatementAdvancedParameter2: `The Introductory Minimum Payments Delay Number of Days parameter determines the number of days that the introductory minimum payment period is in effect. Your setting in the Delay Payment Start parameter determines whether the System begins counting the number of days from the post date of the first promotional transaction or from the account open date. The System edits the Introductory Minimum Payments Delay Number of Days parameter for numeric values. If you set this parameter, you must also set the following parameters in this section to zeros.
  - Introductory Minimum Payments Delay Date
  - Introductory Minimum Payments Delay Number of Cycles
  - Introductory Minimum Payments Delay Number of Months
  - Introductory Minimum Payments MPD End Date Ext`,
  DesignPromotionStatementAdvancedParameter3: `The Return-to-Revolve Internal Status parameter determines which internal statuses, based on your settings in the Days Delinquent and Percent Overlimit parameters in this section, cause the System to begin processing the promotional balance based on the cardholder pricing strategy.`,
  DesignPromotionStatementAdvancedParameter4: `The Promotion Expiration TEXT ID parameter identifies the message text printed on a customer statement for a client-specified number of months prior to the expiration of a promotion.`,
  DesignPromotionStatementAdvancedParameter5: `The Expiring Promotion Notification Months parameter determines the number of months prior to the promotional expiration date that a notification message is included on statements. The System edits this parameter for values 00 through 12 and 99.
  Set this parameter from 1 to 12 as the number of months prior to the promotional expiration date that a notification message is included on statements. For example, if you enter 03 in this parameter, and the promotion expires on October 31 and the account statement cycles on the 15th of the month, the notification message appears on statements for August 15, September 15, and October 15
  Set this parameter to 99 to have the System include a message about the promotion expiration date on each statement throughout the duration of the promotion.
  Set this parameter to zeros to not provide a notification message.`,
  DesignPromotionStatementAdvancedParameter6: `The Payment Delay Number of Cycles parameter determines the number of cycles that the introductory minimum payment period is in effect. Your setting in the Delay Payment Start parameter determines whether the System begins counting the number of cycles from the last statement date prior to the post date of the first promotional transaction.`,
  DesignPromotionStatementAdvancedParameter7: `The Interest On Unbilled Interest parameter determines whether promotional balances that have unbilled interest will accrue interest. If you set this parameter to 1, interest will accrue only on promotional unbilled interest for accounts being charged interest on interest. An account is billed interest on interest if the Interest on Interest Calculation parameter in the Interest Methods section (CP IC IM) of the Product Control File is set to 1, 3, or 5.`,
  DesignPromotionStatementAdvancedParameter8: `The Delay Interest Start Code parameter controls how the System calculates the end date for the promotion’s introductory interest rate period.`,
  DesignPromotionStatementAdvancedParameter9: `The Exclude Cash Option from 2-Cycle Average Daily Balance parameter determines whether to exclude a cash option promotion from the two cycle average daily balance interest calculation method for cash advance or merchandise transactions
  If you set this parameter to 1, you must also set the Interest Methods (CP IC IM) parameter in this section to blank.
  Setting this parameter to 1 overrides the Cycle-To-Date Cash interest method in the Interest Methods section (CP IC IM) from the pricing strategy or method override with a nondeferring interest method for cash option promotions as described in the following paragraphs.
          - Accounts with cash option promotional balances that use interest methods 03, Deferred Interest, or 09, Revolving Variable, will instead use interest method 02, Average Daily Balance, to determine the interest on the cash option promotional balance.
          - Accounts with cash option promotional balances that use interest methods 08, Deferred at Daily Rate, or 11, Daily Revolving Variable, will instead use interest method 05, Daily Rate Average Daily Balance, to determine the interest on the cash option promotional balance.
  Since interest methods 02 and 05 are not interest deferring methods, cycle-to-date interest is determined and billed on the account’s promotional balances. The interest is displayed on the cardholder’s first statement immediately following statement cycle in which the cash option transaction posts to the account.
  Accounts with cash option promotional balances that use interest deferring methods 03, 08, 09, or 11 will still use the interest deferring method to determine interest on the revolving side of the account, as well as for noncash option promotional purchases.
  You have to have 2-cycle ADB set in CP IC IM or else this Method will not work effectively.
  Warning!
  Use this feature with new promotions only. Changing the setting of this parameter from zero to 1 on an existing promotion may cause the following results.
  If you initially set this parameter to zero then change it to 1, the cardholder’s two-cycle average daily balance will be lost. If you initially set this parameter to zero on a multi-ticket promotion, the System will generate a deferred average daily balance and will not generate unbilled interest. If you change the setting to 1 in the next statement cycle and backdate a transaction across cycle, the System will generate last statement unbilled interest and will not generate a last statement deferred average daily balance.`,
  DesignPromotionStatementAdvancedParameter10: `The Introductory Interest Default (CP IC ID) Set 1 parameter identifies the method name for the Cardholder Pricing interest default method to use during the promotion’s introductory period.`,
  DesignPromotionStatementAdvancedParameter11: `The Introductory Interest Default (CP IC ID) Set 2 parameter identifies the method name for the Cardholder Pricing interest default method to use during the promotion’s introductory period.`,
  DesignPromotionStatementAdvancedParameter12: `The Introductory Interest Default (CP IC ID) Set 3 parameter identifies the method name for the Cardholder Pricing interest default method to use during the promotion’s introductory period.`,
  DesignPromotionStatementAdvancedParameter13: `The Interest Defaults (CP IC ID) Set 1 parameter identifies the method name for the Cardholder Pricing interest default method to use following the end of the promotion’s introductory period.`,
  DesignPromotionStatementAdvancedParameter14: `The Interest Defaults (CP IC ID) Set 2 parameter identifies the method name for the Cardholder Pricing interest default method to use following the end of the promotion’s introductory period.`,
  DesignPromotionStatementAdvancedParameter15: `The Interest Defaults (CP IC ID) Set 3 parameter identifies the method name for the Cardholder Pricing interest default method to use following the end of the promotion’s introductory period.`,
  DesignPromotionStatementAdvancedParameter16: `The Minimum Payment(PL RT MP) Set 1 parameter identifies the Promotional Level minimum payment due method to use for a promotion.`,
  DesignPromotionStatementAdvancedParameter17: `The Minimum Payment(PL RT MP) Set 1 identifies the Promotional Level minimum payment due method to use for a promotion.`,
  DesignPromotionStatementAdvancedParameter18: `The Minimum Payment(PL RT MP) Set 3 identifies the Promotional Level minimum payment due method to use for a promotion.`,
  DesignPromotionStatementAdvancedParameter19: `The Associate with Plan (PL RT PA) parameter identifies the method using the plan level terms, if any, to which a promotional transaction is linked.`,
  DesignPromotionStatementAdvancedParameter20: `The Zero Interest Extend Expiration CD parameter determines whether a zero percent interest rate promotion is extended to the billing cycle date following the interest delay end date set for the promotion`,
  DesignPromotionStatementAdvancedParameter21: `The Payoff Exception 17 Include/Exclude parameter determines whether an individual promotional balance will be excluded from the required payoff balance.`,
  DesignPromotionStatementAdvancedParameter22: `This only applies to the intro interest rate, relative to the Reset Date field that applies to everything in the Method.`,
  DesignPromotionStatementAdvancedParameter23: `The Regular Annual Load Time parameter determines when non-monetary updates to the promotional regular annual rate for cash or merchandise transactions are loaded into the Transaction Level ProcessingSM service when you set the Base Rate for Regular Annual Rate parameter to zero, or 1.`,
  DesignPromotionStatementAdvancedParameter24: `The CPOCPP Return Usage parameter determines whether you use the following pricing strategy-level parameters in the Promotional Purchases section (CP OC PP) for return to revolving processing of a delinquent account.
  - Return to Revolving Days Delinquent
  - Return to Revolving Dlnq Rtr Timing Code`,
  DesignPromotionStatementAdvancedParameter25: `The Billed Pay Due NM Code parameter determines whether you permit a zero billed payment due when a promotional balance is not delinquent and the calculated minimum payment due is zero.`,
  DesignPromotionStatementAdvancedParameter26: `The Reinstate MPD Terms Code parameter controls the post suspension minimum payment due terms calculation for reinstated promotional balances with unique minimum payment due terms`,
  DesignPromotionStatementAdvancedParameter27: `The Promotion Billed Interest Code parameter controls the level at which the System applies unpaid billed interest when reinstating a suspended promotional balance.`,
  DesignPromotionStatementAdvancedParameter28: `The Statement Promotion Display Method ID (PL RT SD) parameter identifies the method identifier for the Promotion Statement Display section of the Product Control File to use at the promotional balance level.`,
  DesignPromotionStatementAdvancedTextType_mt_pt:
    'Text Area = MT, Text Type = PT.'
};
