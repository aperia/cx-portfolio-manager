import EnhanceDatePicker from 'app/components/EnhanceDatePicker';
import EnhanceDropdownList from 'app/components/EnhanceDropdownList';
import {
  MethodFieldNameEnum,
  MethodFieldParameterEnum
} from 'app/constants/enums';
import { isValidDate, matchSearchValue } from 'app/helpers';
import {
  ColumnType,
  ComboBox,
  DateRangePicker,
  Grid,
  NumericTextBox,
  TextBox,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React, { useMemo } from 'react';
import {
  parameterGroup,
  parameterGroupNewVersion,
  ParameterList
} from './newMethodParameterList';

interface IProps {
  initialMethod: string;
  searchValue: string;
  method: WorkflowSetupMethodObjectParams;
  original: ParameterList;
  minDate?: Date;
  maxDate?: Date;
  metadata: {
    designPromotionBasicInformation3: RefData[];
    designPromotionBasicInformation4: RefData[];
    designPromotionBasicInformation6: RefData[];
    designPromotionBasicInformation7: RefData[];
    designPromotionBasicInformation8: RefData[];
    designPromotionBasicInformation9: RefData[];
    designPromotionBasicInformation10: RefData[];
    designPromotionBasicInformation11: RefData[];
    designPromotionBasicInformation12: RefData[];
    designPromotionInterestAssessment9: RefData[];
    designPromotionInterestAssessment10: RefData[];
    designPromotionInterestAssessment11: RefData[];
    designPromotionInterestAssessment12: RefData[];
    designPromotionInterestAssessment13: RefData[];
    designPromotionInterestAssessment14: RefData[];
    designPromotionInterestAssessment15: RefData[];
    designPromotionInterestAssessment16: RefData[];
    designPromotionInterestAssessment17: RefData[];
    designPromotionInterestOverrides1: RefData[];
    designPromotionInterestOverrides2: RefData[];
    designPromotionInterestOverrides3: RefData[];
    designPromotionInterestOverrides4: RefData[];
    designPromotionInterestOverrides5: RefData[];
    designPromotionInterestOverrides6: RefData[];
    designPromotionInterestOverrides7: RefData[];
    designPromotionInterestOverrides8: RefData[];
    designPromotionRetailPath1: RefData[];
    designPromotionRetailPath4: RefData[];
    designPromotionReturnRevolve2: RefData[];
    designPromotionReturnRevolve3: RefData[];
    designPromotionReturnRevolve4: RefData[];
    designPromotionReturnRevolve6: RefData[];
    designPromotionStatementMessaging1: RefData[];
    designPromotionStatementMessaging2: RefData[];
    designPromotionStatementMessaging3: RefData[];
    designPromotionStatementMessaging4: RefData[];
    designPromotionStatementMessaging5: RefData[];
    //design promotion statement advanced parameter
    designPromotionStatementAdvancedParameter1: RefData[];
    designPromotionStatementAdvancedParameter3: RefData[];
    designPromotionStatementAdvancedParameter4: RefData[];
    designPromotionStatementAdvancedParameter5: RefData[];
    designPromotionStatementAdvancedParameter7: RefData[];
    designPromotionStatementAdvancedParameter8: RefData[];
    designPromotionStatementAdvancedParameter9: RefData[];
    designPromotionStatementAdvancedParameter10: RefData[];
    designPromotionStatementAdvancedParameter11: RefData[];
    designPromotionStatementAdvancedParameter12: RefData[];
    designPromotionStatementAdvancedParameter13: RefData[];
    designPromotionStatementAdvancedParameter14: RefData[];
    designPromotionStatementAdvancedParameter15: RefData[];
    designPromotionStatementAdvancedParameter16: RefData[];
    designPromotionStatementAdvancedParameter17: RefData[];
    designPromotionStatementAdvancedParameter18: RefData[];
    designPromotionStatementAdvancedParameter19: RefData[];
    designPromotionStatementAdvancedParameter20: RefData[];
    designPromotionStatementAdvancedParameter21: RefData[];
    designPromotionStatementAdvancedParameter22: RefData[];
    designPromotionStatementAdvancedParameter23: RefData[];
    designPromotionStatementAdvancedParameter24: RefData[];
    designPromotionStatementAdvancedParameter25: RefData[];
    designPromotionStatementAdvancedParameter26: RefData[];
    designPromotionStatementAdvancedParameter27: RefData[];
    designPromotionStatementAdvancedParameter28: RefData[];
  };
  onChange: (
    field: keyof WorkflowSetupMethodObjectParams,
    isRefData?: boolean
  ) => (e: any) => void;
}
const SubGridNewMethod: React.FC<IProps> = ({
  searchValue,
  method,
  original,
  metadata,
  onChange,
  initialMethod,
  minDate,
  maxDate
}) => {
  const { t } = useTranslation();

  const {
    designPromotionBasicInformation3,
    designPromotionBasicInformation4,
    designPromotionBasicInformation6,
    designPromotionBasicInformation7,
    designPromotionBasicInformation8,
    designPromotionBasicInformation9,
    designPromotionBasicInformation10,
    designPromotionBasicInformation11,
    designPromotionBasicInformation12,
    designPromotionInterestAssessment9,
    designPromotionInterestAssessment10,
    designPromotionInterestAssessment11,
    designPromotionInterestAssessment12,
    designPromotionInterestAssessment13,
    designPromotionInterestAssessment14,
    designPromotionInterestAssessment15,
    designPromotionInterestAssessment16,
    designPromotionInterestAssessment17,
    designPromotionInterestOverrides1,
    designPromotionInterestOverrides2,
    designPromotionInterestOverrides3,
    designPromotionInterestOverrides4,
    designPromotionInterestOverrides5,
    designPromotionInterestOverrides6,
    designPromotionInterestOverrides7,
    designPromotionInterestOverrides8,
    designPromotionRetailPath1,
    designPromotionRetailPath4,
    designPromotionReturnRevolve2,
    designPromotionReturnRevolve3,
    designPromotionReturnRevolve4,
    designPromotionReturnRevolve6,
    designPromotionStatementMessaging1,
    designPromotionStatementMessaging2,
    designPromotionStatementMessaging3,
    designPromotionStatementMessaging4,
    //design promotion statement advanced parameter
    designPromotionStatementMessaging5,
    designPromotionStatementAdvancedParameter1,
    designPromotionStatementAdvancedParameter3,
    designPromotionStatementAdvancedParameter4,
    designPromotionStatementAdvancedParameter5,
    designPromotionStatementAdvancedParameter7,
    designPromotionStatementAdvancedParameter8,
    designPromotionStatementAdvancedParameter9,
    designPromotionStatementAdvancedParameter10,
    designPromotionStatementAdvancedParameter11,
    designPromotionStatementAdvancedParameter12,
    designPromotionStatementAdvancedParameter13,
    designPromotionStatementAdvancedParameter14,
    designPromotionStatementAdvancedParameter15,
    designPromotionStatementAdvancedParameter16,
    designPromotionStatementAdvancedParameter17,
    designPromotionStatementAdvancedParameter18,
    designPromotionStatementAdvancedParameter19,
    designPromotionStatementAdvancedParameter20,
    designPromotionStatementAdvancedParameter21,
    designPromotionStatementAdvancedParameter22,
    designPromotionStatementAdvancedParameter23,
    designPromotionStatementAdvancedParameter24,
    designPromotionStatementAdvancedParameter25,
    designPromotionStatementAdvancedParameter26,
    designPromotionStatementAdvancedParameter27,
    designPromotionStatementAdvancedParameter28
  } = metadata;
  const dataColumn =
    initialMethod === 'NEWVERSION' ? parameterGroupNewVersion : parameterGroup;

  const data = useMemo(
    () =>
      dataColumn[original.id].filter(p =>
        matchSearchValue(
          [p.fieldName, p.moreInfoText, p.onlinePCF],
          searchValue
        )
      ),
    [dataColumn, original.id, searchValue]
  );

  const valueAccessor = (data: Record<string, any>) => {
    const paramId = data.id as MethodFieldParameterEnum;

    switch (paramId) {
      case MethodFieldParameterEnum.DesignPromotionBasicInformation13: {
        return (
          <NumericTextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__designPromotionBasicInformation13"
            size="sm"
            format="n"
            maxLength={2}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum.DesignPromotionBasicInformation13
              ]
            }
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionBasicInformation13
            )}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionInterestAssessment3: {
        const _val =
          method?.[MethodFieldParameterEnum.DesignPromotionInterestAssessment3];
        const val = isValidDate(_val) ? new Date(_val!) : undefined;
        return (
          <div>
            <EnhanceDatePicker
              size="small"
              id="addNewMethod__DesignPromotionInterestAssessment3Format"
              value={val}
              minDate={minDate}
              maxDate={maxDate}
              name={MethodFieldNameEnum.DesignPromotionInterestAssessment3}
              onChange={onChange(
                MethodFieldParameterEnum.DesignPromotionInterestAssessment3
              )}
            />
          </div>
        );
      }
      case initialMethod === 'NEWVERSION' &&
        MethodFieldParameterEnum.DesignPromotionInterestAssessment18: {
        const _val =
          method?.[
            MethodFieldParameterEnum.DesignPromotionInterestAssessment18
          ];
        const val = isValidDate(_val) ? new Date(_val!) : undefined;
        return (
          <div>
            <EnhanceDatePicker
              size="small"
              id="addNewMethod__DesignPromotionInterestAssessment18Format"
              value={val}
              minDate={minDate}
              maxDate={maxDate}
              name={MethodFieldNameEnum.DesignPromotionInterestAssessment18}
              onChange={onChange(
                MethodFieldParameterEnum.DesignPromotionInterestAssessment18
              )}
            />
          </div>
        );
      }
      case MethodFieldParameterEnum.DesignPromotionInterestAssessment1: {
        return (
          <NumericTextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__designPromotionInterestAssessment1"
            size="sm"
            format="p3"
            showStep={false}
            maxLength={5}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum.DesignPromotionInterestAssessment1
              ] || ''
            }
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionInterestAssessment1
            )}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionRetailPath3: {
        return (
          <NumericTextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__designPromotionRetailPath3"
            size="sm"
            format="p3"
            showStep={false}
            maxLength={5}
            autoFocus={false}
            value={
              method?.[MethodFieldParameterEnum.DesignPromotionRetailPath3] ||
              ''
            }
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionRetailPath3
            )}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionReturnRevolve1: {
        return (
          <NumericTextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__designPromotionReturnRevolve1"
            size="sm"
            format="n"
            maxLength={3}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum.DesignPromotionReturnRevolve1
              ] || ''
            }
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionReturnRevolve1
            )}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionReturnRevolve5: {
        return (
          <NumericTextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__designPromotionReturnRevolve5"
            size="sm"
            format="n"
            maxLength={3}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum.DesignPromotionReturnRevolve5
              ] || ''
            }
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionReturnRevolve5
            )}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionInterestAssessment2: {
        return (
          <NumericTextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__designPromotionInterestAssessment2"
            size="sm"
            format="p3"
            showStep={false}
            maxLength={5}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum.DesignPromotionInterestAssessment2
              ] || ''
            }
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionInterestAssessment2
            )}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionInterestAssessment4: {
        return (
          <NumericTextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__designPromotionInterestAssessment4"
            size="sm"
            format="n"
            maxLength={5}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum.DesignPromotionInterestAssessment4
              ]
            }
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionInterestAssessment4
            )}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionInterestAssessment5: {
        return (
          <NumericTextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__designPromotionInterestAssessment5"
            size="sm"
            format="n"
            maxLength={2}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum.DesignPromotionInterestAssessment5
              ]
            }
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionInterestAssessment5
            )}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionInterestAssessment6: {
        return (
          <NumericTextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__designPromotionInterestAssessment6"
            size="sm"
            format="n"
            maxLength={3}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum.DesignPromotionInterestAssessment6
              ]
            }
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionInterestAssessment6
            )}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionInterestAssessment7: {
        return (
          <NumericTextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__designPromotionInterestAssessment7"
            size="sm"
            format="n"
            maxLength={2}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum.DesignPromotionInterestAssessment7
              ]
            }
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionInterestAssessment7
            )}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionInterestAssessment8: {
        return (
          <NumericTextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__designPromotionInterestAssessment8"
            size="sm"
            format="c2"
            showStep={false}
            maxLength={17}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum.DesignPromotionInterestAssessment8
              ]
            }
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionInterestAssessment8
            )}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionBasicInformation1: {
        return (
          <TextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__designPromotionBasicInformation1"
            value={
              method?.[
                MethodFieldParameterEnum.DesignPromotionBasicInformation1
              ] || ''
            }
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionBasicInformation1
            )}
            size="sm"
            maxLength={30}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionBasicInformation2: {
        return (
          <TextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__designPromotionBasicInformation2"
            value={
              method?.[
                MethodFieldParameterEnum.DesignPromotionBasicInformation2
              ] || ''
            }
            size="sm"
            maxLength={30}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionBasicInformation2
            )}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionInterestAssessment9: {
        const value = designPromotionInterestAssessment9.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum.DesignPromotionInterestAssessment9
            ]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__designPromotionInterestAssessment9"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionInterestAssessment9,
              true
            )}
            options={designPromotionInterestAssessment9}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionInterestAssessment10: {
        const value = designPromotionInterestAssessment10.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum.DesignPromotionInterestAssessment10
            ]
        );

        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            textField="text"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionInterestAssessment10,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {designPromotionInterestAssessment10.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.DesignPromotionInterestAssessment11: {
        const value = designPromotionInterestAssessment11.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum.DesignPromotionInterestAssessment11
            ]
        );

        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            size="small"
            textField="text"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionInterestAssessment11,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {designPromotionInterestAssessment11.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.DesignPromotionInterestAssessment12: {
        const value = designPromotionInterestAssessment12.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum.DesignPromotionInterestAssessment12
            ]
        );

        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            size="small"
            textField="text"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionInterestAssessment12,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {designPromotionInterestAssessment12.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.DesignPromotionInterestAssessment13: {
        const value = designPromotionInterestAssessment13.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum.DesignPromotionInterestAssessment13
            ]
        );

        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            size="small"
            textField="text"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionInterestAssessment13,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {designPromotionInterestAssessment13.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.DesignPromotionInterestAssessment14: {
        const value = designPromotionInterestAssessment14.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum.DesignPromotionInterestAssessment14
            ]
        );

        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            size="small"
            textField="text"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionInterestAssessment14,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {designPromotionInterestAssessment14.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.DesignPromotionInterestAssessment15: {
        const value = designPromotionInterestAssessment15.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum.DesignPromotionInterestAssessment15
            ]
        );

        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            size="small"
            textField="text"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionInterestAssessment15,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {designPromotionInterestAssessment15.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.DesignPromotionInterestAssessment16: {
        const value = designPromotionInterestAssessment16.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum.DesignPromotionInterestAssessment16
            ]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__designPromotionInterestAssessment16"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionInterestAssessment16,
              true
            )}
            options={designPromotionInterestAssessment16}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionInterestAssessment17: {
        const value = designPromotionInterestAssessment17.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum.DesignPromotionInterestAssessment17
            ]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__designPromotionInterestAssessment17"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionInterestAssessment17,
              true
            )}
            options={designPromotionInterestAssessment17}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionInterestOverrides1: {
        const value = designPromotionInterestOverrides1.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.DesignPromotionInterestOverrides1]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__designPromotionInterestOverrides1"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionInterestOverrides1,
              true
            )}
            options={designPromotionInterestOverrides1}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionInterestOverrides2: {
        const value = designPromotionInterestOverrides2.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.DesignPromotionInterestOverrides2]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__designPromotionInterestOverrides2"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionInterestOverrides2,
              true
            )}
            options={designPromotionInterestOverrides2}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionInterestOverrides3: {
        const value = designPromotionInterestOverrides3.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.DesignPromotionInterestOverrides3]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__designPromotionInterestOverrides3"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionInterestOverrides3,
              true
            )}
            options={designPromotionInterestOverrides3}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionInterestOverrides4: {
        const value = designPromotionInterestOverrides4.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.DesignPromotionInterestOverrides4]
        );

        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            size="small"
            textField="text"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionInterestOverrides4,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {designPromotionInterestOverrides4.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.DesignPromotionInterestOverrides5: {
        const value = designPromotionInterestOverrides5.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.DesignPromotionInterestOverrides5]
        );

        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            size="small"
            textField="text"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionInterestOverrides5,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {designPromotionInterestOverrides5.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.DesignPromotionInterestOverrides6: {
        const value = designPromotionInterestOverrides6.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.DesignPromotionInterestOverrides6]
        );

        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            size="small"
            textField="text"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionInterestOverrides6,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {designPromotionInterestOverrides6.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.DesignPromotionInterestOverrides7: {
        const value = designPromotionInterestOverrides7.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.DesignPromotionInterestOverrides7]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__designPromotionInterestOverrides7"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionInterestOverrides7,
              true
            )}
            options={designPromotionInterestOverrides7}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionInterestOverrides8: {
        const value = designPromotionInterestOverrides8.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.DesignPromotionInterestOverrides8]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__designPromotionInterestOverrides8"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionInterestOverrides8,
              true
            )}
            options={designPromotionInterestOverrides8}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionReturnRevolve2: {
        const value = designPromotionReturnRevolve2.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.DesignPromotionReturnRevolve2]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__designPromotionReturnRevolve2"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionReturnRevolve2,
              true
            )}
            options={designPromotionReturnRevolve2}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionReturnRevolve3: {
        const value = designPromotionReturnRevolve3.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.DesignPromotionReturnRevolve3]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__designPromotionReturnRevolve3"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionReturnRevolve3,
              true
            )}
            options={designPromotionReturnRevolve3}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionReturnRevolve6: {
        const value = designPromotionReturnRevolve6.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.DesignPromotionReturnRevolve6]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__designPromotionReturnRevolve6"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionReturnRevolve6,
              true
            )}
            options={designPromotionReturnRevolve6}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionReturnRevolve4: {
        const value = designPromotionReturnRevolve4.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.DesignPromotionReturnRevolve4]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__designPromotionReturnRevolve4"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionReturnRevolve4,
              true
            )}
            options={designPromotionReturnRevolve4}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionRetailPath1: {
        const value = designPromotionRetailPath1.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.DesignPromotionRetailPath1]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__designPromotionRetailPath1"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionRetailPath1,
              true
            )}
            options={designPromotionRetailPath1}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionRetailPath4: {
        const value = designPromotionRetailPath4.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.DesignPromotionRetailPath4]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__designPromotionRetailPath4"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionRetailPath4,
              true
            )}
            options={designPromotionRetailPath4}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionStatementMessaging1: {
        const value = designPromotionStatementMessaging1.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum.DesignPromotionStatementMessaging1
            ]
        );

        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            size="small"
            textField="text"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionStatementMessaging1,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {designPromotionStatementMessaging1.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.DesignPromotionBasicInformation3: {
        const value = designPromotionBasicInformation3.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.DesignPromotionBasicInformation3]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__designPromotionBasicInformation3"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionBasicInformation3,
              true
            )}
            options={designPromotionBasicInformation3}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionBasicInformation4: {
        const value = designPromotionBasicInformation4.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.DesignPromotionBasicInformation4]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            size="small"
            dataTestId="addNewMethod__designPromotionBasicInformation4"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionBasicInformation4,
              true
            )}
            options={designPromotionBasicInformation4}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionBasicInformation5: {
        const _value =
          method?.[MethodFieldParameterEnum.DesignPromotionBasicInformation5];

        const value = {
          start: _value?.start && new Date(_value.start),
          end: _value?.end && new Date(_value.end)
        };

        return (
          <div>
            <DateRangePicker
              size="small"
              id="addNewMethod__designPromotionBasicInformation5Format"
              value={value}
              minDate={minDate}
              maxDate={maxDate}
              name={MethodFieldNameEnum.DesignPromotionBasicInformation5}
              onChange={onChange(
                MethodFieldParameterEnum.DesignPromotionBasicInformation5
              )}
            />
          </div>
        );
      }
      case MethodFieldParameterEnum.DesignPromotionRetailPath2: {
        const _value = method?.[
          MethodFieldParameterEnum.DesignPromotionRetailPath2
        ] as any;

        const value = {
          start: _value?.start && new Date(_value.start),
          end: _value?.end && new Date(_value.end)
        };

        return (
          <div>
            <DateRangePicker
              size="small"
              id="addNewMethod__designPromotionRetailPath2Format"
              value={value}
              minDate={minDate}
              maxDate={maxDate}
              name={MethodFieldNameEnum.DesignPromotionRetailPath2}
              onChange={onChange(
                MethodFieldParameterEnum.DesignPromotionRetailPath2
              )}
            />
          </div>
        );
      }
      case MethodFieldParameterEnum.DesignPromotionBasicInformation6: {
        const value = designPromotionBasicInformation6.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.DesignPromotionBasicInformation6]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__designPromotionBasicInformation6"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionBasicInformation6,
              true
            )}
            options={designPromotionBasicInformation6}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionBasicInformation7: {
        const value = designPromotionBasicInformation7.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.DesignPromotionBasicInformation7]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__designPromotionBasicInformation7"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionBasicInformation7,
              true
            )}
            options={designPromotionBasicInformation7}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionBasicInformation8: {
        const value = designPromotionBasicInformation8.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.DesignPromotionBasicInformation8]
        );
        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            size="small"
            textField="text"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionBasicInformation8,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {designPromotionBasicInformation8.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.DesignPromotionBasicInformation9: {
        const value = designPromotionBasicInformation9.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.DesignPromotionBasicInformation9]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__designPromotionBasicInformation9"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionBasicInformation9,
              true
            )}
            options={designPromotionBasicInformation9}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionBasicInformation10: {
        const value = designPromotionBasicInformation10.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.DesignPromotionBasicInformation10]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__designPromotionBasicInformation10"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionBasicInformation10,
              true
            )}
            options={designPromotionBasicInformation10}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionBasicInformation11: {
        const value = designPromotionBasicInformation11.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.DesignPromotionBasicInformation11]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__designPromotionBasicInformation11"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionBasicInformation11,
              true
            )}
            options={designPromotionBasicInformation11}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionBasicInformation12: {
        const value = designPromotionBasicInformation12.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.DesignPromotionBasicInformation12]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__designPromotionBasicInformation12"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionBasicInformation12,
              true
            )}
            options={designPromotionBasicInformation12}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionStatementMessaging2: {
        const value = designPromotionStatementMessaging2.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum.DesignPromotionStatementMessaging2
            ]
        );

        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            size="small"
            textField="text"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionStatementMessaging2,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {designPromotionStatementMessaging2.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.DesignPromotionStatementMessaging3: {
        const value = designPromotionStatementMessaging3.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum.DesignPromotionStatementMessaging3
            ]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__designPromotionStatementMessaging3"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionStatementMessaging3,
              true
            )}
            options={designPromotionStatementMessaging3}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionStatementMessaging4: {
        const value = designPromotionStatementMessaging4.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum.DesignPromotionStatementMessaging4
            ]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__designPromotionStatementMessaging4"
            size="small"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionStatementMessaging4,
              true
            )}
            options={designPromotionStatementMessaging4}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionStatementMessaging5: {
        const value = designPromotionStatementMessaging5.find(
          o =>
            o.code ===
            method?.[
              MethodFieldParameterEnum.DesignPromotionStatementMessaging5
            ]
        );

        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            size="small"
            textField="text"
            value={value}
            onChange={onChange(
              MethodFieldParameterEnum.DesignPromotionStatementMessaging5,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {designPromotionStatementMessaging5.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }

      // design promotion statement advanced parameter
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter1: {
        const value = designPromotionStatementAdvancedParameter1.find(
          o => o.code === method?.[paramId]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__DesignPromotionStatementAdvancedParameter1"
            size="small"
            value={value}
            onChange={onChange(paramId, true)}
            options={designPromotionStatementAdvancedParameter1}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter2: {
        return (
          <NumericTextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__DesignPromotionStatementAdvancedParameter2"
            size="sm"
            format="n"
            maxLength={5}
            autoFocus={false}
            value={method?.[paramId]}
            onChange={onChange(paramId)}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter3: {
        const value = designPromotionStatementAdvancedParameter3.find(
          o => o.code === method?.[paramId]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__DesignPromotionStatementAdvancedParameter3"
            size="small"
            value={value}
            onChange={onChange(paramId, true)}
            options={designPromotionStatementAdvancedParameter3}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter4: {
        const value = designPromotionStatementAdvancedParameter4.find(
          o => o.code === method?.[paramId]
        );

        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            size="small"
            textField="text"
            value={value}
            onChange={onChange(paramId, true)}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {designPromotionStatementAdvancedParameter4.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter5: {
        const value = designPromotionStatementAdvancedParameter5.find(
          o => o.code === method?.[paramId]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__DesignPromotionStatementAdvancedParameter5"
            size="small"
            value={value}
            onChange={onChange(paramId, true)}
            options={designPromotionStatementAdvancedParameter5}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter6: {
        return (
          <NumericTextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__designPromotionBasicInformation13"
            size="sm"
            format="n"
            maxLength={2}
            autoFocus={false}
            value={method?.[paramId]}
            onChange={onChange(paramId)}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter7: {
        const value = designPromotionStatementAdvancedParameter7.find(
          o => o.code === method?.[paramId]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__DesignPromotionStatementAdvancedParameter7"
            size="small"
            value={value}
            onChange={onChange(paramId, true)}
            options={designPromotionStatementAdvancedParameter7}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter8: {
        const value = designPromotionStatementAdvancedParameter8.find(
          o => o.code === method?.[paramId]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__DesignPromotionStatementAdvancedParameter8"
            size="small"
            value={value}
            onChange={onChange(paramId, true)}
            options={designPromotionStatementAdvancedParameter8}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter9: {
        const value = designPromotionStatementAdvancedParameter9.find(
          o => o.code === method?.[paramId]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__DesignPromotionStatementAdvancedParameter9"
            size="small"
            value={value}
            onChange={onChange(paramId, true)}
            options={designPromotionStatementAdvancedParameter9}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter10: {
        const value = designPromotionStatementAdvancedParameter10.find(
          o => o.code === method?.[paramId]
        );

        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            size="small"
            textField="text"
            value={value}
            onChange={onChange(paramId, true)}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {designPromotionStatementAdvancedParameter10.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter11: {
        const value = designPromotionStatementAdvancedParameter11.find(
          o => o.code === method?.[paramId]
        );

        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            size="small"
            textField="text"
            value={value}
            onChange={onChange(paramId, true)}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {designPromotionStatementAdvancedParameter11.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter12: {
        const value = designPromotionStatementAdvancedParameter12.find(
          o => o.code === method?.[paramId]
        );

        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            size="small"
            textField="text"
            value={value}
            onChange={onChange(paramId, true)}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {designPromotionStatementAdvancedParameter12.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter13: {
        const value = designPromotionStatementAdvancedParameter13.find(
          o => o.code === method?.[paramId]
        );

        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            size="small"
            textField="text"
            value={value}
            onChange={onChange(paramId, true)}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {designPromotionStatementAdvancedParameter13.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter14: {
        const value = designPromotionStatementAdvancedParameter14.find(
          o => o.code === method?.[paramId]
        );

        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            size="small"
            textField="text"
            value={value}
            onChange={onChange(paramId, true)}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {designPromotionStatementAdvancedParameter14.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter15: {
        const value = designPromotionStatementAdvancedParameter15.find(
          o => o.code === method?.[paramId]
        );

        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            size="small"
            textField="text"
            value={value}
            onChange={onChange(paramId, true)}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {designPromotionStatementAdvancedParameter15.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter16: {
        const value = designPromotionStatementAdvancedParameter16.find(
          o => o.code === method?.[paramId]
        );

        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            size="small"
            textField="text"
            value={value}
            onChange={onChange(paramId, true)}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {designPromotionStatementAdvancedParameter16.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter17: {
        const value = designPromotionStatementAdvancedParameter17.find(
          o => o.code === method?.[paramId]
        );

        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            size="small"
            textField="text"
            value={value}
            onChange={onChange(paramId, true)}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {designPromotionStatementAdvancedParameter17.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter18: {
        const value = designPromotionStatementAdvancedParameter18.find(
          o => o.code === method?.[paramId]
        );

        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            size="small"
            textField="text"
            value={value}
            onChange={onChange(paramId, true)}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {designPromotionStatementAdvancedParameter18.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter19: {
        const value = designPromotionStatementAdvancedParameter19.find(
          o => o.code === method?.[paramId]
        );

        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            size="small"
            textField="text"
            value={value}
            onChange={onChange(paramId, true)}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {designPromotionStatementAdvancedParameter19.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter20: {
        const value = designPromotionStatementAdvancedParameter20.find(
          o => o.code === method?.[paramId]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__DesignPromotionStatementAdvancedParameter20"
            size="small"
            value={value}
            onChange={onChange(paramId, true)}
            options={designPromotionStatementAdvancedParameter20}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter21: {
        const value = designPromotionStatementAdvancedParameter21.find(
          o => o.code === method?.[paramId]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__DesignPromotionStatementAdvancedParameter21"
            size="small"
            value={value}
            onChange={onChange(paramId, true)}
            options={designPromotionStatementAdvancedParameter21}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter22: {
        const value = designPromotionStatementAdvancedParameter22.find(
          o => o.code === method?.[paramId]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__DesignPromotionStatementAdvancedParameter22"
            size="small"
            value={value}
            onChange={onChange(paramId, true)}
            options={designPromotionStatementAdvancedParameter22}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter23: {
        const value = designPromotionStatementAdvancedParameter23.find(
          o => o.code === method?.[paramId]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__DesignPromotionStatementAdvancedParameter23"
            size="small"
            value={value}
            onChange={onChange(paramId, true)}
            options={designPromotionStatementAdvancedParameter23}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter24: {
        const value = designPromotionStatementAdvancedParameter24.find(
          o => o.code === method?.[paramId]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__DesignPromotionStatementAdvancedParameter24"
            size="small"
            value={value}
            onChange={onChange(paramId, true)}
            options={designPromotionStatementAdvancedParameter24}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter25: {
        const value = designPromotionStatementAdvancedParameter25.find(
          o => o.code === method?.[paramId]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__DesignPromotionStatementAdvancedParameter25"
            size="small"
            value={value}
            onChange={onChange(paramId, true)}
            options={designPromotionStatementAdvancedParameter25}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter26: {
        const value = designPromotionStatementAdvancedParameter26.find(
          o => o.code === method?.[paramId]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__DesignPromotionStatementAdvancedParameter26"
            size="small"
            value={value}
            onChange={onChange(paramId, true)}
            options={designPromotionStatementAdvancedParameter26}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter27: {
        const value = designPromotionStatementAdvancedParameter27.find(
          o => o.code === method?.[paramId]
        );

        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId="addNewMethod__DesignPromotionStatementAdvancedParameter27"
            size="small"
            value={value}
            onChange={onChange(paramId, true)}
            options={designPromotionStatementAdvancedParameter27}
            tooltipItemCustom={[
              {
                code: '1',
                tooltipItem: (
                  <div>
                    <p>
                      1 - Apply unpaid billed interest using
                      next-available-level logic, where the sequence is plan
                      balance level, protected balance level, and then the
                      standard balance level. If you set this parameter to 1,
                      the System will take your setting in the base interest
                      revolving switch on the *CM6 screen into account as
                      follows:
                    </p>
                    <p className="mt-8">
                      • If the switch is set to the promotional balance level,
                      the System will use next-available-level logic.
                    </p>
                    <p className="mt-8">
                      • If the switch is set to the standard balance level and
                      the promotion is unexpired, the System will use
                      next-available-level logic.
                    </p>
                    <p className="mt-8">
                      • If the switch is set to the standard balance level and
                      the promotion has expired, the System will apply any
                      unpaid billed interest at the promotional balance level.
                    </p>
                  </div>
                )
              },
              {
                code: '8',
                tooltipItem: (
                  <div>
                    <p>
                      8 - Do not assess late charges if the account meets one of
                      the following conditions:
                    </p>
                    <p className="mt-8">
                      • The unpaid portion of the PCF-calculated MPD is less
                      than or equal to the amount in the Threshold Waive Amount
                      parameter in this section.
                    </p>
                    <p className="mt-8">
                      • The balance is less than that in the Exclusion Balance
                      parameter. If you use this option, you must also set the
                      Threshold Waive Amount parameter to a value greater than
                      zero.
                    </p>
                  </div>
                )
              }
            ]}
          />
        );
      }
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter28: {
        const value = designPromotionStatementAdvancedParameter28.find(
          o => o.code === method?.[paramId]
        );

        return (
          <ComboBox
            placeholder={t('txt_select_an_option')}
            size="small"
            textField="text"
            value={value}
            onChange={onChange(paramId, true)}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {designPromotionStatementAdvancedParameter28.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
    }
  };

  const columns: ColumnType[] = [
    {
      id: 'fieldName',
      Header: t('txt_business_name'),
      accessor: ({ fieldName }) => (
        <TruncateText
          resizable
          lines={2}
          title={fieldName}
          ellipsisLessText={t('txt_less')}
          ellipsisMoreText={t('txt_more')}
        >
          {fieldName}
        </TruncateText>
      ),
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'onlinePCF',
      Header: t('txt_manage_penalty_fee_online_PCF'),
      accessor: ({ onlinePCF }) => (
        <TruncateText
          resizable
          lines={2}
          title={onlinePCF}
          ellipsisLessText={t('txt_less')}
          ellipsisMoreText={t('txt_more')}
        >
          {onlinePCF}
        </TruncateText>
      ),
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'value',
      Header: t('txt_value'),
      cellBodyProps: { className: 'py-8' },
      accessor: valueAccessor,
      width: 300
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      accessor: viewMoreInfo(['moreInfo'], t),
      width: 105,
      cellBodyProps: { className: 'pt-12 pb-8' }
    }
  ];

  return (
    <div className="p-20">
      <Grid columns={columns} data={data} />
    </div>
  );
};

export default SubGridNewMethod;
