import {
  parameterGroup,
  parameterGroupNewVersion,
  parameterList
} from './newMethodParameterList';

describe('pages > WorkflowDesignPromotions > DesignPromotionsStep > newMethodParameterList', () => {
  it('parameterList', () => {
    expect(Object.keys(parameterList).length).toEqual(7);
  });

  it('parameterGroup', () => {
    expect(Object.keys(parameterGroupNewVersion).length).toEqual(7);
    expect(Object.keys(parameterGroup).length).toEqual(7);
  });
});
