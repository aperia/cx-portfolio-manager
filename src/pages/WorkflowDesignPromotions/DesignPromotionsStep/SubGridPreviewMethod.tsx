import { FormatTime, MethodFieldParameterEnum } from 'app/constants/enums';
import { formatCommon, formatTimeDefault, matchSearchValue } from 'app/helpers';
import {
  ColumnType,
  Grid,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import { isUndefined } from 'lodash';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React, { useMemo } from 'react';
import {
  parameterGroup,
  parameterGroupNewVersion,
  ParameterList
} from './newMethodParameterList';
import { ParameterValueType } from './SubRowGrid';

interface IProps {
  original: ParameterList;
  metadata: ParameterValueType;
  methodType: string;
}
const SubGridPreviewMethod: React.FC<IProps> = ({
  original,
  metadata,
  methodType
}) => {
  const { t } = useTranslation();

  const {
    designPromotionBasicInformation1,
    designPromotionBasicInformation2,
    designPromotionBasicInformation3,
    designPromotionBasicInformation4,
    designPromotionBasicInformation5,
    designPromotionBasicInformation6,
    designPromotionBasicInformation7,
    designPromotionBasicInformation8,
    designPromotionBasicInformation9,
    designPromotionBasicInformation10,
    designPromotionBasicInformation11,
    designPromotionBasicInformation12,
    designPromotionBasicInformation13,
    //design promotions Interest Assessment and Fees
    designPromotionInterestAssessment1,
    designPromotionInterestAssessment2,
    designPromotionInterestAssessment3,
    designPromotionInterestAssessment4,
    designPromotionInterestAssessment5,
    designPromotionInterestAssessment6,
    designPromotionInterestAssessment7,
    designPromotionInterestAssessment8,
    designPromotionInterestAssessment9,
    designPromotionInterestAssessment10,
    designPromotionInterestAssessment11,
    designPromotionInterestAssessment12,
    designPromotionInterestAssessment13,
    designPromotionInterestAssessment14,
    designPromotionInterestAssessment15,
    designPromotionInterestAssessment16,
    designPromotionInterestAssessment17,
    designPromotionInterestAssessment18,
    //design promotions Interest Overrides
    designPromotionInterestOverrides1,
    designPromotionInterestOverrides2,
    designPromotionInterestOverrides3,
    designPromotionInterestOverrides4,
    designPromotionInterestOverrides5,
    designPromotionInterestOverrides6,
    designPromotionInterestOverrides7,
    designPromotionInterestOverrides8,
    //design promotions Retail Path
    designPromotionRetailPath1,
    designPromotionRetailPath2,
    designPromotionRetailPath3,
    designPromotionRetailPath4,
    //design promotions Return-to-Revolve
    designPromotionReturnRevolve1,
    designPromotionReturnRevolve2,
    designPromotionReturnRevolve3,
    designPromotionReturnRevolve4,
    designPromotionReturnRevolve5,
    designPromotionReturnRevolve6,
    //design promotions Statement Messaging
    designPromotionStatementMessaging1,
    designPromotionStatementMessaging2,
    designPromotionStatementMessaging3,
    designPromotionStatementMessaging4,
    designPromotionStatementMessaging5,
    //design promotion statement advanced parameter
    designPromotionStatementAdvancedParameter1,
    designPromotionStatementAdvancedParameter2,
    designPromotionStatementAdvancedParameter3,
    designPromotionStatementAdvancedParameter4,
    designPromotionStatementAdvancedParameter5,
    designPromotionStatementAdvancedParameter6,
    designPromotionStatementAdvancedParameter7,
    designPromotionStatementAdvancedParameter8,
    designPromotionStatementAdvancedParameter9,
    designPromotionStatementAdvancedParameter10,
    designPromotionStatementAdvancedParameter11,
    designPromotionStatementAdvancedParameter12,
    designPromotionStatementAdvancedParameter13,
    designPromotionStatementAdvancedParameter14,
    designPromotionStatementAdvancedParameter15,
    designPromotionStatementAdvancedParameter16,
    designPromotionStatementAdvancedParameter17,
    designPromotionStatementAdvancedParameter18,
    designPromotionStatementAdvancedParameter19,
    designPromotionStatementAdvancedParameter20,
    designPromotionStatementAdvancedParameter21,
    designPromotionStatementAdvancedParameter22,
    designPromotionStatementAdvancedParameter23,
    designPromotionStatementAdvancedParameter24,
    designPromotionStatementAdvancedParameter25,
    designPromotionStatementAdvancedParameter26,
    designPromotionStatementAdvancedParameter27,
    designPromotionStatementAdvancedParameter28
  } = metadata;

  const allParameters = useMemo(
    () =>
      methodType === 'NEWVERSION' ? parameterGroupNewVersion : parameterGroup,
    [methodType]
  );
  const data = useMemo(
    () =>
      allParameters[original.id].filter(p =>
        matchSearchValue([p.fieldName, p.moreInfoText, p.onlinePCF], undefined)
      ),
    [original.id, allParameters]
  );

  const formatPercent = (
    text: string | number | undefined,
    format = 3 as number
  ) => {
    if (!text) return '';
    return `${parseFloat(`${text}`)
      .toFixed(format)
      .replace(/\d(?=(\d{3})+\.)/g, '$&,')}%`;
  };

  const formatNumber = (text: string | number | undefined) => {
    if (!text) return '';
    return new Intl.NumberFormat().format(parseFloat(`${text}`));
  };

  const formatAmount = (
    text: string | number | undefined,
    format = 2 as number
  ) => {
    if (!text) return '';
    return `$${parseFloat(`${text}`)
      .toFixed(format)
      .replace(/\d(?=(\d{3})+\.)/g, '$&,')}`;
  };

  const valueAccessor = (data: Record<string, any>) => {
    let valueTruncate: any = '';
    const paramId = data.id as MethodFieldParameterEnum;

    switch (paramId) {
      case MethodFieldParameterEnum.DesignPromotionStatementMessaging1: {
        valueTruncate = designPromotionStatementMessaging1;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionStatementMessaging2: {
        valueTruncate = designPromotionStatementMessaging2;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionStatementMessaging3: {
        valueTruncate = designPromotionStatementMessaging3;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionStatementMessaging4: {
        valueTruncate = designPromotionStatementMessaging4;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionStatementMessaging5: {
        valueTruncate = designPromotionStatementMessaging5;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionReturnRevolve1: {
        valueTruncate = designPromotionReturnRevolve1;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionReturnRevolve2: {
        valueTruncate = designPromotionReturnRevolve2;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionReturnRevolve3: {
        valueTruncate = designPromotionReturnRevolve3;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionReturnRevolve4: {
        valueTruncate = designPromotionReturnRevolve4;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionReturnRevolve5: {
        valueTruncate = designPromotionReturnRevolve5;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionReturnRevolve6: {
        valueTruncate = designPromotionReturnRevolve6;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionRetailPath1: {
        valueTruncate = designPromotionRetailPath1;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionRetailPath2: {
        const isRangeDate =
          isUndefined((designPromotionRetailPath2 as any)?.end) &&
          isUndefined((designPromotionRetailPath2 as any)?.start);
        const val = isRangeDate
          ? ''
          : !isUndefined((designPromotionRetailPath2 as any)?.end)
          ? `${formatTimeDefault(
              new Date((designPromotionRetailPath2 as any)?.start).toString(),
              FormatTime.Date
            )} - ${formatTimeDefault(
              new Date((designPromotionRetailPath2 as any)?.end).toString(),
              FormatTime.Date
            )}`
          : `${formatTimeDefault(
              new Date((designPromotionRetailPath2 as any)?.start).toString(),
              FormatTime.Date
            )}`;
        valueTruncate = val;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionRetailPath3: {
        valueTruncate = formatPercent(designPromotionRetailPath3);
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionRetailPath4: {
        valueTruncate = designPromotionRetailPath4;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionInterestOverrides1: {
        valueTruncate = designPromotionInterestOverrides1;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionInterestOverrides2: {
        valueTruncate = designPromotionInterestOverrides2;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionInterestOverrides3: {
        valueTruncate = designPromotionInterestOverrides3;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionInterestOverrides4: {
        valueTruncate = designPromotionInterestOverrides4;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionInterestOverrides5: {
        valueTruncate = designPromotionInterestOverrides5;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionInterestOverrides7: {
        valueTruncate = designPromotionInterestOverrides7;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionInterestOverrides6: {
        valueTruncate = designPromotionInterestOverrides6;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionInterestOverrides8: {
        valueTruncate = designPromotionInterestOverrides8;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionBasicInformation1: {
        valueTruncate = designPromotionBasicInformation1;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionBasicInformation2: {
        valueTruncate = designPromotionBasicInformation2;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionBasicInformation3: {
        valueTruncate = designPromotionBasicInformation3;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionBasicInformation4: {
        valueTruncate = designPromotionBasicInformation4;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionBasicInformation5: {
        const isRangeDate =
          isUndefined(designPromotionBasicInformation5?.end) &&
          isUndefined(designPromotionBasicInformation5?.start);
        const val = isRangeDate
          ? ''
          : !isUndefined(designPromotionBasicInformation5?.end)
          ? `${formatTimeDefault(
              new Date(designPromotionBasicInformation5?.start).toString(),
              FormatTime.Date
            )} - ${formatTimeDefault(
              new Date(designPromotionBasicInformation5?.end).toString(),
              FormatTime.Date
            )}`
          : `${formatTimeDefault(
              new Date(designPromotionBasicInformation5?.start).toString(),
              FormatTime.Date
            )}`;
        valueTruncate = val;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionInterestAssessment18: {
        valueTruncate = designPromotionInterestAssessment18
          ? formatTimeDefault(
              new Date(designPromotionInterestAssessment18).toString(),
              FormatTime.Date
            )
          : '';
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionInterestAssessment3: {
        valueTruncate = designPromotionInterestAssessment3
          ? formatTimeDefault(
              new Date(designPromotionInterestAssessment3).toString(),
              FormatTime.Date
            )
          : '';
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionInterestAssessment1: {
        valueTruncate = formatPercent(designPromotionInterestAssessment1);
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionInterestAssessment2: {
        valueTruncate = formatPercent(designPromotionInterestAssessment2);
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionInterestAssessment4: {
        valueTruncate = formatNumber(designPromotionInterestAssessment4);
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionInterestAssessment5: {
        valueTruncate = designPromotionInterestAssessment5;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionInterestAssessment6: {
        valueTruncate = designPromotionInterestAssessment6;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionInterestAssessment7: {
        valueTruncate = designPromotionInterestAssessment7;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionInterestAssessment8: {
        valueTruncate = formatAmount(designPromotionInterestAssessment8);
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionInterestAssessment9: {
        valueTruncate = designPromotionInterestAssessment9;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionInterestAssessment10: {
        valueTruncate = designPromotionInterestAssessment10;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionInterestAssessment11: {
        valueTruncate = designPromotionInterestAssessment11;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionInterestAssessment12: {
        valueTruncate = designPromotionInterestAssessment12;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionInterestAssessment13: {
        valueTruncate = designPromotionInterestAssessment13;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionInterestAssessment14: {
        valueTruncate = designPromotionInterestAssessment14;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionInterestAssessment15: {
        valueTruncate = designPromotionInterestAssessment15;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionInterestAssessment16: {
        valueTruncate = designPromotionInterestAssessment16;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionInterestAssessment17: {
        valueTruncate = designPromotionInterestAssessment17;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionBasicInformation6: {
        valueTruncate = designPromotionBasicInformation6;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionBasicInformation7: {
        valueTruncate = designPromotionBasicInformation7;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionBasicInformation8: {
        valueTruncate = designPromotionBasicInformation8;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionBasicInformation9: {
        valueTruncate = designPromotionBasicInformation9;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionBasicInformation10: {
        valueTruncate = designPromotionBasicInformation10;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionBasicInformation11: {
        valueTruncate = designPromotionBasicInformation11;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionBasicInformation12: {
        valueTruncate = designPromotionBasicInformation12;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionBasicInformation13: {
        valueTruncate = designPromotionBasicInformation13;
        break;
      }
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter1:
        valueTruncate = designPromotionStatementAdvancedParameter1;
        break;
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter2:
        valueTruncate = formatCommon(
          designPromotionStatementAdvancedParameter2 || ''
        ).quantity;
        break;
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter3:
        valueTruncate = designPromotionStatementAdvancedParameter3;
        break;
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter4:
        valueTruncate = designPromotionStatementAdvancedParameter4;
        break;
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter5:
        valueTruncate = designPromotionStatementAdvancedParameter5;
        break;
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter6:
        valueTruncate = designPromotionStatementAdvancedParameter6;
        break;
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter7:
        valueTruncate = designPromotionStatementAdvancedParameter7;
        break;
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter8:
        valueTruncate = designPromotionStatementAdvancedParameter8;
        break;
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter9:
        valueTruncate = designPromotionStatementAdvancedParameter9;
        break;
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter10:
        valueTruncate = designPromotionStatementAdvancedParameter10;
        break;
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter11:
        valueTruncate = designPromotionStatementAdvancedParameter11;
        break;
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter12:
        valueTruncate = designPromotionStatementAdvancedParameter12;
        break;
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter13:
        valueTruncate = designPromotionStatementAdvancedParameter13;
        break;
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter14:
        valueTruncate = designPromotionStatementAdvancedParameter14;
        break;
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter15:
        valueTruncate = designPromotionStatementAdvancedParameter15;
        break;
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter16:
        valueTruncate = designPromotionStatementAdvancedParameter16;
        break;
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter17:
        valueTruncate = designPromotionStatementAdvancedParameter17;
        break;
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter18:
        valueTruncate = designPromotionStatementAdvancedParameter18;
        break;
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter19:
        valueTruncate = designPromotionStatementAdvancedParameter19;
        break;
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter20:
        valueTruncate = designPromotionStatementAdvancedParameter20;
        break;
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter21:
        valueTruncate = designPromotionStatementAdvancedParameter21;
        break;
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter22:
        valueTruncate = designPromotionStatementAdvancedParameter22;
        break;
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter23:
        valueTruncate = designPromotionStatementAdvancedParameter23;
        break;
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter24:
        valueTruncate = designPromotionStatementAdvancedParameter24;
        break;
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter25:
        valueTruncate = designPromotionStatementAdvancedParameter25;
        break;
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter26:
        valueTruncate = designPromotionStatementAdvancedParameter26;
        break;
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter27:
        valueTruncate = designPromotionStatementAdvancedParameter27;
        break;
      case MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter28:
        valueTruncate = designPromotionStatementAdvancedParameter28;
        break;
    }

    return (
      <TruncateText
        resizable
        lines={2}
        ellipsisLessText={t('txt_less')}
        ellipsisMoreText={t('txt_more')}
      >
        {valueTruncate}
      </TruncateText>
    );
  };

  const columns: ColumnType[] = [
    {
      id: 'fieldName',
      Header: t('txt_business_name'),
      accessor: ({ fieldName }) => (
        <div>
          <TruncateText
            resizable
            lines={2}
            title={fieldName}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {fieldName}
          </TruncateText>
        </div>
      )
    },
    {
      id: 'onlinePCF',
      Header: t('txt_manage_penalty_fee_online_PCF'),
      accessor: ({ onlinePCF }) => (
        <div>
          <TruncateText
            resizable
            lines={2}
            title={onlinePCF}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {onlinePCF}
          </TruncateText>
        </div>
      )
    },
    {
      id: 'value',
      Header: t('txt_value'),
      accessor: valueAccessor
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      accessor: viewMoreInfo(['moreInfo'], t),
      width: 105
    }
  ];

  return (
    <div className="p-20">
      <Grid columns={columns} data={data} />
    </div>
  );
};

export default SubGridPreviewMethod;
