import { render } from '@testing-library/react';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import * as CommonRedux from 'pages/_commons/redux/Common/select-hooks';
import React from 'react';
import SubGridNewMethod from './SubGridNewMethod';

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

const mockUseSelectWindowDimension = jest.spyOn(
  CommonRedux,
  'useSelectWindowDimension'
);

const metadata = {
  designPromotionBasicInformation3: [{ code: 'code', text: 'text' }],
  designPromotionBasicInformation4: [{ code: 'code', text: 'text' }],
  designPromotionBasicInformation6: [{ code: 'code', text: 'text' }],
  designPromotionBasicInformation7: [{ code: 'code', text: 'text' }],
  designPromotionBasicInformation8: [{ code: 'code', text: 'text' }],
  designPromotionBasicInformation9: [{ code: 'code', text: 'text' }],
  designPromotionBasicInformation10: [{ code: 'code', text: 'text' }],
  designPromotionBasicInformation11: [{ code: 'code', text: 'text' }],
  designPromotionBasicInformation12: [{ code: 'code', text: 'text' }],
  designPromotionInterestAssessment9: [{ code: 'code', text: 'text' }],
  designPromotionInterestAssessment10: [{ code: 'code', text: 'text' }],
  designPromotionInterestAssessment11: [{ code: 'code', text: 'text' }],
  designPromotionInterestAssessment12: [{ code: 'code', text: 'text' }],
  designPromotionInterestAssessment13: [{ code: 'code', text: 'text' }],
  designPromotionInterestAssessment14: [{ code: 'code', text: 'text' }],
  designPromotionInterestAssessment15: [{ code: 'code', text: 'text' }],
  designPromotionInterestAssessment16: [{ code: 'code', text: 'text' }],
  designPromotionInterestAssessment17: [{ code: 'code', text: 'text' }],
  designPromotionInterestAssessment18: '11/11/2021',
  designPromotionInterestOverrides1: [{ code: 'code', text: 'text' }],
  designPromotionInterestOverrides2: [{ code: 'code', text: 'text' }],
  designPromotionInterestOverrides3: [{ code: 'code', text: 'text' }],
  designPromotionInterestOverrides4: [{ code: 'code', text: 'text' }],
  designPromotionInterestOverrides5: [{ code: 'code', text: 'text' }],
  designPromotionInterestOverrides6: [{ code: 'code', text: 'text' }],
  designPromotionInterestOverrides7: [{ code: 'code', text: 'text' }],
  designPromotionInterestOverrides8: [{ code: 'code', text: 'text' }],
  designPromotionRetailPath1: [{ code: 'code', text: 'text' }],
  designPromotionRetailPath4: [{ code: 'code', text: 'text' }],
  designPromotionReturnRevolve2: [{ code: 'code', text: 'text' }],
  designPromotionReturnRevolve3: [{ code: 'code', text: 'text' }],
  designPromotionReturnRevolve4: [{ code: 'code', text: 'text' }],
  designPromotionReturnRevolve6: [{ code: 'code', text: 'text' }],
  designPromotionStatementMessaging1: [{ code: 'code', text: 'text' }],
  designPromotionStatementMessaging2: [{ code: 'code', text: 'text' }],
  designPromotionStatementMessaging3: [{ code: 'code', text: 'text' }],
  designPromotionStatementMessaging4: [{ code: 'code', text: 'text' }],
  designPromotionStatementMessaging5: [{ code: 'code', text: 'text' }],
  designPromotionStatementAdvancedParameter1: [{ code: 'code', text: 'text' }],
  designPromotionStatementAdvancedParameter2: [{ code: 'code', text: 'text' }],
  designPromotionStatementAdvancedParameter3: [{ code: 'code', text: 'text' }],
  designPromotionStatementAdvancedParameter4: [{ code: 'code', text: 'text' }],
  designPromotionStatementAdvancedParameter5: [{ code: 'code', text: 'text' }],
  designPromotionStatementAdvancedParameter6: [{ code: 'code', text: 'text' }],
  designPromotionStatementAdvancedParameter7: [{ code: 'code', text: 'text' }],
  designPromotionStatementAdvancedParameter8: [{ code: 'code', text: 'text' }],
  designPromotionStatementAdvancedParameter9: [{ code: 'code', text: 'text' }],
  designPromotionStatementAdvancedParameter10: [{ code: 'code', text: 'text' }],
  designPromotionStatementAdvancedParameter11: [{ code: 'code', text: 'text' }],
  designPromotionStatementAdvancedParameter12: [{ code: 'code', text: 'text' }],
  designPromotionStatementAdvancedParameter13: [{ code: 'code', text: 'text' }],
  designPromotionStatementAdvancedParameter14: [{ code: 'code', text: 'text' }],
  designPromotionStatementAdvancedParameter15: [{ code: 'code', text: 'text' }],
  designPromotionStatementAdvancedParameter16: [{ code: 'code', text: 'text' }],
  designPromotionStatementAdvancedParameter17: [{ code: 'code', text: 'text' }],
  designPromotionStatementAdvancedParameter18: [{ code: 'code', text: 'text' }],
  designPromotionStatementAdvancedParameter19: [{ code: 'code', text: 'text' }],
  designPromotionStatementAdvancedParameter20: [{ code: 'code', text: 'text' }],
  designPromotionStatementAdvancedParameter21: [{ code: 'code', text: 'text' }],
  designPromotionStatementAdvancedParameter22: [{ code: 'code', text: 'text' }],
  designPromotionStatementAdvancedParameter23: [{ code: 'code', text: 'text' }],
  designPromotionStatementAdvancedParameter24: [{ code: 'code', text: 'text' }],
  designPromotionStatementAdvancedParameter25: [{ code: 'code', text: 'text' }],
  designPromotionStatementAdvancedParameter26: [{ code: 'code', text: 'text' }],
  designPromotionStatementAdvancedParameter27: [{ code: 'code', text: 'text' }],
  designPromotionStatementAdvancedParameter28: [{ code: 'code', text: 'text' }]
};
const _props = {
  metadata,
  onChange: jest.fn(),
  searchValue: ''
};

describe('pages > WorkflowDesignPromotions > DesignPromotionsStep > SubGridNewMethod', () => {
  const mockUseSelectWindowDimensionImplementation = (width: number) => {
    mockUseSelectWindowDimension.mockImplementation(() => ({ width } as any));
  };

  afterEach(() => {
    mockUseSelectWindowDimension.mockClear();
  });
  it('Info_Basic  > initialMethod = NEWVERSION', () => {
    mockUseSelectWindowDimensionImplementation(1080);
    const props = { ..._props, original: { id: 'Info_Basic' } };
    const wrapper = render(
      <SubGridNewMethod initialMethod={'NEWVERSION'} {...props} />
    );

    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
    expect(wrapper.getByText('txt_value')).toBeInTheDocument();
    expect(wrapper.getByText('txt_more_info')).toBeInTheDocument();
  });

  it('Info_Basic  > initialMethod = NEWMETHOD', () => {
    mockUseSelectWindowDimensionImplementation(1080);
    const props = {
      ..._props,
      original: { id: 'Info_Basic' }
    };
    const wrapper = render(
      <SubGridNewMethod initialMethod={'NEWMETHOD'} {...props} />
    );

    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
    expect(wrapper.getByText('txt_value')).toBeInTheDocument();
    expect(wrapper.getByText('txt_more_info')).toBeInTheDocument();
  });

  it('Info_InterestAssessmentFees with data', () => {
    mockUseSelectWindowDimensionImplementation(1080);
    const props = {
      ..._props,
      original: { id: 'Info_InterestAssessmentFees' },
      method: {
        [MethodFieldParameterEnum.DesignPromotionInterestAssessment3]:
          '11-20-2021',
        [MethodFieldParameterEnum.DesignPromotionInterestAssessment18]:
          '11-20-2021'
      }
    };
    const wrapper = render(
      <SubGridNewMethod initialMethod={'NEWVERSION'} {...props} />
    );

    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
    expect(wrapper.getByText('txt_value')).toBeInTheDocument();
    expect(wrapper.getByText('txt_more_info')).toBeInTheDocument();
  });

  it('Info_InterestAssessmentFees without data', () => {
    mockUseSelectWindowDimensionImplementation(1080);
    const props = {
      ..._props,
      original: { id: 'Info_InterestAssessmentFees' },
      method: {
        [MethodFieldParameterEnum.DesignPromotionInterestAssessment3]: '',
        [MethodFieldParameterEnum.DesignPromotionInterestAssessment18]: ''
      }
    };
    const wrapper = render(
      <SubGridNewMethod initialMethod={'NEWVERSION'} {...props} />
    );

    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
    expect(wrapper.getByText('txt_value')).toBeInTheDocument();
    expect(wrapper.getByText('txt_more_info')).toBeInTheDocument();
  });

  it('Info_InterestAssessmentFees > initialMethod = NEWMETHOD', () => {
    mockUseSelectWindowDimensionImplementation(1080);
    const props = {
      ..._props,
      original: { id: 'Info_InterestAssessmentFees' }
    };
    const wrapper = render(
      <SubGridNewMethod initialMethod={'NEWMETHOD'} {...props} />
    );

    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
    expect(wrapper.getByText('txt_value')).toBeInTheDocument();
    expect(wrapper.getByText('txt_more_info')).toBeInTheDocument();
  });

  it('Info_InterestOverrides', () => {
    mockUseSelectWindowDimensionImplementation(1080);
    const props = { ..._props, original: { id: 'Info_InterestOverrides' } };
    const wrapper = render(<SubGridNewMethod {...props} />);

    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
    expect(wrapper.getByText('txt_value')).toBeInTheDocument();
    expect(wrapper.getByText('txt_more_info')).toBeInTheDocument();
  });

  it('Info_RetailPathRelatedSettings', () => {
    mockUseSelectWindowDimensionImplementation(1080);
    const props = {
      ..._props,
      original: { id: 'Info_RetailPathRelatedSettings' }
    };
    const wrapper = render(<SubGridNewMethod {...props} />);

    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
    expect(wrapper.getByText('txt_value')).toBeInTheDocument();
    expect(wrapper.getByText('txt_more_info')).toBeInTheDocument();
  });

  it('Info_ReturnRevolveSettings', () => {
    mockUseSelectWindowDimensionImplementation(1080);
    const props = { ..._props, original: { id: 'Info_ReturnRevolveSettings' } };
    const wrapper = render(<SubGridNewMethod {...props} />);

    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
    expect(wrapper.getByText('txt_value')).toBeInTheDocument();
    expect(wrapper.getByText('txt_more_info')).toBeInTheDocument();
  });

  it('Info_StatementMessaging', () => {
    mockUseSelectWindowDimensionImplementation(1080);
    const props = { ..._props, original: { id: 'Info_StatementMessaging' } };
    const wrapper = render(<SubGridNewMethod {...props} />);

    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
    expect(wrapper.getByText('txt_value')).toBeInTheDocument();
    expect(wrapper.getByText('txt_more_info')).toBeInTheDocument();
  });

  it('Info_AdvancedParameters', () => {
    mockUseSelectWindowDimensionImplementation(1080);
    const props = { ..._props, original: { id: 'Info_AdvancedParameters' } };
    const wrapper = render(<SubGridNewMethod {...props} />);

    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
    expect(wrapper.getByText('txt_value')).toBeInTheDocument();
    expect(wrapper.getByText('txt_more_info')).toBeInTheDocument();
  });

  it('Info_AdvancedParameters', () => {
    mockUseSelectWindowDimensionImplementation(1440);
    const props = { ..._props, original: { id: 'Info_AdvancedParameters' } };
    const wrapper = render(<SubGridNewMethod {...props} />);

    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
    expect(wrapper.getByText('txt_value')).toBeInTheDocument();
    expect(wrapper.getByText('txt_more_info')).toBeInTheDocument();
  });
});
