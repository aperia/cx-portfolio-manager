import { render } from '@testing-library/react';
import React from 'react';
import SubGridPreviewMethod from './SubGridPreviewMethod';

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

const metadata = {
  designPromotionBasicInformation1: 'designPromotionBasicInformation1',
  designPromotionBasicInformation2: 'designPromotionBasicInformation2',
  designPromotionBasicInformation3: 'designPromotionBasicInformation3',
  designPromotionBasicInformation4: 'designPromotionBasicInformation4',
  designPromotionBasicInformation5: '11/11/2021',
  designPromotionBasicInformation6: 'designPromotionBasicInformation6',
  designPromotionBasicInformation7: 'designPromotionBasicInformation7',
  designPromotionBasicInformation8: 'designPromotionBasicInformation8',
  designPromotionBasicInformation9: 'designPromotionBasicInformation9',
  designPromotionBasicInformation10: 'designPromotionBasicInformation10',
  designPromotionBasicInformation11: 'designPromotionBasicInformation11',
  designPromotionBasicInformation12: 'designPromotionBasicInformation12',
  designPromotionBasicInformation13: 'designPromotionBasicInformation13',
  //design promotions Interest Assessment and Fees
  designPromotionInterestAssessment1: 'designPromotionInterestAssessment1',
  designPromotionInterestAssessment2: 'designPromotionInterestAssessment2',
  designPromotionInterestAssessment3: '11/11/2021',
  designPromotionInterestAssessment4: 'designPromotionInterestAssessment4',
  designPromotionInterestAssessment5: 'designPromotionInterestAssessment5',
  designPromotionInterestAssessment6: 'designPromotionInterestAssessment6',
  designPromotionInterestAssessment7: 'designPromotionInterestAssessment7',
  designPromotionInterestAssessment8: 'designPromotionInterestAssessment8',
  designPromotionInterestAssessment9: 'designPromotionInterestAssessment9',
  designPromotionInterestAssessment10: 'designPromotionInterestAssessment10',
  designPromotionInterestAssessment11: 'designPromotionInterestAssessment11',
  designPromotionInterestAssessment12: 'designPromotionInterestAssessment12',
  designPromotionInterestAssessment13: 'designPromotionInterestAssessment13',
  designPromotionInterestAssessment14: 'designPromotionInterestAssessment14',
  designPromotionInterestAssessment15: 'designPromotionInterestAssessment15',
  designPromotionInterestAssessment16: 'designPromotionInterestAssessment16',
  designPromotionInterestAssessment17: 'designPromotionInterestAssessment17',
  designPromotionInterestAssessment18: '11/11/2021',
  //design promotions Interest Overrides
  designPromotionInterestOverrides1: 'designPromotionInterestOverrides1',
  designPromotionInterestOverrides2: 'designPromotionInterestOverrides2',
  designPromotionInterestOverrides3: 'designPromotionInterestOverrides3',
  designPromotionInterestOverrides4: 'designPromotionInterestOverrides4',
  designPromotionInterestOverrides5: 'designPromotionInterestOverrides5',
  designPromotionInterestOverrides6: 'designPromotionInterestOverrides6',
  designPromotionInterestOverrides7: 'designPromotionInterestOverrides7',
  designPromotionInterestOverrides8: 'designPromotionInterestOverrides8',
  //design promotions Retail Path
  designPromotionRetailPath1: 'designPromotionRetailPath1',
  designPromotionRetailPath2: '11/11/2021',
  designPromotionRetailPath3: 'designPromotionRetailPath3',
  designPromotionRetailPath4: 'designPromotionRetailPath4',
  //design promotions Return-to-Revolve
  designPromotionReturnRevolve1: 'designPromotionReturnRevolve1',
  designPromotionReturnRevolve2: 'designPromotionReturnRevolve2',
  designPromotionReturnRevolve3: 'designPromotionReturnRevolve3',
  designPromotionReturnRevolve4: 'designPromotionReturnRevolve4',
  designPromotionReturnRevolve5: 'designPromotionReturnRevolve5',
  designPromotionReturnRevolve6: 'designPromotionReturnRevolve6',
  //design promotions Statement Messaging
  designPromotionStatementMessaging1: 'designPromotionStatementMessaging1',
  designPromotionStatementMessaging2: 'designPromotionStatementMessaging2',
  designPromotionStatementMessaging3: 'designPromotionStatementMessaging3',
  designPromotionStatementMessaging4: 'designPromotionStatementMessaging4',
  designPromotionStatementMessaging5: 'designPromotionStatementMessaging5',
  //design promotion statement advanced parameter
  designPromotionStatementAdvancedParameter1:
    'designPromotionStatementAdvancedParameter1',
  designPromotionStatementAdvancedParameter2:
    'designPromotionStatementAdvancedParameter2',
  designPromotionStatementAdvancedParameter3:
    'designPromotionStatementAdvancedParameter3',
  designPromotionStatementAdvancedParameter4:
    'designPromotionStatementAdvancedParameter4',
  designPromotionStatementAdvancedParameter5:
    'designPromotionStatementAdvancedParameter5',
  designPromotionStatementAdvancedParameter6:
    'designPromotionStatementAdvancedParameter6',
  designPromotionStatementAdvancedParameter7:
    'designPromotionStatementAdvancedParameter7',
  designPromotionStatementAdvancedParameter8:
    'designPromotionStatementAdvancedParameter8',
  designPromotionStatementAdvancedParameter9:
    'designPromotionStatementAdvancedParameter9',
  designPromotionStatementAdvancedParameter10:
    'designPromotionStatementAdvancedParameter10',
  designPromotionStatementAdvancedParameter11:
    'designPromotionStatementAdvancedParameter11',
  designPromotionStatementAdvancedParameter12:
    'designPromotionStatementAdvancedParameter12',
  designPromotionStatementAdvancedParameter13:
    'designPromotionStatementAdvancedParameter13',
  designPromotionStatementAdvancedParameter14:
    'designPromotionStatementAdvancedParameter14',
  designPromotionStatementAdvancedParameter15:
    'designPromotionStatementAdvancedParameter15',
  designPromotionStatementAdvancedParameter16:
    'designPromotionStatementAdvancedParameter16',
  designPromotionStatementAdvancedParameter17:
    'designPromotionStatementAdvancedParameter17',
  designPromotionStatementAdvancedParameter18:
    'designPromotionStatementAdvancedParameter18',
  designPromotionStatementAdvancedParameter19:
    'designPromotionStatementAdvancedParameter19',
  designPromotionStatementAdvancedParameter20:
    'designPromotionStatementAdvancedParameter20',
  designPromotionStatementAdvancedParameter21:
    'designPromotionStatementAdvancedParameter21',
  designPromotionStatementAdvancedParameter22:
    'designPromotionStatementAdvancedParameter22',
  designPromotionStatementAdvancedParameter23:
    'designPromotionStatementAdvancedParameter23',
  designPromotionStatementAdvancedParameter24:
    'designPromotionStatementAdvancedParameter24',
  designPromotionStatementAdvancedParameter25:
    'designPromotionStatementAdvancedParameter25',
  designPromotionStatementAdvancedParameter26:
    'designPromotionStatementAdvancedParameter26',
  designPromotionStatementAdvancedParameter27:
    'designPromotionStatementAdvancedParameter27',
  designPromotionStatementAdvancedParameter28:
    'designPromotionStatementAdvancedParameter28'
};

describe('pages > WorkflowDesignPromotions > DesignPromotionsStep > SubGridPreviewMethod', () => {
  it('Info_Basic', () => {
    const original = { id: 'Info_Basic' };
    const props = { metadata, original };
    const wrapper = render(<SubGridPreviewMethod {...props} />);
    expect(
      wrapper.getByText('designPromotionBasicInformation1')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('designPromotionBasicInformation2')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('designPromotionBasicInformation3')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('designPromotionBasicInformation4')
    ).toBeInTheDocument();
    // expect(wrapper.getAllByText('11/11/2021')[0]).toBeInTheDocument();
    expect(
      wrapper.getByText('designPromotionBasicInformation6')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('designPromotionBasicInformation7')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('designPromotionBasicInformation8')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('designPromotionBasicInformation9')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('designPromotionBasicInformation10')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('designPromotionBasicInformation11')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('designPromotionBasicInformation12')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('designPromotionBasicInformation13')
    ).toBeInTheDocument();
  });
  it('Info_Basic > empty designPromotionBasicInformation5', () => {
    const original = { id: 'Info_Basic' };
    const props = {
      metadata: { ...metadata, designPromotionBasicInformation5: '' },
      original
    };
    const wrapper = render(<SubGridPreviewMethod {...props} />);
    expect(
      wrapper.getByText('designPromotionBasicInformation1')
    ).toBeInTheDocument();
  });

  it('Info_InterestAssessmentFees', async () => {
    const original = { id: 'Info_InterestAssessmentFees' };
    const props = { metadata, original, methodType: 'NEWVERSION' };
    const wrapper = await render(<SubGridPreviewMethod {...props} />);

    expect(wrapper.getAllByText('11/11/2021')[0]).toBeInTheDocument();
  });

  it('Info_InterestOverrides', () => {
    const original = { id: 'Info_InterestOverrides' };
    const props = { metadata, original };
    const wrapper = render(<SubGridPreviewMethod {...props} />);
    expect(
      wrapper.getByText('designPromotionInterestOverrides1')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('designPromotionInterestOverrides2')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('designPromotionInterestOverrides3')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('designPromotionInterestOverrides4')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('designPromotionInterestOverrides5')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('designPromotionInterestOverrides6')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('designPromotionInterestOverrides7')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('designPromotionInterestOverrides8')
    ).toBeInTheDocument();
  });

  it('Info_RetailPathRelatedSettings', () => {
    const original = { id: 'Info_RetailPathRelatedSettings' };
    const props = { metadata, original };
    const wrapper = render(<SubGridPreviewMethod {...props} />);
    expect(wrapper.getByText('designPromotionRetailPath1')).toBeInTheDocument();
    expect(wrapper.getByText('designPromotionRetailPath4')).toBeInTheDocument();
  });

  it('Info_RetailPathRelatedSettings > empty designPromotionRetailPath2', () => {
    const original = { id: 'Info_RetailPathRelatedSettings' };
    const props = {
      metadata: { ...metadata, designPromotionRetailPath2: '' },
      original
    };
    const wrapper = render(<SubGridPreviewMethod {...props} />);
    expect(wrapper.getByText('designPromotionRetailPath1')).toBeInTheDocument();
  });

  it('Info_ReturnRevolveSettings', () => {
    const original = { id: 'Info_ReturnRevolveSettings' };
    const props = { metadata, original };
    const wrapper = render(<SubGridPreviewMethod {...props} />);
    expect(
      wrapper.getByText('designPromotionReturnRevolve1')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('designPromotionReturnRevolve2')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('designPromotionReturnRevolve3')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('designPromotionReturnRevolve4')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('designPromotionReturnRevolve5')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('designPromotionReturnRevolve6')
    ).toBeInTheDocument();
  });
  it('Info_StatementMessaging', () => {
    const original = { id: 'Info_StatementMessaging' };
    const props = { metadata, original };
    const wrapper = render(<SubGridPreviewMethod {...props} />);
    expect(
      wrapper.getByText('designPromotionStatementMessaging1')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('designPromotionStatementMessaging2')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('designPromotionStatementMessaging3')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('designPromotionStatementMessaging4')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('designPromotionStatementMessaging5')
    ).toBeInTheDocument();
  });

  it('Info_AdvancedParameters', () => {
    const original = { id: 'Info_AdvancedParameters' };
    const props = { metadata, original };
    const wrapper = render(<SubGridPreviewMethod {...props} />);
    expect(
      wrapper.getByText('designPromotionStatementAdvancedParameter1')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('designPromotionStatementAdvancedParameter2')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('designPromotionStatementAdvancedParameter3')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('designPromotionStatementAdvancedParameter4')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('designPromotionStatementAdvancedParameter5')
    ).toBeInTheDocument();
  });

  it('Info_InterestAssessmentFees > empty designPromotionInterestAssessment18', () => {
    const original = { id: 'Info_InterestAssessmentFees' };
    const props = {
      metadata: {
        designPromotionInterestAssessment18: ''
      },
      methodType: 'NEWVERSION',
      original
    };
    const wrapper = render(<SubGridPreviewMethod {...props} />);
    expect(wrapper.getByText('Promo Method Reload')).toBeInTheDocument();
  });
});
