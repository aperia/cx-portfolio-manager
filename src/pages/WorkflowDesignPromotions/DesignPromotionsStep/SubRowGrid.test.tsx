import React from 'react';
import SubRowGrid from './SubRowGrid';
import { renderComponent } from 'app/utils';
import { parameterList } from './newMethodParameterList';

const t = value => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

jest.mock('./SubGridPreviewMethod', () => ({
  __esModule: true,
  default: () => <div>SubGridPreviewMethod</div>
}));

describe('pages > WorkflowDesignLoanOffers > DesignLoanOffersStep > SubRowGrid', () => {
  it('render', async () => {
    const original = [
      //design promotions basic
      { newValue: '0', name: 'promotion.description' },
      { newValue: '0', name: 'alternate.promotion.description' },
      { newValue: '0', name: 'promotion.description.display' },
      { newValue: '0', name: 'promotion.type' },
      { newValue: '0', name: 'promotion.date.range' },
      { newValue: '0', name: 'credit.application.options' },
      { newValue: '0', name: 'credit.application.group' },
      { newValue: '0', name: 'credit.application.group.id' },
      { newValue: '0', name: 'multi.ticket.criteria' },
      { newValue: '0', name: 'multi.ticket.terms.creation' },
      { newValue: '0', name: 'balance.transfer.promotion.option' },
      { newValue: '0', name: 'promotion.return.application.option' },
      { newValue: '0', name: 'number.of.retentin.months' },
      //design promotions Interest Assessment and Fees
      { newValue: '0', name: 'promotional.interest.rate' },
      { newValue: '0', name: 'introductory.promotional.interest.rate' },
      { newValue: '0', name: 'promotional.interest.delay.date' },
      { newValue: '0', name: 'promotional.interest.delay.number.of.days' },
      { newValue: '0', name: 'promotional.interest.delay.number.of.cycles' },
      { newValue: '0', name: 'promotional.cash.option.number.of.days' },
      { newValue: '0', name: 'promotional.cash.option.number.of.cycles' },
      { newValue: '0', name: 'payoff.amount' },
      { newValue: '0', name: 'promotion.payoff.exception.options' },
      { newValue: '0', name: 'merchandise.items.fees.cp.io.mi.set.1' },
      { newValue: '0', name: 'merchandise.items.fees.cp.io.mi.set.2' },
      { newValue: '0', name: 'merchandise.items.fees.cp.io.mi.set.3' },
      { newValue: '0', name: 'cash.advance.item.fees.cp.io.ci.set.1' },
      { newValue: '0', name: 'cash.advance.item.fees.cp.io.ci.set.2' },
      { newValue: '0', name: 'cash.advance.item.fees.cp.io.ci.set.3' },
      { newValue: '0', name: 'promotion.method.reload' },
      { newValue: '0', name: 'promotion.interest.override.i.e' },
      { newValue: '0', name: 'reset.date' },
      //design promotions Interest Overrides
      { newValue: '0', name: 'base.interest.set.1' },
      { newValue: '0', name: 'base.interest.set.2' },
      { newValue: '0', name: 'base.interest.set.3' },
      { newValue: '0', name: 'payoff.exceptions.cp.ic.pe.set.1' },
      { newValue: '0', name: 'payoff.exceptions.cp.ic.pe.set.2' },
      { newValue: '0', name: 'payoff.exceptions.cp.ic.pe.set.3' },
      { newValue: '0', name: 'introductory.interest.options' },
      { newValue: '0', name: 'regular.annual.interest.options' },
      //design promotions Retail Path
      { newValue: '0', name: 'r.path.promotion.return.indicator' },
      { newValue: '0', name: 'r.path.promotion.return.date.range' },
      { newValue: '0', name: 'merchant.discount.rate' },
      { newValue: '0', name: 'promotion.discount.calculation.code' },
      //design promotions Return-to-Revolve
      { newValue: '0', name: 'number.of.days.delinquent' },
      { newValue: '0', name: 'collapse.return.to.revolving.option' },
      { newValue: '0', name: 'collapse.expiration.date.option' },
      { newValue: '0', name: 'delinquency.return.timing.code' },
      { newValue: '0', name: 'return.to.revolve.delay.number.of.days' },
      { newValue: '0', name: 'return.to.revolving.base.interest' },
      //design promotions Statement Messaging
      { newValue: '0', name: 'promotion.statement.text.id' },
      { newValue: '0', name: 'introductory.promotional.statement.text.id' },
      { newValue: '0', name: 'introductory.message.control' },
      { newValue: '0', name: 'regular.message.display.control' },
      { newValue: '0', name: 'description.from.text.id' },
      // design promotion statement advanced parameter
      { newValue: '0', name: 'delay.payment.start' },
      {
        newValue: '0',
        name: 'introductory.minimum.payments.delay.number.of.days'
      },
      { newValue: '0', name: 'return.to.revolve.internal.status' },
      { newValue: '0', name: 'promotion.expiration.text.id' },
      { newValue: '0', name: 'expiring.promotion.notification.months' },
      { newValue: '0', name: 'payment.delay.number.of.cycles' },
      { newValue: '0', name: 'interest.on.unbilled.interest' },
      { newValue: '0', name: 'delay.interest.start.code' },
      {
        newValue: '0',
        name: 'exclude.cash.option.from.2.cycle.average.daily.balance'
      },
      { newValue: '0', name: 'introductory.interest.default.cp.ic.id.set.1' },
      { newValue: '0', name: 'introductory.interest.default.cp.ic.id.set.2' },
      { newValue: '0', name: 'introductory.interest.default.cp.ic.id.set.3' },
      { newValue: '0', name: 'interest.defaults.cp.ic.id.set.1' },
      { newValue: '0', name: 'interest.defaults.cp.ic.id.set.2' },
      { newValue: '0', name: 'interest.defaults.cp.ic.id.set.3' },
      { newValue: '0', name: 'minimum.payment.pl.rt.mp.set.1' },
      { newValue: '0', name: 'minimum.payment.pl.rt.mp.set.2' },
      { newValue: '0', name: 'minimum.payment.pl.rt.mp.set.3' },
      { newValue: '0', name: 'associate.with.plan.pl.rt.pa' },
      { newValue: '0', name: 'zero.interest.extend.expiration.cd' },
      { newValue: '0', name: 'promotion.payoff.exception.option' },
      { newValue: '0', name: 'introductory.promotion.load.time' },
      { newValue: '0', name: 'regular.annual.load.time' },
      { newValue: '0', name: 'cpocpp.return.usage' },
      { newValue: '0', name: 'billed.pay.due.nm.code' },
      { newValue: '0', name: 'reinstate.mpd.terms.code' },
      { newValue: '0', name: 'promotion.billed.interest.code' },
      { newValue: '0', name: 'statement.promotion.display.method.id.pl.rt.sd' }
    ];

    const metadata = {
      designPromotionBasicInformation3: [
        { code: '0', value: 'designPromotionBasicInformation3' }
      ],
      designPromotionBasicInformation4: [
        { code: '0', value: 'designPromotionBasicInformation4' }
      ],
      designPromotionBasicInformation6: [
        { code: '0', value: 'designPromotionBasicInformation6' }
      ],
      designPromotionBasicInformation7: [
        { code: '0', value: 'designPromotionBasicInformation7' }
      ],
      designPromotionBasicInformation8: [
        { code: '0', value: 'designPromotionBasicInformation8' }
      ],
      designPromotionBasicInformation9: [
        { code: '0', value: 'designPromotionBasicInformation9' }
      ],
      designPromotionBasicInformation10: [
        { code: '0', value: 'designPromotionBasicInformation10' }
      ],
      designPromotionBasicInformation11: [
        { code: '0', value: 'designPromotionBasicInformation11' }
      ],
      designPromotionBasicInformation12: [
        { code: '0', value: 'designPromotionBasicInformation12' }
      ],
      designPromotionInterestAssessment9: [
        { code: '0', value: 'designPromotionInterestAssessment9' }
      ],
      designPromotionInterestAssessment10: [
        { code: '0', value: 'designPromotionInterestAssessment10' }
      ],
      designPromotionInterestAssessment11: [
        { code: '0', value: 'designPromotionInterestAssessment11' }
      ],
      designPromotionInterestAssessment12: [
        { code: '0', value: 'designPromotionInterestAssessment12' }
      ],
      designPromotionInterestAssessment13: [
        { code: '0', value: 'designPromotionInterestAssessment13' }
      ],
      designPromotionInterestAssessment14: [
        { code: '0', value: 'designPromotionInterestAssessment14' }
      ],
      designPromotionInterestAssessment15: [
        { code: '0', value: 'designPromotionInterestAssessment15' }
      ],
      designPromotionInterestAssessment16: [
        { code: '0', value: 'designPromotionInterestAssessment16' }
      ],
      designPromotionInterestAssessment17: [
        { code: '0', value: 'designPromotionInterestAssessment17' }
      ],
      designPromotionInterestOverrides1: [
        { code: '0', value: 'designPromotionInterestOverrides1' }
      ],
      designPromotionInterestOverrides2: [
        { code: '0', value: 'designPromotionInterestOverrides2' }
      ],
      designPromotionInterestOverrides3: [
        { code: '0', value: 'designPromotionInterestOverrides3' }
      ],
      designPromotionInterestOverrides4: [
        { code: '0', value: 'designPromotionInterestOverrides4' }
      ],
      designPromotionInterestOverrides5: [
        { code: '0', value: 'designPromotionInterestOverrides5' }
      ],
      designPromotionInterestOverrides6: [
        { code: '0', value: 'designPromotionInterestOverrides6' }
      ],
      designPromotionInterestOverrides7: [
        { code: '0', value: 'designPromotionInterestOverrides7' }
      ],
      designPromotionInterestOverrides8: [
        { code: '0', value: 'designPromotionInterestOverrides8' }
      ],
      designPromotionRetailPath1: [
        { code: '0', value: 'designPromotionRetailPath1' }
      ],
      designPromotionRetailPath4: [
        { code: '0', value: 'designPromotionRetailPath4' }
      ],
      designPromotionReturnRevolve2: [
        { code: '0', value: 'designPromotionReturnRevolve2' }
      ],
      designPromotionReturnRevolve3: [
        { code: '0', value: 'designPromotionReturnRevolve3' }
      ],
      designPromotionReturnRevolve4: [
        { code: '0', value: 'designPromotionReturnRevolve4' }
      ],
      designPromotionReturnRevolve6: [
        { code: '0', value: 'designPromotionReturnRevolve6' }
      ],
      designPromotionStatementMessaging1: [
        { code: '0', value: 'designPromotionStatementMessaging1' }
      ],
      designPromotionStatementMessaging2: [
        { code: '0', value: 'designPromotionStatementMessaging2' }
      ],
      designPromotionStatementMessaging3: [
        { code: '0', value: 'designPromotionStatementMessaging3' }
      ],
      designPromotionStatementMessaging4: [
        { code: '0', value: 'designPromotionStatementMessaging4' }
      ],
      designPromotionStatementMessaging5: [
        { code: '0', value: 'designPromotionStatementMessaging5' }
      ],
      designPromotionStatementAdvancedParameter1: [
        { code: '0', value: 'designPromotionStatementAdvancedParameter1' }
      ],
      designPromotionStatementAdvancedParameter2: [
        { code: '0', value: 'designPromotionStatementAdvancedParameter2' }
      ],
      designPromotionStatementAdvancedParameter3: [
        { code: '0', value: 'designPromotionStatementAdvancedParameter3' }
      ],
      designPromotionStatementAdvancedParameter4: [
        { code: '0', value: 'designPromotionStatementAdvancedParameter4' }
      ],
      designPromotionStatementAdvancedParameter5: [
        { code: '0', value: 'designPromotionStatementAdvancedParameter5' }
      ],
      designPromotionStatementAdvancedParameter6: [
        { code: '0', value: 'designPromotionStatementAdvancedParameter6' }
      ],
      designPromotionStatementAdvancedParameter7: [
        { code: '0', value: 'designPromotionStatementAdvancedParameter7' }
      ],
      designPromotionStatementAdvancedParameter8: [
        { code: '0', value: 'designPromotionStatementAdvancedParameter8' }
      ],
      designPromotionStatementAdvancedParameter9: [
        { code: '0', value: 'designPromotionStatementAdvancedParameter9' }
      ],
      designPromotionStatementAdvancedParameter10: [
        { code: '0', value: 'designPromotionStatementAdvancedParameter10' }
      ],
      designPromotionStatementAdvancedParameter11: [
        { code: '0', value: 'designPromotionStatementAdvancedParameter11' }
      ],
      designPromotionStatementAdvancedParameter12: [
        { code: '0', value: 'designPromotionStatementAdvancedParameter12' }
      ],
      designPromotionStatementAdvancedParameter13: [
        { code: '0', value: 'designPromotionStatementAdvancedParameter13' }
      ],
      designPromotionStatementAdvancedParameter14: [
        { code: '0', value: 'designPromotionStatementAdvancedParameter14' }
      ],
      designPromotionStatementAdvancedParameter15: [
        { code: '0', value: 'designPromotionStatementAdvancedParameter15' }
      ],
      designPromotionStatementAdvancedParameter16: [
        { code: '0', value: 'designPromotionStatementAdvancedParameter16' }
      ],
      designPromotionStatementAdvancedParameter17: [
        { code: '0', value: 'designPromotionStatementAdvancedParameter17' }
      ],
      designPromotionStatementAdvancedParameter18: [
        { code: '0', value: 'designPromotionStatementAdvancedParameter18' }
      ],
      designPromotionStatementAdvancedParameter19: [
        { code: '0', value: 'designPromotionStatementAdvancedParameter19' }
      ],
      designPromotionStatementAdvancedParameter20: [
        { code: '0', value: 'designPromotionStatementAdvancedParameter20' }
      ],
      designPromotionStatementAdvancedParameter21: [
        { code: '0', value: 'designPromotionStatementAdvancedParameter21' }
      ],
      designPromotionStatementAdvancedParameter22: [
        { code: '0', value: 'designPromotionStatementAdvancedParameter22' }
      ],
      designPromotionStatementAdvancedParameter23: [
        { code: '0', value: 'designPromotionStatementAdvancedParameter23' }
      ],
      designPromotionStatementAdvancedParameter24: [
        { code: '0', value: 'designPromotionStatementAdvancedParameter24' }
      ],
      designPromotionStatementAdvancedParameter25: [
        { code: '0', value: 'designPromotionStatementAdvancedParameter25' }
      ],
      designPromotionStatementAdvancedParameter26: [
        { code: '0', value: 'designPromotionStatementAdvancedParameter26' }
      ],
      designPromotionStatementAdvancedParameter27: [
        { code: '0', value: 'designPromotionStatementAdvancedParameter27' }
      ],
      designPromotionStatementAdvancedParameter28: [
        { code: '0', value: 'designPromotionStatementAdvancedParameter28' }
      ]
    };

    const wrapper = await renderComponent(
      'testId',
      <SubRowGrid metadata={metadata} original={original} />
    );

    expect(
      wrapper.getByText(parameterList[0].parameterGroup)
    ).toBeInTheDocument();
    expect(
      wrapper.getByText(parameterList[1].parameterGroup)
    ).toBeInTheDocument();
    expect(
      wrapper.getByText(parameterList[2].parameterGroup)
    ).toBeInTheDocument();
  });
});
