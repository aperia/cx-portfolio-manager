import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { stepRegistry } from '../_commons/WorkflowSetup/registries';
import DesignPromotionsMethod from './DesignPromotionsStep';
import GettingStartStep from './GettingStartStep';

stepRegistry.registerStep('GetStartedDesignPromotions', GettingStartStep);

stepRegistry.registerStep('DesignPromotionsMethod', DesignPromotionsMethod, [
  UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__DESIGN_PROMOTIONS__PARAMETERS
]);
