import { PAGE_SIZE } from 'app/constants/constants';
import { mapGridExpandCollapse } from 'app/helpers';
import {
  Button,
  ColumnType,
  Grid,
  Icon,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import {
  DEFAULT_PAGE_NUMBER,
  DEFAULT_PAGE_SIZE
} from 'app/_libraries/_dls/components/Pagination/constants';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import { useWorkflowFormStep } from 'pages/_commons/redux/WorkflowSetup';
import Paging from 'pages/_commons/Utils/Paging';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, { useEffect, useMemo, useState } from 'react';
import TableListSubGrid from './TableListSubGrid';

export interface TLPTableListProp {
  isValid?: boolean;
}

const TLPTableList: React.FC<WorkflowSetupProps<TLPTableListProp>> = ({
  stepId,
  selectedStep,
  type
}) => {
  const { t } = useTranslation();
  const [isExpand, setIsExpand] = useState(false);
  const [expandedList, setExpandedList] = useState<string[]>([]);
  const [page, setPage] = useState<number>(DEFAULT_PAGE_NUMBER);
  const [pageSize, setPageSize] = useState<number>(PAGE_SIZE[0]);

  const workflowFormStepName = useMemo(() => {
    switch (type) {
      case 'CA':
        return 'configMlpCa';
      case 'ST':
        return 'configMlpSt';
      default:
        return 'configMlpAq';
    }
  }, [type]);

  const { tables = [] } = useWorkflowFormStep(workflowFormStepName);

  const dataTablePage = tables?.slice(pageSize * (page - 1), page * pageSize);
  const handleChangePage = (page: number) => setPage(page);
  const handleChangePageSize = (pageSize: number) => setPageSize(pageSize);

  const columns: ColumnType[] = useMemo(
    () => [
      {
        id: 'name',
        Header: t('txt_manage_transaction_level_tlp_config_tq_table_id'),
        accessor: 'tableId'
      }
    ],
    [t]
  );

  useEffect(() => {
    if (stepId !== selectedStep) {
      setIsExpand(false);
      setExpandedList([]);
      setPage(DEFAULT_PAGE_NUMBER);
      setPageSize(PAGE_SIZE[0]);
    }
  }, [stepId, selectedStep]);

  useEffect(() => {
    if (dataTablePage.length === 0 && page > DEFAULT_PAGE_NUMBER) {
      setPage(page => page - 1);
    }
  }, [dataTablePage, page]);

  const total = useMemo(() => tables?.length || 0, [tables]);

  const handleGetTableList = useMemo(() => {
    switch (type) {
      case 'CA':
        return 'MLPCA';
      case 'ST':
        return 'MLPST';
      default:
        return 'MLPAQ';
    }
  }, [type]);

  return (
    <div className="border-bottom py-24">
      <div className="d-flex align-items-center ">
        <div className="flex-shrink-0 ml-n2 mr-8">
          <Tooltip
            element={isExpand ? t('txt_collapse') : t('txt_expand')}
            variant="primary"
            placement="top"
          >
            <Button
              variant="icon-secondary"
              size="sm"
              onClick={() => setIsExpand(!isExpand)}
            >
              <Icon name={isExpand ? 'minus' : 'plus'} />
            </Button>
          </Tooltip>
        </div>
        <h5>
          {type !== 'ST'
            ? t('txt_manage_transaction_level_tlp_config_tq_tableList')
            : t('txt_manage_account_level_table_decision_element_list')}
        </h5>
      </div>
      {isExpand && (
        <div className="mt-8">
          <p className="color-grey">
            {type !== 'ST'
              ? t(
                  'txt_manage_transaction_level_tlp_download_template_help_text'
                )
              : t(
                  'txt_manage_method_level_processing_decision_tables_upload_file_client_allocation_help_text'
                )}
          </p>
          {!isEmpty(tables) && type !== 'ST' && (
            <div className="mt-16">
              <Grid
                columns={columns}
                data={dataTablePage}
                subRow={(data: any) => {
                  return (
                    <div className="p-20">
                      <TableListSubGrid
                        elementList={data?.original?.elementList}
                        tableControlParameters={
                          data?.original?.tableControlParameters
                        }
                        tableType={handleGetTableList}
                      />
                    </div>
                  );
                }}
                togglable
                expandedItemKey="rowId"
                dataItemKey="rowId"
                toggleButtonConfigList={dataTablePage.map(
                  mapGridExpandCollapse('rowId', t)
                )}
                expandedList={expandedList}
                onExpand={setExpandedList}
              />
              {total > DEFAULT_PAGE_SIZE && (
                <div className="mt-16">
                  <Paging
                    page={page}
                    pageSize={pageSize}
                    totalItem={total}
                    onChangePage={handleChangePage}
                    onChangePageSize={handleChangePageSize}
                  />
                </div>
              )}
            </div>
          )}
          {!isEmpty(tables) && type === 'ST' && (
            <div className="pt-20">
              <TableListSubGrid
                elementList={dataTablePage?.[0]?.elementList || []}
                tableType={handleGetTableList}
              />
            </div>
          )}
        </div>
      )}
    </div>
  );
};

const ExtraStaticTLPTableList =
  TLPTableList as WorkflowSetupStaticProp<TLPTableListProp>;

ExtraStaticTLPTableList.defaultValues = { isValid: true };

export default ExtraStaticTLPTableList;
