import { RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { TableFieldParameterEnum } from 'app/constants/enums';
import { renderWithMockStore } from 'app/utils';
import 'app/utils/_mockComponent/mockUseTranslation';
import * as getElementAttribute from 'app/_libraries/_dls/utils/getElementAttribute';
import * as CommonSelectHooks from 'pages/_commons/redux/Common/select-hooks';
import * as WorkflowSetupTLP from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageTransactionLevelProcessingDecisionTLP';
import * as selectHooks from 'pages/_commons/redux/WorkflowSetup/select-hooks/_common';
import React from 'react';
import TableListSubGrid from './TableListSubGrid';

const useSelectDecisionElementListData = jest.spyOn(
  WorkflowSetupTLP,
  'useSelectDecisionElementListData'
);

const useWorkflowFormStepMock = jest.spyOn(selectHooks, 'useWorkflowFormStep');

const mockUseSelectWindowDimension = jest.spyOn(
  CommonSelectHooks,
  'useSelectWindowDimension'
);

const renderComponent = (props: any): RenderResult => {
  return renderWithMockStore(
    <div>
      <TableListSubGrid {...props} />
    </div>
  ) as any;
};

jest.mock('resize-observer-polyfill', () =>
  jest.requireActual('app/_libraries/_dls/test-utils/mocks/MockResizeObserver')
);
jest.mock('app/_libraries/_dls/utils/canvasTextWidth.ts');

beforeEach(() => {
  jest
    .spyOn(getElementAttribute, 'getAvailableWidth')
    .mockImplementation(() => 1000);
  mockUseSelectWindowDimension.mockImplementation(() => ({
    width: 1920,
    height: 1080
  }));
  useSelectDecisionElementListData.mockReturnValue({
    decisionElementList: [
      {
        name: 'ACS CURR PORT',
        moreInfo:
          'Adaptive Control System portfolio currently in effect for the customer account; variable-length, 4 positions, numeric.',
        immediateAllocation: 'No',
        configurable: true
      },
      {
        name: 'ACS PORT CHG DATE',
        moreInfo:
          'Date (MMDDYYYY) the current Adaptive Control System portfolio identification was last changed; variable-length, 10 positions, numeric.\n Example: 031404 or 03142004 = March 14, 2004',
        immediateAllocation: 'No',
        configurable: true
      },
      {
        name: 'ADVERTISING GROUP',
        moreInfo:
          'Issuer-defined advertising group code; variable-length, 3 positions, numeric.',
        immediateAllocation: 'No',
        configurable: true
      },
      {
        name: 'AGE',
        moreInfo:
          'Principal cardholder’s age expressed in whole number of years; variable-length, 3 positions, numeric.\n To determine whether a cardholder account matches a value, the System subtracts the year of birth from the current year. The System adds one year if the month of birth has already passed. If the date of birth consists of zeros, the age is 0.',
        immediateAllocation: 'No',
        configurable: true
      },
      {
        name: 'ALT FIN RPT BRCH/P',
        moreInfo:
          'Issuer-defined alternate financial reporting branch/portfolio field; fixed-length, 5 positions, numeric.',
        immediateAllocation: 'No',
        configurable: true
      },
      {
        name: 'ALT FIN RPT C/NO C',
        moreInfo:
          'Issuer-defined alternate financial reporting card/no card field; fixed-length, 5 positions, numeric.',
        immediateAllocation: 'No',
        configurable: true
      },
      {
        name: 'ALT FIN RPT ENTITY',
        moreInfo:
          'Issuer-defined alternate financial reporting entity field; fixed-length, 5 positions, numeric.',
        immediateAllocation: 'No',
        configurable: true
      },
      {
        name: 'ALT FIN RPT OTHER1',
        moreInfo:
          'Issuer-defined alternate financial reporting other 1 field; fixed-length, 5 positions, numeric.',
        immediateAllocation: 'No',
        configurable: true
      },
      {
        name: 'ANN CASH INT RATE',
        moreInfo:
          'Annual cash advance interest rate on cardholder account record; fixed-length, 6 positions, numeric.\nYou must enter two positions, a decimal point, and three positions to the right of the decimal point.\nExamples:\n18.550 = 18.55%\n09.990 = 9.99%',
        immediateAllocation: 'No',
        configurable: true
      },
      {
        name: 'ANN MRCH INT RATE',
        moreInfo:
          'Annual merchandise interest rate on cardholder account record; fixed-length, 6 positions, numeric.\nYou must enter two positions, a decimal point, and three positions to the right of the decimal point.\nExamples:\n18.550 = 18.55%\n09.990 = 9.99%',
        immediateAllocation: 'No',
        configurable: true
      },
      {
        name: 'ANNUAL CHARGE FLAG',
        moreInfo:
          'Value indicating the annual charge rate for a cardholder account; valid codes are A-F, 0-6, 9.',
        immediateAllocation: 'No',
        configurable: true
      },
      {
        name: 'ANNUAL CHG DATE',
        moreInfo:
          'Next date (MMYYYY) that a cardholder account will be reviewed and assessed an annual charge; fixed-length, 6 positions, numeric.',
        immediateAllocation: 'No',
        configurable: true
      },
      {
        name: 'APPLICATION SCORE',
        moreInfo:
          'Application Processing System (EAPS) first-pass score, based on the information provided on the new cardholder account application; variable-length, 5 positions, numeric.\nThis score does not include information provided by the credit bureau.',
        immediateAllocation: 'No',
        configurable: true
      },
      {
        name: 'APPROVAL OFFICER',
        moreInfo:
          'Issuer-defined code representing the person at your institution who approved this account; variable-length, 7 positions, numeric.',
        immediateAllocation: 'No',
        configurable: true
      },
      {
        name: 'ATTRITION INDEX',
        moreInfo:
          'Issuer-defined identifier of the attrition index; variable-length, 3 positions, numeric.',
        immediateAllocation: 'No',
        configurable: true
      },
      {
        name: 'AUTHORIZATION FLAG',
        moreInfo:
          'Value used to control authorizations or correspondence; fixed-length, 1 position, alphanumeric.',
        immediateAllocation: 'No',
        configurable: true
      },
      {
        name: 'AVG END BAL LST 03',
        moreInfo:
          'Dollar-and-cent amount of the customer account’s average ending balance for the last three cycles; variable-length, 15 positions, numeric.\nExample: 100019 = $1,000.19\nTo determine whether a customer account matches a value, the System totals the ending balances for the last three cycles and divides the result by three. The result is the correct average if the customer account has cycled three times in the Fiserv System.',
        immediateAllocation: 'No',
        configurable: true
      },
      {
        name: 'AVG END BAL LST 06',
        moreInfo:
          'Dollar-and-cent amount of the customer account’s average ending balance for the last six cycles; variable-length, 15 positions, numeric.\nExample: 100019 = $1,000.19\nTo determine whether a customer account matches a value, the System totals the ending balances for the last six cycles and divides the result by six. The result is the correct average if the customer account has cycled six times in the Fiserv System.',
        immediateAllocation: 'No',
        configurable: true
      },
      {
        name: 'AVG END BAL LST 12',
        moreInfo:
          'Dollar-and-cent amount of the customer account’s average ending balance for the last 12 cycles; variable-length, 15 positions, numeric.\nExample: 100019 = $1,000.19\nTo determine whether a customer account matches a value, the System totals the ending balances for the last 12 cycles and divides the result by 12. The result is the correct average if the customer account has cycled 12 times in the Fiserv System.',
        immediateAllocation: 'No',
        configurable: true
      },
      {
        name: 'BEHAVIOR SCORE',
        moreInfo:
          'Number representing the likelihood that an account will go three or more cycles delinquent in the next six months; variable-length, 3 positions, numeric.',
        immediateAllocation: 'No',
        configurable: true
      },
      {
        name: 'BILLING CYCLE CODE',
        moreInfo:
          'Code determining when the customer statement is generated; variable-length, 2 positions, numeric.',
        immediateAllocation: 'No',
        configurable: true
      },
      {
        name: 'BONUS STRATEGY',
        moreInfo:
          'Identifier of the bonus strategy established for the Rewards product; variable-length, 8 positions, alphanumeric.\nThis field must match an existing bonus strategy. Refer to the Rewards manual for more information about bonus strategies.',
        immediateAllocation: 'No',
        configurable: true
      },
      {
        name: 'ACCT OPEN DAYS',
        moreInfo: null,
        immediateAllocation: 'No',
        configurable: true
      },
      {
        name: 'ACQUIRING BIN',
        moreInfo: null,
        immediateAllocation: 'No',
        configurable: true
      },
      {
        name: 'ACQUIRING MC ICA',
        moreInfo: null,
        immediateAllocation: 'No',
        configurable: true
      },
      {
        name: 'ACQUIRING VISA BIN',
        moreInfo: null,
        immediateAllocation: 'No',
        configurable: true
      },
      {
        name: 'CIT METHOD FLAG',
        moreInfo:
          'Enter the Promotion Identifier in the RESULT ID field for each criteria row to point cardholder accounts to Product Control File settings.\nFor the transaction qualification tables, the Promotion Identifier is a variable-length, 8-position code that identifies a group of Product Control File methods. The methods include parameter settings that control processing options.',
        immediateAllocation: 'No',
        configurable: false
      },
      {
        name: 'RESULT ID',
        moreInfo:
          'Enter the Promotion Identifier in the RESULT ID field for each criteria row to point cardholder accounts to Product Control File settings.\nFor the transaction qualification tables, the Promotion Identifier is a variable-length, 8-position code that identifies a group of Product Control File methods. The methods include parameter settings that control processing options.',
        immediateAllocation: 'No',
        configurable: false
      }
    ]
  });
});

afterEach(() => {
  mockUseSelectWindowDimension.mockClear();
  useWorkflowFormStepMock.mockClear();
});

const props = {
  elementList: [
    {
      configurable: true,
      immediateAllocation: 'Yes',
      moreInfo:
        'Principal cardholders age expressed in whole number of years; variable-length, 3 positions, numeric.\nTo determine whether a cardholder account matches a value, the System subtracts the year of birth from the current year. The System adds one year if the month of birth has already passed. If the date of birth consists of zeros, the age is 0.',
      name: 'AGE',
      rowId: 'QIbO9VcqvPVLbAIMP7j9h',
      searchCode: 'E',
      searchCodeText: 'E - Exact',
      value: ['25', '26', '27']
    },
    {
      configurable: true,
      immediateAllocation: 'Yes',
      moreInfo:
        'Issuer-defined advertising group code; variable-length, 3 positions, numeric.',
      name: 'ADVERTISING GROUP',
      rowId: 'etEaGuMEzJW7347lohmjJ',
      searchCode: 'R',
      searchCodeText: 'R - Range',
      value: ['500', '500', '***']
    },
    {
      configurable: true,
      immediateAllocation: 'Yes',
      isNewElement: false,
      moreInfo:
        'Annual cash advance interest rate on cardholder account record; fixed-length, 6 positions, numeric.\nYou must enter two positions, a decimal point, and three positions to the right of the decimal point.\nExamples:\n18.550 = 18.55%\n09.990 = 9.99%',
      name: 'ANN CASH INT RATE',
      rowId: 'VwNV8qTbxKXAVSW8szkoj',
      searchCode: 'E',
      searchCodeText: 'E - Exact'
    }
  ],
  tableType: 'MLPAQ',
  tableControlParameters: {
    [TableFieldParameterEnum.ChangeInTermsMethodFlag]: 'Y',
    [TableFieldParameterEnum.AllocationDate]: '02/02/2022'
  }
};

describe('WorkflowManageMethodLevelProcessingDecisionTablesMLP > TableListSubGrid', () => {
  it('Should render a grid with content with ChangeInTermsMethodFlag', async () => {
    await renderComponent(props);
  });
  it('Should render a grid with content with not ChangeInTermsMethodFlag', async () => {
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1200,
      height: 1080
    }));
    await renderComponent({ ...props, tableControlParameters: {} });
  });
  it('Should render a grid with content with no data', async () => {
    useSelectDecisionElementListData.mockReturnValue({});
    await renderComponent({
      tableType: 'MLPAQ',
      tableControlParameters: {
        [TableFieldParameterEnum.ChangeInTermsMethodFlag]: 'Y',
        [TableFieldParameterEnum.AllocationDate]: '02/02/2022'
      }
    });
  });
  it('Should handleTruncate', async () => {
    useSelectDecisionElementListData.mockReturnValue({});
    const wrapper = await renderComponent(props);

    const moreButton = wrapper.getAllByText('txt_more');
    userEvent.click(moreButton[2]);
  });
});
