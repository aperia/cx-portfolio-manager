import { TableFieldParameterEnum } from 'app/constants/enums';
import {
  ColumnType,
  Grid,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import { useSelectDecisionElementListData } from 'pages/_commons/redux/WorkflowSetup';
import { formatText } from 'pages/_commons/Utils/formatGridField';
import React, { useCallback, useMemo, useRef } from 'react';

interface ITLPTableListSubGrid {
  elementList: any[];
  tableControlParameters?: Record<string, any>;
  tableType: string;
}

const TLPTableListSubGrid: React.FC<ITLPTableListSubGrid> = ({
  elementList = [],
  tableType,
  tableControlParameters
}) => {
  const currentTableList = useRef<Record<string, boolean>>({});
  const data = useSelectDecisionElementListData(tableType);
  const decisionElementList = useMemo(
    () => data?.decisionElementList || [],
    [data?.decisionElementList]
  );
  const nonConfigurableOption = useMemo(
    () =>
      decisionElementList.filter((el: any) => {
        return tableControlParameters?.[
          TableFieldParameterEnum.ChangeInTermsMethodFlag
        ] === 'Y'
          ? !el.configurable
          : !el.configurable && el.name !== 'CIT METHOD FLAG';
      }),
    [decisionElementList, tableControlParameters]
  );

  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();
  const elementListShow = [...elementList, ...nonConfigurableOption];

  const handleTruncate = useCallback(
    (name: string) => (isTruncated: boolean, showAll: boolean) => {
      if (isTruncated && showAll) {
        currentTableList.current = {
          ...currentTableList.current,
          [name]: true
        };
      } else {
        currentTableList.current = {
          ...currentTableList.current,
          [name]: false
        };
      }
    },
    []
  );

  const columns: ColumnType[] = [
    {
      id: 'name',
      Header: 'Decision Element',
      accessor: formatText(['name']),
      cellBodyProps: { className: 'px-20 py-12' },
      width: width < 1280 ? 200 : 270
    },
    {
      id: 'moreInfo',
      Header: t('txt_description'),
      accessor: ({ name, moreInfo }) => (
        <TruncateText
          id={name}
          className={currentTableList.current?.[name] ? 'pre-line' : ''}
          resizable
          lines={2}
          ellipsisLessText={t('txt_less')}
          ellipsisMoreText={t('txt_more')}
          onTruncate={handleTruncate(name)}
        >
          {moreInfo}
        </TruncateText>
      )
    }
  ];

  return <Grid columns={columns} data={elementListShow} />;
};

export default TLPTableListSubGrid;
