import { PAGE_SIZE } from 'app/constants/constants';
import { BadgeColorType } from 'app/constants/enums';
import { mapGridExpandCollapse } from 'app/helpers';
import { Button, ColumnType, Grid, useTranslation } from 'app/_libraries/_dls';
import {
  DEFAULT_PAGE_NUMBER,
  DEFAULT_PAGE_SIZE
} from 'app/_libraries/_dls/components/Pagination/constants';
import { orderBy } from 'app/_libraries/_dls/lodash';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import {
  formatBadge,
  formatText,
  formatTruncate
} from 'pages/_commons/Utils/formatGridField';
import Paging from 'pages/_commons/Utils/Paging';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { AddNewTableAction } from './AddNewTableModal';
import TableListSubGrid from './TableListSubGrid';
import {
  CONFIG_ACTIONS_MODAL,
  EXPANDED_LIST,
  EXPAND_ACTION
} from './useConfigActions';

interface ITableList {
  tableList: any[];
  handleSetSelectedTable: (selectedTable: any) => void;
  handleOpenConfigModal: (tableName: CONFIG_ACTIONS_MODAL) => void;
  handleSetExpandedList: (
    tableListName: EXPANDED_LIST
  ) => (expandItem: Record<string, any>) => void;
  expandedList: Record<string, any>;
  type?: string;
  selectedStep?: string;
  handleChangeRowIdAdded: (rowId: string | undefined) => void;
  rowIdAdded?: string;
}

const TableList: React.FC<ITableList> = ({
  tableList,
  handleSetSelectedTable,
  handleOpenConfigModal,
  handleSetExpandedList,
  expandedList,
  type,
  selectedStep,
  rowIdAdded,
  handleChangeRowIdAdded
}) => {
  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();
  const [page, setPage] = useState<number>(DEFAULT_PAGE_NUMBER);
  const [pageSize, setPageSize] = useState<number>(PAGE_SIZE[0]);
  const [total, setTotal] = useState<number | null>(null);

  const dataTablePage = useMemo(() => {
    return orderBy(tableList, 'tableId', 'asc').slice(
      pageSize * (page - 1),
      page * pageSize
    );
  }, [tableList, pageSize, page]);

  const handleChangePage = (page: number) => setPage(page);
  const handleChangePageSize = (pageSize: number) => setPageSize(pageSize);

  useEffect(() => {
    if (page !== DEFAULT_PAGE_NUMBER) {
      setPage(DEFAULT_PAGE_NUMBER);
    }

    if (pageSize !== PAGE_SIZE[0]) {
      setPageSize(PAGE_SIZE[0]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedStep]);

  useEffect(() => {
    if (!rowIdAdded || total === null) return;
    const index = orderBy(tableList, 'tableId', 'asc').findIndex(
      i => i.rowId === rowIdAdded
    );
    const pageHasTableChanged = Math.ceil((index + 1) / pageSize);
    setPage(pageHasTableChanged);
    handleChangeRowIdAdded(undefined);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [rowIdAdded]);

  useEffect(() => {
    if (total !== tableList.length) {
      setTotal(tableList.length);
    }
  }, [tableList, total]);

  useEffect(() => {
    if (dataTablePage.length === 0 && page > DEFAULT_PAGE_NUMBER) {
      setPage(page => page - 1);
    }
  }, [dataTablePage, page]);

  const handleEditTable = useCallback(
    (table: any) => () => {
      handleSetSelectedTable(table);
      setPage(1);
      handleOpenConfigModal(CONFIG_ACTIONS_MODAL.addNewTableModal);
    },
    [handleSetSelectedTable, handleOpenConfigModal]
  );

  const handleDeleteTable = useCallback(
    table => () => {
      handleSetSelectedTable(table);
      handleOpenConfigModal(CONFIG_ACTIONS_MODAL.deleteTableModal);
    },
    [handleOpenConfigModal, handleSetSelectedTable]
  );

  const handleExpand = (expandList: string[]) => {
    const expandItem = {} as Record<string, any>;
    expandList.forEach((expandKey: string) => {
      expandItem[expandKey] = true;
    });
    handleSetExpandedList(EXPANDED_LIST.tableListIndex)({
      expandAction: EXPAND_ACTION.replace,
      expandedList: expandItem
    });
  };

  const expandedListGrid = useMemo(() => {
    return Object.keys(expandedList);
  }, [expandedList]);

  const methodTypeToText = useCallback((type: WorkflowSetupMethodType) => {
    switch (type) {
      case AddNewTableAction.createNewVersion:
        return 'Version Created';
      case AddNewTableAction.createNewTable:
        return 'Table Created';
      default:
        return 'Table Modeled';
    }
  }, []);

  const columns: ColumnType[] = useMemo(
    () => [
      {
        id: 'tableId',
        Header: t('txt_manage_transaction_level_tlp_config_tq_table_id'),
        accessor: formatText(['tableId']),
        width: 120
      },
      {
        id: 'tableName',
        Header: t('txt_manage_transaction_level_tlp_config_tq_table_name'),
        accessor: formatText(['tableName']),
        width: 120
      },
      {
        id: 'modelOrCreateFrom',
        Header: t('txt_change_interest_rates_model_create'),
        accessor: formatText(['selectedTableId']),
        width: 128
      },
      {
        id: 'comment',
        Header: t('txt_comment_area'),
        accessor: formatTruncate(['comment']),
        width: width > 1280 ? 360 : 300
      },
      {
        id: 'methodType',
        Header: t('txt_manage_penalty_fee_action_taken'),
        accessor: (data, idx) =>
          formatBadge(['tableType'], {
            colorType: BadgeColorType.TableType,
            noBorder: true
          })(
            {
              ...data,
              tableType: methodTypeToText(data.actionTaken)
            },
            idx
          ),
        width: 153
      },
      {
        id: 'actions',
        Header: t('txt_actions'),
        className: 'text-center',
        accessor: (data: any) => (
          <div className="d-flex justify-content-center">
            <Button
              size="sm"
              variant="outline-primary"
              onClick={handleEditTable(data)}
            >
              {t('txt_edit')}
            </Button>
            <Button
              size="sm"
              variant="outline-danger"
              className="ml-8"
              onClick={handleDeleteTable(data)}
            >
              {t('txt_delete')}
            </Button>
          </div>
        ),
        cellBodyProps: { className: 'text-center py-8' },
        width: 132
      }
    ],
    [t, methodTypeToText, handleEditTable, handleDeleteTable, width]
  );

  const totalGrid = useMemo(() => tableList?.length, [tableList]);

  return (
    <div className="pt-16">
      <Grid
        columns={columns}
        data={dataTablePage}
        subRow={(data: any) => {
          return (
            <TableListSubGrid
              elementList={data?.original?.elementList}
              tableControlParameters={data?.original?.tableControlParameters}
              type={type}
            />
          );
        }}
        togglable
        toggleButtonConfigList={tableList.map(
          mapGridExpandCollapse('rowId', t)
        )}
        expandedItemKey={'rowId'}
        expandedList={expandedListGrid}
        dataItemKey="rowId"
        onExpand={handleExpand}
      />
      {totalGrid > DEFAULT_PAGE_SIZE && (
        <div className="mt-16">
          <Paging
            page={page}
            pageSize={pageSize}
            totalItem={totalGrid}
            onChangePage={handleChangePage}
            onChangePageSize={handleChangePageSize}
          />
        </div>
      )}
    </div>
  );
};

export default TableList;
