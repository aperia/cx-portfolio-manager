import { render, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import 'app/utils/_mockComponent/mockNoDataFound';
import 'app/utils/_mockComponent/mockPaging';
import * as CommonSelectHooks from 'pages/_commons/redux/Common/select-hooks';
import React from 'react';
import * as reactRedux from 'react-redux';
import GridDecision from './GridDecision';
import * as hooks from './useGridDecision';

const mockUseSelectWindowDimension = jest.spyOn(
  CommonSelectHooks,
  'useSelectWindowDimension'
);

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

jest.mock('app/_libraries/_dls/components/CheckBox', () => {
  const MockCheckBox = ({ onChange }: any) => {
    return (
      <div
        onClick={() =>
          onChange({
            target: {
              value: {
                checked: true
              }
            }
          })
        }
      >
        MockCheckBox
      </div>
    );
  };

  MockCheckBox.Input = () => <div>Mock Input</div>;
  return {
    __esModule: true,
    default: MockCheckBox
  };
});

HTMLCanvasElement.prototype.getContext = jest.fn();
jest.mock('pages/_commons/Utils/ClearAndResetButton', () => ({
  __esModule: true,
  default: ({ onClearAndReset }: any) => (
    <button onClick={onClearAndReset}>{'Clear_And_Reset'}</button>
  )
}));

const SearchCodeOptions = [
  {
    code: 'E',
    text: 'E - Exact'
  },
  {
    code: 'R',
    text: 'R - Range'
  }
];

const ElementOptions = [
  {
    code: 'test',
    text: 'Test'
  },
  {
    code: 'test-1',
    text: 'Test-1'
  }
];

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <GridDecision {...props} />
    </div>
  );
};

describe('WorkflowManageMethodLevelProcessingDecisionTablesMLP > ConfigureParameters > ElementList', () => {
  const mockDispatch = jest.fn();
  const handleSetTouchField = () => jest.fn();
  const handleChangeElementValue = () => jest.fn();
  const elementList = [] as any[];
  const handleSetElementList = jest.fn();
  const handleSetElementListHasError = jest.fn();
  const handleDeleteElement = () => jest.fn();
  const handleChangeSearchValue = jest.fn();
  const handleChangeImmediate = jest.fn();
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1920,
      height: 1080
    }));
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
    mockUseSelectWindowDimension.mockClear();
  });

  it('Should render Element List With 1 element', () => {
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1280,
      height: 1080
    }));
    jest.spyOn(hooks, 'useGridDecision').mockImplementation(() => {
      return {
        elements: [
          {
            name: '',
            searchCode: '',
            rowId: '1',
            isNewElement: true
          }
        ],
        searchCodeOptions: SearchCodeOptions,
        elementListNameOptions: ElementOptions,
        handleSetTouchField,
        handleChangeElementValue
      } as any;
    });
    const wrapper = renderComponent({
      elementList,
      handleSetElementList,
      handleSetElementListHasError
    });
    expect(
      wrapper.getByText(
        'txt_manage_transaction_level_tlp_config_tq_decision_element_list'
      )
    ).toBeInTheDocument();
  });

  it('Should render Element List With 2 element', () => {
    jest.spyOn(hooks, 'useGridDecision').mockImplementation(() => {
      return {
        elements: [
          {
            name: '',
            searchCode: '',
            rowId: '1',
            isNewElement: true
          },
          {
            name: 'test',
            searchCode: 'E',
            rowId: '2'
          }
        ],
        searchCodeOptions: SearchCodeOptions,
        elementListNameOptions: ElementOptions,
        handleSetTouchField,
        handleChangeElementValue,
        touches: { 2: { name: true, searchCode: true } },
        errors: { 2: { name: 'error', searchCode: 'error' } },
        handleDeleteElement,
        searchValue: 'search',
        handleChangeSearchValue
      } as any;
    });
    const wrapper = renderComponent({
      elementList,
      handleSetElementList,
      handleSetElementListHasError
    });
    expect(
      wrapper.getByText(
        'txt_manage_transaction_level_tlp_config_tq_decision_element_list'
      )
    ).toBeInTheDocument();
    const clearAndResetBtn = wrapper.getByText('Clear_And_Reset');
    userEvent.click(clearAndResetBtn);
    expect(handleChangeSearchValue).toBeCalled();
  });

  it('Should render No Data found', () => {
    jest.spyOn(hooks, 'useGridDecision').mockImplementation(() => {
      return {
        elements: [],
        searchValue: 'search',
        handleChangeSearchValue
      } as any;
    });
    const wrapper = renderComponent({
      elementList,
      handleSetElementList,
      handleSetElementListHasError
    });
    const noData = wrapper.getByText('No data found');
    expect(noData).toBeInTheDocument();
    userEvent.click(noData);
    expect(handleChangeSearchValue).toBeCalled();
  });

  it('Should render Element List with 20 Elements', () => {
    jest.spyOn(hooks, 'useGridDecision').mockImplementation(() => {
      return {
        elements: [
          {
            name: 'test',
            searchCode: 'E',
            rowId: '1',
            immediateAllocation: 'Yes'
          },
          {
            name: 'test',
            searchCode: 'E',
            rowId: '2',
            immediateAllocation: 'Yes'
          },
          {
            name: '',
            searchCode: 'E',
            rowId: '3',
            immediateAllocation: 'Y'
          },
          {
            name: 'test',
            searchCode: 'E',
            rowId: '4',
            immediateAllocation: 'N'
          }
        ],
        totalElement: 20,
        searchValue: 'search',
        handleChangeSearchValue,
        searchCodeOptions: SearchCodeOptions,
        elementListNameOptions: ElementOptions,
        handleSetTouchField,
        handleChangeElementValue,
        touches: { 2: { name: true, searchCode: true } },
        errors: { 2: { name: 'error', searchCode: 'error' } },
        handleDeleteElement,
        handleChangeImmediate
      } as any;
    });
    const wrapper = renderComponent({
      elementList: [
        {
          name: 'test',
          immediateAllocation: 'Yes'
        },
        {
          name: '',
          immediateAllocation: 'Yes'
        },
        {
          name: 'name',
          immediateAllocation: ''
        }
      ],
      handleSetElementList,
      handleSetElementListHasError
    });

    userEvent.click(wrapper.getAllByText('MockCheckBox')[0]);
  });
});
