import { deepEqual } from 'app/helpers';
import { ConfigureParametersValue } from '../ConfigureParameters';

const isEqualFormValues: WorkflowSetupIsEqualFormValuesFunc<ConfigureParametersValue> =
  (pre, next) => {
    return deepEqual(pre, next, ['configurable', 'moreInfo', 'searchCodeText']);
  };

export default isEqualFormValues;
