import { fireEvent, render, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { queryAllByClass } from 'app/_libraries/_dls/test-utils';
import * as CommonRedux from 'pages/_commons/redux/Common/select-hooks';
import * as mockSelector from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageMethodLevelProcessingDecisionMLP';
import React from 'react';
import Summary from './Summary';

const mockStore = jest.spyOn(
  mockSelector,
  'useSelectElementMetadataManageMethodLevelProcessingMLP'
);

const useSelectWindowDimension = jest.spyOn(
  CommonRedux,
  'useSelectWindowDimension'
);

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

jest.mock('pages/_commons/Utils/Paging', () => ({
  __esModule: true,
  default: ({ onChangePage, onChangePageSize }: any) => {
    return (
      <div>
        <h1>Paging</h1>
        <button onClick={() => onChangePage(2)}>Page</button>
        <button onClick={() => onChangePageSize(20)}>PageSize</button>
      </div>
    );
  }
}));

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <Summary {...props} />
    </div>
  );
};

const props = {
  originalStepConfig: {},
  onEditStep: jest.fn(),
  selectedStep: '1',
  stepId: '2',
  formValues: {
    tables: [
      {
        original: { elementList: [], tableControlParameters: [] },
        actionTaken: 'createNewTable',
        tableType: 'tableType',
        tableId: 'Table 1',
        tableName: 'Table Name 1'
      },
      {
        original: { elementList: [], tableControlParameters: [] },
        actionTaken: 'addTableToModel',
        tableType: 'tableType',
        tableId: 'Table 2',
        tableName: 'Table Name 2'
      },
      {
        original: { elementList: [], tableControlParameters: [] },
        actionTaken: 'createNewVersion',
        tableType: 'tableType',
        tableId: 'Table 3',
        tableName: 'Table Name 3'
      },
      {
        original: { elementList: [], tableControlParameters: [] },
        tableType: 'tableType',
        tableId: 'Table 4',
        tableName: 'Table Name 4'
      },
      {
        original: { elementList: [], tableControlParameters: [] },
        tableType: 'tableType',
        tableId: 'Table 5',
        tableName: 'Table Name 5'
      },
      {
        original: { elementList: [], tableControlParameters: [] },
        tableType: 'tableType',
        tableId: 'Table 6',
        tableName: 'Table Name 6'
      },
      {
        original: { elementList: [], tableControlParameters: [] },
        tableType: 'tableType',
        tableId: 'Table 7',
        tableName: 'Table Name 7'
      },
      {
        original: { elementList: [], tableControlParameters: [] },
        tableType: 'tableType',
        tableId: 'Table 8',
        tableName: 'Table Name 8'
      },
      {
        original: { elementList: [], tableControlParameters: [] },
        tableType: 'tableType',
        tableId: 'Table 9',
        tableName: 'Table Name 9'
      },
      {
        original: { elementList: [], tableControlParameters: [] },
        tableType: 'tableType',
        tableId: 'Table 10',
        tableName: 'Table Name 10'
      },
      {
        original: { elementList: [], tableControlParameters: [] },
        tableType: 'tableType',
        tableId: 'Table 11',
        tableName: 'Table Name 11'
      }
    ]
  }
};

describe('WorkflowManageMethodLevelProcessingDecisionTablesMLP > ConfigureParameters > Summary', () => {
  beforeEach(() => {
    useSelectWindowDimension.mockReturnValue({
      width: 1280,
      height: 2000
    } as any);
    mockStore.mockReturnValue({
      allocationBeforeAfterCycle: [],
      caAllocationFlag: [],
      aqAllocationFlag: [],
      allocationInterval: [],
      changeCode: [],
      changeInTermsMethodFlag: [],
      searchCode: [],
      serviceSubjectSectionAreas: []
    });
  });

  it('Should render Summary Config when width 1280', () => {
    const wrapper = renderComponent({ ...props, formValues: {} });

    expect(wrapper.getByText('txt_edit')).toBeInTheDocument();
  });
  it('Should render Summary Config when width 1300', () => {
    useSelectWindowDimension.mockReturnValue({
      width: 1300,
      height: 2000
    } as any);
    const wrapper = renderComponent({ ...props, stepId: '1' });

    fireEvent.click(wrapper.getByText('txt_edit'));
    const icon = queryAllByClass(wrapper.container, /icon-plus/);
    fireEvent.click(icon[0] as Element);
    fireEvent.click(icon[0] as Element);
    fireEvent.click(wrapper.getByText('Page'));
    fireEvent.click(wrapper.getByText('PageSize'));
  });

  it('Should render Summary Config when width 1300', () => {
    useSelectWindowDimension.mockReturnValue({
      width: 1300,
      height: 2000
    } as any);
    const wrapper = render(<Summary {...props} />);

    userEvent.click(wrapper.getByText('txt_edit'));
    userEvent.click(wrapper.getByText('Page'));

    wrapper.rerender(<Summary {...props} selectedStep="3" />);
  });
  it('Should render Summary Config when page size 25', () => {
    useSelectWindowDimension.mockReturnValue({
      width: 1300,
      height: 2000
    } as any);
    const wrapper = render(<Summary {...props} />);

    userEvent.click(wrapper.getByText('txt_edit'));
    userEvent.click(wrapper.getByText('PageSize'));

    wrapper.rerender(<Summary {...props} selectedStep="3" />);
  });
});
