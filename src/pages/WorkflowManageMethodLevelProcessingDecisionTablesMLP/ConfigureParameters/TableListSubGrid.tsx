import { ColumnType, Grid, useTranslation } from 'app/_libraries/_dls';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import { formatText, viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React from 'react';
import TableParameterInfo from './TableParameterInfo';

interface ITableListSubGrid {
  elementList: any[];
  tableControlParameters: Record<string, any>;
  type?: string;
}

const TableListSubGrid: React.FC<ITableListSubGrid> = ({
  elementList = [],
  tableControlParameters = {},
  type = 'AQ'
}) => {
  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();

  const columns: ColumnType[] = [
    {
      id: 'name',
      Header: t('txt_manage_transaction_level_tlp_decision_element'),
      accessor: formatText(['name']),
      width: width > 1280 ? undefined : 280
    },
    {
      id: 'searchCode',
      Header: t('txt_manage_transaction_level_tlp_config_tq_search_code'),
      accessor: formatText(['searchCodeText']),
      width: width > 1280 ? 240 : 210
    },
    {
      id: 'immediateAllocation',
      Header: t('txt_manage_account_level_table_decision_immediate_allocation'),
      accessor: item =>
        ['Yes', 'N'].includes(item.immediateAllocation) ? 'Y' : 'N',
      width: 182
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      accessor: viewMoreInfo(['moreInfo'], t),
      width: 106
    }
  ];

  return (
    <div className="p-20">
      <h5 className="mb-16 fs-14">
        {t('txt_manage_account_level_table_parameters')}
      </h5>
      <TableParameterInfo
        type={type}
        tableControlParameters={tableControlParameters}
      />
      <h5 className="mt-24 mb-16 fs-14">
        {t('txt_manage_transaction_level_tlp_element_list')}
      </h5>
      <Grid columns={columns} data={elementList} />
    </div>
  );
};

export default TableListSubGrid;
