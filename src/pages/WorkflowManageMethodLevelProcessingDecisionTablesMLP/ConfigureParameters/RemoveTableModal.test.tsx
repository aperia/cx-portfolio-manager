import { fireEvent, render, RenderResult } from '@testing-library/react';
import { mockUseModalRegistryFnc } from 'app/utils';
import React from 'react';
import RemoveTableModal from './RemoveTableModal';

const mockUseModalRegistry = mockUseModalRegistryFnc();

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderModal = (props: any): RenderResult => {
  return render(
    <div>
      <RemoveTableModal {...props} />
    </div>
  );
};

describe('WorkflowManageMethodLevelProcessingDecisionTablesMLP > ConfigureParameters > RemoveTableModal', () => {
  beforeEach(() => {
    mockUseModalRegistry.mockImplementation(() => [true, false]);
  });

  afterEach(() => {
    mockUseModalRegistry.mockClear();
  });

  it('Should render modal', () => {
    const props = {
      id: '1',
      show: true,
      onClose: jest.fn()
    };

    const wrapper = renderModal(props);
    expect(wrapper.getByText('Confirm Delete')).toBeInTheDocument();
  });

  it('Should close modal without delete', () => {
    const onClose = jest.fn();

    const props = {
      id: '1',
      show: true,
      onClose
    };

    const wrapper = renderModal(props);
    fireEvent.click(
      wrapper.baseElement.querySelector(
        'button[class="btn btn-secondary"]'
      ) as Element
    );
    expect(onClose).toBeCalled();
  });

  it('Should close modal with deleted', () => {
    const onClose = jest.fn();
    const onOk = jest.fn();
    const props = {
      id: '1',
      show: true,
      onClose,
      onOk
    };

    const wrapper = renderModal(props);
    fireEvent.click(
      wrapper.baseElement.querySelector(
        'button[class="btn btn-danger"]'
      ) as Element
    );
    expect(onOk).toBeCalled();
  });
});
