import EnhanceDropdownButton from 'app/components/EnhanceDropdownButton';
import { ADD_TABLE_BUTTONS } from 'app/constants/constants';
import { AddTableButtons } from 'app/constants/enums';
import { useTranslation } from 'app/_libraries/_dls';
import {
  DropdownButton,
  DropdownButtonSelectEvent,
  Icon,
  Tooltip
} from 'app/_libraries/_dls/components';
import React, { Fragment, useCallback, useMemo } from 'react';

interface ActionButtonsProps {
  small?: boolean;
  title?: string;
  onClickCreateNewVersion?: () => void;
  onClickTableToModel?: () => void;
  onClickAddNewTable?: () => void;
}

const ActionButtons: React.FC<ActionButtonsProps> = ({
  small = false,
  title = '',
  onClickCreateNewVersion,
  onClickTableToModel,
  onClickAddNewTable
}) => {
  const { t } = useTranslation();

  const renderButtons = useMemo(() => {
    return (
      <Fragment>
        <h5>{t('txt_manage_penalty_fee_select_an_option')}</h5>
        <div className="row mt-16">
          <div className="col-xl col-6">
            <div
              className="rcc-btn d-flex justify-content-between border rounded-lg py-10"
              onClick={onClickCreateNewVersion}
            >
              <span className="d-flex align-items-center ml-16">
                <Icon
                  name="method-create"
                  size="9x"
                  className="color-grey-l16"
                />
                <span className="ml-12 mr-8">
                  {t('txt_manage_penalty_fee_create_new_version')}
                </span>
                <Tooltip
                  element={t(
                    'txt_manage_transaction_level_tlp_create_new_version_info'
                  )}
                  triggerClassName="d-flex"
                >
                  <Icon
                    name="information"
                    className="color-grey-l16"
                    size="5x"
                  />
                </Tooltip>
              </span>
            </div>
          </div>
          <div className="col-xl col-6">
            <div
              className="rcc-btn d-flex justify-content-between border rounded-lg py-10"
              onClick={onClickTableToModel}
            >
              <span className="d-flex align-items-center ml-16">
                <Icon
                  name="method-clone"
                  size="9x"
                  className="color-grey-l16"
                />
                <span className="ml-12 mr-8">
                  {t('txt_manage_transaction_level_tlp_choose_table_to_model')}
                </span>
                <Tooltip
                  element={t(
                    'txt_manage_transaction_level_tlp_choose_table_to_model_info'
                  )}
                  triggerClassName="d-flex"
                >
                  <Icon
                    name="information"
                    className="color-grey-l16"
                    size="5x"
                  />
                </Tooltip>
              </span>
            </div>
          </div>
          <div className="col-xl col-6 mt-xl-0 mt-16">
            <div
              className="rcc-btn d-flex justify-content-between border rounded-lg py-10"
              onClick={onClickAddNewTable}
            >
              <span className="d-flex align-items-center ml-16">
                <Icon name="method-add" size="9x" className="color-grey-l16" />
                <span className="ml-12 mr-8">
                  {t('txt_manage_transaction_level_tlp_create_new_table')}
                </span>
                <Tooltip
                  element={t(
                    'txt_manage_transaction_level_tlp_create_new_table_info'
                  )}
                  triggerClassName="d-flex"
                >
                  <Icon
                    name="information"
                    className="color-grey-l16"
                    size="5x"
                  />
                </Tooltip>
              </span>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }, [t, onClickCreateNewVersion, onClickTableToModel, onClickAddNewTable]);

  const dropdownListItem = useMemo(() => {
    return ADD_TABLE_BUTTONS.map(item => (
      <DropdownButton.Item
        key={item.value}
        label={t(item.description)}
        value={item}
      />
    ));
  }, [t]);

  const onSelectAddMethod = useCallback(
    (event: DropdownButtonSelectEvent) => {
      const { value } = event.target.value;
      switch (value) {
        case AddTableButtons.CREATE_NEW_VERSION:
          onClickCreateNewVersion && onClickCreateNewVersion();
          break;
        case AddTableButtons.ADD_NEW_TABLE:
          onClickAddNewTable && onClickAddNewTable();
          break;
        case AddTableButtons.CHOOSE_TABLE_TO_MODEL:
          onClickTableToModel && onClickTableToModel();
          break;
      }
    },
    [onClickAddNewTable, onClickTableToModel, onClickCreateNewVersion]
  );

  return (
    <div>
      {small ? (
        <div className="d-flex align-items-center">
          <h5>{t(title)}</h5>
          <div className="d-flex ml-auto mr-n8">
            <EnhanceDropdownButton
              buttonProps={{
                size: 'sm',
                children: 'Add Table',
                variant: 'outline-primary'
              }}
              onSelect={onSelectAddMethod}
            >
              {dropdownListItem}
            </EnhanceDropdownButton>
          </div>
        </div>
      ) : (
        renderButtons
      )}
    </div>
  );
};

export default ActionButtons;
