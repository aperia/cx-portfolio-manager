import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { TableFieldParameterEnum } from 'app/constants/enums';
import React from 'react';
import * as reactRedux from 'react-redux';
import ConfigureParameters from '.';
import * as hooks from './useConfigActions';
import { CONFIG_ACTIONS_MODAL, EXPANDED_LIST } from './useConfigActions';

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');
jest.mock('./TableListModal', () => ({
  __esModule: true,
  default: ({ handleClose }: any) => {
    return (
      <div>
        <h1>TableListModal_component</h1>
        <button onClick={handleClose}>Close</button>
      </div>
    );
  }
}));

jest.mock('./AddNewTableModal', () => ({
  __esModule: true,
  default: ({ handleClose }: any) => {
    return (
      <div>
        <h1>AddNewTableModal_component</h1>
        <button onClick={handleClose}>Close</button>
      </div>
    );
  }
}));

jest.mock('./ActionButtons', () => ({
  __esModule: true,
  default: ({
    onClickAddNewTable,
    onClickTableToModel,
    onClickCreateNewVersion
  }: any) => {
    return (
      <div>
        <button onClick={onClickAddNewTable}>Add_New_Table</button>
        <button onClick={onClickTableToModel}>Table_To_Model</button>
        <button onClick={onClickCreateNewVersion}>Create_New_version</button>
      </div>
    );
  }
}));

jest.mock('./RemoveTableModal', () => ({
  __esModule: true,
  default: ({ onOk, onClose }: any) => {
    return (
      <div>
        <h1>RemoveTableModal_component</h1>
        <button onClick={onClose}>Close</button>
        <button onClick={onOk}>Ok</button>
      </div>
    );
  }
}));

jest.mock('./TableList', () => ({
  __esModule: true,
  default: () => {
    return <div>TableList_component</div>;
  }
}));

jest.mock('app/hooks/useUnsavedChangeRegistry', () => {
  return {
    __esModule: true,
    useUnsavedChangeRegistry: (options: any, changes: any, onConfirm: any) => {
      onConfirm && onConfirm();
    }
  };
});

const t = (value: string) => value;

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

const renderComponent = (props: any) => {
  return render(
    <div>
      <ConfigureParameters {...props} />
    </div>
  );
};

describe('WorkflowManageMethodLevelProcessingDecisionTablesMLP > ConfigureParameters > index', () => {
  const mockDispatch = jest.fn();

  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Should render Table List ', () => {
    jest.spyOn(hooks, 'useConfigAction').mockImplementation(() => {
      return {
        tableList: [1, 2],
        expandedList: {
          [EXPANDED_LIST.tableListModal]: {},
          [EXPANDED_LIST.tableListIndex]: {}
        },
        handleOpenConfigModal: jest.fn(),
        handleSetIsCreateNewVersion: jest.fn(),
        reduxDispatch: jest.fn(),
        handleSetDraftNewTable: jest.fn(),
        handleSetTable: jest.fn(),
        handleChangeRowIdAdded: jest.fn()
      } as any;
    });
    const wrapper = renderComponent({
      formValues: {},
      setFormValues: jest.fn(),
      type: 'CA',
      isStuck: true,
      dependencies: { files: [{ status: 0, idx: '1234' }] },
      clearFormValues: jest.fn(),
      clearFormName: 'abc'
    });
    expect(wrapper.getByText('TableList_component')).toBeInTheDocument();
    const btnCreateNewTable = wrapper.getByText('Add_New_Table');
    const btnTableToModel = wrapper.getByText('Table_To_Model');
    const btnCreateNewVersion = wrapper.getByText('Create_New_version');
    userEvent.click(btnCreateNewTable);
    userEvent.click(btnTableToModel);
    userEvent.click(btnCreateNewVersion);
  });

  it('Should render Table List Modal', () => {
    jest.spyOn(hooks, 'useConfigAction').mockImplementation(() => {
      return {
        tableList: [],
        expandedList: {
          [EXPANDED_LIST.tableListModal]: {},
          [EXPANDED_LIST.tableListIndex]: {}
        },
        openedModal: CONFIG_ACTIONS_MODAL.tableListModal,
        handleOpenConfigModal: jest.fn(),
        handleSetDraftNewTable: jest.fn(),
        handleSetTable: jest.fn(),
        handleChangeRowIdAdded: jest.fn()
      } as any;
    });
    const wrapper = renderComponent({
      formValues: {},
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    });
    expect(wrapper.getByText('TableListModal_component')).toBeInTheDocument();
    const closeBtn = wrapper.getByText('Close');
    userEvent.click(closeBtn);
  });

  it('Should render Table List Modal with draftNewTable', () => {
    jest.spyOn(hooks, 'useConfigAction').mockImplementation(() => {
      return {
        tableList: [],
        expandedList: {
          [EXPANDED_LIST.tableListModal]: {},
          [EXPANDED_LIST.tableListIndex]: {}
        },
        openedModal: CONFIG_ACTIONS_MODAL.tableListModal,
        draftNewTable: [{ id: '123' }],
        handleOpenConfigModal: jest.fn(),
        handleSetDraftNewTable: jest.fn(),
        handleSetTable: jest.fn(),
        handleChangeRowIdAdded: jest.fn()
      } as any;
    });
    const wrapper = renderComponent({
      formValues: {},
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    });
    expect(wrapper.getByText('TableListModal_component')).toBeInTheDocument();
    const closeBtn = wrapper.getByText('Close');
    userEvent.click(closeBtn);
  });

  it('Should render Add New Table Modal', () => {
    jest.spyOn(hooks, 'useConfigAction').mockImplementation(() => {
      return {
        tableList: [1, 2],
        expandedList: {
          [EXPANDED_LIST.tableListModal]: {},
          [EXPANDED_LIST.tableListIndex]: {}
        },
        openedModal: CONFIG_ACTIONS_MODAL.addNewTableModal,
        handleOpenConfigModal: jest.fn(),
        handleSetDraftNewTable: jest.fn(),
        handleSetTable: jest.fn(),
        handleChangeRowIdAdded: jest.fn()
      } as any;
    });
    const wrapper = renderComponent({
      formValues: {
        tables: [
          {
            tableId: '123',
            elementList: [{ data: '123', rowId: '123' }],
            tableControlParameters: [
              TableFieldParameterEnum.ChangeInTermsMethodFlag
            ]
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn(),
      clearFormValues: jest.fn(),
      handleChangeRowIdAdded: jest.fn()
    });
    expect(wrapper.getByText('AddNewTableModal_component')).toBeInTheDocument();
    const closeBtn = wrapper.getByText('Close');
    userEvent.click(closeBtn);
  });

  it('Should render Add New Table Modal with type CA', () => {
    jest.spyOn(hooks, 'useConfigAction').mockImplementation(() => {
      return {
        tableList: [1, 2],
        expandedList: {
          [EXPANDED_LIST.tableListModal]: {},
          [EXPANDED_LIST.tableListIndex]: {}
        },
        openedModal: CONFIG_ACTIONS_MODAL.addNewTableModal,
        handleOpenConfigModal: jest.fn(),
        handleSetDraftNewTable: jest.fn(),
        handleSetTable: jest.fn(),
        handleChangeRowIdAdded: jest.fn()
      } as any;
    });
    const wrapper = renderComponent({
      formValues: {
        tables: [
          {
            tableId: '123',
            elementList: [{ data: '123', rowId: '123' }],
            tableControlParameters: [
              TableFieldParameterEnum.ChangeInTermsMethodFlag
            ]
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn(),
      clearFormValues: jest.fn(),
      type: 'CA'
    });
    expect(wrapper.getByText('AddNewTableModal_component')).toBeInTheDocument();
    const closeBtn = wrapper.getByText('Close');
    userEvent.click(closeBtn);
  });

  it('Should render Remove Table Modal', () => {
    jest.spyOn(hooks, 'useConfigAction').mockImplementation(() => {
      return {
        tableList: [],
        expandedList: {
          [EXPANDED_LIST.tableListModal]: {},
          [EXPANDED_LIST.tableListIndex]: {}
        },
        openedModal: CONFIG_ACTIONS_MODAL.deleteTableModal,
        selectedTable: {},
        handleDeleteTable: jest.fn(),
        handleOpenConfigModal: jest.fn(),
        handleSetSelectedTable: jest.fn(),
        handleSetDraftNewTable: jest.fn(),
        handleSetTable: jest.fn(),
        handleChangeRowIdAdded: jest.fn()
      } as any;
    });
    const wrapper = renderComponent({
      formValues: {
        tables: [
          {
            tableId: '123',
            elementList: [{ data: '123', rowId: '123' }],
            tableControlParameters: [
              TableFieldParameterEnum.ChangeInTermsMethodFlag
            ]
          }
        ]
      },
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    });
    expect(wrapper.getByText('RemoveTableModal_component')).toBeInTheDocument();
    const closeBtn = wrapper.getByText('Close');
    const okBtn = wrapper.getByText('Ok');
    userEvent.click(closeBtn);
    userEvent.click(okBtn);
  });
});
