import {
  MethodFieldParameterEnum,
  TableFieldParameterEnum
} from 'app/constants/enums';
import get from 'lodash.get';
import {
  actionsWorkflowSetup,
  useSelectDecisionElementListData,
  useSelectElementMetadataManageMethodLevelProcessingMLP
} from 'pages/_commons/redux/WorkflowSetup';
import { useCallback, useEffect, useMemo, useReducer } from 'react';
import { useDispatch } from 'react-redux';

interface ConfigActionState {
  openedModal: null | CONFIG_ACTIONS_MODAL;
  expandedList: Record<EXPANDED_LIST, Record<string, any>>;
  selectedVersion?: any;
  selectedTable?: any;
  isCreateNewVersion?: boolean;
  draftNewTable?: Record<string, any> | null;
  tableList: any[];
  rowIdAdded?: string | undefined;
}

export enum CONFIG_ACTIONS_MODAL {
  addNewTableModal = 'addNewTableModal',
  tableListModal = 'tableListModal',
  deleteTableModal = 'deleteTableModal'
}

export enum EXPANDED_LIST {
  tableListModal = 'tableListModal',
  tableListIndex = 'tableListIndex'
}
export enum EXPAND_ACTION {
  toggle = 'toggle',
  replace = 'replace',
  edit = 'edit'
}

const ACTION_TYPE = {
  SET_OPENED_MODAL: 'SET_OPENED_MODAL',
  SET_EXPANDED_LIST: 'SET_EXPANDED_LIST',
  SET_SELECTED_VERSION: 'SET_SELECTED_VERSION',
  SET_SELECTED_TABLE: 'SET_SELECTED_TABLE',
  SET_IS_CREATE_NEW_VERSION: 'SET_IS_CREATE_NEW_VERSION',
  SET_DRAFT_NEW_TABLE: 'SET_DRAFT_NEW_TABLE',
  SET_TABLE_LIST: 'SET_TABLE_LIST',
  SET_EXPANDED_LIST_DEFAULT: 'SET_EXPANDED_LIST_DEFAULT',
  SET_ROW_ID_ADDED: 'SET_ROW_ID_ADDED'
};

export const INITIAL_STATE: ConfigActionState = {
  openedModal: null,
  expandedList: {
    [EXPANDED_LIST.tableListModal]: {},
    [EXPANDED_LIST.tableListIndex]: {}
  },
  draftNewTable: null,
  tableList: []
};

const configActionsReducer = (state: ConfigActionState, action: any) => {
  switch (action.type) {
    case ACTION_TYPE.SET_OPENED_MODAL:
      return {
        ...state,
        openedModal: action.modalName
      };
    case ACTION_TYPE.SET_EXPANDED_LIST:
      let newExpandEdList = {
        ...state.expandedList[action.expandedListName as EXPANDED_LIST]
      };
      if (action.expandAction === EXPAND_ACTION.toggle) {
        newExpandEdList[action.expandKey]
          ? delete newExpandEdList[action.expandKey]
          : (newExpandEdList[action.expandKey] = true);
      }
      if (action.expandAction === EXPAND_ACTION.replace) {
        newExpandEdList = action.expandedList || {};
      }

      if (action.expandAction === EXPAND_ACTION.edit) {
        newExpandEdList[action.expandKey] = true;
      }

      return {
        ...state,
        expandedList: {
          ...state.expandedList,
          [action.expandedListName]: newExpandEdList
        }
      };
    case ACTION_TYPE.SET_SELECTED_VERSION:
      return {
        ...state,
        selectedVersion: action.selectedVersion
      };
    case ACTION_TYPE.SET_SELECTED_TABLE:
      return {
        ...state,
        selectedTable: action.selectedTable
      };
    case ACTION_TYPE.SET_IS_CREATE_NEW_VERSION:
      return {
        ...state,
        isCreateNewVersion: action.isCreateNewVersion
      };
    case ACTION_TYPE.SET_DRAFT_NEW_TABLE:
      return {
        ...state,
        draftNewTable: action.draftNewTable
      };
    case ACTION_TYPE.SET_TABLE_LIST:
      return {
        ...state,
        tableList: action.tableList
      };
    case ACTION_TYPE.SET_EXPANDED_LIST_DEFAULT:
      return {
        ...state,
        expandedList: {
          ...state.expandedList,
          [EXPANDED_LIST.tableListIndex]: {}
        }
      };
    case ACTION_TYPE.SET_ROW_ID_ADDED:
      return {
        ...state,
        rowIdAdded: action.rowIdAdded
      };
    default:
      return state;
  }
};

export const useConfigAction = (selectedStep: string, type: string) => {
  const [state, dispatch] = useReducer(configActionsReducer, INITIAL_STATE);
  const {
    openedModal,
    expandedList,
    selectedVersion,
    selectedTable,
    isCreateNewVersion,
    draftNewTable,
    tableList,
    rowIdAdded
  } = state;

  const reduxDispatch = useDispatch();

  const handleGetDecisionElementListName = useMemo(() => {
    switch (type) {
      case 'CA':
        return 'MLPCA';
      default:
        return 'MLPAQ';
    }
  }, [type]);

  const data = useSelectDecisionElementListData(
    handleGetDecisionElementListName
  );
  const decisionElementList = useMemo(
    () => data?.decisionElementList || [],
    [data]
  );

  const metadata = useSelectElementMetadataManageMethodLevelProcessingMLP();

  const { searchCode } = metadata;

  const getSearchCodeText = (codeValue: string) => {
    const searchCodeOption = searchCode.find(code => code.code === codeValue);
    return searchCodeOption?.text || '';
  };

  useEffect(() => {
    if (!tableList.length || !decisionElementList?.length) return;

    const newTableList = tableList.map((item: Record<string, any>) => {
      const elementList = item.elementList
        .map((el: any) => {
          const matchElement =
            decisionElementList.find(
              (element: any) => element.name === el.name
            ) || {};
          return {
            ...matchElement,
            ...el,
            searchCodeText: getSearchCodeText(el?.searchCode)
          };
        })
        .filter((el: any) => el.configurable || el.isNewElement);

      return { ...item, elementList };
    });

    dispatch({
      type: ACTION_TYPE.SET_TABLE_LIST,
      tableList: newTableList
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [decisionElementList]);

  useEffect(() => {
    reduxDispatch(
      actionsWorkflowSetup.getWorkflowMetadata([
        MethodFieldParameterEnum.SearchCode,
        TableFieldParameterEnum.AllocationBeforeAfterCycle,
        TableFieldParameterEnum.CAAllocationFlag,
        TableFieldParameterEnum.AQAllocationFlag,
        TableFieldParameterEnum.AllocationInterval,
        TableFieldParameterEnum.ChangeInTermsMethodFlag,
        TableFieldParameterEnum.ServiceSubjectSectionAreas
      ])
    );
  }, [reduxDispatch]);

  const handleGetTableListName = useMemo(() => {
    switch (type) {
      case 'CA':
        return 'CA';
      default:
        return 'AQ';
    }
  }, [type]);

  useEffect(() => {
    reduxDispatch(
      actionsWorkflowSetup.getDecisionElementList({
        tableType: handleGetTableListName,
        serviceNameAbbreviation: 'MLP'
      })
    );
  }, [reduxDispatch, handleGetTableListName]);

  const handleChangeRowIdAdded = useCallback((rowId: string | undefined) => {
    dispatch({
      type: ACTION_TYPE.SET_ROW_ID_ADDED,
      rowIdAdded: rowId
    });
  }, []);

  const handleOpenConfigModal = useCallback(
    (modalName: CONFIG_ACTIONS_MODAL | null) =>
      dispatch({
        type: ACTION_TYPE.SET_OPENED_MODAL,
        modalName
      }),
    []
  );

  const handleCheckDuplicateTableId = useCallback(
    (tableId: string) => {
      const selectedTableId = get(selectedVersion, 'tableId', '');
      const versionTableId = get(selectedVersion, ['version', 'tableId'], '');
      const tableListIds = tableList.map((table: any) => table?.tableId);
      const arrayCheck = selectedVersion
        ? [...tableListIds, selectedTableId, versionTableId]
        : tableListIds;
      return arrayCheck.some(
        (existedTableId: string) => tableId === existedTableId
      );
    },
    [selectedVersion, tableList]
  );

  const handleSetExpandedList = useCallback(
    (expandedListName: EXPANDED_LIST) =>
      ({ expandAction, expandKey, expandedList }: Record<string, any>) => {
        dispatch({
          type: ACTION_TYPE.SET_EXPANDED_LIST,
          expandedList,
          expandedListName,
          expandKey,
          expandAction
        });
      },
    []
  );

  const handleSetSelectedVersion = useCallback((selectedVersion: any) => {
    dispatch({
      type: ACTION_TYPE.SET_SELECTED_VERSION,
      selectedVersion
    });
  }, []);

  const handleSetIsCreateNewVersion = useCallback(
    (isCreateNewVersion: boolean) => {
      dispatch({
        type: ACTION_TYPE.SET_IS_CREATE_NEW_VERSION,
        isCreateNewVersion
      });
    },
    []
  );

  const handleSetDraftNewTable = useCallback(
    (draftNewTable: Record<string, any> | null) => {
      dispatch({
        type: ACTION_TYPE.SET_DRAFT_NEW_TABLE,
        draftNewTable
      });
    },
    []
  );

  const handleAddNewTable = useCallback(
    (table: any) => {
      dispatch({
        type: ACTION_TYPE.SET_TABLE_LIST,
        tableList: [...tableList, table]
      });
      handleChangeRowIdAdded(table.rowId);
    },
    [tableList, handleChangeRowIdAdded]
  );

  const handleSetSelectedTable = useCallback((selectedTable: any) => {
    if (Array.isArray(selectedTable?.tableControlParameters)) {
      selectedTable.tableControlParameters =
        selectedTable.tableControlParameters.reduce(
          (obj: Record<string, any>, el: any) => {
            obj[el.name] = el.value;
            return obj;
          },
          {}
        );
    }

    dispatch({
      type: ACTION_TYPE.SET_SELECTED_TABLE,
      selectedTable
    });
  }, []);

  const handleEditTable = useCallback(
    (newTable: any) => {
      const tableIndex = tableList.findIndex(
        (table: any) => table.rowId === newTable.rowId
      );
      const newTableList = [...tableList];
      newTableList[tableIndex] = newTable;
      dispatch({
        type: ACTION_TYPE.SET_TABLE_LIST,
        tableList: newTableList
      });
    },
    [tableList]
  );

  const handleDeleteTable = useCallback(
    (rowId: string) => {
      const newTableList = tableList.filter(
        (table: any) => table.rowId !== rowId
      );
      dispatch({
        type: ACTION_TYPE.SET_TABLE_LIST,
        tableList: newTableList
      });
      dispatch({
        type: ACTION_TYPE.SET_SELECTED_TABLE,
        selectedTable: undefined
      });
    },
    [tableList]
  );

  useEffect(() => {
    dispatch({
      type: ACTION_TYPE.SET_EXPANDED_LIST_DEFAULT
    });
  }, [selectedStep]);

  const handleSetTable = useCallback((table: any) => {
    dispatch({
      type: ACTION_TYPE.SET_TABLE_LIST,
      tableList: table
    });
  }, []);

  return {
    handleOpenConfigModal,
    openedModal,
    selectedTable,
    expandedList,
    selectedVersion,
    handleSetExpandedList,
    handleSetSelectedVersion,
    handleSetIsCreateNewVersion,
    isCreateNewVersion,
    handleSetDraftNewTable,
    draftNewTable,
    handleAddNewTable,
    tableList,
    handleSetSelectedTable,
    handleEditTable,
    handleDeleteTable,
    handleCheckDuplicateTableId,
    dispatch,
    reduxDispatch,
    handleSetTable,
    handleChangeRowIdAdded,
    rowIdAdded
  };
};
