import { act, renderHook } from '@testing-library/react-hooks';
import { useAddNewTable } from './useAddNewTable';

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

describe('WorkflowManageTransactionLevelProcessingDecisionTablesTLP > ConfigureParametersTQ > useAddNewTable', () => {
  beforeEach(() => {});

  it('Run Default reducer function', () => {
    const { result } = renderHook(() => useAddNewTable({}, false, null, 'QA'));
    act(() => {
      result.current.dispatch({ type: 'Test Action' });
    });
  });

  it('Run with not null selectable', () => {
    const { result } = renderHook(() =>
      useAddNewTable({}, false, { id: '123' }, 'QA')
    );
    act(() => {
      result.current.dispatch({ type: 'Test Action' });
    });
  });

  it('isFormChanging', () => {
    const { result } = renderHook(() =>
      useAddNewTable(
        {
          id: '123',
          tableId: '1',
          comment: 'comment',
          tableName: 'tableName',
          tableControlParameters: 'params',
          isFormChanging: true,
          elementList: [
            {
              tableId: '1',
              comment: 'comment',
              tableName: 'tableName',
              tableControlParameters: 'params',
              immediateAllocation: 'NO'
            },
            {
              tableId: '2',
              comment: 'comment',
              tableName: 'tableName',
              tableControlParameters: {},
              immediateAllocation: 'Yes'
            },
            {
              tableId: '2',
              comment: 'comment',
              tableName: 'tableName',
              tableControlParameters: 'params',
              immediateAllocation: 'Yes'
            }
          ],
          initialValue: {
            elementList: [
              {
                tableId: '1',
                comment: 'comment',
                tableName: 'tableName',
                tableControlParameters: 'params',
                immediateAllocation: 'NO'
              },
              {
                tableId: '2',
                comment: 'comment',
                tableName: 'tableName',
                tableControlParameters: 'params',
                immediateAllocation: 'Yes'
              },
              {
                tableId: '2',
                comment: 'comment',
                tableName: 'tableName',
                tableControlParameters: 'params',
                immediateAllocation: 'Yes'
              }
            ],
            tableControlParameters: {}
          }
        },
        false,
        {},
        'CA'
      )
    );
    act(() => {
      result.current.handleSetElementList(
        [
          {
            tableId: '1',
            comment: 'comment',
            tableName: 'tableName',
            tableControlParameters: 'params'
          },
          {
            tableId: '2',
            comment: 'comment',
            tableName: 'tableName',
            tableControlParameters: 'params'
          },
          {
            tableId: '3',
            comment: 'comment',
            tableName: 'tableName',
            tableControlParameters: 'params'
          }
        ],
        false
      );
    });
  });

  it('handleChangeFieldValue', () => {
    const { result } = renderHook(() =>
      useAddNewTable({}, false, { id: '123' }, 'CA')
    );
    act(() => {
      result.current.dispatch({ type: 'Test Action' });
      result.current.handleChangeFieldValue('tableId')({
        target: {
          value: '123'
        }
      });

      result.current.handleChangeFieldValue('tableId')({
        target: {
          value: '####'
        }
      });

      result.current.handleChangeFieldValue('tableName')({
        target: {
          value: '123'
        }
      });
      result.current.handleSetElementList(['123'], true);
      result.current.handleSetElementList(
        [
          {
            tableId: '1',
            comment: 'comment',
            tableName: 'tableName',
            tableControlParameters: 'params'
          },
          {
            tableId: '2',
            comment: 'comment',
            tableName: 'tableName',
            tableControlParameters: 'params'
          },
          {
            tableId: '3',
            comment: 'comment',
            tableName: 'tableName',
            tableControlParameters: 'params'
          }
        ],
        false
      );
      result.current.handleChangeTableControlParameterAllField(['123']);
      result.current.handleChangeTableControlParameter('name', 'value');
    });
  });

  it('Run hook with draftNew State', () => {
    const { result } = renderHook(() =>
      useAddNewTable(
        {
          tableId: 'test'
        },
        false,
        null,
        'QA'
      )
    );
    act(() => {
      result.current.handleChangeFieldValue('tableId')({
        target: {
          value: 'test'
        }
      });
      result.current.handleChangeFieldValue('tableId')({
        target: {
          value: '###'
        }
      });
      result.current.handleChangeFieldValue('comment')({
        target: {
          value: 'abc'
        }
      });
      result.current.handleBlurField('tableId')();
    });

    act(() => {
      result.current.handleSetElementList([1, 2]);
      result.current.handleSetElementListHasError(true);
      result.current.handleSetIsDuplicateTableId(true);
    });
    expect(result.current.elementList).toEqual([1, 2]);
    expect(result.current.isElementListHasError).toEqual(true);
    expect(result.current.isDuplicateTableId).toEqual(true);
  });
});
