import EnhanceDropdownList from 'app/components/EnhanceDropdownList';
import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import { TableFieldParameterEnum } from 'app/constants/enums';
import { matchSearchValue } from 'app/helpers';
import {
  ColumnType,
  DatePicker,
  Grid,
  TextBox,
  useTranslation
} from 'app/_libraries/_dls';
import MultiSelect from 'app/_libraries/_dls/components/MultiSelect';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import { useSelectElementMetadataManageMethodLevelProcessingMLP } from 'pages/_commons/redux/WorkflowSetup';
import ClearAndResetButton from 'pages/_commons/Utils/ClearAndResetButton';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React, { useEffect, useMemo, useRef } from 'react';
import { TableFieldParameter } from './TableParameterList';
import useGridTable from './useGridTable';

interface IProps {
  data: any;
  handleChangeControlParameters: (name: string, val?: any) => void;
  type: string;
}

const GridTable: React.FC<IProps> = ({
  data: dataProp,
  handleChangeControlParameters,
  type
}) => {
  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();
  const metadata = useSelectElementMetadataManageMethodLevelProcessingMLP();
  const { searchValue, handleSearchValue } = useGridTable();
  const {
    caAllocationFlag,
    aqAllocationFlag,
    allocationBeforeAfterCycle,
    allocationInterval,
    changeInTermsMethodFlag,
    serviceSubjectSectionAreas
  } = metadata;

  const simpleSearchRef = useRef<any>(null);

  useEffect(() => {
    if (!searchValue && simpleSearchRef?.current) {
      simpleSearchRef.current.clear();
    }
  }, [searchValue]);

  const dataTable = useMemo(() => {
    return TableFieldParameter(type).filter(i =>
      matchSearchValue(
        [i.businessName, i.greenScreenName, i.moreInfoText],
        searchValue
      )
    );
  }, [searchValue, type]);

  const valueAccessor = (data: Record<string, any>) => {
    const paramId = data.id;

    switch (paramId) {
      case TableFieldParameterEnum.AllocationDate: {
        const value = dataProp[TableFieldParameterEnum.AllocationDate];

        return (
          <DatePicker
            placeholder={t('txt_date_format')}
            required
            size="small"
            value={value && new Date(value)}
            onChange={e =>
              handleChangeControlParameters(
                TableFieldParameterEnum.AllocationDate,
                e.target.value
              )
            }
          />
        );
      }
      case TableFieldParameterEnum.AQAllocationFlag: {
        const value = aqAllocationFlag?.find(
          o => o.code === dataProp[TableFieldParameterEnum.AQAllocationFlag]
        );

        return (
          <EnhanceDropdownList
            size="small"
            value={value}
            placeholder={t('txt_select_an_option')}
            onChange={e =>
              handleChangeControlParameters(
                TableFieldParameterEnum.AQAllocationFlag,
                e.target.value.code
              )
            }
            options={aqAllocationFlag}
          />
        );
      }
      case TableFieldParameterEnum.CAAllocationFlag: {
        const value = caAllocationFlag?.find(
          o => o.code === dataProp[TableFieldParameterEnum.CAAllocationFlag]
        );

        return (
          <EnhanceDropdownList
            size="small"
            value={value}
            placeholder={t('txt_select_an_option')}
            onChange={e =>
              handleChangeControlParameters(
                TableFieldParameterEnum.CAAllocationFlag,
                e.target.value.code
              )
            }
            options={caAllocationFlag}
          />
        );
      }
      case TableFieldParameterEnum.AllocationBeforeAfterCycle: {
        const value = allocationBeforeAfterCycle?.find(
          o =>
            o.code ===
            dataProp[TableFieldParameterEnum.AllocationBeforeAfterCycle]
        );

        return (
          <EnhanceDropdownList
            size="small"
            value={value}
            placeholder={t('txt_select_an_option')}
            onChange={e =>
              handleChangeControlParameters(
                TableFieldParameterEnum.AllocationBeforeAfterCycle,
                e.target.value.code
              )
            }
            options={allocationBeforeAfterCycle}
          />
        );
      }
      case TableFieldParameterEnum.AllocationInterval: {
        const value = allocationInterval?.find(
          o => o.code === dataProp[TableFieldParameterEnum.AllocationInterval]
        );

        return (
          <EnhanceDropdownList
            size="small"
            value={value}
            placeholder={t('txt_select_an_option')}
            onChange={e =>
              handleChangeControlParameters(
                TableFieldParameterEnum.AllocationInterval,
                e.target.value.code
              )
            }
            options={allocationInterval}
          />
        );
      }
      case TableFieldParameterEnum.ChangeCode: {
        return (
          <TextBox
            id="changeCodeMLP"
            value={dataProp[TableFieldParameterEnum.ChangeCode]}
            maxLength={2}
            size="sm"
            placeholder={t('txt_enter_a_value')}
            onChange={e =>
              handleChangeControlParameters(
                TableFieldParameterEnum.ChangeCode,
                e.target.value
              )
            }
          />
        );
      }
      case TableFieldParameterEnum.ChangeInTermsMethodFlag: {
        const value = changeInTermsMethodFlag?.find(
          o =>
            o.code === dataProp[TableFieldParameterEnum.ChangeInTermsMethodFlag]
        );

        return (
          <EnhanceDropdownList
            size="small"
            value={value}
            placeholder={t('txt_select_an_option')}
            onChange={e =>
              handleChangeControlParameters(
                TableFieldParameterEnum.ChangeInTermsMethodFlag,
                e.target.value.code
              )
            }
            options={changeInTermsMethodFlag}
          />
        );
      }
      case TableFieldParameterEnum.ServiceSubjectSectionAreas: {
        const value = serviceSubjectSectionAreas?.filter(i =>
          dataProp[TableFieldParameterEnum.ServiceSubjectSectionAreas]
            ?.split(/; |;/)
            .some((a: string) => a === i.code)
        );

        return (
          <MultiSelect
            value={value}
            onChange={e => {
              const valueChange = e.target.value
                .map((i: any) => i.code)
                .join('; ');
              handleChangeControlParameters(
                TableFieldParameterEnum.ServiceSubjectSectionAreas,
                valueChange
              );
            }}
            textField="text"
            placeholder={t('txt_select_options')}
          >
            {serviceSubjectSectionAreas.map(option => (
              <MultiSelect.Item
                key={option.code}
                label={option.text}
                value={option}
                variant="checkbox"
              />
            ))}
          </MultiSelect>
        );
      }
    }
  };

  const columns: ColumnType[] = [
    {
      id: 'businessName',
      Header: t('txt_manage_account_level_table_decision_business_name'),
      accessor: data => data.businessName,
      cellBodyProps: { className: 'pt-12 pb-8' },
      width: 233
    },
    {
      id: 'greenScreenName',
      Header: t('txt_manage_account_level_table_decision_green_name'),
      accessor: data => data.greenScreenName,
      cellBodyProps: { className: 'pt-12 pb-8' },
      width: 244
    },
    {
      id: 'value',
      Header: t('txt_manage_account_level_table_decision_value'),
      width: width < 1280 ? 320 : 360,
      cellBodyProps: { className: 'py-8' },
      accessor: valueAccessor
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      width: 106,
      cellBodyProps: { className: 'pt-12 pb-8' },
      accessor: viewMoreInfo(['moreInfo'], t)
    }
  ];

  return (
    <>
      <div className="d-flex align-items-center justify-content-between mb-16 mt-24">
        <h5>{t('txt_manage_account_level_table_parameters')}</h5>
        <SimpleSearch
          ref={simpleSearchRef}
          placeholder={t('txt_type_to_search')}
          onSearch={handleSearchValue}
          defaultValue={searchValue}
          popperElement={
            <>
              <p className="color-grey">{t('txt_search_description')}</p>
              <ul className="search-field-item list-unstyled">
                <li className="mt-16">{t('txt_business_name')}</li>
                <li className="mt-16">{t('txt_green_screen_name')}</li>
                <li className="mt-16">{t('txt_more_info')}</li>
              </ul>
            </>
          }
        />
      </div>
      {searchValue && !isEmpty(dataTable) && (
        <div className="d-flex justify-content-end mb-16 mr-n8">
          <ClearAndResetButton
            small
            onClearAndReset={() => handleSearchValue('')}
          />
        </div>
      )}
      {isEmpty(dataTable) ? (
        <div className="d-flex flex-column justify-content-center mt-40">
          <NoDataFound
            id="method-list-NoDataFound"
            title={t('No methods to display')}
            hasSearch={!!searchValue}
            linkTitle={!!searchValue && t('txt_clear_and_reset')}
            onLinkClicked={() => handleSearchValue('')}
          />
        </div>
      ) : (
        <Grid columns={columns} data={dataTable} />
      )}
    </>
  );
};

export default GridTable;
