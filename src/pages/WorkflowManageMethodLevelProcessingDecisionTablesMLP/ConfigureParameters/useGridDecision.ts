import { matchSearchValue } from 'app/helpers';
import { useTranslation } from 'app/_libraries/_dls';
import get from 'lodash.get';
import isEmpty from 'lodash.isempty';
import isEqual from 'lodash.isequal';
import _orderBy from 'lodash.orderby';
import set from 'lodash.set';
import { nanoid } from 'nanoid';
import {
  useSelectDecisionElementListData,
  useSelectElementMetadataManageMethodLevelProcessingMLP
} from 'pages/_commons/redux/WorkflowSetup';
import { useCallback, useEffect, useMemo, useReducer } from 'react';

export enum FIELD_TYPE {
  dropDown = 'dropDown',
  checkBox = 'checkBox'
}

interface ElementListState {
  errors: Record<string, Record<string, string>>;
  touches: Record<string, Record<string, boolean>>;
  searchValue: string;
}

const INITIAL_STATE: ElementListState = {
  errors: {},
  touches: {},
  searchValue: ''
};

const ACTION_TYPES = {
  CHANGE_SEARCH_VALUE: 'CHANGE_SEARCH_VALUE',
  SET_TOUCHES_FIELD: 'SET_TOUCHES_FIELD',
  SET_ERRORS: 'SET_ERRORS',
  RESET_TOUCHES_FIELD: 'RESET_TOUCHES_FIELD'
};

const elementListReducer = (state: ElementListState, action: any) => {
  switch (action.type) {
    case ACTION_TYPES.CHANGE_SEARCH_VALUE:
      return { ...state, searchValue: action.searchValue };
    case ACTION_TYPES.SET_TOUCHES_FIELD:
      const newStateTouch = { ...state, touches: { ...state.touches } };
      set(newStateTouch, ['touches', action.rowId, action.fieldName], true);
      return { ...newStateTouch };
    case ACTION_TYPES.SET_ERRORS:
      return { ...state, errors: action.errors };
    case ACTION_TYPES.RESET_TOUCHES_FIELD:
      const newStateTouchReset = { ...state, touches: { ...state.touches } };
      set(
        newStateTouchReset,
        ['touches', action.rowId, action.fieldName],
        false
      );
      return { ...newStateTouchReset };
    default:
      return state;
  }
};

export const useGridDecision = (
  elementList: any[],
  handleSetElementList: (
    elementList: any[],
    isChangeElementList?: boolean
  ) => void,
  handleSetElementListHasError: (isElementListHasError: boolean) => void,
  tableType: string
) => {
  const { t } = useTranslation();
  const [state, reactDispatch] = useReducer(elementListReducer, INITIAL_STATE);
  const { searchValue, errors, touches } = state;

  const data = useSelectDecisionElementListData(tableType);
  const decisionElementList = useMemo(
    () => data?.decisionElementList || [],
    [data]
  );

  const metadata = useSelectElementMetadataManageMethodLevelProcessingMLP();

  const { searchCode } = metadata;

  const handleChangeImmediate = useCallback(
    (rowId: string) => {
      const rowIndex = elementList.findIndex(row => row.rowId === rowId);
      if (rowIndex === -1) return;
      const newElementList = [...elementList];
      newElementList[rowIndex] = {
        ...newElementList[rowIndex],
        immediateAllocation:
          newElementList[rowIndex].immediateAllocation === 'Yes' ? 'No' : 'Yes'
      };
      handleSetElementList(newElementList, true);
    },
    [elementList, handleSetElementList]
  );

  const elementListNameOptions = useMemo(() => {
    return _orderBy(decisionElementList, 'name', 'asc')
      .map((el: any) => ({
        code: el.name,
        text: el.name,
        ...el
      }))
      .filter((el: any) => el.configurable);
  }, [decisionElementList]);

  const handleChangeSearchValue = (searchValue: string) => {
    reactDispatch({ type: ACTION_TYPES.CHANGE_SEARCH_VALUE, searchValue });
  };

  const handleErrorElement = useCallback(
    newElementList => {
      const newErrors = {};
      if (newElementList.length === 0) return;
      newElementList.forEach((el: any) => {
        if (!el.name) {
          set(
            newErrors,
            [el.rowId, 'name'],
            t(
              'txt_manage_transaction_level_tlp_config_tq_require_decision_element'
            )
          );
        }
        if (!el.searchCode) {
          set(
            newErrors,
            [el.rowId, 'searchCode'],
            t('txt_manage_transaction_level_tlp_config_tq_require_search_code')
          );
        }
      });

      !isEmpty(newErrors)
        ? handleSetElementListHasError(true)
        : handleSetElementListHasError(false);

      if (isEqual(errors, newErrors)) return;
      reactDispatch({
        type: ACTION_TYPES.SET_ERRORS,
        errors: newErrors
      });
    },
    [t, handleSetElementListHasError, errors]
  );

  useEffect(() => {
    handleErrorElement(elementList);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [elementList]);

  const handleChangeElementValue = useCallback(
    (fieldName: string, rowId: string) => (e: any) => {
      const value = e?.target?.value;
      const rowIndex = elementList.findIndex(row => row.rowId === rowId);

      if (rowIndex === -1) return;
      const newElementList = [...elementList];
      newElementList[rowIndex] = {
        ...elementList[rowIndex],
        [fieldName]: value?.code || '',
        searchCodeText:
          fieldName === 'searchCode'
            ? value?.text
            : elementList[rowIndex]?.searchCodeText
      };
      if (fieldName === 'name' && elementList[rowIndex].name !== value?.code) {
        newElementList[rowIndex] = {
          ...newElementList[rowIndex],
          moreInfo: value?.moreInfo,
          configurable: value?.configurable,
          searchCode: '',
          searchCodeText: '',
          immediateAllocation: value?.immediateAllocation,
          isNewElement: !!!value?.code
        };

        if (get(touches, [rowId, 'searchCode'])) {
          reactDispatch({
            type: ACTION_TYPES.RESET_TOUCHES_FIELD,
            fieldName: 'searchCode',
            rowId
          });
        }
      }
      handleSetElementList(newElementList, true);
    },
    [elementList, handleSetElementList, touches]
  );

  const handleDeleteElement = useCallback(
    rowId => () => {
      const newElementList = elementList.filter(el => el.rowId !== rowId);
      if (!newElementList.length) {
        newElementList.push({ name: '', isNewElement: true, rowId: nanoid() });
      }
      handleSetElementList(newElementList, true);
    },
    [handleSetElementList, elementList]
  );

  const handleSetTouchField = useCallback(
    (fieldName: string, rowId: string) => (e: any) => {
      const rowIndex = elementList.findIndex(row => row.rowId === rowId);
      if (
        fieldName === 'name' &&
        !elementList[rowIndex].name &&
        rowIndex > -1
      ) {
        const newElementList = [...elementList];
        newElementList[rowIndex] = {
          name: '',
          isNewElement: true,
          rowId: newElementList[rowIndex].rowId
        };
        handleSetElementList(newElementList);
        if (get(touches, [rowId, 'searchCode'])) {
          reactDispatch({
            type: ACTION_TYPES.RESET_TOUCHES_FIELD,
            fieldName: 'searchCode',
            rowId
          });
        }
      }
      handleErrorElement(elementList);
      if (get(touches, [rowId, fieldName])) return;
      reactDispatch({
        type: ACTION_TYPES.SET_TOUCHES_FIELD,
        fieldName,
        rowId
      });
    },
    [touches, elementList, handleErrorElement, handleSetElementList]
  );

  const handleAddNewElement = useCallback(() => {
    const newElement = { name: '', isNewElement: true, rowId: nanoid() };
    const newElementList = [...elementList, newElement];
    handleSetElementList(newElementList, true);
    handleChangeSearchValue('');
  }, [handleSetElementList, elementList]);

  const _searchValue = searchValue ? searchValue.toLowerCase() : '';

  const _elementListSearched = elementList.filter((w: any) => {
    if (!_searchValue && !w.name) return true;
    return matchSearchValue([w?.name, w?.moreInfo], _searchValue);
  });

  const totalElement = elementList.length;

  const handleDnDDataChange = (newElementList: any) => {
    if (!searchValue) {
      handleSetElementList(newElementList, true);
      return;
    }
  };

  const tooltipDragList = useMemo(() => {
    if (!searchValue) return [];
    return elementList?.map(item => ({
      id: item?.rowId,
      tooltipProps: {
        element: t(
          'txt_manage_account_level_table_decision_tooltip_disabled_dnd'
        )
      }
    }));
  }, [searchValue, elementList, t]);

  return {
    searchValue,
    handleChangeSearchValue,
    handleChangeElementValue,
    elements: _elementListSearched,
    handleDeleteElement,
    handleSetTouchField,
    errors,
    touches,
    elementListNameOptions,
    handleAddNewElement,
    totalElement,
    handleDnDDataChange,
    searchCodeOptions: searchCode,
    dispatch: reactDispatch,
    handleChangeImmediate,
    tooltipDragList
  };
};
