import { TableFieldParameterEnum } from 'app/constants/enums';
import { formatTime, isValidDate } from 'app/helpers';
import {
  ColumnType,
  Grid,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import { useSelectElementMetadataManageMethodLevelProcessingMLP } from 'pages/_commons/redux/WorkflowSetup';
import {
  formatText,
  formatTruncate,
  viewMoreInfo
} from 'pages/_commons/Utils/formatGridField';
import React, { useCallback, useMemo } from 'react';
import { TableFieldParameter } from './TableParameterList';

interface ITableParameterInfo {
  tableControlParameters: Record<string, any>;
  type?: string;
}

const TableParameterInfo: React.FC<ITableParameterInfo> = ({
  tableControlParameters,
  type
}) => {
  const { width } = useSelectWindowDimension();
  const { t } = useTranslation();
  const metadata = useSelectElementMetadataManageMethodLevelProcessingMLP();
  const {
    caAllocationFlag,
    aqAllocationFlag,
    allocationBeforeAfterCycle,
    allocationInterval,
    changeInTermsMethodFlag
  } = metadata;

  const values = useMemo<Record<string, any | undefined>>(() => {
    return {
      [TableFieldParameterEnum.AllocationDate]:
        tableControlParameters[TableFieldParameterEnum.AllocationDate],
      [TableFieldParameterEnum.CAAllocationFlag]: caAllocationFlag.find(
        o =>
          o.code ===
          tableControlParameters[TableFieldParameterEnum.CAAllocationFlag]
      )?.text,
      [TableFieldParameterEnum.AQAllocationFlag]: aqAllocationFlag.find(
        o =>
          o.code ===
          tableControlParameters[TableFieldParameterEnum.AQAllocationFlag]
      )?.text,
      [TableFieldParameterEnum.AllocationBeforeAfterCycle]:
        allocationBeforeAfterCycle.find(
          o =>
            o.code ===
            tableControlParameters[
              TableFieldParameterEnum.AllocationBeforeAfterCycle
            ]
        )?.text,
      [TableFieldParameterEnum.AllocationInterval]: allocationInterval.find(
        o =>
          o.code ===
          tableControlParameters[TableFieldParameterEnum.AllocationInterval]
      )?.text,
      [TableFieldParameterEnum.ChangeCode]:
        tableControlParameters[TableFieldParameterEnum.ChangeCode],
      [TableFieldParameterEnum.ChangeInTermsMethodFlag]:
        changeInTermsMethodFlag.find(
          o =>
            o.code ===
            tableControlParameters[
              TableFieldParameterEnum.ChangeInTermsMethodFlag
            ]
        )?.text,
      [TableFieldParameterEnum.ServiceSubjectSectionAreas]:
        tableControlParameters[
          TableFieldParameterEnum.ServiceSubjectSectionAreas
        ]
          ?.split(/; |;/)
          .join(', ')
    };
  }, [
    allocationBeforeAfterCycle,
    allocationInterval,
    aqAllocationFlag,
    caAllocationFlag,
    changeInTermsMethodFlag,
    tableControlParameters
  ]);

  const valueAccessor = useCallback(
    data => {
      const valueTruncate = () => {
        if (
          isValidDate(values[data.id]) &&
          data.id === TableFieldParameterEnum.AllocationDate
        ) {
          return formatTime(values[data.id]).date;
        }

        return values[data.id]?.toString().toLowerCase() !== 'none'
          ? values[data.id]
          : '';
      };

      return (
        <TruncateText
          resizable
          lines={2}
          ellipsisLessText={t('txt_less')}
          ellipsisMoreText={t('txt_more')}
        >
          {valueTruncate()}
        </TruncateText>
      );
    },
    [t, values]
  );

  const columnsTableControl: ColumnType[] = [
    {
      id: 'businessName',
      Header: t('txt_manage_account_level_table_decision_business_name'),
      accessor: formatTruncate(['businessName']),
      width: 233
    },
    {
      id: 'greenScreenName',
      Header: t('txt_manage_account_level_table_decision_green_name'),
      accessor: formatText(['greenScreenName']),
      width: 244
    },
    {
      id: 'value',
      Header: t('txt_manage_account_level_table_decision_value'),
      accessor: valueAccessor,
      width: width < 1280 ? 320 : 360
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      width: 106,
      cellBodyProps: { className: 'pt-12 pb-8' },
      accessor: viewMoreInfo(['moreInfo'], t)
    }
  ];

  return (
    <Grid columns={columnsTableControl} data={TableFieldParameter(type!)} />
  );
};

export default TableParameterInfo;
