import { ORDER_BY_LIST, PAGE_SIZE } from 'app/constants/constants';
import { OrderBy } from 'app/constants/enums';
import { matchSearchValue } from 'app/helpers';
import { DEFAULT_PAGE_NUMBER } from 'app/_libraries/_dls/components/Pagination/constants';
import { get } from 'app/_libraries/_dls/lodash';
import _orderBy from 'lodash.orderby';
import { nanoid } from 'nanoid';
import {
  actionsWorkflowSetup,
  useSelectDecisionElementListData,
  useSelectElementMetadataManageMethodLevelProcessingMLP,
  useSelectTableListData
} from 'pages/_commons/redux/WorkflowSetup';
import { useCallback, useEffect, useMemo, useReducer } from 'react';
import { useDispatch } from 'react-redux';
import { CONFIG_ACTIONS_MODAL } from '../ConfigureParametersST/useConfigActions';
import { EXPANDED_LIST, EXPAND_ACTION } from './useConfigActions';

interface ITableListStateProps {
  page: number;
  pageSize: number;
  searchValue: string;
  orderBy: OrderByType;
  versionDetail: any;
}

const TABLE_LIST_INITIAL_STATE = {
  page: DEFAULT_PAGE_NUMBER,
  pageSize: PAGE_SIZE[0],
  searchValue: '',
  orderBy: OrderBy.ASC,
  versionDetail: null
} as ITableListStateProps;

const ACTION_TYPES = {
  CHANGE_PAGE: 'CHANGE_PAGE',
  CHANGE_PAGE_SIZE: 'CHANGE_PAGE_SIZE',
  CHANGE_SEARCH_VALUE: 'CHANGE_SEARCH_VALUE',
  CHANGE_ORDER_BY: 'CHANGE_ORDER_BY',
  SET_SHOW_VERSION_DETAIL: 'SET_SHOW_VERSION_DETAIL'
};

const tableListReducer = (state: ITableListStateProps, action: any) => {
  switch (action.type) {
    case ACTION_TYPES.CHANGE_PAGE:
      return { ...state, page: action.page };
    case ACTION_TYPES.CHANGE_PAGE_SIZE:
      return { ...state, pageSize: action.pageSize };
    case ACTION_TYPES.CHANGE_SEARCH_VALUE:
      return {
        ...state,
        searchValue: action.searchValue,
        page: DEFAULT_PAGE_NUMBER
      };
    case ACTION_TYPES.CHANGE_ORDER_BY:
      return {
        ...state,
        orderBy: action.orderBy,
        page: DEFAULT_PAGE_NUMBER
      };
    case ACTION_TYPES.SET_SHOW_VERSION_DETAIL:
      return {
        ...state,
        versionDetail: action.versionDetail
      };
    default:
      return state;
  }
};

export const useTableListModal = (
  selectedVersion: any,
  handleSetExpandedList: (
    tableListName: EXPANDED_LIST
  ) => (expandItem: Record<string, any>) => void,
  type: string,
  handleOpenConfigModal: (modal: CONFIG_ACTIONS_MODAL | null) => void,
  handleSelectVersion: (version: any) => void,
  draftNewTable: any,
  handleSetDraftNewTable: (table: any) => void
) => {
  const dispatch = useDispatch();

  const handleGetTableListName = useMemo(() => {
    switch (type) {
      case 'CA':
        return 'MLPCA';
      default:
        return 'MLPAQ';
    }
  }, [type]);

  const data = useSelectDecisionElementListData(handleGetTableListName);
  const decisionElementList = data?.decisionElementList || [];

  const [state, reactDispatch] = useReducer(
    tableListReducer,
    TABLE_LIST_INITIAL_STATE
  );

  const { page, pageSize, searchValue, orderBy, versionDetail } = state;

  const { tableList = [], loading, error } = useSelectTableListData();

  const metadata = useSelectElementMetadataManageMethodLevelProcessingMLP();

  const { searchCode } = metadata;

  const getSearchCodeText = (codeValue: string) => {
    const searchCodeOption = searchCode.find(code => code.code === codeValue);
    return searchCodeOption?.text || '';
  };

  const handleGoToNextStep = (versionDetail?: any) => {
    handleOpenConfigModal(CONFIG_ACTIONS_MODAL.addNewTableModal);
    if (versionDetail) handleSelectVersion(versionDetail);
    const versionSelect = versionDetail || selectedVersion;
    if (
      !draftNewTable ||
      (draftNewTable &&
        draftNewTable.selectedVersionId !== versionSelect?.version?.tableId)
    ) {
      let elementListSelected = get(
        versionSelect,
        ['version', 'decisionElements'],
        []
      );

      elementListSelected = elementListSelected
        .map((el: any) => {
          const matchElement =
            decisionElementList.find(
              (element: any) => element.name === el.name
            ) || {};
          return {
            ...el,
            ...matchElement,
            searchCodeText: getSearchCodeText(el?.searchCode),
            rowId: nanoid()
          };
        })
        .filter((el: any) => el.configurable || el.isNewElement);

      let tableControlParameters = get(
        versionSelect,
        ['version', 'tableControlParameters'],
        []
      );

      tableControlParameters = tableControlParameters.reduce(
        (obj: Record<string, any>, el: any) => {
          obj[el.name] = el.value;
          return obj;
        },
        {}
      );

      handleSetDraftNewTable({
        selectedVersionId: versionSelect?.version?.tableId,
        elementList: elementListSelected,
        selectedTableId: versionSelect?.tableId,
        tableControlParameters,
        modeledFrom: {
          tableId: versionSelect?.tableId,
          tableName: versionSelect?.version?.tableName,
          versions: [{ tableId: versionSelect?.version?.tableId }]
        }
      });
    }

    if (
      draftNewTable &&
      draftNewTable.selectedVersionId === versionSelect?.version?.tableId
    ) {
      let tableControlParameters = get(
        versionSelect,
        ['version', 'tableControlParameters'],
        []
      );

      tableControlParameters = tableControlParameters.reduce(
        (obj: Record<string, any>, el: any) => {
          obj[el.name] = el.value;
          return obj;
        },
        {}
      );

      let elementListSelected = get(
        versionSelect,
        ['version', 'decisionElements'],
        []
      );

      elementListSelected = elementListSelected
        .map((el: any) => {
          const matchElement =
            decisionElementList.find(
              (element: any) => element.name === el.name
            ) || {};
          return {
            ...el,
            ...matchElement,
            searchCodeText: getSearchCodeText(el?.searchCode),
            rowId: nanoid()
          };
        })
        .filter((el: any) => el.configurable || el.isNewElement);

      handleSetDraftNewTable({
        ...draftNewTable,
        initialValue: {
          tableControlParameters,
          elementList: elementListSelected
        }
      });
    }
  };

  const handleGetTableList = useCallback(() => {
    switch (type) {
      case 'CA':
        dispatch(
          actionsWorkflowSetup.getTableList({
            tableType: 'CA',
            serviceNameAbbreviation: 'MLP'
          })
        );
        break;
      default:
        dispatch(
          actionsWorkflowSetup.getTableList({
            tableType: 'AQ',
            serviceNameAbbreviation: 'MLP'
          })
        );
    }
  }, [dispatch, type]);

  useEffect(() => {
    handleGetTableList();
  }, [handleGetTableList]);

  const handleChangePage = useCallback((page: number) => {
    reactDispatch({ type: ACTION_TYPES.CHANGE_PAGE, page });
  }, []);
  const handleChangePageSize = useCallback(
    (pageSize: number) => {
      reactDispatch({ type: ACTION_TYPES.CHANGE_PAGE_SIZE, pageSize });
      handleChangePage(DEFAULT_PAGE_NUMBER);
    },
    [handleChangePage]
  );

  const handleSearch = useCallback(
    (val: string) => {
      if (searchValue === val) return;
      reactDispatch({
        type: ACTION_TYPES.CHANGE_SEARCH_VALUE,
        searchValue: val
      });
    },
    [searchValue]
  );
  const handleChangeOrderBy = useCallback((orderBy: OrderByType) => {
    reactDispatch({
      type: ACTION_TYPES.CHANGE_ORDER_BY,
      orderBy
    });
  }, []);

  const _searchValue = searchValue.toLowerCase() || '';
  const _tablesOrdered = _orderBy(tableList, 'tableId', orderBy);
  const _tablesFiltered = _tablesOrdered
    .filter((w: any) => {
      const listVersionName = Array.isArray(w?.versions)
        ? w?.versions.map((v: any) => v?.tableName)
        : [];
      return matchSearchValue([w?.tableId, ...listVersionName], _searchValue);
    })
    .map((w: any) => {
      const sortedVersions = _orderBy(w.versions, 'effectiveDate', 'desc');
      return { ...w, versions: sortedVersions };
    });

  const total = _tablesFiltered.length;
  const tables = _tablesFiltered.slice(
    (page! - 1) * pageSize!,
    page! * pageSize!
  );

  const oderByItem = ORDER_BY_LIST.find(i => i.value === orderBy);

  const handleChangePageToSelectedVersion = useCallback(() => {
    if (!Array.isArray(tableList) || tableList.length === 0 || !selectedVersion)
      return;
    const selectedVersionIndex = _tablesOrdered.findIndex(
      (table: any) => table.tableId === selectedVersion?.tableId
    );
    if (selectedVersionIndex === -1) return;
    const selectedVersionPage = Math.ceil(
      (selectedVersionIndex + 1) / pageSize
    );
    handleChangePage(selectedVersionPage);

    handleSetExpandedList(EXPANDED_LIST.tableListModal)({
      expandAction: EXPAND_ACTION.replace,
      expandedList: { [selectedVersion?.tableId]: true }
    });

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tableList, selectedVersion, _tablesOrdered]);

  useEffect(() => {
    handleChangePageToSelectedVersion();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tableList]);

  const handleSetShowVersionDetail = useCallback((versionDetail: any) => {
    reactDispatch({
      type: ACTION_TYPES.SET_SHOW_VERSION_DETAIL,
      versionDetail
    });
  }, []);

  const handleClearAndReset = () => {
    handleSearch('');
    if (selectedVersion) {
      handleChangePageToSelectedVersion();
      return;
    }
    handleSetExpandedList(EXPANDED_LIST.tableListModal)({
      expandAction: EXPAND_ACTION.replace,
      expandedList: {}
    });
    handleChangePageSize(PAGE_SIZE[0]);
  };

  return {
    tables,
    total,
    page,
    pageSize,
    handleChangePage,
    handleChangePageSize,
    handleSearch,
    orderBy: oderByItem,
    searchValue,
    handleChangeOrderBy,
    loading,
    error,
    handleGetTableList,
    handleClearAndReset,
    handleSetShowVersionDetail,
    versionDetail,
    dispatch: reactDispatch,
    handleGoToNextStep
  };
};
