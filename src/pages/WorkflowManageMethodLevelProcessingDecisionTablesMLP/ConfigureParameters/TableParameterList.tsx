import {
  TableFieldNameEnum,
  TableFieldParameterEnum
} from 'app/constants/enums';
import React from 'react';

export interface ParameterGroup {
  id: TableFieldParameterEnum;
  businessName: TableFieldNameEnum;
  greenScreenName: string;
  moreInfoText: string;
  moreInfo: React.ReactNode;
}

export const TableFieldParameter: (type: string) => ParameterGroup[] = type => {
  switch (type) {
    case 'CA':
      return [
        {
          id: TableFieldParameterEnum.AllocationDate,
          businessName: TableFieldNameEnum.AllocationDate,
          greenScreenName: 'ALLOC Date',
          moreInfoText:
            'The Allocation Date parameter defines the date you want the System to allocate accounts through the client allocation table.',
          moreInfo:
            'The Allocation Date parameter defines the date you want the System to allocate accounts through the client allocation table.'
        },
        {
          id: TableFieldParameterEnum.CAAllocationFlag,
          businessName: TableFieldNameEnum.CAAllocationFlag,
          greenScreenName: 'ALLOC Flag',
          moreInfoText:
            'The Allocation Flag parameter determines when you want the System to allocate accounts through the account qualification table.',
          moreInfo:
            'The Allocation Flag parameter determines when you want the System to allocate accounts through the account qualification table.'
        },
        {
          id: TableFieldParameterEnum.AllocationInterval,
          businessName: TableFieldNameEnum.AllocationInterval,
          greenScreenName: 'ALLOC Interval',
          moreInfoText:
            'The Allocation Interval parameter determines the number of months after the account’s Statement Cycle Allocation Date you want the System to allocate accounts through the account qualification table if you set the Allocation Flag field to 2.',
          moreInfo:
            'The Allocation Interval parameter determines the number of months after the account’s Statement Cycle Allocation Date you want the System to allocate accounts through the account qualification table if you set the Allocation Flag field to 2.'
        }
      ];
    default:
      return [
        {
          id: TableFieldParameterEnum.AllocationDate,
          businessName: TableFieldNameEnum.AllocationDate,
          greenScreenName: 'ALLOC Date',
          moreInfoText:
            'The Allocation Date parameter defines the date you want the System to allocate accounts through the client allocation table.',
          moreInfo:
            'The Allocation Date parameter defines the date you want the System to allocate accounts through the client allocation table.'
        },
        {
          id: TableFieldParameterEnum.AQAllocationFlag,
          businessName: TableFieldNameEnum.AQAllocationFlag,
          greenScreenName: 'ALLOC Flag',
          moreInfoText:
            'The Allocation Flag parameter determines when you want the System to allocate accounts through the account qualification table.',
          moreInfo:
            'The Allocation Flag parameter determines when you want the System to allocate accounts through the account qualification table.'
        },
        {
          id: TableFieldParameterEnum.AllocationInterval,
          businessName: TableFieldNameEnum.AllocationInterval,
          greenScreenName: 'ALLOC Interval',
          moreInfoText:
            'The Allocation Interval parameter determines the number of months after the account’s Statement Cycle Allocation Date you want the System to allocate accounts through the account qualification table if you set the Allocation Flag field to 2.',
          moreInfo:
            'The Allocation Interval parameter determines the number of months after the account’s Statement Cycle Allocation Date you want the System to allocate accounts through the account qualification table if you set the Allocation Flag field to 2.'
        },
        {
          id: TableFieldParameterEnum.AllocationBeforeAfterCycle,
          businessName: TableFieldNameEnum.AllocationBeforeAfterCycle,
          greenScreenName: 'ALLOC B/A Cycle',
          moreInfoText:
            'The Allocation Before/After Cycle parameter determines whether the System allocates accounts before or after statement cycle processing.',
          moreInfo:
            'The Allocation Before/After Cycle parameter determines whether the System allocates accounts before or after statement cycle processing.'
        },
        {
          id: TableFieldParameterEnum.ChangeCode,
          businessName: TableFieldNameEnum.ChangeCode,
          greenScreenName: 'Change Code',
          moreInfoText:
            'The Change Code parameter indicates the client-defined reason code for the current method override; variable-length, 2-position, optional field.',
          moreInfo:
            'The Change Code parameter indicates the client-defined reason code for the current method override; variable-length, 2-position, optional field.'
        },
        {
          id: TableFieldParameterEnum.ChangeInTermsMethodFlag,
          businessName: TableFieldNameEnum.ChangeInTermsMethodFlag,
          greenScreenName: 'CIT Method Flag',
          moreInfoText:
            'The Change In Terms Method Flag parameter designates whether you want to associate CIT methods with the pricing strategies assigned during reallocation processing.',
          moreInfo:
            'The Change In Terms Method Flag parameter designates whether you want to associate CIT methods with the pricing strategies assigned during reallocation processing.'
        },
        {
          id: TableFieldParameterEnum.ServiceSubjectSectionAreas,
          businessName: TableFieldNameEnum.ServiceSubjectSectionAreas,
          greenScreenName: 'SS/SS/SS Areas',
          moreInfoText:
            'The Service/Subject/Section Areas defines a list of Product Control File method overrides can be selected to include in your table.',
          moreInfo:
            'The Service/Subject/Section Areas defines a list of Product Control File method overrides can be selected to include in your table.'
        }
      ];
  }
};
