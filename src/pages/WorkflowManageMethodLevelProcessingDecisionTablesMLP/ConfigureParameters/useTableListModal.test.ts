import { act, renderHook } from '@testing-library/react-hooks';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageAccountLevel';
import * as mockSelector from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageMethodLevelProcessingDecisionMLP';
import * as WorkflowSetupSelector from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageTransactionLevelProcessingDecisionTLP';
import * as reactRedux from 'react-redux';
import { useTableListModal } from './useTableListModal';

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');
const mockUseSelector = jest.spyOn(reactRedux, 'useSelector');

const useSelectDecisionElementListData = jest.spyOn(
  WorkflowSetupSelector,
  'useSelectDecisionElementListData'
);

const mockStore = jest.spyOn(
  mockSelector,
  'useSelectElementMetadataManageMethodLevelProcessingMLP'
);

const TableListMockData = [
  {
    tableId: 'LUMEJ598',
    tableName: '8A41RDTB',
    versions: [
      {
        tableId: 'IFAA8XEE',
        tableName: 'WFRKLMZB',
        effectiveDate: '2021-05-31T17:00:00Z',
        status: 'Production',
        comment: 'at tempor accusam',
        tableControlParameters: [{ name: 'abc', value: 123 }],
        decisionElements: [
          {
            name: 'APPLICATION SCORE',
            searchCode: 'E',
            immediateAllocation: ''
          },
          {
            name: 'RESULT ID',
            searchCode: '',
            immediateAllocation: ''
          },
          {
            name: 'ACS CURR PORT',
            searchCode: '',
            immediateAllocation: ''
          }
        ]
      },
      {
        tableId: 'K1TIBA4C',
        tableName: 'ZHFMK7K2',
        effectiveDate: '2021-05-28T17:00:00Z',
        status: 'Client Approved',
        comment: 'eu labore congue ea',
        tableControlParameters: [{ name: 'abc', value: 123 }],
        decisionElements: [
          {
            name: 'APPLICATION SCORE',
            searchCode: 'E',
            immediateAllocation: ''
          },
          {
            name: 'RESULT ID',
            searchCode: '',
            immediateAllocation: ''
          },
          {
            name: 'ACS CURR PORT',
            searchCode: '',
            immediateAllocation: ''
          }
        ]
      },
      {
        tableId: 'WMAEVM7G',
        tableName: '91IDUIVL',
        effectiveDate: '2021-10-17T17:00:00Z',
        status: 'Validating',
        comment: 'nulla aliquip cum magna iriure ex velit',
        tableControlParameters: [{ name: 'abc', value: 123 }],
        decisionElements: [
          {
            name: 'APPLICATION SCORE',
            searchCode: 'E',
            immediateAllocation: ''
          },
          {
            name: 'RESULT ID',
            searchCode: '',
            immediateAllocation: ''
          },
          {
            name: 'ACS CURR PORT',
            searchCode: '',
            immediateAllocation: ''
          }
        ]
      }
    ]
  },
  {
    tableId: 'LUMEJ5F',
    tableName: '8A41RDTH',
    versions: [
      {
        tableId: 'IFAA8XEE',
        tableName: 'WFRKLMZB',
        effectiveDate: '2021-05-31T17:00:00Z',
        status: 'Production',
        comment: 'at tempor accusam',
        tableControlParameters: [{ name: 'abc', value: 123 }],
        decisionElements: [
          {
            name: 'APPLICATION SCORE',
            searchCode: 'E',
            immediateAllocation: ''
          },
          {
            name: 'RESULT ID',
            searchCode: '',
            immediateAllocation: ''
          },
          {
            name: 'ACS CURR PORT',
            searchCode: '',
            immediateAllocation: ''
          }
        ]
      },
      {
        tableId: 'K1TIBA4DD',
        tableName: 'ZHFMK7EE',
        effectiveDate: '2021-05-28T17:00:00Z',
        status: 'Client Approved',
        comment: 'eu labore congue ea',
        tableControlParameters: [{ name: 'abc', value: 123 }],
        decisionElements: [
          {
            name: 'APPLICATION SCORE',
            searchCode: 'E',
            immediateAllocation: ''
          },
          {
            name: 'RESULT ID',
            searchCode: '',
            immediateAllocation: ''
          },
          {
            name: 'ACS CURR PORT',
            searchCode: '',
            immediateAllocation: ''
          }
        ]
      },
      {
        tableId: 'WMAEVMKK',
        tableName: '91IDUIVL',
        effectiveDate: '2021-10-17T17:00:00Z',
        status: 'Validating',
        comment: 'nulla aliquip cum magna iriure ex velit',
        tableControlParameters: [{ name: 'abc', value: 123 }],
        decisionElements: [
          {
            name: 'APPLICATION SCORE',
            searchCode: 'E',
            immediateAllocation: ''
          },
          {
            name: 'RESULT ID',
            searchCode: '',
            immediateAllocation: ''
          },
          {
            name: 'ACS CURR PORT',
            searchCode: '',
            immediateAllocation: ''
          }
        ]
      }
    ]
  },
  {
    tableId: 'LUMEJ5F',
    tableName: '8A41RDTH'
  }
];

const SelectedVersion = {
  tableId: 'LUMEJ598',
  version: {
    tableId: 'IFAA8XEE',
    tableName: 'WFRKLMZB',
    effectiveDate: '2021-05-31T17:00:00Z',
    status: 'Production',
    comment: 'at tempor accusam',
    tableControlParameters: [{ name: 'abc', value: 123 }],
    decisionElements: [
      {
        name: 'APPLICATION SCORE',
        searchCode: 'E',
        immediateAllocation: ''
      },
      {
        name: 'RESULT ID',
        searchCode: '',
        immediateAllocation: ''
      },
      {
        name: 'ACS CURR PORT',
        searchCode: '',
        immediateAllocation: ''
      }
    ]
  }
};

const useSelectTableListData = jest.spyOn(
  WorkflowSetup,
  'useSelectTableListData'
);

const t = (value: string) => value;

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

const MockElementListOptions = [
  {
    name: 'ACS CURR PORT',
    moreInfo:
      'Adaptive Control System portfolio currently in effect for the customer account; variable-length, 4 positions, numeric.',
    immediateAllocation: 'No',
    configurable: true
  },
  {
    name: 'ACS PORT CHG DATE',
    moreInfo:
      'Date (MMDDYYYY) the current Adaptive Control System portfolio identification was last changed; variable-length, 10 positions, numeric.\n Example: 031404 or 03142004 = March 14, 2004',
    immediateAllocation: 'No',
    configurable: true
  },
  {
    name: 'RESULT ID',
    moreInfo:
      'Enter the Promotion Identifier in the RESULT ID field for each criteria row to point cardholder accounts to Product Control File settings.\nFor the transaction qualification tables, the Promotion Identifier is a variable-length, 8-position code that identifies a group of Product Control File methods. The methods include parameter settings that control processing options.',
    immediateAllocation: 'No',
    configurable: false
  }
];

describe('WorkflowManageMethodLevelProcessingDecisionTablesMLP > ConfigureParameters > useTableListModal', () => {
  const mockDispatch = jest.fn();
  const handleSetExpandedList = () => jest.fn();
  const handleOpenConfigModal = () => jest.fn();
  const handleSelectVersion = () => jest.fn();
  const handleSetDraftNewTable = () => jest.fn();

  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
    mockUseSelector.mockImplementation(selector =>
      selector({
        caches: {}
      })
    );
    mockStore.mockReturnValue({
      allocationBeforeAfterCycle: [{ code: 'A', text: 'A - ATest' }],
      caAllocationFlag: [{ code: 'A', text: 'A - ATest' }],
      aqAllocationFlag: [{ code: 'A', text: 'A - ATest' }],
      allocationInterval: [{ code: 'A', text: 'A - ATest' }],
      changeCode: [{ code: 'A', text: 'A - ATest' }],
      changeInTermsMethodFlag: [{ code: 'A', text: 'A - ATest' }],
      searchCode: [{ code: 'A', text: 'A - ATest' }],
      serviceSubjectSectionAreas: [{ code: 'A', text: 'A - ATest' }]
    } as any);
    useSelectDecisionElementListData.mockReturnValue({
      decisionElementList: MockElementListOptions
    });
    useSelectTableListData.mockReturnValue({
      tableList: TableListMockData
    });
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
    mockUseSelector.mockClear();
  });

  it('Run Default reducer function', () => {
    useSelectTableListData.mockReturnValue({});

    const { result } = renderHook(() =>
      useTableListModal(
        null,
        handleSetExpandedList,
        'AQ',
        handleOpenConfigModal,
        handleSelectVersion,
        '123',
        handleSetDraftNewTable
      )
    );

    act(() => {
      result.current.dispatch({ type: 'Test Action' });
    });
  });

  it('Should run normal hook', () => {
    const { result } = renderHook(() =>
      useTableListModal(
        null,
        handleSetExpandedList,
        'CA',
        handleOpenConfigModal,
        handleSelectVersion,
        '123',
        handleSetDraftNewTable
      )
    );
    act(() => {
      result.current.handleChangePage(2);
    });
    expect(result.current.page).toEqual(2);

    act(() => {
      result.current.handleChangePageSize(10);
    });
    expect(result.current.pageSize).toEqual(10);

    act(() => {
      result.current.handleSetShowVersionDetail(true);
      result.current.handleChangeOrderBy('desc');
      result.current.handleSearch('abc');
      result.current.handleSearch('abc');
    });

    expect(result.current.searchValue).toEqual('abc');

    act(() => {
      result.current.handleClearAndReset();
    });
    expect(result.current.searchValue).toEqual('');
  });

  it('It should render hook with selected versionDetail', () => {
    const { result } = renderHook(() =>
      useTableListModal(
        SelectedVersion,
        handleSetExpandedList,
        'CA',
        handleOpenConfigModal,
        handleSelectVersion,
        '123',
        handleSetDraftNewTable
      )
    );

    act(() => {
      result.current.handleSearch('abc');
      result.current.handleClearAndReset();
      result.current.handleGoToNextStep(SelectedVersion);
    });
  });

  it('It should render hook with selected version', () => {
    const { result } = renderHook(() =>
      useTableListModal(
        SelectedVersion,
        handleSetExpandedList,
        'CA',
        handleOpenConfigModal,
        handleSelectVersion,
        {
          selectedVersionId: 'IFAA8XEE'
        },
        handleSetDraftNewTable
      )
    );

    act(() => {
      result.current.handleSearch('abc');
      result.current.handleClearAndReset();
      result.current.handleGoToNextStep(null);
    });
  });

  it('It should render hook with selected version Not In List', () => {
    useSelectDecisionElementListData.mockReturnValue({});
    renderHook(() =>
      useTableListModal(
        { tableId: 'test', version: {} },
        handleSetExpandedList,
        'AQ',
        handleOpenConfigModal,
        handleSelectVersion,
        '123',
        handleSetDraftNewTable
      )
    );
  });
});
