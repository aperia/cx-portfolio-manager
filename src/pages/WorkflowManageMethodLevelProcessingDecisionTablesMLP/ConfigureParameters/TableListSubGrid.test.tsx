import { TableFieldParameterEnum } from 'app/constants/enums';
import { renderWithMockStore } from 'app/utils';
import * as CommonSelectHooks from 'pages/_commons/redux/Common/select-hooks';
import * as mockSelector from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageMethodLevelProcessingDecisionMLP';
import React from 'react';
import TableListSubGrid from './TableListSubGrid';

const mockStore = jest.spyOn(
  mockSelector,
  'useSelectElementMetadataManageMethodLevelProcessingMLP'
);

const mockUseSelectWindowDimension = jest.spyOn(
  CommonSelectHooks,
  'useSelectWindowDimension'
);

const MockElementList = [] as any[];

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderComponent = (props: any) =>
  renderWithMockStore(<TableListSubGrid {...props} />, {});

describe('WorkflowManageMethodLevelProcessingDecisionTablesMLP > ConfigureParameter > TableListSubGrid', () => {
  beforeEach(() => {
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1920,
      height: 1080
    }));
    mockStore.mockReturnValue({
      allocationBeforeAfterCycle: [{ code: 'none', text: 'none' }],
      caAllocationFlag: [{ code: 'A', text: 'A - ATest' }],
      aqAllocationFlag: [{ code: 'A', text: 'A - ATest' }],
      allocationInterval: [{ code: 'A', text: 'A - ATest' }],
      changeCode: [{ code: 'A', text: 'A - ATest' }],
      changeInTermsMethodFlag: [{ code: 'A', text: 'A - ATest' }],
      searchCode: [{ code: 'A', text: 'A - ATest' }],
      serviceSubjectSectionAreas: [{ code: 'abc', text: 'A - ATest' }]
    } as any);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Should render Table List SubGrid empty', async () => {
    const wrapper = await renderComponent({});

    expect(
      wrapper.getByText('txt_manage_transaction_level_tlp_element_list')
    ).toBeInTheDocument();
  });

  it('Should render Table List SubGrid CA', async () => {
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 900,
      height: 1080
    }));
    const wrapper = await renderComponent({
      elementList: MockElementList,
      type: 'CA',
      tableControlParameters: {
        [TableFieldParameterEnum.AllocationDate]: '02/02/2022'
      }
    });

    expect(
      wrapper.getByText('txt_manage_transaction_level_tlp_element_list')
    ).toBeInTheDocument();
  });

  it('Should render Table List SubGrid AQ', async () => {
    const wrapper = await renderComponent({
      type: 'AQ',
      tableControlParameters: {
        [TableFieldParameterEnum.AllocationDate]: '02/02/2022',
        [TableFieldParameterEnum.ServiceSubjectSectionAreas]:
          'CPICBP; CPICID; CPICII; CPICIV',
        [TableFieldParameterEnum.AllocationBeforeAfterCycle]: 'none'
      },
      elementList: [
        {
          name: '123',
          searchCode: 'search',
          moreInfo: '123',
          immediateAllocation: '123'
        },
        {
          name: '123',
          searchCode: 'search',
          moreInfo: '123',
          immediateAllocation: 'Yes'
        },
        {
          name: '123',
          searchCode: 'search',
          moreInfo: '123',
          immediateAllocation: 'No'
        },
        {
          name: '123',
          searchCode: 'search',
          moreInfo: '123',
          immediateAllocation: null
        }
      ]
    });

    expect(
      wrapper.getByText('txt_manage_transaction_level_tlp_element_list')
    ).toBeInTheDocument();
  });
});
