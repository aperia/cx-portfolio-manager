import { fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { TableFieldParameterEnum } from 'app/constants/enums';
import { renderWithMockStore } from 'app/utils';
import 'app/utils/_mockComponent/mockDatePicker';
import 'app/utils/_mockComponent/mockEnhanceMultiSelect';
import * as CommonRedux from 'pages/_commons/redux/Common/select-hooks';
import * as mockSelector from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageMethodLevelProcessingDecisionMLP';
import React from 'react';
import { act } from 'react-dom/test-utils';
import GridTable from './GridTable';

jest.mock('app/_libraries/_dls/components/MultiSelect', () => {
  const MockMultiSelect = ({ onChange }: any) => {
    return (
      <div
        onClick={() =>
          onChange({
            target: {
              value: [
                {
                  code: 'A',
                  text: 'A - ATest'
                }
              ]
            }
          })
        }
      >
        MockMultiSelect
      </div>
    );
  };

  MockMultiSelect.Item = () => <div>Mock Item</div>;
  return {
    __esModule: true,
    default: MockMultiSelect
  };
});

const mockStore = jest.spyOn(
  mockSelector,
  'useSelectElementMetadataManageMethodLevelProcessingMLP'
);
const mockUseSelectWindowDimension = jest.spyOn(
  CommonRedux,
  'useSelectWindowDimension'
);

HTMLCanvasElement.prototype.getContext = jest.fn();

const checkByCase = (type: string, data: any, idx: number, typeGrid = 'AQ') => {
  it(type, async () => {
    const wrapper = await renderWithMockStore(
      <GridTable
        data={data}
        type={typeGrid}
        handleChangeControlParameters={jest.fn()}
      />,
      {}
    );
    //handleChangeControlParameters
    const getText = wrapper.getAllByText('txt_select_an_option');
    userEvent.click(getText[idx]);
    userEvent.click(wrapper.getAllByText('A - ATest')[0]);
  });
};

describe('', () => {
  beforeEach(() => {
    mockUseSelectWindowDimension.mockReturnValue({ width: 900 } as any);
    mockStore.mockReturnValue({
      allocationBeforeAfterCycle: [{ code: 'A', text: 'A - ATest' }],
      caAllocationFlag: [{ code: 'A', text: 'A - ATest' }],
      aqAllocationFlag: [{ code: 'A', text: 'A - ATest' }],
      allocationInterval: [{ code: 'A', text: 'A - ATest' }],
      changeCode: [{ code: 'A', text: 'A - ATest' }],
      changeInTermsMethodFlag: [{ code: 'A', text: 'A - ATest' }],
      searchCode: [{ code: 'A', text: 'A - ATest' }],
      serviceSubjectSectionAreas: [{ code: 'A', text: 'A - ATest' }]
    });
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  checkByCase(
    TableFieldParameterEnum.AllocationDate,
    {
      [TableFieldParameterEnum.AllocationDate]: '123'
    },
    0
  );
  checkByCase(
    TableFieldParameterEnum.CAAllocationFlag,
    {
      [TableFieldParameterEnum.CAAllocationFlag]: '123'
    },
    0,
    'CA'
  );
  checkByCase(
    TableFieldParameterEnum.AQAllocationFlag,
    {
      [TableFieldParameterEnum.AQAllocationFlag]: '123'
    },
    1
  );
  checkByCase(
    TableFieldParameterEnum.AllocationBeforeAfterCycle,
    {
      [TableFieldParameterEnum.AllocationBeforeAfterCycle]: '123'
    },
    2
  );
  checkByCase(
    TableFieldParameterEnum.AllocationInterval,
    {
      [TableFieldParameterEnum.AllocationInterval]: '123'
    },
    3
  );

  it('changeCode', async () => {
    const wrapper = await renderWithMockStore(
      <GridTable
        data={{
          [TableFieldParameterEnum.ServiceSubjectSectionAreas]: ''
        }}
        type="AQ"
        handleChangeControlParameters={jest.fn()}
      />,
      {}
    );
    const input = wrapper.queryByPlaceholderText(
      'txt_enter_a_value'
    ) as HTMLElement;
    userEvent.type(input, 'test');
  });

  it('serviceSubjectSectionAreas', async () => {
    mockUseSelectWindowDimension.mockReturnValue({ width: 1300 } as any);
    const wrapper = await renderWithMockStore(
      <GridTable
        data={{
          [TableFieldParameterEnum.ServiceSubjectSectionAreas]: 'acb;bc'
        }}
        type="AQ"
        handleChangeControlParameters={jest.fn()}
      />,
      {}
    );

    const input = wrapper.queryByTestId(
      'MultiSelect.Input_onChange'
    ) as HTMLElement;
    userEvent.type(input, 'test');
  });

  it('serviceSubjectSectionAreas', async () => {
    mockUseSelectWindowDimension.mockReturnValue({ width: 1300 } as any);
    const wrapper = await renderWithMockStore(
      <GridTable
        data={{
          [TableFieldParameterEnum.ServiceSubjectSectionAreas]: null
        }}
        type="AQ"
        handleChangeControlParameters={jest.fn()}
      />,
      {}
    );

    const searchInput = wrapper.getByPlaceholderText('txt_type_to_search');
    userEvent.click(searchInput);
    userEvent.type(searchInput, '123');

    userEvent.click(wrapper.getByText('MockMultiSelect'));
  });

  it('AllocationDate', async () => {
    const wrapper = await renderWithMockStore(
      <GridTable
        data={{
          [TableFieldParameterEnum.AllocationDate]: '01/01/2020'
        }}
        type="AQ"
        handleChangeControlParameters={jest.fn()}
      />,
      {}
    );

    const datepicker = wrapper.queryByPlaceholderText('txt_date_format');
    userEvent.click(datepicker!);
    userEvent.clear(datepicker!);
    act(() => {
      fireEvent.change(datepicker!, {
        target: {
          value: '01/02/2020'
        }
      });
    });

    fireEvent.blur(datepicker!);
  });

  it('handleSearchValue', async () => {
    const wrapper = await renderWithMockStore(
      <GridTable
        data={{
          [TableFieldParameterEnum.AllocationDate]: '01/01/2020'
        }}
        type="AQ"
        handleChangeControlParameters={jest.fn()}
      />,
      {}
    );

    const searchInput = wrapper.getByPlaceholderText('txt_type_to_search');
    userEvent.type(searchInput, '123');
    userEvent.type(searchInput, '{enter}');

    userEvent.click(wrapper.getByText('txt_clear_and_reset'));

    userEvent.type(searchInput, 'A');
    userEvent.type(searchInput, '{enter}');

    userEvent.click(wrapper.getByText('txt_clear_and_reset'));
  });
});
