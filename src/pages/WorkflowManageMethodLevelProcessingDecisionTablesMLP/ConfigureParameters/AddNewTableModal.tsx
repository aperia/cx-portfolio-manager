import ModalRegistry from 'app/components/ModalRegistry';
import TextAreaCountDown from 'app/components/TextAreaCountDown';
import {
  unsavedChangesProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { useUnsavedChangeRegistry, useUnsavedChangesRedirect } from 'app/hooks';
import {
  Button,
  InlineMessage,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  TextBox,
  TransDLS,
  useTranslation
} from 'app/_libraries/_dls';
import { nanoid } from 'nanoid';
import React, { useEffect, useMemo } from 'react';
import GridDecision from './GridDecision';
import GridTable from './GridTable';
import { useAddNewTable } from './useAddNewTable';
import {
  CONFIG_ACTIONS_MODAL,
  EXPANDED_LIST,
  EXPAND_ACTION
} from './useConfigActions';

export const AddNewTableAction = {
  createNewTable: 'createNewTable',
  createNewVersion: 'createNewVersion',
  addTableToModel: 'addTableToModel'
};

interface AddNewTableModalProps {
  id: string;
  show: boolean;
  handleClose: () => void;
  selectedVersion: any;
  handleOpenConfigModal: (tableName: CONFIG_ACTIONS_MODAL) => void;
  handleSelectVersion: (version: any) => void;
  handleSetDraftNewTable: (draftNewTable: any) => void;
  draftNewTable: any;
  isCreateNewVersion: boolean;
  handleAddNewTable: (table: any) => void;
  handleSetIsCreateNewVersion: (isCreateNewVersion: boolean) => void;
  selectedTable: any;
  handleEditTable: (newTable: any) => void;
  handleSetSelectedTable: (table: any) => void;
  handleSetExpandedList: (
    tableListName: EXPANDED_LIST
  ) => (expandItem: Record<string, any>) => void;
  handleCheckDuplicateTableId: (tableId: string) => boolean;
  type: string;
}

const AddNewTableModal: React.FC<AddNewTableModalProps> = ({
  show,
  handleClose,
  id,
  handleOpenConfigModal,
  handleSelectVersion,
  draftNewTable,
  handleSetDraftNewTable,
  selectedVersion,
  isCreateNewVersion,
  handleAddNewTable,
  handleSetIsCreateNewVersion,
  selectedTable,
  handleEditTable,
  handleSetSelectedTable,
  handleSetExpandedList,
  handleCheckDuplicateTableId,
  type
}) => {
  const { t } = useTranslation();

  const redirect = useUnsavedChangesRedirect();

  const actionType = useMemo(() => {
    if (selectedTable) return selectedTable.actionTaken;
    if (!selectedVersion) return AddNewTableAction.createNewTable;
    if (selectedVersion && isCreateNewVersion)
      return AddNewTableAction.createNewVersion;
    return AddNewTableAction.addTableToModel;
  }, [isCreateNewVersion, selectedVersion, selectedTable]);

  const {
    tableId,
    tableName,
    comment,
    handleBlurField,
    handleChangeFieldValue,
    errors,
    elementList,
    handleSetElementList,
    handleSetElementListHasError,
    selectedTableId,
    selectedVersionId,
    isElementListHasError,
    handleSetIsDuplicateTableId,
    isDuplicateTableId,
    handleChangeTableControlParameter,
    tableControlParameters,
    isFormChanging,
    modeledFrom
  } = useAddNewTable(draftNewTable, isCreateNewVersion, selectedTable, type);

  useEffect(() => {
    if (
      actionType === AddNewTableAction.createNewTable &&
      !elementList?.[0].name
    ) {
      handleSetElementListHasError(true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const unsavedChangeName = useMemo(() => {
    switch (type) {
      case 'CA':
        return UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__LEVEL_PROCESSING_CA__METHOD_DETAILS;
      default:
        return UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__LEVEL_PROCESSING_AQ__METHOD_DETAILS;
    }
  }, [type]);

  useUnsavedChangeRegistry(
    {
      ...unsavedChangesProps,
      formName: unsavedChangeName,
      priority: 1
    },
    [isFormChanging]
  );

  const handleGoBackToPreviousStep = () => {
    handleOpenConfigModal(CONFIG_ACTIONS_MODAL.tableListModal);
    handleSetDraftNewTable({
      comment,
      tableId,
      tableName,
      selectedTableId,
      elementList,
      selectedVersionId,
      tableControlParameters,
      isFormChanging
    });
    if (selectedTable && !selectedVersion) {
      handleSelectVersion(selectedTable?.selectedVersion);
    }
  };

  const handleCloseAndClearState = () => {
    handleClose();
    handleSelectVersion(null);
    handleSetIsCreateNewVersion(false);
    handleSetDraftNewTable(null);
    handleSetSelectedTable(null);
  };

  const handleCloseModal = () => {
    redirect({
      onConfirm: () => handleCloseAndClearState(),
      formsWatcher: [unsavedChangeName]
    });
  };

  const isDisabledSaveButton =
    isElementListHasError ||
    (!tableId && actionType !== AddNewTableAction.createNewVersion) ||
    !tableName;

  const handleCreateNewTable = () => {
    if (
      tableId !== selectedTable?.tableId &&
      handleCheckDuplicateTableId(tableId)
    ) {
      handleSetIsDuplicateTableId(true);
      return;
    }
    const tableItem = {
      rowId: selectedTable ? selectedTable.rowId : nanoid(),
      tableId:
        actionType === AddNewTableAction.createNewVersion
          ? selectedTableId
          : tableId,
      tableName,
      elementList,
      comment,
      actionTaken: actionType,
      selectedVersionId,
      selectedTableId,
      selectedVersion: selectedTable?.selectedVersion || selectedVersion,
      tableControlParameters,
      modeledFrom
    };

    selectedTable ? handleEditTable(tableItem) : handleAddNewTable(tableItem);
    handleCloseAndClearState();
    !selectedTable &&
      handleSetExpandedList(EXPANDED_LIST.tableListIndex)({
        expandAction: EXPAND_ACTION.replace,
        expandedList: { [tableItem?.rowId]: true }
      });
  };

  const renderTableDetailsText = () => {
    if (type === 'AQ') {
      return (
        <>
          <p>
            <TransDLS keyTranslation="txt_manage_method_level_processing_aq_help_text">
              <sup />
            </TransDLS>
          </p>
          <p className="mt-8">
            {t('txt_manage_method_level_processing_aq_help_text_2')}
          </p>
        </>
      );
    } else {
      return (
        <TransDLS keyTranslation="txt_manage_method_level_processing_ca_help_text">
          <sup />
        </TransDLS>
      );
    }
  };

  const handleGetTableList = useMemo(() => {
    switch (type) {
      case 'CA':
        return 'MLPCA';
      default:
        return 'MLPAQ';
    }
  }, [type]);

  const renderMaxLengthTableID = useMemo(() => {
    switch (type) {
      case 'CA':
        return 6;
      default:
        return 8;
    }
  }, [type]);

  const renderMaxLengthTableName = useMemo(() => {
    switch (type) {
      case 'CA':
        return 8;
      default:
        return 18;
    }
  }, [type]);

  return (
    <ModalRegistry lg id={id} show={show} onAutoClosedAll={handleCloseModal}>
      <ModalHeader border closeButton onHide={handleCloseModal}>
        <ModalTitle>
          {t('txt_manage_transaction_level_tlp_config_tq_tableDetails')}
        </ModalTitle>
      </ModalHeader>
      <ModalBody>
        <div className="color-grey">{renderTableDetailsText()}</div>
        <div>
          {isDuplicateTableId && (
            <InlineMessage variant="danger" withIcon className="mb-8 mt-16">
              {t('txt_manage_transaction_level_tlp_warning_duplicate_table_id')}
            </InlineMessage>
          )}
        </div>
        <div className="row mt-16">
          {actionType !== AddNewTableAction.createNewTable && (
            <div className="col-6 col-lg-4">
              <TextBox
                id="addNewMethod__selectedTableId"
                readOnly
                value={selectedTableId}
                maxLength={8}
                label={t(
                  'txt_manage_transaction_level_tlp_config_tq_selected_table_id'
                )}
              />
            </div>
          )}
          {actionType !== AddNewTableAction.createNewVersion && (
            <div className="col-6 col-lg-4">
              <TextBox
                id="addNewTable__TableId"
                value={tableId}
                onChange={handleChangeFieldValue('tableId')}
                required
                onBlur={handleBlurField('tableId')}
                maxLength={renderMaxLengthTableID}
                label={t('txt_manage_transaction_level_tlp_config_tq_table_id')}
                error={
                  isDuplicateTableId
                    ? {
                        status: true,
                        message: t(
                          'txt_manage_transaction_level_tlp_warning_duplicate_table_id'
                        )
                      }
                    : errors.tableId
                }
              />
            </div>
          )}
          {
            <div className="col-6 col-lg-4">
              <TextBox
                id="addNewTable__tableName"
                value={tableName}
                onChange={handleChangeFieldValue('tableName')}
                onBlur={handleBlurField('tableName')}
                required
                maxLength={renderMaxLengthTableName}
                label={t(
                  'txt_manage_transaction_level_tlp_config_tq_table_name'
                )}
                error={errors.tableName}
              />
            </div>
          }
          <div className="mt-16 col-12 col-lg-8">
            <TextAreaCountDown
              id="addNewTable__comment"
              label={t('txt_comment_area')}
              value={comment}
              onChange={handleChangeFieldValue('comment')}
              rows={4}
              maxLength={204}
            />
          </div>
        </div>
        <GridTable
          data={tableControlParameters}
          handleChangeControlParameters={handleChangeTableControlParameter}
          type={type}
        />
        <hr className="my-24" />
        <GridDecision
          elementList={elementList}
          handleSetElementList={handleSetElementList}
          handleSetElementListHasError={handleSetElementListHasError}
          tableType={handleGetTableList}
        />
      </ModalBody>
      <ModalFooter
        cancelButtonText={t('txt_cancel')}
        okButtonText={t('txt_save')}
        onCancel={handleCloseModal}
        onOk={handleCreateNewTable}
        disabledOk={isDisabledSaveButton}
      >
        {actionType !== AddNewTableAction.createNewTable && (
          <Button
            className="mr-auto ml-n8"
            variant="outline-primary"
            onClick={handleGoBackToPreviousStep}
          >
            {t('txt_back')}
          </Button>
        )}
      </ModalFooter>
    </ModalRegistry>
  );
};

export default AddNewTableModal;
