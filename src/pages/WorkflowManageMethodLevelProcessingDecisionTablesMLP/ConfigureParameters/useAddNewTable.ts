import { METHOD_LEVEL_PROCESSING_DEFAULT } from 'app/constants/mapping';
import { booleanToString, stringValidate } from 'app/helpers';
import { useFormValidations } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls';
import { isEqual } from 'app/_libraries/_dls/lodash';
import { omit } from 'lodash';
import isEmpty from 'lodash.isempty';
import { nanoid } from 'nanoid';
import { useCallback, useMemo, useReducer } from 'react';

interface NewTableState {
  selectedTableId: string;
  tableId: string;
  tableName: string;
  comment: string;
  elementList: any[];
  isElementListHasError?: boolean;
  selectedVersionId: string;
  isDuplicateTableId: boolean;
  tableControlParameters: Record<string, any>;
  isChangeElementList: boolean;
  isFormChanging?: boolean | undefined;
  initialValue?: any;
  modeledFrom?: any;
}

const NEW_TABLE_INITIAL_STATE = {
  selectedTableId: '',
  tableId: '',
  tableName: '',
  comment: '',
  elementList: [],
  selectedVersionId: '',
  isDuplicateTableId: false,
  tableControlParameters: {},
  isChangeElementList: false
};

const ACTION_TYPES = {
  CHANGE_FORM_VALUE: 'CHANGE_FORM_VALUE',
  SET_ELEMENT_LIST: 'SET_ELEMENT_LIST',
  SET_TABLE_CONTROL_PARAMETERS: 'SET_TABLE_CONTROL_PARAMETERS',
  SET_ELEMENT_LIST_HAS_ERROR: 'SET_ELEMENT_LIST_HAS_ERROR',
  SET_IS_DUPLICATE_TABLE_ID: 'SET_IS_DUPLICATE_TABLE_ID',
  SET_IS_CHANGE_ELEMENT_LIST: 'SET_IS_CHANGE_ELEMENT_LIST'
};

const newTableReducer = (state: NewTableState, action: any) => {
  switch (action.type) {
    case ACTION_TYPES.CHANGE_FORM_VALUE:
      return { ...state, [action.field]: action.value };
    case ACTION_TYPES.SET_ELEMENT_LIST:
      return { ...state, elementList: action.elementList };
    case ACTION_TYPES.SET_ELEMENT_LIST_HAS_ERROR:
      return { ...state, isElementListHasError: action.isElementListHasError };
    case ACTION_TYPES.SET_IS_DUPLICATE_TABLE_ID:
      return { ...state, isDuplicateTableId: action.isDuplicateTableId };
    case ACTION_TYPES.SET_TABLE_CONTROL_PARAMETERS:
      return {
        ...state,
        tableControlParameters: action.tableControlParameters
      };
    case ACTION_TYPES.SET_IS_CHANGE_ELEMENT_LIST:
      return { ...state, isChangeElementList: action.isChangeElementList };
    default:
      return state;
  }
};

export const useAddNewTable = (
  draftNewTable: Record<string, any>,
  isCreateNewVersion: boolean,
  selectedTable: any,
  type: string
) => {
  const initialState = useMemo(() => {
    if (selectedTable && isEmpty(draftNewTable)) {
      return { ...NEW_TABLE_INITIAL_STATE, ...selectedTable };
    }
    if (!selectedTable && isEmpty(draftNewTable)) {
      return {
        ...NEW_TABLE_INITIAL_STATE,
        elementList: [{ name: '', isNewElement: true, rowId: nanoid() }],
        tableControlParameters: METHOD_LEVEL_PROCESSING_DEFAULT
      };
    }
    return { ...NEW_TABLE_INITIAL_STATE, ...draftNewTable };
  }, [selectedTable, draftNewTable]);

  const [state, reactDispatch] = useReducer(newTableReducer, initialState);
  const { t } = useTranslation();

  const {
    tableName,
    tableId,
    comment,
    elementList,
    selectedTableId,
    selectedVersionId,
    isElementListHasError,
    isDuplicateTableId,
    tableControlParameters,
    isChangeElementList,
    modeledFrom
  } = state;

  const currentErrors = useMemo(
    () =>
      ({
        tableName: !tableName && {
          status: true,
          message: t('txt_required_validation', {
            fieldName: 'Table Name'
          })
        },
        tableId: !isCreateNewVersion &&
          !tableId && {
            status: true,
            message: t('txt_required_validation', {
              fieldName: 'Table ID'
            })
          }
      } as Record<string, IFormError>),
    [t, tableId, tableName, isCreateNewVersion]
  );

  const [touches, errors, setTouched] = useFormValidations(currentErrors);

  const handleBlurField = (fieldName: string) => () => {
    setTouched(fieldName, true)();
  };

  const handleChangeFieldValue = (field: string) => (e: any) => {
    let value = e.target?.value;

    if (field === 'tableId') {
      value = value?.toUpperCase();
      if (type !== 'AQ' && !stringValidate(value).isAlphanumeric()) return;

      reactDispatch({
        type: ACTION_TYPES.SET_IS_DUPLICATE_TABLE_ID,
        isDuplicateTableId: false
      });
    }

    if (field === 'tableName') {
      value = value.toUpperCase();
    }

    reactDispatch({
      type: ACTION_TYPES.CHANGE_FORM_VALUE,
      field,
      value
    });
  };

  const handleSetElementList = useCallback(
    (elementList: any[], isChangeElement?: boolean) => {
      reactDispatch({
        type: ACTION_TYPES.SET_ELEMENT_LIST,
        elementList
      });
      if (!isChangeElementList && isChangeElement) {
        reactDispatch({
          type: ACTION_TYPES.SET_IS_CHANGE_ELEMENT_LIST,
          isChangeElementList: true
        });
      }
    },
    [isChangeElementList]
  );

  const handleSetElementListHasError = useCallback(
    (isElementListHasError: boolean) => {
      reactDispatch({
        type: ACTION_TYPES.SET_ELEMENT_LIST_HAS_ERROR,
        isElementListHasError
      });
    },
    []
  );

  const handleSetIsDuplicateTableId = useCallback(
    (isDuplicateTableId: boolean) => {
      reactDispatch({
        type: ACTION_TYPES.SET_IS_DUPLICATE_TABLE_ID,
        isDuplicateTableId
      });
    },
    []
  );

  const handleChangeTableControlParameterAllField = (value: any) => {
    reactDispatch({
      type: ACTION_TYPES.SET_TABLE_CONTROL_PARAMETERS,
      tableControlParameters: { ...value }
    });
  };

  const handleChangeTableControlParameter = (name: string, value: any) => {
    reactDispatch({
      type: ACTION_TYPES.SET_TABLE_CONTROL_PARAMETERS,
      tableControlParameters: { ...tableControlParameters, [name]: value }
    });
  };

  const isFormChanging = useMemo(() => {
    if (initialState.isFormChanging) {
      return (
        initialState.isFormChanging &&
        !isEqual(
          {
            tableId,
            comment,
            tableName,
            tableControlParameters,
            elementList: elementList.map((i: any) => {
              const item = { ...i };
              item.immediateAllocation = booleanToString(
                item.immediateAllocation
              );

              return omit(item, 'rowId');
            })
          },
          {
            tableId: '',
            comment: '',
            tableName: '',
            tableControlParameters:
              initialState?.initialValue?.tableControlParameters,
            elementList: initialState?.initialValue?.elementList?.map(
              (i: any) => {
                const item = { ...i };
                item.immediateAllocation = booleanToString(
                  item.immediateAllocation
                );

                return omit(item, 'rowId');
              }
            )
          }
        )
      );
    }

    return (
      elementList.length > 2 &&
      !isEqual(
        {
          tableId,
          comment,
          tableName,
          tableControlParameters,
          elementList
        },
        {
          tableId: initialState?.tableId,
          comment: initialState?.comment,
          tableName: initialState?.tableName,
          tableControlParameters: initialState?.tableControlParameters,
          elementList: initialState?.elementList
        }
      )
    );
  }, [
    tableName,
    comment,
    tableId,
    initialState,
    tableControlParameters,
    elementList
  ]);

  return {
    handleBlurField,
    errors,
    touches,
    handleChangeFieldValue,
    tableId,
    tableName,
    comment,
    handleSetElementList,
    elementList,
    handleSetElementListHasError,
    selectedTableId,
    selectedVersionId,
    isElementListHasError,
    handleSetIsDuplicateTableId,
    isDuplicateTableId,
    dispatch: reactDispatch,
    handleChangeTableControlParameter,
    tableControlParameters,
    handleChangeTableControlParameterAllField,
    isFormChanging,
    modeledFrom
  };
};
