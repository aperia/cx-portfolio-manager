import { PAGE_SIZE } from 'app/constants/constants';
import { BadgeColorType, OrderBy } from 'app/constants/enums';
import { mapGridExpandCollapse } from 'app/helpers';
import { Button, ColumnType, Grid, useTranslation } from 'app/_libraries/_dls';
import {
  DEFAULT_PAGE_NUMBER,
  DEFAULT_PAGE_SIZE
} from 'app/_libraries/_dls/components/Pagination/constants';
import { isEmpty, orderBy } from 'app/_libraries/_dls/lodash';
import { isFunction } from 'formik';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import {
  formatBadge,
  formatText,
  formatTruncate
} from 'pages/_commons/Utils/formatGridField';
import Paging from 'pages/_commons/Utils/Paging';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { ConfigureParametersValue } from '.';
import { AddNewTableAction } from './AddNewTableModal';
import TableListSubGrid from './TableListSubGrid';

const Summary: React.FC<WorkflowSetupSummaryProps<ConfigureParametersValue>> =
  ({ onEditStep, formValues, stepId, selectedStep, originalStepConfig }) => {
    const [expandedList, setExpandedList] = useState<string[]>([]);
    const [page, setPage] = useState<number>(DEFAULT_PAGE_NUMBER);
    const [pageSize, setPageSize] = useState<number>(PAGE_SIZE[0]);
    const { t } = useTranslation();
    const { width } = useSelectWindowDimension();

    const tables = useMemo(
      () => formValues?.tables || [],
      [formValues?.tables]
    );

    const dataTablePage = tables?.slice(pageSize * (page - 1), page * pageSize);
    const dataSortDefault = orderBy(dataTablePage, 'tableId', OrderBy.ASC);
    const handleChangePage = (page: number) => setPage(page);
    const handleChangePageSize = (pageSize: number) => setPageSize(pageSize);

    useEffect(() => {
      if (stepId !== selectedStep) {
        if (page !== DEFAULT_PAGE_NUMBER) {
          setPage(DEFAULT_PAGE_NUMBER);
        }

        if (pageSize !== PAGE_SIZE[0]) {
          setPageSize(PAGE_SIZE[0]);
        }
        return;
      }

      setExpandedList([]);
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [selectedStep]);

    const methodTypeToText = useCallback((type: WorkflowSetupMethodType) => {
      switch (type) {
        case AddNewTableAction.createNewVersion:
          return 'Version Created';
        case AddNewTableAction.createNewTable:
          return 'Table Created';
        default:
          return 'Table Modeled';
      }
    }, []);

    const columns: ColumnType[] = useMemo(
      () => [
        {
          id: 'tableId',
          Header: t('txt_manage_transaction_level_tlp_config_tq_table_id'),
          accessor: formatText(['tableId']),
          width: 120
        },
        {
          id: 'tableName',
          Header: t('txt_manage_transaction_level_tlp_config_tq_table_name'),
          accessor: formatText(['tableName']),
          width: 120
        },
        {
          id: 'modelOrCreateFrom',
          Header: t('txt_change_interest_rates_model_create'),
          accessor: formatText(['selectedTableId']),
          width: 128
        },
        {
          id: 'comment',
          Header: t('txt_comment_area'),
          accessor: formatTruncate(['comment']),
          width: width > 1280 ? 360 : 300
        },
        {
          id: 'methodType',
          Header: t('txt_manage_penalty_fee_action_taken'),
          accessor: (data, idx) =>
            formatBadge(['tableType'], {
              colorType: BadgeColorType.TableType,
              noBorder: true
            })(
              {
                ...data,
                tableType: methodTypeToText(data.actionTaken)
              },
              idx
            ),
          width: 153
        }
      ],
      [t, methodTypeToText, width]
    );

    const handleEdit = () => {
      isFunction(onEditStep) && onEditStep();
    };

    const handleOnExpand = (dataKeyList: string[]) => {
      setExpandedList(
        isEmpty(dataKeyList) ? [] : [dataKeyList[dataKeyList.length - 1]]
      );
    };

    const total = useMemo(() => tables?.length || 0, [tables]);

    return (
      <div className="position-relative">
        <div className="absolute-top-right mt-n26 mr-n8">
          <Button variant="outline-primary" size="sm" onClick={handleEdit}>
            {t('txt_edit')}
          </Button>
        </div>
        <div className="pt-16">
          <Grid
            columns={columns}
            data={dataSortDefault}
            subRow={(data: any) => {
              return (
                <TableListSubGrid
                  elementList={data?.original?.elementList}
                  tableControlParameters={
                    data?.original?.tableControlParameters
                  }
                  type={originalStepConfig.type}
                />
              );
            }}
            togglable
            toggleButtonConfigList={dataSortDefault.map(
              mapGridExpandCollapse('rowId', t)
            )}
            expandedItemKey={'rowId'}
            expandedList={expandedList}
            dataItemKey="rowId"
            onExpand={handleOnExpand}
          />
          {total > DEFAULT_PAGE_SIZE && (
            <div className="mt-16">
              <Paging
                page={page}
                pageSize={pageSize}
                totalItem={total}
                onChangePage={handleChangePage}
                onChangePageSize={handleChangePageSize}
              />
            </div>
          )}
        </div>
      </div>
    );
  };

export default Summary;
