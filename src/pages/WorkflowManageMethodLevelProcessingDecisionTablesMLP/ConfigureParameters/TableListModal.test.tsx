import userEvent from '@testing-library/user-event';
import { mockUseModalRegistryFnc, renderWithMockStore } from 'app/utils';
import 'app/utils/_mockComponent/mockFailedApiReload';
import 'app/utils/_mockComponent/mockNoDataFound';
import 'app/utils/_mockComponent/mockPaging';
import * as CommonSelectHooks from 'pages/_commons/redux/Common/select-hooks';
import React from 'react';
import * as reactRedux from 'react-redux';
import TableListModal from './TableListModal';
import * as hooks from './useTableListModal';

const mockUseModalRegistry = mockUseModalRegistryFnc();
const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');

const mockUseSelectWindowDimension = jest.spyOn(
  CommonSelectHooks,
  'useSelectWindowDimension'
);

jest.mock('app/_libraries/_dls/components/DropdownList', () => {
  const MockDropDown = ({ onChange }: any) => {
    return (
      <div
        onClick={() =>
          onChange({
            target: {
              value: {
                value: 'value',
                code: 'code'
              }
            }
          })
        }
      >
        MockDropDown
      </div>
    );
  };

  MockDropDown.Item = () => <div>Mock Drop down item</div>;
  return {
    __esModule: true,
    default: MockDropDown
  };
});

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const TableListMockData = [
  {
    tableId: 'LUMEJ598',
    tableName: '8A41RDTB',
    versions: [
      {
        tableId: 'IFAA8XEE',
        tableName: 'WFRKLMZB',
        effectiveDate: '2021-05-31T17:00:00Z',
        status: 'Production',
        comment: 'at tempor accusam',
        tableControlParameters: [],
        decisionElements: []
      },
      {
        tableId: 'K1TIBA4C',
        tableName: 'ZHFMK7K2',
        effectiveDate: '2021-05-28T17:00:00Z',
        status: 'Client Approved',
        comment: 'eu labore congue ea',
        tableControlParameters: [],
        decisionElements: []
      },
      {
        tableId: 'WMAEVM7G',
        tableName: '91IDUIVL',
        effectiveDate: '2021-10-17T17:00:00Z',
        status: 'Validating',
        comment: 'nulla aliquip cum magna iriure ex velit',
        tableControlParameters: [],
        decisionElements: [{ name: 'Name', searchCode: 'searchCode' }]
      }
    ]
  },
  {
    tableId: 'LUMEJ5F',
    tableName: '8A41RDTH',
    versions: [
      {
        tableId: 'IFAA8XEE',
        tableName: 'WFRKLMZB',
        effectiveDate: '2021-05-31T17:00:00Z',
        status: 'Production',
        comment: 'at tempor accusam',
        tableControlParameters: [],
        decisionElements: []
      },
      {
        tableId: 'K1TIBA4DD',
        tableName: 'ZHFMK7EE',
        effectiveDate: '2021-05-28T17:00:00Z',
        status: 'Client Approved',
        comment: 'eu labore congue ea',
        tableControlParameters: [],
        decisionElements: []
      },
      {
        tableId: 'WMAEVMKK',
        tableName: '91IDUIVL',
        effectiveDate: '2021-10-17T17:00:00Z',
        status: 'Validating',
        comment: 'nulla aliquip cum magna iriure ex velit',
        tableControlParameters: [],
        decisionElements: []
      }
    ]
  }
];

const SelectedVersion = {
  tableId: 'LUMEJ598',
  version: {
    tableId: 'IFAA8XEE',
    tableName: 'WFRKLMZB',
    effectiveDate: '2021-05-31T17:00:00Z',
    status: 'Production',
    comment: 'at tempor accusam',
    decisionElements: [{ name: 'Name', searchCode: 'searchCode' }],
    tableControlParameters: [{ name: 'abc', value: '123' }]
  }
};

jest.mock('./ViewModal', () => ({
  __esModule: true,
  default: ({ onCancel, onOK }: any) => {
    return (
      <div>
        <h1>ViewModal_component</h1>
        <button onClick={onCancel}>Close_Modal_View_detail</button>
        <button onClick={onOK}>Go_To_Next_Step</button>
      </div>
    );
  }
}));

jest.mock('app/components/TableCardView', () => ({
  __esModule: true,
  default: ({ onSelectVersion, onViewVersionDetail, onToggle }: any) => {
    return (
      <div>
        <h1>Table Card View</h1>
        <button onClick={() => onSelectVersion(SelectedVersion)}>
          Select Version
        </button>
        <button onClick={() => onViewVersionDetail(SelectedVersion)}>
          View Version
        </button>
        <button onClick={onToggle}>Toggle View</button>
      </div>
    );
  }
}));

HTMLCanvasElement.prototype.getContext = jest.fn();
jest.mock('pages/_commons/Utils/ClearAndResetButton', () => ({
  __esModule: true,
  default: ({ onClearAndReset }: any) => (
    <button onClick={onClearAndReset}>{'Clear_And_Reset'}</button>
  )
}));

const renderComponent = (props: any) =>
  renderWithMockStore(<TableListModal {...props} />, {});

describe('WorkflowManageMethodLevelProcessingDecisionTablesMLP > ConfigureParameters > TableListModal', () => {
  const mockDispatch = jest.fn();

  const id = 'id';
  const show = true;
  const handleClose = jest.fn();
  const selectedVersion = null;
  const handleSetExpandedList = () => jest.fn();
  const handleSelectVersion = jest.fn();
  const expandedList = {};
  const handleOpenConfigModal = jest.fn();
  const isCreateNewVersion = false;
  const handleSetIsCreateNewVersion = jest.fn();
  const handleSetDraftNewTable = jest.fn();
  const draftNewTable = null;
  const handleSetShowVersionDetail = jest.fn();
  const handleChangeOrderBy = jest.fn();
  const handleGoToNextStep = jest.fn();
  const defaultProps = {
    id,
    show,
    handleClose,
    selectedVersion,
    handleSetExpandedList,
    handleSelectVersion,
    expandedList,
    handleOpenConfigModal,
    isCreateNewVersion,
    handleSetIsCreateNewVersion,
    handleSetDraftNewTable,
    draftNewTable,
    handleGoToNextStep
  };

  beforeEach(() => {
    mockUseModalRegistry.mockImplementation(() => [true, false]);
    mockUseDispatch.mockImplementation(() => mockDispatch);
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1920,
      height: 1080
    }));
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
    mockUseModalRegistry.mockClear();
    mockUseSelectWindowDimension.mockClear();
  });

  it('Should render Table List Modal with no data found', async () => {
    jest.spyOn(hooks, 'useTableListModal').mockImplementation(() => {
      return {
        total: 0,
        searchValue: 'abc'
      } as any;
    });
    const wrapper = await renderComponent(defaultProps);
    expect(
      wrapper.getByText('txt_manage_transaction_level_tlp_config_tq_tableList')
    ).toBeInTheDocument();

    const noDataFound = wrapper.getByText('No data found');
    expect(noDataFound).toBeInTheDocument();
  });

  it('Should render Table List Modal with APi fail component', async () => {
    jest.spyOn(hooks, 'useTableListModal').mockImplementation(() => {
      return {
        total: 0,
        searchValue: 'abc',
        error: {}
      } as any;
    });
    const wrapper = await renderComponent(defaultProps);
    const apiFailed = wrapper.getByText('Data load unsuccessful');
    expect(apiFailed).toBeInTheDocument();
  });

  it('Should render Table List', async () => {
    jest.spyOn(hooks, 'useTableListModal').mockImplementation(() => {
      return {
        total: 2,
        searchValue: 'abc',
        tables: TableListMockData,
        handleSetShowVersionDetail
      } as any;
    });
    const wrapper = await renderComponent({
      ...defaultProps,
      selectedVersion: SelectedVersion,
      isCreateNewVersion: true
    });

    const btnClose = wrapper.getByText('txt_cancel');
    userEvent.click(btnClose);

    const selectVersionBtnList = wrapper.getAllByText('Select Version');
    userEvent.click(selectVersionBtnList[0]);

    const viewVersionBtnList = wrapper.getAllByText('View Version');
    userEvent.click(viewVersionBtnList[0]);

    const toggleVersionBtnList = wrapper.getAllByText('Toggle View');
    userEvent.click(toggleVersionBtnList[0]);
  });

  it('Should Show modal version detail', async () => {
    jest.spyOn(hooks, 'useTableListModal').mockImplementation(() => {
      return {
        total: 2,
        searchValue: '',
        tables: TableListMockData,
        isShowVersionDetail: true,
        handleSetShowVersionDetail,
        handleGoToNextStep
      } as any;
    });
    const wrapper = await renderComponent({
      ...defaultProps,
      selectedVersion: SelectedVersion,
      isCreateNewVersion: true,
      draftNewTable: {},
      type: 'ST'
    });
    const continueBtn = wrapper.getAllByText('txt_continue');
    userEvent.click(continueBtn[1]);

    const btnClose = wrapper.getByText('txt_cancel');
    userEvent.click(btnClose);
  });

  it('Should render with draftnew table', async () => {
    jest.spyOn(hooks, 'useTableListModal').mockImplementation(() => {
      return {
        total: 2,
        searchValue: '',
        tables: TableListMockData,
        isShowVersionDetail: true,
        handleSetShowVersionDetail,
        handleChangeOrderBy,
        handleGoToNextStep
      } as any;
    });
    const wrapper = await renderComponent({
      ...defaultProps,
      selectedVersion: SelectedVersion,
      isCreateNewVersion: true,
      draftNewTable: { selectedVersionId: 'IFAA8XEE' },
      type: 'CA'
    });
    const continueBtn = wrapper.getAllByText('txt_continue');
    userEvent.click(continueBtn[1]);
  });
  it('Should render with draftnew table by type CA', async () => {
    jest.spyOn(hooks, 'useTableListModal').mockImplementation(() => {
      return {
        total: 2,
        searchValue: '',
        tables: TableListMockData,
        isShowVersionDetail: true,
        handleSetShowVersionDetail,
        handleChangeOrderBy,
        handleGoToNextStep,
        versionDetail: {
          tableId: 'id',
          version: [{ id: '12' }]
        }
      } as any;
    });
    const wrapper = await renderComponent({
      ...defaultProps,
      selectedVersion: SelectedVersion,
      isCreateNewVersion: false,
      draftNewTable: { selectedVersionId: 'IFAA8XEE' },
      type: 'CA'
    });
    const continueBtn = wrapper.getByText('Go_To_Next_Step');
    const cancelBtn = wrapper.getByText('Close_Modal_View_detail');
    userEvent.click(continueBtn);
    userEvent.click(cancelBtn);

    const mockDropDown = wrapper.getByText('MockDropDown');
    userEvent.click(mockDropDown);
  });
});
