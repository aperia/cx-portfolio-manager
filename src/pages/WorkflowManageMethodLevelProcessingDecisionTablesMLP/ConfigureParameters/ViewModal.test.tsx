import { fireEvent, render, RenderResult } from '@testing-library/react';
import { mockUseModalRegistryFnc } from 'app/utils';
import 'app/utils/_mockComponent/mockNoDataFound';
import 'app/utils/_mockComponent/mockPaging';
import * as mockMetaData from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageMethodLevelProcessingDecisionMLP';
import React from 'react';
import * as reactRedux from 'react-redux';
import ViewModal from './ViewModal';

const mockOnTruncateParams = jest.fn();
const mockMeta = jest.spyOn(
  mockMetaData,
  'useSelectElementMetadataManageMethodLevelProcessingMLP'
);

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    }),
    TruncateText: ({ onTruncate, children }: any) => (
      <div>
        {children}
        <button
          onFocus={() => onTruncate && onTruncate(...mockOnTruncateParams())}
        >
          On Truncate
        </button>
      </div>
    )
  };
});

jest.mock('./TableParameterInfo', () => {
  return {
    __esModule: true,
    default: () => {
      return (
        <div>
          <div>TableParameterInfo_component</div>
        </div>
      );
    }
  };
});

const mockUseModalRegistry = mockUseModalRegistryFnc();
const mockUseSelector = jest.spyOn(reactRedux, 'useSelector');

const defaultProps = {
  tableId: '123',
  data: {},
  onCancel: jest.fn(),
  onOK: jest.fn()
};

const renderModal = (props: any): RenderResult => {
  return render(
    <div>
      <ViewModal {...props} />
    </div>
  );
};

const mockDecisionElements = [
  {
    name: 'APPLICATION SCORE',
    searchCode: 'E',
    immediateAllocation: '2342',
    value: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
  },
  {
    name: 'APPLICATION SCORE',
    searchCode: 'E',
    immediateAllocation: '3242342',
    value: []
  },
  {
    name: 'APPLICATION SCORE',
    searchCode: 'E',
    immediateAllocation: '',
    value: []
  },
  {
    name: 'APPLICATION SCORE',
    searchCode: 'E',
    immediateAllocation: '',
    value: []
  },
  {
    name: 'APPLICATION SCORE',
    searchCode: 'E',
    immediateAllocation: '',
    value: []
  },
  {
    name: 'APPLICATION SCORE',
    searchCode: 'E',
    immediateAllocation: '',
    value: []
  },
  {
    name: 'APPLICATION SCORE',
    searchCode: 'E',
    immediateAllocation: '',
    value: []
  },
  {
    name: 'APPLICATION SCORE',
    searchCode: 'E',
    immediateAllocation: '',
    value: []
  },
  {
    name: 'APPLICATION SCORE',
    searchCode: 'E',
    immediateAllocation: '',
    value: []
  },
  {
    name: 'APPLICATION SCORE',
    searchCode: 'E',
    immediateAllocation: '',
    value: []
  },
  {
    name: 'APPLICATION SCORE',
    searchCode: 'E',
    immediateAllocation: '',
    value: []
  },
  {
    name: 'RESULT ID',
    searchCode: '',
    immediateAllocation: '',
    value: []
  }
];

const { getComputedStyle } = window;
window.getComputedStyle = (elt, pseudoElt) => getComputedStyle(elt, pseudoElt);

describe('WorkflowManageAccountLevel > ManageAccountLevelCAStep > ViewModal', () => {
  beforeEach(() => {
    mockUseSelector.mockImplementation(selector =>
      selector({
        caches: {}
      })
    );
    mockMeta.mockReturnValue({
      searchCode: [
        {
          value: 'E',
          description: 'E - Exact'
        },
        {
          value: 'R',
          description: 'R - Ranger'
        }
      ],
      allocationBeforeAfterCycle: [],
      caAllocationFlag: [],
      aqAllocationFlag: [],
      allocationInterval: [],
      changeCode: [],
      changeInTermsMethodFlag: [],
      serviceSubjectSectionAreas: []
    });
    mockUseModalRegistry.mockImplementation(() => [true, false]);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Should render modal with no data', () => {
    mockOnTruncateParams.mockReturnValue([false, false]);
    renderModal({
      ...defaultProps,
      data: { status: '', tableControlParameters: [] }
    });
  });

  it('Should render modal with data', () => {
    mockOnTruncateParams.mockReturnValue([false, true]);
    const wrapper = renderModal({
      ...defaultProps,
      data: {
        decisionElements: mockDecisionElements,
        status: '',
        tableControlParameters: [{ abc: 'abc' }],
        comment: 'comment'
      }
    });
    fireEvent.click(
      wrapper.getByText('txt_manage_account_level_table_decision_elements')
    );
    fireEvent.click(
      wrapper.baseElement.querySelector(
        'button[class="change-page-number"]'
      ) as Element
    );
    fireEvent.click(
      wrapper.baseElement.querySelector(
        'button[class="change-page-size"]'
      ) as Element
    );
    wrapper.queryAllByText(/On Truncate/).forEach(element => {
      fireEvent.focus(element!);
    });
  });
  it('Should render modal with data', () => {
    mockOnTruncateParams.mockReturnValue([true, false]);
    const wrapper = renderModal({
      ...defaultProps,
      data: {
        decisionElements: mockDecisionElements,
        status: '',
        tableControlParameters: [{ abc: 'abc' }],
        comment: 'comment'
      }
    });
    wrapper.queryAllByText(/On Truncate/).forEach(element => {
      fireEvent.focus(element!);
    });
  });
  it('Should render modal with data', () => {
    mockOnTruncateParams.mockReturnValue([true, true]);
    const wrapper = renderModal({
      ...defaultProps,
      data: {
        decisionElements: mockDecisionElements,
        status: '',
        tableControlParameters: [{ abc: 'abc' }],
        comment: 'comment'
      }
    });
    wrapper.queryAllByText(/On Truncate/).forEach(element => {
      fireEvent.focus(element!);
    });
  });
});
