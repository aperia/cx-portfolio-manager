import { TableFieldParameterEnum } from 'app/constants/enums';
import {
  confirmEditWhenChangeEffectToUploadProps,
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { deepEqual } from 'app/helpers';
import { useUnsavedChangeRegistry } from 'app/hooks';
import { InlineMessage, useTranslation } from 'app/_libraries/_dls';
import { StateFile } from 'app/_libraries/_dls/components/Upload/File';
import { STATUS } from 'app/_libraries/_dls/components/Upload/helper';
import { isEqual } from 'app/_libraries/_dls/lodash';
import { isEmpty, omit } from 'lodash';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, { useEffect, useMemo, useRef, useState } from 'react';
import ActionButtons from './ActionButtons';
import AddNewTableModal from './AddNewTableModal';
import isEqualFormValues from './isEqualFormValues';
import parseFormValues from './parseFormValues';
import RemoveTableModal from './RemoveTableModal';
import Summary from './Summary';
import TableList from './TableList';
import TableListModal from './TableListModal';
import {
  CONFIG_ACTIONS_MODAL,
  EXPANDED_LIST,
  useConfigAction
} from './useConfigActions';

export interface ConfigureParametersValue {
  isValid?: boolean;
  tables?: (any & { rowId?: string })[];
}
interface IDependencies {
  files?: StateFile[];
}

const ConfigureParameters: React.FC<
  WorkflowSetupProps<ConfigureParametersValue, IDependencies> & {
    clearFormName: string;
  }
> = ({
  savedAt,
  isStuck,
  formValues,
  setFormValues,
  clearFormName,
  clearFormValues,
  selectedStep,
  dependencies,
  type = 'AQ'
}) => {
  const keepRef = useRef({ setFormValues });
  const { t } = useTranslation();
  keepRef.current.setFormValues = setFormValues;
  const [initialValues, setInitialValues] = useState(formValues);

  const {
    openedModal,
    handleOpenConfigModal,
    selectedVersion,
    handleSetExpandedList,
    handleSetSelectedVersion,
    expandedList,
    isCreateNewVersion,
    handleSetIsCreateNewVersion,
    handleSetDraftNewTable,
    draftNewTable,
    handleAddNewTable,
    tableList,
    handleSetSelectedTable,
    selectedTable,
    handleEditTable,
    handleDeleteTable,
    handleCheckDuplicateTableId,
    reduxDispatch,
    handleSetTable,
    handleChangeRowIdAdded,
    rowIdAdded
  } = useConfigAction(selectedStep, type);

  const unSaveChangeNames = useMemo(() => {
    switch (type) {
      case 'CA':
        return UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__LEVEL_PROCESSING_CA;
      default:
        return UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__LEVEL_PROCESSING_AQ;
    }
  }, [type]);

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName: unSaveChangeNames,
      priority: 1
    },
    [
      !deepEqual(formValues.tables, initialValues.tables, [
        'configurable',
        'moreInfo',
        'searchCodeText'
      ])
    ]
  );

  useEffect(() => {
    setInitialValues(formValues);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [savedAt]);

  const handleResetUploadStep = () => {
    const file = dependencies?.files?.[0];
    const attachmentId = file?.idx;
    const isValid = file?.status === STATUS.valid;

    clearFormValues(clearFormName);
    isValid &&
      reduxDispatch(actionsWorkflowSetup.deleteTemplateFile({ attachmentId }));
  };

  const unSaveChangeNamesEdited = useMemo(() => {
    switch (type) {
      case 'CA':
        return UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__LEVEL_PROCESSING_CA_CONFIRM_EDIT;
      default:
        return UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__LEVEL_PROCESSING_AQ_CONFIRM_EDIT;
    }
  }, [type]);

  const isConfirmUpload = useMemo(() => {
    const tablesInfo = formValues.tables?.map(i => ({
      tableId: i.tableId,
      elementList: i.elementList.map((i: Record<string, any>) =>
        omit(i, 'rowId', 'moreInfo', 'searchCodeText', 'configurable')
      ),
      ...(type === 'CA'
        ? {}
        : {
            tableControlParameters: {
              [TableFieldParameterEnum.ChangeInTermsMethodFlag]:
                i.tableControlParameters[
                  TableFieldParameterEnum.ChangeInTermsMethodFlag
                ]
            }
          })
    }));
    const tablesInit = initialValues.tables?.map(i => ({
      tableId: i.tableId,
      elementList: i.elementList.map((i: Record<string, any>) =>
        omit(i, 'rowId', 'moreInfo', 'searchCodeText', 'configurable')
      ),
      ...(type === 'CA'
        ? {}
        : {
            tableControlParameters: {
              [TableFieldParameterEnum.ChangeInTermsMethodFlag]:
                i.tableControlParameters[
                  TableFieldParameterEnum.ChangeInTermsMethodFlag
                ]
            }
          })
    }));
    return !isEqual(tablesInfo, tablesInit);
  }, [formValues, initialValues, type]);

  useUnsavedChangeRegistry(
    {
      ...confirmEditWhenChangeEffectToUploadProps,
      formName: unSaveChangeNamesEdited,
      priority: 1
    },
    [!isEmpty(dependencies?.files) && isConfirmUpload],
    handleResetUploadStep
  );

  useEffect(() => {
    const isValid = (formValues?.tables?.length || 0) > 0;
    if (formValues.isValid === isValid) return;

    keepRef.current.setFormValues({ isValid });
  }, [formValues]);

  useEffect(() => {
    keepRef.current.setFormValues({ tables: tableList });
  }, [tableList]);

  useEffect(() => {
    handleSetTable(formValues.tables);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div>
      <div className="mt-24">
        {isStuck && (
          <InlineMessage className="mb-24 mt-0" variant="danger" withIcon>
            {t('txt_step_stuck_move_forward_message')}
          </InlineMessage>
        )}
        <ActionButtons
          small={!isEmpty(tableList)}
          title="Table List"
          onClickAddNewTable={() =>
            handleOpenConfigModal(CONFIG_ACTIONS_MODAL.addNewTableModal)
          }
          onClickTableToModel={() =>
            handleOpenConfigModal(CONFIG_ACTIONS_MODAL.tableListModal)
          }
          onClickCreateNewVersion={() => {
            handleOpenConfigModal(CONFIG_ACTIONS_MODAL.tableListModal);
            handleSetIsCreateNewVersion(true);
          }}
        />
      </div>
      {tableList && tableList.length > 0 && (
        <TableList
          tableList={tableList}
          selectedStep={selectedStep}
          handleSetSelectedTable={handleSetSelectedTable}
          handleOpenConfigModal={handleOpenConfigModal}
          handleSetExpandedList={handleSetExpandedList}
          expandedList={expandedList[EXPANDED_LIST.tableListIndex]}
          type={type}
          handleChangeRowIdAdded={handleChangeRowIdAdded}
          rowIdAdded={rowIdAdded}
        />
      )}
      {openedModal === CONFIG_ACTIONS_MODAL.deleteTableModal && (
        <RemoveTableModal
          id="removeMethod"
          show
          onOk={() => handleDeleteTable(selectedTable?.rowId)}
          onClose={() => {
            handleOpenConfigModal(null);
            handleSetSelectedTable(null);
          }}
        />
      )}
      {openedModal === CONFIG_ACTIONS_MODAL.addNewTableModal && (
        <AddNewTableModal
          id="addNewMethod"
          show
          handleClose={() => handleOpenConfigModal(null)}
          selectedVersion={selectedVersion}
          handleOpenConfigModal={handleOpenConfigModal}
          handleSelectVersion={handleSetSelectedVersion}
          handleSetDraftNewTable={handleSetDraftNewTable}
          draftNewTable={draftNewTable}
          isCreateNewVersion={isCreateNewVersion}
          handleAddNewTable={handleAddNewTable}
          handleSetIsCreateNewVersion={handleSetIsCreateNewVersion}
          selectedTable={selectedTable}
          handleEditTable={handleEditTable}
          handleSetSelectedTable={handleSetSelectedTable}
          handleSetExpandedList={handleSetExpandedList}
          handleCheckDuplicateTableId={handleCheckDuplicateTableId}
          type={type}
        />
      )}
      {openedModal === CONFIG_ACTIONS_MODAL.tableListModal && (
        <TableListModal
          id="editMethodFromModel"
          show
          handleClose={() => {
            handleOpenConfigModal(null);
            if (!isEmpty(draftNewTable)) handleSetDraftNewTable(null);
          }}
          selectedVersion={selectedVersion}
          handleSetExpandedList={handleSetExpandedList}
          handleSelectVersion={handleSetSelectedVersion}
          expandedList={expandedList[EXPANDED_LIST.tableListModal]}
          handleOpenConfigModal={handleOpenConfigModal}
          isCreateNewVersion={isCreateNewVersion}
          handleSetIsCreateNewVersion={handleSetIsCreateNewVersion}
          handleSetDraftNewTable={handleSetDraftNewTable}
          draftNewTable={draftNewTable}
          type={type}
        />
      )}
    </div>
  );
};

const ExtraStaticConfigureParameters =
  ConfigureParameters as WorkflowSetupStaticProp;

ExtraStaticConfigureParameters.summaryComponent = Summary;
ExtraStaticConfigureParameters.defaultValues = {
  isValid: false,
  tables: []
};
ExtraStaticConfigureParameters.parseFormValues = parseFormValues;
ExtraStaticConfigureParameters.isEqualFormValues = isEqualFormValues;

export default ExtraStaticConfigureParameters;
