export const createTableText = (type: string) => {
  switch (type) {
    case 'AQ':
      return 'txt_manage_method_level_processing_aq_create_method';
    case 'CA':
      return 'txt_manage_method_level_processing_ca_create_method';
  }
};

export const chooseTableText = (type: string) => {
  switch (type) {
    case 'AQ':
      return 'txt_manage_method_level_processing_aq_choose_method';
    case 'CA':
      return 'txt_manage_method_level_processing_ca_choose_method';
  }
};

export const maxLength = (data: any) =>
  Array.from({ length: data?.length }, (_, i) => i);

export const mappingData = (data: DecisionType[]) =>
  maxLength(data[0]?.value).reduce((current: any, value) => {
    const result = data?.reduce((currentMap: any, valueMap) => {
      return { ...currentMap, [valueMap.name]: valueMap.value[value] };
    }, {});

    return [...current, result];
  }, []);
