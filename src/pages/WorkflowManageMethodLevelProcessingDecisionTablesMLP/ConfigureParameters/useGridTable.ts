import { useState } from 'react';

const useGridTable = () => {
  const [searchValue, setSearchValue] = useState('');

  const handleSearchValue = (searchValue: string) => {
    setSearchValue(searchValue);
  };

  return {
    searchValue,
    handleSearchValue
  };
};

export default useGridTable;
