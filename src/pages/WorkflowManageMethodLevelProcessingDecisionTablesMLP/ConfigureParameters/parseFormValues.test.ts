import mockDate from 'app/_libraries/_dls/test-utils/mockDate';
import parseFormValues from './parseFormValues';

const todayString = '02/23/2022';
const today = new Date(todayString);
describe('pages > WorkflowManageMethodLevelProcessingDecisionTablesMLP > ConfigureParameters > parseFormValues', () => {
  beforeEach(() => {
    mockDate.set(today);
  });
  it('should return undefined with methodList is undefined', () => {
    parseFormValues(
      { data: { workflowSetupData: [{ id: '1' }] } } as any,
      '1',
      jest.fn()
    );
  });

  it('should return undefined with stepInfo is undefined', () => {
    const response = parseFormValues(
      {
        data: {
          workflowSetupData: [
            {
              id: '2'
            }
          ],
          configurations: {
            tableList: []
          }
        }
      } as any,
      '1',
      jest.fn()
    );
    expect(response).toEqual(undefined);
  });

  it('should return valid data case CA', () => {
    parseFormValues(
      {
        data: {
          workflowSetupData: [
            {
              id: 'configMlpCa'
            }
          ],
          configurations: {
            tableList: [
              {
                id: '123',
                name: '123',
                tableType: 'CA',
                tableControlParameters: [{ value: '123' }]
              }
            ]
          }
        }
      } as any,
      'configMlpCa',
      jest.fn()
    );
  });

  it('should return valid data case AQ', () => {
    parseFormValues(
      {
        data: {
          workflowSetupData: [
            {
              id: 'configMlpSt'
            }
          ],
          configurations: {
            tableList: [
              {
                id: '123',
                name: '123',
                tableType: 'CA',
                tableControlParameters: [{ value: '123' }]
              }
            ]
          }
        }
      } as any,
      'configMlpSt',
      jest.fn()
    );
  });
});
