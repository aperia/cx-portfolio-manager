import { render, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { mockUseModalRegistryFnc } from 'app/utils';
import 'app/utils/_mockComponent/mockFailedApiReload';
import 'app/utils/_mockComponent/mockNoDataFound';
import 'app/utils/_mockComponent/mockPaging';
import * as CommonSelectHooks from 'pages/_commons/redux/Common/select-hooks';
import React from 'react';
import * as reactRedux from 'react-redux';
import TableListModal from './TableListModal';
import * as hooks from './useTableListModal';

const mockUseModalRegistry = mockUseModalRegistryFnc();
const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');

const mockUseSelectWindowDimension = jest.spyOn(
  CommonSelectHooks,
  'useSelectWindowDimension'
);

jest.mock('app/_libraries/_dls/components/DropdownList', () => {
  const MockDropDown = ({ onChange }: any) => {
    return (
      <div
        onClick={() =>
          onChange({
            target: {
              value: {
                value: 'value',
                code: 'code'
              }
            }
          })
        }
      >
        MockDropDown
      </div>
    );
  };

  MockDropDown.Item = () => <div>Mock Drop down item</div>;
  return {
    __esModule: true,
    default: MockDropDown
  };
});

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

jest.mock(
  'pages/WorkflowManageTransactionLevelProcessingDecisionTablesTLP/ConfigureParameters/VersionCardView',
  () => ({
    __esModule: true,
    default: ({ onSelectVersion, onViewVersionDetail }: any) => {
      return (
        <div>
          <h1>Table Card View</h1>
          <button onClick={() => onSelectVersion('LUMEJ598')}>
            Select Version
          </button>
          <button onClick={() => onViewVersionDetail('LUMEJ598', true)}>
            View Version
          </button>
        </div>
      );
    }
  })
);

jest.mock('./ViewModal', () => ({
  __esModule: true,
  default: ({ onCancel, onOK }: any) => {
    return (
      <div>
        <h1>ViewModal_component</h1>
        <button onClick={onCancel}>Close_Modal_View_detail</button>
        <button onClick={onOK}>Go_To_Next_Step</button>
      </div>
    );
  }
}));

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <TableListModal {...props} />
    </div>
  );
};

const selectedVersion = {
  tableId: 'LUMEJ598',
  version: {
    tableId: 'IFAA8XEE',
    tableName: 'WFRKLMZB',
    effectiveDate: '2021-05-31T17:00:00Z',
    status: 'Production',
    comment: 'at tempor accusam',
    decisionElements: [{ name: 'Name', searchCode: 'searchCode' }],
    tableControlParameters: [{ name: 'abc', value: '123' }]
  }
};

const defaultProps = {
  id: '1',
  show: true,
  handleClose: jest.fn(),
  selectedVersion,
  handleSelectVersion: jest.fn(),
  draftNewTable: null,
  handleSetDraftNewTable: jest.fn(),
  handleOpenConfigModal: jest.fn()
};

const defaultStore = {
  tables: [
    {
      comment: 'takimata praesent est tincidunt',
      decisionElements: [
        {
          immediateAllocation: '',
          name: 'METHOD OVERRIDE',
          searchCode: 'E',
          value: ['1', '2', '3']
        }
      ],
      effectiveDate: '2022-03-22T17:00:00Z',
      status: 'Previous',
      tableControlParameters: [],
      tableId: 'PF7ZZX09',
      tableName: 'ZUBR8BK6',
      versions: []
    }
  ],
  total: 30,
  page: 1,
  handleChangePage: jest.fn(),
  handleChangePageSize: jest.fn(),
  handleChangeSortBy: jest.fn(),
  handleChangeOrderBy: jest.fn(),
  handleGetTableList: jest.fn(),
  handleClearAndReset: jest.fn(),
  handleSetShowVersionDetail: jest.fn(),
  versionDetail: null,
  dataFilter: {
    orderBy: ['desc'],
    page: 1,
    searchValue: '',
    sortBy: ['effectiveDate']
  }
};

describe('WorkflowManageMethodLevelProcessingDecisionTablesMLP > ConfigureParameters > TableListModal', () => {
  beforeEach(() => {
    mockUseModalRegistry.mockImplementation(() => [true, false]);
    mockUseDispatch.mockImplementation(() => jest.fn());
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1920,
      height: 1080
    }));
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
    mockUseModalRegistry.mockClear();
    mockUseSelectWindowDimension.mockClear();
  });

  it('Should render Table List Modal with no data found', () => {
    jest.spyOn(hooks, 'useTableListModal').mockImplementation(() => {
      return {
        total: 0,
        searchValue: 'abc'
      } as any;
    });
    renderComponent(defaultProps);
  });

  it('Should render Table List Modal with no error', () => {
    jest.spyOn(hooks, 'useTableListModal').mockImplementation(() => {
      return {
        total: 0,
        searchValue: 'abc',
        error: true
      } as any;
    });
    renderComponent(defaultProps);
  });

  it('Should render Table List Modal with version detail', () => {
    jest.spyOn(hooks, 'useTableListModal').mockImplementation(() => {
      return {
        ...defaultStore,
        versionDetail: {
          tableId: 'id',
          selectedVersionId: 'id',
          version: [
            {
              id: '123'
            }
          ]
        }
      } as any;
    });
    const wrapper = renderComponent({
      ...defaultProps,
      draftNewTable: {
        selectedVersionId: 'id'
      }
    });
    userEvent.click(wrapper.getByText('Close_Modal_View_detail'));
    userEvent.click(wrapper.getByText('Go_To_Next_Step'));
  });

  it('Should render Table List', () => {
    jest.spyOn(hooks, 'useTableListModal').mockImplementation(() => {
      return defaultStore as any;
    });
    const wrapper = renderComponent(defaultProps);

    userEvent.click(wrapper.getByText('txt_cancel'));
    userEvent.click(wrapper.getByText('txt_continue'));
    userEvent.click(wrapper.getByText('Select Version'));
    userEvent.click(wrapper.getByText('View Version'));
  });
});
