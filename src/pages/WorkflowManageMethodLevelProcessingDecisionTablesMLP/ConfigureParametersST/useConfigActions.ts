import { MethodFieldParameterEnum } from 'app/constants/enums';
import get from 'lodash.get';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import { useCallback, useEffect, useReducer } from 'react';
import { useDispatch } from 'react-redux';

interface ConfigActionState {
  openedModal: null | CONFIG_ACTIONS_MODAL;
  selectedVersion?: any;
  selectedTable?: any;
  draftNewTable?: Record<string, any> | null;
  tableList: any[];
}

export enum CONFIG_ACTIONS_MODAL {
  addNewTableModal = 'addNewTableModal',
  tableListModal = 'tableListModal',
  deleteTableModal = 'deleteTableModal'
}

const ACTION_TYPE = {
  SET_OPENED_MODAL: 'SET_OPENED_MODAL',
  SET_SELECTED_VERSION: 'SET_SELECTED_VERSION',
  SET_SELECTED_TABLE: 'SET_SELECTED_TABLE',
  SET_DRAFT_NEW_TABLE: 'SET_DRAFT_NEW_TABLE',
  SET_TABLE_LIST: 'SET_TABLE_LIST'
};

export const INITIAL_STATE: ConfigActionState = {
  openedModal: null,
  draftNewTable: null,
  tableList: []
};

const configActionsReducer = (state: ConfigActionState, action: any) => {
  switch (action.type) {
    case ACTION_TYPE.SET_OPENED_MODAL:
      return {
        ...state,
        openedModal: action.modalName
      };
    case ACTION_TYPE.SET_SELECTED_VERSION:
      return {
        ...state,
        selectedVersion: action.selectedVersion
      };
    case ACTION_TYPE.SET_SELECTED_TABLE:
      return {
        ...state,
        selectedTable: action.selectedTable
      };
    case ACTION_TYPE.SET_DRAFT_NEW_TABLE:
      return {
        ...state,
        draftNewTable: action.draftNewTable
      };
    case ACTION_TYPE.SET_TABLE_LIST:
      return {
        ...state,
        tableList: action.tableList
      };
    default:
      return state;
  }
};

export const useConfigAction = (selectedStep: string) => {
  const [state, dispatch] = useReducer(configActionsReducer, INITIAL_STATE);
  const {
    openedModal,
    selectedVersion,
    selectedTable,
    draftNewTable,
    tableList
  } = state;

  const reduxDispatch = useDispatch();

  useEffect(() => {
    reduxDispatch(
      actionsWorkflowSetup.getWorkflowMetadata([
        MethodFieldParameterEnum.SearchCode
      ])
    );
  }, [reduxDispatch]);

  const handleOpenConfigModal = useCallback(
    (modalName: CONFIG_ACTIONS_MODAL | null) =>
      dispatch({
        type: ACTION_TYPE.SET_OPENED_MODAL,
        modalName
      }),
    []
  );

  const handleCheckDuplicateTableId = useCallback(
    (tableId: string) => {
      const selectedTableId = get(selectedVersion, 'tableId', '');
      const versionTableId = get(selectedVersion, ['version', 'tableId'], '');
      const tableListIds = tableList.map((table: any) => table?.tableId);
      const arrayCheck = selectedVersion
        ? [...tableListIds, selectedTableId, versionTableId]
        : tableListIds;
      return arrayCheck.some(
        (existedTableId: string) => tableId === existedTableId
      );
    },
    [selectedVersion, tableList]
  );

  const handleSetSelectedVersion = useCallback((selectedVersion: any) => {
    dispatch({
      type: ACTION_TYPE.SET_SELECTED_VERSION,
      selectedVersion
    });
  }, []);

  const handleSetDraftNewTable = useCallback(
    (draftNewTable: Record<string, any> | null) => {
      dispatch({
        type: ACTION_TYPE.SET_DRAFT_NEW_TABLE,
        draftNewTable
      });
    },
    []
  );

  const handleAddNewTable = useCallback(
    (table: any) => {
      dispatch({
        type: ACTION_TYPE.SET_TABLE_LIST,
        tableList: [...tableList, table]
      });
    },
    [tableList]
  );

  const handleSetTable = useCallback((table: any) => {
    dispatch({
      type: ACTION_TYPE.SET_TABLE_LIST,
      tableList: table
    });
  }, []);

  const handleSetSelectedTable = useCallback((selectedTable: any) => {
    dispatch({
      type: ACTION_TYPE.SET_SELECTED_TABLE,
      selectedTable
    });
  }, []);

  const handleEditTable = useCallback(
    (newTable: any) => {
      const tableIndex = tableList.findIndex(
        (table: any) => table.rowId === newTable.rowId
      );
      const newTableList = [...tableList];
      newTableList[tableIndex] = newTable;
      dispatch({
        type: ACTION_TYPE.SET_TABLE_LIST,
        tableList: newTableList
      });
    },
    [tableList]
  );

  const handleDeleteTable = useCallback(
    (rowId: string) => {
      const newTableList = tableList.filter(
        (table: any) => table.rowId !== rowId
      );
      dispatch({
        type: ACTION_TYPE.SET_TABLE_LIST,
        tableList: newTableList
      });
      dispatch({
        type: ACTION_TYPE.SET_SELECTED_TABLE,
        selectedTable: undefined
      });
    },
    [tableList]
  );

  return {
    handleOpenConfigModal,
    openedModal,
    selectedTable,
    selectedVersion,
    handleSetSelectedVersion,
    handleSetDraftNewTable,
    draftNewTable,
    handleAddNewTable,
    tableList,
    handleSetSelectedTable,
    handleEditTable,
    handleDeleteTable,
    handleCheckDuplicateTableId,
    dispatch,
    reduxDispatch,
    handleSetTable
  };
};
