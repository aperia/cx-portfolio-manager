import { ORDER_BY_LIST, PAGE_SIZE } from 'app/constants/constants';
import { OrderBy } from 'app/constants/enums';
import { matchSearchValue } from 'app/helpers';
import { DEFAULT_PAGE_NUMBER } from 'app/_libraries/_dls/components/Pagination/constants';
import _orderBy from 'lodash.orderby';
import {
  actionsWorkflowSetup,
  useSelectTableListData
} from 'pages/_commons/redux/WorkflowSetup';
import { useCallback, useEffect, useMemo, useReducer } from 'react';
import { useDispatch } from 'react-redux';

interface ITableListStateProps {
  page: number;
  pageSize: number;
  searchValue: string;
  orderBy: OrderByType[];
  sortBy: string[];
  versionDetail: any;
}

const TABLE_LIST_INITIAL_STATE = {
  page: DEFAULT_PAGE_NUMBER,
  pageSize: PAGE_SIZE[0],
  searchValue: '',
  orderBy: [OrderBy.DESC],
  sortBy: ['effectiveDate'],
  versionDetail: null
} as ITableListStateProps;

const ACTION_TYPES = {
  CHANGE_PAGE: 'CHANGE_PAGE',
  CHANGE_PAGE_SIZE: 'CHANGE_PAGE_SIZE',
  CHANGE_SEARCH_VALUE: 'CHANGE_SEARCH_VALUE',
  CHANGE_ORDER_BY: 'CHANGE_ORDER_BY',
  CHANGE_SORT_BY: 'CHANGE_SORT_BY',
  SET_SHOW_VERSION_DETAIL: 'SET_SHOW_VERSION_DETAIL'
};

const tableListReducer = (state: ITableListStateProps, action: any) => {
  switch (action.type) {
    case ACTION_TYPES.CHANGE_PAGE:
      return { ...state, page: action.page };
    case ACTION_TYPES.CHANGE_PAGE_SIZE:
      return { ...state, pageSize: action.pageSize };
    case ACTION_TYPES.CHANGE_SEARCH_VALUE:
      return {
        ...state,
        searchValue: action.searchValue,
        page: DEFAULT_PAGE_NUMBER
      };
    case ACTION_TYPES.CHANGE_ORDER_BY:
      return {
        ...state,
        orderBy: action.orderBy,
        page: DEFAULT_PAGE_NUMBER
      };
    case ACTION_TYPES.CHANGE_SORT_BY:
      return {
        ...state,
        sortBy: action.sortBy,
        page: DEFAULT_PAGE_NUMBER
      };
    case ACTION_TYPES.SET_SHOW_VERSION_DETAIL:
      return {
        ...state,
        versionDetail: action.versionDetail
      };
    default:
      return state;
  }
};

export const useTableListModal = (selectedVersion: any) => {
  const dispatch = useDispatch();

  const [state, reactDispatch] = useReducer(
    tableListReducer,
    TABLE_LIST_INITIAL_STATE
  );

  const { page, pageSize, searchValue, orderBy, versionDetail, sortBy } = state;

  const { tableList = [], loading, error } = useSelectTableListData();

  const tableLists = useMemo(() => tableList[0]?.versions || [], [tableList]);

  const handleGetTableList = useCallback(() => {
    dispatch(
      actionsWorkflowSetup.getTableList({
        tableType: 'ST',
        serviceNameAbbreviation: 'MLP'
      })
    );
  }, [dispatch]);

  useEffect(() => {
    handleGetTableList();
  }, [handleGetTableList]);

  const handleChangePage = useCallback((page: number) => {
    reactDispatch({ type: ACTION_TYPES.CHANGE_PAGE, page });
  }, []);

  const handleChangePageSize = useCallback(
    (pageSize: number) => {
      reactDispatch({ type: ACTION_TYPES.CHANGE_PAGE_SIZE, pageSize });
      handleChangePage(DEFAULT_PAGE_NUMBER);
    },
    [handleChangePage]
  );

  const handleSearch = useCallback(
    (val: string) => {
      if (searchValue === val) return;
      reactDispatch({
        type: ACTION_TYPES.CHANGE_SEARCH_VALUE,
        searchValue: val
      });
    },
    [searchValue]
  );

  const handleChangeSortBy = useCallback((sortBy: string) => {
    reactDispatch({
      type: ACTION_TYPES.CHANGE_SORT_BY,
      sortBy: [sortBy]
    });
  }, []);

  const handleChangeOrderBy = useCallback((orderBy: string) => {
    reactDispatch({
      type: ACTION_TYPES.CHANGE_ORDER_BY,
      orderBy: [orderBy]
    });
  }, []);

  const handleClearAndReset = () => {
    handleSearch('');
    if (selectedVersion) {
      handleChangePageToSelectedVersion();
    }
    reactDispatch({
      type: ACTION_TYPES.CHANGE_PAGE_SIZE,
      pageSize: PAGE_SIZE[0]
    });
  };

  const _searchValue = searchValue.toLowerCase() || '';
  const _tablesOrdered = _orderBy(tableLists, sortBy, orderBy);

  const _tablesFiltered = _tablesOrdered
    .filter((w: any) => {
      const listVersionName = Array.isArray(w?.versions)
        ? w?.versions.map((v: any) => v?.tableName)
        : [];
      return matchSearchValue([w?.tableName, ...listVersionName], _searchValue);
    })
    .map((w: any) => {
      const sortedVersions = _orderBy(w.versions, 'effectiveDate', 'desc');
      return { ...w, versions: sortedVersions };
    });

  const total = _tablesFiltered.length;
  const tables = _tablesFiltered.slice(
    (page! - 1) * pageSize!,
    page! * pageSize!
  );

  const oderByItem = ORDER_BY_LIST.find(i => i.value === orderBy);

  const dataFilter = {
    searchValue: _searchValue,
    sortBy,
    orderBy,
    page
  } as IDataFilter;

  const handleChangePageToSelectedVersion = useCallback(() => {
    if (
      !Array.isArray(tableLists) ||
      tableLists.length === 0 ||
      !selectedVersion
    )
      return;
    const selectedVersionIndex = _tablesOrdered.findIndex(
      (table: any) => table.tableId === selectedVersion?.tableId
    );
    if (selectedVersionIndex === -1) return;
    const selectedVersionPage = Math.ceil(
      (selectedVersionIndex + 1) / pageSize
    );
    handleChangePage(selectedVersionPage);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tableLists, selectedVersion, _tablesOrdered]);

  useEffect(() => {
    handleChangePageToSelectedVersion();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tableLists]);

  const handleSetShowVersionDetail = useCallback((versionDetail: any) => {
    reactDispatch({
      type: ACTION_TYPES.SET_SHOW_VERSION_DETAIL,
      versionDetail
    });
  }, []);

  return {
    tables,
    total,
    page,
    pageSize,
    dataFilter,
    handleChangePage,
    handleChangePageSize,
    handleSearch,
    orderBy: oderByItem,
    searchValue,
    handleChangeSortBy,
    handleChangeOrderBy,
    loading,
    error,
    handleGetTableList,
    handleClearAndReset,
    handleSetShowVersionDetail,
    versionDetail,
    dispatch: reactDispatch
  };
};
