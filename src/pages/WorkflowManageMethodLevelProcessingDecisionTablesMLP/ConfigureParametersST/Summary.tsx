import BadgeGroupTextControl from 'app/components/DofControl/BadgeGroupTextControl';
import GroupTextControl from 'app/components/DofControl/GroupTextControl';
import { BadgeColorType } from 'app/constants/enums';
import { Button, useTranslation } from 'app/_libraries/_dls';
import { isFunction } from 'formik';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { ConfigureParametersValue } from '.';
import GridDecision from './GridDecision';

const Summary: React.FC<WorkflowSetupSummaryProps<ConfigureParametersValue>> =
  ({ onEditStep, formValues, stepId, selectedStep, savedAt }) => {
    const { t } = useTranslation();
    const tables = useMemo(
      () => formValues?.tables?.[0] || {},
      [formValues?.tables]
    );

    const [changeComment, setChangeComment] = useState(tables.comment);
    useEffect(() => {
      if (stepId !== selectedStep) return;
    }, [stepId, selectedStep]);

    useEffect(() => {
      setChangeComment(tables.comment);
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [savedAt]);

    const handleEdit = () => {
      isFunction(onEditStep) && onEditStep();
    };

    const handleTruncate = useCallback(
      (isTruncated: boolean, showAll: boolean) => {
        if (!isTruncated) return;
        if (showAll) {
          setChangeComment(tables.comment);
        } else {
          setChangeComment(tables.comment?.replace(/\n/g, ''));
        }
      },
      [tables]
    );

    return (
      <div className="position-relative">
        <div className="absolute-top-right mt-n26 mr-n8">
          <Button variant="outline-primary" size="sm" onClick={handleEdit}>
            {t('txt_edit')}
          </Button>
        </div>
        <div className="row">
          <div className="col-12 col-lg-3">
            <GroupTextControl
              id="summary_changeName"
              input={{ value: tables.tableName } as any}
              meta={{} as any}
              label={t('txt_manage_account_level_table_decision_name')}
              options={{ inline: false }}
            />
          </div>
          <div className="col-12 col-lg-3">
            <GroupTextControl
              id="summary_effectiveDate"
              input={{ value: tables.tableId } as any}
              meta={{} as any}
              label={t('txt_manage_account_level_table_id')}
              options={{ inline: false }}
            />
          </div>
          <div className="col-12 col-lg-6">
            <span className="form-group-static__label mt-16">
              <span className="color-grey text-nowrap mr-2">
                {t('txt_manage_account_level_table_action_taken')}
              </span>
            </span>
            <BadgeGroupTextControl
              id="manage_account_level_table_version_created"
              input={
                {
                  value: t('txt_manage_account_level_table_version_created')
                } as any
              }
              meta={{} as any}
              options={{
                colorType: BadgeColorType.VersionCreated,
                noBorder: true
              }}
            />
          </div>
          <div className="col-12 col-lg-6 text-white-space-pre-line">
            <GroupTextControl
              id="summary_workflowNote"
              input={{ value: changeComment } as any}
              meta={{} as any}
              label={t('txt_comment_area')}
              options={{
                inline: false,
                lineTruncate: 2,
                ellipsisLessText: t('txt_less'),
                ellipsisMoreText: t('txt_more'),
                handleTruncateProp: handleTruncate
              }}
            />
          </div>
        </div>
        <GridDecision
          elementList={tables.elementList || []}
          isTextOnly
          isSmallHeader
        />
      </div>
    );
  };

export default Summary;
