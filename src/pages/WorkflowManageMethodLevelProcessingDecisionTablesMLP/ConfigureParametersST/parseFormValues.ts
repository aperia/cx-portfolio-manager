import {
  getWorkflowSetupStepStatus,
  getWorkflowSetupTableType
} from 'app/helpers';
import { ConfigureParametersValue } from './';

const parseFormValues: WorkflowSetupStepFormDataFunc<ConfigureParametersValue> =
  (data, id) => {
    const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);
    if (!stepInfo) return;

    const now = Date.now();

    const tables = (data.data.configurations?.tableList || [])
      .filter((table: any) => table.tableType === 'ST')
      .map(
        (table: any, idx: number) =>
          ({
            ...table,
            id: '', // Remove Id due to error 500 when update with methodId
            actionTaken: getWorkflowSetupTableType(table),
            rowId: now + idx,
            elementList: table.decisionElements,
            selectedTableId: table.modeledFrom?.tableId,
            tableControlParameters: table.tableControlParameters.reduce(
              (obj: Record<string, any>, item: Record<string, any>) => {
                obj[item.name] = item.value;
                return obj;
              },
              {}
            )
          } as WorkflowSetupMethod & { rowId?: number })
      );

    return {
      ...getWorkflowSetupStepStatus(stepInfo),
      isValid: tables.length > 0,
      tables
    };
  };

export default parseFormValues;
