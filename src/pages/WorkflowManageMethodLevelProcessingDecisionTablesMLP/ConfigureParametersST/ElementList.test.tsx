import userEvent from '@testing-library/user-event';
import { renderWithMockStore } from 'app/utils';
import 'app/utils/_mockComponent/mockModalRegistry';
import { queryByClass } from 'app/_libraries/_dls/test-utils';
import React from 'react';
import * as reactRedux from 'react-redux';
import ElementList from './ElementList';

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

jest.mock('./GridDecision', () => {
  const MockGrid = () => {
    return <div>Grid</div>;
  };
  return {
    __esModule: true,
    default: MockGrid
  };
});

jest.mock('app/_libraries/_dls/components/CheckBox', () => {
  const MockCheckBox = ({ onChange }: any) => {
    return (
      <div
        onClick={() =>
          onChange({
            target: {
              value: {
                checked: true
              }
            }
          })
        }
      >
        MockCheckBox
      </div>
    );
  };

  MockCheckBox.Input = () => <div>Mock Input</div>;
  return {
    __esModule: true,
    default: MockCheckBox
  };
});

HTMLCanvasElement.prototype.getContext = jest.fn();

const renderComponent = async (props: any) =>
  renderWithMockStore(<ElementList {...props} />, {});

const defaultProps = {
  data: [
    {
      tableName: '123',
      tableId: '123',
      comment: '1231412421',
      rowId: '123'
    }
  ],
  handleEdit: jest.fn(),
  handleDelete: jest.fn(),
  handleOpenConfigModal: jest.fn()
};

describe('WorkflowManageMethodLevelProcessingDecisionTablesMLP > ConfigureParameters > ElementList', () => {
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => jest.fn());
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
  });

  it('Should render Element List With 1 element', async () => {
    const wrapper = await renderComponent(defaultProps);

    userEvent.click(wrapper.getByText('txt_edit'));
    userEvent.click(wrapper.getByText('txt_delete'));
    userEvent.click(wrapper.getByText('txt_cancel'));

    userEvent.click(wrapper.getByText('txt_edit'));
    userEvent.click(wrapper.getByText('txt_delete'));
    userEvent.click(wrapper.getByText('AutoClose'));
  });

  it('Should render Element List With close modal', async () => {
    const wrapper = await renderComponent(defaultProps);

    userEvent.click(wrapper.getByText('txt_edit'));
    userEvent.click(wrapper.getByText('txt_delete'));
    userEvent.click(wrapper.getAllByText('txt_delete')[1]);

    userEvent.click(
      queryByClass(wrapper.container, /icon-close/) as HTMLElement
    );
  });
});
