import { ColumnType, Grid, TextBox, useTranslation } from 'app/_libraries/_dls';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React from 'react';
import { useGridDecision } from './useGridDecision';

interface IElementList {
  elementList: any[];
  isTextOnly?: boolean;
  isSmallHeader?: boolean;
  handleSetElementList?: (elementList: any[]) => void;
}

const GirdDecision: React.FC<IElementList> = ({
  elementList,
  isTextOnly,
  handleSetElementList,
  isSmallHeader
}) => {
  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();
  const { elements } = useGridDecision(elementList, handleSetElementList);

  const columns: ColumnType[] = [
    {
      id: 'name',
      Header: t('txt_manage_transaction_level_tlp_config_tq_decision_element'),
      accessor: ({ name }) =>
        isTextOnly ? (
          <p className="pt-4">{name}</p>
        ) : (
          <TextBox type="text" value={name} size="sm" readOnly />
        ),
      cellBodyProps: { className: 'py-8' },
      width: width > 1280 ? 320 : 280
    },
    {
      id: 'searchCode',
      Header: t('txt_manage_transaction_level_tlp_config_tq_search_code'),
      accessor: ({ searchCodeText }) =>
        isTextOnly ? (
          <p className="pt-4">{searchCodeText}</p>
        ) : (
          <TextBox type="text" value={searchCodeText} size="sm" readOnly />
        ),
      cellBodyProps: { className: 'py-8' },
      width: width > 1280 ? 240 : 210
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      accessor: viewMoreInfo(['moreInfo'], t),
      width: 106,
      cellBodyProps: { className: 'pt-12 pb-8' }
    }
  ];

  return (
    <div className="mt-24">
      <div className="d-flex align-items-center justify-content-between mb-16">
        {!isSmallHeader ? (
          <h5>
            {t(
              'txt_manage_transaction_level_tlp_config_tq_decision_element_list'
            )}
          </h5>
        ) : (
          <p className="fs-14 fw-500">
            {t(
              'txt_manage_transaction_level_tlp_config_tq_decision_element_list'
            )}
          </p>
        )}
      </div>

      <div className="mt-16">
        <Grid columns={columns} data={elements} />
      </div>
    </div>
  );
};

export default GirdDecision;
