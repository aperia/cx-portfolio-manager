import mockDate from 'app/_libraries/_dls/test-utils/mockDate';
import parseFormValues from './parseFormValues';

const todayString = '02/23/2022';
const today = new Date(todayString);
describe('pages > WorkflowManageMethodLevelProcessingDecisionTablesMLP > ConfigureParameters > parseFormValues', () => {
  beforeEach(() => {
    mockDate.set(today);
  });
  it('should return undefined with methodList is undefined', () => {
    const response = parseFormValues(
      { data: { workflowSetupData: [{ id: '1' }] } } as any,
      '1',
      jest.fn()
    );
    expect(response).toEqual(
      expect.objectContaining({
        isPass: false,
        isSelected: false,
        isValid: false,
        tables: []
      })
    );
  });

  it('should return undefined with stepInfo is undefined', () => {
    const response = parseFormValues(
      {
        data: {
          workflowSetupData: [
            {
              id: '2'
            }
          ],
          configurations: {
            tableList: []
          }
        }
      } as any,
      '1',
      jest.fn()
    );
    expect(response).toEqual(undefined);
  });

  it('should return valid data', () => {
    const response = parseFormValues(
      {
        data: {
          workflowSetupData: [
            {
              id: 'configMlpCa'
            }
          ],
          configurations: {
            tableList: [
              {
                id: '123',
                name: '123',
                tableType: 'CA'
              },
              { tableType: 'ST', tableControlParameters: [{ value: 'value' }] }
            ]
          }
        }
      } as any,
      'configMlpCa',
      jest.fn()
    );
    expect(response).toEqual(
      expect.objectContaining({
        isPass: false,
        isSelected: false,
        isValid: true
      })
    );
  });
});
