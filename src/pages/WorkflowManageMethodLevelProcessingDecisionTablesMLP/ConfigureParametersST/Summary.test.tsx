import { fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { renderWithMockStore } from 'app/utils';
import React from 'react';
import * as reactRedux from 'react-redux';
import Summary from './Summary';

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');
const mockOnTruncateParams = jest.fn();
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    }),
    TruncateText: ({ onTruncate, children }: any) => (
      <div>
        {children}
        <button
          onFocus={() => onTruncate && onTruncate(...mockOnTruncateParams())}
        >
          On Truncate
        </button>
      </div>
    )
  };
});

const renderComponent = async (props: any) =>
  await renderWithMockStore(<Summary {...props} />, {});

const props = {
  originalStepConfig: {},
  onEditStep: jest.fn(),
  selectedStep: '1',
  stepId: '2',
  formValues: {
    tables: [
      {
        original: { elementList: [], tableControlParameters: [] },
        actionTaken: 'createNewTable',
        tableType: 'tableType',
        tableId: 'Table 1',
        tableName: 'Table Name 1',
        comment:
          'comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment comment'
      }
    ]
  }
};

describe('WorkflowManageMethodLevelProcessingDecisionTablesMLP > ConfigureParameters > Summary', () => {
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => jest.fn());
  });

  it('Should render Summary', async () => {
    const wrapper = await renderComponent({ ...props, formValues: {} });

    expect(wrapper.getByText('txt_edit')).toBeInTheDocument();
  });

  it('Should render Summary', async () => {
    mockOnTruncateParams.mockReturnValue([true, false]);
    const wrapper = await renderComponent({ ...props, stepId: '2' });
    userEvent.click(wrapper.getByText('txt_edit'));
    wrapper.queryAllByText(/On Truncate/).forEach(element => {
      fireEvent.focus(element!);
    });
  });

  it('Should render Summary', async () => {
    mockOnTruncateParams.mockReturnValue([false, true]);
    const wrapper = await renderComponent({ ...props, stepId: '1' });
    wrapper.queryAllByText(/On Truncate/).forEach(element => {
      fireEvent.focus(element!);
    });
  });
  it('Should render Summary', async () => {
    mockOnTruncateParams.mockReturnValue([true, true]);
    const wrapper = await renderComponent({ ...props, stepId: '2' });
    wrapper.queryAllByText(/On Truncate/).forEach(element => {
      fireEvent.focus(element!);
    });
  });
});
