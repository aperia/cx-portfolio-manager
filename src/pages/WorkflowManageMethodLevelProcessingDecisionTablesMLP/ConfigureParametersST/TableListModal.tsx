import FailedApiReload from 'app/components/FailedApiReload';
import ModalRegistry from 'app/components/ModalRegistry';
import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import { WORKFLOW_TLP_ST_SORT_BY_LIST } from 'app/constants/constants';
import { classnames } from 'app/helpers';
import {
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import get from 'lodash.get';
import isEmpty from 'lodash.isempty';
import { nanoid } from 'nanoid';
import VersionCardView from 'pages/WorkflowManageTransactionLevelProcessingDecisionTablesTLP/ConfigureParameters/VersionCardView';
import { defaultWorkflowMethodsFilter } from 'pages/_commons/redux/WorkflowSetup/reducers';
import Paging from 'pages/_commons/Utils/Paging';
import SortOrder from 'pages/_commons/Utils/SortOrder';
import React, { useCallback, useMemo, useRef } from 'react';
import { CONFIG_ACTIONS_MODAL } from './useConfigActions';
import { useTableListModal } from './useTableListModal';
import ViewModal from './ViewModal';

interface TableListModalProps {
  id: string;
  show: boolean;
  handleClose: () => void;
  selectedVersion: any;
  handleSelectVersion: (version: any) => void;
  handleSetDraftNewTable: (draftNewTable: any) => void;
  draftNewTable: any;
  handleOpenConfigModal: (modal: CONFIG_ACTIONS_MODAL | null) => void;
}

const TableListModal: React.FC<TableListModalProps> = ({
  id,
  show,
  handleClose,
  selectedVersion,
  handleSelectVersion,
  draftNewTable,
  handleSetDraftNewTable,
  handleOpenConfigModal
}) => {
  const { t } = useTranslation();
  const simpleSearchRef = useRef<any>(null);
  const {
    tables,
    total,
    page,
    pageSize,
    handleChangePage,
    handleChangePageSize,
    handleSearch,
    searchValue,
    handleChangeSortBy,
    handleChangeOrderBy,
    loading,
    error,
    handleGetTableList,
    handleClearAndReset,
    handleSetShowVersionDetail,
    versionDetail,
    dataFilter
  } = useTableListModal(selectedVersion);

  const handleGoToNextStep = (versionDetail?: any) => {
    handleOpenConfigModal(CONFIG_ACTIONS_MODAL.addNewTableModal);
    /* istanbul ignore else */
    if (versionDetail) handleSelectVersion(versionDetail);
    const versionSelect = versionDetail || selectedVersion;
    /* istanbul ignore else */
    if (
      !draftNewTable ||
      (draftNewTable &&
        draftNewTable?.selectedVersionId !== versionSelect?.version?.tableId)
    ) {
      let elementListSelected = get(
        versionSelect,
        ['version', 'decisionElements'],
        []
      );

      elementListSelected = elementListSelected.map((el: any) => ({
        ...el,
        rowId: nanoid()
      }));

      handleSetDraftNewTable({
        selectedVersionId: versionSelect?.version?.tableId,
        elementList: elementListSelected,
        selectedTableId: versionSelect?.tableId,
        modeledFrom: {
          tableId: versionSelect?.tableId,
          tableName: versionSelect?.version?.tableName,
          versions: [{ tableId: versionSelect?.version?.tableId }]
        }
      });
    }
  };

  const handleSelectTableVersion = useCallback(
    (tableId: string, isViewDetail?: boolean) => (version: any) => {
      isViewDetail
        ? handleSetShowVersionDetail({ tableId, version })
        : handleSelectVersion({ tableId, version });
    },
    [handleSelectVersion, handleSetShowVersionDetail]
  );

  const handleCloseModalVersionDetail = () => {
    handleSetShowVersionDetail(false);
  };

  const handleCloseAndClearState = () => {
    handleClose();
    handleSelectVersion(null);
  };

  const noDataFound = useMemo(
    () =>
      !loading &&
      !error &&
      total === 0 && (
        <div className="d-flex flex-column justify-content-center mt-40">
          <NoDataFound
            id="method-list-NoDataFound"
            title={t('txt_no_tables_to_display')}
            hasSearch={!!searchValue}
            linkTitle={!!searchValue && t('txt_clear_and_reset')}
            onLinkClicked={handleClearAndReset}
          />
        </div>
      ),
    [t, loading, error, total, searchValue, handleClearAndReset]
  );
  const failedReload = useMemo(
    () => (
      <div>
        {error && (
          <FailedApiReload
            id="method-list-error"
            onReload={handleGetTableList}
            className="mt-40 pt-40 d-flex flex-column justify-content-center align-items-center"
          />
        )}
      </div>
    ),
    [error, handleGetTableList]
  );

  const tableList = useMemo(
    () => (
      <div>
        {total > 0 && (
          <React.Fragment>
            <div className="mt-16">
              {tables.map((table: any) => (
                <VersionCardView
                  key={table.tableId}
                  version={table}
                  selectedVersion={selectedVersion}
                  onSelectVersion={handleSelectTableVersion(table.tableId)}
                  onViewVersionDetail={handleSelectTableVersion(
                    table.tableId,
                    true
                  )}
                />
              ))}
            </div>
            <div className="mt-8">
              <Paging
                page={page}
                pageSize={pageSize}
                totalItem={total}
                onChangePage={handleChangePage}
                onChangePageSize={handleChangePageSize}
              />
            </div>
          </React.Fragment>
        )}
      </div>
    ),
    [
      handleChangePage,
      handleChangePageSize,
      total,
      page,
      pageSize,
      tables,
      selectedVersion,
      handleSelectTableVersion
    ]
  );

  return (
    <React.Fragment>
      <ModalRegistry
        id={id}
        show={show}
        lg
        onAutoClosedAll={handleCloseAndClearState}
      >
        <ModalHeader border closeButton onHide={handleCloseAndClearState}>
          <ModalTitle>{t('txt_create_new_version')}</ModalTitle>
        </ModalHeader>
        <ModalBody
          className={classnames('px-24 pt-24 pb-24 pb-lg-0', {
            loading
          })}
        >
          <div>
            <p>
              To create a new version of the Method Level Processing
              <sup>SM</sup> Selection Table table <b>MLPTABLE</b>, select the
              version you want to model and click <b>Continue</b>.
            </p>
          </div>
          <div className="mt-24">
            <div className="d-flex justify-content-between align-items-center">
              <h5>{t('txt_manage_account_level_table_versions')}</h5>
              {(total > 0 || searchValue) && (
                <div className="method-list-simple-search">
                  <SimpleSearch
                    defaultValue={searchValue}
                    ref={simpleSearchRef}
                    clearTooltip={t('txt_clear_search_criteria')}
                    onSearch={handleSearch}
                    placeholder={t('txt_manage_account_level_search')}
                  />
                </div>
              )}
            </div>
            {total > 0 && (
              <div className="mt-16 d-flex justify-content-end align-items-center">
                <SortOrder
                  hasFilter={!!searchValue}
                  dataFilter={dataFilter}
                  defaultDataFilter={defaultWorkflowMethodsFilter}
                  sortByFields={WORKFLOW_TLP_ST_SORT_BY_LIST}
                  onChangeOrderBy={handleChangeOrderBy}
                  onChangeSortBy={handleChangeSortBy}
                  onClearSearch={handleClearAndReset}
                />
              </div>
            )}
          </div>
          {tableList}
          {noDataFound}
          {failedReload}
        </ModalBody>

        <ModalFooter
          cancelButtonText={t('txt_cancel')}
          okButtonText={t('txt_continue')}
          onCancel={handleCloseAndClearState}
          disabledOk={!selectedVersion}
          onOk={() => handleGoToNextStep()}
        />
      </ModalRegistry>

      {!isEmpty(versionDetail) && (
        <ViewModal
          onCancel={handleCloseModalVersionDetail}
          tableId={versionDetail?.tableId}
          onOK={() => handleGoToNextStep(versionDetail)}
          data={versionDetail?.version}
        />
      )}
    </React.Fragment>
  );
};

export default TableListModal;
