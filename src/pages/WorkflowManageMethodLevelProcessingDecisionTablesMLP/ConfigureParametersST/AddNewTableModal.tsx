import ModalRegistry from 'app/components/ModalRegistry';
import TextAreaCountDown from 'app/components/TextAreaCountDown';
import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { useUnsavedChangesRedirect } from 'app/hooks';
import {
  Button,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  TextBox,
  TransDLS,
  useTranslation
} from 'app/_libraries/_dls';
import { nanoid } from 'nanoid';
import React, { useMemo } from 'react';
import GridDecision from './GridDecision';
import { useAddNewTable } from './useAddNewTable';
import { CONFIG_ACTIONS_MODAL } from './useConfigActions';

export const AddNewTableAction = {
  createNewVersion: 'createNewVersion'
};

interface AddNewTableModalProps {
  id: string;
  show: boolean;
  handleClose: () => void;
  selectedVersion: any;
  handleSelectVersion: (version: any) => void;
  handleSetDraftNewTable: (draftNewTable: any) => void;
  draftNewTable: any;
  selectedTable: any;
  handleEditTable: (newTable: any) => void;
  handleAddNewTable: (table: any) => void;
  handleSetSelectedTable: (table: any) => void;
  handleOpenConfigModal: (modal: CONFIG_ACTIONS_MODAL | null) => void;
}

const AddNewTableModal: React.FC<AddNewTableModalProps> = ({
  show,
  handleClose,
  id,
  handleSelectVersion,
  draftNewTable,
  handleSetDraftNewTable,
  selectedVersion,
  selectedTable,
  handleEditTable,
  handleAddNewTable,
  handleSetSelectedTable,
  handleOpenConfigModal
}) => {
  const { t } = useTranslation();

  const redirect = useUnsavedChangesRedirect();

  const {
    tableId,
    tableName,
    comment,
    handleBlurField,
    handleChangeFieldValue,
    elementList,
    selectedTableId,
    selectedVersionId,
    errors,
    modeledFrom,
    handleSetElementList
  } = useAddNewTable(draftNewTable, selectedTable);

  const actionType = useMemo(() => {
    if (selectedTable) return selectedTable.actionTaken;
    return AddNewTableAction.createNewVersion;
  }, [selectedTable]);

  const handleGoBackToPreviousStep = () => {
    handleOpenConfigModal(CONFIG_ACTIONS_MODAL.tableListModal);
    handleSetDraftNewTable({
      comment,
      tableId,
      tableName,
      selectedTableId,
      elementList,
      selectedVersionId
    });
    if (selectedTable && !selectedVersion) {
      handleSelectVersion(selectedTable?.selectedVersion);
    }
  };

  const handleCloseAndClearState = () => {
    handleClose();
    handleSelectVersion(null);
    handleSetDraftNewTable(null);
    handleSetSelectedTable(null);
  };

  const handleCloseModal = () => {
    redirect({
      onConfirm: () => handleCloseAndClearState(),
      formsWatcher: [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__LEVEL_PROCESSING_ST__METHOD_DETAILS
      ]
    });
  };

  const handleCreateNewTable = () => {
    const tableItem = {
      rowId: selectedTable ? selectedTable.rowId : nanoid(),
      tableId:
        actionType === AddNewTableAction.createNewVersion
          ? selectedTableId
          : tableId,
      tableName,
      elementList,
      comment,
      actionTaken: actionType,
      selectedVersionId,
      selectedTableId,
      selectedVersion: selectedTable?.selectedVersion || selectedVersion,
      modeledFrom
    };

    selectedTable ? handleEditTable(tableItem) : handleAddNewTable(tableItem);
    handleCloseAndClearState();
  };

  const isDisabledSaveButton = !tableName;

  return (
    <ModalRegistry lg id={id} show={show} onAutoClosedAll={handleCloseModal}>
      <ModalHeader border closeButton onHide={handleCloseModal}>
        <ModalTitle>
          {t('txt_manage_transaction_level_tlp_config_tq_tableDetails')}
        </ModalTitle>
      </ModalHeader>
      <ModalBody>
        <div className="color-grey">
          <TransDLS keyTranslation="txt_manage_method_level_processing_st_help_text">
            <sup />
          </TransDLS>
        </div>
        <div className="row mt-16">
          <div className="col-6 col-lg-4">
            <TextBox
              id="addNewMethod__selectedTableId"
              readOnly
              value={selectedTableId}
              maxLength={8}
              label={t(
                'txt_manage_transaction_level_tlp_config_tq_selected_table_id'
              )}
            />
          </div>
          <div className="col-6 col-lg-4">
            <TextBox
              id="addNewTable__tableName"
              value={tableName}
              onChange={handleChangeFieldValue('tableName')}
              onBlur={handleBlurField('tableName')}
              required
              maxLength={18}
              label={t('txt_manage_transaction_level_tlp_config_tq_table_name')}
              error={errors.tableName}
            />
          </div>
          <div className="mt-16 col-12 col-lg-8">
            <TextAreaCountDown
              id="addNewTable__comment"
              label={t('txt_comment_area')}
              value={comment}
              onChange={handleChangeFieldValue('comment')}
              rows={4}
              maxLength={204}
            />
          </div>
        </div>
        <GridDecision
          elementList={elementList}
          handleSetElementList={handleSetElementList}
        />
      </ModalBody>

      <ModalFooter
        cancelButtonText={t('txt_cancel')}
        okButtonText={t('txt_save')}
        onCancel={handleCloseModal}
        onOk={handleCreateNewTable}
        disabledOk={isDisabledSaveButton}
      >
        <Button
          className="mr-auto ml-n8"
          variant="outline-primary"
          onClick={handleGoBackToPreviousStep}
        >
          {t('txt_back')}
        </Button>
      </ModalFooter>
    </ModalRegistry>
  );
};

export default AddNewTableModal;
