import { useFormValidations } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls';
import { isEqual } from 'app/_libraries/_dls/lodash';
import isEmpty from 'lodash.isempty';
import { useCallback, useMemo, useReducer } from 'react';

interface NewTableState {
  selectedTableId: string;
  tableId: string;
  tableName: string;
  comment: string;
  elementList: any[];
  selectedVersionId: string;
  modeledFrom?: any;
}

const NEW_TABLE_INITIAL_STATE = {
  selectedTableId: '',
  tableId: '',
  tableName: '',
  comment: '',
  elementList: [],
  selectedVersionId: ''
};

const ACTION_TYPES = {
  CHANGE_FORM_VALUE: 'CHANGE_FORM_VALUE',
  SET_ELEMENT_LIST: 'SET_ELEMENT_LIST'
};

const newTableReducer = (state: NewTableState, action: any) => {
  switch (action.type) {
    case ACTION_TYPES.CHANGE_FORM_VALUE:
      return { ...state, [action.field]: action.value };
    case ACTION_TYPES.SET_ELEMENT_LIST:
      return { ...state, elementList: action.elementList };
    default:
      return state;
  }
};

export const useAddNewTable = (
  draftNewTable: Record<string, any>,
  selectedTable: any
) => {
  const initialState = useMemo(() => {
    if (selectedTable && isEmpty(draftNewTable)) {
      return { ...NEW_TABLE_INITIAL_STATE, ...selectedTable };
    }
    return { ...NEW_TABLE_INITIAL_STATE, ...draftNewTable };
  }, [selectedTable, draftNewTable]);

  const [state, reactDispatch] = useReducer(newTableReducer, initialState);
  const { t } = useTranslation();

  const {
    tableName,
    tableId,
    comment,
    elementList,
    selectedTableId,
    selectedVersionId,
    modeledFrom
  } = state;

  const currentErrors = useMemo(
    () =>
      ({
        tableName: !tableName && {
          status: true,
          message: t('txt_required_validation', {
            fieldName: 'Table Name'
          })
        }
      } as Record<string, IFormError>),
    [t, tableName]
  );

  const [touches, errors, setTouched] = useFormValidations(currentErrors);

  const handleBlurField = (fieldName: string) => () => {
    setTouched(fieldName, true)();
  };

  const handleChangeFieldValue = (field: string) => (e: any) => {
    let value = e.target?.value;

    if (field === 'tableName') {
      value = value.toUpperCase();
    }

    reactDispatch({
      type: ACTION_TYPES.CHANGE_FORM_VALUE,
      field,
      value
    });
  };

  const isFormChanging = useMemo(() => {
    return !isEqual(
      {
        tableName,
        comment
      },
      {
        comment: initialState?.comment,
        tableName: initialState?.tableName
      }
    );
  }, [tableName, comment, initialState]);

  const handleSetElementList = useCallback((elementList: any[]) => {
    reactDispatch({
      type: ACTION_TYPES.SET_ELEMENT_LIST,
      elementList
    });
  }, []);

  return {
    handleBlurField,
    errors,
    touches,
    handleChangeFieldValue,
    tableId,
    tableName,
    comment,
    elementList,
    selectedTableId,
    modeledFrom,
    selectedVersionId,
    dispatch: reactDispatch,
    isFormChanging,
    handleSetElementList
  };
};
