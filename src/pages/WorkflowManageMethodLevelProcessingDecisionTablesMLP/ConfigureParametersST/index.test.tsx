import userEvent from '@testing-library/user-event';
import { renderWithMockStore } from 'app/utils';
import React from 'react';
import * as reactRedux from 'react-redux';
import ConfigureParameters from '.';
import * as hooks from './useConfigActions';
import { CONFIG_ACTIONS_MODAL } from './useConfigActions';

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');
jest.mock('./TableListModal', () => ({
  __esModule: true,
  default: ({ handleClose }: any) => {
    return (
      <div>
        <h1>TableListModal_component</h1>
        <button onClick={handleClose}>Close</button>
      </div>
    );
  }
}));

jest.mock('./AddNewTableModal', () => ({
  __esModule: true,
  default: ({ handleClose }: any) => {
    return (
      <div>
        <h1>AddNewTableModal_component</h1>
        <button onClick={handleClose}>Close Add</button>
      </div>
    );
  }
}));

jest.mock('./ActionButtons', () => ({
  __esModule: true,
  default: ({ onClickCreateNewVersion }: any) => {
    return (
      <div>
        <button onClick={onClickCreateNewVersion}>Add_New_Table</button>
      </div>
    );
  }
}));

jest.mock('./TableListModal', () => ({
  __esModule: true,
  default: ({ handleClose }: any) => {
    return (
      <div>
        <h1>TableList_component</h1>
        <button onClick={handleClose}>Close Table</button>
      </div>
    );
  }
}));

jest.mock('app/hooks/useUnsavedChangeRegistry', () => {
  return {
    __esModule: true,
    useUnsavedChangeRegistry: (options: any, changes: any, onConfirm: any) => {
      onConfirm && onConfirm();
    }
  };
});

const t = (value: string) => value;

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

const renderComponent = async (props: any) =>
  renderWithMockStore(<ConfigureParameters {...props} />);

const defaultConfig = {
  openedModal: CONFIG_ACTIONS_MODAL.tableListModal,
  selectedVersion: 'abc',
  draftNewTable: 'acb',
  selectedTable: 'acb',
  tableList: null,
  handleOpenConfigModal: jest.fn(),
  handleSetSelectedVersion: jest.fn(),
  handleSetDraftNewTable: jest.fn(),
  handleEditTable: jest.fn(),
  handleAddNewTable: jest.fn(),
  handleDeleteTable: jest.fn(),
  handleSetSelectedTable: jest.fn(),
  handleSetTable: jest.fn()
};

const defaultProps = {
  savedAt: 1231231,
  isStuck: true,
  formValues: {
    isValid: true,
    tables: null
  },
  selectedStep: 'abc',
  setFormValues: jest.fn(),
  clearFormName: jest.fn(),
  clearFormValues: jest.fn()
};

describe('WorkflowManageMethodLevelProcessingDecisionTablesMLP > ConfigureParameters > index', () => {
  const mockDispatch = jest.fn();

  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Should render Table List ', async () => {
    jest.spyOn(hooks, 'useConfigAction').mockReturnValue(defaultConfig as any);
    const wrapper = await renderComponent(defaultProps);

    const btnAdd = wrapper.getByText('Add_New_Table');
    userEvent.click(btnAdd);

    const btnClose = wrapper.getByText('Close Table');
    userEvent.click(btnClose);
  });

  it('Should render Table List ', async () => {
    jest
      .spyOn(hooks, 'useConfigAction')
      .mockReturnValue({ ...defaultConfig, draftNewTable: undefined } as any);
    const wrapper = await renderComponent(defaultProps);

    const btnAdd = wrapper.getByText('Add_New_Table');
    userEvent.click(btnAdd);

    const btnClose = wrapper.getByText('Close Table');
    userEvent.click(btnClose);
  });

  it('Should render Table List ', async () => {
    jest.spyOn(hooks, 'useConfigAction').mockReturnValue({
      ...defaultConfig,
      tableList: [
        {
          id: '123'
        }
      ],
      openedModal: CONFIG_ACTIONS_MODAL.addNewTableModal
    } as any);
    const wrapper = await renderComponent({
      ...defaultProps,
      tableList: null,
      formValues: {
        isValid: true,
        tables: [{ id: '123' }]
      }
    });

    const btnClose = wrapper.getByText('Close Add');

    userEvent.click(btnClose);
  });
});
