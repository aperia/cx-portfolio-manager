import BadgeGroupTextControl from 'app/components/DofControl/BadgeGroupTextControl';
import GroupTextControl from 'app/components/DofControl/GroupTextControl';
import ModalRegistry from 'app/components/ModalRegistry';
import { BadgeColorType } from 'app/constants/enums';
import { classnames } from 'app/helpers';
import {
  Button,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import React, { useState } from 'react';
import GridDecision from './GridDecision';
import { CONFIG_ACTIONS_MODAL } from './useConfigActions';

interface ElementListProps {
  data: any;
  handleEdit: (data: any) => void;
  handleDelete: (rowId: string) => void;
  handleOpenConfigModal: (modal: CONFIG_ACTIONS_MODAL | null) => void;
}

const ElementList: React.FC<ElementListProps> = ({
  data,
  handleEdit,
  handleDelete,
  handleOpenConfigModal
}) => {
  const { t } = useTranslation();
  const [isDelete, setIsDelete] = useState(false);
  const { tableName, tableId, comment, elementList = [], rowId } = data[0];

  return (
    <div>
      <div className="d-flex justify-content-between align-items-center">
        <h5>{tableName}</h5>
        <div className="mr-n8">
          <Button
            size="sm"
            variant="outline-primary"
            onClick={() => {
              handleEdit(data[0]);
              handleOpenConfigModal(CONFIG_ACTIONS_MODAL.addNewTableModal);
            }}
          >
            {t('txt_edit')}
          </Button>
          <Button
            size="sm"
            variant="outline-danger"
            onClick={() => setIsDelete(true)}
          >
            {t('txt_delete')}
          </Button>
        </div>
      </div>

      <div className="row">
        <div className="col-6 col-xl-3">
          <GroupTextControl
            id="tableId"
            input={{ value: tableId } as any}
            meta={{} as any}
            label={t('txt_manage_account_level_table_id')}
            options={{ inline: false }}
          />
        </div>
        <div className="col-6 col-xl-3">
          <div className="form-group-static align-items-center">
            <span className="form-group-static__label">
              <span className="color-grey text-nowrap mr-2">
                {t('txt_manage_account_level_table_action_taken')}
              </span>
            </span>
            <BadgeGroupTextControl
              id="manage_account_level_table_version_created"
              input={
                {
                  value: t('txt_manage_account_level_table_version_created')
                } as any
              }
              meta={{} as any}
              options={{
                colorType: BadgeColorType.VersionCreated,
                noBorder: true
              }}
            />
          </div>
        </div>
        <div className="col-12 col-xl-6">
          <GroupTextControl
            id="comment"
            input={{ value: comment } as any}
            meta={{} as any}
            label={t('Comment Area')}
            options={{
              inline: false,
              lineTruncate: 2,
              ellipsisLessText: t('txt_less'),
              ellipsisMoreText: t('txt_more')
            }}
          />
        </div>
      </div>
      <hr className="my-24" />
      <GridDecision elementList={elementList} isTextOnly />

      {isDelete && (
        <ModalRegistry
          id="deleteNewVersion"
          show
          sm
          onAutoClosedAll={() => setIsDelete(false)}
        >
          <ModalHeader border closeButton onHide={() => setIsDelete(false)}>
            <ModalTitle>
              {t('txt_manage_account_level_table_delete_title')}
            </ModalTitle>
          </ModalHeader>
          <ModalBody className={classnames('px-24 pt-24 pb-24 overflow-auto')}>
            <p>{t('txt_manage_account_level_table_delete_desc')}</p>
          </ModalBody>

          <ModalFooter>
            <Button variant="secondary" onClick={() => setIsDelete(false)}>
              {t('txt_cancel')}
            </Button>
            <Button variant="danger" onClick={() => handleDelete(rowId)}>
              {t('txt_delete')}
            </Button>
          </ModalFooter>
        </ModalRegistry>
      )}
    </div>
  );
};

export default ElementList;
