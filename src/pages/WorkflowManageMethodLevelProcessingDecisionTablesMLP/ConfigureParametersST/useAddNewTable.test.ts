import { act, renderHook } from '@testing-library/react-hooks';
import { useAddNewTable } from './useAddNewTable';

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

describe('WorkflowManageTransactionLevelProcessingDecisionTablesTLP > ConfigureParametersTQ > useAddNewTable', () => {
  afterAll(() => {
    jest.clearAllMocks();
  });

  it('Run Default reducer function', () => {
    const { result } = renderHook(() => useAddNewTable({}, false));
    act(() => {
      result.current.dispatch({ type: 'Test Action' });
    });
  });

  it('Run Default reducer function', () => {
    const { result } = renderHook(() =>
      useAddNewTable(
        {},
        {
          tableId: 'tableId'
        }
      )
    );
    act(() => {
      result.current.dispatch({ type: 'Test Action' });
      result.current.handleSetElementList([]);
      result.current.handleBlurField('123')();
      result.current.handleChangeFieldValue('123')({
        target: {
          value: '123'
        }
      });
      result.current.handleChangeFieldValue('tableName')({
        target: {
          value: '123'
        }
      });
    });
  });
});
