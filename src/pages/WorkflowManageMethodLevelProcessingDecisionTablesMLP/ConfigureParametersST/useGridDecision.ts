import { isFunction } from 'app/_libraries/_dls/lodash';
import {
  actionsWorkflowSetup,
  useSelectDecisionElementListData,
  useSelectElementMetadataManageMethodLevelProcessingMLP
} from 'pages/_commons/redux/WorkflowSetup';
import { useEffect, useMemo, useState } from 'react';
import { useDispatch } from 'react-redux';

export enum FIELD_TYPE {
  dropDown = 'dropDown',
  checkBox = 'checkBox'
}

export const useGridDecision = (
  elementList: any[],
  handleSetElementList?: (elementList: any[]) => void
) => {
  const dispatch = useDispatch();
  const [elements, setElements] = useState<any[]>([]);
  const data = useSelectDecisionElementListData('MLPST');

  const decisionElementList = useMemo(
    () => data?.decisionElementList || [],
    [data]
  );

  const metadata = useSelectElementMetadataManageMethodLevelProcessingMLP();

  const { searchCode } = metadata;

  useEffect(() => {
    dispatch(
      actionsWorkflowSetup.getDecisionElementList({
        tableType: 'ST',
        serviceNameAbbreviation: 'MLP'
      })
    );
  }, [dispatch]);

  const getSearchCodeText = (codeValue: string) => {
    const searchCodeOption = searchCode.find(code => code.code === codeValue);
    return searchCodeOption?.text || '';
  };

  useEffect(() => {
    if (decisionElementList.length === 0) return;
    const newElementList = elementList
      ?.map(el => {
        const matchElement =
          decisionElementList.find(
            (element: any) => element.name === el.name
          ) || {};

        return {
          ...el,
          ...matchElement,
          searchCodeText: getSearchCodeText(el?.searchCode)
        };
      })
      .filter((el: any) => el.configurable || el.isNewElement);

    setElements(newElementList);
    isFunction(handleSetElementList) && handleSetElementList(newElementList);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [decisionElementList]);

  const totalElement = elementList?.length || 0;

  return {
    elements,
    totalElement
  };
};
