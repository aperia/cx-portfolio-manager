import { render, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { mockUseModalRegistryFnc } from 'app/utils';
import 'app/utils/_mockComponent/mockModalRegistry';
import 'app/utils/_mockComponent/mockUseTranslation';
import React from 'react';
import * as reactRedux from 'react-redux';
import AddNewTableModal, { AddNewTableAction } from './AddNewTableModal';
import * as hooks from './useAddNewTable';

const mockUseModalRegistry = mockUseModalRegistryFnc();
const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return {
    __esModule: true,
    default: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const mockOnConfirmParams = jest.fn();
jest.mock('app/hooks/useUnsavedChangesRedirect', () => {
  return {
    __esModule: true,
    useUnsavedChangesRedirect:
      () =>
      ({ onConfirm, onDiscard, onCancel }: any) => {
        onDiscard && onDiscard();
        onCancel && onCancel();
        onConfirm && onConfirm(mockOnConfirmParams());
      }
  };
});

jest.mock('./GridDecision', () => ({
  __esModule: true,
  default: ({}: any) => {
    return (
      <div>
        <h1>GridDecision</h1>
      </div>
    );
  }
}));

HTMLCanvasElement.prototype.getContext = jest.fn();

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <AddNewTableModal {...props} />
    </div>
  );
};

describe('WorkflowManageMethodLevelProcessingDecisionTablesMLP > ConfigureParameters > AddNewTableModal', () => {
  const mockDispatch = jest.fn();

  const id = 'id';
  const show = true;
  const handleClose = jest.fn();
  const selectedVersion = null;
  const handleSetExpandedList = () => jest.fn();
  const handleSelectVersion = jest.fn();
  const expandedList = {};
  const handleOpenConfigModal = jest.fn();
  const isCreateNewVersion = false;
  const handleSetIsCreateNewVersion = jest.fn();
  const handleSetDraftNewTable = jest.fn();
  const draftNewTable = null;
  const handleAddNewTable = jest.fn();
  const selectedTable = null;
  const handleEditTable = jest.fn();
  const handleSetSelectedTable = jest.fn();
  const handleCheckDuplicateTableId = jest.fn();
  const handleChangeFieldValue = () => jest.fn();
  const handleBlurField = () => jest.fn();
  const handleSetIsDuplicateTableId = jest.fn();
  const handleSetElementListHasError = jest.fn();

  const defaultProps = {
    id,
    show,
    handleClose,
    selectedVersion,
    handleOpenConfigModal,
    handleSetExpandedList,
    handleSelectVersion,
    handleSetDraftNewTable,
    expandedList,
    handleAddNewTable,
    isCreateNewVersion,
    handleSetIsCreateNewVersion,
    selectedTable,
    draftNewTable,
    handleEditTable,
    handleSetSelectedTable,
    handleCheckDuplicateTableId,
    handleSetElementListHasError,
    handleSetIsDuplicateTableId
  };

  beforeEach(() => {
    mockUseModalRegistry.mockImplementation(() => [true, false]);
    mockUseDispatch.mockImplementation(() => mockDispatch);
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
    mockUseModalRegistry.mockClear();
    jest.clearAllMocks();
  });

  it('Should render add new table Modal', () => {
    jest.spyOn(hooks, 'useAddNewTable').mockImplementation(() => {
      return {
        searchValue: 'abc',
        handleChangeFieldValue,
        handleBlurField,
        handleSetElementListHasError,
        handleSetIsDuplicateTableId,
        errors: {},
        tableId: 'test',
        tableName: 'test'
      } as any;
    });
    const wrapper = renderComponent({ ...defaultProps, type: 'AQ' });
    expect(
      wrapper.getByText(
        'txt_manage_transaction_level_tlp_config_tq_tableDetails'
      )
    ).toBeInTheDocument();

    const cancelBtn = wrapper.getByText('txt_cancel');
    userEvent.click(cancelBtn);
    expect(handleClose).toBeCalled();

    const saveBtn = wrapper.getByText('txt_save');
    userEvent.click(saveBtn);
  });

  it('Should render add new table Modal with create new version click button back', () => {
    jest.spyOn(hooks, 'useAddNewTable').mockImplementation(() => {
      return {
        searchValue: '',
        handleChangeFieldValue,
        handleBlurField,
        handleSetElementListHasError,
        handleSetIsDuplicateTableId,
        errors: {},
        tableId: 'test',
        tableName: 'Updated',
        elementList: [{}]
      } as any;
    });
    const wrapper = renderComponent({
      ...defaultProps,
      selectedTable: {
        actionTaken: AddNewTableAction.createNewVersion,
        tableId: 'test',
        tableName: 'test'
      },
      selectedVersion: 'avc'
    });

    const backBtn = wrapper.getByText('txt_back');
    userEvent.click(backBtn);
  });

  it('Should render add new table Modal with create new version', () => {
    jest.spyOn(hooks, 'useAddNewTable').mockImplementation(() => {
      return {
        searchValue: '',
        handleChangeFieldValue,
        handleBlurField,
        handleSetElementListHasError,
        handleSetIsDuplicateTableId,
        errors: {},
        tableId: 'test',
        tableName: 'Updated'
      } as any;
    });
    const wrapper = renderComponent({
      ...defaultProps,
      selectedTable: {
        actionTaken: 'edit',
        tableId: 'test',
        tableName: 'test'
      },
      type: 'CA'
    });

    const saveBtn = wrapper.getByText('txt_save');
    userEvent.click(saveBtn);

    const backBtn = wrapper.getByText('txt_back');
    userEvent.click(backBtn);
    expect(handleOpenConfigModal).toBeCalled();
  });

  it('Should render add new table Modal with add table to model', () => {
    jest.spyOn(hooks, 'useAddNewTable').mockImplementation(() => {
      return {
        searchValue: '',
        handleChangeFieldValue,
        handleBlurField,
        handleSetElementListHasError,
        errors: {},
        tableName: 'Updated',
        isDuplicateTableId: true,
        elementList: [{ name: 'ab' }],
        handleSetIsDuplicateTableId
      } as any;
    });
    const wrapper = renderComponent({
      ...defaultProps,
      selectedVersion: {
        tableId: 'test'
      },
      handleCheckDuplicateTableId: () => true
    });
    const saveBtn = wrapper.getByText('txt_save');
    userEvent.click(saveBtn);
  });

  it('Should render add new table Modal with add table to model', () => {
    jest.spyOn(hooks, 'useAddNewTable').mockImplementation(() => {
      return {
        searchValue: '',
        handleChangeFieldValue,
        handleBlurField,
        handleSetElementListHasError,
        handleSetIsDuplicateTableId,
        errors: {},
        tableId: '',
        tableName: 'Updated'
      } as any;
    });
    const wrapper = renderComponent({
      ...defaultProps,
      selectedVersion: {
        tableId: 'test'
      }
    });
    expect(
      wrapper.getByText(
        'txt_manage_transaction_level_tlp_config_tq_tableDetails'
      )
    ).toBeInTheDocument();
  });

  it('Should render add new table Modal with create new vertion action', () => {
    jest.spyOn(hooks, 'useAddNewTable').mockImplementation(() => {
      return {
        searchValue: '',
        handleChangeFieldValue,
        handleBlurField,
        handleSetElementListHasError,
        handleSetIsDuplicateTableId,
        errors: {},
        tableId: '123',
        tableName: 'Updated'
      } as any;
    });
    const wrapper = renderComponent({
      ...defaultProps,
      selectedVersion: {
        tableId: 'test'
      },
      isCreateNewVersion: true
    });
    expect(
      wrapper.getByText(
        'txt_manage_transaction_level_tlp_config_tq_tableDetails'
      )
    ).toBeInTheDocument();
  });

  it('Should render add new table Modal', () => {
    handleCheckDuplicateTableId.mockReturnValue(true);
    jest.spyOn(hooks, 'useAddNewTable').mockImplementation(() => {
      return {
        searchValue: '',
        handleChangeFieldValue,
        handleBlurField,
        handleSetIsDuplicateTableId,
        handleSetElementListHasError,
        errors: {},
        tableId: '123',
        tableName: 'Updated',
        elementList: [{ name: null }]
      } as any;
    });
    const wrapper = renderComponent({
      ...defaultProps,
      isCreateNewVersion: true,
      handleCheckDuplicateTableId
    });

    userEvent.click(wrapper.getByText('txt_cancel'));

    userEvent.click(wrapper.getByText('txt_save'));
  });
});
