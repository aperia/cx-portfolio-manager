import {
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { useUnsavedChangeRegistry } from 'app/hooks';
import { InlineMessage, useTranslation } from 'app/_libraries/_dls';
import { StateFile } from 'app/_libraries/_dls/components/Upload/File';
import { isEqual } from 'app/_libraries/_dls/lodash';
import { isEmpty } from 'lodash';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, { useEffect, useRef, useState } from 'react';
import ActionButtons from './ActionButtons';
import AddNewTableModal from './AddNewTableModal';
import ElementList from './ElementList';
import parseFormValues from './parseFormValues';
import Summary from './Summary';
import TableListModal from './TableListModal';
import { CONFIG_ACTIONS_MODAL, useConfigAction } from './useConfigActions';

export interface ConfigureParametersValue {
  isValid?: boolean;
  tables?: (any & { rowId?: string })[];
}
interface IDependencies {
  files?: StateFile[];
}

const ConfigureParameters: React.FC<
  WorkflowSetupProps<ConfigureParametersValue, IDependencies> & {
    clearFormName: string;
  }
> = ({
  savedAt,
  isStuck,
  formValues,
  setFormValues,
  clearFormName,
  clearFormValues,
  selectedStep,
  dependencies
}) => {
  const keepRef = useRef({ setFormValues });
  const { t } = useTranslation();
  keepRef.current.setFormValues = setFormValues;
  const [initialValues, setInitialValues] = useState(formValues);

  const {
    openedModal,
    handleOpenConfigModal,
    selectedVersion,
    handleSetSelectedVersion,
    handleSetDraftNewTable,
    draftNewTable,
    tableList,
    handleEditTable,
    handleAddNewTable,
    selectedTable,
    handleDeleteTable,
    handleSetSelectedTable,
    handleSetTable
  } = useConfigAction(selectedStep);

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName: UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__LEVEL_PROCESSING_ST,
      priority: 1
    },
    [!isEqual(formValues.tables, initialValues.tables)]
  );

  useEffect(() => {
    setInitialValues(formValues);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [savedAt]);

  useEffect(() => {
    const isValid = (formValues?.tables?.length || 0) > 0;
    if (formValues.isValid === isValid) return;

    keepRef.current.setFormValues({ isValid });
  }, [formValues]);

  useEffect(() => {
    keepRef.current.setFormValues({ tables: tableList });
  }, [tableList]);

  useEffect(() => {
    handleSetTable(formValues.tables);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="mt-24">
      {isStuck && (
        <InlineMessage className="mb-24 mt-0" variant="danger" withIcon>
          {t('txt_step_stuck_move_forward_message')}
        </InlineMessage>
      )}

      {!isEmpty(tableList) ? (
        <ElementList
          data={tableList}
          handleEdit={handleSetSelectedTable}
          handleDelete={handleDeleteTable}
          handleOpenConfigModal={handleOpenConfigModal}
        />
      ) : (
        <ActionButtons
          onClickCreateNewVersion={() =>
            handleOpenConfigModal(CONFIG_ACTIONS_MODAL.tableListModal)
          }
        />
      )}

      {openedModal === CONFIG_ACTIONS_MODAL.tableListModal && (
        <TableListModal
          id="editMethodFromModel"
          show
          handleClose={() => {
            handleOpenConfigModal(null);
            if (!isEmpty(draftNewTable)) handleSetDraftNewTable(null);
          }}
          selectedVersion={selectedVersion}
          handleSelectVersion={handleSetSelectedVersion}
          handleSetDraftNewTable={handleSetDraftNewTable}
          draftNewTable={draftNewTable}
          handleOpenConfigModal={handleOpenConfigModal}
        />
      )}

      {openedModal === CONFIG_ACTIONS_MODAL.addNewTableModal && (
        <AddNewTableModal
          id="addNewMethod"
          show
          handleClose={() => handleOpenConfigModal(null)}
          selectedVersion={selectedVersion}
          handleSelectVersion={handleSetSelectedVersion}
          handleSetDraftNewTable={handleSetDraftNewTable}
          draftNewTable={draftNewTable}
          selectedTable={selectedTable}
          handleEditTable={handleEditTable}
          handleAddNewTable={handleAddNewTable}
          handleSetSelectedTable={handleSetSelectedTable}
          handleOpenConfigModal={handleOpenConfigModal}
        />
      )}
    </div>
  );
};

const ExtraStaticConfigureParameters =
  ConfigureParameters as WorkflowSetupStaticProp;

ExtraStaticConfigureParameters.summaryComponent = Summary;
ExtraStaticConfigureParameters.defaultValues = {
  isValid: false,
  tables: []
};
ExtraStaticConfigureParameters.parseFormValues = parseFormValues;

export default ExtraStaticConfigureParameters;
