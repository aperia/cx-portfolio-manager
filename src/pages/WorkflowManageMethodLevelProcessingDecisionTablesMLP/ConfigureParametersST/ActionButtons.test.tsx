import { fireEvent, render } from '@testing-library/react';
import React from 'react';
import ActionButtons from './ActionButtons';

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

describe('WorkflowManageMethodLevelProcessingDecisionTablesMLP > ConfigureParameters> ActionButtons', () => {
  it('Should render with large buttons', () => {
    const props = {
      onClickCreateNewVersion: jest.fn()
    };
    const wrapper = render(<ActionButtons {...props} />);
    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    );
    expect(props.onClickCreateNewVersion).toHaveBeenCalled();
  });
});
