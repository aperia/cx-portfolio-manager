import { act, renderHook } from '@testing-library/react-hooks';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageAccountLevel';
import * as reactRedux from 'react-redux';
import { useTableListModal } from './useTableListModal';

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');
const TableListMockData = [
  {
    tableId: 'LUMEJ598',
    tableName: '8A41RDTB',
    versions: [
      {
        tableId: 'IFAA8XEE',
        tableName: 'WFRKLMZB',
        effectiveDate: '2021-05-31T17:00:00Z',
        status: 'Production',
        comment: 'at tempor accusam',
        tableControlParameters: [],
        decisionElements: [],
        versions: [
          {
            tableName: '123'
          }
        ]
      },
      {
        tableId: 'K1TIBA4C',
        tableName: 'ZHFMK7K2',
        effectiveDate: '2021-05-28T17:00:00Z',
        status: 'Client Approved',
        comment: 'eu labore congue ea',
        tableControlParameters: [],
        decisionElements: []
      },
      {
        tableId: 'LUMEJ598',
        tableName: '91IDUIVL',
        effectiveDate: '2021-10-17T17:00:00Z',
        status: 'Validating',
        comment: 'nulla aliquip cum magna iriure ex velit',
        tableControlParameters: [],
        decisionElements: [],
        versions: [
          {
            tableName: '123'
          }
        ]
      }
    ]
  }
];

const SelectedVersion = {
  tableId: 'LUMEJ598',
  version: {
    tableId: 'IFAA8XEE',
    tableName: 'WFRKLMZB',
    effectiveDate: '2021-05-31T17:00:00Z',
    status: 'Production',
    comment: 'at tempor accusam',
    tableControlParameters: [],
    decisionElements: [],
    versions: [
      {
        tableName: '123'
      }
    ]
  }
};

const useSelectTableListData = jest.spyOn(
  WorkflowSetup,
  'useSelectTableListData'
);

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

describe('WorkflowManageMethodLevelProcessingDecisionTablesMLP > ConfigureParameters > useTableListModal', () => {
  const mockDispatch = jest.fn();

  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
  });

  it('Run Default reducer function', () => {
    useSelectTableListData.mockReturnValue({});

    const { result } = renderHook(() => useTableListModal(null));

    act(() => {
      result.current.dispatch({ type: 'Test Action' });
    });
  });

  it('Should run normal hook', () => {
    useSelectTableListData.mockReturnValue({
      tableList: TableListMockData
    });
    const { result } = renderHook(() => useTableListModal(null));
    act(() => {
      result.current.handleChangePage(2);
    });
    expect(result.current.page).toEqual(2);

    act(() => {
      result.current.handleChangePageSize(10);
    });
    expect(result.current.pageSize).toEqual(10);

    act(() => {
      result.current.handleSetShowVersionDetail(true);
      result.current.handleChangeOrderBy('desc');
      result.current.handleChangeSortBy('tableName');
      result.current.handleSearch('abc');
      result.current.handleSearch('abc');
    });

    expect(result.current.searchValue).toEqual('abc');

    act(() => {
      result.current.handleClearAndReset();
    });
    expect(result.current.searchValue).toEqual('');
  });

  it('It should render hook with selected version', () => {
    useSelectTableListData.mockReturnValue({
      tableList: TableListMockData
    });
    const { result } = renderHook(() => useTableListModal(SelectedVersion));

    act(() => {
      result.current.handleSearch('abc');
      result.current.handleClearAndReset();
    });
  });

  it('It should render hook with none selected version', () => {
    useSelectTableListData.mockReturnValue({
      tableList: TableListMockData
    });
    const { result } = renderHook(() => useTableListModal());

    act(() => {
      result.current.handleSearch('abc');
      result.current.handleClearAndReset();
    });
  });

  it('It should render hook with selected version Not In List', () => {
    useSelectTableListData.mockReturnValue({
      tableList: TableListMockData
    });
    renderHook(() => useTableListModal({ tableId: 'test', version: {} }));
  });
});
