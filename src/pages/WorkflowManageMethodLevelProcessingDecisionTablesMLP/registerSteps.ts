import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import {
  stepRegistry,
  uploadFileStepRegistry
} from '../_commons/WorkflowSetup/registries';
import ConfigureParameters from './ConfigureParameters';
import ConfigureParametersST from './ConfigureParametersST';
import GettingStartStep from './GettingStartStep';
import TableList from './TableList';

stepRegistry.registerStep(
  'GetStartedManageMethodLevelProcessingDecisionTables',
  GettingStartStep,
  [
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__LEVEL_PROCESSING__EXISTED_WORKFLOW,
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__LEVEL_PROCESSING__EXISTED_WORKFLOW__STUCK
  ]
);

stepRegistry.registerStep(
  'ConfigureParameterLevelProcessingAQWorkflow',
  ConfigureParameters,
  [UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__LEVEL_PROCESSING_AQ]
);

stepRegistry.registerStep(
  'ConfigureParameterLevelProcessingCAWorkflow',
  ConfigureParameters,
  [UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__LEVEL_PROCESSING_CA]
);

stepRegistry.registerStep(
  'ConfigureParameterLevelProcessingSTWorkflow',
  ConfigureParametersST,
  [UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__LEVEL_PROCESSING_ST]
);

uploadFileStepRegistry.registerComponent('MLPTableList', TableList);
