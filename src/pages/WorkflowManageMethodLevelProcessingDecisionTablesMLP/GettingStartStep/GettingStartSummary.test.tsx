import { fireEvent, render, RenderResult } from '@testing-library/react';
import React from 'react';
import GettingStartSummary from './GettingStartSummary';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <GettingStartSummary {...props} />
    </div>
  );
};

describe('WorkflowManageMethodLevelProcessingDecisionTablesMLP > GettingStartStep > GettingStartSummary', () => {
  it('Should render summary contains CA Check Charges', () => {
    const wrapper = renderComponent({
      formValues: {
        ca: true
      }
    } as WorkflowSetupSummaryProps);

    expect(wrapper.getByText('Client Allocation (CA)')).toBeInTheDocument;
  });

  it('Should click on edit', () => {
    const mockFn = jest.fn();
    const wrapper = renderComponent({
      formValues: {
        ca: true
      },
      onEditStep: mockFn
    } as WorkflowSetupSummaryProps);

    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(mockFn).toHaveBeenCalled();
  });
});
