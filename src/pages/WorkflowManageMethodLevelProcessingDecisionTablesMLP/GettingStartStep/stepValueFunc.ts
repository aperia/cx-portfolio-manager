import { FormGettingStart } from '.';

const stepValueFunc: WorkflowSetupStepValueFunc<FormGettingStart> = ({
  stepsForm: formValues
}) => {
  const { aq, ca, st } = formValues || {};

  const arrStr = [];

  if (aq) {
    arrStr.push('Account Qualification (AQ)');
  }

  if (ca) {
    arrStr.push('Client Allocation (CA)');
  }

  if (st) {
    arrStr.push('Selection Table (ST)');
  }

  return arrStr.length > 2 ? arrStr.join(', ') : arrStr.join(' and ');
};

export default stepValueFunc;
