import { fireEvent, render, RenderResult } from '@testing-library/react';
import { mockUseDispatchFnc } from 'app/utils';
import React from 'react';
import GettingStartStep, { FormGettingStart } from './index';

const mockUseDispatch = mockUseDispatchFnc();

beforeEach(() => {
  mockUseDispatch.mockImplementation(() => jest.fn());
});

afterEach(() => {
  mockUseDispatch.mockClear();
});

jest.mock('pages/_commons/redux/WorkflowSetup', () => ({
  actionsWorkflowSetup: { getWorkflowMetadata: jest.fn() },
  useSelectElementMetadataManageMethodLevelProcessingMLP: () => ({
    mlpTableConfigOptions: [
      {
        value: '',
        description: 'None',
        code: '',
        text: 'None'
      },
      {
        value: 'D',
        description:
          'Deny the item if it exceeds the amount in the LIMIT field.',
        code: 'D'
      },
      {
        value: 'Q',
        description:
          'Assign the item to the pending queue if it exceeds the amount in the LIMIT field.',
        code: 'Q',
        text: 'Q - Assign the item to the pending queue if it exceeds the amount in the LIMIT field.'
      }
    ]
  })
}));

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});
jest.mock('app/hooks/useUnsavedChangeRegistry', () => {
  return {
    __esModule: true,
    useUnsavedChangeRegistry: (options: any, changes: any, onConfirm: any) => {
      onConfirm && onConfirm();
    }
  };
});

jest.mock('pages/_commons/OverviewModal', () => ({
  __esModule: true,
  default: ({ onClose }: any) => {
    return (
      <div>
        <div>OverviewModal</div>
        <button onClick={onClose}>OverviewButtonCloseModal</button>
      </div>
    );
  }
}));

jest.mock('pages/_commons/ContentExpand', () => ({
  __esModule: true,
  default: ({ instruction, children }: any) => (
    <div>
      <div>ContentExpand</div>
      <div>{instruction}</div>
      <div>{children}</div>
    </div>
  )
}));

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <GettingStartStep {...props} />
    </div>
  );
};

describe('ManagePenaltyFeeWorkflow > GettingStartStep > Index', () => {
  it('Should render without Overview modal > Open overview modal', () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: {},
      setFormValues: mockFn,
      isStuck: true
    } as WorkflowSetupProps<FormGettingStart>;

    const wrapper = renderComponent(props);
    expect(wrapper.queryByText(/OverviewModal/)).not.toBeInTheDocument;
    wrapper.rerender(
      <div>
        <GettingStartStep
          {...props}
          stepId="1"
          formValues={{ alreadyShown: true, aq: true }}
        />
      </div>
    );

    fireEvent.click(wrapper.getByText('txt_view_overview'));
    expect(wrapper.queryByText(/OverviewModal/)).toBeInTheDocument;
  });

  it('Should render Overview modal > Close overview modal', () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: {
        ca: true,
        isValid: true
      },
      setFormValues: mockFn
    } as WorkflowSetupProps<FormGettingStart>;

    const wrapper = renderComponent(props);
    expect(wrapper.queryByText(/OverviewModal/)).toBeInTheDocument;

    fireEvent.click(wrapper.getByText('OverviewButtonCloseModal'));
    expect(wrapper.queryByText(/OverviewModal/)).not.toBeInTheDocument;
  });

  it('Select Change option', () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: {
        st: true,
        isValid: true
      },
      aqFormId: 'id',
      aqUploadFileFormId: 'id',
      caFormId: 'id',
      caUploadFileFormId: 'id',
      stFormId: 'id',
      stUploadFileFormId: 'id',
      hasInstance: true,
      setFormValues: mockFn,
      clearFormValues: jest.fn()
    } as unknown as WorkflowSetupProps<FormGettingStart>;

    const wrapper = renderComponent(props);
    const checkbox = wrapper.baseElement.querySelector(
      'input[class="custom-control-input dls-checkbox-input"]'
    ) as Element;
    const _checkbox = checkbox as HTMLInputElement;
    expect(_checkbox.checked).toEqual(false);

    fireEvent.click(checkbox);
    expect(_checkbox.checked).toEqual(true);
  });

  it('Select Change option', () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: {
        st: false,
        isValid: true
      },
      aqFormId: 'id',
      aqUploadFileFormId: 'id',
      caFormId: 'id',
      caUploadFileFormId: 'id',
      stFormId: 'id',
      stUploadFileFormId: 'id',
      hasInstance: true,
      setFormValues: mockFn,
      clearFormValues: jest.fn()
    } as unknown as WorkflowSetupProps<FormGettingStart>;

    const wrapper = renderComponent(props);
    const checkbox = wrapper.baseElement.querySelector(
      'input[class="custom-control-input dls-checkbox-input"]'
    ) as Element;
    const _checkbox = checkbox as HTMLInputElement;
    expect(_checkbox.checked).toEqual(false);

    fireEvent.click(checkbox);
    expect(_checkbox.checked).toEqual(true);
  });
});
