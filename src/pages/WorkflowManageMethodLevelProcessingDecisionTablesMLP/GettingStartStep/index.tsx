import { ConfigTableOptionsEnum } from 'app/constants/enums';
import { WORKFLOW_SETUP } from 'app/constants/local-storage';
import {
  unsavedExistedWorkflowSetupDataOnStuckProps,
  unsavedExistedWorkflowSetupDataProps,
  unsavedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { useUnsavedChangeRegistry } from 'app/hooks';
import {
  Button,
  CheckBox,
  Icon,
  InlineMessage,
  TransDLS,
  useTranslation
} from 'app/_libraries/_dls';
import ContentExpand from 'pages/_commons/ContentExpand';
import OverviewModal from 'pages/_commons/OverviewModal';
import {
  actionsWorkflowSetup,
  useSelectElementMetadataManageMethodLevelProcessingMLP
} from 'pages/_commons/redux/WorkflowSetup';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';
import GettingStartSummary from './GettingStartSummary';
import parseFormValues from './parseFormValues';
import stepValueFunc from './stepValueFunc';

export interface FormGettingStart {
  isValid?: boolean;

  aq?: boolean;
  ca?: boolean;
  st?: boolean;

  alreadyShown?: boolean;
}
export interface GettingStartProps
  extends WorkflowSetupProps<FormGettingStart> {
  aqFormId?: string;
  aqUploadFileFormId?: string;
  caFormId?: string;
  caUploadFileFormId?: string;
  stFormId?: string;
  stUploadFileFormId?: string;
}
const GettingStartStep: React.FC<GettingStartProps> = ({
  title,
  stepId,
  selectedStep,
  savedAt,
  formValues,
  hasInstance,
  isStuck,

  aqFormId,
  aqUploadFileFormId,
  caFormId,
  caUploadFileFormId,
  stFormId,
  stUploadFileFormId,

  setFormValues,
  clearFormValues
}) => {
  const { t } = useTranslation();
  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;
  const dispatch = useDispatch();
  const [initialValues, setInitialValues] = useState(formValues);
  const [showOverview, setShowOverview] = useState(
    formValues.alreadyShown
      ? false
      : localStorage.getItem(WORKFLOW_SETUP.SHOW_OVERVIEW_AGAIN) !== 'false'
  );

  const metadata = useSelectElementMetadataManageMethodLevelProcessingMLP();
  const { mlpTableConfigOptions } = metadata;
  useEffect(() => {
    dispatch(
      actionsWorkflowSetup.getWorkflowMetadata([
        ConfigTableOptionsEnum.mlpTable
      ])
    );
  }, [dispatch]);

  useEffect(() => {
    const isValid = !!formValues.aq || !!formValues.ca || !!formValues.st;
    if (formValues.isValid === isValid) return;

    keepRef.current.setFormValues({ isValid });
  }, [formValues]);

  useEffect(() => {
    setInitialValues(formValues);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [savedAt]);

  useUnsavedChangeRegistry(
    {
      ...unsavedWorkflowSetupDataProps,
      formName: UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__LEVEL_PROCESSING,
      priority: 1
    },
    [!hasInstance && (!!formValues.ca || !!formValues.st || !!formValues.aq)]
  );

  const handleConfirmChangeDependencies = useCallback(() => {
    aqFormId && !formValues.aq && clearFormValues(aqFormId);
    aqUploadFileFormId && !formValues.aq && clearFormValues(aqUploadFileFormId);

    caFormId && !formValues.ca && clearFormValues(caFormId);
    caUploadFileFormId && !formValues.ca && clearFormValues(caUploadFileFormId);

    stFormId && !formValues.st && clearFormValues(stFormId);
    stUploadFileFormId && !formValues.st && clearFormValues(stUploadFileFormId);
  }, [
    clearFormValues,
    formValues.aq,
    formValues.ca,
    formValues.st,
    aqFormId,
    aqUploadFileFormId,
    caFormId,
    caUploadFileFormId,
    stFormId,
    stUploadFileFormId
  ]);

  const isUncheckAll = useMemo(
    () => !formValues.aq && !formValues.ca && !formValues.st,
    [formValues.aq, formValues.ca, formValues.st]
  );
  const hasChanged = useMemo(
    () =>
      initialValues.ca !== formValues.ca ||
      initialValues.st !== formValues.st ||
      initialValues.aq !== formValues.aq,
    [initialValues, formValues]
  );

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__LEVEL_PROCESSING__EXISTED_WORKFLOW,
      priority: 1,
      confirmFirst: true
    },
    [!isUncheckAll && !!hasInstance && hasChanged],
    handleConfirmChangeDependencies
  );
  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataOnStuckProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__LEVEL_PROCESSING__EXISTED_WORKFLOW__STUCK,
      priority: 1,
      confirmFirst: true
    },
    [isUncheckAll && !!hasInstance && hasChanged]
  );

  const handleChangeOption = (option: keyof FormGettingStart) => {
    const onChange = (e: any) => {
      setFormValues({ [option]: !formValues[option] });
    };

    return onChange;
  };

  return (
    <React.Fragment>
      <ContentExpand
        isSelectedStep={stepId === selectedStep}
        change={savedAt}
        instruction={
          <div className="pt-24 px-24">
            <div className="d-flex align-items-center justify-content-between">
              <h4>{title}</h4>
              <Button
                className="mr-n8"
                variant="outline-primary"
                size="sm"
                onClick={() => setShowOverview(true)}
              >
                {t('txt_view_overview')}
              </Button>
            </div>
            <div className="pb-24">
              {isStuck && (
                <InlineMessage className="mb-0 mt-24" variant="danger" withIcon>
                  {t('txt_step_stuck_move_forward_message')}
                </InlineMessage>
              )}
              <div className="row">
                <div className="col-6 mt-24">
                  <div className="bg-light-l20 rounded-lg p-16">
                    <div className="text-center">
                      <Icon
                        name="request"
                        size="12x"
                        className="color-grey-l16"
                      />
                    </div>
                    <p className="mt-8 color-grey">
                      {t(
                        'txt_manage_method_level_processing_decision_tables_get_started_desc_1'
                      )}
                    </p>
                    <p className="mt-16 fw-600">
                      {t(
                        'txt_manage_method_level_processing_decision_tables_get_started_desc_highlight_1'
                      )}
                    </p>
                    <p className="mt-8 color-grey">
                      {t(
                        'txt_manage_method_level_processing_decision_tables_get_started_desc_2'
                      )}
                    </p>
                    <ul className="list-dot color-grey">
                      <li>
                        {t(
                          'txt_manage_method_level_processing_decision_tables_get_started_desc_2_1'
                        )}
                      </li>
                      <li>
                        {t(
                          'txt_manage_method_level_processing_decision_tables_get_started_desc_2_2'
                        )}
                      </li>
                    </ul>
                    <p className="mt-8 color-grey">
                      {t(
                        'txt_manage_method_level_processing_decision_tables_get_started_desc_3'
                      )}
                    </p>
                    <ul className="list-dot color-grey">
                      <li>
                        {t(
                          'txt_manage_method_level_processing_decision_tables_get_started_desc_4_1'
                        )}
                      </li>
                      <li>
                        {t(
                          'txt_manage_method_level_processing_decision_tables_get_started_desc_4_2'
                        )}
                      </li>
                      <li>
                        {t(
                          'txt_manage_method_level_processing_decision_tables_get_started_desc_4_3'
                        )}
                      </li>
                      <li>
                        {t(
                          'txt_manage_method_level_processing_decision_tables_get_started_desc_4_4'
                        )}
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="col-6 mt-24 color-grey">
                  <p>
                    {t(
                      'txt_manage_method_level_processing_decision_tables_get_started_steps_guides_title'
                    )}
                  </p>
                  <p className="mt-8">
                    <TransDLS keyTranslation="txt_manage_method_level_processing_decision_tables_get_started_steps_guides_1">
                      <strong className="color-grey-d20" />
                    </TransDLS>
                  </p>
                  <p className="mt-8">
                    <TransDLS keyTranslation="txt_manage_method_level_processing_decision_tables_get_started_steps_guides_2">
                      <strong className="color-grey-d20" />
                    </TransDLS>
                  </p>
                  <p className="mt-8">
                    <TransDLS keyTranslation="txt_manage_method_level_processing_decision_tables_get_started_steps_guides_3">
                      <strong className="color-grey-d20" />
                    </TransDLS>
                  </p>
                  <p className="mt-8">
                    <TransDLS keyTranslation="txt_manage_method_level_processing_decision_tables_get_started_steps_guides_4">
                      <strong className="color-grey-d20" />
                    </TransDLS>
                  </p>
                  <p className="mt-8">
                    <TransDLS keyTranslation="txt_manage_method_level_processing_decision_tables_get_started_steps_guides_5">
                      <strong className="color-grey-d20" />
                    </TransDLS>
                  </p>
                </div>
              </div>
            </div>
          </div>
        }
      >
        <div className="p-24">
          <h5>
            <TransDLS keyTranslation="txt_manage_method_level_processing_decision_tables_get_started_select_the_MLP">
              <strong className="color-red" />
            </TransDLS>
          </h5>
          <div className="row mt-16 list-cards list-cards--selectable list-cards--single">
            {Array.isArray(mlpTableConfigOptions) &&
              mlpTableConfigOptions.map(o => (
                <div key={o.code} className="col-4 col-lg-4">
                  <label
                    className="list-cards__item custom-control-root"
                    htmlFor={o.code}
                  >
                    <span className="d-flex align-items-center">
                      <span className="pr-8">{t(o.text)}</span>
                    </span>
                    <CheckBox className="mr-n4">
                      <CheckBox.Input
                        id={o.code}
                        checked={
                          formValues[
                            o.code?.toLocaleLowerCase() as keyof FormGettingStart
                          ]
                        }
                        onChange={handleChangeOption(
                          o.code?.toLocaleLowerCase() as any
                        )}
                      />
                    </CheckBox>
                  </label>
                </div>
              ))}
          </div>
        </div>
      </ContentExpand>
      {showOverview && (
        <OverviewModal
          id="overviewWorkflowSetup"
          show
          onClose={() => {
            setShowOverview(false);
            keepRef.current.setFormValues({ alreadyShown: true });
          }}
        />
      )}
    </React.Fragment>
  );
};

const ExtraStaticGettingStartStep =
  GettingStartStep as WorkflowSetupStaticProp<FormGettingStart>;

ExtraStaticGettingStartStep.summaryComponent = GettingStartSummary;
ExtraStaticGettingStartStep.stepValue = stepValueFunc;
ExtraStaticGettingStartStep.defaultValues = {
  isValid: false,
  aq: false,
  st: false,
  ca: false
};
ExtraStaticGettingStartStep.parseFormValues = parseFormValues;

export default ExtraStaticGettingStartStep;
