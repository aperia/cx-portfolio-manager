import { FormGettingStart } from '.';
import stepValueFunc from './stepValueFunc';

describe('ManagePenaltyFeeWorkflow > GettingStartStep > stepValueFunc', () => {
  it('Should AQ changes and st changes', () => {
    const stepsForm = {
      aq: true,
      st: true,
      ca: true
    } as FormGettingStart;

    const response = stepValueFunc({ stepsForm });
    expect(response).toEqual(
      'Account Qualification (AQ), Client Allocation (CA), Selection Table (ST)'
    );
  });

  it('Should AQ changes and st changes', () => {
    const stepsForm = {
      aq: true,
      st: true
    } as FormGettingStart;

    const response = stepValueFunc({ stepsForm });
    expect(response).toEqual(
      'Account Qualification (AQ) and Selection Table (ST)'
    );
  });

  it('Should CA changes', () => {
    const stepsForm = {
      ca: true
    } as FormGettingStart;

    const response = stepValueFunc({ stepsForm });
    expect(response).toEqual('Client Allocation (CA)');
  });

  it('Should Return empty', () => {
    const stepsForm: any = undefined;

    const response = stepValueFunc({ stepsForm });
    expect(response).toEqual('');
  });
});
