import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { default as stepRegistry } from '../_commons/WorkflowSetup/registries/stepRegistry';

const registerFunc = jest.spyOn(stepRegistry, 'registerStep');

describe('WorkflowManageMethodLevelProcessingDecisionTablesMLP > registerSteps', () => {
  it('Should register components', async () => {
    await import('./registerSteps');

    expect(registerFunc).toBeCalledWith(
      'GetStartedManageMethodLevelProcessingDecisionTables',
      expect.anything(),
      [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__LEVEL_PROCESSING__EXISTED_WORKFLOW,
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__LEVEL_PROCESSING__EXISTED_WORKFLOW__STUCK
      ]
    );
  });
});
