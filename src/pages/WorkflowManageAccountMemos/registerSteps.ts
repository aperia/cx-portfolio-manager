import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import {
  stepRegistry,
  uploadFileStepRegistry
} from '../_commons/WorkflowSetup/registries';
import GettingStartStep from './GettingStartStep';
import ConfigureParameters from './ManageAccountMemosStep';
import SubContent from './ManageAccountMemosStep/SubContent';

stepRegistry.registerStep('GetStartedManageAccountMemos', GettingStartStep, [
  UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_ACCOUNT_MEMOS__GET_STARTED__EXISTED_WORKFLOW
]);

uploadFileStepRegistry.registerComponent(
  'SubContentManageAccountMemos',
  SubContent
);

stepRegistry.registerStep(
  'ConfigureParametersManageAccountMemos',
  ConfigureParameters,
  [UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_ACCOUNT_MEMOS_CONFIRM_EDIT]
);
