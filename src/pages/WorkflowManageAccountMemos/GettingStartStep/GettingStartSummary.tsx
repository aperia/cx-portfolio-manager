import { Button, useTranslation } from 'app/_libraries/_dls';
import { isFunction } from 'lodash';
import React from 'react';
import { FormGettingStart } from '.';
import { ACTIONS_MEMOS, NAME_MEMOS } from './constant';

const GettingStartSummary: React.FC<
  WorkflowSetupSummaryProps<FormGettingStart>
> = ({ formValues, onEditStep }) => {
  const { t } = useTranslation();

  const handleEdit = () => {
    isFunction(onEditStep) && onEditStep();
  };

  const name =
    formValues?.nameMemos === NAME_MEMOS.CIS
      ? t('txt_manage_account_memos_form_3')
      : t('txt_manage_account_memos_form_4');
  let action = '';

  switch (formValues?.actionsMemos) {
    case ACTIONS_MEMOS.ADD:
      action = t('txt_manage_account_memos_form_6');
      break;
    case ACTIONS_MEMOS.FIND:
      action = t('txt_manage_account_memos_form_7');
      break;
    case ACTIONS_MEMOS.DELETE:
    default:
      action = t('txt_manage_account_memos_form_8');
      break;
  }

  return (
    <div className="position-relative">
      <div className="absolute-top-right mt-n26 mr-n8">
        <Button variant="outline-primary" size="sm" onClick={handleEdit}>
          {t('txt_edit')}
        </Button>
      </div>
      <div className="pt-16">
        <div className="border rounded-lg">
          <div className="p-16 border-bottom">
            <div className="row align-items-lg-center">
              <div className="col-lg-5">
                <p>{t('txt_manage_account_memos_form_2')}</p>
              </div>
              <div className="col-lg-7 mt-8 mt-lg-0">
                <div className=" d-flex align-items-center justify-content-lg-end">
                  {name}
                </div>
              </div>
            </div>
          </div>
          <div className="p-16">
            <div className="row align-items-lg-center">
              <div className="col-lg-5">
                <p>{t('txt_manage_account_memos_form_5')}</p>
              </div>
              <div className="col-lg-7 mt-8 mt-lg-0">
                <div className="d-flex align-items-center justify-content-lg-end">
                  {action}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default GettingStartSummary;
