import { FormGettingStart } from '.';
import stepValueFunc from './stepValueFunc';

describe('ManagePenaltyFeeWorkflow > GettingStartStep > stepValueFunc', () => {
  it('Should Return Check Charges and Late Charges', () => {
    const stepsForm = {
      nameMemos: 'Customer Inquiry System (CIS) Memos',
      actionsMemos: 'Add Memos'
    } as FormGettingStart;

    const response = stepValueFunc({ stepsForm });
    expect(response).toEqual('Customer Inquiry System (CIS) Memos - Add Memos');
  });

  it('Should Return Check Charges and Late Charges', () => {
    const stepsForm = {
      nameMemos: 'Customer Inquiry System (CIS) Memos',
      actionsMemos: 'Find Memos'
    } as FormGettingStart;

    const response = stepValueFunc({ stepsForm });
    expect(response).toEqual(
      'Customer Inquiry System (CIS) Memos - Find Memos'
    );
  });

  it('Should Return Check Charges and Late Charges', () => {
    const stepsForm = {
      nameMemos: 'Customer Inquiry System (CIS) Memos',
      actionsMemos: 'Delete Memos'
    } as FormGettingStart;

    const response = stepValueFunc({ stepsForm });
    expect(response).toEqual(
      'Customer Inquiry System (CIS) Memos - Delete Memos'
    );
  });

  it('Should Return Check Charges and Late Charges', () => {
    const stepsForm = {
      nameMemos: '',
      actionsMemos: ''
    } as FormGettingStart;

    const response = stepValueFunc({ stepsForm });
    expect(response).toEqual('');
  });

  it('Should Return Check Charges and Late Charges', () => {
    const response = stepValueFunc({});
    expect(response).toEqual('');
  });

  it('Should Return Check Charges and Late Charges', () => {
    const stepsForm = {
      nameMemos: 'Chronicle Memos',
      actionsMemos: 'Delete Memos'
    } as FormGettingStart;

    const response = stepValueFunc({ stepsForm });
    expect(response).toEqual('Chronicle Memos - Delete Memos');
  });
});
