import { FormGettingStart } from '.';
import { ACTIONS_MEMOS, NAME_MEMOS } from './constant';

const stepValueFunc: WorkflowSetupStepValueFunc<FormGettingStart> = ({
  stepsForm: formValues
}) => {
  const { nameMemos, actionsMemos } = formValues || {};

  if (nameMemos && actionsMemos) {
    const name =
      formValues?.nameMemos === NAME_MEMOS.CIS
        ? 'Customer Inquiry System (CIS) Memos'
        : 'Chronicle Memos';
    let action = '';

    switch (formValues?.actionsMemos) {
      case ACTIONS_MEMOS.ADD:
        action = 'Add Memos';
        break;
      case ACTIONS_MEMOS.FIND:
        action = 'Find Memos';
        break;
      case ACTIONS_MEMOS.DELETE:
      default:
        action = 'Delete Memos';
        break;
    }
    return `${name} - ${action}`;
  }

  return '';
};

export default stepValueFunc;
