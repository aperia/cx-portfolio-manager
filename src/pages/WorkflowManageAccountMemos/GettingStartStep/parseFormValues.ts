import { getWorkflowSetupStepStatus } from 'app/helpers';
import { FormGettingStart } from '.';

const parseFormValues: WorkflowSetupStepFormDataFunc<FormGettingStart> = (
  data,
  id
) => {
  const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);
  const config = (data?.data as any)?.configurations || {};

  if (!stepInfo || !config) return;

  return {
    ...getWorkflowSetupStepStatus(stepInfo),
    actionsMemos: config?.action,
    nameMemos: config?.memoType
  };
};

export default parseFormValues;
