import { MethodFieldParameterEnum } from 'app/constants/enums';
import { WORKFLOW_SETUP } from 'app/constants/local-storage';
import {
  unsavedExistedWorkflowSetupDataProps,
  unsavedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { useUnsavedChangeRegistry } from 'app/hooks';
import {
  Button,
  Icon,
  InlineMessage,
  Radio,
  TransDLS,
  useTranslation
} from 'app/_libraries/_dls';
import ContentExpand from 'pages/_commons/ContentExpand';
import OverviewModal from 'pages/_commons/OverviewModal';
import {
  actionsWorkflowSetup,
  useSelectElementMetadataManageAcountMemos
} from 'pages/_commons/redux/WorkflowSetup';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, { useEffect, useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import GettingStartSummary from './GettingStartSummary';
import parseFormValues from './parseFormValues';
import stepValueFunc from './stepValueFunc';
export interface FormGettingStart {
  isValid?: boolean;
  alreadyShown?: boolean;
  nameMemos?: string;
  actionsMemos?: string;
}
const GettingStartStep: React.FC<WorkflowSetupProps<FormGettingStart>> = ({
  title,
  stepId,
  selectedStep,
  savedAt,
  formValues,
  hasInstance,
  isStuck,
  setFormValues
}) => {
  const { t } = useTranslation();
  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;
  const dispatch = useDispatch();

  const { nameMemos, actionsMemos } = formValues;

  const metadata = useSelectElementMetadataManageAcountMemos();
  const { memosTypes, memosActions } = metadata;

  const [initialValues, setInitialValues] = useState(formValues);

  const [showOverview, setShowOverview] = useState(
    formValues.alreadyShown
      ? false
      : localStorage.getItem(WORKFLOW_SETUP.SHOW_OVERVIEW_AGAIN) !== 'false'
  );

  const handleChange = (name: string, val: string) => {
    keepRef.current.setFormValues({
      ...formValues,
      isValid: !!nameMemos && !!actionsMemos,
      [name]: val
    });
  };

  useEffect(() => {
    const isValid = !!formValues.nameMemos && !!formValues.actionsMemos;
    if (formValues.isValid === isValid) return;

    keepRef.current.setFormValues({ isValid });
  }, [formValues]);

  useEffect(() => {
    setInitialValues(formValues);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [savedAt]);

  useEffect(() => {
    dispatch(
      actionsWorkflowSetup.getWorkflowMetadata([
        MethodFieldParameterEnum.MemosTypes,
        MethodFieldParameterEnum.MemosActions,
        MethodFieldParameterEnum.MemosTypesFormCIS,
        MethodFieldParameterEnum.MemosTypesFormChronicle,
        MethodFieldParameterEnum.MemosOperator,
        MethodFieldParameterEnum.MemosEnteredIdOperator,
        MethodFieldParameterEnum.MemosSysOperator,
        MethodFieldParameterEnum.MemosPrinOperator,
        MethodFieldParameterEnum.MemosAgentOperator,
        MethodFieldParameterEnum.MemosDateOperator,
        MethodFieldParameterEnum.MemosMemoTextOperator,
        MethodFieldParameterEnum.MemosTypeOperator,
        MethodFieldParameterEnum.MemosTypeOperatorValue,
        MethodFieldParameterEnum.MemosSPAs
      ])
    );
  }, [dispatch]);

  useUnsavedChangeRegistry(
    {
      ...unsavedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_ACCOUNT_MEMOS__GET_STARTED,
      priority: 1
    },
    [!hasInstance && (!!formValues.actionsMemos || !!formValues.nameMemos)]
  );

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_ACCOUNT_MEMOS__GET_STARTED__EXISTED_WORKFLOW,
      priority: 1
    },
    [
      !!hasInstance &&
        (initialValues.actionsMemos !== formValues.actionsMemos ||
          initialValues.nameMemos !== formValues.nameMemos)
    ]
  );

  return (
    <React.Fragment>
      <ContentExpand
        isSelectedStep={stepId === selectedStep}
        change={savedAt}
        instruction={
          <div className="pt-24 px-24">
            <div className="d-flex align-items-center justify-content-between">
              <h4>{title}</h4>
              <Button
                className="mr-n8"
                variant="outline-primary"
                size="sm"
                onClick={() => setShowOverview(true)}
              >
                {t('txt_view_overview')}
              </Button>
            </div>
            <div className="pb-24">
              {isStuck && (
                <InlineMessage className="mb-0 mt-24" variant="danger" withIcon>
                  {t('txt_step_stuck_move_forward_message')}
                </InlineMessage>
              )}

              <div className="row">
                <div className="col-6 mt-24">
                  <div className="bg-light-l20 rounded-lg p-16">
                    <div className="text-center">
                      <Icon
                        name="request"
                        size="12x"
                        className="color-grey-l16"
                      />
                    </div>
                    <p className="mt-8 color-grey">
                      {t('txt_manage_account_memos_left_1')}
                    </p>
                    <p className="mt-16 fw-600">
                      {t('txt_manage_account_memos_left_2')}
                    </p>
                    <p className="mt-8 color-grey">
                      {t('txt_manage_account_memos_left_3')}
                    </p>
                    <ul className="list-dot color-grey">
                      <li>{t('txt_manage_account_memos_left_3_1')}</li>
                      <li>{t('txt_manage_account_memos_left_3_2')}</li>
                      <li>{t('txt_manage_account_memos_left_3_3')}</li>
                    </ul>
                    <p className="mt-16 fw-600">
                      {t('txt_manage_account_memos_left_4')}
                    </p>
                    <p className="mt-8 color-grey">
                      {t('txt_manage_account_memos_left_5')}
                    </p>
                    <ul className="list-dot color-grey">
                      <li>{t('txt_manage_account_memos_left_5_1')}</li>
                      <li>{t('txt_manage_account_memos_left_5_2')}</li>
                    </ul>
                  </div>
                </div>
                <div className="col-6 mt-24 color-grey">
                  <p>{t('txt_manage_account_memos_right_1')}</p>
                  <p className="mt-8">
                    <TransDLS keyTranslation="txt_manage_account_memos_right_2">
                      <strong className="color-grey-d20" />
                    </TransDLS>
                  </p>
                  <p className="mt-8">
                    <TransDLS keyTranslation="txt_manage_account_memos_right_3">
                      <strong className="color-grey-d20" />
                    </TransDLS>
                  </p>
                  <p className="mt-8">
                    <TransDLS keyTranslation="txt_manage_account_memos_right_4">
                      <strong className="color-grey-d20" />
                    </TransDLS>
                  </p>
                  <p className="mt-8 pl-16">
                    <TransDLS keyTranslation="txt_manage_account_memos_right_4_1">
                      <strong className="color-grey-d20" />
                    </TransDLS>
                  </p>
                  <p className="mt-8 pl-16">
                    <TransDLS keyTranslation="txt_manage_account_memos_right_4_2">
                      <strong className="color-grey-d20" />
                    </TransDLS>
                  </p>
                  <p className="mt-8 pl-16">
                    <TransDLS keyTranslation="txt_manage_account_memos_right_4_3">
                      <strong className="color-grey-d20" />
                    </TransDLS>
                  </p>
                  <p className="mt-8">
                    <TransDLS keyTranslation="txt_manage_account_memos_right_5">
                      <strong className="color-grey-d20" />
                    </TransDLS>
                  </p>
                  <p className="mt-8">
                    <TransDLS keyTranslation="txt_manage_account_memos_right_6">
                      <strong className="color-grey-d20" />
                    </TransDLS>
                  </p>
                </div>
              </div>
            </div>
          </div>
        }
      >
        <div className="p-24">
          <h5>{t('txt_manage_account_memos_form_9')}</h5>
          <div className="group-card mt-16">
            <div className="group-card-item">
              <div className="row">
                <div className="col-lg-5">
                  <p>{t('txt_manage_account_memos_form_10')}</p>
                </div>
                <div className="col-lg-7 mt-8 mt-lg-0">
                  <div className="group-control justify-content-lg-end">
                    {memosTypes?.map((memoType: any) => (
                      <div
                        key={memoType?.value}
                        className="group-control-item custom-control-root"
                      >
                        <Radio>
                          <Radio.Input
                            id={`dls-radio-${memoType?.value}`}
                            className="checked-style"
                            checked={nameMemos === memoType?.value}
                            value={memoType?.value}
                            name="nameMemos"
                            onChange={e =>
                              handleChange('nameMemos', e.target.value)
                            }
                          />
                        </Radio>
                        <label
                          htmlFor={`dls-radio-${memoType?.value}`}
                          className="dls-radio-label"
                        >
                          {memoType?.description}
                        </label>
                      </div>
                    ))}
                  </div>
                </div>
              </div>
            </div>
            <div className="group-card-item">
              <div className="row">
                <div className="col-lg-5">
                  <p>{t('txt_manage_account_memos_form_11')}</p>
                </div>
                <div className="col-lg-7 mt-8 mt-lg-0">
                  <div className="group-control justify-content-lg-end">
                    {memosActions?.map((memoAction: any) => (
                      <div
                        key={memoAction?.value}
                        className="group-control-item custom-control-root"
                      >
                        <Radio>
                          <Radio.Input
                            id={`dls-radio-${memoAction?.value}`}
                            className="checked-style"
                            checked={actionsMemos === memoAction?.value}
                            value={memoAction?.value}
                            name="actionsMemos"
                            onChange={e =>
                              handleChange('actionsMemos', e.target.value)
                            }
                          />
                        </Radio>
                        <label
                          htmlFor={`dls-radio-${memoAction?.value}`}
                          className="dls-radio-label"
                        >
                          {memoAction?.description}
                        </label>
                      </div>
                    ))}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </ContentExpand>
      {showOverview && (
        <OverviewModal
          id="overviewWorkflowSetup"
          show
          onClose={() => {
            setShowOverview(false);
            keepRef.current.setFormValues({ alreadyShown: true });
          }}
        />
      )}
    </React.Fragment>
  );
};

const ExtraStaticGettingStartStep =
  GettingStartStep as WorkflowSetupStaticProp<FormGettingStart>;

ExtraStaticGettingStartStep.summaryComponent = GettingStartSummary;
ExtraStaticGettingStartStep.stepValue = stepValueFunc;
ExtraStaticGettingStartStep.defaultValues = {
  isValid: false,
  nameMemos: '',
  actionsMemos: ''
};
ExtraStaticGettingStartStep.parseFormValues = parseFormValues;

export default ExtraStaticGettingStartStep;
