import { fireEvent, render, RenderResult } from '@testing-library/react';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageAccountMemos';
import React from 'react';
import * as reactRedux from 'react-redux';
import GettingStartStep, { FormGettingStart } from './index';

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');

const useSelectElementMetadataManageAcountMemos = jest.spyOn(
  WorkflowSetup,
  'useSelectElementMetadataManageAcountMemos'
);

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});
jest.mock('app/hooks/useUnsavedChangeRegistry', () => {
  return {
    __esModule: true,
    useUnsavedChangeRegistry: () => {}
  };
});
jest.mock('app/hooks/useUnsavedChangeRegistry', () => {
  return {
    __esModule: true,
    useUnsavedChangeRegistry: () => {}
  };
});

jest.mock('pages/_commons/OverviewModal', () => ({
  __esModule: true,
  default: ({ onClose }: any) => {
    return (
      <div>
        <div>OverviewModal</div>
        <button onClick={onClose}>OverviewButtonCloseModal</button>
      </div>
    );
  }
}));

jest.mock('pages/_commons/ContentExpand', () => ({
  __esModule: true,
  default: ({ instruction, children }: any) => (
    <div>
      <div>ContentExpand</div>
      <div>{instruction}</div>
      <div>{children}</div>
    </div>
  )
}));

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <GettingStartStep {...props} />
    </div>
  );
};

describe('WorkflowManageAccountMemos > GettingStartStep > Index', () => {
  const mockDispatch = jest.fn();
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
    useSelectElementMetadataManageAcountMemos.mockImplementation(() => ({
      memosTypes: [
        {
          description: 'Customer Inquiry System (CIS) Memos',
          value: 'Customer Inquiry System (CIS) Memos'
        },
        {
          description: 'Chronicle Memos',
          value: 'Chronicle Memos'
        }
      ],
      memosActions: [
        {
          description: 'Add Memos',
          value: 'Add Memos'
        },
        {
          description: 'Find Memos',
          value: 'Find Memos'
        },
        {
          description: 'Delete Memos',
          value: 'Delete Memos'
        }
      ],
      memosTypesFormCIS: [],
      memosTypesFormChronicle: [],
      memosOperator: [],
      memosEnteredIdOperator: [],
      memosSysOperator: [],
      memosPrinOperator: [],
      memosAgentOperator: [],
      memosDateOperator: [],
      memosMemoTextOperator: [],
      memosTypeOperator: [],
      memosTypeOperatorValue: [],
      memosSys: [],
      memosPrin: [],
      memosAgent: []
    }));
  });

  it('Should render without Overview modal > Open overview modal', () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: {},
      setFormValues: mockFn
    } as WorkflowSetupProps<FormGettingStart>;

    const wrapper = renderComponent(props);
    expect(wrapper.queryByText(/OverviewModal/)).not.toBeInTheDocument;
    wrapper.rerender(
      <div>
        <GettingStartStep
          {...props}
          stepId="1"
          formValues={{
            alreadyShown: true,
            actionsMemos: 'Add Memos',
            nameMemos: 'Customer Inquiry System (CIS) Memos'
          }}
          isStuck
        />
      </div>
    );

    fireEvent.click(wrapper.getByText('txt_view_overview'));
    expect(wrapper.queryByText(/OverviewModal/)).toBeInTheDocument;
  });

  it('Should render Overview modal > Close overview modal', () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: {
        actionsMemos: 'Add Memos',
        nameMemos: 'Customer Inquiry System (CIS) Memos'
      },
      setFormValues: mockFn
    } as WorkflowSetupProps<FormGettingStart>;

    const wrapper = renderComponent(props);
    expect(wrapper.queryByText(/OverviewModal/)).toBeInTheDocument;

    fireEvent.click(wrapper.getByText('OverviewButtonCloseModal'));
    expect(wrapper.queryByText(/OverviewModal/)).not.toBeInTheDocument;
  });

  it('Select Change option', () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: {
        actionsMemos: 'Add Memos',
        nameMemos: 'Customer Inquiry System (CIS) Memos',
        isValid: true
      },
      setFormValues: mockFn
    } as WorkflowSetupProps<FormGettingStart>;

    const wrapper = renderComponent(props);
    const radio1 = wrapper.getByText('Customer Inquiry System (CIS) Memos');
    const radio2 = wrapper.getByText('Add Memos');
    const radio3 = wrapper.getByText('Chronicle Memos');
    const radio4 = wrapper.getByText('Find Memos');
    const radio5 = wrapper.getByText('Delete Memos');

    fireEvent.click(radio1);
    fireEvent.click(radio2);
    fireEvent.click(radio3);
    fireEvent.click(radio4);
    fireEvent.click(radio5);

    expect(wrapper.getByText('OverviewButtonCloseModal')).toBeInTheDocument();
  });
});
