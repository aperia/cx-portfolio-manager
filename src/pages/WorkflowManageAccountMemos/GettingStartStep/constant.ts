export const NAME_MEMOS = {
  CIS: 'Customer Inquiry System (CIS) Memos',
  CHRONICLE: 'Chronicle Memos'
};

export const ACTIONS_MEMOS = {
  ADD: 'Add Memos',
  FIND: 'Find Memos',
  DELETE: 'Delete Memos'
};
