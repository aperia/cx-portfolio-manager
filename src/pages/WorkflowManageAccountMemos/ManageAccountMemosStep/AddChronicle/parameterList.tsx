import {
  ManageAccountMemosNameEnum,
  ManageAccountMemosParameterEnum
} from 'app/constants/enums';
import React from 'react';

export interface AddMemosMAS {
  id: ManageAccountMemosParameterEnum;
  fieldName: ManageAccountMemosNameEnum;
  greenScreenName: string;
  moreInfoText: string;
  moreInfo: React.ReactNode;
}

export const addMemosChronicleMAS: AddMemosMAS[] = [
  {
    id: ManageAccountMemosParameterEnum.Type,
    fieldName: ManageAccountMemosNameEnum.Type,
    greenScreenName: 'Type',
    moreInfoText: 'Client-defined memo type',
    moreInfo: 'Client-defined memo type'
  },
  {
    id: ManageAccountMemosParameterEnum.Memos,
    fieldName: ManageAccountMemosNameEnum.MemosText,
    greenScreenName: 'Text',
    moreInfoText: 'Memo placed on the account’s memo file',
    moreInfo: 'Memo placed on the account’s memo file'
  }
];
