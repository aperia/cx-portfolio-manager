import userEvent from '@testing-library/user-event';
import { WorkflowMetadataOverride } from 'app/fixtures/workflow-metadata';
import { mockUseDispatchFnc, renderWithMockStore } from 'app/utils';
import 'app/utils/_mockComponent/mockModalRegistry';
import 'app/_libraries/_dls/test-utils/mocks/mockCanvas';
import * as CommonRedux from 'pages/_commons/redux/Common/select-hooks';
import React from 'react';
import ParameterGrid from './ParameterGrid';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

jest.mock('app/_libraries/_dls/components/DateRangePicker', () => {
  const actualModule = jest.requireActual(
    'app/_libraries/_dls/components/DateRangePicker'
  );
  return {
    ...actualModule,
    __esModule: true,
    default: ({ onChange }: any) => (
      <div onClick={onChange}>DateRangePicker</div>
    )
  };
});

jest.mock('./parameterList', () => ({
  __esModule: true,
  addMemosChronicleMAS: [
    {
      id: 'type',
      fieldName: 'Type',
      greenScreenName: 'Type',
      moreInfoText: 'Client-defined memo type',
      moreInfo: 'Client-defined memo type'
    },
    {
      id: 'memos',
      fieldName: 'Memo Text',
      greenScreenName: 'Text',
      moreInfoText: 'Memo placed on the account’s memo file',
      moreInfo: 'Memo placed on the account’s memo file'
    },
    {
      id: 'something else',
      fieldName: 'Memo Text',
      greenScreenName: 'Text',
      moreInfoText: 'Memo placed on the account’s memo file',
      moreInfo: 'Memo placed on the account’s memo file'
    }
  ]
}));

const mockUseDispatch = mockUseDispatchFnc();

const getInitialState = ({
  elements = WorkflowMetadataOverride
}: any = {}) => ({
  workflowSetup: { elementMetadata: { elements } }
});

const renderComponent = (props: any) => {
  return renderWithMockStore(<ParameterGrid {...props} />, {
    initialState: getInitialState()
  });
};

describe('WorkflowManageAccountMemos > SummaryGrid', () => {
  const mockDispatch = jest.fn();
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
  });

  it('Should render search bar', async () => {
    const wrapper = await renderComponent({
      formValues: {},
      setFormValues: jest.fn(),
      stepId: '2',
      selectedStep: ''
    });

    const searchBar = wrapper.getByPlaceholderText('txt_type_to_search');
    userEvent.type(searchBar, 't {enter}');
    const resetButton = wrapper.getByText('txt_clear_and_reset');
    userEvent.click(resetButton);
    expect(
      wrapper.getByText(
        'txt_manage_account_memos_add_memos_parameter_chronicle'
      )
    ).toBeInTheDocument;
  });

  it('Should render search bar', async () => {
    const spy = jest
      .spyOn(CommonRedux, 'useSelectWindowDimension')
      .mockImplementation(() => ({ width: 900, height: 900 }));
    const wrapper = await renderComponent({
      formValues: {},
      setFormValues: jest.fn(),
      stepId: '2',
      selectedStep: ''
    });

    const searchBar = wrapper.getByPlaceholderText('txt_type_to_search');
    userEvent.type(searchBar, '123 {enter}');
    const resetButton = wrapper.getByText('txt_clear_and_reset');
    userEvent.click(resetButton);
    expect(
      wrapper.getByText(
        'txt_manage_account_memos_add_memos_parameter_chronicle'
      )
    ).toBeInTheDocument;
    spy.mockReset();
    spy.mockRestore();
  });

  it('Should render SummaryGrid contains Completed', async () => {
    const wrapper = await renderComponent({
      formValues: {},
      setFormValues: jest.fn(),
      stepId: '2',
      selectedStep: ''
    });

    expect(
      wrapper.getByText(
        'txt_manage_account_memos_add_memos_parameter_chronicle'
      )
    ).toBeInTheDocument;
  });

  it('Should render text box', async () => {
    const wrapper = await renderComponent({
      formValues: { parameters: { type: '1' } },
      setFormValues: jest.fn(),
      stepId: 'stepId',
      selectedStep: 'stepId'
    });

    const searchBar = wrapper.getByPlaceholderText('txt_type_to_search');
    userEvent.type(searchBar, '123 {enter}');
    const resetButton = wrapper.getByText('txt_clear_and_reset');
    userEvent.click(resetButton);

    const textBox = wrapper.getAllByPlaceholderText('txt_enter_a_value');

    userEvent.type(textBox[1], '{enter}');

    userEvent.type(textBox[1], '1');
    userEvent.type(textBox[0], '12');

    expect(
      wrapper.getByText(
        'txt_manage_account_memos_add_memos_parameter_chronicle'
      )
    ).toBeInTheDocument;
  });
});
