import {
  ManageAccountMemosNameEnum,
  ManageAccountMemosParameterEnum
} from 'app/constants/enums';
import { addMemosChronicleMAS } from './parameterList';

describe('pages > WorkflowManageAccountMemos > ManageAccountMemosStep > AddChronicle ', () => {
  it('> recordParameterMAS', () => {
    expect(addMemosChronicleMAS).toEqual([
      {
        id: ManageAccountMemosParameterEnum.Type,
        fieldName: ManageAccountMemosNameEnum.Type,
        greenScreenName: 'Type',
        moreInfoText: 'Client-defined memo type',
        moreInfo: 'Client-defined memo type'
      },
      {
        id: ManageAccountMemosParameterEnum.Memos,
        fieldName: ManageAccountMemosNameEnum.MemosText,
        greenScreenName: 'Text',
        moreInfoText: 'Memo placed on the account’s memo file',
        moreInfo: 'Memo placed on the account’s memo file'
      }
    ]);
  });
});
