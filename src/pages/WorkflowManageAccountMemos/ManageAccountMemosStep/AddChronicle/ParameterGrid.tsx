import FormatTextBox from 'app/components/FormatTextBox';
import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import TextAreaCountDown from 'app/components/TextAreaCountDown';
import { ManageAccountMemosParameterEnum } from 'app/constants/enums';
import { matchSearchValue } from 'app/helpers';
import { Grid, useTranslation } from 'app/_libraries/_dls';
import isEmpty from 'lodash.isempty';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import ClearAndResetButton from 'pages/_commons/Utils/ClearAndResetButton';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React, { useEffect, useMemo, useRef, useState } from 'react';
import { ConfigureParametersFormValue } from '..';
import { addMemosChronicleMAS } from './parameterList';

const ParameterGridChronicle: React.FC<
  WorkflowSetupProps<ConfigureParametersFormValue>
> = ({ setFormValues, formValues, selectedStep, stepId }) => {
  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();

  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;

  const simpleSearchRef = useRef<any>(null);
  const [searchValue, setSearchValue] = useState('');

  const handleSearch = (val: string) => setSearchValue(val);

  const handleClearFilter = () => setSearchValue('');

  useEffect(() => {
    if (!searchValue && simpleSearchRef?.current) {
      simpleSearchRef.current.clear();
    }
  }, [searchValue]);

  useEffect(() => {
    if (stepId !== selectedStep) {
      setSearchValue('');
    }
  }, [stepId, selectedStep]);

  useEffect(() => {
    if (stepId === selectedStep) {
      keepRef.current.setFormValues({ isValid: true });
    }
  }, [stepId, selectedStep]);

  const parameters = useMemo(
    () =>
      addMemosChronicleMAS.filter(g =>
        matchSearchValue(
          [g.fieldName, g.moreInfoText, g.greenScreenName],
          searchValue
        )
      ),
    [searchValue]
  );

  const handleChangeMemos = (e: any) => {
    keepRef.current.setFormValues({
      ...formValues,
      parameters: { ...formValues?.parameters, memoText: e?.target?.value }
    });
  };

  const handleChangeType = (e: any) => {
    keepRef.current.setFormValues({
      ...formValues,
      parameters: { ...formValues?.parameters, type: e?.target?.value }
    });
  };

  const valueAccessor = (data: MagicKeyValue) => {
    const paramId = data.id as ManageAccountMemosParameterEnum;
    if (paramId === ManageAccountMemosParameterEnum.Memos) {
      return (
        <TextAreaCountDown
          placeholder={t('txt_enter_a_value')}
          onKeyPress={e => {
            e.key === 'Enter' && e.preventDefault();
          }}
          dataTestId="manageAccountMemos__memos"
          rows={4}
          value={formValues?.parameters?.memoText}
          onChange={e => handleChangeMemos(e)}
          maxLength={2000}
        />
      );
    }

    if (paramId === ManageAccountMemosParameterEnum.Type) {
      return (
        <FormatTextBox
          size="sm"
          id="cis_type"
          placeholder={t('txt_enter_a_value')}
          value={formValues?.parameters?.type}
          autoFocus={false}
          maxLength={6}
          onChange={e => handleChangeType(e)}
          pattern="^[^ ]{0,6}$"
        />
      );
    }
  };

  const columns = [
    {
      id: 'fieldName',
      Header: t('txt_business_name'),
      accessor: 'fieldName',
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'greenScreenName',
      Header: t('txt_green_screen_name'),
      accessor: 'greenScreenName',
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'value',
      Header: t('txt_value'),
      accessor: valueAccessor,
      cellBodyProps: { className: 'py-8' },
      width: width < 1280 ? 220 : undefined
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      accessor: viewMoreInfo(['moreInfo'], t),
      width: 105,
      cellBodyProps: { className: 'pt-12 pb-8' }
    }
  ];

  return (
    <div>
      <div className="d-flex align-items-center justify-content-between pt-24 pb-16">
        <h5>{t('txt_manage_account_memos_add_memos_parameter_chronicle')}</h5>
        <SimpleSearch
          ref={simpleSearchRef}
          placeholder={t('txt_type_to_search')}
          onSearch={handleSearch}
          popperElement={
            <>
              <p className="color-grey">{t('txt_search_description')}</p>
              <ul className="search-field-item list-unstyled">
                <li className="mt-16">{t('txt_business_name')}</li>
                <li className="mt-16">{t('txt_green_screen_name')}</li>
                <li className="mt-16">{t('txt_more_info')}</li>
              </ul>
            </>
          }
        />
      </div>
      {searchValue && !isEmpty(parameters) && (
        <div className="d-flex justify-content-end mt-16 mr-n8">
          <ClearAndResetButton small onClearAndReset={handleClearFilter} />
        </div>
      )}
      {!isEmpty(parameters) && <Grid columns={columns} data={parameters} />}
      {isEmpty(parameters) && (
        <div className="d-flex flex-column justify-content-center mt-40 mb-32">
          <NoDataFound
            id="newRecord_notfound"
            hasSearch
            title={t('txt_no_results_found')}
            linkTitle={t('txt_clear_and_reset')}
            onLinkClicked={handleClearFilter}
          />
        </div>
      )}
    </div>
  );
};

export default ParameterGridChronicle;
