import {
  ManageAccountMemosNameEnum,
  ManageAccountMemosParameterEnum
} from 'app/constants/enums';
import React from 'react';

export interface AddMemosMAS {
  id: ManageAccountMemosParameterEnum;
  fieldName: ManageAccountMemosNameEnum;
  greenScreenName: string;
  moreInfoText: string;
  moreInfo: React.ReactNode;
}

export const addMemosMAS: AddMemosMAS[] = [
  {
    id: ManageAccountMemosParameterEnum.Memos,
    fieldName: ManageAccountMemosNameEnum.Memos,
    greenScreenName: 'Memo',
    moreInfoText: 'Memo placed on the account’s memo file',
    moreInfo: 'Memo placed on the account’s memo file'
  }
];
