import {
  ManageAccountMemosNameEnum,
  ManageAccountMemosParameterEnum
} from 'app/constants/enums';
import { addMemosMAS } from './parameterList';

describe('pages > WorkflowManageAccountMemos > ManageAccountMemosStep > AddChronicle ', () => {
  it('> recordParameterMAS', () => {
    expect(addMemosMAS).toEqual([
      {
        id: ManageAccountMemosParameterEnum.Memos,
        fieldName: ManageAccountMemosNameEnum.Memos,
        greenScreenName: 'Memo',
        moreInfoText: 'Memo placed on the account’s memo file',
        moreInfo: 'Memo placed on the account’s memo file'
      }
    ]);
  });
});
