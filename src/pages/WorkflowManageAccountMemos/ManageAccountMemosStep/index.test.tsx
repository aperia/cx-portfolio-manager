import { RenderResult } from '@testing-library/react';
import { renderWithMockStore } from 'app/utils';
import React from 'react';
import ConfigureParametersStep from '.';
import { ACTIONS_MEMOS, NAME_MEMOS } from '../GettingStartStep/constant';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

jest.mock('./ParameterGrid', () => {
  return {
    __esModule: true,
    default: () => <div>ConfigureParametersForm</div>
  };
});

jest.mock('./AddChronicle/ParameterGrid', () => {
  return {
    __esModule: true,
    default: () => <div>ConfigureParametersForm</div>
  };
});

const renderComponent = (props: any): RenderResult => {
  return renderWithMockStore(
    <div>
      <ConfigureParametersStep {...props} />
    </div>,
    {}
  );
};

describe('ManageAccountMemosStep > ConfigureParametersStep', () => {
  it('Should render ConfigureParametersStep contains Completed', async () => {
    const wrapper = await renderComponent({
      formValues: {
        checked: 'r1',
        queryString: [
          {
            id: '0001',
            operator: 'Where',
            type: 'Condition',
            item: {
              filterBy: '',
              operator: '',
              value: ''
            }
          }
        ]
      },
      dependencies: {
        nameMemos: NAME_MEMOS.CHRONICLE,
        actionsMemos: ACTIONS_MEMOS.ADD
      },
      setFormValues: jest.fn()
    });

    expect(wrapper.getByText('ConfigureParametersForm')).toBeInTheDocument;
  });

  it('Should render ConfigureParametersStep contains with queryString empty', async () => {
    const wrapper = await renderComponent({
      formValues: {
        checked: 'r1'
      },
      dependencies: {
        nameMemos: 'chronicle',
        actionsMemos: ACTIONS_MEMOS.FIND
      },
      setFormValues: jest.fn()
    });

    expect(
      wrapper.getByText(
        'txt_manage_account_memos_add_memos_parameter_chronicle_find'
      )
    ).toBeInTheDocument;
  });

  it('Should render ConfigureParametersStep contains Completed', async () => {
    const wrapper = await renderComponent({
      formValues: {
        checked: 'r1',
        queryString: [
          {
            id: '0001',
            operator: 'Where',
            type: 'Condition',
            item: {
              filterBy: '',
              operator: '',
              value: ''
            }
          }
        ]
      },
      dependencies: {
        nameMemos: NAME_MEMOS.CIS,
        actionsMemos: ACTIONS_MEMOS.ADD
      },
      setFormValues: jest.fn()
    });

    expect(wrapper.getByText('ConfigureParametersForm')).toBeInTheDocument;
  });

  it('Should render ConfigureParametersStep contains with queryString empty', async () => {
    const wrapper = await renderComponent({
      formValues: {
        checked: 'r1'
      },
      dependencies: {
        nameMemos: NAME_MEMOS.CIS,
        actionsMemos: ACTIONS_MEMOS.FIND
      },
      setFormValues: jest.fn()
    });

    expect(
      wrapper.getByText('txt_manage_account_memos_add_memos_parameter_find')
    ).toBeInTheDocument;
  });
});
