import { WorkflowMetadataOverride } from 'app/fixtures/workflow-metadata';
import { mockUseDispatchFnc, renderWithMockStore } from 'app/utils';
import 'app/utils/_mockComponent/mockModalRegistry';
import 'app/_libraries/_dls/test-utils/mocks/mockCanvas';
import React from 'react';
import { TYPE_MEMOS_CONDITION } from './FindChronicle/constants';
import FindSummary from './FindSummary';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const mockUseDispatch = mockUseDispatchFnc();

const queryString = [
  {
    value: '345',
    type: TYPE_MEMOS_CONDITION.CONDITION_GROUP,
    item: {
      operator: 'Not Between',
      filterBy: 'Date',
      value: '123'
    },
    conditions: [
      {
        type: TYPE_MEMOS_CONDITION.CONDITION_GROUP,
        item: {
          operator: 'op',
          filterBy: 'Date',
          value: '123'
        },
        conditions: [
          {
            type: TYPE_MEMOS_CONDITION.CONDITION_GROUP,
            item: {
              operator: 'op',
              filterBy: 'Date',
              value: '123'
            }
          },
          {
            type: TYPE_MEMOS_CONDITION.CONDITION,
            item: {
              operator: 'Not Between',
              filterBy: 'Test',
              value: '123'
            }
          }
        ]
      },
      {
        type: TYPE_MEMOS_CONDITION.CONDITION,
        operator: 'op',
        item: {
          filterBy: 'Test',
          value: '123',
          operator: 'Not Between'
        },
        conditions: [
          {
            type: TYPE_MEMOS_CONDITION.CONDITION_GROUP,
            operator: 'op',
            item: {
              filterBy: 'Date',
              value: '123',
              operator: 'Not Between'
            }
          },
          {
            type: TYPE_MEMOS_CONDITION.CONDITION_GROUP,
            operator: 'Not Between',
            item: {
              filterBy: 'Date',
              value: '123',
              operator: 'Not Between'
            }
          }
        ]
      }
    ]
  },
  {
    type: TYPE_MEMOS_CONDITION.CONDITION_GROUP,
    item: {
      operator: 'Not Between',
      filterBy: 'Date',
      value: '123'
    },
    conditions: [
      {
        type: TYPE_MEMOS_CONDITION.CONDITION,
        item: {
          operator: 'op',
          filterBy: 'Date',
          value: '123'
        },
        conditions: [
          {
            type: TYPE_MEMOS_CONDITION.CONDITION_GROUP,
            item: {
              operator: 'Between',
              filterBy: 'Date',
              value: '123'
            }
          },
          {
            type: TYPE_MEMOS_CONDITION.CONDITION,
            item: {
              operator: 'Not Between',
              filterBy: 'Test',
              value: '123'
            }
          }
        ]
      },
      {
        type: TYPE_MEMOS_CONDITION.CONDITION_GROUP,
        item: {
          operator: 'Between',
          filterBy: 'Test',
          value: '123'
        },
        conditions: [
          {
            type: TYPE_MEMOS_CONDITION.CONDITION_GROUP,
            item: {
              operator: 'Between',
              filterBy: 'Date',
              value: '123'
            }
          },
          {
            type: TYPE_MEMOS_CONDITION.CONDITION,
            item: {
              operator: 'Not Between',
              filterBy: 'Test',
              value: '123'
            }
          }
        ]
      }
    ]
  },
  {
    type: 'Error',
    item: {
      operator: 'Not Between',
      filterBy: 'Date',
      value: '123'
    }
  },
  {
    type: 'Error',
    item: {
      operator: 'Not Between',
      filterBy: 'Test',
      value: '123'
    }
  }
];

const getInitialState = ({
  elements = WorkflowMetadataOverride
}: any = {}) => ({
  workflowSetup: { elementMetadata: { elements } }
});

const renderComponent = (props: any) => {
  return renderWithMockStore(<FindSummary {...props} />, {
    initialState: getInitialState()
  });
};

describe('WorkflowManageAccountMemos > SummaryGrid', () => {
  const mockDispatch = jest.fn();
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
  });

  it('Should render FindSummary contains Completed', async () => {
    const wrapper = await renderComponent({
      formValues: {
        queryString: queryString
      }
    });

    expect(wrapper.getAllByText('txt_manage_account_memos_value'))
      .toBeInTheDocument;
  });
});
