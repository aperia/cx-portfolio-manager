import isDefaultValues from './isDefaultValues';

describe('ManageAccountMemosStep > ConfigureParametersStep > isDefaultValues', () => {
  it('function isDefaultValues', () => {
    const formValues = { isValid: true };
    const defaultValue = { isValid: true };

    const result = isDefaultValues(formValues, defaultValue);

    expect(result).toEqual(true);
  });
  it('function isDefaultValues return false', () => {
    const formValues = { isValid: true, queryString: [] };
    const defaultValue = { isValid: true };

    const result = isDefaultValues(formValues, defaultValue);

    expect(result).toEqual(false);
  });
});
