import TextAreaCountDown from 'app/components/TextAreaCountDown';
import { ManageAccountMemosParameterEnum } from 'app/constants/enums';
import { Grid, useTranslation } from 'app/_libraries/_dls';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React, { useEffect, useRef } from 'react';
import { ConfigureParametersFormValue } from '.';
import { addMemosMAS } from './parameterList';

const ParameterGrid: React.FC<
  WorkflowSetupProps<ConfigureParametersFormValue>
> = ({ setFormValues, formValues, selectedStep, stepId }) => {
  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();

  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;

  useEffect(() => {
    if (stepId === selectedStep) {
      keepRef.current.setFormValues({ isValid: true });
    }
  }, [stepId, selectedStep]);

  const handleChange = (e: any) => {
    keepRef.current.setFormValues({
      ...formValues,
      parameters: { memos: e?.target?.value }
    });
  };

  const valueAccessor = (data: MagicKeyValue) => {
    const paramId = data.id as ManageAccountMemosParameterEnum;
    if (paramId === ManageAccountMemosParameterEnum.Memos) {
      return (
        <TextAreaCountDown
          placeholder={t('txt_enter_a_value')}
          dataTestId="manageAccountMemos__memos"
          onKeyPress={e => {
            e.key === 'Enter' && e.preventDefault();
          }}
          rows={4}
          value={formValues?.parameters?.memos}
          onChange={e => handleChange(e)}
          maxLength={1260}
        />
      );
    }
  };

  const columns = [
    {
      id: 'fieldName',
      Header: t('txt_business_name'),
      accessor: 'fieldName',
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'greenScreenName',
      Header: t('txt_green_screen_name'),
      accessor: 'greenScreenName',
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'value',
      Header: t('txt_value'),
      accessor: valueAccessor,
      cellBodyProps: { className: 'py-8' },
      width: width < 1280 ? 220 : undefined
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      accessor: viewMoreInfo(['moreInfo'], t),
      width: 105,
      cellBodyProps: { className: 'pt-12 pb-8' }
    }
  ];

  return (
    <div>
      <div className="d-flex align-items-center justify-content-between pt-24 pb-16">
        <h5>{t('txt_manage_account_memos_add_memos_parameter')}</h5>
      </div>
      <Grid columns={columns} data={addMemosMAS} />
    </div>
  );
};
export default ParameterGrid;
