import { ManageAccountMemosParameterEnum } from 'app/constants/enums';
import { Grid, TruncateText, useTranslation } from 'app/_libraries/_dls';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React from 'react';
import { ConfigureParametersFormValue, IDependencies } from '.';
import { NAME_MEMOS } from '../GettingStartStep/constant';
import { addMemosChronicleMAS } from './AddChronicle/parameterList';
import { addMemosMAS } from './parameterList';

const SummaryGrid: React.FC<
  WorkflowSetupSummaryProps<ConfigureParametersFormValue, IDependencies>
> = ({ formValues, dependencies }) => {
  const { t } = useTranslation();

  const valueAccessor = (row: MagicKeyValue) => {
    if (row?.id === ManageAccountMemosParameterEnum.Memos) {
      const val =
        dependencies?.nameMemos === NAME_MEMOS?.CHRONICLE
          ? formValues?.parameters?.memoText
          : formValues?.parameters?.memos;
      return (
        <TruncateText
          resizable
          lines={2}
          ellipsisLessText={t('txt_less')}
          ellipsisMoreText={t('txt_more')}
        >
          {val}
        </TruncateText>
      );
    }
    if (row?.id === ManageAccountMemosParameterEnum.Type) {
      return (
        <TruncateText
          resizable
          lines={2}
          ellipsisLessText={t('txt_less')}
          ellipsisMoreText={t('txt_more')}
        >
          {formValues?.parameters?.type}
        </TruncateText>
      );
    }
  };

  const columns = [
    {
      id: 'fieldName',
      Header: t('txt_business_name'),
      accessor: 'fieldName'
    },
    {
      id: 'greenScreenName',
      Header: t('txt_green_screen_name'),
      accessor: 'greenScreenName'
    },
    {
      id: 'value',
      Header: t('txt_value'),
      accessor: valueAccessor
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      accessor: viewMoreInfo(['moreInfo'], t),
      width: 105
    }
  ];

  const dataGrid =
    dependencies?.nameMemos === NAME_MEMOS?.CHRONICLE
      ? addMemosChronicleMAS
      : addMemosMAS;

  return (
    <div>
      <Grid columns={columns} data={dataGrid} />
    </div>
  );
};

export default SummaryGrid;
