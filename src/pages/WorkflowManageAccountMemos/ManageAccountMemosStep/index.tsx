import {
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { useUnsavedChangeRegistry } from 'app/hooks';
import { InlineMessage, useTranslation } from 'app/_libraries/_dls';
import { isEmpty } from 'lodash';
import isEqual from 'lodash.isequal';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, { useEffect, useMemo, useRef, useState } from 'react';
import { ACTIONS_MEMOS, NAME_MEMOS } from '../GettingStartStep/constant';
import ParameterGridChronicle from './AddChronicle/ParameterGrid';
import FindChronicle from './FindChronicle';
import FindCIS from './FindCIS';
import isDefaultValues from './isDefaultValues';
import ParameterGrid from './ParameterGrid';
import parseFormValues from './parseFormValues';
import ConfigureParametersSummary from './Summary';

export interface ConfigureParametersFormValue {
  isValid?: boolean;
  parameters?: {
    memos?: string;
    type?: string;
    memoText?: string;
  };
  queryString?: MagicKeyValue[];
  isEdit?: boolean;
}

export interface IDependencies {
  nameMemos?: string;
  actionsMemos?: string;
}

const ConfigureParameters: React.FC<
  WorkflowSetupProps<ConfigureParametersFormValue, IDependencies>
> = props => {
  const { t } = useTranslation();
  const { setFormValues, formValues, dependencies, isStuck, savedAt } = props;
  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;

  const [preValues, setPreValues] = useState(formValues);

  useEffect(() => {
    setPreValues(formValues);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [savedAt]);

  const isHasChange =
    !isEqual(preValues.parameters, formValues.parameters) ||
    !isEqual(preValues.queryString, formValues.queryString);

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_ACCOUNT_MEMOS_CONFIRM_EDIT,
      priority: 1
    },
    [isHasChange]
  );

  useEffect(() => {
    if (dependencies?.actionsMemos === ACTIONS_MEMOS.ADD) {
      keepRef.current.setFormValues({
        isValid: true
      });
    }
    if (
      dependencies?.actionsMemos === ACTIONS_MEMOS.FIND &&
      isEmpty(formValues?.queryString)
    ) {
      keepRef.current.setFormValues({
        isValid: false
      });
    }
  }, [dependencies?.actionsMemos, formValues?.queryString]);

  const stepRendered = useMemo(() => {
    if (
      dependencies?.nameMemos === NAME_MEMOS.CHRONICLE &&
      dependencies?.actionsMemos === ACTIONS_MEMOS.ADD
    ) {
      return <ParameterGridChronicle {...props} />;
    }
    if (
      dependencies?.nameMemos === NAME_MEMOS.CIS &&
      dependencies?.actionsMemos === ACTIONS_MEMOS.ADD
    ) {
      return <ParameterGrid {...props} />;
    }
    if (
      dependencies?.nameMemos === NAME_MEMOS.CIS &&
      dependencies?.actionsMemos === ACTIONS_MEMOS.FIND
    ) {
      return <FindCIS {...props} />;
    }
    return <FindChronicle {...props} />;
  }, [props, dependencies]);

  return (
    <>
      {isStuck && (
        <InlineMessage className="mb-0 mt-24" variant="danger" withIcon>
          {t('txt_step_stuck_move_forward_message')}
        </InlineMessage>
      )}
      {stepRendered}
    </>
  );
};

const ExtraStaticConfigureParametersStep =
  ConfigureParameters as WorkflowSetupStaticProp<ConfigureParametersFormValue>;

ExtraStaticConfigureParametersStep.summaryComponent =
  ConfigureParametersSummary;
ExtraStaticConfigureParametersStep.defaultValues = {
  isValid: false,
  isEdit: false,
  parameters: {
    memos: '',
    type: '',
    memoText: ''
  },
  queryString: [
    {
      id: '0001',
      operator: 'Where',
      type: 'Condition',
      item: {
        filterBy: '',
        operator: '',
        value: ''
      }
    }
  ]
};

ExtraStaticConfigureParametersStep.parseFormValues = parseFormValues;
ExtraStaticConfigureParametersStep.isDefaultValues = isDefaultValues;

export default ExtraStaticConfigureParametersStep;
