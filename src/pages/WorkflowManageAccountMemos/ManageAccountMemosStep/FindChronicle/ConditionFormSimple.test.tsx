import { fireEvent, queryByText, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { mockUseDispatchFnc, renderWithMockStore } from 'app/utils';
import * as mockUseSelect from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageAccountMemos';
import React from 'react';
import { ConfigureParametersFormValue } from '..';
import { ConditionFormProps } from './ConditionForm';
import ConditionFormSimple from './ConditionFormSimple';

const renderComponent = (props: any): Promise<RenderResult> => {
  return renderWithMockStore(<ConditionFormSimple {...props} />, {});
};
const conditionLv3 = {
  id: 'id3',
  item: { filterBy: 'Sys', operator: 'And', value: '0000' },
  operator: 'And',
  type: 'Condition',
  level: '1'
};

const conditionLv4 = {
  id: 'id4',
  item: { filterBy: 'Sys', operator: 'And', value: '0000' },
  operator: 'And',
  type: 'Condition',
  level: '1'
};

const conditionLv22 = {
  id: 'id2',
  conditions: [conditionLv3, conditionLv4],
  item: { filterBy: 'Agent', operator: 'And', value: '0001' },
  level: '2'
};

const conditionLv12 = {
  id: 'id1',
  item: { filterBy: 'Sys', operator: 'And', value: '0000' },
  operator: 'And',
  conditions: [conditionLv22],
  level: '1'
};

const conditionLv2 = {
  id: 'id2',
  conditions: [{}],
  item: { filterBy: 'Agent', operator: 'And', value: '0001' },
  level: '2'
};

const conditionLv1 = {
  id: 'id1',
  item: { filterBy: 'Sys', operator: 'And', value: '0000' },
  operator: 'And',
  conditions: [conditionLv2],
  level: '1'
};

const defaultElementData = {
  memosEnteredIdOperator: [
    {
      value: 'equal',
      description: 'Equal'
    },
    {
      value: 'notEqual',
      description: 'Not Equal'
    }
  ],
  memosSysOperator: [
    {
      value: 'equal',
      description: 'Equal',
      workFlow: 'mam'
    },
    {
      value: 'notEqual',
      description: 'Not Equal',
      workFlow: 'mam'
    },
    {
      value: 'equal',
      description: 'Equal',
      workFlow: 'cpd'
    }
  ],
  memosPrinOperator: [
    {
      value: 'equal',
      description: 'Equal',
      workFlow: 'mam'
    },
    {
      value: 'notEqual',
      description: 'Not Equal',
      workFlow: 'mam'
    },
    {
      value: 'equal',
      description: 'Equal',
      workFlow: 'cpd'
    }
  ],
  memosAgentOperator: [
    {
      value: 'equal',
      description: 'Equal',
      workFlow: 'mam'
    },
    {
      value: 'notEqual',
      description: 'Not Equal',
      workFlow: 'mam'
    },
    {
      value: 'equal',
      description: 'Equal',
      workFlow: 'cpd'
    }
  ],
  memosDateOperator: [
    {
      value: 'between',
      description: 'Between'
    },
    {
      value: 'before',
      description: 'Before'
    },
    {
      value: 'after',
      description: 'After'
    },
    {
      value: 'notBetween',
      description: 'Not Between'
    }
  ],
  memosMemoTextOperator: [
    {
      value: 'equal',
      description: 'Equal'
    },
    {
      value: 'contains',
      description: 'Contains'
    },
    {
      value: 'notContain',
      description: 'Not Contains'
    }
  ],
  memosTypesFormChronicle: [
    {
      value: 'Entered ID',
      description: 'Entered ID'
    },
    {
      value: 'Sys',
      description: 'Sys'
    },
    {
      value: 'Prin',
      description: 'Prin'
    },
    {
      value: 'Agent',
      description: 'Agent'
    },
    {
      value: 'Date',
      description: 'Date'
    },
    {
      value: 'Memo Text',
      description: 'Memo Text'
    },
    {
      value: 'Type',
      description: 'Type'
    }
  ],
  memosSys: [
    {
      value: '0000',
      description: '0000'
    },
    {
      value: '0001',
      description: '0001'
    }
  ],
  memosPrin: [
    {
      value: '0000',
      description: '0000'
    },
    {
      value: '3312',
      description: '3312'
    }
  ],
  memosAgent: [
    {
      value: '0001',
      description: '0001'
    },
    {
      value: '7658',
      description: '7658'
    }
  ],
  memosTypeOperatorValue: [
    {
      value: 'Type01',
      description: 'Type01',
      workFlow: 'mam'
    },
    {
      value: 'Type02',
      description: 'Type02',
      workFlow: 'mam'
    },
    {
      value: 'TypeAA',
      description: 'TypeAA',
      workFlow: 'mam'
    }
  ],
  memosTypeOperator: [
    {
      value: 'equal',
      description: 'Equal',
      workFlow: 'mam'
    },
    {
      value: 'notEqual',
      description: 'Not Equal',
      workFlow: 'mam'
    }
  ]
};

const mockData = jest.spyOn(
  mockUseSelect,
  'useSelectElementMetadataManageAcountMemos'
);

const selectedValueDropdown = (text: any, wrapper: Element, value) => {
  const dropdownListElement =
    wrapper.getByText(text).parentElement?.parentElement?.parentElement;
  const iconElement = dropdownListElement?.querySelector('.icon');
  userEvent.click(iconElement!);

  const popupElement = wrapper.baseElement.querySelector('.dls-popup');
  expect(popupElement).toBeInTheDocument();
  const item = queryByText(popupElement as HTMLElement, value);
  jest.useFakeTimers();
  userEvent.click(item!);
  jest.runAllTimers();
};

const mockUseDispatch = mockUseDispatchFnc();

describe('ManageAccountMemosStep > ManageAccountMemosStep > FindChronicle > ConditionFormSimple', () => {
  const mockDispatch = jest.fn();
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
    mockData.mockReturnValue({ ...defaultElementData } as any);
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
    jest.clearAllMocks();
  });

  describe('> with parent', () => {
    const getProps = ({
      dataCondition,
      queryString = [],
      ...props
    }: Partial<ConditionFormProps & ConfigureParametersFormValue> = {}) => ({
      formValues: { queryString },
      dataCondition,
      isChildrenGroup: true,
      idParent: 'id2',
      ...props
    });

    it('should render filter by Sys', async () => {
      const setFormValues = jest.fn();
      const handleShow = jest.fn();
      const dataCondition = { ...conditionLv3, operator: 'And' };
      const wrapper = await renderComponent(
        getProps({
          dataCondition: dataCondition,
          queryString: [conditionLv12],
          setFormValues,
          handleShow
        })
      );

      const { getByText, container } = wrapper;

      expect(
        getByText('txt_manage_account_memos_filter_by')
      ).toBeInTheDocument();

      selectedValueDropdown(
        'txt_manage_account_memos_filter_by',
        wrapper,
        'Sys'
      );

      selectedValueDropdown(
        'txt_manage_account_memos_operator',
        wrapper,
        'Not Equal'
      );

      selectedValueDropdown('txt_manage_account_memos_value', wrapper, '0001');

      expect(setFormValues).toBeCalled();

      userEvent.click(container.querySelector('.icon.icon-close')!);
    });
  });

  describe('> with parent', () => {
    const getProps = ({
      dataCondition,
      queryString = [],
      ...props
    }: Partial<ConditionFormProps & ConfigureParametersFormValue> = {}) => ({
      formValues: { queryString },
      dataCondition,
      isChildrenGroup: true,
      idParent: 'id1',
      ...props
    });

    it('should render filter by Sys', async () => {
      const setFormValues = jest.fn();
      const handleShow = jest.fn();
      const dataCondition = { ...conditionLv1, operator: 'And' };
      const wrapper = await renderComponent(
        getProps({
          dataCondition: dataCondition,
          queryString: [dataCondition],
          setFormValues,
          handleShow
        })
      );

      const { getByText, container } = wrapper;

      expect(
        getByText('txt_manage_account_memos_filter_by')
      ).toBeInTheDocument();

      selectedValueDropdown(
        'txt_manage_account_memos_filter_by',
        wrapper,
        'Sys'
      );

      selectedValueDropdown(
        'txt_manage_account_memos_operator',
        wrapper,
        'Not Equal'
      );

      selectedValueDropdown('txt_manage_account_memos_value', wrapper, '0001');

      userEvent.click(wrapper.getAllByText('And')[0]);

      userEvent.click(getByText('Or'));

      expect(setFormValues).toBeCalled();

      userEvent.click(container.querySelector('.icon.icon-close')!);
      expect(handleShow).toBeCalled();
    });

    it('should render filter by Prin', async () => {
      const setFormValues = jest.fn();
      const handleShow = jest.fn();
      const dataCondition = {
        ...conditionLv1,
        item: { filterBy: 'Prin', operator: 'And', value: '0000' }
      };
      const wrapper = await renderComponent(
        getProps({
          idParent: 'id2',
          dataCondition: dataCondition,
          queryString: [dataCondition, { ...conditionLv2, id: 'id3' }],
          setFormValues,
          handleShow
        })
      );

      const { getByText, container } = wrapper;

      expect(
        getByText('txt_manage_account_memos_filter_by')
      ).toBeInTheDocument();

      selectedValueDropdown(
        'txt_manage_account_memos_filter_by',
        wrapper,
        'Prin'
      );

      selectedValueDropdown(
        'txt_manage_account_memos_operator',
        wrapper,
        'Not Equal'
      );

      selectedValueDropdown('txt_manage_account_memos_value', wrapper, '3312');

      expect(setFormValues).toBeCalled();

      userEvent.click(container.querySelector('.icon.icon-close')!);
      expect(handleShow).toBeCalled();
    });

    it('should render filter by Prin level 1 > 1', async () => {
      const setFormValues = jest.fn();
      const handleShow = jest.fn();
      const dataCondition = {
        ...conditionLv1,
        conditions: [conditionLv2, conditionLv2],
        item: { filterBy: 'Prin', operator: 'And', value: '0000' }
      };
      const wrapper = await renderComponent(
        getProps({
          idParent: 'id2',
          dataCondition: dataCondition,
          queryString: [dataCondition, { ...conditionLv2, id: 'id3' }],
          setFormValues,
          handleShow
        })
      );
      const { getByText, container } = wrapper;

      expect(
        getByText('txt_manage_account_memos_filter_by')
      ).toBeInTheDocument();

      selectedValueDropdown(
        'txt_manage_account_memos_filter_by',
        wrapper,
        'Prin'
      );

      selectedValueDropdown(
        'txt_manage_account_memos_operator',
        wrapper,
        'Not Equal'
      );

      selectedValueDropdown('txt_manage_account_memos_value', wrapper, '3312');

      expect(setFormValues).toBeCalled();

      userEvent.click(container.querySelector('.icon.icon-close')!);
      expect(handleShow).not.toBeCalled();
    });

    it('should render filter by Prin level 2 > 1', async () => {
      const setFormValues = jest.fn();
      const handleShow = jest.fn();
      const dataCondition = {
        ...conditionLv1,
        conditions: [
          { ...conditionLv2, conditions: [conditionLv2, conditionLv2] }
        ],
        item: { filterBy: 'Prin', operator: 'And', value: '0000' }
      };
      const wrapper = await renderComponent(
        getProps({
          idParent: 'id2',
          dataCondition: dataCondition,
          queryString: [dataCondition, { ...conditionLv2, id: 'id3' }],
          setFormValues,
          handleShow
        })
      );

      const { getByText, container } = wrapper;

      expect(
        getByText('txt_manage_account_memos_filter_by')
      ).toBeInTheDocument();

      selectedValueDropdown(
        'txt_manage_account_memos_filter_by',
        wrapper,
        'Prin'
      );

      selectedValueDropdown(
        'txt_manage_account_memos_operator',
        wrapper,
        'Not Equal'
      );

      selectedValueDropdown('txt_manage_account_memos_value', wrapper, '3312');

      expect(setFormValues).toBeCalled();

      userEvent.click(container.querySelector('.icon.icon-close')!);
      expect(handleShow).not.toBeCalled();
    });

    it('should render filter by Agent', async () => {
      const setFormValues = jest.fn();
      const handleShow = jest.fn();
      const dataCondition = {
        ...conditionLv1,
        operator: 'Where',
        item: { filterBy: 'Agent', operator: 'And', value: '0000' }
      };
      const wrapper = await renderComponent(
        getProps({
          dataCondition: dataCondition,
          queryString: [dataCondition, conditionLv2],
          setFormValues,
          handleShow
        })
      );
      const { getByText, container } = wrapper;

      expect(
        getByText('txt_manage_account_memos_filter_by')
      ).toBeInTheDocument();

      selectedValueDropdown(
        'txt_manage_account_memos_filter_by',
        wrapper,
        'Agent'
      );

      selectedValueDropdown(
        'txt_manage_account_memos_operator',
        wrapper,
        'Not Equal'
      );

      selectedValueDropdown('txt_manage_account_memos_value', wrapper, '7658');

      expect(setFormValues).toBeCalled();

      userEvent.click(container.querySelector('.icon.icon-close')!);
      expect(handleShow).not.toBeCalled();
    });

    it('should render filter by Date with DateRange', async () => {
      const setFormValues = jest.fn();
      const handleShow = jest.fn();
      const dataCondition = {
        ...conditionLv1,
        item: { filterBy: 'Date', operator: 'Not Between', value: null }
      };
      const wrapper = await renderComponent(
        getProps({
          dataCondition: dataCondition,
          queryString: [dataCondition, conditionLv2],
          setFormValues,
          handleShow
        })
      );

      const { getByText, container } = wrapper;

      expect(
        getByText('txt_manage_account_memos_filter_by')
      ).toBeInTheDocument();

      selectedValueDropdown(
        'txt_manage_account_memos_filter_by',
        wrapper,
        'Date'
      );

      selectedValueDropdown(
        'txt_manage_account_memos_operator',
        wrapper,
        'Before'
      );

      const input = wrapper.baseElement.querySelector(
        '.text-field-container > input'
      );
      userEvent.type(input, '10');
      fireEvent.blur(input);

      expect(setFormValues).toBeCalled();

      userEvent.click(container.querySelector('.icon.icon-close')!);
      expect(handleShow).not.toBeCalled();
    });

    it('should render filter by Date with Date', async () => {
      const setFormValues = jest.fn();
      const handleShow = jest.fn();
      const dataCondition = {
        ...conditionLv1,
        conditions: [conditionLv2, conditionLv2],
        item: { filterBy: 'Date', operator: 'After', value: null }
      };
      const wrapper = await renderComponent(
        getProps({
          dataCondition: dataCondition,
          queryString: [dataCondition],
          setFormValues,
          handleShow
        })
      );

      const { getByText, container } = wrapper;

      expect(
        getByText('txt_manage_account_memos_filter_by')
      ).toBeInTheDocument();

      selectedValueDropdown(
        'txt_manage_account_memos_filter_by',
        wrapper,
        'Date'
      );

      selectedValueDropdown(
        'txt_manage_account_memos_operator',
        wrapper,
        'Before'
      );

      const input = wrapper.baseElement.querySelector(
        '.text-field-container > input'
      );
      userEvent.type(input, '10');
      fireEvent.blur(input);

      expect(setFormValues).toBeCalled();

      userEvent.click(container.querySelector('.icon.icon-close')!);
      expect(handleShow).not.toBeCalled();
    });

    it('should render filter by Memo Text', async () => {
      const setFormValues = jest.fn();
      const handleShow = jest.fn();
      const dataCondition = {
        ...conditionLv1,
        conditions: [conditionLv1, conditionLv1],
        item: { filterBy: 'Memo Text', operator: 'Is', value: 'text' }
      };
      const wrapper = await renderComponent(
        getProps({
          dataCondition: dataCondition,
          queryString: [dataCondition],
          setFormValues,
          handleShow
        })
      );

      const { getByText, container } = wrapper;

      expect(
        getByText('txt_manage_account_memos_filter_by')
      ).toBeInTheDocument();

      selectedValueDropdown(
        'txt_manage_account_memos_filter_by',
        wrapper,
        'Memo Text'
      );

      selectedValueDropdown(
        'txt_manage_account_memos_operator',
        wrapper,
        'Contains'
      );
      userEvent.type(getByText('txt_manage_account_memos_value'), 'value');
      userEvent.type(getByText('txt_manage_account_memos_value'), '{enter}');

      expect(setFormValues).toBeCalled();

      userEvent.click(container.querySelector('.icon.icon-close')!);
      expect(handleShow).not.toBeCalled();
    });

    it('should render filter by Type', async () => {
      const setFormValues = jest.fn();
      const handleShow = jest.fn();
      const dataCondition = {
        ...conditionLv1,
        conditions: [conditionLv1, conditionLv1],
        item: { filterBy: 'Type', operator: 'Equal', value: 'Type02' }
      };
      const wrapper = await renderComponent(
        getProps({
          dataCondition: dataCondition,
          queryString: [dataCondition],
          setFormValues,
          handleShow
        })
      );
      const { getByText, container } = wrapper;

      expect(
        getByText('txt_manage_account_memos_filter_by')
      ).toBeInTheDocument();

      selectedValueDropdown(
        'txt_manage_account_memos_filter_by',
        wrapper,
        'Type'
      );

      selectedValueDropdown(
        'txt_manage_account_memos_operator',
        wrapper,
        'Not Equal'
      );

      selectedValueDropdown(
        'txt_manage_account_memos_value',
        wrapper,
        'Type01'
      );

      expect(setFormValues).toBeCalled();

      userEvent.click(container.querySelector('.icon.icon-close')!);
      expect(handleShow).not.toBeCalled();
    });

    it('should render filter by Date', async () => {
      const setFormValues = jest.fn();
      const handleShow = jest.fn();
      const condition = {
        ...conditionLv1,
        item: { filterBy: 'Date', operator: 'Equal', value: '' }
      };
      const dataCondition = {
        ...condition,
        conditions: [condition, condition]
      };
      const wrapper = await renderComponent(
        getProps({
          dataCondition: dataCondition,
          queryString: [dataCondition],
          setFormValues,
          handleShow
        })
      );
      const { getByText, container } = wrapper;

      expect(
        getByText('txt_manage_account_memos_filter_by')
      ).toBeInTheDocument();

      selectedValueDropdown(
        'txt_manage_account_memos_filter_by',
        wrapper,
        'Type'
      );

      selectedValueDropdown(
        'txt_manage_account_memos_operator',
        wrapper,
        'Before'
      );

      const input = wrapper.baseElement.querySelector(
        '.text-field-container > input'
      );
      userEvent.type(input, '10');
      fireEvent.blur(input);

      expect(setFormValues).toBeCalled();

      userEvent.click(container.querySelector('.icon.icon-close')!);
      expect(handleShow).not.toBeCalled();
    });

    it('should render filter by Entered ID', async () => {
      const setFormValues = jest.fn();
      const handleShow = jest.fn();
      const dataCondition = {
        ...conditionLv1,
        item: { filterBy: 'Entered ID', operator: 'Equal', value: '01' }
      };
      const wrapper = await renderComponent(
        getProps({
          dataCondition: dataCondition,
          queryString: [dataCondition],
          setFormValues,
          handleShow
        })
      );

      const { getByText, container } = wrapper;

      expect(
        getByText('txt_manage_account_memos_filter_by')
      ).toBeInTheDocument();

      selectedValueDropdown(
        'txt_manage_account_memos_filter_by',
        wrapper,
        'Entered ID'
      );

      selectedValueDropdown(
        'txt_manage_account_memos_operator',
        wrapper,
        'Not Equal'
      );

      userEvent.type(getByText('txt_manage_account_memos_value'), '23');

      expect(setFormValues).toBeCalled();

      userEvent.click(container.querySelector('.icon.icon-close')!);
      expect(handleShow).toBeCalled();
    });

    it('should render filter by default', async () => {
      const setFormValues = jest.fn();
      const handleShow = jest.fn();
      const dataCondition = {
        ...conditionLv1,
        item: { filterBy: '', operator: 'Equal', value: 'text' }
      };
      const { getByText, container } = await renderComponent(
        getProps({
          dataCondition: dataCondition,
          queryString: [dataCondition],
          setFormValues,
          handleShow
        })
      );

      expect(
        getByText('txt_manage_account_memos_filter_by')
      ).toBeInTheDocument();

      userEvent.click(container.querySelector('.icon.icon-close')!);
      expect(handleShow).toBeCalled();
    });
  });

  describe('> without parent', () => {
    const getProps = ({
      dataCondition,
      queryString = [],
      ...props
    }: Partial<ConditionFormProps & ConfigureParametersFormValue> = {}) => ({
      formValues: { queryString },
      dataCondition,
      isChildrenGroup: undefined,
      idParent: undefined,
      ...props
    });

    it('should render filter by Sys', async () => {
      const setFormValues = jest.fn();
      const handleShow = jest.fn();
      const dataCondition = { ...conditionLv1, operator: 'And' };
      const wrapper = await renderComponent(
        getProps({
          dataCondition: dataCondition,
          queryString: [dataCondition],
          setFormValues,
          handleShow
        })
      );

      const { getByText, container } = wrapper;
      expect(
        getByText('txt_manage_account_memos_filter_by')
      ).toBeInTheDocument();

      selectedValueDropdown(
        'txt_manage_account_memos_filter_by',
        wrapper,
        'Sys'
      );
      selectedValueDropdown(
        'txt_manage_account_memos_operator',
        wrapper,
        'Not Equal'
      );
      selectedValueDropdown('txt_manage_account_memos_value', wrapper, '0001');

      expect(setFormValues).toBeCalled();

      userEvent.click(container.querySelector('.icon.icon-close')!);
      expect(handleShow).not.toBeCalled();
    });

    it('should render filter by Prin', async () => {
      const setFormValues = jest.fn();
      const handleShow = jest.fn();
      const dataCondition = {
        ...conditionLv1,
        operator: 'Where',
        item: { filterBy: 'Prin', operator: 'And', value: '0000' }
      };
      const { getByText, container } = await renderComponent(
        getProps({
          dataCondition: dataCondition,
          queryString: [dataCondition],
          setFormValues,
          handleShow
        })
      );

      expect(
        getByText('txt_manage_account_memos_filter_by')
      ).toBeInTheDocument();

      userEvent.click(getByText('txt_manage_account_memos_filter_by'));

      userEvent.click(getByText('txt_manage_account_memos_value'));

      userEvent.click(getByText('txt_manage_account_memos_operator'));

      userEvent.click(container.querySelector('.icon.icon-close')!);
      expect(handleShow).toBeCalled();
    });

    it('should render filter by Agent', async () => {
      const setFormValues = jest.fn();
      const handleShow = jest.fn();
      const dataCondition = {
        ...conditionLv1,
        operator: 'Where',
        item: { filterBy: 'Agent', operator: 'And', value: '0000' }
      };
      const { getByText, container } = await renderComponent(
        getProps({
          dataCondition: dataCondition,
          queryString: [dataCondition, conditionLv2],
          setFormValues,
          handleShow
        })
      );

      expect(
        getByText('txt_manage_account_memos_filter_by')
      ).toBeInTheDocument();

      userEvent.click(getByText('txt_manage_account_memos_filter_by'));

      userEvent.click(getByText('txt_manage_account_memos_value'));

      userEvent.click(getByText('txt_manage_account_memos_operator'));

      userEvent.click(container.querySelector('.icon.icon-close')!);
      expect(handleShow).not.toBeCalled();
    });

    it('should render filter by Date with DateRange', async () => {
      const setFormValues = jest.fn();
      const handleShow = jest.fn();
      const dataCondition = {
        ...conditionLv1,
        item: { filterBy: 'Date', operator: 'Not Between', value: null }
      };
      const { getByText, getAllByText, container } = await renderComponent(
        getProps({
          dataCondition: dataCondition,
          queryString: [dataCondition],
          setFormValues,
          handleShow
        })
      );

      expect(
        getByText('txt_manage_account_memos_filter_by')
      ).toBeInTheDocument();

      userEvent.click(getByText('txt_manage_account_memos_filter_by'));

      userEvent.click(getByText('txt_manage_account_memos_value'));
      userEvent.click(getAllByText('10')[0]);

      userEvent.click(getByText('txt_manage_account_memos_operator'));

      userEvent.click(container.querySelector('.icon.icon-close')!);
      expect(handleShow).not.toBeCalled();
    });

    it('should render filter by Date with Date', async () => {
      const setFormValues = jest.fn();
      const handleShow = jest.fn();
      const dataCondition = {
        ...conditionLv1,
        item: { filterBy: 'Date', operator: 'After', value: null }
      };
      const { getByText, getAllByText, container } = await renderComponent(
        getProps({
          dataCondition: dataCondition,
          queryString: [dataCondition],
          setFormValues,
          handleShow
        })
      );

      expect(
        getByText('txt_manage_account_memos_filter_by')
      ).toBeInTheDocument();

      userEvent.click(getByText('txt_manage_account_memos_filter_by'));

      userEvent.click(getByText('txt_manage_account_memos_value'));
      userEvent.click(getAllByText('10')[0]);

      userEvent.click(getByText('txt_manage_account_memos_operator'));

      userEvent.click(container.querySelector('.icon.icon-close')!);
      expect(handleShow).not.toBeCalled();
    });

    it('should render filter by Memo Text', async () => {
      const setFormValues = jest.fn();
      const handleShow = jest.fn();
      const dataCondition = {
        ...conditionLv1,
        item: { filterBy: 'Memo Text', operator: 'Is', value: 'text' }
      };
      const { getByText, container } = await renderComponent(
        getProps({
          dataCondition: dataCondition,
          queryString: [dataCondition],
          setFormValues,
          handleShow
        })
      );

      expect(
        getByText('txt_manage_account_memos_filter_by')
      ).toBeInTheDocument();

      userEvent.click(getByText('txt_manage_account_memos_filter_by'));

      userEvent.type(getByText('txt_manage_account_memos_value'), '{enter}');

      userEvent.type(getByText('txt_manage_account_memos_value'), 'value');

      userEvent.click(getByText('txt_manage_account_memos_operator'));

      userEvent.click(container.querySelector('.icon.icon-close')!);
      expect(handleShow).not.toBeCalled();
    });

    it('should render filter by Entered ID', async () => {
      const setFormValues = jest.fn();
      const handleShow = jest.fn();
      const dataCondition = {
        ...conditionLv1,
        item: { filterBy: 'Entered ID', operator: 'Equal', value: '01' }
      };
      const { getByText, container } = await renderComponent(
        getProps({
          dataCondition: dataCondition,
          queryString: [dataCondition],
          setFormValues,
          handleShow
        })
      );

      expect(
        getByText('txt_manage_account_memos_filter_by')
      ).toBeInTheDocument();

      userEvent.click(getByText('txt_manage_account_memos_filter_by'));

      userEvent.type(getByText('txt_manage_account_memos_value'), '23');

      userEvent.click(getByText('txt_manage_account_memos_operator'));

      userEvent.click(container.querySelector('.icon.icon-close')!);
      expect(handleShow).not.toBeCalled();
    });

    it('should render filter by default', async () => {
      const setFormValues = jest.fn();
      const handleShow = jest.fn();
      const dataCondition = {
        ...conditionLv1,
        item: { filterBy: '', operator: 'Equal', value: 'text' }
      };
      const { getByText, container } = await renderComponent(
        getProps({
          dataCondition: dataCondition,
          queryString: [dataCondition],
          setFormValues,
          handleShow
        })
      );

      expect(
        getByText('txt_manage_account_memos_filter_by')
      ).toBeInTheDocument();

      userEvent.click(getByText('txt_manage_account_memos_filter_by'));

      userEvent.type(getByText('txt_manage_account_memos_value'), 'value');

      userEvent.click(container.querySelector('.icon.icon-close')!);
      expect(handleShow).not.toBeCalled();
    });
  });
});
