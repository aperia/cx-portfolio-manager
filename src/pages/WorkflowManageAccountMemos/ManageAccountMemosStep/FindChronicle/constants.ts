export const TYPE_MEMOS_CONDITION = {
  CONDITION: 'Condition',
  CONDITION_GROUP: 'Group'
};

export const ACTIONS_CONDITION = [
  { text: 'Add Condition', code: 'Condition' },
  { text: 'Add Condition Group', code: 'Group' }
];

export const TYPE_CONDITION = [
  { description: 'And', value: 'And' },
  { description: 'Or', value: 'Or' }
];

export const FILTER_BY_CIS = [
  {
    value: 'Sys',
    description: 'Sys'
  },
  {
    value: 'Prin',
    description: 'Prin'
  },
  {
    value: 'Agent',
    description: 'Agent'
  },
  {
    value: 'Date',
    description: 'Date'
  },
  {
    value: 'Entered ID',
    description: 'Entered ID'
  },
  {
    value: 'Memo Text',
    description: 'Memo Text'
  },
  {
    value: 'Type',
    description: 'Type'
  }
];

export const OPERATOR_GROUP = {
  enteredID: [
    {
      value: 'equal',
      description: 'Equal'
    },
    {
      value: 'notEqual',
      description: 'Not Equal'
    }
  ],
  sys: [
    {
      value: 'equal',
      description: 'Equal'
    },
    {
      value: 'notEqual',
      description: 'Not Equal'
    }
  ],
  prin: [
    {
      value: 'equal',
      description: 'Equal'
    },
    {
      value: 'notEqual',
      description: 'Not Equal'
    }
  ],
  agent: [
    {
      value: 'equal',
      description: 'Equal'
    },
    {
      value: 'notEqual',
      description: 'Not Equal'
    }
  ],
  type: [
    {
      value: 'equal',
      description: 'Equal'
    },
    {
      value: 'notEqual',
      description: 'Not Equal'
    }
  ],
  date: [
    {
      value: 'between',
      description: 'Between'
    },
    {
      value: 'before',
      description: 'Before'
    },
    {
      value: 'after',
      description: 'After'
    },
    {
      value: 'notBetween',
      description: 'Not Between'
    }
  ],
  memoText: [
    {
      value: 'equal',
      description: 'Equal'
    },
    {
      value: 'contains',
      description: 'Contains'
    },
    {
      value: 'notContain',
      description: 'Not Contain'
    }
  ]
};

export const dataQueryString = [
  {
    id: '1',
    operator: 'Where',
    type: 'Condition',
    item: {
      filterBy: 'Sys',
      operator: 'Equal',
      value: '0001'
    }
  },
  {
    id: '2',
    operator: 'And',
    type: 'Condition',
    item: {
      filterBy: 'Sys',
      operator: 'Equal',
      value: '0001'
    }
  }
];

export const TYPE_VALUE = {
  FILTER_BY: 'filterBy',
  OPERATOR: 'operator',
  VALUE: 'value',
  OPERATOR_PARENT: 'operatorParent'
};

export const TYPE_FIELD = [
  {
    value: 'Type01',
    description: 'Type01'
  },
  {
    value: 'Type02',
    description: 'Type02'
  },
  {
    value: 'TypeAA',
    description: 'TypeAA'
  }
];

export const SPA_VALUE = {
  SYS: [
    {
      value: '0000',
      description: '0000'
    },
    {
      value: '0001',
      description: '0001'
    },
    {
      value: '0002',
      description: '0002'
    }
  ],
  PRIN: [
    {
      value: '2212',
      description: '2212'
    },
    {
      value: '3312',
      description: '3312'
    },
    {
      value: '5163',
      description: '5163'
    }
  ],
  AGENT: [
    {
      value: '4654',
      description: '4654'
    },
    {
      value: '7658',
      description: '7658'
    },
    {
      value: '8769',
      description: '8769'
    }
  ]
};
