import userEvent from '@testing-library/user-event';
import { WorkflowMetadataOverride } from 'app/fixtures/workflow-metadata';
import { mockUseDispatchFnc, renderWithMockStore } from 'app/utils';
import React from 'react';
import FindCIS from '.';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const mockUseDispatch = mockUseDispatchFnc();

jest.mock('./Condition', () => {
  return {
    __esModule: true,
    default: ({ handleShow }: any) => (
      <div onClick={handleShow}>ConditionComponent</div>
    )
  };
});

const getInitialState = ({
  elements = WorkflowMetadataOverride
}: any = {}) => ({
  workflowSetup: { elementMetadata: { elements } }
});

const renderComponent = (props: any) => {
  return renderWithMockStore(<FindCIS {...props} />, {
    initialState: getInitialState()
  });
};

describe('WorkflowBulkSuspendFraudStrategy > ConfigureParametersStep', () => {
  const mockDispatch = jest.fn();
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
  });

  it('Should render FindCIS component', async () => {
    const wrapper = await renderComponent({
      formValues: {
        queryString: [
          {
            id: '0001',
            operator: 'Where',
            type: 'Condition',
            item: {
              filterBy: '',
              operator: '',
              value: ''
            }
          }
        ]
      },
      hasInstance: true,
      setFormValues: jest.fn(),
      stepId: '2',
      selectedStep: ''
    });

    expect(
      wrapper.getByText(
        'txt_manage_account_memos_add_memos_parameter_chronicle_find'
      )
    ).toBeInTheDocument;
  });

  it('Should render FindCIS component', async () => {
    const wrapper = await renderComponent({
      formValues: {
        queryString: [
          {
            id: '0001',
            operator: 'Where',
            type: 'Condition',
            item: {
              filterBy: '1212',
              operator: '',
              value: ''
            }
          }
        ]
      },
      hasInstance: true,
      setFormValues: jest.fn(),
      stepId: '2',
      selectedStep: ''
    });

    expect(
      wrapper.getByText(
        'txt_manage_account_memos_add_memos_parameter_chronicle_find'
      )
    ).toBeInTheDocument;
  });

  it('Should render FindCIS component', async () => {
    const wrapper = await renderComponent({
      formValues: {
        queryString: [
          {
            id: '0001',
            operator: 'Where',
            type: 'Condition',
            item: {
              filterBy: '1212',
              operator: '122122',
              value: ''
            }
          }
        ]
      },
      hasInstance: true,
      setFormValues: jest.fn(),
      stepId: '2',
      selectedStep: ''
    });

    expect(
      wrapper.getByText(
        'txt_manage_account_memos_add_memos_parameter_chronicle_find'
      )
    ).toBeInTheDocument;
  });

  it('Should render FindCIS component', async () => {
    const wrapper = await renderComponent({
      formValues: {
        queryString: [
          {
            id: '1642503007202',
            operator: 'And',
            type: 'Group',
            level: '1',
            conditions: [
              {
                id: '16425030072021',
                operator: '',
                type: 'Condition',
                item: {
                  filterBy: '',
                  operator: '',
                  value: ''
                }
              }
            ]
          }
        ]
      },
      hasInstance: true,
      setFormValues: jest.fn(),
      stepId: '2',
      selectedStep: ''
    });

    expect(
      wrapper.getByText(
        'txt_manage_account_memos_add_memos_parameter_chronicle_find'
      )
    ).toBeInTheDocument;
  });

  it('Should render FindCIS component', async () => {
    const wrapper = await renderComponent({
      formValues: {
        queryString: [
          {
            id: '1642503007202',
            operator: 'And',
            type: 'Group',
            level: '1',
            conditions: [
              {
                id: '16425030072021',
                operator: '',
                type: 'Condition',
                item: {
                  filterBy: '1122',
                  operator: '',
                  value: ''
                }
              }
            ]
          }
        ]
      },
      hasInstance: true,
      setFormValues: jest.fn(),
      stepId: '2',
      selectedStep: ''
    });

    expect(
      wrapper.getByText(
        'txt_manage_account_memos_add_memos_parameter_chronicle_find'
      )
    ).toBeInTheDocument;
  });

  it('Should render FindCIS component', async () => {
    const wrapper = await renderComponent({
      formValues: {
        queryString: [
          {
            id: '1642503007202',
            operator: 'And',
            type: 'Group',
            level: '1',
            conditions: [
              {
                id: '16425030072021',
                operator: '',
                type: 'Condition',
                item: {
                  filterBy: '1122',
                  operator: '12121',
                  value: ''
                }
              }
            ]
          }
        ]
      },
      hasInstance: true,
      setFormValues: jest.fn(),
      stepId: '2',
      selectedStep: ''
    });

    expect(
      wrapper.getByText(
        'txt_manage_account_memos_add_memos_parameter_chronicle_find'
      )
    ).toBeInTheDocument;
  });

  it('Should render FindCIS component', async () => {
    const wrapper = await renderComponent({
      formValues: {
        queryString: [
          {
            id: '1642503007202',
            operator: 'And',
            type: 'Group',
            level: '1',
            conditions: [
              {
                id: '16425030072021',
                operator: '',
                type: 'Condition',
                item: {
                  filterBy: '1122',
                  operator: '12121',
                  value: '12212'
                }
              },
              {
                id: '1642503009254',
                operator: 'And',
                type: 'Group',
                level: '2',
                conditions: [
                  {
                    id: '16425030092541',
                    operator: '',
                    type: 'Condition',
                    item: {
                      filterBy: '1111',
                      operator: '',
                      value: ''
                    }
                  }
                ]
              }
            ]
          }
        ]
      },
      hasInstance: true,
      setFormValues: jest.fn(),
      stepId: '2',
      selectedStep: ''
    });

    expect(
      wrapper.getByText(
        'txt_manage_account_memos_add_memos_parameter_chronicle_find'
      )
    ).toBeInTheDocument;
  });

  it('Should render FindCIS component', async () => {
    const wrapper = await renderComponent({
      formValues: {
        queryString: [
          {
            id: '1642503007202',
            operator: 'And',
            type: 'Group',
            level: '1',
            conditions: [
              {
                id: '16425030072021',
                operator: '',
                type: 'Condition',
                item: {
                  filterBy: '1122',
                  operator: '12121',
                  value: '12212'
                }
              },
              {
                id: '1642503009254',
                operator: 'And',
                type: 'Group',
                level: '2',
                conditions: [
                  {
                    id: '16425030092541',
                    operator: '',
                    type: 'Condition',
                    item: {
                      filterBy: '1111',
                      operator: '12121',
                      value: ''
                    }
                  }
                ]
              }
            ]
          }
        ]
      },
      hasInstance: true,
      setFormValues: jest.fn(),
      stepId: '2',
      selectedStep: ''
    });

    expect(
      wrapper.getByText(
        'txt_manage_account_memos_add_memos_parameter_chronicle_find'
      )
    ).toBeInTheDocument;
  });

  it('Should render FindCIS component', async () => {
    const wrapper = await renderComponent({
      formValues: {
        queryString: [
          {
            id: '1642503007202',
            operator: 'And',
            type: 'Group',
            level: '1',
            conditions: [
              {
                id: '16425030072021',
                operator: '',
                type: 'Condition',
                item: {
                  filterBy: '1122',
                  operator: '12121',
                  value: '12212'
                }
              },
              {
                id: '1642503009254',
                operator: 'And',
                type: 'Group',
                level: '2',
                conditions: [
                  {
                    id: '16425030092541',
                    operator: '',
                    type: 'Condition',
                    item: {
                      filterBy: '',
                      operator: '',
                      value: ''
                    }
                  }
                ]
              }
            ]
          }
        ]
      },
      hasInstance: true,
      setFormValues: jest.fn(),
      stepId: '2',
      selectedStep: ''
    });

    expect(
      wrapper.getByText(
        'txt_manage_account_memos_add_memos_parameter_chronicle_find'
      )
    ).toBeInTheDocument;
  });

  it('Should render FindCIS component and trigger add form simple', async () => {
    const wrapper = await renderComponent({
      formValues: {
        queryString: []
      },
      hasInstance: true,
      setFormValues: jest.fn(),
      stepId: '2',
      selectedStep: ''
    });

    expect(wrapper.getByText('txt_manage_account_memos_add_condition'))
      .toBeInTheDocument;

    userEvent.click(
      wrapper.getByText('txt_manage_account_memos_add_condition')
    );
  });

  it('Should render FindCIS component and trigger add form group', async () => {
    const wrapper = await renderComponent({
      formValues: {
        queryString: []
      },
      hasInstance: true,
      setFormValues: jest.fn(),
      stepId: '2',
      selectedStep: ''
    });

    expect(wrapper.getByText('txt_manage_account_memos_add_condition_group'))
      .toBeInTheDocument;

    userEvent.click(
      wrapper.getByText('txt_manage_account_memos_add_condition_group')
    );
  });

  it('Should render FindCIS component and trigger ConditionComponent', async () => {
    const wrapper = await renderComponent({
      formValues: {
        queryString: []
      },
      hasInstance: true,
      setFormValues: jest.fn(),
      stepId: '2',
      selectedStep: ''
    });

    userEvent.click(
      wrapper.getByText('txt_manage_account_memos_add_condition')
    );

    expect(wrapper.getByText('ConditionComponent')).toBeInTheDocument;

    userEvent.click(wrapper.getByText('ConditionComponent'));
  });
});
