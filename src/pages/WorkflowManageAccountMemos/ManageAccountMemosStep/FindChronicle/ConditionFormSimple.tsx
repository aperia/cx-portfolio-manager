import EnhanceDatePicker from 'app/components/EnhanceDatePicker';
import FieldTooltip from 'app/components/FieldTooltip';
import FormatTextBox from 'app/components/FormatTextBox';
import TextAreaCountDown from 'app/components/TextAreaCountDown';
import { useFormValidations } from 'app/hooks';
import {
  Button,
  ComboBox,
  DateRangePicker,
  DropdownButton,
  DropdownList,
  Icon,
  TextBox,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import { findIndex, isEmpty } from 'lodash';
import { useSelectElementMetadataManageAcountMemos } from 'pages/_commons/redux/WorkflowSetup';
import React, { useEffect, useMemo, useRef, useState } from 'react';
import { ConditionFormProps } from './ConditionForm';
import { TYPE_CONDITION, TYPE_VALUE } from './constants';
import { valOperator } from './helpers';

const ConditionFormSimple: React.FC<ConditionFormProps> = props => {
  const { t } = useTranslation();
  const {
    setFormValues,
    formValues,
    dataCondition,
    isChildrenGroup = false,
    handleShow,
    idParent = ''
  } = props;
  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;
  const [resetTouch, setResetTouch] = useState(0);

  const metadata = useSelectElementMetadataManageAcountMemos();

  const {
    memosTypesFormChronicle,
    memosSys,
    memosPrin,
    memosAgent,
    memosTypeOperatorValue
  } = metadata;

  const currentErrors = useMemo(() => {
    const isEmtyValue =
      dataCondition?.item?.filterBy === 'Date'
        ? dataCondition?.item?.value === ''
        : isEmpty(dataCondition?.item?.value);
    return {
      filterBy: isEmpty(dataCondition?.item?.filterBy) && {
        status: true,
        message: t('txt_required_validation', {
          fieldName: 'Filter By'
        })
      },
      operator: !isEmpty(dataCondition?.item?.filterBy) &&
        isEmpty(dataCondition?.item?.operator) && {
          status: true,
          message: t('txt_required_validation', {
            fieldName: 'Operator'
          })
        },
      value: !isEmpty(dataCondition?.item?.filterBy) &&
        isEmtyValue && {
          status: true,
          message: t('txt_required_validation', {
            fieldName: 'Value'
          })
        }
    } as Record<any, IFormError>;
  }, [
    dataCondition?.item?.filterBy,
    dataCondition?.item?.operator,
    dataCondition?.item?.value,
    t
  ]);

  const [, errors, setTouched] = useFormValidations<any>(
    currentErrors,
    resetTouch
  );

  useEffect(() => {
    process.nextTick(() => {
      setResetTouch(t => t + 1);
    });
  }, [
    dataCondition?.item?.filterBy,
    dataCondition?.item?.operator,
    dataCondition?.item?.value
  ]);

  const setFormValue = (type: string, valTemp: MagicKeyValue, val: any) => {
    switch (type) {
      case TYPE_VALUE.FILTER_BY:
        return {
          ...valTemp,
          item: {
            filterBy: val,
            operator: '',
            value: ''
          }
        };
      case TYPE_VALUE.OPERATOR:
        if (valTemp?.item?.filterBy === 'Date') {
          return {
            ...valTemp,
            item: {
              ...valTemp?.item,
              operator: val,
              value: ''
            }
          };
        }
        return {
          ...valTemp,
          item: {
            ...valTemp?.item,
            operator: val
          }
        };
      case TYPE_VALUE.VALUE:
        return {
          ...valTemp,
          item: {
            ...valTemp?.item,
            value: val
          }
        };
      case TYPE_VALUE.OPERATOR_PARENT:
        return {
          ...valTemp,
          operator: val
        };
    }
  };

  const handleChangeForm = (val: any, type: string) => {
    if (!isChildrenGroup) {
      let valTemp = { ...dataCondition };
      const index = findIndex(formValues?.queryString, [
        'id',
        dataCondition?.id
      ]);
      const queryVal = [...(formValues?.queryString as MagicKeyValue[])];
      valTemp = setFormValue(type, valTemp, val) as MagicKeyValue;
      queryVal[index] = valTemp;
      keepRef.current.setFormValues({
        ...formValues,
        queryString: queryVal
      });
      return;
    }
    const indexParent = findIndex(formValues?.queryString, ['id', idParent]);
    const queryStringVal = [...(formValues?.queryString as MagicKeyValue[])];

    if (indexParent === -1) {
      // condition group level 2
      const arrIsConditions = queryStringVal.filter(item => !!item?.conditions);
      let id = '';
      arrIsConditions.forEach(item => {
        item?.conditions.forEach((val: MagicKeyValue) => {
          if (val?.id === idParent) {
            id = item?.id;
          }
        });
      });

      const indexParentLevel1 = findIndex(queryStringVal, ['id', id]);
      let valParentLevel1 = { ...queryStringVal[indexParentLevel1] };
      const valConditionsLevel1 = [...valParentLevel1?.conditions];

      const indexParentLevel2 = findIndex(valConditionsLevel1, [
        'id',
        idParent
      ]);

      let valParentLevel2 = { ...valConditionsLevel1[indexParentLevel2] };
      const valConditionsLevel2 = [...valParentLevel2?.conditions];

      const indexCondition = findIndex(valConditionsLevel2, [
        'id',
        dataCondition?.id
      ]);

      let valTemp = {
        ...valConditionsLevel2[indexCondition]
      };

      valTemp = setFormValue(type, valTemp, val);
      valConditionsLevel2[indexCondition] = valTemp;

      valParentLevel2 = {
        ...valParentLevel2,
        conditions: valConditionsLevel2
      };
      valConditionsLevel1[indexParentLevel2] = valParentLevel2;
      valParentLevel1 = {
        ...valParentLevel1,
        conditions: valConditionsLevel1
      };

      queryStringVal[indexParentLevel1] = valParentLevel1;
      keepRef.current.setFormValues({
        ...formValues,
        queryString: queryStringVal
      });

      return;
    }

    let valParent = { ...queryStringVal[indexParent] };

    const conditionsVal = [...valParent?.conditions];
    const indexChildren = findIndex(valParent?.conditions, [
      'id',
      dataCondition?.id
    ]);

    let valTemp = {
      ...conditionsVal[indexChildren]
    };

    valTemp = setFormValue(type, valTemp, val);
    conditionsVal[indexChildren] = valTemp;
    valParent = {
      ...valParent,
      conditions: conditionsVal
    };
    queryStringVal[indexParent] = valParent;
    keepRef.current.setFormValues({
      ...formValues,
      queryString: queryStringVal
    });
  };

  const isOperator = dataCondition?.operator !== 'Where';

  const isEmptyOperator = isEmpty(dataCondition?.operator);

  const addButtonCondition = () => (
    <DropdownButton
      buttonProps={{
        children: dataCondition?.operator,
        variant: 'outline-secondary',
        size: 'sm',
        className: 'ml-n8'
      }}
      onSelect={e =>
        handleChangeForm(e.target.value, TYPE_VALUE.OPERATOR_PARENT)
      }
    >
      {TYPE_CONDITION.map(item => (
        <DropdownList.Item
          key={item.value}
          label={item.description}
          value={item.value}
        />
      ))}
    </DropdownButton>
  );

  const renderTemplateControl = (control: string) => {
    switch (control) {
      case 'Type':
        return (
          <FieldTooltip
            placement="right-start"
            element={
              <div>
                <p className="pb-8">
                  {t('txt_manage_account_memos_title_tooltip_type')}
                </p>
                <p>{t('txt_manage_account_memos_tooltip_type')}</p>
              </div>
            }
          >
            <ComboBox
              placeholder={t('txt_enter_a_value')}
              dataTestId="findMemos_value"
              textField="text"
              value={dataCondition?.item?.value}
              onChange={e => handleChangeForm(e.target.value, TYPE_VALUE.VALUE)}
              required
              disabled={isEmpty(dataCondition?.item?.filterBy)}
              label={t('txt_manage_account_memos_value')}
              onFocus={setTouched('value')}
              onBlur={setTouched('value', true)}
              error={errors?.value}
            >
              {memosTypeOperatorValue?.map(item => (
                <ComboBox.Item
                  key={item.value}
                  value={item.value}
                  label={item.value}
                />
              ))}
            </ComboBox>
          </FieldTooltip>
        );
      case 'Sys':
        return (
          <FieldTooltip
            placement="right-start"
            element={
              <div>
                <p className="pb-8">
                  {t('txt_manage_account_memos_title_tooltip_sys')}
                </p>
                <p>{t('txt_manage_account_memos_tooltip_sys')}</p>
              </div>
            }
          >
            <ComboBox
              placeholder={t('txt_enter_a_value')}
              dataTestId="findMemos_value"
              textField="text"
              value={dataCondition?.item?.value}
              onChange={e => handleChangeForm(e.target.value, TYPE_VALUE.VALUE)}
              required
              disabled={isEmpty(dataCondition?.item?.filterBy)}
              label={t('txt_manage_account_memos_value')}
              onFocus={setTouched('value')}
              onBlur={setTouched('value', true)}
              error={errors?.value}
            >
              {memosSys?.map(item => (
                <ComboBox.Item
                  key={item.value}
                  value={item.value}
                  label={item.value}
                />
              ))}
            </ComboBox>
          </FieldTooltip>
        );
      case 'Prin':
        return (
          <FieldTooltip
            placement="right-start"
            element={
              <div>
                <p className="pb-8">
                  {t('txt_manage_account_memos_title_tooltip_prin')}
                </p>
                <p>{t('txt_manage_account_memos_tooltip_prin')}</p>
              </div>
            }
          >
            <ComboBox
              placeholder={t('txt_enter_a_value')}
              dataTestId="findMemos_value"
              textField="text"
              value={dataCondition?.item?.value}
              onChange={e => handleChangeForm(e.target.value, TYPE_VALUE.VALUE)}
              required
              disabled={isEmpty(dataCondition?.item?.filterBy)}
              label={t('txt_manage_account_memos_value')}
              onFocus={setTouched('value')}
              onBlur={setTouched('value', true)}
              error={errors?.value}
            >
              {memosPrin?.map(item => (
                <ComboBox.Item
                  key={item.value}
                  value={item.value}
                  label={item.value}
                />
              ))}
            </ComboBox>
          </FieldTooltip>
        );
      case 'Agent':
        return (
          <FieldTooltip
            placement="right-start"
            element={
              <div>
                <p className="pb-8">
                  {t('txt_manage_account_memos_title_tooltip_agent')}
                </p>
                <p>{t('txt_manage_account_memos_tooltip_agent')}</p>
              </div>
            }
          >
            <ComboBox
              placeholder={t('txt_enter_a_value')}
              dataTestId="findMemos_value"
              textField="text"
              value={dataCondition?.item?.value}
              onChange={e => handleChangeForm(e.target.value, TYPE_VALUE.VALUE)}
              required
              disabled={isEmpty(dataCondition?.item?.filterBy)}
              label={t('txt_manage_account_memos_value')}
              onFocus={setTouched('value')}
              onBlur={setTouched('value', true)}
              error={errors?.value}
            >
              {memosAgent?.map(item => (
                <ComboBox.Item
                  key={item.value}
                  value={item.value}
                  label={item.value}
                />
              ))}
            </ComboBox>
          </FieldTooltip>
        );
      case 'Date':
        const operatorVal = dataCondition?.item?.operator;
        if (operatorVal === 'Between' || operatorVal === 'Not Between') {
          return (
            <FieldTooltip
              placement="right-end"
              element={
                <div>
                  <p className="pb-8">
                    {t('txt_manage_account_memos_title_tooltip_date')}
                  </p>
                  <p>{t('txt_manage_account_memos_tooltip_date')}</p>
                </div>
              }
            >
              <DateRangePicker
                placeholder={t('txt_enter_a_value')}
                dataTestId="findMemos_value"
                value={dataCondition?.item?.value}
                onChange={e =>
                  handleChangeForm(e.target.value, TYPE_VALUE.VALUE)
                }
                required
                disabled={isEmpty(dataCondition?.item?.filterBy)}
                label={t('txt_manage_account_memos_value')}
                onFocus={setTouched('value')}
                onBlur={setTouched('value', true)}
                error={errors?.value}
              />
            </FieldTooltip>
          );
        }
        return (
          <FieldTooltip
            placement="right-end"
            element={
              <div>
                <p className="pb-8">
                  {t('txt_manage_account_memos_title_tooltip_date')}
                </p>
                <p>{t('txt_manage_account_memos_tooltip_date')}</p>
              </div>
            }
          >
            <EnhanceDatePicker
              placeholder={t('txt_enter_a_value')}
              dataTestId="findMemos_value"
              value={dataCondition?.item?.value}
              onChange={e => handleChangeForm(e.target.value, TYPE_VALUE.VALUE)}
              required
              disabled={isEmpty(dataCondition?.item?.filterBy)}
              label={t('txt_manage_account_memos_value')}
              onFocus={setTouched('value')}
              onBlur={setTouched('value', true)}
              error={errors?.value}
            />
          </FieldTooltip>
        );
      case 'Memo Text':
        return (
          <FieldTooltip
            placement="right-start"
            element={
              <div>
                <p className="pb-8">
                  {t('txt_manage_account_memos_title_tooltip_text')}
                </p>
                <p>{t('txt_manage_account_memos_tooltip_memo_text')}</p>
              </div>
            }
          >
            <TextAreaCountDown
              placeholder={t('txt_enter_a_value')}
              dataTestId="findMemos_value"
              onKeyPress={e => {
                e.key === 'Enter' && e.preventDefault();
              }}
              rows={4}
              value={dataCondition?.item?.value}
              onChange={e => handleChangeForm(e.target.value, TYPE_VALUE.VALUE)}
              maxLength={2000}
              disabled={isEmpty(dataCondition?.item?.filterBy)}
              label={t('txt_manage_account_memos_value')}
            />
          </FieldTooltip>
        );
      case 'Entered ID':
        return (
          <FieldTooltip
            placement="right-start"
            element={
              <div>
                <p className="pb-8">
                  {t('txt_manage_account_memos_title_tooltip_id')}
                </p>
                <p>{t('txt_manage_account_memos_tooltip_account')}</p>
              </div>
            }
          >
            <FormatTextBox
              placeholder={t('txt_enter_a_value')}
              dataTestId="findMemos_value"
              value={dataCondition?.item?.value}
              maxLength={16}
              autoFocus={false}
              onChange={e => handleChangeForm(e.target.value, TYPE_VALUE.VALUE)}
              required
              disabled={isEmpty(dataCondition?.item?.filterBy)}
              label={t('txt_manage_account_memos_value')}
              onFocus={setTouched('value')}
              onBlur={setTouched('value', true)}
              error={errors?.value}
              pattern="^[0-9]{0,16}$"
            />
          </FieldTooltip>
        );
      default:
        return (
          <TextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="findMemos_value"
            disabled
            label={t('txt_manage_account_memos_value')}
          />
        );
    }
  };

  // delete condition
  const handleDeleteCondition = () => {
    if (!isChildrenGroup) {
      if (dataCondition?.operator === 'Where') {
        if (formValues?.queryString?.length === 1) {
          keepRef.current.setFormValues({
            ...formValues,
            queryString: [],
            isEdit: false
          });
          handleShow();
          return;
        }
        const queryStringVal = [
          ...(formValues?.queryString as MagicKeyValue[])
        ];
        queryStringVal.splice(0, 1);
        queryStringVal[0] = {
          ...queryStringVal[0],
          operator: 'Where'
        };
        keepRef.current.setFormValues({
          ...formValues,
          queryString: queryStringVal
        });
        return;
      }
      const index = findIndex(formValues?.queryString, [
        'id',
        dataCondition?.id
      ]);
      const queryVal = [...(formValues?.queryString as MagicKeyValue[])];
      queryVal.splice(index, 1);
      keepRef.current.setFormValues({
        ...formValues,
        queryString: queryVal
      });
      return;
    }

    const indexParent = findIndex(formValues?.queryString, ['id', idParent]);
    const queryStringVal = [...(formValues?.queryString as MagicKeyValue[])];
    // group in group
    if (indexParent === -1) {
      // delete condition level 2
      const arrIsConditions = queryStringVal.filter(item => !!item?.conditions);
      let id = '';
      arrIsConditions.forEach(item => {
        item?.conditions.forEach((val: MagicKeyValue) => {
          if (val?.id === idParent) {
            id = item?.id;
          }
        });
      });

      const indexParentLevel1 = findIndex(queryStringVal, ['id', id]);
      let valParentLevel1 = { ...queryStringVal[indexParentLevel1] };
      const valConditionsLevel1 = [...valParentLevel1?.conditions];

      const indexParentLevel2 = findIndex(valConditionsLevel1, [
        'id',
        idParent
      ]);
      let valParentLevel2 = { ...valConditionsLevel1[indexParentLevel2] };

      const valConditionsLevel2 = [...valParentLevel2?.conditions];

      const indexDeleteCondition = findIndex(valConditionsLevel2, [
        'id',
        dataCondition?.id
      ]);
      valConditionsLevel2.splice(indexDeleteCondition, 1);

      if (valConditionsLevel2.length < 1) {
        valConditionsLevel1.splice(indexParentLevel2, 1);
        if (indexParentLevel2 === 0 && valConditionsLevel1.length > 0) {
          valConditionsLevel1[0] = {
            ...valConditionsLevel1[0],
            operator: ''
          };
        }
        valParentLevel1 = {
          ...valParentLevel1,
          conditions: valConditionsLevel1
        };
        if (valConditionsLevel1.length < 1) {
          keepRef.current.setFormValues({
            ...formValues,
            queryString: [],
            isEdit: false
          });
          handleShow();
          return;
        }
        queryStringVal[indexParentLevel1] = valParentLevel1;
        keepRef.current.setFormValues({
          ...formValues,
          queryString: queryStringVal
        });
        return;
      }

      if (indexDeleteCondition === 0) {
        valConditionsLevel2[0] = {
          ...valConditionsLevel2[0],
          operator: ''
        };
      }
      valParentLevel2 = {
        ...valParentLevel2,
        conditions: valConditionsLevel2
      };
      valConditionsLevel1[indexParentLevel2] = valParentLevel2;
      valParentLevel1 = {
        ...valParentLevel1,
        conditions: valConditionsLevel1
      };

      queryStringVal[indexParentLevel1] = valParentLevel1;
      keepRef.current.setFormValues({
        ...formValues,
        queryString: queryStringVal
      });
      return;
    }

    // group
    let valParent = { ...queryStringVal[indexParent] };

    const conditionsVal = [...valParent?.conditions];
    const indexChildren = findIndex(valParent?.conditions, [
      'id',
      dataCondition?.id
    ]);
    conditionsVal.splice(indexChildren, 1);

    if (conditionsVal.length < 1) {
      if (formValues?.queryString?.length === 1) {
        keepRef.current.setFormValues({
          ...formValues,
          queryString: [],
          isEdit: false
        });
        handleShow();
        return;
      }
      if (valParent?.operator === 'Where') {
        queryStringVal.splice(0, 1);
        queryStringVal[0] = {
          ...queryStringVal[0],
          operator: 'Where'
        };
        keepRef.current.setFormValues({
          ...formValues,
          queryString: queryStringVal
        });
        return;
      }

      queryStringVal.splice(indexParent, 1);
      keepRef.current.setFormValues({
        ...formValues,
        queryString: queryStringVal
      });
      return;
    }

    if (indexChildren === 0) {
      conditionsVal[0] = {
        ...conditionsVal[0],
        operator: ''
      };
    }

    valParent = {
      ...valParent,
      conditions: conditionsVal
    };
    queryStringVal[indexParent] = valParent;
    keepRef.current.setFormValues({
      ...formValues,
      queryString: queryStringVal
    });
  };

  const valOperatorForm = valOperator(
    dataCondition?.item?.filterBy,
    metadata
  ) as MagicKeyValue[];

  const classConditionGroup = isChildrenGroup
    ? 'flex-shrink-0 w-10 w-xl-8'
    : 'flex-shrink-0 w-10 w-lg-8 w-xxl-5';
  const classConditionTextOrButton = isOperator ? ' pt-8' : ' pt-12';

  return (
    <>
      <div className="condition-item">
        {isEmptyOperator && (
          <div className="flex-shrink-0 w-10 w-xl-8 pt-12"></div>
        )}
        {!isEmptyOperator && (
          <div className={classConditionGroup + classConditionTextOrButton}>
            {!isOperator && (
              <p>{t('txt_manage_account_memos_add_condition_where')}</p>
            )}
            {isOperator && addButtonCondition()}
          </div>
        )}
        <div className="row flex-1 mx-4">
          <div className="col-6 col-lg-3">
            <DropdownList
              value={dataCondition?.item?.filterBy}
              label={t('txt_manage_account_memos_filter_by')}
              name="findMemos_filterBy"
              textField="text"
              onChange={e =>
                handleChangeForm(e.target.value, TYPE_VALUE.FILTER_BY)
              }
              onFocus={setTouched('filterBy')}
              onBlur={setTouched('filterBy', true)}
              error={errors?.filterBy}
              required
            >
              {memosTypesFormChronicle?.map(item => (
                <DropdownList.Item
                  key={item?.value}
                  value={item?.value}
                  label={item?.description}
                />
              ))}
            </DropdownList>
          </div>
          <div className="col-6 col-lg-3">
            <DropdownList
              value={dataCondition?.item?.operator}
              label={t('txt_manage_account_memos_operator')}
              name="findMemos_operator"
              textField="text"
              required
              disabled={isEmpty(dataCondition?.item?.filterBy)}
              onChange={e =>
                handleChangeForm(e.target.value, TYPE_VALUE.OPERATOR)
              }
              onFocus={setTouched('operator')}
              onBlur={setTouched('operator', true)}
              error={errors?.operator}
            >
              {valOperatorForm?.map(item => (
                <DropdownList.Item
                  key={item.value}
                  value={item.value}
                  label={item.description}
                />
              ))}
            </DropdownList>
          </div>
          <div className="col-12 col-lg-6 mt-16 mt-lg-0">
            {renderTemplateControl(dataCondition?.item?.filterBy)}
          </div>
        </div>
        <div className="flex-shrink-0 pt-12">
          <Tooltip element="Remove" placement="top" variant="primary">
            <Button
              size="sm"
              variant="icon-danger"
              onClick={handleDeleteCondition}
            >
              <Icon name="close" size="4x" />
            </Button>
          </Tooltip>
        </div>
      </div>
    </>
  );
};

export default ConditionFormSimple;
