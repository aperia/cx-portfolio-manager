import {
  DropdownButton,
  DropdownButtonSelectEvent,
  DropdownList
} from 'app/_libraries/_dls';
import React, { useRef } from 'react';
import { ConfigureParametersFormValue } from '..';
import ConditionForm from './ConditionForm';
import { ACTIONS_CONDITION, TYPE_MEMOS_CONDITION } from './constants';

export interface ConditionProps
  extends WorkflowSetupProps<ConfigureParametersFormValue> {
  handleShow: () => void;
}

const Condition: React.FC<ConditionProps> = props => {
  const { formValues, setFormValues } = props;
  const queryString = formValues?.queryString as MagicKeyValue[];
  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;

  const handleAddCondition = (e: DropdownButtonSelectEvent) => {
    const id = new Date().getTime().toString();
    const val =
      e.target.value === TYPE_MEMOS_CONDITION.CONDITION
        ? {
            id,
            operator: 'And',
            type: 'Condition',
            item: {
              filterBy: '',
              operator: '',
              value: ''
            }
          }
        : {
            id,
            operator: 'And',
            type: 'Group',
            level: '1',
            conditions: [
              {
                id: id + 1,
                operator: '',
                type: 'Condition',
                item: {
                  filterBy: '',
                  operator: '',
                  value: ''
                }
              }
            ]
          };
    const valQuery = formValues?.queryString as MagicKeyValue[];
    keepRef.current.setFormValues({
      ...formValues,
      queryString: [...valQuery, val]
    });
  };

  const addButton = () => (
    <div className="d-flex justify-content-end mt-16 mr-n8">
      <DropdownButton
        onSelect={handleAddCondition}
        buttonProps={{
          children: 'Add',
          variant: 'outline-primary',
          size: 'sm'
        }}
      >
        {ACTIONS_CONDITION.map(item => (
          <DropdownList.Item
            key={item.code}
            label={item.text}
            value={item.code}
          />
        ))}
      </DropdownButton>
    </div>
  );

  return (
    <>
      <div className="condition-content">
        {queryString.map(item => (
          <ConditionForm key={item?.id} {...props} dataCondition={item} />
        ))}
      </div>
      {addButton()}
    </>
  );
};

export default Condition;
