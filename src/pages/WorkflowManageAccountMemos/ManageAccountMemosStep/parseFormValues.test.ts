import { ACTIONS_MEMOS } from '../GettingStartStep/constant';
import parseFormValues from './parseFormValues';

describe('pages > WorkflowManageAccountMemos > ManageAccountMemosStep > parseFormValues', () => {
  it('parseFormValues', () => {
    const configurations: any = {};
    const id = 'id';
    const input: any = {
      data: {
        configurations,
        workflowSetupData: [{ id, status: 'DONE' }]
      }
    };

    // isvalid
    let result = parseFormValues(input, id, jest.fn);
    expect(result?.isEdit).toEqual(false);

    // other step
    result = parseFormValues(input, 'id1', jest.fn);
    expect(result).toBeUndefined();
  });
  it('parseFormValues with custom config', () => {
    const configurations: any = { actionsMemos: ACTIONS_MEMOS.ADD };
    const id = 'id';
    const input: any = {
      data: {
        configurations,
        workflowSetupData: [{ id, status: 'DONE' }]
      }
    };

    // isvalid
    let result = parseFormValues(input, id, jest.fn);
    expect(result?.isEdit).toEqual(false);

    // other step
    result = parseFormValues(input, 'id1', jest.fn);
    expect(result).toBeUndefined();
  });
  it('parseFormValues with custom config', () => {
    const configurations: any = {
      actionsMemos: ACTIONS_MEMOS.FIND,
      queryString: 'queryString'
    };
    const id = 'id';
    const input: any = {
      data: {
        configurations,
        workflowSetupData: [{ id, status: 'DONE' }]
      }
    };

    // isvalid
    let result = parseFormValues(input, id, jest.fn);
    expect(result?.isEdit).toEqual(true);

    // other step
    result = parseFormValues(input, 'id1', jest.fn);
    expect(result).toBeUndefined();
  });

  it('parseFormValues with undefined data', () => {
    const id = 'id';
    const input: any = {
      data: {
        workflowSetupData: [{ id, status: 'DONE' }]
      }
    };

    // isvalid
    let result = parseFormValues(input, id, jest.fn);
    expect(result?.isEdit).toEqual(false);

    // other step
    result = parseFormValues(input, 'id1', jest.fn);
    expect(result).toBeUndefined();
  });
});
