import { getWorkflowSetupStepStatus } from 'app/helpers';
import isEmpty from 'lodash.isempty';
import isEqual from 'lodash.isequal';
import { ConfigureParametersFormValue } from '.';
import { ACTIONS_MEMOS } from '../GettingStartStep/constant';

const parseFormValues: WorkflowSetupStepFormDataFunc<ConfigureParametersFormValue> =
  (data, id) => {
    const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);
    const config = (data?.data as any)?.configurations || {};

    if (!stepInfo || !config) return;

    const validate = () => {
      if (
        config?.actionsMemos === ACTIONS_MEMOS.ADD ||
        (config?.actionsMemos === ACTIONS_MEMOS.FIND &&
          !isEmpty(config?.queryString))
      ) {
        return true;
      }
      return false;
    };

    return {
      ...getWorkflowSetupStepStatus(stepInfo),
      isEdit:
        !isEqual(config?.queryString, [
          {
            id: '0001',
            operator: 'Where',
            type: 'Condition',
            item: {
              filterBy: '',
              operator: '',
              value: ''
            }
          }
        ]) && !isEmpty(config?.queryString),
      parameters: {
        memoText: config?.memoText,
        memos: config?.addMemo,
        type: config?.type
      },
      queryString: config?.queryString,
      isValid: validate()
    };
  };

export default parseFormValues;
