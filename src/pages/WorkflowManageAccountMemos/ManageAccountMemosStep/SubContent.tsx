import { METHOD_FRAUD_FIELDS } from 'app/constants/mapping';
import {
  Button,
  ColumnType,
  Grid,
  Icon,
  Tooltip,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty } from 'lodash';
import {
  actionsWorkflowSetup,
  useSelectElementMetadataBulkFraud
} from 'pages/_commons/redux/WorkflowSetup';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { ConfigureParametersFormValue, IDependencies } from '.';

const SubContent: React.FC<
  WorkflowSetupProps<ConfigureParametersFormValue, IDependencies>
> = ({ stepId, selectedStep }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const [isExpand, setIsExpand] = useState<boolean>(false);

  const metadata = useSelectElementMetadataBulkFraud();

  const { transaction } = metadata;

  useEffect(() => {
    if (!isEmpty(transaction)) return;
    dispatch(actionsWorkflowSetup.getWorkflowMetadata(METHOD_FRAUD_FIELDS));
  }, [dispatch, transaction]);

  useEffect(() => {
    stepId !== selectedStep && setIsExpand(false);
  }, [stepId, selectedStep]);

  const columns: ColumnType[] = [
    {
      id: 'fieldName',
      Header: t('txt_bulk_suspend_fraud_strategy_field_name'),
      width: 211,
      accessor: 'value'
    },
    {
      id: 'value',
      Header: t('txt_bulk_suspend_fraud_strategy_field_type'),
      width: 103,
      accessor: 'type'
    },
    {
      id: 'moreInfo',
      Header: t('txt_bulk_suspend_fraud_strategy_description'),
      accessor: data => {
        return (
          <TruncateText
            resizable
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {data?.description}
          </TruncateText>
        );
      }
    }
  ];

  return (
    <div className="pt-24">
      <div className="d-flex align-items-center">
        <Tooltip
          element={isExpand ? t('txt_collapse') : t('txt_expand')}
          variant="primary"
          placement="top"
          triggerClassName="d-flex ml-n2"
        >
          <Button
            size="sm"
            variant="icon-secondary"
            onClick={() => setIsExpand(!isExpand)}
          >
            <Icon name={isExpand ? 'minus' : 'plus'} size="4x" />
          </Button>
        </Tooltip>
        <p className="fw-600 ml-8">{t('txt_parameter_list')}</p>
      </div>

      {isExpand && (
        <>
          <p className="mt-8 mb-16">
            {t('txt_bulk_suspend_fraud_strategy_upload_title')}
          </p>
          <Grid columns={columns} data={transaction} />
        </>
      )}
      <hr className="my-24" />
    </div>
  );
};

const ExtraStaticSubContentStep = SubContent as WorkflowSetupStaticProp;

ExtraStaticSubContentStep.defaultValues = {
  isValid: true,
  parameters: []
};

export default ExtraStaticSubContentStep;
