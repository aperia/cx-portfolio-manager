import React from 'react';
import { ConditionProps } from './Condition';
import ConditionFormGroup from './ConditionFormGroup';
import ConditionFormSimple from './ConditionFormSimple';
import { TYPE_MEMOS_CONDITION } from './constants';

export interface ConditionFormProps extends ConditionProps {
  dataCondition: MagicKeyValue;
  isChildrenGroup?: boolean;
  idParent?: string;
}

const ConditionForm: React.FC<ConditionFormProps> = props => {
  const { dataCondition } = props;

  if (dataCondition?.type === TYPE_MEMOS_CONDITION.CONDITION) {
    return <ConditionFormSimple {...props} />;
  }

  return <ConditionFormGroup {...props} />;
};

export default ConditionForm;
