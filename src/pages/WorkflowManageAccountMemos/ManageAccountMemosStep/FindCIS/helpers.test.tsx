import { valOperator } from './helpers';
describe('Name of the group', () => {
  it('should Entered ID', () => {
    const val = valOperator('Entered ID', {
      memosEnteredIdOperator: 'Entered ID'
    });

    expect(val).toEqual('Entered ID');
  });

  it('should Sys', () => {
    const val = valOperator('Sys', { memosSysOperator: 'Sys' });

    expect(val).toEqual('Sys');
  });

  it('should Prin', () => {
    const val = valOperator('Prin', { memosPrinOperator: 'Prin' });

    expect(val).toEqual('Prin');
  });

  it('should Agent', () => {
    const val = valOperator('Agent', { memosAgentOperator: 'Agent' });

    expect(val).toEqual('Agent');
  });

  it('should Date', () => {
    const val = valOperator('Date', { memosDateOperator: 'Date' });

    expect(val).toEqual('Date');
  });

  it('should Memo Text', () => {
    const val = valOperator('Memo Text', {
      memosMemoTextOperator: 'Memo Text'
    });

    expect(val).toEqual('Memo Text');
  });

  it('should empty', () => {
    const val = valOperator('');

    expect(val).toEqual([]);
  });
});
