import {
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { useUnsavedChangeRegistry } from 'app/hooks';
import { Icon, Tooltip, useTranslation } from 'app/_libraries/_dls';
import { isEmpty, isEqual } from 'app/_libraries/_dls/lodash';
import React, { useEffect, useRef, useState } from 'react';
import { ConfigureParametersFormValue } from '..';
import Condition from './Condition';
import { TYPE_MEMOS_CONDITION } from './constants';

const FindCIS: React.FC<WorkflowSetupProps<ConfigureParametersFormValue>> =
  props => {
    const { t } = useTranslation();
    const { setFormValues, formValues, savedAt, hasInstance } = props;
    const keepRef = useRef({ setFormValues });
    keepRef.current.setFormValues = setFormValues;

    const [initialValues, setInitialValues] = useState(formValues);

    const [show, setShow] = useState<boolean>(true);

    useEffect(() => {
      setInitialValues(formValues);
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [savedAt]);

    useUnsavedChangeRegistry(
      {
        ...unsavedExistedWorkflowSetupDataProps,
        formName:
          UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_ACCOUNT_FIND_MEMOS,
        priority: 1
      },
      [
        !!hasInstance &&
          !isEqual(formValues?.queryString, initialValues?.queryString)
      ]
    );

    useEffect(() => {
      let isValid = true;
      if (!isEmpty(formValues?.queryString)) {
        const valQueryString = [
          ...(formValues?.queryString as MagicKeyValue[])
        ];
        valQueryString.forEach(query => {
          if (query?.type === 'Group') {
            const arrParent = [...query?.conditions];

            arrParent.forEach(e => {
              if (e?.type === 'Group') {
                e?.conditions?.forEach((condition: MagicKeyValue) => {
                  const isEmtyValue =
                    condition?.item?.filterBy === 'Date'
                      ? condition?.item?.value === ''
                      : isEmpty(condition?.item?.value);
                  if (isEmpty(condition?.item?.filterBy)) {
                    isValid = false;
                    return;
                  }
                  if (isEmpty(condition?.item?.operator)) {
                    isValid = false;
                    return;
                  }
                  if (
                    isEmtyValue &&
                    condition?.item?.filterBy !== 'Memo Text'
                  ) {
                    isValid = false;
                    return;
                  }
                });
              } else {
                const isEmtyValue =
                  e?.item?.filterBy === 'Date'
                    ? e?.item?.value === ''
                    : isEmpty(e?.item?.value);
                if (isEmpty(e?.item?.filterBy)) {
                  isValid = false;
                  return;
                }
                if (isEmpty(e?.item?.operator)) {
                  isValid = false;
                  return;
                }
                if (isEmtyValue && e?.item?.filterBy !== 'Memo Text') {
                  isValid = false;
                  return;
                }
              }
            });
          } else {
            const isEmtyValue =
              query?.item?.filterBy === 'Date'
                ? query?.item?.value === ''
                : isEmpty(query?.item?.value);
            if (isEmpty(query?.item?.filterBy)) {
              isValid = false;
              return;
            }
            if (isEmpty(query?.item?.operator)) {
              isValid = false;
              return;
            }
            if (isEmtyValue && query?.item?.filterBy !== 'Memo Text') {
              isValid = false;
              return;
            }
          }
        });
      }
      keepRef.current.setFormValues({
        isValid
      });
    }, [formValues?.queryString]);

    const handleChangeCondition = (val: string) => {
      setShow(false);
      const queryString =
        val === TYPE_MEMOS_CONDITION.CONDITION
          ? [
              {
                id: '0001',
                operator: 'Where',
                type: 'Condition',
                item: {
                  filterBy: '',
                  operator: '',
                  value: ''
                }
              }
            ]
          : [
              {
                id: '0001',
                operator: 'Where',
                type: 'Group',
                level: '1',
                conditions: [
                  {
                    id: '0002',
                    operator: '',
                    type: 'Condition',
                    item: {
                      filterBy: '',
                      operator: '',
                      value: ''
                    }
                  }
                ]
              }
            ];
      keepRef.current.setFormValues({
        ...formValues,
        queryString
      });
    };

    const handleShow = () => setShow(true);

    if (!formValues?.isEdit && show)
      return (
        <div className="mt-24">
          <h5>{t('txt_manage_account_memos_add_memos_parameter_find')}</h5>
          <div className="row mt-16">
            <div className="col-xl-4 col-6">
              <div
                className="rcc-btn d-flex justify-content-between border rounded-lg py-10"
                onClick={() =>
                  handleChangeCondition(TYPE_MEMOS_CONDITION.CONDITION)
                }
              >
                <span className="d-flex align-items-center ml-16">
                  <Icon name="add-file" size="9x" className="color-grey-l16" />
                  <span className="ml-12 mr-8">
                    {t('txt_manage_account_memos_add_condition')}
                  </span>
                  <Tooltip
                    element={t(
                      'txt_manage_account_memos_add_condition_tooltip'
                    )}
                    triggerClassName="d-flex "
                  >
                    <Icon
                      name="information"
                      className="color-grey-l16"
                      size="5x"
                    />
                  </Tooltip>
                </span>
              </div>
            </div>
            <div className="col-xl-4 col-6">
              <div
                className="rcc-btn d-flex justify-content-between border rounded-lg py-10"
                onClick={() =>
                  handleChangeCondition(TYPE_MEMOS_CONDITION.CONDITION_GROUP)
                }
              >
                <span className="d-flex align-items-center ml-16">
                  <Icon
                    name="knowledge-base"
                    size="9x"
                    className="color-grey-l16"
                  />
                  <span className="ml-12 mr-8">
                    {t('txt_manage_account_memos_add_condition_group')}
                  </span>
                  <Tooltip
                    element={t(
                      'txt_manage_account_memos_add_condition_group_tooltip'
                    )}
                    triggerClassName="d-flex "
                  >
                    <Icon
                      name="information"
                      className="color-grey-l16"
                      size="5x"
                    />
                  </Tooltip>
                </span>
              </div>
            </div>
          </div>
        </div>
      );

    return (
      <div className="mt-24">
        <h5>{t('txt_manage_account_memos_add_memos_parameter_find')}</h5>
        <Condition {...props} handleShow={handleShow} />
      </div>
    );
  };

export default FindCIS;
