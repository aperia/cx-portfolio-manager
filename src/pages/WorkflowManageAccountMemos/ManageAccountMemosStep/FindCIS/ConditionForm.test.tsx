import { render } from '@testing-library/react';
import React from 'react';
import ConditionForm from './ConditionForm';
import { TYPE_MEMOS_CONDITION } from './constants';

jest.mock('./ConditionFormSimple', () => {
  return {
    __esModule: true,
    default: () => <div>ConditionFormSimple</div>
  };
});

jest.mock('./ConditionFormGroup', () => {
  return {
    __esModule: true,
    default: () => <div>ConditionFormGroup</div>
  };
});

const renderComponent = (props: any) => {
  return render(<ConditionForm {...props} />);
};

describe('WorkflowBulkSuspendFraudStrategy > ConfigureParametersStep', () => {
  it('Should render Condition component', () => {
    const wrapper = renderComponent({
      dataCondition: { type: TYPE_MEMOS_CONDITION.CONDITION },
      setFormValues: jest.fn()
    });

    expect(wrapper.getByText('ConditionFormSimple')).toBeInTheDocument;
  });

  it('Should render Condition component', () => {
    const wrapper = renderComponent({
      dataCondition: { type: TYPE_MEMOS_CONDITION.CONDITION_GROUP },
      setFormValues: jest.fn()
    });

    expect(wrapper.getByText('ConditionFormGroup')).toBeInTheDocument;
  });
});
