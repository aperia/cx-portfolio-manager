import { RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { renderWithMockStore } from 'app/utils';
import React from 'react';
import { ConfigureParametersFormValue } from '..';
import { ConditionFormProps } from './ConditionForm';
import ConditionFormGroup from './ConditionFormGroup';

jest.mock('./ConditionForm', () => {
  return {
    __esModule: true,
    default: () => <div>ConditionForm</div>
  };
});

const renderComponent = (props: any): Promise<RenderResult> => {
  return renderWithMockStore(<ConditionFormGroup {...props} />, {});
};

describe('ManageAccountMemosStep > ManageAccountMemosStep > FindCIS > ConditionFormGroup', () => {
  const conditionLv2 = {
    id: 'id2',
    conditions: [{}],
    level: '2'
  };
  const conditionLv1 = {
    id: 'id1',
    operator: 'Where',
    conditions: [conditionLv2],
    level: '1'
  };

  const getProps = ({
    dataCondition,
    queryString = [],
    ...props
  }: Partial<ConditionFormProps & ConfigureParametersFormValue> = {}) => ({
    formValues: { queryString },
    dataCondition,
    ...props
  });

  it('should render condition level 1', async () => {
    const setFormValues = jest.fn();
    const handleShow = jest.fn();
    const { getByText, container } = await renderComponent(
      getProps({
        dataCondition: conditionLv1,
        queryString: [conditionLv1],
        setFormValues,
        handleShow
      })
    );

    expect(getByText('ConditionForm')).toBeInTheDocument();

    userEvent.click(getByText('Add'));
    userEvent.click(getByText('Add Condition'));

    userEvent.click(getByText('Add'));
    userEvent.click(getByText('Add Condition Group'));

    expect(setFormValues).toBeCalled();

    userEvent.click(container.querySelector('.icon.icon-close')!);
    expect(handleShow).toBeCalled();
  });

  it('should render condition level 1 with operator "Where"', async () => {
    const setFormValues = jest.fn();
    const handleShow = jest.fn();
    const { getByText, container } = await renderComponent(
      getProps({
        dataCondition: conditionLv1,
        queryString: [conditionLv1, conditionLv2],
        setFormValues,
        handleShow
      })
    );

    expect(getByText('ConditionForm')).toBeInTheDocument();

    userEvent.click(container.querySelector('.icon.icon-close')!);
    expect(handleShow).not.toBeCalled();
  });

  it('should render condition level 1 with operator "And"', async () => {
    const setFormValues = jest.fn();
    const handleShow = jest.fn();
    const { getByText, container } = await renderComponent(
      getProps({
        dataCondition: { ...conditionLv1, operator: 'And' },
        queryString: [{ ...conditionLv1, operator: 'And' }],
        setFormValues,
        handleShow
      })
    );

    expect(getByText('ConditionForm')).toBeInTheDocument();

    userEvent.click(getByText('And'));
    userEvent.click(getByText('Or'));

    expect(setFormValues).toBeCalled();

    userEvent.click(container.querySelector('.icon.icon-close')!);
    expect(handleShow).not.toBeCalled();
  });

  it('should render condition level 2', async () => {
    const setFormValues = jest.fn();
    const handleShow = jest.fn();
    const { getByText, container } = await renderComponent(
      getProps({
        dataCondition: conditionLv2,
        queryString: [conditionLv1, conditionLv2],
        setFormValues,
        handleShow,
        idParent: 'id1'
      })
    );

    expect(getByText('ConditionForm')).toBeInTheDocument();

    userEvent.click(
      getByText('txt_manage_account_memos_add_condition_text_add_condition')
    );
    expect(setFormValues).toBeCalled();

    userEvent.click(container.querySelector('.icon.icon-close')!);
    expect(handleShow).not.toBeCalled();
  });

  it('should render condition level 2 with operator And', async () => {
    const setFormValues = jest.fn();
    const handleShow = jest.fn();
    const { getByText, container } = await renderComponent(
      getProps({
        dataCondition: { ...conditionLv2, operator: 'And' },
        queryString: [conditionLv1, conditionLv2],
        setFormValues,
        handleShow,
        idParent: 'id1'
      })
    );

    expect(getByText('ConditionForm')).toBeInTheDocument();

    userEvent.click(getByText('And'));
    userEvent.click(getByText('Or'));

    userEvent.click(container.querySelector('.icon.icon-close')!);
    expect(handleShow).not.toBeCalled();
  });

  it('should delete condition level 2 with parent operator = "Where"', async () => {
    const setFormValues = jest.fn();
    const handleShow = jest.fn();
    const { getByText, container } = await renderComponent(
      getProps({
        dataCondition: { ...conditionLv2, operator: 'And' },
        queryString: [conditionLv1],
        setFormValues,
        handleShow,
        idParent: 'id1'
      })
    );

    expect(getByText('ConditionForm')).toBeInTheDocument();

    userEvent.click(getByText('And'));
    userEvent.click(getByText('Or'));

    userEvent.click(container.querySelector('.icon.icon-close')!);
    expect(handleShow).toBeCalled();
  });

  it('should delete condition level 2 with parent operator not "Where"', async () => {
    const setFormValues = jest.fn();
    const handleShow = jest.fn();

    const dataCondition = { ...conditionLv2, operator: 'And' };
    const { getByText, container } = await renderComponent(
      getProps({
        dataCondition,
        queryString: [{ ...conditionLv1, operator: 'And' }, dataCondition],
        setFormValues,
        handleShow,
        idParent: 'id1'
      })
    );

    expect(getByText('ConditionForm')).toBeInTheDocument();

    userEvent.click(container.querySelector('.icon.icon-close')!);
    expect(handleShow).not.toBeCalled();
  });

  it("should delete condition level 2 with multiple parent's conditions", async () => {
    const setFormValues = jest.fn();
    const handleShow = jest.fn();

    const dataCondition = { ...conditionLv2, operator: 'And' };
    const { getByText, container } = await renderComponent(
      getProps({
        dataCondition,
        queryString: [
          { ...conditionLv1, conditions: [dataCondition, dataCondition] },
          dataCondition
        ],
        setFormValues,
        handleShow,
        idParent: 'id1'
      })
    );

    expect(getByText('ConditionForm')).toBeInTheDocument();

    userEvent.click(container.querySelector('.icon.icon-close')!);
    expect(setFormValues).toBeCalled();
  });
});
