import {
  Button,
  DropdownButton,
  DropdownButtonSelectEvent,
  DropdownList,
  Icon,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import { findIndex, isEmpty } from 'lodash';
import React, { useRef } from 'react';
import ConditionForm, { ConditionFormProps } from './ConditionForm';
import {
  ACTIONS_CONDITION,
  TYPE_CONDITION,
  TYPE_MEMOS_CONDITION
} from './constants';

const ConditionFormGroup: React.FC<ConditionFormProps> = props => {
  const { t } = useTranslation();
  const {
    setFormValues,
    dataCondition,
    formValues,
    idParent = '',
    handleShow
  } = props;
  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;

  const isOperator = dataCondition?.operator !== 'Where';
  const isLevel1 = dataCondition?.level === '1';
  const isShowOperator =
    isEmpty(dataCondition?.operator) && dataCondition?.level === '2';

  const handleChangeForm = (val: any) => {
    const queryStringVal = [...(formValues?.queryString as MagicKeyValue[])];
    if (dataCondition?.level === '1') {
      const indexGroup = findIndex(queryStringVal, ['id', dataCondition?.id]);
      let valTemp = { ...dataCondition };
      valTemp = {
        ...valTemp,
        operator: val
      };
      queryStringVal[indexGroup] = valTemp;
      keepRef.current.setFormValues({
        ...formValues,
        queryString: queryStringVal
      });
      return;
    }
    const indexParent = findIndex(queryStringVal, ['id', idParent]);
    let valParent = { ...queryStringVal[indexParent] };

    const conditionsVal = [...valParent?.conditions];
    const indexChildren = findIndex(valParent?.conditions, [
      'id',
      dataCondition?.id
    ]);
    let valTemp = { ...dataCondition };
    valTemp = {
      ...valTemp,
      operator: val
    };
    conditionsVal[indexChildren] = valTemp;
    valParent = {
      ...valParent,
      conditions: conditionsVal
    };
    queryStringVal[indexParent] = valParent;
    keepRef.current.setFormValues({
      ...formValues,
      queryString: queryStringVal
    });
    return;
  };

  const addButtonCondition = () => (
    <DropdownButton
      buttonProps={{
        children: dataCondition?.operator,
        variant: 'outline-secondary',
        size: 'sm',
        className: 'ml-n8'
      }}
      onSelect={e => handleChangeForm(e.target.value)}
    >
      {TYPE_CONDITION.map(item => (
        <DropdownList.Item
          key={item.value}
          label={item.description}
          value={item.description}
        />
      ))}
    </DropdownButton>
  );

  const handleAddCondition = (e: DropdownButtonSelectEvent) => {
    const id = new Date().getTime().toString();
    const val =
      e.target.value === TYPE_MEMOS_CONDITION.CONDITION
        ? {
            id,
            operator: 'And',
            type: 'Condition',
            item: {
              filterBy: '',
              operator: '',
              value: ''
            }
          }
        : {
            id,
            operator: 'And',
            type: 'Group',
            level: '2',
            conditions: [
              {
                id: id + 1,
                operator: '',
                type: 'Condition',
                item: {
                  filterBy: '',
                  operator: '',
                  value: ''
                }
              }
            ]
          };
    const valQuery = [...(formValues?.queryString as MagicKeyValue[])];
    const index = findIndex(valQuery, ['id', dataCondition?.id]);
    valQuery[index] = {
      ...valQuery[index],
      conditions: [...valQuery[index]?.conditions, val]
    };
    keepRef.current.setFormValues({
      ...formValues,
      queryString: [...valQuery]
    });
  };

  const handleAdd = () => {
    const id = new Date().getTime().toString();

    const val = {
      id,
      operator: 'And',
      type: 'Condition',
      item: {
        filterBy: '',
        operator: '',
        value: ''
      }
    };
    const valQuery = [...(formValues?.queryString as MagicKeyValue[])];

    const index = findIndex(valQuery, ['id', idParent]);

    let valParent = { ...valQuery[index] };

    const valConditionParent = [...valParent.conditions];

    const indexChildren = findIndex(valConditionParent, [
      'id',
      dataCondition?.id
    ]);

    let valTempChildren = { ...valConditionParent[indexChildren] };

    valTempChildren = {
      ...valTempChildren,
      conditions: [...valTempChildren.conditions, val]
    };
    valConditionParent[indexChildren] = valTempChildren;

    valParent = {
      ...valParent,
      conditions: valConditionParent
    };

    valQuery[index] = valParent;
    keepRef.current.setFormValues({
      ...formValues,
      queryString: [...valQuery]
    });
  };

  const handleDeleteConditionGroup = () => {
    const queryStringVal = [...(formValues?.queryString as MagicKeyValue[])];
    if (dataCondition?.level === '1') {
      if (dataCondition?.operator === 'Where' && queryStringVal.length === 1) {
        keepRef.current.setFormValues({
          ...formValues,
          queryString: [],
          isEdit: false
        });
        handleShow();
        return;
      }

      if (dataCondition?.operator === 'Where') {
        queryStringVal.splice(0, 1);
        queryStringVal[0] = {
          ...queryStringVal[0],
          operator: 'Where'
        };
        keepRef.current.setFormValues({
          ...formValues,
          queryString: queryStringVal
        });
        return;
      }

      const indexGroup = findIndex(queryStringVal, ['id', dataCondition?.id]);
      queryStringVal.splice(indexGroup, 1);
      keepRef.current.setFormValues({
        ...formValues,
        queryString: queryStringVal
      });
      return;
    }
    const indexParent = findIndex(queryStringVal, ['id', idParent]);
    let valParent = { ...queryStringVal[indexParent] };

    const conditionsVal = [...valParent?.conditions];
    const indexChildren = findIndex(valParent?.conditions, [
      'id',
      dataCondition?.id
    ]);

    conditionsVal.splice(indexChildren, 1);

    if (conditionsVal.length < 1) {
      // empty conditions
      queryStringVal.splice(indexParent, 1);
      if (queryStringVal.length < 1) {
        keepRef.current.setFormValues({
          ...formValues,
          queryString: [],
          isEdit: false
        });
        handleShow();
        return;
      }
      if (valParent?.operator === 'Where') {
        queryStringVal[0] = {
          ...queryStringVal[0],
          operator: 'Where'
        };
        keepRef.current.setFormValues({
          ...formValues,
          queryString: queryStringVal
        });
        return;
      }
      keepRef.current.setFormValues({
        ...formValues,
        queryString: queryStringVal
      });
      return;
    }

    if (indexChildren === 0) {
      conditionsVal[0] = {
        ...conditionsVal[0],
        operator: ''
      };
    }

    valParent = {
      ...valParent,
      conditions: conditionsVal
    };
    queryStringVal[indexParent] = valParent;
    keepRef.current.setFormValues({
      ...formValues,
      queryString: queryStringVal
    });
    return;
  };

  const addButton = () => (
    <div className="d-flex justify-content-end mt-16 mr-n8">
      <DropdownButton
        onSelect={handleAddCondition}
        buttonProps={{
          children: 'Add',
          variant: 'outline-primary',
          size: 'sm'
        }}
      >
        {ACTIONS_CONDITION.map(item => (
          <DropdownList.Item
            key={item.code}
            label={item.text}
            value={item.code}
          />
        ))}
      </DropdownButton>
    </div>
  );

  const classSubCondition = isLevel1
    ? 'condition-sub-item bg-light-l20 p-16 rounded-lg flex-1 mx-16'
    : 'condition-sub-sub-item bg-light-l16 p-16 rounded-lg flex-1 mx-16';
  const classConditionGroup = !isLevel1
    ? 'flex-shrink-0 w-10 w-xl-8 pt-24'
    : 'flex-shrink-0 w-10 w-lg-8 w-xxl-5 pt-24';

  return (
    <>
      <div className="condition-item condition-item-group">
        <div className={classConditionGroup}>
          {!isOperator && isLevel1 && (
            <p>{t('txt_manage_account_memos_add_condition_where')}</p>
          )}
          {isOperator && !isShowOperator && addButtonCondition()}
        </div>
        <div className={classSubCondition}>
          {dataCondition?.conditions?.map((item: MagicKeyValue) => (
            <ConditionForm
              key={item?.id}
              {...props}
              dataCondition={item}
              isChildrenGroup={true}
              idParent={dataCondition?.id}
            />
          ))}
          {isLevel1 && addButton()}
          {!isLevel1 && (
            <div className="d-flex justify-content-end mt-16 mr-n8">
              <Button
                size="sm"
                variant="outline-primary"
                onClick={() => handleAdd()}
              >
                {t('txt_manage_account_memos_add_condition_text_add_condition')}
              </Button>
            </div>
          )}
        </div>
        <div className="flex-shrink-0 pt-28">
          <Tooltip element="Remove" placement="top" variant="primary">
            <Button
              size="sm"
              variant="icon-danger"
              onClick={handleDeleteConditionGroup}
            >
              <Icon name="close" size="4x" />
            </Button>
          </Tooltip>
        </div>
      </div>
    </>
  );
};

export default ConditionFormGroup;
