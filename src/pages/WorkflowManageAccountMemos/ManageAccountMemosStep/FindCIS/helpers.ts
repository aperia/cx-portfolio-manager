export const valOperator = (val: string, metadata: MagicKeyValue) => {
  switch (val) {
    case 'Entered ID':
      return metadata?.memosEnteredIdOperator;
    case 'Sys':
      return metadata?.memosSysOperator;
    case 'Prin':
      return metadata?.memosPrinOperator;
    case 'Agent':
      return metadata?.memosAgentOperator;
    case 'Date':
      return metadata?.memosDateOperator;
    case 'Memo Text':
      return metadata?.memosMemoTextOperator;
    default:
      return [];
  }
};
