import { deepEqual } from 'app/helpers';
import { ConfigureParametersFormValue } from '.';

const isDefaultValues: WorkflowSetupIsDefaultValueFunc<ConfigureParametersFormValue> =
  (formValues, defaultValues) => {
    const ignoreProps = !!formValues?.queryString
      ? []
      : ['queryString', 'parameters'];

    return deepEqual(formValues, defaultValues, ignoreProps);
  };

export default isDefaultValues;
