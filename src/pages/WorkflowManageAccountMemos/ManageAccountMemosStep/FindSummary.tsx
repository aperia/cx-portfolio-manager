import { formatCommon } from 'app/helpers';
import { TruncateText, useTranslation } from 'app/_libraries/_dls';
import React from 'react';
import { TYPE_MEMOS_CONDITION } from './FindCIS/constants';

export interface SummaryFindMemos {
  formValues: MagicKeyValue;
}

const SummaryFindMemos: React.FC<SummaryFindMemos> = ({ formValues }) => {
  const { t } = useTranslation();
  const data = formValues?.queryString as MagicKeyValue[];

  const formatValue = (value: any, type: string) => {
    return type === 'Between' || type === 'Not Between'
      ? `${formatCommon(value?.start).time.date} - ${
          formatCommon(value?.end).time.date
        }`
      : formatCommon(value)?.time.date;
  };

  const contentMemos = data?.map((memo: MagicKeyValue, indexRoot) => {
    if (memo?.type === TYPE_MEMOS_CONDITION.CONDITION_GROUP) {
      const groupConditions = memo?.conditions;

      return (
        <div
          className={`condition-group ${
            indexRoot !== 0 ? 'border-top mt-8 pt-8' : 'mt-16'
          }`}
        >
          <div className="row">
            <div className="form-group-static col maxw-100 pr-8">
              <p
                className={`${
                  groupConditions[0].type ===
                  TYPE_MEMOS_CONDITION.CONDITION_GROUP
                    ? 'pt-36'
                    : 'pt-20'
                }`}
              >
                {memo?.operator}
              </p>
            </div>
            <div className="col pl-0">
              <div className="p-16 bg-light-l20 rounded-lg">
                {groupConditions.map(
                  (memoLevel1: MagicKeyValue, index: number) => {
                    if (
                      memoLevel1?.type === TYPE_MEMOS_CONDITION.CONDITION_GROUP
                    ) {
                      const groupConditionsLevel2 = memoLevel1?.conditions;
                      return (
                        <div
                          className={`group-condition-level1 ${
                            index !== 0 ? 'border-top mt-8 pt-8' : ''
                          }`}
                        >
                          <div className="row">
                            <div className="form-group-static col maxw-100 pr-8">
                              <p className="pt-20">{memoLevel1?.operator}</p>
                            </div>
                            <div className="col pl-0">
                              <div className="p-16 bg-light-l16 rounded-lg">
                                {groupConditionsLevel2.map(
                                  (
                                    memoLevel2: MagicKeyValue,
                                    index1: number
                                  ) => {
                                    const operator1 =
                                      index1 === 0
                                        ? memoLevel1?.operator
                                        : memoLevel2?.operator;
                                    const key1 =
                                      index1 === 0
                                        ? memoLevel1?.id
                                        : memoLevel2?.id;
                                    const value1 =
                                      memoLevel2?.item?.filterBy === 'Date'
                                        ? formatValue(
                                            memoLevel2?.item?.value,
                                            memoLevel2?.item?.operator
                                          )
                                        : memoLevel2?.item?.value;
                                    return (
                                      <div
                                        key={key1}
                                        className={`condition-children2 ${
                                          index1 !== 0
                                            ? 'border-top mt-8 pt-8'
                                            : ''
                                        }`}
                                      >
                                        <div className="row mt-n16">
                                          <div className="form-group-static col maxw-100 pr-8">
                                            {index1 !== 0 && (
                                              <p className="pt-20">
                                                {operator1}
                                              </p>
                                            )}
                                          </div>
                                          <div className="col">
                                            <div className="row">
                                              <div className="form-group-static col">
                                                <div className="form-group-static__label">
                                                  {t(
                                                    'txt_manage_account_memos_filter_by'
                                                  )}
                                                </div>
                                                <div className="form-group-static__text">
                                                  {memoLevel2?.item?.filterBy}
                                                </div>
                                              </div>
                                              <div className="form-group-static col">
                                                <div className="form-group-static__label">
                                                  {t(
                                                    'txt_manage_account_memos_operator'
                                                  )}
                                                </div>
                                                <div className="form-group-static__text">
                                                  {memoLevel2?.item?.operator}
                                                </div>
                                              </div>
                                              <div className="form-group-static col-12 col-xl-6">
                                                <div className="form-group-static__label">
                                                  {t(
                                                    'txt_manage_account_memos_value'
                                                  )}
                                                </div>
                                                <div className="form-group-static__text">
                                                  <TruncateText
                                                    resizable
                                                    lines={2}
                                                    ellipsisLessText={t(
                                                      'txt_less'
                                                    )}
                                                    ellipsisMoreText={t(
                                                      'txt_more'
                                                    )}
                                                  >
                                                    {value1}
                                                  </TruncateText>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    );
                                  }
                                )}
                              </div>
                            </div>
                          </div>
                        </div>
                      );
                    } else {
                      const operator =
                        index === 0 ? memo?.operator : memoLevel1?.operator;
                      const key = index === 0 ? memo?.id : memoLevel1?.id;
                      const value2 =
                        memoLevel1?.item?.filterBy === 'Date'
                          ? formatValue(
                              memoLevel1?.item?.value,
                              memoLevel1?.item?.operator
                            )
                          : memoLevel1?.item?.value;
                      return (
                        <div
                          key={key}
                          className={`condition-children1 ${
                            index !== 0 ? 'border-top mt-8 pt-8' : ''
                          }`}
                        >
                          <div className="row mt-n16">
                            <div className="form-group-static col maxw-100 pr-8 pr-8">
                              {index !== 0 && (
                                <p className="pt-20">{operator}</p>
                              )}
                            </div>
                            <div className="col pl-16">
                              <div className="row">
                                <div className="form-group-static col">
                                  <div className="form-group-static__label">
                                    {t('txt_manage_account_memos_filter_by')}
                                  </div>
                                  <div className="form-group-static__text">
                                    {memoLevel1?.item?.filterBy}
                                  </div>
                                </div>
                                <div className="form-group-static col">
                                  <div className="form-group-static__label">
                                    {t('txt_manage_account_memos_operator')}
                                  </div>
                                  <div className="form-group-static__text">
                                    {memoLevel1?.item?.operator}
                                  </div>
                                </div>
                                <div className="form-group-static col-12 col-xl-6">
                                  <div className="form-group-static__label">
                                    {t('txt_manage_account_memos_value')}
                                  </div>
                                  <div className="form-group-static__text">
                                    <TruncateText
                                      resizable
                                      lines={2}
                                      ellipsisLessText={t('txt_less')}
                                      ellipsisMoreText={t('txt_more')}
                                    >
                                      {value2}
                                    </TruncateText>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      );
                    }
                  }
                )}
              </div>
            </div>
          </div>
        </div>
      );
    }
    const value =
      memo?.item?.filterBy === 'Date'
        ? formatValue(memo?.item?.value, memo?.item?.operator)
        : memo?.item?.value;
    return (
      <div
        key={memo?.id}
        className={`condition ${indexRoot !== 0 ? 'border-top mt-8 pt-8' : ''}`}
      >
        <div className={`row ${indexRoot !== 0 ? 'mt-n16' : ''}`}>
          <div className="form-group-static col maxw-100 pr-8">
            <p className="pt-20">{memo?.operator}</p>
          </div>
          <div className="col pl-16">
            <div className="row">
              <div className="form-group-static col">
                <div className="form-group-static__label">
                  {t('txt_manage_account_memos_filter_by')}
                </div>
                <div className="form-group-static__text">
                  {memo?.item?.filterBy}
                </div>
              </div>
              <div className="form-group-static col">
                <div className="form-group-static__label">
                  {t('txt_manage_account_memos_operator')}
                </div>
                <div className="form-group-static__text">
                  {memo?.item?.operator}
                </div>
              </div>
              <div className="form-group-static col-12 col-xl-6">
                <div className="form-group-static__label">
                  {t('txt_manage_account_memos_value')}
                </div>
                <div className="form-group-static__text">
                  {' '}
                  <TruncateText
                    resizable
                    lines={2}
                    ellipsisLessText={t('txt_less')}
                    ellipsisMoreText={t('txt_more')}
                  >
                    {value}
                  </TruncateText>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  });

  return <>{contentMemos}</>;
};

export default SummaryFindMemos;
