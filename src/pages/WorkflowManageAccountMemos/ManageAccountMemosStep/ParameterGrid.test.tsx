import userEvent from '@testing-library/user-event';
import { WorkflowMetadataOverride } from 'app/fixtures/workflow-metadata';
import { mockUseDispatchFnc, renderWithMockStore } from 'app/utils';
import 'app/utils/_mockComponent/mockModalRegistry';
import 'app/_libraries/_dls/test-utils/mocks/mockCanvas';
import * as CommonRedux from 'pages/_commons/redux/Common/select-hooks';
import React from 'react';
import ParameterGrid from './ParameterGrid';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

jest.mock('app/_libraries/_dls/components/DateRangePicker', () => {
  const actualModule = jest.requireActual(
    'app/_libraries/_dls/components/DateRangePicker'
  );
  return {
    ...actualModule,
    __esModule: true,
    default: ({ onChange }: any) => (
      <div onClick={onChange}>DateRangePicker</div>
    )
  };
});

const mockUseDispatch = mockUseDispatchFnc();

const getInitialState = ({
  elements = WorkflowMetadataOverride
}: any = {}) => ({
  workflowSetup: { elementMetadata: { elements } }
});

const renderComponent = (props: any) => {
  return renderWithMockStore(<ParameterGrid {...props} />, {
    initialState: getInitialState()
  });
};

describe('WorkflowManageAccountMemos > SummaryGrid', () => {
  const mockUseSelectWindowDimension = jest.spyOn(
    CommonRedux,
    'useSelectWindowDimension'
  );
  const mockDispatch = jest.fn();
  const mockUseSelectWindowDimensionImplementation = (width: number) => {
    mockUseSelectWindowDimension.mockImplementation(() => ({ width } as any));
  };

  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
    mockUseSelectWindowDimension.mockClear();
  });

  it('Should render SummaryGrid contains Completed', async () => {
    mockUseSelectWindowDimensionImplementation(1000);
    const wrapper = await renderComponent({
      formValues: {},
      setFormValues: jest.fn()
    });

    expect(wrapper.getByText('txt_manage_account_memos_add_memos_parameter'))
      .toBeInTheDocument;
  });

  it('Should render text box', async () => {
    mockUseSelectWindowDimensionImplementation(1280);
    const wrapper = await renderComponent({
      formValues: {},
      setFormValues: jest.fn(),
      stepId: '2',
      selectedStep: ''
    });

    const textBox = wrapper.getByPlaceholderText('txt_enter_a_value');
    userEvent.type(textBox, '12');
    userEvent.type(textBox, '{enter}');

    expect(wrapper.getByText('txt_manage_account_memos_add_memos_parameter'))
      .toBeInTheDocument;
  });
});
