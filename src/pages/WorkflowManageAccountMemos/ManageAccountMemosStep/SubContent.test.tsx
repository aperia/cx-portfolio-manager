import { fireEvent, render, RenderResult } from '@testing-library/react';
import { mockUseDispatchFnc } from 'app/utils';
import * as workflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowBulkSuspendFraudStrategy';
import React from 'react';
import SubContent from './SubContent';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const mockUseDispatch = mockUseDispatchFnc();

const mockUseSelectElementMetadataBulkFraud = jest.spyOn(
  workflowSetup,
  'useSelectElementMetadataBulkFraud'
);

const renderComponent = (props: any): RenderResult => {
  mockUseSelectElementMetadataBulkFraud.mockReturnValue({
    transaction: [{ text: '', code: '' }]
  });
  return render(
    <div>
      <SubContent {...props} />
    </div>
  );
};

const renderComponent1 = (props: any): RenderResult => {
  mockUseSelectElementMetadataBulkFraud.mockReturnValue({
    transaction: []
  });
  return render(
    <div>
      <SubContent {...props} />
    </div>
  );
};

describe('WorkflowBulkSuspendFraudStrategy > ConfigureParametersStep', () => {
  const mockDispatch = jest.fn();
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
    mockUseSelectElementMetadataBulkFraud.mockReturnValue({} as any);
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
    mockUseSelectElementMetadataBulkFraud.mockReset();
  });
  it('Should render ConfigureParametersStep contains Completed', () => {
    const wrapper = renderComponent({
      formValues: {}
    });

    const buttonEx = wrapper.baseElement.querySelector(
      'button[class="btn btn-icon-secondary btn-sm"]'
    ) as Element;
    fireEvent.click(buttonEx);

    expect(wrapper.getByText('txt_parameter_list')).toBeInTheDocument;
  });

  it('Should render', () => {
    const wrapper = renderComponent1({
      formValues: {}
    });

    expect(wrapper.getByText('txt_parameter_list')).toBeInTheDocument;
  });
});
