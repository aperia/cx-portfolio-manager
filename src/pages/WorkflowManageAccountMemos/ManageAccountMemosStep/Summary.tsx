import { Button, useTranslation } from 'app/_libraries/_dls';
import { isFunction } from 'app/_libraries/_dls/lodash';
import React from 'react';
import { ConfigureParametersFormValue, IDependencies } from '.';
import { ACTIONS_MEMOS } from '../GettingStartStep/constant';
import SummaryFindMemos from './FindSummary';
import SummaryGrid from './SummaryGrid';

const Summary: React.FC<
  WorkflowSetupSummaryProps<ConfigureParametersFormValue, IDependencies>
> = props => {
  const { t } = useTranslation();

  const { onEditStep, dependencies, formValues } = props;

  const handleEdit = () => {
    isFunction(onEditStep) && onEditStep();
  };

  return (
    <>
      <div className="position-relative">
        <div className="absolute-top-right mt-n26 mr-n8">
          <Button variant="outline-primary" size="sm" onClick={handleEdit}>
            {t('txt_edit')}
          </Button>
        </div>
      </div>
      <div className="summary-content">
        {dependencies?.actionsMemos === ACTIONS_MEMOS.FIND && (
          <SummaryFindMemos formValues={formValues} />
        )}
        {dependencies?.actionsMemos === ACTIONS_MEMOS.ADD && (
          <div className="pt-16">
            <SummaryGrid {...props} />
          </div>
        )}
      </div>
    </>
  );
};

export default Summary;
