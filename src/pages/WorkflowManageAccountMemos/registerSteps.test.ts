import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { uploadFileStepRegistry } from 'pages/_commons/WorkflowSetup/registries';
import { default as stepRegistry } from '../_commons/WorkflowSetup/registries/stepRegistry';

const registerFunc = jest.spyOn(stepRegistry, 'registerStep');
const registerCom = jest.spyOn(uploadFileStepRegistry, 'registerComponent');

describe('CustomDataLinkElements > registerSteps', () => {
  it('Should register components', async () => {
    await import('./registerSteps');

    expect(registerFunc).toBeCalledWith(
      'GetStartedManageAccountMemos',
      expect.anything(),
      [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_ACCOUNT_MEMOS__GET_STARTED__EXISTED_WORKFLOW
      ]
    );

    expect(registerCom).toBeCalledWith(
      'SubContentManageAccountMemos',
      expect.anything()
    );

    expect(registerFunc).toBeCalledWith(
      'ConfigureParametersManageAccountMemos',
      expect.anything(),
      [UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_ACCOUNT_MEMOS_CONFIRM_EDIT]
    );
  });
});
