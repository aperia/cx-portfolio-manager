import { FilterOption } from '../type';

export const methodFilterValues = {
  all: 'all',
  selected: 'selected',
  unSelected: 'Unselected'
};

export const methodFilterOptions = (t: any): FilterOption[] => {
  return [
    {
      value: methodFilterValues.all,
      text: t('txt_all')
    },
    {
      value: methodFilterValues.selected,
      text: t('txt_export_impor_change_details__selected_changes')
    },
    {
      value: methodFilterValues.unSelected,
      text: t('txt_export_impor_change_details__unselected_changes')
    }
  ];
};
