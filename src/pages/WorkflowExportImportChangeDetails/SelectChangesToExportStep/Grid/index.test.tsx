import { queryByText, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { mockThunkAction, renderWithMockStore } from 'app/utils';
import 'app/utils/_mockComponent/mockNoDataFound';
import { STATUS } from 'app/_libraries/_dls/components/Upload/helper';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import React from 'react';
import { act } from 'react-dom/test-utils';
import Grid from '.';

const setFormValues = jest.fn();
const clearFormValues = jest.fn();

const mockWorkflowSetup = mockThunkAction(actionsWorkflowSetup);

const defaultState = {
  initialState: {
    workflowSetup: {
      workflowFormSteps: {
        forms: {
          changeInformation: {
            changeSetId: '1111',
            workflowInstanceId: '2222',
            name: 'mock name',
            description: 'mock description',
            workflowNote: 'mock note',
            effectiveDate: new Date()
          }
        }
      }
    }
  }
};

const t = (value: string) => value;
const useTranslation = () => ({ t });
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { __esModule: true, ...actualModule, useTranslation };
});

jest.mock('app/hooks/useUnsavedChangeRegistry', () => {
  return {
    __esModule: true,
    useUnsavedChangeRegistry: (options: any, changes: any, onConfirm: any) => {
      onConfirm && onConfirm();
    }
  };
});

const getValueDropdownList = (wrapper: HTMLElement, value: string) => {
  const dropdownListElement =
    wrapper.container.querySelector('.dls-dropdown-list');
  const iconElement = dropdownListElement?.querySelector('.icon');
  userEvent.click(iconElement!);

  const popupElement = wrapper.baseElement.querySelector('.dls-popup');
  expect(popupElement).toBeInTheDocument();

  const item = queryByText(popupElement as HTMLElement, value);
  userEvent.click(item!);
};

describe('WorkflowExportImportChangeDetails > SelectTransactionsStep > Grid', () => {
  it('render with loading', async () => {
    mockWorkflowSetup('getWorkflowChangeList', {
      match: true,
      payload: { payload: [] }
    });
    const props = {
      stepId: 'id',
      selectedStep: 'id1',
      savedAt: 1,
      formValues: { isValid: true },
      dependencies: {
        files: [
          {
            status: STATUS.valid
          }
        ]
      },
      setFormValues,
      clearFormValues
    } as any;
    await renderWithMockStore(<Grid {...props} />, defaultState);
  });

  it('render with no data', async () => {
    mockWorkflowSetup('getWorkflowChangeList', {
      match: true,
      payload: { payload: [] }
    });

    const props = {
      stepId: 'id',
      selectedStep: 'id',
      savedAt: 1,
      formValues: { isValid: true },

      dependencies: {},
      setFormValues
    } as any;

    jest.useFakeTimers();
    const wrapper = await renderWithMockStore(
      <Grid isEdit {...props} />,
      defaultState
    );
    jest.runAllTimers();
    expect(wrapper.getByText('No data found')).toBeInTheDocument();

    act(() => {
      userEvent.click(wrapper.getByText('No data found'));
    });
  });

  describe('with data', () => {
    it('render', async () => {
      mockWorkflowSetup('getWorkflowChangeList', {
        match: true,
        payload: {
          payload: [
            {
              name: 'name',
              id: 1,
              changeId: '123',
              changeName: 'changeName',
              createdDate: '2022-03-22'
            }
          ]
        }
      });

      const props = {
        stepId: 'id',
        selectedStep: 'id',
        savedAt: 1,
        formValues: { isValid: true },

        dependencies: { attachmentId: 'id' },
        setFormValues
      } as any;

      jest.useFakeTimers();
      const wrapper = await renderWithMockStore(
        <Grid isEdit {...props} />,
        defaultState
      );
      jest.runAllTimers();

      const searchBox = wrapper.getByPlaceholderText('txt_type_to_search');

      userEvent.type(searchBox, 'test');
      userEvent.click(
        searchBox.parentElement!.querySelector(
          '.icon-simple-search .icon.icon-search'
        )!
      );
      expect(
        wrapper.queryByText(
          'txt_export_impor_change_details__number_changes_selected'
        )
      ).not.toBeInTheDocument();
    });

    it('check all', async () => {
      mockWorkflowSetup('getWorkflowChangeList', {
        match: true,
        payload: {
          payload: [
            {
              name: 'name',
              id: 1,
              changeId: '123',
              changeName: 'changeName',
              createdDate: '2022-03-22'
            }
          ]
        }
      });
      const props = {
        stepId: 'id',
        selectedStep: 'id',
        savedAt: 1,
        formValues: { isValid: true },

        dependencies: { attachmentId: 'id' },
        setFormValues
      } as any;

      jest.useFakeTimers();
      const wrapper = await renderWithMockStore(
        <Grid isEdit {...props} />,
        defaultState
      );
      jest.runAllTimers();

      const checkboxAll = screen.getAllByRole('checkbox')[0];
      act(() => {
        userEvent.click(checkboxAll);
      });
      getValueDropdownList(
        wrapper,
        'txt_export_impor_change_details__unselected_changes'
      );
    });

    it('check one row', async () => {
      mockWorkflowSetup('getWorkflowChangeList', {
        match: true,
        payload: {
          payload: [
            {
              name: 'name',
              id: 1,
              changeId: '123',
              changeName: 'changeName',
              createdDate: '2022-03-22'
            }
          ]
        }
      });
      const props = {
        stepId: 'id',
        selectedStep: 'id',
        savedAt: 1,
        formValues: { isValid: false },

        dependencies: {},
        setFormValues
      } as any;

      jest.useFakeTimers();
      const wrapper = await renderWithMockStore(
        <Grid isEdit {...props} />,
        defaultState
      );
      jest.runAllTimers();

      const checkbox = wrapper.getAllByRole('checkbox')[1];
      act(() => {
        userEvent.click(checkbox);
      });
      userEvent.click(
        wrapper.getAllByText('txt_export_impor_change_details__view_details')[0]
      );
      expect(
        wrapper.getByText(
          `txt_export_impor_change_details__number_changes_selected`
        )
      ).toBeInTheDocument();
    });

    it('with formValue changeLists', async () => {
      mockWorkflowSetup('getWorkflowChangeList', {
        match: true,
        payload: {
          payload: [
            {
              name: 'name',
              id: '1',
              changeId: '123',
              changeName: 'changeName',
              createdDate: '2022-03-22'
            },
            {
              name: 'name',
              id: '2',
              changeId: '123',
              changeName: 'changeName',
              createdDate: '2022-03-22'
            },
            {
              name: 'name',
              id: '3',
              changeId: '123',
              changeName: 'changeName',
              createdDate: '2022-03-22'
            },
            {
              name: 'name',
              id: '4',
              changeId: '123',
              changeName: 'changeName',
              createdDate: '2022-03-22'
            }
          ]
        }
      });
      const props = {
        stepId: 'id',
        selectedStep: 'id',
        savedAt: 1,
        formValues: { isValid: true, changeLists: ['1', '2'] },

        dependencies: {},
        setFormValues
      } as any;

      jest.useFakeTimers();

      const wrapper = await renderWithMockStore(
        <Grid isEdit {...props} />,
        defaultState
      );
      jest.runAllTimers();
      act(() => {
        userEvent.click(wrapper.getAllByRole('checkbox')[1]);
        userEvent.click(wrapper.getAllByRole('checkbox')[2]);
      });
      getValueDropdownList(
        wrapper,
        'txt_export_impor_change_details__selected_changes'
      );
      getValueDropdownList(
        wrapper,
        'txt_export_impor_change_details__unselected_changes'
      );
      getValueDropdownList(wrapper, 'txt_all');
      act(() => {
        userEvent.click(wrapper.getAllByRole('checkbox')[3]);
        userEvent.click(wrapper.getAllByRole('checkbox')[4]);
      });

      wrapper.rerender(
        <Grid
          isEdit
          {...props}
          formValues={{ isValid: true, changeLists: ['1', '2'] }}
        />
      );
      act(() => {
        userEvent.click(wrapper.getAllByRole('checkbox')[1]);
        userEvent.click(wrapper.getAllByRole('checkbox')[2]);
      });

      wrapper.rerender(
        <Grid
          isEdit
          {...props}
          formValues={{ isValid: true, changeLists: ['1', '2'] }}
        />
      );
    });
  });
});
