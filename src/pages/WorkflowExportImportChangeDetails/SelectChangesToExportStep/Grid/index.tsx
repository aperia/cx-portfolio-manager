import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import { PAGE_SIZE } from 'app/constants/constants';
import { changeDetailLinkFromImportExportWF } from 'app/constants/links';
import {
  confirmEditWhenChangeEffectToUploadProps,
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { classnames } from 'app/helpers';
import { compare2Array } from 'app/helpers/array';
import { useCheckAllPagination, useUnsavedChangeRegistry } from 'app/hooks';
import { usePagination } from 'app/hooks/usePagination';
import {
  Button,
  DropdownBaseChangeEvent,
  DropdownList,
  Grid,
  InlineMessage,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import { STATUS } from 'app/_libraries/_dls/components/Upload/helper';
import { differenceWith, isEmpty } from 'app/_libraries/_dls/lodash';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import { formatBubble, formatText } from 'pages/_commons/Utils/formatGridField';
import Paging from 'pages/_commons/Utils/Paging';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';
import { SelectChangesToExportStepProps } from '..';
import { FilterOption } from '../type';
import { methodFilterOptions } from './constant';

const GridStep: React.FC<SelectChangesToExportStepProps> = props => {
  const {
    stepId,
    selectedStep,
    savedAt,
    isStuck,
    formValues,
    dependencies,
    setFormValues,
    clearFormValues,
    clearFormName
  } = props;

  const setFormValuesRef = useRef(setFormValues);
  setFormValuesRef.current = setFormValues;

  const dispatch = useDispatch();
  const { t } = useTranslation();

  const initLists = useCallback(async () => {
    setFormValuesRef.current({
      loading: true
    });

    const response = (await Promise.resolve(
      dispatch(actionsWorkflowSetup.getWorkflowChangeList({}))
    )) as any;

    const data = response?.payload || [];
    setChangeList(data);
    setTotalChangeList(data);

    setFormValuesRef.current({
      loading: false
    });
  }, [dispatch]);

  useEffect(() => {
    initLists();
  }, [initLists]);

  const [changeList, setChangeList] = useState([]);
  const [totalChangeList, setTotalChangeList] = useState([]);
  const [initialValues, setInitialValues] = useState(formValues?.changeLists);
  const methodOptions = methodFilterOptions(t);

  const [filterValue, setFilterValue] = useState<string>(t('txt_all'));
  const isDefaultFilter = filterValue === t('txt_all');

  const {
    sort,
    total,
    currentPage,
    currentPageSize,
    searchValue,
    gridData: dataView,
    dataChecked,
    isCheckAll,
    onSetDataChecked,
    onCheckInPage,
    onSetIsCheckAll,
    onSortChange,
    onPageChange,
    onPageSizeChange,
    onSearchChange,
    onResetToDefaultFilter
  } = usePagination(changeList, ['name', 'changeId', 'changeName'], 'name');

  const { gridClassName, handleClickCheckAll } = useCheckAllPagination(
    {
      gridClassName: 'export-import-change-details',
      allIds: changeList.map((x: any) => x.id),
      hasFilter: !!searchValue || !isDefaultFilter,
      filterIds: changeList.map((x: any) => x.id),
      checkedList: dataChecked,
      setCheckAll: onSetIsCheckAll,
      setCheckList: onSetDataChecked
    },
    [searchValue, sort, currentPageSize, currentPageSize]
  );

  useEffect(() => {
    if (selectedStep === stepId) {
      onResetToDefaultFilter();
      setFilterValue(t('txt_all'));
    }
  }, [selectedStep, stepId, onResetToDefaultFilter, t]);

  const onViewDetail = useCallback((id: string) => {
    window.open(changeDetailLinkFromImportExportWF(id));
  }, []);

  const columns = useMemo(
    () => [
      {
        id: 'name',
        Header: t('txt_export_impor_change_details__column_change_name'),
        accessor: 'name',
        isSort: true,
        width: 336
      },
      {
        id: 'changeId',
        Header: t('txt_export_impor_change_details__column_change_id'),
        accessor: 'id',
        isSort: true,
        width: 120
      },
      {
        id: 'createdDate',
        Header: t('txt_export_impor_change_details__column_created_date'),
        accessor: formatText(['createdDate'], { format: 'date' }),
        isSort: true,
        width: 120
      },
      {
        id: 'changeName',
        Header: t('txt_export_impor_change_details__column_change_owner'),
        accessor: formatBubble(['changeName']),
        isSort: true,
        width: 155
      },
      {
        id: 'action',
        Header: t('txt_action'),
        accessor: (data: any) => (
          <div className="d-flex justify-content-center">
            <Button
              size="sm"
              variant="outline-primary"
              onClick={() => onViewDetail(data.id)}
            >
              {t('txt_export_impor_change_details__view_details')}
            </Button>
          </div>
        ),
        className: 'text-center',
        cellBodyProps: { className: 'py-8' },
        width: 100
      }
    ],
    [t, onViewDetail]
  );

  const handleClearAndReset = useCallback(() => {
    onPageSizeChange(PAGE_SIZE[0]);
    onPageChange(1);
    setFilterValue(t('txt_all'));
    onSearchChange('');
  }, [t, onPageChange, onPageSizeChange, onSearchChange]);

  const handleCheck = (dataKeyList: string[]) => {
    onCheckInPage(dataKeyList);
  };

  useEffect(() => {
    if (isEmpty(formValues.changeLists)) return;

    onSetDataChecked(formValues.changeLists);
  }, [formValues.changeLists, onSetDataChecked]);

  useEffect(() => {
    setFormValuesRef.current({
      changeLists: dataChecked,
      changeListsData: totalChangeList.filter((item: any) =>
        dataChecked.includes(item.id)
      )
    });
  }, [dataChecked, formValues.changeLists, totalChangeList]);

  const hasChange = !(
    isEmpty(differenceWith(initialValues, dataChecked)) &&
    isEmpty(differenceWith(dataChecked, initialValues!))
  );
  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CONDITION_M_CYCLE_ACCOUNTS_FOR_TESTING,
      priority: 1
    },
    [hasChange]
  );

  const unsaveOptions = useMemo(
    () => ({
      ...confirmEditWhenChangeEffectToUploadProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CONDITION_M_CYCLE_ACCOUNTS_FOR_TESTING_CONFIRM_EDIT,
      priority: 1
    }),
    []
  );

  const handleResetUploadStep = () => {
    const file = dependencies?.files?.[0];
    const attachmentId = file?.idx;
    const isValid = file?.status === STATUS.valid;

    typeof clearFormValues === 'function' && clearFormValues(clearFormName);
    isValid &&
      dispatch(actionsWorkflowSetup.deleteTemplateFile({ attachmentId }));
  };
  useUnsavedChangeRegistry(
    unsaveOptions,
    [
      !isEmpty(dependencies?.files) &&
        !compare2Array(initialValues, dataChecked)
    ],
    handleResetUploadStep
  );

  const changeListsSelected = useMemo(() => {
    const listNumber = dataChecked?.length;
    const content =
      dataView.length === 0 ||
      filterValue === t('txt_export_impor_change_details__unselected_changes')
        ? ''
        : isCheckAll
        ? t('txt_export_impor_change_details__all_changes_selected')
        : listNumber !== 1
        ? t('txt_export_impor_change_details__number_changes_selected', {
            listNumber
          })
        : t('txt_export_impor_change_details__number_change_selected', {
            listNumber
          });

    return (
      content !== '' && (
        <TruncateText title={content} resizable>
          {content}
        </TruncateText>
      )
    );
  }, [dataChecked, t, isCheckAll, dataView.length, filterValue]);

  const handleFilter = (e: DropdownBaseChangeEvent) => {
    setFilterValue(e.target?.value);
  };

  useMemo(() => {
    switch (filterValue) {
      case t('txt_export_impor_change_details__selected_changes'):
        setChangeList(
          totalChangeList.filter((x: any) => dataChecked.includes(x.id))
        );
        break;

      case t('txt_export_impor_change_details__unselected_changes'):
        setChangeList(
          totalChangeList.filter((x: any) => !dataChecked.includes(x.id))
        );
        break;

      case t('txt_all'):
        setChangeList(totalChangeList);
        break;
    }
  }, [t, filterValue, dataChecked, totalChangeList]);

  useEffect(() => {
    isCheckAll &&
      filterValue ===
        t('txt_export_impor_change_details__unselected_changes') &&
      onSetDataChecked(totalChangeList.map((el: any) => el.id));
  }, [isCheckAll, filterValue, t, onSetDataChecked, totalChangeList]);

  const viewNoData = useMemo(() => {
    const hasData = totalChangeList.length > 0;

    return (
      <div className={classnames(formValues?.loading && 'loading')}>
        <div className="mt-24">
          <div className="d-flex align-items-center">
            <h5>{t('txt_export_impor_change_details__change_list')}</h5>
            {hasData && (
              <div className="ml-auto">
                <div className="ml-auto">
                  <SimpleSearch
                    defaultValue={searchValue}
                    onSearch={onSearchChange}
                    placeholder={t('txt_type_to_search')}
                    popperElement={
                      <>
                        <p className="color-grey">
                          {t('txt_search_description')}
                        </p>
                        <ul className="search-field-item list-unstyled">
                          <li className="mt-16">Change ID</li>
                          <li className="mt-16">Change Name</li>
                          <li className="mt-16">Change Owner</li>
                        </ul>
                      </>
                    }
                  />
                </div>
              </div>
            )}
          </div>
        </div>
        <div className="d-flex align-items-center pr-24 mt-12">
          <strong className="fs-14 color-grey mr-4">{t('txt_filter')}:</strong>
          <DropdownList
            name="status"
            value={filterValue}
            textField="description"
            onChange={handleFilter}
            variant="no-border"
          >
            {methodOptions.map((item: FilterOption) => (
              <DropdownList.Item
                key={item.value}
                label={item.text}
                value={item.text}
              />
            ))}
          </DropdownList>
        </div>
        {!searchValue && <div className="mt-16">{changeListsSelected}</div>}

        <div className="d-flex flex-column justify-content-center mt-40 mb-32">
          {!formValues?.loading && (
            <NoDataFound
              hasSearch={!!searchValue}
              id="search-workflow-condition-m-cycle-not-found"
              title={t(
                hasData
                  ? 'txt_no_changeLists_to_display'
                  : 'txt_no_changes_to_display'
              )}
              linkTitle={(!!searchValue || hasData) && t('txt_clear_and_reset')}
              onLinkClicked={handleClearAndReset}
            />
          )}
        </div>
      </div>
    );
  }, [
    handleClearAndReset,
    onSearchChange,
    searchValue,
    totalChangeList.length,
    t,
    changeListsSelected,
    filterValue,
    methodOptions,
    formValues?.loading
  ]);

  // Bind formValue changeLists to checkedList
  useEffect(() => {
    const newData = changeList.filter((item: any) =>
      formValues.changeLists?.includes(item.id)
    );
    const newCheckedList = newData.map((item: any) => item.id);

    if (differenceWith(newCheckedList, dataChecked).length === 0) return;

    setFormValuesRef.current({ changeListsData: newData });
    onSetDataChecked(newCheckedList);
    onSetIsCheckAll(formValues.changeLists?.length === changeList.length);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [formValues.changeLists]);

  // update isValid
  useEffect(() => {
    const newIsValid = (formValues.changeLists?.length || 0) > 0;
    if (newIsValid === formValues.isValid) return;
    setFormValuesRef.current({
      isValid: newIsValid,
      listLength: changeList.length
    });
  }, [changeList.length, formValues]);

  // set initialValues
  useEffect(() => {
    setInitialValues(formValues.changeLists);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [savedAt]);

  if (dataView.length === 0) return viewNoData;

  return (
    <div className="mt-24">
      {isStuck && (
        <InlineMessage className="mb-24" variant="danger" withIcon>
          {t('txt_step_stuck_move_forward_message')}
        </InlineMessage>
      )}
      <div className="d-flex align-items-center">
        <h5>{t('txt_export_impor_change_details__change_list')}</h5>
        {(!isEmpty(searchValue) || total > 0) && (
          <div className="ml-auto">
            <SimpleSearch
              defaultValue={searchValue}
              onSearch={onSearchChange}
              placeholder={t('txt_type_to_search')}
              popperElement={
                <>
                  <p className="color-grey">{t('txt_search_description')}</p>
                  <ul className="search-field-item list-unstyled">
                    <li className="mt-16">Change ID</li>
                    <li className="mt-16">Change Name</li>
                    <li className="mt-16">Change Owner</li>
                  </ul>
                </>
              }
            />
          </div>
        )}
      </div>
      <div className="d-flex align-items-center pr-24 mt-12">
        <strong className="fs-14 color-grey mr-4">{t('txt_filter')}:</strong>
        <DropdownList
          name="status"
          value={filterValue}
          textField="description"
          onChange={handleFilter}
          variant="no-border"
        >
          {methodOptions.map((item: FilterOption) => (
            <DropdownList.Item
              key={item.value}
              label={item.text}
              value={item.text}
            />
          ))}
        </DropdownList>
      </div>
      <div className="d-flex align-items-center mb-16">
        {!!(!searchValue && changeListsSelected) && (
          <div className="flex-1 py-4 mt-12">{changeListsSelected}</div>
        )}
        {!!searchValue && (
          <div className="ml-auto mr-n8">
            <Button
              size="sm"
              variant="outline-primary"
              onClick={handleClearAndReset}
            >
              {t('txt_clear_and_reset')}
            </Button>
          </div>
        )}
      </div>
      <Grid
        className={gridClassName}
        data={dataView}
        columns={columns}
        checkedList={dataChecked}
        onSortChange={onSortChange}
        sortBy={[sort]}
        rowKey="id"
        dataItemKey="id"
        variant={{
          id: 'id',
          type: 'checkbox',
          cellHeaderProps: { onClick: handleClickCheckAll }
        }}
        onCheck={handleCheck}
      />
      {total > 10 && (
        <div className="mt-16">
          <Paging
            page={currentPage}
            pageSize={currentPageSize}
            totalItem={total}
            onChangePage={onPageChange}
            onChangePageSize={onPageSizeChange}
          />
        </div>
      )}
    </div>
  );
};

export default GridStep;
