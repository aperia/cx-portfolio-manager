import { StateFile } from 'app/_libraries/_dls/components/Upload/File';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React from 'react';
import Grid from './Grid';
import parseFormValues from './parseFormValues';
import stepValueFunc from './stepValueFunc';
import Summary from './Summary';

export interface Form {
  isValid?: boolean;
  changeLists?: string[];
  changeListsData?: MagicKeyValue[];
  listLength?: number;
  loading?: boolean;
}

export interface IDependencies {
  files?: StateFile[];
}
export interface SelectChangesToExportStepProps
  extends WorkflowSetupProps<Form, IDependencies> {
  clearFormName: string;
}

const SelectNonMonetaryTransactionsStep: React.FC<SelectChangesToExportStepProps> =
  props => {
    return <Grid {...props} />;
  };

const ExtraStaticConfigureParameters =
  SelectNonMonetaryTransactionsStep as WorkflowSetupStaticProp<
    Form,
    IDependencies
  >;
ExtraStaticConfigureParameters.summaryComponent = Summary;
ExtraStaticConfigureParameters.parseFormValues = parseFormValues;
ExtraStaticConfigureParameters.stepValue = stepValueFunc as any;

ExtraStaticConfigureParameters.defaultValues = {
  loading: false,
  isValid: false,
  changeLists: [],
  changeListsData: []
};

export default ExtraStaticConfigureParameters;
