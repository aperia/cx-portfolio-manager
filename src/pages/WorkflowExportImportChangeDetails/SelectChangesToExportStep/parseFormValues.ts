import { getWorkflowSetupStepStatus } from 'app/helpers';
import { isEmpty } from 'lodash';

const parseFormValues: WorkflowSetupStepFormDataFunc<any> = (data, id) => {
  const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);

  const { changeLists, changeListsData } =
    (data?.data as any)?.configurations || {};
  if (isEmpty(changeLists) || isEmpty(changeListsData) || !stepInfo) return;

  const isValid = !isEmpty(changeLists) && !isEmpty(changeListsData);

  return {
    ...getWorkflowSetupStepStatus(stepInfo),
    isValid,
    changeLists,
    changeListsData
  };
};

export default parseFormValues;
