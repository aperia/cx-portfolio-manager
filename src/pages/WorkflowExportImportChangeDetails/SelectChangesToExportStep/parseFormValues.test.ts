import parseFormValues from './parseFormValues';

describe('WorkflowConditionMCycleAccountsForTesting > SelectTransactionsStep > parseFormValues', () => {
  const dispatchMock = jest.fn();
  const data = {
    changeLists: ['1', '2'],
    changeListsData: [
      {
        name: 'August Release',
        id: 838363039,
        changeId: 583729637,
        changeName: 'Jesse Dunn',
        createdDate: '2022-04-26T15:31:02.4536604+07:00'
      },
      {
        name: 'August Release',
        id: 1004637621,
        changeId: 4730039,
        changeName: 'Luke Skywalker',
        createdDate: '2022-04-27T15:31:02.456671+07:00'
      }
    ]
  };
  const input: any = {
    data: {
      configurations: data,
      methodList: [],
      workflowSetupData: [
        {
          id: 'id',
          status: 'DONE'
        }
      ]
    }
  };

  it('selected step', () => {
    const id = 'id';
    const result = parseFormValues(input, id, dispatchMock);

    expect(result.changeLists).toEqual(data.changeLists);
    expect(result.changeListsData).toEqual(data.changeListsData);
  });

  it('select other step', () => {
    const id = 'id1';
    const result = parseFormValues(input, id, dispatchMock);

    expect(result).toBeUndefined();
  });

  it('empty config', () => {
    const id = 'id';
    const result = parseFormValues({} as any, id, dispatchMock);

    expect(result).toBeUndefined();
  });
});
