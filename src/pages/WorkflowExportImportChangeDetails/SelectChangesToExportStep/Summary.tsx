import { classnames } from 'app/helpers';
import { usePagination } from 'app/hooks/usePagination';
import { Button, ColumnType, Grid, useTranslation } from 'app/_libraries/_dls';
import { isFunction, orderBy } from 'app/_libraries/_dls/lodash';
import { formatBubble, formatText } from 'pages/_commons/Utils/formatGridField';
import Paging from 'pages/_commons/Utils/Paging';
import React, { useEffect, useMemo } from 'react';
import { Form } from '.';

const Summary: React.FC<WorkflowSetupSummaryProps<Form>> = props => {
  const { onEditStep, formValues, stepId, selectedStep } = props;
  const { t } = useTranslation();

  const handleEdit = () => {
    isFunction(onEditStep) && onEditStep();
  };

  const columnDefault: ColumnType[] = useMemo(
    () => [
      {
        id: 'name',
        Header: t('txt_export_impor_change_details__column_change_name'),
        accessor: 'name',
        isSort: false,
        width: 336
      },
      {
        id: 'changeId',
        Header: t('txt_export_impor_change_details__column_change_id'),
        accessor: 'changeId',
        isSort: false,
        width: 120
      },
      {
        id: 'createdDate',
        Header: t('txt_export_impor_change_details__column_created_date'),
        accessor: formatText(['createdDate'], { format: 'date' }),
        isSort: false,
        width: 120
      },
      {
        id: 'changeName',
        Header: t('txt_export_impor_change_details__column_change_owner'),
        accessor: formatBubble(['changeName']),
        isSort: false,
        width: 155
      }
    ],
    [t]
  );

  const selectedData = useMemo(() => {
    return orderBy(formValues?.changeListsData || [], 'name', 'asc');
  }, [formValues?.changeListsData]);

  const resetFilter = useMemo(
    () => stepId === selectedStep,
    [stepId, selectedStep]
  );

  const {
    total,
    currentPage,
    currentPageSize,
    gridData: dataView,
    onPageSizeChange,
    onPageChange,
    onResetToDefaultFilter
  } = usePagination(selectedData, ['name', 'changeId', 'changeName'], 'name');

  useEffect(() => {
    if (resetFilter) {
      onResetToDefaultFilter();
    }
  }, [resetFilter, onResetToDefaultFilter]);

  return (
    <div className="position-relative">
      <div className="absolute-top-right mt-n26 mr-n8">
        <Button variant="outline-primary" size="sm" onClick={handleEdit}>
          {t('txt_edit')}
        </Button>
      </div>
      <div className={classnames('section', formValues?.loading && 'loading')}>
        <div className="pt-16">
          <Grid columns={columnDefault} data={dataView} />
        </div>
        <div className="mt-16">
          <Paging
            page={currentPage}
            pageSize={currentPageSize}
            totalItem={total}
            onChangePage={onPageChange}
            onChangePageSize={onPageSizeChange}
          />
        </div>
      </div>
    </div>
  );
};

export default Summary;
