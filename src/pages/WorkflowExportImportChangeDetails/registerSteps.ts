import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { stepRegistry } from '../_commons/WorkflowSetup/registries';
import GettingStartStep from './GettingStartStep';
import SelectChangesToExportStep from './SelectChangesToExportStep';

stepRegistry.registerStep(
  'GetStartedExportImportChangeDetailsWorkflow',
  GettingStartStep
);

stepRegistry.registerStep(
  'SelectChangeToExportExportImportChangeDetailsWorkflow',
  SelectChangesToExportStep,
  [UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__EXPORT_INPORT_CHANGE_DETAILS]
);
