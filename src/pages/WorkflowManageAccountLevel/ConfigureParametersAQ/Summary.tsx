import { Button, useTranslation } from 'app/_libraries/_dls';
import { isFunction } from 'app/_libraries/_dls/lodash';
import React from 'react';
import { ConfigureParametersFormValue } from '../ConfigureParameters';
import TableList from './TableList';

const Summary: React.FC<
  WorkflowSetupSummaryProps<ConfigureParametersFormValue>
> = props => {
  const { onEditStep } = props;
  const { t } = useTranslation();
  const handleEdit = () => {
    isFunction(onEditStep) && onEditStep();
  };
  return (
    <div className="position-relative">
      <div className="absolute-top-right mt-n26 mr-n8">
        <Button variant="outline-primary" size="sm" onClick={handleEdit}>
          {t('txt_edit')}
        </Button>
      </div>
      <TableList {...props} />
    </div>
  );
};

export default Summary;
