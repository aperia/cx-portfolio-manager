import { fireEvent, render, RenderResult } from '@testing-library/react';
import * as CommonSelectHooks from 'pages/_commons/redux/Common/select-hooks';
import React from 'react';
import Summary from './Summary';

const mockUseSelectWindowDimension = jest.spyOn(
  CommonSelectHooks,
  'useSelectWindowDimension'
);

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <Summary {...props} />
    </div>
  );
};

const mOnEditStep = jest.fn();

describe('ConfigureParametersAQ > Summary', () => {
  it('Should render Summary Config AQ ', () => {
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1920,
      height: 1080
    }));

    const wrapper = renderComponent({ onEditStep: mOnEditStep });
    expect(wrapper.getByText('txt_edit')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(mOnEditStep).toBeCalled();
  });
});
