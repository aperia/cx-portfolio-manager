import { PAGE_SIZE } from 'app/constants/constants';
import { BadgeColorType } from 'app/constants/enums';
import { mapGridExpandCollapse } from 'app/helpers';
import { ColumnType, Grid, useTranslation } from 'app/_libraries/_dls';
import {
  DEFAULT_PAGE_NUMBER,
  DEFAULT_PAGE_SIZE
} from 'app/_libraries/_dls/components/Pagination/constants';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import {
  formatBadge,
  formatText,
  formatTruncate
} from 'pages/_commons/Utils/formatGridField';
import Paging from 'pages/_commons/Utils/Paging';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { ConfigureParametersFormValue } from '../ConfigureParameters';
import { AddNewTableAction } from '../ConfigureParameters/AddNewTableModal';
import TableListSubGrid from '../ConfigureParameters/TableListSubGrid';

const TableList: React.FC<
  WorkflowSetupSummaryProps<ConfigureParametersFormValue>
> = ({ formValues, stepId, selectedStep }) => {
  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();
  const [page, setPage] = useState<number>(DEFAULT_PAGE_NUMBER);
  const [pageSize, setPageSize] = useState<number>(PAGE_SIZE[0]);
  const [expandList, setExpandList] = useState<string[]>([]);

  const { tables = [] } = formValues || {};

  const dataTablePage = useMemo(() => {
    return tables.slice(pageSize * (page - 1), page * pageSize);
  }, [tables, pageSize, page]);

  const handleChangePage = (page: number) => setPage(page);
  const handleChangePageSize = (pageSize: number) => setPageSize(pageSize);
  const handleExpand = (expandList: string[]) => {
    setExpandList(expandList);
  };

  useEffect(() => {
    if (selectedStep === stepId) {
      setExpandList([]);
      setPage(DEFAULT_PAGE_NUMBER);
      setPageSize(PAGE_SIZE[0]);
    }
  }, [selectedStep, stepId]);

  const methodTypeToText = useCallback((type: WorkflowSetupMethodType) => {
    switch (type) {
      case AddNewTableAction.createNewVersion:
        return 'Version Created';
      case AddNewTableAction.createNewTable:
        return 'Table Created';
      default:
        return 'Table Modeled';
    }
  }, []);
  const columns: ColumnType[] = useMemo(
    () => [
      {
        id: 'tableId',
        Header: t('txt_manage_transaction_level_tlp_config_tq_table_id'),
        accessor: formatText(['tableId']),
        width: 120
      },
      {
        id: 'tableName',
        Header: t('txt_manage_transaction_level_tlp_config_tq_table_name'),
        accessor: formatText(['tableName']),
        width: 120
      },
      {
        id: 'modelOrCreateFrom',
        Header: t('txt_change_interest_rates_model_create'),
        accessor: formatText(['selectedTableId']),
        width: 128
      },
      {
        id: 'comment',
        Header: t('txt_comment_area'),
        accessor: formatTruncate(['comment']),
        width: width > 1280 ? 360 : 300
      },
      {
        id: 'methodType',
        Header: t('txt_manage_penalty_fee_action_taken'),
        accessor: (data, idx) =>
          formatBadge(['tableType'], {
            colorType: BadgeColorType.TableType,
            noBorder: true
          })(
            {
              ...data,
              tableType: methodTypeToText(data.actionTaken)
            },
            idx
          ),
        width: 153
      }
    ],
    [t, methodTypeToText, width]
  );

  return (
    <div className="pt-16">
      <Grid
        columns={columns}
        data={dataTablePage}
        subRow={(data: any) => {
          return (
            <TableListSubGrid
              controlParameters={data?.original?.controlParameters}
              elementList={data?.original?.elementList}
            />
          );
        }}
        togglable
        toggleButtonConfigList={tables.map(mapGridExpandCollapse('rowId', t))}
        expandedItemKey={'rowId'}
        expandedList={expandList}
        dataItemKey="rowId"
        onExpand={handleExpand}
      />
      {tables?.length > DEFAULT_PAGE_SIZE && (
        <div className="mt-16">
          <Paging
            page={page}
            pageSize={pageSize}
            totalItem={tables?.length}
            onChangePage={handleChangePage}
            onChangePageSize={handleChangePageSize}
          />
        </div>
      )}
    </div>
  );
};

export default TableList;
