import { ServiceSubjectSection } from 'app/constants/enums';
import { mockThunkAction } from 'app/utils';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import parseFormValues from './parseFormValues';

const mockActionsWorkflowSetup = mockThunkAction(actionsWorkflowSetup);

beforeEach(() => {
  mockActionsWorkflowSetup('getDecisionElementList');
});

describe('pages > workflowManageAccountLevel > ConfigureParametersAQ > parseFormValues', () => {
  it('should return undefined with methodList is undefined', async () => {
    const response = await parseFormValues(
      {
        data: {
          configurations: {
            tableList: [
              { tableType: 'AQ', modeledFrom: '' },
              { tableType: 'AQ', modeledFrom: 'Modeled' }
            ]
          },
          workflowSetupData: [{ id: '1' }]
        }
      } as any,
      '1',
      jest.fn()
    );
    expect(response).toEqual(
      expect.objectContaining({
        isPass: false,
        isSelected: false,
        isValid: true
      })
    );
  });

  it('should return undefined with stepInfo is undefined', async () => {
    const response = await parseFormValues(
      {
        data: {
          workflowSetupData: [
            {
              id: '2'
            }
          ],
          configurations: {
            methodList: []
          }
        }
      } as any,
      '1',
      jest.fn()
    );
    expect(response).toEqual(undefined);
  });

  it('should return valid data', async () => {
    const response = await parseFormValues(
      {
        data: {
          workflowSetupData: [
            {
              id: '1'
            }
          ],
          configurations: {
            methodList: [
              {
                id: '123',
                name: '123',
                serviceSubjectSection: ServiceSubjectSection.CIT
              }
            ]
          }
        }
      } as any,
      '1',
      jest.fn()
    );

    expect(response).toEqual(
      expect.objectContaining({
        isPass: false,
        isSelected: false,
        isValid: false,
        tableType: 'AQ',
        tables: []
      })
    );
  });
});
