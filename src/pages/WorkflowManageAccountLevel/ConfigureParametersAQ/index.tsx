import {
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { useUnsavedChangeRegistry } from 'app/hooks';
import { isEqual } from 'lodash';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, { useEffect, useState } from 'react';
import ConfigureParameters, {
  ConfigureParametersFormValue
} from '../ConfigureParameters';
import { TableType } from '../ConfigureParameters/constant';
import parseFormValues from './parseFormValues';
import Summary from './Summary';

const ConfigureParametersAQ: React.FC<
  WorkflowSetupProps<ConfigureParametersFormValue>
> = props => {
  const { formValues, savedAt } = props;
  const [prevValues, setPrevValues] = useState(formValues);

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_ACCOUNT_LEVEL__AQ__CONFIGURE_PARAMETERS,
      priority: 1
    },
    [!isEqual(prevValues?.tables, formValues?.tables)]
  );

  useEffect(() => {
    setPrevValues(formValues);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [savedAt]);

  return (
    <div>
      <ConfigureParameters {...props} tableType={TableType.alpaq} />
    </div>
  );
};

const ExtraStaticConfigureParametersAQ =
  ConfigureParametersAQ as WorkflowSetupStaticProp;

ExtraStaticConfigureParametersAQ.summaryComponent = Summary;
ExtraStaticConfigureParametersAQ.defaultValues = {
  isValid: false,
  tables: [],
  tableType: TableType.alpaq
};
ExtraStaticConfigureParametersAQ.parseFormValues = parseFormValues;

export default ExtraStaticConfigureParametersAQ;
