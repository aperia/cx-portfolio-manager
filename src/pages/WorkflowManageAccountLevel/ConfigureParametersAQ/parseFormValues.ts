import { getWorkflowSetupStepStatus } from 'app/helpers';
import get from 'lodash.get';
import { nanoid } from 'nanoid';
import { TableType } from 'pages/WorkflowManageTransactionLevelProcessingDecisionTablesTLP/ConfigureParameters/constant';
import {
  getTableActionType,
  mappingControlParameterValues,
  mappingElementListInfo
} from 'pages/WorkflowManageTransactionLevelProcessingDecisionTablesTLP/ConfigureParameters/helper';
import { ConfigTableItem } from 'pages/WorkflowManageTransactionLevelProcessingDecisionTablesTLP/ConfigureParameters/type';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';

const parseFormValues: WorkflowSetupStepFormDataFunc<any> = async (
  data,
  id,
  dispatch
) => {
  const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);
  if (!stepInfo) return;

  const result = (await Promise.resolve(
    dispatch(
      actionsWorkflowSetup.getDecisionElementList({
        tableType: TableType.tlpaq,
        serviceNameAbbreviation: 'ALP'
      })
    )
  )) as any;

  const decisionElements = get(result, ['payload', 'decisionElementList'], []);

  const tables = (data.data.configurations?.tableList || [])
    .filter((table: any) => table.tableType === TableType.tlpaq)
    .map(
      (table: any) =>
        ({
          ...table,
          id: '',
          rowId: nanoid(),
          selectedVersion: table.modeledFrom
            ? {
                tableId: table?.modeledFrom?.tableId,
                tableName: table?.modeledFrom?.tableName,
                version: { tableId: table?.modeledFrom?.version?.tableId }
              }
            : null,
          selectedTableId: table?.modeledFrom
            ? table?.modeledFrom?.tableId
            : null,
          selectedVersionId: table?.modeledFrom
            ? table?.modeledFrom?.version?.tableId
            : null,
          elementList: mappingElementListInfo(
            table?.decisionElements ?? [],
            decisionElements
          ),
          actionTaken: getTableActionType(table?.modeledFrom, table?.tableId),
          controlParameters: mappingControlParameterValues(
            TableType.tlpaq,
            table?.tableControlParameters
          )
        } as ConfigTableItem)
    );

  return {
    ...getWorkflowSetupStepStatus(stepInfo),
    isValid: tables.length > 0,
    tables,
    tableType: TableType.tlpaq
  };
};

export default parseFormValues;
