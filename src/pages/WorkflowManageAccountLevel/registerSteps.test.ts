import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { default as stepRegistry } from '../_commons/WorkflowSetup/registries/stepRegistry';

const registerFunc = jest.spyOn(stepRegistry, 'registerStep');

describe('ManagePenaltyFeeWorkflow > registerSteps', () => {
  it('Should register components', async () => {
    await import('./registerSteps');

    expect(registerFunc).toBeCalledWith(
      'GetStartedManageAccountLevel',
      expect.anything(),
      [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_ACCOUNT_LEVEL__GET_STARTED__EXISTED_WORKFLOW,
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_ACCOUNT_LEVEL__GET_STARTED__EXISTED_WORKFLOW__STUCK
      ]
    );

    expect(registerFunc).toBeCalledWith(
      'ManageAccountLevelAC',
      expect.anything(),
      [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_ACCOUNT_LEVEL__AC__CONFIGURE_PARAMETERS
      ]
    );

    expect(registerFunc).toBeCalledWith(
      'ConfigureParametersAQManageAccountLevel',
      expect.anything(),
      [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_ACCOUNT_LEVEL__AQ__CONFIGURE_PARAMETERS
      ]
    );
  });
});
