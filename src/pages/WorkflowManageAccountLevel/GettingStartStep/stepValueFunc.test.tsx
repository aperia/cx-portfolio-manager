import { FormGettingStart } from '.';
import stepValueFunc from './stepValueFunc';

describe('ManageAccountLevelWorkflow > GettingStartStep > stepValueFunc', () => {
  it('Should Returned Account Qualification (AQ) and Client Allocation (CA)', () => {
    const stepsForm = {
      alpAQ: true,
      alpCA: true
    } as FormGettingStart;

    const response = stepValueFunc({ stepsForm });
    expect(response).toEqual(
      'Account Qualification (AQ) and Client Allocation (CA)'
    );
  });

  it('Should Return Account Qualification (AQ)', () => {
    const stepsForm = {
      alpAQ: true
    } as FormGettingStart;

    const response = stepValueFunc({ stepsForm });
    expect(response).toEqual('Account Qualification (AQ)');
  });

  it('Should Return Client Allocation (CA)', () => {
    const stepsForm = {
      alpCA: true
    } as FormGettingStart;

    const response = stepValueFunc({ stepsForm });
    expect(response).toEqual('Client Allocation (CA)');
  });

  it('Should Return empty', () => {
    const stepsForm: any = undefined;

    const response = stepValueFunc({ stepsForm });
    expect(response).toEqual('');
  });
});
