import { fireEvent, render, RenderResult } from '@testing-library/react';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageAccountLevel';
import React from 'react';
import * as reactRedux from 'react-redux';
import GettingStartStep, { FormGettingStart } from './index';

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');

const useSelectDecisionElementListData = jest.spyOn(
  WorkflowSetup,
  'useSelectElementMetadataManageAcountLevel'
);

const MockTableConfigOptions = [
  {
    code: 'AQ',
    text: 'Account Qualification'
  },
  {
    code: 'CA',
    text: 'Client Allocation'
  }
];

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});
jest.mock('app/hooks/useUnsavedChangeRegistry', () => {
  return {
    __esModule: true,
    useUnsavedChangeRegistry: (options: any, changes: any, onConfirm: any) => {
      onConfirm && onConfirm();
    }
  };
});

jest.mock('pages/_commons/OverviewModal', () => ({
  __esModule: true,
  default: ({ onClose }: any) => {
    return (
      <div>
        <div>OverviewModal</div>
        <button onClick={onClose}>OverviewButtonCloseModal</button>
      </div>
    );
  }
}));

jest.mock('pages/_commons/ContentExpand', () => ({
  __esModule: true,
  default: ({ instruction, children }: any) => (
    <div>
      <div>ContentExpand</div>
      <div>{instruction}</div>
      <div>{children}</div>
    </div>
  )
}));

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <GettingStartStep {...props} />
    </div>
  );
};

describe('WorkflowManageTransactionLevelProcessingDecisionTablesTLP > GettingStartStep > Index', () => {
  const mockDispatch = jest.fn();
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
    useSelectDecisionElementListData.mockImplementation(() => ({
      searchCode: [],
      allocationFlag: [{ code: '2', text: '2' }],
      allocationBeforeAfterCycle: [{ code: 'B', value: 'B' }],
      changeInTermsMethodFlag: [{ code: 'N', value: 'N' }],
      allocationInterval: [],
      alpConfig: MockTableConfigOptions
    }));
  });

  it('Should render without Overview modal > Open overview modal', () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: {},
      setFormValues: mockFn
    } as WorkflowSetupProps<FormGettingStart>;

    const wrapper = renderComponent(props);
    expect(wrapper.queryByText(/OverviewModal/)).toBeInTheDocument();
    wrapper.rerender(
      <div>
        <GettingStartStep
          {...props}
          stepId="1"
          formValues={{ alreadyShown: true, alpCA: true }}
        />
      </div>
    );

    fireEvent.click(wrapper.getByText('txt_view_overview'));
    expect(wrapper.queryByText(/OverviewModal/)).toBeInTheDocument();
  });

  it('Should render Overview modal > Close overview modal', () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: {
        alpCA: true,
        isValid: true
      },
      setFormValues: mockFn
    } as WorkflowSetupProps<FormGettingStart>;

    const wrapper = renderComponent(props);
    expect(wrapper.queryByText(/OverviewModal/)).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('OverviewButtonCloseModal'));
    expect(wrapper.queryByText(/OverviewModal/)).not.toBeInTheDocument();
  });

  it('change transaction', () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: {
        alpCA: true,
        isValid: true
      },
      setFormValues: mockFn
    } as WorkflowSetupProps<FormGettingStart>;

    const wrapper = renderComponent(props);
    const checkbox = wrapper.baseElement.querySelector(
      'input[class="custom-control-input dls-checkbox-input"]'
    ) as Element;
    const _checkbox = checkbox as HTMLInputElement;
    expect(_checkbox.checked).toEqual(false);

    fireEvent.click(checkbox);
    expect(_checkbox.checked).toEqual(true);

    fireEvent.click(wrapper.getByText('Account Qualification'));
  });

  it('should render stuck message', () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: {
        alpCA: true,
        isValid: true
      },
      setFormValues: mockFn,
      isStuck: true
    } as WorkflowSetupProps<FormGettingStart>;

    const wrapper = renderComponent(props);
    expect(
      wrapper.getByText('txt_step_stuck_move_forward_message')
    ).toBeInTheDocument();
  });
});
