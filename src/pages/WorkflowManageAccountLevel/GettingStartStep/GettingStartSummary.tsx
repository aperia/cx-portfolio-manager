import GroupTextControl from 'app/components/DofControl/GroupTextControl';
import { Button, useTranslation } from 'app/_libraries/_dls';
import { isFunction } from 'lodash';
import React, { useMemo } from 'react';
import stepValueFunc from './stepValueFunc';

const GettingStartSummary: React.FC<WorkflowSetupSummaryProps> = ({
  formValues,
  onEditStep
}) => {
  const { t } = useTranslation();
  const gettingStartText = useMemo(
    () => stepValueFunc({ stepsForm: formValues, dependencies: {}, t }),
    [formValues, t]
  );

  const handleEdit = () => {
    isFunction(onEditStep) && onEditStep();
  };

  return (
    <div className="position-relative">
      <div className="absolute-top-right mt-n26 mr-n8">
        <Button variant="outline-primary" size="sm" onClick={handleEdit}>
          {t('txt_edit')}
        </Button>
      </div>
      <div className="row">
        <div className="col-12">
          <GroupTextControl
            id="summary_typeOfFee"
            input={{ value: gettingStartText } as any}
            meta={{} as any}
            label={t('txt_manage_account_level_table_alp_table')}
            options={{
              inline: false,
              lineTruncate: 2,
              ellipsisLessText: t('txt_less'),
              ellipsisMoreText: t('txt_more')
            }}
          />
        </div>
      </div>
    </div>
  );
};

export default GettingStartSummary;
