import { ConfigTableALPOptionsEnum } from 'app/constants/enums';
import { WORKFLOW_SETUP } from 'app/constants/local-storage';
import {
  unsavedExistedWorkflowSetupDataOnStuckProps,
  unsavedExistedWorkflowSetupDataProps,
  unsavedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { useUnsavedChangeRegistry } from 'app/hooks';
import {
  Button,
  CheckBox,
  Icon,
  InlineMessage,
  TransDLS,
  useTranslation
} from 'app/_libraries/_dls';
import ContentExpand from 'pages/_commons/ContentExpand';
import OverviewModal from 'pages/_commons/OverviewModal';
import {
  actionsWorkflowSetup,
  useSelectElementMetadataManageAcountLevel
} from 'pages/_commons/redux/WorkflowSetup';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';
import GettingStartSummary from './GettingStartSummary';
import parseFormValues from './parseFormValues';
import stepValueFunc from './stepValueFunc';

export interface FormGettingStart {
  isValid?: boolean;

  alpAQ?: boolean;
  alpCA?: boolean;

  alreadyShown?: boolean;
}
export interface GettingStartProps
  extends WorkflowSetupProps<FormGettingStart> {
  alpCAFormId?: string;
  alpCAUploadFileFormId?: string;
  alpAQFormId?: string;
  alpAQUploadFileFormId?: string;
}
const GettingStartStep: React.FC<GettingStartProps> = ({
  title,
  stepId,
  selectedStep,
  savedAt,
  formValues,
  hasInstance,
  isStuck,

  alpCAFormId,
  alpCAUploadFileFormId,
  alpAQFormId,
  alpAQUploadFileFormId,

  setFormValues,
  clearFormValues
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;

  const [initialValues, setInitialValues] = useState(formValues);
  const [showOverview, setShowOverview] = useState(
    formValues.alreadyShown
      ? false
      : localStorage.getItem(WORKFLOW_SETUP.SHOW_OVERVIEW_AGAIN) !== 'false'
  );

  const metadata = useSelectElementMetadataManageAcountLevel();
  const { alpConfig } = metadata;

  useEffect(() => {
    dispatch(
      actionsWorkflowSetup.getWorkflowMetadata([
        ConfigTableALPOptionsEnum.alpTable
      ])
    );
  }, [dispatch]);

  useEffect(() => {
    const isValid = !!formValues.alpAQ || !!formValues.alpCA;
    if (formValues.isValid === isValid) return;

    keepRef.current.setFormValues({ isValid });
  }, [formValues]);

  useEffect(() => {
    setInitialValues(formValues);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [savedAt]);

  useUnsavedChangeRegistry(
    {
      ...unsavedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_ACCOUNT_LEVEL__GET_STARTED,
      priority: 1
    },
    [!hasInstance && (!!formValues.alpCA || !!formValues.alpAQ)]
  );

  const handleConfirmChangeDependencies = useCallback(() => {
    alpCAFormId && !formValues.alpCA && clearFormValues(alpCAFormId);
    alpCAUploadFileFormId &&
      !formValues.alpCA &&
      clearFormValues(alpCAUploadFileFormId);

    alpAQFormId && !formValues.alpAQ && clearFormValues(alpAQFormId);
    alpAQUploadFileFormId &&
      !formValues.alpAQ &&
      clearFormValues(alpAQUploadFileFormId);
  }, [
    clearFormValues,
    formValues.alpCA,
    formValues.alpAQ,
    alpCAFormId,
    alpCAUploadFileFormId,
    alpAQFormId,
    alpAQUploadFileFormId
  ]);

  const isUncheckAll = useMemo(
    () => !formValues.alpCA && !formValues.alpAQ,
    [formValues.alpCA, formValues.alpAQ]
  );
  const hasChanged = useMemo(
    () =>
      initialValues.alpCA !== formValues.alpCA ||
      initialValues.alpAQ !== formValues.alpAQ,
    [initialValues, formValues]
  );

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_ACCOUNT_LEVEL__GET_STARTED__EXISTED_WORKFLOW,
      priority: 1,
      confirmFirst: true
    },
    [!isUncheckAll && !!hasInstance && hasChanged],
    handleConfirmChangeDependencies
  );
  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataOnStuckProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_ACCOUNT_LEVEL__GET_STARTED__EXISTED_WORKFLOW__STUCK,
      priority: 1,
      confirmFirst: true
    },
    [isUncheckAll && !!hasInstance && hasChanged]
  );

  const handleChangeOption = (option: keyof FormGettingStart) => {
    const onChange = (e: any) => {
      setFormValues({ [option]: !formValues[option] });
    };

    return onChange;
  };

  return (
    <React.Fragment>
      <ContentExpand
        isSelectedStep={stepId === selectedStep}
        change={savedAt}
        instruction={
          <div className="pt-24 px-24">
            <div className="d-flex align-items-center justify-content-between">
              <h4>{title}</h4>
              <Button
                className="mr-n8"
                variant="outline-primary"
                size="sm"
                onClick={() => setShowOverview(true)}
              >
                {t('txt_view_overview')}
              </Button>
            </div>
            <div className="pb-24">
              {isStuck && (
                <InlineMessage className="mb-0 mt-24" variant="danger" withIcon>
                  {t('txt_step_stuck_move_forward_message')}
                </InlineMessage>
              )}

              <div className="row">
                <div className="col-6 mt-24">
                  <div className="bg-light-l20 rounded-lg p-16">
                    <div className="text-center">
                      <Icon
                        name="request"
                        size="12x"
                        className="color-grey-l16"
                      />
                    </div>
                    <p className="mt-8 color-grey">
                      {t('txt_manage_account_level_left_1')}
                    </p>
                    <p className="mt-16 fw-600">
                      {t('txt_manage_account_level_left_2')}
                    </p>
                    <p className="mt-8 color-grey">
                      {t('txt_manage_account_level_left_3')}
                    </p>
                    <ul className="list-dot color-grey">
                      <li>{t('txt_manage_account_level_left_3_1')}</li>
                      <li>{t('txt_manage_account_level_left_3_2')}</li>
                    </ul>
                    <p className="mt-8 color-grey">
                      {t('txt_manage_account_level_left_4')}
                    </p>
                    <ul className="list-dot color-grey">
                      <li>{t('txt_manage_account_level_left_4_1')}</li>
                      <li>{t('txt_manage_account_level_left_4_2')}</li>
                      <li>{t('txt_manage_account_level_left_4_3')}</li>
                      <li>{t('txt_manage_account_level_left_4_4')}</li>
                    </ul>
                  </div>
                </div>
                <div className="col-6 mt-24 color-grey">
                  <p>{t('txt_manage_account_level_right_1')}</p>
                  <p className="mt-8">
                    <TransDLS keyTranslation="txt_manage_account_level_right_2">
                      <strong className="color-grey-d20" />
                    </TransDLS>
                  </p>
                  <p className="mt-8">
                    <TransDLS keyTranslation="txt_manage_account_level_right_3">
                      <strong className="color-grey-d20" />
                    </TransDLS>
                  </p>
                  <p className="mt-8">
                    <TransDLS keyTranslation="txt_manage_account_level_right_4">
                      <strong className="color-grey-d20" />
                    </TransDLS>
                  </p>
                  <p className="mt-8">
                    <TransDLS keyTranslation="txt_manage_account_level_right_5">
                      <strong className="color-grey-d20" />
                    </TransDLS>
                  </p>
                  <p className="mt-8">
                    <TransDLS keyTranslation="txt_manage_account_level_right_6">
                      <strong className="color-grey-d20" />
                    </TransDLS>
                  </p>
                </div>
              </div>
            </div>
          </div>
        }
      >
        <div className="p-24">
          <h5>{t('txt_manage_account_level_getting_start_title')}</h5>
          <div className="row mt-16 list-cards list-cards--selectable list-cards--single">
            {Array.isArray(alpConfig) &&
              alpConfig.map(o => (
                <div className="col-6" key={o.code}>
                  <label
                    className="list-cards__item custom-control-root"
                    htmlFor={o.code}
                  >
                    <span className="d-flex align-items-center">
                      <span className="pr-8">{t(o.text)}</span>
                    </span>
                    <CheckBox className="mr-n4">
                      <CheckBox.Input
                        id={o.code}
                        checked={
                          formValues[`alp${o.code}` as keyof FormGettingStart]
                        }
                        onChange={handleChangeOption(
                          `alp${o.code}` as keyof FormGettingStart
                        )}
                      />
                    </CheckBox>
                  </label>
                </div>
              ))}
          </div>
        </div>
      </ContentExpand>
      {showOverview && (
        <OverviewModal
          id="overviewWorkflowSetup"
          show
          onClose={() => {
            setShowOverview(false);
            keepRef.current.setFormValues({ alreadyShown: true });
          }}
        />
      )}
    </React.Fragment>
  );
};

const ExtraStaticGettingStartStep =
  GettingStartStep as WorkflowSetupStaticProp<FormGettingStart>;

ExtraStaticGettingStartStep.summaryComponent = GettingStartSummary;
ExtraStaticGettingStartStep.stepValue = stepValueFunc;
ExtraStaticGettingStartStep.defaultValues = {
  isValid: false,
  alpCA: false,
  alpAQ: false
};
ExtraStaticGettingStartStep.parseFormValues = parseFormValues;

export default ExtraStaticGettingStartStep;
