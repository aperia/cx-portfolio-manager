import { FormGettingStart } from '.';

const stepValueFunc: WorkflowSetupStepValueFunc<FormGettingStart> = ({
  stepsForm: formValues
}) => {
  const { alpAQ, alpCA } = formValues || {};

  if (alpAQ && alpCA) {
    return 'Account Qualification (AQ) and Client Allocation (CA)';
  }

  return alpAQ
    ? 'Account Qualification (AQ)'
    : alpCA
    ? 'Client Allocation (CA)'
    : '';
};

export default stepValueFunc;
