import { MethodFieldParameterEnum } from 'app/constants/enums';
import {
  ColumnType,
  Grid,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import { useSelectDecisionElementListData } from 'pages/_commons/redux/WorkflowSetup';
import { formatText } from 'pages/_commons/Utils/formatGridField';
import React from 'react';
import { TableType } from '../ConfigureParameters/constant';

interface IAQTableListSubGrid {
  elementList: any[];
  tableType: TableType;
  controlParameters: any[];
}

const AQTableListSubGrid: React.FC<IAQTableListSubGrid> = ({
  elementList = [],
  tableType,
  controlParameters
}) => {
  const data = useSelectDecisionElementListData(tableType);
  const decisionElementList = data?.decisionElementList || [];
  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();
  const nonConfigurableOption = decisionElementList.filter((el: any) => {
    const param = controlParameters.find(
      item => item?.id === MethodFieldParameterEnum.ChangeInTermsMethodFlag
    );
    return param?.value === 'Y'
      ? !el.configurable
      : !el.configurable && el.name !== 'CIT METHOD FLAG';
  });
  const elementListShow = [...elementList, ...nonConfigurableOption];
  const columns: ColumnType[] = [
    {
      id: 'name',
      Header: 'Decision Element',
      accessor: formatText(['name']),
      cellBodyProps: { className: 'px-20 py-12' },
      width: width < 1280 ? 200 : 270
    },
    {
      id: 'moreInfo',
      Header: t('txt_description'),
      accessor: ({ moreInfo }) => (
        <TruncateText
          resizable
          lines={2}
          ellipsisLessText={t('txt_less')}
          ellipsisMoreText={t('txt_more')}
        >
          {moreInfo}
        </TruncateText>
      )
    }
  ];

  return (
    <div className="p-20">
      <Grid columns={columns} data={elementListShow} />
    </div>
  );
};

export default AQTableListSubGrid;
