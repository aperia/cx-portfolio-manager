import { PAGE_SIZE } from 'app/constants/constants';
import { mapGridExpandCollapse } from 'app/helpers';
import {
  Button,
  ColumnType,
  Grid,
  Icon,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import {
  DEFAULT_PAGE_NUMBER,
  DEFAULT_PAGE_SIZE
} from 'app/_libraries/_dls/components/Pagination/constants';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import { useWorkflowFormStep } from 'pages/_commons/redux/WorkflowSetup';
import Paging from 'pages/_commons/Utils/Paging';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, { useEffect, useMemo, useState } from 'react';
import { TableType } from '../ConfigureParameters/constant';
import AQTableListSubGrid from './AQTableListSubGrid';

export interface TLPTableListProp {
  isValid?: boolean;
}

const TLPTableList: React.FC<WorkflowSetupProps<TLPTableListProp>> = ({
  stepId,
  selectedStep
}) => {
  const { t } = useTranslation();
  const [isExpand, setIsExpand] = useState(false);
  const [expandedList, setExpandedList] = useState<string[]>([]);
  const [page, setPage] = useState<number>(DEFAULT_PAGE_NUMBER);
  const [pageSize, setPageSize] = useState<number>(PAGE_SIZE[0]);

  const tableType = TableType.alpaq;

  const data = useWorkflowFormStep('configAlpAq');

  const tables = data?.tables || [];

  const dataTablePage = tables.slice(pageSize * (page - 1), page * pageSize);

  const handleChangePage = (page: number) => setPage(page);
  const handleChangePageSize = (pageSize: number) => setPageSize(pageSize);

  const columns: ColumnType[] = useMemo(
    () => [
      {
        id: 'name',
        Header: t('txt_manage_transaction_level_tlp_config_tq_table_id'),
        accessor: 'tableId'
      }
    ],
    [t]
  );

  useEffect(() => {
    if (stepId !== selectedStep) {
      setIsExpand(false);
      setExpandedList([]);
      setPage(DEFAULT_PAGE_NUMBER);
      setPageSize(PAGE_SIZE[0]);
    }
  }, [stepId, selectedStep]);

  return (
    <div className="border-bottom py-24">
      <div className="d-flex align-items-center ">
        <div className="flex-shrink-0 ml-n2 mr-8">
          <Tooltip
            element={isExpand ? t('txt_collapse') : t('txt_expand')}
            variant="primary"
            placement="top"
          >
            <Button
              variant="icon-secondary"
              size="sm"
              onClick={() => setIsExpand(!isExpand)}
            >
              <Icon name={isExpand ? 'minus' : 'plus'} />
            </Button>
          </Tooltip>
        </div>
        <h5>{t('txt_manage_transaction_level_tlp_config_tq_tableList')}</h5>
      </div>
      {isExpand && (
        <div className="mt-8">
          <p className="color-grey">
            {t('txt_manage_transaction_level_tlp_download_template_help_text')}
          </p>
          {!isEmpty(tables) && (
            <div className="mt-16">
              <Grid
                columns={columns}
                data={dataTablePage}
                subRow={(data: any) => {
                  return (
                    <AQTableListSubGrid
                      elementList={data?.original?.elementList}
                      controlParameters={data?.original?.controlParameters}
                      tableType={tableType}
                    />
                  );
                }}
                togglable
                expandedItemKey="rowId"
                dataItemKey="rowId"
                toggleButtonConfigList={dataTablePage.map(
                  mapGridExpandCollapse('rowId', t)
                )}
                expandedList={expandedList}
                onExpand={setExpandedList}
              />
              {tables?.length > DEFAULT_PAGE_SIZE && (
                <div className="mt-16">
                  <Paging
                    page={page}
                    pageSize={pageSize}
                    totalItem={tables?.length}
                    onChangePage={handleChangePage}
                    onChangePageSize={handleChangePageSize}
                  />
                </div>
              )}
            </div>
          )}
        </div>
      )}
    </div>
  );
};

const ExtraStaticTLPTableList =
  TLPTableList as WorkflowSetupStaticProp<TLPTableListProp>;

ExtraStaticTLPTableList.defaultValues = { isValid: true };

export default ExtraStaticTLPTableList;
