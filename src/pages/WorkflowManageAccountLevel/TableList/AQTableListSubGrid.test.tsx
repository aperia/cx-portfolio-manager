import { render, RenderResult } from '@testing-library/react';
import * as CommonSelectHooks from 'pages/_commons/redux/Common/select-hooks';
import * as WorkflowSetupTLP from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageTransactionLevelProcessingDecisionTLP';
import React from 'react';
import TableListSubGrid from './AQTableListSubGrid';

const mockUseSelectWindowDimension = jest.spyOn(
  CommonSelectHooks,
  'useSelectWindowDimension'
);

const useSelectDecisionElementListData = jest.spyOn(
  WorkflowSetupTLP,
  'useSelectDecisionElementListData'
);

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const MockElementList = [
  { name: 'test', moreInfo: 'test' },
  { name: '1', moreInfo: '2' }
] as any[];

const MockElementListOptions = [
  {
    name: 'ACQUIRING VISA BIN',
    moreInfo: null,
    immediateAllocation: 'No',
    configurable: true
  },
  {
    name: 'RESULT ID',
    moreInfo:
      'Enter the Promotion Identifier in the RESULT ID field for each criteria row to point cardholder accounts to Product Control File settings.\nFor the transaction qualification tables, the Promotion Identifier is a variable-length, 8-position code that identifies a group of Product Control File methods. The methods include parameter settings that control processing options.',
    immediateAllocation: 'No',
    configurable: false
  }
];

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <TableListSubGrid {...props} controlParameters={[{ id: 'test' }]} />
    </div>
  );
};

describe('WorkflowManageTransactionLevelProcessingDecisionTablesTLP > TLPTableList > TLPTableListSubGrid', () => {
  beforeEach(() => {
    useSelectDecisionElementListData.mockReturnValue({
      decisionElementList: MockElementListOptions
    });
  });

  it('Should render Table List SubGrid ', () => {
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1920,
      height: 1080
    }));
    const wrapper = renderComponent({
      elementList: MockElementList
    });

    expect(wrapper.getByText('txt_description')).toBeInTheDocument();
  });

  it('Should render Table List SubGrid ', () => {
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1200,
      height: 1080
    }));
    const wrapper = renderComponent({});

    expect(wrapper.getByText('txt_description')).toBeInTheDocument();
  });
});
