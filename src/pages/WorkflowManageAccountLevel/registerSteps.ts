import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import {
  stepRegistry,
  uploadFileStepRegistry
} from '../_commons/WorkflowSetup/registries';
import ConfigureParametersAQ from './ConfigureParametersAQ';
import GettingStartStep from './GettingStartStep';
import ManageAccountCAStep from './ManageAccountLevelCAStep';
import SubContent from './ManageAccountLevelCAStep/SubContent';
import AQTableList from './TableList';

stepRegistry.registerStep('GetStartedManageAccountLevel', GettingStartStep, [
  UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_ACCOUNT_LEVEL__GET_STARTED__EXISTED_WORKFLOW,
  UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_ACCOUNT_LEVEL__GET_STARTED__EXISTED_WORKFLOW__STUCK
]);

stepRegistry.registerStep('ManageAccountLevelAC', ManageAccountCAStep, [
  UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_ACCOUNT_LEVEL__AC__CONFIGURE_PARAMETERS
]);

stepRegistry.registerStep(
  'ConfigureParametersAQManageAccountLevel',
  ConfigureParametersAQ,
  [
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_ACCOUNT_LEVEL__AQ__CONFIGURE_PARAMETERS
  ]
);

uploadFileStepRegistry.registerComponent(
  'SubContentManageAccountLevelCA',
  SubContent
);

uploadFileStepRegistry.registerComponent(
  'SubContentManageAccountLevelAQ',
  AQTableList
);
