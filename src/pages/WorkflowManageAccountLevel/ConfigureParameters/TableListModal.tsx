import FailedApiReload from 'app/components/FailedApiReload';
import ModalRegistry from 'app/components/ModalRegistry';
import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import TableCardView from 'app/components/TableCardView';
import { ORDER_BY_LIST } from 'app/constants/constants';
import { classnames } from 'app/helpers';
import {
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  TransDLS,
  useTranslation
} from 'app/_libraries/_dls';
import DropdownList from 'app/_libraries/_dls/components/DropdownList';
import isEmpty from 'lodash.isempty';
import ViewVersionDetail from 'pages/WorkflowManageAccountLevel/ManageAccountLevelCAStep/ViewModal';
import ClearAndResetButton from 'pages/_commons/Utils/ClearAndResetButton';
import Paging from 'pages/_commons/Utils/Paging';
import React, { useMemo, useRef } from 'react';
import { TableType } from './constant';
import { CONFIG_ACTIONS_MODAL, EXPANDED_LIST } from './useConfigActions';
import { useTableListModal } from './useTableListModal';

export interface TableListModalProps {
  id: string;
  show: boolean;
  handleClose: () => void;
  selectedVersion: any;
  handleSetExpandedList: (
    tableListName: EXPANDED_LIST
  ) => (expandItem: Record<string, any>) => void;
  handleSelectVersion: (version: any) => void;
  expandedList: Record<string, any>;
  handleOpenConfigModal: (tableName: CONFIG_ACTIONS_MODAL | null) => void;
  isCreateNewVersion: boolean;
  handleSetIsCreateNewVersion: (isCreateNewVersion: boolean) => void;
  handleSetDraftNewTable: (draftNewTable: any) => void;
  draftNewTable: any;
  tableType: TableType;
}

const TableListModal: React.FC<TableListModalProps> = props => {
  const { t } = useTranslation();
  const { id, show, expandedList, selectedVersion } = props;
  const simpleSearchRef = useRef<any>(null);
  const {
    tables,
    total,
    page,
    pageSize,
    handleChangePage,
    handleChangePageSize,
    handleSearch,
    orderBy,
    searchValue,
    handleChangeOrderBy,
    loading,
    error,
    handleGetTableList,
    handleClearAndReset,
    versionDetail,
    title,
    helpText1,
    handleGoToNextStep,
    handleToggleExpandedTableListModal,
    handleSelectTableVersion,
    handleCloseModalVersionDetail,
    handleCloseAndClearState
  } = useTableListModal(props);

  const orderByListItems = useMemo(() => {
    return ORDER_BY_LIST.map(item => (
      <DropdownList.Item
        key={item.value}
        label={item.description}
        value={item}
      />
    ));
  }, []);

  const noDataFound = useMemo(
    () =>
      !loading &&
      !error &&
      total === 0 && (
        <div className="d-flex flex-column justify-content-center mt-40">
          <NoDataFound
            id="method-list-NoDataFound"
            title={t('txt_no_tables_to_display')}
            hasSearch={!!searchValue}
            linkTitle={!!searchValue && t('txt_clear_and_reset')}
            onLinkClicked={handleClearAndReset}
          />
        </div>
      ),
    [t, loading, error, total, searchValue, handleClearAndReset]
  );
  const failedReload = useMemo(
    () => (
      <div>
        {error && (
          <FailedApiReload
            id="method-list-error"
            onReload={handleGetTableList}
            className="mt-40 pt-40 d-flex flex-column justify-content-center align-items-center"
          />
        )}
      </div>
    ),
    [error, handleGetTableList]
  );

  const tableList = useMemo(
    () => (
      <div>
        {total > 0 && (
          <React.Fragment>
            <div className="mt-16 mx-n12">
              {tables.map((table: any) => (
                <TableCardView
                  id={table.tableId}
                  key={table.tableId}
                  value={table}
                  selectedVersion={selectedVersion}
                  onSelectVersion={handleSelectTableVersion(table.tableId)}
                  onViewVersionDetail={handleSelectTableVersion(
                    table.tableId,
                    true
                  )}
                  isExpand={expandedList[table?.tableId]}
                  onToggle={handleToggleExpandedTableListModal}
                />
              ))}
            </div>
            <div className="mt-8">
              <Paging
                page={page}
                pageSize={pageSize}
                totalItem={total}
                onChangePage={handleChangePage}
                onChangePageSize={handleChangePageSize}
              />
            </div>
          </React.Fragment>
        )}
      </div>
    ),
    [
      handleChangePage,
      handleChangePageSize,
      total,
      page,
      pageSize,
      tables,
      selectedVersion,
      handleToggleExpandedTableListModal,
      handleSelectTableVersion,
      expandedList
    ]
  );

  return (
    <React.Fragment>
      <ModalRegistry
        id={id}
        show={show}
        lg
        onAutoClosedAll={handleCloseAndClearState}
      >
        <ModalHeader border closeButton onHide={handleCloseAndClearState}>
          <ModalTitle>{title}</ModalTitle>
        </ModalHeader>
        <ModalBody
          className={classnames('px-24 pt-24 pb-24 pb-lg-0', {
            loading
          })}
        >
          <div className="color-grey">
            <p>
              <TransDLS keyTranslation={helpText1}>
                <sup></sup>
              </TransDLS>{' '}
              <span className="fw-500 color-grey-d20">{t('txt_continue')}</span>
              .
            </p>
          </div>
          <div className="mt-24">
            <div className="d-flex justify-content-between align-items-center mb-16">
              <h5>
                {t('txt_manage_transaction_level_tlp_config_tq_tableList')}
              </h5>
              {(total > 0 || searchValue) && (
                <div className="method-list-simple-search">
                  <SimpleSearch
                    defaultValue={searchValue}
                    ref={simpleSearchRef}
                    clearTooltip={t('txt_clear_search_criteria')}
                    onSearch={handleSearch}
                    placeholder={t(
                      'txt_manage_transaction_level_tlp_config_tq_tableList_search_placeholder'
                    )}
                  />
                </div>
              )}
            </div>
            {total > 0 && searchValue && !isEmpty(tables) && (
              <div className="d-flex justify-content-end mb-16 mr-n8">
                <ClearAndResetButton
                  small
                  onClearAndReset={handleClearAndReset}
                />
              </div>
            )}
            {total > 0 && (
              <div className="d-flex justify-content-end align-items-center">
                <div className="d-flex align-items-center">
                  <div className="d-flex align-items-center mr-n8">
                    <strong className="fs-14 color-grey mr-4">Order by:</strong>
                    <DropdownList
                      name="orderBy"
                      value={orderBy}
                      textField="description"
                      onChange={event => {
                        const orderBy = event.target.value;
                        handleChangeOrderBy(orderBy?.value);
                      }}
                      variant="no-border"
                    >
                      {orderByListItems}
                    </DropdownList>
                  </div>
                </div>
              </div>
            )}
          </div>
          {tableList}
          {noDataFound}
          {failedReload}
        </ModalBody>

        <ModalFooter
          cancelButtonText={t('txt_cancel')}
          okButtonText={t('txt_continue')}
          onCancel={handleCloseAndClearState}
          disabledOk={!selectedVersion}
          onOk={handleGoToNextStep()}
        />
      </ModalRegistry>

      {versionDetail && (
        <ViewVersionDetail
          onCancel={handleCloseModalVersionDetail}
          tableId={versionDetail?.tableId}
          onOK={handleGoToNextStep(versionDetail)}
          isTableDetailAQ
          data={versionDetail?.version}
        />
      )}
    </React.Fragment>
  );
};

export default TableListModal;
