import { MethodFieldParameterEnum } from 'app/constants/enums';

export const parameterGroup: {
  id: string;
  businessName: string;
  greenScreenName: string;
  moreInfo: string;
}[] = [
  {
    id: MethodFieldParameterEnum.AllocationDate,
    businessName: 'Allocation Date',
    greenScreenName: 'ALLOC Date',
    moreInfo:
      'The Allocation Date parameter defines the date you want the System to allocate accounts through the client allocation table.'
  },
  {
    id: MethodFieldParameterEnum.AllocationFlag,
    businessName: 'Allocation Flag',
    greenScreenName: 'ALLOC Flag',
    moreInfo:
      'The Allocation Flag parameter determines when you want the System to allocate accounts through the account qualification table.'
  },
  {
    id: MethodFieldParameterEnum.AllocationInterval,
    businessName: 'Allocation Interval',
    greenScreenName: 'ALLOC Interval',
    moreInfo:
      'The Allocation Interval parameter determines the number of months after the account’s Statement Cycle Allocation Date you want the System to allocate accounts through the account qualification table if you set the Allocation Flag field to 2.'
  },
  {
    id: MethodFieldParameterEnum.AllocationBeforeAfterCycle,
    businessName: 'Allocation Before/After Cycle',
    greenScreenName: 'ALLOC B/A Cycle',
    moreInfo:
      'The Allocation Before/After Cycle parameter determines whether the System allocates accounts before or after statement cycle processing.'
  },
  {
    id: MethodFieldParameterEnum.DefaultStrategy,
    businessName: 'Default Strategy',
    greenScreenName: 'Default Strategy',
    moreInfo:
      'The Default Strategy parameter defines the default pricing strategy to be assigned to a new or transferred cardholder account if the account receives the SAME result ID.'
  },
  {
    id: MethodFieldParameterEnum.ChangeInTermsMethodFlag,
    businessName: 'Change In Terms Method Flag',
    greenScreenName: 'CIT Method Flag',
    moreInfo:
      'The Change In Terms Method Flag parameter designates whether you want to associate CIT methods with the pricing strategies assigned during reallocation processing.'
  }
];
