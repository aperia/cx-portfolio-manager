import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import { STATUS } from 'app/_libraries/_dls/components/Upload/helper';
import React from 'react';
import * as reactRedux from 'react-redux';
import ConfigureParameters from '.';
import { TableType } from './constant';
import * as hooks from './useConfigActions';
import { CONFIG_ACTIONS_MODAL, EXPANDED_LIST } from './useConfigActions';

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');
jest.mock('./TableListModal', () => ({
  __esModule: true,
  default: ({ handleClose }: any) => {
    return (
      <div>
        <h1>TableListModal_component</h1>
        <button onClick={handleClose}>Close</button>
      </div>
    );
  }
}));

jest.mock('./AddNewTableModal', () => ({
  __esModule: true,
  default: ({ handleClose }: any) => {
    return (
      <div>
        <h1>AddNewTableModal_component</h1>
        <button onClick={handleClose}>Close</button>
      </div>
    );
  }
}));

jest.mock('./ActionButtons', () => ({
  __esModule: true,
  default: ({
    onClickAddNewTable,
    onClickTableToModel,
    onClickCreateNewVersion
  }: any) => {
    return (
      <div>
        <button onClick={onClickAddNewTable}>Add_New_Table</button>
        <button onClick={onClickTableToModel}>Table_To_Model</button>
        <button onClick={onClickCreateNewVersion}>Create_New_version</button>
      </div>
    );
  }
}));

jest.mock('./RemoveTableModal', () => ({
  __esModule: true,
  default: ({ onOk, onClose }: any) => {
    return (
      <div>
        <h1>RemoveTableModal_component</h1>
        <button onClick={onClose}>Close</button>
        <button onClick={onOk}>Ok</button>
      </div>
    );
  }
}));

jest.mock('./TableList', () => ({
  __esModule: true,
  default: () => {
    return <div>TableList_component</div>;
  }
}));

// jest.mock('./ViewNewVersion', () => ({
//   __esModule: true,
//   default: () => {
//     return <div>ViewNewVersion</div>;
//   }
// }));

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

let confirmFunc: any;
jest.mock('app/hooks/useUnsavedChangeRegistry', () => {
  return {
    __esModule: true,
    useUnsavedChangeRegistry: (options: any, changes: any, onConfirm: any) => {
      confirmFunc = onConfirm;
    }
  };
});

const renderComponent = (props: any) => {
  return render(
    <div>
      <ConfigureParameters {...props} />
      <div onClick={() => confirmFunc()}>ConfirmUnsavedChange</div>
    </div>
  );
};

describe('WorkflowManageTransactionLevelProcessingDecisionTablesTLP > ConfigureParameters > index', () => {
  const mockDispatch = jest.fn();
  const handleOpenConfigModal = jest.fn();
  const handleSetIsCreateNewVersion = jest.fn();
  const handleSetSelectedTable = jest.fn();
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
  });

  const defaultUseConfigReturn = {
    handleOpenConfigModal,
    handleSetIsCreateNewVersion,
    handleSetSelectedTable,
    expandedList: {
      [EXPANDED_LIST.tableListModal]: {},
      [EXPANDED_LIST.tableListIndex]: {}
    }
  };

  const defaultProps = {
    stepId: 'config',
    selectedStep: 'config',
    setFormValues: jest.fn()
  };

  it('Should render Table List ', () => {
    jest.spyOn(hooks, 'useConfigAction').mockImplementation(() => {
      return {
        ...defaultUseConfigReturn,
        tableList: [1, 2]
      } as any;
    });
    const wrapper = renderComponent({
      ...defaultProps,
      formValues: {},
      tableType: TableType.alpaq
    });
    expect(wrapper.getByText('TableList_component')).toBeInTheDocument();
    const btnCreateNewTable = wrapper.getByText('Add_New_Table');
    const btnTableToModel = wrapper.getByText('Table_To_Model');
    const btnCreateNewVersion = wrapper.getByText('Create_New_version');
    userEvent.click(btnCreateNewTable);
    userEvent.click(btnTableToModel);
    userEvent.click(btnCreateNewVersion);
  });

  it('Should render Table List Modal', () => {
    jest.spyOn(hooks, 'useConfigAction').mockImplementation(() => {
      return {
        ...defaultUseConfigReturn,
        openedModal: CONFIG_ACTIONS_MODAL.tableListModal,
        tableList: []
      } as any;
    });
    const wrapper = renderComponent({
      ...defaultProps,
      formValues: {},
      tableType: TableType.alpaq
    });
    expect(wrapper.getByText('TableListModal_component')).toBeInTheDocument();
    const closeBtn = wrapper.getByText('Close');
    userEvent.click(closeBtn);
  });
  it('Should render Add New Table Modal', () => {
    jest.spyOn(hooks, 'useConfigAction').mockImplementation(() => {
      return {
        ...defaultUseConfigReturn,
        tableList: [1, 2],
        openedModal: CONFIG_ACTIONS_MODAL.addNewTableModal
      } as any;
    });
    const wrapper = renderComponent({
      ...defaultProps,
      formValues: {
        tables: [1, 2],
        isValid: true
      },
      tableType: TableType.alpaq
    });
    expect(wrapper.getByText('AddNewTableModal_component')).toBeInTheDocument();
    const closeBtn = wrapper.getByText('Close');
    userEvent.click(closeBtn);
  });
  it('Should render Remove Table Modal', () => {
    jest.spyOn(hooks, 'useConfigAction').mockImplementation(() => {
      return {
        ...defaultUseConfigReturn,
        tableList: [],
        openedModal: CONFIG_ACTIONS_MODAL.deleteTableModal,
        selectedTable: {},
        handleDeleteTable: jest.fn()
      } as any;
    });
    const wrapper = renderComponent({
      ...defaultProps,
      formValues: {},
      tableType: TableType.alpaq
    });
    expect(wrapper.getByText('RemoveTableModal_component')).toBeInTheDocument();
    const closeBtn = wrapper.getByText('Close');
    const okBtn = wrapper.getByText('Ok');
    userEvent.click(closeBtn);
    userEvent.click(okBtn);
  });

  // it('Should render View New version Table Modal', () => {
  //   jest.spyOn(hooks, 'useConfigAction').mockImplementation(() => {
  //     return {
  //       ...defaultUseConfigReturn,
  //       tableList: [1],
  //       openedModal: CONFIG_ACTIONS_MODAL.deleteTableModal,
  //       selectedTable: {},
  //       handleDeleteTable: jest.fn()
  //     } as any;
  //   });
  //   const wrapper = renderComponent({
  //     ...defaultProps,
  //     formValues: {},
  //     tableType: TableType.alpaq
  //   });
  //   //expect(wrapper.getByText('ViewNewVersion')).toBeInTheDocument();
  // });

  it('Should show stuck inline message', () => {
    jest.spyOn(hooks, 'useConfigAction').mockImplementation(() => {
      return {
        ...defaultUseConfigReturn,
        tableList: [1],
        openedModal: CONFIG_ACTIONS_MODAL.deleteTableModal,
        selectedTable: {},
        handleDeleteTable: jest.fn()
      } as any;
    });
    const wrapper = renderComponent({
      ...defaultProps,
      formValues: {},
      isStuck: true,
      tableType: undefined
    });
    expect(
      wrapper.getByText('txt_step_stuck_move_forward_message')
    ).toBeInTheDocument();
  });

  it('Should render with files dependencies', () => {
    jest.spyOn(hooks, 'useConfigAction').mockImplementation(() => {
      return {
        ...defaultUseConfigReturn,
        tableList: [
          {
            tableId: '123',
            elementList: [{ name: '2', rowId: '12' }],
            controlParameters: [
              {
                id: MethodFieldParameterEnum.ChangeInTermsMethodFlag,
                value: '1'
              }
            ]
          }
        ],
        openedModal: CONFIG_ACTIONS_MODAL.deleteTableModal,
        selectedTable: {},
        handleDeleteTable: jest.fn()
      } as any;
    });
    const wrapper = renderComponent({
      ...defaultProps,
      formValues: {
        tables: [
          {
            tableId: '123',
            elementList: [{ name: '2', rowId: '12' }],
            controlParameters: [
              {
                id: MethodFieldParameterEnum.ChangeInTermsMethodFlag,
                value: '1'
              }
            ]
          },
          {
            tableId: '12g',
            elementList: [{ name: '2', rowId: '11' }],
            controlParameters: [
              {
                id: MethodFieldParameterEnum.ChangeInTermsMethodFlag,
                value: '2'
              }
            ]
          }
        ]
      },
      isStuck: true,
      tableType: undefined,
      clearFormValues: jest.fn(),
      dependencies: {
        files: [{ status: STATUS.valid }]
      }
    });
    expect(
      wrapper.getByText('txt_step_stuck_move_forward_message')
    ).toBeInTheDocument();

    const confirmUnSaveChanged = wrapper.getByText('ConfirmUnsavedChange');
    userEvent.click(confirmUnSaveChanged);
  });

  it('Should render with isSelected is false', () => {
    jest.spyOn(hooks, 'useConfigAction').mockImplementation(() => {
      return {
        ...defaultUseConfigReturn,
        tableList: [
          { tableId: '123', elementList: [{ name: '2', rowId: '12' }] },
          { tableId: '123', elementList: [{ name: '3', rowId: '6' }] }
        ],
        openedModal: CONFIG_ACTIONS_MODAL.deleteTableModal,
        selectedTable: {},
        handleDeleteTable: jest.fn(),
        handleSetExpandedList: () => jest.fn()
      } as any;
    });
    renderComponent({
      ...defaultProps,
      stepId: 'config',
      selectedStep: 'config12',
      formValues: {
        tables: [
          { tableId: '123', elementList: [{ name: '2', rowId: '12' }] },
          { tableId: '12g', elementList: [{ name: '2', rowId: '11' }] }
        ]
      },
      isStuck: true,
      tableType: undefined,
      dependencies: {
        files: ['123']
      }
    });
  });
});
