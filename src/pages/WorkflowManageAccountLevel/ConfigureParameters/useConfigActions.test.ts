import { act, renderHook } from '@testing-library/react-hooks';
import * as reactRedux from 'react-redux';
import { TableType } from './constant';
import {
  CONFIG_ACTIONS_MODAL,
  EXPANDED_LIST,
  EXPAND_ACTION,
  useConfigAction
} from './useConfigActions';

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');
const MockSelectedVersion = {
  tableId: '4BYPMV28',
  version: {
    tableId: 'TYB4YPKU',
    tableName: 'E6RMO7RI',
    effectiveDate: '2021-06-23T17:00:00Z',
    status: 'Previous',
    comment: 'magna clita enim dolore nam ea',
    tableControlParameters: [],
    decisionElements: [
      {
        name: 'AVG END BAL LST 03',
        searchCode: 'E',
        immediateAllocation: 'Yes'
      },
      {
        name: 'AGE',
        searchCode: 'R',
        immediateAllocation: 'Yes'
      },
      {
        name: 'APPROVAL OFFICER',
        searchCode: 'R',
        immediateAllocation: 'Yes'
      }
    ]
  }
} as any;
const MockTable = {
  rowId: 'rOUQmbJ_nwjCoY9L6XHgB',
  tableId: '30D66C93',
  tableName: 'Test',
  elementList: [
    {
      name: 'APPLICATION SCORE',
      searchCode: 'E',
      immediateAllocation: 'No',
      value: ['492', '408', '9', '485', '333', '444', '18', '463', '329', '15'],
      rowId: '0Z5TCuvJMjFOZnkZfw_Vs',
      moreInfo:
        'Application Processing System (EAPS) first-pass score, based on the information provided on the new cardholder account application; variable-length, 5 positions, numeric.\nThis score does not include information provided by the credit bureau.',
      configurable: true,
      searchCodeText: 'E - Exact'
    }
  ],
  comment: '',
  actionTaken: 'createNewVersion',
  selectedVersionId: 'I5IIY2P8',
  selectedTableId: '30D66C93',
  selectedVersion: {
    tableId: '30D66C93',
    version: {
      tableId: 'I5IIY2P8',
      tableName: '5885E54F',
      effectiveDate: '2021-08-10T17:00:00Z',
      status: 'Lapsed',
      comment: 'velit dignissim',
      tableControlParameters: [],
      decisionElements: [
        {
          name: 'APPLICATION SCORE',
          searchCode: 'E',
          immediateAllocation: 'Yes'
        },
        {
          name: 'RESULT ID',
          searchCode: '',
          immediateAllocation: 'Yes'
        }
      ]
    }
  }
} as any;

describe('WorkflowManageTransactionLevelProcessingDecisionTablesTLP > ConfigureParameters > useConfigActions', () => {
  const mockDispatch = jest.fn();
  const tables = [MockTable] as any[];
  const tableTypes = TableType.tlptq;
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
  });
  it('Should render Use Config Hook ', () => {
    const { result } = renderHook(() => useConfigAction(tables, tableTypes));

    act(() => {
      result.current.handleOpenConfigModal(
        CONFIG_ACTIONS_MODAL.addNewTableModal
      );
    });
    expect(result.current.openedModal).toEqual(
      CONFIG_ACTIONS_MODAL.addNewTableModal
    );

    act(() => {
      result.current.handleSetExpandedList(EXPANDED_LIST.tableListIndex)({
        expandAction: EXPAND_ACTION.replace,
        expandedList: { '1': true }
      });
      result.current.handleSetExpandedList(EXPANDED_LIST.tableListModal)({
        expandAction: EXPAND_ACTION.toggle,
        expandKey: '1'
      });
    });
    expect(
      result.current.expandedList[EXPANDED_LIST.tableListModal]
    ).toHaveProperty('1');
    expect(
      result.current.expandedList[EXPANDED_LIST.tableListIndex]
    ).toHaveProperty('1');

    act(() => {
      result.current.handleSetExpandedList(EXPANDED_LIST.tableListModal)({
        expandAction: EXPAND_ACTION.toggle,
        expandKey: '1'
      });
      result.current.handleSetExpandedList(EXPANDED_LIST.tableListIndex)({
        expandAction: EXPAND_ACTION.replace
      });
    });
    expect(
      result.current.expandedList[EXPANDED_LIST.tableListModal]
    ).not.toHaveProperty('1');
    expect(
      result.current.expandedList[EXPANDED_LIST.tableListIndex]
    ).not.toHaveProperty('1');

    act(() => {
      result.current.handleSetIsCreateNewVersion(true);
    });
    expect(result.current.isCreateNewVersion).toEqual(true);
  });

  it('Add Table Flow', () => {
    const { result } = renderHook(() => useConfigAction([], tableTypes));
    act(() => {
      result.current.handleSetSelectedVersion(MockSelectedVersion);
    });
    expect(result.current.selectedVersion).toHaveProperty('tableId');

    act(() => {
      result.current.handleAddNewTable(MockTable);
    });
    expect(result.current.tableList).toHaveLength(1);

    act(() => {
      result.current.handleEditTable({ ...MockTable, comment: 'edit table' });
    });
    expect(result.current.tableList[0].comment).toEqual('edit table');

    act(() => {
      result.current.handleSetDraftNewTable({ tableId: '1' } as any);
    });
    expect(result.current.draftNewTable?.tableId).toEqual('1');

    let isDuplicateFalse;
    let isDuplicateTrue;
    act(() => {
      isDuplicateFalse = result.current.handleCheckDuplicateTableId('12');
      isDuplicateTrue = result.current.handleCheckDuplicateTableId('30D66C93');
    });
    expect(isDuplicateFalse).toEqual(false);
    expect(isDuplicateTrue).toEqual(true);

    act(() => {
      result.current.handleSetSelectedTable(result.current.tableList[0]);
    });
    expect(result.current.selectedTable?.tableId).toEqual('30D66C93');

    act(() => {
      result.current.handleDeleteTable(result.current.tableList[0].rowId);
    });
    expect(result.current.tableList).toHaveLength(0);
  });

  it('Run Default reducer function', () => {
    const { result } = renderHook(() => useConfigAction([], tableTypes));
    let isDuplicate;
    act(() => {
      isDuplicate = result.current.handleCheckDuplicateTableId('12');
    });
    expect(isDuplicate).toEqual(false);

    act(() => {
      result.current.dispatch({ type: 'Test Action' });
    });
  });
});
