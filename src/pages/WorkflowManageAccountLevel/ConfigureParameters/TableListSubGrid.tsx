import { MethodFieldParameterEnum } from 'app/constants/enums';
import { formatCommon } from 'app/helpers';
import {
  ColumnType,
  Grid,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import isEmpty from 'lodash.isempty';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import { useSelectElementMetadataManageAcountLevel } from 'pages/_commons/redux/WorkflowSetup';
import { formatText, viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React from 'react';

interface ITableListSubGrid {
  elementList: any[];
  controlParameters: any[];
}

const TableListSubGrid: React.FC<ITableListSubGrid> = ({
  elementList = [],
  controlParameters = []
}) => {
  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();
  const metadata = useSelectElementMetadataManageAcountLevel();
  const {
    allocationBeforeAfterCycle,
    allocationFlag,
    changeInTermsMethodFlag
  } = metadata;
  const columnsElementList: ColumnType[] = [
    {
      id: 'name',
      Header: t('txt_manage_transaction_level_tlp_decision_element'),
      cellBodyProps: { className: 'pt-12 pb-8' },
      accessor: formatText(['name']),
      width: width > 1280 ? undefined : 280
    },
    {
      id: 'searchCode',
      Header: t('txt_manage_transaction_level_tlp_config_tq_search_code'),
      cellBodyProps: { className: 'pt-12 pb-8' },
      accessor: formatText(['searchCodeText']),
      width: width > 1280 ? 240 : 210
    },
    {
      id: 'immediateAllocation',
      Header: t('txt_manage_account_level_table_decision_immediate_allocation'),
      width: 182,
      accessor: ({ immediateAllocation }) =>
        ['Yes', 'Y'].includes(immediateAllocation) ? 'Y' : 'N'
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      accessor: viewMoreInfo(['moreInfo'], t),
      cellBodyProps: { className: 'pt-12 pb-8' },
      width: 106
    }
  ];

  const columnsControlParameters: ColumnType[] = [
    {
      id: 'businessName',
      Header: t('txt_manage_account_level_table_decision_business_name'),
      accessor: data => t(data.businessName),
      cellBodyProps: { className: 'pt-12 pb-8' },
      width: 233
    },
    {
      id: 'greenScreenName',
      Header: t('txt_manage_account_level_table_decision_green_name'),
      accessor: data => t(data.greenScreenName),
      cellBodyProps: { className: 'pt-12 pb-8' },
      width: 244
    },
    {
      id: 'value',
      Header: t('txt_manage_account_level_table_decision_value'),
      width: width < 1280 ? 320 : 360,
      cellBodyProps: { className: 'pt-12 pb-8' },
      accessor: (data, indx) => {
        if (data?.id === MethodFieldParameterEnum.AllocationDate) {
          return formatCommon(data?.value as any).time.date;
        }

        if (data?.id === MethodFieldParameterEnum.AllocationFlag) {
          const valTemp = allocationFlag.filter(
            item => item?.code === data?.value
          )[0];
          const flag = valTemp?.code === '' ? valTemp?.code : valTemp?.text;
          return (
            <TruncateText
              resizable
              lines={2}
              ellipsisLessText={t('txt_less')}
              ellipsisMoreText={t('txt_more')}
            >
              {flag}
            </TruncateText>
          );
        }

        if (data?.id === MethodFieldParameterEnum.AllocationBeforeAfterCycle) {
          const beforeAfter =
            isEmpty(data?.value) && isEmpty(data?.valueText)
              ? allocationBeforeAfterCycle.find(option => option?.code === 'B')
              : allocationBeforeAfterCycle.find(
                  option => option?.code === data?.value
                );
          const before =
            beforeAfter?.code === '' ? beforeAfter?.code : beforeAfter?.text;
          return (
            <TruncateText
              resizable
              lines={2}
              ellipsisLessText={t('txt_less')}
              ellipsisMoreText={t('txt_more')}
            >
              {before}
            </TruncateText>
          );
        }

        if (data?.id === MethodFieldParameterEnum.ChangeInTermsMethodFlag) {
          const changeInTerms =
            isEmpty(data?.value) && isEmpty(data?.valueText)
              ? changeInTermsMethodFlag.find(option => option?.code === 'N')
              : changeInTermsMethodFlag.find(
                  option => option?.code === data?.value
                );
          const changeInTermMethod =
            changeInTerms?.code === ''
              ? changeInTerms?.code
              : changeInTerms?.text;
          return (
            <TruncateText
              resizable
              lines={2}
              ellipsisLessText={t('txt_less')}
              ellipsisMoreText={t('txt_more')}
            >
              {changeInTermMethod}
            </TruncateText>
          );
        }

        return data?.value;
      }
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      width: 106,
      cellBodyProps: { className: 'pt-12 pb-8' },
      accessor: viewMoreInfo(['moreInfo'], t)
    }
  ];

  return (
    <div className="p-20">
      {!isEmpty(controlParameters) && (
        <div className="mb-24">
          <h5 className="mb-16 fs-14">
            {t('txt_manage_account_level_table_parameters')}
          </h5>
          <Grid columns={columnsControlParameters} data={controlParameters} />
        </div>
      )}
      <div>
        <h5 className="mb-16 fs-14">
          {t('txt_manage_transaction_level_tlp_element_list')}
        </h5>
        <Grid columns={columnsElementList} data={elementList} />
      </div>
    </div>
  );
};

export default TableListSubGrid;
