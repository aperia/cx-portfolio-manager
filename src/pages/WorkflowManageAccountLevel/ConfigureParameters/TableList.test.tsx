import { fireEvent, render, RenderResult } from '@testing-library/react';
import 'app/utils/_mockComponent/mockPaging';
import * as CommonSelectHooks from 'pages/_commons/redux/Common/select-hooks';
import React from 'react';
import TableList from './TableList';

const mockUseSelectWindowDimension = jest.spyOn(
  CommonSelectHooks,
  'useSelectWindowDimension'
);

jest.mock('./TableListSubGrid', () => ({
  __esModule: true,
  default: () => <div>TableList_SubRowGrid_component</div>
}));

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const MockTableList = [
  {
    rowId: 'gTHW7hyGq2ubw826Kkdka',
    tableId: '1',
    tableName: 'dsadas',
    elementList: [
      {
        name: 'ACS PORT CHG DATE',
        isNewElement: false,
        rowId: '-HsGx8-Xdn1RvNChAwnL4',
        searchCodeText: 'R - Range',
        moreInfo:
          'Date (MMDDYYYY) the current Adaptive Control System portfolio identification was last changed; variable-length, 10 positions, numeric.\n Example: 031404 or 03142004 = March 14, 2004',
        immediateAllocation: 'No',
        configurable: true,
        searchCode: 'R'
      }
    ],
    comment: '',
    actionTaken: 'createNewTable',
    selectedVersionId: '',
    selectedTableId: ''
  },
  {
    rowId: '84j6GYkMJrJWo983pyRsZ',
    tableId: '30D66C93',
    tableName: '2',
    elementList: [
      {
        name: 'APPLICATION SCORE',
        searchCode: 'E',
        immediateAllocation: 'No',
        value: [],
        rowId: '152oCY_2UT3c5vJ8NUGoj',
        moreInfo:
          'Application Processing System (EAPS) first-pass score, based on the information provided on the new cardholder account application; variable-length, 5 positions, numeric.\nThis score does not include information provided by the credit bureau.',
        configurable: true,
        searchCodeText: 'E - Exact'
      }
    ],
    comment: 'dsada',
    actionTaken: 'createNewVersion',
    selectedVersionId: 'I5IIY2P8',
    selectedTableId: '30D66C93',
    selectedVersion: {
      tableId: '30D66C93',
      version: {
        tableId: 'I5IIY2P8',
        tableName: '5885E54F',
        effectiveDate: '2021-08-10T17:00:00Z',
        status: 'Lapsed',
        comment: 'velit dignissim',
        tableControlParameters: [],
        decisionElements: [
          {
            name: 'APPLICATION SCORE',
            searchCode: 'E',
            immediateAllocation: 'Yes',
            value: []
          },
          {
            name: 'RESULT ID',
            searchCode: '',
            immediateAllocation: 'Yes',
            value: []
          }
        ]
      }
    }
  },
  {
    rowId: 'l0LqFVvrRYpK0zEZyXPeI',
    tableId: '3',
    tableName: '3',
    elementList: [
      {
        name: 'ACS CURR PORT',
        searchCode: 'R',
        immediateAllocation: 'No',
        value: [],
        rowId: 'xFm-eevbGML6GibNfJJIe',
        moreInfo:
          'Adaptive Control System portfolio currently in effect for the customer account; variable-length, 4 positions, numeric.',
        configurable: true,
        searchCodeText: 'R - Range'
      },
      {
        name: 'BEHAVIOR SCORE',
        searchCode: 'R',
        immediateAllocation: 'No',
        value: [],
        rowId: 'j-lL5fulwe-BZdmY3fFr0',
        moreInfo:
          'Number representing the likelihood that an account will go three or more cycles delinquent in the next six months; variable-length, 3 positions, numeric.',
        configurable: true,
        searchCodeText: 'R - Range'
      },
      {
        name: 'ACCT OPEN DAYS',
        searchCode: 'R',
        immediateAllocation: 'No',
        value: [],
        rowId: '4b96GOgOrw2pl03HVRIbS',
        moreInfo: null,
        configurable: true,
        searchCodeText: 'R - Range'
      },
      {
        name: 'ADVERTISING GROUP',
        searchCode: 'E',
        immediateAllocation: 'No',
        value: ['71', '9', '226', '372', '50', '413', '220', '12', '247', '4'],
        rowId: 'OcF52v_qpcR1q7C7Ujn-9',
        moreInfo:
          'Issuer-defined advertising group code; variable-length, 3 positions, numeric.',
        configurable: true,
        searchCodeText: 'E - Exact'
      },
      {
        name: 'ATTRITION INDEX',
        searchCode: 'E',
        immediateAllocation: 'No',
        value: [],
        rowId: 'zXnt1PRww-zC2D89zVhgB',
        moreInfo:
          'Issuer-defined identifier of the attrition index; variable-length, 3 positions, numeric.',
        configurable: true,
        searchCodeText: 'E - Exact'
      },
      {
        name: 'AGE',
        searchCode: 'E',
        immediateAllocation: 'No',
        value: [],
        rowId: 'hwtUCM33nRDHkpU6kt776',
        moreInfo:
          'Principal cardholder’s age expressed in whole number of years; variable-length, 3 positions, numeric.\n To determine whether a cardholder account matches a value, the System subtracts the year of birth from the current year. The System adds one year if the month of birth has already passed. If the date of birth consists of zeros, the age is 0.',
        configurable: true,
        searchCodeText: 'E - Exact'
      },
      {
        name: 'ANNUAL CHG DATE',
        searchCode: 'R',
        immediateAllocation: 'No',
        value: [],
        rowId: 'Io4e8inLzDXU2XSDkt1pB',
        moreInfo:
          'Next date (MMYYYY) that a cardholder account will be reviewed and assessed an annual charge; fixed-length, 6 positions, numeric.',
        configurable: true,
        searchCodeText: 'R - Range'
      },
      {
        name: 'AVG END BAL LST 12',
        searchCode: 'E',
        immediateAllocation: 'No',
        value: [],
        rowId: 'j0OIbGy0EA6bwkCpycwC6',
        moreInfo:
          'Dollar-and-cent amount of the customer account’s average ending balance for the last 12 cycles; variable-length, 15 positions, numeric.\nExample: 100019 = $1,000.19\nTo determine whether a customer account matches a value, the System totals the ending balances for the last 12 cycles and divides the result by 12. The result is the correct average if the customer account has cycled 12 times in the Fiserv System.',
        configurable: true,
        searchCodeText: 'E - Exact'
      },
      {
        name: 'AVG END BAL LST 03',
        searchCode: 'R',
        immediateAllocation: 'No',
        value: [],
        rowId: 'hjdIZEC4txjija1BL-3-e',
        moreInfo:
          'Dollar-and-cent amount of the customer account’s average ending balance for the last three cycles; variable-length, 15 positions, numeric.\nExample: 100019 = $1,000.19\nTo determine whether a customer account matches a value, the System totals the ending balances for the last three cycles and divides the result by three. The result is the correct average if the customer account has cycled three times in the Fiserv System.',
        configurable: true,
        searchCodeText: 'R - Range'
      },
      {
        name: 'ACQUIRING MC ICA',
        searchCode: 'E',
        immediateAllocation: 'No',
        value: ['3', '487', '258', '240', '18', '11', '32', '370', '385', '74'],
        rowId: 'btEIyNaHxZQMJTgwO5M7I',
        moreInfo: null,
        configurable: true,
        searchCodeText: 'E - Exact'
      }
    ],
    comment: 'dsadas',
    actionTaken: 'addTableToModel',
    selectedVersionId: 'VSJQCLTR',
    selectedTableId: '30D66C93',
    selectedVersion: {
      tableId: '30D66C93',
      version: {
        tableId: 'VSJQCLTR',
        tableName: 'J45SZ6B6',
        effectiveDate: '2021-04-20T17:00:00Z',
        status: 'Scheduled',
        comment: 'tempor dolor',
        tableControlParameters: [],
        decisionElements: [
          {
            name: 'ACS CURR PORT',
            searchCode: 'R',
            immediateAllocation: 'Yes',
            value: []
          },
          {
            name: 'BEHAVIOR SCORE',
            searchCode: 'R',
            immediateAllocation: 'Yes',
            value: []
          },
          {
            name: 'ACCT OPEN DAYS',
            searchCode: 'R',
            immediateAllocation: 'Yes',
            value: []
          },
          {
            name: 'ADVERTISING GROUP',
            searchCode: 'E',
            immediateAllocation: 'Yes',
            value: []
          },
          {
            name: 'ATTRITION INDEX',
            searchCode: 'E',
            immediateAllocation: 'Yes',
            value: []
          },
          {
            name: 'AGE',
            searchCode: 'E',
            immediateAllocation: 'Yes',
            value: []
          },
          {
            name: 'ANNUAL CHG DATE',
            searchCode: 'R',
            immediateAllocation: 'Yes',
            value: []
          },
          {
            name: 'AVG END BAL LST 12',
            searchCode: 'E',
            immediateAllocation: 'Yes',
            value: []
          },
          {
            name: 'AVG END BAL LST 03',
            searchCode: 'R',
            immediateAllocation: 'Yes',
            value: []
          },
          {
            name: 'ACQUIRING MC ICA',
            searchCode: 'E',
            immediateAllocation: 'Yes',
            value: []
          },
          {
            name: 'RESULT ID',
            searchCode: '',
            immediateAllocation: 'Yes',
            value: []
          }
        ]
      }
    }
  },
  {
    rowId: 'MxLtWDrMqRMr3uMgZwzlT',
    tableId: '4',
    tableName: '4',
    elementList: [
      {
        name: 'ALT FIN RPT BRCH/P',
        isNewElement: false,
        rowId: 'jq7hsw1ReDM6DNaE3QRMU',
        searchCodeText: 'E - Exact',
        moreInfo:
          'Issuer-defined alternate financial reporting branch/portfolio field; fixed-length, 5 positions, numeric.',
        immediateAllocation: 'No',
        configurable: true,
        searchCode: 'E'
      }
    ],
    comment: '',
    actionTaken: 'createNewTable',
    selectedVersionId: '',
    selectedTableId: '',
    selectedVersion: null
  },
  {
    rowId: 'sRGC9hflC7uLYYpY7Gpnv',
    tableId: '5',
    tableName: '5',
    elementList: [
      {
        name: 'ADVERTISING GROUP',
        isNewElement: false,
        rowId: 'PXynXA8ES1u06QmRwqReJ',
        searchCodeText: 'R - Range',
        moreInfo:
          'Issuer-defined advertising group code; variable-length, 3 positions, numeric.',
        immediateAllocation: 'No',
        configurable: true,
        searchCode: 'R'
      }
    ],
    comment: '',
    actionTaken: 'createNewTable',
    selectedVersionId: '',
    selectedTableId: '',
    selectedVersion: null
  },
  {
    rowId: 'FLuu2Uike6m_-D2Kk1CLU',
    tableId: '6',
    tableName: '6',
    elementList: [
      {
        name: 'AGE',
        isNewElement: false,
        rowId: '0hTIoIf-0vK9f3eCygeu2',
        searchCodeText: 'R - Range',
        moreInfo:
          'Principal cardholder’s age expressed in whole number of years; variable-length, 3 positions, numeric.\n To determine whether a cardholder account matches a value, the System subtracts the year of birth from the current year. The System adds one year if the month of birth has already passed. If the date of birth consists of zeros, the age is 0.',
        immediateAllocation: 'No',
        configurable: true,
        searchCode: 'R'
      }
    ],
    comment: 'dsadsa',
    actionTaken: 'createNewTable',
    selectedVersionId: '',
    selectedTableId: '',
    selectedVersion: null
  },
  {
    rowId: 'mZnE9oVKlnJsRg6OmL7qJ',
    tableId: '7',
    tableName: '7',
    elementList: [
      {
        name: 'ADVERTISING GROUP',
        isNewElement: false,
        rowId: 'MRJjJHDEroO2jZNTtg6hs',
        searchCodeText: 'R - Range',
        moreInfo:
          'Issuer-defined advertising group code; variable-length, 3 positions, numeric.',
        immediateAllocation: 'No',
        configurable: true,
        searchCode: 'R'
      }
    ],
    comment: 'dsadsa',
    actionTaken: 'createNewTable',
    selectedVersionId: '',
    selectedTableId: '',
    selectedVersion: null
  },
  {
    rowId: 'wjULM3uigGe_M3wQglXEY',
    tableId: '8',
    tableName: '8',
    elementList: [
      {
        name: 'ADVERTISING GROUP',
        isNewElement: false,
        rowId: '-VtTeHqmINvW7cvQ2Bo5-',
        searchCodeText: 'R - Range',
        moreInfo:
          'Issuer-defined advertising group code; variable-length, 3 positions, numeric.',
        immediateAllocation: 'No',
        configurable: true,
        searchCode: 'R'
      }
    ],
    comment: 'sadsadas',
    actionTaken: 'createNewTable',
    selectedVersionId: '',
    selectedTableId: '',
    selectedVersion: null
  },
  {
    rowId: '5YThH6o-JHRbrU7uG-n_C',
    tableId: '9',
    tableName: '9',
    elementList: [
      {
        name: 'ADVERTISING GROUP',
        isNewElement: false,
        rowId: '9mhMG3f-7fbpLCApEW-3A',
        searchCodeText: 'R - Range',
        moreInfo:
          'Issuer-defined advertising group code; variable-length, 3 positions, numeric.',
        immediateAllocation: 'No',
        configurable: true,
        searchCode: 'R'
      }
    ],
    comment: 'dsadsadsa',
    actionTaken: 'createNewTable',
    selectedVersionId: '',
    selectedTableId: '',
    selectedVersion: null
  },
  {
    rowId: '6C732qqSD-nSKt_oNpkFz',
    tableId: 'ewqewq',
    tableName: 'sadsa',
    elementList: [
      {
        name: 'AGE',
        isNewElement: false,
        rowId: 'aAtB_eGie4nfW71VMvNFz',
        searchCodeText: 'R - Range',
        moreInfo:
          'Principal cardholder’s age expressed in whole number of years; variable-length, 3 positions, numeric.\n To determine whether a cardholder account matches a value, the System subtracts the year of birth from the current year. The System adds one year if the month of birth has already passed. If the date of birth consists of zeros, the age is 0.',
        immediateAllocation: 'No',
        configurable: true,
        searchCode: 'R'
      }
    ],
    comment: '',
    actionTaken: 'createNewTable',
    selectedVersionId: '',
    selectedTableId: '',
    selectedVersion: null
  },
  {
    rowId: 'U_7PTpR6yBbgUQbhqXod7',
    tableId: 'sadsa',
    tableName: 'dsada',
    elementList: [
      {
        name: 'ACS PORT CHG DATE',
        isNewElement: false,
        rowId: 'aWGflJw1Gte0i4PQwl8yL',
        searchCodeText: 'R - Range',
        moreInfo:
          'Date (MMDDYYYY) the current Adaptive Control System portfolio identification was last changed; variable-length, 10 positions, numeric.\n Example: 031404 or 03142004 = March 14, 2004',
        immediateAllocation: 'No',
        configurable: true,
        searchCode: 'R'
      }
    ],
    comment: '',
    actionTaken: 'createNewTable',
    selectedVersionId: '',
    selectedTableId: '',
    selectedVersion: null
  }
] as any[];

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <TableList {...props} />
    </div>
  );
};

describe('WorkflowManageTransactionLevelProcessingDecisionTablesTLP > ConfigureParametersTQ > TableList', () => {
  it('Should render Table List and show edit modal', () => {
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1920,
      height: 1080
    }));
    const handleSetSelectedTable = jest.fn();
    const handleOpenConfigModal = jest.fn();
    const handleSetExpandedList = jest.fn(() => jest.fn());
    let tableList = [...MockTableList];
    const wrapper = renderComponent({
      tableList,
      handleSetSelectedTable,
      handleOpenConfigModal,
      handleSetExpandedList,
      expandedList: {}
    });
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    expect(body?.children.length).toEqual(10);
    //SHOW Add new method modal
    const expandBtn = body?.children[1].querySelector(
      'button[class="btn btn-icon-secondary btn-sm"]'
    ) as Element;
    fireEvent.click(expandBtn);
    tableList = [
      ...tableList,
      {
        rowId: 'U_7PTpR6yBbgUQbhqXdsadsa',
        tableId: 'sadsa',
        tableName: 'dsada',
        elementList: [
          {
            name: 'ACS PORT CHG DATE',
            isNewElement: false,
            rowId: 'aWGflJw1Gte0i4PQwl8yL',
            searchCodeText: 'R - Range',
            moreInfo:
              'Date (MMDDYYYY) the current Adaptive Control System portfolio identification was last changed; variable-length, 10 positions, numeric.\n Example: 031404 or 03142004 = March 14, 2004',
            immediateAllocation: 'No',
            configurable: true,
            searchCode: 'R'
          }
        ],
        comment: '',
        actionTaken: 'createNewTable',
        selectedVersionId: '',
        selectedTableId: '',
        selectedVersion: null
      }
    ];
    wrapper.rerender(
      <TableList
        {...{
          tableList,
          handleSetSelectedTable,
          handleOpenConfigModal,
          handleSetExpandedList,
          expandedList: {},
          tableIndexChanged: null
        }}
      />
    );
  });

  it('should render Table List and call remove function', () => {
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1200,
      height: 1080
    }));
    const handleSetSelectedTable = jest.fn();
    const handleOpenConfigModal = jest.fn();
    const handleSetExpandedList = jest.fn();
    const wrapper = renderComponent({
      tableList: MockTableList,
      handleSetSelectedTable,
      handleOpenConfigModal,
      handleSetExpandedList,
      expandedList: { '84j6GYkMJrJWo983pyRsZ': true }
    });
    const btnDeleteList = wrapper.getAllByText('txt_delete');
    fireEvent.click(btnDeleteList[0]);
    const btnEditList = wrapper.getAllByText('txt_edit');
    fireEvent.click(btnEditList[0]);
    fireEvent.click(
      wrapper.baseElement.querySelector(
        'button[class="change-page-number"]'
      ) as Element
    );

    fireEvent.click(
      wrapper.baseElement.querySelector(
        'button[class="change-page-size"]'
      ) as Element
    );
  });

  it('Should render Table List SubGrid ', () => {
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1920,
      height: 1080
    }));
    const handleSetSelectedTable = jest.fn();
    const handleOpenConfigModal = jest.fn();
    const handleSetExpandedList = jest.fn();
    const { rerender } = render(
      <TableList
        tableList={MockTableList}
        handleSetSelectedTable={handleSetSelectedTable}
        handleOpenConfigModal={handleOpenConfigModal}
        handleSetExpandedList={handleSetExpandedList}
        expandedList={{}}
      />
    );

    rerender(
      <TableList
        tableList={[
          ...MockTableList,
          {
            rowId: 'U_7PTpR6yBbgUQbhqXdcxcxcx',
            tableId: 'sadsa',
            tableName: 'dsada',
            elementList: [
              {
                name: 'ACS PORT CHG DATE',
                isNewElement: false,
                rowId: 'aWGflJw1Gte0i4PQwl8yL',
                searchCodeText: 'R - Range',
                moreInfo:
                  'Date (MMDDYYYY) the current Adaptive Control System portfolio identification was last changed; variable-length, 10 positions, numeric.\n Example: 031404 or 03142004 = March 14, 2004',
                immediateAllocation: 'No',
                configurable: true,
                searchCode: 'R'
              }
            ],
            comment: '',
            actionTaken: 'createNewTable',
            selectedVersionId: '',
            selectedTableId: '',
            selectedVersion: null
          }
        ]}
        handleSetSelectedTable={handleSetSelectedTable}
        handleOpenConfigModal={handleOpenConfigModal}
        handleSetExpandedList={handleSetExpandedList}
        expandedList={{}}
      />
    );
  });
});
