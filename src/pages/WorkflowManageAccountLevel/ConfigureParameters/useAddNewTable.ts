import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { stringValidate } from 'app/helpers';
import { useFormValidations, useUnsavedChangesRedirect } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls';
import isEmpty from 'lodash.isempty';
import isEqual from 'lodash.isequal';
import { nanoid } from 'nanoid';
import { useCallback, useMemo, useReducer } from 'react';
import { AddNewTableAction, AddNewTableModalProps } from './AddNewTableModal';
import { getControlParameters, TableType } from './constant';
import {
  CONFIG_ACTIONS_MODAL,
  EXPANDED_LIST,
  EXPAND_ACTION
} from './useConfigActions';

interface NewTableState {
  selectedTableId: string;
  tableId: string;
  tableName: string;
  comment: string;
  elementList: any[];
  isElementListHasError?: boolean;
  selectedVersionId: string;
  isDuplicateTableId: boolean;
  controlParameters: any[];
}

export enum FIELD_NAME {
  tableId = 'tableId',
  tableName = 'tableName',
  comment = 'comment'
}

const NEW_TABLE_INITIAL_STATE = {
  selectedTableId: '',
  tableId: '',
  tableName: '',
  comment: '',
  elementList: [],
  selectedVersionId: '',
  isDuplicateTableId: false,
  controlParameters: []
};

const ACTION_TYPES = {
  CHANGE_FORM_VALUE: 'CHANGE_FORM_VALUE',
  SET_ELEMENT_LIST: 'SET_ELEMENT_LIST',
  SET_ELEMENT_LIST_HAS_ERROR: 'SET_ELEMENT_LIST_HAS_ERROR',
  SET_IS_DUPLICATE_TABLE_ID: 'SET_IS_DUPLICATE_TABLE_ID',
  SET_CONTROL_PARAMETERS: 'SET_CONTROL_PARAMETERS'
};

const newTableReducer = (state: NewTableState, action: any) => {
  switch (action.type) {
    case ACTION_TYPES.CHANGE_FORM_VALUE:
      return { ...state, [action.field]: action.value };
    case ACTION_TYPES.SET_ELEMENT_LIST:
      return { ...state, elementList: action.elementList };
    case ACTION_TYPES.SET_ELEMENT_LIST_HAS_ERROR:
      return { ...state, isElementListHasError: action.isElementListHasError };
    case ACTION_TYPES.SET_IS_DUPLICATE_TABLE_ID:
      return { ...state, isDuplicateTableId: action.isDuplicateTableId };
    case ACTION_TYPES.SET_CONTROL_PARAMETERS:
      return { ...state, controlParameters: action.controlParameters };
    default:
      return state;
  }
};

export const useAddNewTable = (props: AddNewTableModalProps) => {
  const {
    handleClose,
    handleOpenConfigModal,
    handleSelectVersion,
    draftNewTable,
    handleSetDraftNewTable,
    selectedVersion,
    isCreateNewVersion,
    handleAddNewTable,
    handleSetIsCreateNewVersion,
    selectedTable,
    handleEditTable,
    handleSetSelectedTable,
    handleSetExpandedList,
    handleCheckDuplicateTableId,
    tableType
  } = props;

  const initialState = useMemo(() => {
    if (selectedTable && isEmpty(draftNewTable)) {
      return { ...NEW_TABLE_INITIAL_STATE, ...selectedTable };
    }
    if (!selectedTable && isEmpty(draftNewTable)) {
      return {
        ...NEW_TABLE_INITIAL_STATE,
        elementList: [{ name: '', isNewElement: true, rowId: nanoid() }],
        controlParameters: getControlParameters(tableType)
      };
    }
    return { ...NEW_TABLE_INITIAL_STATE, ...draftNewTable };
  }, [selectedTable, draftNewTable, tableType]);

  const redirect = useUnsavedChangesRedirect();

  const [state, reactDispatch] = useReducer(newTableReducer, initialState);
  const { t } = useTranslation();

  const {
    tableName,
    tableId,
    comment,
    elementList,
    selectedTableId,
    selectedVersionId,
    isElementListHasError,
    isDuplicateTableId,
    controlParameters
  } = state;

  const currentErrors = useMemo(
    () =>
      ({
        tableName: !tableName && {
          status: true,
          message: t('txt_required_validation', {
            fieldName: 'Table Name'
          })
        },
        tableId: !isCreateNewVersion &&
          !tableId && {
            status: true,
            message: t('txt_required_validation', {
              fieldName: 'Table ID'
            })
          }
      } as Record<string, IFormError>),
    [t, tableId, tableName, isCreateNewVersion]
  );

  const [touches, errors, setTouched] = useFormValidations(currentErrors);

  const actionType = useMemo(() => {
    if (selectedTable) return selectedTable.actionTaken;
    if (!selectedVersion) return AddNewTableAction.createNewTable;
    if (selectedVersion && isCreateNewVersion)
      return AddNewTableAction.createNewVersion;
    return AddNewTableAction.addTableToModel;
  }, [isCreateNewVersion, selectedVersion, selectedTable]);

  const isShowControlParameters = useMemo(() => {
    return tableType === TableType.alpaq;
  }, [tableType]);

  const isFormChanging = useMemo(() => {
    return (
      elementList?.length > 2 &&
      !isEqual(
        {
          tableId,
          comment,
          tableName,
          elementList
        },
        {
          tableId: initialState?.tableId,
          comment: initialState?.comment,
          tableName: initialState?.tableName,
          elementList: initialState?.elementList
        }
      )
    );
  }, [tableName, elementList, comment, tableId, initialState]);

  const handleBlurField = (fieldName: FIELD_NAME) => () => {
    setTouched(fieldName, true)();
  };

  const handleChangeFieldValue = (field: FIELD_NAME) => (e: any) => {
    let value: string = e.target?.value || '';
    if (field === FIELD_NAME.tableId) {
      if (!stringValidate(value).isAlphanumeric()) return;
    }
    if (field === FIELD_NAME.tableId || field === FIELD_NAME.tableName) {
      value = value.toUpperCase();
    }
    reactDispatch({
      type: ACTION_TYPES.CHANGE_FORM_VALUE,
      field,
      value
    });
  };

  const handleSetElementList = useCallback((elementList: any[]) => {
    reactDispatch({
      type: ACTION_TYPES.SET_ELEMENT_LIST,
      elementList
    });
  }, []);

  const handleSetElementListHasError = useCallback(
    (isElementListHasError: boolean) => {
      reactDispatch({
        type: ACTION_TYPES.SET_ELEMENT_LIST_HAS_ERROR,
        isElementListHasError
      });
    },
    []
  );

  const handleSetIsDuplicateTableId = useCallback(
    (isDuplicateTableId: boolean) => {
      reactDispatch({
        type: ACTION_TYPES.SET_IS_DUPLICATE_TABLE_ID,
        isDuplicateTableId
      });
    },
    []
  );

  const handleSetControlParameters = useCallback((controlParameters: any[]) => {
    reactDispatch({
      type: ACTION_TYPES.SET_CONTROL_PARAMETERS,
      controlParameters
    });
  }, []);

  const handleGoBackToPreviousStep = () => {
    handleOpenConfigModal(CONFIG_ACTIONS_MODAL.tableListModal);
    handleSetDraftNewTable({
      comment,
      tableId,
      tableName,
      selectedTableId,
      elementList,
      selectedVersionId,
      controlParameters
    });
    if (selectedTable && !selectedVersion) {
      handleSelectVersion(selectedTable?.selectedVersion);
    }
  };

  const handleCloseAndClearState = () => {
    handleClose();
    handleSelectVersion(null);
    handleSetIsCreateNewVersion(false);
    handleSetDraftNewTable(null);
    handleSetSelectedTable(null);
  };

  const handleCloseModal = () => {
    redirect({
      onConfirm: () => handleCloseAndClearState(),
      formsWatcher: [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_TRANSACTION_LEVEL_PROCESSING_DECISION_TABLES_TLP__CONFIG_TQ_DETAILS
      ]
    });
  };

  const handleCreateNewTable = () => {
    if (
      tableId !== selectedTable?.tableId &&
      handleCheckDuplicateTableId(tableId)
    ) {
      handleSetIsDuplicateTableId(true);
      return;
    }
    const tableItem = {
      rowId: selectedTable ? selectedTable.rowId : nanoid(),
      tableId:
        actionType === AddNewTableAction.createNewVersion
          ? selectedTableId
          : tableId,
      tableName,
      elementList,
      comment,
      actionTaken: actionType,
      selectedVersionId,
      selectedTableId,
      selectedVersion,
      controlParameters
    };

    selectedTable ? handleEditTable(tableItem) : handleAddNewTable(tableItem);
    handleCloseAndClearState();
    !selectedTable &&
      handleSetExpandedList(EXPANDED_LIST.tableListIndex)({
        expandAction: EXPAND_ACTION.replace,
        expandedList: { [tableItem?.rowId]: true }
      });
  };

  return {
    handleBlurField,
    errors,
    touches,
    handleChangeFieldValue,
    tableId,
    tableName,
    comment,
    handleSetElementList,
    elementList,
    handleSetElementListHasError,
    selectedTableId,
    isElementListHasError,
    isDuplicateTableId,
    dispatch: reactDispatch,
    controlParameters,
    handleSetControlParameters,
    actionType,
    isShowControlParameters,
    isFormChanging,
    handleGoBackToPreviousStep,
    handleCloseModal,
    handleCreateNewTable
  };
};
