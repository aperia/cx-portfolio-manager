import { render, RenderResult } from '@testing-library/react';
import { mockUseModalRegistryFnc } from 'app/utils';
import 'app/utils/_mockComponent/mockModalRegistry';
import 'app/utils/_mockComponent/mockUseTranslation';
import React from 'react';
import AddNewTableModal, { AddNewTableAction } from './AddNewTableModal';
import { TableType } from './constant';
import * as hooks from './useAddNewTable';

const mockUseModalRegistry = mockUseModalRegistryFnc();

jest.mock('./ElementList', () => ({
  __esModule: true,
  default: ({}: any) => {
    return (
      <div>
        <h1>Element_List</h1>
      </div>
    );
  }
}));

jest.mock('./ControlParameters', () => ({
  __esModule: true,
  default: ({}: any) => {
    return (
      <div>
        <h1>ControlParameters</h1>
      </div>
    );
  }
}));

jest.mock('app/hooks/useUnsavedChangeRegistry', () => {
  return {
    __esModule: true,
    useUnsavedChangeRegistry: (options: any, changes: any, onConfirm: any) => {}
  };
});

HTMLCanvasElement.prototype.getContext = jest.fn();

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <AddNewTableModal {...props} />
    </div>
  );
};

describe('WorkflowManageTransactionLevelProcessingDecisionTablesTLP > ConfigureParametersTQ > AddNewTableModal', () => {
  const id = 'id';
  const show = true;
  const tableType = TableType.alpaq;
  const handleChangeFieldValue = () => jest.fn();
  const handleBlurField = () => jest.fn();

  const defaultProps = {
    id,
    show,
    tableType
  };

  beforeEach(() => {
    mockUseModalRegistry.mockImplementation(() => [true, false]);
  });

  afterEach(() => {
    mockUseModalRegistry.mockClear();
  });

  it('Should render add new table Modal', () => {
    jest.spyOn(hooks, 'useAddNewTable').mockImplementation(() => {
      return {
        searchValue: 'abc',
        handleChangeFieldValue,
        handleBlurField,
        errors: {},
        tableId: 'test',
        tableName: 'test'
      } as any;
    });
    const wrapper = renderComponent(defaultProps);
    expect(
      wrapper.getByText(
        'txt_manage_transaction_level_tlp_config_tq_tableDetails'
      )
    ).toBeInTheDocument();
  });

  it('Should render add new table Modal with create new version', () => {
    jest.spyOn(hooks, 'useAddNewTable').mockImplementation(() => {
      return {
        searchValue: '',
        handleChangeFieldValue,
        handleBlurField,
        errors: {},
        tableId: 'test',
        tableName: 'Updated',
        actionType: AddNewTableAction.createNewVersion
      } as any;
    });
    const wrapper = renderComponent({
      ...defaultProps,
      tableType: TableType.alpaq
    });
    expect(
      wrapper.getByText(
        'txt_manage_transaction_level_tlp_config_tq_selected_table_id'
      )
    ).toBeInTheDocument();
  });

  it('Should render add new table Modal with add table to model', () => {
    jest.spyOn(hooks, 'useAddNewTable').mockImplementation(() => {
      return {
        searchValue: '',
        handleChangeFieldValue,
        handleBlurField,
        errors: {},
        tableId: 'test',
        tableName: 'Updated',
        isDuplicateTableId: true,
        actionType: AddNewTableAction.addTableToModel
      } as any;
    });
    const wrapper = renderComponent({
      ...defaultProps,
      tableType: TableType.alpaq
    });
    expect(
      wrapper.getByText('txt_manage_transaction_level_tlp_config_tq_table_id')
    ).toBeInTheDocument();
  });

  it('Should render Control parameters list', () => {
    jest.spyOn(hooks, 'useAddNewTable').mockImplementation(() => {
      return {
        searchValue: '',
        handleChangeFieldValue,
        handleBlurField,
        errors: {},
        tableId: '',
        tableName: 'Updated',
        isDuplicateTableId: true,
        actionType: AddNewTableAction.addTableToModel,
        isShowControlParameters: true
      } as any;
    });
    const wrapper = renderComponent({
      ...defaultProps,
      tableType: TableType.alpaq
    });
    expect(wrapper.getByText('ControlParameters')).toBeInTheDocument();
  });

  it('Should render helper text with default', () => {
    jest.spyOn(hooks, 'useAddNewTable').mockImplementation(() => {
      return {
        handleChangeFieldValue,
        handleBlurField,
        errors: {},
        tableId: '',
        tableName: ''
      } as any;
    });
    renderComponent({
      ...defaultProps,
      tableType: ''
    });
  });
});
