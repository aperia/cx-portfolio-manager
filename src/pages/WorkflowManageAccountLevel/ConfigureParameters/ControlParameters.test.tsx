import { render, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import 'app/utils/_mockComponent/mockNoDataFound';
import 'app/utils/_mockComponent/mockPaging';
import 'app/utils/_mockComponent/mockSimpleSearch';
import 'app/utils/_mockComponent/mockUseTranslation';
import * as CommonSelectHooks from 'pages/_commons/redux/Common/select-hooks';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageAccountLevel';
import React from 'react';
import { act } from 'react-dom/test-utils';
import { getControlParameters, TableType } from './constant';
import ControlParameters from './ControlParameters';
import * as hooks from './useControlParameters';

const mockUseSelectWindowDimension = jest.spyOn(
  CommonSelectHooks,
  'useSelectWindowDimension'
);

const useSelectDecisionElementListData = jest.spyOn(
  WorkflowSetup,
  'useSelectStrategiesListData'
);

HTMLCanvasElement.prototype.getContext = jest.fn();
jest.mock('pages/_commons/Utils/ClearAndResetButton', () => ({
  __esModule: true,
  default: ({ onClearAndReset }: any) => (
    <button onClick={onClearAndReset}>{'Clear_And_Reset'}</button>
  )
}));

const MockIncludeActivityOptions = [{ code: 'N', text: 'test' }];
const MockPosPromoValidationOptions = [{ code: 'N', text: 'test' }];

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <ControlParameters {...props} />
    </div>
  );
};

describe('WorkflowManageTransactionLevelProcessingDecisionTablesTLP > ConfigureParameters > ControlParameters', () => {
  const handleSetTouchField = () => jest.fn();
  const handleChangeElementValue = () => jest.fn();
  const handleChangeSearchValue = jest.fn();
  const handleChangeParametersValue = () => jest.fn();

  const controlParameters = getControlParameters(TableType.alpaq);

  beforeEach(() => {
    useSelectDecisionElementListData.mockImplementation(() => ({
      strategiesListAQData: ['1', '3']
    }));
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1920,
      height: 1080
    }));
  });

  afterEach(() => {
    mockUseSelectWindowDimension.mockClear();
  });

  it('Should render TLP-AQ Control parameters list', () => {
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1920,
      height: 1080
    }));
    jest.spyOn(hooks, 'useControlParameters').mockImplementation(() => {
      return {
        handleSetTouchField,
        handleChangeElementValue,
        isHideSearchBox: false,
        includeActivityOptions: MockIncludeActivityOptions,
        posPromoValidationOptions: MockPosPromoValidationOptions,
        parameters: controlParameters,
        handleChangeParametersValue,
        handleChangeSearchValue,
        searchValue: '123',
        allocationFlag: [{ code: '1', text: '1' }],
        allocationBeforeAfterCycle: [{ code: '1', text: '1' }],
        changeInTermsMethodFlag: [{ code: '1', text: '1' }],
        allocationInterval: [{ code: '1', text: '1' }]
      } as any;
    });
    const wrapper = renderComponent({});
    expect(
      wrapper.getByText('txt_manage_account_level_table_parameters')
    ).toBeInTheDocument();

    expect(wrapper.getByText('Simple Search')).toBeInTheDocument();
    const clearAndResetBtn = wrapper.getByText('Clear_And_Reset');

    act(() => {
      userEvent.click(clearAndResetBtn);
    });
    expect(handleChangeSearchValue).toBeCalled();
  });

  it('Should render TLP-RQ Control parameters list', () => {
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1000,
      height: 1080
    }));
    jest.spyOn(hooks, 'useControlParameters').mockImplementation(() => {
      return {
        handleSetTouchField,
        handleChangeElementValue,
        isHideSearchBox: false,
        includeActivityOptions: MockIncludeActivityOptions,
        posPromoValidationOptions: MockPosPromoValidationOptions,
        parameters: [
          {
            id: MethodFieldParameterEnum.AllocationDate,
            businessName: 'Allocation Date',
            greenScreenName: 'ALLOC Date',
            moreInfo:
              'The Allocation Date parameter defines the date you want the System to allocate accounts through the client allocation table.'
          },
          {
            id: MethodFieldParameterEnum.AllocationFlag,
            businessName: 'Allocation Flag',
            greenScreenName: 'ALLOC Flag',
            moreInfo:
              'The Allocation Flag parameter determines when you want the System to allocate accounts through the account qualification table.'
          },
          {
            id: MethodFieldParameterEnum.AllocationInterval,
            businessName: 'Allocation Interval',
            greenScreenName: 'ALLOC Interval',
            moreInfo:
              'The Allocation Interval parameter determines the number of months after the account’s Statement Cycle Allocation Date you want the System to allocate accounts through the account qualification table if you set the Allocation Flag field to 2.'
          },
          {
            id: MethodFieldParameterEnum.AllocationBeforeAfterCycle,
            value: '1',
            valueText: '1',
            businessName: 'Allocation Before/After Cycle',
            greenScreenName: 'ALLOC B/A Cycle',
            moreInfo:
              'The Allocation Before/After Cycle parameter determines whether the System allocates accounts before or after statement cycle processing.'
          },
          {
            id: MethodFieldParameterEnum.DefaultStrategy,
            businessName: 'Default Strategy',
            greenScreenName: 'Default Strategy',
            moreInfo:
              'The Default Strategy parameter defines the default pricing strategy to be assigned to a new or transferred cardholder account if the account receives the SAME result ID.'
          },
          {
            id: MethodFieldParameterEnum.ChangeInTermsMethodFlag,
            value: '1',
            valueText: '1',
            businessName: 'Change In Terms Method Flag',
            greenScreenName: 'CIT Method Flag',
            moreInfo:
              'The Change In Terms Method Flag parameter designates whether you want to associate CIT methods with the pricing strategies assigned during reallocation processing.'
          }
        ],
        allocationFlag: [{ code: '1', text: '1' }],
        allocationBeforeAfterCycle: [{ code: '1', text: '1' }],
        changeInTermsMethodFlag: [{ code: '1', text: '1' }],
        allocationInterval: [{ code: '1', text: '1' }],
        handleChangeParametersValue,
        searchValue: '123'
      } as any;
    });
    const wrapper = renderComponent({});
    expect(
      wrapper.getByText('txt_manage_account_level_table_parameters')
    ).toBeInTheDocument();

    expect(wrapper.getByText('Simple Search')).toBeInTheDocument();
  });

  it('Should render Empty Control parameters list', () => {
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1000,
      height: 1080
    }));
    jest.spyOn(hooks, 'useControlParameters').mockImplementation(() => {
      return {
        handleSetTouchField,
        handleChangeElementValue,
        isHideSearchBox: false,
        includeActivityOptions: MockIncludeActivityOptions,
        posPromoValidationOptions: MockPosPromoValidationOptions,
        parameters: [],
        handleChangeParametersValue,
        searchValue: '123',
        handleChangeSearchValue,
        allocationFlag: [{ code: '1', text: '1' }],
        allocationBeforeAfterCycle: [{ code: '1', text: '1' }],
        changeInTermsMethodFlag: [{ code: '1', text: '1' }],
        allocationInterval: [{ code: '1', text: '1' }]
      } as any;
    });
    const wrapper = renderComponent({});
    const noDataFound = wrapper.getByText('No data found');
    expect(noDataFound).toBeInTheDocument();

    act(() => {
      userEvent.click(noDataFound);
    });
    expect(handleChangeSearchValue).toBeCalled();
  });

  it('Should render TLP-AQ Control parameters list with date value', () => {
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1000,
      height: 1080
    }));
    jest.spyOn(hooks, 'useControlParameters').mockImplementation(() => {
      return {
        handleSetTouchField,
        handleChangeElementValue,
        isHideSearchBox: false,
        includeActivityOptions: MockIncludeActivityOptions,
        posPromoValidationOptions: MockPosPromoValidationOptions,
        parameters: [...getControlParameters(TableType.alpaq)].map(control => ({
          ...control,
          value: '22/11/2022'
        })),
        handleChangeParametersValue,
        allocationFlag: [{ code: '1', text: '1' }],
        allocationBeforeAfterCycle: [{ code: '1', text: '1' }],
        changeInTermsMethodFlag: [{ code: '1', text: '1' }],
        allocationInterval: [{ code: '1', text: '1' }]
      } as any;
    });
    const wrapper = renderComponent({});
    expect(
      wrapper.getByText('txt_manage_account_level_table_parameters')
    ).toBeInTheDocument();
  });
});
