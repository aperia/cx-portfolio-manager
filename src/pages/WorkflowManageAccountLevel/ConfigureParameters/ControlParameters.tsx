import EnhanceDatePicker from 'app/components/EnhanceDatePicker';
import EnhanceDropdownList from 'app/components/EnhanceDropdownList';
import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import {
  ColumnType,
  ComboBox,
  Grid,
  Icon,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import isEmpty from 'lodash.isempty';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import { useSelectStrategiesListData } from 'pages/_commons/redux/WorkflowSetup';
import ClearAndResetButton from 'pages/_commons/Utils/ClearAndResetButton';
import React from 'react';
import { FieldType, useControlParameters } from './useControlParameters';

export interface ControlParametersProps {
  controlParameterList: any[];
  handleChangeControlParameters: (controlParameters: any[]) => void;
}

const ControlParameters: React.FC<ControlParametersProps> = props => {
  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();
  const { strategiesListAQData } = useSelectStrategiesListData();

  const {
    searchValue,
    handleChangeSearchValue,
    parameters,
    simpleSearchRef,
    handleChangeParametersValue,
    allocationFlag,
    allocationBeforeAfterCycle,
    changeInTermsMethodFlag,
    allocationInterval
  } = useControlParameters(props);

  const renderValuesField = (data: any) => {
    const { id } = data;
    switch (id) {
      case MethodFieldParameterEnum.DefaultStrategy:
        return (
          <ComboBox
            size="small"
            popupBaseProps={{ popupBaseClassName: 'inside-infobar' }}
            placeholder={t('txt_select_an_option')}
            onChange={handleChangeParametersValue(id, FieldType.comBoBox)}
            noResult={t('txt_no_results_found_without_dot')}
            name="sectionCode"
            value={data?.value}
          >
            {strategiesListAQData.map((item: string) => (
              <ComboBox.Item key={item} value={item} label={item} />
            ))}
          </ComboBox>
        );
      case MethodFieldParameterEnum.AllocationFlag:
        const flag = allocationFlag.find(
          option => option?.code === data?.value
        );
        return (
          <EnhanceDropdownList
            size="small"
            value={flag}
            placeholder={t('txt_select_an_option')}
            onChange={handleChangeParametersValue(id, FieldType.dropDown)}
            options={allocationFlag}
          />
        );
      case MethodFieldParameterEnum.AllocationInterval:
        const interval = allocationInterval.find(
          option => option?.code === data?.value
        );
        return (
          <EnhanceDropdownList
            size="small"
            value={interval}
            placeholder={t('txt_select_an_option')}
            onChange={handleChangeParametersValue(id, FieldType.dropDown)}
            options={allocationInterval}
          />
        );
      case MethodFieldParameterEnum.AllocationBeforeAfterCycle:
        const beforeAfter =
          isEmpty(data?.value) && isEmpty(data?.valueText)
            ? allocationBeforeAfterCycle.find(option => option?.code === 'B')
            : allocationBeforeAfterCycle.find(
                option => option?.code === data?.value
              );
        return (
          <EnhanceDropdownList
            size="small"
            value={beforeAfter}
            placeholder={t('txt_select_an_option')}
            onChange={handleChangeParametersValue(id, FieldType.dropDown)}
            options={allocationBeforeAfterCycle}
          />
        );
      case MethodFieldParameterEnum.ChangeInTermsMethodFlag:
        const changeInTerms =
          isEmpty(data?.value) && isEmpty(data?.valueText)
            ? changeInTermsMethodFlag.find(option => option?.code === 'N')
            : changeInTermsMethodFlag.find(
                option => option?.code === data?.value
              );
        return (
          <EnhanceDropdownList
            size="small"
            value={changeInTerms}
            placeholder={t('txt_select_an_option')}
            onChange={handleChangeParametersValue(id, FieldType.dropDown)}
            options={changeInTermsMethodFlag}
          />
        );
      case MethodFieldParameterEnum.AllocationDate:
        const allocationDateValue = data.value
          ? new Date(data?.value)
          : undefined;
        return (
          <EnhanceDatePicker
            placeholder={t('txt_enter_a_value')}
            required
            size="small"
            value={allocationDateValue}
            onChange={handleChangeParametersValue(id, FieldType.datePicker)}
          />
        );
    }
  };

  const columns: ColumnType[] = [
    {
      id: 'businessName',
      Header: t('txt_manage_account_level_table_decision_business_name'),
      accessor: data => t(data.businessName),
      cellBodyProps: { className: 'pt-12 pb-8' },
      width: 233
    },
    {
      id: 'greenScreenName',
      Header: t('txt_manage_account_level_table_decision_green_name'),
      accessor: data => t(data.greenScreenName),
      cellBodyProps: { className: 'pt-12 pb-8' },
      width: 244
    },
    {
      id: 'value',
      Header: t('txt_manage_account_level_table_decision_value'),
      width: width < 1280 ? 320 : 360,
      cellBodyProps: { className: 'py-8' },
      accessor: renderValuesField
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      width: 106,
      cellBodyProps: { className: 'pt-12 pb-8' },
      accessor: data => (
        <Tooltip element={t(data.moreInfo)}>
          <Icon name="information" size="5x" className="color-grey-l16" />
        </Tooltip>
      )
    }
  ];

  return (
    <div className="mt-16">
      <div className="d-flex align-items-center justify-content-between mb-16">
        <h5>{t('txt_manage_account_level_table_parameters')}</h5>
        <SimpleSearch
          ref={simpleSearchRef}
          placeholder={t('txt_type_to_search')}
          defaultValue={searchValue}
          onSearch={handleChangeSearchValue}
          popperElement={
            <>
              <p className="color-grey">{t('txt_search_description')}</p>
              <ul className="search-field-item list-unstyled">
                <li className="mt-16">{t('txt_business_name')}</li>
                <li className="mt-16">{t('txt_green_screen_name')}</li>
                <li className="mt-16">{t('txt_more_info')}</li>
              </ul>
            </>
          }
        />
      </div>
      {searchValue && !isEmpty(parameters) && (
        <div className="d-flex justify-content-end mb-16 mr-n8">
          <ClearAndResetButton
            small
            onClearAndReset={() => {
              handleChangeSearchValue('');
            }}
          />
        </div>
      )}
      {isEmpty(parameters) && (
        <div className="d-flex flex-column justify-content-center mt-40">
          <NoDataFound
            id="newMethod_notfound"
            hasSearch
            title={t('txt_no_results_found')}
            linkTitle={t('txt_clear_and_reset')}
            onLinkClicked={() => {
              handleChangeSearchValue('');
            }}
          />
        </div>
      )}
      {!isEmpty(parameters) && <Grid columns={columns} data={parameters} />}
    </div>
  );
};

export default ControlParameters;
