import { render, RenderResult } from '@testing-library/react';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import * as CommonSelectHooks from 'pages/_commons/redux/Common/select-hooks';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageAccountLevel';
import React from 'react';
import TableListSubGrid from './TableListSubGrid';

const mockUseSelectWindowDimension = jest.spyOn(
  CommonSelectHooks,
  'useSelectWindowDimension'
);

const useSelectDecisionElementListData = jest.spyOn(
  WorkflowSetup,
  'useSelectElementMetadataManageAcountLevel'
);

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const MockElementList1 = [{ immediateAllocation: 'Yes' }] as any[];
const MockElementList2 = [{ immediateAllocation: 'No' }] as any[];
const MockElementList3 = [{ immediateAllocation: '' }] as any[];
const MockControlParameters = [
  {
    businessName: '1',
    greenScreenNam: '1',
    id: MethodFieldParameterEnum.AllocationDate,
    value: new Date()
  },
  {
    businessName: '2',
    greenScreenNam: '2',
    id: MethodFieldParameterEnum.AllocationFlag,
    value: '2'
  },
  {
    businessName: '3',
    greenScreenNam: '3',
    id: MethodFieldParameterEnum.AllocationBeforeAfterCycle,
    value: '3',
    valueText: '3'
  },
  {
    businessName: '4',
    greenScreenNam: '4',
    id: MethodFieldParameterEnum.AllocationBeforeAfterCycle,
    value: '',
    valueText: ''
  },
  {
    businessName: '5',
    greenScreenNam: '5',
    id: MethodFieldParameterEnum.ChangeInTermsMethodFlag,
    value: '5',
    valueText: '5'
  },
  {
    businessName: '6',
    greenScreenNam: '6',
    id: MethodFieldParameterEnum.ChangeInTermsMethodFlag,
    value: '',
    valueText: ''
  },
  {
    businessName: '6',
    greenScreenNam: '6',
    id: '',
    value: '',
    valueText: ''
  }
] as any[];

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <TableListSubGrid {...props} />
    </div>
  );
};

describe('WorkflowManageTransactionLevelProcessingDecisionTablesTLP > ConfigureParametersTQ > TableListSubGrid', () => {
  beforeEach(() => {
    useSelectDecisionElementListData.mockImplementation(() => ({
      searchCode: [],
      allocationFlag: [{ code: '2', text: '2' }],
      allocationBeforeAfterCycle: [{ code: 'B', value: 'B' }],
      changeInTermsMethodFlag: [{ code: 'N', value: 'N' }],
      allocationInterval: [],
      alpConfig: []
    }));
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1920,
      height: 1080
    }));
  });

  afterEach(() => {
    mockUseSelectWindowDimension.mockClear();
  });
  it('Should render Table List SubGrid ', () => {
    const wrapper = renderComponent({
      elementList: MockElementList1,
      controlParameters: MockControlParameters
    });

    expect(
      wrapper.getByText('txt_manage_transaction_level_tlp_element_list')
    ).toBeInTheDocument();
  });

  it('Should render Table List SubGrid ', () => {
    const wrapper = renderComponent({
      elementList: MockElementList2,
      controlParameters: MockControlParameters
    });

    expect(
      wrapper.getByText('txt_manage_transaction_level_tlp_element_list')
    ).toBeInTheDocument();
  });

  it('Should render Table List SubGrid ', () => {
    const wrapper = renderComponent({
      elementList: MockElementList3,
      controlParameters: MockControlParameters
    });

    expect(
      wrapper.getByText('txt_manage_transaction_level_tlp_element_list')
    ).toBeInTheDocument();
  });

  it('Should render Table List SubGrid ', () => {
    const wrapper = renderComponent({});

    expect(
      wrapper.getByText('txt_manage_transaction_level_tlp_element_list')
    ).toBeInTheDocument();
  });
});
