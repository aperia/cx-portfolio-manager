import { MethodFieldParameterEnum } from 'app/constants/enums';
import get from 'lodash.get';
import _orderBy from 'lodash.orderby';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import { useCallback, useEffect, useReducer } from 'react';
import { useDispatch } from 'react-redux';
import { TableType } from './constant';

interface ConfigActionState {
  openedModal: null | CONFIG_ACTIONS_MODAL;
  expandedList: Record<EXPANDED_LIST, Record<string, any>>;
  selectedVersion?: any;
  selectedTable?: any;
  isCreateNewVersion?: boolean;
  draftNewTable?: Record<string, any> | null;
  tableList: any[];
  tableIndexChanged: number | null;
}

export enum CONFIG_ACTIONS_MODAL {
  addNewTableModal = 'addNewTableModal',
  tableListModal = 'tableListModal',
  deleteTableModal = 'deleteTableModal'
}

export enum EXPANDED_LIST {
  tableListModal = 'tableListModal',
  tableListIndex = 'tableListIndex'
}
export enum EXPAND_ACTION {
  toggle = 'toggle',
  replace = 'replace'
}

const ACTION_TYPE = {
  SET_OPENED_MODAL: 'SET_OPENED_MODAL',
  SET_EXPANDED_LIST: 'SET_EXPANDED_LIST',
  SET_SELECTED_VERSION: 'SET_SELECTED_VERSION',
  SET_SELECTED_TABLE: 'SET_SELECTED_TABLE',
  SET_IS_CREATE_NEW_VERSION: 'SET_IS_CREATE_NEW_VERSION',
  SET_DRAFT_NEW_TABLE: 'SET_DRAFT_NEW_TABLE',
  SET_TABLE_LIST: 'SET_TABLE_LIST',
  SET_TABLE_INDEX_CHANGED: 'SET_TABLE_INDEX_CHANGED'
};

export const INITIAL_STATE: ConfigActionState = {
  openedModal: null,
  expandedList: {
    [EXPANDED_LIST.tableListModal]: {},
    [EXPANDED_LIST.tableListIndex]: {}
  },
  draftNewTable: null,
  tableList: [],
  tableIndexChanged: null
};

const configActionsReducer = (state: ConfigActionState, action: any) => {
  switch (action.type) {
    case ACTION_TYPE.SET_OPENED_MODAL:
      return {
        ...state,
        openedModal: action.modalName
      };
    case ACTION_TYPE.SET_EXPANDED_LIST:
      let newExpandEdList = {
        ...state.expandedList[action.expandedListName as EXPANDED_LIST]
      };
      if (action.expandAction === EXPAND_ACTION.toggle) {
        newExpandEdList[action.expandKey]
          ? delete newExpandEdList[action.expandKey]
          : (newExpandEdList[action.expandKey] = true);
      }
      if (action.expandAction === EXPAND_ACTION.replace) {
        newExpandEdList = action.expandedList || {};
      }
      return {
        ...state,
        expandedList: {
          ...state.expandedList,
          [action.expandedListName]: newExpandEdList
        }
      };
    case ACTION_TYPE.SET_SELECTED_VERSION:
      return {
        ...state,
        selectedVersion: action.selectedVersion
      };
    case ACTION_TYPE.SET_SELECTED_TABLE:
      return {
        ...state,
        selectedTable: action.selectedTable
      };
    case ACTION_TYPE.SET_IS_CREATE_NEW_VERSION:
      return {
        ...state,
        isCreateNewVersion: action.isCreateNewVersion
      };
    case ACTION_TYPE.SET_DRAFT_NEW_TABLE:
      return {
        ...state,
        draftNewTable: action.draftNewTable
      };
    case ACTION_TYPE.SET_TABLE_LIST:
      return {
        ...state,
        tableList: action.tableList
      };
    case ACTION_TYPE.SET_TABLE_INDEX_CHANGED:
      return {
        ...state,
        tableIndexChanged: action.tableIndexChanged
      };
    default:
      return state;
  }
};

export const useConfigAction = (tables: any[], tableType: TableType) => {
  const initialState = { ...INITIAL_STATE, tableList: tables };
  const [state, dispatch] = useReducer(configActionsReducer, initialState);
  const {
    openedModal,
    expandedList,
    selectedVersion,
    selectedTable,
    isCreateNewVersion,
    draftNewTable,
    tableList,
    tableIndexChanged
  } = state;

  const reduxDispatch = useDispatch();

  useEffect(() => {
    reduxDispatch(
      actionsWorkflowSetup.getWorkflowMetadata([
        MethodFieldParameterEnum.SearchCode,
        MethodFieldParameterEnum.AllocationFlag,
        MethodFieldParameterEnum.AllocationBeforeAfterCycle,
        MethodFieldParameterEnum.DefaultStrategy,
        MethodFieldParameterEnum.ChangeInTermsMethodFlag,
        MethodFieldParameterEnum.AllocationInterval
      ])
    );
  }, [reduxDispatch]);

  useEffect(() => {
    reduxDispatch(
      actionsWorkflowSetup.getDecisionElementList({
        tableType,
        serviceNameAbbreviation: 'ALP'
      })
    );
  }, [reduxDispatch, tableType]);

  const handleOpenConfigModal = useCallback(
    (modalName: CONFIG_ACTIONS_MODAL | null) =>
      dispatch({
        type: ACTION_TYPE.SET_OPENED_MODAL,
        modalName
      }),
    []
  );

  const handleCheckDuplicateTableId = useCallback(
    (tableId: string) => {
      const selectedTableId = get(selectedVersion, 'tableId', '');
      const versionTableId = get(selectedVersion, ['version', 'tableId'], '');
      const tableListIds = tableList.map((table: any) => table?.tableId);
      const arrayCheck = selectedVersion
        ? [...tableListIds, selectedTableId, versionTableId]
        : tableListIds;
      return arrayCheck.some(
        (existedTableId: string) => tableId === existedTableId
      );
    },
    [selectedVersion, tableList]
  );

  const handleSetExpandedList = useCallback(
    (expandedListName: EXPANDED_LIST) =>
      ({ expandAction, expandKey, expandedList }: Record<string, any>) => {
        dispatch({
          type: ACTION_TYPE.SET_EXPANDED_LIST,
          expandedList,
          expandedListName,
          expandKey,
          expandAction
        });
      },
    []
  );

  const handleSetSelectedVersion = useCallback((selectedVersion: any) => {
    dispatch({
      type: ACTION_TYPE.SET_SELECTED_VERSION,
      selectedVersion
    });
  }, []);

  const handleSetIsCreateNewVersion = useCallback(
    (isCreateNewVersion: boolean) => {
      dispatch({
        type: ACTION_TYPE.SET_IS_CREATE_NEW_VERSION,
        isCreateNewVersion
      });
    },
    []
  );

  const handleSetDraftNewTable = useCallback(
    (draftNewTable: Record<string, any>) => {
      dispatch({
        type: ACTION_TYPE.SET_DRAFT_NEW_TABLE,
        draftNewTable
      });
    },
    []
  );

  const handleAddNewTable = useCallback(
    (table: any) => {
      const newTables = _orderBy([...tableList, table], 'tableId', 'asc');
      const tableIndexChanged = newTables.findIndex(
        (t: any) => t?.tableId === table?.tableId
      );
      dispatch({
        type: ACTION_TYPE.SET_TABLE_LIST,
        tableList: newTables
      });
      dispatch({
        type: ACTION_TYPE.SET_TABLE_INDEX_CHANGED,
        tableIndexChanged
      });
    },
    [tableList]
  );

  const handleSetSelectedTable = useCallback((selectedTable: any) => {
    dispatch({
      type: ACTION_TYPE.SET_SELECTED_TABLE,
      selectedTable
    });
  }, []);

  const handleEditTable = useCallback(
    (newTable: any) => {
      const tableIndex = tableList.findIndex(
        (table: any) => table.rowId === newTable.rowId
      );
      const newTableList = [...tableList];
      newTableList[tableIndex] = newTable;
      const newTablesOrdered = _orderBy(newTableList, 'tableId', 'asc');
      const tableIndexChanged = newTablesOrdered.findIndex(
        (t: any) => t?.tableId === newTable?.tableId
      );
      dispatch({
        type: ACTION_TYPE.SET_TABLE_LIST,
        tableList: newTablesOrdered
      });
      dispatch({
        type: ACTION_TYPE.SET_TABLE_INDEX_CHANGED,
        tableIndexChanged
      });
    },
    [tableList]
  );

  const handleDeleteTable = useCallback(
    (rowId: string) => {
      const newTableList =
        tableList.filter((table: any) => table.rowId !== rowId) || [];
      dispatch({
        type: ACTION_TYPE.SET_TABLE_LIST,
        tableList: newTableList
      });
    },
    [tableList]
  );

  return {
    handleOpenConfigModal,
    openedModal,
    selectedTable,
    expandedList,
    selectedVersion,
    handleSetExpandedList,
    handleSetSelectedVersion,
    handleSetIsCreateNewVersion,
    isCreateNewVersion,
    handleSetDraftNewTable,
    draftNewTable,
    handleAddNewTable,
    tableList,
    handleSetSelectedTable,
    handleEditTable,
    handleDeleteTable,
    handleCheckDuplicateTableId,
    dispatch,
    tableIndexChanged
  };
};
