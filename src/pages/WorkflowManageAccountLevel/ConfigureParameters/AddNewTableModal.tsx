import FormatTextBox from 'app/components/FormatTextBox';
import ModalRegistry from 'app/components/ModalRegistry';
import TextAreaCountDown from 'app/components/TextAreaCountDown';
import {
  unsavedChangesProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { useUnsavedChangeRegistry } from 'app/hooks';
import {
  Button,
  InlineMessage,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  TextBox,
  useTranslation
} from 'app/_libraries/_dls';
import React from 'react';
import { TableType } from './constant';
import ControlParameters from './ControlParameters';
import ElementList from './ElementList';
import { FIELD_NAME, useAddNewTable } from './useAddNewTable';
import { CONFIG_ACTIONS_MODAL, EXPANDED_LIST } from './useConfigActions';

export const AddNewTableAction = {
  createNewTable: 'createNewTable',
  createNewVersion: 'createNewVersion',
  addTableToModel: 'addTableToModel'
};

export interface AddNewTableModalProps {
  id: string;
  show: boolean;
  handleClose: () => void;
  selectedVersion: any;
  handleOpenConfigModal: (tableName: CONFIG_ACTIONS_MODAL) => void;
  handleSelectVersion: (version: any) => void;
  handleSetDraftNewTable: (draftNewTable: any) => void;
  draftNewTable: any;
  isCreateNewVersion: boolean;
  handleAddNewTable: (table: any) => void;
  handleSetIsCreateNewVersion: (isCreateNewVersion: boolean) => void;
  selectedTable: any;
  handleEditTable: (newTable: any) => void;
  handleSetSelectedTable: (table: any) => void;
  handleSetExpandedList: (
    tableListName: EXPANDED_LIST
  ) => (expandItem: Record<string, any>) => void;
  handleCheckDuplicateTableId: (tableId: string) => boolean;
  tableType: TableType;
  enableShowConfirmFileStep: (enable: boolean) => void;
}

const AddNewTableModal: React.FC<AddNewTableModalProps> = props => {
  const { t } = useTranslation();
  const { id, show, tableType } = props;
  const {
    tableId,
    tableName,
    comment,
    handleBlurField,
    handleChangeFieldValue,
    errors,
    elementList,
    handleSetElementList,
    handleSetElementListHasError,
    selectedTableId,
    isElementListHasError,
    isDuplicateTableId,
    controlParameters,
    handleSetControlParameters,
    actionType,
    isShowControlParameters,
    isFormChanging,
    handleCloseModal,
    handleCreateNewTable,
    handleGoBackToPreviousStep
  } = useAddNewTable(props);

  useUnsavedChangeRegistry(
    {
      ...unsavedChangesProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_TRANSACTION_LEVEL_PROCESSING_DECISION_TABLES_TLP__CONFIG_TQ_DETAILS,
      priority: 1
    },
    [isFormChanging]
  );

  const isDisabledSaveButton =
    isElementListHasError ||
    (!tableId && actionType !== AddNewTableAction.createNewVersion) ||
    !tableName;

  return (
    <ModalRegistry lg id={id} show={show} onAutoClosedAll={handleCloseModal}>
      <ModalHeader border closeButton onHide={handleCloseModal}>
        <ModalTitle>
          {t('txt_manage_transaction_level_tlp_config_tq_tableDetails')}
        </ModalTitle>
      </ModalHeader>
      <ModalBody>
        <div className="color-grey">
          <p>
            {t(
              'txt_manage_transaction_level_tlp_config_aq_table_detail_help_text_1'
            )}
          </p>
          <p className="pt-8">
            {t(
              'txt_manage_transaction_level_tlp_config_aq_table_detail_help_text_2'
            )}
          </p>
        </div>

        <div>
          {isDuplicateTableId && (
            <InlineMessage variant="danger" withIcon className="mb-8 mt-16">
              {t('txt_manage_transaction_level_tlp_warning_duplicate_table_id')}
            </InlineMessage>
          )}
        </div>
        <div className="row mt-16">
          {actionType !== AddNewTableAction.createNewTable && (
            <div className="col-6 col-lg-4">
              <FormatTextBox
                id="addNewMethod__selectedTableId"
                readOnly
                value={selectedTableId}
                maxLength={4}
                label={t(
                  'txt_manage_transaction_level_tlp_config_tq_selected_table_id'
                )}
              />
            </div>
          )}
          {actionType !== AddNewTableAction.createNewVersion && (
            <div className="col-6 col-lg-4">
              <FormatTextBox
                id="addNewTable__TableId"
                value={tableId}
                onChange={handleChangeFieldValue(FIELD_NAME.tableId)}
                required
                onBlur={handleBlurField(FIELD_NAME.tableId)}
                maxLength={4}
                pattern="^[0-9]{0,4}$"
                label={t('txt_manage_transaction_level_tlp_config_tq_table_id')}
                error={errors.tableId}
              />
            </div>
          )}
          {
            <div className="col-6 col-lg-4">
              <TextBox
                id="addNewTable__tableName"
                value={tableName}
                onChange={handleChangeFieldValue(FIELD_NAME.tableName)}
                onBlur={handleBlurField(FIELD_NAME.tableName)}
                required
                maxLength={18}
                label={t(
                  'txt_manage_transaction_level_tlp_config_tq_table_name'
                )}
                error={errors.tableName}
              />
            </div>
          }
          <div className="mt-16 col-12 col-lg-8">
            <TextAreaCountDown
              id="addNewTable__comment"
              label={t('txt_comment_area')}
              value={comment}
              onChange={handleChangeFieldValue(FIELD_NAME.comment)}
              rows={4}
              maxLength={204}
            />
          </div>
        </div>
        {isShowControlParameters && (
          <ControlParameters
            controlParameterList={controlParameters}
            handleChangeControlParameters={handleSetControlParameters}
          />
        )}
        <ElementList
          elementList={elementList}
          handleSetElementList={handleSetElementList}
          handleSetElementListHasError={handleSetElementListHasError}
          tableType={tableType}
        />
      </ModalBody>
      <ModalFooter
        cancelButtonText={t('txt_cancel')}
        okButtonText={t('txt_save')}
        onCancel={handleCloseModal}
        onOk={handleCreateNewTable}
        disabledOk={isDisabledSaveButton}
      >
        {actionType !== AddNewTableAction.createNewTable && (
          <Button
            className="mr-auto ml-n8"
            variant="outline-primary"
            onClick={handleGoBackToPreviousStep}
          >
            {t('txt_back')}
          </Button>
        )}
      </ModalFooter>
    </ModalRegistry>
  );
};

export default AddNewTableModal;
