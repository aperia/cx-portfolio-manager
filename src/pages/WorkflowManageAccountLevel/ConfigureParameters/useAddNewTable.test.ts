import { act, renderHook } from '@testing-library/react-hooks';
import 'app/utils/_mockComponent/mockUseTranslation';
import * as reactRedux from 'react-redux';
import { AddNewTableAction } from './AddNewTableModal';
import { FIELD_NAME, useAddNewTable } from './useAddNewTable';

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');

const mockOnConfirmParams = jest.fn();
jest.mock('app/hooks/useUnsavedChangesRedirect', () => {
  return {
    __esModule: true,
    useUnsavedChangesRedirect:
      () =>
      ({ onConfirm, onDiscard, onCancel }: any) => {
        onDiscard && onDiscard();
        onCancel && onCancel();
        onConfirm && onConfirm(mockOnConfirmParams());
      }
  };
});

describe('WorkflowManageTransactionLevelProcessingDecisionTablesTLP > ConfigureParametersTQ > useAddNewTable', () => {
  const mockDispatch = jest.fn();
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
  });

  const handleClose = jest.fn();
  const handleOpenConfigModal = jest.fn();
  const handleSelectVersion = jest.fn();
  const draftNewTable = null;
  const handleSetDraftNewTable = jest.fn();
  const isCreateNewVersion = false;
  const handleAddNewTable = jest.fn();
  const handleSetIsCreateNewVersion = jest.fn();
  const selectedTable = null;
  const handleSetSelectedTable = jest.fn();
  const handleSetExpandedList = () => jest.fn();
  const handleCheckDuplicateTableId = () => true;
  const tableType = '';
  const handleEditTable = jest.fn();

  const defaultProps = {
    handleClose,
    handleOpenConfigModal,
    handleSelectVersion,
    draftNewTable,
    handleSetDraftNewTable,
    isCreateNewVersion,
    handleAddNewTable,
    handleSetIsCreateNewVersion,
    selectedTable,
    handleSetSelectedTable,
    handleSetExpandedList,
    handleCheckDuplicateTableId,
    tableType,
    handleEditTable
  } as any;
  it('Run Default reducer function', () => {
    const { result } = renderHook(() => useAddNewTable(defaultProps));
    act(() => {
      result.current.dispatch({ type: 'Test Action' });
    });
  });

  it('Run hook with draftNew State', () => {
    const { result } = renderHook(() =>
      useAddNewTable({
        ...defaultProps,
        draftNewTable: {
          tableId: 'test',
          elementList: [
            {
              name: 'name1'
            },
            {
              name: 'name2'
            },
            {
              name: 'name3'
            }
          ]
        }
      })
    );
    act(() => {
      result.current.handleChangeFieldValue(FIELD_NAME.tableId)({
        target: {
          value: 'test'
        }
      });
      result.current.handleChangeFieldValue(FIELD_NAME.tableId)({
        target: {
          value: '###'
        }
      });
      result.current.handleBlurField(FIELD_NAME.tableId)();
    });

    act(() => {
      result.current.handleSetElementList([1, 2]);
      result.current.handleSetElementListHasError(true);
    });
    expect(result.current.elementList).toEqual([1, 2]);
    expect(result.current.isElementListHasError).toEqual(true);

    act(() => {
      result.current.handleCreateNewTable();
    });
  });

  it('Run hook with selected table', () => {
    const { result } = renderHook(() =>
      useAddNewTable({
        ...defaultProps,
        selectedTable: {
          tableId: 'test abc',
          actionTaken: AddNewTableAction.addTableToModel
        }
      })
    );

    act(() => {
      result.current.handleChangeFieldValue(FIELD_NAME.tableId)({
        target: {}
      });
      result.current.handleChangeFieldValue(FIELD_NAME.tableName)({
        target: {
          value: '###'
        }
      });
      result.current.handleChangeFieldValue(FIELD_NAME.tableId)({
        target: {
          value: 'change table id'
        }
      });
      result.current.handleBlurField(FIELD_NAME.tableId)();
      result.current.handleCreateNewTable();
    });
    expect(handleEditTable).toBeCalled();

    act(() => {
      result.current.handleGoBackToPreviousStep();
    });

    expect(handleSetDraftNewTable).toBeCalled();
  });

  it('Run hook with selected version', () => {
    const { result } = renderHook(() =>
      useAddNewTable({
        ...defaultProps,
        handleCheckDuplicateTableId: () => false,
        selectedVersion: {
          tableId: 'test abc'
        },
        isCreateNewVersion: true
      })
    );

    act(() => {
      result.current.handleCreateNewTable();
      result.current.handleSetControlParameters([]);
    });
    expect(handleAddNewTable).toBeCalled();
  });

  it('Run hook with selected version and choose table to model', () => {
    const { result } = renderHook(() =>
      useAddNewTable({
        ...defaultProps,
        selectedVersion: {
          tableId: 'test abc'
        },
        isCreateNewVersion: false
      })
    );
    act(() => {
      result.current.handleCloseModal();
    });
  });
});
