import ModalRegistry from 'app/components/ModalRegistry';
import {
  Button,
  ModalBody,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import isFunction from 'lodash.isfunction';
import React from 'react';

interface RemoveTableModal {
  id: string;
  show: boolean;
  onClose?: () => void;
  onOk: () => void;
}

const RemoveTableModal: React.FC<RemoveTableModal> = ({
  id,
  show,
  onClose,
  onOk
}) => {
  const { t } = useTranslation();
  const onRemove = async () => {
    isFunction(onOk) && onOk();
    isFunction(onClose) && onClose();
  };

  const handleClose = () => {
    isFunction(onClose) && onClose();
  };

  return (
    <ModalRegistry
      id={id}
      show={show}
      onAutoClosedAll={handleClose}
      classes={{ content: '' }}
    >
      <ModalHeader border closeButton onHide={handleClose}>
        <ModalTitle>Confirm Delete</ModalTitle>
      </ModalHeader>
      <ModalBody>
        <span>
          Are you sure you want to permanently delete this table from this
          workflow?
        </span>
      </ModalBody>
      <div className="dls-modal-footer modal-footer">
        <Button variant="secondary" onClick={handleClose}>
          {t('txt_cancel')}
        </Button>
        <Button variant="danger" onClick={onRemove}>
          Delete
        </Button>
      </div>
    </ModalRegistry>
  );
};

export default RemoveTableModal;
