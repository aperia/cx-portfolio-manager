import { parameterGroup } from './parameterList';

export enum TableType {
  alpaq = 'AQ'
}

export const getControlParameters = (type: TableType) => {
  switch (type) {
    case TableType.alpaq:
    default:
      return parameterGroup;
  }
};
