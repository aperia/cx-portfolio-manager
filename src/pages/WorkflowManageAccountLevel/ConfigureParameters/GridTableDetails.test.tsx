import { render, RenderResult } from '@testing-library/react';
import * as CommonSelectHooks from 'pages/_commons/redux/Common/select-hooks';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageAccountLevel';
import React from 'react';
import GridTableDetails from './GridTableDetails';

const mockUseSelectWindowDimension = jest.spyOn(
  CommonSelectHooks,
  'useSelectWindowDimension'
);

const useSelectDecisionElementListData = jest.spyOn(
  WorkflowSetup,
  'useSelectElementMetadataManageAcountLevel'
);

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const MockData = [
  {
    id: '1',
    businessName: '1',
    greenScreenName: '1',
    value: '1'
  }
] as any[];

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      GridTableDetails
      <GridTableDetails {...props} />
    </div>
  );
};

describe('workflowManageAccountLevel > ConfigureParameters > GridTableDetails', () => {
  beforeEach(() => {
    useSelectDecisionElementListData.mockImplementation(() => ({
      searchCode: [],
      allocationFlag: [{ code: '2', text: '2' }],
      allocationBeforeAfterCycle: [{ code: 'B', value: 'B' }],
      changeInTermsMethodFlag: [{ code: 'N', value: 'N' }],
      allocationInterval: [],
      alpConfig: []
    }));
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1920,
      height: 1080
    }));
  });

  afterEach(() => {
    mockUseSelectWindowDimension.mockClear();
  });
  it('Should render Table Grid Details ', () => {
    const wrapper = renderComponent({
      data: MockData
    });

    expect(
      wrapper.getByText('txt_manage_account_level_table_decision_business_name')
    ).toBeInTheDocument();
  });
});
