import { ORDER_BY_LIST, PAGE_SIZE } from 'app/constants/constants';
import { OrderBy } from 'app/constants/enums';
import { matchSearchValue } from 'app/helpers';
import { useTranslation } from 'app/_libraries/_dls';
import { DEFAULT_PAGE_NUMBER } from 'app/_libraries/_dls/components/Pagination/constants';
import get from 'lodash.get';
import _orderBy from 'lodash.orderby';
import { nanoid } from 'nanoid';
import {
  actionsWorkflowSetup,
  useSelectDecisionElementListData,
  useSelectElementMetadataManageTransactionTLP,
  useSelectTableListData
} from 'pages/_commons/redux/WorkflowSetup';
import { useCallback, useEffect, useMemo, useReducer } from 'react';
import { useDispatch } from 'react-redux';
import { getControlParameters, TableType } from './constant';
import { TableListModalProps } from './TableListModal';
import {
  CONFIG_ACTIONS_MODAL,
  EXPANDED_LIST,
  EXPAND_ACTION
} from './useConfigActions';

interface ITableListStateProps {
  page: number;
  pageSize: number;
  searchValue: string;
  orderBy: OrderByType;
  versionDetail: Record<string, any> | null;
}

const TABLE_LIST_INITIAL_STATE = {
  page: DEFAULT_PAGE_NUMBER,
  pageSize: PAGE_SIZE[0],
  searchValue: '',
  orderBy: OrderBy.ASC
} as ITableListStateProps;

const ACTION_TYPES = {
  CHANGE_PAGE: 'CHANGE_PAGE',
  CHANGE_PAGE_SIZE: 'CHANGE_PAGE_SIZE',
  CHANGE_SEARCH_VALUE: 'CHANGE_SEARCH_VALUE',
  CHANGE_ORDER_BY: 'CHANGE_ORDER_BY',
  SET_VERSION_DETAIL: 'SET_VERSION_DETAIL'
};

const tableListReducer = (state: ITableListStateProps, action: any) => {
  switch (action.type) {
    case ACTION_TYPES.CHANGE_PAGE:
      return { ...state, page: action.page };
    case ACTION_TYPES.CHANGE_PAGE_SIZE:
      return { ...state, pageSize: action.pageSize };
    case ACTION_TYPES.CHANGE_SEARCH_VALUE:
      return {
        ...state,
        searchValue: action.searchValue,
        page: DEFAULT_PAGE_NUMBER
      };
    case ACTION_TYPES.CHANGE_ORDER_BY:
      return {
        ...state,
        orderBy: action.orderBy,
        page: DEFAULT_PAGE_NUMBER
      };
    case ACTION_TYPES.SET_VERSION_DETAIL:
      return {
        ...state,
        versionDetail: action.versionDetail
      };
    default:
      return state;
  }
};

export const useTableListModal = (tableListProps: TableListModalProps) => {
  const {
    handleClose,
    selectedVersion,
    handleSetExpandedList,
    handleSelectVersion,
    handleOpenConfigModal,
    isCreateNewVersion,
    handleSetIsCreateNewVersion,
    draftNewTable,
    handleSetDraftNewTable,
    tableType
  } = tableListProps;

  const hasControlParameters = useMemo(() => {
    return tableType === TableType.alpaq;
  }, [tableType]);

  const dispatch = useDispatch();
  const { t } = useTranslation();

  const data = useSelectDecisionElementListData(`ALP${tableType}`);

  const decisionElementList = data?.decisionElementList || [];

  const metadata = useSelectElementMetadataManageTransactionTLP();

  const defaultControlParameters = useMemo(() => {
    return getControlParameters(tableType);
  }, [tableType]);

  const { searchCode } = metadata;
  const [state, reactDispatch] = useReducer(
    tableListReducer,
    TABLE_LIST_INITIAL_STATE
  );

  const { page, pageSize, searchValue, orderBy, versionDetail } = state;

  const { tableList = [], loading, error } = useSelectTableListData();

  const handleGetTableList = useCallback(() => {
    dispatch(
      actionsWorkflowSetup.getTableList({
        tableType,
        serviceNameAbbreviation: 'ALP'
      })
    );
  }, [dispatch, tableType]);

  useEffect(() => {
    handleGetTableList();
  }, [handleGetTableList]);

  const handleChangePage = useCallback((page: number) => {
    reactDispatch({ type: ACTION_TYPES.CHANGE_PAGE, page });
  }, []);
  const handleChangePageSize = useCallback(
    (pageSize: number) => {
      reactDispatch({ type: ACTION_TYPES.CHANGE_PAGE_SIZE, pageSize });
      handleChangePage(DEFAULT_PAGE_NUMBER);
    },
    [handleChangePage]
  );

  const handleSearch = useCallback(
    (val: string) => {
      if (searchValue === val) return;
      reactDispatch({
        type: ACTION_TYPES.CHANGE_SEARCH_VALUE,
        searchValue: val
      });
    },
    [searchValue]
  );
  const handleChangeOrderBy = useCallback((orderBy: OrderByType) => {
    reactDispatch({
      type: ACTION_TYPES.CHANGE_ORDER_BY,
      orderBy
    });
  }, []);

  const _searchValue = searchValue.toLowerCase() || '';
  const _tablesOrdered = _orderBy(tableList, 'tableId', orderBy);
  const _tablesFiltered = _tablesOrdered
    .filter((w: any) => {
      const listVersionName = Array.isArray(w?.versions)
        ? w?.versions.map((v: any) => v?.tableName)
        : [];
      return matchSearchValue([w?.tableId, ...listVersionName], _searchValue);
    })
    .map((w: any) => {
      const sortedVersions = _orderBy(w.versions, 'effectiveDate', 'desc');
      return { ...w, versions: sortedVersions };
    });

  const total = _tablesFiltered.length;
  const tables = _tablesFiltered.slice(
    (page! - 1) * pageSize!,
    page! * pageSize!
  );

  const oderByItem = ORDER_BY_LIST.find(i => i.value === orderBy);

  const handleChangePageToSelectedVersion = useCallback(() => {
    if (!Array.isArray(tableList) || tableList.length === 0 || !selectedVersion)
      return;
    const selectedVersionIndex = _tablesOrdered.findIndex(
      (table: any) => table.tableId === selectedVersion?.tableId
    );
    if (selectedVersionIndex === -1) return;
    const selectedVersionPage = Math.ceil(
      (selectedVersionIndex + 1) / pageSize
    );
    handleChangePage(selectedVersionPage);

    handleSetExpandedList(EXPANDED_LIST.tableListModal)({
      expandAction: EXPAND_ACTION.replace,
      expandedList: { [selectedVersion?.tableId]: true }
    });

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tableList, selectedVersion]);

  useEffect(() => {
    handleChangePageToSelectedVersion();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tableList]);

  const handleSetVersionDetail = useCallback((versionDetail: any) => {
    reactDispatch({
      type: ACTION_TYPES.SET_VERSION_DETAIL,
      versionDetail
    });
  }, []);

  const handleClearAndReset = () => {
    handleSearch('');
    if (selectedVersion) {
      handleChangePageToSelectedVersion();
      return;
    }
    handleSetExpandedList(EXPANDED_LIST.tableListModal)({
      expandAction: EXPAND_ACTION.replace,
      expandedList: {}
    });
    handleChangePageSize(PAGE_SIZE[0]);
  };

  const [title, helpText1] = useMemo(() => {
    let title, helpText1;
    if (isCreateNewVersion) {
      title = t('txt_workflow_manage_change_in_terms_new_version');
      helpText1 = 'txt_manage_transaction_level_aq_tableList_help_text';
    } else {
      title = t(
        'txt_manage_transaction_level_tlp_config_tq_choose_table_to_model'
      );
      helpText1 =
        'txt_manage_transaction_level_aq_choose_table_to_model_help_text';
    }
    return [title, helpText1];
  }, [isCreateNewVersion, t]);

  const getSearchCodeText = (codeValue: string) => {
    const searchCodeOption = searchCode.find(code => code.code === codeValue);
    return searchCodeOption?.text || '';
  };

  const handleGoToNextStep = (version?: any) => () => {
    handleOpenConfigModal(CONFIG_ACTIONS_MODAL.addNewTableModal);
    if (version) handleSelectVersion(version);
    const versionSelect = version || selectedVersion;
    if (
      !draftNewTable ||
      (draftNewTable &&
        draftNewTable.selectedVersionId !== versionSelect?.version?.tableId)
    ) {
      const elementListSelected = get(
        versionSelect,
        ['version', 'decisionElements'],
        []
      );

      const controlParametersSelected = get(
        versionSelect,
        ['version', 'tableControlParameters'],
        []
      );

      const newControlParameters = defaultControlParameters.map(parameter => {
        const matchItem = controlParametersSelected.find(
          (item: any) => item.name === parameter.id
        );
        return matchItem
          ? { ...parameter, value: matchItem?.value }
          : parameter;
      });

      const newElementList = elementListSelected
        .map((el: any) => {
          const matchElement =
            decisionElementList.find(
              (element: any) => element.name === el.name
            ) || {};

          return {
            ...el,
            ...matchElement,
            rowId: nanoid(),
            searchCodeText: getSearchCodeText(el?.searchCode)
          };
        })
        .filter((el: any) => el.configurable);

      handleSetDraftNewTable({
        selectedVersionId: versionSelect?.version?.tableId,
        elementList: newElementList,
        selectedTableId: versionSelect?.tableId,
        controlParameters: newControlParameters
      });
    }
  };

  const handleToggleExpandedTableListModal = useCallback(
    (tableId: string) => {
      return handleSetExpandedList(EXPANDED_LIST.tableListModal)({
        expandAction: EXPAND_ACTION.toggle,
        expandKey: tableId
      });
    },
    [handleSetExpandedList]
  );

  const handleSelectTableVersion = useCallback(
    (tableId: string, isViewDetail?: boolean) => (version: any) => {
      isViewDetail
        ? handleSetVersionDetail({ tableId, version })
        : handleSelectVersion({ tableId, version });
    },
    [handleSelectVersion, handleSetVersionDetail]
  );

  const handleCloseModalVersionDetail = () => {
    handleSetVersionDetail(null);
  };

  useEffect(() => {
    return () => {
      handleSetExpandedList(EXPANDED_LIST.tableListModal)({
        expandAction: EXPAND_ACTION.replace,
        expandedList: {}
      });
    };
  }, [handleSetExpandedList]);

  const handleCloseAndClearState = () => {
    handleClose();
    handleSetExpandedList(EXPANDED_LIST.tableListModal)({
      expandedList: {},
      expandAction: EXPAND_ACTION.replace
    });
    handleSelectVersion(null);
    handleSetIsCreateNewVersion(false);
  };

  return {
    tables,
    total,
    page,
    pageSize,
    handleChangePage,
    handleChangePageSize,
    handleSearch,
    orderBy: oderByItem,
    searchValue,
    handleChangeOrderBy,
    loading,
    error,
    handleGetTableList,
    handleClearAndReset,
    versionDetail,
    dispatch: reactDispatch,
    title,
    helpText1,
    handleGoToNextStep,
    handleToggleExpandedTableListModal,
    handleSelectTableVersion,
    handleCloseModalVersionDetail,
    handleCloseAndClearState,
    hasControlParameters
  };
};
