import { MethodFieldParameterEnum } from 'app/constants/enums';
import {
  confirmEditWhenChangeEffectToUploadProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { useUnsavedChangeRegistry } from 'app/hooks';
import { InlineMessage, useTranslation } from 'app/_libraries/_dls';
import { StateFile } from 'app/_libraries/_dls/components/Upload/File';
import { STATUS } from 'app/_libraries/_dls/components/Upload/helper';
import { isEqual, omit } from 'lodash';
import get from 'lodash.get';
import isEmpty from 'lodash.isempty';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import React, { useEffect, useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import ActionButtons from './ActionButtons';
import AddNewTableModal from './AddNewTableModal';
import { TableType } from './constant';
import RemoveTableModal from './RemoveTableModal';
import TableList from './TableList';
import TableListModal from './TableListModal';
import {
  CONFIG_ACTIONS_MODAL,
  EXPANDED_LIST,
  EXPAND_ACTION,
  useConfigAction
} from './useConfigActions';

export interface ConfigureParametersFormValue {
  isValid?: boolean;
  tables?: (any & { rowId?: number })[];
}

interface IDependencies {
  files?: StateFile[];
}

const ConfigureParameters: React.FC<
  WorkflowSetupProps<ConfigureParametersFormValue, IDependencies> & {
    tableType: TableType;
  }
> = ({
  formValues,
  setFormValues,
  stepId,
  selectedStep,
  tableType,
  isStuck,
  ...props
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const tables = formValues.tables || [];
  const {
    openedModal,
    handleOpenConfigModal,
    selectedVersion,
    handleSetExpandedList,
    handleSetSelectedVersion,
    expandedList,
    isCreateNewVersion,
    handleSetIsCreateNewVersion,
    handleSetDraftNewTable,
    draftNewTable,
    handleAddNewTable,
    tableList,
    handleSetSelectedTable,
    selectedTable,
    handleEditTable,
    handleDeleteTable,
    handleCheckDuplicateTableId,
    tableIndexChanged
  } = useConfigAction(tables, tableType);

  const [isShowConfirmAffectFile, setShowConfirmAffectFile] = useState(false);
  const [originTable, setOriginTable] = useState(tables);

  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;

  useEffect(() => {
    dispatch(actionsWorkflowSetup.getStrategiesList({}));
  }, [dispatch]);

  useEffect(() => {
    setFormValues({ tables: tableList });
  }, [tableList, setFormValues]);

  useEffect(() => {
    const isValid = (formValues?.tables?.length || 0) > 0;
    if (formValues.isValid === isValid) return;

    keepRef.current.setFormValues({ isValid });
  }, [formValues]);

  useEffect(() => {
    if (stepId === selectedStep) return;
    handleSetExpandedList(EXPANDED_LIST.tableListIndex)({
      expandAction: EXPAND_ACTION.replace,
      expandedList: {}
    });
  }, [stepId, handleSetExpandedList, selectedStep]);

  useEffect(() => {
    setFormValues({ tables: tableList });

    if (isEmpty(props?.dependencies?.files)) return;

    if (originTable.length !== tableList.length) {
      setShowConfirmAffectFile(true);
      return;
    }

    const isChange = originTable.some((table, index) => {
      const parameterFlag = table?.controlParameters?.find(
        (item: MagicKeyValue) =>
          item?.id === MethodFieldParameterEnum.ChangeInTermsMethodFlag
      );
      const parameterFlagInit = tableList[index]?.controlParameters?.find(
        (item: MagicKeyValue) =>
          item?.id === MethodFieldParameterEnum.ChangeInTermsMethodFlag
      );

      return !isEqual(
        {
          flag: parameterFlag,
          tableId: table?.tableId,
          elementList: table?.elementList?.map((element: Element) =>
            omit(element, 'rowId')
          )
        },
        {
          flag: parameterFlagInit,
          tableId: tableList[index]?.tableId,
          elementList: tableList[index]?.elementList?.map((element: Element) =>
            omit(element, 'rowId')
          )
        }
      );
    });

    setShowConfirmAffectFile(isChange);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tableList, setFormValues]);

  useEffect(() => {
    if (isEmpty(props?.dependencies?.files)) return;
    setOriginTable(tableList);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props?.dependencies?.files]);

  const handleResetUploadStep = () => {
    const file = props?.dependencies?.files?.[0];
    const attachmentId = file?.idx;
    const isValid = file?.status === STATUS.valid;
    props?.clearFormValues(get(props, 'clearFormName', ''));
    isValid &&
      dispatch(actionsWorkflowSetup.deleteTemplateFile({ attachmentId }));
    setShowConfirmAffectFile(false);
  };

  useUnsavedChangeRegistry(
    {
      ...confirmEditWhenChangeEffectToUploadProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_ACCOUNT_LEVEL__AQ__CONFIGURE_PARAMETERS_CONFIRM_EDIT,
      priority: 1
    },
    [!isEmpty(props?.dependencies?.files) && isShowConfirmAffectFile],
    handleResetUploadStep
  );

  return (
    <div>
      {isStuck && (
        <InlineMessage className="mb-0 mt-24" variant="danger" withIcon>
          {t('txt_step_stuck_move_forward_message')}
        </InlineMessage>
      )}
      <div className="mt-24">
        <ActionButtons
          small={!isEmpty(tableList)}
          title="Table List"
          onClickAddNewTable={() =>
            handleOpenConfigModal(CONFIG_ACTIONS_MODAL.addNewTableModal)
          }
          onClickTableToModel={() =>
            handleOpenConfigModal(CONFIG_ACTIONS_MODAL.tableListModal)
          }
          onClickCreateNewVersion={() => {
            handleOpenConfigModal(CONFIG_ACTIONS_MODAL.tableListModal);
            handleSetIsCreateNewVersion(true);
          }}
        />
      </div>
      {tableList && tableList.length > 0 && (
        <TableList
          tableList={tableList}
          handleSetSelectedTable={handleSetSelectedTable}
          handleOpenConfigModal={handleOpenConfigModal}
          handleSetExpandedList={handleSetExpandedList}
          expandedList={expandedList[EXPANDED_LIST.tableListIndex]}
          tableIndexChanged={tableIndexChanged}
          stepId={stepId}
          selectedStep={selectedStep}
        />
      )}

      {openedModal === CONFIG_ACTIONS_MODAL.deleteTableModal && (
        <RemoveTableModal
          id="removeMethod"
          show
          onOk={() => handleDeleteTable(selectedTable?.rowId)}
          onClose={() => {
            handleOpenConfigModal(null);
            handleSetSelectedTable(null);
          }}
        />
      )}
      {openedModal === CONFIG_ACTIONS_MODAL.addNewTableModal && (
        <AddNewTableModal
          id="addNewMethod"
          show
          handleClose={() => handleOpenConfigModal(null)}
          selectedVersion={selectedVersion}
          handleOpenConfigModal={handleOpenConfigModal}
          handleSelectVersion={handleSetSelectedVersion}
          handleSetDraftNewTable={handleSetDraftNewTable}
          draftNewTable={draftNewTable}
          isCreateNewVersion={isCreateNewVersion}
          handleAddNewTable={handleAddNewTable}
          handleSetIsCreateNewVersion={handleSetIsCreateNewVersion}
          selectedTable={selectedTable}
          handleEditTable={handleEditTable}
          handleSetSelectedTable={handleSetSelectedTable}
          handleSetExpandedList={handleSetExpandedList}
          handleCheckDuplicateTableId={handleCheckDuplicateTableId}
          tableType={tableType}
          enableShowConfirmFileStep={setShowConfirmAffectFile}
        />
      )}
      {openedModal === CONFIG_ACTIONS_MODAL.tableListModal && (
        <TableListModal
          id="editMethodFromModel"
          show
          handleClose={() => handleOpenConfigModal(null)}
          selectedVersion={selectedVersion}
          handleSetExpandedList={handleSetExpandedList}
          handleSelectVersion={handleSetSelectedVersion}
          expandedList={expandedList[EXPANDED_LIST.tableListModal]}
          handleOpenConfigModal={handleOpenConfigModal}
          isCreateNewVersion={isCreateNewVersion}
          handleSetIsCreateNewVersion={handleSetIsCreateNewVersion}
          handleSetDraftNewTable={handleSetDraftNewTable}
          draftNewTable={draftNewTable}
          tableType={tableType}
        />
      )}
    </div>
  );
};

export default ConfigureParameters;
