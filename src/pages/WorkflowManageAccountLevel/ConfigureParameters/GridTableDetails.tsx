import { MethodFieldParameterEnum } from 'app/constants/enums';
import { formatCommon } from 'app/helpers';
import {
  ColumnType,
  Grid,
  Icon,
  Tooltip,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import { useSelectElementMetadataManageAcountLevel } from 'pages/_commons/redux/WorkflowSetup';
import React from 'react';
import { parameterGroup } from './parameterList';

interface IProps {
  isShowFlyout?: boolean;
  data: TableContentType[];
}

const GridTableDetail: React.FC<IProps> = ({
  isShowFlyout = false,
  data = []
}) => {
  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();
  const metadata = useSelectElementMetadataManageAcountLevel();
  const {
    allocationBeforeAfterCycle,
    allocationFlag,
    changeInTermsMethodFlag
  } = metadata;

  const columns: ColumnType[] = [
    {
      id: 'businessName',
      Header: t('txt_manage_account_level_table_decision_business_name'),
      accessor: ({ businessName }) => t(businessName),
      width: isShowFlyout ? 230 : 233
    },
    {
      id: 'greenScreenName',
      Header: t('txt_manage_account_level_table_decision_green_name'),
      accessor: ({ greenScreenName }) => t(greenScreenName),
      width: isShowFlyout ? 200 : 244
    },
    {
      id: 'value',
      Header: t('txt_manage_account_level_table_decision_value'),
      accessor: ({ id }) => {
        const val = data?.filter(item => item?.name === id)[0];
        if (id === MethodFieldParameterEnum.AllocationDate) {
          return formatCommon(val?.value as any).time.date;
        }
        if (id === MethodFieldParameterEnum.AllocationFlag) {
          const valTemp = allocationFlag.find(
            item => item?.code === val?.value
          );
          const flag = valTemp?.code === '' ? valTemp?.code : valTemp?.text;
          return (
            <TruncateText
              resizable
              lines={2}
              ellipsisLessText={t('txt_less')}
              ellipsisMoreText={t('txt_more')}
            >
              {flag}
            </TruncateText>
          );
        }
        if (id === MethodFieldParameterEnum.AllocationBeforeAfterCycle) {
          const valTemp = allocationBeforeAfterCycle.filter(
            item => item?.code === val?.value
          )[0];
          const before = valTemp?.code === '' ? valTemp?.code : valTemp?.text;
          return (
            <TruncateText
              resizable
              lines={2}
              ellipsisLessText={t('txt_less')}
              ellipsisMoreText={t('txt_more')}
            >
              {before}
            </TruncateText>
          );
        }
        if (id === MethodFieldParameterEnum.ChangeInTermsMethodFlag) {
          const valTemp = changeInTermsMethodFlag.filter(
            item => item?.code === val?.value
          )[0];
          const changeInTerm =
            valTemp?.code === '' ? valTemp?.code : valTemp?.text;
          return (
            <TruncateText
              resizable
              lines={2}
              ellipsisLessText={t('txt_less')}
              ellipsisMoreText={t('txt_more')}
            >
              {changeInTerm}
            </TruncateText>
          );
        }
        return val?.value;
      },
      width: isShowFlyout ? 300 : width < 1280 ? 320 : 360
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      cellBodyProps: { className: 'pt-12 pb-8' },
      width: 106,
      accessor: ({ moreInfo }) => (
        <Tooltip element={t(moreInfo)}>
          <Icon name="information" size="5x" className="color-grey-l16" />
        </Tooltip>
      )
    }
  ];

  return <Grid columns={columns} data={parameterGroup} />;
};

export default GridTableDetail;
