import { act, renderHook } from '@testing-library/react-hooks';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageAccountLevel';
import { getControlParameters, TableType } from './constant';
import { FieldType, useControlParameters } from './useControlParameters';

const useSelectDecisionElementListData = jest.spyOn(
  WorkflowSetup,
  'useSelectElementMetadataManageAcountLevel'
);

jest.mock('react', () => {
  const originReact = jest.requireActual('react');
  const mockUseRef = () => ({ current: { clear: jest.fn() } });
  return {
    ...originReact,
    useRef: mockUseRef
  };
});

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

describe('WorkflowManageTransactionLevelProcessingDecisionTablesTLP > ConfigureParameters > useControlParameters', () => {
  const controlParameterList = getControlParameters(TableType.alpaq);
  const handleChangeControlParameters = jest.fn();
  const tableType = TableType.alpaq;
  const defaultProps = {
    controlParameterList,
    handleChangeControlParameters,
    tableType
  } as any;
  beforeEach(() => {
    useSelectDecisionElementListData.mockImplementation(() => ({
      searchCode: [],
      allocationFlag: [{ code: '2', text: '2' }],
      allocationBeforeAfterCycle: [{ code: 'B', value: 'B' }],
      changeInTermsMethodFlag: [{ code: 'N', value: 'N' }],
      allocationInterval: [],
      alpConfig: []
    }));
  });

  // it('Run Default reducer function', () => {
  //   //const { result } = renderHook(() => useControlParameters(defaultProps));
  //   // act(() => {
  //   //   result.current.dispatch({ type: 'Test Action' });
  //   // });
  // });

  it('Run normal hook', () => {
    const { result } = renderHook(() => useControlParameters(defaultProps));
    act(() => {
      result.current.handleChangeSearchValue('123');
    });
    expect(result.current.searchValue).toEqual('123');

    act(() => {
      result.current.handleChangeParametersValue(
        MethodFieldParameterEnum.NextDebitDate,
        FieldType.datePicker
      )({ target: { value: '22/12/2022' } });

      result.current.handleChangeParametersValue(
        MethodFieldParameterEnum.IncludeActivityAction,
        FieldType.dropDown
      )({ target: { value: { code: '123' } } });

      result.current.handleChangeParametersValue(
        MethodFieldParameterEnum.PosPromotionValidation,
        FieldType.textBox
      )({ target: { value: { code: '123' } } });
      result.current.handleChangeParametersValue(
        MethodFieldParameterEnum.AllocationFlag,
        FieldType.comBoBox
      )({ target: { value: { code: '123' } } });
    });
    // expect(handleChangeControlParameters).toBeCalled();
  });
});
