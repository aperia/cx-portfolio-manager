import { act, renderHook } from '@testing-library/react-hooks';
import * as WorkflowSetupAccountLevel from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageAccountLevel';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageTransactionLevelProcessingDecisionTLP';
import * as reactRedux from 'react-redux';
import { TableType } from './constant';
import { useTableListModal } from './useTableListModal';

const useSelectElementMetadataManageTransactionTLP = jest.spyOn(
  WorkflowSetup,
  'useSelectElementMetadataManageTransactionTLP'
);

const useSelectDecisionElementListData = jest.spyOn(
  WorkflowSetup,
  'useSelectDecisionElementListData'
);

const mockOnConfirmParams = jest.fn();
jest.mock('app/hooks/useUnsavedChangesRedirect', () => {
  return {
    __esModule: true,
    useUnsavedChangesRedirect:
      () =>
      ({ onConfirm, onDiscard, onCancel }: any) => {
        onDiscard && onDiscard();
        onCancel && onCancel();
        onConfirm && onConfirm(mockOnConfirmParams());
      }
  };
});

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');
const TableListMockData = [
  {
    tableId: 'LUMEJ598',
    tableName: '8A41RDTB',
    versions: [
      {
        tableId: 'IFAA8XEE',
        tableName: 'WFRKLMZB',
        effectiveDate: '2021-05-31T17:00:00Z',
        status: 'Production',
        comment: 'at tempor accusam',
        tableControlParameters: [
          { name: 'include.activity.option', value: 'N' }
        ],
        decisionElements: [{ name: 'AGE', value: [1, 2, 3, 4] }]
      },
      {
        tableId: 'K1TIBA4C',
        tableName: 'ZHFMK7K2',
        effectiveDate: '2021-05-28T17:00:00Z',
        status: 'Client Approved',
        comment: 'eu labore congue ea',
        tableControlParameters: [],
        decisionElements: []
      },
      {
        tableId: 'WMAEVM7G',
        tableName: '91IDUIVL',
        effectiveDate: '2021-10-17T17:00:00Z',
        status: 'Validating',
        comment: 'nulla aliquip cum magna iriure ex velit',
        tableControlParameters: [],
        decisionElements: []
      }
    ]
  },
  {
    tableId: 'LUMEJ5F',
    tableName: '8A41RDTH',
    versions: [
      {
        tableId: 'IFAA8XEE',
        tableName: 'WFRKLMZB',
        effectiveDate: '2021-05-31T17:00:00Z',
        status: 'Production',
        comment: 'at tempor accusam',
        tableControlParameters: [],
        decisionElements: []
      },
      {
        tableId: 'K1TIBA4DD',
        tableName: 'ZHFMK7EE',
        effectiveDate: '2021-05-28T17:00:00Z',
        status: 'Client Approved',
        comment: 'eu labore congue ea',
        tableControlParameters: [],
        decisionElements: []
      },
      {
        tableId: 'WMAEVMKK',
        tableName: '91IDUIVL',
        effectiveDate: '2021-10-17T17:00:00Z',
        status: 'Validating',
        comment: 'nulla aliquip cum magna iriure ex velit',
        tableControlParameters: [
          { name: 'include.activity.option', value: 'N' }
        ],
        decisionElements: [{ name: 'AGE', value: [1, 2, 3, 4] }]
      }
    ]
  },
  {
    tableId: 'LUMEJ5F',
    tableName: '8A41RDTH'
  }
] as any;

const SelectedVersion = {
  tableId: 'LUMEJ598',
  version: {
    tableId: 'IFAA8XEE',
    tableName: 'WFRKLMZB',
    effectiveDate: '2021-05-31T17:00:00Z',
    status: 'Production',
    comment: 'at tempor accusam',
    tableControlParameters: [
      { name: 'include.activity.option', value: 'N' },
      { name: 'test', value: '' }
    ],
    decisionElements: [
      { name: 'AGE', value: [1, 2, 3, 4] },
      { name: 'test', value: [] }
    ]
  }
} as any;

const useSelectTableListData = jest.spyOn(
  WorkflowSetupAccountLevel,
  'useSelectTableListData'
);

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

const MockSearchCode = [
  {
    code: 'E',
    text: 'E - Exact'
  },
  {
    code: 'R',
    text: 'R - Range'
  }
];

describe('WorkflowManageTransactionLevelProcessingDecisionTablesTLP > ConfigureParametersTQ > useTableListModal', () => {
  const mockDispatch = jest.fn();
  beforeEach(() => {
    useSelectElementMetadataManageTransactionTLP.mockReturnValue({
      searchCode: MockSearchCode,
      includeActivityOptions: [],
      posPromoValidationOptions: []
    });
    mockUseDispatch.mockImplementation(() => mockDispatch);
  });

  const handleClose = jest.fn();
  const handleOpenConfigModal = jest.fn();
  const handleSelectVersion = jest.fn();
  const draftNewTable = null;
  const handleSetDraftNewTable = jest.fn();
  const isCreateNewVersion = false;
  const selectedVersion = null;
  const handleSetIsCreateNewVersion = jest.fn();
  const handleSetExpandedList = () => jest.fn();
  const tableType = '';

  const defaultProps = {
    handleClose,
    handleOpenConfigModal,
    handleSelectVersion,
    draftNewTable,
    handleSetDraftNewTable,
    isCreateNewVersion,
    handleSetIsCreateNewVersion,
    handleSetExpandedList,
    tableType,
    selectedVersion
  } as any;

  it('Run Default reducer function', () => {
    useSelectTableListData.mockReturnValue({
      tableList: [{ id: '1' }, { id: '2' }]
    });
    const { result } = renderHook(() =>
      useTableListModal({
        ...defaultProps,
        tableType: TableType.alpaq,
        isCreateNewVersion: true
      })
    );
    act(() => {
      result.current.dispatch({ type: 'Test Action' });
    });
  });

  it('Should run normal hook', () => {
    useSelectTableListData.mockReturnValue({
      tableList: TableListMockData
    });
    const { result } = renderHook(() =>
      useTableListModal({
        ...defaultProps,
        tableType: TableType.alpaq
      })
    );
    act(() => {
      result.current.handleChangePage(2);
    });
    expect(result.current.page).toEqual(2);

    act(() => {
      result.current.handleChangePageSize(10);
    });
    expect(result.current.pageSize).toEqual(10);

    act(() => {
      result.current.handleChangeOrderBy('desc');
      //result.current.handleChangeSortBy('effectiveDate');
      result.current.handleSearch('abc');
      result.current.handleSearch('abc');
    });
    expect(result.current.searchValue).toEqual('abc');

    act(() => {
      result.current.handleClearAndReset();
    });
    expect(result.current.searchValue).toEqual('');
  });

  it('It should render hook with selected version', () => {
    useSelectTableListData.mockReturnValue({
      tableList: TableListMockData
    });
    useSelectDecisionElementListData.mockReturnValue({
      decisionElementList: [
        {
          name: 'AGE',
          moreInfo:
            'Principal cardholder’s age expressed in whole number of years; variable-length, 3 positions, numeric.\n To determine whether a cardholder account matches a value, the System subtracts the year of birth from the current year. The System adds one year if the month of birth has already passed. If the date of birth consists of zeros, the age is 0.',
          immediateAllocation: 'No',
          configurable: true
        }
      ]
    });
    const { result } = renderHook(() =>
      useTableListModal({
        ...defaultProps,
        selectedVersion: SelectedVersion,
        tableType: TableType.alpaq
      })
    );

    act(() => {
      result.current.handleSearch('abc');
      result.current.handleClearAndReset();
    });
  });

  it('It should render hook with selected version and TLPCA table type', () => {
    useSelectTableListData.mockReturnValue({
      tableList: TableListMockData
    });
    useSelectDecisionElementListData.mockReturnValue({
      decisionElementList: [
        {
          name: 'AGE',
          moreInfo:
            'Principal cardholder’s age expressed in whole number of years; variable-length, 3 positions, numeric.\n To determine whether a cardholder account matches a value, the System subtracts the year of birth from the current year. The System adds one year if the month of birth has already passed. If the date of birth consists of zeros, the age is 0.',
          immediateAllocation: 'No',
          configurable: true
        }
      ]
    });
    const { result } = renderHook(() =>
      useTableListModal({
        ...defaultProps,
        selectedVersion: {},
        tableType: TableType.alpaq,
        isCreateNewVersion: true
      })
    );

    act(() => {
      result.current.handleSearch('abc');
      result.current.handleClearAndReset();
    });

    act(() => {
      result.current.handleSelectTableVersion('123', true)('version' as any);
      result.current.handleSelectTableVersion('123')({} as any);
    });
    expect(result.current.versionDetail.tableId).toEqual('123');

    act(() => {
      result.current.handleGoToNextStep()();
    });
    expect(handleOpenConfigModal).toBeCalled();
  });

  it('It should render hook with selected version Not In List', () => {
    useSelectTableListData.mockReturnValue({
      tableList: TableListMockData
    });
    useSelectDecisionElementListData.mockReturnValue({
      decisionElementList: [
        {
          name: 'AGE',
          moreInfo:
            'Principal cardholder’s age expressed in whole number of years; variable-length, 3 positions, numeric.\n To determine whether a cardholder account matches a value, the System subtracts the year of birth from the current year. The System adds one year if the month of birth has already passed. If the date of birth consists of zeros, the age is 0.',
          immediateAllocation: 'No',
          configurable: true
        }
      ]
    });
    const { result } = renderHook(() =>
      useTableListModal({
        ...defaultProps,
        tableType: TableType.alpaq,
        isCreateNewVersion: true
      })
    );

    act(() => {
      result.current.handleGoToNextStep(SelectedVersion as any)();
    });

    expect(handleSelectVersion).toBeCalled();
  });

  it('It should render hook with Draft New Table', () => {
    useSelectTableListData.mockReturnValue({
      tableList: TableListMockData
    });
    useSelectDecisionElementListData.mockReturnValue({
      decisionElementList: [
        {
          name: 'AGE',
          moreInfo:
            'Principal cardholder’s age expressed in whole number of years; variable-length, 3 positions, numeric.\n To determine whether a cardholder account matches a value, the System subtracts the year of birth from the current year. The System adds one year if the month of birth has already passed. If the date of birth consists of zeros, the age is 0.',
          immediateAllocation: 'No',
          configurable: true
        }
      ]
    });
    const { result } = renderHook(() =>
      useTableListModal({
        ...defaultProps,
        tableType: TableType.alpaq,
        draftNewTable: { selectedVersionId: 'IFAA8XEE' },
        selectedVersion: SelectedVersion
      })
    );

    act(() => {
      result.current.handleGoToNextStep()();
    });
    expect(result.current.helpText1).toEqual(
      'txt_manage_transaction_level_aq_choose_table_to_model_help_text'
    );

    // act(() => {
    //   result.current.handleCloseModal();
    // });
    //expect(handleClose).toBeCalled();

    act(() => {
      result.current.handleCloseModalVersionDetail();
      result.current.handleCloseAndClearState();
    });
    expect(result.current.versionDetail).toEqual(null);
  });

  it('It should render hook with table type TLPRQ', () => {
    useSelectTableListData.mockReturnValue({
      tableList: TableListMockData
    });
    const { result } = renderHook(() =>
      useTableListModal({
        ...defaultProps,
        tableType: TableType.alpaq,
        isCreateNewVersion: true
      })
    );

    expect(result.current.helpText1).toEqual(
      'txt_manage_transaction_level_aq_tableList_help_text'
    );

    act(() => {
      result.current.handleToggleExpandedTableListModal('test');
    });
  });

  it('It should render hook with table type TLPRQ', () => {
    useSelectTableListData.mockReturnValue({
      tableList: [{ test: 'test' }]
    });
    const { result } = renderHook(() =>
      useTableListModal({
        ...defaultProps,
        tableType: TableType.alpaq
      })
    );

    expect(result.current.helpText1).toEqual(
      'txt_manage_transaction_level_aq_choose_table_to_model_help_text'
    );
  });

  it('It should render hook with no table type TLPRQ', () => {
    useSelectTableListData.mockReturnValue({
      tableList: [{ test: 'test' }]
    });
    const { result } = renderHook(() =>
      useTableListModal({
        ...defaultProps
      })
    );

    expect(result.current.helpText1).toEqual(
      'txt_manage_transaction_level_aq_choose_table_to_model_help_text'
    );
  });

  it('It should render hook with no table type TLPCA', () => {
    useSelectTableListData.mockReturnValue({
      tableList: [{ test: 'test' }]
    });
    const { result } = renderHook(() =>
      useTableListModal({
        ...defaultProps,
        tableType: TableType.alpaq
      })
    );

    expect(result.current.total).toEqual(0);
  });
});
