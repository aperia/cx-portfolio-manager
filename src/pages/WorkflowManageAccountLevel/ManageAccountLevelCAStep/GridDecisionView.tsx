import {
  ColumnType,
  Grid,
  Icon,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import {
  useSelectDecisionListData,
  useSelectElementMetadataManageAcountLevel
} from 'pages/_commons/redux/WorkflowSetup';
import React, { useRef } from 'react';

interface IProps {
  data: DecisionType[];
}
const GridDecision: React.FC<IProps> = ({ data = [] }) => {
  const { t } = useTranslation();
  const gridRef = useRef<any>(null);
  const { width } = useSelectWindowDimension();

  const decisionList = useSelectDecisionListData()?.decisionList;
  const metadata = useSelectElementMetadataManageAcountLevel();

  const dataList = data?.filter((item: MagicKeyValue) => {
    const val = decisionList?.find((i: DecisionType) => i?.name === item?.name);
    return val?.configurable;
  });

  const columns: ColumnType[] = [
    {
      id: 'name',
      Header: t('txt_manage_account_level_table_decision_element'),
      width: width > 1280 ? undefined : 280,
      accessor: ({ name }) => name
    },
    {
      id: 'searchCode',
      Header: t('txt_manage_account_level_table_decision_search'),
      width: width > 1280 ? 240 : 210,
      accessor: ({ searchCode }) => {
        const value = metadata.searchCode.filter(
          (val: MagicKeyValue) => val.code === searchCode
        )[0]?.text;
        return value;
      }
    },
    {
      id: 'immediateAllocation',
      Header: t('txt_manage_account_level_table_decision_immediate_allocation'),
      width: 182,
      accessor: ({ immediateAllocation }) =>
        ['Yes', 'N'].includes(immediateAllocation) ? 'Y' : 'N'
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      cellBodyProps: { className: 'pt-12 pb-8' },
      width: 106,
      accessor: ({ name }) => {
        const decision = decisionList?.filter(
          (item: MagicKeyValue) => item?.name === name
        )[0];
        return (
          <Tooltip element={decision?.moreInfo}>
            <Icon name="information" size="5x" className="color-grey-l16" />
          </Tooltip>
        );
      }
    }
  ];

  return <Grid ref={gridRef} columns={columns} data={dataList} />;
};

export default GridDecision;
