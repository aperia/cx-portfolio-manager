import { fireEvent, render, RenderResult } from '@testing-library/react';
import { mockUseModalRegistryFnc } from 'app/utils';
import 'app/utils/_mockComponent/mockNoDataFound';
import 'app/utils/_mockComponent/mockPaging';
import * as mockMetaData from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageAccountLevel';
import React from 'react';
import * as reactRedux from 'react-redux';
import ViewModal from './ViewModal';

const mockMeta = jest.spyOn(
  mockMetaData,
  'useSelectElementMetadataManageAcountLevel'
);

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

jest.mock('./MethodListModal', () => {
  const MethodListModal = () => {
    return (
      <div>
        <div>MethodListModal_component</div>
      </div>
    );
  };

  const StatusName = {
    production: 'production',
    clientApproved: 'client approved',
    scheduled: 'scheduled',
    working: 'working',
    validating: 'validating',
    previous: 'previous',
    lapsed: 'lapsed'
  };

  return {
    __esModule: true,
    default: MethodListModal,
    StatusName
  };
});

jest.mock('./GridTableDetails', () => {
  return {
    __esModule: true,
    default: () => {
      return (
        <div>
          <div>GridTableDetails_component</div>
        </div>
      );
    }
  };
});

jest.mock('../ConfigureParameters/GridTableDetails', () => {
  return {
    __esModule: true,
    default: () => {
      return (
        <div data-testid="GridTableDetailAQ">
          <div>GridTableDetailAQ </div>
        </div>
      );
    }
  };
});

const mockUseModalRegistry = mockUseModalRegistryFnc();
const mockUseSelector = jest.spyOn(reactRedux, 'useSelector');

const defaultProps = {
  tableId: '123',
  data: {},
  onCancel: jest.fn(),
  onOK: jest.fn()
};

const renderModal = (props: any): RenderResult => {
  return render(
    <div>
      <ViewModal {...props} />
    </div>
  );
};

const mockDecisionElements = [
  {
    name: 'APPLICATION SCORE',
    searchCode: 'E',
    immediateAllocation: '2342',
    value: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
  },
  {
    name: 'APPLICATION SCORE',
    searchCode: 'E',
    immediateAllocation: '3242342',
    value: []
  },
  {
    name: 'APPLICATION SCORE',
    searchCode: 'E',
    immediateAllocation: '',
    value: []
  },
  {
    name: 'APPLICATION SCORE',
    searchCode: 'E',
    immediateAllocation: '',
    value: []
  },
  {
    name: 'APPLICATION SCORE',
    searchCode: 'E',
    immediateAllocation: '',
    value: []
  },
  {
    name: 'APPLICATION SCORE',
    searchCode: 'E',
    immediateAllocation: '',
    value: []
  },
  {
    name: 'APPLICATION SCORE',
    searchCode: 'E',
    immediateAllocation: '',
    value: []
  },
  {
    name: 'APPLICATION SCORE',
    searchCode: 'E',
    immediateAllocation: '',
    value: []
  },
  {
    name: 'APPLICATION SCORE',
    searchCode: 'E',
    immediateAllocation: '',
    value: []
  },
  {
    name: 'APPLICATION SCORE',
    searchCode: 'E',
    immediateAllocation: '',
    value: []
  },
  {
    name: 'APPLICATION SCORE',
    searchCode: 'E',
    immediateAllocation: '',
    value: []
  },
  {
    name: 'RESULT ID',
    searchCode: '',
    immediateAllocation: '',
    value: []
  }
];

const { getComputedStyle } = window;
window.getComputedStyle = (elt, pseudoElt) => getComputedStyle(elt, pseudoElt);

describe('WorkflowManageAccountLevel > ManageAccountLevelCAStep > ViewModal', () => {
  // const mockDispatch = jest.fn();

  beforeEach(() => {
    mockUseSelector.mockImplementation(selector =>
      selector({
        caches: {}
      })
    );
    mockMeta.mockReturnValue({
      searchCode: [
        {
          value: 'E',
          description: 'E - Exact'
        },
        {
          value: 'R',
          description: 'R - Ranger'
        }
      ]
    } as any);
    mockUseModalRegistry.mockImplementation(() => [true, false]);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Should render modal with no data', () => {
    renderModal({
      ...defaultProps,
      data: { status: '' }
    });
  });

  it('Should render modal with data', () => {
    const wrapper = renderModal({
      ...defaultProps,
      data: { decisionElements: mockDecisionElements, status: '' }
    });
    fireEvent.click(
      wrapper.getByText('txt_manage_account_level_table_decision_elements')
    );
    fireEvent.click(
      wrapper.baseElement.querySelector(
        'button[class="change-page-number"]'
      ) as Element
    );
    fireEvent.click(
      wrapper.baseElement.querySelector(
        'button[class="change-page-size"]'
      ) as Element
    );
  });

  it('Should render modal with data when onlyShowElementList', () => {
    const wrapper = renderModal({
      ...defaultProps,
      data: { decisionElements: mockDecisionElements, status: '' },
      onlyShowElementList: true
    });
    fireEvent.click(
      wrapper.baseElement.querySelector(
        'button[class="change-page-number"]'
      ) as Element
    );
    fireEvent.click(
      wrapper.baseElement.querySelector(
        'button[class="change-page-size"]'
      ) as Element
    );
  });

  it('Should render GridTableDetailAQ', () => {
    const wrapper = renderModal({
      ...defaultProps,
      data: { decisionElements: mockDecisionElements, status: '' },
      onlyShowElementList: false,
      isTableDetailAQ: true
    });

    expect(wrapper.getByTestId('GridTableDetailAQ')).toBeInTheDocument();
  });
});
