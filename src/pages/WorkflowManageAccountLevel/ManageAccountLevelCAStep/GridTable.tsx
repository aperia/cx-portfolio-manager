import EnhanceDatePicker from 'app/components/EnhanceDatePicker';
import {
  ColumnType,
  Grid,
  Icon,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import React from 'react';

interface IProps {
  data: TableContentType[];
  handleChangeControlParameters: (val: Date | undefined) => void;
}

const GridTable: React.FC<IProps> = ({
  data,
  handleChangeControlParameters
}) => {
  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();

  const columns: ColumnType[] = [
    {
      id: 'businessName',
      Header: t('txt_manage_account_level_table_decision_business_name'),
      accessor: () =>
        t('txt_manage_account_level_table_decision_ac_allocation_date'),
      cellBodyProps: { className: 'pt-12 pb-8' },
      width: 233
    },
    {
      id: 'greenScreenName',
      Header: t('txt_manage_account_level_table_decision_green_name'),
      accessor: () =>
        t('txt_manage_account_level_table_decision_ac_alloc_date'),
      cellBodyProps: { className: 'pt-12 pb-8' },
      width: 244
    },
    {
      id: 'value',
      Header: t('txt_manage_account_level_table_decision_value'),
      width: width < 1280 ? 320 : 360,
      cellBodyProps: { className: 'py-8' },
      accessor: data => (
        <EnhanceDatePicker
          placeholder={t('txt_enter_a_value')}
          required
          size="small"
          value={new Date(data?.value)}
          onChange={e => handleChangeControlParameters(e.target.value)}
        />
      )
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      width: 106,
      cellBodyProps: { className: 'pt-12 pb-8' },
      accessor: data => (
        <Tooltip
          element={t(
            'txt_manage_account_level_table_decision_ac_alloc_more_info'
          )}
        >
          <Icon name="information" size="5x" className="color-grey-l16" />
        </Tooltip>
      )
    }
  ];

  return <Grid columns={columns} data={data} />;
};

export default GridTable;
