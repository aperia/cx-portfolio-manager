import userEvent from '@testing-library/user-event';
import { WorkflowMetadataList } from 'app/fixtures/workflow-metadata';
import { renderWithMockStore } from 'app/utils';
import 'app/utils/_mockComponent/mockModalRegistry';
import 'app/utils/_mockComponent/mockUseTranslation';
import mockDevice from 'app/utils/_mockHelperConstant/mockDevice';
import React from 'react';
import AddNewMethodModal from './AddNewMethodModal';

const mockOnConfirmParams = jest.fn();
jest.mock('app/hooks/useUnsavedChangesRedirect', () => {
  return {
    __esModule: true,
    useUnsavedChangesRedirect:
      () =>
      ({ onConfirm, onDiscard, onCancel }: any) => {
        onDiscard && onDiscard();
        onCancel && onCancel();
        onConfirm && onConfirm(mockOnConfirmParams());
      }
  };
});

jest.mock('./GridTable', () => ({
  __esModule: true,
  default: ({ handleChangeControlParameters }: any) => {
    return (
      <div>
        <button
          className="detail-close-btn"
          onClick={() => handleChangeControlParameters([])}
        >
          GridTable
        </button>
      </div>
    );
  }
}));

jest.mock('./GridDecision', () => ({
  __esModule: true,
  default: ({ handleChangeDataDecision }: any) => {
    return (
      <div>
        <button
          className="detail-close-btn"
          onClick={() => handleChangeDataDecision([])}
        >
          GridDecision
        </button>
      </div>
    );
  }
}));

describe('pages > WorkflowManageAccountLevel > ManageAccountLevelCAStep > AddNewMethodModal', () => {
  const onClose = jest.fn();
  const handleReviewNewVersion = jest.fn();
  const setFormValues = jest.fn();
  const handleBackTableList = jest.fn();
  const defaultProps = {
    id: 'AddNewMethodModal',
    show: true,
    onClose,
    handleReviewNewVersion,
    setFormValues,
    handleBackTableList
  };

  const getInitialState = ({ elements = WorkflowMetadataList }: any = {}) => ({
    workflowSetup: { elementMetadata: { elements } }
  });
  const queryInputFromLabel = (tierLabel: HTMLElement) => {
    return tierLabel
      .closest(
        '.dls-input-container, .dls-numreric-container, .text-field-container'
      )
      ?.querySelector('input, .input, textarea') as HTMLInputElement;
  };

  beforeEach(() => {
    mockDevice.isDevice = false;
    mockOnConfirmParams.mockReturnValue('');
  });

  afterEach(() => {
    onClose.mockClear();
  });

  it('should render AddNewMethodModal', async () => {
    mockDevice.isDevice = true;
    const tableData: IAPLPropsType = {
      id: 'id',
      name: 'MTHMTCI',
      tableControlParameters: [
        {
          id: 'id1',
          name: 'MTHMTCI',
          versions: [{ id: 'id' }]
        }
      ],
      tableName: 'NEWVERSION',
      comment: '',
      elementList: [
        {
          name: 'credit.balance.hold.options',
          idDecision: 'T',
          searchCode: 'A'
        },
        {
          name: 'where.to.send.requested.statements',
          idDecision: '0',
          searchCode: 'B'
        }
      ]
    };
    const wrapper = await renderWithMockStore(
      <AddNewMethodModal {...defaultProps} keepState tableData={tableData} />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByText('txt_manage_account_level_table_parameters')
    ).toBeInTheDocument();

    const description = queryInputFromLabel(
      wrapper.getByText('txt_comment_area')
    );
    userEvent.type(description, '1234');

    const tableName = queryInputFromLabel(
      wrapper.getByText('txt_manage_account_level_table_decision_name')
    );
    userEvent.type(tableName, '1234');

    userEvent.click(wrapper.getByText('txt_save'));
    expect(handleReviewNewVersion).toBeCalled();
  });

  it('should render AddNewMethodModal without keepstate', async () => {
    const tableData: IAPLPropsType = {
      id: 'id',
      name: 'MTHMTCI',
      tableControlParameters: [
        {
          id: 'id1',
          name: 'MTHMTCI',
          versions: [{ id: 'id' }]
        }
      ],
      tableName: 'NEWVERSION',
      comment: '',
      elementList: [
        {
          name: 'use.fixed.minimum.payment.due.date',
          idDecision: '8',
          searchCode: 'E'
        },
        {
          name: '',
          idDecision: '1000',
          searchCode: ''
        }
      ]
    };
    const wrapper = await renderWithMockStore(
      <AddNewMethodModal {...defaultProps} tableData={tableData} />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByText('txt_manage_account_level_table_parameters')
    ).toBeInTheDocument();
    userEvent.click(wrapper.getByText('GridTable'));
    userEvent.click(wrapper.getByText('GridDecision'));

    userEvent.click(wrapper.getByText('txt_cancel'));
    expect(onClose).toBeCalled();
  });

  it('should render AddNewMethodModal > back to table list', async () => {
    const tableData: IAPLPropsType = {
      id: 'id',
      name: 'MTHMTCI',
      tableControlParameters: [
        {
          id: 'id1',
          name: 'MTHMTCI',
          versions: [{ id: 'id' }]
        }
      ],
      tableName: 'NEWVERSION',
      comment: '',
      elementList: [
        {
          name: 'use.fixed.minimum.payment.due.date',
          idDecision: '8',
          searchCode: 'E'
        },
        {
          name: '',
          idDecision: '1000',
          searchCode: ''
        }
      ]
    };
    const wrapper = await renderWithMockStore(
      <AddNewMethodModal
        {...defaultProps}
        formValues={{ isEditForm: true }}
        tableData={tableData}
      />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByText('txt_manage_account_level_table_parameters')
    ).toBeInTheDocument();
    userEvent.click(wrapper.getByText('txt_back'));

    expect(handleBackTableList).toBeCalled();
  });

  it('should render AddNewMethodModal > back to table list with setDecisionElements', async () => {
    const tableData: IAPLPropsType = {
      id: 'id',
      name: 'MTHMTCI',
      tableControlParameters: [
        {
          id: 'id1',
          name: 'MTHMTCI',
          versions: [{ id: 'id' }]
        }
      ],
      tableName: 'NEWVERSION',
      comment: '',
      elementList: [
        {
          name: 'use.fixed.minimum.payment.due.date',
          idDecision: '8',
          searchCode: 'E'
        },
        {
          name: '',
          idDecision: '1000',
          searchCode: ''
        }
      ]
    };
    const wrapper = await renderWithMockStore(
      <AddNewMethodModal {...defaultProps} tableData={tableData} />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByText('txt_manage_account_level_table_parameters')
    ).toBeInTheDocument();
    userEvent.click(wrapper.getByText('txt_back'));

    expect(handleBackTableList).toBeCalled();
  });
});
