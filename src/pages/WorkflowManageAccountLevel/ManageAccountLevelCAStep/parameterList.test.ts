import {
  MethodFieldNameEnum,
  MethodFieldParameterEnum
} from 'app/constants/enums';
import { tableParameterGroup } from './parameterList';

describe('pages > ManageAccountLevelCAStep > parameterList ', () => {
  it('> parameterList', () => {
    expect(tableParameterGroup).toEqual([
      {
        id: MethodFieldParameterEnum.AllocationDate,
        businessName: MethodFieldNameEnum.AllocationDate,
        greenScreenName: 'ALLOC Date',
        moreInfo:
          'Code determining when you want the System to allocate accounts through the account qualification table'
      }
    ]);
  });
});
