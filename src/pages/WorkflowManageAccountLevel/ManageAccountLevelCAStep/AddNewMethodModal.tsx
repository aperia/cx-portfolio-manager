import ModalRegistry from 'app/components/ModalRegistry';
import TextAreaCountDown from 'app/components/TextAreaCountDown';
import {
  unsavedChangesProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import {
  useFormValidations,
  useUnsavedChangeRegistry,
  useUnsavedChangesRedirect
} from 'app/hooks';
import {
  Button,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  TextBox,
  useTranslation
} from 'app/_libraries/_dls';
import { isEqual } from 'app/_libraries/_dls/lodash';
import { isEmpty, isFunction } from 'lodash';
import { useSelectDecisionListData } from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageAccountLevel';
import React, { useEffect, useMemo, useRef, useState } from 'react';
import { ManageAccountLevelCAFormValues } from '.';
import GridDecision, { DecisionTypeValue } from './GridDecision';
import GridTable from './GridTable';
import { IAPLPropsType } from './MethodListModal';
interface IProps {
  id: string;
  tableData: IAPLPropsType;
  onClose?: () => void;
  type?: string;
  setFormValues: (values: Partial<ManageAccountLevelCAFormValues>) => void;
  formValues: ManageAccountLevelCAFormValues;
  handleReviewNewVersion: () => void;
  keepState?: boolean;
  handleBackTableList: () => void;
}

const AddNewMethodModal: React.FC<IProps> = ({
  id,
  tableData,
  onClose,
  formValues,
  setFormValues,
  handleReviewNewVersion,
  keepState = false,
  handleBackTableList
}) => {
  const { t } = useTranslation();
  const redirect = useUnsavedChangesRedirect();
  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;

  const [initialValue, setInitialValue] = useState<IAPLPropsType>();
  const [tableName, setTableName] = useState<string>('');
  const [comment, setComment] = useState<string>('');

  const decisionList = useSelectDecisionListData()?.decisionList;

  const controlParameters = tableData?.tableControlParameters;
  const decisionElementData = (
    tableData?.elementList || tableData?.decisionElements
  )?.map((item, index: number) => ({
    ...item,
    idDecision: `${item?.name}__${index}`
  }));

  const [decisionElements, setDecisionElements] = useState(decisionElementData);
  const [tableControlParameters, setTableControlParameters] =
    useState(controlParameters);

  useEffect(() => {
    if (!formValues?.isEditForm || !controlParameters || !decisionElementData)
      return;
    setTableControlParameters(controlParameters);
    setDecisionElements(decisionElementData);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tableData, formValues?.isEditForm]);

  const currentErrors = useMemo(
    () =>
      ({
        tableName: isEmpty(tableName) && {
          status: true,
          message: t('Table Name is required.')
        }
      } as Record<'tableName', IFormError>),
    [tableName, t]
  );

  const [, errors, setTouched] = useFormValidations<
    keyof { tableName: string }
  >(currentErrors, 0);

  const dataList = decisionElements?.filter(
    (item: MagicKeyValue) => item?.name !== 'RESULT ID'
  );

  let isOk = false;

  dataList?.forEach((item: MagicKeyValue) => {
    if (isEmpty(item?.name) || isEmpty(item?.searchCode)) {
      isOk = true;
    }
  });

  const handleChangeControlParameters = (val: Date | undefined) => {
    setTableControlParameters([
      {
        ...tableControlParameters[0],
        value: val
      }
    ]);
  };

  const handleChangeDataDecision = (data: DecisionTypeValue[]) => {
    setDecisionElements(data);
  };

  useEffect(() => {
    if (keepState) {
      setTableName(tableData?.tableName);
      setComment(tableData?.comment);
      setInitialValue({
        ...tableData,
        decisionElements: decisionElementData
      });
      return;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [keepState]);

  useEffect(() => {
    if (keepState) return;
    setInitialValue({
      ...tableData,
      tableName: '',
      comment: '',
      decisionElements: decisionElementData
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const isChangeForm =
    initialValue?.tableName !== tableName ||
    initialValue?.comment !== comment ||
    !isEqual(tableControlParameters, initialValue?.tableControlParameters) ||
    !isEqual(decisionElements, initialValue?.decisionElements);

  useUnsavedChangeRegistry(
    {
      ...unsavedChangesProps,
      formName: UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_ACCOUNT_LEVEL__AC,
      priority: 1
    },
    [isChangeForm]
  );

  const handleCloseModal = () => {
    redirect({
      onConfirm: () => {
        isFunction(onClose) && onClose();
      },
      formsWatcher: [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_ACCOUNT_LEVEL__AC
      ]
    });
  };

  return (
    <ModalRegistry lg id={id} show>
      <ModalHeader border closeButton onHide={handleCloseModal}>
        <ModalTitle>{t('txt_manage_account_level_table_details')}</ModalTitle>
      </ModalHeader>
      <ModalBody>
        <p>
          In the Account Level Processing<sup>SM</sup> service (ALP<sup>SM</sup>{' '}
          service), client allocation is used to group accounts for processing
          through account qualification tables. You designate how you want to
          group accounts within your cardholder base based on the decision
          elements you choose.
        </p>
        <div className="row mt-16">
          <div className="col-6 col-lg-4">
            <TextBox
              id="addNewMethod__selectedMethod"
              readOnly
              value={tableData?.tableId}
              maxLength={8}
              label={t(
                'txt_manage_account_level_table_decision_selected_table'
              )}
            />
          </div>
          <div className="col-6 col-lg-4 mr-lg-4">
            <TextBox
              id="addNewTable__methodName"
              required
              maxLength={8}
              label={t('txt_manage_account_level_table_decision_name')}
              value={tableName}
              onChange={e => setTableName(e.target.value?.toUpperCase())}
              onFocus={setTouched('tableName')}
              onBlur={setTouched('tableName', true)}
              error={errors.tableName}
            />
          </div>
          <div className="mt-16 col-12 col-lg-8">
            <TextAreaCountDown
              id="addNewTable__comment"
              label={t('txt_comment_area')}
              value={comment}
              rows={4}
              maxLength={204}
              onChange={e => setComment(e.target.value)}
            />
          </div>
        </div>
        <div className="mt-24 mb-24 mb-xl-0">
          <div className="d-flex align-items-center justify-content-between mb-16">
            <h5>{t('txt_manage_account_level_table_parameters')}</h5>
          </div>
          <GridTable
            handleChangeControlParameters={handleChangeControlParameters}
            data={tableControlParameters}
          />
          <hr className="my-24" />
          <GridDecision
            data={decisionElements}
            handleChangeDataDecision={handleChangeDataDecision}
          />
        </div>
      </ModalBody>
      <ModalFooter
        cancelButtonText={t('txt_cancel')}
        okButtonText={t('txt_save')}
        onCancel={handleCloseModal}
        onOk={() => {
          keepRef.current.setFormValues({
            ...formValues,
            isValid: true,
            tables: {
              tableId: tableData?.tableId,
              tableName,
              comment,
              tableControlParameters,
              elementList: decisionElements
            },
            decisionList
          });
          handleReviewNewVersion();
        }}
        disabledOk={isEmpty(tableName) || isOk}
      >
        <Button
          className="mr-auto ml-n8"
          variant="outline-primary"
          onClick={() => {
            keepRef.current.setFormValues({
              ...formValues,
              isValid: true,
              tables: {
                tableId: tableData?.tableId,
                tableName,
                comment,
                tableControlParameters,
                elementList: decisionElements
              }
            });
            handleBackTableList();
          }}
        >
          {t('txt_back')}
        </Button>
      </ModalFooter>
    </ModalRegistry>
  );
};

export default AddNewMethodModal;
