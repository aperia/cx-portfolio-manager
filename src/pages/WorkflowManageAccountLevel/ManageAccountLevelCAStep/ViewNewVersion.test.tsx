import userEvent from '@testing-library/user-event';
import {
  mockUseDispatchFnc,
  mockUseModalRegistryFnc,
  renderWithMockStore
} from 'app/utils';
import { queryByClass } from 'app/_libraries/_dls/test-utils';
import React from 'react';
import ViewNewVersion, { ViewNewVersionProps } from './ViewNewVersion';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const mockUseModalRegistry = mockUseModalRegistryFnc();
const mockUseDispatch = mockUseDispatchFnc();

const renderComponent = (props: ViewNewVersionProps) => {
  return renderWithMockStore(<ViewNewVersion {...props} />, {
    initialState: {}
  });
};

describe('ViewNewVersion', () => {
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => jest.fn());
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
    mockUseModalRegistry.mockClear();
    jest.clearAllMocks();
  });

  it('render component', async () => {
    const props = {
      formValues: {
        isValid: true,
        dataTableVersion: '123'
      },
      handleDeleteNewVersion: jest.fn(),
      handleEditNewVersion: jest.fn()
    } as unknown as ViewNewVersionProps;

    mockUseModalRegistry.mockImplementation(() => [true, false]);

    const wrapper = await renderComponent(props);
    userEvent.click(wrapper.getByText('txt_edit'));

    userEvent.click(wrapper.getByText('txt_delete'));

    userEvent.click(wrapper.getByText('txt_cancel'));

    userEvent.click(wrapper.getByText('txt_delete'));
    userEvent.click(
      queryByClass(wrapper.baseElement, /icon-close/) as HTMLElement
    );
  });
  it('render component', async () => {
    const props = {
      formValues: {
        isValid: true,
        dataTableVersion: '123'
      },
      handleDeleteNewVersion: jest.fn(),
      handleEditNewVersion: jest.fn()
    } as unknown as ViewNewVersionProps;

    mockUseModalRegistry.mockImplementation(() => [true, true]);

    const wrapper = await renderComponent(props);

    userEvent.click(wrapper.getByText('txt_edit'));
    userEvent.click(wrapper.getByText('txt_delete'));
  });
});
