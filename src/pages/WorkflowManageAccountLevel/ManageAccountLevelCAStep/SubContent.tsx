import {
  Button,
  ColumnType,
  Grid,
  Icon,
  Tooltip,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import {
  useSelectDecisionData,
  useSelectDecisionListData
} from 'pages/_commons/redux/WorkflowSetup';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, { useEffect, useState } from 'react';
import { ManageAccountLevelCAFormValues } from '.';

const SubContent: React.FC<WorkflowSetupProps<ManageAccountLevelCAFormValues>> =
  props => {
    const { t } = useTranslation();
    const { width } = useSelectWindowDimension();

    const { selectedStep, stepId } = props;

    const decisionList = useSelectDecisionListData()?.decisionList || [];
    const data = useSelectDecisionData() || [];

    const [isExpand, setIsExpand] = useState<boolean>(false);

    const decisionMapData = decisionList?.filter((item: MagicKeyValue) => {
      return !item?.configurable;
    });

    const dataList = data?.filter((item: MagicKeyValue) => {
      const val = decisionList.find(
        (i: DecisionType) => i?.name === item?.name
      );
      return val?.configurable;
    });

    const dataDecision = [...dataList, ...decisionMapData];

    useEffect(() => {
      if (selectedStep === stepId) {
        setIsExpand(false);
      }
    }, [selectedStep, stepId]);

    const columns: ColumnType[] = [
      {
        id: 'decisionElement',
        Header: t('Decision Element'),
        width: width < 1280 ? 200 : 338,
        accessor: ({ name }) => name
      },
      {
        id: 'description',
        Header: t('Description'),
        accessor: ({ name }) => {
          const decision = decisionList.filter(
            (item: MagicKeyValue) => item?.name === name
          )[0];
          return (
            <TruncateText
              resizable
              lines={2}
              ellipsisLessText={t('txt_less')}
              ellipsisMoreText={t('txt_more')}
            >
              {decision?.moreInfo}
            </TruncateText>
          );
        }
      }
    ];

    return (
      <div className="pt-24">
        <div className="d-flex align-items-center">
          <Tooltip
            element={isExpand ? t('txt_collapse') : t('txt_expand')}
            variant="primary"
            placement="top"
            triggerClassName="d-flex ml-n2"
          >
            <Button
              size="sm"
              variant="icon-secondary"
              onClick={() => setIsExpand(!isExpand)}
            >
              <Icon name={isExpand ? 'minus' : 'plus'} size="4x" />
            </Button>
          </Tooltip>
          <p className="fw-600 ml-8">{t('Decision Element List')}</p>
        </div>

        {isExpand && (
          <>
            <p className="mt-8 mb-16">
              {t(
                'This list contains the decision elements you will configure using the template in this step.'
              )}
            </p>
            <Grid columns={columns} data={dataDecision} />
          </>
        )}
        <hr className="my-24" />
      </div>
    );
  };

const ExtraStaticSubContentStep = SubContent as WorkflowSetupStaticProp;

ExtraStaticSubContentStep.defaultValues = {
  isValid: true,
  parameters: []
};

export default ExtraStaticSubContentStep;
