import {
  MethodFieldNameEnum,
  MethodFieldParameterEnum
} from 'app/constants/enums';

export interface TableParameter {
  id: MethodFieldParameterEnum;
  businessName: MethodFieldNameEnum;
  greenScreenName: string;
  moreInfo: React.ReactNode;
}

export const tableParameterGroup: TableParameter[] = [
  {
    id: MethodFieldParameterEnum.AllocationDate,
    businessName: MethodFieldNameEnum.AllocationDate,
    greenScreenName: 'ALLOC Date',
    moreInfo:
      'Code determining when you want the System to allocate accounts through the account qualification table'
  }
];
