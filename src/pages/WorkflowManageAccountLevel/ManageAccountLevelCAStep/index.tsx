import { MethodFieldParameterEnum } from 'app/constants/enums';
import {
  confirmEditWhenChangeEffectToUploadProps,
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { useUnsavedChangeRegistry } from 'app/hooks';
import { InlineMessage, useTranslation } from 'app/_libraries/_dls';
import { StateFile } from 'app/_libraries/_dls/components/Upload/File';
import { STATUS } from 'app/_libraries/_dls/components/Upload/helper';
import { isEqual, omit } from 'lodash';
import isEmpty from 'lodash.isempty';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, { useEffect, useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import ActionButtons from './ActionButtons';
import AddNewMethodModal from './AddNewMethodModal';
import MethodListModal, { IAPLPropsType } from './MethodListModal';
import parseFormValues from './parseFormValues';
import Summary from './Summary';
import ViewNewVersion from './ViewNewVersion';

export interface ManageAccountLevelTable {
  comment?: string;
  id?: string;
  tableId?: string;
  tableName?: string;
  elementList?: any[];
  tableControlParameters?: TableContentType[];
}
export interface ManageAccountLevelCAFormValues {
  isValid?: boolean;
  tables?: ManageAccountLevelTable;
  isEditForm?: boolean;
  decisionList?: MagicKeyValue[];
}
interface IDependencies {
  files?: StateFile[];
}

const ManageAccountCAStep: React.FC<
  WorkflowSetupProps<ManageAccountLevelCAFormValues, IDependencies> & {
    clearFormName: string;
  }
> = ({
  setFormValues,
  formValues,
  dependencies,
  clearFormName,
  isStuck,
  savedAt,
  clearFormValues
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;

  const [isCreateNewVersion, setIsCreateNewVersion] = useState(false);
  const [isViewNewVersion, setIsViewNewVersion] = useState(false);

  const [isShowAddModal, setIsShowAddModal] = useState<boolean>(false);
  const [selectedVersion, setSelectedVersion] = useState<IAPLPropsType>();
  const [keepState, setKeepState] = useState<boolean>(false);
  const [isClearAndReset, setIsClearAndReset] = useState<boolean>(false);
  const [idSelectedVal, setIdSelectedVal] = useState<string>('');
  const [isBackTableList, setIsBackTableList] = useState<boolean>(false);
  const [intIdSelected, setIntIdSelected] = useState<string>('');
  const [isEdit, setIsEdit] = useState<boolean>(false);

  const [isEditAffectFile, setEditAffectFileStep] = useState<boolean>(false);
  const [originTable, setOriginTable] = useState(formValues?.tables);

  const [prevValues, setPrevValues] = useState(formValues);
  useEffect(() => {
    setPrevValues(formValues);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [savedAt]);

  useEffect(() => {
    if (!isEmpty(dependencies?.files)) {
      const isChange = !isEqual(
        {
          tableId: formValues?.tables?.tableId,
          elementList: formValues?.tables?.elementList?.map((element: any) =>
            omit(element, 'rowId')
          )
        },
        {
          tableId: originTable?.tableId,
          elementList: originTable?.elementList?.map((element: any) =>
            omit(element, 'rowId')
          )
        }
      );
      setEditAffectFileStep(isChange);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [formValues?.tables]);

  useEffect(() => {
    if (isEmpty(dependencies?.files)) return;

    setOriginTable(formValues?.tables);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dependencies?.files]);

  const onClickCreateNewVersion = () => {
    setIsCreateNewVersion(true);
  };

  const closeMethodListModalModal = () => {
    setIsCreateNewVersion(false);
  };

  const handleReviewNewVersion = () => {
    setIsCreateNewVersion(false);
    setIsViewNewVersion(true);
    setIsShowAddModal(false);
  };

  const handleCloseAddModal = () => {
    setIsShowAddModal(false);
    setKeepState(false);
    if (keepState && isEdit) {
      setIsViewNewVersion(true);
      return;
    }
    setIsClearAndReset(true);
    setIsCreateNewVersion(true);
  };

  const handleAddNewVersion = () => {
    if (isBackTableList && intIdSelected === idSelectedVal) {
      setIsBackTableList(false);
      setKeepState(true);
    }
    setIsShowAddModal(true);
  };

  const setDataNewVersion = (data: IAPLPropsType) => {
    if (isBackTableList && intIdSelected === data?.tableId) {
      setSelectedVersion(formValues?.tables as any);
      return;
    }
    setSelectedVersion(data);
  };

  const handleEditNewVersion = () => {
    setIsViewNewVersion(false);
    setIsShowAddModal(true);
    setKeepState(true);
    setSelectedVersion(formValues?.tables as any);
    setIsEdit(true);

    keepRef.current.setFormValues({
      ...formValues,
      isEditForm: false
    });
  };

  const handleDeleteNewVersion = () => {
    setIsViewNewVersion(false);
    setIsShowAddModal(false);
    setKeepState(false);
    setIsCreateNewVersion(false);
    keepRef.current.setFormValues({
      ...formValues,
      tables: {},
      isValid: false,
      isEditForm: false
    });
    setSelectedVersion({} as any);
  };

  const handleBackTableList = () => {
    setIsShowAddModal(false);
    setIsCreateNewVersion(true);
    setIsClearAndReset(true);
    setIdSelectedVal(selectedVersion?.tableId as string);
    setKeepState(false);
    setIsBackTableList(true);
    setIntIdSelected(selectedVersion?.tableId as string);
  };

  useEffect(() => {
    dispatch(
      actionsWorkflowSetup.getWorkflowMetadata([
        MethodFieldParameterEnum.SearchCode
      ])
    );
  }, [dispatch]);

  useEffect(() => {
    dispatch(
      actionsWorkflowSetup.getTableList({
        tableType: 'CA',
        serviceNameAbbreviation: 'ALP'
      })
    );
    dispatch(actionsWorkflowSetup.getDecisionList({ tableType: 'CA' }));
  }, [dispatch]);

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_ACCOUNT_LEVEL__AC__CONFIGURE_PARAMETERS,
      priority: 1
    },
    [!isEqual(prevValues, formValues)]
  );

  const handleResetUploadStep = () => {
    const file = dependencies?.files?.[0];
    const attachmentId = file?.idx;
    const isValid = file?.status === STATUS.valid;
    clearFormValues(clearFormName);
    isValid &&
      dispatch(actionsWorkflowSetup.deleteTemplateFile({ attachmentId }));
    isEditAffectFile && setEditAffectFileStep(false);
  };

  useUnsavedChangeRegistry(
    {
      ...confirmEditWhenChangeEffectToUploadProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_ACCOUNT_LEVEL__AC__CONFIGURE_PARAMETERS_CONFIRM_EDIT,
      priority: 1
    },
    [!isEmpty(dependencies?.files) && isEditAffectFile],
    handleResetUploadStep
  );

  return (
    <div>
      {isStuck && (
        <InlineMessage className="mb-0 mt-24" variant="danger" withIcon>
          {t('txt_step_stuck_move_forward_message')}
        </InlineMessage>
      )}
      <div className="mt-24">
        {!isViewNewVersion && !formValues?.isEditForm && (
          <ActionButtons
            small
            title="txt_manage_penalty_fee_late_charge_method_list_title"
            onClickCreateNewVersion={onClickCreateNewVersion}
          />
        )}
        {(isViewNewVersion || formValues?.isEditForm) && (
          <ViewNewVersion
            formValues={formValues}
            handleEditNewVersion={handleEditNewVersion}
            handleDeleteNewVersion={handleDeleteNewVersion}
          />
        )}
      </div>

      {isCreateNewVersion && (
        <MethodListModal
          id="addAccountLevelFromModel"
          isCreateNewVersion={isCreateNewVersion}
          onClose={closeMethodListModalModal}
          handleAddNewVersion={handleAddNewVersion}
          setDataNewVersion={setDataNewVersion}
          isClearAndReset={isClearAndReset}
          idSelectedVal={idSelectedVal}
          resetClearAndReset={setIsClearAndReset}
        />
      )}

      {isShowAddModal && (
        <AddNewMethodModal
          id="createNewVersion"
          tableData={selectedVersion as IAPLPropsType}
          onClose={handleCloseAddModal}
          setFormValues={setFormValues}
          formValues={formValues}
          handleReviewNewVersion={handleReviewNewVersion}
          keepState={keepState}
          handleBackTableList={handleBackTableList}
        />
      )}
    </div>
  );
};

const ExtraStaticManageAccountCAStep =
  ManageAccountCAStep as WorkflowSetupStaticProp<
    ManageAccountLevelCAFormValues,
    IDependencies
  >;

ExtraStaticManageAccountCAStep.summaryComponent = Summary;

ExtraStaticManageAccountCAStep.defaultValues = {
  isValid: false,
  isEditForm: false,
  tables: {
    comment: undefined,
    elementList: undefined,
    tableControlParameters: undefined,
    tableId: undefined,
    tableName: undefined
  }
};
ExtraStaticManageAccountCAStep.parseFormValues = parseFormValues;

export default ExtraStaticManageAccountCAStep;
