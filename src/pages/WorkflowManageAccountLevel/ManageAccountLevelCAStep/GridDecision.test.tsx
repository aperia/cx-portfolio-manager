import { act, render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { mockUseDispatchFnc } from 'app/utils';
import 'app/utils/_mockComponent/mockComboBox';
import { queryByClass } from 'app/_libraries/_dls/test-utils';
import * as CommonRedux from 'pages/_commons/redux/Common/select-hooks';
import * as decisionList from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageAccountLevel';
import * as mockMetaData from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageAccountLevel';
import React from 'react';
import GridDecision from './GridDecision';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    Popper: ({ childrend, ...props }: any) => <div>{childrend}</div>,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderComponent = (props: any) => {
  return render(<GridDecision {...props} />);
};

const mockUseDispatch = mockUseDispatchFnc();

const mockUseSelectWindowDimension = jest.spyOn(
  CommonRedux,
  'useSelectWindowDimension'
);

HTMLCanvasElement.prototype.getContext = jest.fn();

describe('GridDecision', () => {
  const mockUseSelectWindowDimensionImplementation = (width: number) => {
    mockUseSelectWindowDimension.mockImplementation(() => ({ width } as any));
  };

  const mockDecision = jest.spyOn(decisionList, 'useSelectDecisionListData');
  const mockMeta = jest.spyOn(
    mockMetaData,
    'useSelectElementMetadataManageAcountLevel'
  );

  beforeEach(() => {
    mockDecision.mockReturnValue({
      decisionList: [
        {
          configurable: true,
          immediateAllocation: 'Yes',
          moreInfo:
            'Adaptive Control System portfolio currently in effect for the customer account; variable-length, 4 positions, numeric.',
          name: 'ACS CURR PORT1'
        },
        {
          configurable: true,
          immediateAllocation: 'Yes',
          moreInfo:
            'Adaptive Control System portfolio currently in effect for the customer account; variable-length, 4 positions, numeric.',
          name: 'ACS CURR PORT2'
        },
        {
          configurable: true,
          immediateAllocation: 'Yes',
          moreInfo:
            'Adaptive Control System portfolio currently in effect for the customer account; variable-length, 4 positions, numeric.',
          name: 'ACS CURR PORT3'
        },
        {
          configurable: true,
          immediateAllocation: 'Yes',
          moreInfo:
            'Adaptive Control System portfolio currently in effect for the customer account; variable-length, 4 positions, numeric.',
          name: 'ACS CURR PORT'
        },
        {
          configurable: true,
          immediateAllocation: 'No',
          moreInfo: 'Date (MMDDYYYY)',
          name: 'ACS PORT CHG DATE'
        },
        {
          configurable: true,
          immediateAllocation: 'Yes',
          moreInfo:
            'Issuer-defined advertising group code; variable-length, 3 positions, numeric.',
          name: 'ADVERTISING GROUP'
        },
        {
          configurable: true,
          immediateAllocation: 'Yes',
          moreInfo:
            'Issuer-defined advertising group code; variable-length, 3 positions, numeric.',
          name: ''
        }
      ]
    });
    mockMeta.mockReturnValue({
      searchCode: [
        {
          code: 'E',
          text: 'E - Exact'
        },
        {
          code: 'R',
          text: 'R - Ranger'
        },
        {
          code: '',
          text: 'Empty'
        }
      ]
    } as any);
    mockUseDispatch.mockImplementation(() => jest.fn());
    mockUseSelectWindowDimensionImplementation(900);
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
    jest.clearAllMocks();
  });

  it('render component with value length === 1', () => {
    mockUseSelectWindowDimensionImplementation(1300);
    const props = {
      data: [
        {
          idDecision: 'AVG END BAL LST 12__0',
          immediateAllocation: '',
          moreInfo: 'Dollar-and-cent',
          name: '',
          searchCode: 'R',
          value: ['3', '136', '137', '224', '310']
        }
      ],
      handleChangeDataDecision: jest.fn()
    };

    renderComponent(props);
  });

  it('render component with value length === 1 and remove', () => {
    mockUseSelectWindowDimensionImplementation(1300);
    const props = {
      data: [
        {
          idDecision: 'AVG END BAL LST 12__0',
          immediateAllocation: '',
          moreInfo: 'Dollar-and-cent',
          name: 'ACS CURR PORT',
          searchCode: 'R',
          value: ['3', '136', '137', '224', '310']
        }
      ],
      handleChangeDataDecision: jest.fn()
    };

    const wrapper = renderComponent(props);
    userEvent.click(wrapper.getAllByText('txt_remove')[0]);
  });

  it('render component with error', () => {
    mockUseSelectWindowDimensionImplementation(1300);
    const props = {
      data: [],
      handleChangeDataDecision: jest.fn()
    };

    renderComponent(props);
  });

  it('render component', () => {
    const props = {
      data: [
        {
          idDecision: 'AVG END BAL LST 1',
          immediateAllocation: '123',
          moreInfo: 'Dollar-and-cent',
          name: '',
          searchCode: 'R',
          value: ['3', '136', '137', '224', '310']
        },
        {
          idDecision: 'AVG END BAL LST 2',
          immediateAllocation: '',
          moreInfo: 'Dollar-and-cent',
          name: 'ACS CURR PORT',
          searchCode: 'R',
          value: ['3', '136', '137', '224', '310']
        },
        {
          idDecision: 'AVG END BAL LST 3',
          immediateAllocation: '',
          moreInfo: 'Dollar-and-cent',
          name: '',
          searchCode: '',
          value: ['3', '136', '137', '224', '310']
        },
        {
          idDecision: 'AVG END BAL LST 4',
          immediateAllocation: '',
          moreInfo: 'Dollar-and-cent',
          name: 'AVG END BAL LST 12',
          searchCode: 'R',
          value: ['3', '136', '137', '224', '310']
        },
        {
          idDecision: 'AVG END BAL LST 5',
          immediateAllocation: '',
          moreInfo: 'Dollar-and-cent',
          name: 'AVG END BAL LST 12',
          searchCode: 'R',
          value: ['3', '136', '137', '224', '310']
        },
        {
          idDecision: 'AVG END BAL LST 6',
          immediateAllocation: '',
          moreInfo: 'Dollar-and-cent',
          name: 'AVG END BAL LST 12',
          searchCode: 'R',
          value: ['3', '136', '137', '224', '310']
        }
      ],
      handleChangeDataDecision: jest.fn()
    };

    const wrapper = renderComponent(props);

    //Handler Search
    const input = wrapper.getByPlaceholderText(
      'Type to search by element, more info'
    );
    userEvent.click(input);
    userEvent.type(input, 'a');
    userEvent.type(input, '{enter}');

    userEvent.click(input);
    userEvent.type(input, 'name');
    userEvent.type(input, '{enter}');

    //Clear and reset
    userEvent.click(wrapper.getByText('txt_clear_and_reset'));

    //handle Remove
    userEvent.click(wrapper.getAllByText('txt_remove')[0]);

    //handle change decision
    const comboBox = wrapper.getAllByPlaceholderText(
      'txt_workflow_manage_change_in_terms_select_an_option'
    );

    act(() => {
      userEvent.click(comboBox[0]);
    });

    jest.useFakeTimers();
    act(() => {
      userEvent.click(wrapper.queryByText('ACS PORT CHG DATE') as HTMLElement);
    });
    jest.runAllTimers();

    act(() => {
      userEvent.click(comboBox[0]);
    });

    act(() => {
      userEvent.type(comboBox[0], '{backspace}');
    });
  });

  it('render component without error decision', () => {
    const props = {
      data: [
        {
          idDecision: 'AVG END BAL LST 1',
          immediateAllocation: '123',
          moreInfo: 'Dollar-and-cent',
          name: 'John',
          searchCode: 'R',
          value: ['3', '136', '137', '224', '310']
        },
        {
          idDecision: 'AVG END BAL LST 2',
          immediateAllocation: '',
          moreInfo: 'Dollar-and-cent',
          name: 'ACS CURR PORT',
          searchCode: 'R',
          value: ['3', '136', '137', '224', '310']
        },
        {
          idDecision: 'AVG END BAL LST 3',
          immediateAllocation: '',
          moreInfo: 'Dollar-and-cent',
          name: '',
          searchCode: '',
          value: ['3', '136', '137', '224', '310']
        },
        {
          idDecision: 'AVG END BAL LST 4',
          immediateAllocation: '',
          moreInfo: 'Dollar-and-cent',
          name: 'AVG END BAL LST 12',
          searchCode: 'R',
          value: ['3', '136', '137', '224', '310']
        },
        {
          idDecision: 'AVG END BAL LST 5',
          immediateAllocation: '',
          moreInfo: 'Dollar-and-cent',
          name: 'AVG END BAL LST 12',
          searchCode: 'R',
          value: ['3', '136', '137', '224', '310']
        },
        {
          idDecision: 'AVG END BAL LST 6',
          immediateAllocation: '',
          moreInfo: 'Dollar-and-cent',
          name: 'AVG END BAL LST 12',
          searchCode: 'R',
          value: ['3', '136', '137', '224', '310']
        }
      ],
      handleChangeDataDecision: jest.fn()
    };

    const wrapper = renderComponent(props);

    //Handler Search
    const input = wrapper.getByPlaceholderText(
      'Type to search by element, more info'
    );
    userEvent.click(input);
    userEvent.type(input, 'Dollar');
    userEvent.type(input, '{enter}');

    userEvent.click(input);
    userEvent.type(input, 'name');
    userEvent.type(input, '{enter}');

    //Clear and reset
    userEvent.click(wrapper.getByText('txt_clear_and_reset'));

    //handle Remove
    userEvent.click(wrapper.getAllByText('txt_remove')[0]);

    //handle change decision
    const comboBox = wrapper.getAllByPlaceholderText(
      'txt_workflow_manage_change_in_terms_select_an_option'
    );

    act(() => {
      userEvent.click(comboBox[0]);
    });

    jest.useFakeTimers();
    act(() => {
      userEvent.click(wrapper.queryByText('ACS PORT CHG DATE') as HTMLElement);
    });
    jest.runAllTimers();

    act(() => {
      userEvent.click(comboBox[0]);
    });

    act(() => {
      userEvent.type(comboBox[0], '{backspace}');
    });
  });

  it('change searchCode', () => {
    const props = {
      data: [
        {
          idDecision: 'AVG END BAL LST 1',
          immediateAllocation: '123',
          moreInfo: 'Dollar-and-cent',
          name: '',
          searchCode: 'R',
          value: ['3', '136', '137', '224', '310']
        },
        {
          idDecision: 'AVG END BAL LST 2',
          immediateAllocation: '',
          moreInfo: 'Dollar-and-cent',
          name: 'ACS CURR PORT',
          searchCode: 'R',
          value: ['3', '136', '137', '224', '310']
        },
        {
          idDecision: 'AVG END BAL LST 3',
          immediateAllocation: '',
          moreInfo: 'Dollar-and-cent',
          name: '',
          searchCode: '',
          value: ['3', '136', '137', '224', '310']
        }
      ],
      handleChangeDataDecision: jest.fn()
    };

    const wrapper = renderComponent(props);
    userEvent.click(wrapper.getAllByText('R - Ranger')[0]);
    userEvent.click(wrapper.getByText('E - Exact'));
    userEvent.click(wrapper.getByText('Empty'));
  });

  it('change searchCode empty', () => {
    const props = {
      data: [
        {
          idDecision: 'AVG END BAL LST 1',
          immediateAllocation: '123',
          moreInfo: 'Dollar-and-cent',
          name: '',
          searchCode: 'R',
          value: ['3', '136', '137', '224', '310']
        },
        {
          idDecision: 'AVG END BAL LST 2',
          immediateAllocation: '',
          moreInfo: 'Dollar-and-cent',
          name: 'ACS CURR PORT',
          searchCode: '',
          value: ['3', '136', '137', '224', '310']
        },
        {
          idDecision: 'AVG END BAL LST 3',
          immediateAllocation: '',
          moreInfo: 'Dollar-and-cent',
          name: '',
          searchCode: 'E',
          value: ['3', '136', '137', '224', '310']
        }
      ],
      handleChangeDataDecision: jest.fn()
    };

    const wrapper = renderComponent(props);

    act(() => {
      userEvent.click(wrapper.getAllByText('Empty')[0]);
    });

    act(() => {
      userEvent.click(wrapper.getAllByText('Empty')[1]);
    });
  });

  it('change immediateAllocation', () => {
    const props = {
      data: [
        {
          idDecision: 'AVG END BAL LST 1',
          immediateAllocation: '123',
          moreInfo: 'Dollar-and-cent',
          name: '',
          searchCode: 'R',
          value: ['3', '136', '137', '224', '310']
        },
        {
          idDecision: 'AVG END BAL LST 2',
          immediateAllocation: 'Yes',
          moreInfo: 'Dollar-and-cent',
          name: 'ACS CURR PORT',
          searchCode: 'R',
          value: ['3', '136', '137', '224', '310']
        },
        {
          idDecision: 'AVG END BAL LST 3',
          immediateAllocation: '',
          moreInfo: 'Dollar-and-cent',
          name: '',
          searchCode: '',
          value: ['3', '136', '137', '224', '310']
        }
      ],
      handleChangeDataDecision: jest.fn()
    };

    const wrapper = renderComponent(props);
    const input = queryByClass(
      wrapper.container,
      /dls-checkbox-input/
    ) as HTMLElement;

    act(() => {
      userEvent.click(input);
    });

    act(() => {
      userEvent.click(input);
    });
  });

  it('change immediateAllocation', () => {
    const props = {
      data: [
        {
          idDecision: 'AVG END BAL LST 1',
          immediateAllocation: '123',
          moreInfo: 'Dollar-and-cent',
          name: '',
          searchCode: 'R',
          value: ['3', '136', '137', '224', '310']
        },
        {
          idDecision: 'AVG END BAL LST 2',
          immediateAllocation: 'No',
          moreInfo: 'Dollar-and-cent',
          name: 'ACS CURR PORT',
          searchCode: 'R',
          value: ['3', '136', '137', '224', '310']
        },
        {
          idDecision: 'AVG END BAL LST 3',
          immediateAllocation: '',
          moreInfo: 'Dollar-and-cent',
          name: '',
          searchCode: '',
          value: ['3', '136', '137', '224', '310']
        }
      ],
      handleChangeDataDecision: jest.fn()
    };

    const wrapper = renderComponent(props);
    const input = queryByClass(
      wrapper.container,
      /dls-checkbox-input/
    ) as HTMLElement;

    act(() => {
      userEvent.click(input);
    });

    act(() => {
      userEvent.click(input);
    });
  });

  it('add decision', () => {
    const props = {
      data: [
        {
          idDecision: 'AVG END BAL LST 1',
          immediateAllocation: '123',
          moreInfo: 'Dollar-and-cent',
          name: '',
          searchCode: 'R',
          value: ['3', '136', '137', '224', '310']
        },
        {
          idDecision: 'AVG END BAL LST 2',
          immediateAllocation: 'Yes',
          moreInfo: 'Dollar-and-cent',
          name: 'ACS CURR PORT',
          searchCode: 'R',
          value: ['3', '136', '137', '224', '310']
        },
        {
          idDecision: 'AVG END BAL LST 3',
          immediateAllocation: '',
          moreInfo: 'Dollar-and-cent',
          name: '',
          searchCode: '',
          value: ['3', '136', '137', '224', '310']
        }
      ],
      handleChangeDataDecision: jest.fn()
    };

    const wrapper = renderComponent(props);

    act(() => {
      userEvent.click(
        wrapper.getByText(
          'txt_manage_transaction_level_tlp_config_tq_add_new_element'
        )
      );
    });
  });

  it('render multi', () => {
    const props = {
      data: [
        {
          idDecision: 'AVG END BAL LST 1',
          immediateAllocation: '123',
          moreInfo: 'Dollar-and-cent',
          name: 'ACS PORT CHG DATE',
          searchCode: 'R',
          value: ['3', '136', '137', '224', '310']
        },
        {
          idDecision: 'AVG END BAL LST 2',
          immediateAllocation: 'Yes',
          moreInfo: 'Dollar-and-cent',
          name: 'ACS CURR PORT',
          searchCode: 'R',
          value: ['3', '136', '137', '224', '310']
        },
        {
          idDecision: 'AVG END BAL LST 3',
          immediateAllocation: '',
          moreInfo: 'Dollar-and-cent',
          name: 'ACS CURR PORT1',
          searchCode: '',
          value: ['3', '136', '137', '224', '310']
        },
        {
          idDecision: 'AVG END BAL LST 1',
          immediateAllocation: '123',
          moreInfo: 'Dollar-and-cent',
          name: 'ACS CURR PORT2',
          searchCode: 'R',
          value: ['3', '136', '137', '224', '310']
        },
        {
          idDecision: 'AVG END BAL LST 2',
          immediateAllocation: 'Yes',
          moreInfo: 'Dollar-and-cent',
          name: 'ACS CURR PORT3',
          searchCode: 'R',
          value: ['3', '136', '137', '224', '310']
        },
        {
          idDecision: 'AVG END BAL LST 3',
          immediateAllocation: '',
          moreInfo: 'Dollar-and-cent',
          name: 'ADVERTISING GROUP',
          searchCode: '',
          value: ['3', '136', '137', '224', '310']
        },
        {
          idDecision: 'AVG END BAL LST 3',
          immediateAllocation: '',
          moreInfo: 'Dollar-and-cent',
          name: '',
          searchCode: '',
          value: ['3', '136', '137', '224', '310']
        }
      ],
      handleChangeDataDecision: jest.fn()
    };

    const wrapper = renderComponent(props);

    act(() => {
      userEvent.click(
        wrapper.getByText(
          'txt_manage_transaction_level_tlp_config_tq_add_new_element'
        )
      );
    });
  });
});
