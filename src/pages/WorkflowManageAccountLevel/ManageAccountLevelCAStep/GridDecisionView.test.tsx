import { renderWithMockStore } from 'app/utils';
import 'app/utils/_mockComponent/mockDatePicker';
import 'app/utils/_mockComponent/mockEnhanceMultiSelect';
import * as CommonRedux from 'pages/_commons/redux/Common/select-hooks';
import * as decisionList from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageAccountLevel';
import * as mockMetaData from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageAccountLevel';
import React from 'react';
import GridDecisionView from './GridDecisionView';

const mockUseSelectWindowDimension = jest.spyOn(
  CommonRedux,
  'useSelectWindowDimension'
);

HTMLCanvasElement.prototype.getContext = jest.fn();
const mockDecision = jest.spyOn(decisionList, 'useSelectDecisionListData');
const mockMeta = jest.spyOn(
  mockMetaData,
  'useSelectElementMetadataManageAcountLevel'
);

describe('', () => {
  beforeEach(() => {
    mockDecision.mockReturnValue({
      decisionList: [
        {
          configurable: true,
          immediateAllocation: 'Yes',
          moreInfo:
            'Adaptive Control System portfolio currently in effect for the customer account; variable-length, 4 positions, numeric.',
          name: 'ACS CURR PORT'
        },
        {
          configurable: true,
          immediateAllocation: 'No',
          moreInfo: 'Date (MMDDYYYY)',
          name: 'ACS PORT CHG DATE'
        },
        {
          configurable: true,
          immediateAllocation: 'Yes',
          moreInfo:
            'Issuer-defined advertising group code; variable-length, 3 positions, numeric.',
          name: 'ADVERTISING GROUP'
        },
        {
          configurable: true,
          immediateAllocation: 'Yes',
          moreInfo:
            'Issuer-defined advertising group code; variable-length, 3 positions, numeric.',
          name: 'RESULT ID'
        },
        {
          configurable: true,
          immediateAllocation: 'Yes',
          moreInfo:
            'Issuer-defined advertising group code; variable-length, 3 positions, numeric.',
          name: ''
        }
      ]
    });
    mockMeta.mockReturnValue({
      searchCode: [
        {
          code: 'E',
          text: 'E - Exact'
        },
        {
          code: 'R',
          text: 'R - Ranger'
        },
        {
          code: '',
          text: 'Empty'
        }
      ]
    } as any);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Grid Table Details', async () => {
    mockUseSelectWindowDimension.mockReturnValue({ width: 900, height: 1000 });
    const props = {
      data: [
        {
          name: 'ATTRITION INDEX',
          searchCode: 'R',
          immediateAllocation: 'Yes',
          value: [
            '378',
            '202',
            '448',
            '127',
            '416',
            '31',
            '192',
            '479',
            '304',
            '171'
          ],
          idDecision: 'ATTRITION INDEX__0'
        },
        {
          name: 'ACS CURR PORT',
          searchCode: 'E',
          immediateAllocation: 'Yes',
          value: [
            '270',
            '183',
            '23',
            '467',
            '165',
            '356',
            '489',
            '385',
            '7',
            '433'
          ],
          idDecision: 'ACS CURR PORT'
        },
        {
          name: 'ANN MRCH INT RATE',
          searchCode: 'R',
          immediateAllocation: 'No',
          value: [
            '194',
            '192',
            '456',
            '453',
            '375',
            '271',
            '385',
            '350',
            '69',
            '9'
          ],
          idDecision: 'ANN MRCH INT RATE__2'
        },
        {
          name: 'ALT FIN RPT OTHER1',
          searchCode: 'R',
          immediateAllocation: 'Yes',
          value: [
            '438',
            '397',
            '56',
            '411',
            '242',
            '385',
            '40',
            '222',
            '44',
            '470'
          ],
          idDecision: 'ALT FIN RPT OTHER1__3'
        },
        {
          name: 'ACS PORT CHG DATE',
          searchCode: 'E',
          immediateAllocation: 'No',
          value: [
            '198',
            '143',
            '234',
            '132',
            '101',
            '450',
            '263',
            '200',
            '363',
            '386'
          ],
          idDecision: 'ACS PORT CHG DATE'
        },
        {
          name: 'RESULT ID',
          searchCode: '',
          immediateAllocation: '',
          value: [
            '214',
            '159',
            '241',
            '475',
            '269',
            '64',
            '36',
            '228',
            '171',
            '25'
          ],
          idDecision: 'RESULT ID__5'
        },
        {
          name: 'ADVERTISING GROUP',
          searchCode: 'E',
          idDecision: 1649058054686
        }
      ] as any
    };
    const wrapper = await renderWithMockStore(
      <GridDecisionView {...props} />,
      {}
    );

    expect(
      wrapper.getByText('txt_manage_account_level_table_decision_element')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_account_level_table_decision_search')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText(
        'txt_manage_account_level_table_decision_immediate_allocation'
      )
    ).toBeInTheDocument();
    expect(wrapper.getByText('txt_more_info')).toBeInTheDocument();
  });

  it('Grid Table Details with no data', async () => {
    mockUseSelectWindowDimension.mockReturnValue({ width: 2000, height: 1000 });
    const props = {
      data: undefined as any
    };
    const wrapper = await renderWithMockStore(
      <GridDecisionView {...props} />,
      {}
    );

    expect(
      wrapper.getByText('txt_manage_account_level_table_decision_element')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_account_level_table_decision_search')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText(
        'txt_manage_account_level_table_decision_immediate_allocation'
      )
    ).toBeInTheDocument();
    expect(wrapper.getByText('txt_more_info')).toBeInTheDocument();
  });
});
