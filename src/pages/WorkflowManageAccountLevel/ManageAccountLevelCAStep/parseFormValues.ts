import { getWorkflowSetupStepStatus } from 'app/helpers';
import isEmpty from 'lodash.isempty';

const parseFormValues = (data: WorkflowSetupInstance, id: string) => {
  const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);
  if (!stepInfo) return;

  const configCA = (data.data.configurations?.tableList || []).filter(
    (table: any) => table?.tableType === 'CA'
  );

  return {
    ...getWorkflowSetupStepStatus(stepInfo),
    isEditForm: !isEmpty(configCA[0]?.tableName),
    tables: {
      tableId: configCA[0]?.tableId,
      tableName: configCA[0]?.tableName,
      comment: configCA[0]?.comment,
      tableControlParameters: configCA[0]?.tableControlParameters,
      elementList: configCA[0]?.decisionElements
    },
    isValid: !isEmpty(configCA[0]?.tableName)
  };
};

export default parseFormValues;
