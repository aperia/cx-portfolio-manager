import { formatCommon } from 'app/helpers';
import {
  ColumnType,
  Grid,
  Icon,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import React from 'react';

interface IProps {
  isShowFlyout?: boolean;
  data?: TableContentType[];
}

const GridTableDetail: React.FC<IProps> = ({
  isShowFlyout = false,
  data = []
}) => {
  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();

  const columns: ColumnType[] = [
    {
      id: 'businessName',
      Header: t('txt_manage_account_level_table_decision_business_name'),
      accessor: () =>
        t('txt_manage_account_level_table_decision_ac_allocation_date'),
      width: isShowFlyout ? 230 : 233
    },
    {
      id: 'greenScreenName',
      Header: t('txt_manage_account_level_table_decision_green_name'),
      accessor: () =>
        t('txt_manage_account_level_table_decision_ac_alloc_date'),
      width: isShowFlyout ? 200 : 244
    },
    {
      id: 'value',
      Header: t('txt_manage_account_level_table_decision_value'),
      accessor: data => formatCommon(data?.value).time.date,
      width: isShowFlyout ? 300 : width < 1280 ? 320 : 360
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      cellBodyProps: { className: 'pt-12 pb-8' },
      width: 106,
      accessor: () => (
        <Tooltip
          element={t(
            'txt_manage_account_level_table_decision_ac_alloc_more_info'
          )}
        >
          <Icon name="information" size="5x" className="color-grey-l16" />
        </Tooltip>
      )
    }
  ];

  return <Grid columns={columns} data={data} />;
};

export default GridTableDetail;
