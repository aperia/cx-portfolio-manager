import { fireEvent, render, RenderResult } from '@testing-library/react';
import { mockUseModalRegistryFnc } from 'app/utils';
import 'app/utils/_mockComponent/mockFailedApiReload';
import 'app/utils/_mockComponent/mockNoDataFound';
import 'app/utils/_mockComponent/mockPaging';
import 'app/utils/_mockComponent/mockSortOrder';
import * as WorkflowManageAccountLevelSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageAccountLevel';
import * as WorkflowManagePenaltyFeeSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManagePenaltyFee';
import React from 'react';
import * as reactRedux from 'react-redux';
import MethodListModal from './MethodListModal';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

jest.mock('./ViewModal', () => ({
  __esModule: true,
  default: ({ onOK, onCancel }: any) => {
    return (
      <div>
        <div>ViewModal_component</div>
        <button onClick={onOK}>OK</button>
        <button onClick={onCancel}>Cancel</button>
      </div>
    );
  }
}));

const tableListReturnValue = {
  loading: false,
  error: false,
  tableList: [
    {
      tableId: 'KB3BO58W',
      tableName: 'JUTILH9T',
      comment: 'enim est commodo eos',
      versions: [
        {
          tableId: 'V38A2HCT',
          tableName: 'Y8SZI8QS',
          effectiveDate: '2022-01-26T17:00:00Z',
          status: 'SCHEDULED',
          comment: 'no elitr enim'
        },
        {
          tableId: 'V38A2HCD',
          tableName: 'Y8SZI8QF',
          effectiveDate: '2022-01-26T17:00:00Z',
          status: 'production',
          comment: 'no elitr enim'
        },
        {
          tableId: 'V38A2HCH',
          tableName: 'Y8SZI8QH',
          effectiveDate: '2022-01-26T17:00:00Z',
          status: 'client approved',
          comment: 'no elitr enim'
        },
        {
          tableId: 'V38A2HCK',
          tableName: 'Y8SZI8QK',
          effectiveDate: '2022-01-26T17:00:00Z',
          status: 'working',
          comment: 'no elitr enim'
        },
        {
          tableId: 'V38A2HCL',
          tableName: 'Y8SZI8QL',
          effectiveDate: '2022-01-26T17:00:00Z',
          status: 'validating',
          comment: 'no elitr enim'
        },
        {
          tableId: 'V38A2HCM',
          tableName: 'Y8SZI8QM',
          effectiveDate: '2022-01-26T17:00:00Z',
          status: 'previous',
          comment: 'no elitr enim'
        },
        {
          tableId: 'V38A2HCN',
          tableName: 'Y8SZI8QN',
          effectiveDate: '2022-01-26T17:00:00Z',
          status: 'lapsed',
          comment: 'lapsed'
        },
        {
          tableId: 'V38A2HCP',
          tableName: 'Y8SZI8QA',
          effectiveDate: '2022-01-26T17:00:00Z',
          status: '',
          comment: 'lapsed'
        },
        {
          tableId: 'V38A2HCA',
          tableName: 'Y8SZI8QA',
          effectiveDate: '2022-01-26T17:00:00Z',
          status: '',
          comment: 'lapsed'
        },
        {
          tableId: 'V38A2HCB',
          tableName: 'Y8SZI8QB',
          effectiveDate: '2022-01-26T17:00:00Z',
          status: '',
          comment: 'lapsed'
        },
        {
          tableId: 'V38A2HCC',
          tableName: 'Y8SZI8QC',
          effectiveDate: '2022-01-26T17:00:00Z',
          status: '',
          comment: 'lapsed'
        },
        {
          tableId: 'V38A2HCD',
          tableName: 'Y8SZI8QD',
          effectiveDate: '2022-01-26T17:00:00Z',
          status: '',
          comment: 'lapsed'
        }
      ]
    }
  ]
};

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');
const mockUseSelector = jest.spyOn(reactRedux, 'useSelector');

const mockUseModalRegistry = mockUseModalRegistryFnc();

const MockSelectWorkflowMethodsFilterSelector = jest.spyOn(
  WorkflowManagePenaltyFeeSetup,
  'useSelectWorkflowMethodsFilterSelector'
);

const MockSelectTableListDataSelector = jest.spyOn(
  WorkflowManageAccountLevelSetup,
  'useSelectTableListData'
);

const renderModal = (props: any): RenderResult => {
  return render(
    <div>
      <MethodListModal {...props} />
    </div>
  );
};

const defaultProps = {
  id: 'modal_id',
  isCreateNewVersion: true,
  onClose: jest.fn(),
  setDataNewVersion: jest.fn(),
  handleAddNewVersion: jest.fn(),
  resetClearAndReset: jest.fn()
};

describe('WorkflowManageAccountLevel > ManageAccountLevelCAStep > MethodListModal', () => {
  const mockDispatch = jest.fn();

  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
    mockUseSelector.mockImplementation(selector =>
      selector({
        caches: {}
      })
    );
    mockUseModalRegistry.mockImplementation(() => [true, false]);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Should render modal with No data', () => {
    MockSelectTableListDataSelector.mockReturnValue({
      loading: false,
      error: false,
      tableList: null
    });

    MockSelectWorkflowMethodsFilterSelector.mockReturnValue({
      searchValue: '',
      page: 0,
      pageSize: 10
    });

    const wrapper = renderModal(defaultProps);
    fireEvent.click(wrapper.getByText('No data found'));
  });

  it('Should render modal with view modal click onOK', () => {
    MockSelectTableListDataSelector.mockReturnValue(tableListReturnValue);

    MockSelectWorkflowMethodsFilterSelector.mockReturnValue({
      searchValue: 'test',
      page: 1,
      pageSize: 10
    });

    const wrapper = renderModal(defaultProps);

    const viewButtons = wrapper.getAllByText('txt_view');
    fireEvent.click(viewButtons[0] as Element);
    fireEvent.click(wrapper.getByText('OK') as Element);
  });

  it('Should render modal with view modal click onCancel', () => {
    MockSelectTableListDataSelector.mockReturnValue(tableListReturnValue);

    MockSelectWorkflowMethodsFilterSelector.mockReturnValue({
      searchValue: 'test',
      page: 1,
      pageSize: 10
    });

    const wrapper = renderModal(defaultProps);
    const viewButtons = wrapper.getAllByText('txt_view');

    fireEvent.click(viewButtons[0] as Element);
    fireEvent.click(wrapper.getByText('Cancel') as Element);
  });
  it('Should render modal with view modal one data', () => {
    MockSelectTableListDataSelector.mockReturnValue({
      loading: false,
      error: false,
      tableList: [
        {
          tableId: 'KB3BO58W',
          tableName: 'JUTILH9T',
          comment: 'enim est commodo eos',
          versions: [
            {
              tableId: 'V38A2HCT',
              tableName: 'Y8SZI8QS',
              effectiveDate: '2022-01-26T17:00:00Z',
              status: 'SCHEDULED',
              comment: 'no elitr enim'
            }
          ]
        }
      ]
    });

    MockSelectWorkflowMethodsFilterSelector.mockReturnValue({
      searchValue: 'test',
      page: 1,
      pageSize: 10
    });

    const wrapper = renderModal({ ...defaultProps, isClearAndReset: true });
    fireEvent.click(
      wrapper.baseElement.querySelector(
        'input[class="custom-control-input dls-radio-input checked-style"]'
      ) as Element
    );
    fireEvent.change(
      wrapper.getByPlaceholderText('Type to search by table name'),
      { target: { value: 'abc' } }
    );
    fireEvent.click(
      wrapper.baseElement.querySelector(
        'i[class="icon icon-search"]'
      ) as Element
    );
    fireEvent.click(wrapper.getByText('No data found'));
  });

  it('Should render modal with order and paging', () => {
    MockSelectTableListDataSelector.mockReturnValue(tableListReturnValue);

    MockSelectWorkflowMethodsFilterSelector.mockReturnValue({
      searchValue: 'test',
      page: 1,
      pageSize: 10
    });

    const wrapper = renderModal(defaultProps);
    fireEvent.click(wrapper.getByText('Sort By'));
    fireEvent.click(wrapper.getByText('Order By'));
    fireEvent.click(wrapper.getByText('Clear And Reset'));
    fireEvent.click(
      wrapper.baseElement.querySelector(
        'button[class="change-page-number"]'
      ) as Element
    );
    fireEvent.click(
      wrapper.baseElement.querySelector(
        'button[class="change-page-size"]'
      ) as Element
    );
  });
});
