import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import { matchSearchValue } from 'app/helpers';
import {
  Button,
  CheckBox,
  ColumnType,
  ComboBox,
  DropdownList,
  Grid,
  Icon,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import {
  useSelectDecisionListData,
  useSelectElementMetadataManageAcountLevel
} from 'pages/_commons/redux/WorkflowSetup';
import ClearAndResetButton from 'pages/_commons/Utils/ClearAndResetButton';
import React, { useEffect, useMemo, useRef, useState } from 'react';

export interface DecisionTypeValue {
  idDecision: string;
  immediateAllocation: string;
  name: string;
  searchCode: string;
  value: string[];
}
interface IProps {
  data: DecisionTypeValue[];
  handleChangeDataDecision: (data: DecisionTypeValue[]) => void;
}
const GridDecision: React.FC<IProps> = ({
  data = [],
  handleChangeDataDecision
}) => {
  const { t } = useTranslation();
  const searchRef = useRef<any>(null);
  const { width } = useSelectWindowDimension();

  const [searchValue, setSearchValue] = useState<string>('');
  const [errorDecision, setErrorDecision] = useState<MagicKeyValue>({});
  const [errorSearchCode, setSearchCode] = useState<MagicKeyValue>({});

  const decisionList = useSelectDecisionListData()?.decisionList;
  const metadata = useSelectElementMetadataManageAcountLevel();
  const tooltipDragList = useMemo(() => {
    if (!searchValue) return [];
    return data.map(item => ({
      id: item?.idDecision,
      tooltipProps: {
        element: t(
          'txt_manage_account_level_table_decision_tooltip_disabled_dnd'
        )
      }
    }));
  }, [searchValue, data, t]);

  const handleSearch = (val: string) => {
    setSearchValue(val);
  };

  const dataList = data
    .filter((item: DecisionType) => {
      const val = decisionList.find(
        (i: DecisionType) => i?.name === item?.name
      );
      return val?.configurable || isEmpty(item?.name);
    })
    .map((decision: DecisionType) => {
      const decisionData = decisionList?.filter(
        (item: MagicKeyValue) => item?.name === decision?.name
      )[0];
      return {
        ...decision,
        moreInfo: decisionData?.moreInfo
      };
    });

  const filteredList = useMemo(() => {
    return dataList.filter(
      (w: MagicKeyValue) =>
        (isEmpty(w?.name) && isEmpty(searchValue)) ||
        matchSearchValue([w?.name, w?.moreInfo], searchValue)
    );
  }, [searchValue, dataList]);

  const decisionMapData = decisionList?.filter((item: MagicKeyValue) => {
    const index = dataList.findIndex(
      (itemDecision: MagicKeyValue) => item?.name === itemDecision?.name
    );
    return item?.configurable && index === -1;
  });

  const handleChangeForm = (name: string, val: string, id: string) => {
    const valTemp = [...data];
    const index = valTemp.findIndex(item => item?.name === name);
    valTemp[index] = {
      ...valTemp[index],
      name: val,
      searchCode: '',
      immediateAllocation: 'No'
    };

    handleChangeDataDecision(valTemp);

    if (!isEmpty(val)) {
      setErrorDecision({
        ...errorDecision,
        [id]: ''
      });
    }
  };

  const handleChangeSearchCode = (name: string, val: string, id: string) => {
    const valTemp = [...data];
    const index = valTemp.findIndex(item => item?.name === name);
    valTemp[index] = {
      ...valTemp[index],
      searchCode: val
    };

    handleChangeDataDecision(valTemp);

    if (isEmpty(val)) {
      setSearchCode({
        ...errorSearchCode,
        [id]: t('txt_manage_account_level_table_decision_search_code_required')
      });
      return;
    }
    setSearchCode({
      ...errorSearchCode,
      [id]: ''
    });
  };

  const handleRemove = (id: string) => {
    const dataTemp = data.filter((item: MagicKeyValue) => {
      const val = decisionList.find(
        (i: DecisionType) => i?.name === item?.name
      );
      return !val?.configurable;
    });
    if (dataList.length === 1) {
      handleChangeDataDecision([
        ...dataTemp,
        {
          name: '',
          searchCode: '',
          immediateAllocation: 'No',
          idDecision: new Date().getTime()
        }
      ] as any);
      return;
    }
    const valTemp = [...dataList, ...dataTemp];
    const decisionElements = valTemp.filter(
      (item: MagicKeyValue) => item?.idDecision !== id
    );
    handleChangeDataDecision(decisionElements as any);
  };

  const handleChangeImmediate = (val: string, name: string) => {
    const valTemp = [...data];
    const index = valTemp.findIndex(item => item?.name === name);
    valTemp[index] = {
      ...valTemp[index],
      immediateAllocation: val === 'Yes' ? 'No' : 'Yes'
    };

    handleChangeDataDecision(valTemp);
  };

  const handleBlurDecision = (idDecision: string) => {
    const decisions = [...dataList];
    const decisionVal = decisions.filter(
      (item: MagicKeyValue) => item?.idDecision === idDecision
    )[0];
    if (isEmpty(decisionVal?.name)) {
      setErrorDecision({
        ...errorDecision,
        [idDecision]: t('txt_manage_account_level_table_decision_name_required')
      });
    }
  };

  const handleBlurSearch = (idDecision: string) => {
    const decisions = [...dataList];
    const decisionVal = decisions.filter(
      (item: MagicKeyValue) => item?.idDecision === idDecision
    )[0];
    if (isEmpty(decisionVal?.searchCode)) {
      setSearchCode({
        ...errorSearchCode,
        [idDecision]: t(
          'txt_manage_account_level_table_decision_search_code_required'
        )
      });
    }
  };

  const columns: ColumnType[] = [
    {
      id: 'name',
      Header: t('txt_manage_account_level_table_decision_element'),
      width: width > 1280 ? undefined : 280,
      required: true,
      cellBodyProps: { className: 'py-8' },
      accessor: ({ name, idDecision }) => (
        <ComboBox
          placeholder={t(
            'txt_workflow_manage_change_in_terms_select_an_option'
          )}
          textField="text"
          value={name}
          size="small"
          noResult={t('txt_no_results_found_without_dot')}
          onChange={e => handleChangeForm(name, e.target.value, idDecision)}
          onBlur={() => handleBlurDecision(idDecision)}
          error={{
            status: !!errorDecision[idDecision],
            message: errorDecision[idDecision]
          }}
        >
          {decisionMapData?.map((item: MagicKeyValue, index: number) => {
            return (
              <ComboBox.Item
                value={item?.name}
                label={item?.name}
                key={item?.name}
              />
            );
          })}
        </ComboBox>
      )
    },
    {
      id: 'searchCode',
      Header: t('txt_manage_account_level_table_decision_search'),
      width: width > 1280 ? 240 : 210,
      required: true,
      cellBodyProps: { className: 'py-8' },
      accessor: ({ searchCode, name, idDecision }) => {
        const val = metadata?.searchCode.filter(
          item => item?.code === searchCode
        )[0]?.text;
        if (isEmpty(name)) return <span />;
        return (
          <DropdownList
            placeholder={t(
              'txt_workflow_manage_change_in_terms_select_an_option'
            )}
            textField="text"
            value={val}
            size="small"
            noResult={t('txt_no_results_found_without_dot')}
            onChange={e =>
              handleChangeSearchCode(name, e.target.value, idDecision)
            }
            onBlur={() => handleBlurSearch(idDecision)}
            error={{
              status: !!errorSearchCode[idDecision],
              message: errorSearchCode[idDecision]
            }}
          >
            {metadata?.searchCode.map((item: MagicKeyValue) => {
              return (
                <DropdownList.Item
                  value={item?.code}
                  label={item?.text}
                  key={item?.text}
                />
              );
            })}
          </DropdownList>
        );
      }
    },
    {
      id: 'immediateAllocation',
      Header: t('txt_manage_account_level_table_decision_immediate_allocation'),
      width: 182,
      className: 'text-center',
      cellBodyProps: { className: 'pt-12 pb-8' },
      accessor: ({ name, immediateAllocation }) => {
        const decision = decisionList?.filter(
          (item: MagicKeyValue) => item?.name === name
        )[0];
        if (!decision?.immediateAllocation || isEmpty(name)) return <span />;
        return (
          <CheckBox
            className="custom-checkbox-grid mx-auto"
            onChange={e => handleChangeImmediate(immediateAllocation, name)}
          >
            <CheckBox.Input
              id={name}
              name="name"
              checked={immediateAllocation === 'Yes'}
            />
          </CheckBox>
        );
      }
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      width: 106,
      cellBodyProps: { className: 'pt-12 pb-8' },
      accessor: ({ name }) => {
        const decision = decisionList?.filter(
          (item: MagicKeyValue) => item?.name === name
        )[0];
        if (isEmpty(name)) return <span />;
        return (
          <Tooltip element={decision?.moreInfo}>
            <Icon name="information" size="5x" className="color-grey-l16" />
          </Tooltip>
        );
      }
    },
    {
      id: 'action',
      Header: t('txt_action'),
      className: 'text-center',
      width: 101,
      cellBodyProps: { className: 'py-8 px-12' },
      accessor: ({ idDecision, name }) => {
        if (dataList.length === 1 && isEmpty(name)) return <span />;
        return (
          <Button
            size="sm"
            variant="outline-danger"
            onClick={() => handleRemove(idDecision)}
          >
            {t('txt_remove')}
          </Button>
        );
      }
    }
  ];

  useEffect(() => {
    !searchValue && searchRef?.current?.clear();
  }, [searchValue]);

  /* istanbul ignore next */
  const handleChangeData = (nextData: MagicKeyValue[]) => {
    const dataTemp = data?.filter((item: MagicKeyValue) => {
      const val = decisionList.find(
        (i: DecisionType) => i?.name === item?.name
      );
      return !val?.configurable;
    });
    const decisionElements = [...nextData, ...dataTemp];
    handleChangeDataDecision(decisionElements as any);
  };

  const handleClearAndReset = () => {
    setSearchValue('');
  };

  const handleAddDecision = () => {
    setSearchValue('');
    const valTemp = [
      ...data,
      {
        name: '',
        searchCode: '',
        immediateAllocation: 'No',
        idDecision: new Date().getTime()
      }
    ];

    handleChangeDataDecision(valTemp as any);
  };

  return (
    <>
      <div className="d-flex justify-content-between align-items-center">
        <h5>{t('txt_manage_account_level_table_decision_element_list')}</h5>
        <div className="method-list-simple-search">
          <SimpleSearch
            ref={searchRef}
            defaultValue=""
            clearTooltip={t('txt_clear_search_criteria')}
            placeholder="Type to search by element, more info"
            onSearch={handleSearch}
          />
        </div>
      </div>

      <div className="d-flex justify-content-end mt-16 mr-n8">
        {dataList?.length > 5 ? (
          <Tooltip
            element={t(
              'txt_manage_transaction_level_tlp_config_tq_maximum_element'
            )}
          >
            <Button size="sm" variant="outline-primary" disabled>
              {t('txt_manage_transaction_level_tlp_config_tq_add_new_element')}
            </Button>
          </Tooltip>
        ) : (
          <Button
            size="sm"
            variant="outline-primary"
            onClick={handleAddDecision}
          >
            {t('txt_manage_transaction_level_tlp_config_tq_add_new_element')}
          </Button>
        )}
        {!!searchValue && !isEmpty(filteredList) && (
          <div className="d-flex justify-content-end ml-8">
            <ClearAndResetButton small onClearAndReset={handleClearAndReset} />
          </div>
        )}
      </div>
      {isEmpty(filteredList) && (
        <div className="d-flex flex-column justify-content-center mt-40 mb-24 mb-xl-0">
          <NoDataFound
            id="newMethod_notfound"
            hasSearch={!!searchValue}
            title={t('txt_no_results_found')}
            linkTitle={!!searchValue && t('txt_clear_and_reset')}
            onLinkClicked={handleClearAndReset}
          />
        </div>
      )}
      {!isEmpty(filteredList) && (
        <div className="mt-16">
          <Grid
            columns={columns}
            data={filteredList}
            variant={{
              id: 'dragDrop',
              type: 'dnd',
              isFixedLeft: true,
              cellBodyProps: {
                className: 'text-center'
              }
            }}
            dnd
            dataItemKey={'idDecision'}
            onDataChange={handleChangeData}
            dndReadOnly={!isEmpty(searchValue)}
            dndButtonConfigList={tooltipDragList}
          />
        </div>
      )}
    </>
  );
};

export default GridDecision;
