import BadgeGroupTextControl from 'app/components/DofControl/BadgeGroupTextControl';
import GroupTextControl from 'app/components/DofControl/GroupTextControl';
import { BadgeColorType } from 'app/constants/enums';
import { Button, useTranslation } from 'app/_libraries/_dls';
import { isFunction } from 'app/_libraries/_dls/lodash';
import React from 'react';
import { ManageAccountLevelCAFormValues } from '.';
import GridDecisionView from './GridDecisionView';
import GridTableDetail from './GridTableDetails';

const Summary: React.FC<
  WorkflowSetupSummaryProps<ManageAccountLevelCAFormValues>
> = ({ onEditStep, formValues, stepId, selectedStep }) => {
  const { t } = useTranslation();
  const handleEdit = () => {
    isFunction(onEditStep) && onEditStep();
  };
  return (
    <div className="position-relative">
      <div className="absolute-top-right mt-n26 mr-n8">
        <Button variant="outline-primary" size="sm" onClick={handleEdit}>
          {t('txt_edit')}
        </Button>
      </div>
      <div className="row">
        <div className="col-6 col-xl-3">
          <GroupTextControl
            id="tableId"
            input={{ value: formValues?.tables?.tableName } as any}
            meta={{} as any}
            label={t('txt_manage_account_level_table_id')}
            options={{ inline: false }}
          />
        </div>
        <div className="col-6 col-xl-3">
          <GroupTextControl
            id="tableId"
            input={{ value: formValues?.tables?.tableId } as any}
            meta={{} as any}
            label={t('txt_manage_account_level_table_id')}
            options={{ inline: false }}
          />
        </div>
        <div className="col-6 col-xl-3">
          <div className="form-group-static align-items-center">
            <span className="form-group-static__label">
              <span className="color-grey text-nowrap mr-2">
                {t('txt_manage_account_level_table_action_taken')}
              </span>
            </span>
            <BadgeGroupTextControl
              id="manage_account_level_table_version_created"
              input={
                {
                  value: t('txt_manage_account_level_table_version_created')
                } as any
              }
              meta={{} as any}
              options={{
                colorType: BadgeColorType.VersionCreated,
                noBorder: true
              }}
            />
          </div>
        </div>
        <div className="col-6">
          <GroupTextControl
            id="comment"
            input={{ value: formValues?.tables?.comment } as any}
            meta={{} as any}
            label={t('Comment Area')}
            options={{
              inline: false,
              lineTruncate: 2,
              ellipsisLessText: t('txt_less'),
              ellipsisMoreText: t('txt_more')
            }}
          />
        </div>
      </div>
      <div className="d-flex align-items-center justify-content-between mb-16 mt-24">
        <h5>{t('txt_manage_account_level_table_parameters')}</h5>
      </div>
      <GridTableDetail data={formValues?.tables?.tableControlParameters} />
      <div className="d-flex align-items-center justify-content-between">
        <h5 className="mb-16 mt-24">
          {t('txt_manage_account_level_table_decision_element_list')}
        </h5>
      </div>
      <GridDecisionView data={formValues?.tables?.elementList || []} />
    </div>
  );
};

export default Summary;
