import get from 'lodash.get';
import { StatusName } from './MethodListModal';

export const mappingData = (data: DecisionType[]) => {
  const arrayValues = data[0]?.value || [];
  return arrayValues.reduce((current: any, _, index) => {
    const result = data!.reduce((currentMap: any, valueMap) => {
      return {
        ...currentMap,
        [valueMap.name]: get(valueMap, ['value', index])
      };
    }, {});
    return [...current, result];
  }, []);
};

export const mapColorBadge = (status: string) => {
  switch (status.toLocaleLowerCase()) {
    case StatusName.scheduled:
    case StatusName.working:
      return 'cyan';
    case StatusName.validating:
    case StatusName.previous:
      return 'orange';
    case StatusName.clientApproved:
    case StatusName.production:
      return 'green';
    case StatusName.lapsed:
      return 'red';
    default:
      return 'grey';
  }
};
