import { fireEvent, render, RenderResult } from '@testing-library/react';
import 'app/utils/_mockComponent/mockNoDataFound';
import 'app/utils/_mockComponent/mockPaging';
import { queryByClass } from 'app/_libraries/_dls/test-utils';
import * as CommonRedux from 'pages/_commons/redux/Common/select-hooks';
import * as mockStore from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageAccountLevel';
import React from 'react';
import * as reactRedux from 'react-redux';
import SubContent from './SubContent';

const mockDecision = jest.spyOn(mockStore, 'useSelectDecisionListData');
const mockDecisionData = jest.spyOn(mockStore, 'useSelectDecisionData');
const mockUseSelector = jest.spyOn(reactRedux, 'useSelector');

const mockUseSelectWindowDimension = jest.spyOn(
  CommonRedux,
  'useSelectWindowDimension'
);

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderModal = (props: any): RenderResult => {
  return render(
    <div>
      <SubContent {...props} />
    </div>
  );
};

describe('WorkflowManageAccountLevel > ManageAccountLevelCAStep > SubContent', () => {
  // const mockDispatch = jest.fn();

  beforeEach(() => {
    mockUseSelector.mockImplementation(selector =>
      selector({
        caches: {}
      })
    );
    mockUseSelectWindowDimension.mockReturnValue({ width: 1200, height: 1000 });
    mockDecision.mockReturnValue({
      decisionList: [
        {
          configurable: false,
          immediateAllocation: 'Yes',
          moreInfo:
            'Adaptive Control System portfolio currently in effect for the customer account; variable-length, 4 positions, numeric.',
          name: 'ACS CURR PORT'
        },
        {
          configurable: false,
          immediateAllocation: 'No',
          moreInfo: 'Date (MMDDYYYY)',
          name: 'ACS PORT CHG DATE'
        },
        {
          configurable: true,
          immediateAllocation: 'Yes',
          moreInfo:
            'Issuer-defined advertising group code; variable-length, 3 positions, numeric.',
          name: 'ADVERTISING GROUP'
        },
        {
          configurable: true,
          immediateAllocation: 'Yes',
          moreInfo:
            'Issuer-defined advertising group code; variable-length, 3 positions, numeric.',
          name: ''
        }
      ]
    });
    mockDecisionData.mockReturnValue([
      { decisionElement: 'code', description: 'text' }
    ]);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Should render subcontent', () => {
    const wrapper = renderModal({});
    const iconPlus = queryByClass(wrapper.container, /icon icon-plus/);
    fireEvent.click(iconPlus as HTMLElement);
    expect(wrapper.getByRole('button')).toBeInTheDocument();
  });
  it('Should render subcontent with width 1300', () => {
    mockUseSelectWindowDimension.mockReturnValue({ width: 1300, height: 1000 });
    const wrapper = renderModal({});
    const iconPlus = queryByClass(wrapper.container, /icon icon-plus/);
    fireEvent.click(iconPlus as HTMLElement);
    expect(wrapper.getByRole('button')).toBeInTheDocument();
  });
  it('Should render subcontent no data', () => {
    mockDecision.mockReturnValue({});
    mockDecisionData.mockReturnValue(null as any);
    const wrapper = renderModal({ selectedStep: '', stepId: 'stepId' });
    expect(wrapper.getByRole('button')).toBeInTheDocument();
  });
});
