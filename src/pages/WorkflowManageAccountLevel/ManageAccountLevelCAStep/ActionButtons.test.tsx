import { fireEvent, render } from '@testing-library/react';
import React from 'react';
import ActionButtons from './ActionButtons';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

describe('WorkflowManageAccountLevel > ManageAccountLevelCAStep > ActionButtons', () => {
  it('Should render with large buttons', () => {
    const props = {
      onClickCreateNewVersion: jest.fn()
    };
    const wrapper = render(<ActionButtons {...props} />);
    expect(
      wrapper.getByText('txt_manage_account_level_select_an_option')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    );
    expect(props.onClickCreateNewVersion).toHaveBeenCalled();
  });
});
