import {
  fireEvent,
  queryByText,
  render,
  RenderResult
} from '@testing-library/react';
import * as CommonRedux from 'pages/_commons/redux/Common/select-hooks';
import React from 'react';
import * as reactRedux from 'react-redux';
import ManageAccountCAStep from './index';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const mockUseSelectWindowDimension = jest.spyOn(
  CommonRedux,
  'useSelectWindowDimension'
);

const mockUseSelectWindowDimensionImplementation = (width: number) => {
  mockUseSelectWindowDimension.mockImplementation(() => ({ width } as any));
};

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');

const MockComponent = ({ onEditStep }: any) => {
  return (
    <div>
      <div>StepSummary</div>
      <button onClick={onEditStep}>EditStep</button>
    </div>
  );
};

const mockOnCloseWithoutMethod = jest.fn();
jest.mock('./MethodListModal', () => ({
  __esModule: true,
  default: ({ onClose, handleAddNewVersion, setDataNewVersion }: any) => {
    const handleOnClose = () => {
      if (mockOnCloseWithoutMethod()) {
        onClose();
        return;
      }

      onClose({
        id: '1',
        name: 'method-1',
        versions: [
          {
            id: 'version 1.0'
          }
        ]
      });
    };

    return (
      <>
        <div onClick={handleOnClose}>MethodListModal_component</div>
        <div onClick={handleAddNewVersion}>handleAddNewVersion</div>
        <div
          onClick={() =>
            setDataNewVersion({
              ...formValue.dataTableVersion,
              tableId: 'ABCD'
            })
          }
        >
          setDataNewVersion
        </div>
      </>
    );
  }
}));

jest.mock('./AddNewMethodModal', () => ({
  __esModule: true,
  default: ({
    onClose,
    handleReviewNewVersion,
    handleBackTableList,
    setFormValues
  }: any) => {
    const handleOnClose = () => {
      if (mockOnCloseWithoutMethod()) {
        onClose();
        return;
      }

      onClose({
        id: '1',
        name: 'method-1',
        versions: [
          {
            id: 'version 1.0'
          }
        ]
      });
    };

    const reviewNewVersion = () => {
      handleReviewNewVersion();
    };

    return (
      <>
        <div onClick={handleOnClose}>AddNewMethodModal</div>
        <div onClick={reviewNewVersion}>handleReviewNewVersion</div>
        <div onClick={handleBackTableList}>handleBackTableList</div>
        <div onClick={setFormValues}>setFormValues</div>
      </>
    );
  }
}));

jest.mock('./ViewNewVersion', () => ({
  __esModule: true,
  default: ({ onClose, handleEditNewVersion, handleDeleteNewVersion }: any) => {
    const handleOnClose = () => {
      if (mockOnCloseWithoutMethod()) {
        onClose();
        return;
      }

      onClose({
        id: '1',
        name: 'method-1',
        versions: [
          {
            id: 'version 1.0'
          }
        ]
      });
    };

    return (
      <>
        <div onClick={handleOnClose}>ViewNewVersion</div>
        <div onClick={handleEditNewVersion}>handleEditNewVersion</div>
        <div onClick={handleDeleteNewVersion}>handleDeleteNewVersion</div>
      </>
    );
  }
}));

const renderComponent = (props: any): RenderResult => {
  return render(<ManageAccountCAStep {...props} />);
};

const formValue = {
  isValid: true,
  tables: {
    tableId: 'DXHWNJ7L',
    tableName: '123',
    comment: '',
    tableControlParameters: [
      { name: 'allocation.date', value: '12/17/2021 5:00:00 PM' }
    ],
    elementList: [
      {
        name: 'ATTRITION INDEX',
        searchCode: 'R',
        immediateAllocation: 'Yes',
        value: [
          '378',
          '202',
          '448',
          '127',
          '416',
          '31',
          '192',
          '479',
          '304',
          '171'
        ],
        idDecision: 'ATTRITION INDEX__0'
      },
      {
        name: 'ALT FIN RPT ENTITY',
        searchCode: 'E',
        immediateAllocation: 'Yes',
        value: [
          '270',
          '183',
          '23',
          '467',
          '165',
          '356',
          '489',
          '385',
          '7',
          '433'
        ],
        idDecision: 'ALT FIN RPT ENTITY__1'
      },
      {
        name: 'ANN MRCH INT RATE',
        searchCode: 'R',
        immediateAllocation: 'No',
        value: [
          '194',
          '192',
          '456',
          '453',
          '375',
          '271',
          '385',
          '350',
          '69',
          '9'
        ],
        idDecision: 'ANN MRCH INT RATE__2'
      },
      {
        name: 'ALT FIN RPT OTHER1',
        searchCode: 'R',
        immediateAllocation: 'No',
        value: [
          '438',
          '397',
          '56',
          '411',
          '242',
          '385',
          '40',
          '222',
          '44',
          '470'
        ],
        idDecision: 'ALT FIN RPT OTHER1__3'
      },
      {
        name: 'APPROVAL OFFICER',
        searchCode: 'E',
        immediateAllocation: 'No',
        value: [
          '198',
          '143',
          '234',
          '132',
          '101',
          '450',
          '263',
          '200',
          '363',
          '386'
        ],
        idDecision: 'APPROVAL OFFICER__4'
      },
      {
        name: 'RESULT ID',
        searchCode: '',
        immediateAllocation: '',
        value: [
          '214',
          '159',
          '241',
          '475',
          '269',
          '64',
          '36',
          '228',
          '171',
          '25'
        ],
        idDecision: 'RESULT ID__5'
      }
    ]
  }
};

jest.mock('app/hooks/useUnsavedChangeRegistry', () => {
  return {
    __esModule: true,
    useUnsavedChangeRegistry: (options: any, changes: any, onConfirm: any) => {
      onConfirm && onConfirm();
    }
  };
});

describe('WorkflowManageAccountLevel > ManageAccountLevelCAStep > Index', () => {
  const mockDispatch = jest.fn();

  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
    mockOnCloseWithoutMethod.mockReturnValue(true);
  });

  afterEach(() => {
    mockUseSelectWindowDimension.mockClear();
    mockUseDispatch.mockClear();
  });

  it('Should render with no method > Create new version', () => {
    const props = {
      stepId: '1',
      selectedStep: 'selectedStep',
      formValues: {},
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    );
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('MethodListModal_component'));
    expect(
      queryByText(wrapper.container, /MethodListModal_component/i)
    ).not.toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    );
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('MethodListModal_component'));
    expect(
      queryByText(wrapper.container, /MethodListModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render > Edit method', () => {
    mockUseSelectWindowDimensionImplementation(1300);

    const props = {
      stepId: '1',
      selectedStep: 'selectedStep',
      formValues: formValue,
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    );
    fireEvent.click(wrapper.getByText('handleAddNewVersion'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal/i)
    ).toBeInTheDocument();
    expect(wrapper.getByText('AddNewMethodModal')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('AddNewMethodModal'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal/i)
    ).not.toBeInTheDocument();
  });

  it('Should render > Edit method > setDataNewVersion', () => {
    mockUseSelectWindowDimensionImplementation(1300);

    const props = {
      stepId: '1',
      selectedStep: 'selectedStep',
      formValues: formValue,
      setFormValues: jest.fn(),
      clearFormValues: jest.fn(),
      dependencies: {
        files: { hasFile: true }
      }
    };

    const wrapper = renderComponent(props);
    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    );
    fireEvent.click(wrapper.getByText('handleAddNewVersion'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal/i)
    ).toBeInTheDocument();
    expect(wrapper.getByText('AddNewMethodModal')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('setDataNewVersion'));

    fireEvent.click(wrapper.getByText('AddNewMethodModal'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal/i)
    ).not.toBeInTheDocument();
  });

  it('Should render > Edit method > back to table list', () => {
    mockUseSelectWindowDimensionImplementation(900);

    const props = {
      stepId: '1',
      selectedStep: 'selectedStep',
      formValues: formValue,
      setFormValues: jest.fn(),
      clearFormValues: jest.fn(),
      dependencies: {
        files: { hasFile: true }
      }
    };

    const wrapper = renderComponent(props);
    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    );
    fireEvent.click(wrapper.getByText('handleAddNewVersion'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal/i)
    ).toBeInTheDocument();

    const addMethodModal = wrapper.getByText('AddNewMethodModal');
    expect(addMethodModal).toBeInTheDocument();
    expect(wrapper.getByText('handleBackTableList')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('handleBackTableList'));
    fireEvent.click(wrapper.getByText('handleAddNewVersion'));
  });

  it('Should render > Edit method > back to table list and set Data', () => {
    mockUseSelectWindowDimensionImplementation(900);

    const props = {
      stepId: '1',
      selectedStep: 'selectedStep',
      formValues: formValue,
      setFormValues: jest.fn(),
      clearFormValues: jest.fn(),
      dependencies: {
        files: { hasFile: true }
      }
    };

    const wrapper = renderComponent(props);
    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    );
    fireEvent.click(wrapper.getByText('handleAddNewVersion'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal/i)
    ).toBeInTheDocument();

    const addMethodModal = wrapper.getByText('AddNewMethodModal');
    expect(addMethodModal).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('setDataNewVersion'));
    expect(wrapper.getByText('handleBackTableList')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('handleBackTableList'));
    fireEvent.click(wrapper.getByText('setDataNewVersion'));
  });

  it('Should render > ViewNewVersion', () => {
    mockUseSelectWindowDimensionImplementation(900);

    const props = {
      stepId: '1',
      selectedStep: 'selectedStep',
      formValues: formValue,
      setFormValues: jest.fn(),
      clearFormValues: jest.fn(),
      dependencies: {
        files: { hasFile: true }
      }
    };

    const wrapper = renderComponent(props);
    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    );
    fireEvent.click(wrapper.getByText('handleAddNewVersion'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal/i)
    ).toBeInTheDocument();

    const addMethodModal = wrapper.getByText('AddNewMethodModal');
    expect(addMethodModal).toBeInTheDocument();
    expect(wrapper.getByText('handleReviewNewVersion')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('handleReviewNewVersion'));
    expect(wrapper.getByText('ViewNewVersion')).toBeInTheDocument();
  });

  it('Should render > ViewNewVersion > handle Edit New Version', () => {
    mockUseSelectWindowDimensionImplementation(900);

    const props = {
      stepId: '1',
      selectedStep: 'selectedStep',
      formValues: formValue,
      setFormValues: jest.fn(),
      clearFormValues: jest.fn(),
      dependencies: {
        files: { hasFile: true }
      }
    };

    const wrapper = renderComponent(props);
    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    );
    fireEvent.click(wrapper.getByText('handleAddNewVersion'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal/i)
    ).toBeInTheDocument();

    const addMethodModal = wrapper.getByText('AddNewMethodModal');
    expect(addMethodModal).toBeInTheDocument();
    expect(wrapper.getByText('handleReviewNewVersion')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('handleReviewNewVersion'));
    expect(wrapper.getByText('ViewNewVersion')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('handleEditNewVersion'));
    fireEvent.click(wrapper.getByText('AddNewMethodModal'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal/i)
    ).not.toBeInTheDocument();
  });

  it('Should render > ViewNewVersion > handle Delete New Version', () => {
    mockUseSelectWindowDimensionImplementation(900);

    const props = {
      stepId: '1',
      selectedStep: 'selectedStep',
      formValues: formValue,
      setFormValues: jest.fn(),
      clearFormValues: jest.fn(),
      dependencies: {
        files: { hasFile: true }
      }
    };

    const wrapper = renderComponent(props);
    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    );
    fireEvent.click(wrapper.getByText('handleAddNewVersion'));

    expect(
      queryByText(wrapper.container, /AddNewMethodModal/i)
    ).toBeInTheDocument();
    const addMethodModal = wrapper.getByText('AddNewMethodModal');
    expect(addMethodModal).toBeInTheDocument();
    expect(wrapper.getByText('handleReviewNewVersion')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('handleReviewNewVersion'));
    expect(wrapper.getByText('ViewNewVersion')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('handleDeleteNewVersion'));
  });

  it('Should render > ViewNewVersion > handle Update data table', () => {
    mockUseSelectWindowDimensionImplementation(900);

    const props = {
      stepId: '1',
      selectedStep: 'selectedStep',
      formValues: formValue,
      setFormValues: jest.fn().mockReturnValue({
        isValid: true,
        tables: {
          tableId: 'ABC',
          tableName: '123',
          comment: '',
          tableControlParameters: [
            { name: 'allocation.date', value: '12/17/2021 5:00:00 PM' }
          ],
          elementList: [
            {
              name: 'ATTRITION INDEX',
              searchCode: 'R',
              immediateAllocation: 'Yes',
              value: [
                '378',
                '202',
                '448',
                '127',
                '416',
                '31',
                '192',
                '479',
                '304',
                '171'
              ],
              idDecision: 'ATTRITION INDEX__0'
            }
          ]
        }
      }),
      clearFormValues: jest.fn(),
      dependencies: {
        files: { hasFile: true }
      },
      summaries: [
        {
          stepId: '0',
          title: 'title-1',
          element: MockComponent,
          dependencies: {}
        } as any
      ] as any,
      isEdit: true,
      savedAt: 123,
      handleSave: jest.fn()
    } as any;

    const { container, getByText, rerender } = render(
      <ManageAccountCAStep {...props} />
    );
    fireEvent.click(getByText('txt_manage_penalty_fee_create_new_version'));
    fireEvent.click(getByText('handleAddNewVersion'));

    expect(queryByText(container, /AddNewMethodModal/i)).toBeInTheDocument();

    fireEvent.click(getByText('setFormValues'));
    rerender(
      <ManageAccountCAStep
        {...props}
        formValues={{
          isValid: true,
          tables: {
            tableId: 'ABC',
            tableName: '123',
            comment: '',
            tableControlParameters: [
              { name: 'allocation.date', value: '12/17/2021 5:00:00 PM' }
            ],
            elementList: [
              {
                name: 'ATTRITION INDEX',
                searchCode: 'R',
                immediateAllocation: 'Yes',
                value: [
                  '378',
                  '202',
                  '448',
                  '127',
                  '416',
                  '31',
                  '192',
                  '479',
                  '304',
                  '171'
                ],
                idDecision: 'ATTRITION INDEX__0'
              }
            ]
          }
        }}
      />
    ) as any;
  });

  it('Should render with no method > render stuck message', () => {
    const props = {
      stepId: '1',
      selectedStep: 'selectedStep',
      formValues: {},
      isStuck: true,
      dependencies: { files: [{ status: 0 }] },
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);

    expect(
      wrapper.getByText('txt_step_stuck_move_forward_message')
    ).toBeInTheDocument();
  });
});
