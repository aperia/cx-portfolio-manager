import { render, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React from 'react';
import Summary from './Summary';

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <Summary {...props} />
    </div>
  );
};

jest.mock('./GridTableDetails', () => ({
  __esModule: true,
  default: ({ ...props }: any) => {
    return <div>GridTableDetail</div>;
  }
}));

jest.mock('./GridDecisionView', () => ({
  __esModule: true,
  default: ({ ...props }: any) => {
    return <div>GridDecisionView</div>;
  }
}));

describe('WorkflowManageAccountLevel > ManageAccountLevelCAStep >  Summary', () => {
  it('Should render Summary Config TQ > GridTableDetails ', () => {
    const wrapper = renderComponent({});

    expect(wrapper.getByText('GridTableDetail')).toBeInTheDocument();
  });

  it('Should render Summary Config TQ > GridDecisionView', () => {
    const onEditStep = jest.fn();
    const wrapper = renderComponent({ onEditStep });
    userEvent.click(wrapper.getByText('txt_edit'));

    expect(wrapper.getByText('GridDecisionView')).toBeInTheDocument();

    expect(onEditStep).toBeCalled();
  });
});
