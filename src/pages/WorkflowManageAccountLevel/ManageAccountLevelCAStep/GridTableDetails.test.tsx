import { renderWithMockStore } from 'app/utils';
import 'app/utils/_mockComponent/mockDatePicker';
import 'app/utils/_mockComponent/mockEnhanceMultiSelect';
import * as CommonRedux from 'pages/_commons/redux/Common/select-hooks';
import React from 'react';
import GridTableDetails from './GridTableDetails';

const mockUseSelectWindowDimension = jest.spyOn(
  CommonRedux,
  'useSelectWindowDimension'
);

HTMLCanvasElement.prototype.getContext = jest.fn();

describe('', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Grid Table Details', async () => {
    mockUseSelectWindowDimension.mockReturnValue({ width: 900, height: 1000 });
    const props = {
      isShowFlyout: false,
      data: [
        {
          name: 'allocation.date',
          value: '12/17/2021 5:00:00 PM',
          businessName: 'bussiness',
          greenScreenName: 'name',
          moreInfo: 'moreInfo'
        }
      ]
    };
    const wrapper = await renderWithMockStore(
      <GridTableDetails {...props} />,
      {}
    );

    expect(
      wrapper.getByText('txt_manage_account_level_table_decision_business_name')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_account_level_table_decision_green_name')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_account_level_table_decision_value')
    ).toBeInTheDocument();
    expect(wrapper.getByText('txt_more_info')).toBeInTheDocument();
  });

  it('Grid Table Details with large width', async () => {
    mockUseSelectWindowDimension.mockReturnValue({ width: 2000, height: 1000 });
    const props = {
      isShowFlyout: true,
      data: [
        {
          name: 'allocation.date',
          value: '12/17/2021 5:00:00 PM',
          businessName: 'bussiness',
          greenScreenName: 'name',
          moreInfo: 'moreInfo'
        }
      ]
    };
    const wrapper = await renderWithMockStore(
      <GridTableDetails {...props} />,
      {}
    );

    expect(
      wrapper.getByText('txt_manage_account_level_table_decision_business_name')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_account_level_table_decision_green_name')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_account_level_table_decision_value')
    ).toBeInTheDocument();
    expect(wrapper.getByText('txt_more_info')).toBeInTheDocument();
  });
});
