import { useTranslation } from 'app/_libraries/_dls';
import { Icon, Tooltip } from 'app/_libraries/_dls/components';
import React, { Fragment, useMemo } from 'react';

interface ActionButtonsProps {
  small?: boolean;
  title?: string;
  onClickCreateNewVersion?: () => void;
}

const ActionButtons: React.FC<ActionButtonsProps> = ({
  small = false,
  title = '',
  onClickCreateNewVersion
}) => {
  const { t } = useTranslation();

  const renderButtons = useMemo(() => {
    return (
      <Fragment>
        <h5>{t('txt_manage_account_level_select_an_option')}</h5>
        <div className="row mt-16">
          <div className="col-6 col-xl-4">
            <div
              className="rcc-btn d-flex justify-content-between border rounded-lg py-10"
              onClick={onClickCreateNewVersion}
            >
              <span className="d-flex align-items-center ml-16">
                <Icon
                  name="method-create"
                  size="9x"
                  className="color-grey-l16"
                />
                <span className="ml-12 mr-8">
                  {t('txt_manage_penalty_fee_create_new_version')}
                </span>
                <Tooltip
                  element={t(
                    'txt_manage_account_level_create_new_version_desc'
                  )}
                  triggerClassName="d-flex"
                >
                  <Icon
                    name="information"
                    className="color-grey-l16"
                    size="5x"
                  />
                </Tooltip>
              </span>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }, [t, onClickCreateNewVersion]);

  return <div>{renderButtons}</div>;
};

export default ActionButtons;
