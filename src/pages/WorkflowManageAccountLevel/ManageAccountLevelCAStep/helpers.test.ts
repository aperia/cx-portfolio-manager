import { mapColorBadge, mappingData } from './helper';

describe('test helper', () => {
  it('mappingData', () => {
    mappingData([
      {
        name: 'ATTRITION INDEX',
        searchCode: 'R',
        immediateAllocation: 'Yes',
        value: [
          '378',
          '202',
          '448',
          '127',
          '416',
          '31',
          '192',
          '479',
          '304',
          '171'
        ],
        idDecision: 'ATTRITION INDEX__0'
      },
      {
        name: 'ALT FIN RPT ENTITY',
        searchCode: 'E',
        immediateAllocation: 'Yes',
        value: [
          '270',
          '183',
          '23',
          '467',
          '165',
          '356',
          '489',
          '385',
          '7',
          '433'
        ],
        idDecision: 'ALT FIN RPT ENTITY__1'
      },
      {
        name: 'ANN MRCH INT RATE',
        searchCode: 'R',
        immediateAllocation: 'No',
        value: [
          '194',
          '192',
          '456',
          '453',
          '375',
          '271',
          '385',
          '350',
          '69',
          '9'
        ],
        idDecision: 'ANN MRCH INT RATE__2'
      },
      {
        name: 'ALT FIN RPT OTHER1',
        searchCode: 'R',
        immediateAllocation: 'No',
        value: [
          '438',
          '397',
          '56',
          '411',
          '242',
          '385',
          '40',
          '222',
          '44',
          '470'
        ],
        idDecision: 'ALT FIN RPT OTHER1__3'
      },
      {
        name: 'APPROVAL OFFICER',
        searchCode: 'E',
        immediateAllocation: 'No',
        value: [
          '198',
          '143',
          '234',
          '132',
          '101',
          '450',
          '263',
          '200',
          '363',
          '386'
        ],
        idDecision: 'APPROVAL OFFICER__4'
      },
      {
        name: 'RESULT ID',
        searchCode: '',
        immediateAllocation: '',
        value: [
          '214',
          '159',
          '241',
          '475',
          '269',
          '64',
          '36',
          '228',
          '171',
          '25'
        ],
        idDecision: 'RESULT ID__5'
      },
      {
        name: 'ALT FIN RPT C/NO C',
        searchCode: 'E',
        idDecision: 1649058054686
      }
    ] as any);
  });

  it('mapColorBadge > status production', () => {
    const color = mapColorBadge('production');
    expect(color).toEqual('green');
  });
  it('mapColorBadge > status scheduled', () => {
    const color = mapColorBadge('scheduled');
    expect(color).toEqual('cyan');
  });
  it('mapColorBadge > status working', () => {
    const color = mapColorBadge('working');
    expect(color).toEqual('cyan');
  });

  it('mapColorBadge > status validating', () => {
    const color = mapColorBadge('validating');
    expect(color).toEqual('orange');
  });
  it('mapColorBadge > status previous', () => {
    const color = mapColorBadge('previous');
    expect(color).toEqual('orange');
  });
  it('mapColorBadge > status clientApproved', () => {
    const color = mapColorBadge('client approved');
    expect(color).toEqual('green');
  });
  it('mapColorBadge > status lapsed', () => {
    const color = mapColorBadge('lapsed');
    expect(color).toEqual('red');
  });

  it('mapColorBadge > status default', () => {
    const color = mapColorBadge('');
    expect(color).toEqual('grey');
  });
});
