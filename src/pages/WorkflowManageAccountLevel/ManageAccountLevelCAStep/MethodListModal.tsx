import ModalRegistry from 'app/components/ModalRegistry';
import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import {
  PAGE_SIZE,
  WORKFLOW_TABLE_SORT_BY_MANAGE_ACCOUNT_LEVEL
} from 'app/constants/constants';
import { WorkflowMethodsSortByFields } from 'app/constants/enums';
import { classnames, formatCommon, matchSearchValue } from 'app/helpers';
import {
  Badge,
  Button,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  Radio,
  useTranslation
} from 'app/_libraries/_dls';
import { DEFAULT_PAGE_NUMBER } from 'app/_libraries/_dls/components/Pagination/constants';
import { isArray, isEmpty } from 'lodash';
import orderBy from 'lodash.orderby';
import {
  actionsWorkflowSetup,
  useSelectTableListData,
  useSelectWorkflowMethodsFilterSelector
} from 'pages/_commons/redux/WorkflowSetup';
import { defaultWorkflowMethodsFilter } from 'pages/_commons/redux/WorkflowSetup/reducers';
import Paging from 'pages/_commons/Utils/Paging';
import SortOrder from 'pages/_commons/Utils/SortOrder';
import React, { useEffect, useMemo, useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import ViewModal from './ViewModal';

interface IMethodListModalProps {
  id: string;
  isCreateNewVersion: boolean;
  onClose?: () => void;
  handleAddNewVersion: () => void;
  setDataNewVersion: (data: IAPLPropsType) => void;
  isClearAndReset: boolean;
  idSelectedVal: string;
  resetClearAndReset: (isReset: boolean) => void;
}

export interface IAPLPropsType extends WorkflowSetupMethod {
  tableId: string;
  tableName: string;
  effectiveDate: string;
  status: string;
}

export interface IAPLPropsType extends WorkflowSetupMethod {
  tableId: string;
  tableName: string;
  effectiveDate: string;
  status: string;
}

export const StatusName = {
  production: 'production',
  clientApproved: 'client approved',
  scheduled: 'scheduled',
  working: 'working',
  validating: 'validating',
  previous: 'previous',
  lapsed: 'lapsed'
};

const MethodListModal: React.FC<IMethodListModalProps> = ({
  id,
  isCreateNewVersion,
  onClose,
  handleAddNewVersion,
  setDataNewVersion,
  isClearAndReset,
  idSelectedVal,
  resetClearAndReset
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const searchRef = useRef<any>(null);

  const [page, setPage] = useState<number>(DEFAULT_PAGE_NUMBER);
  const [pageSize, setPageSize] = useState<number>(PAGE_SIZE[0]);
  const [idSelected, setIdSelected] = useState<string>(idSelectedVal);
  const [searchValue, setSearchValue] = useState<string>('');
  const [order, setOrderBy] = useState<'desc' | 'asc'>('desc');
  const [sort, setSortBy] = useState<string>(
    WorkflowMethodsSortByFields.EFFECTIVE_DATE
  );
  const [isShowViewModal, setIsShowViewModal] = useState<boolean>(false);
  const [selectedVersion, setSelectedVersion] = useState<IAPLPropsType>();

  const dataFilter = useSelectWorkflowMethodsFilterSelector();
  const sortByList = WORKFLOW_TABLE_SORT_BY_MANAGE_ACCOUNT_LEVEL;
  const tableList = useSelectTableListData()?.tableList;

  const versions = useMemo(
    () => (isArray(tableList) ? tableList[0]?.versions : []),
    [tableList]
  );
  const tableId = isArray(tableList) ? tableList[0]?.tableId : '';

  const filteredTableList = useMemo(() => {
    return versions.filter((w: MagicKeyValue) =>
      matchSearchValue([w?.tableName], searchValue)
    );
  }, [searchValue, versions]);

  const sortOrderByList = orderBy(filteredTableList, [sort], [order]);

  useEffect(() => {
    !searchValue && searchRef?.current?.clear();
  }, [searchValue]);

  useEffect(() => {
    if (!isClearAndReset) return;
    resetClearAndReset(false);
    setSearchValue('');
    if (!isEmpty(idSelected)) {
      const sortOrderByListTemp = orderBy(versions, [sort], [order]);
      const index = sortOrderByListTemp.findIndex(
        (item: MagicKeyValue) => item?.tableId === idSelected
      );
      const pageTemp = Math.ceil((index + 1) / pageSize);
      setPage(pageTemp);
      return;
    }
    setPage(DEFAULT_PAGE_NUMBER);
  }, [
    idSelected,
    isClearAndReset,
    order,
    pageSize,
    resetClearAndReset,
    sort,
    versions
  ]);

  const handleSearch = (val: string) => {
    setSearchValue(val);
    setPage(DEFAULT_PAGE_NUMBER);
  };

  const mapColorBadge = (status: string) => {
    switch (status.toLocaleLowerCase()) {
      case StatusName.scheduled:
      case StatusName.working:
        return 'cyan';
      case StatusName.validating:
      case StatusName.previous:
        return 'orange';
      case StatusName.clientApproved:
      case StatusName.production:
        return 'green';
      case StatusName.lapsed:
        return 'red';
      default:
        return 'grey';
    }
  };

  const handleChangePage = (page: number) => {
    setPage(page);
  };

  const handleChangePageSize = (pageSize: number) => {
    setPageSize(pageSize);
  };

  const handleSelected = (id: string) => {
    setIdSelected(id);
  };

  const handleClearAndReset = () => {
    setSearchValue('');
    if (!isEmpty(idSelected)) {
      const sortOrderByListTemp = orderBy(versions, [sort], [order]);
      const index = sortOrderByListTemp.findIndex(
        (item: MagicKeyValue) => item?.tableId === idSelected
      );
      const pageTemp = Math.ceil((index + 1) / pageSize);
      setPage(pageTemp);
      return;
    }
    setPage(DEFAULT_PAGE_NUMBER);
  };

  const handleChangeOrderBy = (orderBy: any) => {
    setOrderBy(orderBy);
    dispatch(
      actionsWorkflowSetup.updateWorkflowMethodFilter({
        orderBy: [orderBy as OrderByType]
      })
    );
  };

  const handleChangeSortBy = (sortBy: string) => {
    setSortBy(sortBy);
    dispatch(
      actionsWorkflowSetup.updateWorkflowMethodFilter({
        sortBy: [sortBy]
      })
    );
  };

  const dataTablePage = sortOrderByList?.slice(
    pageSize * (page - 1),
    page * pageSize
  );

  const handleOpenAddNewVersion = () => {
    handleAddNewVersion();
    setIsShowViewModal(false);
  };

  return (
    <>
      <ModalRegistry
        id={id}
        show={isCreateNewVersion}
        lg
        onAutoClosedAll={onClose}
      >
        <ModalHeader border closeButton onHide={onClose}>
          <ModalTitle>{t('txt_create_new_version')}</ModalTitle>
        </ModalHeader>
        <ModalBody
          className={classnames('px-24 pt-24 pb-24 pb-lg-0 overflow-auto')}
        >
          <p>
            To create a new version of the Account Level Processing<sup>SM</sup>{' '}
            Client Allocation table <b>{tableId}</b>, select the version you
            want to model and click <b>Continue</b>.
          </p>
          <div className="mt-24">
            <div className="d-flex justify-content-between align-items-center">
              <h5>{t('txt_manage_account_level_table_versions')}</h5>
              {filteredTableList?.length > 0 && (
                <div className="method-list-simple-search">
                  <SimpleSearch
                    ref={searchRef}
                    clearTooltip={t('txt_clear_search_criteria')}
                    onSearch={handleSearch}
                    placeholder="Type to search by table name"
                  />
                </div>
              )}
            </div>
            {!isEmpty(filteredTableList) && (
              <div className="d-flex justify-content-end align-items-center mt-16">
                <SortOrder
                  dataFilter={dataFilter}
                  defaultDataFilter={defaultWorkflowMethodsFilter}
                  sortByFields={sortByList}
                  onChangeOrderBy={handleChangeOrderBy}
                  onChangeSortBy={handleChangeSortBy}
                  onClearSearch={handleClearAndReset}
                  hasFilter={!!searchValue}
                />
              </div>
            )}

            <div className="mt-16 mx-n12">
              {isEmpty(filteredTableList) && (
                <div className="d-flex flex-column justify-content-center mt-40">
                  <NoDataFound
                    id="newMethod_notfound"
                    hasSearch={!!searchValue}
                    title={t('txt_no_tables_to_display')}
                    linkTitle={!!searchValue && t('txt_clear_and_reset')}
                    onLinkClicked={handleClearAndReset}
                  />
                </div>
              )}
              <div className="mt-12 col-12">
                {filteredTableList?.length > 0 &&
                  dataTablePage.map((version: MagicKeyValue) => {
                    return (
                      <div key={version?.tableId} className="position-relative">
                        <div
                          className="list-view method-card-view py-12 mt-12 workflow-card-view bg-checkbox-hover-light-l20 custom-control-root"
                          onClick={() => {
                            handleSelected(version?.tableId);
                            setSelectedVersion(version as IAPLPropsType);
                            setDataNewVersion(version as IAPLPropsType);
                          }}
                        >
                          <div className="row align-items-center">
                            <Radio
                              className="select-version-radio"
                              onClick={() => {
                                handleSelected(version?.tableId);
                                setSelectedVersion(version as IAPLPropsType);
                                setDataNewVersion(version as IAPLPropsType);
                              }}
                            >
                              <Radio.Input
                                className="checked-style"
                                checked={version?.tableId === idSelected}
                              />
                            </Radio>
                            <div className="col-4 mt-n4" role="dof-field">
                              <div className="form-group-static mt-4 d-flex align-items-center w-100 pl-24 ml-24">
                                <div className="color-grey text-nowrap mr-2">
                                  Table Name:
                                </div>
                                <div className="form-group-static__text flex-fill fw-600">
                                  {version?.tableName}
                                </div>
                              </div>
                            </div>
                            <div className="col-4 mt-n4" role="dof-field">
                              <div className="form-group-static mt-4 d-flex align-items-center w-100">
                                <div className="color-grey text-nowrap mr-2">
                                  Effective Date:
                                </div>
                                <div className="form-group-static__text flex-fill">
                                  {
                                    formatCommon(version?.effectiveDate).time
                                      .date
                                  }
                                </div>
                              </div>
                            </div>
                            <div
                              className="col-3 pl-0 pl-lg-12"
                              role="dof-field"
                            >
                              <div className="form-group-static mt-0 d-flex align-items-center">
                                <Badge color={mapColorBadge(version?.status)}>
                                  <span>{version?.status}</span>
                                </Badge>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="method-card-view-btn" role="dof-field">
                          <Button
                            size="sm"
                            variant="outline-primary"
                            onClick={e => {
                              e.stopPropagation();
                              setIsShowViewModal(true);
                              setSelectedVersion(version as IAPLPropsType);
                              setDataNewVersion(version as IAPLPropsType);
                            }}
                          >
                            {t('txt_view')}
                          </Button>
                        </div>
                      </div>
                    );
                  })}
              </div>
            </div>
            {filteredTableList?.length > 0 && (
              <div className="mt-16">
                <Paging
                  page={page}
                  pageSize={pageSize}
                  totalItem={filteredTableList?.length}
                  onChangePage={handleChangePage}
                  onChangePageSize={handleChangePageSize}
                />
              </div>
            )}
          </div>
        </ModalBody>

        <ModalFooter
          cancelButtonText={t('txt_cancel')}
          okButtonText={t('txt_continue')}
          onCancel={onClose}
          onOk={handleAddNewVersion}
          disabledOk={isEmpty(idSelected)}
        />
      </ModalRegistry>

      {isShowViewModal && (
        <ViewModal
          tableId={tableId}
          data={selectedVersion}
          onCancel={() => setIsShowViewModal(false)}
          onOK={handleOpenAddNewVersion}
        />
      )}
    </>
  );
};

export default MethodListModal;
