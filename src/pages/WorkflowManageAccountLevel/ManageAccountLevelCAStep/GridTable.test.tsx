import { fireEvent } from '@testing-library/react';
import { renderWithMockStore } from 'app/utils';
import 'app/utils/_mockComponent/mockDatePicker';
import 'app/utils/_mockComponent/mockEnhanceMultiSelect';
import * as CommonRedux from 'pages/_commons/redux/Common/select-hooks';
import React from 'react';
import GridTable from './GridTable';

const mockUseSelectWindowDimension = jest.spyOn(
  CommonRedux,
  'useSelectWindowDimension'
);

HTMLCanvasElement.prototype.getContext = jest.fn();
jest.mock(
  'app/components/EnhanceDatePicker',
  () =>
    ({ value, onChange }: { value: Date; onChange: () => void }) => {
      return (
        <input
          data-testid="DatePicker_onChange"
          value={value.toISOString()}
          onChange={onChange}
        />
      );
    }
);
describe('', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Grid table', async () => {
    mockUseSelectWindowDimension.mockReturnValue({ width: 900, height: 1000 });
    const props = {
      handleChangeControlParameters: jest.fn(),
      data: [
        {
          name: 'allocation.date',
          value: '12/17/2021 5:00:00 PM',
          businessName: 'bussiness',
          greenScreenName: 'name',
          moreInfo: 'moreInfo'
        }
      ]
    };
    const wrapper = await renderWithMockStore(<GridTable {...props} />, {});

    expect(
      wrapper.getByText('txt_manage_account_level_table_decision_business_name')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_account_level_table_decision_green_name')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_account_level_table_decision_value')
    ).toBeInTheDocument();
    expect(wrapper.getByText('txt_more_info')).toBeInTheDocument();

    const inputElement = wrapper.getByTestId('DatePicker_onChange');
    fireEvent.change(inputElement, {
      target: {
        value: 'undefined',
        name: 'dateValue',
        date: new Date('02/03/2020')
      }
    });
    fireEvent.blur(inputElement!);
  });

  it('Grid table with large width', async () => {
    mockUseSelectWindowDimension.mockReturnValue({ width: 2000, height: 1000 });
    const props = {
      handleChangeControlParameters: jest.fn(),
      data: [
        {
          name: 'allocation.date',
          value: '12/17/2021 5:00:00 PM',
          businessName: 'bussiness',
          greenScreenName: 'name',
          moreInfo: 'moreInfo'
        }
      ]
    };
    const wrapper = await renderWithMockStore(<GridTable {...props} />, {});

    expect(
      wrapper.getByText('txt_manage_account_level_table_decision_business_name')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_account_level_table_decision_green_name')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_account_level_table_decision_value')
    ).toBeInTheDocument();
    expect(wrapper.getByText('txt_more_info')).toBeInTheDocument();

    const inputElement = wrapper.getByTestId('DatePicker_onChange');
    fireEvent.change(inputElement, {
      target: {
        value: 'undefined',
        name: 'dateValue',
        date: new Date('02/03/2020')
      }
    });
    fireEvent.blur(inputElement!);
  });
});
