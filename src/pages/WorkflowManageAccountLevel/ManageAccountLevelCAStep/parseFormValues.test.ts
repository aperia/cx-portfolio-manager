import { ServiceSubjectSection } from 'app/constants/enums';
import parseFormValues from './parseFormValues';

describe('ManageAccountLevelWorkflow > CAStep > parseFormValues', () => {
  it('should return undefined with methodList is undefined', () => {
    const response = parseFormValues(
      { data: { workflowSetupData: [{ id: '1' }] } } as any,
      '1'
    );
    expect(response).toEqual(
      expect.objectContaining({
        isPass: false,
        isSelected: false,
        isValid: false
      })
    );
  });

  it('should return undefined with stepInfo is undefined', () => {
    const response = parseFormValues(
      {
        data: {
          workflowSetupData: [
            {
              id: '2'
            }
          ],
          configurations: {
            methodList: []
          }
        }
      } as any,
      '1'
    );
    expect(response).toEqual(undefined);
  });

  it('should return valid data', () => {
    const response = parseFormValues(
      {
        data: {
          workflowSetupData: [
            {
              id: '1'
            }
          ],
          configurations: {
            tableList: [
              {
                id: '123',
                name: '123',
                serviceSubjectSection: ServiceSubjectSection.LC
              }
            ]
          }
        }
      } as any,
      '1'
    );
    expect(response).toEqual(
      expect.objectContaining({
        isPass: false,
        isSelected: false,
        isValid: false
      })
    );
  });
});
