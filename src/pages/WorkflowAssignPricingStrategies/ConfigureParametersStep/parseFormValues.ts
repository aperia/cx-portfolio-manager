import { getWorkflowSetupStepStatus } from 'app/helpers';
import { isUndefined } from 'lodash';
import findParameter from './findParameter';
import { ParameterCode } from './parameterCodes';

const parseFormValues: WorkflowSetupStepFormDataFunc<any> = (data, id) => {
  const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);

  const parameters = (data?.data as any)?.configurations || {};
  if (!stepInfo) return;

  const getParameterValue = (name: ParameterCode) =>
    findParameter(parameters.parameters, name)?.value;

  const action = parameters?.transactionId;
  const mn168Action = getParameterValue(ParameterCode.Entry_168);
  const mn738Action = getParameterValue(ParameterCode.Entry_738);

  let isValid = !isUndefined(action);

  if (action === 'nm.168' && mn168Action === 'CA') {
    isValid =
      findParameter(parameters.parameters, ParameterCode.PortfolioId)?.value !==
      undefined;
  }

  if (action === 'nm.169' || action === 'nm.170') {
    isValid = true;
  }

  if (action === 'nm.738' && mn738Action == '01-31') {
    isValid =
      findParameter(parameters.parameters, ParameterCode.SubtransactionCode)
        ?.value !== undefined &&
      findParameter(parameters.parameters, ParameterCode.MethodOverrideId)
        ?.value !== undefined;
  }

  if (action === 'nm.738' && mn738Action == '60') {
    isValid =
      findParameter(parameters.parameters, ParameterCode.SVC_SubjectSection)
        ?.value !== undefined;
  }

  return {
    ...getWorkflowSetupStepStatus(stepInfo),
    ...stepInfo.data,
    parameters,
    isValid: isValid
  };
};

export default parseFormValues;
