import EnhanceDropdownList from 'app/components/EnhanceDropdownList';
import FieldTooltip from 'app/components/FieldTooltip';
import {
  ComboBox,
  DropdownBaseChangeEvent,
  useTranslation
} from 'app/_libraries/_dls';
import React from 'react';
import { ConfigureParametersStepProps } from '.';
import findParameter from './findParameter';
import { ParameterCode } from './parameterCodes';

interface NM168Props {
  value: {
    strategy: RefData[];
    status: RefData[];
    type: RefData[];
  };
  formValue?: ConfigureParametersStepProps;
  onchange?: (event: DropdownBaseChangeEvent) => void;
}
const NM168_CP: React.FC<NM168Props> = ({
  value: { strategy, status, type },
  formValue,
  onchange
}) => {
  const { t } = useTranslation();

  const getParameterValue = (name: ParameterCode) =>
    findParameter(formValue?.parameters?.parameters, name)?.value;

  const getDropdownValue = (name: ParameterCode, items?: RefData[]) =>
    items?.find(i => i.code === getParameterValue(name));

  return (
    <div className="border-top mt-24 pt-24">
      <h5>{t('txt_assign_pricing_strategies_configuration_nm168_CP_title')}</h5>
      <div className="row">
        <div className="col-lg-4 col-md-6 mt-16">
          <FieldTooltip
            keepShowOnDevice={false}
            placement="right"
            element={
              <div className="my-n8">
                <div className="text-uppercase">{t('txt_strategy')}</div>
                <p>
                  {t(
                    'txt_assign_pricing_strategies_configuration_nm168_CP_strategy_tooltip_desc'
                  )}
                </p>
              </div>
            }
          >
            <ComboBox
              value={getDropdownValue(ParameterCode.StrategyId, strategy)}
              onChange={onchange}
              label={t('txt_strategy')}
              name={ParameterCode.StrategyId}
              textField="text"
            >
              {strategy?.map(item => (
                <ComboBox.Item key={item.code} value={item} label={item.text} />
              ))}
            </ComboBox>
          </FieldTooltip>
        </div>
        <div className="col-lg-4 col-md-6 mt-16">
          <FieldTooltip
            keepShowOnDevice={false}
            placement="left"
            element={
              <div>
                <div className="text-uppercase">{t('txt_status')}</div>
                <p className="mt-8">
                  {t(
                    'txt_assign_pricing_strategies_configuration_nm168_CP_status_tooltip_desc'
                  )}
                </p>
              </div>
            }
          >
            <EnhanceDropdownList
              value={getDropdownValue(ParameterCode.Status, status)}
              onChange={onchange}
              label={t('txt_status')}
              name={ParameterCode.Status}
              options={status}
            />
          </FieldTooltip>
        </div>
        <div className="col-lg-4 col-md-6 mt-16">
          <FieldTooltip
            keepShowOnDevice={false}
            placement="left"
            element={
              <div>
                <div className="text-uppercase">{t('txt_type')}</div>
                <p className="mt-8">
                  {t(
                    'txt_assign_pricing_strategies_configuration_nm168_CP_type_tooltip_desc'
                  )}
                </p>
              </div>
            }
          >
            <EnhanceDropdownList
              value={getDropdownValue(ParameterCode.Type, type)}
              onChange={onchange}
              label={t('txt_type')}
              name={ParameterCode.Type}
              options={type}
            />
          </FieldTooltip>
        </div>
      </div>
    </div>
  );
};

export default NM168_CP;
