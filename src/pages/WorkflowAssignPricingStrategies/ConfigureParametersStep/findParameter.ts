import { Parameter } from '.';
import { ParameterCode } from './parameterCodes';

const findParameter = (
  parameters: Parameter[] | undefined,
  name: ParameterCode
) => parameters?.find(i => i.name === name);

export default findParameter;
