import { fireEvent, screen } from '@testing-library/dom';
import { act, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { renderWithMockStore } from 'app/utils';
import { DatePickerChangeEvent } from 'app/_libraries/_dls';
import React from 'react';
import * as ReactRedux from 'react-redux';
import { default as NM } from './index';
import { ParameterCode } from './parameterCodes';

jest.mock('app/_libraries/_dls/components/ComboBox', () => {
  const actualModule = jest.requireActual(
    'app/_libraries/_dls/components/ComboBox'
  );
  const mockModule = jest.requireActual(
    'app/utils/_mockComponent/mockComboBox'
  );
  return {
    ...actualModule,
    ...mockModule,
    __esModule: true
  };
});

const mockDateTooltipParams = jest.fn();
jest.mock('app/_libraries/_dls/components/DatePicker', () => {
  const actualModule = jest.requireActual(
    'app/_libraries/_dls/components/DatePicker'
  );
  return {
    ...actualModule,
    __esModule: true,
    default: ({ onChange, dateTooltip, label, onBlur, ...props }: any) => (
      <div>
        <span>{label}</span>
        <input
          {...props}
          onClick={() => {
            const params = mockDateTooltipParams();

            !!dateTooltip && dateTooltip(...params);
          }}
          onChange={(e: any) => {
            onChange!({
              target: { value: new Date(e.target.value), name: e.target.name }
            } as DatePickerChangeEvent);
          }}
          onBlur={onBlur}
        />
      </div>
    )
  };
});

const mockUseDispatch = jest.spyOn(ReactRedux, 'useDispatch');
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});
jest.mock('app/hooks/useUnsavedChangeRegistry', () => {
  return {
    __esModule: true,
    useUnsavedChangeRegistry: () => {}
  };
});

jest.mock('pages/_commons/OverviewModal', () => ({
  __esModule: true,
  default: ({ onClose }: any) => {
    return (
      <div>
        <div>OverviewModal</div>
        <button onClick={onClose}>OverviewButtonCloseModal</button>
      </div>
    );
  }
}));

jest.mock('pages/_commons/ContentExpand', () => ({
  __esModule: true,
  default: ({ instruction, children }: any) => (
    <div>
      <div>ContentExpand</div>
      <div>{instruction}</div>
      <div>{children}</div>
    </div>
  )
}));

const then_change_combobox = (value: RefData) => {
  const inputElement = screen.getByTestId('ComboBox.Input_onChange');

  fireEvent.change(inputElement, {
    target: {
      value: value
    }
  });
};

const then_change_date_picker = (
  wrapper: RenderResult,
  text: string,
  value: string
) => {
  const datepicker = wrapper
    .getByText(text)
    .parentElement?.querySelector('input');
  userEvent.click(datepicker!);
  userEvent.clear(datepicker!);
  act(() => {
    userEvent.type(datepicker!, value);
  });

  fireEvent.blur(datepicker!);
};

// fake redux data

const elementMetaData = [
  {
    id: '29',
    workflowTemplateId: '964929713',
    name: 'transaction.id',
    options: [
      {
        value: 'nm.168',
        description: 'Reallocate Strategy'
      },
      {
        value: 'nm.169',
        description: 'Lock Strategy'
      },
      {
        value: 'nm.170',
        description: 'Unlock Strategy'
      },
      {
        value: 'nm.738',
        description: 'Override Strategy Using Method Level Override'
      }
    ]
  },
  {
    id: '30',
    workflowTemplateId: '964929713',
    name: 'nm.168.entry.format',
    options: [
      {
        value: 'CA',
        description: 'Cardholder account qualification table (CA)'
      },
      {
        value: 'CP',
        description: 'Cardholder pricing (CP)'
      }
    ]
  },
  {
    id: '31',
    workflowTemplateId: '964929713',
    name: 'portfolio.id',
    options: [
      {
        value: '1234',
        description: ''
      },
      {
        value: '9864',
        description: ''
      },
      { value: '7658', description: '' }
    ]
  },
  {
    id: '32',
    workflowTemplateId: '964929713',
    name: 'strategy.id',
    options: [
      {
        value: '1234',
        description: ''
      },
      {
        value: '9864',
        description: ''
      },
      { value: '7658', description: '' }
    ]
  },
  {
    id: '33',
    workflowTemplateId: '964929713',
    name: 'status',
    options: [
      {
        value: 'L',
        description: 'Yes, lock the strategy onto the account.'
      },
      {
        value: 'Blank',
        description:
          'No, do not lock the strategy onto the account; the system assumes this is a U.'
      }
    ]
  },
  {
    id: '34',
    workflowTemplateId: '964929713',
    name: 'type',
    options: [
      {
        value: '0',
        description:
          'Process cash advance and merchandise principals using the new Product Control Fil'
      },
      {
        value: '1',
        description:
          'Cash advance balance. Process existing cash advance principals using the current'
      },
      {
        value: '2',
        description:
          'Merchandise balance. Process existing merchandise principals using the current'
      },
      {
        value: '3',
        description:
          'Cash advance and merchandise balances. Process existing merchandise and cash'
      }
    ]
  },
  {
    id: '35',
    workflowTemplateId: '964929713',
    name: 'nm.738.entry.format',
    options: [
      {
        value: '01-31',
        description: 'Subtransactions 01-31'
      },
      {
        value: '60',
        description: 'Subtransaction 60'
      }
    ]
  },
  {
    id: '36',
    workflowTemplateId: '964929713',
    name: 'subtransaction.code',
    options: [
      {
        value: '01',
        description: 'Break point interest (CP IC BP)'
      },
      {
        value: '02',
        description: 'Interest rate (CP IC ID)'
      },
      {
        value: '03',
        description: 'Interest method (CP IC IM)'
      },
      {
        value: '04',
        description: 'Incentive rate (CP IC IP)'
      },
      {
        value: '05',
        description: 'State Controls (CP IC SC)'
      },
      {
        value: '06',
        description: 'Annual charge (CP IO AC)'
      },
      {
        value: '07',
        description: 'Cash item (CP IO CI)'
      },
      {
        value: '08',
        description: 'Late fee (CP PF LC)'
      },
      {
        value: '09',
        description: 'Overlimit fee (CP PF OC)'
      },
      {
        value: '10',
        description: 'Return fee (CP PF RC)'
      },
      {
        value: '11',
        description: 'Minimum finance (CP IC MF)'
      },
      {
        value: '12',
        description: 'Payoff exception (CP IC PE)'
      },
      {
        value: '13',
        description: 'Credit application (CP PO CA)'
      },
      {
        value: '14',
        description: 'Minimum payment due (CP PO MP)'
      },
      {
        value: '15',
        description: 'Interest on interest (CP IC II)'
      },
      {
        value: '16',
        description: 'Securitization (CP IC SZ)'
      },
      {
        value: '17',
        description: 'Incentive pricing break point (CP IC IB)'
      },
      {
        value: '18',
        description: 'Incentive pricing variable (CP IC IV)'
      },
      {
        value: '19',
        description: 'Statement production (CP IC SP)'
      },
      {
        value: '20',
        description: 'Variable interest (CP IC VI)'
      },
      {
        value: '21',
        description: 'Merchandise item charges (CP IO MI)'
      },
      {
        value: '22',
        description: 'Miscellaneous charges (CP IO MC)'
      },
      {
        value: '23',
        description: 'Statement charges (CP IO SC)'
      },
      {
        value: '24',
        description: 'Debit ratification (CP OC DR)'
      },
      {
        value: '25',
        description: 'Declined authorization fee (CP PF DA)'
      },
      {
        value: '26',
        description: 'Skip payment (CP PO SP)'
      },
      {
        value: '27',
        description: 'Statement design (CP OC SD)'
      },
      {
        value: '28',
        description: 'MULTRAN processing (CP OC MP)'
      },
      {
        value: '29',
        description: 'Rules Minimum Payment (CP PO RM)'
      },
      {
        value: '30',
        description: 'Method level (CP IC ML)'
      },
      {
        value: '31',
        description: 'Max Cap EAPR (CP IC ME)'
      }
    ]
  },
  {
    id: '37',
    workflowTemplateId: '964929713',
    name: 'method.override.id',
    options: [
      {
        value: 'LSTDIFF',
        description:
          'Replace the current method override on this account with the last different method override.'
      },
      {
        value: 'REMOVE',
        description: 'Remove the current method override from this account.'
      },
      {
        value: 'Blank',
        description: 'Remove the current method override from this account.'
      }
    ]
  },
  {
    id: '38',
    workflowTemplateId: '964929713',
    name: 'lock.unlock',
    options: [
      {
        value: 'L',
        description: 'Lock the current method override for this account.'
      },
      {
        value: 'U',
        description: 'Unlock the current method override for this account.'
      }
    ]
  },
  {
    id: '39',
    workflowTemplateId: '964929713',
    name: 'allocate.immediately',
    options: [
      {
        value: 'X',
        description:
          'Use the account qualification and client allocation tables to review and, if applicable, allocate the account during nightly processing.'
      },
      {
        value: 'Blank',
        description: 'No immediate allocation.'
      }
    ]
  },
  {
    id: '40',
    workflowTemplateId: '964929713',
    name: 'method.override.svc.subject.section',
    options: [
      {
        value: 'CPICBP',
        description:
          'Method ID for the method override for the Break Points section (CP IC BP) of the Product Control File.'
      },
      {
        value: 'CPICIB',
        description:
          'Method ID for the method override for the Incentive Pricing Break Points section (CP IC IB) of the Product Control File.'
      },
      {
        value: 'CPICID',
        description:
          'Method ID for the method override for the Interest Defaults section (CP IC ID) of the Product Control File.'
      }
    ]
  },
  {
    id: '41',
    workflowTemplateId: '964929713',
    name: 'lock',
    options: [
      {
        value: 'L',
        description: 'Lock the current method override on this account.'
      },
      {
        value: 'U',
        description:
          'The current method override will be locked according to the dates set in the LOCK DATE and UNLOCK fields.'
      }
    ]
  },
  {
    id: '42',
    workflowTemplateId: '964929713',
    name: 'reallocate',
    options: [
      {
        value: 'I',
        description:
          'Review and, if applicable, reallocate the account on the date set in the UNLOCK field.'
      },
      {
        value: 'Blank',
        description:
          'Do not reallocate the account based on the date set in the UNLOCK field.'
      }
    ]
  },
  {
    id: '43',
    workflowTemplateId: '964929713',
    name: 'aq.table',
    options: [
      {
        value: 'AQTABLE1',
        description: ''
      },
      {
        value: 'AQTABLE2',
        description: ''
      },
      {
        value: 'AQTABLE3',
        description: ''
      }
    ]
  }
];

const renderComponent = (props: any) => {
  return renderWithMockStore(<NM {...props} />, {
    initialState: {
      workflowSetup: {
        elementMetadata: {
          elements: elementMetaData as any
        }
      }
    } as AppState
  });
};

const setFormValuesMock = jest.fn();

describe('AssignPricingStrategiesWorkFlow > GettingStartStep > Index', () => {
  beforeEach(() => {
    mockUseDispatch.mockReturnValue(jest.fn());
    mockDateTooltipParams.mockReturnValue([new Date('01/01/2021')]);
  });
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Render Component with initial formValues NM168', async () => {
    const props = {
      formValues: { parameters: { transactionId: 'nm.168' } },
      setFormValues: setFormValuesMock
    } as any;

    const wrapper = await renderComponent(props);
    expect(wrapper.getByText(/Reallocate Strategy/)).toBeInTheDocument();
    userEvent.click(wrapper.getByText(/Reallocate Strategy/));
    userEvent.click(
      wrapper.getByText('Cardholder account qualification table (CA)')
    );
    expect(setFormValuesMock).toBeCalled();
  });

  it('Render Component with initial formValues NM169', async () => {
    const props = {
      formValues: { parameters: { transactionId: 'nm.169' } },
      setFormValues: setFormValuesMock
    } as any;

    const wrapper = await renderComponent(props);
    fireEvent.click(
      wrapper.getByText(
        'txt_assign_pricing_strategies_configuration_nm169_reallocate_label'
      )
    );
    expect(setFormValuesMock).toBeCalled();
  });

  it('Render Component with initial formValues NM738', async () => {
    const props = {
      formValues: { parameters: { transactionId: 'nm.738' } },
      setFormValues: setFormValuesMock
    } as any;

    const wrapper = await renderComponent(props);
    userEvent.click(wrapper.getByText('Subtransactions 01-31'));
    expect(setFormValuesMock).toBeCalled();
  });

  it('Render NM168 CA', async () => {
    const props = {
      formValues: {
        parameters: {
          transactionId: 'nm.168',
          parameters: [
            { name: ParameterCode.Entry_168, value: 'CA' },
            { name: ParameterCode.PortfolioId, value: '1234' }
          ]
        }
      },
      setFormValues: setFormValuesMock
    } as any;

    const wrapper = await renderComponent(props);
    expect(
      wrapper.getByText(
        'txt_assign_pricing_strategies_configuration_select_action'
      )
    ).toBeInTheDocument();

    expect(wrapper.getByText(/Reallocate Strategy/)).toBeInTheDocument();
    fireEvent.click(wrapper.getByText(/Reallocate Strategy/));
    expect(setFormValuesMock).toBeCalled();
    fireEvent.click(
      wrapper.getByText('Cardholder account qualification table (CA)')
    );
    expect(setFormValuesMock).toBeCalled();
    expect(wrapper.getByText('txt_pofolio_id')).toBeInTheDocument();

    then_change_combobox({ code: '001', text: 'John cena' });
    expect(setFormValuesMock).toBeCalled();
  });

  it('Render NM168 CP', async () => {
    const props = {
      formValues: {
        parameters: {
          transactionId: 'nm.168',
          parameters: [
            { name: ParameterCode.Entry_168, value: 'CP' },
            { name: ParameterCode.Status, value: 'Blank' },
            { name: ParameterCode.StrategyId, value: '1234' },
            { name: ParameterCode.Type, value: '2' }
          ]
        }
      },
      setFormValues: setFormValuesMock
    } as any;

    const wrapper = await renderComponent(props);
    expect(wrapper.getByText(/Reallocate Strategy/)).toBeInTheDocument();
    fireEvent.click(wrapper.getByText(/Reallocate Strategy/));
    expect(setFormValuesMock).toBeCalled();
    fireEvent.click(wrapper.getByText('Cardholder pricing (CP)'));
    expect(setFormValuesMock).toBeCalled();
  });

  it('Render NM169 has NO invalide date tooltip', async () => {
    mockUseDispatch.mockReturnValue(
      jest.fn().mockReturnValue({
        payload: {
          data: {
            maxDate: '2021/02/04',
            minDate: '2021/02/01',
            invalidDates: [{ date: '2021/02/03', reason: 'no reason' }]
          }
        }
      })
    );
    const props = {
      formValues: {
        parameters: {
          transactionId: 'nm.169',
          parameters: [
            { name: ParameterCode.EndDate, value: new Date() },
            { name: ParameterCode.StartDate, value: undefined },
            { name: ParameterCode.Reallocate, value: true }
          ]
        }
      },
      setFormValues: setFormValuesMock
    } as any;

    const wrapper = await renderComponent(props);
    expect(wrapper.getByText(/Lock Strategy/)).toBeInTheDocument();
    fireEvent.click(wrapper.getByText(/Lock Strategy/));
    expect(setFormValuesMock).toBeCalled();
    expect(
      wrapper.getAllByText(
        'txt_assign_pricing_strategies_configuration_nm169_entry_title'
      )
    ).toHaveLength(1);

    expect(
      wrapper.getByText(
        'txt_assign_pricing_strategies_configuration_nm169_reallocate_label'
      )
    ).toBeInTheDocument();
    then_change_date_picker(
      wrapper,
      'txt_start_date',
      new Date().toDateString()
    );
    expect(setFormValuesMock).toBeCalled();
    mockDateTooltipParams.mockReturnValue([new Date('02/01/2021'), 'year']);
    then_change_date_picker(wrapper, 'txt_end_date', new Date().toDateString());
    expect(setFormValuesMock).toBeCalled();
    fireEvent.click(
      wrapper.getByText(
        'txt_assign_pricing_strategies_configuration_nm169_reallocate_label'
      )
    );
    expect(setFormValuesMock).toBeCalled();
  });

  it('Render NM169 has invalide date tooltip', async () => {
    mockUseDispatch.mockReturnValue(
      jest.fn().mockReturnValue({
        payload: {
          data: {
            maxDate: '2021/02/04',
            minDate: '2021/02/01',
            invalidDates: [{ date: '2021/02/03', reason: 'no reason' }]
          }
        }
      })
    );
    mockDateTooltipParams.mockReturnValue([new Date('2021/02/03')]);
    const props = {
      formValues: {
        parameters: {
          transactionId: 'nm.169',
          parameters: [
            { name: ParameterCode.EndDate, value: new Date() },
            { name: ParameterCode.StartDate, value: undefined },
            { name: ParameterCode.Reallocate, value: true }
          ]
        }
      },
      setFormValues: setFormValuesMock
    } as any;

    const wrapper = await renderComponent(props);
    expect(wrapper.getByText(/Lock Strategy/)).toBeInTheDocument();
    fireEvent.click(wrapper.getByText(/Lock Strategy/));
    expect(setFormValuesMock).toBeCalled();
    expect(
      wrapper.getAllByText(
        'txt_assign_pricing_strategies_configuration_nm169_entry_title'
      )
    ).toHaveLength(1);

    expect(
      wrapper.getByText(
        'txt_assign_pricing_strategies_configuration_nm169_reallocate_label'
      )
    ).toBeInTheDocument();
    then_change_date_picker(
      wrapper,
      'txt_start_date',
      new Date().toDateString()
    );
    expect(setFormValuesMock).toBeCalled();
    mockDateTooltipParams.mockReturnValue([new Date('02/01/2021'), 'year']);
    then_change_date_picker(wrapper, 'txt_end_date', new Date().toDateString());
    expect(setFormValuesMock).toBeCalled();
    fireEvent.click(
      wrapper.getByText(
        'txt_assign_pricing_strategies_configuration_nm169_reallocate_label'
      )
    );
    expect(setFormValuesMock).toBeCalled();
  });

  it('Render NM170', async () => {
    const props = {
      formValues: {
        isValid: true,
        parameters: {
          transactionId: 'nm.170',
          parameters: [
            { name: ParameterCode.EndDate, value: new Date() },
            { name: ParameterCode.Reallocate, value: true }
          ]
        }
      },
      setFormValues: setFormValuesMock,
      hasInstance: true
    } as any;

    const wrapper = await renderComponent(props);
    expect(wrapper.getByText(/Unlock Strategy/)).toBeInTheDocument();
    fireEvent.click(wrapper.getByText(/Unlock Strategy/));
    expect(setFormValuesMock).toBeCalled();
    expect(
      wrapper.getAllByText(
        'txt_assign_pricing_strategies_configuration_nm170_entry_title'
      )
    ).toHaveLength(1);
    then_change_date_picker(wrapper, 'txt_end_date', new Date().toDateString());
    expect(setFormValuesMock).toBeCalled();
  });

  it('Render NM738 - Subtransaction 01-31', async () => {
    const props = {
      formValues: {
        parameters: {
          transactionId: 'nm.738',
          parameters: [
            { name: ParameterCode.Entry_738, value: '01-31' },
            { name: ParameterCode.SubtransactionCode, value: '08' },
            { name: ParameterCode.MethodOverrideId, value: 'Blank' }
          ]
        }
      },
      setFormValues: setFormValuesMock
    } as any;

    const wrapper = await renderComponent(props);
    expect(
      wrapper.getByText(/Override Strategy Using Method Level Override/)
    ).toBeInTheDocument();
    fireEvent.click(
      wrapper.getByText(/Override Strategy Using Method Level Override/)
    );
    expect(setFormValuesMock).toBeCalled();
    expect(wrapper.getAllByText(/Subtransactions 01-31/)).toHaveLength(1);
  });

  it('Render NM738 - Subtransaction 60', async () => {
    const props = {
      formValues: {
        parameters: {
          transactionId: 'nm.738',
          parameters: [{ name: ParameterCode.Entry_738, value: '60' }]
        }
      },
      setFormValues: setFormValuesMock
    } as any;

    const wrapper = await renderComponent(props);
    expect(
      wrapper.getByText(/Override Strategy Using Method Level Override/)
    ).toBeInTheDocument();
    userEvent.click(
      wrapper.getByText(/Override Strategy Using Method Level Override/)
    );
    expect(setFormValuesMock).toBeCalled();
    fireEvent.click(wrapper.getByText('Subtransaction 60'));
    expect(setFormValuesMock).toBeCalled();
    expect(wrapper.getAllByText(/Subtransaction 60/)).toHaveLength(1);

    const reasonInputParent =
      wrapper.getByText('txt_reason').parentElement?.parentElement;
    const reasonInput = reasonInputParent?.querySelector('input');
    userEvent.type(reasonInput!, 'reason');
    expect(setFormValuesMock).toBeCalled();
  });
});
