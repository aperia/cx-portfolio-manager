import { ParameterCode } from './parameterCodes';
import parseFormValues from './parseFormValues';

describe('AssignPricingStrategiesWorkFlow > ConfigureParametersStep > parseFormValues', () => {
  it('Should return undefined', async () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '2'
            }
          ] as any
        }
      } as WorkflowSetupInstance,
      id: '1'
    };

    const dispatch = jest.fn();
    const response = await parseFormValues(props.data, props.id, dispatch);
    expect(response).toEqual(undefined);
  });

  it('Should return data > action value: CA', async () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '1'
            }
          ] as any,
          configurations: {
            transactionId: 'nm.168',
            parameters: [
              { name: ParameterCode.Entry_168, value: 'CA' },
              { name: ParameterCode.PortfolioId, value: '123' }
            ]
          } as any
        }
      } as any,
      id: '1'
    };

    const dispatch = jest.fn();
    const response = await parseFormValues(props.data, props.id, dispatch);
    expect(response)?.toEqual({
      parameters: {
        transactionId: 'nm.168',
        parameters: [
          { name: ParameterCode.Entry_168, value: 'CA' },
          { name: ParameterCode.PortfolioId, value: '123' }
        ]
      },
      isPass: false,
      isSelected: false,
      isValid: true
    });
  });

  it('Should return data >  value: NM169', async () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '1'
            }
          ] as any,
          configurations: {
            transactionId: 'nm.169'
          } as any
        }
      } as any,
      id: '1'
    };

    const dispatch = jest.fn();
    const response = await parseFormValues(props.data, props.id, dispatch);
    expect(response)?.toEqual({
      parameters: {
        transactionId: 'nm.169'
      },
      isPass: false,
      isSelected: false,
      isValid: true
    });
  });

  it('Should return data > action value: 01-31', async () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '1'
            }
          ] as any,
          configurations: {
            transactionId: 'nm.738',
            parameters: [
              { name: ParameterCode.Entry_738, value: '01-31' },
              { name: ParameterCode.SubtransactionCode, value: '123' },
              { name: ParameterCode.MethodOverrideId, value: '123' }
            ]
          } as any
        }
      } as any,
      id: '1'
    };

    const dispatch = jest.fn();
    const response = await parseFormValues(props.data, props.id, dispatch);
    expect(response)?.toEqual({
      parameters: {
        transactionId: 'nm.738',
        parameters: [
          { name: ParameterCode.Entry_738, value: '01-31' },
          { name: ParameterCode.SubtransactionCode, value: '123' },
          { name: ParameterCode.MethodOverrideId, value: '123' }
        ]
      },
      isPass: false,
      isSelected: false,
      isValid: true
    });
  });

  it('Should return data > action value: 60', async () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '1'
            }
          ] as any,
          configurations: {
            transactionId: 'nm.738',
            parameters: [
              { name: ParameterCode.Entry_738, value: '60' },
              { name: ParameterCode.SVC_SubjectSection, value: '123' }
            ]
          } as any
        }
      } as any,
      id: '1'
    };

    const dispatch = jest.fn();
    const response = await parseFormValues(props.data, props.id, dispatch);
    expect(response)?.toEqual({
      parameters: {
        transactionId: 'nm.738',
        parameters: [
          { name: ParameterCode.Entry_738, value: '60' },
          { name: ParameterCode.SVC_SubjectSection, value: '123' }
        ]
      },
      isPass: false,
      isSelected: false,
      isValid: true
    });
  });
});
