import EnhanceDropdownList from 'app/components/EnhanceDropdownList';
import FieldTooltip from 'app/components/FieldTooltip';
import { useFormValidations } from 'app/hooks';
import {
  ComboBox,
  DropdownBaseChangeEvent,
  TextBox,
  useTranslation
} from 'app/_libraries/_dls';
import React, { useCallback, useMemo } from 'react';
import { ConfigureParametersStepProps, IFieldValidate } from '.';
import findParameter from './findParameter';
import { ParameterCode } from './parameterCodes';

interface NM738Props {
  value: {
    subtransactionCode: RefData[];
    methodOverrideID: RefData[];
    lockUnlock: RefData[];
    allocateImmediately: RefData[];
    aQTable: RefData[];
  };
  formValue?: ConfigureParametersStepProps;
  onchange?: (event: DropdownBaseChangeEvent) => void;
  onchangeTextBox?: (event: React.ChangeEvent<HTMLInputElement>) => void;
}
const NM738_0131: React.FC<NM738Props> = ({
  value: {
    subtransactionCode,
    methodOverrideID,
    lockUnlock,
    allocateImmediately,
    aQTable
  },
  formValue,
  onchange,
  onchangeTextBox
}) => {
  const { t } = useTranslation();

  const getParameterValue = useCallback(
    (name: ParameterCode) =>
      findParameter(formValue?.parameters?.parameters, name)?.value,
    [formValue?.parameters?.parameters]
  );

  const getDropdownValue = (name: ParameterCode, items?: RefData[]) =>
    items?.find(i => i.code === getParameterValue(name));

  const currentErrors = useMemo(
    () =>
      ({
        subtransactionCode: !getParameterValue(
          ParameterCode.SubtransactionCode
        )?.trim() && {
          status: true,
          message: t(
            'txt_assign_pricing_strategies_configuration_nm738_0131_validation_required_subtransaction'
          )
        },
        methodOverrideID: !getParameterValue(
          ParameterCode.MethodOverrideId
        )?.trim() && {
          status: true,
          message: t(
            'txt_assign_pricing_strategies_configuration_nm738_0131_validation_required_method_override'
          )
        }
      } as Record<keyof IFieldValidate, IFormError>),
    [getParameterValue, t]
  );

  const [, errors, setTouched] =
    useFormValidations<keyof IFieldValidate>(currentErrors);

  return (
    <div className="border-top mt-24 pt-24">
      <h5 className="mb-8">
        {t('txt_assign_pricing_strategies_configuration_nm738_0131_title')}
      </h5>
      <p className="color-grey">
        {t('txt_assign_pricing_strategies_configuration_nm738_0131_desc')}
      </p>
      <div className="row">
        <div className="col-lg-4 col-md-6 mt-16">
          <FieldTooltip
            keepShowOnDevice={false}
            placement="right"
            element={
              <div>
                <div className="text-uppercase">
                  {t(
                    'txt_assign_pricing_strategies_configuration_nm738_0131_subtransaction_code_title'
                  )}
                </div>
                <p className="mt-8">
                  {t(
                    'txt_assign_pricing_strategies_configuration_nm738_0131_subtransaction_code_desc'
                  )}
                </p>
              </div>
            }
          >
            <ComboBox
              value={getDropdownValue(
                ParameterCode.SubtransactionCode,
                subtransactionCode
              )}
              onChange={onchange}
              label={t(
                'txt_assign_pricing_strategies_configuration_nm738_0131_subtransaction_code'
              )}
              name={ParameterCode.SubtransactionCode}
              textField="text"
              required
              onFocus={setTouched('subtransactionCode')}
              onBlur={setTouched('subtransactionCode', true)}
              error={errors.subtransactionCode}
            >
              {subtransactionCode?.map(item => {
                return (
                  <ComboBox.Item
                    key={item.code}
                    value={item}
                    label={item.text}
                  />
                );
              })}
            </ComboBox>
          </FieldTooltip>
        </div>
        <div className="col-lg-4 col-md-6 mt-16">
          <FieldTooltip
            keepShowOnDevice={false}
            placement="right"
            element={
              <div>
                <div className="text-uppercase">{t('txt_id')}</div>
                <p className="mt-8">
                  {t(
                    'txt_assign_pricing_strategies_configuration_nm738_0131_method_override_desc'
                  )}
                </p>
              </div>
            }
          >
            <EnhanceDropdownList
              value={getDropdownValue(
                ParameterCode.MethodOverrideId,
                methodOverrideID
              )}
              onChange={onchange}
              label={t(
                'txt_assign_pricing_strategies_configuration_nm738_0131_method_override_id'
              )}
              name={ParameterCode.MethodOverrideId}
              options={methodOverrideID}
              required
              onFocus={setTouched('methodOverrideID')}
              onBlur={setTouched('methodOverrideID', true)}
            />
          </FieldTooltip>
        </div>
        <div className="col-lg-4 col-md-6 mt-16">
          <FieldTooltip
            keepShowOnDevice={false}
            placement="right"
            element={
              <div>
                <div className="text-uppercase">
                  {t('txt_assign_pricing_strategies_configuration_change')}
                </div>
                <p className="mt-8">
                  {t(
                    'txt_assign_pricing_strategies_configuration_nm738_0131_override_reason_code_desc'
                  )}
                </p>
              </div>
            }
          >
            <TextBox
              value={getParameterValue(ParameterCode.OverrideReasonCode) || ''}
              onChange={onchangeTextBox}
              label={t(
                'txt_assign_pricing_strategies_configuration_nm738_0131_override_reason_code'
              )}
              name={ParameterCode.OverrideReasonCode}
              id="OverrideReasonCode_id"
              maxLength={2}
            />
          </FieldTooltip>
        </div>
        <div className="col-lg-4 col-md-6 mt-16">
          <FieldTooltip
            keepShowOnDevice={false}
            placement="right"
            element={
              <div>
                <div className="text-uppercase">
                  {t('txt_assign_pricing_strategies_configuration_lock')}
                </div>
                <p className="mt-8">
                  {t(
                    'txt_assign_pricing_strategies_configuration_nm738_0131_lock_unlock_desc'
                  )}
                </p>
              </div>
            }
          >
            <EnhanceDropdownList
              value={getDropdownValue(ParameterCode.LockUnlock, lockUnlock)}
              onChange={onchange}
              label={t(
                'txt_assign_pricing_strategies_configuration_nm738_0131_lock_unlock'
              )}
              name={ParameterCode.LockUnlock}
              options={lockUnlock}
            />
          </FieldTooltip>
        </div>
        <div className="col-lg-4 col-md-6 mt-16">
          <FieldTooltip
            keepShowOnDevice={false}
            placement="right"
            element={
              <div>
                <div className="text-uppercase">
                  {t(
                    'txt_assign_pricing_strategies_configuration_nm738_0131_allocate_immediately_title'
                  )}
                </div>
                <p className="mt-8">
                  {t(
                    'txt_assign_pricing_strategies_configuration_nm738_0131_allocate_immediately_desc'
                  )}
                </p>
              </div>
            }
          >
            <EnhanceDropdownList
              value={getDropdownValue(
                ParameterCode.AllocateImmediately,
                allocateImmediately
              )}
              onChange={onchange}
              label={t(
                'txt_assign_pricing_strategies_configuration_nm738_0131_allocate_immediately'
              )}
              name={ParameterCode.AllocateImmediately}
              options={allocateImmediately}
            />
          </FieldTooltip>
        </div>
        <div className="col-lg-4 col-md-6 mt-16">
          <FieldTooltip
            keepShowOnDevice={false}
            placement="right"
            element={
              <div>
                <div className="text-uppercase">
                  {t(
                    'txt_assign_pricing_strategies_configuration_nm738_0131_aq_table_title'
                  )}
                </div>
                <p className="mt-8">
                  {t(
                    'txt_assign_pricing_strategies_configuration_nm738_0131_aq_table_desc'
                  )}
                </p>
              </div>
            }
          >
            <ComboBox
              textField="text"
              value={getDropdownValue(ParameterCode.AqTable, aQTable)}
              name={ParameterCode.AqTable}
              label={t(
                'txt_assign_pricing_strategies_configuration_nm738_0131_aq_table'
              )}
              id="AQTable_id"
              onChange={onchange}
              noResult={t('txt_no_results_found_without_dot')}
            >
              {aQTable?.map(item => (
                <ComboBox.Item key={item.code} value={item} label={item.text} />
              ))}
            </ComboBox>
          </FieldTooltip>
        </div>
      </div>
    </div>
  );
};

export default NM738_0131;
