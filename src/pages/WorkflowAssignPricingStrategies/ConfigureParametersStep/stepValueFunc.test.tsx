import { ConfigureParametersStepProps } from '.';
import { ParameterCode } from './parameterCodes';
import stepValueFunc from './stepValueFunc';

describe('AssignPricingStrategiesWorkFlow > ConfigureParametersStep > stepValueFunc', () => {
  const t = (v: string) => v;

  it('Should Return Check param', () => {
    const stepsForm = {
      parameters: {
        transactionId: 'nm.168',
        parameters: [{ name: ParameterCode.Entry_168, value: 'CA' }]
      }
    } as ConfigureParametersStepProps;

    const response = stepValueFunc({ stepsForm, t });
    expect(response).toEqual(
      'txt_assign_pricing_strategies_configuration_progress_bar_nm168_CA'
    );
  });

  it('Should Return Check param', () => {
    const stepsForm = {
      parameters: {
        transactionId: 'nm.168',
        parameters: [{ name: ParameterCode.Entry_168, value: 'CP' }]
      }
    } as ConfigureParametersStepProps;

    const response = stepValueFunc({ stepsForm, t });
    expect(response).toEqual(
      'txt_assign_pricing_strategies_configuration_progress_bar_nm168_CP'
    );
  });

  it('Should Return Check param', () => {
    const stepsForm = {
      parameters: {
        transactionId: 'nm.169'
      }
    } as ConfigureParametersStepProps;

    const response = stepValueFunc({ stepsForm, t });
    expect(response).toEqual(
      'txt_assign_pricing_strategies_configuration_progress_bar_nm169'
    );
  });

  it('Should Return Check param', () => {
    const stepsForm = {
      parameters: {
        transactionId: 'nm.170'
      }
    } as ConfigureParametersStepProps;

    const response = stepValueFunc({ stepsForm, t });
    expect(response).toEqual(
      'txt_assign_pricing_strategies_configuration_progress_bar_nm170'
    );
  });

  it('Should Return Check param', () => {
    const stepsForm = {
      parameters: {
        transactionId: 'nm.738',
        parameters: [{ name: ParameterCode.Entry_738, value: '01-31' }]
      }
    } as ConfigureParametersStepProps;

    const response = stepValueFunc({ stepsForm, t });
    expect(response).toEqual(
      'txt_assign_pricing_strategies_configuration_progress_bar_nm738_0131'
    );
  });

  it('Should Return Check param', () => {
    const stepsForm = {
      parameters: {
        transactionId: 'nm.738',
        parameters: [{ name: ParameterCode.Entry_738, value: '60' }]
      }
    } as ConfigureParametersStepProps;

    const response = stepValueFunc({ stepsForm, t });
    expect(response).toEqual(
      'txt_assign_pricing_strategies_configuration_progress_bar_nm738_60'
    );
  });

  it('Should Return Check param', () => {
    const response = stepValueFunc({ stepsForm: undefined as any, t });
    expect(response).toEqual('');
  });
});
