import { WorkflowMetadataList } from 'app/fixtures/workflow-metadata';
import * as helper from 'app/helpers/isDevice';
import { mockUseDispatchFnc, renderWithMockStore } from 'app/utils';
import React from 'react';
import NM168_CA from './NM168_CA';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/utils/MockView')
);
const mockUseDispatch = mockUseDispatchFnc();
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderComponent = (props: any) => {
  return renderWithMockStore(<NM168_CA {...props} />, {
    initialState: {
      workflowSetup: {
        elementMetadata: {
          elements: WorkflowMetadataList
        }
      }
    } as AppState
  });
};
const mockHelper: any = helper;
describe('ConfigureParametersStepSummary ', () => {
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => (() => {}) as any);
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
  });

  it('Should render NM168_CA > isDevice', async () => {
    mockHelper.isDevice = true;
  });

  it('Should render NM168_CA > isDevice false', async () => {
    mockHelper.isDevice = false;
  });

  it('Should render NM168_CA > value', async () => {
    const props = {
      value: [
        {
          value: '1234'
        }
      ]
    } as any;
    const wrapper = renderComponent(props);
    const text = (await wrapper).getByText(
      'txt_assign_pricing_strategies_configuration_nm168_CA_title'
    );
    expect(text).toBeInTheDocument;
  });
});
