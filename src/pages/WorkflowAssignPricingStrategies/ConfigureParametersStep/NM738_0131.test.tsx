import { WorkflowMetadataList } from 'app/fixtures/workflow-metadata';
import * as helper from 'app/helpers/isDevice';
import { mockUseDispatchFnc, renderWithMockStore } from 'app/utils';
import React from 'react';
import NM738_0131 from './NM738_0131';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/utils/MockView')
);
const mockUseDispatch = mockUseDispatchFnc();
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderComponent = (props: any) => {
  return renderWithMockStore(<NM738_0131 {...props} />, {
    initialState: {
      workflowSetup: {
        elementMetadata: {
          elements: WorkflowMetadataList
        }
      }
    } as AppState
  });
};
const mockHelper: any = helper;
describe('ConfigureParametersStepSummary ', () => {
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => (() => {}) as any);
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
  });

  it('Should render NM738_0131 > isDevice', async () => {
    mockHelper.isDevice = true;
  });

  it('Should render NM738_0131 > isDevice false', async () => {
    mockHelper.isDevice = false;
  });

  it('Should render NM738_0131 > value', async () => {
    const props = {
      value: {
        subtransactionCode: [
          {
            value: '02',
            name: 'Interest rate (CP IC ID)'
          }
        ],
        methodOverrideID: [
          {
            value: 'LSTDIFF',
            name: 'Replace the current method override on this account with the last different method override.'
          }
        ],
        lockUnlock: [
          {
            value: 'L',
            name: 'Lock the current method override for this account.'
          }
        ],
        allocateImmediately: [
          {
            value: 'X',
            name: 'Use the account qualification and client allocation tables to review and, if applicable, allocate the account during nightly processing.'
          }
        ],
        aQTable: [
          {
            value: 'AQTABLE1'
          }
        ]
      }
    } as any;
    const wrapper = renderComponent(props);
    const text = (await wrapper).getByText(
      'txt_assign_pricing_strategies_configuration_nm738_0131_aq_table'
    );
    expect(text).toBeInTheDocument;
  });
});
