export enum ParameterCode {
  Entry_168 = 'nm.168.entry.format',
  Entry_738 = 'nm.738.entry.format',

  PortfolioId = 'portfolio.id',
  StrategyId = 'strategy.id',
  Status = 'status',
  Type = 'type',

  StartDate = 'start.date',
  EndDate = 'end.date',
  ReallocateImmediately = 'reallocate.immediately',

  SubtransactionCode = 'subtransaction.code',
  MethodOverrideId = 'method.override.id',
  OverrideReasonCode = 'override.reason.code',
  LockUnlock = 'lock.unlock',
  AllocateImmediately = 'allocate.immediately',
  AqTable = 'aq.table',
  SVC_SubjectSection = 'method.override.svc.subject.section',
  Lock = 'lock',
  LockDate = 'lock.date',
  UnlockDate = 'unlock.date',
  Reallocate = 'reallocate',
  ReasonCode = 'reason.code'
}
