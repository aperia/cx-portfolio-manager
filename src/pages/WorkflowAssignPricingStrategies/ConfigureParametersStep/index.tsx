import { FormatTime } from 'app/constants/enums';
import {
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { formatTimeDefault } from 'app/helpers';
import { useUnsavedChangeRegistry } from 'app/hooks';
import {
  DropdownBaseChangeEvent,
  InlineMessage,
  Radio,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import { isUndefined, uniqBy } from 'lodash';
import {
  actionsWorkflowSetup,
  useSelectElementMetadataForAssignPricingStrategies
} from 'pages/_commons/redux/WorkflowSetup';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { Detail } from 'react-calendar';
import { useDispatch } from 'react-redux';
import ConfigureParametersStepSummary from './ConfigureParametersStepSummary';
import findParameter from './findParameter';
import NM168_CA from './NM168_CA';
import NM168_CP from './NM168_CP';
import NM169 from './NM169';
import NM170 from './NM170';
import NM738_0131 from './NM738_0131';
import NM738_60 from './NM738_60';
import { ParameterCode } from './parameterCodes';
import parseFormValues from './parseFormValues';
import stepValueFunc from './stepValueFunc';

export interface Parameter {
  value?: string;
  name: ParameterCode;
}
export interface ConfigureParametersStepProps {
  isValid?: boolean;
  parameters?: {
    transactionId?: string;
    parameters?: Parameter[];
  };
  parametersNM168?: Parameter[];
  parametersNM169?: Parameter[];
  parametersNM170?: Parameter[];
  parametersNM738?: Parameter[];
}
export interface IFieldValidate {
  portfolio: string;
  subtransactionCode: string;
  methodOverrideID: string;
  serviceSubjectSection: string;
}

interface DateRangeInfo {
  minStartDateLock?: Date;
  maxStartDateLock?: Date;
  minEndDateLock?: Date;
  maxEndDateLock?: Date;
}

const ConfigureParametersStep: React.FC<
  WorkflowSetupProps<ConfigureParametersStepProps>
> = ({ savedAt, formValues, isStuck, setFormValues }) => {
  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;

  const { t } = useTranslation();
  const dispatch = useDispatch();

  const [initialValues, setInitialValues] = useState(formValues);

  const {
    configuration,
    reallocate,
    portfolio,
    strategy,
    status,
    type,
    subtraction,
    subtransactionCode,
    allocateImmediately,
    lock,
    lockUnlock,
    methodOverrideID,
    reallocateNM738,
    serviceSubjectSection,
    aQTable
  } = useSelectElementMetadataForAssignPricingStrategies();

  const dropdownCP = { strategy, status, type };
  const subtransactionData0130 = {
    subtransactionCode,
    methodOverrideID,
    lockUnlock,
    allocateImmediately,
    aQTable
  };
  const subtransactionData60 = {
    serviceSubjectSection,
    lock,
    reallocateNM738
  };

  const [dateRange, setDateRange] = useState<DateRangeInfo>();

  const defaultStatus = status.filter(
    (x: any) => x.code?.toLowerCase() === 'blank'
  )[0];
  const [minDateSetting, setMinDateSetting] = useState();
  const [maxDateSetting, setMaxDateSetting] = useState();
  const [invalidDate, setInvalidDate] = useState([]);

  const minDate = useMemo(() => {
    return new Date(formatTimeDefault(minDateSetting!, FormatTime.DateServer));
  }, [minDateSetting]);

  const maxDate = useMemo(() => {
    return new Date(formatTimeDefault(maxDateSetting!, FormatTime.DateServer));
  }, [maxDateSetting]);

  const getParameterValue = useCallback(
    (name: ParameterCode) =>
      findParameter(formValues.parameters?.parameters, name)?.value,
    [formValues.parameters?.parameters]
  );

  const mn168Action = useMemo(
    () => getParameterValue(ParameterCode.Entry_168),
    [getParameterValue]
  );
  const mn738Action = useMemo(
    () => getParameterValue(ParameterCode.Entry_738),
    [getParameterValue]
  );

  useEffect(() => {
    dispatch(
      actionsWorkflowSetup.getWorkflowMetadata([
        'transaction.id',
        ParameterCode.Entry_168,
        ParameterCode.Entry_738,
        ParameterCode.PortfolioId,
        ParameterCode.StrategyId,
        ParameterCode.Status,
        ParameterCode.Type,
        ParameterCode.SubtransactionCode,
        ParameterCode.MethodOverrideId,
        ParameterCode.SVC_SubjectSection,
        ParameterCode.LockUnlock,
        ParameterCode.AllocateImmediately,
        ParameterCode.Lock,
        ParameterCode.Reallocate,
        ParameterCode.AqTable
      ])
    );
  }, [dispatch]);

  const initFields = useCallback(async () => {
    const responseDateRange = await Promise.resolve(
      dispatch(actionsWorkflowSetup.getWorkflowEffectiveDateOptions({}))
    );

    const { maxDate, minDate, invalidDates } =
      (responseDateRange as any)?.payload?.data || {};
    setMinDateSetting(minDate);
    setMaxDateSetting(maxDate);
    setInvalidDate(invalidDates);
  }, [dispatch]);

  useEffect(() => {
    initFields();
  }, [initFields]);

  useEffect(() => {
    setInitialValues(formValues);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [savedAt]);

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__ASSIGN_PRICING_STRATEGIES_CONFIGURE_PARAMETERS,
      priority: 1
    },
    [initialValues !== formValues]
  );

  useEffect(() => {
    setDateRange(dateRange => ({
      ...dateRange,
      ['minStartDateLock']: minDate
    }));
    setDateRange(dateRange => ({
      ...dateRange,
      ['maxStartDateLock']: maxDate
    }));
    setDateRange(dateRange => ({
      ...dateRange,
      ['minEndDateLock']: minDate
    }));
    setDateRange(dateRange => ({
      ...dateRange,
      ['maxEndDateLock']: maxDate
    }));
  }, [minDate, maxDate]);

  useEffect(() => {
    const action = formValues?.parameters?.transactionId;

    let isValid = !isUndefined(action);

    if (action === 'nm.168' && mn168Action === 'CA') {
      isValid = getParameterValue(ParameterCode.PortfolioId) !== undefined;
    }

    if (action === 'nm.169' || action === 'nm.170') {
      isValid = true;
    }

    if (action === 'nm.738' && mn738Action == '01-31') {
      isValid =
        getParameterValue(ParameterCode.SubtransactionCode) !== undefined &&
        getParameterValue(ParameterCode.MethodOverrideId) !== undefined;
    }

    if (action === 'nm.738' && mn738Action == '60') {
      isValid =
        getParameterValue(ParameterCode.SVC_SubjectSection) !== undefined;
    }

    if (formValues.isValid === isValid) return;
    keepRef.current.setFormValues({ isValid });
  }, [getParameterValue, formValues, mn168Action, mn738Action]);

  const disabledRange = useMemo(
    () =>
      invalidDate?.map((d: any) => {
        const fromDate = new Date(
          formatTimeDefault(d.date, FormatTime.DateFrom)
        );
        const toDate = new Date(formatTimeDefault(d.date, FormatTime.DateTo));
        return { fromDate, toDate };
      }),
    [invalidDate]
  );

  const invalidDateTooltips = useMemo(
    () =>
      invalidDate?.reduceRight((pre, val: any) => {
        pre[formatTimeDefault(val.date, FormatTime.DateServer)] = val.reason;
        return pre;
      }, {} as Record<string, string>) || {},
    [invalidDate]
  );

  const getDateTooltip = useCallback(
    (date: Date, view: Detail | undefined = 'month') => {
      if (view !== 'month') return;

      const tooltipContent =
        invalidDateTooltips[
          formatTimeDefault(date.toISOString(), FormatTime.DateServer)
        ];

      if (tooltipContent) return <Tooltip element={tooltipContent} />;
    },
    [invalidDateTooltips]
  );

  const handleSelectAction = (transactionId: string) => {
    setFormValues({
      parameters: {
        transactionId,
        parameters: (formValues as any)?.[`parameters${transactionId}`]
      }
    });
  };

  const handleSelectActionNM168 = (action: string) => {
    const parameters: Parameter[] = [
      { name: ParameterCode.Entry_168, value: action },
      ...(formValues.parameters?.parameters || [])
    ];
    if (action === 'CP') {
      parameters.unshift({
        name: ParameterCode.Status,
        value: defaultStatus.code
      });
    }

    const value = {
      transactionId: 'nm.168',
      parameters: uniqBy(parameters, 'name')
    };
    setFormValues({
      parametersNM168: value.parameters,
      parameters: value
    });
  };

  const handleSelectActionNM738 = (action: string) => {
    const parameters: Parameter[] = [
      { name: ParameterCode.Entry_738, value: action },
      ...(formValues.parameters?.parameters || [])
    ];
    const value = {
      transactionId: 'nm.738',
      parameters: uniqBy(parameters, 'name')
    };
    setFormValues({
      parametersNM168: value.parameters,
      parameters: value
    });
  };

  const handleDateChange = (e: any) => {
    const { value, name } = e.target;
    const action = formValues.parameters?.transactionId;

    if (name === ParameterCode.StartDate) {
      setDateRange(dateRange => ({
        ...dateRange,
        ['minEndDateLock']: value
      }));
    }

    if (name === ParameterCode.EndDate) {
      setDateRange(dateRange => ({
        ...dateRange,
        ['maxStartDateLock']: value
      }));
    }

    setValue(action, name, value);
  };

  const handleCheckBoxChange = (e: any) => {
    const value = e.target?.checked;
    const action = formValues.parameters?.transactionId;

    setValue(action, e.target?.name, value);
  };

  const handleOnChangeDropdown = (event: DropdownBaseChangeEvent) => {
    const { name, value } = event.target;
    const action = formValues.parameters?.transactionId;

    setValue(action, name as ParameterCode, value?.code);
  };

  const handleOnChangeTextBox = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const { name, value } = event.target;
    const action = formValues.parameters?.transactionId;

    setValue(action, name as ParameterCode, value);
  };

  const setValue = (action: any, name: ParameterCode, value: string) => {
    const parameters: Parameter[] = [
      { name, value },
      ...(formValues.parameters?.parameters || [])
    ];
    const _value = {
      transactionId: formValues.parameters?.transactionId,
      parameters: uniqBy(parameters, 'name')
    };

    const actionKeepId = `parameters${action}`;
    setFormValues({
      parameters: _value,
      [actionKeepId]: _value.parameters
    });
  };

  return (
    <div className="position-relative">
      {isStuck && (
        <InlineMessage className="mb-0 mt-24" variant="danger" withIcon>
          {t('txt_step_stuck_move_forward_message')}
        </InlineMessage>
      )}
      <h5 className="mt-24 mb-16">
        {t('txt_assign_pricing_strategies_configuration_select_action')}
      </h5>
      <div className="row">
        <div className="col-md-6">
          <div className="group-card">
            {configuration?.map((item: any, index: number) => {
              return (
                <div
                  key={index}
                  className="group-card-item d-flex align-items-center custom-control-root px-16 py-18"
                  onClick={() => handleSelectAction(item.code)}
                >
                  <span className="pr-8">{item.text}</span>
                  <Radio className="ml-auto mr-n4">
                    <Radio.Input
                      checked={
                        item.code === formValues.parameters?.transactionId
                      }
                      className="checked-style"
                    />
                  </Radio>
                </div>
              );
            })}
          </div>
        </div>
        {formValues.parameters?.transactionId === 'nm.168' && (
          <div className="col-md-6">
            <div className="bg-light-l20 rounded-lg p-16 h-100 ">
              <p className="fw-600 mb-8">
                {t(
                  'txt_assign_pricing_strategies_configuration_nm168_entry_title'
                )}
              </p>
              <p className="color-grey">
                {t(
                  'txt_assign_pricing_strategies_configuration_nm168_entry_desc'
                )}
              </p>
              <div className="divider-dashed my-8"></div>
              <p className="color-grey mb-8 fw-600">
                {t('txt_assign_pricing_strategies_configuration_select_entry')}
              </p>
              {reallocate?.map((item: any, index: number) => {
                return (
                  <div
                    key={index}
                    className="d-flex mb-8"
                    onClick={() => handleSelectActionNM168(item.code)}
                  >
                    <Radio>
                      <Radio.Input
                        id={item.code}
                        checked={item.code === mn168Action}
                      />
                    </Radio>
                    <span className="cursor-pointer">{item.text}</span>
                  </div>
                );
              })}
            </div>
          </div>
        )}
        {formValues.parameters?.transactionId === 'nm.169' && (
          <div className="col-md-6">
            <div className="bg-light-l20 rounded-lg p-16 h-100 ">
              <p className="fw-600 mb-8">
                {t(
                  'txt_assign_pricing_strategies_configuration_nm169_entry_title'
                )}
              </p>
              <p className="color-grey">
                {t(
                  'txt_assign_pricing_strategies_configuration_nm169_entry_desc'
                )}
              </p>
            </div>
          </div>
        )}
        {formValues.parameters?.transactionId === 'nm.170' && (
          <div className="col-md-6">
            <div className="bg-light-l20 rounded-lg p-16 h-100 ">
              <p className="fw-600 mb-8">
                {t(
                  'txt_assign_pricing_strategies_configuration_nm170_entry_title'
                )}
              </p>
              <p className="color-grey">
                {t(
                  'txt_assign_pricing_strategies_configuration_nm170_entry_desc'
                )}
              </p>
            </div>
          </div>
        )}
        {formValues.parameters?.transactionId === 'nm.738' && (
          <div className="col-md-6">
            <div className="bg-light-l20 rounded-lg p-16 h-100 ">
              <p className="fw-600 mb-8">
                {t(
                  'txt_assign_pricing_strategies_configuration_nm738_entry_title'
                )}
              </p>
              <p className="color-grey">
                {t(
                  'txt_assign_pricing_strategies_configuration_nm738_entry_desc'
                )}
              </p>
              <div className="divider-dashed my-8"></div>
              <p className="color-grey mb-8 fw-600">
                {t(
                  'txt_assign_pricing_strategies_configuration_select_subtraction_option'
                )}
              </p>
              {subtraction?.map((item: any, index: number) => {
                return (
                  <div
                    key={index}
                    className="d-flex mb-8"
                    onClick={() => handleSelectActionNM738(item.code)}
                  >
                    <Radio>
                      <Radio.Input
                        id={item.name}
                        checked={item.code === mn738Action}
                      />
                    </Radio>
                    <span className="cursor-pointer">{item.text}</span>
                  </div>
                );
              })}
            </div>
          </div>
        )}
      </div>

      {formValues.parameters?.transactionId === 'nm.168' &&
        mn168Action === 'CA' && (
          <NM168_CA
            value={portfolio}
            formValue={formValues}
            onchange={handleOnChangeDropdown}
          />
        )}

      {formValues.parameters?.transactionId === 'nm.168' &&
        mn168Action === 'CP' && (
          <NM168_CP
            value={dropdownCP}
            formValue={formValues}
            onchange={handleOnChangeDropdown}
          />
        )}

      {formValues.parameters?.transactionId === 'nm.169' && (
        <NM169
          formValue={formValues}
          onchangeDate={handleDateChange}
          onchangeCheckBox={handleCheckBoxChange}
          dateRange={dateRange || ''}
          disabledRange={disabledRange}
          tooltipDisabledDate={getDateTooltip}
        />
      )}

      {formValues.parameters?.transactionId === 'nm.170' && (
        <NM170
          formValue={formValues}
          onchangeDate={handleDateChange}
          onchangeCheckBox={handleCheckBoxChange}
          minDate={minDate}
          maxDate={maxDate}
        />
      )}

      {formValues.parameters?.transactionId === 'nm.738' &&
        mn738Action === '01-31' && (
          <NM738_0131
            value={subtransactionData0130}
            formValue={formValues}
            onchange={handleOnChangeDropdown}
            onchangeTextBox={handleOnChangeTextBox}
          />
        )}

      {formValues.parameters?.transactionId === 'nm.738' &&
        mn738Action === '60' && (
          <NM738_60
            value={subtransactionData60}
            formValue={formValues}
            onchange={handleOnChangeDropdown}
            onchangeTextBox={handleOnChangeTextBox}
            onchangeDate={handleDateChange}
            dateRange={dateRange || ''}
          />
        )}
    </div>
  );
};

const ExtraStaticConfigureParametersStep =
  ConfigureParametersStep as WorkflowSetupStaticProp<ConfigureParametersStepProps>;

ExtraStaticConfigureParametersStep.summaryComponent =
  ConfigureParametersStepSummary;
ExtraStaticConfigureParametersStep.stepValue = stepValueFunc;
ExtraStaticConfigureParametersStep.defaultValues = {
  isValid: false,
  parameters: {}
};
ExtraStaticConfigureParametersStep.parseFormValues = parseFormValues;

export default ExtraStaticConfigureParametersStep;
