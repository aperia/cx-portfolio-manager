import { WorkflowMetadataList } from 'app/fixtures/workflow-metadata';
import * as helper from 'app/helpers/isDevice';
import { mockUseDispatchFnc, renderWithMockStore } from 'app/utils';
import React from 'react';
import NM170 from './NM170';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/utils/MockView')
);
const mockUseDispatch = mockUseDispatchFnc();
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderComponent = (props: any) => {
  return renderWithMockStore(<NM170 {...props} />, {
    initialState: {
      workflowSetup: {
        elementMetadata: {
          elements: WorkflowMetadataList
        }
      }
    } as AppState
  });
};
const mockHelper: any = helper;
describe('ConfigureParametersStepSummary ', () => {
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => (() => {}) as any);
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
  });

  it('Should render NM170 > isDevice', async () => {
    mockHelper.isDevice = true;

    const props = {};
    const wrapper = await renderComponent(props);
    const text = wrapper.getByText(
      'txt_assign_pricing_strategies_configuration_nm170_reallocate_label'
    );
    expect(text).toBeInTheDocument;
  });

  it('Should render NM170 > isDevice false', async () => {
    mockHelper.isDevice = false;

    const props = {};
    const wrapper = await renderComponent(props);
    const text = wrapper.getByText(
      'txt_assign_pricing_strategies_configuration_nm170_reallocate_label'
    );
    expect(text).toBeInTheDocument;
  });
});
