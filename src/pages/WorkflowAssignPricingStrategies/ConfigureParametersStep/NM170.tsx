import EnhanceDatePicker from 'app/components/EnhanceDatePicker';
import FieldTooltip from 'app/components/FieldTooltip';
import { isDevice, isValidDate } from 'app/helpers';
import {
  CheckBox,
  DatePickerChangeEvent,
  Icon,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import React from 'react';
import { ConfigureParametersStepProps } from '.';
import findParameter from './findParameter';
import { ParameterCode } from './parameterCodes';

interface NM170Props {
  value?: any;
  formValue?: ConfigureParametersStepProps;
  onchangeDate?: (event: DatePickerChangeEvent) => void;
  onchangeCheckBox?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  dateRange?: any;
  disabledRange?: any;
  minDate?: any;
  maxDate?: any;
}
const NM170: React.FC<NM170Props> = ({
  formValue,
  onchangeDate,
  onchangeCheckBox,
  minDate,
  maxDate
}) => {
  const { t } = useTranslation();

  const getParameterValue = (
    name: ParameterCode,
    { parseDate }: { parseDate?: boolean } = {}
  ) => {
    const value = findParameter(formValue?.parameters?.parameters, name)?.value;

    return parseDate
      ? isValidDate(value)
        ? new Date(value!)
        : undefined
      : value;
  };

  return (
    <div className="border-top mt-24 pt-24">
      <h5 className="mb-16">
        {t('txt_assign_pricing_strategies_configuration_nm170_title')}
      </h5>
      <div className="row">
        <div className="col-lg-4 col-md-6">
          <FieldTooltip
            placement={isDevice ? 'right-start' : 'left-start'}
            element={
              <div>
                <div className="text-uppercase">
                  {t(
                    'txt_assign_pricing_strategies_configuration_nm170_end_date'
                  )}
                </div>
                <p className="mt-8">
                  {t(
                    'txt_assign_pricing_strategies_configuration_nm170_end_date_desc'
                  )}
                </p>
              </div>
            }
          >
            <EnhanceDatePicker
              id="newChange__170endDate"
              value={
                getParameterValue(ParameterCode.EndDate, {
                  parseDate: true
                }) as any
              }
              minDate={minDate}
              maxDate={maxDate}
              name={ParameterCode.EndDate}
              onChange={onchangeDate}
              label={t('txt_end_date')}
            />
          </FieldTooltip>
        </div>
      </div>

      <div className="d-flex align-items-center mt-16">
        <p className="color-grey mr-8 fw-600">
          {t(
            'txt_assign_pricing_strategies_configuration_nm170_reallocate_immediately'
          )}
        </p>
        <Tooltip
          element={
            <div className="my-8">
              <div className="text-uppercase">
                {t(
                  'txt_assign_pricing_strategies_configuration_nm170_reallocate'
                )}
              </div>
              <p className="mt-8">
                {t(
                  'txt_assign_pricing_strategies_configuration_nm170_reallocate_desc'
                )}
              </p>
            </div>
          }
          displayOnClick={false}
          triggerClassName="d-flex"
        >
          <Icon name="information" size="5x" className="color-grey-l16" />
        </Tooltip>
      </div>
      <div className="d-flex mt-8">
        <CheckBox>
          <CheckBox.Input
            id={'ReallocateUnLock_id'}
            name={ParameterCode.ReallocateImmediately}
            disabled={false}
            checked={
              getParameterValue(ParameterCode.ReallocateImmediately) as any
            }
            onChange={onchangeCheckBox}
          />
          <CheckBox.Label>
            {t(
              'txt_assign_pricing_strategies_configuration_nm170_reallocate_label'
            )}
          </CheckBox.Label>
        </CheckBox>
      </div>
    </div>
  );
};

export default NM170;
