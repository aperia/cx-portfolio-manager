import EnhanceDatePicker from 'app/components/EnhanceDatePicker';
import EnhanceDropdownList from 'app/components/EnhanceDropdownList';
import FieldTooltip from 'app/components/FieldTooltip';
import { isDevice, isValidDate } from 'app/helpers';
import { useFormValidations } from 'app/hooks';
import {
  ComboBox,
  DatePickerChangeEvent,
  DropdownBaseChangeEvent,
  TextBox,
  useTranslation
} from 'app/_libraries/_dls';
import React, { useCallback, useMemo } from 'react';
import { ConfigureParametersStepProps, IFieldValidate } from '.';
import findParameter from './findParameter';
import { ParameterCode } from './parameterCodes';

interface NM738Props {
  value: {
    serviceSubjectSection: RefData[];
    lock: RefData[];
    reallocateNM738: RefData[];
  };
  formValue?: ConfigureParametersStepProps;
  onchange?: (event: DropdownBaseChangeEvent) => void;
  onchangeTextBox?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  onchangeDate?: (event: DatePickerChangeEvent) => void;
  dateRange?: any;
}
const NM738_60: React.FC<NM738Props> = ({
  value: { serviceSubjectSection, lock, reallocateNM738 },
  formValue,
  onchange,
  onchangeTextBox,
  onchangeDate
}) => {
  const { t } = useTranslation();
  const currentDate = new Date();

  const getParameterValue = useCallback(
    (name: ParameterCode, { parseDate }: { parseDate?: boolean } = {}): any => {
      const value = findParameter(
        formValue?.parameters?.parameters,
        name
      )?.value;

      return parseDate
        ? isValidDate(value)
          ? new Date(value!)
          : undefined
        : value;
    },
    [formValue?.parameters?.parameters]
  );

  const getDropdownValue = (name: ParameterCode, items?: RefData[]) =>
    items?.find(i => i.code === getParameterValue(name));

  const currentErrors = useMemo(
    () =>
      ({
        serviceSubjectSection: !getParameterValue(
          ParameterCode.SVC_SubjectSection
        )?.trim() && {
          status: true,
          message: t(
            'txt_assign_pricing_strategies_configuration_nm738_60_validation_required_svc_subject_section'
          )
        }
      } as Record<keyof IFieldValidate, IFormError>),
    [getParameterValue, t]
  );

  const [, errors, setTouched] =
    useFormValidations<keyof IFieldValidate>(currentErrors);

  return (
    <div className="border-top mt-24 pt-24">
      <h5 className="mb-8">
        {t('txt_assign_pricing_strategies_configuration_nm738_60_title')}
      </h5>
      <p className="color-grey">
        {t('txt_assign_pricing_strategies_configuration_nm738_60_desc')}
      </p>
      <div className="row">
        <div className="col-lg-4 col-md-6 mt-16">
          <FieldTooltip
            keepShowOnDevice={false}
            placement="right"
            element={
              <div>
                <div className="text-uppercase">{t('txt_type')}</div>
                <p className="mt-8">
                  {t(
                    'txt_assign_pricing_strategies_configuration_nm738_60_svc_subject_section_desc'
                  )}
                </p>
              </div>
            }
          >
            <ComboBox
              value={getDropdownValue(
                ParameterCode.SVC_SubjectSection,
                serviceSubjectSection
              )}
              onChange={onchange}
              label={t(
                'txt_assign_pricing_strategies_configuration_nm738_60_svc_subject_section'
              )}
              name={ParameterCode.SVC_SubjectSection}
              required
              onFocus={setTouched('serviceSubjectSection')}
              onBlur={setTouched('serviceSubjectSection', true)}
              error={errors.serviceSubjectSection}
              textField="text"
            >
              {serviceSubjectSection?.map(item => {
                return (
                  <ComboBox.Item
                    key={item.code}
                    value={item}
                    label={item.text}
                  />
                );
              })}
            </ComboBox>
          </FieldTooltip>
        </div>
        <div className="col-lg-4 col-md-6 mt-16">
          <FieldTooltip
            keepShowOnDevice={false}
            placement="right"
            element={
              <div>
                <div className="text-uppercase">
                  {t('txt_assign_pricing_strategies_configuration_lock')}
                </div>
                <p className="mt-8">
                  {t(
                    'txt_assign_pricing_strategies_configuration_nm738_60_lock_desc'
                  )}
                </p>
              </div>
            }
          >
            <EnhanceDropdownList
              value={getDropdownValue(ParameterCode.LockUnlock, lock)}
              onChange={onchange}
              label={t('txt_assign_pricing_strategies_configuration_lock')}
              name={ParameterCode.Lock}
              options={lock}
            />
          </FieldTooltip>
        </div>
        <div className="col-lg-4 col-md-6 mt-16">
          <FieldTooltip
            placement={isDevice ? 'right-start' : 'left-start'}
            element={
              <div>
                <div className="text-uppercase">{t('txt_date')}</div>
                <p className="mt-8">
                  {t(
                    'txt_assign_pricing_strategies_configuration_nm738_60_lock_date_desc'
                  )}
                </p>
              </div>
            }
          >
            <EnhanceDatePicker
              id="NM738__LockDate"
              value={
                getParameterValue(ParameterCode.LockDate, {
                  parseDate: true
                }) as any
              }
              minDate={currentDate}
              name={ParameterCode.LockDate}
              onChange={onchangeDate}
              label={t(
                'txt_assign_pricing_strategies_configuration_nm738_60_lock_date'
              )}
            />
          </FieldTooltip>
        </div>
        <div className="col-lg-4 col-md-6 mt-16">
          <FieldTooltip
            placement={isDevice ? 'right-start' : 'left-start'}
            element={
              <div>
                <div className="text-uppercase">
                  {t(
                    'txt_assign_pricing_strategies_configuration_nm738_60_unlock'
                  )}
                </div>
                <p className="mt-8">
                  {t(
                    'txt_assign_pricing_strategies_configuration_nm738_60_unlock_date_desc'
                  )}
                </p>
              </div>
            }
          >
            <EnhanceDatePicker
              id="NM738__UnlockDate"
              value={
                getParameterValue(ParameterCode.UnlockDate, {
                  parseDate: true
                }) as any
              }
              minDate={currentDate}
              name={ParameterCode.UnlockDate}
              onChange={onchangeDate}
              label={t(
                'txt_assign_pricing_strategies_configuration_nm738_60_unlock_date'
              )}
            />
          </FieldTooltip>
        </div>
        <div className="col-lg-4 col-md-6 mt-16">
          <FieldTooltip
            keepShowOnDevice={false}
            placement="right"
            element={
              <div>
                <div className="text-uppercase">
                  {t(
                    'txt_assign_pricing_strategies_configuration_nm738_60_reallocate'
                  )}
                </div>
                <p className="mt-8">
                  {t(
                    'txt_assign_pricing_strategies_configuration_nm738_60_reallocate_desc'
                  )}
                </p>
              </div>
            }
          >
            <EnhanceDropdownList
              value={getDropdownValue(
                ParameterCode.Reallocate,
                reallocateNM738
              )}
              onChange={onchange}
              label={t(
                'txt_assign_pricing_strategies_configuration_nm738_60_reallocate'
              )}
              name={ParameterCode.Reallocate}
              options={reallocateNM738}
            />
          </FieldTooltip>
        </div>
        <div className="col-lg-4 col-md-6 mt-16">
          <FieldTooltip
            keepShowOnDevice={false}
            placement="right"
            element={
              <div>
                <div className="text-uppercase">
                  {t('txt_assign_pricing_strategies_configuration_change')}
                </div>
                <p className="mt-8">
                  {t(
                    'txt_assign_pricing_strategies_configuration_nm738_60_reason_desc'
                  )}
                </p>
              </div>
            }
          >
            <TextBox
              value={getParameterValue(ParameterCode.ReasonCode)}
              onChange={onchangeTextBox}
              label={t('txt_reason')}
              name={ParameterCode.ReasonCode}
              id="Reason_id"
              maxLength={2}
            />
          </FieldTooltip>
        </div>
      </div>
    </div>
  );
};

export default NM738_60;
