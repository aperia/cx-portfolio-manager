import { WorkflowMetadataList } from 'app/fixtures/workflow-metadata';
import * as helper from 'app/helpers/isDevice';
import { mockUseDispatchFnc, renderWithMockStore } from 'app/utils';
import React from 'react';
import NM168_CP from './NM168_CP';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/utils/MockView')
);
const mockUseDispatch = mockUseDispatchFnc();
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderComponent = (props: any) => {
  return renderWithMockStore(<NM168_CP {...props} />, {
    initialState: {
      workflowSetup: {
        elementMetadata: {
          elements: WorkflowMetadataList
        }
      }
    } as AppState
  });
};
const mockHelper: any = helper;
describe('ConfigureParametersStepSummary ', () => {
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => (() => {}) as any);
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
  });

  it('Should render NM168_CP > isDevice', async () => {
    mockHelper.isDevice = true;
  });

  it('Should render NM168_CP > isDevice false', async () => {
    mockHelper.isDevice = false;
  });

  it('Should render NM168_CP > value', async () => {
    const props = {
      value: {
        strategy: [
          {
            value: '9864'
          }
        ],
        status: [
          {
            value: 'Blank',
            description:
              'No, do not lock the strategy onto the account; the system assumes this is a U.'
          }
        ],
        type: [
          {
            value: '0',
            description:
              'Process cash advance and merchandise principals using the new Product Control Fil'
          }
        ]
      }
    } as any;
    const wrapper = renderComponent(props);
    const text = (await wrapper).getByText('txt_strategy');
    expect(text).toBeInTheDocument;
  });
});
