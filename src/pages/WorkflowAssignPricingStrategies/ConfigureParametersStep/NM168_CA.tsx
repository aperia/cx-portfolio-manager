import FieldTooltip from 'app/components/FieldTooltip';
import { useFormValidations } from 'app/hooks';
import {
  ComboBox,
  DropdownBaseChangeEvent,
  useTranslation
} from 'app/_libraries/_dls';
import React, { useCallback, useMemo } from 'react';
import { ConfigureParametersStepProps, IFieldValidate } from '.';
import findParameter from './findParameter';
import { ParameterCode } from './parameterCodes';

interface NM168CAProps {
  value: RefData[];
  formValue?: ConfigureParametersStepProps;
  onchange?: (event: DropdownBaseChangeEvent) => void;
}
const NM168_CA: React.FC<NM168CAProps> = ({ value, formValue, onchange }) => {
  const { t } = useTranslation();

  const getParameterValue = useCallback(
    (name: ParameterCode) =>
      findParameter(formValue?.parameters?.parameters, name)?.value,
    [formValue?.parameters?.parameters]
  );

  const getDropdownValue = (name: ParameterCode, items?: RefData[]) =>
    items?.find(i => i.code === getParameterValue(name));

  const currentErrors = useMemo(
    () =>
      ({
        portfolio: !getParameterValue(ParameterCode.PortfolioId)?.trim() && {
          status: true,
          message: t('txt_required_validation', {
            fieldName: t('txt_pofolio_id')
          })
        }
      } as Record<keyof IFieldValidate, IFormError>),
    [getParameterValue, t]
  );

  const [, errors, setTouched] =
    useFormValidations<keyof IFieldValidate>(currentErrors);

  return (
    <div className="border-top mt-24 pt-24">
      <h5 className="mb-16">
        {t('txt_assign_pricing_strategies_configuration_nm168_CA_title')}
      </h5>
      <div className="row">
        <div className="col-lg-4 col-md-6">
          <FieldTooltip
            keepShowOnDevice={false}
            placement="right"
            element={
              <div>
                <div className="text-uppercase">{t('txt_pofolio_id')}</div>
                <p className="mt-8">
                  {t(
                    'txt_assign_pricing_strategies_configuration_nm168_CA_portfolio_tooltip_desc'
                  )}
                </p>
              </div>
            }
          >
            <ComboBox
              value={getDropdownValue(ParameterCode.PortfolioId, value)}
              onChange={onchange}
              label={t('txt_pofolio_id')}
              name={ParameterCode.PortfolioId}
              textField="text"
              required
              onFocus={setTouched('portfolio')}
              onBlur={setTouched('portfolio', true)}
              error={errors.portfolio}
            >
              {value?.map(item => (
                <ComboBox.Item key={item.code} value={item} label={item.text} />
              ))}
            </ComboBox>
          </FieldTooltip>
        </div>
      </div>
    </div>
  );
};

export default NM168_CA;
