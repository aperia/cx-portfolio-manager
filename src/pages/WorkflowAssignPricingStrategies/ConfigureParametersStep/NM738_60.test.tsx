import { WorkflowMetadataList } from 'app/fixtures/workflow-metadata';
import * as helper from 'app/helpers/isDevice';
import { mockUseDispatchFnc, renderWithMockStore } from 'app/utils';
import React from 'react';
import NM738_60 from './NM738_60';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/utils/MockView')
);
const mockUseDispatch = mockUseDispatchFnc();
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderComponent = (props: any) => {
  return renderWithMockStore(<NM738_60 {...props} />, {
    initialState: {
      workflowSetup: {
        elementMetadata: {
          elements: WorkflowMetadataList
        }
      }
    } as AppState
  });
};
const mockHelper: any = helper;
describe('ConfigureParametersStepSummary ', () => {
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => (() => {}) as any);
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
  });

  it('Should render NM738_60 > value & isDevice', async () => {
    mockHelper.isDevice = true;
    const props = {
      value: {
        serviceSubjectSection: [
          {
            value: 'CPICBP',
            name: 'Method ID for the method override for the Break Points section (CP IC BP) of the Product Control File.'
          }
        ],
        lock: [
          {
            value: 'L',
            name: 'Lock the current method override on this account.'
          }
        ],
        reallocateNM738: [
          {
            value: 'I',
            name: 'Review and, if applicable, reallocate the account on the date set in the UNLOCK field.'
          }
        ]
      }
    } as any;
    const wrapper = renderComponent(props);
    const text = (await wrapper).getByText(
      'txt_assign_pricing_strategies_configuration_nm738_60_reallocate'
    );
    expect(text).toBeInTheDocument;
  });

  it('Should render NM738_60 > value & !isDevice', async () => {
    mockHelper.isDevice = false;
    const props = {
      value: {
        serviceSubjectSection: [
          {
            value: 'CPICBP',
            name: 'Method ID for the method override for the Break Points section (CP IC BP) of the Product Control File.'
          }
        ],
        lock: [
          {
            value: 'L',
            name: 'Lock the current method override on this account.'
          }
        ],
        reallocateNM738: [
          {
            value: 'I',
            name: 'Review and, if applicable, reallocate the account on the date set in the UNLOCK field.'
          }
        ]
      }
    } as any;
    const wrapper = renderComponent(props);
    const text = (await wrapper).getByText(
      'txt_assign_pricing_strategies_configuration_nm738_60_reallocate'
    );
    expect(text).toBeInTheDocument;
  });
});
