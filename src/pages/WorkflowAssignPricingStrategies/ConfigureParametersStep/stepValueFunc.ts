import { ConfigureParametersStepProps } from '.';
import findParameter from './findParameter';
import { ParameterCode } from './parameterCodes';

const stepValueFunc: WorkflowSetupStepValueFunc<ConfigureParametersStepProps> =
  ({ stepsForm: formValues, t }) => {
    const { parameters } = formValues || {};

    const getParameterValue = (name: ParameterCode) =>
      findParameter(parameters?.parameters, name)?.value;

    const action = parameters?.transactionId;
    const mn168Action = getParameterValue(ParameterCode.Entry_168);
    const mn738Action = getParameterValue(ParameterCode.Entry_738);

    return action === 'nm.168' && mn168Action === 'CA'
      ? t('txt_assign_pricing_strategies_configuration_progress_bar_nm168_CA')
      : action === 'nm.168' && mn168Action === 'CP'
      ? t('txt_assign_pricing_strategies_configuration_progress_bar_nm168_CP')
      : action === 'nm.169'
      ? t('txt_assign_pricing_strategies_configuration_progress_bar_nm169')
      : action === 'nm.170'
      ? t('txt_assign_pricing_strategies_configuration_progress_bar_nm170')
      : action === 'nm.738' && mn738Action === '01-31'
      ? t('txt_assign_pricing_strategies_configuration_progress_bar_nm738_0131')
      : action === 'nm.738' && mn738Action === '60'
      ? t('txt_assign_pricing_strategies_configuration_progress_bar_nm738_60')
      : '';
  };

export default stepValueFunc;
