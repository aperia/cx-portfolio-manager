import { fireEvent } from '@testing-library/react';
import { WorkflowMetadataAll } from 'app/fixtures/workflow-metadata';
import { mockUseDispatchFnc, renderWithMockStore } from 'app/utils';
import React from 'react';
import ConfigureParametersStepSummary from './ConfigureParametersStepSummary';
import { ParameterCode } from './parameterCodes';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/utils/MockView')
);
const mockUseDispatch = mockUseDispatchFnc();
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderComponent = (props: any) => {
  return renderWithMockStore(<ConfigureParametersStepSummary {...props} />, {
    initialState: {
      workflowSetup: {
        elementMetadata: {
          elements: WorkflowMetadataAll
        }
      }
    } as AppState
  });
};

describe('ConfigureParametersStepSummary ', () => {
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => (() => {}) as any);
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
  });

  it('Should render ConfigureParametersStepSummary > action :NM168 CA', async () => {
    const mockFunction = jest.fn();

    const props = {
      formValues: {
        parameters: {
          transactionId: 'nm.168',
          parameters: [{ name: ParameterCode.Entry_168, value: 'CA' }]
        }
      },
      onEditStep: mockFunction
    };
    const wrapper = await renderComponent(props);
    const editButton = wrapper.getByText('txt_edit');
    expect(editButton).toBeInTheDocument();
    fireEvent.click(editButton);
    expect(mockFunction).toHaveBeenCalled();
  });

  it('Should render ConfigureParametersStepSummary > action :NM168 CP', async () => {
    const mockFunction = jest.fn();

    const props = {
      formValues: {
        parameters: {
          transactionId: 'nm.168',
          parameters: [{ name: ParameterCode.Entry_168, value: 'CP' }]
        }
      },
      onEditStep: mockFunction
    };
    const wrapper = await renderComponent(props);
    const editButton = wrapper.getByText('txt_edit');
    expect(editButton).toBeInTheDocument();
    fireEvent.click(editButton);
    expect(mockFunction).toHaveBeenCalled();
  });

  it('Should render ConfigureParametersStepSummary > action :NM169', async () => {
    const mockFunction = jest.fn();

    const props = {
      formValues: {
        parameters: {
          transactionId: 'nm.169'
        }
      },
      onEditStep: mockFunction
    };
    const wrapper = await renderComponent(props);
    const editButton = wrapper.getByText('txt_edit');
    expect(editButton).toBeInTheDocument();
    fireEvent.click(editButton);
    expect(mockFunction).toHaveBeenCalled();
  });

  it('Should render ConfigureParametersStepSummary > action :NM170', async () => {
    const mockFunction = jest.fn();

    const props = {
      formValues: {
        parameters: {
          transactionId: 'nm.170'
        }
      },
      onEditStep: mockFunction
    };
    const wrapper = await renderComponent(props);
    const editButton = wrapper.getByText('txt_edit');
    expect(editButton).toBeInTheDocument();
    fireEvent.click(editButton);
    expect(mockFunction).toHaveBeenCalled();
  });

  it('Should render ConfigureParametersStepSummary > action :NM738 01-31', async () => {
    const mockFunction = jest.fn();

    const props = {
      formValues: {
        parameters: {
          transactionId: 'nm.738',
          parameters: [{ name: ParameterCode.Entry_738, value: '01-31' }]
        }
      },
      onEditStep: mockFunction
    };
    const wrapper = await renderComponent(props);
    const editButton = wrapper.getByText('txt_edit');
    expect(editButton).toBeInTheDocument();
    fireEvent.click(editButton);
    expect(mockFunction).toHaveBeenCalled();
  });

  it('Should render ConfigureParametersStepSummary > action :NM738 60', async () => {
    const mockFunction = jest.fn();

    const props = {
      formValues: {
        parameters: {
          transactionId: 'nm.738',
          parameters: [{ name: ParameterCode.Entry_738, value: '60' }]
        }
      },
      onEditStep: mockFunction
    };
    const wrapper = await renderComponent(props);
    const editButton = wrapper.getByText('txt_edit');
    expect(editButton).toBeInTheDocument();
    fireEvent.click(editButton);
    expect(mockFunction).toHaveBeenCalled();
  });

  it('Should render ConfigureParametersStepSummary > default', async () => {
    const mockFunction = jest.fn();

    const props = {
      formValues: {},
      onEditStep: mockFunction
    };
    const wrapper = await renderComponent(props);
    const editButton = wrapper.getByText('txt_edit');
    expect(editButton).toBeInTheDocument();
    fireEvent.click(editButton);
    expect(mockFunction).toHaveBeenCalled();
  });

  it('Should render ConfigureParametersStepSummary > 169 reallocateLock true', async () => {
    const mockFunction = jest.fn();

    const props = {
      formValues: {
        parameters: {
          transactionId: 'nm.169',
          parameters: [
            { name: ParameterCode.ReallocateImmediately, value: true }
          ]
        }
      },
      onEditStep: mockFunction
    };
    const wrapper = await renderComponent(props);
    const editButton = wrapper.getByText('txt_edit');
    expect(editButton).toBeInTheDocument();
    fireEvent.click(editButton);
    expect(mockFunction).toHaveBeenCalled();
  });

  it('Should render ConfigureParametersStepSummary > 170 reallocateLock true', async () => {
    const mockFunction = jest.fn();

    const props = {
      formValues: {
        parameters: {
          transactionId: 'nm.170',
          parameters: [
            { name: ParameterCode.ReallocateImmediately, value: 'true' }
          ]
        }
      },
      onEditStep: mockFunction
    };
    const wrapper = await renderComponent(props);
    const editButton = wrapper.getByText('txt_edit');
    expect(editButton).toBeInTheDocument();
    fireEvent.click(editButton);
    expect(mockFunction).toHaveBeenCalled();
  });
});
