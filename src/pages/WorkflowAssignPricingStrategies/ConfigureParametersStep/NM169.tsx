import EnhanceDatePicker from 'app/components/EnhanceDatePicker';
import FieldTooltip from 'app/components/FieldTooltip';
import { isDevice, isValidDate } from 'app/helpers';
import {
  CheckBox,
  DatePickerChangeEvent,
  Icon,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import React from 'react';
import { ConfigureParametersStepProps } from '.';
import findParameter from './findParameter';
import { ParameterCode } from './parameterCodes';

interface NM169Props {
  value?: any;
  formValue?: ConfigureParametersStepProps;
  onchangeDate?: (event: DatePickerChangeEvent) => void;
  onchangeCheckBox?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  dateRange?: any;
  disabledRange?: any;
  tooltipDisabledDate?: any;
}
const NM169: React.FC<NM169Props> = ({
  formValue,
  onchangeDate,
  onchangeCheckBox,
  dateRange,
  disabledRange,
  tooltipDisabledDate
}) => {
  const { t } = useTranslation();

  const getParameterValue = (
    name: ParameterCode,
    { parseDate }: { parseDate?: boolean } = {}
  ) => {
    const value = findParameter(formValue?.parameters?.parameters, name)?.value;

    return parseDate
      ? isValidDate(value)
        ? new Date(value!)
        : undefined
      : value;
  };

  return (
    <div className="border-top mt-24 pt-24">
      <h5>{t('txt_assign_pricing_strategies_configuration_nm169_title')}</h5>
      <div className="row">
        <div className="col-lg-4 col-md-6 mt-16">
          <FieldTooltip
            placement={isDevice ? 'right-start' : 'left-start'}
            element={
              <div>
                <div className="text-uppercase">
                  {t(
                    'txt_assign_pricing_strategies_configuration_nm169_begin_date'
                  )}
                </div>
                <p className="mt-8">
                  {t(
                    'txt_assign_pricing_strategies_configuration_nm169_start_date_desc'
                  )}
                </p>
              </div>
            }
          >
            <EnhanceDatePicker
              id="newChange__169startDate"
              value={
                getParameterValue(ParameterCode.StartDate, {
                  parseDate: true
                }) as any
              }
              minDate={dateRange?.minStartDateLock}
              maxDate={dateRange?.maxStartDateLock}
              disabledRange={disabledRange}
              dateTooltip={tooltipDisabledDate}
              name={ParameterCode.StartDate}
              onChange={onchangeDate}
              label={t('txt_start_date')}
            />
          </FieldTooltip>
        </div>
        <div className="col-lg-4 col-md-6 mt-16">
          <FieldTooltip
            placement={isDevice ? 'right-start' : 'left-start'}
            element={
              <div>
                <div className="text-uppercase">
                  {t(
                    'txt_assign_pricing_strategies_configuration_nm169_end_date'
                  )}
                </div>
                <p className="mt-8">
                  {t(
                    'txt_assign_pricing_strategies_configuration_nm169_end_date_desc'
                  )}
                </p>
              </div>
            }
          >
            <EnhanceDatePicker
              id="newChange__169endDate"
              value={
                getParameterValue(ParameterCode.EndDate, {
                  parseDate: true
                }) as any
              }
              minDate={dateRange?.minEndDateLock}
              maxDate={dateRange?.maxEndDateLock}
              disabledRange={disabledRange}
              dateTooltip={tooltipDisabledDate}
              name={ParameterCode.EndDate}
              onChange={onchangeDate}
              label={t('txt_end_date')}
            />
          </FieldTooltip>
        </div>
      </div>

      <div className="d-flex align-items-center mt-16">
        <p className="color-grey mr-8 fw-600">
          {t(
            'txt_assign_pricing_strategies_configuration_nm169_reallocate_immediately'
          )}
        </p>
        <Tooltip
          element={
            <div className="my-8">
              <div className="text-uppercase">
                {t(
                  'txt_assign_pricing_strategies_configuration_nm169_reallocate'
                )}
              </div>
              <p className="mt-8">
                {t(
                  'txt_assign_pricing_strategies_configuration_nm169_reallocate_desc'
                )}
              </p>
            </div>
          }
          triggerClassName="d-flex"
        >
          <Icon name="information" size="5x" className="color-grey-l16" />
        </Tooltip>
      </div>
      <div className="d-flex mt-8">
        <CheckBox>
          <CheckBox.Input
            id={'ReallocateLock_id'}
            name={ParameterCode.ReallocateImmediately}
            disabled={false}
            checked={
              getParameterValue(ParameterCode.ReallocateImmediately) as any
            }
            onChange={onchangeCheckBox}
          />
          <CheckBox.Label>
            {t(
              'txt_assign_pricing_strategies_configuration_nm169_reallocate_label'
            )}
          </CheckBox.Label>
        </CheckBox>
      </div>
    </div>
  );
};

export default NM169;
