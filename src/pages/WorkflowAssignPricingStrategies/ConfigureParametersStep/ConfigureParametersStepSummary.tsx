import GroupTextControl from 'app/components/DofControl/GroupTextControl';
import { GroupTextFormat } from 'app/constants/enums';
import { Button, useTranslation } from 'app/_libraries/_dls';
import { isFunction } from 'app/_libraries/_dls/lodash';
import { useSelectElementMetadataForAssignPricingStrategies } from 'pages/_commons/redux/WorkflowSetup';
import React from 'react';
import { ConfigureParametersStepProps } from '.';
import findParameter from './findParameter';
import { ParameterCode } from './parameterCodes';

const ConfigureParametersStepSummary: React.FC<
  WorkflowSetupSummaryProps<ConfigureParametersStepProps>
> = ({ formValues, onEditStep }) => {
  const { t } = useTranslation();

  const {
    status,
    type,
    subtransactionCode,
    allocateImmediately,
    lock,
    lockUnlock,
    methodOverrideID,
    reallocateNM738,
    serviceSubjectSection
    // aQTable
  } = useSelectElementMetadataForAssignPricingStrategies();

  const handleEdit = () => {
    isFunction(onEditStep) && onEditStep();
  };

  const getParameterValue = (name: ParameterCode) =>
    findParameter(formValues?.parameters?.parameters, name)?.value;

  const getDropdownText = (name: ParameterCode, items: RefData[]) =>
    items?.find(i => i.code === getParameterValue(name))?.text;

  const action = formValues.parameters?.transactionId;
  const mn168Action = getParameterValue(ParameterCode.Entry_168);
  const mn738Action = getParameterValue(ParameterCode.Entry_738);

  let actionText = '';
  let transactionText = '';
  let entryFormat = '';

  switch (action) {
    case 'nm.168':
      actionText = t(
        'txt_assign_pricing_strategies_configuration_reallocate_strategy'
      );
      transactionText = t(
        'txt_assign_pricing_strategies_configuration_nm168_entry_title'
      );
      entryFormat =
        mn168Action === 'CA'
          ? t(
              'txt_assign_pricing_strategies_configuration_nm168_CA_entry_format'
            )
          : t(
              'txt_assign_pricing_strategies_configuration_nm168_CP_entry_format'
            );
      break;

    case 'nm.169':
      actionText = t(
        'txt_assign_pricing_strategies_configuration_lock_strategy'
      );
      transactionText = t(
        'txt_assign_pricing_strategies_configuration_nm169_entry_title'
      );
      entryFormat = '';
      break;

    case 'nm.170':
      actionText = t(
        'txt_assign_pricing_strategies_configuration_unlock_strategy'
      );
      transactionText = t(
        'txt_assign_pricing_strategies_configuration_nm170_entry_title'
      );
      entryFormat = '';
      break;

    case 'nm.738':
      actionText = t(
        'txt_assign_pricing_strategies_configuration_override_strategy'
      );
      transactionText = t(
        'txt_assign_pricing_strategies_configuration_nm738_entry_title'
      );
      entryFormat =
        mn738Action === '01-31'
          ? t(
              'txt_assign_pricing_strategies_configuration_nm738_0131_entry_format'
            )
          : t(
              'txt_assign_pricing_strategies_configuration_nm738_60_entry_format'
            );
      break;

    default:
      break;
  }

  return (
    <div className="position-relative">
      <div className="absolute-top-right mt-n26 mr-n8">
        <Button variant="outline-primary" size="sm" onClick={handleEdit}>
          {t('txt_edit')}
        </Button>
      </div>
      <div className="row">
        <div className="col-12 col-lg-3">
          <GroupTextControl
            id="summary_actionText"
            input={{ value: actionText } as any}
            meta={{} as any}
            label={t('txt_action')}
            options={{
              inline: false,
              lineTruncate: 2,
              ellipsisLessText: t('txt_less'),
              ellipsisMoreText: t('txt_more')
            }}
          />
        </div>
        <div className="col-12 col-lg-3">
          <GroupTextControl
            id="summary_transactionText"
            input={{ value: transactionText } as any}
            meta={{} as any}
            label={t('txt_transaction')}
            options={{
              inline: false,
              lineTruncate: 2,
              ellipsisLessText: t('txt_less'),
              ellipsisMoreText: t('txt_more')
            }}
          />
        </div>
        {action === 'nm.168' && (
          <div className="col-12 col-lg-3">
            <GroupTextControl
              id="summary_entryFormat"
              input={{ value: entryFormat } as any}
              meta={{} as any}
              label={t(
                'txt_assign_pricing_strategies_configuration_entry_format'
              )}
              options={{
                inline: false,
                lineTruncate: 2,
                ellipsisLessText: t('txt_less'),
                ellipsisMoreText: t('txt_more')
              }}
            />
          </div>
        )}

        {action === 'nm.738' && (
          <div className="col-12 col-lg-3">
            <GroupTextControl
              id="summary_entryFormat"
              input={{ value: entryFormat } as any}
              meta={{} as any}
              label={t(
                'txt_assign_pricing_strategies_configuration_subtraction_option'
              )}
              options={{
                inline: false,
                lineTruncate: 2,
                ellipsisLessText: t('txt_less'),
                ellipsisMoreText: t('txt_more')
              }}
            />
          </div>
        )}

        {action === 'nm.168' && mn168Action === 'CA' && (
          <div className="col-12 col-lg-3">
            <GroupTextControl
              id="summary_entryFormat"
              input={
                { value: getParameterValue(ParameterCode.PortfolioId) } as any
              }
              meta={{} as any}
              label={t('txt_pofolio_id')}
              options={{
                inline: false,
                lineTruncate: 2,
                ellipsisLessText: t('txt_less'),
                ellipsisMoreText: t('txt_more')
              }}
            />
          </div>
        )}

        {action === 'nm.168' && mn168Action === 'CP' && (
          <>
            <div className="col-12 col-lg-3">
              <GroupTextControl
                id="summary_entryFormat"
                input={
                  { value: getParameterValue(ParameterCode.StrategyId) } as any
                }
                meta={{} as any}
                label={t('txt_strategy')}
                options={{
                  inline: false,
                  lineTruncate: 2,
                  ellipsisLessText: t('txt_less'),
                  ellipsisMoreText: t('txt_more')
                }}
              />
            </div>
            <div className="col-12 col-lg-3">
              <GroupTextControl
                id="summary_entryFormat"
                input={
                  {
                    value: getDropdownText(ParameterCode.Status, status)
                  } as any
                }
                meta={{} as any}
                label={t('txt_status')}
                options={{
                  inline: false,
                  lineTruncate: 2,
                  ellipsisLessText: t('txt_less'),
                  ellipsisMoreText: t('txt_more')
                }}
              />
            </div>
            <div className="col-12 col-lg-3">
              <GroupTextControl
                id="summary_entryFormat"
                input={
                  { value: getDropdownText(ParameterCode.Type, type) } as any
                }
                meta={{} as any}
                label={t('txt_type')}
                options={{
                  inline: false,
                  lineTruncate: 2,
                  ellipsisLessText: t('txt_less'),
                  ellipsisMoreText: t('txt_more')
                }}
              />
            </div>
          </>
        )}

        {action === 'nm.169' && (
          <>
            <div className="col-12 col-lg-3">
              <GroupTextControl
                id="summary_startDate"
                input={
                  { value: getParameterValue(ParameterCode.StartDate) } as any
                }
                meta={{} as any}
                label={t('txt_start_date')}
                options={{
                  inline: false,
                  format: GroupTextFormat.Date,
                  lineTruncate: 2,
                  ellipsisLessText: t('txt_less'),
                  ellipsisMoreText: t('txt_more')
                }}
              />
            </div>
            <div className="col-12 col-lg-3">
              <GroupTextControl
                id="summary_endDate"
                input={
                  { value: getParameterValue(ParameterCode.EndDate) } as any
                }
                meta={{} as any}
                label={t('txt_end_date')}
                options={{
                  inline: false,
                  format: GroupTextFormat.Date,
                  lineTruncate: 2,
                  ellipsisLessText: t('txt_less'),
                  ellipsisMoreText: t('txt_more')
                }}
              />
            </div>
            <div className="col-12 col-lg-3">
              <GroupTextControl
                id="summary_reallocateImmediately"
                input={
                  {
                    value: getParameterValue(
                      ParameterCode.ReallocateImmediately
                    )
                      ? 'True'
                      : 'False'
                  } as any
                }
                meta={{} as any}
                label={t(
                  'txt_assign_pricing_strategies_configuration_nm169_reallocate_label'
                )}
                options={{
                  inline: false,
                  lineTruncate: 2,
                  ellipsisLessText: t('txt_less'),
                  ellipsisMoreText: t('txt_more')
                }}
              />
            </div>
          </>
        )}

        {action === 'nm.170' && (
          <>
            <div className="col-12 col-lg-3">
              <GroupTextControl
                id="summary_endDate"
                input={
                  { value: getParameterValue(ParameterCode.EndDate) } as any
                }
                meta={{} as any}
                label={t('txt_end_date')}
                options={{
                  inline: false,
                  format: GroupTextFormat.Date,
                  lineTruncate: 2,
                  ellipsisLessText: t('txt_less'),
                  ellipsisMoreText: t('txt_more')
                }}
              />
            </div>
            <div className="col-12 col-lg-3">
              <GroupTextControl
                id="summary_reallocateUnLock"
                input={
                  {
                    value: getParameterValue(
                      ParameterCode.ReallocateImmediately
                    )
                      ? 'True'
                      : 'False'
                  } as any
                }
                meta={{} as any}
                label={t(
                  'txt_assign_pricing_strategies_configuration_nm170_reallocate_label'
                )}
                options={{
                  inline: false,
                  lineTruncate: 2,
                  ellipsisLessText: t('txt_less'),
                  ellipsisMoreText: t('txt_more')
                }}
              />
            </div>
          </>
        )}

        {action === 'nm.738' && mn738Action === '01-31' && (
          <>
            <div className="col-12 col-lg-3">
              <GroupTextControl
                id="summary_subtransactionCode"
                input={
                  {
                    value: getDropdownText(
                      ParameterCode.SubtransactionCode,
                      subtransactionCode
                    )
                  } as any
                }
                meta={{} as any}
                label={t(
                  'txt_assign_pricing_strategies_configuration_nm738_0131_subtransaction_code'
                )}
                options={{
                  inline: false,
                  lineTruncate: 2,
                  ellipsisLessText: t('txt_less'),
                  ellipsisMoreText: t('txt_more')
                }}
              />
            </div>
            <div className="col-12 col-lg-3">
              <GroupTextControl
                id="summary_methodOverrideID"
                input={
                  {
                    value: getDropdownText(
                      ParameterCode.MethodOverrideId,
                      methodOverrideID
                    )
                  } as any
                }
                meta={{} as any}
                label={t(
                  'txt_assign_pricing_strategies_configuration_nm738_0131_method_override_id'
                )}
                options={{
                  inline: false,
                  lineTruncate: 2,
                  ellipsisLessText: t('txt_less'),
                  ellipsisMoreText: t('txt_more')
                }}
              />
            </div>
            <div className="col-12 col-lg-3">
              <GroupTextControl
                id="summary_overrideReasonCode"
                input={
                  {
                    value: getParameterValue(ParameterCode.OverrideReasonCode)
                  } as any
                }
                meta={{} as any}
                label={t(
                  'txt_assign_pricing_strategies_configuration_nm738_0131_override_reason_code'
                )}
                options={{
                  inline: false,
                  lineTruncate: 2,
                  ellipsisLessText: t('txt_less'),
                  ellipsisMoreText: t('txt_more')
                }}
              />
            </div>
            <div className="col-12 col-lg-3">
              <GroupTextControl
                id="summary_lockUnlock"
                input={
                  {
                    value: getDropdownText(ParameterCode.LockUnlock, lockUnlock)
                  } as any
                }
                meta={{} as any}
                label={t(
                  'txt_assign_pricing_strategies_configuration_nm738_0131_lock_unlock'
                )}
                options={{
                  inline: false,
                  lineTruncate: 2,
                  ellipsisLessText: t('txt_less'),
                  ellipsisMoreText: t('txt_more')
                }}
              />
            </div>
            <div className="col-12 col-lg-3">
              <GroupTextControl
                id="summary_allocateImmediately"
                input={
                  {
                    value: getDropdownText(
                      ParameterCode.AllocateImmediately,
                      allocateImmediately
                    )
                  } as any
                }
                meta={{} as any}
                label={t(
                  'txt_assign_pricing_strategies_configuration_nm738_0131_allocate_immediately'
                )}
                options={{
                  inline: false,
                  lineTruncate: 2,
                  ellipsisLessText: t('txt_less'),
                  ellipsisMoreText: t('txt_more')
                }}
              />
            </div>
            <div className="col-12 col-lg-3">
              <GroupTextControl
                id="summary_aQTable"
                input={
                  { value: getParameterValue(ParameterCode.AqTable) } as any
                }
                meta={{} as any}
                label={t(
                  'txt_assign_pricing_strategies_configuration_nm738_0131_aq_table'
                )}
                options={{
                  inline: false,
                  lineTruncate: 2,
                  ellipsisLessText: t('txt_less'),
                  ellipsisMoreText: t('txt_more')
                }}
              />
            </div>
          </>
        )}

        {action === 'nm.738' && mn738Action === '60' && (
          <>
            <div className="col-12 col-lg-3">
              <GroupTextControl
                id="summary_serviceSubjectSection"
                input={
                  {
                    value: getDropdownText(
                      ParameterCode.SVC_SubjectSection,
                      serviceSubjectSection
                    )
                  } as any
                }
                meta={{} as any}
                label={t(
                  'txt_assign_pricing_strategies_configuration_nm738_60_svc_subject_section'
                )}
                options={{
                  inline: false,
                  lineTruncate: 2,
                  ellipsisLessText: t('txt_less'),
                  ellipsisMoreText: t('txt_more')
                }}
              />
            </div>
            <div className="col-12 col-lg-3">
              <GroupTextControl
                id="summary_lock"
                input={
                  { value: getDropdownText(ParameterCode.Lock, lock) } as any
                }
                meta={{} as any}
                label={t('txt_assign_pricing_strategies_configuration_lock')}
                options={{
                  inline: false,
                  lineTruncate: 2,
                  ellipsisLessText: t('txt_less'),
                  ellipsisMoreText: t('txt_more')
                }}
              />
            </div>
            <div className="col-12 col-lg-3">
              <GroupTextControl
                id="summary_lockDate"
                input={
                  { value: getParameterValue(ParameterCode.LockDate) } as any
                }
                meta={{} as any}
                label={t(
                  'txt_assign_pricing_strategies_configuration_nm738_60_lock_date'
                )}
                options={{
                  inline: false,
                  format: GroupTextFormat.Date,
                  lineTruncate: 2,
                  ellipsisLessText: t('txt_less'),
                  ellipsisMoreText: t('txt_more')
                }}
              />
            </div>
            <div className="col-12 col-lg-3">
              <GroupTextControl
                id="summary_unlockDate"
                input={
                  { value: getParameterValue(ParameterCode.UnlockDate) } as any
                }
                meta={{} as any}
                label={t(
                  'txt_assign_pricing_strategies_configuration_nm738_60_unlock_date'
                )}
                options={{
                  inline: false,
                  format: GroupTextFormat.Date,
                  lineTruncate: 2,
                  ellipsisLessText: t('txt_less'),
                  ellipsisMoreText: t('txt_more')
                }}
              />
            </div>
            <div className="col-12 col-lg-3">
              <GroupTextControl
                id="summary_reallocate"
                input={
                  {
                    value: getDropdownText(
                      ParameterCode.Reallocate,
                      reallocateNM738
                    )
                  } as any
                }
                meta={{} as any}
                label={t(
                  'txt_assign_pricing_strategies_configuration_nm738_60_reallocate'
                )}
                options={{
                  inline: false,
                  lineTruncate: 2,
                  ellipsisLessText: t('txt_less'),
                  ellipsisMoreText: t('txt_more')
                }}
              />
            </div>
            <div className="col-12 col-lg-3">
              <GroupTextControl
                id="summary_reason"
                input={
                  { value: getParameterValue(ParameterCode.ReasonCode) } as any
                }
                meta={{} as any}
                label={t('txt_reason')}
                options={{
                  inline: false,
                  lineTruncate: 2,
                  ellipsisLessText: t('txt_less'),
                  ellipsisMoreText: t('txt_more')
                }}
              />
            </div>
          </>
        )}
      </div>
    </div>
  );
};

export default ConfigureParametersStepSummary;
