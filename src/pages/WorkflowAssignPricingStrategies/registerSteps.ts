import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { stepRegistry } from '../_commons/WorkflowSetup/registries';
import ConfigureParametersStep from './ConfigureParametersStep';
import GettingStartStep from './GettingStartStep';

stepRegistry.registerStep(
  'GetStartedAssignPricingStrategies',
  GettingStartStep
);
stepRegistry.registerStep(
  'ConfigureParametersAssignPricingStrategies',
  ConfigureParametersStep,
  [
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__ASSIGN_PRICING_STRATEGIES_CONFIGURE_PARAMETERS
  ]
);
