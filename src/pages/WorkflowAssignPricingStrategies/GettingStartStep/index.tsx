import { WORKFLOW_SETUP } from 'app/constants/local-storage';
import { Button, Icon, TransDLS, useTranslation } from 'app/_libraries/_dls';
import OverviewModal from 'pages/_commons/OverviewModal';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, { useRef, useState } from 'react';
import GettingStartSummary from './GettingStartSummary';

export interface GettingStartStepProps {
  isValid?: boolean;
  title?: any;

  alreadyShown?: boolean;
}

const GettingStartStep: React.FC<WorkflowSetupProps<GettingStartStepProps>> = ({
  formValues,
  setFormValues
}) => {
  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;

  const { t } = useTranslation();

  const [showOverview, setShowOverview] = useState(
    formValues.alreadyShown
      ? false
      : localStorage.getItem(WORKFLOW_SETUP.SHOW_OVERVIEW_AGAIN) !== 'false'
  );

  return (
    <React.Fragment>
      <div className="position-relative">
        <div className="absolute-top-right mt-n28">
          <Button
            className="mr-n8"
            variant="outline-primary"
            size="sm"
            onClick={() => setShowOverview(true)}
          >
            {t('txt_view_overview')}
          </Button>
        </div>
        <div className="row">
          <div className="col-12 col-md-6 mt-24">
            <div className="bg-light-l20 rounded-lg p-16">
              <div className="text-center">
                <Icon name="request" size="12x" className="color-grey-l16" />
              </div>
              <p className="mt-8 color-grey">
                {t('txt_assign_pricing_strategies_desc_1')}
              </p>
              <p className="mt-16 fw-600">
                {t('txt_assign_pricing_strategies_desc_2')}
              </p>
              <p className="mt-8 color-grey">
                {t('txt_assign_pricing_strategies_desc_3')}
              </p>
              <ul className="list-dot color-grey">
                <li>{t('txt_assign_pricing_strategies_desc_4')}</li>
                <li>{t('txt_assign_pricing_strategies_desc_5')}</li>
                <li>{t('txt_assign_pricing_strategies_desc_6')}</li>
              </ul>
            </div>
          </div>
          <div className="col-12 col-md-6 mt-24 color-grey">
            <p>{t('txt_assign_pricing_strategies_desc_7')}</p>
            <p className="mt-8">
              <TransDLS keyTranslation="txt_assign_pricing_strategies_desc_8">
                <strong className="color-grey-d20" />
              </TransDLS>
            </p>
            <p className="mt-8">
              <TransDLS keyTranslation="txt_assign_pricing_strategies_desc_9">
                <strong className="color-grey-d20" />
              </TransDLS>
            </p>
            <p className="mt-8">
              <TransDLS keyTranslation="txt_assign_pricing_strategies_desc_10">
                <strong className="color-grey-d20" />
              </TransDLS>
            </p>
            <p className="mt-8">
              <TransDLS keyTranslation="txt_assign_pricing_strategies_desc_11">
                <strong className="color-grey-d20" />
              </TransDLS>
            </p>
          </div>
        </div>
      </div>
      {showOverview && (
        <OverviewModal
          id="overviewWorkflowSetup"
          show
          onClose={() => {
            setShowOverview(false);
            keepRef.current.setFormValues({ alreadyShown: true });
          }}
        />
      )}
    </React.Fragment>
  );
};

const ExtraStaticGettingStartStep =
  GettingStartStep as WorkflowSetupStaticProp<GettingStartStepProps>;

ExtraStaticGettingStartStep.summaryComponent = GettingStartSummary;
ExtraStaticGettingStartStep.defaultValues = { isValid: true };

export default ExtraStaticGettingStartStep;
