import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { default as stepRegistry } from '../_commons/WorkflowSetup/registries/stepRegistry';

const registerFunc = jest.spyOn(stepRegistry, 'registerStep');

describe('AssignPricingStrategiesWorkFlow > registerSteps', () => {
  it('Should register components', async () => {
    await import('./registerSteps');

    expect(registerFunc).toBeCalledWith(
      'GetStartedAssignPricingStrategies',
      expect.anything()
    );

    expect(registerFunc).toBeCalledWith(
      'ConfigureParametersAssignPricingStrategies',
      expect.anything(),
      [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__ASSIGN_PRICING_STRATEGIES_CONFIGURE_PARAMETERS
      ]
    );
  });
});
