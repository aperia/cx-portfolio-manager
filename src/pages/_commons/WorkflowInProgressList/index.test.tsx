import { fireEvent, render, RenderResult } from '@testing-library/react';
import 'app/utils/_mockComponent/mockDofView';
import 'app/utils/_mockComponent/mockFailedApiReload';
import * as workflowHook from 'pages/_commons/redux/Workflow/select-hooks';
import React from 'react';
import * as reactRedux from 'react-redux';
import WorkflowInProgressList from './index';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');

const mockSelectWorkflowsFetchingStatus = jest.spyOn(
  workflowHook,
  'useSelectWorkflowsFetchingStatus'
);

describe('Test WorkflowInProgressList Component', () => {
  const mockDispatch = jest.fn();

  const workflowInProgressListProps = {
    workflows: []
  };

  const renderWorkflowInProgressList = (
    props?: Record<string, any>
  ): RenderResult => {
    return render(
      <div>
        <WorkflowInProgressList {...workflowInProgressListProps} {...props} />
      </div>
    );
  };

  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
    mockSelectWorkflowsFetchingStatus.mockReturnValue({
      loading: false,
      error: false
    });
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
    mockSelectWorkflowsFetchingStatus.mockClear();
  });

  it('should render WorkflowInProgressList > no data', () => {
    const wrapper = renderWorkflowInProgressList();

    expect(
      wrapper.getByText('txt_no_in_progress_workflows_to_display')
    ).toBeInTheDocument();
  });

  it('should render WorkflowInProgressList > has error', () => {
    mockSelectWorkflowsFetchingStatus.mockReturnValue({
      loading: false,
      error: true
    });
    const wrapper = renderWorkflowInProgressList();

    expect(wrapper.getByText('Data load unsuccessful')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('Reload'));
    expect(mockDispatch).toHaveBeenCalled();
  });

  it('should render WorkflowInProgressList > has data', () => {
    const wrapper = renderWorkflowInProgressList({
      workflows: [
        {
          id: 1,
          name: 'Mock workflow 1'
        },
        {
          id: 2,
          name: 'Mock workflow 2'
        }
      ]
    });

    expect(
      wrapper.baseElement.querySelectorAll('.workflow-in-progress-card-view')
        .length
    ).toEqual(2);

    const toggleBtn = wrapper.baseElement
      .querySelectorAll('.workflow-in-progress-card-view')[0]
      .querySelector('.btn-icon-secondary') as HTMLButtonElement;

    fireEvent.click(toggleBtn);

    expect(
      wrapper.getByText(
        'workflow-in-progress-card-view__workflow-in-progress-1'
      )
    ).toBeInTheDocument();
  });
});
