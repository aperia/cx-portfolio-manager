import FailedApi from 'app/components/FailedApiReload';
import NoDataFound from 'app/components/NoDataFound';
import WorkflowInProgressCardView from 'app/components/WorkflowInProgressCardView';
import { classnames } from 'app/helpers';
import { useTranslation } from 'app/_libraries/_dls';
import {
  actionsWorkflow,
  useSelectWorkflowsFetchingStatus
} from 'pages/_commons/redux/Workflow';
import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';

interface IProps {
  workflows: IWorkflowModel[];
  small?: boolean;
  containerClassName?: string;
}

const WorkflowInProgressList: React.FC<IProps> = ({
  workflows,
  small,
  containerClassName
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const [expandIds, setExpandIds] = useState<string[]>([]);

  const { loading, error } = useSelectWorkflowsFetchingStatus();

  useEffect(() => {
    dispatch(actionsWorkflow.getWorkflows());
  }, [dispatch]);

  const handleApiReload = useCallback(() => {
    dispatch(actionsWorkflow.getWorkflows());
  }, [dispatch]);

  const handleToggle = useCallback((workflowId: string) => {
    setExpandIds([workflowId]);
  }, []);

  return (
    <div
      className={classnames(
        {
          'loading mh-320': loading
        },
        containerClassName
      )}
    >
      {error ? (
        <FailedApi
          id="homepage-workflow-in-progress-card-error"
          onReload={handleApiReload}
          className="mt-40 pt-40 d-flex flex-column justify-content-center align-items-center"
        />
      ) : workflows?.length > 0 ? (
        workflows.map(item => (
          <div key={item.id} className="pt-4">
            <WorkflowInProgressCardView
              small={small}
              id={`workflow-in-progress-${item.id}`}
              value={item}
              isExpand={expandIds.includes(item.id)}
              onToggle={handleToggle}
            />
          </div>
        ))
      ) : (
        !loading && (
          <div className="mt-80 mb-24">
            <NoDataFound
              id="homepage-workflow-in-progress-not-found"
              className="my-40 py-40"
              title={t('txt_no_in_progress_workflows_to_display')}
            />
          </div>
        )
      )}
    </div>
  );
};

export default WorkflowInProgressList;
