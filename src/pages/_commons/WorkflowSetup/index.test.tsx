import { fireEvent } from '@testing-library/dom';
import { changeDetailLink } from 'app/constants/links';
import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { MockWorkflowTemplate } from 'app/fixtures/workflow-data';
import {
  mockActionCreator,
  mockUseDispatchFnc,
  renderComponent,
  wait
} from 'app/utils';
import * as reduxModal from 'pages/_commons/redux/modal';
import React from 'react';
import { act } from 'react-dom/test-utils';
import WorkflowSetup, { IWorkflowSetupProps } from '.';
import { actionsWorkflow } from '../redux/Workflow';
import { actionsWorkflowSetup } from '../redux/WorkflowSetup';
import * as reduxWorkflowSetup from '../redux/WorkflowSetup/select-hooks/_common';
import * as useWorkflowSetupBuilder from './useWorkflowSetupBuilder';
import * as useWorkflowSetupParser from './useWorkflowSetupParser';
import * as useWorkflowSetupStep from './useWorkflowSetupStep';

const useDispatchMock = mockUseDispatchFnc();
const dispatchFn = jest.fn();

jest.mock('../redux/Workflow', () => {
  const actual = jest.requireActual('../redux/Workflow');
  return {
    __esModule: true,
    ...actual,
    useSelectChangeWorkflowRemoval: () => ({})
  };
});

const actionsWorkflowMock: any = actionsWorkflow;

const mockHistory = {
  replace: jest.fn(),
  push: jest.fn(),
  location: { pathname: '/' }
};
jest.mock('react-router', () => ({
  useHistory: () => mockHistory,
  useLocation: () => ({}),
  useParams: () => ({})
}));

jest.mock('./stepDefinition', () => {
  return {
    __esModule: true,
    stepDefinition: {}
  };
});
jest.mock('./ProgressBar', () => ({ onSelectStep }: any) => {
  return <div onClick={() => onSelectStep('step1')}>Progress Bar</div>;
});
jest.mock(
  'app/components/ConfirmationModal',
  () =>
    ({ onCancel, onConfirm }: any) => {
      return (
        <div>
          <button onClick={onCancel}>Confirmation Modal Cancel</button>
          <button onClick={onConfirm}>Confirmation Modal Confirm</button>
        </div>
      );
    }
);

const mockOnConfirmParams = jest.fn();
jest.mock('app/hooks/useUnsavedChangesRedirect', () => {
  return {
    __esModule: true,
    useUnsavedChangesRedirect:
      () =>
      ({ onConfirm, onDiscard, onCancel }: any) => {
        onDiscard && onDiscard();
        onCancel && onCancel();
        onConfirm && onConfirm(mockOnConfirmParams());
      }
  };
});

// Spyon all hooks
const useWorkflowSetupBuilderMock = jest.spyOn(
  useWorkflowSetupBuilder,
  'useWorkflowSetupBuilder'
);
const useWorkflowSetupParserMock = jest.spyOn(
  useWorkflowSetupParser,
  'useWorkflowSetupParser'
);
const useWorkflowSetupStepMock = jest.spyOn(
  useWorkflowSetupStep,
  'useWorkflowSetupStep'
);
const useSelectCurrentOpenedModal = jest.spyOn(
  reduxModal,
  'useSelectCurrentOpenedModal'
);

// mock select hooks
const useSelectWorkflowSetupLoadingMock = jest.spyOn(
  reduxWorkflowSetup,
  'useSelectWorkflowSetupLoading'
);

const useWorkflowFormStepValidateMock = jest.spyOn(
  reduxWorkflowSetup,
  'useWorkflowFormStepValidate'
);

const useWorkflowFormStepsHaveBeenSavedMock = jest.spyOn(
  reduxWorkflowSetup,
  'useWorkflowFormStepsHaveBeenSaved'
);

const spyActionsWorkflowSetup = mockActionCreator(actionsWorkflowSetup);

beforeEach(() => {
  useDispatchMock.mockImplementation(() => dispatchFn);
  useSelectCurrentOpenedModal.mockReturnValue([true, true]);
  useWorkflowFormStepsHaveBeenSavedMock.mockReturnValue(true);
  mockOnConfirmParams.mockReturnValue('');
  mockHistory.location.pathname = '/';
});
afterEach(() => {
  jest.clearAllMocks();
});
describe('workflow setup', () => {
  const defaultMockProps: IWorkflowSetupProps = {
    workflow: MockWorkflowTemplate,
    onSubmitted: jest.fn()
  };

  const renderWorkflowSetup = async (mockProps?: IWorkflowSetupProps) => {
    const renderResult = await renderComponent(
      'workflowSetupTestId',
      <WorkflowSetup {...defaultMockProps} {...mockProps} />
    );
    fireEvent.click(renderResult.getByText(/txt_close/));
    return renderResult;
  };

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Render new workflow setup first step', async () => {
    // mock implementation
    useWorkflowSetupParserMock.mockReturnValue({
      info: {},
      stepsMapping: [],
      allStepsMapping: []
    });

    useWorkflowSetupStepMock.mockReturnValue({
      selectedStep: 'step1',
      lastStep: 'lastStep',
      handleSelectStep: jest.fn(),
      loading: true
    } as any);

    useWorkflowSetupBuilderMock.mockReturnValue({
      changeId: undefined,
      workflowInstanceId: undefined,
      isCompletedWorkflow: false,
      steps: [
        {
          id: 'step1',
          title: 'Step 1'
        },
        {
          id: 'step2',
          title: 'Step 2'
        },
        {
          id: 'step3',
          title: 'Step 3'
        },
        {
          id: 'step4',
          title: 'Step 4'
        }
      ] as any,
      menuSteps: [
        {
          clickable: true,
          dataApi: undefined,
          description: 'Returned Check Charges',
          error: undefined,
          id: 'step1',
          title: 'Step 1'
        },
        {
          clickable: true,
          dataApi: undefined,
          description: 'Returned Check Charges',
          error: undefined,
          id: 'step2',
          title: 'Step 2'
        },
        {
          clickable: true,
          dataApi: undefined,
          description: 'Returned Check Charges',
          error: undefined,
          id: 'step3',
          title: 'Step 3'
        },
        {
          clickable: true,
          dataApi: undefined,
          description: 'Returned Check Charges',
          error: undefined,
          id: 'step4',
          title: 'Step 4'
        }
      ],
      componentSteps: [
        {
          content: <div>content</div>,
          id: 'step1',
          title: 'Step 1',
          wrappedContent: true
        },
        {
          content: <div>content</div>,
          id: 'step2',
          title: 'Step 2'
        },
        {
          content: <div>content</div>,
          id: 'step3',
          title: 'Step 3'
        },
        {
          content: <div>content</div>,
          id: 'step4',
          title: 'Step 4'
        }
      ]
    });

    useWorkflowFormStepValidateMock.mockReturnValue(true);
    useSelectWorkflowSetupLoadingMock.mockReturnValue(false);

    // expect render properly
    const { getByText } = await renderWorkflowSetup();
    expect(getByText(/txt_title_set_up_workflow/)).toBeInTheDocument();

    // next step
    fireEvent.click(getByText(/txt_next_step/));

    // click progress bar
    fireEvent.click(getByText(/Progress Bar/));

    // close modal
    const mockSetWorkflowSetup = spyActionsWorkflowSetup('setWorkflowSetup');
    fireEvent.click(getByText(/txt_close/));
    expect(mockSetWorkflowSetup).toBeCalledTimes(2);
  });

  it('Delete workflow Success', async () => {
    // mock implementation
    useWorkflowSetupParserMock.mockReturnValue({
      info: {},
      stepsMapping: [],
      allStepsMapping: []
    });

    useWorkflowSetupStepMock.mockReturnValue({
      selectedStep: 'step3',
      lastStep: 'step4',
      handleSelectStep: jest.fn()
    } as any);

    useWorkflowSetupBuilderMock.mockReturnValue({
      changeId: 'changeId',
      workflowInstanceId: 'workflowIntstanceId',
      isCompletedWorkflow: false,
      steps: [
        {
          id: 'step1',
          title: 'Step 1'
        },
        {
          id: 'step2',
          title: 'Step 2'
        },
        {
          id: 'step3',
          title: 'Step 3'
        },
        {
          id: 'step4',
          title: 'Step 4'
        }
      ] as any,
      menuSteps: [
        {
          clickable: true,
          dataApi: undefined,
          description: 'Returned Check Charges',
          error: undefined,
          id: 'step1',
          title: 'Step 1'
        },
        {
          clickable: true,
          dataApi: undefined,
          description: 'Returned Check Charges',
          error: undefined,
          id: 'step2',
          title: 'Step 2'
        },
        {
          clickable: true,
          dataApi: undefined,
          description: 'Returned Check Charges',
          error: undefined,
          id: 'step3',
          title: 'Step 3'
        },
        {
          clickable: true,
          dataApi: undefined,
          description: 'Returned Check Charges',
          error: undefined,
          id: 'step4',
          title: 'Step 4'
        }
      ],
      componentSteps: [
        {
          content: <div>content</div>,
          id: 'step1',
          title: 'Step 1'
        },
        {
          content: <div>content</div>,
          id: 'step2',
          title: 'Step 2'
        },
        {
          content: <div>content</div>,
          id: 'step3',
          title: 'Step 3'
        },
        {
          content: <div>content</div>,
          id: 'step4',
          title: 'Step 4'
        }
      ]
    });

    useWorkflowFormStepValidateMock.mockReturnValue(true);
    useSelectWorkflowSetupLoadingMock.mockReturnValue(false);
    actionsWorkflowMock.removeWorkflow = () => {
      actionsWorkflowMock.removeWorkflow = {
        fulfilled: {
          match: () => true
        }
      };
    };

    const { getByText } = await renderWorkflowSetup({
      ...defaultMockProps,
      workflow: { ...MockWorkflowTemplate, name: '' }
    });
    fireEvent.click(getByText(/txt_back/));
    act(() => {
      fireEvent.click(getByText(/txt_delete_workflow/));
    });
    fireEvent.click(getByText(/Confirmation Modal Cancel/));
    act(() => {
      fireEvent.click(getByText(/txt_delete_workflow/));
    });
    fireEvent.click(getByText(/Confirmation Modal Confirm/));

    await wait();
    expect(getByText(/txt_title_set_up_workflow/)).toBeInTheDocument();
  });

  it('Delete workflow Failed', async () => {
    // mock implementation
    mockHistory.location.pathname = changeDetailLink('changeId');
    useWorkflowSetupParserMock.mockReturnValue({
      info: {},
      stepsMapping: [],
      allStepsMapping: []
    });

    useWorkflowSetupStepMock.mockReturnValue({
      selectedStep: 'step3',
      lastStep: 'step4',
      handleSave: jest.fn().mockResolvedValue(true),
      handleSelectStep: jest.fn()
    } as any);

    useWorkflowSetupBuilderMock.mockReturnValue({
      changeId: 'changeId',
      workflowInstanceId: 'workflowIntstanceId',
      isCompletedWorkflow: false,
      steps: [
        {
          id: 'step1',
          title: 'Step 1'
        },
        {
          id: 'step2',
          title: 'Step 2'
        },
        {
          id: 'step3',
          title: 'Step 3'
        },
        {
          id: 'step4',
          title: 'Step 4'
        }
      ] as any,
      menuSteps: [
        {
          clickable: true,
          dataApi: undefined,
          description: 'Returned Check Charges',
          error: undefined,
          id: 'step1',
          title: 'Step 1'
        },
        {
          clickable: true,
          dataApi: undefined,
          description: 'Returned Check Charges',
          error: undefined,
          id: 'step2',
          title: 'Step 2'
        },
        {
          clickable: true,
          dataApi: undefined,
          description: 'Returned Check Charges',
          error: undefined,
          id: 'step3',
          title: 'Step 3'
        },
        {
          clickable: true,
          dataApi: undefined,
          description: 'Returned Check Charges',
          error: undefined,
          id: 'step4',
          title: 'Step 4'
        }
      ],
      componentSteps: [
        {
          content: <div>content</div>,
          id: 'step1',
          title: 'Step 1'
        },
        {
          content: <div>content</div>,
          id: 'step2',
          title: 'Step 2'
        },
        {
          content: <div>content</div>,
          id: 'step3',
          title: 'Step 3'
        },
        {
          content: <div>content</div>,
          id: 'step4',
          title: 'Step 4'
        }
      ]
    });

    useWorkflowFormStepValidateMock.mockReturnValue(true);
    useSelectWorkflowSetupLoadingMock.mockReturnValue(false);
    actionsWorkflowMock.removeWorkflow = () => {
      actionsWorkflowMock.removeWorkflow = {
        fulfilled: {
          match: () => false
        }
      };
    };

    const { getByText } = await renderWorkflowSetup({
      ...defaultMockProps,
      workflow: { ...MockWorkflowTemplate, name: '' }
    });
    fireEvent.click(getByText(/txt_back/));
    fireEvent.click(getByText(/txt_save/));
    act(() => {
      fireEvent.click(getByText(/txt_delete_workflow/));
    });
    fireEvent.click(getByText(/Confirmation Modal Cancel/));
    act(() => {
      fireEvent.click(getByText(/txt_delete_workflow/));
    });
    fireEvent.click(getByText(/Confirmation Modal Confirm/));

    await wait();
    expect(getByText(/txt_title_set_up_workflow/)).toBeInTheDocument();
  });

  it('Last step and submit success', async () => {
    mockOnConfirmParams.mockReturnValue(
      UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__SELECT_A_CHANGE__EDIT_CHANGE
    );
    // mock implementation
    useWorkflowSetupParserMock.mockReturnValue({
      info: {},
      stepsMapping: [],
      allStepsMapping: []
    });

    useWorkflowSetupStepMock.mockReturnValue({
      selectedStep: 'step4',
      lastStep: 'step4',
      handleSelectStep: jest.fn(),
      handleStepApiSubmit: jest.fn().mockResolvedValue({ isSuccess: true })
    } as any);

    useWorkflowSetupBuilderMock.mockReturnValue({
      changeId: 'changeId',
      workflowInstanceId: 'workflowIntstanceId',
      isCompletedWorkflow: false,
      steps: [
        {
          id: 'step1',
          title: 'Step 1'
        },
        {
          id: 'step2',
          title: 'Step 2'
        },
        {
          id: 'step3',
          title: 'Step 3'
        },
        {
          id: 'step4',
          title: 'Step 4'
        }
      ] as any,
      menuSteps: [
        {
          clickable: true,
          dataApi: undefined,
          description: 'Returned Check Charges',
          error: undefined,
          id: 'step1',
          title: 'Step 1'
        },
        {
          clickable: true,
          dataApi: undefined,
          description: 'Returned Check Charges',
          error: undefined,
          id: 'step2',
          title: 'Step 2'
        },
        {
          clickable: true,
          dataApi: undefined,
          description: 'Returned Check Charges',
          error: undefined,
          id: 'step3',
          title: 'Step 3'
        },
        {
          clickable: true,
          dataApi: undefined,
          description: 'Returned Check Charges',
          error: undefined,
          id: 'step4',
          title: 'Step 4'
        }
      ],
      componentSteps: [
        {
          content: <div>content</div>,
          id: 'step1',
          title: 'Step 1'
        },
        {
          content: <div>content</div>,
          id: 'step2',
          title: 'Step 2'
        },
        {
          content: <div>content</div>,
          id: 'step3',
          title: 'Step 3'
        },
        {
          content: <div>content</div>,
          id: 'step4',
          title: 'Step 4'
        }
      ]
    });

    useWorkflowFormStepValidateMock.mockReturnValue(true);
    useSelectWorkflowSetupLoadingMock.mockReturnValue(false);

    const { getByText } = await renderWorkflowSetup();
    act(() => {
      fireEvent.click(getByText(/txt_submit/));
    });
    expect(getByText(/txt_title_set_up_workflow/)).toBeInTheDocument();
  });

  it('Last step and submit failed', async () => {
    mockOnConfirmParams.mockReturnValue(
      UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__SELECT_A_CHANGE__EDIT_CHANGE__EXISTED_WORKFLOW__CLOSE
    );
    // mock implementation
    useWorkflowSetupParserMock.mockReturnValue({
      info: {},
      stepsMapping: [],
      allStepsMapping: []
    });

    useWorkflowSetupStepMock.mockReturnValue({
      selectedStep: 'step4',
      lastStep: 'step4',
      handleSave: jest.fn().mockResolvedValue(true),
      handleSelectStep: jest.fn(),
      handleStepApiSubmit: jest.fn().mockResolvedValue({ isSuccess: false })
    } as any);

    useWorkflowSetupBuilderMock.mockReturnValue({
      changeId: 'changeId',
      workflowInstanceId: 'workflowIntstanceId',
      isCompletedWorkflow: false,
      steps: [
        {
          id: 'step1',
          title: 'Step 1'
        },
        {
          id: 'step2',
          title: 'Step 2'
        },
        {
          id: 'step3',
          title: 'Step 3'
        },
        {
          id: 'step4',
          title: 'Step 4'
        }
      ] as any,
      menuSteps: [
        {
          clickable: true,
          dataApi: undefined,
          description: 'Returned Check Charges',
          error: undefined,
          id: 'step1',
          title: 'Step 1'
        },
        {
          clickable: true,
          dataApi: undefined,
          description: 'Returned Check Charges',
          error: undefined,
          id: 'step2',
          title: 'Step 2'
        },
        {
          clickable: true,
          dataApi: undefined,
          description: 'Returned Check Charges',
          error: undefined,
          id: 'step3',
          title: 'Step 3'
        },
        {
          clickable: true,
          dataApi: undefined,
          description: 'Returned Check Charges',
          error: undefined,
          id: 'step4',
          title: 'Step 4'
        }
      ],
      componentSteps: [
        {
          content: <div>content</div>,
          id: 'step1',
          title: 'Step 1'
        },
        {
          content: <div>content</div>,
          id: 'step2',
          title: 'Step 2'
        },
        {
          content: <div>content</div>,
          id: 'step3',
          title: 'Step 3'
        },
        {
          content: <div>content</div>,
          id: 'step4',
          title: 'Step 4'
        }
      ]
    });

    useWorkflowFormStepValidateMock.mockReturnValue(true);
    useSelectWorkflowSetupLoadingMock.mockReturnValue(false);

    const { getByText } = await renderWorkflowSetup();
    act(() => {
      fireEvent.click(getByText(/txt_submit/));
    });
    expect(getByText(/txt_title_set_up_workflow/)).toBeInTheDocument();
  });

  it('Render edit workflow setup - submit success', async () => {
    // mock implementation
    useWorkflowSetupParserMock.mockReturnValue({
      info: { workflowInfo: 'workflow info' },
      stepsMapping: [],
      allStepsMapping: []
    });

    useWorkflowSetupStepMock.mockReturnValue({
      selectedStep: 'step4',
      lastStep: 'step4',
      handleSelectStep: jest.fn(),
      handleStepApiSubmit: jest.fn().mockResolvedValue({ isSuccess: true })
    } as any);

    useWorkflowSetupBuilderMock.mockReturnValue({
      changeId: 'changeId',
      workflowInstanceId: 'workflowIntstanceId',
      isCompletedWorkflow: false,
      steps: [
        {
          id: 'step1',
          title: 'Step 1'
        },
        {
          id: 'step2',
          title: 'Step 2'
        },
        {
          id: 'step3',
          title: 'Step 3'
        },
        {
          id: 'step4',
          title: 'Step 4'
        }
      ] as any,
      menuSteps: [
        {
          clickable: true,
          dataApi: undefined,
          description: 'Returned Check Charges',
          error: undefined,
          id: 'step1',
          title: 'Step 1'
        },
        {
          clickable: true,
          dataApi: undefined,
          description: 'Returned Check Charges',
          error: undefined,
          id: 'step2',
          title: 'Step 2'
        },
        {
          clickable: true,
          dataApi: undefined,
          description: 'Returned Check Charges',
          error: undefined,
          id: 'step3',
          title: 'Step 3'
        },
        {
          clickable: true,
          dataApi: undefined,
          description: 'Returned Check Charges',
          error: undefined,
          id: 'step4',
          title: 'Step 4'
        }
      ],
      componentSteps: [
        {
          content: <div>content</div>,
          id: 'step1',
          title: 'Step 1'
        },
        {
          content: <div>content</div>,
          id: 'step2',
          title: 'Step 2'
        },
        {
          content: <div>content</div>,
          id: 'step3',
          title: 'Step 3'
        },
        {
          content: <div>content</div>,
          id: 'step4',
          title: 'Step 4'
        }
      ]
    });

    useWorkflowFormStepValidateMock.mockReturnValue(true);
    useSelectWorkflowSetupLoadingMock.mockReturnValue(false);

    const { getByText } = await renderWorkflowSetup({
      ...defaultMockProps,
      isEdit: true,
      onSubmitted: jest.fn().mockReturnValue(true)
    });

    act(() => {
      fireEvent.click(getByText(/txt_submit/));
    });
    expect(getByText(/txt_title_edit_workflow/)).toBeInTheDocument();
  });
});
