import stepRegistry from './stepRegistry';

describe('stepRegistry', () => {
  it('has full data', () => {
    const stepId = 'stepId';
    const stepComponent = 'step component' as any;
    const unsaveChange = [] as string[];
    stepRegistry.registerStep(stepId, stepComponent, unsaveChange);
    expect(stepRegistry.stepsMap[stepId]).toEqual(stepComponent);
    expect(stepRegistry.unsavedChangeFormsMap[stepId]).toEqual(unsaveChange);

    stepRegistry.unregisterStep(stepId);
    expect(stepRegistry.stepsMap[stepId]).toEqual(undefined);
    expect(stepRegistry.unsavedChangeFormsMap[stepId]).toEqual(undefined);
  });

  it('has NOT full data', () => {
    const stepId = 'stepId';
    const stepComponent = 'step component' as any;
    stepRegistry.registerStep(stepId, stepComponent);
    expect(stepRegistry.stepsMap[stepId]).toEqual(stepComponent);
    expect(stepRegistry.unsavedChangeFormsMap[stepId]).toEqual([]);

    stepRegistry.unregisterStep(stepId);
    expect(stepRegistry.stepsMap[stepId]).toEqual(undefined);
    expect(stepRegistry.unsavedChangeFormsMap[stepId]).toEqual(undefined);
  });
});
