import {
  SubContentStepUploadMap,
  SubContentStepUploadRegistry
} from '../types';

class SubContentRegistryImpl implements SubContentStepUploadRegistry {
  readonly subContentsMap: SubContentStepUploadMap = {};

  registerComponent(
    subContentName: string,
    component: React.ElementType<WorkflowSetupProps<any, any>>
  ): void {
    this.subContentsMap[subContentName] = component;
  }

  get<T = {}, U = {}>(
    subContentName: string
  ): React.ElementType<WorkflowSetupProps<T, U>> {
    return this.subContentsMap[subContentName];
  }

  unregisterComponent(subContentName: string): void {
    delete this.subContentsMap[subContentName];
  }
}

const subContentStepUploadRegistry: SubContentStepUploadRegistry =
  new SubContentRegistryImpl();

export default subContentStepUploadRegistry;
