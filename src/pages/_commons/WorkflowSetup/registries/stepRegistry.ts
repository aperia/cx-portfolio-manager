import {
  StepRegistry,
  StepsMap,
  UnsavedChangeFormsMap,
  WorkflowSetupStaticProp
} from '../types';

class StepRegistryImpl implements StepRegistry {
  readonly stepsMap: StepsMap = {};
  readonly unsavedChangeFormsMap: UnsavedChangeFormsMap = {};

  registerStep(
    stepId: string,
    stepComponent: WorkflowSetupStaticProp,
    unsavedChangeForms: string[] = []
  ): void {
    this.stepsMap[stepId] = stepComponent;
    this.unsavedChangeFormsMap[stepId] = unsavedChangeForms;
  }

  unregisterStep(stepId: string): void {
    delete this.stepsMap[stepId];
    delete this.unsavedChangeFormsMap[stepId];
  }
}

const stepRegistry: StepRegistry = new StepRegistryImpl();

export default stepRegistry;
