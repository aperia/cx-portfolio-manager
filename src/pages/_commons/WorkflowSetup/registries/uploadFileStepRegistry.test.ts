import uploadFileStepRegistry from './uploadFileStepRegistry';

describe('uploadFileStepRegistry', () => {
  it('has full data', () => {
    const stepId = 'stepId';
    const stepComponent = 'step component' as any;
    uploadFileStepRegistry.registerComponent(stepId, stepComponent);
    expect(uploadFileStepRegistry.get(stepId)).toEqual(stepComponent);

    uploadFileStepRegistry.unregisterComponent(stepId);
    expect(uploadFileStepRegistry.get(stepId)).toEqual(undefined);
  });

  it('has NOT full data', () => {
    const stepId = 'stepId';
    const stepComponent = 'step component' as any;
    uploadFileStepRegistry.registerComponent(stepId, stepComponent);
    expect(uploadFileStepRegistry.get(stepId)).toEqual(stepComponent);

    uploadFileStepRegistry.unregisterComponent(stepId);
    expect(uploadFileStepRegistry.get(stepId)).toEqual(undefined);
  });
});
