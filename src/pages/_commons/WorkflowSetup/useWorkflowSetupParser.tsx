import { useTranslation } from 'app/_libraries/_dls';
import { differenceWith, isEmpty, orderBy } from 'lodash';
import isBoolean from 'lodash.isboolean';
import isUndefined from 'lodash.isundefined';
import {
  useSelectWorkflowInstance,
  useWorkflowFormStepsDependency,
  useWorkflowFormStepsDependencyNoCache
} from 'pages/_commons/redux/WorkflowSetup';
import { stepRegistry } from 'pages/_commons/WorkflowSetup/registries';
import { useEffect, useMemo, useRef, useState } from 'react';
import { WorkflowSetupLockedField, WorkflowSetupMapping } from './types';

const getConditionResult = (
  deps: Record<string, any>,
  condition: Condition
): boolean => {
  if (isBoolean(condition)) return condition;

  switch (condition.operator) {
    case 'and':
      return condition.childrens.every(c => getConditionResult(deps, c));
    case 'or':
      return condition.childrens.some(c => getConditionResult(deps, c));
  }

  const value = deps[condition.source];
  const targetValue = condition.target || deps[condition.targetPath!];

  switch (condition.operator) {
    case '<':
      return value < targetValue;
    case '>':
      return value > targetValue;
    case '<=':
      return value <= targetValue;
    case '>=':
      return value >= targetValue;
    case '=':
      return value === targetValue;
    case '!=':
      return value !== targetValue;
  }
};

const getConditionDependencies = (condition?: Condition): string[] => {
  if (isBoolean(condition) || isUndefined(condition)) return [];

  const operator = condition.operator;
  if (operator === 'and' || operator === 'or') {
    return [
      ...(condition as ConditionWrapper).childrens.reduceRight(
        (pre, cur) => [...pre, ...getConditionDependencies(cur)],
        [] as string[]
      )
    ];
  } else if (!!(condition as Comparation).source) {
    const comparation = condition as Comparation;
    const deps = [comparation.source];
    if (!!comparation.targetPath) deps.push(comparation.targetPath);
    return deps;
  }

  return [];
};

export const useWorkflowSetupParser = (
  stepsJson: WorkflowSetupConfig,
  isEdit: boolean
): {
  info: WorkflowSetupInfoConfig;
  stepsMapping: WorkflowSetupMapping[];
  allStepsMapping: WorkflowSetupMapping[];
  stepsNoCache: WorkflowSetupMapping[];
} => {
  const { t } = useTranslation();
  const jsonRef = useRef(stepsJson);
  const { steps: json, ...info } = useMemo(() => jsonRef.current, []);
  const workflowInstance = useSelectWorkflowInstance();
  const [isEditWorkflow] = useState(isEdit);

  const conditionDependencies = useMemo(() => {
    return json.reduceRight(
      (pre, curr) => [
        ...pre,
        ...getConditionDependencies(curr.condition).map(d => ({
          effectTo: curr.id,
          field: d
        }))
      ],
      [] as WorkflowSetupLockedField[]
    );
  }, [json]);

  const formConditionDependencies = useWorkflowFormStepsDependency(
    conditionDependencies.map(d => d.field)
  );
  const formConditionDependenciesNoCache =
    useWorkflowFormStepsDependencyNoCache(
      conditionDependencies.map(d => d.field)
    );

  const allStepsMapping = useMemo(
    () =>
      json
        .filter(
          s =>
            !s.envStep ||
            !!window?.account?.envWorkflowSteps?.includes(s.envStep)
        )
        .map(
          s =>
            ({
              ...s,
              title: t(s.title),
              lockedFields: conditionDependencies
                .filter(c => {
                  const path = c.field.split('.');
                  if (path.length !== 2) return false;

                  return path[0] === s.id;
                })
                .map(({ effectTo, field }) => ({
                  effectTo,
                  field: field.split('.')[1]
                })),
              dependencies: getConditionDependencies(s.condition),
              componentName: s.component,
              component: stepRegistry.stepsMap[s.component]
            } as WorkflowSetupMapping)
        ),
    [t, json, conditionDependencies]
  );

  const stepsOrder = useMemo<any>(
    () =>
      workflowInstance?.data?.workflowSetupData?.reduce(
        (pre, curr, idx) => ({
          ...pre,
          [curr.id]: idx
        }),
        {}
      ),
    [workflowInstance]
  );

  const filterStepsNoCache = useMemo(
    () =>
      orderBy(
        allStepsMapping
          .filter(
            ({ component, condition = true }) =>
              !!component &&
              getConditionResult(formConditionDependenciesNoCache, condition)
          )
          .map(s => ({ ...s, newOrder: stepsOrder?.[s.id] })),
        stepsOrder ? 'newOrder' : 'order',
        'asc'
      ),
    [allStepsMapping, formConditionDependenciesNoCache, stepsOrder]
  );
  const [stepsNoCache, setStepsNoCache] =
    useState<WorkflowSetupMapping[]>(filterStepsNoCache);
  const prevFilterStepsNoCacheStrRef = useRef(
    filterStepsNoCache.map(s => s.id).join(',')
  );
  useEffect(
    () => () => {
      prevFilterStepsNoCacheStrRef.current = filterStepsNoCache
        .map(s => s.id)
        .join(',');
    },
    [filterStepsNoCache, isEditWorkflow, workflowInstance]
  );
  useEffect(() => {
    if (isEditWorkflow && !workflowInstance) return;

    setStepsNoCache(preStepsMapping => {
      const filteredStepsStr = filterStepsNoCache.map(s => s.id).join(',');
      if (filteredStepsStr === prevFilterStepsNoCacheStrRef.current) {
        return preStepsMapping;
      }
      if (preStepsMapping.length <= 3) {
        return filterStepsNoCache;
      }

      const additionSteps = differenceWith(
        filterStepsNoCache,
        preStepsMapping,
        (o1, o2) => o1.id === o2.id
      );

      if (isEmpty(additionSteps)) {
        const removedSteps = differenceWith(
          preStepsMapping,
          filterStepsNoCache,
          (o1, o2) => o1.id === o2.id
        );

        return preStepsMapping.filter(
          s => !removedSteps.some(d => d.id === s.id)
        );
      }

      const filteredSteps = preStepsMapping.filter(s =>
        filterStepsNoCache.some(d => d.id === s.id)
      );

      const orders = filteredSteps.map(s => s.order);
      const stepOrderWithIdx = orders.indexOf(additionSteps[0].order! + 1);

      filteredSteps.splice(stepOrderWithIdx, 0, ...additionSteps);
      return filteredSteps;
    });
  }, [filterStepsNoCache, isEditWorkflow, workflowInstance]);

  const filterSteps = useMemo(
    () =>
      orderBy(
        allStepsMapping
          .filter(
            ({ component, condition = true }) =>
              !!component &&
              getConditionResult(formConditionDependencies, condition)
          )
          .map(s => ({ ...s, newOrder: stepsOrder?.[s.id] })),
        stepsOrder ? 'newOrder' : 'order',
        'asc'
      ),
    [allStepsMapping, formConditionDependencies, stepsOrder]
  );
  const prevFilterStepsStrRef = useRef(filterSteps.map(s => s.id).join(','));
  useEffect(
    () => () => {
      prevFilterStepsStrRef.current = filterSteps.map(s => s.id).join(',');
    },
    [filterSteps, isEditWorkflow, workflowInstance]
  );
  const [stepsMapping, setStepsMapping] =
    useState<WorkflowSetupMapping[]>(filterSteps);
  useEffect(() => {
    if (isEditWorkflow && !workflowInstance) return;

    setStepsMapping(preStepsMapping => {
      const filteredStepsStr = filterSteps.map(s => s.id).join(',');
      if (filteredStepsStr === prevFilterStepsStrRef.current) {
        return preStepsMapping;
      }
      if (preStepsMapping.length <= 3) {
        return filterSteps;
      }

      const additionSteps = differenceWith(
        filterSteps,
        preStepsMapping,
        (o1, o2) => o1.id === o2.id
      );

      if (isEmpty(additionSteps)) {
        const removedSteps = differenceWith(
          preStepsMapping,
          filterSteps,
          (o1, o2) => o1.id === o2.id
        );

        return preStepsMapping.filter(
          s => !removedSteps.some(d => d.id === s.id)
        );
      }

      const filteredSteps = preStepsMapping.filter(s =>
        filterSteps.some(d => d.id === s.id)
      );

      const orders = filteredSteps.map(s => s.order);
      const stepOrderWithIdx = orders.indexOf(additionSteps[0].order! + 1);

      filteredSteps.splice(stepOrderWithIdx, 0, ...additionSteps);
      return filteredSteps;
    });
  }, [filterSteps, isEditWorkflow, workflowInstance]);

  const result = useMemo(
    () => ({ stepsMapping, allStepsMapping, stepsNoCache, info }),
    [stepsMapping, allStepsMapping, stepsNoCache, info]
  );
  return result;
};
