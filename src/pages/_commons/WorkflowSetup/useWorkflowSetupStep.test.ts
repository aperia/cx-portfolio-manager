import { renderHook } from '@testing-library/react-hooks';
import * as useUnsavedChangesWatchingChange from 'app/hooks/useUnsavedChangesWatchingChange';
import { mockUseDispatchFnc } from 'app/utils';
import 'app/utils/_mockComponent/mockUseTranslation';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import { WorkflowSetupMapping, WorkflowSetupStaticProp } from './types';
import { useWorkflowSetupStep } from './useWorkflowSetupStep';

// dispatch
const useDispatchMock = mockUseDispatchFnc();

// useUnsavedChangesRedirect
jest.mock('app/hooks/useUnsavedChangesRedirect', () => {
  return {
    __esModule: true,
    useUnsavedChangesRedirect:
      () =>
      ({ onConfirm, onDiscard, onCancel }: any) => {
        onDiscard && onDiscard();
        onCancel && onCancel();
        onConfirm && onConfirm();
      }
  };
});

const mockLastStep = jest.fn();
const mockSelectedStep = jest.fn();
const mockUseWorkflowSetupForms = jest.fn();
// WorkflowSetup Select Hooks
jest.mock('pages/_commons/redux/WorkflowSetup', () => {
  const actualModule = jest.requireActual('pages/_commons/redux/WorkflowSetup');
  return {
    ...actualModule,
    useSelectWorkflowInstance: () => ({
      instance: {
        workflowInstanceDetails: {
          id: 'id'
        },
        data: {
          changeDetail: {}
        }
      }
    }),
    useLastWorkflowStep: () => mockLastStep(),
    useSelectedWorkflowStep: () => mockSelectedStep(),
    useWorkflowSetupForms: () => mockUseWorkflowSetupForms()
  };
});

// useUnsavedChangesWatchingChange
const useUnsavedChangesWatchingChangeMock = jest.spyOn(
  useUnsavedChangesWatchingChange,
  'useUnsavedChangesWatchingChange'
);

jest.mock('pages/_commons/WorkflowSetup/registries', () => {
  return {
    stepRegistry: {
      stepsMap: {
        step1: {},
        step2: {},
        step3: {}
      },
      unsavedChangeFormsMap: {
        component1: ['unsaveChanged'],
        component2: undefined
      }
    }
  };
});

const mockActionsWorkflowSetup = actionsWorkflowSetup as any;
const dispatchFn = jest.fn().mockReturnValue({ type: 'type' });
beforeEach(() => {
  jest.useFakeTimers();
  mockLastStep.mockReturnValue('step3');
  mockSelectedStep.mockReturnValue('step1');
  mockUseWorkflowSetupForms.mockReturnValue({});
  dispatchFn.mockReturnValue({ type: 'type' });
  useDispatchMock.mockImplementation(() => dispatchFn);
  useUnsavedChangesWatchingChangeMock.mockReturnValue(['unsaveChanged']);

  const failed = () => {
    mockActionsWorkflowSetup.failed = {
      fulfilled: {
        match: () => false
      }
    };

    return false;
  };
  const success = () => {
    mockActionsWorkflowSetup.success = {
      fulfilled: {
        match: () => true
      }
    };
    return true;
  };
  const noApiCalled = () => {
    mockActionsWorkflowSetup.noApiCalled = {
      fulfilled: {
        match: () => true
      }
    };
    return;
  };
  mockActionsWorkflowSetup.failed = failed;
  mockActionsWorkflowSetup.success = success;
  mockActionsWorkflowSetup.noApiCalled = noApiCalled;
});
afterEach(() => {
  jest.clearAllMocks();
  jest.useRealTimers();
});

describe('pages > _commons > WorkflowSetup > useWorkflowSetupStep', () => {
  const mockStep = (
    step: number,
    { isSummary = false, ...props }: Partial<WorkflowSetupStaticProp> = {},
    stepProps: Partial<WorkflowSetupMapping> = {}
  ) => {
    return {
      id: `step${step}`,
      title: `title${step}`,
      component: {
        ...props,
        stepValue: jest.fn(),
        stepError: jest.fn(),
        isSummary,
        summaryComponent: isSummary ? 'summary component' : undefined,
        defaultValues: { isValid: true }
      },
      componentName: `component${step}`,
      ...stepProps
    };
  };

  it('new workflow', async () => {
    mockUseWorkflowSetupForms.mockReturnValue({
      step1: { isValid: true },
      step2: { isValid: true },
      step3: { isValid: true },
      step4: { isValid: true }
    });
    // mock props and render
    const isDefaultValues = jest.fn();
    const parseFormValues = jest.fn();
    const workflowId = '1234';
    const stepsMapping: WorkflowSetupMapping[] = [
      mockStep(1, { isSummary: true }),
      mockStep(2, { isDefaultValues }),
      mockStep(3),
      mockStep(4)
    ] as any;
    const allStepsMapping: WorkflowSetupMapping[] = [
      mockStep(1, { parseFormValues, isSummary: true }),
      mockStep(2, { parseFormValues }),
      mockStep(3),
      mockStep(4)
    ] as any;
    const isEdit = false;
    const { result } = renderHook(() =>
      useWorkflowSetupStep(
        workflowId,
        stepsMapping,
        allStepsMapping,
        isEdit,
        stepsMapping
      )
    );
    jest.runAllTicks();

    const { loading, selectedStep, lastStep, handleSelectStep, handleSave } =
      result.current;

    handleSelectStep('step2');
    handleSelectStep('step2', true, false);
    handleSave();
    handleSave({ saveOnClose: true });

    expect(loading).toBe(false);
    expect(selectedStep).toBe('step1');
    expect(lastStep).toBe('step3');
    expect(handleSelectStep).toBeInstanceOf(Function);
    expect(handleSave).toBeInstanceOf(Function);
  });

  it('new workflow with wrong component name', async () => {
    mockUseWorkflowSetupForms.mockReturnValue({
      step1: { isValid: true },
      step2: { isValid: true },
      step3: { isValid: true }
    });
    // mock props and render
    const parseFormValues = jest.fn();
    const workflowId = '1234';
    const stepsMapping: WorkflowSetupMapping[] = [
      { ...mockStep(1, { isSummary: true }), componentName: undefined },
      mockStep(2, { defaultValues: { isValid: true } })
    ] as any;
    const allStepsMapping: WorkflowSetupMapping[] = [
      mockStep(1, { parseFormValues, isSummary: true }),
      mockStep(2, { parseFormValues }),
      mockStep(3)
    ] as any;
    const isEdit = false;
    const { result } = renderHook(() =>
      useWorkflowSetupStep(
        workflowId,
        stepsMapping,
        allStepsMapping,
        isEdit,
        stepsMapping
      )
    );
    jest.runAllTicks();

    const { loading, selectedStep, lastStep, handleSelectStep, handleSave } =
      result.current;

    handleSelectStep('step2');
    handleSelectStep('step2', true, false);
    handleSave();
    handleSave({ saveOnClose: true });

    expect(loading).toBe(false);
    expect(selectedStep).toBe('step1');
    expect(lastStep).toBe('step3');
    expect(handleSelectStep).toBeInstanceOf(Function);
    expect(handleSave).toBeInstanceOf(Function);
  });

  it('should save failed with new workflow', async () => {
    mockUseWorkflowSetupForms.mockReturnValue({
      step1: { isValid: true },
      step2: { isValid: true },
      step3: { isValid: true },
      step4: { isValid: true }
    });
    dispatchFn.mockReturnValue({ payload: true });
    // mock props and render
    const parseFormValues = jest.fn();
    const workflowId = '1234';
    const stepsMapping: WorkflowSetupMapping[] = [
      mockStep(1, { isSummary: true }, { dataApi: 'failed' }),
      mockStep(2),
      mockStep(3),
      mockStep(4)
    ] as any;
    const allStepsMapping: WorkflowSetupMapping[] = [
      mockStep(1, { parseFormValues, isSummary: true }, { dataApi: 'failed' }),
      mockStep(2, { parseFormValues }),
      mockStep(3),
      mockStep(4)
    ] as any;
    const isEdit = false;
    const { result } = renderHook(() =>
      useWorkflowSetupStep(
        workflowId,
        stepsMapping,
        allStepsMapping,
        isEdit,
        stepsMapping
      )
    );
    jest.runAllTicks();

    const { loading, selectedStep, lastStep, handleSelectStep, handleSave } =
      result.current;

    handleSelectStep('step2');
    handleSelectStep('step2', true, false);
    const failed = () => () => {
      mockActionsWorkflowSetup.failed = {
        fulfilled: {
          match: () => false
        }
      };

      return false;
    };
    mockActionsWorkflowSetup.failed = failed();
    handleSave();

    mockActionsWorkflowSetup.failed = failed();
    handleSave({ saveOnClose: true });

    expect(loading).toBe(false);
    expect(selectedStep).toBe('step1');
    expect(lastStep).toBe('step3');
    expect(handleSelectStep).toBeInstanceOf(Function);
    expect(handleSave).toBeInstanceOf(Function);
  });
  it('should save success with new workflow', async () => {
    mockUseWorkflowSetupForms.mockReturnValue({
      step1: { isValid: true },
      step2: { isValid: true },
      step3: { isValid: true }
    });
    dispatchFn.mockReturnValue({ payload: true });
    // mock props and render
    const parseFormValues = jest.fn();
    const workflowId = '1234';
    const stepsMapping: WorkflowSetupMapping[] = [
      mockStep(1, { isSummary: true }),
      mockStep(2)
    ] as any;
    const allStepsMapping: WorkflowSetupMapping[] = [
      mockStep(1, { parseFormValues, isSummary: true }),
      mockStep(2, { parseFormValues })
    ] as any;
    const isEdit = false;
    const { result } = renderHook(() =>
      useWorkflowSetupStep(
        workflowId,
        stepsMapping,
        allStepsMapping,
        isEdit,
        stepsMapping
      )
    );
    jest.runAllTicks();

    const { loading, selectedStep, lastStep, handleSelectStep, handleSave } =
      result.current;

    handleSelectStep('step2');
    handleSelectStep('step2', true, false);
    const success = () => () => {
      mockActionsWorkflowSetup.success = {
        fulfilled: {
          match: () => true
        }
      };
      return true;
    };
    mockActionsWorkflowSetup.success = success();
    handleSave();

    mockActionsWorkflowSetup.success = success();
    handleSave({ saveOnClose: true });

    mockActionsWorkflowSetup.success = success();
    handleSelectStep('step1');

    expect(loading).toBe(false);
    expect(selectedStep).toBe('step1');
    expect(lastStep).toBe('step3');
    expect(handleSelectStep).toBeInstanceOf(Function);
    expect(handleSave).toBeInstanceOf(Function);
  });

  it('should save success with new workflow when invalid form', async () => {
    dispatchFn.mockReturnValue({ payload: true });
    // mock props and render
    const isDefaultValues = jest.fn();
    const parseFormValues = jest.fn();
    const workflowId = '1234';
    const stepsMapping: WorkflowSetupMapping[] = [
      mockStep(0),
      mockStep(1, { isSummary: true }, { dataApi: 'success' }),
      mockStep(2),
      mockStep(3, { isDefaultValues })
    ] as any;
    const allStepsMapping: WorkflowSetupMapping[] = [
      mockStep(0, { parseFormValues }),
      mockStep(1, { parseFormValues, isSummary: true }, { dataApi: 'success' }),
      mockStep(2, { parseFormValues }),
      mockStep(3)
    ] as any;
    const isEdit = false;
    const { result } = renderHook(() =>
      useWorkflowSetupStep(
        workflowId,
        stepsMapping,
        allStepsMapping,
        isEdit,
        stepsMapping
      )
    );
    jest.runAllTicks();

    const { loading, selectedStep, lastStep, handleSelectStep, handleSave } =
      result.current;

    handleSelectStep('step2');
    handleSelectStep('step2', true, false);
    handleSave();

    const success = () => {
      mockActionsWorkflowSetup.success = {
        fulfilled: {
          match: () => true
        }
      };
      return true;
    };
    mockActionsWorkflowSetup.success = success;
    handleSave({ saveOnClose: true });

    mockActionsWorkflowSetup.success = success;
    handleSelectStep('step0');

    expect(loading).toBe(false);
    expect(selectedStep).toBe('step1');
    expect(lastStep).toBe('step3');
    expect(handleSelectStep).toBeInstanceOf(Function);
    expect(handleSave).toBeInstanceOf(Function);
  });

  it('should save success with new workflow when valid form', async () => {
    dispatchFn.mockReturnValue({ payload: true });
    mockUseWorkflowSetupForms.mockReturnValue({
      step0: { isValid: true },
      step1: { isValid: true },
      step2: { isValid: true },
      step3: { isValid: true }
    });
    // mock props and render
    const isDefaultValues = jest.fn();
    const parseFormValues = jest.fn();
    const workflowId = '1234';
    const stepsMapping: WorkflowSetupMapping[] = [
      mockStep(0),
      mockStep(1, { isSummary: true }, { dataApi: 'success' }),
      mockStep(2),
      mockStep(3, { isDefaultValues })
    ] as any;
    const allStepsMapping: WorkflowSetupMapping[] = [
      mockStep(0, { parseFormValues }),
      mockStep(1, { parseFormValues, isSummary: true }, { dataApi: 'success' }),
      mockStep(2, { parseFormValues }),
      mockStep(3)
    ] as any;
    const isEdit = false;
    const { result } = renderHook(() =>
      useWorkflowSetupStep(
        workflowId,
        stepsMapping,
        allStepsMapping,
        isEdit,
        stepsMapping
      )
    );
    jest.runAllTicks();

    const { loading, selectedStep, lastStep, handleSelectStep, handleSave } =
      result.current;

    handleSelectStep('step2');
    handleSelectStep('step2', true, false);
    handleSave();

    const success = () => {
      mockActionsWorkflowSetup.success = {
        fulfilled: {
          match: () => true
        }
      };
      return true;
    };
    mockActionsWorkflowSetup.success = success;
    handleSave({ saveOnClose: true });

    mockActionsWorkflowSetup.success = success;
    handleSelectStep('step0');

    expect(loading).toBe(false);
    expect(selectedStep).toBe('step1');
    expect(lastStep).toBe('step3');
    expect(handleSelectStep).toBeInstanceOf(Function);
    expect(handleSave).toBeInstanceOf(Function);
  });

  it('should save success with new workflow when invalid pervious step', async () => {
    dispatchFn.mockReturnValue({ payload: true });
    mockSelectedStep.mockReturnValue('step2');
    mockUseWorkflowSetupForms.mockReturnValue({
      step0: { isValid: false },
      step1: { isValid: true },
      step2: { isValid: true },
      step3: { isValid: true }
    });
    // mock props and render
    const parseFormValues = jest.fn();
    const workflowId = '1234';
    const stepsMapping: WorkflowSetupMapping[] = [
      mockStep(0),
      mockStep(1, { isSummary: true }),
      mockStep(2, {}, { dataApi: 'success' }),
      mockStep(3)
    ] as any;
    const allStepsMapping: WorkflowSetupMapping[] = [
      mockStep(0, { parseFormValues }),
      mockStep(1, { parseFormValues, isSummary: true }, { dataApi: 'success' }),
      mockStep(2, { parseFormValues }),
      mockStep(3)
    ] as any;
    const isEdit = false;
    const { result } = renderHook(() =>
      useWorkflowSetupStep(
        workflowId,
        stepsMapping,
        allStepsMapping,
        isEdit,
        stepsMapping
      )
    );
    jest.runAllTicks();

    const { loading, selectedStep, lastStep, handleSelectStep, handleSave } =
      result.current;

    handleSelectStep('step1');

    expect(loading).toBe(false);
    expect(selectedStep).toBe('step2');
    expect(lastStep).toBe('step3');
    expect(handleSelectStep).toBeInstanceOf(Function);
    expect(handleSave).toBeInstanceOf(Function);
  });

  it('should save success with new workflow and without apiCalled', async () => {
    mockUseWorkflowSetupForms.mockReturnValue({
      step1: { isValid: false },
      step2: { isValid: true },
      step3: { isValid: true }
    });
    // mock props and render
    const parseFormValues = jest.fn();
    const workflowId = '1234';
    const stepsMapping: WorkflowSetupMapping[] = [
      mockStep(1, { isSummary: true }, { dataApi: 'noApiCalled' }),
      mockStep(2, { defaultValues: { isValid: true } })
    ] as any;
    const allStepsMapping: WorkflowSetupMapping[] = [
      mockStep(1, { parseFormValues, isSummary: true }),
      mockStep(2, { parseFormValues, defaultValues: { isValid: true } })
    ] as any;
    const isEdit = false;
    const { result } = renderHook(() =>
      useWorkflowSetupStep(
        workflowId,
        stepsMapping,
        allStepsMapping,
        isEdit,
        stepsMapping
      )
    );
    jest.runAllTicks();

    const { loading, selectedStep, lastStep, handleSelectStep, handleSave } =
      result.current;

    handleSelectStep('step2');
    handleSelectStep('step2', true, false);
    handleSave();
    handleSave({ saveOnClose: true });

    expect(loading).toBe(false);
    expect(selectedStep).toBe('step1');
    expect(lastStep).toBe('step3');
    expect(handleSelectStep).toBeInstanceOf(Function);
    expect(handleSave).toBeInstanceOf(Function);
  });

  it('should save error with edit workflow', async () => {
    dispatchFn.mockReturnValue({ payload: true });
    // mock props and render
    const parseFormValues = jest
      .fn()
      .mockReturnValueOnce({ isSelected: true, isPass: true });
    const workflowId = '1234';
    const stepsMapping: WorkflowSetupMapping[] = [
      mockStep(1, { isSummary: true }, { dataApi: 'failed' }),
      mockStep(2),
      mockStep(3),
      mockStep(4)
    ] as any;
    const allStepsMapping: WorkflowSetupMapping[] = [
      mockStep(1, { parseFormValues, isSummary: true }),
      mockStep(2, { parseFormValues }),
      mockStep(3, { parseFormValues }),
      mockStep(4, { parseFormValues })
    ] as any;
    const isEdit = true;
    const { result } = renderHook(() =>
      useWorkflowSetupStep(
        workflowId,
        stepsMapping,
        allStepsMapping,
        isEdit,
        stepsMapping
      )
    );
    jest.runAllTicks();

    const { loading, selectedStep, lastStep, handleSelectStep, handleSave } =
      result.current;

    expect(loading).toBe(true);
    expect(selectedStep).toBe('step1');
    expect(lastStep).toBe('step3');
    expect(handleSelectStep).toBeInstanceOf(Function);
    expect(handleSave).toBeInstanceOf(Function);
  });

  it('should save success with edit workflow and without api called', async () => {
    // mock props and render
    const parseFormValues = jest
      .fn()
      .mockReturnValue({ isSelected: true, isPass: true, isValid: true });
    const workflowId = '1234';
    const stepsMapping: WorkflowSetupMapping[] = [
      mockStep(1, { isSummary: true }, { dataApi: 'noApiCalled' }),
      mockStep(2, { parseFormValues }),
      mockStep(3, { parseFormValues }),
      mockStep(4)
    ] as any;
    const allStepsMapping: WorkflowSetupMapping[] = [
      mockStep(1, { parseFormValues, isSummary: true }),
      mockStep(2, { parseFormValues }),
      mockStep(3),
      mockStep(4)
    ] as any;
    const isEdit = true;
    const { result } = renderHook(() =>
      useWorkflowSetupStep(
        workflowId,
        stepsMapping,
        allStepsMapping,
        isEdit,
        stepsMapping
      )
    );
    jest.runAllTicks();

    const { loading, selectedStep, lastStep, handleSelectStep, handleSave } =
      result.current;

    expect(loading).toBe(true);
    expect(selectedStep).toBe('step1');
    expect(lastStep).toBe('step3');
    expect(handleSelectStep).toBeInstanceOf(Function);
    expect(handleSave).toBeInstanceOf(Function);
  });

  it('should save success with edit workflow', async () => {
    dispatchFn.mockReturnValue({ payload: true });
    // mock props and render
    const parseFormValues = jest
      .fn()
      .mockReturnValueOnce({ isSelected: true, isPass: true });
    const workflowId = '1234';
    const stepsMapping: WorkflowSetupMapping[] = [
      mockStep(1, { isSummary: true }, { dataApi: 'success' }),
      mockStep(2),
      mockStep(3),
      mockStep(4)
    ] as any;
    const allStepsMapping: WorkflowSetupMapping[] = [
      mockStep(1, { parseFormValues, isSummary: true }, { dataApi: 'success' }),
      mockStep(2, { parseFormValues }),
      mockStep(3),
      mockStep(4)
    ] as any;
    const isEdit = true;
    const { result } = renderHook(() =>
      useWorkflowSetupStep(
        workflowId,
        stepsMapping,
        allStepsMapping,
        isEdit,
        stepsMapping
      )
    );
    jest.runAllTicks();

    const { loading, selectedStep, lastStep, handleSelectStep, handleSave } =
      result.current;

    expect(loading).toBe(true);
    expect(selectedStep).toBe('step1');
    expect(lastStep).toBe('step3');
    expect(handleSelectStep).toBeInstanceOf(Function);
    expect(handleSave).toBeInstanceOf(Function);
  });

  it('should move to step before if selected step not found', async () => {
    mockSelectedStep.mockReturnValue('step2');
    // mock props and render
    const parseFormValues = jest
      .fn()
      .mockReturnValueOnce({ isSelected: true, isPass: false });
    const workflowId = '1234';
    const stepsMapping: WorkflowSetupMapping[] = [
      mockStep(1, { parseFormValues }),
      mockStep(2)
    ] as any;
    const allStepsMapping: WorkflowSetupMapping[] = [
      mockStep(1, { parseFormValues }),
      mockStep(2),
      mockStep(3)
    ] as any;
    const { rerender, result } = renderHook(
      ({ workflowId, stepsMapping, allStepsMapping }: any) =>
        useWorkflowSetupStep(
          workflowId,
          stepsMapping,
          allStepsMapping,
          true,
          stepsMapping
        ),
      { initialProps: { workflowId, stepsMapping, allStepsMapping } }
    );
    jest.runAllTicks();

    rerender({
      workflowId,
      stepsMapping: [mockStep(1, { parseFormValues })] as any,
      allStepsMapping
    });
    jest.runAllTicks();

    const { selectedStep, lastStep } = result.current;

    expect(selectedStep).toBe('step2');
    expect(lastStep).toBe('step3');
  });

  it('should check invalid step', async () => {
    mockSelectedStep.mockReturnValue('step2');
    // mock props and render
    const parseFormValues = jest
      .fn()
      .mockReturnValueOnce({ isSelected: true, isPass: true, isValid: true });
    const workflowId = '1234';
    const stepsMapping: WorkflowSetupMapping[] = [
      mockStep(1, { parseFormValues }),
      mockStep(2),
      mockStep(3)
    ] as any;
    const allStepsMapping: WorkflowSetupMapping[] = [
      mockStep(1, { parseFormValues }),
      mockStep(2),
      mockStep(3),
      mockStep(4)
    ] as any;
    const { rerender, result } = renderHook(
      ({ workflowId, stepsMapping, allStepsMapping }: any) =>
        useWorkflowSetupStep(
          workflowId,
          stepsMapping,
          allStepsMapping,
          true,
          stepsMapping
        ),
      { initialProps: { workflowId, stepsMapping, allStepsMapping } }
    );
    jest.runAllTicks();
    jest.runAllTimers();

    rerender({
      workflowId,
      stepsMapping: allStepsMapping,
      allStepsMapping
    });
    jest.runAllTicks();

    mockUseWorkflowSetupForms.mockReturnValue({
      step1: { isValid: true },
      step2: { isValid: true },
      step3: { isValid: false },
      step4: { isValid: false }
    });

    rerender({
      workflowId,
      stepsMapping: allStepsMapping,
      allStepsMapping
    });
    jest.runAllTicks();

    mockUseWorkflowSetupForms.mockReturnValue({
      step1: { isValid: true },
      step2: { isValid: true },
      step3: { isValid: true },
      step4: { isValid: true }
    });

    rerender({
      workflowId,
      stepsMapping: allStepsMapping,
      allStepsMapping
    });
    jest.runAllTicks();

    const { selectedStep, lastStep } = result.current;

    expect(selectedStep).toBe('step2');
    expect(lastStep).toBe('step3');
  });

  it('should check invalid step and move last step', async () => {
    // mock props and render
    const isDefaultValues = jest.fn().mockReturnValue(true);
    const parseFormValues = jest
      .fn()
      .mockReturnValueOnce({ isSelected: true, isPass: true, isValid: true });
    const workflowId = '1234';
    const stepsMapping: WorkflowSetupMapping[] = [
      mockStep(0),
      mockStep(1, { parseFormValues }),
      mockStep(2),
      mockStep(3, { isDefaultValues, defaultValues: {} })
    ] as any;
    const allStepsMapping: WorkflowSetupMapping[] = [
      mockStep(0),
      mockStep(1, { parseFormValues }),
      mockStep(2),
      mockStep(3, { isDefaultValues }),
      mockStep(4)
    ] as any;

    mockUseWorkflowSetupForms.mockReturnValue({
      step0: { isValid: true },
      step1: { isValid: true },
      step2: { isValid: true },
      step3: { isValid: true },
      step4: { isValid: true }
    });
    const { rerender, result } = renderHook(
      ({ workflowId, stepsMapping, allStepsMapping }: any) =>
        useWorkflowSetupStep(
          workflowId,
          stepsMapping,
          allStepsMapping,
          true,
          stepsMapping
        ),
      { initialProps: { workflowId, stepsMapping, allStepsMapping } }
    );
    jest.runAllTicks();
    jest.runAllTimers();

    rerender({
      workflowId,
      stepsMapping: allStepsMapping,
      allStepsMapping
    });
    jest.runAllTicks();

    rerender({
      workflowId,
      stepsMapping,
      allStepsMapping
    });
    jest.runAllTicks();

    mockLastStep.mockReturnValue('step5');
    rerender({
      workflowId,
      stepsMapping: allStepsMapping,
      allStepsMapping
    });
    jest.runAllTicks();

    mockUseWorkflowSetupForms.mockReturnValue({
      step0: { isValid: true },
      step1: { isValid: true },
      step2: { isValid: true },
      step3: { isValid: false },
      step4: { isValid: false }
    });
    isDefaultValues.mockReturnValue(false);

    rerender({
      workflowId,
      stepsMapping,
      allStepsMapping
    });
    jest.runAllTicks();

    mockLastStep.mockReturnValue('step3');
    isDefaultValues.mockReturnValue(true);
    mockUseWorkflowSetupForms.mockReturnValue({
      step0: { isValid: true },
      step1: { isValid: false },
      step2: { isValid: true },
      step3: { isValid: true },
      step4: { isValid: true },
      step5: { isValid: true }
    });

    rerender({
      workflowId,
      stepsMapping: [
        mockStep(0),
        mockStep(2),
        mockStep(3),
        mockStep(4, { parseFormValues }),
        mockStep(5)
      ] as any,
      allStepsMapping
    });
    jest.runAllTicks();

    mockUseWorkflowSetupForms.mockReturnValue({
      step0: { isValid: true },
      step1: { isValid: true },
      step2: { isValid: true },
      step3: { isValid: false },
      step4: { isValid: false }
    });

    rerender({
      workflowId,
      stepsMapping,
      allStepsMapping
    });
    jest.runAllTicks();

    const { selectedStep, lastStep } = result.current;

    expect(selectedStep).toBe('step1');
    expect(lastStep).toBe('step3');
  });

  it('should check invalid step and move last step to invalid step', async () => {
    // mock props and render
    const isDefaultValues = jest.fn().mockReturnValue(true);
    const parseFormValues = jest
      .fn()
      .mockReturnValueOnce({ isSelected: true, isPass: true, isValid: true });
    const workflowId = '1234';
    const stepsMapping: WorkflowSetupMapping[] = [
      mockStep(0),
      mockStep(1, { parseFormValues }),
      mockStep(2),
      mockStep(3, { isDefaultValues, defaultValues: {} })
    ] as any;
    const allStepsMapping: WorkflowSetupMapping[] = [
      mockStep(0),
      mockStep(1, { parseFormValues }),
      mockStep(2),
      mockStep(3, { isDefaultValues }),
      mockStep(4)
    ] as any;

    mockUseWorkflowSetupForms.mockReturnValue({
      step0: { isValid: true },
      step1: { isValid: true },
      step2: { isValid: true },
      step3: { isValid: true },
      step4: { isValid: true }
    });
    const { rerender, result } = renderHook(
      ({ workflowId, stepsMapping, allStepsMapping }: any) =>
        useWorkflowSetupStep(
          workflowId,
          stepsMapping,
          allStepsMapping,
          true,
          stepsMapping
        ),
      { initialProps: { workflowId, stepsMapping, allStepsMapping } }
    );
    jest.runAllTicks();
    jest.runAllTimers();

    rerender({
      workflowId,
      stepsMapping: allStepsMapping,
      allStepsMapping
    });
    jest.runAllTicks();

    rerender({
      workflowId,
      stepsMapping,
      allStepsMapping
    });
    jest.runAllTicks();

    mockLastStep.mockReturnValue('step5');
    rerender({
      workflowId,
      stepsMapping: allStepsMapping,
      allStepsMapping
    });
    jest.runAllTicks();

    mockUseWorkflowSetupForms.mockReturnValue({
      step0: { isValid: true },
      step1: { isValid: true },
      step2: { isValid: true },
      step3: { isValid: false },
      step4: { isValid: false }
    });

    rerender({
      workflowId,
      stepsMapping,
      allStepsMapping
    });
    jest.runAllTicks();

    mockLastStep.mockReturnValue('step3');
    mockUseWorkflowSetupForms.mockReturnValue({
      step0: { isValid: true },
      step1: { isValid: false },
      step2: { isValid: true },
      step3: { isValid: true },
      step4: { isValid: true },
      step5: { isValid: true }
    });

    rerender({
      workflowId,
      stepsMapping: [
        mockStep(0),
        mockStep(2),
        mockStep(3),
        mockStep(4, { parseFormValues }),
        mockStep(5)
      ] as any,
      allStepsMapping
    });
    jest.runAllTicks();

    mockUseWorkflowSetupForms.mockReturnValue({
      step0: { isValid: true },
      step1: { isValid: true },
      step2: { isValid: true },
      step3: { isValid: false },
      step4: { isValid: false }
    });

    rerender({
      workflowId,
      stepsMapping,
      allStepsMapping
    });
    jest.runAllTicks();

    const { selectedStep, lastStep } = result.current;

    expect(selectedStep).toBe('step1');
    expect(lastStep).toBe('step3');
  });

  it('should stuck at invalid step', async () => {
    // mock props and render
    const parseFormValues = jest
      .fn()
      .mockReturnValueOnce({ isSelected: true, isPass: true });
    mockSelectedStep.mockReturnValue('step5');
    const workflowId = '1234';
    mockUseWorkflowSetupForms.mockReturnValue({
      step1: { isValid: true },
      step2: { isValid: false },
      step3: { isValid: true },
      step4: { isValid: true }
    });
    const stepsMapping: WorkflowSetupMapping[] = [
      mockStep(1, { parseFormValues }),
      mockStep(2),
      mockStep(3),
      mockStep(4)
    ] as any;
    const allStepsMapping: WorkflowSetupMapping[] = [
      mockStep(1, { parseFormValues }),
      mockStep(2),
      mockStep(3),
      mockStep(4)
    ] as any;
    const { result } = renderHook(() =>
      useWorkflowSetupStep(
        workflowId,
        stepsMapping,
        allStepsMapping,
        true,
        stepsMapping
      )
    );

    const { selectedStep, lastStep, handleSelectStep } = result.current;
    handleSelectStep('step3', false, false);

    expect(selectedStep).toBe('step5');
    expect(lastStep).toBe('step3');
  });
});
