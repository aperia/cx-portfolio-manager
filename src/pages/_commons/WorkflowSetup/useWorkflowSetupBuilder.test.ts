import { renderHook } from '@testing-library/react-hooks';
import * as parseDependencies from 'app/helpers/parseDependencies';
import * as workflowSetupRedux from '../redux/WorkflowSetup/select-hooks/_common';
import { WorkflowSetupMapping } from './types';
import { useWorkflowSetupBuilder } from './useWorkflowSetupBuilder';

const useWorkflowSetupStepTrackingMock = jest.spyOn(
  workflowSetupRedux,
  'useWorkflowSetupStepTracking'
);

const usePrevInvalidSavedWorkflowSetupFormsMock = jest.spyOn(
  workflowSetupRedux,
  'usePrevInvalidSavedWorkflowSetupForms'
);
const usePrevSavedWorkflowSetupFormsMock = jest.spyOn(
  workflowSetupRedux,
  'usePrevSavedWorkflowSetupForms'
);
const useWorkflowInstanceIdMock = jest.spyOn(
  workflowSetupRedux,
  'useWorkflowInstanceId'
);

const parseDependenciesMock = jest.spyOn(
  parseDependencies,
  'parseDependencies'
);

const t = (value: string) => {
  return value;
};
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

describe('pages > _commons > WorkflowSetup > useWorkflowSetupBuilder', () => {
  afterAll(() => {
    jest.clearAllMocks();
  });

  beforeEach(() => {
    usePrevInvalidSavedWorkflowSetupFormsMock.mockReturnValue({
      step1: { isValid: true }
    });
    useWorkflowSetupStepTrackingMock.mockReturnValue({
      step1: { doneValidation: false, inprogress: true }
    });
    usePrevSavedWorkflowSetupFormsMock.mockReturnValue({});
    useWorkflowInstanceIdMock.mockReturnValue({});
    parseDependenciesMock.mockReturnValue({});
  });

  it('full data', () => {
    const stepsMapping: WorkflowSetupMapping[] = [
      {
        id: 'step1',
        title: 'title1',
        component: {
          stepValue: jest.fn(),
          stepError: jest.fn(),
          isSummary: true
        },
        componentName: 'component1'
      },
      {
        id: 'step2',
        title: 'title2',
        component: { summaryComponent: 'summary component' },
        componentName: 'component2'
      },
      {
        id: 'step3',
        title: 'title3',
        component: 'component3',
        componentName: 'component3'
      }
    ] as any;

    const selectedStepId = 'step1';
    const lastStepId = 'step2';
    const handleSelectStep = jest.fn();
    const moveLastStep = jest.fn();
    const { result } = renderHook(() =>
      useWorkflowSetupBuilder({
        selectedStepId,
        lastStepId,
        stepsMapping,
        handleSelectStep,
        moveLastStep
      })
    );
    expect(result.current.steps).toHaveLength(3);
  });

  it('without summary, stepValue, stepError', () => {
    const stepsMapping: WorkflowSetupMapping[] = [
      {
        id: 'step1',
        title: 'title1',
        component: 'component1',
        componentName: 'component1'
      },
      {
        id: 'step2',
        title: 'title2',
        component: 'component2',
        componentName: 'component2'
      },
      {
        id: 'step3',
        title: 'title3',
        component: 'component3',
        componentName: 'component3'
      }
    ] as any;

    const selectedStepId = 'step1';
    const lastStepId = 'step2';
    const handleSelectStep = jest.fn();
    const moveLastStep = jest.fn();
    const { result } = renderHook(() =>
      useWorkflowSetupBuilder({
        selectedStepId,
        lastStepId,
        stepsMapping,
        handleSelectStep,
        moveLastStep
      })
    );
    expect(result.current.steps).toHaveLength(3);
  });

  it('return only 1 item of step 1', () => {
    const stepsMapping: WorkflowSetupMapping[] = [
      {
        id: 'step1',
        title: 'title1',
        component: 'component1',
        componentName: 'component1'
      },
      {
        id: 'step2',
        title: 'title2',
        component: 'component2',
        componentName: 'component2'
      },
      {
        id: 'step3',
        title: 'title3',
        component: 'component3',
        componentName: 'component3'
      }
    ] as any;

    const selectedStepId = 'step1';
    const lastStepId = 'step1';
    const handleSelectStep = jest.fn();
    const moveLastStep = jest.fn();
    const { result } = renderHook(() =>
      useWorkflowSetupBuilder({
        selectedStepId,
        lastStepId,
        stepsMapping,
        handleSelectStep,
        moveLastStep
      })
    );
    expect(result.current.steps).toHaveLength(3);
    expect(result.current.menuSteps).toHaveLength(1);
  });
});
