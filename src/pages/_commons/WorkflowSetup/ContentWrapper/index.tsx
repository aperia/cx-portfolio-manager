import {
  addSuffixFormName,
  confirmEditStepProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { useUnsavedChangeRegistry } from 'app/hooks';
import { isFunction } from 'lodash';
import {
  actionsWorkflowSetup,
  useCurrentWorkflowSetup,
  useSelectedWorkflowStep,
  useWorkflowFormStep,
  useWorkflowFormStepsSavedAt,
  useWorkflowFormStepsStuck
} from 'pages/_commons/redux/WorkflowSetup';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { batch, useDispatch } from 'react-redux';
import { WorkflowSetupLockedField, WorkflowSetupStaticProp } from '../types';
import useInprogressStep from './useInprogressStep';

export interface IProps {
  title: string;
  content: WorkflowSetupStaticProp;
  id: string;
  defaultValues?: Record<string, any>;
  lockedFields?: WorkflowSetupLockedField[];
  dependencies?: Record<string, any>;
  summaries?: WorkflowSetupSummary[];
  hasInstance?: boolean;
  ready?: boolean;

  handleSelectStep?: (stepId: string, moveNextLastStep?: boolean) => void;
  moveLastStep?: (stepId: string) => void;
  handleSave?: (options?: SaveOptions) => void;
}
const ContentWrapper: React.FC<IProps> = ({
  title,
  id,
  lockedFields = [],
  dependencies,
  content: Content,
  summaries = [],
  hasInstance,
  ready = true,
  defaultValues = {},

  handleSelectStep,
  moveLastStep,
  handleSave,
  ...props
}) => {
  const dispatch = useDispatch();

  const keepRef = useRef({ firstTimeSetLockedFields: false });
  const formValues = useWorkflowFormStep(id);
  const selectedStep = useSelectedWorkflowStep();
  const workflow = useCurrentWorkflowSetup();
  const savedAt = useWorkflowFormStepsSavedAt();
  const isStuck = useWorkflowFormStepsStuck(id);

  const isEdit = useMemo(
    () => !!(workflow as IWorkflowModel)?.workflowTemplateId,
    [workflow]
  );

  useInprogressStep(id, ready, Content.isEqualFormValues);

  const clearFormValues = useCallback(
    id => {
      batch(() => {
        dispatch(
          actionsWorkflowSetup.clearWorkflowFormStep({
            step: id,

            values: defaultValues[id]
          })
        );

        dispatch(
          actionsWorkflowSetup.setSavedAt({
            isStuck: { [id]: false }
          })
        );
      });
    },

    [dispatch, defaultValues]
  );

  const setCurrentFormValues = useCallback(
    values => {
      dispatch(
        actionsWorkflowSetup.updateWorkflowFormStep({ name: id, values })
      );
    },
    [dispatch, id]
  );

  useEffect(() => {
    if (formValues) return;

    setCurrentFormValues(Content.defaultValues);
  }, [setCurrentFormValues, Content.defaultValues, formValues]);

  const getLockedFieldsValue = useCallback(
    () =>
      lockedFields.reduceRight((pre, curr) => {
        pre[curr.field] = formValues?.[curr.field];
        return pre;
      }, {} as any),
    [lockedFields, formValues]
  );
  const [lockedFieldsValue, setLockedFieldsValue] = useState(
    getLockedFieldsValue()
  );
  useEffect(() => {
    if (
      lockedFields.length <= 0 ||
      keepRef.current.firstTimeSetLockedFields ||
      selectedStep !== id
    ) {
      keepRef.current.firstTimeSetLockedFields = true;
      return;
    }

    return () => {
      setLockedFieldsValue(getLockedFieldsValue());
    };
  }, [getLockedFieldsValue, lockedFields, id, selectedStep, formValues]);

  const handleConfirmMoveStep = useCallback(() => {
    setLockedFieldsValue(getLockedFieldsValue());
    const dependeceLockedFields = lockedFields.filter(
      f => formValues?.[f.field] !== lockedFieldsValue?.[f.field]
    );
    dependeceLockedFields
      .reduceRight((pre, curr) => {
        if (!pre.includes(curr.effectTo)) {
          pre.push(curr.effectTo);
        }
        return pre;
      }, [] as string[])
      .forEach(clearFormValues);
  }, [
    getLockedFieldsValue,
    clearFormValues,
    lockedFields,
    formValues,
    lockedFieldsValue
  ]);

  const handleSaveInBackground = () => {
    isFunction(handleSave) && handleSave({ saveInBackground: true });
  };

  useUnsavedChangeRegistry(
    {
      ...confirmEditStepProps,
      formName: addSuffixFormName(
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__LOCKED_STEP_BY_DEPENDENCIES,
        id
      ),
      priority: 1,
      confirmFirst: true
    },
    [
      selectedStep === id &&
        !!hasInstance &&
        !!formValues?.isValid &&
        lockedFields.some(
          f => formValues?.[f.field] !== lockedFieldsValue?.[f.field]
        )
    ],
    handleConfirmMoveStep
  );

  if (!formValues) return <React.Fragment />;

  if (Content.isSummary)
    return (
      <Content
        {...props}
        workflow={workflow}
        title={title}
        stepId={id}
        selectedStep={selectedStep}
        savedAt={savedAt}
        hasInstance={hasInstance}
        summaries={summaries}
        formValues={formValues}
        isStuck={isStuck}
        ready={ready}
        isEdit={isEdit}
        setFormValues={setCurrentFormValues}
        clearFormValues={clearFormValues}
        handleSelectStep={handleSelectStep}
        handleSave={handleSaveInBackground}
      />
    );

  return (
    <Content
      {...props}
      workflow={workflow}
      title={title}
      stepId={id}
      hasInstance={hasInstance}
      selectedStep={selectedStep}
      savedAt={savedAt}
      dependencies={dependencies}
      formValues={formValues}
      isStuck={isStuck}
      ready={ready}
      isEdit={isEdit}
      clearFormValues={clearFormValues}
      setFormValues={setCurrentFormValues}
      handleSave={handleSaveInBackground}
    />
  );
};

export default ContentWrapper;
