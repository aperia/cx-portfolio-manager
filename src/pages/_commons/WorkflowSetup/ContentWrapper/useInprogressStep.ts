import { deepEqual } from 'app/helpers';
import { WorkflowSetupStepForm } from 'app/types';
import { isFunction, isUndefined } from 'lodash';
import {
  actionsWorkflowSetup,
  useSelectedWorkflowStep,
  useWorkflowFormStep,
  useWorkflowFormStepsStuck,
  useWorkflowSetupStepTracking
} from 'pages/_commons/redux/WorkflowSetup';
import { useEffect, useMemo, useRef } from 'react';
import { batch, useDispatch } from 'react-redux';

const useInprogressStep = (
  stepId: string,
  ready: boolean,
  isEqualFormValues?: (
    f1: WorkflowSetupStepForm,
    f2: WorkflowSetupStepForm
  ) => boolean
) => {
  const dispatch = useDispatch();
  const formValues = useWorkflowFormStep(stepId);
  const isStuck = useWorkflowFormStepsStuck(stepId);

  const keepRef = useRef({ stepId, formValues, prevFormValues: formValues });
  keepRef.current.stepId = stepId;
  keepRef.current.formValues = formValues;

  const selectedStep = useSelectedWorkflowStep();
  const stepTracking = useWorkflowSetupStepTracking();
  const doneValidation = useMemo(
    () => stepTracking[keepRef.current.stepId]?.doneValidation || 0,
    [stepTracking]
  );

  useEffect(() => {
    if (doneValidation <= 0) return;
    const { formValues: _formValues } = keepRef.current;

    keepRef.current.prevFormValues = _formValues;
  }, [doneValidation]);
  useEffect(() => {
    if (!ready) return;

    keepRef.current.prevFormValues = keepRef.current.formValues;
  }, [ready]);

  useEffect(() => {
    const {
      stepId: _stepId,
      formValues: _formValues,
      prevFormValues
    } = keepRef.current;

    if (isUndefined(prevFormValues) || !ready) return;

    batch(() => {
      dispatch(
        actionsWorkflowSetup.setStepTrackingError({
          stepId: _stepId,
          error: !prevFormValues?.isValid
        })
      );

      (isFunction(isEqualFormValues)
        ? !isEqualFormValues(prevFormValues, _formValues)
        : !deepEqual(prevFormValues, _formValues)) &&
        dispatch(
          actionsWorkflowSetup.setStepTrackingInprogress({
            stepId: _stepId,
            inprogress: !isStuck
          })
        );
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch, selectedStep, isStuck]);
};

export default useInprogressStep;
