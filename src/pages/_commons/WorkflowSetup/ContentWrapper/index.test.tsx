import { fireEvent } from '@testing-library/dom';
import { mockActionCreator, renderWithMockStore } from 'app/utils';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import * as selectHooks from 'pages/_commons/redux/WorkflowSetup/select-hooks/_common';
import React from 'react';
import ContentWrapper, { IProps } from './index';

// Mock dls
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    __esModule: true,
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

// Select hooks
const useSelectedWorkflowStepMock = jest.spyOn(
  selectHooks,
  'useSelectedWorkflowStep'
);
const useWorkflowFormStepsStuckMock = jest.spyOn(
  selectHooks,
  'useWorkflowFormStepsStuck'
);
const mockActionsWorkflowSetup = mockActionCreator(actionsWorkflowSetup);

// mock custom hooks
let confirmFunc: any;
jest.mock('app/hooks/useUnsavedChangeRegistry', () => {
  return {
    __esModule: true,
    useUnsavedChangeRegistry: (options: any, changes: any, onConfirm: any) => {
      confirmFunc = onConfirm;
    }
  };
});

beforeEach(() => {
  useSelectedWorkflowStepMock.mockReturnValue('step1');
  useWorkflowFormStepsStuckMock.mockReturnValue(false);
});

afterAll(() => {
  jest.clearAllMocks();
});

describe('Content Wrapper', () => {
  const mockGetValues = jest.fn();
  const Content: React.FC<any> = ({ setFormValues, handleSave }) => {
    const handleSetValue = () => {
      const values = mockGetValues();
      setFormValues(values);
    };

    const saveInBackground = () => {
      handleSave();
    };

    return (
      <div>
        <div>content</div>
        <button onClick={handleSetValue}>content_setValues</button>
        <button onClick={saveInBackground}>content_saveInBackground</button>
      </div>
    );
  };
  const ExtraStaticSummaryStep = Content as any;
  ExtraStaticSummaryStep.isSummary = true;
  const moveLastStep = jest.fn();
  const defaultMockProps: IProps = {
    title: 'title',
    content: ExtraStaticSummaryStep,
    id: 'step1',
    dependencies: {},
    handleSelectStep: jest.fn(),
    handleSave: jest.fn(),
    moveLastStep: moveLastStep
  };
  const renderContentWrapper = async (
    mockProps?: IProps,
    forms: any = {},
    savedSuccess = false
  ) => {
    const component = (
      <div>
        <ContentWrapper {...defaultMockProps} {...mockProps} />
        <div onClick={() => confirmFunc(savedSuccess)}>Confirm</div>
      </div>
    );
    const renderResult = await renderWithMockStore(component, {
      initialState: {
        workflowSetup: { workflowFormSteps: { forms, stepTracking: {} } }
      }
    });
    return renderResult;
  };

  it('render component is summary', async () => {
    // Render component properly
    const { getByText } = await renderContentWrapper(
      {
        ...defaultMockProps,
        lockedFields: [
          { effectTo: 'step1', field: 'field1' },
          { effectTo: 'step2', field: 'field2' }
        ],
        summaries: [],
        content: ExtraStaticSummaryStep
      },
      {
        step1: {
          field1: 'field1',
          field2: 'field2',
          isValid: true
        }
      },
      true
    );
    expect(getByText('content')).toBeInTheDocument();

    fireEvent.click(getByText('Confirm'));
    //expect(moveLastStep).toBeCalled();
  });

  it('render component NOT is summary', async () => {
    const ContentNotSummary: React.FC = () => <div>content</div>;

    // Render component properly
    const { getByText } = await renderContentWrapper({
      ...defaultMockProps,
      content: ContentNotSummary as any
    });
    expect(getByText('content')).toBeInTheDocument();
  });

  it('should clear locked form fields when confirm move step', async () => {
    const clearWorkflowFormStep = mockActionsWorkflowSetup(
      'clearWorkflowFormStep'
    );

    const { getByText } = await renderContentWrapper(
      {
        ...defaultMockProps,
        hasInstance: true,
        lockedFields: [
          { effectTo: 'step2', field: 'field1' },
          { effectTo: 'step2', field: 'field1' }
        ],
        content: ExtraStaticSummaryStep
      },
      {
        step1: {
          field1: 'field1',
          field2: 'field2',
          isValid: true
        }
      }
    );
    mockGetValues.mockReturnValue({ field1: 'change data' });
    fireEvent.click(getByText('content_setValues')); //

    fireEvent.click(getByText('Confirm'));

    fireEvent.click(getByText('content_saveInBackground'));
    expect(clearWorkflowFormStep).toBeCalledWith({
      step: 'step2',
      values: undefined
    });
  });
});
