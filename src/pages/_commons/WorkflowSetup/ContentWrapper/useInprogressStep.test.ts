import { renderHook } from '@testing-library/react-hooks';
import { mockThunkAction, mockUseDispatchFnc } from 'app/utils';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import * as selectHooks from 'pages/_commons/redux/WorkflowSetup/select-hooks/_common';
import useInprogressStep from './useInprogressStep';

const useSelectedWorkflowStepMock = jest.spyOn(
  selectHooks,
  'useSelectedWorkflowStep'
);
const useWorkflowFormStepMock = jest.spyOn(selectHooks, 'useWorkflowFormStep');
const useWorkflowSetupStepTrackingMock = jest.spyOn(
  selectHooks,
  'useWorkflowSetupStepTracking'
);
const useWorkflowFormStepsStuckMock = jest.spyOn(
  selectHooks,
  'useWorkflowFormStepsStuck'
);

const mockActionsWorkflowSetup = mockThunkAction(actionsWorkflowSetup);

describe('Content Wrapper > useInprogressStep', () => {
  beforeEach(() => {
    mockUseDispatchFnc().mockReturnValue(() => ({} as any));

    useSelectedWorkflowStepMock.mockReturnValue('step1');
    useWorkflowFormStepMock.mockReturnValue({});
    useWorkflowSetupStepTrackingMock.mockReturnValue({
      step1: { doneValidation: -1 }
    });
    useWorkflowFormStepsStuckMock.mockReturnValue(false);

    mockActionsWorkflowSetup('setStepTrackingInprogress');
  });

  it('should run setStepTrackingInprogress when click next step', async () => {
    const setStepTrackingInprogress = mockActionsWorkflowSetup(
      'setStepTrackingInprogress'
    );
    const setStepTrackingError = mockActionsWorkflowSetup(
      'setStepTrackingError'
    );
    useWorkflowFormStepMock.mockReturnValue(undefined as any);

    const { rerender } = renderHook((ready = true) =>
      useInprogressStep('step1', ready as boolean)
    );

    useWorkflowSetupStepTrackingMock.mockReturnValue({
      step1: { doneValidation: 1000 }
    });
    rerender(false);
    useWorkflowFormStepMock.mockReturnValue({ isValid: true });
    useSelectedWorkflowStepMock.mockReturnValue('step2');
    rerender(false);
    rerender(true);
    useWorkflowFormStepMock.mockReturnValue({});
    useSelectedWorkflowStepMock.mockReturnValue('step3');
    rerender(true);

    expect(setStepTrackingError).toBeCalled();
    expect(setStepTrackingInprogress).toBeCalled();
  });
});
