import { fireEvent } from '@testing-library/dom';
import * as helper from 'app/helpers/isDevice';
import { renderWithMockStore } from 'app/utils';
import React from 'react';
import { act } from 'react-dom/test-utils';
import ProgressBar, { IProgressBarProps } from './index';

jest.mock('app/helpers/isDevice', () => ({
  __esModule: true,
  isDevice: false
}));
const mockHelper: any = helper;

describe('Progress Bar', () => {
  jest.mock('app/helpers/isDevice', () => ({
    isDevice: false
  }));

  const initialState = {
    common: {
      window: {
        width: 1920,
        height: 1080
      }
    }
  };
  const onSelectStep = jest.fn();
  const defaultMockProps: IProgressBarProps = {
    steps: [
      { id: '1', title: 'step1', description: 'step1', clickable: true },
      { id: '2', title: 'step2', description: 'step2' }
    ],
    selectedStep: 0,
    lastStep: 1,
    isCompletedWorkflow: false,
    onSelectStep: onSelectStep
  };
  const renderProgressBar = async (mockProps?: IProgressBarProps) => {
    const renderResult = await renderWithMockStore(
      <ProgressBar {...defaultMockProps} {...mockProps} />,
      { initialState }
    );
    return renderResult;
  };

  it('render step 1', async () => {
    // Render component properly
    const { getAllByText } = await renderProgressBar();
    expect(getAllByText(/step1/).length).toBeGreaterThan(0);
    expect(getAllByText(/step2/).length).toBeGreaterThan(0);

    // Toggle progress bar
    const toggleButton = document.querySelector('.nav-collapse-expand button');
    const iconChevronLeft = document.querySelector(
      '.icon.icon-chevron-left.size-4x'
    );
    expect(iconChevronLeft).toBeInTheDocument();
    act(() => {
      toggleButton && fireEvent.click(toggleButton);
    });
    const iconChevronRight = document.querySelector(
      '.icon.icon-chevron-right.size-4x'
    );
    expect(iconChevronRight).toBeInTheDocument();
  });

  it('render step 2', async () => {
    // Render step 2
    const { getByText } = await renderProgressBar({
      ...defaultMockProps,
      selectedStep: 1,
      steps: [
        { id: '1', title: 'step1', description: 'desc1', clickable: true },
        { id: '2', title: 'step2', description: 'desc2', clickable: true },
        { id: '3', title: 'step3', description: 'desc3', clickable: true }
      ],
      isCompletedWorkflow: false
    });
    expect(getByText(/step1/)).toBeInTheDocument();
    expect(getByText(/step2/)).toBeInTheDocument();

    // handle click step 1
    act(() => {
      fireEvent.click(getByText(/step1/));
    });

    expect(getByText('1')).toBeInTheDocument();
    expect(onSelectStep).toBeCalled();
  });

  it('render only 1 step', async () => {
    mockHelper.isDevice = true;
    const { getByText } = await renderProgressBar({
      ...defaultMockProps,
      selectedStep: 0,
      steps: [
        { id: '1', title: 'step1', description: 'desc1', clickable: true }
      ],
      lastStep: 0
    });
    expect(getByText(/step1/)).toBeInTheDocument();
  });
});
