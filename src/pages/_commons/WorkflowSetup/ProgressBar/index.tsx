import { MIN_WIDTH_PC } from 'app/constants/constants';
import { classnames, isDevice } from 'app/helpers';
import {
  Button,
  Icon,
  SimpleBar,
  Tooltip,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import isFunction from 'lodash.isfunction';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import React, { useEffect, useMemo, useState } from 'react';

export interface IProgressBarProps {
  steps: WorkflowSetupStep[];
  selectedStep: number;
  lastStep: number;
  isCompletedWorkflow?: boolean;
  onSelectStep?: (step: string) => void;
}

const ProgressBar: React.FC<IProgressBarProps> = ({
  steps,
  selectedStep,
  lastStep,
  isCompletedWorkflow,
  onSelectStep
}) => {
  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();
  const [expanded, setExpanded] = useState(width > MIN_WIDTH_PC && !isDevice);
  const [isMoveOverFirstStep, setIsMoveOverFirstStep] = useState(false);

  useEffect(() => {
    if (lastStep <= 0 || isMoveOverFirstStep) return;

    return () => {
      setIsMoveOverFirstStep(true);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [lastStep]);

  const navClassnames = useMemo(
    () =>
      classnames('workflow-creation__nav', {
        'workflow-creation__nav--collapsed': !expanded
      }),
    [expanded]
  );

  const toggleMenu = () => {
    setExpanded(expanded => !expanded);
  };

  const handleClickStep = (step: string) => {
    const onClick = () => {
      isFunction(onSelectStep) && onSelectStep(step);
    };

    return onClick;
  };

  return (
    <nav className={navClassnames}>
      <div className="nav-collapse-expand">
        <Tooltip
          element={expanded ? t('txt_collapse') : t('txt_expand')}
          variant="primary"
          placement="top"
          opened={isDevice ? false : undefined}
        >
          <Button variant="icon-secondary" onClick={toggleMenu}>
            <Icon
              size="4x"
              name={expanded ? 'chevron-left' : 'chevron-right'}
            />
          </Button>
        </Tooltip>
      </div>
      <SimpleBar>
        <div className="p-16">
          {steps.map(({ id, title, description, error, inprogress }, idx) => {
            const isDone = idx <= lastStep || isCompletedWorkflow;
            const isError = isDone && error && idx <= lastStep;
            const isSelected = idx === selectedStep;
            const isInprogress =
              (lastStep === 0 && idx === lastStep && !isMoveOverFirstStep) ||
              (idx <= lastStep && inprogress && !isCompletedWorkflow);

            const canClick = !isSelected && (isDone || isInprogress);
            const handleClick = canClick ? handleClickStep(id) : undefined;

            return (
              <div
                key={idx}
                className={classnames('nav-step', {
                  'nav-step--hovered': canClick,
                  'nav-step--done': isDone,
                  'nav-step--error': isError,
                  'nav-step--selected': isSelected,
                  'nav-step--inprogress': isInprogress,
                  'nav-step--last-step': lastStep === idx
                })}
              >
                <div className="nav-step-inner" onClick={handleClick}>
                  <div className="nav-step-counter">{idx + 1}</div>

                  <div className="nav-step-info">
                    <div className="nav-step-info__title">
                      <span>{t(title)}</span>
                    </div>
                    {isDone && description && (
                      <div className="nav-step-info__description">
                        <TruncateText title={description} lines={3} resizable>
                          {description}
                        </TruncateText>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </SimpleBar>
    </nav>
  );
};

export default ProgressBar;
