import { renderHook } from '@testing-library/react-hooks';
import 'app/utils/_mockComponent/mockUseTranslation';
import * as workflowSetupSelects from 'pages/_commons/redux/WorkflowSetup/select-hooks/_common';
import { useWorkflowSetupParser } from './useWorkflowSetupParser';

jest.mock('pages/_commons/WorkflowSetup/registries', () => {
  return {
    stepRegistry: {
      stepsMap: {
        Component: 'Component'
      },
      unsavedChangeFormsMap: {}
    }
  };
});

// mock hooks
const useWorkflowFormStepsDependencyMock = jest.spyOn(
  workflowSetupSelects,
  'useWorkflowFormStepsDependency'
);
const useWorkflowFormStepsDependencyNoCacheMock = jest.spyOn(
  workflowSetupSelects,
  'useWorkflowFormStepsDependencyNoCache'
);
const useSelectWorkflowInstanceMock = jest.spyOn(
  workflowSetupSelects,
  'useSelectWorkflowInstance'
);

beforeEach(() => {
  useWorkflowFormStepsDependencyMock.mockReturnValue({
    sourceData: 5,
    'gettingStart.returnData': 5
  });
  useWorkflowFormStepsDependencyNoCacheMock.mockReturnValue({
    sourceData: 5,
    'gettingStart.returnData': 5
  });
  useSelectWorkflowInstanceMock.mockReturnValue({
    data: {
      changeDetail: {},
      workflowSetupData: [{ id: 'gettingStart' }]
    }
  } as any);
});

afterEach(() => {
  jest.clearAllMocks();
});

describe('useWorkflowSetupParser', () => {
  // mock params
  const fakeSteps = {
    title: 'title',
    workflowInfo: 'Information',
    steps: [
      {
        id: 'gettingStart',
        title: 'Get Started',
        component: 'Component',
        condition: true,
        order: 1
      },
      {
        id: 'changeInformation',
        title: 'Select a Change',
        component: 'Component',
        condition: {
          operator: '>',
          source: 'gettingStart.returnData',
          target: 4
        },
        dataApi: 'createWorkflowInstance',
        order: 2
      },
      {
        id: 'returnedCheckCharges',
        title: 'Manage Penalty Fee Methods - Returned Check Charges',
        component: 'Component',
        condition: {
          operator: '<',
          source: 'sourceData',
          target: 6
        },
        dataApi: 'updateWorkflowInstance',
        order: 3
      },
      {
        id: 'lateCharges',
        title: 'Manage Penalty Fee Methods - Late Charges',
        component: 'Component',
        condition: {
          operator: '>=',
          source: 'sourceData',
          target: 5
        },
        dataApi: 'updateWorkflowInstance',
        order: 3
      },
      {
        id: 'summaryInformation',
        title: 'Review and Submit',
        component: 'Component',
        condition: {
          operator: '<=',
          source: 'sourceData',
          targetPath: 'sourceData'
        },
        dataApi: 'setupWorkflow',
        order: 4
      },
      {
        id: 'dif',
        title: 'Review and Submit',
        component: 'Component',
        condition: {
          operator: '!=',
          source: 'sourceData',
          target: 1
        },
        dataApi: 'setupWorkflow',
        order: 5
      },
      {
        id: 'and',
        title: 'And',
        component: 'Component',
        condition: {
          operator: 'and',
          childrens: [
            {
              operator: '>',
              source: 'sourceData',
              target: 1
            }
          ]
        },
        dataApi: 'setupWorkflow',
        order: 6
      },
      {
        id: 'or',
        title: 'Or',
        component: 'Component',
        condition: {
          operator: 'or',
          childrens: [
            {
              operator: '>',
              source: 'sourceData',
              target: 1
            }
          ]
        },
        dataApi: 'setupWorkflow',
        order: 7
      },
      {
        id: 'noCondition',
        title: 'Review and Submit',
        component: 'Component',
        dataApi: 'setupWorkflow'
      },
      {
        id: 'no condition source',
        title: 'no condition source',
        component: 'Component',
        condition: {},
        dataApi: 'setupWorkflow',
        order: 8
      },
      {
        id: 'no condition source',
        title: 'no condition source',
        component: 'Component',
        condition: {
          operator: '=',
          source: 'sourceData',
          target: 5
        },
        dataApi: 'setupWorkflow',
        order: 9
      }
    ]
  } as any;

  it('hook', () => {
    const { result, rerender } = renderHook(() =>
      useWorkflowSetupParser(fakeSteps, true)
    );

    expect(result.current.stepsMapping).toHaveLength(10);
    expect(result.current.allStepsMapping).toHaveLength(11);

    useWorkflowFormStepsDependencyMock.mockReturnValue({
      sourceData: 4,
      'gettingStart.returnData': 5
    });
    useWorkflowFormStepsDependencyNoCacheMock.mockReturnValue({
      sourceData: 4,
      'gettingStart.returnData': 5
    });
    rerender();
    expect(result.current.stepsMapping).toHaveLength(8);

    useWorkflowFormStepsDependencyMock.mockReturnValue({
      sourceData: 5,
      'gettingStart.returnData': 5
    });
    useWorkflowFormStepsDependencyNoCacheMock.mockReturnValue({
      sourceData: 5,
      'gettingStart.returnData': 5
    });
    rerender();
    expect(result.current.stepsMapping).toHaveLength(10);

    useWorkflowFormStepsDependencyMock.mockReturnValue({
      sourceData: 6,
      'gettingStart.returnData': 5
    });
    useWorkflowFormStepsDependencyNoCacheMock.mockReturnValue({
      sourceData: 6,
      'gettingStart.returnData': 5
    });
    rerender();
    expect(result.current.stepsMapping).toHaveLength(8);

    useWorkflowFormStepsDependencyMock.mockReturnValue({
      sourceData: 5,
      'gettingStart.returnData': 5
    });
    useWorkflowFormStepsDependencyNoCacheMock.mockReturnValue({
      sourceData: 5,
      'gettingStart.returnData': 5
    });
    rerender();
    expect(result.current.stepsMapping).toHaveLength(10);
  });

  it('hook 2', () => {
    const { result, rerender } = renderHook(() =>
      useWorkflowSetupParser(
        {
          steps: [
            {
              id: 'gettingStart',
              title: 'Get Started',
              component: 'Component',
              order: 1
            },
            {
              id: 'gettingStart1',
              title: 'Get Started',
              component: 'Component',
              condition: {
                operator: '>',
                source: 'gettingStart.returnData',
                target: 4
              },
              order: 2
            }
          ]
        },
        true
      )
    );

    useWorkflowFormStepsDependencyMock.mockReturnValue({
      'gettingStart.returnData': 5
    });
    useWorkflowFormStepsDependencyNoCacheMock.mockReturnValue({
      'gettingStart.returnData': 5
    });
    expect(result.current.stepsMapping).toHaveLength(2);
    expect(result.current.allStepsMapping).toHaveLength(2);

    useWorkflowFormStepsDependencyMock.mockReturnValue({
      'gettingStart.returnData': 3
    });
    useWorkflowFormStepsDependencyNoCacheMock.mockReturnValue({
      'gettingStart.returnData': 3
    });
    rerender();
    expect(result.current.stepsMapping).toHaveLength(1);
  });

  it('hook 3', () => {
    useSelectWorkflowInstanceMock.mockReturnValue(undefined);
    const { result } = renderHook(() =>
      useWorkflowSetupParser(
        {
          steps: [
            {
              id: 'gettingStart',
              title: 'Get Started',
              component: 'Component',
              order: 1
            },
            {
              id: 'gettingStart1',
              title: 'Get Started',
              component: 'Component',
              condition: {
                operator: '>',
                source: 'gettingStart.returnData',
                target: 4
              },
              order: 2
            }
          ]
        },
        true
      )
    );

    useWorkflowFormStepsDependencyMock.mockReturnValue({
      'gettingStart.returnData': 5
    });
    useWorkflowFormStepsDependencyNoCacheMock.mockReturnValue({
      'gettingStart.returnData': 5
    });
    expect(result.current.stepsMapping).toHaveLength(2);
    expect(result.current.allStepsMapping).toHaveLength(2);
  });
});
