import { workflowTemplateService } from 'app/services';
import parseFormValues from './parseFormValues';

jest.mock('app/services');

jest.mock('../_components/UploadFile/helpers', () => ({
  __esModule: true,
  getStatusFileFromValidationMessages: () => ''
}));
const factory = async (
  hasAttachment?: boolean,
  hasStepInfo?: boolean,
  isSuccessResponse?: boolean,
  tableType?: string,
  id = 'uploadFileAQ'
) => {
  jest.spyOn(workflowTemplateService, 'getFileDetails').mockResolvedValue({
    data: {
      attachments: [
        {
          validationMessages: [{ field: '', message: 'msg' }],
          status: isSuccessResponse ? 'ready' : ''
        }
      ]
    }
  } as never);
  return parseFormValues(
    {
      data: {
        configurations: {
          attachments: hasAttachment
            ? [
                {
                  attachmentId: id,
                  ['table.type']: tableType ?? 'AQ'
                }
              ]
            : undefined
        },
        workflowSetupData: [
          {
            id: hasStepInfo ? id : '0'
          }
        ]
      }
    } as unknown as WorkflowSetupInstance,
    id,
    jest.fn()
  );
};

describe('test parseFormValues helper', () => {
  it('should be return undefined', async () => {
    const actual = await factory();
    expect(actual).toEqual(undefined);
  });
  it('should be return undefined', async () => {
    const actual = await factory(true);
    expect(actual).toEqual(undefined);
  });
  it('should be return with no file', async () => {
    const actual = await factory(false, true);
    expect(actual).toEqual({
      attachmentId: undefined,
      files: [],
      isPass: false,
      isSelected: false,
      isValid: false
    });
  });
  it('should be working', async () => {
    const actual = await factory(true, true);
    expect(actual).toEqual({
      attachmentId: 'uploadFileAQ',
      files: [
        {
          idx: undefined,
          name: undefined,
          percentage: 98,
          size: undefined,
          status: '',
          type: undefined
        }
      ],
      isPass: false,
      isSelected: false,
      isValid: false
    });
  });
  it('should be working with successResponse', async () => {
    const actual = await factory(true, true, true);
    expect(actual).toEqual({
      attachmentId: 'uploadFileAQ',
      files: [
        {
          idx: undefined,
          name: undefined,
          percentage: 100,
          size: undefined,
          status: '',
          type: undefined
        }
      ],
      isPass: false,
      isSelected: false,
      isValid: false
    });
  });
  it('should be working with successResponse - CA', async () => {
    const actual = await factory(
      true,
      true,
      true,
      'CA',
      'uploadFileClientAllocation'
    );
    expect(actual).toEqual({
      attachmentId: 'uploadFileClientAllocation',
      files: [
        {
          idx: undefined,
          name: undefined,
          percentage: 100,
          size: undefined,
          status: '',
          type: undefined
        }
      ],
      isPass: false,
      isSelected: false,
      isValid: false
    });
  });
  it('should be working with successResponse - CA - uploadFileCA', async () => {
    const actual = await factory(true, true, true, 'CA', 'uploadFileCA');
    expect(actual).toEqual({
      attachmentId: 'uploadFileCA',
      files: [
        {
          idx: undefined,
          name: undefined,
          percentage: 100,
          size: undefined,
          status: '',
          type: undefined
        }
      ],
      isPass: false,
      isSelected: false,
      isValid: false
    });
  });

  it('should be working with successResponse - ST - uploadFileSelectionTable', async () => {
    const actual = await factory(
      true,
      true,
      true,
      'ST',
      'uploadFileSelectionTable'
    );
    expect(actual).toEqual({
      attachmentId: 'uploadFileSelectionTable',
      files: [
        {
          idx: undefined,
          name: undefined,
          percentage: 100,
          size: undefined,
          status: '',
          type: undefined
        }
      ],
      isPass: false,
      isSelected: false,
      isValid: false
    });
  });

  it('should be working with successResponse - TQ - uploadFileTransactionQualification', async () => {
    const actual = await factory(
      true,
      true,
      true,
      'TQ',
      'uploadFileTransactionQualification'
    );
    expect(actual).toEqual({
      attachmentId: 'uploadFileTransactionQualification',
      files: [
        {
          idx: undefined,
          name: undefined,
          percentage: 100,
          size: undefined,
          status: '',
          type: undefined
        }
      ],
      isPass: false,
      isSelected: false,
      isValid: false
    });
  });

  it('should be working with successResponse - RQ - uploadFileRetailQualification', async () => {
    const actual = await factory(
      true,
      true,
      true,
      'RQ',
      'uploadFileRetailQualification'
    );
    expect(actual).toEqual({
      attachmentId: 'uploadFileRetailQualification',
      files: [
        {
          idx: undefined,
          name: undefined,
          percentage: 100,
          size: undefined,
          status: '',
          type: undefined
        }
      ],
      isPass: false,
      isSelected: false,
      isValid: false
    });
  });
});
