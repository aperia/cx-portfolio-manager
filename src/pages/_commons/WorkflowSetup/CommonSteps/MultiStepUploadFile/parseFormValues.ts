import { getWorkflowSetupStepStatus } from 'app/helpers';
import { workflowTemplateService } from 'app/services';
import { STATUS } from 'app/_libraries/_dls/components/Upload/helper';
import { get } from 'lodash';
import { VERIFIED_STATUSES_RESPONSE } from '../_components/UploadFile/constants';
import { getStatusFileFromValidationMessages } from '../_components/UploadFile/helpers';

const getAttachmentIdStep = (attachments: any[], id: string) => {
  let attachmentIds = [];
  switch (id) {
    case 'uploadFileAQ':
    case 'uploadFileAccountQualification':
      attachmentIds = attachments
        .filter(i => i['table.type'] === 'AQ')
        .map(i => i.attachmentId);
      break;
    case 'uploadFileClientAllocation':
    case 'uploadFileCA':
      attachmentIds = attachments
        .filter(i => i['table.type'] === 'CA')
        .map(i => i.attachmentId);
      break;
    case 'uploadFileSelectionTable':
      attachmentIds = attachments
        .filter(i => i['table.type'] === 'ST')
        .map(i => i.attachmentId);
      break;
    case 'uploadFileTransactionQualification':
      attachmentIds = attachments
        .filter(i => i['table.type'] === 'TQ')
        .map(i => i.attachmentId);
      break;
    case 'uploadFileRetailQualification':
      attachmentIds = attachments
        .filter(i => i['table.type'] === 'RQ')
        .map(i => i.attachmentId);
      break;
  }
  return attachmentIds;
};

const parseFormValues: WorkflowSetupStepFormDataFunc<any> = async (
  data,
  id
) => {
  const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);
  const attachments = data?.data?.configurations?.attachments || [];
  if (!stepInfo) return;

  let file, status;
  const attachmentIds: any[] = getAttachmentIdStep(attachments, id);

  if (attachmentIds[0]) {
    const response = await workflowTemplateService.getFileDetails({
      searchFields: { attachmentIds }
    });
    const { validationMessages, ...attachment } = get(
      response,
      'data.attachments.0',
      {}
    );
    const successResponse = VERIFIED_STATUSES_RESPONSE.includes(
      attachment?.status?.toLowerCase()
    );

    status = getStatusFileFromValidationMessages(validationMessages);
    file = attachment && {
      ...attachment,
      idx: attachment.id,
      name: attachment.filename,
      size: attachment.fileSize,
      type: attachment.mineType,
      percentage: successResponse ? 100 : 98,
      status
    };
  }

  return {
    ...getWorkflowSetupStepStatus(stepInfo),
    files: !!file ? [file] : [],
    isValid: !!file && status === STATUS.valid,
    attachmentId: attachmentIds[0]
  };
};

export default parseFormValues;
