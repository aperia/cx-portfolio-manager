import React from 'react';
import { WorkflowSetupStaticProp } from '../../types';
import { UploadFileStep, UploadFileStepProps } from '../UploadFileStep/index';
import isDefaultValues from '../UploadFileStep/isDefaultValues';
import UploadFileSummary from '../UploadFileStep/UploadFileSummary';
import { FormUploadFile } from '../_components/UploadFile';
import parseFormValues from './parseFormValues';

const MultiStepUploadFile: React.FC<UploadFileStepProps> = props => {
  return <UploadFileStep {...props} />;
};

const ExtraStaticSummaryStep =
  MultiStepUploadFile as WorkflowSetupStaticProp<FormUploadFile>;
ExtraStaticSummaryStep.summaryComponent = UploadFileSummary;
ExtraStaticSummaryStep.defaultValues = {
  isValid: false,
  files: [],
  attachmentId: undefined
};
ExtraStaticSummaryStep.isDefaultValues = isDefaultValues;
ExtraStaticSummaryStep.parseFormValues = parseFormValues;
export default ExtraStaticSummaryStep;
