import { render } from '@testing-library/react';
import React from 'react';
import MultiStepUploadFile from './index';

jest.mock('../UploadFileStep', () => ({
  __esModule: true,
  UploadFileStep: () => <div data-testid="UploadFileStep"></div>
}));

const factory = () => {
  const { getByTestId } = render(<MultiStepUploadFile {...({} as never)} />);
  return {
    getUploadFile: () => getByTestId('UploadFileStep')
  };
};

describe('test MultiStepUploadFile component', () => {
  it('should render', () => {
    const { getUploadFile } = factory();
    expect(getUploadFile()).toBeInTheDocument();
  });
});
