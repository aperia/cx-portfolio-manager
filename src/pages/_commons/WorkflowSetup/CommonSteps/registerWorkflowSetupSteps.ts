import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import 'pages/WorkflowActionCardCompromiseEvent/registerSteps';
import 'pages/WorkflowAssignPricingStrategies/registerSteps';
import 'pages/WorkflowBulkArchivePCF/registerSteps';
import 'pages/WorkflowBulkExternalStatusManagement/registerSteps';
import 'pages/WorkflowBulkSuspendFraudStrategy/registerSteps';
import 'pages/WorkflowChangeInterestRates/registerSteps';
import 'pages/WorkflowConditionMCycleAccountsForTesting/registerSteps';
import 'pages/WorkflowConfigureProcessingMerchants/registerSteps';
import 'pages/WorkflowCustomDataLinkElements/registerSteps';
import 'pages/WorkflowCycleAccountsTestEnvironment/registerSteps';
import 'pages/WorkflowDesignLoanOffers/registerSteps';
import 'pages/WorkflowDesignPromotions/registerSteps';
import 'pages/WorkflowExportImportChangeDetails/registerSteps';
import 'pages/WorkflowFlexibleMonetaryTransactions/registerSteps';
import 'pages/WorkflowFlexibleNonMonetaryTransactions/registerSteps';
import 'pages/WorkflowForceEmbossReplacementCards/registerSteps';
import 'pages/WorkflowHaltInterestChargesAndFees/registerSteps';
import 'pages/WorkflowManageAccountDelinquency/registerSteps';
import 'pages/WorkflowManageAccountLevel/registerSteps';
import 'pages/WorkflowManageAccountMemos/registerSteps';
import 'pages/WorkflowManageAccountSerialization/registerSteps';
import 'pages/WorkflowManageAccountTransfer/registerSteps';
import 'pages/WorkflowManageAdjustmentsSystem/registerSteps';
import 'pages/WorkflowManageChangeInTerms/registerSteps';
import 'pages/WorkflowManageCreditLimit/registerSteps';
import 'pages/WorkflowManageIncomeOptionMethods/registerSteps';
import 'pages/WorkflowManageMethodLevelProcessingDecisionTablesMLP/registerSteps';
import 'pages/WorkflowManageOCSShells/registerSteps';
import 'pages/WorkflowManagePayoffExceptionMethods/registerSteps';
import 'pages/WorkflowManagePenaltyFee/registerSteps';
import 'pages/WorkflowManagePricingStrategiesAndMethodAssignments/registerSteps';
import 'pages/WorkflowManageStatementProductionSettings/registerSteps';
import 'pages/WorkflowManageTransactionLevelProcessingDecisionTablesTLP/registerSteps';
import 'pages/WorkflowManualAccountDeletion/registerSteps';
import 'pages/WorkflowPerformMonetaryAdjustments/registerSteps';
import 'pages/WorkflowPortfolioDefinitions/registerSteps';
import 'pages/WorkflowSendLetterToAccounts/registerSteps';
import 'pages/WorkflowUpdateIndexRate/registerSteps';
import { stepRegistry } from '../registries';
import ChangeInfoStep from './ChangeInfoStep';
import MultiStepUploadFile from './MultiStepUploadFile';
import SummaryStep from './SummaryStep';
import UploadFileStep from './UploadFileStep';

stepRegistry.registerStep('UploadFile', UploadFileStep, [
  UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__UPLOAD_FILE
]);

stepRegistry.registerStep('MultiStepUploadFile', MultiStepUploadFile, [
  UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__UPLOAD_FILE
]);

stepRegistry.registerStep('Summary', SummaryStep);
stepRegistry.registerStep('ChangeInformation', ChangeInfoStep, [
  UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__SELECT_A_CHANGE__EDIT_CHANGE__EXISTED_WORKFLOW,
  UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__SELECT_A_CHANGE__EDIT_CHANGE__EXISTED_WORKFLOW__CLOSE,
  UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__SELECT_A_CHANGE__EDIT_CHANGE
]);
