import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { default as stepRegistry } from '../registries/stepRegistry';

const registerFunc = jest.spyOn(stepRegistry, 'registerStep');

describe('WorkflowSetup > CommonsStep > registerWorkflowSetupSteps', () => {
  it('Should register components', async () => {
    await import('./registerWorkflowSetupSteps');

    expect(registerFunc).toBeCalledWith('UploadFile', expect.anything(), [
      UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__UPLOAD_FILE
    ]);
    expect(registerFunc).toBeCalledWith('Summary', expect.anything());

    expect(registerFunc).toBeCalledWith(
      'ChangeInformation',
      expect.anything(),
      [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__SELECT_A_CHANGE__EDIT_CHANGE__EXISTED_WORKFLOW,
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__SELECT_A_CHANGE__EDIT_CHANGE__EXISTED_WORKFLOW__CLOSE,
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__SELECT_A_CHANGE__EDIT_CHANGE
      ]
    );
  });
});
