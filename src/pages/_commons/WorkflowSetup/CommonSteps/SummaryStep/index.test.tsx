import { fireEvent, render } from '@testing-library/react';
import { WorkflowSetupStepForm } from 'app/types/workflowSetupState';
import * as workflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/_common';
import React from 'react';
import SummaryStep from './index';

const mockWorkflowSetupForms = jest.spyOn(
  workflowSetup,
  'useWorkflowSetupForms'
);

const MockComponent = ({ onEditStep }: any) => {
  return (
    <div>
      <div>StepSummary</div>
      <button onClick={onEditStep}>EditStep</button>
    </div>
  );
};

describe('WorkflowSetup > CommonsStep > SummaryStep > index', () => {
  it('Should', () => {
    mockWorkflowSetupForms.mockReturnValue([
      {
        isValid: true
      } as WorkflowSetupStepForm
    ]);

    const mockFn = jest.fn();

    const props = {
      summaries: [
        {
          stepId: '0',
          title: 'title-1',
          element: MockComponent,
          dependencies: {}
        } as any
      ] as any,
      handleSelectStep: mockFn
    } as any;

    const wrapper = render(<SummaryStep {...props} />);
    expect(wrapper.getByText('StepSummary')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('EditStep'));
    expect(mockFn).toHaveBeenCalled();
  });
});
