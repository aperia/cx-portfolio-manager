import { classnames } from 'app/helpers';
import { TransDLS, useTranslation } from 'app/_libraries/_dls';
import { isFunction } from 'lodash';
import { useWorkflowSetupForms } from 'pages/_commons/redux/WorkflowSetup';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, { useMemo } from 'react';
import parseFormValues from './parseFormValues';

const SummaryStep: React.FC<WorkflowSetupSummaryStepProps> = ({
  summaries,
  handleSelectStep,
  setFormValues,
  ...props
}) => {
  const { t } = useTranslation();
  const forms = useWorkflowSetupForms();

  const maxStepIdx = useMemo(() => summaries.length - 1, [summaries.length]);

  const editStep = (id: string) => {
    return () => isFunction(handleSelectStep) && handleSelectStep(id);
  };

  return (
    <div>
      <div className="pb-24 border-bottom color-grey">
        <p className="mt-8">
          <TransDLS keyTranslation="txt_workflow_setup_summary_desc_1">
            <strong className="color-grey-d20" />
          </TransDLS>
        </p>
        <p className="mt-8">
          <TransDLS keyTranslation="txt_workflow_setup_summary_desc_2">
            <strong className="color-grey-d20" />
          </TransDLS>
        </p>
      </div>

      {summaries.map((step, idx) => {
        const {
          element: StepSummary,
          dependencies,
          ...originalStepConfig
        } = step;
        const { stepId, title } = step;

        return (
          <React.Fragment key={`${idx}-${stepId}`}>
            <div
              className={classnames({
                'my-24': idx < maxStepIdx,
                'mt-24': idx === maxStepIdx
              })}
            >
              <h5>
                {t('txt_step')} {idx + 1} - {title}
              </h5>
              <StepSummary
                {...props}
                originalStepConfig={originalStepConfig}
                formValues={forms[stepId]}
                onEditStep={editStep(stepId)}
                dependencies={dependencies}
              />
            </div>
            {idx < maxStepIdx && <div className="divider-dashed" />}
          </React.Fragment>
        );
      })}
    </div>
  );
};

const ExtraStaticSummaryStep = SummaryStep as WorkflowSetupStaticProp;

ExtraStaticSummaryStep.isSummary = true;
ExtraStaticSummaryStep.defaultValues = { isValid: true };
ExtraStaticSummaryStep.parseFormValues = parseFormValues;

export default ExtraStaticSummaryStep;
