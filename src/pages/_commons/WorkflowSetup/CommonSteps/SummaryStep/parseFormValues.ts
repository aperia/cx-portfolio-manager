import { getWorkflowSetupStepStatus } from 'app/helpers';

const parseFormValues: WorkflowSetupStepFormDataFunc = (data, id) => {
  const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);
  if (!stepInfo) return;

  return getWorkflowSetupStepStatus(stepInfo);
};

export default parseFormValues;
