import { workflowTemplateService } from 'app/services';
import parseFormValues from './parseFormValues';

describe('UploadFileStep > parseFormValues', () => {
  it('has attachmentId', async () => {
    const fileResponse = {
      id: 'id',
      filename: 'filename',
      fileSize: 'fileSize',
      mineType: 'mineType'
    };
    jest.spyOn(workflowTemplateService, 'getFileDetails').mockResolvedValue({
      data: { attachments: [fileResponse] }
    } as any);
    const data: any = {
      data: {
        configurations: { attachmentId: 'attachmentId' },
        workflowSetupData: [{ id: 'id', data: { file: { id: 'file' } } }]
      }
    };
    const id = 'id';
    const result = await parseFormValues(data, id, jest.fn());

    expect(result.attachmentId).toEqual('attachmentId');
  });
  it('has attachmentId with successResponse', async () => {
    const fileResponse = {
      id: 'id',
      filename: 'filename',
      fileSize: 'fileSize',
      mineType: 'mineType',
      status: 'ready'
    };
    jest.spyOn(workflowTemplateService, 'getFileDetails').mockResolvedValue({
      data: { attachments: [fileResponse] }
    } as any);
    const data: any = {
      data: {
        configurations: { attachmentId: 'attachmentId' },
        workflowSetupData: [{ id: 'id', data: { file: { id: 'file' } } }]
      }
    };
    const id = 'id';
    const result = await parseFormValues(data, id, jest.fn());

    expect(result.attachmentId).toEqual('attachmentId');
  });

  it('attachmentId empty', async () => {
    const data: any = {
      data: {
        workflowSetupData: [
          {
            id: 'id',
            data: {
              file: {
                id: 'file'
              }
            }
          }
        ]
      }
    };
    const id = 'id';
    const result = await parseFormValues(data, id, jest.fn());
    expect(result).toEqual({
      attachmentId: undefined,
      files: [],
      isPass: false,
      isSelected: false,
      isValid: false
    });
  });

  it('empty workflow data', async () => {
    const data: any = {
      data: {
        workflowSetupData: [
          {
            id: 'id',
            data: {
              file: {
                id: 'file'
              }
            }
          }
        ]
      }
    };
    const id = 'id1';
    const result = await parseFormValues(data, id, jest.fn());

    expect(result).toBeUndefined();
  });
});
