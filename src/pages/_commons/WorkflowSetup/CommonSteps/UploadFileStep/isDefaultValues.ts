import { isEqual } from 'lodash';
import { FormUploadFile } from '../_components/UploadFile';

const isDefaultValues: WorkflowSetupIsDefaultValueFunc<FormUploadFile> = (
  formValues,
  defaultValues
) => {
  return (
    isEqual(formValues?.files, defaultValues?.files) &&
    formValues?.isValid === defaultValues?.isValid
  );
};

export default isDefaultValues;
