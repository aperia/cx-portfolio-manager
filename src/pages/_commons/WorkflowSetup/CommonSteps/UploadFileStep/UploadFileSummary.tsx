import AttachmentItem from 'app/components/AttachmentItem';
import { MIME_TYPE } from 'app/constants/mine-type';
import { isDevice } from 'app/helpers';
import { base64toBlob, downloadBlobFile } from 'app/helpers/fileUtilities';
import { Button, Icon, Tooltip, useTranslation } from 'app/_libraries/_dls';
import isFunction from 'lodash.isfunction';
import { actionsWorkflow } from 'pages/_commons/redux/Workflow';
import { actionsToast } from 'pages/_commons/ToastNotifications/redux';
import React from 'react';
import { useDispatch } from 'react-redux';
import { FormUploadFile } from '../_components/UploadFile';

const UploadFileSummary: React.FC<WorkflowSetupSummaryProps<FormUploadFile>> =
  ({ formValues, onEditStep }) => {
    const { idx, name, type } = formValues?.files?.[0] || ({} as any);
    const { t } = useTranslation();
    const dispatch = useDispatch();

    const handleEdit = () => {
      isFunction(onEditStep) && onEditStep();
    };

    const handleDownloadFile = async (attachmentId: string) => {
      const result: any = await Promise.resolve(
        dispatch(actionsWorkflow.downloadAttachment(attachmentId?.toString()))
      );
      const isSuccess =
        actionsWorkflow.downloadAttachment.fulfilled.match(result);

      if (isSuccess) {
        const {
          fileStream = '',
          mimeType: mimeTypeDownload = '',
          filename: filenameDownload = ''
        } = (result?.payload || {}) as IDownloadAttachment;

        let fileName = filenameDownload;
        if (filenameDownload.split('.').length < 2) {
          const ext = MIME_TYPE[mimeTypeDownload]?.extensions[0] || '';
          fileName = `${filenameDownload}.${ext}`;
        }
        const blol = base64toBlob(fileStream, mimeTypeDownload);
        downloadBlobFile(blol, fileName);
      } else {
        dispatch(
          actionsToast.addToast({
            type: 'error',
            message: t('txt_file_failed_to_download')
          })
        );
      }
    };

    return (
      <div className="position-relative d-flex">
        <div className="absolute-top-right mt-n26 mr-n8">
          <Button variant="outline-primary" size="sm" onClick={handleEdit}>
            {t('txt_edit')}
          </Button>
        </div>
        {idx && (
          <div className="mt-16 w-100">
            <AttachmentItem
              id={idx}
              mimeType={type}
              fileName={name}
              suffix={
                <div className="mr-n4 d-flex">
                  <Tooltip
                    element={t('txt_download')}
                    variant={'primary'}
                    opened={isDevice ? false : undefined}
                  >
                    <Button
                      variant="icon-secondary"
                      className={'d-inline ml-8'}
                      size="sm"
                      onClick={() => handleDownloadFile(idx)}
                    >
                      <Icon
                        name="download"
                        color={'grey-l16' as any}
                        size="4x"
                      />
                    </Button>
                  </Tooltip>
                </div>
              }
            />
          </div>
        )}
      </div>
    );
  };

export default UploadFileSummary;
