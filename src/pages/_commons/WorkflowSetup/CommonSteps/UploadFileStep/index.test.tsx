import { render, RenderResult } from '@testing-library/react';
import React from 'react';
import * as ReactRedux from 'react-redux';
import UploadFileStep from '.';

const mockDispatch = jest.spyOn(ReactRedux, 'useDispatch');
const dispatchFn = jest.fn();

beforeEach(() => {
  mockDispatch.mockReturnValue(dispatchFn);
});

afterEach(() => {
  mockDispatch.mockClear();
});

jest.mock('../_components/UploadFile', () => {
  return {
    __esModule: true,
    default: () => <div>UploadFile</div>
  };
});

jest.mock('../_components/DownloadFile', () => {
  return {
    __esModule: true,
    default: () => <div>DownloadFileStep</div>
  };
});

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <UploadFileStep {...props} />
    </div>
  );
};

describe('_commons > WorkflowSetup > CommonSteps > UploadFileStep', () => {
  it('Should render', () => {
    const wrapper = renderComponent({
      downloadTemplateType: 'manageTemporaryCreditLimitWorkflow',
      formValue: {
        file: {
          fileId: '1',
          idx: '1',
          status: '1',
          size: 1024
        }
      }
    });
    expect(wrapper.getByText('UploadFile')).toBeInTheDocument();
  });
});
