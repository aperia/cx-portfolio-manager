import isDefaultValues from './isDefaultValues';

describe('UploadFileStep > isDefaultValues', () => {
  it('function isDefaultValues', () => {
    const formValues = {
      files: 'id',
      isValid: true
    };
    const defaultValue = {
      files: 'id',
      isValid: true
    };

    const result = isDefaultValues(formValues, defaultValue);

    expect(result).toEqual(true);
  });
});
