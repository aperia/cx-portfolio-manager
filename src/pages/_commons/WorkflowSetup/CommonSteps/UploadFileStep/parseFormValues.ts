import { getWorkflowSetupStepStatus } from 'app/helpers';
import { workflowTemplateService } from 'app/services';
import { STATUS } from 'app/_libraries/_dls/components/Upload/helper';
import { get } from 'lodash';
import { VERIFIED_STATUSES_RESPONSE } from '../_components/UploadFile/constants';
import { getStatusFileFromValidationMessages } from '../_components/UploadFile/helpers';

const parseFormValues: WorkflowSetupStepFormDataFunc<any> = async (
  data,
  id
) => {
  const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);
  const attachmentId = data?.data?.configurations?.attachmentId;
  if (!stepInfo) return;

  let file, status;
  if (attachmentId) {
    const attachmentIds = [attachmentId + ''];
    const response = await workflowTemplateService.getFileDetails({
      searchFields: { attachmentIds }
    });
    const { validationMessages, ...attachment } = get(
      response,
      'data.attachments.0',
      {}
    );
    const successResponse = VERIFIED_STATUSES_RESPONSE.includes(
      attachment?.status?.toLowerCase()
    );

    status = getStatusFileFromValidationMessages(validationMessages);
    file = attachment && {
      ...attachment,
      idx: attachment.id,
      name: attachment.filename,
      size: attachment.fileSize,
      type: attachment.mineType,
      percentage: successResponse ? 100 : 98,
      status
    };
  }

  return {
    ...getWorkflowSetupStepStatus(stepInfo),
    files: !!file ? [file] : [],
    isValid: !!file && status === STATUS.valid,
    attachmentId
  };
};

export default parseFormValues;
