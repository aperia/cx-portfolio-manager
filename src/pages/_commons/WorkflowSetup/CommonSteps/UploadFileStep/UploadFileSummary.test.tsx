import { fireEvent } from '@testing-library/dom';
import * as FileUtils from 'app/helpers/fileUtilities';
import * as helper from 'app/helpers/isDevice';
import { mockThunkAction, renderWithMockStore } from 'app/utils';
import { actionsWorkflow } from 'pages/_commons/redux/Workflow';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import React from 'react';
import { FormUploadFile } from '../_components/UploadFile';
import UploadFileSummary from './UploadFileSummary';

const mockHelper: Record<string, any> = helper;
const base64toBlobMock = jest.spyOn(FileUtils, 'base64toBlob');
const downloadBlobFileMock = jest.spyOn(FileUtils, 'downloadBlobFile');

const storeCofig = {
  initialState: {}
};
const mockActionsWorkflowSetup = mockThunkAction(actionsWorkflowSetup);
const mockActionsWorkflow = mockThunkAction(actionsWorkflow);

describe('UploadFileSummary', () => {
  beforeEach(() => {
    base64toBlobMock.mockReturnValue(new Blob());
    downloadBlobFileMock.mockImplementation(() => {});
    mockHelper.isDevice = false;
  });

  it('not render', async () => {
    const props = {
      formValues: {
        attachmentId: '123'
      },
      onEditStep: jest.fn(),
      stepId: 'id',
      selectedStep: 'id1'
    } as WorkflowSetupSummaryProps<FormUploadFile>;

    const wrapper = await renderWithMockStore(
      <UploadFileSummary {...props} />,
      {}
    );

    expect(wrapper.getByText('txt_edit')).toBeInTheDocument();
  });

  it('has value', async () => {
    mockActionsWorkflowSetup('getFileDetails', {
      match: false,
      payload: {
        payload: {
          attachments: [
            {
              validationMessages: [],
              id: 'id',
              fileName: 'name.xls',
              fileSize: 123,
              mineType: 'mineType'
            }
          ]
        }
      }
    });
    const props = {
      formValues: {
        attachmentId: '123'
      },
      onEditStep: jest.fn(),
      stepId: 'id',
      selectedStep: 'id'
    } as WorkflowSetupSummaryProps<FormUploadFile>;

    const wrapper = await renderWithMockStore(
      <UploadFileSummary {...props} />,
      storeCofig
    );

    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(props.onEditStep).toBeCalled();
  });
  it('not attachmentID', async () => {
    mockActionsWorkflowSetup('getFileDetails', {
      match: false,
      payload: {}
    });
    const props = {
      formValues: {
        attachmentId: '123'
      },
      onEditStep: jest.fn(),
      stepId: 'id',
      selectedStep: 'id'
    } as WorkflowSetupSummaryProps<FormUploadFile>;

    const wrapper = await renderWithMockStore(
      <UploadFileSummary {...props} />,
      storeCofig
    );

    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(props.onEditStep).toBeCalled();
  });

  it('render with file on device', async () => {
    mockHelper.isDevice = true;
    const props = {
      formValues: {
        files: [
          {
            idx: 'idx',
            name: 'abc.xls',
            item: jest.fn(),
            size: 5 * 1024,
            length: 1
          }
        ]
      },
      onEditStep: jest.fn(),
      stepId: 'id',
      selectedStep: 'id'
    } as WorkflowSetupSummaryProps<FormUploadFile>;
    const wrapper = await renderWithMockStore(
      <UploadFileSummary {...props} />,
      storeCofig
    );
    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(props.onEditStep).toBeCalled();
  });

  it('render with empty', async () => {
    const props = {
      formValues: {},
      onEditStep: jest.fn(),
      stepId: 'id',
      selectedStep: 'id'
    } as WorkflowSetupSummaryProps<FormUploadFile>;
    const wrapper = await renderWithMockStore(
      <UploadFileSummary {...props} />,
      storeCofig
    );

    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(props.onEditStep).toBeCalled();
  });

  it('Download file failed', async () => {
    const props = {
      formValues: {
        files: [
          {
            idx: 'idx',
            name: 'abc.xls',
            item: jest.fn(),
            size: 5 * 1024,
            length: 1
          }
        ]
      },
      onEditStep: jest.fn(),
      stepId: 'id',
      selectedStep: 'id'
    } as WorkflowSetupSummaryProps<FormUploadFile>;
    mockActionsWorkflow('downloadAttachment', {
      match: false,
      payload: {}
    });
    const wrapper = await renderWithMockStore(
      <UploadFileSummary {...props} />,
      storeCofig
    );

    const downloadIcon = wrapper.container.querySelector(
      'i.icon.icon-download'
    );
    fireEvent.click(downloadIcon!);
    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(props.onEditStep).toBeCalled();
  });

  it('Download file successfully with full file name', async () => {
    const props = {
      formValues: {
        files: [
          {
            idx: 'idx',
            name: 'abc.xls',
            item: jest.fn(),
            size: 5 * 1024,
            length: 1
          }
        ]
      },
      onEditStep: jest.fn(),
      stepId: 'id',
      selectedStep: 'id'
    } as WorkflowSetupSummaryProps<FormUploadFile>;
    mockActionsWorkflow('downloadAttachment', {
      match: true,
      payload: {
        payload: {
          fileStream: '',
          mimeType: 'text/csv',
          filename: 'mane.csv'
        }
      }
    });
    const wrapper = await renderWithMockStore(
      <UploadFileSummary {...props} />,
      storeCofig
    );
    const downloadIcon = wrapper.container.querySelector(
      'i.icon.icon-download'
    );
    fireEvent.click(downloadIcon!);
    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(props.onEditStep).toBeCalled();
  });

  it('Download file successfully with file name not full', async () => {
    const props = {
      formValues: {
        files: [
          {
            idx: 'idx',
            name: 'abc.xls',
            item: jest.fn(),
            size: 5 * 1024,
            length: 1
          }
        ]
      },
      onEditStep: jest.fn(),
      stepId: 'id',
      selectedStep: 'id'
    } as WorkflowSetupSummaryProps<FormUploadFile>;
    mockActionsWorkflow('downloadAttachment', {
      match: true,
      payload: {
        payload: {
          attachments: [
            {
              validationMessages: [],
              id: 'id',
              fileName: 'mane',
              fileSize: 123,
              mineType: 'text/csv'
            }
          ]
        }
      }
    });
    const wrapper = await renderWithMockStore(
      <UploadFileSummary {...props} />,
      storeCofig
    );
    const downloadIcon = wrapper.container.querySelector(
      'i.icon.icon-download'
    );
    fireEvent.click(downloadIcon!);
    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(props.onEditStep).toBeCalled();
  });

  it('Download file successfully with file name without payload', async () => {
    const props = {
      formValues: {
        files: [
          {
            idx: 'idx',
            name: 'abc.xls',
            item: jest.fn(),
            size: 5 * 1024,
            length: 1
          }
        ]
      },
      onEditStep: jest.fn(),
      stepId: 'id',
      selectedStep: 'id'
    } as WorkflowSetupSummaryProps<FormUploadFile>;
    mockActionsWorkflow('downloadAttachment', {
      match: true,
      payload: {}
    });
    const wrapper = await renderWithMockStore(
      <UploadFileSummary {...props} />,
      storeCofig
    );
    const downloadIcon = wrapper.container.querySelector(
      'i.icon.icon-download'
    );
    fireEvent.click(downloadIcon!);
    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(props.onEditStep).toBeCalled();
  });
});
