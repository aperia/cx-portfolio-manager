import { InlineMessage, useTranslation } from 'app/_libraries/_dls';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, { useMemo } from 'react';
import { uploadFileStepRegistry } from '../../registries';
import DownloadFile from '../_components/DownloadFile';
import UploadFile, { FormUploadFile } from '../_components/UploadFile';
import isDefaultValues from './isDefaultValues';
import parseFormValues from './parseFormValues';
import UploadFileSummary from './UploadFileSummary';

export interface UploadFileStepProps
  extends WorkflowSetupProps<FormUploadFile> {
  downloadTemplateType?: string;
  downloadHeaderText?: string;
  hideDownloadTemplate?: boolean;

  subContent?: string;
  uploadDescription?: string;
}

export const UploadFileStep: React.FC<UploadFileStepProps> = props => {
  const {
    stepId,
    selectedStep,
    formValues,
    dependencies,
    savedAt,
    ready,
    isStuck,

    hideDownloadTemplate = false,
    downloadTemplateType,
    downloadHeaderText,

    uploadDescription,
    subContent,

    setFormValues,
    handleSave
  } = props;

  const { t } = useTranslation();

  const SubContentComponent = useMemo(
    () => (subContent ? uploadFileStepRegistry.get(subContent) : undefined),
    [subContent]
  );

  return (
    <div className="position-relative">
      {!hideDownloadTemplate && (
        <DownloadFile
          downloadTemplateType={downloadTemplateType}
          downloadHeaderText={downloadHeaderText}
        />
      )}

      {SubContentComponent && <SubContentComponent {...props} />}
      {isStuck && (
        <InlineMessage className="mb-0 mt-24" variant="danger" withIcon>
          {t('txt_step_stuck_move_forward_message')}
        </InlineMessage>
      )}
      <UploadFile
        required
        stepId={stepId}
        selectedStep={selectedStep}
        formValues={formValues}
        dependencies={dependencies}
        savedAt={savedAt}
        ready={ready}
        uploadDescription={uploadDescription}
        setFormValues={setFormValues}
        handleSave={handleSave}
      />
    </div>
  );
};

const ExtraStaticSummaryStep =
  UploadFileStep as WorkflowSetupStaticProp<FormUploadFile>;

ExtraStaticSummaryStep.summaryComponent = UploadFileSummary;
ExtraStaticSummaryStep.defaultValues = {
  isValid: false,
  files: [],
  attachmentId: undefined
};
ExtraStaticSummaryStep.parseFormValues = parseFormValues;
ExtraStaticSummaryStep.isDefaultValues = isDefaultValues;

export default ExtraStaticSummaryStep;
