import FailedApiReload from 'app/components/FailedApiReload';
import ModalRegistry from 'app/components/ModalRegistry';
import { LOAD_MORE_PAGE_SIZE } from 'app/constants/constants';
import { BadgeColorType } from 'app/constants/enums';
import { classnames } from 'app/helpers';
import {
  Grid,
  GridRef,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import { orderBy } from 'lodash';
import {
  actionsWorkflow,
  useSelectAllChangeWorkflows
} from 'pages/_commons/redux/Workflow';
import {
  formatBadge,
  formatBubble,
  formatText,
  formatTruncate
} from 'pages/_commons/Utils/formatGridField';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';

interface IWorkflowIncludedModalProps {
  show: boolean;
  changeId: string;

  onClose?: () => void;
}
const WorkflowIncludedModal: React.FC<IWorkflowIncludedModalProps> = ({
  changeId,
  show,

  onClose
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const {
    workflows: _workflows,
    loading,
    error
  } = useSelectAllChangeWorkflows();
  const workflows = useMemo(
    () => orderBy(_workflows, ['updatedDate', 'name'], ['desc', 'asc']),
    [_workflows]
  );
  const total = useMemo(() => workflows.length, [workflows.length]);
  const [loadingMore, setLoadingMore] = useState<boolean>(true);
  const [page, setPage] = useState<number>(1);

  useEffect(() => {
    dispatch(
      actionsWorkflow.getChangeWorkflows({
        changeId,
        ignoreChangeNotFoundDetect: true
      })
    );
  }, [dispatch, changeId]);

  const handleApiReload = useCallback(() => {
    dispatch(
      actionsWorkflow.getChangeWorkflows({
        changeId,
        ignoreChangeNotFoundDetect: true
      })
    );
  }, [dispatch, changeId]);

  const columns = useMemo(
    () => [
      {
        id: 'name',
        Header: t('txt_workflow_name'),
        accessor: 'name',
        width: 312
      },
      {
        id: 'description',
        Header: t('txt_description'),
        accessor: formatTruncate(['description']),
        width: 347
      },
      {
        id: 'createdDate',
        Header: t('txt_created_date'),
        accessor: formatText(['firstRan'], { format: 'date' }),
        width: 150
      },
      {
        id: 'createdBy',
        Header: t('txt_created_by'),
        accessor: formatBubble(['firstRanBy']),
        width: 166
      },
      {
        id: 'updatedDate',
        Header: t('txt_last_updated_date'),
        accessor: formatText(['updatedDate'], { format: 'date' }),
        width: 150
      },
      {
        id: 'updatedBy',
        Header: t('txt_last_updated_by'),
        accessor: formatBubble(['updatedBy']),
        width: 166
      },
      {
        id: 'workflowType',
        Header: t('txt_type'),
        accessor: formatBadge(['workflowType'], {
          colorType: BadgeColorType.WorkflowType,
          noBorder: true
        }),
        width: 200
      },
      {
        id: 'status',
        Header: t('txt_status'),
        accessor: formatBadge(['status'], {
          colorType: BadgeColorType.WorkflowStatus
          //noBorder: true
        }),
        width: 200
      }
    ],
    [t]
  );

  const handleOnChangePage = () => {
    if (total <= LOAD_MORE_PAGE_SIZE * page) return;

    setLoadingMore(true);
    const timeoutId = setTimeout(() => {
      clearTimeout(timeoutId);

      setPage(page + 1);
      setLoadingMore(false);
    }, 300);
  };

  const gridRef = useRef<GridRef>(null);
  const handleBackToTop = useCallback(() => {
    gridRef.current?.gridBodyElement?.scrollTo({ top: 0, behavior: 'smooth' });
  }, []);
  const loadElement = useMemo(() => {
    if (total <= LOAD_MORE_PAGE_SIZE) return '';

    if (loadingMore) {
      return (
        <tr className="row-loading">
          <td className="py-16">
            <span className="loading" />
            <span>{t('txt_loading_more_workflows')}</span>
          </td>
        </tr>
      );
    }

    return (
      <tr className="row-loading">
        <td className="py-16">
          <span className="mr-4">
            {t('txt_end_of')} {t('txt_workflows')}.
          </span>
          <span>{t('txt_click')} &nbsp;</span>
          <span className="link text-decoration-none" onClick={handleBackToTop}>
            {t('txt_back_to_top')}.
          </span>
        </td>
      </tr>
    );
  }, [handleBackToTop, t, loadingMore, total]);

  return (
    <ModalRegistry
      id={`WorkflowIncludedModal_${changeId}`}
      lg
      scrollable={false}
      show={show}
      onAutoClosedAll={onClose}
      classes={{ content: classnames({ loading }) }}
    >
      <ModalHeader border closeButton onHide={onClose}>
        <ModalTitle>{t('txt_workflow_included')}</ModalTitle>
      </ModalHeader>
      <ModalBody>
        {!error && (
          <Grid
            ref={gridRef}
            classes={{ body: 'dls-grid-body--load-more' }}
            columns={columns}
            scrollable
            data={workflows.slice(0, page * LOAD_MORE_PAGE_SIZE)}
            loadElement={loadElement}
            onLoadMore={handleOnChangePage}
          />
        )}
        {error && (
          <FailedApiReload
            id="change-management-error"
            onReload={handleApiReload}
            className="mt-40 pt-40 d-flex flex-column justify-content-center align-items-center"
          />
        )}
      </ModalBody>
      <ModalFooter cancelButtonText={t('txt_close')} onCancel={onClose} />
    </ModalRegistry>
  );
};

export default WorkflowIncludedModal;
