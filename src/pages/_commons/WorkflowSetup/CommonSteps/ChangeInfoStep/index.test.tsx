import { render } from '@testing-library/react';
import React from 'react';
import ChangeInfoStep from './index';

jest.mock('./ExistingChange', () => ({
  __esModule: true,
  default: () => <div>ExistingChange component</div>
}));

jest.mock('app/_libraries/_dls/components/i18next/Trans', () => {
  return {
    __esModule: true,
    default: (props: any) => {
      const { keyTranslation } = props || {};
      return <div>{keyTranslation}</div>;
    }
  };
});

describe('WorkflowSetup > CommonsStep > ChangeInfoStep', () => {
  it('Should render TYPE TESTING', () => {
    const props = {} as any;
    const wrapper = render(
      <ChangeInfoStep
        {...props}
        workflow={{
          type: 'TYPE TESTING',
          workflowType: ''
        }}
      />
    );
    expect(
      wrapper.getByText('txt_change_info_step_header')
    ).toBeInTheDocument();
    // expect(wrapper.getByText('TYPE TESTING')).toBeInTheDocument();
  });

  it('Should render WORKFLOW TYPE', () => {
    const props = {} as any;
    const wrapper = render(
      <ChangeInfoStep
        {...props}
        {...({ headerText: 'txt_headerText' } as any)}
        workflow={{
          type: undefined,
          workflowType: 'WORKFLOW TYPE'
        }}
      />
    );
    expect(wrapper.getByText('txt_headerText')).toBeInTheDocument();
    // expect(wrapper.getByText('WORKFLOW TYPE')).toBeInTheDocument();
  });
});
