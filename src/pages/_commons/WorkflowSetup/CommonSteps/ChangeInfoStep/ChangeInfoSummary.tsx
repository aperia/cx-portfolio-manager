import GroupTextControl from 'app/components/DofControl/GroupTextControl';
import { GroupTextFormat } from 'app/constants/enums';
import { Button, useTranslation } from 'app/_libraries/_dls';
import isFunction from 'lodash.isfunction';
import React, { useCallback, useEffect, useState } from 'react';
import { FormChangeInfo } from '.';

const ChangeInfoSummary: React.FC<WorkflowSetupSummaryProps<FormChangeInfo>> =
  ({ formValues, onEditStep, dependencies, savedAt, stepId, selectedStep }) => {
    const { t } = useTranslation();

    const [workflowNote, setWorkflowNote] = useState(formValues?.workflowNote);
    const [changeDescription, setChangeDescription] = useState(
      formValues?.changeDescription
    );

    useEffect(() => {
      setWorkflowNote(formValues?.workflowNote);
      setChangeDescription(formValues?.changeDescription);
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [savedAt]);

    const handleEdit = () => {
      isFunction(onEditStep) && onEditStep();
    };

    const handleTruncateNote = useCallback(
      (isTruncated: boolean, showAll: boolean) => {
        if (!isTruncated) return;
        if (showAll) {
          setWorkflowNote(formValues?.workflowNote);
        } else {
          setWorkflowNote(formValues?.workflowNote?.replace(/\n/g, ''));
        }
      },
      [formValues?.workflowNote]
    );

    const handleTruncateDescription = useCallback(
      (isTruncated: boolean, showAll: boolean) => {
        if (!isTruncated) return;
        if (showAll) {
          setChangeDescription(formValues?.changeDescription);
        } else {
          setChangeDescription(
            formValues?.changeDescription?.replace(/\n/g, '')
          );
        }
      },
      [formValues?.changeDescription]
    );

    return (
      <div className="position-relative">
        <div className="absolute-top-right mt-n26 mr-n8">
          <Button variant="outline-primary" size="sm" onClick={handleEdit}>
            {t('txt_edit')}
          </Button>
        </div>
        <div className="row">
          <div className="col-12 col-lg-3">
            <GroupTextControl
              id="summary_changeName"
              input={{ value: formValues.changeName } as any}
              meta={{} as any}
              label={t('txt_name_of_change')}
              options={{ inline: false }}
            />
          </div>
          <div className="col-12 col-lg-3">
            <GroupTextControl
              id="summary_effectiveDate"
              input={{ value: formValues.effectiveDate } as any}
              meta={{} as any}
              label={t('txt_effective_date')}
              options={{ inline: false, format: GroupTextFormat.DateUTC }}
            />
          </div>
          <div className="col-12 col-lg-6 text-white-space-pre-line">
            <GroupTextControl
              id="summary_changeDescription"
              input={{ value: changeDescription } as any}
              meta={{} as any}
              label={t('txt_change_description')}
              options={{
                inline: false,
                lineTruncate: 2,
                ellipsisLessText: t('txt_less'),
                ellipsisMoreText: t('txt_more'),
                handleTruncateProp: handleTruncateDescription
              }}
            />
          </div>
          <div className="col-12 col-lg-6 text-white-space-pre-line">
            {stepId === selectedStep && (
              <GroupTextControl
                id="summary_workflowNote"
                input={{ value: workflowNote } as any}
                meta={{} as any}
                label={t('txt_workflow_note')}
                options={{
                  inline: false,
                  lineTruncate: 2,
                  ellipsisLessText: t('txt_less'),
                  ellipsisMoreText: t('txt_more'),
                  handleTruncateProp: handleTruncateNote
                }}
              />
            )}
          </div>
        </div>
      </div>
    );
  };

export default ChangeInfoSummary;
