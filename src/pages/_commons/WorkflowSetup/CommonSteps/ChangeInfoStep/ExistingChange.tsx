import ExistingChangeCardView from 'app/components/ExistingChangeCardView';
import FailedApiReload from 'app/components/FailedApiReload';
import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import { EXISTING_CHANGE_SORT_BY_LIST } from 'app/constants/constants';
import {
  unsavedExistedWorkflowSetupDataProps,
  unsavedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { classnames } from 'app/helpers';
import { useUnsavedChangeRegistry } from 'app/hooks';
import {
  Button,
  DropdownBaseChangeEvent,
  DropdownList,
  InlineMessage,
  TransDLS,
  useTranslation
} from 'app/_libraries/_dls';
import { isFunction } from 'lodash';
import {
  actionsWorkflowSetup,
  useSelectWorkflowInstance,
  useWorkflowChangeSetFilterSelector,
  useWorkflowChangeSetsSelector,
  useWorkflowChangeSetWithTemplateSelector,
  useWorkflowChangeStatusListSelector
} from 'pages/_commons/redux/WorkflowSetup';
import { defaultWorkflowChangeSetFilter } from 'pages/_commons/redux/WorkflowSetup/reducers';
import Paging from 'pages/_commons/Utils/Paging';
import SortOrder from 'pages/_commons/Utils/SortOrder';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';
import { FormChangeInfo } from '.';
import NewChangeModal from './NewChangeModal';
import WorkflowIncludedModal from './WorkflowIncludedModal';

interface ExistingChangeProps
  extends Pick<
    WorkflowSetupProps<FormChangeInfo>,
    | 'savedAt'
    | 'formValues'
    | 'setFormValues'
    | 'isEdit'
    | 'stepId'
    | 'selectedStep'
  > {
  workflowType: string;
}
const ExistingChange: React.FC<ExistingChangeProps> = ({
  stepId,
  selectedStep,
  workflowType,
  savedAt,
  formValues,
  isEdit,
  setFormValues
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const [initialValues, setInitialValues] = useState(formValues);
  const [changeIdWorkflowInclused, setChangeIdWorkflowInclused] = useState('');
  const [showNewChange, setShowNewChange] = useState(false);
  const [workflowNotes, setWorkflowNotes] = useState(
    formValues.changeSetId
      ? { [formValues.changeSetId]: formValues.workflowNote }
      : {}
  );
  const [expandedChangeId, setExpandedChangeId] = useState('');
  const existingChangeStatus = useWorkflowChangeStatusListSelector();
  const instance = useSelectWorkflowInstance();

  const dataFilter = useWorkflowChangeSetFilterSelector();
  const { changeSets, loading, error, total, totalItemFilter } =
    useWorkflowChangeSetsSelector();
  const changesLocked = useWorkflowChangeSetWithTemplateSelector();

  const { searchValue, page, pageSize, orderBy, changeStatus } = dataFilter;
  const hasDataFilter = changeStatus?.toLowerCase() !== 'all';
  const hasSearchOrDataFilter = !!searchValue || hasDataFilter;

  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;

  useEffect(() => {
    if (!formValues?.changeSetId) return;

    setWorkflowNotes({ [formValues.changeSetId]: formValues.workflowNote });
    setInitialValues(formValues);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [savedAt]);

  const unsaveOptions = useMemo(
    () =>
      ({
        formName:
          UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__SELECT_A_CHANGE__EDIT_CHANGE__EXISTED_WORKFLOW,
        priority: 1,
        confirmTitle: t('txt_confirm_edit'),
        confirmMessage: (
          <>
            <TransDLS keyTranslation="txt_workflow_setup_confirm_select_another_change">
              <strong>{{ changeName: initialValues?.changeName }}</strong>
            </TransDLS>
          </>
        )
      } as any),
    [t, initialValues?.changeName]
  );
  useUnsavedChangeRegistry(unsaveOptions, [
    !!initialValues.changeSetId &&
      initialValues.changeSetId !== formValues.changeSetId
  ]);
  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__SELECT_A_CHANGE__EDIT_CHANGE__EXISTED_WORKFLOW__CLOSE,
      priority: 1
    },
    [
      !!initialValues.changeSetId &&
        initialValues.changeSetId !== formValues.changeSetId,
      !!initialValues.changeSetId &&
        initialValues.workflowNote !== formValues.workflowNote
    ]
  );
  useUnsavedChangeRegistry(
    {
      ...unsavedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__SELECT_A_CHANGE__EDIT_CHANGE,
      priority: 1
    },
    [!initialValues.changeSetId && !!formValues.changeSetId]
  );

  const handleChangePage = useCallback(
    (pageNumber: number) => {
      dispatch(
        actionsWorkflowSetup.updateWorkflowChangeSetFilter({ page: pageNumber })
      );
      setExpandedChangeId('');
    },
    [dispatch]
  );

  const handleChangePageSize = useCallback(
    (pageSizeNumber: number) => {
      dispatch(
        actionsWorkflowSetup.updateWorkflowChangeSetFilter({
          pageSize: pageSizeNumber
        })
      );
    },
    [dispatch]
  );

  const handleFilterByChangeStatus = useCallback(
    (e: DropdownBaseChangeEvent) => {
      dispatch(
        actionsWorkflowSetup.updateWorkflowChangeSetFilter({
          changeStatus: e.target.value
        })
      );
      setExpandedChangeId('');
    },
    [dispatch]
  );

  const handleChangeSortBy = useCallback(
    (sortByValue: string) => {
      dispatch(
        actionsWorkflowSetup.updateWorkflowChangeSetFilter({
          sortBy: [sortByValue],
          orderBy: orderBy?.slice(0, 1)
        })
      );
      setExpandedChangeId('');
    },
    [dispatch, orderBy]
  );

  const handleChangeOrderBy = useCallback(
    (orderByValue: string) => {
      dispatch(
        actionsWorkflowSetup.updateWorkflowChangeSetFilter({
          orderBy: [orderByValue as OrderByType]
        })
      );
      setExpandedChangeId('');
    },
    [dispatch]
  );

  const handleSearch = useCallback(
    (val: string) => {
      dispatch(
        actionsWorkflowSetup.updateWorkflowChangeSetFilter({
          searchValue: val
        })
      );
      setExpandedChangeId('');
    },
    [dispatch]
  );

  const handleClearAndReset = useCallback(
    ({ keepSort = true }: { keepSort?: boolean } = {}) => {
      dispatch(actionsWorkflowSetup.clearWorkflowChangeSetFilter({ keepSort }));
    },
    [dispatch]
  );

  const handleApiReload = useCallback(
    async (callback?: any) => {
      await Promise.resolve(
        dispatch(actionsWorkflowSetup.getWorkflowChangeSetList(''))
      );

      isFunction(callback) && callback();
    },
    [dispatch]
  );

  const handleReloadChanges = () => {
    handleApiReload();
    keepRef.current.setFormValues({
      showChangeNoExistError: false,
      changeSetId: instance?.workflowInstanceDetails?.changeSetId || undefined,
      isValid: !!instance?.workflowInstanceDetails?.changeSetId
    });
  };

  useEffect(() => {
    if (stepId !== selectedStep) return;

    handleApiReload();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [handleApiReload, savedAt]);

  useEffect(() => {
    if (stepId === selectedStep) return;

    handleSearch('');
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [handleApiReload, stepId, selectedStep]);

  useEffect(() => {
    if (isEdit) return;

    handleApiReload();
  }, [handleApiReload, isEdit]);

  const simpleSearchRef = useRef<any>(null);
  useEffect(() => {
    if (!searchValue && simpleSearchRef?.current) {
      simpleSearchRef.current.clear();
    }
  }, [searchValue]);
  useEffect(() => {
    const changeId = formValues.changeSetId;
    if (loading || !changeId || stepId === selectedStep) return;

    dispatch(
      actionsWorkflowSetup.updateSelectedWorkflowChangeSetPage({
        selectedChangeId: changeId
      })
    );
  }, [dispatch, formValues.changeSetId, loading, stepId, selectedStep]);

  return (
    <div className={classnames({ loading })}>
      <div className="d-flex align-items-center">
        <h5>{t('txt_change_list')}</h5>
        <div className="ml-auto d-flex ">
          {total > 0 && (
            <SimpleSearch
              ref={simpleSearchRef}
              placeholder={t('txt_type_to_search')}
              onSearch={handleSearch}
              popperElement={
                <>
                  <p className="color-grey">{t('txt_search_description')}</p>
                  <ul className="search-field-item list-unstyled">
                    <li className="mt-16">ID</li>
                    <li className="mt-16">Name</li>
                    <li className="mt-16">Owner</li>
                  </ul>
                </>
              }
            />
          )}
          <Button
            className="ml-16 mr-n8"
            size="sm"
            variant="outline-primary"
            onClick={() => setShowNewChange(true)}
          >
            {t('txt_new_change')}
          </Button>
        </div>
      </div>
      {formValues?.showChangeNoExistError && (
        <InlineMessage className="mb-8 mt-24" variant="danger">
          <TransDLS keyTranslation="txt_select_a_change_not_found">
            <span
              onClick={handleReloadChanges}
              className="ml-4 link text-decoration-none"
            />
          </TransDLS>
        </InlineMessage>
      )}
      <div className="d-flex mb-4 mt-4">
        <div className="d-flex align-items-center pr-24 mt-12">
          <strong className="fs-14 color-grey mr-4">{t('txt_status')}:</strong>
          <DropdownList
            name="status"
            value={changeStatus}
            textField="description"
            onChange={handleFilterByChangeStatus}
            variant="no-border"
          >
            {existingChangeStatus.map((item, idx) => (
              <DropdownList.Item key={idx} label={item} value={item} />
            ))}
          </DropdownList>
        </div>
        {totalItemFilter > 0 && (
          <div className="d-flex ml-auto mt-12">
            <SortOrder
              hasFilter={hasSearchOrDataFilter}
              dataFilter={dataFilter}
              defaultDataFilter={defaultWorkflowChangeSetFilter}
              sortByFields={EXISTING_CHANGE_SORT_BY_LIST}
              onChangeSortBy={handleChangeSortBy}
              onChangeOrderBy={handleChangeOrderBy}
              onClearSearch={() => handleClearAndReset()}
            />
          </div>
        )}
      </div>
      <div>
        {changeSets.length > 0 ? (
          <>
            {changeSets.map(change => (
              <ExistingChangeCardView
                key={change.changeId}
                id={change.changeId}
                workflowNote={workflowNotes[change.changeId] || ''}
                value={change}
                isSelected={formValues?.changeSetId === change.changeId}
                isExpand={expandedChangeId === change.changeId}
                isLocked={
                  formValues?.changeSetId !== change.changeId &&
                  changesLocked
                    .filter(c => c !== initialValues.changeSetId)
                    .includes(change.changeId)
                }
                onSelected={isSelected => {
                  if (!isSelected) return;

                  setFormValues({
                    changeSetId: change.changeId,
                    changeName: change.changeName,
                    changeDescription: change.changeDescription,
                    effectiveDate: new Date(change.effectiveDate),
                    workflowNote: workflowNotes[change.changeId] || '',
                    isValid: !!change.changeId
                  });
                }}
                onExpand={isExpand =>
                  setExpandedChangeId(isExpand ? change.changeId : '')
                }
                onChangeWorkflowNote={workflowNote => {
                  setWorkflowNotes({
                    ...workflowNotes,
                    [change.changeId]: workflowNote
                  });
                  setFormValues({ workflowNote });
                }}
                onClickWorkflowIncluded={() =>
                  setChangeIdWorkflowInclused(change.changeId)
                }
              />
            ))}
            <Paging
              page={page}
              pageSize={pageSize}
              totalItem={totalItemFilter}
              onChangePage={handleChangePage}
              onChangePageSize={handleChangePageSize}
            />
          </>
        ) : (
          !loading &&
          !error && (
            <div className="d-flex flex-column justify-content-center">
              <div className="mt-40 mb-32">
                <NoDataFound
                  id="change-management-NoDataFound"
                  title={t('txt_no_changes_to_display')}
                  hasSearch={!!searchValue}
                  hasFilter={hasDataFilter}
                  linkTitle={hasSearchOrDataFilter && t('txt_clear_and_reset')}
                  onLinkClicked={() => handleClearAndReset()}
                />
              </div>
            </div>
          )
        )}
        {error && (
          <FailedApiReload
            id="change-management-error"
            onReload={handleApiReload}
            className="mt-40 pt-40 d-flex flex-column justify-content-center align-items-center"
          />
        )}
        {showNewChange && (
          <NewChangeModal
            show
            workflowType={workflowType}
            onClose={() => setShowNewChange(false)}
            onSaved={change => {
              setShowNewChange(false);
              setFormValues({
                changeSetId: change.changeSetId,
                changeName: change.changeName,
                changeDescription: change.changeDescription,
                effectiveDate: change.effectiveDate,
                isValid: !!change.changeSetId
              });
              handleClearAndReset({ keepSort: false });
              handleApiReload(() => {
                dispatch(
                  actionsWorkflowSetup.setNewChangeId(change.changeSetId)
                );
              });
            }}
          />
        )}
        {changeIdWorkflowInclused && (
          <WorkflowIncludedModal
            show
            changeId={changeIdWorkflowInclused}
            onClose={() => setChangeIdWorkflowInclused('')}
          />
        )}
      </div>
    </div>
  );
};

export default ExistingChange;
