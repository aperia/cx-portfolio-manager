import parseFormValues from './parseFormValues';

describe('WorkflowSetup > CommonsStep > ChangeInfoStep > parseFormValues', () => {
  it('changeDetail is undefined > should return empty', async () => {
    const workflowData = {
      data: {
        workflowSetupData: [
          {
            id: 'id-1',
            status: 'DONE',
            data: {},
            selected: false
          }
        ]
      }
    } as WorkflowSetupInstance;
    const id = '';
    const dispatch = jest.fn().mockResolvedValue({ payload: { changes: [] } });
    const result = await parseFormValues(workflowData, id, dispatch);
    expect(result).toBeUndefined();
  });

  it('workflowInstanceDetails is undefined > should return empty', async () => {
    const workflowData = {
      data: {
        workflowSetupData: [
          {
            id: 'id-1',
            status: 'DONE',
            data: {},
            selected: false
          }
        ]
      }
    } as WorkflowSetupInstance;
    const id = '';
    const dispatch = jest.fn().mockResolvedValue({ payload: { changes: [] } });
    const result = await parseFormValues(workflowData, id, dispatch);
    expect(result).toBeUndefined();
  });

  it('stepInfo is undefined > should return empty', async () => {
    const workflowData = {
      data: {
        workflowSetupData: [
          {
            id: 'id-1',
            status: 'DONE',
            data: {},
            selected: false
          }
        ]
      },
      workflowInstanceDetails: {
        id: 'wfiDetailId',
        notes: 'wfiDetailNotes',
        status: 'wfiDetailStatus'
      }
    } as WorkflowSetupInstance;
    const id = '';
    const dispatch = jest.fn().mockResolvedValue({ payload: { changes: [] } });
    const result = await parseFormValues(workflowData, id, dispatch);
    expect(result).toBeUndefined();
  });

  it('should return value', async () => {
    const workflowData = {
      data: {
        workflowSetupData: [
          {
            id: 'id-1',
            status: 'DONE',
            data: {},
            selected: false
          }
        ]
      },
      workflowInstanceDetails: {
        id: 'wfiDetailId',
        notes: 'wfiDetailNotes',
        status: 'wfiDetailStatus',
        changeSetId: 'changeId'
      }
    } as WorkflowSetupInstance;
    const id = 'id-1';
    const dispatch = jest
      .fn()
      .mockResolvedValue({ payload: { changes: [{ changeId: 'changeId' }] } });
    const result = await parseFormValues(workflowData, id, dispatch);
    const expectedValue = {
      isPass: true,
      isSelected: false,
      isNewChange: false,
      changeSetId: 'changeId',
      isCompletedWorkflow: false,
      isValid: true,
      workflowInstanceId: 'wfiDetailId',
      workflowNote: 'wfiDetailNotes'
    };
    expect(result).toEqual(expectedValue);
  });

  it('should return value with valid form', async () => {
    const workflowData = {
      data: {
        workflowSetupData: [
          {
            id: 'id-1',
            status: 'DONE',
            data: {},
            selected: false
          }
        ]
      },
      workflowInstanceDetails: {
        id: 'wfiDetailId',
        notes: 'wfiDetailNotes',
        status: 'wfiDetailStatus',
        changeSetId: 'changeId'
      }
    } as WorkflowSetupInstance;
    const id = 'id-1';
    const dispatch = jest.fn().mockResolvedValue({
      payload: {
        changes: [{ changeId: 'changeId', effectiveDate: '01/01/2021' }]
      }
    });
    const result = await parseFormValues(workflowData, id, dispatch);
    const expectedValue = {
      isPass: true,
      isSelected: false,
      isNewChange: false,
      changeSetId: 'changeId',
      isCompletedWorkflow: false,
      isValid: true,
      workflowInstanceId: 'wfiDetailId',
      workflowNote: 'wfiDetailNotes',
      changeDescription: undefined,
      changeName: undefined,
      effectiveDate: new Date('01/01/2021')
    };
    expect(result).toEqual(expectedValue);
  });

  it('should return value with invalid form', async () => {
    const workflowData = {
      data: {
        workflowSetupData: [
          {
            id: 'id-1',
            status: 'DONE',
            data: {},
            selected: false
          }
        ]
      },
      workflowInstanceDetails: {
        id: 'wfiDetailId',
        notes: 'wfiDetailNotes',
        status: 'wfiDetailStatus',
        changeSetId: 'changeId'
      }
    } as WorkflowSetupInstance;
    const id = 'id-1';
    const dispatch = jest.fn().mockResolvedValue({ payload: { changes: [] } });
    const result = await parseFormValues(workflowData, id, dispatch);
    const expectedValue = {
      isPass: true,
      isSelected: false,
      isNewChange: false,
      changeSetId: 'changeId',
      isCompletedWorkflow: false,
      isValid: false,
      workflowInstanceId: 'wfiDetailId',
      workflowNote: 'wfiDetailNotes'
    };
    expect(result).toEqual(expectedValue);
  });
});
