import { camelToText } from 'app/helpers';
import { TransDLS } from 'app/_libraries/_dls';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, { useMemo } from 'react';
import ChangeInfoSummary from './ChangeInfoSummary';
import ExistingChange from './ExistingChange';
import isEqualFormValues from './isEqualFormValues';
import parseFormValues from './parseFormValues';
import stepValueFunc from './stepValueFunc';

export interface FormChangeInfo {
  isValid?: boolean;
  changeName?: string;
  effectiveDate?: Date;
  changeDescription?: string;
  workflowNote?: string;
  changeSetId?: string;
  workflowInstanceId?: string;
  isCompletedWorkflow?: boolean;
  headerText?: string;
  showChangeNoExistError?: boolean;
}
const ChangeInfoStep: React.FC<WorkflowSetupProps<FormChangeInfo>> = ({
  stepId,
  selectedStep,
  savedAt,
  formValues,
  workflow,
  ready,
  isEdit,
  setFormValues,
  ...rest
}) => {
  const { headerText = 'txt_change_info_step_header' } = rest as any;
  const type = useMemo(() => {
    const _workflow = workflow as any;

    return camelToText(_workflow?.type || _workflow?.workflowType);
  }, [workflow]);

  return (
    <div>
      <div className="mt-8 pb-24 border-bottom">
        <p className="color-grey">
          <TransDLS keyTranslation={headerText}>
            <strong className="color-grey-d20">{{ type }}</strong>
          </TransDLS>
        </p>
      </div>

      <div className="pt-24">
        <ExistingChange
          stepId={stepId}
          selectedStep={selectedStep}
          isEdit={isEdit}
          workflowType={type}
          savedAt={savedAt}
          formValues={formValues}
          setFormValues={setFormValues}
        />
      </div>
    </div>
  );
};

const ExtraStaticChangeInfoStep =
  ChangeInfoStep as WorkflowSetupStaticProp<FormChangeInfo>;

ExtraStaticChangeInfoStep.summaryComponent = ChangeInfoSummary;
ExtraStaticChangeInfoStep.stepValue = stepValueFunc;
ExtraStaticChangeInfoStep.defaultValues = { isValid: false };
ExtraStaticChangeInfoStep.parseFormValues = parseFormValues;
ExtraStaticChangeInfoStep.isEqualFormValues = isEqualFormValues;

export default ExtraStaticChangeInfoStep;
