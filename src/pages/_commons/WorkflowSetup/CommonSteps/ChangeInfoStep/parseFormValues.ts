import { getWorkflowSetupStepStatus } from 'app/helpers';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';

const parseFormValues: WorkflowSetupStepFormDataFunc<any> = async (
  data,
  id,
  dispatch
) => {
  const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);
  if (!stepInfo) return;

  const {
    id: workflowInstanceId,
    status: workflowStatus,
    notes: workflowNote,
    changeSetId
  } = data?.workflowInstanceDetails;

  const result = (await Promise.resolve(
    dispatch(actionsWorkflowSetup.getWorkflowChangeSetList(''))
  )) as any;
  const allChangeSets: IWorkflowChangeDetail[] = result?.payload?.changes;
  const change = allChangeSets?.find(
    change => change.changeId === `${changeSetId}`
  );
  let changeValues;
  if (change) {
    changeValues = {
      changeName: change.changeName,
      changeDescription: change.changeDescription,
      effectiveDate: change.effectiveDate
        ? new Date(change?.effectiveDate)
        : undefined
    };
  }

  return {
    ...getWorkflowSetupStepStatus(stepInfo),
    ...changeValues,
    isNewChange: false,
    changeSetId: `${changeSetId}`,
    isCompletedWorkflow: workflowStatus?.toLowerCase() === 'complete',
    workflowInstanceId,
    workflowNote,
    isValid: !!change
  };
};

export default parseFormValues;
