import { act, fireEvent, render, RenderResult } from '@testing-library/react';
import { ChangeWorkflows } from 'app/types';
import { mockUseModalRegistryFnc } from 'app/utils';
import * as WorkflowHook from 'pages/_commons/redux/Workflow/select-hooks';
import React from 'react';
import * as reactRedux from 'react-redux';
import WorkflowIncludedModal from './WorkflowIncludedModal';

const mockUseModalRegistry = mockUseModalRegistryFnc();

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  const ActualReact = jest.requireActual('react');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    }),
    Grid: ActualReact.forwardRef(
      ({ loadElement, onLoadMore }: any, ref: React.Ref<HTMLDivElement>) => {
        return (
          <div ref={ref}>
            <div>{loadElement}</div>
            <button onClick={onLoadMore}>LoadMore</button>
          </div>
        );
      }
    )
  };
});

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');
const mockSelectAllChangeWorkflows = jest.spyOn(
  WorkflowHook,
  'useSelectAllChangeWorkflows'
);

describe('WorkflowSetup > CommonsStep > ChangeInfoStep > WorkflowIncludedModal', () => {
  const mockDispatch = jest.fn();
  const mockOnClose = jest.fn();
  const props = {
    changeId: '123',
    show: true,
    onClose: mockOnClose
  };

  const renderModal = (): RenderResult => {
    return render(
      <div>
        <WorkflowIncludedModal {...props} />
      </div>
    );
  };

  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
    mockUseModalRegistry.mockImplementation(() => [true, false]);
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
    mockUseModalRegistry.mockClear();
  });

  it('should open with grid without Loading More', () => {
    mockSelectAllChangeWorkflows.mockReturnValue({
      workflows: [] as any,
      loading: false
    } as ChangeWorkflows);

    const wrapper = renderModal();
    expect(
      wrapper.queryByText('txt_loading_more_workflows')
    ).not.toBeInTheDocument();
  });

  it('should open with grid with Loading More > click Load More > click Back to Top', () => {
    mockSelectAllChangeWorkflows.mockReturnValue({
      workflows: new Array(40),
      loading: false
    } as ChangeWorkflows);

    jest.useFakeTimers();

    const wrapper = renderModal();

    expect(
      wrapper.queryByText('txt_loading_more_workflows')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('LoadMore'));

    act(() => {
      jest.advanceTimersByTime(300);
    });

    fireEvent.click(wrapper.getByText('LoadMore'));

    expect(wrapper.queryByText('txt_back_to_top.')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('txt_back_to_top.'));
  });

  it('should show api error', () => {
    mockSelectAllChangeWorkflows.mockReturnValue({
      workflows: [] as any,
      loading: false,
      error: true
    } as ChangeWorkflows);

    const wrapper = renderModal();
    fireEvent.click(wrapper.queryByText('Reload')!);
    expect(wrapper.queryByText('Reload')).toBeInTheDocument();
  });
});
