import { FormChangeInfo } from '.';

const stepValueFunc: WorkflowSetupStepValueFunc<FormChangeInfo> = ({
  stepsForm: formValues
}) => {
  if (!formValues?.changeName) return '';

  return `${formValues.changeName}`;
};

export default stepValueFunc;
