import { fireEvent, render } from '@testing-library/react';
import React from 'react';
import ChangeInfoSummary from './ChangeInfoSummary';

const mockOnTruncateParams = jest.fn();
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    }),
    TruncateText: ({ onTruncate, children }: any) => (
      <div>
        {children}
        <button
          onFocus={() => onTruncate && onTruncate(...mockOnTruncateParams())}
        >
          On Truncate
        </button>
      </div>
    )
  };
});

describe('WorkflowSetup > CommonsStep > ChangeInfoStep > ChangeInfoSummary', () => {
  beforeEach(() => {
    // (isTruncated: boolean, showAll: boolean)
    mockOnTruncateParams.mockReturnValue([true, false]);
  });

  it('should has button edit > click on this', () => {
    const mockFunction = jest.fn();
    const props = {
      onEditStep: mockFunction,
      formValues: {
        changeDescription: 'changeDescription',
        workflowNote: 'workflowNote'
      }
    };
    const wrapper = render(<ChangeInfoSummary {...props} />);

    wrapper.queryAllByText(/On Truncate/).forEach(element => {
      fireEvent.focus(element!);
    });

    mockOnTruncateParams.mockReturnValue([false, false]);
    wrapper.queryAllByText(/On Truncate/).forEach(element => {
      fireEvent.focus(element!);
    });

    mockOnTruncateParams.mockReturnValue([true, true]);
    wrapper.queryAllByText(/On Truncate/).forEach(element => {
      fireEvent.focus(element!);
    });

    const editButon = wrapper.getByText('txt_edit');
    expect(editButon).toBeInTheDocument();
    fireEvent.click(editButon);
    expect(mockFunction).toHaveBeenCalled();
  });
});
