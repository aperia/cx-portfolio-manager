import isEqualFormValues from './isEqualFormValues';

describe('WorkflowSetup > CommonsStep > ChangeInfoStep > IsEqualFormValues', () => {
  it('Should render isEqualFormValues', () => {
    const result = isEqualFormValues(
      { changeSetId: '123' },
      { changeSetId: '123' }
    );
    expect(result).toEqual(true);
  });
});
