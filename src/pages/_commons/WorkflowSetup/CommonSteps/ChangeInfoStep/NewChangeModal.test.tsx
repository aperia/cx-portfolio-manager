import { screen } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';
import { mockThunkAction, renderWithMockStore, wait } from 'app/utils';
// import mockDateTooltipParams from 'app/utils/_mockComponent/mockDatePicker';
import 'app/utils/_mockComponent/mockModalRegistry';
import 'app/utils/_mockComponent/mockUseTranslation';
import mockDevice from 'app/utils/_mockHelperConstant/mockDevice';
import { DatePickerChangeEvent } from 'app/_libraries/_dls';
import { actionsChange } from 'pages/_commons/redux/Change';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import React from 'react';
import NewChangeModal from './NewChangeModal';

const mockActionsChange = mockThunkAction(actionsChange);
const mockActionsWorkflowSetup = mockThunkAction(actionsWorkflowSetup);

(function mockDOMMatrix() {
  class DOMMatrixMock {
    scale = jest.fn();
    translate = jest.fn();
  }
  global.DOMMatrix = DOMMatrixMock as any;
})();

jest.mock('app/_libraries/_dls/components/i18next/Trans', () => {
  return {
    __esModule: true,
    default: ({ keyTranslation }: any) => <div>{keyTranslation}</div>
  };
});

const mockDateTooltipParams = jest.fn();
jest.mock('app/_libraries/_dls/components/DatePicker', () => {
  const actualModule = jest.requireActual(
    'app/_libraries/_dls/components/DatePicker'
  );
  return {
    ...actualModule,
    __esModule: true,
    default: ({ onChange, dateTooltip, label, onBlur, ...props }: any) => (
      <div>
        <span>{label}</span>
        <input
          {...props}
          onClick={() => {
            const params = mockDateTooltipParams();

            !!dateTooltip && dateTooltip(...params);
          }}
          onChange={(e: any) => {
            onChange!({
              target: { value: new Date(e.target.value), name: e.target.name }
            } as DatePickerChangeEvent);
          }}
          onBlur={onBlur}
        />
      </div>
    )
  };
});

describe('WorkflowSetup > CommonsStep > ChangeInfoStep > NewChangeModal', () => {
  beforeEach(() => {
    mockDevice.isDevice = false;
    mockDateTooltipParams.mockReturnValue([new Date('01/01/2021')]);

    mockActionsChange('getChangeDateInfo');
    mockActionsWorkflowSetup('createNewChange', { payload: {} });
  });

  it('Should render NewChangeModal', async () => {
    mockDevice.isDevice = true;
    const wrapper = await renderWithMockStore(
      <NewChangeModal workflowType="Pricing" show={true} />,
      {
        initialState: {
          change: { changeDateInfo: { data: {}, loading: true } }
        }
      }
    );
    expect(wrapper.getByText('txt_new_change_details')).toBeInTheDocument();
  });

  it('Should show tooltip when focus effectiveDate', async () => {
    const wrapper = await renderWithMockStore(
      <NewChangeModal workflowType="Pricing" show={true} />,
      {
        initialState: {
          change: {
            changeDateInfo: {
              data: {
                minDate: '',
                maxDate: '',
                invalidDate: [
                  { date: '2021-01-01T00:00:00', reason: 'New year' },
                  { date: '', reason: 'Invalid date' }
                ]
              }
            }
          }
        }
      }
    );

    const effectiveDateInput = wrapper
      .getByText('txt_effective_date')
      .parentElement?.querySelector('input');
    const descriptionInput = wrapper.getByPlaceholderText(
      'txt_enter_description'
    );

    userEvent.type(descriptionInput, new Array(254).fill(0).join(''));

    expect(effectiveDateInput?.value).toEqual('');
    userEvent.click(effectiveDateInput!);

    mockDateTooltipParams.mockReturnValue([new Date('02/01/2021'), 'month']);
    userEvent.click(effectiveDateInput!);

    mockDateTooltipParams.mockReturnValue([new Date('02/01/2021'), 'year']);
    userEvent.click(effectiveDateInput!);

    expect(
      screen.getByText(
        'Date the change will be executed and made effective on the Optis platform.'
      )
    ).toBeInTheDocument();
  });

  it('Should submit save change success', async () => {
    const onSaved = jest.fn();

    const wrapper = await renderWithMockStore(
      <NewChangeModal workflowType="Pricing" show={true} onSaved={onSaved} />,
      {
        initialState: {
          change: {
            changeDateInfo: { data: {}, loading: false }
          }
        }
      }
    );

    const changeNameInput = wrapper
      .getByText('txt_name_of_change')
      .closest('.dls-input-container')
      ?.querySelector('input');
    const effectiveDateInput = wrapper
      .getByText('txt_effective_date')
      .parentElement?.querySelector('input');
    const descriptionInput = wrapper.getByPlaceholderText(
      'txt_enter_description'
    );

    userEvent.type(changeNameInput!, 'changeName');
    userEvent.type(effectiveDateInput!, '02/01/2021');
    userEvent.type(descriptionInput, 'description');
    userEvent.click(changeNameInput!);

    const saveBtn = wrapper.getByText('txt_save');
    userEvent.click(saveBtn);

    await wait();

    expect(onSaved).toBeCalled();
  });

  it('Should submit save change fail', async () => {
    mockActionsWorkflowSetup('createNewChange', { match: false, payload: {} });
    const onSaved = jest.fn();

    const wrapper = await renderWithMockStore(
      <NewChangeModal workflowType="Pricing" show={true} onSaved={onSaved} />,
      {
        initialState: {
          change: {
            changeDateInfo: { data: {}, loading: false }
          }
        }
      }
    );

    const changeNameInput = wrapper
      .getByText('txt_name_of_change')
      .closest('.dls-input-container')
      ?.querySelector('input');
    const effectiveDateInput = wrapper
      .getByText('txt_effective_date')
      .parentElement?.querySelector('input');
    const descriptionInput = wrapper.getByPlaceholderText(
      'txt_enter_description'
    );

    userEvent.type(changeNameInput!, 'changeName');
    userEvent.type(effectiveDateInput!, '02/01/2021');
    userEvent.type(descriptionInput, 'description');
    userEvent.click(changeNameInput!);

    const saveBtn = wrapper.getByText('txt_save');
    userEvent.click(saveBtn);

    await wait();

    expect(onSaved).not.toBeCalled();
  });

  it('Should close modal', async () => {
    const onClose = jest.fn();

    const wrapper = await renderWithMockStore(
      <NewChangeModal workflowType="Pricing" show={true} onClose={onClose} />,
      {
        initialState: {
          change: {
            changeDateInfo: { data: {}, loading: false }
          }
        }
      }
    );

    const closeBtn = wrapper.getByText('txt_cancel');
    userEvent.click(closeBtn);

    expect(onClose).toBeCalled();
  });
});
