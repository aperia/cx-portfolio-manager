import { FormChangeInfo } from '.';

const isEqualFormValues: WorkflowSetupIsEqualFormValuesFunc<FormChangeInfo> = (
  pre,
  next
) => {
  return pre?.changeSetId === next?.changeSetId;
};

export default isEqualFormValues;
