import EnhanceDatePicker from 'app/components/EnhanceDatePicker';
import FieldTooltip from 'app/components/FieldTooltip';
import ModalRegistry from 'app/components/ModalRegistry';
import TextAreaCountDown from 'app/components/TextAreaCountDown';
import { FormatTime } from 'app/constants/enums';
import { formatTimeDefault, isDevice } from 'app/helpers';
import { useFormValidations } from 'app/hooks';
import {
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  TextBox,
  Tooltip,
  TransDLS,
  useTranslation
} from 'app/_libraries/_dls';
import { isFunction } from 'lodash';
import {
  actionsChange,
  useSelectChangeDateInfo
} from 'pages/_commons/redux/Change';
import {
  actionsWorkflowSetup,
  useSelectCreateNewChangeStatus
} from 'pages/_commons/redux/WorkflowSetup';
import { actionsToast } from 'pages/_commons/ToastNotifications/redux';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { Detail } from 'react-calendar';
import { useDispatch } from 'react-redux';

interface ChangeInfo {
  changeSetId?: string;

  changeName?: string;
  effectiveDate?: Date;
  changeDescription?: string;
}
interface INewChangeModalProps {
  show: boolean;
  workflowType: string;

  onClose?: () => void;
  onSaved?: (change: ChangeInfo) => void;
}
const NewChangeModal: React.FC<INewChangeModalProps> = ({
  show,
  workflowType,

  onClose,
  onSaved
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const { loading } = useSelectCreateNewChangeStatus();
  const [change, setChange] = useState<ChangeInfo>({});

  const {
    data: { minDate: minDateSetting, maxDate: maxDateSetting, invalidDate },
    loading: dateInfoLoading
  } = useSelectChangeDateInfo();

  const invalidDateTooltips = useMemo(
    () =>
      invalidDate?.reduceRight((pre, val) => {
        pre[formatTimeDefault(val.date, FormatTime.DateServer)] = val.reason;
        return pre;
      }, {} as Record<string, string>) || {},
    [invalidDate]
  );

  const disabledRange = useMemo(
    () =>
      invalidDate?.map(d => {
        const fromDate = new Date(
          formatTimeDefault(d.date, FormatTime.DateFrom)
        );
        const toDate = new Date(formatTimeDefault(d.date, FormatTime.DateTo));
        return { fromDate, toDate };
      }),
    [invalidDate]
  );

  const minDate = useMemo(() => {
    return new Date(formatTimeDefault(minDateSetting!, FormatTime.DateServer));
  }, [minDateSetting]);
  const maxDate = useMemo(() => {
    return new Date(formatTimeDefault(maxDateSetting!, FormatTime.DateServer));
  }, [maxDateSetting]);

  useEffect(() => {
    dispatch(actionsChange.getChangeDateInfo(''));
  }, [dispatch]);

  const getDateTooltip = useCallback(
    (date: Date, view: Detail | undefined = 'month') => {
      if (view !== 'month') return;

      const tooltipContent =
        invalidDateTooltips[
          formatTimeDefault(date.toISOString(), FormatTime.DateServer)
        ];

      if (tooltipContent) return <Tooltip element={tooltipContent} />;
    },
    [invalidDateTooltips]
  );

  const errors = useMemo(
    () =>
      ({
        changeName: !change?.changeName?.trim() && {
          status: true,
          message: t('txt_required_validation', {
            fieldName: t('txt_name_of_change')
          })
        },
        effectiveDate: !change?.effectiveDate && {
          status: true,
          message: t('txt_required_validation', {
            fieldName: t('txt_effective_date')
          })
        },
        changeDescription: !change?.changeDescription?.trim() && {
          status: true,
          message: t('txt_required_validation', {
            fieldName: t('txt_change_description')
          })
        }
      } as Record<keyof ChangeInfo, IFormError>),
    [change, t]
  );
  const [touches, , setTouched] = useFormValidations<keyof ChangeInfo>(errors);

  const isValid = useMemo(
    () => !Object.keys(errors).some(k => errors[k as keyof ChangeInfo]),
    [errors]
  );

  const handleFieldChange = (field: keyof ChangeInfo) => (e: any) => {
    const value = e.target?.value;

    setChange(change => ({ ...change, [field]: value }));
  };

  const handleClose = () => {
    isFunction(onClose) && onClose();
  };

  const handleSave = async () => {
    const result: any = await Promise.resolve(
      dispatch(actionsWorkflowSetup.createNewChange(change))
    );
    const changeDetail = {
      ...change,
      ...result.payload
    } as ChangeInfo;

    const isSuccess =
      actionsWorkflowSetup.createNewChange.fulfilled.match(result);
    if (isSuccess) {
      dispatch(
        actionsToast.addToast({
          type: 'success',
          message: t('txt_change_created')
        })
      );
      isFunction(onSaved) && onSaved(changeDetail);
    } else {
      dispatch(
        actionsToast.addToast({
          type: 'error',
          message: t('txt_change_failed')
        })
      );
    }
  };

  const getFormError = (field: keyof ChangeInfo) =>
    touches[field]?.touched ? errors[field] : undefined;

  return (
    <ModalRegistry
      id="newChangeModal"
      md
      show={show}
      onAutoClosedAll={handleClose}
      classes={{ content: dateInfoLoading || loading ? 'loading' : '' }}
    >
      <ModalHeader border closeButton onHide={handleClose}>
        <ModalTitle>{t('txt_new_change_details')}</ModalTitle>
      </ModalHeader>
      <ModalBody>
        <p className="color-grey">
          <TransDLS keyTranslation="txt_set_up_new_change_desc">
            <strong className="color-grey-d20">{{ workflowType }}</strong>
          </TransDLS>
        </p>
        <div className="row mt-16">
          <div className="col-6 mb-16">
            <TextBox
              id="newChange__changeName"
              value={change?.changeName || ''}
              onChange={handleFieldChange('changeName')}
              required
              maxLength={50}
              label={t('txt_name_of_change')}
              onFocus={setTouched('changeName')}
              onBlur={setTouched('changeName', true)}
              error={getFormError('changeName')}
            />
          </div>

          <div className="col-6 mb-16">
            <FieldTooltip
              placement={isDevice ? 'right-start' : 'left-start'}
              element="Date the change will be executed and made effective on the Optis platform."
            >
              <EnhanceDatePicker
                id="newChange__effectiveDate"
                value={change?.effectiveDate}
                minDate={minDate}
                maxDate={maxDate}
                disabledRange={disabledRange}
                dateTooltip={getDateTooltip}
                onChange={handleFieldChange('effectiveDate')}
                required
                label={t('txt_effective_date')}
                onFocus={setTouched('effectiveDate')}
                onBlur={setTouched('effectiveDate', true)}
                error={getFormError('effectiveDate')}
              />
            </FieldTooltip>
          </div>
          <div className="col-12">
            <div className="mb-8 text-uppercase color-grey fw-500 fs-11">
              {t('txt_change_description')} <span className="color-red">*</span>
            </div>
            <TextAreaCountDown
              id="newChange__changeDescription"
              value={change?.changeDescription}
              placeholder={t('txt_enter_description')}
              onChange={handleFieldChange('changeDescription')}
              required
              rows={4}
              maxLength={255}
              onFocus={setTouched('changeDescription')}
              onBlur={setTouched('changeDescription', true)}
              error={getFormError('changeDescription')}
            />
          </div>
        </div>
      </ModalBody>

      <ModalFooter
        cancelButtonText={t('txt_cancel')}
        okButtonText={t('txt_save')}
        onCancel={handleClose}
        disabledOk={!isValid || dateInfoLoading}
        onOk={handleSave}
      />
    </ModalRegistry>
  );
};

export default NewChangeModal;
