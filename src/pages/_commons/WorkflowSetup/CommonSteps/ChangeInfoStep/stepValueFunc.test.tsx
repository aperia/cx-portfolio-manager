import stepValueFunc from './stepValueFunc';

describe('WorkflowSetup > CommonsStep > ChangeInfoStep > stepValueFunc', () => {
  it('should return empty', () => {
    const stepsForm = {
      changeName: ''
    };

    const result = stepValueFunc({ stepsForm });
    expect(result).toEqual('');
  });

  it('should return changeName', () => {
    const stepsForm = {
      changeName: 'changeName'
    };

    const result = stepValueFunc({ stepsForm });
    expect(result).toEqual('changeName');
  });
});
