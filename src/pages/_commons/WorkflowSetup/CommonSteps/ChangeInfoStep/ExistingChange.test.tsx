import {
  fireEvent,
  queryByText,
  render,
  RenderResult
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import 'app/utils/_mockComponent/mockFailedApiReload';
import 'app/utils/_mockComponent/mockNoDataFound';
import 'app/utils/_mockComponent/mockPaging';
import 'app/utils/_mockComponent/mockSortOrder';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/_common';
import React from 'react';
import * as reactRedux from 'react-redux';
import ExistingChange from './ExistingChange';

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');
const mockUseSelectWorkflowInstance = jest.spyOn(
  WorkflowSetup,
  'useSelectWorkflowInstance'
);
const MockWorkflowChangeSetFilterSelector = jest.spyOn(
  WorkflowSetup,
  'useWorkflowChangeSetFilterSelector'
);
const MockWorkflowChangeSetsSelector = jest.spyOn(
  WorkflowSetup,
  'useWorkflowChangeSetsSelector'
);
const MockWorkflowChangeSetWithTemplateSelector = jest.spyOn(
  WorkflowSetup,
  'useWorkflowChangeSetWithTemplateSelector'
);
const MockWorkflowChangeStatusListSelector = jest.spyOn(
  WorkflowSetup,
  'useWorkflowChangeStatusListSelector'
);

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    }),
    TransDLS: ({ keyTranslation, children }: any) => (
      <div>
        {keyTranslation}{' '}
        <div data-testid={`${keyTranslation}_children`}>{children}</div>
      </div>
    )
  };
});

jest.mock('app/components/ExistingChangeCardView', () => ({
  __esModule: true,
  default: ({
    onSelected,
    onExpand,
    onChangeWorkflowNote,
    onClickWorkflowIncluded
  }: any) => {
    const handleSelected = (isSelected: boolean) => {
      onSelected(isSelected);
    };

    const handleExpand = (isExpand: boolean) => {
      onExpand(isExpand);
    };
    return (
      <div>
        <div>ExistingChangeCardView_component</div>
        <button
          onClick={() => {
            handleSelected(false);
          }}
        >
          Change_UnSelected
        </button>
        <button
          onClick={() => {
            handleSelected(true);
          }}
        >
          Change_Selected
        </button>
        <button
          onClick={() => {
            handleExpand(false);
          }}
        >
          Change_Collapse
        </button>
        <button
          onClick={() => {
            handleExpand(true);
          }}
        >
          Change_Expand
        </button>
        <button onClick={onChangeWorkflowNote}>ChangeWorkflowNote</button>
        <button onClick={onClickWorkflowIncluded}>WorkflowIncluded</button>
      </div>
    );
  }
}));

jest.mock('./NewChangeModal', () => ({
  __esModule: true,
  default: ({ onClose, onSaved }: any) => {
    return (
      <div>
        <div onClick={onClose}>NewChangeModal_component</div>
        <button onClick={onSaved}>NewChangeModal_Saved</button>
      </div>
    );
  }
}));

jest.mock('./WorkflowIncludedModal', () => ({
  __esModule: true,
  default: ({ onClose }: any) => {
    return <div onClick={onClose}>WorkflowIncludedModal_component</div>;
  }
}));

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <ExistingChange {...props} />
    </div>
  );
};

describe('WorkflowSetup > CommonsStep > ChangeInfoStep > ExistingChange', () => {
  const mockDispatch = jest.fn();
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
    mockUseSelectWorkflowInstance.mockReturnValue({
      workflowInstanceDetails: {},
      data: {}
    } as any);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Should render with No data found', () => {
    MockWorkflowChangeStatusListSelector.mockReturnValue(['All', 'Complete']);
    MockWorkflowChangeSetFilterSelector.mockReturnValue({
      searchValue: '123123',
      page: 1,
      pageSize: 10,
      orderBy: ['asc'],
      changeStatus: 'All'
    });
    MockWorkflowChangeSetsSelector.mockReturnValue({
      changeSets: [],
      loading: false,
      error: false,
      total: 0,
      totalItemFilter: 0
    } as any);
    MockWorkflowChangeSetWithTemplateSelector.mockReturnValue(['change-1']);

    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        changeSetId: '1'
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent({ ...props });
    const noDataFound = wrapper.getByText('No data found');
    expect(noDataFound).toBeInTheDocument();
    fireEvent.click(noDataFound);
    expect(mockDispatch).toHaveBeenCalled();
  });

  it('Should render with Failed API', () => {
    MockWorkflowChangeStatusListSelector.mockReturnValue(['All', 'Complete']);
    MockWorkflowChangeSetFilterSelector.mockReturnValue({
      searchValue: '',
      page: 1,
      pageSize: 10,
      orderBy: ['asc'],
      changeStatus: 'All'
    });
    MockWorkflowChangeSetsSelector.mockReturnValue({
      changeSets: [],
      loading: false,
      error: true,
      total: 0,
      totalItemFilter: 0
    } as any);
    MockWorkflowChangeSetWithTemplateSelector.mockReturnValue(['change-1']);

    const props = {
      stepId: '1',
      selectedStep: '2',
      formValues: {},
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent({ ...props });
    expect(wrapper.getByText('Data load unsuccessful')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('Reload'));
    expect(mockDispatch).toHaveBeenCalled();
  });

  it('Should render with data', () => {
    MockWorkflowChangeStatusListSelector.mockReturnValue([
      'All_Status',
      'Complete_Status'
    ]);
    MockWorkflowChangeSetFilterSelector.mockReturnValue({
      searchValue: '',
      page: 1,
      pageSize: 10,
      orderBy: ['asc'],
      changeStatus: 'All_Status'
    });
    MockWorkflowChangeSetsSelector.mockReturnValue({
      changeSets: [
        {
          changeId: '11',
          changeName: 'change 11'
        }
      ],
      loading: false,
      error: false,
      total: 1,
      totalItemFilter: 1
    } as any);
    MockWorkflowChangeSetWithTemplateSelector.mockReturnValue(['11']);

    const props = {
      stepId: '1',
      selectedStep: '2',
      formValues: {
        isValid: true,
        changeName: 'new change',
        workflowInstanceId: '1',
        changeSetId: '11',
        workflowNote: 'note'
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent({ ...props });
    expect(
      wrapper.getByText('ExistingChangeCardView_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('WorkflowIncluded'));
    expect(
      wrapper.getByText('WorkflowIncludedModal_component')
    ).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('WorkflowIncludedModal_component'));
    expect(
      queryByText(wrapper.container, /WorkflowIncludedModal_component/i)
    ).not.toBeInTheDocument();

    fireEvent.click(wrapper.getByText('Change_Collapse'));
    fireEvent.click(wrapper.getByText('Change_Expand'));
    fireEvent.click(wrapper.getByText('ChangeWorkflowNote'));
    fireEvent.click(wrapper.getByText('Change_UnSelected'));
    fireEvent.click(wrapper.getByText('Change_Selected'));
    expect(props.setFormValues).toHaveBeenCalled();

    fireEvent.click(
      wrapper.container.querySelector('button.change-page-number') as Element
    );
    expect(mockDispatch).toHaveBeenCalledWith({
      payload: { page: 2 },
      type: 'workflowSetup/updateWorkflowChangeSetFilter'
    });

    fireEvent.click(
      wrapper.container.querySelector('button.change-page-size') as Element
    );
    expect(mockDispatch).toHaveBeenCalledWith({
      payload: { pageSize: 25 },
      type: 'workflowSetup/updateWorkflowChangeSetFilter'
    });

    fireEvent.click(
      wrapper.baseElement.querySelector(
        'button[class="sort-order--sort-btn"]'
      ) as Element
    );
    expect(mockDispatch).toHaveBeenCalled();

    fireEvent.click(
      wrapper.baseElement.querySelector(
        'button[class="sort-order--order-btn"]'
      ) as Element
    );
    expect(mockDispatch).toHaveBeenCalled();

    fireEvent.click(
      wrapper.baseElement.querySelector(
        'i[class="icon icon-search"]'
      ) as Element
    );
    expect(mockDispatch).toHaveBeenCalled();

    fireEvent.click(wrapper.getByText('Clear And Reset'));

    userEvent.click(wrapper.getByText('All_Status') as Element);
    expect(wrapper.getByText('Complete_Status')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('Complete_Status'));

    expect(mockDispatch).toHaveBeenCalled();
  });

  it('Should render with data > New Change', () => {
    MockWorkflowChangeStatusListSelector.mockReturnValue(['All', 'Complete']);
    MockWorkflowChangeSetFilterSelector.mockReturnValue({
      searchValue: '',
      page: 1,
      pageSize: 10,
      orderBy: ['asc'],
      changeStatus: 'All'
    });
    MockWorkflowChangeSetsSelector.mockReturnValue({
      changeSets: [
        {
          changeId: '11',
          changeName: 'change 11',
          changeDescription: 'change description',
          effectiveDate: '10-20-2021'
        },
        {
          changeId: '11',
          changeName: 'change 11'
        }
      ],
      loading: false,
      error: false,
      total: 1,
      totalItemFilter: 1
    } as any);
    MockWorkflowChangeSetWithTemplateSelector.mockReturnValue(['change-1']);

    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        isValid: true,
        changeName: 'new change',
        workflowInstanceId: '1',
        changeSetId: '11'
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent({ ...props });
    fireEvent.click(wrapper.getByText('txt_new_change'));
    expect(wrapper.getByText('NewChangeModal_component')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('NewChangeModal_component'));
    expect(
      queryByText(wrapper.container, /NewChangeModal_component/i)
    ).not.toBeInTheDocument();

    fireEvent.click(wrapper.getByText('txt_new_change'));
    expect(wrapper.getByText('NewChangeModal_component')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('NewChangeModal_Saved'));
    expect(mockDispatch).toHaveBeenCalled();
  });

  it('Should render with data > New Change with allChangeSets > changeId difference formValues > changeSetId', () => {
    MockWorkflowChangeStatusListSelector.mockReturnValue(['All', 'Complete']);
    MockWorkflowChangeSetFilterSelector.mockReturnValue({
      searchValue: '',
      page: 1,
      pageSize: 10,
      orderBy: ['asc'],
      changeStatus: 'All'
    });
    MockWorkflowChangeSetsSelector.mockReturnValue({
      changeSets: [
        {
          changeId: '22',
          changeName: 'change 11',
          changeDescription: 'change description',
          effectiveDate: '10-20-2021'
        },
        {
          changeId: '11',
          changeName: 'change 11'
        }
      ],
      loading: false,
      error: false,
      total: 1,
      totalItemFilter: 1
    } as any);
    MockWorkflowChangeSetWithTemplateSelector.mockReturnValue(['change-1']);

    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        isValid: true,
        changeName: 'new change',
        workflowInstanceId: '1',
        changeSetId: '11'
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent({ ...props });
    fireEvent.click(wrapper.getByText('txt_new_change'));
    expect(wrapper.getByText('NewChangeModal_component')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('NewChangeModal_component'));
    expect(
      queryByText(wrapper.container, /NewChangeModal_component/i)
    ).not.toBeInTheDocument();

    fireEvent.click(wrapper.getByText('txt_new_change'));
    expect(wrapper.getByText('NewChangeModal_component')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('NewChangeModal_Saved'));
    expect(mockDispatch).toHaveBeenCalled();
  });

  it('Should render with change locked', () => {
    MockWorkflowChangeStatusListSelector.mockReturnValue(['All', 'Complete']);
    MockWorkflowChangeSetFilterSelector.mockReturnValue({
      searchValue: '',
      page: 1,
      pageSize: 10,
      orderBy: ['asc'],
      changeStatus: 'All'
    });
    MockWorkflowChangeSetsSelector.mockReturnValue({
      changeSets: [
        {
          changeId: '11',
          changeName: 'change 11'
        },
        {
          changeId: '22',
          changeName: 'change 11',
          changeDescription: 'change description',
          effectiveDate: '10-20-2021'
        }
      ],
      loading: false,
      error: false,
      total: 1,
      totalItemFilter: 1
    } as any);
    MockWorkflowChangeSetWithTemplateSelector.mockReturnValue(['22']);

    const props = {
      isEdit: true,
      stepId: '1',
      selectedStep: '1',
      formValues: {
        isValid: true,
        changeName: 'new change',
        workflowInstanceId: '1',
        changeSetId: '11'
      },
      setFormValues: jest.fn()
    } as any;

    const wrapper = renderComponent({ ...props });
    fireEvent.click(wrapper.getByText('txt_new_change'));
    expect(wrapper.getByText('NewChangeModal_component')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('NewChangeModal_component'));
    expect(
      queryByText(wrapper.container, /NewChangeModal_component/i)
    ).not.toBeInTheDocument();

    fireEvent.click(wrapper.getByText('txt_new_change'));
    expect(wrapper.getByText('NewChangeModal_component')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('NewChangeModal_Saved'));
    expect(mockDispatch).toHaveBeenCalled();
  });

  it('Should render reload change set list error', () => {
    MockWorkflowChangeStatusListSelector.mockReturnValue(['All', 'Complete']);
    MockWorkflowChangeSetFilterSelector.mockReturnValue({
      searchValue: '',
      page: 1,
      pageSize: 10,
      orderBy: ['asc'],
      changeStatus: 'All'
    });
    MockWorkflowChangeSetsSelector.mockReturnValue({
      changeSets: [
        {
          changeId: '11',
          changeName: 'change 11'
        },
        {
          changeId: '22',
          changeName: 'change 11',
          changeDescription: 'change description',
          effectiveDate: '10-20-2021'
        }
      ],
      loading: false,
      error: false,
      total: 1,
      totalItemFilter: 1
    } as any);
    MockWorkflowChangeSetWithTemplateSelector.mockReturnValue(['22']);

    const props = {
      isEdit: true,
      stepId: '1',
      selectedStep: '1',
      formValues: {
        isValid: true,
        changeName: 'new change',
        workflowInstanceId: '1',
        changeSetId: '11',
        showChangeNoExistError: true
      },
      setFormValues: jest.fn()
    } as any;

    const wrapper = renderComponent({ ...props });
    fireEvent.click(
      wrapper
        .getByTestId('txt_select_a_change_not_found_children')
        .querySelector('span')!
    );
    expect(
      wrapper.getByText('txt_select_a_change_not_found')
    ).toBeInTheDocument();
  });
});
