import { fireEvent } from '@testing-library/react';
import { mockThunkAction, renderWithMockStore } from 'app/utils';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import React from 'react';
import { act } from 'react-dom/test-utils';
import DownloadFile from '.';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

jest.mock('app/helpers/fileUtilities', () => {
  const actualModule = jest.requireActual('app/helpers/fileUtilities');
  return {
    ...actualModule,
    downloadBlobFile: jest.fn()
  };
});
const mockActionsWorkflowSetup = mockThunkAction(actionsWorkflowSetup);

describe('_commons > WorkflowSetup > CommonSteps > UploadFileStep > DownloadFile', () => {
  it('Should render & click download', async () => {
    mockActionsWorkflowSetup('downloadTemplateFile', {
      match: true,
      payload: {
        payload: {
          fileStream: '123',
          mimeType: 'application/vnd.ms-excel',
          filename: 'abc.xlsx'
        }
      }
    });

    const props = {
      downloadTemplateType: 'manageTemporaryCreditLimitWorkflow',
      formValue: { file: { fileId: '1', idx: '1', status: '1', size: 1024 } }
    };
    const wrapper = await renderWithMockStore(<DownloadFile {...props} />);

    fireEvent.click(wrapper.getByText('txt_download_template'));
    expect(wrapper.getByText('txt_download_template')).toBeInTheDocument();
  });
  it('download file', async () => {
    mockActionsWorkflowSetup('downloadTemplateFile', { match: false });
    const props = {
      downloadTemplateType: 'manageTemporaryCreditLimitWorkflow',
      formValue: { file: { fileId: '1', idx: '1', status: '1', size: 1024 } }
    };
    const wrapper = await renderWithMockStore(<DownloadFile {...props} />);

    act(() => {
      fireEvent.click(wrapper.getByText('txt_download_template'));
    });
    expect(wrapper.getByText('txt_download_template')).toBeInTheDocument();
  });
  it('download file unsupported type', async () => {
    mockActionsWorkflowSetup('downloadTemplateFile', {
      match: true,
      payload: {
        payload: {
          fileStream: '123',
          mimeType: 'abc',
          filename: 'abc'
        }
      }
    });

    const props = {
      downloadTemplateType: 'manageTemporaryCreditLimitWorkflow',
      formValue: { file: { fileId: '1', idx: '1', status: '1', size: 1024 } }
    };
    const wrapper = await renderWithMockStore(<DownloadFile {...props} />);
    fireEvent.click(wrapper.getByText('txt_download_template'));
    expect(wrapper.getByText('txt_download_template')).toBeInTheDocument();
  });
});
