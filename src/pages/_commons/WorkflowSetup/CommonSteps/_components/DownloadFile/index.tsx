import { MIME_TYPE } from 'app/constants/mine-type';
import { base64toBlob, downloadBlobFile } from 'app/helpers/fileUtilities';
import { useTranslation } from 'app/_libraries/_dls';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import { actionsToast } from 'pages/_commons/ToastNotifications/redux';
import React from 'react';
import { useDispatch } from 'react-redux';

interface DownloadFileProps {
  downloadTemplateType?: string;
  downloadHeaderText?: string;
  className?: string;
}

const DownloadFileStep: React.FC<DownloadFileProps> = props => {
  const {
    downloadTemplateType,
    downloadHeaderText = 'txt_then_complete_the_template_with_the_account_information',
    className = 'border-bottom'
  } = props;
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const handleDownloadFile = async (event: any) => {
    event.stopPropagation();
    event.preventDefault();

    const response: any = await Promise.resolve(
      dispatch(
        actionsWorkflowSetup.downloadTemplateFile({
          downloadTemplateType
        })
      )
    );

    const { payload: file } = response || {};
    if (
      actionsWorkflowSetup.downloadTemplateFile.fulfilled.match(response) &&
      !!file.fileStream
    ) {
      const {
        fileStream = '',
        mimeType: mimeTypeDownload = '',
        filename: filenameDownload = ''
      } = file;

      let fileName = filenameDownload;
      if (filenameDownload.split('.').length < 2) {
        const ext = MIME_TYPE[mimeTypeDownload]?.extensions[0] || '';
        fileName = `${filenameDownload}.${ext}`;
      }
      const blol = base64toBlob(fileStream, mimeTypeDownload);
      downloadBlobFile(blol, fileName);
    } else {
      dispatch(
        actionsToast.addToast({
          type: 'error',
          message: t('txt_template_failed_to_download')
        })
      );
    }
  };

  return (
    <p className={`mt-8 color-grey pb-24 ${className}`}>
      <a href="#" className="link" onClick={handleDownloadFile}>
        {t('txt_download_template')}
      </a>
      {t(downloadHeaderText)}
    </p>
  );
};

export default DownloadFileStep;
