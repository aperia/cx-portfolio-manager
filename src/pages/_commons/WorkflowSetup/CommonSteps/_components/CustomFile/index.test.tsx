import '@testing-library/jest-dom';
import {
  act,
  cleanup,
  fireEvent,
  render,
  screen
} from '@testing-library/react';
import * as helper from 'app/_libraries/_dls/components/Upload/helper';
import { queryByClass } from 'app/_libraries/_dls/test-utils';
import 'app/_libraries/_dls/test-utils/mocks/mockCanvas';
import * as canvasTextWidth from 'app/_libraries/_dls/utils/canvasTextWidth';
import * as getElementAttribute from 'app/_libraries/_dls/utils/getElementAttribute';
import React from 'react';
import File from '.';

afterEach(cleanup);

const STATUS = helper.STATUS;

jest.mock('resize-observer-polyfill', () =>
  jest.requireActual('app/_libraries/_dls/test-utils/mocks/MockResizeObserver')
);

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => () => ({
  t: (text: string) => text
}));

const renderWrapper = (props: any) => {
  let wrapper;

  jest.useFakeTimers();
  act(() => {
    wrapper = render(<File {...props} />);
    jest.runAllTimers();
  });

  return wrapper as any;
};

const index = 1;

describe('File', () => {
  it('Change name & description', () => {
    const handleRemoveFile = jest.fn();
    const file = {
      idx: '1',
      name: 'name.txt',
      length: 1024,
      txtDescription: '',
      txtName: '',
      status: STATUS.valid,
      item: () => null
    };
    renderWrapper({
      onRemoveFile: handleRemoveFile,
      file,
      typeFiles: ['txt'],
      isEditFileName: true,
      index
    });
    const cpmName = screen.getByPlaceholderText(
      'txt_type_to_add_name'
    ) as HTMLInputElement;
    fireEvent.change(cpmName, { target: { value: '123' } });
    expect(cpmName.value).toBe('123');
  });

  it('Delete item', () => {
    const handleRemoveFile = jest.fn();
    const file = {
      idx: '1',
      name: 'name.txt',
      length: 1024,
      txtDescription: '',
      txtName: '',
      status: STATUS.valid,
      percentage: 20,
      item: () => null
    };
    renderWrapper({
      onRemoveFile: handleRemoveFile,
      file,
      typeFiles: ['txt'],
      isEditFileName: true,
      index
    });
    act(() => {
      screen.getAllByRole('button')[1].click();
    });

    expect(handleRemoveFile).toHaveBeenCalledWith(index, file.idx);
  });

  it('Download item', () => {
    const handleDownloadFile = jest.fn();
    const file = {
      idx: '1',
      name: 'name.txt',
      length: 1024,
      txtDescription: '',
      txtName: '',
      status: STATUS.valid,
      percentage: 20,
      item: () => null
    };
    renderWrapper({
      onDownloadFile: handleDownloadFile,
      file,
      typeFiles: ['txt'],
      isEditFileName: true,
      index
    });
    act(() => {
      screen.getAllByRole('button')[0].click();
    });

    expect(handleDownloadFile).toHaveBeenCalledWith(file.idx);
  });

  it('Disabled input name & description', () => {
    const handleRemoveFile = jest.fn();
    const file = {
      idx: '1',
      name: 'name.txt',
      length: 1024,
      txtName: '',
      txtDescription: 'description',
      status: STATUS.valid,
      item: () => null
    };
    renderWrapper({
      onRemoveFile: handleRemoveFile,
      file,
      typeFiles: ['txt'],
      index
    });
    const cpmName = screen.queryByPlaceholderText(
      'Type to add name'
    ) as HTMLInputElement;
    const cpmDescription = screen.queryByPlaceholderText(
      'Type to add description'
    ) as HTMLInputElement;

    expect(cpmName).toBeNull();
    expect(cpmDescription).toBeNull();
  });

  it('Render loading', () => {
    const handleRemoveFile = jest.fn();
    const file = {
      idx: '1',
      name: 'name.txt',
      length: 1024,
      txtName: '',
      txtDescription: 'description',
      status: STATUS.uploading,
      percentage: 20,
      item: () => null
    };
    const { container } = renderWrapper({
      onRemoveFile: handleRemoveFile,
      file,
      typeFiles: ['txt'],
      index
    });

    const progressBar = queryByClass(container, /file-progress uploading/);

    expect(window.getComputedStyle(progressBar!).width).toEqual('20%');
  });

  it('Render error', () => {
    const handleRemoveFile = jest.fn();
    const file = {
      idx: '1',
      name: 'name.txt',
      length: 1024,
      txtName: '',
      txtDescription: 'description',
      status: STATUS.error,
      percentage: 20,
      item: () => null
    };
    const { container } = renderWrapper({
      onRemoveFile: handleRemoveFile,
      file,
      typeFiles: ['pdf'],
      index
    });

    expect(queryByClass(container, /icon icon-error/)).toBeInTheDocument();
  });

  it('Render valid file', () => {
    const handleRemoveFile = jest.fn();
    const file = {
      idx: '1',
      name: 'name.txt',
      length: 1024,
      txtName: '',
      txtDescription: 'description',
      status: STATUS.valid,
      percentage: 20,
      item: () => null
    };
    const { container } = renderWrapper({
      onRemoveFile: handleRemoveFile,
      file,
      typeFiles: ['pdf'],
      index
    });

    expect(
      queryByClass(container, /icon icon-success/)
    ).not.toBeInTheDocument();
  });

  it('Render 2 line', () => {
    const handleRemoveFile = jest.fn();
    const file = {
      idx: '1',
      name: 'name.txt',
      length: 1024,
      status: STATUS.success,
      item: () => null
    };
    const wrapper = renderWrapper({
      onRemoveFile: handleRemoveFile,
      file: file,
      typeFiles: ['txt'],
      index: index
    });

    jest.spyOn(canvasTextWidth, 'default').mockImplementation(() => null);

    act(() => {
      let fake = 0;
      Object.defineProperty(Element.prototype, 'clientWidth', {
        get: () => {
          fake++;
          return fake % 2 === 0 ? 1 : 2;
        }
      });
      // trigger resize
      window.dispatchEvent(new Event('resize'));
    });
    expect(queryByClass(document.body, /default/)).toBeInTheDocument();

    act(() => {
      wrapper.rerender(
        <File
          onRemoveFile={handleRemoveFile}
          file={{ ...file, name: 'a.txt' }}
          typeFiles={['txt']}
          index={index}
        />
      );
      jest.runAllTimers();
    });
    expect(queryByClass(document.body, /default/)).not.toBeInTheDocument();
  });

  it('Render one line & text truncate', () => {
    const handleRemoveFile = jest.fn();
    const file = {
      idx: '1',
      name: 'name.txt',
      length: 1024,
      status: STATUS.success,
      item: () => null
    };
    act(() => {
      Object.defineProperty(Element.prototype, 'clientWidth', {
        value: 123
      });
      jest
        .spyOn(getElementAttribute, 'getAvailableWidth')
        .mockImplementation(() => 10);
      jest.spyOn(helper, 'getAllWidthChildren').mockImplementation(() => 1000);
    });
    renderWrapper({
      onRemoveFile: handleRemoveFile,
      file: file,
      typeFiles: ['txt'],
      index: index
    });

    screen.debug();
    expect(queryByClass(document.body, /text-truncate/)).toBeInTheDocument();
  });
});
