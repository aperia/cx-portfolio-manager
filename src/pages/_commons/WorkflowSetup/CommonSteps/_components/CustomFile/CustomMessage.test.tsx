import '@testing-library/jest-dom';
import { cleanup, render } from '@testing-library/react';
import { formatBytes } from 'app/_libraries/_dls/components/Upload/helper';
import React from 'react';
import CustomMessage from './CustomMessage';

afterEach(cleanup);

describe('WorkflowSetup > CommonSteps > UploadFileStep > CustomMessages', () => {
  it('is done', () => {
    const valid = { isTypeSupport: true };
    const status = { isDone: true };

    const { container } = render(
      <CustomMessage valid={valid} status={status} />
    );
    expect(container.textContent).toEqual('Uploaded');
  });

  it('is uploading', () => {
    const valid = { isTypeSupport: true };
    const status = { isUpload: true };

    const { container } = render(
      <CustomMessage valid={valid} status={status} />
    );
    expect(container.textContent).toEqual('txt_validating_file');
  });

  it('is invalid', () => {
    const valid = { isInvalid: true, isTypeSupport: true };
    const status = { isDone: false };

    const { container } = render(
      <CustomMessage valid={valid} status={status} />
    );
    expect(container.textContent).toEqual('txt_invalid_file');
  });

  it('is upload failed', () => {
    const valid = { isTypeSupport: true };
    const status = { isUploadFailed: true };

    const { container } = render(
      <CustomMessage valid={valid} status={status} />
    );
    expect(container.textContent).toEqual('Upload Failed');
  });

  it('is invalid type file & over size', () => {
    const valid = { isTypeSupport: false, isOverSize: true };
    const status = { maxFile: 1024, minFile: 0 };
    const txtMaxSize = formatBytes(status.maxFile);

    const { container } = render(
      <CustomMessage valid={valid} status={status} />
    );
    expect(container.textContent).toEqual(
      `Invalid file format and size exceeds ${txtMaxSize}`
    );
  });

  it('is invalid type file', () => {
    const valid = { isTypeSupport: false };
    const status = {};

    const { container } = render(
      <CustomMessage valid={valid} status={status} />
    );
    expect(container.textContent).toEqual('Invalid file format');
  });

  it('is over size', () => {
    const valid = { isTypeSupport: true, isOverSize: true };
    const status = { maxFile: 1024 };
    const txtMaxSize = formatBytes(status.maxFile);

    const { container } = render(
      <CustomMessage valid={valid} status={status} />
    );
    expect(container.textContent).toEqual(`File size exceeds ${txtMaxSize}`);
  });

  it('is under size', () => {
    const valid = { isTypeSupport: true, isUnderSize: true };
    const status = { minFile: 1024 };
    const txtMinSize = formatBytes(status.minFile);

    const { container } = render(
      <CustomMessage valid={valid} status={status} />
    );
    expect(container.textContent).toEqual(`File size exceeds ${txtMinSize}`);
  });

  it('is valid file', () => {
    const valid = { isTypeSupport: true };
    const status = {};

    const { container } = render(
      <CustomMessage valid={valid} status={status} />
    );
    expect(container.textContent).toEqual('Valid');
  });
});
