import userEvent from '@testing-library/user-event';
import * as Helpers from 'app/helpers/fileUtilities';
import { workflowTemplateService } from 'app/services/workflowTemplate.service';
import { mockThunkAction, renderWithMockStore } from 'app/utils';
import { actionsWorkflow } from 'pages/_commons/redux/Workflow';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import React from 'react';
import UploadFile from '.';

jest.mock('./constants', () => ({
  ...jest.requireActual('./constants'),
  INTERVAL_FILE_PROGRESS: 1,
  INTERVAL_FILE_VALIDATION: 1
}));
const actionsWorkflowSetupMock = mockThunkAction(actionsWorkflowSetup);
const actionsWorkflowMock = mockThunkAction(actionsWorkflow);
const downloadBlobFile = jest.spyOn(Helpers, 'downloadBlobFile');

describe('UploadFile', () => {
  it('should render empty props', async () => {
    const props = {
      formValues: {},
      setFormValues: jest.fn(),
      handleSave: jest.fn(),
      savedAt: 123,
      isSelected: false,
      dependencies: {}
    };

    const { getByText } = await renderWithMockStore(<UploadFile {...props} />);
    expect(getByText('Choose File')).toBeInTheDocument();
  });

  it('should render with a file and delete online success', async () => {
    const deleteTemplateFile = actionsWorkflowSetupMock('deleteTemplateFile', {
      match: true
    });
    const props = {
      formValues: {
        files: [
          {
            idx: 'id',
            name: 'file.xlsx',
            size: 1024,
            length: 1024,
            percentage: 100,
            item: () => null
          }
        ]
      },
      setFormValues: jest.fn(),
      handleSave: jest.fn(),
      savedAt: 123,
      isSelected: false,
      dependencies: {}
    };

    const { container, getByText } = await renderWithMockStore(
      <UploadFile {...props} />
    );
    expect(getByText('file.xlsx')).toBeInTheDocument();

    userEvent.click(container.querySelector('.file-item .icon.icon-close')!);
    expect(deleteTemplateFile).toBeCalled();
  });

  it('should render with a file and delete online false', async () => {
    const deleteTemplateFile = actionsWorkflowSetupMock('deleteTemplateFile', {
      match: false
    });
    const props = {
      formValues: {
        files: [
          {
            idx: 'id',
            name: 'file.xlsx',
            size: 1024,
            length: 1024,
            percentage: 100,
            item: () => null
          }
        ]
      },
      setFormValues: jest.fn(),
      handleSave: jest.fn(),
      savedAt: 123,
      isSelected: false,
      dependencies: {}
    };

    const { container, getByText } = await renderWithMockStore(
      <UploadFile {...props} />
    );
    expect(getByText('file.xlsx')).toBeInTheDocument();

    userEvent.click(container.querySelector('.file-item .icon.icon-close')!);
    expect(deleteTemplateFile).toBeCalled();
  });

  it('should remove offline', async () => {
    const props = {
      formValues: {
        files: [
          {
            idx: 'id',
            name: 'file.csv',
            size: 1024,
            length: 1024,
            status: 1,
            percentage: 100,
            item: () => null
          }
        ]
      },
      setFormValues: jest.fn(),
      handleSave: jest.fn(),
      savedAt: 123,
      isSelected: false,
      dependencies: {}
    };

    const { container, getByText } = await renderWithMockStore(
      <UploadFile {...props} />
    );
    expect(getByText('file.csv')).toBeInTheDocument();

    userEvent.click(container.querySelector('.file-item .icon.icon-close')!);
  });

  it('should check valid when change file', async () => {
    const file = new File(['test'], 'file.xlsx', { type: 'xlsx' } as any);
    jest
      .spyOn(workflowTemplateService, 'uploadWorkflowTemplate')
      .mockImplementation(((formData: any, { onUploadProgress }: any) => {
        onUploadProgress({ loaded: 100, total: 100 });
        return Promise.resolve({
          idx: 'id',
          data: { attachment: [file] }
        });
      }) as any);
    jest.useFakeTimers();
    const props = {
      formValues: {},
      setFormValues: jest.fn(),
      handleSave: jest.fn(),
      savedAt: 123,
      isSelected: false,
      dependencies: {}
    };

    const { container, queryByText, rerender } = await renderWithMockStore(
      <UploadFile {...props} />
    );
    expect(queryByText('file.xlsx')).not.toBeInTheDocument();

    const input = container.querySelector('input');
    userEvent.upload(input!, file);
    rerender(<UploadFile {...props} formValues={{ files: [file] as any }} />);
    jest.runAllTicks();
    jest.runAllTimers();

    expect(queryByText('file.xlsx')).toBeInTheDocument();

    jest.useRealTimers();
  });

  it('should invalid when change file but data responce is empty attachment', async () => {
    const file = new File(['test'], 'file.xlsx', { type: 'xlsx' });
    jest
      .spyOn(workflowTemplateService, 'uploadWorkflowTemplate')
      .mockImplementation(((formData: any, { onUploadProgress }: any) => {
        onUploadProgress({ loaded: 100, total: 100 });
        return Promise.resolve({
          idx: 'id',
          data: { attachment: [] }
        });
      }) as any);
    jest.useFakeTimers();
    const props = {
      formValues: {},
      setFormValues: jest.fn(),
      handleSave: jest.fn(),
      savedAt: 123,
      isSelected: false,
      dependencies: {}
    };

    const { container, queryByText, rerender } = await renderWithMockStore(
      <UploadFile {...props} />
    );
    expect(queryByText('file.xlsx')).not.toBeInTheDocument();

    const input = container.querySelector('input');
    userEvent.upload(input!, file);
    rerender(<UploadFile {...props} formValues={{ files: [file] as any }} />);
    jest.runAllTicks();
    jest.runAllTimers();

    expect(queryByText('file.xlsx')).toBeInTheDocument();

    jest.useRealTimers();
  });

  it('should wait for next validation check', async () => {
    const file = new File(['test'], 'file.xlsx', { type: 'xlsx' });
    jest
      .spyOn(workflowTemplateService, 'uploadWorkflowTemplate')
      .mockImplementationOnce(((formData: any, { onUploadProgress }: any) => {
        onUploadProgress({ loaded: 100, total: 100 });
        return Promise.resolve({
          idx: 'id',
          data: {
            attachment: [
              {
                ...file,
                validationMessages: [
                  { field: 'field1', message: 'message1' },
                  { field: 'Extension', message: 'invalid Extension' }
                ]
              }
            ]
          }
        });
      }) as any);
    jest.spyOn(workflowTemplateService, 'getFileDetails').mockResolvedValue({
      data: { attachments: [{ status: 'REVIEW' }] }
    } as any);
    jest.useFakeTimers();
    const props = {
      formValues: {},
      setFormValues: jest.fn(),
      handleSave: jest.fn(),
      savedAt: 123,
      isSelected: false,
      dependencies: {}
    };

    const { container, queryByText, rerender } = await renderWithMockStore(
      <UploadFile {...props} />
    );
    expect(queryByText('file.xlsx')).not.toBeInTheDocument();

    const input = container.querySelector('input');
    userEvent.upload(input!, file);
    rerender(<UploadFile {...props} formValues={{ files: [file] as any }} />);
    jest.runAllTicks();
    jest.runTimersToTime(2);

    expect(queryByText('file.xlsx')).toBeInTheDocument();

    jest.useRealTimers();
  });

  it('should check valid when change file with validation success', async () => {
    const file = new File(['test'], 'file.xlsx', { type: 'xlsx' });
    jest
      .spyOn(workflowTemplateService, 'uploadWorkflowTemplate')
      .mockImplementationOnce(((formData: any, { onUploadProgress }: any) => {
        onUploadProgress({ loaded: 100, total: 100 });
        return Promise.resolve({
          idx: 'id',
          data: {
            attachment: [
              {
                ...file,
                validationMessages: [
                  { field: 'field1', message: 'message1' },
                  { field: 'Extension', message: 'invalid Extension' }
                ]
              }
            ]
          }
        });
      }) as any);
    jest.spyOn(workflowTemplateService, 'getFileDetails').mockResolvedValue({
      data: { attachments: [{ status: 'VALID' }] }
    } as any);
    jest.useFakeTimers();
    const props = {
      formValues: { files: [{ idx: '12', percentage: 100 } as any] },
      setFormValues: jest.fn(),
      handleSave: jest.fn(),
      savedAt: 123,
      isSelected: false,
      dependencies: {}
    };

    const { container, queryByText, rerender } = await renderWithMockStore(
      <UploadFile {...props} />
    );
    expect(queryByText('file.xlsx')).not.toBeInTheDocument();

    const input = container.querySelector('input');
    userEvent.upload(input!, file);
    rerender(<UploadFile {...props} formValues={{ files: [file] as any }} />);
    jest.runAllTicks();
    jest.runTimersToTime(2);

    expect(queryByText('file.xlsx')).toBeInTheDocument();
    rerender(<UploadFile {...props} formValues={{ files: [] }} />);
    expect(queryByText('file.xlsx')).not.toBeInTheDocument();

    jest.useRealTimers();
  });

  it('should check valid when change file with validation message', async () => {
    const file = new File(['test'], 'file.xlsx', { type: 'xlsx' });
    jest
      .spyOn(workflowTemplateService, 'uploadWorkflowTemplate')
      .mockImplementationOnce(((formData: any, { onUploadProgress }: any) => {
        onUploadProgress({ loaded: 100, total: 100 });
        return Promise.resolve({
          idx: 'id',
          data: {
            attachment: [
              {
                ...file,
                validationMessages: [
                  { field: 'field1', message: 'message1' },
                  { field: 'Extension', message: 'invalid Extension' }
                ]
              }
            ]
          }
        });
      }) as any);
    jest.spyOn(workflowTemplateService, 'getFileDetails').mockResolvedValue({
      data: {
        attachments: [
          { status: 'INVALID', validationMessages: [{ message: 'message' }] }
        ]
      }
    } as any);
    jest.useFakeTimers();
    const props = {
      formValues: {},
      setFormValues: jest.fn(),
      handleSave: jest.fn(),
      savedAt: 123,
      isSelected: false,
      dependencies: {}
    };

    const { container, queryByText, rerender } = await renderWithMockStore(
      <UploadFile {...props} />
    );
    expect(queryByText('file.xlsx')).not.toBeInTheDocument();

    const input = container.querySelector('input');
    userEvent.upload(input!, file);
    rerender(<UploadFile {...props} formValues={{ files: [file] as any }} />);
    jest.runAllTicks();
    jest.runTimersToTime(2);

    expect(queryByText('file.xlsx')).toBeInTheDocument();

    jest.useRealTimers();
  });

  it('should check invalid when change file with fail to upload', async () => {
    const file = new File(['test'], 'file.xlsx', { type: 'xlsx' });
    jest
      .spyOn(workflowTemplateService, 'uploadWorkflowTemplate')
      .mockRejectedValueOnce({});
    jest.useFakeTimers();
    const props = {
      formValues: {},
      setFormValues: jest.fn(),
      handleSave: jest.fn(),
      savedAt: 123,
      isSelected: false,
      dependencies: {}
    };

    const { container, queryByText, rerender } = await renderWithMockStore(
      <UploadFile {...props} />
    );
    expect(queryByText('file.xlsx')).not.toBeInTheDocument();
    const input = container.querySelector('input');
    userEvent.upload(input!, file);

    rerender(<UploadFile {...props} formValues={{ files: [file] as any }} />);
    jest.runAllTicks();
    jest.runAllTimers();

    expect(queryByText('file.xlsx')).toBeInTheDocument();
    jest.useRealTimers();
  });

  it('should get file detail when edit wf', async () => {
    jest.spyOn(workflowTemplateService, 'getFileDetails').mockResolvedValue({
      data: {
        attachments: [
          { id: 'id', status: 'Valid', filename: 'file.xlsx', fileSize: 123 }
        ]
      }
    } as any);
    jest.useFakeTimers();
    const props = {
      formValues: {
        attachmentId: '12',
        files: [{ idx: '12', percentage: 90 } as any]
      },
      setFormValues: jest.fn(),
      handleSave: jest.fn(),
      savedAt: 123,
      isSelected: false,
      dependencies: {}
    };

    await renderWithMockStore(<UploadFile {...props} />);
    jest.useRealTimers();
  });

  it('should show download file success', async () => {
    const downloadAttachment = actionsWorkflowMock('downloadAttachment', {
      match: true
    });
    jest.spyOn(workflowTemplateService, 'getFileDetails').mockResolvedValue({
      data: {
        attachments: [
          { id: 'id', status: 'Valid', filename: 'file.xlsx', fileSize: 123 }
        ]
      }
    } as any);
    const props = {
      formValues: {
        attachmentId: '12',
        files: [
          {
            id: 'id',
            status: 0,
            filename: 'file.xlsx',
            fileSize: 123,
            percentage: 100
          } as any
        ]
      },
      setFormValues: jest.fn(),
      handleSave: jest.fn(),
      savedAt: 123,
      isSelected: false,
      dependencies: {}
    };

    const { container } = await renderWithMockStore(<UploadFile {...props} />);

    userEvent.click(container.querySelector('.icon.icon-download')!);
    actionsWorkflowMock('downloadAttachment', {
      match: true,
      payload: { payload: { filename: 'file.xlsx' } }
    });
    userEvent.click(container.querySelector('.icon.icon-download')!);
    expect(downloadAttachment).toBeCalled();
  });

  it('should show download file fail', async () => {
    downloadBlobFile.mockReturnValue();
    const downloadAttachment = actionsWorkflowMock('downloadAttachment', {
      match: false
    });
    jest.spyOn(workflowTemplateService, 'getFileDetails').mockResolvedValue({
      data: {
        attachments: [
          { id: 'id', status: 'Valid', filename: 'file.xlsx', fileSize: 123 }
        ]
      }
    } as any);
    const props = {
      formValues: {
        attachmentId: '12',
        files: [
          {
            id: 'id',
            status: 0,
            filename: 'file.xlsx',
            fileSize: 123,
            percentage: 100
          } as any
        ]
      },
      setFormValues: jest.fn(),
      handleSave: jest.fn(),
      savedAt: 123,
      isSelected: false,
      dependencies: {}
    };

    const { container } = await renderWithMockStore(<UploadFile {...props} />);

    userEvent.click(container.querySelector('.icon.icon-download')!);
    expect(downloadAttachment).toBeCalled();
  });
});
