import { StateFile } from 'app/_libraries/_dls/components/Upload/File';
import { Options, STATUS } from 'app/_libraries/_dls/components/Upload/helper';
import { isArray, isEmpty } from 'lodash';

export const getStatusFileFromValidationMessages = (
  validationMessages: any[]
) => {
  let newStatus: 0 | 1 = STATUS.valid;

  if (!isEmpty(validationMessages)) {
    validationMessages?.forEach(
      (validation: { field: string; message: string }) => {
        if (validation.field === 'Extension') {
          newStatus = STATUS.invalid;
        }
      }
    );
  }

  return newStatus;
};

export const getValid = (file: StateFile, options?: Options) => {
  const { name = '', size = 0 } = file;
  const { maxFile, typeFiles, minFile } = options || {};
  const extension = (name.split('.').pop() || '').toLocaleLowerCase();

  const isTypeSupport = Boolean(
    typeFiles && isArray(typeFiles)
      ? typeFiles.map(type => type.toLocaleLowerCase()).includes(extension)
      : true
  );

  const isOverSize = Boolean(maxFile ? maxFile < size : false);
  const isUnderSize = Boolean(minFile ? minFile > size : false);

  return {
    isInvalid: Boolean(
      !isTypeSupport ||
        isOverSize ||
        isUnderSize ||
        file.status === STATUS.invalid
    ),
    isTypeSupport,
    isOverSize,
    isUnderSize
  };
};
