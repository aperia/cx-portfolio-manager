export const TYPE_FILE = ['xlsx'];
export const MAX_FILE = 50 * 1024 * 1024;
export const INTERVAL_FILE_PROGRESS = 1000;
export const INTERVAL_FILE_VALIDATION = 1000;
export const VERIFIED_STATUSES_RESPONSE = ['ready', 'invalid'];
export const INVALID_STATUS = 'invalid';
