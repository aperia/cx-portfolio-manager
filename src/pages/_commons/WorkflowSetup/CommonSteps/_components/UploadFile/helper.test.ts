import { StateFile } from 'app/_libraries/_dls/components/Upload/File';
import { Options, STATUS } from 'app/_libraries/_dls/components/Upload/helper';
import { getStatusFileFromValidationMessages, getValid } from './helpers';

describe('UploadFileStep > helper', () => {
  it('getStatusFileFromValidationMessages', () => {
    const data: any[] = [
      {
        field: 'Extension',
        message: 'message'
      },
      {
        field: 'fileSize',
        message: 'message'
      }
    ];
    const result = getStatusFileFromValidationMessages(data);

    expect(result).toEqual(STATUS.invalid);
  });

  it('getValid', () => {
    const file: StateFile = {
      idx: 'idx',
      item: jest.fn(),
      length: 1,
      name: 'abc.xlsx',
      status: STATUS.valid,
      size: 1024 * 50
    };
    const options: Options = {
      maxFile: 500 * 1024,
      minFile: 0,
      typeFiles: ['xlsx']
    };

    const result = getValid(file, options);

    expect(result.isInvalid).toEqual(false);
    expect(result.isTypeSupport).toEqual(true);
    expect(result.isOverSize).toEqual(false);
    expect(result.isUnderSize).toEqual(false);
  });

  it('getValid default options', () => {
    const file: StateFile = {
      idx: 'idx',
      item: jest.fn(),
      length: 1,
      name: 'abc.xlsx',
      status: STATUS.valid,
      size: 1024 * 50
    };
    const options = undefined;

    const result = getValid(file, options);

    expect(result.isInvalid).toEqual(false);
    expect(result.isTypeSupport).toEqual(true);
    expect(result.isOverSize).toEqual(false);
    expect(result.isUnderSize).toEqual(false);
  });

  it('getValid wrong options', () => {
    const file: StateFile = {
      idx: 'idx',
      item: jest.fn(),
      length: 1,
      name: 'abc.xlsx',
      status: STATUS.valid,
      size: 1024 * 50
    };
    const options: Options = { minFile: 1024 * 100 };

    const result = getValid(file, options);

    expect(result.isInvalid).toEqual(true);
    expect(result.isTypeSupport).toEqual(true);
    expect(result.isOverSize).toEqual(false);
    expect(result.isUnderSize).toEqual(true);
  });
});
