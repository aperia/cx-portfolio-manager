import { MIME_TYPE } from 'app/constants/mine-type';
import {
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { base64toBlob, downloadBlobFile } from 'app/helpers/fileUtilities';
import { useUnsavedChangeRegistry } from 'app/hooks';
import { workflowTemplateService } from 'app/services';
import { InlineMessage, TransDLS, useTranslation } from 'app/_libraries/_dls';
import Upload, {
  STATUS_SUCCESS,
  UploadRef
} from 'app/_libraries/_dls/components/Upload';
import { StateFile } from 'app/_libraries/_dls/components/Upload/File';
import { STATUS } from 'app/_libraries/_dls/components/Upload/helper';
import classNames from 'classnames';
import { get, isEmpty } from 'lodash';
import { actionsWorkflow } from 'pages/_commons/redux/Workflow';
import {
  actionsWorkflowSetup,
  useWorkflowInstanceId
} from 'pages/_commons/redux/WorkflowSetup';
import { actionsToast } from 'pages/_commons/ToastNotifications/redux';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';
import CustomFile from '../CustomFile';
import {
  INTERVAL_FILE_PROGRESS,
  INTERVAL_FILE_VALIDATION,
  INVALID_STATUS,
  MAX_FILE,
  TYPE_FILE,
  VERIFIED_STATUSES_RESPONSE
} from './constants';
import { getStatusFileFromValidationMessages, getValid } from './helpers';

export interface StateFileProp extends StateFile {
  fileId?: string;
}
export interface FormUploadFile {
  attachmentId?: string;
  files?: StateFileProp[];
  isValid?: boolean;
}
interface Props {
  formValues: FormUploadFile;
  setFormValues?: any;
  savedAt?: number;
  ready?: boolean;
  stepId?: string;
  selectedStep?: string;
  dependencies: any;
  required?: boolean;
  uploadDescription?: string;
  handleSave: VoidFunction;
}

const UploadFile: React.FC<Props> = ({
  stepId,
  selectedStep,
  formValues,
  setFormValues,
  savedAt,
  required = false,
  uploadDescription = 'txt_workflow_setup_upload_file_desc',
  handleSave
}) => {
  const { files = [] } = formValues;

  const { workflowInstanceId } = useWorkflowInstanceId();
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const uploadRef = useRef<UploadRef | null>(null);
  const intervalRef = useRef<{
    id?: NodeJS.Timeout;
    processId?: NodeJS.Timeout;
    skipInterval?: boolean;
  }>({});

  const [initialAttachmentId, setInitialAttachmentId] = useState(
    formValues.attachmentId
  );
  const [validationMessages, setValidationMessages] = useState<
    IValidationMessage[]
  >([]);

  const hasFile = useMemo(() => !isEmpty(files), [files]);

  const disabledBtnRemove = (disabled = false) => {
    const btnElement =
      uploadRef.current?.containerElement?.getElementsByClassName(
        'btn btn-icon-secondary btn-sm'
      )?.[0];
    disabled
      ? btnElement?.classList?.add('disabled')
      : btnElement?.classList?.remove('disabled');
  };

  const handleRemoveOffline = () => {
    setFormValues({
      files: [],
      isValid: !required,
      attachmentId: undefined
    });
    disabledBtnRemove();
  };

  const handleRemoveOnline = async () => {
    const { idx } = files[0];

    const result: any = await Promise.resolve(
      dispatch(actionsWorkflowSetup.deleteTemplateFile({ attachmentId: idx }))
    );

    const isSuccess =
      actionsWorkflowSetup.deleteTemplateFile.fulfilled.match(result);
    if (isSuccess) {
      handleRemoveOffline();

      handleSave();
    }
    disabledBtnRemove();
  };

  const handleRemove = async () => {
    const { status } = files[0];
    disabledBtnRemove(true);

    status === STATUS.valid ? handleRemoveOnline() : handleRemoveOffline();

    clearFileValidationInterval();
    setValidationMessages([]);
  };

  const handleDownloadFile = async (attachmentId: string) => {
    const result: any = await Promise.resolve(
      dispatch(actionsWorkflow.downloadAttachment(attachmentId?.toString()))
    );
    const isSuccess =
      actionsWorkflow.downloadAttachment.fulfilled.match(result);

    if (isSuccess) {
      const {
        fileStream = '',
        mimeType: mimeTypeDownload = '',
        filename: filenameDownload = ''
      } = (result?.payload || {}) as IDownloadAttachment;

      let fileName = filenameDownload;
      if (filenameDownload.split('.').length < 2) {
        const ext = MIME_TYPE[mimeTypeDownload]?.extensions[0] || '';
        fileName = `${filenameDownload}.${ext}`;
      }
      const blol = base64toBlob(fileStream, mimeTypeDownload);
      downloadBlobFile(blol, fileName);
    } else {
      dispatch(
        actionsToast.addToast({
          type: 'error',
          message: t('txt_file_failed_to_download')
        })
      );
    }
  };

  const handleChangeFile = (data: { files: StateFile[] }) => {
    setFormValues({ files: data.files });
    process.nextTick(() => {
      data.files[0]?.status === STATUS.valid &&
        uploadRef.current?.uploadFiles();
    });
  };

  const uploadFiles = (files: any[], onUploadProgress: any) => {
    return files.map(async (file: any) => {
      const requestValue = { searchFields: { workflowInstanceId } };

      const formData = new FormData();
      formData.append('uploadFileRequest', JSON.stringify(requestValue));
      formData.append('file', file as any);

      return workflowTemplateService
        .uploadWorkflowTemplate(formData, {
          onUploadProgress: (event: any) => {
            const loaded = event.loaded / 2;
            Object.defineProperty(event, 'loaded', {
              get: () => loaded
            });

            onUploadProgress(file.idx, event);
          }
        })
        .then(response => ({
          idx: file.idx,
          data: response?.data,
          origin: file
        }))
        .catch(error => Promise.reject({ idx: file.idx, error, origin: file }));
    });
  };

  const handleFinally = (result: MagicKeyValue[]) => {
    const [response] = result;
    const isSuccess = response.status === STATUS_SUCCESS;

    if (!isSuccess) {
      return;
    }

    const { data, origin } = response.value;
    const { validationMessages, ..._file } = data.attachment[0] || {};

    if (
      isEmpty(_file) ||
      INVALID_STATUS === _file.status?.toString()?.toLowerCase()
    ) {
      setValidationMessages(validationMessages || []);
      origin.status = STATUS.error;
      setFormValues({ files: [origin] });
      return;
    }

    const newValidValue: MagicKeyValue = { isTypeSupport: true };
    // update valid value & get status for new file updated
    if (!isEmpty(validationMessages)) {
      validationMessages?.forEach(
        (validation: { field: string; message: string }) => {
          newValidValue.isInvalid = true;
          if (validation.field === 'Extension') {
            newValidValue.isTypeSupport = false;
          }
        }
      );
    }

    const newFile = {
      idx: _file.id,
      name: _file.filename,
      size: _file.fileSize,
      type: _file.mineType,
      lastModifiedDate: new Date(_file.uploadedDate),
      percentage: 50,
      status: STATUS.uploading
    };

    /* istanbul ignore next */
    if (!intervalRef.current.skipInterval) {
      setFormValues({
        attachmentId: _file.id,
        files: [newFile],
        isValid: false
      });

      handleSave();

      watchFileValidation(newFile);
    }
  };

  const watchFileValidation = (file: any) => {
    watchEditRef.current++;

    let count = 0;
    /* istanbul ignore next */
    intervalRef.current.processId = setInterval(() => {
      count++;
      const percent = file.percentage + count * 2;
      const newFile = {
        ...file,
        percentage: percent >= 98 ? 98 : percent
      };

      setFormValues({ files: [newFile] });
    }, INTERVAL_FILE_PROGRESS);

    /* istanbul ignore next */
    intervalRef.current.id = setInterval(async () => {
      if (!!intervalRef.current.skipInterval) {
        return;
      }

      intervalRef.current.skipInterval = true;
      const response = await workflowTemplateService.getFileDetails({
        searchFields: { attachmentIds: [`${file.idx}`] }
      });
      intervalRef.current.skipInterval = false;
      const result = get(response, 'data.attachments.0', {});

      const successResponse = VERIFIED_STATUSES_RESPONSE.includes(
        result.status?.toLowerCase()
      );
      if (isEmpty(result) || !successResponse) {
        return;
      }

      const isInvalid =
        INVALID_STATUS === result.status?.toLowerCase() ||
        getStatusFileFromValidationMessages(result.validationMessages) ===
          STATUS.invalid;
      const newFile = {
        ...file,
        status: isInvalid ? STATUS.invalid : STATUS.valid
      };
      setFormValues({ files: [newFile] });

      setValidationMessages(result.validationMessages || []);
      setFormValues({
        isValid: !isInvalid,
        file: newFile,
        attachmentId: newFile.idx
      });
      clearFileValidationInterval();
    }, INTERVAL_FILE_VALIDATION);
  };

  const clearFileValidationInterval = useCallback(() => {
    intervalRef.current.id && clearInterval(intervalRef.current.id);
    intervalRef.current.processId &&
      clearInterval(intervalRef.current.processId);
    intervalRef.current = {};
  }, []);

  useEffect(
    () => () => {
      intervalRef.current.skipInterval = true;
      clearFileValidationInterval();
    },
    [clearFileValidationInterval]
  );

  useEffect(() => {
    setInitialAttachmentId(formValues.attachmentId);

    if (formValues?.files?.[0]?.status !== STATUS.invalid) {
      setValidationMessages([]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [savedAt]);

  // re-try file validation when edit workflow
  const watchEditRef = useRef(0);
  useEffect(() => {
    if (
      isEmpty(files) ||
      files[0].percentage === 100 ||
      watchEditRef.current > 0 ||
      stepId !== selectedStep
    )
      return;
    watchEditRef.current++;

    watchFileValidation(files[0]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [savedAt]);

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName: UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__UPLOAD_FILE,
      priority: 1
    },
    [initialAttachmentId !== formValues.attachmentId]
  );

  return (
    <>
      <div className="mt-24">
        {
          /* istanbul ignore next */
          !isEmpty(validationMessages) && (
            <div className="mb-8">
              {validationMessages.map((m, idx) => (
                <InlineMessage
                  key={idx}
                  withIcon
                  // {m.severity === 'error' ? 'danger' : 'warning'}
                  variant="danger"
                >
                  {m.message}
                </InlineMessage>
              ))}
            </div>
          )
        }
        <p className="mb-16 color-grey">
          <TransDLS keyTranslation={uploadDescription}>
            <strong className="color-grey-d20" />
          </TransDLS>
        </p>
      </div>
      <div
        className={classNames('mt-16 custom-dls w-100', {
          uploaded: hasFile
        })}
      >
        <Upload
          ref={uploadRef}
          files={files}
          saveUrl={uploadFiles}
          id="automation-test-id"
          title={t('txt_choose_file')}
          inputConfig={{
            accept:
              'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel'
          }}
          multiple={false}
          typeFiles={TYPE_FILE}
          maxFile={MAX_FILE}
          onRemove={handleRemove}
          onChangeFile={handleChangeFile}
          onFinally={handleFinally}
          renderFile={(props: any) => (
            <CustomFile
              {...props}
              getValid={getValid}
              onDownloadFile={handleDownloadFile}
            />
          )}
        />
      </div>
    </>
  );
};

export default UploadFile;
