import { convertNameToCode } from 'app/helpers/convertCommon';
import { default as ActionCardCompromiseEventWorkflow } from './action-card-compromise-event-template.json';
import { default as AssignPricingStrategiesAndMethodOverridestoAccountsWorkflow } from './assign-pricing-strategies-template.json';
import { default as BulkArchivePCFWorkflow } from './bulk-archive-pcf-workflow.json';
import { default as BulkExternalStatusManagementWorkflow } from './bulk-external-status-management.json';
import { default as BulkSuspendFraudStrategyWorkflow } from './bulk-suspend-fraud-strategy-workflow.json';
import { default as ChangeInterestRatesWorkflow } from './change-interest-rates-workflow.json';
import { default as ConditionMCycleAccountsForTestingWorkflow } from './condition-m-cycle-accounts-for-testing-template.json';
import { default as ConfigureProcessingMerchantsWorkflow } from './configure-processing-merchants-template.json';
import { default as CreateCustomDataLinkElementsWorkflow } from './custom-datalink-elements-template.json';
import { default as CycleAccountsTestEnvironmentWorkflow } from './cycle-accounts-test-environment-template.json';
import { default as DesignLoanOffersWorkflow } from './design-loan-offers-template.json';
import { default as DesignPromotionsWorkflow } from './design-promotions-workflow.json';
import { default as ExportImportChangeDetailsWorkflow } from './export-import-change-details-workflow.json';
import { default as RunFlexibleMonetaryTransactionsWorkflow } from './flexible-monetary-transactions-template.json';
import { default as RunFlexibleNonMonetaryTransactionsWorkflow } from './flexible-non-monetary-transactions.json';
import { default as ForceEmbossReplacementCardsWorkflow } from './force-emboss-replacement-cards-template.json';
import { default as HaltInterestChargesAndFeesWorkFlow } from './halt-interest-charges-and-fees.json';
import { default as ManageAccountDelinquencyWorkflow } from './manage-account-delinquency-template.json';
import { default as ManageAccountLevelProcessingDecisionTablesALPWorkflow } from './manage-account-level-workflow.json';
import { default as ManageAccountMemosWorkflow } from './manage-account-memos-workflow.json';
import { default as ManageAccountSerializationNSWorkflow } from './manage-account-serialization-template.json';
import { default as ManageChangeInTermsSettingsWorkflow } from './manage-change-in-terms-settings-workflow.json';
import { default as ManageIncomeOptionMethodsWorkflow } from './manage-income-option-methods-workflow.json';
import { default as ManageMethodLevelProcessingDecisionTablesMLPWorkflow } from './manage-method-level-processing-decision-tables.json';
import { default as ManageOCSShellsWorkflow } from './manage-ocs-shells-workflow.json';
import { default as ManagePayoffExceptionMethodsWorkflow } from './manage-payoff-exception-methods-template.json';
import { default as ManagePenaltyFeesWorkflow } from './manage-penalty-fees-template.json';
import { default as ManagePricingStrategiesAndMethodAssignmentsWorkflow } from './manage-pricing-strategies-and-method-assignments-workflow.json';
import { default as ManageStatementProductionSettingsWorkflow } from './manage-statement-production-settings-template.json';
import { default as ManageTemporaryCreditLimitWorkflow } from './manage-temporary-credit-limit-workflow.json';
import { default as ManageAccountTransferWorkflow } from './manage_account_transfer_workflow.json';
import { default as ManageAutomatedAdjustmentsSystemWorkflow } from './manage_adjustments_system-templatey.json';
import { default as ManageTransactionLevelProcessingDecisionTablesTLPWorkFlow } from './manage_transaction_level_processing_decision_tables.json';
import { default as ManualAccountDeletionWorkflow } from './manual-account-deletion.json';
import { default as PerformMonetaryAdjustmentsWorkflow } from './perform-monetary-adjustments-workflow.json';
import { default as PortfolioDefinitionsWorkflow } from './portfolio-definitions-template.json';
import { default as SendLetterToAccountsWorkflow } from './send-letter-to-accounts-workflow.json';
import { default as UpdateIndexRateWorkflow } from './update-index-rate-workflow.json';

const _stepDefinition: Record<string, any> = Object.freeze({
  ActionCardCompromiseEventWorkflow,
  AssignPricingStrategiesAndMethodOverridestoAccountsWorkflow,
  BulkArchivePCFWorkflow,
  BulkExternalStatusManagementWorkflow,
  BulkSuspendFraudStrategyWorkflow,
  ChangeInterestRatesWorkflow,
  ConditionMCycleAccountsForTestingWorkflow,
  ConfigureProcessingMerchantsWorkflow,
  CreateCustomDataLinkElementsWorkflow,
  CycleAccountsTestEnvironmentWorkflow,
  DesignLoanOffersWorkflow,
  DesignPromotionsWorkflow,
  ExportImportChangeDetailsWorkflow,
  ForceEmbossReplacementCardsWorkflow,
  HaltInterestChargesAndFeesWorkFlow,
  ManageAccountDelinquencyWorkflow,
  ManageAccountLevelProcessingDecisionTablesALPWorkflow,
  ManageAccountMemosWorkflow,
  ManageAccountSerializationNSWorkflow,
  ManageAccountTransferWorkflow,
  ManageAutomatedAdjustmentsSystemWorkflow,
  ManageChangeInTermsSettingsWorkflow,
  ManageIncomeOptionMethodsWorkflow,
  ManageMethodLevelProcessingDecisionTablesMLPWorkflow,
  ManageOCSShellsWorkflow,
  ManagePayoffExceptionMethodsWorkflow,
  ManagePenaltyFeesWorkflow,
  ManagePricingStrategiesAndMethodAssignmentsWorkflow,
  ManageStatementProductionSettingsWorkflow,
  ManageTemporaryCreditLimitWorkflow,
  ManageTransactionLevelProcessingDecisionTablesTLPWorkFlow,
  ManualAccountDeletionWorkflow,
  PerformMonetaryAdjustmentsWorkflow,
  PortfolioDefinitionsWorkflow,
  RunFlexibleMonetaryTransactionsWorkflow,
  RunFlexibleNonMonetaryTransactionsWorkflow,
  SendLetterToAccountsWorkflow,
  UpdateIndexRateWorkflow
});

export const stepDefinition: Record<string, any> = Object.freeze(
  Object.keys(_stepDefinition).reduceRight(
    (pre, curr) => ({
      ...pre,
      [convertNameToCode(curr)]: _stepDefinition[curr]
    }),
    {}
  )
);
