import {
  addSuffixFormName,
  UNSAVED_CHANGE_NAMES,
  workflowSetupDangerChangeOnMoveStepWatcher,
  workflowSetupDangerChangeOnSaveWatcher
} from 'app/constants/unsave-changes-form-names';
import {
  useUnsavedChangesRedirect,
  useUnsavedChangesWatchingChange
} from 'app/hooks';
import { WorkflowSetupStepTracking } from 'app/types';
import { useTranslation } from 'app/_libraries/_dls';
import { isEmpty, isEqual, isFunction, isUndefined } from 'lodash';
import {
  actionsWorkflowSetup,
  useLastWorkflowStep,
  useSelectedWorkflowStep,
  useSelectWorkflowInstance,
  useWorkflowSetupForms
} from 'pages/_commons/redux/WorkflowSetup';
import { stepRegistry } from 'pages/_commons/WorkflowSetup/registries';
import { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { batch, useDispatch } from 'react-redux';
import { actionsToast } from '../ToastNotifications/redux';
import { WorkflowSetupMapping } from './types';

export const useWorkflowSetupStep = (
  id: string,
  stepsMapping: WorkflowSetupMapping[],
  allStepsMapping: WorkflowSetupMapping[],
  isEdit: boolean,
  stepsNoCache: WorkflowSetupMapping[]
) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const redirect = useUnsavedChangesRedirect();
  const [isEditWorkflow] = useState(isEdit);
  const [workflowId] = useState(id);
  const [loading, setLoading] = useState(!!isEdit);

  const workflowInstance = useSelectWorkflowInstance();
  const lastStep = useLastWorkflowStep();
  const selectedStep = useSelectedWorkflowStep();
  const forms = useWorkflowSetupForms();

  const unsavedChangeFormsRegistry = useMemo(
    () =>
      Object.values(stepRegistry.unsavedChangeFormsMap).reduceRight(
        (prev, curr = []) => [...prev, ...curr],
        []
      ),
    []
  );
  const formsChange = useUnsavedChangesWatchingChange(
    unsavedChangeFormsRegistry
  );

  const selectedStepIdx = useMemo(
    () => stepsMapping.findIndex(s => s.id === selectedStep),
    [selectedStep, stepsMapping]
  );
  const lastStepIdx = useMemo(
    () => stepsMapping.findIndex(s => s.id === lastStep),
    [lastStep, stepsMapping]
  );

  const keepRef = useRef({
    prevStepsMapping: stepsMapping,
    prevLastStep: lastStep,
    forms,
    formsChange,
    stepsNoCache,
    lastStep,
    lastStepIdx,
    selectedStep,
    selectedStepIdx,
    alredyGetFormData: false,
    alredyRenderFormData: false
  });
  keepRef.current.forms = forms;
  keepRef.current.formsChange = formsChange;
  keepRef.current.stepsNoCache = stepsNoCache;
  keepRef.current.lastStep = lastStep;
  keepRef.current.lastStepIdx = lastStepIdx;
  keepRef.current.selectedStep = selectedStep;
  keepRef.current.selectedStepIdx = selectedStepIdx;

  const confirmOnSaveWatcher = useMemo(() => {
    const watcher = [...workflowSetupDangerChangeOnSaveWatcher];
    watcher.push(
      addSuffixFormName(
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__LOCKED_STEP_BY_DEPENDENCIES,
        keepRef.current.selectedStep
      )
    );

    return watcher;
  }, []);
  const confirmOnMoveStepWatcher = useMemo(() => {
    const watcher = [...workflowSetupDangerChangeOnMoveStepWatcher];
    watcher.push(
      addSuffixFormName(
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__LOCKED_STEP_BY_DEPENDENCIES,
        keepRef.current.selectedStep
      )
    );

    return watcher;
  }, []);

  const isStepDefaultValues = useCallback(
    (
      formValues,
      defaultValues,
      isDefaultValues?: WorkflowSetupIsDefaultValueFunc
    ) =>
      (isFunction(isDefaultValues) &&
        isDefaultValues(formValues, defaultValues)) ||
      isEqual(formValues, defaultValues),
    []
  );

  const getWorkflowSetupData = useCallback((): WorkflowSetupInstanceStep[] => {
    const {
      selectedStep: _selectedStep,
      lastStep: _lastStep,
      forms: _forms,
      stepsNoCache: _stepsNoCache
    } = keepRef.current;

    let _lastStepIdx = _stepsNoCache.findIndex(s => s.id === _lastStep);

    const stepIds = _stepsNoCache.map(s => s.id);
    let lastValidStepIdx = stepIds.reverse().findIndex(i => _forms[i]?.isValid);
    lastValidStepIdx =
      lastValidStepIdx === 0
        ? stepIds.length - 2
        : lastValidStepIdx === -1
        ? 0
        : stepIds.length - 1 - lastValidStepIdx;

    _lastStepIdx = _lastStepIdx !== -1 ? _lastStepIdx : lastValidStepIdx;

    return _stepsNoCache.map(({ id: wfId }, idx) => {
      /*eslint  @typescript-eslint/no-unused-vars: ["error", { "ignoreRestSiblings": true }]*/
      const { isValid, id: _id, ...data } = _forms[wfId] || {};

      return {
        id: wfId,
        selected: wfId === _selectedStep,
        data,
        status:
          _lastStepIdx === idx
            ? 'INPROGRESS'
            : idx < _lastStepIdx
            ? 'DONE'
            : 'READY'
      };
    });
  }, []);

  const handleStepApiSubmit = useCallback(async () => {
    const { selectedStepIdx: _selectedStepIdx } = keepRef.current;
    const dataApi = stepsMapping[_selectedStepIdx]?.dataApi;

    if (dataApi && (actionsWorkflowSetup as any)[`${dataApi}`]) {
      const workflowSetupData = getWorkflowSetupData();
      const _result: any = await Promise.resolve(
        dispatch(
          (actionsWorkflowSetup as any)[`${dataApi}`]({ workflowSetupData })
        )
      );

      const isSuccess = (actionsWorkflowSetup as any)[
        `${dataApi}`
      ].fulfilled.match(_result);

      return {
        isSuccess,
        hasApiCalled: !isUndefined(_result.payload || _result.error)
      };
    }
    return { isSuccess: true, hasApiCalled: false };
  }, [dispatch, getWorkflowSetupData, stepsMapping]);

  const handleAutoSave = useCallback(
    async (nextStep: string) => {
      const {
        formsChange: _formsChange,
        selectedStep: _selectedStep,
        selectedStepIdx: _selectedStepIdx,
        forms: _forms
      } = keepRef.current;
      const isValidUpdate = !!_forms[_selectedStep]?.isValid;

      const nextStepIdx = stepsMapping.findIndex(s => s.id === nextStep);

      const selectedStepComponentName =
        stepsMapping.find(s => s.id === _selectedStep)?.componentName || '';
      const selectedStepUnsavedChangeForms =
        stepRegistry.unsavedChangeFormsMap[selectedStepComponentName] || [];
      const isSelectedStepHasChange = selectedStepUnsavedChangeForms.some(f =>
        _formsChange.includes(f)
      );

      if (!isSelectedStepHasChange) return true;

      // handle call api
      const { hasApiCalled, isSuccess } = await handleStepApiSubmit();

      if (!hasApiCalled) return true;

      const type = isSuccess ? 'success' : 'error';
      const message = isSuccess
        ? t('txt_workflow_auto_saved')
        : t('txt_workflow_failed_to_auto_save');

      batch(() => {
        if (isSuccess) {
          const now = Date.now();
          const savedAt = { haveBeenSaved: true, savedAt: now } as {
            savedAt: number;
            isStuck: Record<string, boolean>;
            lastSavedInvalidAt: number;
            haveBeenSaved: boolean;
          };
          isValidUpdate &&
            nextStepIdx > _selectedStepIdx &&
            (savedAt.isStuck = { [_selectedStep]: false });
          !isValidUpdate && (savedAt.lastSavedInvalidAt = now);

          dispatch(actionsWorkflowSetup.setSavedAt(savedAt));
        }

        dispatch(
          actionsToast.addToast({
            type,
            message
          })
        );
      });

      return isSuccess;
    },
    [dispatch, handleStepApiSubmit, stepsMapping, t]
  );

  const handleSave = useCallback(
    async ({
      saveOnClose = false,
      saveInBackground = false
    }: SaveOptions = {}) => {
      const _handleSave = async (formName?: string) => {
        const {
          formsChange: _formsChange,
          selectedStep: _selectedStep,
          lastStep: _lastStep,
          forms: _forms
        } = keepRef.current;
        const isValidUpdate = !!_forms[_selectedStep]?.isValid;

        const selectedStepComponentName =
          stepsMapping.find(s => s.id === _selectedStep)?.componentName || '';
        const selectedStepUnsavedChangeForms =
          stepRegistry.unsavedChangeFormsMap[selectedStepComponentName] || [];
        const isSelectedStepHasChange = selectedStepUnsavedChangeForms.some(f =>
          _formsChange.includes(f)
        );

        const { hasApiCalled, isSuccess } = await handleStepApiSubmit();

        if (!hasApiCalled) {
          if (!isValidUpdate) {
            dispatch(
              actionsWorkflowSetup.setSavedAt({
                lastSavedInvalidAt: Date.now(),
                isStuck: { [_selectedStep]: true }
              })
            );
          }

          return true;
        }

        const type = isSuccess ? 'success' : 'error';
        const message = isSuccess
          ? t(
              saveOnClose
                ? 'txt_workflow_saved_and_closed'
                : 'txt_workflow_saved'
            )
          : t('txt_workflow_failed_to_save');

        batch(() => {
          if (isSuccess) {
            const now = Date.now();
            const savedAt: {
              savedAt?: number | undefined;
              lastSavedInvalidAt?: number | undefined;
              isStuck?: Record<string, boolean> | undefined;
              haveBeenSaved?: boolean | undefined;
            } = {
              haveBeenSaved: true,
              savedAt: now,
              isStuck: { [_selectedStep]: false }
            };

            !saveInBackground &&
              !isValidUpdate &&
              (savedAt.lastSavedInvalidAt = now);

            dispatch(actionsWorkflowSetup.setSavedAt(savedAt));
          }

          !saveInBackground &&
            dispatch(
              actionsToast.addToast({
                type,
                message
              })
            );

          const isChangeSteMapping = formName?.includes(
            UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__LOCKED_STEP_BY_DEPENDENCIES
          );
          // always update if this step effected to another step
          isSelectedStepHasChange &&
            !isChangeSteMapping &&
            _selectedStep !== _lastStep &&
            !saveInBackground &&
            updateProgressBar();
        });

        return isSuccess;
      };

      if (saveOnClose || saveInBackground) {
        return await _handleSave();
      }

      redirect({
        onConfirm: _handleSave,
        formsWatcher: confirmOnSaveWatcher
      });
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [dispatch, redirect, handleStepApiSubmit, t, confirmOnSaveWatcher]
  );

  const moveLastStep = useCallback(
    (stepId: string) => {
      dispatch(actionsWorkflowSetup.setWorkflowLastStep(stepId));
    },
    [dispatch]
  );

  const handleSelectStep = useCallback(
    (stepId: string, moveNextLastStep = false, autoSaved = true) => {
      const _handleSelectStep = async () => {
        const {
          selectedStep: _selectedStep,
          forms: _forms,
          selectedStepIdx: currentStepIdx,
          lastStepIdx: _lastStepIdx,
          lastStep: _lastStep
        } = keepRef.current;
        const isValidUpdate = !!_forms[_selectedStep]?.isValid;

        const isSuccessSaved = autoSaved ? await handleAutoSave(stepId) : true;

        const nextStepIdx = stepsMapping.findIndex(s => s.id === stepId);

        batch(() => {
          const firstInvalidIdx = stepsMapping.findIndex(
            s => !_forms[s.id]?.isValid
          );

          let lastStepMoved = '';
          let actualNextStep = _selectedStep;
          if (
            isSuccessSaved &&
            (isValidUpdate ||
              currentStepIdx === -1 ||
              nextStepIdx < currentStepIdx)
          ) {
            const isDefaultValues =
              stepsMapping[_lastStepIdx]?.component?.isDefaultValues;
            const defaultValues =
              stepsMapping[_lastStepIdx]?.component?.defaultValues;
            if (
              _lastStepIdx > nextStepIdx &&
              _lastStepIdx - 1 >= 0 &&
              (isStepDefaultValues(
                _forms[_lastStep],
                defaultValues,
                isDefaultValues
              ) ||
                _lastStepIdx === stepsMapping.length - 1)
            ) {
              lastStepMoved = stepsMapping[_lastStepIdx - 1].id;
              moveLastStep(lastStepMoved);
            }

            actualNextStep = stepId;
            dispatch(actionsWorkflowSetup.setWorkflowSelectedStep(stepId));
            if (moveNextLastStep) {
              lastStepMoved = stepId;
              moveLastStep(stepId);
            }
          }

          /* istanbul ignore next */
          if (
            firstInvalidIdx !== -1 &&
            firstInvalidIdx < nextStepIdx &&
            nextStepIdx >= currentStepIdx
          ) {
            actualNextStep = stepsMapping[firstInvalidIdx].id;
            dispatch(
              actionsWorkflowSetup.setWorkflowSelectedStep(actualNextStep)
            );
            dispatch(
              actionsWorkflowSetup.setSavedAt({
                lastSavedInvalidAt: Date.now(),
                isStuck: { [actualNextStep]: true }
              })
            );
          }

          const actualNextStepIdx = stepsMapping.findIndex(
            s => s.id === actualNextStep
          );

          const resetStepStuckBefore = stepsMapping
            .filter((step, idx) => idx < actualNextStepIdx)
            .reduce((prev, { id }) => ({ ...prev, [id]: false }), {});
          dispatch(
            actionsWorkflowSetup.setSavedAt({
              lastSavedInvalidAt: Date.now(),
              isStuck: resetStepStuckBefore
            })
          );

          const stepTracking: Record<string, WorkflowSetupStepTracking> = {};

          if (nextStepIdx > currentStepIdx) {
            stepsMapping.forEach((step, idx) => {
              const isDefaultValues = step.component?.isDefaultValues;
              const defaultValues = step.component?.defaultValues;

              if (
                step.id === lastStepMoved &&
                isStepDefaultValues(
                  _forms[lastStepMoved],
                  defaultValues,
                  isDefaultValues
                )
              ) {
                stepTracking[step.id] = {
                  doneValidation: -1,
                  inprogress: true
                };
                return;
              }
              if (
                idx > actualNextStepIdx ||
                (actualNextStep === stepId && idx === nextStepIdx)
              )
                return;

              stepTracking[step.id] = {
                doneValidation: Date.now(),
                inprogress: false
              };
            });
          } else {
            stepsMapping.forEach((step, idx) => {
              if (idx > currentStepIdx) return;

              stepTracking[step.id] = { doneValidation: -1 };
            });
          }
          dispatch(
            actionsWorkflowSetup.setStepTrackingValidation(stepTracking)
          );
        });

        return isSuccessSaved;
      };

      // handle check invalid step before navigate to next step without confirmation
      const {
        forms: _forms,
        selectedStepIdx: currentStepIdx,
        lastStep: _lastStep
      } = keepRef.current;
      const firstInvalidIdx = stepsMapping.findIndex(
        s => !_forms[s.id]?.isValid
      );
      const nextStepIdx = stepsMapping.findIndex(s => s.id === stepId);

      if (firstInvalidIdx !== -1 && firstInvalidIdx < nextStepIdx) {
        const actualNextStep = stepsMapping[firstInvalidIdx].id;
        dispatch(
          actionsWorkflowSetup.setSavedAt({
            lastSavedInvalidAt: Date.now(),
            isStuck: { [actualNextStep]: true }
          })
        );
        const actualNextStepIdx = stepsMapping.findIndex(
          s => s.id === actualNextStep
        );

        const resetStepStuckBefore = stepsMapping
          .filter((step, idx) => idx < actualNextStepIdx)
          .reduce((prev, { id }) => ({ ...prev, [id]: false }), {});
        dispatch(
          actionsWorkflowSetup.setSavedAt({
            lastSavedInvalidAt: Date.now(),
            isStuck: resetStepStuckBefore
          })
        );

        const stepTracking: Record<string, WorkflowSetupStepTracking> = {};

        if (nextStepIdx > currentStepIdx) {
          stepsMapping.forEach((step, idx) => {
            const isDefaultValues = step.component?.isDefaultValues;
            const defaultValues = step.component?.defaultValues;

            if (
              step.id === _lastStep &&
              isStepDefaultValues(
                _forms[_lastStep],
                defaultValues,
                isDefaultValues
              )
            ) {
              stepTracking[step.id] = {
                doneValidation: -1,
                inprogress: true
              };
              return;
            }
            if (idx > actualNextStepIdx) return;

            stepTracking[step.id] = {
              doneValidation: Date.now(),
              inprogress: false
            };
          });
        } else {
          stepsMapping.forEach((step, idx) => {
            if (idx > currentStepIdx) return;

            stepTracking[step.id] = { doneValidation: -1 };
          });
        }
        dispatch(actionsWorkflowSetup.setStepTrackingValidation(stepTracking));
        return;
      }

      redirect({
        onConfirm: _handleSelectStep,
        formsWatcher: confirmOnMoveStepWatcher
      });
    },
    [
      dispatch,
      redirect,
      moveLastStep,
      handleAutoSave,
      isStepDefaultValues,
      stepsMapping,
      confirmOnMoveStepWatcher
    ]
  );

  const handleUpdateProgressBar = useCallback(
    ({
      prevStepsMapping,
      isManualUpdate = true,
      shouldSelectStep = true
    }: {
      prevStepsMapping: WorkflowSetupMapping[];
      isManualUpdate?: boolean;
      shouldSelectStep?: boolean;
    }) => {
      const {
        selectedStep: _selectedStep,
        lastStep: _lastStep,
        forms: _forms
      } = keepRef.current;
      const selectedIdx = stepsMapping.findIndex(s => s.id === _selectedStep);

      if (selectedIdx !== -1) {
        let invalidIdx = stepsMapping.findIndex(s => !_forms[s.id]?.isValid);
        const _lastStepIdx = stepsMapping.findIndex(s => s.id === _lastStep);

        if (!isManualUpdate && _lastStepIdx !== -1) return;

        const isDefaultValues =
          stepsMapping[invalidIdx]?.component?.isDefaultValues;
        const defaultValues =
          stepsMapping[invalidIdx]?.component?.defaultValues;

        invalidIdx =
          invalidIdx === -1
            ? -1
            : isStepDefaultValues(
                _forms[invalidIdx],
                defaultValues,
                isDefaultValues
              )
            ? invalidIdx
            : invalidIdx - 1;

        const _prevLastStepIdx = prevStepsMapping.findIndex(
          s => s.id === _lastStep
        );
        // try to get current last step index 2 levels
        let _currentLastStepIdx = stepsMapping.findIndex(
          (s, idx) => _prevLastStepIdx - 1 === idx
        );
        _currentLastStepIdx =
          _currentLastStepIdx === -1
            ? stepsMapping.findIndex((s, idx) => _prevLastStepIdx - 2 === idx)
            : _currentLastStepIdx;

        const lastIdx = invalidIdx === -1 ? _currentLastStepIdx : invalidIdx;
        moveLastStep(stepsMapping[lastIdx]?.id);

        invalidIdx !== -1 &&
          shouldSelectStep &&
          handleSelectStep(stepsMapping[invalidIdx].id, true, false);
        return;
      }

      const _selectedStepIdx = prevStepsMapping.findIndex(
        s => s.id === _selectedStep
      );

      const _nextSelectStep =
        stepsMapping.find(
          s => s.id === prevStepsMapping[_selectedStepIdx - 1].id
        ) ||
        stepsMapping.find(
          /* istanbul ignore next */
          s => s.id === prevStepsMapping[_selectedStepIdx - 2].id
        )!;
      shouldSelectStep &&
        handleSelectStep(
          _nextSelectStep.id,
          _lastStep === _selectedStep,
          false
        );
    },
    [moveLastStep, handleSelectStep, isStepDefaultValues, stepsMapping]
  );

  const updateProgressBar = useCallback(() => {
    const { prevStepsMapping } = keepRef.current;

    process.nextTick(() => {
      handleUpdateProgressBar({ prevStepsMapping, shouldSelectStep: false });
    });
  }, [handleUpdateProgressBar]);

  // handle select first step if selected step not mapping
  useEffect(() => {
    if (allStepsMapping.some(s => s.id === selectedStep)) return;

    handleSelectStep(allStepsMapping[0].id, true, false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [allStepsMapping]);

  useEffect(() => {
    const current = keepRef.current;
    return () => {
      current.prevStepsMapping = stepsMapping;
    };
  }, [stepsMapping, formsChange]);

  useEffect(() => {
    const { prevStepsMapping } = keepRef.current;

    const stepsMappingStr = stepsMapping.map(s => s.id).join(',');
    const prevStepsMappingStr = prevStepsMapping.map(s => s.id).join(',');

    if (prevStepsMapping.length < 4 || stepsMappingStr === prevStepsMappingStr)
      return;

    process.nextTick(() => {
      handleUpdateProgressBar({ prevStepsMapping, isManualUpdate: false });
    });
  }, [
    handleUpdateProgressBar,
    moveLastStep,
    handleSelectStep,
    isStepDefaultValues,
    stepsMapping
  ]);

  const setFormValues = useCallback(
    (_id, values) => {
      dispatch(
        actionsWorkflowSetup.updateWorkflowFormStep({ name: _id, values })
      );
    },
    [dispatch]
  );

  useEffect(() => {
    if (!workflowId || !isEditWorkflow || keepRef.current.alredyGetFormData)
      return;

    keepRef.current.alredyGetFormData = true;
    dispatch(actionsWorkflowSetup.getWorkflowInstance(workflowId));
  }, [dispatch, workflowId, isEditWorkflow]);
  useEffect(() => {
    if (
      !workflowInstance ||
      !isEditWorkflow ||
      keepRef.current.alredyRenderFormData
    ) {
      return;
    }

    keepRef.current.alredyRenderFormData = true;

    const handleInitEditWorkflow = async () => {
      let selectedId = '';
      let lastId = '';
      let lastIdDefaultValues = '';
      const lastIds: string[] = [];
      const firstId = allStepsMapping[0].id;

      for (const { id: _id, component } of allStepsMapping) {
        if (!isFunction(component.parseFormValues)) continue;

        const isDefaultValues = component?.isDefaultValues;
        const defaultValues = component?.defaultValues;

        const { isSelected, isPass, ...values } =
          (await component.parseFormValues(workflowInstance, _id, dispatch)) ||
          ({} as any);

        setFormValues(_id, isEmpty(values) ? defaultValues : values);

        if (isSelected) {
          selectedId = _id;
        }
        if (isPass) {
          lastId = _id;
          lastIds.push(_id);

          if (isStepDefaultValues(values, defaultValues, isDefaultValues)) {
            lastIdDefaultValues = _id;
          }
        }
      }

      process.nextTick(() => {
        dispatch(
          actionsWorkflowSetup.setSavedAt({
            savedAt: Date.now(),
            lastSavedInvalidAt: Date.now(),
            haveBeenSaved: false
          })
        );

        const movePreLastStep =
          lastId === lastIdDefaultValues && lastIds.length >= 2;
        const lastStepDefaultValues = movePreLastStep
          ? lastIds[lastIds.length - 2]
          : lastId;
        const lastStepId = lastStepDefaultValues || firstId;

        const selectedStepIdx = allStepsMapping.findIndex(
          s => s.id === selectedId
        );
        const lastStepIdx = allStepsMapping.findIndex(s => s.id === lastStepId);

        handleSelectStep(
          !selectedId || selectedStepIdx > lastStepIdx
            ? lastStepId
            : selectedId,
          false,
          false
        );
        moveLastStep(lastStepId);
        setLoading(false);
      });
    };

    handleInitEditWorkflow();
  }, [
    dispatch,
    setFormValues,
    handleSelectStep,
    moveLastStep,
    isStepDefaultValues,
    allStepsMapping,
    isEditWorkflow,
    workflowInstance
  ]);

  const result = useMemo(
    () => ({
      loading,
      selectedStep,
      lastStep,
      handleSelectStep,
      moveLastStep,
      handleSave,
      handleStepApiSubmit
    }),
    [
      handleSelectStep,
      moveLastStep,
      handleSave,
      handleStepApiSubmit,
      selectedStep,
      lastStep,
      loading
    ]
  );
  return result;
};
