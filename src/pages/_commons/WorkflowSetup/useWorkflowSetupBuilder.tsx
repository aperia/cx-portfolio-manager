import { parseDependencies } from 'app/helpers';
import { useTranslation } from 'app/_libraries/_dls';
import isFunction from 'lodash.isfunction';
import {
  usePrevInvalidSavedWorkflowSetupForms,
  usePrevSavedWorkflowSetupForms,
  useWorkflowInstanceId,
  useWorkflowSetupStepTracking
} from 'pages/_commons/redux/WorkflowSetup';
import React, { useMemo } from 'react';
import ContentWrapper from './ContentWrapper';
import { WorkflowSetupMapping } from './types';

export const useWorkflowSetupBuilder = ({
  selectedStepId,
  lastStepId,
  stepsMapping,
  ready = true,
  handleSelectStep,
  moveLastStep,
  handleSave
}: {
  selectedStepId: string;
  lastStepId: string;
  stepsMapping: WorkflowSetupMapping[];
  ready?: boolean;
  handleSelectStep: (stepId: string, moveNextLastStep?: boolean) => void;
  moveLastStep: (stepId: string) => void;
  handleSave: (options?: SaveOptions) => void;
}) => {
  const { t } = useTranslation();
  const formsCache = usePrevSavedWorkflowSetupForms();
  const formsValidate = usePrevInvalidSavedWorkflowSetupForms();
  const stepTracking = useWorkflowSetupStepTracking();

  const { changeId, workflowInstanceId, isCompletedWorkflow } =
    useWorkflowInstanceId();

  const hasAnyInvalidStep = useMemo(
    () =>
      stepsMapping
        .map(s => s.id)
        .some(
          id =>
            formsValidate?.[id]?.hasOwnProperty('isValid') &&
            !formsValidate[id].isValid
        ),
    [stepsMapping, formsValidate]
  );
  const stepsWithDependencies = useMemo<
    (Omit<WorkflowSetupMapping, 'dependencies'> & {
      dependencies?: Record<string, any>;
    })[]
  >(() => {
    const steps = stepsMapping.map(
      ({ dependencies: dependenciesConfig, ...rest }) => {
        const dependencies = parseDependencies(formsCache, dependenciesConfig);
        return {
          ...rest,
          dependencies
        };
      }
    );

    return steps;
  }, [formsCache, stepsMapping]);

  const menuSteps = useMemo<WorkflowSetupStep[]>(() => {
    const steps: WorkflowSetupStep[] = stepsWithDependencies.map(
      ({
        id,
        title,
        component: { stepValue, stepError },
        dependencies,
        dataApi
      }) => {
        const inprogress =
          (stepTracking[id]?.doneValidation || 0) <= 0 &&
          !!stepTracking[id]?.inprogress;

        const description = isFunction(stepValue)
          ? stepValue({
              stepsForm: formsCache[id],
              dependencies,
              t
            })
          : stepValue;
        const error =
          !!stepTracking[id]?.error ||
          !formsValidate[id]?.isValid ||
          (isFunction(stepError) && stepError(formsCache[id], dependencies));

        return {
          id,
          title,
          description,
          error,
          inprogress,
          dataApi
        };
      }
    );

    return selectedStepId === lastStepId &&
      steps[0]?.id === selectedStepId &&
      !formsCache[selectedStepId]?.isValid
      ? [steps[0]]
      : steps;
  }, [
    selectedStepId,
    lastStepId,
    formsCache,
    formsValidate,
    stepsWithDependencies,
    t,
    stepTracking
  ]);

  const compSteps = useMemo<WorkflowSetupContent[]>(() => {
    const excludesSummary = stepsWithDependencies.filter(
      ({ component: { isSummary } }) => !isSummary
    );
    const defaultValues = excludesSummary.reduce(
      (pre, curr) => ({ ...pre, [curr.id]: curr.component.defaultValues }),
      {}
    );
    const steps = excludesSummary.map(
      ({
        id,
        title,
        component,
        lockedFields,
        dependencies,
        wrappedContent = true,

        // removed props
        condition,
        dataApi,
        componentName,
        order,
        ...props
      }) => {
        return {
          id,
          title,
          wrappedContent,
          content: (
            <ContentWrapper
              {...props}
              key={id}
              title={title}
              content={component}
              id={id}
              ready={ready}
              lockedFields={lockedFields}
              hasInstance={!!workflowInstanceId}
              defaultValues={defaultValues}
              dependencies={dependencies}
              handleSelectStep={handleSelectStep}
              moveLastStep={moveLastStep}
              handleSave={handleSave}
            />
          )
        } as WorkflowSetupContent;
      }
    );

    const summary = stepsWithDependencies.find(
      ({ component: { isSummary } }) => isSummary
    );
    const summaries = excludesSummary
      .filter(({ component: { summaryComponent } }) => !!summaryComponent)
      .map(
        ({
          id,
          title,
          component: { summaryComponent },
          dependencies,
          // removed props
          condition,
          dataApi,
          componentName,
          order,
          lockedFields,
          ...rest
        }) => ({
          stepId: id,
          title,
          dependencies,
          element: summaryComponent,
          ...rest
        })
      ) as WorkflowSetupSummary[];

    if (!!summary) {
      /*eslint  @typescript-eslint/no-unused-vars: ["error", { "ignoreRestSiblings": true }]*/
      const {
        id,
        title,
        wrappedContent = true,
        component,
        // removed props
        condition,
        dataApi,
        componentName,
        order,
        lockedFields,
        dependencies,
        ...props
      } = summary;
      steps.push({
        id,
        title,
        wrappedContent,
        content: (
          <ContentWrapper
            {...props}
            title={title}
            content={component}
            id={id}
            ready={ready}
            summaries={summaries}
            hasInstance={!!workflowInstanceId}
            handleSelectStep={handleSelectStep}
            handleSave={handleSave}
          />
        )
      });
    }
    return steps;
  }, [
    handleSelectStep,
    moveLastStep,
    handleSave,
    ready,
    stepsWithDependencies,
    workflowInstanceId
  ]);

  const result = useMemo(
    () => ({
      changeId,
      workflowInstanceId,
      isCompletedWorkflow,
      hasAnyInvalidStep,
      steps: stepsWithDependencies,
      menuSteps,
      componentSteps: compSteps
    }),
    [
      stepsWithDependencies,
      menuSteps,
      compSteps,
      changeId,
      workflowInstanceId,
      isCompletedWorkflow,
      hasAnyInvalidStep
    ]
  );
  return result;
};
