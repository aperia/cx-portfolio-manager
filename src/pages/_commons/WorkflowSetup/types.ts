import React from 'react';

export interface WorkflowSetupLockedField {
  effectTo: string;
  field: string;
}
export interface WorkflowSetupMapping
  extends Omit<WorkflowSetupStepConfig, 'component'> {
  component: WorkflowSetupStaticProp;
  componentName: string;

  lockedFields?: WorkflowSetupLockedField[];
  dependencies?: string[];
}
export interface WorkflowSetupStaticProp<T = {}, U = {}>
  extends React.ExoticComponent<
    WorkflowSetupProps<T, U> | WorkflowSetupSummaryStepProps<T, U>
  > {
  summaryComponent?: React.ElementType<WorkflowSetupSummaryProps<T, U>>;
  parseFormValues?: WorkflowSetupStepFormDataFunc<T>;
  stepValue?: WorkflowSetupStepValueFunc<T, U> | string;
  stepError?: WorkflowSetupStepErrorFunc<T, U>;
  defaultValues?: Partial<T>;
  isDefaultValues?: WorkflowSetupIsDefaultValueFunc<T>;
  isEqualFormValues?: WorkflowSetupIsEqualFormValuesFunc<T>;

  isSummary?: boolean;
}
export type StepsMap = Record<string, WorkflowSetupStaticProp<any>>;
export type UnsavedChangeFormsMap = Record<string, string[]>;
export interface StepRegistry {
  readonly stepsMap: StepsMap;
  readonly unsavedChangeFormsMap: UnsavedChangeFormsMap;
  registerStep(
    stepId: string,
    stepComponent: WorkflowSetupStaticProp<any>,
    unsavedChangeForms?: string[]
  ): void;
  unregisterStep(stepId: string): void;
}

export type SubContentStepUploadMap = Record<
  string,
  React.ElementType<WorkflowSetupProps<any, any>>
>;
export interface SubContentStepUploadRegistry {
  readonly subContentsMap: SubContentStepUploadMap;
  registerComponent(
    subContentName: string,
    component: React.ElementType<WorkflowSetupProps<any, any>>
  ): void;
  get<T = {}, U = {}>(
    subContentName: string
  ): React.ElementType<WorkflowSetupProps<T, U>>;
  unregisterComponent(subContentName: string): void;
}
