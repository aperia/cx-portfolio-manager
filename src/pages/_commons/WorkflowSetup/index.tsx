import ConfirmationModal from 'app/components/ConfirmationModal';
import ModalRegistry from 'app/components/ModalRegistry';
import { changeDetailLink } from 'app/constants/links';
import {
  existedWorkflowSetupUnsavedDataWatcher,
  workflowSetupUnsavedDataWatcher
} from 'app/constants/unsave-changes-form-names';
import { classnames, getSetupWorkflowConfig } from 'app/helpers';
import { useUnsavedChangesRedirect } from 'app/hooks';
import {
  Button,
  Icon,
  ModalBody,
  ModalHeader,
  ModalTitle,
  SimpleBar,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import { isFunction } from 'lodash';
import React, { useEffect, useMemo, useState } from 'react';
import { batch, useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
import {
  actionsWorkflow,
  useSelectChangeWorkflowRemoval
} from '../redux/Workflow';
import {
  actionsWorkflowSetup,
  useSelectWorkflowSetupLoading,
  useWorkflowFormStepsHaveBeenSaved,
  useWorkflowFormStepValidate
} from '../redux/WorkflowSetup';
import { actionsToast } from '../ToastNotifications/redux';
import ProgressBar from './ProgressBar';
import { useWorkflowSetupBuilder } from './useWorkflowSetupBuilder';
import { useWorkflowSetupParser } from './useWorkflowSetupParser';
import { useWorkflowSetupStep } from './useWorkflowSetupStep';

export interface IWorkflowSetupProps {
  isEdit?: boolean;
  workflow: IWorkflowModel | IWorkflowTemplateModel;
  onSubmitted?: (params: { isDeleted?: boolean }) => boolean;
}
const WorkflowSetup: React.FC<IWorkflowSetupProps> = ({
  isEdit = false,
  workflow,
  onSubmitted
}) => {
  const { t } = useTranslation();
  const history = useHistory();
  const dispatch = useDispatch();
  const redirect = useUnsavedChangesRedirect();
  const loading = useSelectWorkflowSetupLoading();
  const hasBeenSaved = useWorkflowFormStepsHaveBeenSaved();

  const { stepsMapping, allStepsMapping, stepsNoCache, info } =
    useWorkflowSetupParser(getSetupWorkflowConfig(workflow?.name), isEdit);
  const {
    loading: setupStepLoading,
    selectedStep: selectedStepId,
    lastStep: lastStepId,
    handleSelectStep,
    moveLastStep,
    handleStepApiSubmit,
    handleSave
  } = useWorkflowSetupStep(
    workflow?.id,
    stepsMapping,
    allStepsMapping,
    isEdit,
    stepsNoCache
  );
  const [showDeleteWorkflowModal, setShowDeleteWorkflowModal] =
    useState<boolean>(false);
  const { loading: removeWorkflowLoading } = useSelectChangeWorkflowRemoval();
  const {
    changeId,
    isCompletedWorkflow,
    workflowInstanceId,
    steps,
    menuSteps,
    componentSteps,
    hasAnyInvalidStep
  } = useWorkflowSetupBuilder({
    selectedStepId,
    lastStepId,
    stepsMapping,
    ready: !setupStepLoading,
    handleSelectStep,
    moveLastStep,
    handleSave
  });

  const isValid = useWorkflowFormStepValidate(selectedStepId);

  const selectedStepIdx = useMemo(
    () => steps.findIndex(s => s.id === selectedStepId),
    [selectedStepId, steps]
  );

  const lastStepIdx = useMemo(
    () => steps.findIndex(s => s.id === lastStepId),
    [lastStepId, steps]
  );

  const isFirstStep = useMemo(() => selectedStepIdx === 0, [selectedStepIdx]);
  const stepLength = useMemo(() => steps.length, [steps.length]);
  const isLastStep = useMemo(
    () => selectedStepIdx === stepLength - 1,
    [selectedStepIdx, stepLength]
  );

  useEffect(() => {
    return () => {
      dispatch(actionsWorkflowSetup.resetWorkflowFormSteps());
    };
  }, [dispatch]);

  const handleClose = () => {
    redirect({
      onDiscard: () => {
        const message = isEdit
          ? 'txt_workflow_edit_exited_and_discarded'
          : 'txt_workflow_set_up_existed_and_discarded';

        batch(() => {
          dispatch(actionsWorkflowSetup.setWorkflowSetup());
          workflowInstanceId && hasBeenSaved && navigateToChangeDetail();
          dispatch(
            actionsToast.addToast({ type: 'success', message: t(message) })
          );
        });
      },
      onConfirm: async formName => {
        if (existedWorkflowSetupUnsavedDataWatcher.includes(formName!)) {
          const isSuccess = await handleSave({ saveOnClose: true });
          isSuccess && dispatch(actionsWorkflowSetup.setWorkflowSetup());
          isSuccess && navigateToChangeDetail();
          return;
        }

        if (workflowSetupUnsavedDataWatcher.includes(formName!)) {
          batch(() => {
            dispatch(actionsWorkflowSetup.setWorkflowSetup());
            dispatch(
              actionsToast.addToast({
                type: 'success',
                message: t('txt_workflow_set_up_and_all_data_discarded')
              })
            );
          });
          return;
        }
        dispatch(actionsWorkflowSetup.setWorkflowSetup());
        workflowInstanceId && hasBeenSaved && navigateToChangeDetail();
      },
      formsWatcher: [
        ...existedWorkflowSetupUnsavedDataWatcher,
        ...workflowSetupUnsavedDataWatcher
      ]
    });
  };
  const navigateToChangeDetail = (
    params: { isDeleted: boolean } = { isDeleted: false }
  ) => {
    const reload = isFunction(onSubmitted) && onSubmitted(params);
    if (reload) return;

    const redirectChangePath = changeDetailLink(changeId!);
    if (history.location.pathname === redirectChangePath) {
      dispatch(actionsWorkflow.getChangeWorkflows({ changeId: changeId! }));
    } else {
      history.replace({
        pathname: redirectChangePath
      });
    }
  };

  const handleSubmit = async () => {
    const dataApiRs = await handleStepApiSubmit();
    if (dataApiRs.isSuccess) {
      dispatch(
        actionsToast.addToast({
          type: 'success',
          message: t('txt_workflow_submitted')
        })
      );
      dispatch(actionsWorkflowSetup.setWorkflowSetup());
      navigateToChangeDetail();
    } else {
      dispatch(
        actionsToast.addToast({
          type: 'error',
          message: t('txt_workflow_failed_to_submit')
        })
      );
    }
  };

  const handleNextStepClicked = () => {
    const nextStep = steps[selectedStepIdx + 1].id;
    handleSelectStep(nextStep, lastStepIdx === selectedStepIdx);
  };

  const handleProgressBarClicked = (step: string) => {
    const nextStepIdx = steps.findIndex(s => s.id === step);
    const autoSaved = nextStepIdx !== 0 || !!workflowInstanceId;

    handleSelectStep(step, false, autoSaved);
  };

  const handleRemoveWorkflowInstance = async () => {
    const result: any = await Promise.resolve(
      dispatch(actionsWorkflow.removeWorkflow(workflowInstanceId!))
    );
    const isSuccess = actionsWorkflow.removeWorkflow.fulfilled.match(result);
    if (isSuccess) {
      setShowDeleteWorkflowModal(false);
      batch(() => {
        dispatch(
          actionsToast.addToast({
            type: 'success',
            message: t('txt_notification_delete_workflow_success')
          })
        );
        dispatch(actionsWorkflowSetup.setWorkflowSetup());
      });

      navigateToChangeDetail({ isDeleted: true });
    } else {
      dispatch(
        actionsToast.addToast({
          type: 'error',
          message: t('txt_notification_delete_workflow_fail')
        })
      );
    }
  };

  const confirmDeleteWorkflowInstance = () => {
    setShowDeleteWorkflowModal(true);
  };

  return (
    <>
      <ModalRegistry
        id={`workflowSetup__${workflow?.id}`}
        show={!!workflow}
        full
        classes={{ content: loading || setupStepLoading ? 'loading' : '' }}
        className="dls-modal-br-footer custom-modal"
        enforceFocus={false}
      >
        <ModalHeader border closeButton onHide={handleClose}>
          <ModalTitle className="d-flex align-items-center">
            <span className="pr-8">
              {t(
                isEdit ? 'txt_title_edit_workflow' : 'txt_title_set_up_workflow'
              ).replace('{0}', info?.title || workflow?.name)}
            </span>
            {info?.workflowInfo && (
              <Tooltip element={info.workflowInfo} triggerClassName="d-flex">
                <Icon name="information" className="color-grey-l16" />
              </Tooltip>
            )}
          </ModalTitle>
        </ModalHeader>
        <ModalBody className="p-0">
          <div className="workflow-creation">
            <ProgressBar
              steps={menuSteps}
              lastStep={lastStepIdx}
              selectedStep={selectedStepIdx}
              isCompletedWorkflow={isCompletedWorkflow}
              onSelectStep={handleProgressBarClicked}
            />
            <div className="workflow-creation__content">
              {componentSteps.map(({ id, title, content, wrappedContent }) => {
                const Wrapper = wrappedContent ? SimpleBar : 'div';

                return (
                  <Wrapper
                    key={id}
                    className={classnames({
                      'd-none': id !== selectedStepId,
                      'p-24': wrappedContent,
                      'h-100 overflow-hidden': !wrappedContent
                    })}
                  >
                    {wrappedContent && <h4>{t(title)}</h4>}
                    {(!isEdit || !!workflowInstanceId) && content}
                  </Wrapper>
                );
              })}
            </div>
          </div>
        </ModalBody>
        <div className="dls-modal-footer modal-footer d-flex justify-content-between pl-16">
          <div>
            {!isFirstStep && (
              <Button
                variant="outline-primary"
                onClick={() =>
                  handleSelectStep(
                    steps[selectedStepIdx - 1].id,
                    false,
                    !!workflowInstanceId
                  )
                }
              >
                {t('txt_back')}
              </Button>
            )}
            <Button
              variant="outline-danger"
              disabled={!changeId || !workflowInstanceId}
              onClick={confirmDeleteWorkflowInstance}
            >
              {t('txt_delete_workflow')}
            </Button>
          </div>
          <div>
            <Button variant="secondary" onClick={handleClose}>
              {t('txt_close')}
            </Button>
            {!isLastStep && (
              <React.Fragment>
                <Button
                  variant="secondary"
                  disabled={
                    loading ||
                    setupStepLoading ||
                    !changeId ||
                    !workflowInstanceId
                  }
                  onClick={() => handleSave()}
                >
                  {t('txt_save')}
                </Button>
                <Button
                  variant="primary"
                  onClick={handleNextStepClicked}
                  disabled={!isValid}
                >
                  {t('txt_next_step')}
                </Button>
              </React.Fragment>
            )}
            {isLastStep && (
              <Button
                variant="primary"
                onClick={handleSubmit}
                disabled={loading || setupStepLoading || hasAnyInvalidStep}
              >
                {t('txt_submit')}
              </Button>
            )}
          </div>
        </div>
      </ModalRegistry>
      {showDeleteWorkflowModal && changeId && workflowInstanceId && (
        <ConfirmationModal
          id={'delete-workflow-instance-modal'}
          show
          loading={removeWorkflowLoading}
          modalTitle={t('txt_confirm_deletion')}
          modalContent={
            <>
              <span>
                {t('txt_delete_workflow_instance_content')}{' '}
                <b>{workflow?.name || ''}</b>?
              </span>
            </>
          }
          modalButtons={{
            cancelText: t('txt_cancel'),
            confirmText: t('txt_delete'),
            cancelVariant: 'secondary',
            confirmVariant: 'danger'
          }}
          onCancel={() => setShowDeleteWorkflowModal(false)}
          onConfirm={handleRemoveWorkflowInstance}
        />
      )}
    </>
  );
};

export default WorkflowSetup;
