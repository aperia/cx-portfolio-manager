import React from 'react';
// store
import { useSelector } from 'react-redux';

const LoggerInternal: React.FC = props => {
  const store = useSelector<RootState, RootState>(state => state);

  // allow show log in next time
  window.debug && console.log('DEBUG', store);

  return null;
};

const Logger: React.FC = props => {
  return process.env.NODE_ENV === 'development' ? <LoggerInternal /> : <></>;
};

export default Logger;
