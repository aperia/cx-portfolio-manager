// components
import { renderWithMockStore } from 'app/utils/renderWithMockStore';
import Logger from 'pages/_commons/Logger';
import React from 'react';

describe('Test Logger', () => {
  it('log the store', async () => {
    window.debug = true;
    process = Object.create(process);
    process.env = { NODE_ENV: 'development', PUBLIC_URL: '' };
    const { queryByText } = await renderWithMockStore(<Logger />);
    expect(queryByText('logger')).toBeNull();
  });
  it("don't log the store", async () => {
    window.debug = true;
    process = Object.create(process);
    process.env = { NODE_ENV: 'production', PUBLIC_URL: '' };
    const { queryByText } = await renderWithMockStore(<Logger />);
    expect(queryByText('logger')).toBeNull();
  });
});
