import ModalRegistry from 'app/components/ModalRegistry';
import { WORKFLOW_SETUP } from 'app/constants/local-storage';
import {
  Button,
  CheckBox,
  ModalBody,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import isFunction from 'lodash.isfunction';
import React, { useState } from 'react';
import { Link } from 'react-router-dom';

interface IProps {
  id: string;
  show: boolean;

  onClose?: () => void;
}

const OverviewModal: React.FC<IProps> = ({
  id,
  show,

  onClose
}) => {
  const { t } = useTranslation();

  const [showAgain, setShowAgain] = useState(
    localStorage.getItem(WORKFLOW_SETUP.SHOW_OVERVIEW_AGAIN) !== 'false'
  );

  const handleClose = () => {
    localStorage.setItem(WORKFLOW_SETUP.SHOW_OVERVIEW_AGAIN, `${showAgain}`);

    isFunction(onClose) && onClose();
  };

  return (
    <ModalRegistry lg id={id} show={show} onAutoClosedAll={handleClose}>
      <ModalHeader border closeButton onHide={handleClose}>
        <ModalTitle>Workflow Overview</ModalTitle>
      </ModalHeader>
      <ModalBody>
        <section>
          <h5>Workflow Purpose</h5>
          <p className="mt-16">
            Workflows in Portfolio Manager are step-by-step executable actions
            to make changes to your card or loan portfolio settings on Optis, or
            to update groups of accounts identified by the user, such as
            generating letters to deliver to cardholders or making monetary
            adjustments against cardholders’ accounts. Portfolio Manager
            interacts with systems on Optis such as the Product Control File or
            the Cardholder Masterfile, and has the ability to execute monetary
            and non-monetary transactions depending on the Workflow you select.
            You can schedule a Change in Portfolio Manager that executes your
            Workflow at a specified date, with an Approval system within the
            tool that is configured to meet your organization’s needs.
          </p>
        </section>

        <section className="mt-24">
          <h5>Workflow Setup</h5>
          <p className="mt-16">
            Portfolio Manager has a distinct set of permissions, which are
            required to Setup a Workflow, which are managed in ESS. The Get
            Started page for each Workflow outlines the steps you must complete
            to Set Up the Workflow for execution. To move through the Workflow
            Setup steps, click Next Step at the bottom of each page as you
            complete the required parameters on each screen. Once you start the
            Workflow Setup, you can click Save and Close if you are unable to
            complete the setup and want to come back to this Workflow to
            complete at a later date or time. You can click Delete to remove the
            Workflow you are working on.
          </p>
          <p className="mt-8">
            A Workflow is executed by a Change record, which is the package in
            Portfolio Manager that is scheduled and approved prior to the
            Workflows performing the actions on Optis determined within the
            Workflow Setup screens for each Workflow within a Change. When
            Setting Up a Workflow, you can add the Workflow to an existing
            Change, or create a new Change that will execute the Workflow you
            are Setting Up. You can only associate a Workflow with a Change in
            Portfolio Manager under the following conditions:
          </p>
          <p className="mt-8">
            • If the Change Type matches the Workflow Type of the Workflow you
            are Setting Up
            <br />
            • A Workflow of the same Name is not already associated with a given
            Change (i.e., you cannot have two (2) “Manage Penalty Fees”
            Workflows within the same Change)
            <br />• The Change has a Status of Work, Validated, or Rejected.
          </p>
          <p className="mt-8">
            A workflow may require uploading a list of accounts, entering
            variables or parameters, updating a PCF Method for performing the
            task, or other steps. The Workflow Setup Steps for each Workflow
            contain the required parameters in order to execute the action on
            the Optis platform that the Workflow is intended to execute upon the
            Effective Date of the Change.
          </p>
          <p className="mt-8">
            Each Workflow contains a Summary of the actions to be performed on
            the Optis platform based on the parameters you entered into the
            Workflow Setup screens at the end of the Setup process. When you
            click “Submit” on the Review and Submit page at the end of the
            Workflow Setup screens, the Workflow parameters will be validated
            against the rules in the system associated with the action to be
            performed by the Workflow.
          </p>
        </section>

        <section className="mt-24">
          <h5>Workflow Execution</h5>
          <p className="mt-16">
            If the parameters within the Workflow Setup screens entered by the
            user do not comply with the Validation rules within the system, the
            Workflow will be identified as “Incomplete” within the Change and
            the Change will not be able to be executed until the Workflow passes
            Validation. The Workflow Setup screens will contain error messages
            describing the corrective action that needs to be performed in order
            for the Workflow to pass Validation before execution. No Change can
            be made Effective on the Effective Date and on the Optis platform
            until the Workflow is in a Complete Status, visible from the Change
            Record in Portfolio Manager.
          </p>
          <p className="mt-8">
            For a Change to execute the Workflows on the Optis platform, a
            Change must be reviewed and Approved by your Approval Team or Teams,
            depending on what the Change Type is and what Approval Teams are
            required to Approve this Change Type. Each Change Type (i.e.,
            “Pricing” Changes, or “Account Management” Changes) has its own
            Approval Hierarchy configured by your organization. All the
            Workflows associated with an approved Change are executed on the
            Effective Date of the Change defined by the Change Owner. If a
            change has not been Approved by the Change’s Effective Date, then it
            will not be implemented and none of the associated workflows will be
            executed, and the Change Status will be updated to Lapsed.
          </p>
          <p className="mt-8">
            For more information on the execution and Setup of Workflows, click{' '}
            <Link to={'#'}>link</Link> to access the Portfolio Manager User
            Guide.
          </p>
        </section>
      </ModalBody>
      <div className="dls-modal-footer modal-footer justify-content-between">
        <div>
          <CheckBox>
            <CheckBox.Input
              checked={!showAgain}
              onChange={e => setShowAgain(!e.target.checked)}
            />
            <CheckBox.Label>Don’t show this modal again</CheckBox.Label>
          </CheckBox>
        </div>
        <Button variant="secondary" onClick={handleClose}>
          {t('txt_close')}
        </Button>
      </div>
    </ModalRegistry>
  );
};

export default OverviewModal;
