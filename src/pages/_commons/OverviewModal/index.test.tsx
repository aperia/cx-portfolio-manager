import { fireEvent, render, RenderResult } from '@testing-library/react';
import { mockUseModalRegistryFnc } from 'app/utils';
import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import OverviewModal from '.';

const mockUseModalRegistry = mockUseModalRegistryFnc();

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

describe('ManagePenaltyFeeWorkflow > GettingStartStep > OverviewModal', () => {
  const renderModal = (onCloseFn: any): RenderResult => {
    return render(
      <BrowserRouter>
        <OverviewModal id={'modal-id'} show={true} onClose={onCloseFn} />
      </BrowserRouter>
    );
  };

  beforeEach(() => {
    mockUseModalRegistry.mockImplementation(() => [true, false]);
  });

  afterEach(() => {
    mockUseModalRegistry.mockClear();
  });

  it('Should open modal', () => {
    const onCloseFn = jest.fn();
    const wrapper = renderModal(onCloseFn);
    expect(wrapper.getByText('Workflow Overview')).toBeInTheDocument;
  });

  it('Should click on Dont show again checkbox', () => {
    const onCloseFn = jest.fn();
    renderModal(onCloseFn);
    const checkbox = document.getElementsByClassName(
      'dls-checkbox-input'
    )[0] as Element;
    const _checkbox = checkbox as HTMLInputElement;
    expect(_checkbox.checked).toEqual(false);
    fireEvent.click(checkbox);
    expect(_checkbox.checked).toEqual(true);
  });

  it('Should click on close button', () => {
    const onCloseFn = jest.fn();
    const wrapper = renderModal(onCloseFn);
    fireEvent.click(wrapper.getByText('txt_close'));
    expect(onCloseFn).toHaveBeenCalled();
  });
});
