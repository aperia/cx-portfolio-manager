import { RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { renderComponent } from 'app/utils';
import * as CommonRedux from 'pages/_commons/redux/Common/select-hooks';
import React from 'react';
import ContentExpand, { ContentExpandProps } from '.';

const mockUseSelectWindowDimension = jest.spyOn(
  CommonRedux,
  'useSelectWindowDimension'
);
const mockWindowDimension = (height: number) => {
  mockUseSelectWindowDimension.mockImplementation(() => ({ height } as any));
};

const mockRenderSimpleBarChildren = jest.fn();
jest.mock('app/_libraries/_dls/components/SimpleBar', () => ({
  __esModule: true,
  default: ({ children }: any) => (
    <div>{mockRenderSimpleBarChildren() ? children : undefined}</div>
  )
}));

describe('commons > ContentExpand', () => {
  const idDomTest = 'test';
  const defaultProps: ContentExpandProps = {
    instruction: 'instruction'
  };

  let renderResult: RenderResult;
  const createWrapper = async (
    children?: React.ReactNode,
    isSelected = true
  ) => {
    renderResult = await renderComponent(
      idDomTest,
      <ContentExpand {...defaultProps} isSelectedStep={isSelected}>
        {children}
      </ContentExpand>
    );
  };

  beforeEach(() => {
    mockWindowDimension(1000);
    mockRenderSimpleBarChildren.mockReturnValue(true);
  });

  it('should render', async () => {
    jest.useFakeTimers();

    await createWrapper('', false);
    jest.runAllTicks();

    const { getByText } = renderResult;
    expect(getByText('instruction')).toBeInTheDocument();

    jest.useRealTimers();
  });

  it('should render component without children', async () => {
    jest.useFakeTimers();

    await createWrapper();
    jest.runAllTicks();

    const { getByText } = renderResult;
    expect(getByText('instruction')).toBeInTheDocument();

    jest.useRealTimers();
  });

  it('should render component large children', async () => {
    mockWindowDimension(100);
    jest.useFakeTimers();

    await createWrapper(<div className="p-40">content</div>);
    jest.runAllTicks();

    const { getByText } = renderResult;
    expect(getByText('instruction')).toBeInTheDocument();

    jest.useRealTimers();
  });

  it('should show expand', async () => {
    mockWindowDimension(129);
    jest.useFakeTimers();

    await createWrapper();
    jest.runAllTicks();

    const { getByText, container, debug } = renderResult;

    debug();
    const expandBtn = container.querySelector(
      '.content-expand__expanded-btn button.btn'
    );
    userEvent.hover(expandBtn!);
    expect(getByText('txt_expand')).toBeInTheDocument();

    userEvent.click(expandBtn!);
    userEvent.hover(expandBtn!);
    expect(getByText('txt_collapse')).toBeInTheDocument();

    jest.useRealTimers();
  });

  it('should render without instruction', async () => {
    mockWindowDimension(100);
    mockRenderSimpleBarChildren.mockReturnValue(false);
    jest.useFakeTimers();

    await createWrapper(<div className="p-40">content</div>);
    jest.runAllTicks();

    const { queryByText } = renderResult;
    expect(queryByText('instruction')).not.toBeInTheDocument();

    jest.useRealTimers();
  });
});
