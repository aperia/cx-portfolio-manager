import { classnames } from 'app/helpers';
import {
  Button,
  Icon,
  SimpleBar,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import React, { useEffect, useRef, useState } from 'react';
import { useSelectWindowDimension } from '../redux/Common';

export interface ContentExpandProps {
  instruction: React.ReactNode;

  isSelectedStep?: boolean;
  change?: any;
  className?: String;
  children?: React.ReactNode;
}
const ContentExpand: React.FC<ContentExpandProps> = ({
  instruction,

  isSelectedStep,
  change,
  className,
  children
}) => {
  const { t } = useTranslation();
  const instructionRef = useRef<HTMLDivElement>(null);
  const expandedRef = useRef<HTMLDivElement>(null);
  const expandedContentRef = useRef<HTMLDivElement>(null);

  const { height } = useSelectWindowDimension();

  const [expanded, setExpanded] = useState(true);
  const [hasExpanded, setHasExpanded] = useState(false);

  useEffect(() => {
    if (!isSelectedStep) return;

    process.nextTick(() => {
      if (!instructionRef.current) return;

      const instructionHeight =
        instructionRef.current.getBoundingClientRect().height;

      const bodyHeight = height - 129; // header 60 + footer 69
      const hasExpanded = bodyHeight / 2 <= instructionHeight;
      setHasExpanded(hasExpanded);

      if (!expandedRef.current || !expandedContentRef.current || !hasExpanded)
        return;

      const expandedContentHeight =
        expandedContentRef.current!.getBoundingClientRect().height;

      let expandedHeight = bodyHeight - instructionHeight;
      if (expandedHeight < expandedContentHeight) expandedHeight = 24;

      expandedRef.current.style.height = `${expandedHeight}px`;
    });
  }, [height, isSelectedStep, change]);

  return (
    <div
      className={classnames('content-expand', className, {
        expanded,
        'has-expanded': hasExpanded
      })}
    >
      <div className="content-expand__instruction">
        <SimpleBar>
          <div ref={instructionRef}>{instruction}</div>

          {!hasExpanded && (
            <div className="content-expand__content">{children}</div>
          )}
        </SimpleBar>
      </div>
      {hasExpanded && (
        <div ref={expandedRef} className="content-expand__expanded">
          <div className="content-expand__expanded-btn">
            <Tooltip
              element={!expanded ? t('txt_collapse') : t('txt_expand')}
              variant="primary"
              placement="top"
            >
              <Button
                variant="icon-secondary"
                onClick={() => setExpanded(!expanded)}
              >
                <Icon
                  size="4x"
                  name={expanded ? 'chevron-down' : 'chevron-up'}
                />
              </Button>
            </Tooltip>
          </div>

          <div ref={expandedContentRef} className="content-expand__content">
            <SimpleBar>{children} </SimpleBar>
          </div>
        </div>
      )}
    </div>
  );
};
export default ContentExpand;
