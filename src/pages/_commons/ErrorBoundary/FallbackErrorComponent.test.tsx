import { fireEvent, render } from '@testing-library/react';
// Components
import FallbackErrorComponent from 'pages/_commons/ErrorBoundary/FallbackErrorComponent';
import React from 'react';

describe('Testing FallbackErrorComponent', () => {
  it('Should unmount component when click try again', () => {
    const { getByText } = render(<FallbackErrorComponent />);

    expect(getByText(/something went wrong/i)).not.toBeEmptyDOMElement();
    const tryAgainBtn = getByText(/try again/i);
    fireEvent.click(tryAgainBtn);
    expect(tryAgainBtn).not.toBeEmptyDOMElement();
  });
});
