import { Button } from 'app/_libraries/_dls';
import React from 'react';

export interface FallbackComponentProps {}

const FallbackErrorComponent: React.FC<FallbackComponentProps> = () => {
  return (
    <div className="d-flex justify-content-center align-items-center vw-100 vh-100">
      <div className="text-center">
        <h2 className="mt-16">Something went wrong</h2>

        <Button
          id="tryAgainBtn"
          className="fs-14 mt-16"
          variant="primary"
          onClick={() => window.location.reload()}
        >
          Try again
        </Button>
      </div>
    </div>
  );
};

export default FallbackErrorComponent;
