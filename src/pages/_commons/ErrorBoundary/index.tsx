import React, { ErrorInfo } from 'react';
import { connect } from 'react-redux';

const mapDispatchToProps = {};

const mapStateToProps: any = () => {
  return {};
};

export type ErrorBoundaryProps = Omit<
  ReturnType<typeof mapStateToProps> & typeof mapDispatchToProps,
  'props'
>;

class ErrorBoundary extends React.Component<any, any> {
  constructor(props: ErrorBoundaryProps) {
    super(props);
    this.state = {
      error: null,
      errorInfo: null
    };

    this.handleErrorEvent = this.handleErrorEvent.bind(this);
  }

  // Update state error then show the fallback component
  static getDerivedStateFromError(error: Error) {
    return { error };
  }

  handleErrorEvent(event: ErrorEvent) {
    // dispatch a toast error
  }

  // Catch any errors will be throw by any components bellow the ErrorBoundary and render the error message
  componentDidCatch(error: Error, errorInfo: ErrorInfo) {
    const errorReported = {
      error: error,
      errorInfo
    };

    this.setState(errorReported);
  }

  componentDidMount() {
    window.addEventListener('error', this.handleErrorEvent);
  }

  componentWillUnmount() {
    window.removeEventListener('error', this.handleErrorEvent);
  }

  render() {
    const { fallbackComponent: FallbackComponent, children } = this.props;
    const { error } = this.state;

    if (error) {
      return <FallbackComponent />;
    }
    return children;
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ErrorBoundary);
