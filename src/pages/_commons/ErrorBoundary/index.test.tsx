import { screen } from '@testing-library/dom';
import { renderWithMockStore } from 'app/utils/renderWithMockStore';
// Components
import ErrorBoundary from 'pages/_commons/ErrorBoundary';
import FallbackErrorComponent from 'pages/_commons/ErrorBoundary/FallbackErrorComponent';
import React from 'react';

const NoErrorComponent: React.FC = () => <p>Testing Component</p>;
const ErrorComponent: React.FC = () => {
  throw new Error('Error Boundary');
};

describe('Test ErrorBoundary ', () => {
  it('Should render children when there have no error', async () => {
    await renderWithMockStore(
      <ErrorBoundary fallbackComponent={FallbackErrorComponent}>
        <NoErrorComponent />
      </ErrorBoundary>
    );
    expect(screen.getByText(/testing component/i)).not.toBeEmptyDOMElement();
  });

  it('Should render fallback component when crash render', async () => {
    await renderWithMockStore(
      <ErrorBoundary fallbackComponent={FallbackErrorComponent}>
        <ErrorComponent />
      </ErrorBoundary>
    );

    window.dispatchEvent(new Event('error'));
    const queryTextWrong = screen.getByText('Something went wrong');
    expect(queryTextWrong).toBeInTheDocument();
  });
});
