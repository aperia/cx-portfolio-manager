import { fireEvent, render, RenderResult } from '@testing-library/react';
import { mockUseModalRegistryFnc, wait } from 'app/utils';
import * as unSaveHook from 'pages/_commons/redux/UnsavedChanges';
import React from 'react';
import * as reactRedux from 'react-redux';
import UnsavedConfirmModal from './index';

const mockUseModalRegistry = mockUseModalRegistryFnc();

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');
const mockCurrentIncompletedForm = jest.spyOn(
  unSaveHook,
  'useCurrentIncompletedForm'
);
const mockUnsavedRedirectInfo = jest.spyOn(
  unSaveHook,
  'useUnsavedRedirectInfo'
);

describe('Test UnsavedConfirmModal Component', () => {
  const mockDispatch = jest.fn();

  const renderConfirmationModal = (): RenderResult => {
    return render(
      <div>
        <UnsavedConfirmModal />
      </div>
    );
  };
  const modalProps = {
    formName: 'mockForm',
    confirmTitle: 'Mock title',
    confirmMessage: <p>Mock unsaved message</p>,
    priority: 0,
    change: true,
    confirmBtn: { text: 'txt_discard_changes' }
  };

  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
    mockUseModalRegistry.mockImplementation(() => [true, false]);
    mockUnsavedRedirectInfo.mockReturnValue({});
    mockCurrentIncompletedForm.mockReturnValue({ ...modalProps });
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
    mockCurrentIncompletedForm.mockClear();
    mockUnsavedRedirectInfo.mockClear();
    mockUseModalRegistry.mockClear();
  });

  it('should render UnsavedConfirmModal UI > modal not open', async () => {
    const wrapper = renderConfirmationModal();

    expect(wrapper.baseElement.firstChild?.firstChild).toBeEmptyDOMElement();
  });

  it('should render UnsavedConfirmModal UI > modal not open when redirect is complete', async () => {
    mockUnsavedRedirectInfo.mockReturnValue({
      to: undefined,
      completed: true,
      formsWatcher: ['mockForm']
    });

    const wrapper = renderConfirmationModal();
    expect(wrapper.baseElement.firstChild?.firstChild).toBeEmptyDOMElement();
  });

  it('should render UnsavedConfirmModal UI > modal open', async () => {
    const onConfirmFn = jest.fn().mockResolvedValue(true);

    mockUnsavedRedirectInfo.mockReturnValue({
      to: undefined,
      completed: false,
      formsWatcher: ['mockForm'],
      onConfirm: onConfirmFn
    });

    const wrapper = renderConfirmationModal();
    expect(wrapper.getByRole('dialog')).toBeInTheDocument();
    expect(wrapper.getByText('Mock title')).toBeInTheDocument();
    expect(wrapper.getByText('Mock unsaved message')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('txt_discard_changes'));
    await wait();

    expect(onConfirmFn).toHaveBeenCalled();
    expect(mockDispatch).toHaveBeenCalled();
  });

  it('should render UnsavedConfirmModal UI > modal open with type=confirmation', async () => {
    const discardFn = jest.fn();
    const closeFn = jest.fn();
    const confirmFn = jest.fn();

    mockUnsavedRedirectInfo.mockReturnValue({
      to: '/mock-url',
      completed: false,
      formsWatcher: ['mockForm'],
      onDiscard: jest.fn(),
      onClose: jest.fn(),
      onConfirm: jest.fn().mockResolvedValue(true)
    });

    mockCurrentIncompletedForm.mockReturnValue({
      formName: 'mockForm',
      confirmTitle: 'Mock title',
      confirmMessage: 'Mock unsaved message',
      priority: 0,
      change: true,
      discardBtn: { text: 'txt_discard' },
      onDiscard: discardFn,
      onClose: closeFn,
      onConfirm: confirmFn
    });

    const wrapper = renderConfirmationModal();
    expect(wrapper.getByRole('dialog')).toBeInTheDocument();
    expect(wrapper.getByText('txt_discard')).toBeInTheDocument();
    expect(wrapper.getByText('txt_cancel')).toBeInTheDocument();
    expect(wrapper.getByText('txt_confirm')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('txt_discard'));

    expect(discardFn).toHaveBeenCalled();
    expect(mockDispatch).toHaveBeenCalled();

    mockDispatch.mockClear();

    fireEvent.click(wrapper.getByText('txt_cancel'));

    expect(closeFn).toHaveBeenCalled();
    expect(mockDispatch).toHaveBeenCalled();

    mockDispatch.mockClear();

    fireEvent.click(wrapper.getByText('txt_confirm'));
    await wait();

    expect(confirmFn).toHaveBeenCalled();
    expect(mockDispatch).toHaveBeenCalled();
  });
});
