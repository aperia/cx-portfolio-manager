import ModalRegistry from 'app/components/ModalRegistry';
import {
  Button,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import { isFunction, isString } from 'lodash';
import React, { useMemo } from 'react';
import { useDispatch } from 'react-redux';
import {
  actionsUnsavedChanges,
  useCurrentIncompletedForm,
  useUnsavedRedirectInfo
} from '../redux/UnsavedChanges';

interface UnsavedConfirmModalProps {}
const UnsavedConfirmModal: React.FC<UnsavedConfirmModalProps> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const {
    to: redirectTo,
    formsWatcher,
    onClose,
    onDiscard,
    onConfirm,
    completed: isRedirectCompleted
  } = useUnsavedRedirectInfo();
  const currentIncompleted = useCurrentIncompletedForm(formsWatcher);
  const incompletedForm = isRedirectCompleted ? undefined : currentIncompleted;

  const hasRedirectTo = !!redirectTo;
  const hasActions =
    isFunction(onConfirm) || isFunction(onClose) || isFunction(onDiscard);
  const opened =
    !isRedirectCompleted &&
    !!incompletedForm &&
    !incompletedForm.hideUnsavedConfirmation &&
    (hasRedirectTo || hasActions);

  const handleDiscard = () => {
    isFunction(onDiscard) && onDiscard(incompletedForm!.formName);
    isFunction(incompletedForm?.onDiscard) && incompletedForm?.onDiscard();
    dispatch(actionsUnsavedChanges.resetRedirectInfo());
  };
  const handleClose = () => {
    isFunction(onClose) && onClose(incompletedForm!.formName);
    isFunction(incompletedForm?.onClose) && incompletedForm?.onClose();
    dispatch(actionsUnsavedChanges.resetRedirectInfo());
  };
  const handleConfirm = async () => {
    dispatch(actionsUnsavedChanges.resetRedirectInfo());

    !!incompletedForm?.confirmFirst &&
      isFunction(incompletedForm?.onConfirm) &&
      (await incompletedForm?.onConfirm());

    let isSucess;
    if (isFunction(onConfirm))
      isSucess = await onConfirm(incompletedForm!.formName);

    !incompletedForm?.confirmFirst &&
      isFunction(incompletedForm?.onConfirm) &&
      incompletedForm?.onConfirm(isSucess);

    if (redirectTo) {
      window.location.href = redirectTo;
    }
  };

  const size = useMemo(
    () => ({ [incompletedForm?.size || 'xs']: true }),
    [incompletedForm?.size]
  );

  if (!opened) return <React.Fragment></React.Fragment>;
  return (
    <ModalRegistry
      {...size}
      id="modal-unsaved-confirmation"
      show={opened}
      onAutoClosedAll={handleClose}
    >
      <ModalHeader border closeButton onHide={handleClose}>
        <ModalTitle>{t(incompletedForm?.confirmTitle)}</ModalTitle>
      </ModalHeader>
      <ModalBody>
        {isString(incompletedForm?.confirmMessage)
          ? t(incompletedForm?.confirmMessage)
          : incompletedForm?.confirmMessage}
      </ModalBody>
      <ModalFooter className={incompletedForm?.footerClassName}>
        {incompletedForm?.discardBtn?.text && (
          <Button
            className={incompletedForm.discardBtn.className || 'mr-auto ml-n8'}
            variant={incompletedForm.discardBtn.variant || 'outline-danger'}
            onClick={handleDiscard}
          >
            {t(incompletedForm.discardBtn.text)}
          </Button>
        )}

        {!incompletedForm?.closeBtn?.hide && (
          <Button
            className={incompletedForm?.closeBtn?.className}
            variant={incompletedForm?.closeBtn?.variant || 'secondary'}
            onClick={handleClose}
          >
            {t(incompletedForm?.closeBtn?.text || 'txt_cancel')}
          </Button>
        )}
        {!incompletedForm?.confirmBtn?.hide && (
          <Button
            className={incompletedForm?.confirmBtn?.className}
            variant={incompletedForm?.confirmBtn?.variant || 'primary'}
            onClick={handleConfirm}
          >
            {t(incompletedForm?.confirmBtn?.text || 'txt_confirm')}
          </Button>
        )}
      </ModalFooter>
    </ModalRegistry>
  );
};

export default UnsavedConfirmModal;
