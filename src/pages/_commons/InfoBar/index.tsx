import { isDevice } from 'app/helpers';
import { InfoBar as InfoBarBS, InfoBarItem } from 'app/_libraries/_dls';
import React, { useEffect, useMemo, useRef, useState } from 'react';
import { useLocation } from 'react-router-dom';
import UtilitiesTab from './UtilitiesTab';
import WorkflowsTab from './WorkflowsTab';

const InfoBar: React.FC = () => {
  const [showInfoBar, setShowInfoBar] = useState(false);
  const infoBarRef = useRef<HTMLDivElement>(null);
  const location = useLocation();

  const items = useMemo<InfoBarItem[]>(
    () => [
      {
        id: 'portfolios',
        name: 'Portfolios',
        icon: { name: 'card-collection' },
        showTooltip: isDevice ? false : undefined,
        component: () => <div />
      },
      {
        id: 'workflows',
        name: 'Workflows',
        icon: { name: 'workflow' as any },
        showTooltip: isDevice ? false : undefined,
        keepComponent: true,
        component: WorkflowsTab
      },
      {
        id: 'reminderList',
        name: 'Reminder List',
        showTooltip: isDevice ? false : undefined,
        icon: { name: 'notification-manage' },
        component: () => <div />
      },
      {
        id: 'utilities',
        name: 'Utilities',
        showTooltip: isDevice ? false : undefined,
        icon: { name: 'tools' },
        component: UtilitiesTab
      }
    ],
    []
  );

  // remove expand/colapse button
  useEffect(() => {
    infoBarRef?.current
      ?.querySelector('.infobar-nav-item.css-tooltip-wrapper')
      ?.remove();
  }, []);

  // remount infobar after change route
  useEffect(() => {
    setShowInfoBar(false);
  }, [location]);
  useEffect(() => {
    if (showInfoBar) return;

    setShowInfoBar(true);
  }, [showInfoBar]);

  if (!showInfoBar) return <React.Fragment />;

  return <InfoBarBS toggle={false} ref={infoBarRef} items={items} />;
};
export default InfoBar;
