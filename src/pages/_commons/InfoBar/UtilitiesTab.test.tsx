import userEvent from '@testing-library/user-event';
import * as helper from 'app/helpers/isDevice';
import { mockUseDispatchFnc, renderComponent } from 'app/utils';
import 'app/utils/_mockComponent/mockSortAndOrder';
import UtilitiesTab from 'pages/_commons/InfoBar/UtilitiesTab';
import * as nonWorkflow from 'pages/_commons/redux/WorkflowSetup/select-hooks/nonWorkflow';
import React from 'react';
import * as ReactRedux from 'react-redux';

const mockNonWorkflow = jest.spyOn(
  nonWorkflow,
  'useSelectElementMetadataNonWorkflow'
);

const mockUseDispatch = mockUseDispatchFnc();
const mockSelector = jest.spyOn(ReactRedux, 'useSelector');
const mockHelper: any = helper;

const testId = 'UtilitiesTab';

const props = {
  handlePin: jest.fn(),
  handleUnpin: jest.fn(),
  handleExpand: jest.fn(),
  handleCollapse: jest.fn(),
  id: 'UtilitiesTab'
};

HTMLCanvasElement.prototype.getContext = jest.fn();

describe('Test UtilitiesTab RAF', () => {
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => jest.fn());
    mockNonWorkflow.mockReturnValue({
      systemPrincipalAgentsOptions: [
        {
          code: '0000-0200-3456'
        },
        {
          code: '0000-0200-0001'
        },
        {
          code: '0000-0200-0001'
        },
        {
          code: '0000-0201-0009'
        },
        {
          code: '0010-0300-0005'
        },
        {
          code: '0010-0301'
        }
      ],
      dmmTableTypeOptions: [
        {
          code: 'ALP',
          textDmm: 'Account Level Processing',
          text: 'Account Level Processing'
        },
        {
          code: 'MLP',
          textDmm: 'Method Level Processing',
          text: 'Method Level Processing'
        },
        {
          code: 'TLP',
          textDmm: 'Transaction Level Processing',
          text: 'Transaction Level Processing'
        }
      ],
      alpTableOptions: [
        {
          code: 'AQ',
          text: 'Account Qualification',
          textDmm: 'Account Qualification'
        },
        {
          code: 'CA',
          text: 'Client Allocation',
          textDmm: 'Client Allocation'
        }
      ],
      mlpTableOptions: [
        {
          code: 'AQ',
          text: 'Account Qualification',
          textDmm: 'Account Qualification'
        },
        {
          code: 'CA',
          text: 'Client Allocation',
          textDmm: 'Client Allocation'
        },
        {
          code: 'ST',
          text: 'Selection Table',
          textDmm: 'Selection Table'
        }
      ],
      tlpTableOptions: [
        {
          code: 'TQ',
          text: 'Transaction Qualification',
          textDmm: 'Transaction Qualification'
        },
        {
          code: 'AQ',
          text: 'Account Qualification',
          textDmm: 'Account Qualification'
        },
        {
          code: 'CA',
          text: 'Client Allocation',
          textDmm: 'Client Allocation'
        },
        {
          code: 'RQ',
          text: 'Retail Qualification',
          textDmm: 'Retail Qualification'
        }
      ]
    });
    mockSelector.mockImplementation(selector =>
      selector({
        workflowSetup: {
          services: {
            data: [
              {
                name: 'CP',
                description: 'Cardholder Pricing',
                subjects: [
                  {
                    name: 'IC',
                    description: 'Interest Charges',
                    sections: [
                      {
                        name: 'IC',
                        description: 'Interest Charges'
                      },
                      {
                        name: 'IB',
                        description: 'Incentive Pricing Break Points'
                      },
                      {
                        name: 'ID',
                        description: 'Interest Defaults'
                      },
                      {
                        name: 'IF',
                        description: 'Interest and Fees'
                      },
                      {
                        name: 'II',
                        description: 'Interest on Interest'
                      },
                      {
                        name: 'IM',
                        description: 'Interest Methods'
                      },
                      {
                        name: 'IP',
                        description: 'Incentive Pricing'
                      },
                      {
                        name: 'IR',
                        description: 'Index Rate'
                      },
                      {
                        name: 'IV',
                        description: 'Incentive Pricing Variable Interest'
                      }
                    ]
                  },
                  {
                    name: 'IO',
                    description: 'Income Options',
                    sections: [
                      {
                        name: 'AC',
                        description: 'Annual Charges'
                      },
                      {
                        name: 'CI',
                        description: 'Cash Advance Item Charges'
                      },
                      {
                        name: 'CL',
                        description: 'Credit Life'
                      },
                      {
                        name: 'MC',
                        description: 'Miscellaneous Charges'
                      },
                      {
                        name: 'MI',
                        description: 'Merchandise Item Charges'
                      },
                      {
                        name: 'SC',
                        description: 'Statement Charges'
                      }
                    ]
                  },
                  {
                    name: 'OC',
                    description: 'Other Controls',
                    sections: [
                      {
                        name: 'AP',
                        description: 'Adjustment Pricing'
                      },
                      {
                        name: 'BD',
                        description: 'Backdating'
                      },
                      {
                        name: 'CL',
                        description: 'Credit Line Management'
                      },
                      {
                        name: 'CP',
                        description: 'Current Pricing Notification'
                      },
                      {
                        name: 'DR',
                        description: 'Debit Ratification'
                      }
                    ]
                  }
                ]
              },
              {
                name: 'AM',
                description: 'Account Marketing',
                subjects: [
                  {
                    name: 'AA',
                    description: 'Alternate Accounts',
                    sections: [
                      {
                        name: 'HE',
                        description: 'Home Equity'
                      }
                    ]
                  },
                  {
                    name: 'FD',
                    description: 'Group Delimiters',
                    sections: [
                      {
                        name: 'DC',
                        description: 'Decision Controls'
                      },
                      {
                        name: 'ER',
                        description: 'Exception Report'
                      },
                      {
                        name: 'KC',
                        description: 'Key Controls'
                      }
                    ]
                  },
                  {
                    name: 'PS',
                    description: 'Payment Selection',
                    sections: [
                      {
                        name: 'PA',
                        description: 'Pay Ahead'
                      },
                      {
                        name: 'PR',
                        description: 'Payment Reversal'
                      }
                    ]
                  }
                ]
              }
            ]
          },
          tableListWorkflowSetup: {
            data: [
              { tableId: '7UVS5T7O', tableName: '9O4B720B' },
              { tableId: '7UVS5T74', tableName: '9O4B7204' },
              { tableId: '7UVS5T77', tableName: '9O4B7207' }
            ]
          },
          tableListItem: [
            {
              code: 'All',
              text: 'All'
            },
            {
              code: '00CWEQMN',
              text: 'HQ7V3WID'
            },
            {
              code: '1B4GFSU0',
              text: '8KXOMR45'
            },
            {
              code: '1EI3O7PF',
              text: 'ILTUMZ5C'
            },
            {
              code: '2MIL16UA',
              text: 'OH1XSWVA'
            },
            {
              code: '4HKRR2Y3',
              text: 'YU2LT4DN'
            },
            {
              code: '5G5X7B0X',
              text: 'JPU71HFF'
            },
            {
              code: '6BFYXUVP',
              text: 'JP2GZDBL'
            },
            {
              code: '7MIQMS4U',
              text: 'JEG34WNC'
            },
            {
              code: 'A9VKU0KW',
              text: '777XIEXB'
            },
            {
              code: 'AL4UPPXO',
              text: '7N8DJ4OJ'
            },
            {
              code: 'AUDY6Z2X',
              text: 'LCVOHOQW'
            },
            {
              code: 'CHUM5LPF',
              text: 'QV88ASNH'
            },
            {
              code: 'CS2D2Q9Y',
              text: 'APBRJ34K'
            },
            {
              code: 'DH67YK5R',
              text: 'SLTRPXZ8'
            },
            {
              code: 'G9Z3SVB6',
              text: 'SSK2XVK4'
            },
            {
              code: 'IY7DEFVW',
              text: 'AWJ1ZKKG'
            },
            {
              code: 'J38G7TNC',
              text: 'J4LPEAOD'
            },
            {
              code: 'J60AIMPV',
              text: 'E18IZHJW'
            },
            {
              code: 'JYD9BWQ3',
              text: 'OJ1URPWR'
            },
            {
              code: 'KW3PJRR8',
              text: 'GKE4I48J'
            },
            {
              code: 'LTM51L0J',
              text: '1QO3YXCC'
            },
            {
              code: 'OUA0HSW2',
              text: 'R5NT3A0T'
            },
            {
              code: 'P6IAJB2S',
              text: 'DZ56M0R5'
            },
            {
              code: 'QLYYGAZ9',
              text: 'YI0OQGXC'
            },
            {
              code: 'TTFK6AQ4',
              text: 'HR302CCA'
            },
            {
              code: 'U25XKWJX',
              text: 'RXAOH2W3'
            },
            {
              code: 'VGP3JM5Z',
              text: 'FZN6SMJ2'
            },
            {
              code: 'WNW00JQR',
              text: 'MUA8CJ2R'
            },
            {
              code: 'X6ZH1VFY',
              text: 'ZWRIM2NO'
            },
            {
              code: 'YNFICCJV',
              text: '2CDAUOCZ'
            }
          ]
        }
      })
    );
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
  });

  it('should render UtilitiesTab DMM ALP', async () => {
    const { getByText } = await renderComponent(
      testId,
      <UtilitiesTab {...props} />
    );

    const checkboxDMM = getByText('txt_utilities_dmm_analyzer');
    userEvent.click(checkboxDMM);
    const checkboxALP = getByText('Account Level Processing');
    userEvent.click(checkboxALP);
    const checkboxSelectected = getByText('Account Qualification');
    userEvent.click(checkboxSelectected);
    const comboboxTableId = getByText(
      'txt_utilities_dmm_analyzer_select_tableId'
    );
    userEvent.click(comboboxTableId);
    userEvent.click(getByText('9O4B720B'));
    expect(comboboxTableId).toBeTruthy();
  });

  it('should render UtilitiesTab DMM MLP', async () => {
    const { getByText } = await renderComponent(
      testId,
      <UtilitiesTab {...props} />
    );

    const checkboxDMM = getByText('txt_utilities_dmm_analyzer');
    userEvent.click(checkboxDMM);
    const checkboxMLP = getByText('Method Level Processing');
    userEvent.click(checkboxMLP);
    userEvent.click(checkboxMLP);
    const checkboxSelectected = getByText('Account Qualification');
    userEvent.click(checkboxSelectected);
    const btnExport = getByText('txt_utilities_export_button');
    userEvent.click(btnExport);
  });

  it('should render UtilitiesTab DMM TLP', async () => {
    const { getByText } = await renderComponent(
      testId,
      <UtilitiesTab {...props} />
    );

    const checkboxDMM = getByText('txt_utilities_dmm_analyzer');
    userEvent.click(checkboxDMM);
    const checkboxTLP = getByText('Transaction Level Processing');
    userEvent.click(checkboxTLP);
    const checkboxSelectected = getByText('Account Qualification');
    userEvent.click(checkboxSelectected);
  });

  it('should render UtilitiesTab PCF', async () => {
    mockHelper.isDevice = true;
    const { getByText, getAllByText } = await renderComponent(
      testId,
      <UtilitiesTab {...props} />
    );
    const checkboxPCF = getByText('txt_utilities_pcf_analyzer');

    userEvent.click(checkboxPCF);
    userEvent.click(checkboxPCF);
    const dropdownService = getByText('txt_utilities_pcf_analyzer_service');
    userEvent.click(dropdownService);
    userEvent.click(getByText('AM, Account Marketing'));
    const dropdownSystem = getByText(
      'txt_utilities_pcf_analyzer_select_system_prin_agent_system_placeholder'
    );
    userEvent.click(dropdownSystem);
    userEvent.click(getAllByText('0000')[0]);

    const dropdownPrin = getByText(
      'txt_utilities_pcf_analyzer_select_system_prin_agent_prin_placeholder'
    );
    userEvent.click(dropdownPrin);
    userEvent.click(getAllByText('All')[1]);
    userEvent.click(dropdownPrin);
    userEvent.click(getAllByText('0200')[0]);
    const dropdownAgent = getByText(
      'txt_utilities_pcf_analyzer_select_system_prin_agent_agent_placeholder'
    );
    userEvent.click(dropdownAgent);
    userEvent.click(getAllByText('0001')[0]);
    userEvent.click(dropdownSystem);
    userEvent.click(getAllByText('All')[0]);
    const dropdownSubject = getByText('txt_utilities_pcf_analyzer_subject');
    userEvent.click(dropdownSubject);
    userEvent.click(getByText('AA, Alternate Accounts'));
    userEvent.click(getByText('txt_utilities_export_button'));
  });

  it('should render UtilitiesTab PCF when one subject list', async () => {
    mockHelper.isDevice = false;
    mockSelector.mockImplementation(selector =>
      selector({
        workflowSetup: {
          services: {
            data: [
              {
                name: 'AM',
                description: 'Account Marketing',
                subjects: [
                  {
                    name: 'AA',
                    description: 'Alternate Accounts',
                    sections: [
                      {
                        name: 'HE',
                        description: 'Home Equity'
                      },
                      {
                        name: 'BE',
                        description: 'Bee Equity'
                      }
                    ]
                  }
                ]
              }
            ]
          },
          tableListWorkflowSetup: {
            data: [{ tableId: '7UVS5T7O', tableName: '9O4B720B' }]
          }
        }
      })
    );
    const { getByText } = await renderComponent(
      testId,
      <UtilitiesTab {...props} />
    );
    const checkboxPCF = getByText('txt_utilities_pcf_analyzer');

    userEvent.click(checkboxPCF);
    const dropdownService = getByText('txt_utilities_pcf_analyzer_service');
    userEvent.click(dropdownService);
    userEvent.click(getByText('AM, Account Marketing'));
    const dropdownSubject = getByText('txt_utilities_pcf_analyzer_subject');
    userEvent.click(dropdownSubject);
    userEvent.click(getByText('AA, Alternate Accounts'));
    const dropdownSection = getByText('txt_utilities_pcf_analyzer_section');
    userEvent.click(dropdownSection);
    userEvent.click(getByText('HE, Home Equity'));
  });
});
