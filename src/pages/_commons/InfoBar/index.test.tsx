import { fireEvent } from '@testing-library/react';
import * as helper from 'app/helpers/isDevice';
import { mockUseDispatchFnc, renderComponent } from 'app/utils';
import 'app/utils/_mockComponent/mockSortAndOrder';
import { App as DOFApp } from 'app/_libraries/_dof/core';
import InfoBar from 'pages/_commons/InfoBar';
import * as nonWorkflow from 'pages/_commons/redux/WorkflowSetup/select-hooks/nonWorkflow';
import React from 'react';
import * as ReactRedux from 'react-redux';

const mockNonWorkflow = jest.spyOn(
  nonWorkflow,
  'useSelectElementMetadataNonWorkflow'
);

const mockHelper: any = helper;
const mockUseDispatch = mockUseDispatchFnc();
const mockSelector = jest.spyOn(ReactRedux, 'useSelector');

jest.mock('./WorkflowsTab', () => () => <div></div>);
jest.mock('react-router-dom', () => ({
  useLocation: jest.fn()
}));

const testId = 'InfoBar';
describe('Test InfoBar RAF', () => {
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => jest.fn());
    mockNonWorkflow.mockReturnValue({
      systemPrincipalAgentsOptions: [],
      dmmTableTypeOptions: [],
      alpTableOptions: [],
      mlpTableOptions: [],
      tlpTableOptions: []
    });
    mockSelector.mockImplementation(selector =>
      selector({
        workflowSetup: {
          services: { data: [] },
          tableListWorkflowSetup: { data: [] }
        }
      })
    );
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
  });

  it('render to UI', async () => {
    const renderResult = await renderComponent(
      testId,
      <DOFApp urlConfigs={[]}>
        <InfoBar />
      </DOFApp>
    );
    const wrapper = await renderResult.findByTestId(testId);
    const allTab = wrapper.querySelectorAll('i');
    allTab.forEach(element => fireEvent.click(element));
    expect(allTab.length).toEqual(4);
  });

  it('should not show tooltip in device', async () => {
    mockHelper.isDevice = true;
    const renderResult = await renderComponent(
      testId,
      <DOFApp urlConfigs={[]}>
        <InfoBar />
      </DOFApp>
    );
    const wrapper = await renderResult.findByTestId(testId);
    const allTab = wrapper.querySelectorAll('i');
    allTab.forEach(element => fireEvent.click(element));
    expect(allTab.length).toEqual(4);
  });
});
