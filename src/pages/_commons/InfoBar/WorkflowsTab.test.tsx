import { fireEvent } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';
import { WorkflowTemplateData } from 'app/fixtures/workflow-data';
import * as helper from 'app/helpers/isDevice';
import { mockUseDispatchFnc, renderComponent } from 'app/utils';
import 'app/utils/_mockComponent/mockSortAndOrder';
import { ComponentProps } from 'app/_libraries/_dls';
import { App as DOFApp } from 'app/_libraries/_dof/core';
import WorkflowsTab from 'pages/_commons/InfoBar/WorkflowsTab';
import React from 'react';
import * as WorkflowUseSelector from '../redux/Workflow/select-hooks';

const mockHelper: any = helper;
const mockUseDispatch = mockUseDispatchFnc();
const mockUseSelectFavoriteWorkflowTemplate = jest.spyOn(
  WorkflowUseSelector,
  'useSelectFavoriteWorkflowTemplate'
);
const mockUseSelectTemplateWorkFlows = jest.spyOn(
  WorkflowUseSelector,
  'useSelectTemplateWorkFlows'
);
const mockUseSelectTemplateWorkFlowsFilter = jest.spyOn(
  WorkflowUseSelector,
  'useSelectTemplateWorkFlowsFilter'
);
const mockUseSelectUnfavoriteWorkflowTemplate = jest.spyOn(
  WorkflowUseSelector,
  'useSelectUnfavoriteWorkflowTemplate'
);

jest.mock('app/components/SimpleSearchSmall', () => ({ onSearch }: any) => (
  <div id="SimpleSearchSmall" onClick={onSearch}></div>
));

jest.mock('react-router-dom', () => ({
  useHistory: () => ({
    push: jest.fn()
  })
}));

const mockProps = {
  id: 'WorkflowsTab',
  itemSelected: 'WorkflowsTab'
} as ComponentProps;

const testId = 'WorkflowsTab';
describe('Test WorkflowsTab', () => {
  const dispatchMock = jest.fn();
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => dispatchMock);
    mockUseSelectFavoriteWorkflowTemplate.mockImplementation(() => ({
      loading: false
    }));
    mockUseSelectTemplateWorkFlows.mockImplementation(() => ({
      workflows: WorkflowTemplateData as any,
      loading: false,
      error: false,
      alreadyLoad: true
    }));
    mockUseSelectTemplateWorkFlowsFilter.mockImplementation(() => ({
      searchValue: ''
    }));
    mockUseSelectUnfavoriteWorkflowTemplate.mockImplementation(() => ({
      loading: false
    }));
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
    mockUseSelectFavoriteWorkflowTemplate.mockClear();
    mockUseSelectTemplateWorkFlows.mockClear();
    mockUseSelectTemplateWorkFlowsFilter.mockClear();
    mockUseSelectUnfavoriteWorkflowTemplate.mockClear();
  });

  it('render loading', async () => {
    mockHelper.isDevice = true;
    mockUseSelectTemplateWorkFlows.mockImplementation(() => ({
      workflows: WorkflowTemplateData as any,
      loading: false,
      error: false,
      alreadyLoad: false
    }));
    await renderComponent(
      testId,
      <DOFApp urlConfigs={[]}>
        <WorkflowsTab {...({} as any)} />
      </DOFApp>
    );
    expect(dispatchMock).toHaveBeenCalled();
  });

  it('render to UI', async () => {
    jest.useFakeTimers();
    mockHelper.isDevice = false;
    const renderResult = await renderComponent(
      testId,
      <DOFApp urlConfigs={[]}>
        <WorkflowsTab {...mockProps} />
      </DOFApp>
    );
    const wrapper = await renderResult.findByTestId(testId);
    const expand = wrapper.querySelector('i.icon-plus')?.parentElement;
    fireEvent.click(expand!);
    jest.runAllTimers();

    expect(
      wrapper.querySelector('#workflow-template-card-view-inforbar-03208548')
    ).toBeInTheDocument();
  });

  it('render retry api', async () => {
    mockHelper.isDevice = false;
    mockUseSelectTemplateWorkFlows.mockImplementation(() => ({
      workflows: [],
      loading: false,
      error: true,
      alreadyLoad: true
    }));
    const renderResult = await renderComponent(
      testId,
      <DOFApp urlConfigs={[]}>
        <WorkflowsTab {...mockProps} />
      </DOFApp>
    );
    const wrapper = await renderResult.findByTestId(testId);
    const reloadButton = wrapper.querySelector(
      '#workflow-template-error_Button'
    );
    fireEvent.click(reloadButton!);

    expect(dispatchMock).toBeCalled();
  });

  it('should search correct', async () => {
    mockHelper.isDevice = false;
    mockUseSelectTemplateWorkFlowsFilter.mockImplementation(() => ({
      searchValue: 'search'
    }));
    const renderResult = await renderComponent(
      testId,
      <DOFApp urlConfigs={[]}>
        <WorkflowsTab {...mockProps} />
      </DOFApp>
    );
    const wrapper = await renderResult.findByTestId(testId);
    const search = wrapper.querySelector('#SimpleSearchSmall');
    fireEvent.click(search!);

    expect(dispatchMock).toBeCalled();
  });

  it('should render no data', async () => {
    mockHelper.isDevice = false;
    mockUseSelectTemplateWorkFlowsFilter.mockImplementation(() => ({
      searchValue: 'search'
    }));
    mockUseSelectTemplateWorkFlows.mockImplementation(() => ({
      workflows: WorkflowTemplateData as any,
      loading: false,
      error: false,
      alreadyLoad: true
    }));
    const renderResult = await renderComponent(
      testId,
      <DOFApp urlConfigs={[]}>
        <WorkflowsTab {...mockProps} />
      </DOFApp>
    );
    const wrapper = await renderResult.findByTestId(testId);
    const notFound = wrapper.querySelector('#workflow-tab-not-found');
    expect(notFound).toEqual(null);
  });

  it('should render clear search', async () => {
    mockHelper.isDevice = false;
    mockUseSelectTemplateWorkFlowsFilter.mockImplementation(() => ({
      searchValue: '12312321321'
    }));
    mockUseSelectTemplateWorkFlows.mockImplementation(() => ({
      workflows: WorkflowTemplateData as any,
      loading: false,
      error: false,
      alreadyLoad: false
    }));
    const renderResult = await renderComponent(
      testId,
      <DOFApp urlConfigs={[]}>
        <WorkflowsTab {...mockProps} />
      </DOFApp>
    );

    const clearBtn = renderResult.getByText('txt_clear_and_reset');
    userEvent.click(clearBtn);

    const bntEx = renderResult.getAllByRole('button')[2];
    userEvent.click(bntEx);

    const bntCo = renderResult.getAllByRole('button')[2];
    userEvent.click(bntCo);

    expect(dispatchMock).toBeCalled();
  });

  it('should go to all workflow', async () => {
    mockHelper.isDevice = false;
    const renderResult = await renderComponent(
      testId,
      <DOFApp urlConfigs={[]}>
        <WorkflowsTab {...mockProps} />
      </DOFApp>
    );
    const wrapper = await renderResult.findByTestId(testId);
    const popout = wrapper.querySelector('i.icon-popout')?.parentElement;
    fireEvent.click(popout!);

    expect(dispatchMock).toBeCalled();
  });
});
