import EnhanceDropdownList from 'app/components/EnhanceDropdownList';
import FieldTooltip from 'app/components/FieldTooltip';
import { isDevice } from 'app/helpers';
import { useFormValidations } from 'app/hooks';
import {
  Button,
  ComboBox,
  ComponentProps,
  DropdownBaseChangeEvent,
  Icon,
  Radio,
  SimpleBar,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import Input from 'app/_libraries/_dls/components/Radio/Input';
import Label from 'app/_libraries/_dls/components/Radio/Label';
import { isEmpty, orderBy } from 'app/_libraries/_dls/lodash';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { batch, useDispatch } from 'react-redux';
import {
  actionsWorkflowSetup,
  useSelectElementMetadataNonWorkflow,
  useSelectServicesData,
  useSelectTableListForWorkflowSetup
} from '../redux/WorkflowSetup';

export interface IFieldValidate {
  serviceCode: string;
  subjectCode: string;
  sectionCode: string;
  agentCode: string;
  prinCode: string;
  tableIdCode: string;
}

const optionAll = { code: 'All', text: 'All' };
const defaultOption = { code: '', text: '' };

const initformValues = {
  dmmSelected: '',
  tableTypeSelected: '',
  action: '',
  service: defaultOption,
  subject: defaultOption,
  section: defaultOption,
  system: defaultOption,
  prin: defaultOption,
  agent: defaultOption,
  tableId: defaultOption
};

const UtilitiesTab: React.FC<ComponentProps> = ({}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [formValues, setFormValues] = useState(initformValues);
  const [resetTouch, setResetTouch] = useState(0);

  const {
    systemPrincipalAgentsOptions,
    dmmTableTypeOptions,
    alpTableOptions,
    mlpTableOptions,
    tlpTableOptions
  } = useSelectElementMetadataNonWorkflow();

  const service = useSelectServicesData();
  const tableListItem = useSelectTableListForWorkflowSetup();

  const serviceCodeVal = formValues.service.code;
  const subjectCodeVal = formValues.subject.code;
  const sectionCodeVal = formValues.section.code;
  const systemCodeVal = formValues.system.code;
  const prinCodeVal = formValues.prin.code;
  const agentCodeVal = formValues.agent.code;
  const tableIdCodeVal = formValues.tableId.code;
  const tableTypeSelectedVal = formValues.tableTypeSelected;

  const arrSPA = useMemo(() => {
    const sysPrinAgents: any[] = [];
    for (let i = 0; i < systemPrincipalAgentsOptions.length; i++) {
      const item = { ...systemPrincipalAgentsOptions[i] };
      const sysPrinAgentSplit = item?.code?.split('-');
      if (sysPrinAgentSplit?.length !== 3) continue;

      const dataForm = {
        sys: sysPrinAgentSplit[0],
        prin: sysPrinAgentSplit[1],
        agent: sysPrinAgentSplit[2]
      };

      sysPrinAgents.push(dataForm);
    }

    return orderBy(sysPrinAgents, ['sys', 'prin', 'agent'], ['asc']);
  }, [systemPrincipalAgentsOptions]);

  const systemList = useMemo(() => {
    const uniSys: any = [];

    arrSPA.map(x => {
      if (uniSys.indexOf(x.sys) === -1) {
        uniSys.push(x.sys);
      }
    });

    const refSys = uniSys.map((x: any) => {
      return {
        code: x,
        text: x
      };
    });

    const OrdRefSys = orderBy(refSys, ['code'], ['asc']);
    OrdRefSys.unshift(optionAll);

    return OrdRefSys;
  }, [arrSPA]);

  const prinList = useMemo(() => {
    const prins = arrSPA.filter(x => x.sys === systemCodeVal);

    const uniPrin: any = [];
    prins.map(x => {
      if (uniPrin.indexOf(x.prin) === -1) {
        uniPrin.push(x.prin);
      }
    });

    const refPrin = uniPrin.map((x: any) => {
      return {
        code: x,
        text: x
      };
    });
    const OrdRefPrin = orderBy(refPrin, ['code'], ['asc']);
    OrdRefPrin.unshift(optionAll);

    return OrdRefPrin;
  }, [arrSPA, systemCodeVal]);

  const agentList = useMemo(() => {
    const agens = arrSPA.filter(
      x => x.sys === systemCodeVal && x.prin === prinCodeVal
    );

    const uniAgent: any = [];
    agens.map(x => {
      if (uniAgent.indexOf(x.agent) === -1) {
        uniAgent.push(x.agent);
      }
    });

    const refAgent = uniAgent.map((x: any) => {
      return {
        code: x,
        text: x
      };
    });
    const OrdRefAgent = orderBy(refAgent, ['code'], ['asc']);
    OrdRefAgent.unshift(optionAll);

    return OrdRefAgent;
  }, [arrSPA, systemCodeVal, prinCodeVal]);

  const serviceList = orderBy(service, ['name'], ['asc']).map(x => {
    return { code: x.name, text: x.name + ', ' + x.description };
  });

  const subjectList = useMemo(() => {
    const currentService = serviceCodeVal;
    const subjects =
      service.find(x => x.name === currentService)?.subjects || [];

    let data: RefData[] = [];
    data = subjects.map(y => {
      return { code: y.name, text: y.name + ', ' + y.description };
    });

    orderBy(data, ['code'], ['asc']);

    return data;
  }, [serviceCodeVal, service]);

  const sectionList = useMemo(() => {
    const currentService = serviceCodeVal;
    const currentSubject = subjectCodeVal;
    const subjects =
      service.find(x => x.name === currentService)?.subjects || [];

    const sections =
      subjects.find(x => x.name === currentSubject)?.sections || [];

    const data = sections.map(y => {
      return { code: y.name, text: y.name + ', ' + y.description };
    });
    const ordData = orderBy(data, ['code'], ['asc']);

    ordData.unshift(optionAll);

    return ordData;
  }, [subjectCodeVal, serviceCodeVal, service]);

  useEffect(() => {
    batch(() => {
      dispatch(
        actionsWorkflowSetup.getNonWorkflowMetadata([
          'system.principal.agents',
          'dmm.table.type',
          'alp.table',
          'mlp.table',
          'tlp.table'
        ])
      );
      dispatch(actionsWorkflowSetup.getServiceSubjectSectionOptions(''));
    });
  }, [dispatch]);

  const handleOnSelectOption = (action: string) => {
    if (formValues.action === action) return;

    setFormValues({
      ...formValues,
      action: action
    });
  };

  const handleOnSelectDmmAnalyzer = (action: string, item: string) => {
    if (formValues.dmmSelected === item) return;

    dispatch(actionsWorkflowSetup.resetDataTableList());

    switch (action) {
      case 'dmmSelected':
        setFormValues({
          ...formValues,
          [action]: item,
          tableTypeSelected: '',
          tableId: defaultOption
        });
        break;

      case 'tableTypeSelected':
        setFormValues({
          ...formValues,
          [action]: item,
          tableId: defaultOption
        });
        const TableType = `${formValues.dmmSelected}${item}`;

        dispatch(
          actionsWorkflowSetup.getTableListForWorkflowSetup(
            TableType?.toUpperCase()
          )
        );
        break;
    }
  };

  const handleOnBlur = (action?: string) => {
    switch (action) {
      case 'tableId':
      case 'section':
      case 'prin':
      case 'agent':
        const { code, text } = formValues[action];

        setFormValues({
          ...formValues,
          [action]: {
            code: code === undefined ? 'All' : code,
            text: text === undefined ? 'All' : text
          }
        });
        break;
    }
  };

  const hanldeOnchange =
    (action: string) => (event: DropdownBaseChangeEvent) => {
      const { text, code } = event?.target?.value || '';

      switch (action) {
        case 'service':
          if (formValues[action].code !== code)
            setFormValues({
              ...formValues,
              [action]: { code, text },
              subject: defaultOption,
              section: defaultOption,
              system: defaultOption,
              prin: defaultOption,
              agent: defaultOption
            });
          break;

        case 'subject':
          if (formValues[action].code !== code)
            setFormValues({
              ...formValues,
              [action]: { code, text },
              section: defaultOption
            });
          break;

        case 'system':
          if (formValues[action].code !== code)
            setFormValues({
              ...formValues,
              [action]: { code, text },
              prin: defaultOption,
              agent: defaultOption
            });
          break;

        case 'prin':
          if (formValues[action].code !== code)
            setFormValues({
              ...formValues,
              [action]: {
                code,
                text
              },
              agent: defaultOption
            });
          break;

        default:
          setFormValues({
            ...formValues,
            [action]: {
              code,
              text
            }
          });
          break;
      }
    };

  const isShowSysPriAge = useMemo(() => {
    const notExistSPAList = ['CP', 'PL', 'CC', 'DA', 'FP'];

    return !notExistSPAList.includes(serviceCodeVal) && serviceCodeVal !== '';
  }, [serviceCodeVal]);

  useEffect(() => {
    process.nextTick(() => {
      setResetTouch(t => t + 1);
    });
  }, [serviceCodeVal, systemCodeVal, prinCodeVal, tableTypeSelectedVal]);

  const currentErrors = useMemo(
    () =>
      ({
        serviceCode: !serviceCodeVal?.trim() && {
          status: true,
          message: t('txt_required_validation', {
            fieldName: t('txt_utilities_pcf_analyzer_service')
          })
        },
        subjectCode: !subjectCodeVal?.trim() && {
          status: true,
          message: t('txt_required_validation', {
            fieldName: t('txt_utilities_pcf_analyzer_subject')
          })
        },
        sectionCode: !sectionCodeVal?.trim() && {
          status: true,
          message: t('txt_required_validation', {
            fieldName: t('txt_utilities_pcf_analyzer_section')
          })
        },
        agentCode: !agentCodeVal?.trim() && {
          status: true,
          message: t('txt_required_validation', {
            fieldName: t('txt_utilities_pcf_analyzer_agen')
          })
        },
        prinCode: !prinCodeVal?.trim() && {
          status: true,
          message: t('txt_required_validation', {
            fieldName: t('txt_utilities_pcf_analyzer_prin')
          })
        },
        tableIdCode: !tableIdCodeVal?.trim() && {
          status: true,
          message: t('txt_required_validation', {
            fieldName: t('txt_utilities_dmm_analyzer_tableId')
          })
        }
      } as Record<keyof IFieldValidate, IFormError>),
    [
      serviceCodeVal,
      subjectCodeVal,
      sectionCodeVal,
      agentCodeVal,
      prinCodeVal,
      tableIdCodeVal,
      t
    ]
  );

  const [, errors, setTouched] = useFormValidations<keyof IFieldValidate>(
    currentErrors,
    resetTouch
  );

  const isValidFormPcfAnalyzer = useMemo(() => {
    let valid = false;

    if (
      isEmpty(systemCodeVal) ||
      systemCodeVal === undefined ||
      systemCodeVal === 'All'
    ) {
      valid =
        !errors.sectionCode &&
        !errors.serviceCode &&
        !errors.subjectCode &&
        serviceCodeVal !== '' &&
        subjectCodeVal !== '';
    } else {
      valid =
        !errors.agentCode &&
        !errors.prinCode &&
        !errors.sectionCode &&
        !errors.serviceCode &&
        !errors.subjectCode &&
        serviceCodeVal !== '' &&
        subjectCodeVal !== '';
    }

    return valid;
  }, [errors, serviceCodeVal, subjectCodeVal, systemCodeVal]);

  const isValidFormDmmAnalyzer = useMemo(() => {
    return (
      !errors.tableIdCode &&
      formValues.dmmSelected !== '' &&
      formValues.tableTypeSelected !== '' &&
      formValues.tableId.code !== ''
    );
  }, [formValues, errors]);

  const convertAllValues = (value: any) =>
    value?.code.toLocaleLowerCase() === 'all' ? `$${value.code}` : value.code;

  const handleValue = (value: RefData) => {
    return value.code !== '' ? value : undefined;
  };

  const handleValueCombobox = (value: RefData) => {
    return value.code === undefined || isEmpty(value.code)
      ? undefined
      : handleValue(value);
  };

  const handleExportAnalyzer = useCallback(() => {
    if (formValues.action === 'pcf') {
      dispatch(
        actionsWorkflowSetup.exportAnalyzer({
          parameters: [
            {
              name: 'analyzer.type',
              values: [formValues?.action.toUpperCase()]
            },
            {
              name: 'service',
              values: [convertAllValues(formValues.service)]
            },
            {
              name: 'subject',
              values: [convertAllValues(formValues.subject)]
            },
            {
              name: 'section',
              values: [convertAllValues(formValues.section)]
            },
            {
              name: 'system',
              values: [convertAllValues(formValues.system)]
            },
            {
              name: 'prin',
              values: [convertAllValues(formValues.prin)]
            },
            {
              name: 'agent',
              values: [convertAllValues(formValues.agent)]
            }
          ]
        })
      );
    }
    if (formValues.action === 'dmm') {
      dispatch(
        actionsWorkflowSetup.exportAnalyzer({
          parameters: [
            {
              name: 'analyzer.type',
              values: [formValues?.action.toUpperCase()]
            },
            {
              name: 'dmm.table.type',
              values: [formValues?.dmmSelected.toUpperCase()]
            },
            {
              name: 'table.type',
              values: [formValues?.tableTypeSelected.toUpperCase()]
            },
            {
              name: 'table.id',
              values: [convertAllValues(formValues.tableId)]
            }
          ]
        })
      );
    }
  }, [
    dispatch,
    formValues.action,
    formValues.agent,
    formValues?.dmmSelected,
    formValues.prin,
    formValues.section,
    formValues.service,
    formValues.subject,
    formValues.system,
    formValues.tableId,
    formValues?.tableTypeSelected
  ]);

  //set data for only 1 service option
  useEffect(() => {
    if (serviceList.length === 1 && formValues.service.code === '') {
      setFormValues({
        ...formValues,
        service: { code: serviceList[0].code, text: serviceList[0].text || '' }
      });
    }
  }, [serviceList, formValues]);

  //set data for only 1 subject option
  useEffect(() => {
    if (subjectList.length === 1 && formValues.subject.code === '') {
      setFormValues({
        ...formValues,
        subject: { code: subjectList[0].code, text: subjectList[0].text || '' }
      });
    }
  }, [formValues.service, formValues, subjectList]);

  //set data for only 1 section option
  useEffect(() => {
    if (formValues.section.code !== '' || sectionList.length < 2) return;

    if (sectionList.length === 2) {
      setFormValues({
        ...formValues,
        section: { code: sectionList[1].code, text: sectionList[1].text || '' }
      });
    } else {
      setFormValues({
        ...formValues,
        section: { code: sectionList[0].code, text: sectionList[0].text || '' }
      });
    }
  }, [formValues, sectionList]);

  //set data for only 1 tableId option
  useEffect(() => {
    if (formValues.tableId.code !== '' || isEmpty(tableListItem)) return;

    setFormValues({
      ...formValues,
      tableId: {
        code: tableListItem[0].code,
        text: tableListItem[0].text || ''
      }
    });
  }, [tableListItem, formValues]);

  useEffect(() => {
    if (formValues.prin.code !== '' || isEmpty(prinList)) return;
    if (
      systemCodeVal === 'All' ||
      systemCodeVal === undefined ||
      isEmpty(systemCodeVal)
    )
      return;

    setFormValues({
      ...formValues,
      prin: {
        code: prinList.length === 2 ? prinList[1].code : prinList[0].code,
        text: prinList.length === 2 ? prinList[1].text : prinList[0].text
      }
    });
  }, [prinList, formValues, systemCodeVal]);

  useEffect(() => {
    if (formValues.agent.code !== '' || isEmpty(agentList)) return;
    if (
      prinCodeVal === 'All' ||
      prinCodeVal === undefined ||
      isEmpty(prinCodeVal)
    )
      return;

    setFormValues({
      ...formValues,
      agent: {
        code: agentList.length === 2 ? agentList[1].code : agentList[0].code,
        text: agentList.length === 2 ? agentList[1].text : agentList[0].text
      }
    });
  }, [agentList, formValues, prinCodeVal]);

  const pcfContent = () => {
    return (
      <div>
        <hr className="my-24" />
        <h5>{t('txt_utilities_pcf_analyzer')}</h5>
        <p className="pt-8 color-grey">
          {t('txt_utilities_pcf_analyzer_title')}
        </p>
        <div className="pt-16">
          <EnhanceDropdownList
            popupBaseProps={{ popupBaseClassName: 'inside-infobar' }}
            required={true}
            label={t('txt_utilities_pcf_analyzer_service')}
            name="serviceCode"
            onFocus={setTouched('serviceCode')}
            onBlur={setTouched('serviceCode', true)}
            error={errors.serviceCode}
            options={serviceList}
            readOnly={serviceList.length === 1}
            onChange={hanldeOnchange('service')}
            value={() => handleValue(formValues.service)}
          />
        </div>
        <div className="mt-16">
          <EnhanceDropdownList
            popupBaseProps={{ popupBaseClassName: 'inside-infobar' }}
            required={true}
            label={t('txt_utilities_pcf_analyzer_subject')}
            name="subjectCode"
            onFocus={setTouched('subjectCode')}
            onBlur={setTouched('subjectCode', true)}
            error={errors.subjectCode}
            options={subjectList}
            onChange={hanldeOnchange('subject')}
            disabled={isEmpty(serviceCodeVal)}
            readOnly={subjectList.length === 1}
            value={() => handleValue(formValues.subject)}
          />
        </div>
        <div className="mt-16">
          <ComboBox
            label={t('txt_utilities_pcf_analyzer_section')}
            popupBaseProps={{ popupBaseClassName: 'inside-infobar' }}
            textField="text"
            onChange={hanldeOnchange('section')}
            noResult={t('txt_no_results_found_without_dot')}
            name="sectionCode"
            disabled={isEmpty(subjectCodeVal)}
            readOnly={sectionList.length === 2}
            value={() => handleValueCombobox(formValues.section)}
            onBlur={() => handleOnBlur('section')}
          >
            {sectionList.map((item: MagicKeyValue) => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        </div>
      </div>
    );
  };

  const dmmContent = () => {
    return (
      <div>
        <hr className="my-24" />
        <h5>{t('txt_utilities_dmm_analyzer')}</h5>
        <p className="pt-8 pb-16 color-grey">
          {t('txt_utilities_dmm_analyzer_titile')}
        </p>

        <div className="group-card">
          {dmmTableTypeOptions.map((x, key) => {
            return (
              <div
                key={key}
                className="group-card-item custom-control-root"
                onClick={() =>
                  handleOnSelectDmmAnalyzer(
                    'dmmSelected',
                    x.code.toLocaleLowerCase()
                  )
                }
              >
                <div className="row">
                  <div className="col-12 col-lg-10">
                    <p className="title">{x.textDmm}</p>
                  </div>
                  <div className="col-12 col-lg-2 mt-8 mt-lg-0">
                    <div className="group-control justify-content-lg-end">
                      <Radio className="ml-auto mr-n4">
                        <Radio.Input
                          checked={
                            formValues.dmmSelected ===
                            x.code.toLocaleLowerCase()
                          }
                          className="checked-style"
                        />
                      </Radio>
                    </div>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  };

  const isDmmAnalyzer = useMemo(() => {
    return formValues.action === 'dmm';
  }, [formValues.action]);

  const isPcfAnalyzer = useMemo(() => {
    return formValues.action === 'pcf';
  }, [formValues.action]);

  const SysPriAgeContent = () => {
    return (
      <div className=" pt-24">
        <div className="d-flex">
          <h5>
            {t('txt_utilities_pcf_analyzer_select_system_prin_agent_title')}
          </h5>
          <div className="ml-8">
            <Tooltip
              element={t(
                'txt_utilities_pcf_analyzer_select_system_prin_agent_title_tooltip'
              )}
              triggerClassName="d-flex "
            >
              <Icon name="information" className="color-grey-l16" size="5x" />
            </Tooltip>
          </div>
        </div>

        <div className="row mt-16">
          <div className="col-4">
            <FieldTooltip
              placement={isDevice ? 'right-start' : 'left-start'}
              element={
                <div>
                  <div className="text-uppercase">
                    {t(
                      'txt_utilities_pcf_analyzer_select_system_prin_agent_system_tooltip_1'
                    )}
                  </div>
                  <p className="mt-8">
                    {t(
                      'txt_utilities_pcf_analyzer_select_system_prin_agent_system_tooltip_2'
                    )}
                  </p>
                </div>
              }
            >
              <ComboBox
                label={t(
                  'txt_utilities_pcf_analyzer_select_system_prin_agent_system_placeholder'
                )}
                popupBaseProps={{ popupBaseClassName: 'inside-infobar' }}
                value={() => handleValueCombobox(formValues.system)}
                textField="text"
                onChange={hanldeOnchange('system')}
                noResult={t('txt_no_results_found_without_dot')}
                name="system"
              >
                {systemList?.map((item: MagicKeyValue) => (
                  <ComboBox.Item
                    key={item.code}
                    value={item}
                    label={item.text}
                  />
                ))}
              </ComboBox>
            </FieldTooltip>
          </div>
          <div className="col-4">
            <FieldTooltip
              placement={isDevice ? 'right-start' : 'left-start'}
              element={
                <div>
                  <div className="text-uppercase">
                    {t(
                      'txt_utilities_pcf_analyzer_select_system_prin_agent_prin_tooltip_1'
                    )}
                  </div>
                  <p className="mt-8">
                    {t(
                      'txt_utilities_pcf_analyzer_select_system_prin_agent_prin_tooltip_2'
                    )}
                  </p>
                </div>
              }
            >
              <ComboBox
                label={t(
                  'txt_utilities_pcf_analyzer_select_system_prin_agent_prin_placeholder'
                )}
                popupBaseProps={{ popupBaseClassName: 'inside-infobar' }}
                textField="text"
                onChange={hanldeOnchange('prin')}
                noResult={t('txt_no_results_found_without_dot')}
                name="prin"
                disabled={isEmpty(systemCodeVal) || systemCodeVal === 'All'}
                readOnly={prinList.length === 2}
                value={() => handleValueCombobox(formValues.prin)}
                onBlur={() => handleOnBlur('prin')}
              >
                {prinList.map((item: MagicKeyValue) => (
                  <ComboBox.Item
                    key={item.code}
                    value={item}
                    label={item.text}
                  />
                ))}
              </ComboBox>
            </FieldTooltip>
          </div>
          <div className="col-4">
            <FieldTooltip
              placement={isDevice ? 'right-start' : 'left-start'}
              element={
                <div>
                  <div className="text-uppercase">
                    {t(
                      'txt_utilities_pcf_analyzer_select_system_prin_agent_agent_tooltip_1'
                    )}
                  </div>
                  <p className="mt-8">
                    {t(
                      'txt_utilities_pcf_analyzer_select_system_prin_agent_agent_tooltip_2'
                    )}
                  </p>
                </div>
              }
            >
              <ComboBox
                label={t(
                  'txt_utilities_pcf_analyzer_select_system_prin_agent_agent_placeholder'
                )}
                popupBaseProps={{ popupBaseClassName: 'inside-infobar' }}
                textField="text"
                onChange={hanldeOnchange('agent')}
                noResult={t('txt_no_results_found_without_dot')}
                name="agent"
                disabled={
                  isEmpty(prinCodeVal) ||
                  prinCodeVal === 'All' ||
                  systemCodeVal === 'All'
                }
                readOnly={agentList.length === 2}
                value={() => handleValueCombobox(formValues.agent)}
                onBlur={() => handleOnBlur('agent')}
              >
                {agentList.map((item: MagicKeyValue) => (
                  <ComboBox.Item
                    key={item.code}
                    value={item}
                    label={item.text}
                  />
                ))}
              </ComboBox>
            </FieldTooltip>
          </div>
        </div>
      </div>
    );
  };

  const tableTypeAndID = () => {
    return (
      <div>
        <div className=" pt-24">
          <h5 className="mb-16">
            {t('txt_utilities_dmm_analyzer_table_type_title')}
          </h5>
          <>
            {formValues.dmmSelected === 'alp' &&
              alpTableOptions.map((x, index) => {
                return (
                  <div
                    className="mb-8"
                    key={`txt_utilities_dmm_analyzer_table_type_${index}`}
                  >
                    <Radio>
                      <Input
                        checked={
                          formValues.tableTypeSelected ===
                          x.code.toLocaleLowerCase()
                        }
                        id={`txt_utilities_dmm_analyzer_table_type_${index}`}
                        onChange={() =>
                          handleOnSelectDmmAnalyzer(
                            'tableTypeSelected',
                            x.code.toLocaleLowerCase()
                          )
                        }
                      />
                      <Label
                        htmlFor={`txt_utilities_dmm_analyzer_table_type_${index}`}
                      >
                        {x.textDmm}
                      </Label>
                    </Radio>
                  </div>
                );
              })}

            {formValues.dmmSelected === 'mlp' &&
              mlpTableOptions.map((x, index) => {
                return (
                  <div
                    className="mb-8"
                    key={`txt_utilities_dmm_analyzer_table_type_${index}`}
                  >
                    <Radio>
                      <Input
                        checked={
                          formValues.tableTypeSelected ===
                          x.code.toLocaleLowerCase()
                        }
                        id={`txt_utilities_dmm_analyzer_table_type_${index}`}
                        onChange={() =>
                          handleOnSelectDmmAnalyzer(
                            'tableTypeSelected',
                            x.code.toLocaleLowerCase()
                          )
                        }
                      />
                      <Label
                        htmlFor={`txt_utilities_dmm_analyzer_table_type_${index}`}
                      >
                        {x.textDmm}
                      </Label>
                    </Radio>
                  </div>
                );
              })}

            {formValues.dmmSelected === 'tlp' &&
              tlpTableOptions.map((x, index) => {
                return (
                  <div
                    className="mb-8"
                    key={`txt_utilities_dmm_analyzer_table_type_${index}`}
                  >
                    <Radio>
                      <Input
                        checked={
                          formValues.tableTypeSelected ===
                          x.code.toLocaleLowerCase()
                        }
                        id={`txt_utilities_dmm_analyzer_table_type_${index}`}
                        onChange={() =>
                          handleOnSelectDmmAnalyzer(
                            'tableTypeSelected',
                            x.code.toLocaleLowerCase()
                          )
                        }
                      />
                      <Label
                        htmlFor={`txt_utilities_dmm_analyzer_table_type_${index}`}
                      >
                        {x.textDmm}
                      </Label>
                    </Radio>
                  </div>
                );
              })}
          </>
        </div>

        {formValues.tableTypeSelected !== '' && (
          <div className=" pt-16">
            <div className="">
              <h5 className="">
                {t('txt_utilities_dmm_analyzer_table_id_title')}
              </h5>
              <div className="pt-16">
                <ComboBox
                  label={t('txt_utilities_dmm_analyzer_select_tableId')}
                  popupBaseProps={{ popupBaseClassName: 'inside-infobar' }}
                  textField="text"
                  onChange={hanldeOnchange('tableId')}
                  noResult={t('txt_no_results_found_without_dot')}
                  name="tableId"
                  value={() => handleValueCombobox(formValues.tableId)}
                  readOnly={tableListItem.length === 1}
                  onBlur={() => handleOnBlur('tableId')}
                >
                  {tableListItem.map((item: MagicKeyValue) => (
                    <ComboBox.Item
                      key={item.code}
                      value={item}
                      label={item.text}
                    />
                  ))}
                </ComboBox>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  };

  return (
    <SimpleBar>
      <div className="p-24">
        <div className="d-flex align-items-center justify-content-between mb-24">
          <h4>{t('txt_utilities')}</h4>
        </div>
        <h5 className="pb-16">{t('txt_utilities_select_the_option')}</h5>
        {/* ACTION */}
        <div className="group-card">
          <div
            className="group-card-item custom-control-root"
            onClick={() => handleOnSelectOption('dmm')}
          >
            <div className="row">
              <div className="col-12 col-lg-8">
                <p className="title">{t('txt_utilities_dmm_analyzer')}</p>
              </div>
              <div className="col-12 col-lg-4 mt-8 mt-lg-0">
                <div className="group-control justify-content-lg-end">
                  <Radio className="ml-auto mr-n4">
                    <Radio.Input
                      checked={formValues.action === 'dmm'}
                      className="checked-style"
                    />
                  </Radio>
                </div>
              </div>
            </div>
          </div>
          <div
            className="group-card-item custom-control-root"
            onClick={() => handleOnSelectOption('pcf')}
          >
            <div className="row">
              <div className="col-12 col-lg-8">
                <p className="title">{t('txt_utilities_pcf_analyzer')}</p>
              </div>
              <div className="col-12 col-lg-4 mt-8 mt-lg-0">
                <div className="group-control justify-content-lg-end">
                  <Radio className="ml-auto mr-n4">
                    <Radio.Input
                      checked={formValues.action === 'pcf'}
                      className="checked-style"
                    />
                  </Radio>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* CONTENT PCF*/}
        {isPcfAnalyzer && pcfContent()}
        {/* Sys/Prin/Agent */}
        {isPcfAnalyzer && isShowSysPriAge && SysPriAgeContent()}

        {/* CONTENT DMM */}
        {isDmmAnalyzer && dmmContent()}
        {/* TABLE TYPE */}
        {isDmmAnalyzer && formValues.dmmSelected !== '' && tableTypeAndID()}

        {isDmmAnalyzer || isPcfAnalyzer ? (
          <div className="d-flex justify-content-between pt-24">
            <Button
              variant="primary"
              className="ml-auto"
              onClick={handleExportAnalyzer}
              disabled={
                (isDmmAnalyzer && !isValidFormDmmAnalyzer) ||
                (isPcfAnalyzer && !isValidFormPcfAnalyzer)
              }
            >
              {t('txt_utilities_export_button')}
            </Button>
          </div>
        ) : (
          <div className="d-flex justify-content-between pt-24">
            <Button variant="primary" className="ml-auto" disabled={true}>
              {t('txt_utilities_export_button')}
            </Button>
          </div>
        )}
      </div>
    </SimpleBar>
  );
};

export default UtilitiesTab;
