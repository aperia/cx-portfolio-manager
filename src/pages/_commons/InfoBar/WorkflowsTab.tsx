import FailedApiReload from 'app/components/FailedApiReload';
import NoDataFound from 'app/components/NoDataFound';
import SimpleSearchSmall from 'app/components/SimpleSearchSmall';
import WorkflowTemplateCardView from 'app/components/WorkflowTemplateCardView';
import { WorkflowSection } from 'app/constants/enums';
import { WORKFLOWS_URL } from 'app/constants/links';
import { camelToText, classnames, isDevice } from 'app/helpers';
import {
  Button,
  ComponentProps,
  Icon,
  SimpleBar,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import {
  actionsWorkflow,
  useSelectFavoriteWorkflowTemplate,
  useSelectTemplateWorkFlows,
  useSelectTemplateWorkFlowsFilter,
  useSelectUnfavoriteWorkflowTemplate
} from '../redux/Workflow';

const WorkflowsTab: React.FC<ComponentProps> = ({ id, itemSelected }) => {
  const { t } = useTranslation();
  const history = useHistory();
  const dispatch = useDispatch();

  const [expandIds, setExpandIds] = useState<string[]>([]);
  const { workflows, loading, error, alreadyLoad } =
    useSelectTemplateWorkFlows();
  const { searchValue } = useSelectTemplateWorkFlowsFilter();

  const groupWorkflows = useMemo(
    () =>
      workflows?.reduce((pre, curr) => {
        if (!pre[curr.type]) {
          pre[curr.type] = [];
        }
        pre[curr.type].push(curr);
        return pre;
      }, {} as Record<string, IWorkflowTemplateModel[]>) || {},
    [workflows]
  );

  useEffect(() => {
    if (!alreadyLoad && id === itemSelected) {
      dispatch(actionsWorkflow.getTemplateWorkflows());
    }
  }, [dispatch, id, itemSelected, alreadyLoad]);

  useEffect(() => {
    if (itemSelected) return;

    setExpandIds([]);
    dispatch(actionsWorkflow.clearAndResetInfoBarWorkflowsFilter());
  }, [dispatch, itemSelected]);

  useEffect(() => {
    if (!searchValue) return;

    setExpandIds(Object.keys(groupWorkflows));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [groupWorkflows]);

  const handleSearch = (value: string) => {
    dispatch(
      actionsWorkflow.updateInfoBarWorkflowsFilter({ searchValue: value })
    );
  };

  const toggleExpaned = (id: string) => {
    const onClick = () => {
      setExpandIds(exIds => {
        const newIds = exIds?.filter(_id => _id !== id);
        const isExisted = exIds?.includes(id);

        return isExisted ? newIds : [...newIds, id];
      });
    };

    return onClick;
  };

  const handleApiReload = () => {
    dispatch(actionsWorkflow.getTemplateWorkflows());
  };

  const handleClearAndReset = useCallback(() => {
    setExpandIds([]);
    dispatch(actionsWorkflow.clearAndResetInfoBarWorkflowsFilter());
  }, [dispatch]);

  const goToWorkflowsPage = useCallback(() => {
    history.push(WORKFLOWS_URL);
    dispatch(
      actionsWorkflow.updatePageWorkflowsFilter({
        currentSection: WorkflowSection.ALL
      })
    );
  }, [history, dispatch]);

  const clearAndResetElement = useMemo(
    () => (
      <Button
        id="workflowTab_clearAndResetNotFound"
        variant="outline-primary"
        size="sm"
        onClick={handleClearAndReset}
      >
        {t('txt_clear_and_reset')}
      </Button>
    ),
    [t, handleClearAndReset]
  );

  return (
    <SimpleBar>
      <div
        className={classnames('p-24', {
          'h-body loading': loading
        })}
      >
        {!loading && (
          <div className="d-flex align-items-center justify-content-between mb-16">
            <h4>{t('txt_workflows')}</h4>
            <div>
              {(workflows.length > 0 || searchValue) && (
                <SimpleSearchSmall
                  insideInfobar
                  placeholder={t(
                    'txt_type_to_search_by_workflow_name_and_desc'
                  )}
                  maxLength={200}
                  onSearch={handleSearch}
                  defaultValue={searchValue}
                />
              )}
              <Tooltip
                element={t('txt_view_all_workflows')}
                variant="primary"
                opened={isDevice ? false : undefined}
              >
                <Button
                  className="mr-n4"
                  variant="icon-secondary"
                  size="lg"
                  onClick={goToWorkflowsPage}
                >
                  <Icon name="popout" />
                </Button>
              </Tooltip>
            </div>
          </div>
        )}
        {searchValue && workflows.length > 0 && (
          <div className="mt-16 mr-n8 d-flex justify-content-end">
            {clearAndResetElement}
          </div>
        )}
        <div>
          {!loading &&
            !error &&
            Object.keys(groupWorkflows).map((k, idx) => {
              const items = groupWorkflows[k];

              return (
                <div
                  key={k}
                  className={classnames('mb-16 pt-16', {
                    'border-top': idx !== 0
                  })}
                >
                  <div className="d-flex align-items-center justify-content-between">
                    <span className="d-flex align-items-center">
                      <Tooltip
                        element={
                          expandIds?.includes(k)
                            ? t('txt_collapse')
                            : t('txt_expand')
                        }
                        variant="primary"
                        opened={isDevice ? false : undefined}
                      >
                        <Button
                          variant="icon-secondary"
                          size="sm"
                          onClick={toggleExpaned(k)}
                        >
                          <Icon
                            name={expandIds?.includes(k) ? 'minus' : 'plus'}
                          />
                        </Button>
                      </Tooltip>
                      <span className="pl-8 fs-14 fw-500">
                        {camelToText(k)}
                      </span>
                    </span>
                    <span className="fs-14 fw-500">{items.length}</span>
                  </div>
                  {expandIds?.includes(k) && <LazyRenderDof items={items} />}
                </div>
              );
            })}
        </div>
      </div>
      {!error && !loading && workflows.length === 0 && (
        <div className="d-flex flex-column justify-content-center mt-40 mb-32">
          <NoDataFound
            id="workflow-tab-not-found"
            title={t('txt_no_workflows_to_display')}
            hasSearch={!!searchValue}
            linkTitle={searchValue && t('txt_clear_and_reset')}
            onLinkClicked={handleClearAndReset}
          />
        </div>
      )}
      {error && (
        <FailedApiReload
          id="workflow-template-error"
          onReload={handleApiReload}
          className="mt-40 pt-40 d-flex flex-column justify-content-center align-items-center"
        />
      )}
    </SimpleBar>
  );
};

interface ILazyRenderDofProps {
  items: IWorkflowTemplateModel[];
}
const LazyRenderDof: React.FC<ILazyRenderDofProps> = ({ items }) => {
  const [renderItems, setRenderItems] = useState<IWorkflowTemplateModel[]>([]);
  const { loading: favoriting } = useSelectFavoriteWorkflowTemplate();
  const { loading: unfavoriting } = useSelectUnfavoriteWorkflowTemplate();
  const hide = useMemo(
    () => renderItems.length < items.length,
    [renderItems.length, items.length]
  );

  useEffect(() => {
    setRenderItems(_items => (_items.length !== items.length ? [] : items));
  }, [items]);

  useEffect(() => {
    if (renderItems.length === items.length) return;
    process.nextTick(() =>
      setRenderItems(items.slice(0, renderItems.length + 10))
    );
  }, [items, renderItems.length]);

  return (
    <React.Fragment>
      {(hide || favoriting || unfavoriting) && (
        <div className="my-24 py-24 loading loading-sm" />
      )}
      <div
        className={classnames({
          'mt-16': true,
          'd-none': hide || favoriting || unfavoriting
        })}
      >
        {renderItems.map(i => (
          <div key={i.id} className="mt-4">
            <WorkflowTemplateCardView
              id={`inforbar-${i.id}`}
              value={i}
              isStatusGroup
            />
          </div>
        ))}
      </div>
    </React.Fragment>
  );
};

export default WorkflowsTab;
