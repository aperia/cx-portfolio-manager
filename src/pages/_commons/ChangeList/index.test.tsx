import { fireEvent } from '@testing-library/react';
import {
  ChangeList as ChangeListData,
  ChangeMapping
} from 'app/fixtures/change-data';
import { mappingDataFromArray } from 'app/helpers';
import {
  mockUseDispatchFnc,
  mockUseModalRegistryFnc,
  renderComponent
} from 'app/utils';
import 'app/utils/_mockComponent/mockSortAndOrder';
import * as getElementAttribute from 'app/_libraries/_dls/utils/getElementAttribute';
import { App as DOFApp } from 'app/_libraries/_dof/core';
import ChangeList, { IChangeListProps } from 'pages/_commons/ChangeList';
import * as ChangeSelectHooks from 'pages/_commons/redux/Change/select-hooks';
import * as ChommonSelectHooks from 'pages/_commons/redux/Common/select-hooks';
import React from 'react';

const mockHistory = {
  push: jest.fn()
};
jest.mock('react-router-dom', () => ({
  useHistory: () => mockHistory,
  useLocation: () => ({}),
  useParams: () => ({})
}));

jest.mock('resize-observer-polyfill', () =>
  jest.requireActual('app/_libraries/_dls/test-utils/mocks/MockResizeObserver')
);
jest.mock('app/_libraries/_dls/utils/canvasTextWidth.ts');

beforeEach(() => {
  jest
    .spyOn(getElementAttribute, 'getAvailableWidth')
    .mockImplementation(() => 1000);
});

const mockUseDispatch = mockUseDispatchFnc();
const mockUseModalRegistry = mockUseModalRegistryFnc();

const mockUseSelectChangesManagement = jest.spyOn(
  ChangeSelectHooks,
  'useSelectChangesManagement'
);
const mockUseSelectChangeManagementFilter = jest.spyOn(
  ChangeSelectHooks,
  'useSelectChangeManagementFilter'
);
const mockUseSelectWindowDimension = jest.spyOn(
  ChommonSelectHooks,
  'useSelectWindowDimension'
);

const mockChangesData = mappingDataFromArray(
  [...ChangeListData],
  ChangeMapping
);
const mockChange = { changes: [], loading: false, error: false, totalItem: 0 };
const mockFilter = {
  searchValue: '',
  page: 1,
  pageSize: 10,
  sortBy: ['effectiveDate', 'name'],
  orderBy: ['desc', 'asc'],
  changeStatus: 'All',
  filterField: '',
  filterValue: ''
};

jest.mock('resize-observer-polyfill', () => {
  class ResizeObserver {
    _cb: Function;
    constructor(callback: Function) {
      this._cb = callback;
    }

    observe = () => {
      this._cb();
    };
    disconnect = () => {};
  }
  return ResizeObserver;
});

jest.mock('./CustomFilter', () => {
  return {
    __esModule: true,
    default: ({
      onFilterByChangeStatus,
      onFilterByChange,
      onFilterValueChange,
      onFilterByType
    }: any) => {
      return (
        <div>
          <div className="change-filter-field" onClick={onFilterByChange} />
          <div className="change-filter-value" onClick={onFilterValueChange} />
          <div className="change-filter-by-type" onClick={onFilterByType} />
          <div
            className="change-filter-status"
            onClick={onFilterByChangeStatus}
          />
        </div>
      );
    }
  };
});

const mockProps: IChangeListProps = {};

const testId = 'changeList';
describe('ChangeList', () => {
  const mockDispatch = jest.fn();
  let wrapper: HTMLElement;

  const createWrapper = async (...props: IChangeListProps[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    const renderResult = await renderComponent(
      testId,
      <DOFApp urlConfigs={[]}>
        <ChangeList {...mergeProps} />
      </DOFApp>
    );
    wrapper = await renderResult.findByTestId(testId);
    const container = wrapper.querySelector('.pagination-wrapper')
      ?.parentElement as HTMLDivElement;

    if (!container) return;
    jest.spyOn(container, 'clientWidth', 'get').mockImplementation(() => 1024);
  };

  const mockUseSelectImplement = ({ change, filter }: any) => {
    mockUseSelectChangesManagement.mockImplementation(() => change);
    mockUseSelectChangeManagementFilter.mockImplementation(() => filter);
    mockUseSelectWindowDimension.mockImplementation(() => ({
      width: 1920,
      height: 1080
    }));
  };

  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
    mockUseModalRegistry.mockImplementation(() => [true, false]);
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
    mockUseModalRegistry.mockClear();
    mockUseSelectChangesManagement.mockClear();
    mockUseSelectChangeManagementFilter.mockClear();
    mockUseSelectWindowDimension.mockClear();
  });

  const it_should_render_numof_rows = (value: number) => {
    expect(wrapper.querySelectorAll('.list-view').length).toEqual(value);
  };
  const it_should_render_list_view = () => {
    expect(wrapper.querySelector('.masonry-column')).not.toBeTruthy();
  };

  const it_should_navigate_to = (to: string) => {
    expect(mockHistory.push).toBeCalledWith(to);
  };
  const it_should_dispatch_action = () => {
    expect(mockDispatch).toHaveBeenCalled();
  };

  const then_click_an_item = () => {
    const firstItem = wrapper.querySelector('.list-view');
    fireEvent.click(firstItem!);
  };
  const then_change_to_page = (page: number) => {
    const element = wrapper.querySelector(
      `.pagination .page-item:nth-child(${page}) > .page-link`
    );
    fireEvent.click(element!);
  };
  const then_change_page_size = (pageNo: number) => {
    const element = wrapper.querySelector(
      '.pagination-page-size .text-field-container'
    );
    fireEvent.focus(element!);

    const pageElement = document.querySelector(
      `.dls-popup .item:nth-child(${pageNo})`
    );
    fireEvent.click(pageElement!);
  };

  const then_change_sort_by = () => {
    const element = wrapper.querySelector(`.change-sort-by`);
    fireEvent.click(element!);
  };
  const then_change_order_by = () => {
    const element = wrapper.querySelector(`.change-order-by`);
    fireEvent.click(element!);
  };
  const then_change_filter_status = () => {
    const element = wrapper.querySelector(`.change-filter-status`);
    fireEvent.click(element!);
  };
  const then_change_filter_field = () => {
    const element = wrapper.querySelector(`.change-filter-field`);
    fireEvent.click(element!);
  };
  const then_change_filter_value = () => {
    const element = wrapper.querySelector(`.change-filter-value`);
    fireEvent.click(element!);
  };
  const then_change_filter_type = () => {
    const element = wrapper.querySelector(`.change-filter-by-type`);
    fireEvent.click(element!);
  };
  const then_click_search = () => {
    const element = wrapper.querySelectorAll('.icon-simple-search div').item(1);
    fireEvent.click(element);
  };
  const then_click_clear_and_reset = () => {
    const element = wrapper.querySelector(
      '#change-management-NoDataFound__btn'
    );
    fireEvent.click(element!);
  };
  const then_click_reload_fail_api = () => {
    const element = wrapper.querySelector('#change-management-error_Button');
    fireEvent.click(element!);
  };
  const then_click_breadcrum_homePage = () => {
    const element = wrapper.querySelector('.breadcrumb-item-text a');
    fireEvent.click(element!);
  };

  it('should show empty changes', async () => {
    mockUseSelectImplement({
      change: mockChange,
      filter: mockFilter
    });
    await createWrapper(mockProps, { maxDataNumber: 10 });

    it_should_render_numof_rows(0);
    it_should_render_list_view();
  });

  it('should show error API call fail', async () => {
    mockUseSelectImplement({
      change: { ...mockChange, error: true },
      filter: mockFilter
    });
    await createWrapper(mockProps);

    it_should_render_numof_rows(0);

    then_click_reload_fail_api();
    it_should_dispatch_action();
  });

  it('should render within daterange fitlter', async () => {
    mockUseSelectImplement({
      change: mockChange,
      filter: {
        ...mockFilter,
        filterValue: { start: 'startDate', end: 'endDate' },
        searchValue: 'not found'
      }
    });
    await createWrapper(mockProps);

    it_should_render_numof_rows(0);

    then_click_clear_and_reset();
    it_should_dispatch_action();

    then_click_breadcrum_homePage();
    it_should_navigate_to('/');
  });

  it('should show items of changes', async () => {
    mockUseSelectImplement({
      change: {
        ...mockChange,
        changes: mockChangesData,
        totalItem: 50
      },
      filter: mockFilter
    });
    await createWrapper(mockProps);

    it_should_render_numof_rows(3);
    it_should_render_list_view();

    then_change_to_page(3);
    it_should_dispatch_action();

    then_change_page_size(3);
    it_should_dispatch_action();

    then_change_sort_by();
    it_should_dispatch_action();

    then_change_order_by();
    it_should_dispatch_action();

    then_change_filter_field();
    it_should_dispatch_action();

    then_change_filter_value();
    it_should_dispatch_action();

    then_change_filter_type();
    it_should_dispatch_action();

    then_change_filter_status();
    it_should_dispatch_action();

    then_click_search();
    it_should_dispatch_action();

    then_click_an_item();

    it_should_navigate_to(
      `/change-management/detail/${mockChangesData[0].changeId}`
    );
  });
});
