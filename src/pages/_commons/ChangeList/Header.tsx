import BreadCrumb from 'app/components/BreadCrumb';
import SimpleSearch from 'app/components/SimpleSearch';
import { BASE_URL } from 'app/constants/links';
import { useTranslation } from 'app/_libraries/_dls';
import isFunction from 'lodash.isfunction';
import React, { Fragment, useCallback, useEffect, useRef } from 'react';
import { useHistory } from 'react-router-dom';

interface IChangeHeader {
  isListView?: boolean;
  hideBreadCrumb?: boolean;
  title?: string;
  hideViewMode?: boolean;
  hideFilter?: boolean;
  searchValue?: string;
  onChangeListView?: (isListView: boolean) => void;
  onSearch?: (val: string) => void;
}

const ChangeHeader: React.FC<IChangeHeader> = ({
  isListView,
  hideBreadCrumb,
  title,
  hideViewMode,
  hideFilter,
  searchValue,
  onChangeListView,
  onSearch
}) => {
  const { t } = useTranslation();

  const simpleSearchRef = useRef<any>(null);

  const handleSearch = useCallback(
    (val: string) => {
      isFunction(onSearch) && onSearch(val);
    },
    [onSearch]
  );

  useEffect(() => {
    if (!searchValue && simpleSearchRef?.current) {
      simpleSearchRef.current.clear();
    }
  }, [searchValue]);

  return (
    <Fragment>
      {!hideBreadCrumb && (
        <div className="mb-16">
          <div className="section">
            <Breadcrumbs />
          </div>
        </div>
      )}
      <div className="d-flex align-items-center">
        <h4>{title}</h4>
        {!hideFilter && (
          <div className="ml-auto d-flex ">
            <SimpleSearch
              ref={simpleSearchRef}
              placeholder={t('txt_type_to_search')}
              onSearch={handleSearch}
              popperElement={
                <>
                  <p className="color-grey">{t('txt_search_description')}</p>
                  <ul className="search-field-item list-unstyled">
                    <li className="mt-16">ID</li>
                    <li className="mt-16">Name</li>
                    <li className="mt-16">Owner</li>
                    <li className="mt-16">Type</li>
                  </ul>
                </>
              }
            />
          </div>
        )}
      </div>
    </Fragment>
  );
};

const Breadcrumbs: React.FC = () => {
  const history = useHistory();

  const handleClickLink = useCallback(
    (url: string) => {
      const onClick = (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
        e.preventDefault();

        history.push(url);
      };

      return onClick;
    },
    [history]
  );

  const breadCrmbItems = [
    {
      id: 'home',
      title: (
        <a href={BASE_URL} onClick={handleClickLink(BASE_URL)}>
          Home
        </a>
      )
    },
    { id: 'changeManagement', title: 'Change Management' }
  ];

  return (
    <BreadCrumb
      id={'change-management__BreadCrumb'}
      items={breadCrmbItems}
      width={992}
    />
  );
};

export default ChangeHeader;
