import ModalRegistry from 'app/components/ModalRegistry';
import {
  CHANGE_FILTER_BY,
  CHANGE_FILTER_VALUES,
  CHANGE_STATUS,
  CHANGE_TYPE
} from 'app/constants/constants';
import { formatTime, isDevice, textToCamel } from 'app/helpers';
import {
  DateRangePicker,
  DateRangePickerValue,
  DropdownBaseChangeEvent,
  DropdownList,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import includes from 'lodash.includes';
import isFunction from 'lodash.isfunction';
import React, { useCallback, useMemo, useState } from 'react';

export interface ITimeFilter {
  description: string;
  value: DateRangePickerValue;
}
export interface IChangeFilter {
  changeTypeList: string[];
  changeStatusList: string[];
  changeStatusFilter?: string;
  filterBy?: string;
  timeFilterText?: string;
  filterType?: string;
  onFilterByChangeStatus?: (value: string) => void;
  onFilterByChange?: (value: string) => void;
  onFilterValueChange?: (value: ITimeFilter) => void;
  onFilterByType?: (value: string) => void;
}

const ChangeFilter: React.FC<IChangeFilter> = ({
  changeTypeList,
  changeStatusList,
  changeStatusFilter,
  filterBy,
  timeFilterText = 'All',
  filterType,
  onFilterByChangeStatus,
  onFilterByChange,
  onFilterValueChange,
  onFilterByType
}) => {
  const { t } = useTranslation();
  const [openDateRangePicker, setOpenDateRangePicker] =
    useState<boolean>(false);
  const [datePickerValue, setDatePickerValue] = useState<any>(null);
  const isValid =
    datePickerValue && datePickerValue.start && datePickerValue.end;

  const isCustomValue = timeFilterText.includes('-');

  const handleFilterByChangeStatus = useCallback(
    (e: DropdownBaseChangeEvent) => {
      isFunction(onFilterByChangeStatus) &&
        onFilterByChangeStatus(e.target.value);
    },
    [onFilterByChangeStatus]
  );

  const handleFilterByChange = useCallback(
    (e: DropdownBaseChangeEvent) => {
      isFunction(onFilterByChange) && onFilterByChange(e.target.value.value);
    },
    [onFilterByChange]
  );

  const handleDateRangeChange = useCallback(
    (description: string, dateRangeValue: DateRangePickerValue) => {
      const { start, end } = dateRangeValue;
      isFunction(onFilterValueChange) &&
        onFilterValueChange({
          description,
          value: {
            start: start && new Date(Date.parse(start.toDateString())),
            end: end && new Date(Date.parse(end.toDateString()))
          }
        });
    },
    [onFilterValueChange]
  );

  const handleFilterValueChange = useCallback(
    (e: DropdownBaseChangeEvent) => {
      const value = e.target.value;
      if (value === 'Custom') {
        setOpenDateRangePicker(true);
      } else {
        const dateRangValue: DateRangePickerValue = {};
        switch (value) {
          case 'Today':
            dateRangValue.start = new Date();
            dateRangValue.end = new Date();
            break;
          case 'Last 7 days':
            const last7 = new Date();
            last7.setDate(last7.getDate() - 7);
            dateRangValue.start = last7;
            dateRangValue.end = new Date();
            break;
          case 'Last 30 days':
            const last30 = new Date();
            last30.setDate(last30.getDate() - 30);
            dateRangValue.start = last30;
            dateRangValue.end = new Date();
            break;
        }
        handleDateRangeChange(value, dateRangValue);
      }
    },
    [handleDateRangeChange]
  );

  const handleFilterByType = useCallback(
    (e: DropdownBaseChangeEvent) => {
      isFunction(onFilterByType) && onFilterByType(e.target.value);
    },
    [onFilterByType]
  );

  const handleCloseDateRangePicker = () => {
    !isValid && setDatePickerValue(null);
    setOpenDateRangePicker(false);
  };

  const changeTypeListDynamic = useMemo(() => {
    const list = CHANGE_TYPE.filter(i =>
      includes(changeTypeList, textToCamel(i)?.toLowerCase())
    );
    return ['All', ...list];
  }, [changeTypeList]);

  const changeStatusListDynamic = useMemo(() => {
    const list = CHANGE_STATUS.filter(i =>
      includes(changeStatusList, textToCamel(i)?.toLowerCase())
    );
    return ['All', ...list];
  }, [changeStatusList]);

  return (
    <div className="d-flex flex-wrap">
      <div className="d-flex align-items-center pr-24 mt-12">
        <strong className="fs-14 color-grey mr-4">{t('txt_filter_by')}:</strong>
        <DropdownList
          name="filterBy"
          textField="description"
          value={CHANGE_FILTER_BY.find(item => item.value === filterBy)}
          onChange={handleFilterByChange}
          variant="no-border"
        >
          {CHANGE_FILTER_BY.map((item, idx) => (
            <DropdownList.Item
              key={idx}
              label={item.description}
              value={item}
            />
          ))}
        </DropdownList>
      </div>
      <div className="d-flex align-items-center pr-24 mt-12">
        <strong className="fs-14 color-grey mr-4">{t('txt_time')}:</strong>
        <DropdownList
          name="time"
          value={timeFilterText}
          onChange={handleFilterValueChange}
          variant="no-border"
        >
          <DropdownList.Group>
            {CHANGE_FILTER_VALUES.map((item, idx) => (
              <DropdownList.Item key={idx} label={item} value={item} />
            ))}
          </DropdownList.Group>
          <DropdownList.Group>
            <DropdownList.Item
              key="custom"
              label="Custom"
              value="Custom"
              className={isCustomValue ? 'fw-500' : ''}
            />
          </DropdownList.Group>
        </DropdownList>
      </div>
      <div className="d-flex align-items-center pr-24 mt-12">
        <strong className="fs-14 color-grey mr-4">{t('txt_type')}:</strong>
        <DropdownList
          name="change-type"
          value={filterType}
          textField="description"
          onChange={handleFilterByType}
          variant="no-border"
        >
          {changeTypeListDynamic.map((item, idx) => (
            <DropdownList.Item key={idx} label={item} value={item} />
          ))}
        </DropdownList>
      </div>
      <div className="d-flex align-items-center pr-24 mt-12">
        <strong className="fs-14 color-grey mr-4">{t('txt_status')}:</strong>
        <DropdownList
          name="status"
          value={changeStatusFilter}
          textField="description"
          onChange={handleFilterByChangeStatus}
          variant="no-border"
        >
          {changeStatusListDynamic.map((item, idx) => (
            <DropdownList.Item key={idx} label={item} value={item} />
          ))}
        </DropdownList>
      </div>
      <ModalRegistry id={'date-range-picker'} show={openDateRangePicker}>
        <ModalHeader border closeButton onHide={handleCloseDateRangePicker}>
          <ModalTitle>{t('txt_filter_by_date')}</ModalTitle>
        </ModalHeader>
        <ModalBody>
          <DateRangePicker
            required
            label={
              (
                CHANGE_FILTER_BY.find(item => item.value === filterBy) ||
                CHANGE_FILTER_BY[0]
              ).description
            }
            value={datePickerValue}
            onChange={e => {
              setDatePickerValue(e.target.value);
            }}
            canInput={!isDevice}
          />
        </ModalBody>
        <ModalFooter
          cancelButtonText={t('txt_cancel')}
          okButtonText={t('txt_apply')}
          onCancel={handleCloseDateRangePicker}
          onOk={() => {
            const { start, end } = datePickerValue;
            const startStr = formatTime(start.toDateString()).date;
            const endStr = formatTime(end.toDateString()).date;
            const description = startStr + ' - ' + endStr;
            handleDateRangeChange(description, datePickerValue);
            setOpenDateRangePicker(false);
          }}
          disabledOk={!isValid}
        />
      </ModalRegistry>
    </div>
  );
};

export default ChangeFilter;
