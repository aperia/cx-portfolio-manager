import ChangeCardView from 'app/components/ChangeCardView';
import FailedApiReload from 'app/components/FailedApiReload';
import NoDataFound from 'app/components/NoDataFound';
import {
  CHANGE_FILTER_BY,
  CHANGE_MANAGEMENT_SORT_BY_LIST
} from 'app/constants/constants';
import { changeDetailLink } from 'app/constants/links';
import { classnames } from 'app/helpers';
import { SimpleBar, useTranslation } from 'app/_libraries/_dls';
import {
  actionsChange,
  useSelectChangeManagementFilter,
  useSelectChangesManagement,
  useSelectExpandingChange
} from 'pages/_commons/redux/Change';
import { defaultChangeManagementDataFilter } from 'pages/_commons/redux/Change/reducers';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { useSelectWindowDimension } from '../redux/Common';
import Paging from '../Utils/Paging';
import SortOrder from '../Utils/SortOrder';
import ChangeFilter from './CustomFilter';
import ChangeHeader from './Header';

export interface IChangeListProps {
  containerClassName?: string;
  hideBreadCrumb?: boolean;
  hideHeader?: boolean;
  hideViewMode?: boolean;
  hideFilter?: boolean;
  title?: string;
  maxDataNumber?: number;
  isFullPage?: boolean;
}
const ChangeList: React.FC<IChangeListProps> = ({
  containerClassName,
  hideBreadCrumb,
  hideHeader,
  hideViewMode,
  hideFilter,
  title,
  maxDataNumber,
  isFullPage
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const history = useHistory();
  const [isListView, setIsListView] = useState<boolean>(true);
  const { width } = useSelectWindowDimension();

  const {
    changes: viewItems,
    loading,
    error,
    totalItem,
    changeTypeList,
    changeStatusList
  } = useSelectChangesManagement();

  const expandingChange = useSelectExpandingChange();

  const dataFilter = useSelectChangeManagementFilter();

  const {
    searchValue,
    page,
    pageSize,
    orderBy,
    filterField,
    filterValue,
    timeDescription,
    changeStatus: changeStatusFilter,
    changeType: changeTypeFilter
  } = dataFilter;

  const hasFilterBy = filterField !== CHANGE_FILTER_BY[1].value;
  const { start, end } = filterValue || {};
  const hasDateRangeFilter = start && end;
  const hasStatusFilter = changeStatusFilter !== 'All';
  const hasTypeFilter = changeTypeFilter !== 'All';
  const hasDataFilter =
    hasFilterBy || hasDateRangeFilter || hasStatusFilter || hasTypeFilter;
  const hasSearchOrDataFilter = searchValue || hasDataFilter;

  const handleToggle = useCallback(
    (changeId: string) => {
      dispatch(actionsChange.updateExpandingChange(changeId));
    },
    [dispatch]
  );

  // pagination
  const handleChangePage = useCallback(
    (pageNumber: number) => {
      dispatch(
        actionsChange.updateChangeManagementFilter({ page: pageNumber })
      );
      handleToggle('');
    },
    [dispatch, handleToggle]
  );

  const handleChangePageSize = useCallback(
    (pageSizeNumber: number) => {
      dispatch(
        actionsChange.updateChangeManagementFilter({
          pageSize: pageSizeNumber
        })
      );
    },
    [dispatch]
  );

  // sort by
  const handleChangeSortBy = useCallback(
    (sortByValue: string) => {
      dispatch(
        actionsChange.updateChangeManagementFilter({
          sortBy: [sortByValue],
          orderBy: orderBy?.slice(0, 1)
        })
      );
      handleToggle('');
    },
    [dispatch, handleToggle, orderBy]
  );

  // order by
  const handleChangeOrderBy = useCallback(
    (orderByValue: string) => {
      dispatch(
        actionsChange.updateChangeManagementFilter({
          orderBy: [orderByValue as OrderByType]
        })
      );
      handleToggle('');
    },
    [dispatch, handleToggle]
  );

  const handleSearch = useCallback(
    (val: string) => {
      dispatch(
        actionsChange.updateChangeManagementFilter({
          searchValue: val
        })
      );
      handleToggle('');
    },
    [dispatch, handleToggle]
  );

  const handleFilterByChangeStatus = useCallback(
    (changeStatus: string) => {
      dispatch(
        actionsChange.updateChangeManagementFilter({
          changeStatus
        })
      );
      handleToggle('');
    },
    [dispatch, handleToggle]
  );

  const handleFilterByChange = useCallback(
    (filter: string) => {
      dispatch(
        actionsChange.updateChangeManagementFilter({
          filterField: filter
        })
      );
      handleToggle('');
    },
    [dispatch, handleToggle]
  );

  const handleFilterValueChange = useCallback(
    ({ description, value }) => {
      dispatch(
        actionsChange.updateChangeManagementFilter({
          filterValue: value,
          timeDescription: description
        })
      );
      handleToggle('');
    },
    [dispatch, handleToggle]
  );

  const handleFilterByChangeType = useCallback(
    (changeType: string) => {
      dispatch(actionsChange.updateChangeManagementFilter({ changeType }));
      handleToggle('');
    },
    [dispatch, handleToggle]
  );

  // clear and reset
  const handleClearAndReset = useCallback(() => {
    dispatch(actionsChange.clearAndResetChangeManagement());
    handleToggle('');
  }, [dispatch, handleToggle]);

  const handleApiReload = () => {
    dispatch(actionsChange.getLastUpdatedChanges());
  };

  const navigateToChangeDetail = useCallback(
    (changeId: string) => {
      history.push(changeDetailLink(changeId));
    },
    [history]
  );

  useEffect(() => {
    dispatch(actionsChange.getLastUpdatedChanges());
    if (maxDataNumber) {
      dispatch(
        actionsChange.updateChangeManagementFilter({
          pageSize: maxDataNumber
        })
      );
    }
    return () => {
      dispatch(actionsChange.setChangeManagementToDefault());
    };
  }, [dispatch, maxDataNumber]);

  useEffect(() => {
    return () => {
      handleToggle('');
    };
  }, [handleToggle]);

  const changesCard = useMemo(() => {
    return (
      <div className={classnames('row', { 'masonry-column': !isListView })}>
        {viewItems.map(item => (
          <ChangeCardView
            key={item.changeId}
            id={`changeCardView__${item.changeId}`}
            value={item}
            onClick={() => navigateToChangeDetail(item.changeId)}
            isListViewMode={isListView}
            isExpand={expandingChange === item.changeId}
            onToggle={handleToggle}
          />
        ))}
      </div>
    );
  }, [
    viewItems,
    navigateToChangeDetail,
    isListView,
    expandingChange,
    handleToggle
  ]);

  return (
    <SimpleBar>
      <div
        className={classnames(
          {
            'loading h-100-main': loading && !hideBreadCrumb,
            'loading mh-320': loading && hideBreadCrumb
          },
          containerClassName
        )}
      >
        {!hideHeader && (
          <ChangeHeader
            isListView={isListView}
            hideBreadCrumb={hideBreadCrumb}
            hideViewMode={hideViewMode}
            hideFilter={
              hideFilter || (viewItems.length === 0 && !hasSearchOrDataFilter)
            }
            title={title}
            searchValue={searchValue}
            onChangeListView={setIsListView}
            onSearch={handleSearch}
          />
        )}
        {!hideFilter && (viewItems.length > 0 || hasSearchOrDataFilter) && (
          <div className="d-flex mb-4 mt-4">
            <ChangeFilter
              changeTypeList={changeTypeList}
              changeStatusList={changeStatusList}
              changeStatusFilter={changeStatusFilter}
              filterBy={filterField}
              filterType={changeTypeFilter}
              timeFilterText={timeDescription}
              onFilterByChangeStatus={handleFilterByChangeStatus}
              onFilterByChange={handleFilterByChange}
              onFilterValueChange={handleFilterValueChange}
              onFilterByType={handleFilterByChangeType}
            />
            {viewItems.length > 0 && (
              <div className="d-flex ml-auto mt-12">
                <SortOrder
                  small={hasSearchOrDataFilter ? width <= 1580 : width <= 1050}
                  hasFilter={hasSearchOrDataFilter}
                  dataFilter={dataFilter}
                  defaultDataFilter={defaultChangeManagementDataFilter}
                  sortByFields={CHANGE_MANAGEMENT_SORT_BY_LIST}
                  onChangeSortBy={handleChangeSortBy}
                  onChangeOrderBy={handleChangeOrderBy}
                  onClearSearch={handleClearAndReset}
                />
              </div>
            )}
          </div>
        )}
        {viewItems.length > 0 ? (
          <>
            {changesCard}
            {!hideFilter && (
              <Paging
                page={page}
                pageSize={pageSize}
                totalItem={totalItem}
                onChangePage={handleChangePage}
                onChangePageSize={handleChangePageSize}
              />
            )}
          </>
        ) : (
          !loading &&
          !error && (
            <div className="d-flex flex-column justify-content-center">
              <div className="mt-80 mb-24">
                <NoDataFound
                  id="change-management-NoDataFound"
                  title={t('txt_no_changes_to_display')}
                  hasSearch={!!searchValue}
                  hasFilter={hasDataFilter}
                  linkTitle={hasSearchOrDataFilter && t('txt_clear_and_reset')}
                  onLinkClicked={handleClearAndReset}
                  isFullPage={isFullPage}
                />
              </div>
            </div>
          )
        )}
        {error && (
          <FailedApiReload
            id="change-management-error"
            onReload={handleApiReload}
            className="mt-40 pt-40 d-flex flex-column justify-content-center align-items-center"
          />
        )}
      </div>
    </SimpleBar>
  );
};

export default ChangeList;
