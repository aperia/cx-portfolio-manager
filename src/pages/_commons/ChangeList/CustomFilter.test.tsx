import { fireEvent } from '@testing-library/react';
import {
  CHANGE_FILTER_BY,
  CHANGE_STATUS,
  CHANGE_TYPE
} from 'app/constants/constants';
import { mockUseModalRegistryFnc, renderComponent } from 'app/utils';
import CustomFilter, {
  IChangeFilter,
  ITimeFilter
} from 'pages/_commons/ChangeList/CustomFilter';
import React from 'react';

const mockUseModalRegistry = mockUseModalRegistryFnc();
const mockProps: IChangeFilter = {
  changeStatusFilter: 'All',
  changeTypeList: CHANGE_TYPE.map(i => i.toLowerCase()),
  changeStatusList: CHANGE_STATUS.map(i => i.toLowerCase()),
  filterBy: '',
  onFilterValueChange: jest.fn(),
  onFilterByChange: jest.fn(),
  onFilterByChangeStatus: jest.fn(),
  onFilterByType: jest.fn()
};

const testId = 'CustomFilter';
describe('ChangeList > CustomFilter', () => {
  let wrapper: HTMLElement;

  const createWrapper = async (...props: IChangeFilter[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    ) as IChangeFilter;
    const renderResult = await renderComponent(
      testId,
      <CustomFilter {...mergeProps} />
    );
    wrapper = await renderResult.findByTestId(testId);
  };

  beforeEach(() => {
    mockUseModalRegistry.mockImplementation(() => [true, false]);
  });

  afterEach(() => {
    mockUseModalRegistry.mockClear();
  });

  const it_should_render_filter = () => {
    expect(wrapper.querySelectorAll('.dls-dropdown-list').length).toEqual(4);
  };
  const it_should_call_onFilterValueChange = (params?: ITimeFilter) => {
    if (params) {
      expect(mockProps.onFilterValueChange).toHaveBeenCalledWith(params);
    } else {
      expect(mockProps.onFilterValueChange).toHaveBeenCalled();
    }
  };
  const it_should_call_onFilterByChange = (params: any) => {
    expect(mockProps.onFilterByChange).toHaveBeenCalledWith(params);
  };
  const it_should_call_onFilterByChangeStatus = (params: any) => {
    expect(mockProps.onFilterByChangeStatus).toHaveBeenCalled();
  };

  const it_should_call_onFilterType = (params: any) => {
    expect(mockProps.onFilterByType).toHaveBeenCalled();
  };

  const then_change_dropdown_item = (filterIdx: number, itemIdx: number) => {
    const element = wrapper
      .querySelectorAll('.dls-dropdown-list .text-field-container')
      .item(filterIdx);
    fireEvent.focus(element);

    const pageElement = document
      .querySelectorAll('.dls-popup .item')
      .item(itemIdx);
    fireEvent.click(pageElement);
  };
  const then_select_daterange_time = () => {
    const element = document.querySelector(
      '.dls-modal-root .dls-date-range-picker > i.icon'
    );
    fireEvent.mouseDown(element!);

    const dayElements = document.querySelectorAll(
      '.dls-popup.dls-date-picker-popup .react-calendar__tile'
    );
    fireEvent.click(dayElements.item(6));
    fireEvent.click(dayElements.item(15));
  };
  const then_apply_daterange_time = () => {
    const element = document.querySelector(
      '.dls-modal-root .dls-modal-footer button.btn-primary'
    );
    fireEvent.click(element!);
  };
  const then_close_daterange_time = () => {
    const element = document.querySelector(
      '.dls-modal-root .dls-modal-footer button.btn-secondary'
    );
    fireEvent.click(element!);
  };

  it('should show empty default filter', async () => {
    await createWrapper(mockProps);

    it_should_render_filter();
  });

  it('should change filter by', async () => {
    await createWrapper(mockProps);

    it_should_render_filter();

    then_change_dropdown_item(0, 2);
    it_should_call_onFilterByChange(CHANGE_FILTER_BY[2].value);
  });

  it('should change filter by time', async () => {
    await createWrapper(mockProps);

    it_should_render_filter();

    then_change_dropdown_item(1, 0);
    it_should_call_onFilterValueChange({
      description: 'All',
      value: { start: undefined, end: undefined }
    });
    then_change_dropdown_item(1, 1);
    it_should_call_onFilterValueChange();
    then_change_dropdown_item(1, 2);
    it_should_call_onFilterValueChange();
    then_change_dropdown_item(1, 3);
    it_should_call_onFilterValueChange();
    then_close_daterange_time();

    then_change_dropdown_item(1, 4);
    then_select_daterange_time();
    then_apply_daterange_time();
    it_should_call_onFilterValueChange();
  });

  it('should change filter status', async () => {
    await createWrapper(mockProps, { timeFilterText: 'aa-a' } as IChangeFilter);

    it_should_render_filter();

    then_change_dropdown_item(3, 2);
    it_should_call_onFilterByChangeStatus(CHANGE_STATUS[2].toLowerCase());
  });

  it('should change filter type', async () => {
    await createWrapper(mockProps);

    it_should_render_filter();

    then_change_dropdown_item(2, 2);
    it_should_call_onFilterType(CHANGE_TYPE[2].toLowerCase());
  });
});
