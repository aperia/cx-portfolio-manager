import * as config from 'app/constants/configurations';
import { renderWithMockStore } from 'app/utils';
import * as NotificationSelector from 'pages/_commons/redux/Notification/select-hooks';
import React from 'react';
import * as reactRedux from 'react-redux';
import NotificationProvider from '.';
const mockConfig: any = config;

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');
const mockselectNotificationLoading = jest.spyOn(
  NotificationSelector,
  'useSelectNotificationLoading'
);

jest.mock('./InAppNotifications', () => ({
  __esModule: true,
  default: () => <div>Mocked InAppNotifications</div>
}));
jest.mock('react-router-dom', () => ({
  useLocation: () => ({})
}));

mockConfig.appSetting = { notificationInterval: 1 };
mockConfig.appFeatureFlag = { notificationOff: false };

describe('Test NotificationProvider', () => {
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => (() => {}) as any);
    mockselectNotificationLoading.mockReturnValue(false);
    mockConfig.appFeatureFlag = { notificationOff: false };
  });

  it('render to UI', async () => {
    jest.useFakeTimers();

    const storeCofig = {
      initialState: {}
    };
    const { queryByText } = await renderWithMockStore(
      <NotificationProvider>
        <div>test Notifications</div>
      </NotificationProvider>,
      storeCofig
    );
    jest.advanceTimersByTime(1100);
    expect(queryByText('test Notifications')).toBeTruthy();
  });

  it('should not render when appFeatureFlag.notificationOff = true', async () => {
    jest.useFakeTimers();
    mockConfig.appFeatureFlag = { notificationOff: true };

    const storeCofig = {
      initialState: {}
    };
    const { queryByText } = await renderWithMockStore(
      <NotificationProvider>
        <div>test Notifications</div>
      </NotificationProvider>,
      storeCofig
    );
    jest.advanceTimersByTime(1100);
    expect(queryByText('test Notifications')).toBeTruthy();
  });
});
