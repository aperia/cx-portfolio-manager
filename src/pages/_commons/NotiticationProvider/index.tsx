import { appFeatureFlag, appSetting } from 'app/constants/configurations';
import {
  actionsNotification,
  useSelectNotificationLoading
} from 'pages/_commons/redux/Notification';
import React, { Fragment, useEffect, useRef } from 'react';
import { useDispatch } from 'react-redux';
import { useLocation } from 'react-router-dom';
import { actionsModals } from '../redux/modal';
import InAppNotifications from './InAppNotifications';

const useInterval = (callback: Function) => {
  const savedCallback = useRef<Function>();
  const timeout = (appSetting.notificationInterval as number) * 1000;
  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  useEffect(() => {
    if (appFeatureFlag.notificationOff) return;

    function call() {
      savedCallback.current && savedCallback.current();
    }
    call();
    const intervalId = setInterval(call, timeout);
    return () => clearInterval(intervalId);
  }, [timeout]);
};

const NotificationProvider: React.FC = ({ children }) => {
  const dispatch = useDispatch();
  const location = useLocation();
  const notificationLoading = useSelectNotificationLoading();

  async function fetchNotifications() {
    !notificationLoading &&
      dispatch(actionsNotification.getNotifications({ showLoading: false }));
  }
  useInterval(fetchNotifications);

  useEffect(() => {
    dispatch(actionsModals.closeAll());
  }, [location, dispatch]);

  return (
    <Fragment>
      {children}
      <InAppNotifications />
    </Fragment>
  );
};

export default NotificationProvider;
