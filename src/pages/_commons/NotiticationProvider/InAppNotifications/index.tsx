import ReminderModal from 'app/components/ReminderModal';
import { IN_APP_NOTIFICATION_DELAY } from 'app/constants/constants';
import { changeDetailLink } from 'app/constants/links';
import { NOTIFICATION_REMINDER_ACTIONS } from 'app/constants/notification-types';
import {
  actionsNotification,
  useSelectInAppNotifications
} from 'pages/_commons/redux/Notification';
import React, { useCallback, useState } from 'react';
import { batch, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { TransitionGroup } from 'react-transition-group';
import Alert from './Alert';
import AlertContainer from './AlertContainer';
import AlertTransition from './AlertTransition';

export interface InAppNotificationsProps {}

const InAppNotifications: React.FC<InAppNotificationsProps> = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const notificationsInApp = useSelectInAppNotifications();

  const [reminderItem, setReminderItem] = useState<INotification>();

  const navigateToChangeDetail = useCallback(
    changeSetId => {
      setTimeout(() => {
        if (history.location.pathname === changeDetailLink(changeSetId)) {
          document.location.reload();
        } else {
          history.push(changeDetailLink(changeSetId));
        }
      }, 400);
    },
    [history]
  );

  const handleClickNotification = useCallback(
    (item: INotification) => {
      const { id, changeSetId, action } = item;
      const onClick = () => {
        batch(() => {
          dispatch(actionsNotification.removeInAppNotification(id));
          dispatch(
            actionsNotification.markNotificationsAsRead({
              notificationIds: [id],
              date: new Date()
            })
          );
        });

        const actionName = action?.toLowerCase();

        const isReminder = NOTIFICATION_REMINDER_ACTIONS.includes(actionName);
        if (isReminder) {
          setReminderItem(item);
          return;
        }

        navigateToChangeDetail(changeSetId);
      };

      return onClick;
    },
    [dispatch, navigateToChangeDetail]
  );

  return (
    <AlertContainer>
      <TransitionGroup>
        {notificationsInApp.slice(0, 5).map((item: INotification) => {
          return (
            <AlertTransition key={item.id}>
              <Alert
                id={item.id}
                message={item.title}
                description={item.description}
                delay={IN_APP_NOTIFICATION_DELAY}
                onClick={handleClickNotification(item)}
              />
            </AlertTransition>
          );
        })}
      </TransitionGroup>
      {reminderItem && (
        <ReminderModal
          id={`ChangeSetReminderModal_${reminderItem.id}`}
          show
          workflowTemplateId={reminderItem.workflowTemplateId!}
          workflowName={reminderItem.workflowName!}
          description={reminderItem.description}
          onClose={() => setReminderItem(undefined)}
        />
      )}
    </AlertContainer>
  );
};

export default InAppNotifications;
