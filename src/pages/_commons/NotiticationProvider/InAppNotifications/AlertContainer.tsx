import React, { Children, cloneElement } from 'react';
import { TransitionGroup } from 'react-transition-group';
import AlertTransition from './AlertTransition';
interface AlertContainerProps {}
const AlertContainer: React.FC<AlertContainerProps> = ({ children }) => {
  return (
    <div className="notifications">
      <TransitionGroup>
        {Children.map(children, (child, idx) =>
          child ? (
            <AlertTransition key={idx}>
              {cloneElement(child as any)}
            </AlertTransition>
          ) : null
        )}
      </TransitionGroup>
    </div>
  );
};

export default AlertContainer;
