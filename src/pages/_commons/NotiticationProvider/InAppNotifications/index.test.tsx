import { fireEvent } from '@testing-library/dom';
import { IReminderModalProps } from 'app/components/ReminderModal';
import { BASE_URL } from 'app/constants/links';
import { mockUseDispatchFnc, renderComponent } from 'app/utils';
import React from 'react';
import * as reactRedux from 'react-redux';
import InAppNotifications from '.';
import { AlertProps } from './Alert';

jest.mock('./Alert', () => ({
  __esModule: true,
  default: ({ id, message, description, delay, onClick }: AlertProps) => {
    return (
      <div>
        <div>Mocked Alert</div>
        <div onClick={() => onClick && onClick()}>{id}</div>
      </div>
    );
  }
}));

jest.mock('app/components/ReminderModal', () => {
  return {
    __esModule: true,
    default: ({ onClose }: IReminderModalProps) => {
      return (
        <div>
          <div>Reminder Modal</div>
          <button onClick={() => onClose && onClose()}>Close Modal</button>
        </div>
      );
    }
  };
});
const testId = 'inAppNotifications';
let selectorMock: jest.SpyInstance;
const mockUseDispatch = mockUseDispatchFnc();
const mockHistory = {
  replace: jest.fn(),
  push: jest.fn(),
  location: {
    pathname: `${BASE_URL}change-management/detail/changeSetId1`
  }
};
jest.mock('react-router', () => ({
  useHistory: () => mockHistory,
  useLocation: () => ({}),
  useParams: () => ({})
}));

const mockSelectorNotifications = () => {
  selectorMock = jest
    .spyOn(reactRedux, 'useSelector')
    .mockImplementation(selector =>
      selector({
        notification: {
          notificationList: {
            notifications: [],
            notificationsInApp: [
              {
                id: 'id1',
                changeSetId: 'changeSetId1',
                title: 'title',
                description: 'description'
              },
              {
                id: 'id2',
                changeSetId: 'changeSetId2',
                title: 'title',
                description: 'description'
              },
              {
                id: 'id3',
                changeSetId: 'changeSetId3',
                title: 'title',
                description: 'description',
                action: 'addedreminder'
              }
            ],
            loading: true,
            error: false,
            filter: undefined
          }
        }
      })
    );
};
describe('Test InAppNotifications', () => {
  beforeEach(() => {
    mockSelectorNotifications();
    mockUseDispatch.mockImplementation(() => jest.fn());
    jest.useFakeTimers();
  });
  afterEach(() => {
    selectorMock.mockClear();
    jest.useRealTimers();
  });

  it('render to UI', async () => {
    mockSelectorNotifications();
    const { getAllByText, getByText } = await renderComponent(
      testId,
      <InAppNotifications />
    );
    expect(getAllByText('Mocked Alert').length).toEqual(3);

    fireEvent.click(getByText(/id1/));
    jest.runAllTimers();
    expect(setTimeout).toBeCalled();

    fireEvent.click(getByText(/id2/));
    jest.runAllTimers();
    expect(setTimeout).toBeCalled();
  });

  it('render to UI > Reminder Modal', async () => {
    mockSelectorNotifications();
    const { getAllByText, getByText } = await renderComponent(
      testId,
      <InAppNotifications />
    );
    expect(getAllByText('Mocked Alert').length).toEqual(3);
    fireEvent.click(getByText(/id3/));
    expect(getByText(/Reminder Modal/)).toBeInTheDocument();
    fireEvent.click(getByText(/Close Modal/));
    expect(getByText(/id3/)).toBeInTheDocument();
  });
});
