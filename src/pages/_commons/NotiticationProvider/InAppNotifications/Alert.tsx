import { classnames } from 'app/helpers';
import { Button, Icon, TruncateText } from 'app/_libraries/_dls';
import { isFunction } from 'lodash';
import { actionsNotification } from 'pages/_commons/redux/Notification';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';

export interface AlertProps {
  id: string;
  message: string;
  description?: string;
  delay?: number;
  onClick?: () => void;
}
const Alert: React.FC<AlertProps> = ({
  id,
  message,
  description = '',
  delay = 10000,
  onClick
}) => {
  const dispatch = useDispatch();
  const [isHovered, setIsHovered] = useState(false);

  const handleOnCloseClicked = (
    event: React.MouseEvent<HTMLElement, MouseEvent>
  ) => {
    event.stopPropagation();
    dispatch(actionsNotification.removeInAppNotification(id));
  };

  useEffect(() => {
    const timeoutId = setTimeout(() => {
      dispatch(actionsNotification.removeInAppNotification(id));
    }, delay);

    return () => {
      if (timeoutId) {
        clearTimeout(timeoutId);
      }
    };
  }, [delay, dispatch, id]);

  const handleClick = () => {
    setIsHovered(true);
    isFunction(onClick) && onClick();
  };

  return (
    <div
      className="notification"
      onClick={handleClick}
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
    >
      <div
        className={classnames('notification-wrapper', isHovered && 'hovered')}
      >
        <div className="d-flex">
          <Icon className="p-4" name={'notification' as any} size="5x" />

          <div className="p-4">
            <div className="notification-text">{message}</div>
            {description && (
              <div className="pt-8">
                <TruncateText lines={2} title={description}>
                  {description}
                </TruncateText>
              </div>
            )}
          </div>

          <Button
            className="ml-auto"
            variant="icon-secondary"
            onClick={handleOnCloseClicked}
          >
            <Icon name="close" size="4x" />
          </Button>
        </div>
      </div>
    </div>
  );
};

export default Alert;
