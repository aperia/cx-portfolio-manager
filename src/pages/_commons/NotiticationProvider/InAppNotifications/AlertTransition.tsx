import React from 'react';
import { CSSTransition } from 'react-transition-group';

const AlertTransition: React.FC = prop => {
  const timeout = { enter: 500, exit: 300 };
  return (
    <CSSTransition timeout={timeout} classNames="notification" {...prop} />
  );
};

export default AlertTransition;
