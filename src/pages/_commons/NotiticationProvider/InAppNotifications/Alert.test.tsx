import { fireEvent } from '@testing-library/react';
import { renderComponent } from 'app/utils';
import React from 'react';
import * as reactRedux from 'react-redux';
import Alert, { AlertProps } from './Alert';

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');
jest.mock('react-router-dom', () => ({
  useLocation: () => ({}),
  useHistory: () => ({
    push: jest.fn()
  })
}));

const testId = 'alert';

const alertProps: AlertProps = {
  id: '123',
  message: 'test message',
  description: 'test description',
  delay: 300,
  onClick: jest.fn()
};

describe('Test Alert', () => {
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => (() => {}) as any);
  });

  it('render to UI', async () => {
    const { queryByText } = await renderComponent(
      testId,
      <Alert {...alertProps} />
    );
    expect(queryByText('test message')).toBeTruthy();
  });

  it('should be navigate to detail when clicking on alert', async () => {
    const { getByText, queryByText } = await renderComponent(
      testId,
      <Alert {...alertProps} />
    );
    expect(queryByText('test message')).toBeTruthy();
    expect(queryByText('test description')).toBeTruthy();
    jest.useFakeTimers();
    fireEvent.click(getByText('test message'));
    jest.advanceTimersByTime(400);
    expect(mockUseDispatch).toBeCalled();
  });

  it('should be remove alert when clicking on close button', async () => {
    const alertProps: AlertProps = {
      id: '123',
      message: 'test message'
    };

    const { queryByText } = await renderComponent(
      testId,
      <Alert {...alertProps} />
    );
    expect(queryByText('test message')).toBeTruthy();
    expect(queryByText('test description')).toBeNull();
    const closeIcon = document.querySelector('i.icon-close');
    jest.useFakeTimers();
    fireEvent.click(closeIcon!);
    jest.advanceTimersByTime(400);
    expect(mockUseDispatch).toBeCalled();
  });

  it('should add hovered class when hover on alert', async () => {
    const { getByText, findByTestId } = await renderComponent(
      testId,
      <Alert {...alertProps} />
    );
    fireEvent.mouseEnter(getByText('test message'));
    const wrapper = await findByTestId(testId);

    expect(wrapper.querySelector('div.hovered')).toBeTruthy();
  });

  it('should remove hovered class when not hover on alert', async () => {
    const { getByText, findByTestId } = await renderComponent(
      testId,
      <Alert {...alertProps} />
    );
    fireEvent.mouseEnter(getByText('test message'));
    fireEvent.mouseLeave(getByText('test message'));
    const wrapper = await findByTestId(testId);

    expect(wrapper.querySelector('div.hovered')).toBeNull();
  });
});
