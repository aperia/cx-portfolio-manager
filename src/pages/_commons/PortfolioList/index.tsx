import FailedApiReload from 'app/components/FailedApiReload';
import NoDataFound from 'app/components/NoDataFound';
import PortfolioCardView from 'app/components/PortfolioCardView';
import {
  PORTFOLIOS_CRITERIA_FIELDS,
  PORTFOLIOS_SORT_BY_LIST
} from 'app/constants/constants';
import { PortfoliosSortByFields } from 'app/constants/enums';
import { myPortfoliosDetail } from 'app/constants/links';
import { classnames } from 'app/helpers';
import { useTranslation } from 'app/_libraries/_dls';
import diff from 'lodash.differencewith';
import includes from 'lodash.includes';
import isEmpty from 'lodash.isempty';
import sortBy from 'lodash.sortby';
import {
  actionsPortfolio,
  defaultDataFilter,
  filterPortfolios,
  useSelectPortfolios,
  useSelectPortfoliosFilter
} from 'pages/_commons/redux/Portfolio';
import Paging from 'pages/_commons/Utils/Paging';
import SortOrder from 'pages/_commons/Utils/SortOrder';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
import PortfolioHeader from './Header';

interface IPortfolioListProps {
  changeId?: string;
  containerClassName?: string;
  hideBreadCrumb?: boolean;
  hideHeader?: boolean;
  hideViewMode?: boolean;
  hideFilter?: boolean;
  title?: string;
  maxDataNumber?: number;
  isFullPage?: boolean;
  isTopFive?: boolean;
}
const PortfolioList: React.FC<IPortfolioListProps> = ({
  changeId,
  containerClassName,
  hideBreadCrumb,
  hideHeader,
  hideViewMode,
  hideFilter,
  title,
  maxDataNumber,
  isFullPage,
  isTopFive
}) => {
  const dispatch = useDispatch();
  const [isListView, setIsListView] = useState<boolean>(true);
  const history = useHistory();
  const selectPortfolios = useSelectPortfolios();
  const { portfolios, loading, error, portfolioCriteriaFields } =
    selectPortfolios;
  const { t } = useTranslation();

  const dataFilter = useSelectPortfoliosFilter();
  const { page, pageSize, searchValue, orderBy } = dataFilter;

  const [isReady, setIsReady] = useState<boolean>(false);
  const [myPortfolios, setMyPortfolio] = useState<IPortfolioModel[]>([]);
  const [totalItem, setTotalItem] = useState<number>(0);

  const sortByFields = useMemo(() => {
    let sortBy = PORTFOLIOS_SORT_BY_LIST;
    const hiddenFields = diff(
      PORTFOLIOS_CRITERIA_FIELDS,
      portfolioCriteriaFields!
    );
    if (!isEmpty(hiddenFields)) {
      sortBy = sortBy.filter(i => !includes(hiddenFields, i.value));
    }

    return sortBy;
  }, [portfolioCriteriaFields]);

  // sort by
  const handleChangeSortBy = useCallback(
    (sortByValue: string) => {
      dispatch(
        actionsPortfolio.updateFilter({
          sortBy: [sortByValue],
          orderBy: orderBy!.slice(0, 1)
        })
      );
    },
    [dispatch, orderBy]
  );

  // order by
  const handleChangeOrderBy = useCallback(
    (orderByValue: string) => {
      dispatch(
        actionsPortfolio.updateFilter({
          orderBy: [orderByValue as OrderByType]
        })
      );
    },
    [dispatch]
  );

  // pagination
  const handleChangePage = useCallback(
    (pageNumber: number) => {
      dispatch(actionsPortfolio.updateFilter({ page: pageNumber }));
    },
    [dispatch]
  );
  const handleChangePageSize = useCallback(
    (pageSizeNumber: number) => {
      dispatch(
        actionsPortfolio.updateFilter({
          pageSize: pageSizeNumber
        })
      );
    },
    [dispatch]
  );

  const handleSearch = useCallback(
    (val: string) => {
      dispatch(
        actionsPortfolio.updateFilter({
          searchValue: val
        })
      );
    },
    [dispatch]
  );

  const handleClearAndReset = useCallback(() => {
    dispatch(actionsPortfolio.clearAndReset());
  }, [dispatch]);

  const handleApiReload = () => {
    dispatch(actionsPortfolio.getPortfolioInchange(changeId));
  };

  useEffect(() => {
    dispatch(actionsPortfolio.getPortfolioInchange(changeId));
    if (maxDataNumber) {
      dispatch(
        actionsPortfolio.updateFilter({
          pageSize: maxDataNumber
        })
      );
    }
    setIsReady(true);
    return () => {
      dispatch(actionsPortfolio.setToDefault());
    };
  }, [dispatch, maxDataNumber, changeId]);

  useEffect(() => {
    if (isReady && portfolios?.length > 0) {
      const { portfolioList, totalItem: totalItemNumber } = filterPortfolios(
        portfolios,
        dataFilter
      );

      setMyPortfolio(
        isTopFive ? sortBy(portfolioList, ['portfolioName']) : portfolioList
      );
      setTotalItem(totalItemNumber);
    }
  }, [dataFilter, portfolios, isReady, isTopFive]);

  const handleClickViewPortfolioDetail = useCallback(
    item => {
      history.push(myPortfoliosDetail(item.id));
    },
    [history]
  );

  const portfolioCard = useMemo(() => {
    return (
      <div className="row">
        {myPortfolios.map(item => (
          <PortfolioCardView
            key={item.id}
            id={`portfolioCardView__${item.id}`}
            value={item}
            isListViewMode={isListView}
            onClick={() => handleClickViewPortfolioDetail(item)}
          />
        ))}
      </div>
    );
  }, [myPortfolios, isListView, handleClickViewPortfolioDetail]);

  const searchFieldItems = useMemo(() => {
    return PORTFOLIOS_SORT_BY_LIST.filter(
      i =>
        includes(portfolioCriteriaFields, i.value) ||
        i.value === PortfoliosSortByFields.PORTFOLIO_NAME
    );
  }, [portfolioCriteriaFields]);

  return (
    <div
      className={classnames(
        {
          'loading h-100-main': loading && !hideBreadCrumb,
          'loading mh-320': loading && hideBreadCrumb
        },
        containerClassName
      )}
    >
      {!hideHeader && (
        <PortfolioHeader
          isListView={isListView}
          hideBreadCrumb={hideBreadCrumb}
          hideViewMode={hideViewMode}
          hideFilter={isEmpty(myPortfolios) && !searchValue}
          title={title}
          searchValue={searchValue}
          onChangeListView={setIsListView}
          searchFieldItems={searchFieldItems}
          onSearch={handleSearch}
        />
      )}
      {!isEmpty(myPortfolios) ? (
        <>
          {!hideFilter && (
            <div className="d-flex align-items-center mt-16 mb-4">
              <SortOrder
                hasFilter={!!searchValue}
                dataFilter={dataFilter}
                defaultDataFilter={defaultDataFilter}
                sortByFields={sortByFields}
                onChangeSortBy={handleChangeSortBy}
                onChangeOrderBy={handleChangeOrderBy}
                onClearSearch={handleClearAndReset}
              />
            </div>
          )}
          {portfolioCard}
          {!hideFilter && (
            <Paging
              page={page}
              pageSize={pageSize}
              totalItem={totalItem}
              onChangePage={handleChangePage}
              onChangePageSize={handleChangePageSize}
            />
          )}
        </>
      ) : (
        !loading &&
        !error &&
        (portfolios?.length == 0 || totalItem === 0) && (
          <>
            <div className="d-flex flex-column justify-content-center mt-80 mb-24">
              <NoDataFound
                id="portfolio-NoDataFound"
                title={t('txt_portfolios_no_data_to_display')}
                hasSearch={!!searchValue}
                linkTitle={searchValue && t('txt_clear_and_reset')}
                onLinkClicked={handleClearAndReset}
                isFullPage={isFullPage}
              />
            </div>
          </>
        )
      )}
      {error && (
        <FailedApiReload
          id="portfolios-error"
          onReload={handleApiReload}
          className="mt-40 pt-40 d-flex flex-column justify-content-center align-items-center"
        />
      )}
    </div>
  );
};

export default PortfolioList;
