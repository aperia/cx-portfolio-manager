import { fireEvent, screen } from '@testing-library/dom';
import { RenderResult } from '@testing-library/react';
import { PortfolioData, PortfolioMapping } from 'app/fixtures/portfolio-data';
import { mappingDataFromArray } from 'app/helpers';
import { PortfolioState } from 'app/types';
import { renderWithMockStore, spyOnTranslationHoook } from 'app/utils';
import 'app/utils/_mockComponent/mockBreadCrumb';
import 'app/utils/_mockComponent/mockButtonGroupFilter';
import 'app/utils/_mockComponent/mockDofView';
import 'app/utils/_mockComponent/mockFailedApiReload';
import 'app/utils/_mockComponent/mockNoDataFound';
import 'app/utils/_mockComponent/mockPaging';
import 'app/utils/_mockComponent/mockSortOrder';
import * as PortfolioSelector from 'pages/_commons/redux/Portfolio/select-hooks';
import React from 'react';
import { defaultDataFilter } from '../redux/Portfolio';
import PortfolioList from './';

const mockHistory = {
  push: jest.fn()
};
jest.mock('react-router', () => ({
  useHistory: () => mockHistory
}));

const mockUseSelectPortfolios = ({
  portfolios,
  loading,
  error,
  dataFilter,
  portfolioCriteriaFields
}: any) => {
  jest.spyOn(PortfolioSelector, 'useSelectPortfolios').mockReturnValue({
    portfolios,
    loading,
    error,
    dataFilter,
    portfolioCriteriaFields
  });
};

const mockUseSelectPortfoliosFilter = (dataFilter: any) => {
  jest
    .spyOn(PortfolioSelector, 'useSelectPortfoliosFilter')
    .mockReturnValue(dataFilter);
};

const mockFilterPortfolios = ({ portfolioList, totalItem }: any) => {
  jest.spyOn(PortfolioSelector, 'filterPortfolios').mockReturnValue({
    portfolioList,
    totalItem
  });
};

const testId = 'list-test-id';

let mockPortfolioData: IPortfolioCardView[] = [];
mockPortfolioData = mappingDataFromArray(PortfolioData, PortfolioMapping)?.map(
  (p: any) => {
    if (!p.icon) {
      delete p.icon;
    }
    return p;
  }
);

export const ListMock: any = {
  changeId: '123',
  containerClassName: 'container',
  title: 'Portfolio title'
};

describe('PortfolioList', () => {
  let renderResult: RenderResult;
  let storeCofig: any = {};
  beforeEach(() => {
    storeCofig = {
      initialState: {
        portfolio: {
          portfoliosData: {
            portfolios: mockPortfolioData,
            loading: false,
            dataFilter: defaultDataFilter,
            error: false
          }
        } as PortfolioState
      }
    };
    spyOnTranslationHoook();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render portfolio list', async () => {
    mockUseSelectPortfolios({
      portfolios: mockPortfolioData,
      loading: false,
      error: false,
      dataFilter: defaultDataFilter
    });
    mockFilterPortfolios({
      portfolioList: mockPortfolioData,
      totalItem: 30
    });
    mockUseSelectPortfoliosFilter(defaultDataFilter);
    renderResult = await renderWithMockStore(
      <PortfolioList {...ListMock} />,
      storeCofig,
      {},
      testId
    );
    const wrapper = await renderResult.findByTestId(testId);
    expect(wrapper).toBeInTheDocument();
    expect(screen.queryByText('Mock BreadCrumb')).toBeInTheDocument();
    expect(screen.queryByText('Portfolio title')).toBeInTheDocument();
    expect(screen.getAllByText('portfolio-card-view').length).toEqual(2);
  });

  it('should render portfolio list without header, filter & breadcrumb', async () => {
    mockUseSelectPortfolios({
      portfolios: mockPortfolioData,
      loading: false,
      error: false,
      dataFilter: defaultDataFilter
    });
    mockFilterPortfolios({
      portfolioList: mockPortfolioData,
      totalItem: 30
    });
    mockUseSelectPortfoliosFilter(defaultDataFilter);
    renderResult = await renderWithMockStore(
      <PortfolioList
        {...ListMock}
        hideBreadCrumb
        hideFilter
        hideHeader
        isTopFive
        maxDataNumber={5}
      />,
      storeCofig,
      {},
      testId
    );
    const wrapper = await renderResult.findByTestId(testId);
    expect(wrapper).toBeInTheDocument();
    expect(screen.queryByText('Mock BreadCrumb')).toBeNull();
    expect(screen.queryByText('Portfolio title')).toBeNull();
    expect(screen.getAllByText('portfolio-card-view').length).toEqual(2);
  });

  it('should render no data found', async () => {
    mockUseSelectPortfolios({
      portfolios: [],
      loading: false,
      error: false,
      dataFilter: defaultDataFilter
    });
    mockFilterPortfolios({
      portfolioList: [],
      totalItem: 0
    });
    mockUseSelectPortfoliosFilter(defaultDataFilter);
    renderResult = await renderWithMockStore(
      <PortfolioList {...ListMock} />,
      storeCofig,
      {},
      testId
    );
    const { getByText } = renderResult;
    expect(getByText('No data found')).toBeInTheDocument();
  });

  it('should render no data found with search', async () => {
    mockUseSelectPortfolios({
      portfolios: [],
      loading: false,
      error: false,
      dataFilter: defaultDataFilter
    });
    mockFilterPortfolios({
      portfolioList: [],
      totalItem: 0
    });
    mockUseSelectPortfoliosFilter({
      ...defaultDataFilter,
      searchValue: 'test value'
    });
    renderResult = await renderWithMockStore(
      <PortfolioList {...ListMock} />,
      storeCofig,
      {},
      testId
    );
    const { getByText } = renderResult;
    expect(getByText('No data found')).toBeInTheDocument();
  });

  it('should render data load unsuccessfully', async () => {
    mockUseSelectPortfolios({
      portfolios: mockPortfolioData,
      loading: false,
      error: true,
      dataFilter: defaultDataFilter
    });
    mockFilterPortfolios({
      portfolioList: [],
      totalItem: 0
    });
    mockUseSelectPortfoliosFilter(defaultDataFilter);
    renderResult = await renderWithMockStore(
      <PortfolioList {...ListMock} />,
      storeCofig,
      {},
      testId
    );
    const { getByText } = renderResult;
    fireEvent.click(screen.getByText('Reload'));
    expect(getByText('Data load unsuccessful')).toBeInTheDocument();
  });

  it('filter on portfolio list', async () => {
    mockUseSelectPortfolios({
      portfolios: mockPortfolioData,
      loading: false,
      error: false,
      dataFilter: defaultDataFilter
    });
    mockFilterPortfolios({
      portfolioList: mockPortfolioData,
      totalItem: 30
    });
    mockUseSelectPortfoliosFilter(defaultDataFilter);
    renderResult = await renderWithMockStore(
      <PortfolioList {...ListMock} />,
      storeCofig,
      {},
      testId
    );

    const wrapper = await renderResult.findByTestId(testId);

    fireEvent.click(wrapper.querySelector('.icon.icon-search')!);
    fireEvent.click(screen.getByText('Sort By'));
    fireEvent.click(screen.getByText('Order By'));
    fireEvent.click(screen.getByText('Clear And Reset'));
    fireEvent.click(screen.getByText('Page 2'));
    fireEvent.click(screen.getByText('Page Size 25'));
    const firstItem = wrapper.querySelector('.list-view');
    fireEvent.click(firstItem!);
    expect(screen.getByTestId(testId)!).toBeInTheDocument();
    expect(screen.queryByText('Mock BreadCrumb')).toBeInTheDocument();
    expect(screen.queryByText('Portfolio title')).toBeInTheDocument();
    expect(screen.getAllByText('portfolio-card-view').length).toEqual(2);
  });
});
