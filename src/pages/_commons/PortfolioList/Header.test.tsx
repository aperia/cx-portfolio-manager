import { fireEvent, RenderResult, screen } from '@testing-library/react';
import { renderComponent, spyOnTranslationHoook, spyOnUseRef } from 'app/utils';
import 'app/utils/_mockComponent/mockBreadCrumb';
import React from 'react';
import PortfolioListHeader from './Header';

const testId = 'header-test-id';

export const HeaderMock = {
  id: 'header-portfolio-list',
  title: 'Mock title',
  searchValue: 'test',
  searchFieldItems: [
    {
      value: 'val1',
      description: 'Value 1'
    }
  ]
};

describe('PortfolioListHeader', () => {
  let renderResult: RenderResult;

  beforeEach(() => {
    spyOnUseRef({
      current: {
        clear: () => {}
      }
    });
    spyOnTranslationHoook();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render header with view mode & breadcrumb', async () => {
    renderResult = await renderComponent(
      testId,
      <PortfolioListHeader {...HeaderMock} />
    );
    const wrapper = await renderResult.findByTestId(testId);
    expect(wrapper.querySelector('h4')!.textContent).toEqual('Mock title');
    expect(screen.getByText('Mock BreadCrumb')).toBeInTheDocument();
    expect(wrapper.querySelector('.icon-view-list')).toBeInTheDocument();
    expect(wrapper.querySelector('.icon-view-grid')).toBeInTheDocument();
  });

  it('should render header with default list view', async () => {
    renderResult = await renderComponent(
      testId,
      <PortfolioListHeader {...HeaderMock} isListView={true} />
    );
    const wrapper = await renderResult.findByTestId(testId);
    expect(wrapper.querySelector('h4')!.textContent).toEqual('Mock title');
    expect(screen.getByText('Mock BreadCrumb')).toBeInTheDocument();
    expect(
      wrapper.querySelector('.icon-view-list')?.parentElement?.className
    ).toContain('active');
  });

  it('should render header without view mode & breadcrumb', async () => {
    renderResult = await renderComponent(
      testId,
      <PortfolioListHeader {...HeaderMock} hideBreadCrumb hideViewMode />
    );
    const wrapper = await renderResult.findByTestId(testId);
    expect(wrapper.querySelector('h4')!.textContent).toEqual('Mock title');
    expect(screen.queryByText('Mock BreadCrumb')).toBeNull();
    expect(wrapper.querySelector('.breadcrumb-list')).toBeNull();
    expect(wrapper.querySelector('.icon-view-list')).toBeNull();
  });

  it('click on view mode buttons', async () => {
    const onChangeListViewFn = jest.fn();
    const onSearchFn = jest.fn();

    renderResult = await renderComponent(
      testId,
      <PortfolioListHeader
        {...HeaderMock}
        onChangeListView={onChangeListViewFn}
        onSearch={onSearchFn}
      />
    );
    const wrapper = await renderResult.findByTestId(testId);

    fireEvent.click(wrapper.querySelector('.icon-view-list')!);
    expect(onChangeListViewFn).toHaveBeenCalled();

    fireEvent.click(wrapper.querySelector('.icon-view-grid')!);
    expect(onChangeListViewFn).toHaveBeenCalled();

    fireEvent.click(wrapper.querySelector('.icon.icon-search')!);
    expect(onSearchFn).toHaveBeenCalled();
  });
});
