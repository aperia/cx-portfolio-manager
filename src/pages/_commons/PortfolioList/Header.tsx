import BreadCrumb from 'app/components/BreadCrumb';
import SimpleSearch from 'app/components/SimpleSearch';
import { BASE_URL } from 'app/constants/links';
import { classnames } from 'app/helpers';
import { Button, Icon, Tooltip, useTranslation } from 'app/_libraries/_dls';
import isFunction from 'lodash.isfunction';
import React, { Fragment, useCallback, useEffect, useRef } from 'react';
import { Link } from 'react-router-dom';

interface IPortfolioHeader {
  isListView?: boolean;
  hideBreadCrumb?: boolean;
  title?: string;
  hideFilter?: boolean;
  hideViewMode?: boolean;
  searchValue?: string;
  searchFieldItems: any[];
  onChangeListView?: (isListView: boolean) => void;
  onSearch?: (val: string) => void;
}

const PortfolioHeader: React.FC<IPortfolioHeader> = ({
  isListView,
  hideBreadCrumb,
  title,
  hideViewMode,
  searchValue,
  searchFieldItems,
  onChangeListView,
  onSearch,
  hideFilter
}) => {
  const { t } = useTranslation();

  const simpleSearchRef = useRef<any>(null);

  const handleSearch = useCallback(
    (val: string) => {
      isFunction(onSearch) && onSearch(val);
    },
    [onSearch]
  );

  const handleChangeListView = useCallback(
    (value: boolean) => {
      isFunction(onChangeListView) && onChangeListView(value);
    },
    [onChangeListView]
  );

  useEffect(() => {
    !searchValue && simpleSearchRef.current && simpleSearchRef.current.clear();
  }, [searchValue]);

  return (
    <Fragment>
      {!hideBreadCrumb && (
        <div className="mb-16">
          <div className="section">
            <BreadCrumb
              id={'portfolio__BreadCrumb'}
              items={breadCrmbItems}
              width={992}
            />
          </div>
        </div>
      )}
      <div className="d-flex align-items-center">
        <h4>{title}</h4>
        {!hideFilter && (
          <div className="ml-auto d-flex ">
            <SimpleSearch
              ref={simpleSearchRef}
              placeholder={t('txt_type_to_search')}
              onSearch={handleSearch}
              popperElement={
                <>
                  <p className="color-grey">{t('txt_search_description')}</p>
                  <ul className="search-field-item list-unstyled">
                    {searchFieldItems?.map((i, index) => (
                      <li key={index} className="mt-16">
                        {i.description}
                      </li>
                    ))}
                  </ul>
                </>
              }
            />

            {!hideViewMode && (
              <div className="d-flex mr-n4 only-pc">
                <div className="ml-24">
                  <Tooltip
                    element={t('txt_list_view')}
                    placement="top"
                    variant={'primary'}
                  >
                    <Button
                      id={'portfolio-view-list-button'}
                      variant="icon-secondary"
                      className={classnames(isListView && 'active')}
                    >
                      <Icon
                        name="view-list"
                        color={'grey-l16' as any}
                        size="5x"
                        onClick={() => handleChangeListView(true)}
                      />
                    </Button>
                  </Tooltip>
                </div>
                <div className="ml-16">
                  <Tooltip
                    element={t('txt_grid_view')}
                    placement="top"
                    variant={'primary'}
                  >
                    <Button
                      id={'portfolio-view-grid-button'}
                      variant="icon-secondary"
                      className={classnames(!isListView && 'active')}
                    >
                      <Icon
                        name="view-grid"
                        color={'grey-l16' as any}
                        size="5x"
                        onClick={() => handleChangeListView(false)}
                      />
                    </Button>
                  </Tooltip>
                </div>
              </div>
            )}
          </div>
        )}
      </div>
    </Fragment>
  );
};

export default PortfolioHeader;

const breadCrmbItems = [
  { id: 'home', title: <Link to={BASE_URL}>Home</Link> },
  { id: 'myPortfolios', title: 'My Portfolios' }
];
