import { fireEvent, screen } from '@testing-library/dom';
import { RenderResult } from '@testing-library/react';
import { cleanup } from '@testing-library/react-hooks';
import {
  PricingStrategyData,
  PricingStrategyMapping
} from 'app/fixtures/pricing-strategy-data';
import { mappingDataFromArray } from 'app/helpers';
import { PricingStrategiesState } from 'app/types';
import { renderWithMockStore, spyOnTranslationHoook } from 'app/utils';
import 'app/utils/_mockComponent/mockButtonGroupFilter';
import 'app/utils/_mockComponent/mockDofView';
import 'app/utils/_mockComponent/mockFailedApiReload';
import 'app/utils/_mockComponent/mockNoDataFound';
import 'app/utils/_mockComponent/mockPaging';
import 'app/utils/_mockComponent/mockSortOrder';
import 'pages/_commons/PricingStrategy/mockPricingStrategyDetail';
import 'pages/_commons/PricingStrategy/mockPricingStrategyHeader';
import * as CommonRedux from 'pages/_commons/redux/Common/select-hooks';
import * as PricingStrategySelector from 'pages/_commons/redux/PricingStrategy/selector';
import React from 'react';
import { defaultDataFilter } from '../redux/PricingStrategy';
import PricingStrategyList from './List';

const mockUseSelectWindowDimension = jest.spyOn(
  CommonRedux,
  'useSelectWindowDimension'
);

const mockUseSelectWindowDimensionImplementation = (width: number) => {
  mockUseSelectWindowDimension.mockImplementation(() => ({ width } as any));
};

const mockUseSelectPricingStrategies = ({
  pricingStrategies,
  loading,
  error,
  totalItem,
  totalItemFilter
}: any) => {
  jest
    .spyOn(PricingStrategySelector, 'useSelectPricingStrategies')
    .mockReturnValue({
      pricingStrategies,
      loading,
      error,
      totalItemFilter,
      totalItem
    });
};

const mockUseSelectPricingStrategyFilter = (dataFilter: any) => {
  jest
    .spyOn(PricingStrategySelector, 'useSelectPricingStrategiesFilter')
    .mockReturnValue(dataFilter);
};

const testId = 'list-test-id';

let mockPricingStrategyData: IPricingStrategyModel[] = [];
mockPricingStrategyData = mappingDataFromArray(
  PricingStrategyData,
  PricingStrategyMapping
)?.map(d => ({
  ...d,
  versionCreating: d.modeledFrom ? 'cloned' : 'new',
  methodsUpdated: d.parameters?.length || 0
}));

export const ListMock: any = {
  changeId: '123',
  containerClassName: 'container',
  title: 'Pricing strategy title'
};

describe('PricingStrategyList', () => {
  let renderResult: RenderResult;
  let storeCofig: any = {};
  beforeEach(() => {
    mockUseSelectWindowDimensionImplementation(1300);
    storeCofig = {
      initialState: {
        pricingStrategies: {
          pricingStrategyData: {
            pricingStrategies: mockPricingStrategyData,
            loading: false,
            dataFilter: defaultDataFilter,
            error: false
          }
        } as PricingStrategiesState
      }
    };
    spyOnTranslationHoook();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render pricing strategy list', async () => {
    mockUseSelectPricingStrategies({
      pricingStrategies: mockPricingStrategyData,
      loading: false,
      error: false,
      totalItem: 30,
      totalItemFilter: 30
    });
    mockUseSelectPricingStrategyFilter(defaultDataFilter);
    renderResult = await renderWithMockStore(
      <PricingStrategyList {...ListMock} />,
      storeCofig,
      {},
      testId
    );
    const wrapper = await renderResult.findByTestId(testId);
    expect(
      wrapper.querySelector('#pricing-strategy-list-' + ListMock.changeId)!
    ).toBeInTheDocument();
    fireEvent.click(screen.getByText('Filter 1'));
    mockUseSelectPricingStrategyFilter({
      ...defaultDataFilter,
      searchFields: ['test search field']
    });
    cleanup();
    renderResult = await renderWithMockStore(
      <PricingStrategyList {...ListMock} />,
      storeCofig,
      {},
      testId
    );
    expect(screen.getByText('test search field')).toBeInTheDocument();
  });

  it('should render pricing strategy list with empty id', async () => {
    mockUseSelectPricingStrategies({
      pricingStrategies: mockPricingStrategyData.map((p: any) => ({
        ...p,
        id: undefined
      })),
      loading: false,
      error: false,
      totalItem: 30,
      totalItemFilter: 30
    });
    mockUseSelectPricingStrategyFilter({
      ...defaultDataFilter,
      searchFields: ['test search field']
    });
    renderResult = await renderWithMockStore(
      <PricingStrategyList
        {...ListMock}
        changeId={undefined}
        maxDataNumber={5}
      />,
      storeCofig,
      {},
      testId
    );
    const wrapper = await renderResult.findByTestId(testId);

    expect(
      wrapper.querySelector('#pricing-strategy-list-undefined')!
    ).toBeInTheDocument();
  });

  it('should render no data found', async () => {
    mockUseSelectPricingStrategies({
      pricingStrategies: mockPricingStrategyData,
      loading: false,
      error: false,
      totalItem: 0,
      totalItemFilter: 0
    });
    mockUseSelectPricingStrategyFilter(defaultDataFilter);
    renderResult = await renderWithMockStore(
      <PricingStrategyList {...ListMock} />,
      storeCofig,
      {},
      testId
    );
    const { getByText } = renderResult;
    expect(getByText('No data found')).toBeInTheDocument();
  });

  it('should render no data found with search', async () => {
    mockUseSelectPricingStrategies({
      pricingStrategies: mockPricingStrategyData,
      loading: false,
      error: false,
      totalItem: 0,
      totalItemFilter: 0
    });
    mockUseSelectPricingStrategyFilter({
      ...defaultDataFilter,
      searchValue: 'test value'
    });
    renderResult = await renderWithMockStore(
      <PricingStrategyList {...ListMock} />,
      storeCofig,
      {},
      testId
    );
    const { getByText } = renderResult;
    expect(getByText('No data found')).toBeInTheDocument();
  });

  it('should render data load unsuccessfully', async () => {
    mockUseSelectPricingStrategies({
      pricingStrategies: mockPricingStrategyData,
      loading: false,
      error: true,
      totalItem: 0,
      totalItemFilter: 0
    });
    mockUseSelectPricingStrategyFilter({
      ...defaultDataFilter
    });
    renderResult = await renderWithMockStore(
      <PricingStrategyList {...ListMock} />,
      storeCofig,
      {},
      testId
    );
    const { getByText } = renderResult;
    fireEvent.click(screen.getByText('Reload'));
    expect(getByText('Data load unsuccessful')).toBeInTheDocument();
  });
  it('should show detail on list item clicked', async () => {
    mockUseSelectPricingStrategies({
      pricingStrategies: mockPricingStrategyData,
      loading: false,
      error: false,
      totalItem: 40,
      totalItemFilter: 40
    });
    mockUseSelectPricingStrategyFilter({
      ...defaultDataFilter
    });
    renderResult = await renderWithMockStore(
      <PricingStrategyList {...ListMock} />,
      storeCofig,
      {},
      testId
    );
    const { getByText } = renderResult;
    expect(
      getByText(
        `change-pricing-strategy-view__pricingStrategiesCardView__${mockPricingStrategyData[0].id}`
      )
    ).toBeInTheDocument();
    fireEvent.click(
      getByText(
        `change-pricing-strategy-view__pricingStrategiesCardView__${mockPricingStrategyData[0].id}`
      )
    );
    expect(getByText(`Mock Pricing Detail`)).toBeInTheDocument();
    fireEvent.click(getByText('Close Detail'));
    expect(screen.queryByText(`Mock Pricing Detail`)).toBeNull();
  });
  it('filter on pricing strategy list', async () => {
    mockUseSelectPricingStrategies({
      pricingStrategies: mockPricingStrategyData,
      loading: false,
      error: false,
      totalItem: 30,
      totalItemFilter: 30
    });
    mockUseSelectPricingStrategyFilter(defaultDataFilter);
    renderResult = await renderWithMockStore(
      <PricingStrategyList {...ListMock} />,
      storeCofig,
      {},
      testId
    );

    fireEvent.click(screen.getByText('List search'));
    fireEvent.click(screen.getByText('Sort By'));
    fireEvent.click(screen.getByText('Order By'));
    fireEvent.click(screen.getByText('Clear And Reset'));
    fireEvent.click(screen.getByText('Page 2'));
    fireEvent.click(screen.getByText('Page Size 25'));
    expect(screen.getByTestId(testId)!).toBeInTheDocument();
  });
});
