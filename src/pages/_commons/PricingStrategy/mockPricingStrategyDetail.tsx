import React from 'react';

jest.mock('pages/_commons/PricingStrategy/Detail', () => {
  return {
    __esModule: true,
    default: ({ onCloseDetails }: any) => {
      return (
        <div>
          <p>Mock Pricing Detail</p>
          <button className="detail-close-btn" onClick={onCloseDetails}>
            Close Detail
          </button>
        </div>
      );
    }
  };
});
