import FailedApiReload from 'app/components/FailedApiReload';
import NoDataFound from 'app/components/NoDataFound';
import PricingStrategyCardView from 'app/components/PricingStrategyCardView';
import { PRICING_STRATEGIES_SORT_BY_LIST } from 'app/constants/constants';
import { classnames, mapVersionCreatingValue } from 'app/helpers';
import { useTranslation } from 'app/_libraries/_dls';
import isEmpty from 'lodash.isempty';
import {
  actionsPricingStrategies,
  defaultDataFilter,
  useSelectPricingStrategies,
  useSelectPricingStrategiesFilter
} from 'pages/_commons/redux/PricingStrategy';
import ButtonGroupFilter from 'pages/_commons/Utils/ButtonGroupFilter';
import Paging from 'pages/_commons/Utils/Paging';
import SortOrder from 'pages/_commons/Utils/SortOrder';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useSelectWindowDimension } from '../redux/Common';
import PricingStrategyDetail from './Detail';
import PricingStrategyHeader from './Header';

interface IProps {
  changeId?: string;
  containerClassName?: string;
  hideBreadCrumb?: boolean;
  hideHeader?: boolean;
  hideViewMode?: boolean;
  hideFilter?: boolean;
  title?: string;
  maxDataNumber?: number;
}
const PricingStrategyList: React.FC<IProps> = ({
  changeId,
  containerClassName,
  hideHeader,
  hideFilter,
  title,
  maxDataNumber
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();

  const buttons = useMemo(
    () => [
      {
        id: 'all',
        label: t('txt_all'),
        value: '',
        tooltip: ''
      },
      {
        id: 'created',
        label: t('txt_created'),
        value: 'New',
        tooltip: t('txt_pricing_strategies_created_tooltip')
      },
      {
        id: 'updated',
        label: t('txt_updated'),
        value: 'Cloned',
        tooltip: t('txt_pricing_strategies_updated_tooltip')
      },
      {
        id: 'impacted',
        label: t('txt_impacted'),
        value: 'Impacted',
        tooltip: t('txt_pricing_strategies_impacted_tooltip')
      }
    ],
    [t]
  );

  const [detailsShow, setDetailsShow] = useState<boolean>(false);

  const [details, setDetails] = useState<IPricingStrategyModel>();

  const pricingStrategies = useSelectPricingStrategies();

  const {
    loading,
    pricingStrategies: data,
    totalItem,
    totalItemFilter,
    error
  } = pricingStrategies;
  const mappedData = data.map(item => ({
    ...item,
    versionCreating: mapVersionCreatingValue(item.versionCreating)
  }));
  const filter = useSelectPricingStrategiesFilter();
  const { page, pageSize, searchValue } = filter;

  const hasFilterOnGroupBtn = !isEmpty(filter.searchFields);

  const handleChangePage = useCallback(
    (pageNumber: number) => {
      dispatch(
        actionsPricingStrategies.updateFilter({
          page: pageNumber
        })
      );
    },
    [dispatch]
  );
  const handleChangePageSize = useCallback(
    (pageSizeNumber: number) => {
      dispatch(
        actionsPricingStrategies.updateFilter({
          pageSize: pageSizeNumber
        })
      );
    },
    [dispatch]
  );

  const handleSearch = useCallback(
    (searchInputValue: string) => {
      dispatch(
        actionsPricingStrategies.updateFilter({
          searchValue: searchInputValue.trim()
        })
      );
    },
    [dispatch]
  );
  const handleSort = useCallback(
    (sortByValue: string) => {
      dispatch(
        actionsPricingStrategies.updateFilter({
          sortBy: [sortByValue]
        })
      );
    },
    [dispatch]
  );
  const handleOrder = useCallback(
    (orderByValue: string) => {
      dispatch(
        actionsPricingStrategies.updateFilter({
          orderBy: [orderByValue as OrderByType]
        })
      );
    },
    [dispatch]
  );

  const handleChangeVersionCreating = useCallback(
    (value: string) => {
      dispatch(
        actionsPricingStrategies.updateFilter({
          searchFields: value ? [value] : []
        })
      );
    },
    [dispatch]
  );

  const handleClearFilter = useCallback(() => {
    dispatch(actionsPricingStrategies.clearAndReset());
  }, [dispatch]);

  const handleApiReload = () => {
    changeId &&
      dispatch(actionsPricingStrategies.getPricingStrategies(changeId));
  };

  const onShowDetails = (item: IPricingStrategyModel) => {
    setDetailsShow(true);
    setDetails(item);
  };

  const onCloseDetails = () => {
    setDetailsShow(false);
  };

  useEffect(() => {
    changeId &&
      dispatch(actionsPricingStrategies.getPricingStrategies(changeId));
    if (maxDataNumber) {
      dispatch(
        actionsPricingStrategies.updateFilter({
          pageSize: maxDataNumber
        })
      );
    }
    return () => {
      dispatch(actionsPricingStrategies.setToDefault());
    };
  }, [dispatch, changeId, maxDataNumber]);

  const strategiesList = useMemo(
    () => (
      <div className="row">
        {mappedData.map((item: IPricingStrategyModel) => (
          <PricingStrategyCardView
            key={item.id}
            id={`pricingStrategiesCardView__${item.id}`}
            value={item}
            onClick={() => {
              onShowDetails(item);
            }}
          />
        ))}
      </div>
    ),
    [mappedData]
  );

  return (
    <div
      id={`pricing-strategy-list-${changeId}`}
      className={classnames({ 'loading mh-320': loading }, containerClassName)}
    >
      {!hideHeader && (
        <PricingStrategyHeader
          id={changeId || 'header'}
          title={title}
          searchValue={searchValue}
          onSearch={handleSearch}
          showSearch={totalItem > 0}
        />
      )}
      {!error && !loading && (
        <>
          {totalItem > 0 && (
            <>
              {!hideFilter && (
                <div className="d-flex align-items-center mt-16 pb-4">
                  {
                    <ButtonGroupFilter
                      id={changeId}
                      buttonList={buttons}
                      defaultSelectedValue={
                        filter?.searchFields ? filter.searchFields[0] : ''
                      }
                      onButtonSelectedChange={handleChangeVersionCreating}
                    />
                  }
                  {totalItemFilter > 0 && (
                    <SortOrder
                      small={
                        hasFilterOnGroupBtn || !!searchValue
                          ? width <= 1345
                          : width <= 1210
                      }
                      hasFilter={hasFilterOnGroupBtn || !!searchValue}
                      dataFilter={filter}
                      defaultDataFilter={defaultDataFilter}
                      sortByFields={PRICING_STRATEGIES_SORT_BY_LIST}
                      onChangeSortBy={handleSort}
                      onChangeOrderBy={handleOrder}
                      onClearSearch={handleClearFilter}
                    />
                  )}
                </div>
              )}
              {totalItemFilter > 0 && strategiesList}
              {!hideFilter && totalItemFilter > 0 && (
                <Paging
                  page={page}
                  pageSize={pageSize}
                  totalItem={totalItemFilter}
                  onChangePage={handleChangePage}
                  onChangePageSize={handleChangePageSize}
                />
              )}
              {detailsShow && (
                <PricingStrategyDetail
                  show={detailsShow}
                  details={details}
                  onCloseDetails={onCloseDetails}
                />
              )}
            </>
          )}

          {(totalItem === 0 || totalItemFilter === 0) && (
            <div className="d-flex flex-column justify-content-center mt-80 mb-24">
              <NoDataFound
                id="change-pricing-methods"
                title={t('txt_pricing_strategies_no_data_to_display')}
                hasSearch={!!searchValue}
                hasFilter={hasFilterOnGroupBtn}
                linkTitle={
                  (hasFilterOnGroupBtn || !!searchValue) &&
                  t('txt_clear_and_reset')
                }
                onLinkClicked={handleClearFilter}
              />
            </div>
          )}
        </>
      )}

      {!loading && error && (
        <FailedApiReload
          id="change-details--pricing-methods-tab--error"
          onReload={handleApiReload}
          className="mt-40 pt-40 d-flex flex-column justify-content-center align-items-center"
        />
      )}
    </div>
  );
};

export default PricingStrategyList;
