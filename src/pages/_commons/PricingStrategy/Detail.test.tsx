import { RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import {
  PricingStrategyData,
  PricingStrategyMapping
} from 'app/fixtures/pricing-strategy-data';
import { mappingDataFromArray } from 'app/helpers';
import { renderComponent, spyOnTranslationHoook } from 'app/utils';
import 'app/utils/_mockComponent/mockDofView';
import 'app/utils/_mockComponent/mockLoadMore';
import 'app/utils/_mockComponent/mockModalRegistry';
import React from 'react';
import PricingStrategyDetail, { IPricingStrategyDetailProps } from './Detail';

const testId = 'detail-test-id';

let mockPricingStrategyData: IPricingStrategyModel[] = [];
mockPricingStrategyData = mappingDataFromArray(
  PricingStrategyData,
  PricingStrategyMapping
)?.map(d => ({
  ...d,
  versionCreating: d.modeledFrom ? 'cloned' : 'new',
  methodsUpdated: d.methods?.length || 0
}));

export const DetailMock: IPricingStrategyDetailProps = {
  show: true,
  details: mockPricingStrategyData[1],
  onCloseDetails: () => {}
};

export const DetailMockWithImpactingMethods: IPricingStrategyDetailProps = {
  show: true,
  details: mockPricingStrategyData[2],
  onCloseDetails: () => {}
};

describe('PricingStrategyDetail', () => {
  let renderResult: RenderResult;

  beforeEach(() => {
    spyOnTranslationHoook();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render detail', async () => {
    renderResult = await renderComponent(
      testId,
      <PricingStrategyDetail {...DetailMock} />
    );
    await renderResult.findByTestId(testId);
    expect(
      document.body.querySelector('div.dls-modal-title')!.textContent
    ).toContain('txt_pricing_strategies_details');
  });

  it('should render DetailMockWithImpactingMethods', async () => {
    renderResult = await renderComponent(
      testId,
      <PricingStrategyDetail {...DetailMockWithImpactingMethods} />
    );
    await renderResult.findByTestId(testId);
    expect(
      document.body.querySelector('div.dls-modal-title')!.textContent
    ).toContain('txt_pricing_strategies_details');
  });

  it('should render empty detail', async () => {
    renderResult = await renderComponent(
      testId,
      <PricingStrategyDetail {...DetailMock} show={false} details={undefined} />
    );
    await renderResult.findByTestId(testId);
    expect(
      document.body.querySelectorAll('div.dls-modal-title').length
    ).toEqual(0);
  });

  it('load more data ', async () => {
    jest.useFakeTimers();
    DetailMock.details = {
      ...DetailMock.details,
      methodsUpdated: 50
    } as any;
    renderResult = await renderComponent(
      testId,
      <PricingStrategyDetail {...DetailMock} />
    );
    await renderResult.findByTestId(testId);
    const loadMoreBtn = document.body.querySelector('button.btn-load-more');
    userEvent.click(loadMoreBtn!);
    expect(document.body.querySelector('.load-more-cont')!.className).toContain(
      'loading'
    );
    jest.advanceTimersByTime(300);
    expect(
      document.body.querySelector('.load-more-cont')!.className
    ).not.toContain('loading');
  });
});
