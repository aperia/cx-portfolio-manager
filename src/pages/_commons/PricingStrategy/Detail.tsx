import LoadMore from 'app/components/LoadMore';
import ModalRegistry from 'app/components/ModalRegistry';
import { LOAD_MORE_PAGE_SIZE } from 'app/constants/constants';
import {
  Grid,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  SimpleBar,
  useTranslation
} from 'app/_libraries/_dls';
import { View } from 'app/_libraries/_dof/core';
import React, { useMemo, useState } from 'react';

export interface IPricingStrategyDetailProps {
  show: boolean;
  details?: IPricingStrategyModel;
  onCloseDetails: () => void;
}
const PricingStrategyDetail: React.FC<IPricingStrategyDetailProps> = ({
  show,
  details,
  onCloseDetails
}) => {
  const { id, methods = [], methodsUpdated: total } = details || {};
  const [loadingMore, setLoadingMore] = useState<boolean>(false);
  const [page, setPage] = useState<number>(1);
  const { t } = useTranslation();
  const columns = useMemo(
    () => [
      {
        id: 'method',
        Header: t('txt_pricing_strategies_service_subject_section'),
        accessor: 'serviceSubjectSection',
        width: 161
      },
      {
        id: 'previousValue',
        Header: t('txt_pricing_strategies_previous_value'),
        accessor: 'previousValue',
        width: 130
      },
      {
        id: 'newValue',
        Header: t('txt_pricing_strategies_new_value'),
        accessor: 'newValue'
      }
    ],
    [t]
  );

  const columnsImpacted = useMemo(
    () => [
      {
        id: 'serviceSubjectSection',
        Header: t('txt_pricing_strategies_service_subject_section'),
        accessor: 'serviceSubjectSection',
        width: 197
      },
      {
        id: 'method',
        Header: t('txt_method_name'),
        accessor: 'newValue'
      }
    ],
    [t]
  );

  const handleOnChangePage = () => {
    setLoadingMore(true);
    setTimeout(() => {
      setPage(page + 1);
      setLoadingMore(false);
    }, 300);
  };

  return (
    <ModalRegistry
      id={`pricing-strategy-detail-${id}`}
      rt
      enforceFocus={false}
      scrollable={false}
      show={show}
      animationDuration={500}
      animationComponentProps={{ direction: 'right' }}
      onAutoClosedAll={onCloseDetails}
    >
      <ModalHeader border closeButton onHide={onCloseDetails}>
        <ModalTitle>{t('txt_pricing_strategies_details')} </ModalTitle>
      </ModalHeader>
      <ModalBody className="p-0 overflow-auto">
        <SimpleBar>
          <div className="bg-light-l20 px-24 pt-20 pb-24 border-bottom">
            <View
              id={`change-pricing-strategy-details__${id}`}
              formKey={`change-pricing-strategy-details__${id}`}
              descriptor="change-pricing-strategy-details"
              value={details}
            />
          </div>
          <div className="p-24 h">
            <h5 className="pb-16">{t('txt_methods_updated')}</h5>
            <Grid
              columns={
                details?.impactingMethods && details.impactingMethods.length > 0
                  ? columnsImpacted
                  : columns
              }
              data={methods.slice(0, page * LOAD_MORE_PAGE_SIZE)}
            />
            {total! > LOAD_MORE_PAGE_SIZE && (
              <LoadMore
                loading={loadingMore}
                onLoadMore={handleOnChangePage}
                isEnd={page * LOAD_MORE_PAGE_SIZE >= total!}
                className="mt-24"
                endLoadMoreText={t(
                  'txt_pricing_strategies_end_of_methods_updated'
                )}
              />
            )}
          </div>
        </SimpleBar>
      </ModalBody>
      <ModalFooter border cancelButtonText="Close" onCancel={onCloseDetails} />
    </ModalRegistry>
  );
};

export default PricingStrategyDetail;
