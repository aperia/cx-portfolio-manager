import SimpleSearch from 'app/components/SimpleSearch';
import { useTranslation } from 'app/_libraries/_dls';
import isFunction from 'lodash.isfunction';
import React, { useCallback, useEffect, useRef } from 'react';

interface IProps {
  id: string;
  title?: string;
  searchValue?: string;
  showSearch?: boolean;
  onSearch?: (val: string) => void;
}
const PricingStrategyHeader: React.FC<IProps> = ({
  id,
  title,
  searchValue,
  onSearch,
  showSearch = true
}) => {
  const { t } = useTranslation();

  const simpleSearchRef = useRef<any>(null);

  const handleSearch = useCallback(
    (val: string) => {
      isFunction(onSearch) && onSearch(val);
    },
    [onSearch]
  );

  useEffect(() => {
    if (!searchValue && simpleSearchRef) {
      showSearch && simpleSearchRef.current.clear();
    }
  }, [searchValue, showSearch]);

  return (
    <div
      id={`pricing-strategy-header-${id}`}
      className="d-flex align-self-center"
    >
      <h4>{title}</h4>
      <div className="ml-auto">
        {showSearch && (
          <SimpleSearch
            ref={simpleSearchRef}
            placeholder={t('txt_pricing_strategies_search_placeholder')}
            onSearch={handleSearch}
          />
        )}
      </div>
    </div>
  );
};

export default PricingStrategyHeader;
