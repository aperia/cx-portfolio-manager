import React from 'react';

jest.mock('pages/_commons/PricingStrategy/Header', () => {
  return {
    __esModule: true,
    default: ({ id, onSearch }: any) => {
      return (
        <div id={id}>
          <p>Mock List Title</p>
          <button
            className="header-clear-btn"
            onClick={() => onSearch('test value')}
          >
            List search
          </button>
        </div>
      );
    }
  };
});
