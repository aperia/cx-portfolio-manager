import ModalRegistry from 'app/components/ModalRegistry';
import { CHANGE_DETAIL_URL, CHANGE_MANAGEMENT_URL } from 'app/constants/links';
import {
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import React from 'react';
import { useDispatch } from 'react-redux';
import { matchPath, useHistory } from 'react-router';
import { actionsChange, useShowChangeNotFoundModal } from '../redux/Change';
import { actionsWorkflowSetup, useWorkflowSetup } from '../redux/WorkflowSetup';

interface IProps {}

const ChangeNotFoundModal: React.FC<IProps> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const history = useHistory();
  const { isTemplate, workflow, onSubmitted } = useWorkflowSetup();

  const show = useShowChangeNotFoundModal();

  const handleContinue = () => {
    const isChangeDetailPage = matchPath(
      history.location.pathname,
      CHANGE_DETAIL_URL
    );
    dispatch(actionsChange.setShowChangeNotFoundModal(false));

    if (isChangeDetailPage) {
      history.push(CHANGE_MANAGEMENT_URL);
      return;
    }

    if (isTemplate) {
      dispatch(actionsWorkflowSetup.setWorkflowSetup());
      process.nextTick(() => {
        dispatch(
          actionsWorkflowSetup.setWorkflowSetup({
            isTemplate: true,
            workflow,
            onSubmitted
          })
        );
      });
    } else {
      window.location.reload();
    }
  };

  return (
    <ModalRegistry id="error_change-not-found-modal" show={show}>
      <ModalHeader border>
        <ModalTitle>{t('txt_error')}</ModalTitle>
      </ModalHeader>
      <ModalBody>
        <p>{t('txt_change_not_found_message')}</p>
      </ModalBody>
      <ModalFooter okButtonText={t('txt_continue')} onOk={handleContinue} />
    </ModalRegistry>
  );
};

export default ChangeNotFoundModal;
