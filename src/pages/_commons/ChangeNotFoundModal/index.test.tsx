import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { mockThunkAction, mockUseModalRegistryFnc } from 'app/utils';
import 'app/_libraries/_dls/test-utils/mocks/mockCanvas';
import * as ChangeSelector from 'pages/_commons/redux/Change/select-hooks';
import React from 'react';
import * as ReactRedux from 'react-redux';
import { actionsChange } from '../redux/Change';
import ChangeNotFoundModal from './';

const mockUseModalRegistry = mockUseModalRegistryFnc();
const mockActionsChange = mockThunkAction(actionsChange);

const mockDispatch = jest.spyOn(ReactRedux, 'useDispatch');
const mockShowChangeNotFoundModal = jest.spyOn(
  ChangeSelector,
  'useShowChangeNotFoundModal'
);

const mockHistory = {
  replace: jest.fn(),
  push: jest.fn(),
  location: { pathname: '/change-management/detail/:id' }
};
const mockMatchPath = jest.fn();
jest.mock('react-router', () => ({
  useHistory: () => mockHistory,
  matchPath: () => mockMatchPath(),
  useLocation: () => ({}),
  useParams: () => ({})
}));

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const mockUseWorkflowSetup = jest.fn();
jest.mock('pages/_commons/redux/WorkflowSetup', () => {
  return {
    useWorkflowSetup: () => {
      return mockUseWorkflowSetup();
    },
    actionsWorkflowSetup: {
      setWorkflowSetup: jest.fn()
    }
  };
});

describe('Test Commons > ChangeNotFoundModal', () => {
  const dispatchFn = jest.fn();
  beforeEach(() => {
    mockDispatch.mockImplementation(() => dispatchFn);
    mockUseModalRegistry.mockImplementation(() => [true, false]);
    mockActionsChange('setShowChangeNotFoundModal');
    mockShowChangeNotFoundModal.mockReturnValue(true);
    mockUseWorkflowSetup.mockReturnValue({ workflow: 'ok', isTemplate: true });
    mockMatchPath.mockReturnValue(true);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Should render ChangeNotFoundModal', () => {
    const { getByText } = render(<ChangeNotFoundModal />);

    const label_not_found = getByText('txt_change_not_found_message');

    expect(label_not_found).toBeInTheDocument();

    const btn_continue = getByText('txt_continue');
    userEvent.click(btn_continue);
    expect(btn_continue).toBeInTheDocument();
  });

  it('Should render ChangeNotFoundModal when setup workflow', () => {
    mockUseWorkflowSetup.mockReturnValue({ workflow: 'ok', isTemplate: false });
    mockMatchPath.mockReturnValue(false);
    const { getByText } = render(<ChangeNotFoundModal />);

    const label_not_found = getByText('txt_change_not_found_message');

    expect(label_not_found).toBeInTheDocument();

    const btn_continue = getByText('txt_continue');
    userEvent.click(btn_continue);
    expect(btn_continue).toBeInTheDocument();
  });

  it('Should render ChangeNotFoundModal when edit workflow', () => {
    jest.useFakeTimers();

    mockMatchPath.mockReturnValue(false);
    const { getByText } = render(<ChangeNotFoundModal />);

    const label_not_found = getByText('txt_change_not_found_message');

    expect(label_not_found).toBeInTheDocument();
    const btn_continue = getByText('txt_continue');
    userEvent.click(btn_continue);
    jest.runAllTicks();

    expect(btn_continue).toBeInTheDocument();

    jest.useRealTimers();
  });
});
