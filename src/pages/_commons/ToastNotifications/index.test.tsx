// components
import { fireEvent } from '@testing-library/react';
import { renderWithMockStore } from 'app/utils/renderWithMockStore';
import { ToastProps } from 'app/_libraries/_dls';
import ToastNotifications from 'pages/_commons/ToastNotifications';
import React from 'react';

describe('Toast Notifications', () => {
  it('render element to UI', async () => {
    const storeConfig = {
      initialState: {
        toastNotifications: {
          notifications: [
            {
              id: '123',
              show: true,
              message: 'Test toast'
            }
          ]
        }
      }
    };
    const element = <ToastNotifications {...({} as ToastProps)} />;
    const { getByText, queryByText } = await renderWithMockStore(
      element,
      storeConfig
    );
    expect(getByText('Test toast')).not.toBeEmptyDOMElement();
    const closeIcon = document.querySelector('i.icon-close');
    jest.useFakeTimers();
    fireEvent.click(closeIcon!);
    jest.advanceTimersByTime(2000);
    expect(queryByText('Test toast')).toBeNull();
  });
});
