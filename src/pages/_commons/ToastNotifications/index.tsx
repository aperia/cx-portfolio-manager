import { Toast, ToastDataType, ToastProps } from 'app/_libraries/_dls';
// helpers
import get from 'lodash.get';
import React from 'react';
// redux-store
import { useDispatch, useSelector } from 'react-redux';
// types
import { actionsToast } from './redux';

interface IProps extends Omit<ToastProps, 'onClose' | 'data'> {}
const ToastNotifications: React.FC<IProps> = ({
  delay = 5000,
  autohide = true,
  direction = 'top',
  duration = 600,
  from = 0,
  to = 92,
  ...props
}) => {
  const dispatch = useDispatch();
  const toasts = useSelector<RootState, ToastDataType[]>(
    state =>
      get(state.toastNotifications, 'notifications', []) as ToastDataType[]
  );

  const handleClose = (id: string) => {
    const timeoutId = setTimeout(() => {
      clearTimeout(timeoutId);
      id && dispatch(actionsToast.removeToast({ id }));
    }, 1000);
  };

  return (
    <Toast
      {...props}
      onClose={handleClose}
      data={toasts}
      delay={delay}
      autohide={autohide}
      direction={direction}
      duration={duration}
      from={from}
      to={to}
    />
  );
};

export default ToastNotifications;
