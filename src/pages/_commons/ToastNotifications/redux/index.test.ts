import { ToastState } from 'app/types';
import {
  actionsToast,
  reducer,
  ToastType
} from 'pages/_commons/ToastNotifications/redux';

const initReducer: ToastState = {
  notifications: []
};

describe('toast > redux-store ', () => {
  it('updateSearch', () => {
    const toastMock = {
      id: 'id',
      message: 'message'
    } as ToastType;

    const nextState = reducer(initReducer, actionsToast.addToast(toastMock));
    expect(nextState.notifications).toEqual([toastMock]);
  });

  it('clearAllToast', () => {
    const toastMock = {
      id: 'id',
      message: 'message'
    } as ToastType;

    const nextState = reducer(
      { ...initReducer, notifications: [toastMock] },
      actionsToast.clearAllToast()
    );
    expect(nextState.notifications).toEqual([]);
  });
});
