import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ToastState } from 'app/types';
import { ToastDataType } from 'app/_libraries/_dls';
import get from 'lodash.get';
import pullAllBy from 'lodash.pullallby';
import set from 'lodash.set';

export interface ToastType extends Omit<ToastDataType, 'id'> {
  id?: string;
}

const { actions, reducer } = createSlice({
  name: 'toastNotifications',
  initialState: {} as ToastState,
  reducers: {
    addToast: (draftState, action: PayloadAction<ToastType>) => {
      const { payload } = action;
      const now = new Date();
      payload.id = now.getTime().toString();
      const notifications = get(draftState, 'notifications', []);
      const newNotifications = [payload, ...notifications];
      set(draftState, 'notifications', newNotifications);
    },
    removeToast: (draftState, action: PayloadAction<Pick<ToastType, 'id'>>) => {
      const id = action.payload;
      const toasts = [...draftState.notifications];
      pullAllBy(toasts, [{ id }], 'id');
      set(draftState, 'notifications', toasts);
    },
    clearAllToast: draftState => {
      const newToasts: ToastType[] = [];
      set(draftState, 'notifications', newToasts);
    }
  }
});

const allActions = {
  ...actions
};

export { allActions as actionsToast, reducer };
