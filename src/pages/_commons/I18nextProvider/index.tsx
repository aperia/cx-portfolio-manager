import { IResource } from 'app/types';
import { I18nextProvider } from 'app/_libraries/_dls';
import React, { ReactElement, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { i18nextActions } from './redux';
import { getResource } from './redux/selector';

const I18next: React.FC = ({ children }) => {
  const dispatch = useDispatch();

  const resource = useSelector<RootState, IResource>(getResource);
  useEffect(() => {
    dispatch(
      i18nextActions.getMultipleLanguage({
        lang: window.location.pathname.includes('vi') ? 'vi' : 'en'
      })
    );
  }, [dispatch]);

  return (
    <I18nextProvider resource={resource}>
      {children as ReactElement}
    </I18nextProvider>
  );
};

export default I18next;
