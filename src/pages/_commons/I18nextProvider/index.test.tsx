import { renderComponent } from 'app/utils';
import I18next from 'pages/_commons/I18nextProvider';
import React from 'react';
import * as ReactRedux from 'react-redux';

const mockUseDispatch = jest.spyOn(ReactRedux, 'useDispatch');
const mockUseSelector = jest.spyOn(ReactRedux, 'useSelector');

const testId = 'pages/_commons/I18next';
describe('Test RAF/I18next', () => {
  it('render to UI', async () => {
    const dispatchFnc = jest.fn();
    mockUseDispatch.mockImplementation(() => dispatchFnc);
    mockUseSelector.mockImplementation(selector => (state: any) =>
      selector(state)
    );

    const renderResult = await renderComponent(testId, <I18next />);
    await renderResult.findByTestId(testId);

    expect(dispatchFnc).toHaveBeenCalled();
  });

  it('render to UI with vi', async () => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    delete window.location;

    const newLocation = new URL('https://test.aperia/vi');
    window.location = newLocation as any;

    const dispatchFnc = jest.fn();
    mockUseDispatch.mockImplementation(() => dispatchFnc);
    mockUseSelector.mockImplementation(selector => (state: any) =>
      selector(state)
    );

    const renderResult = await renderComponent(testId, <I18next />);
    await renderResult.findByTestId(testId);

    expect(dispatchFnc).toHaveBeenCalled();
  });
});
