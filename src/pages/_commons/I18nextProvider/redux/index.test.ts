import { i18nextActions, reducer } from 'pages/_commons/I18nextProvider/redux';

const initReducer = {
  lang: 'en',
  isLoading: false,
  resources: {}
};

describe('i18next > reducers', () => {
  it('changeLang', () => {
    const nextState = reducer(
      initReducer,
      i18nextActions.changeLang({
        lang: 'fr'
      })
    );
    expect(nextState.lang).toEqual('fr');
  });
});
