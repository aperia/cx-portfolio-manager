import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { mappingDataFromObj } from 'app/helpers';
import { II18nextArg, II18nextReducer, II18nextReturn } from 'app/types';

export const getMultipleLanguage = createAsyncThunk<
  II18nextReturn,
  II18nextArg,
  ThunkAPIConfig
>('i18next/getMultipleLanguage', async ({ lang = 'en' }, thunkAPI) => {
  try {
    const { i18nextService } = thunkAPI.extra;
    const { data } = await i18nextService.getMultipleLanguage(lang);
    const { mapping } = thunkAPI.getState();
    const mappingData = mappingDataFromObj(data, mapping!.data.i18next!);

    return mappingData;
  } catch (error) {
    return thunkAPI.rejectWithValue(error);
  }
});

export const getMultipleLanguageBuilder = (
  builder: ActionReducerMapBuilder<II18nextReducer>
) => {
  builder
    .addCase(getMultipleLanguage.pending, (draftState, action) => {
      draftState.isLoading = true;
    })
    .addCase(getMultipleLanguage.fulfilled, (draftState, action) => {
      const { lang, resource } = action.payload;
      draftState.resources[lang] = { ...resource };
      draftState.lang = lang;
      draftState.isLoading = false;
    })
    .addCase(getMultipleLanguage.rejected, (draftState, action) => {
      draftState.isLoading = false;
    });
};
