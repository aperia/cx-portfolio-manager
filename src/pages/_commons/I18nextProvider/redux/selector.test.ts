import { I18NextData } from 'app/fixtures/i18next-data';
import {
  getLang,
  getResource
} from 'pages/_commons/I18nextProvider/redux/selector';

describe('select-hook > i18next', () => {
  it('getResource', () => {
    const result = getResource({
      i18next: {
        resources: {
          en: I18NextData
        },
        lang: 'en'
      }
    } as any);

    expect(Object.keys(result.data).length).toEqual(2);
  });
  it('getLang', () => {
    const result = getLang({
      i18next: {
        lang: 'en'
      }
    } as any);

    expect(result).toEqual('en');
  });
});
