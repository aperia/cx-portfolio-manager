import { I18NextData, I18NextMapping } from 'app/fixtures/i18next-data';
import { II18nextReducer } from 'app/types';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import { i18nextActions, reducer } from 'pages/_commons/I18nextProvider/redux';

const initReducer: II18nextReducer = {
  lang: 'en',
  isLoading: false,
  resources: {}
};

describe('i18next > redux-store > getLanguages', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      i18nextActions.getMultipleLanguage.pending('getMultipleLanguage', {
        lang: 'en'
      })
    );
    expect(nextState.isLoading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      i18nextActions.getMultipleLanguage.fulfilled(
        [] as any,
        'getMultipleLanguage',
        { lang: 'en' }
      )
    );
    expect(nextState.isLoading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      i18nextActions.getMultipleLanguage.rejected(null, 'getMultipleLanguage', {
        lang: 'en'
      })
    );
    expect(nextState.isLoading).toEqual(false);
  });
  it('thunk action', async () => {
    const lang = 'en';
    const nextState: any = await i18nextActions.getMultipleLanguage({ lang })(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: { data: { i18next: I18NextMapping } }
        } as RootState),
      {
        i18nextService: {
          getMultipleLanguage: jest
            .fn()
            .mockResolvedValue({ data: { lang, resource: I18NextData } })
        }
      } as any
    );
    expect(nextState.payload.lang).toEqual('en');
    expect(Object.keys(nextState.payload.resource).length).toEqual(2);
  });
  it('thunk action > default lang', async () => {
    const nextState: any = await i18nextActions.getMultipleLanguage({})(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: { data: { i18next: I18NextMapping } }
        } as RootState),
      {
        i18nextService: {
          getMultipleLanguage: jest
            .fn()
            .mockResolvedValue({ data: { resource: I18NextData } })
        }
      } as any
    );
    expect(Object.keys(nextState.payload.resource).length).toEqual(2);
  });
  it('thunk action > api error', async () => {
    const lang = 'en';
    const nextState = await i18nextActions.getMultipleLanguage({ lang })(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: { data: { i18next: I18NextMapping } }
        } as RootState),
      {
        i18nextService: {
          getMultipleLanguage: jest.fn().mockRejectedValue({})
        }
      } as any
    );
    expect(nextState.payload).toEqual({});
  });
});
