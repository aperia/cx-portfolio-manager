import { PayloadAction } from '@reduxjs/toolkit';
import { NotificationSortByFields, OrderBy } from 'app/constants/enums';
import { NotificationState } from '../types';

const reducers = {
  updateNotificationListFilter: (
    draftState: NotificationState,
    action: PayloadAction<IDataFilter>
  ) => {
    const { sortBy = [], orderBy = [], page, ...filter } = action.payload;

    const customFilter: IDataFilter = {};
    const hasSort = sortBy.length > 0;
    const hasOrder = orderBy.length > 0;

    if (hasSort) {
      customFilter.sortBy = [...sortBy, ...defaultSortBy];
    }
    if (hasOrder) {
      customFilter.orderBy = [...orderBy, ...defaultOrderBy];
    }
    if (!page && !hasSort && !hasOrder) {
      customFilter.page = 1;
    }
    if (page) {
      customFilter.page = page;
    }

    draftState.notificationList.filter = {
      ...draftState.notificationList.filter,
      ...filter,
      ...customFilter
    };
  },
  clearAndResetNotificationListFilter: (draftState: NotificationState) => {
    draftState.notificationList.filter = {
      ...defaultDataFilter,
      sortBy: draftState.notificationList.filter.sortBy,
      orderBy: draftState.notificationList.filter.orderBy
    };
  },
  removeInAppNotification: (
    draftState: NotificationState,
    action: PayloadAction<string>
  ) => {
    const id = action.payload;
    draftState.notificationList = {
      ...draftState.notificationList,
      notificationsInApp: draftState.notificationList.notificationsInApp.filter(
        notification => notification.id !== id
      )
    };
  }
};

// TODO
const defaultSortBy: string[] = [];
const defaultOrderBy: OrderByType[] = [];

export const defaultDataFilter: IDataFilter = {
  page: 1,
  pageSize: 30,
  searchFields: '',
  searchValue: '',
  sortBy: [NotificationSortByFields.DATE, ...defaultSortBy],
  orderBy: [OrderBy.DESC, ...defaultOrderBy]
};

export default reducers;
