import { NotificationList } from 'app/fixtures/notification-data';
import {
  actionsNotification,
  NotificationState,
  reducer
} from 'pages/_commons/redux/Notification';
import { defaultDataFilter } from 'pages/_commons/redux/Notification/reducers';

const initReducer: NotificationState = {
  notificationList: {
    notifications: [],
    notificationsInApp: NotificationList,
    filter: defaultDataFilter
  },
  updateNotification: {
    total: 0
  }
};

describe('Notification > reducer', () => {
  it(' > updateNotificationListFilter has sort, order, page', () => {
    const nextState = reducer(
      initReducer,
      actionsNotification.updateNotificationListFilter({
        page: 2,
        sortBy: ['id'],
        orderBy: ['asc']
      })
    );
    expect(nextState.notificationList.filter.page).toEqual(2);
  });
  it(' > updateNotificationListFilter has search value', () => {
    const nextState = reducer(
      initReducer,
      actionsNotification.updateNotificationListFilter({
        searchValue: 'search'
      })
    );
    expect(nextState.notificationList.filter.searchValue).toEqual('search');
  });
  it(' > clearAndResetNotificationListFilter', () => {
    const nextState = reducer(
      initReducer,
      actionsNotification.clearAndResetNotificationListFilter()
    );
    expect(nextState.notificationList.filter.searchValue).toEqual('');
  });
  it(' > removeInAppNotification', () => {
    const removeId = '123';
    const nextState = reducer(
      initReducer,
      actionsNotification.removeInAppNotification(removeId)
    );
    expect(
      nextState.notificationList.notificationsInApp.find(n => n.id === removeId)
    ).toEqual(undefined);
  });
});
