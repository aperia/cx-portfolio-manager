export interface NotificationState extends IFetching {
  notificationList: NotificationList;
  updateNotification: UpdateNotification;
}

export interface NotificationList extends IFetching {
  notifications: INotification[];
  notificationsInApp: INotification[];
  filter: IDataFilter;
  loadingApi?: boolean;
}
export interface UpdateNotification extends IFetching {
  total: number;
}
