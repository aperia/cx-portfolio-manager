import { createSlice } from '@reduxjs/toolkit';
import {
  getNotifications,
  getNotificationsBuilder,
  markNotificationsAsRead,
  markNotificationsAsReadBuilder
} from './async-thunk';
import reducers, { defaultDataFilter } from './reducers';
import { NotificationState } from './types';

const { actions, reducer } = createSlice({
  name: 'notification',
  initialState: {
    notificationList: {
      notifications: [],
      notificationsInApp: [],
      loading: false,
      loadingApi: false,
      error: false,
      filter: defaultDataFilter
    },
    updateNotification: {
      loading: false,
      error: false,
      total: 0
    }
  } as NotificationState,
  reducers,
  extraReducers: builder => {
    getNotificationsBuilder(builder);
    markNotificationsAsReadBuilder(builder);
  }
});

const extraActions = {
  ...actions,
  getNotifications,
  markNotificationsAsRead
};

export * from './select-hooks';
export * from './types';
export { extraActions as actionsNotification, reducer };
