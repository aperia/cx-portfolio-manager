import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsNotification,
  NotificationState,
  reducer
} from 'pages/_commons/redux/Notification';
import { defaultDataFilter } from 'pages/_commons/redux/Notification/reducers';

const initReducer: NotificationState = {
  notificationList: {
    notifications: [],
    notificationsInApp: [],
    loading: true,
    error: false,
    filter: defaultDataFilter
  },
  updateNotification: {
    loading: false,
    error: false,
    total: 0
  }
};

describe('Notification > redux-store > markNotificationsAsRead', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsNotification.markNotificationsAsRead.pending(
        'markNotificationsAsRead',
        {
          notificationIds: []
        }
      )
    );
    expect(nextState.updateNotification.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsNotification.markNotificationsAsRead.fulfilled(
        0,
        'markNotificationsAsRead',
        {
          notificationIds: []
        }
      )
    );
    expect(nextState.updateNotification.loading).toEqual(false);
    expect(nextState.updateNotification.total).toEqual(0);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsNotification.markNotificationsAsRead.rejected(
        null,
        'markNotificationsAsRead',
        {
          notificationIds: []
        }
      )
    );
    expect(nextState.updateNotification.loading).toEqual(false);
  });
  it('thunk action', async () => {
    const nextState = await actionsNotification.markNotificationsAsRead({
      notificationIds: ['123']
    })(
      () => {
        return {
          dispatch: jest.fn().mockResolvedValue({
            payload: {},
            type: ''
          })
        } as any;
      },
      () =>
        ({
          ...store.getState()
        } as RootState),
      {
        notificationService: {
          updateNotification: jest
            .fn()
            .mockResolvedValue({ data: { total: 1 } })
        }
      }
    );
    expect(nextState.payload).toEqual(1);

    const noIds = await actionsNotification.markNotificationsAsRead({})(
      () => {
        return {
          dispatch: jest.fn().mockResolvedValue({
            payload: {},
            type: ''
          })
        } as any;
      },
      () =>
        ({
          ...store.getState()
        } as RootState),
      {
        notificationService: {
          updateNotification: jest
            .fn()
            .mockResolvedValue({ data: { total: 0 } })
        }
      }
    );
    expect(noIds.payload).toEqual(0);
  });
});
