import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  PayloadAction
} from '@reduxjs/toolkit';
import { CHANGE_STATUS_MAPPING } from 'app/constants/mapping';
import { NOTIFICATION_ACTIONS } from 'app/constants/notification-types';
import {
  camelToText,
  convertUTCDateToLocalDate,
  mappingDataFromArray
} from 'app/helpers';
import { APIMapping } from 'app/services';
import get from 'lodash.get';
import isEmpty from 'lodash.isempty';
import { NotificationState } from '../types';

interface IFetchNotifications {
  showLoading: boolean;
}
interface NotificationsPayload {
  notifications: INotification[];
  notificationsNew: INotification[];
}

export const getNotifications = createAsyncThunk<
  any,
  IFetchNotifications,
  ThunkAPIConfig
>('mapping/getNotifications', async (args, thunkAPI) => {
  const { notificationService } = get(thunkAPI, 'extra') as APIMapping;
  const resp = await notificationService.getNotifications();

  const state = thunkAPI.getState();
  const mappingJson = state.mapping?.data?.notification;

  const dataMapping = mappingDataFromArray(
    resp?.data?.notifications,
    mappingJson!
  )
    ?.filter(n => NOTIFICATION_ACTIONS.includes(n.action?.toLowerCase()))
    .map((n: INotification) => {
      n.date = convertUTCDateToLocalDate(new Date(n.date)).toString();
      switch (n.action?.toLowerCase()) {
        case 'changesetsubmittedforapproval':
          let teamName = n.teamName;
          if (!teamName && n.approvalRecords && n.approvalRecords?.length > 0) {
            teamName = n.approvalRecords[0]?.group?.name;
          }
          n.title = `${n.changeName} change ${camelToText(
            CHANGE_STATUS_MAPPING['PENDINGAPPROVAL']
          )} by ${teamName || ''} team.`;
          n.description = '';
          break;
        case 'changesetapproved':
          n.title = `${n.changeName} change has been approved.`;
          n.description = '';
          break;
        case 'changesetrejected':
          n.title = `${n.changeName} change has been rejected.`;
          n.description = '';
          break;
        case 'changesetlapsed':
          n.title = `Effective date on the ${n.changeName} change has lapsed.`;
          n.description = '';
          break;
        case 'changesetsubmittedforvalidation':
        case 'validationcompleted':
          n.title = `Validation for the ${n.changeName} change completed.`;
          n.description = '';
          break;
        case 'addedreminder':
          n.title = `The ${n.workflowName} needs to be set up.`;
          n.description = n.reminderDescription || '';
          break;
      }
      return n;
    });

  const notifications = dataMapping;
  const notificationsNew = notifications.filter(i => i.isNew);
  if (!isEmpty(notificationsNew)) {
    const noti = notificationsNew.map(item => ({
      Id: item.id?.toString(),
      isRead: false,
      isNew: false
    }));
    notificationService.updateNotification(noti);
  }
  return { notifications, notificationsNew };
});

export const getNotificationsBuilder = (
  builder: ActionReducerMapBuilder<NotificationState>
) => {
  builder.addCase(getNotifications.pending, (draftState, action) => {
    const { showLoading } = action.meta.arg;

    return {
      ...draftState,
      notificationList: {
        ...draftState.notificationList,
        loading: showLoading,
        loadingApi: true,
        error: false
      }
    };
  });
  builder.addCase(
    getNotifications.fulfilled,
    (draftState, action: PayloadAction<NotificationsPayload>) => {
      const { notifications, notificationsNew } = action.payload;

      const notificationsInApp: INotification[] = [
        ...draftState.notificationList.notificationsInApp,
        ...notificationsNew
      ];

      return {
        ...draftState,
        notificationList: {
          ...draftState.notificationList,
          notifications: notifications,
          notificationsInApp: notificationsInApp,
          loading: false,
          loadingApi: false
        }
      };
    }
  );
  builder.addCase(getNotifications.rejected, (draftState, action) => {
    return {
      ...draftState,
      notificationList: {
        ...draftState.notificationList,
        loading: false,
        loadingApi: false,
        error: true
      }
    };
  });
};
