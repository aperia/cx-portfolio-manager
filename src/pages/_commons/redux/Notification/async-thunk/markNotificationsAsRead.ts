import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import get from 'lodash.get';
import { NotificationState } from '../types';
import { getNotifications } from './getNotifications';

export const markNotificationsAsRead = createAsyncThunk<
  any,
  any,
  ThunkAPIConfig
>('mapping/markNotificationsAsRead', async (args: any, thunkAPI) => {
  const { notificationService } = get(thunkAPI, 'extra');
  const notifications = ((args?.notificationIds || []) as any[]).map(
    (id: string) => ({
      Id: id?.toString(),
      isRead: true,
      isNew: false
    })
  );

  const resp = await notificationService.updateNotification(notifications);
  const { dispatch } = thunkAPI;
  await Promise.resolve(
    dispatch(getNotifications({ showLoading: true }) as any)
  );
  return resp?.data?.total;
});

export const markNotificationsAsReadBuilder = (
  builder: ActionReducerMapBuilder<NotificationState>
) => {
  builder.addCase(markNotificationsAsRead.pending, (draftState, action) => {
    return {
      ...draftState,
      updateNotification: {
        ...draftState.updateNotification,
        loading: true,
        error: false
      }
    };
  });
  builder.addCase(markNotificationsAsRead.fulfilled, (draftState, action) => {
    return {
      ...draftState,
      updateNotification: {
        ...draftState.updateNotification,
        total: action.payload,
        loading: false
      }
    };
  });
  builder.addCase(markNotificationsAsRead.rejected, (draftState, action) => {
    return {
      ...draftState,
      updateNotification: {
        ...draftState.updateNotification,
        loading: false,
        error: true
      }
    };
  });
};
