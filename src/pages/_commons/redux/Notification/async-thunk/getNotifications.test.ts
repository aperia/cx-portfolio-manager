import {
  NotificationList,
  NotificationMapping
} from 'app/fixtures/notification-data';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsNotification,
  NotificationState,
  reducer
} from 'pages/_commons/redux/Notification';
import { defaultDataFilter } from 'pages/_commons/redux/Notification/reducers';

const initReducer: NotificationState = {
  notificationList: {
    notifications: [],
    notificationsInApp: [],
    loading: true,
    error: false,
    filter: defaultDataFilter
  },
  updateNotification: {
    loading: false,
    error: false,
    total: 0
  }
};

describe('Notification > redux-store > getNotifications', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsNotification.getNotifications.pending('getNotifications', {
        showLoading: true
      })
    );
    expect(nextState.notificationList.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsNotification.getNotifications.fulfilled(
        { notifications: [], notificationsNew: [] },
        'getNotifications',
        {
          showLoading: true
        }
      )
    );
    expect(nextState.notificationList.loading).toEqual(false);
    expect(nextState.notificationList.notifications).toEqual([]);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsNotification.getNotifications.rejected(null, 'getNotifications', {
        showLoading: true
      })
    );
    expect(nextState.notificationList.loading).toEqual(false);
  });
  it('thunk action has new', async () => {
    const nextState = await actionsNotification.getNotifications({
      showLoading: true
    })(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: { data: { notification: NotificationMapping } }
        } as RootState),
      {
        notificationService: {
          getNotifications: jest
            .fn()
            .mockResolvedValue({ data: { notifications: NotificationList } }),
          updateNotification: jest
            .fn()
            .mockResolvedValue({ data: { messages: [] } })
        }
      }
    );
    expect(nextState.payload.notifications.length).toEqual(8);
  });
  it('thunk action has no new', async () => {
    const nextState = await actionsNotification.getNotifications({
      showLoading: true
    })(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: { data: { notification: NotificationMapping } }
        } as RootState),
      {
        notificationService: {
          getNotifications: jest.fn().mockResolvedValue({
            data: {
              notifications: NotificationList.map(n => ({
                ...n,
                isNew: false
              }))
            }
          }),
          updateNotification: jest
            .fn()
            .mockResolvedValue({ data: { messages: [] } })
        }
      }
    );
    expect(nextState.payload.notificationsNew.length).toEqual(0);
  });
});
