import { renderHook } from '@testing-library/react-hooks';
import { NotificationList } from 'app/fixtures/notification-data';
import {
  NotificationState,
  useSelectInAppNotifications,
  useSelectNotificationFilter,
  useSelectNotificationLoading,
  useSelectNotifications,
  useSelectUnReadNotificationIds,
  useSelectUpdateNotification
} from 'pages/_commons/redux/Notification';
import * as reactRedux from 'react-redux';
import { defaultDataFilter } from '../reducers';

describe('select-hooks > notification', () => {
  let selectorMock: jest.SpyInstance;
  const mockSelectorNotifications = (notificationState: NotificationState) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector =>
        selector({ notification: notificationState })
      );
  };
  afterEach(() => {
    selectorMock.mockClear();
  });

  const pre: NotificationState = {
    notificationList: {
      notifications: NotificationList,
      notificationsInApp: NotificationList,
      filter: defaultDataFilter
    },
    updateNotification: {
      total: 0
    }
  };

  it('useSelectNotifications', () => {
    mockSelectorNotifications(pre);
    const { result } = renderHook(() => useSelectNotifications());

    expect(result.current.notifications.length).toEqual(8);
  });

  it('useSelectNotifications has Search', () => {
    const preWithSearch: NotificationState = {
      notificationList: {
        notifications: NotificationList,
        notificationsInApp: [],
        filter: {
          ...defaultDataFilter,
          searchFields: 'id',
          searchValue: '123'
        }
      },
      updateNotification: {
        total: 0
      }
    };

    mockSelectorNotifications(preWithSearch);
    const { result } = renderHook(() => useSelectNotifications());

    expect(result.current.notifications.length).toEqual(1);
  });

  it('useSelectInAppNotifications', () => {
    mockSelectorNotifications(pre);
    const { result } = renderHook(() => useSelectInAppNotifications());

    expect(result.current.length).toEqual(8);
  });

  it('useSelectNotificationFilter', () => {
    mockSelectorNotifications(pre);
    const { result } = renderHook(() => useSelectNotificationFilter());

    expect(result.current.page).toEqual(1);
  });

  it('useSelectUnReadNotificationIds', () => {
    mockSelectorNotifications(pre);
    const { result } = renderHook(() => useSelectUnReadNotificationIds());

    expect(result.current.isAllRead).toEqual(false);
  });

  it('useSelectUpdateNotification', () => {
    mockSelectorNotifications(pre);
    const { result } = renderHook(() => useSelectUpdateNotification());

    expect(result.current.total).toEqual(0);
  });

  it('useSelectNotificationLoading  => loading true', () => {
    const pre = {
      notificationList: {
        loadingApi: true
      }
    } as any;

    mockSelectorNotifications(pre);
    const { result } = renderHook(() => useSelectNotificationLoading());

    expect(result.current).toEqual(true);
  });

  it('useSelectNotificationLoading => loading undefined', () => {
    const pre = {
      notificationList: {
        loadingApi: undefined
      }
    } as any;

    mockSelectorNotifications(pre);
    const { result } = renderHook(() => useSelectNotificationLoading());

    expect(result.current).toEqual(false);
  });
});
