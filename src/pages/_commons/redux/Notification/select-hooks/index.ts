import { createSelector } from '@reduxjs/toolkit';
import { FormatTime } from 'app/constants/enums';
import { formatTimeDefault } from 'app/helpers';
import _orderBy from 'lodash.orderby';
import { useSelector } from 'react-redux';
import { UpdateNotification } from '../types';

const notificationsSelector = createSelector(
  (state: RootState) => state.notification!.notificationList,
  data => {
    const {
      notifications: rawNotifications,
      loading = false,
      error = false,
      filter: { page, pageSize, orderBy, searchFields, searchValue }
    } = data;

    let notifications = _orderBy(
      rawNotifications.map(i => ({ ...i })),
      item => {
        return formatTimeDefault(item.date, FormatTime.YearMonthDayTime);
      },
      orderBy
    );

    notifications = searchFields
      ? notifications.filter(
          (n: INotification) =>
            n[searchFields as keyof INotification]!.toString().toLowerCase() ===
            searchValue!.toLowerCase()
        )
      : notifications;

    const total = notifications.length;
    notifications = notifications.slice(0, page! * pageSize!);

    return {
      loading,
      total,
      notifications,
      error
    };
  }
);

export const useSelectNotifications = () => {
  return useSelector<
    RootState,
    {
      notifications: INotification[];
      total: number;
      loading: boolean;
      error?: boolean;
    }
  >(notificationsSelector);
};

export const useSelectNotificationLoading = () => {
  return useSelector<RootState, boolean>(
    state => state.notification!.notificationList.loadingApi || false
  );
};

const inAppNotificationsSelector = createSelector(
  (state: RootState) => state.notification!.notificationList.notificationsInApp,
  data => data
);

export const useSelectInAppNotifications = () => {
  return useSelector<RootState, INotification[]>(inAppNotificationsSelector);
};

const notificationFilterSelector = createSelector(
  (state: RootState) => state.notification!.notificationList.filter,
  data => data
);

export const useSelectNotificationFilter = () => {
  return useSelector<RootState, IDataFilter>(notificationFilterSelector);
};

const unReadNotificationIdsSelector = createSelector(
  (state: RootState) => state.notification!.notificationList,
  data => {
    const { notifications } = data;
    const unRead = notifications.filter(n => !n.isRead);
    const notificationIds = unRead.map(n => n.id);
    const hasUnRead = notificationIds && notificationIds.length > 0;
    return {
      notificationIds,
      isAllRead: !hasUnRead
    };
  }
);

export const useSelectUnReadNotificationIds = () => {
  return useSelector<
    RootState,
    {
      notificationIds: string[];
      isAllRead: boolean;
    }
  >(unReadNotificationIdsSelector);
};

const updateNotificationSelector = createSelector(
  (state: RootState) => state.notification!.updateNotification,
  data => data
);

export const useSelectUpdateNotification = () => {
  return useSelector<RootState, UpdateNotification>(updateNotificationSelector);
};
