import { createSelector } from '@reduxjs/toolkit';
import { orderBySelectedField } from 'app/helpers';
import includes from 'lodash.includes';
import isEmpty from 'lodash.isempty';
import { useSelector } from 'react-redux';

const pricingStrategiesSelector = createSelector(
  (state: RootState) => state.pricingStrategies!.pricingStrategyData,
  data => {
    const {
      pricingStrategies: rawPricingStrategies,
      loading = false,
      error = false,
      dataFilter: { searchValue, page, pageSize, sortBy, orderBy, searchFields }
    } = data;

    let pricingStrategies = orderBySelectedField(
      rawPricingStrategies,
      sortBy,
      orderBy
    ).filter(
      i =>
        i.name
          ?.toLowerCase()
          .includes(searchValue?.trim().toLowerCase() || '') ||
        i.modeledFrom
          ?.toLowerCase()
          .includes(searchValue?.trim().toLowerCase() || '')
    );

    if (!isEmpty(searchFields)) {
      const searchFieldsLowerCase = searchFields?.map((i: string) =>
        i.toLowerCase()
      );
      pricingStrategies = pricingStrategies.filter(i =>
        includes(searchFieldsLowerCase, i.versionCreating?.toLowerCase())
      );
    }

    const totalItemFilter = pricingStrategies.length;
    pricingStrategies = pricingStrategies.slice(
      (page - 1) * pageSize,
      page * pageSize
    );

    return {
      pricingStrategies,
      loading,
      error,
      totalItemFilter,
      totalItem: rawPricingStrategies.length
    };
  }
);

export const useSelectPricingStrategies = () => {
  return useSelector<
    RootState,
    {
      pricingStrategies: IPricingStrategyModel[];
      loading: boolean;
      error: boolean;
      totalItem: number;
      totalItemFilter: number;
    }
  >(pricingStrategiesSelector);
};

export const useSelectPricingStrategiesFilter = () => {
  const pricingStrategiesFilterSelector = createSelector(
    (state: RootState) =>
      state.pricingStrategies!.pricingStrategyData.dataFilter,
    data => data
  );
  return useSelector<RootState, IDataFilter>(pricingStrategiesFilterSelector);
};
