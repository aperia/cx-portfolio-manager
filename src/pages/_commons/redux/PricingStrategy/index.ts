import { createSlice } from '@reduxjs/toolkit';
import {
  getPricingStrategies,
  getPricingStrategiesBuilder
} from './async-thunk/getPricingStrategies';
import reducers from './reducers';
import { defaultDataFilter, PricingStrategiesState } from './types';

const { actions, reducer } = createSlice({
  name: 'pricingStrategies',
  initialState: {
    pricingStrategyData: {
      pricingStrategies: [],
      loading: true,
      dataFilter: defaultDataFilter
    }
  } as PricingStrategiesState,
  reducers,
  extraReducers: builder => {
    getPricingStrategiesBuilder(builder);
  }
});

const extraActions = {
  ...actions,
  getPricingStrategies
};

export * from './selector';
export * from './types';
export { extraActions as actionsPricingStrategies, reducer };
