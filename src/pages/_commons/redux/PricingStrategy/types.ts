import { FIRST_PAGE, PAGE_SIZE } from 'app/constants/constants';
import { OrderBy, PricingStrategiesSortByFields } from 'app/constants/enums';

export interface PricingStrategiesState {
  pricingStrategyData: PricingStrategyData;
}

export interface PricingStrategyData extends IFetching {
  pricingStrategies: IPricingStrategyModel[];
  dataFilter: IDataFilter;
}

export const defaultSortBy: string[] = [
  PricingStrategiesSortByFields.PRICING_STRATEGY_NAME
];
export const defaultOrderBy: OrderByType[] = [OrderBy.ASC];
export const defaultDataFilter: IDataFilter = {
  page: FIRST_PAGE,
  pageSize: PAGE_SIZE[0],
  searchValue: '',
  sortBy: [...defaultSortBy],
  orderBy: [...defaultOrderBy]
};
