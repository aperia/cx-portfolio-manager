import { PayloadAction } from '@reduxjs/toolkit';
import { defaultDataFilter, defaultOrderBy, defaultSortBy } from '../types';
import { PricingStrategiesState } from './../types';

const reducers = {
  updateFilter: (
    draftState: PricingStrategiesState,
    action: PayloadAction<IDataFilter>
  ) => {
    const { sortBy, orderBy, page, ...filter } = action.payload;

    const customFilter: IDataFilter = {};
    const hasSort = (sortBy?.length || 0) > 0;
    const hasOrder = (orderBy?.length || 0) > 0;

    if (hasSort) {
      customFilter.sortBy = [...(sortBy || []), ...defaultSortBy];
    }
    if (hasOrder) {
      customFilter.orderBy = [...(orderBy || []), ...defaultOrderBy];
    }

    if (page) {
      customFilter.page = page;
    } else {
      customFilter.page = 1;
    }

    draftState.pricingStrategyData = {
      ...draftState.pricingStrategyData,
      dataFilter: {
        ...draftState.pricingStrategyData.dataFilter,
        ...filter,
        ...customFilter
      }
    };
  },
  clearAndReset: (draftState: PricingStrategiesState) => {
    draftState.pricingStrategyData = {
      ...draftState.pricingStrategyData,
      dataFilter: {
        ...defaultDataFilter,
        sortBy: draftState.pricingStrategyData.dataFilter.sortBy,
        orderBy: draftState.pricingStrategyData.dataFilter.orderBy
      }
    };
  },
  setToDefault: (draftState: PricingStrategiesState) => {
    draftState.pricingStrategyData = {
      ...draftState.pricingStrategyData,
      pricingStrategies: [],
      loading: true,
      dataFilter: defaultDataFilter
    };
  }
};

export default reducers;
