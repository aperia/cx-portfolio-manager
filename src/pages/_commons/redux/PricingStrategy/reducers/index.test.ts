import {
  actionsPricingStrategies,
  defaultDataFilter,
  PricingStrategiesState,
  reducer
} from 'pages/_commons/redux/PricingStrategy';

const initReducer: PricingStrategiesState = {
  pricingStrategyData: {
    pricingStrategies: [],
    loading: false,
    error: false,
    dataFilter: defaultDataFilter
  }
};

describe('redux-store > pricing-strategies > reducers', () => {
  it('updateFilter', () => {
    const nextState = reducer(
      initReducer,
      actionsPricingStrategies.updateFilter({
        ...defaultDataFilter,
        sortBy: ['modeledFrom'],
        orderBy: ['desc'],
        page: 1
      })
    );
    expect(nextState.pricingStrategyData.dataFilter!.sortBy![0]).toEqual(
      'modeledFrom'
    );
    expect(nextState.pricingStrategyData.dataFilter!.orderBy![0]).toEqual(
      'desc'
    );
  });

  it('updateFilter > default sort', () => {
    const nextState = reducer(
      initReducer,
      actionsPricingStrategies.updateFilter({
        ...defaultDataFilter,
        sortBy: ['name'],
        orderBy: ['asc'],
        page: 1
      })
    );
    expect(nextState.pricingStrategyData.dataFilter!.sortBy![0]).toEqual(
      'name'
    );
    expect(nextState.pricingStrategyData.dataFilter!.orderBy![0]).toEqual(
      'asc'
    );
  });

  it('updateFilter > no sortBy, orderBy and page', () => {
    const nextState = reducer(
      initReducer,
      actionsPricingStrategies.updateFilter({})
    );
    expect(nextState.pricingStrategyData.dataFilter!.sortBy![0]).toEqual(
      'name'
    );
    expect(nextState.pricingStrategyData.dataFilter!.orderBy![0]).toEqual(
      'asc'
    );
    expect(nextState.pricingStrategyData.dataFilter.page).toEqual(1);
  });

  it('clearAndReset', () => {
    const nextState = reducer(
      initReducer,
      actionsPricingStrategies.clearAndReset()
    );
    expect(nextState.pricingStrategyData.dataFilter.page).toEqual(1);
  });

  it('setToDefault', () => {
    const nextState = reducer(
      initReducer,
      actionsPricingStrategies.setToDefault()
    );
    expect(nextState.pricingStrategyData.dataFilter.page).toEqual(1);
    expect(nextState.pricingStrategyData.pricingStrategies.length).toEqual(0);
  });
});
