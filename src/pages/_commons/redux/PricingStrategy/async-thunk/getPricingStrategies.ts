import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { mappingDataFromArray } from 'app/helpers';
import get from 'lodash.get';
import set from 'lodash.set';
import { PricingStrategiesState } from '../types';

export const getPricingStrategies = createAsyncThunk<
  any,
  string,
  ThunkAPIConfig
>('mapping/getPricingStrategies', async (changeId: string, thunkAPI) => {
  const { changeService } = get(thunkAPI, 'extra');

  const state = thunkAPI.getState();

  // Take mapping model
  const pricingStrategiesMapping = state.mapping?.data?.pricingStrategies;

  // Request API
  const resp = await changeService.getChangePricingStrategies(changeId);

  const pricingStrategiesData = resp?.data?.pricingStrategyList;

  // Mapping
  const mappedData = mappingDataFromArray(
    pricingStrategiesData,
    pricingStrategiesMapping!
  ).map(d => ({
    ...d,
    versionCreating:
      d.impactingMethods?.length > 0
        ? 'impacted'
        : d.modeledFrom && d.modeledFrom.toLowerCase() !== 'null'
        ? 'cloned'
        : 'new',
    methodsUpdated: d.methods?.length || 0,
    modeledFrom:
      d.modeledFrom?.toLowerCase() === 'null' ? undefined : d.modeledFrom
  }));

  return mappedData;
});
export const getPricingStrategiesBuilder = (
  builder: ActionReducerMapBuilder<PricingStrategiesState>
) => {
  builder.addCase(getPricingStrategies.pending, (draftState, action) => {
    set(draftState.pricingStrategyData, 'loading', true);
    set(draftState.pricingStrategyData, 'error', false);
  });
  builder.addCase(getPricingStrategies.fulfilled, (draftState, action) => {
    return {
      ...draftState,
      pricingStrategyData: {
        ...draftState.pricingStrategyData,
        pricingStrategies: action.payload,
        loading: false
      }
    };
  });
  builder.addCase(getPricingStrategies.rejected, (draftState, action) => {
    set(draftState.pricingStrategyData, 'loading', false);
    set(draftState.pricingStrategyData, 'error', true);
  });
};
