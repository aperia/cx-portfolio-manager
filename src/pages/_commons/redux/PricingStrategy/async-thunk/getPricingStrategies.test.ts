import {
  PricingStrategyData,
  PricingStrategyMapping
} from 'app/fixtures/pricing-strategy-data';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsPricingStrategies,
  defaultDataFilter,
  PricingStrategiesState,
  reducer
} from 'pages/_commons/redux/PricingStrategy';

const initReducer: PricingStrategiesState = {
  pricingStrategyData: {
    pricingStrategies: [],
    loading: false,
    error: false,
    dataFilter: defaultDataFilter
  }
};

describe('redux-store > pricing-strategies > getPricingStrategies', () => {
  const mockChangeId = '123';

  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsPricingStrategies.getPricingStrategies.pending(
        'getPricingStrategies',
        mockChangeId
      )
    );
    expect(nextState.pricingStrategyData.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsPricingStrategies.getPricingStrategies.fulfilled(
        [],
        'getPricingStrategies',
        mockChangeId
      )
    );
    expect(nextState.pricingStrategyData.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsPricingStrategies.getPricingStrategies.rejected(
        null,
        'getPricingStrategies',
        mockChangeId
      )
    );
    expect(nextState.pricingStrategyData.error).toEqual(true);
  });
  it('thunk action', async () => {
    const nextState = await actionsPricingStrategies.getPricingStrategies(
      '123'
    )(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: { data: { pricingStrategies: PricingStrategyMapping } }
        } as RootState),
      {
        changeService: {
          getChangePricingStrategies: jest.fn().mockResolvedValue({
            data: { pricingStrategyList: PricingStrategyData }
          })
        }
      }
    );
    expect(nextState.payload.length).toEqual(3);
    expect(nextState.payload[1].methods.length).toEqual(2);
  });
});
