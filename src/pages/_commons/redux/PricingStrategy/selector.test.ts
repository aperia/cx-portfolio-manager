import { renderHook } from '@testing-library/react-hooks';
import {
  PricingStrategyData,
  PricingStrategyMapping
} from 'app/fixtures/pricing-strategy-data';
import { mappingDataFromArray } from 'app/helpers';
import {
  defaultDataFilter,
  PricingStrategiesState,
  useSelectPricingStrategies,
  useSelectPricingStrategiesFilter
} from 'pages/_commons/redux/PricingStrategy';
import * as reactRedux from 'react-redux';

describe('redux-store > pricing-strategies > selector', () => {
  let mockPricingStrategiesData: any[] = [];
  let selectorMock: jest.SpyInstance;
  const mockSelectorPricingStrategies = (
    pricingStrategyState: PricingStrategiesState
  ) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector =>
        selector({ pricingStrategies: pricingStrategyState })
      );
  };
  beforeEach(() => {
    mockPricingStrategiesData = mappingDataFromArray(
      PricingStrategyData,
      PricingStrategyMapping
    )?.map(d => ({
      ...d,
      versionCreating: d.modeledFrom ? 'cloned' : 'new',
      methodsUpdated: d.methods?.length || 0
    }));
  });
  afterEach(() => {
    selectorMock && selectorMock.mockClear();
  });

  it('useSelectPricingMethods > no searchFields', () => {
    const pre = {
      pricingStrategyData: {
        pricingStrategies: mockPricingStrategiesData,
        dataFilter: { ...defaultDataFilter, searchValue: 'abcdef' }
      }
    } as any;

    mockSelectorPricingStrategies(pre);
    const { result } = renderHook(() => useSelectPricingStrategies());

    expect(result.current.totalItemFilter).toEqual(0);
  });
  it('useSelectPricingStrategies > have searchFields', () => {
    const pre = {
      pricingStrategyData: {
        pricingStrategies: mockPricingStrategiesData,
        dataFilter: {
          ...defaultDataFilter,
          searchValue: '',
          searchFields: ['new']
        }
      }
    } as any;

    mockSelectorPricingStrategies(pre);
    const { result } = renderHook(() => useSelectPricingStrategies());

    expect(result.current.totalItemFilter).toEqual(1);
  });
  it('useSelectPricingStrategiesFilter', () => {
    const pre = {
      pricingStrategyData: {
        dataFilter: {
          ...defaultDataFilter,
          searchValue: '',
          searchFields: ['cloned']
        }
      }
    } as any;

    mockSelectorPricingStrategies(pre);
    const { result } = renderHook(() => useSelectPricingStrategiesFilter());

    expect(result.current.searchFields[0]).toEqual('cloned');
  });
});
