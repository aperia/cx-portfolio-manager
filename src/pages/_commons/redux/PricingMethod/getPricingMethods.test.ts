import {
  PricingMethodData,
  PricingMethodMapping
} from 'app/fixtures/pricing-method-data';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsPricingMethods,
  defaultDataFilter,
  PricingMethodsState,
  reducer
} from 'pages/_commons/redux/PricingMethod';

const initReducer: PricingMethodsState = {
  changePricingMethods: {
    pricingMethods: [],
    loading: false,
    error: false,
    dataFilter: defaultDataFilter
  }
};

describe('redux-store > pricing-methods > getPricingMethods', () => {
  const mockChangeId = '123';

  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsPricingMethods.getPricingMethods.pending(
        'getPricingMethods',
        mockChangeId
      )
    );
    expect(nextState.changePricingMethods.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsPricingMethods.getPricingMethods.fulfilled(
        [],
        'getPricingMethods',
        mockChangeId
      )
    );
    expect(nextState.changePricingMethods.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsPricingMethods.getPricingMethods.rejected(
        null,
        'getPricingMethods',
        mockChangeId
      )
    );
    expect(nextState.changePricingMethods.error).toEqual(true);
  });
  it('thunk action', async () => {
    const nextState = await actionsPricingMethods.getPricingMethods('123')(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: { data: { pricingMethods: PricingMethodMapping } }
        } as RootState),
      {
        changeService: {
          getPricingMethods: jest.fn().mockResolvedValue({
            data: {
              pricingMethodList: [
                ...PricingMethodData,
                {
                  id: 1622679248,
                  name: 'MTIC',
                  modeledFrom: { name: 'MTIC', pricingStrategies: [{}] },
                  methodType: 'YUJZEW',
                  parameters: [
                    {
                      name: 'incentive.pricing.override',
                      previousValue: '8',
                      newValue: '7'
                    },
                    {
                      name: 'rate.override',
                      previousValue: '3',
                      newValue: '7'
                    },
                    {
                      name: 'apply.time',
                      previousValue: '3',
                      newValue: '3'
                    }
                  ]
                },
                {
                  id: 1622679249,
                  name: 'MTXXX',
                  methodType: 'type',
                  modeledFrom: { name: 'null' },
                  parameters: [
                    {
                      name: 'incentive.pricing.override',
                      previousValue: '8',
                      newValue: '7'
                    },
                    {
                      name: 'rate.override',
                      previousValue: '3',
                      newValue: '7'
                    },
                    {
                      name: 'apply.time',
                      previousValue: '3',
                      newValue: '3'
                    }
                  ]
                }
              ]
            }
          })
        }
      }
    );
    expect(nextState.payload.length).toEqual(5);
    expect(nextState.payload[2].parameters.length).toEqual(3);
  });
});
