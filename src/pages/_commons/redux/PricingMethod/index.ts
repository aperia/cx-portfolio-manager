import { createSlice } from '@reduxjs/toolkit';
import {
  getPricingMethods,
  getPricingMethodsBuilder
} from './getPricingMethods';
import reducers from './reducers';
import { defaultDataFilter, PricingMethodsState } from './types';

const { actions, reducer } = createSlice({
  name: 'pricingMethods',
  initialState: {
    changePricingMethods: {
      pricingMethods: [],
      loading: true,
      dataFilter: defaultDataFilter
    }
  } as PricingMethodsState,
  reducers,
  extraReducers: builder => {
    getPricingMethodsBuilder(builder);
  }
});

const extraActions = {
  ...actions,
  getPricingMethods
};

export * from './selector';
export * from './types';
export { extraActions as actionsPricingMethods, reducer };
