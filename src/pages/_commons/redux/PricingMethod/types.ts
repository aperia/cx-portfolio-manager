import { FIRST_PAGE, PAGE_SIZE } from 'app/constants/constants';
import { OrderBy, PricingMethodsSortByFields } from 'app/constants/enums';

export interface PricingMethodsState {
  changePricingMethods: ChangePricingMethods;
}

export interface ChangePricingMethods {
  pricingMethods: IPricingMethod[];
  loading: boolean;
  error?: boolean;
  dataFilter?: IDataFilter;
}

export const defaultSortBy: string[] = [
  PricingMethodsSortByFields.PRICING_METHOD_NAME
];
export const defaultOrderBy: OrderByType[] = [OrderBy.ASC];
export const defaultDataFilter: IDataFilter = {
  page: FIRST_PAGE,
  pageSize: PAGE_SIZE[0],
  searchValue: '',
  sortBy: [...defaultSortBy],
  orderBy: [...defaultOrderBy]
};
