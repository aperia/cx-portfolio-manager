import { createSelector } from '@reduxjs/toolkit';
import { orderBySelectedField } from 'app/helpers';
import includes from 'lodash.includes';
import isEmpty from 'lodash.isempty';
import { useSelector } from 'react-redux';

const pricingMethodsSelector = createSelector(
  (state: RootState) => state.pricingMethods!.changePricingMethods,
  data => {
    const {
      pricingMethods: rawPricingMethods,
      loading,
      error = false,
      dataFilter = {}
    } = data;

    const { searchValue, page, pageSize, sortBy, orderBy, searchFields } =
      dataFilter;
    let pricingMethods = orderBySelectedField(
      rawPricingMethods.map((m, idx) => ({ ...m, id: `${idx + 1}` })),
      sortBy!,
      orderBy!
    ).filter(
      i =>
        i.name
          ?.toLowerCase()
          .includes(searchValue?.trim().toLowerCase() || '') ||
        i.modeledFrom
          ?.toLowerCase()
          .includes(searchValue?.trim().toLowerCase() || '')
    );

    if (!isEmpty(searchFields)) {
      const searchFieldsLowerCase = searchFields.map((i: string) =>
        i?.toLowerCase()
      );
      pricingMethods = pricingMethods.filter(i =>
        includes(searchFieldsLowerCase, i.versionCreating?.toLowerCase())
      );
    }

    const totalItemFilter = pricingMethods.length;
    pricingMethods = pricingMethods.slice(
      (page! - 1) * pageSize!,
      page! * pageSize!
    );

    return {
      loading,
      error,
      totalItem: rawPricingMethods.length,
      totalItemFilter,
      pricingMethods
    };
  }
);

export const useSelectPricingMethods = () => {
  return useSelector<
    RootState,
    {
      pricingMethods: IPricingMethod[];
      loading: boolean;
      error: boolean;
      totalItem: number;
      totalItemFilter: number;
    }
  >(pricingMethodsSelector);
};

export const useSelectPricingMethodsFilter = () => {
  const pricingMethodsFilterSelector = createSelector(
    (state: RootState) =>
      state.pricingMethods!.changePricingMethods.dataFilter!,
    data => data
  );
  return useSelector<RootState, IDataFilter>(pricingMethodsFilterSelector);
};
