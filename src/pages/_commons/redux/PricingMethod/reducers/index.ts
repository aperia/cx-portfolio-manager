import { PayloadAction } from '@reduxjs/toolkit';
import { defaultDataFilter, defaultOrderBy, defaultSortBy } from '../types';
import { PricingMethodsState } from './../types';

const reducers = {
  updateFilter: (
    draftState: PricingMethodsState,
    action: PayloadAction<IDataFilter>
  ) => {
    const { sortBy, orderBy, page, ...filter } = action.payload;

    const customFilter: IDataFilter = {};
    const hasSort = (sortBy?.length || 0) > 0;
    const hasOrder = (orderBy?.length || 0) > 0;

    if (hasSort) {
      customFilter.sortBy = [...(sortBy || []), ...defaultSortBy];
    }
    if (hasOrder) {
      customFilter.orderBy = [...(orderBy || []), ...defaultOrderBy];
    }

    if (page) {
      customFilter.page = page;
    } else {
      customFilter.page = 1;
    }

    draftState.changePricingMethods = {
      ...draftState.changePricingMethods,
      dataFilter: {
        ...draftState.changePricingMethods.dataFilter,
        ...filter,
        ...customFilter
      }
    };
  },
  clearAndReset: (draftState: PricingMethodsState) => {
    draftState.changePricingMethods = {
      ...draftState.changePricingMethods,
      dataFilter: {
        ...defaultDataFilter,
        sortBy: draftState.changePricingMethods.dataFilter?.sortBy,
        orderBy: draftState.changePricingMethods.dataFilter?.orderBy
      }
    };
  },
  setToDefault: (draftState: PricingMethodsState) => {
    draftState.changePricingMethods = {
      ...draftState.changePricingMethods,
      pricingMethods: [],
      loading: true,
      dataFilter: defaultDataFilter
    };
  }
};

export default reducers;
