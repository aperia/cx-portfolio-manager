import {
  actionsPricingMethods,
  defaultDataFilter,
  PricingMethodsState,
  reducer
} from 'pages/_commons/redux/PricingMethod';

const initReducer: PricingMethodsState = {
  changePricingMethods: {
    pricingMethods: [],
    loading: false,
    error: false,
    dataFilter: defaultDataFilter
  }
};

describe('redux-store > pricing-methods > reducers', () => {
  it('updateFilter', () => {
    const nextState = reducer(
      initReducer,
      actionsPricingMethods.updateFilter({
        ...defaultDataFilter,
        sortBy: ['clientId'],
        orderBy: ['desc'],
        page: 1
      })
    );
    expect(nextState.changePricingMethods.dataFilter!.sortBy![0]).toEqual(
      'clientId'
    );
    expect(nextState.changePricingMethods.dataFilter!.orderBy![0]).toEqual(
      'desc'
    );
  });

  it('updateFilter > default sort', () => {
    const nextState = reducer(
      initReducer,
      actionsPricingMethods.updateFilter({
        ...defaultDataFilter,
        sortBy: ['name'],
        orderBy: ['asc'],
        page: 1
      })
    );
    expect(nextState.changePricingMethods.dataFilter!.sortBy![0]).toEqual(
      'name'
    );
    expect(nextState.changePricingMethods.dataFilter!.orderBy![0]).toEqual(
      'asc'
    );
  });

  it('updateFilter > no sortBy, orderBy and page', () => {
    const nextState = reducer(
      initReducer,
      actionsPricingMethods.updateFilter({})
    );
    expect(nextState.changePricingMethods.dataFilter!.sortBy![0]).toEqual(
      'name'
    );
    expect(nextState.changePricingMethods.dataFilter!.orderBy![0]).toEqual(
      'asc'
    );
    expect(nextState.changePricingMethods.dataFilter!.page).toEqual(1);
  });

  it('clearAndReset', () => {
    const nextState = reducer(
      initReducer,
      actionsPricingMethods.clearAndReset()
    );
    expect(nextState.changePricingMethods.dataFilter!.page).toEqual(1);
  });

  it('setToDefault', () => {
    const nextState = reducer(
      initReducer,
      actionsPricingMethods.setToDefault()
    );
    expect(nextState.changePricingMethods.dataFilter!.page).toEqual(1);
    expect(nextState.changePricingMethods.pricingMethods.length).toEqual(0);
  });
});
