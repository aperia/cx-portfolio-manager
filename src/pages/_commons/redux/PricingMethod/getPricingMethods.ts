import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import {
  getMethodVersionCreatingValue,
  mappingDataFromArray
} from 'app/helpers';
import get from 'lodash.get';
import set from 'lodash.set';
import { PricingMethodsState } from './types';

export const getPricingMethods = createAsyncThunk<any, string, ThunkAPIConfig>(
  'mapping/GetPricingMethods',
  async (changeId: string, thunkAPI) => {
    const { changeService } = get(thunkAPI, 'extra');

    const state = thunkAPI.getState();
    // Take mapping model
    const pricingMethodsMapping = state.mapping?.data?.pricingMethods;

    // Request API
    const resp = await changeService.getPricingMethods(changeId);
    const pricingMethodsData = resp?.data?.pricingMethodList;

    // Mapping
    const mappedData = mappingDataFromArray(
      pricingMethodsData,
      pricingMethodsMapping!
    ).map(d => {
      const pricingMethodItem = {
        ...d,
        versionCreating: getMethodVersionCreatingValue(d),
        parametersUpdated: d.parameters?.length || 0,
        modeledFrom:
          d.modeledFrom?.toLowerCase() === 'null' ? undefined : d.modeledFrom,
        newVersionOf: d.name,
        strategiesImpactedCount:
          d.modeledFrom &&
          d.modeledFrom === d.name &&
          d.strategiesImpacted?.length > 0
            ? d.strategiesImpacted.length
            : undefined
      };
      if (pricingMethodItem.strategiesImpactedCount) {
        delete pricingMethodItem.modeledFrom;
      } else {
        delete pricingMethodItem.newVersionOf;
      }
      return pricingMethodItem;
    });

    return mappedData;
  }
);
export const getPricingMethodsBuilder = (
  builder: ActionReducerMapBuilder<PricingMethodsState>
) => {
  builder.addCase(getPricingMethods.pending, (draftState, action) => {
    set(draftState.changePricingMethods, 'loading', true);
    set(draftState.changePricingMethods, 'error', false);
  });
  builder.addCase(getPricingMethods.fulfilled, (draftState, action) => {
    return {
      ...draftState,
      changePricingMethods: {
        ...draftState.changePricingMethods,
        pricingMethods: action.payload,
        loading: false
      }
    };
  });
  builder.addCase(getPricingMethods.rejected, (draftState, action) => {
    set(draftState.changePricingMethods, 'loading', false);
    set(draftState.changePricingMethods, 'error', true);
  });
};
