import { renderHook } from '@testing-library/react-hooks';
import {
  PricingMethodData,
  PricingMethodMapping
} from 'app/fixtures/pricing-method-data';
import { mappingDataFromArray } from 'app/helpers';
import {
  defaultDataFilter,
  PricingMethodsState,
  useSelectPricingMethods,
  useSelectPricingMethodsFilter
} from 'pages/_commons/redux/PricingMethod';
import * as reactRedux from 'react-redux';

describe('redux-store > pricing-methods > selector', () => {
  let mockPricingMethodData: any[] = [];
  let selectorMock: jest.SpyInstance;
  const mockSelectorPricingMethods = (
    pricingMethodState: PricingMethodsState
  ) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector =>
        selector({ pricingMethods: pricingMethodState })
      );
  };
  beforeEach(() => {
    mockPricingMethodData = mappingDataFromArray(
      PricingMethodData,
      PricingMethodMapping
    )?.map(d => ({
      ...d,
      versionCreating: d.modeledFrom ? 'cloned' : 'new',
      parametersUpdated: d.parameters?.length || 0
    }));
  });
  afterEach(() => {
    selectorMock && selectorMock.mockClear();
  });

  it('useSelectPricingMethods > no searchFields', () => {
    const pre = {
      changePricingMethods: {
        pricingMethods: mockPricingMethodData,
        dataFilter: { ...defaultDataFilter, searchValue: 'abcdef' }
      }
    } as any;

    mockSelectorPricingMethods(pre);
    const { result } = renderHook(() => useSelectPricingMethods());

    expect(result.current.totalItemFilter).toEqual(0);
  });
  it('useSelectPricingMethods > have searchFields', () => {
    const pre = {
      changePricingMethods: {
        pricingMethods: mockPricingMethodData,
        dataFilter: {
          ...defaultDataFilter,
          searchValue: '',
          searchFields: ['new']
        }
      }
    } as any;

    mockSelectorPricingMethods(pre);
    const { result } = renderHook(() => useSelectPricingMethods());

    expect(result.current.totalItem).toEqual(3);
  });
  it('useSelectPricingMethodsFilter', () => {
    const pre = {
      changePricingMethods: {
        dataFilter: {
          ...defaultDataFilter,
          searchValue: '',
          searchFields: ['cloned']
        }
      }
    } as any;

    mockSelectorPricingMethods(pre);
    const { result } = renderHook(() => useSelectPricingMethodsFilter());

    expect(result.current.searchFields[0]).toEqual('cloned');
  });
  it('pricingMethodsSelector without dataFilter', () => {
    const pre = {
      changePricingMethods: {
        pricingMethods: [],
        dataFilter: undefined
      }
    } as any;

    mockSelectorPricingMethods(pre);
    const { result } = renderHook(() => useSelectPricingMethods());

    expect(result.current.totalItem).toEqual(0);
  });
});
