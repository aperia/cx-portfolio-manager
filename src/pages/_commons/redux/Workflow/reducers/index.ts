import { PayloadAction } from '@reduxjs/toolkit';
import {
  OrderBy,
  WorkflowSection,
  WorkflowSortByFields,
  WorkflowTemplateSortByFields
} from 'app/constants/enums';
import { IWorkflowPageFilter, WorkflowState } from '../types';

const reducers = {
  updateChangeWorkflowsFilter: (
    draftState: WorkflowState,
    action: PayloadAction<IDataFilter>
  ) => {
    const { sortBy, orderBy, page, ...filter } = action.payload;

    const customFilter: IDataFilter = {};
    const hasSort = (sortBy?.length || 0) > 0;
    const hasOrder = (orderBy?.length || 0) > 0;
    if (hasSort) {
      customFilter.sortBy = [...(sortBy || []), ...defaultSortBy];
    }
    if (hasOrder) {
      customFilter.orderBy = [...(orderBy || []), ...defaultOrderBy];
    }
    if (page) {
      customFilter.page = page;
    } else {
      customFilter.page = 1;
    }

    draftState.changeWorkflows = {
      ...draftState.changeWorkflows,
      dataFilter: {
        ...draftState.changeWorkflows.dataFilter,
        ...filter,
        ...customFilter
      }
    };
  },
  clearAndResetChangeWorkflowsFilter: (draftState: WorkflowState) => {
    draftState.changeWorkflows = {
      ...draftState.changeWorkflows,
      dataFilter: {
        ...defaultDataFilter,
        sortBy: draftState.changeWorkflows.dataFilter?.sortBy,
        orderBy: draftState.changeWorkflows.dataFilter?.orderBy
      }
    };
  },
  updateInfoBarWorkflowsFilter: (
    draftState: WorkflowState,
    action: PayloadAction<IDataFilter>
  ) => {
    const { searchValue, ...filter } = action.payload;

    draftState.templateWorkflows = {
      ...draftState.templateWorkflows,
      dataFilter: {
        ...draftState.templateWorkflows.dataFilter,
        ...filter,
        searchValue
      }
    };
  },
  updatePageWorkflowsFilter: (
    draftState: WorkflowState,
    action: PayloadAction<IWorkflowPageFilter>
  ) => {
    draftState.templateWorkflows = {
      ...draftState.templateWorkflows,
      pageFilter: {
        ...draftState.templateWorkflows.pageFilter,
        ...action.payload
      }
    };
  },
  updateInProgressWorkflowsFilter: (
    draftState: WorkflowState,
    action: PayloadAction<IDataFilter>
  ) => {
    const { sortBy, orderBy, page, ...filter } = action.payload;

    const customFilter: IDataFilter = {};
    const hasSort = (sortBy?.length || 0) > 0;
    const hasOrder = (orderBy?.length || 0) > 0;
    if (hasSort) {
      customFilter.sortBy = [...(sortBy || []), ...defaultInProgressSortBy];
    }
    if (hasOrder) {
      customFilter.orderBy = [...(orderBy || []), ...defaultInProgressOrderBy];
    }
    if (page) {
      customFilter.page = page;
    } else {
      customFilter.page = 1;
    }

    draftState.workflows = {
      ...draftState.workflows,
      dataFilter: {
        ...draftState.workflows.dataFilter,
        ...filter,
        ...customFilter
      }
    };
  },
  clearAndResetInProgressWorkflowsFilter: (draftState: WorkflowState) => {
    draftState.workflows = {
      ...draftState.workflows,
      dataFilter: {
        ...defaultInprogressDataFilter,
        sortBy: draftState.workflows.dataFilter?.sortBy,
        orderBy: draftState.workflows.dataFilter?.orderBy
      }
    };
  },
  resetInProgressWorkflowsFilterToDefault: (draftState: WorkflowState) => {
    draftState.workflows = {
      ...draftState.workflows,
      dataFilter: {
        ...defaultInprogressDataFilter
      }
    };
  },
  clearAndResetInfoBarWorkflowsFilter: (draftState: WorkflowState) => {
    draftState.templateWorkflows = {
      ...draftState.templateWorkflows,
      loading: false,
      error: false,
      dataFilter: { searchValue: '' }
    };
  },
  resetPageWorkflowsFilterToDefault: (draftState: WorkflowState) => {
    draftState.templateWorkflows = {
      ...draftState.templateWorkflows,
      pageFilter: {
        ...defaultTemplateDataFilter
      },
      pageFetching: { loading: true, error: false }
    };
  },
  setToDefault: (draftState: WorkflowState) => {
    draftState.changeWorkflows = {
      ...draftState.changeWorkflows,
      workflows: [],
      loading: true,
      dataFilter: defaultDataFilter
    };
  },
  resetWorkflowDetail: (draftState: WorkflowState) => {
    draftState.changeWorkflowDetail = {
      data: {} as IWorkflowDetail,
      error: false,
      loading: false
    };
  }
};

// TODO
const defaultSortBy: string[] = [];
const defaultOrderBy: OrderByType[] = [];

export const defaultPageFilter: IWorkflowPageFilter = {
  searchValue: '',
  myWorkflowFilterValue: {
    accesible: true
  },
  favoriteWorkflowFilterValue: {
    // accesible: true,
    isFavorite: true
  },
  sortBys: {},
  orderBys: {}
};

export const defaultDataFilter: IDataFilter = {
  page: 1,
  pageSize: 10,
  searchValue: '',
  sortBy: [WorkflowSortByFields.LAST_UPDATED, ...defaultSortBy],
  orderBy: [OrderBy.DESC, ...defaultOrderBy]
};

export const defaultTemplateDataFilter: IWorkflowPageFilter = {
  page: 1,
  pageSize: 10,
  sortBy: [WorkflowTemplateSortByFields.LAST_EXECUTED, ...defaultSortBy],
  orderBy: [OrderBy.DESC, ...defaultOrderBy],
  filterValue: {},
  myWorkflowFilterValue: {},
  favoriteWorkflowFilterValue: {},
  currentSection: WorkflowSection.ALL,
  searchValues: {},
  orderBys: {},
  sortBys: {}
};

const defaultInProgressSortBy: string[] = [
  WorkflowSortByFields.LAST_UPDATED,
  WorkflowSortByFields.WORKFLOW_NAME
];
const defaultInProgressOrderBy: OrderByType[] = [OrderBy.DESC, OrderBy.ASC];
export const defaultInprogressDataFilter: IDataFilter = {
  page: 1,
  pageSize: 10,
  sortBy: [...defaultInProgressSortBy],
  orderBy: [...defaultInProgressOrderBy],
  searchValue: '',
  filterValue: ''
};

export default reducers;
