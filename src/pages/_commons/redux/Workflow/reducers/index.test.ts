import { WorkflowSortByFields } from 'app/constants/enums';
import { actionsWorkflow, reducer } from 'pages/_commons/redux/Workflow';
import {
  defaultDataFilter,
  defaultInprogressDataFilter,
  defaultTemplateDataFilter
} from 'pages/_commons/redux/Workflow/reducers';

const initReducer: any = {
  changeWorkflows: {
    loading: false,
    error: false,
    dataFilter: defaultDataFilter
  },
  templateWorkflows: {
    dataFilter: defaultDataFilter
  },
  workflows: {
    loading: false,
    error: false,
    dataFilter: defaultInprogressDataFilter
  },
  changeWorkflowDetail: {
    id: '1002'
  }
};

describe('redux-store > workflow > reducers', () => {
  it('updateChangeWorkflowsFilter', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.updateChangeWorkflowsFilter({
        ...defaultDataFilter,
        sortBy: ['status'],
        orderBy: ['desc'],
        page: 1
      })
    );
    expect(nextState.changeWorkflows.dataFilter!.sortBy![0]).toEqual('status');
    expect(nextState.changeWorkflows.dataFilter!.orderBy![0]).toEqual('desc');
  });

  it('updateChangeWorkflowsFilter > default sort', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.updateChangeWorkflowsFilter({
        ...defaultDataFilter,
        sortBy: ['updatedDate'],
        orderBy: ['desc'],
        page: 1
      })
    );
    expect(nextState.changeWorkflows.dataFilter!.sortBy![0]).toEqual(
      'updatedDate'
    );
    expect(nextState.changeWorkflows.dataFilter!.orderBy![0]).toEqual('desc');
  });

  it('updateChangeWorkflowsFilter > no sortBy, orderBy and page', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.updateChangeWorkflowsFilter({})
    );
    expect(nextState.changeWorkflows.dataFilter!.sortBy![0]).toEqual(
      'updatedDate'
    );
    expect(nextState.changeWorkflows.dataFilter!.orderBy![0]).toEqual('desc');
    expect(nextState.changeWorkflows.dataFilter!.page).toEqual(1);
  });

  it('clearAndResetChangeWorkflowsFilter', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.clearAndResetChangeWorkflowsFilter()
    );
    expect(nextState.changeWorkflows.dataFilter!.page).toEqual(1);
  });

  it('setToDefault', () => {
    const nextState = reducer(initReducer, actionsWorkflow.setToDefault());
    expect(nextState.changeWorkflows.dataFilter!.page).toEqual(1);
    expect(nextState.changeWorkflows.workflows.length).toEqual(0);
  });

  it('resetWorkflowDetail', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.resetWorkflowDetail()
    );
    expect(nextState.changeWorkflowDetail.data!.id).toEqual(undefined);
  });

  it('resetPageWorkflowsFilterToDefault', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.resetPageWorkflowsFilterToDefault()
    );
    expect(nextState.templateWorkflows.pageFilter).toEqual(
      defaultTemplateDataFilter
    );
    expect(nextState.templateWorkflows.pageFetching!.loading).toEqual(true);
    expect(nextState.templateWorkflows.pageFetching!.error).toEqual(false);
  });

  it('clearAndResetInfoBarWorkflowsFilter', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.clearAndResetInfoBarWorkflowsFilter()
    );
    expect(nextState.templateWorkflows.loading).toEqual(false);
    expect(nextState.templateWorkflows.error).toEqual(false);
    expect(nextState.templateWorkflows.dataFilter!.searchValue).toEqual('');
  });

  it('clearAndResetInProgressWorkflowsFilter', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.clearAndResetInProgressWorkflowsFilter()
    );
    expect(nextState.workflows.dataFilter!.page).toEqual(1);
  });

  it('resetInProgressWorkflowsFilterToDefault', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.resetInProgressWorkflowsFilterToDefault()
    );
    expect(nextState.workflows.dataFilter!.page).toEqual(1);
  });

  it('updateWorkflowMethodFilter', () => {
    // No sort & order
    let nextState = reducer(
      initReducer,
      actionsWorkflow.updateInProgressWorkflowsFilter({
        ...defaultDataFilter,
        sortBy: undefined,
        orderBy: undefined
      })
    );

    expect(nextState.workflows.dataFilter!.sortBy![0]).toEqual(
      WorkflowSortByFields.LAST_UPDATED
    );

    // has sort & order
    nextState = reducer(
      initReducer,
      actionsWorkflow.updateInProgressWorkflowsFilter({
        ...defaultDataFilter,
        sortBy: ['status'],
        orderBy: ['desc']
      })
    );

    expect(nextState.workflows.dataFilter!.sortBy![0]).toEqual('status');

    // no page
    nextState = reducer(
      initReducer,
      actionsWorkflow.updateInProgressWorkflowsFilter({
        ...defaultDataFilter,
        page: undefined
      })
    );

    expect(nextState.workflows.dataFilter!.page).toEqual(1);
  });

  it('updatePageWorkflowsFilter', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.updatePageWorkflowsFilter({ ...defaultDataFilter })
    );
    expect(nextState.templateWorkflows.pageFilter!.searchValue).toEqual('');
  });

  it('updateInfoBarWorkflowsFilter', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.updateInfoBarWorkflowsFilter({
        ...defaultDataFilter,
        searchValue: 'search something'
      })
    );
    expect(nextState.templateWorkflows.dataFilter!.searchValue).toEqual(
      'search something'
    );
  });
});
