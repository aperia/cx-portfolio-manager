import { createSlice } from '@reduxjs/toolkit';
import {
  downloadAttachment,
  downloadAttachmentBuilder,
  favoriteWorkflowTemplate,
  favoriteWorkflowTemplateBuilder,
  getChangeWorkflowDetails,
  getChangeWorkflowDetailsBuilder,
  getChangeWorkflows,
  getChangeWorkflowsBuilder,
  getPageTemplateWorkflows,
  getPageTemplateWorkflowsBuilder,
  getTemplateWorkflows,
  getTemplateWorkflowsBuilder,
  getWorkflows,
  getWorkflowsBuilder,
  removeWorkflow,
  removeWorkflowBuilder,
  setReminderWorkflow,
  setReminderWorkflowBuilder,
  unfavoriteWorkflowTemplate,
  unfavoriteWorkflowTemplateBuilder
} from './async-thunk';
import reducers, {
  defaultDataFilter,
  defaultInprogressDataFilter,
  defaultTemplateDataFilter
} from './reducers';
import { WorkflowState } from './types';

const { actions, reducer } = createSlice({
  name: 'workflow',
  initialState: {
    templateWorkflows: {
      workflows: [],
      loading: true,
      error: false,
      dataFilter: {
        searchValue: ''
      },
      pageFilter: defaultTemplateDataFilter,
      pageFetching: {
        loading: true,
        error: false
      },
      alreadyLoad: false
    },
    changeWorkflows: {
      workflows: [],
      loading: true,
      error: false,
      dataFilter: defaultDataFilter
    },
    changeWorkflowDetail: {
      error: false,
      loading: false
    },
    workflowRemoval: {
      error: false,
      loading: false
    },
    favoriteWorkflowTemplate: {
      error: false,
      loading: false
    },
    unfavoriteWorkflowTemplate: {
      error: false,
      loading: false
    },
    downloadAttachment: {
      attachment: {} as IDownloadAttachment,
      error: false,
      loading: false
    },
    reminderSet: { loading: false, error: false },
    workflows: {
      workflows: [],
      loading: true,
      error: false,
      dataFilter: defaultInprogressDataFilter
    }
  } as WorkflowState,
  reducers,
  extraReducers: builder => {
    getChangeWorkflowsBuilder(builder);
    getChangeWorkflowDetailsBuilder(builder);
    getTemplateWorkflowsBuilder(builder);
    getPageTemplateWorkflowsBuilder(builder);
    removeWorkflowBuilder(builder);
    favoriteWorkflowTemplateBuilder(builder);
    unfavoriteWorkflowTemplateBuilder(builder);
    downloadAttachmentBuilder(builder);
    setReminderWorkflowBuilder(builder);
    getWorkflowsBuilder(builder);
  }
});

const extraActions = {
  ...actions,
  getChangeWorkflows,
  getChangeWorkflowDetails,
  getTemplateWorkflows,
  getPageTemplateWorkflows,
  removeWorkflow,
  favoriteWorkflowTemplate,
  unfavoriteWorkflowTemplate,
  downloadAttachment,
  setReminderWorkflow,
  getWorkflows
};

export * from './select-hooks';
export * from './types';
export { extraActions as actionsWorkflow, reducer };
