import {
  WorkflowMapping,
  WorkflowTemplateData
} from 'app/fixtures/workflow-data';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import { actionsWorkflow, reducer } from 'pages/_commons/redux/Workflow';

const initReducer: any = {
  templateWorkflows: {
    workflows: [],
    pageFetching: { loading: false, error: false }
  }
};

describe('redux-store > workflow > getPageTemplateWorkflows', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.getPageTemplateWorkflows.pending(
        'getPageTemplateWorkflows',
        undefined
      )
    );
    expect(nextState.templateWorkflows.pageFetching!.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.getPageTemplateWorkflows.fulfilled(
        [] as any,
        'getPageTemplateWorkflows',
        undefined
      )
    );
    expect(nextState.templateWorkflows.pageFetching!.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.getPageTemplateWorkflows.rejected(
        null,
        'getPageTemplateWorkflows',
        undefined
      )
    );
    expect(nextState.templateWorkflows.pageFetching!.error).toEqual(true);
  });
  it('thunk action', async () => {
    const nextState: any = await actionsWorkflow.getPageTemplateWorkflows()(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: {
            data: { workflowTemplateList: WorkflowMapping.WorkflowTemplate }
          }
        } as RootState),
      {
        workflowService: {
          getTemplateWorkflows: jest.fn().mockResolvedValue({
            data: { workflowTemplateList: [WorkflowTemplateData] }
          })
        }
      }
    );
    expect(nextState.payload?.workflows.length).toEqual(1);
  });
  it('thunk action > empty data', async () => {
    const nextState: any = await actionsWorkflow.getPageTemplateWorkflows()(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: {
            data: { workflowTemplateList: WorkflowMapping.WorkflowTemplate }
          }
        } as RootState),
      {
        workflowService: {
          getTemplateWorkflows: jest.fn().mockResolvedValue({
            data: { workflowTemplateList: [] }
          })
        }
      }
    );

    expect(nextState.payload.workflows.length).toEqual(0);
  });
});
