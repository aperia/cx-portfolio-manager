import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import get from 'lodash.get';
import set from 'lodash.set';
import { WorkflowState } from '../types';

export interface IWorkflowReminder {
  workflowTemplateId: string;
  cronStartDate: Date | undefined;
  cronExpression: string;
  type: string;
  recipients: string[];
  description: string;
}

export const setReminderWorkflow = createAsyncThunk<
  boolean,
  IWorkflowReminder,
  ThunkAPIConfig
>('mapping/setReminderWorkflow', async (reminder, thunkAPI) => {
  const { workflowService } = get(thunkAPI, 'extra');
  await workflowService.setReminderWorkflow(reminder);
  return true;
});

export const setReminderWorkflowBuilder = (
  builder: ActionReducerMapBuilder<WorkflowState>
) => {
  builder.addCase(setReminderWorkflow.pending, draftState => {
    set(draftState.reminderSet, 'loading', true);
  });
  builder.addCase(setReminderWorkflow.fulfilled, draftState => {
    set(draftState.reminderSet, 'loading', false);
  });
  builder.addCase(setReminderWorkflow.rejected, draftState => {
    set(draftState.reminderSet, 'loading', false);
    set(draftState.reminderSet, 'error', true);
  });
};
