import {
  WorkflowMapping,
  WorkflowTemplateData
} from 'app/fixtures/workflow-data';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import { actionsWorkflow, reducer } from 'pages/_commons/redux/Workflow';

const initReducer: any = {
  templateWorkflows: {
    workflows: [],
    loading: false,
    error: false
  }
};

describe('redux-store > workflow > getTemplateWorkflows', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.getTemplateWorkflows.pending(
        'getTemplateWorkflows',
        undefined
      )
    );
    expect(nextState.templateWorkflows.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.getTemplateWorkflows.fulfilled(
        [] as any,
        'getTemplateWorkflows',
        undefined
      )
    );
    expect(nextState.templateWorkflows.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.getTemplateWorkflows.rejected(
        null,
        'getTemplateWorkflows',
        undefined
      )
    );
    expect(nextState.templateWorkflows.error).toEqual(true);
  });
  it('thunk action', async () => {
    const nextState: any = await actionsWorkflow.getTemplateWorkflows()(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: {
            data: { workflowTemplateList: WorkflowMapping.WorkflowTemplate }
          }
        } as RootState),
      {
        workflowService: {
          getTemplateWorkflows: jest.fn().mockResolvedValue({
            data: { workflowTemplateList: [WorkflowTemplateData] }
          })
        }
      }
    );
    expect(nextState.payload.workflows.length).toEqual(1);
  });
  it('thunk action > empty data', async () => {
    const nextState: any = await actionsWorkflow.getTemplateWorkflows()(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: {
            data: { workflowTemplateList: WorkflowMapping.WorkflowTemplate }
          }
        } as RootState),
      {
        workflowService: {
          getTemplateWorkflows: jest.fn().mockResolvedValue({
            data: { workflowTemplateList: [] }
          })
        }
      }
    );
    expect(nextState.payload.workflows.length).toEqual(0);
  });
});
