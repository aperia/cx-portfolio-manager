import {
  WorkflowAttachmentData,
  WorkflowMapping
} from 'app/fixtures/workflow-data';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import { actionsWorkflow, reducer } from 'pages/_commons/redux/Workflow';
import { WorkflowState } from '../types';

const initReducer = {
  downloadAttachment: {
    attachment: {},
    loading: false,
    error: false
  }
} as WorkflowState;

describe('redux-store > workflow > downloadAttachment', () => {
  const attachmentId = '123';
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.downloadAttachment.pending(
        'downloadAttachment',
        attachmentId
      )
    );

    expect(nextState.downloadAttachment.loading).toEqual(true);
  });

  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.downloadAttachment.fulfilled(
        [] as any,
        'downloadAttachment',
        attachmentId
      )
    );
    expect(nextState.downloadAttachment.loading).toEqual(false);
  });

  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.downloadAttachment.rejected(
        null,
        'downloadAttachment',
        attachmentId
      )
    );
    expect(nextState.downloadAttachment.error).toEqual(true);
  });

  it('thunk action', async () => {
    const nextState: any = await actionsWorkflow.downloadAttachment('21722129')(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: {
            data: {
              downloadAttachment: WorkflowMapping.WorkflowAttachment
            }
          }
        } as RootState),
      {
        changeService: {
          downloadAttachment: jest.fn().mockResolvedValue({
            data: { attachment: WorkflowAttachmentData }
          })
        }
      }
    );

    expect(nextState.payload.filename).toEqual('Employment Agreement.pdf');
  });
});
