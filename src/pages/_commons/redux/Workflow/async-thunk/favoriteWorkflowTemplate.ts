import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import get from 'lodash.get';
import set from 'lodash.set';
import { actionsToast } from 'pages/_commons/ToastNotifications/redux';
import { WorkflowState } from '../types';
import { getPageTemplateWorkflows } from './getPageTemplateWorkflows';

export const favoriteWorkflowTemplate = createAsyncThunk<
  boolean,
  string,
  ThunkAPIConfig
>('mapping/favoriteWorkflowTemplate', async (workflowId, thunkAPI) => {
  const { dispatch, rejectWithValue } = thunkAPI;
  const { workflowService } = get(thunkAPI, 'extra');
  try {
    await workflowService.favoriteWorkflowTemplate(workflowId);
    await dispatch(getPageTemplateWorkflows() as any);
    dispatch(
      actionsToast.addToast({
        type: 'success',
        message: 'Workflow favorited.'
      })
    );
  } catch {
    dispatch(
      actionsToast.addToast({
        type: 'error',
        message: 'Workflow failed to favorite.'
      })
    );
    rejectWithValue({ errorMessage: 'Workflow failed to favorite.' });
  }
  return true;
});
export const favoriteWorkflowTemplateBuilder = (
  builder: ActionReducerMapBuilder<WorkflowState>
) => {
  builder.addCase(favoriteWorkflowTemplate.pending, draftState => {
    set(draftState.favoriteWorkflowTemplate, 'loading', true);
  });
  builder.addCase(favoriteWorkflowTemplate.fulfilled, draftState => {
    set(draftState.favoriteWorkflowTemplate, 'loading', false);
  });
  builder.addCase(favoriteWorkflowTemplate.rejected, (draftState, action) => {
    set(draftState.favoriteWorkflowTemplate, 'loading', false);
    set(draftState.favoriteWorkflowTemplate, 'error', true);
  });
};
