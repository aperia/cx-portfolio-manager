import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  PayloadAction
} from '@reduxjs/toolkit';
import {
  CHANGE_TYPE_MAPPING,
  WORKFLOW_ACTION_MAPPING,
  WORKFLOW_STATUS_MAPPING
} from 'app/constants/mapping';
import { mappingDataFromObj } from 'app/helpers';
import get from 'lodash.get';
import set from 'lodash.set';
import { WorkflowState } from '../types';

export const getChangeWorkflowDetails = createAsyncThunk<
  IWorkflowDetail,
  string,
  ThunkAPIConfig
>('mapping/getChangeWorkflowDetails', async (workflowId, thunkAPI) => {
  const { workflowService } = get(thunkAPI, 'extra');
  const resp = await workflowService.getWorkflowDetails({
    searchFields: { workflowInstanceId: workflowId }
  });

  const state = thunkAPI.getState();
  const mappingJson = state.mapping?.data?.changeWorkflowDetail;
  const dataMapping = mappingDataFromObj(
    resp?.data?.workflowInstanceDetails,
    mappingJson!
  );
  if (dataMapping) {
    dataMapping.status = WORKFLOW_STATUS_MAPPING[dataMapping.status];
    dataMapping.workflowType = CHANGE_TYPE_MAPPING[dataMapping.workflowType];
    dataMapping.actions = dataMapping.actions?.map(
      (a: string) => WORKFLOW_ACTION_MAPPING[a]
    );
  }
  return dataMapping;
});
export const getChangeWorkflowDetailsBuilder = (
  builder: ActionReducerMapBuilder<WorkflowState>
) => {
  builder.addCase(getChangeWorkflowDetails.pending, draftState => {
    set(draftState.changeWorkflowDetail, 'loading', true);
  });
  builder.addCase(
    getChangeWorkflowDetails.fulfilled,
    (draftState, action: PayloadAction<IWorkflowDetail>) => {
      return {
        ...draftState,
        changeWorkflowDetail: {
          data: action.payload,
          error: false,
          loading: false
        }
      };
    }
  );
  builder.addCase(getChangeWorkflowDetails.rejected, draftState => {
    set(draftState.changeWorkflowDetail, 'loading', false);
    set(draftState.changeWorkflowDetail, 'error', true);
  });
};
