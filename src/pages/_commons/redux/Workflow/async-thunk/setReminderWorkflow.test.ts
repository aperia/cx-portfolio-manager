import store from 'app/_libraries/_dof/core/redux/createAppStore';
import { actionsWorkflow, reducer } from 'pages/_commons/redux/Workflow';
import { WorkflowState } from '../types';

const initReducer = {
  reminderSet: {
    loading: false,
    error: false
  }
} as WorkflowState;

describe('redux-store > workflow > setReminderWorkflow', () => {
  const workReminder = {
    workflowTemplateId: '',
    cronStartDate: undefined,
    cronExpression: '',
    type: '',
    recipients: [''],
    description: ''
  };
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.setReminderWorkflow.pending(
        'setReminderWorkflow',
        workReminder
      )
    );

    expect(nextState.reminderSet.loading).toEqual(true);
  });

  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.setReminderWorkflow.fulfilled(
        [] as any,
        'setReminderWorkflow',
        workReminder
      )
    );
    expect(nextState.reminderSet.loading).toEqual(false);
  });

  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.setReminderWorkflow.rejected(
        null,
        'setReminderWorkflow',
        workReminder
      )
    );
    expect(nextState.reminderSet.error).toEqual(true);
  });

  it('thunk action', async () => {
    const nextState: any = await actionsWorkflow.setReminderWorkflow(
      workReminder
    )(store.dispatch, store.getState(), {
      workflowService: {
        setReminderWorkflow: jest.fn().mockResolvedValue({
          data: {}
        })
      }
    });

    expect(nextState.payload).toEqual(true);
  });
});
