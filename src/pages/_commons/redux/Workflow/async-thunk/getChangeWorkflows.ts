import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  PayloadAction
} from '@reduxjs/toolkit';
import {
  CHANGE_TYPE_MAPPING,
  WORKFLOW_ACTION_MAPPING,
  WORKFLOW_STATUS_MAPPING
} from 'app/constants/mapping';
import { mappingDataFromArray } from 'app/helpers';
import get from 'lodash.get';
import set from 'lodash.set';
import { WorkflowState } from '../types';

interface GetChangeWorkflowArgs {
  changeId: string;
  ignoreChangeNotFoundDetect?: boolean;
}
export const getChangeWorkflows = createAsyncThunk<
  any,
  GetChangeWorkflowArgs,
  ThunkAPIConfig
>(
  'mapping/getChangeWorkflows',
  async ({ changeId, ignoreChangeNotFoundDetect = false }, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra');
    const resp = await workflowService.getWorkflowList({
      searchFields: { changeSetId: changeId },
      ignoreChangeNotFoundDetect
    });

    if (resp.isCancel) {
      return { isCancel: true };
    }

    const state = thunkAPI.getState();
    const mappingJson = state.mapping?.data?.changeWorkflow;
    const dataMapping = mappingDataFromArray(
      resp?.data?.workflowInstanceList?.filter(
        (w: any) => !w.state || w.state.toLowerCase() !== 'deleted'
      ),
      mappingJson!
    )?.map(w => ({
      ...w,
      status: WORKFLOW_STATUS_MAPPING[w.status],
      workflowType: CHANGE_TYPE_MAPPING[w.workflowType],
      actions: w.actions?.map((a: string) => WORKFLOW_ACTION_MAPPING[a])
    }));
    const deletedWorkflowIds = resp?.data?.workflowInstanceList
      ?.filter((w: any) => w.state?.toLowerCase() === 'deleted')
      .map((w: any) => w.id.toString());
    return { dataMapping, deletedWorkflowIds };
  }
);
export const getChangeWorkflowsBuilder = (
  builder: ActionReducerMapBuilder<WorkflowState>
) => {
  builder.addCase(getChangeWorkflows.pending, draftState => {
    set(draftState.changeWorkflows, 'loading', true);
  });
  builder.addCase(
    getChangeWorkflows.fulfilled,
    (draftState, action: PayloadAction<any>) => {
      if (action.payload.isCancel) {
        return draftState;
      }
      return {
        ...draftState,
        changeWorkflows: {
          ...draftState.changeWorkflows,
          workflows: action.payload.dataMapping,
          deletedWorkflowIds: action.payload.deletedWorkflowIds,
          loading: false,
          error: false
        }
      };
    }
  );
  builder.addCase(getChangeWorkflows.rejected, draftState => {
    set(draftState.changeWorkflows, 'loading', false);
    set(draftState.changeWorkflows, 'error', true);
    set(draftState.changeWorkflows, 'workflows', []);
  });
};
