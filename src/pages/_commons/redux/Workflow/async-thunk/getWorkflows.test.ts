import { WorkflowData, WorkflowMapping } from 'app/fixtures/workflow-data';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import { actionsWorkflow, reducer } from 'pages/_commons/redux/Workflow';

const initReducer: any = {
  workflows: {
    workflows: [],
    loading: false,
    error: false,
    dataFilter: []
  }
};

describe('redux-store > workflow > getWorkflows', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.getWorkflows.pending('getWorkflows', undefined)
    );
    expect(nextState.workflows.loading).toEqual(true);
  });
  it('fulfilled -> isCancel', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.getWorkflows.fulfilled(
        { isCancel: true } as any,
        'getWorkflows',
        undefined
      )
    );
    expect(nextState.workflows.loading).toEqual(false);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.getWorkflows.fulfilled(
        [] as any,
        'getWorkflows',
        undefined
      )
    );
    expect(nextState.workflows.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.getWorkflows.rejected(null, 'getWorkflows', undefined)
    );
    expect(nextState.workflows.error).toEqual(true);
  });

  it('thunk action', async () => {
    const nextState: any = await actionsWorkflow.getWorkflows()(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: {
            data: { changeWorkflow: WorkflowMapping.Workflow }
          }
        } as RootState),
      {
        workflowService: {
          getInProgressWorkflow: jest.fn().mockResolvedValue({
            data: { workflowInstanceList: WorkflowData }
          })
        }
      }
    );
    expect(nextState.payload.dataMapping.length).toEqual(1);
  });

  it('thunk action -> isCancel', async () => {
    const nextState: any = await actionsWorkflow.getWorkflows()(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: {
            data: { changeWorkflow: WorkflowMapping.Workflow }
          }
        } as RootState),
      {
        workflowService: {
          getInProgressWorkflow: jest.fn().mockResolvedValue({
            data: { workflowInstanceList: WorkflowData },
            isCancel: true
          })
        }
      }
    );

    expect(nextState.payload.isCancel).toEqual(true);
  });
});
