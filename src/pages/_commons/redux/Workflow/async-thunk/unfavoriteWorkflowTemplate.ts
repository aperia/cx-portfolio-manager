import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import get from 'lodash.get';
import set from 'lodash.set';
import { actionsToast } from 'pages/_commons/ToastNotifications/redux';
import { WorkflowState } from '../types';
import { getPageTemplateWorkflows } from './getPageTemplateWorkflows';

export const unfavoriteWorkflowTemplate = createAsyncThunk<
  boolean,
  string,
  ThunkAPIConfig
>('mapping/unfavoriteWorkflowTemplate', async (workflowId, thunkAPI) => {
  const { dispatch, rejectWithValue } = thunkAPI;
  const { workflowService } = get(thunkAPI, 'extra');
  try {
    await workflowService.unfavoriteWorkflowTemplate(workflowId);
    await dispatch(getPageTemplateWorkflows() as any);
    dispatch(
      actionsToast.addToast({
        type: 'success',
        message: 'Workflow unfavorited.'
      })
    );
  } catch {
    dispatch(
      actionsToast.addToast({
        type: 'error',
        message: 'Workflow failed to unfavorite.'
      })
    );
    rejectWithValue({ errorMessage: 'Workflow failed to unfavorite.' });
  }
  return true;
});
export const unfavoriteWorkflowTemplateBuilder = (
  builder: ActionReducerMapBuilder<WorkflowState>
) => {
  builder.addCase(unfavoriteWorkflowTemplate.pending, draftState => {
    set(draftState.unfavoriteWorkflowTemplate, 'loading', true);
  });
  builder.addCase(unfavoriteWorkflowTemplate.fulfilled, draftState => {
    set(draftState.unfavoriteWorkflowTemplate, 'loading', false);
  });
  builder.addCase(unfavoriteWorkflowTemplate.rejected, draftState => {
    set(draftState.unfavoriteWorkflowTemplate, 'loading', false);
    set(draftState.unfavoriteWorkflowTemplate, 'error', true);
  });
};
