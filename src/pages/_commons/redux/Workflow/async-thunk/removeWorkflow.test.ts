import store from 'app/_libraries/_dof/core/redux/createAppStore';
import { actionsWorkflow, reducer } from 'pages/_commons/redux/Workflow';

const initReducer: any = {
  workflowRemoval: {
    loading: false,
    error: false
  }
};

describe('redux-store > workflow > removeWorkflow', () => {
  const mockChangeId = '123';

  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.removeWorkflow.pending('removeWorkflow', mockChangeId)
    );
    expect(nextState.workflowRemoval.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.removeWorkflow.fulfilled(
        [] as any,
        'removeWorkflow',
        mockChangeId
      )
    );
    expect(nextState.workflowRemoval.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.removeWorkflow.rejected(
        null,
        'removeWorkflow',
        mockChangeId
      )
    );
    expect(nextState.workflowRemoval.error).toEqual(true);
  });
  it('thunk action', async () => {
    const nextState: any = await actionsWorkflow.removeWorkflow(mockChangeId)(
      store.dispatch,
      store.getState(),
      {
        workflowService: {
          removeWorkflow: jest.fn().mockResolvedValue({
            data: {}
          })
        }
      }
    );
    expect(nextState.payload).toEqual(true);
  });
});
