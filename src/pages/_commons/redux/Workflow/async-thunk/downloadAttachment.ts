import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { mappingDataFromObj } from 'app/helpers/mappingData';
import get from 'lodash.get';
import set from 'lodash.set';
import { WorkflowState } from '../types';

export const downloadAttachment = createAsyncThunk<any, string, ThunkAPIConfig>(
  'mapping/downloadAttachment',
  async (attachmentId, thunkAPI) => {
    const { changeService } = get(thunkAPI, 'extra');
    const state = thunkAPI.getState();
    // Take mapping model

    const downloadAttachmentMapping = state.mapping?.data?.downloadAttachment;
    const workflowInstanceId = state.workflow?.changeWorkflowDetail?.data?.id;

    const resp = await changeService.downloadAttachment(
      attachmentId,
      workflowInstanceId
    );
    const attachmentData = resp?.data?.attachment;

    // Mapping
    const mappedData = mappingDataFromObj(
      attachmentData,
      downloadAttachmentMapping!
    );

    return mappedData;
  }
);

export const downloadAttachmentBuilder = (
  builder: ActionReducerMapBuilder<WorkflowState>
) => {
  builder.addCase(downloadAttachment.pending, draftState => {
    set(draftState.downloadAttachment, 'loading', true);
  });
  builder.addCase(downloadAttachment.fulfilled, draftState => {
    set(draftState.downloadAttachment, 'loading', false);
  });
  builder.addCase(downloadAttachment.rejected, draftState => {
    set(draftState.downloadAttachment, 'loading', false);
    set(draftState.downloadAttachment, 'error', true);
  });
};
