import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { CHANGE_TYPE_MAPPING } from 'app/constants/mapping';
import { formatTime, mappingDataFromArray } from 'app/helpers';
import { APIMapping } from 'app/services';
import get from 'lodash.get';
import orderBy from 'lodash.orderby';
import set from 'lodash.set';
import { WorkflowState } from '../types';

export const getTemplateWorkflows = createAsyncThunk<
  any,
  undefined,
  ThunkAPIConfig
>('mapping/getTemplateWorkflows', async (args: undefined, thunkAPI) => {
  const { workflowService } = get(thunkAPI, 'extra') as APIMapping;
  const resp = await workflowService.getTemplateWorkflows();

  const state = thunkAPI.getState();
  // Take mapping model
  const workflowTemplateListMapping = state.mapping?.data?.workflowTemplateList;
  const rawData = resp?.data?.workflowTemplateList;
  const workflowTemplateList = orderBy(
    rawData.map((r: any) => ({
      ...r,
      lastExecutedFormat: formatTime(r.lastExecuted).date
    })),
    ['lastExecutedFormat', 'name'],
    ['desc', 'asc']
  );
  // Mapping
  const mappedData = mappingDataFromArray(
    workflowTemplateList,
    workflowTemplateListMapping!
  )?.map(w => ({
    ...w,
    type: CHANGE_TYPE_MAPPING[w.type]
  }));

  return { workflows: mappedData };
});
export const getTemplateWorkflowsBuilder = (
  builder: ActionReducerMapBuilder<WorkflowState>
) => {
  builder.addCase(getTemplateWorkflows.pending, (draftState, action) => {
    set(draftState.templateWorkflows, 'loading', true);
    set(draftState.templateWorkflows, 'error', false);
  });
  builder.addCase(getTemplateWorkflows.fulfilled, (draftState, action) => {
    return {
      ...draftState,
      templateWorkflows: {
        ...draftState.templateWorkflows,
        workflows: action.payload.workflows,
        loading: false,
        error: false,
        alreadyLoad: true
      }
    };
  });
  builder.addCase(getTemplateWorkflows.rejected, (draftState, action) => {
    set(draftState.templateWorkflows, 'loading', false);
    set(draftState.templateWorkflows, 'error', true);
  });
};
