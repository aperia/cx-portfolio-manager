import { WorkflowData, WorkflowMapping } from 'app/fixtures/workflow-data';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import { actionsWorkflow, reducer } from 'pages/_commons/redux/Workflow';
import { defaultDataFilter } from 'pages/_commons/redux/Workflow/reducers';

const initReducer: any = {
  changeWorkflows: {
    workflows: {},
    loading: false,
    error: false,
    dataFilter: defaultDataFilter
  }
};

describe('redux-store > workflow > getChangeWorkflows', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.getChangeWorkflows.pending('getChangeWorkflows', '')
    );
    expect(nextState.changeWorkflows.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.getChangeWorkflows.fulfilled(
        [] as any,
        'getChangeWorkflows',
        ''
      )
    );
    expect(nextState.changeWorkflows.loading).toEqual(false);
  });
  it('fulfilled -> isCancel', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.getChangeWorkflows.fulfilled(
        { isCancel: true } as any,
        'getChangeWorkflows',
        ''
      )
    );
    expect(nextState.changeWorkflows.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.getChangeWorkflows.rejected(
        null,
        'getChangeWorkflows',
        ''
      )
    );
    expect(nextState.changeWorkflows.error).toEqual(true);
  });
  it('thunk action', async () => {
    const changeId = '10000001';
    const nextState: any = await actionsWorkflow.getChangeWorkflows(changeId)(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: { data: { changeWorkflow: WorkflowMapping.Workflow } }
        } as RootState),
      {
        workflowService: {
          getWorkflowList: jest.fn().mockResolvedValue({
            data: { workflowInstanceList: WorkflowData }
          })
        }
      }
    );
    expect(nextState.payload.dataMapping.length).toEqual(1);
    expect(nextState.payload.dataMapping[0].name).toEqual(
      'Create Change in Terms Event'
    );
  });

  it('thunk action -> isCancel', async () => {
    const changeId = '10000001';
    const nextState: any = await actionsWorkflow.getChangeWorkflows(changeId)(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: { data: { changeWorkflow: WorkflowMapping.Workflow } }
        } as RootState),
      {
        workflowService: {
          getWorkflowList: jest.fn().mockResolvedValue({
            data: { workflowInstanceList: WorkflowData },
            isCancel: true
          })
        }
      }
    );

    expect(nextState.payload.isCancel).toEqual(true);
  });
});
