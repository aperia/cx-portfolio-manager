import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import get from 'lodash.get';
import set from 'lodash.set';
import { WorkflowState } from '../types';

export const removeWorkflow = createAsyncThunk<boolean, string, ThunkAPIConfig>(
  'mapping/removeWorkflow',
  async (workflowId, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra');
    await workflowService.removeWorkflow(workflowId);
    return true;
  }
);
export const removeWorkflowBuilder = (
  builder: ActionReducerMapBuilder<WorkflowState>
) => {
  builder.addCase(removeWorkflow.pending, draftState => {
    set(draftState.workflowRemoval, 'loading', true);
  });
  builder.addCase(removeWorkflow.fulfilled, draftState => {
    set(draftState.workflowRemoval, 'loading', false);
  });
  builder.addCase(removeWorkflow.rejected, draftState => {
    set(draftState.workflowRemoval, 'loading', false);
    set(draftState.workflowRemoval, 'error', true);
  });
};
