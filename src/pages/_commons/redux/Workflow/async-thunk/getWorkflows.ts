import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  PayloadAction
} from '@reduxjs/toolkit';
import {
  CHANGE_TYPE_MAPPING,
  WORKFLOW_ACTION_MAPPING,
  WORKFLOW_STATUS_MAPPING
} from 'app/constants/mapping';
import { mappingDataFromArray } from 'app/helpers';
import { APIMapping } from 'app/services';
import get from 'lodash.get';
import set from 'lodash.set';
import { WorkflowState } from '../types';

export const getWorkflows = createAsyncThunk<any, undefined, ThunkAPIConfig>(
  'mapping/getWorkflows',
  async (args, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;
    const resp = await workflowService.getInProgressWorkflow();

    if ((resp as any).isCancel) {
      return { isCancel: true };
    }

    const state = thunkAPI.getState();
    const mappingJson = state.mapping?.data?.changeWorkflow;
    const dataMapping = mappingDataFromArray(
      resp?.data?.workflowInstanceList?.filter(
        (w: any) => !w.state || w.state.toLowerCase() !== 'deleted'
      ),
      mappingJson!
    )?.map(w => ({
      ...w,
      status: WORKFLOW_STATUS_MAPPING[w.status],
      workflowType: CHANGE_TYPE_MAPPING[w.workflowType],
      actions: w.actions?.map((a: string) => WORKFLOW_ACTION_MAPPING[a]),
      changeDetail: {
        changeSetId: w.changeId,
        changSetName: w.changeName
      }
    }));

    return { dataMapping };
  }
);
export const getWorkflowsBuilder = (
  builder: ActionReducerMapBuilder<WorkflowState>
) => {
  builder.addCase(getWorkflows.pending, draftState => {
    set(draftState.workflows, 'loading', true);
  });
  builder.addCase(
    getWorkflows.fulfilled,
    (draftState, action: PayloadAction<any>) => {
      if (action.payload.isCancel) {
        return draftState;
      }
      return {
        ...draftState,
        workflows: {
          ...draftState.workflows,
          workflows: action.payload.dataMapping,
          loading: false,
          error: false
        }
      };
    }
  );
  builder.addCase(getWorkflows.rejected, draftState => {
    set(draftState.workflows, 'loading', false);
    set(draftState.workflows, 'error', true);
  });
};
