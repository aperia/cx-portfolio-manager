import {
  WorkflowDetailData,
  WorkflowMapping
} from 'app/fixtures/workflow-data';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import { actionsWorkflow, reducer } from 'pages/_commons/redux/Workflow';
import { WorkflowState } from '../types';

const initReducer = {
  changeWorkflowDetail: {
    data: {},
    loading: false,
    error: false
  }
} as WorkflowState;

describe('redux-store > workflow > getChangeWorkflowDetails', () => {
  const mockChangeId = '123';

  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.getChangeWorkflowDetails.pending(
        'getChangeWorkflowDetails',
        mockChangeId
      )
    );
    expect(nextState.changeWorkflowDetail.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.getChangeWorkflowDetails.fulfilled(
        [] as any,
        'getChangeWorkflowDetails',
        mockChangeId
      )
    );
    expect(nextState.changeWorkflowDetail.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.getChangeWorkflowDetails.rejected(
        null,
        'getChangeWorkflowDetails',
        mockChangeId
      )
    );
    expect(nextState.changeWorkflowDetail.error).toEqual(true);
  });
  it('thunk action', async () => {
    const nextState: any = await actionsWorkflow.getChangeWorkflowDetails(
      '123'
    )(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: {
            data: { changeWorkflowDetail: WorkflowMapping.WorkflowDetail }
          }
        } as RootState),
      {
        workflowService: {
          getWorkflowDetails: jest.fn().mockResolvedValue({
            data: { workflowInstanceDetails: WorkflowDetailData }
          })
        }
      }
    );
    expect(nextState.payload.name).toEqual('Prime Interest Rate Change');
    expect(nextState.payload.uploadedAttachment.length).toEqual(1);
  });
});
