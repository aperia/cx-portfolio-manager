import { mockThunkAction } from 'app/utils';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import { actionsWorkflow, reducer } from 'pages/_commons/redux/Workflow';
import { WorkflowState } from '../types';
import * as GetPageWorkflowTemplateAction from './getPageTemplateWorkflows';

const initReducer = {
  favoriteWorkflowTemplate: {
    loading: false,
    error: false
  }
} as WorkflowState;

const GetPageWorkflowTemplateActionMock = mockThunkAction(
  GetPageWorkflowTemplateAction
);

describe('redux-store > workflow > favoriteWorkflowTemplate', () => {
  beforeEach(() => {
    GetPageWorkflowTemplateActionMock('getPageTemplateWorkflows');
  });

  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.favoriteWorkflowTemplate.pending(
        'favoriteWorkflowTemplate',
        ''
      )
    );

    expect(nextState.favoriteWorkflowTemplate.loading).toEqual(true);
  });

  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.favoriteWorkflowTemplate.fulfilled(
        [] as any,
        'favoriteWorkflowTemplate',
        ''
      )
    );
    expect(nextState.favoriteWorkflowTemplate.loading).toEqual(false);
  });

  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.favoriteWorkflowTemplate.rejected(
        null,
        'favoriteWorkflowTemplate',
        ''
      )
    );
    expect(nextState.favoriteWorkflowTemplate.error).toEqual(true);
  });

  it('thunk action', async () => {
    const workflowId = '123';
    const nextState: any = await actionsWorkflow.favoriteWorkflowTemplate(
      workflowId
    )(store.dispatch, store.getState(), {
      workflowService: {
        favoriteWorkflowTemplate: jest.fn().mockResolvedValue({
          data: {}
        })
      }
    });

    expect(nextState.payload).toEqual(true);
  });

  it('thunk action with error', async () => {
    GetPageWorkflowTemplateActionMock(
      'getPageTemplateWorkflows'
    ).mockReturnValue({});

    const workflowId = '123';
    const nextState: any = await actionsWorkflow.favoriteWorkflowTemplate(
      workflowId
    )(store.dispatch, store.getState(), {
      workflowService: {
        favoriteWorkflowTemplate: jest.fn().mockResolvedValue({
          data: {}
        })
      }
    });

    expect(nextState.payload).toEqual(true);
  });
});
