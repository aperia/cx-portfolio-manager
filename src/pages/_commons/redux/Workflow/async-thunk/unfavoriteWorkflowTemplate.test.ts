import { mockThunkAction } from 'app/utils';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import { actionsWorkflow, reducer } from 'pages/_commons/redux/Workflow';
import { WorkflowState } from '../types';
import * as GetPageWorkflowTemplateAction from './getPageTemplateWorkflows';

const initReducer = {
  unfavoriteWorkflowTemplate: {
    loading: false,
    error: false
  }
} as WorkflowState;

const GetPageWorkflowTemplateActionMock = mockThunkAction(
  GetPageWorkflowTemplateAction
);

describe('redux-store > workflow > unfavoriteWorkflowTemplate', () => {
  beforeEach(() => {
    GetPageWorkflowTemplateActionMock('getPageTemplateWorkflows');
  });

  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.unfavoriteWorkflowTemplate.pending(
        'unfavoriteWorkflowTemplate',
        ''
      )
    );

    expect(nextState.unfavoriteWorkflowTemplate.loading).toEqual(true);
  });

  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.unfavoriteWorkflowTemplate.fulfilled(
        [] as any,
        'unfavoriteWorkflowTemplate',
        ''
      )
    );
    expect(nextState.unfavoriteWorkflowTemplate.loading).toEqual(false);
  });

  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflow.unfavoriteWorkflowTemplate.rejected(
        null,
        'unfavoriteWorkflowTemplate',
        ''
      )
    );
    expect(nextState.unfavoriteWorkflowTemplate.error).toEqual(true);
  });

  it('thunk action', async () => {
    const nextState: any = await actionsWorkflow.unfavoriteWorkflowTemplate(
      '1000'
    )(store.dispatch, store.getState(), {
      workflowService: {
        unfavoriteWorkflowTemplate: jest.fn().mockResolvedValue({
          data: {}
        })
      }
    });

    expect(nextState.payload).toEqual(true);
  });

  it('thunk action with error', async () => {
    GetPageWorkflowTemplateActionMock(
      'getPageTemplateWorkflows'
    ).mockReturnValue({});

    const nextState: any = await actionsWorkflow.unfavoriteWorkflowTemplate(
      '1000'
    )(store.dispatch, store.getState(), {
      workflowService: {
        unfavoriteWorkflowTemplate: jest.fn().mockResolvedValue({
          data: {}
        })
      }
    });

    expect(nextState.payload).toEqual(true);
  });
});
