import { WorkflowSection } from 'app/constants/enums';

export interface WorkflowState {
  templateWorkflows: TemplateWorkflowState;
  changeWorkflows: ChangeWorkflows;
  changeWorkflowDetail: ChangeWorkflowDetail;
  workflowRemoval: WorkflowRemoval;
  favoriteWorkflowTemplate: FavoriteWorkflow;
  unfavoriteWorkflowTemplate: UnfavoriteWorkflow;
  downloadAttachment: DownloadAttachment;
  reminderSet: ReminderSet;
  workflows: WorkflowsListState;
}

export interface ReminderSet {
  loading: boolean;
  error: boolean;
}

export interface WorkflowRemoval {
  loading: boolean;
  error: boolean;
}

export interface ChangeWorkflowDetail {
  data?: IWorkflowDetail;
  loading: boolean;
  error: boolean;
}

export interface ChangeWorkflows {
  workflows: IWorkflowModel[];
  deletedWorkflowIds?: string[];
  loading: boolean;
  error: boolean;
  dataFilter?: IDataFilter;
}

export interface WorkflowsListState {
  workflows: IWorkflowModel[];
  loading: boolean;
  error: boolean;
  dataFilter?: IDataFilter;
}

export interface TemplateWorkflows {
  workflows: IWorkflowTemplateModel[];
}

export interface IWorkflowPageFilter extends IDataFilter {
  currentSection?: WorkflowSection;
  myWorkflowFilterValue?: any;
  favoriteWorkflowFilterValue?: any;
  searchValues?: Record<string, string>;
  sortBys?: Record<string, string[]>;
  orderBys?: Record<string, OrderByType[]>;
}

export interface TemplateWorkflowState extends TemplateWorkflows {
  alreadyLoad?: boolean;
  loading: boolean;
  error?: boolean;
  dataFilter?: IDataFilter;
  pageFilter?: IWorkflowPageFilter;
  pageFetching?: IFetching;
}

export interface WorkflowTemplateType {
  id: string;
  label: string;
  count?: number;
  title?: string;
  description?: string;
}

export interface FavoriteWorkflow extends IFetching {}
export interface UnfavoriteWorkflow extends IFetching {}
export interface DownloadAttachment extends IFetching {
  attachment: IDownloadAttachment;
}
