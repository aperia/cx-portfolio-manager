import { createSelector } from '@reduxjs/toolkit';
import { FormatTime, OrderBy, WorkflowSection } from 'app/constants/enums';
import { camelToText, formatTimeDefault } from 'app/helpers';
import { matchSearchValue } from 'app/helpers/matchSearchValue';
import { ChangeWorkflows } from 'app/types';
import { isString, orderBy } from 'lodash';
import _orderBy from 'lodash.orderby';
import { useSelector } from 'react-redux';
import {
  ChangeWorkflowDetail,
  FavoriteWorkflow,
  IWorkflowPageFilter,
  TemplateWorkflowState,
  UnfavoriteWorkflow,
  WorkflowRemoval,
  WorkflowTemplateType
} from '../types';

const lastUpdatedFavoriteWorkflowsSelector = createSelector(
  (state: RootState) => state.workflow!.templateWorkflows,
  data => {
    const { workflows: rawData, pageFetching } = data;
    const { loading, error } = pageFetching!;
    const workflows = rawData.filter(
      (w: IWorkflowTemplateModel) => w.isFavorite && w.accesible
    );
    return {
      workflows: workflows.slice(0, 5),
      loading,
      error,
      total: workflows.length
    };
  }
);
export const useSelectLastUpdatedFavoriteWorkflows = () => {
  return useSelector<
    RootState,
    {
      workflows: IWorkflowTemplateModel[];
      loading?: boolean;
      error?: boolean;
      total: number;
    }
  >(lastUpdatedFavoriteWorkflowsSelector);
};

const lastUpdatedInProgressWorkflowsSelector = createSelector(
  (state: RootState) => state.workflow!.workflows.workflows,
  data => {
    const inprogressWorkflows = _orderBy(
      data
        .filter(i => i.status?.toLowerCase() === 'incomplete')
        .map(i => ({
          ...i,
          updateDateFormat: formatTimeDefault(i.updatedDate, FormatTime.Date)
        })),
      ['updateDateFormat', 'name'],
      ['desc', 'asc']
    );
    return {
      workflows: inprogressWorkflows.slice(0, 5),
      total: inprogressWorkflows.length
    };
  }
);
export const useSelectLastUpdatedInProgressWorkflows = () => {
  return useSelector<RootState, { workflows: IWorkflowModel[]; total: number }>(
    lastUpdatedInProgressWorkflowsSelector
  );
};

const workflowsFetchingStatusSelector = createSelector(
  (state: RootState) => state.workflow!.workflows.error,
  (state: RootState) => state.workflow!.workflows.loading,
  (error, loading) => ({ error, loading })
);
export const useSelectWorkflowsFetchingStatus = () => {
  return useSelector<RootState, IFetching>(workflowsFetchingStatusSelector);
};

const templateWorkflowsSelector = createSelector(
  (state: RootState) => state.workflow!.templateWorkflows,
  data => {
    const {
      workflows: rawData,
      dataFilter,
      loading,
      error,
      alreadyLoad
    } = data;

    const searchValue = dataFilter?.searchValue?.toLowerCase() || '';
    const workflows =
      rawData?.filter(w =>
        matchSearchValue([w.name, w.description], searchValue)
      ) || [];

    return {
      workflows: orderBy(
        workflows,
        [
          (value: IWorkflowTemplateModel) => value.type?.toLowerCase()?.trim(),
          (value: IWorkflowTemplateModel) => value.name?.toLowerCase()?.trim()
        ],
        ['asc', 'asc']
      ),
      loading,
      error,
      alreadyLoad
    };
  }
);
export const useSelectTemplateWorkFlows = () => {
  return useSelector<RootState, TemplateWorkflowState>(
    templateWorkflowsSelector
  );
};

const templateWorkflowsFilterSelector = createSelector(
  (state: RootState) => state.workflow!.templateWorkflows.dataFilter,
  data => data || {}
);
export const useSelectTemplateWorkFlowsFilter = () => {
  return useSelector<RootState, IDataFilter>(templateWorkflowsFilterSelector);
};

const workflowSelector = createSelector(
  (state: RootState) => state.workflow!.changeWorkflows,
  data => {
    const { workflows: rawWorkflows, loading, error, dataFilter } = data;

    const { searchValue, page, pageSize, sortBy, orderBy } = dataFilter || {};

    let workflows = _orderBy(rawWorkflows, sortBy, orderBy).filter(i =>
      matchSearchValue([i.name, i.description], searchValue)
    );

    const totalItemFilter = workflows.length;
    workflows = workflows.slice((page! - 1) * pageSize!, page! * pageSize!);

    return {
      loading,
      total: rawWorkflows.length,
      totalItemFilter,
      error,
      workflows
    };
  }
);
export const useSelectChangeWorkflows = () => {
  return useSelector<
    RootState,
    {
      workflows: IWorkflowModel[];
      loading: boolean;
      error: boolean;
      total: number;
      totalItemFilter: number;
    }
  >(workflowSelector);
};
const selectAllChangeWorkflows = createSelector(
  (state: RootState) => state.workflow!.changeWorkflows,
  data => data
);
export const useSelectAllChangeWorkflows = () => {
  return useSelector<RootState, ChangeWorkflows>(selectAllChangeWorkflows);
};

const workflowDeletedSelector = createSelector(
  (state: RootState) => state.workflow!.changeWorkflows,
  data => {
    return data?.deletedWorkflowIds || [];
  }
);
export const useSelectDeletedChangeWorkflowIds = () => {
  return useSelector<RootState, string[]>(workflowDeletedSelector);
};

const workflowsInvalidMessagesSelector = createSelector(
  (state: RootState) => state.workflow!.changeWorkflows?.workflows || [],
  workflows =>
    (workflows as any).flatMap(
      (w: any) =>
        w.validationMessages?.map((m: any) => ({
          ...m,
          workflowId: w.id,
          source: w.name,

          actions: w.actions || []
        })) || []
    )
);
export const useSelectChangeWorkflowsInvalidMessages = () => {
  return useSelector<RootState, IValidationMessage[]>(
    workflowsInvalidMessagesSelector
  );
};

const workflowFilterSelector = createSelector(
  (state: RootState) => state.workflow!.changeWorkflows.dataFilter!,
  data => data
);
export const useSelectChangeWorkflowFilter = () => {
  return useSelector<RootState, IDataFilter>(workflowFilterSelector);
};

const workflowDetailSelector = createSelector(
  (state: RootState) => state.workflow!.changeWorkflowDetail,
  data => data
);
export const useSelectChangeWorkflowDetail = () => {
  return useSelector<RootState, ChangeWorkflowDetail>(workflowDetailSelector);
};

const workflowRemovalSelector = createSelector(
  (state: RootState) => state.workflow!.workflowRemoval,
  data => data
);
export const useSelectChangeWorkflowRemoval = () => {
  return useSelector<RootState, WorkflowRemoval>(workflowRemovalSelector);
};

const pageWorkflowTemplatesSelector = createSelector(
  (state: RootState) => state.workflow!.templateWorkflows,
  data => {
    const { workflows: rawWorkflowTemplates, pageFetching, pageFilter } = data;

    const {
      page,
      pageSize,
      sortBy: sortByDefault,
      orderBy: orderByDefault,
      currentSection = WorkflowSection.ALL,
      filterValue = {},
      favoriteWorkflowFilterValue = {},
      myWorkflowFilterValue = {},
      searchValues = {},
      sortBys = {},
      orderBys = {}
    } = pageFilter || {};

    const currentFilter =
      currentSection === WorkflowSection.FAVORITE_WORKFLOWS
        ? favoriteWorkflowFilterValue
        : currentSection === WorkflowSection.MY_WORKFLOWS
        ? myWorkflowFilterValue
        : filterValue;

    const currentFilterWithoutType = { ...currentFilter, type: '' };
    const changeTypeList = rawWorkflowTemplates
      .filter(w =>
        Object.keys(currentFilterWithoutType).every(
          (field: string) =>
            !currentFilterWithoutType[field] ||
            w[field as keyof IWorkflowTemplateModel] ===
              currentFilterWithoutType[field]
        )
      )
      .map(i => i.type)
      .filter((v, i, a) => a.indexOf(v) === i);

    const sortBy = sortBys[currentSection] ?? sortByDefault;
    const orderBy = orderBys[currentSection] ?? orderByDefault;
    const _workflowTemplates = _orderBy(
      rawWorkflowTemplates.filter(i =>
        matchSearchValue([i.name, i.description], searchValues[currentSection])
      ),
      sortBy?.map(
        k => (value: any) =>
          isString(value[k]) ? value[k]?.trim()?.toLowerCase() : value[k]
      ),
      orderBy
    );

    const _filterTypeWorkflows = _workflowTemplates.filter(w =>
      Object.keys(currentFilter).every(
        (field: string) =>
          !currentFilter[field] ||
          w[field as keyof IWorkflowTemplateModel] === currentFilter[field]
      )
    );

    const total = _filterTypeWorkflows.length;
    const workflowTemplates = _filterTypeWorkflows.slice(
      (page! - 1) * pageSize!,
      page! * pageSize!
    );
    const { loading, error } = pageFetching!;
    return { loading, total, error, workflowTemplates, changeTypeList };
  }
);
export const useSelectPageWorkflowTemplates = () => {
  return useSelector<
    RootState,
    {
      workflowTemplates: IWorkflowTemplateModel[];
      loading: boolean | undefined;
      error: boolean | undefined;
      total: number;
      changeTypeList: string[];
    }
  >(pageWorkflowTemplatesSelector);
};

const pageWorkflowTemplateFilterSelector = createSelector(
  (state: RootState) => state.workflow!.templateWorkflows.pageFilter!,
  data => data
);
export const useSelectPageWorkflowTemplatesFilter = () => {
  return useSelector<RootState, IWorkflowPageFilter>(
    pageWorkflowTemplateFilterSelector
  );
};

const pageWorkflowTemplateTypesSelector = createSelector(
  (state: RootState) => state.workflow!.templateWorkflows.workflows,
  data => {
    const types = _orderBy(
      data.map(template => template.type),
      [type => type?.toLowerCase()]
    );
    const workflowTemplateTypes = types.reduce(
      (acc: WorkflowTemplateType[], curr: string) => {
        const exist = acc.find(a => a.id === curr);
        exist && exist.count
          ? exist.count++
          : acc.push({ id: curr, label: camelToText(curr), count: 1 });
        return acc;
      },
      []
    );
    return workflowTemplateTypes;
  }
);
export const useSelectPageWorkflowTemplateTypes = () => {
  return useSelector<RootState, WorkflowTemplateType[]>(
    pageWorkflowTemplateTypesSelector
  );
};

export const useSelectFavoriteWorkflowTemplate = () => {
  return useSelector<RootState, FavoriteWorkflow>(
    state => state.workflow!.favoriteWorkflowTemplate
  );
};

export const useSelectUnfavoriteWorkflowTemplate = () => {
  return useSelector<RootState, UnfavoriteWorkflow>(
    state => state.workflow!.unfavoriteWorkflowTemplate
  );
};

const inProgressWorkflowsSelector = createSelector(
  (state: RootState) => state.workflow!.workflows,
  data => {
    const {
      workflows: rawData,
      dataFilter = {},
      loading = false,
      error = false
    } = data;
    const {
      sortBy,
      orderBy,
      page,
      pageSize,
      searchValue = '',
      filterValue = ''
    } = dataFilter;

    const _searchValue = searchValue.toLowerCase() || '';
    const formattedDateData = rawData.map(r => ({
      ...r,
      updatedDate: formatTimeDefault(r.updatedDate, FormatTime.Date)
    }));
    const _workflows = _orderBy(
      formattedDateData,
      sortBy?.map(
        id => (value: any) =>
          isString(value[id]) ? value[id]?.toLowerCase()?.trim() : value[id]
      ),
      orderBy
    )
      .filter(w => matchSearchValue([w.name, w.description], _searchValue))
      .filter(w => !filterValue || w.workflowType === filterValue)
      .filter(i => i.status?.toLowerCase() === 'incomplete');

    const total = _workflows.length;
    const workflows = _workflows.slice(
      (page! - 1) * pageSize!,
      page! * pageSize!
    );
    return { workflows, total, loading, error };
  }
);
export const useSelectInProgressWorkflowsSelector = () => {
  return useSelector<
    RootState,
    {
      workflows: IWorkflowModel[];
      loading: boolean;
      error: boolean;
      total: number;
    }
  >(inProgressWorkflowsSelector);
};

const inProgressWorkflowsFilterSelector = createSelector(
  (state: RootState) => state.workflow!.workflows.dataFilter,
  data => data || {}
);
export const useSelectInProgressWorkflowsFilterSelector = () => {
  return useSelector<RootState, IDataFilter>(inProgressWorkflowsFilterSelector);
};

const inProgressWorkflowTypesSelector = createSelector(
  (state: RootState) => state.workflow!.workflows.workflows,
  data => {
    const types = _orderBy(
      data
        .filter(i => i.status?.toLowerCase() === 'incomplete')
        .map(d => d.workflowType)
    );
    const uniqTypes = types.reduce((acc: RefData[], curr: string) => {
      const exist = acc.find(a => a.code === curr);
      !exist && acc.push({ code: curr, text: camelToText(curr) });
      return acc;
    }, []);
    return [{ code: '', text: 'All' }, ...uniqTypes];
  }
);
export const useSelectInProgressWorkflowTypesSelector = () => {
  return useSelector<RootState, RefData[]>(inProgressWorkflowTypesSelector);
};

const allWorkflowRefData = createSelector(
  (state: RootState) => state.workflow!.templateWorkflows.workflows,
  data => {
    const refDatas = data.map(w => ({
      code: w.id,
      text: w.name
    }));
    return _orderBy(refDatas, 'text', OrderBy.ASC);
  }
);
export const useSelectAllWorkflowRefDataSelector = () => {
  return useSelector<RootState, RefData[]>(allWorkflowRefData);
};
