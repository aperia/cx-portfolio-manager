import { renderHook } from '@testing-library/react-hooks';
import {
  WorkflowData,
  WorkflowMapping,
  WorkflowTemplateData,
  WorkflowTemplateData1
} from 'app/fixtures/workflow-data';
import { mappingDataFromArray } from 'app/helpers';
import { ChangeWorkflowDetail } from 'app/types';
import {
  useSelectAllChangeWorkflows,
  useSelectAllWorkflowRefDataSelector,
  useSelectChangeWorkflowDetail,
  useSelectChangeWorkflowFilter,
  useSelectChangeWorkflowRemoval,
  useSelectChangeWorkflows,
  useSelectChangeWorkflowsInvalidMessages,
  useSelectDeletedChangeWorkflowIds,
  useSelectFavoriteWorkflowTemplate,
  useSelectInProgressWorkflowsFilterSelector,
  useSelectInProgressWorkflowsSelector,
  useSelectInProgressWorkflowTypesSelector,
  useSelectLastUpdatedFavoriteWorkflows,
  useSelectLastUpdatedInProgressWorkflows,
  useSelectPageWorkflowTemplates,
  useSelectPageWorkflowTemplatesFilter,
  useSelectPageWorkflowTemplateTypes,
  useSelectTemplateWorkFlows,
  useSelectTemplateWorkFlowsFilter,
  useSelectUnfavoriteWorkflowTemplate,
  useSelectWorkflowsFetchingStatus
} from 'pages/_commons/redux/Workflow';
import { defaultDataFilter } from 'pages/_commons/redux/Workflow/reducers';
import * as reactRedux from 'react-redux';

describe('select-hooks > workflow > select-hooks ', () => {
  it('useSelectLastUpdatedFavoriteWorkflows', () => {
    const mockWorkflowTemplateData = mappingDataFromArray(
      WorkflowTemplateData,
      WorkflowMapping.WorkflowTemplate
    );
    jest.spyOn(reactRedux, 'useSelector').mockImplementation(selector =>
      selector({
        workflow: {
          templateWorkflows: {
            workflows: mockWorkflowTemplateData,
            pageFetching: {
              loading: false,
              error: false
            }
          }
        } as any
      })
    );

    const { result } = renderHook(() =>
      useSelectLastUpdatedFavoriteWorkflows()
    );

    expect(result.current.workflows.length).toEqual(1);
    expect(result.current.workflows[0].name).toEqual(
      'Modify Incentive Pricing'
    );
  });

  it('useSelectChangeWorkflows > search have data', () => {
    const mockWorkflowData = mappingDataFromArray(
      WorkflowData,
      WorkflowMapping.Workflow
    );
    jest.spyOn(reactRedux, 'useSelector').mockImplementation(selector =>
      selector({
        workflow: {
          changeWorkflows: {
            workflows: mockWorkflowData,
            loading: false,
            dataFilter: {
              ...defaultDataFilter,
              searchValue: 'change in terms'
            }
          }
        } as any
      })
    );

    const { result } = renderHook(() => useSelectChangeWorkflows());

    expect(result.current.workflows.length).toEqual(2);
    expect(result.current.loading).toEqual(false);
  });

  it('useSelectChangeWorkflows > dataFilter: undefined', () => {
    const mockWorkflowData = mappingDataFromArray(
      WorkflowData,
      WorkflowMapping.Workflow
    );
    jest.spyOn(reactRedux, 'useSelector').mockImplementation(selector =>
      selector({
        workflow: {
          changeWorkflows: {
            workflows: mockWorkflowData,
            loading: false,
            dataFilter: undefined
          }
        } as any
      })
    );

    const { result } = renderHook(() => useSelectChangeWorkflows());

    expect(result.current.workflows.length).toEqual(0);
    expect(result.current.loading).toEqual(false);
  });

  it('useSelectChangeWorkflows > search no data', () => {
    const mockWorkflowData = mappingDataFromArray(
      WorkflowData,
      WorkflowMapping.Workflow
    );
    jest.spyOn(reactRedux, 'useSelector').mockImplementation(selector =>
      selector({
        workflow: {
          changeWorkflows: {
            workflows: mockWorkflowData,
            loading: false,
            dataFilter: {
              ...defaultDataFilter,
              searchValue: 'abcdef'
            }
          }
        } as any
      })
    );

    const { result } = renderHook(() => useSelectChangeWorkflows());

    expect(result.current.workflows.length).toEqual(0);
    expect(result.current.loading).toEqual(false);
  });

  it('lastUpdatedInProgressWorkflowsSelector', () => {
    const mockWorkflowData = mappingDataFromArray(
      WorkflowData,
      WorkflowMapping.Workflow
    );
    jest.spyOn(reactRedux, 'useSelector').mockImplementation(selector =>
      selector({
        workflow: {
          workflows: {
            workflows: mockWorkflowData,
            total: mockWorkflowData.length
          }
        } as any
      })
    );

    const { result } = renderHook(() =>
      useSelectLastUpdatedInProgressWorkflows()
    );

    expect(result.current.workflows.length).toEqual(2);
  });

  it('useSelectChangeWorkflowFilter', () => {
    jest.spyOn(reactRedux, 'useSelector').mockImplementation(selector =>
      selector({
        workflow: {
          changeWorkflows: {
            dataFilter: {
              ...defaultDataFilter,
              searchValue: 'search something'
            }
          }
        } as any
      })
    );

    const { result } = renderHook(() => useSelectChangeWorkflowFilter());

    expect(result.current.searchValue).toEqual('search something');
    expect(result.current.page).toEqual(1);
  });

  it('useSelectChangeWorkflowDetail', () => {
    jest.spyOn(reactRedux, 'useSelector').mockImplementation(selector =>
      selector({
        workflow: {
          changeWorkflowDetail: {
            data: {
              id: '1000',
              name: 'Mock workflow name'
            },
            loading: false,
            error: false
          } as ChangeWorkflowDetail
        }
      })
    );

    const { result } = renderHook(() => useSelectChangeWorkflowDetail());

    expect(result.current.data!.id).toEqual('1000');
    expect(result.current.data!.name).toEqual('Mock workflow name');
  });

  it('useSelectChangeWorkflowRemoval', () => {
    jest.spyOn(reactRedux, 'useSelector').mockImplementation(selector =>
      selector({
        workflow: {
          workflowRemoval: {
            loading: true,
            error: false
          }
        } as any
      })
    );

    const { result } = renderHook(() => useSelectChangeWorkflowRemoval());

    expect(result.current.loading).toEqual(true);
    expect(result.current.error).toEqual(false);
  });

  it('useSelectPageWorkflowTemplatesFilter', () => {
    jest.spyOn(reactRedux, 'useSelector').mockImplementation(selector =>
      selector({
        workflow: {
          templateWorkflows: {
            pageFilter: {
              currentSection: 'allWorkflows',
              myWorkflowFilterValue: {
                accesible: true
              },
              favoriteWorkflowFilterValue: {
                // accesible: true,
                isFavorite: true
              },
              searchValue: 'search something',
              sortBys: {},
              orderBys: {}
            }
          }
        } as any
      })
    );

    const { result } = renderHook(() => useSelectPageWorkflowTemplatesFilter());

    expect(result.current.currentSection).toEqual('allWorkflows');
    expect(result.current.myWorkflowFilterValue.accesible).toEqual(true);
    expect(result.current.favoriteWorkflowFilterValue.isFavorite).toEqual(true);
    expect(result.current.searchValue).toEqual('search something');
    expect(result.current.sortBys).toEqual({});
    expect(result.current.orderBys).toEqual({});
  });

  it('useSelectFavoriteWorkflowTemplate', () => {
    jest.spyOn(reactRedux, 'useSelector').mockImplementation(selector =>
      selector({
        workflow: {
          favoriteWorkflowTemplate: {
            loading: true,
            error: false
          }
        } as any
      })
    );

    const { result } = renderHook(() => useSelectFavoriteWorkflowTemplate());

    expect(result.current.loading).toEqual(true);
    expect(result.current.error).toEqual(false);
  });

  it('useSelectUnfavoriteWorkflowTemplate', () => {
    jest.spyOn(reactRedux, 'useSelector').mockImplementation(selector =>
      selector({
        workflow: {
          unfavoriteWorkflowTemplate: {
            loading: true,
            error: false
          }
        } as any
      })
    );

    const { result } = renderHook(() => useSelectUnfavoriteWorkflowTemplate());

    expect(result.current.loading).toEqual(true);
    expect(result.current.error).toEqual(false);
  });

  it('inProgressWorkflowsFilterSelector', () => {
    jest.spyOn(reactRedux, 'useSelector').mockImplementation(selector =>
      selector({
        workflow: {
          workflows: {
            dataFilter: {
              searchValue: 'search something',
              searchFields: 'name',
              sortBy: ['name'],
              orderBy: 'asc',
              page: 1,
              pageSize: 1,
              totalItem: 10,
              filterField: 'name',
              filterValue: 'BOB'
            }
          }
        } as any
      })
    );

    const { result } = renderHook(() =>
      useSelectInProgressWorkflowsFilterSelector()
    );

    expect(result.current.searchValue).toEqual('search something');
    expect(result.current.searchFields).toEqual('name');
    expect(result.current.sortBy).toEqual(['name']);
    expect(result.current.orderBy).toEqual('asc');
    expect(result.current.page).toEqual(1);
    expect(result.current.pageSize).toEqual(1);
    expect(result.current.totalItem).toEqual(10);
    expect(result.current.filterField).toEqual('name');
    expect(result.current.filterValue).toEqual('BOB');
  });

  it('inProgressWorkflowsFilterSelector > dataFilter: undefined', () => {
    jest.spyOn(reactRedux, 'useSelector').mockImplementation(selector =>
      selector({
        workflow: {
          workflows: {
            dataFilter: undefined
          }
        } as any
      })
    );

    const { result } = renderHook(() =>
      useSelectInProgressWorkflowsFilterSelector()
    );

    expect(result.current.searchValue).toEqual(undefined);
  });

  it('workflowsFetchingStatusSelector', () => {
    jest.spyOn(reactRedux, 'useSelector').mockImplementation(selector =>
      selector({
        workflow: {
          workflows: {
            loading: true,
            error: false
          }
        } as any
      })
    );

    const { result } = renderHook(() => useSelectWorkflowsFetchingStatus());

    expect(result.current.loading).toEqual(true);
    expect(result.current.error).toEqual(false);
  });

  it('templateWorkflowsSelector ', () => {
    const mockWorkflowData = mappingDataFromArray(
      WorkflowData,
      WorkflowMapping.Workflow
    );

    jest.spyOn(reactRedux, 'useSelector').mockImplementation(selector =>
      selector({
        workflow: {
          templateWorkflows: {
            workflows: mockWorkflowData,
            dataFilter: {
              ...defaultDataFilter
            },
            loading: true,
            error: false,
            alreadyLoad: true
          }
        } as any
      })
    );

    const { result } = renderHook(() => useSelectTemplateWorkFlows());

    expect(result.current.loading).toEqual(true);
    expect(result.current.error).toEqual(false);
  });

  it('templateWorkflowsSelector =>  workflows: undefined', () => {
    jest.spyOn(reactRedux, 'useSelector').mockImplementation(selector =>
      selector({
        workflow: {
          templateWorkflows: {
            workflows: undefined,
            dataFilter: {
              ...defaultDataFilter
            },
            loading: true,
            error: false,
            alreadyLoad: true
          }
        } as any
      })
    );

    const { result } = renderHook(() => useSelectTemplateWorkFlows());

    expect(result.current.loading).toEqual(true);
    expect(result.current.error).toEqual(false);
  });

  it('templateWorkflowsFilterSelector ', () => {
    jest.spyOn(reactRedux, 'useSelector').mockImplementation(selector =>
      selector({
        workflow: {
          templateWorkflows: {
            dataFilter: {
              ...defaultDataFilter
            }
          }
        } as any
      })
    );

    const { result } = renderHook(() => useSelectTemplateWorkFlowsFilter());

    expect(result.current.page).toEqual(1);
  });

  it('templateWorkflowsFilterSelector =>  dataFilter: undefined', () => {
    jest.spyOn(reactRedux, 'useSelector').mockImplementation(selector =>
      selector({
        workflow: {
          templateWorkflows: {
            dataFilter: undefined
          }
        } as any
      })
    );

    const { result } = renderHook(() => useSelectTemplateWorkFlowsFilter());

    expect(result.current).toEqual({});
  });

  it('selectAllChangeWorkflows ', () => {
    const mockWorkflowData = mappingDataFromArray(
      WorkflowData,
      WorkflowMapping.Workflow
    );

    jest.spyOn(reactRedux, 'useSelector').mockImplementation(selector =>
      selector({
        workflow: {
          changeWorkflows: {
            workflows: mockWorkflowData,
            loading: true,
            error: false
          }
        } as any
      })
    );

    const { result } = renderHook(() => useSelectAllChangeWorkflows());

    expect(result.current.loading).toEqual(true);
    expect(result.current.error).toEqual(false);
  });

  it('workflowDeletedSelector', () => {
    jest.spyOn(reactRedux, 'useSelector').mockImplementation(selector =>
      selector({
        workflow: {
          changeWorkflows: {
            deletedWorkflowIds: ['1000']
          }
        } as any
      })
    );

    const { result } = renderHook(() => useSelectDeletedChangeWorkflowIds());

    expect(result.current[0]).toEqual('1000');
  });

  it('workflowDeletedSelector =>  deletedWorkflowIds undefined', () => {
    jest.spyOn(reactRedux, 'useSelector').mockImplementation(selector =>
      selector({
        workflow: {
          changeWorkflows: {
            deletedWorkflowIds: undefined
          }
        } as any
      })
    );

    const { result } = renderHook(() => useSelectDeletedChangeWorkflowIds());

    expect(result.current[0]).toEqual(undefined);
  });

  it('workflowsInvalidMessagesSelector', () => {
    jest.spyOn(reactRedux, 'useSelector').mockImplementation(selector =>
      selector({
        workflow: {
          changeWorkflows: {
            workflows: [
              {
                validationMessages: [
                  {
                    changeId: '1000',
                    workflowId: '2000',
                    source: 'abc',
                    field: 'name',
                    severity: 'error',
                    message: 'UT',
                    isChangeFeedback: true,
                    actions: ['edit']
                  }
                ]
              }
            ]
          }
        } as any
      })
    );

    const { result } = renderHook(() =>
      useSelectChangeWorkflowsInvalidMessages()
    );

    expect(result.current[0].changeId).toEqual('1000');
  });

  it('workflowsInvalidMessagesSelector => workflows undefined', () => {
    jest.spyOn(reactRedux, 'useSelector').mockImplementation(selector =>
      selector({
        workflow: {
          changeWorkflows: {
            workflows: undefined
          }
        } as any
      })
    );

    const { result } = renderHook(() =>
      useSelectChangeWorkflowsInvalidMessages()
    );

    expect(result.current[0]).toEqual(undefined);
  });

  it('workflowsInvalidMessagesSelector => validationMessages undefined', () => {
    jest.spyOn(reactRedux, 'useSelector').mockImplementation(selector =>
      selector({
        workflow: {
          changeWorkflows: {
            workflows: [
              {
                validationMessages: undefined
              }
            ]
          }
        } as any
      })
    );

    const { result } = renderHook(() =>
      useSelectChangeWorkflowsInvalidMessages()
    );

    expect(result.current[0]).toEqual(undefined);
  });

  it('pageWorkflowTemplatesSelector', () => {
    const mockWorkflowTemplateData = mappingDataFromArray(
      WorkflowTemplateData,
      WorkflowMapping.WorkflowTemplate
    );
    jest.spyOn(reactRedux, 'useSelector').mockImplementation(selector =>
      selector({
        workflow: {
          templateWorkflows: {
            workflows: mockWorkflowTemplateData,
            loading: false,
            error: false,
            total: mockWorkflowTemplateData.length,
            changeTypeList: ['123'],
            pageFetching: {
              loading: false,
              error: false
            },
            pageFilter: {
              sortBy: ['type', 'field'],
              myWorkflowFilterValue: {
                accesible: true
              },
              favoriteWorkflowFilterValue: {
                // accesible: true,
                isFavorite: true
              },
              searchValue: 'search something',
              sortBys: {},
              orderBys: {}
            }
          }
        } as any
      })
    );

    const { result } = renderHook(() => useSelectPageWorkflowTemplates());

    expect(result.current.loading).toEqual(false);
  });

  it('pageWorkflowTemplatesSelector > pageFilter: undefined', () => {
    const mockWorkflowTemplateData = mappingDataFromArray(
      WorkflowTemplateData,
      WorkflowMapping.WorkflowTemplate
    );
    jest.spyOn(reactRedux, 'useSelector').mockImplementation(selector =>
      selector({
        workflow: {
          templateWorkflows: {
            workflows: mockWorkflowTemplateData,
            loading: false,
            error: false,
            total: mockWorkflowTemplateData.length,
            changeTypeList: ['123'],
            pageFetching: {
              loading: false,
              error: false
            },
            pageFilter: undefined
          }
        } as any
      })
    );

    const { result } = renderHook(() => useSelectPageWorkflowTemplates());

    expect(result.current.loading).toEqual(false);
  });

  it('pageWorkflowTemplatesSelector > currentSection: favoriteWorkflows', () => {
    const mockWorkflowTemplateData = mappingDataFromArray(
      WorkflowTemplateData,
      WorkflowMapping.WorkflowTemplate
    );
    jest.spyOn(reactRedux, 'useSelector').mockImplementation(selector =>
      selector({
        workflow: {
          templateWorkflows: {
            workflows: mockWorkflowTemplateData,
            loading: false,
            error: false,
            total: mockWorkflowTemplateData.length,
            changeTypeList: ['123'],
            pageFetching: {
              loading: false,
              error: false
            },
            pageFilter: {
              currentSection: 'favoriteWorkflows',
              myWorkflowFilterValue: {
                accesible: true
              },
              favoriteWorkflowFilterValue: {
                // accesible: true,
                isFavorite: true
              },
              searchValue: 'search something',
              sortBys: {},
              orderBys: {}
            }
          }
        } as any
      })
    );

    const { result } = renderHook(() => useSelectPageWorkflowTemplates());

    expect(result.current.loading).toEqual(false);
  });

  it('pageWorkflowTemplatesSelector > currentSection: myWorkflows', () => {
    const mockWorkflowTemplateData = mappingDataFromArray(
      WorkflowTemplateData,
      WorkflowMapping.WorkflowTemplate
    );
    jest.spyOn(reactRedux, 'useSelector').mockImplementation(selector =>
      selector({
        workflow: {
          templateWorkflows: {
            workflows: mockWorkflowTemplateData,
            loading: false,
            error: false,
            total: mockWorkflowTemplateData.length,
            changeTypeList: ['123'],
            pageFetching: {
              loading: false,
              error: false
            },
            pageFilter: {
              currentSection: 'myWorkflows',
              myWorkflowFilterValue: {
                accesible: true
              },
              favoriteWorkflowFilterValue: {
                // accesible: true,
                isFavorite: true
              },
              searchValue: 'search something',
              sortBys: {},
              orderBys: {}
            }
          }
        } as any
      })
    );

    const { result } = renderHook(() => useSelectPageWorkflowTemplates());

    expect(result.current.loading).toEqual(false);
  });

  it('pageWorkflowTemplateTypesSelector', () => {
    const mockWorkflowTemplateData = mappingDataFromArray(
      [WorkflowTemplateData, WorkflowTemplateData1],
      WorkflowMapping.WorkflowTemplate
    );
    jest.spyOn(reactRedux, 'useSelector').mockImplementation(selector =>
      selector({
        workflow: {
          templateWorkflows: {
            workflows: mockWorkflowTemplateData,
            pageFetching: {
              loading: false,
              error: false
            }
          }
        } as any
      })
    );

    const { result } = renderHook(() => useSelectPageWorkflowTemplateTypes());
    expect(result.current[0].count).toEqual(2);
  });

  describe('inProgressWorkflowsSelector', () => {
    it('workflow have workflowType', () => {
      const mockWorkflowTemplateData = mappingDataFromArray(
        WorkflowData,
        WorkflowMapping.Workflow
      );
      jest.spyOn(reactRedux, 'useSelector').mockImplementation(selector =>
        selector({
          workflow: {
            workflows: {
              workflows: mockWorkflowTemplateData,
              dataFilter: {
                sortBy: ['id', 'field'],
                searchValue: 'rebum',
                filterValue: 'workflowType'
              },
              total: mockWorkflowTemplateData.length,
              changeTypeList: ['123'],
              pageFetching: {
                loading: false,
                error: false
              },
              pageFilter: {
                currentSection: 'myWorkflows',
                myWorkflowFilterValue: {
                  accesible: true
                },
                favoriteWorkflowFilterValue: {
                  isFavorite: true
                },
                searchValue: 'search something',
                sortBys: {},
                orderBys: {}
              }
            }
          } as any
        })
      );

      const { result } = renderHook(() =>
        useSelectInProgressWorkflowsSelector()
      );

      expect(result.current.loading).toEqual(false);
    });

    it('filterValue is empty', () => {
      const mockWorkflowTemplateData = mappingDataFromArray(
        WorkflowData,
        WorkflowMapping.Workflow
      );
      jest.spyOn(reactRedux, 'useSelector').mockImplementation(selector =>
        selector({
          workflow: {
            workflows: {
              workflows: mockWorkflowTemplateData,
              // loading: false,
              // error: false,
              dataFilter: {
                searchValue: 'rebum'
              },
              total: mockWorkflowTemplateData.length,
              changeTypeList: ['123'],
              pageFetching: {
                loading: false,
                error: false
              },
              pageFilter: {
                currentSection: 'myWorkflows',
                myWorkflowFilterValue: {
                  accesible: true
                },
                favoriteWorkflowFilterValue: {
                  // accesible: true,
                  isFavorite: true
                },
                searchValue: 'search something',
                sortBys: {},
                orderBys: {}
              }
            }
          } as any
        })
      );

      const { result } = renderHook(() =>
        useSelectInProgressWorkflowsSelector()
      );

      expect(result.current.loading).toEqual(false);
    });

    it('inProgressWorkflowsSelector', () => {
      const mockWorkflowTemplateData = mappingDataFromArray(
        WorkflowData,
        WorkflowMapping.Workflow
      );
      jest.spyOn(reactRedux, 'useSelector').mockImplementation(selector =>
        selector({
          workflow: {
            workflows: {
              workflows: mockWorkflowTemplateData,
              // loading: false,
              // error: false,
              total: mockWorkflowTemplateData.length,
              changeTypeList: ['123'],
              pageFetching: {
                loading: false,
                error: false
              },
              pageFilter: {
                currentSection: 'myWorkflows',
                myWorkflowFilterValue: {
                  accesible: true
                },
                favoriteWorkflowFilterValue: {
                  // accesible: true,
                  isFavorite: true
                },
                searchValue: 'search something',
                sortBys: {},
                orderBys: {}
              }
            }
          } as any
        })
      );

      const { result } = renderHook(() =>
        useSelectInProgressWorkflowsSelector()
      );

      expect(result.current.loading).toEqual(false);
    });
  });

  it('inProgressWorkflowTypesSelector', () => {
    const mockWorkflowData = mappingDataFromArray(
      WorkflowData,
      WorkflowMapping.Workflow
    );
    jest.spyOn(reactRedux, 'useSelector').mockImplementation(selector =>
      selector({
        workflow: {
          workflows: {
            workflows: mockWorkflowData,
            code: 'code',
            text: 'text'
          }
        } as any
      })
    );

    const { result } = renderHook(() =>
      useSelectInProgressWorkflowTypesSelector()
    );

    expect(result.current[0].text).toEqual('All');
  });

  it('allWorkflowRefData', () => {
    jest.spyOn(reactRedux, 'useSelector').mockImplementation(selector =>
      selector({
        workflow: {
          templateWorkflows: {
            workflows: [
              {
                id: '1000',
                name: 'bod'
              }
            ],
            code: 'code',
            text: 'text'
          }
        } as any
      })
    );

    const { result } = renderHook(() => useSelectAllWorkflowRefDataSelector());

    expect(result.current[0].text).toEqual('bod');
  });
});
