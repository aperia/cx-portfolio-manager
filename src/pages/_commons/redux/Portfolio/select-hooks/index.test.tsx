import { renderHook } from '@testing-library/react-hooks';
import { PortfolioData, PortfolioMapping } from 'app/fixtures/portfolio-data';
import { mappingDataFromArray } from 'app/helpers';
import {
  defaultDataFilter,
  filterPortfolios,
  PortfolioState,
  useSelectPortfolios,
  useSelectPortfoliosFilter
} from 'pages/_commons/redux/Portfolio';
import * as reactRedux from 'react-redux';
import {
  selectKPIData,
  useKpiIdSelector,
  useSelectKPIListForSetup,
  useSelectKPIListForSetupMenu,
  useSelectOpendChooseModal,
  useSelectPortfoliosDetail
} from '.';

describe('redux-store > portfolio > selectors', () => {
  let mockPortfolioData: any[] = [];
  let selectorMock: jest.SpyInstance;
  const mockSelectorActivities = (portfolioState: PortfolioState) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector => selector({ portfolio: portfolioState }));
  };
  beforeEach(() => {
    mockPortfolioData = mappingDataFromArray(
      PortfolioData,
      PortfolioMapping
    )?.map((p: any) => {
      if (!p.icon) {
        delete p.icon;
      }
      return p;
    });
  });
  afterEach(() => {
    selectorMock && selectorMock.mockClear();
  });
  it('filterPortfolios', () => {
    const result = filterPortfolios(mockPortfolioData, {
      ...defaultDataFilter,
      searchValue: 'visa'
    });
    expect(result.portfolioList.length).toEqual(1);
  });
  it('filterPortfolios with undefined date filter', () => {
    const result = filterPortfolios(mockPortfolioData);
    expect(result.portfolioList.length).toEqual(0);
  });
  it('useSelectPortfolios', () => {
    const pre = {
      portfoliosData: {
        portfolios: mockPortfolioData
      }
    } as any;

    mockSelectorActivities(pre);
    const { result } = renderHook(() => useSelectPortfolios());

    expect(result.current.portfolios.length).toEqual(2);
  });
  it('useSelectPortfoliosDetail', () => {
    const pre = {
      portfoliosData: {
        portfolios: mockPortfolioData
      },
      portfoliosDetails: {
        loading: false
      }
    } as any;

    mockSelectorActivities(pre);
    const { result } = renderHook(() => useSelectPortfoliosDetail());

    expect(result.current.loading).toEqual(false);
  });
  it('useSelectPortfoliosFilter', () => {
    const pre = {
      portfoliosData: {
        dataFilter: { ...defaultDataFilter, searchValue: 'master' }
      }
    } as any;

    mockSelectorActivities(pre);
    const { result } = renderHook(() => useSelectPortfoliosFilter());

    expect(result.current.searchValue).toEqual('master');
  });

  it('useSelectKPIListForSetup', () => {
    const pre = {
      getKPIListForSetup: {
        data: [],
        loading: false,
        error: false
      }
    } as any;

    mockSelectorActivities(pre);
    const { result } = renderHook(() => useSelectKPIListForSetup());

    expect(result.current.data).toEqual([]);
  });

  it('useSelectKPIListForSetupMenuSelector', () => {
    const pre = {
      getKPIListForSetup: {
        data: [],
        loading: false,
        error: false
      }
    } as any;

    mockSelectorActivities(pre);
    const { result } = renderHook(() => useSelectKPIListForSetupMenu());

    expect(result.current).toEqual([
      {
        count: 0,
        id: 'all',
        label: 'All'
      }
    ]);
  });

  it('useSelectKPIListForSetupMenuSelector', () => {
    const pre = {
      getKPIListForSetup: {
        data: [
          {
            label: '1',
            id: '1',
            subjectArea: '1'
          },
          {
            label: '2',
            id: '2',
            subjectArea: '2'
          }
        ],
        loading: false,
        error: false
      }
    } as any;

    mockSelectorActivities(pre);
    const { result } = renderHook(() => useSelectKPIListForSetupMenu());

    expect(result.current).toEqual([
      {
        count: 2,
        id: 'all',
        label: 'All'
      },
      {
        count: 1,
        id: '1',
        label: '1'
      },
      {
        count: 1,
        id: '2',
        label: '2'
      }
    ]);
  });

  it('useSelectOpendChooseModal', () => {
    const pre = {
      chooseKPI: {
        openedChoosePopup: false,
        openedAddPopup: false
      }
    } as any;

    mockSelectorActivities(pre);
    const { result } = renderHook(() => useSelectOpendChooseModal());

    expect(result.current.openedChoosePopup).toEqual(false);
  });

  it('useKpiIdSelector value not exist', () => {
    const pre = {
      getKPIDetails: {
        data: [],
        loading: false,
        error: false
      }
    } as any;

    mockSelectorActivities(pre);
    const { result } = renderHook(() => useKpiIdSelector(selectKPIData, '123'));

    expect(result).toEqual({
      all: [{ data: undefined, loading: undefined }],
      current: { data: undefined, loading: undefined },
      error: undefined
    });
  });

  it('selectKPIData', () => {
    const pre = {
      getKPIDetails: {
        data: [],
        loading: false,
        error: false
      }
    } as any;

    mockSelectorActivities(pre);
    const { result } = renderHook(() => selectKPIData());

    expect(result.current.data).toEqual(undefined);
  });
});
