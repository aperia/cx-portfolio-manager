import { camelToText, orderByNullLast } from 'app/helpers';
import { matchSearchValue } from 'app/helpers/matchSearchValue';
import { shallowEqual, useSelector } from 'react-redux';
import { createSelector } from 'reselect';
import {
  chooseKPI,
  getKPIListForSetupState,
  KPISetupTemplateType,
  LastUpdatedPortfoliosState,
  PortfoliosDetailsState
} from '../types';

export const filterPortfolios = (
  portfolios: IPortfolioModel[],
  dataFilter?: IDataFilter
) => {
  const { searchValue, page, pageSize, sortBy, orderBy } = dataFilter || {};

  const searchTrimmed = searchValue?.trim();

  let portfoliosFiltered: any[];

  portfoliosFiltered = orderByNullLast(
    portfolios.map(i => ({ ...i })),
    sortBy!,
    orderBy!
  );

  portfoliosFiltered = portfoliosFiltered.filter(i =>
    matchSearchValue(
      [...(i.spa || []), i.clientId, i.portfolioId, i.portfolioName],
      searchTrimmed
    )
  );

  const totalItem = portfoliosFiltered.length;

  portfoliosFiltered = portfoliosFiltered.slice(
    (page! - 1) * pageSize!,
    page! * pageSize!
  );

  return {
    portfolioList: portfoliosFiltered,
    totalItem
  };
};

export const useSelectPortfolios = () => {
  return useSelector<RootState, LastUpdatedPortfoliosState>(
    state => state.portfolio!.portfoliosData,
    shallowEqual
  );
};

export const useSelectPortfoliosDetail = () => {
  return useSelector<RootState, PortfoliosDetailsState>(
    state => state.portfolio!.portfoliosDetails,
    shallowEqual
  );
};

export const useSelectPortfoliosFilter = () => {
  return useSelector<RootState, IDataFilter>(
    state => state.portfolio!.portfoliosData.dataFilter!,
    shallowEqual
  );
};

export const useSelectKPIListForSetup = () => {
  return useSelector<RootState, getKPIListForSetupState>(
    state => state.portfolio!.getKPIListForSetup,
    shallowEqual
  );
};

export const selectKPIData: any = (state: RootState, id: string) => {
  return {
    data: state?.portfolio!.getKPIDetails[id]?.data,
    loading: state?.portfolio!.getKPIDetails[id]?.loading
  };
};

export const useKpiIdSelector = <T extends unknown>(
  selector: Function,
  id: string
): T => {
  const dataSelector = useSelector<RootState, T>(states =>
    selector(states, id)
  );

  return dataSelector;
};

const useSelectKPIListForSetupMenuSelector = createSelector(
  (state: RootState) => state.portfolio!.getKPIListForSetup,
  data => {
    const kpiList = data?.data || [];

    const kpiTypes = kpiList.reduce((acc: KPISetupTemplateType[], curr) => {
      const exist = acc.find((a: any) => a.label === curr.subjectArea);
      exist && exist.count
        ? exist.count++
        : acc.push({
            id: curr.subjectArea.toLocaleLowerCase(),
            label: camelToText(curr.subjectArea),
            count: 1
          });
      return acc;
    }, []);

    const kpiSubjectAreaList = kpiTypes.sort((a: any, b: any) =>
      a.id > b.id ? 1 : -1
    );

    kpiSubjectAreaList.unshift({
      id: 'all',
      label: camelToText('All'),
      count: kpiList.length
    });

    return kpiSubjectAreaList;
  }
);

export const useSelectKPIListForSetupMenu = () => {
  return useSelector<RootState, KPISetupTemplateType[]>(
    useSelectKPIListForSetupMenuSelector
  );
};

export const useSelectOpendChooseModal = () => {
  return useSelector<RootState, chooseKPI>(
    state => state.portfolio!.chooseKPI,
    shallowEqual
  );
};
