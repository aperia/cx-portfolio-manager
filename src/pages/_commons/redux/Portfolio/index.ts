import { createSlice } from '@reduxjs/toolkit';
import {
  addPortfolioKPI,
  addPortfolioKPIBuilder,
  getKPIDetails,
  getKPIDetailsBuilder,
  getKPIListForSetup,
  getKPIListForSetupBuilder,
  getPortfolioDetail,
  getPortfolioDetailBuilder,
  getPortfolioInchange,
  getPortfolioInchangeBuilder
} from './async-thunk';
import {
  deletePortfolioKPI,
  deletePortfolioKPIBuilder
} from './async-thunk/deletePortfolioKPI';
import {
  updatePortfolioKPI,
  updatePortfolioKPIBuilder
} from './async-thunk/updatePortfolioKPI';
import reducers from './reducers';
import { defaultDataFilter, PortfolioState } from './types';

const { actions, reducer } = createSlice({
  name: 'portfolio',
  initialState: {
    portfoliosData: {
      portfolios: [],
      loading: true,
      dataFilter: defaultDataFilter
    },
    portfoliosDetails: {
      loading: false
    },
    getKPIListForSetup: {
      data: [],
      loading: false
    },
    getKPIDetails: {},
    chooseKPI: {
      openedChoosePopup: false,
      openedAddPopup: false
    },
    addPortfolioKPI: {
      loading: false
    },
    updatePortfolioKPI: {
      loading: false
    },
    deletePortfolioKPI: {
      loading: false
    }
  } as PortfolioState,
  reducers,
  extraReducers: builder => {
    getPortfolioInchangeBuilder(builder);
    getPortfolioDetailBuilder(builder);
    getKPIListForSetupBuilder(builder);
    getKPIDetailsBuilder(builder);
    addPortfolioKPIBuilder(builder);
    updatePortfolioKPIBuilder(builder);
    deletePortfolioKPIBuilder(builder);
  }
});

const extraActions = {
  ...actions,
  getPortfolioInchange,
  getPortfolioDetail,
  getKPIListForSetup,
  getKPIDetails,
  addPortfolioKPI,
  updatePortfolioKPI,
  deletePortfolioKPI
};

export * from './select-hooks';
export * from './types';
export { extraActions as actionsPortfolio, reducer };
