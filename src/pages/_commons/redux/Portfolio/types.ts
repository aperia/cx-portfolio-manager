import { FIRST_PAGE, PAGE_SIZE } from 'app/constants/constants';
import { OrderBy, PortfoliosSortByFields } from 'app/constants/enums';

export interface PortfolioState {
  portfoliosData: LastUpdatedPortfoliosState;
  portfoliosDetails: PortfoliosDetailsState;
  getKPIListForSetup: getKPIListForSetupState;
  addPortfolioKPI: AddPortfolioKPIState;
  updatePortfolioKPI: UpdatePortfolioKPIState;
  deletePortfolioKPI: DeletePortfolioKPIState;
  getKPIDetails: Record<any, any>;
  chooseKPI: chooseKPI;
}

export interface AddPortfolioKPIState {
  data?: IAddPortfolioKPI;
}

export interface UpdatePortfolioKPIState {
  data?: IUpdatePortfolioKPI[];
}

export interface DeletePortfolioKPIState {
  data?: IDeletePortfolioKPI[];
}

export interface GetKPIDetailsState {
  data?: IKPIDetails;
  loading: boolean;
  error?: boolean;
}

interface IKPIDetails {
  id: string;
  name: string;
  kpi: string;
  subjectArea: string;
  submetric: string;
  period: string;
  daysPreviously: string;
  items: IItem[];
}

interface IItem {
  name: string;
  values: IValue[];
}

interface IValue {
  label: string;
  value: string;
}

export interface chooseKPI {
  openedChoosePopup: boolean;
  openedAddPopup: boolean;
  dataChooseKPI?: IKPIListForSetup;
}

export interface getKPIListForSetupState {
  data: IKPIListForSetup[];
  loading: boolean;
  error?: boolean;
}

export interface KPISetupTemplateType {
  id: string;
  label: string;
  count?: number;
  title?: string;
  description?: string;
}

export interface KPITemplateType {
  id: string;
  label: string;
  count?: number;
  title?: string;
  description?: string;
}

export interface PortfoliosDetailsState {
  data?: IPortfoliosDetail;
  loading: boolean;
  error?: boolean;
}

export interface LastUpdatedPortfoliosState {
  portfolios: IPortfolioModel[];
  loading?: boolean;
  error?: boolean;
  dataFilter?: IDataFilter;
  portfolioCriteriaFields?: string[];
}

export interface ChangePageSize {
  size: number;
}

export interface ChangePage {
  page: number;
}

export interface ISort {
  orderBy: OrderByType[];
  sortBy: string[];
}

export interface UpdateSearch {
  searchValue: string;
}

export const defaultSortBy: string[] = [
  PortfoliosSortByFields.ACTIVE_ACCOUNTS,
  PortfoliosSortByFields.PORTFOLIO_NAME,
  PortfoliosSortByFields.CLIENT_ID
];
export const defaultOrderBy: OrderByType[] = [
  OrderBy.DESC,
  OrderBy.ASC,
  OrderBy.ASC
];
export const defaultDataFilter: IDataFilter = {
  page: FIRST_PAGE,
  pageSize: PAGE_SIZE[0],
  searchValue: '',
  sortBy: [...defaultSortBy],
  orderBy: [...defaultOrderBy]
};
