import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsPortfolio,
  PortfolioState,
  reducer
} from 'pages/_commons/redux/Portfolio';

const initReducer: PortfolioState = {
  getKPIDetails: {
    loading: false
  }
};

describe('redux-store > portfolio > getKPIDetails', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsPortfolio.getKPIDetails.pending('getKPIDetails', { id: 1 })
    );
    expect(nextState.getKPIDetails.loading).toEqual(false);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsPortfolio.getKPIDetails.fulfilled({}, 'getKPIDetails', { id: 1 })
    );
    expect(nextState.getKPIDetails.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsPortfolio.getKPIDetails.rejected(null, 'getKPIDetails', { id: 1 })
    );
    expect(nextState.getKPIDetails.error).toEqual(undefined);
  });
  it('thunk action', async () => {
    const nextState = await actionsPortfolio.getKPIDetails({ id: 1 })(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: { data: { portfoliosDetails: {} } }
        } as RootState),
      {
        portfolioService: {
          getKPIDetails: jest
            .fn()
            .mockResolvedValue({ data: { kpiDetails: {} } })
        }
      }
    );
    expect(nextState.payload.data).toEqual({ kpiDetails: {} });
  });

  it('thunk action', async () => {
    const nextState = await actionsPortfolio.getKPIDetails({ id: 1 })(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: { data: { portfoliosDetails: {} } }
        } as RootState),
      {
        portfolioService: {
          getKPIDetails: jest.fn().mockResolvedValue({})
        }
      }
    );
    expect(nextState.payload).toEqual({ data: {} });
  });
});
