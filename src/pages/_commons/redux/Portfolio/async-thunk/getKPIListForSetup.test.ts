import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsPortfolio,
  PortfolioState,
  reducer
} from 'pages/_commons/redux/Portfolio';

const initReducer: PortfolioState = {
  getKPIListForSetup: {
    loading: false
  }
};

describe('redux-store > portfolio > getKPIListForSetup', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsPortfolio.getKPIListForSetup.pending('getKPIListForSetup', '')
    );
    expect(nextState.getKPIListForSetup.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsPortfolio.getKPIListForSetup.fulfilled(
        {},
        'getKPIListForSetup',
        ''
      )
    );
    expect(nextState.getKPIListForSetup.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsPortfolio.getKPIListForSetup.rejected(
        null,
        'getKPIListForSetup',
        ''
      )
    );
    expect(nextState.getKPIListForSetup.error).toEqual(true);
  });
  it('thunk action', async () => {
    const nextState = await actionsPortfolio.getKPIListForSetup('id')(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: { data: { portfoliosDetails: {} } }
        } as RootState),
      {
        portfolioService: {
          getKPIListForSetup: jest.fn().mockResolvedValue({
            data: {
              kpis: [
                {
                  name: '123'
                },
                {
                  name: '456'
                }
              ]
            }
          })
        }
      }
    );
    expect(nextState.payload.data).toEqual([{ name: '123' }, { name: '456' }]);
  });
  it('thunk action', async () => {
    const nextState = await actionsPortfolio.getKPIListForSetup('id')(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: { data: { portfoliosDetails: {} } }
        } as RootState),
      {
        portfolioService: {
          getKPIListForSetup: jest.fn().mockResolvedValue({
            data: {
              kpis: [
                {
                  name: '123'
                },
                {
                  name: '123'
                }
              ]
            }
          })
        }
      }
    );
    expect(nextState.payload.data).toEqual([{ name: '123' }, { name: '123' }]);
  });
  it('thunk action', async () => {
    const nextState = await actionsPortfolio.getKPIListForSetup('id')(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: { data: { portfoliosDetails: {} } }
        } as RootState),
      {
        portfolioService: {
          getKPIListForSetup: jest.fn().mockResolvedValue({})
        }
      }
    );
    expect(nextState.payload.data).toEqual([]);
  });
});
