import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsPortfolio,
  PortfolioState,
  reducer
} from 'pages/_commons/redux/Portfolio';

const initReducer: PortfolioState = {
  deletePortfolioKPI: {
    loading: false
  }
};

describe('redux-store > portfolio > deletePortfolioKPI', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsPortfolio.deletePortfolioKPI.pending('deletePortfolioKPI', '')
    );
    expect(nextState.deletePortfolioKPI.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsPortfolio.deletePortfolioKPI.fulfilled(
        {},
        'deletePortfolioKPI',
        ''
      )
    );
    expect(nextState.deletePortfolioKPI.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsPortfolio.deletePortfolioKPI.rejected(
        null,
        'deletePortfolioKPI',
        ''
      )
    );
    expect(nextState.deletePortfolioKPI.error).toEqual(true);
  });
  it('thunk action', async () => {
    const nextState = await actionsPortfolio.deletePortfolioKPI({})(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: { data: { portfoliosDetails: {} } }
        } as RootState),
      {
        portfolioService: {
          deletePortfolioKPI: jest
            .fn()
            .mockResolvedValue({ data: { kpiId: {} } })
        }
      }
    );
    expect(nextState.payload.data).toEqual('');
  });

  it('thunk action', async () => {
    const nextState = await actionsPortfolio.deletePortfolioKPI({})(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: { data: { portfoliosDetails: {} } }
        } as RootState),
      {
        portfolioService: {
          deletePortfolioKPI: jest.fn().mockResolvedValue({})
        }
      }
    );
    expect(nextState.payload.data).toEqual('');
  });
});
