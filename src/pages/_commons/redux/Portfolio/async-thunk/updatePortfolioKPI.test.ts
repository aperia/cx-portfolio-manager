import store from 'app/_libraries/_dof/core/redux/createAppStore';
import { actionsPortfolio, reducer } from 'pages/_commons/redux/Portfolio';

const initReducer: any = {
  updatePortfolioKPI: {
    data: []
  }
};

describe('redux-store > portfolio > updatePortfolioKPI', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsPortfolio.updatePortfolioKPI.pending('updatePortfolioKPI', {
        id: '',
        name: 'name',
        period: 'period',
        daysPreviously: 'days'
      })
    );
    expect(nextState.updatePortfolioKPI.data).toEqual([]);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsPortfolio.updatePortfolioKPI.fulfilled(
        { data: [{ name: 'name' }] },
        'updatePortfolioKPI',
        {
          id: '',
          name: 'name',
          period: 'period',
          daysPreviously: 'days'
        }
      )
    );
    expect(nextState.updatePortfolioKPI.data).toEqual([{ name: 'name' }]);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsPortfolio.updatePortfolioKPI.rejected(null, 'updatePortfolioKPI', {
        id: '',
        name: 'name',
        period: 'period',
        daysPreviously: 'days'
      })
    );
    expect(nextState.updatePortfolioKPI.data).toEqual({});
  });
  it('thunk action', async () => {
    const nextState = await actionsPortfolio.updatePortfolioKPI({
      id: '',
      name: 'name',
      period: 'period',
      daysPreviously: 'days'
    })(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: { data: { portfoliosDetails: {} } }
        } as RootState),
      {
        portfolioService: {
          updatePortfolioKPI: jest
            .fn()
            .mockResolvedValue({ data: { kpiId: {} } })
        }
      }
    );
    expect(nextState.payload).toBeUndefined();
  });

  it('thunk action with message 200', async () => {
    const nextState = await actionsPortfolio.updatePortfolioKPI({
      id: '',
      name: 'name',
      period: 'period',
      daysPreviously: 'days'
    })(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: { data: { portfoliosDetails: {} } }
        } as RootState),
      {
        portfolioService: {
          updatePortfolioKPI: jest
            .fn()
            .mockResolvedValue({ data: { messages: [{ resultCode: '200' }] } })
        }
      }
    );
    expect(nextState.payload).toBeUndefined();
  });
  it('thunk action with message 404', async () => {
    const nextState = await actionsPortfolio.updatePortfolioKPI({
      id: '',
      name: 'name',
      period: 'period',
      daysPreviously: 'days'
    })(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: { data: { portfoliosDetails: {} } }
        } as RootState),
      {
        portfolioService: {
          updatePortfolioKPI: jest
            .fn()
            .mockResolvedValue({ data: { messages: [{ resultCode: '404' }] } })
        }
      }
    );
    expect(nextState.payload.data).toEqual([{ resultCode: '404' }]);
  });

  it('thunk action with undefined data', async () => {
    const nextState = await actionsPortfolio.updatePortfolioKPI({
      id: '',
      name: 'name',
      period: 'period',
      daysPreviously: 'days'
    })(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: { data: { portfoliosDetails: {} } }
        } as RootState),
      {
        portfolioService: {
          updatePortfolioKPI: jest.fn().mockResolvedValue({})
        }
      }
    );
    expect(nextState.payload).toBeUndefined();
  });
});
