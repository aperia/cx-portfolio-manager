import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import get from 'lodash.get';
import set from 'lodash.set';
import { actionsPortfolio } from '..';
import { PortfolioState } from '../types';

interface PortfolioKpiUpdate {
  id: any;
  name: string;
  period: string;
  daysPreviously: string;
}

export const updatePortfolioKPI = createAsyncThunk<
  any,
  PortfolioKpiUpdate,
  ThunkAPIConfig
>(
  'mapping/updatePortfolioKPI',
  async (portfolioKpiUpdate: IPortfolioKpiUpdate, thunkAPI) => {
    const { dispatch } = thunkAPI;
    const { portfolioService } = get(thunkAPI, 'extra');

    const resp = await portfolioService.updatePortfolioKPI(portfolioKpiUpdate);

    const { messages } = resp?.data || {};

    if (messages[0]?.resultCode === '200') {
      dispatch(
        actionsPortfolio.getKPIDetails({ id: portfolioKpiUpdate.id }) as any
      );
    }

    return { data: messages || '' };
  }
);

export const updatePortfolioKPIBuilder = (
  builder: ActionReducerMapBuilder<PortfolioState>
) => {
  builder.addCase(updatePortfolioKPI.pending, (draftState, action) => {
    set(draftState.updatePortfolioKPI, 'loading', true);
    set(draftState.updatePortfolioKPI, 'error', false);
  });
  builder.addCase(updatePortfolioKPI.fulfilled, (draftState, action) => {
    set(draftState.updatePortfolioKPI, 'loading', false);
    set(draftState.updatePortfolioKPI, 'data', action.payload.data);
  });
  builder.addCase(updatePortfolioKPI.rejected, (draftState, action) => {
    set(draftState.updatePortfolioKPI, 'loading', false);
    set(draftState.updatePortfolioKPI, 'data', {});
    set(draftState.updatePortfolioKPI, 'error', true);
  });
};
