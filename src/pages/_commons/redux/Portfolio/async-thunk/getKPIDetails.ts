import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import get from 'lodash.get';
import { PortfolioState } from '../types';

export const getKPIDetails = createAsyncThunk<
  any,
  { id: number },
  ThunkAPIConfig
>('mapping/getKPIdetails', async (arg, thunkAPI) => {
  const { portfolioService } = get(thunkAPI, 'extra');
  const { id } = arg;

  const resp = await portfolioService.getKPIDetails(id);

  const kpiDetails = resp?.data || {};

  return { data: kpiDetails };
});

export const getKPIDetailsBuilder = (
  builder: ActionReducerMapBuilder<PortfolioState>
) => {
  builder.addCase(getKPIDetails.pending, (draftState, action) => {
    const { id } = action.meta.arg;

    return {
      ...draftState,
      getKPIDetails: {
        ...draftState.getKPIDetails,
        [id]: {
          data: [],
          loading: true
        }
      }
    };
  });
  builder.addCase(getKPIDetails.fulfilled, (draftState, action) => {
    const { data } = action.payload;
    const { id } = action.meta.arg;

    return {
      ...draftState,
      getKPIDetails: {
        ...draftState.getKPIDetails,
        [id]: {
          data: data,
          loading: false
        }
      }
    };
  });
  builder.addCase(getKPIDetails.rejected, (draftState, action) => {
    const { id } = action.meta.arg;

    return {
      ...draftState,
      getKPIDetails: {
        ...draftState.getKPIDetails,
        [id]: {
          data: [],
          loading: true
        }
      }
    };
  });
};
