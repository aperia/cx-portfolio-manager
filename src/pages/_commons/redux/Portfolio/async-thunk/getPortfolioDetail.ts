import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { mappingDataFromObj } from 'app/helpers';
import get from 'lodash.get';
import set from 'lodash.set';
import { PortfolioState } from '../types';

export const getPortfolioDetail = createAsyncThunk<any, string, ThunkAPIConfig>(
  'mapping/gettPortfolioDetail',
  async (portfolioId: string, thunkAPI) => {
    const { portfolioService } = get(thunkAPI, 'extra');

    const resp = await portfolioService.getPortfolioDetail(portfolioId);

    const state = thunkAPI.getState();
    const portfolioDetailMapping = state.mapping?.data?.portfolioDetail;

    const data = mappingDataFromObj(
      resp?.data?.portfolioDetail,
      portfolioDetailMapping!
    );

    const { portfolioStatistic } = resp?.data?.portfolioDetail;
    data.portfolioStatistic = portfolioStatistic;

    return { data };
  }
);

export const getPortfolioDetailBuilder = (
  builder: ActionReducerMapBuilder<PortfolioState>
) => {
  builder.addCase(getPortfolioDetail.pending, (draftState, action) => {
    set(draftState.portfoliosDetails, 'loading', true);
    set(draftState.portfoliosDetails, 'error', false);
  });
  builder.addCase(getPortfolioDetail.fulfilled, (draftState, action) => {
    set(draftState.portfoliosDetails, 'loading', false);
    set(draftState.portfoliosDetails, 'data', action.payload.data);
  });
  builder.addCase(getPortfolioDetail.rejected, (draftState, action) => {
    set(draftState.portfoliosDetails, 'loading', false);
    set(draftState.portfoliosDetails, 'data', {});
    set(draftState.portfoliosDetails, 'error', true);
  });
};
