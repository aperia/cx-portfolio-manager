import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import get from 'lodash.get';
import set from 'lodash.set';
import { PortfolioState } from '../types';

export const deletePortfolioKPI = createAsyncThunk<any, string, ThunkAPIConfig>(
  'mapping/deletePortfolioKPI',
  async (portfolioKpiDelete: string, thunkAPI) => {
    const { portfolioService } = get(thunkAPI, 'extra');

    const resp = await portfolioService.deletePortfolioKPI(portfolioKpiDelete);
    const { messages } = resp?.data || {};

    return { data: messages || '' };
  }
);

export const deletePortfolioKPIBuilder = (
  builder: ActionReducerMapBuilder<PortfolioState>
) => {
  builder.addCase(deletePortfolioKPI.pending, (draftState, action) => {
    set(draftState.deletePortfolioKPI, 'loading', true);
    set(draftState.deletePortfolioKPI, 'error', false);
  });
  builder.addCase(deletePortfolioKPI.fulfilled, (draftState, action) => {
    set(draftState.deletePortfolioKPI, 'loading', false);
    set(draftState.deletePortfolioKPI, 'data', action.payload.data);
  });
  builder.addCase(deletePortfolioKPI.rejected, (draftState, action) => {
    set(draftState.deletePortfolioKPI, 'loading', false);
    set(draftState.deletePortfolioKPI, 'data', {});
    set(draftState.deletePortfolioKPI, 'error', true);
  });
};
