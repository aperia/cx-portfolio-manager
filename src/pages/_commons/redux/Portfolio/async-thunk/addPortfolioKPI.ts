import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import get from 'lodash.get';
import set from 'lodash.set';
import { PortfolioState } from '../types';

interface PortfolioKpiDetails {
  portfolioId: string;
  kpi: string;
  subjectArea: string;
  submetric: string;
  name: string;
  period: string;
  daysPreviously: string;
}

export const addPortfolioKPI = createAsyncThunk<
  any,
  PortfolioKpiDetails,
  ThunkAPIConfig
>(
  'mapping/addPortfolioKPI',
  async (portfolioKpiDetails: PortfolioKpiDetails, thunkAPI) => {
    const { portfolioService } = get(thunkAPI, 'extra');

    const resp = await portfolioService.addPortfolioKPI(portfolioKpiDetails);
    //response on local !== INT
    return {
      data: {
        id: resp?.data?.kpiId || resp?.data?.id,
        kpiDetails: resp?.data?.portfolioKpiDetails
      }
    };
  }
);

export const addPortfolioKPIBuilder = (
  builder: ActionReducerMapBuilder<PortfolioState>
) => {
  builder.addCase(addPortfolioKPI.pending, (draftState, action) => {
    set(draftState.addPortfolioKPI, 'loading', true);
    set(draftState.addPortfolioKPI, 'error', false);
  });
  builder.addCase(addPortfolioKPI.fulfilled, (draftState, action) => {
    set(draftState.addPortfolioKPI, 'loading', false);
    set(draftState.addPortfolioKPI, 'data', action.payload.data);
  });
  builder.addCase(addPortfolioKPI.rejected, (draftState, action) => {
    set(draftState.addPortfolioKPI, 'loading', false);
    set(draftState.addPortfolioKPI, 'data', {});
    set(draftState.addPortfolioKPI, 'error', true);
  });
};
