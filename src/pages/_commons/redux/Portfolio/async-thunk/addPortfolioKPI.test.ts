import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsPortfolio,
  PortfolioState,
  reducer
} from 'pages/_commons/redux/Portfolio';

const initReducer: PortfolioState = {
  addPortfolioKPI: {
    loading: false
  }
};

describe('redux-store > portfolio > addPortfolioKPI', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsPortfolio.addPortfolioKPI.pending('addPortfolioKPI', '')
    );
    expect(nextState.addPortfolioKPI.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsPortfolio.addPortfolioKPI.fulfilled({}, 'addPortfolioKPI', '')
    );
    expect(nextState.addPortfolioKPI.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsPortfolio.addPortfolioKPI.rejected(null, 'addPortfolioKPI', '')
    );
    expect(nextState.addPortfolioKPI.error).toEqual(true);
  });
  it('thunk action', async () => {
    const nextState = await actionsPortfolio.addPortfolioKPI({})(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: { data: { portfoliosDetails: {} } }
        } as RootState),
      {
        portfolioService: {
          addPortfolioKPI: jest.fn().mockResolvedValue({ data: { kpiId: {} } })
        }
      }
    );
    expect(nextState.payload.data).toEqual({ id: {}, kpiDetails: undefined });
  });

  it('thunk action', async () => {
    const nextState = await actionsPortfolio.addPortfolioKPI({})(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: { data: { portfoliosDetails: {} } }
        } as RootState),
      {
        portfolioService: {
          addPortfolioKPI: jest.fn().mockResolvedValue({})
        }
      }
    );
    expect(nextState.payload.data).toEqual({
      id: undefined,
      kpiDetails: undefined
    });
  });
});
