import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsPortfolio,
  defaultDataFilter,
  PortfolioState,
  reducer
} from 'pages/_commons/redux/Portfolio';

const initReducer: PortfolioState = {
  portfoliosData: {
    portfolios: [],
    loading: false,
    error: false,
    dataFilter: defaultDataFilter
  },
  portfoliosDetails: {
    loading: false
  }
};

describe('redux-store > portfolio > getPortfolioDetail', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsPortfolio.getPortfolioDetail.pending('getPortfolioDetail', '')
    );
    expect(nextState.portfoliosDetails.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsPortfolio.getPortfolioDetail.fulfilled(
        {},
        'getPortfolioDetail',
        ''
      )
    );
    expect(nextState.portfoliosDetails.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsPortfolio.getPortfolioDetail.rejected(
        null,
        'getPortfolioDetail',
        ''
      )
    );
    expect(nextState.portfoliosDetails.error).toEqual(true);
  });
  it('thunk action', async () => {
    const nextState = await actionsPortfolio.getPortfolioDetail('123')(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: { data: { portfoliosDetails: {} } }
        } as RootState),
      {
        portfolioService: {
          getPortfolioDetail: jest
            .fn()
            .mockResolvedValue({ data: { portfolioDetail: {} } })
        }
      }
    );
    expect(nextState.payload.data).toEqual({});
  });
});
