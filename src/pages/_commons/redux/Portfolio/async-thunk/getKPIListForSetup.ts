import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import get from 'lodash.get';
import set from 'lodash.set';
import { PortfolioState } from '../types';

export const getKPIListForSetup = createAsyncThunk<any, string, ThunkAPIConfig>(
  'mapping/getKPIListForSetup',
  async (portfolioId: string, thunkAPI) => {
    const { portfolioService } = get(thunkAPI, 'extra');

    const resp = await portfolioService.getKPIListForSetup();

    const kpiLst: IKPIListForSetup[] = (resp?.data?.kpis || []).sort(
      (a: any, b: any) =>
        a.name.toLocaleLowerCase().trim() > b.name.toLocaleLowerCase().trim()
          ? 1
          : -1
    );

    return { data: kpiLst };
  }
);

export const getKPIListForSetupBuilder = (
  builder: ActionReducerMapBuilder<PortfolioState>
) => {
  builder.addCase(getKPIListForSetup.pending, (draftState, action) => {
    set(draftState.getKPIListForSetup, 'loading', true);
    set(draftState.getKPIListForSetup, 'error', false);
  });
  builder.addCase(getKPIListForSetup.fulfilled, (draftState, action) => {
    set(draftState.getKPIListForSetup, 'loading', false);
    set(draftState.getKPIListForSetup, 'data', action.payload.data);
  });
  builder.addCase(getKPIListForSetup.rejected, (draftState, action) => {
    set(draftState.getKPIListForSetup, 'loading', false);
    set(draftState.getKPIListForSetup, 'data', []);
    set(draftState.getKPIListForSetup, 'error', true);
  });
};
