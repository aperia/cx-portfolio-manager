import { PortfolioData, PortfolioMapping } from 'app/fixtures/portfolio-data';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsPortfolio,
  defaultDataFilter,
  PortfolioState,
  reducer
} from 'pages/_commons/redux/Portfolio';

const initReducer: PortfolioState = {
  portfoliosData: {
    portfolios: [],
    loading: false,
    error: false,
    dataFilter: defaultDataFilter
  },
  portfoliosDetails: {
    loading: false
  }
};

describe('redux-store > portfolio > getPortfolioInchange', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsPortfolio.getPortfolioInchange.pending(
        'getPortfolioInchange',
        undefined
      )
    );
    expect(nextState.portfoliosData.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsPortfolio.getPortfolioInchange.fulfilled(
        [],
        'getPortfolioInchange',
        undefined
      )
    );
    expect(nextState.portfoliosData.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsPortfolio.getPortfolioInchange.rejected(
        null,
        'getPortfolioInchange',
        undefined
      )
    );
    expect(nextState.portfoliosData.error).toEqual(true);
  });
  it('thunk action', async () => {
    const nextState = await actionsPortfolio.getPortfolioInchange('123')(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: { data: { portfolioList: PortfolioMapping } }
        } as RootState),
      {
        portfolioService: {
          getPortfolioList: jest
            .fn()
            .mockResolvedValue({ data: { portfolioList: PortfolioData } })
        }
      }
    );
    expect(nextState.payload.portfolios.length).toEqual(2);
  });

  it('thunk action > portfolioList: undefined', async () => {
    const nextState = await actionsPortfolio.getPortfolioInchange('123')(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: { data: { portfolioList: PortfolioMapping } }
        } as RootState),
      {
        portfolioService: {
          getPortfolioList: jest
            .fn()
            .mockResolvedValue({ data: { portfolioList: undefined } })
        }
      }
    );
    expect(nextState.payload.portfolios.length).toEqual(0);
  });
});
