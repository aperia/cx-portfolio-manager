import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { mappingDataAndDynamicFieldsFromArray } from 'app/helpers';
import get from 'lodash.get';
import set from 'lodash.set';
import { PortfolioState } from '../types';

export const getPortfolioInchange = createAsyncThunk<
  any,
  string | undefined,
  ThunkAPIConfig
>('mapping/getPortfolioList', async (changeId, thunkAPI) => {
  const { portfolioService } = get(thunkAPI, 'extra');
  const resp = await portfolioService.getPortfolioList(changeId);

  const state = thunkAPI.getState();
  // Take mapping model
  const portfolioListMapping = state.mapping?.data?.portfolioList;

  const portfolios = resp?.data?.portfolioList;

  if (!portfolios)
    return {
      portfolios: [],
      portfolioCriteriaFields: []
    };

  // Mapping
  const mappedData = mappingDataAndDynamicFieldsFromArray(
    portfolios,
    portfolioListMapping!,
    ['dynamic']
  ) as any;

  const { data, dynamicFields } = mappedData;

  const portfoliosData = data.map((p: any) => {
    if (!p.icon) {
      delete p.icon;
    }
    return p;
  });

  return {
    portfolios: portfoliosData,
    portfolioCriteriaFields: dynamicFields['dynamic']
  };
});
export const getPortfolioInchangeBuilder = (
  builder: ActionReducerMapBuilder<PortfolioState>
) => {
  builder.addCase(getPortfolioInchange.pending, (draftState, action) => {
    set(draftState.portfoliosData, 'portfolios', []);
    set(draftState.portfoliosData, 'portfolioCriteriaFields', []);
    set(draftState.portfoliosData, 'loading', true);
    set(draftState.portfoliosData, 'error', false);
  });
  builder.addCase(getPortfolioInchange.fulfilled, (draftState, action) => {
    const { portfolios, portfolioCriteriaFields } = action.payload;
    set(draftState.portfoliosData, ['loading'], false);
    set(draftState.portfoliosData, ['portfolios'], portfolios);
    set(
      draftState.portfoliosData,
      'portfolioCriteriaFields',
      portfolioCriteriaFields
    );
  });
  builder.addCase(getPortfolioInchange.rejected, (draftState, action) => {
    set(draftState.portfoliosData, 'loading', false);
    set(draftState.portfoliosData, 'error', true);
  });
};
