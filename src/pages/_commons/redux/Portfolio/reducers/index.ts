import { PayloadAction } from '@reduxjs/toolkit';
import { defaultDataFilter, defaultOrderBy, defaultSortBy } from '../types';
import { PortfolioState } from './../types';

const reducers = {
  updateFilter: (
    draftState: PortfolioState,
    action: PayloadAction<IDataFilter>
  ) => {
    const { sortBy, orderBy, page, ...filter } = action.payload;

    const customFilter: IDataFilter = {};
    const hasSort = (sortBy?.length || 0) > 0;
    const hasOrder = (orderBy?.length || 0) > 0;

    if (hasSort) {
      customFilter.sortBy = [...(sortBy || []), ...defaultSortBy];
    }
    if (hasOrder) {
      customFilter.orderBy = [...(orderBy || []), ...defaultOrderBy];
    }

    if (page) {
      customFilter.page = page;
    } else {
      customFilter.page = 1;
    }

    draftState.portfoliosData = {
      ...draftState.portfoliosData,
      dataFilter: {
        ...draftState.portfoliosData.dataFilter,
        ...filter,
        ...customFilter
      }
    };
  },
  updateData: (
    draftState: PortfolioState,
    action: PayloadAction<IPortfolioModel[]>
  ) => {
    draftState.portfoliosData.portfolios = action.payload;
  },
  clearAndReset: (draftState: PortfolioState) => {
    draftState.portfoliosData = {
      ...draftState.portfoliosData,
      dataFilter: {
        ...defaultDataFilter,
        sortBy: draftState.portfoliosData.dataFilter?.sortBy,
        orderBy: draftState.portfoliosData.dataFilter?.orderBy
      }
    };
  },
  setToDefault: (draftState: PortfolioState) => {
    draftState.portfoliosData = {
      ...draftState.portfoliosData,
      portfolios: [],
      loading: true,
      dataFilter: defaultDataFilter
    };
  },
  setOpenedChoosePopup: (draftState: PortfolioState, action: any) => {
    const payload = action.payload;
    draftState.chooseKPI.openedChoosePopup = payload.isChooseOpen;
    draftState.chooseKPI.dataChooseKPI = payload.value;
    draftState.chooseKPI.openedAddPopup = payload.isAddOpen;
  }
};

export default reducers;
