import {
  actionsPortfolio,
  defaultDataFilter,
  PortfolioState,
  reducer
} from 'pages/_commons/redux/Portfolio';

const initReducer: PortfolioState = {
  portfoliosData: {
    portfolios: [],
    loading: false,
    error: false,
    dataFilter: defaultDataFilter
  },
  chooseKPI: {}
};

describe('redux-store > portfolio > reducers', () => {
  it('updateFilter', () => {
    const nextState = reducer(
      initReducer,
      actionsPortfolio.updateFilter({
        ...defaultDataFilter,
        sortBy: ['clientId'],
        orderBy: ['desc'],
        page: 1
      })
    );
    expect(nextState.portfoliosData.dataFilter!.sortBy!.length).toEqual(4);
    expect(nextState.portfoliosData.dataFilter!.orderBy!.length).toEqual(4);
  });

  it('updateFilter > no sortBy, orderBy and page', () => {
    const nextState = reducer(initReducer, actionsPortfolio.updateFilter({}));
    expect(nextState.portfoliosData.dataFilter!.sortBy!.length).toEqual(3);
    expect(nextState.portfoliosData.dataFilter!.orderBy!.length).toEqual(3);
    expect(nextState.portfoliosData.dataFilter!.page).toEqual(1);
  });

  it('updateData', () => {
    const nextState = reducer(initReducer, actionsPortfolio.updateData([]));
    expect(nextState.portfoliosData.portfolios.length).toEqual(0);
  });

  it('clearAndReset', () => {
    const nextState = reducer(initReducer, actionsPortfolio.clearAndReset());
    expect(nextState.portfoliosData.dataFilter!.page).toEqual(1);
  });

  it('setToDefault', () => {
    const nextState = reducer(initReducer, actionsPortfolio.setToDefault());
    expect(nextState.portfoliosData.dataFilter!.page).toEqual(1);
    expect(nextState.portfoliosData.portfolios.length).toEqual(0);
  });

  it('setOpenedChoosePopup', () => {
    const nextState = reducer(
      initReducer,
      actionsPortfolio.setOpenedChoosePopup({
        isChooseOpen: true,
        value: [],
        isAddOpen: false
      })
    );
    expect(nextState.chooseKPI.openedChoosePopup).toEqual(true);
    expect(nextState.chooseKPI.dataChooseKPI).toEqual([]);
    expect(nextState.chooseKPI.openedAddPopup).toEqual(false);
  });
});
