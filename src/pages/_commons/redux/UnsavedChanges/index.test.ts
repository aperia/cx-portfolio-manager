import { renderHook } from '@testing-library/react-hooks';
import {
  actionsUnsavedChanges,
  reducer,
  useAllFormChanges,
  useCurrentIncompletedForm,
  useHasAnyIncompletedForm,
  useShowUnsavedConfirmation,
  useUnsavedRedirectInfo
} from 'pages/_commons/redux/UnsavedChanges';
import * as reactRedux from 'react-redux';

const initReducer: any = {
  redirect: {
    completed: true,
    to: ''
  },
  changes: {}
};

describe('test REDUX > unsaved changes', () => {
  describe('unsaved changes > reducer', () => {
    it('onFormChange reducer', () => {
      const nextState = reducer(
        initReducer,
        actionsUnsavedChanges.onFormChange({
          formName: 'mockForm',
          type: 'unsaved',
          confirmTitle: 'Mock title',
          confirmMessage: null,
          priority: 0
        })
      );
      expect(nextState.changes['mockForm'].change).toEqual(true);
    });

    it('onFormChange reducer > undefined payload', () => {
      const nextState = reducer(
        initReducer,
        actionsUnsavedChanges.onFormChange(undefined as any)
      );
      expect(nextState.changes['mockForm']).toBeUndefined();
    });

    it('updateFormChangeActions reducer', () => {
      const nextState = reducer(
        initReducer,
        actionsUnsavedChanges.updateFormChangeActions({
          formName: 'mockForm'
        })
      );
      expect(nextState.changes['mockForm']).toEqual({});
    });

    it('updateFormChangeActions reducer > undefined payload', () => {
      const nextState = reducer(
        initReducer,
        actionsUnsavedChanges.updateFormChangeActions(undefined as any)
      );
      expect(nextState.changes['mockForm']).toBeUndefined();
    });

    it('resetFormChange reducer', () => {
      const nextState = reducer(
        initReducer,
        actionsUnsavedChanges.resetFormChange('mockForm')
      );
      expect(nextState.changes['mockForm'].change).toEqual(false);
    });

    it('resetFormChange reducer', () => {
      const nextState = reducer(
        initReducer,
        actionsUnsavedChanges.resetFormChange('mockForm')
      );
      expect(nextState.changes['mockForm'].change).toEqual(false);
    });

    it('redirectTo reducer', () => {
      const nextState = reducer(
        initReducer,
        actionsUnsavedChanges.redirectTo({
          to: '/mock-url'
        })
      );
      expect(nextState.redirect).toEqual({
        to: '/mock-url',
        completed: false
      });
    });

    it('resetRedirectInfo reducer', () => {
      const nextState = reducer(
        initReducer,
        actionsUnsavedChanges.resetRedirectInfo()
      );
      expect(nextState.redirect).toEqual({
        to: '',
        completed: true
      });
    });
  });

  describe('unsaved changes > selector', () => {
    const selectorMock = jest.spyOn(reactRedux, 'useSelector');

    it('useHasAnyIncompletedForm selector > empty changes', () => {
      selectorMock.mockImplementation(selector =>
        selector({
          unsavedChanges: {
            changes: {}
          }
        })
      );

      const { result } = renderHook(() => useHasAnyIncompletedForm());

      expect(result.current).toEqual(false);
    });

    it('useHasAnyIncompletedForm selector > has changes', () => {
      selectorMock.mockImplementation(selector =>
        selector({
          unsavedChanges: {
            changes: {
              mockForm: {
                change: true
              }
            }
          }
        })
      );

      const { result } = renderHook(() => useHasAnyIncompletedForm());

      expect(result.current).toEqual(true);
    });

    it('useShowUnsavedConfirmation selector', () => {
      selectorMock.mockImplementation(selector =>
        selector({
          unsavedChanges: {
            changes: {
              mockForm: {
                change: true,
                hideUnsavedConfirmation: false
              }
            }
          }
        })
      );

      const { result } = renderHook(() => useShowUnsavedConfirmation());

      expect(result.current).toEqual(true);
    });

    it('useCurrentIncompletedForm selector', () => {
      selectorMock.mockImplementation(selector =>
        selector({
          unsavedChanges: {
            changes: {
              mockForm: {
                change: true,
                priority: 2,
                formName: 'mockForm',
                confirmTitle: 'Mock title'
              },
              mockForm2: {
                change: true,
                priority: 1,
                formName: 'mockForm2',
                confirmTitle: 'Mock title 2'
              }
            }
          }
        })
      );

      const { result } = renderHook(() =>
        useCurrentIncompletedForm(['mockForm', 'mockForm1'])
      );

      expect(result.current).toEqual({
        change: true,
        priority: 2,
        formName: 'mockForm',
        confirmTitle: 'Mock title'
      });
    });

    it('useAllFormChanges selector', () => {
      selectorMock.mockImplementation(selector =>
        selector({
          unsavedChanges: {
            changes: {
              mockForm: {
                change: true,
                priority: 2,
                formName: 'mockForm',
                confirmTitle: 'Mock title'
              },
              mockForm2: {
                change: true,
                priority: 1,
                formName: 'mockForm2',
                confirmTitle: 'Mock title 2'
              }
            }
          }
        })
      );

      const { result } = renderHook(() => useAllFormChanges());

      expect(result.current).toEqual(['mockForm2', 'mockForm']);
    });

    it('useAllFormChanges selector > with equalityFn funcion', () => {
      selectorMock.mockImplementation((selector, equalityFn) => {
        selector({
          unsavedChanges: {
            changes: {
              mockForm: {
                change: true,
                priority: 2,
                formName: 'mockForm',
                confirmTitle: 'Mock title'
              },
              mockForm2: {
                change: true,
                priority: 1,
                formName: 'mockForm2',
                confirmTitle: 'Mock title 2'
              }
            }
          }
        });
        equalityFn!(['mockForm2', 'mockForm'], ['mockForm2', 'mockForm']);
      });

      const { result } = renderHook(() => useAllFormChanges());

      expect(result.current).toBeUndefined();
    });

    it('useUnsavedRedirectInfo selector', () => {
      selectorMock.mockImplementation(selector => {
        selector({
          unsavedChanges: {
            redirect: undefined
          }
        });
      });

      const { result } = renderHook(() => useUnsavedRedirectInfo());

      expect(result.current).toBeUndefined();
    });
  });
});
