import { createSelector, createSlice, PayloadAction } from '@reduxjs/toolkit';
import {
  UnsavedChangesOptions,
  UnsavedChangesRedirect,
  UnsavedChangesState
} from 'app/types';
import { shallowEqual, useSelector } from 'react-redux';

const { actions, reducer } = createSlice({
  name: 'unsavedChanges',
  initialState: {
    redirect: {
      completed: true,
      to: ''
    },
    changes: {}
  } as UnsavedChangesState,
  reducers: {
    onFormChange: (
      state,
      action: PayloadAction<Omit<UnsavedChangesOptions, 'change'>>
    ) => {
      const { formName, ...rest } = action.payload || {};
      state.changes[formName] = { ...rest, formName, change: true };
    },
    updateFormChangeActions: (
      state,
      action: PayloadAction<
        Pick<
          UnsavedChangesOptions,
          'formName' | 'onDiscard' | 'onClose' | 'onConfirm'
        >
      >
    ) => {
      const { formName, ...rest } = action.payload || {};
      state.changes[formName] = { ...state.changes[formName], ...rest };
    },
    resetFormChange: (state, action: PayloadAction<string>) => {
      state.changes[action.payload] = {
        formName: action.payload,
        change: false
      } as UnsavedChangesOptions;
    },
    redirectTo: (state, action: PayloadAction<UnsavedChangesRedirect>) => {
      state.redirect = { ...action.payload, completed: false };
    },
    resetRedirectInfo: state => {
      state.redirect = { completed: true, to: '' };
    }
  }
});

const useHasAnyIncompletedForm = (): boolean =>
  useSelector<RootState, boolean>(state =>
    Object.values(state.unsavedChanges!.changes).some(v => v.change)
  );

const useShowUnsavedConfirmation = (): boolean =>
  useSelector<RootState, boolean>(
    state =>
      Object.values(state.unsavedChanges!.changes).some(
        v => v.change && !v.hideUnsavedConfirmation
      ),
    shallowEqual
  );

const useCurrentIncompletedForm = (
  formsWatcher: string[] | undefined
): UnsavedChangesOptions =>
  useSelector<RootState, UnsavedChangesOptions>(
    state =>
      Object.values(state.unsavedChanges!.changes)
        .sort((pre, next) => pre.priority - next.priority)
        .find(
          i => (!formsWatcher || formsWatcher.includes(i.formName)) && i.change
        )!
  );

const selectAllFormChanges = createSelector(
  [(state: RootState) => state.unsavedChanges!.changes],
  changes =>
    Object.values(changes)
      .sort((pre, next) => pre.priority - next.priority)
      .filter(i => i.change)
      .map(i => i.formName)
);
const useAllFormChanges = (): string[] =>
  useSelector<RootState, string[]>(
    selectAllFormChanges,
    (left, right) => left.join() === right.join()
  );

const useUnsavedRedirectInfo = (): UnsavedChangesRedirect =>
  useSelector<RootState, UnsavedChangesRedirect>(
    state => state.unsavedChanges!.redirect
  );

export {
  reducer,
  actions as actionsUnsavedChanges,
  useHasAnyIncompletedForm,
  useCurrentIncompletedForm,
  useShowUnsavedConfirmation,
  useAllFormChanges,
  useUnsavedRedirectInfo
};
