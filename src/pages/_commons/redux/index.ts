import { reducer as auth } from 'pages/_commons/Header/redux';
import { reducer as i18next } from 'pages/_commons/I18nextProvider/redux';
import { reducer as mapping } from 'pages/_commons/MappingProvider/redux';
import { reducer as activity } from 'pages/_commons/redux/Activity';
import { reducer as change } from 'pages/_commons/redux/Change';
import { reducer as common } from 'pages/_commons/redux/Common';
import { reducer as dmmTables } from 'pages/_commons/redux/DMMTables';
import { reducer as modals } from 'pages/_commons/redux/modal';
import { reducer as notification } from 'pages/_commons/redux/Notification';
import { reducer as portfolio } from 'pages/_commons/redux/Portfolio';
import { reducer as pricingMethods } from 'pages/_commons/redux/PricingMethod';
import { reducer as pricingStrategies } from 'pages/_commons/redux/PricingStrategy';
import { reducer as refData } from 'pages/_commons/redux/RefData';
import { reducer as unsavedChanges } from 'pages/_commons/redux/UnsavedChanges';
import { reducer as workflow } from 'pages/_commons/redux/Workflow';
import { reducer as workflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import { reducer as toastNotifications } from 'pages/_commons/ToastNotifications/redux';
import { combineReducers } from 'redux';

export const reducerMappingList = {
  common,
  mapping,
  refData,
  toastNotifications,
  change,
  activity,
  portfolio,
  workflow,
  pricingMethods,
  pricingStrategies,
  i18next,
  modals,
  notification,
  dmmTables,
  auth,
  workflowSetup,
  unsavedChanges
};

export const rootReducer = combineReducers(reducerMappingList);

export type AppState = ReturnType<typeof rootReducer>;
