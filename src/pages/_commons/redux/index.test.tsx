import { renderComponent } from 'app/utils';
import React from 'react';
import { Provider, useSelector } from 'react-redux';
import { createStore } from 'redux';
import { rootReducer } from '.';

describe('Test Redux Common', () => {
  it('Redux common', async () => {
    const store = createStore(rootReducer, {
      common: { window: { width: 1920 } }
    });
    const ComponentFake = () => {
      const width = useSelector(
        (state: AppState) => state.common?.window.width
      );
      return <div>{width}</div>;
    };
    const AppFake = () => (
      <Provider store={store}>
        <ComponentFake />
      </Provider>
    );

    const { getByText } = await renderComponent('app-fake', <AppFake />);
    expect(getByText(/1920/)).toBeInTheDocument();
  });
});
