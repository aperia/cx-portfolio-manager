import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import set from 'lodash.set';
export interface WindowDimension {
  width: number;
  height: number;
}
export interface CommonState {
  window: WindowDimension;
}
const { actions, reducer } = createSlice({
  name: 'common',
  initialState: {
    window: {}
  } as CommonState,
  reducers: {
    updateWindowDimension: (
      draftState,
      action: PayloadAction<WindowDimension>
    ) => {
      const { payload: window } = action;
      set(draftState, 'window', window);
    }
  }
});

const allActions = {
  ...actions
};

export * from './select-hooks';
export { allActions as commonActions, reducer };
