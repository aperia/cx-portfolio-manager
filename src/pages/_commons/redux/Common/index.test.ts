import { CommonState } from 'app/types';
import { commonActions, reducer } from 'pages/_commons/redux/Common';

const initReducer = {
  window: {
    width: 0,
    height: 0
  }
} as CommonState;

describe('common > reducers', () => {
  it('updateWindowDimension', () => {
    const nextState = reducer(
      initReducer,
      commonActions.updateWindowDimension({
        width: 100,
        height: 100
      })
    );
    expect(nextState.window.width).toEqual(100);
  });
});
