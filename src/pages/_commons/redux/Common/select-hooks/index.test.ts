import { renderHook } from '@testing-library/react-hooks';
import { CommonState } from 'app/types';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import * as reactRedux from 'react-redux';

describe('select-hook -> common', () => {
  let selectorMock: jest.SpyInstance;
  const mockSelectorCommon = (commonState: CommonState) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector => selector({ common: commonState }));
  };

  afterEach(() => {
    selectorMock.mockClear();
  });

  it('useSelectWindowDimension', () => {
    const pre = {
      window: {
        width: 100,
        height: 100
      }
    } as any;

    mockSelectorCommon(pre);
    const { result } = renderHook(() => useSelectWindowDimension());

    expect(result.current.width).toEqual(100);
  });
});
