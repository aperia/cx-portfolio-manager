import { useSelector } from 'react-redux';
import { WindowDimension } from '../index';

export const useSelectWindowDimension = () => {
  return useSelector<RootState, WindowDimension>(state => state.common!.window);
};
