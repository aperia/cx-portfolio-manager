import store from 'app/_libraries/_dof/core/redux/createAppStore';
import { getRefDataAction, reducer } from 'pages/_commons/redux/RefData';
import { RefDataState } from 'pages/_commons/redux/RefData/types';

const initReducer: RefDataState = {
  loading: false,
  data: [],
  error: false
};

describe('refData > redux-store ', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      getRefDataAction.getStateRefDataRequest.pending('', undefined)
    );
    expect(nextState.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      getRefDataAction.getStateRefDataRequest.fulfilled(
        { data: [] },
        '',
        undefined
      )
    );
    expect(nextState.loading).toEqual(false);
    expect(nextState.data).toEqual([]);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      getRefDataAction.getStateRefDataRequest.rejected(null, '', undefined)
    );
    expect(nextState.loading).toEqual(false);
  });
  it('thunk action', async () => {
    const expectData = { data: [] } as any;
    const nextState = await getRefDataAction.getStateRefDataRequest({})(
      store.dispatch,
      store.getState,
      {
        refDataService: {
          getStateRefData: jest
            .fn()
            .mockResolvedValue({ data: { stateRefData: [] } })
        }
      }
    );
    expect(nextState.payload).toEqual(expectData);
  });
});
