import { createSlice } from '@reduxjs/toolkit';
import {
  getStateRefDataBuilder,
  getStateRefDataRequest
} from './getStateRefData';
import { RefDataState } from './types';

export interface IRemoveAccountSearchTabPayload {
  storeId: string;
}

const { actions, reducer } = createSlice({
  name: 'refData',
  initialState: {} as RefDataState,
  reducers: {},
  extraReducers: builder => {
    getStateRefDataBuilder(builder);
  }
});

const allActions = {
  ...actions,
  getStateRefDataRequest
};

export { allActions as getRefDataAction, reducer };
