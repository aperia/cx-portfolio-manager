export interface RefDataState {
  error: boolean;
  loading: boolean;
  data: Record<string, any>[];
}
