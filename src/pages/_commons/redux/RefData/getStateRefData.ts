// import { getRefDataRequest } from './getMoreAccountInfo';
import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import get from 'lodash.get';
import set from 'lodash.set';

/**
 * GET search account number
 */
export const getStateRefDataRequest = createAsyncThunk<
  any,
  any,
  ThunkAPIConfig
>('referenceData', async (args: {}, thunkAPI) => {
  const { refDataService } = thunkAPI?.extra;
  const { data } = await refDataService.getStateRefData();
  const stateRefData = get(data, ['stateRefData'], []);
  return {
    data: stateRefData
  };
});

/**
 * EXTRA REDUCERS BUILDER: GET search account number
 */
export const getStateRefDataBuilder = (
  builder: ActionReducerMapBuilder<any>
) => {
  builder
    .addCase(getStateRefDataRequest.pending, draftState => {
      set(draftState, ['loading'], true);
    })
    .addCase(getStateRefDataRequest.fulfilled, (draftState, action) => {
      const { data } = action.payload;

      set(draftState, ['loading'], false);

      set(draftState, ['data'], data);
    })
    .addCase(getStateRefDataRequest.rejected, (draftState, action) => {
      set(draftState, ['loading'], false);
    });
};
