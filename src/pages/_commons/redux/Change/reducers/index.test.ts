import { actionsChange, reducer } from 'pages/_commons/redux/Change';
import {
  defaultApprovalQueuesDataFilter,
  defaultChangeManagementDataFilter
} from 'pages/_commons/redux/Change/reducers';

const initReducer: any = {
  lastUpdatedChanges: {
    changes: [],
    loading: true,
    dataFilter: defaultChangeManagementDataFilter
  },
  approvalQueues: {
    data: [],
    loading: false,
    error: false,
    dataFilter: defaultApprovalQueuesDataFilter
  },
  changeDataExport: {
    exporting: false
  }
};

describe('change > reducers', () => {
  it('update change management filter', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.updateChangeManagementFilter({
        searchValue: 'december',
        page: 2,
        sortBy: ['dateChanged'],
        orderBy: ['asc']
      })
    );
    expect(nextState.lastUpdatedChanges.dataFilter!.page).toEqual(2);
    expect(nextState.lastUpdatedChanges.dataFilter!.sortBy).toEqual([
      'dateChanged',
      'effectiveDate',
      'changeName'
    ]);
    expect(nextState.lastUpdatedChanges.dataFilter!.orderBy).toEqual([
      'asc',
      'desc',
      'asc'
    ]);
    expect(nextState.lastUpdatedChanges.dataFilter!.searchValue).toEqual(
      'december'
    );
  });
  it('update change management filter => empty sortby, orderby, paging', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.updateChangeManagementFilter({
        page: undefined,
        sortBy: undefined,
        orderBy: undefined
      })
    );
    expect(nextState.lastUpdatedChanges.dataFilter!.page).toEqual(1);
    expect(nextState.lastUpdatedChanges.dataFilter!.sortBy).toEqual([
      'effectiveDate',
      'effectiveDate',
      'changeName'
    ]);
    expect(nextState.lastUpdatedChanges.dataFilter!.orderBy).toEqual([
      'desc',
      'desc',
      'asc'
    ]);
  });
  it('update change management filter > clear & reset', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.clearAndResetChangeManagement()
    );
    expect(nextState.lastUpdatedChanges.dataFilter!.changeStatus).toEqual(
      'All'
    );
    const nextState1 = reducer(
      initReducer,
      actionsChange.setChangeManagementToDefault()
    );
    expect(nextState1.lastUpdatedChanges.dataFilter!.changeStatus).toEqual(
      'All'
    );
  });
  it('change snapshot detail > set to default & clear export data', () => {
    const nextState = reducer(initReducer, actionsChange.clearExportData());

    expect(nextState.changeDataExport.exporting).toEqual(false);
  });
});

describe('approval queue > reducers', () => {
  it('update approval queue filter', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.updateApprovalQueuesFilter({
        searchValue: 'december',
        page: 3,
        sortBy: ['dateChanged'],
        orderBy: ['asc']
      })
    );
    expect(nextState.approvalQueues.dataFilter!.page).toEqual(3);
    expect(nextState.approvalQueues.dataFilter!.sortBy).toEqual([
      'dateChanged',
      'effectiveDate',
      'changeName'
    ]);
    expect(nextState.approvalQueues.dataFilter!.orderBy).toEqual([
      'asc',
      'desc',
      'asc'
    ]);
    expect(nextState.approvalQueues.dataFilter!.searchValue).toEqual(
      'december'
    );
  });
  it('update approval queue filter -> empty sortby, orderby, paging', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.updateApprovalQueuesFilter({
        page: undefined,
        sortBy: undefined,
        orderBy: undefined
      })
    );
    expect(nextState.approvalQueues.dataFilter!.page).toEqual(1);
    expect(nextState.approvalQueues.dataFilter!.sortBy).toEqual([
      'effectiveDate',
      'effectiveDate',
      'changeName'
    ]);
    expect(nextState.approvalQueues.dataFilter!.orderBy).toEqual([
      'desc',
      'desc',
      'asc'
    ]);
  });
  it('update approval queue filter > clear & reset', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.clearAndResetApprovalQueuesFilter()
    );
    expect(nextState.approvalQueues.dataFilter!.searchValue).toEqual('');
    const nextState1 = reducer(
      initReducer,
      actionsChange.setToDefaultApprovalQueues()
    );
    expect(nextState1.approvalQueues.dataFilter!.searchValue).toEqual('');
  });
  it('update change detail > set to default', () => {
    const nextState = reducer(initReducer, actionsChange.setToDefault());
    expect(nextState.changeDetails.loading).toEqual(true);
  });

  it('closeWorkflowDetail', () => {
    const nextState = reducer(initReducer, actionsChange.closeWorkflowDetail());
    expect(nextState.workflowDetailId).toEqual('');
  });

  it('openWorkflowDetail', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.openWorkflowDetail('1000')
    );
    expect(nextState.workflowDetailId).toEqual('1000');
  });

  it('updateExpandingChange', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.updateExpandingChange('1000')
    );
    expect(nextState.lastUpdatedChanges.expandingChange).toEqual('1000');
  });

  it('updateExpandingChange with undefined value', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.updateExpandingChange(undefined as any)
    );
    expect(nextState.lastUpdatedChanges.expandingChange).toEqual('');
  });

  it('updateExpandingApproval', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.updateExpandingApproval('1000')
    );
    expect(nextState.approvalQueues.expandingApproval).toEqual('1000');
  });

  it('updateExpandingApproval with undefined value', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.updateExpandingApproval(undefined as any)
    );
    expect(nextState.approvalQueues.expandingApproval).toEqual('');
  });

  it('setShowChangeNotFoundModal', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.setShowChangeNotFoundModal(true)
    );
    expect(nextState.approvalQueues.expandingApproval).toEqual(undefined);
  });
});
