import { PayloadAction } from '@reduxjs/toolkit';
import { CHANGE_FILTER_BY } from 'app/constants/constants';
import {
  ChangeManagementSortByFields,
  OrderBy,
  PendingApprovalQueuesSortByFields
} from 'app/constants/enums';
import set from 'lodash.set';
import {
  ChangeState,
  IChangeManagementFilter,
  IPendingApprovalsFilter
} from '../types';

const reducers = {
  updateApprovalQueuesFilter: (
    draftState: ChangeState,
    action: PayloadAction<IPendingApprovalsFilter>
  ) => {
    const { sortBy, orderBy, page, ...filter } = action.payload;

    const customFilter: IDataFilter = {};
    const hasSort = (sortBy?.length || 0) > 0;
    const hasOrder = (orderBy?.length || 0) > 0;
    if (hasSort) {
      customFilter.sortBy = [...sortBy!, ...defaultApprovalQueuesSortBy];
    }
    if (hasOrder) {
      customFilter.orderBy = [...orderBy!, ...defaultApprovalQueuesOrderBy];
    }
    customFilter.page = page || 1;

    draftState.approvalQueues = {
      ...draftState.approvalQueues,
      dataFilter: {
        ...draftState.approvalQueues.dataFilter,
        ...filter,
        ...customFilter
      }
    };
  },
  clearAndResetApprovalQueuesFilter: (draftState: ChangeState) => {
    draftState.approvalQueues = {
      ...draftState.approvalQueues,
      dataFilter: {
        ...defaultApprovalQueuesDataFilter,
        sortBy: draftState.approvalQueues.dataFilter?.sortBy,
        orderBy: draftState.approvalQueues.dataFilter?.orderBy
      }
    };
  },
  setToDefaultApprovalQueues: (draftState: ChangeState) => {
    draftState.approvalQueues = {
      data: [],
      changeTypeList: [],
      loading: false,
      error: false,
      dataFilter: defaultApprovalQueuesDataFilter
    };
  },
  setToDefault: (draftState: ChangeState) => {
    draftState.changeDetails = {
      ...draftState.changeDetails,
      data: {} as any,
      loading: true
    };
  },
  clearExportData: (draftState: ChangeState) => {
    set(draftState.changeDataExport, 'exporting', false);
  },
  updateChangeManagementFilter: (
    draftState: ChangeState,
    action: PayloadAction<IChangeManagementFilter>
  ) => {
    const { sortBy, orderBy, page, ...filter } = action.payload;

    const customFilter: IDataFilter = {};
    const hasSort = (sortBy?.length || 0) > 0;
    const hasOrder = (orderBy?.length || 0) > 0;
    if (hasSort) {
      customFilter.sortBy = [...sortBy!, ...defaultChangeManagementSortBy];
    }
    if (hasOrder) {
      customFilter.orderBy = [...orderBy!, ...defaultChangeManagementOrderBy];
    }
    customFilter.page = page || 1;

    draftState.lastUpdatedChanges = {
      ...draftState.lastUpdatedChanges,
      dataFilter: {
        ...draftState.lastUpdatedChanges.dataFilter,
        ...filter,
        ...customFilter
      }
    };
  },
  clearAndResetChangeManagement: (draftState: ChangeState) => {
    draftState.lastUpdatedChanges = {
      ...draftState.lastUpdatedChanges,
      dataFilter: {
        ...defaultChangeManagementDataFilter,
        sortBy: draftState.lastUpdatedChanges.dataFilter?.sortBy,
        orderBy: draftState.lastUpdatedChanges.dataFilter?.orderBy
      }
    };
  },
  setChangeManagementToDefault: (draftState: ChangeState) => {
    draftState.lastUpdatedChanges = {
      ...draftState.lastUpdatedChanges,
      changes: [],
      loading: true,
      dataFilter: { ...defaultChangeManagementDataFilter }
    };
  },
  openWorkflowDetail: (
    draftState: ChangeState,
    action: PayloadAction<string>
  ) => {
    const workflowId = action.payload;
    draftState.workflowDetailId = workflowId;
  },
  closeWorkflowDetail: (draftState: ChangeState) => {
    draftState.workflowDetailId = '';
  },
  updateExpandingChange: (
    draftState: ChangeState,
    action: PayloadAction<string>
  ) => {
    const current = draftState.lastUpdatedChanges.expandingChange;
    const changeId = action.payload === current ? '' : action.payload;
    set(draftState.lastUpdatedChanges, 'expandingChange', changeId);
  },
  updateExpandingApproval: (
    draftState: ChangeState,
    action: PayloadAction<string>
  ) => {
    const current = draftState.approvalQueues.expandingApproval;
    const changeId = action.payload === current ? '' : action.payload;
    set(draftState.approvalQueues, 'expandingApproval', changeId);
  },
  setShowChangeNotFoundModal: (
    draftState: ChangeState,
    action: PayloadAction<boolean>
  ) => {
    draftState.showChangeNotFoundModal = action.payload;
  }
};

const defaultChangeManagementSortBy: string[] = [
  ChangeManagementSortByFields.EFFECTIVE_DATE,
  ChangeManagementSortByFields.CHANGE_NAME
];
const defaultChangeManagementOrderBy: OrderByType[] = [
  OrderBy.DESC,
  OrderBy.ASC
];

export const defaultChangeManagementDataFilter: IChangeManagementFilter = {
  page: 1,
  pageSize: 10,
  searchValue: '',
  sortBy: [
    ChangeManagementSortByFields.EFFECTIVE_DATE,
    ...defaultChangeManagementSortBy
  ],
  orderBy: [OrderBy.DESC, ...defaultChangeManagementOrderBy],
  filterField: CHANGE_FILTER_BY[1].value,
  filterValue: {},

  changeStatus: 'All',
  changeType: 'All'
};

const defaultApprovalQueuesSortBy: string[] = [
  PendingApprovalQueuesSortByFields.EFFECTIVE_DATE,
  PendingApprovalQueuesSortByFields.CHANGE_NAME
];
const defaultApprovalQueuesOrderBy: OrderByType[] = [OrderBy.DESC, OrderBy.ASC];

export const defaultApprovalQueuesDataFilter: IPendingApprovalsFilter = {
  page: 1,
  pageSize: 10,
  searchValue: '',
  sortBy: [
    PendingApprovalQueuesSortByFields.EFFECTIVE_DATE,
    ...defaultApprovalQueuesSortBy
  ],
  orderBy: [OrderBy.DESC, ...defaultApprovalQueuesOrderBy],

  changeType: 'All'
};

export default reducers;
