import { createSlice } from '@reduxjs/toolkit';
import {
  approveChange,
  approveChangeBuilder,
  deleteChange,
  deleteChangeBuilder,
  demoteChange,
  demoteChangeBuilder,
  exportChangeSet,
  exportChangeSetBuilder,
  getApprovalQueueList,
  getApprovalQueueListBuilder,
  getAttachmentList,
  getAttachmentListBuilder,
  getChangeDateInfo,
  getChangeDateInfoBuilder,
  getChangeDetail,
  getChangeDetailBuilder,
  getChangeOwners,
  getChangeOwnersBuilder,
  getLastUpdatedApprovalQueues,
  getLastUpdatedApprovalQueuesBuilder,
  getLastUpdatedChanges,
  getLastUpdatedChangesBuilder,
  getPendingApprovalGroup,
  getPendingApprovalGroupBuilder,
  rejectChange,
  rejectChangeBuilder,
  rescheduleChange,
  rescheduleChangeBuilder,
  submitChange,
  submitChangeBuilder,
  updateChangeDetail,
  updateChangeDetailBuilder,
  validateChange,
  validateChangeBuilder
} from './async-thunk';
import reducers, {
  defaultApprovalQueuesDataFilter,
  defaultChangeManagementDataFilter
} from './reducers';
import { ChangeState } from './types';

const { actions, reducer } = createSlice({
  name: 'change',
  initialState: {
    lastUpdatedChanges: {
      changes: [],
      loading: true,
      dataFilter: defaultChangeManagementDataFilter,
      error: false,
      changeTypeList: [],
      changeStatusList: []
    },
    lastUpdatedApprovalQueues: {
      approvalQueues: [],
      loading: true,
      error: false
    },
    changeDetails: { loading: true, error: false },
    changeDetail: {},
    changeOwners: { data: [], loading: false, error: false },
    changeDateInfo: { data: {}, loading: false, error: false },
    changeUpdateStatus: { loading: false, error: false },
    changeDataExport: { loading: false, error: false, exporting: false },
    changeApproveStatus: { loading: false, error: false },
    changeRejectStatus: { loading: false, error: false },
    changeDemoteStatus: { loading: false, error: false },
    changeSubmitStatus: { loading: false, error: false },
    approvalQueues: {
      data: [],
      changeTypeList: [],
      loading: false,
      error: false,
      dataFilter: defaultApprovalQueuesDataFilter
    },
    changeAttachmentList: { loading: false, data: {}, error: false },
    changeValidateStatus: { loading: false, error: false },
    changeDeleteStatus: { loading: false, error: false },
    pendingApprovalByGroup: { loading: false, error: false }
  } as ChangeState,
  reducers,
  extraReducers: builder => {
    getLastUpdatedChangesBuilder(builder);
    getLastUpdatedApprovalQueuesBuilder(builder);
    getChangeDetailBuilder(builder);
    getChangeOwnersBuilder(builder);
    getChangeDateInfoBuilder(builder);
    updateChangeDetailBuilder(builder);
    exportChangeSetBuilder(builder);
    rescheduleChangeBuilder(builder);
    getApprovalQueueListBuilder(builder);
    approveChangeBuilder(builder);
    rejectChangeBuilder(builder);
    demoteChangeBuilder(builder);
    getAttachmentListBuilder(builder);
    validateChangeBuilder(builder);
    deleteChangeBuilder(builder);
    submitChangeBuilder(builder);
    getPendingApprovalGroupBuilder(builder);
  }
});

const extraActions = {
  ...actions,
  getLastUpdatedChanges,
  getLastUpdatedApprovalQueues,
  getChangeDetail,
  getChangeOwners,
  getChangeDateInfo,
  updateChangeDetail,
  exportChangeSet,
  rescheduleChange,
  getApprovalQueueList,
  approveChange,
  rejectChange,
  demoteChange,
  validateChange,
  deleteChange,
  getAttachmentList,
  submitChange,
  getPendingApprovalGroup
};

export * from './select-hooks';
export * from './types';
export { extraActions as actionsChange, reducer };
