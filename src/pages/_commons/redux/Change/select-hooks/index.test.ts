import { renderHook } from '@testing-library/react-hooks';
import { ChangeType } from 'app/constants/enums';
import { ChangeList, ChangeMapping } from 'app/fixtures/change-data';
import { mappingDataFromArray } from 'app/helpers';
import {
  ChangeState,
  useSelectApprovalByGroup,
  useSelectApprovalQueues,
  useSelectApprovalQueuesFilter,
  useSelectApprovalQueuesStatus,
  useSelectChangeDateInfo,
  useSelectChangeManagementFilter,
  useSelectChangeOwners,
  useSelectChangesManagement,
  useSelectCurrentChangeDetail,
  useSelectCurrentChangeInvalidMessages,
  useSelectDataForExport,
  useSelectDataForExportError,
  useSelectDataForExportLoading,
  useSelectExpandingApproval,
  useSelectExpandingChange,
  useSelectLastUpdatedApprovalQueues,
  useSelectLastUpdatedChanges,
  useSelectLoadingOnApproveChange,
  useSelectLoadingOnDemoteChange,
  useSelectLoadingOnRejectChange,
  useSelectLoadingOnSubmitChange,
  useSelectLoadingOnUpdateChange,
  useSelectOpenWorkflowDetail,
  useSelectWorkflowsForExport,
  useShowChangeNotFoundModal
} from 'pages/_commons/redux/Change';
import {
  defaultApprovalQueuesDataFilter,
  defaultChangeManagementDataFilter
} from 'pages/_commons/redux/Change/reducers';
import * as reactRedux from 'react-redux';

describe('select-hooks > change', () => {
  let mockChangeListData: any[] = [];

  let selectorMock: jest.SpyInstance;
  const mockSelectorChanges = (changeState: ChangeState) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector => selector({ change: changeState }));
  };
  const mockSelectorExportChanges = (state: RootState) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector => selector(state));
  };
  beforeEach(() => {
    mockChangeListData = mappingDataFromArray(ChangeList, ChangeMapping)?.map(
      (p: IChangeDetail) => {
        if (p.changeType !== ChangeType.Pricing) {
          delete p.pricingStrategiesCreatedCount;
          delete p.pricingStrategiesUpdatedCount;
          delete p.pricingMethodsCreatedCount;
          delete p.pricingMethodsUpdatedCount;
          delete p.noOfDMMTablesCreated;
          delete p.noOfDMMTablesUpdated;
        } else {
          delete p.cardholdersAccountsImpacted;
          delete p.portfoliosImpacted;
        }
        return p;
      }
    );
  });
  afterEach(() => {
    selectorMock.mockClear();
  });

  it('useSelectLastUpdatedChanges', () => {
    const pre = {
      lastUpdatedChanges: {
        changes: [{}]
      }
    } as any;

    mockSelectorChanges(pre);
    const { result } = renderHook(() => useSelectLastUpdatedChanges());

    expect(result.current.changes.length).toEqual(1);
  });
  it('useSelectApprovalQueues -> search -> have data', () => {
    const pre = {
      approvalQueues: {
        data: mockChangeListData,
        dataFilter: {
          ...defaultApprovalQueuesDataFilter,
          searchValue: ''
        }
      }
    } as any;

    mockSelectorChanges(pre);
    const { result } = renderHook(() => useSelectApprovalQueues());

    expect(result.current.total).toEqual(3);
  });
  it('useSelectApprovalQueues -> search ->  dataFilter is undefined', () => {
    const pre = {
      approvalQueues: {
        data: mockChangeListData,
        dataFilter: undefined
      }
    } as any;

    mockSelectorChanges(pre);
    const { result } = renderHook(() => useSelectApprovalQueues());

    expect(result.current.total).toEqual(0);
  });
  it('useSelectApprovalQueues -> search -> no data', () => {
    const pre = {
      approvalQueues: {
        data: mockChangeListData,
        dataFilter: {
          ...defaultApprovalQueuesDataFilter,
          changeType: 'pricing',
          searchValue: 'abcdef'
        }
      }
    } as any;

    mockSelectorChanges(pre);
    const { result } = renderHook(() => useSelectApprovalQueues());

    expect(result.current.total).toEqual(0);
  });

  it('useSelectApprovalQueuesStatus', () => {
    const pre = {
      approvalQueues: {
        loading: true,
        error: false
      }
    } as any;

    mockSelectorChanges(pre);
    const { result } = renderHook(() => useSelectApprovalQueuesStatus());

    expect(result.current.loading).toEqual(true);
    expect(result.current.error).toEqual(false);
  });
  it('useSelectApprovalQueuesFilter', () => {
    const pre = {
      approvalQueues: {
        dataFilter: defaultApprovalQueuesDataFilter
      }
    } as any;

    mockSelectorChanges(pre);
    const { result } = renderHook(() => useSelectApprovalQueuesFilter());

    expect(result.current.changeType).toEqual('All');
  });
  it('useSelectLastUpdatedApprovalQueues', () => {
    const pre = {
      lastUpdatedApprovalQueues: {
        approvalQueues: [{}]
      }
    } as any;

    mockSelectorChanges(pre);
    const { result } = renderHook(() => useSelectLastUpdatedApprovalQueues());

    expect(result.current.approvalQueues.length).toEqual(1);
  });
  it('useSelectCurrentChangeDetail', () => {
    const pre = {
      changeDetails: {
        data: { changeId: '10000001' }
      }
    } as any;

    mockSelectorChanges(pre);
    const { result } = renderHook(() => useSelectCurrentChangeDetail());

    expect(result.current.data!.changeId).toEqual('10000001');
  });
  it('useSelectChangeOwners', () => {
    const pre = {
      changeOwners: {
        data: [{ code: '001', text: 'John cena' }]
      }
    } as any;

    mockSelectorChanges(pre);
    const { result } = renderHook(() => useSelectChangeOwners());

    expect(result.current.data[0].text).toEqual('John cena');
  });
  it('useSelectChangeDateInfo', () => {
    const pre = {
      changeDateInfo: {
        data: {
          minDate: '2021-02-03'
        }
      }
    } as any;

    mockSelectorChanges(pre);
    const { result } = renderHook(() => useSelectChangeDateInfo());

    expect(result.current.data.minDate).toEqual('2021-02-03');
  });
  it('useSelectLoadingOnUpdateChange', () => {
    const pre = {
      changeUpdateStatus: {
        loading: true
      }
    } as any;

    mockSelectorChanges(pre);
    const { result } = renderHook(() => useSelectLoadingOnUpdateChange());

    expect(result.current).toEqual(true);
  });
  it('useSelectWorkflowsForExport', () => {
    jest.spyOn(reactRedux, 'useSelector').mockImplementation(selector =>
      selector({
        workflow: {
          changeWorkflows: {
            workflows: [
              {
                id: '1001'
              }
            ]
          }
        }
      })
    );
    const { result } = renderHook(() => useSelectWorkflowsForExport());

    expect(result.current.workflowList!.length).toEqual(1);
  });
  it('useSelectDataForExport', () => {
    const pre = {
      change: {
        changeDetails: {
          data: {
            changeId: '10000001'
          }
        },
        changeAttachmentList: {
          data: {
            attachments: [],
            reports: []
          }
        }
      },
      workflow: {
        changeWorkflows: {
          workflows: [{}]
        }
      },
      portfolio: {
        portfoliosData: {
          portfolios: []
        }
      },
      pricingStrategies: {
        pricingStrategyData: {
          pricingStrategies: []
        }
      },
      pricingMethods: {
        changePricingMethods: {
          pricingMethods: []
        }
      },
      activity: {
        activitiesInChange: {
          activities: []
        }
      }
    } as any;

    mockSelectorExportChanges(pre);

    const { result } = renderHook(() => useSelectDataForExport());

    expect(result.current.snapshot!.changeId).toEqual('10000001');
  });
  it('useSelectDataForExportLoading', () => {
    const pre = {
      changeDataExport: {
        loading: true
      }
    } as any;

    mockSelectorChanges(pre);
    const { result } = renderHook(() => useSelectDataForExportLoading());

    expect(result.current).toEqual(true);
  });
  it('useSelectChangeManagementFilter', () => {
    const pre = {
      lastUpdatedChanges: {
        dataFilter: {
          ...defaultChangeManagementDataFilter,
          searchValue: 'august'
        }
      }
    } as any;

    mockSelectorChanges(pre);
    const { result } = renderHook(() => useSelectChangeManagementFilter());

    expect(result.current.searchValue).toEqual('august');
  });

  it('useSelectExpandingApproval', () => {
    const pre = {
      approvalQueues: {
        expandingApproval: 'something'
      }
    } as any;

    mockSelectorChanges(pre);
    const { result } = renderHook(() => useSelectExpandingApproval());

    expect(result.current).toEqual('something');
  });

  describe('useSelectChangesManagement', () => {
    it('filter dateRange has value', () => {
      const pre = {
        lastUpdatedChanges: {
          changes: [
            ...mockChangeListData,
            {
              ...mockChangeListData[0],
              changeType: 'abc',
              changeStatus: 'all'
            }
          ],
          dataFilter: {
            ...defaultChangeManagementDataFilter,
            changeType: 'abc',
            filterValue: {
              start: 1,
              end: new Date().valueOf()
            }
          }
        }
      } as any;

      mockSelectorChanges(pre);
      const { result } = renderHook(() => useSelectChangesManagement());

      expect(result.current.totalItem).toEqual(1);
    });

    it('Pass with case no value with dateRange empty start or end', () => {
      const pre = {
        lastUpdatedChanges: {
          changes: [
            ...mockChangeListData,
            {
              ...mockChangeListData[0],
              changeType: 'abc',
              changeStatus: 'all'
            }
          ],
          dataFilter: {
            ...defaultChangeManagementDataFilter,
            changeType: 'abc',
            filterField: '',
            filterValue: {
              end: new Date().valueOf()
            }
          }
        }
      } as any;

      mockSelectorChanges(pre);
      const { result } = renderHook(() => useSelectChangesManagement());

      expect(result.current.totalItem).toEqual(1);
    });

    it('Pass with case no value with filterField empty', () => {
      const pre = {
        lastUpdatedChanges: {
          changes: [
            ...mockChangeListData,
            {
              ...mockChangeListData[0],
              changeType: 'abc',
              changeStatus: 'all'
            }
          ],
          dataFilter: {
            ...defaultChangeManagementDataFilter,
            changeType: 'abc',
            filterField: '',
            filterValue: {
              start: 1,
              end: new Date().valueOf()
            }
          }
        }
      } as any;

      mockSelectorChanges(pre);
      const { result } = renderHook(() => useSelectChangesManagement());

      expect(result.current.totalItem).toEqual(0);
    });
  });
  it('useSelectChangesManagement -> search -> have data', () => {
    const pre = {
      lastUpdatedChanges: {
        changes: mockChangeListData,
        dataFilter: defaultChangeManagementDataFilter
      }
    } as any;

    mockSelectorChanges(pre);
    const { result } = renderHook(() => useSelectChangesManagement());

    expect(result.current.totalItem).toEqual(3);
  });
  it('useSelectChangesManagement -> search -> no data', () => {
    const pre = {
      lastUpdatedChanges: {
        changes: mockChangeListData,
        dataFilter: {
          ...defaultChangeManagementDataFilter,
          changeStatus: 'Validate',
          searchValue: 'abcdef'
        }
      }
    } as any;

    mockSelectorChanges(pre);
    const { result } = renderHook(() => useSelectChangesManagement());

    expect(result.current.totalItem).toEqual(0);
  });
  it('useSelectChangesManagement -> search -> changeType !== all', () => {
    const pre = {
      lastUpdatedChanges: {
        changes: mockChangeListData,
        dataFilter: {
          ...defaultChangeManagementDataFilter,
          changeStatus: 'Validate',
          searchValue: 'abcdef',
          changeType: 'Pricing'
        }
      }
    } as any;

    mockSelectorChanges(pre);
    const { result } = renderHook(() => useSelectChangesManagement());

    expect(result.current.totalItem).toEqual(0);
  });
  it('useSelectChangesManagement -> search -> undefined dataFilter ', () => {
    const pre = {
      lastUpdatedChanges: {
        changes: mockChangeListData,
        dataFilter: undefined
      }
    } as any;

    mockSelectorChanges(pre);
    const { result } = renderHook(() => useSelectChangesManagement());

    expect(result.current.totalItem).toEqual(0);
  });

  it('useSelectLoadingOnApproveChange', () => {
    const pre = {
      changeApproveStatus: {
        loading: true
      }
    } as any;

    mockSelectorChanges(pre);
    const { result } = renderHook(() => useSelectLoadingOnApproveChange());

    expect(result.current).toEqual(true);
  });

  it('useSelectLoadingOnApproveChange => loading: undefined', () => {
    const pre = {
      changeApproveStatus: {
        loading: undefined
      }
    } as any;

    mockSelectorChanges(pre);
    const { result } = renderHook(() => useSelectLoadingOnApproveChange());

    expect(result.current).toEqual(false);
  });

  it('useSelectLoadingOnRejectChange', () => {
    const pre = {
      changeRejectStatus: {
        loading: true
      }
    } as any;

    mockSelectorChanges(pre);
    const { result } = renderHook(() => useSelectLoadingOnRejectChange());

    expect(result.current).toEqual(true);
  });
  it('useSelectLoadingOnRejectChange => loading: undefined', () => {
    const pre = {
      changeRejectStatus: {
        loading: undefined
      }
    } as any;

    mockSelectorChanges(pre);
    const { result } = renderHook(() => useSelectLoadingOnRejectChange());

    expect(result.current).toEqual(false);
  });
  it('useSelectLoadingOnDemoteChange', () => {
    const pre = {
      changeDemoteStatus: {
        loading: true
      }
    } as any;

    mockSelectorChanges(pre);
    const { result } = renderHook(() => useSelectLoadingOnDemoteChange());

    expect(result.current).toEqual(true);
  });
  it('useSelectLoadingOnDemoteChange => loading: undefined', () => {
    const pre = {
      changeDemoteStatus: {
        loading: undefined
      }
    } as any;

    mockSelectorChanges(pre);
    const { result } = renderHook(() => useSelectLoadingOnDemoteChange());

    expect(result.current).toEqual(false);
  });
  it('useSelectDataForExportError', () => {
    const pre = {
      changeDataExport: {
        error: true
      }
    } as any;

    mockSelectorChanges(pre);
    const { result } = renderHook(() => useSelectDataForExportError());

    expect(result.current).toEqual(true);
  });
  it('useSelectDataForExportError =>  error: undefined', () => {
    const pre = {
      changeDataExport: {
        error: undefined
      }
    } as any;

    mockSelectorChanges(pre);
    const { result } = renderHook(() => useSelectDataForExportError());

    expect(result.current).toEqual(false);
  });

  it('currentChangeInvalidMessagesSelector', () => {
    const pre = {
      changeDetails: {
        data: {
          validationMessages: [
            {
              changeId: '1000',
              changeName: 'bob',
              actions: ['edit'],
              isChangeFeedback: false
            }
          ]
        },
        loading: false,
        error: false
      }
    } as any;

    mockSelectorChanges(pre);
    const { result } = renderHook(() =>
      useSelectCurrentChangeInvalidMessages()
    );

    expect(result.current[0].isChangeFeedback).toEqual(true);
  });

  it('currentChangeInvalidMessagesSelector > data: undefined', () => {
    const pre = {
      changeDetails: {
        data: undefined,
        loading: false,
        error: false
      }
    } as any;

    mockSelectorChanges(pre);
    const { result } = renderHook(() =>
      useSelectCurrentChangeInvalidMessages()
    );

    expect(result.current[0]).toEqual(undefined);
  });

  it('useSelectExpandingChange', () => {
    const pre = {
      lastUpdatedChanges: {
        expandingChange: 'change'
      }
    } as any;

    mockSelectorChanges(pre);
    const { result } = renderHook(() => useSelectExpandingChange());

    expect(result.current).toEqual('change');
  });

  it('useSelectLoadingOnSubmitChange', () => {
    const pre = {
      changeSubmitStatus: {
        loading: true
      }
    } as any;

    mockSelectorChanges(pre);
    const { result } = renderHook(() => useSelectLoadingOnSubmitChange());

    expect(result.current).toEqual(true);
  });

  it('useSelectLoadingOnSubmitChange => loading undefined', () => {
    const pre = {
      changeSubmitStatus: {
        loading: undefined
      }
    } as any;

    mockSelectorChanges(pre);
    const { result } = renderHook(() => useSelectLoadingOnSubmitChange());

    expect(result.current).toEqual(false);
  });

  it('useSelectOpenWorkflowDetail', () => {
    const pre = {
      workflowDetailId: 'shallow'
    } as any;

    mockSelectorChanges(pre);
    const { result } = renderHook(() => useSelectOpenWorkflowDetail());

    expect(result.current).toEqual('shallow');
  });

  it('useSelectApprovalByGroup', () => {
    const pre = {
      pendingApprovalByGroup: {
        approvalByGroup: {
          id: '1000',
          name: 'bod',
          changeType: 'spicing'
        },
        loading: false,
        error: false
      }
    } as any;

    mockSelectorChanges(pre);
    const { result } = renderHook(() => useSelectApprovalByGroup());
    expect(result.current.error).toEqual(false);
  });

  it('useShowChangeNotFoundModal', () => {
    const pre = {
      showChangeNotFoundModal: true
    } as any;

    mockSelectorChanges(pre);
    const { result } = renderHook(() => useShowChangeNotFoundModal());

    expect(result.current).toEqual(true);
  });
});
