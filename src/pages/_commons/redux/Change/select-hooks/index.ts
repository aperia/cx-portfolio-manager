import { createSelector } from '@reduxjs/toolkit';
import { SPECIAL_TYPE } from 'app/constants/constants';
import { getSortComparator, textToCamel } from 'app/helpers';
import { matchSearchValue } from 'app/helpers/matchSearchValue';
import { isString } from 'lodash';
import _orderBy from 'lodash.orderby';
import { shallowEqual, useSelector } from 'react-redux';
import {
  ChangeDataExport,
  ChangeDateInfoState,
  ChangeDetailState,
  ChangeOwnerState,
  IChangeManagementFilter,
  IPendingApprovalsFilter,
  LastUpdatedApprovalQueuesState,
  LastUpdatedChangesState,
  PendingApprovalByGroup
} from '../types';

export const useSelectLastUpdatedChanges = () => {
  return useSelector<RootState, LastUpdatedChangesState>(
    state => state.change!.lastUpdatedChanges
  );
};

const approvalQueuesSelector = createSelector(
  (state: RootState) => state.change!.approvalQueues.data,
  (state: RootState) => state.change!.approvalQueues.dataFilter,
  (state: RootState) => state.change!.approvalQueues.changeTypeList,
  (rawData, dataFilter, changeTypeList) => {
    const { searchValue, page, pageSize, sortBy, orderBy, changeType } =
      dataFilter || {};

    const search = searchValue?.toLowerCase() || '';
    const typeAndStatusSearch =
      searchValue?.replace(/[ \-()]/g, '').toLowerCase() || '';

    const changeTypeFilter =
      textToCamel(SPECIAL_TYPE[`${changeType}`] || changeType) || '';

    let approvalQueues = rawData
      .map(i => ({ ...i }))
      .filter(
        i => changeTypeFilter === 'all' || i.changeType === changeTypeFilter
      )
      .filter(
        i =>
          i.changeName?.toLowerCase().includes(search) ||
          i.changeOwner?.toLowerCase().includes(search) ||
          i.changeStatus?.toLowerCase().includes(typeAndStatusSearch) ||
          i.changeType?.toLowerCase().includes(typeAndStatusSearch)
      );

    approvalQueues = approvalQueues.sort(getSortComparator(sortBy, orderBy));

    const total = approvalQueues.length;
    approvalQueues = approvalQueues.slice(
      (page! - 1) * pageSize!,
      page! * pageSize!
    );

    return { total, approvalQueues, changeTypeList };
  }
);
export const useSelectApprovalQueues = () => {
  return useSelector<
    RootState,
    {
      approvalQueues: IChangeDetail[];
      total: number;
      changeTypeList: string[];
    }
  >(approvalQueuesSelector);
};

const selectExpandingApproval = createSelector(
  (state: RootState) => state.change?.approvalQueues.expandingApproval,
  data => data!
);

export const useSelectExpandingApproval = () => {
  return useSelector<RootState, string>(selectExpandingApproval);
};

const approvalQueuesStatusSelector = createSelector(
  (state: RootState) => state.change!.approvalQueues.loading,
  (state: RootState) => state.change!.approvalQueues.error!,
  (loading, error) => ({ loading, error })
);
export const useSelectApprovalQueuesStatus = () => {
  return useSelector<
    RootState,
    {
      loading: boolean;
      error: boolean;
    }
  >(approvalQueuesStatusSelector);
};
const approvalQueuesFilterSelector = createSelector(
  (state: RootState) => state.change!.approvalQueues.dataFilter!,
  data => data
);
export const useSelectApprovalQueuesFilter = () => {
  return useSelector<RootState, IPendingApprovalsFilter>(
    approvalQueuesFilterSelector
  );
};

export const useSelectLastUpdatedApprovalQueues = () => {
  return useSelector<RootState, LastUpdatedApprovalQueuesState>(
    state => state.change!.lastUpdatedApprovalQueues
  );
};

export const useSelectCurrentChangeDetail = () => {
  return useSelector<RootState, ChangeDetailState>(
    state => state.change!.changeDetails
  );
};

const currentChangeInvalidMessagesSelector = createSelector(
  (state: RootState) => state.change!.changeDetails.data,
  change =>
    change?.validationMessages?.map(m => ({
      ...m,
      changeId: change.changeId,
      source: change.changeName,

      isChangeFeedback: true,
      actions: change.actions || []
    })) || []
);
export const useSelectCurrentChangeInvalidMessages = () => {
  return useSelector<RootState, IValidationMessage[]>(
    currentChangeInvalidMessagesSelector
  );
};

export const useSelectChangeOwners = () => {
  return useSelector<RootState, ChangeOwnerState>(
    state => state.change!.changeOwners
  );
};

export const useSelectChangeDateInfo = () => {
  return useSelector<RootState, ChangeDateInfoState>(
    state => state.change!.changeDateInfo
  );
};

export const useSelectLoadingOnUpdateChange = () => {
  return useSelector<RootState, boolean>(
    state => state.change!.changeUpdateStatus?.loading
  );
};

export const useSelectWorkflowsForExport = () => {
  const workflowSelector = createSelector(
    (state: RootState) => state.workflow!.changeWorkflows,
    data => ({ workflowList: data.workflows })
  );
  return useSelector<RootState, Partial<ChangeDataExport>>(workflowSelector);
};

export const useSelectDataForExport = () => {
  const changeDataExportSelector = createSelector(
    (state: RootState) => state.change!.changeDetails.data,
    (state: RootState) => state.workflow!.changeWorkflows.workflows,
    (state: RootState) => state.portfolio!.portfoliosData.portfolios,
    (state: RootState) =>
      state.pricingStrategies!.pricingStrategyData.pricingStrategies,
    (state: RootState) =>
      state.pricingMethods!.changePricingMethods.pricingMethods,
    (state: RootState) => state.activity!.activitiesInChange.activities,
    (state: RootState) => state.change!.changeAttachmentList.data,
    (
      snapshot,
      workflowList,
      portfolioList,
      pricingStrategyList,
      pricingMethodList,
      activityList,
      attachmentList
    ) => ({
      snapshot,
      activityList,
      portfolioList,
      workflowList,
      pricingStrategyList,
      pricingMethodList,
      attachmentList
    })
  );
  return useSelector<RootState, ChangeDataExport>(changeDataExportSelector);
};

export const useSelectDataForExportLoading = () => {
  return useSelector<RootState, boolean>(
    state => state.change!.changeDataExport.loading,
    shallowEqual
  );
};

const managementFilterSelector = createSelector(
  (state: RootState) => state.change!.lastUpdatedChanges.dataFilter!,
  data => data
);
export const useSelectChangeManagementFilter = () => {
  return useSelector<RootState, IChangeManagementFilter>(
    managementFilterSelector
  );
};

const changesManagementSelector = createSelector(
  (state: RootState) => state.change!.lastUpdatedChanges,
  data => {
    const {
      changes: rawChanges,
      loading,
      error = false,
      dataFilter,
      changeTypeList,
      changeStatusList
    } = data;

    const {
      searchValue,
      page,
      pageSize,
      sortBy,
      orderBy,
      filterField,
      filterValue: dateRange,
      changeStatus,
      changeType
    } = dataFilter || {};

    const changeStatusCamel = textToCamel(changeStatus)?.toLowerCase();
    const changeTypeCamel = textToCamel(changeType)?.toLowerCase();

    let changes = _orderBy(
      rawChanges.map(i => ({ ...i })),
      sortBy?.map(
        id => (value: any) =>
          isString(value[id]) ? value[id]?.toLowerCase()?.trim() : value[id]
      ),
      orderBy
    )
      .filter(
        i =>
          (changeStatusCamel === 'all' ||
            i.changeStatus?.toLowerCase() === changeStatusCamel) &&
          (changeTypeCamel === 'all' ||
            i.changeType?.toLowerCase() === changeTypeCamel)
      )
      .filter(i =>
        matchSearchValue(
          [i.changeName, i.changeId, i.changeType, i.changeOwner],
          searchValue
        )
      )
      .filter((i: any) => {
        const { start, end } = dateRange;
        if (!start || !end) return true;
        const checkDate = filterField && new Date(i[filterField]);
        if (!checkDate) return false;
        checkDate.setHours(0, 0, 0, 0);
        return checkDate >= dateRange.start && checkDate <= dateRange.end;
      });

    const totalItem = changes.length;
    changes = changes.slice((page! - 1) * pageSize!, page! * pageSize!);

    return {
      loading,
      totalItem,
      error,
      changes,
      changeTypeList,
      changeStatusList
    };
  }
);
export const useSelectChangesManagement = () => {
  return useSelector<
    RootState,
    {
      changes: IChangeDetail[];
      loading: boolean;
      error: boolean;
      totalItem: number;
      changeTypeList: string[];
      changeStatusList: string[];
    }
  >(changesManagementSelector);
};

const expandingChangeSelector = createSelector(
  (state: RootState) => state.change?.lastUpdatedChanges.expandingChange,
  data => data!
);

export const useSelectExpandingChange = () => {
  return useSelector<RootState, string>(expandingChangeSelector);
};

export const useSelectLoadingOnApproveChange = () => {
  return useSelector<RootState, boolean>(
    state => state.change!.changeApproveStatus?.loading || false
  );
};

export const useSelectLoadingOnSubmitChange = () => {
  return useSelector<RootState, boolean>(
    state => state.change!.changeSubmitStatus?.loading || false
  );
};

export const useSelectLoadingOnRejectChange = () => {
  return useSelector<RootState, boolean>(
    state => state.change!.changeRejectStatus?.loading || false
  );
};

export const useSelectLoadingOnDemoteChange = () => {
  return useSelector<RootState, boolean>(
    state => state.change!.changeDemoteStatus?.loading || false
  );
};

export const useSelectDataForExportError = () => {
  return useSelector<RootState, boolean>(
    state => state.change!.changeDataExport.error || false,
    shallowEqual
  );
};

export const useSelectOpenWorkflowDetail = () => {
  return useSelector<RootState, string>(
    state => state.change!.workflowDetailId,
    shallowEqual
  );
};

export const useSelectApprovalByGroup = () => {
  return useSelector<RootState, PendingApprovalByGroup>(
    createSelector(
      (state: RootState) => state.change!.pendingApprovalByGroup,
      data => data
    )
  );
};

const selectShowChangeNotFoundModal = createSelector(
  (state: RootState) => state.change!.showChangeNotFoundModal,
  data => data || false
);

export const useShowChangeNotFoundModal = () =>
  useSelector(selectShowChangeNotFoundModal);
