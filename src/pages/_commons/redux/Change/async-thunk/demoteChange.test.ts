import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsChange,
  ChangeState,
  reducer
} from 'pages/_commons/redux/Change';

const initReducer = {
  changeDemoteStatus: { loading: false, error: true }
} as ChangeState;

describe('change > redux-store > demoteChange', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.demoteChange.pending('demoteChange', {})
    );
    expect(nextState.changeDemoteStatus.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.demoteChange.fulfilled([], 'demoteChange', {})
    );
    expect(nextState.changeDemoteStatus.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.demoteChange.rejected(null, 'demoteChange', {})
    );
    expect(nextState.changeDemoteStatus.error).toEqual(true);
  });
  it('thunk action ', async () => {
    const changeId = '123';
    const nextState = await actionsChange.demoteChange({
      changeId,
      reason: 'Demote change'
    })(
      store.dispatch,
      () =>
        ({
          ...store.getState()
        } as RootState),
      {
        changeService: {
          demoteChange: jest.fn().mockResolvedValue({ data: { messages: [] } })
        }
      }
    );
    expect(nextState.payload).toEqual(true);
  });
});
