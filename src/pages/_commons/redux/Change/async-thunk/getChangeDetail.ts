import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { ChangeType } from 'app/constants/enums';
import {
  CHANGE_ACTION_MAPPING,
  CHANGE_STATUS_MAPPING,
  CHANGE_TYPE_MAPPING
} from 'app/constants/mapping';
import { mappingDataFromObj } from 'app/helpers';
import get from 'lodash.get';
import orderBy from 'lodash.orderby';
import set from 'lodash.set';
import { ChangeState } from '../types';

export const getChangeDetail = createAsyncThunk<any, string, ThunkAPIConfig>(
  'mapping/getChangeDetail',
  async (changeId: string, thunkAPI) => {
    const { changeService } = get(thunkAPI, 'extra');
    const resp = await changeService.getChangeDetail(changeId);

    const state = thunkAPI.getState();
    // Take mapping model
    const changeDetailSnapshotMapping =
      state.mapping?.data?.changeDetailSnapshot;

    // Mapping
    const mappedData = mappingDataFromObj(
      resp?.data?.changeSetDetail,
      changeDetailSnapshotMapping!
    );

    mappedData.changeStatus = CHANGE_STATUS_MAPPING[mappedData.changeStatus!];
    mappedData.changeType = CHANGE_TYPE_MAPPING[mappedData.changeType];
    mappedData.actions = mappedData.actions?.map(
      (a: string) => CHANGE_ACTION_MAPPING[a]
    );
    if (
      mappedData.approvalRecords?.filter(
        (a: any) => a.action?.toLowerCase() === 'approve'
      ).length > 0
    ) {
      const record = orderBy(
        mappedData.approvalRecords.filter(
          (a: any) => a.action?.toLowerCase() === 'approve'
        ),
        ['actionTime'],
        ['desc']
      )[0];
      mappedData.lastApprovalBy = record.user?.name;
      mappedData.approvalDate = record.actionTime?.toString();
    } else {
      mappedData.lastApprovalBy = undefined;
      mappedData.approvalDate = undefined;
    }
    if (mappedData.changeType !== ChangeType.Pricing) {
      delete mappedData.pricingStrategiesCreatedCount;
      delete mappedData.pricingStrategiesUpdatedCount;
      delete mappedData.pricingStrategiesImpacted;
      delete mappedData.pricingMethodsCreatedCount;
      delete mappedData.pricingMethodsUpdatedCount;
      delete mappedData.noOfDMMTablesCreated;
      delete mappedData.noOfDMMTablesUpdated;
    } else {
      delete mappedData.cardholdersAccountsImpacted;
      delete mappedData.portfoliosImpacted;
    }

    return { data: mappedData };
  }
);
export const getChangeDetailBuilder = (
  builder: ActionReducerMapBuilder<ChangeState>
) => {
  builder.addCase(getChangeDetail.pending, (draftState, action) => {
    set(draftState.changeDetails, 'loading', true);
    set(draftState.changeDetails, 'error', false);
  });
  builder.addCase(getChangeDetail.fulfilled, (draftState, action) => {
    set(draftState.changeDetails, 'loading', false);
    set(draftState.changeDetails, 'data', action.payload.data);
  });
  builder.addCase(getChangeDetail.rejected, (draftState, action) => {
    set(draftState.changeDetails, 'loading', false);
    set(draftState.changeDetails, 'data', {});
    set(draftState.changeDetails, 'error', true);
  });
};
