import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import get from 'lodash.get';
import set from 'lodash.set';
import { ChangeState } from '../types';

interface IChangeFields {
  changeId: string;
  changeName: string;
  changeOwnerId: string;
  effectiveDate: string;
  description: string;
}
export const updateChangeDetail = createAsyncThunk<
  any,
  IChangeFields,
  ThunkAPIConfig
>('mapping/updateChangeDetail', async ({ changeId, ...change }, thunkAPI) => {
  const { changeService } = get(thunkAPI, 'extra');
  await changeService.updateChangeDetail({
    changeSetId: changeId,
    ...change
  });

  return true;
});
export const updateChangeDetailBuilder = (
  builder: ActionReducerMapBuilder<ChangeState>
) => {
  builder.addCase(updateChangeDetail.pending, (draftState, action) => {
    set(draftState.changeUpdateStatus, 'loading', true);
  });
  builder.addCase(updateChangeDetail.fulfilled, (draftState, action) => {
    set(draftState.changeUpdateStatus, 'loading', false);
  });
  builder.addCase(updateChangeDetail.rejected, (draftState, action) => {
    set(draftState.changeUpdateStatus, 'loading', false);
    set(draftState.changeUpdateStatus, 'error', true);
  });
};
