import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { CHANGE_OWNER_LIST_FIELDS } from 'app/constants/api-links';
import { mappingDataFromArray } from 'app/helpers';
import get from 'lodash.get';
import set from 'lodash.set';
import { ChangeState } from '../types';

export const getChangeOwners = createAsyncThunk<any, string, ThunkAPIConfig>(
  'mapping/getChangeOwners',
  async (changeId: string, thunkAPI) => {
    const { changeService } = get(thunkAPI, 'extra');
    const resp = await changeService.getChangeOwners({
      selectFields: CHANGE_OWNER_LIST_FIELDS,
      searchFields: { changeSetId: changeId }
    });

    const state = thunkAPI.getState();
    // Take mapping model
    const changeDetailSnapshotMapping = state.mapping?.data?.changeOwner;

    // Mapping
    const mappedData = mappingDataFromArray(
      resp?.data?.users,
      changeDetailSnapshotMapping!
    );

    return { data: mappedData };
  }
);
export const getChangeOwnersBuilder = (
  builder: ActionReducerMapBuilder<ChangeState>
) => {
  builder.addCase(getChangeOwners.pending, (draftState, action) => {
    set(draftState.changeOwners, 'loading', true);
  });
  builder.addCase(getChangeOwners.fulfilled, (draftState, action) => {
    return {
      ...draftState,
      changeOwners: { ...action.payload, loading: false, error: false }
    };
  });
  builder.addCase(getChangeOwners.rejected, (draftState, action) => {
    set(draftState.changeOwners, 'loading', false);
    set(draftState.changeOwners, 'error', true);
  });
};
