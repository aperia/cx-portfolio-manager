import { ExportData } from 'app/fixtures/change-data';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsChange,
  ChangeState,
  reducer
} from 'pages/_commons/redux/Change';

jest.mock('app/helpers/fileUtilities.ts', () => ({
  ...jest.requireActual('app/helpers/fileUtilities.ts'),
  downloadBlobFile: jest.fn()
}));

const initReducer = {
  changeDataExport: { loading: false, error: true, exporting: false }
} as ChangeState;

describe('change > redux-store > exportChangeSet', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.exportChangeSet.pending('exportChangeSet', '1')
    );

    expect(nextState.changeDataExport.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.exportChangeSet.fulfilled([], 'exportChangeSet', '1')
    );
    expect(nextState.changeDataExport.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.exportChangeSet.rejected(null, 'exportChangeSet', '1')
    );
    expect(nextState.changeDataExport.error).toEqual(true);
  });
  it('thunk action -> pricing', async () => {
    const nextState = await actionsChange.exportChangeSet(
      ExportData.changeDetailPricing
    )(
      () => {
        return {
          dispatch: jest.fn().mockResolvedValue({
            payload: {},
            type: ''
          })
        } as any;
      },
      () =>
        ({
          ...store.getState()
        } as RootState),
      {
        changeService: {
          exportChangeSet: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );
    expect(nextState.payload).toEqual(true);
  });
  it('thunk action -> non pricing', async () => {
    const nextState = await actionsChange.exportChangeSet(
      ExportData.changeDetailNonPricing
    )(
      () => {
        return {
          dispatch: jest.fn().mockResolvedValue({
            payload: {},
            type: ''
          })
        } as any;
      },
      () =>
        ({
          ...store.getState()
        } as RootState),
      {
        changeService: {
          exportChangeSet: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );
    expect(nextState.payload).toEqual(true);
  });
});
