import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { MIME_TYPE } from 'app/constants/mine-type';
import { mappingDataFromObj } from 'app/helpers';
import { base64toBlob, downloadBlobFile } from 'app/helpers/fileUtilities';
import { get } from 'lodash';
import set from 'lodash.set';
import { ChangeState } from '../types';

export const exportChangeSet = createAsyncThunk<any, any, ThunkAPIConfig>(
  'mapping/exportChangeSet',
  async (snapshot: IChangeSnapshot, thunkAPI) => {
    const state = thunkAPI.getState();
    const { changeService } = get(thunkAPI, 'extra');

    const downloadFileMapping = state.mapping?.data?.downloadTemplateFile;
    const response = await changeService.exportChangeSet(snapshot.changeId);

    const attachmentData = response?.data?.attachment;
    const file = mappingDataFromObj(attachmentData, downloadFileMapping!);
    const {
      fileStream = '',
      mimeType: mimeTypeDownload = '',
      filename: filenameDownload = ''
    } = file;

    let fileName = filenameDownload;
    if (filenameDownload.split('.').length < 2) {
      const ext = MIME_TYPE[mimeTypeDownload]?.extensions[0] || '';
      fileName = `${filenameDownload}.${ext}`;
    }
    const blol = base64toBlob(fileStream, mimeTypeDownload);
    downloadBlobFile(blol, fileName);

    return true;
  }
);
export const exportChangeSetBuilder = (
  builder: ActionReducerMapBuilder<ChangeState>
) => {
  builder.addCase(exportChangeSet.pending, (draftState, action) => {
    set(draftState.changeDataExport, 'loading', true);
  });
  builder.addCase(exportChangeSet.fulfilled, (draftState, action) => {
    set(draftState.changeDataExport, 'loading', false);
    set(draftState.changeDataExport, 'error', false);
  });
  builder.addCase(exportChangeSet.rejected, (draftState, action) => {
    set(draftState.changeDataExport, 'loading', false);
    set(draftState.changeDataExport, 'error', true);
  });
};
