import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import get from 'lodash.get';
import set from 'lodash.set';
import { ChangeState } from '../types';

interface IRejectChangeFields {
  changeId: string;
  reason: string;
}
export const rejectChange = createAsyncThunk<
  any,
  IRejectChangeFields,
  ThunkAPIConfig
>('mapping/rejectChange', async ({ changeId, reason }, thunkAPI) => {
  const { changeService } = get(thunkAPI, 'extra');
  await changeService.rejectChange(changeId, reason);

  return true;
});

export const rejectChangeBuilder = (
  builder: ActionReducerMapBuilder<ChangeState>
) => {
  builder.addCase(rejectChange.pending, (draftState, action) => {
    set(draftState.changeRejectStatus, 'loading', true);
  });
  builder.addCase(rejectChange.fulfilled, (draftState, action) => {
    set(draftState.changeRejectStatus, 'loading', false);
  });
  builder.addCase(rejectChange.rejected, (draftState, action) => {
    set(draftState.changeRejectStatus, 'loading', false);
    set(draftState.changeRejectStatus, 'error', true);
  });
};
