import {
  pendingApproval,
  pendingApprovalMapping
} from 'app/fixtures/change-data';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import { actionsChange, reducer } from 'pages/_commons/redux/Change';

const initReducer: any = {
  pendingApprovalByGroup: {
    approvalByGroup: {
      id: '',
      name: '',
      changeType: '',
      nextApproverGroup: {},
      defaultgroup: true
    },
    loading: false,
    error: true
  }
};

describe('change > redux-store > getPendingApprovalGroup', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.getPendingApprovalGroup.pending(
        'getPendingApprovalGroup',
        ''
      )
    );

    expect(nextState.pendingApprovalByGroup.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.getPendingApprovalGroup.fulfilled(
        [],
        'getPendingApprovalGroup',
        ''
      )
    );
    expect(nextState.pendingApprovalByGroup.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.getPendingApprovalGroup.rejected(
        null,
        'getPendingApprovalGroup',
        ''
      )
    );
    expect(nextState.pendingApprovalByGroup.error).toEqual(true);
  });
  it('thunk action ', async () => {
    const changeId = '1000';
    const nextState = await actionsChange.getPendingApprovalGroup(changeId)(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: {
            data: {
              pendingApprovals: pendingApprovalMapping
            }
          }
        } as RootState),
      {
        changeService: {
          pendingApprovalGroupList: jest
            .fn()
            .mockResolvedValue({ data: { pendingApprovalBy: pendingApproval } })
        }
      }
    );
    expect(nextState.payload.approvalByGroup.id).toEqual(1150226841);
  });

  it('thunk action => data empty ', async () => {
    const changeId = '1000';
    const nextState = await actionsChange.getPendingApprovalGroup(changeId)(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: {
            data: {
              pendingApprovals: pendingApprovalMapping
            }
          }
        } as RootState),
      {
        changeService: {
          pendingApprovalGroupList: jest
            .fn()
            .mockResolvedValue({ data: { pendingApprovalBy: '' } })
        }
      }
    );
    expect(nextState.payload.approvalByGroup).toEqual(null);
  });
});
