import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsChange,
  ChangeState,
  reducer
} from 'pages/_commons/redux/Change';

const initReducer = {
  changeOwners: { loading: false, error: true, data: [] as RefData[] }
} as ChangeState;

describe('change > redux-store > getChangeOwners', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.getChangeOwners.pending('getChangeOwners', '1')
    );

    expect(nextState.changeOwners.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.getChangeOwners.fulfilled([], 'getChangeOwners', '1')
    );
    expect(nextState.changeOwners.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.getChangeOwners.rejected(null, 'getChangeOwners', '1')
    );
    expect(nextState.changeOwners.error).toEqual(true);
  });
  it('thunk action ', async () => {
    const changeId = '123';
    const nextState = await actionsChange.getChangeOwners(changeId)(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: {
            data: {
              changeOwner: {
                code: 'id',
                text: 'name'
              }
            }
          }
        } as RootState),
      {
        changeService: {
          getChangeOwners: jest.fn().mockResolvedValue({
            data: {
              users: [
                {
                  id: '48533597',
                  name: 'Laura Bailey'
                },
                {
                  id: '28321431',
                  name: 'Laura Bailey'
                },
                {
                  id: '26858799',
                  name: 'Jesse Dunn'
                }
              ]
            }
          })
        }
      }
    );
    expect(nextState.payload.data.length).toEqual(3);
  });
});
