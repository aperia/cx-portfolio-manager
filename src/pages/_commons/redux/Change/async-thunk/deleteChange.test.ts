import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsChange,
  ChangeState,
  reducer
} from 'pages/_commons/redux/Change';

const initReducer = {
  changeDeleteStatus: { loading: false, error: true }
} as ChangeState;

describe('change > redux-store > deleteChange', () => {
  const mockChangeField = {
    changeId: '123',
    changeName: 'John cena',
    changeOwnerId: '1',
    effectiveDate: '2021-03-03',
    description: 'Some description'
  };

  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.deleteChange.pending('deleteChange', mockChangeField)
    );
    expect(nextState.changeDeleteStatus.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.deleteChange.fulfilled([], 'deleteChange', mockChangeField)
    );
    expect(nextState.changeDeleteStatus.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.deleteChange.rejected(null, 'deleteChange', mockChangeField)
    );
    expect(nextState.changeDeleteStatus.error).toEqual(true);
  });
  it('thunk action => success', async () => {
    const nextState = await actionsChange.deleteChange(mockChangeField)(
      store.dispatch,
      () =>
        ({
          ...store.getState()
        } as RootState),
      {
        changeService: {
          deleteChange: jest.fn().mockResolvedValue({
            data: {}
          })
        }
      }
    );
    expect(nextState.payload).toEqual(true);
  });
});
