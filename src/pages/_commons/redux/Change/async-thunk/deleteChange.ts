import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import get from 'lodash.get';
import set from 'lodash.set';
import { ChangeState } from '../types';

interface IChangeFields {
  changeId: string;
}

export const deleteChange = createAsyncThunk<
  any,
  IChangeFields,
  ThunkAPIConfig
>('mapping/deleteChange', async ({ changeId }, thunkAPI) => {
  const { changeService } = get(thunkAPI, 'extra') as APIMapping;

  await changeService.deleteChange(changeId);

  return true;
});

export const deleteChangeBuilder = (
  builder: ActionReducerMapBuilder<ChangeState>
) => {
  builder.addCase(deleteChange.pending, (draftState, action) => {
    set(draftState.changeDeleteStatus, 'loading', true);
  });
  builder.addCase(deleteChange.fulfilled, (draftState, action) => {
    set(draftState.changeDeleteStatus, 'loading', false);
  });
  builder.addCase(deleteChange.rejected, (draftState, action) => {
    set(draftState.changeDeleteStatus, 'loading', false);
    set(draftState.changeDeleteStatus, 'error', true);
  });
};
