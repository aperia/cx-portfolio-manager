import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import get from 'lodash.get';
import set from 'lodash.set';
import { ChangeState } from '../types';

export const demoteChange = createAsyncThunk<any, any, ThunkAPIConfig>(
  'mapping/demoteChange',
  async ({ changeId, reason }, thunkAPI) => {
    const { changeService } = get(thunkAPI, 'extra');
    await changeService.demoteChange(changeId, reason);

    return true;
  }
);

export const demoteChangeBuilder = (
  builder: ActionReducerMapBuilder<ChangeState>
) => {
  builder.addCase(demoteChange.pending, (draftState, action) => {
    set(draftState.changeDemoteStatus, 'loading', true);
    set(draftState.changeDemoteStatus, 'error', false);
  });
  builder.addCase(demoteChange.fulfilled, (draftState, action) => {
    set(draftState.changeDemoteStatus, 'loading', false);
  });
  builder.addCase(demoteChange.rejected, (draftState, action) => {
    set(draftState.changeDemoteStatus, 'loading', false);
    set(draftState.changeDemoteStatus, 'error', true);
  });
};
