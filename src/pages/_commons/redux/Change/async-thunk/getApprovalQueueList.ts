import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { ChangeType } from 'app/constants/enums';
import {
  CHANGE_STATUS_MAPPING,
  CHANGE_TYPE_MAPPING
} from 'app/constants/mapping';
import { formatTime, mappingDataAndDynamicFieldsFromArray } from 'app/helpers';
import get from 'lodash.get';
import orderBy from 'lodash.orderby';
import set from 'lodash.set';
import { ChangeState } from '../types';

export const getApprovalQueueList = createAsyncThunk<
  any,
  undefined,
  ThunkAPIConfig
>('mapping/getApprovalQueueList', async (args: undefined, thunkAPI) => {
  const { changeService } = get(thunkAPI, 'extra');
  const resp = await changeService.getApprovalQueueList();

  const state = thunkAPI.getState();
  // Take mapping model
  const changeManagementListMapping = state.mapping?.data?.changeManagementList;

  // Mapping
  const mappedData = mappingDataAndDynamicFieldsFromArray(
    resp?.data?.changeSetList,
    changeManagementListMapping!,
    ['changeType']
  ) as any;

  const { data, dynamicFields } = mappedData;
  if (!data) return { rawData: [], dynamicFields: {} };

  const changeData = data.map((p: IChangeDetail) => {
    p.changeStatus = CHANGE_STATUS_MAPPING[p.changeStatus!];
    p.changeType = CHANGE_TYPE_MAPPING[p.changeType];
    if (
      p.approvalRecords?.filter(a => a.action?.toLowerCase() === 'approve')
        .length > 0
    ) {
      const record = orderBy(
        p.approvalRecords.filter(a => a.action?.toLowerCase() === 'approve'),
        ['actionTime'],
        ['desc']
      )[0];
      p.lastApprovalBy = record.user?.name;
      p.approvalDate = record.actionTime?.toString();
    } else {
      p.lastApprovalBy = undefined;
      p.approvalDate = undefined;
    }
    if (p.changeType !== ChangeType.Pricing) {
      delete p.pricingStrategiesCreatedCount;
      delete p.pricingStrategiesUpdatedCount;
      delete p.pricingStrategiesImpacted;
      delete p.pricingMethodsCreatedCount;
      delete p.pricingMethodsUpdatedCount;
      delete p.noOfDMMTablesCreated;
      delete p.noOfDMMTablesUpdated;
    } else {
      delete p.cardholdersAccountsImpacted;
      delete p.portfoliosImpacted;
    }
    return p;
  });

  const rawData = changeData?.map((r: any) => ({
    ...r,
    effectiveDateFormat: formatTime(r.effectiveDate).date
  }));

  return { rawData, dynamicFields };
});
export const getApprovalQueueListBuilder = (
  builder: ActionReducerMapBuilder<ChangeState>
) => {
  builder.addCase(getApprovalQueueList.pending, (draftState, action) => {
    set(draftState.approvalQueues, 'loading', true);
    set(draftState.approvalQueues, 'error', false);
  });
  builder.addCase(getApprovalQueueList.fulfilled, (draftState, action) => {
    const { rawData, dynamicFields } = action.payload;
    return {
      ...draftState,
      approvalQueues: {
        ...draftState.approvalQueues,
        changeTypeList: get(dynamicFields, 'changeType', []),
        data: rawData,
        loading: false,
        error: false
      }
    };
  });
  builder.addCase(getApprovalQueueList.rejected, (draftState, action) => {
    set(draftState.approvalQueues, 'loading', false);
    set(draftState.approvalQueues, 'error', true);
  });
};
