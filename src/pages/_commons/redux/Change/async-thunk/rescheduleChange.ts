import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import get from 'lodash.get';
import set from 'lodash.set';
import { ChangeState } from '../types';

export const rescheduleChange = createAsyncThunk<
  boolean,
  { changeId: string; effectiveDate: string },
  ThunkAPIConfig
>('mapping/rescheduleChange', async ({ changeId, effectiveDate }, thunkAPI) => {
  const { changeService } = get(thunkAPI, 'extra');
  await changeService.rescheduleChange(changeId, effectiveDate);

  return true;
});
export const rescheduleChangeBuilder = (
  builder: ActionReducerMapBuilder<ChangeState>
) => {
  builder.addCase(rescheduleChange.pending, draftState => {
    set(draftState.changeUpdateStatus, 'loading', true);
  });
  builder.addCase(rescheduleChange.fulfilled, (draftState, action) => {
    set(draftState.changeUpdateStatus, 'loading', false);
  });
  builder.addCase(rescheduleChange.rejected, (draftState, action) => {
    set(draftState.changeUpdateStatus, 'loading', false);
    set(draftState.changeUpdateStatus, 'error', true);
  });
};
