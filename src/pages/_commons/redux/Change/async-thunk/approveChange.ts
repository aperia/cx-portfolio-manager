import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import get from 'lodash.get';
import set from 'lodash.set';
import { ChangeState } from '../types';

interface IChangeFields {
  changeId: string;
}
export const approveChange = createAsyncThunk<
  any,
  IChangeFields,
  ThunkAPIConfig
>('mapping/approveChange', async ({ changeId }, thunkAPI) => {
  const { changeService } = get(thunkAPI, 'extra');
  await changeService.approveChange(changeId);

  return true;
});

export const approveChangeBuilder = (
  builder: ActionReducerMapBuilder<ChangeState>
) => {
  builder.addCase(approveChange.pending, (draftState, action) => {
    set(draftState.changeApproveStatus, 'loading', true);
  });
  builder.addCase(approveChange.fulfilled, (draftState, action) => {
    set(draftState.changeApproveStatus, 'loading', false);
  });
  builder.addCase(approveChange.rejected, (draftState, action) => {
    set(draftState.changeApproveStatus, 'loading', false);
    set(draftState.changeApproveStatus, 'error', true);
  });
};
