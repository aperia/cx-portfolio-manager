import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { ChangeType } from 'app/constants/enums';
import {
  CHANGE_STATUS_MAPPING,
  CHANGE_TYPE_MAPPING
} from 'app/constants/mapping';
import { formatTime, mappingDataAndDynamicFieldsFromArray } from 'app/helpers';
import get from 'lodash.get';
import orderBy from 'lodash.orderby';
import set from 'lodash.set';
import { ChangeState } from '../types';

export const getLastUpdatedChanges = createAsyncThunk<
  any,
  undefined,
  ThunkAPIConfig
>('mapping/getLastUpdatedChanges', async (args: undefined, thunkAPI) => {
  const { changeService } = get(thunkAPI, 'extra');
  const resp = await changeService.getLastUpdatedChanges();

  const state = thunkAPI.getState();
  // Take mapping model
  const changeManagementListMapping = state.mapping?.data?.changeManagementList;

  const mappedData = mappingDataAndDynamicFieldsFromArray(
    resp?.data?.changeSetList,
    changeManagementListMapping!,
    ['changeType', 'changeStatus']
  ) as any;
  const { data, dynamicFields } = mappedData;
  if (!data) return { changes: [], dynamicFields: {} };
  // Mapping
  const changeData = data.map((p: IChangeDetail) => {
    p.changeStatus = CHANGE_STATUS_MAPPING[p.changeStatus!];
    p.changeType = CHANGE_TYPE_MAPPING[p.changeType];
    if (
      p.approvalRecords?.filter(a => a.action?.toLowerCase() === 'approve')
        .length > 0
    ) {
      const record = orderBy(
        p.approvalRecords.filter(a => a.action?.toLowerCase() === 'approve'),
        ['actionTime'],
        ['desc']
      )[0];
      p.lastApprovalBy = record.user?.name;
      p.approvalDate = record.actionTime?.toString();
    } else {
      p.lastApprovalBy = undefined;
      p.approvalDate = undefined;
    }
    if (p.changeType !== ChangeType.Pricing) {
      delete p.pricingStrategiesCreatedCount;
      delete p.pricingStrategiesUpdatedCount;
      delete p.pricingStrategiesImpacted;
      delete p.pricingMethodsCreatedCount;
      delete p.pricingMethodsUpdatedCount;
      delete p.noOfDMMTablesCreated;
      delete p.noOfDMMTablesUpdated;
    } else {
      delete p.cardholdersAccountsImpacted;
      delete p.portfoliosImpacted;
    }
    return p;
  });

  const rawData = changeData?.map((r: any) => ({
    ...r,
    effectiveDateFormat: formatTime(r.effectiveDate).date
  }));

  const changes = orderBy(
    rawData,
    ['effectiveDateFormat', 'changeName'],
    ['desc', 'asc']
  );

  return { changes, dynamicFields };
});
export const getLastUpdatedChangesBuilder = (
  builder: ActionReducerMapBuilder<ChangeState>
) => {
  builder.addCase(getLastUpdatedChanges.pending, (draftState, action) => {
    set(draftState.lastUpdatedChanges, 'loading', true);
    set(draftState.lastUpdatedChanges, 'error', false);
  });
  builder.addCase(getLastUpdatedChanges.fulfilled, (draftState, action) => {
    const { changes, dynamicFields } = action.payload;
    return {
      ...draftState,
      lastUpdatedChanges: {
        ...draftState.lastUpdatedChanges,
        changes: changes,
        changeTypeList: get(dynamicFields, 'changeType', []),
        changeStatusList: get(dynamicFields, 'changeStatus', []),
        error: false,
        loading: false
      }
    };
  });
  builder.addCase(getLastUpdatedChanges.rejected, (draftState, action) => {
    set(draftState.lastUpdatedChanges, 'loading', false);
    set(draftState.lastUpdatedChanges, 'error', true);
  });
};
