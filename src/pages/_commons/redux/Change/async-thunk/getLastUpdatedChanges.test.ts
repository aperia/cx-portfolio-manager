import { ChangeList, ChangeMapping } from 'app/fixtures/change-data';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import { actionsChange, reducer } from 'pages/_commons/redux/Change';

const initReducer: any = {
  lastUpdatedChanges: { loading: false, error: true, changes: [] }
};

describe('change > redux-store > getLastUpdatedChanges', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.getLastUpdatedChanges.pending(
        'getLastUpdatedChanges',
        undefined
      )
    );

    expect(nextState.lastUpdatedChanges.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.getLastUpdatedChanges.fulfilled(
        [],
        'getLastUpdatedChanges',
        undefined
      )
    );
    expect(nextState.lastUpdatedChanges.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.getLastUpdatedChanges.rejected(
        null,
        'getLastUpdatedChanges',
        undefined
      )
    );
    expect(nextState.lastUpdatedChanges.error).toEqual(true);
  });
  it('thunk action ', async () => {
    const nextState = await actionsChange.getLastUpdatedChanges()(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: { data: { changeManagementList: ChangeMapping } }
        } as RootState),
      {
        changeService: {
          getLastUpdatedChanges: jest
            .fn()
            .mockResolvedValue({ data: { changeSetList: ChangeList } })
        }
      }
    );
    expect(nextState.payload.changes.length).toEqual(3);
  });

  it('thunk action => data undefined ', async () => {
    const nextState = await actionsChange.getLastUpdatedChanges()(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: { data: { changeManagementList: ChangeMapping } }
        } as RootState),
      {
        changeService: {
          getLastUpdatedChanges: jest
            .fn()
            .mockResolvedValue({ data: { changeSetList: undefined } })
        }
      }
    );
    expect(nextState.payload.changes.length).toEqual(0);
  });
});
