import {
  AttachmentData,
  AttachmentMapping,
  ReportMapping
} from 'app/fixtures/change-data';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import { actionsChange, reducer } from 'pages/_commons/redux/Change';

const initReducer: any = {
  changeAttachmentList: { loading: false, error: true, data: {} }
};

describe('change > redux-store > getAttachmentList', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.getAttachmentList.pending('getAttachmentList', '1')
    );

    expect(nextState.changeAttachmentList.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.getAttachmentList.fulfilled([], 'getAttachmentList', '1')
    );
    expect(nextState.changeAttachmentList.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.getAttachmentList.rejected(null, 'getAttachmentList', '1')
    );
    expect(nextState.changeAttachmentList.error).toEqual(true);
  });
  it('thunk action ', async () => {
    const changeId = '123';
    const nextState = await actionsChange.getAttachmentList(changeId)(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: {
            data: { attachmentList: AttachmentMapping, reports: ReportMapping }
          }
        } as RootState),
      {
        changeService: {
          getChangeAttachmentList: jest.fn().mockResolvedValue({
            data: { changeSetAttachment: AttachmentData, reports: [{}] }
          })
        }
      }
    );
    expect(nextState.payload.attachments.length).toEqual(2);
    expect(nextState.payload.reports.length).toEqual(1);
  });
});
