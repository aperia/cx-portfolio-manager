import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsChange,
  ChangeState,
  reducer
} from 'pages/_commons/redux/Change';

const initReducer = {
  changeRejectStatus: { loading: false, error: true }
} as ChangeState;

describe('change > redux-store > rejectChange', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.rejectChange.pending('rejectChange', {
        changeId: '1',
        reason: 'mock reason'
      })
    );
    expect(nextState.changeRejectStatus.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.rejectChange.fulfilled([], 'rejectChange', {
        changeId: '1',
        reason: 'mock reason'
      })
    );
    expect(nextState.changeRejectStatus.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.rejectChange.rejected(null, 'rejectChange', {
        changeId: '1',
        reason: 'mock reason'
      })
    );
    expect(nextState.changeRejectStatus.error).toEqual(true);
  });
  it('thunk action ', async () => {
    const changeId = '123';
    const nextState = await actionsChange.rejectChange({
      changeId,
      reason: 'Reject change'
    })(
      store.dispatch,
      () =>
        ({
          ...store.getState()
        } as RootState),
      {
        changeService: {
          rejectChange: jest.fn().mockResolvedValue({ data: { messages: [] } })
        }
      }
    );
    expect(nextState.payload).toEqual(true);
  });
});
