import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { CHANGE_DATE_INFORMATION_FIELDS } from 'app/constants/api-links';
import { mappingDataFromObj } from 'app/helpers';
import get from 'lodash.get';
import set from 'lodash.set';
import { ChangeState } from '../types';

export const getChangeDateInfo = createAsyncThunk<any, string, ThunkAPIConfig>(
  'mapping/getDateInformation',
  async (changeId: string, thunkAPI) => {
    const { changeService } = get(thunkAPI, 'extra');
    const resp = await changeService.getDateInformation({
      selectFields: CHANGE_DATE_INFORMATION_FIELDS,
      searchFields: { changeSetId: changeId }
    });

    const state = thunkAPI.getState();
    // Take mapping model
    const changeDetailSnapshotMapping = state.mapping?.data?.changeDateInfo;

    // Mapping
    const mappedData = mappingDataFromObj(
      resp?.data,
      changeDetailSnapshotMapping!
    );

    return { data: mappedData };
  }
);
export const getChangeDateInfoBuilder = (
  builder: ActionReducerMapBuilder<ChangeState>
) => {
  builder.addCase(getChangeDateInfo.pending, (draftState, action) => {
    set(draftState.changeDateInfo, 'loading', true);
  });
  builder.addCase(getChangeDateInfo.fulfilled, (draftState, action) => {
    return {
      ...draftState,
      changeDateInfo: { ...action.payload, loading: false, error: false }
    };
  });
  builder.addCase(getChangeDateInfo.rejected, (draftState, action) => {
    set(draftState.changeDateInfo, 'loading', false);
    set(draftState.changeDateInfo, 'error', true);
  });
};
