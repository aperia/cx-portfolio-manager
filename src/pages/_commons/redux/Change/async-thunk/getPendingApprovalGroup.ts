import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { mappingDataFromObj } from 'app/helpers';
import { isEmpty } from 'lodash';
import get from 'lodash.get';
import set from 'lodash.set';
import { ChangeState } from '../types';

export const getPendingApprovalGroup = createAsyncThunk<
  any,
  string,
  ThunkAPIConfig
>('mapping/getPendingApprovalGroup', async (changeId: string, thunkAPI) => {
  const { changeService } = get(thunkAPI, 'extra');
  const resp = await changeService.pendingApprovalGroupList(changeId);

  const state = thunkAPI.getState();
  // Take mapping model
  const pendingApprovalGroupMapping = state.mapping?.data?.pendingApprovals;

  // Mapping
  const mappedData = mappingDataFromObj(
    resp?.data?.pendingApprovalBy,
    pendingApprovalGroupMapping!
  );

  if (isEmpty(mappedData)) return { approvalByGroup: null };

  mappedData.users = mappedData.user;

  return { approvalByGroup: mappedData };
});
export const getPendingApprovalGroupBuilder = (
  builder: ActionReducerMapBuilder<ChangeState>
) => {
  builder.addCase(getPendingApprovalGroup.pending, (draftState, action) => {
    set(draftState.pendingApprovalByGroup, 'loading', true);
    set(draftState.pendingApprovalByGroup, 'error', false);
  });
  builder.addCase(getPendingApprovalGroup.fulfilled, (draftState, action) => {
    const { approvalByGroup } = action.payload;
    set(draftState.pendingApprovalByGroup, 'loading', false);
    set(draftState.pendingApprovalByGroup, 'error', false);
    set(draftState.pendingApprovalByGroup, 'approvalByGroup', approvalByGroup);
  });
  builder.addCase(getPendingApprovalGroup.rejected, (draftState, action) => {
    set(draftState.pendingApprovalByGroup, 'loading', false);
    set(draftState.pendingApprovalByGroup, 'error', true);
  });
};
