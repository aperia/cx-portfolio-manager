import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsChange,
  ChangeState,
  reducer
} from 'pages/_commons/redux/Change';

const initReducer = {
  changeSubmitStatus: { loading: false, error: true }
} as ChangeState;

describe('change > redux-store > submitChange', () => {
  const mockChangeField = {
    changeId: '123',
    changeName: 'John cena',
    changeOwnerId: '1',
    effectiveDate: '2021-03-03',
    description: 'Some description'
  };

  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.submitChange.pending('submitChange', mockChangeField)
    );
    expect(nextState.changeSubmitStatus.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.submitChange.fulfilled([], 'submitChange', mockChangeField)
    );
    expect(nextState.changeSubmitStatus.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.submitChange.rejected(null, 'submitChange', mockChangeField)
    );
    expect(nextState.changeSubmitStatus.error).toEqual(true);
  });
  it('thunk action => success', async () => {
    const nextState = await actionsChange.submitChange(mockChangeField)(
      store.dispatch,
      () =>
        ({
          ...store.getState()
        } as RootState),
      {
        changeService: {
          submitChange: jest.fn().mockResolvedValue({
            data: {}
          })
        }
      }
    );
    expect(nextState.payload).toEqual(undefined);
  });
});
