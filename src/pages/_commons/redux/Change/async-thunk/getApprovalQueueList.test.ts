import { ChangeList, ChangeMapping } from 'app/fixtures/change-data';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import { actionsChange, reducer } from 'pages/_commons/redux/Change';

const initReducer: any = {
  approvalQueues: { loading: false, error: true, data: [], changeTypeList: [] }
};

describe('change > redux-store > getApprovalQueueList', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.getApprovalQueueList.pending(
        'getApprovalQueueList',
        undefined
      )
    );

    expect(nextState.approvalQueues.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.getApprovalQueueList.fulfilled(
        [],
        'getApprovalQueueList',
        undefined
      )
    );
    expect(nextState.approvalQueues.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.getApprovalQueueList.rejected(
        null,
        'getApprovalQueueList',
        undefined
      )
    );
    expect(nextState.approvalQueues.error).toEqual(true);
  });
  it('thunk action ', async () => {
    const nextState = await actionsChange.getApprovalQueueList()(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: { data: { changeManagementList: ChangeMapping } }
        } as RootState),
      {
        changeService: {
          getApprovalQueueList: jest
            .fn()
            .mockResolvedValue({ data: { changeSetList: ChangeList } })
        }
      }
    );
    expect(nextState.payload.rawData.length).toEqual(3);
  });

  it('thunk action => data undefined', async () => {
    const nextState = await actionsChange.getApprovalQueueList()(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: { data: { changeManagementList: ChangeMapping } }
        } as RootState),
      {
        changeService: {
          getApprovalQueueList: jest
            .fn()
            .mockResolvedValue({ data: { changeSetList: undefined } })
        }
      }
    );
    expect(nextState.payload.rawData.length).toEqual(0);
  });
});
