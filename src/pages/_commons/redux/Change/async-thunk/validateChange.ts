import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import get from 'lodash.get';
import set from 'lodash.set';
import { ChangeState } from '../types';

interface IChangeFields {
  changeId: string;
}
export const validateChange = createAsyncThunk<
  any,
  IChangeFields,
  ThunkAPIConfig
>('mapping/validateChange', async ({ changeId }, thunkAPI) => {
  const { changeService } = get(thunkAPI, 'extra') as APIMapping;
  await changeService.validateChange(changeId);

  return true;
});

export const validateChangeBuilder = (
  builder: ActionReducerMapBuilder<ChangeState>
) => {
  builder.addCase(validateChange.pending, (draftState, action) => {
    set(draftState.changeValidateStatus, 'loading', true);
  });
  builder.addCase(validateChange.fulfilled, (draftState, action) => {
    set(draftState.changeValidateStatus, 'loading', false);
  });
  builder.addCase(validateChange.rejected, (draftState, action) => {
    set(draftState.changeValidateStatus, 'loading', false);
    set(draftState.changeValidateStatus, 'error', true);
  });
};
