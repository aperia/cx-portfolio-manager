import { DataMaping, ExportData } from 'app/fixtures/change-data';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsChange,
  ChangeState,
  reducer
} from 'pages/_commons/redux/Change';

const initReducer = {
  changeDetails: { loading: false, error: true, data: {} }
} as ChangeState;

describe('change > redux-store > getChangeDetail', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.getChangeDetail.pending('getChangeDetail', '1')
    );
    expect(nextState.changeDetails.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.getChangeDetail.fulfilled([], 'getChangeDetail', '1')
    );
    expect(nextState.changeDetails.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.getChangeDetail.rejected(null, 'getChangeDetail', '1')
    );
    expect(nextState.changeDetails.error).toEqual(true);
  });
  it('thunk action -> pricing', async () => {
    const changeId = '123';
    const nextState = await actionsChange.getChangeDetail(changeId)(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: {
            data: { changeDetailSnapshot: DataMaping.changeDetailSnapshot }
          }
        } as RootState),
      {
        changeService: {
          getChangeDetail: jest.fn().mockResolvedValue({
            data: {
              changeSetDetail: ExportData.changeDetailPricing
            }
          })
        }
      }
    );
    expect(nextState.payload.data.changeId).toEqual('66190634');
  });
  it('thunk action -> non pricing', async () => {
    const changeId = '123';
    const nextState = await actionsChange.getChangeDetail(changeId)(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: {
            data: { changeDetailSnapshot: DataMaping.changeDetailSnapshot }
          }
        } as RootState),
      {
        changeService: {
          getChangeDetail: jest.fn().mockResolvedValue({
            data: { changeSetDetail: ExportData.changeDetailNonPricing }
          })
        }
      }
    );

    expect(nextState.payload.data.changeId).toEqual('79565064');
    expect(nextState.payload.data.noOfDMMTablesCreated).toEqual(undefined);
  });

  it('thunk action -> non pricing - ChangeType.Pricing', async () => {
    const changeId = '123';
    const nextState = await actionsChange.getChangeDetail(changeId)(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: {
            data: { changeDetailSnapshot: DataMaping.changeDetailSnapshot }
          }
        } as RootState),
      {
        changeService: {
          getChangeDetail: jest.fn().mockResolvedValue({
            data: {
              changeSetDetail: {
                ...ExportData.changeDetailNonPricing,
                changeType: 'PRICING'
              }
            }
          })
        }
      }
    );

    expect(nextState.payload.data.changeId).toEqual('79565064');
    expect(nextState.payload.data.noOfDMMTablesCreated).toEqual(6005);
  });
});
