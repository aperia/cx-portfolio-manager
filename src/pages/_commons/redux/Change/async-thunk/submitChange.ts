import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import get from 'lodash.get';
import set from 'lodash.set';
import { ChangeState } from '../types';

interface IChangeFields {
  changeId: string;
}
export const submitChange = createAsyncThunk<
  any,
  IChangeFields,
  ThunkAPIConfig
>('mapping/submitChange', async ({ changeId }, thunkAPI) => {
  const { changeService } = get(thunkAPI, 'extra');
  const resp = await changeService.submitChange(changeId);

  return resp?.data?.changeSetDetail;
});

export const submitChangeBuilder = (
  builder: ActionReducerMapBuilder<ChangeState>
) => {
  builder.addCase(submitChange.pending, (draftState, action) => {
    set(draftState.changeSubmitStatus, 'loading', true);
  });
  builder.addCase(submitChange.fulfilled, (draftState, action) => {
    set(draftState.changeSubmitStatus, 'loading', false);
  });
  builder.addCase(submitChange.rejected, (draftState, action) => {
    set(draftState.changeSubmitStatus, 'loading', false);
    set(draftState.changeSubmitStatus, 'error', true);
  });
};
