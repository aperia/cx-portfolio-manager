import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsChange,
  ChangeState,
  reducer
} from 'pages/_commons/redux/Change';

const initReducer = {
  changeDateInfo: { loading: false, error: true, data: {} }
} as ChangeState;

describe('change > redux-store > getChangeDateInfo', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.getChangeDateInfo.pending('getChangeDateInfo', '1')
    );

    expect(nextState.changeDateInfo.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.getChangeDateInfo.fulfilled([], 'getChangeDateInfo', '1')
    );
    expect(nextState.changeDateInfo.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.getChangeDateInfo.rejected(null, 'getChangeDateInfo', '1')
    );
    expect(nextState.changeDateInfo.error).toEqual(true);
  });
  it('thunk action ', async () => {
    const changeId = '123';
    const nextState = await actionsChange.getChangeDateInfo(changeId)(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: {
            data: {
              changeDateInfo: {
                minDate: 'minDate',
                maxDate: 'maxDate',
                invalidDate: 'invalidDate'
              }
            }
          }
        } as RootState),
      {
        changeService: {
          getDateInformation: jest.fn().mockResolvedValue({
            data: {
              minDate: '2021-01-30T00:00:00',
              maxDate: '2021-05-04T00:00:00',
              invalidDate: [
                '2021-03-01T14:46:17.5680773+07:00',
                '2021-03-03T14:46:17.5680829+07:00'
              ]
            }
          })
        }
      }
    );
    expect(nextState.payload.data.invalidDate.length).toEqual(2);
  });
});
