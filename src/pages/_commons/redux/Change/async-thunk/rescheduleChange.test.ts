import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsChange,
  ChangeState,
  reducer
} from 'pages/_commons/redux/Change';

const initReducer = {
  changeUpdateStatus: { loading: false, error: true }
} as ChangeState;

describe('change > redux-store > rescheduleChange', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.rescheduleChange.pending('rescheduleChange', {
        changeId: '1',
        effectiveDate: '2021-01-01'
      })
    );
    expect(nextState.changeUpdateStatus.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.rescheduleChange.fulfilled(true, 'rescheduleChange', {
        changeId: '1',
        effectiveDate: '2021-01-01'
      })
    );
    expect(nextState.changeUpdateStatus.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.rescheduleChange.rejected(null, 'rescheduleChange', {
        changeId: '1',
        effectiveDate: '2021-01-01'
      })
    );
    expect(nextState.changeUpdateStatus.error).toEqual(true);
  });

  it('thunk action => success', async () => {
    const changeId = '123';
    const nextState = await actionsChange.rescheduleChange({
      changeId,
      effectiveDate: '2021-03-03'
    })(
      store.dispatch,
      () =>
        ({
          ...store.getState()
        } as RootState),
      {
        changeService: {
          rescheduleChange: jest.fn().mockResolvedValue({
            data: {}
          })
        }
      }
    );
    expect(nextState.payload).toEqual(true);
  });
});
