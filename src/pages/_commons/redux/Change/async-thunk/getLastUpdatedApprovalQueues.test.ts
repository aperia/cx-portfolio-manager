import { ChangeList, ChangeMapping } from 'app/fixtures/change-data';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsChange,
  ChangeState,
  reducer
} from 'pages/_commons/redux/Change';

const initReducer = {
  lastUpdatedApprovalQueues: {
    loading: false,
    error: true,
    approvalQueues: [] as IChangeDetail[]
  }
} as ChangeState;

describe('change > redux-store > getLastUpdatedApprovalQueues', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.getLastUpdatedApprovalQueues.pending(
        'getLastUpdatedApprovalQueues',
        undefined
      )
    );

    expect(nextState.lastUpdatedApprovalQueues.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.getLastUpdatedApprovalQueues.fulfilled(
        [],
        'getLastUpdatedApprovalQueues',
        undefined
      )
    );
    expect(nextState.lastUpdatedApprovalQueues.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.getLastUpdatedApprovalQueues.rejected(
        null,
        'getLastUpdatedApprovalQueues',
        undefined
      )
    );
    expect(nextState.lastUpdatedApprovalQueues.error).toEqual(true);
  });
  it('thunk action ', async () => {
    const nextState = await actionsChange.getLastUpdatedApprovalQueues()(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: { data: { changeManagementList: ChangeMapping } }
        } as RootState),
      {
        changeService: {
          getApprovalQueueList: jest
            .fn()
            .mockResolvedValue({ data: { approvalQueueList: ChangeList } })
        }
      }
    );
    expect(nextState.payload.approvalQueues.length).toEqual(3);
  });
});
