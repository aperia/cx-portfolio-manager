import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { formatTime, mappingDataFromArray } from 'app/helpers';
import get from 'lodash.get';
import set from 'lodash.set';
import { ChangeState } from '../types';

export const getAttachmentList = createAsyncThunk<any, string, ThunkAPIConfig>(
  'mapping/getAttachmentList',
  async (changeId: string, thunkAPI) => {
    const { changeService } = get(thunkAPI, 'extra');
    const resp = await changeService.getChangeAttachmentList(changeId);

    const state = thunkAPI.getState();
    // Take mapping model
    const changeAttachmentListMapping = state.mapping?.data?.attachmentList;
    const changeReportsMapping = state.mapping?.data?.reports;

    // Mapping
    const mappedAttachmentsData = mappingDataFromArray(
      resp?.data?.changeSetAttachment?.attachmentList,
      changeAttachmentListMapping!
    );
    const mappedReportsData = mappingDataFromArray(
      resp?.data?.reports,
      changeReportsMapping!
    );

    const attachments = mappedAttachmentsData?.map((r: any) => ({
      ...r,
      uploadedDate: formatTime(r.uploadedDate).datetime
    }));
    const reports = mappedReportsData?.map((r: any) => ({
      ...r,
      systemGeneratedDate: formatTime(r.systemGeneratedDate).datetime
    }));

    return { attachments, reports };
  }
);

export const getAttachmentListBuilder = (
  builder: ActionReducerMapBuilder<ChangeState>
) => {
  builder.addCase(getAttachmentList.pending, (draftState, action) => {
    set(draftState.changeAttachmentList, 'loading', true);
  });
  builder.addCase(getAttachmentList.fulfilled, (draftState, action) => {
    return {
      ...draftState,
      changeAttachmentList: {
        ...draftState.changeAttachmentList,
        data: action.payload,
        loading: false
      }
    };
  });
  builder.addCase(getAttachmentList.rejected, (draftState, action) => {
    set(draftState.changeAttachmentList, 'loading', false);
    set(draftState.changeAttachmentList, 'error', true);
  });
};
