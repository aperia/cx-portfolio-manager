import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { ChangeType } from 'app/constants/enums';
import { formatTime, mappingDataFromArray } from 'app/helpers';
import get from 'lodash.get';
import orderBy from 'lodash.orderby';
import set from 'lodash.set';
import { ChangeState } from '../types';

export const getLastUpdatedApprovalQueues = createAsyncThunk<
  any,
  undefined,
  ThunkAPIConfig
>('mapping/getLastUpdatedApprovalQueues', async (args: undefined, thunkAPI) => {
  const { changeService } = get(thunkAPI, 'extra');
  const resp = await changeService.getApprovalQueueList();

  const state = thunkAPI.getState();
  // Take mapping model
  const changeManagementListMapping = state.mapping?.data?.changeManagementList;

  // Mapping
  const mappedData = mappingDataFromArray(
    resp?.data?.approvalQueueList,
    changeManagementListMapping!
  )?.map((p: IChangeDetail) => {
    if (p.changeType !== ChangeType.Pricing) {
      delete p.pricingStrategiesCreatedCount;
      delete p.pricingStrategiesUpdatedCount;
      delete p.pricingStrategiesImpacted;
      delete p.pricingMethodsCreatedCount;
      delete p.pricingMethodsUpdatedCount;
      delete p.noOfDMMTablesCreated;
      delete p.noOfDMMTablesUpdated;
    } else {
      delete p.cardholdersAccountsImpacted;
      delete p.portfoliosImpacted;
    }
    return p;
  });

  const rawData = mappedData?.map((r: any) => ({
    ...r,
    effectiveDateFormat: formatTime(r.effectiveDate).date
  }));

  let approvalQueues = orderBy(
    rawData,
    ['effectiveDateFormat', 'changeName'],
    ['desc', 'asc']
  );

  approvalQueues = approvalQueues.slice(0, 5);

  return {
    approvalQueues
  };
});
export const getLastUpdatedApprovalQueuesBuilder = (
  builder: ActionReducerMapBuilder<ChangeState>
) => {
  builder.addCase(
    getLastUpdatedApprovalQueues.pending,
    (draftState, action) => {
      set(draftState.lastUpdatedApprovalQueues, 'loading', true);
    }
  );
  builder.addCase(
    getLastUpdatedApprovalQueues.fulfilled,
    (draftState, action) => {
      return {
        ...draftState,
        lastUpdatedApprovalQueues: { ...action.payload, loading: false }
      };
    }
  );
  builder.addCase(
    getLastUpdatedApprovalQueues.rejected,
    (draftState, action) => {
      set(draftState.lastUpdatedApprovalQueues, 'loading', false);
      set(draftState.lastUpdatedApprovalQueues, 'error', true);
    }
  );
};
