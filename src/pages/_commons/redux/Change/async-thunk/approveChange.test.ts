import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsChange,
  ChangeState,
  reducer
} from 'pages/_commons/redux/Change';

const initReducer = {
  changeApproveStatus: { loading: false, error: true }
} as ChangeState;

describe('change > redux-store > approveChange', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.approveChange.pending('approveChange', { changeId: '1' })
    );
    expect(nextState.changeApproveStatus.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.approveChange.fulfilled([], 'approveChange', {
        changeId: '1'
      })
    );
    expect(nextState.changeApproveStatus.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsChange.approveChange.rejected(null, 'approveChange', {
        changeId: '1'
      })
    );
    expect(nextState.changeApproveStatus.error).toEqual(true);
  });
  it('thunk action ', async () => {
    const changeId = '123';
    const nextState = await actionsChange.approveChange({ changeId })(
      store.dispatch,
      () =>
        ({
          ...store.getState()
        } as RootState),
      {
        changeService: {
          approveChange: jest.fn().mockResolvedValue({ data: { messages: [] } })
        }
      }
    );
    expect(nextState.payload).toEqual(true);
  });
});
