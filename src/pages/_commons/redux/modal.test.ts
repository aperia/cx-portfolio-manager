import { renderHook } from '@testing-library/react-hooks';
import { ModalsState } from 'app/types';
import {
  actionsModals,
  reducer,
  useSelectCurrentOpenedModal
} from 'pages/_commons/redux/modal';
import * as reactRedux from 'react-redux';

const initReducer: ModalsState = {
  registerModals: []
};

describe('modals > reducers', () => {
  it('register', () => {
    const nextState = reducer(
      initReducer,
      actionsModals.register({ name: 'modal-id-01' })
    );
    expect(nextState.registerModals.length).toEqual(1);
  });
  it('register duplicate', () => {
    const nextState = reducer(
      { ...initReducer, registerModals: [{ name: 'modal-id-01' }] },
      actionsModals.register({ name: 'modal-id-01' })
    );
    expect(nextState.registerModals.length).toEqual(1);
  });
  it('close', () => {
    const nextState = reducer(
      { ...initReducer, registerModals: [{ name: 'modal-id-01' }] },
      actionsModals.close('modal-id-01')
    );
    expect(nextState.registerModals.length).toEqual(0);
  });
  it('closeAll', () => {
    const nextState = reducer(
      { ...initReducer, registerModals: [{ name: 'modal-id-01' }] },
      actionsModals.closeAll()
    );
    expect(nextState.registerModals.length).toEqual(0);
  });
  it('close but id not exist', () => {
    const nextState = reducer(
      { ...initReducer, registerModals: [{ name: 'modal-id-01' }] },
      actionsModals.close('modal-id-02')
    );

    expect(nextState.registerModals.length).toEqual(1);
  });
});
describe('modals > select-hook', () => {
  let selectorMock: jest.SpyInstance;
  const mockSelectorModal = (modalState: ModalsState) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector => selector({ modals: modalState }));
  };
  afterEach(() => {
    selectorMock.mockClear();
  });

  it('useSelectCurrentOpenedModal', () => {
    const pre = {
      registerModals: [{ name: 'modal-id-01' }, { name: 'modal-id-02' }]
    } as any;

    mockSelectorModal(pre);
    const { result } = renderHook(() =>
      useSelectCurrentOpenedModal('modal-id-02')
    );

    expect(result.current).toEqual([true, false]);
  });

  it('useSelectCurrentOpenedModal > isModalFull', () => {
    const pre = {
      registerModals: [
        { name: 'modal-id-01', isModalFull: true },
        { name: 'modal-id-02', isModalFull: true },
        { name: 'modal-id-03', isModalFull: true }
      ]
    } as any;

    mockSelectorModal(pre);
    const { result } = renderHook(() =>
      useSelectCurrentOpenedModal('modal-id-02')
    );

    expect(result.current).toEqual([true, false]);
  });

  it('useSelectCurrentOpenedModal > empty registerModals', () => {
    const pre = {
      registerModals: []
    } as any;

    mockSelectorModal(pre);
    const { result } = renderHook(() =>
      useSelectCurrentOpenedModal('modal-id-01')
    );

    expect(result.current).toEqual([false, true]);
  });
});
