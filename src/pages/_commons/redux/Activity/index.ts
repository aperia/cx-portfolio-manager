import { createSlice } from '@reduxjs/toolkit';
import {
  getActivitiesInChange,
  getActivitiesInChangeBuilder
} from './async-thunk';
import reducers, { defaultDataFilter } from './reducers';
import { ActivityState } from './types';

const { actions, reducer } = createSlice({
  name: 'activity',
  initialState: {
    lastUpdatedActivities: { activities: [], loading: true },
    activitiesInChange: {
      activities: [],
      loading: true,
      dataFilter: defaultDataFilter
    }
  } as ActivityState,
  reducers,
  extraReducers: builder => {
    getActivitiesInChangeBuilder(builder);
  }
});

const extraActions = {
  ...actions,
  getActivitiesInChange
};

export * from './select-hooks';
export * from './types';
export { extraActions as actionsActivity, reducer };
