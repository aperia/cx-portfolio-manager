import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import {
  ACTIVITY_ACTION,
  ACTIVITY_FIELD,
  CHANGE_STATUS_MAPPING
} from 'app/constants/mapping';
import {
  camelToText,
  convertUTCDateToLocalDate,
  mappingDataFromArray
} from 'app/helpers';
import get from 'lodash.get';
import isEmpty from 'lodash.isempty';
import { ActivityState } from '../types';

export const getActivitiesInChange = createAsyncThunk<
  any,
  string,
  ThunkAPIConfig
>('mapping/getActivitiesInChange', async (changeId: string, thunkAPI) => {
  const { activityService } = get(thunkAPI, 'extra');
  const resp = await activityService.getActivitiesInChange(changeId);

  const state = thunkAPI.getState();
  const mappingJson = state.mapping?.data?.activityList;
  const rawData = mappingDataFromArray(
    resp?.data?.activityList,
    mappingJson!
  )?.map((activity: IActivity) => {
    const { action, actionDetails } = activity;

    return {
      ...activity,
      dateChanged: convertUTCDateToLocalDate(new Date(activity.dateChanged)),
      action: ACTIVITY_ACTION[action] || camelToText(action)?.toLowerCase(),
      actionDetails: actionDetails
        ?.filter(
          ({ previousValue, newValue }: IActivityDetail) =>
            isEmpty(previousValue) ||
            isEmpty(newValue) ||
            previousValue !== newValue
        )
        ?.map((detail: IActivityDetail) => {
          let { field, previousValue, newValue } = detail;
          const { workflowId, workflowName } = detail;
          if (workflowId && workflowName) {
            field = 'Workflow Name';
            previousValue = undefined;
            newValue = workflowName;
          } else {
            switch (field) {
              case 'ChangeStatus':
                previousValue = camelToText(
                  CHANGE_STATUS_MAPPING[previousValue!] || ''
                );
                newValue = camelToText(CHANGE_STATUS_MAPPING[newValue!] || '');
                break;
              case 'ChangeOwner':
                //TODO: convert change owner if needed
                break;
            }
            field = ACTIVITY_FIELD[field!] || camelToText(field || '');
          }
          return {
            field,
            previousValue,
            newValue,
            workflowId,
            workflowName
          };
        })
    };
  });

  const dataMapping = rawData.filter(
    r => r.action && r.actionDetails && r.actionDetails.length > 0
  );

  return dataMapping;
});
export const getActivitiesInChangeBuilder = (
  builder: ActionReducerMapBuilder<ActivityState>
) => {
  builder.addCase(getActivitiesInChange.pending, (draftState, action) => {
    return {
      ...draftState,
      activitiesInChange: {
        ...draftState.activitiesInChange,
        loading: true,
        error: false
      }
    };
  });
  builder.addCase(getActivitiesInChange.fulfilled, (draftState, action) => {
    return {
      ...draftState,
      activitiesInChange: {
        ...draftState.activitiesInChange,
        activities: action.payload,
        loading: false
      }
    };
  });
  builder.addCase(getActivitiesInChange.rejected, (draftState, action) => {
    return {
      ...draftState,
      activitiesInChange: {
        ...draftState.activitiesInChange,
        loading: false,
        error: true
      }
    };
  });
};
