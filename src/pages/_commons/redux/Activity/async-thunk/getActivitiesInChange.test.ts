import { ActivityList, ActivityMapping } from 'app/fixtures/activity-data';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsActivity,
  ActivityState,
  reducer
} from 'pages/_commons/redux/Activity';
import { defaultDataFilter } from 'pages/_commons/redux/Activity/reducers';

const initReducer: ActivityState = {
  lastUpdatedActivities: { activities: [], loading: false },
  activitiesInChange: {
    activities: [],
    loading: false,
    dataFilter: defaultDataFilter
  }
};

describe('activity > redux-store > getActivitiesInChange', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsActivity.getActivitiesInChange.pending(
        'getActivitiesInChange',
        '1'
      )
    );
    expect(nextState.activitiesInChange.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsActivity.getActivitiesInChange.fulfilled(
        [],
        'getActivitiesInChange',
        '1'
      )
    );
    expect(nextState.activitiesInChange.loading).toEqual(false);
    expect(nextState.activitiesInChange.activities).toEqual([]);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsActivity.getActivitiesInChange.rejected(null, 'activities', '1')
    );
    expect(nextState.activitiesInChange.loading).toEqual(false);
  });
  it('thunk action', async () => {
    const changeId = '123';
    const nextState = await actionsActivity.getActivitiesInChange(changeId)(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: { data: { activityList: ActivityMapping } }
        } as RootState),
      {
        activityService: {
          getActivitiesInChange: jest
            .fn()
            .mockResolvedValue({ data: { activityList: ActivityList } })
        }
      }
    );
    expect(nextState.payload.length).toEqual(3);
  });
});
