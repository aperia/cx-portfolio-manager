import { renderHook } from '@testing-library/react-hooks';
import { ActivityList } from 'app/fixtures/activity-data';
import {
  ActivityState,
  useSelectActivitiesInChange,
  useSelectActivitiesInChangeFilter,
  useSelectActivitiesInChangeSearchValue,
  useSelectLastUpdatedActivities
} from 'pages/_commons/redux/Activity';
import { defaultDataFilter } from 'pages/_commons/redux/Activity/reducers';
import * as reactRedux from 'react-redux';

describe('select-hooks > activity', () => {
  let selectorMock: jest.SpyInstance;
  const mockSelectorActivities = (activityState: ActivityState) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector => selector({ activity: activityState }));
  };
  afterEach(() => {
    selectorMock.mockClear();
  });

  it('useSelectLastUpdatedActivities', () => {
    const pre = {
      lastUpdatedActivities: {
        activities: [{}]
      }
    } as any;

    mockSelectorActivities(pre);
    const { result } = renderHook(() => useSelectLastUpdatedActivities());

    expect(result.current.activities.length).toEqual(1);
  });

  it('useSelectActivitiesInChange -> search value = Jesse Dunn', () => {
    const pre: any = {
      activitiesInChange: {
        activities: ActivityList,
        loading: false,
        error: false,
        dataFilter: { ...defaultDataFilter, searchValue: 'Jesse Dunn' }
      }
    };

    mockSelectorActivities(pre);
    const { result } = renderHook(() => useSelectActivitiesInChange());

    expect(result.current.activityList.length).toEqual(1);
  });

  it('useSelectActivitiesInChange -> search value = rejectedChange', () => {
    const pre: any = {
      activitiesInChange: {
        activities: ActivityList,
        loading: false,
        error: false,
        dataFilter: { ...defaultDataFilter, searchValue: 'rejectedChange' }
      }
    };

    mockSelectorActivities(pre);
    const { result } = renderHook(() => useSelectActivitiesInChange());

    expect(result.current.activityList.length).toEqual(1);
  });

  it('useSelectActivitiesInChange -> search value = changeName', () => {
    const pre: any = {
      activitiesInChange: {
        activities: ActivityList,
        loading: false,
        error: false,
        dataFilter: { ...defaultDataFilter, searchValue: 'changeName' }
      }
    };

    mockSelectorActivities(pre);
    const { result } = renderHook(() => useSelectActivitiesInChange());

    expect(result.current.activityList.length).toEqual(1);
  });

  it('useSelectActivitiesInChange -> search value empty', () => {
    const pre: any = {
      activitiesInChange: {
        activities: ActivityList,
        loading: false,
        error: false,
        dataFilter: defaultDataFilter
      }
    };

    mockSelectorActivities(pre);
    const { result } = renderHook(() => useSelectActivitiesInChange());

    expect(result.current.activityList.length).toEqual(3);
  });

  it('useSelectActivitiesInChange -> empty details', () => {
    const pre: any = {
      activitiesInChange: {
        activities: ActivityList.map(a => ({
          ...a,
          actionDetails: null
        })),
        loading: false,
        error: false,
        dataFilter: { ...defaultDataFilter, searchValue: 'changeName' }
      }
    };

    mockSelectorActivities(pre);
    const { result } = renderHook(() => useSelectActivitiesInChange());

    expect(result.current.activityList.length).toEqual(0);
  });

  it('useSelectActivitiesInChangeFilter return searchValue = test', () => {
    mockSelectorActivities({
      activitiesInChange: {
        activities: [],
        loading: false,
        error: false,
        dataFilter: { searchValue: 'test' }
      },
      lastUpdatedActivities: {
        activities: [],
        loading: false
      }
    } as ActivityState);

    const { result } = renderHook(() => useSelectActivitiesInChangeFilter());
    expect(result.current.searchValue).toEqual('test');
  });

  it('useSelectActivitiesInChangeSearchValue return test', () => {
    const searchValue = 'test';
    const pre: ActivityState = {
      activitiesInChange: {
        activities: [],
        loading: false,
        error: false,
        dataFilter: { searchValue }
      },
      lastUpdatedActivities: {} as any
    };

    mockSelectorActivities(pre);
    const { result } = renderHook(() =>
      useSelectActivitiesInChangeSearchValue()
    );

    expect(result.current).toEqual('test');
  });
});
