import { createSelector } from '@reduxjs/toolkit';
import isEmpty from 'lodash.isempty';
import _orderBy from 'lodash.orderby';
import { shallowEqual, useSelector } from 'react-redux';
import { LastUpdatedActivitiesState } from '../types';

export const useSelectLastUpdatedActivities = () => {
  return useSelector<RootState, LastUpdatedActivitiesState>(
    state => state.activity!.lastUpdatedActivities,
    shallowEqual
  );
};

const activitiesInChangeSelector = createSelector(
  (state: RootState) => state.activity!.activitiesInChange,
  data => {
    const { activities: rawActivities, loading, error, dataFilter } = data;

    const { searchValue, page, pageSize, sortBy, orderBy } = dataFilter || {};

    const searchTrimmed = searchValue?.trim();

    let activities = _orderBy(
      rawActivities.map(i => ({ ...i })),
      sortBy,
      orderBy
    );

    if (!isEmpty(searchTrimmed)) {
      activities = activities.filter(
        i =>
          i.name?.toLowerCase().includes(searchTrimmed!.toLowerCase()) ||
          i.action?.toLowerCase().includes(searchTrimmed!.toLowerCase()) ||
          (i.actionDetails || []).some(
            d =>
              d.field?.toLowerCase().includes(searchTrimmed!.toLowerCase()) ||
              d.previousValue
                ?.toLowerCase()
                .includes(searchTrimmed!.toLowerCase()) ||
              d.newValue?.toLowerCase().includes(searchTrimmed!.toLowerCase())
          )
      );
    }

    const total = activities.length;
    activities = activities.slice(0, page! * pageSize!);

    return {
      loading,
      total,
      activityList: activities,
      error
    };
  }
);

export const useSelectActivitiesInChange = () => {
  return useSelector<
    RootState,
    {
      activityList: IActivity[];
      loading: boolean;
      total: number;
      error?: boolean;
    }
  >(activitiesInChangeSelector);
};

const activitiesInChangeFilterSelector = createSelector(
  (state: RootState) => state.activity!.activitiesInChange.dataFilter!,
  data => data
);

export const useSelectActivitiesInChangeFilter = () => {
  return useSelector<RootState, IDataFilter>(activitiesInChangeFilterSelector);
};

export const useSelectActivitiesInChangeSearchValue = () => {
  return useSelector<RootState, string>(
    state => state.activity!.activitiesInChange.dataFilter!.searchValue!,
    shallowEqual
  );
};
