import { PayloadAction } from '@reduxjs/toolkit';
import { ActivitySortByFields, OrderBy } from 'app/constants/enums';
import { ActivityState } from '../types';

const reducers = {
  updateActivitiesInChangeFilter: (
    draftState: ActivityState,
    action: PayloadAction<IDataFilter>
  ) => {
    const { sortBy, orderBy, page, ...filter } = action.payload;

    const customFilter: IDataFilter = {};
    const hasSort = (sortBy?.length || 0) > 0;
    const hasOrder = (orderBy?.length || 0) > 0;
    if (hasSort) {
      customFilter.sortBy = [...(sortBy || []), ...defaultSortBy];
    }
    if (hasOrder) {
      customFilter.orderBy = [...(orderBy || []), ...defaultOrderBy];
    }
    if (!page && !hasSort && !hasOrder) {
      customFilter.page = 1;
    }
    if (page) {
      customFilter.page = page;
    }

    draftState.activitiesInChange.dataFilter = {
      ...draftState.activitiesInChange.dataFilter,
      ...filter,
      ...customFilter
    };
  },
  clearAndResetActivitiesInChangeFilter: (draftState: ActivityState) => {
    draftState.activitiesInChange = {
      ...draftState.activitiesInChange,
      dataFilter: {
        ...defaultDataFilter,
        sortBy: draftState.activitiesInChange.dataFilter?.sortBy,
        orderBy: draftState.activitiesInChange.dataFilter?.orderBy
      }
    };
  },
  setToDefault: (draftState: ActivityState) => {
    draftState.activitiesInChange = {
      ...draftState.activitiesInChange,
      dataFilter: {
        ...defaultDataFilter
      }
    };
  }
};

const defaultSortBy: string[] = [];
const defaultOrderBy: OrderByType[] = [];

export const defaultDataFilter: IDataFilter = {
  page: 1,
  pageSize: 30,
  searchValue: '',
  sortBy: [ActivitySortByFields.DATE_CHANGED, ...defaultSortBy],
  orderBy: [OrderBy.DESC, ...defaultOrderBy]
};

export default reducers;
