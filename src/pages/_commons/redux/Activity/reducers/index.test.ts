import {
  actionsActivity,
  ActivityState,
  reducer
} from 'pages/_commons/redux/Activity';
import { defaultDataFilter } from 'pages/_commons/redux/Activity/reducers';

const initReducer: ActivityState = {
  lastUpdatedActivities: { activities: [], loading: false },
  activitiesInChange: {
    activities: [],
    loading: false,
    dataFilter: defaultDataFilter
  }
};

describe('activity > reducers', () => {
  it('update activities filter > search value', () => {
    const nextState = reducer(
      initReducer,
      actionsActivity.updateActivitiesInChangeFilter({
        searchValue: 'edit changed'
      })
    );
    expect(nextState.activitiesInChange.dataFilter!.searchValue).toEqual(
      'edit changed'
    );
  });
  it('update activities filter > paging', () => {
    const nextState = reducer(
      initReducer,
      actionsActivity.updateActivitiesInChangeFilter({ page: 2 })
    );
    expect(nextState.activitiesInChange.dataFilter!.page).toEqual(2);
  });
  it('update activities filter > sort by', () => {
    const nextState = reducer(
      initReducer,
      actionsActivity.updateActivitiesInChangeFilter({
        sortBy: ['dateChanged']
      })
    );
    expect(nextState.activitiesInChange.dataFilter!.sortBy).toEqual([
      'dateChanged'
    ]);
  });
  it('update activities filter > order by', () => {
    const nextState = reducer(
      initReducer,
      actionsActivity.updateActivitiesInChangeFilter({
        orderBy: ['asc']
      })
    );
    expect(nextState.activitiesInChange.dataFilter!.orderBy).toEqual(['asc']);
  });
  it('update activities filter > clear & reset', () => {
    const nextState = reducer(
      initReducer,
      actionsActivity.clearAndResetActivitiesInChangeFilter()
    );
    expect(nextState.activitiesInChange.dataFilter!.sortBy).toEqual([
      'dateChanged'
    ]);
  });

  it('setToDefault', () => {
    const nextState = reducer(initReducer, actionsActivity.setToDefault());
    expect(nextState.activitiesInChange.dataFilter!.page).toEqual(1);
  });
});
