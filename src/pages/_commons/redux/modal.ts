import { createSelector, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ModalsRegistryState, ModalsState } from 'app/types';
import { useSelector } from 'react-redux';

const { actions, reducer } = createSlice({
  name: 'modals',
  initialState: {
    registerModals: []
  } as ModalsState,
  reducers: {
    register: (state, action: PayloadAction<ModalsRegistryState>) => {
      !state.registerModals.find(m => m.name === action.payload?.name) &&
        state.registerModals.push(action.payload);
    },
    close: (state, action: PayloadAction<string>) => {
      const modalName = action.payload;
      const modalIdx = state.registerModals.findIndex(
        m => m.name === modalName
      );
      if (modalIdx !== -1) {
        state.registerModals.splice(modalIdx, 1);
      }
    },
    closeAll: state => {
      state.registerModals = [];
    }
  }
});

// [boolean, boolean] => [isCurrentOpened, isAllClosed]
const selectCurrentOpenedModal = createSelector<
  RootState,
  string,
  [boolean, boolean],
  [boolean, boolean]
>(
  (state, name) => {
    const numOfRegisterModals = state.modals!.registerModals?.length || 0;

    if (numOfRegisterModals === 0) return [false, true];

    const keepOpen =
      state.modals!.registerModals[numOfRegisterModals - 1]?.name === name ||
      state.modals!.registerModals.some(m => m.name === name && m.isModalFull);

    return [keepOpen, false];
  },
  opened => opened
);

const useSelectCurrentOpenedModal = (name: string): [boolean, boolean] =>
  useSelector<RootState, [boolean, boolean]>(state =>
    selectCurrentOpenedModal(state, name)
  );

export { actions as actionsModals, reducer, useSelectCurrentOpenedModal };
