import { createSlice } from '@reduxjs/toolkit';
import {
  createNewChange,
  createNewChangeBuilder,
  deleteTemplateFile,
  downloadTemplateFile,
  downloadTemplateFileBuilder,
  exportAnalyzer,
  exportAnalyzerBuilder,
  getFileDetails,
  getFileDetailsBuilder,
  getNonWorkflowMetadata,
  getNonWorkflowMetadataBuilder,
  getServiceSubjectSectionOptions,
  getServiceSubjectSectionOptionsBuilder,
  getTableListForWorkflowSetup,
  getTableListForWorkflowSetupBuilder,
  getWorkflowChangeSetList,
  getWorkflowChangeSetListBuilder,
  getWorkflowInstance,
  getWorkflowInstanceBuilder,
  getWorkflowMetadata,
  getWorkflowMetadataBuilder,
  getWorkflowTransactionList
} from './async-thunk';
import {
  createActionCardCompromiseEventWorkflowInstance,
  createActionCardCompromiseEventWorkflowInstanceBuilder,
  setupActionCardCompromiseEventWorkflow,
  setupActionCardCompromiseEventWorkflowBuilder,
  updateActionCardCompromiseEventWorkflowInstance,
  updateActionCardCompromiseEventWorkflowInstanceBuilder
} from './async-thunk/ActionCardCompromiseEventWorkflow';
import {
  createAssignPricingStrategiesWorkflowInstance,
  createAssignPricingStrategiesWorkflowInstanceBuilder,
  setupAssignPricingStrategiesWorkflow,
  setupAssignPricingStrategiesWorkflowBuilder,
  updateAssignPricingStrategiesWorkflowInstance,
  updateAssignPricingStrategiesWorkflowInstanceBuilder
} from './async-thunk/AssignPricingStrategiesWorkflow';
import {
  createBulkArchivePCFWorkflowInstance,
  createBulkArchivePCFWorkflowInstanceBuilder,
  getBulkArchivePCFWorkflowStrategies,
  getBulkArchivePCFWorkflowStrategiesBuilder,
  getBulkArchivePCFWorkflowTableAreas,
  getBulkArchivePCFWorkflowTableAreasBuilder,
  getBulkArchivePCFWorkflowTextAreas,
  getBulkArchivePCFWorkflowTextAreasBuilder,
  setupBulkArchivePCFWorkflowInstance,
  setupBulkArchivePCFWorkflowInstanceBuilder,
  updateBulkArchivePCFWorkflowInstance,
  updateBulkArchivePCFWorkflowInstanceBuilder
} from './async-thunk/BulkArchivePCFWorkflow';
import {
  createBulkExternalStatusManagementWorkflowInstance,
  createBulkExternalStatusManagementWorkflowInstanceBuilder,
  setupBulkExternalStatusManagementWorkflow,
  setupBulkExternalStatusManagementWorkflowBuilder,
  updateBulkExternalStatusManagementWorkflowInstance,
  updateBulkExternalStatusManagementWorkflowInstanceBuilder
} from './async-thunk/BulkExternalStatusManagementWorkflow';
import {
  createBulkSuspendFraudStrategyWorkflowInstance,
  createBulkSuspendFraudStrategyWorkflowInstanceBuilder,
  setupBulkSuspendFraudStrategyWorkflow,
  setupBulkSuspendFraudStrategyWorkflowBuilder,
  updateBulkSuspendFraudStrategyWorkflowInstance,
  updateBulkSuspendFraudStrategyWorkflowInstanceBuilder
} from './async-thunk/BulkSuspendFraudStrategyWorkflow';
import {
  createChangeInterestRatesWorkflowInstance,
  createChangeInterestRatesWorkflowInstanceBuilder,
  getChangeInterestRatesWorkflowMethods,
  getChangeInterestRatesWorkflowMethodsBuilder,
  setupChangeInterestRatesWorkflow,
  setupChangeInterestRatesWorkflowBuilder,
  updateChangeInterestRatesWorkflowInstance,
  updateChangeInterestRatesWorkflowInstanceBuilder
} from './async-thunk/ChangeInterestRatesWorkflow';
import {
  createConditionMCycleAccountsForTestingWorkflowInstance,
  createConditionMCycleAccountsForTestingWorkflowInstanceBuilder,
  getWorkflowTransactionListCondition,
  setupConditionMCycleAccountsForTestingWorkflow,
  setupConditionMCycleAccountsForTestingWorkflowBuilder,
  updateConditionMCycleAccountsForTestingWorkflowInstance,
  updateConditionMCycleAccountsForTestingWorkflowInstanceBuilder
} from './async-thunk/ConditionMCycleAccountsForTestingWorkflow';
import {
  createConfigureProcessingMerchantsWorkflowInstance,
  createConfigureProcessingMerchantsWorkflowInstanceBuilder,
  exportConfigureProcessingMerchants,
  exportConfigureProcessingMerchantsBuilder,
  getConfigureProcessingMerchantsList,
  getConfigureProcessingMerchantsListBuilder,
  setupConfigureProcessingMerchantsWorkflow,
  setupConfigureProcessingMerchantsWorkflowBuilder,
  updateConfigureProcessingMerchantsWorkflowInstance,
  updateConfigureProcessingMerchantsWorkflowInstanceBuilder
} from './async-thunk/ConfigureProcessingMerchantsWorkflow';
import {
  createCustomDataLinkElementsWorkflowInstance,
  createCustomDataLinkElementsWorkflowInstanceBuilder,
  setupCustomDataLinkElementsWorkflow,
  setupCustomDataLinkElementsWorkflowBuilder,
  updateCustomDataLinkElementsWorkflowInstance,
  updateCustomDataLinkElementsWorkflowInstanceBuilder
} from './async-thunk/CustomDataLinkElementsWorkflow';
import {
  createCycleAccountsTestEnvironmentWorkflowInstance,
  createCycleAccountsTestEnvironmentWorkflowInstanceBuilder,
  setupCycleAccountsTestEnvironmentWorkflow,
  setupCycleAccountsTestEnvironmentWorkflowBuilder,
  updateCycleAccountsTestEnvironmentWorkflowInstance,
  updateCycleAccountsTestEnvironmentWorkflowInstanceBuilder
} from './async-thunk/CycleAccountsTestEnvironmentWorkflow';
import {
  createDesignLoanOffersWorkflowInstance,
  createDesignLoanOffersWorkflowInstanceBuilder,
  getDesignLoanOffersWorkflowMethods,
  getDesignLoanOffersWorkflowMethodsBuilder,
  setupDesignLoanOffersWorkflow,
  setupDesignLoanOffersWorkflowBuilder,
  updateDesignLoanOffersWorkflowInstance,
  updateDesignLoanOffersWorkflowInstanceBuilder
} from './async-thunk/DesignLoanOfferWorkflow';
import {
  createDesignPromotionsWorkflowInstance,
  createDesignPromotionsWorkflowInstanceBuilder,
  getDesignPromotionsWorkflowMethods,
  getDesignPromotionsWorkflowMethodsBuilder,
  setupDesignPromotionsWorkflow,
  setupDesignPromotionsWorkflowBuilder,
  updateDesignPromotionsWorkflowInstance,
  updateDesignPromotionsWorkflowInstanceBuilder
} from './async-thunk/DesignPromotionsWorkflow';
import {
  createExportImportChangeDetailsWorkflowInstance,
  createExportImportChangeDetailsWorkflowInstanceBuilder,
  getWorkflowChangeList,
  setupExportImportChangeDetailsWorkflowBuilder,
  setupExportImportChangeDetailsWorkflowInstance,
  updateExportImportChangeDetailsWorkflowInstance,
  updateExportImportChangeDetailsWorkflowInstanceBuilder
} from './async-thunk/ExportImportChangeDetailsWorkflow';
import {
  createFlexibleMonetaryTransactionsWorkflowInstance,
  createFlexibleMonetaryTransactionsWorkflowInstanceBuilder,
  setupFlexibleMonetaryTransactionsWorkflow,
  setupFlexibleMonetaryTransactionsWorkflowBuilder,
  updateFlexibleMonetaryTransactionsWorkflowInstance,
  updateFlexibleMonetaryTransactionsWorkflowInstanceBuilder
} from './async-thunk/FlexibleMonetaryTransactionsWorkflow';
import {
  createFlexibleNonMonetaryTransactionsWorkflowInstance,
  createFlexibleNonMonetaryTransactionsWorkflowInstanceBuilder,
  setupFlexibleNonMonetaryTransactionsWorkflow,
  setupFlexibleNonMonetaryTransactionsWorkflowBuilder,
  updateFlexibleNonMonetaryTransactionsWorkflowInstance,
  updateFlexibleNonMonetaryTransactionsWorkflowInstanceBuilder
} from './async-thunk/FlexibleNonMonetaryTransactionsWorkflow';
import {
  createForceEmbossReplacementCardsWorkflowInstance,
  createForceEmbossReplacementCardsWorkflowInstanceBuilder,
  setupForceEmbossReplacementCardsWorkflow,
  setupForceEmbossReplacementCardsWorkflowBuilder,
  updateForceEmbossReplacementCardsWorkflowInstance,
  updateForceEmbossReplacementCardsWorkflowInstanceBuilder
} from './async-thunk/ForceEmbossReplacementCardsWorkflow';
import {
  createHaltInterestChargesAndFeesWorkflowInstance,
  createHaltInterestChargesAndFeesWorkflowInstanceBuilder,
  setupHaltInterestChargesAndFeesWorkflowInstance,
  setupHaltInterestChargesAndFeesWorkflowInstanceBuilder,
  updateHaltInterestChargesAndFeesWorkflowInstance,
  updateHaltInterestChargesAndFeesWorkflowInstanceBuilder
} from './async-thunk/HaltInterestChargesAndFees';
import {
  createManageAccountDelinquencyWorkflowInstance,
  createManageAccountDelinquencyWorkflowInstanceBuilder,
  setupManageAccountDelinquencyWorkflow,
  setupManageAccountDelinquencyWorkflowBuilder,
  updateManageAccountDelinquencyWorkflowInstance,
  updateManageAccountDelinquencyWorkflowInstanceBuilder
} from './async-thunk/ManageAccountDelinquencyWorkflow';
import {
  createManageAccountLevelWorkflowInstance,
  createManageAccountLevelWorkflowInstanceBuilder,
  getDecisionList,
  getDecisionListBuilder,
  getStrategiesList,
  getStrategiesListBuilder,
  getTableList,
  getTableListBuilder,
  setupManageAccountLevelWorkflow,
  setupManageAccountLevelWorkflowBuilder,
  updateManageAccountLevelWorkflowInstance,
  updateManageAccountLevelWorkflowInstanceBuilder
} from './async-thunk/ManageAccountLevelWorkflow';
import {
  createManageAccountMemosWorkflowInstance,
  createManageAccountMemosWorkflowInstanceBuilder,
  setupManageAccountMemosWorkflow,
  setupManageAccountMemosWorkflowBuilder,
  updateManageAccountMemosWorkflowInstance,
  updateManageAccountMemosWorkflowInstanceBuilder
} from './async-thunk/ManageAccountMemosWorkflow';
import {
  createManageAccountSerializationWorkflowInstance,
  createManageAccountSerializationWorkflowInstanceBuilder,
  getSPAListWorkflow,
  setupManageAccountSerializationWorkflow,
  setupManageAccountSerializationWorkflowBuilder,
  updateManageAccountSerializationWorkflowInstance,
  updateManageAccountSerializationWorkflowInstanceBuilder
} from './async-thunk/ManageAccountSerializationWorkflow';
import {
  createManageAccountTransferWorkflowInstance,
  createManageAccountTransferWorkflowInstanceBuilder,
  setupManageAccountTransferWorkflow,
  setupManageAccountTransferWorkflowBuilder,
  updateManageAccountTransferWorkflowInstance,
  updateManageAccountTransferWorkflowInstanceBuilder
} from './async-thunk/ManageAccountTransferWorkflow';
import {
  createManageAdjustmentsSystemWorkflowInstance,
  createManageAdjustmentsSystemWorkflowInstanceBuilder,
  exportShellProfile,
  exportShellProfileBuilder,
  getManageAdjustmentsSystemOcsShellProfile,
  getManageAdjustmentsSystemOcsShellProfileBuilder,
  getManageAdjustmentsSystemProfileTransactions,
  getManageAdjustmentsSystemProfileTransactionsBuilder,
  getManageAdjustmentsSystemQueueProfile,
  getManageAdjustmentsSystemQueueProfileBuilder,
  getShellList,
  getShellListBuilder,
  getSupervisorList,
  getSupervisorListBuilder,
  setupManageAdjustmentsSystemWorkflow,
  setupManageAdjustmentsSystemWorkflowBuilder,
  updateManageAdjustmentsSystemWorkflowInstance,
  updateManageAdjustmentsSystemWorkflowInstanceBuilder
} from './async-thunk/ManageAdjustmentsSystemWorkflow';
import {
  createChangeInTermsWorkflowInstance,
  createChangeInTermsWorkflowInstanceBuilder,
  setupChangeInTermsWorkflow,
  setupChangeInTermsWorkflowBuilder,
  updateChangeInTermsWorkflowInstance,
  updateChangeInTermsWorkflowInstanceBuilder
} from './async-thunk/ManageChangeInTermsWorkflow';
import {
  createManageCreditLimitWorkflowInstance,
  createManageCreditLimitWorkflowInstanceBuilder,
  getWorkflowEffectiveDateOptions,
  setupManageCreditLimitWorkflow,
  setupManageCreditLimitWorkflowBuilder,
  updateManageCreditLimitWorkflowInstance,
  updateManageCreditLimitWorkflowInstanceBuilder
} from './async-thunk/ManageCreditLimitWorkflow';
import {
  createIncomeOptionMethodsWorkflowInstance,
  createIncomeOptionMethodsWorkflowInstanceBuilder,
  setupIncomeOptionMethodsWorkflow,
  setupIncomeOptionMethodsWorkflowBuilder,
  updateIncomeOptionMethodsWorkflowInstance,
  updateIncomeOptionMethodsWorkflowInstanceBuilder
} from './async-thunk/ManageIncomeOptionMethodsWorkflow';
import {
  createManageMethodLevelProcessingDecisionTablesMLPWorkflowInstance,
  createManageMethodLevelProcessingDecisionTablesMLPWorkflowInstanceBuilder,
  getDecisionListMLP,
  getDecisionListMLPBuilder,
  getTableListMLP,
  getTableListMLPBuilder,
  setupManageMethodLevelProcessingDecisionTablesMLPWorkflow,
  setupManageMethodLevelProcessingDecisionTablesMLPWorkflowBuilder,
  updateManageMethodLevelProcessingDecisionTablesMLPWorkflowInstance,
  updateManageMethodLevelProcessingDecisionTablesMLPWorkflowInstanceBuilder
} from './async-thunk/ManageMethodLevelProcessingDecisionTablesMLPWorkflow';
import {
  createManageOCSShellsWorkflowInstance,
  createManageOCSShellsWorkflowInstanceBuilder,
  getOCSShellList,
  getOCSShellListBuilder,
  setupManageOCSShellsWorkflow,
  setupManageOCSShellsWorkflowBuilder,
  updateManageOCSShellsWorkflowInstance,
  updateManageOCSShellsWorkflowInstanceBuilder
} from './async-thunk/ManageOCSShellsWorkflow';
import {
  createManagePayoffExceptionMethodsWorkflowInstance,
  createManagePayoffExceptionMethodsWorkflowInstanceBuilder,
  getManagePayoffExceptionMethodsWorkflow,
  getManagePayoffExceptionMethodsWorkflowBuilder,
  setupManagePayoffExceptionMethodsWorkflow,
  setupManagePayoffExceptionMethodsWorkflowBuilder,
  updateManagePayoffExceptionMethodsWorkflowInstance,
  updateManagePayoffExceptionMethodsWorkflowInstanceBuilder
} from './async-thunk/ManagePayoffExceptionMethodsWorkflow';
import {
  createManagePenaltyFeeWorkflowInstance,
  createManagePenaltyFeeWorkflowInstanceBuilder,
  getManagePenaltyFeeWorkflowMethods,
  getManagePenaltyFeeWorkflowMethodsBuilder,
  setupManagePenaltyFeeWorkflow,
  setupManagePenaltyFeeWorkflowBuilder,
  updateManagePenaltyFeeWorkflowInstance,
  updateManagePenaltyFeeWorkflowInstanceBuilder
} from './async-thunk/ManagePenaltyFeeWorkflow';
import {
  createManagePricingStrategiesAndMethodAssignmentsWorkflowInstance,
  createManagePricingStrategiesAndMethodAssignmentsWorkflowInstanceBuilder,
  getPricingStrategyList,
  getPricingStrategyListBuilder,
  getPricingStrategyMethodList,
  getPricingStrategyMethodListBuilder,
  setupManagePricingStrategiesAndMethodAssignmentsWorkflow,
  setupManagePricingStrategiesAndMethodAssignmentsWorkflowBuilder,
  updateManagePricingStrategiesAndMethodAssignmentsWorkflow,
  updateManagePricingStrategiesAndMethodAssignmentsWorkflowBuilder
} from './async-thunk/ManagePricingStrategiesAndMethodAssignmentsWorkflow';
import {
  createManageStatementProductionSettingsWorkflowInstance,
  createManageStatementProductionSettingsWorkflowInstanceBuilder,
  setupManageStatementProductionSettingsWorkflow,
  setupManageStatementProductionSettingsWorkflowBuilder,
  updateManageStatementProductionSettingsWorkflowInstance,
  updateManageStatementProductionSettingsWorkflowInstanceBuilder
} from './async-thunk/ManageStatementProductionSettingsWorkflow';
import {
  createManageTransactionLevelProcessingDecisionTablesTLPWorkflowBuilder,
  createManageTransactionLevelProcessingDecisionTablesTLPWorkflowInstance,
  getDecisionElementList,
  getDecisionElementListBuilder,
  setupManageTransactionLevelProcessingDecisionTablesTLPWorkflowInstance,
  setupsetupManageTransactionLevelProcessingDecisionTablesTLPWorkflowBuilder,
  updateManageTransactionLevelProcessingDecisionTablesTLPWorkflowInstance,
  updateManageTransactionLevelProcessingDecisionTablesTLPWorkflowInstanceBuilder
} from './async-thunk/ManageTransactionLevelProcessingDecisionTablesTLPWorkflow';
import {
  createManualAccountDeletionWorkflowInstance,
  createManualAccountDeletionWorkflowInstanceBuilder,
  setupManualAccountDeletionWorkflow,
  setupManualAccountDeletionWorkflowBuilder,
  updateManualAccountDeletionWorkflowInstance,
  updateManualAccountDeletionWorkflowInstanceBuilder
} from './async-thunk/ManualAccountDeletionWorkflow';
import {
  createPerformMonetaryAdjustmentTransactionsWorkflowInstance,
  createPerformMonetaryAdjustmentTransactionsWorkflowInstanceBuilder,
  setupPerformMonetaryAdjustmentTransactionsWorkflow,
  setupPerformMonetaryAdjustmentTransactionsWorkflowBuilder,
  updatePerformMonetaryAdjustmentTransactionsWorkflowInstance,
  updatePerformMonetaryAdjustmentTransactionsWorkflowInstanceBuilder
} from './async-thunk/PerformMonetaryAdjustmentsWorkflow';
import {
  createPortfolioDefinitionsWorkflowInstance,
  createPortfolioDefinitionsWorkflowInstanceBuilder,
  setupPortfolioDefinitionsWorkflow,
  setupPortfolioDefinitionsWorkflowBuilder,
  updatePortfolioDefinitionsWorkflowInstance,
  updatePortfolioDefinitionsWorkflowInstanceBuilder
} from './async-thunk/PortfolioDefinitionsWorkflow';
import {
  getPortfolioDefinitionsWorkflow,
  getPortfolioDefinitionsWorkflowBuilder
} from './async-thunk/PortfolioDefinitionsWorkflow/getWorkflowMethods';
import {
  createSendLetterToAccountsWorkflowInstance,
  createSendLetterToAccountsWorkflowInstanceBuilder,
  getLetters,
  getLettersBuilder,
  setupSendLetterToAccountsWorkflow,
  setupSendLetterToAccountsWorkflowBuilder,
  updateSendLetterToAccountsWorkflowInstance,
  updateSendLetterToAccountsWorkflowInstanceBuilder
} from './async-thunk/SendLetterToAccountsWorkflow';
import {
  createUpdateIndexRateWorkflowInstance,
  createUpdateIndexRateWorkflowInstanceBuilder,
  setupUpdateIndexRateWorkflow,
  setupUpdateIndexRateWorkflowBuilder,
  updateUpdateIndexRateWorkflowInstance,
  updateUpdateIndexRateWorkflowInstanceBuilder
} from './async-thunk/UpdateIndexRateWorkflow';
import reducers, { defaultState } from './reducers';

const { actions, reducer } = createSlice({
  name: 'workflowSetup',
  initialState: { ...defaultState },
  reducers,
  extraReducers: builder => {
    // commons
    getWorkflowMetadataBuilder(builder);
    getWorkflowChangeSetListBuilder(builder);
    getWorkflowInstanceBuilder(builder);
    createNewChangeBuilder(builder);

    getTableListForWorkflowSetupBuilder(builder);
    getNonWorkflowMetadataBuilder(builder);
    getServiceSubjectSectionOptionsBuilder(builder);

    setupManageCreditLimitWorkflowBuilder(builder);
    createManageCreditLimitWorkflowInstanceBuilder(builder);
    getFileDetailsBuilder(builder);
    updateManageCreditLimitWorkflowInstanceBuilder(builder);

    // Manage Penalty Fee Workflow
    updateManagePenaltyFeeWorkflowInstanceBuilder(builder);
    setupManagePenaltyFeeWorkflowBuilder(builder);
    getManagePenaltyFeeWorkflowMethodsBuilder(builder);
    createManagePenaltyFeeWorkflowInstanceBuilder(builder);

    downloadTemplateFileBuilder(builder);

    // SendLetterToAccountsWorkflow
    createSendLetterToAccountsWorkflowInstanceBuilder(builder);
    updateSendLetterToAccountsWorkflowInstanceBuilder(builder);
    setupSendLetterToAccountsWorkflowBuilder(builder);
    getLettersBuilder(builder);

    // ManagePricingStrategiesAndMethodAssignmentsWorkflow
    createManagePricingStrategiesAndMethodAssignmentsWorkflowInstanceBuilder(
      builder
    );
    setupManagePricingStrategiesAndMethodAssignmentsWorkflowBuilder(builder);
    updateManagePricingStrategiesAndMethodAssignmentsWorkflowBuilder(builder);
    getPricingStrategyListBuilder(builder);
    getPricingStrategyMethodListBuilder(builder);

    // Halt Interest Charges and Fees
    setupHaltInterestChargesAndFeesWorkflowInstanceBuilder(builder);
    createHaltInterestChargesAndFeesWorkflowInstanceBuilder(builder);
    updateHaltInterestChargesAndFeesWorkflowInstanceBuilder(builder);

    // Assign Pricing Strategies
    createAssignPricingStrategiesWorkflowInstanceBuilder(builder);
    setupAssignPricingStrategiesWorkflowBuilder(builder);
    updateAssignPricingStrategiesWorkflowInstanceBuilder(builder);

    // Manage Account Delinquency
    createManageAccountDelinquencyWorkflowInstanceBuilder(builder);
    setupManageAccountDelinquencyWorkflowBuilder(builder);
    updateManageAccountDelinquencyWorkflowInstanceBuilder(builder);

    // Flexible Monetary Transactions
    createFlexibleMonetaryTransactionsWorkflowInstanceBuilder(builder);
    setupFlexibleMonetaryTransactionsWorkflowBuilder(builder);
    updateFlexibleMonetaryTransactionsWorkflowInstanceBuilder(builder);

    // Flexible Non-Monetary Transactions
    createFlexibleNonMonetaryTransactionsWorkflowInstanceBuilder(builder);
    updateFlexibleNonMonetaryTransactionsWorkflowInstanceBuilder(builder);
    setupFlexibleNonMonetaryTransactionsWorkflowBuilder(builder);

    // Force Emboss Replacement Cards
    createForceEmbossReplacementCardsWorkflowInstanceBuilder(builder);
    setupForceEmbossReplacementCardsWorkflowBuilder(builder);
    updateForceEmbossReplacementCardsWorkflowInstanceBuilder(builder);

    // Perform Monetary Adjustment Transaction
    createPerformMonetaryAdjustmentTransactionsWorkflowInstanceBuilder(builder);
    setupPerformMonetaryAdjustmentTransactionsWorkflowBuilder(builder);
    updatePerformMonetaryAdjustmentTransactionsWorkflowInstanceBuilder(builder);

    // Condition M Cycle Accounts For Testing
    createConditionMCycleAccountsForTestingWorkflowInstanceBuilder(builder);
    updateConditionMCycleAccountsForTestingWorkflowInstanceBuilder(builder);
    setupConditionMCycleAccountsForTestingWorkflowBuilder(builder);

    // Export Import Change Details
    createExportImportChangeDetailsWorkflowInstanceBuilder(builder);
    updateExportImportChangeDetailsWorkflowInstanceBuilder(builder);
    setupExportImportChangeDetailsWorkflowBuilder(builder);

    // Design Loan Offers
    createDesignLoanOffersWorkflowInstanceBuilder(builder);
    getDesignLoanOffersWorkflowMethodsBuilder(builder);
    setupDesignLoanOffersWorkflowBuilder(builder);
    updateDesignLoanOffersWorkflowInstanceBuilder(builder);

    // ManageAccountTransferWorkflow
    createManageAccountTransferWorkflowInstanceBuilder(builder);
    setupManageAccountTransferWorkflowBuilder(builder);
    updateManageAccountTransferWorkflowInstanceBuilder(builder);

    // Design Promotions
    createDesignPromotionsWorkflowInstanceBuilder(builder);
    getDesignPromotionsWorkflowMethodsBuilder(builder);
    setupDesignPromotionsWorkflowBuilder(builder);
    updateDesignPromotionsWorkflowInstanceBuilder(builder);

    // ManageAccountSerializationWorkflow
    createManageAccountSerializationWorkflowInstanceBuilder(builder);
    setupManageAccountSerializationWorkflowBuilder(builder);
    updateManageAccountSerializationWorkflowInstanceBuilder(builder);

    // ChangeInTermsWorkflow
    createChangeInTermsWorkflowInstanceBuilder(builder);
    setupChangeInTermsWorkflowBuilder(builder);
    updateChangeInTermsWorkflowInstanceBuilder(builder);

    // IncomeOptionMethodsWorkflow
    createIncomeOptionMethodsWorkflowInstanceBuilder(builder);
    setupIncomeOptionMethodsWorkflowBuilder(builder);
    updateIncomeOptionMethodsWorkflowInstanceBuilder(builder);

    // Configure Processing Merchants Workflow
    createConfigureProcessingMerchantsWorkflowInstanceBuilder(builder);
    setupConfigureProcessingMerchantsWorkflowBuilder(builder);
    updateConfigureProcessingMerchantsWorkflowInstanceBuilder(builder);
    getConfigureProcessingMerchantsListBuilder(builder);
    exportConfigureProcessingMerchantsBuilder(builder);

    // Manage Adjustments System Workflow
    createManageAdjustmentsSystemWorkflowInstanceBuilder(builder);
    setupManageAdjustmentsSystemWorkflowBuilder(builder);
    updateManageAdjustmentsSystemWorkflowInstanceBuilder(builder);
    getManageAdjustmentsSystemQueueProfileBuilder(builder);
    exportShellProfileBuilder(builder);
    getManageAdjustmentsSystemOcsShellProfileBuilder(builder);
    getManageAdjustmentsSystemProfileTransactionsBuilder(builder);
    getShellListBuilder(builder);
    getSupervisorListBuilder(builder);

    // Manage OCS Shells
    createManageOCSShellsWorkflowInstanceBuilder(builder);
    updateManageOCSShellsWorkflowInstanceBuilder(builder);
    setupManageOCSShellsWorkflowBuilder(builder);
    getOCSShellListBuilder(builder);

    // BulkExternalStatusManagement
    createBulkExternalStatusManagementWorkflowInstanceBuilder(builder);
    updateBulkExternalStatusManagementWorkflowInstanceBuilder(builder);
    setupBulkExternalStatusManagementWorkflowBuilder(builder);

    // ManualAccountDeletion
    createManualAccountDeletionWorkflowInstanceBuilder(builder);
    updateManualAccountDeletionWorkflowInstanceBuilder(builder);
    setupManualAccountDeletionWorkflowBuilder(builder);

    // CustomDataLinkElement
    createCustomDataLinkElementsWorkflowInstanceBuilder(builder);
    setupCustomDataLinkElementsWorkflowBuilder(builder);
    updateCustomDataLinkElementsWorkflowInstanceBuilder(builder);

    // BulkSuspendFraudStrategyWorkflow
    createBulkSuspendFraudStrategyWorkflowInstanceBuilder(builder);
    setupBulkSuspendFraudStrategyWorkflowBuilder(builder);
    updateBulkSuspendFraudStrategyWorkflowInstanceBuilder(builder);

    // CycleAccountsTestEnvironmentWorkflow
    createCycleAccountsTestEnvironmentWorkflowInstanceBuilder(builder);
    setupCycleAccountsTestEnvironmentWorkflowBuilder(builder);
    updateCycleAccountsTestEnvironmentWorkflowInstanceBuilder(builder);

    // Manage Method Level Processing Decision Tables MLP
    createManageMethodLevelProcessingDecisionTablesMLPWorkflowInstanceBuilder(
      builder
    );
    setupManageMethodLevelProcessingDecisionTablesMLPWorkflowBuilder(builder);
    updateManageMethodLevelProcessingDecisionTablesMLPWorkflowInstanceBuilder(
      builder
    );
    getDecisionListMLPBuilder(builder);
    getTableListMLPBuilder(builder);

    //AccountMemos
    createManageAccountMemosWorkflowInstanceBuilder(builder);
    setupManageAccountMemosWorkflowBuilder(builder);
    updateManageAccountMemosWorkflowInstanceBuilder(builder);

    //AccountLevel
    createManageAccountLevelWorkflowInstanceBuilder(builder);
    setupManageAccountLevelWorkflowBuilder(builder);
    updateManageAccountLevelWorkflowInstanceBuilder(builder);
    getTableListBuilder(builder);
    getDecisionListBuilder(builder);
    getStrategiesListBuilder(builder);

    // ActionCardCompromiseEvent
    createActionCardCompromiseEventWorkflowInstanceBuilder(builder);
    setupActionCardCompromiseEventWorkflowBuilder(builder);
    updateActionCardCompromiseEventWorkflowInstanceBuilder(builder);

    // UpdateIndexRate
    createUpdateIndexRateWorkflowInstanceBuilder(builder);
    updateUpdateIndexRateWorkflowInstanceBuilder(builder);
    setupUpdateIndexRateWorkflowBuilder(builder);

    // Manage Payoff Exception Methods
    createManagePayoffExceptionMethodsWorkflowInstanceBuilder(builder);
    setupManagePayoffExceptionMethodsWorkflowBuilder(builder);
    updateManagePayoffExceptionMethodsWorkflowInstanceBuilder(builder);
    getManagePayoffExceptionMethodsWorkflowBuilder(builder);

    // Change Interest Rates
    setupChangeInterestRatesWorkflowBuilder(builder);
    createChangeInterestRatesWorkflowInstanceBuilder(builder);
    updateChangeInterestRatesWorkflowInstanceBuilder(builder);
    getChangeInterestRatesWorkflowMethodsBuilder(builder);

    // Manage Transaction Level Processing Decision Tables - TLP
    createManageTransactionLevelProcessingDecisionTablesTLPWorkflowBuilder(
      builder
    );
    setupsetupManageTransactionLevelProcessingDecisionTablesTLPWorkflowBuilder(
      builder
    );
    updateManageTransactionLevelProcessingDecisionTablesTLPWorkflowInstanceBuilder(
      builder
    );
    getDecisionElementListBuilder(builder);
    // Bulk Archive PCF
    createBulkArchivePCFWorkflowInstanceBuilder(builder);
    setupBulkArchivePCFWorkflowInstanceBuilder(builder);
    updateBulkArchivePCFWorkflowInstanceBuilder(builder);
    getBulkArchivePCFWorkflowStrategiesBuilder(builder);
    getBulkArchivePCFWorkflowTextAreasBuilder(builder);
    getBulkArchivePCFWorkflowTableAreasBuilder(builder);

    //Manage Statement Production Settings
    createManageStatementProductionSettingsWorkflowInstanceBuilder(builder);
    setupManageStatementProductionSettingsWorkflowBuilder(builder);
    updateManageStatementProductionSettingsWorkflowInstanceBuilder(builder);

    // Portfolio Definitions
    setupPortfolioDefinitionsWorkflowBuilder(builder);
    updatePortfolioDefinitionsWorkflowInstanceBuilder(builder);
    createPortfolioDefinitionsWorkflowInstanceBuilder(builder);
    getPortfolioDefinitionsWorkflowBuilder(builder);

    // Export Analyzer
    exportAnalyzerBuilder(builder);
  }
});

const extraActions = {
  ...actions,
  // commons
  getWorkflowMetadata,
  getWorkflowChangeSetList,
  getWorkflowInstance,
  createNewChange,
  getFileDetails,
  getWorkflowTransactionList,

  getWorkflowEffectiveDateOptions,
  downloadTemplateFile,
  deleteTemplateFile,
  updateManageCreditLimitWorkflowInstance,

  //get Non-Workflow
  exportAnalyzer,
  getTableListForWorkflowSetup,
  getNonWorkflowMetadata,
  getServiceSubjectSectionOptions,

  // ManageAccountTransferWorkflow
  createManageAccountTransferWorkflowInstance,
  setupManageAccountTransferWorkflow,
  updateManageAccountTransferWorkflowInstance,

  // ManageAccountSerializationWorkflow
  createManageAccountSerializationWorkflowInstance,
  setupManageAccountSerializationWorkflow,
  updateManageAccountSerializationWorkflowInstance,
  getSPAListWorkflow,

  // ManageCreditLimitWorkflow
  createManageCreditLimitWorkflowInstance,
  setupManageCreditLimitWorkflow,

  // Manage Penalty Fee Workflow
  getManagePenaltyFeeWorkflowMethods,
  createManagePenaltyFeeWorkflowInstance,
  updateManagePenaltyFeeWorkflowInstance,
  setupManagePenaltyFeeWorkflow,

  // SendLetterToAccountsWorkflow
  createSendLetterToAccountsWorkflowInstance,
  updateSendLetterToAccountsWorkflowInstance,
  setupSendLetterToAccountsWorkflow,
  getLetters,

  // ManagePricingStrategiesAndMethodAssignmentsWorkflow
  createManagePricingStrategiesAndMethodAssignmentsWorkflowInstance,
  setupManagePricingStrategiesAndMethodAssignmentsWorkflow,
  updateManagePricingStrategiesAndMethodAssignmentsWorkflow,
  getPricingStrategyList,
  getPricingStrategyMethodList,

  // Halt Interest Charges And Fees
  createHaltInterestChargesAndFeesWorkflowInstance,
  setupHaltInterestChargesAndFeesWorkflowInstance,
  updateHaltInterestChargesAndFeesWorkflowInstance,

  // Assign Pricing Strategies
  createAssignPricingStrategiesWorkflowInstance,
  setupAssignPricingStrategiesWorkflow,
  updateAssignPricingStrategiesWorkflowInstance,

  // Manage Account Delinquency
  createManageAccountDelinquencyWorkflowInstance,
  setupManageAccountDelinquencyWorkflow,
  updateManageAccountDelinquencyWorkflowInstance,

  // Flexible Monetary Transactions
  createFlexibleMonetaryTransactionsWorkflowInstance,
  setupFlexibleMonetaryTransactionsWorkflow,
  updateFlexibleMonetaryTransactionsWorkflowInstance,

  // Flexible Non-Monetary Transactions
  createFlexibleNonMonetaryTransactionsWorkflowInstance,
  updateFlexibleNonMonetaryTransactionsWorkflowInstance,
  setupFlexibleNonMonetaryTransactionsWorkflow,

  // Perform Monetary Adjustment Workflow
  createPerformMonetaryAdjustmentTransactionsWorkflowInstance,
  setupPerformMonetaryAdjustmentTransactionsWorkflow,
  updatePerformMonetaryAdjustmentTransactionsWorkflowInstance,

  // Force Emboss Replacement Cards
  createForceEmbossReplacementCardsWorkflowInstance,
  setupForceEmbossReplacementCardsWorkflow,
  updateForceEmbossReplacementCardsWorkflowInstance,

  //Condition M Cycle Accounts For Testing
  createConditionMCycleAccountsForTestingWorkflowInstance,
  setupConditionMCycleAccountsForTestingWorkflow,
  updateConditionMCycleAccountsForTestingWorkflowInstance,
  getWorkflowTransactionListCondition,

  //Export Import Change Details
  createExportImportChangeDetailsWorkflowInstance,
  setupExportImportChangeDetailsWorkflowInstance,
  updateExportImportChangeDetailsWorkflowInstance,
  getWorkflowChangeList,

  // Design Loan Offers
  createDesignLoanOffersWorkflowInstance,
  getDesignLoanOffersWorkflowMethods,
  setupDesignLoanOffersWorkflow,
  updateDesignLoanOffersWorkflowInstance,

  // Design Promotions
  createDesignPromotionsWorkflowInstance,
  getDesignPromotionsWorkflowMethods,
  setupDesignPromotionsWorkflow,
  updateDesignPromotionsWorkflowInstance,

  // ChangeInTermsWorkflow
  createChangeInTermsWorkflowInstance,
  setupChangeInTermsWorkflow,
  updateChangeInTermsWorkflowInstance,

  // IncomeOptionMethodsWorkflow
  createIncomeOptionMethodsWorkflowInstance,
  setupIncomeOptionMethodsWorkflow,
  updateIncomeOptionMethodsWorkflowInstance,

  // Configure Processing Merchants Workflow
  createConfigureProcessingMerchantsWorkflowInstance,
  setupConfigureProcessingMerchantsWorkflow,
  updateConfigureProcessingMerchantsWorkflowInstance,
  getConfigureProcessingMerchantsList,
  exportConfigureProcessingMerchants,

  // Manage Adjustments System Workflow
  createManageAdjustmentsSystemWorkflowInstance,
  setupManageAdjustmentsSystemWorkflow,
  updateManageAdjustmentsSystemWorkflowInstance,
  getManageAdjustmentsSystemQueueProfile,
  getManageAdjustmentsSystemProfileTransactions,
  getManageAdjustmentsSystemOcsShellProfile,
  exportShellProfile,
  getShellList,
  getSupervisorList,

  // Manage OCS Shells
  getOCSShellList,
  createManageOCSShellsWorkflowInstance,
  updateManageOCSShellsWorkflowInstance,
  setupManageOCSShellsWorkflow,

  // Manage Method Level Processing Decision Tables MLP
  createManageMethodLevelProcessingDecisionTablesMLPWorkflowInstance,
  setupManageMethodLevelProcessingDecisionTablesMLPWorkflow,
  updateManageMethodLevelProcessingDecisionTablesMLPWorkflowInstance,
  getDecisionListMLP,
  getTableListMLP,

  // CustomDataLinkElements
  createCustomDataLinkElementsWorkflowInstance,
  setupCustomDataLinkElementsWorkflow,
  updateCustomDataLinkElementsWorkflowInstance,

  // BulkSuspendFraudStrategyWorkflow
  createBulkSuspendFraudStrategyWorkflowInstance,
  setupBulkSuspendFraudStrategyWorkflow,
  updateBulkSuspendFraudStrategyWorkflowInstance,

  // CycleAccountsTestEnvironmentWorkflow
  createCycleAccountsTestEnvironmentWorkflowInstance,
  setupCycleAccountsTestEnvironmentWorkflow,
  updateCycleAccountsTestEnvironmentWorkflowInstance,

  // BulkExternalStatusManagement
  createBulkExternalStatusManagementWorkflowInstance,
  updateBulkExternalStatusManagementWorkflowInstance,
  setupBulkExternalStatusManagementWorkflow,

  //AccountMemos
  createManageAccountMemosWorkflowInstance,
  setupManageAccountMemosWorkflow,
  updateManageAccountMemosWorkflowInstance,

  //AccountLevel
  createManageAccountLevelWorkflowInstance,
  setupManageAccountLevelWorkflow,
  updateManageAccountLevelWorkflowInstance,
  getTableList,
  getDecisionList,
  getStrategiesList,
  // ManualAccountDeletion
  createManualAccountDeletionWorkflowInstance,
  updateManualAccountDeletionWorkflowInstance,
  setupManualAccountDeletionWorkflow,

  // ActionCardCompromiseEvent
  createActionCardCompromiseEventWorkflowInstance,
  setupActionCardCompromiseEventWorkflow,
  updateActionCardCompromiseEventWorkflowInstance,

  // UpdateIndexRate
  createUpdateIndexRateWorkflowInstance,
  updateUpdateIndexRateWorkflowInstance,
  setupUpdateIndexRateWorkflow,

  // Manage Payoff Exception Methods
  createManagePayoffExceptionMethodsWorkflowInstance,
  updateManagePayoffExceptionMethodsWorkflowInstance,
  setupManagePayoffExceptionMethodsWorkflow,
  getManagePayoffExceptionMethodsWorkflow,

  // Change Interest Rates
  createChangeInterestRatesWorkflowInstance,
  setupChangeInterestRatesWorkflow,
  updateChangeInterestRatesWorkflowInstance,
  getChangeInterestRatesWorkflowMethods,

  // Manage Transaction Level Processing Decision Tables - TLP
  createManageTransactionLevelProcessingDecisionTablesTLPWorkflowInstance,
  setupManageTransactionLevelProcessingDecisionTablesTLPWorkflowInstance,
  updateManageTransactionLevelProcessingDecisionTablesTLPWorkflowInstance,
  getDecisionElementList,

  // Bulk Archive PCF
  createBulkArchivePCFWorkflowInstance,
  setupBulkArchivePCFWorkflowInstance,
  updateBulkArchivePCFWorkflowInstance,
  getBulkArchivePCFWorkflowStrategies,
  getBulkArchivePCFWorkflowTextAreas,
  getBulkArchivePCFWorkflowTableAreas,

  //Manage Statement Production Settings
  createManageStatementProductionSettingsWorkflowInstance,
  setupManageStatementProductionSettingsWorkflow,
  updateManageStatementProductionSettingsWorkflowInstance,

  //Portfolio Definitions
  createPortfolioDefinitionsWorkflowInstance,
  updatePortfolioDefinitionsWorkflowInstance,
  setupPortfolioDefinitionsWorkflow,
  getPortfolioDefinitionsWorkflow
};

export * from './select-hooks';
export { extraActions as actionsWorkflowSetup, reducer };
