import { renderHook } from '@testing-library/react-hooks';
import { WorkflowMetadataAll } from 'app/fixtures/workflow-metadata';
import { WorkflowSetupRootState } from 'app/types';
import * as reactRedux from 'react-redux';
import {
  useSelectDecisionElementListData,
  useSelectElementMetadataManageTransactionTLP
} from './workflowManageTransactionLevelProcessingDecisionTLP';

describe('redux-store > workflow-setup > select-hooks ', () => {
  let selectorMock: jest.SpyInstance;
  const mockSelectorWorkflowSetup = (
    workflowSetup:
      | Pick<WorkflowSetupRootState, 'workflowFormSteps'>
      | Pick<WorkflowSetupRootState, 'workflowSetup'>
      | Pick<WorkflowSetupRootState, 'workflowMethods'>
      | Pick<WorkflowSetupRootState, 'workflowInstance'>
      | Pick<WorkflowSetupRootState, 'elementMetadata'>
      | Pick<WorkflowSetupRootState, 'workflowChangeSets'>
      | Pick<WorkflowSetupRootState, 'createNewChange'>
      | Pick<WorkflowSetupRootState, 'parameterData'>
  ) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector => selector({ workflowSetup }));
  };

  afterEach(() => {
    selectorMock.mockClear();
  });

  it('test on selector useSelectDecisionElementListData ', () => {
    mockSelectorWorkflowSetup({
      decisionElementList: {
        loading: false,
        error: false
      }
    } as any);

    const { rerender, result } = renderHook(() =>
      useSelectDecisionElementListData('ALPAQ')
    );

    mockSelectorWorkflowSetup({
      decisionElementList: {
        loading: false,
        error: false
      }
    } as any);
    rerender();
    expect(result.current).toEqual(undefined);
    expect(result.current).toEqual(undefined);
  });

  it('test on selector useSelectElementMetadataManageTransactionTLP ', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: { elements: WorkflowMetadataAll }
    } as any);

    const { rerender, result } = renderHook(() =>
      useSelectElementMetadataManageTransactionTLP()
    );

    mockSelectorWorkflowSetup({
      elementMetadata: { elements: WorkflowMetadataAll }
    } as any);
    rerender();
    expect(result.current.searchCode.length).toEqual(2);
  });

  it('test on selector useSelectElementMetadataManageTransactionTLP with empty data ', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: { elements: [] }
    } as any);

    const { rerender, result } = renderHook(() =>
      useSelectElementMetadataManageTransactionTLP()
    );

    mockSelectorWorkflowSetup({
      elementMetadata: { elements: [] }
    } as any);
    rerender();
    expect(result.current).toEqual({
      includeActivityOptions: [],
      posPromoValidationOptions: [],
      searchCode: [],
      tlpTableConfigOptions: []
    });
  });
});
