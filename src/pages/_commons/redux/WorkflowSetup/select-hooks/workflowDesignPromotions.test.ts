import { renderHook } from '@testing-library/react-hooks';
import { WorkflowMetadataAll } from 'app/fixtures/workflow-metadata';
import { WorkflowSetupRootState } from 'app/types';
import * as reactRedux from 'react-redux';
import { useSelectElementMetadataForPW } from '.';

describe('redux-store > workflow-setup > select-hooks ', () => {
  let selectorMock: jest.SpyInstance;
  const mockSelectorWorkflowSetup = (
    workflowSetup:
      | Pick<WorkflowSetupRootState, 'workflowFormSteps'>
      | Pick<WorkflowSetupRootState, 'workflowSetup'>
      | Pick<WorkflowSetupRootState, 'workflowMethods'>
      | Pick<WorkflowSetupRootState, 'workflowInstance'>
      | Pick<WorkflowSetupRootState, 'elementMetadata'>
      | Pick<WorkflowSetupRootState, 'workflowChangeSets'>
      | Pick<WorkflowSetupRootState, 'createNewChange'>
      | Pick<WorkflowSetupRootState, 'parameterData'>
  ) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector => selector({ workflowSetup }));
  };

  afterEach(() => {
    selectorMock.mockClear();
  });

  it('test on selector useSelectElementMetadataForPW ', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: {
        loading: true,
        error: false,
        elements: [...WorkflowMetadataAll] as Omit<
          ElementMetaData,
          'workflowTemplateId'
        >[]
      }
    });

    const { result } = renderHook(() => useSelectElementMetadataForPW());
    const expectResult = {
      designPromotionBasicInformation3:
        result.current.designPromotionBasicInformation3.length,
      designPromotionBasicInformation4:
        result.current.designPromotionBasicInformation4.length,
      designPromotionBasicInformation5:
        result.current.designPromotionBasicInformation5.length,
      designPromotionBasicInformation6:
        result.current.designPromotionBasicInformation6.length,
      designPromotionBasicInformation7:
        result.current.designPromotionBasicInformation7.length,
      designPromotionBasicInformation8:
        result.current.designPromotionBasicInformation8.length,
      designPromotionBasicInformation9:
        result.current.designPromotionBasicInformation9.length,
      designPromotionBasicInformation10:
        result.current.designPromotionBasicInformation10.length,
      designPromotionBasicInformation11:
        result.current.designPromotionBasicInformation11.length,
      designPromotionBasicInformation12:
        result.current.designPromotionBasicInformation12.length,
      designPromotionInterestAssessment9:
        result.current.designPromotionInterestAssessment9.length,
      designPromotionInterestAssessment10:
        result.current.designPromotionInterestAssessment10.length,
      designPromotionInterestAssessment11:
        result.current.designPromotionInterestAssessment11.length,
      designPromotionInterestAssessment12:
        result.current.designPromotionInterestAssessment12.length,
      designPromotionInterestAssessment13:
        result.current.designPromotionInterestAssessment13.length,
      designPromotionInterestAssessment14:
        result.current.designPromotionInterestAssessment14.length,
      designPromotionInterestAssessment15:
        result.current.designPromotionInterestAssessment15.length,
      designPromotionInterestAssessment16:
        result.current.designPromotionInterestAssessment16.length,
      designPromotionInterestAssessment17:
        result.current.designPromotionInterestAssessment17.length,
      designPromotionInterestOverrides1:
        result.current.designPromotionInterestOverrides1.length,
      designPromotionInterestOverrides2:
        result.current.designPromotionInterestOverrides2.length,
      designPromotionInterestOverrides3:
        result.current.designPromotionInterestOverrides3.length,
      designPromotionInterestOverrides4:
        result.current.designPromotionInterestOverrides4.length,
      designPromotionInterestOverrides5:
        result.current.designPromotionInterestOverrides5.length,
      designPromotionInterestOverrides6:
        result.current.designPromotionInterestOverrides6.length,
      designPromotionInterestOverrides7:
        result.current.designPromotionInterestOverrides7.length,
      designPromotionInterestOverrides8:
        result.current.designPromotionInterestOverrides8.length,
      designPromotionRetailPath1:
        result.current.designPromotionRetailPath1.length,
      designPromotionRetailPath4:
        result.current.designPromotionRetailPath4.length,
      designPromotionReturnRevolve2:
        result.current.designPromotionReturnRevolve2.length,
      designPromotionReturnRevolve3:
        result.current.designPromotionReturnRevolve3.length,
      designPromotionReturnRevolve4:
        result.current.designPromotionReturnRevolve4.length,
      designPromotionReturnRevolve6:
        result.current.designPromotionReturnRevolve6.length,
      designPromotionStatementMessaging1:
        result.current.designPromotionStatementMessaging1.length,
      designPromotionStatementMessaging2:
        result.current.designPromotionStatementMessaging2.length,
      designPromotionStatementMessaging3:
        result.current.designPromotionStatementMessaging3.length,
      designPromotionStatementMessaging4:
        result.current.designPromotionStatementMessaging4.length,
      designPromotionStatementMessaging5:
        result.current.designPromotionStatementMessaging5.length
    };

    expect(expectResult).toEqual({
      designPromotionBasicInformation10: 3,
      designPromotionBasicInformation11: 3,
      designPromotionBasicInformation12: 3,
      designPromotionBasicInformation3: 5,
      designPromotionBasicInformation4: 3,
      designPromotionBasicInformation5: 0,
      designPromotionBasicInformation6: 3,
      designPromotionBasicInformation7: 12,
      designPromotionBasicInformation8: 3,
      designPromotionBasicInformation9: 3,
      designPromotionInterestAssessment10: 3,
      designPromotionInterestAssessment11: 3,
      designPromotionInterestAssessment12: 3,
      designPromotionInterestAssessment13: 3,
      designPromotionInterestAssessment14: 3,
      designPromotionInterestAssessment15: 3,
      designPromotionInterestAssessment16: 3,
      designPromotionInterestAssessment17: 4,
      designPromotionInterestAssessment9: 4,
      designPromotionInterestOverrides1: 4,
      designPromotionInterestOverrides2: 4,
      designPromotionInterestOverrides3: 4,
      designPromotionInterestOverrides4: 3,
      designPromotionInterestOverrides5: 3,
      designPromotionInterestOverrides6: 3,
      designPromotionInterestOverrides7: 3,
      designPromotionInterestOverrides8: 4,
      designPromotionRetailPath1: 3,
      designPromotionRetailPath4: 4,
      designPromotionReturnRevolve2: 3,
      designPromotionReturnRevolve3: 3,
      designPromotionReturnRevolve4: 3,
      designPromotionReturnRevolve6: 3,
      designPromotionStatementMessaging1: 4,
      designPromotionStatementMessaging2: 4,
      designPromotionStatementMessaging3: 3,
      designPromotionStatementMessaging4: 3,
      designPromotionStatementMessaging5: 4
    });
  });
  it('test on selector useSelectElementMetadataForPW with elements is empty', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: {
        loading: true,
        error: false,
        elements: [] as Omit<ElementMetaData, 'workflowTemplateId'>[]
      }
    });

    renderHook(() => useSelectElementMetadataForPW());
  });
});
