import { renderHook } from '@testing-library/react-hooks';
import { WorkflowSetupRootState } from 'app/types';
import * as reactRedux from 'react-redux';
import {
  useSelectElementMetadataForPortfolioDefinitionsSelector,
  useSelectExpandingPayoffExceptionMethod,
  useSelectWorkflowPayoffExceptionMethodsFilterSelector
} from './workflowPortfolioDefinitions';

const elements = [
  {
    id: '341',
    name: 'sys.operators',
    options: [
      {
        value: 'equal',
        description: 'Equal',
        workFlow: 'mam'
      },
      {
        value: 'notEqual',
        description: 'Not Equal',
        workFlow: 'mam'
      },
      {
        value: 'equal',
        description: 'Equal',
        workFlow: 'cpd'
      }
    ]
  },
  {
    id: '191',
    name: 'system.principal.agents',
    options: [
      {
        value: '0000-0200-3456',
        description: 'system principle agent numbers'
      },
      {
        value: '0000-0200-0001',
        description: 'system principle agent numbers'
      },
      {
        value: '0000-0200-0002',
        description: 'system principle agent numbers'
      },
      {
        value: '0000-0220-3451',
        description: 'system principle agent numbers'
      },
      {
        value: '0000-0220-0002',
        description: 'system principle agent numbers'
      },
      {
        value: '0000-0220-0003',
        description: 'system principle agent numbers'
      },
      {
        value: '0000-0230-3221',
        description: 'system principle agent numbers'
      },
      {
        value: '0000-0230-0008',
        description: 'system principle agent numbers'
      },
      {
        value: '0000-0230-0004',
        description: 'system principle agent numbers'
      },
      {
        value: '0010-0300-0003',
        description: 'system principle agent numbers'
      },
      {
        value: '0010-0300-0004',
        description: 'system principle agent numbers'
      },
      {
        value: '0010-0300-0005',
        description: 'system principle agent numbers'
      },
      {
        value: '0010-0330-0003',
        description: 'system principle agent numbers'
      },
      {
        value: '0010-0330-0004',
        description: 'system principle agent numbers'
      },
      {
        value: '0010-0330-0005',
        description: 'system principle agent numbers'
      },
      {
        value: '0010-0304-0003',
        description: 'system principle agent numbers'
      },
      {
        value: '0010-0304-0004',
        description: 'system principle agent numbers'
      },
      {
        value: '0010-0304-0005',
        description: 'system principle agent numbers'
      },
      {
        value: '0020-0400-0006',
        description: 'system principle agent numbers'
      },
      {
        value: '0020-0400-0007',
        description: 'system principle agent numbers'
      },
      {
        value: '0020-0400-0008',
        description: 'system principle agent numbers'
      },
      {
        value: '0020-1400-0006',
        description: 'system principle agent numbers'
      },
      {
        value: '0020-1400-0007',
        description: 'system principle agent numbers'
      },
      {
        value: '0020-1400-0008',
        description: 'system principle agent numbers'
      },
      {
        value: '0020-2400-0006',
        description: 'system principle agent numbers'
      },
      {
        value: '0020-2400-0007',
        description: 'system principle agent numbers'
      },
      {
        value: '0020-2400-0008',
        description: 'system principle agent numbers'
      }
    ]
  },
  ,
  {
    id: '331',
    name: 'filter.by',
    options: [
      {
        value: 'Sys',
        description: ''
      },
      {
        value: 'Prin',
        description: ''
      },
      {
        value: 'Agent',
        description: ''
      },
      {
        value: 'BIN',
        description: ''
      },
      {
        value: 'Company ID',
        description: ''
      },
      {
        value: 'Company Name',
        description: ''
      },
      {
        value: 'Datalink Elements',
        description: ''
      },
      {
        value: 'Portfolio ID',
        description: ''
      },
      {
        value: 'Pricing Strategy',
        description: ''
      },
      {
        value: 'Product Type Code',
        description: ''
      },
      {
        value: 'State Code',
        description: ''
      },
      {
        value: 'Miscellaneous Fields 1-10-TX',
        description: ''
      },
      {
        value: 'Miscellaneous Fields 11-15-TX',
        description: ''
      },
      {
        value: 'Miscellaneous Fields 16-20-TX',
        description: ''
      },
      {
        value: 'Miscellaneous Fields 21-25-TX',
        description: ''
      },
      {
        value: 'Miscellaneous Fields 26-TX',
        description: ''
      },
      {
        value: 'Miscellaneous Fields 27-TX',
        description: ''
      },
      {
        value: 'Miscellaneous Fields 28-TX',
        description: ''
      },
      {
        value: 'Miscellaneous Fields 29-TX',
        description: ''
      },
      {
        value: 'Miscellaneous Fields 30-TX',
        description: ''
      },
      {
        value: 'Miscellaneous Fields 31-TX',
        description: ''
      },
      {
        value: 'Miscellaneous Fields 32-TX',
        description: ''
      },
      {
        value: 'Miscellaneous Fields 33-TX',
        description: ''
      },
      {
        value: 'Miscellaneous Fields 34-TX',
        description: ''
      },
      {
        value: 'Miscellaneous Fields 35-TX',
        description: ''
      },
      {
        value: 'Miscellaneous Fields 36-TX',
        description: ''
      },
      {
        value: 'Miscellaneous Fields 37-TX',
        description: ''
      },
      {
        value: 'Miscellaneous Fields 38-TX',
        description: ''
      },
      {
        value: 'Miscellaneous Fields 39-TX',
        description: ''
      },
      {
        value: 'Miscellaneous Fields 40-TX',
        description: ''
      },
      {
        value: 'Miscellaneous Fields 41-TX',
        description: ''
      },
      {
        value: 'Miscellaneous Fields 42-TX',
        description: ''
      },
      {
        value: 'Miscellaneous Fields 43-TX',
        description: ''
      },
      {
        value: 'Miscellaneous Fields 44-TX',
        description: ''
      },
      {
        value: 'Miscellaneous Fields 45-TX',
        description: ''
      },
      {
        value: 'Miscellaneous Fields 46-TX',
        description: ''
      },
      {
        value: 'Miscellaneous Fields 47-TX',
        description: ''
      },
      {
        value: 'Miscellaneous Fields 48-TX',
        description: ''
      },
      {
        value: 'Miscellaneous Fields 49-TX',
        description: ''
      },
      {
        value: 'Miscellaneous Fields 50-TX',
        description: ''
      }
    ]
  }
];

describe('redux-store > workflow-setup > select-hooks ', () => {
  let selectorMock: jest.SpyInstance;
  const mockSelectorWorkflowSetup = (
    workflowSetup:
      | Pick<WorkflowSetupRootState, 'workflowFormSteps'>
      | Pick<WorkflowSetupRootState, 'workflowSetup'>
      | Pick<WorkflowSetupRootState, 'workflowMethods'>
      | Pick<WorkflowSetupRootState, 'workflowInstance'>
      | Pick<WorkflowSetupRootState, 'elementMetadata'>
      | Pick<WorkflowSetupRootState, 'workflowChangeSets'>
      | Pick<WorkflowSetupRootState, 'createNewChange'>
      | Pick<WorkflowSetupRootState, 'parameterData'>
  ) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector => selector({ workflowSetup }));
  };

  afterEach(() => {
    selectorMock.mockClear();
  });

  it('test on selector useSelectElementMetadataForPortfolioDefinitionsSelector', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: {
        elements: elements
      },
      workflowStrategies: {
        strategies: [{ strategyName: '1' }, { strategyName: '2' }]
      }
    } as any);

    const { rerender, result } = renderHook(() =>
      useSelectElementMetadataForPortfolioDefinitionsSelector()
    );

    mockSelectorWorkflowSetup({
      elementMetadata: {
        elements: elements
      },
      workflowStrategies: {
        strategies: [{ strategyName: '1' }, { strategyName: '2' }]
      }
    } as any);
    rerender();
    expect(result.current.agent.length).toEqual(11);
  });

  it('test on selector useSelectWorkflowPayoffExceptionMethodsFilterSelector', () => {
    mockSelectorWorkflowSetup({
      workflowMethods: {
        dataFilter: {
          filterValue: 'transaction.id'
        }
      }
    } as any);

    const { rerender, result } = renderHook(() =>
      useSelectWorkflowPayoffExceptionMethodsFilterSelector()
    );

    mockSelectorWorkflowSetup({
      workflowMethods: {
        dataFilter: {
          filterValue: 'transaction.id'
        }
      }
    } as any);
    rerender();
    expect(result.current.filterValue).toEqual('transaction.id');
  });

  it('test on selector useSelectExpandingPayoffExceptionMethod', () => {
    mockSelectorWorkflowSetup({
      workflowMethods: {
        expandingMethod: {
          payment: 'transaction.id'
        }
      }
    } as any);

    const { rerender, result } = renderHook(() =>
      useSelectExpandingPayoffExceptionMethod()
    );

    mockSelectorWorkflowSetup({
      workflowMethods: {
        expandingMethod: {
          payment: 'transaction.id'
        }
      }
    } as any);
    rerender();
    expect(result.current.payment).toEqual('transaction.id');
  });
});
