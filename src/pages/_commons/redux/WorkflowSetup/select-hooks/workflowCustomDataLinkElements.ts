import { createSelector } from '@reduxjs/toolkit';
import { ElementsFieldParameterEnum } from 'app/constants/enums';
import { orderBy } from 'app/_libraries/_dls/lodash';
import { useSelector } from 'react-redux';

const elementMetadataForCustomDataLinkElementsSelector = createSelector(
  (state: RootState) => state.workflowSetup!.elementMetadata.elements,
  data => {
    const elements = data.map(e => {
      return {
        name: e.name,
        options: orderBy(
          e.options?.map(opt => {
            const des = opt.description ? ` - ${opt.description}` : '';
            const text = opt.value ? `${opt.value}${des}` : opt.description;

            return {
              code: opt.value,
              text: text,
              orderText: opt.value ? text : '0',
              selected: opt.selected
            };
          }),
          ['orderText'],
          ['asc']
        )
      } as {
        name: string;
        options: RefData[];
      };
    });
    const elementId =
      elements.find(e => e.name === ElementsFieldParameterEnum.ElementID)
        ?.options || [];

    const elementName =
      elements.find(e => e.name === ElementsFieldParameterEnum.ElementName)
        ?.options || [];

    const topName =
      elements.find(e => e.name === ElementsFieldParameterEnum.TopName)
        ?.options || [];

    const bottomName =
      elements.find(e => e.name === ElementsFieldParameterEnum.BottomName)
        ?.options || [];

    const reallocation =
      elements.find(e => e.name === ElementsFieldParameterEnum.Reallocation)
        ?.options || [];

    const deltaType =
      elements.find(e => e.name === ElementsFieldParameterEnum.DeltaType)
        ?.options || [];

    const transferTypes =
      elements.find(e => e.name === ElementsFieldParameterEnum.TransferTypes)
        ?.options || [];

    const elementType =
      elements.find(e => e.name === ElementsFieldParameterEnum.ElementType)
        ?.options || [];

    const elementLength =
      elements.find(e => e.name === ElementsFieldParameterEnum.ElementLength)
        ?.options || [];

    const numberOfDecimals =
      elements.find(e => e.name === ElementsFieldParameterEnum.NumberOfDecimals)
        ?.options || [];

    const minValue =
      elements.find(e => e.name === ElementsFieldParameterEnum.MinValue)
        ?.options || [];
    const maxValue =
      elements.find(e => e.name === ElementsFieldParameterEnum.MaxValue)
        ?.options || [];

    const validValues =
      elements.find(e => e.name === ElementsFieldParameterEnum.ValidValues)
        ?.options || [];

    const defaultValue =
      elements.find(e => e.name === ElementsFieldParameterEnum.DefaultValue)
        ?.options || [];

    const letterFormat =
      elements.find(e => e.name === ElementsFieldParameterEnum.LetterFormat)
        ?.options || [];

    const useDefaultOnLetter =
      elements.find(
        e => e.name === ElementsFieldParameterEnum.UseDefaultOnLetter
      )?.options || [];

    const ocsTran =
      elements.find(e => e.name === ElementsFieldParameterEnum.OCSTran)
        ?.options || [];

    const maskText =
      elements.find(e => e.name === ElementsFieldParameterEnum.MaskText)
        ?.options || [];

    const realTimeUpdate =
      elements.find(e => e.name === ElementsFieldParameterEnum.RealTimeUpdate)
        ?.options || [];

    const retainOnTransfer =
      elements.find(e => e.name === ElementsFieldParameterEnum.RetainOnTransfer)
        ?.options || [];

    const elementBusinessDescription =
      elements.find(
        e => e.name === ElementsFieldParameterEnum.ElementBusinessDescription
      )?.options || [];

    return {
      elementId,
      elementName,
      topName,
      bottomName,
      reallocation,
      deltaType,
      transferTypes,
      elementType,
      elementLength,
      numberOfDecimals,
      minValue,
      maxValue,
      validValues,
      defaultValue,
      letterFormat,
      useDefaultOnLetter,
      ocsTran,
      maskText,
      realTimeUpdate,
      retainOnTransfer,
      elementBusinessDescription
    };
  }
);

export const useSelectMetadataForCustomDataLinkElements = () => {
  return useSelector<
    RootState,
    {
      elementId: RefData[];
      elementName: RefData[];
      topName: RefData[];
      bottomName: RefData[];
      reallocation: RefData[];
      deltaType: RefData[];
      transferTypes: RefData[];
      elementType: RefData[];
      elementLength: RefData[];
      numberOfDecimals: RefData[];
      minValue: RefData[];
      maxValue: RefData[];
      validValues: RefData[];
      defaultValue: RefData[];
      letterFormat: RefData[];
      useDefaultOnLetter: RefData[];
      ocsTran: RefData[];
      maskText: RefData[];
      realTimeUpdate: RefData[];
      retainOnTransfer: RefData[];
      elementBusinessDescription: RefData[];
    }
  >(elementMetadataForCustomDataLinkElementsSelector);
};
