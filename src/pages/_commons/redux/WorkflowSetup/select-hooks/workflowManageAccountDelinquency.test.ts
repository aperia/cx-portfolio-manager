import { renderHook } from '@testing-library/react-hooks';
import { WorkflowSetupRootState } from 'app/types';
import * as reactRedux from 'react-redux';
import { useSelectElementMetadataForManageAccountDelinquency } from '.';
import { useSelectParameterData } from './workflowManageAccountDelinquency';

describe('redux-store > workflow-setup > select-hooks ', () => {
  let selectorMock: jest.SpyInstance;
  const mockSelectorWorkflowSetup = (
    workflowSetup:
      | Pick<WorkflowSetupRootState, 'workflowFormSteps'>
      | Pick<WorkflowSetupRootState, 'workflowSetup'>
      | Pick<WorkflowSetupRootState, 'workflowMethods'>
      | Pick<WorkflowSetupRootState, 'workflowInstance'>
      | Pick<WorkflowSetupRootState, 'elementMetadata'>
      | Pick<WorkflowSetupRootState, 'workflowChangeSets'>
      | Pick<WorkflowSetupRootState, 'createNewChange'>
      | Pick<WorkflowSetupRootState, 'parameterData'>
  ) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector => selector({ workflowSetup }));
  };

  afterEach(() => {
    selectorMock.mockClear();
  });

  it('test on selector useSelectParameterData', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: {
        elements: [
          {
            name: '123456.subTransactionCode.transaction',
            options: [
              { value: 'value', name: 'name', workFlow: 'workFlow' },
              { value: '', name: 'name', workFlow: 'workFlow' }
            ]
          },
          {
            name: 'name',
            options: [
              { value: 'value', name: 'name', workFlow: 'workFlow' },
              { value: '', name: 'name', workFlow: 'workFlow' }
            ]
          },
          {
            name: 'transaction.id',
            options: [
              { value: 'value', name: 'name', workFlow: 'accountDelinquency' },
              { value: '', name: 'name', workFlow: 'accountDelinquency' }
            ]
          }
        ] as any
      }
    });

    const { result } = renderHook(() =>
      useSelectParameterData({
        actionCode: '1234567',
        subTransactionCode: 'subTransactionCode'
      })
    );

    expect(result.current.length).toEqual(2);
  });

  it('test on selector useSelectParameterData with diff data', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: {
        elements: [
          {
            name: '123456.subTransactionCode',
            options: [
              { value: 'value', name: 'name', workFlow: 'workFlow' },
              { value: '', name: 'name', workFlow: 'workFlow' }
            ]
          },
          {
            name: 'name',
            options: [
              { value: 'value', name: 'name', workFlow: 'workFlow' },
              { value: '', name: 'name', workFlow: 'workFlow' }
            ]
          },
          {
            name: 'transaction.id',
            options: [
              { value: 'value', name: 'name', workFlow: 'accountDelinquency' },
              { value: '', name: 'name', workFlow: 'accountDelinquency' }
            ]
          }
        ] as any
      }
    });

    const { result } = renderHook(() =>
      useSelectParameterData({
        actionCode: '1234567',
        subTransactionCode: ''
      })
    );

    expect(result.current.length).toEqual(0);
  });

  it('test on selector useSelectElementMetadataForManageAccountDelinquency', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: {
        elements: [
          {
            name: 'name',
            options: [
              { value: 'value', name: 'name', workFlow: 'workFlow' },
              { value: '', name: 'name', workFlow: 'workFlow' }
            ]
          },
          {
            name: 'transaction.id',
            options: [
              { value: 'value', name: 'name', workFlow: 'accountDelinquency' },
              { value: '', name: 'name', workFlow: 'accountDelinquency' }
            ]
          }
        ] as any
      }
    });

    const { result } = renderHook(() =>
      useSelectElementMetadataForManageAccountDelinquency()
    );

    expect(result.current.action.length).toEqual(2);
  });

  it('test on selector useSelectElementMetadataForManageAccountDelinquency with options > workflow equal accountDelinquency', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: {
        elements: [
          {
            name: 'name',
            options: [
              { value: 'value', name: 'name', workFlow: 'workFlow' },
              { value: '', name: 'name', workFlow: 'workFlow' }
            ]
          }
        ] as any
      }
    });

    const { result } = renderHook(() =>
      useSelectElementMetadataForManageAccountDelinquency()
    );

    expect(result.current.action).toEqual([]);
  });

  it('test on selector useSelectElementMetadataForManageAccountDelinquency with options > workflow difference accountDelinquency', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: {
        elements: [
          {
            name: 'action',
            options: [
              { value: 'value', name: 'name', workFlow: 'workFlow' },
              { value: '', name: 'name', workFlow: 'workFlow' }
            ]
          }
        ] as any
      }
    });

    const { result } = renderHook(() =>
      useSelectElementMetadataForManageAccountDelinquency()
    );
    expect(result.current.action.length).toEqual(0);
  });

  it('test on selector useSelectElementMetadataForManageAccountDelinquency with options equal undefined', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: {
        elements: [
          {
            name: 'action',
            options: undefined
          }
        ] as any
      }
    });

    const { result } = renderHook(() =>
      useSelectElementMetadataForManageAccountDelinquency()
    );

    expect(result.current.action).toEqual([]);
  });
});
