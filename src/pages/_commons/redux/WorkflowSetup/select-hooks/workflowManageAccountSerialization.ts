import { createSelector } from '@reduxjs/toolkit';
import { RecordFieldParameterEnum } from 'app/constants/enums';
import { isEmpty } from 'lodash';
import { useSelector } from 'react-redux';

const elementMetadataForSPASelector = createSelector(
  (state: RootState) => state.workflowSetup!.elementMetadata.elements,
  data => {
    const elements = data.map(e => {
      return {
        name: e.name,
        options: e.options?.map(opt => ({
          code: opt.value,
          text: !isEmpty(opt.value) ? `${opt.value} - ${opt.description}` : ''
        }))
      } as {
        name: string;
        options: RefData[];
      };
    });

    const accountIDOrPIID =
      elements.find(e => e.name === RecordFieldParameterEnum.AccountIDOrPIID)
        ?.options || [];

    const spas =
      elements.find(e => e.name === RecordFieldParameterEnum.SPAs)?.options ||
      [];

    let sys: string[] = [];
    const prin: MagicKeyValue = {};
    const agent: MagicKeyValue = {};

    spas.forEach(spa => {
      const sysItem = spa.code.split('-')[0];
      const index = sys.indexOf(sysItem);
      if (index === -1) {
        sys = [...sys, sysItem];
      }
    });

    sys.forEach(sysI => {
      prin[sysI] = [];
      spas.forEach(spa => {
        const sysItem = spa.code.split('-')[0];
        const prinItem = spa.code.split('-')[1];
        if (sysI == sysItem) {
          const index = prin[sysI].indexOf(prinItem);
          if (index === -1) {
            prin[sysI] = [...prin[sysI], prinItem];
          }
        }
      });
    });

    for (const [key, value] of Object.entries(prin)) {
      value.forEach((spinI: string) => {
        agent[`${key}${spinI}`] = [];
        spas.forEach(spa => {
          const spaArr = spa.code.split('-');
          const sysItem = spaArr[0];
          const prinItem = spaArr[1];
          const agentItem = spaArr[2];
          if (key == sysItem && spinI == prinItem) {
            const index = agent[`${key}${spinI}`].indexOf(agentItem);
            if (index === -1) {
              agent[`${key}${spinI}`] = [...agent[`${key}${spinI}`], agentItem];
            }
          }
        });
      });
    }

    return {
      spas,
      sys,
      prin,
      agent,
      accountIDOrPIID
    };
  }
);

export const useSelectElementMetadataForSPA = () => {
  return useSelector<
    RootState,
    {
      spas: RefData[];
      sys: string[];
      prin: MagicKeyValue;
      agent: MagicKeyValue;
      accountIDOrPIID: RefData[];
    }
  >(elementMetadataForSPASelector);
};
