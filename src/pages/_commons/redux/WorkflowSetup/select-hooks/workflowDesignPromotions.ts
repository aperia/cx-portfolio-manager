import { createSelector } from '@reduxjs/toolkit';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import { useSelector } from 'react-redux';
import { KeepTextDelayDesignPromotion } from './_common';

const KeepTextElementsPromotions = [
  MethodFieldParameterEnum.PromotionStatementTextIDDTDD.toString(),
  MethodFieldParameterEnum.PromotionStatementTextIDDTDX.toString(),
  MethodFieldParameterEnum.PromotionStatementTextIDStandard.toString(),
  MethodFieldParameterEnum.PayoffExceptionMethod.toString(),
  MethodFieldParameterEnum.CashAdvanceItemFeesMethod.toString(),
  MethodFieldParameterEnum.MerchantItemFeesMethod.toString(),
  MethodFieldParameterEnum.PromotionGroupIdentifier.toString(),
  MethodFieldParameterEnum.RulesMinimumPaymentDueMethod.toString(),
  MethodFieldParameterEnum.DefaultInterestRateMethodOverride.toString(),
  MethodFieldParameterEnum.InterestCalculationMethodOverride.toString(),
  MethodFieldParameterEnum.IntroductoryPeriodStatementTextID.toString(),
  MethodFieldParameterEnum.DesignPromotionBasicInformation8.toString(),
  MethodFieldParameterEnum.DesignPromotionInterestAssessment10.toString(),
  MethodFieldParameterEnum.DesignPromotionInterestAssessment11.toString(),
  MethodFieldParameterEnum.DesignPromotionInterestAssessment12.toString(),
  MethodFieldParameterEnum.DesignPromotionInterestAssessment13.toString(),
  MethodFieldParameterEnum.DesignPromotionInterestAssessment14.toString(),
  MethodFieldParameterEnum.DesignPromotionInterestAssessment15.toString(),
  MethodFieldParameterEnum.DesignPromotionInterestOverrides4.toString(),
  MethodFieldParameterEnum.DesignPromotionInterestOverrides5.toString(),
  MethodFieldParameterEnum.DesignPromotionInterestOverrides6.toString(),
  MethodFieldParameterEnum.DesignPromotionStatementMessaging2.toString(),
  MethodFieldParameterEnum.DesignPromotionStatementMessaging5.toString()
];

const FilterByPWElements = [
  MethodFieldParameterEnum.DesignPromotionBasicInformation12.toString(),
  MethodFieldParameterEnum.DesignPromotionReturnRevolve6.toString()
];

const NoneAndNumberList: string[] = [
  MethodFieldParameterEnum.DesignPromotionBasicInformation7,
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter5
];

const elementMetadataForPWSelector = createSelector(
  (state: RootState) => state.workflowSetup!.elementMetadata.elements,
  data => {
    const elements = data.map(e => {
      if (e.name == 'promotion.statement.text.id') {
        return {
          name: e.name,
          options: e.options
            ?.filter(
              optFilter =>
                optFilter.workFlow === 'designPromotion' || !optFilter.workFlow
            )
            .map(opt => ({
              code: opt.value,
              text: `${opt.value}`
            }))
        };
      }
      if (NoneAndNumberList.includes(e.name)) {
        return {
          name: e.name,
          options: e.options?.map(opt => ({
            code: opt.value,
            text: opt.description
          }))
        };
      }
      if (KeepTextElementsPromotions.includes(e.name)) {
        return {
          name: e.name,
          options: e.options?.map(opt => ({
            code: opt.value,
            text: `${opt.value}`
          }))
        } as {
          name: string;
          options: RefData[];
        };
      }
      if (KeepTextDelayDesignPromotion.includes(e.name)) {
        return {
          name: e.name,
          options: e.options?.map(opt => ({
            code: opt.value,
            text: `${opt.value}`
          }))
        } as {
          name: string;
          options: RefData[];
        };
      }
      if (FilterByPWElements.includes(e.name)) {
        return {
          name: e.name,
          options: e.options
            ?.filter(
              optFilter =>
                optFilter.workFlow === 'designPromotion' || !optFilter.workFlow
            )
            .map(opt => ({
              code: opt.value,
              text: `${opt.value ? opt.value + ' - ' : ''}${opt.description}`
            }))
        };
      }
      return {
        name: e.name,
        options: e.options?.map(opt => ({
          code: opt.value,
          text: `${opt.value ? opt.value + ' - ' : ''}${opt.description}`
        }))
      } as {
        name: string;
        options: RefData[];
      };
    });

    // design promotions
    const designPromotionBasicInformation3 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionBasicInformation3
      )?.options || [];

    const designPromotionBasicInformation4 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionBasicInformation4
      )?.options || [];

    const designPromotionBasicInformation5 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionBasicInformation5
      )?.options || [];

    const designPromotionBasicInformation6 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionBasicInformation6
      )?.options || [];

    const designPromotionBasicInformation7 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionBasicInformation7
      )?.options || [];

    const designPromotionBasicInformation8 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionBasicInformation8
      )?.options || [];

    const designPromotionBasicInformation9 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionBasicInformation9
      )?.options || [];

    const designPromotionBasicInformation10 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionBasicInformation10
      )?.options || [];

    const designPromotionBasicInformation11 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionBasicInformation11
      )?.options || [];

    const designPromotionBasicInformation12 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionBasicInformation12
      )?.options || [];

    const designPromotionInterestAssessment9 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionInterestAssessment9
      )?.options || [];

    const designPromotionInterestAssessment10 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionInterestAssessment10
      )?.options || [];

    const designPromotionInterestAssessment11 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionInterestAssessment11
      )?.options || [];

    const designPromotionInterestAssessment12 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionInterestAssessment12
      )?.options || [];

    const designPromotionInterestAssessment13 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionInterestAssessment13
      )?.options || [];

    const designPromotionInterestAssessment14 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionInterestAssessment14
      )?.options || [];

    const designPromotionInterestAssessment15 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionInterestAssessment15
      )?.options || [];

    const designPromotionInterestAssessment16 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionInterestAssessment16
      )?.options || [];

    const designPromotionInterestAssessment17 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionInterestAssessment17
      )?.options || [];

    const designPromotionInterestOverrides1 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionInterestOverrides1
      )?.options || [];
    const designPromotionInterestOverrides2 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionInterestOverrides2
      )?.options || [];

    const designPromotionInterestOverrides3 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionInterestOverrides3
      )?.options || [];

    const designPromotionInterestOverrides4 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionInterestOverrides4
      )?.options || [];

    const designPromotionInterestOverrides5 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionInterestOverrides5
      )?.options || [];

    const designPromotionInterestOverrides6 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionInterestOverrides6
      )?.options || [];

    const designPromotionInterestOverrides7 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionInterestOverrides7
      )?.options || [];

    const designPromotionInterestOverrides8 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionInterestOverrides8
      )?.options || [];

    const designPromotionRetailPath1 =
      elements.find(
        e => e.name === MethodFieldParameterEnum.DesignPromotionRetailPath1
      )?.options || [];

    const designPromotionRetailPath4 =
      elements.find(
        e => e.name === MethodFieldParameterEnum.DesignPromotionRetailPath4
      )?.options || [];

    const designPromotionReturnRevolve2 =
      elements.find(
        e => e.name === MethodFieldParameterEnum.DesignPromotionReturnRevolve2
      )?.options || [];

    const designPromotionReturnRevolve3 =
      elements.find(
        e => e.name === MethodFieldParameterEnum.DesignPromotionReturnRevolve3
      )?.options || [];

    const designPromotionReturnRevolve4 =
      elements.find(
        e => e.name === MethodFieldParameterEnum.DesignPromotionReturnRevolve4
      )?.options || [];

    const designPromotionReturnRevolve6 =
      elements.find(
        e => e.name === MethodFieldParameterEnum.DesignPromotionReturnRevolve6
      )?.options || [];

    const designPromotionStatementMessaging1 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionStatementMessaging1
      )?.options || [];

    const designPromotionStatementMessaging2 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionStatementMessaging2
      )?.options || [];

    const designPromotionStatementMessaging3 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionStatementMessaging3
      )?.options || [];

    const designPromotionStatementMessaging4 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionStatementMessaging4
      )?.options || [];

    const designPromotionStatementMessaging5 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionStatementMessaging5
      )?.options || [];

    const designPromotionStatementAdvancedParameter1 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter1
      )?.options || [];
    const designPromotionStatementAdvancedParameter3 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter3
      )?.options || [];
    const designPromotionStatementAdvancedParameter4 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter4
      )?.options || [];
    const designPromotionStatementAdvancedParameter5 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter5
      )?.options || [];
    const designPromotionStatementAdvancedParameter7 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter7
      )?.options || [];
    const designPromotionStatementAdvancedParameter8 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter8
      )?.options || [];
    const designPromotionStatementAdvancedParameter9 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter9
      )?.options || [];
    const designPromotionStatementAdvancedParameter10 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter10
      )?.options || [];
    const designPromotionStatementAdvancedParameter11 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter11
      )?.options || [];
    const designPromotionStatementAdvancedParameter12 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter12
      )?.options || [];
    const designPromotionStatementAdvancedParameter13 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter13
      )?.options || [];
    const designPromotionStatementAdvancedParameter14 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter14
      )?.options || [];
    const designPromotionStatementAdvancedParameter15 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter15
      )?.options || [];
    const designPromotionStatementAdvancedParameter16 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter16
      )?.options || [];
    const designPromotionStatementAdvancedParameter17 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter17
      )?.options || [];
    const designPromotionStatementAdvancedParameter18 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter18
      )?.options || [];
    const designPromotionStatementAdvancedParameter19 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter19
      )?.options || [];
    const designPromotionStatementAdvancedParameter20 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter20
      )?.options || [];
    const designPromotionStatementAdvancedParameter21 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter21
      )?.options || [];
    const designPromotionStatementAdvancedParameter22 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter22
      )?.options || [];
    const designPromotionStatementAdvancedParameter23 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter23
      )?.options || [];
    const designPromotionStatementAdvancedParameter24 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter24
      )?.options || [];
    const designPromotionStatementAdvancedParameter25 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter25
      )?.options || [];
    const designPromotionStatementAdvancedParameter26 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter26
      )?.options || [];
    const designPromotionStatementAdvancedParameter27 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter27
      )?.options || [];
    const designPromotionStatementAdvancedParameter28 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter28
      )?.options || [];

    return {
      designPromotionBasicInformation3,
      designPromotionBasicInformation4,
      designPromotionBasicInformation5,
      designPromotionBasicInformation6,
      designPromotionBasicInformation7,
      designPromotionBasicInformation8,
      designPromotionBasicInformation9,
      designPromotionBasicInformation10,
      designPromotionBasicInformation11,
      designPromotionBasicInformation12,
      designPromotionInterestAssessment9,
      designPromotionInterestAssessment10,
      designPromotionInterestAssessment11,
      designPromotionInterestAssessment12,
      designPromotionInterestAssessment13,
      designPromotionInterestAssessment14,
      designPromotionInterestAssessment15,
      designPromotionInterestAssessment16,
      designPromotionInterestAssessment17,
      designPromotionInterestOverrides1,
      designPromotionInterestOverrides2,
      designPromotionInterestOverrides3,
      designPromotionInterestOverrides4,
      designPromotionInterestOverrides5,
      designPromotionInterestOverrides6,
      designPromotionInterestOverrides7,
      designPromotionInterestOverrides8,
      designPromotionRetailPath1,
      designPromotionRetailPath4,
      designPromotionReturnRevolve2,
      designPromotionReturnRevolve3,
      designPromotionReturnRevolve4,
      designPromotionReturnRevolve6,
      designPromotionStatementMessaging1,
      designPromotionStatementMessaging2,
      designPromotionStatementMessaging3,
      designPromotionStatementMessaging4,
      designPromotionStatementMessaging5,
      designPromotionStatementAdvancedParameter1,
      designPromotionStatementAdvancedParameter3,
      designPromotionStatementAdvancedParameter4,
      designPromotionStatementAdvancedParameter5,
      designPromotionStatementAdvancedParameter7,
      designPromotionStatementAdvancedParameter8,
      designPromotionStatementAdvancedParameter9,
      designPromotionStatementAdvancedParameter10,
      designPromotionStatementAdvancedParameter11,
      designPromotionStatementAdvancedParameter12,
      designPromotionStatementAdvancedParameter13,
      designPromotionStatementAdvancedParameter14,
      designPromotionStatementAdvancedParameter15,
      designPromotionStatementAdvancedParameter16,
      designPromotionStatementAdvancedParameter17,
      designPromotionStatementAdvancedParameter18,
      designPromotionStatementAdvancedParameter19,
      designPromotionStatementAdvancedParameter20,
      designPromotionStatementAdvancedParameter21,
      designPromotionStatementAdvancedParameter22,
      designPromotionStatementAdvancedParameter23,
      designPromotionStatementAdvancedParameter24,
      designPromotionStatementAdvancedParameter25,
      designPromotionStatementAdvancedParameter26,
      designPromotionStatementAdvancedParameter27,
      designPromotionStatementAdvancedParameter28
    };
  }
);

export const useSelectElementMetadataForPW = () => {
  return useSelector<
    RootState,
    {
      designPromotionBasicInformation3: RefData[];
      designPromotionBasicInformation4: RefData[];
      designPromotionBasicInformation5: RefData[];
      designPromotionBasicInformation6: RefData[];
      designPromotionBasicInformation7: RefData[];
      designPromotionBasicInformation8: RefData[];
      designPromotionBasicInformation9: RefData[];
      designPromotionBasicInformation10: RefData[];
      designPromotionBasicInformation11: RefData[];
      designPromotionBasicInformation12: RefData[];
      designPromotionInterestAssessment9: RefData[];
      designPromotionInterestAssessment10: RefData[];
      designPromotionInterestAssessment11: RefData[];
      designPromotionInterestAssessment12: RefData[];
      designPromotionInterestAssessment13: RefData[];
      designPromotionInterestAssessment14: RefData[];
      designPromotionInterestAssessment15: RefData[];
      designPromotionInterestAssessment16: RefData[];
      designPromotionInterestAssessment17: RefData[];
      designPromotionInterestOverrides1: RefData[];
      designPromotionInterestOverrides2: RefData[];
      designPromotionInterestOverrides3: RefData[];
      designPromotionInterestOverrides4: RefData[];
      designPromotionInterestOverrides5: RefData[];
      designPromotionInterestOverrides6: RefData[];
      designPromotionInterestOverrides7: RefData[];
      designPromotionInterestOverrides8: RefData[];
      designPromotionRetailPath1: RefData[];
      designPromotionRetailPath4: RefData[];
      designPromotionReturnRevolve2: RefData[];
      designPromotionReturnRevolve3: RefData[];
      designPromotionReturnRevolve4: RefData[];
      designPromotionReturnRevolve6: RefData[];
      designPromotionStatementMessaging1: RefData[];
      designPromotionStatementMessaging2: RefData[];
      designPromotionStatementMessaging3: RefData[];
      designPromotionStatementMessaging4: RefData[];
      designPromotionStatementMessaging5: RefData[];
      // design promotion statement advanced parameter
      designPromotionStatementAdvancedParameter1: RefData[];
      designPromotionStatementAdvancedParameter3: RefData[];
      designPromotionStatementAdvancedParameter4: RefData[];
      designPromotionStatementAdvancedParameter5: RefData[];
      designPromotionStatementAdvancedParameter7: RefData[];
      designPromotionStatementAdvancedParameter8: RefData[];
      designPromotionStatementAdvancedParameter9: RefData[];
      designPromotionStatementAdvancedParameter10: RefData[];
      designPromotionStatementAdvancedParameter11: RefData[];
      designPromotionStatementAdvancedParameter12: RefData[];
      designPromotionStatementAdvancedParameter13: RefData[];
      designPromotionStatementAdvancedParameter14: RefData[];
      designPromotionStatementAdvancedParameter15: RefData[];
      designPromotionStatementAdvancedParameter16: RefData[];
      designPromotionStatementAdvancedParameter17: RefData[];
      designPromotionStatementAdvancedParameter18: RefData[];
      designPromotionStatementAdvancedParameter19: RefData[];
      designPromotionStatementAdvancedParameter20: RefData[];
      designPromotionStatementAdvancedParameter21: RefData[];
      designPromotionStatementAdvancedParameter22: RefData[];
      designPromotionStatementAdvancedParameter23: RefData[];
      designPromotionStatementAdvancedParameter24: RefData[];
      designPromotionStatementAdvancedParameter25: RefData[];
      designPromotionStatementAdvancedParameter26: RefData[];
      designPromotionStatementAdvancedParameter27: RefData[];
      designPromotionStatementAdvancedParameter28: RefData[];
    }
  >(elementMetadataForPWSelector);
};
