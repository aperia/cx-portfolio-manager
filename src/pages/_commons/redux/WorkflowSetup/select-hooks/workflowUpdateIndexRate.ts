import { matchSearchValue } from 'app/helpers';
import _orderBy from 'lodash.orderby';
import { useSelector } from 'react-redux';
import { createSelector } from 'reselect';
const workflowUpdateIndexRateSelector = createSelector(
  (state: RootState) => state.workflowSetup!.workflowMethods,
  data => {
    const {
      methods: rawData,
      dataFilter = {},
      loading = false,
      error = false
    } = data;
    const { orderBy, page, pageSize, searchValue = '' } = dataFilter;
    const _searchValue = searchValue.toLowerCase() || '';
    const _method = _orderBy(rawData, ['name'], orderBy).filter(w => {
      return matchSearchValue([w.name], _searchValue);
    });
    const total = _method.length;
    const methods = _method.slice((page! - 1) * pageSize!, page! * pageSize!);
    return { methods, total, loading, error };
  }
);

export const useSelectWorkflowUpdateIndexRateSelector = () => {
  return useSelector<
    RootState,
    {
      methods: IMethodModel[];
      loading: boolean;
      error: boolean;
      total: number;
    }
  >(workflowUpdateIndexRateSelector);
};
