import { createSelector } from '@reduxjs/toolkit';
import { matchSearchValue } from 'app/helpers';
import _orderBy from 'lodash.orderby';
import { PAYOFF_DROPDOWN_FIELDS } from 'pages/WorkflowManagePayoffExceptionMethods/ConfigureParameters/constant';
import { useSelector } from 'react-redux';

const elementMetadataForPayoffExceptionMethodsSelector = createSelector(
  (state: RootState) => state.workflowSetup!.elementMetadata.elements,
  data => {
    const elements = {};
    data.forEach(e => {
      if (PAYOFF_DROPDOWN_FIELDS.includes(e.name)) {
        Object.assign(elements, {
          [e.name]: e.options?.map(opt => ({
            code: opt.value,
            text: opt.value
              ? `${opt.value}${opt.description ? ` - ${opt.description}` : ''}`
              : `${opt.description}`
          })) as RefData[]
        });
      }
    });

    return elements;
  }
);

export const useSelectElementMetadataForPayoffMethods = () => {
  return useSelector<RootState, Record<string, RefData[]>>(
    elementMetadataForPayoffExceptionMethodsSelector
  );
};

const workflowPayoffExceptionMethodsSelector = createSelector(
  (state: RootState) => state.workflowSetup!.workflowMethods,
  data => {
    const {
      methods: rawData,
      dataFilter = {},
      loading = false,
      error = false
    } = data;
    const { sortBy, orderBy, page, pageSize, searchValue = '' } = dataFilter;
    const _searchValue = searchValue.toLowerCase() || '';
    const _method = _orderBy(rawData, sortBy, orderBy).filter(w => {
      return matchSearchValue([w.name], _searchValue);
    });
    const total = _method.length;
    const methods = _method.slice((page! - 1) * pageSize!, page! * pageSize!);
    return { methods, total, loading, error };
  }
);

export const useSelectWorkflowPayoffExceptionMethodsSelector = () => {
  return useSelector<
    RootState,
    {
      methods: IMethodModel[];
      loading: boolean;
      error: boolean;
      total: number;
    }
  >(workflowPayoffExceptionMethodsSelector);
};

const workflowPayoffExceptionMethodsFilterSelector = createSelector(
  (state: RootState) => state.workflowSetup?.workflowMethods.dataFilter,
  data => data || {}
);
export const useSelectWorkflowPayoffExceptionMethodsFilterSelector = () => {
  return useSelector<RootState, IDataFilter>(
    workflowPayoffExceptionMethodsFilterSelector
  );
};

const expandingPayoffExceptionMethodSelector = createSelector(
  (state: RootState) => state.workflowSetup?.workflowMethods.expandingMethod,
  data => data!
);

export const useSelectExpandingPayoffExceptionMethod = () => {
  return useSelector<RootState, string>(expandingPayoffExceptionMethodSelector);
};
