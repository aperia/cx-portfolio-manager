import { createSelector } from '@reduxjs/toolkit';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import { PORTFOLIO_ATTRIBUTE_FIELDS } from 'app/constants/mapping';
import { filter, get, isEmpty, orderBy, uniqBy } from 'lodash';
import { useSelector } from 'react-redux';

const elementMetadataForPortfolioDefinitionsSelector = createSelector(
  (state: RootState) => state.workflowSetup!,
  workflowData => {
    //.elementMetadata.elements
    const data = get(workflowData, ['elementMetadata', 'elements']);
    const stratergies = get(workflowData, ['workflowStrategies', 'strategies']);

    const stratergiesMetadata = stratergies.map(
      stratergy =>
        ({
          code: get(stratergy, 'strategyName'),
          text: get(stratergy, 'strategyName')
        } as RefData)
    );

    const elements = {
      ['pricing.stratergies']: orderBy(stratergiesMetadata, ['text'], ['asc'])
    };
    data.forEach(e => {
      if (
        e.name ===
        MethodFieldParameterEnum.PortfolioDefinitionsAttributeSysPrinAgents
      ) {
        const sysPrinAgent: any = [[], [], []];

        e.options?.forEach(opt => {
          const valueArr = opt.value.split('-');
          sysPrinAgent.forEach((item: any, i: number) => {
            item.push({
              code: valueArr[i],
              text: valueArr[i]
            });
          });
        });

        Object.assign(elements, {
          sys: orderBy(uniqBy(sysPrinAgent[0], 'code'), ['code', 'text']),
          prin: orderBy(uniqBy(sysPrinAgent[1], 'code'), ['code', 'text']),
          agent: orderBy(uniqBy(sysPrinAgent[2], 'code'), ['code', 'text'])
        });
        return;
      }

      if (PORTFOLIO_ATTRIBUTE_FIELDS.includes(e.name)) {
        let optionsByWf: any[] = filter(e.options, { workFlow: 'cpd' });

        if (!isEmpty(optionsByWf)) {
          optionsByWf = orderBy(optionsByWf, ['value']);
          Object.assign(elements, {
            [e.name]: optionsByWf.map(opt => ({
              code: opt.value,
              text: opt.description || opt.value
            })) as RefData[]
          });
          return;
        }

        optionsByWf =
          e.name ===
          MethodFieldParameterEnum.PortfolioDefinitionsAttributeFilterBy
            ? e.options || []
            : orderBy(e.options, ['value']);

        Object.assign(elements, {
          [e.name]: optionsByWf.map(opt => ({
            code: opt.value,
            text: opt.description || opt.value
          })) as RefData[]
        });
      }
    });

    return elements;
  }
);

export const useSelectElementMetadataForPortfolioDefinitionsSelector = () => {
  return useSelector<RootState, Record<string, RefData[]>>(
    elementMetadataForPortfolioDefinitionsSelector
  );
};

const workflowPayoffExceptionMethodsFilterSelector = createSelector(
  (state: RootState) => state.workflowSetup?.workflowMethods.dataFilter,
  data => data || {}
);
export const useSelectWorkflowPayoffExceptionMethodsFilterSelector = () => {
  return useSelector<RootState, IDataFilter>(
    workflowPayoffExceptionMethodsFilterSelector
  );
};

const expandingPayoffExceptionMethodSelector = createSelector(
  (state: RootState) => state.workflowSetup?.workflowMethods.expandingMethod,
  data => data!
);

export const useSelectExpandingPayoffExceptionMethod = () => {
  return useSelector<RootState, string>(expandingPayoffExceptionMethodSelector);
};
