import { renderHook } from '@testing-library/react-hooks';
import { WorkflowSetupRootState } from 'app/types';
import * as reactRedux from 'react-redux';
import { useSelectElementMetadataCycleAccount } from './workflowCycleAccountsTestEnvironment';

describe('redux-store > workflow-setup > select-hooks ', () => {
  let selectorMock: jest.SpyInstance;
  const mockSelectorWorkflowSetup = (
    workflowSetup:
      | Pick<WorkflowSetupRootState, 'workflowFormSteps'>
      | Pick<WorkflowSetupRootState, 'workflowSetup'>
      | Pick<WorkflowSetupRootState, 'workflowMethods'>
      | Pick<WorkflowSetupRootState, 'workflowInstance'>
      | Pick<WorkflowSetupRootState, 'elementMetadata'>
      | Pick<WorkflowSetupRootState, 'workflowChangeSets'>
      | Pick<WorkflowSetupRootState, 'createNewChange'>
      | Pick<WorkflowSetupRootState, 'parameterData'>
  ) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector => selector({ workflowSetup }));
  };

  afterEach(() => {
    selectorMock.mockClear();
  });

  it('test on selector useSelectElementMetadataCycleAccount', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: {
        elements: [
          {
            id: '331',
            name: 'nm.792.36.transaction',
            options: [
              {
                id: 17,
                value: 'IDENTIFIER',
                type: 'Required',
                description: "Identifier of the customer's account"
              }
            ]
          },
          {
            id: '331',
            name: 'code',
            options: [
              {
                value: '',
                description: 'None'
              },
              {
                value: 'N',
                description: 'Do not allow CAT region force statement cycle'
              },
              {
                value: 'Y',
                description: 'Allow CAT region force statement cycle'
              }
            ]
          },
          {
            id: '335',
            name: 'transaction',
            options: [
              {
                value: '',
                description: 'None'
              },
              {
                value: 'N',
                description: 'Do not allow CAT region force statement cycle'
              },
              {
                value: 'Y',
                description: 'Allow CAT region force statement cycle'
              }
            ]
          }
        ]
      }
    } as any);

    const { rerender, result } = renderHook(() =>
      useSelectElementMetadataCycleAccount()
    );

    mockSelectorWorkflowSetup({
      elementMetadata: {}
    } as any);
    rerender();
    expect(result.current.cycleAccountsTestEnvironmentCode.length).toEqual(0);
  });
});
