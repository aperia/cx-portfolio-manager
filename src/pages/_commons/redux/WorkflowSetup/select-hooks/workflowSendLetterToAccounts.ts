import { createSelector } from '@reduxjs/toolkit';
import { useSelector } from 'react-redux';

const lettersSelector = createSelector(
  (state: RootState) => state.workflowSetup!.lettersData?.letters,
  data => data
);
export const useSelectLetters = () => {
  return useSelector<RootState, ILetter[]>(lettersSelector);
};
