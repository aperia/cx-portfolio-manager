import { createSelector } from '@reduxjs/toolkit';
import { SubTransactionCode } from 'pages/WorkflowManageAccountDelinquency/ConfigureParametersStep';
import { useSelector } from 'react-redux';

interface Params {
  actionCode: string;
  subTransactionCode: string;
}

const getParameterList = (elementFilter: Params) =>
  createSelector(
    (state: RootState) => state.workflowSetup!.elementMetadata?.elements,
    data => {
      const { actionCode, subTransactionCode } = elementFilter;
      const subCode = !!subTransactionCode ? '.' + subTransactionCode : '';
      const filterCode = `${actionCode?.slice(0, 6)}${subCode}.transaction`;

      const elementsData = data?.find(e => e.name === filterCode);

      if (!elementsData) return [];

      const listData = elementsData.options?.map(e => ({
        ...e,
        name: e.value
      }));
      return listData as IParameterModel[];
    }
  );
export const useSelectParameterData = (elementFilter: Params) => {
  return useSelector<RootState, IParameterModel[]>(
    getParameterList(elementFilter)
  );
};

const elementMetadataForManageAccountDelinquencySelector = createSelector(
  (state: RootState) => state.workflowSetup!.elementMetadata.elements,
  data => {
    const elements = data.map(e => {
      return {
        name: e.name,
        options: e.options?.map(opt => ({
          ...opt,
          code: opt.value,
          text: opt?.value
            ? `${opt.value} - ${opt?.description || ''}`
            : opt.description || ''
        }))
      } as {
        name: string;
        options: (ManageAccountDelinquencyMetaDataOption & RefData)[];
      };
    });
    const options =
      elements.find(e => e.name === 'transaction.id')?.options || [];
    const action = options.filter(
      x => !x.workFlow || x?.workFlow === 'accountDelinquency'
    );

    const nm180codes =
      elements.find(e => e.name === SubTransactionCode.NM180Code)?.options ||
      [];
    const nm180Mcsr =
      elements.find(e => e.name === SubTransactionCode.NM180Csr)?.options || [];
    const nm183codes =
      elements.find(e => e.name === SubTransactionCode.NM183)?.options || [];
    const nm181csa =
      elements.find(e => e.name === SubTransactionCode.NM181)?.options || [];

    return {
      action,
      nm180codes,
      nm180Mcsr,
      nm183codes,
      nm181csa
    };
  }
);

export const useSelectElementMetadataForManageAccountDelinquency = () => {
  return useSelector<
    RootState,
    {
      action: (ManageAccountDelinquencyMetaDataOption & RefData)[];
      nm180codes: (ManageAccountDelinquencyMetaDataOption & RefData)[];
      nm180Mcsr: (ManageAccountDelinquencyMetaDataOption & RefData)[];
      nm183codes: (ManageAccountDelinquencyMetaDataOption & RefData)[];
      nm181csa: (ManageAccountDelinquencyMetaDataOption & RefData)[];
    }
  >(elementMetadataForManageAccountDelinquencySelector);
};
