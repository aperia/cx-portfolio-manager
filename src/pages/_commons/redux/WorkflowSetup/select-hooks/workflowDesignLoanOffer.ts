import { createSelector } from '@reduxjs/toolkit';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import { matchSearchValue } from 'app/helpers';
import _orderBy from 'lodash.orderby';
import { useSelector } from 'react-redux';

const workflowMethodsDLOSelector = createSelector(
  (state: RootState) => state.workflowSetup!.workflowMethods,
  data => {
    const {
      methods: rawData,
      dataFilter = {},
      loading = false,
      error = false
    } = data;
    const { sortBy, orderBy, page, pageSize, searchValue = '' } = dataFilter;
    const _searchValue = searchValue.toLowerCase() || '';
    const _method = _orderBy(rawData, sortBy, orderBy).filter(w => {
      return matchSearchValue([w.name], _searchValue);
    });
    const total = _method.length;
    const methods = _method.slice((page! - 1) * pageSize!, page! * pageSize!);
    return { methods, total, loading, error };
  }
);

export const useSelectWorkflowMethodsDLOSelector = () => {
  return useSelector<
    RootState,
    {
      methods: IMethodModel[];
      loading: boolean;
      error: boolean;
      total: number;
    }
  >(workflowMethodsDLOSelector);
};

const KeepTextElementsDLO = [
  MethodFieldParameterEnum.PromotionStatementTextIDDTDD.toString(),
  MethodFieldParameterEnum.PromotionStatementTextIDDTDX.toString(),
  MethodFieldParameterEnum.PromotionStatementTextIDStandard.toString(),
  MethodFieldParameterEnum.PayoffExceptionMethod.toString(),
  MethodFieldParameterEnum.CashAdvanceItemFeesMethod.toString(),
  MethodFieldParameterEnum.MerchantItemFeesMethod.toString(),
  MethodFieldParameterEnum.PromotionGroupIdentifier.toString(),
  MethodFieldParameterEnum.RulesMinimumPaymentDueMethod.toString(),
  MethodFieldParameterEnum.DefaultInterestRateMethodOverride.toString(),
  MethodFieldParameterEnum.InterestCalculationMethodOverride.toString(),
  MethodFieldParameterEnum.IntroductoryPeriodStatementTextID.toString(),
  MethodFieldParameterEnum.DesignPromotionBasicInformation8.toString(),
  MethodFieldParameterEnum.DesignPromotionInterestAssessment10.toString(),
  MethodFieldParameterEnum.DesignPromotionInterestAssessment11.toString(),
  MethodFieldParameterEnum.DesignPromotionInterestAssessment12.toString(),
  MethodFieldParameterEnum.DesignPromotionInterestAssessment13.toString(),
  MethodFieldParameterEnum.DesignPromotionInterestAssessment14.toString(),
  MethodFieldParameterEnum.DesignPromotionInterestAssessment15.toString(),
  MethodFieldParameterEnum.DesignPromotionInterestOverrides4.toString(),
  MethodFieldParameterEnum.DesignPromotionInterestOverrides5.toString(),
  MethodFieldParameterEnum.DesignPromotionInterestOverrides6.toString()
];
const FilterByWLElements = [
  MethodFieldParameterEnum.PromotionReturnApplicationOption.toString(),
  MethodFieldParameterEnum.ReturnToRevolvingBaseInterest.toString()
];

const elementMetadataForDLOSelector = createSelector(
  (state: RootState) => state.workflowSetup!.elementMetadata.elements,
  data => {
    const elements = data.map(e => {
      return {
        name: e.name,
        options: KeepTextElementsDLO.includes(e.name)
          ? e.options?.map(opt => ({
              code: opt.value,
              text: `${opt.value}`
            }))
          : FilterByWLElements.includes(e.name)
          ? e.options
              ?.filter(
                optFilter =>
                  optFilter.workFlow === 'designLoanOffers' ||
                  !optFilter.workFlow
              )
              .map(opt => ({
                code: opt.value,
                text: opt.value
                  ? `${opt.value}${
                      opt.description ? ` - ${opt.description}` : ''
                    }`
                  : `${opt.description}`
              }))
          : e.options?.map(opt => ({
              code: opt.value,
              text:
                opt.value && opt.value !== opt.description
                  ? `${opt.value}${
                      opt.description ? ` - ${opt.description}` : ''
                    }`
                  : `${opt.description}`
            }))
      } as {
        name: string;
        options: RefData[];
      };
    });

    const promotionDescription =
      elements.find(
        e => e.name === MethodFieldParameterEnum.PromotionDescription
      )?.options || [];
    const promotionAlternateDescription =
      elements.find(
        e => e.name === MethodFieldParameterEnum.PromotionAlternateDescription
      )?.options || [];
    const promotionDescriptionDisplayOptions =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.PromotionDescriptionDisplayOptions
      )?.options || [];
    const promotionStatementTextIDDTDD =
      elements.find(
        e => e.name === MethodFieldParameterEnum.PromotionStatementTextIDDTDD
      )?.options || [];
    const promotionStatementTextIDDTDX =
      elements.find(
        e => e.name === MethodFieldParameterEnum.PromotionStatementTextIDDTDX
      )?.options || [];
    const promotionStatementTextIDStandard =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.PromotionStatementTextIDStandard
      )?.options || [];
    const promotionStatementTextControl =
      elements.find(
        e => e.name === MethodFieldParameterEnum.PromotionStatementTextControl
      )?.options || [];
    const promotionReturnApplicationOption =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.PromotionReturnApplicationOption
      )?.options || [];
    const promotionPayoffExceptionOption =
      elements.find(
        e => e.name === MethodFieldParameterEnum.PromotionPayoffExceptionOption
      )?.options || [];
    const loanBalanceClassification =
      elements.find(
        e => e.name === MethodFieldParameterEnum.LoanBalanceClassification
      )?.options || [];
    const payoffExceptionMethod =
      elements.find(
        e => e.name === MethodFieldParameterEnum.PayoffExceptionMethod
      )?.options || [];
    const cashAdvanceItemFeesMethod =
      elements.find(
        e => e.name === MethodFieldParameterEnum.CashAdvanceItemFeesMethod
      )?.options || [];
    const merchantItemFeesMethod =
      elements.find(
        e => e.name === MethodFieldParameterEnum.MerchantItemFeesMethod
      )?.options || [];
    const creditApplicationGroup =
      elements.find(
        e => e.name === MethodFieldParameterEnum.CreditApplicationGroup
      )?.options || [];
    const promotionGroupIdentifier =
      elements.find(
        e => e.name === MethodFieldParameterEnum.PromotionGroupIdentifier
      )?.options || [];
    const standardMinimumPaymentCalculationCode =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.StandardMinimumPaymentCalculationCode
      )?.options || [];
    const rulesMinimumPaymentDueMethod =
      elements.find(
        e => e.name === MethodFieldParameterEnum.RulesMinimumPaymentDueMethod
      )?.options || [];
    const standardMinimumPaymentsRate =
      elements.find(
        e => e.name === MethodFieldParameterEnum.StandardMinimumPaymentsRate
      )?.options || [];
    const standardMinimumPaymentRoundingOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.StandardMinimumPaymentRoundingOptions
      )?.options || [];

    const annualInterestRate =
      elements.find(e => e.name === MethodFieldParameterEnum.AnnualInterestRate)
        ?.options || [];

    const payoutPeriod =
      elements.find(e => e.name === MethodFieldParameterEnum.PayoutPeriod)
        ?.options || [];

    const merchantDiscount =
      elements.find(e => e.name === MethodFieldParameterEnum.MerchantDiscount)
        ?.options || [];

    const merchantDiscountCalculationCode =
      elements.find(
        e => e.name === MethodFieldParameterEnum.MerchantDiscountCalculationCode
      )?.options || [];

    const returnToRevolvingBaseInterest =
      elements.find(
        e => e.name === MethodFieldParameterEnum.ReturnToRevolvingBaseInterest
      )?.options || [];

    const applyPenaltyPricingOnLoanOffer =
      elements.find(
        e => e.name === MethodFieldParameterEnum.ApplyPenaltyPricingOnLoanOffer
      )?.options || [];

    const defaultInterestRateMethodOverride =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DefaultInterestRateMethodOverride
      )?.options || [];

    const interestCalculationMethodOverride =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.InterestCalculationMethodOverride
      )?.options || [];

    const introductoryMessageDisplayControl =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.IntroductoryMessageDisplayControl
      )?.options || [];

    const positiveAmortizationUsageCode =
      elements.find(
        e => e.name === MethodFieldParameterEnum.PositiveAmortizationUsageCode
      )?.options || [];

    const introductoryAnnualRate =
      elements.find(
        e => e.name === MethodFieldParameterEnum.IntroductoryAnnualRate
      )?.options || [];

    const introductoryAnnualRateFixedEndDate =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.IntroductoryAnnualRateFixedEndDate
      )?.options || [];

    const introductoryRateNumberOfDays =
      elements.find(
        e => e.name === MethodFieldParameterEnum.IntroductoryRateNumberOfDays
      )?.options || [];

    const introductoryRateNumberOfCycles =
      elements.find(
        e => e.name === MethodFieldParameterEnum.IntroductoryRateNumberOfCycles
      )?.options || [];

    const introductoryPeriodStatementTextID =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.IntroductoryPeriodStatementTextID
      )?.options || [];

    const sameAsCashLoanOfferNumberOfCycles =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.SameAsCashLoanOfferNumberOfCycles
      )?.options || [];

    const accrueInterestOnUnbilledInterest =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.AccrueInterestOnUnbilledInterest
      )?.options || [];

    const delayPaymentForStatementCycles =
      elements.find(
        e => e.name === MethodFieldParameterEnum.DelayPaymentForStatementCycles
      )?.options || [];

    const delayPaymentForMonths =
      elements.find(
        e => e.name === MethodFieldParameterEnum.DelayPaymentForMonths
      )?.options || [];

    return {
      promotionDescription,
      promotionAlternateDescription,
      promotionDescriptionDisplayOptions,
      promotionStatementTextIDDTDD,
      promotionStatementTextIDDTDX,
      promotionStatementTextIDStandard,
      promotionStatementTextControl,
      promotionReturnApplicationOption,
      promotionPayoffExceptionOption,
      loanBalanceClassification,
      payoffExceptionMethod,
      cashAdvanceItemFeesMethod,
      merchantItemFeesMethod,
      creditApplicationGroup,
      promotionGroupIdentifier,
      standardMinimumPaymentCalculationCode,
      rulesMinimumPaymentDueMethod,
      standardMinimumPaymentsRate,
      standardMinimumPaymentRoundingOptions,
      annualInterestRate,
      payoutPeriod,
      merchantDiscount,
      merchantDiscountCalculationCode,
      returnToRevolvingBaseInterest,
      applyPenaltyPricingOnLoanOffer,
      defaultInterestRateMethodOverride,
      interestCalculationMethodOverride,
      introductoryMessageDisplayControl,
      positiveAmortizationUsageCode,
      introductoryAnnualRate,
      introductoryAnnualRateFixedEndDate,
      introductoryRateNumberOfDays,
      introductoryRateNumberOfCycles,
      introductoryPeriodStatementTextID,
      sameAsCashLoanOfferNumberOfCycles,
      accrueInterestOnUnbilledInterest,
      delayPaymentForStatementCycles,
      delayPaymentForMonths
    };
  }
);

export const useSelectElementMetadataForDLO = () => {
  return useSelector<
    RootState,
    {
      promotionDescription: RefData[];
      promotionAlternateDescription: RefData[];
      promotionDescriptionDisplayOptions: RefData[];
      promotionStatementTextIDDTDD: RefData[];
      promotionStatementTextIDDTDX: RefData[];
      promotionStatementTextIDStandard: RefData[];
      promotionStatementTextControl: RefData[];
      promotionReturnApplicationOption: RefData[];
      promotionPayoffExceptionOption: RefData[];
      loanBalanceClassification: RefData[];
      payoffExceptionMethod: RefData[];
      cashAdvanceItemFeesMethod: RefData[];
      merchantItemFeesMethod: RefData[];
      creditApplicationGroup: RefData[];
      promotionGroupIdentifier: RefData[];
      standardMinimumPaymentCalculationCode: RefData[];
      rulesMinimumPaymentDueMethod: RefData[];
      standardMinimumPaymentsRate: RefData[];
      standardMinimumPaymentRoundingOptions: RefData[];
      annualInterestRate: RefData[];
      payoutPeriod: RefData[];
      merchantDiscount: RefData[];
      merchantDiscountCalculationCode: RefData[];
      returnToRevolvingBaseInterest: RefData[];
      applyPenaltyPricingOnLoanOffer: RefData[];
      defaultInterestRateMethodOverride: RefData[];
      interestCalculationMethodOverride: RefData[];
      introductoryMessageDisplayControl: RefData[];
      positiveAmortizationUsageCode: RefData[];
      introductoryAnnualRate: RefData[];
      introductoryAnnualRateFixedEndDate: RefData[];
      introductoryRateNumberOfDays: RefData[];
      introductoryRateNumberOfCycles: RefData[];
      introductoryPeriodStatementTextID: RefData[];
      sameAsCashLoanOfferNumberOfCycles: RefData[];
      accrueInterestOnUnbilledInterest: RefData[];
      delayPaymentForStatementCycles: RefData[];
      delayPaymentForMonths: RefData[];
    }
  >(elementMetadataForDLOSelector);
};
