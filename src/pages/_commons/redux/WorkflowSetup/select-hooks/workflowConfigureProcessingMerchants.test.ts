import { renderHook } from '@testing-library/react-hooks';
import { WorkflowMetadataAll } from 'app/fixtures/workflow-metadata';
import { WorkflowSetupRootState } from 'app/types';
import * as reactRedux from 'react-redux';
import {
  useExportMerchantsStatus,
  useMerchantList,
  useMerchantListStatus,
  useSelectElementsForConfigProcessingMerchants
} from '.';

describe('redux-store > workflow-setup > select-hooks ', () => {
  let selectorMock: jest.SpyInstance;
  const mockSelectorWorkflowSetup = (
    workflowSetup:
      | Pick<WorkflowSetupRootState, 'workflowFormSteps'>
      | Pick<WorkflowSetupRootState, 'workflowSetup'>
      | Pick<WorkflowSetupRootState, 'workflowMethods'>
      | Pick<WorkflowSetupRootState, 'workflowInstance'>
      | Pick<WorkflowSetupRootState, 'elementMetadata'>
      | Pick<WorkflowSetupRootState, 'workflowChangeSets'>
      | Pick<WorkflowSetupRootState, 'createNewChange'>
      | Pick<WorkflowSetupRootState, 'parameterData'>
  ) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector => selector({ workflowSetup }));
  };

  afterEach(() => {
    selectorMock.mockClear();
  });

  it('test on selector useMerchantListStatus ', () => {
    mockSelectorWorkflowSetup({
      merchantData: {
        loading: true,
        error: false
      }
    } as any);

    const { result } = renderHook(() => useMerchantListStatus());

    expect(result.current.loading).toEqual(true);
    expect(result.current.error).toEqual(false);
  });

  it('test on selector useExportMerchantsStatus  ', () => {
    mockSelectorWorkflowSetup({
      exportMerchant: {
        loading: true,
        error: false
      }
    } as any);

    const { result } = renderHook(() => useExportMerchantsStatus());

    expect(result.current.loading).toEqual(true);
    expect(result.current.error).toEqual(false);
  });

  it('test on selector useSelectElementsForConfigProcessingMerchants ', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: {
        elements: [
          {
            name: 'system.principal',
            options: [
              { value: '00-11-22' },
              { value: '00-11', description: 'desc' }
            ]
          },
          { name: 'assessment.code', options: [{}] }
        ]
      }
    } as any);

    const { rerender, result } = renderHook(() =>
      useSelectElementsForConfigProcessingMerchants()
    );

    mockSelectorWorkflowSetup({
      elementMetadata: { elements: [] }
    } as any);
    rerender();
    expect(result.current.sparkExclusionIndicator.length).toEqual(0);
  });

  it('test on selector useMerchantList ', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: { elements: WorkflowMetadataAll }
    } as any);

    const { rerender, result } = renderHook(() => useMerchantList());

    mockSelectorWorkflowSetup({
      elementMetadata: { elements: [] }
    } as any);
    rerender();
    expect(result.current).toEqual(undefined);
  });
});
