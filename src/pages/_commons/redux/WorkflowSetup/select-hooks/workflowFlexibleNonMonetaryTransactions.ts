import { createSelector } from '@reduxjs/toolkit';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import { ParameterCode as FNMTParameterCode } from 'pages/WorkflowFlexibleNonMonetaryTransactions/SelectNonMonetaryTransactionsStep/type';
import { useSelector } from 'react-redux';
import { findByName, mapNameAndTextToText } from './_helper';

const elementMetadataForNonMonetaryTransactionsSelector = createSelector(
  (state: RootState) => state.workflowSetup!.elementMetadata.elements,
  data => {
    const elements = data.map(e => {
      return {
        name: e.name,
        options: e.options?.map(opt => ({
          ...opt,
          code: opt.value,
          text: !isEmpty(opt.description) ? opt.description : opt.value
        }))
      } as WorkflowElement;
    });

    const rawTransactions =
      elements
        .find(findByName(FNMTParameterCode.TransactionId))
        ?.options?.map(mapNameAndTextToText) || [];

    const transactionMapping: Record<string, RefData[]> =
      rawTransactions.reduce(
        (prev, curr) => ({
          ...prev,
          [curr.code]:
            elements.find(findByName(`${curr.code}.transaction`))?.options || []
        }),
        {}
      );
    const transactions = rawTransactions.map(option => ({
      ...option,
      id: option.code,
      name: option.text,
      parameters: transactionMapping[option.code]
    }));
    return { transactions };
  }
);
export const useSelectElementsForNonMonetaryTransactions = () => {
  return useSelector<RootState, { transactions: RefData[] }>(
    elementMetadataForNonMonetaryTransactionsSelector
  );
};
