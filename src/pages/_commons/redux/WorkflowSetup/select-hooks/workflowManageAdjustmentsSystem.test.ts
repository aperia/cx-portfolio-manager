import { renderHook } from '@testing-library/react-hooks';
import { WorkflowMetadataAll } from 'app/fixtures/workflow-metadata';
import { WorkflowSetupRootState } from 'app/types';
import * as reactRedux from 'react-redux';
import {
  useExportShellProfileStatus,
  useOcsShellProfileStatus,
  useProfileTransactionsStatus,
  useQueueProfileStatus,
  useSelectElementsForManageAdjustmentSystem
} from './workflowManageAdjustmentsSystem';

describe('redux-store > workflow-setup > select-hooks ', () => {
  let selectorMock: jest.SpyInstance;
  const mockSelectorWorkflowSetup = (
    workflowSetup:
      | Pick<WorkflowSetupRootState, 'workflowFormSteps'>
      | Pick<WorkflowSetupRootState, 'workflowSetup'>
      | Pick<WorkflowSetupRootState, 'workflowMethods'>
      | Pick<WorkflowSetupRootState, 'workflowInstance'>
      | Pick<WorkflowSetupRootState, 'elementMetadata'>
      | Pick<WorkflowSetupRootState, 'workflowChangeSets'>
      | Pick<WorkflowSetupRootState, 'createNewChange'>
      | Pick<WorkflowSetupRootState, 'parameterData'>
  ) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector => selector({ workflowSetup }));
  };

  afterEach(() => {
    selectorMock.mockClear();
  });

  it('test on selector useProfileTransactionsStatus  ', () => {
    mockSelectorWorkflowSetup({
      profileTransactions: {
        loading: true,
        error: false
      }
    } as any);

    const { result } = renderHook(() => useProfileTransactionsStatus());

    expect(result.current.loading).toEqual(true);
    expect(result.current.error).toEqual(false);
  });

  it('test on selector useExportShellProfileStatus  ', () => {
    mockSelectorWorkflowSetup({
      exportShellProfile: {
        loading: true,
        error: false
      }
    } as any);

    const { result } = renderHook(() => useExportShellProfileStatus());

    expect(result.current.loading).toEqual(true);
    expect(result.current.error).toEqual(false);
  });

  it('test on selector useOcsShellProfileStatus  ', () => {
    mockSelectorWorkflowSetup({
      ocsShellProfile: {
        loading: true,
        error: false
      }
    } as any);

    const { result } = renderHook(() => useOcsShellProfileStatus());

    expect(result.current.loading).toEqual(true);
    expect(result.current.error).toEqual(false);
  });

  it('test on selector useQueueProfileStatus  ', () => {
    mockSelectorWorkflowSetup({
      queueProfile: {
        loading: true,
        error: false
      }
    } as any);

    const { result } = renderHook(() => useQueueProfileStatus());

    expect(result.current.loading).toEqual(true);
    expect(result.current.error).toEqual(false);
  });

  it('test on selector useSelectElementsForManageAdjustmentSystem ', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: { elements: WorkflowMetadataAll }
    } as any);

    const { rerender, result } = renderHook(() =>
      useSelectElementsForManageAdjustmentSystem()
    );

    mockSelectorWorkflowSetup({
      elementMetadata: { elements: [] }
    } as any);
    rerender();
    expect(result.current.transactions.length).toEqual(0);
  });
});
