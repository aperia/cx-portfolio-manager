import { createSelector } from '@reduxjs/toolkit';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import { orderBy } from 'app/_libraries/_dls/lodash';
import { useSelector } from 'react-redux';

const elementMetadataForChangeInTermSelector = createSelector(
  (state: RootState) => state.workflowSetup!.elementMetadata.elements,
  data => {
    const elements = data.map(e => {
      return {
        name: e.name,
        options: orderBy(
          e.options?.map(opt => {
            const des = opt.description ? ` - ${opt.description}` : '';
            const text = opt.value ? `${opt.value}${des}` : opt.description;

            return {
              code: opt.value || '',
              text: text,
              orderText: opt.value ? text : '0',
              selected: opt.selected
            };
          }),
          ['orderText'],
          ['asc']
        )
      } as {
        name: string;
        options: RefData[];
      };
    });
    const changeInTermsBasicSettingsCisMemoSettings =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ChangeInTermsBasicSettingsCisMemoSettings
      )?.options || [];

    const changeInTermsBasicSettingsTermsConditionsTable =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ChangeInTermsBasicSettingsTermsConditionsTable
      )?.options || [];

    const changeInTermsBasicSettingsCitPcs =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.ChangeInTermsBasicSettingsCitPcs
      )?.options || [];

    const changeInTermsBasicSettingsEnableCit =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ChangeInTermsBasicSettingsEnableCit
      )?.options || [];

    const changeInTermsCommunicationLetterIdForActice =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ChangeInTermsCommunicationLetterIdForActice
      )?.options || [];

    const changeInTermsCommunicationLetterIdForInActice =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ChangeInTermsCommunicationLetterIdForInActice
      )?.options || [];

    const changeInTermsMethodDisclosureCpIcBp =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcBp
      )?.options || [];

    const changeInTermsMethodDisclosureCpIcId =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcId
      )?.options || [];

    const changeInTermsMethodDisclosureCpIcIi =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIi
      )?.options || [];

    const changeInTermsMethodDisclosureCpIcIm =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIm
      )?.options || [];

    const changeInTermsMethodDisclosureCpIcIp =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIp
      )?.options || [];
    const changeInTermsMethodDisclosureCpIcIr =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcIr
      )?.options || [];

    const changeInTermsMethodDisclosureCpIcMe =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcMe
      )?.options || [];

    const changeInTermsMethodDisclosureCpIcMf =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcMf
      )?.options || [];

    const changeInTermsMethodDisclosureCpIcVi =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIcVi
      )?.options || [];

    const changeInTermsMethodDisclosureCpIoAc =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoAc
      )?.options || [];

    const changeInTermsMethodDisclosureCpIoCi =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoCi
      )?.options || [];

    const changeInTermsMethodDisclosureCpIoMc =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoMc
      )?.options || [];

    const changeInTermsMethodDisclosureCpIoMi =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpIoMi
      )?.options || [];

    const changeInTermsMethodDisclosureCpPfLc =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPfLc
      )?.options || [];

    const changeInTermsMethodDisclosureCpPfOc =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPfOc
      )?.options || [];

    const changeInTermsMethodDisclosureCpPfRc =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPfRc
      )?.options || [];

    const changeInTermsMethodDisclosureCpPoMp =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ChangeInTermsMethodDisclosureCpPoMp
      )?.options || [];

    const changeInTermsPenaltyPricingIndicator =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ChangeInTermsPenaltyPricingIndicator
      )?.options || [];

    const changeInTermsProtectedBalancesMethodForCash =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ChangeInTermsProtectedBalancesMethodForCash
      )?.options || [];

    const changeInTermsProtectedBalancesMethodForMerchandise =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ChangeInTermsProtectedBalancesMethodForMerchandise
      )?.options || [];

    const changeInTermsProtectedBalancesMethodForNonInterest =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ChangeInTermsProtectedBalancesMethodForNonInterest
      )?.options || [];

    const changeInTermsTimmingCitnumber =
      elements.find(
        e => e.name === MethodFieldParameterEnum.ChangeInTermsTimmingCitnumber
      )?.options || [];

    const changeInTermsBasicSettingsCitPriorityOrder =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ChangeInTermsBasicSettingsCitPriorityOrder
      )?.options || [];

    const protectedBalancesAttributesCreditApplicationGroup =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ProtectedBalancesAttributesCreditApplicationGroup
      )?.options || [];

    const protectedBalancesAttributesMethodToProtectInterestDefault1 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault1
      )?.options || [];

    const protectedBalancesAttributesMethodToProtectInterestDefault2 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault2
      )?.options || [];

    const protectedBalancesAttributesMethodToProtectInterestDefault3 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestDefault3
      )?.options || [];

    const protectedBalancesAttributesMethodToProtectInterestMethod1 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod1
      )?.options || [];

    const protectedBalancesAttributesMethodToProtectInterestMethod2 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod2
      )?.options || [];

    const protectedBalancesAttributesMethodToProtectInterestMethod3 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectInterestMethod3
      )?.options || [];

    const protectedBalancesAttributesMethodToProtectMinimumPayment1 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment1
      )?.options || [];

    const protectedBalancesAttributesMethodToProtectMinimumPayment2 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment2
      )?.options || [];

    const protectedBalancesAttributesMethodToProtectMinimumPayment3 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ProtectedBalancesAttributesMethodToProtectMinimumPayment3
      )?.options || [];

    const protectedBalancesAttributesDescriptionDisplayOption =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ProtectedBalancesAttributesDescriptionDisplayOption
      )?.options || [];

    const protectedBalancesAttributesMessageText =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ProtectedBalancesAttributesMessageText
      )?.options || [];

    const protectedBalancesAttributesMessageOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ProtectedBalancesAttributesMessageOptions
      )?.options || [];

    const changeInTermsDisclosedMethodOverridesCpIcIdInterestDefaultsOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ChangeInTermsDisclosedMethodOverridesCpIcIdInterestDefaults
      )?.options || [];

    const changeInTermsReasonCode =
      elements.find(
        e => e.name === MethodFieldParameterEnum.ChangeInTermsReasonCode
      )?.options || [];

    const changeInTermsDisclosedMethodOverridesCpIcIdInterestMethodsOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ChangeInTermsDisclosedMethodOverridesCpIcIdInterestMethods
      )?.options || [];

    const protectedBalancesAttributesProtectBalancesForPenaltyPricingOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ProtectedBalancesAttributesProtectBalancesForPenaltyPricing
      )?.options || [];

    const protectedBalancesAttributesDelayIntroTextIDOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ProtectedBalancesAttributesDelayIntroTextID
      )?.options || [];

    const protectedBalancesAttributesPBBalanceTransferOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ProtectedBalancesAttributesPBBalanceTransfer
      )?.options || [];

    const protectedBalancesAttributesProtectedBalanceOverrideCodeOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ProtectedBalancesAttributesProtectedBalanceOverrideCode
      )?.options || [];

    const protectedBalancesAttributesGroupIdentifierOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ProtectedBalancesAttributesGroupIdentifier
      )?.options || [];

    return {
      changeInTermsBasicSettingsCisMemoSettings,
      changeInTermsBasicSettingsTermsConditionsTable,
      changeInTermsBasicSettingsCitPcs,
      changeInTermsBasicSettingsEnableCit,
      changeInTermsCommunicationLetterIdForActice,
      changeInTermsCommunicationLetterIdForInActice,
      changeInTermsMethodDisclosureCpIcBp,
      changeInTermsMethodDisclosureCpIcId,
      changeInTermsMethodDisclosureCpIcIi,
      changeInTermsMethodDisclosureCpIcIm,
      changeInTermsMethodDisclosureCpIcIp,
      changeInTermsMethodDisclosureCpIcIr,
      changeInTermsMethodDisclosureCpIcMe,
      changeInTermsMethodDisclosureCpIcMf,
      changeInTermsMethodDisclosureCpIcVi,
      changeInTermsMethodDisclosureCpIoAc,
      changeInTermsMethodDisclosureCpIoCi,
      changeInTermsMethodDisclosureCpIoMc,
      changeInTermsMethodDisclosureCpIoMi,
      changeInTermsMethodDisclosureCpPfLc,
      changeInTermsMethodDisclosureCpPfOc,
      changeInTermsMethodDisclosureCpPfRc,
      changeInTermsMethodDisclosureCpPoMp,
      changeInTermsPenaltyPricingIndicator,
      changeInTermsProtectedBalancesMethodForCash,
      changeInTermsProtectedBalancesMethodForMerchandise,
      changeInTermsProtectedBalancesMethodForNonInterest,
      changeInTermsTimmingCitnumber,
      changeInTermsBasicSettingsCitPriorityOrder,
      protectedBalancesAttributesCreditApplicationGroup,
      protectedBalancesAttributesMethodToProtectInterestDefault1,
      protectedBalancesAttributesMethodToProtectInterestDefault2,
      protectedBalancesAttributesMethodToProtectInterestDefault3,
      protectedBalancesAttributesMethodToProtectInterestMethod1,
      protectedBalancesAttributesMethodToProtectInterestMethod2,
      protectedBalancesAttributesMethodToProtectInterestMethod3,
      protectedBalancesAttributesMethodToProtectMinimumPayment1,
      protectedBalancesAttributesMethodToProtectMinimumPayment2,
      protectedBalancesAttributesMethodToProtectMinimumPayment3,
      protectedBalancesAttributesDescriptionDisplayOption,
      protectedBalancesAttributesMessageText,
      protectedBalancesAttributesMessageOptions,
      changeInTermsDisclosedMethodOverridesCpIcIdInterestDefaultsOptions,
      changeInTermsDisclosedMethodOverridesCpIcIdInterestMethodsOptions,
      protectedBalancesAttributesProtectBalancesForPenaltyPricingOptions,
      protectedBalancesAttributesDelayIntroTextIDOptions,
      protectedBalancesAttributesPBBalanceTransferOptions,
      protectedBalancesAttributesProtectedBalanceOverrideCodeOptions,
      protectedBalancesAttributesGroupIdentifierOptions,
      changeInTermsReasonCode
    };
  }
);

export const useSelectElementMetadataForChangeInTerm = () => {
  return useSelector<
    RootState,
    {
      changeInTermsBasicSettingsCisMemoSettings: RefData[];
      changeInTermsBasicSettingsTermsConditionsTable: RefData[];
      changeInTermsBasicSettingsCitPcs: RefData[];
      changeInTermsBasicSettingsEnableCit: RefData[];
      changeInTermsCommunicationLetterIdForActice: RefData[];
      changeInTermsCommunicationLetterIdForInActice: RefData[];
      changeInTermsMethodDisclosureCpIcBp: RefData[];
      changeInTermsMethodDisclosureCpIcId: RefData[];
      changeInTermsMethodDisclosureCpIcIi: RefData[];
      changeInTermsMethodDisclosureCpIcIm: RefData[];
      changeInTermsMethodDisclosureCpIcIp: RefData[];
      changeInTermsMethodDisclosureCpIcIr: RefData[];
      changeInTermsMethodDisclosureCpIcMe: RefData[];
      changeInTermsMethodDisclosureCpIcMf: RefData[];
      changeInTermsMethodDisclosureCpIcVi: RefData[];
      changeInTermsMethodDisclosureCpIoAc: RefData[];
      changeInTermsMethodDisclosureCpIoCi: RefData[];
      changeInTermsMethodDisclosureCpIoMc: RefData[];
      changeInTermsMethodDisclosureCpIoMi: RefData[];
      changeInTermsMethodDisclosureCpPfLc: RefData[];
      changeInTermsMethodDisclosureCpPfOc: RefData[];
      changeInTermsMethodDisclosureCpPfRc: RefData[];
      changeInTermsMethodDisclosureCpPoMp: RefData[];
      changeInTermsPenaltyPricingIndicator: RefData[];
      changeInTermsProtectedBalancesMethodForCash: RefData[];
      changeInTermsProtectedBalancesMethodForMerchandise: RefData[];
      changeInTermsProtectedBalancesMethodForNonInterest: RefData[];
      changeInTermsTimmingCitnumber: RefData[];
      changeInTermsBasicSettingsCitPriorityOrder: RefData[];
      protectedBalancesAttributesCreditApplicationGroup: RefData[];
      protectedBalancesAttributesMethodToProtectInterestDefault1: RefData[];
      protectedBalancesAttributesMethodToProtectInterestDefault2: RefData[];
      protectedBalancesAttributesMethodToProtectInterestDefault3: RefData[];
      protectedBalancesAttributesMethodToProtectInterestMethod1: RefData[];
      protectedBalancesAttributesMethodToProtectInterestMethod2: RefData[];
      protectedBalancesAttributesMethodToProtectInterestMethod3: RefData[];
      protectedBalancesAttributesMethodToProtectMinimumPayment1: RefData[];
      protectedBalancesAttributesMethodToProtectMinimumPayment2: RefData[];
      protectedBalancesAttributesMethodToProtectMinimumPayment3: RefData[];
      protectedBalancesAttributesDescriptionDisplayOption: RefData[];
      protectedBalancesAttributesMessageText: RefData[];
      protectedBalancesAttributesMessageOptions: RefData[];
      //Advance
      changeInTermsReasonCode: RefData[];
      changeInTermsDisclosedMethodOverridesCpIcIdInterestDefaultsOptions: RefData[];
      changeInTermsDisclosedMethodOverridesCpIcIdInterestMethodsOptions: RefData[];
      protectedBalancesAttributesProtectBalancesForPenaltyPricingOptions: RefData[];
      protectedBalancesAttributesDelayIntroTextIDOptions: RefData[];
      protectedBalancesAttributesPBBalanceTransferOptions: RefData[];
      protectedBalancesAttributesProtectedBalanceOverrideCodeOptions: RefData[];
      protectedBalancesAttributesGroupIdentifierOptions: RefData[];
    }
  >(elementMetadataForChangeInTermSelector);
};
