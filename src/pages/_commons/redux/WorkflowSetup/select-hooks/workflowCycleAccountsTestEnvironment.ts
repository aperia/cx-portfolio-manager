import { createSelector } from '@reduxjs/toolkit';
import { MethodFieldParameterEnum as ParameterEnum } from 'app/constants/enums';
import { useSelector } from 'react-redux';
import { findByName } from './_helper';

const elementMetadataCycleAccountSelector = createSelector(
  (state: RootState) => state.workflowSetup!.elementMetadata.elements,
  data => {
    const elements = data?.map(e => {
      const options =
        e.options?.map(opt => {
          let text = '';
          switch (e.name) {
            case ParameterEnum.CycleAccountsTestEnvironmentCode:
              text = opt?.value
                ? `${opt.value} - ${opt.description}`
                : opt.description;
              break;
            case ParameterEnum.CycleAccountsTestEnvironmentTransaction:
              text = opt?.description;
              break;
          }

          return {
            code: opt?.value || '',
            text: text,
            description: opt?.description,
            type: opt?.type
          };
        }) || [];
      return {
        name: e.name,
        options: options
      } as {
        name: string;
        options: any[];
      };
    });

    const cycleAccountsTestEnvironmentTransaction =
      elements?.find(
        findByName(ParameterEnum.CycleAccountsTestEnvironmentTransaction)
      )?.options || [];

    const cycleAccountsTestEnvironmentCode =
      elements?.find(findByName(ParameterEnum.CycleAccountsTestEnvironmentCode))
        ?.options || [];

    return {
      cycleAccountsTestEnvironmentTransaction,
      cycleAccountsTestEnvironmentCode
    };
  }
);

export const useSelectElementMetadataCycleAccount = () => {
  return useSelector<
    RootState,
    {
      cycleAccountsTestEnvironmentTransaction: (RefData & { type?: string })[];
      cycleAccountsTestEnvironmentCode: RefData[];
    }
  >(elementMetadataCycleAccountSelector);
};
