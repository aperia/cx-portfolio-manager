import { isEmpty } from 'lodash';

export const mapValueAndDescToRefItem = ({
  value: _value,
  description,
  ...rest
}: {
  value?: string;
  description?: string;
}) => {
  const value = _value?.trim();
  return {
    ...rest,
    code: value,
    text: !isEmpty(description) ? description : value
  };
};

export const mapCodeAndTextToText = ({
  code: _code,
  text,
  ...rest
}: RefData) => {
  const code = _code?.trim();

  return {
    ...rest,
    code,
    text: !isEmpty(code) ? `${code} - ${text}` : text
  };
};
export const findByName = (value: string) => (element: WorkflowElement) =>
  element.name === value;

export const mapNameAndTextToText = ({
  code: _code,
  text,
  name,
  ...rest
}: any) => {
  const code = _code?.trim();

  return {
    ...rest,
    code,
    text: !isEmpty(name) ? `${name} ${text}` : text
  };
};
