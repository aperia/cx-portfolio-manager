import { createSelector } from '@reduxjs/toolkit';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import { useSelector } from 'react-redux';
import { mapCodeAndTextToText } from './_helper';

const elementMetadataForAssignPricingStrategiesSelector = createSelector(
  (state: RootState) => state.workflowSetup!.elementMetadata.elements,
  data => {
    const elements = data.map(e => {
      return {
        name: e.name,
        options: e.options?.map(opt => ({
          ...opt,
          code: opt.value,
          text: !isEmpty(opt.description) ? opt.description : opt.value
        }))
      } as {
        name: string;
        options: (AssignPricingStrategiesMetaDataOption & RefData)[];
      };
    });

    const configuration =
      elements
        .find(e => e.name === 'transaction.id')
        ?.options?.filter(x => !x.workFlow || x?.workFlow === 'apsmoa') || [];

    const reallocate =
      elements.find(e => e.name === 'nm.168.entry.format')?.options || [];

    const portfolio =
      elements.find(e => e.name === 'portfolio.id')?.options || [];
    const strategy =
      elements.find(e => e.name === 'strategy.id')?.options || [];
    const status =
      elements
        .find(e => e.name === 'status')
        ?.options?.map(mapCodeAndTextToText) || [];
    const type =
      elements
        .find(e => e.name === 'type')
        ?.options?.map(mapCodeAndTextToText) || [];
    const subtraction =
      elements.find(e => e.name === 'nm.738.entry.format')?.options || [];
    const subtransactionCode =
      elements
        .find(e => e.name === 'subtransaction.code')
        ?.options?.map(mapCodeAndTextToText) || [];
    const methodOverrideID =
      elements
        .find(e => e.name === 'method.override.id')
        ?.options?.map(mapCodeAndTextToText) || [];
    const lockUnlock =
      elements
        .find(e => e.name === 'lock.unlock')
        ?.options?.map(mapCodeAndTextToText) || [];
    const allocateImmediately =
      elements
        .find(e => e.name === 'allocate.immediately')
        ?.options?.map(mapCodeAndTextToText) || [];
    const serviceSubjectSection =
      elements
        .find(e => e.name === 'method.override.svc.subject.section')
        ?.options?.map(mapCodeAndTextToText) || [];
    const lock =
      elements
        .find(e => e.name === 'lock')
        ?.options?.map(mapCodeAndTextToText) || [];
    const reallocateNM738 =
      elements
        .find(e => e.name === 'reallocate')
        ?.options?.map(mapCodeAndTextToText) || [];
    const aQTable = elements.find(e => e.name === 'aq.table')?.options || [];

    return {
      configuration,
      reallocate,
      portfolio,
      strategy,
      status,
      type,
      subtraction,
      subtransactionCode,
      methodOverrideID,
      lockUnlock,
      allocateImmediately,
      serviceSubjectSection,
      lock,
      reallocateNM738,
      aQTable
    };
  }
);

export const useSelectElementMetadataForAssignPricingStrategies = () => {
  return useSelector<
    RootState,
    {
      configuration: (AssignPricingStrategiesMetaDataOption & RefData)[];
      reallocate: (AssignPricingStrategiesMetaDataOption & RefData)[];
      portfolio: (AssignPricingStrategiesMetaDataOption & RefData)[];
      strategy: (AssignPricingStrategiesMetaDataOption & RefData)[];
      status: (AssignPricingStrategiesMetaDataOption & RefData)[];
      type: (AssignPricingStrategiesMetaDataOption & RefData)[];
      subtraction: (AssignPricingStrategiesMetaDataOption & RefData)[];
      subtransactionCode: (AssignPricingStrategiesMetaDataOption & RefData)[];
      methodOverrideID: (AssignPricingStrategiesMetaDataOption & RefData)[];
      lockUnlock: (AssignPricingStrategiesMetaDataOption & RefData)[];
      allocateImmediately: (AssignPricingStrategiesMetaDataOption & RefData)[];
      serviceSubjectSection: (AssignPricingStrategiesMetaDataOption &
        RefData)[];
      lock: (AssignPricingStrategiesMetaDataOption & RefData)[];
      reallocateNM738: (AssignPricingStrategiesMetaDataOption & RefData)[];
      aQTable: (AssignPricingStrategiesMetaDataOption & RefData)[];
    }
  >(elementMetadataForAssignPricingStrategiesSelector);
};
