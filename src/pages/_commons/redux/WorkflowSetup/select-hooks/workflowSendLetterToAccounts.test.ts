import { renderHook } from '@testing-library/react-hooks';
import { WorkflowSetupRootState, WorkflowSetupState } from 'app/types';
import * as reactRedux from 'react-redux';
import { useSelectLetters } from '.';

describe('redux-store > workflow-setup > select-hooks ', () => {
  let selectorMock: jest.SpyInstance;
  const mockSelectorWorkflowSetup = (
    workflowSetup:
      | Pick<WorkflowSetupRootState, 'workflowFormSteps'>
      | Pick<WorkflowSetupRootState, 'workflowSetup'>
      | Pick<WorkflowSetupRootState, 'workflowMethods'>
      | Pick<WorkflowSetupRootState, 'workflowInstance'>
      | Pick<WorkflowSetupRootState, 'elementMetadata'>
      | Pick<WorkflowSetupRootState, 'workflowChangeSets'>
      | Pick<WorkflowSetupRootState, 'createNewChange'>
      | Pick<WorkflowSetupRootState, 'parameterData'>
  ) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector => selector({ workflowSetup }));
  };

  afterEach(() => {
    selectorMock.mockClear();
  });

  it('test on selector useSelectLetters', () => {
    mockSelectorWorkflowSetup({
      workflowSetup: {
        lettersData: { letters: [] }
      } as WorkflowSetupState
    });

    const { result } = renderHook(() => useSelectLetters());

    expect(result.current).toEqual(undefined);
  });
});
