import { createSelector } from '@reduxjs/toolkit';
import { OCSShellDataState } from 'app/types';
import { orderBy } from 'app/_libraries/_dls/lodash';
import { MetadataKey as ManageOCSShellMetadataKey } from 'pages/WorkflowManageOCSShells/ConfigureParameters/constants';
import { useSelector } from 'react-redux';

const ocsShellsSelector = createSelector(
  (state: RootState) => state.workflowSetup?.ocsShellData,
  data => data
);
export const useSelectOCSShells = () => {
  return useSelector<RootState, OCSShellDataState | undefined>(
    ocsShellsSelector
  );
};

const metadataForManageOCSShellsSelector = createSelector(
  (state: RootState) => state.workflowSetup!.elementMetadata.elements,
  data => {
    const elements = data.map(e => {
      return {
        name: e.name,
        options: e.options?.map(opt => {
          const des = opt.description ? ` - ${opt.description}` : '';
          const text = opt.value ? `${opt.value}${des}` : opt.description;

          return {
            code: opt.value,
            text: text,
            orderText: opt.value ? text : '0',
            selected: opt.selected
          };
        })
      } as {
        name: string;
        options: RefData[];
      };
    });

    const orderedElement = elements.map(element => {
      return element.name === ManageOCSShellMetadataKey.DaysAllowed ||
        element.name === ManageOCSShellMetadataKey.TimesAllowed
        ? { ...element }
        : { ...element, options: orderBy(element.options, 'orderText', 'asc') };
    });
    const metadata = Object.values(ManageOCSShellMetadataKey).reduce(
      (acc, key) => {
        const keyOptions =
          orderedElement.find(e => e.name === key)?.options || [];
        return { ...acc, [key]: keyOptions };
      },
      {}
    );

    return metadata;
  }
);
export const useSelectMetadataForManageOCSShells = () => {
  return useSelector<RootState, { [key: string]: RefData[] }>(
    metadataForManageOCSShellsSelector
  );
};
