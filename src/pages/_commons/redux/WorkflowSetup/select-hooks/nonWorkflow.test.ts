import { renderHook } from '@testing-library/react-hooks';
import { WorkflowMetadataAll } from 'app/fixtures/workflow-metadata';
import { WorkflowSetupRootState } from 'app/types';
import * as reactRedux from 'react-redux';
import {
  useSelectElementMetadataNonWorkflow,
  useSelectServicesData,
  useSelectTableListForWorkflowSetup
} from '.';

describe('redux-store > workflow-setup > select-hooks ', () => {
  let selectorMock: jest.SpyInstance;
  const mockSelectorWorkflowSetup = (
    workflowSetup:
      | Pick<WorkflowSetupRootState, 'workflowFormSteps'>
      | Pick<WorkflowSetupRootState, 'workflowSetup'>
      | Pick<WorkflowSetupRootState, 'workflowMethods'>
      | Pick<WorkflowSetupRootState, 'workflowInstance'>
      | Pick<WorkflowSetupRootState, 'elementMetadata'>
      | Pick<WorkflowSetupRootState, 'workflowChangeSets'>
      | Pick<WorkflowSetupRootState, 'createNewChange'>
      | Pick<WorkflowSetupRootState, 'parameterData'>
  ) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector => selector({ workflowSetup }));
  };

  afterEach(() => {
    selectorMock.mockClear();
  });

  it('test on selector useSelectElementMetadataNonWorkflow ', () => {
    mockSelectorWorkflowSetup({
      elementMetadataNonWorkflow: { elements: WorkflowMetadataAll }
    } as any);

    const { rerender, result } = renderHook(() =>
      useSelectElementMetadataNonWorkflow()
    );

    mockSelectorWorkflowSetup({
      elementMetadataNonWorkflow: { elements: [] }
    } as any);
    rerender();
    expect(result.current.tlpTableOptions.length).toEqual(0);
  });

  it(' useSelectServicesData ', () => {
    mockSelectorWorkflowSetup({
      elementMetadataNonWorkflow: { elements: WorkflowMetadataAll }
    } as any);

    const { rerender, result } = renderHook(() => useSelectServicesData());

    mockSelectorWorkflowSetup({
      elementMetadataNonWorkflow: { elements: [] }
    } as any);
    rerender();
    expect(result.current).toEqual(undefined);
  });
  it('useSelectTableListForWorkflowSetup ', () => {
    mockSelectorWorkflowSetup({
      tableListWorkflowSetup: { data: [{ tableId: 'id', tableName: 'name' }] }
    } as any);

    const { rerender, result } = renderHook(() =>
      useSelectTableListForWorkflowSetup()
    );

    mockSelectorWorkflowSetup({
      tableListWorkflowSetup: { data: [] }
    } as any);
    rerender();
    expect(result.current).toEqual([]);
  });
});
