import { renderHook } from '@testing-library/react-hooks';
import { WorkflowMetadataAll } from 'app/fixtures/workflow-metadata';
import { WorkflowSetupRootState } from 'app/types';
import * as reactRedux from 'react-redux';
import { useSelectElementMetadataForAssignPricingStrategies } from '.';

describe('redux-store > workflow-setup > select-hooks ', () => {
  let selectorMock: jest.SpyInstance;
  const mockSelectorWorkflowSetup = (
    workflowSetup:
      | Pick<WorkflowSetupRootState, 'workflowFormSteps'>
      | Pick<WorkflowSetupRootState, 'workflowSetup'>
      | Pick<WorkflowSetupRootState, 'workflowMethods'>
      | Pick<WorkflowSetupRootState, 'workflowInstance'>
      | Pick<WorkflowSetupRootState, 'elementMetadata'>
      | Pick<WorkflowSetupRootState, 'workflowChangeSets'>
      | Pick<WorkflowSetupRootState, 'createNewChange'>
      | Pick<WorkflowSetupRootState, 'parameterData'>
  ) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector => selector({ workflowSetup }));
  };

  afterEach(() => {
    selectorMock.mockClear();
  });

  it('test on selector useSelectElementMetadataForAssignPricingStrategies', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: {
        elements: WorkflowMetadataAll as any
      }
    });

    const { result } = renderHook(() =>
      useSelectElementMetadataForAssignPricingStrategies()
    );

    expect(result.current.configuration.length).toEqual(5);
    expect(result.current.reallocate.length).toEqual(0);
    expect(result.current.portfolio.length).toEqual(3);
    expect(result.current.strategy.length).toEqual(3);
    expect(result.current.status.length).toEqual(2);
    expect(result.current.type.length).toEqual(4);
    expect(result.current.subtraction.length).toEqual(0);
    expect(result.current.subtransactionCode.length).toEqual(31);
    expect(result.current.methodOverrideID.length).toEqual(3);
    expect(result.current.lockUnlock.length).toEqual(2);
    expect(result.current.allocateImmediately.length).toEqual(2);
    expect(result.current.serviceSubjectSection.length).toEqual(0);
    expect(result.current.lock.length).toEqual(2);
    expect(result.current.reallocateNM738.length).toEqual(2);
    expect(result.current.aQTable.length).toEqual(3);
  });

  it('test on selector useSelectElementMetadataForAssignPricingStrategies without workFlow in options', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: {
        elements: [
          {
            name: 'transaction.id',
            options: [
              { value: 'value', description: 'name', workFlow: 'apsmoa' }
            ]
          }
        ] as any
      }
    });

    const { rerender, result } = renderHook(() =>
      useSelectElementMetadataForAssignPricingStrategies()
    );

    expect(result.current.configuration.length).toEqual(1);

    mockSelectorWorkflowSetup({ elementMetadata: { elements: [] as any } });
    rerender();
    expect(result.current.configuration.length).toEqual(0);
  });
});
