import { createSelector } from '@reduxjs/toolkit';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import { uniqBy } from 'lodash';
import { ParameterCode as BAPParameterCode } from 'pages/WorkflowBulkArchivePCF/type';
import { useSelector } from 'react-redux';
import { findByName } from './_helper';

const elementMetadataForBulkArchivePCFSelector = createSelector(
  (state: RootState) => state.workflowSetup!.elementMetadata.elements,
  data => {
    const elements = data.map(e => {
      return {
        name: e.name,
        options: e.options?.map(opt => ({
          ...opt,
          code: opt.value,
          text: !isEmpty(opt.description) ? opt.description : opt.value
        }))
      } as WorkflowElement;
    });

    const methodsRefData = (
      elements.find(findByName(BAPParameterCode.MethodName))?.options || []
    ).sort((a: RefData, b: RefData) =>
      a.code.localeCompare(b.code)
    ) as RefData[];
    const methods = uniqBy(methodsRefData, 'code');

    const strategies = (
      elements.find(findByName(BAPParameterCode.StrategyName))?.options || []
    )
      ?.map((s: any) => ({
        ...s,
        text: `${s.code} - ${s.description}`
      }))
      .sort((a: any, b: any) => a.text.localeCompare(b.text)) as RefData[];

    const textAreas = (
      elements.find(findByName(BAPParameterCode.TextAreaName))?.options || []
    )
      ?.map((s: any) => ({
        ...s,
        text: `${s.code} - ${s.description}`
      }))
      .sort((a: any, b: any) => a.text.localeCompare(b.text)) as RefData[];

    const tableAreas = (
      elements.find(findByName(BAPParameterCode.TableAreaName))?.options || []
    )
      ?.map((s: any) => ({
        ...s,
        text: `${s.code} - ${s.description}`
      }))
      .sort((a: any, b: any) => a.text.localeCompare(b.text)) as RefData[];

    return { methods, strategies, textAreas, tableAreas };
  }
);

export const useSelectElementsBulkArchivePCF = () => {
  return useSelector<
    RootState,
    {
      methods: RefData[];
      strategies: RefData[];
      textAreas: RefData[];
      tableAreas: RefData[];
    }
  >(elementMetadataForBulkArchivePCFSelector);
};

const selectStrategyTransactionsStatus = createSelector(
  (state: RootState) => state.workflowSetup!.workflowStrategies,
  data => data
);
export const useStrategyTransactionsStatus = () => {
  return useSelector<RootState, IFetching>(selectStrategyTransactionsStatus);
};

const selectMethodTransactionsStatus = createSelector(
  (state: RootState) => state.workflowSetup!.workflowMethods,
  data => data
);
export const useMethodTransactionsStatus = () => {
  return useSelector<RootState, IFetching>(selectMethodTransactionsStatus);
};

const selectTextAreaTransactionsStatus = createSelector(
  (state: RootState) => state.workflowSetup!.workflowTextAreas,
  data => data
);
export const useTextAreaTransactionsStatus = () => {
  return useSelector<RootState, IFetching>(selectTextAreaTransactionsStatus);
};

const selectTableAreaTransactionsStatus = createSelector(
  (state: RootState) => state.workflowSetup!.workflowTableAreas,
  data => data
);
export const useTableAreaTransactionsStatus = () => {
  return useSelector<RootState, IFetching>(selectTableAreaTransactionsStatus);
};
