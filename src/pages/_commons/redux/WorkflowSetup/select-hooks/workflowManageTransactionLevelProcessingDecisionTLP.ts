import { createSelector } from '@reduxjs/toolkit';
import {
  ConfigTableOptionsEnum,
  MethodFieldParameterEnum
} from 'app/constants/enums';
import { useSelector } from 'react-redux';

export const decisionElementListData = (tableType: any) =>
  createSelector(
    (state: RootState) => state.workflowSetup!.decisionElementList[tableType],
    data => data
  );
export const useSelectDecisionElementListData = (tableType: any) => {
  return useSelector<RootState, MagicKeyValue>(
    decisionElementListData(tableType)
  );
};

const elementMetadataManageTransactionTLPSelector = createSelector(
  (state: RootState) => state.workflowSetup!.elementMetadata.elements,
  data => {
    const searchCode =
      data.find(e => e.name === MethodFieldParameterEnum.SearchCode)?.options ||
      [];
    const includeActivity =
      data.find(e => e.name === MethodFieldParameterEnum.IncludeActivityAction)
        ?.options || [];
    const posPromoValidation =
      data.find(e => e.name === MethodFieldParameterEnum.PosPromotionValidation)
        ?.options || [];

    const tlpTableConfig =
      data.find(e => e.name === ConfigTableOptionsEnum.tlpTable)?.options || [];

    const tlpTableConfigOptions = tlpTableConfig.map(code => ({
      code: code.value,
      text: `${code.description} (${code.value})`
    }));

    const searchCodeOptions = searchCode.map(code => ({
      code: code.value,
      text: `${code.value} - ${code.description}`
    }));
    const includeActivityOptions = includeActivity.map(code => ({
      code: code.value,
      text: code.value
        ? `${code.value} - ${code.description}`
        : code.description,
      description: code.description
    }));
    const posPromoValidationOptions = posPromoValidation.map(code => ({
      code: code.value,
      text: code.value
        ? `${code.value} - ${code.description}`
        : code.description,
      description: code.description
    }));
    return {
      searchCode: searchCodeOptions,
      includeActivityOptions,
      posPromoValidationOptions,
      tlpTableConfigOptions
    };
  }
);

export const useSelectElementMetadataManageTransactionTLP = () => {
  return useSelector<
    RootState,
    {
      searchCode: RefData[];
      includeActivityOptions: any[];
      posPromoValidationOptions: any[];
      tlpTableConfigOptions?: RefData[];
    }
  >(elementMetadataManageTransactionTLPSelector);
};
