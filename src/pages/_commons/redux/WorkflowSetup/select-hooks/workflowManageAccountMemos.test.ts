import { renderHook } from '@testing-library/react-hooks';
import { WorkflowSetupRootState } from 'app/types';
import * as reactRedux from 'react-redux';
import { useSelectElementMetadataManageAcountMemos } from './workflowManageAccountMemos';

const elements = [
  {
    id: '34',
    name: 'type',
    options: [
      {
        value: '0',
        description:
          'Process cash advance and merchandise principals using the new Product Control Fil',
        workFlow: 'apsmoa'
      },
      {
        value: '1',
        description:
          'Cash advance balance. Process existing cash advance principals using the current',
        workFlow: 'apsmoa'
      },
      {
        value: '2',
        description:
          'Merchandise balance. Process existing merchandise principals using the current',
        workFlow: 'apsmoa'
      },
      {
        value: '3',
        description:
          'Cash advance and merchandise balances. Process existing merchandise and cash',
        workFlow: 'apsmoa'
      },
      {
        value: 'Type01',
        description: '',
        workFlow: 'mam'
      },
      {
        value: 'Type02',
        description: '',
        workFlow: 'mam'
      },
      {
        value: 'TypeAA',
        description: '',
        workFlow: 'mam'
      }
    ]
  },
  {
    id: '312',
    name: 'memo.type',
    options: [
      {
        value: 'Customer Inquiry System (CIS) Memos',
        description: ''
      },
      {
        value: 'Chronicle Memos',
        description: ''
      }
    ]
  },
  {
    id: '313',
    name: 'action',
    options: [
      {
        value: 'Add Memos',
        description: ''
      },
      {
        value: 'Find Memos',
        description: ''
      },
      {
        value: 'Delete Memos',
        description: ''
      }
    ]
  },
  {
    id: '336',
    name: 'cis.memo.filter.by',
    options: [
      {
        value: 'Entered ID',
        description: ''
      },
      {
        value: 'Sys',
        description: ''
      },
      {
        value: 'Prin',
        description: ''
      },
      {
        value: 'Agent',
        description: ''
      },
      {
        value: 'Date',
        description: ''
      },
      {
        value: 'Memo Text',
        description: ''
      }
    ]
  },
  {
    id: '337',
    name: 'chronicle.memo.filter.by',
    options: [
      {
        value: 'Entered ID',
        description: ''
      },
      {
        value: 'Sys',
        description: ''
      },
      {
        value: 'Prin',
        description: ''
      },
      {
        value: 'Agent',
        description: ''
      },
      {
        value: 'Date',
        description: ''
      },
      {
        value: 'Memo Text',
        description: ''
      },
      {
        value: 'Type',
        description: ''
      }
    ]
  },
  {
    id: '339',
    name: 'operator',
    options: [
      {
        value: 'and',
        description: 'And'
      },
      {
        value: 'or',
        description: 'Or'
      }
    ]
  },
  {
    id: '340',
    name: 'entered.id.operators',
    options: [
      {
        value: 'equal',
        description: 'Equal'
      },
      {
        value: 'notEqual',
        description: 'Not Equal'
      }
    ]
  },
  {
    id: '341',
    name: 'sys.operators',
    options: [
      {
        value: 'equal',
        description: 'Equal',
        workFlow: 'mam'
      },
      {
        value: 'notEqual',
        description: 'Not Equal',
        workFlow: 'mam'
      },
      {
        value: 'equal',
        description: 'Equal',
        workFlow: 'cpd'
      }
    ]
  },
  {
    id: '342',
    name: 'prin.operators',
    options: [
      {
        value: 'equal',
        description: 'Equal',
        workFlow: 'mam'
      },
      {
        value: 'notEqual',
        description: 'Not Equal',
        workFlow: 'mam'
      },
      {
        value: 'equal',
        description: 'Equal',
        workFlow: 'cpd'
      }
    ]
  },
  {
    id: '343',
    name: 'agent.operators',
    options: [
      {
        value: 'equal',
        description: 'Equal',
        workFlow: 'mam'
      },
      {
        value: 'notEqual',
        description: 'Not Equal',
        workFlow: 'mam'
      },
      {
        value: 'equal',
        description: 'Equal',
        workFlow: 'cpd'
      }
    ]
  },
  {
    id: '344',
    name: 'date.operators',
    options: [
      {
        value: 'between',
        description: 'Between'
      },
      {
        value: 'before',
        description: 'Before'
      },
      {
        value: 'after',
        description: 'After'
      },
      {
        value: 'notBetween',
        description: 'Not Between'
      }
    ]
  },
  {
    id: '345',
    name: 'memo.text.operators',
    options: [
      {
        value: 'equal',
        description: 'Equal'
      },
      {
        value: 'contains',
        description: 'Contains'
      },
      {
        value: 'notContain',
        description: 'Not Contains'
      }
    ]
  },
  {
    id: '346',
    name: 'type.operators',
    options: [
      {
        value: 'equal',
        description: 'Equal'
      },
      {
        value: 'notEqual',
        description: 'Not Equal'
      }
    ]
  },
  {
    id: '191',
    name: 'system.principal.agents',
    options: [
      {
        value: '0000-0200-3456',
        description: 'system principle agent numbers'
      },
      {
        value: '0000-0200-0001',
        description: 'system principle agent numbers'
      },
      {
        value: '0000-0200-0002',
        description: 'system principle agent numbers'
      },
      {
        value: '0000-0220-3451',
        description: 'system principle agent numbers'
      },
      {
        value: '0000-0220-0002',
        description: 'system principle agent numbers'
      },
      {
        value: '0000-0220-0003',
        description: 'system principle agent numbers'
      },
      {
        value: '0000-0230-3221',
        description: 'system principle agent numbers'
      },
      {
        value: '0000-0230-0008',
        description: 'system principle agent numbers'
      },
      {
        value: '0000-0230-0004',
        description: 'system principle agent numbers'
      },
      {
        value: '0010-0300-0003',
        description: 'system principle agent numbers'
      },
      {
        value: '0010-0300-0004',
        description: 'system principle agent numbers'
      },
      {
        value: '0010-0300-0005',
        description: 'system principle agent numbers'
      },
      {
        value: '0010-0330-0003',
        description: 'system principle agent numbers'
      },
      {
        value: '0010-0330-0004',
        description: 'system principle agent numbers'
      },
      {
        value: '0010-0330-0005',
        description: 'system principle agent numbers'
      },
      {
        value: '0010-0304-0003',
        description: 'system principle agent numbers'
      },
      {
        value: '0010-0304-0004',
        description: 'system principle agent numbers'
      },
      {
        value: '0010-0304-0005',
        description: 'system principle agent numbers'
      },
      {
        value: '0020-0400-0006',
        description: 'system principle agent numbers'
      },
      {
        value: '0020-0400-0007',
        description: 'system principle agent numbers'
      },
      {
        value: '0020-0400-0008',
        description: 'system principle agent numbers'
      },
      {
        value: '0020-1400-0006',
        description: 'system principle agent numbers'
      },
      {
        value: '0020-1400-0007',
        description: 'system principle agent numbers'
      },
      {
        value: '0020-1400-0008',
        description: 'system principle agent numbers'
      },
      {
        value: '0020-2400-0006',
        description: 'system principle agent numbers'
      },
      {
        value: '0020-2400-0007',
        description: 'system principle agent numbers'
      },
      {
        value: '0020-2400-0008',
        description: 'system principle agent numbers'
      }
    ]
  }
];

describe('redux-store > workflow-setup > select-hooks ', () => {
  let selectorMock: jest.SpyInstance;
  const mockSelectorWorkflowSetup = (
    workflowSetup:
      | Pick<WorkflowSetupRootState, 'workflowFormSteps'>
      | Pick<WorkflowSetupRootState, 'workflowSetup'>
      | Pick<WorkflowSetupRootState, 'workflowMethods'>
      | Pick<WorkflowSetupRootState, 'workflowInstance'>
      | Pick<WorkflowSetupRootState, 'elementMetadata'>
      | Pick<WorkflowSetupRootState, 'workflowChangeSets'>
      | Pick<WorkflowSetupRootState, 'createNewChange'>
      | Pick<WorkflowSetupRootState, 'parameterData'>
  ) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector => selector({ workflowSetup }));
  };

  afterEach(() => {
    selectorMock.mockClear();
  });

  it('test on selector useSelectElementMetadataManageAcountMemos ', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: {
        elements: elements
      }
    } as any);

    const { rerender, result } = renderHook(() =>
      useSelectElementMetadataManageAcountMemos()
    );

    mockSelectorWorkflowSetup({
      elementMetadata: {
        elements: elements
      }
    } as any);
    rerender();
    expect(result.current.memosTypes.length).toEqual(2);
  });
});
