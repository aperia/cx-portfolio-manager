import { createSelector } from '@reduxjs/toolkit';
import {
  ConfigTableALPOptionsEnum,
  MethodFieldParameterEnum
} from 'app/constants/enums';
import { isEmpty } from 'lodash';
import { useSelector } from 'react-redux';

const strategiesList = createSelector(
  (state: RootState) => state.workflowSetup!.strategiesListAQ,
  data => data
);
export const useSelectStrategiesListData = () => {
  return useSelector<RootState, MagicKeyValue>(strategiesList);
};

const tableData = createSelector(
  (state: RootState) => state.workflowSetup!.tableList,
  data => data
);
export const useSelectTableListData = () => {
  return useSelector<RootState, MagicKeyValue>(tableData);
};

const decisionLevelData = createSelector(
  (state: RootState) => state.workflowSetup!.decisionLevelData,
  data => data
);
export const useSelectDecisionData = () => {
  return useSelector<RootState, MagicKeyValue[]>(decisionLevelData);
};

const decisionData = createSelector(
  (state: RootState) => state.workflowSetup!.decisionList,
  data => data
);
export const useSelectDecisionListData = () => {
  return useSelector<RootState, MagicKeyValue>(decisionData);
};

const elementMetadataManageAcountLevelSelector = createSelector(
  (state: RootState) => state.workflowSetup!.elementMetadata.elements,
  data => {
    const elements = data.map(e => {
      if (e.name === MethodFieldParameterEnum.AllocationInterval) {
        return {
          name: e.name,
          options: e.options?.map(otp => {
            if (isEmpty(otp?.value)) {
              return {
                code: otp?.value,
                text: otp?.description
              };
            }
            return {
              code: otp?.value,
              text: otp?.value
            };
          })
        };
      }
      if (e.name === ConfigTableALPOptionsEnum.alpTable) {
        return {
          name: e.name,
          options: e.options?.map(otp => {
            return {
              code: otp?.value,
              text: `${otp?.description} (${otp?.value})`
            };
          })
        };
      }
      return {
        name: e.name,
        options: e.options?.map(otp => {
          if (isEmpty(otp?.value)) {
            return {
              code: otp?.value,
              text: otp?.description
            };
          }
          return {
            code: otp?.value,
            text: `${otp?.value} - ${otp?.description}`
          };
        })
      };
    });
    const alpConfig =
      elements.find(e => e.name === ConfigTableALPOptionsEnum.alpTable)
        ?.options || [];

    const searchCode =
      elements.find(e => e.name === MethodFieldParameterEnum.SearchCode)
        ?.options || [];

    const allocationFlag =
      elements.find(e => e.name === MethodFieldParameterEnum.AllocationFlag)
        ?.options || [];
    const allocationBeforeAfterCycle =
      elements.find(
        e => e.name === MethodFieldParameterEnum.AllocationBeforeAfterCycle
      )?.options || [];
    const changeInTermsMethodFlag =
      elements.find(
        e => e.name === MethodFieldParameterEnum.ChangeInTermsMethodFlag
      )?.options || [];
    const allocationInterval =
      elements.find(e => e.name === MethodFieldParameterEnum.AllocationInterval)
        ?.options || [];

    return {
      searchCode,
      allocationFlag,
      allocationBeforeAfterCycle,
      changeInTermsMethodFlag,
      allocationInterval,
      alpConfig
    };
  }
);

export const useSelectElementMetadataManageAcountLevel = () => {
  return useSelector<
    RootState,
    {
      searchCode: any[];
      allocationFlag: any[];
      allocationBeforeAfterCycle: any[];
      changeInTermsMethodFlag: any[];
      allocationInterval: any[];
      alpConfig: RefData[];
    }
  >(elementMetadataManageAcountLevelSelector);
};
