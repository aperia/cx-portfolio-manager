import { createSelector } from '@reduxjs/toolkit';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import { useSelector } from 'react-redux';

export const elementMetadataForActionCardCompromiseEvent = createSelector(
  (state: RootState) => state.workflowSetup!.elementMetadata.elements,
  data => {
    const elements = data.map(e => {
      return {
        name: e.name,
        options: e.options?.map(opt => ({
          code: opt.value || '',
          text: opt.value
            ? `${opt.value}${opt.description ? ` - ${opt.description}` : ''}`
            : `${opt.description}`,
          type: opt.type,
          workFlow: opt.workFlow
        }))
      } as {
        name: string;
        options: RefData[];
      };
    });

    const question1 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.ActionCardCompromiseEventQuestion1
      )?.options || [];
    const question2SingleEntity =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ActionCardCompromiseEventQuestion2SingleEntity
      )?.options || [];
    const question2SeparateEntity =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ActionCardCompromiseEventQuestion2SeparateEntity
      )?.options || [];
    const question3 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.ActionCardCompromiseEventQuestion3
      )?.options || [];
    const question4 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.ActionCardCompromiseEventQuestion4
      )?.options || [];
    const transaction =
      elements
        .find(
          e =>
            e.name ===
            MethodFieldParameterEnum.ActionCardCompromiseEventTransaction
        )
        ?.options?.filter((f: any) => f.workFlow === 'acce' || !f.workFlow) ||
      [];
    const singleEntityYesTrans =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ActionCardCompromiseEventSingleEntityAccountTransferYesTransaction
      )?.options || [];
    const singleEntityNoTrans =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ActionCardCompromiseEventSingleEntityAccountTransferNoTransaction
      )?.options || [];
    const singleEntityReportStolen =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ActionCardCompromiseEventSingleEntityReportStolenAccountTransaction
      )?.options || [];
    const singleEntityReportLost =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ActionCardCompromiseEventSingleEntityReportLostAccountTransaction
      )?.options || [];
    const separateEntityYesTrans =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ActionCardCompromiseEventSeparateEntityPIIDTransferYesTransaction
      )?.options || [];
    const separateEntityNoTrans =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ActionCardCompromiseEventSeparateEntityPIIDTransferNoTransaction
      )?.options || [];
    const separateEntityReportStolen =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ActionCardCompromiseEventSeparateEntityReportStolenPIIDTransaction
      )?.options || [];
    const separateEntityReportLost =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ActionCardCompromiseEventSeparateEntityReportLostPIIDTransaction
      )?.options || [];

    const externalStatus =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ActionCardCompromiseEventExternalStatus
      )?.options || [];

    const pinLostIndicator =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ActionCardCompromiseEventPINLostIndicator
      )?.options || [];

    const fraudLossOperator =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ActionCardCompromiseEventFraudLossOperator
      )?.options || [];

    const possibleFraudActivityCode =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ActionCardCompromiseEventPossibleFraudActivityCode
      )?.options || [];

    const fraudTypeCode =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ActionCardCompromiseEventFraudTypeCode
      )?.options || [];

    const lostLocationCode =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ActionCardCompromiseEventLostLocationCode
      )?.options || [];

    const rushPlasticCode =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ActionCardCompromiseEventRushPlasticCode
      )?.options || [];

    const piStatusCode =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ActionCardCompromiseEventPiStatusCode
      )?.options || [];

    const lossOperatorId =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ActionCardCompromiseEventLossOperatorId
      )?.options || [];

    const producePlasticIndicatorPLGEN =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ActionCardCompromiseEventProducePlasticIndicatorPLGEN
      )?.options || [];

    const producePlasticIndicatorSuppress =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ActionCardCompromiseEventProducePlasticIndicatorSuppress
      )?.options || [];

    const producePlasticIndicatorPlastic =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ActionCardCompromiseEventProducePlasticIndicatorPlastic
      )?.options || [];

    const typeLossCode =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ActionCardCompromiseEventTypeLossCode
      )?.options || [];

    const visaResponseCode =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ActionCardCompromiseEventVisaResponseCode
      )?.options || [];

    const visaWarningBulletinRegion =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ActionCardCompromiseEventVisaWarningBulletinRegion
      )?.options || [];

    const inasReasonCode =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardINASReasonCode
      )?.options || [];

    const masterCardStatus =
      elements
        .find(
          e =>
            e.name ===
            MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardStatus
        )
        ?.options?.filter((f: any) => f.workFlow === 'acce' || !f.workFlow) ||
      [];

    const masterCardWarningBulletinRegion =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ActionCardCompromiseEventMasterCardWarningBulletinRegion
      )?.options || [];

    const accountTransferTypeCode =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ActionCardCompromiseEventAccountTransferTypeCode
      )?.options || [];

    const letterIdentifier =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ActionCardCompromiseEventGenerateLetterIdentifier
      )?.options || [];

    const rushMailCode =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ActionCardCompromiseEventRushMailCode
      )?.options || [];

    return {
      question1,
      question2SingleEntity,
      question2SeparateEntity,
      question3,
      question4,
      transaction,
      singleEntityNoTrans,
      singleEntityYesTrans,
      singleEntityReportLost,
      singleEntityReportStolen,
      separateEntityNoTrans,
      separateEntityYesTrans,
      separateEntityReportStolen,
      separateEntityReportLost,
      externalStatus,
      pinLostIndicator,
      fraudLossOperator,
      possibleFraudActivityCode,
      fraudTypeCode,
      lostLocationCode,
      rushPlasticCode,
      visaWarningBulletinRegion,
      visaResponseCode,
      inasReasonCode,
      masterCardStatus,
      masterCardWarningBulletinRegion,
      piStatusCode,
      lossOperatorId,
      producePlasticIndicatorPLGEN,
      producePlasticIndicatorPlastic,
      producePlasticIndicatorSuppress,
      typeLossCode,
      accountTransferTypeCode,
      letterIdentifier,
      rushMailCode
    };
  }
);

export const useSelectElementMetadataForActionCardCompromiseEvent = () => {
  return useSelector<
    RootState,
    {
      question1: RefData[];
      question2SingleEntity: RefData[];
      question2SeparateEntity: RefData[];
      question3: RefData[];
      question4: RefData[];
      transaction: RefData[];
      singleEntityNoTrans: RefData[];
      singleEntityYesTrans: RefData[];
      singleEntityReportLost: RefData[];
      singleEntityReportStolen: RefData[];
      separateEntityNoTrans: RefData[];
      separateEntityYesTrans: RefData[];
      separateEntityReportStolen: RefData[];
      separateEntityReportLost: RefData[];
      externalStatus: RefData[];
      piStatusCode: RefData[];
      lossOperatorId: RefData[];
      producePlasticIndicatorPLGEN: RefData[];
      producePlasticIndicatorPlastic: RefData[];
      producePlasticIndicatorSuppress: RefData[];
      typeLossCode: RefData[];
      pinLostIndicator: RefData[];
      fraudLossOperator: RefData[];
      possibleFraudActivityCode: RefData[];
      fraudTypeCode: RefData[];
      lostLocationCode: RefData[];
      rushPlasticCode: RefData[];
      visaWarningBulletinRegion: RefData[];
      visaResponseCode: RefData[];
      inasReasonCode: RefData[];
      masterCardStatus: RefData[];
      masterCardWarningBulletinRegion: RefData[];
      accountTransferTypeCode: RefData[];
      letterIdentifier: RefData[];
      rushMailCode: RefData[];
    }
  >(elementMetadataForActionCardCompromiseEvent);
};
