import { renderHook } from '@testing-library/react-hooks';
import { WorkflowSetupRootState } from 'app/types';
import * as reactRedux from 'react-redux';
import {
  useSelectElementMetadataForMSPS,
  useSelectWorkflowMethodsMSPSSelector
} from './workflowManageStatementProductionSettings';

const elements = [
  {
    name: 'minimum.amount.for.inactive.deleted.account.statements',
    previousValue: '9015',
    newValue: '3309',
    options: [{ code: 'code' }, { text: 'text' }]
  },
  {
    id: '331',
    name: 'credit.balance.hold.options',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '03',
        description:
          'Place the hold code on the statement when an account cycles three consecutive times with a credit balance and send statement to issuer.'
      },
      {
        value: '05',
        description:
          'Place the hold code on the statement when an account cycles five consecutive times with a credit balance and send statement to issuer.'
      },
      {
        value: '06',
        description:
          'Place the hold code on the statement when an account cycles six consecutive times with a credit balance and send statement to issuer.'
      },
      {
        value: '07',
        description:
          'Place the hold code on the statement when an account cycles seven consecutive times with a credit balance and send statement to issuer.'
      },
      {
        value: '13',
        description:
          'Place the hold code on the statement when an account cycles six consecutive times with a credit balance and send statement to the issuer unless there is current activity on the account, in which case send a regular statement to the customer.'
      }
    ]
  },
  {
    id: '331',
    name: 'where.to.send.requested.statements',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description: 'Send requested statements to the issuer.'
      },
      {
        value: '1',
        description: 'Send requested statements to the customer.'
      }
    ]
  },
  {
    id: '331',
    name: 'payment.due.days',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '01',
        description: ''
      },
      {
        value: '02',
        description: ''
      },
      {
        value: '03',
        description: ''
      },
      {
        value: '04',
        description: ''
      },
      {
        value: '05',
        description: ''
      },
      {
        value: '06',
        description: ''
      },
      {
        value: '07',
        description: ''
      },
      {
        value: '08',
        description: ''
      },
      {
        value: '09',
        description: ''
      },
      {
        value: '10',
        description: ''
      },
      {
        value: '11',
        description: ''
      },
      {
        value: '12',
        description: ''
      },
      {
        value: '13',
        description: ''
      },
      {
        value: '14',
        description: ''
      },
      {
        value: '15',
        description: ''
      },
      {
        value: '16',
        description: ''
      },
      {
        value: '17',
        description: ''
      },
      {
        value: '18',
        description: ''
      },
      {
        value: '19',
        description: ''
      },
      {
        value: '20',
        description: ''
      },
      {
        value: '21',
        description: ''
      },
      {
        value: '22',
        description: ''
      },
      {
        value: '23',
        description: ''
      },
      {
        value: '24',
        description: ''
      },
      {
        value: '25',
        description: ''
      },
      {
        value: '26',
        description: ''
      },
      {
        value: '27',
        description: ''
      },
      {
        value: '28',
        description: ''
      },
      {
        value: '29',
        description: ''
      },
      {
        value: '30',
        description: ''
      }
    ]
  },
  {
    id: '331',
    name: 'same.day.lost.status.option',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description: "Statement with today's lost status.."
      },
      {
        value: '1',
        description: "Statement without today's lost status."
      }
    ]
  },
  {
    id: '331',
    name: 'use.fixed.minimum.payment.due.date',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: 'N',
        description:
          'No, do not use the fixed minimum payment due date feature.'
      },
      {
        value: 'Y',
        description: 'Yes, use the fixed minimum payment due date feature.'
      }
    ]
  },
  {
    id: '331',
    name: 'use.fixed.minimum.payment.due.date.table.id',
    options: [
      {
        value: 'MPDD7001',
        description: ''
      },
      {
        value: 'MPDD7012',
        description: ''
      },
      {
        value: 'MPDD7023',
        description: ''
      }
    ]
  },
  {
    id: '331',
    name: 'annual.interest.display',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description: 'Do not print on cardholder statements.'
      },
      {
        value: '1',
        description:
          'Print billed interest on cardholder statements generated during the months of December, January, February, and March. Also, display billed interest on the BS screen and in the &PRVYTDINT variable in the Cardholder Letters System.'
      },
      {
        value: '2',
        description:
          'Print total paid interest on cardholder statements generated during the months of January, February, and March. Also, display total paid interest on the BS screen and in the &PRVYTDINT variable in the Cardholder Letters System.'
      }
    ]
  },
  {
    id: '331',
    name: 'include.late.charges.in.annual.interest.display',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: 'E',
        description: 'Exclude'
      },
      {
        value: 'I',
        description: 'Include'
      }
    ]
  },
  {
    id: '331',
    name: 'include.cash.item.in.annual.interest.display',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: 'E',
        description: 'Exclude'
      },
      {
        value: 'I',
        description: 'Include'
      }
    ]
  },
  {
    id: '331',
    name: 'include.merchandise.item.in.annual.interest.display',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: 'E',
        description: 'Exclude'
      },
      {
        value: 'I',
        description: 'Include'
      }
    ]
  },
  {
    id: '331',
    name: 'include.overlimit.fees.in.annual.interest.display',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: 'E',
        description: 'Exclude'
      },
      {
        value: 'I',
        description: 'Include'
      }
    ]
  },
  {
    id: '331',
    name: 'include.statement.production.fees.in.annual.interest.display',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: 'E',
        description: 'Exclude'
      },
      {
        value: 'I',
        description: 'Include'
      }
    ]
  },
  {
    id: '331',
    name: 'produce.statements.for.inactive.deleted.accounts',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description:
          'Do not produce statements for inactive or deleted cardholder accounts.'
      },
      {
        value: '1',
        description:
          'Produce statements all year for inactive cardholder accounts.'
      },
      {
        value: '2',
        description:
          'Produce statements in January for inactive cardholder accounts whose interest during the previous year was more than the amount specified in the Special Statement Minimum Amount parameter in this section.'
      },
      {
        value: '5',
        description:
          'Produce statements in January for inactive and deleted cardholder accounts whose interest during the previous year was more than the amount specified in the Special Statement Minimum Amount parameter.'
      }
    ]
  },
  {
    id: '331',
    name: 'us.inactive.account.external.status.control',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: 'E',
        description:
          'Send statements to all inactive accounts except those with an external status other than blank.'
      },
      {
        value: 'I',
        description: 'Send statements to all inactive accounts.'
      }
    ]
  },
  {
    id: '331',
    name: 'non.us.inactive.account.external.status.control',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: 'E',
        description:
          'Send statements to all inactive accounts except those with an external status other than blank.'
      },
      {
        value: 'I',
        description: 'Send statements to all inactive accounts.'
      }
    ]
  },
  {
    id: '331',
    name: 'pricing.strategy.change.statement.production',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description:
          'Do not produce statements for inactive accounts when the pricing strategy changes.'
      },
      {
        value: '1',
        description:
          'Produce statements for inactive accounts when the pricing strategy changes.'
      },
      {
        value: '2',
        description:
          'Do not produce statements for inactive accounts when the pricing strategy changes. For valid code 2, the notification of a change in pricing strategy appears on the first active statement produced following the change.'
      }
    ]
  },
  {
    id: '331',
    name: 'status.a.authorization.prohibited',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description: 'Print statements for accounts regardless of the balance.'
      },
      {
        value: '1',
        description:
          'Print statements for accounts with a balance other than $0.00. Do not print statements for accounts with a balance of $0.00 as the result of a single payment offsetting a debit balance. Print statements for commercial card subaccounts that meet these requirements, even though subaccounts have a zero balance.'
      },
      {
        value: '2',
        description:
          'Print statements only for accounts with a balance other than $0.00. Do not print statements for accounts with a balance of $0.00 regardless of the reason. Print statements for commercial card subaccounts that meet these requirements, even though subaccounts have a zero balance.'
      },
      {
        value: '3',
        description:
          'Print statements for accounts with a balance other than $0.00. Do not print statements for accounts with a balance of $0.00 as the result of a single payment offsetting a debit balance or as the result of a small balance charge off.'
      },
      {
        value: '5',
        description:
          'Do not print statements regardless of the account balance. Option 5 is available for any external status statement print control parameter other than the Normal parameter.'
      },
      {
        value: '6',
        description:
          'Print statements for accounts with a balance other than $0.00. Do not print statements for accounts with a balance of $0.00 as long as the account has no sales, returns, cash advances, credit insurance charges, debt cancellation, or finance charges posted to the account during the cycle. Print statements for commercial card subaccounts that meet these requirements, even though subaccounts have a zero balance.'
      },
      {
        value: '7',
        description:
          'Print statements for accounts with a balance of $0.00, unless the account started and ended the cycle with a zero balance and had no monetary transactions. If the account became a zero balance due to any kind of monetary activity, print a statement. For example, a statement will print if an account had a zero beginning and ending balance, but had a monetary transaction that was reversed in the same cycle.'
      }
    ]
  },
  {
    id: '331',
    name: 'status.b.bankrupt',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description: 'Print statements for accounts regardless of the balance.'
      },
      {
        value: '1',
        description:
          'Print statements for accounts with a balance other than $0.00. Do not print statements for accounts with a balance of $0.00 as the result of a single payment offsetting a debit balance. Print statements for commercial card subaccounts that meet these requirements, even though subaccounts have a zero balance.'
      },
      {
        value: '2',
        description:
          'Print statements only for accounts with a balance other than $0.00. Do not print statements for accounts with a balance of $0.00 regardless of the reason. Print statements for commercial card subaccounts that meet these requirements, even though subaccounts have a zero balance.'
      },
      {
        value: '3',
        description:
          'Print statements for accounts with a balance other than $0.00. Do not print statements for accounts with a balance of $0.00 as the result of a single payment offsetting a debit balance or as the result of a small balance charge off.'
      },
      {
        value: '5',
        description:
          'Do not print statements regardless of the account balance. Option 5 is available for any external status statement print control parameter other than the Normal parameter.'
      },
      {
        value: '6',
        description:
          'Print statements for accounts with a balance other than $0.00. Do not print statements for accounts with a balance of $0.00 as long as the account has no sales, returns, cash advances, credit insurance charges, debt cancellation, or finance charges posted to the account during the cycle. Print statements for commercial card subaccounts that meet these requirements, even though subaccounts have a zero balance.'
      },
      {
        value: '7',
        description:
          'Print statements for accounts with a balance of $0.00, unless the account started and ended the cycle with a zero balance and had no monetary transactions. If the account became a zero balance due to any kind of monetary activity, print a statement. For example, a statement will print if an account had a zero beginning and ending balance, but had a monetary transaction that was reversed in the same cycle.'
      }
    ]
  },
  {
    id: '331',
    name: 'status.c.closed',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description: 'Print statements for accounts regardless of the balance.'
      },
      {
        value: '1',
        description:
          'Print statements for accounts with a balance other than $0.00. Do not print statements for accounts with a balance of $0.00 as the result of a single payment offsetting a debit balance. Print statements for commercial card subaccounts that meet these requirements, even though subaccounts have a zero balance.'
      },
      {
        value: '2',
        description:
          'Print statements only for accounts with a balance other than $0.00. Do not print statements for accounts with a balance of $0.00 regardless of the reason. Print statements for commercial card subaccounts that meet these requirements, even though subaccounts have a zero balance.'
      },
      {
        value: '3',
        description:
          'Print statements for accounts with a balance other than $0.00. Do not print statements for accounts with a balance of $0.00 as the result of a single payment offsetting a debit balance or as the result of a small balance charge off.'
      },
      {
        value: '5',
        description:
          'Do not print statements regardless of the account balance. Option 5 is available for any external status statement print control parameter other than the Normal parameter.'
      },
      {
        value: '6',
        description:
          'Print statements for accounts with a balance other than $0.00. Do not print statements for accounts with a balance of $0.00 as long as the account has no sales, returns, cash advances, credit insurance charges, debt cancellation, or finance charges posted to the account during the cycle. Print statements for commercial card subaccounts that meet these requirements, even though subaccounts have a zero balance.'
      },
      {
        value: '7',
        description:
          'Print statements for accounts with a balance of $0.00, unless the account started and ended the cycle with a zero balance and had no monetary transactions. If the account became a zero balance due to any kind of monetary activity, print a statement. For example, a statement will print if an account had a zero beginning and ending balance, but had a monetary transaction that was reversed in the same cycle.'
      }
    ]
  },
  {
    id: '331',
    name: 'status.e.revoked',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description: 'Print statements for accounts regardless of the balance.'
      },
      {
        value: '1',
        description:
          'Print statements for accounts with a balance other than $0.00. Do not print statements for accounts with a balance of $0.00 as the result of a single payment offsetting a debit balance. Print statements for commercial card subaccounts that meet these requirements, even though subaccounts have a zero balance.'
      },
      {
        value: '2',
        description:
          'Print statements only for accounts with a balance other than $0.00. Do not print statements for accounts with a balance of $0.00 regardless of the reason. Print statements for commercial card subaccounts that meet these requirements, even though subaccounts have a zero balance.'
      },
      {
        value: '3',
        description:
          'Print statements for accounts with a balance other than $0.00. Do not print statements for accounts with a balance of $0.00 as the result of a single payment offsetting a debit balance or as the result of a small balance charge off.'
      },
      {
        value: '5',
        description:
          'Do not print statements regardless of the account balance. Option 5 is available for any external status statement print control parameter other than the Normal parameter.'
      },
      {
        value: '6',
        description:
          'Print statements for accounts with a balance other than $0.00. Do not print statements for accounts with a balance of $0.00 as long as the account has no sales, returns, cash advances, credit insurance charges, debt cancellation, or finance charges posted to the account during the cycle. Print statements for commercial card subaccounts that meet these requirements, even though subaccounts have a zero balance.'
      },
      {
        value: '7',
        description:
          'Print statements for accounts with a balance of $0.00, unless the account started and ended the cycle with a zero balance and had no monetary transactions. If the account became a zero balance due to any kind of monetary activity, print a statement. For example, a statement will print if an account had a zero beginning and ending balance, but had a monetary transaction that was reversed in the same cycle.'
      }
    ]
  },
  {
    id: '331',
    name: 'status.f.frozen',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description: 'Print statements for accounts regardless of the balance.'
      },
      {
        value: '1',
        description:
          'Print statements for accounts with a balance other than $0.00. Do not print statements for accounts with a balance of $0.00 as the result of a single payment offsetting a debit balance. Print statements for commercial card subaccounts that meet these requirements, even though subaccounts have a zero balance.'
      },
      {
        value: '2',
        description:
          'Print statements only for accounts with a balance other than $0.00. Do not print statements for accounts with a balance of $0.00 regardless of the reason. Print statements for commercial card subaccounts that meet these requirements, even though subaccounts have a zero balance.'
      },
      {
        value: '3',
        description:
          'Print statements for accounts with a balance other than $0.00. Do not print statements for accounts with a balance of $0.00 as the result of a single payment offsetting a debit balance or as the result of a small balance charge off.'
      },
      {
        value: '5',
        description:
          'Do not print statements regardless of the account balance. Option 5 is available for any external status statement print control parameter other than the Normal parameter.'
      },
      {
        value: '6',
        description:
          'Print statements for accounts with a balance other than $0.00. Do not print statements for accounts with a balance of $0.00 as long as the account has no sales, returns, cash advances, credit insurance charges, debt cancellation, or finance charges posted to the account during the cycle. Print statements for commercial card subaccounts that meet these requirements, even though subaccounts have a zero balance.'
      },
      {
        value: '7',
        description:
          'Print statements for accounts with a balance of $0.00, unless the account started and ended the cycle with a zero balance and had no monetary transactions. If the account became a zero balance due to any kind of monetary activity, print a statement. For example, a statement will print if an account had a zero beginning and ending balance, but had a monetary transaction that was reversed in the same cycle.'
      }
    ]
  },
  {
    id: '331',
    name: 'status.i.interest.prohibited',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description: 'Print statements for accounts regardless of the balance.'
      },
      {
        value: '1',
        description:
          'Print statements for accounts with a balance other than $0.00. Do not print statements for accounts with a balance of $0.00 as the result of a single payment offsetting a debit balance. Print statements for commercial card subaccounts that meet these requirements, even though subaccounts have a zero balance.'
      },
      {
        value: '2',
        description:
          'Print statements only for accounts with a balance other than $0.00. Do not print statements for accounts with a balance of $0.00 regardless of the reason. Print statements for commercial card subaccounts that meet these requirements, even though subaccounts have a zero balance.'
      },
      {
        value: '3',
        description:
          'Print statements for accounts with a balance other than $0.00. Do not print statements for accounts with a balance of $0.00 as the result of a single payment offsetting a debit balance or as the result of a small balance charge off.'
      },
      {
        value: '5',
        description:
          'Do not print statements regardless of the account balance. Option 5 is available for any external status statement print control parameter other than the Normal parameter.'
      },
      {
        value: '6',
        description:
          'Print statements for accounts with a balance other than $0.00. Do not print statements for accounts with a balance of $0.00 as long as the account has no sales, returns, cash advances, credit insurance charges, debt cancellation, or finance charges posted to the account during the cycle. Print statements for commercial card subaccounts that meet these requirements, even though subaccounts have a zero balance.'
      },
      {
        value: '7',
        description:
          'Print statements for accounts with a balance of $0.00, unless the account started and ended the cycle with a zero balance and had no monetary transactions. If the account became a zero balance due to any kind of monetary activity, print a statement. For example, a statement will print if an account had a zero beginning and ending balance, but had a monetary transaction that was reversed in the same cycle.'
      }
    ]
  },
  {
    id: '331',
    name: 'status.l.lost',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description: 'Print statements for accounts regardless of the balance.'
      },
      {
        value: '1',
        description:
          'Print statements for accounts with a balance other than $0.00. Do not print statements for accounts with a balance of $0.00 as the result of a single payment offsetting a debit balance. Print statements for commercial card subaccounts that meet these requirements, even though subaccounts have a zero balance.'
      },
      {
        value: '2',
        description:
          'Print statements only for accounts with a balance other than $0.00. Do not print statements for accounts with a balance of $0.00 regardless of the reason. Print statements for commercial card subaccounts that meet these requirements, even though subaccounts have a zero balance.'
      },
      {
        value: '3',
        description:
          'Print statements for accounts with a balance other than $0.00. Do not print statements for accounts with a balance of $0.00 as the result of a single payment offsetting a debit balance or as the result of a small balance charge off.'
      },
      {
        value: '5',
        description:
          'Do not print statements regardless of the account balance. Option 5 is available for any external status statement print control parameter other than the Normal parameter.'
      },
      {
        value: '6',
        description:
          'Print statements for accounts with a balance other than $0.00. Do not print statements for accounts with a balance of $0.00 as long as the account has no sales, returns, cash advances, credit insurance charges, debt cancellation, or finance charges posted to the account during the cycle. Print statements for commercial card subaccounts that meet these requirements, even though subaccounts have a zero balance.'
      },
      {
        value: '7',
        description:
          'Print statements for accounts with a balance of $0.00, unless the account started and ended the cycle with a zero balance and had no monetary transactions. If the account became a zero balance due to any kind of monetary activity, print a statement. For example, a statement will print if an account had a zero beginning and ending balance, but had a monetary transaction that was reversed in the same cycle.'
      }
    ]
  },
  {
    id: '331',
    name: 'status.u.stolen',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description: 'Print statements for accounts regardless of the balance.'
      },
      {
        value: '1',
        description:
          'Print statements for accounts with a balance other than $0.00. Do not print statements for accounts with a balance of $0.00 as the result of a single payment offsetting a debit balance. Print statements for commercial card subaccounts that meet these requirements, even though subaccounts have a zero balance.'
      },
      {
        value: '2',
        description:
          'Print statements only for accounts with a balance other than $0.00. Do not print statements for accounts with a balance of $0.00 regardless of the reason. Print statements for commercial card subaccounts that meet these requirements, even though subaccounts have a zero balance.'
      },
      {
        value: '3',
        description:
          'Print statements for accounts with a balance other than $0.00. Do not print statements for accounts with a balance of $0.00 as the result of a single payment offsetting a debit balance or as the result of a small balance charge off.'
      },
      {
        value: '5',
        description:
          'Do not print statements regardless of the account balance. Option 5 is available for any external status statement print control parameter other than the Normal parameter.'
      },
      {
        value: '6',
        description:
          'Print statements for accounts with a balance other than $0.00. Do not print statements for accounts with a balance of $0.00 as long as the account has no sales, returns, cash advances, credit insurance charges, debt cancellation, or finance charges posted to the account during the cycle. Print statements for commercial card subaccounts that meet these requirements, even though subaccounts have a zero balance.'
      },
      {
        value: '7',
        description:
          'Print statements for accounts with a balance of $0.00, unless the account started and ended the cycle with a zero balance and had no monetary transactions. If the account became a zero balance due to any kind of monetary activity, print a statement. For example, a statement will print if an account had a zero beginning and ending balance, but had a monetary transaction that was reversed in the same cycle.'
      }
    ]
  },
  {
    id: '331',
    name: 'status.blank',
    options: [
      {
        value: '',
        description: 'None'
      },
      {
        value: '0',
        description: 'Print statements for accounts regardless of the balance.'
      },
      {
        value: '1',
        description:
          'Print statements for accounts with a balance other than $0.00. Do not print statements for accounts with a balance of $0.00 as the result of a single payment offsetting a debit balance. Print statements for commercial card subaccounts that meet these requirements, even though subaccounts have a zero balance.'
      },
      {
        value: '2',
        description:
          'Print statements only for accounts with a balance other than $0.00. Do not print statements for accounts with a balance of $0.00 regardless of the reason. Print statements for commercial card subaccounts that meet these requirements, even though subaccounts have a zero balance.'
      },
      {
        value: '3',
        description:
          'Print statements for accounts with a balance other than $0.00. Do not print statements for accounts with a balance of $0.00 as the result of a single payment offsetting a debit balance or as the result of a small balance charge off.'
      },
      {
        value: '5',
        description:
          'Do not print statements regardless of the account balance. Option 5 is available for any external status statement print control parameter other than the Normal parameter.'
      },
      {
        value: '6',
        description:
          'Print statements for accounts with a balance other than $0.00. Do not print statements for accounts with a balance of $0.00 as long as the account has no sales, returns, cash advances, credit insurance charges, debt cancellation, or finance charges posted to the account during the cycle. Print statements for commercial card subaccounts that meet these requirements, even though subaccounts have a zero balance.'
      },
      {
        value: '7',
        description:
          'Print statements for accounts with a balance of $0.00, unless the account started and ended the cycle with a zero balance and had no monetary transactions. If the account became a zero balance due to any kind of monetary activity, print a statement. For example, a statement will print if an account had a zero beginning and ending balance, but had a monetary transaction that was reversed in the same cycle.'
      }
    ]
  }
];

describe('redux-store > workflow-setup > select-hooks ', () => {
  let selectorMock: jest.SpyInstance;
  const mockSelectorWorkflowSetup = (
    workflowSetup:
      | Pick<WorkflowSetupRootState, 'workflowFormSteps'>
      | Pick<WorkflowSetupRootState, 'workflowSetup'>
      | Pick<WorkflowSetupRootState, 'workflowMethods'>
      | Pick<WorkflowSetupRootState, 'workflowInstance'>
      | Pick<WorkflowSetupRootState, 'elementMetadata'>
      | Pick<WorkflowSetupRootState, 'workflowChangeSets'>
      | Pick<WorkflowSetupRootState, 'createNewChange'>
      | Pick<WorkflowSetupRootState, 'parameterData'>
  ) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector => selector({ workflowSetup }));
  };

  afterEach(() => {
    selectorMock.mockClear();
  });

  it('test on selector useSelectWorkflowIncomeOptionMethodsSelector ', () => {
    mockSelectorWorkflowSetup({
      workflowMethods: {
        methods: [
          {
            id: '1',
            name: 'name',
            effectiveDate: '2020-03-22',
            comment: 'comment',
            relatedPricingStrategies: 1,
            numberOfVersions: 1,
            versions: [
              {
                id: '2',
                comment: 'comment',
                effectiveDate: '2020-03-22',
                status: 'done',
                parameters: ''
              }
            ],
            pricingStrategies: [
              {
                id: '3',
                name: 'name',
                description: 'description',
                effectiveDate: '2020-03-22',
                numberOfCardHolders: 1
              }
            ]
          }
        ],
        dataFilter: {},
        loading: false,
        error: false
      }
    } as any);

    const { rerender, result } = renderHook(() =>
      useSelectWorkflowMethodsMSPSSelector()
    );

    mockSelectorWorkflowSetup({
      workflowMethods: {
        methods: [
          {
            id: '1',
            name: 'name',
            effectiveDate: '2020-03-22',
            comment: 'comment',
            relatedPricingStrategies: 1,
            numberOfVersions: 1,
            versions: [
              {
                id: '2',
                comment: 'comment',
                effectiveDate: '2020-03-22',
                status: 'done',
                parameters: ''
              }
            ],
            pricingStrategies: [
              {
                id: '3',
                name: 'name',
                description: 'description',
                effectiveDate: '2020-03-22',
                numberOfCardHolders: 1
              }
            ]
          }
        ],
        dataFilter: {},
        loading: false,
        error: false
      }
    } as any);
    rerender();
    expect(result.current.loading).toEqual(false);
    expect(result.current.error).toEqual(false);
  });

  it('test on selector useSelectElementMetadataForMSPS ', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: { elements: elements }
    } as any);

    const { rerender, result } = renderHook(() =>
      useSelectElementMetadataForMSPS()
    );

    mockSelectorWorkflowSetup({
      elementMetadata: { elements: elements }
    } as any);
    rerender();
    expect(result.current.creditBalanceHoldOptions.length).toEqual(6);
  });
});
