import { renderHook } from '@testing-library/react-hooks';
import { WorkflowSetupRootState } from 'app/types';
import * as reactRedux from 'react-redux';
import { useSelectMetadataForManageOCSShells, useSelectOCSShells } from '.';

describe('redux-store > workflow-setup > select-hooks ', () => {
  let selectorMock: jest.SpyInstance;
  const mockSelectorWorkflowSetup = (
    workflowSetup:
      | Pick<WorkflowSetupRootState, 'workflowFormSteps'>
      | Pick<WorkflowSetupRootState, 'workflowSetup'>
      | Pick<WorkflowSetupRootState, 'workflowMethods'>
      | Pick<WorkflowSetupRootState, 'workflowInstance'>
      | Pick<WorkflowSetupRootState, 'elementMetadata'>
      | Pick<WorkflowSetupRootState, 'workflowChangeSets'>
      | Pick<WorkflowSetupRootState, 'createNewChange'>
      | Pick<WorkflowSetupRootState, 'parameterData'>
  ) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector => selector({ workflowSetup }));
  };

  afterEach(() => {
    selectorMock.mockClear();
  });

  it('test on selector useSelectMetadataForManageOCSShells', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: {
        elements: [
          {
            id: '1',
            name: 'account.profile',
            options: [
              {
                value: '',
                description: 'None'
              },
              {
                value: 'Sadio',
                description: 'Sadio'
              },
              {
                value: 'Mane',
                description: ''
              }
            ]
          },
          {
            id: '1',
            name: 'times.allowed',
            options: [
              {
                value: 'Monday',
                description: 'Monday'
              }
            ]
          }
        ] as any
      }
    });

    const { result } = renderHook(() => useSelectMetadataForManageOCSShells());
    expect(result.current['account.profile']).toHaveLength(3);
    expect(result.current['times.allowed']).toHaveLength(1);
  });

  it('test on selector useSelectOCSShells', () => {
    mockSelectorWorkflowSetup({
      ocsShellData: {
        ocsShells: [{ shellName: 'SadioMane' }]
      }
    } as any);

    const { result } = renderHook(() => useSelectOCSShells());
    expect(result.current?.ocsShells).toHaveLength(1);
  });
});
