import { renderHook } from '@testing-library/react-hooks';
import { WorkflowSetupRootState } from 'app/types';
import { ParameterCode } from 'pages/WorkflowManageAdjustmentsSystem/type';
import * as reactRedux from 'react-redux';
import { useSelectElementsForConditionMCycleAccountsForTesting } from '.';

describe('redux-store > workflow-setup > select-hooks ', () => {
  let selectorMock: jest.SpyInstance;
  const mockSelectorWorkflowSetup = (
    workflowSetup:
      | Pick<WorkflowSetupRootState, 'workflowFormSteps'>
      | Pick<WorkflowSetupRootState, 'workflowSetup'>
      | Pick<WorkflowSetupRootState, 'workflowMethods'>
      | Pick<WorkflowSetupRootState, 'workflowInstance'>
      | Pick<WorkflowSetupRootState, 'elementMetadata'>
      | Pick<WorkflowSetupRootState, 'workflowChangeSets'>
      | Pick<WorkflowSetupRootState, 'createNewChange'>
      | Pick<WorkflowSetupRootState, 'parameterData'>
  ) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector => selector({ workflowSetup }));
  };

  afterEach(() => {
    selectorMock.mockClear();
  });
  it('test on selector useSelectElementsForConditionMCycleAccountsForTesting', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: {
        elements: [
          {
            id: '1',
            name: 'account.profile',
            options: [
              {
                value: '',
                description: 'None'
              },
              {
                value: 'Sadio',
                description: 'Sadio'
              },
              {
                value: 'Mane',
                description: ''
              }
            ]
          },
          {
            id: '2',
            name: 'times.allowed',
            options: [
              {
                value: 'Monday',
                description: 'Monday'
              }
            ]
          },
          {
            id: '3',
            name: ParameterCode.TransactionId,
            options: [
              {
                value: 'Monday',
                description: 'Monday'
              }
            ]
          }
        ] as any
      }
    } as any);

    const { rerender, result } = renderHook(() =>
      useSelectElementsForConditionMCycleAccountsForTesting()
    );

    rerender();
    expect(result.current).toEqual({
      transactions: [
        {
          code: 'Monday',
          description: 'Monday',
          id: 'Monday',
          name: 'Monday',
          parameters: undefined,
          text: 'Monday',
          value: 'Monday'
        }
      ]
    });
  });

  it('test on selector useSelectElementsForConditionMCycleAccountsForTesting with empty element', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: {
        elements: [] as any
      }
    } as any);

    const { rerender, result } = renderHook(() =>
      useSelectElementsForConditionMCycleAccountsForTesting()
    );

    rerender();
    expect(result.current).toEqual({ transactions: [] });
  });
});
