import { renderHook } from '@testing-library/react-hooks';
import { ChangeList, ChangeMapping } from 'app/fixtures/change-data';
import { mappingDataFromArray } from 'app/helpers';
import {
  WorkflowSetupRootState,
  WorkflowSetupStepForm,
  WorkflowSetupStepTracking
} from 'app/types';
import {
  useLastWorkflowStep,
  useSelectCreateNewChangeStatus,
  useSelectedWorkflowStep,
  useSelectElementMetadataStatus,
  useSelectWorkflowInstance,
  useWorkflowChangeSetFilterSelector,
  useWorkflowChangeSetsSelector,
  useWorkflowChangeSetWithTemplateSelector,
  useWorkflowChangeStatusListSelector,
  useWorkflowFormStep,
  useWorkflowFormStepsDependency,
  useWorkflowFormStepValidate,
  useWorkflowInstanceId,
  useWorkflowSetup,
  useWorkflowSetupForms,
  useWorkflowSetupStepTracking
} from 'pages/_commons/redux/WorkflowSetup';
import * as reactRedux from 'react-redux';
import { defaultWorkflowChangeSetFilter } from '../reducers';
import {
  selectedChangeSetPageSelector,
  useCurrentWorkflowSetup,
  usePrevInvalidSavedWorkflowSetupForms,
  usePrevSavedWorkflowSetupForms,
  useSelectWorkflowSetupLoading,
  useWorkflowFormStepsDependencyNoCache,
  useWorkflowFormStepsHaveBeenSaved,
  useWorkflowFormStepsSavedAt,
  useWorkflowFormStepsStuck,
  useWorkflowSetupTemplateId
} from './_common';

describe('redux-store > workflow-setup > select-hooks ', () => {
  let selectorMock: jest.SpyInstance;
  const mockSelectorWorkflowSetup = (
    workflowSetup:
      | Pick<WorkflowSetupRootState, 'workflowFormSteps'>
      | Pick<WorkflowSetupRootState, 'workflowSetup'>
      | Pick<WorkflowSetupRootState, 'workflowMethods'>
      | Pick<WorkflowSetupRootState, 'workflowInstance'>
      | Pick<WorkflowSetupRootState, 'elementMetadata'>
      | Pick<WorkflowSetupRootState, 'workflowChangeSets'>
      | Pick<WorkflowSetupRootState, 'createNewChange'>
      | Pick<WorkflowSetupRootState, 'parameterData'>
  ) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector => selector({ workflowSetup }));
  };

  afterEach(() => {
    selectorMock.mockClear();
  });

  it('test on selector useWorkflowSetup', () => {
    mockSelectorWorkflowSetup({
      workflowSetup: {
        loading: false,
        error: false,
        isTemplate: true,
        workflow: {
          id: '123'
        } as IWorkflowTemplateModel
      }
    });

    const { result } = renderHook(() => useWorkflowSetup());

    expect(result.current.isTemplate).toEqual(true);
    expect(result.current.workflow!.id).toEqual('123');
  });

  it('test on selector useWorkflowSetupTemplateId', () => {
    mockSelectorWorkflowSetup({
      workflowSetup: { workflow: { id: '123' } as IWorkflowTemplateModel }
    });

    const { result, rerender } = renderHook(() => useWorkflowSetupTemplateId());
    expect(result.current).toEqual('123');

    mockSelectorWorkflowSetup({
      workflowSetup: { workflow: { id: '' } as IWorkflowTemplateModel }
    });
    rerender();
    expect(result.current).toEqual('');
  });

  it('test on selector useWorkflowSetupForms', () => {
    mockSelectorWorkflowSetup({
      workflowFormSteps: {
        selectedStep: 'gettingStart',
        lastStep: 'gettingStart',
        forms: {
          changeInformation: {
            name: 'mock name',
            description: 'mock description',
            workflowNote: 'mock note'
          }
        },
        workflowInfo: {}
      } as any
    });

    const { result } = renderHook(() => useWorkflowSetupForms());

    expect(result.current.changeInformation).toEqual({
      name: 'mock name',
      description: 'mock description',
      workflowNote: 'mock note'
    });
  });

  it('test on selector usePrevSavedWorkflowSetupForms', () => {
    mockSelectorWorkflowSetup({
      workflowFormSteps: {
        selectedStep: 'gettingStart',
        lastStep: 'gettingStart',
        forms: {},
        workflowInfo: {}
      } as any
    });

    const { result, rerender } = renderHook(() =>
      usePrevSavedWorkflowSetupForms()
    );
    expect(result.current.changeInformation).toEqual(undefined);

    mockSelectorWorkflowSetup({
      workflowFormSteps: {
        selectedStep: 'gettingStart',
        lastStep: 'gettingStart',
        forms: {
          changeInformation: {
            name: 'mock name',
            description: 'mock description',
            workflowNote: 'mock note'
          }
        },
        workflowInfo: {}
      } as any
    });

    rerender();
    expect(result.current.changeInformation).toEqual({
      name: 'mock name',
      description: 'mock description',
      workflowNote: 'mock note'
    });

    mockSelectorWorkflowSetup({
      workflowFormSteps: {
        selectedStep: 'change',
        lastStep: 'gettingStart',
        forms: {
          changeInformation: { name: 'name' }
        },
        workflowInfo: {}
      } as any
    });

    rerender();
    expect(result.current.changeInformation).toEqual({ name: 'name' });
  });

  it('test on selector usePrevInvalidSavedWorkflowSetupForms', () => {
    mockSelectorWorkflowSetup({
      workflowFormSteps: {
        selectedStep: 'gettingStart',
        lastStep: 'gettingStart',
        forms: {},
        workflowInfo: {}
      } as any
    });

    const { result, rerender } = renderHook(() =>
      usePrevInvalidSavedWorkflowSetupForms()
    );
    expect(result.current.changeInformation).toEqual(undefined);

    mockSelectorWorkflowSetup({
      workflowFormSteps: {
        selectedStep: 'gettingStart',
        lastStep: 'gettingStart',
        forms: {
          changeInformation: {
            name: 'mock name',
            description: 'mock description',
            workflowNote: 'mock note'
          }
        },
        workflowInfo: {}
      } as any
    });

    rerender();
    expect(result.current.changeInformation).toEqual({
      name: 'mock name',
      description: 'mock description',
      workflowNote: 'mock note'
    });

    mockSelectorWorkflowSetup({
      workflowFormSteps: {
        selectedStep: 'change',
        lastStep: 'gettingStart',
        forms: {
          changeInformation: { name: 'name' }
        },
        workflowInfo: {}
      } as any
    });

    rerender();
    expect(result.current.changeInformation).toEqual({ name: 'name' });
  });

  it('test on selector useCurrentWorkflowSetup', () => {
    mockSelectorWorkflowSetup({
      workflowSetup: { workflow: {} as any }
    });

    const { result } = renderHook(() => useCurrentWorkflowSetup());

    expect(result.current).toEqual({});
  });

  it('test on selector useWorkflowFormStepsSavedAt', () => {
    mockSelectorWorkflowSetup({
      workflowSetup: { workflowFormSteps: {} } as any
    });
    const { result } = renderHook(() => useWorkflowFormStepsSavedAt());
    expect(result.current).toEqual(0);
  });

  it('test on selector useWorkflowFormStepsStuck', () => {
    mockSelectorWorkflowSetup({
      workflowSetup: { workflowFormSteps: {} } as any
    });
    const { result } = renderHook(() => useWorkflowFormStepsStuck('1'));
    expect(result.current).toEqual(false);
  });

  it('test on selector useWorkflowFormStepsHaveBeenSaved', () => {
    mockSelectorWorkflowSetup({
      workflowSetup: { workflowFormSteps: {} } as any
    });
    const { result } = renderHook(() => useWorkflowFormStepsHaveBeenSaved());
    expect(result.current).toEqual(false);
  });

  it('test on selector useWorkflowFormStepsDependency', () => {
    mockSelectorWorkflowSetup({
      workflowFormSteps: {
        selectedStep: 'gettingStart',
        lastStep: 'gettingStart',
        forms: {
          gettingStart: {
            returnedCheckCharges: true,
            lateCharges: false
          }
        },
        workflowInfo: {}
      } as any
    });

    const { result } = renderHook(() =>
      useWorkflowFormStepsDependency([
        'gettingStart.returnedCheckCharges',
        'gettingStart.lateCharges'
      ])
    );

    expect(result.current).toEqual({
      'gettingStart.lateCharges': false,
      'gettingStart.returnedCheckCharges': true
    });
  });

  it('test on selector useWorkflowFormStepsDependency > with equality function', () => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation((selector, equalityFn) => {
        selector({
          workflowSetup: {
            workflowFormSteps: {
              selectedStep: 'gettingStart',
              lastStep: 'gettingStart',
              forms: {
                gettingStart: {
                  returnedCheckCharges: true,
                  lateCharges: false
                }
              }
            }
          }
        });
        equalityFn &&
          equalityFn!(
            { returnedCheckCharges: true },
            { returnedCheckCharges: true }
          );
      });

    const { result } = renderHook(() =>
      useWorkflowFormStepsDependency([
        'gettingStart.returnedCheckCharges',
        'gettingStart.lateCharges'
      ])
    );

    expect(result.current).toEqual(undefined);
  });

  it('test on selector useWorkflowFormStepsDependencyNoCache ', () => {
    mockSelectorWorkflowSetup({
      workflowFormSteps: {
        selectedStep: 'gettingStart',
        lastStep: 'summary',
        forms: {},
        workflowInfo: {}
      } as any
    });

    const { result } = renderHook(() =>
      useWorkflowFormStepsDependencyNoCache(['123'])
    );

    expect(result.current).toEqual({ '123': undefined });
  });

  it('test on selector useLastWorkflowStep', () => {
    mockSelectorWorkflowSetup({
      workflowFormSteps: {
        selectedStep: 'gettingStart',
        lastStep: 'summary',
        forms: {},
        workflowInfo: {}
      } as any
    });

    const { result } = renderHook(() => useLastWorkflowStep());

    expect(result.current).toEqual('summary');
  });

  it('test on selector useSelectedWorkflowStep', () => {
    mockSelectorWorkflowSetup({
      workflowFormSteps: {
        selectedStep: 'gettingStart',
        lastStep: 'summary',
        forms: {},
        workflowInfo: {}
      } as any
    });

    const { result } = renderHook(() => useSelectedWorkflowStep());

    expect(result.current).toEqual('gettingStart');
  });

  it('test on selector useWorkflowFormStep', () => {
    mockSelectorWorkflowSetup({
      workflowFormSteps: {
        selectedStep: 'gettingStart',
        lastStep: 'summary',
        forms: {
          mockForm: 'mock form'
        } as WorkflowSetupStepForm,
        workflowInfo: {}
      } as any
    });

    const { result } = renderHook(() => useWorkflowFormStep('mockForm'));

    expect(result.current).toEqual('mock form');
  });

  it('test on selector useWorkflowFormStepValidate', () => {
    mockSelectorWorkflowSetup({
      workflowFormSteps: {
        selectedStep: 'gettingStart',
        lastStep: 'summary',
        forms: {
          mockForm: {
            isValid: true
          }
        } as WorkflowSetupStepForm,
        workflowInfo: {}
      } as any
    });

    const { result } = renderHook(() =>
      useWorkflowFormStepValidate('mockForm')
    );

    expect(result.current).toEqual(true);

    selectorMock.mockClear();

    mockSelectorWorkflowSetup({
      workflowFormSteps: {
        selectedStep: 'gettingStart',
        lastStep: 'summary',
        forms: {
          mockForm: {}
        } as WorkflowSetupStepForm,
        workflowInfo: {}
      } as any
    });

    const { result: resultDefault } = renderHook(() =>
      useWorkflowFormStepValidate('mockForm')
    );

    expect(resultDefault.current).toEqual(false);
  });

  it('test on selector useWorkflowChangeSetsSelector', () => {
    const mockChangeSetList = mappingDataFromArray(ChangeList, ChangeMapping!);
    const mockChangeSetListError = mappingDataFromArray(
      [
        {
          id: 1708984056,
          name: 'Increase Fees for Black Card',
          changeOwner: {
            id: 1051691920,
            name: 'Freddy Kruger'
          },
          createdDate: undefined,
          effectiveDate: undefined,
          lastChangedDate: undefined,
          lastChangedBy: {
            id: 708119904,
            name: 'Elmer Hall'
          },
          changeType: 'PRICING',
          changeStatus: 'REJECTED',
          portfoliosImpactedCount: 3849,
          cardholderAccountsImpactedCount: 4449,
          pricingStrategiesCreatedCount: 8357,
          pricingStrategiesUpdatedCount: 2336,
          pricingMethodsCreatedCount: 7146,
          pricingMethodsUpdatedCount: 5299,
          dmmTablesCreated: 1378,
          dmmTablesUpdated: 3103,
          pendingApprovalBy: [],
          approvalRecords: [{ action: 'approve' }]
        },
        {
          workflowInstanceCount: 35,
          id: 1150226841,
          name: 'Card Compromise November',
          changeOwner: {
            id: 1160668354,
            name: 'Nate Nash'
          },
          createdDate: undefined,
          effectiveDate: undefined,
          lastChangedDate: undefined,
          lastChangedBy: {
            id: 2072692943,
            name: 'Nate Nash'
          },
          changeType: 'pricing',
          changeStatus: 'VALIDATED',
          portfoliosImpactedCount: 7894,
          cardholderAccountsImpactedCount: 4472,
          pricingStrategiesCreatedCount: 3281,
          pricingStrategiesUpdatedCount: 4361,
          pricingMethodsCreatedCount: 6787,
          pricingMethodsUpdatedCount: 3784,
          dmmTablesCreated: 7628,
          dmmTablesUpdated: 7236,
          pendingApprovalBy: [],
          approvalRecords: [{ action: 'approve' }]
        }
      ],
      ChangeMapping!
    );

    mockSelectorWorkflowSetup({
      workflowChangeSets: {
        loading: undefined,
        error: undefined,
        changeSetList: mockChangeSetList,
        dataFilter: {
          ...defaultWorkflowChangeSetFilter,
          sortBy: [...defaultWorkflowChangeSetFilter.sortBy!, 'changeId']
        }
      }
    });

    const { result } = renderHook(() => useWorkflowChangeSetsSelector());
    expect(result.current.changeSets.length).toEqual(3);

    selectorMock.mockClear();

    mockSelectorWorkflowSetup({
      workflowChangeSets: {
        loading: false,
        error: false,
        changeSetList: mockChangeSetList,
        dataFilter: defaultWorkflowChangeSetFilter,
        newChangeId: 1708984056 as any
      }
    });

    const { result: rsNewChange } = renderHook(() =>
      useWorkflowChangeSetsSelector()
    );
    expect(rsNewChange.current.changeSets.length).toEqual(3);

    selectorMock.mockClear();

    // empty data filter
    mockSelectorWorkflowSetup({
      workflowChangeSets: {
        loading: undefined,
        error: undefined,
        changeSetList: mockChangeSetList,
        dataFilter: undefined
      }
    });

    const { result: resultEmptyFilter } = renderHook(() =>
      useWorkflowChangeSetsSelector()
    );
    expect(resultEmptyFilter.current.changeSets.length).toEqual(0);

    selectorMock.mockClear();

    // data filter with search field
    mockSelectorWorkflowSetup({
      workflowChangeSets: {
        loading: undefined,
        error: undefined,
        changeSetList: mockChangeSetList,
        dataFilter: {
          ...defaultWorkflowChangeSetFilter,
          searchValue: 'Compromise November'
        }
      }
    });

    const { result: resultWithSearchValue } = renderHook(() =>
      useWorkflowChangeSetsSelector()
    );
    expect(resultWithSearchValue.current.changeSets.length).toEqual(2);

    // data filter with empty
    mockSelectorWorkflowSetup({
      workflowChangeSets: {
        loading: undefined,
        error: undefined,
        changeSetList: mockChangeSetListError,
        changeStatusList: undefined,
        dataFilter: {
          ...defaultWorkflowChangeSetFilter,
          sortBy: [...defaultWorkflowChangeSetFilter.sortBy!, 'changeId']
        }
      }
    });

    const { result: resultWithEmpty } = renderHook(() =>
      useWorkflowChangeSetsSelector()
    );
    expect(resultWithEmpty.current.changeSets.length).toEqual(2);
  });

  it('test on selector useWorkflowChangeSetFilterSelector', () => {
    mockSelectorWorkflowSetup({
      workflowChangeSets: {
        loading: undefined,
        error: undefined,
        changeSetList: [],
        dataFilter: {
          ...defaultWorkflowChangeSetFilter,
          searchValue: 'Compromise November'
        }
      }
    });

    const { result } = renderHook(() => useWorkflowChangeSetFilterSelector());
    expect(result.current.searchValue).toEqual('Compromise November');

    selectorMock.mockClear();

    // Empty data filter
    mockSelectorWorkflowSetup({
      workflowChangeSets: {
        loading: undefined,
        error: undefined,
        changeSetList: [],
        dataFilter: undefined
      }
    });

    const { result: resultEmpty } = renderHook(() =>
      useWorkflowChangeSetFilterSelector()
    );

    expect(resultEmpty.current).toEqual({});
  });

  it('test on selector useWorkflowChangeStatusListSelector', () => {
    mockSelectorWorkflowSetup({
      workflowChangeSets: {
        loading: undefined,
        error: undefined,
        changeSetList: [],
        changeStatusList: ['work', 'validated']
      }
    });

    const { result } = renderHook(() => useWorkflowChangeStatusListSelector());
    expect(result.current.length).toEqual(3); // + all

    selectorMock.mockClear();

    // Empty change status list
    mockSelectorWorkflowSetup({
      workflowChangeSets: {
        loading: undefined,
        error: undefined,
        changeSetList: [],
        changeStatusList: undefined
      }
    });

    const { result: resultEmpty } = renderHook(() =>
      useWorkflowChangeStatusListSelector()
    );

    expect(resultEmpty.current).toEqual(['All']);
  });

  it('test on selector useWorkflowChangeSetWithTemplateSelector', () => {
    mockSelectorWorkflowSetup({
      workflowChangeSets: {
        loading: undefined,
        error: undefined,
        changeSetList: [],
        changeSetWithTemplate: ['1', '2', '3']
      }
    });

    const { result } = renderHook(() =>
      useWorkflowChangeSetWithTemplateSelector()
    );
    expect(result.current.length).toEqual(3);

    selectorMock.mockClear();

    // Empty change set with template list
    mockSelectorWorkflowSetup({
      workflowChangeSets: {
        loading: undefined,
        error: undefined,
        changeSetList: [],
        changeSetWithTemplate: undefined
      }
    });

    const { result: resultEmpty } = renderHook(() =>
      useWorkflowChangeSetWithTemplateSelector()
    );

    expect(resultEmpty.current).toEqual([]);
  });

  it('test on selector useSelectWorkflowSetupLoading', () => {
    mockSelectorWorkflowSetup({
      workflowSetup: { loading: true }
    });

    const { result } = renderHook(() => useSelectWorkflowSetupLoading());
    expect(result.current).toEqual(true);
  });

  it('test on selector useSelectWorkflowInstance', () => {
    mockSelectorWorkflowSetup({
      workflowInstance: {
        instance: {} as any
      }
    });

    const { result } = renderHook(() => useSelectWorkflowInstance());

    expect(result.current).toEqual({});
  });

  it('test on selector useSelectElementMetadataStatus', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: {
        loading: true,
        error: false,
        elements: []
      }
    });

    const { result } = renderHook(() => useSelectElementMetadataStatus());

    expect(result.current.loading).toEqual(true);
    expect(result.current.error).toEqual(false);
  });

  it('test on selector useSelectCreateNewChangeStatus', () => {
    mockSelectorWorkflowSetup({
      createNewChange: {
        loading: true,
        error: false
      }
    });

    const { result } = renderHook(() => useSelectCreateNewChangeStatus());

    expect(result.current.loading).toEqual(true);
    expect(result.current.error).toEqual(false);
  });

  it('test on selector useWorkflowInstanceId', () => {
    mockSelectorWorkflowSetup({
      workflowFormSteps: {
        lastStep: '',
        selectedStep: '',
        workflowInfo: {},
        forms: {}
      } as any
    });

    const { result } = renderHook(() => useWorkflowInstanceId());

    expect(result.current.changeId).toBeUndefined();
    expect(result.current.workflowInstanceId).toBeUndefined();

    selectorMock.mockClear();

    mockSelectorWorkflowSetup({
      workflowFormSteps: {
        lastStep: 'get',
        selectedStep: '',
        workflowInfo: {},
        forms: {
          changeInformation: {
            changeSetId: 'mock-change-id',
            workflowInstanceId: 'mock-workflow-instance-id'
          }
        }
      } as any
    });

    const { result: rs1 } = renderHook(() => useWorkflowInstanceId());

    expect(rs1.current.changeId).toEqual('mock-change-id');
    expect(rs1.current.workflowInstanceId).toEqual('mock-workflow-instance-id');
  });

  it('test on selector selectedChangeSetPageSelector', () => {
    const mockChangeSetList = mappingDataFromArray(
      [
        ...ChangeList,
        {
          createdDate: 1
        }
      ],
      ChangeMapping!
    );

    let state = {
      workflowChangeSets: {
        changeSetList: mockChangeSetList,
        dataFilter: {
          ...defaultWorkflowChangeSetFilter,
          changeStatus: 'rejected'
        }
      }
    } as WorkflowSetupRootState;

    const { result } = renderHook(() =>
      selectedChangeSetPageSelector(state, '1708984056')
    );

    expect(result.current).toEqual(1);

    state = {
      workflowChangeSets: {
        changeSetList: mockChangeSetList,
        dataFilter: undefined
      }
    } as WorkflowSetupRootState;

    const { result: rs1 } = renderHook(() =>
      selectedChangeSetPageSelector(state, '1708984056')
    );

    expect(rs1.current).toEqual(1);

    state = {
      workflowChangeSets: {
        changeSetList: mockChangeSetList,
        dataFilter: {
          ...defaultWorkflowChangeSetFilter,
          sortBy: [...defaultWorkflowChangeSetFilter.sortBy!, 'changeId']
        }
      }
    } as WorkflowSetupRootState;

    const { result: rs2 } = renderHook(() =>
      selectedChangeSetPageSelector(state, '1708984056')
    );

    expect(rs2.current).toEqual(1);
  });

  it('test on selector useWorkflowSetupStepTracking', () => {
    const stepTracking: Record<string, WorkflowSetupStepTracking> = {
      test: { doneValidation: 0, inprogress: true }
    };
    mockSelectorWorkflowSetup({ workflowFormSteps: { stepTracking } } as any);

    const { result } = renderHook(() => useWorkflowSetupStepTracking());

    expect(result.current).toEqual(stepTracking);
  });
});
