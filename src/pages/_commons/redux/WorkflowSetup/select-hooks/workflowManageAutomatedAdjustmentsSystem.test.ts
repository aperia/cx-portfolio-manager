import { renderHook } from '@testing-library/react-hooks';
import { WorkflowSetupRootState } from 'app/types';
import * as reactRedux from 'react-redux';
import { useSelectSupervisors } from './workflowManageAutomatedAdjustmentsSystem';

describe('redux-store > workflow-setup > select-hooks ', () => {
  let selectorMock: jest.SpyInstance;
  const mockSelectorWorkflowSetup = (
    workflowSetup:
      | Pick<WorkflowSetupRootState, 'workflowFormSteps'>
      | Pick<WorkflowSetupRootState, 'workflowSetup'>
      | Pick<WorkflowSetupRootState, 'workflowMethods'>
      | Pick<WorkflowSetupRootState, 'workflowInstance'>
      | Pick<WorkflowSetupRootState, 'elementMetadata'>
      | Pick<WorkflowSetupRootState, 'workflowChangeSets'>
      | Pick<WorkflowSetupRootState, 'createNewChange'>
      | Pick<WorkflowSetupRootState, 'parameterData'>
  ) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector => selector({ workflowSetup }));
  };

  afterEach(() => {
    selectorMock.mockClear();
  });

  it('test on selector useSelectSupervisors  ', () => {
    mockSelectorWorkflowSetup({
      supervisorData: {
        supervisorList: {
          loading: true,
          error: false
        }
      }
    } as any);

    const { result } = renderHook(() => useSelectSupervisors());

    expect(result.current.loading).toEqual(true);
    expect(result.current.error).toEqual(false);
  });
});
