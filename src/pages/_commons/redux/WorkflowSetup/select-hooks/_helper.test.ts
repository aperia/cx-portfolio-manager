import {
  findByName,
  mapCodeAndTextToText,
  mapNameAndTextToText,
  mapValueAndDescToRefItem
} from './_helper';

describe('_commons > redux > WorkflowSetup > select-hooks > helper', () => {
  it('mapValueAndDescToRefItem', () => {
    let result = mapValueAndDescToRefItem({
      value: 'value',
      description: 'description',
      rest: { code: 'code' }
    });
    expect(result.code).toEqual('value');

    result = mapValueAndDescToRefItem({
      value: 'value',
      description: '',
      rest: { code: 'code' }
    });
    expect(result.code).toEqual('value');
  });

  it('mapCodeAndTextToText', () => {
    let result = mapCodeAndTextToText({
      code: 'value',
      text: 'description',
      rest: { code: 'code' }
    });
    expect(result.code).toEqual('value');

    result = mapCodeAndTextToText({
      code: '',
      text: 'description',
      rest: { code: 'code' }
    });
    expect(result.code).toEqual('');
  });

  it('findByName', () => {
    const result = findByName('value')({ text: 'text', name: 'name' });
    expect(result).toEqual(false);
  });

  it('mapNameAndTextToText', () => {
    let result = mapNameAndTextToText({
      code: 'value',
      text: 'description',
      name: 'name',
      rest: {
        code: 'code'
      }
    });
    expect(result.code).toEqual('value');

    result = mapNameAndTextToText({
      code: 'value',
      text: 'description',
      name: '',
      rest: {
        code: 'code'
      }
    });
    expect(result.code).toEqual('value');
  });
});
