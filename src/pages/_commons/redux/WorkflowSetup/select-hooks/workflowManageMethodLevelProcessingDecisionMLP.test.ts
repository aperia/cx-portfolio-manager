import { renderHook } from '@testing-library/react-hooks';
import { WorkflowMetadataAll } from 'app/fixtures/workflow-metadata';
import { WorkflowSetupRootState } from 'app/types';
import { useSelectElementMetadataManageMethodLevelProcessingMLP } from 'pages/_commons/redux/WorkflowSetup';
import * as reactRedux from 'react-redux';

describe('redux-store > workflow-setup > select-hooks ', () => {
  let selectorMock: jest.SpyInstance;
  const mockSelectorWorkflowSetup = (
    workflowSetup:
      | Pick<WorkflowSetupRootState, 'workflowFormSteps'>
      | Pick<WorkflowSetupRootState, 'workflowSetup'>
      | Pick<WorkflowSetupRootState, 'workflowMethods'>
      | Pick<WorkflowSetupRootState, 'workflowInstance'>
      | Pick<WorkflowSetupRootState, 'elementMetadata'>
      | Pick<WorkflowSetupRootState, 'workflowChangeSets'>
      | Pick<WorkflowSetupRootState, 'createNewChange'>
      | Pick<WorkflowSetupRootState, 'parameterData'>
  ) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector => selector({ workflowSetup }));
  };

  afterEach(() => {
    selectorMock.mockClear();
  });

  it('test on selector useSelectElementMetadataManageMethodLevelProcessingMLP', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: { elements: WorkflowMetadataAll }
    } as any);

    const { rerender, result } = renderHook(() =>
      useSelectElementMetadataManageMethodLevelProcessingMLP()
    );

    const expectResult = {
      serviceSubjectSectionAreas:
        result.current.serviceSubjectSectionAreas.length,
      searchCode: result.current.searchCode.length,
      allocationFlag: result?.current?.allocationFlag?.length,
      allocationInterval: result.current.allocationInterval.length,
      allocationBeforeAfterCycle:
        result.current.allocationBeforeAfterCycle.length,
      changeCode: result.current.changeCode.length,
      changeInTermsMethodFlag: result.current.changeInTermsMethodFlag.length
    };

    expect(expectResult).toEqual({
      serviceSubjectSectionAreas: 2,
      searchCode: 2,
      allocationFlag: undefined,
      allocationInterval: 3,
      allocationBeforeAfterCycle: 2,
      changeCode: 2,
      changeInTermsMethodFlag: 2
    });

    mockSelectorWorkflowSetup({
      elementMetadata: { elements: [] }
    } as any);
    rerender();
    expect(result.current.searchCode.length).toEqual(0);
  });

  it('test on selector useSelectElementMetadataManageMethodLevelProcessingMLP with empty data', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: { elements: [] }
    } as any);

    const { result } = renderHook(() =>
      useSelectElementMetadataManageMethodLevelProcessingMLP()
    );

    expect(result.current.searchCode.length).toEqual(0);
  });

  it('test on selector useSelectElementMetadataManageMethodLevelProcessingMLP with empty data', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: {
        elements: [
          {
            id: '5126',
            name: 'mlp.table',
            options: [
              {
                code: 'AQ',
                description: 'Account Qualification'
              },
              {
                code: 'CA',
                description: 'Client Allocation'
              }
            ]
          },
          {
            id: '5126',
            name: 'allocation.before.after.cycle',
            options: [
              {
                code: 'AQ',
                description: 'Account Qualification'
              },
              {
                code: 'CA',
                description: 'Client Allocation'
              }
            ]
          },
          {
            id: '5127',
            name: 'change.code',
            options: [
              {
                code: 'AQ',
                description: 'Account Qualification'
              },
              {
                code: 'CA',
                description: 'Client Allocation'
              }
            ]
          },
          {
            id: '5128',
            name: 'service.subject.section.areas',
            options: [
              {
                code: 'AQ',
                description: 'Account Qualification'
              },
              {
                code: 'CA',
                description: 'Client Allocation'
              }
            ]
          },
          {
            id: '5126',
            name: 'change.in.terms.method.flag',
            options: [
              {
                code: 'AQ',
                description: 'Account Qualification'
              },
              {
                code: 'CA',
                description: 'Client Allocation'
              }
            ]
          },
          {
            id: '5126',
            name: 'ca.allocation.flag',
            options: [
              {
                code: 'AQ',
                description: 'Account Qualification'
              },
              {
                code: 'CA',
                description: 'Client Allocation'
              }
            ]
          },
          {
            id: '5124',
            name: 'aq.allocation.flag',
            options: [
              {
                code: 'AQ',
                description: 'Account Qualification'
              },
              {
                code: 'CA',
                description: 'Client Allocation'
              }
            ]
          },
          {
            id: '5123',
            name: 'text.area',
            options: [
              {
                code: 'AQ',
                description: 'Account Qualification'
              },
              {
                code: 'CA',
                description: 'Client Allocation'
              }
            ]
          },
          {
            id: '5125',
            name: 'table.area',
            options: [
              {
                code: 'AQ',
                description: 'Account Qualification'
              },
              {
                code: 'CA',
                description: 'Client Allocation'
              }
            ]
          },
          {
            id: '331',
            name: 'alp.table',
            options: [
              {
                code: 'AQ',
                description: 'Account Qualification'
              },
              {
                code: 'CA',
                description: 'Client Allocation'
              }
            ]
          }
        ]
      }
    } as any);

    const { result } = renderHook(() =>
      useSelectElementMetadataManageMethodLevelProcessingMLP()
    );

    expect(result.current.searchCode.length).toEqual(0);
  });
});
