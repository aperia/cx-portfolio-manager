import { createSelector } from '@reduxjs/toolkit';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import { camelToText, matchSearchValue, textToCamel } from 'app/helpers';
import {
  IChangeManagementFilter,
  WorkflowSetupRootState,
  WorkflowSetupState,
  WorkflowSetupStepForm
} from 'app/types';
import { orderBy } from 'app/_libraries/_dls/lodash';
import { isString, isUndefined } from 'lodash';
import get from 'lodash.get';
import _orderBy from 'lodash.orderby';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

export const KeepTextDelayDesignPromotion = [
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter4.toString(),
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter10.toString(),
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter11.toString(),
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter12.toString(),
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter13.toString(),
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter14.toString(),
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter15.toString(),
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter16.toString(),
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter17.toString(),
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter18.toString(),
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter19.toString(),
  MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter28.toString()
];

const workflowSetupSelector = createSelector(
  (state: RootState) => state.workflowSetup!.workflowSetup,
  data => data
);
export const useWorkflowSetup = () => {
  return useSelector<RootState, WorkflowSetupState>(workflowSetupSelector);
};
export const workflowSetupTemplateIdSelector = createSelector(
  (state: RootState) => state.workflowSetup?.workflowSetup?.workflow,
  data =>
    ((data as IWorkflowModel)?.workflowTemplateId || data?.id)?.toString() || ''
);
export const useWorkflowSetupTemplateId = () => {
  return useSelector<RootState, string>(workflowSetupTemplateIdSelector);
};
const workflowFormStepsSelector = createSelector(
  (state: RootState) => state.workflowSetup!.workflowFormSteps,
  data => data
);
const workflowSetupFormsSelector = createSelector(
  workflowFormStepsSelector,
  data => data.forms
);
const workflowSetupStepTrackingSelector = createSelector(
  (state: RootState) => state.workflowSetup!.workflowFormSteps.stepTracking,
  data => data
);
export const useWorkflowSetupStepTracking = () => {
  return useSelector(workflowSetupStepTrackingSelector);
};
export const useWorkflowSetupForms = () => {
  return useSelector<RootState, WorkflowSetupStepForm>(
    workflowSetupFormsSelector
  );
};

export const usePrevSavedWorkflowSetupForms = () => {
  const selectedStep = useSelectedWorkflowStep();
  const savedAt = useWorkflowFormStepsSavedAt();
  const _forms = useWorkflowSetupForms();

  const [forms, setForms] = useState(_forms);
  const [isDefaultValue, setIsDefaultValue] = useState(
    isUndefined(_forms[Object.keys(_forms)[0]])
  );
  useEffect(() => {
    setIsDefaultValue(value =>
      !value ? value : isUndefined(_forms[Object.keys(_forms)[0]])
    );
  }, [_forms]);

  useEffect(() => {
    setForms(_forms);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedStep, savedAt, isDefaultValue]);

  return forms;
};
export const usePrevInvalidSavedWorkflowSetupForms = () => {
  const selectedStep = useSelectedWorkflowStep();
  const savedAt = useWorkflowFormStepsLastSavedInvalidAt();
  const _forms = useWorkflowSetupForms();

  const [forms, setForms] = useState(_forms);
  const [isDefaultValue, setIsDefaultValue] = useState(
    isUndefined(_forms[Object.keys(_forms)[0]])
  );
  useEffect(() => {
    setIsDefaultValue(isDefaultValue =>
      !isDefaultValue
        ? isDefaultValue
        : isUndefined(_forms[Object.keys(_forms)[0]])
    );
  }, [_forms]);

  useEffect(() => {
    setForms(_forms);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedStep, savedAt, isDefaultValue]);

  return forms;
};
const currentWorkflowSetupSelector = createSelector(
  (state: RootState) => state.workflowSetup!.workflowSetup?.workflow,
  data => data!
);
export const useCurrentWorkflowSetup = () => {
  return useSelector<RootState, IWorkflowModel | IWorkflowTemplateModel>(
    currentWorkflowSetupSelector
  );
};
const workflowFormStepsDependencySelector = createSelector(
  (formSteps: WorkflowSetupStepForm, deps: string[]) => {
    return deps.reduceRight((pre, cur) => {
      const paths = cur.split('.');
      pre[cur] = get(formSteps, paths);
      return pre;
    }, {} as Record<string, any>);
  },
  deps => deps
);
const selectWorkflowFormStepsSavedAt = createSelector<
  RootState,
  number | undefined,
  number
>(
  state => state.workflowSetup?.workflowFormSteps?.savedAt,
  savedAt => savedAt || 0
);
export const useWorkflowFormStepsSavedAt = () =>
  useSelector(selectWorkflowFormStepsSavedAt);

const selectWorkflowFormStepsStuck = createSelector<
  RootState,
  Record<string, boolean> | undefined,
  Record<string, boolean>
>(
  state => state.workflowSetup?.workflowFormSteps?.isStuck,
  isStuck => isStuck || {}
);
export const useWorkflowFormStepsStuck = (stepId: string) => {
  const stucks = useSelector(selectWorkflowFormStepsStuck);

  return stucks[stepId] || false;
};

const selectWorkflowFormStepsLastSavedInvalidAt = createSelector<
  RootState,
  number | undefined,
  number
>(
  state => state.workflowSetup?.workflowFormSteps?.lastSavedInvalidAt,
  savedInvalidAt => savedInvalidAt || 0
);
export const useWorkflowFormStepsLastSavedInvalidAt = () =>
  useSelector(selectWorkflowFormStepsLastSavedInvalidAt);

const selectWorkflowFormStepsHaveBeenSaved = createSelector<
  RootState,
  boolean | undefined,
  boolean
>(
  state => state.workflowSetup?.workflowFormSteps?.haveBeenSaved,
  haveBeenSaved => haveBeenSaved || false
);
export const useWorkflowFormStepsHaveBeenSaved = () =>
  useSelector(selectWorkflowFormStepsHaveBeenSaved);
export const useWorkflowFormStepsDependency = (deps: string[]) => {
  const selectedStep = useSelectedWorkflowStep();
  const savedAt = useSelector(selectWorkflowFormStepsSavedAt);
  const formDependencies = useSelector<RootState, Record<string, any>>(
    state =>
      workflowFormStepsDependencySelector(
        workflowSetupFormsSelector(state),
        deps
      ),
    (pre, next) => Object.keys(pre).every(k => pre[k] === next[k])
  );

  const [stepDependencies, setStepDependencies] = useState(formDependencies);

  useEffect(() => {
    setStepDependencies(formDependencies);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedStep, savedAt]);

  return stepDependencies;
};
export const useWorkflowFormStepsDependencyNoCache = (deps: string[]) => {
  const formDependencies = useSelector<RootState, Record<string, any>>(state =>
    workflowFormStepsDependencySelector(workflowSetupFormsSelector(state), deps)
  );

  return formDependencies;
};
const lastWorkflowStepSelector = createSelector(
  [workflowFormStepsSelector],
  data => data.lastStep
);
export const useLastWorkflowStep = () => {
  return useSelector<RootState, string>(lastWorkflowStepSelector);
};
const selectedWorkflowStepSelector = createSelector(
  [workflowFormStepsSelector],
  data => data.selectedStep
);
export const useSelectedWorkflowStep = () => {
  return useSelector<RootState, string>(selectedWorkflowStepSelector);
};
const workflowFormStepSelector = createSelector(
  (state: RootState, name: string) =>
    state.workflowSetup!.workflowFormSteps.forms[name],
  data => data
);
export const useWorkflowFormStep = (name: string) => {
  return useSelector<RootState, WorkflowSetupStepForm>(state =>
    workflowFormStepSelector(state, name)
  );
};
const workflowFormStepValidateSelector = createSelector(
  (state: RootState, name: string) =>
    state.workflowSetup!.workflowFormSteps.forms[name]?.isValid || false,
  data => data
);
export const useWorkflowFormStepValidate = (name: string) => {
  return useSelector<RootState, boolean>(state =>
    workflowFormStepValidateSelector(state, name)
  );
};

const sortChangeSetsNumberFields = ['changeId'];
const workflowChangeSetsSelector = createSelector(
  (state: RootState) => state.workflowSetup!.workflowChangeSets,
  data => {
    const {
      newChangeId,
      changeSetList: rawData,
      dataFilter = {},
      loading = false,
      error = false
    } = data;
    const {
      sortBy,
      orderBy,
      page,
      pageSize,
      searchValue = '',
      changeStatus
    } = dataFilter;
    const _searchValue = searchValue.toLowerCase() || '';

    const changeStatusCamel = textToCamel(changeStatus)?.toLowerCase();

    const _changeSets = _orderBy(
      rawData,
      sortBy?.map(sort => (item: any) => {
        const number = Number(item[sort]);

        if (sortChangeSetsNumberFields.includes(sort) && !isNaN(number)) {
          return number;
        }

        return isString(item[sort])
          ? item[sort]?.toLowerCase()?.trim()
          : item[sort];
      }),
      orderBy
    )
      .filter(
        i =>
          changeStatusCamel === 'all' ||
          i.changeStatus?.toLowerCase() === changeStatusCamel
      )
      .filter(i => {
        return matchSearchValue(
          [i.changeName, i.changeId, i.changeOwner],
          _searchValue
        );
      });
    const total = rawData.length;
    const totalItemFilter = _changeSets.length;
    const changeSets = _changeSets.slice(
      (page! - 1) * pageSize!,
      page! * pageSize!
    );
    const newChange = changeSets.find(c => c.changeId === newChangeId);
    const changeRest = changeSets.filter(c => c.changeId !== newChangeId);
    return {
      allChangeSets: rawData,
      changeSets: newChange ? [newChange, ...changeRest] : changeSets,
      total,
      totalItemFilter,
      loading,
      error
    };
  }
);

export const selectedChangeSetPageSelector = createSelector(
  (state: WorkflowSetupRootState, changeId: string) => {
    const { changeSetList: rawData, dataFilter = {} } =
      state.workflowChangeSets;
    const {
      sortBy,
      orderBy,
      pageSize = 10,
      searchValue = '',
      changeStatus
    } = dataFilter;
    const _searchValue = searchValue.toLowerCase() || '';

    const changeStatusCamel = textToCamel(changeStatus)?.toLowerCase();

    const changeSets = _orderBy(
      rawData,
      sortBy?.map(sort => (item: any) => {
        const number = Number(item[sort]);

        if (sortChangeSetsNumberFields.includes(sort) && !isNaN(number)) {
          return number;
        }

        return isString(item[sort])
          ? item[sort]?.toLowerCase()?.trim()
          : item[sort];
      }),
      orderBy
    )
      .filter(
        i =>
          changeStatusCamel === 'all' ||
          i.changeStatus?.toLowerCase() === changeStatusCamel
      )
      .filter(i => {
        return matchSearchValue(
          [i.changeName, i.changeId, i.changeOwner],
          _searchValue
        );
      });

    const selectedPage = Math.ceil(
      (changeSets.findIndex(c => c.changeId === changeId) + 1) / pageSize
    );
    return selectedPage || 1;
  },
  page => page
);
export const useWorkflowChangeSetsSelector = () => {
  return useSelector<
    RootState,
    {
      allChangeSets: IWorkflowChangeDetail[];
      changeSets: IWorkflowChangeDetail[];
      loading: boolean;
      error: boolean;
      total: number;
      totalItemFilter: number;
    }
  >(workflowChangeSetsSelector);
};

const workflowChangeStatusListSelector = createSelector(
  (state: RootState) =>
    state.workflowSetup!.workflowChangeSets.changeStatusList,
  data => {
    return ['All', ...orderBy(data || [])].map(s => camelToText(s));
  }
);
export const useWorkflowChangeStatusListSelector = () => {
  return useSelector<RootState, string[]>(workflowChangeStatusListSelector);
};

const workflowChangeSetWithTemplateSelector = createSelector(
  (state: RootState) =>
    state.workflowSetup!.workflowChangeSets.changeSetWithTemplate,
  data => {
    return data || [];
  }
);
export const useWorkflowChangeSetWithTemplateSelector = () => {
  return useSelector<RootState, string[]>(
    workflowChangeSetWithTemplateSelector
  );
};

const workflowChangeSetFilterSelector = createSelector(
  (state: RootState) => state.workflowSetup!.workflowChangeSets.dataFilter,
  data => data || {}
);
export const useWorkflowChangeSetFilterSelector = () => {
  return useSelector<RootState, IChangeManagementFilter>(
    workflowChangeSetFilterSelector
  );
};

export const useSelectWorkflowSetupLoading = () => {
  return useSelector<RootState, boolean>(
    createSelector(
      (state: RootState) => state.workflowSetup!.workflowInstance?.loading,
      (state: RootState) => state.workflowSetup!.workflowSetup?.loading,
      (loadingInstance, loadingSetup) => !!loadingInstance || !!loadingSetup
    )
  );
};

export const useSelectWorkflowInstance = () => {
  return useSelector<RootState, WorkflowSetupInstance | undefined>(
    createSelector(
      (state: RootState) => state.workflowSetup!.workflowInstance?.instance,
      data => data
    )
  );
};

export const useSelectElementMetadataStatus = () => {
  return useSelector<RootState, IFetching>(
    createSelector(
      (state: RootState) => state.workflowSetup!.elementMetadata,
      data => {
        return {
          loading: data.loading,
          error: data.error
        };
      }
    )
  );
};

const selectCreateNewChangeStatus = createSelector(
  (state: RootState) => state.workflowSetup!.createNewChange,
  data => {
    return {
      loading: data.loading,
      error: data.error
    };
  }
);
export const useSelectCreateNewChangeStatus = () => {
  return useSelector<RootState, IFetching>(selectCreateNewChangeStatus);
};

export const selectWorkflowInstanceInfo = createSelector(
  (state: RootState) => state.workflowSetup!.workflowFormSteps,
  data => {
    const { changeSetId, workflowInstanceId, isCompletedWorkflow } =
      (data.forms['changeInformation'] as FormChangeInfo) || {};
    return {
      changeId: changeSetId,
      workflowInstanceId,
      isCompletedWorkflow
    };
  }
);
export const useWorkflowInstanceId = () => {
  return useSelector<
    RootState,
    {
      changeId?: string;
      workflowInstanceId?: string;
      isCompletedWorkflow?: boolean;
    }
  >(selectWorkflowInstanceInfo);
};
