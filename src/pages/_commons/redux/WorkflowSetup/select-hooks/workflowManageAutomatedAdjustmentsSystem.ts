import { createSelector } from '@reduxjs/toolkit';
import { useSelector } from 'react-redux';

const supervisorSelector = createSelector(
  (state: RootState) => state.workflowSetup!.supervisorData?.supervisorList,
  data => data
);
export const useSelectSupervisors = () => {
  return useSelector<RootState, ISupervisor[]>(supervisorSelector);
};
