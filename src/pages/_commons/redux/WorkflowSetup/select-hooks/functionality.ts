import { createSelector } from '@reduxjs/toolkit';
import { orderBy } from 'app/_libraries/_dls/lodash';
import { useSelector } from 'react-redux';

const functionalitySelector = createSelector(
  (state: RootState) => state.workflowSetup!.elementMetadata.elements,
  data => {
    const elements = data.map(e => {
      return {
        name: e.name,
        options: orderBy(
          e.options?.map(opt => {
            const des = opt.description ? ` - ${opt.description}` : '';
            const text = opt.value ? `${opt.value}${des}` : opt.description;

            return {
              code: opt.value,
              text: text,
              orderText: opt.value ? text : '0',
              selected: opt.selected
            };
          }),
          ['orderText'],
          ['asc']
        )
      } as {
        name: string;
        options: RefData[];
      };
    });

    const systemRef =
      elements.find(e => e.name === 'chd.system.no')?.options || [];

    const prinRef =
      elements.find(e => e.name === 'chd.prin.bank')?.options || [];

    const agentRef =
      elements.find(e => e.name === 'chd.agent.bank')?.options || [];
    return {
      systemRef,
      prinRef,
      agentRef
    };
  }
);

export const useSelectElementMetadataForfunctionality = () => {
  return useSelector<
    RootState,
    {
      systemRef: RefData[];
      prinRef: RefData[];
      agentRef: RefData[];
    }
  >(functionalitySelector);
};
