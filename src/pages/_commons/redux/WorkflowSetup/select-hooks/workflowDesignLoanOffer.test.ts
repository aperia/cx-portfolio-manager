import { renderHook } from '@testing-library/react-hooks';
import { WorkflowMetadataAll } from 'app/fixtures/workflow-metadata';
import {
  GetMethodListData,
  GetMethodListDataMapping
} from 'app/fixtures/workflow-method-list';
import { mappingDataFromArray } from 'app/helpers';
import { WorkflowSetupRootState } from 'app/types';
import { useSelectElementMetadataForDLO } from 'pages/_commons/redux/WorkflowSetup';
import * as reactRedux from 'react-redux';
import { useSelectWorkflowMethodsDLOSelector } from '.';
import { defaultWorkflowMethodsFilter } from '../reducers';

describe('redux-store > workflow-setup > select-hooks ', () => {
  let selectorMock: jest.SpyInstance;
  const mockSelectorWorkflowSetup = (
    workflowSetup:
      | Pick<WorkflowSetupRootState, 'workflowFormSteps'>
      | Pick<WorkflowSetupRootState, 'workflowSetup'>
      | Pick<WorkflowSetupRootState, 'workflowMethods'>
      | Pick<WorkflowSetupRootState, 'workflowInstance'>
      | Pick<WorkflowSetupRootState, 'elementMetadata'>
      | Pick<WorkflowSetupRootState, 'workflowChangeSets'>
      | Pick<WorkflowSetupRootState, 'createNewChange'>
      | Pick<WorkflowSetupRootState, 'parameterData'>
  ) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector => selector({ workflowSetup }));
  };

  afterEach(() => {
    selectorMock.mockClear();
  });

  it('test on selector useSelectElementMetadataForDLO', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: { elements: WorkflowMetadataAll }
    } as any);

    const { rerender, result } = renderHook(() =>
      useSelectElementMetadataForDLO()
    );

    const expectResult = {
      promotionDescription: result.current.promotionDescription.length,
      promotionAlternateDescription:
        result.current.promotionAlternateDescription.length,
      promotionDescriptionDisplayOptions:
        result.current.promotionDescriptionDisplayOptions.length,
      promotionStatementTextIDDTDD:
        result.current.promotionStatementTextIDDTDD.length,
      promotionStatementTextIDDTDX:
        result.current.promotionStatementTextIDDTDX.length,
      promotionStatementTextIDStandard:
        result.current.promotionStatementTextIDStandard.length,
      promotionStatementTextControl:
        result.current.promotionStatementTextControl.length,
      promotionReturnApplicationOption:
        result.current.promotionReturnApplicationOption.length,
      promotionPayoffExceptionOption:
        result.current.promotionPayoffExceptionOption.length,
      loanBalanceClassification:
        result.current.loanBalanceClassification.length,
      payoffExceptionMethod: result.current.payoffExceptionMethod.length,
      cashAdvanceItemFeesMethod:
        result.current.cashAdvanceItemFeesMethod.length,
      merchantItemFeesMethod: result.current.merchantItemFeesMethod.length,
      creditApplicationGroup: result.current.creditApplicationGroup.length,
      promotionGroupIdentifier: result.current.promotionGroupIdentifier.length,
      standardMinimumPaymentCalculationCode:
        result.current.standardMinimumPaymentCalculationCode.length,
      rulesMinimumPaymentDueMethod:
        result.current.rulesMinimumPaymentDueMethod.length,
      standardMinimumPaymentsRate:
        result.current.standardMinimumPaymentsRate.length,
      standardMinimumPaymentRoundingOptions:
        result.current.standardMinimumPaymentRoundingOptions.length,
      annualInterestRate: result.current.annualInterestRate.length,
      payoutPeriod: result.current.payoutPeriod.length,
      merchantDiscount: result.current.merchantDiscount.length,
      merchantDiscountCalculationCode:
        result.current.merchantDiscountCalculationCode.length,
      returnToRevolvingBaseInterest:
        result.current.returnToRevolvingBaseInterest.length,
      applyPenaltyPricingOnLoanOffer:
        result.current.applyPenaltyPricingOnLoanOffer.length,
      defaultInterestRateMethodOverride:
        result.current.defaultInterestRateMethodOverride.length,
      interestCalculationMethodOverride:
        result.current.interestCalculationMethodOverride.length,
      introductoryMessageDisplayControl:
        result.current.introductoryMessageDisplayControl.length,
      positiveAmortizationUsageCode:
        result.current.positiveAmortizationUsageCode.length,
      introductoryAnnualRate: result.current.introductoryAnnualRate.length,
      introductoryAnnualRateFixedEndDate:
        result.current.introductoryAnnualRateFixedEndDate.length,
      introductoryRateNumberOfDays:
        result.current.introductoryRateNumberOfDays.length,
      introductoryRateNumberOfCycles:
        result.current.introductoryRateNumberOfCycles.length,
      introductoryPeriodStatementTextID:
        result.current.introductoryPeriodStatementTextID.length,
      sameAsCashLoanOfferNumberOfCycles:
        result.current.sameAsCashLoanOfferNumberOfCycles.length,
      accrueInterestOnUnbilledInterest:
        result.current.accrueInterestOnUnbilledInterest.length,
      delayPaymentForStatementCycles:
        result.current.delayPaymentForStatementCycles.length,
      delayPaymentForMonths: result.current.delayPaymentForMonths.length
    };

    expect(expectResult).toEqual({
      accrueInterestOnUnbilledInterest: 3,
      annualInterestRate: 0,
      applyPenaltyPricingOnLoanOffer: 4,
      cashAdvanceItemFeesMethod: 4,
      creditApplicationGroup: 12,
      defaultInterestRateMethodOverride: 4,
      delayPaymentForMonths: 0,
      delayPaymentForStatementCycles: 0,
      interestCalculationMethodOverride: 4,
      introductoryAnnualRate: 0,
      introductoryAnnualRateFixedEndDate: 0,
      introductoryMessageDisplayControl: 4,
      introductoryPeriodStatementTextID: 4,
      introductoryRateNumberOfCycles: 0,
      introductoryRateNumberOfDays: 0,
      loanBalanceClassification: 0,
      merchantDiscount: 0,
      merchantDiscountCalculationCode: 4,
      merchantItemFeesMethod: 4,
      payoffExceptionMethod: 4,
      payoutPeriod: 0,
      positiveAmortizationUsageCode: 7,
      promotionAlternateDescription: 0,
      promotionDescription: 0,
      promotionDescriptionDisplayOptions: 6,
      promotionGroupIdentifier: 4,
      promotionPayoffExceptionOption: 5,
      promotionReturnApplicationOption: 5,
      promotionStatementTextControl: 4,
      promotionStatementTextIDDTDD: 10,
      promotionStatementTextIDDTDX: 10,
      promotionStatementTextIDStandard: 14,
      returnToRevolvingBaseInterest: 3,
      rulesMinimumPaymentDueMethod: 4,
      sameAsCashLoanOfferNumberOfCycles: 0,
      standardMinimumPaymentCalculationCode: 14,
      standardMinimumPaymentRoundingOptions: 3,
      standardMinimumPaymentsRate: 0
    });

    mockSelectorWorkflowSetup({
      elementMetadata: { elements: [] }
    } as any);
    rerender();
    expect(result.current.promotionDescription.length).toEqual(0);
  });

  it('test on selector useSelectWorkflowMethodsDLOSelector ', () => {
    const mockMethodList = mappingDataFromArray(
      GetMethodListData,
      GetMethodListDataMapping!
    );

    mockSelectorWorkflowSetup({
      workflowMethods: {
        loading: undefined,
        error: undefined,
        methods: mockMethodList,
        dataFilter: defaultWorkflowMethodsFilter
      }
    });

    const { result } = renderHook(() => useSelectWorkflowMethodsDLOSelector());

    expect(result.current.methods.length).toEqual(2);
  });

  it('test on selector useSelectWorkflowMethodsDLOSelector  > empty data filter', () => {
    const mockMethodList = mappingDataFromArray(
      GetMethodListData,
      GetMethodListDataMapping!
    );

    mockSelectorWorkflowSetup({
      workflowMethods: {
        loading: undefined,
        error: undefined,
        methods: mockMethodList,
        dataFilter: undefined
      }
    });

    const { result } = renderHook(() => useSelectWorkflowMethodsDLOSelector());

    expect(result.current.methods.length).toEqual(0);
  });
});
