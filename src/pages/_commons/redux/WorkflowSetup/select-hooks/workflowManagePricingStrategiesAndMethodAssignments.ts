import { createSelector } from '@reduxjs/toolkit';
import { orderBy } from 'app/_libraries/_dls/lodash';
import { useSelector } from 'react-redux';

const methodsData = createSelector(
  (state: RootState) => state.workflowSetup!.strategyData?.methods,
  data => data || {}
);
export const useSelectMethods = () => {
  return useSelector<RootState, Record<string, string[]>>(methodsData);
};

const strategyData = createSelector(
  (state: RootState) => state.workflowSetup!.strategyData?.strategyList,
  data => orderBy(data, ['name'])
);
export const useSelectStrategyData = () => {
  return useSelector<RootState, Strategy[]>(strategyData);
};

const strategyDataFetchingStatusSelector = createSelector(
  (state: RootState) => state.workflowSetup!.strategyData?.error,
  (state: RootState) => state.workflowSetup!.strategyData?.loading,
  (error, loading) => ({ error, loading })
);
export const useSelectStrategyDataFetchingStatus = () => {
  return useSelector<RootState, IFetching>(strategyDataFetchingStatusSelector);
};
