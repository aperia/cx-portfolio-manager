import { createSelector } from '@reduxjs/toolkit';
import { useSelector } from 'react-redux';

const elementMetadataForForceEmbossReplacementCardsSelector = createSelector(
  (state: RootState) => state.workflowSetup!.elementMetadata.elements,
  data => {
    const elements = data.map(e => {
      return {
        name: e.name,
        options: e.options?.map(opt => ({
          ...opt,
          code: opt.value,
          text: opt?.value
            ? `${opt.value} - ${opt?.description || ''}`
            : opt.description || '',
          workFlow: opt.workFlow
        }))
      } as {
        name: string;
        options: (AssignPricingStrategiesMetaDataOption & RefData)[];
      };
    });

    const optionConfig =
      elements?.find(e => e.name === 'transaction.id')?.options || [];

    const configuration = optionConfig.filter(
      x => !x.workFlow || x?.workFlow === 'emboss'
    );

    const transaction =
      elements.find(e => e.name === 'pid.194.transaction.id')?.options || [];

    const PID194Code =
      elements.find(e => e.name === 'pid.194.code')?.options || [];

    const PID194Transaction =
      elements.find(e => e.name === 'pid.194.transaction')?.options || [];

    const PID200Code =
      elements.find(e => e.name === 'pid.200.code')?.options || [];

    const PID200Transaction =
      elements.find(e => e.name === 'pid.200.transaction')?.options || [];

    const NM65Transaction =
      elements.find(e => e.name === 'nm.65.transaction')?.options || [];

    return {
      configuration,
      transaction,
      PID194Code,
      PID200Code,
      PID194Transaction,
      PID200Transaction,
      NM65Transaction
    };
  }
);

export const useSelectElementMetadataForForceEmbossReplacementCards = () => {
  return useSelector<
    RootState,
    {
      configuration: (AssignPricingStrategiesMetaDataOption & RefData)[];
      transaction: (AssignPricingStrategiesMetaDataOption & RefData)[];
      PID194Code: (AssignPricingStrategiesMetaDataOption & RefData)[];
      PID200Code: (AssignPricingStrategiesMetaDataOption & RefData)[];
      PID194Transaction: (AssignPricingStrategiesMetaDataOption & RefData)[];
      PID200Transaction: (AssignPricingStrategiesMetaDataOption & RefData)[];
      NM65Transaction: (AssignPricingStrategiesMetaDataOption & RefData)[];
    }
  >(elementMetadataForForceEmbossReplacementCardsSelector);
};
