import { createSelector } from '@reduxjs/toolkit';
import { MethodFieldParameterEnum as MethodField } from 'app/constants/enums';
import { WorkflowManualAccountDeletionMetaParameters } from 'pages/WorkflowManualAccountDeletion/ConfigureParametersStep/constant';
import { useSelector } from 'react-redux';
import { findByName } from './_helper';

const elementMetadataForManualAccountDeletionSelector = createSelector(
  (state: RootState) => state.workflowSetup!.elementMetadata.elements,
  data => {
    const elements = data
      ?.filter(f =>
        WorkflowManualAccountDeletionMetaParameters.includes(
          f.name as MethodField
        )
      )
      .map(e => {
        const options = e.options?.map(opt => {
          const des = opt.description ? ` - ${opt.description}` : '';
          let text = opt.value ? `${opt.value}${des}` : opt.description;
          if (!!opt.name) {
            text = `${opt.name} ${opt.description}`;
          }

          switch (e.name) {
            case MethodField.ManualAccountDeletionMN0TransactionId:
              text = `${opt.name}, ${opt.description}`;
              break;
            case MethodField.ManualAccountDeletionTransaction:
              text = opt.description;
              break;
          }

          return {
            code: opt.value || '',
            text: text,
            name: opt.name,
            description: opt.description,
            selected: opt.selected,
            type: opt.type,
            workFlow: opt.workFlow
          };
        });
        return {
          name: e.name,
          options
        } as {
          name: string;
          options: any[];
        };
      });
    const manualAccountDeletionExternalStatusCode =
      elements.find(
        findByName(MethodField.ManualAccountDeletionExternalStatusCode)
      )?.options || [];

    const manualAccountDeletionPropagate =
      elements.find(findByName(MethodField.ManualAccountDeletionPropagate))
        ?.options || [];

    const manualAccountDeletionTransactionId =
      elements
        .find(findByName(MethodField.ManualAccountDeletionTransactionId))
        ?.options?.filter((f: any) => f.workFlow === 'mad' || !f.workFlow) ||
      [];

    const manualAccountDeletionMN0TransactionId =
      elements.find(
        findByName(MethodField.ManualAccountDeletionMN0TransactionId)
      )?.options || [];

    const manualAccountDeletionTransaction =
      elements
        .find(findByName(MethodField.ManualAccountDeletionTransaction))
        ?.options?.filter((f: any) => f.workFlow === 'mad' || !f.workFlow) ||
      [];

    return {
      manualAccountDeletionExternalStatusCode,
      manualAccountDeletionPropagate,
      manualAccountDeletionTransaction,
      manualAccountDeletionTransactionId,
      manualAccountDeletionMN0TransactionId
    };
  }
);

export const useSelectElementMetadataForManualAccountDeletion = () => {
  return useSelector<
    RootState,
    {
      manualAccountDeletionExternalStatusCode: RefData[];
      manualAccountDeletionPropagate: RefData[];
      manualAccountDeletionTransaction: RefData[];
      manualAccountDeletionMN0TransactionId: RefData[];
      manualAccountDeletionTransactionId: (RefData & {
        description?: string;
      })[];
    }
  >(elementMetadataForManualAccountDeletionSelector);
};
