import { createSelector } from '@reduxjs/toolkit';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import { matchSearchValue } from 'app/helpers';
import orderBy, { default as _orderBy } from 'lodash.orderby';
import { LIST_COMBOBOX_FORM } from 'pages/WorkflowManageStatementProductionSettings/ManageStatementProductionSettingsStep/helper';
import { useSelector } from 'react-redux';
const workflowMethodsMSPSSelector = createSelector(
  (state: RootState) => state.workflowSetup!.workflowMethods,
  data => {
    const {
      methods: rawData,
      dataFilter = {},
      loading = false,
      error = false
    } = data;
    const { orderBy, sortBy, page, pageSize, searchValue = '' } = dataFilter;

    const _searchValue = searchValue.toLowerCase() || '';
    const _method = _orderBy(rawData, sortBy, orderBy).filter(w => {
      return matchSearchValue([w.name], _searchValue);
    });
    const total = _method.length;
    const totalMethods = _orderBy(rawData, sortBy, orderBy);
    const methods = _method.slice((page! - 1) * pageSize!, page! * pageSize!);
    return { totalMethods, methods, total, loading, error };
  }
);

export const useSelectWorkflowMethodsMSPSSelector = () => {
  return useSelector<
    RootState,
    {
      methods: IMethodModel[];
      loading: boolean;
      error: boolean;
      total: number;
      totalMethods: IMethodModel[];
    }
  >(workflowMethodsMSPSSelector);
};

const KeepTextElementsMSPS = [
  MethodFieldParameterEnum.ManageStatementProductionSettingsMinimumAmountForInactiveDeletedAccounts.toString(),
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPICBP.toString(),
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPICID.toString(),
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPICII.toString(),
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIM.toString(),
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIP.toString(),
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIR.toString(),
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPICMF.toString(),
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPICVI.toString(),
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOAC.toString(),
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOCI.toString(),
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOMC.toString(),
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOMI.toString(),
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFLC.toString(),
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFOC.toString(),
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFRC.toString(),
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPPOMP.toString(),
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPICME.toString()
];

const elementMetadataForMSPSSelector = createSelector(
  (state: RootState) => state.workflowSetup!.elementMetadata.elements,
  data => {
    const elements = data.map(e => {
      return {
        name: e.name,
        options: KeepTextElementsMSPS.includes(e.name)
          ? e.options?.map(opt => ({
              code: opt.value,
              text: `${opt.value}`,
              selected: opt.selected
            }))
          : LIST_COMBOBOX_FORM.includes(e.name)
          ? orderBy(
              e.options?.map(opt => ({
                code: opt.value,
                text:
                  opt.value && opt.value !== opt.description
                    ? `${opt.value}${
                        opt.description ? ` - ${opt.description}` : ''
                      }`
                    : `${opt.description}`,
                selected: opt.selected
              })),
              ['code'],
              ['asc']
            )
          : e.options?.map(opt => ({
              code: opt.value,
              text:
                opt.value && opt.value !== opt.description
                  ? `${opt.value}${
                      opt.description ? ` - ${opt.description}` : ''
                    }`
                  : `${opt.description}`,
              selected: opt.selected
            }))
      } as {
        name: string;
        options: RefData[];
      };
    });

    const creditBalanceHoldOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCreditBalanceHoldOptions
      )?.options || [];
    const whereToSendRequestPayments =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsWhereToSendRequestPayments
      )?.options || [];
    const sameDayLostStatusOption =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsSameDayLostStatusOption
      )?.options || [];
    const useFixedMinimumPaymentDueDate =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsUseFixedMinimumPaymentDueDate
      )?.options || [];
    const useFixedMinimumPaymentDueDateTableId =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsUseFixedMinimumPaymentDueDateTableID
      )?.options || [];
    const annualInterestDisplay =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsAnnualInterestDisplay
      )?.options || [];
    const includeLateChargesInAnnualInterestDisplay =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeLateChargesInAnnualInterestDisplay
      )?.options || [];
    const includeCashItemInAnnualInterestDisplay =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeCashItemInAnnualInterestDisplay
      )?.options || [];
    const includeMerchandiseItemInAnnualInterestDisplay =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeMerchandiseItemInAnnualInterestDisplay
      )?.options || [];
    const includeOverLimitFeesInAnnualInterestDisplay =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeOverLimitFeesInAnnualInterestDisplay
      )?.options || [];
    const includesStatementProductionFeesInAnnualInterestDisplay =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeStatementProductionFeesInAnnualInterestDisplay
      )?.options || [];
    const produceStatementsForInactiveDeletedAccounts =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsProduceStatementsForInactiveDeletedAccounts
      )?.options || [];
    const usInactiveAccountExternalStatusControl =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsUSInactiveAccountExternalStatusControl
      )?.options || [];
    const nonUsInactiveAccountExternalStatusControl =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsNonUSInactiveAccountExternalStatusControl
      )?.options || [];
    const pricingStrategyChangeStatementProduction =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsPricingStrategyChangeStatementProduction
      )?.options || [];
    const statusAAuthorizationProhibited =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsStatusAAuthorizationProhibited
      )?.options || [];
    const statusBBankrupt =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsStatusBBankrupt
      )?.options || [];
    const statusCClosed =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsStatusCClosed
      )?.options || [];

    const statusERevoked =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsStatusERevoked
      )?.options || [];

    const statusFFrozen =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsStatusFFrozen
      )?.options || [];

    const statusIInterestProhibited =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsStatusIInterestProhibited
      )?.options || [];

    const statusLLost =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsStatusLLost
      )?.options || [];

    const statusUStolen =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsStatusUStolen
      )?.options || [];

    const statusBlank =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsStatusBlank
      )?.options || [];

    const creditLifeInsurancePlan1 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance1
      )?.options || [];

    const creditLifeInsurancePlan2 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance2
      )?.options || [];

    const creditLifeInsurancePlan3 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance3
      )?.options || [];

    const creditLifeInsurancePlan4 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance4
      )?.options || [];

    const creditLifeInsurancePlan5 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance5
      )?.options || [];
    const creditLifeInsurancePlan6 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance6
      )?.options || [];
    const creditLifeInsurancePlan7 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance7
      )?.options || [];
    const creditLifeInsurancePlan8 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance8
      )?.options || [];
    const creditLifeInsurancePlan9 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance9
      )?.options || [];

    const cashAdvanceFinanceCharges =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCashAdvanceFinanceCharges
      )?.options || [];
    const cashItemCharges =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCashItemCharges
      )?.options || [];
    const merchandiseFinanceCharges =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsMerchandiseFinanceCharges
      )?.options || [];
    const merchandiseItemCharges =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsMerchandiseItemCharges
      )?.options || [];
    const lateCharges =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsLateCharge
      )?.options || [];
    const overlimitCharge =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsOverlimitCharge
      )?.options || [];
    const creditLifeInsuranceRefunds =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsuranceRefunds
      )?.options || [];
    const eBillLanguageOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsEbillLanguageOptions
      )?.options || [];

    const financeChargeStatementDisplayOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsFinanceChargeStatementDisplayOptions
      )?.options || [];

    const displayAirlineTransaction =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsDisplayAirlineTransactionDetailsOnStatement
      )?.options || [];

    const cardHolderNamesOnStatement =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCardholderNamesOnStatement
      )?.options || [];

    const reversalDisplayOption =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsReversalDisplayOption
      )?.options || [];

    const lateChargeDateDisplay =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsLateChargeDateDisplay
      )?.options || [];

    const overlimitChargeDateDisplay =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsOverlimitChargeDateDisplay
      )?.options || [];

    const foreignCurrencyDisplayOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsForeignCurrencyDisplayOptions
      )?.options || [];

    const feeRecordIndicator =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsFeeRecordIndicatorOnCISScreens
      )?.options || [];

    const optionalIssuerFeeStatementDisplayCode =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsOptionalIssuerFeeStatementDisplayCode
      )?.options || [];

    const optionalIssuerFeeMessageText =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsOptionalIssuerFeeMessageText
      )?.options || [];

    const saleAmountAdjText =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsSaleAmountAdjustmentText
      )?.options || [];

    const cashAdvanceAmountAdjText =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCashAdvanceAmountAdjustmentText
      )?.options || [];

    const returnAmountAdjText =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsReturnAmountAdjustmentText
      )?.options || [];

    const paymentAmountAdjText =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsPaymentAmountAdjustmentText
      )?.options || [];

    const chargeOffPrincipalAdjText =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffPrincipalAdjustmentTextLine1
      )?.options || [];

    const chargeOffFinanceChargesAdjText =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffFinanceChargesAdjustmentTextLine2
      )?.options || [];

    const miscSpecificCreditAdjText =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsMiscellaneousSpecificCreditAdjustmentText
      )?.options || [];

    const chargeOffSmallBalanceAdjText =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffSmallBalanceAdjustmentText
      )?.options || [];

    const chargeOffCreditLifeInsuranceAdjText =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffCreditLifeInsuranceAdjustmentText
      )?.options || [];

    const chargeOffCashInterestRefundText =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffCashInterestRefundText
      )?.options || [];

    const chargeOffLateChargeRefundText =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffLateChargeRefundText
      )?.options || [];

    const chargeOffMerchandiseInterestRefundText =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffMerchandiseInterestRefundText
      )?.options || [];

    const chargeOffCashItemRefundText =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffCashItemRefundText
      )?.options || [];

    const chargeOffMerchandiseItemRefundText =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffMerchandiseItemRefundText
      )?.options || [];

    const chargeOffOverlimitFeeRefundText =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffOverlimitFeeRefundText
      )?.options || [];

    const saleReversalText =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsSaleReversalText
      )?.options || [];

    const cashAdvanceReversalText =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCashAdvanceReversalText
      )?.options || [];

    const returnReversalText =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsReturnReversalText
      )?.options || [];

    const paymentReversalText =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsPaymentReversalText
      )?.options || [];

    const cpIcBp =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCPICBP
      )?.options || [];
    const cpIcId =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCPICID
      )?.options || [];

    const cpIcIi =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCPICII
      )?.options || [];

    const cpIcIm =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIM
      )?.options || [];

    const cpIcIp =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIP
      )?.options || [];

    const cpIcIr =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIR
      )?.options || [];

    const cpIcMf =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCPICMF
      )?.options || [];

    const cpIcVi =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCPICVI
      )?.options || [];

    const cpIoAc =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOAC
      )?.options || [];

    const cpAoCi =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOCI
      )?.options || [];

    const cpIoMc =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOMC
      )?.options || [];

    const cpIoMi =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOMI
      )?.options || [];

    const cpPfLc =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFLC
      )?.options || [];

    const cpPfOc =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFOC
      )?.options || [];

    const cpPfRc =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFRC
      )?.options || [];

    const cpPoMp =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCPPOMP
      )?.options || [];

    const cpIcMe =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCPICME
      )?.options || [];

    const creditBalanceHoldCode =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCreditBalanceHoldCode
      )?.options || [];

    const printHardCopySec =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsPrintHardCopySecurityStatements
      )?.options || [];

    const cssStatementOverrideTableId =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsSCSStatementOverrideTableID
      )?.options || [];

    const lateFeeWaiverText =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsLateFeeWaiverText
      )?.options || [];

    const genericMessageAbove =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsGenericMessageAboveRevolvingBalance
      )?.options || [];

    const genericMessageBelow =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsGenericMessageBelowRevolvingBalance
      )?.options || [];

    const normalTermsDateId =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsNormalTermsDateID
      )?.options || [];

    const crossCycleFeeDisplayOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCrossCycleFeeDisplayOptions
      )?.options || [];

    const crossCycleInterestDisplayOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCrossCycleInterestDisplayOptions
      )?.options || [];

    const transactionSeriesDisplayOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettings280TransactionsSeriesDisplayOptions
      )?.options || [];

    const prevCycCashAdv =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycCashAdvance
      )?.options || [];

    const prevCycMerchandise =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycMerchandise
      )?.options || [];

    const prevCycCashItemCharge =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycCashItemCharge
      )?.options || [];

    const prevCycMdsItemCharge =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycMdseItemCharge
      )?.options || [];

    const minimumFinanceCharges =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsMinimumFinanceCharges
      )?.options || [];

    const activityFee =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsActivityFee
      )?.options || [];

    const prevCycLateCharge =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycLateCharge
      )?.options || [];

    const prevCycOverLimitCharge =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycOverlimitCharge
      )?.options || [];

    const creditLifeInsuranceCharge =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsuranceCharge
      )?.options || [];

    const otherCreditAdjText =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsOtherCreditAdjustmentText
      )?.options || [];

    const otherDebitAdjText =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsOtherDebitAdjustmentText
      )?.options || [];

    const cashAdvFinanceChargesText =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCashAdvanceFinanceChargesText
      )?.options || [];

    const merchandiseItemChargeText =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsMerchandiseItemChargeText
      )?.options || [];

    const cashItemChargesText =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCashItemChargesText
      )?.options || [];

    const merchandiseFinanceChargesText =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsMerchandiseFinanceChargesText
      )?.options || [];

    const lateChargesText =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsLateChargesText
      )?.options || [];

    const overlimitChargesText =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsOverlimitChargesText
      )?.options || [];

    const minimumFinanceChargesText =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsMinimumFinanceChargesText
      )?.options || [];

    const activityFeesText =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsActivityFeesText
      )?.options || [];

    const creditLifeInsuranceText =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsuranceText
      )?.options || [];

    return {
      creditBalanceHoldOptions,
      whereToSendRequestPayments,
      sameDayLostStatusOption,
      useFixedMinimumPaymentDueDate,
      useFixedMinimumPaymentDueDateTableId,
      annualInterestDisplay,
      includeLateChargesInAnnualInterestDisplay,
      includeCashItemInAnnualInterestDisplay,
      includeMerchandiseItemInAnnualInterestDisplay,
      includeOverLimitFeesInAnnualInterestDisplay,
      includesStatementProductionFeesInAnnualInterestDisplay,
      produceStatementsForInactiveDeletedAccounts,
      usInactiveAccountExternalStatusControl,
      nonUsInactiveAccountExternalStatusControl,
      pricingStrategyChangeStatementProduction,
      statusAAuthorizationProhibited,
      statusBBankrupt,
      statusCClosed,
      statusERevoked,
      statusFFrozen,
      statusIInterestProhibited,
      statusLLost,
      statusUStolen,
      statusBlank,
      creditLifeInsurancePlan1,
      creditLifeInsurancePlan2,
      creditLifeInsurancePlan3,
      creditLifeInsurancePlan4,
      creditLifeInsurancePlan5,
      creditLifeInsurancePlan6,
      creditLifeInsurancePlan7,
      creditLifeInsurancePlan8,
      creditLifeInsurancePlan9,
      cashAdvanceFinanceCharges,
      cashItemCharges,
      merchandiseFinanceCharges,
      merchandiseItemCharges,
      lateCharges,
      overlimitCharge,
      creditLifeInsuranceRefunds,
      eBillLanguageOptions,
      financeChargeStatementDisplayOptions,
      displayAirlineTransaction,
      cardHolderNamesOnStatement,
      reversalDisplayOption,
      lateChargeDateDisplay,
      overlimitChargeDateDisplay,
      foreignCurrencyDisplayOptions,
      feeRecordIndicator,
      optionalIssuerFeeStatementDisplayCode,
      optionalIssuerFeeMessageText,
      saleAmountAdjText,
      cashAdvanceAmountAdjText,
      returnAmountAdjText,
      paymentAmountAdjText,
      chargeOffPrincipalAdjText,
      chargeOffFinanceChargesAdjText,
      miscSpecificCreditAdjText,
      chargeOffSmallBalanceAdjText,
      chargeOffCreditLifeInsuranceAdjText,
      chargeOffCashInterestRefundText,
      chargeOffLateChargeRefundText,
      chargeOffMerchandiseInterestRefundText,
      chargeOffCashItemRefundText,
      chargeOffMerchandiseItemRefundText,
      chargeOffOverlimitFeeRefundText,
      saleReversalText,
      cashAdvanceReversalText,
      returnReversalText,
      paymentReversalText,
      cpIcBp,
      cpIcId,
      cpIcIi,
      cpIcIm,
      cpIcIp,
      cpIcIr,
      cpIcMf,
      cpIcVi,
      cpIoAc,
      cpAoCi,
      cpIoMc,
      cpIoMi,
      cpPfLc,
      cpPfOc,
      cpPfRc,
      cpPoMp,
      cpIcMe,
      creditBalanceHoldCode,
      printHardCopySec,
      cssStatementOverrideTableId,
      lateFeeWaiverText,
      genericMessageAbove,
      genericMessageBelow,
      normalTermsDateId,
      crossCycleFeeDisplayOptions,
      crossCycleInterestDisplayOptions,
      transactionSeriesDisplayOptions,
      prevCycCashAdv,
      prevCycMerchandise,
      prevCycCashItemCharge,
      prevCycMdsItemCharge,
      minimumFinanceCharges,
      activityFee,
      prevCycLateCharge,
      prevCycOverLimitCharge,
      creditLifeInsuranceCharge,
      otherCreditAdjText,
      otherDebitAdjText,
      cashAdvFinanceChargesText,
      merchandiseItemChargeText,
      cashItemChargesText,
      merchandiseFinanceChargesText,
      lateChargesText,
      overlimitChargesText,
      minimumFinanceChargesText,
      activityFeesText,
      creditLifeInsuranceText
    };
  }
);

export const useSelectElementMetadataForMSPS = () => {
  return useSelector<
    RootState,
    {
      creditBalanceHoldOptions: RefData[];
      whereToSendRequestPayments: RefData[];
      sameDayLostStatusOption: RefData[];
      useFixedMinimumPaymentDueDate: RefData[];
      useFixedMinimumPaymentDueDateTableId: RefData[];
      annualInterestDisplay: RefData[];
      includeLateChargesInAnnualInterestDisplay: RefData[];
      includeCashItemInAnnualInterestDisplay: RefData[];
      includeMerchandiseItemInAnnualInterestDisplay: RefData[];
      includeOverLimitFeesInAnnualInterestDisplay: RefData[];
      includesStatementProductionFeesInAnnualInterestDisplay: RefData[];
      produceStatementsForInactiveDeletedAccounts: RefData[];
      usInactiveAccountExternalStatusControl: RefData[];
      nonUsInactiveAccountExternalStatusControl: RefData[];
      pricingStrategyChangeStatementProduction: RefData[];
      statusAAuthorizationProhibited: RefData[];
      statusBBankrupt: RefData[];
      statusCClosed: RefData[];
      statusERevoked: RefData[];
      statusFFrozen: RefData[];
      statusIInterestProhibited: RefData[];
      statusLLost: RefData[];
      statusUStolen: RefData[];
      statusBlank: RefData[];
      creditLifeInsurancePlan1: RefData[];
      creditLifeInsurancePlan2: RefData[];
      creditLifeInsurancePlan3: RefData[];
      creditLifeInsurancePlan4: RefData[];
      creditLifeInsurancePlan5: RefData[];
      creditLifeInsurancePlan6: RefData[];
      creditLifeInsurancePlan7: RefData[];
      creditLifeInsurancePlan8: RefData[];
      creditLifeInsurancePlan9: RefData[];
      cashAdvanceFinanceCharges: RefData[];
      cashItemCharges: RefData[];
      merchandiseFinanceCharges: RefData[];
      merchandiseItemCharges: RefData[];
      lateCharges: RefData[];
      overlimitCharge: RefData[];
      creditLifeInsuranceRefunds: RefData[];
      eBillLanguageOptions: RefData[];
      financeChargeStatementDisplayOptions: RefData[];
      displayAirlineTransaction: RefData[];
      cardHolderNamesOnStatement: RefData[];
      reversalDisplayOption: RefData[];
      lateChargeDateDisplay: RefData[];
      overlimitChargeDateDisplay: RefData[];
      foreignCurrencyDisplayOptions: RefData[];
      feeRecordIndicator: RefData[];
      optionalIssuerFeeStatementDisplayCode: RefData[];
      optionalIssuerFeeMessageText: RefData[];
      saleAmountAdjText: RefData[];
      cashAdvanceAmountAdjText: RefData[];
      returnAmountAdjText: RefData[];
      paymentAmountAdjText: RefData[];
      chargeOffPrincipalAdjText: RefData[];
      chargeOffFinanceChargesAdjText: RefData[];
      miscSpecificCreditAdjText: RefData[];
      chargeOffSmallBalanceAdjText: RefData[];
      chargeOffCreditLifeInsuranceAdjText: RefData[];
      chargeOffCashInterestRefundText: RefData[];
      chargeOffLateChargeRefundText: RefData[];
      chargeOffMerchandiseInterestRefundText: RefData[];
      chargeOffCashItemRefundText: RefData[];
      chargeOffMerchandiseItemRefundText: RefData[];
      chargeOffOverlimitFeeRefundText: RefData[];
      saleReversalText: RefData[];
      cashAdvanceReversalText: RefData[];
      returnReversalText: RefData[];
      paymentReversalText: RefData[];
      cpIcBp: RefData[];
      cpIcId: RefData[];
      cpIcIi: RefData[];
      cpIcIm: RefData[];
      cpIcIp: RefData[];
      cpIcIr: RefData[];
      cpIcMf: RefData[];
      cpIcVi: RefData[];
      cpIoAc: RefData[];
      cpAoCi: RefData[];
      cpIoMc: RefData[];
      cpIoMi: RefData[];
      cpPfLc: RefData[];
      cpPfOc: RefData[];
      cpPfRc: RefData[];
      cpPoMp: RefData[];
      cpIcMe: RefData[];
      creditBalanceHoldCode: RefData[];
      printHardCopySec: RefData[];
      cssStatementOverrideTableId: RefData[];
      lateFeeWaiverText: RefData[];
      genericMessageAbove: RefData[];
      genericMessageBelow: RefData[];
      normalTermsDateId: RefData[];
      crossCycleFeeDisplayOptions: RefData[];
      crossCycleInterestDisplayOptions: RefData[];
      transactionSeriesDisplayOptions: RefData[];
      prevCycCashAdv: RefData[];
      prevCycMerchandise: RefData[];
      prevCycCashItemCharge: RefData[];
      prevCycMdsItemCharge: RefData[];
      minimumFinanceCharges: RefData[];
      activityFee: RefData[];
      prevCycLateCharge: RefData[];
      prevCycOverLimitCharge: RefData[];
      creditLifeInsuranceCharge: RefData[];
      otherCreditAdjText: RefData[];
      otherDebitAdjText: RefData[];
      cashAdvFinanceChargesText: RefData[];
      merchandiseItemChargeText: RefData[];
      cashItemChargesText: RefData[];
      merchandiseFinanceChargesText: RefData[];
      lateChargesText: RefData[];
      overlimitChargesText: RefData[];
      minimumFinanceChargesText: RefData[];
      activityFeesText: RefData[];
      creditLifeInsuranceText: RefData[];
    }
  >(elementMetadataForMSPSSelector);
};
