import { createSelector } from '@reduxjs/toolkit';
import { WorkflowStrategyFlyoutProps } from 'app/components/WorkflowStrategyFlyout';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import { matchSearchValue } from 'app/helpers';
import _orderBy from 'lodash.orderby';
import { useSelector } from 'react-redux';
import { KeepTextDelayDesignPromotion } from './_common';

const workflowMethodsSelector = createSelector(
  (state: RootState) => state.workflowSetup!.workflowMethods,
  data => {
    const {
      methods: rawData,
      dataFilter = {},
      loading = false,
      error = false
    } = data;
    const { sortBy, orderBy, page, pageSize, searchValue = '' } = dataFilter;
    const _searchValue = searchValue.toLowerCase() || '';

    const _method = _orderBy(rawData, sortBy, orderBy).filter(w => {
      const pricingStrategyNames = (w.pricingStrategies || []).map(
        (r: any) => r.name
      );
      return matchSearchValue([w.name, ...pricingStrategyNames], _searchValue);
    });
    const total = _method.length;
    const methods = _method.slice((page! - 1) * pageSize!, page! * pageSize!);
    return { methods, total, loading, error };
  }
);
export const useSelectWorkflowMethodsSelector = () => {
  return useSelector<
    RootState,
    {
      methods: IMethodModel[];
      loading: boolean;
      error: boolean;
      total: number;
    }
  >(workflowMethodsSelector);
};

const workflowMethodsRawData = createSelector(
  (state: RootState) => state.workflowSetup!.workflowMethods.methods,
  data => data || {}
);
export const useSelectWorkflowMethodsRawData = () => {
  return useSelector<RootState, IMethodModel[]>(workflowMethodsRawData);
};

const strategyFlyoutSelector = createSelector(
  (state: RootState) => state.workflowSetup!.workflowMethods.strategyFlyoutData,
  data => data || null
);
export const useSelectStrategyFlyoutSelector = () => {
  return useSelector<
    RootState,
    Omit<WorkflowStrategyFlyoutProps, 'onClose'> | null
  >(strategyFlyoutSelector);
};

const workflowMethodsFilterSelector = createSelector(
  (state: RootState) => state.workflowSetup!.workflowMethods.dataFilter,
  data => data || {}
);
export const useSelectWorkflowMethodsFilterSelector = () => {
  return useSelector<RootState, IDataFilter>(workflowMethodsFilterSelector);
};

const expandingMethodSelector = createSelector(
  (state: RootState) => state.workflowSetup?.workflowMethods.expandingMethod,
  data => data!
);

export const useSelectExpandingMethod = () => {
  return useSelector<RootState, string>(expandingMethodSelector);
};

const KeepTextElements = [
  MethodFieldParameterEnum.PostingTextID.toString(),
  MethodFieldParameterEnum.ReversalTextID.toString(),
  MethodFieldParameterEnum.ReversalDetailsTextID.toString(),
  MethodFieldParameterEnum.WaivedMessageTextID.toString(),
  MethodFieldParameterEnum.LatePaymentWarningMessage.toString()
];

const elementMetadataForRCSelector = createSelector(
  (state: RootState) => state.workflowSetup!.elementMetadata.elements,
  data => {
    const elements = data.map(e => {
      return {
        name: e.name,
        options: KeepTextElements.includes(e.name)
          ? e.options?.map(opt => ({
              code: opt.value,
              text: opt.description
            }))
          : e.options?.map(opt => ({
              code: opt.value,
              text: `${opt.value ? opt.value + ' - ' : ''}${opt.description}`
            }))
      } as {
        name: string;
        options: RefData[];
      };
    });

    const rccOptions =
      elements.find(
        e => e.name === MethodFieldParameterEnum.ReturnedCheckChargesOption
      )?.options || [];
    const minimumPayDueOptions =
      elements.find(
        e => e.name === MethodFieldParameterEnum.MinimumPayDueOptions
      )?.options || [];
    const oneFeeParticipationCodeOptions =
      elements.find(
        e => e.name === MethodFieldParameterEnum.OneFeeParticipationCode
      )?.options || [];
    const postingTextIDOptions =
      elements.find(e => e.name === MethodFieldParameterEnum.PostingTextID)
        ?.options || [];
    const reversalTextIDOptions =
      elements.find(e => e.name === MethodFieldParameterEnum.ReversalTextID)
        ?.options || [];
    const firstYearMaxManagementOptions =
      elements.find(
        e => e.name === MethodFieldParameterEnum.FirstYrMaxManagement
      )?.options || [];
    const oneFeePriorityCodeOptions =
      elements.find(e => e.name === MethodFieldParameterEnum.OneFeePriorityCode)
        ?.options || [];
    return {
      rccOptions,
      minimumPayDueOptions,
      oneFeeParticipationCodeOptions,
      postingTextIDOptions,
      reversalTextIDOptions,
      firstYearMaxManagementOptions,
      oneFeePriorityCodeOptions
    };
  }
);

export const useSelectElementMetadataForRC = () => {
  return useSelector<
    RootState,
    {
      rccOptions: RefData[];
      minimumPayDueOptions: RefData[];
      oneFeeParticipationCodeOptions: RefData[];
      postingTextIDOptions: RefData[];
      reversalTextIDOptions: RefData[];
      firstYearMaxManagementOptions: RefData[];
      oneFeePriorityCodeOptions: RefData[];
    }
  >(elementMetadataForRCSelector);
};

const elementMetadataForLCSelector = createSelector(
  (state: RootState) => state.workflowSetup!.elementMetadata.elements,
  data => {
    const elements = data.map(e => {
      return {
        name: e.name,
        options: KeepTextDelayDesignPromotion.includes(e.name)
          ? e.options?.map(opt => ({
              code: opt.value,
              text: `${opt.value}`
            }))
          : e.options?.map(opt => ({
              code: opt.value,
              text: opt.value
                ? `${opt.value}${
                    opt.description ? ` - ${opt.description}` : ''
                  }`
                : `${opt.description}`
            }))
      } as {
        name: string;
        options: RefData[];
      };
    });

    const lateChargesOption =
      elements.find(e => e.name === MethodFieldParameterEnum.LateChargesOption)
        ?.options || [];
    const calculationBase =
      elements.find(e => e.name === MethodFieldParameterEnum.CalculationBase)
        ?.options || [];
    const assessmentControl =
      elements.find(e => e.name === MethodFieldParameterEnum.AssessmentControl)
        ?.options || [];
    const balanceIndicator =
      elements.find(e => e.name === MethodFieldParameterEnum.BalanceIndicator)
        ?.options || [];
    const assessedAccounts =
      elements.find(e => e.name === MethodFieldParameterEnum.AssessedAccounts)
        ?.options || [];
    const currentBalanceAssessment =
      elements.find(
        e => e.name === MethodFieldParameterEnum.CurrentBalanceAssessment
      )?.options || [];
    const calculationDayControl =
      elements.find(
        e => e.name === MethodFieldParameterEnum.CalculationDayControl
      )?.options || [];
    const nonProcessingLateCharge =
      elements.find(
        e => e.name === MethodFieldParameterEnum.NonProcessingLateCharge
      )?.options || [];
    const includeExcludeControl =
      elements.find(
        e => e.name === MethodFieldParameterEnum.IncludeExcludeControl
      )?.options || [];
    const status1 =
      elements.find(e => e.name === MethodFieldParameterEnum.Status1)
        ?.options || [];
    const status2 =
      elements.find(e => e.name === MethodFieldParameterEnum.Status1)
        ?.options || [];
    const status3 =
      elements.find(e => e.name === MethodFieldParameterEnum.Status1)
        ?.options || [];
    const status4 =
      elements.find(e => e.name === MethodFieldParameterEnum.Status1)
        ?.options || [];
    const status5 =
      elements.find(e => e.name === MethodFieldParameterEnum.Status1)
        ?.options || [];
    const lateChargeResetCounter =
      elements.find(
        e => e.name === MethodFieldParameterEnum.LateChargeResetCounter
      )?.options || [];
    const minimumPayDueOptions =
      elements.find(
        e => e.name === MethodFieldParameterEnum.MinimumPayDueOptions
      )?.options || [];

    const reversalDetailsTextID =
      elements.find(
        e => e.name === MethodFieldParameterEnum.ReversalDetailsTextID
      )?.options || [];

    const waivedMessageTextID =
      elements.find(
        e => e.name === MethodFieldParameterEnum.WaivedMessageTextID
      )?.options || [];

    const latePaymentWarningMessage =
      elements.find(
        e => e.name === MethodFieldParameterEnum.LatePaymentWarningMessage
      )?.options || [];

    // design promotions
    const designPromotionBasicInformation3 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionBasicInformation3
      )?.options || [];

    const designPromotionBasicInformation4 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionBasicInformation4
      )?.options || [];

    const designPromotionBasicInformation5 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionBasicInformation5
      )?.options || [];

    const designPromotionBasicInformation6 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionBasicInformation6
      )?.options || [];

    const designPromotionBasicInformation7 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionBasicInformation7
      )?.options || [];

    const designPromotionBasicInformation8 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionBasicInformation8
      )?.options || [];

    const designPromotionBasicInformation9 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionBasicInformation9
      )?.options || [];

    const designPromotionBasicInformation10 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionBasicInformation10
      )?.options || [];

    const designPromotionBasicInformation11 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionBasicInformation11
      )?.options || [];

    const designPromotionBasicInformation12 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionBasicInformation12
      )?.options || [];

    const designPromotionInterestAssessment9 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionInterestAssessment9
      )?.options || [];

    const designPromotionInterestAssessment10 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionInterestAssessment10
      )?.options || [];

    const designPromotionInterestAssessment11 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionInterestAssessment11
      )?.options || [];

    const designPromotionInterestAssessment12 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionInterestAssessment12
      )?.options || [];

    const designPromotionInterestAssessment13 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionInterestAssessment13
      )?.options || [];

    const designPromotionInterestAssessment14 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionInterestAssessment14
      )?.options || [];

    const designPromotionInterestAssessment15 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionInterestAssessment15
      )?.options || [];

    const designPromotionInterestAssessment16 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionInterestAssessment16
      )?.options || [];

    const designPromotionInterestAssessment17 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionInterestAssessment17
      )?.options || [];

    const designPromotionInterestOverrides1 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionInterestOverrides1
      )?.options || [];
    const designPromotionInterestOverrides2 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionInterestOverrides2
      )?.options || [];

    const designPromotionInterestOverrides3 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionInterestOverrides3
      )?.options || [];

    const designPromotionInterestOverrides4 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionInterestOverrides4
      )?.options || [];

    const designPromotionInterestOverrides5 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionInterestOverrides5
      )?.options || [];

    const designPromotionInterestOverrides6 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionInterestOverrides6
      )?.options || [];

    const designPromotionInterestOverrides7 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionInterestOverrides7
      )?.options || [];

    const designPromotionInterestOverrides8 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionInterestOverrides8
      )?.options || [];

    const designPromotionRetailPath1 =
      elements.find(
        e => e.name === MethodFieldParameterEnum.DesignPromotionRetailPath1
      )?.options || [];

    const designPromotionRetailPath4 =
      elements.find(
        e => e.name === MethodFieldParameterEnum.DesignPromotionRetailPath4
      )?.options || [];

    const designPromotionReturnRevolve2 =
      elements.find(
        e => e.name === MethodFieldParameterEnum.DesignPromotionReturnRevolve2
      )?.options || [];

    const designPromotionReturnRevolve3 =
      elements.find(
        e => e.name === MethodFieldParameterEnum.DesignPromotionReturnRevolve3
      )?.options || [];

    const designPromotionReturnRevolve4 =
      elements.find(
        e => e.name === MethodFieldParameterEnum.DesignPromotionReturnRevolve4
      )?.options || [];

    const designPromotionReturnRevolve6 =
      elements.find(
        e => e.name === MethodFieldParameterEnum.DesignPromotionReturnRevolve6
      )?.options || [];

    const designPromotionStatementMessaging1 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionStatementMessaging1
      )?.options || [];

    const designPromotionStatementMessaging2 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionStatementMessaging2
      )?.options || [];

    const designPromotionStatementMessaging3 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionStatementMessaging3
      )?.options || [];

    const designPromotionStatementMessaging4 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionStatementMessaging4
      )?.options || [];

    const designPromotionStatementMessaging5 =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DesignPromotionStatementMessaging5
      )?.options || [];
    const designPromotionStatementAdvancedParameter1 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter1
      )?.options || [];
    const designPromotionStatementAdvancedParameter3 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter3
      )?.options || [];
    const designPromotionStatementAdvancedParameter4 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter4
      )?.options || [];
    const designPromotionStatementAdvancedParameter5 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter5
      )?.options || [];
    const designPromotionStatementAdvancedParameter7 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter7
      )?.options || [];
    const designPromotionStatementAdvancedParameter8 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter8
      )?.options || [];
    const designPromotionStatementAdvancedParameter9 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter9
      )?.options || [];
    const designPromotionStatementAdvancedParameter10 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter10
      )?.options || [];
    const designPromotionStatementAdvancedParameter11 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter11
      )?.options || [];
    const designPromotionStatementAdvancedParameter12 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter12
      )?.options || [];
    const designPromotionStatementAdvancedParameter13 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter13
      )?.options || [];
    const designPromotionStatementAdvancedParameter14 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter14
      )?.options || [];
    const designPromotionStatementAdvancedParameter15 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter15
      )?.options || [];
    const designPromotionStatementAdvancedParameter16 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter16
      )?.options || [];
    const designPromotionStatementAdvancedParameter17 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter17
      )?.options || [];
    const designPromotionStatementAdvancedParameter18 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter18
      )?.options || [];
    const designPromotionStatementAdvancedParameter19 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter19
      )?.options || [];
    const designPromotionStatementAdvancedParameter20 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter20
      )?.options || [];
    const designPromotionStatementAdvancedParameter21 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter21
      )?.options || [];
    const designPromotionStatementAdvancedParameter22 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter22
      )?.options || [];
    const designPromotionStatementAdvancedParameter23 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter23
      )?.options || [];
    const designPromotionStatementAdvancedParameter24 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter24
      )?.options || [];
    const designPromotionStatementAdvancedParameter25 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter25
      )?.options || [];
    const designPromotionStatementAdvancedParameter26 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter26
      )?.options || [];
    const designPromotionStatementAdvancedParameter27 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter27
      )?.options || [];
    const designPromotionStatementAdvancedParameter28 =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DesignPromotionStatementAdvancedParameter28
      )?.options || [];
    return {
      lateChargesOption,
      calculationBase,
      assessmentControl,
      balanceIndicator,
      assessedAccounts,
      currentBalanceAssessment,
      calculationDayControl,
      nonProcessingLateCharge,
      includeExcludeControl,
      status1,
      status2,
      status3,
      status4,
      status5,
      lateChargeResetCounter,
      minimumPayDueOptions,
      reversalDetailsTextID,
      waivedMessageTextID,
      latePaymentWarningMessage,
      designPromotionBasicInformation3,
      designPromotionBasicInformation4,
      designPromotionBasicInformation5,
      designPromotionBasicInformation6,
      designPromotionBasicInformation7,
      designPromotionBasicInformation8,
      designPromotionBasicInformation9,
      designPromotionBasicInformation10,
      designPromotionBasicInformation11,
      designPromotionBasicInformation12,
      designPromotionInterestAssessment9,
      designPromotionInterestAssessment10,
      designPromotionInterestAssessment11,
      designPromotionInterestAssessment12,
      designPromotionInterestAssessment13,
      designPromotionInterestAssessment14,
      designPromotionInterestAssessment15,
      designPromotionInterestAssessment16,
      designPromotionInterestAssessment17,
      designPromotionInterestOverrides1,
      designPromotionInterestOverrides2,
      designPromotionInterestOverrides3,
      designPromotionInterestOverrides4,
      designPromotionInterestOverrides5,
      designPromotionInterestOverrides6,
      designPromotionInterestOverrides7,
      designPromotionInterestOverrides8,
      designPromotionRetailPath1,
      designPromotionRetailPath4,
      designPromotionReturnRevolve2,
      designPromotionReturnRevolve3,
      designPromotionReturnRevolve4,
      designPromotionReturnRevolve6,
      designPromotionStatementMessaging1,
      designPromotionStatementMessaging2,
      designPromotionStatementMessaging3,
      designPromotionStatementMessaging4,
      designPromotionStatementMessaging5,
      designPromotionStatementAdvancedParameter1,
      designPromotionStatementAdvancedParameter3,
      designPromotionStatementAdvancedParameter4,
      designPromotionStatementAdvancedParameter5,
      designPromotionStatementAdvancedParameter7,
      designPromotionStatementAdvancedParameter8,
      designPromotionStatementAdvancedParameter9,
      designPromotionStatementAdvancedParameter10,
      designPromotionStatementAdvancedParameter11,
      designPromotionStatementAdvancedParameter12,
      designPromotionStatementAdvancedParameter13,
      designPromotionStatementAdvancedParameter14,
      designPromotionStatementAdvancedParameter15,
      designPromotionStatementAdvancedParameter16,
      designPromotionStatementAdvancedParameter17,
      designPromotionStatementAdvancedParameter18,
      designPromotionStatementAdvancedParameter19,
      designPromotionStatementAdvancedParameter20,
      designPromotionStatementAdvancedParameter21,
      designPromotionStatementAdvancedParameter22,
      designPromotionStatementAdvancedParameter23,
      designPromotionStatementAdvancedParameter24,
      designPromotionStatementAdvancedParameter25,
      designPromotionStatementAdvancedParameter26,
      designPromotionStatementAdvancedParameter27,
      designPromotionStatementAdvancedParameter28
    };
  }
);

export const useSelectElementMetadataForLC = () => {
  return useSelector<
    RootState,
    {
      lateChargesOption: RefData[];
      calculationBase: RefData[];
      assessmentControl: RefData[];
      balanceIndicator: RefData[];
      assessedAccounts: RefData[];
      currentBalanceAssessment: RefData[];
      calculationDayControl: RefData[];
      nonProcessingLateCharge: RefData[];
      includeExcludeControl: RefData[];
      status1: RefData[];
      status2: RefData[];
      status3: RefData[];
      status4: RefData[];
      status5: RefData[];
      lateChargeResetCounter: RefData[];
      minimumPayDueOptions: RefData[];
      reversalDetailsTextID: RefData[];
      waivedMessageTextID: RefData[];
      latePaymentWarningMessage: RefData[];
      designPromotionBasicInformation3: RefData[];
      designPromotionBasicInformation4: RefData[];
      designPromotionBasicInformation5: RefData[];
      designPromotionBasicInformation6: RefData[];
      designPromotionBasicInformation7: RefData[];
      designPromotionBasicInformation8: RefData[];
      designPromotionBasicInformation9: RefData[];
      designPromotionBasicInformation10: RefData[];
      designPromotionBasicInformation11: RefData[];
      designPromotionBasicInformation12: RefData[];
      designPromotionInterestAssessment9: RefData[];
      designPromotionInterestAssessment10: RefData[];
      designPromotionInterestAssessment11: RefData[];
      designPromotionInterestAssessment12: RefData[];
      designPromotionInterestAssessment13: RefData[];
      designPromotionInterestAssessment14: RefData[];
      designPromotionInterestAssessment15: RefData[];
      designPromotionInterestAssessment16: RefData[];
      designPromotionInterestAssessment17: RefData[];
      designPromotionInterestOverrides1: RefData[];
      designPromotionInterestOverrides2: RefData[];
      designPromotionInterestOverrides3: RefData[];
      designPromotionInterestOverrides4: RefData[];
      designPromotionInterestOverrides5: RefData[];
      designPromotionInterestOverrides6: RefData[];
      designPromotionInterestOverrides7: RefData[];
      designPromotionInterestOverrides8: RefData[];
      designPromotionRetailPath1: RefData[];
      designPromotionRetailPath4: RefData[];
      designPromotionReturnRevolve2: RefData[];
      designPromotionReturnRevolve3: RefData[];
      designPromotionReturnRevolve4: RefData[];
      designPromotionReturnRevolve6: RefData[];
      designPromotionStatementMessaging1: RefData[];
      designPromotionStatementMessaging2: RefData[];
      designPromotionStatementMessaging3: RefData[];
      designPromotionStatementMessaging4: RefData[];
      designPromotionStatementMessaging5: RefData[];
      designPromotionStatementAdvancedParameter1: RefData[];
      designPromotionStatementAdvancedParameter3: RefData[];
      designPromotionStatementAdvancedParameter4: RefData[];
      designPromotionStatementAdvancedParameter5: RefData[];
      designPromotionStatementAdvancedParameter7: RefData[];
      designPromotionStatementAdvancedParameter8: RefData[];
      designPromotionStatementAdvancedParameter9: RefData[];
      designPromotionStatementAdvancedParameter10: RefData[];
      designPromotionStatementAdvancedParameter11: RefData[];
      designPromotionStatementAdvancedParameter12: RefData[];
      designPromotionStatementAdvancedParameter13: RefData[];
      designPromotionStatementAdvancedParameter14: RefData[];
      designPromotionStatementAdvancedParameter15: RefData[];
      designPromotionStatementAdvancedParameter16: RefData[];
      designPromotionStatementAdvancedParameter17: RefData[];
      designPromotionStatementAdvancedParameter18: RefData[];
      designPromotionStatementAdvancedParameter19: RefData[];
      designPromotionStatementAdvancedParameter20: RefData[];
      designPromotionStatementAdvancedParameter21: RefData[];
      designPromotionStatementAdvancedParameter22: RefData[];
      designPromotionStatementAdvancedParameter23: RefData[];
      designPromotionStatementAdvancedParameter24: RefData[];
      designPromotionStatementAdvancedParameter25: RefData[];
      designPromotionStatementAdvancedParameter26: RefData[];
      designPromotionStatementAdvancedParameter27: RefData[];
      designPromotionStatementAdvancedParameter28: RefData[];
    }
  >(elementMetadataForLCSelector);
};
