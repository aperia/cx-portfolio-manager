import { createSelector } from '@reduxjs/toolkit';
import { ProfileRefData } from 'app/types';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import { MetaData } from 'pages/WorkflowManageAdjustmentsSystem/ConfigJQAStep/types';
import { ParameterCode as MASParameterCode } from 'pages/WorkflowManageAdjustmentsSystem/type';
import { useSelector } from 'react-redux';
import { findByName, mapCodeAndTextToText } from './_helper';

const elementMetadataForManageAdjustmentSystemSelector = createSelector(
  (state: RootState) => state.workflowSetup!.elementMetadata.elements,
  data => {
    const elements = data.map(e => {
      return {
        name: e.name,
        options: e.options?.map(opt => ({
          ...opt,
          code: opt.value || '',
          text: !isEmpty(opt.description) ? opt.description : opt.value
        }))
      } as WorkflowElement;
    });

    const transactions =
      elements.find(findByName(MASParameterCode.TransactionCode))?.options ||
      [];
    const profiles = (elements.find(findByName(MASParameterCode.ProfileName))
      ?.options || []) as ProfileRefData[];
    const batchCode =
      elements.find(findByName(MASParameterCode.BatchCode))?.options || [];
    const actionCode =
      elements
        .find(findByName(MASParameterCode.ActionCode))
        ?.options?.map(mapCodeAndTextToText) || [];

    const queueOwner =
      elements.find(findByName(MetaData.QueueOwner))?.options || [];
    const managerId =
      elements.find(findByName(MetaData.ManagerId))?.options || [];

    const shellNames =
      elements
        .find(findByName(MASParameterCode.ShellName))
        ?.options?.map(mapCodeAndTextToText) || [];

    return {
      transactions,
      profiles,
      batchCode,
      actionCode,
      queueOwner,
      managerId,
      shellNames
    };
  }
);

export const useSelectElementsForManageAdjustmentSystem = () => {
  return useSelector<
    RootState,
    {
      transactions: RefData[];
      profiles: ProfileRefData[];
      batchCode: RefData[];
      actionCode: RefData[];
      queueOwner: RefData[];
      managerId: RefData[];
      shellNames: RefData[];
    }
  >(elementMetadataForManageAdjustmentSystemSelector);
};

const selectQueueProfileStatus = createSelector(
  (state: RootState) => state.workflowSetup!.queueProfile,
  data => data
);
export const useQueueProfileStatus = () => {
  return useSelector<RootState, IFetching>(selectQueueProfileStatus);
};

const selectOcsShellProfileStatus = createSelector(
  (state: RootState) => state.workflowSetup!.ocsShellProfile,
  data => data
);
export const useOcsShellProfileStatus = () => {
  return useSelector<RootState, IFetching>(selectOcsShellProfileStatus);
};

const selectExportShellProfileStatus = createSelector(
  (state: RootState) => state.workflowSetup!.exportShellProfile,
  data => data
);
export const useExportShellProfileStatus = () => {
  return useSelector<RootState, IFetching>(selectExportShellProfileStatus);
};

const selectProfileTransactionsStatus = createSelector(
  (state: RootState) => state.workflowSetup!.profileTransactions,
  data => data
);
export const useProfileTransactionsStatus = () => {
  return useSelector<RootState, IFetching>(selectProfileTransactionsStatus);
};
