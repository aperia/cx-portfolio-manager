import { renderHook } from '@testing-library/react-hooks';
import { WorkflowMetadataAll } from 'app/fixtures/workflow-metadata';
import {
  GetMethodListData,
  GetMethodListDataMapping
} from 'app/fixtures/workflow-method-list';
import { mappingDataFromArray } from 'app/helpers';
import { WorkflowSetupRootState } from 'app/types';
import * as reactRedux from 'react-redux';
import { defaultWorkflowMethodsFilter } from '../reducers';
import {
  useSelectElementMetadataForChangeInterest,
  useSelectStrategyFlyoutChangeInterestRatesSelector,
  useSelectWorkflowMethodsChangeInterestRatesSelector
} from './workflowChangeInterestRates';

describe('redux-store > workflow-setup > select-hooks ', () => {
  let selectorMock: jest.SpyInstance;
  const mockSelectorWorkflowSetup = (
    workflowSetup:
      | Pick<WorkflowSetupRootState, 'workflowFormSteps'>
      | Pick<WorkflowSetupRootState, 'workflowSetup'>
      | Pick<WorkflowSetupRootState, 'workflowMethods'>
      | Pick<WorkflowSetupRootState, 'workflowInstance'>
      | Pick<WorkflowSetupRootState, 'elementMetadata'>
      | Pick<WorkflowSetupRootState, 'workflowChangeSets'>
      | Pick<WorkflowSetupRootState, 'createNewChange'>
      | Pick<WorkflowSetupRootState, 'parameterData'>
  ) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector => selector({ workflowSetup }));
  };

  afterEach(() => {
    selectorMock.mockClear();
  });

  it('test on selector useSelectWorkflowMethodsChangeInterestRatesSelector', () => {
    const mockMethodList = mappingDataFromArray(
      GetMethodListData,
      GetMethodListDataMapping!
    );

    mockSelectorWorkflowSetup({
      workflowMethods: {
        loading: undefined,
        error: undefined,
        methods: mockMethodList,
        dataFilter: defaultWorkflowMethodsFilter
      }
    });

    const { result } = renderHook(() =>
      useSelectWorkflowMethodsChangeInterestRatesSelector()
    );

    expect(result.current.methods.length).toEqual(2);
  });

  it('test on selector useSelectWorkflowMethodsChangeInterestRatesSelector > empty data filter', () => {
    const mockMethodList = mappingDataFromArray(
      GetMethodListData,
      GetMethodListDataMapping!
    );

    mockSelectorWorkflowSetup({
      workflowMethods: {
        loading: undefined,
        error: undefined,
        methods: mockMethodList,
        dataFilter: undefined
      }
    });

    const { result } = renderHook(() =>
      useSelectWorkflowMethodsChangeInterestRatesSelector()
    );

    expect(result.current.methods.length).toEqual(0);
  });

  it('test on selector useSelectElementMetadataForChangeInterest', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: { elements: WorkflowMetadataAll }
    } as any);

    const { rerender, result } = renderHook(() =>
      useSelectElementMetadataForChangeInterest()
    );

    const expectResult = {
      defaultCashBaseInterest: result.current.defaultCashBaseInterest.length,
      defaultMerchandiseBaseInterest:
        result.current.defaultMerchandiseBaseInterest.length,
      defaultMaximumCashInterest:
        result.current.defaultMaximumCashInterest.length,
      defaultMaximumMerchandiseInterest:
        result.current.defaultMaximumMerchandiseInterest.length,
      defaultMinimumCashInterest:
        result.current.defaultMinimumCashInterest.length,
      defaultMinimumMerchandiseInterest:
        result.current.defaultMinimumMerchandiseInterest.length,
      indexRateMethod: result.current.indexRateMethod.length,
      defaultBaseInterestUsage: result.current.defaultBaseInterestUsage.length,
      defaultOverrideInterestLimit:
        result.current.defaultOverrideInterestLimit.length,
      defaultMinimumUsage: result.current.defaultMinimumUsage.length,
      defaultMaximumUsage: result.current.defaultMaximumUsage.length,
      defaultMaximumInterestUsage:
        result.current.defaultMaximumInterestUsage.length,
      incentivePricingUsageCode:
        result.current.incentivePricingUsageCode.length,
      incentiveBaseInterestUsageRate:
        result.current.incentiveBaseInterestUsageRate.length,
      delinquencyCyclesForTermination:
        result.current.delinquencyCyclesForTermination.length,
      minimumMaximumRateOverrideOptions:
        result.current.minimumMaximumRateOverrideOptions.length,
      incentivePricingOverrideOption:
        result.current.incentivePricingOverrideOption.length,
      indexRateMethodName: result.current.indexRateMethodName.length,
      terminateIncentivePricingOnPricingStrategyChange:
        result.current.terminateIncentivePricingOnPricingStrategyChange.length,
      methodOverrideTerminationOption:
        result.current.methodOverrideTerminationOption.length,
      endofIncentiveWarningNotificationTiming:
        result.current.endofIncentiveWarningNotificationTiming.length,
      endofIncentiveWarningNotificationStatementText:
        result.current.endofIncentiveWarningNotificationStatementText.length,
      incentivePricingNumberOfMonths:
        result.current.incentivePricingNumberOfMonths.length,
      messageForProtectedBalanceIncentivePricing:
        result.current.messageForProtectedBalanceIncentivePricing.length,
      interestRateRoundingCalculation:
        result.current.interestRateRoundingCalculation.length,
      dailyAPRRoundingCalculation:
        result.current.dailyAPRRoundingCalculation.length,
      stopCalculatingInterestAfterNumberofDaysDelinquent:
        result.current.stopCalculatingInterestAfterNumberofDaysDelinquent
          .length,
      includeAdditionalCreditBalanceInAverageDailyBalanceCalculation:
        result.current
          .includeAdditionalCreditBalanceInAverageDailyBalanceCalculation
          .length,
      combineMerchandiseAndCashBalancesOnInterestCalculations:
        result.current.combineMerchandiseAndCashBalancesOnInterestCalculations
          .length,
      restartInterestCalculationsAfterClearingDelinquency:
        result.current.restartInterestCalculationsAfterClearingDelinquency
          .length,
      calculateCompoundInterest:
        result.current.calculateCompoundInterest.length,
      overridePromotionalRatesForPricingStrategies:
        result.current.overridePromotionalRatesForPricingStrategies.length,
      monthlyInterestCalculationOptions:
        result.current.monthlyInterestCalculationOptions.length,
      dailyInterestRoundingOption:
        result.current.dailyInterestRoundingOption.length,
      dailyInterestAmountTruncationOption:
        result.current.dailyInterestAmountTruncationOption.length,
      overrideInterestRatesForProtectedBalance:
        result.current.overrideInterestRatesForProtectedBalance.length,
      minimumDaysForCycleCodeChange:
        result.current.minimumDaysForCycleCodeChange.length,
      roundNumberOfDaysInCycleForInterestCalculations:
        result.current.roundNumberOfDaysInCycleForInterestCalculations.length,
      cycleCodeChangeOptions: result.current.cycleCodeChangeOptions.length,
      cyclePeriodChangeCISMemoOption:
        result.current.cyclePeriodChangeCISMemoOption.length,
      interestMethodOldCash: result.current.interestMethodOldCash.length,
      interestMethodCycleToDateCash:
        result.current.interestMethodCycleToDateCash.length,
      interestMethodTwoCycleOldCash:
        result.current.interestMethodTwoCycleOldCash.length,
      interestMethodOneCycleOldMerchandise:
        result.current.interestMethodOneCycleOldMerchandise.length,
      interestMethodCycleToDateMerchandise:
        result.current.interestMethodCycleToDateMerchandise.length,
      additionalInterestOnMerchandise1CyclesDelinquent:
        result.current.additionalInterestOnMerchandise1CyclesDelinquent.length,
      additionalInterestOnMerchandise2CyclesDelinquent:
        result.current.additionalInterestOnMerchandise2CyclesDelinquent.length,
      additionalInterestOnCash2CyclesDelinquent:
        result.current.additionalInterestOnCash2CyclesDelinquent.length,
      additionalInterestOnMerchandise3CyclesDelinquent:
        result.current.additionalInterestOnMerchandise3CyclesDelinquent.length,
      additionalInterestOnCash3CyclesDelinquent:
        result.current.additionalInterestOnCash3CyclesDelinquent.length,
      automatedMemosForAdditionalInterest:
        result.current.automatedMemosForAdditionalInterest.length,
      useExternalStatusesForAdditionalInterest:
        result.current.useExternalStatusesForAdditionalInterest.length,
      externalStatus1ForAdditionalInterest:
        result.current.externalStatus1ForAdditionalInterest.length,
      externalStatus2ForAdditionalInterest:
        result.current.externalStatus2ForAdditionalInterest.length,
      externalStatus3ForAdditionalInterest:
        result.current.externalStatus3ForAdditionalInterest.length,
      externalStatus4ForAdditionalInterest:
        result.current.externalStatus4ForAdditionalInterest.length,
      externalStatus5ForAdditionalInterest:
        result.current.externalStatus5ForAdditionalInterest.length,
      penaltyPricingStatusReasonCodeTable:
        result.current.penaltyPricingStatusReasonCodeTable.length,
      penaltyPricingTimingOptions:
        result.current.penaltyPricingTimingOptions.length,
      penaltyPricingStatementMessage1CyclesOldBalance:
        result.current.penaltyPricingStatementMessage1CyclesOldBalance.length,
      penaltyPricingStatementMessage2CyclesOldBalance:
        result.current.penaltyPricingStatementMessage2CyclesOldBalance.length,
      penaltyPricingStatementMessage3CyclesOldBalance:
        result.current.penaltyPricingStatementMessage3CyclesOldBalance.length,
      cureDelinquencyFor1CycleDelinquentBalances:
        result.current.cureDelinquencyFor1CycleDelinquentBalances.length,
      cureDelinquencyFor2CycleDelinquentBalances:
        result.current.cureDelinquencyFor2CycleDelinquentBalances.length,
      cureDelinquencyFor3CycleDelinquentBalances:
        result.current.cureDelinquencyFor3CycleDelinquentBalances.length,
      penaltyPricingOnPromotions:
        result.current.penaltyPricingOnPromotions.length,
      citMethodForPenaltyPricing:
        result.current.citMethodForPenaltyPricing.length,
      includeZeroBalanceDaysInADBCalculation:
        result.current.includeZeroBalanceDaysInADBCalculation.length
    };

    expect(expectResult).toEqual({
      defaultCashBaseInterest: 0,
      defaultMerchandiseBaseInterest: 0,
      defaultMaximumCashInterest: 0,
      defaultMaximumMerchandiseInterest: 0,
      defaultMinimumCashInterest: 0,
      defaultMinimumMerchandiseInterest: 0,
      indexRateMethod: 4,
      defaultBaseInterestUsage: 5,
      defaultOverrideInterestLimit: 0,
      defaultMinimumUsage: 4,
      defaultMaximumUsage: 3,
      defaultMaximumInterestUsage: 3,
      incentivePricingUsageCode: 4,
      incentiveBaseInterestUsageRate: 3,
      delinquencyCyclesForTermination: 11,
      minimumMaximumRateOverrideOptions: 4,
      incentivePricingOverrideOption: 3,
      indexRateMethodName: 0,
      terminateIncentivePricingOnPricingStrategyChange: 3,
      methodOverrideTerminationOption: 3,
      endofIncentiveWarningNotificationTiming: 14,
      endofIncentiveWarningNotificationStatementText: 4,
      incentivePricingNumberOfMonths: 4,
      messageForProtectedBalanceIncentivePricing: 4,
      interestRateRoundingCalculation: 0,
      dailyAPRRoundingCalculation: 0,
      stopCalculatingInterestAfterNumberofDaysDelinquent: 0,
      includeAdditionalCreditBalanceInAverageDailyBalanceCalculation: 0,
      combineMerchandiseAndCashBalancesOnInterestCalculations: 0,
      restartInterestCalculationsAfterClearingDelinquency: 0,
      calculateCompoundInterest: 0,
      overridePromotionalRatesForPricingStrategies: 0,
      monthlyInterestCalculationOptions: 0,
      dailyInterestRoundingOption: 0,
      dailyInterestAmountTruncationOption: 0,
      overrideInterestRatesForProtectedBalance: 0,
      minimumDaysForCycleCodeChange: 0,
      roundNumberOfDaysInCycleForInterestCalculations: 0,
      cycleCodeChangeOptions: 0,
      cyclePeriodChangeCISMemoOption: 0,
      interestMethodOldCash: 0,
      interestMethodCycleToDateCash: 0,
      interestMethodTwoCycleOldCash: 0,
      interestMethodOneCycleOldMerchandise: 0,
      interestMethodCycleToDateMerchandise: 0,
      additionalInterestOnMerchandise1CyclesDelinquent: 0,
      additionalInterestOnMerchandise2CyclesDelinquent: 0,
      additionalInterestOnCash2CyclesDelinquent: 0,
      additionalInterestOnMerchandise3CyclesDelinquent: 0,
      additionalInterestOnCash3CyclesDelinquent: 0,
      automatedMemosForAdditionalInterest: 0,
      useExternalStatusesForAdditionalInterest: 0,
      externalStatus1ForAdditionalInterest: 0,
      externalStatus2ForAdditionalInterest: 0,
      externalStatus3ForAdditionalInterest: 0,
      externalStatus4ForAdditionalInterest: 0,
      externalStatus5ForAdditionalInterest: 0,
      penaltyPricingStatusReasonCodeTable: 0,
      penaltyPricingTimingOptions: 0,
      penaltyPricingStatementMessage1CyclesOldBalance: 0,
      penaltyPricingStatementMessage2CyclesOldBalance: 0,
      penaltyPricingStatementMessage3CyclesOldBalance: 0,
      cureDelinquencyFor1CycleDelinquentBalances: 0,
      cureDelinquencyFor2CycleDelinquentBalances: 0,
      cureDelinquencyFor3CycleDelinquentBalances: 0,
      penaltyPricingOnPromotions: 0,
      citMethodForPenaltyPricing: 0,
      includeZeroBalanceDaysInADBCalculation: 0
    });

    mockSelectorWorkflowSetup({
      elementMetadata: { elements: [] }
    } as any);
    rerender();
    expect(result.current.indexRateMethod.length).toEqual(0);
  });

  it('test on selector useSelectStrategyFlyoutChangeInterestRatesSelector > have strategy data', () => {
    mockSelectorWorkflowSetup({
      workflowMethods: {
        loading: undefined,
        error: undefined,
        methods: [],
        strategyFlyoutData: {
          id: 'mock-id',
          isNewVersion: true,
          show: true,
          methodName: 'mock-name',
          strategyList: []
        },
        dataFilter: undefined
      }
    });

    const { result } = renderHook(() =>
      useSelectStrategyFlyoutChangeInterestRatesSelector()
    );

    expect(result.current).toEqual({
      id: 'mock-id',
      isNewVersion: true,
      show: true,
      methodName: 'mock-name',
      strategyList: []
    });
  });

  it('test on selector useSelectStrategyFlyoutChangeInterestRatesSelector > no data', () => {
    mockSelectorWorkflowSetup({
      workflowMethods: {
        loading: undefined,
        error: undefined,
        methods: [],
        dataFilter: undefined
      }
    });

    const { result } = renderHook(() =>
      useSelectStrategyFlyoutChangeInterestRatesSelector()
    );

    expect(result.current).toBeNull();
  });
});
