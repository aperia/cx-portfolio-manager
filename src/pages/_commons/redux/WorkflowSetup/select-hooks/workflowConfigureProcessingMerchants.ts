import { createSelector } from '@reduxjs/toolkit';
import { isEmpty, orderBy } from 'app/_libraries/_dls/lodash';
import { ParameterCode as CPMParameterCode } from 'pages/WorkflowConfigureProcessingMerchants/ConfigParametersStep/type';
import { useSelector } from 'react-redux';
import { findByName, mapCodeAndTextToText } from './_helper';

const elementMetadataForConfigProcessingMerchantsSelector = createSelector(
  (state: RootState) => state.workflowSetup!.elementMetadata.elements,
  data => {
    const elements = data.map(e => {
      return {
        name: e.name,
        options: e.options?.map(opt => ({
          ...opt,
          code: opt.value,
          text: !isEmpty(opt.description) ? opt.description : opt.value
        }))
      } as WorkflowElement;
    });

    const systemPrinciple =
      elements
        .find(findByName(CPMParameterCode.SystemPrinciple))
        ?.options?.map(o => o.code) || [];
    const systems = orderBy(
      systemPrinciple.reduce((pre, curr) => {
        const system = curr.split('-')[0];

        if (!pre.some(s => s.code === system))
          pre.push({ code: system, text: system });
        return pre;
      }, [] as RefData[]),
      ['text'],
      ['asc']
    );
    const principles = systemPrinciple.reduce((pre, curr) => {
      const sp = curr.split('-');
      const system = sp[0];
      const principle = sp[1];

      if (!pre[system]) pre[system] = [];

      if (!pre[system].some(s => s.code === principle))
        pre[system] = [...pre[system], { code: principle, text: principle }];

      return pre;
    }, {} as Record<string, RefData[]>);

    const assessmentCode =
      elements
        .find(findByName(CPMParameterCode.AssessmentCode))
        ?.options?.map(mapCodeAndTextToText) || [];
    const informationIndicator =
      elements
        .find(findByName(CPMParameterCode.InformationIndicator))
        ?.options?.map(mapCodeAndTextToText) || [];
    const aggregatorType =
      elements
        .find(findByName(CPMParameterCode.AggregatorType))
        ?.options?.map(mapCodeAndTextToText) || [];
    const TINType =
      elements
        .find(findByName(CPMParameterCode.TINType))
        ?.options?.map(mapCodeAndTextToText) || [];
    const sparkExclusionIndicator =
      elements
        .find(findByName(CPMParameterCode.SparkExclusionIndicator))
        ?.options?.map(mapCodeAndTextToText) || [];
    const transactionDecisionTableId =
      elements.find(findByName(CPMParameterCode.TransactionDecisionTableId))
        ?.options || [];
    const promotionDiscountRateTableId =
      elements.find(findByName(CPMParameterCode.PromotionDiscountRateTableId))
        ?.options || [];
    const inStorePayment =
      elements
        .find(findByName(CPMParameterCode.InStorePayment))
        ?.options?.map(mapCodeAndTextToText) || [];
    const merchantPaymentDescription =
      elements
        .find(findByName(CPMParameterCode.MerchantPaymentDescription))
        ?.options?.map(mapCodeAndTextToText) || [];

    const processFlag =
      elements
        .find(findByName(CPMParameterCode.ProcessFlag))
        ?.options?.map(mapCodeAndTextToText) || [];
    const discountMethodParticipation =
      elements
        .find(findByName(CPMParameterCode.DiscountMethodParticipation))
        ?.options?.map(mapCodeAndTextToText) || [];
    return {
      systems,
      principles,

      assessmentCode,
      informationIndicator,
      aggregatorType,
      TINType,
      sparkExclusionIndicator,
      transactionDecisionTableId,
      promotionDiscountRateTableId,
      inStorePayment,
      merchantPaymentDescription,
      processFlag,
      discountMethodParticipation
    };
  }
);

export const useSelectElementsForConfigProcessingMerchants = () => {
  return useSelector<
    RootState,
    {
      systems: RefData[];
      principles: Record<string, RefData[]>;

      assessmentCode: RefData[];
      informationIndicator: RefData[];
      aggregatorType: RefData[];
      TINType: RefData[];
      sparkExclusionIndicator: RefData[];
      transactionDecisionTableId: RefData[];
      promotionDiscountRateTableId: RefData[];
      inStorePayment: RefData[];
      merchantPaymentDescription: RefData[];
      processFlag: RefData[];
      discountMethodParticipation: RefData[];
    }
  >(elementMetadataForConfigProcessingMerchantsSelector);
};

const merchantListSelector = createSelector(
  (state: RootState) => state.workflowSetup!.merchantData?.merchants,
  data => data
);
export const useMerchantList = () => {
  return useSelector<RootState, MerchantItem[]>(merchantListSelector);
};

const merchantListStatusSelector = createSelector(
  (state: RootState) => state.workflowSetup!.merchantData?.loading,
  (state: RootState) => state.workflowSetup!.merchantData?.error,
  (loading, error) => ({ loading, error })
);
export const useMerchantListStatus = () => {
  return useSelector<RootState, IFetching>(merchantListStatusSelector);
};

const exportMerchantsStatusSelector = createSelector(
  (state: RootState) => state.workflowSetup!.exportMerchant?.loading,
  (state: RootState) => state.workflowSetup!.exportMerchant?.error,
  (loading, error) => ({ loading, error })
);
export const useExportMerchantsStatus = () => {
  return useSelector<RootState, IFetching>(exportMerchantsStatusSelector);
};
