import { renderHook } from '@testing-library/react-hooks';
import { WorkflowSetupRootState } from 'app/types';
import * as reactRedux from 'react-redux';
import {
  useSelectMethods,
  useSelectStrategyData,
  useSelectStrategyDataFetchingStatus
} from '.';

describe('redux-store > workflow-setup > select-hooks ', () => {
  let selectorMock: jest.SpyInstance;
  const mockSelectorWorkflowSetup = (
    workflowSetup:
      | Pick<WorkflowSetupRootState, 'workflowFormSteps'>
      | Pick<WorkflowSetupRootState, 'workflowSetup'>
      | Pick<WorkflowSetupRootState, 'workflowMethods'>
      | Pick<WorkflowSetupRootState, 'workflowInstance'>
      | Pick<WorkflowSetupRootState, 'elementMetadata'>
      | Pick<WorkflowSetupRootState, 'workflowChangeSets'>
      | Pick<WorkflowSetupRootState, 'createNewChange'>
      | Pick<WorkflowSetupRootState, 'parameterData'>
  ) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector => selector({ workflowSetup }));
  };

  afterEach(() => {
    selectorMock.mockClear();
  });

  it('test on selector useSelectStrategyData ', () => {
    mockSelectorWorkflowSetup({
      strategyData: {
        strategyList: [
          {
            id: '1',
            name: 'name',
            cisDescription: 'description',
            effectiveDate: '10-20-2021',
            status: 'status',
            processed: '1',
            accounts: '2000',
            checked: 'false'
          }
        ]
      }
    } as any);

    const { result } = renderHook(() => useSelectStrategyData());

    expect(result.current[0].name).toEqual('name');
  });
  it('test on selector useSelectStrategyDataFetchingStatus ', () => {
    mockSelectorWorkflowSetup({
      strategyData: {
        strategyList: [
          {
            id: '1',
            name: 'name',
            cisDescription: 'description',
            effectiveDate: '10-20-2021',
            status: 'status',
            processed: '1',
            accounts: '2000',
            checked: 'false'
          }
        ]
      }
    } as any);

    const { result } = renderHook(() => useSelectStrategyDataFetchingStatus());

    expect(result.current.error).toEqual(undefined);
  });
  it('test on selector useSelectMethods ', () => {
    mockSelectorWorkflowSetup({
      strategyData: {
        methods: [
          {
            id: '1',
            name: 'name',
            cisDescription: 'description',
            effectiveDate: '10-20-2021',
            status: 'status',
            processed: '1',
            accounts: '2000',
            checked: 'false'
          }
        ]
      }
    } as any);

    const { result } = renderHook(() => useSelectMethods());

    expect(result.current.methods).toEqual(undefined);
  });
  it('test on selector useSelectMethods ', () => {
    mockSelectorWorkflowSetup({} as any);

    const { result } = renderHook(() => useSelectMethods());

    expect(result.current.methods).toEqual(undefined);
  });
});
