import { IServicesData } from 'app/types';
import { orderBy } from 'app/_libraries/_dls/lodash';
import { shallowEqual, useSelector } from 'react-redux';
import { createSelector } from 'reselect';

const elementMetadataSelector = createSelector(
  (state: RootState) =>
    state.workflowSetup!.elementMetadataNonWorkflow.elements,
  data => {
    const elements = data?.map((e: any) => {
      return {
        name: e.name,
        options: orderBy(
          e.options?.map((opt: any) => {
            const des = opt.description ? ` - ${opt.description}` : '';
            const text = opt.value ? `${opt.value}${des}` : opt.description;
            const textDmm = opt.value
              ? `${opt.description}` + ` (${opt.value})`
              : `${opt.description}`;

            return {
              code: opt.value,
              text: text,
              textDmm: textDmm,
              orderText: opt.value ? text : '0',
              selected: opt.selected
            };
          }),
          ['orderText'],
          ['asc']
        )
      } as {
        name: string;
        options: OptionsData[];
      };
    });

    const systemPrincipalAgentsOptions =
      elements.find((e: any) => e.name === 'system.principal.agents')
        ?.options || [];

    const dmmTableTypeOptions =
      elements.find((e: any) => e.name === 'dmm.table.type')?.options || [];

    const alpTableOptions =
      elements.find((e: any) => e.name === 'alp.table')?.options || [];

    const mlpTableOptions =
      elements.find((e: any) => e.name === 'mlp.table')?.options || [];

    const tlpTableOptions =
      elements.find((e: any) => e.name === 'tlp.table')?.options || [];

    return {
      systemPrincipalAgentsOptions,
      dmmTableTypeOptions,
      alpTableOptions,
      mlpTableOptions,
      tlpTableOptions
    };
  }
);

export const useSelectElementMetadataNonWorkflow = () => {
  return useSelector<
    RootState,
    {
      systemPrincipalAgentsOptions: OptionsData[];
      dmmTableTypeOptions: OptionsData[];
      alpTableOptions: OptionsData[];
      mlpTableOptions: OptionsData[];
      tlpTableOptions: OptionsData[];
    }
  >(elementMetadataSelector);
};

export const useSelectServicesData = () => {
  return useSelector<RootState, IServicesData[]>(
    state => state.workflowSetup!.services?.data,
    shallowEqual
  );
};

const useSelectTableListForWorkflowSetupSelector = createSelector(
  (state: RootState) => state.workflowSetup!.tableListWorkflowSetup.data,
  data => {
    const tables: OptionsData[] = data.map(x => {
      return { code: x.tableId, text: x.tableName };
    });

    return tables;
  }
);

export const useSelectTableListForWorkflowSetup = () => {
  return useSelector<RootState, OptionsData[]>(
    useSelectTableListForWorkflowSetupSelector
  );
};
