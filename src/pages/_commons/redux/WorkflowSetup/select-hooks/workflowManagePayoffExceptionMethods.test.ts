import { renderHook } from '@testing-library/react-hooks';
import { WorkflowSetupRootState } from 'app/types';
import * as reactRedux from 'react-redux';
import {
  useSelectElementMetadataForPayoffMethods,
  useSelectExpandingPayoffExceptionMethod,
  useSelectWorkflowPayoffExceptionMethodsFilterSelector,
  useSelectWorkflowPayoffExceptionMethodsSelector
} from '.';

describe('redux-store > workflow-setup > select-hooks ', () => {
  let selectorMock: jest.SpyInstance;
  const mockSelectorWorkflowSetup = (
    workflowSetup:
      | Pick<WorkflowSetupRootState, 'workflowFormSteps'>
      | Pick<WorkflowSetupRootState, 'workflowSetup'>
      | Pick<WorkflowSetupRootState, 'workflowMethods'>
      | Pick<WorkflowSetupRootState, 'workflowInstance'>
      | Pick<WorkflowSetupRootState, 'elementMetadata'>
      | Pick<WorkflowSetupRootState, 'workflowChangeSets'>
      | Pick<WorkflowSetupRootState, 'createNewChange'>
      | Pick<WorkflowSetupRootState, 'parameterData'>
  ) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector => selector({ workflowSetup }));
  };

  afterEach(() => {
    selectorMock.mockClear();
  });

  it('test on selector useSelectWorkflowPayoffExceptionMethodsSelector', () => {
    mockSelectorWorkflowSetup({
      workflowMethods: {
        methods: [
          {
            id: 'id',
            name: 'name',
            effectiveDate: 'eff',
            comment: 'comment',
            relatedPricingStrategies: 'relatedPricingStrategies',
            numberOfVersions: 'numberOfVersions'
          }
        ]
      }
    } as any);

    const { rerender, result } = renderHook(() =>
      useSelectWorkflowPayoffExceptionMethodsSelector()
    );

    rerender();
    expect(result.current).toEqual({
      error: false,
      loading: false,
      methods: [],
      total: 1
    });
  });

  it('test on selector useSelectExpandingPayoffExceptionMethod', () => {
    mockSelectorWorkflowSetup({
      workflowMethods: {
        expandingMethod: '123'
      }
    } as any);

    const { rerender, result } = renderHook(() =>
      useSelectExpandingPayoffExceptionMethod()
    );

    rerender();
    expect(result.current).toEqual('123');
  });

  it('test on selector useSelectElementMetadataForPayoffMethods', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: {
        elements: [
          {
            name: 'old.cash',
            options: [
              { value: '256', description: 'description' },
              { name: '256' }
            ]
          },
          {
            name: 'old.cash',
            options: [{ value: '2567' }, { name: '123' }]
          }
        ]
      }
    } as any);

    const { rerender, result } = renderHook(() =>
      useSelectElementMetadataForPayoffMethods()
    );

    rerender();
    expect(result.current).toEqual({
      'old.cash': [
        { code: '2567', text: '2567' },
        { code: undefined, text: 'undefined' }
      ]
    });
  });

  it('test on selector useSelectWorkflowPayoffExceptionMethodsFilterSelector', () => {
    mockSelectorWorkflowSetup({
      workflowMethods: {
        methods: '',
        dataFilter: {
          sortBy: 'asc',
          orderBy: 'asc',
          page: 1,
          pageSize: 20
        }
      }
    } as any);

    const { rerender, result } = renderHook(() =>
      useSelectWorkflowPayoffExceptionMethodsFilterSelector()
    );

    rerender();
    expect(result.current).toEqual({
      sortBy: 'asc',
      orderBy: 'asc',
      page: 1,
      pageSize: 20
    });
  });

  it('test on selector useSelectWorkflowPayoffExceptionMethodsFilterSelector with empty filter', () => {
    mockSelectorWorkflowSetup({
      workflowMethods: {
        methods: ''
      }
    } as any);

    const { rerender, result } = renderHook(() =>
      useSelectWorkflowPayoffExceptionMethodsFilterSelector()
    );

    rerender();
    expect(result.current).toEqual({});
  });
});
