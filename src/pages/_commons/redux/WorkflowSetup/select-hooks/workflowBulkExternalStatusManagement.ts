import { createSelector } from '@reduxjs/toolkit';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import { orderBy } from 'app/_libraries/_dls/lodash';
import { useSelector } from 'react-redux';

const elementMetadataForBulkExternalStatusSelector = createSelector(
  (state: RootState) => state.workflowSetup!.elementMetadata.elements,
  data => {
    const elements = data.map(e => {
      const options =
        e.options?.map(opt => {
          const des = opt.description ? ` - ${opt.description}` : '';
          let text = opt.value ? `${opt.value}${des}` : opt.description;
          if (!!opt.name) {
            text = `${opt.name} ${opt.description}`;
          }
          switch (e.name) {
            case MethodFieldParameterEnum.BulkExternalStatusManagementTransactionId:
              text = `${opt.name}, ${opt.description}`;
              break;
            case MethodFieldParameterEnum.BulkExternalStatusManagementTransaction:
              text = opt.description;
              break;
          }

          return {
            code: opt.value || '',
            text: text,
            description: opt.description,
            orderText: opt.value ? text : '0',
            selected: opt.selected,
            type: opt.type,
            workFlow: opt.workFlow
          };
        }) || [];
      return {
        name: e.name,
        options: orderBy(
          options.filter(f => f.code?.toLowerCase() !== 'blank'),
          ['orderText'],
          ['asc']
        ).concat(options.filter(f => f.code?.toLowerCase() === 'blank'))
      } as {
        name: string;
        options: any[];
      };
    });

    const bulkExternalStatusManagementStatusCode =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.BulkExternalStatusManagementStatusCode
      )?.options || [];

    const bulkExternalStatusManagementReasonCode =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.BulkExternalStatusManagementReasonCode
      )?.options || [];

    const bulkExternalStatusManagementTransactionId =
      elements
        .find(
          e =>
            e.name ===
            MethodFieldParameterEnum.BulkExternalStatusManagementTransactionId
        )
        ?.options?.filter((f: any) => f.workFlow === 'besm' || !f.workFlow) ||
      [];

    const bulkExternalStatusManagementTransaction = (elements
      .find(
        e =>
          e.name ===
          MethodFieldParameterEnum.BulkExternalStatusManagementTransaction
      )
      ?.options?.filter((f: any) => f.workFlow === 'besm' || !f.workFlow) ||
      []) as (RefData & {
      type?: string;
    })[];

    return {
      bulkExternalStatusManagementStatusCode,
      bulkExternalStatusManagementReasonCode,
      bulkExternalStatusManagementTransactionId,
      bulkExternalStatusManagementTransaction
    };
  }
);

export const useSelectElementMetadataForBulkExternalStatus = () => {
  return useSelector<
    RootState,
    {
      bulkExternalStatusManagementStatusCode: RefData[];
      bulkExternalStatusManagementReasonCode: RefData[];
      bulkExternalStatusManagementTransactionId: (RefData & {
        description?: string;
      })[];
      bulkExternalStatusManagementTransaction: (RefData & { type?: string })[];
    }
  >(elementMetadataForBulkExternalStatusSelector);
};
