import { createSelector } from '@reduxjs/toolkit';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import { matchSearchValue } from 'app/helpers';
import { orderBy } from 'app/_libraries/_dls/lodash';
import _orderBy from 'lodash.orderby';
import { useSelector } from 'react-redux';
const workflowIncomeOptionMethodsSelector = createSelector(
  (state: RootState) => state.workflowSetup!.workflowMethods,
  data => {
    const {
      methods: rawData,
      dataFilter = {},
      loading = false,
      error = false
    } = data;
    const { orderBy, page, pageSize, searchValue = '' } = dataFilter;
    const _searchValue = searchValue.toLowerCase() || '';
    const _method = _orderBy(rawData, ['name'], orderBy).filter(w => {
      return matchSearchValue([w.name], _searchValue);
    });
    const total = _method.length;
    const methods = _method.slice((page! - 1) * pageSize!, page! * pageSize!);
    return { methods, total, loading, error };
  }
);

export const useSelectWorkflowIncomeOptionMethodsSelector = () => {
  return useSelector<
    RootState,
    {
      methods: IMethodModel[];
      loading: boolean;
      error: boolean;
      total: number;
    }
  >(workflowIncomeOptionMethodsSelector);
};

const elementMetadataForManageIncomeOptionMethodsSelector = createSelector(
  (state: RootState) => state.workflowSetup!.elementMetadata.elements,
  data => {
    const elements = data.map(e => {
      return {
        name: e.name,
        options: orderBy(
          e.options?.map(opt => {
            const des = opt.description ? ` - ${opt.description}` : '';
            const text = opt.value ? `${opt.value}${des}` : opt.description;

            return {
              code: opt.value,
              text: text,
              orderText: opt.value ? text : '0',
              selected: opt.selected
            };
          }),
          ['orderText'],
          ['asc']
        )
      } as {
        name: string;
        options: RefData[];
      };
    });

    const manageIncomeOptionMethodsMcJoiningFeeStatementDescriptionOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageIncomeOptionMethodsMcJoiningFeeStatementDescription
      )?.options || [];

    const manageIncomeOptionMethodsMcJoiningFeeBatchIdOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageIncomeOptionMethodsMcJoiningFeeBatchId
      )?.options || [];

    const manageIncomeOptionMethodsMcCardReplacementFeeBatchIdOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageIncomeOptionMethodsMcCardReplacementFeeBatchId
      )?.options || [];

    const manageIncomeOptionMethodsMcCardReplacementFeeStatementDescriptionOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageIncomeOptionMethodsMcCardReplacementFeeStatementDescription
      )?.options || [];

    const manageIncomeOptionMethodsMcFirstYearMaximumFeeManagementOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageIncomeOptionMethodsMcFirstYearMaximumFeeManagement
      )?.options || [];

    const manageIncomeOptionMethodsAcBatchTypeCodeOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcBatchTypeCode
      )?.options || [];

    const manageIncomeOptionMethodsAcIncludeAnnualChargeInInterestCalculationsOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcIncludeAnnualChargeInInterestCalculations
      )?.options || [];

    const manageIncomeOptionMethodsAcPreventDelinquencyFromAnnualFeeBalanceOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPreventDelinquencyFromAnnualFeeBalance
      )?.options || [];

    const manageIncomeOptionMethodsAcAnnualChargeOptionOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualChargeOption
      )?.options || [];

    const manageIncomeOptionMethodsAcFirstYearMaximumFeeManagementOptionOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFirstYearMaximumFeeManagementOption
      )?.options || [];

    const manageIncomeOptionMethodsAcPrenotificationAmountChangeOptionsOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPrenotificationAmountChangeOptions
      )?.options || [];

    const manageIncomeOptionMethodsAcAnnualChargeStatementDisplayControlOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualChargeStatementDisplayControl
      )?.options || [];

    const manageIncomeOptionMethodsAcPreAnnualChargeNotificationsOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPreAnnualChargeNotifications
      )?.options || [];

    const manageIncomeOptionMethodsAcRegulationZNotificationMediaMethodOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcRegulationZNotificationMediaMethod
      )?.options || [];

    const manageIncomeOptionMethodsAcRegulationZnotificationGenerationPeriodOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcRegulationZnotificationGenerationPeriod
      )?.options || [];

    const manageIncomeOptionMethodsAcPreNotificationMessageIdOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPreNotificationMessageId
      )?.options || [];

    const manageIncomeOptionMethodsAcPlasticsRequirementCodeOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPlasticsRequirementCode
      )?.options || [];

    const manageIncomeOptionMethodsAcSpecialConditionsForExternalStatusOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcSpecialConditionsForExternalStatus
      )?.options || [];

    const manageIncomeOptionMethodsAcSpecialConditionsForExpiredAccountsOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcSpecialConditionsForExpiredAccounts
      )?.options || [];

    const manageIncomeOptionMethodsAcAnnualChargeIncludeTableOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualChargeIncludeTable
      )?.options || [];

    const manageIncomeOptionMethodsAcFinanceChargeSuppressionCodeOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFinanceChargeSuppressionCode
      )?.options || [];

    const manageIncomeOptionMethodsAcNoPrenotificationAssessmentOptionsOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcNoPrenotificationAssessmentOptions
      )?.options || [];

    const manageIncomeOptionMethodsAcExistingAccountStartMonthOptionOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcExistingAccountStartMonthOption
      )?.options || [];

    const manageIncomeOptionMethodsAcNumberOfMonthsToFirstChargeOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcNumberOfMonthsToFirstCharge
      )?.options || [];

    const manageIncomeOptionMethodsAcPostingCycleOptionsOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPostingCycleOptions
      )?.options || [];

    const manageIncomeOptionMethodsAcFeeTimingOptionsOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeTimingOptions
      )?.options || [];

    const manageIncomeOptionMethodsAcSpecifiedMonthForAnnualChargeOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcSpecifiedMonthForAnnualCharge
      )?.options || [];

    const manageIncomeOptionMethodsAcFeeWaiverMonthsOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeWaiverMonths
      )?.options || [];

    const manageIncomeOptionMethodsAcDebitActivityOptionOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcDebitActivityOption
      )?.options || [];

    const manageIncomeOptionMethodsAcWaiverTotalDollarCalculationOptionOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcWaiverTotalDollarCalculationOption
      )?.options || [];

    const manageIncomeOptionMethodsAcRateChangePeriodOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcRateChangePeriod
      )?.options || [];

    const manageIncomeOptionMethodsAcFeeWaiverOptionOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeWaiverOption
      )?.options || [];

    const manageIncomeOptionMethodsAcMinimumBalanceAndExternalStatusOptionOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcMinimumBalanceAndExternalStatusOption
      )?.options || [];

    const manageIncomeOptionMethodsAcReversalStatementMessageWarningTextIDOptions =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcReversalStatementMessageWarningTextID
      )?.options || [];

    return {
      manageIncomeOptionMethodsMcJoiningFeeStatementDescriptionOptions,
      manageIncomeOptionMethodsMcJoiningFeeBatchIdOptions,
      manageIncomeOptionMethodsMcCardReplacementFeeBatchIdOptions,
      manageIncomeOptionMethodsMcCardReplacementFeeStatementDescriptionOptions,
      manageIncomeOptionMethodsMcFirstYearMaximumFeeManagementOptions,
      manageIncomeOptionMethodsAcBatchTypeCodeOptions,
      manageIncomeOptionMethodsAcIncludeAnnualChargeInInterestCalculationsOptions,
      manageIncomeOptionMethodsAcPreventDelinquencyFromAnnualFeeBalanceOptions,
      manageIncomeOptionMethodsAcAnnualChargeOptionOptions,
      manageIncomeOptionMethodsAcFirstYearMaximumFeeManagementOptionOptions,
      manageIncomeOptionMethodsAcPrenotificationAmountChangeOptionsOptions,
      manageIncomeOptionMethodsAcAnnualChargeStatementDisplayControlOptions,
      manageIncomeOptionMethodsAcPreAnnualChargeNotificationsOptions,
      manageIncomeOptionMethodsAcRegulationZnotificationGenerationPeriodOptions,
      manageIncomeOptionMethodsAcRegulationZNotificationMediaMethodOptions,
      manageIncomeOptionMethodsAcPreNotificationMessageIdOptions,
      manageIncomeOptionMethodsAcPlasticsRequirementCodeOptions,
      manageIncomeOptionMethodsAcSpecialConditionsForExternalStatusOptions,
      manageIncomeOptionMethodsAcSpecialConditionsForExpiredAccountsOptions,
      manageIncomeOptionMethodsAcAnnualChargeIncludeTableOptions,
      manageIncomeOptionMethodsAcFinanceChargeSuppressionCodeOptions,
      manageIncomeOptionMethodsAcNoPrenotificationAssessmentOptionsOptions,
      manageIncomeOptionMethodsAcExistingAccountStartMonthOptionOptions,
      manageIncomeOptionMethodsAcNumberOfMonthsToFirstChargeOptions,
      manageIncomeOptionMethodsAcPostingCycleOptionsOptions,
      manageIncomeOptionMethodsAcFeeTimingOptionsOptions,
      manageIncomeOptionMethodsAcSpecifiedMonthForAnnualChargeOptions,
      manageIncomeOptionMethodsAcFeeWaiverMonthsOptions,
      manageIncomeOptionMethodsAcDebitActivityOptionOptions,
      manageIncomeOptionMethodsAcWaiverTotalDollarCalculationOptionOptions,
      manageIncomeOptionMethodsAcRateChangePeriodOptions,
      manageIncomeOptionMethodsAcFeeWaiverOptionOptions,
      manageIncomeOptionMethodsAcMinimumBalanceAndExternalStatusOptionOptions,
      manageIncomeOptionMethodsAcReversalStatementMessageWarningTextIDOptions
    };
  }
);

export const useSelectElementMetadataForManageIncomeOptionMethods = () => {
  return useSelector<
    RootState,
    {
      //Mc
      manageIncomeOptionMethodsMcJoiningFeeStatementDescriptionOptions: RefData[];
      manageIncomeOptionMethodsMcJoiningFeeBatchIdOptions: RefData[];
      manageIncomeOptionMethodsMcCardReplacementFeeBatchIdOptions: RefData[];
      manageIncomeOptionMethodsMcCardReplacementFeeStatementDescriptionOptions: RefData[];
      manageIncomeOptionMethodsMcFirstYearMaximumFeeManagementOptions: RefData[];
      //Ac - Statement and Notification Display
      manageIncomeOptionMethodsAcAnnualChargeStatementDisplayControlOptions: RefData[];
      manageIncomeOptionMethodsAcPreAnnualChargeNotificationsOptions: RefData[];
      manageIncomeOptionMethodsAcRegulationZnotificationGenerationPeriodOptions: RefData[];
      manageIncomeOptionMethodsAcRegulationZNotificationMediaMethodOptions: RefData[];
      manageIncomeOptionMethodsAcPreNotificationMessageIdOptions: RefData[];
      //Ac - Assessment Calculations
      manageIncomeOptionMethodsAcBatchTypeCodeOptions: RefData[];
      manageIncomeOptionMethodsAcIncludeAnnualChargeInInterestCalculationsOptions: RefData[];
      manageIncomeOptionMethodsAcPreventDelinquencyFromAnnualFeeBalanceOptions: RefData[];
      manageIncomeOptionMethodsAcAnnualChargeOptionOptions: RefData[];
      manageIncomeOptionMethodsAcFirstYearMaximumFeeManagementOptionOptions: RefData[];
      manageIncomeOptionMethodsAcPrenotificationAmountChangeOptionsOptions: RefData[];
      //AC - Account Eligibility
      manageIncomeOptionMethodsAcPlasticsRequirementCodeOptions: RefData[];
      manageIncomeOptionMethodsAcSpecialConditionsForExternalStatusOptions: RefData[];
      manageIncomeOptionMethodsAcSpecialConditionsForExpiredAccountsOptions: RefData[];
      manageIncomeOptionMethodsAcAnnualChargeIncludeTableOptions: RefData[];
      manageIncomeOptionMethodsAcFinanceChargeSuppressionCodeOptions: RefData[];
      manageIncomeOptionMethodsAcNoPrenotificationAssessmentOptionsOptions: RefData[];
      //AC - Assessment Timing
      manageIncomeOptionMethodsAcExistingAccountStartMonthOptionOptions: RefData[];
      manageIncomeOptionMethodsAcNumberOfMonthsToFirstChargeOptions: RefData[];
      manageIncomeOptionMethodsAcPostingCycleOptionsOptions: RefData[];
      manageIncomeOptionMethodsAcFeeTimingOptionsOptions: RefData[];
      //AC - Advanced Parameters - Assessment Timing
      manageIncomeOptionMethodsAcSpecifiedMonthForAnnualChargeOptions: RefData[];
      manageIncomeOptionMethodsAcFeeWaiverMonthsOptions: RefData[];
      //AC - Advanced Parameters - Assessment Calculations
      manageIncomeOptionMethodsAcDebitActivityOptionOptions: RefData[];
      manageIncomeOptionMethodsAcWaiverTotalDollarCalculationOptionOptions: RefData[];
      manageIncomeOptionMethodsAcRateChangePeriodOptions: RefData[];
      //AC - Advanced Parameters - Account Eligibility
      manageIncomeOptionMethodsAcFeeWaiverOptionOptions: RefData[];
      manageIncomeOptionMethodsAcMinimumBalanceAndExternalStatusOptionOptions: RefData[];
      //AC - Advanced Parameters -  Reversal Options
      manageIncomeOptionMethodsAcReversalStatementMessageWarningTextIDOptions: RefData[];
    }
  >(elementMetadataForManageIncomeOptionMethodsSelector);
};
