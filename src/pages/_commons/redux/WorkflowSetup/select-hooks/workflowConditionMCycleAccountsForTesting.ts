import { isEmpty } from 'lodash';
import { ParameterCode } from 'pages/WorkflowConditionMCycleAccountsForTesting/SelectTransactionsStep/type';
import { useSelector } from 'react-redux';
import { createSelector } from 'reselect';
import { findByName, mapNameAndTextToText } from './_helper';

const elementMetadataForConditionMCycleAccountsForTestingSelector =
  createSelector(
    (state: RootState) => state.workflowSetup!.elementMetadata.elements,
    data => {
      const elements = data.map(e => {
        return {
          name: e.name,
          options: e.options?.map(opt => ({
            ...opt,
            code: opt.value,
            text: !isEmpty(opt.description) ? opt.description : opt.value
          }))
        } as WorkflowElement;
      });
      const transaction253 =
        elements.find(findByName(ParameterCode.Transaction253))?.options || [];
      const transaction254 =
        elements.find(findByName(ParameterCode.Transaction254))?.options || [];
      const transaction255 =
        elements.find(findByName(ParameterCode.Transaction254))?.options || [];
      const transaction271 =
        elements.find(findByName(ParameterCode.Transaction271))?.options || [];
      const transactionCU956 =
        elements.find(findByName(ParameterCode.TransactionCU956))?.options ||
        [];
      const transactionNM102 =
        elements.find(findByName(ParameterCode.TransactionNM102))?.options ||
        [];
      const transactionNM146 =
        elements.find(findByName(ParameterCode.TransactionNM146))?.options ||
        [];
      const transactionNM150 =
        elements.find(findByName(ParameterCode.TransactionNM150))?.options ||
        [];

      const transactionMapping: Record<string, RefData[]> = {
        '253': transaction253,
        '254': transaction254,
        '255': transaction255,
        '271': transaction271,
        CU956: transactionCU956,
        NM102: transactionNM102,
        NM146: transactionNM146,
        NM150: transactionNM150
      };
      const transactions =
        elements
          .find(findByName(ParameterCode.TransactionId))
          ?.options?.map(mapNameAndTextToText)
          ?.map(option => ({
            ...option,
            id: option.code,
            name: option.text,
            parameters: transactionMapping[option.code]
          })) || [];
      return { transactions };
    }
  );
export const useSelectElementsForConditionMCycleAccountsForTesting = () => {
  return useSelector<RootState, { transactions: RefData[] }>(
    elementMetadataForConditionMCycleAccountsForTestingSelector
  );
};
