import { createSelector } from '@reduxjs/toolkit';
import { useSelector } from 'react-redux';

const configureParametersOverride = createSelector(
  (state: RootState) => state.workflowSetup!.elementMetadata.elements,
  data => {
    const elements = data.map(e => {
      return {
        name: e.name,
        options: e.options?.map(opt => ({
          code: opt.value,
          text: opt.description
        }))
      } as {
        name: string;
        options: RefData[];
      };
    });

    const override =
      elements.find(e => e.name === 'temporary.credit.limit.override')
        ?.options || [];

    return { override };
  }
);

export const useSelectConfigureParametersOverride = () => {
  return useSelector<RootState, { override: RefData[] }>(
    configureParametersOverride
  );
};
