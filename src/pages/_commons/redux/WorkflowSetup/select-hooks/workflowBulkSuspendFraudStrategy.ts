import { createSelector } from '@reduxjs/toolkit';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import { useSelector } from 'react-redux';

const elementMetadataBulkFraudSelector = createSelector(
  (state: RootState) => state.workflowSetup!.elementMetadata.elements,
  data => {
    const elements = data.map(e => {
      return {
        name: e.name,
        options:
          e.name !== 'transaction'
            ? e.options?.map(opt => ({
                code: opt.value,
                text: opt.value
                  ? `${opt.value}${
                      opt.description ? ` - ${opt.description}` : ''
                    }`
                  : `${opt.description}`
              }))
            : e.options
                ?.filter(
                  optFilter =>
                    optFilter.workFlow === 'bsfs' || !optFilter.workFlow
                )
                .map(opt => ({
                  value: opt.value,
                  description: opt.description,
                  type: 'Required'
                }))
      } as {
        name: string;
        options: RefData[];
      };
    });
    const transaction =
      elements.find(
        e => e.name === MethodFieldParameterEnum.BulkFraudTransaction
      )?.options || [];
    return {
      transaction
    };
  }
);

export const useSelectElementMetadataBulkFraud = () => {
  return useSelector<
    RootState,
    {
      transaction: RefData[];
    }
  >(elementMetadataBulkFraudSelector);
};
