import { renderHook } from '@testing-library/react-hooks';
import { WorkflowSetupRootState } from 'app/types';
import * as reactRedux from 'react-redux';
import { useSelectElementMetadataForForceEmbossReplacementCards } from '.';

describe('redux-store > workflow-setup > select-hooks ', () => {
  let selectorMock: jest.SpyInstance;
  const mockSelectorWorkflowSetup = (
    workflowSetup:
      | Pick<WorkflowSetupRootState, 'workflowFormSteps'>
      | Pick<WorkflowSetupRootState, 'workflowSetup'>
      | Pick<WorkflowSetupRootState, 'workflowMethods'>
      | Pick<WorkflowSetupRootState, 'workflowInstance'>
      | Pick<WorkflowSetupRootState, 'elementMetadata'>
      | Pick<WorkflowSetupRootState, 'workflowChangeSets'>
      | Pick<WorkflowSetupRootState, 'createNewChange'>
      | Pick<WorkflowSetupRootState, 'parameterData'>
  ) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector => selector({ workflowSetup }));
  };

  afterEach(() => {
    selectorMock.mockClear();
  });

  it('test on selector useSelectElementMetadataForForceEmbossReplacementCards', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: {
        elements: [
          {
            name: 'name',
            options: [
              { value: 'value', name: 'name', workFlow: 'workFlow' },
              { value: '', name: 'name', workFlow: 'workFlow' }
            ]
          },
          {
            name: 'transaction.id',
            options: [
              { value: 'value', name: 'name', workFlow: 'emboss' },
              { value: '', name: 'name', workFlow: 'emboss' }
            ]
          }
        ] as any
      }
    });

    const { result } = renderHook(() =>
      useSelectElementMetadataForForceEmbossReplacementCards()
    );

    expect(result.current.configuration.length).toEqual(2);
    expect(result.current.transaction).toEqual([]);
    expect(result.current.PID194Code).toEqual([]);
    expect(result.current.PID200Code).toEqual([]);
  });

  it('test on selector useSelectElementMetadataForForceEmbossReplacementCards with options > workflow equal emboss', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: {
        elements: [
          {
            name: 'name',
            options: [
              { value: 'value', name: 'name', workFlow: 'workFlow' },
              { value: '', name: 'name', workFlow: 'workFlow' }
            ]
          }
        ] as any
      }
    });

    const { result } = renderHook(() =>
      useSelectElementMetadataForForceEmbossReplacementCards()
    );

    expect(result.current.configuration).toEqual([]);
  });

  it('test on selector useSelectElementMetadataForForceEmbossReplacementCards with options > workflow difference emboss', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: {
        elements: [
          {
            name: 'action',
            options: [
              { value: 'value', name: 'name', workFlow: 'workFlow' },
              { value: '', name: 'name', workFlow: 'workFlow' }
            ]
          }
        ] as any
      }
    });

    const { result } = renderHook(() =>
      useSelectElementMetadataForForceEmbossReplacementCards()
    );
    expect(result.current.configuration.length).toEqual(0);
  });

  it('test on selector useSelectElementMetadataForForceEmbossReplacementCards with options equal undefined', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: {
        elements: [
          {
            name: 'action',
            options: undefined
          }
        ] as any
      }
    });

    const { result } = renderHook(() =>
      useSelectElementMetadataForForceEmbossReplacementCards()
    );

    expect(result.current.configuration).toEqual([]);
  });
});
