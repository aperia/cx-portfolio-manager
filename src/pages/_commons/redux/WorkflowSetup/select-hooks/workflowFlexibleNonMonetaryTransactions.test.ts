import { renderHook } from '@testing-library/react-hooks';
import { WorkflowSetupRootState } from 'app/types';
import * as reactRedux from 'react-redux';
import { useSelectElementsForNonMonetaryTransactions } from '.';

describe('redux-store > workflow-setup > select-hooks ', () => {
  let selectorMock: jest.SpyInstance;
  const mockSelectorWorkflowSetup = (
    workflowSetup:
      | Pick<WorkflowSetupRootState, 'workflowFormSteps'>
      | Pick<WorkflowSetupRootState, 'workflowSetup'>
      | Pick<WorkflowSetupRootState, 'workflowMethods'>
      | Pick<WorkflowSetupRootState, 'workflowInstance'>
      | Pick<WorkflowSetupRootState, 'elementMetadata'>
      | Pick<WorkflowSetupRootState, 'workflowChangeSets'>
      | Pick<WorkflowSetupRootState, 'createNewChange'>
      | Pick<WorkflowSetupRootState, 'parameterData'>
  ) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector => selector({ workflowSetup }));
  };

  afterEach(() => {
    selectorMock.mockClear();
  });

  it('test on selector useSelectElementsForNonMonetaryTransactions', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: {
        elements: [
          {
            name: 'transaction.id',
            options: [
              { value: 'CU956', description: 'description' },
              { name: 'CU*956' }
            ]
          }
        ]
      }
    } as any);

    const { rerender, result } = renderHook(() =>
      useSelectElementsForNonMonetaryTransactions()
    );

    mockSelectorWorkflowSetup({
      elementMetadata: { elements: [] }
    } as any);
    rerender();
    expect(result.current).toEqual({ transactions: [] });
  });
});
