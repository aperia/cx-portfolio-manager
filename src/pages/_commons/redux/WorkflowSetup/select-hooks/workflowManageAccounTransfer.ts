import { createSelector } from '@reduxjs/toolkit';
import { useSelector } from 'react-redux';
import { findByName, mapValueAndDescToRefItem } from './_helper';

const elementMetadataForManageAccounTransferSelector = createSelector(
  (state: RootState) => state.workflowSetup!.elementMetadata.elements,
  data => {
    const elements = data.map(e => {
      const options = e.options?.map(mapValueAndDescToRefItem);

      return {
        name: e.name,
        options
      } as {
        name: string;
        options: any[];
      };
    });

    const transactions =
      elements.find(findByName('transaction'))?.options || [];

    return { transactions };
  }
);

export const useSelectElementMetadataForManageAccounTransfer = () => {
  return useSelector<RootState, { transactions: RefData[] }>(
    elementMetadataForManageAccounTransferSelector
  );
};
