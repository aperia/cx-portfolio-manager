import { renderHook } from '@testing-library/react-hooks';
import { WorkflowMetadataAll } from 'app/fixtures/workflow-metadata';
import { WorkflowSetupRootState } from 'app/types';
import * as reactRedux from 'react-redux';
import {
  useSelectDecisionData,
  useSelectDecisionListData,
  useSelectElementMetadataManageAcountLevel,
  useSelectStrategiesListData,
  useSelectTableListData
} from '.';

describe('redux-store > workflow-setup > select-hooks ', () => {
  let selectorMock: jest.SpyInstance;
  const mockSelectorWorkflowSetup = (
    workflowSetup:
      | Pick<WorkflowSetupRootState, 'workflowFormSteps'>
      | Pick<WorkflowSetupRootState, 'workflowSetup'>
      | Pick<WorkflowSetupRootState, 'workflowMethods'>
      | Pick<WorkflowSetupRootState, 'workflowInstance'>
      | Pick<WorkflowSetupRootState, 'elementMetadata'>
      | Pick<WorkflowSetupRootState, 'workflowChangeSets'>
      | Pick<WorkflowSetupRootState, 'createNewChange'>
      | Pick<WorkflowSetupRootState, 'parameterData'>
  ) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector => selector({ workflowSetup }));
  };

  afterEach(() => {
    selectorMock.mockClear();
  });

  it('test on selector useSelectElementMetadataManageAcountLevel ', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: { elements: WorkflowMetadataAll }
    } as any);

    const { rerender, result } = renderHook(() =>
      useSelectElementMetadataManageAcountLevel()
    );

    mockSelectorWorkflowSetup({
      elementMetadata: { elements: [] }
    } as any);
    rerender();
    expect(result.current).toEqual({
      allocationBeforeAfterCycle: [],
      allocationFlag: [],
      allocationInterval: [],
      alpConfig: [],
      changeInTermsMethodFlag: [],
      searchCode: []
    });
  });

  it('test on selector useSelectStrategiesListData ', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: { elements: WorkflowMetadataAll }
    } as any);

    const { rerender, result } = renderHook(() =>
      useSelectStrategiesListData()
    );

    mockSelectorWorkflowSetup({
      elementMetadata: { elements: [] }
    } as any);
    rerender();
    expect(result.current).toEqual(undefined);
  });

  it('test on selector useSelectTableListData ', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: { elements: WorkflowMetadataAll }
    } as any);

    const { rerender, result } = renderHook(() => useSelectTableListData());

    mockSelectorWorkflowSetup({
      elementMetadata: { elements: [] }
    } as any);
    rerender();
    expect(result.current).toEqual(undefined);
  });

  it('test on selector useSelectDecisionListData', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: { elements: WorkflowMetadataAll }
    } as any);

    const { rerender, result } = renderHook(() => useSelectDecisionListData());

    mockSelectorWorkflowSetup({
      elementMetadata: { elements: [] }
    } as any);
    rerender();
    expect(result.current).toEqual(undefined);
  });
  it('test on selector useSelectDecisionData', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: { elements: WorkflowMetadataAll }
    } as any);

    const { rerender, result } = renderHook(() => useSelectDecisionData());

    mockSelectorWorkflowSetup({
      elementMetadata: { elements: [] }
    } as any);
    rerender();
    expect(result.current).toEqual(undefined);
  });
});
