import { renderHook } from '@testing-library/react-hooks';
import { WorkflowSetupRootState } from 'app/types';
import * as reactRedux from 'react-redux';
import { useSelectElementMetadataForSPA } from '.';

describe('redux-store > workflow-setup > select-hooks ', () => {
  let selectorMock: jest.SpyInstance;
  const mockSelectorWorkflowSetup = (
    workflowSetup:
      | Pick<WorkflowSetupRootState, 'workflowFormSteps'>
      | Pick<WorkflowSetupRootState, 'workflowSetup'>
      | Pick<WorkflowSetupRootState, 'workflowMethods'>
      | Pick<WorkflowSetupRootState, 'workflowInstance'>
      | Pick<WorkflowSetupRootState, 'elementMetadata'>
      | Pick<WorkflowSetupRootState, 'workflowChangeSets'>
      | Pick<WorkflowSetupRootState, 'createNewChange'>
      | Pick<WorkflowSetupRootState, 'parameterData'>
  ) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector => selector({ workflowSetup }));
  };

  afterEach(() => {
    selectorMock.mockClear();
  });

  it('test on selector useSelectElementMetadataForSPA', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: {
        elements: [
          {
            name: 'system.principal.agents',
            options: [
              { value: '00' },
              { value: '00-11' },
              { value: '00-11-22' },
              { value: '00-11-22' },
              { value: '33' }
            ]
          },
          { name: 'name', options: [{}] }
        ] as any
      }
    });

    const { result, rerender } = renderHook(() =>
      useSelectElementMetadataForSPA()
    );

    expect(result.current.prin).toEqual({
      '00': [undefined, '11'],
      '33': [undefined]
    });
    expect(result.current.spas.length).toEqual(5);
    expect(result.current.agent).not.toBeUndefined();

    mockSelectorWorkflowSetup({
      elementMetadata: { elements: [] }
    });
    rerender();
    expect(result.current.spas.length).toEqual(0);
  });
});
