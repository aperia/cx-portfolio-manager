import { renderHook } from '@testing-library/react-hooks';
import { WorkflowSetupRootState } from 'app/types';
import * as reactRedux from 'react-redux';
import { useSelectWorkflowUpdateIndexRateSelector } from './workflowUpdateIndexRate';

describe('redux-store > workflow-setup > select-hooks ', () => {
  let selectorMock: jest.SpyInstance;
  const mockSelectorWorkflowSetup = (
    workflowSetup:
      | Pick<WorkflowSetupRootState, 'workflowFormSteps'>
      | Pick<WorkflowSetupRootState, 'workflowSetup'>
      | Pick<WorkflowSetupRootState, 'workflowMethods'>
      | Pick<WorkflowSetupRootState, 'workflowInstance'>
      | Pick<WorkflowSetupRootState, 'elementMetadata'>
      | Pick<WorkflowSetupRootState, 'workflowChangeSets'>
      | Pick<WorkflowSetupRootState, 'createNewChange'>
      | Pick<WorkflowSetupRootState, 'parameterData'>
  ) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector => selector({ workflowSetup }));
  };

  afterEach(() => {
    selectorMock.mockClear();
  });

  it('test on selector useSelectWorkflowUpdateIndexRateSelector ', () => {
    mockSelectorWorkflowSetup({
      workflowMethods: {
        methods: [
          {
            id: '1',
            name: 'name',
            effectiveDate: '2020-03-22',
            comment: 'comment',
            relatedPricingStrategies: 1,
            numberOfVersions: 1,
            versions: [
              {
                id: '2',
                comment: 'comment',
                effectiveDate: '2020-03-22',
                status: 'done',
                parameters: ''
              }
            ],
            pricingStrategies: [
              {
                id: '3',
                name: 'name',
                description: 'description',
                effectiveDate: '2020-03-22',
                numberOfCardHolders: 1
              }
            ]
          }
        ],
        dataFilter: {},
        loading: false,
        error: false
      }
    } as any);

    const { rerender, result } = renderHook(() =>
      useSelectWorkflowUpdateIndexRateSelector()
    );

    mockSelectorWorkflowSetup({
      workflowMethods: {
        methods: [
          {
            id: '1',
            name: 'name',
            effectiveDate: '2020-03-22',
            comment: 'comment',
            relatedPricingStrategies: 1,
            numberOfVersions: 1,
            versions: [
              {
                id: '2',
                comment: 'comment',
                effectiveDate: '2020-03-22',
                status: 'done',
                parameters: ''
              }
            ],
            pricingStrategies: [
              {
                id: '3',
                name: 'name',
                description: 'description',
                effectiveDate: '2020-03-22',
                numberOfCardHolders: 1
              }
            ]
          }
        ],
        dataFilter: {},
        loading: false,
        error: false
      }
    } as any);
    rerender();
    expect(result.current.loading).toEqual(false);
    expect(result.current.error).toEqual(false);
  });
  it('test on selector useSelectWorkflowUpdateIndexRateSelector with empty data', () => {
    mockSelectorWorkflowSetup({
      workflowMethods: {}
    } as any);

    const { rerender, result } = renderHook(() =>
      useSelectWorkflowUpdateIndexRateSelector()
    );

    mockSelectorWorkflowSetup({
      workflowMethods: {}
    } as any);
    rerender();
    expect(result.current.loading).toEqual(false);
    expect(result.current.error).toEqual(false);
  });
});
