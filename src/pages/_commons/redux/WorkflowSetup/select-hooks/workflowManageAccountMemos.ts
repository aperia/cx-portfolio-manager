import { createSelector } from '@reduxjs/toolkit';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import orderBy from 'lodash.orderby';
import { useSelector } from 'react-redux';

const elementMetadataManageAcountMemosSelector = createSelector(
  (state: RootState) => state.workflowSetup!.elementMetadata.elements,
  data => {
    const elements = data.map(e => {
      if (e.name === MethodFieldParameterEnum.MemosSPAs) {
        return {
          name: e.name,
          options: e.options?.map(otp => ({
            value: otp?.value,
            description: !!otp?.description ? otp?.description : otp?.value
          }))
        };
      }

      return {
        name: e.name,
        options: e.options?.map(otp => ({
          value: !!otp?.description ? otp?.description : otp?.value,
          description: !!otp?.description ? otp?.description : otp?.value
        }))
      };
    });
    const memosTypes =
      elements.find(e => e.name === MethodFieldParameterEnum.MemosTypes)
        ?.options || [];
    const memosActions =
      elements.find(e => e.name === MethodFieldParameterEnum.MemosActions)
        ?.options || [];
    const memosTypesFormCIS =
      elements.find(e => e.name === MethodFieldParameterEnum.MemosTypesFormCIS)
        ?.options || [];

    const memosTypesFormChronicle =
      elements.find(
        e => e.name === MethodFieldParameterEnum.MemosTypesFormChronicle
      )?.options || [];

    const memosOperator =
      elements.find(e => e.name === MethodFieldParameterEnum.MemosOperator)
        ?.options || [];
    const memosEnteredIdOperator =
      elements.find(
        e => e.name === MethodFieldParameterEnum.MemosEnteredIdOperator
      )?.options || [];
    const memosSysOperator =
      elements.find(e => e.name === MethodFieldParameterEnum.MemosSysOperator)
        ?.options || [];
    const memosPrinOperator =
      elements.find(e => e.name === MethodFieldParameterEnum.MemosPrinOperator)
        ?.options || [];
    const memosAgentOperator =
      elements.find(e => e.name === MethodFieldParameterEnum.MemosAgentOperator)
        ?.options || [];
    const memosDateOperator =
      elements.find(e => e.name === MethodFieldParameterEnum.MemosDateOperator)
        ?.options || [];
    const memosMemoTextOperator =
      elements.find(
        e => e.name === MethodFieldParameterEnum.MemosMemoTextOperator
      )?.options || [];
    const memosTypeOperator =
      elements.find(e => e.name === MethodFieldParameterEnum.MemosTypeOperator)
        ?.options || [];
    const memosTypeOperatorValue =
      elements.find(
        e => e.name === MethodFieldParameterEnum.MemosTypeOperatorValue
      )?.options || [];

    const spas =
      elements.find(e => e.name === MethodFieldParameterEnum.MemosSPAs)
        ?.options || [];

    let sys: string[] = [];
    let prin: string[] = [];
    let agent: string[] = [];

    spas.forEach(spa => {
      const sysItem = spa?.value.split('-')[0];
      const index = sys.indexOf(sysItem);
      if (index === -1) {
        sys = [...sys, sysItem];
      }
    });

    spas.forEach(spa => {
      const prinItem = spa?.value.split('-')[1];
      const index = prin.indexOf(prinItem);
      if (index === -1) {
        prin = [...prin, prinItem];
      }
    });

    spas.forEach(spa => {
      const agentItem = spa?.value.split('-')[2];
      const index = agent.indexOf(agentItem);
      if (index === -1) {
        agent = [...agent, agentItem];
      }
    });

    const memosSys = sys?.map(val => ({
      value: val,
      description: val
    }));
    const memosPrin = prin?.map(val => ({
      value: val,
      description: val
    }));
    const memosAgent = agent?.map(val => ({
      value: val,
      description: val
    }));

    const memosSysOrder = orderBy(memosSys, ['value'], ['asc']);
    const memosPrinOrder = orderBy(memosPrin, ['value'], ['asc']);
    const memosAgentOrder = orderBy(memosAgent, ['value'], ['asc']);
    const memosTypesFormCISOrder = orderBy(
      memosTypesFormCIS,
      ['value'],
      ['asc']
    ).filter(
      item =>
        item?.value !== 'Sys' &&
        item?.value !== 'Prin' &&
        item?.value !== 'Agent'
    );
    const memosTypesFormChronicleOrder = orderBy(
      memosTypesFormChronicle,
      ['value'],
      ['asc']
    ).filter(
      item =>
        item?.value !== 'Sys' &&
        item?.value !== 'Prin' &&
        item?.value !== 'Agent'
    );
    return {
      memosTypes,
      memosActions,
      memosTypesFormCIS: [
        { description: 'Sys', value: 'Sys' },
        { description: 'Prin', value: 'Prin' },
        { description: 'Agent', value: 'Agent' },
        ...memosTypesFormCISOrder
      ],
      memosTypesFormChronicle: [
        { description: 'Sys', value: 'Sys' },
        { description: 'Prin', value: 'Prin' },
        { description: 'Agent', value: 'Agent' },
        ...memosTypesFormChronicleOrder
      ],
      memosOperator,
      memosEnteredIdOperator,
      memosSysOperator,
      memosPrinOperator,
      memosAgentOperator,
      memosDateOperator,
      memosMemoTextOperator,
      memosTypeOperator,
      memosTypeOperatorValue,
      memosSys: memosSysOrder,
      memosPrin: memosPrinOrder,
      memosAgent: memosAgentOrder
    };
  }
);

export const useSelectElementMetadataManageAcountMemos = () => {
  return useSelector<
    RootState,
    {
      memosTypes: any[];
      memosActions: any[];
      memosTypesFormCIS: any[];
      memosTypesFormChronicle: any[];
      memosOperator: any[];
      memosEnteredIdOperator: any[];
      memosSysOperator: any[];
      memosPrinOperator: any[];
      memosAgentOperator: any[];
      memosDateOperator: any[];
      memosMemoTextOperator: any[];
      memosTypeOperator: any[];
      memosTypeOperatorValue: any[];
      memosSys: any[];
      memosPrin: any[];
      memosAgent: any[];
    }
  >(elementMetadataManageAcountMemosSelector);
};
