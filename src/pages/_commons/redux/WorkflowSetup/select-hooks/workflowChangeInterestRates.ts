import { createSelector } from '@reduxjs/toolkit';
import { WorkflowStrategyFlyoutProps } from 'app/components/WorkflowStrategyFlyout';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import { matchSearchValue } from 'app/helpers';
import _orderBy from 'lodash.orderby';
import { useSelector } from 'react-redux';
import { KeepTextDelayDesignPromotion } from '.';

const workflowMethodsChangeInterestRatesSelector = createSelector(
  (state: RootState) => state.workflowSetup!.workflowMethods,
  data => {
    const {
      methods: rawData,
      dataFilter = {},
      loading = false,
      error = false
    } = data;
    const { sortBy, orderBy, page, pageSize, searchValue = '' } = dataFilter;
    const _searchValue = searchValue.toLowerCase() || '';
    const _method = _orderBy(rawData, sortBy, orderBy).filter(w => {
      return matchSearchValue([w.name], _searchValue);
    });
    const total = _method.length;
    const methods = _method.slice((page! - 1) * pageSize!, page! * pageSize!);
    return { methods, total, loading, error };
  }
);

export const useSelectWorkflowMethodsChangeInterestRatesSelector = () => {
  return useSelector<
    RootState,
    {
      methods: IMethodModel[];
      loading: boolean;
      error: boolean;
      total: number;
    }
  >(workflowMethodsChangeInterestRatesSelector);
};

const strategyFlyoutChangeInterestRatesSelector = createSelector(
  (state: RootState) => state.workflowSetup!.workflowMethods.strategyFlyoutData,
  data => {
    if (!data) return null;
    const strategyList = _orderBy(data.strategyList, ['name'], ['asc']);
    return {
      ...data,
      strategyList
    };
  }
);
export const useSelectStrategyFlyoutChangeInterestRatesSelector = () => {
  return useSelector<
    RootState,
    Omit<WorkflowStrategyFlyoutProps, 'onClose'> | null
  >(strategyFlyoutChangeInterestRatesSelector);
};

const elementMetadataIDSelector = createSelector(
  (state: RootState) => state.workflowSetup!.elementMetadata.elements,
  data => {
    const elements = data.map(e => {
      return {
        name: e.name,
        options: KeepTextDelayDesignPromotion.includes(e.name)
          ? e.options?.map(opt => ({
              code: opt.value,
              text: `${opt.value}`
            }))
          : e.options?.map(opt => ({
              code: opt.value,
              text: opt.value
                ? `${opt.value}${
                    opt.description ? ` - ${opt.description}` : ''
                  }`
                : `${opt.description}`
            }))
      } as {
        name: string;
        options: RefData[];
      };
    });

    const defaultCashBaseInterest =
      elements.find(
        e => e.name === MethodFieldParameterEnum.DefaultCashBaseInterest
      )?.options || [];
    const defaultMerchandiseBaseInterest =
      elements.find(
        e => e.name === MethodFieldParameterEnum.DefaultMerchandiseBaseInterest
      )?.options || [];
    const defaultMaximumCashInterest =
      elements.find(
        e => e.name === MethodFieldParameterEnum.DefaultMaximumCashInterest
      )?.options || [];
    const defaultMaximumMerchandiseInterest =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DefaultMaximumMerchandiseInterest
      )?.options || [];
    const defaultMinimumCashInterest =
      elements.find(
        e => e.name === MethodFieldParameterEnum.DefaultMinimumCashInterest
      )?.options || [];
    const defaultMinimumMerchandiseInterest =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.DefaultMinimumMerchandiseInterest
      )?.options || [];
    const indexRateMethod =
      elements.find(e => e.name === MethodFieldParameterEnum.IndexRateMethod)
        ?.options || [];
    const defaultBaseInterestUsage =
      elements.find(
        e => e.name === MethodFieldParameterEnum.DefaultBaseInterestUsage
      )?.options || [];
    const defaultOverrideInterestLimit =
      elements.find(
        e => e.name === MethodFieldParameterEnum.DefaultOverrideInterestLimit
      )?.options || [];
    const defaultMinimumUsage =
      elements.find(
        e => e.name === MethodFieldParameterEnum.DefaultMinimumUsage
      )?.options || [];
    const defaultMaximumUsage =
      elements.find(
        e => e.name === MethodFieldParameterEnum.DefaultMaximumUsage
      )?.options || [];
    const defaultMaximumInterestUsage =
      elements.find(
        e => e.name === MethodFieldParameterEnum.DefaultMaximumInterestUsage
      )?.options || [];

    const incentivePricingUsageCode =
      elements.find(
        e => e.name === MethodFieldParameterEnum.IncentivePricingUsageCode
      )?.options || [];
    const delinquencyCyclesForTermination =
      elements.find(
        e => e.name === MethodFieldParameterEnum.DelinquencyCyclesForTermination
      )?.options || [];
    const incentiveBaseInterestUsageRate =
      elements.find(
        e => e.name === MethodFieldParameterEnum.IncentiveBaseInterestUsageRate
      )?.options || [];
    const minimumMaximumRateOverrideOptions =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.MinimumMaximumRateOverrideOptions
      )?.options || [];
    const incentivePricingOverrideOption =
      elements.find(
        e => e.name === MethodFieldParameterEnum.IncentivePricingOverrideOption
      )?.options || [];
    const terminateIncentivePricingOnPricingStrategyChange =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.TerminateIncentivePricingOnPricingStrategyChange
      )?.options || [];
    const methodOverrideTerminationOption =
      elements.find(
        e => e.name === MethodFieldParameterEnum.MethodOverrideTerminationOption
      )?.options || [];
    const endofIncentiveWarningNotificationTiming =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.EndofIncentiveWarningNotificationTiming
      )?.options || [];
    const endofIncentiveWarningNotificationStatementText =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.EndofIncentiveWarningNotificationStatementText
      )?.options || [];
    const incentivePricingNumberOfMonths =
      elements.find(
        e => e.name === MethodFieldParameterEnum.IncentivePricingNumberOfMonths
      )?.options || [];
    const messageForProtectedBalanceIncentivePricing =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.MessageForProtectedBalanceIncentivePricing
      )?.options || [];
    const interestRateRoundingCalculation =
      elements.find(
        e => e.name === MethodFieldParameterEnum.InterestRateRoundingCalculation
      )?.options || [];
    const dailyAPRRoundingCalculation =
      elements.find(
        e => e.name === MethodFieldParameterEnum.DailyAPRRoundingCalculation
      )?.options || [];
    const stopCalculatingInterestAfterNumberofDaysDelinquent =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.StopCalculatingInterestAfterNumberofDaysDelinquent
      )?.options || [];
    const includeAdditionalCreditBalanceInAverageDailyBalanceCalculation =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.IncludeAdditionalCreditBalanceInAverageDailyBalanceCalculation
      )?.options || [];
    const combineMerchandiseAndCashBalancesOnInterestCalculations =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.CombineMerchandiseAndCashBalancesOnInterestCalculations
      )?.options || [];
    const restartInterestCalculationsAfterClearingDelinquency =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.RestartInterestCalculationsAfterClearingDelinquency
      )?.options || [];
    const calculateCompoundInterest =
      elements.find(
        e => e.name === MethodFieldParameterEnum.CalculateCompoundInterest
      )?.options || [];
    const overridePromotionalRatesForPricingStrategies =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.OverridePromotionalRatesForPricingStrategies
      )?.options || [];
    const monthlyInterestCalculationOptions =
      elements.find(
        e =>
          e.name === MethodFieldParameterEnum.MonthlyInterestCalculationOptions
      )?.options || [];
    const dailyInterestRoundingOption =
      elements.find(
        e => e.name === MethodFieldParameterEnum.DailyInterestRoundingOption
      )?.options || [];
    const dailyInterestAmountTruncationOption =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.DailyInterestAmountTruncationOption
      )?.options || [];
    const overrideInterestRatesForProtectedBalance =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.OverrideInterestRatesForProtectedBalance
      )?.options || [];
    const indexRateMethodName =
      elements.find(
        e => e.name === MethodFieldParameterEnum.IndexRateMethodName
      )?.options || [];
    const minimumDaysForCycleCodeChange =
      elements.find(
        e => e.name === MethodFieldParameterEnum.MinimumDaysForCycleCodeChange
      )?.options || [];
    const roundNumberOfDaysInCycleForInterestCalculations =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.RoundNumberOfDaysInCycleForInterestCalculations
      )?.options || [];
    const cycleCodeChangeOptions =
      elements.find(
        e => e.name === MethodFieldParameterEnum.CycleCodeChangeOptions
      )?.options || [];
    const cyclePeriodChangeCISMemoOption =
      elements.find(
        e => e.name === MethodFieldParameterEnum.CyclePeriodChangeCISMemoOption
      )?.options || [];
    const interestMethodOldCash =
      elements.find(
        e => e.name === MethodFieldParameterEnum.InterestMethodOldCash
      )?.options || [];
    const interestMethodCycleToDateCash =
      elements.find(
        e => e.name === MethodFieldParameterEnum.InterestMethodCycleToDateCash
      )?.options || [];
    const interestMethodTwoCycleOldCash =
      elements.find(
        e => e.name === MethodFieldParameterEnum.InterestMethodTwoCycleOldCash
      )?.options || [];
    const interestMethodOneCycleOldMerchandise =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.InterestMethodOneCycleOldMerchandise
      )?.options || [];
    const interestMethodCycleToDateMerchandise =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.InterestMethodCycleToDateMerchandise
      )?.options || [];
    const additionalInterestOnMerchandise1CyclesDelinquent =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.AdditionalInterestOnMerchandise1CyclesDelinquent
      )?.options || [];
    const additionalInterestOnMerchandise2CyclesDelinquent =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.AdditionalInterestOnMerchandise2CyclesDelinquent
      )?.options || [];
    const additionalInterestOnCash2CyclesDelinquent =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.AdditionalInterestOnCash2CyclesDelinquent
      )?.options || [];
    const additionalInterestOnMerchandise3CyclesDelinquent =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.AdditionalInterestOnMerchandise3CyclesDelinquent
      )?.options || [];
    const additionalInterestOnCash3CyclesDelinquent =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.AdditionalInterestOnCash3CyclesDelinquent
      )?.options || [];
    const automatedMemosForAdditionalInterest =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.AutomatedMemosForAdditionalInterest
      )?.options || [];
    const useExternalStatusesForAdditionalInterest =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.UseExternalStatusesForAdditionalInterest
      )?.options || [];
    const externalStatus1ForAdditionalInterest =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ExternalStatus1ForAdditionalInterest
      )?.options || [];
    const externalStatus2ForAdditionalInterest =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ExternalStatus2ForAdditionalInterest
      )?.options || [];
    const externalStatus3ForAdditionalInterest =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ExternalStatus3ForAdditionalInterest
      )?.options || [];
    const externalStatus4ForAdditionalInterest =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ExternalStatus4ForAdditionalInterest
      )?.options || [];
    const externalStatus5ForAdditionalInterest =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.ExternalStatus5ForAdditionalInterest
      )?.options || [];
    const penaltyPricingStatusReasonCodeTable =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.PenaltyPricingStatusReasonCodeTable
      )?.options || [];
    const penaltyPricingTimingOptions =
      elements.find(
        e => e.name === MethodFieldParameterEnum.PenaltyPricingTimingOptions
      )?.options || [];
    const penaltyPricingStatementMessage1CyclesOldBalance =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.PenaltyPricingStatementMessage1CyclesOldBalance
      )?.options || [];
    const penaltyPricingStatementMessage2CyclesOldBalance =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.PenaltyPricingStatementMessage2CyclesOldBalance
      )?.options || [];
    const penaltyPricingStatementMessage3CyclesOldBalance =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.PenaltyPricingStatementMessage3CyclesOldBalance
      )?.options || [];
    const cureDelinquencyFor1CycleDelinquentBalances =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.CureDelinquencyFor1CycleDelinquentBalances
      )?.options || [];
    const cureDelinquencyFor2CycleDelinquentBalances =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.CureDelinquencyFor2CycleDelinquentBalances
      )?.options || [];
    const cureDelinquencyFor3CycleDelinquentBalances =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.CureDelinquencyFor3CycleDelinquentBalances
      )?.options || [];
    const penaltyPricingOnPromotions =
      elements.find(
        e => e.name === MethodFieldParameterEnum.PenaltyPricingOnPromotions
      )?.options || [];
    const citMethodForPenaltyPricing =
      elements.find(
        e => e.name === MethodFieldParameterEnum.CITMethodForPenaltyPricing
      )?.options || [];
    const includeZeroBalanceDaysInADBCalculation =
      elements.find(
        e =>
          e.name ===
          MethodFieldParameterEnum.IncludeZeroBalanceDaysInADBCalculation
      )?.options || [];

    return {
      defaultCashBaseInterest,
      defaultMerchandiseBaseInterest,
      defaultMaximumCashInterest,
      defaultMaximumMerchandiseInterest,
      defaultMinimumCashInterest,
      defaultMinimumMerchandiseInterest,
      indexRateMethod,
      defaultBaseInterestUsage,
      defaultOverrideInterestLimit,
      defaultMinimumUsage,
      defaultMaximumUsage,
      defaultMaximumInterestUsage,
      incentivePricingUsageCode,
      incentiveBaseInterestUsageRate,
      delinquencyCyclesForTermination,
      minimumMaximumRateOverrideOptions,
      incentivePricingOverrideOption,
      indexRateMethodName,
      terminateIncentivePricingOnPricingStrategyChange,
      methodOverrideTerminationOption,
      endofIncentiveWarningNotificationTiming,
      endofIncentiveWarningNotificationStatementText,
      incentivePricingNumberOfMonths,
      messageForProtectedBalanceIncentivePricing,
      interestRateRoundingCalculation,
      dailyAPRRoundingCalculation,
      stopCalculatingInterestAfterNumberofDaysDelinquent,
      includeAdditionalCreditBalanceInAverageDailyBalanceCalculation,
      combineMerchandiseAndCashBalancesOnInterestCalculations,
      restartInterestCalculationsAfterClearingDelinquency,
      calculateCompoundInterest,
      overridePromotionalRatesForPricingStrategies,
      monthlyInterestCalculationOptions,
      dailyInterestRoundingOption,
      dailyInterestAmountTruncationOption,
      overrideInterestRatesForProtectedBalance,
      minimumDaysForCycleCodeChange,
      roundNumberOfDaysInCycleForInterestCalculations,
      cycleCodeChangeOptions,
      cyclePeriodChangeCISMemoOption,
      interestMethodOldCash,
      interestMethodCycleToDateCash,
      interestMethodTwoCycleOldCash,
      interestMethodOneCycleOldMerchandise,
      interestMethodCycleToDateMerchandise,
      additionalInterestOnMerchandise1CyclesDelinquent,
      additionalInterestOnMerchandise2CyclesDelinquent,
      additionalInterestOnCash2CyclesDelinquent,
      additionalInterestOnMerchandise3CyclesDelinquent,
      additionalInterestOnCash3CyclesDelinquent,
      automatedMemosForAdditionalInterest,
      useExternalStatusesForAdditionalInterest,
      externalStatus1ForAdditionalInterest,
      externalStatus2ForAdditionalInterest,
      externalStatus3ForAdditionalInterest,
      externalStatus4ForAdditionalInterest,
      externalStatus5ForAdditionalInterest,
      penaltyPricingStatusReasonCodeTable,
      penaltyPricingTimingOptions,
      penaltyPricingStatementMessage1CyclesOldBalance,
      penaltyPricingStatementMessage2CyclesOldBalance,
      penaltyPricingStatementMessage3CyclesOldBalance,
      cureDelinquencyFor1CycleDelinquentBalances,
      cureDelinquencyFor2CycleDelinquentBalances,
      cureDelinquencyFor3CycleDelinquentBalances,
      penaltyPricingOnPromotions,
      citMethodForPenaltyPricing,
      includeZeroBalanceDaysInADBCalculation
    };
  }
);

export const useSelectElementMetadataForChangeInterest = () => {
  return useSelector<
    RootState,
    {
      defaultCashBaseInterest: RefData[];
      defaultMerchandiseBaseInterest: RefData[];
      defaultMaximumCashInterest: RefData[];
      defaultMaximumMerchandiseInterest: RefData[];
      defaultMinimumCashInterest: RefData[];
      defaultMinimumMerchandiseInterest: RefData[];
      indexRateMethod: RefData[];
      defaultBaseInterestUsage: RefData[];
      defaultOverrideInterestLimit: RefData[];
      defaultMinimumUsage: RefData[];
      defaultMaximumUsage: RefData[];
      defaultMaximumInterestUsage: RefData[];
      incentivePricingUsageCode: RefData[];
      incentiveBaseInterestUsageRate: RefData[];
      delinquencyCyclesForTermination: RefData[];
      minimumMaximumRateOverrideOptions: RefData[];
      incentivePricingOverrideOption: RefData[];
      terminateIncentivePricingOnPricingStrategyChange: RefData[];
      methodOverrideTerminationOption: RefData[];
      endofIncentiveWarningNotificationTiming: RefData[];
      endofIncentiveWarningNotificationStatementText: RefData[];
      incentivePricingNumberOfMonths: RefData[];
      indexRateMethodName: RefData[];
      messageForProtectedBalanceIncentivePricing: RefData[];
      interestRateRoundingCalculation: RefData[];
      dailyAPRRoundingCalculation: RefData[];
      stopCalculatingInterestAfterNumberofDaysDelinquent: RefData[];
      includeAdditionalCreditBalanceInAverageDailyBalanceCalculation: RefData[];
      combineMerchandiseAndCashBalancesOnInterestCalculations: RefData[];
      restartInterestCalculationsAfterClearingDelinquency: RefData[];
      calculateCompoundInterest: RefData[];
      overridePromotionalRatesForPricingStrategies: RefData[];
      monthlyInterestCalculationOptions: RefData[];
      dailyInterestRoundingOption: RefData[];
      dailyInterestAmountTruncationOption: RefData[];
      overrideInterestRatesForProtectedBalance: RefData[];
      minimumDaysForCycleCodeChange: RefData[];
      roundNumberOfDaysInCycleForInterestCalculations: RefData[];
      cycleCodeChangeOptions: RefData[];
      cyclePeriodChangeCISMemoOption: RefData[];
      interestMethodOldCash: RefData[];
      interestMethodCycleToDateCash: RefData[];
      interestMethodTwoCycleOldCash: RefData[];
      interestMethodOneCycleOldMerchandise: RefData[];
      interestMethodCycleToDateMerchandise: RefData[];
      additionalInterestOnMerchandise1CyclesDelinquent: RefData[];
      additionalInterestOnMerchandise2CyclesDelinquent: RefData[];
      additionalInterestOnCash2CyclesDelinquent: RefData[];
      additionalInterestOnMerchandise3CyclesDelinquent: RefData[];
      additionalInterestOnCash3CyclesDelinquent: RefData[];
      automatedMemosForAdditionalInterest: RefData[];
      useExternalStatusesForAdditionalInterest: RefData[];
      externalStatus1ForAdditionalInterest: RefData[];
      externalStatus2ForAdditionalInterest: RefData[];
      externalStatus3ForAdditionalInterest: RefData[];
      externalStatus4ForAdditionalInterest: RefData[];
      externalStatus5ForAdditionalInterest: RefData[];
      penaltyPricingStatusReasonCodeTable: RefData[];
      penaltyPricingTimingOptions: RefData[];
      penaltyPricingStatementMessage1CyclesOldBalance: RefData[];
      penaltyPricingStatementMessage2CyclesOldBalance: RefData[];
      penaltyPricingStatementMessage3CyclesOldBalance: RefData[];
      cureDelinquencyFor1CycleDelinquentBalances: RefData[];
      cureDelinquencyFor2CycleDelinquentBalances: RefData[];
      cureDelinquencyFor3CycleDelinquentBalances: RefData[];
      penaltyPricingOnPromotions: RefData[];
      citMethodForPenaltyPricing: RefData[];
      includeZeroBalanceDaysInADBCalculation: RefData[];
    }
  >(elementMetadataIDSelector);
};
