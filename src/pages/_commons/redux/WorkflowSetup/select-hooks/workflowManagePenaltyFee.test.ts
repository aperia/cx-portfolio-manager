import { renderHook } from '@testing-library/react-hooks';
import { WorkflowMetadataList } from 'app/fixtures/workflow-metadata';
import {
  GetMethodListData,
  GetMethodListDataMapping
} from 'app/fixtures/workflow-method-list';
import { mappingDataFromArray } from 'app/helpers';
import { WorkflowSetupRootState } from 'app/types';
import {
  useSelectElementMetadataForLC,
  useSelectElementMetadataForRC,
  useSelectExpandingMethod,
  useSelectStrategyFlyoutSelector,
  useSelectWorkflowMethodsFilterSelector,
  useSelectWorkflowMethodsSelector
} from 'pages/_commons/redux/WorkflowSetup';
import * as reactRedux from 'react-redux';
import { defaultWorkflowMethodsFilter } from '../reducers';
import { useSelectWorkflowMethodsRawData } from './workflowManagePenaltyFee';

describe('redux-store > workflow-setup > select-hooks ', () => {
  let selectorMock: jest.SpyInstance;
  const mockSelectorWorkflowSetup = (
    workflowSetup:
      | Pick<WorkflowSetupRootState, 'workflowFormSteps'>
      | Pick<WorkflowSetupRootState, 'workflowSetup'>
      | Pick<WorkflowSetupRootState, 'workflowMethods'>
      | Pick<WorkflowSetupRootState, 'workflowInstance'>
      | Pick<WorkflowSetupRootState, 'elementMetadata'>
      | Pick<WorkflowSetupRootState, 'workflowChangeSets'>
      | Pick<WorkflowSetupRootState, 'createNewChange'>
      | Pick<WorkflowSetupRootState, 'parameterData'>
  ) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector => selector({ workflowSetup }));
  };

  afterEach(() => {
    selectorMock.mockClear();
  });

  it('test on selector useSelectWorkflowMethodsSelector', () => {
    const mockMethodList = mappingDataFromArray(
      GetMethodListData,
      GetMethodListDataMapping!
    );

    mockSelectorWorkflowSetup({
      workflowMethods: {
        loading: undefined,
        error: undefined,
        methods: mockMethodList,
        dataFilter: defaultWorkflowMethodsFilter
      }
    });

    const { result } = renderHook(() => useSelectWorkflowMethodsSelector());

    expect(result.current.methods.length).toEqual(2);
  });

  it('test on selector useSelectWorkflowMethodsSelector > empty data filter', () => {
    const mockMethodList = mappingDataFromArray(
      GetMethodListData,
      GetMethodListDataMapping!
    );

    mockSelectorWorkflowSetup({
      workflowMethods: {
        loading: undefined,
        error: undefined,
        methods: mockMethodList,
        dataFilter: undefined
      }
    });

    const { result } = renderHook(() => useSelectWorkflowMethodsSelector());

    expect(result.current.methods.length).toEqual(0);
  });

  it('test on selector useSelectStrategyFlyoutSelector > have strategy data', () => {
    mockSelectorWorkflowSetup({
      workflowMethods: {
        loading: undefined,
        error: undefined,
        methods: [],
        strategyFlyoutData: {
          id: 'mock-id',
          isNewVersion: true,
          show: true,
          methodName: 'mock-name',
          strategyList: []
        },
        dataFilter: undefined
      }
    });

    const { result } = renderHook(() => useSelectStrategyFlyoutSelector());

    expect(result.current).toEqual({
      id: 'mock-id',
      isNewVersion: true,
      show: true,
      methodName: 'mock-name',
      strategyList: []
    });
  });

  it('test on selector useSelectStrategyFlyoutSelector > no data', () => {
    mockSelectorWorkflowSetup({
      workflowMethods: {
        loading: undefined,
        error: undefined,
        methods: [],
        dataFilter: undefined
      }
    });

    const { result } = renderHook(() => useSelectStrategyFlyoutSelector());

    expect(result.current).toBeNull();
  });

  it('test on selector useSelectWorkflowMethodsFilterSelector', () => {
    mockSelectorWorkflowSetup({
      workflowMethods: {
        loading: undefined,
        error: undefined,
        methods: [],
        dataFilter: defaultWorkflowMethodsFilter
      }
    });

    const { result } = renderHook(() =>
      useSelectWorkflowMethodsFilterSelector()
    );

    expect(result.current).toEqual(defaultWorkflowMethodsFilter);

    selectorMock.mockClear();

    mockSelectorWorkflowSetup({
      workflowMethods: {
        loading: undefined,
        error: undefined,
        methods: [],
        dataFilter: undefined
      }
    });

    const { result: resultEmpty } = renderHook(() =>
      useSelectWorkflowMethodsFilterSelector()
    );

    expect(resultEmpty.current).toEqual({});
  });

  it('test on selector useSelectExpandingMethod', () => {
    mockSelectorWorkflowSetup({
      workflowMethods: {
        loading: undefined,
        error: undefined,
        methods: [],
        dataFilter: undefined,
        expandingMethod: 'mock-expanding'
      }
    });

    const { result } = renderHook(() => useSelectExpandingMethod());

    expect(result.current).toEqual('mock-expanding');
  });

  it('test on selector useSelectElementMetadataForRC', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: {
        loading: true,
        error: false,
        elements: [
          {
            id: '1',
            name: 'minimum.pay.due.option',
            options: [
              {
                value: '0',
                description: 'Do not use reasonable amount fee processing.'
              },
              {
                value: '1',
                description: 'Use the current MPD amount.'
              },
              {
                value: '2',
                description:
                  'Use the current non-delinquent portion of the MPD amount.'
              },
              {
                value: '3',
                description: 'Use the historical last statement MPD amount.'
              },
              {
                description:
                  'Use the historical non-delinquent last statement MPD amount.'
              },
              {
                value: '5',
                description:
                  'Use the lesser of the current or the historical last statement MPD amount.'
              }
            ]
          },
          ...WorkflowMetadataList.slice(1)
        ] as Omit<ElementMetaData, 'workflowTemplateId'>[]
      }
    });

    const { result } = renderHook(() => useSelectElementMetadataForRC());

    expect(result.current.rccOptions.length).toEqual(2);
    expect(result.current.minimumPayDueOptions.length).toEqual(6);
    expect(result.current.oneFeeParticipationCodeOptions.length).toEqual(2);
    expect(result.current.postingTextIDOptions.length).toEqual(6);
    expect(result.current.reversalTextIDOptions.length).toEqual(6);
    expect(result.current.firstYearMaxManagementOptions.length).toEqual(3);
    expect(result.current.oneFeePriorityCodeOptions.length).toEqual(2);
  });

  it('test on selector useSelectElementMetadataForLC', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: {
        loading: true,
        error: false,
        elements: WorkflowMetadataList as Omit<
          ElementMetaData,
          'workflowTemplateId'
        >[]
      }
    });

    const { result } = renderHook(() => useSelectElementMetadataForLC());

    expect(result.current.lateChargesOption.length).toEqual(5);
    expect(result.current.calculationBase.length).toEqual(6);
    expect(result.current.assessmentControl.length).toEqual(3);
    expect(result.current.balanceIndicator.length).toEqual(2);
    expect(result.current.assessedAccounts.length).toEqual(6);
    expect(result.current.currentBalanceAssessment.length).toEqual(2);
    expect(result.current.calculationDayControl.length).toEqual(2);
    expect(result.current.nonProcessingLateCharge.length).toEqual(2);
    expect(result.current.includeExcludeControl.length).toEqual(3);
    expect(result.current.status1.length).toEqual(11);
    expect(result.current.lateChargeResetCounter.length).toEqual(2);
  });

  it('test on selector useSelectElementMetadataForLC with empty value', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: {
        loading: true,
        error: false,
        elements: [
          {
            name: 'late.charges.option',
            options: [{ value: '', description: 'late.charges.option' }]
          }
        ] as Omit<ElementMetaData, 'workflowTemplateId'>[]
      }
    });

    const { result } = renderHook(() => useSelectElementMetadataForLC());

    expect(result.current.lateChargesOption.length).toEqual(1);
  });

  it('test on selector useSelectElementMetadataForRC > undefine / null options', () => {
    const emptyOptMetadataList = WorkflowMetadataList.map(m => ({
      ...m,
      options: undefined
    }));

    mockSelectorWorkflowSetup({
      elementMetadata: {
        loading: true,
        error: false,
        elements: emptyOptMetadataList as Omit<
          ElementMetaData,
          'workflowTemplateId'
        >[]
      }
    });

    const { result } = renderHook(() => useSelectElementMetadataForRC());

    expect(result.current.rccOptions.length).toEqual(0);
    expect(result.current.minimumPayDueOptions.length).toEqual(0);
    expect(result.current.oneFeeParticipationCodeOptions.length).toEqual(0);
    expect(result.current.postingTextIDOptions.length).toEqual(0);
    expect(result.current.reversalTextIDOptions.length).toEqual(0);
    expect(result.current.firstYearMaxManagementOptions.length).toEqual(0);
    expect(result.current.oneFeePriorityCodeOptions.length).toEqual(0);
  });

  it('test on selector useSelectElementMetadataForLC > undefine / null options', () => {
    const emptyOptMetadataList = WorkflowMetadataList.map(m => ({
      ...m,
      options: undefined
    }));

    mockSelectorWorkflowSetup({
      elementMetadata: {
        loading: true,
        error: false,
        elements: emptyOptMetadataList as Omit<
          ElementMetaData,
          'workflowTemplateId'
        >[]
      }
    });

    const { result } = renderHook(() => useSelectElementMetadataForLC());

    expect(result.current.lateChargesOption.length).toEqual(0);
    expect(result.current.calculationBase.length).toEqual(0);
    expect(result.current.assessmentControl.length).toEqual(0);
    expect(result.current.balanceIndicator.length).toEqual(0);
    expect(result.current.assessedAccounts.length).toEqual(0);
    expect(result.current.currentBalanceAssessment.length).toEqual(0);
    expect(result.current.calculationDayControl.length).toEqual(0);
    expect(result.current.nonProcessingLateCharge.length).toEqual(0);
    expect(result.current.includeExcludeControl.length).toEqual(0);
    expect(result.current.status1.length).toEqual(0);
    expect(result.current.lateChargeResetCounter.length).toEqual(0);
  });

  it('test on selector useSelectWorkflowMethodsRawData ', () => {
    const mockMethodList = mappingDataFromArray(
      GetMethodListData,
      GetMethodListDataMapping!
    );

    mockSelectorWorkflowSetup({
      workflowMethods: {
        loading: undefined,
        error: undefined,
        methods: mockMethodList,
        dataFilter: defaultWorkflowMethodsFilter
      }
    });

    const { result } = renderHook(() => useSelectWorkflowMethodsRawData());

    expect(result.current.length).toEqual(2);
  });
});
