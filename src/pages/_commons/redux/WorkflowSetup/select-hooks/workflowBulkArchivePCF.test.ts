import { renderHook } from '@testing-library/react-hooks';
import { WorkflowMetadataAll } from 'app/fixtures/workflow-metadata';
import { WorkflowSetupRootState } from 'app/types';
import * as reactRedux from 'react-redux';
import {
  useMethodTransactionsStatus,
  useSelectElementsBulkArchivePCF,
  useStrategyTransactionsStatus,
  useTableAreaTransactionsStatus,
  useTextAreaTransactionsStatus
} from '.';

describe('redux-store > workflow-setup > select-hooks ', () => {
  let selectorMock: jest.SpyInstance;
  const mockSelectorWorkflowSetup = (
    workflowSetup:
      | Pick<WorkflowSetupRootState, 'workflowFormSteps'>
      | Pick<WorkflowSetupRootState, 'workflowSetup'>
      | Pick<WorkflowSetupRootState, 'workflowMethods'>
      | Pick<WorkflowSetupRootState, 'workflowInstance'>
      | Pick<WorkflowSetupRootState, 'elementMetadata'>
      | Pick<WorkflowSetupRootState, 'workflowChangeSets'>
      | Pick<WorkflowSetupRootState, 'createNewChange'>
      | Pick<WorkflowSetupRootState, 'parameterData'>
  ) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector => selector({ workflowSetup }));
  };

  afterEach(() => {
    selectorMock.mockClear();
  });

  it('test on selector useSelectElementsBulkArchivePCF ', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: { elements: WorkflowMetadataAll }
    } as any);

    const { rerender, result } = renderHook(() =>
      useSelectElementsBulkArchivePCF()
    );

    mockSelectorWorkflowSetup({
      elementMetadata: { elements: [] }
    } as any);
    rerender();
    expect(result.current.methods.length).toEqual(0);
  });

  it(' useStrategyTransactionsStatus ', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: { elements: WorkflowMetadataAll }
    } as any);

    const { rerender, result } = renderHook(() =>
      useStrategyTransactionsStatus()
    );

    mockSelectorWorkflowSetup({
      elementMetadata: { elements: [] }
    } as any);
    rerender();
    expect(result.current).toEqual(undefined);
  });

  it(' useStrategyTransactionsStatus ', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: { elements: WorkflowMetadataAll }
    } as any);

    const { rerender, result } = renderHook(() =>
      useStrategyTransactionsStatus()
    );

    mockSelectorWorkflowSetup({
      elementMetadata: { elements: [] }
    } as any);
    rerender();
    expect(result.current).toEqual(undefined);
  });

  it(' useMethodTransactionsStatus ', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: { elements: WorkflowMetadataAll }
    } as any);

    const { rerender, result } = renderHook(() =>
      useMethodTransactionsStatus()
    );

    mockSelectorWorkflowSetup({
      elementMetadata: { elements: [] }
    } as any);
    rerender();
    expect(result.current).toEqual(undefined);
  });

  it(' useTextAreaTransactionsStatus ', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: { elements: WorkflowMetadataAll }
    } as any);

    const { rerender, result } = renderHook(() =>
      useTextAreaTransactionsStatus()
    );

    mockSelectorWorkflowSetup({
      elementMetadata: { elements: [] }
    } as any);
    rerender();
    expect(result.current).toEqual(undefined);
  });
  it(' useTableAreaTransactionsStatus ', () => {
    mockSelectorWorkflowSetup({
      elementMetadata: { elements: WorkflowMetadataAll }
    } as any);

    const { rerender, result } = renderHook(() =>
      useTableAreaTransactionsStatus()
    );

    mockSelectorWorkflowSetup({
      elementMetadata: { elements: [] }
    } as any);
    rerender();
    expect(result.current).toEqual(undefined);
  });
});
