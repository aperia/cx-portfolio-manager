import { createSelector } from '@reduxjs/toolkit';
import {
  ConfigTableOptionsEnum,
  MethodFieldParameterEnum,
  TableFieldParameterEnum
} from 'app/constants/enums';
import { useSelector } from 'react-redux';
import { KeepTextDelayDesignPromotion } from './_common';

const elementMetadataManageMethodLevelProcessingMLPSelector = createSelector(
  (state: RootState) => state.workflowSetup!.elementMetadata.elements,
  data => {
    const elements = data.map(e => {
      return {
        name: e.name,
        options: KeepTextDelayDesignPromotion.includes(e.name)
          ? e.options?.map(opt => ({
              code: opt.value,
              text: `${opt.value}`
            }))
          : e.options?.map(opt => ({
              code: opt.value,
              text: opt.value
                ? `${opt.value}${
                    opt.description ? ` - ${opt.description}` : ''
                  }`
                : `${opt.description}`
            }))
      } as {
        name: string;
        options: RefData[];
      };
    });

    const searchCode =
      data.find(e => e.name === MethodFieldParameterEnum.SearchCode)?.options ||
      [];
    const aqAllocationFlag = (
      elements.find(e => e.name === TableFieldParameterEnum.AQAllocationFlag)
        ?.options || []
    ).map(item => ({ ...item, code: item.code || '' }));
    const caAllocationFlag = (
      elements.find(e => e.name === TableFieldParameterEnum.CAAllocationFlag)
        ?.options || []
    ).map(item => ({ ...item, code: item.code || '' }));
    const allocationInterval = (
      elements.find(e => e.name === TableFieldParameterEnum.AllocationInterval)
        ?.options || []
    ).map(item => ({ ...item, code: item.code || '' }));
    const allocationBeforeAfterCycle = (
      elements.find(
        e => e.name === TableFieldParameterEnum.AllocationBeforeAfterCycle
      )?.options || []
    ).map(item => ({ ...item, code: item.code || '' }));
    const changeCode = (
      elements.find(e => e.name === TableFieldParameterEnum.ChangeCode)
        ?.options || []
    ).map(item => ({ ...item, code: item.code || '' }));
    const changeInTermsMethodFlag = (
      elements.find(
        e => e.name === TableFieldParameterEnum.ChangeInTermsMethodFlag
      )?.options || []
    ).map(item => ({ ...item, code: item.code || '' }));
    const serviceSubjectSectionAreas = (
      elements.find(
        e => e.name === TableFieldParameterEnum.ServiceSubjectSectionAreas
      )?.options || []
    ).map(item => ({ ...item, code: item.code || '' }));

    const searchCodeOptions = searchCode.map(code => ({
      code: code.value,
      text: `${code.value} - ${code.description}`
    }));

    const mlpTableConfig =
      data.find(e => e.name === ConfigTableOptionsEnum.mlpTable)?.options || [];

    const mlpTableConfigOptions = mlpTableConfig.map(code => ({
      code: code.value,
      text: `${code.description} (${code.value})`
    }));

    return {
      searchCode: searchCodeOptions,
      aqAllocationFlag,
      caAllocationFlag,
      allocationInterval,
      allocationBeforeAfterCycle,
      changeCode,
      changeInTermsMethodFlag,
      serviceSubjectSectionAreas,
      mlpTableConfigOptions
    };
  }
);

export const useSelectElementMetadataManageMethodLevelProcessingMLP = () => {
  return useSelector<
    RootState,
    {
      searchCode: any[];
      aqAllocationFlag: RefData[];
      caAllocationFlag: RefData[];
      allocationInterval: RefData[];
      allocationBeforeAfterCycle: RefData[];
      changeCode: RefData[];
      changeInTermsMethodFlag: RefData[];
      serviceSubjectSectionAreas: RefData[];
      mlpTableConfigOptions: RefData[];
    }
  >(elementMetadataManageMethodLevelProcessingMLPSelector);
};
