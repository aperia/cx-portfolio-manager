import { PayloadAction } from '@reduxjs/toolkit';
import { WorkflowStrategyFlyoutProps } from 'app/components/WorkflowStrategyFlyout';
import {
  ChangeManagementSortByFields,
  OrderBy,
  WorkflowMethodsSortByFields
} from 'app/constants/enums';
import {
  IChangeManagementFilter,
  WorkflowSetupRootState,
  WorkflowSetupState,
  WorkflowSetupStepForm,
  WorkflowSetupStepTracking
} from 'app/types';
import set from 'lodash.set';
import { selectedChangeSetPageSelector } from '../select-hooks';

const reducers = {
  setWorkflowSetup: (
    draftState: WorkflowSetupRootState,
    action: PayloadAction<WorkflowSetupState | undefined>
  ) => {
    draftState.workflowSetup = {
      ...(action.payload || {})
    };
  },
  setWorkflowSelectedStep: (
    draftState: WorkflowSetupRootState,
    action: PayloadAction<string>
  ) => {
    draftState.workflowFormSteps.selectedStep = action.payload;
  },
  setWorkflowLastStep: (
    draftState: WorkflowSetupRootState,
    action: PayloadAction<string>
  ) => {
    draftState.workflowFormSteps.lastStep = action.payload;
  },
  updateWorkflowFormStep: (
    draftState: WorkflowSetupRootState,
    action: PayloadAction<{
      name: string;
      values: WorkflowSetupStepForm;
    }>
  ) => {
    const { name, values } = action.payload;

    draftState.workflowFormSteps.forms = {
      ...draftState.workflowFormSteps.forms,
      [name]: {
        ...(draftState.workflowFormSteps.forms[name] || {}),
        ...(values || {})
      }
    };
  },
  clearWorkflowFormStep: (
    draftState: WorkflowSetupRootState,
    action: PayloadAction<{ step: string; values: any }>
  ) => {
    draftState.workflowFormSteps.forms[action.payload.step] =
      action.payload.values;
  },
  resetWorkflowFormSteps: (draftState: WorkflowSetupRootState) => {
    draftState.createWorkflow = { ...defaultState.createWorkflow };
    draftState.elementMetadata = { ...defaultState.elementMetadata };
    draftState.updateWorkflow = { ...defaultState.updateWorkflow };
    draftState.workflowChangeSets = { ...defaultState.workflowChangeSets };
    draftState.workflowFormSteps = {
      ...defaultState.workflowFormSteps,
      isStuck: {},
      lastSavedInvalidAt: Date.now(),
      savedAt: Date.now()
    };
    draftState.workflowInstance = { ...defaultState.workflowInstance };
    draftState.workflowMethods = { ...defaultState.workflowMethods };
    draftState.workflowSetup = { ...defaultState.workflowSetup };
  },
  updateExpandingMethod: (
    draftState: WorkflowSetupRootState,
    action: PayloadAction<string>
  ) => {
    const current = draftState.workflowMethods.expandingMethod;
    const methodId = action.payload === current ? '' : action.payload;
    set(draftState.workflowMethods, 'expandingMethod', methodId);
  },
  updateWorkflowMethodFilter: (
    draftState: WorkflowSetupRootState,
    action: PayloadAction<IDataFilter>
  ) => {
    const { sortBy, orderBy, page, ...filter } = action.payload;

    const customFilter: IDataFilter = {};
    const hasSort = (sortBy?.length || 0) > 0;
    const hasOrder = (orderBy?.length || 0) > 0;
    if (hasSort) {
      customFilter.sortBy = [...sortBy!, ...defaultWorkflowMethodsSortBy];
    }
    if (hasOrder) {
      customFilter.orderBy = [...orderBy!, ...defaultWorkflowMethodsOrderBy];
    }
    if (page) {
      customFilter.page = page;
    } else if (!hasSort && !hasOrder) {
      customFilter.page = 1;
    }

    draftState.workflowMethods = {
      ...draftState.workflowMethods,
      dataFilter: {
        ...draftState.workflowMethods.dataFilter,
        ...filter,
        ...customFilter
      }
    };
  },
  clearAndResetWorkflowMethodFilterKeepPage: (
    draftState: WorkflowSetupRootState,
    action: PayloadAction<{ page?: number }>
  ) => {
    const sortObject = draftState.workflowMethods.dataFilter;
    draftState.workflowMethods = {
      ...draftState.workflowMethods,
      dataFilter: {
        ...defaultWorkflowMethodsFilter,
        page:
          action.payload.page ||
          sortObject?.page ||
          defaultWorkflowMethodsFilter.page
      },
      strategyFlyoutData: null
    };
  },
  clearAndResetWorkflowMethodFilter: (
    draftState: WorkflowSetupRootState,
    action: PayloadAction<{ resetSort?: boolean } | undefined>
  ) => {
    const resetSort = action.payload?.resetSort;

    const sortObject = resetSort
      ? defaultWorkflowMethodsFilter
      : draftState.workflowMethods.dataFilter;
    draftState.workflowMethods = {
      ...draftState.workflowMethods,
      dataFilter: {
        ...defaultWorkflowMethodsFilter,
        sortBy: sortObject?.sortBy,
        orderBy: sortObject?.orderBy
      },
      strategyFlyoutData: null
    };
  },

  clearAndResetWorkflowPCFMethodFilterKeepPage: (
    draftState: WorkflowSetupRootState,
    action: PayloadAction<{ page?: number; pageSize?: number }>
  ) => {
    const sortObject = draftState.workflowMethods.dataFilter;
    draftState.workflowMethods = {
      ...draftState.workflowMethods,
      dataFilter: {
        ...defaultWorkflowPCFMethodsFilter,
        page:
          action.payload.page ||
          sortObject?.page ||
          defaultWorkflowPCFMethodsFilter.page,
        pageSize:
          action.payload.pageSize ||
          sortObject?.pageSize ||
          defaultWorkflowPCFMethodsFilter.pageSize
      },
      strategyFlyoutData: null
    };
  },
  clearAndResetWorkflowPCFMethodFilter: (
    draftState: WorkflowSetupRootState,
    action: PayloadAction<{ resetSort?: boolean } | undefined>
  ) => {
    const resetSort = action.payload?.resetSort;

    const sortObject = resetSort
      ? defaultWorkflowPCFMethodsFilter
      : draftState.workflowMethods.dataFilter;
    draftState.workflowMethods = {
      ...draftState.workflowMethods,
      dataFilter: {
        ...defaultWorkflowPCFMethodsFilter,
        sortBy: sortObject?.sortBy,
        orderBy: sortObject?.orderBy
      },
      strategyFlyoutData: null
    };
  },
  setStrategyFlyout: (
    draftState: WorkflowSetupRootState,
    action: PayloadAction<{ id: string; isNewVersion: boolean } | null>
  ) => {
    let strategyFlyoutData: Omit<
      WorkflowStrategyFlyoutProps,
      'onClose'
    > | null = null;

    const method = draftState.workflowMethods.methods.find(
      m => m.id === action.payload?.id
    );
    if (method) {
      strategyFlyoutData = {
        id: method.id,
        isNewVersion: action.payload?.isNewVersion,
        methodName: method.name,
        show: true,
        strategyList: method.pricingStrategies || []
      };
    }

    draftState.workflowMethods = {
      ...draftState.workflowMethods,
      strategyFlyoutData
    };
  },
  updateWorkflowChangeSetFilter: (
    draftState: WorkflowSetupRootState,
    action: PayloadAction<IChangeManagementFilter>
  ) => {
    const { sortBy, orderBy, page, ...filter } = action.payload;

    const customFilter: IDataFilter = {};
    const hasSort = (sortBy?.length || 0) > 0;
    const hasOrder = (orderBy?.length || 0) > 0;
    if (hasSort) {
      customFilter.sortBy = [
        ...sortBy!,
        ...defaultWorkflowChangeSetFilter.sortBy!
      ];
    }
    if (hasOrder) {
      customFilter.orderBy = [
        ...orderBy!,
        ...defaultWorkflowChangeSetFilter.orderBy!
      ];
    }
    if (page) {
      customFilter.page = page;
    } else if (!hasSort && !hasOrder) {
      customFilter.page = 1;
    }

    draftState.workflowChangeSets = {
      ...draftState.workflowChangeSets,
      dataFilter: {
        ...draftState.workflowChangeSets.dataFilter,
        ...filter,
        ...customFilter
      },
      newChangeId: ''
    };
  },
  setNewChangeId: (
    draftState: WorkflowSetupRootState,
    action: PayloadAction<string | undefined>
  ) => {
    draftState.workflowChangeSets = {
      ...draftState.workflowChangeSets,
      newChangeId: action.payload
    };
  },
  updateSelectedWorkflowChangeSetPage: (
    draftState: WorkflowSetupRootState,
    action: PayloadAction<{ selectedChangeId: string }>
  ) => {
    const page = selectedChangeSetPageSelector(
      draftState,
      action.payload.selectedChangeId
    );

    draftState.workflowChangeSets = {
      ...draftState.workflowChangeSets,
      dataFilter: {
        ...draftState.workflowChangeSets.dataFilter,
        page
      }
    };
  },
  clearWorkflowChangeSetFilter: (
    draftState: WorkflowSetupRootState,
    action: PayloadAction<{ keepSort: boolean }>
  ) => {
    const { keepSort } = action.payload;

    if (keepSort) {
      draftState.workflowChangeSets = {
        ...draftState.workflowChangeSets,
        dataFilter: {
          ...defaultWorkflowChangeSetFilter,
          sortBy: draftState.workflowChangeSets.dataFilter?.sortBy,
          orderBy: draftState.workflowChangeSets.dataFilter?.orderBy
        },
        newChangeId: ''
      };
    } else {
      draftState.workflowChangeSets = {
        ...draftState.workflowChangeSets,
        dataFilter: {
          ...defaultWorkflowChangeSetFilter
        },
        newChangeId: ''
      };
    }
  },
  setSavedAt: (
    draftState: WorkflowSetupRootState,
    action: PayloadAction<{
      savedAt?: number;
      lastSavedInvalidAt?: number;
      isStuck?: Record<string, boolean>;
      haveBeenSaved?: boolean;
    }>
  ) => {
    draftState.workflowFormSteps = {
      ...draftState.workflowFormSteps,
      ...action.payload,
      isStuck: {
        ...draftState.workflowFormSteps.isStuck,
        ...action.payload.isStuck
      }
    };
  },
  setStepTrackingValidation: (
    draftState: WorkflowSetupRootState,
    action: PayloadAction<Record<string, WorkflowSetupStepTracking>>
  ) => {
    const stateStepTracking = draftState.workflowFormSteps.stepTracking;

    const stepTracking = action.payload;
    Object.keys(stepTracking).forEach(step => {
      stepTracking[step] = {
        ...stateStepTracking[step],
        ...stepTracking[step]
      };
    });

    draftState.workflowFormSteps = {
      ...draftState.workflowFormSteps,
      stepTracking: {
        ...draftState.workflowFormSteps.stepTracking,
        ...stepTracking
      }
    };
  },
  setStepTrackingInprogress: (
    draftState: WorkflowSetupRootState,
    action: PayloadAction<{
      stepId: string;
      inprogress?: boolean;
    }>
  ) => {
    const { stepId, inprogress = true } = action.payload;

    draftState.workflowFormSteps = {
      ...draftState.workflowFormSteps,
      stepTracking: {
        ...draftState.workflowFormSteps.stepTracking,
        [stepId]: {
          ...draftState.workflowFormSteps.stepTracking[stepId],
          inprogress
        }
      }
    };
  },
  setStepTrackingError: (
    draftState: WorkflowSetupRootState,
    action: PayloadAction<{
      stepId: string;
      error?: boolean;
    }>
  ) => {
    const { stepId, error = false } = action.payload;

    draftState.workflowFormSteps = {
      ...draftState.workflowFormSteps,
      stepTracking: {
        ...draftState.workflowFormSteps.stepTracking,
        [stepId]: {
          ...draftState.workflowFormSteps.stepTracking[stepId],
          error
        }
      }
    };
  },
  decisionLevelData: (
    draftState: WorkflowSetupRootState,
    action: PayloadAction<{
      data: MagicKeyValue[];
    }>
  ) => {
    const { data } = action.payload;

    draftState.decisionLevelData = data;
  },

  resetDataTableList: (draftState: WorkflowSetupRootState) => {
    draftState.tableListWorkflowSetup = {
      ...draftState.tableListWorkflowSetup,
      data: []
    };
  },
  updateWorkflowInstance: (
    draftState: WorkflowSetupRootState,
    action: PayloadAction<WorkflowSetupInstance>
  ) => {
    draftState.workflowInstance = {
      ...draftState.workflowInstance,
      instance: action.payload
    };
  }
};

const defaultWorkflowMethodsSortBy: string[] = [
  WorkflowMethodsSortByFields.EFFECTIVE_DATE,
  WorkflowMethodsSortByFields.NAME
];
const defaultWorkflowMethodsOrderBy: OrderByType[] = [
  OrderBy.DESC,
  OrderBy.ASC
];
export const defaultWorkflowMethodsFilter: IDataFilter = {
  page: 1,
  pageSize: 10,
  sortBy: [...defaultWorkflowMethodsSortBy],
  orderBy: [...defaultWorkflowMethodsOrderBy],
  searchValue: ''
};
const defaultWorkflowPCFMethodsSortBy: string[] = [
  WorkflowMethodsSortByFields.NAME
];
const defaultWorkflowPCFMethodsOrderBy: OrderByType[] = [OrderBy.ASC];
export const defaultWorkflowPCFMethodsFilter: IDataFilter = {
  page: 1,
  pageSize: 10,
  sortBy: [...defaultWorkflowPCFMethodsSortBy],
  orderBy: [...defaultWorkflowPCFMethodsOrderBy],
  searchValue: ''
};
const defaultWorkflowBulkPCFSortBy: string[] = [
  WorkflowMethodsSortByFields.EFFECTIVE_DATE,
  WorkflowMethodsSortByFields.NAME
];
const defaultWorkflowBulkPCFOrderBy: OrderByType[] = [
  OrderBy.DESC,
  OrderBy.ASC
];
export const defaultWorkflowBulkPCFilter: IDataFilter = {
  page: 1,
  pageSize: 10,
  sortBy: [...defaultWorkflowBulkPCFSortBy],
  orderBy: [...defaultWorkflowBulkPCFOrderBy],
  searchValue: ''
};
export const defaultWorkflowChangeSetFilter: IChangeManagementFilter = {
  page: 1,
  pageSize: 10,
  searchValue: '',
  sortBy: [ChangeManagementSortByFields.CREATED_DATE],
  orderBy: [OrderBy.DESC],
  changeStatus: 'All'
};

export const defaultState = {
  workflowSetup: {
    isTemplate: false,
    workflow: undefined
  },
  workflowFormSteps: {
    selectedStep: 'gettingStart',
    lastStep: 'gettingStart',
    forms: {} as Record<string, WorkflowSetupStepForm>,
    workflowInfo: {},
    savedInvalidAt: {},
    lastSavedInvalidAt: Date.now(),
    savedValidAt: {},
    savedAt: Date.now(),
    haveBeenSaved: false,
    stepTracking: {}
  },
  workflowMethods: {
    methods: [],
    loading: false,
    error: false,
    expandingMethod: '',
    dataFilter: defaultWorkflowMethodsFilter,
    strategyFlyoutData: null
  },
  workflowStrategies: {
    strategies: [],
    loading: false,
    error: false,
    expandingStrategy: '',
    dataFilter: defaultWorkflowBulkPCFilter
  },
  workflowTextAreas: {
    textAreas: [],
    loading: false,
    error: false,
    expandingTextArea: '',
    dataFilter: defaultWorkflowBulkPCFilter
  },
  workflowTableAreas: {
    tableAreas: [],
    loading: false,
    error: false,
    expandingTableArea: '',
    dataFilter: defaultWorkflowBulkPCFilter
  },
  workflowInstance: {
    loading: false,
    error: false
  },
  createWorkflow: {
    loading: false,
    error: false
  },
  updateWorkflow: {
    loading: false,
    error: false
  },
  elementMetadata: {
    loading: false,
    error: false,
    elements: []
  },
  elementMetadataNonWorkflow: {
    loading: false,
    error: false,
    elements: []
  },
  exportAnalyzer: {
    loading: false,
    error: false
  },
  services: { loading: false, error: false, data: [] },
  tableListWorkflowSetup: { loading: false, error: false, data: [], total: 0 },
  workflowChangeSets: {
    loading: false,
    error: false,
    dataFilter: defaultWorkflowChangeSetFilter,
    changeSetList: []
  },
  createNewChange: {
    loading: false,
    error: false
  },
  strategyData: {
    loading: false,
    error: false,
    strategyList: [],
    methods: {}
  },
  parameterData: {
    loading: false,
    error: false,
    parameterList: []
  },
  lettersData: {
    loading: false,
    error: false,
    letters: []
  },
  ocsShellData: {
    loading: false,
    error: false,
    ocsShells: []
  },
  merchantData: { loading: false, error: false, merchants: [] },
  exportMerchant: { loading: false, error: false },
  queueProfile: { loading: false, error: false },
  exportShellProfile: { loading: false, error: false },
  ocsShellProfile: { loading: false, error: false },
  profileTransactions: { loading: false, error: false },
  shellList: { loading: false, error: false },
  tableList: { loading: false, error: false },
  decisionList: { loading: false, error: false },
  decisionLevelData: [],
  decisionElementList: { loading: false, error: false },
  supervisorData: { loading: false, error: false, supervisorList: [] },
  strategiesListAQ: { loading: false, error: false, strategiesListAQData: [] }
} as WorkflowSetupRootState;

export default reducers;
