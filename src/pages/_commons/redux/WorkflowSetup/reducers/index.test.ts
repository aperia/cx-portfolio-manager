import {
  ChangeManagementSortByFields,
  OrderBy,
  WorkflowMethodsSortByFields
} from 'app/constants/enums';
import { WorkflowSetupStepForm } from 'app/types';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';
import {
  defaultWorkflowChangeSetFilter,
  defaultWorkflowMethodsFilter
} from '.';

const initReducer: any = {
  workflowMethods: {
    methods: [],
    loading: false,
    error: false,
    expandingMethod: '',
    dataFilter: defaultWorkflowMethodsFilter
  },
  workflowChangeSets: {
    loading: false,
    error: false,
    dataFilter: defaultWorkflowChangeSetFilter,
    changeSetList: []
  },
  workflowSetup: {
    isTemplate: false,
    workflow: undefined
  },
  workflowFormSteps: {
    selectedStep: 'gettingStart',
    lastStep: 'gettingStart',
    forms: {} as Record<string, WorkflowSetupStepForm>,
    stepTracking: {}
  }
};

describe('redux-store > workflow-setup > reducers', () => {
  it('setWorkflowSetup', () => {
    let nextState = reducer(
      initReducer,
      actionsWorkflowSetup.setWorkflowSetup({
        isTemplate: true,
        workflow: { id: '123' } as IWorkflowTemplateModel
      })
    );
    expect(nextState.workflowSetup.isTemplate).toEqual(true);
    expect(nextState.workflowSetup.workflow!.id).toEqual('123');

    nextState = reducer(
      initReducer,
      actionsWorkflowSetup.setWorkflowSetup(undefined)
    );

    expect(nextState.workflowSetup.isTemplate).toBeUndefined();
  });

  it('setWorkflowSelectedStep', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.setWorkflowSelectedStep('gettingStart')
    );
    expect(nextState.workflowFormSteps.selectedStep).toEqual('gettingStart');
  });

  it('setWorkflowLastStep', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.setWorkflowLastStep('summary')
    );
    expect(nextState.workflowFormSteps.lastStep).toEqual('summary');
  });

  it('updateWorkflowFormSteps', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.updateWorkflowFormStep({
        name: 'changeInformation',
        values: {
          name: 'mock name',
          description: 'mock description',
          workflowNote: 'mock note'
        }
      })
    );
    expect(nextState.workflowFormSteps.forms['changeInformation']).toEqual({
      name: 'mock name',
      description: 'mock description',
      workflowNote: 'mock note'
    });
  });

  it('clearWorkflowFormStep', () => {
    const nextState = reducer(
      {
        ...initReducer,
        workflowFormSteps: {
          forms: {
            gettingStart: 'mock-value'
          }
        }
      },
      actionsWorkflowSetup.clearWorkflowFormStep({
        step: 'gettingStart',
        values: {}
      })
    );
    expect(nextState.workflowFormSteps.forms['gettingStart']).toEqual({});
  });

  it('updateWorkflowFormSteps > undefined values', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.updateWorkflowFormStep({
        name: 'changeInformation',
        values: undefined as any
      })
    );
    expect(nextState.workflowFormSteps.forms['changeInformation']).toEqual({});
  });

  it('resetWorkflowFormSteps', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.resetWorkflowFormSteps()
    );
    expect(nextState.workflowFormSteps.forms).toEqual({});
  });

  it('updateExpandingMethod', () => {
    let nextState = reducer(
      initReducer,
      actionsWorkflowSetup.updateExpandingMethod('')
    );

    expect(nextState.workflowMethods.expandingMethod).toEqual('');

    nextState = reducer(
      initReducer,
      actionsWorkflowSetup.updateExpandingMethod('1')
    );

    expect(nextState.workflowMethods.expandingMethod).toEqual('1');
  });

  it('updateWorkflowMethodFilter', () => {
    // No sort & order
    let nextState = reducer(
      initReducer,
      actionsWorkflowSetup.updateWorkflowMethodFilter({
        ...defaultWorkflowMethodsFilter,
        page: 0,
        sortBy: undefined,
        orderBy: undefined
      })
    );

    expect(nextState.workflowMethods.dataFilter!.sortBy![0]).toEqual(
      WorkflowMethodsSortByFields.EFFECTIVE_DATE
    );

    // has sort & order
    nextState = reducer(
      initReducer,
      actionsWorkflowSetup.updateWorkflowMethodFilter({
        ...defaultWorkflowMethodsFilter,
        sortBy: [WorkflowMethodsSortByFields.NAME],
        orderBy: ['desc']
      })
    );

    expect(nextState.workflowMethods.dataFilter!.sortBy![0]).toEqual(
      WorkflowMethodsSortByFields.NAME
    );

    // no page
    nextState = reducer(
      initReducer,
      actionsWorkflowSetup.updateWorkflowMethodFilter({
        ...defaultWorkflowMethodsFilter,
        page: undefined
      })
    );

    expect(nextState.workflowMethods.dataFilter!.page).toEqual(1);
  });

  it('clearAndResetWorkflowMethodFilter', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.clearAndResetWorkflowMethodFilter()
    );

    expect(nextState.workflowMethods.dataFilter?.page).toEqual(1);
  });

  it('clearAndResetWorkflowMethodFilterKeepPage', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.clearAndResetWorkflowMethodFilterKeepPage({
        ...defaultWorkflowMethodsFilter,
        page: 0,
        sortBy: undefined,
        orderBy: undefined
      })
    );

    expect(nextState.workflowMethods.dataFilter?.page).toEqual(1);
  });

  it('clearAndResetWorkflowPCFMethodFilterKeepPage', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.clearAndResetWorkflowPCFMethodFilterKeepPage({
        ...defaultWorkflowMethodsFilter,
        page: 0,
        sortBy: undefined,
        orderBy: undefined
      })
    );

    expect(nextState.workflowMethods.dataFilter?.page).toEqual(1);
  });

  it('clearAndResetWorkflowPCFMethodFilter', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.clearAndResetWorkflowPCFMethodFilter({
        ...defaultWorkflowMethodsFilter,
        page: 0,
        sortBy: undefined,
        orderBy: undefined,
        resetSort: true
      })
    );

    expect(nextState.workflowMethods.dataFilter?.page).toEqual(1);
  });

  it('decisionLevelData', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.decisionLevelData({
        data: { id: 'id' }
      })
    );
    expect(nextState.decisionLevelData).toEqual({
      id: 'id'
    });
  });

  it('resetDataTableList', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.resetDataTableList()
    );
    expect(nextState.tableListWorkflowSetup.data).toEqual([]);
  });

  it('setStrategyFlyout > have method data', () => {
    const nextState = reducer(
      {
        ...initReducer,
        workflowMethods: {
          ...initReducer.workflowMethods,
          methods: [{ id: 'mock-id' }]
        }
      },
      actionsWorkflowSetup.setStrategyFlyout({
        id: 'mock-id',
        isNewVersion: true
      })
    );

    expect(nextState.workflowMethods.strategyFlyoutData?.id).toEqual('mock-id');
    expect(nextState.workflowMethods.strategyFlyoutData?.isNewVersion).toEqual(
      true
    );
  });

  it('setStrategyFlyout > no data', () => {
    const nextState = reducer(
      {
        ...initReducer,
        workflowMethods: {
          ...initReducer.workflowMethods,
          methods: []
        }
      },
      actionsWorkflowSetup.setStrategyFlyout({
        id: 'mock-id',
        isNewVersion: true
      })
    );

    expect(nextState.workflowMethods.strategyFlyoutData).toBeNull();
  });

  it('updateWorkflowChangeSetFilter', () => {
    // No sort & order
    let nextState = reducer(
      initReducer,
      actionsWorkflowSetup.updateWorkflowChangeSetFilter({
        ...defaultWorkflowChangeSetFilter,
        page: 0,
        sortBy: undefined,
        orderBy: undefined
      })
    );

    expect(nextState.workflowChangeSets.dataFilter!.sortBy![0]).toEqual(
      ChangeManagementSortByFields.CREATED_DATE
    );
    expect(nextState.workflowChangeSets.dataFilter!.orderBy![0]).toEqual(
      OrderBy.DESC
    );

    // has sort & order
    nextState = reducer(
      initReducer,
      actionsWorkflowSetup.updateWorkflowChangeSetFilter({
        ...defaultWorkflowChangeSetFilter,
        sortBy: [ChangeManagementSortByFields.CHANGE_NAME],
        orderBy: ['asc']
      })
    );

    expect(nextState.workflowChangeSets.dataFilter!.sortBy![0]).toEqual(
      ChangeManagementSortByFields.CHANGE_NAME
    );
    expect(nextState.workflowChangeSets.dataFilter!.orderBy![0]).toEqual(
      OrderBy.ASC
    );

    // no page
    nextState = reducer(
      initReducer,
      actionsWorkflowSetup.updateWorkflowChangeSetFilter({
        ...defaultWorkflowChangeSetFilter,
        page: undefined
      })
    );

    expect(nextState.workflowChangeSets.dataFilter!.page).toEqual(1);
  });

  it('updateSelectedWorkflowChangeSetPage', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.updateSelectedWorkflowChangeSetPage({
        selectedChangeId: 'mock-id'
      })
    );

    expect(nextState.workflowChangeSets.dataFilter?.page).toEqual(1);
  });

  it('clearWorkflowChangeSetFilter', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.clearWorkflowChangeSetFilter({ keepSort: false })
    );

    expect(nextState.workflowChangeSets.dataFilter?.page).toEqual(1);
  });

  it('clearWorkflowChangeSetFilter when keepSort = true', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.clearWorkflowChangeSetFilter({ keepSort: true })
    );

    expect(nextState.workflowChangeSets.dataFilter?.page).toEqual(1);
  });

  it('setNewChangeId', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.setNewChangeId('1000')
    );

    expect(nextState.workflowChangeSets.newChangeId).toEqual('1000');
  });

  it('setSavedAt', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.setSavedAt({ savedAt: 123456, haveBeenSaved: true })
    );
    expect(nextState.workflowFormSteps.savedAt).toEqual(123456);
    expect(nextState.workflowFormSteps.haveBeenSaved).toEqual(true);
  });

  it('setStepTrackingValidation', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.setStepTrackingValidation({
        step1: { doneValidation: 123456 }
      })
    );
    expect(nextState.workflowFormSteps.stepTracking).toEqual({
      step1: { doneValidation: 123456 }
    });
  });

  it('setStepTrackingError', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.setStepTrackingError({
        stepId: 'step1'
      })
    );
    expect(nextState.workflowFormSteps.stepTracking).toEqual({
      step1: { error: false }
    });
  });

  it('setStepTrackingInprogress', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.setStepTrackingInprogress({ stepId: 'step1' })
    );
    expect(nextState.workflowFormSteps.stepTracking).toEqual({
      step1: { inprogress: true }
    });
  });

  it('updateWorkflowInstance', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.updateWorkflowInstance({
        workflowInstanceDetails: {}
      } as any)
    );
    expect(
      nextState.workflowInstance.instance!.workflowInstanceDetails
    ).toEqual({});
  });
});
