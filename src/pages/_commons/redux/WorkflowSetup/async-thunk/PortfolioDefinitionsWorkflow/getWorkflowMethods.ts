import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { OrderBy, WorkflowMethodsSortByFields } from 'app/constants/enums';
import { CHANGE_STATUS_MAPPING } from 'app/constants/mapping';
import { mappingDataFromArray } from 'app/helpers';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import _orderBy from 'lodash.orderby';
import set from 'lodash.set';
import { defaultWorkflowMethodsFilter } from '../../reducers';

export interface GetWorkflowMethodsArgs {
  serviceSubjectSection: string;
  defaultMethodId?: string;
}

export const getPortfolioDefinitionsWorkflow = createAsyncThunk<
  any,
  GetWorkflowMethodsArgs,
  ThunkAPIConfig
>(
  'workflowSetup/getPortfolioDefinitionsWorkflow',
  async (
    args: {
      serviceSubjectSection: string;
      defaultMethodId?: string;
    },
    thunkAPI
  ) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;
    const resp = await workflowService.getWorkflowMethods(
      args.serviceSubjectSection
    );

    const state = thunkAPI.getState();
    // Take mapping model
    const workflowMethodListMapping = state.mapping?.data?.workflowMethodList;
    const methodList = resp?.data?.methodList;
    // Mapping
    const mappedData = mappingDataFromArray(
      methodList,
      workflowMethodListMapping!
    );
    const { sortBy, orderBy } =
      state.workflowSetup!.workflowMethods.dataFilter!;
    const orderedMethods = _orderBy(mappedData, sortBy, orderBy);

    const methods = orderedMethods.map((m, index) => {
      const versions = (m.versions || []).map((v: IMethodVersion) => ({
        ...v,
        status: CHANGE_STATUS_MAPPING[v.status]
      }));

      return {
        ...m,
        index,
        versions: _orderBy(
          versions,
          [WorkflowMethodsSortByFields.EFFECTIVE_DATE],
          [OrderBy.DESC]
        ),
        numberOfVersions: m.versions?.length || 0,
        relatedPricingStrategies: m.pricingStrategies?.length || 0
      };
    });

    const selectedIdx = methods.findIndex(i => i.id === args.defaultMethodId);
    if (selectedIdx >= 0) {
      const page = Math.ceil((selectedIdx + 1) / 10);
      return {
        methods,
        page,
        expandingMethod: methods[selectedIdx]?.id
      };
    }

    return {
      methods,
      page: defaultWorkflowMethodsFilter.page,
      expandingMethod: ''
    };
  }
);
export const getPortfolioDefinitionsWorkflowBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    getPortfolioDefinitionsWorkflow.pending,
    (draftState, action) => {
      set(draftState.workflowMethods, 'loading', true);
      set(draftState.workflowMethods, 'error', false);
    }
  );
  builder.addCase(
    getPortfolioDefinitionsWorkflow.fulfilled,
    (draftState, action) => {
      return {
        ...draftState,
        workflowMethods: {
          ...draftState.workflowMethods,
          methods: action.payload.methods,
          dataFilter: {
            ...draftState.workflowMethods.dataFilter,
            page: action.payload.page
          },
          expandingMethod: action.payload.expandingMethod,
          loading: false,
          error: false
        }
      };
    }
  );
  builder.addCase(
    getPortfolioDefinitionsWorkflow.rejected,
    (draftState, action) => {
      set(draftState.workflowMethods, 'loading', false);
      set(draftState.workflowMethods, 'error', true);
    }
  );
};
