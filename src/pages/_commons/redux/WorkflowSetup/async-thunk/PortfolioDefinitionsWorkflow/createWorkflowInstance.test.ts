import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  workflowInstance: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      changeInformation: {
        changeSetId: 'changeSetId',
        name: 'mock name',
        description: 'mock description',
        effectiveDate: new Date()
      },
      selectMonetaryAdjustmentTransactions: {},
      uploadFile: {}
    }
  }
};

describe('redux-store > workflow-setup > PortfolioDefinitionsWorkflow > createWorkflowInstance', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.createPortfolioDefinitionsWorkflowInstance.pending(
        'createWorkflowInstance',
        { workflowSetupData: [] }
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.createPortfolioDefinitionsWorkflowInstance.fulfilled(
        {},
        'createWorkflowInstance',
        { workflowSetupData: [] }
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.createPortfolioDefinitionsWorkflowInstance.rejected(
        new Error('mock error'),
        'createWorkflowInstance',
        { workflowSetupData: [] }
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(true);
  });

  it('thunk action > without workflowInstanceId & changeId', async () => {
    const nextState: any =
      await actionsWorkflowSetup.createPortfolioDefinitionsWorkflowInstance({
        workflowSetupData: []
      })(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer
            }
          } as RootState),
        {
          workflowService: {
            createWorkflowInstance: jest.fn().mockResolvedValue({
              data: { workflowInstanceDetails: { id: 'workflowId' } }
            }),
            updateWorkflowInstanceData: jest.fn()
          }
        }
      );

    expect(nextState.payload).toEqual({
      changeId: 'changeSetId',
      workflowInstanceId: 'workflowId',
      workflowSetupData: []
    });
  });

  it('thunk action > with workflowInstanceId & changeId', async () => {
    const state = {
      workflowFormSteps: {
        forms: {
          changeInformation: {
            changeSetId: 'changeSetId',
            workflowInstanceId: 'workflowId',
            name: 'mock name',
            description: 'mock description',
            effectiveDate: new Date()
          },
          selectMonetaryAdjustmentTransactions: {},
          uploadFile: {}
        }
      }
    };

    const nextState: any =
      await actionsWorkflowSetup.createPortfolioDefinitionsWorkflowInstance({
        workflowSetupData: []
      })(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer,
              ...state
            }
          } as RootState),
        {
          workflowService: {
            updateWorkflowInstance: jest.fn(),
            updateWorkflowInstanceData: jest.fn()
          }
        }
      );
    expect(nextState.payload).toEqual({
      changeId: 'changeSetId',
      workflowInstanceId: 'workflowId',
      workflowSetupData: []
    });
  });
});
