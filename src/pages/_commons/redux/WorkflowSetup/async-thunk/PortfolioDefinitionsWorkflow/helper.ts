import { MethodFieldParameterEnum } from 'app/constants/enums';

export const parseMethodFieldParameter = (parameter: any) => {
  const newValue = parameter.newValue;
  return { ...parameter, newValue };
};

const getBase64File = (file: File) => {
  return new Promise((res, rej) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      res(reader.result);
    };
  });
};

export const parseMethodData = async (...methodForm: any[]) => {
  const methodList: any[] = [];

  for (let i = 0; i < methodForm.length; i++) {
    const form = methodForm[i];
    if (form?.methods?.length > 0)
      for (let j = 0; j < form?.methods.length; j++) {
        const m = form?.methods[j];

        const method = {
          id: '',
          portfolioName: m.name,
          portfolioAtributes:
            m[MethodFieldParameterEnum.PortfolioDefinitionsAttributes]
        };

        if (m.portfolioIcon) {
          const file = await getBase64File(m.portfolioIcon);
          Object.assign(method, {
            portfolioIcon: { value: file, name: m.portfolioIcon?.name }
          });
        }

        methodList.push(method);
      }
  }
  return methodList;
};
