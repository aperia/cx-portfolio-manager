import { MethodFieldParameterEnum } from 'app/constants/enums';
import { parseMethodData, parseMethodFieldParameter } from './helper';

describe('redux-store > workflow-setup > PortfolioDefinitions > helper', () => {
  beforeEach(() => {
    jest.setTimeout(5000);
  });

  it('parseMethodFieldParameter', () => {
    const input = {
      name: MethodFieldParameterEnum.PayoffExceptionStatementValue,
      newValue: '1111'
    };
    const result = parseMethodFieldParameter(input);
    expect(result.newValue).toEqual('1111');
  });

  it('parseMethodData', async () => {
    let input: any;
    try {
      await parseMethodData(input);

      input = {
        methods: [
          {
            name: 'name',
            modeledFrom: 'modeledFrom',
            comment: 'comment',
            serviceSubjectSection: 'serviceSubjectSection',
            parameters: [
              {
                name: 'attributes',
                newValue: [
                  {
                    type: 'attribute',
                    operator: 'where',
                    item: {
                      filterBy: 'Sys',
                      operator: 'and',
                      value: '123'
                    }
                  }
                ]
              }
            ],
            portfolioIcon: new File([new ArrayBuffer(1)], 'file.png'),
            [MethodFieldParameterEnum.PortfolioDefinitionsAttributes]: '123'
          },
          {
            name: 'name1',
            serviceSubjectSection: 'serviceSubjectSection1',
            parameters: [
              {
                name: 'attributes',
                newValue: [
                  {
                    type: 'attribute',
                    operator: 'where',
                    item: {
                      filterBy: 'Sys',
                      operator: 'and',
                      value: '123'
                    }
                  }
                ]
              }
            ]
          }
        ]
      };
      await parseMethodData(input);
    } catch (error) {
      console.log(error);
    }
  });
});
