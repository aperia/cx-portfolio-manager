import { FormConfigFDSA } from 'pages/WorkflowManageAdjustmentsSystem/ConfigFDSAStep';
import { FormConfigFLSADJ } from 'pages/WorkflowManageAdjustmentsSystem/ConfigFLSADJStep';
import { FormConfigJFS } from 'pages/WorkflowManageAdjustmentsSystem/ConfigJFSStep';
import { FormConfigJQA } from 'pages/WorkflowManageAdjustmentsSystem/ConfigJQAStep';
import { parseConfigurationsValue } from './helper';
describe('helper', () => {
  it('should parseConfigurationsValue with empty value', () => {
    const configJfsForm = {} as FormConfigJFS;
    const configFlsAdjForm = {
      profiles: []
    } as FormConfigFLSADJ;
    const configJqaForm = {} as FormConfigJQA;
    const configFdsaForm = {} as FormConfigFDSA;

    const value = parseConfigurationsValue(
      configJfsForm,
      configFlsAdjForm,
      configJqaForm,
      configFdsaForm
    );

    expect(value).toEqual({});
  });
  it('should parseConfigurationsValue with empty value', () => {
    const configJfsForm = {
      shellName: 'shellName',
      generalInfo: 'generalInfo',
      adjustmentLimitInfo: 'adjustmentLimitInfo',
      profiles: [
        {
          code: '1',
          subSystemAdj: '1',
          transactions: [
            {
              status: 'status',
              batchCode: 'batchCode',
              originalProfile: {
                batchCode: 'batchCode',
                transactionCode: 'transactionCode',
                maximumAmount: 'maximumAmount',
                actionCode: 'actionCode'
              }
            }
          ]
        }
      ] as any
    } as FormConfigJFS;
    const configFlsAdjForm = {
      profiles: [
        {
          code: '1',
          subSystemAdj: '1',
          transactions: [
            {
              status: 'status',
              batchCode: 'batchCode',
              originalProfile: {
                batchCode: 'batchCode',
                transactionCode: 'transactionCode',
                maximumAmount: 'maximumAmount',
                actionCode: 'actionCode'
              }
            }
          ]
        }
      ] as any
    } as FormConfigFLSADJ;
    const configJqaForm = {
      queueOwner: 'queueOwner',
      managerId: { code: 'code', text: 'text', selected: true },
      generalInfo: {
        managerId: 'managerId',
        queueOwner: 'queueOwner',
        queueOwnerId: 'queueOwnerId',
        terminalGroupId: 'terminalGroupId'
      },
      alternateAccessShellNames: [
        {
          rowId: 1,
          status: 'status',
          shellName: 'shellName',
          newShellName: 'newShellName'
        }
      ] as any
    } as FormConfigJQA;
    const configFdsaForm = {
      shells: [
        {
          status: 'status',
          shellName: 'shellName',
          departmentId: 'departmentId',
          transactionProfile: 'transactionProfile',
          description: 'description',
          operatorCode: 'operatorCode',
          employeeId: 'employeeId',
          original: { level: '1', supervisorShell: 'supervisorShell' },
          level: '2',
          supervisorShell: 'supervisorShell'
        }
      ] as any
    } as FormConfigFDSA;

    const value = parseConfigurationsValue(
      configJfsForm,
      configFlsAdjForm,
      configJqaForm,
      configFdsaForm
    );

    expect(value).toEqual({
      profileList: [
        {
          ocsTransactionActions: [
            {
              parameters: [
                {
                  name: 'batch.code',
                  newValue: 'batchCode',
                  previousValue: 'batchCode'
                },
                {
                  name: 'transaction.code',
                  newValue: undefined,
                  previousValue: 'transactionCode'
                },
                {
                  name: 'maximum.amount',
                  newValue: undefined,
                  previousValue: 'maximumAmount'
                },
                {
                  name: 'action.code',
                  newValue: undefined,
                  previousValue: 'actionCode'
                }
              ],
              status: 'status'
            }
          ],
          profileName: '1',
          subSystemAdj: '1'
        }
      ],
      shellProfile: {
        shellName: 'shellName',
        adjustmentLimitInformation: 'adjustmentLimitInfo',
        generalInformation: 'generalInfo'
      },
      queueProfile: {
        alternateAccessShellNames: [
          {
            parameters: [
              {
                name: 'shell.name',
                newValue: 'newShellName',
                previousValue: 'shellName'
              }
            ],
            status: 'status'
          }
        ],
        generalInformation: {
          managerId: 'managerId',
          queueOwner: 'queueOwner',
          queueOwnerId: 'queueOwnerId',
          terminalGroupId: 'terminalGroupId'
        },
        managerId: {
          code: 'code',
          selected: true,
          text: 'text'
        },
        queueOwner: 'queueOwner'
      },
      shellList: [
        {
          departmentId: 'departmentId',
          description: 'description',
          employeeId: 'employeeId',
          operatorCode: 'operatorCode',
          parameters: [
            {
              name: 'level',
              newValue: '2',
              previousValue: '1'
            },
            {
              name: 'supervisor.shell',
              newValue: 'supervisorShell',
              previousValue: 'supervisorShell'
            }
          ],
          shellName: 'shellName',
          status: 'status',
          transactionProfile: 'transactionProfile'
        }
      ]
    });
  });
});
