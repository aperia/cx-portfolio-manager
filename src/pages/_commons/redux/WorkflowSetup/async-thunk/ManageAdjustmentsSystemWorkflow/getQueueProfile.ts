import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { mappingDataFromObj } from 'app/helpers';
import { APIMapping } from 'app/services';
import { QueueProfile, WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';

export const getManageAdjustmentsSystemQueueProfile = createAsyncThunk<
  QueueProfile,
  string,
  ThunkAPIConfig
>(
  'workflowSetup/getManageAdjustmentsSystemQueueProfile',
  async (args, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;
    const resp = await workflowService.getQueueProfile(args);

    const state = thunkAPI.getState();
    // Take mapping model
    const queueProfileListMapping = state.mapping?.data?.queueProfile;

    const queueProfile = resp?.data?.queueProfile;
    // Mapping
    const mappedData = mappingDataFromObj(
      queueProfile,
      queueProfileListMapping!
    ) as QueueProfile;
    return mappedData;
  }
);
export const getManageAdjustmentsSystemQueueProfileBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    getManageAdjustmentsSystemQueueProfile.pending,
    draftState => {
      set(draftState.queueProfile, 'loading', true);
      set(draftState.queueProfile, 'error', false);
    }
  );
  builder.addCase(
    getManageAdjustmentsSystemQueueProfile.fulfilled,
    draftState => {
      set(draftState.queueProfile, 'loading', false);
      set(draftState.queueProfile, 'error', false);
    }
  );
  builder.addCase(
    getManageAdjustmentsSystemQueueProfile.rejected,
    draftState => {
      set(draftState.queueProfile, 'loading', false);
      set(draftState.queueProfile, 'error', true);
    }
  );
};
