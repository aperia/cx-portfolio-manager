import { isEmpty } from 'lodash';
import { FormConfigFDSA } from 'pages/WorkflowManageAdjustmentsSystem/ConfigFDSAStep';
import { ParameterMetadata } from 'pages/WorkflowManageAdjustmentsSystem/ConfigFDSAStep/type';
import { FormConfigFLSADJ } from 'pages/WorkflowManageAdjustmentsSystem/ConfigFLSADJStep';
import { FormConfigJFS } from 'pages/WorkflowManageAdjustmentsSystem/ConfigJFSStep';
import { FormConfigJQA } from 'pages/WorkflowManageAdjustmentsSystem/ConfigJQAStep';
import { ParameterCode } from 'pages/WorkflowManageAdjustmentsSystem/type';

export const parseConfigurationsValue = (
  configJfsForm: FormConfigJFS,
  configFlsAdjForm: FormConfigFLSADJ,
  configJqaForm: FormConfigJQA,
  configFdsaForm: FormConfigFDSA
): Record<string, any> => {
  const configurations: Record<string, any> = {};
  configJfsForm?.shellName &&
    (configurations.shellProfile = {
      shellName: configJfsForm.shellName,
      generalInformation: configJfsForm.generalInfo,
      adjustmentLimitInformation: configJfsForm.adjustmentLimitInfo
    });

  !isEmpty(configFlsAdjForm?.profiles) &&
    (configurations.profileList = configFlsAdjForm.profiles.map(p => ({
      profileName: p.code,
      subSystemAdj: p.subSystemAdj,
      ocsTransactionActions: p.transactions?.map(t => ({
        status: t.status,
        parameters: [
          {
            name: ParameterCode.BatchCode,
            previousValue: t.originalProfile?.batchCode,
            newValue: t.batchCode
          },
          {
            name: ParameterCode.TransactionCode,
            previousValue: t.originalProfile?.transactionCode,
            newValue: t.transactionCode
          },
          {
            name: ParameterCode.MaximumAmount,
            previousValue: t.originalProfile?.maximumAmount,
            newValue: t.maximumAmount
          },
          {
            name: ParameterCode.ActionCode,
            previousValue: t.originalProfile?.actionCode,
            newValue: t.actionCode
          }
        ]
      }))
    })));

  configJqaForm?.queueOwner &&
    (configurations.queueProfile = {
      queueOwner: configJqaForm.queueOwner,
      managerId: configJqaForm.managerId,
      generalInformation: configJqaForm.generalInfo,
      alternateAccessShellNames: configJqaForm.alternateAccessShellNames?.map(
        a => ({
          status: a.status,
          parameters: [
            {
              name: ParameterCode.ShellName,
              previousValue: a.shellName,
              newValue: a.newShellName
            }
          ]
        })
      )
    });

  !isEmpty(configFdsaForm?.shells) &&
    (configurations.shellList = configFdsaForm.shells?.map(p => ({
      status: p.status,
      shellName: p.shellName,
      departmentId: p.departmentId,
      transactionProfile: p.transactionProfile,
      description: p.description,
      operatorCode: p.operatorCode,
      employeeId: p.employeeId,
      parameters: [
        {
          name: ParameterMetadata.Level,
          previousValue: p.original?.level,
          newValue: p.level
        },
        {
          name: ParameterMetadata.SupervisorShell,
          previousValue: p.original?.supervisorShell,
          newValue: p.supervisorShell
        }
      ]
    })));

  return configurations;
};
