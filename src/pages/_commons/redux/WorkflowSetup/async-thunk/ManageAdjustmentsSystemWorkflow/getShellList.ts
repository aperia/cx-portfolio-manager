import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { mappingDataFromArray } from 'app/helpers';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import { set } from 'lodash';
import get from 'lodash.get';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';

export const getShellList = createAsyncThunk<any, undefined, ThunkAPIConfig>(
  'workflowSetup/getShellList',
  async (undefined, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;
    const state = thunkAPI.getState();
    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;

    const response = await workflowService.getOCSShellList({
      searchFields: {
        workflowInstanceId: changeForm.workflowInstanceId?.toString()
      }
    });

    // Mapping
    const ocsShellListMapping = state.mapping?.data?.ocsShell;

    const mappedData = mappingDataFromArray(
      response?.data?.ocsShells,
      ocsShellListMapping!
    ).map((i, idx) => ({
      ...i,
      rowId: `${i.shellName}_${idx}_${Date.now()}`,
      original: i
    }));

    return mappedData;
  }
);

export const getShellListBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(getShellList.pending, (draftState, action) => {
    set(draftState.shellList, 'loading', true);
    set(draftState.shellList, 'error', false);
  });
  builder.addCase(getShellList.fulfilled, (draftState, action) => {
    set(draftState.shellList, 'loading', false);
    set(draftState.shellList, 'error', false);
    // set(draftState.ocsShellData, 'ocsShells', action.payload?.ocsShells);
  });
  builder.addCase(getShellList.rejected, (draftState, action) => {
    set(draftState.shellList, 'loading', false);
    set(draftState.shellList, 'error', true);
  });
};
