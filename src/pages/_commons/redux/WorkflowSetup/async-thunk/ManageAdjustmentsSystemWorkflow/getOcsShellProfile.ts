import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { mappingDataFromObj } from 'app/helpers';
import { APIMapping } from 'app/services';
import { OcsShellProfile, WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';

export const getManageAdjustmentsSystemOcsShellProfile = createAsyncThunk<
  OcsShellProfile,
  string,
  ThunkAPIConfig
>(
  'workflowSetup/getManageAdjustmentsSystemOcsShellProfile',
  async (args, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;
    const resp = await workflowService.getOcsShellProfile(args);

    const state = thunkAPI.getState();
    // Take mapping model
    const ocsShellProfileListMapping = state.mapping?.data?.ocsShellProfile;

    const ocsShellProfile = resp?.data?.shellProfile;
    // Mapping
    const mappedData = mappingDataFromObj(
      ocsShellProfile,
      ocsShellProfileListMapping!
    ) as OcsShellProfile;
    return mappedData;
  }
);
export const getManageAdjustmentsSystemOcsShellProfileBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    getManageAdjustmentsSystemOcsShellProfile.pending,
    draftState => {
      set(draftState.ocsShellProfile, 'loading', true);
      set(draftState.ocsShellProfile, 'error', false);
    }
  );
  builder.addCase(
    getManageAdjustmentsSystemOcsShellProfile.fulfilled,
    draftState => {
      set(draftState.ocsShellProfile, 'loading', false);
      set(draftState.ocsShellProfile, 'error', false);
    }
  );
  builder.addCase(
    getManageAdjustmentsSystemOcsShellProfile.rejected,
    draftState => {
      set(draftState.ocsShellProfile, 'loading', false);
      set(draftState.ocsShellProfile, 'error', true);
    }
  );
};
