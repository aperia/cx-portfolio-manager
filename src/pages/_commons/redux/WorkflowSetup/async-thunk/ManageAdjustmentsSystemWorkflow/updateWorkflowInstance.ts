import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';
import { FormConfigFDSA } from 'pages/WorkflowManageAdjustmentsSystem/ConfigFDSAStep';
import { FormConfigFLSADJ } from 'pages/WorkflowManageAdjustmentsSystem/ConfigFLSADJStep';
import { FormConfigJFS } from 'pages/WorkflowManageAdjustmentsSystem/ConfigJFSStep';
import { FormConfigJQA } from 'pages/WorkflowManageAdjustmentsSystem/ConfigJQAStep';
import { FormGettingStart } from 'pages/WorkflowManageAdjustmentsSystem/GettingStartStep';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { parseConfigurationsValue } from './helper';

export const updateManageAdjustmentsSystemWorkflowInstance = createAsyncThunk<
  any,
  WorkflowSetupThunkArg,
  ThunkAPIConfig
>(
  'workflowSetup/updateManageAdjustmentsSystemWorkflowInstance',
  async (args, thunkAPI) => {
    const { workflowSetupData } = args;
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

    const state = thunkAPI.getState();

    const gettingStart = state.workflowSetup?.workflowFormSteps.forms[
      'gettingStart'
    ] as FormGettingStart;

    // invalid save
    if (
      !gettingStart?.jfs &&
      !gettingStart.flsAdj &&
      !gettingStart.jqa &&
      !gettingStart.fdsa
    ) {
      return;
    }

    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;
    const configJfsForm = state.workflowSetup?.workflowFormSteps.forms[
      'configJfs'
    ] as FormConfigJFS;
    const configFlsAdjForm = state.workflowSetup?.workflowFormSteps.forms[
      'configFlsAdj'
    ] as FormConfigFLSADJ;
    const configJqaForm = state.workflowSetup?.workflowFormSteps.forms[
      'configJqa'
    ] as FormConfigJQA;
    const configFdsaForm = state.workflowSetup?.workflowFormSteps.forms[
      'configFdsa'
    ] as FormConfigFDSA;

    const configurations: Record<string, any> = parseConfigurationsValue(
      configJfsForm,
      configFlsAdjForm,
      configJqaForm,
      configFdsaForm
    );

    const updateRequest: UpdateflowInstanceData = {
      workflowInstanceId: changeForm.workflowInstanceId?.toString(),
      status: 'INCOMPLETE',
      data: {
        workflowSetupData,
        configurations
      }
    };

    await workflowService.updateWorkflowInstanceData(updateRequest);
    return true;
  }
);
export const updateManageAdjustmentsSystemWorkflowInstanceBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    updateManageAdjustmentsSystemWorkflowInstance.pending,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', true);
      set(draftState.workflowInstance, 'error', false);
    }
  );
  builder.addCase(
    updateManageAdjustmentsSystemWorkflowInstance.fulfilled,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', false);
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'isCompletedWorkflow',
        false
      );
    }
  );
  builder.addCase(
    updateManageAdjustmentsSystemWorkflowInstance.rejected,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', true);
    }
  );
};
