import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { mappingDataFromArray } from 'app/helpers';
import { APIMapping } from 'app/services';
import { ProfileTransactions, WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';

export const getManageAdjustmentsSystemProfileTransactions = createAsyncThunk<
  ProfileTransactions[],
  string,
  ThunkAPIConfig
>(
  'workflowSetup/getManageAdjustmentsSystemProfileTransactions',
  async (args, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;
    const resp = await workflowService.getProfileTransactions(args);

    const state = thunkAPI.getState();
    // Take mapping model
    const profileTransactionsListMapping =
      state.mapping?.data?.profileTransactions;

    const transactions = resp?.data?.ocsTransactionActions;
    // Mapping
    const mappedData = mappingDataFromArray(
      transactions,
      profileTransactionsListMapping!
    ) as ProfileTransactions[];

    return mappedData;
  }
);
export const getManageAdjustmentsSystemProfileTransactionsBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    getManageAdjustmentsSystemProfileTransactions.pending,
    draftState => {
      set(draftState.profileTransactions, 'loading', true);
      set(draftState.profileTransactions, 'error', false);
    }
  );
  builder.addCase(
    getManageAdjustmentsSystemProfileTransactions.fulfilled,
    draftState => {
      set(draftState.profileTransactions, 'loading', false);
      set(draftState.profileTransactions, 'error', false);
    }
  );
  builder.addCase(
    getManageAdjustmentsSystemProfileTransactions.rejected,
    draftState => {
      set(draftState.profileTransactions, 'loading', false);
      set(draftState.profileTransactions, 'error', true);
    }
  );
};
