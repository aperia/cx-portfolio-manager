import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { mappingDataFromArray } from 'app/helpers';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import { set } from 'app/_libraries/_dls/lodash';
import get from 'lodash.get';

export const getSupervisorList = createAsyncThunk<any, void, ThunkAPIConfig>(
  'workflowSetup/getSupervisorList',
  async (args, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;
    const state = thunkAPI.getState();
    const response = await workflowService.getSupervisorList({});

    // Mapping
    const supervisorListMapping = state.mapping?.data?.supervisorList;
    const mappedData = mappingDataFromArray(
      response?.data?.supervisors,
      supervisorListMapping!
    );

    return mappedData;
  }
);
export const getSupervisorListBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(getSupervisorList.pending, (draftState, action) => {
    set(draftState.supervisorData, 'loading', true);
    set(draftState.supervisorData, 'error', false);
  });
  builder.addCase(getSupervisorList.fulfilled, (draftState, action) => {
    set(draftState.supervisorData, 'loading', false);
    set(draftState.supervisorData, 'error', false);
    set(draftState.supervisorData, 'supervisorList', action.payload);
  });
  builder.addCase(getSupervisorList.rejected, (draftState, action) => {
    set(draftState.supervisorData, 'loading', false);
    set(draftState.supervisorData, 'error', true);
  });
};
