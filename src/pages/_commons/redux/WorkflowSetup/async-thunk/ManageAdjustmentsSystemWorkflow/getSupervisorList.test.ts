import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  workflowInstance: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      gettingStart: {
        jfs: 'jfs',
        fdsa: 'fdsa'
      },
      changeInformation: {
        changeSetId: '1111',
        workflowInstanceId: '2222',
        name: 'mock name',
        description: 'mock description',
        workflowNote: 'mock note',
        effectiveDate: new Date()
      }
    }
  }
};

describe('redux-store > workflow-setup >  ManageAdjustmentsSystemWorkflow > getSupervisorList', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getSupervisorList.pending('getSupervisorList')
    );
    expect(nextState.profileTransactions).toBeUndefined();
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getSupervisorList.fulfilled([], 'getSupervisorList')
    );
    expect(nextState.supervisorData).toBeUndefined();
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getSupervisorList.rejected(
        new Error('mock error'),
        'getSupervisorList'
      )
    );
    expect(nextState.supervisorData).toBeUndefined();
  });
  it('thunk action > with getSupervisorList', async () => {
    const nextState: any = await actionsWorkflowSetup.getSupervisorList()(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: {
            data: {
              supervisorData: {}
            }
          },
          workflowSetup: {
            ...initReducer
          }
        } as RootState),
      {
        workflowService: {
          getSupervisorList: jest.fn().mockResolvedValue({
            data: {
              supervisorData: []
            }
          })
        }
      }
    );
    expect(nextState.payload).toEqual([]);
  });
});
