import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  workflowInstance: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      gettingStart: {
        jfs: 'jfs',
        fdsa: 'fdsa'
      },
      changeInformation: {
        changeSetId: '1111',
        workflowInstanceId: '2222',
        name: 'mock name',
        description: 'mock description',
        workflowNote: 'mock note',
        effectiveDate: new Date()
      }
    }
  }
};

describe('redux-store > workflow-setup >  ManageAdjustmentsSystemWorkflow > getManageAdjustmentsSystemProfileTransactions', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getManageAdjustmentsSystemProfileTransactions.pending(
        'getManageAdjustmentsSystemProfileTransactions',
        ''
      )
    );
    expect(nextState.profileTransactions).toBeUndefined();
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getManageAdjustmentsSystemProfileTransactions.fulfilled(
        [],
        'getManageAdjustmentsSystemProfileTransactions',
        ''
      )
    );
    expect(nextState.profileTransactions).toBeUndefined();
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getManageAdjustmentsSystemProfileTransactions.rejected(
        new Error('mock error'),
        'getManageAdjustmentsSystemProfileTransactions',
        ''
      )
    );
    expect(nextState.profileTransactions).toBeUndefined();
  });
  it('thunk action > with getManageAdjustmentsSystemProfileTransactions', async () => {
    const nextState: any =
      await actionsWorkflowSetup.getManageAdjustmentsSystemProfileTransactions(
        ''
      )(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            mapping: {
              data: {
                profileTransactions: {}
              }
            },
            workflowSetup: {
              ...initReducer
            }
          } as RootState),
        {
          workflowService: {
            getProfileTransactions: jest.fn().mockResolvedValue({
              data: {
                profileTransactions: []
              }
            })
          }
        }
      );
    expect(nextState.payload).toEqual([]);
  });
});
