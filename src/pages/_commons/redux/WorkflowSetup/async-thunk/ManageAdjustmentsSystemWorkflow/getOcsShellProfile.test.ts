import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  workflowInstance: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      gettingStart: {
        jfs: 'jfs',
        fdsa: 'fdsa'
      },
      changeInformation: {
        changeSetId: '1111',
        workflowInstanceId: '2222',
        name: 'mock name',
        description: 'mock description',
        workflowNote: 'mock note',
        effectiveDate: new Date()
      }
    }
  }
};

describe('redux-store > workflow-setup >  ManageAdjustmentsSystemWorkflow > getManageAdjustmentsSystemOcsShellProfile', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getManageAdjustmentsSystemOcsShellProfile.pending(
        'getManageAdjustmentsSystemOcsShellProfile',
        ''
      )
    );
    expect(nextState.ocsShellProfile).toBeUndefined();
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getManageAdjustmentsSystemOcsShellProfile.fulfilled(
        {
          id: 'string',
          shellName: 'string',
          generalInformation: {
            terminalGroupId: 'string',
            profile: 'string',
            departmentId: 'string',
            managerOperatorId: 'string',
            supervisorOperatorId: 'string',
            operatorId: 'string'
          },
          adjustmentLimitInformation: [
            {
              batchCode: 'string',
              adjustmentTypeId: 'string',
              shellMaximumAmount: 123,
              managerDepartmentMaximumAmount: 123,
              actionCode: 'string'
            }
          ]
        },
        'getManageAdjustmentsSystemOcsShellProfile',
        ''
      )
    );
    expect(nextState.ocsShellProfile).toBeUndefined();
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getManageAdjustmentsSystemOcsShellProfile.rejected(
        new Error('mock error'),
        'getManageAdjustmentsSystemOcsShellProfile',
        ''
      )
    );
    expect(nextState.ocsShellProfile).toBeUndefined();
  });
  it('thunk action > with getManageAdjustmentsSystemOcsShellProfile', async () => {
    const nextState: any =
      await actionsWorkflowSetup.getManageAdjustmentsSystemOcsShellProfile('')(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            mapping: {
              data: {
                ocsShellProfile: {}
              }
            },
            workflowSetup: {
              ...initReducer
            }
          } as RootState),
        {
          workflowService: {
            getOcsShellProfile: jest.fn().mockResolvedValue({
              data: {
                ocsShellProfile: []
              }
            })
          }
        }
      );
    expect(nextState.payload).toEqual({});
  });
});
