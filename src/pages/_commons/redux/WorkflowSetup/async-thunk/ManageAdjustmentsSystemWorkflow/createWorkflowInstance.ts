import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';
import { FormConfigFDSA } from 'pages/WorkflowManageAdjustmentsSystem/ConfigFDSAStep';
import { FormConfigFLSADJ } from 'pages/WorkflowManageAdjustmentsSystem/ConfigFLSADJStep';
import { FormConfigJFS } from 'pages/WorkflowManageAdjustmentsSystem/ConfigJFSStep';
import { FormConfigJQA } from 'pages/WorkflowManageAdjustmentsSystem/ConfigJQAStep';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { workflowSetupTemplateIdSelector } from '../../select-hooks';
import { parseConfigurationsValue } from './helper';

export const createManageAdjustmentsSystemWorkflowInstance = createAsyncThunk<
  any,
  WorkflowSetupThunkArg,
  ThunkAPIConfig
>(
  'workflowSetup/createManageAdjustmentsSystemWorkflowInstance',
  async (args, thunkAPI) => {
    const { workflowSetupData } = args;
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

    const state = thunkAPI.getState();

    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;

    const workflowTemplateId = workflowSetupTemplateIdSelector(state);

    const changeId = changeForm.changeSetId;
    let workflowInstanceId = changeForm.workflowInstanceId?.toString();

    if (workflowInstanceId && changeId) {
      await workflowService.updateWorkflowInstance({
        workflowInstanceId,
        changeSetId: changeId.toString(),
        workflowInstanceDetails: {
          note: changeForm.workflowNote
        }
      } as any);
    } else {
      const workflowRes = await workflowService.createWorkflowInstance({
        workflowInstanceDetails: {
          note: changeForm.workflowNote
        },
        workflowTemplateId: workflowTemplateId || '',
        changeSetId: changeId?.toString()
      });

      workflowInstanceId =
        workflowRes?.data?.workflowInstanceDetails?.id?.toString();
    }

    const configJfsForm = state.workflowSetup?.workflowFormSteps.forms[
      'configJfs'
    ] as FormConfigJFS;
    const configFlsAdjForm = state.workflowSetup?.workflowFormSteps.forms[
      'configFlsAdj'
    ] as FormConfigFLSADJ;
    const configJqaForm = state.workflowSetup?.workflowFormSteps.forms[
      'configJqa'
    ] as FormConfigJQA;
    const configFdsaForm = state.workflowSetup?.workflowFormSteps.forms[
      'configFdsa'
    ] as FormConfigFDSA;

    const configurations: Record<string, any> = parseConfigurationsValue(
      configJfsForm,
      configFlsAdjForm,
      configJqaForm,
      configFdsaForm
    );

    const updateRequest: UpdateflowInstanceData = {
      workflowInstanceId: workflowInstanceId?.toString(),
      status: 'INCOMPLETE',
      data: {
        workflowSetupData,
        configurations
      }
    };
    await workflowService.updateWorkflowInstanceData(updateRequest);

    return { changeId, workflowInstanceId };
  }
);
export const createManageAdjustmentsSystemWorkflowInstanceBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    createManageAdjustmentsSystemWorkflowInstance.pending,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', true);
      set(draftState.workflowInstance, 'error', false);
    }
  );
  builder.addCase(
    createManageAdjustmentsSystemWorkflowInstance.fulfilled,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', false);
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'changeSetId',
        action.payload?.changeId
      );
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'workflowInstanceId',
        action.payload?.workflowInstanceId
      );
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'isCompletedWorkflow',
        false
      );
    }
  );
  builder.addCase(
    createManageAdjustmentsSystemWorkflowInstance.rejected,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', true);
    }
  );
};
