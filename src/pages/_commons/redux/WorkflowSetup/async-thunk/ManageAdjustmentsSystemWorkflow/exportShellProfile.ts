import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { MIME_TYPE } from 'app/constants/mine-type';
import { mappingDataFromObj } from 'app/helpers';
import { base64toBlob, downloadBlobFile } from 'app/helpers/fileUtilities';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';

export const exportShellProfile = createAsyncThunk<any, string, ThunkAPIConfig>(
  'workflowSetup/exportShellProfile',
  async (args, thunkAPI) => {
    const state = thunkAPI.getState();
    const downloadFileMapping = state.mapping?.data?.downloadTemplateFile;
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

    const response = await workflowService.exportShellProfile(args);

    const attachmentData = response?.data?.attachment;
    const file = mappingDataFromObj(attachmentData, downloadFileMapping!);
    const {
      fileStream = '',
      mimeType: mimeTypeDownload = '',
      filename: filenameDownload = ''
    } = file;

    let fileName = filenameDownload;
    if (filenameDownload.split('.').length < 2) {
      const ext = MIME_TYPE[mimeTypeDownload]?.extensions[0] || '';
      fileName = `${filenameDownload}.${ext}`;
    }
    const blol = base64toBlob(fileStream, mimeTypeDownload);
    downloadBlobFile(blol, fileName);

    return true;
  }
);

export const exportShellProfileBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(exportShellProfile.pending, draftState => {
    set(draftState.exportShellProfile, 'loading', true);
  });
  builder.addCase(exportShellProfile.fulfilled, draftState => {
    set(draftState.exportShellProfile, 'loading', false);
    set(draftState.exportShellProfile, 'error', false);
  });
  builder.addCase(exportShellProfile.rejected, draftState => {
    set(draftState.exportShellProfile, 'loading', false);
    set(draftState.exportShellProfile, 'error', true);
  });
};
