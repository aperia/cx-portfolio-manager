import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  workflowInstance: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      gettingStart: {
        jfs: 'jfs',
        fdsa: 'fdsa'
      },
      changeInformation: {
        changeSetId: '1111',
        workflowInstanceId: '2222',
        name: 'mock name',
        description: 'mock description',
        workflowNote: 'mock note',
        effectiveDate: new Date()
      }
    }
  }
};

describe('redux-store > workflow-setup >  ManageAdjustmentsSystemWorkflow > getManageAdjustmentsSystemQueueProfile', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getManageAdjustmentsSystemQueueProfile.pending(
        'getManageAdjustmentsSystemQueueProfile',
        ''
      )
    );
    expect(nextState.profileTransactions).toBeUndefined();
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getManageAdjustmentsSystemQueueProfile.fulfilled(
        {
          generalInformation: {
            queueOwner: 'queue 1',
            queueOwnerId: 'string',
            managerId: 'string',
            terminalGroupId: 'string'
          },
          alternateAccessShellNames: []
        },
        'getManageAdjustmentsSystemQueueProfile',
        ''
      )
    );
    expect(nextState.queueProfile).toBeUndefined();
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getManageAdjustmentsSystemQueueProfile.rejected(
        new Error('mock error'),
        'getManageAdjustmentsSystemQueueProfile',
        ''
      )
    );
    expect(nextState.queueProfile).toBeUndefined();
  });
  it('thunk action > with getManageAdjustmentsSystemQueueProfile', async () => {
    const nextState: any =
      await actionsWorkflowSetup.getManageAdjustmentsSystemQueueProfile('')(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            mapping: {
              data: {
                queueProfile: {}
              }
            },
            workflowSetup: {
              ...initReducer
            }
          } as RootState),
        {
          workflowService: {
            getQueueProfile: jest.fn().mockResolvedValue({
              data: {
                queueProfile: []
              }
            })
          }
        }
      );
    expect(nextState.payload).toEqual({});
  });
});
