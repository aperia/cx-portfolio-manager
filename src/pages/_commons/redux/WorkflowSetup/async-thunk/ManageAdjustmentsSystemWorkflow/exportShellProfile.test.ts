import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

jest.mock('app/helpers/fileUtilities.ts', () => ({
  ...jest.requireActual('app/helpers/fileUtilities.ts'),
  downloadBlobFile: jest.fn()
}));

const initReducer: any = {
  workflowInstance: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      gettingStart: {
        jfs: 'jfs',
        fdsa: 'fdsa'
      },
      changeInformation: {
        changeSetId: '1111',
        workflowInstanceId: '2222',
        name: 'mock name',
        description: 'mock description',
        workflowNote: 'mock note',
        effectiveDate: new Date()
      }
    }
  }
};

describe('redux-store > workflow-setup >  ManageAdjustmentsSystemWorkflow > exportShellProfile', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.exportShellProfile.pending('exportShellProfile', '')
    );
    expect(nextState.exportShellProfile).toBeUndefined();
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.exportShellProfile.fulfilled(
        {
          data: {
            attachment: {
              attachment: {
                attachmentId: '001',
                filename: 'name.ext.png',
                mimeType: 'type',
                fileSize: 100,
                fileStream: 'steam',
                createdDate: '2021-01-28T11:16:14.3536967+07:00'
              }
            }
          }
        },
        'exportShellProfile',
        ''
      )
    );
    expect(nextState.exportShellProfile).toBeUndefined();
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.exportShellProfile.rejected(
        new Error('mock error'),
        'exportShellProfile',
        ''
      )
    );
    expect(nextState.profileTransactions).toBeUndefined();
  });
  it('thunk action > with exportShellProfile', async () => {
    const nextState: any = await actionsWorkflowSetup.exportShellProfile('')(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: {
            data: {}
          },
          workflowSetup: {
            ...initReducer
          }
        } as RootState),
      {
        workflowService: {
          exportShellProfile: jest.fn().mockResolvedValue({
            data: {
              attachment: {
                attachmentId: '001',
                filename: 'name',
                mimeType: 'type',
                fileSize: 100,
                fileStream: 'steam',
                createdDate: '2021-01-28T11:16:14.3536967+07:00'
              }
            }
          })
        }
      }
    );

    expect(nextState.payload).toEqual(true);
  });

  it('thunk action > with exportShellProfile file name split dot .length > 2', async () => {
    const nextState: any = await actionsWorkflowSetup.exportShellProfile('')(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: {
            data: {
              downloadTemplateFile: {
                attachmentId: 'attachmentId',
                filename: 'filename',
                mimeType: 'mimeType',
                fileSize: 'fileSize',
                fileStream: 'fileStream',
                createdDate: 'createdDate'
              }
            }
          },
          workflowSetup: {
            ...initReducer
          }
        } as RootState),
      {
        workflowService: {
          exportShellProfile: jest.fn().mockResolvedValue({
            data: {
              attachment: {
                attachmentId: '001',
                filename: 'name.ext.ext.png',
                mimeType: 'type',
                fileSize: 100,
                fileStream: 'steam',
                createdDate: '2021-01-28T11:16:14.3536967+07:00'
              }
            }
          })
        }
      }
    );
    expect(nextState.payload).toEqual(undefined);
  });
});
