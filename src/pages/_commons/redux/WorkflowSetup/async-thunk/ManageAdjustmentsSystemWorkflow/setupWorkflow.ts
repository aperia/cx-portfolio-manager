import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types/workflowSetupState';
import get from 'lodash.get';
import set from 'lodash.set';
import { FormConfigFDSA } from 'pages/WorkflowManageAdjustmentsSystem/ConfigFDSAStep';
import { FormConfigFLSADJ } from 'pages/WorkflowManageAdjustmentsSystem/ConfigFLSADJStep';
import { FormConfigJFS } from 'pages/WorkflowManageAdjustmentsSystem/ConfigJFSStep';
import { FormConfigJQA } from 'pages/WorkflowManageAdjustmentsSystem/ConfigJQAStep';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { parseConfigurationsValue } from './helper';

export const setupManageAdjustmentsSystemWorkflow = createAsyncThunk<
  any,
  WorkflowSetupThunkArg,
  ThunkAPIConfig
>(
  'workflowSetup/setupManageAdjustmentsSystemWorkflow',
  async (args, thunkAPI) => {
    const { workflowSetupData } = args;
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

    const state = thunkAPI.getState();

    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;
    const configJfsForm = state.workflowSetup?.workflowFormSteps.forms[
      'configJfs'
    ] as FormConfigJFS;
    const configFlsAdjForm = state.workflowSetup?.workflowFormSteps.forms[
      'configFlsAdj'
    ] as FormConfigFLSADJ;
    const configJqaForm = state.workflowSetup?.workflowFormSteps.forms[
      'configJqa'
    ] as FormConfigJQA;
    const configFdsaForm = state.workflowSetup?.workflowFormSteps.forms[
      'configFdsa'
    ] as FormConfigFDSA;

    const configurations: Record<string, any> = parseConfigurationsValue(
      configJfsForm,
      configFlsAdjForm,
      configJqaForm,
      configFdsaForm
    );

    const updateRequest: UpdateflowInstanceData = {
      workflowInstanceId: changeForm.workflowInstanceId?.toString(),
      status: 'COMPLETE',
      data: {
        workflowSetupData,
        configurations
      }
    };

    await workflowService.updateWorkflowInstanceData(updateRequest);

    return true;
  }
);
export const setupManageAdjustmentsSystemWorkflowBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(setupManageAdjustmentsSystemWorkflow.pending, draftState => {
    set(draftState.workflowSetup, 'loading', true);
  });
  builder.addCase(
    setupManageAdjustmentsSystemWorkflow.fulfilled,
    (draftState, action) => {
      set(draftState.workflowSetup, 'loading', false);
      set(draftState.workflowSetup, 'error', false);
    }
  );
  builder.addCase(setupManageAdjustmentsSystemWorkflow.rejected, draftState => {
    set(draftState.workflowSetup, 'loading', false);
    set(draftState.workflowSetup, 'error', true);
  });
};
