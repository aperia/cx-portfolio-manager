import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  workflowInstance: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      gettingStart: {
        jfs: 'jfs',
        fdsa: 'fdsa'
      },
      changeInformation: {
        changeSetId: '1111',
        workflowInstanceId: '2222',
        name: 'mock name',
        description: 'mock description',
        workflowNote: 'mock note',
        effectiveDate: new Date()
      }
    }
  }
};

jest.mock('app/helpers', () => {
  const modules = jest.requireActual('app/helpers');
  return {
    ...modules,
    formatTimeDefault: () => '01/01/2021'
  };
});

describe('redux-store > workflow-setup > ManageAdjustmentsSystemWorkflow > getShellList', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getShellList.pending('getShellList', undefined)
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getShellList.fulfilled(
        true,
        'getShellList',
        undefined
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getShellList.rejected(
        new Error('mock error'),
        'getShellList',
        undefined
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(false);
  });

  it('thunk action', async () => {
    const mockRes = { data: { workflowSetupData: [] } };
    const updateWorkflowDataFn = jest.fn().mockResolvedValue(mockRes);

    const nextState: any = await actionsWorkflowSetup.getShellList()(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          workflowSetup: {
            ...initReducer
          },
          mapping: {
            data: {
              ocsShell: {
                shellName: 'shellName',
                transactionProfile: 'transactionProfile',
                fieldProfile: 'fieldProfile'
              }
            }
          }
        } as RootState),
      {
        workflowService: {
          updateWorkflowInstanceData: updateWorkflowDataFn,
          getOCSShellList: jest.fn().mockResolvedValue({
            data: {
              ocsShells: [
                {
                  shellName: 'shellName',
                  transactionProfile: 'transactionProfile',
                  fieldProfile: 'fieldProfile'
                }
              ]
            }
          })
        }
      }
    );
    expect(nextState?.payload?.workflowSetupData).toBeUndefined();
  });
});
