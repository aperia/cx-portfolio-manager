import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';
import { defaultWorkflowBulkPCFilter } from '../../reducers';

export interface GetWorkflowTextAreasArgs {
  textArea: string;
}
export const getBulkArchivePCFWorkflowTextAreas = createAsyncThunk<
  any,
  GetWorkflowTextAreasArgs,
  ThunkAPIConfig
>(
  'workflowSetup/getBulkArchivePCFWorkflowTextAreas',
  async (
    args: {
      textArea: string;
    },
    thunkAPI
  ) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;
    const resp = await workflowService.getTextIDListForArchive(args.textArea);

    const textAreaList = resp?.data?.textIDs;

    return {
      textAreas: textAreaList,
      page: defaultWorkflowBulkPCFilter.page,
      expandingTextArea: ''
    };
  }
);
export const getBulkArchivePCFWorkflowTextAreasBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    getBulkArchivePCFWorkflowTextAreas.pending,
    (draftState, action) => {
      set(draftState.workflowTextAreas, 'loading', true);
      set(draftState.workflowTextAreas, 'error', false);
    }
  );
  builder.addCase(
    getBulkArchivePCFWorkflowTextAreas.fulfilled,
    (draftState, action) => {
      return {
        ...draftState,
        workflowTextAreas: {
          ...draftState.workflowTextAreas,
          textAreas: action.payload.textAreas,
          dataFilter: {
            ...draftState.workflowTextAreas.dataFilter,
            page: action.payload.page
          },
          loading: false,
          error: false
        }
      };
    }
  );
  builder.addCase(
    getBulkArchivePCFWorkflowTextAreas.rejected,
    (draftState, action) => {
      set(draftState.workflowTextAreas, 'loading', false);
      set(draftState.workflowTextAreas, 'error', true);
    }
  );
};
