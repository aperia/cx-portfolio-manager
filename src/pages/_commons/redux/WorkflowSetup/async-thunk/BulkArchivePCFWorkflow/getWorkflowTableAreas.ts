import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';
import { defaultWorkflowBulkPCFilter } from '../../reducers';

export interface GetWorkflowTableAreasArgs {
  tableArea: string;
}
export const getBulkArchivePCFWorkflowTableAreas = createAsyncThunk<
  any,
  GetWorkflowTableAreasArgs,
  ThunkAPIConfig
>(
  'workflowSetup/getBulkArchivePCFWorkflowTableAreas',
  async (
    args: {
      tableArea: string;
    },
    thunkAPI
  ) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;
    const resp = await workflowService.getTableListForArchive(args.tableArea);

    const tableAreaList = resp?.data?.tables;

    return {
      tableAreas: tableAreaList,
      page: defaultWorkflowBulkPCFilter.page,
      expandingTableArea: ''
    };
  }
);
export const getBulkArchivePCFWorkflowTableAreasBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    getBulkArchivePCFWorkflowTableAreas.pending,
    (draftState, action) => {
      set(draftState.workflowTableAreas, 'loading', true);
      set(draftState.workflowTableAreas, 'error', false);
    }
  );
  builder.addCase(
    getBulkArchivePCFWorkflowTableAreas.fulfilled,
    (draftState, action) => {
      return {
        ...draftState,
        workflowTableAreas: {
          ...draftState.workflowTableAreas,
          tableAreas: action.payload.tableAreas,
          dataFilter: {
            ...draftState.workflowTableAreas.dataFilter,
            page: action.payload.page
          },
          loading: false,
          error: false
        }
      };
    }
  );
  builder.addCase(
    getBulkArchivePCFWorkflowTableAreas.rejected,
    (draftState, action) => {
      set(draftState.workflowTableAreas, 'loading', false);
      set(draftState.workflowTableAreas, 'error', true);
    }
  );
};
