import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';
import { FormSelectMethod } from 'pages/WorkflowBulkArchivePCF/SelectPricingMethodStep';
import { FormSelectStrategy } from 'pages/WorkflowBulkArchivePCF/SelectPricingStrategyStep';
import { FormSelectTableArea } from 'pages/WorkflowBulkArchivePCF/SelectTableArea';
import { FormSelectTextArea } from 'pages/WorkflowBulkArchivePCF/SelectTextArea';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { workflowSetupTemplateIdSelector } from '../../select-hooks';

export const createBulkArchivePCFWorkflowInstance = createAsyncThunk<
  any,
  WorkflowSetupThunkArg,
  ThunkAPIConfig
>(
  'workflowSetup/createBulkArchivePCFWorkflowInstance',
  async (args, thunkAPI) => {
    const { workflowSetupData } = args;
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

    const state = thunkAPI.getState();

    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;

    const tableAreaPCF = state.workflowSetup?.workflowFormSteps.forms[
      'selectTableAreaToArchivePCF'
    ] as FormSelectTableArea;

    const selectedListTable = tableAreaPCF?.selectedList as MagicKeyValue;

    const tableIDs = tableAreaPCF?.tableIDs?.map(prsMethod => {
      const selectedList = selectedListTable[prsMethod?.code] as string[];
      return {
        ...prsMethod,
        methodList: prsMethod?.methodList?.map(method => {
          return {
            ...method,
            selected:
              selectedList?.findIndex(item => item === method?.id) !== -1
          };
        })
      };
    });

    const textAreaPCF = state.workflowSetup?.workflowFormSteps.forms[
      'selectTextAreaToArchivePCF'
    ] as FormSelectTextArea;

    const selectedListText = textAreaPCF?.selectedList as MagicKeyValue;

    const textIDs = textAreaPCF?.textIDs?.map(prsMethod => {
      const selectedList = selectedListText[prsMethod?.code] as string[];
      return {
        ...prsMethod,
        textIdList: prsMethod?.textIdList?.map(method => {
          return {
            ...method,
            selected:
              selectedList?.findIndex(item => item === method?.id) !== -1
          };
        })
      };
    });

    const prs = state.workflowSetup?.workflowFormSteps.forms[
      'selectPricingStrategiesToArchive'
    ] as FormSelectStrategy;

    const selectedListPrs = prs?.selectedList as MagicKeyValue;

    const pricingStrategies = prs?.pricingStrategies?.map(prsMethod => {
      const selectedList = selectedListPrs[prsMethod?.code] as string[];
      return {
        ...prsMethod,
        methodList: prsMethod?.methodList?.map(method => {
          return {
            ...method,
            selected:
              selectedList?.findIndex(item => item === method?.id) !== -1
          };
        })
      };
    });

    const prm = state.workflowSetup?.workflowFormSteps.forms[
      'selectPricingMethodsToArchive'
    ] as FormSelectMethod;
    const selectedListPrm = prm?.selectedList as MagicKeyValue;

    const pricingMethods = prm?.pricingMethods?.map(prmMethod => {
      const selectedList = selectedListPrm[prmMethod?.code] as string[];
      return {
        ...prmMethod,
        methodList: prmMethod?.methodList?.map(method => {
          return {
            ...method,
            selected:
              selectedList?.findIndex(item => item === method?.id) !== -1
          };
        })
      };
    });

    const workflowTemplateId = workflowSetupTemplateIdSelector(state);

    const changeId = changeForm.changeSetId;
    let workflowInstanceId = changeForm.workflowInstanceId?.toString();

    if (workflowInstanceId && changeId) {
      await workflowService.updateWorkflowInstance({
        workflowInstanceId,
        changeSetId: changeId.toString(),
        workflowInstanceDetails: {
          note: changeForm.workflowNote
        }
      } as any);
    } else {
      const workflowRes = await workflowService.createWorkflowInstance({
        workflowInstanceDetails: {
          note: changeForm.workflowNote
        },
        workflowTemplateId: workflowTemplateId || '',
        changeSetId: changeId?.toString()
      });

      workflowInstanceId =
        workflowRes?.data?.workflowInstanceDetails?.id?.toString();
    }

    const updateRequest: UpdateflowInstanceData = {
      workflowInstanceId: workflowInstanceId?.toString(),
      status: 'INCOMPLETE',
      data: {
        workflowSetupData,
        configurations: {
          pricingMethods,
          pricingStrategies,
          textIDs,
          tableIDs
        }
      }
    };
    await workflowService.updateWorkflowInstanceData(updateRequest);

    return { changeId, workflowInstanceId };
  }
);
export const createBulkArchivePCFWorkflowInstanceBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    createBulkArchivePCFWorkflowInstance.pending,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', true);
      set(draftState.workflowInstance, 'error', false);
    }
  );
  builder.addCase(
    createBulkArchivePCFWorkflowInstance.fulfilled,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', false);
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'changeSetId',
        action.payload?.changeId
      );
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'workflowInstanceId',
        action.payload?.workflowInstanceId
      );
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'isCompletedWorkflow',
        false
      );
    }
  );
  builder.addCase(
    createBulkArchivePCFWorkflowInstance.rejected,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', true);
    }
  );
};
