import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  workflowInstance: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      changeInformation: {
        name: 'mock name',
        description: 'mock description',
        effectiveDate: new Date()
      },
      selectTableAreaToArchivePCF: {
        selectedList: { AO: ['AO_0_1650942430380'] },
        tableIDs: [
          {
            code: 'AO',
            description: 'Agent Only',
            methodList: [
              {
                id: 'AO_0_1650942430380',
                'effective.date': '2022-07-03T00:00:00',
                tableId: 'TEMPO',
                effectiveDate: '2022-07-03T00:00:00',
                rowId: 'AO_0_1650942430380',
                'table.id': 'TEMPO'
              }
            ],
            text: 'AO - Agent Only',
            value: 'AO'
          }
        ]
      },
      selectTextAreaToArchivePCF: {
        selectedList: { AO: ['AO_0_1650942425741'] },
        textIDs: [
          {
            value: 'AO',
            code: 'AO',
            description: 'Agent Only',
            text: 'AO - Agent Only',
            textIdList: [
              {
                'effective.date': '2022-07-03T00:00:00',
                effectiveDate: '2022-07-03T00:00:00',
                id: 'AO_0_1650942425741',
                'language.code': 'EN',
                languageCode: 'EN',
                rowId: 'AO_0_1650942425741',
                'text.id': 'TEMPO',
                'text.type': 'CO',
                textId: 'TEMPO',
                textType: 'CO'
              }
            ]
          }
        ]
      },
      selectPricingStrategiesToArchive: {
        selectedList: { CO: ['CO_0_1650942420669'] },
        pricingStrategies: [
          {
            code: 'CO',
            description: 'Collections',
            text: 'CO - Collections',
            value: 'CO',
            methodList: [
              {
                'effective.date': '2022-01-13T00:00:00',
                effectiveDate: '2022-01-13T00:00:00',
                id: 'CO_0_1650942420669',
                numberOfVersions: 0,
                relatedPricingStrategies: 0,
                rowId: 'CO_0_1650942420669',
                'strategy.name': 'LP6UTIXX',
                strategyName: 'LP6UTIXX',
                originalStrategy: {
                  'effective.date': '2022-01-13T00:00:00',
                  effectiveDate: '2022-01-13T00:00:00',
                  id: 'CO_0_1650942420669',
                  numberOfVersions: 0,
                  relatedPricingStrategies: 0,
                  rowId: 'CO_0_1650942420669',
                  'strategy.name': 'LP6UTI'
                }
              }
            ]
          }
        ]
      },
      selectPricingMethodsToArchive: {
        selectedList: { 'CP IC IP': ['CP IC IP_0_1650942412732'] },
        pricingMethods: [
          {
            code: 'CP IC IP',
            value: 'CP IC IP',
            methodList: [
              {
                comment: 'comment CP IC IP',
                'effective.date': '2022-04-21',
                effectiveDate: '2022-04-21',
                id: 'CP IC IP_0_1650942412732',
                'method.name': 'MTHTTT23',
                name: 'MTHTTT23',
                numberOfVersions: 1
              }
            ]
          }
        ]
      }
    }
  }
};

describe('redux-store > workflow-setup > BulkArchivePCFWorkflow > createWorkflowInstance', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.createBulkArchivePCFWorkflowInstance.pending(
        'createWorkflowInstance',
        {}
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.createBulkArchivePCFWorkflowInstance.fulfilled(
        {},
        'createWorkflowInstance',
        {}
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.createBulkArchivePCFWorkflowInstance.rejected(
        new Error('mock error'),
        'createWorkflowInstance',
        {}
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(true);
  });

  it('thunk action > without workflowInstanceId & changeId', async () => {
    const nextState: any =
      await actionsWorkflowSetup.createBulkArchivePCFWorkflowInstance({
        workflowSetupData: []
      })(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer
            }
          } as RootState),
        {
          workflowService: {
            createWorkflowInstance: jest.fn().mockResolvedValue({
              data: { workflowInstanceDetails: { id: '1111' } }
            }),
            updateWorkflowInstanceData: jest.fn()
          },
          changeService: {
            createChangeSet: jest.fn().mockResolvedValue({
              data: { id: undefined }
            })
          }
        }
      );

    expect(nextState.payload).toEqual({
      changeId: undefined,
      workflowInstanceId: '1111'
    });
  });

  it('thunk action > with workflowInstanceId & changeId', async () => {
    const state = {
      workflowSetup: {
        workflow: {
          id: undefined
        }
      },
      workflowFormSteps: {
        forms: {
          changeInformation: {
            changeSetId: '2222',
            workflowInstanceId: '1111',
            name: 'mock name',
            description: 'mock description',
            effectiveDate: new Date()
          },
          gettingStart: {
            returnedCheckCharges: true,
            lateCharges: true
          }
        }
      }
    };
    const nextState: any =
      await actionsWorkflowSetup.createBulkArchivePCFWorkflowInstance({
        workflowSetupData: []
      })(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...state
            }
          } as RootState),
        {
          workflowService: {
            updateWorkflowInstance: jest.fn(),
            updateWorkflowInstanceData: jest.fn()
          }
        }
      );
    expect(nextState.payload).toEqual({
      changeId: '2222',
      workflowInstanceId: '1111'
    });
  });

  it('thunk action > with existing changeId ', async () => {
    const state = {
      workflowSetup: {
        workflow: {
          id: undefined
        }
      },
      workflowFormSteps: {
        forms: {
          changeInformation: {
            changeSetId: '2222',
            name: 'mock name',
            description: 'mock description',
            effectiveDate: new Date()
          }
        }
      }
    };
    const nextState: any =
      await actionsWorkflowSetup.createBulkArchivePCFWorkflowInstance({
        workflowSetupData: []
      })(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...state
            }
          } as RootState),
        {
          workflowService: {
            createWorkflowInstance: jest.fn().mockResolvedValue({
              data: { workflowInstanceDetails: { id: '1111' } }
            }),
            updateWorkflowInstanceData: jest
              .fn()
              .mockResolvedValue({ data: { workflowSetupData: [] } })
          }
        }
      );
    expect(nextState.payload).toEqual({
      changeId: '2222',
      workflowInstanceId: '1111'
    });
  });
});
