import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { OrderBy, WorkflowStrategiesSortByFields } from 'app/constants/enums';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import _orderBy from 'lodash.orderby';
import set from 'lodash.set';
import { defaultWorkflowBulkPCFilter } from '../../reducers';

export interface GetWorkflowStrategiesArgs {
  strategyArea: string;
}
export const getBulkArchivePCFWorkflowStrategies = createAsyncThunk<
  any,
  GetWorkflowStrategiesArgs,
  ThunkAPIConfig
>(
  'workflowSetup/getBulkArchivePCFWorkflowStrategies',
  async (
    args: {
      strategyArea: string;
    },
    thunkAPI
  ) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;
    const resp = await workflowService.getWorkflowStrategies(args.strategyArea);

    const strategyList = resp?.data?.pricingStrategies;

    const countMappedData = strategyList?.map(m => {
      return {
        ...m,
        versions: _orderBy(
          [WorkflowStrategiesSortByFields.EFFECTIVE_DATE],
          [OrderBy.DESC]
        ),
        numberOfVersions: m.versions?.length || 0,
        relatedPricingStrategies: m.pricingStrategies?.length || 0
      };
    });

    return {
      strategies: countMappedData,
      page: defaultWorkflowBulkPCFilter.page,
      expandingStrategy: ''
    };
  }
);
export const getBulkArchivePCFWorkflowStrategiesBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    getBulkArchivePCFWorkflowStrategies.pending,
    (draftState, action) => {
      set(draftState.workflowStrategies, 'loading', true);
      set(draftState.workflowStrategies, 'error', false);
    }
  );
  builder.addCase(
    getBulkArchivePCFWorkflowStrategies.fulfilled,
    (draftState, action) => {
      return {
        ...draftState,
        workflowStrategies: {
          ...draftState.workflowStrategies,
          strategies: action.payload.strategies,
          dataFilter: {
            ...draftState.workflowStrategies.dataFilter,
            page: action.payload.page
          },
          loading: false,
          error: false
        }
      };
    }
  );
  builder.addCase(
    getBulkArchivePCFWorkflowStrategies.rejected,
    (draftState, action) => {
      set(draftState.workflowStrategies, 'loading', false);
      set(draftState.workflowStrategies, 'error', true);
    }
  );
};
