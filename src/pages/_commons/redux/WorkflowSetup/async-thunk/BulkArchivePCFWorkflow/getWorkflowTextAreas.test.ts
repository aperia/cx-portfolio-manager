import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';
import { getBulkArchivePCFWorkflowTextAreas } from './getWorkflowTextAreas';

const initReducer: any = {
  workflowTextAreas: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  }
};

describe('redux-store > workflow-setup > BulkArchivePCFWorkflow > getBulkArchivePCFWorkflowTextAreas', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getBulkArchivePCFWorkflowTextAreas.pending(
        'getBulkArchivePCFWorkflowTextAreas',
        {
          textArea: ''
        }
      )
    );
    expect(nextState.workflowTextAreas.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getBulkArchivePCFWorkflowTextAreas.fulfilled(
        true,
        'getBulkArchivePCFWorkflowTextAreas',
        {
          textArea: ''
        }
      )
    );
    expect(nextState.workflowTextAreas.loading).toEqual(false);
    expect(nextState.workflowTextAreas.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getBulkArchivePCFWorkflowTextAreas.rejected(
        new Error('mock error'),
        'getBulkArchivePCFWorkflowTextAreas',
        { textArea: '' }
      )
    );
    expect(nextState.workflowTextAreas.loading).toEqual(false);
    expect(nextState.workflowTextAreas.error).toEqual(true);
  });

  it('thunk action', async () => {
    const nextState: any = await getBulkArchivePCFWorkflowTextAreas({
      textArea: ''
    })(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          workflowSetup: {
            ...initReducer
          }
        } as RootState),
      {
        workflowService: {
          getTextIDListForArchive: jest.fn().mockResolvedValue({
            data: {
              textIDs: [{ test: '1' }],
              total: 10
            }
          })
        }
      }
    );
    expect(nextState?.payload?.workflowSetupData).toBeUndefined();
  });
});
