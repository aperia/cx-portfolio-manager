import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types/workflowSetupState';
import get from 'lodash.get';
import set from 'lodash.set';
import { FormSelectMethod } from 'pages/WorkflowBulkArchivePCF/SelectPricingMethodStep';
import { FormSelectStrategy } from 'pages/WorkflowBulkArchivePCF/SelectPricingStrategyStep';
import { FormSelectTableArea } from 'pages/WorkflowBulkArchivePCF/SelectTableArea';
import { FormSelectTextArea } from 'pages/WorkflowBulkArchivePCF/SelectTextArea';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';

export const setupBulkArchivePCFWorkflowInstance = createAsyncThunk<
  any,
  WorkflowSetupThunkArg,
  ThunkAPIConfig
>(
  'workflowSetup/setupBulkArchivePCFWorkflowInstance',
  async (args, thunkAPI) => {
    const { workflowSetupData } = args;
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

    const state = thunkAPI.getState();

    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;

    const tableAreaPCF = state.workflowSetup?.workflowFormSteps.forms[
      'selectTableAreaToArchivePCF'
    ] as FormSelectTableArea;

    const selectedListTable = tableAreaPCF?.selectedList as MagicKeyValue;

    const tableIDs = tableAreaPCF?.tableIDs?.map(prsMethod => {
      const selectedList = selectedListTable[prsMethod?.code] as string[];
      return {
        ...prsMethod,
        methodList: prsMethod?.methodList?.map(method => {
          return {
            ...method,
            selected:
              selectedList?.findIndex(item => item === method?.id) !== -1
          };
        })
      };
    });

    const textAreaPCF = state.workflowSetup?.workflowFormSteps.forms[
      'selectTextAreaToArchivePCF'
    ] as FormSelectTextArea;

    const selectedListText = textAreaPCF?.selectedList as MagicKeyValue;

    const textIDs = textAreaPCF?.textIDs?.map(prsMethod => {
      const selectedList = selectedListText[prsMethod?.code] as string[];
      return {
        ...prsMethod,
        textIdList: prsMethod?.textIdList?.map(method => {
          return {
            ...method,
            selected:
              selectedList?.findIndex(item => item === method?.id) !== -1
          };
        })
      };
    });

    const prs = state.workflowSetup?.workflowFormSteps.forms[
      'selectPricingStrategiesToArchive'
    ] as FormSelectStrategy;

    const selectedListPrs = prs?.selectedList as MagicKeyValue;

    const pricingStrategies = prs?.pricingStrategies?.map(prsMethod => {
      const selectedList = selectedListPrs[prsMethod?.code] as string[];
      return {
        ...prsMethod,
        methodList: prsMethod?.methodList?.map(method => {
          return {
            ...method,
            selected:
              selectedList?.findIndex(item => item === method?.id) !== -1
          };
        })
      };
    });

    const prm = state.workflowSetup?.workflowFormSteps.forms[
      'selectPricingMethodsToArchive'
    ] as FormSelectMethod;
    const selectedListPrm = prm?.selectedList as MagicKeyValue;

    const pricingMethods = prm?.pricingMethods?.map(prmMethod => {
      const selectedList = selectedListPrm[prmMethod?.code] as string[];
      return {
        ...prmMethod,
        methodList: prmMethod?.methodList?.map(method => {
          return {
            ...method,
            selected:
              selectedList?.findIndex(item => item === method?.id) !== -1
          };
        })
      };
    });

    const updateRequest: UpdateflowInstanceData = {
      workflowInstanceId: changeForm.workflowInstanceId?.toString(),
      status: 'COMPLETE',
      data: {
        workflowSetupData,
        configurations: {
          pricingMethods,
          pricingStrategies,
          textIDs,
          tableIDs
        }
      }
    };

    await workflowService.updateWorkflowInstanceData(updateRequest);

    return true;
  }
);
export const setupBulkArchivePCFWorkflowInstanceBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(setupBulkArchivePCFWorkflowInstance.pending, draftState => {
    set(draftState.workflowSetup, 'loading', true);
  });
  builder.addCase(
    setupBulkArchivePCFWorkflowInstance.fulfilled,
    (draftState, action) => {
      set(draftState.workflowSetup, 'loading', false);
      set(draftState.workflowSetup, 'error', false);
    }
  );
  builder.addCase(setupBulkArchivePCFWorkflowInstance.rejected, draftState => {
    set(draftState.workflowSetup, 'loading', false);
    set(draftState.workflowSetup, 'error', true);
  });
};
