import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';
import { getBulkArchivePCFWorkflowTableAreas } from './getWorkflowTableAreas';

const initReducer: any = {
  workflowTableAreas: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  }
};

describe('redux-store > workflow-setup > BulkArchivePCFWorkflow > getBulkArchivePCFWorkflowTableAreas', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getBulkArchivePCFWorkflowTableAreas.pending(
        'getBulkArchivePCFWorkflowTableAreas',
        {
          tableArea: ''
        }
      )
    );
    expect(nextState.workflowTableAreas.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getBulkArchivePCFWorkflowTableAreas.fulfilled(
        true,
        'getBulkArchivePCFWorkflowTableAreas',
        {
          tableArea: ''
        }
      )
    );
    expect(nextState.workflowTableAreas.loading).toEqual(false);
    expect(nextState.workflowTableAreas.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getBulkArchivePCFWorkflowTableAreas.rejected(
        new Error('mock error'),
        'getBulkArchivePCFWorkflowTableAreas',
        { tableArea: '' }
      )
    );
    expect(nextState.workflowTableAreas.loading).toEqual(false);
    expect(nextState.workflowTableAreas.error).toEqual(true);
  });

  it('thunk action', async () => {
    const nextState: any = await getBulkArchivePCFWorkflowTableAreas({
      tableArea: ''
    })(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          workflowSetup: {
            ...initReducer
          }
        } as RootState),
      {
        workflowService: {
          getTableListForArchive: jest.fn().mockResolvedValue({
            data: {
              tables: [{ test: '1' }],
              total: 10
            }
          })
        }
      }
    );
    expect(nextState?.payload?.workflowSetupData).toBeUndefined();
  });
});
