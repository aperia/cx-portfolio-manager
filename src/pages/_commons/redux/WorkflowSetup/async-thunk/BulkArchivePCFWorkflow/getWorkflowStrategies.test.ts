import { WorkflowStrategyListData } from 'app/fixtures/workflow-strategy-list';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';
import { defaultWorkflowStrategiesFilter } from '../../reducers';

const initReducer: any = {
  workflowStrategies: {
    strategies: [],
    loading: false,
    error: false,
    dataFilter: defaultWorkflowStrategiesFilter
  }
};

describe('redux-store > workflow-setup > BulkArchivePCFWorkflow > getWorkflowStrategies', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getBulkArchivePCFWorkflowStrategies.pending(
        'getWorkflowStrategies',
        {
          strategyArea: 'mock data'
        }
      )
    );
    expect(nextState.workflowStrategies.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getBulkArchivePCFWorkflowStrategies.fulfilled(
        {},
        'getWorkflowStrategies',
        {
          strategyArea: 'mock data'
        }
      )
    );
    expect(nextState.workflowStrategies.loading).toEqual(false);
    expect(nextState.workflowStrategies.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getBulkArchivePCFWorkflowStrategies.rejected(
        new Error('mock error'),
        'getWorkflowStrategies',
        {
          strategyArea: 'mock data'
        }
      )
    );
    expect(nextState.workflowStrategies.loading).toEqual(false);
    expect(nextState.workflowStrategies.error).toEqual(true);
  });

  it('thunk action > call api', async () => {
    const nextState: any =
      await actionsWorkflowSetup.getBulkArchivePCFWorkflowStrategies({
        strategyArea: 'mock data'
      })(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer
            },
            mapping: { data: { pricingStrategies: WorkflowStrategyListData } }
          } as RootState),
        {
          workflowService: {
            getWorkflowStrategies: jest.fn().mockResolvedValue({
              data: { pricingStrategies: WorkflowStrategyListData }
            })
          }
        }
      );

    expect(nextState.payload.strategies.length).toEqual(4);
  });
});
