import {
  ActionsValues,
  SubTransactionCode
} from 'pages/WorkflowManageAccountDelinquency/ConfigureParametersStep';
import { parseParametersToRequest } from './helper';

const parameterForm = {
  action: '',
  parameters: [
    { name: SubTransactionCode.NM180ManualCode, value: '00' },
    { name: SubTransactionCode.NM180WorkoutCode, value: '00' },
    { name: SubTransactionCode.NM180Code, value: '00' },
    { name: SubTransactionCode.NM180ManualCsr, value: 'Y' },
    { name: SubTransactionCode.NM180WorkoutCsr, value: 'Y' },
    { name: SubTransactionCode.NM180Csr, value: 'Y' },
    { name: SubTransactionCode.NM181, value: 'Y' },
    { name: SubTransactionCode.NM183, value: '01' }
  ]
};

describe('Redux > ManageAccountDelinquency > Helper', () => {
  it('Should return data properly for NM180Manual', () => {
    const result = parseParametersToRequest({
      ...parameterForm,
      action: ActionsValues.NM180Manual
    });
    expect(result).toEqual([
      { name: SubTransactionCode.NM180Code, value: '00' },
      { name: SubTransactionCode.NM180Csr, value: 'Y' }
    ]);
  });

  it('Should return data properly for NM180Workout', () => {
    const result = parseParametersToRequest({
      ...parameterForm,
      action: ActionsValues.NM180Workout
    });
    expect(result).toEqual([
      { name: SubTransactionCode.NM180Code, value: '00' },
      { name: SubTransactionCode.NM180Csr, value: 'Y' }
    ]);
  });

  it('Should return data properly for NM181', () => {
    const result = parseParametersToRequest({
      ...parameterForm,
      action: ActionsValues.NM181
    });
    expect(result).toEqual([{ name: SubTransactionCode.NM181, value: 'Y' }]);
  });

  it('Should return data properly for NM182', () => {
    const result = parseParametersToRequest({
      ...parameterForm,
      action: ActionsValues.NM182
    });
    expect(result).toEqual([]);
  });

  it('Should return data properly for NM183', () => {
    const result = parseParametersToRequest({
      ...parameterForm,
      action: ActionsValues.NM183
    });
    expect(result).toEqual([{ name: SubTransactionCode.NM183, value: '01' }]);
  });

  it('Should return data properly for NO Action', () => {
    const result = parseParametersToRequest({
      ...parameterForm
    });
    expect(result).toEqual([]);
  });
});
