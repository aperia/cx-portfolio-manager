import {
  ActionsValues,
  SubTransactionCode
} from 'pages/WorkflowManageAccountDelinquency/ConfigureParametersStep';

export const parseParametersToRequest = (parametersForm: any) => {
  const { action, parameters = [] } = parametersForm || {};
  switch (action) {
    case ActionsValues.NM180Manual:
    case ActionsValues.NM180Workout:
      const nm180Parameters = parameters.filter(
        (p: any) =>
          p.name === SubTransactionCode.NM180Code ||
          p.name === SubTransactionCode.NM180Csr
      );
      return nm180Parameters;
    case ActionsValues.NM181:
      const nm181Parameters = parameters.filter(
        (p: any) => p.name === SubTransactionCode.NM181
      );
      return nm181Parameters;
    case ActionsValues.NM182:
      return [];
    case ActionsValues.NM183:
      const nm183Parameters = parameters.filter(
        (p: any) => p.name === SubTransactionCode.NM183
      );
      return nm183Parameters;

    default:
      return [];
  }
};
