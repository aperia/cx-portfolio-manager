import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { FormatTime } from 'app/constants/enums';
import { addCurrentTime, formatTimeDefault } from 'app/helpers';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { workflowSetupTemplateIdSelector } from '../../select-hooks';
import { parseParametersToRequest } from './helper';

export const createManageAccountDelinquencyWorkflowInstance = createAsyncThunk<
  any,
  WorkflowSetupThunkArg,
  ThunkAPIConfig
>(
  'workflowSetup/createManageAccountDelinquencyWorkflowInstance',
  async ({ workflowSetupData }, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

    const state = thunkAPI.getState();
    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;
    const uploadFileForm = state.workflowSetup?.workflowFormSteps.forms[
      'uploadFile'
    ] as any;
    const parametersForm = state.workflowSetup?.workflowFormSteps.forms[
      'configureParameters'
    ] as any;

    const workflowTemplateId = workflowSetupTemplateIdSelector(state);

    const changeDetailData = {
      name: changeForm.changeName || '',
      description: changeForm.changeDescription || '',
      effectiveDate: formatTimeDefault(
        addCurrentTime(changeForm.effectiveDate!).toISOString(),
        FormatTime.DateServer
      )!
    };
    const changeId = changeForm.changeSetId;
    let workflowInstanceId = changeForm.workflowInstanceId?.toString();

    if (workflowInstanceId && changeId) {
      await workflowService.updateWorkflowInstance({
        workflowInstanceId,
        changeSetId: changeId.toString(),
        workflowInstanceDetails: {
          note: changeForm.workflowNote
        },
        changeSetDetail: changeDetailData
      });
    } else {
      const workflowRes = await workflowService.createWorkflowInstance({
        workflowInstanceDetails: {
          note: changeForm.workflowNote
        },
        workflowTemplateId: workflowTemplateId || '',
        changeSetId: changeId?.toString()
      });

      workflowInstanceId =
        workflowRes?.data?.workflowInstanceDetails?.id?.toString();
    }

    const updateRequest: UpdateflowInstanceData = {
      workflowInstanceId: workflowInstanceId?.toString(),
      status: 'INCOMPLETE',
      data: {
        workflowSetupData,
        configurations: {
          attachmentId: uploadFileForm?.attachmentId,
          transactionId: parametersForm?.action,
          parameters: parseParametersToRequest(parametersForm)
        }
      }
    };
    await workflowService.updateWorkflowInstanceData(updateRequest);

    return {
      changeId,
      workflowInstanceId
    };
  }
);
export const createManageAccountDelinquencyWorkflowInstanceBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    createManageAccountDelinquencyWorkflowInstance.pending,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', true);
      set(draftState.workflowInstance, 'error', false);
    }
  );
  builder.addCase(
    createManageAccountDelinquencyWorkflowInstance.fulfilled,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', false);
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'changeSetId',
        action.payload?.changeId
      );
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'workflowInstanceId',
        action.payload?.workflowInstanceId
      );
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'isCompletedWorkflow',
        false
      );
    }
  );
  builder.addCase(
    createManageAccountDelinquencyWorkflowInstance.rejected,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', true);
    }
  );
};
