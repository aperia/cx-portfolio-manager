import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  tableListWorkflowSetup: {}
};

describe('redux-store > workflow-setup > getTableListForWorkflowSetup', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getTableListForWorkflowSetup.pending(
        'getTableListForWorkflowSetup',
        {
          elementMetaData: []
        }
      )
    );
    expect(nextState.tableListWorkflowSetup.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getTableListForWorkflowSetup.fulfilled(
        true,
        'getTableListForWorkflowSetup',
        {
          elementMetaData: []
        }
      )
    );
    expect(nextState.tableListWorkflowSetup.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getTableListForWorkflowSetup.rejected(
        new Error('mock error'),
        'getTableListForWorkflowSetup',

        { elementMetaData: [] }
      )
    );
    expect(nextState.tableListWorkflowSetup.loading).toEqual(false);
  });

  // it('thunk action > with files', async () => {
  //   const nextState: any =
  //     await actionsWorkflowSetup.getTableListForWorkflowSetup('test')(
  //       store.dispatch,
  //       () => store.getState(),
  //       {
  //         workflowService: {
  //           getTableListForWorkflowSetup: jest.fn().mockResolvedValue({
  //             data: { tableList: [{ tableId: 'All', tableName: 'All' }] }
  //           })
  //         }
  //       }
  //     );
  //   expect(nextState.payload).toEqual([{ tableId: 'All', tableName: 'All' }]);
  // });

  // it('thunk action > with files', async () => {
  //   const nextState: any =
  //     await actionsWorkflowSetup.getTableListForWorkflowSetup([test])(
  //       store.dispatch,
  //       () => store.getState(),
  //       {
  //         workflowService: {
  //           getTableListForWorkflowSetup: jest.fn().mockResolvedValue({
  //             data: {
  //               tableList: [
  //                 { tableId: 'All', tableName: 'All' },
  //                 { tableId: 'All', tableName: 'All' }
  //               ]
  //             }
  //           })
  //         }
  //       }
  //     );
  //   expect(nextState.payload).toEqual([
  //     { tableId: 'All', tableName: 'All' },
  //     { tableId: 'All', tableName: 'All' },
  //     { tableId: 'All', tableName: 'All' }
  //   ]);
  // });
  it('thunk action > with files', async () => {
    const nextState: any =
      await actionsWorkflowSetup.getTableListForWorkflowSetup([test])(
        store.dispatch,
        () => store.getState(),
        {
          workflowService: {
            getTableListForWorkflowSetup: jest.fn().mockResolvedValue({
              data: {}
            })
          }
        }
      );
    expect(nextState.payload).toEqual(undefined);
  });
});
