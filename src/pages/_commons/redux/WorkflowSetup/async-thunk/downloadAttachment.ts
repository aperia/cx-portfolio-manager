import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import {
  SendLetterParameterEnum,
  TableFieldParameterEnum
} from 'app/constants/enums';
import { booleanToString, mappingDataFromObj } from 'app/helpers';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import { get } from 'lodash';
import set from 'lodash.set';
import { ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME } from 'pages/WorkflowActionCardCompromiseEvent/ActionCardCompromiseEventSelectActionsStep/helper';
import {
  ActionsValues,
  SubTransactionCode
} from 'pages/WorkflowManageAccountDelinquency/ConfigureParametersStep';
import { ManageAccountLevelTable } from 'pages/WorkflowManageAccountLevel/ManageAccountLevelCAStep';
import { ConfigTableItem } from 'pages/WorkflowManageTransactionLevelProcessingDecisionTablesTLP/ConfigureParameters/type';
import {
  decisionElementListData,
  elementMetadataForActionCardCompromiseEvent,
  selectWorkflowInstanceInfo,
  workflowSetupTemplateIdSelector
} from '../select-hooks';

interface DataPost {
  searchFields: {
    workflowTemplateId?: string;
    workflowInstanceId?: string;
    templateCode: string;
    transactionId?: string;
    parameters?: {
      name?: any;
      value?: any;
      tableId?: string;
      tableType?: string;
      decisionElements?: any[];
    }[];
  };
}

export const downloadTemplateFile = createAsyncThunk<
  any,
  MagicKeyValue,
  ThunkAPIConfig
>('workflowSetup/downloadTemplateFile', async (args, thunkAPI) => {
  const { downloadTemplateType } = args;
  const state = thunkAPI.getState();
  const { workflowTemplateService } = get(thunkAPI, 'extra') as APIMapping;

  // Take mapping model
  let dataPost = { searchFields: {} } as DataPost;
  const downloadFileMapping = state.mapping?.data?.downloadTemplateFile;
  const workflowTemplateId = workflowSetupTemplateIdSelector(state);
  const { workflowInstanceId } = selectWorkflowInstanceInfo(state);

  switch (downloadTemplateType) {
    case 'accountDelinquency':
      const action =
        state.workflowSetup?.workflowFormSteps?.forms['configureParameters']
          .action || '';
      let transactionCode = '';
      let subCode = '';
      switch (action) {
        case ActionsValues.NM180Manual:
        case ActionsValues.NM180Workout:
          transactionCode = 'NM180';
          subCode = SubTransactionCode.NM180Code;
          break;
        default:
          transactionCode = action;
          break;
      }
      const configParameters =
        (subCode &&
          state.workflowSetup?.workflowFormSteps?.forms['configureParameters']
            .parameters) ||
        [];

      const subCodeValue =
        subCode && configParameters.find((p: any) => p.name === subCode)?.value;

      const template = subCodeValue
        ? `MAD_${transactionCode}_${subCodeValue}_TEMPLATE_CODE`
        : `MAD_${transactionCode}_TEMPLATE_CODE`;

      dataPost = {
        searchFields: {
          templateCode: template
        }
      };
      break;
    case 'forceEmbossReplacementCardsWorkflow':
      const parameters =
        state.workflowSetup?.workflowFormSteps?.forms['configureParameters']
          ?.config?.parameters;

      const transactionId = parameters?.value
        ?.replace('MASS', '')
        ?.replace('RUSH', '');

      const isMN = parameters?.action?.param?.isNM65MailCode;

      let value = [] as string[];

      value = [...value, transactionId];

      if (isMN) {
        value = [...value, 'nm.65'];
      }

      dataPost = {
        searchFields: {
          templateCode: 'FERC_TEMPLATE_CODE',
          parameters: [
            {
              name: 'transactionId',
              value
            }
          ]
        }
      };
      break;
    case 'manageTemporaryCreditLimitWorkflow':
      dataPost = {
        searchFields: {
          templateCode: 'MTCL_TEMPLATE_CODE'
        }
      };
      break;
    case 'manageAccountTransferWorkflow':
      dataPost = {
        searchFields: {
          templateCode: 'MAT_TEMPLATE_CODE'
        }
      };
      break;
    case 'flexibleNonMonetaryTransactionsWorkflow':
      const transactionsNonMonetary =
        state.workflowSetup?.workflowFormSteps?.forms
          ?.selectNonMonetaryTransactions?.transactions;
      dataPost = {
        searchFields: {
          templateCode: 'RFNMT_TEMPLATE_CODE',
          parameters: [
            {
              name: 'transactionid',
              value: transactionsNonMonetary
            }
          ]
        }
      };
      break;
    case 'sendLetterToAccountsWorkflow': {
      const configureParameters = state.workflowSetup?.workflowFormSteps?.forms[
        'configureParameters'
      ]?.configureParameters as WorkflowSetupConfigureParameters;

      const letterNumber =
        configureParameters?.[SendLetterParameterEnum.LetterNumber];
      const addressing =
        configureParameters?.[SendLetterParameterEnum.Addressing] === '0'
          ? 'STANDARD'
          : 'UNIVERSAL';

      const parameters = [{ name: 'letterNumber', value: [letterNumber] }];
      const templateCode = `SLA_${addressing}_TEMPLATE_CODE`;
      dataPost = {
        searchFields: {
          templateCode,
          parameters
        }
      };
      break;
    }
    case 'haltInterestChargesAndFees':
      dataPost = {
        searchFields: {
          templateCode: 'HICF_TEMPLATE_CODE'
        }
      };
      break;
    case 'assignPricingStrategiesWorkflow':
      dataPost = {
        searchFields: {
          templateCode: 'APSMOA_TEMPLATE_CODE'
        }
      };
      break;
    case 'flexibleMonetaryTransactionsWorkflow': {
      const transactions = state.workflowSetup?.workflowFormSteps?.forms
        ?.selectMonetaryTransactions?.transactions as RefData[];

      const parameters = [
        { name: 'transactionid', value: transactions.map(t => t.code) }
      ];

      dataPost = {
        searchFields: {
          templateCode: 'RFMT_TEMPLATE_CODE',
          parameters
        }
      };
      break;
    }
    case 'bulkSuspendFraudStrategyWorkflow':
      dataPost = {
        searchFields: {
          templateCode: 'BSFS_TEMPLATE_CODE'
        }
      };
      break;
    case 'manageAccountMemosWorkflow':
      dataPost = {
        searchFields: {
          templateCode: 'MAM_TEMPLATE_CODE'
        }
      };
      break;
    case 'manageAccountLevelWorkflowAQ': {
      const tablesALPAQ =
        (state.workflowSetup?.workflowFormSteps?.forms?.configAlpAq
          ?.tables as ConfigTableItem[]) || [];
      const dataALPAQ = decisionElementListData('ALPAQ')(state);
      const decisionElementListALPAQ = dataALPAQ?.decisionElementList || [];
      const arrParametersALPAQ = tablesALPAQ.map(item => {
        const decisionElements = (
          Array.isArray(item?.elementList)
            ? item.elementList.map(el => ({
                name: el.name,
                searchCode: el.searchCode
              }))
            : []
        ).concat(
          decisionElementListALPAQ.filter((el: any) => {
            return !el.configurable;
          })
        );
        return { tableId: item.tableId, tableType: 'AQ', decisionElements };
      });
      dataPost = {
        searchFields: {
          templateCode: 'MALPDT_TEMPLATE_CODE',
          parameters: arrParametersALPAQ
        }
      };
      break;
    }
    case 'manageAccountLevelWorkflowCA':
      const caConfig = state.workflowSetup?.workflowFormSteps?.forms
        ?.configAlpCa?.tables as ManageAccountLevelTable;

      const decisionList =
        (state.workflowSetup?.workflowFormSteps?.forms?.configAlpCa
          ?.decisionList as MagicKeyValue[]) || [];

      const parametersCA = {
        tableId: caConfig?.tableId,
        tableType: 'CA',
        decisionElements: caConfig?.elementList
          ?.map((item: MagicKeyValue) => ({
            name: item?.name,
            searchCode: item?.searchCode,
            immediateAllocation: booleanToString(item?.immediateAllocation)
          }))
          ?.concat(
            decisionList
              .filter((el: any) => {
                return !el.configurable;
              })
              .map((i: MagicKeyValue) => ({
                name: i?.name,
                searchCode: i?.searchCode,
                immediateAllocation: booleanToString(i?.immediateAllocation)
              }))
          )
      };
      dataPost = {
        searchFields: {
          templateCode: 'MALPDT_TEMPLATE_CODE',
          parameters: [parametersCA as any]
        }
      };
      break;
    case 'monetaryAdjustmentTransactionsWorkflow':
      const selectedTransactions =
        state.workflowSetup?.workflowFormSteps?.forms
          ?.selectMonetaryAdjustmentTransactions?.transactions;
      dataPost = {
        searchFields: {
          templateCode: 'PMA_TEMPLATE_CODE',
          parameters: [
            {
              name: 'transactionid',
              value: selectedTransactions
            }
          ]
        }
      };
      break;
    case 'conditionMCycleAccountsForTestingWorkflow': {
      const transactions = state.workflowSetup?.workflowFormSteps?.forms
        ?.selectTransactionsConditionMCycleAccountsForTesting
        ?.transactions as RefData[];

      const parameters = [{ name: 'transactionid', value: transactions }];
      dataPost = {
        searchFields: {
          templateCode: 'CMCAFT_TEMPLATE_CODE',
          parameters
        }
      };
      break;
    }
    case 'manageOCSShells':
      dataPost = {
        searchFields: {
          templateCode: 'MOCSS_TEMPLATE_CODE'
        }
      };
      break;
    case 'bulkExternalStatusManagementWorkflow':
      dataPost = {
        searchFields: {
          templateCode: 'BESM_TEMPLATE_CODE'
        }
      };
      break;
    case 'manageTransactionLevelProcessingDecisionTablesTLPWorkflowAQ':
      const tablesTLPAQ =
        (state.workflowSetup?.workflowFormSteps?.forms?.configTlpAq
          ?.tables as ConfigTableItem[]) || [];
      const dataTLPAQ = decisionElementListData('TLPAQ')(state);
      const decisionElementListTLPAQ = dataTLPAQ?.decisionElementList || [];
      const arrParametersTLPAQ = tablesTLPAQ.map(item => {
        const decisionElements = (
          Array.isArray(item?.elementList)
            ? item.elementList.map(el => ({
                name: el.name,
                searchCode: el.searchCode
              }))
            : []
        ).concat(
          decisionElementListTLPAQ.filter((el: any) => {
            return !el.configurable;
          })
        );
        return { tableId: item.tableId, tableType: 'AQ', decisionElements };
      });
      dataPost = {
        searchFields: {
          templateCode: 'MTLPDT_TEMPLATE_CODE',
          parameters: arrParametersTLPAQ
        }
      };
      break;
    case 'manageTransactionLevelProcessingDecisionTablesTLPWorkflowTQ':
      const tablesTLPTQ =
        (state.workflowSetup?.workflowFormSteps?.forms?.configTlpTq
          ?.tables as ConfigTableItem[]) || [];
      const dataTLPTQ = decisionElementListData('TLPTQ')(state);
      const decisionElementListTLPTQ = dataTLPTQ?.decisionElementList || [];
      const arrParametersTLPTQ = tablesTLPTQ.map(item => {
        const decisionElements = (
          Array.isArray(item?.elementList)
            ? item.elementList.map(el => ({
                name: el.name,
                searchCode: el.searchCode
              }))
            : []
        ).concat(
          decisionElementListTLPTQ.filter((el: any) => {
            return !el.configurable;
          })
        );
        return { tableId: item.tableId, tableType: 'TQ', decisionElements };
      });
      dataPost = {
        searchFields: {
          templateCode: 'MTLPDT_TEMPLATE_CODE',
          parameters: arrParametersTLPTQ
        }
      };
      break;
    case 'manageTransactionLevelProcessingDecisionTablesTLPWorkflowRQ':
      const tablesTLPRQ =
        (state.workflowSetup?.workflowFormSteps?.forms?.configTlpRq
          ?.tables as ConfigTableItem[]) || [];
      const dataTLPRQ = decisionElementListData('TLPRQ')(state);
      const decisionElementListTLPRQ = dataTLPRQ?.decisionElementList || [];
      const arrParametersTLPRQ = tablesTLPRQ.map(item => {
        const decisionElements = (
          Array.isArray(item?.elementList)
            ? item.elementList.map(el => ({
                name: el.name,
                searchCode: el.searchCode
              }))
            : []
        ).concat(
          decisionElementListTLPRQ.filter((el: any) => {
            return !el.configurable;
          })
        );
        return { tableId: item.tableId, tableType: 'RQ', decisionElements };
      });
      dataPost = {
        searchFields: {
          templateCode: 'MTLPDT_TEMPLATE_CODE',
          parameters: arrParametersTLPRQ
        }
      };
      break;
    case 'manageTransactionLevelProcessingDecisionTablesTLPWorkflowCA':
      const tablesTLPCA =
        (state.workflowSetup?.workflowFormSteps?.forms?.configTlpCa
          ?.tables as ConfigTableItem[]) || [];
      const dataTLPCA = decisionElementListData('TLPCA')(state);
      const decisionElementListTLPCA = dataTLPCA?.decisionElementList || [];
      const arrParametersTLPCA = tablesTLPCA.map(item => {
        const decisionElements = (
          Array.isArray(item?.elementList)
            ? item.elementList.map(el => ({
                name: el.name,
                searchCode: el.searchCode
              }))
            : []
        ).concat(
          decisionElementListTLPCA.filter((el: any) => {
            return !el.configurable;
          })
        );
        return { tableId: item.tableId, tableType: 'CA', decisionElements };
      });
      dataPost = {
        searchFields: {
          templateCode: 'MTLPDT_TEMPLATE_CODE',
          parameters: arrParametersTLPCA
        }
      };
      break;
    case 'manageMethodLevelProcessingDecisionTablesMLPWorkflowAQ':
      const tablesAQ = state.workflowSetup?.workflowFormSteps?.forms
        ?.configMlpAq?.tables as RefData[];
      const dataMLPAQ = decisionElementListData('MLPAQ')(state);
      const decisionElementListMLPAQ = dataMLPAQ?.decisionElementList || [];
      const arrParametersAQ = tablesAQ.map((item: Record<string, any>) => {
        const decisionElements = item.elementList
          .map((i: Record<string, any>) => ({
            name: i.name,
            searchCode: i.searchCode,
            immediateAllocation: booleanToString(item?.immediateAllocation)
          }))
          .concat(
            decisionElementListMLPAQ.filter((el: any) => {
              return item.tableControlParameters?.[
                TableFieldParameterEnum.ChangeInTermsMethodFlag
              ] === 'Y'
                ? !el.configurable
                : !el.configurable && el.name !== 'CIT METHOD FLAG';
            })
          );
        return { tableId: item.tableId, tableType: 'AQ', decisionElements };
      });
      dataPost = {
        searchFields: {
          templateCode: 'MMLPDT_TEMPLATE_CODE',
          parameters: arrParametersAQ
        }
      };
      break;

    case 'manageMethodLevelProcessingDecisionTablesMLPWorkflowCA':
      const tablesCA = state.workflowSetup?.workflowFormSteps?.forms
        ?.configMlpCa?.tables as RefData[];
      const dataMLPCA = decisionElementListData('MLPCA')(state);
      const decisionElementListMLPCA = dataMLPCA?.decisionElementList || [];
      const arrParametersCA = tablesCA.map((item: Record<string, any>) => {
        const decisionElements = item.elementList
          .map((i: Record<string, any>) => ({
            name: i.name,
            searchCode: i.searchCode,
            immediateAllocation: booleanToString(i.immediateAllocation)
          }))
          .concat(
            decisionElementListMLPCA.filter((el: any) => {
              return !el.configurable;
            })
          );
        return { tableId: item.tableId, tableType: 'CA', decisionElements };
      });
      dataPost = {
        searchFields: {
          templateCode: 'MMLPDT_TEMPLATE_CODE',
          parameters: arrParametersCA
        }
      };
      break;

    case 'manageMethodLevelProcessingDecisionTablesMLPWorkflowST':
      const tablesST = state.workflowSetup?.workflowFormSteps?.forms
        ?.configMlpSt?.tables as RefData[];
      const arrParametersST = tablesST.map((item: Record<string, any>) => {
        const decisionElements = item.elementList.map(
          (i: Record<string, any>) => ({
            name: i.name,
            searchCode: i.searchCode,
            immediateAllocation: booleanToString(i.immediateAllocation)
          })
        );
        return { tableId: item.tableId, tableType: 'CA', decisionElements };
      });
      dataPost = {
        searchFields: {
          templateCode: 'MMLPDT_TEMPLATE_CODE',
          parameters: arrParametersST
        }
      };
      break;

    case 'manualAccountDeletionWorkflow':
      dataPost = {
        searchFields: {
          templateCode: 'MACCTD_TEMPLATE_CODE'
        }
      };
      break;
    case 'actionCardCompromiseEventWorkflow': {
      const actionForm =
        state.workflowSetup?.workflowFormSteps?.forms?.selectActions?.action;

      const {
        singleEntityYesTrans,
        singleEntityNoTrans,
        singleEntityReportLost,
        singleEntityReportStolen,
        separateEntityYesTrans,
        separateEntityNoTrans,
        separateEntityReportLost,
        separateEntityReportStolen
      } = elementMetadataForActionCardCompromiseEvent(state);

      const getParameterList = () => {
        if (
          actionForm?.radio ===
          ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SINGLE_ENTITY
        ) {
          if (actionForm?.question3 === 'Yes') {
            return singleEntityYesTrans;
          }
          if (actionForm?.question3 === 'No') {
            return singleEntityNoTrans;
          }
          if (
            actionForm?.question2 ===
            'Report Lost Accounts using the SL, Lost/Stolen Report transaction.'
          ) {
            return singleEntityReportLost;
          }
          if (
            actionForm?.question2 ===
            'Report Stolen Accounts using the SL, Lost/Stolen Report transaction.'
          ) {
            return singleEntityReportStolen;
          }
          return [];
        }
        if (
          actionForm?.radio ===
          ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SEPARATE_ENTITY
        ) {
          if (actionForm?.question3 === 'Yes') {
            return separateEntityYesTrans;
          }
          if (actionForm?.question3 === 'No') {
            return separateEntityNoTrans;
          }
          if (
            actionForm?.question2 ===
            'Report Lost PIIDs using the SLP, Lost/Stolen Report transaction.'
          ) {
            return separateEntityReportLost;
          }
          if (
            actionForm?.question2 ===
            'Report Stolen PIIDs using the SLP, Lost/Stolen Report transaction.'
          ) {
            return separateEntityReportStolen;
          }
          return [];
        }
        return [];
      };

      const parameterList = getParameterList();

      const parameters = [
        { name: 'parameterList', value: parameterList.map(p => p.code) }
      ];
      dataPost = {
        searchFields: {
          templateCode: 'ACCE_TEMPLATE_CODE',
          parameters
        }
      };
      break;
    }
    case 'cycleAccountsTestEnvironmentWorkflow':
      dataPost = {
        searchFields: {
          templateCode: 'CAITENV_TEMPLATE_CODE'
        }
      };
      break;
    default:
      break;
  }

  dataPost.searchFields.workflowTemplateId = workflowTemplateId;
  dataPost.searchFields.workflowInstanceId = workflowInstanceId;

  const response = await workflowTemplateService.downloadWorkflowTemplate(
    dataPost
  );

  const attachmentData = response?.data?.attachment;
  const file = mappingDataFromObj(attachmentData, downloadFileMapping!);

  return file;
});

export const downloadTemplateFileBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(downloadTemplateFile.pending, draftState => {
    set(draftState.workflowSetup, 'loading', true);
  });
  builder.addCase(downloadTemplateFile.fulfilled, draftState => {
    set(draftState.workflowSetup, 'loading', false);
  });
  builder.addCase(downloadTemplateFile.rejected, draftState => {
    set(draftState.workflowSetup, 'loading', false);
  });
};
