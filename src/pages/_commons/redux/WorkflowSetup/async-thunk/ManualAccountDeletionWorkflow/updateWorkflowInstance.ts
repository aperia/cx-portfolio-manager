import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';
import { ConfigureParametersFormValues } from 'pages/WorkflowManualAccountDeletion/ConfigureParametersStep';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { FormUploadFile } from 'pages/_commons/WorkflowSetup/CommonSteps/_components/UploadFile';

export const updateManualAccountDeletionWorkflowInstance = createAsyncThunk<
  any,
  WorkflowSetupThunkArg,
  ThunkAPIConfig
>(
  'workflowSetup/updateManualAccountDeletionWorkflowInstance',
  async ({ workflowSetupData }, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

    const state = thunkAPI.getState();

    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;

    const configureParametersForm = state.workflowSetup?.workflowFormSteps
      .forms['configureParameters'] as ConfigureParametersFormValues;
    const uploadedFile = state.workflowSetup?.workflowFormSteps.forms[
      'uploadFile'
    ] as FormUploadFile;

    const configureParameters: any =
      configureParametersForm.configureParameters || {};
    const parameters = Object.keys(configureParameters).map(p => ({
      name: p,
      value:
        p === MethodFieldParameterEnum.ManualAccountDeletionMN0TransactionId
          ? configureParameters[p]?.join(';')
          : configureParameters[p]
    }));

    const updateRequest: UpdateflowInstanceData = {
      workflowInstanceId: changeForm.workflowInstanceId?.toString(),
      status: 'INCOMPLETE',
      data: {
        workflowSetupData,
        configurations: {
          parameters,
          transactionId: configureParametersForm.transactionId,
          attachmentId: uploadedFile?.attachmentId?.toString()
        }
      }
    };

    await workflowService.updateWorkflowInstanceData(updateRequest);

    return { workflowSetupData };
  }
);
export const updateManualAccountDeletionWorkflowInstanceBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    updateManualAccountDeletionWorkflowInstance.pending,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', true);
      set(draftState.workflowInstance, 'error', false);
    }
  );
  builder.addCase(
    updateManualAccountDeletionWorkflowInstance.fulfilled,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', false);
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'isCompletedWorkflow',
        false
      );
      set(
        draftState.workflowFormSteps,
        'workflowSetupData',
        action.payload?.workflowSetupData
      );
    }
  );
  builder.addCase(
    updateManualAccountDeletionWorkflowInstance.rejected,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', true);
    }
  );
};
