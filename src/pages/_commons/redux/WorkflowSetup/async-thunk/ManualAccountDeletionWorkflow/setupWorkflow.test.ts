import { MethodFieldParameterEnum } from 'app/constants/enums';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  workflowInstanceStatus: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      changeInformation: {
        changeSetId: '1111',
        workflowInstanceId: '2222',
        name: 'mock name',
        description: 'mock description',
        workflowNote: 'mock note',
        effectiveDate: new Date()
      },
      configureParameters: {
        transactionId: 123,
        configureParameters: {
          [MethodFieldParameterEnum.ManualAccountDeletionExternalStatusCode]:
            '2',
          [MethodFieldParameterEnum.ManualAccountDeletionPropagate]: '1',
          [MethodFieldParameterEnum.ManualAccountDeletionMN0Transaction]: [
            'mn01',
            'mn02'
          ]
        }
      },
      uploadFile: {
        attachmentId: 'attachmentId'
      }
    }
  }
};

jest.mock('app/helpers', () => {
  const modules = jest.requireActual('app/helpers');
  return {
    ...modules,
    formatTimeDefault: () => '01/01/2021'
  };
});

describe('redux-store > workflow-setup > ManualAccountDeletionWorkflow > setupWorkflow', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.setupManualAccountDeletionWorkflow.pending(
        'setupWorkflow',
        {}
      )
    );
    expect(nextState.workflowSetup.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.setupManualAccountDeletionWorkflow.fulfilled(
        true,
        'setupWorkflow',
        {}
      )
    );
    expect(nextState.workflowSetup.loading).toEqual(false);
    expect(nextState.workflowSetup.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.setupManualAccountDeletionWorkflow.rejected(
        new Error('mock error'),
        'setupWorkflow',
        {}
      )
    );
    expect(nextState.workflowSetup.loading).toEqual(false);
    expect(nextState.workflowSetup.error).toEqual(true);
  });

  it('thunk action > with workflowInstanceId & changeId', async () => {
    const updateWorkflowDataFn = jest.fn();

    const nextState: any =
      await actionsWorkflowSetup.setupManualAccountDeletionWorkflow({
        workflowSetupData: []
      })(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer
            }
          } as RootState),
        {
          workflowService: {
            updateWorkflowInstanceData: updateWorkflowDataFn
          }
        }
      );
    expect(nextState.payload).toEqual({ workflowSetupData: [] });

    expect(updateWorkflowDataFn).toHaveBeenCalled();
  });
});
