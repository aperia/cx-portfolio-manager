import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types/workflowSetupState';
import get from 'lodash.get';
import set from 'lodash.set';
import { ConfigureParametersFormValues } from 'pages/WorkflowManualAccountDeletion/ConfigureParametersStep';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { FormUploadFile } from 'pages/_commons/WorkflowSetup/CommonSteps/_components/UploadFile';

export const setupManualAccountDeletionWorkflow = createAsyncThunk<
  any,
  WorkflowSetupThunkArg,
  ThunkAPIConfig
>(
  'workflowSetup/setupManualAccountDeletionWorkflow',
  async ({ workflowSetupData }, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

    const state = thunkAPI.getState();

    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;

    const configureParametersForm = state.workflowSetup?.workflowFormSteps
      .forms['configureParameters'] as ConfigureParametersFormValues;
    const uploadedFile = state.workflowSetup?.workflowFormSteps.forms[
      'uploadFile'
    ] as FormUploadFile;

    const configureParameters: any =
      configureParametersForm.configureParameters || {};
    const parameters = Object.keys(configureParameters).map(p => ({
      name: p,
      value:
        p === MethodFieldParameterEnum.ManualAccountDeletionMN0TransactionId
          ? configureParameters[p]?.join(';')
          : configureParameters[p]
    }));

    const updateRequest: UpdateflowInstanceData = {
      workflowInstanceId: changeForm.workflowInstanceId?.toString(),
      status: 'COMPLETE',
      data: {
        workflowSetupData,
        configurations: {
          parameters,
          transactionId: configureParametersForm.transactionId,
          attachmentId: uploadedFile?.attachmentId?.toString()
        }
      }
    };

    await workflowService.updateWorkflowInstanceData(updateRequest);

    return { workflowSetupData };
  }
);
export const setupManualAccountDeletionWorkflowBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(setupManualAccountDeletionWorkflow.pending, draftState => {
    set(draftState.workflowSetup, 'loading', true);
  });
  builder.addCase(
    setupManualAccountDeletionWorkflow.fulfilled,
    (draftState, action) => {
      set(draftState.workflowSetup, 'loading', false);
      set(draftState.workflowSetup, 'error', false);
      set(
        draftState.workflowFormSteps,
        'workflowSetupData',
        action.payload?.workflowSetupData
      );
    }
  );
  builder.addCase(setupManualAccountDeletionWorkflow.rejected, draftState => {
    set(draftState.workflowSetup, 'loading', false);
    set(draftState.workflowSetup, 'error', true);
  });
};
