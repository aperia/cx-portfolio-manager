import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { workflowSetupTemplateIdSelector } from '../../select-hooks';

export const createConditionMCycleAccountsForTestingWorkflowInstance =
  createAsyncThunk<any, WorkflowSetupThunkArg, ThunkAPIConfig>(
    'workflowSetup/createConditionMCycleAccountsForTestingWorkflowInstance',
    async ({ workflowSetupData }, thunkAPI) => {
      const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

      const state = thunkAPI.getState();

      const changeForm = state.workflowSetup?.workflowFormSteps.forms[
        'changeInformation'
      ] as FormChangeInfo;

      const workflowTemplateId = workflowSetupTemplateIdSelector(state);
      const changeId = changeForm.changeSetId;
      let workflowInstanceId = changeForm.workflowInstanceId?.toString();

      if (workflowInstanceId && changeId) {
        await workflowService.updateWorkflowInstance({
          workflowInstanceId,
          changeSetId: changeId.toString(),
          workflowInstanceDetails: {
            note: changeForm.workflowNote
          }
        } as any);
      } else {
        const workflowRes = await workflowService.createWorkflowInstance({
          workflowInstanceDetails: {
            note: changeForm.workflowNote
          },
          workflowTemplateId: workflowTemplateId || '',
          changeSetId: changeId?.toString()
        });

        workflowInstanceId =
          workflowRes?.data?.workflowInstanceDetails?.id?.toString();
      }

      const uploadFileForm = state.workflowSetup?.workflowFormSteps.forms[
        'uploadFile'
      ] as any;
      const { transactions } = state.workflowSetup?.workflowFormSteps.forms[
        'selectTransactionsConditionMCycleAccountsForTesting'
      ] as any;
      const updateRequest: UpdateflowInstanceData = {
        workflowInstanceId: workflowInstanceId?.toString(),
        status: 'INCOMPLETE',
        data: {
          workflowSetupData,
          configurations: {
            attachmentId: uploadFileForm.attachmentId,
            transactions
          }
        }
      };
      await workflowService.updateWorkflowInstanceData(updateRequest);
      return { changeId, workflowInstanceId, workflowSetupData };
    }
  );
export const createConditionMCycleAccountsForTestingWorkflowInstanceBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    createConditionMCycleAccountsForTestingWorkflowInstance.pending,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', true);
      set(draftState.workflowInstance, 'error', false);
    }
  );
  builder.addCase(
    createConditionMCycleAccountsForTestingWorkflowInstance.fulfilled,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', false);
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'changeSetId',
        action.payload?.changeId
      );
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'workflowInstanceId',
        action.payload?.workflowInstanceId
      );
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'isCompletedWorkflow',
        false
      );
      set(
        draftState.workflowFormSteps,
        'workflowSetupData',
        action.payload?.workflowSetupData
      );
    }
  );
  builder.addCase(
    createConditionMCycleAccountsForTestingWorkflowInstance.rejected,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', true);
    }
  );
};
