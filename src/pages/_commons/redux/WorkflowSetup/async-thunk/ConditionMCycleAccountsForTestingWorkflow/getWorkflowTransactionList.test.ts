import store from 'app/_libraries/_dof/core/redux/createAppStore';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  workflowInstanceStatus: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      changeInformation: {
        changeSetId: '1111',
        workflowInstanceId: '2222',
        name: 'mock name',
        description: 'mock description',
        workflowNote: 'mock note',
        effectiveDate: new Date()
      }
    }
  }
};

describe('redux-store > workflow-setup >  ConditionMCycleAccountsForTestingWorkflow > getWorkflowTransactionListCondition', () => {
  it('thunk action > with getWorkflowTransactionListCondition', async () => {
    const nextState: any =
      await actionsWorkflowSetup.getWorkflowTransactionListCondition({
        type: ['MONETARY', 'NON_MONETARY']
      })(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            mapping: {
              data: {
                data: {}
              }
            },
            workflowSetup: {
              ...initReducer
            }
          } as RootState),
        {
          workflowService: {
            getWorkflowTransactionListCondition: jest
              .fn()
              .mockResolvedValue({ data: [] })
          }
        }
      );
    expect(nextState.payload.data).toEqual([]);
  });
});
