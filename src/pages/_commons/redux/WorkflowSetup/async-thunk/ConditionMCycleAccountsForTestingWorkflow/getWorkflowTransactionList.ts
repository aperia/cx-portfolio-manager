import { createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import get from 'lodash.get';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';

export interface getWorkflowTransactionArgs {
  type?: string[];
}

export const getWorkflowTransactionListCondition = createAsyncThunk<
  any,
  getWorkflowTransactionArgs,
  ThunkAPIConfig
>(
  'workflowSetup/getWorkflowTransactionListCondition',
  async (args, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;
    const { type } = args;

    const state = thunkAPI.getState();

    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;

    const response = await workflowService.getWorkflowTransactionListCondition({
      searchFields: {
        workflowInstanceId: changeForm.workflowInstanceId?.toString(),
        type
      }
    });

    return response;
  }
);
