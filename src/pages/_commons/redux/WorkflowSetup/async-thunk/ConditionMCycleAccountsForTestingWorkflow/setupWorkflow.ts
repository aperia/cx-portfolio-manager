import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types/workflowSetupState';
import get from 'lodash.get';
import set from 'lodash.set';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';

export const setupConditionMCycleAccountsForTestingWorkflow = createAsyncThunk<
  any,
  WorkflowSetupThunkArg,
  ThunkAPIConfig
>(
  'workflowSetup/setupConditionMCycleAccountsForTestingWorkflow',
  async ({ workflowSetupData }, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

    const state = thunkAPI.getState();

    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;

    const uploadFileForm = state.workflowSetup?.workflowFormSteps.forms[
      'uploadFile'
    ] as any;
    const { transactions } = state.workflowSetup?.workflowFormSteps.forms[
      'selectTransactionsConditionMCycleAccountsForTesting'
    ] as any;

    const updateRequest: UpdateflowInstanceData = {
      workflowInstanceId: changeForm.workflowInstanceId?.toString(),
      status: 'COMPLETE',
      data: {
        workflowSetupData,
        configurations: {
          attachmentId: uploadFileForm.attachmentId,
          transactions
        }
      }
    };

    await workflowService.updateWorkflowInstanceData(updateRequest);

    return { workflowSetupData };
  }
);
export const setupConditionMCycleAccountsForTestingWorkflowBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    setupConditionMCycleAccountsForTestingWorkflow.pending,
    draftState => {
      set(draftState.workflowSetup, 'loading', true);
    }
  );
  builder.addCase(
    setupConditionMCycleAccountsForTestingWorkflow.fulfilled,
    (draftState, action) => {
      set(draftState.workflowSetup, 'loading', false);
      set(draftState.workflowSetup, 'error', false);
      set(
        draftState.workflowFormSteps,
        'workflowSetupData',
        action.payload?.workflowSetupData
      );
    }
  );
  builder.addCase(
    setupConditionMCycleAccountsForTestingWorkflow.rejected,
    draftState => {
      set(draftState.workflowSetup, 'loading', false);
      set(draftState.workflowSetup, 'error', true);
    }
  );
};
