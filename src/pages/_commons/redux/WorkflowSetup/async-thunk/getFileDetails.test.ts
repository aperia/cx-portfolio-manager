import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  workflowSetup: {}
};

describe('redux-store > workflow-setup > getFileDetails', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getFileDetails.pending('getFileDetails', {
        attachmentIds: []
      })
    );
    expect(nextState.workflowSetup.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getFileDetails.fulfilled(true, 'getFileDetails', {
        attachmentIds: []
      })
    );
    expect(nextState.workflowSetup.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getFileDetails.rejected(
        new Error('mock error'),
        'getFileDetails',

        { attachmentIds: [] }
      )
    );
    expect(nextState.workflowSetup.loading).toEqual(false);
  });

  it('thunk action > with files', async () => {
    const nextState: any = await actionsWorkflowSetup.getFileDetails({
      attachmentIds: []
    })(store.dispatch, () => store.getState(), {
      workflowTemplateService: {
        getFileDetails: jest.fn().mockResolvedValue({ data: {} })
      }
    });
    expect(nextState.payload).toEqual({});
  });
});
