import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  elementMetadataNonWorkflow: {}
};

describe('redux-store > workflow-setup > getNonWorkflowMetadata', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getNonWorkflowMetadata.pending(
        'getNonWorkflowMetadata',
        {
          elementMetaData: []
        }
      )
    );
    expect(nextState.elementMetadataNonWorkflow.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getNonWorkflowMetadata.fulfilled(
        true,
        'getNonWorkflowMetadata',
        {
          elementMetaData: []
        }
      )
    );
    expect(nextState.elementMetadataNonWorkflow.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getNonWorkflowMetadata.rejected(
        new Error('mock error'),
        'getNonWorkflowMetadata',

        { elementMetaData: [] }
      )
    );
    expect(nextState.elementMetadataNonWorkflow.loading).toEqual(false);
  });

  it('thunk action > with files', async () => {
    const nextState: any = await actionsWorkflowSetup.getNonWorkflowMetadata([
      'test'
    ])(store.dispatch, () => store.getState(), {
      workflowService: {
        getElementDataByName: jest
          .fn()
          .mockResolvedValue({ data: { elementMetaData: [] } })
      }
    });
    expect(nextState.payload).toEqual({ elements: [] });
  });

  it('thunk action > with files', async () => {
    const nextState: any = await actionsWorkflowSetup.getNonWorkflowMetadata([
      'test'
    ])(store.dispatch, () => store.getState(), {
      workflowService: {
        getElementDataByName: jest.fn().mockResolvedValue({ data: {} })
      }
    });
    expect(nextState.payload).toEqual({ elements: [] });
  });
});
