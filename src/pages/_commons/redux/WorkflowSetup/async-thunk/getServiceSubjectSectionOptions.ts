import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import { get, set } from 'app/_libraries/_dls/lodash';

export const getServiceSubjectSectionOptions = createAsyncThunk<
  any,
  any,
  ThunkAPIConfig
>(
  'workflowSetup/getServiceSubjectSectionOptions',
  async (elementNames: any, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

    const result = await workflowService.getServiceSubjectSectionOptions();

    const { services } = result?.data || [];

    return services;
  }
);

export const getServiceSubjectSectionOptionsBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    getServiceSubjectSectionOptions.pending,
    (draftState, action) => {
      set(draftState.services, 'loading', true);
      set(draftState.services, 'error', false);
    }
  );
  builder.addCase(
    getServiceSubjectSectionOptions.fulfilled,
    (draftState, action) => {
      const data = action.payload;
      set(draftState.services, 'loading', false);
      set(draftState.services, 'data', data);
    }
  );
  builder.addCase(
    getServiceSubjectSectionOptions.rejected,
    (draftState, action) => {
      set(draftState.services, 'loading', false);
      set(draftState.services, 'data', []);
      set(draftState.services, 'error', true);
    }
  );
};
