import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  services: {}
};

describe('redux-store > workflow-setup > getServiceSubjectSectionOptions', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getServiceSubjectSectionOptions.pending(
        'getServiceSubjectSectionOptions',
        {
          elementMetaData: []
        }
      )
    );
    expect(nextState.services.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getServiceSubjectSectionOptions.fulfilled(
        true,
        'getServiceSubjectSectionOptions',
        {
          elementMetaData: []
        }
      )
    );
    expect(nextState.services.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getServiceSubjectSectionOptions.rejected(
        new Error('mock error'),
        'getServiceSubjectSectionOptions',

        { elementMetaData: [] }
      )
    );
    expect(nextState.services.loading).toEqual(false);
  });

  it('thunk action > with files', async () => {
    const nextState: any =
      await actionsWorkflowSetup.getServiceSubjectSectionOptions(['test'])(
        store.dispatch,
        () => store.getState(),
        {
          workflowService: {
            getServiceSubjectSectionOptions: jest
              .fn()
              .mockResolvedValue({ data: { services: {} } })
          }
        }
      );
    expect(nextState.payload).toEqual({});
  });

  it('thunk action > with files', async () => {
    const nextState: any =
      await actionsWorkflowSetup.getServiceSubjectSectionOptions(['test'])(
        store.dispatch,
        () => store.getState(),
        {
          workflowService: {
            getServiceSubjectSectionOptions: jest.fn().mockResolvedValue({})
          }
        }
      );
    expect(nextState.payload).toEqual(undefined);
  });
});
