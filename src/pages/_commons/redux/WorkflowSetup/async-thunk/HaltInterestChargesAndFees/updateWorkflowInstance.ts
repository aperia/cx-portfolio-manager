import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { formatCommon } from 'app/helpers';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';

export const updateHaltInterestChargesAndFeesWorkflowInstance =
  createAsyncThunk<boolean, WorkflowSetupThunkArg, ThunkAPIConfig>(
    'workflowSetup/updateHaltInterestChargesAndFeesWorkflowInstance',
    async ({ workflowSetupData }, thunkAPI) => {
      const { workflowService } = get(thunkAPI, 'extra') as APIMapping;
      const state = thunkAPI.getState();

      const changeForm = state.workflowSetup?.workflowFormSteps.forms[
        'changeInformation'
      ] as FormChangeInfo;

      const uploadFileForm = state.workflowSetup?.workflowFormSteps.forms[
        'uploadFile'
      ] as any;

      const { dateRange } = state.workflowSetup?.workflowFormSteps.forms[
        'configureParameters'
      ] as any;

      const updateRequest = {
        workflowInstanceId: changeForm.workflowInstanceId?.toString(),
        status: 'INCOMPLETE',
        data: {
          workflowSetupData,
          configurations: {
            attachmentId: uploadFileForm.attachmentId,
            date: {
              minDate: formatCommon(dateRange?.start).time.date,
              maxDate: formatCommon(dateRange?.end).time.date
            }
          }
        }
      };

      await workflowService.updateWorkflowInstanceData(updateRequest);

      return true;
    }
  );
export const updateHaltInterestChargesAndFeesWorkflowInstanceBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    updateHaltInterestChargesAndFeesWorkflowInstance.pending,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', true);
      set(draftState.workflowInstance, 'error', false);
    }
  );
  builder.addCase(
    updateHaltInterestChargesAndFeesWorkflowInstance.fulfilled,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', false);
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'isCompletedWorkflow',
        false
      );
    }
  );
  builder.addCase(
    updateHaltInterestChargesAndFeesWorkflowInstance.rejected,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', true);
    }
  );
};
