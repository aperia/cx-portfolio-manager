import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { formatCommon } from 'app/helpers';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { workflowSetupTemplateIdSelector } from '../../select-hooks';

export const createHaltInterestChargesAndFeesWorkflowInstance =
  createAsyncThunk<any, WorkflowSetupThunkArg, ThunkAPIConfig>(
    'workflowSetup/createHaltInterestChargesAndFeesWorkflowInstance',
    async ({ workflowSetupData }, thunkAPI) => {
      const { workflowService } = get(thunkAPI, 'extra') as APIMapping;
      const state = thunkAPI.getState();
      const changeForm = state.workflowSetup?.workflowFormSteps.forms[
        'changeInformation'
      ] as FormChangeInfo;
      const uploadFileForm = state.workflowSetup?.workflowFormSteps.forms[
        'uploadFile'
      ] as any;
      const { dateRange } = state.workflowSetup?.workflowFormSteps.forms[
        'configureParameters'
      ] as any;
      const workflowTemplateId = workflowSetupTemplateIdSelector(state);
      const changeSetId = changeForm?.changeSetId?.toString();
      let workflowInstanceId = changeForm?.workflowInstanceId?.toString();

      if (workflowInstanceId && changeSetId) {
        // Update change of workflow instance
        await workflowService.updateWorkflowInstance({
          workflowInstanceId: workflowInstanceId,
          changeSetId: changeSetId,
          workflowInstanceDetails: {
            note: changeForm.workflowNote
          }
        });
      } else {
        // Create workfow instance
        const workflowRes = await workflowService.createWorkflowInstance({
          workflowInstanceDetails: {
            note: changeForm.workflowNote
          },
          workflowTemplateId: workflowTemplateId || '',
          changeSetId: changeSetId
        });

        workflowInstanceId =
          workflowRes?.data?.workflowInstanceDetails?.id?.toString();
      }

      // Update workflow instance data
      const updateRequest = {
        workflowInstanceId,
        status: 'INCOMPLETE',
        data: {
          workflowSetupData,
          configurations: {
            attachmentId: uploadFileForm?.attachmentId,
            date: {
              minDate: formatCommon(dateRange?.start)?.time.date,
              maxDate: formatCommon(dateRange?.end)?.time.date
            }
          }
        }
      };
      await workflowService.updateWorkflowInstanceData(updateRequest);

      return {
        changeId: changeSetId,
        workflowInstanceId
      };
    }
  );
export const createHaltInterestChargesAndFeesWorkflowInstanceBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    createHaltInterestChargesAndFeesWorkflowInstance.pending,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', true);
      set(draftState.workflowInstance, 'error', false);
    }
  );
  builder.addCase(
    createHaltInterestChargesAndFeesWorkflowInstance.fulfilled,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', false);
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'changeSetId',
        action.payload?.changeId
      );
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'workflowInstanceId',
        action.payload?.workflowInstanceId
      );
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'isCompletedWorkflow',
        false
      );
    }
  );
  builder.addCase(
    createHaltInterestChargesAndFeesWorkflowInstance.rejected,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', true);
    }
  );
};
