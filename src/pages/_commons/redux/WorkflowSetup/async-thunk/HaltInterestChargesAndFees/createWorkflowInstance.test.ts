import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  workflowInstance: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      changeInformation: {
        name: 'mock name',
        description: 'mock description',
        effectiveDate: new Date()
      }
    }
  }
};

jest.mock('app/helpers', () => {
  const modules = jest.requireActual('app/helpers');
  return {
    ...modules,
    formatTimeDefault: () => '01/01/2021'
  };
});

describe('redux-store > workflow-setup > HaltInterestChargesAndFees > createWorkflowInstance', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.createHaltInterestChargesAndFeesWorkflowInstance.pending(
        'createWorkflowInstance',
        { workflowSetupData: [] }
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.createHaltInterestChargesAndFeesWorkflowInstance.fulfilled(
        {},
        'createWorkflowInstance',
        { workflowSetupData: [] }
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.createHaltInterestChargesAndFeesWorkflowInstance.rejected(
        new Error('mock error'),
        'createWorkflowInstance',
        { workflowSetupData: [] }
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(true);
  });

  it('thunk action > without workflowInstanceId & changeId', async () => {
    const nextState: any =
      await actionsWorkflowSetup.createHaltInterestChargesAndFeesWorkflowInstance(
        { workflowSetupData: [] }
      )(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer,
              workflowFormSteps: {
                forms: {
                  changeInformation: {
                    changeSetId: 'changeId'
                  },
                  uploadFile: {},
                  configureParameters: {}
                }
              }
            }
          } as RootState),
        {
          workflowService: {
            createWorkflowInstance: jest.fn().mockResolvedValue({
              data: { workflowInstanceDetails: { id: 'workflowId' } }
            }),
            updateWorkflowInstance: jest.fn().mockResolvedValue({}),
            updateWorkflowInstanceData: jest.fn().mockResolvedValue({})
          }
        }
      );

    expect(nextState.payload).toEqual({
      changeId: 'changeId',
      workflowInstanceId: 'workflowId'
    });
  });

  it('thunk action > with workflowInstanceId & changeId', async () => {
    const state = {
      workflowSetup: {
        workflow: {
          id: undefined
        }
      },
      workflowFormSteps: {
        forms: {
          changeInformation: {
            changeSetId: 'changeId',
            workflowInstanceId: 'workflowId'
          },
          uploadFile: {},
          configureParameters: {}
        }
      }
    };
    const nextState: any =
      await actionsWorkflowSetup.createHaltInterestChargesAndFeesWorkflowInstance(
        { workflowSetupData: [] }
      )(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...state
            }
          } as RootState),
        {
          workflowService: {
            createWorkflowInstance: jest.fn().mockResolvedValue({
              data: { workflowInstanceDetails: { id: 'workflowId' } }
            }),
            updateWorkflowInstance: jest.fn().mockResolvedValue({}),
            updateWorkflowInstanceData: jest.fn().mockResolvedValue({})
          }
        }
      );
    expect(nextState.payload).toEqual({
      changeId: 'changeId',
      workflowInstanceId: 'workflowId'
    });
  });

  it('thunk action > with existing changeId ', async () => {
    const state = {
      workflowSetup: {
        workflow: {
          id: undefined
        }
      },
      workflowFormSteps: {
        forms: {
          changeInformation: {
            changeSetId: 'changeId'
          },
          uploadFile: {},
          configureParameters: {}
        }
      }
    };
    const nextState: any =
      await actionsWorkflowSetup.createHaltInterestChargesAndFeesWorkflowInstance(
        { workflowSetupData: [] }
      )(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...state
            }
          } as RootState),
        {
          workflowService: {
            createWorkflowInstance: jest.fn().mockResolvedValue({
              data: { workflowInstanceDetails: { id: 'workflowId' } }
            }),
            updateWorkflowInstance: jest.fn().mockResolvedValue({}),
            updateWorkflowInstanceData: jest.fn().mockResolvedValue({})
          }
        }
      );
    expect(nextState.payload).toEqual({
      changeId: 'changeId',
      workflowInstanceId: 'workflowId'
    });
  });
});
