import { createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { get } from 'lodash';

export const deleteTemplateFile = createAsyncThunk<
  any,
  MagicKeyValue,
  ThunkAPIConfig
>('workflowSetup/deleteTemplateFile', async (args, thunkAPI) => {
  const { attachmentId } = args;
  const { workflowTemplateService } = get(thunkAPI, 'extra') as APIMapping;

  // Take mapping model
  const dataPost = {
    attachmentId: attachmentId.toString()
  };

  const response = await workflowTemplateService.deleteWorkflowTemplate(
    dataPost
  );

  return response;
});
