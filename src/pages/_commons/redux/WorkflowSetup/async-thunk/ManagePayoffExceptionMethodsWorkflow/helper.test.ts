import { MethodFieldParameterEnum } from 'app/constants/enums';
import { parseMethodData } from './helper';

describe('redux-store > workflow-setup > PayoffExceptionMethodsWorkflow > designLoanOffers > helper', () => {
  it('parseMethodData', () => {
    // eslint-disable-next-line prefer-const
    let input: any;
    let result = parseMethodData(input);
    expect(result).toEqual([]);

    input = {
      methods: [
        {
          name: 'name',
          modeledFrom: 'modeledFrom',
          comment: 'comment',
          serviceSubjectSection: 'serviceSubjectSection',
          parameters: [
            {
              name: MethodFieldParameterEnum.PayoffExceptionStatementValue,
              newValue: '1111'
            }
          ]
        },
        {
          name: 'name1',
          serviceSubjectSection: 'serviceSubjectSection1',
          parameters: [
            {
              name: MethodFieldParameterEnum.PayoffExceptionAmountToAvoidFinanceChargesSetting,
              newValue: '1111'
            }
          ]
        }
      ]
    };

    result = parseMethodData(input);
    expect(result).toEqual([
      {
        id: '',
        name: 'name',
        modeledFrom: 'modeledFrom',
        comment: 'comment',
        serviceSubjectSection: 'serviceSubjectSection',
        parameters: [
          {
            name: MethodFieldParameterEnum.PayoffExceptionStatementValue,
            newValue: '1111'
          }
        ]
      },
      {
        id: '',
        name: 'name1',
        modeledFrom: null,
        comment: '',
        serviceSubjectSection: 'serviceSubjectSection1',
        parameters: [
          {
            name: MethodFieldParameterEnum.PayoffExceptionAmountToAvoidFinanceChargesSetting,
            newValue: '1111'
          }
        ]
      }
    ]);
  });
});
