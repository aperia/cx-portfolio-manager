import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  createNewChange: {}
};

jest.mock('app/helpers', () => {
  const modules = jest.requireActual('app/helpers');
  return {
    ...modules,
    formatTimeDefault: () => '01/01/2022',
    addCurrentTime: () => new Date('01/01/2022')
  };
});

describe('redux-store > workflow-setup > createNewChange', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.createNewChange.pending('createNewChange', undefined)
    );
    expect(nextState.createNewChange.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.createNewChange.fulfilled({}, 'createNewChange', {})
    );
    expect(nextState.createNewChange.loading).toEqual(false);
    expect(nextState.createNewChange.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.createNewChange.rejected(
        new Error('mock error'),
        'createNewChange',
        undefined
      )
    );
    expect(nextState.createNewChange.loading).toEqual(false);
    expect(nextState.createNewChange.error).toEqual(true);
  });

  it('thunk action > with change form data', async () => {
    const nextState: any = await actionsWorkflowSetup.createNewChange({
      changeName: 'mock name',
      changeDescription: 'mock description',
      effectiveDate: '01/01/2021'
    })(store.dispatch, store.getState, {
      changeService: {
        createChangeSet: jest.fn().mockResolvedValue({
          data: {
            id: '2222',
            name: 'mock name',
            description: 'mock description',
            effectiveDate: '01/01/2021'
          }
        })
      }
    });

    expect(nextState.payload.changeSetId).toEqual('2222');
    expect(nextState.payload.changeName).toEqual('mock name');
    expect(nextState.payload.changeDescription).toEqual('mock description');
  });

  it('thunk action > without change form data', async () => {
    const nextState: any = await actionsWorkflowSetup.createNewChange({})(
      store.dispatch,
      () => store.getState(),
      {
        changeService: {
          createChangeSet: jest.fn().mockResolvedValue({})
        }
      }
    );
    expect(nextState.payload.changeName).toBeUndefined();
    expect(nextState.payload.changeDescription).toBeUndefined();
  });
});
