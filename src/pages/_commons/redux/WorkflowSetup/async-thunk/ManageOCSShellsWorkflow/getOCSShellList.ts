import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { mappingDataFromArray } from 'app/helpers';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import { set } from 'lodash';
import get from 'lodash.get';
import { STATUS } from 'pages/WorkflowManageOCSShells/ConfigureParameters/constants';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';

export const getOCSShellList = createAsyncThunk<any, undefined, ThunkAPIConfig>(
  'workflowSetup/getOCSShellList',
  async (undefined, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;
    const state = thunkAPI.getState();
    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;

    const response = await workflowService.getOCSShellList({
      searchFields: {
        workflowInstanceId: changeForm.workflowInstanceId?.toString()
      }
    });

    // Mapping
    const ocsShellListMapping = state.mapping?.data?.ocsShell;
    const mappedData = mappingDataFromArray(
      response?.data?.ocsShells,
      ocsShellListMapping!
    ).map((i, idx) => ({
      ...i,
      id: `${Date.now() + idx}`,
      status: STATUS.NotUpdated,
      modelFromName: i.shellName
    }));

    return { ocsShells: mappedData };
  }
);

export const getOCSShellListBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(getOCSShellList.pending, (draftState, action) => {
    set(draftState.ocsShellData, 'loading', true);
    set(draftState.ocsShellData, 'error', false);
  });
  builder.addCase(getOCSShellList.fulfilled, (draftState, action) => {
    set(draftState.ocsShellData, 'loading', false);
    set(draftState.ocsShellData, 'error', false);
    set(draftState.ocsShellData, 'ocsShells', action.payload?.ocsShells);
  });
  builder.addCase(getOCSShellList.rejected, (draftState, action) => {
    set(draftState.ocsShellData, 'loading', false);
    set(draftState.ocsShellData, 'error', true);
  });
};
