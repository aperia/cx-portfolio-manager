import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

describe('redux-store > workflow > Manage OCS Shells >  getOCSShellList', () => {
  const initReducer: any = {
    workflowInstance: {
      loading: false,
      error: false
    },
    workflowSetup: {
      workflow: {
        id: undefined
      }
    },
    workflowFormSteps: {
      forms: {
        changeInformation: {
          name: 'mock name',
          description: 'mock description',
          effectiveDate: new Date()
        }
      }
    },
    ocsShellData: {
      loading: false,
      error: false,
      ocsShells: []
    }
  };

  it('thunk action > with existing changeId ', async () => {
    const result = {
      data: { ocsShells: [{ shellName: 'name' }] }
    };
    const workflowSetupState = {
      workflowSetup: { workflow: { id: undefined } },
      workflowFormSteps: {
        forms: {
          changeInformation: { workflowInstanceId: 'workflowInstanceId' },
          uploadFile: {},
          configureParameters: {}
        }
      }
    };
    const mappingState = {
      data: {
        ocsShell: {
          shellName: 'shellName',
          transactionProfile: 'transactionProfile',
          fieldProfile: 'fieldProfile',
          sysPrinProfile: 'sysPrinProfile',
          description: 'description',
          accountProfile: 'accountProfile',
          operatorCode: 'operatorCode',
          singleSession: 'singleSession',
          employeeId: 'employeeId',
          supervisorShell: 'supervisorShell',
          reportManagementSystemId: 'reportManagementSystemId',
          departmentId: 'departmentId',
          level: 'level',
          masterSignOnOption: 'masterSignOnOption',
          pwdExpirationOption: 'pwdExpirationOption',
          expireDate: 'expireDate',
          neverDisable: 'neverDisable',
          comment: 'comment',
          initialPwd: 'initialPwd',
          confirmPwd: 'confirmPwd',
          shellStatus: 'status',
          daysAllowed: 'daysAllowed',
          timesAllowed: 'timesAllowed'
        }
      }
    };
    const nextState: any = await actionsWorkflowSetup.getOCSShellList()(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          workflowSetup: { ...workflowSetupState },
          mapping: { ...mappingState }
        } as RootState),
      {
        workflowService: {
          getOCSShellList: jest.fn().mockResolvedValue(result)
        }
      }
    );
    expect(nextState.payload.ocsShells[0].shellName).toEqual('name');
  });

  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getOCSShellList.pending('getOCSShellList', undefined)
    );
    expect(nextState.ocsShellData.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getOCSShellList.fulfilled(
        {},
        'getOCSShellList',
        undefined
      )
    );
    expect(nextState.ocsShellData.loading).toEqual(false);
    expect(nextState.ocsShellData.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getOCSShellList.rejected(
        new Error('mock error'),
        'getOCSShellList',
        undefined
      )
    );
    expect(nextState.ocsShellData.loading).toEqual(false);
    expect(nextState.ocsShellData.error).toEqual(true);
  });
});
