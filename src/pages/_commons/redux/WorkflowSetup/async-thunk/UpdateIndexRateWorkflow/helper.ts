export const parseMethodData = (...methodForm: any[]) => {
  let methodList: any[] = [];
  methodForm?.forEach(form => {
    if (form?.methods?.length > 0)
      methodList = methodList.concat(
        form.methods?.map((m: any) => {
          return {
            id: '',
            name: m.name,
            modeledFrom: m.modeledFrom || null,
            comment: m.comment || '',
            serviceSubjectSection: m.serviceSubjectSection,
            parameters: m.parameters
          };
        })
      );
  });
  return methodList;
};
