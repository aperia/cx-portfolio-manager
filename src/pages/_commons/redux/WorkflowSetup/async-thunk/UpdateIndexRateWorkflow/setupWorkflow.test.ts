import { WorkflowMethodListData } from 'app/fixtures/workflow-method-list';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  workflowInstanceStatus: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      changeInformation: {
        changeSetId: '1111',
        workflowInstanceId: '2222',
        name: 'mock name',
        description: 'mock description',
        workflowNote: 'mock note',
        effectiveDate: new Date()
      },
      returnedCheckCharges: {
        methods: WorkflowMethodListData
      }
    }
  }
};

jest.mock('app/helpers', () => {
  const modules = jest.requireActual('app/helpers');
  return {
    ...modules,
    formatTimeDefault: () => '01/01/2021'
  };
});

describe('redux-store > workflow-setup > UpdateIndexRateWorkflow > setupWorkflow', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.setupUpdateIndexRateWorkflow.pending(
        'UpdateIndexRateWorkflow',
        {}
      )
    );
    expect(nextState.workflowSetup.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.setupUpdateIndexRateWorkflow.fulfilled(
        true,
        'UpdateIndexRateWorkflow',
        {}
      )
    );
    expect(nextState.workflowSetup.loading).toEqual(false);
    expect(nextState.workflowSetup.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.setupUpdateIndexRateWorkflow.rejected(
        new Error('mock error'),
        'setupWorkflow',
        {}
      )
    );
    expect(nextState.workflowSetup.loading).toEqual(false);
    expect(nextState.workflowSetup.error).toEqual(true);
  });

  it('thunk action > with workflowInstanceId & changeId', async () => {
    const mockRes = { data: { workflowSetupData: [] } };
    const updateWorkflowDataFn = jest.fn().mockResolvedValue(mockRes);

    const nextState: any =
      await actionsWorkflowSetup.setupUpdateIndexRateWorkflow({
        workflowSetupData: []
      })(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer
            }
          } as RootState),
        {
          workflowService: {
            updateWorkflowInstanceData: updateWorkflowDataFn
          }
        }
      );
    expect(nextState.payload.workflowSetupData).toBeUndefined();
    expect(updateWorkflowDataFn).toHaveBeenCalled();
  });
});
