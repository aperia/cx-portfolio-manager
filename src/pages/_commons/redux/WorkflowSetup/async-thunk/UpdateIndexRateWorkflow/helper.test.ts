import { parseMethodData } from './helper';

describe('parseMethodData', () => {
  const data = {
    methods: [
      {
        id: '1',
        name: 'name',
        modeledFrom: 'modeledFrom',
        comment: 'comment',
        serviceSubjectSection: 'serviceSubjectSection',
        parameters: [{ id: '1' }]
      }
    ]
  };
  it('should test parseMethodData', () => {
    const value = parseMethodData(data);

    expect(value).toBeTruthy();
  });

  it('should test parseMethodData', () => {
    const value = parseMethodData({
      methods: [
        {
          id: '1',
          name: 'name',
          serviceSubjectSection: 'serviceSubjectSection',
          parameters: [{ id: '1' }]
        }
      ]
    });

    expect(value).toBeTruthy();
  });
});
