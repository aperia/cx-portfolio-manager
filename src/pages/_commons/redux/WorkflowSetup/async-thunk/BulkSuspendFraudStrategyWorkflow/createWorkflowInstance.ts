import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';
import { ConfigureParametersFormValue } from 'pages/WorkflowBulkSuspendFraudStrategy/BulkSuspendFraudStrategyStep';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { workflowSetupTemplateIdSelector } from '../../select-hooks';

export const createBulkSuspendFraudStrategyWorkflowInstance = createAsyncThunk<
  any,
  WorkflowSetupThunkArg,
  ThunkAPIConfig
>(
  'workflowSetup/createBulkSuspendFraudStrategyWorkflowInstance',
  async ({ workflowSetupData }, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;
    const state = thunkAPI.getState();

    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;
    const uploadFileForm = state.workflowSetup?.workflowFormSteps.forms[
      'uploadFile'
    ] as any;

    const workflowTemplateId = workflowSetupTemplateIdSelector(state);
    const changeSetId = changeForm?.changeSetId?.toString();
    let workflowInstanceId = changeForm?.workflowInstanceId?.toString();

    if (workflowInstanceId && changeSetId) {
      await workflowService.updateWorkflowInstance({
        workflowInstanceId,
        changeSetId,
        workflowInstanceDetails: {
          note: changeForm.workflowNote
        }
      });
    } else {
      const workflowRes = await workflowService.createWorkflowInstance({
        workflowInstanceDetails: {
          note: changeForm?.workflowNote
        },
        workflowTemplateId: workflowTemplateId || '',
        changeSetId
      });

      workflowInstanceId =
        workflowRes?.data?.workflowInstanceDetails?.id?.toString();
    }

    const configureParametersForm = state.workflowSetup?.workflowFormSteps
      .forms['configureParameters'] as ConfigureParametersFormValue;

    const dateRange = configureParametersForm?.parameters?.dateRange;
    const date = { minDate: dateRange?.start, maxDate: dateRange?.end };
    const parameters = [
      { name: 'strategy', value: configureParametersForm?.parameters?.strategy }
    ];

    const updateRequest = {
      workflowInstanceId,
      status: 'INCOMPLETE',
      data: {
        workflowSetupData,
        configurations: {
          attachmentId: uploadFileForm.attachmentId,
          date,
          parameters
        }
      }
    };

    await workflowService.updateWorkflowInstanceData(updateRequest);

    return { changeSetId, workflowInstanceId };
  }
);
export const createBulkSuspendFraudStrategyWorkflowInstanceBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    createBulkSuspendFraudStrategyWorkflowInstance.pending,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', true);
      set(draftState.workflowInstance, 'error', false);
    }
  );
  builder.addCase(
    createBulkSuspendFraudStrategyWorkflowInstance.fulfilled,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', false);
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'changeSetId',
        action.payload?.changeSetId
      );
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'workflowInstanceId',
        action.payload?.workflowInstanceId
      );
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'isCompletedWorkflow',
        false
      );
    }
  );
  builder.addCase(
    createBulkSuspendFraudStrategyWorkflowInstance.rejected,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', true);
    }
  );
};
