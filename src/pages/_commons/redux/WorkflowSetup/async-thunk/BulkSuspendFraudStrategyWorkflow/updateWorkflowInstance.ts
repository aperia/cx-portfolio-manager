import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';
import { ConfigureParametersFormValue } from 'pages/WorkflowBulkSuspendFraudStrategy/BulkSuspendFraudStrategyStep';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';

export const updateBulkSuspendFraudStrategyWorkflowInstance = createAsyncThunk<
  boolean,
  WorkflowSetupThunkArg,
  ThunkAPIConfig
>(
  'workflowSetup/updateBulkSuspendFraudStrategyWorkflowInstance',
  async ({ workflowSetupData }, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;
    const state = thunkAPI.getState();

    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;
    const uploadFileForm = state.workflowSetup?.workflowFormSteps.forms[
      'uploadFile'
    ] as any;
    const configureParametersForm = state.workflowSetup?.workflowFormSteps
      .forms['configureParameters'] as ConfigureParametersFormValue;

    const dateRange = configureParametersForm?.parameters?.dateRange;
    const date = { minDate: dateRange?.start, maxDate: dateRange?.end };
    const parameters = [
      { name: 'strategy', value: configureParametersForm?.parameters?.strategy }
    ];

    const updateRequest = {
      workflowInstanceId: changeForm?.workflowInstanceId?.toString(),
      status: 'INCOMPLETE',
      data: {
        workflowSetupData,
        configurations: {
          attachmentId: uploadFileForm?.attachmentId,
          date,
          parameters
        }
      }
    };

    await workflowService.updateWorkflowInstanceData(updateRequest);

    return true;
  }
);
export const updateBulkSuspendFraudStrategyWorkflowInstanceBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    updateBulkSuspendFraudStrategyWorkflowInstance.pending,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', true);
      set(draftState.workflowInstance, 'error', false);
    }
  );
  builder.addCase(
    updateBulkSuspendFraudStrategyWorkflowInstance.fulfilled,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', false);
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'isCompletedWorkflow',
        false
      );
    }
  );
  builder.addCase(
    updateBulkSuspendFraudStrategyWorkflowInstance.rejected,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', true);
    }
  );
};
