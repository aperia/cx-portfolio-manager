import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types/workflowSetupState';
import get from 'lodash.get';
import set from 'lodash.set';
import { FormGettingStart } from 'pages/WorkflowChangeInterestRates/GettingStartStep';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { parseMethodData } from '../UpdateIndexRateWorkflow/helper';

export const setupChangeInterestRatesWorkflow = createAsyncThunk<
  any,
  WorkflowSetupThunkArg,
  ThunkAPIConfig
>(
  'workflowSetup/setupChangeInterestRatesWorkflow',
  async ({ workflowSetupData }, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

    const state = thunkAPI.getState();

    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;

    const configureParametersInterestDefault = state.workflowSetup
      ?.workflowFormSteps.forms[
      'configureParametersInterestDefault'
    ] as FormGettingStart;
    const configureParametersInterestMethod = state.workflowSetup
      ?.workflowFormSteps.forms[
      'configureParametersInterestMethod'
    ] as FormGettingStart;
    const configureParametersInterestInventive = state.workflowSetup
      ?.workflowFormSteps.forms[
      'configureParametersInterestInventive'
    ] as FormGettingStart;

    const updateRequest: UpdateflowInstanceData = {
      workflowInstanceId: changeForm.workflowInstanceId?.toString(),
      status: 'COMPLETE',
      data: {
        workflowSetupData,
        configurations: {
          methodList: parseMethodData(
            configureParametersInterestDefault,
            configureParametersInterestMethod,
            configureParametersInterestInventive
          )
        }
      }
    };

    await workflowService.updateWorkflowInstanceData(updateRequest);

    return true;
  }
);
export const setupChangeInterestRatesWorkflowBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(setupChangeInterestRatesWorkflow.pending, draftState => {
    set(draftState.workflowSetup, 'loading', true);
  });
  builder.addCase(
    setupChangeInterestRatesWorkflow.fulfilled,
    (draftState, action) => {
      set(draftState.workflowSetup, 'loading', false);
      set(draftState.workflowSetup, 'error', false);
    }
  );
  builder.addCase(setupChangeInterestRatesWorkflow.rejected, draftState => {
    set(draftState.workflowSetup, 'loading', false);
    set(draftState.workflowSetup, 'error', true);
  });
};
