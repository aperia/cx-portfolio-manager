import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';
import { FormGettingStart } from 'pages/WorkflowChangeInterestRates/GettingStartStep';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { parseMethodData } from '../UpdateIndexRateWorkflow/helper';

export const updateChangeInterestRatesWorkflowInstance = createAsyncThunk<
  any,
  WorkflowSetupThunkArg,
  ThunkAPIConfig
>(
  'workflowSetup/updateChangeInterestRatesWorkflowInstance',
  async (args, thunkAPI) => {
    const { workflowSetupData } = args;
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

    const state = thunkAPI.getState();

    const gettingStart = state.workflowSetup?.workflowFormSteps.forms[
      'gettingStart'
    ] as FormGettingStart;

    // invalid save
    if (
      !gettingStart?.interestDefault &&
      !gettingStart?.interestMethod &&
      !gettingStart?.inventivePricing
    ) {
      return;
    }

    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;

    const configureParametersInterestDefault = state.workflowSetup
      ?.workflowFormSteps.forms[
      'configureParametersInterestDefault'
    ] as FormGettingStart;
    const configureParametersInterestMethod = state.workflowSetup
      ?.workflowFormSteps.forms[
      'configureParametersInterestMethod'
    ] as FormGettingStart;
    const configureParametersInterestInventive = state.workflowSetup
      ?.workflowFormSteps.forms[
      'configureParametersInterestInventive'
    ] as FormGettingStart;

    const updateRequest: UpdateflowInstanceData = {
      workflowInstanceId: changeForm.workflowInstanceId?.toString(),
      status: 'INCOMPLETE',
      data: {
        workflowSetupData,
        configurations: {
          methodList: parseMethodData(
            configureParametersInterestDefault,
            configureParametersInterestMethod,
            configureParametersInterestInventive
          )
        }
      }
    };

    await workflowService.updateWorkflowInstanceData(updateRequest);
    return true;
  }
);
export const updateChangeInterestRatesWorkflowInstanceBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    updateChangeInterestRatesWorkflowInstance.pending,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', true);
      set(draftState.workflowInstance, 'error', false);
    }
  );
  builder.addCase(
    updateChangeInterestRatesWorkflowInstance.fulfilled,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', false);
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'isCompletedWorkflow',
        false
      );
    }
  );
  builder.addCase(
    updateChangeInterestRatesWorkflowInstance.rejected,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', true);
    }
  );
};
