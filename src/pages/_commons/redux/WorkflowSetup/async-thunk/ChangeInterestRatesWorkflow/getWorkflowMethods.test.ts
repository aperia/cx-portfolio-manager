import {
  GetMethodListData,
  GetMethodListDataMapping
} from 'app/fixtures/workflow-method-list';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';
import { defaultWorkflowMethodsFilter } from '../../reducers';

const initReducer: any = {
  workflowMethods: {
    methods: [],
    loading: false,
    error: false,
    expandingMethod: '',
    dataFilter: defaultWorkflowMethodsFilter
  }
};

describe('redux-store > workflow-setup > ChangeInterestRatesWorkflow > getWorkflowMethods', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getChangeInterestRatesWorkflowMethods.pending(
        'getWorkflowMethods',
        {
          serviceSubjectSection: 'mock data',
          defaultMethodId: '1'
        }
      )
    );
    expect(nextState.workflowMethods.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getChangeInterestRatesWorkflowMethods.fulfilled(
        {},
        'getWorkflowMethods',
        {
          serviceSubjectSection: 'mock data',
          defaultMethodId: '1'
        }
      )
    );
    expect(nextState.workflowMethods.loading).toEqual(false);
    expect(nextState.workflowMethods.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getChangeInterestRatesWorkflowMethods.rejected(
        new Error('mock error'),
        'getWorkflowMethods',
        {
          serviceSubjectSection: 'mock data',
          defaultMethodId: '1'
        }
      )
    );
    expect(nextState.workflowMethods.loading).toEqual(false);
    expect(nextState.workflowMethods.error).toEqual(true);
  });

  it('thunk action > with defaultMethodId', async () => {
    const nextState: any =
      await actionsWorkflowSetup.getChangeInterestRatesWorkflowMethods({
        serviceSubjectSection: 'mock data',
        defaultMethodId: '1'
      })(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer
            },
            mapping: { data: { workflowMethodList: GetMethodListDataMapping } }
          } as RootState),
        {
          workflowService: {
            getWorkflowMethods: jest.fn().mockResolvedValue({
              data: { methodList: GetMethodListData }
            })
          }
        }
      );
    expect(nextState.payload.methods.length).toEqual(2);
  });

  it('thunk action > without defaultMethodId', async () => {
    const nextState: any =
      await actionsWorkflowSetup.getChangeInterestRatesWorkflowMethods({
        serviceSubjectSection: 'mock data'
      })(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer
            },
            mapping: { data: { workflowMethodList: GetMethodListDataMapping } }
          } as RootState),
        {
          workflowService: {
            getWorkflowMethods: jest.fn().mockResolvedValue({
              data: { methodList: GetMethodListData }
            })
          }
        }
      );
    expect(nextState.payload.methods.length).toEqual(2);
  });
});
