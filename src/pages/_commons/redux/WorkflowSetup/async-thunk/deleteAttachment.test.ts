import store from 'app/_libraries/_dof/core/redux/createAppStore';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';

describe('redux-store > workflow-setup > deleteTemplateFile', () => {
  it('thunk action > ', async () => {
    const nextState: any = await actionsWorkflowSetup.deleteTemplateFile({
      attachmentId: ''
    })(store.dispatch, () => store.getState(), {
      workflowTemplateService: {
        deleteWorkflowTemplate: jest.fn().mockResolvedValue({})
      }
    });
    expect(nextState.payload).toEqual({});
  });
});
