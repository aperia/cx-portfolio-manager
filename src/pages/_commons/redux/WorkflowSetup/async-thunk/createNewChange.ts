import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { FormatTime } from 'app/constants/enums';
import { addCurrentTime, formatTimeDefault } from 'app/helpers';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';
import { workflowSetupTemplateIdSelector } from '../select-hooks';

export const createNewChange = createAsyncThunk<any, any, ThunkAPIConfig>(
  'workflowSetup/createNewChange',
  async (changeForm, thunkAPI) => {
    const { changeService } = get(thunkAPI, 'extra') as APIMapping;

    const state = thunkAPI.getState();

    const workflowTemplateId = workflowSetupTemplateIdSelector(state);

    const changeDetailData = {
      name: changeForm.changeName || '',
      description: changeForm.changeDescription || '',
      effectiveDate: formatTimeDefault(
        addCurrentTime(changeForm.effectiveDate!).toISOString(),
        FormatTime.DateServer
      )!
    };
    const changeRes = await changeService.createChangeSet({
      workflowTemplateId: workflowTemplateId || '',
      changeSetDetails: {
        ...changeDetailData
      }
    });

    const { id, name, description, effectiveDate } = changeRes?.data || {};

    return {
      changeSetId: `${id}`,
      changeName: name,
      effectiveDate: new Date(effectiveDate),
      changeDescription: description
    };
  }
);

export const createNewChangeBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(createNewChange.pending, (draftState, action) => {
    set(draftState.createNewChange, 'loading', true);
    set(draftState.createNewChange, 'error', false);
  });
  builder.addCase(createNewChange.fulfilled, (draftState, action) => {
    set(draftState.createNewChange, 'loading', false);
    set(draftState.createNewChange, 'error', false);
  });
  builder.addCase(createNewChange.rejected, (draftState, action) => {
    set(draftState.createNewChange, 'loading', false);
    set(draftState.createNewChange, 'error', true);
  });
};
