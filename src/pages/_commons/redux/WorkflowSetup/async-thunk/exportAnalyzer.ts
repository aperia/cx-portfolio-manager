import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { MIME_TYPE } from 'app/constants/mine-type';
import { mappingDataFromObj } from 'app/helpers';
import { base64toBlob, downloadBlobFile } from 'app/helpers/fileUtilities';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';

interface ExportElementType {
  name: string;
  values: string[];
}

export interface ExportAnalyzerArg {
  parameters: ExportElementType[];
}

export const exportAnalyzer = createAsyncThunk<
  any,
  ExportAnalyzerArg,
  ThunkAPIConfig
>(
  'workflowSetup/exportAnalyzer',
  async (searchFields: ExportAnalyzerArg, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;
    const state = thunkAPI.getState();
    const downloadFileMapping = state.mapping?.data?.downloadTemplateFile;
    const response = await workflowService.exportAnalyzer(searchFields);

    const attachmentData = response?.data?.attachment;
    const file = mappingDataFromObj(attachmentData, downloadFileMapping!);
    const {
      fileStream = '',
      mimeType: mimeTypeDownload = '',
      filename: filenameDownload = ''
    } = file;

    let fileName = filenameDownload;
    if (filenameDownload.split('.').length < 2) {
      const ext = MIME_TYPE[mimeTypeDownload]?.extensions[0] || '';
      fileName = `${filenameDownload}.${ext}`;
    }
    const blol = base64toBlob(fileStream, mimeTypeDownload);
    downloadBlobFile(blol, fileName);

    return true;
  }
);

export const exportAnalyzerBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(exportAnalyzer.pending, (draftState, action) => {
    set(draftState.exportAnalyzer, 'loading', true);
    set(draftState.exportAnalyzer, 'error', false);
  });
  builder.addCase(exportAnalyzer.fulfilled, (draftState, action) => {
    set(draftState.exportAnalyzer, 'loading', false);
    set(draftState.exportAnalyzer, 'error', false);
  });
  builder.addCase(exportAnalyzer.rejected, (draftState, action) => {
    set(draftState.exportAnalyzer, 'loading', false);
    set(draftState.exportAnalyzer, 'error', true);
  });
};
