import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';
import { ManageStatementProductionSettingsFormValue } from 'pages/WorkflowManageStatementProductionSettings/ManageStatementProductionSettingsStep';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { parseMethodData } from './helper';

export const updateManageStatementProductionSettingsWorkflowInstance =
  createAsyncThunk<any, WorkflowSetupThunkArg, ThunkAPIConfig>(
    'workflowSetup/updateManageStatementProductionSettingsWorkflowInstance',
    async ({ workflowSetupData }, thunkAPI) => {
      const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

      const state = thunkAPI.getState();

      const changeForm = state.workflowSetup?.workflowFormSteps.forms[
        'changeInformation'
      ] as FormChangeInfo;

      const manageStatementProductionSettingsForm = state.workflowSetup
        ?.workflowFormSteps.forms[
        'ManageStatementProductionSettings'
      ] as ManageStatementProductionSettingsFormValue;

      const methodList = parseMethodData(manageStatementProductionSettingsForm);

      const updateRequest: UpdateflowInstanceData = {
        workflowInstanceId: changeForm.workflowInstanceId?.toString(),
        status: 'INCOMPLETE',
        data: {
          workflowSetupData,
          configurations: {
            methodList
          }
        }
      };

      await workflowService.updateWorkflowInstanceData(updateRequest);
      return true;
    }
  );
export const updateManageStatementProductionSettingsWorkflowInstanceBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    updateManageStatementProductionSettingsWorkflowInstance.pending,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', true);
      set(draftState.workflowInstance, 'error', false);
    }
  );
  builder.addCase(
    updateManageStatementProductionSettingsWorkflowInstance.fulfilled,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', false);
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'isCompletedWorkflow',
        false
      );
    }
  );
  builder.addCase(
    updateManageStatementProductionSettingsWorkflowInstance.rejected,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', true);
    }
  );
};
