import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types/workflowSetupState';
import get from 'lodash.get';
import set from 'lodash.set';
import { ManageStatementProductionSettingsFormValue } from 'pages/WorkflowManageStatementProductionSettings/ManageStatementProductionSettingsStep';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { parseMethodData } from './helper';

export const setupManageStatementProductionSettingsWorkflow = createAsyncThunk<
  any,
  WorkflowSetupThunkArg,
  ThunkAPIConfig
>(
  'workflowSetup/setupManageStatementProductionSettingsWorkflow',
  async ({ workflowSetupData }, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

    const state = thunkAPI.getState();

    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;

    const manageStatementProductionSettingsForm = state.workflowSetup
      ?.workflowFormSteps.forms[
      'ManageStatementProductionSettings'
    ] as ManageStatementProductionSettingsFormValue;

    const methodList = parseMethodData(manageStatementProductionSettingsForm);

    const updateRequest: UpdateflowInstanceData = {
      workflowInstanceId: changeForm.workflowInstanceId?.toString(),
      status: 'COMPLETE',
      data: {
        workflowSetupData,
        configurations: {
          methodList
        }
      }
    };

    await workflowService.updateWorkflowInstanceData(updateRequest);

    return true;
  }
);
export const setupManageStatementProductionSettingsWorkflowBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    setupManageStatementProductionSettingsWorkflow.pending,
    draftState => {
      set(draftState.workflowSetup, 'loading', true);
    }
  );
  builder.addCase(
    setupManageStatementProductionSettingsWorkflow.fulfilled,
    (draftState, action) => {
      set(draftState.workflowSetup, 'loading', false);
      set(draftState.workflowSetup, 'error', false);
    }
  );
  builder.addCase(
    setupManageStatementProductionSettingsWorkflow.rejected,
    draftState => {
      set(draftState.workflowSetup, 'loading', false);
      set(draftState.workflowSetup, 'error', true);
    }
  );
};
