import { createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import get from 'lodash.get';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';

export interface getWorkflowTransactionArgs {
  type?: string[];
}

export const getWorkflowTransactionList = createAsyncThunk<
  any,
  getWorkflowTransactionArgs,
  ThunkAPIConfig
>(
  'workflowSetup/getWorkflowTransactionList',
  async ({ type = '' }, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;
    const state = thunkAPI.getState();
    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;

    const response = await workflowService.getWorkflowTransactionList({
      searchFields: {
        workflowInstanceId: changeForm.workflowInstanceId?.toString(),
        type
      }
    });

    return response;
  }
);
