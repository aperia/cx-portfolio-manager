import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types/workflowSetupState';
import get from 'lodash.get';
import set from 'lodash.set';
import { DesignPromotionsFormValue } from 'pages/WorkflowDesignPromotions/DesignPromotionsStep';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { parseMethodData } from './helper';

export const setupDesignPromotionsWorkflow = createAsyncThunk<
  any,
  WorkflowSetupThunkArg,
  ThunkAPIConfig
>(
  'workflowSetup/setupDesignPromotionsWorkflow',
  async ({ workflowSetupData }, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

    const state = thunkAPI.getState();

    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;

    const designLoanOfferForm = state.workflowSetup?.workflowFormSteps.forms[
      'designPromotions'
    ] as DesignPromotionsFormValue;

    const methodList = parseMethodData(designLoanOfferForm);
    const updateRequest: UpdateflowInstanceData = {
      workflowInstanceId: changeForm.workflowInstanceId?.toString(),
      status: 'COMPLETE',
      data: {
        workflowSetupData,
        configurations: {
          methodList
        }
      }
    };

    await workflowService.updateWorkflowInstanceData(updateRequest);

    return true;
  }
);
export const setupDesignPromotionsWorkflowBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(setupDesignPromotionsWorkflow.pending, draftState => {
    set(draftState.workflowSetup, 'loading', true);
  });
  builder.addCase(
    setupDesignPromotionsWorkflow.fulfilled,
    (draftState, action) => {
      set(draftState.workflowSetup, 'loading', false);
      set(draftState.workflowSetup, 'error', false);
    }
  );
  builder.addCase(setupDesignPromotionsWorkflow.rejected, draftState => {
    set(draftState.workflowSetup, 'loading', false);
    set(draftState.workflowSetup, 'error', true);
  });
};
