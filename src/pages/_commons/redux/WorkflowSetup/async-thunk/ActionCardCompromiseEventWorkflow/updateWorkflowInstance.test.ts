import { WorkflowMethodListData } from 'app/fixtures/workflow-method-list';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import { ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME } from 'pages/WorkflowActionCardCompromiseEvent/ActionCardCompromiseEventSelectActionsStep/helper';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  workflowInstance: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      changeInformation: {
        changeSetId: '1111',
        workflowInstanceId: '2222',
        name: 'mock name',
        description: 'mock description',
        workflowNote: 'mock note',
        effectiveDate: new Date()
      },
      returnedCheckCharges: {
        methods: WorkflowMethodListData
      },
      configureParameters: {
        optionalParameters: {
          radio: ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SINGLE_ENTITY,
          question2:
            'Account Transfer using the AT, Account Transfers transaction.'
        },
        parameters: {
          radio: ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SINGLE_ENTITY,
          question2:
            'Account Transfer using the AT, Account Transfers transaction.'
        }
      },
      selectActions: {
        action: {
          radio: ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SINGLE_ENTITY,
          question2:
            'Account Transfer using the AT, Account Transfers transaction.'
        }
      }
    }
  }
};

jest.mock('app/helpers', () => {
  const modules = jest.requireActual('app/helpers');
  return {
    ...modules,
    formatTimeDefault: () => '01/01/2021'
  };
});

describe('redux-store > workflow-setup > ActionCardCompromiseEventWorkflow > updateWorkflowInstance', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.updateActionCardCompromiseEventWorkflowInstance.pending(
        'updateWorkflowInstance',
        undefined
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.updateActionCardCompromiseEventWorkflowInstance.fulfilled(
        true,
        'updateWorkflowInstance',
        undefined
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.updateActionCardCompromiseEventWorkflowInstance.rejected(
        new Error('mock error'),
        'updateWorkflowInstance',
        undefined
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(true);
  });

  it('thunk action > with workflowInstanceId & changeId', async () => {
    const mockRes = { data: { workflowSetupData: [] } };
    const updateWorkflowDataFn = jest.fn().mockResolvedValue(mockRes);

    const nextState: any =
      await actionsWorkflowSetup.updateActionCardCompromiseEventWorkflowInstance(
        {
          workflowSetupData: []
        }
      )(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer
            }
          } as RootState),
        {
          workflowService: {
            updateWorkflowInstanceData: updateWorkflowDataFn
          }
        }
      );
    expect(nextState.payload.workflowSetupData).toBeUndefined();
    expect(updateWorkflowDataFn).toHaveBeenCalled();
  });
});
