import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';
import { ACCESelectActionsValue } from 'pages/WorkflowActionCardCompromiseEvent/ActionCardCompromiseEventSelectActionsStep';
import { ACCEConfigureParameterValue } from 'pages/WorkflowActionCardCompromiseEvent/ActionCardCompromiseEventStep';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { FormUploadFile } from 'pages/_commons/WorkflowSetup/CommonSteps/_components/UploadFile';
import { workflowSetupTemplateIdSelector } from '../../select-hooks';
import { getActionParameters, getConfigParameters } from './helper';

export const createActionCardCompromiseEventWorkflowInstance = createAsyncThunk<
  any,
  WorkflowSetupThunkArg,
  ThunkAPIConfig
>(
  'workflowSetup/createActionCardCompromiseEventWorkflowInstance',
  async ({ workflowSetupData }, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

    const state = thunkAPI.getState();

    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;

    const workflowTemplateId = workflowSetupTemplateIdSelector(state);

    const changeId = changeForm.changeSetId;
    let workflowInstanceId = changeForm.workflowInstanceId?.toString();

    if (workflowInstanceId && changeId) {
      await workflowService.updateWorkflowInstance({
        workflowInstanceId,
        changeSetId: changeId.toString(),
        workflowInstanceDetails: {
          note: changeForm.workflowNote
        }
      } as any);
    } else {
      const workflowRes = await workflowService.createWorkflowInstance({
        workflowInstanceDetails: {
          note: changeForm.workflowNote
        },
        workflowTemplateId: workflowTemplateId || '',
        changeSetId: changeId?.toString()
      });

      workflowInstanceId =
        workflowRes?.data?.workflowInstanceDetails?.id?.toString();
    }

    const uploadedFile = state.workflowSetup?.workflowFormSteps.forms[
      'uploadFile'
    ] as FormUploadFile;

    const elementForm = state.workflowSetup?.workflowFormSteps.forms[
      'configureParameters'
    ] as ACCEConfigureParameterValue;

    const actionForm = state.workflowSetup?.workflowFormSteps.forms[
      'selectActions'
    ] as ACCESelectActionsValue;

    const actionParameters = getActionParameters(actionForm);
    const elementParameters = getConfigParameters(elementForm);

    const parameters = [...actionParameters, ...elementParameters];

    const updateRequest: UpdateflowInstanceData = {
      workflowInstanceId: workflowInstanceId?.toString(),
      status: 'INCOMPLETE',
      data: {
        workflowSetupData,
        configurations: {
          parameters,
          attachmentId: uploadedFile?.attachmentId?.toString()
        }
      }
    };
    await workflowService.updateWorkflowInstanceData(updateRequest);

    return {
      changeId,
      workflowInstanceId
    };
  }
);
export const createActionCardCompromiseEventWorkflowInstanceBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    createActionCardCompromiseEventWorkflowInstance.pending,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', true);
      set(draftState.workflowInstance, 'error', false);
    }
  );
  builder.addCase(
    createActionCardCompromiseEventWorkflowInstance.fulfilled,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', false);
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'changeSetId',
        action.payload?.changeId
      );
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'workflowInstanceId',
        action.payload?.workflowInstanceId
      );
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'isCompletedWorkflow',
        false
      );
      set(
        draftState.workflowFormSteps,
        'workflowSetupData',
        action.payload?.workflowSetupData
      );
    }
  );
  builder.addCase(
    createActionCardCompromiseEventWorkflowInstance.rejected,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', true);
    }
  );
};
