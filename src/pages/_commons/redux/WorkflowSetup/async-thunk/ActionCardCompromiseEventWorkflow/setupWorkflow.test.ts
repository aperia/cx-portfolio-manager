import { WorkflowMethodListData } from 'app/fixtures/workflow-method-list';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import { ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME } from 'pages/WorkflowActionCardCompromiseEvent/ActionCardCompromiseEventSelectActionsStep/helper';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  workflowInstanceStatus: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      changeInformation: {
        changeSetId: '1111',
        workflowInstanceId: '2222',
        name: 'mock name',
        description: 'mock description',
        workflowNote: 'mock note',
        effectiveDate: new Date()
      },
      ActionCardCompromiseEvent: {
        parameters: WorkflowMethodListData
      },
      configureParameters: {
        optionalParameters: {
          radio: ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SINGLE_ENTITY,
          question2:
            'Account Transfer using the AT, Account Transfers transaction.'
        },
        parameters: {
          radio: ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SINGLE_ENTITY,
          question2:
            'Account Transfer using the AT, Account Transfers transaction.'
        }
      },
      selectActions: {
        action: {
          radio: ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SEPARATE_ENTITY,
          question2:
            'PIID Transfer using the PIT, Presentation Instrument Transfer transaction.'
        }
      }
    }
  }
};

jest.mock('app/helpers', () => {
  const modules = jest.requireActual('app/helpers');
  return {
    ...modules,
    formatTimeDefault: () => '01/01/2021'
  };
});

describe('redux-store > workflow-setup > ActionCardCompromiseEventWorkflow > ActionCardCompromiseEvent > setupWorkflow', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.setupActionCardCompromiseEventWorkflow.pending(
        'ActionCardCompromiseEvent',
        {}
      )
    );
    expect(nextState.workflowSetup.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.setupActionCardCompromiseEventWorkflow.fulfilled(
        true,
        'ActionCardCompromiseEvent',
        {}
      )
    );
    expect(nextState.workflowSetup.loading).toEqual(false);
    expect(nextState.workflowSetup.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.setupActionCardCompromiseEventWorkflow.rejected(
        new Error('mock error'),
        'setupWorkflow',
        {}
      )
    );
    expect(nextState.workflowSetup.loading).toEqual(false);
    expect(nextState.workflowSetup.error).toEqual(true);
  });

  it('thunk action > with workflowInstanceId & changeId', async () => {
    const mockRes = { data: { workflowSetupData: [] } };
    const updateWorkflowDataFn = jest.fn().mockResolvedValue(mockRes);

    const nextState: any =
      await actionsWorkflowSetup.setupActionCardCompromiseEventWorkflow({
        workflowSetupData: []
      })(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer
            }
          } as RootState),
        {
          workflowService: {
            updateWorkflowInstanceData: updateWorkflowDataFn
          }
        }
      );
    expect(nextState.payload.workflowSetupData).toBeUndefined();
    expect(updateWorkflowDataFn).toHaveBeenCalled();
  });
});
