import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types/workflowSetupState';
import get from 'lodash.get';
import set from 'lodash.set';
import { ACCESelectActionsValue } from 'pages/WorkflowActionCardCompromiseEvent/ActionCardCompromiseEventSelectActionsStep';
import { ACCEConfigureParameterValue } from 'pages/WorkflowActionCardCompromiseEvent/ActionCardCompromiseEventStep';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { FormUploadFile } from 'pages/_commons/WorkflowSetup/CommonSteps/_components/UploadFile';
import { getActionParameters, getConfigParameters } from './helper';

export const setupActionCardCompromiseEventWorkflow = createAsyncThunk<
  any,
  WorkflowSetupThunkArg,
  ThunkAPIConfig
>(
  'workflowSetup/setupActionCardCompromiseEventWorkflow',
  async ({ workflowSetupData }, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

    const state = thunkAPI.getState();

    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;

    const uploadedFile = state.workflowSetup?.workflowFormSteps.forms[
      'uploadFile'
    ] as FormUploadFile;

    const elementForm = state.workflowSetup?.workflowFormSteps.forms[
      'configureParameters'
    ] as ACCEConfigureParameterValue;

    const actionForm = state.workflowSetup?.workflowFormSteps.forms[
      'selectActions'
    ] as ACCESelectActionsValue;

    const actionParameters = getActionParameters(actionForm);
    const elementParameters = getConfigParameters(elementForm);

    const parameters = [...actionParameters, ...elementParameters];

    const updateRequest: UpdateflowInstanceData = {
      workflowInstanceId: changeForm.workflowInstanceId?.toString(),
      status: 'COMPLETE',
      data: {
        workflowSetupData,
        configurations: {
          parameters,
          attachmentId: uploadedFile?.attachmentId?.toString()
        }
      }
    };

    await workflowService.updateWorkflowInstanceData(updateRequest);

    return true;
  }
);
export const setupActionCardCompromiseEventWorkflowBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    setupActionCardCompromiseEventWorkflow.pending,
    draftState => {
      set(draftState.workflowSetup, 'loading', true);
    }
  );
  builder.addCase(
    setupActionCardCompromiseEventWorkflow.fulfilled,
    (draftState, action) => {
      set(draftState.workflowSetup, 'loading', false);
      set(draftState.workflowSetup, 'error', false);
    }
  );
  builder.addCase(
    setupActionCardCompromiseEventWorkflow.rejected,
    draftState => {
      set(draftState.workflowSetup, 'loading', false);
      set(draftState.workflowSetup, 'error', true);
    }
  );
};
