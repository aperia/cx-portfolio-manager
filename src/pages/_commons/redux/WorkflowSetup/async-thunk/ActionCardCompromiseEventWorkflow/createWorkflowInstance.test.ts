import { mockThunkAction } from 'app/utils';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import { ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME } from 'pages/WorkflowActionCardCompromiseEvent/ActionCardCompromiseEventSelectActionsStep/helper';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';
import * as UpdateActionCardCompromiseEventWorkflowInstance from './updateWorkflowInstance';

const initReducer: any = {
  workflowInstance: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      changeInformation: {
        name: 'mock name',
        description: 'mock description',
        effectiveDate: new Date()
      },
      configureParameters: {
        optionalParameters: {
          radio: ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SINGLE_ENTITY,
          question2:
            'Account Transfer using the AT, Account Transfers transaction.'
        },
        parameters: {
          radio: ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SINGLE_ENTITY,
          question2:
            'Account Transfer using the AT, Account Transfers transaction.'
        }
      },
      selectActions: {
        action: {
          radio: ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SINGLE_ENTITY,
          question2:
            'Account Transfer using the AT, Account Transfers transaction.'
        }
      }
    }
  }
};

jest.mock('app/helpers', () => {
  const modules = jest.requireActual('app/helpers');
  return {
    ...modules,
    formatTimeDefault: () => '01/01/2021'
  };
});

const mockUpdateActionCardCompromiseEventWorkflowInstance = mockThunkAction(
  UpdateActionCardCompromiseEventWorkflowInstance
);

describe('redux-store > workflow-setup > ActionCardCompromiseEventWorkflow > createWorkflowInstance', () => {
  beforeEach(() => {
    mockUpdateActionCardCompromiseEventWorkflowInstance(
      'updateActionCardCompromiseEventWorkflowInstance'
    );
  });
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.createActionCardCompromiseEventWorkflowInstance.pending(
        'createWorkflowInstance',
        {}
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.createActionCardCompromiseEventWorkflowInstance.fulfilled(
        {},
        'createWorkflowInstance',
        {}
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.createActionCardCompromiseEventWorkflowInstance.rejected(
        new Error('mock error'),
        'createWorkflowInstance',
        {}
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(true);
  });

  it('thunk action > without workflowInstanceId & changeId', async () => {
    const nextState: any =
      await actionsWorkflowSetup.createActionCardCompromiseEventWorkflowInstance(
        {
          workflowSetupData: []
        }
      )(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer
            }
          } as RootState),
        {
          workflowService: {
            createWorkflowInstance: jest.fn().mockResolvedValue({
              data: { workflowInstanceDetails: { id: '1111' } }
            }),
            updateWorkflowInstanceData: jest.fn()
          },
          changeService: {
            createChangeSet: jest.fn().mockResolvedValue({
              data: { id: undefined }
            })
          }
        }
      );

    expect(nextState.payload).toEqual({
      changeId: undefined,
      workflowInstanceId: '1111'
    });
  });

  it('thunk action > with workflowInstanceId & changeId', async () => {
    const state = {
      workflowSetup: {
        workflow: {
          id: undefined
        }
      },
      workflowFormSteps: {
        forms: {
          changeInformation: {
            changeSetId: '2222',
            workflowInstanceId: '1111',
            name: 'mock name',
            description: 'mock description',
            effectiveDate: new Date()
          },
          gettingStart: {
            returnedCheckCharges: true,
            lateCharges: true
          }
        }
      }
    };
    const nextState: any =
      await actionsWorkflowSetup.createActionCardCompromiseEventWorkflowInstance(
        {
          workflowSetupData: []
        }
      )(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...state
            }
          } as RootState),
        {
          workflowService: {
            updateWorkflowInstance: jest.fn(),
            updateWorkflowInstanceData: jest.fn()
          }
        }
      );
    expect(nextState.payload).toBeUndefined();
  });

  it('thunk action > with existing changeId ', async () => {
    const state = {
      workflowSetup: {
        workflow: {
          id: undefined
        }
      },
      workflowFormSteps: {
        forms: {
          changeInformation: {
            changeSetId: '2222',
            name: 'mock name',
            description: 'mock description',
            effectiveDate: new Date()
          }
        }
      }
    };
    const nextState: any =
      await actionsWorkflowSetup.createActionCardCompromiseEventWorkflowInstance(
        {
          workflowSetupData: []
        }
      )(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...state
            }
          } as RootState),
        {
          workflowService: {
            createWorkflowInstance: jest.fn().mockResolvedValue({
              data: { workflowInstanceDetails: { id: '1111' } }
            }),
            updateWorkflowInstanceData: jest
              .fn()
              .mockResolvedValue({ data: { workflowSetupData: [] } })
          }
        }
      );
    expect(nextState.payload).toBeUndefined();
  });
});
