import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';
import { ACCESelectActionsValue } from 'pages/WorkflowActionCardCompromiseEvent/ActionCardCompromiseEventSelectActionsStep';
import { ACCEConfigureParameterValue } from 'pages/WorkflowActionCardCompromiseEvent/ActionCardCompromiseEventStep';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { FormUploadFile } from 'pages/_commons/WorkflowSetup/CommonSteps/_components/UploadFile';
import { getActionParameters, getConfigParameters } from './helper';

export const updateActionCardCompromiseEventWorkflowInstance = createAsyncThunk<
  any,
  WorkflowSetupThunkArg,
  ThunkAPIConfig
>(
  'workflowSetup/updateActionCardCompromiseEventWorkflowInstance',
  async ({ workflowSetupData }, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

    const state = thunkAPI.getState();

    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;

    const uploadedFile = state.workflowSetup?.workflowFormSteps.forms[
      'uploadFile'
    ] as FormUploadFile;

    const elementForm = state.workflowSetup?.workflowFormSteps.forms[
      'configureParameters'
    ] as ACCEConfigureParameterValue;

    const actionForm = state.workflowSetup?.workflowFormSteps.forms[
      'selectActions'
    ] as ACCESelectActionsValue;

    const actionParameters = getActionParameters(actionForm);
    const elementParameters = getConfigParameters(elementForm);

    const parameters = [...actionParameters, ...elementParameters];

    const updateRequest: UpdateflowInstanceData = {
      workflowInstanceId: changeForm.workflowInstanceId?.toString(),
      status: 'INCOMPLETE',
      data: {
        workflowSetupData,
        configurations: {
          parameters,
          attachmentId: uploadedFile?.attachmentId?.toString()
        }
      }
    };

    await workflowService.updateWorkflowInstanceData(updateRequest);
    return true;
  }
);
export const updateActionCardCompromiseEventWorkflowInstanceBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    updateActionCardCompromiseEventWorkflowInstance.pending,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', true);
      set(draftState.workflowInstance, 'error', false);
    }
  );
  builder.addCase(
    updateActionCardCompromiseEventWorkflowInstance.fulfilled,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', false);
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'isCompletedWorkflow',
        false
      );
    }
  );
  builder.addCase(
    updateActionCardCompromiseEventWorkflowInstance.rejected,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', true);
    }
  );
};
