import { MethodFieldParameterEnum as ParameterEnum } from 'app/constants/enums';
import { ACCESelectActionsValue } from 'pages/WorkflowActionCardCompromiseEvent/ActionCardCompromiseEventSelectActionsStep';
import { ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME } from 'pages/WorkflowActionCardCompromiseEvent/ActionCardCompromiseEventSelectActionsStep/helper';
import { ACCEConfigureParameterValue } from 'pages/WorkflowActionCardCompromiseEvent/ActionCardCompromiseEventStep';

export const getActionParameters = ({
  action
}: ACCESelectActionsValue = {}) => {
  const isSingleEntity =
    action?.radio === ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SINGLE_ENTITY;

  const isYesNoQuestion =
    (action?.radio === ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SINGLE_ENTITY &&
      action?.question2 ===
        'Account Transfer using the AT, Account Transfers transaction.') ||
    (action?.radio ===
      ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SEPARATE_ENTITY &&
      action?.question2 ===
        'PIID Transfer using the PIT, Presentation Instrument Transfer transaction.');

  const paramters = [
    {
      name: ParameterEnum.ActionCardCompromiseEventQuestion1,
      value: action?.radio
    },
    {
      name: isSingleEntity
        ? ParameterEnum.ActionCardCompromiseEventQuestion2SingleEntity
        : ParameterEnum.ActionCardCompromiseEventQuestion2SeparateEntity,
      value: action?.question2
    }
  ];

  paramters.push({
    name: isYesNoQuestion
      ? ParameterEnum.ActionCardCompromiseEventQuestion4
      : ParameterEnum.ActionCardCompromiseEventQuestion3,
    value: action?.question3
  });

  return paramters;
};

// const dateParameters: string[] = [
//   ParameterEnum.ActionCardCompromiseEventLostDate,
//   ParameterEnum.ActionCardCompromiseEventStolenDate,
//   ParameterEnum.ActionCardCompromiseEventMasterCardPurgeDate,
//   ParameterEnum.ActionCardCompromiseEventDateOfEvent
// ];
const parseParameters = (parameters: Record<string, string>) =>
  Object.keys(parameters).map(k => ({
    name: k,
    value: parameters[k]
  }));
export const getConfigParameters = ({
  optionalParameters,
  parameters
}: ACCEConfigureParameterValue = {}) => {
  return [
    ...parseParameters(parameters),
    ...parseParameters(optionalParameters)
  ];
};
