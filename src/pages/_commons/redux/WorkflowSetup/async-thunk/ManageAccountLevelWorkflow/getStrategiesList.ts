import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';

interface Params {}

export const getStrategiesList = createAsyncThunk<any, Params, ThunkAPIConfig>(
  'workflowSetup/getStrategiesList',
  async (elementNames: Params, thunkAPI) => {
    const { changeService } = get(thunkAPI, 'extra') as APIMapping;

    const res = await changeService.getStrategiesListAQ();
    const data = res?.data!.pricingStrategies.map(
      (item: MagicKeyValue) => item?.strategyName
    );
    return {
      strategiesListAQ: data
    };
  }
);
export const getStrategiesListBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(getStrategiesList.pending, (draftState, action) => {
    set(draftState.strategiesListAQ, 'loading', true);
    set(draftState.strategiesListAQ, 'error', false);
  });
  builder.addCase(getStrategiesList.fulfilled, (draftState, action) => {
    set(draftState.strategiesListAQ, 'loading', false);
    set(draftState.strategiesListAQ, 'error', false);
    set(
      draftState.strategiesListAQ,
      'strategiesListAQData',
      action.payload?.strategiesListAQ
    );
  });
  builder.addCase(getStrategiesList.rejected, (draftState, action) => {
    set(draftState.strategiesListAQ, 'loading', false);
    set(draftState.strategiesListAQ, 'error', true);
  });
};
