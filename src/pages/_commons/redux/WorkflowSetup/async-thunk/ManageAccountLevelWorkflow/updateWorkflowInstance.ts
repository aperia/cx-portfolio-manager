import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import isEmpty from 'lodash.isempty';
import set from 'lodash.set';
import { ConfigureParametersFormValue } from 'pages/WorkflowManageAccountLevel/ConfigureParameters';
import { FormGettingStart } from 'pages/WorkflowManageAccountLevel/GettingStartStep';
import { ManageAccountLevelCAFormValues } from 'pages/WorkflowManageAccountLevel/ManageAccountLevelCAStep';
import { TableType } from 'pages/WorkflowManageTransactionLevelProcessingDecisionTablesTLP/ConfigureParameters/constant';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { parseTableData } from '../ManageTransactionLevelProcessingDecisionTablesTLPWorkflow/helper';

export const updateManageAccountLevelWorkflowInstance = createAsyncThunk<
  boolean | undefined,
  WorkflowSetupThunkArg,
  ThunkAPIConfig
>(
  'workflowSetup/updateManageAccountLevelWorkflowInstance',
  async ({ workflowSetupData }, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;
    const state = thunkAPI.getState();

    const gettingStart = state.workflowSetup?.workflowFormSteps.forms[
      'gettingStart'
    ] as FormGettingStart;

    // invalid save
    if (!gettingStart?.alpAQ && !gettingStart.alpCA) return;

    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;

    const uploadFileFormCA = state.workflowSetup?.workflowFormSteps.forms[
      'uploadFileCA'
    ] as any;

    const uploadFileFormAQ = state.workflowSetup?.workflowFormSteps.forms[
      'uploadFileAQ'
    ] as any;

    const aqForm = state.workflowSetup?.workflowFormSteps.forms[
      'configAlpAq'
    ] as ConfigureParametersFormValue;

    const caForm = state.workflowSetup?.workflowFormSteps.forms[
      'configAlpCa'
    ] as ManageAccountLevelCAFormValues;

    const tableCAForm = !isEmpty(caForm?.tables?.tableName)
      ? [
          {
            id: caForm?.tables?.id,
            tableId: caForm?.tables?.tableId,
            tableName: caForm?.tables?.tableName,
            tableType: 'CA',
            comment: caForm?.tables?.comment,
            modeledFrom: null,
            tableControlParameters: caForm?.tables?.tableControlParameters,
            decisionElements: caForm?.tables?.elementList
          }
        ]
      : [];

    const tablesAQForm = parseTableData(aqForm?.tables, TableType.tlpaq);

    const tableList = [...tablesAQForm, ...tableCAForm];

    const updateRequest = {
      workflowInstanceId: changeForm?.workflowInstanceId?.toString(),
      status: 'INCOMPLETE',
      data: {
        workflowSetupData,
        configurations: {
          attachments: [
            {
              'table.type': 'CA',
              attachmentId: uploadFileFormCA?.attachmentId
            },
            {
              'table.type': 'AQ',
              attachmentId: uploadFileFormAQ?.attachmentId
            }
          ],
          tableList
        }
      }
    };

    await workflowService.updateWorkflowInstanceData(updateRequest);

    return true;
  }
);
export const updateManageAccountLevelWorkflowInstanceBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    updateManageAccountLevelWorkflowInstance.pending,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', true);
      set(draftState.workflowInstance, 'error', false);
    }
  );
  builder.addCase(
    updateManageAccountLevelWorkflowInstance.fulfilled,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', false);
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'isCompletedWorkflow',
        false
      );
    }
  );
  builder.addCase(
    updateManageAccountLevelWorkflowInstance.rejected,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', true);
    }
  );
};
