import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';
import { getStrategiesList } from './getStrategiesList';

const initReducer: any = {
  strategiesListAQ: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  }
};

describe('redux-store > workflow-setup > ManageAccountLevelWorkflow > getStrategiesList', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getStrategiesList.pending('getStrategiesList', {})
    );
    expect(nextState.strategiesListAQ.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getStrategiesList.fulfilled(
        true,
        'getStrategiesList',
        {}
      )
    );
    expect(nextState.strategiesListAQ.loading).toEqual(false);
    expect(nextState.strategiesListAQ.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getStrategiesList.rejected(
        new Error('mock error'),
        'getStrategiesList',
        {}
      )
    );
    expect(nextState.strategiesListAQ.loading).toEqual(false);
    expect(nextState.strategiesListAQ.error).toEqual(true);
  });

  it('thunk action', async () => {
    const nextState: any = await getStrategiesList({})(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          workflowSetup: {
            ...initReducer
          }
        } as RootState),
      {
        changeService: {
          getStrategiesListAQ: jest.fn().mockResolvedValue({
            data: {
              pricingStrategies: [{ strategyName: '' }]
            }
          })
        }
      }
    );
    expect(nextState?.payload?.workflowSetupData).toBeUndefined();
  });
});
