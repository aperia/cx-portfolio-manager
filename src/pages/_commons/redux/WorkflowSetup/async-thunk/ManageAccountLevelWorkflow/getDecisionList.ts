import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';

interface Params {
  tableType: string;
}

export const getDecisionList = createAsyncThunk<any, Params, ThunkAPIConfig>(
  'workflowSetup/getDecisionList',
  async (elementNames: Params, thunkAPI) => {
    const { changeService } = get(thunkAPI, 'extra') as APIMapping;

    const resp = await changeService.getDecisionList(elementNames.tableType);
    return {
      decisionList: resp?.data?.decisionElements
    };
  }
);
export const getDecisionListBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(getDecisionList.pending, (draftState, action) => {
    set(draftState.decisionList, 'loading', true);
    set(draftState.decisionList, 'error', false);
  });
  builder.addCase(getDecisionList.fulfilled, (draftState, action) => {
    set(draftState.decisionList, 'loading', false);
    set(draftState.decisionList, 'error', false);
    set(draftState.decisionList, 'decisionList', action.payload?.decisionList);
  });
  builder.addCase(getDecisionList.rejected, (draftState, action) => {
    set(draftState.decisionList, 'loading', false);
    set(draftState.decisionList, 'error', true);
  });
};
