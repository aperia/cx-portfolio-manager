import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types/workflowSetupState';
import get from 'lodash.get';
import isEmpty from 'lodash.isempty';
import set from 'lodash.set';
import { ConfigureParametersFormValue } from 'pages/WorkflowManageAccountLevel/ConfigureParameters';
import { ManageAccountLevelCAFormValues } from 'pages/WorkflowManageAccountLevel/ManageAccountLevelCAStep';
import { TableType } from 'pages/WorkflowManageTransactionLevelProcessingDecisionTablesTLP/ConfigureParameters/constant';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { parseTableData } from '../ManageTransactionLevelProcessingDecisionTablesTLPWorkflow/helper';

export const setupManageAccountLevelWorkflow = createAsyncThunk<
  boolean,
  WorkflowSetupThunkArg,
  ThunkAPIConfig
>(
  'workflowSetup/setupManageAccountLevelWorkflow',
  async ({ workflowSetupData }, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

    const state = thunkAPI.getState();
    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;

    const uploadFileFormCA = state.workflowSetup?.workflowFormSteps.forms[
      'uploadFileCA'
    ] as any;

    const uploadFileFormAQ = state.workflowSetup?.workflowFormSteps.forms[
      'uploadFileAQ'
    ] as any;

    const aqForm = state.workflowSetup?.workflowFormSteps.forms[
      'configAlpAq'
    ] as ConfigureParametersFormValue;

    const caForm = state.workflowSetup?.workflowFormSteps.forms[
      'configAlpCa'
    ] as ManageAccountLevelCAFormValues;

    const tableCAForm = !isEmpty(caForm?.tables?.tableName)
      ? [
          {
            id: caForm?.tables?.id,
            tableId: caForm?.tables?.tableId,
            tableName: caForm?.tables?.tableName,
            tableType: 'CA',
            comment: caForm?.tables?.comment,
            modeledFrom: null,
            tableControlParameters: caForm?.tables?.tableControlParameters,
            decisionElements: caForm?.tables?.elementList
          }
        ]
      : [];

    const tablesAQForm = parseTableData(aqForm?.tables, TableType.tlpaq);

    const tableList = [...tablesAQForm, ...tableCAForm];

    const workflowInstanceId = changeForm?.workflowInstanceId?.toString();
    const updateRequest = {
      workflowInstanceId,
      status: 'COMPLETE',
      data: {
        workflowSetupData,
        configurations: {
          attachments: [
            {
              'table.type': 'CA',
              attachmentId: uploadFileFormCA?.attachmentId
            },
            {
              'table.type': 'AQ',
              attachmentId: uploadFileFormAQ?.attachmentId
            }
          ],
          tableList
        }
      }
    };

    await workflowService.updateWorkflowInstanceData(updateRequest);

    return true;
  }
);
export const setupManageAccountLevelWorkflowBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(setupManageAccountLevelWorkflow.pending, draftState => {
    set(draftState.workflowSetup, 'loading', true);
  });
  builder.addCase(setupManageAccountLevelWorkflow.fulfilled, draftState => {
    set(draftState.workflowSetup, 'loading', false);
    set(draftState.workflowSetup, 'error', false);
  });
  builder.addCase(setupManageAccountLevelWorkflow.rejected, draftState => {
    set(draftState.workflowSetup, 'loading', false);
    set(draftState.workflowSetup, 'error', true);
  });
};
