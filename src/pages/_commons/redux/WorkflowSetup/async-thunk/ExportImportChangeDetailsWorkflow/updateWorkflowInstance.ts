import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';

export const updateExportImportChangeDetailsWorkflowInstance = createAsyncThunk<
  any,
  WorkflowSetupThunkArg,
  ThunkAPIConfig
>(
  'workflowSetup/updateExportImportChangeDetailsWorkflowInstance',
  async ({ workflowSetupData }, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;
    const state = thunkAPI.getState();

    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;

    const exportChange = state.workflowSetup?.workflowFormSteps.forms[
      'exportChangeDetails'
    ] as any;

    const importChange = state.workflowSetup?.workflowFormSteps.forms[
      'importChangeDetails'
    ] as any;

    const configurations = {} as any;
    exportChange && (configurations.changeLists = exportChange.changeLists);
    exportChange &&
      (configurations.changeListsData = exportChange.changeListsData);
    importChange && (configurations.attachmentId = importChange.attachmentId);

    const updateRequest = {
      workflowInstanceId: changeForm.workflowInstanceId?.toString(),
      status: 'INCOMPLETE',
      data: {
        workflowSetupData,
        configurations
      }
    };

    await workflowService.updateWorkflowInstanceData(updateRequest);

    return { workflowSetupData };
  }
);
export const updateExportImportChangeDetailsWorkflowInstanceBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    updateExportImportChangeDetailsWorkflowInstance.pending,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', true);
      set(draftState.workflowInstance, 'error', false);
    }
  );
  builder.addCase(
    updateExportImportChangeDetailsWorkflowInstance.fulfilled,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', false);
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'isCompletedWorkflow',
        false
      );
      set(
        draftState.workflowFormSteps,
        'workflowSetupData',
        action.payload?.workflowSetupData
      );
    }
  );
  builder.addCase(
    updateExportImportChangeDetailsWorkflowInstance.rejected,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', true);
    }
  );
};
