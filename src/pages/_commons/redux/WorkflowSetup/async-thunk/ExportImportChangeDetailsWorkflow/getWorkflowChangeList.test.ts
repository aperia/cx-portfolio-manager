import store from 'app/_libraries/_dof/core/redux/createAppStore';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  workflowInstanceStatus: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      changeInformation: {
        changeSetId: '1111',
        workflowInstanceId: '2222',
        name: 'mock name',
        description: 'mock description',
        workflowNote: 'mock note',
        effectiveDate: new Date()
      }
    }
  }
};

describe('redux-store > workflow-setup >  ExportImportChangeDetailsWorkflow > getWorkflowChangeList', () => {
  it('thunk action > with getWorkflowChangeList', async () => {
    const nextState: any = await actionsWorkflowSetup.getWorkflowChangeList({
      type: ['MONETARY', 'NON_MONETARY']
    })(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: {
            data: {
              data: {}
            }
          },
          workflowSetup: {
            ...initReducer
          }
        } as RootState),
      {
        workflowService: {
          getWorkflowChangeList: jest.fn().mockResolvedValue({
            data: {
              changeSetList: [
                {
                  changeType: 'PRICING',
                  changeStatus: 'PRODUCTION',
                  name: 'abc',
                  id: '1',
                  changeOwner: {
                    id: '2',
                    name: 'bc'
                  },
                  createdDate: '22/02/2022'
                }
              ]
            }
          })
        }
      }
    );
    expect(nextState.payload).toEqual([
      {
        name: 'abc',
        id: '1',
        changeId: '2',
        changeName: 'bc',
        createdDate: '22/02/2022'
      }
    ]);
  });
});
