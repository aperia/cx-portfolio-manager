import { createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import get from 'lodash.get';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';

export interface getWorkflowChangeArgs {
  type?: string[];
}

export const getWorkflowChangeList = createAsyncThunk<
  any,
  getWorkflowChangeArgs,
  ThunkAPIConfig
>('workflowSetup/getWorkflowChangeList', async (args, thunkAPI) => {
  const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

  const state = thunkAPI.getState();

  const changeForm = state.workflowSetup?.workflowFormSteps.forms[
    'changeInformation'
  ] as FormChangeInfo;

  const response = await workflowService.getWorkflowChangeList({
    searchFields: {
      workflowInstanceId: changeForm.workflowInstanceId?.toString()
    }
  });
  const data = response?.data?.changeSetList.filter(
    (x: any) =>
      x.changeType.toUpperCase() === 'PRICING' &&
      x.changeStatus.toUpperCase() === 'PRODUCTION'
  );

  const changeSetList = data?.map((e: any) => {
    return {
      name: e.name,
      id: e.id,
      changeId: e.changeOwner.id,
      changeName: e.changeOwner.name,
      createdDate: e.createdDate
    } as {
      name: string;
      id: number;
      changeId: string;
      changeName: string;
      createdDate: string;
    };
  });

  return changeSetList;
});
