import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types/workflowSetupState';
import get from 'lodash.get';
import set from 'lodash.set';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';

export const setupExportImportChangeDetailsWorkflowInstance = createAsyncThunk<
  any,
  WorkflowSetupThunkArg,
  ThunkAPIConfig
>(
  'workflowSetup/setupExportImportChangeDetailsWorkflowInstance',
  async ({ workflowSetupData }, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

    const state = thunkAPI.getState();

    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;

    const exportChange = state.workflowSetup?.workflowFormSteps.forms[
      'exportChangeDetails'
    ] as any;

    const importChange = state.workflowSetup?.workflowFormSteps.forms[
      'importChangeDetails'
    ] as any;

    const configurations = {} as any;
    exportChange && (configurations.changeLists = exportChange.changeLists);
    exportChange &&
      (configurations.changeListsData = exportChange.changeListsData);
    importChange && (configurations.attachmentId = importChange.attachmentId);

    const updateRequest: UpdateflowInstanceData = {
      workflowInstanceId: changeForm.workflowInstanceId?.toString(),
      status: 'COMPLETE',
      data: {
        workflowSetupData,
        configurations
      }
    };

    await workflowService.updateWorkflowInstanceData(updateRequest);

    return { workflowSetupData };
  }
);
export const setupExportImportChangeDetailsWorkflowBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    setupExportImportChangeDetailsWorkflowInstance.pending,
    draftState => {
      set(draftState.workflowSetup, 'loading', true);
    }
  );
  builder.addCase(
    setupExportImportChangeDetailsWorkflowInstance.fulfilled,
    (draftState, action) => {
      set(draftState.workflowSetup, 'loading', false);
      set(draftState.workflowSetup, 'error', false);
      set(
        draftState.workflowFormSteps,
        'workflowSetupData',
        action.payload?.workflowSetupData
      );
    }
  );
  builder.addCase(
    setupExportImportChangeDetailsWorkflowInstance.rejected,
    draftState => {
      set(draftState.workflowSetup, 'loading', false);
      set(draftState.workflowSetup, 'error', true);
    }
  );
};
