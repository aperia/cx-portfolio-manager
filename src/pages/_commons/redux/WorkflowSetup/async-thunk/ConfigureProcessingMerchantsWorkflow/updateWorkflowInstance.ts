import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';
import { FormConfigParameters } from 'pages/WorkflowConfigureProcessingMerchants/ConfigParametersStep';
import { parseMerchantsPropToParameter } from 'pages/WorkflowConfigureProcessingMerchants/ConfigParametersStep/helper';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';

export const updateConfigureProcessingMerchantsWorkflowInstance =
  createAsyncThunk<any, WorkflowSetupThunkArg, ThunkAPIConfig>(
    'workflowSetup/updateConfigureProcessingMerchantsWorkflowInstance',
    async (args, thunkAPI) => {
      const { workflowSetupData } = args;
      const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

      const state = thunkAPI.getState();

      const changeForm = state.workflowSetup?.workflowFormSteps.forms[
        'changeInformation'
      ] as FormChangeInfo;

      const configParameters = state.workflowSetup?.workflowFormSteps.forms[
        'configParameters'
      ] as FormConfigParameters;
      const merchants = parseMerchantsPropToParameter(
        configParameters.merchants
      );

      const updateRequest: UpdateflowInstanceData = {
        workflowInstanceId: changeForm.workflowInstanceId?.toString(),
        status: 'INCOMPLETE',
        data: {
          workflowSetupData,
          configurations: { merchants }
        }
      };

      await workflowService.updateWorkflowInstanceData(updateRequest);
      return true;
    }
  );
export const updateConfigureProcessingMerchantsWorkflowInstanceBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    updateConfigureProcessingMerchantsWorkflowInstance.pending,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', true);
      set(draftState.workflowInstance, 'error', false);
    }
  );
  builder.addCase(
    updateConfigureProcessingMerchantsWorkflowInstance.fulfilled,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', false);
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'isCompletedWorkflow',
        false
      );
    }
  );
  builder.addCase(
    updateConfigureProcessingMerchantsWorkflowInstance.rejected,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', true);
    }
  );
};
