import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';
import { defaultWorkflowMethodsFilter } from '../../reducers';

const initReducer: any = {
  workflowMethods: {
    methods: [],
    loading: false,
    error: false,
    expandingMethod: '',
    dataFilter: defaultWorkflowMethodsFilter
  }
};

describe('redux-store > workflow-setup > ConfigureProcessingMerchantsWorkflow > getMerchantList', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getConfigureProcessingMerchantsList.pending(
        'getConfigureProcessingMerchantsList',
        ''
      )
    );
    expect(nextState.merchantData.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getConfigureProcessingMerchantsList.fulfilled(
        {},
        'getConfigureProcessingMerchantsList',
        ''
      )
    );
    expect(nextState.merchantData.loading).toEqual(false);
    expect(nextState.merchantData.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getConfigureProcessingMerchantsList.rejected(
        {} as any,
        'getConfigureProcessingMerchantsList',
        ''
      )
    );
    expect(nextState.merchantData.loading).toEqual(false);
    expect(nextState.merchantData.error).toEqual(true);
  });

  it('thunk action > with $all prin', async () => {
    const nextState: any =
      await actionsWorkflowSetup.getConfigureProcessingMerchantsList('0001')(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer
            },
            mapping: { data: { workflowMerchant: { id: 'id' } } }
          } as RootState),
        {
          workflowService: {
            getWorkflowMerchantList: jest.fn().mockResolvedValue({
              data: { merchants: [{ id: 'id' }] }
            })
          }
        }
      );
    expect(nextState.payload.length).toEqual(1);
  });

  it('thunk action > with special prin', async () => {
    const nextState: any =
      await actionsWorkflowSetup.getConfigureProcessingMerchantsList(
        '0001 0002'
      )(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer
            },
            mapping: { data: { workflowMerchant: {} } }
          } as RootState),
        {
          workflowService: {
            getWorkflowMerchantList: jest.fn().mockResolvedValue({
              data: { merchants: [] }
            })
          }
        }
      );
    expect(nextState.payload.length).toEqual(0);
  });
});
