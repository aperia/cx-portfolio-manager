import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';
import { FormConfigParameters } from 'pages/WorkflowConfigureProcessingMerchants/ConfigParametersStep';
import { parseMerchantsPropToParameter } from 'pages/WorkflowConfigureProcessingMerchants/ConfigParametersStep/helper';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { workflowSetupTemplateIdSelector } from '../../select-hooks';

export const createConfigureProcessingMerchantsWorkflowInstance =
  createAsyncThunk<any, WorkflowSetupThunkArg, ThunkAPIConfig>(
    'workflowSetup/createConfigureProcessingMerchantsWorkflowInstance',
    async (args, thunkAPI) => {
      const { workflowSetupData } = args;
      const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

      const state = thunkAPI.getState();

      const changeForm = state.workflowSetup?.workflowFormSteps.forms[
        'changeInformation'
      ] as FormChangeInfo;

      const workflowTemplateId = workflowSetupTemplateIdSelector(state);

      const changeId = changeForm.changeSetId;
      let workflowInstanceId = changeForm.workflowInstanceId?.toString();

      if (workflowInstanceId && changeId) {
        await workflowService.updateWorkflowInstance({
          workflowInstanceId,
          changeSetId: changeId.toString(),
          workflowInstanceDetails: {
            note: changeForm.workflowNote
          }
        } as any);
      } else {
        const workflowRes = await workflowService.createWorkflowInstance({
          workflowInstanceDetails: {
            note: changeForm.workflowNote
          },
          workflowTemplateId: workflowTemplateId || '',
          changeSetId: changeId?.toString()
        });

        workflowInstanceId =
          workflowRes?.data?.workflowInstanceDetails?.id?.toString();
      }

      const configParameters = state.workflowSetup?.workflowFormSteps.forms[
        'configParameters'
      ] as FormConfigParameters;
      const merchants = parseMerchantsPropToParameter(
        configParameters.merchants
      );

      const updateRequest: UpdateflowInstanceData = {
        workflowInstanceId: workflowInstanceId?.toString(),
        status: 'INCOMPLETE',
        data: {
          workflowSetupData,
          configurations: { merchants }
        }
      };
      await workflowService.updateWorkflowInstanceData(updateRequest);

      return { changeId, workflowInstanceId };
    }
  );
export const createConfigureProcessingMerchantsWorkflowInstanceBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    createConfigureProcessingMerchantsWorkflowInstance.pending,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', true);
      set(draftState.workflowInstance, 'error', false);
    }
  );
  builder.addCase(
    createConfigureProcessingMerchantsWorkflowInstance.fulfilled,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', false);
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'changeSetId',
        action.payload?.changeId
      );
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'workflowInstanceId',
        action.payload?.workflowInstanceId
      );
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'isCompletedWorkflow',
        false
      );
    }
  );
  builder.addCase(
    createConfigureProcessingMerchantsWorkflowInstance.rejected,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', true);
    }
  );
};
