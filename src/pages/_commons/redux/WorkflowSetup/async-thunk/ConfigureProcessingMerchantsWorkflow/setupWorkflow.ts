import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types/workflowSetupState';
import get from 'lodash.get';
import set from 'lodash.set';
import { FormConfigParameters } from 'pages/WorkflowConfigureProcessingMerchants/ConfigParametersStep';
import { parseMerchantsPropToParameter } from 'pages/WorkflowConfigureProcessingMerchants/ConfigParametersStep/helper';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';

export const setupConfigureProcessingMerchantsWorkflow = createAsyncThunk<
  any,
  WorkflowSetupThunkArg,
  ThunkAPIConfig
>(
  'workflowSetup/setupConfigureProcessingMerchantsWorkflow',
  async (args, thunkAPI) => {
    const { workflowSetupData } = args;
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

    const state = thunkAPI.getState();

    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;

    const configParameters = state.workflowSetup?.workflowFormSteps.forms[
      'configParameters'
    ] as FormConfigParameters;
    const merchants = parseMerchantsPropToParameter(configParameters.merchants);

    const updateRequest: UpdateflowInstanceData = {
      workflowInstanceId: changeForm.workflowInstanceId?.toString(),
      status: 'COMPLETE',
      data: {
        workflowSetupData,
        configurations: { merchants }
      }
    };

    await workflowService.updateWorkflowInstanceData(updateRequest);

    return true;
  }
);
export const setupConfigureProcessingMerchantsWorkflowBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    setupConfigureProcessingMerchantsWorkflow.pending,
    draftState => {
      set(draftState.workflowSetup, 'loading', true);
    }
  );
  builder.addCase(
    setupConfigureProcessingMerchantsWorkflow.fulfilled,
    (draftState, action) => {
      set(draftState.workflowSetup, 'loading', false);
      set(draftState.workflowSetup, 'error', false);
    }
  );
  builder.addCase(
    setupConfigureProcessingMerchantsWorkflow.rejected,
    draftState => {
      set(draftState.workflowSetup, 'loading', false);
      set(draftState.workflowSetup, 'error', true);
    }
  );
};
