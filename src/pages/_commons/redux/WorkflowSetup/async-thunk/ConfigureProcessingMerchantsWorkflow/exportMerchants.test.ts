import * as Helpers from 'app/helpers/fileUtilities';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';
import { defaultWorkflowMethodsFilter } from '../../reducers';

jest.spyOn(Helpers, 'downloadBlobFile');

const initReducer: any = {
  workflowMethods: {
    methods: [],
    loading: false,
    error: false,
    expandingMethod: '',
    dataFilter: defaultWorkflowMethodsFilter
  },
  exportMerchant: {}
};

describe('redux-store > workflow-setup > ConfigureProcessingMerchantsWorkflow > exportMerchants', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.exportConfigureProcessingMerchants.pending(
        'exportConfigureProcessingMerchants',
        ''
      )
    );
    expect(nextState.exportMerchant.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.exportConfigureProcessingMerchants.fulfilled(
        {},
        'exportConfigureProcessingMerchants',
        ''
      )
    );
    expect(nextState.exportMerchant.loading).toEqual(false);
    expect(nextState.exportMerchant.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.exportConfigureProcessingMerchants.rejected(
        {} as any,
        'exportConfigureProcessingMerchants',
        ''
      )
    );
    expect(nextState.exportMerchant.loading).toEqual(false);
    expect(nextState.exportMerchant.error).toEqual(true);
  });

  it('thunk action > with $all prin', async () => {
    const nextState: any =
      await actionsWorkflowSetup.exportConfigureProcessingMerchants('0001')(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer
            },
            mapping: {
              data: { downloadTemplateFile: { filename: 'filename' } }
            }
          } as RootState),
        {
          workflowService: {
            exportMerchants: jest.fn().mockResolvedValue({
              data: { attachment: { filename: 'filename' } }
            })
          }
        }
      );
    expect(nextState.payload).toEqual(true);
  });

  it('thunk action > with special prin', async () => {
    const nextState: any =
      await actionsWorkflowSetup.exportConfigureProcessingMerchants(
        '0001 0002'
      )(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer
            },
            mapping: {
              data: { downloadTemplateFile: { filename: 'filename' } }
            }
          } as RootState),
        {
          workflowService: {
            exportMerchants: jest.fn().mockResolvedValue({
              data: { attachment: { filename: 'filename.xlsx' } }
            })
          }
        }
      );
    expect(nextState.payload).toEqual(true);
  });
});
