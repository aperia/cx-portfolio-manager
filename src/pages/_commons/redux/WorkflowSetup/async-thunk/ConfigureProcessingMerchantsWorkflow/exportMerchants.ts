import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { MIME_TYPE } from 'app/constants/mine-type';
import { mappingDataFromObj } from 'app/helpers';
import { base64toBlob, downloadBlobFile } from 'app/helpers/fileUtilities';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import { get } from 'lodash';
import set from 'lodash.set';

export const exportConfigureProcessingMerchants = createAsyncThunk<
  any,
  string,
  ThunkAPIConfig
>(
  'workflowSetup/exportConfigureProcessingMerchants',
  async (args, thunkAPI) => {
    const state = thunkAPI.getState();
    const downloadFileMapping = state.mapping?.data?.downloadTemplateFile;
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

    const sysPrin = args.includes(' ')
      ? args.replace(' ', '-')
      : `${args}-$All`;
    const response = await workflowService.exportMerchants(sysPrin);

    const attachmentData = response?.data?.attachment;
    const file = mappingDataFromObj(attachmentData, downloadFileMapping!);
    const {
      fileStream = '',
      mimeType: mimeTypeDownload = '',
      filename: filenameDownload = ''
    } = file;

    let fileName = filenameDownload;
    if (filenameDownload.split('.').length < 2) {
      const ext = MIME_TYPE[mimeTypeDownload]?.extensions[0] || '';
      fileName = `${filenameDownload}.${ext}`;
    }
    const blol = base64toBlob(fileStream, mimeTypeDownload);
    downloadBlobFile(blol, fileName);

    return true;
  }
);

export const exportConfigureProcessingMerchantsBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(exportConfigureProcessingMerchants.pending, draftState => {
    set(draftState.exportMerchant, 'loading', true);
  });
  builder.addCase(exportConfigureProcessingMerchants.fulfilled, draftState => {
    set(draftState.exportMerchant, 'loading', false);
    set(draftState.exportMerchant, 'error', false);
  });
  builder.addCase(exportConfigureProcessingMerchants.rejected, draftState => {
    set(draftState.exportMerchant, 'loading', false);
    set(draftState.exportMerchant, 'error', true);
  });
};
