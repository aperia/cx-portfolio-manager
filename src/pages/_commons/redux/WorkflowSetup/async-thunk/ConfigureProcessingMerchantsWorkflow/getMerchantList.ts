import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { mappingDataFromArray } from 'app/helpers';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';

export const getConfigureProcessingMerchantsList = createAsyncThunk<
  any,
  string,
  ThunkAPIConfig
>(
  'workflowSetup/getConfigureProcessingMerchantsList',
  async (args, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

    const sysPrin = args.includes(' ')
      ? args.replace(' ', '-')
      : `${args}-$All`;
    const resp = await workflowService.getWorkflowMerchantList(sysPrin);

    const state = thunkAPI.getState();
    // Take mapping model
    const workflowMerchantListMapping = state.mapping?.data?.workflowMerchant;
    const merchantList = resp?.data?.merchants;
    // Mapping
    const mappedData = mappingDataFromArray(
      merchantList,
      workflowMerchantListMapping!
    ) as MerchantItem[];

    return mappedData.map(
      i =>
        ({
          ...i,
          id: i.merchantAccountNumber,
          taxpayerId: i.taxpayerId?.replace(/-/g, ''),
          imprinterNumber: i.imprinterNumber || '000'
        } as MerchantItem)
    );
  }
);
export const getConfigureProcessingMerchantsListBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    getConfigureProcessingMerchantsList.pending,
    (draftState, action) => {
      set(draftState, ['merchantData', 'loading'], true);
      set(draftState, ['merchantData', 'error'], false);
    }
  );
  builder.addCase(
    getConfigureProcessingMerchantsList.fulfilled,
    (draftState, action) => {
      return {
        ...draftState,
        merchantData: {
          ...draftState.merchantData,
          merchants: action.payload,
          loading: false,
          error: false
        }
      };
    }
  );
  builder.addCase(
    getConfigureProcessingMerchantsList.rejected,
    (draftState, action) => {
      set(draftState, ['merchantData', 'loading'], false);
      set(draftState, ['merchantData', 'error'], true);
    }
  );
};
