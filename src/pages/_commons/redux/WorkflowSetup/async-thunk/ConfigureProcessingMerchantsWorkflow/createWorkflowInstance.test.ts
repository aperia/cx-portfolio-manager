import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  workflowInstance: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      changeInformation: {
        name: 'mock name',
        description: 'mock description',
        effectiveDate: new Date()
      },
      configParameters: {
        merchants: [
          {
            sysPrin: 'sys1',
            id: 'id',
            rowId: 'row1',
            modeledFrom: {
              merchantName: 'name1',
              phoneNumber: 'phone1'
            },
            merchantName: 'name0',
            phoneNumber: 'phone0',
            parameters: [
              {
                name: 'name',
                previousValue: '1',
                newValue: '2'
              }
            ]
          }
        ]
      }
    }
  }
};

describe('redux-store > workflow-setup > ConfigureProcessingMerchantsWorkflow > createWorkflowInstance', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.createConfigureProcessingMerchantsWorkflowInstance.pending(
        'createWorkflowInstance',
        {}
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.createConfigureProcessingMerchantsWorkflowInstance.fulfilled(
        {},
        'createWorkflowInstance',
        {}
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.createConfigureProcessingMerchantsWorkflowInstance.rejected(
        new Error('mock error'),
        'createWorkflowInstance',
        {}
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(true);
  });

  it('thunk action > without workflowInstanceId & changeId', async () => {
    const nextState: any =
      await actionsWorkflowSetup.createConfigureProcessingMerchantsWorkflowInstance(
        {
          workflowSetupData: []
        }
      )(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer
            }
          } as RootState),
        {
          workflowService: {
            createWorkflowInstance: jest.fn().mockResolvedValue({
              data: { workflowInstanceDetails: { id: '1111' } }
            }),
            updateWorkflowInstanceData: jest.fn()
          },
          changeService: {
            createChangeSet: jest.fn().mockResolvedValue({
              data: { id: undefined }
            })
          }
        }
      );

    expect(nextState.payload).toEqual({
      changeId: undefined,
      workflowInstanceId: '1111'
    });
  });

  it('thunk action > with workflowInstanceId & changeId', async () => {
    const state = {
      workflowSetup: {
        workflow: {
          id: undefined
        }
      },
      workflowFormSteps: {
        forms: {
          changeInformation: {
            changeSetId: '2222',
            workflowInstanceId: '1111',
            name: 'mock name',
            description: 'mock description',
            effectiveDate: new Date()
          },
          gettingStart: {
            returnedCheckCharges: true,
            lateCharges: true
          },
          configParameters: {
            merchants: [
              {
                sysPrin: 'sys1',
                id: 'id',
                rowId: 'row1',
                modeledFrom: {
                  merchantName: 'name1',
                  phoneNumber: 'phone1'
                },
                merchantName: 'name0',
                phoneNumber: 'phone0',
                parameters: [
                  {
                    name: 'name',
                    previousValue: '1',
                    newValue: '2'
                  }
                ]
              }
            ]
          }
        }
      }
    };
    const nextState: any =
      await actionsWorkflowSetup.createConfigureProcessingMerchantsWorkflowInstance(
        {
          workflowSetupData: []
        }
      )(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...state
            }
          } as RootState),
        {
          workflowService: {
            updateWorkflowInstance: jest.fn(),
            updateWorkflowInstanceData: jest.fn()
          }
        }
      );
    expect(nextState.payload).toEqual({
      changeId: '2222',
      workflowInstanceId: '1111'
    });
  });

  it('thunk action > with existing changeId ', async () => {
    const state = {
      workflowSetup: {
        workflow: {
          id: undefined
        }
      },
      workflowFormSteps: {
        forms: {
          changeInformation: {
            changeSetId: '2222',
            name: 'mock name',
            description: 'mock description',
            effectiveDate: new Date()
          },
          configParameters: {
            merchants: [
              {
                sysPrin: 'sys1',
                id: 'id',
                rowId: 'row1',
                modeledFrom: {
                  merchantName: 'name1',
                  phoneNumber: 'phone1'
                },
                merchantName: 'name0',
                phoneNumber: 'phone0',
                parameters: [
                  {
                    name: 'name',
                    previousValue: '1',
                    newValue: '2'
                  }
                ]
              }
            ]
          }
        }
      }
    };
    const nextState: any =
      await actionsWorkflowSetup.createConfigureProcessingMerchantsWorkflowInstance(
        {
          workflowSetupData: []
        }
      )(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...state
            }
          } as RootState),
        {
          workflowService: {
            createWorkflowInstance: jest.fn().mockResolvedValue({
              data: { workflowInstanceDetails: { id: '1111' } }
            }),
            updateWorkflowInstanceData: jest
              .fn()
              .mockResolvedValue({ data: { workflowSetupData: [] } })
          }
        }
      );
    expect(nextState.payload).toEqual({
      changeId: '2222',
      workflowInstanceId: '1111'
    });
  });
});
