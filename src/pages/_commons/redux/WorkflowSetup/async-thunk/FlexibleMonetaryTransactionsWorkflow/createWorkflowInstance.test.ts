import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  workflowInstance: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      changeInformation: {
        changeSetId: '1111',
        workflowInstanceId: '2222',
        name: 'mock name',
        description: 'mock description',
        workflowNote: 'mock note',
        effectiveDate: new Date()
      },
      selectMonetaryTransactions: {
        attachmentId: 'attachmentId',
        transactions: []
      },
      uploadFile: {
        attachmentId: 'attachmentId'
      }
    }
  }
};

jest.mock('app/helpers', () => {
  const modules = jest.requireActual('app/helpers');
  return {
    ...modules,
    formatTimeDefault: () => '01/01/2021'
  };
});

describe('redux-store > workflow-setup > FlexibleMonetaryTransactionsWorkflow > createWorkflowInstance', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.createFlexibleMonetaryTransactionsWorkflowInstance.pending(
        'createWorkflowInstance',
        { workflowSetupData: [] }
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.createFlexibleMonetaryTransactionsWorkflowInstance.fulfilled(
        {},
        'createWorkflowInstance',
        { workflowSetupData: [] }
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.createFlexibleMonetaryTransactionsWorkflowInstance.rejected(
        new Error('mock error'),
        'createWorkflowInstance',
        { workflowSetupData: [] }
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(true);
  });

  it('thunk action > without workflowInstanceId & changeId', async () => {
    const nextState: any =
      await actionsWorkflowSetup.createFlexibleMonetaryTransactionsWorkflowInstance(
        { workflowSetupData: [] }
      )(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer,
              workflowFormSteps: {
                forms: {
                  changeInformation: {
                    changeSetId: '1111',
                    name: 'mock name',
                    description: 'mock description',
                    workflowNote: 'mock note',
                    effectiveDate: new Date()
                  },
                  selectMonetaryTransactions: {
                    attachmentId: 'attachmentId',
                    transactions: []
                  },
                  uploadFile: {
                    attachmentId: 'attachmentId'
                  }
                }
              }
            }
          } as RootState),
        {
          workflowService: {
            createWorkflowInstance: jest.fn().mockResolvedValue({
              data: { workflowInstanceDetails: { id: '2222' } }
            }),
            updateWorkflowInstance: jest.fn().mockResolvedValue({}),
            updateWorkflowInstanceData: jest.fn().mockResolvedValue({})
          },
          changeService: {
            createChangeSet: jest.fn().mockResolvedValue({
              data: { id: '2222' }
            })
          }
        }
      );

    expect(nextState.payload).toEqual({
      changeId: '1111',
      workflowInstanceId: '2222'
    });
  });

  it('thunk action > with workflowInstanceId & changeId', async () => {
    const state = {
      workflowSetup: {
        workflow: {
          id: undefined
        }
      },
      workflowFormSteps: {
        forms: {
          changeInformation: {
            changeSetId: '1111',
            workflowInstanceId: '2222',
            name: 'mock name',
            description: 'mock description',
            workflowNote: 'mock note',
            effectiveDate: new Date()
          },
          selectMonetaryTransactions: {
            attachmentId: 'attachmentId',
            transactions: []
          },
          uploadFile: {
            attachmentId: 'attachmentId'
          }
        }
      }
    };
    const nextState: any =
      await actionsWorkflowSetup.createFlexibleMonetaryTransactionsWorkflowInstance(
        { workflowSetupData: [] }
      )(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...state
            }
          } as RootState),
        {
          workflowService: {
            updateWorkflowInstance: jest.fn(),
            updateWorkflowInstanceData: jest.fn()
          }
        }
      );
    expect(nextState.payload).toEqual({
      changeId: '1111',
      workflowInstanceId: '2222'
    });
  });
});
