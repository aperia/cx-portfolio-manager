import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { mappingArrayKey } from 'app/helpers/mappingArrayKey';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types/workflowSetupState';
import get from 'lodash.get';
import set from 'lodash.set';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';

export const setupFlexibleMonetaryTransactionsWorkflow = createAsyncThunk<
  boolean,
  WorkflowSetupThunkArg,
  ThunkAPIConfig
>(
  'workflowSetup/setupFlexibleMonetaryTransactionsWorkflow',
  async ({ workflowSetupData }, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

    const state = thunkAPI.getState();

    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;

    const uploadFileForm = state.workflowSetup?.workflowFormSteps.forms[
      'uploadFile'
    ] as any;

    const workflowInstanceId = changeForm.workflowInstanceId?.toString();

    const { transactions } = state.workflowSetup?.workflowFormSteps.forms[
      'selectMonetaryTransactions'
    ] as any;

    const convertTransactions = mappingArrayKey(transactions, 'id');

    const updateRequest = {
      workflowInstanceId,
      status: 'COMPLETE',
      data: {
        workflowSetupData,
        configurations: {
          attachmentId: uploadFileForm.attachmentId,
          transactions: convertTransactions
        }
      }
    };

    await workflowService.updateWorkflowInstanceData(updateRequest);

    return true;
  }
);
export const setupFlexibleMonetaryTransactionsWorkflowBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    setupFlexibleMonetaryTransactionsWorkflow.pending,
    draftState => {
      set(draftState.workflowSetup, 'loading', true);
    }
  );
  builder.addCase(
    setupFlexibleMonetaryTransactionsWorkflow.fulfilled,
    draftState => {
      set(draftState.workflowSetup, 'loading', false);
      set(draftState.workflowSetup, 'error', false);
    }
  );
  builder.addCase(
    setupFlexibleMonetaryTransactionsWorkflow.rejected,
    draftState => {
      set(draftState.workflowSetup, 'loading', false);
      set(draftState.workflowSetup, 'error', true);
    }
  );
};
