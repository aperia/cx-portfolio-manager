import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  workflowInstanceStatus: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      changeInformation: {
        changeSetId: '1111',
        workflowInstanceId: '2222',
        name: 'mock name',
        description: 'mock description',
        workflowNote: 'mock note',
        effectiveDate: new Date()
      },
      selectMonetaryTransactions: {
        attachmentId: 'attachmentId',
        transactions: []
      },
      uploadFile: {
        attachmentId: 'attachmentId'
      }
    }
  }
};

describe('redux-store > workflow-setup > FlexibleMonetaryTransactionsWorkflow > setupWorkflow', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.setupFlexibleMonetaryTransactionsWorkflow.pending(
        'setupWorkflow',
        { workflowSetupData: [] }
      )
    );
    expect(nextState.workflowSetup.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.setupFlexibleMonetaryTransactionsWorkflow.fulfilled(
        true,
        'setupWorkflow',
        { workflowSetupData: [] }
      )
    );
    expect(nextState.workflowSetup.loading).toEqual(false);
    expect(nextState.workflowSetup.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.setupFlexibleMonetaryTransactionsWorkflow.rejected(
        new Error('mock error'),
        'setupWorkflow',
        { workflowSetupData: [] }
      )
    );
    expect(nextState.workflowSetup.loading).toEqual(false);
    expect(nextState.workflowSetup.error).toEqual(true);
  });

  it('thunk action > with workflowInstanceId & changeId', async () => {
    const updateWorkflowDataFn = jest.fn().mockResolvedValue(true);

    const nextState: any =
      await actionsWorkflowSetup.setupFlexibleMonetaryTransactionsWorkflow({
        workflowSetupData: []
      })(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer
            }
          } as RootState),
        {
          workflowService: {
            updateWorkflowInstanceData: updateWorkflowDataFn
          }
        }
      );
    expect(nextState.payload).toEqual(true);

    expect(updateWorkflowDataFn).toHaveBeenCalled();
  });
});
