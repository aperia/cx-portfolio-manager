import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  workflowInstanceStatus: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      changeInformation: {
        changeSetId: '1111',
        workflowInstanceId: '2222',
        name: 'mock name',
        description: 'mock description',
        workflowNote: 'mock note',
        effectiveDate: new Date()
      },
      selectMonetaryAdjustmentTransactions: {
        transactions: []
      },
      uploadFile: {
        attachmentId: 'attachmentId'
      },
      configureParameters: {
        configureParameters: {
          changeSetId: 'changeSetId',
          name: 'mock name',
          description: 'mock description',
          effectiveDate: new Date()
        }
      }
    }
  }
};

describe('redux-store > workflow-setup > CycleAccountsTestEnvironmentWorkflowInstance > setupWorkflow', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.setupCycleAccountsTestEnvironmentWorkflow.pending(
        'setupWorkflow',
        { workflowSetupData: [] }
      )
    );
    expect(nextState.workflowSetup.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.setupCycleAccountsTestEnvironmentWorkflow.fulfilled(
        true,
        'setupWorkflow',
        { workflowSetupData: [] }
      )
    );
    expect(nextState.workflowSetup.loading).toEqual(false);
    expect(nextState.workflowSetup.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.setupCycleAccountsTestEnvironmentWorkflow.rejected(
        new Error('mock error'),
        'setupWorkflow',
        { workflowSetupData: [] }
      )
    );
    expect(nextState.workflowSetup.loading).toEqual(false);
    expect(nextState.workflowSetup.error).toEqual(true);
  });

  it('thunk action > with workflowInstanceId & changeId', async () => {
    const updateWorkflowDataFn = jest.fn();

    const nextState: any =
      await actionsWorkflowSetup.setupCycleAccountsTestEnvironmentWorkflow({
        workflowSetupData: []
      })(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer
            }
          } as RootState),
        {
          workflowService: {
            updateWorkflowInstanceData: updateWorkflowDataFn
          }
        }
      );
    expect(nextState.payload).toEqual(true);

    expect(updateWorkflowDataFn).toHaveBeenCalled();
  });
});
