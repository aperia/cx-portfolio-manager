import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  workflowInstance: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      changeInformation: {
        changeSetId: '1111',
        workflowInstanceId: '2222',
        name: 'mock name',
        description: 'mock description',
        workflowNote: 'mock note',
        effectiveDate: new Date()
      },
      selectMonetaryAdjustmentTransactions: {
        attachmentId: 'attachmentId',
        date: {
          minDate: '10/01/2021',
          maxDate: '10/31/2021'
        }
      },
      uploadFile: {
        attachmentId: 'attachmentId'
      },
      configureParameters: {
        configureParameters: {
          changeSetId: 'changeSetId',
          name: 'mock name',
          description: 'mock description',
          effectiveDate: new Date()
        }
      }
    }
  }
};

describe('redux-store > workflow-setup > CycleAccountsTestEnvironmentWorkflow > updateWorkflowInstance', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.updateCycleAccountsTestEnvironmentWorkflowInstance.pending(
        'updateWorkflowInstance',
        { workflowSetupData: [] }
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.updateCycleAccountsTestEnvironmentWorkflowInstance.fulfilled(
        true,
        'updateWorkflowInstance',
        { workflowSetupData: [] }
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.updateCycleAccountsTestEnvironmentWorkflowInstance.rejected(
        new Error('mock error'),
        'updateWorkflowInstance',
        { workflowSetupData: [] }
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(true);
  });

  it('thunk action > with workflowInstanceId & changeId', async () => {
    const updateWorkflowDataFn = jest.fn();

    const nextState: any =
      await actionsWorkflowSetup.updateCycleAccountsTestEnvironmentWorkflowInstance(
        { workflowSetupData: [] }
      )(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer
            }
          } as RootState),
        {
          workflowService: {
            updateWorkflowInstanceData: updateWorkflowDataFn
          }
        }
      );
    expect(nextState.payload).toEqual(true);

    expect(updateWorkflowDataFn).toHaveBeenCalled();
  });
});
