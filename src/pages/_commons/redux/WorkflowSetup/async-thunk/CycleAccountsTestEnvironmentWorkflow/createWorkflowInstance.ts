import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';
import { ConfigureParametersFormValues } from 'pages/WorkflowCycleAccountsTestEnvironment/CycleAccountsTestEnvironmentStep';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { workflowSetupTemplateIdSelector } from '../../select-hooks';

export const createCycleAccountsTestEnvironmentWorkflowInstance =
  createAsyncThunk<any, WorkflowSetupThunkArg, ThunkAPIConfig>(
    'workflowSetup/createCycleAccountsTestEnvironmentWorkflowInstance',
    async ({ workflowSetupData }, thunkAPI) => {
      const { workflowService } = get(thunkAPI, 'extra') as APIMapping;
      const state = thunkAPI.getState();

      const changeForm = state.workflowSetup?.workflowFormSteps.forms[
        'changeInformation'
      ] as FormChangeInfo;
      const uploadFileForm = state.workflowSetup?.workflowFormSteps.forms[
        'uploadFile'
      ] as any;

      const workflowTemplateId = workflowSetupTemplateIdSelector(state);
      const changeSetId = changeForm?.changeSetId?.toString();
      let workflowInstanceId = changeForm?.workflowInstanceId?.toString();

      if (workflowInstanceId && changeSetId) {
        await workflowService.updateWorkflowInstance({
          workflowInstanceId,
          changeSetId,
          workflowInstanceDetails: {
            note: changeForm.workflowNote
          }
        });
      } else {
        const workflowRes = await workflowService.createWorkflowInstance({
          workflowInstanceDetails: {
            note: changeForm?.workflowNote
          },
          workflowTemplateId: workflowTemplateId || '',
          changeSetId
        });

        workflowInstanceId =
          workflowRes?.data?.workflowInstanceDetails?.id?.toString();
      }

      const configureParametersForm = state.workflowSetup?.workflowFormSteps
        .forms['configureParameters'] as ConfigureParametersFormValues;

      const configureParameters: any =
        configureParametersForm?.configureParameters || {};
      const parameters = Object.keys(configureParameters).map(p => ({
        name: p,
        value: configureParameters[p]
      }));

      const updateRequest = {
        workflowInstanceId,
        status: 'INCOMPLETE',
        data: {
          workflowSetupData,
          configurations: {
            parameters,
            attachmentId: uploadFileForm.attachmentId
          }
        }
      };

      await workflowService.updateWorkflowInstanceData(updateRequest);

      return { changeSetId, workflowInstanceId };
    }
  );
export const createCycleAccountsTestEnvironmentWorkflowInstanceBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    createCycleAccountsTestEnvironmentWorkflowInstance.pending,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', true);
      set(draftState.workflowInstance, 'error', false);
    }
  );
  builder.addCase(
    createCycleAccountsTestEnvironmentWorkflowInstance.fulfilled,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', false);
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'changeSetId',
        action.payload?.changeSetId
      );
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'workflowInstanceId',
        action.payload?.workflowInstanceId
      );
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'isCompletedWorkflow',
        false
      );
    }
  );
  builder.addCase(
    createCycleAccountsTestEnvironmentWorkflowInstance.rejected,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', true);
    }
  );
};
