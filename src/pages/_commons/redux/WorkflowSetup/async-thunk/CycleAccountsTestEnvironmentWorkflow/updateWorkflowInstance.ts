import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';
import { ConfigureParametersFormValues } from 'pages/WorkflowCycleAccountsTestEnvironment/CycleAccountsTestEnvironmentStep';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';

export const updateCycleAccountsTestEnvironmentWorkflowInstance =
  createAsyncThunk<boolean, WorkflowSetupThunkArg, ThunkAPIConfig>(
    'workflowSetup/updateCycleAccountsTestEnvironmentWorkflowInstance',
    async ({ workflowSetupData }, thunkAPI) => {
      const { workflowService } = get(thunkAPI, 'extra') as APIMapping;
      const state = thunkAPI.getState();

      const changeForm = state.workflowSetup?.workflowFormSteps.forms[
        'changeInformation'
      ] as FormChangeInfo;
      const uploadFileForm = state.workflowSetup?.workflowFormSteps.forms[
        'uploadFile'
      ] as any;

      const configureParametersForm = state.workflowSetup?.workflowFormSteps
        .forms['configureParameters'] as ConfigureParametersFormValues;

      const configureParameters: any =
        configureParametersForm?.configureParameters || {};
      const parameters = Object.keys(configureParameters).map(p => ({
        name: p,
        value: configureParameters[p]
      }));

      const updateRequest = {
        workflowInstanceId: changeForm?.workflowInstanceId?.toString(),
        status: 'INCOMPLETE',
        data: {
          workflowSetupData,
          configurations: {
            parameters,
            attachmentId: uploadFileForm?.attachmentId
          }
        }
      };

      await workflowService.updateWorkflowInstanceData(updateRequest);

      return true;
    }
  );
export const updateCycleAccountsTestEnvironmentWorkflowInstanceBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    updateCycleAccountsTestEnvironmentWorkflowInstance.pending,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', true);
      set(draftState.workflowInstance, 'error', false);
    }
  );
  builder.addCase(
    updateCycleAccountsTestEnvironmentWorkflowInstance.fulfilled,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', false);
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'isCompletedWorkflow',
        false
      );
    }
  );
  builder.addCase(
    updateCycleAccountsTestEnvironmentWorkflowInstance.rejected,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', true);
    }
  );
};
