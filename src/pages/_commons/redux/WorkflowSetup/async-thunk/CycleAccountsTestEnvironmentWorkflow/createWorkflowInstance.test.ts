import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  workflowInstance: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      changeInformation: {
        changeSetId: 'changeSetId',
        name: 'mock name',
        description: 'mock description',
        effectiveDate: new Date()
      },
      configureParameters: {
        changeSetId: 'changeSetId',
        name: 'mock name',
        description: 'mock description',
        effectiveDate: new Date()
      },
      selectMonetaryAdjustmentTransactions: {},
      uploadFile: {}
    }
  }
};

jest.mock('app/helpers', () => {
  const modules = jest.requireActual('app/helpers');
  return {
    ...modules,
    formatTimeDefault: () => '01/01/2021'
  };
});

describe('redux-store > workflow-setup > CycleAccountsTestEnvironmentWorkflow > createWorkflowInstance', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.createCycleAccountsTestEnvironmentWorkflowInstance.pending(
        'createWorkflowInstance',
        { workflowSetupData: [] }
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.createCycleAccountsTestEnvironmentWorkflowInstance.fulfilled(
        {},
        'createWorkflowInstance',
        { workflowSetupData: [] }
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.createCycleAccountsTestEnvironmentWorkflowInstance.rejected(
        new Error('mock error'),
        'createWorkflowInstance',
        { workflowSetupData: [] }
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(true);
  });

  it('thunk action > without workflowInstanceId & changeId', async () => {
    const nextState: any =
      await actionsWorkflowSetup.createCycleAccountsTestEnvironmentWorkflowInstance(
        { workflowSetupData: [] }
      )(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer
            }
          } as RootState),
        {
          workflowService: {
            createWorkflowInstance: jest.fn().mockResolvedValue({
              data: { workflowInstanceDetails: { id: 'workflowId' } }
            }),
            updateWorkflowInstanceData: jest.fn()
          }
        }
      );

    expect(nextState.payload).toEqual({
      changeSetId: 'changeSetId',
      workflowInstanceId: 'workflowId'
    });
  });

  it('thunk action > with workflowInstanceId & changeId', async () => {
    const state = {
      workflowFormSteps: {
        forms: {
          changeInformation: {
            changeSetId: 'changeSetId',
            workflowInstanceId: 'workflowId',
            name: 'mock name',
            description: 'mock description',
            effectiveDate: new Date()
          },
          selectMonetaryAdjustmentTransactions: {},
          uploadFile: {},
          configureParameters: {
            configureParameters: {
              changeSetId: 'changeSetId',
              name: 'mock name',
              description: 'mock description',
              effectiveDate: new Date()
            }
          }
        }
      }
    };

    const nextState: any =
      await actionsWorkflowSetup.createCycleAccountsTestEnvironmentWorkflowInstance(
        { workflowSetupData: [] }
      )(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer,
              ...state
            }
          } as RootState),
        {
          workflowService: {
            updateWorkflowInstance: jest.fn(),
            updateWorkflowInstanceData: jest.fn()
          }
        }
      );
    expect(nextState.payload).toEqual({
      changeSetId: 'changeSetId',
      workflowInstanceId: 'workflowId'
    });
  });
});
