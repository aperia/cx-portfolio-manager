import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types/workflowSetupState';
import get from 'lodash.get';
import set from 'lodash.set';
import { ConfigureParametersFormValue } from 'pages/WorkflowManageChangeInTerms/ConfigureParameters';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { parseMethodData } from './helper';

export const setupChangeInTermsWorkflow = createAsyncThunk<
  any,
  WorkflowSetupThunkArg,
  ThunkAPIConfig
>(
  'workflowSetup/setupChangeInTermsWorkflow',
  async ({ workflowSetupData }, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

    const state = thunkAPI.getState();

    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;

    const changeInTermForm = state.workflowSetup?.workflowFormSteps.forms[
      'configureParametersChangeInTerm'
    ] as ConfigureParametersFormValue;
    const protectedBalancesAttributesForm = state.workflowSetup
      ?.workflowFormSteps.forms[
      'configureParametersProtectedBalancesAttributes'
    ] as ConfigureParametersFormValue;

    const methodList = parseMethodData(
      changeInTermForm,
      protectedBalancesAttributesForm
    );
    const updateRequest: UpdateflowInstanceData = {
      workflowInstanceId: changeForm.workflowInstanceId?.toString(),
      status: 'COMPLETE',
      data: {
        workflowSetupData,
        configurations: {
          methodList
        }
      }
    };

    await workflowService.updateWorkflowInstanceData(updateRequest);

    return { workflowSetupData };
  }
);
export const setupChangeInTermsWorkflowBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(setupChangeInTermsWorkflow.pending, draftState => {
    set(draftState.workflowSetup, 'loading', true);
  });
  builder.addCase(
    setupChangeInTermsWorkflow.fulfilled,
    (draftState, action) => {
      set(draftState.workflowSetup, 'loading', false);
      set(draftState.workflowSetup, 'error', false);
      set(
        draftState.workflowFormSteps,
        'workflowSetupData',
        action.payload?.workflowSetupData
      );
    }
  );
  builder.addCase(setupChangeInTermsWorkflow.rejected, draftState => {
    set(draftState.workflowSetup, 'loading', false);
    set(draftState.workflowSetup, 'error', true);
  });
};
