import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  workflowInstance: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      gettingStart: {
        protectedBalancesAttributes: '123'
      },
      changeInformation: {
        changeSetId: '1111',
        workflowInstanceId: '2222',
        name: 'mock name',
        description: 'mock description',
        workflowNote: 'mock note',
        effectiveDate: new Date()
      },
      designPromotions: {
        methodForm: []
      },
      configureParametersChangeInTerm: {
        isValid: true,
        isChangeInTermStep: true
      },
      configureParametersProtectedBalancesAttributes: {
        isValid: true,
        isChangeInTermStep: true
      }
    }
  }
};

jest.mock('app/helpers', () => {
  const modules = jest.requireActual('app/helpers');
  return {
    ...modules,
    formatTimeDefault: () => '01/01/2021'
  };
});

describe('redux-store > workflow-setup > DesignPromotionsWorkflow > updateWorkflowInstance', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.updateChangeInTermsWorkflowInstance.pending(
        'updateWorkflowInstance',
        {}
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.updateChangeInTermsWorkflowInstance.fulfilled(
        true,
        'updateWorkflowInstance',
        {}
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.updateChangeInTermsWorkflowInstance.rejected(
        new Error('mock error'),
        'updateWorkflowInstance',
        {}
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(true);
  });

  it('thunk action > with workflowInstanceId & changeId', async () => {
    const mockRes = { data: { workflowSetupData: [] } };
    const updateWorkflowDataFn = jest.fn().mockResolvedValue(mockRes);

    const nextState: any =
      await actionsWorkflowSetup.updateChangeInTermsWorkflowInstance({
        workflowSetupData: []
      })(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer
            }
          } as RootState),
        {
          workflowService: {
            updateWorkflowInstanceData: updateWorkflowDataFn
          }
        }
      );
    expect(nextState.payload.workflowSetupData).toEqual([]);
    expect(updateWorkflowDataFn).toHaveBeenCalled();
  });

  it('thunk action > with error', async () => {
    const mockRes = { data: { workflowSetupData: [] } };
    const updateWorkflowDataFn = jest.fn().mockResolvedValue(mockRes);

    const nextState: any =
      await actionsWorkflowSetup.updateChangeInTermsWorkflowInstance({
        workflowSetupData: []
      })(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer,
              workflowFormSteps: {
                forms: {
                  gettingStart: {
                    protectedBalancesAttributes: null
                  }
                }
              }
            }
          } as RootState),
        {
          workflowService: {
            updateWorkflowInstanceData: updateWorkflowDataFn
          }
        }
      );
    expect(nextState.payload?.workflowSetupData).toBeUndefined();
  });
});
