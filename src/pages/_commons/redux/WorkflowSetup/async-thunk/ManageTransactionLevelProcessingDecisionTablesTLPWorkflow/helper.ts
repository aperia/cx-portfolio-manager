import { booleanToString } from 'app/helpers';
import { TableType } from 'pages/WorkflowManageTransactionLevelProcessingDecisionTablesTLP/ConfigureParameters/constant';
import { ConfigTableItem } from 'pages/WorkflowManageTransactionLevelProcessingDecisionTablesTLP/ConfigureParameters/type';

export const parseTableData = (
  tableList: ConfigTableItem[] | undefined,
  type: TableType
) => {
  if (!tableList || tableList.length === 0) return [];

  return tableList.map(table => {
    const tableControlParameters = Array.isArray(table?.controlParameters)
      ? table?.controlParameters.map(parameter => ({
          name: parameter.id,
          value: parameter.value
        }))
      : [];
    const decisionElements = Array.isArray(table?.elementList)
      ? table?.elementList.map((i: Record<string, any>) => ({
          name: i.name,
          searchCode: i.searchCode,
          searchCodeText: i.searchCodeText,
          immediateAllocation: booleanToString(i.immediateAllocation),
          value: i.value
        }))
      : [];
    const modeledFrom = !!table?.selectedVersion
      ? {
          tableId: table?.selectedVersion?.tableId,
          tableName: table?.selectedVersion?.tableName,
          version: { tableId: table?.selectedVersion?.version?.tableId }
        }
      : undefined;

    return {
      id: '',
      tableId: table.tableId,
      tableName: table.tableName,
      comment: table.comment || '',
      decisionElements,
      tableControlParameters,
      tableType: type,
      modeledFrom
    };
  });
};
