import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';
import { ConfigureParametersFormValue } from 'pages/WorkflowManageTransactionLevelProcessingDecisionTablesTLP/ConfigureParameters';
import { TableType } from 'pages/WorkflowManageTransactionLevelProcessingDecisionTablesTLP/ConfigureParameters/constant';
import { FormGettingStart } from 'pages/WorkflowManageTransactionLevelProcessingDecisionTablesTLP/GettingStartStep';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { parseTableData } from './helper';

export const updateManageTransactionLevelProcessingDecisionTablesTLPWorkflowInstance =
  createAsyncThunk<any, WorkflowSetupThunkArg, ThunkAPIConfig>(
    'workflowSetup/updateManageTransactionLevelProcessingDecisionTablesTLPWorkflowInstance',
    async (args, thunkAPI) => {
      const { workflowSetupData } = args;
      const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

      const state = thunkAPI.getState();

      const gettingStart = state.workflowSetup?.workflowFormSteps.forms[
        'gettingStart'
      ] as FormGettingStart;

      // invalid save
      if (
        !gettingStart?.CA &&
        !gettingStart?.RQ &&
        !gettingStart?.TQ &&
        !gettingStart?.AQ
      )
        return;

      const changeForm = state.workflowSetup?.workflowFormSteps.forms[
        'changeInformation'
      ] as FormChangeInfo;

      const aqForm = state.workflowSetup?.workflowFormSteps.forms[
        'configTlpAq'
      ] as ConfigureParametersFormValue;

      const tablesAQForm = parseTableData(aqForm?.tables, TableType.tlpaq);

      const uploadFileAQ = state.workflowSetup?.workflowFormSteps.forms[
        'uploadFileAccountQualification'
      ] as any;

      const caForm = state.workflowSetup?.workflowFormSteps.forms[
        'configTlpCa'
      ] as ConfigureParametersFormValue;

      const tablesCAForm = parseTableData(caForm?.tables, TableType.tlpca);

      const uploadFileCA = state.workflowSetup?.workflowFormSteps.forms[
        'uploadFileClientAllocation'
      ] as any;

      const tqForm = state.workflowSetup?.workflowFormSteps.forms[
        'configTlpTq'
      ] as ConfigureParametersFormValue;

      const tablesTQForm = parseTableData(tqForm?.tables, TableType.tlptq);

      const uploadFileTQ = state.workflowSetup?.workflowFormSteps.forms[
        'uploadFileTransactionQualification'
      ] as any;

      const rqForm = state.workflowSetup?.workflowFormSteps.forms[
        'configTlpRq'
      ] as ConfigureParametersFormValue;

      const uploadFileRQ = state.workflowSetup?.workflowFormSteps.forms[
        'uploadFileRetailQualification'
      ] as any;

      const tablesRQForm = parseTableData(rqForm?.tables, TableType.tlprq);

      const tableList = [
        ...tablesAQForm,
        ...tablesTQForm,
        ...tablesCAForm,
        ...tablesRQForm
      ];

      const updateRequest: UpdateflowInstanceData = {
        workflowInstanceId: changeForm.workflowInstanceId?.toString(),
        status: 'INCOMPLETE',
        data: {
          workflowSetupData,
          configurations: {
            tableList,
            attachments: [
              {
                'table.type': 'AQ',
                attachmentId: uploadFileAQ?.attachmentId
              },
              {
                'table.type': 'CA',
                attachmentId: uploadFileCA?.attachmentId
              },
              {
                'table.type': 'TQ',
                attachmentId: uploadFileTQ?.attachmentId
              },
              {
                'table.type': 'RQ',
                attachmentId: uploadFileRQ?.attachmentId
              }
            ]
          }
        }
      };

      await workflowService.updateWorkflowInstanceData(updateRequest);
      return true;
    }
  );
export const updateManageTransactionLevelProcessingDecisionTablesTLPWorkflowInstanceBuilder =
  (builder: ActionReducerMapBuilder<WorkflowSetupRootState>) => {
    builder.addCase(
      updateManageTransactionLevelProcessingDecisionTablesTLPWorkflowInstance.pending,
      (draftState, action) => {
        set(draftState.workflowInstance, 'loading', true);
        set(draftState.workflowInstance, 'error', false);
      }
    );
    builder.addCase(
      updateManageTransactionLevelProcessingDecisionTablesTLPWorkflowInstance.fulfilled,
      (draftState, action) => {
        set(draftState.workflowInstance, 'loading', false);
        set(draftState.workflowInstance, 'error', false);
        set(
          draftState.workflowFormSteps.forms['changeInformation'],
          'isCompletedWorkflow',
          false
        );
      }
    );
    builder.addCase(
      updateManageTransactionLevelProcessingDecisionTablesTLPWorkflowInstance.rejected,
      (draftState, action) => {
        set(draftState.workflowInstance, 'loading', false);
        set(draftState.workflowInstance, 'error', true);
      }
    );
  };
