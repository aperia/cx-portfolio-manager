import { TableType } from 'pages/WorkflowManageTransactionLevelProcessingDecisionTablesTLP/ConfigureParameters/constant';
import { parseTableData } from './helper';

const fixture = (
  hasControlParameters?: boolean,
  hasElementList?: boolean,
  hasSelectedVersion?: boolean
) => [
  {
    rowId: 'rowId',
    tableId: 'tableId',
    tableName: 'tableName',
    elementList: hasElementList
      ? [
          { name: '1', rowId: '1', immediateAllocation: 'No' },
          { name: '2', rowId: '2', immediateAllocation: 'Yes' },
          { name: '3', rowId: '3' }
        ]
      : ({} as any),
    controlParameters: hasControlParameters
      ? [{ id: 'id', value: 'value' }]
      : (undefined as any),
    comment: '',
    actionTaken: '',
    selectedVersionId: '',
    selectedTableId: '',
    selectedVersion: hasSelectedVersion
      ? {
          tableId: 'tableId',
          tableName: 'tableName',
          version: {
            tableId: 'tableId',
            tableName: 'tableName',
            effectiveDate: '2022-02-02',
            status: 'Production',
            comment: '',
            decisionElements: [],
            tableControlParameters: [{ id: 'id', value: 'value' }]
          }
        }
      : (undefined as any)
  }
];

describe('test parseTableData helper', () => {
  it('should be return empty', () => {
    const actual = parseTableData(undefined, TableType.tlpaq);
    expect(actual).toEqual([]);
  });

  it('should be working', () => {
    const actual = parseTableData(fixture(), TableType.tlpca);
    expect(actual.length).toEqual(1);
  });

  it('should be working without tableControlParameters', () => {
    const actual = parseTableData(fixture(), TableType.tlprq);
    expect(actual.length).toEqual(1);
  });

  it('should be working without tableControlParameters and has immediateAllocation', () => {
    const actual = parseTableData(fixture(true, true, true), TableType.tlptq);
    expect(actual.length).toEqual(1);
  });
});
