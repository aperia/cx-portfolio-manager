import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { booleanToString } from 'app/helpers';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';
import { ElementItem } from 'pages/WorkflowManageTransactionLevelProcessingDecisionTablesTLP/ConfigureParameters/type';

interface Params {
  tableType: string;
  serviceNameAbbreviation: string;
}

export const getDecisionElementList = createAsyncThunk<
  any,
  Params,
  ThunkAPIConfig
>(
  'workflowSetup/getDecisionElementList',
  async (elementNames: Params, thunkAPI) => {
    const { changeService } = get(thunkAPI, 'extra') as APIMapping;

    const resp = await changeService.getDecisionElementList(
      elementNames.tableType,
      elementNames.serviceNameAbbreviation
    );
    return {
      decisionElementList: resp?.data?.decisionElements?.map(
        (e: ElementItem) => ({
          ...e,
          immediateAllocation: booleanToString(e.immediateAllocation)
        })
      )
    };
  }
);
export const getDecisionElementListBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(getDecisionElementList.pending, (draftState, action) => {
    set(
      draftState.decisionElementList,
      [
        `${action.meta.arg.serviceNameAbbreviation}${action.meta.arg.tableType}`,
        'loading'
      ],
      true
    );
    set(
      draftState.decisionElementList,
      [
        `${action.meta.arg.serviceNameAbbreviation}${action.meta.arg.tableType}`,
        'error'
      ],
      false
    );
  });
  builder.addCase(getDecisionElementList.fulfilled, (draftState, action) => {
    set(
      draftState.decisionElementList,
      [
        `${action.meta.arg.serviceNameAbbreviation}${action.meta.arg.tableType}`,
        'loading'
      ],
      false
    );
    set(
      draftState.decisionElementList,
      [
        `${action.meta.arg.serviceNameAbbreviation}${action.meta.arg.tableType}`,
        'error'
      ],
      false
    );
    set(
      draftState.decisionElementList,
      [
        `${action.meta.arg.serviceNameAbbreviation}${action.meta.arg.tableType}`,
        'decisionElementList'
      ],
      action.payload?.decisionElementList
    );
  });
  builder.addCase(getDecisionElementList.rejected, (draftState, action) => {
    set(
      draftState.decisionElementList,
      [
        `${action.meta.arg.serviceNameAbbreviation}${action.meta.arg.tableType}`,
        'loading'
      ],
      false
    );
    set(
      draftState.decisionElementList,
      [
        `${action.meta.arg.serviceNameAbbreviation}${action.meta.arg.tableType}`,
        'error'
      ],
      true
    );
  });
};
