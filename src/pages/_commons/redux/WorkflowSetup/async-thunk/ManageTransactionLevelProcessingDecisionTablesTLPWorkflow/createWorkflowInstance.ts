import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';
import { ConfigureParametersFormValue } from 'pages/WorkflowManageTransactionLevelProcessingDecisionTablesTLP/ConfigureParameters';
import { TableType } from 'pages/WorkflowManageTransactionLevelProcessingDecisionTablesTLP/ConfigureParameters/constant';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { workflowSetupTemplateIdSelector } from '../../select-hooks';
import { parseTableData } from './helper';

export const createManageTransactionLevelProcessingDecisionTablesTLPWorkflowInstance =
  createAsyncThunk<any, WorkflowSetupThunkArg, ThunkAPIConfig>(
    'workflowSetup/createManageTransactionLevelProcessingDecisionTablesTLPWorkflowInstance',
    async (args, thunkAPI) => {
      const { workflowSetupData } = args;
      const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

      const state = thunkAPI.getState();

      const changeForm = state.workflowSetup?.workflowFormSteps.forms[
        'changeInformation'
      ] as FormChangeInfo;

      const workflowTemplateId = workflowSetupTemplateIdSelector(state);

      const changeId = changeForm.changeSetId;
      let workflowInstanceId = changeForm.workflowInstanceId?.toString();

      if (workflowInstanceId && changeId) {
        await workflowService.updateWorkflowInstance({
          workflowInstanceId,
          changeSetId: changeId.toString(),
          workflowInstanceDetails: {
            note: changeForm.workflowNote
          }
        } as any);
      } else {
        const workflowRes = await workflowService.createWorkflowInstance({
          workflowInstanceDetails: {
            note: changeForm.workflowNote
          },
          workflowTemplateId: workflowTemplateId || '',
          changeSetId: changeId?.toString()
        });

        workflowInstanceId =
          workflowRes?.data?.workflowInstanceDetails?.id?.toString();
      }
      const aqForm = state.workflowSetup?.workflowFormSteps.forms[
        'configTlpAq'
      ] as ConfigureParametersFormValue;

      const tablesAQForm = parseTableData(aqForm?.tables, TableType.tlpaq);

      const uploadFileAQ = state.workflowSetup?.workflowFormSteps.forms[
        'uploadFileAccountQualification'
      ] as any;

      const caForm = state.workflowSetup?.workflowFormSteps.forms[
        'configTlpCa'
      ] as ConfigureParametersFormValue;

      const tablesCAForm = parseTableData(caForm?.tables, TableType.tlpca);

      const uploadFileCA = state.workflowSetup?.workflowFormSteps.forms[
        'uploadFileClientAllocation'
      ] as any;

      const tqForm = state.workflowSetup?.workflowFormSteps.forms[
        'configTlpTq'
      ] as ConfigureParametersFormValue;

      const tablesTQForm = parseTableData(tqForm?.tables, TableType.tlptq);

      const uploadFileTQ = state.workflowSetup?.workflowFormSteps.forms[
        'uploadFileTransactionQualification'
      ] as any;

      const rqForm = state.workflowSetup?.workflowFormSteps.forms[
        'configTlpRq'
      ] as ConfigureParametersFormValue;

      const uploadFileRQ = state.workflowSetup?.workflowFormSteps.forms[
        'uploadFileRetailQualification'
      ] as any;

      const tablesRQForm = parseTableData(rqForm?.tables, TableType.tlprq);

      const tableList = [
        ...tablesAQForm,
        ...tablesTQForm,
        ...tablesCAForm,
        ...tablesRQForm
      ] as any[];

      const updateRequest: UpdateflowInstanceData = {
        workflowInstanceId,
        status: 'INCOMPLETE',
        data: {
          workflowSetupData,
          configurations: {
            tableList,
            attachments: [
              {
                'table.type': 'AQ',
                attachmentId: uploadFileAQ?.attachmentId
              },
              {
                'table.type': 'CA',
                attachmentId: uploadFileCA?.attachmentId
              },
              {
                'table.type': 'TQ',
                attachmentId: uploadFileTQ?.attachmentId
              },
              {
                'table.type': 'RQ',
                attachmentId: uploadFileRQ?.attachmentId
              }
            ]
          }
        }
      };

      await workflowService.updateWorkflowInstanceData(updateRequest);

      return { changeId, workflowInstanceId };
    }
  );
export const createManageTransactionLevelProcessingDecisionTablesTLPWorkflowBuilder =
  (builder: ActionReducerMapBuilder<WorkflowSetupRootState>) => {
    builder.addCase(
      createManageTransactionLevelProcessingDecisionTablesTLPWorkflowInstance.pending,
      (draftState, action) => {
        set(draftState.workflowInstance, 'loading', true);
        set(draftState.workflowInstance, 'error', false);
      }
    );
    builder.addCase(
      createManageTransactionLevelProcessingDecisionTablesTLPWorkflowInstance.fulfilled,
      (draftState, action) => {
        set(draftState.workflowInstance, 'loading', false);
        set(draftState.workflowInstance, 'error', false);
        set(
          draftState.workflowFormSteps.forms['changeInformation'],
          'changeSetId',
          action.payload?.changeId
        );
        set(
          draftState.workflowFormSteps.forms['changeInformation'],
          'workflowInstanceId',
          action.payload?.workflowInstanceId
        );
        set(
          draftState.workflowFormSteps.forms['changeInformation'],
          'isCompletedWorkflow',
          false
        );
      }
    );
    builder.addCase(
      createManageTransactionLevelProcessingDecisionTablesTLPWorkflowInstance.rejected,
      (draftState, action) => {
        set(draftState.workflowInstance, 'loading', false);
        set(draftState.workflowInstance, 'error', true);
      }
    );
  };
