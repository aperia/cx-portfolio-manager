import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  workflowInstance: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      changeInformation: {
        changeSetId: '1111',
        workflowInstanceId: '2222',
        name: 'mock name',
        description: 'mock description',
        workflowNote: 'mock note',
        effectiveDate: new Date()
      },
      gettingStart: {
        CA: true,
        AQ: true,
        RQ: true,
        TQ: true
      }
    }
  }
};

const initReducerWithGettingStarted: any = {
  workflowInstance: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      changeInformation: {
        changeSetId: '1111',
        workflowInstanceId: '2222',
        name: 'mock name',
        description: 'mock description',
        workflowNote: 'mock note',
        effectiveDate: new Date()
      },
      gettingStart: {
        CA: false,
        AQ: false,
        RQ: false,
        TQ: false
      }
    }
  }
};

jest.mock('app/helpers', () => {
  const modules = jest.requireActual('app/helpers');
  return {
    ...modules,
    formatTimeDefault: () => '01/01/2021'
  };
});

describe('redux-store > workflow-setup > ManageTransactionLevelProcessingDecisionTablesTLPWorkflow > updateWorkflowInstance', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.updateManageTransactionLevelProcessingDecisionTablesTLPWorkflowInstance.pending(
        'updateWorkflowInstance',
        {}
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.updateManageTransactionLevelProcessingDecisionTablesTLPWorkflowInstance.fulfilled(
        true,
        'updateWorkflowInstance',
        {}
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.updateManageTransactionLevelProcessingDecisionTablesTLPWorkflowInstance.rejected(
        new Error('mock error'),
        'updateWorkflowInstance',
        {}
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(true);
  });

  it('thunk action > with workflowInstanceId & changeId and gettingStart ca equal false', async () => {
    const mockRes = { data: { workflowSetupData: [] } };
    const updateWorkflowDataFn = jest.fn().mockResolvedValue(mockRes);

    const nextState: any =
      await actionsWorkflowSetup.updateManageTransactionLevelProcessingDecisionTablesTLPWorkflowInstance(
        {
          workflowSetupData: []
        }
      )(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducerWithGettingStarted
            }
          } as RootState),
        {
          workflowService: {
            updateWorkflowInstanceData: updateWorkflowDataFn
          }
        }
      );
    expect(nextState?.payload?.workflowSetupData).toBeUndefined();
  });
  it('thunk action > with workflowInstanceId & changeId and gettingStart ca equal true', async () => {
    const mockRes = { data: { workflowSetupData: [] } };
    const updateWorkflowDataFn = jest.fn().mockResolvedValue(mockRes);

    const nextState: any =
      await actionsWorkflowSetup.updateManageTransactionLevelProcessingDecisionTablesTLPWorkflowInstance(
        {
          workflowSetupData: []
        }
      )(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer
            }
          } as RootState),
        {
          workflowService: {
            updateWorkflowInstanceData: updateWorkflowDataFn
          }
        }
      );
    expect(nextState?.payload?.workflowSetupData).toBeUndefined();
    expect(updateWorkflowDataFn).toHaveBeenCalled();
  });

  it('thunk action > with workflowInstanceId & changeId', async () => {
    const updateWorkflowDataFn = jest.fn();

    const nextState: any =
      await actionsWorkflowSetup.updateManageTransactionLevelProcessingDecisionTablesTLPWorkflowInstance(
        {
          workflowSetupData: []
        }
      )(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer,
              workflowFormSteps: {
                ...initReducer.workflowFormSteps,
                forms: {
                  gettingStart: {},
                  changeInformation: {
                    changeSetId: '1111',
                    workflowInstanceId: '2222',
                    name: 'mock name',
                    description: 'mock description',
                    workflowNote: 'mock note',
                    effectiveDate: new Date()
                  }
                }
              }
            }
          } as RootState),
        {
          workflowService: {
            updateWorkflowInstanceData: updateWorkflowDataFn
          }
        }
      );
    expect(nextState.payload).toEqual(undefined);
  });
});
