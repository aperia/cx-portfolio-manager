import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types/workflowSetupState';
import get from 'lodash.get';
import set from 'lodash.set';
import { ConfigureParametersFormValue } from 'pages/WorkflowManageTransactionLevelProcessingDecisionTablesTLP/ConfigureParameters';
import { TableType } from 'pages/WorkflowManageTransactionLevelProcessingDecisionTablesTLP/ConfigureParameters/constant';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { parseTableData } from './helper';

export const setupManageTransactionLevelProcessingDecisionTablesTLPWorkflowInstance =
  createAsyncThunk<any, WorkflowSetupThunkArg, ThunkAPIConfig>(
    'workflowSetup/setupManageTransactionLevelProcessingDecisionTablesTLPWorkflowInstance',
    async (args, thunkAPI) => {
      const { workflowSetupData } = args;
      const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

      const state = thunkAPI.getState();

      const changeForm = state.workflowSetup?.workflowFormSteps.forms[
        'changeInformation'
      ] as FormChangeInfo;

      const aqForm = state.workflowSetup?.workflowFormSteps.forms[
        'configTlpAq'
      ] as ConfigureParametersFormValue;

      const tablesAQForm = parseTableData(aqForm?.tables, TableType.tlpaq);

      const uploadFileAQ = state.workflowSetup?.workflowFormSteps.forms[
        'uploadFileAccountQualification'
      ] as any;

      const caForm = state.workflowSetup?.workflowFormSteps.forms[
        'configTlpCa'
      ] as ConfigureParametersFormValue;

      const tablesCAForm = parseTableData(caForm?.tables, TableType.tlpca);

      const uploadFileCA = state.workflowSetup?.workflowFormSteps.forms[
        'uploadFileClientAllocation'
      ] as any;

      const tqForm = state.workflowSetup?.workflowFormSteps.forms[
        'configTlpTq'
      ] as ConfigureParametersFormValue;

      const tablesTQForm = parseTableData(tqForm?.tables, TableType.tlptq);

      const uploadFileTQ = state.workflowSetup?.workflowFormSteps.forms[
        'uploadFileTransactionQualification'
      ] as any;

      const rqForm = state.workflowSetup?.workflowFormSteps.forms[
        'configTlpRq'
      ] as ConfigureParametersFormValue;

      const uploadFileRQ = state.workflowSetup?.workflowFormSteps.forms[
        'uploadFileRetailQualification'
      ] as any;

      const tablesRQForm = parseTableData(rqForm?.tables, TableType.tlprq);

      const tableList = [
        ...tablesAQForm,
        ...tablesTQForm,
        ...tablesCAForm,
        ...tablesRQForm
      ];

      const updateRequest: UpdateflowInstanceData = {
        workflowInstanceId: changeForm.workflowInstanceId?.toString(),
        status: 'COMPLETE',
        data: {
          workflowSetupData,
          configurations: {
            tableList,
            attachments: [
              {
                'table.type': 'AQ',
                attachmentId: uploadFileAQ?.attachmentId
              },
              {
                'table.type': 'CA',
                attachmentId: uploadFileCA?.attachmentId
              },
              {
                'table.type': 'TQ',
                attachmentId: uploadFileTQ?.attachmentId
              },
              {
                'table.type': 'RQ',
                attachmentId: uploadFileRQ?.attachmentId
              }
            ]
          }
        }
      };

      await workflowService.updateWorkflowInstanceData(updateRequest);

      return true;
    }
  );
export const setupsetupManageTransactionLevelProcessingDecisionTablesTLPWorkflowBuilder =
  (builder: ActionReducerMapBuilder<WorkflowSetupRootState>) => {
    builder.addCase(
      setupManageTransactionLevelProcessingDecisionTablesTLPWorkflowInstance.pending,
      draftState => {
        set(draftState.workflowSetup, 'loading', true);
      }
    );
    builder.addCase(
      setupManageTransactionLevelProcessingDecisionTablesTLPWorkflowInstance.fulfilled,
      (draftState, action) => {
        set(draftState.workflowSetup, 'loading', false);
        set(draftState.workflowSetup, 'error', false);
      }
    );
    builder.addCase(
      setupManageTransactionLevelProcessingDecisionTablesTLPWorkflowInstance.rejected,
      draftState => {
        set(draftState.workflowSetup, 'loading', false);
        set(draftState.workflowSetup, 'error', true);
      }
    );
  };
