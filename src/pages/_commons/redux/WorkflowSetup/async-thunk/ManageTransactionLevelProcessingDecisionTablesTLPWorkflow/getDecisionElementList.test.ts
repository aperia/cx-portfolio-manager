import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  decisionElementList: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      changeInformation: {
        name: 'mock name',
        description: 'mock description',
        effectiveDate: new Date()
      }
    }
  }
};

describe('redux-store > workflow > ManageTransactionLevelProcessingDecisionTablesTLPWorkflow >  getDecisionElementList', async () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getDecisionElementList.pending(
        'decisionElementList',
        {}
      )
    );
    expect(nextState.decisionElementList.loading).toEqual(false);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getDecisionElementList.fulfilled(
        {},
        'decisionElementList',
        {}
      )
    );
    expect(nextState.decisionElementList.loading).toEqual(false);
    expect(nextState.decisionElementList.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getDecisionElementList.rejected(
        new Error('mock error'),
        'decisionElementList',
        {}
      )
    );
    expect(nextState.decisionElementList.loading).toEqual(false);
    expect(nextState.decisionElementList.error).toEqual(false);
  });

  it('thunk action > with existing changeId ', async () => {
    const result = {
      data: { decisionElements: [{ id: '1111' }] }
    };
    const state = {
      workflowSetup: { workflow: { id: undefined } },
      workflowFormSteps: {
        forms: {
          changeInformation: { workflowInstanceId: 'workflowInstanceId' },
          uploadFile: {},
          configureParameters: {}
        }
      }
    };
    const nextState: any = await actionsWorkflowSetup.getDecisionElementList({
      tableType: '123',
      serviceNameAbbreviation: ''
    })(
      store.dispatch,
      () => ({ ...store.getState(), workflowSetup: { ...state } } as RootState),
      {
        changeService: {
          getDecisionElementList: jest.fn().mockResolvedValue(result)
        }
      }
    );
    expect(nextState.payload.decisionElementList[0].id).toEqual('1111');
  });
});
