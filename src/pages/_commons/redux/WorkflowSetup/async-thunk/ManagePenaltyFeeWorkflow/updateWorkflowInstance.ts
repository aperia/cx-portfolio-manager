import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';
import { FormGettingStart } from 'pages/WorkflowManagePenaltyFee/GettingStartStep';
import { ReturnedCheckChargesFormValues } from 'pages/WorkflowManagePenaltyFee/ReturnedCheckChargesStep';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { parseMethodData } from './helper';

export const updateManagePenaltyFeeWorkflowInstance = createAsyncThunk<
  any,
  WorkflowSetupThunkArg,
  ThunkAPIConfig
>(
  'workflowSetup/updateManagePenaltyFeeWorkflowInstance',
  async ({ workflowSetupData }, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

    const state = thunkAPI.getState();

    const gettingStart = state.workflowSetup?.workflowFormSteps.forms[
      'gettingStart'
    ] as FormGettingStart;

    // invalid save
    if (!gettingStart?.lateCharges && !gettingStart.returnedCheckCharges)
      return;

    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;

    const rcForm = state.workflowSetup?.workflowFormSteps.forms[
      'returnedCheckCharges'
    ] as ReturnedCheckChargesFormValues;

    const lcForm = state.workflowSetup?.workflowFormSteps.forms[
      'lateCharges'
    ] as ReturnedCheckChargesFormValues;
    const methodList = parseMethodData(rcForm, lcForm);
    const updateRequest: UpdateflowInstanceData = {
      workflowInstanceId: changeForm.workflowInstanceId?.toString(),
      status: 'INCOMPLETE',
      data: {
        workflowSetupData,
        configurations: {
          methodList
        }
      }
    };

    await workflowService.updateWorkflowInstanceData(updateRequest);
    return { workflowSetupData };
  }
);
export const updateManagePenaltyFeeWorkflowInstanceBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    updateManagePenaltyFeeWorkflowInstance.pending,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', true);
      set(draftState.workflowInstance, 'error', false);
    }
  );
  builder.addCase(
    updateManagePenaltyFeeWorkflowInstance.fulfilled,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', false);
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'isCompletedWorkflow',
        false
      );
      set(
        draftState.workflowFormSteps,
        'workflowSetupData',
        action.payload?.workflowSetupData
      );
    }
  );
  builder.addCase(
    updateManagePenaltyFeeWorkflowInstance.rejected,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', true);
    }
  );
};
