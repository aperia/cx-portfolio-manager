import { ChangeList, ChangeMapping } from 'app/fixtures/change-data';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';
import { defaultWorkflowChangeSetFilter } from '../reducers';

const initReducer: any = {
  workflowSetup: {
    workflow: {
      id: '1111'
    }
  },
  workflowChangeSets: {
    loading: false,
    error: false,
    dataFilter: defaultWorkflowChangeSetFilter,
    changeSetList: []
  }
};

const mockWorkflowTemplateId = '1111';

describe('redux-store > workflow-setup > getWorkflowChangeSetList', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getWorkflowChangeSetList.pending(
        'getWorkflowChangeSetList',
        mockWorkflowTemplateId
      )
    );
    expect(nextState.workflowChangeSets.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getWorkflowChangeSetList.fulfilled(
        {},
        'getWorkflowChangeSetList',
        mockWorkflowTemplateId
      )
    );
    expect(nextState.workflowChangeSets.loading).toEqual(false);
    expect(nextState.workflowChangeSets.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getWorkflowChangeSetList.rejected(
        new Error('mock error'),
        'getWorkflowChangeSetList',
        mockWorkflowTemplateId
      )
    );
    expect(nextState.workflowChangeSets.loading).toEqual(false);
    expect(nextState.workflowChangeSets.error).toEqual(true);
  });

  it('thunk action > with changeSetWithTemplate', async () => {
    const nextState: any = await actionsWorkflowSetup.getWorkflowChangeSetList('')(store.dispatch, () => ({
          ...store.getState(),
          workflowSetup: {
            ...initReducer
          },
          mapping: { data: { workflowChangeSetList: ChangeMapping } }
        } as RootState),
      {
        changeService: {
          getWorkflowChangeSetList: jest.fn().mockResolvedValue({
            data: {
              changeSetList: ChangeList,
              changeSetWithTemplate: ['1', '2']
            }
          })
        }
      }
    );
    
    expect(nextState.payload.changeSetWithTemplate).toEqual(['1', '2']);
    expect(nextState.payload.changes.length).toEqual(3);
    expect(nextState.payload.dynamicFields.changeStatus).toEqual([
      'rejected',
      'validated',
      'work'
    ]);
  });

  it('thunk action > without changeSetList data', async () => {
    const nextState: any = await actionsWorkflowSetup.getWorkflowChangeSetList(
      mockWorkflowTemplateId
    )(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          workflowSetup: {
            ...initReducer
          },
          mapping: { data: { workflowChangeSetList: ChangeMapping } }
        } as RootState),
      {
        changeService: {
          getWorkflowChangeSetList: jest.fn().mockResolvedValue({
            data: {
              changeSetList: []
            }
          })
        }
      }
    );
    expect(nextState.payload.changeSetWithTemplate).toBeUndefined();
    expect(nextState.payload.changes.length).toEqual(0);
    expect(nextState.payload.dynamicFields.changeStatus).toBeUndefined();
  });
});
