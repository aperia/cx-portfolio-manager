import { MethodFieldParameterEnum } from 'app/constants/enums';
import { parseMethodData, parseMethodFieldParameter } from './helper';

describe('redux-store > workflow-setup > DesignLoanOffersWorkflow > designLoanOffers > helper', () => {
  it('parseMethodFieldParameter', () => {
    let input = {
      name: MethodFieldParameterEnum.ComputerLetter,
      newValue: '1111'
    };
    let result = parseMethodFieldParameter(input);
    expect(result.newValue).toEqual('1111');

    input.newValue = '';
    result = parseMethodFieldParameter(input);
    expect(result.newValue).toEqual('0000');

    input = { name: MethodFieldParameterEnum.NumberOfDays, newValue: '1' };
    result = parseMethodFieldParameter(input);
    expect(result.newValue).toEqual('01');

    input.newValue = '';
    result = parseMethodFieldParameter(input);
    expect(result.newValue).toEqual('00');
  });

  it('parseMethodData', () => {
    // eslint-disable-next-line prefer-const
    let input: any;
    let result = parseMethodData(input);
    expect(result).toEqual([]);

    input = {
      methods: [
        {
          name: 'name',
          modeledFrom: 'modeledFrom',
          comment: 'comment',
          serviceSubjectSection: 'serviceSubjectSection',
          parameters: [
            {
              name: MethodFieldParameterEnum.ComputerLetter,
              newValue: '1111'
            }
          ]
        },
        {
          name: 'name1',
          serviceSubjectSection: 'serviceSubjectSection1',
          parameters: [
            {
              name: MethodFieldParameterEnum.ComputerLetter,
              newValue: '1111'
            }
          ]
        }
      ]
    };

    result = parseMethodData(input);
    expect(result).toEqual([
      {
        id: '',
        name: 'name',
        modeledFrom: 'modeledFrom',
        comment: 'comment',
        serviceSubjectSection: 'serviceSubjectSection',
        parameters: [
          {
            name: MethodFieldParameterEnum.ComputerLetter,
            newValue: '1111'
          }
        ]
      },
      {
        id: '',
        name: 'name1',
        modeledFrom: null,
        comment: '',
        serviceSubjectSection: 'serviceSubjectSection1',
        parameters: [
          {
            name: MethodFieldParameterEnum.ComputerLetter,
            newValue: '1111'
          }
        ]
      }
    ]);
  });
});
