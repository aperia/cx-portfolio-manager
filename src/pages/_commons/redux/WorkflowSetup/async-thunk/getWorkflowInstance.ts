import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';

export const getWorkflowInstance = createAsyncThunk<
  WorkflowSetupInstance,
  string,
  ThunkAPIConfig
>('workflowSetup/getWorkflowInstance', async (workflowId: string, thunkAPI) => {
  const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

  const response = await workflowService.getWorkflowInstance(workflowId);
  return response.data;
});
export const getWorkflowInstanceBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(getWorkflowInstance.pending, (draftState, action) => {
    set(draftState.workflowInstance, 'loading', true);
    set(draftState.workflowInstance, 'error', false);
  });
  builder.addCase(getWorkflowInstance.fulfilled, (draftState, action) => {
    set(draftState.workflowInstance, 'loading', false);
    set(draftState.workflowInstance, 'error', false);
    set(draftState.workflowInstance, 'instance', action.payload);
    // Parse WorkflowSetupData from response
    // Remove id due to error 500
    const workflowSetupData = action?.payload.data?.workflowSetupData;
    const removedIdData = workflowSetupData?.map(w => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { id, ...rest } = w.data || {};
      return {
        ...w,
        data: rest
      };
    });
    set(draftState.workflowFormSteps, 'workflowSetupData', removedIdData);

    const workflow = action?.payload.workflowInstanceDetails;
    set(
      draftState.workflowFormSteps,
      ['forms', 'changeInformation', 'workflowInstanceId'],
      workflow.id
    );
    set(
      draftState.workflowFormSteps,
      ['forms', 'changeInformation', 'changeSetId'],
      workflow.changeSetId
    );
  });
  builder.addCase(getWorkflowInstance.rejected, (draftState, action) => {
    set(draftState.workflowInstance, 'loading', false);
    set(draftState.workflowInstance, 'error', true);
  });
};
