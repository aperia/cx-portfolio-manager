import store from 'app/_libraries/_dof/core/redux/createAppStore';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  workflowInstance: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      changeInformation: {
        name: 'mock name',
        description: 'mock description',
        effectiveDate: new Date()
      }
    }
  }
};

describe('redux-store > workflow-setup > FlexibleMonetaryTransactionsWorkflow', () => {
  it('getWorkflowTransactionList with specify type', async () => {
    const nextState: any =
      await actionsWorkflowSetup.getWorkflowTransactionList({})(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer
            }
          } as RootState),
        {
          workflowService: {
            getWorkflowTransactionList: jest.fn().mockResolvedValue({})
          }
        }
      );

    expect(nextState.payload).toEqual({});
  });

  it('getWorkflowTransactionList with multiple types', async () => {
    const nextState: any =
      await actionsWorkflowSetup.getWorkflowTransactionList({
        type: 'Moneytary'
      })(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer
            }
          } as RootState),
        {
          workflowService: {
            getWorkflowTransactionList: jest.fn().mockResolvedValue({})
          }
        }
      );

    expect(nextState.payload).toEqual({});
  });
});
