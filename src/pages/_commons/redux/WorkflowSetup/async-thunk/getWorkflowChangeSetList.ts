import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { ChangeType } from 'app/constants/enums';
import {
  CHANGE_STATUS_MAPPING,
  CHANGE_TYPE_MAPPING
} from 'app/constants/mapping';
import { formatTime, mappingDataAndDynamicFieldsFromArray } from 'app/helpers';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import _orderBy from 'lodash.orderby';
import set from 'lodash.set';
import { workflowSetupTemplateIdSelector } from '../select-hooks';

export const getWorkflowChangeSetList = createAsyncThunk<
  any,
  string,
  ThunkAPIConfig
>('workflowSetup/getWorkflowChangeSetList', async (any, thunkAPI) => {
  const { changeService } = get(thunkAPI, 'extra') as APIMapping;

  // Take mapping model
  const state = thunkAPI.getState();
  const workflowTemplateId = workflowSetupTemplateIdSelector(state);
  const workflowChangeSetListMapping =
    state.mapping?.data?.workflowChangeSetList;
  const resp = await changeService.getWorkflowChangeSetList(workflowTemplateId);
  const changeSetList = resp?.data?.changeSetList;
  const changeSetWithTemplate = resp?.data?.changeSetWithTemplate;

  // Mapping
  const mappedData = mappingDataAndDynamicFieldsFromArray(
    changeSetList,
    workflowChangeSetListMapping!,
    ['changeStatus']
  );
  const { data, dynamicFields } = mappedData;

  if (!data) {
    return { changes: [], dynamicFields: {} };
  }

  const changeData = data.map((p: IWorkflowChangeDetail) => {
    p.changeStatus = CHANGE_STATUS_MAPPING[p.changeStatus!];
    p.changeType = CHANGE_TYPE_MAPPING[p.changeType];
    if (p.changeType !== ChangeType.Pricing) {
      delete p.pricingStrategiesCreatedCount;
      delete p.pricingStrategiesUpdatedCount;
      delete p.pricingStrategiesImpacted;
      delete p.pricingMethodsCreatedCount;
      delete p.pricingMethodsUpdatedCount;
      delete p.noOfDMMTablesCreated;
      delete p.noOfDMMTablesUpdated;
    } else {
      delete p.cardholdersAccountsImpacted;
      delete p.portfoliosImpacted;
    }
    return p;
  });

  const rawData = changeData?.map(r => ({
    ...r,
    changeId: r.changeId && `${r.changeId}`,
    effectiveDateFormat: formatTime(r.effectiveDate).date
  }));

  const changes = _orderBy(
    rawData,
    ['effectiveDateFormat', 'changeName'],
    ['desc', 'asc']
  );

  return { changes, dynamicFields, changeSetWithTemplate };
});
export const getWorkflowChangeSetListBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(getWorkflowChangeSetList.pending, (draftState, action) => {
    set(draftState.workflowChangeSets, 'loading', true);
    set(draftState.workflowChangeSets, 'error', false);
  });
  builder.addCase(getWorkflowChangeSetList.fulfilled, (draftState, action) => {
    set(draftState.workflowChangeSets, 'loading', false);
    set(draftState.workflowChangeSets, 'error', false);
    set(draftState.workflowChangeSets, 'changeSetList', action.payload.changes);
    set(
      draftState.workflowChangeSets,
      'changeSetWithTemplate',
      action.payload.changeSetWithTemplate
    );
    set(
      draftState.workflowChangeSets,
      'changeStatusList',
      get(action.payload.dynamicFields, 'changeStatus', [])
    );
  });
  builder.addCase(getWorkflowChangeSetList.rejected, (draftState, action) => {
    set(draftState.workflowChangeSets, 'loading', false);
    set(draftState.workflowChangeSets, 'error', true);
  });
};
