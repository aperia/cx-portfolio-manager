import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';
import { ConfigureParametersFormValue } from 'pages/WorkflowManageIncomeOptionMethods/ConfigureParameters';
import { FormGettingStart } from 'pages/WorkflowManageIncomeOptionMethods/GettingStartStep';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { parseMethodData } from './helper';

export const updateIncomeOptionMethodsWorkflowInstance = createAsyncThunk<
  any,
  WorkflowSetupThunkArg,
  ThunkAPIConfig
>(
  'workflowSetup/updateIncomeOptionMethodsWorkflowInstance',
  async ({ workflowSetupData }, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

    const state = thunkAPI.getState();

    const gettingStart = state.workflowSetup?.workflowFormSteps.forms[
      'gettingStart'
    ] as FormGettingStart;

    // invalid save
    if (!gettingStart?.miscellaneousCharges && !gettingStart.annualCharges)
      return;

    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;

    const changeInTermForm = state.workflowSetup?.workflowFormSteps.forms[
      'configureParametersAnnualCharges'
    ] as ConfigureParametersFormValue;
    const protectedBalancesAttributesForm = state.workflowSetup
      ?.workflowFormSteps.forms[
      'configureParametersMiscellaneousCharges'
    ] as ConfigureParametersFormValue;

    const methodList = parseMethodData(
      changeInTermForm,
      protectedBalancesAttributesForm
    );

    const updateRequest: UpdateflowInstanceData = {
      workflowInstanceId: changeForm.workflowInstanceId?.toString(),
      status: 'INCOMPLETE',
      data: {
        workflowSetupData,
        configurations: {
          methodList
        }
      }
    };

    await workflowService.updateWorkflowInstanceData(updateRequest);
    return { workflowSetupData };
  }
);
export const updateIncomeOptionMethodsWorkflowInstanceBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    updateIncomeOptionMethodsWorkflowInstance.pending,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', true);
      set(draftState.workflowInstance, 'error', false);
    }
  );
  builder.addCase(
    updateIncomeOptionMethodsWorkflowInstance.fulfilled,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', false);
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'isCompletedWorkflow',
        false
      );
      set(
        draftState.workflowFormSteps,
        'workflowSetupData',
        action.payload?.workflowSetupData
      );
    }
  );
  builder.addCase(
    updateIncomeOptionMethodsWorkflowInstance.rejected,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', true);
    }
  );
};
