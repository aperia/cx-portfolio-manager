import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';
import { workflowSetupTemplateIdSelector } from '../select-hooks';

export const getWorkflowMetadata = createAsyncThunk<
  any,
  string[],
  ThunkAPIConfig
>(
  'workflowSetup/getWorkflowMetadata',
  async (elementNames: string[], thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

    const state = thunkAPI.getState();

    const workflowTemplateId = workflowSetupTemplateIdSelector(state);
    const elementMetadata = state.workflowSetup?.elementMetadata;

    let elements: ElementMetaData[] = [];

    if (
      (!elementMetadata?.error || elementMetadata?.elements.length == 0) &&
      elementNames.length > 0
    ) {
      const result = await workflowService.getElementDataByName(
        workflowTemplateId || '',
        elementNames
      );
      elements = elements.concat(result.data?.elementMetaData || []);
    }

    return { elements };
  }
);

export const getWorkflowMetadataBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(getWorkflowMetadata.pending, (draftState, action) => {
    set(draftState.elementMetadata, 'loading', true);
    set(draftState.elementMetadata, 'error', false);
  });
  builder.addCase(getWorkflowMetadata.fulfilled, (draftState, action) => {
    set(draftState.elementMetadata, 'loading', false);
    set(draftState.elementMetadata, 'error', false);
    set(draftState.elementMetadata, 'elements', [
      ...(draftState.elementMetadata.elements || []),
      ...(action.payload.elements || [])
    ]);
  });
  builder.addCase(getWorkflowMetadata.rejected, (draftState, action) => {
    set(draftState.elementMetadata, 'loading', false);
    set(draftState.elementMetadata, 'error', true);
  });
};
