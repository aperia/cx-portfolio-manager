import { mockThunkAction } from 'app/utils';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';
import * as UpdateManageAccountSerializationWorkflowInstance from './updateWorkflowInstance';

const initReducer: any = {
  workflowInstance: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      manageAccountSerialization: {
        sysPrinAgents: [
          {
            sysPrinAgent: 'sys1',
            recordTypes: 'record1'
          },
          {
            sysPrinAgent: 'sys2',
            recordTypes: 'record2'
          }
        ]
      },
      changeInformation: {
        name: 'mock name',
        description: 'mock description',
        effectiveDate: new Date(),
        changeSetId: '1',
        workflowInstanceId: '1'
      }
    }
  }
};

jest.mock('app/helpers', () => {
  const modules = jest.requireActual('app/helpers');
  return {
    ...modules,
    formatTimeDefault: () => '01/01/2021'
  };
});

const mockUpdateManageAccountSerializationWorkflowInstance = mockThunkAction(
  UpdateManageAccountSerializationWorkflowInstance
);

describe('redux-store > workflow-setup > ManageAccountSerializationWorkflow > createWorkflowInstance', () => {
  beforeEach(() => {
    mockUpdateManageAccountSerializationWorkflowInstance(
      'updateManageAccountSerializationWorkflowInstance'
    );
  });
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.createManageAccountSerializationWorkflowInstance.pending(
        'createWorkflowInstance',
        {}
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.createManageAccountSerializationWorkflowInstance.fulfilled(
        {},
        'createWorkflowInstance',
        {}
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.createManageAccountSerializationWorkflowInstance.rejected(
        new Error('mock error'),
        'createWorkflowInstance',
        {}
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(true);
  });

  it('thunk action > without workflowInstanceId & changeId', async () => {
    const nextState: any =
      await actionsWorkflowSetup.createManageAccountSerializationWorkflowInstance(
        {
          workflowSetupData: []
        }
      )(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer
            }
          } as RootState),
        {
          workflowService: {
            createWorkflowInstance: jest.fn().mockResolvedValue({
              data: { workflowInstanceDetails: { id: '1111' } }
            }),
            updateWorkflowInstanceData: jest.fn()
          },
          changeService: {
            createChangeSet: jest.fn().mockResolvedValue({
              data: { id: undefined }
            })
          }
        }
      );

    expect(nextState.payload).toBeUndefined();
  });

  it('thunk action > with workflowInstanceId & changeId', async () => {
    const state = {
      workflowSetup: {
        workflow: {
          id: undefined
        }
      },
      workflowFormSteps: {
        forms: {
          manageAccountSerialization: {
            sysPrinAgents: [
              {
                sysPrinAgent: 'sys1',
                recordTypes: 'record1'
              },
              {
                sysPrinAgent: 'sys2',
                recordTypes: 'record2'
              }
            ]
          },
          changeInformation: {
            changeSetId: '2222',
            workflowInstanceId: '1111',
            name: 'mock name',
            description: 'mock description',
            effectiveDate: new Date()
          },
          gettingStart: {
            returnedCheckCharges: true,
            lateCharges: true
          }
        }
      }
    };
    const nextState: any =
      await actionsWorkflowSetup.createManageAccountSerializationWorkflowInstance(
        {
          workflowSetupData: []
        }
      )(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...state
            }
          } as RootState),
        {
          workflowService: {
            updateWorkflowInstance: jest.fn(),
            updateWorkflowInstanceData: jest.fn()
          }
        }
      );
    expect(nextState.payload.workflowInstanceId).toEqual('1111');
  });

  it('thunk action > with existing changeId ', async () => {
    const state = {
      workflowSetup: {
        workflow: {
          id: undefined
        }
      },
      workflowFormSteps: {
        forms: {
          manageAccountSerialization: {
            sysPrinAgents: [
              {
                sysPrinAgent: 'sys1',
                recordTypes: 'record1'
              },
              {
                sysPrinAgent: 'sys2',
                recordTypes: 'record2'
              }
            ]
          },
          changeInformation: {
            changeSetId: '2222',
            name: 'mock name',
            description: 'mock description',
            effectiveDate: new Date()
          }
        }
      }
    };
    const nextState: any =
      await actionsWorkflowSetup.createManageAccountSerializationWorkflowInstance(
        {
          workflowSetupData: []
        }
      )(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...state
            }
          } as RootState),
        {
          workflowService: {
            createWorkflowInstance: jest.fn().mockResolvedValue({
              data: { workflowInstanceDetails: { id: '1111' } }
            }),
            updateWorkflowInstanceData: jest
              .fn()
              .mockResolvedValue({ data: { workflowSetupData: [] } })
          }
        }
      );
    expect(nextState.payload.workflowInstanceId).toEqual('1111');
  });
});
