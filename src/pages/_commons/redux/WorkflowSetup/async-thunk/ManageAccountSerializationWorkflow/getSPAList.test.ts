import store from 'app/_libraries/_dof/core/redux/createAppStore';
import { actionsWorkflowSetup } from '../..';

describe('redux-store > workflow > ManageAccountSerializationWorkflow >  getSPAListWorkflow', async () => {
  it('thunk action > with existing changeId ', async () => {
    const result = {
      data: { workflowInstanceDetails: { id: '1111' } }
    };
    const state = {
      workflowSetup: { workflow: { id: undefined } },
      workflowFormSteps: {
        forms: {
          changeInformation: { workflowInstanceId: 'workflowInstanceId' },
          uploadFile: {},
          configureParameters: {}
        }
      }
    };
    const nextState: any = await actionsWorkflowSetup.getSPAListWorkflow({
      dataForm: { sys: 'sys', prin: 'prin', agent: 'agent' }
    })(
      store.dispatch,
      () => ({ ...store.getState(), workflowSetup: { ...state } } as RootState),
      {
        workflowService: {
          getWorkflowSPAList: jest.fn().mockResolvedValue(result)
        }
      }
    );
    expect(nextState.payload).toEqual(result);
  });
});
