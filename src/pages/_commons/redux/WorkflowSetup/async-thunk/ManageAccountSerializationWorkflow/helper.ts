import { MethodFieldParameterEnum } from 'app/constants/enums';

export const parseMethodFieldParameter = (parameter: any) => {
  let newValue = parameter.newValue;
  switch (parameter.name) {
    case MethodFieldParameterEnum.ComputerLetter:
      newValue = parameter.newValue || '0000';
      break;
    case MethodFieldParameterEnum.NumberOfDays:
      newValue = (parameter.newValue || 0).toString().padStart(2, '0');
  }
  return { ...parameter, newValue: newValue };
};

export const parseMethodData = (...methodForm: any[]) => {
  let methodList: any[] = [];
  methodForm?.forEach(form => {
    if (form?.methods?.length > 0) {
      methodList = methodList.concat(
        form.methods?.map((m: any) => {
          return {
            id: '',
            name: m.name,
            modeledFrom: m.modeledFrom || null,
            comment: m.comment || '',
            serviceSubjectSection: m.serviceSubjectSection,
            parameters: m.parameters.map((p: any) =>
              parseMethodFieldParameter(p)
            )
          };
        })
      );
    }
  });
  return methodList;
};
