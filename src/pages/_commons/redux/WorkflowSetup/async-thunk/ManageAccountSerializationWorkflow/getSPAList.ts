import { createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import get from 'lodash.get';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';

export interface getSPAListArgs {
  dataForm: { sys: string; prin: string; agent: string };
}

export const getSPAListWorkflow = createAsyncThunk<
  any,
  getSPAListArgs,
  ThunkAPIConfig
>('workflowSetup/getSPAList', async (args, thunkAPI) => {
  const { workflowService } = get(thunkAPI, 'extra') as APIMapping;
  const { dataForm } = args;

  const state = thunkAPI.getState();

  const changeForm = state.workflowSetup?.workflowFormSteps.forms[
    'changeInformation'
  ] as FormChangeInfo;

  const response = await workflowService.getWorkflowSPAList({
    searchFields: {
      workflowInstanceId: changeForm.workflowInstanceId?.toString(),
      dataForm
    }
  });

  return response;
});
