import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  elementMetadata: {
    loading: false,
    error: false,
    elements: []
  }
};

describe('redux-store > workflow-setup > getWorkflowMetadata', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getWorkflowMetadata.pending(
        'createWorkflowInstance',
        ['minimum.pay.due.option']
      )
    );
    expect(nextState.elementMetadata.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getWorkflowMetadata.fulfilled(
        {},
        'createWorkflowInstance',
        ['minimum.pay.due.option']
      )
    );
    expect(nextState.elementMetadata.loading).toEqual(false);
    expect(nextState.elementMetadata.error).toEqual(false);
  });
  it('fulfilled > empty element', () => {
    const cloneReducer = { ...initReducer };
    cloneReducer.elementMetadata.elements = undefined;
    const nextState = reducer(
      cloneReducer,
      actionsWorkflowSetup.getWorkflowMetadata.fulfilled(
        {},
        'createWorkflowInstance',
        ['minimum.pay.due.option']
      )
    );
    expect(nextState.elementMetadata.loading).toEqual(false);
    expect(nextState.elementMetadata.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getWorkflowMetadata.rejected(
        new Error('mock error'),
        'createWorkflowInstance',
        ['minimum.pay.due.option']
      )
    );
    expect(nextState.elementMetadata.loading).toEqual(false);
    expect(nextState.elementMetadata.error).toEqual(true);
  });

  it('thunk action > metadata element is undefined', async () => {
    const nextState: any = await actionsWorkflowSetup.getWorkflowMetadata([
      'minimum.pay.due.option'
    ])(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          workflowSetup: {
            ...initReducer
          }
        } as RootState),
      {
        workflowService: {
          getElementDataByName: jest.fn().mockResolvedValue({
            data: { elementMetaData: undefined }
          })
        }
      }
    );
    expect(nextState.payload.elements.length).toEqual(0);
  });

  it('thunk action > metadata element already have value', async () => {
    const state: any = {
      workflowSetup: {
        workflow: {
          id: undefined
        }
      },
      elementMetadata: {
        loading: false,
        error: true,
        elements: ['mock element']
      }
    };

    const nextState: any = await actionsWorkflowSetup.getWorkflowMetadata([
      'minimum.pay.due.option'
    ])(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          workflowSetup: {
            ...state
          }
        } as RootState),
      {}
    );
    expect(nextState.payload.elements.length).toEqual(0);
  });
});
