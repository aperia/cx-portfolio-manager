import { TableFieldParameterEnum } from 'app/constants/enums';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import { ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME } from 'pages/WorkflowActionCardCompromiseEvent/ActionCardCompromiseEventSelectActionsStep/helper';
import { ActionsValues } from 'pages/WorkflowManageAccountDelinquency/ConfigureParametersStep';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';
import * as SelectHookActionCardCompromiseEvent from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowActionCardCompromiseEvent';
import * as SelectHookCommon from 'pages/_commons/redux/WorkflowSetup/select-hooks/_common';

const initReducer: any = {
  workflowSetup: {}
};

const useSelectElementMetadataForActionCardCompromiseEvent = jest.spyOn(
  SelectHookActionCardCompromiseEvent,
  'elementMetadataForActionCardCompromiseEvent'
);
const selectWorkflowInstanceInfo = jest.spyOn(
  SelectHookCommon,
  'selectWorkflowInstanceInfo'
);

const actionCardElementData = {
  singleEntityYesTrans: [{ code: 'name', description: 'type' }],
  singleEntityNoTrans: [{ code: 'name', description: 'type' }],
  singleEntityReportLost: [{ code: 'name', description: 'type' }],
  singleEntityReportStolen: [{ code: 'name', description: 'type' }],
  separateEntityYesTrans: [{ code: 'name', description: 'type' }],
  separateEntityNoTrans: [{ code: 'name', description: 'type' }],
  separateEntityReportLost: [{ code: 'name', description: 'type' }],
  separateEntityReportStolen: [{ code: 'name', description: 'type' }],
  visaResponseCode: [{ code: 'name', description: 'type' }]
} as any;

describe('redux-store > workflow-setup > downloadTemplateFile', () => {
  beforeEach(() => {
    selectWorkflowInstanceInfo.mockReturnValue({} as any);
  });

  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.downloadTemplateFile.pending(
        'downloadTemplateFile',
        { downloadTemplateType: '' }
      )
    );
    expect(nextState.workflowSetup.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.downloadTemplateFile.fulfilled(
        true,
        'downloadTemplateFile',
        { downloadTemplateType: '' }
      )
    );
    expect(nextState.workflowSetup.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.downloadTemplateFile.rejected(
        new Error('mock error'),
        'downloadTemplateFile',
        { downloadTemplateType: '' }
      )
    );
    expect(nextState.workflowSetup.loading).toEqual(false);
  });

  it('thunk action > download when downloadTemplateType = "manageTemporaryCreditLimitWorkflow"', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'manageTemporaryCreditLimitWorkflow'
    })(store.dispatch, store.getState, {
      workflowTemplateService: {
        downloadWorkflowTemplate: jest
          .fn()
          .mockResolvedValue({ data: { attachment: {} } })
      }
    });
    expect(nextState.payload).toEqual({});
  });

  describe('thunk action > download when downloadTemplateType = "accountDelinquency"', () => {
    it('action = ActionsValues.NM180Workout', async () => {
      const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
        downloadTemplateType: 'accountDelinquency'
      })(
        store.dispatch,
        () =>
          ({
            workflowSetup: {
              workflowFormSteps: {
                forms: {
                  configureParameters: {
                    action: ActionsValues.NM180Workout,
                    parameters: [{ name: 'test', value: '123' }]
                  }
                }
              }
            }
          } as any),
        {
          workflowTemplateService: {
            downloadWorkflowTemplate: jest
              .fn()
              .mockResolvedValue({ data: { attachment: {} } })
          }
        }
      );
      expect(nextState.payload).toEqual({});
    });
    it('action = ActionsValues.NM180Manual', async () => {
      const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
        downloadTemplateType: 'accountDelinquency'
      })(
        store.dispatch,
        () =>
          ({
            workflowSetup: {
              workflowFormSteps: {
                forms: {
                  configureParameters: {
                    action: ActionsValues.NM180Manual,
                    parameters: [
                      { name: 'nm.180.subtransaction.code', value: '123' }
                    ]
                  }
                }
              }
            }
          } as any),
        {
          workflowTemplateService: {
            downloadWorkflowTemplate: jest
              .fn()
              .mockResolvedValue({ data: { attachment: {} } })
          }
        }
      );
      expect(nextState.payload).toEqual({});
    });
    it('action = other', async () => {
      const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
        downloadTemplateType: 'accountDelinquency'
      })(
        store.dispatch,
        () =>
          ({
            workflowSetup: {
              workflowFormSteps: {
                forms: {
                  configureParameters: {
                    action: ''
                  }
                }
              }
            }
          } as any),
        {
          workflowTemplateService: {
            downloadWorkflowTemplate: jest
              .fn()
              .mockResolvedValue({ data: { attachment: {} } })
          }
        }
      );
      expect(nextState.payload).toEqual({});
    });
  });

  it('thunk action > download when downloadTemplateType = "forceEmbossReplacementCardsWorkflow"', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'forceEmbossReplacementCardsWorkflow'
    })(store.dispatch, store.getState, {
      workflowTemplateService: {
        downloadWorkflowTemplate: jest
          .fn()
          .mockResolvedValue({ data: { attachment: {} } })
      }
    });
    expect(nextState.payload).toEqual({});
  });

  it('thunk action > download when downloadTemplateType = "conditionMCycleAccountsForTestingWorkflow"', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'conditionMCycleAccountsForTestingWorkflow'
    })(store.dispatch, store.getState, {
      workflowTemplateService: {
        downloadWorkflowTemplate: jest
          .fn()
          .mockResolvedValue({ data: { attachment: {} } })
      }
    });
    expect(nextState.payload).toEqual({});
  });

  it('thunk action > download when downloadTemplateType = "bulkSuspendFraudStrategyWorkflow"', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'bulkSuspendFraudStrategyWorkflow'
    })(store.dispatch, store.getState, {
      workflowTemplateService: {
        downloadWorkflowTemplate: jest
          .fn()
          .mockResolvedValue({ data: { attachment: {} } })
      }
    });
    expect(nextState.payload).toEqual({});
  });

  it('thunk action > download when downloadTemplateType = "manageAccountMemosWorkflow"', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'manageAccountMemosWorkflow'
    })(store.dispatch, store.getState, {
      workflowTemplateService: {
        downloadWorkflowTemplate: jest
          .fn()
          .mockResolvedValue({ data: { attachment: {} } })
      }
    });
    expect(nextState.payload).toEqual({});
  });

  it('thunk action > download when downloadTemplateType = "actionCardCompromiseEventWorkflow"', async () => {
    useSelectElementMetadataForActionCardCompromiseEvent.mockReturnValue(
      actionCardElementData
    );
    let nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'actionCardCompromiseEventWorkflow'
    })(
      store.dispatch,
      () =>
        ({
          workflowSetup: {
            workflowFormSteps: {
              forms: {
                selectActions: {
                  action: {
                    radio:
                      ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SINGLE_ENTITY,
                    question3: 'Yes'
                  }
                }
              }
            }
          }
        } as any),
      {
        workflowTemplateService: {
          downloadWorkflowTemplate: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );

    nextState = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'actionCardCompromiseEventWorkflow'
    })(
      store.dispatch,
      () =>
        ({
          workflowSetup: {
            workflowFormSteps: {
              forms: {
                selectActions: {
                  action: {
                    radio:
                      ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SINGLE_ENTITY,
                    question3: 'No'
                  }
                }
              }
            }
          }
        } as any),
      {
        workflowTemplateService: {
          downloadWorkflowTemplate: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );

    nextState = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'actionCardCompromiseEventWorkflow'
    })(
      store.dispatch,
      () =>
        ({
          workflowSetup: {
            workflowFormSteps: {
              forms: {
                selectActions: {
                  action: {
                    radio:
                      ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SINGLE_ENTITY,
                    question2:
                      'Report Lost Accounts using the SL, Lost/Stolen Report transaction.'
                  }
                }
              }
            }
          }
        } as any),
      {
        workflowTemplateService: {
          downloadWorkflowTemplate: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );

    nextState = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'actionCardCompromiseEventWorkflow'
    })(
      store.dispatch,
      () =>
        ({
          workflowSetup: {
            workflowFormSteps: {
              forms: {
                selectActions: {
                  action: {
                    radio:
                      ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SINGLE_ENTITY,
                    question2:
                      'Report Stolen Accounts using the SL, Lost/Stolen Report transaction.'
                  }
                }
              }
            }
          }
        } as any),
      {
        workflowTemplateService: {
          downloadWorkflowTemplate: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );

    nextState = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'actionCardCompromiseEventWorkflow'
    })(
      store.dispatch,
      () =>
        ({
          workflowSetup: {
            workflowFormSteps: {
              forms: {
                selectActions: {
                  action: {
                    radio:
                      ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SINGLE_ENTITY,
                    question2: ''
                  }
                }
              }
            }
          }
        } as any),
      {
        workflowTemplateService: {
          downloadWorkflowTemplate: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );

    nextState = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'actionCardCompromiseEventWorkflow'
    })(
      store.dispatch,
      () =>
        ({
          workflowSetup: {
            workflowFormSteps: {
              forms: {
                selectActions: {
                  action: {
                    radio:
                      ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SEPARATE_ENTITY,
                    question3: 'Yes'
                  }
                }
              }
            }
          }
        } as any),
      {
        workflowTemplateService: {
          downloadWorkflowTemplate: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );

    nextState = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'actionCardCompromiseEventWorkflow'
    })(
      store.dispatch,
      () =>
        ({
          workflowSetup: {
            workflowFormSteps: {
              forms: {
                selectActions: {
                  action: {
                    radio:
                      ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SEPARATE_ENTITY,
                    question3: 'No'
                  }
                }
              }
            }
          }
        } as any),
      {
        workflowTemplateService: {
          downloadWorkflowTemplate: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );

    nextState = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'actionCardCompromiseEventWorkflow'
    })(
      store.dispatch,
      () =>
        ({
          workflowSetup: {
            workflowFormSteps: {
              forms: {
                selectActions: {
                  action: {
                    radio:
                      ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SEPARATE_ENTITY,
                    question2:
                      'Report Lost PIIDs using the SLP, Lost/Stolen Report transaction.'
                  }
                }
              }
            }
          }
        } as any),
      {
        workflowTemplateService: {
          downloadWorkflowTemplate: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );

    nextState = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'actionCardCompromiseEventWorkflow'
    })(
      store.dispatch,
      () =>
        ({
          workflowSetup: {
            workflowFormSteps: {
              forms: {
                selectActions: {
                  action: {
                    radio:
                      ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SEPARATE_ENTITY,
                    question2:
                      'Report Stolen PIIDs using the SLP, Lost/Stolen Report transaction.'
                  }
                }
              }
            }
          }
        } as any),
      {
        workflowTemplateService: {
          downloadWorkflowTemplate: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );

    nextState = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'actionCardCompromiseEventWorkflow'
    })(
      store.dispatch,
      () =>
        ({
          workflowSetup: {
            workflowFormSteps: {
              forms: {
                selectActions: {
                  action: {
                    radio:
                      ACTION_CARD_COMPROMISE_EVENT_RADIO_NAME.SEPARATE_ENTITY,
                    question2: ''
                  }
                }
              }
            }
          }
        } as any),
      {
        workflowTemplateService: {
          downloadWorkflowTemplate: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );

    nextState = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'actionCardCompromiseEventWorkflow'
    })(
      store.dispatch,
      () =>
        ({
          workflowSetup: {
            workflowFormSteps: {
              forms: {
                selectActions: {
                  action: {
                    radio: '',
                    question2: ''
                  }
                }
              }
            }
          }
        } as any),
      {
        workflowTemplateService: {
          downloadWorkflowTemplate: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );

    expect(nextState.payload).toEqual({});
  });

  it('thunk action > download when downloadTemplateType = "manageOCSShells"', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'manageOCSShells'
    })(store.dispatch, store.getState, {
      workflowTemplateService: {
        downloadWorkflowTemplate: jest
          .fn()
          .mockResolvedValue({ data: { attachment: {} } })
      }
    });
    expect(nextState.payload).toEqual({});
  });

  it('thunk action > download when downloadTemplateType = "bulkExternalStatusManagementWorkflow"', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'bulkExternalStatusManagementWorkflow'
    })(store.dispatch, store.getState, {
      workflowTemplateService: {
        downloadWorkflowTemplate: jest
          .fn()
          .mockResolvedValue({ data: { attachment: {} } })
      }
    });
    expect(nextState.payload).toEqual({});
  });

  it('thunk action > download when downloadTemplateType = "manageAccountLevelWorkflow"', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'manageAccountLevelWorkflow'
    })(store.dispatch, store.getState, {
      workflowTemplateService: {
        downloadWorkflowTemplate: jest
          .fn()
          .mockResolvedValue({ data: { attachment: {} } })
      }
    });
    expect(nextState.payload).toEqual({});
  });

  it('thunk action > download when downloadTemplateType = "cycleAccountsTestEnvironmentWorkflow"', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'cycleAccountsTestEnvironmentWorkflow'
    })(store.dispatch, store.getState, {
      workflowTemplateService: {
        downloadWorkflowTemplate: jest
          .fn()
          .mockResolvedValue({ data: { attachment: {} } })
      }
    });
    expect(nextState.payload).toEqual({});
  });

  it('thunk action > download when downloadTemplateType = "manageTransactionLevelProcessingDecisionTablesTLPWorkflow"', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType:
        'manageTransactionLevelProcessingDecisionTablesTLPWorkflow'
    })(store.dispatch, store.getState, {
      workflowTemplateService: {
        downloadWorkflowTemplate: jest
          .fn()
          .mockResolvedValue({ data: { attachment: {} } })
      }
    });
    expect(nextState.payload).toEqual({});
  });

  it('thunk action > download when downloadTemplateType = "manageMethodLevelProcessingDecisionTablesMLPWorkflow"', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType:
        'manageMethodLevelProcessingDecisionTablesMLPWorkflow'
    })(store.dispatch, store.getState, {
      workflowTemplateService: {
        downloadWorkflowTemplate: jest
          .fn()
          .mockResolvedValue({ data: { attachment: {} } })
      }
    });
    expect(nextState.payload).toEqual({});
  });

  it('thunk action > download when downloadTemplateType = "manualAccountDeletionWorkflow"', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'manualAccountDeletionWorkflow'
    })(store.dispatch, store.getState, {
      workflowTemplateService: {
        downloadWorkflowTemplate: jest
          .fn()
          .mockResolvedValue({ data: { attachment: {} } })
      }
    });
    expect(nextState.payload).toEqual({});
  });

  it('thunk action > download when downloadTemplateType = "forceEmbossReplacementCardsWorkflow" with transaction === \'PID194\'', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'forceEmbossReplacementCardsWorkflow'
    })(
      store.dispatch,
      () =>
        ({
          workflowSetup: {
            workflowFormSteps: {
              forms: {
                configureParameters: {
                  config: {
                    parameters: {
                      value: 'pid.194',
                      action: { param: { standardCode: { code: 'code' } } }
                    }
                  }
                }
              }
            }
          }
        } as any),
      {
        workflowTemplateService: {
          downloadWorkflowTemplate: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );
    expect(nextState.payload).toEqual({});
  });

  it('thunk action > download when downloadTemplateType = "forceEmbossReplacementCardsWorkflow" with transactionId === \'PID194_NM65\'', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'forceEmbossReplacementCardsWorkflow'
    })(
      store.dispatch,
      () =>
        ({
          workflowSetup: {
            workflowFormSteps: {
              forms: {
                configureParameters: {
                  config: {
                    parameters: {
                      value: 'pid.194',
                      action: {
                        param: {
                          standardCode: { code: 'code' },
                          isNM65MailCode: true
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        } as any),
      {
        workflowTemplateService: {
          downloadWorkflowTemplate: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );
    expect(nextState.payload).toEqual({});
  });

  it('thunk action > download when downloadTemplateType = "sendLetterToAccountsWorkflow"', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'sendLetterToAccountsWorkflow'
    })(store.dispatch, store.getState, {
      workflowTemplateService: {
        downloadWorkflowTemplate: jest
          .fn()
          .mockResolvedValue({ data: { attachment: {} } })
      }
    });
    expect(nextState.payload).toEqual({});
  });
  it('thunk action > download when downloadTemplateType = "flexibleNonMonetaryTransactionsWorkflow"', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'flexibleNonMonetaryTransactionsWorkflow'
    })(store.dispatch, store.getState, {
      workflowTemplateService: {
        downloadWorkflowTemplate: jest
          .fn()
          .mockResolvedValue({ data: { attachment: {} } })
      }
    });
    expect(nextState.payload).toEqual({});
  });

  it('thunk action > download flexibleMonetaryTransactionsWorkflow with workflowSetup', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'flexibleMonetaryTransactionsWorkflow'
    })(
      store.dispatch,
      () =>
        ({
          workflowSetup: {
            workflowFormSteps: {
              forms: {
                selectMonetaryTransactions: {
                  transactions: [{ code: 'code', text: 'text' }]
                }
              }
            }
          }
        } as any),
      {
        workflowTemplateService: {
          downloadWorkflowTemplate: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );
    expect(nextState.payload).toEqual({});
  });

  it('thunk action > download sendLetterToAccountsWorkflow STANDARD', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'sendLetterToAccountsWorkflow'
    })(
      store.dispatch,
      () =>
        ({
          workflowSetup: {
            workflowFormSteps: {
              forms: {
                configureParameters: {
                  configureParameters: { addressingType: '0' }
                }
              }
            }
          }
        } as any),
      {
        workflowTemplateService: {
          downloadWorkflowTemplate: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );
    expect(nextState.payload).toEqual({});
  });

  it('thunk action > download when downloadTemplateType = "haltInterestChargesAndFees"', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'haltInterestChargesAndFees'
    })(store.dispatch, store.getState, {
      workflowTemplateService: {
        downloadWorkflowTemplate: jest
          .fn()
          .mockResolvedValue({ data: { attachment: {} } })
      }
    });
    expect(nextState.payload).toEqual({});
  });

  it('thunk action > download when downloadTemplateType = "assignPricingStrategiesWorkflow"', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'assignPricingStrategiesWorkflow'
    })(store.dispatch, store.getState, {
      workflowTemplateService: {
        downloadWorkflowTemplate: jest
          .fn()
          .mockResolvedValue({ data: { attachment: {} } })
      }
    });
    expect(nextState.payload).toEqual({});
  });

  it('thunk action > download when downloadTemplateType = "flexibleMonetaryTransactionsWorkflow"', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'flexibleMonetaryTransactionsWorkflow'
    })(store.dispatch, store.getState, {
      workflowTemplateService: {
        downloadWorkflowTemplate: jest
          .fn()
          .mockResolvedValue({ data: { attachment: {} } })
      }
    });
    expect(nextState.payload).toEqual(undefined);
  });

  it('thunk action > download when downloadTemplateType = "monetaryAdjustmentTransactionsWorkflow"', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'monetaryAdjustmentTransactionsWorkflow'
    })(store.dispatch, store.getState, {
      workflowTemplateService: {
        downloadWorkflowTemplate: jest
          .fn()
          .mockResolvedValue({ data: { attachment: {} } })
      }
    });
    expect(nextState.payload).toEqual({});
  });

  it('thunk action > download when downloadTemplateType = "manageAccountTransferWorkflow"', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'manageAccountTransferWorkflow'
    })(store.dispatch, store.getState, {
      workflowTemplateService: {
        downloadWorkflowTemplate: jest
          .fn()
          .mockResolvedValue({ data: { attachment: {} } })
      }
    });
    expect(nextState.payload).toEqual({});
  });

  it('thunk action > download when downloadTemplateType = "others"', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'others'
    })(store.dispatch, store.getState, {
      workflowTemplateService: {
        downloadWorkflowTemplate: jest
          .fn()
          .mockResolvedValue({ data: { attachment: {} } })
      }
    });
    expect(nextState.payload).toEqual({});
  });
  it('thunk action > download when downloadTemplateType = "manageAccountLevelWorkflowAQ"', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'manageAccountLevelWorkflowAQ'
    })(
      store.dispatch,
      () =>
        ({
          workflowSetup: {
            decisionElementList: {
              ALPAQ: {
                decisionElementList: [{ configurable: 'configurable' }]
              }
            },
            workflowFormSteps: {
              forms: {
                configAlpAq: {
                  tables: [
                    {
                      elementList: [{ name: 'name', searchCode: 'searchCode' }]
                    }
                  ]
                }
              }
            }
          }
        } as any),
      {
        workflowTemplateService: {
          downloadWorkflowTemplate: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );
    expect(nextState.payload).toEqual({});
  });
  it('thunk action > download when downloadTemplateType = "manageAccountLevelWorkflowAQ" with empty data', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'manageAccountLevelWorkflowAQ'
    })(
      store.dispatch,
      () =>
        ({
          workflowSetup: {
            decisionElementList: {
              ALPAQ: {
                decisionElementList: undefined
              }
            },
            workflowFormSteps: {
              forms: {
                configAlpAq: {
                  tables: undefined
                }
              }
            }
          }
        } as any),
      {
        workflowTemplateService: {
          downloadWorkflowTemplate: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );
    expect(nextState.payload).toEqual({});
  });
  it('thunk action > download when downloadTemplateType = "manageAccountLevelWorkflowCA"', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'manageAccountLevelWorkflowCA'
    })(
      store.dispatch,
      () =>
        ({
          workflowSetup: {
            workflowFormSteps: {
              forms: {
                configAlpCa: {
                  decisionList: [
                    {
                      configurable: 'configurable',
                      name: 'name',
                      searchCode: 'searchCode',
                      immediateAllocation: ''
                    },
                    {
                      name: 'name',
                      searchCode: 'searchCode',
                      immediateAllocation: ''
                    }
                  ],
                  tables: {
                    tableId: 'tableId',
                    elementList: [
                      {
                        name: 'name',
                        searchCode: 'searchCode',
                        immediateAllocation: 'Yes'
                      }
                    ]
                  }
                }
              }
            }
          }
        } as any),
      {
        workflowTemplateService: {
          downloadWorkflowTemplate: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );
    expect(nextState.payload).toEqual({});
  });
  it('thunk action > download when downloadTemplateType = "manageAccountLevelWorkflowCA" with diff data', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType: 'manageAccountLevelWorkflowCA'
    })(
      store.dispatch,
      () =>
        ({
          workflowSetup: {
            workflowFormSteps: {
              forms: {
                configAlpCa: {
                  decisionList: [
                    {
                      configurable: 'configurable',
                      name: 'name',
                      searchCode: 'searchCode',
                      immediateAllocation: ''
                    },
                    {
                      name: 'name',
                      searchCode: 'searchCode',
                      immediateAllocation: ''
                    }
                  ],
                  tables: {
                    tableId: 'tableId',
                    elementList: [
                      {
                        name: 'name',
                        searchCode: 'searchCode',
                        immediateAllocation: 'immediateAllocation'
                      }
                    ]
                  }
                }
              }
            }
          }
        } as any),
      {
        workflowTemplateService: {
          downloadWorkflowTemplate: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );
    expect(nextState.payload).toEqual({});
  });
  it('thunk action > download when downloadTemplateType = "manageTransactionLevelProcessingDecisionTablesTLPWorkflowAQ"', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType:
        'manageTransactionLevelProcessingDecisionTablesTLPWorkflowAQ'
    })(
      store.dispatch,
      () =>
        ({
          workflowSetup: {
            decisionElementList: {
              TLPAQ: {
                decisionElementList: [{ configurable: 'configurable' }]
              }
            },
            workflowFormSteps: {
              forms: {
                configTlpAq: {
                  tables: [
                    {
                      elementList: [{ name: 'name', searchCode: 'searchCode' }]
                    }
                  ]
                }
              }
            }
          }
        } as any),
      {
        workflowTemplateService: {
          downloadWorkflowTemplate: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );
    expect(nextState.payload).toEqual({});
  });
  it('thunk action > download when downloadTemplateType = "manageTransactionLevelProcessingDecisionTablesTLPWorkflowAQ" with empty', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType:
        'manageTransactionLevelProcessingDecisionTablesTLPWorkflowAQ'
    })(
      store.dispatch,
      () =>
        ({
          workflowSetup: {
            decisionElementList: {
              TLPAQ: {
                decisionElementList: undefined
              }
            },
            workflowFormSteps: {
              forms: {
                configTlpAq: {
                  tables: undefined
                }
              }
            }
          }
        } as any),
      {
        workflowTemplateService: {
          downloadWorkflowTemplate: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );
    expect(nextState.payload).toEqual({});
  });
  it('thunk action > download when downloadTemplateType = "manageTransactionLevelProcessingDecisionTablesTLPWorkflowTQ"', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType:
        'manageTransactionLevelProcessingDecisionTablesTLPWorkflowTQ'
    })(
      store.dispatch,
      () =>
        ({
          workflowSetup: {
            decisionElementList: {
              TLPTQ: {
                decisionElementList: [{ configurable: 'configurable' }]
              }
            },
            workflowFormSteps: {
              forms: {
                configTlpTq: {
                  tables: [
                    {
                      elementList: [{ name: 'name', searchCode: 'searchCode' }]
                    }
                  ]
                }
              }
            }
          }
        } as any),
      {
        workflowTemplateService: {
          downloadWorkflowTemplate: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );
    expect(nextState.payload).toEqual({});
  });
  it('thunk action > download when downloadTemplateType = "manageTransactionLevelProcessingDecisionTablesTLPWorkflowTQ" with empty data', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType:
        'manageTransactionLevelProcessingDecisionTablesTLPWorkflowTQ'
    })(
      store.dispatch,
      () =>
        ({
          workflowSetup: {
            decisionElementList: {
              TLPTQ: {
                decisionElementList: undefined
              }
            },
            workflowFormSteps: {
              forms: {
                configTlpTq: {
                  tables: undefined
                }
              }
            }
          }
        } as any),
      {
        workflowTemplateService: {
          downloadWorkflowTemplate: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );
    expect(nextState.payload).toEqual({});
  });
  it('thunk action > download when downloadTemplateType = "manageTransactionLevelProcessingDecisionTablesTLPWorkflowRQ"', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType:
        'manageTransactionLevelProcessingDecisionTablesTLPWorkflowRQ'
    })(
      store.dispatch,
      () =>
        ({
          workflowSetup: {
            decisionElementList: {
              TLPRQ: {
                decisionElementList: [{ configurable: 'configurable' }]
              }
            },
            workflowFormSteps: {
              forms: {
                configTlpRq: {
                  tables: [
                    {
                      elementList: [{ name: 'name', searchCode: 'searchCode' }]
                    }
                  ]
                }
              }
            }
          }
        } as any),
      {
        workflowTemplateService: {
          downloadWorkflowTemplate: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );
    expect(nextState.payload).toEqual({});
  });
  it('thunk action > download when downloadTemplateType = "manageTransactionLevelProcessingDecisionTablesTLPWorkflowRQ" with empty data', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType:
        'manageTransactionLevelProcessingDecisionTablesTLPWorkflowRQ'
    })(
      store.dispatch,
      () =>
        ({
          workflowSetup: {
            decisionElementList: {
              TLPRQ: {
                decisionElementList: undefined
              }
            },
            workflowFormSteps: {
              forms: {
                configTlpRq: {
                  tables: undefined
                }
              }
            }
          }
        } as any),
      {
        workflowTemplateService: {
          downloadWorkflowTemplate: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );
    expect(nextState.payload).toEqual({});
  });
  it('thunk action > download when downloadTemplateType = "manageTransactionLevelProcessingDecisionTablesTLPWorkflowCA"', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType:
        'manageTransactionLevelProcessingDecisionTablesTLPWorkflowCA'
    })(
      store.dispatch,
      () =>
        ({
          workflowSetup: {
            decisionElementList: {
              TLPCA: {
                decisionElementList: [{ configurable: 'configurable' }]
              }
            },
            workflowFormSteps: {
              forms: {
                configTlpCa: {
                  tables: [
                    {
                      elementList: [{ name: 'name', searchCode: 'searchCode' }]
                    }
                  ]
                }
              }
            }
          }
        } as any),
      {
        workflowTemplateService: {
          downloadWorkflowTemplate: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );
    expect(nextState.payload).toEqual({});
  });
  it('thunk action > download when downloadTemplateType = "manageTransactionLevelProcessingDecisionTablesTLPWorkflowCA" with empty data', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType:
        'manageTransactionLevelProcessingDecisionTablesTLPWorkflowCA'
    })(
      store.dispatch,
      () =>
        ({
          workflowSetup: {
            decisionElementList: {
              TLPCA: {
                decisionElementList: undefined
              }
            },
            workflowFormSteps: {
              forms: {
                configTlpCa: {
                  tables: undefined
                }
              }
            }
          }
        } as any),
      {
        workflowTemplateService: {
          downloadWorkflowTemplate: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );
    expect(nextState.payload).toEqual({});
  });
  it('thunk action > download when downloadTemplateType = "manageMethodLevelProcessingDecisionTablesMLPWorkflowST"', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType:
        'manageMethodLevelProcessingDecisionTablesMLPWorkflowST'
    })(
      store.dispatch,
      () =>
        ({
          workflowSetup: {
            decisionElementList: {
              ALPAQ: {
                decisionElementList: [{ configurable: 'configurable' }]
              }
            },
            workflowFormSteps: {
              forms: {
                configMlpSt: {
                  tables: [
                    {
                      elementList: [
                        {
                          name: 'name',
                          searchCode: 'searchCode',
                          immediateAllocation: 'immediateAllocation'
                        }
                      ]
                    }
                  ]
                }
              }
            }
          }
        } as any),
      {
        workflowTemplateService: {
          downloadWorkflowTemplate: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );
    expect(nextState.payload).toEqual({});
  });
  it('thunk action > download when downloadTemplateType = "manageMethodLevelProcessingDecisionTablesMLPWorkflowST" with diff immediateAllocation', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType:
        'manageMethodLevelProcessingDecisionTablesMLPWorkflowST'
    })(
      store.dispatch,
      () =>
        ({
          workflowSetup: {
            decisionElementList: {
              ALPAQ: {
                decisionElementList: [{ configurable: 'configurable' }]
              }
            },
            workflowFormSteps: {
              forms: {
                configMlpSt: {
                  tables: [
                    {
                      elementList: [
                        {
                          name: 'name',
                          searchCode: 'searchCode',
                          immediateAllocation: 'Yes'
                        }
                      ]
                    }
                  ]
                }
              }
            }
          }
        } as any),
      {
        workflowTemplateService: {
          downloadWorkflowTemplate: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );
    expect(nextState.payload).toEqual({});
  });
  it('thunk action > download when downloadTemplateType = "manageMethodLevelProcessingDecisionTablesMLPWorkflowST" with diff immediateAllocation', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType:
        'manageMethodLevelProcessingDecisionTablesMLPWorkflowST'
    })(
      store.dispatch,
      () =>
        ({
          workflowSetup: {
            decisionElementList: {
              ALPAQ: {
                decisionElementList: [{ configurable: 'configurable' }]
              }
            },
            workflowFormSteps: {
              forms: {
                configMlpSt: {
                  tables: [
                    {
                      elementList: [
                        {
                          name: 'name',
                          searchCode: 'searchCode',
                          immediateAllocation: 'No'
                        }
                      ]
                    }
                  ]
                }
              }
            }
          }
        } as any),
      {
        workflowTemplateService: {
          downloadWorkflowTemplate: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );
    expect(nextState.payload).toEqual({});
  });
  it('thunk action > download when downloadTemplateType = "manageMethodLevelProcessingDecisionTablesMLPWorkflowAQ"', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType:
        'manageMethodLevelProcessingDecisionTablesMLPWorkflowAQ'
    })(
      store.dispatch,
      () =>
        ({
          workflowSetup: {
            decisionElementList: {
              MLPAQ: {
                decisionElementList: [
                  {
                    configurable: 'configurable',
                    tableControlParameters: {
                      [TableFieldParameterEnum.ChangeInTermsMethodFlag]: 'Y'
                    }
                  }
                ]
              }
            },
            workflowFormSteps: {
              forms: {
                configMlpAq: {
                  tables: [
                    {
                      elementList: [
                        {
                          name: 'name',
                          searchCode: 'searchCode',
                          immediateAllocation: 'Yes'
                        }
                      ]
                    }
                  ]
                }
              }
            }
          }
        } as any),
      {
        workflowTemplateService: {
          downloadWorkflowTemplate: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );
    expect(nextState.payload).toEqual({});
  });
  it('thunk action > download when downloadTemplateType = "manageMethodLevelProcessingDecisionTablesMLPWorkflowAQ" with diff immediateAllocation', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType:
        'manageMethodLevelProcessingDecisionTablesMLPWorkflowAQ'
    })(
      store.dispatch,
      () =>
        ({
          workflowSetup: {
            decisionElementList: {
              MLPAQ: {
                decisionElementList: [
                  {
                    configurable: false,
                    name: 'CIT'
                  }
                ]
              }
            },
            workflowFormSteps: {
              forms: {
                configMlpAq: {
                  tables: [
                    {
                      elementList: [
                        {
                          name: 'name',
                          searchCode: 'searchCode',
                          immediateAllocation: 'No'
                        }
                      ],
                      tableControlParameters: {
                        [TableFieldParameterEnum.ChangeInTermsMethodFlag]: 'N'
                      }
                    }
                  ]
                }
              }
            }
          }
        } as any),
      {
        workflowTemplateService: {
          downloadWorkflowTemplate: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );
    expect(nextState.payload).toEqual({});
  });
  it('thunk action > download when downloadTemplateType = "manageMethodLevelProcessingDecisionTablesMLPWorkflowAQ" with diff immediateAllocation', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType:
        'manageMethodLevelProcessingDecisionTablesMLPWorkflowAQ'
    })(
      store.dispatch,
      () =>
        ({
          workflowSetup: {
            decisionElementList: {
              MLPAQ: {
                decisionElementList: [
                  {
                    configurable: 'configurable'
                  }
                ]
              }
            },
            workflowFormSteps: {
              forms: {
                configMlpAq: {
                  tables: [
                    {
                      elementList: [
                        {
                          name: 'name',
                          searchCode: 'searchCode',
                          immediateAllocation: 'immediateAllocation'
                        }
                      ],
                      tableControlParameters: {
                        [TableFieldParameterEnum.ChangeInTermsMethodFlag]: 'Y'
                      }
                    }
                  ]
                }
              }
            }
          }
        } as any),
      {
        workflowTemplateService: {
          downloadWorkflowTemplate: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );
    expect(nextState.payload).toEqual({});
  });
  it('thunk action > download when downloadTemplateType = "manageMethodLevelProcessingDecisionTablesMLPWorkflowCA"', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType:
        'manageMethodLevelProcessingDecisionTablesMLPWorkflowCA'
    })(
      store.dispatch,
      () =>
        ({
          workflowSetup: {
            decisionElementList: {
              MLPCA: {
                decisionElementList: [
                  {
                    configurable: 'configurable',
                    tableControlParameters: {
                      [TableFieldParameterEnum.ChangeInTermsMethodFlag]: 'Y'
                    }
                  }
                ]
              }
            },
            workflowFormSteps: {
              forms: {
                configMlpCa: {
                  tables: [
                    {
                      elementList: [
                        {
                          name: 'name',
                          searchCode: 'searchCode',
                          immediateAllocation: 'immediateAllocation'
                        }
                      ]
                    }
                  ]
                }
              }
            }
          }
        } as any),
      {
        workflowTemplateService: {
          downloadWorkflowTemplate: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );
    expect(nextState.payload).toEqual({});
  });
  it('thunk action > download when downloadTemplateType = "manageMethodLevelProcessingDecisionTablesMLPWorkflowCA" with diff immediateAllocation', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType:
        'manageMethodLevelProcessingDecisionTablesMLPWorkflowCA'
    })(
      store.dispatch,
      () =>
        ({
          workflowSetup: {
            decisionElementList: {
              MLPCA: {
                decisionElementList: [
                  {
                    configurable: 'configurable',
                    tableControlParameters: {
                      [TableFieldParameterEnum.ChangeInTermsMethodFlag]: 'Y'
                    }
                  }
                ]
              }
            },
            workflowFormSteps: {
              forms: {
                configMlpCa: {
                  tables: [
                    {
                      elementList: [
                        {
                          name: 'name',
                          searchCode: 'searchCode',
                          immediateAllocation: 'No'
                        }
                      ]
                    }
                  ]
                }
              }
            }
          }
        } as any),
      {
        workflowTemplateService: {
          downloadWorkflowTemplate: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );
    expect(nextState.payload).toEqual({});
  });
  it('thunk action > download when downloadTemplateType = "manageMethodLevelProcessingDecisionTablesMLPWorkflowCA" with diff immediateAllocation', async () => {
    const nextState: any = await actionsWorkflowSetup.downloadTemplateFile({
      downloadTemplateType:
        'manageMethodLevelProcessingDecisionTablesMLPWorkflowCA'
    })(
      store.dispatch,
      () =>
        ({
          workflowSetup: {
            decisionElementList: {
              MLPCA: {
                decisionElementList: [
                  {
                    configurable: 'configurable',
                    tableControlParameters: {
                      [TableFieldParameterEnum.ChangeInTermsMethodFlag]: 'Y'
                    }
                  }
                ]
              }
            },
            workflowFormSteps: {
              forms: {
                configMlpCa: {
                  tables: [
                    {
                      elementList: [
                        {
                          name: 'name',
                          searchCode: 'searchCode',
                          immediateAllocation: 'Yes'
                        }
                      ]
                    }
                  ]
                }
              }
            }
          }
        } as any),
      {
        workflowTemplateService: {
          downloadWorkflowTemplate: jest
            .fn()
            .mockResolvedValue({ data: { attachment: {} } })
        }
      }
    );
    expect(nextState.payload).toEqual({});
  });
});
