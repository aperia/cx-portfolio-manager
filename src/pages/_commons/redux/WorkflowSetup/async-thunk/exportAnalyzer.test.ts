import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

jest.mock('app/helpers/fileUtilities.ts', () => ({
  ...jest.requireActual('app/helpers/fileUtilities.ts'),
  downloadBlobFile: jest.fn()
}));

const mockParams = {
  parameters: [
    {
      name: 'analyzer.type',
      values: ['PCF']
    },
    {
      name: 'service',
      values: ['AM']
    },
    {
      name: 'subject',
      values: ['AA']
    },
    {
      name: 'section',
      values: ['$ALL']
    },
    {
      name: 'system',
      values: ['$ALL']
    }
  ]
};

const initReducer: any = {
  workflowInstance: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      gettingStart: {
        jfs: 'jfs',
        fdsa: 'fdsa'
      },
      changeInformation: {
        changeSetId: '1111',
        workflowInstanceId: '2222',
        name: 'mock name',
        description: 'mock description',
        workflowNote: 'mock note',
        effectiveDate: new Date()
      }
    }
  }
};

describe('redux-store > workflow-setup > exportAnalyzer', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.exportAnalyzer.pending('exportAnalyzer', mockParams)
    );
    expect(nextState.exportAnalyzer).toBeUndefined();
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.exportAnalyzer.fulfilled(
        {
          data: {
            attachment: {
              attachment: {
                attachmentId: '001',
                filename: 'name.ext.png',
                mimeType: 'type',
                fileSize: 100,
                fileStream: 'steam',
                createdDate: '2021-01-28T11:16:14.3536967+07:00'
              }
            }
          }
        },
        'exportAnalyzer',
        mockParams
      )
    );
    expect(nextState.exportAnalyzer).toBeUndefined();
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.exportAnalyzer.rejected(
        new Error('mock error'),
        'exportAnalyzer',
        mockParams
      )
    );
    expect(nextState.profileTransactions).toBeUndefined();
  });
  it('thunk action > with exportAnalyzer', async () => {
    await actionsWorkflowSetup.exportAnalyzer(mockParams)(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: {
            data: { downloadTemplateFile: { filename: 'filename' } }
          },
          workflowSetup: {
            ...initReducer
          }
        } as RootState),
      {
        workflowService: {
          exportAnalyzer: jest.fn().mockResolvedValue({
            data: {
              attachment: {
                attachmentId: '001',
                mimeType: 'type',
                fileSize: 100,
                fileStream: 'steam',
                createdDate: '2021-01-28T11:16:14.3536967+07:00'
              }
            }
          })
        }
      }
    );
  });

  it('thunk action > with exportAnalyzer file name split dot .length > 2', async () => {
    const nextState: any = await actionsWorkflowSetup.exportAnalyzer(
      mockParams
    )(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: {
            data: {
              downloadTemplateFile: {
                attachmentId: 'attachmentId',
                filename: 'filename',
                mimeType: 'mimeType',
                fileSize: 'fileSize',
                fileStream: 'fileStream',
                createdDate: 'createdDate'
              }
            }
          },
          workflowSetup: {
            ...initReducer
          }
        } as RootState),
      {
        workflowService: {
          exportAnalyzer: jest.fn().mockResolvedValue({
            data: {
              attachment: {
                attachmentId: '001',
                filename: 'name.ext.ext.png',
                mimeType: 'type',
                fileSize: 100,
                fileStream: 'steam',
                createdDate: '2021-01-28T11:16:14.3536967+07:00'
              }
            }
          })
        }
      }
    );
    expect(nextState.payload).toEqual(undefined);
  });
});
