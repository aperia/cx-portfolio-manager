import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';

export const getFileDetails = createAsyncThunk<
  any,
  MagicKeyValue,
  ThunkAPIConfig
>('workflowSetup/getFileDetails', async (args, thunkAPI) => {
  const { attachmentIds } = args;
  const { workflowTemplateService } = get(thunkAPI, 'extra') as APIMapping;

  const response = await workflowTemplateService.getFileDetails({
    searchFields: { attachmentIds }
  });

  return response.data;
});

export const getFileDetailsBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(getFileDetails.pending, draftState => {
    set(draftState.workflowSetup, 'loading', true);
  });
  builder.addCase(getFileDetails.fulfilled, draftState => {
    set(draftState.workflowSetup, 'loading', false);
  });
  builder.addCase(getFileDetails.rejected, draftState => {
    set(draftState.workflowSetup, 'loading', false);
  });
};
