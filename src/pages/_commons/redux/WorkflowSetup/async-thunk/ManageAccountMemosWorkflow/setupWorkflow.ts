import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types/workflowSetupState';
import get from 'lodash.get';
import set from 'lodash.set';
import { FormGettingStart } from 'pages/WorkflowManageAccountMemos/GettingStartStep';
import {
  ACTIONS_MEMOS,
  NAME_MEMOS
} from 'pages/WorkflowManageAccountMemos/GettingStartStep/constant';
import { ConfigureParametersFormValue } from 'pages/WorkflowManageAccountMemos/ManageAccountMemosStep';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';

export const setupManageAccountMemosWorkflow = createAsyncThunk<
  boolean,
  WorkflowSetupThunkArg,
  ThunkAPIConfig
>(
  'workflowSetup/setupManageAccountMemosWorkflow',
  async ({ workflowSetupData }, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

    const state = thunkAPI.getState();
    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;

    const gettingStart = state.workflowSetup?.workflowFormSteps.forms[
      'gettingStart'
    ] as FormGettingStart;

    let idUpload = '';
    let action = '';

    switch (gettingStart?.actionsMemos) {
      case ACTIONS_MEMOS.ADD:
        idUpload = 'uploadFileAdd';
        action = 'Add Memos';
        break;
      case ACTIONS_MEMOS.DELETE:
        idUpload = 'uploadFileDelete';
        action = 'Delete Memos';
        break;
      case ACTIONS_MEMOS.FIND:
      default:
        action = 'Find Memos';
        break;
    }
    const uploadFileForm = state.workflowSetup?.workflowFormSteps.forms[
      idUpload
    ] as any;

    const config = state.workflowSetup?.workflowFormSteps.forms[
      'configureParametersManageAccountMemos'
    ] as ConfigureParametersFormValue;

    const name =
      gettingStart?.nameMemos === NAME_MEMOS.CIS
        ? 'Customer Inquiry System (CIS) Memos'
        : 'Chronicle Memos';

    const workflowInstanceId = changeForm?.workflowInstanceId?.toString();
    const updateRequest = {
      workflowInstanceId,
      status: 'COMPLETE',
      data: {
        workflowSetupData,
        configurations: {
          attachmentId: uploadFileForm?.attachmentId,
          memoType: name,
          action: action,
          addMemo: config?.parameters?.memos,
          memoText: config?.parameters?.memoText,
          type: config?.parameters?.type,
          queryString: config?.queryString
        }
      }
    };

    await workflowService.updateWorkflowInstanceData(updateRequest);

    return true;
  }
);
export const setupManageAccountMemosWorkflowBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(setupManageAccountMemosWorkflow.pending, draftState => {
    set(draftState.workflowSetup, 'loading', true);
  });
  builder.addCase(setupManageAccountMemosWorkflow.fulfilled, draftState => {
    set(draftState.workflowSetup, 'loading', false);
    set(draftState.workflowSetup, 'error', false);
  });
  builder.addCase(setupManageAccountMemosWorkflow.rejected, draftState => {
    set(draftState.workflowSetup, 'loading', false);
    set(draftState.workflowSetup, 'error', true);
  });
};
