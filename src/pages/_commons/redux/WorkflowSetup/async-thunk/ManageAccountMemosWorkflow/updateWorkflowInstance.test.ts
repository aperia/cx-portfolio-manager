import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  ACTIONS_MEMOS,
  NAME_MEMOS
} from 'pages/WorkflowManageAccountMemos/GettingStartStep/constant';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  workflowInstance: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      changeInformation: {
        changeSetId: '1111',
        workflowInstanceId: '2222',
        name: 'mock name',
        description: 'mock description',
        workflowNote: 'mock note',
        effectiveDate: new Date()
      },
      selectMonetaryAdjustmentTransactions: {
        attachmentId: 'attachmentId',
        date: {
          minDate: '10/01/2021',
          maxDate: '10/31/2021'
        }
      },
      uploadFile: {
        attachmentId: 'attachmentId'
      },
      gettingStart: {
        actionsMemos: ACTIONS_MEMOS.ADD,
        nameMemos: NAME_MEMOS.CIS
      }
    }
  }
};

const initReducer1: any = {
  workflowInstance: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      changeInformation: {
        changeSetId: '1111',
        workflowInstanceId: '2222',
        name: 'mock name',
        description: 'mock description',
        workflowNote: 'mock note',
        effectiveDate: new Date()
      },
      selectMonetaryAdjustmentTransactions: {
        attachmentId: 'attachmentId',
        date: {
          minDate: '10/01/2021',
          maxDate: '10/31/2021'
        }
      },
      uploadFile: {
        attachmentId: 'attachmentId'
      },
      gettingStart: {
        actionsMemos: ACTIONS_MEMOS.DELETE,
        nameMemos: NAME_MEMOS.CHRONICLE
      }
    }
  }
};

const initReducer2: any = {
  workflowInstance: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      changeInformation: {
        changeSetId: '1111',
        workflowInstanceId: '2222',
        name: 'mock name',
        description: 'mock description',
        workflowNote: 'mock note',
        effectiveDate: new Date()
      },
      selectMonetaryAdjustmentTransactions: {
        attachmentId: 'attachmentId',
        date: {
          minDate: '10/01/2021',
          maxDate: '10/31/2021'
        }
      },
      uploadFile: {
        attachmentId: 'attachmentId'
      },
      gettingStart: {
        actionsMemos: ACTIONS_MEMOS.FIND,
        nameMemos: NAME_MEMOS.CHRONICLE
      }
    }
  }
};

describe('redux-store > workflow-setup > ManageAccountMemosWorkflow > updateWorkflowInstance', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.updateManageAccountMemosWorkflowInstance.pending(
        'updateWorkflowInstance',
        { workflowSetupData: [] }
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.updateManageAccountMemosWorkflowInstance.fulfilled(
        true,
        'updateWorkflowInstance',
        { workflowSetupData: [] }
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.updateManageAccountMemosWorkflowInstance.rejected(
        new Error('mock error'),
        'updateWorkflowInstance',
        { workflowSetupData: [] }
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(true);
  });

  it('thunk action > with workflowInstanceId & changeId', async () => {
    const updateWorkflowDataFn = jest.fn();

    const nextState: any =
      await actionsWorkflowSetup.updateManageAccountMemosWorkflowInstance({
        workflowSetupData: []
      })(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer
            }
          } as RootState),
        {
          workflowService: {
            updateWorkflowInstanceData: updateWorkflowDataFn
          }
        }
      );
    expect(nextState.payload).toEqual(true);

    expect(updateWorkflowDataFn).toHaveBeenCalled();
  });

  it('thunk action > with workflowInstanceId & changeId', async () => {
    const updateWorkflowDataFn = jest.fn();

    const nextState: any =
      await actionsWorkflowSetup.updateManageAccountMemosWorkflowInstance({
        workflowSetupData: []
      })(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer1
            }
          } as RootState),
        {
          workflowService: {
            updateWorkflowInstanceData: updateWorkflowDataFn
          }
        }
      );
    expect(nextState.payload).toEqual(true);

    expect(updateWorkflowDataFn).toHaveBeenCalled();
  });

  it('thunk action > with workflowInstanceId & changeId', async () => {
    const updateWorkflowDataFn = jest.fn();

    const nextState: any =
      await actionsWorkflowSetup.updateManageAccountMemosWorkflowInstance({
        workflowSetupData: []
      })(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer2
            }
          } as RootState),
        {
          workflowService: {
            updateWorkflowInstanceData: updateWorkflowDataFn
          }
        }
      );
    expect(nextState.payload).toEqual(true);

    expect(updateWorkflowDataFn).toHaveBeenCalled();
  });
});
