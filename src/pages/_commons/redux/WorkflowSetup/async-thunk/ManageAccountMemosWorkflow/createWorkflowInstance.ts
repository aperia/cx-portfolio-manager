import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';
import { FormGettingStart } from 'pages/WorkflowManageAccountMemos/GettingStartStep';
import {
  ACTIONS_MEMOS,
  NAME_MEMOS
} from 'pages/WorkflowManageAccountMemos/GettingStartStep/constant';
import { ConfigureParametersFormValue } from 'pages/WorkflowManageAccountMemos/ManageAccountMemosStep';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { workflowSetupTemplateIdSelector } from '../../select-hooks';

export const createManageAccountMemosWorkflowInstance = createAsyncThunk<
  any,
  WorkflowSetupThunkArg,
  ThunkAPIConfig
>(
  'workflowSetup/createManageAccountMemosWorkflowInstance',
  async ({ workflowSetupData }, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;
    const state = thunkAPI.getState();

    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;

    const gettingStart = state.workflowSetup?.workflowFormSteps.forms[
      'gettingStart'
    ] as FormGettingStart;

    let idUpload = '';
    let action = '';

    switch (gettingStart?.actionsMemos) {
      case ACTIONS_MEMOS.ADD:
        idUpload = 'uploadFileAdd';
        action = 'Add Memos';
        break;
      case ACTIONS_MEMOS.DELETE:
        idUpload = 'uploadFileDelete';
        action = 'Delete Memos';
        break;
      case ACTIONS_MEMOS.FIND:
      default:
        action = 'Find Memos';
        break;
    }

    const uploadFileForm = state.workflowSetup?.workflowFormSteps.forms[
      idUpload
    ] as any;

    const config = state.workflowSetup?.workflowFormSteps.forms[
      'configureParametersManageAccountMemos'
    ] as ConfigureParametersFormValue;

    const name =
      gettingStart?.nameMemos === NAME_MEMOS.CIS
        ? 'Customer Inquiry System (CIS) Memos'
        : 'Chronicle Memos';

    const workflowTemplateId = workflowSetupTemplateIdSelector(state);
    const changeSetId = changeForm?.changeSetId?.toString();
    let workflowInstanceId = changeForm?.workflowInstanceId?.toString();

    if (workflowInstanceId && changeSetId) {
      await workflowService.updateWorkflowInstance({
        workflowInstanceId,
        changeSetId,
        workflowInstanceDetails: {
          note: changeForm.workflowNote
        }
      });
    } else {
      const workflowRes = await workflowService.createWorkflowInstance({
        workflowInstanceDetails: {
          note: changeForm?.workflowNote
        },
        workflowTemplateId: workflowTemplateId || '',
        changeSetId
      });

      workflowInstanceId =
        workflowRes?.data?.workflowInstanceDetails?.id?.toString();
    }

    const updateRequest = {
      workflowInstanceId,
      status: 'INCOMPLETE',
      data: {
        workflowSetupData,
        configurations: {
          attachmentId: uploadFileForm?.attachmentId,
          memoType: name,
          action: action,
          addMemo: config?.parameters?.memos,
          memoText: config?.parameters?.memoText,
          type: config?.parameters?.type,
          queryString: config?.queryString
        }
      }
    };

    await workflowService.updateWorkflowInstanceData(updateRequest);

    return { changeSetId, workflowInstanceId };
  }
);
export const createManageAccountMemosWorkflowInstanceBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    createManageAccountMemosWorkflowInstance.pending,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', true);
      set(draftState.workflowInstance, 'error', false);
    }
  );
  builder.addCase(
    createManageAccountMemosWorkflowInstance.fulfilled,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', false);
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'changeSetId',
        action.payload?.changeSetId
      );
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'workflowInstanceId',
        action.payload?.workflowInstanceId
      );
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'isCompletedWorkflow',
        false
      );
    }
  );
  builder.addCase(
    createManageAccountMemosWorkflowInstance.rejected,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', true);
    }
  );
};
