import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';

export const getNonWorkflowMetadata = createAsyncThunk<
  any,
  string[],
  ThunkAPIConfig
>(
  'workflowSetup/getNonWorkflowMetadata',
  async (elementNames: string[], thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;
    let elements: ElementMetaData[] = [];
    const result = await workflowService.getElementDataByName(
      'NON-WORKFLOW',
      elementNames
    );
    elements = elements.concat(result.data?.elementMetaData || []);

    return { elements };
  }
);

export const getNonWorkflowMetadataBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(getNonWorkflowMetadata.pending, (draftState, action) => {
    set(draftState.elementMetadataNonWorkflow, 'loading', true);
    set(draftState.elementMetadataNonWorkflow, 'error', false);
  });
  builder.addCase(getNonWorkflowMetadata.fulfilled, (draftState, action) => {
    set(draftState.elementMetadataNonWorkflow, 'loading', false);
    set(draftState.elementMetadataNonWorkflow, 'error', false);
    set(draftState.elementMetadataNonWorkflow, 'elements', [
      ...(draftState.elementMetadataNonWorkflow.elements || []),
      ...(action.payload.elements || [])
    ]);
  });
  builder.addCase(getNonWorkflowMetadata.rejected, (draftState, action) => {
    set(draftState.elementMetadataNonWorkflow, 'loading', false);
    set(draftState.elementMetadataNonWorkflow, 'error', true);
  });
};
