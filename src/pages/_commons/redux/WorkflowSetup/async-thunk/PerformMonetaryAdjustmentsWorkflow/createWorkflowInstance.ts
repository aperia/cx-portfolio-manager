import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { workflowSetupTemplateIdSelector } from '../../select-hooks';

export const createPerformMonetaryAdjustmentTransactionsWorkflowInstance =
  createAsyncThunk<any, WorkflowSetupThunkArg, ThunkAPIConfig>(
    'workflowSetup/createPerformMonetaryAdjustmentTransactionsWorkflowInstance',
    async ({ workflowSetupData }, thunkAPI) => {
      const { workflowService } = get(thunkAPI, 'extra') as APIMapping;
      const state = thunkAPI.getState();

      const changeForm = state.workflowSetup?.workflowFormSteps.forms[
        'changeInformation'
      ] as FormChangeInfo;
      const uploadFileForm = state.workflowSetup?.workflowFormSteps.forms[
        'uploadFile'
      ] as any;
      const { transactions } = state.workflowSetup?.workflowFormSteps.forms[
        'selectMonetaryAdjustmentTransactions'
      ] as any;

      const workflowTemplateId = workflowSetupTemplateIdSelector(state);
      const changeSetId = changeForm?.changeSetId?.toString();
      let workflowInstanceId = changeForm?.workflowInstanceId?.toString();

      if (workflowInstanceId && changeSetId) {
        await workflowService.updateWorkflowInstance({
          workflowInstanceId,
          changeSetId,
          workflowInstanceDetails: {
            note: changeForm.workflowNote
          }
        });
      } else {
        const workflowRes = await workflowService.createWorkflowInstance({
          workflowInstanceDetails: {
            note: changeForm?.workflowNote
          },
          workflowTemplateId: workflowTemplateId || '',
          changeSetId
        });

        workflowInstanceId =
          workflowRes?.data?.workflowInstanceDetails?.id?.toString();
      }

      const updateRequest = {
        workflowInstanceId,
        status: 'INCOMPLETE',
        data: {
          workflowSetupData,
          configurations: {
            attachmentId: uploadFileForm.attachmentId,
            transactions
          }
        }
      };

      await workflowService.updateWorkflowInstanceData(updateRequest);

      return { changeSetId, workflowInstanceId };
    }
  );
export const createPerformMonetaryAdjustmentTransactionsWorkflowInstanceBuilder =
  (builder: ActionReducerMapBuilder<WorkflowSetupRootState>) => {
    builder.addCase(
      createPerformMonetaryAdjustmentTransactionsWorkflowInstance.pending,
      (draftState, action) => {
        set(draftState.workflowInstance, 'loading', true);
        set(draftState.workflowInstance, 'error', false);
      }
    );
    builder.addCase(
      createPerformMonetaryAdjustmentTransactionsWorkflowInstance.fulfilled,
      (draftState, action) => {
        set(draftState.workflowInstance, 'loading', false);
        set(draftState.workflowInstance, 'error', false);
        set(
          draftState.workflowFormSteps.forms['changeInformation'],
          'changeSetId',
          action.payload?.changeSetId
        );
        set(
          draftState.workflowFormSteps.forms['changeInformation'],
          'workflowInstanceId',
          action.payload?.workflowInstanceId
        );
        set(
          draftState.workflowFormSteps.forms['changeInformation'],
          'isCompletedWorkflow',
          false
        );
      }
    );
    builder.addCase(
      createPerformMonetaryAdjustmentTransactionsWorkflowInstance.rejected,
      (draftState, action) => {
        set(draftState.workflowInstance, 'loading', false);
        set(draftState.workflowInstance, 'error', true);
      }
    );
  };
