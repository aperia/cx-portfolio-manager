import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';

export const updatePerformMonetaryAdjustmentTransactionsWorkflowInstance =
  createAsyncThunk<boolean, WorkflowSetupThunkArg, ThunkAPIConfig>(
    'workflowSetup/updatePerformMonetaryAdjustmentTransactionsWorkflowInstance',
    async ({ workflowSetupData }, thunkAPI) => {
      const { workflowService } = get(thunkAPI, 'extra') as APIMapping;
      const state = thunkAPI.getState();

      const changeForm = state.workflowSetup?.workflowFormSteps.forms[
        'changeInformation'
      ] as FormChangeInfo;
      const uploadFileForm = state.workflowSetup?.workflowFormSteps.forms[
        'uploadFile'
      ] as any;
      const { transactions } = state.workflowSetup?.workflowFormSteps.forms[
        'selectMonetaryAdjustmentTransactions'
      ] as any;

      const updateRequest = {
        workflowInstanceId: changeForm?.workflowInstanceId?.toString(),
        status: 'INCOMPLETE',
        data: {
          workflowSetupData,
          configurations: {
            attachmentId: uploadFileForm?.attachmentId,
            transactions
          }
        }
      };

      await workflowService.updateWorkflowInstanceData(updateRequest);

      return true;
    }
  );
export const updatePerformMonetaryAdjustmentTransactionsWorkflowInstanceBuilder =
  (builder: ActionReducerMapBuilder<WorkflowSetupRootState>) => {
    builder.addCase(
      updatePerformMonetaryAdjustmentTransactionsWorkflowInstance.pending,
      (draftState, action) => {
        set(draftState.workflowInstance, 'loading', true);
        set(draftState.workflowInstance, 'error', false);
      }
    );
    builder.addCase(
      updatePerformMonetaryAdjustmentTransactionsWorkflowInstance.fulfilled,
      (draftState, action) => {
        set(draftState.workflowInstance, 'loading', false);
        set(draftState.workflowInstance, 'error', false);
        set(
          draftState.workflowFormSteps.forms['changeInformation'],
          'isCompletedWorkflow',
          false
        );
      }
    );
    builder.addCase(
      updatePerformMonetaryAdjustmentTransactionsWorkflowInstance.rejected,
      (draftState, action) => {
        set(draftState.workflowInstance, 'loading', false);
        set(draftState.workflowInstance, 'error', true);
      }
    );
  };
