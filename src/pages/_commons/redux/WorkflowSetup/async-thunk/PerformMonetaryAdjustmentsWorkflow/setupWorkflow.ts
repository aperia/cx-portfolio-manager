import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types/workflowSetupState';
import get from 'lodash.get';
import set from 'lodash.set';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';

export const setupPerformMonetaryAdjustmentTransactionsWorkflow =
  createAsyncThunk<boolean, WorkflowSetupThunkArg, ThunkAPIConfig>(
    'workflowSetup/setupPerformMonetaryAdjustmentTransactionsWorkflow',
    async ({ workflowSetupData }, thunkAPI) => {
      const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

      const state = thunkAPI.getState();
      const changeForm = state.workflowSetup?.workflowFormSteps.forms[
        'changeInformation'
      ] as FormChangeInfo;
      const uploadFileForm = state.workflowSetup?.workflowFormSteps.forms[
        'uploadFile'
      ] as any;
      const { transactions } = state.workflowSetup?.workflowFormSteps.forms[
        'selectMonetaryAdjustmentTransactions'
      ] as any;

      const workflowInstanceId = changeForm?.workflowInstanceId?.toString();
      const updateRequest = {
        workflowInstanceId,
        status: 'COMPLETE',
        data: {
          workflowSetupData,
          configurations: {
            attachmentId: uploadFileForm?.attachmentId,
            transactions
          }
        }
      };

      await workflowService.updateWorkflowInstanceData(updateRequest);

      return true;
    }
  );
export const setupPerformMonetaryAdjustmentTransactionsWorkflowBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    setupPerformMonetaryAdjustmentTransactionsWorkflow.pending,
    draftState => {
      set(draftState.workflowSetup, 'loading', true);
    }
  );
  builder.addCase(
    setupPerformMonetaryAdjustmentTransactionsWorkflow.fulfilled,
    draftState => {
      set(draftState.workflowSetup, 'loading', false);
      set(draftState.workflowSetup, 'error', false);
    }
  );
  builder.addCase(
    setupPerformMonetaryAdjustmentTransactionsWorkflow.rejected,
    draftState => {
      set(draftState.workflowSetup, 'loading', false);
      set(draftState.workflowSetup, 'error', true);
    }
  );
};
