import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  workflowInstance: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      gettingStart: {
        ca: true,
        aq: true,
        st: true
      },
      changeInformation: {
        changeSetId: '1111',
        workflowInstanceId: '2222',
        name: 'mock name',
        description: 'mock description',
        workflowNote: 'mock note',
        effectiveDate: new Date()
      }
    }
  }
};

const initReducerWithGettingStarted: any = {
  workflowInstance: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      gettingStart: {
        ca: false,
        aq: false,
        st: false
      },
      changeInformation: {
        changeSetId: '1111',
        workflowInstanceId: '2222',
        name: 'mock name',
        description: 'mock description',
        workflowNote: 'mock note',
        effectiveDate: new Date()
      }
    }
  }
};

jest.mock('app/helpers', () => {
  const modules = jest.requireActual('app/helpers');
  return {
    ...modules,
    formatTimeDefault: () => '01/01/2021'
  };
});

describe('redux-store > workflow-setup > ManageMethodLevelProcessingDecisionTablesMLPWorkflow > updateWorkflowInstance', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.updateManageMethodLevelProcessingDecisionTablesMLPWorkflowInstance.pending(
        'updateWorkflowInstance',
        {}
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.updateManageMethodLevelProcessingDecisionTablesMLPWorkflowInstance.fulfilled(
        true,
        'updateWorkflowInstance',
        {}
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.updateManageMethodLevelProcessingDecisionTablesMLPWorkflowInstance.rejected(
        new Error('mock error'),
        'updateWorkflowInstance',
        {}
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(true);
  });

  it('thunk action > with workflowInstanceId & changeId', async () => {
    const mockRes = { data: { workflowSetupData: [] } };
    const updateWorkflowDataFn = jest.fn().mockResolvedValue(mockRes);

    const nextState: any =
      await actionsWorkflowSetup.updateManageMethodLevelProcessingDecisionTablesMLPWorkflowInstance(
        {
          workflowSetupData: []
        }
      )(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer
            }
          } as RootState),
        {
          workflowService: {
            updateWorkflowInstanceData: updateWorkflowDataFn
          }
        }
      );
    expect(nextState.payload.workflowSetupData).toEqual([]);
    expect(updateWorkflowDataFn).toHaveBeenCalled();
  });

  it('thunk action > with workflowInstanceId & changeId && ca false', async () => {
    const mockRes = { data: { workflowSetupData: [] } };
    const updateWorkflowDataFn = jest.fn().mockResolvedValue(mockRes);

    const nextState: any =
      await actionsWorkflowSetup.updateManageMethodLevelProcessingDecisionTablesMLPWorkflowInstance(
        {
          workflowSetupData: []
        }
      )(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducerWithGettingStarted
            }
          } as RootState),
        {
          workflowService: {
            updateWorkflowInstanceData: updateWorkflowDataFn
          }
        }
      );
    expect(nextState?.payload?.workflowSetupData).toBeUndefined();
  });
});
