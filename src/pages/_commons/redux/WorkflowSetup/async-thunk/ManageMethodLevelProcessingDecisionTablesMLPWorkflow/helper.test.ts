import { parseTableData } from './helper';

const fixture = (hasParameter?: boolean, immediateAllocation?: string) => ({
  tables: hasParameter
    ? [
        {
          elementList: [{ name: 'John' }],
          tableControlParameters: { name: 'value' }
        }
      ]
    : [
        {
          elementList: [
            { name: 'John', immediateAllocation: immediateAllocation }
          ]
        }
      ]
});

describe('test parseTableData helper', () => {
  it('should be return empty', () => {
    const actual = parseTableData({ tables: [] }, 'type');
    expect(actual).toEqual([]);
  });

  it('should be working', () => {
    const actual = parseTableData(fixture(true), 'type');
    expect(actual.length).toEqual(1);
  });

  it('should be working without tableControlParameters', () => {
    const actual = parseTableData(fixture(), 'type');
    expect(actual.length).toEqual(1);
  });

  it('should be working without tableControlParameters and has immediateAllocation', () => {
    const actual = parseTableData(fixture(false, 'Yes'), 'type');
    expect(actual.length).toEqual(1);
  });
});
