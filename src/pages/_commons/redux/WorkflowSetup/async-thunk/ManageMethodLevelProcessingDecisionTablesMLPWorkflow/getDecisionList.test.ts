import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';
import { getDecisionListMLP } from './getDecisionList';

const initReducer: any = {
  decisionList: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      changeInformation: {
        name: 'mock name',
        description: 'mock description',
        effectiveDate: new Date()
      }
    }
  }
};

describe('redux-store > workflow-setup > ManageAccountLevelWorkflow > getDecisionList', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getDecisionListMLP.pending('getDecisionListMLP', {
        tableType: ''
      })
    );
    expect(nextState.decisionList.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getDecisionListMLP.fulfilled(
        true,
        'getDecisionListMLP',
        {
          tableType: ''
        }
      )
    );
    expect(nextState.decisionList.loading).toEqual(false);
    expect(nextState.decisionList.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getDecisionListMLP.rejected(
        new Error('mock error'),
        'getDecisionListMLP',
        { tableType: '' }
      )
    );
    expect(nextState.decisionList.loading).toEqual(false);
    expect(nextState.decisionList.error).toEqual(true);
  });

  it('thunk action', async () => {
    const nextState: any = await getDecisionListMLP({ tableType: '' })(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          workflowSetup: {
            ...initReducer
          }
        } as RootState),
      {
        changeService: {
          getDecisionList: jest.fn().mockResolvedValue({
            data: {
              tableList: [],
              total: []
            }
          })
        }
      }
    );
    expect(nextState?.payload?.workflowSetupData).toBeUndefined();
  });
});
