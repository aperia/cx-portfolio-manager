import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';
import { ConfigureParametersValue } from 'pages/WorkflowManageMethodLevelProcessingDecisionTablesMLP/ConfigureParameters';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { workflowSetupTemplateIdSelector } from '../../select-hooks';
import { parseTableData } from './helper';

export const createManageMethodLevelProcessingDecisionTablesMLPWorkflowInstance =
  createAsyncThunk<any, WorkflowSetupThunkArg, ThunkAPIConfig>(
    'workflowSetup/createManageMethodLevelProcessingDecisionTablesMLPWorkflowInstance',
    async ({ workflowSetupData }, thunkAPI) => {
      const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

      const state = thunkAPI.getState();

      const changeForm = state.workflowSetup?.workflowFormSteps.forms[
        'changeInformation'
      ] as FormChangeInfo;

      const workflowTemplateId = workflowSetupTemplateIdSelector(state);
      const changeId = changeForm.changeSetId;
      let workflowInstanceId = changeForm.workflowInstanceId?.toString();

      if (workflowInstanceId && changeId) {
        await workflowService.updateWorkflowInstance({
          workflowInstanceId,
          changeSetId: changeId.toString(),
          workflowInstanceDetails: {
            note: changeForm.workflowNote
          }
        } as any);
      } else {
        const workflowRes = await workflowService.createWorkflowInstance({
          workflowInstanceDetails: {
            note: changeForm.workflowNote
          },
          workflowTemplateId: workflowTemplateId || '',
          changeSetId: changeId?.toString()
        });

        workflowInstanceId =
          workflowRes?.data?.workflowInstanceDetails?.id?.toString();
      }

      const aqForm = state.workflowSetup?.workflowFormSteps.forms[
        'configMlpAq'
      ] as ConfigureParametersValue;

      const tablesAQForm = parseTableData(aqForm, 'AQ');

      const uploadFileAQ = state.workflowSetup?.workflowFormSteps.forms[
        'uploadFileAccountQualification'
      ] as any;

      const caForm = state.workflowSetup?.workflowFormSteps.forms[
        'configMlpCa'
      ] as ConfigureParametersValue;

      const tablesCAForm = parseTableData(caForm, 'CA');

      const uploadFileCA = state.workflowSetup?.workflowFormSteps.forms[
        'uploadFileClientAllocation'
      ] as any;

      const stForm = state.workflowSetup?.workflowFormSteps.forms[
        'configMlpSt'
      ] as ConfigureParametersValue;

      const tablesSTForm = parseTableData(stForm, 'ST');

      const tableList = tablesAQForm.concat(tablesCAForm.concat(tablesSTForm));

      const uploadFileST = state.workflowSetup?.workflowFormSteps.forms[
        'uploadFileSelectionTable'
      ] as any;

      const updateRequest: UpdateflowInstanceData = {
        workflowInstanceId: workflowInstanceId?.toString(),
        status: 'INCOMPLETE',
        data: {
          workflowSetupData,
          configurations: {
            tableList,
            attachments: [
              {
                'table.type': 'AQ',
                attachmentId: uploadFileAQ?.attachmentId
              },
              {
                'table.type': 'CA',
                attachmentId: uploadFileCA?.attachmentId
              },
              {
                'table.type': 'ST',
                attachmentId: uploadFileST?.attachmentId
              }
            ]
          }
        }
      };
      await workflowService.updateWorkflowInstanceData(updateRequest);
      return { changeId, workflowInstanceId, workflowSetupData };
    }
  );
export const createManageMethodLevelProcessingDecisionTablesMLPWorkflowInstanceBuilder =
  (builder: ActionReducerMapBuilder<WorkflowSetupRootState>) => {
    builder.addCase(
      createManageMethodLevelProcessingDecisionTablesMLPWorkflowInstance.pending,
      (draftState, action) => {
        set(draftState.workflowInstance, 'loading', true);
        set(draftState.workflowInstance, 'error', false);
      }
    );
    builder.addCase(
      createManageMethodLevelProcessingDecisionTablesMLPWorkflowInstance.fulfilled,
      (draftState, action) => {
        set(draftState.workflowInstance, 'loading', false);
        set(draftState.workflowInstance, 'error', false);
        set(
          draftState.workflowFormSteps.forms['changeInformation'],
          'changeSetId',
          action.payload?.changeId
        );
        set(
          draftState.workflowFormSteps.forms['changeInformation'],
          'workflowInstanceId',
          action.payload?.workflowInstanceId
        );
        set(
          draftState.workflowFormSteps.forms['changeInformation'],
          'isCompletedWorkflow',
          false
        );
        set(
          draftState.workflowFormSteps,
          'workflowSetupData',
          action.payload?.workflowSetupData
        );
      }
    );
    builder.addCase(
      createManageMethodLevelProcessingDecisionTablesMLPWorkflowInstance.rejected,
      (draftState, action) => {
        set(draftState.workflowInstance, 'loading', false);
        set(draftState.workflowInstance, 'error', true);
      }
    );
  };
