import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';
import { getTableListMLP } from './getTableList';

const initReducer: any = {
  workflowInstance: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      gettingStart: {
        jfs: 'jfs',
        fdsa: 'fdsa'
      },
      changeInformation: {
        changeSetId: '1111',
        workflowInstanceId: '2222',
        name: 'mock name',
        description: 'mock description',
        workflowNote: 'mock note',
        effectiveDate: new Date()
      }
    }
  }
};

jest.mock('app/helpers', () => {
  const modules = jest.requireActual('app/helpers');
  return {
    ...modules,
    formatTimeDefault: () => '01/01/2021'
  };
});

describe('redux-store > workflow-setup > ManageAccountLevelWorkflow > getTableListMLP', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getTableListMLP.pending('getTableListMLP', {
        tableType: ''
      })
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getTableListMLP.fulfilled(true, 'getTableListMLP', {
        tableType: ''
      })
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getTableListMLP.rejected(
        new Error('mock error'),
        'getTableListMLP',
        { tableType: '' }
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(false);
  });

  it('thunk action', async () => {
    const nextState: any = await getTableListMLP({ tableType: '' })(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          workflowSetup: {
            ...initReducer
          }
        } as RootState),
      {
        changeService: {
          getTableList: jest.fn().mockResolvedValue({
            data: {
              tableList: [],
              total: []
            }
          })
        }
      }
    );
    expect(nextState?.payload?.workflowSetupData).toBeUndefined();
  });
});
