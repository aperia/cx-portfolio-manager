import { booleanToString } from 'app/helpers';

export const parseTableData = (
  tableForm: Record<string, any>,
  type: string
) => {
  let tableList: any[] = [];

  if (tableForm?.tables?.length > 0)
    tableList = tableList.concat(
      tableForm.tables.map((m: any) => {
        const tableControlParameters = Object.entries(
          m.tableControlParameters || {}
        ).map((i: any[]) => {
          return { name: i[0], value: i[1] };
        });

        const decisionElements = m.elementList.map(
          (i: Record<string, any>) => ({
            name: i.name,
            searchCode: i.searchCode,
            immediateAllocation: booleanToString(i.immediateAllocation),
            value: i.value
          })
        );

        return {
          id: '',
          tableId: m.tableId,
          tableName: m.tableName,
          comment: m.comment || '',
          decisionElements,
          tableControlParameters,
          tableType: type,
          modeledFrom: m.modeledFrom
        };
      })
    );

  return tableList;
};
