import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';

interface Params {
  tableType: string;
  serviceNameAbbreviation: string;
}

export const getTableListMLP = createAsyncThunk<any, Params, ThunkAPIConfig>(
  'workflowSetup/getTableListMLP',
  async (elementNames: Params, thunkAPI) => {
    const { changeService } = get(thunkAPI, 'extra') as APIMapping;

    const resp = await changeService.getTableList(
      elementNames.tableType,
      elementNames.serviceNameAbbreviation
    );
    return {
      tableList: resp?.data?.tableList,
      total: resp?.data?.total
    };
  }
);
export const getTableListMLPBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(getTableListMLP.pending, (draftState, action) => {
    set(draftState.tableList, 'loading', true);
    set(draftState.tableList, 'error', false);
  });
  builder.addCase(getTableListMLP.fulfilled, (draftState, action) => {
    set(draftState.tableList, 'loading', false);
    set(draftState.tableList, 'error', false);
    set(draftState.tableList, 'tableList', action.payload?.tableList);
  });
  builder.addCase(getTableListMLP.rejected, (draftState, action) => {
    set(draftState.tableList, 'loading', false);
    set(draftState.tableList, 'error', true);
  });
};
