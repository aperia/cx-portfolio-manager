import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';
import { ConfigureParametersValue } from 'pages/WorkflowManageMethodLevelProcessingDecisionTablesMLP/ConfigureParameters';
import { FormGettingStart } from 'pages/WorkflowManageMethodLevelProcessingDecisionTablesMLP/GettingStartStep';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { parseTableData } from './helper';

export const updateManageMethodLevelProcessingDecisionTablesMLPWorkflowInstance =
  createAsyncThunk<any, WorkflowSetupThunkArg, ThunkAPIConfig>(
    'workflowSetup/updateManageMethodLevelProcessingDecisionTablesMLPWorkflowInstance',
    async ({ workflowSetupData }, thunkAPI) => {
      const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

      const state = thunkAPI.getState();

      const gettingStart = state.workflowSetup?.workflowFormSteps.forms[
        'gettingStart'
      ] as FormGettingStart;

      // invalid save
      if (!gettingStart?.ca && !gettingStart.st && !gettingStart.aq) return;

      const changeForm = state.workflowSetup?.workflowFormSteps.forms[
        'changeInformation'
      ] as FormChangeInfo;

      const aqForm = state.workflowSetup?.workflowFormSteps.forms[
        'configMlpAq'
      ] as ConfigureParametersValue;

      const tablesAQForm = parseTableData(aqForm, 'AQ');

      const uploadFileAQ = state.workflowSetup?.workflowFormSteps.forms[
        'uploadFileAccountQualification'
      ] as any;

      const caForm = state.workflowSetup?.workflowFormSteps.forms[
        'configMlpCa'
      ] as ConfigureParametersValue;

      const tablesCAForm = parseTableData(caForm, 'CA');

      const uploadFileCA = state.workflowSetup?.workflowFormSteps.forms[
        'uploadFileClientAllocation'
      ] as any;

      const stForm = state.workflowSetup?.workflowFormSteps.forms[
        'configMlpSt'
      ] as ConfigureParametersValue;

      const tablesSTForm = parseTableData(stForm, 'ST');

      const tableList = tablesAQForm.concat(tablesCAForm.concat(tablesSTForm));

      const uploadFileST = state.workflowSetup?.workflowFormSteps.forms[
        'uploadFileSelectionTable'
      ] as any;

      const updateRequest: UpdateflowInstanceData = {
        workflowInstanceId: changeForm.workflowInstanceId?.toString(),
        status: 'INCOMPLETE',
        data: {
          workflowSetupData,
          configurations: {
            tableList,
            attachments: [
              {
                'table.type': 'AQ',
                attachmentId: uploadFileAQ?.attachmentId
              },
              {
                'table.type': 'CA',
                attachmentId: uploadFileCA?.attachmentId
              },
              {
                'table.type': 'ST',
                attachmentId: uploadFileST?.attachmentId
              }
            ]
          }
        }
      };

      await workflowService.updateWorkflowInstanceData(updateRequest);

      return { workflowSetupData };
    }
  );
export const updateManageMethodLevelProcessingDecisionTablesMLPWorkflowInstanceBuilder =
  (builder: ActionReducerMapBuilder<WorkflowSetupRootState>) => {
    builder.addCase(
      updateManageMethodLevelProcessingDecisionTablesMLPWorkflowInstance.pending,
      (draftState, action) => {
        set(draftState.workflowInstance, 'loading', true);
        set(draftState.workflowInstance, 'error', false);
      }
    );
    builder.addCase(
      updateManageMethodLevelProcessingDecisionTablesMLPWorkflowInstance.fulfilled,
      (draftState, action) => {
        set(draftState.workflowInstance, 'loading', false);
        set(draftState.workflowInstance, 'error', false);
        set(
          draftState.workflowFormSteps.forms['changeInformation'],
          'isCompletedWorkflow',
          false
        );
        set(
          draftState.workflowFormSteps,
          'workflowSetupData',
          action.payload?.workflowSetupData
        );
      }
    );
    builder.addCase(
      updateManageMethodLevelProcessingDecisionTablesMLPWorkflowInstance.rejected,
      (draftState, action) => {
        set(draftState.workflowInstance, 'loading', false);
        set(draftState.workflowInstance, 'error', true);
      }
    );
  };
