import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types/workflowSetupState';
import get from 'lodash.get';
import set from 'lodash.set';
import { ConfigureParametersValue } from 'pages/WorkflowManageMethodLevelProcessingDecisionTablesMLP/ConfigureParameters';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { parseTableData } from './helper';

export const setupManageMethodLevelProcessingDecisionTablesMLPWorkflow =
  createAsyncThunk<any, WorkflowSetupThunkArg, ThunkAPIConfig>(
    'workflowSetup/setupManageMethodLevelProcessingDecisionTablesMLPWorkflow',
    async ({ workflowSetupData }, thunkAPI) => {
      const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

      const state = thunkAPI.getState();

      const changeForm = state.workflowSetup?.workflowFormSteps.forms[
        'changeInformation'
      ] as FormChangeInfo;

      const aqForm = state.workflowSetup?.workflowFormSteps.forms[
        'configMlpAq'
      ] as ConfigureParametersValue;

      const tablesAQForm = parseTableData(aqForm, 'AQ');

      const uploadFileAQ = state.workflowSetup?.workflowFormSteps.forms[
        'uploadFileAccountQualification'
      ] as any;

      const caForm = state.workflowSetup?.workflowFormSteps.forms[
        'configMlpCa'
      ] as ConfigureParametersValue;

      const tablesCAForm = parseTableData(caForm, 'CA');

      const uploadFileCA = state.workflowSetup?.workflowFormSteps.forms[
        'uploadFileClientAllocation'
      ] as any;

      const stForm = state.workflowSetup?.workflowFormSteps.forms[
        'configMlpSt'
      ] as ConfigureParametersValue;

      const tablesSTForm = parseTableData(stForm, 'ST');

      const tableList = tablesAQForm.concat(tablesCAForm.concat(tablesSTForm));

      const uploadFileST = state.workflowSetup?.workflowFormSteps.forms[
        'uploadFileSelectionTable'
      ] as any;

      const updateRequest: UpdateflowInstanceData = {
        workflowInstanceId: changeForm.workflowInstanceId?.toString(),
        status: 'COMPLETE',
        data: {
          workflowSetupData,
          configurations: {
            tableList,
            attachments: [
              {
                'table.type': 'AQ',
                attachmentId: uploadFileAQ?.attachmentId
              },
              {
                'table.type': 'CA',
                attachmentId: uploadFileCA?.attachmentId
              },
              {
                'table.type': 'ST',
                attachmentId: uploadFileST?.attachmentId
              }
            ]
          }
        }
      };

      await workflowService.updateWorkflowInstanceData(updateRequest);

      return { workflowSetupData };
    }
  );
export const setupManageMethodLevelProcessingDecisionTablesMLPWorkflowBuilder =
  (builder: ActionReducerMapBuilder<WorkflowSetupRootState>) => {
    builder.addCase(
      setupManageMethodLevelProcessingDecisionTablesMLPWorkflow.pending,
      draftState => {
        set(draftState.workflowSetup, 'loading', true);
      }
    );
    builder.addCase(
      setupManageMethodLevelProcessingDecisionTablesMLPWorkflow.fulfilled,
      (draftState, action) => {
        set(draftState.workflowSetup, 'loading', false);
        set(draftState.workflowSetup, 'error', false);
        set(
          draftState.workflowFormSteps,
          'workflowSetupData',
          action.payload?.workflowSetupData
        );
      }
    );
    builder.addCase(
      setupManageMethodLevelProcessingDecisionTablesMLPWorkflow.rejected,
      draftState => {
        set(draftState.workflowSetup, 'loading', false);
        set(draftState.workflowSetup, 'error', true);
      }
    );
  };
