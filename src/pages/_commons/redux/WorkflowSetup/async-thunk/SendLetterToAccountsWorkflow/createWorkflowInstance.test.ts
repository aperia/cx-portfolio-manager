import { SendLetterParameterEnum } from 'app/constants/enums';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  workflowInstance: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      changeInformation: {
        name: 'mock name',
        description: 'mock description',
        effectiveDate: new Date()
      },
      configureParameters: {
        [SendLetterParameterEnum.LetterNumber]: 'A9B1',
        [SendLetterParameterEnum.Addressing]: '0'
      },
      uploadFile: {
        attachmentId: 'attachmentId'
      }
    }
  }
};

jest.mock('app/helpers', () => {
  const modules = jest.requireActual('app/helpers');
  return {
    ...modules,
    formatTimeDefault: () => '01/01/2021'
  };
});

describe('redux-store > workflow-setup > SendLetterToAccountsWorkflow > createWorkflowInstance', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.createSendLetterToAccountsWorkflowInstance.pending(
        'createWorkflowInstance',
        {}
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.createSendLetterToAccountsWorkflowInstance.fulfilled(
        {},
        'createWorkflowInstance',
        {}
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.createSendLetterToAccountsWorkflowInstance.rejected(
        new Error('mock error'),
        'createWorkflowInstance',
        {}
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(true);
  });

  it('thunk action > without workflowInstanceId & changeId', async () => {
    const nextState: any =
      await actionsWorkflowSetup.createSendLetterToAccountsWorkflowInstance({
        workflowSetupData: []
      })(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer
            }
          } as RootState),
        {
          workflowService: {
            createWorkflowInstance: jest.fn().mockResolvedValue({
              data: { workflowInstanceDetails: { id: '1111' } }
            }),
            updateWorkflowInstanceData: jest.fn()
          },
          changeService: {
            createChangeSet: jest.fn().mockResolvedValue({
              data: { id: undefined }
            })
          }
        }
      );

    expect(nextState.payload).toEqual({
      changeId: undefined,
      workflowInstanceId: '1111',
      workflowSetupData: []
    });
  });
  it('thunk action > with workflowInstanceId & changeId', async () => {
    const state = {
      workflowSetup: {
        workflow: {
          id: undefined
        }
      },
      workflowFormSteps: {
        forms: {
          changeInformation: {
            changeSetId: '2222',
            workflowInstanceId: '1111',
            name: 'mock name',
            description: 'mock description',
            effectiveDate: new Date()
          },
          configureParameters: {
            [SendLetterParameterEnum.LetterNumber]: 'A9B1',
            [SendLetterParameterEnum.Addressing]: '0'
          },
          uploadFile: {
            attachmentId: 'attachmentId'
          }
        }
      }
    };
    const nextState: any =
      await actionsWorkflowSetup.createSendLetterToAccountsWorkflowInstance({
        workflowSetupData: []
      })(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...state
            }
          } as RootState),
        {
          workflowService: {
            updateWorkflowInstance: jest.fn(),
            updateWorkflowInstanceData: jest.fn()
          }
        }
      );
    expect(nextState.payload).toEqual({
      changeId: '2222',
      workflowInstanceId: '1111',
      workflowSetupData: []
    });
  });

  it('thunk action > with existing changeId ', async () => {
    const state = {
      workflowSetup: {
        workflow: {
          id: undefined
        }
      },
      workflowFormSteps: {
        forms: {
          changeInformation: {
            changeSetId: '2222',
            name: 'mock name',
            description: 'mock description',
            effectiveDate: new Date()
          },
          configureParameters: {
            [SendLetterParameterEnum.LetterNumber]: 'A9B1',
            [SendLetterParameterEnum.Addressing]: '0'
          },
          uploadFile: {
            attachmentId: 'attachmentId'
          }
        }
      }
    };
    const nextState: any =
      await actionsWorkflowSetup.createSendLetterToAccountsWorkflowInstance({
        workflowSetupData: []
      })(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...state
            }
          } as RootState),
        {
          workflowService: {
            createWorkflowInstance: jest.fn().mockResolvedValue({
              data: { workflowInstanceDetails: { id: '1111' } }
            }),
            updateWorkflowInstanceData: jest
              .fn()
              .mockResolvedValue({ data: { workflowSetupData: [] } })
          }
        }
      );
    expect(nextState.payload).toEqual({
      changeId: '2222',
      workflowInstanceId: '1111',
      workflowSetupData: []
    });
  });
});
