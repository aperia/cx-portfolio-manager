import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types/workflowSetupState';
import get from 'lodash.get';
import set from 'lodash.set';
import { ConfigureParametersFormValues } from 'pages/WorkflowSendLetterToAccounts/ConfigureParametersStep';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { FormUploadFile } from 'pages/_commons/WorkflowSetup/CommonSteps/_components/UploadFile';

export const setupSendLetterToAccountsWorkflow = createAsyncThunk<
  any,
  WorkflowSetupThunkArg,
  ThunkAPIConfig
>(
  'workflowSetup/setupSendLetterToAccountsWorkflow',
  async ({ workflowSetupData }, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

    const state = thunkAPI.getState();

    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;

    const configureParametersForm = state.workflowSetup?.workflowFormSteps
      .forms['configureParameters'] as ConfigureParametersFormValues;

    const uploadedFile = state.workflowSetup?.workflowFormSteps.forms[
      'uploadFile'
    ] as FormUploadFile;

    const updateRequest: UpdateflowInstanceData = {
      workflowInstanceId: changeForm.workflowInstanceId?.toString(),
      status: 'COMPLETE',
      data: {
        workflowSetupData,
        configurations: {
          ...configureParametersForm.configureParameters,
          attachmentId: uploadedFile?.attachmentId?.toString()
        }
      }
    };

    await workflowService.updateWorkflowInstanceData(updateRequest);

    return { workflowSetupData };
  }
);
export const setupSendLetterToAccountsWorkflowBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(setupSendLetterToAccountsWorkflow.pending, draftState => {
    set(draftState.workflowSetup, 'loading', true);
  });
  builder.addCase(
    setupSendLetterToAccountsWorkflow.fulfilled,
    (draftState, action) => {
      set(draftState.workflowSetup, 'loading', false);
      set(draftState.workflowSetup, 'error', false);
      set(
        draftState.workflowFormSteps,
        'workflowSetupData',
        action.payload?.workflowSetupData
      );
    }
  );
  builder.addCase(setupSendLetterToAccountsWorkflow.rejected, draftState => {
    set(draftState.workflowSetup, 'loading', false);
    set(draftState.workflowSetup, 'error', true);
  });
};
