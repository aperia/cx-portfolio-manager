import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  lettersData: {},
  workflowInstanceStatus: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      changeInformation: {
        changeSetId: '1111',
        workflowInstanceId: '2222',
        name: 'mock name',
        description: 'mock description',
        workflowNote: 'mock note',
        effectiveDate: new Date()
      },
      configureParameters: {
        attachmentId: 'attachmentId',
        date: {
          minDate: '10/01/2021',
          maxDate: '10/31/2021'
        }
      },
      uploadFile: {
        attachmentId: 'attachmentId'
      }
    }
  }
};

describe('redux-store > workflow-setup >  SendLetterToAccountsWorkflow > getLetters', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getLetters.pending('getLetters')
    );
    expect(nextState.lettersData.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getLetters.fulfilled(true, 'getLetters')
    );
    expect(nextState.lettersData.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getLetters.rejected(
        new Error('mock error'),
        'getLetters'
      )
    );
    expect(nextState.lettersData.loading).toEqual(false);
  });
  it('thunk action > with getLetters', async () => {
    const nextState: any = await actionsWorkflowSetup.getLetters()(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: {
            data: {
              getLetters: {
                id: 'id',
                name: 'name',
                description: 'name',
                letterCode: 'letterCode',
                optionalNameAddressPermitted: 'optionalNameAddressPermitted',
                variables: {
                  type: 'mappingDataFromArray',
                  containerProp: 'variables',
                  variables: {
                    id: 'id',
                    name: 'name',
                    description: 'description'
                  }
                }
              }
            }
          },
          workflowSetup: {
            ...initReducer
          }
        } as RootState),
      {
        changeService: {
          getLetters: jest.fn().mockResolvedValue({
            data: {
              letters: [
                {
                  id: '1',
                  name: 'Promise to pay',
                  description: 'description',
                  letterCode: 'A9Z1',
                  optionalNameAddressPermitted: 'N',
                  variables: [
                    {
                      id: '1',
                      name: 'Annual charge',
                      description: 'variable 1'
                    }
                  ]
                },
                {
                  id: '2',
                  name: 'Promise to pay',
                  description: 'description',
                  letterCode: 'A9B1',
                  optionalNameAddressPermitted: 'Y',
                  variables: [
                    {
                      id: '1',
                      name: 'Annual charge',
                      description: 'variable 1'
                    }
                  ]
                }
              ]
            }
          })
        }
      }
    );
    expect(nextState.payload.letters.length).toEqual(2);
  });
});
