import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { mappingDataFromArray } from 'app/helpers';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';

export const getLetters = createAsyncThunk<any, void, ThunkAPIConfig>(
  'workflowSetup/getLetters',
  async (args, thunkAPI) => {
    const { changeService } = get(thunkAPI, 'extra') as APIMapping;

    // Take mapping model
    const state = thunkAPI.getState();

    const getLettersMapping = state.mapping?.data?.getLetters;

    const resp = await changeService.getLetters();

    // // Mapping
    const mappedData = mappingDataFromArray(
      resp?.data?.letters,
      getLettersMapping!
    );

    return { letters: mappedData };
  }
);
export const getLettersBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(getLetters.pending, (draftState, action) => {
    set(draftState.lettersData, 'loading', true);
    set(draftState.lettersData, 'error', false);
  });
  builder.addCase(getLetters.fulfilled, (draftState, action) => {
    set(draftState.lettersData, 'loading', false);
    set(draftState.lettersData, 'error', false);
    set(draftState.lettersData, 'letters', action.payload?.letters);
  });
  builder.addCase(getLetters.rejected, (draftState, action) => {
    set(draftState.lettersData, 'loading', false);
    set(draftState.lettersData, 'error', true);
  });
};
