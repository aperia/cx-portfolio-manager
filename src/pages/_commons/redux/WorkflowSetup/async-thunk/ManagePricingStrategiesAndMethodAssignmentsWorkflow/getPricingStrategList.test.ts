import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

describe('async-thunk > ManagePricingStrategiesAndMethodAssignmentsWorkflow > getPricingStrategList', () => {
  const initReducer: any = {
    workflowInstance: {
      loading: false,
      error: false
    },
    workflowSetup: {
      workflow: {
        id: undefined
      }
    },
    workflowFormSteps: {
      forms: {
        changeInformation: {
          name: 'mock name',
          description: 'mock description',
          effectiveDate: new Date()
        }
      }
    },
    strategyData: {
      loading: false,
      error: false,
      strategyList: []
    }
  };

  it('thunk action > with existing changeId ', async () => {
    const result = {
      data: {
        pricingStrategies: [
          {
            id: 1125362,
            name: '2R81',
            versions: [
              {
                id: 280919,
                name: 'FP5B',
                comment: 'molestie rebum wisi rebum',
                description: 'et ipsum eirmod sit ut',
                accountsProcessedByAgentAssignment: 0,
                accountsProcessedByALP: 87,
                effectiveDate: '2021-11-29T17:00:00Z',
                status: 'Pending Approval A1',
                methods: [
                  {
                    serviceSubjectSection: 'CP IC BP',
                    newValue: 'UU9INAEC'
                  },
                  {
                    serviceSubjectSection: 'CP IC IB',
                    newValue: '65D6C2UN'
                  },
                  {
                    serviceSubjectSection: 'CP IC ID',
                    newValue: 'R12OXLKI'
                  },
                  {
                    serviceSubjectSection: 'CP IC IF',
                    newValue: 'IYZRGRCY'
                  },
                  {
                    serviceSubjectSection: 'CP IC II',
                    newValue: '2PYVBUBL'
                  },
                  {
                    serviceSubjectSection: 'CP IC IM',
                    newValue: 'NQY43E0O'
                  },
                  {
                    serviceSubjectSection: 'CP IC IP',
                    newValue: ''
                  },
                  {
                    serviceSubjectSection: 'CP IC IR',
                    newValue: 'LZ6MEQHO'
                  },
                  {
                    serviceSubjectSection: 'CP IC IV',
                    newValue: ''
                  },
                  {
                    serviceSubjectSection: 'CP IC ME',
                    newValue: ''
                  },
                  {
                    serviceSubjectSection: 'CP IC MF',
                    newValue: 'DR4N9HTG'
                  },
                  {
                    serviceSubjectSection: 'CP IC ML',
                    newValue: 'N0I6YI6D'
                  },
                  {
                    serviceSubjectSection: 'CP IC MS',
                    newValue: 'Q5C4C6HQ'
                  },
                  {
                    serviceSubjectSection: 'CP IC PE',
                    newValue: '7D8T2147'
                  },
                  {
                    serviceSubjectSection: 'CP IC SC',
                    newValue: ''
                  },
                  {
                    serviceSubjectSection: 'CP IC SP',
                    newValue: '19YXDXY2'
                  },
                  {
                    serviceSubjectSection: 'CP IC SZ',
                    newValue: '4QNDV0QA'
                  },
                  {
                    serviceSubjectSection: 'CP IC TI',
                    newValue: 'DJDF1U8W'
                  },
                  {
                    serviceSubjectSection: 'CP IC VI',
                    newValue: 'GWEGOM7A'
                  },
                  {
                    serviceSubjectSection: 'CP IO AC',
                    newValue: ''
                  },
                  {
                    serviceSubjectSection: 'CP IO CI',
                    newValue: 'AECYTDTC'
                  },
                  {
                    serviceSubjectSection: 'CP IO CL',
                    newValue: '6CKWLQWT'
                  },
                  {
                    serviceSubjectSection: 'CP IO MC',
                    newValue: ''
                  },
                  {
                    serviceSubjectSection: 'CP IO MI',
                    newValue: ''
                  },
                  {
                    serviceSubjectSection: 'CP IO SC',
                    newValue: 'M81SSCMY'
                  },
                  {
                    serviceSubjectSection: 'CP OC AP',
                    newValue: 'RDAD2WUL'
                  },
                  {
                    serviceSubjectSection: 'CP OC BD',
                    newValue: 'L3ZHW32H'
                  },
                  {
                    serviceSubjectSection: 'CP OC CL',
                    newValue: 'YGV1B7OR'
                  },
                  {
                    serviceSubjectSection: 'CP OC CP',
                    newValue: '2OJ8B38J'
                  },
                  {
                    serviceSubjectSection: 'CP OC DR',
                    newValue: 'SMCEZHMM'
                  },
                  {
                    serviceSubjectSection: 'CP OC FP',
                    newValue: '3YB2Q9JH'
                  },
                  {
                    serviceSubjectSection: 'CP OC GS',
                    newValue: 'S7CGQSA6'
                  },
                  {
                    serviceSubjectSection: 'CP OC MO',
                    newValue: 'QVPJ88OY'
                  },
                  {
                    serviceSubjectSection: 'CP OC MP',
                    newValue: '84Z1VD0T'
                  },
                  {
                    serviceSubjectSection: 'CP OC PP',
                    newValue: ''
                  },
                  {
                    serviceSubjectSection: 'CP OC RB',
                    newValue: 'OA7RYMVU'
                  },
                  {
                    serviceSubjectSection: 'CP OC SA',
                    newValue: 'WFGN3NWS'
                  },
                  {
                    serviceSubjectSection: 'CP OC SD',
                    newValue: 'HKNUOFOH'
                  },
                  {
                    serviceSubjectSection: 'CP OC TI',
                    newValue: 'M5KCG0AH'
                  },
                  {
                    serviceSubjectSection: 'CP PB CT',
                    newValue: ''
                  },
                  {
                    serviceSubjectSection: 'CP PF DA',
                    newValue: '5D1X0SXY'
                  },
                  {
                    serviceSubjectSection: 'CP PF LC',
                    newValue: ''
                  },
                  {
                    serviceSubjectSection: 'CP PF OC',
                    newValue: 'MVHLESSA'
                  },
                  {
                    serviceSubjectSection: 'CP PF RC',
                    newValue: 'TYK2VECH'
                  },
                  {
                    serviceSubjectSection: 'CP PO CA',
                    newValue: ''
                  },
                  {
                    serviceSubjectSection: 'CP PO FM',
                    newValue: 'EHX5NORI'
                  },
                  {
                    serviceSubjectSection: 'CP PO MP',
                    newValue: '34S9IJVO'
                  },
                  {
                    serviceSubjectSection: 'CP PO RM',
                    newValue: '9AT1IN7M'
                  },
                  {
                    serviceSubjectSection: 'CP PO SP',
                    newValue: '63G1CSDU'
                  }
                ]
              }
            ]
          }
        ]
      }
    };
    const workflowSetupState = {
      workflowSetup: { workflow: { id: undefined } },
      workflowFormSteps: {
        forms: {
          changeInformation: { workflowInstanceId: 'workflowInstanceId' },
          uploadFile: {},
          configureParameters: {}
        }
      }
    };
    const mappingState = {
      data: {
        workflowStrategy: {
          id: 'id',
          name: 'name',
          versions: {
            type: 'mappingDataFromArray',
            containerProp: 'versions',
            versions: {
              id: 'id',
              accountsProcessedALP: 'accountsProcessedByALP',
              accountsProcessedAA: 'accountsProcessedByAgentAssignment',
              effectiveDate: 'effectiveDate',
              commentArea: 'comment',
              cisDescription: 'description',
              status: 'status',
              methods: {
                type: 'mappingDataFromArray',
                containerProp: 'methods',
                methods: {
                  currentAssignment: 'newValue',
                  serviceSubjectSection: 'serviceSubjectSection'
                }
              }
            }
          }
        }
      }
    };
    const nextState: any = await actionsWorkflowSetup.getPricingStrategyList(
      'CP'
    )(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          workflowSetup: { ...workflowSetupState },
          mapping: { ...mappingState }
        } as RootState),
      {
        workflowService: {
          getPricingStrategyListForWorkflowSetup: jest
            .fn()
            .mockResolvedValue(result)
        }
      }
    );
    expect(nextState.payload[0].name).toEqual(
      result.data.pricingStrategies[0].name
    );
  });

  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getPricingStrategyList.pending(
        'getPricingStrategyList',
        'CP'
      )
    );
    expect(nextState.strategyData.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getPricingStrategyList.fulfilled(
        [],
        'getOCSShellList',
        'CP'
      )
    );
    expect(nextState.strategyData.loading).toEqual(false);
    expect(nextState.strategyData.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getPricingStrategyList.rejected(
        new Error('mock error'),
        'getPricingStrategyList',
        'CP'
      )
    );
    expect(nextState.strategyData.loading).toEqual(false);
    expect(nextState.strategyData.error).toEqual(true);
  });
});
