import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types/workflowSetupState';
import get from 'lodash.get';
import set from 'lodash.set';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { parseStrategyList } from './helper';

export const setupManagePricingStrategiesAndMethodAssignmentsWorkflow =
  createAsyncThunk<any, WorkflowSetupThunkArg, ThunkAPIConfig>(
    'workflowSetup/setupManagePricingStrategiesAndMethodAssignmentsWorkflow',
    async ({ workflowSetupData }, thunkAPI) => {
      const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

      const state = thunkAPI.getState();

      const changeForm = state.workflowSetup?.workflowFormSteps.forms[
        'changeInformation'
      ] as FormChangeInfo;

      const strategyList =
        state.workflowSetup?.workflowFormSteps.forms['configureParameters']
          ?.strategyList;
      const updateRequest: UpdateflowInstanceData = {
        workflowInstanceId: changeForm.workflowInstanceId?.toString(),
        status: 'COMPLETE',
        data: {
          workflowSetupData,
          configurations: {
            strategies: parseStrategyList(strategyList)
          }
        }
      };

      await workflowService.updateWorkflowInstanceData(updateRequest);

      return { workflowSetupData };
    }
  );
export const setupManagePricingStrategiesAndMethodAssignmentsWorkflowBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    setupManagePricingStrategiesAndMethodAssignmentsWorkflow.pending,
    draftState => {
      set(draftState.workflowSetup, 'loading', true);
    }
  );
  builder.addCase(
    setupManagePricingStrategiesAndMethodAssignmentsWorkflow.fulfilled,
    (draftState, action) => {
      set(draftState.workflowSetup, 'loading', false);
      set(draftState.workflowSetup, 'error', false);
      set(
        draftState.workflowFormSteps,
        'workflowSetupData',
        action.payload?.workflowSetupData
      );
    }
  );
  builder.addCase(
    setupManagePricingStrategiesAndMethodAssignmentsWorkflow.rejected,
    draftState => {
      set(draftState.workflowSetup, 'loading', false);
      set(draftState.workflowSetup, 'error', true);
    }
  );
};
