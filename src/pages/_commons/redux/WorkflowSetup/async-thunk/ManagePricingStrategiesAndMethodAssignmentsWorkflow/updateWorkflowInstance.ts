import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { parseStrategyList } from './helper';

export const updateManagePricingStrategiesAndMethodAssignmentsWorkflow =
  createAsyncThunk<any, WorkflowSetupThunkArg, ThunkAPIConfig>(
    'workflowSetup/updateManagePricingStrategiesAndMethodAssignmentsWorkflow',
    async ({ workflowSetupData }, thunkAPI) => {
      const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

      const state = thunkAPI.getState();

      const changeForm = state.workflowSetup?.workflowFormSteps.forms[
        'changeInformation'
      ] as FormChangeInfo;
      const strategyList =
        state.workflowSetup?.workflowFormSteps.forms['configureParameters']
          ?.strategyList;

      const updateRequest: UpdateflowInstanceData = {
        workflowInstanceId: changeForm.workflowInstanceId?.toString(),
        status: 'INCOMPLETE',
        data: {
          workflowSetupData,
          configurations: {
            strategies: parseStrategyList(strategyList)
          }
        }
      };

      await workflowService.updateWorkflowInstanceData(updateRequest);

      return { workflowSetupData };
    }
  );
export const updateManagePricingStrategiesAndMethodAssignmentsWorkflowBuilder =
  (builder: ActionReducerMapBuilder<WorkflowSetupRootState>) => {
    builder.addCase(
      updateManagePricingStrategiesAndMethodAssignmentsWorkflow.pending,
      (draftState, action) => {
        set(draftState.workflowInstance, 'loading', true);
        set(draftState.workflowInstance, 'error', false);
      }
    );
    builder.addCase(
      updateManagePricingStrategiesAndMethodAssignmentsWorkflow.fulfilled,
      (draftState, action) => {
        set(draftState.workflowInstance, 'loading', false);
        set(draftState.workflowInstance, 'error', false);
        set(
          draftState.workflowFormSteps.forms['changeInformation'],
          'isCompletedWorkflow',
          false
        );
        set(
          draftState.workflowFormSteps,
          'workflowSetupData',
          action.payload?.workflowSetupData
        );
      }
    );
    builder.addCase(
      updateManagePricingStrategiesAndMethodAssignmentsWorkflow.rejected,
      (draftState, action) => {
        set(draftState.workflowInstance, 'loading', false);
        set(draftState.workflowInstance, 'error', true);
      }
    );
  };
