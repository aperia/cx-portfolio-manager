import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { mappingDataFromArray } from 'app/helpers';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';

export const getPricingStrategyList = createAsyncThunk<
  Strategy[],
  string,
  ThunkAPIConfig
>('workflowSetup/getPricingStrategyList', async (args, thunkAPI) => {
  const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

  // Take mapping model
  const state = thunkAPI.getState();
  const workflowStrategyMapping = state.mapping?.data?.workflowStrategy;

  const resp = await workflowService.getPricingStrategyListForWorkflowSetup(
    args
  );

  return mappingDataFromArray(
    resp?.data?.pricingStrategies,
    workflowStrategyMapping!
  ) as Strategy[];
});
export const getPricingStrategyListBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(getPricingStrategyList.pending, (draftState, action) => {
    set(draftState.strategyData, 'loading', true);
    set(draftState.strategyData, 'error', false);
  });
  builder.addCase(getPricingStrategyList.fulfilled, (draftState, action) => {
    set(draftState.strategyData, 'loading', false);
    set(draftState.strategyData, 'error', false);
    set(draftState.strategyData, 'strategyList', action.payload);
  });
  builder.addCase(getPricingStrategyList.rejected, (draftState, action) => {
    set(draftState.strategyData, 'loading', false);
    set(draftState.strategyData, 'error', true);
  });
};
