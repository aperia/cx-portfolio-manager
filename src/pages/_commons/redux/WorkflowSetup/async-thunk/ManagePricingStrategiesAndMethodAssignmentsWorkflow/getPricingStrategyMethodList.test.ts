import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

describe('async-thunk > ManagePricingStrategiesAndMethodAssignmentsWorkflow > getPricingStrategyMethodList', () => {
  const initReducer: any = {
    workflowInstance: {
      loading: false,
      error: false
    },
    workflowSetup: {
      workflow: {
        id: undefined
      }
    },
    workflowFormSteps: {
      forms: {
        changeInformation: {
          name: 'mock name',
          description: 'mock description',
          effectiveDate: new Date()
        }
      }
    },
    strategyData: {
      loading: false,
      error: false,
      strategyList: []
    }
  };

  it('thunk action > with existing changeId ', async () => {
    const result = {
      data: {
        methodList: [
          {
            name: '3ACZF52L',
            serviceSubjectSection: 'CP IC ID'
          },
          {
            name: 'FR0WDJ2R',
            serviceSubjectSection: 'CP IC ID'
          },
          {
            name: 'FR0WDJ2R',
            serviceSubjectSection: 'CP IC IN'
          }
        ]
      }
    };
    const workflowSetupState = {
      workflowSetup: { workflow: { id: undefined } },
      workflowFormSteps: {
        forms: {
          changeInformation: { workflowInstanceId: 'workflowInstanceId' },
          uploadFile: {},
          configureParameters: {}
        }
      }
    };

    const nextState: any =
      await actionsWorkflowSetup.getPricingStrategyMethodList([
        'CP IC ID',
        'CP IC IB'
      ])(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: { ...workflowSetupState }
          } as RootState),
        {
          workflowService: {
            getWorkflowMethods: jest.fn().mockResolvedValue(result)
          }
        }
      );
    expect(nextState.payload['CP_IC_ID']).toBeTruthy();
  });

  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getPricingStrategyMethodList.pending(
        'getPricingStrategyMethodList',
        ['CP IC ID', 'CP IC IB']
      )
    );
    expect(nextState.strategyData.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getPricingStrategyMethodList.fulfilled(
        {},
        'getPricingStrategyMethodList',
        ['CP IC ID', 'CP IC IB']
      )
    );
    expect(nextState.strategyData.loading).toEqual(false);
    expect(nextState.strategyData.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getPricingStrategyMethodList.rejected(
        new Error('mock error'),
        'getPricingStrategyMethodList',
        ['CP IC ID', 'CP IC IB']
      )
    );
    expect(nextState.strategyData.loading).toEqual(false);
    expect(nextState.strategyData.error).toEqual(true);
  });
});
