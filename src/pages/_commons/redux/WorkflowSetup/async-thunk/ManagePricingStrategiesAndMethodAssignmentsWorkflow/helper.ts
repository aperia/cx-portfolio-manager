import { StrategyListFormValues } from 'pages/WorkflowManagePricingStrategiesAndMethodAssignments/ConfigureParameters/types';

export const parseStrategyList = (list: StrategyListFormValues[]) => {
  return list.map(({ unique, ...rest }) => ({ id: '', ...rest }));
};
