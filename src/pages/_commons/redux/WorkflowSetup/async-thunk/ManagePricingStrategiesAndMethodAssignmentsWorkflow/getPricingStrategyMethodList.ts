import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import { set } from 'app/_libraries/_dls/lodash';
import get from 'lodash.get';
import { SERVICE_SUBJECT_SECTION_KY_MAPPING } from 'pages/WorkflowManagePricingStrategiesAndMethodAssignments/ConfigureParameters/constants';

export const groupByServiceSubject = (
  methodList: { name: string; serviceSubjectSection?: string }[]
) => {
  return methodList?.reduce((acc, method) => {
    const k = method.serviceSubjectSection;
    const pair = Object.entries(SERVICE_SUBJECT_SECTION_KY_MAPPING).find(
      ([, v]) => v === k
    );
    if (!pair) return acc;
    const newK = pair[0];
    if (acc[newK]) {
      acc[newK].push(method.name);
    } else {
      acc[newK] = [method.name];
    }
    return acc;
  }, {} as Record<string, string[]>);
};

export const getPricingStrategyMethodList = createAsyncThunk<
  Record<string, string[]>,
  string[],
  ThunkAPIConfig
>('workflowSetup/getPricingStrategyMethodList', async (args, thunkAPI) => {
  const { workflowService } = get(thunkAPI, 'extra') as APIMapping;
  const resp = await workflowService.getWorkflowMethods(args);
  return groupByServiceSubject(resp.data?.methodList);
});

export const getPricingStrategyMethodListBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    getPricingStrategyMethodList.pending,
    (draftState, action) => {
      set(draftState.strategyData, 'loading', true);
      set(draftState.strategyData, 'error', false);
    }
  );
  builder.addCase(
    getPricingStrategyMethodList.fulfilled,
    (draftState, action) => {
      set(draftState.strategyData, 'loading', false);
      set(draftState.strategyData, 'error', false);
      set(draftState.strategyData, 'methods', action.payload);
    }
  );
  builder.addCase(
    getPricingStrategyMethodList.rejected,
    (draftState, action) => {
      set(draftState.strategyData, 'loading', false);
      set(draftState.strategyData, 'error', true);
    }
  );
};
