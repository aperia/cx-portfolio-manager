export const parseForceBossParams = (config: MagicKeyValue) => {
  const code = config?.parameters?.action?.param?.standardCode?.code || '';
  const value = config?.parameters?.value;
  const isCheckNM65MailCode = config?.parameters?.action?.param?.isNM65MailCode;

  const listPID200 = ['pid.200.rush', 'pid.200.mass'];
  let data = [] as any;

  if (isCheckNM65MailCode) {
    data = [...data, { name: 'pid.194.transaction.id', value: 'nm.65' }];
  }
  if (code) {
    data = [
      ...data,
      {
        name: listPID200.includes(value) ? 'pid.200.code' : 'pid.194.code',
        value: code
      }
    ];
  }
  return value === 'nm.65' ? undefined : data;
};
