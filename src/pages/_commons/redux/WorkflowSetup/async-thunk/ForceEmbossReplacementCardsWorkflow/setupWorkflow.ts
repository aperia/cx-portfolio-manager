import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types/workflowSetupState';
import get from 'lodash.get';
import set from 'lodash.set';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';
import { parseForceBossParams } from './helper';

export const setupForceEmbossReplacementCardsWorkflow = createAsyncThunk<
  boolean,
  WorkflowSetupThunkArg,
  ThunkAPIConfig
>(
  'workflowSetup/setupForceEmbossReplacementCardsWorkflow',
  async ({ workflowSetupData }, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

    const state = thunkAPI.getState();

    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;

    const uploadFileForm = state.workflowSetup?.workflowFormSteps.forms[
      'uploadFile'
    ] as any;

    const workflowInstanceId = changeForm.workflowInstanceId?.toString();

    const { config } = state.workflowSetup?.workflowFormSteps.forms[
      'configureParameters'
    ] as any;

    const transactionId = config?.parameters?.value;

    const parameters = parseForceBossParams(config);

    const updateRequest = {
      workflowInstanceId,
      status: 'COMPLETE',
      data: {
        workflowSetupData,
        configurations: {
          attachmentId: uploadFileForm.attachmentId,
          transactionId,
          parameters
        }
      }
    };

    // upload file

    await workflowService.updateWorkflowInstanceData(updateRequest);

    return true;
  }
);
export const setupForceEmbossReplacementCardsWorkflowBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    setupForceEmbossReplacementCardsWorkflow.pending,
    draftState => {
      set(draftState.workflowSetup, 'loading', true);
    }
  );
  builder.addCase(
    setupForceEmbossReplacementCardsWorkflow.fulfilled,
    draftState => {
      set(draftState.workflowSetup, 'loading', false);
      set(draftState.workflowSetup, 'error', false);
    }
  );
  builder.addCase(
    setupForceEmbossReplacementCardsWorkflow.rejected,
    draftState => {
      set(draftState.workflowSetup, 'loading', false);
      set(draftState.workflowSetup, 'error', true);
    }
  );
};
