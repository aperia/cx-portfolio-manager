import { parseForceBossParams } from './helper';
describe('helper', () => {
  it('should parseForceBossParams', () => {
    const config = {
      parameters: {
        value: 'pid.200.rush',
        action: {
          param: { standardCode: { code: '12' }, isNM65MailCode: true }
        }
      }
    };
    expect(parseForceBossParams(config)).toBeTruthy();
  });
  it('should parseForceBossParams', () => {
    const config = {
      parameters: {
        value: 'nm.65',
        action: {
          param: { standardCode: { code: '12' }, isNM65MailCode: true }
        }
      }
    };
    expect(parseForceBossParams(config)).toBeUndefined();
  });
});
