import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  workflowInstanceStatus: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      changeInformation: {
        changeSetId: '1111',
        workflowInstanceId: '2222',
        name: 'mock name',
        description: 'mock description',
        workflowNote: 'mock note',
        effectiveDate: new Date()
      },
      configureParameters: {
        attachmentId: 'attachmentId',
        date: {
          minDate: '10/01/2021',
          maxDate: '10/31/2021'
        }
      },
      uploadFile: {
        attachmentId: 'attachmentId'
      }
    }
  }
};

describe('redux-store > workflow-setup > AssignPricingStrategiesWorkflow > setupWorkflow', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.setupForceEmbossReplacementCardsWorkflow.pending(
        'setupWorkflow',
        undefined
      )
    );
    expect(nextState.workflowSetup.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.setupForceEmbossReplacementCardsWorkflow.fulfilled(
        true,
        'setupWorkflow',
        {}
      )
    );
    expect(nextState.workflowSetup.loading).toEqual(false);
    expect(nextState.workflowSetup.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.setupForceEmbossReplacementCardsWorkflow.rejected(
        new Error('mock error'),
        'setupWorkflow',
        undefined
      )
    );
    expect(nextState.workflowSetup.loading).toEqual(false);
    expect(nextState.workflowSetup.error).toEqual(true);
  });

  it('thunk action > with workflowInstanceId & changeId', async () => {
    const updateWorkflowDataFn = jest.fn();

    const nextState: any =
      await actionsWorkflowSetup.setupForceEmbossReplacementCardsWorkflow('')(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer
            }
          } as RootState),
        {
          workflowService: {
            updateWorkflowInstanceData: updateWorkflowDataFn
          }
        }
      );
    expect(nextState.payload).toEqual(true);

    expect(updateWorkflowDataFn).toHaveBeenCalled();
  });
});
