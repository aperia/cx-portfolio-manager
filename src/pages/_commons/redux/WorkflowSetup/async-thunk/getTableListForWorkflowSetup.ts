import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import { get, set, sortBy } from 'app/_libraries/_dls/lodash';

export const getTableListForWorkflowSetup = createAsyncThunk<
  any,
  string,
  ThunkAPIConfig
>(
  'workflowSetup/getTableListForWorkflowSetup',
  async (tableType: string, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

    const result = await workflowService.getTableListForWorkflowSetup(
      tableType
    );

    const { dmmTables } = result?.data || [];

    return dmmTables.length === 1
      ? dmmTables
      : [
          { tableId: 'All', tableName: 'All' },
          ...sortBy(dmmTables, ['tableId', 'asc'])
        ];
  }
);

export const getTableListForWorkflowSetupBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    getTableListForWorkflowSetup.pending,
    (draftState, action) => {
      set(draftState.tableListWorkflowSetup, 'loading', true);
      set(draftState.tableListWorkflowSetup, 'data', []);
      set(draftState.tableListWorkflowSetup, 'error', false);
    }
  );
  builder.addCase(
    getTableListForWorkflowSetup.fulfilled,
    (draftState, action) => {
      const data = action.payload;
      set(draftState.tableListWorkflowSetup, 'loading', false);
      set(draftState.tableListWorkflowSetup, 'data', data);
      set(draftState.tableListWorkflowSetup, 'total', data.length);
    }
  );
  builder.addCase(
    getTableListForWorkflowSetup.rejected,
    (draftState, action) => {
      set(draftState.tableListWorkflowSetup, 'loading', false);
      set(draftState.tableListWorkflowSetup, 'data', []);
      set(draftState.tableListWorkflowSetup, 'total', 0);
      set(draftState.tableListWorkflowSetup, 'error', true);
    }
  );
};
