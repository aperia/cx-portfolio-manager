import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  workflowInstance: {}
};

const mockWorkflowInstanceId = '1111';

describe('redux-store > workflow-setup > getWorkflowInstance', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getWorkflowInstance.pending(
        'getWorkflowInstance',
        mockWorkflowInstanceId
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getWorkflowInstance.fulfilled(
        {
          data: {
            workflowSetupData: [{ id: '1' }]
          },
          workflowInstanceDetails: { id: '1', changeSetId: '1' }
        } as any,
        'getWorkflowInstance',
        mockWorkflowInstanceId
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.getWorkflowInstance.rejected(
        new Error('mock error'),
        'getWorkflowInstance',
        mockWorkflowInstanceId
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(true);
  });

  it('thunk action > with workflow instance data data', async () => {
    const nextState: any = await actionsWorkflowSetup.getWorkflowInstance(
      mockWorkflowInstanceId
    )(store.dispatch, () => store.getState(), {
      workflowService: {
        getWorkflowInstance: jest.fn().mockResolvedValue({
          data: {
            workflowInstanceId: mockWorkflowInstanceId,
            name: 'mock name'
          }
        })
      }
    });
    expect(nextState.payload.workflowInstanceId).toEqual(
      mockWorkflowInstanceId
    );
    expect(nextState.payload.name).toEqual('mock name');
  });
});
