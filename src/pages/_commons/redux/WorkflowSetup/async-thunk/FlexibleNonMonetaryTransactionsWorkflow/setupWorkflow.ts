import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types/workflowSetupState';
import get from 'lodash.get';
import set from 'lodash.set';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';

export const setupFlexibleNonMonetaryTransactionsWorkflow = createAsyncThunk<
  boolean,
  WorkflowSetupThunkArg,
  ThunkAPIConfig
>(
  'workflowSetup/setupFlexibleNonMonetaryTransactionsWorkflow',
  async ({ workflowSetupData }, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

    const state = thunkAPI.getState();

    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;

    const uploadFileForm = state.workflowSetup?.workflowFormSteps.forms[
      'uploadFile'
    ] as any;
    const { transactions } = state.workflowSetup?.workflowFormSteps.forms[
      'selectNonMonetaryTransactions'
    ] as any;

    const updateRequest = {
      workflowInstanceId: changeForm.workflowInstanceId?.toString(),
      status: 'COMPLETE',
      data: {
        workflowSetupData,
        configurations: {
          attachmentId: uploadFileForm.attachmentId,
          transactions
        }
      }
    };

    await workflowService.updateWorkflowInstanceData(updateRequest);

    return true;
  }
);
export const setupFlexibleNonMonetaryTransactionsWorkflowBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    setupFlexibleNonMonetaryTransactionsWorkflow.pending,
    draftState => {
      set(draftState.workflowSetup, 'loading', true);
    }
  );
  builder.addCase(
    setupFlexibleNonMonetaryTransactionsWorkflow.fulfilled,
    draftState => {
      set(draftState.workflowSetup, 'loading', false);
      set(draftState.workflowSetup, 'error', false);
    }
  );
  builder.addCase(
    setupFlexibleNonMonetaryTransactionsWorkflow.rejected,
    draftState => {
      set(draftState.workflowSetup, 'loading', false);
      set(draftState.workflowSetup, 'error', true);
    }
  );
};
