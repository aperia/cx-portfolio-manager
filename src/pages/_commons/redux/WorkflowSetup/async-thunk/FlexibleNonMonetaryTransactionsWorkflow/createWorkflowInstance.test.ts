import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  workflowInstance: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      changeInformation: {
        name: 'mock name',
        description: 'mock description',
        effectiveDate: new Date()
      }
    }
  }
};

jest.mock('app/helpers', () => {
  const modules = jest.requireActual('app/helpers');
  return {
    ...modules,
    formatTimeDefault: () => '01/01/2021'
  };
});

describe('redux-store > workflow-setup > FlexibleNonMonetaryTransactionsWorkflow > createWorkflowInstance', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.createFlexibleNonMonetaryTransactionsWorkflowInstance.pending(
        'createWorkflowInstance',
        undefined
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.createFlexibleNonMonetaryTransactionsWorkflowInstance.fulfilled(
        {},
        'createWorkflowInstance',
        {}
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.createFlexibleNonMonetaryTransactionsWorkflowInstance.rejected(
        new Error('mock error'),
        'createWorkflowInstance',
        undefined
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(true);
  });

  it('thunk action > without workflowInstanceId & changeId', async () => {
    const nextState: any =
      await actionsWorkflowSetup.createFlexibleNonMonetaryTransactionsWorkflowInstance(
        { workflowSetupData: [] }
      )(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer,
              workflowFormSteps: {
                forms: {
                  changeInformation: {
                    changeSetId: '2222',
                    name: 'mock name',
                    description: 'mock description',
                    effectiveDate: new Date()
                  },
                  gettingStart: {
                    returnedCheckCharges: true,
                    lateCharges: true
                  },
                  selectNonMonetaryTransactions: {
                    transactions: []
                  },
                  uploadFile: {}
                }
              }
            }
          } as RootState),
        {
          workflowService: {
            createWorkflowInstance: jest.fn().mockResolvedValue({
              data: { workflowInstanceDetails: { id: '1111' } }
            }),
            updateWorkflowInstanceData: jest.fn()
          }
        }
      );

    expect(nextState.payload).toEqual({
      changeId: '2222',
      workflowInstanceId: '1111'
    });
  });

  it('thunk action > with workflowInstanceId & changeId', async () => {
    const state = {
      workflowSetup: {
        workflow: {
          id: undefined
        }
      },
      workflowFormSteps: {
        forms: {
          changeInformation: {
            changeSetId: '2222',
            workflowInstanceId: '1111',
            name: 'mock name',
            description: 'mock description',
            effectiveDate: new Date()
          },
          gettingStart: {
            returnedCheckCharges: true,
            lateCharges: true
          },
          selectNonMonetaryTransactions: {
            transactions: []
          },
          uploadFile: {}
        }
      }
    };
    const nextState: any =
      await actionsWorkflowSetup.createFlexibleNonMonetaryTransactionsWorkflowInstance(
        { workflowSetupData: [] }
      )(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...state
            }
          } as RootState),
        {
          workflowService: {
            updateWorkflowInstance: jest.fn(),
            updateWorkflowInstanceData: jest.fn()
          }
        }
      );
    expect(nextState.payload).toEqual({
      changeId: '2222',
      workflowInstanceId: '1111'
    });
  });
});
