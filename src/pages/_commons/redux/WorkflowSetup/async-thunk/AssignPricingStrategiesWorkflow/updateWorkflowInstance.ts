import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';

export const updateAssignPricingStrategiesWorkflowInstance = createAsyncThunk<
  boolean,
  WorkflowSetupThunkArg,
  ThunkAPIConfig
>(
  'workflowSetup/updateAssignPricingStrategiesWorkflowInstance',
  async ({ workflowSetupData }, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;
    const state = thunkAPI.getState();

    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;

    const uploadFileForm = state.workflowSetup?.workflowFormSteps.forms[
      'uploadFile'
    ] as any;

    const { parameters } = state.workflowSetup?.workflowFormSteps.forms[
      'configureParameters'
    ] as any;

    const updateRequest = {
      workflowInstanceId: changeForm.workflowInstanceId?.toString(),
      status: 'INCOMPLETE',
      data: {
        workflowSetupData,
        configurations: {
          attachmentId: uploadFileForm.attachmentId,
          ...parameters
        }
      }
    };

    await workflowService.updateWorkflowInstanceData(updateRequest);

    return true;
  }
);
export const updateAssignPricingStrategiesWorkflowInstanceBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(
    updateAssignPricingStrategiesWorkflowInstance.pending,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', true);
      set(draftState.workflowInstance, 'error', false);
    }
  );
  builder.addCase(
    updateAssignPricingStrategiesWorkflowInstance.fulfilled,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', false);
      set(
        draftState.workflowFormSteps.forms['changeInformation'],
        'isCompletedWorkflow',
        false
      );
    }
  );
  builder.addCase(
    updateAssignPricingStrategiesWorkflowInstance.rejected,
    (draftState, action) => {
      set(draftState.workflowInstance, 'loading', false);
      set(draftState.workflowInstance, 'error', true);
    }
  );
};
