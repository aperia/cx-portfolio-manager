import { GetMethodListDataMapping } from 'app/fixtures/workflow-method-list';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';

const initReducer: any = {
  workflowInstance: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      changeInformation: {
        changeSetId: '1111',
        workflowInstanceId: '2222',
        name: 'mock name',
        description: 'mock description',
        workflowNote: 'mock note',
        effectiveDate: new Date()
      },
      configureParameters: {
        attachmentId: 'attachmentId',
        date: {
          minDate: '10/01/2021',
          maxDate: '10/31/2021'
        },
        override: 'true',
        reason: 'reasonCode'
      },
      uploadFile: {
        attachmentId: 'attachmentId'
      }
    }
  }
};

describe('redux-store > workflow-setup > ManagePenaltyFeeWorkflow > getWorkflowEffectiveDateOptions', () => {
  it('thunk action ', async () => {
    const nextState: any =
      await actionsWorkflowSetup.getWorkflowEffectiveDateOptions({
        serviceSubjectSection: 'mock data',
        defaultMethodId: '1'
      })(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...initReducer
            },
            mapping: { data: { workflowMethodList: GetMethodListDataMapping } }
          } as RootState),
        {
          workflowService: {
            getWorkflowEffectiveDateOptions: jest.fn().mockResolvedValue({
              data: { maxDate: '10/30/2021', minDate: '10/01/2021' }
            })
          }
        }
      );
    expect(nextState.payload).toEqual({
      data: {
        maxDate: '10/30/2021',
        minDate: '10/01/2021'
      }
    });
  });
});
