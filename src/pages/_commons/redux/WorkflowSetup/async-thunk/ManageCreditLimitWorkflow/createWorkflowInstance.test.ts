import { mockThunkAction } from 'app/utils';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import {
  actionsWorkflowSetup,
  reducer
} from 'pages/_commons/redux/WorkflowSetup';
import * as UpdateManageCreditLimitWorkflowInstance from './updateWorkflowInstance';

const initReducer: any = {
  workflowInstance: {
    loading: false,
    error: false
  },
  workflowSetup: {
    workflow: {
      id: undefined
    }
  },
  workflowFormSteps: {
    forms: {
      uploadFile: {
        attachmentId: 'attachmentId'
      },
      changeInformation: {
        name: 'mock name',
        description: 'mock description',
        effectiveDate: new Date()
      },
      configureParameters: {
        dateRange: ['11/11/2021', '12/12/2021'],
        checked: '0',
        reasonCode: 'reasonCode'
      }
    }
  }
};

jest.mock('app/helpers', () => {
  const modules = jest.requireActual('app/helpers');
  return {
    ...modules,
    formatTimeDefault: () => '01/01/2021'
  };
});

const mockUpdateManageCreditLimitWorkflowInstance = mockThunkAction(
  UpdateManageCreditLimitWorkflowInstance
);

describe('redux-store > workflow-setup > ManageCreditLimitWorkflow > createWorkflowInstance', () => {
  beforeEach(() => {
    mockUpdateManageCreditLimitWorkflowInstance(
      'updateManageCreditLimitWorkflowInstance'
    );
  });
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.createManageCreditLimitWorkflowInstance.pending(
        'createWorkflowInstance',
        {}
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.createManageCreditLimitWorkflowInstance.fulfilled(
        {},
        'createWorkflowInstance',
        {}
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsWorkflowSetup.createManageCreditLimitWorkflowInstance.rejected(
        new Error('mock error'),
        'createWorkflowInstance',
        {}
      )
    );
    expect(nextState.workflowInstance.loading).toEqual(false);
    expect(nextState.workflowInstance.error).toEqual(true);
  });

  it('thunk action > without workflowInstanceId & changeId', async () => {
    const nextState: any =
      await actionsWorkflowSetup.createManageCreditLimitWorkflowInstance({
        workflowSetupData: []
      })(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: { ...initReducer }
          } as RootState),
        {
          workflowService: {
            createWorkflowInstance: jest.fn().mockResolvedValue({
              data: { workflowInstanceDetails: { id: '1111' } }
            }),
            updateWorkflowInstanceData: jest.fn()
          },
          changeService: {
            createChangeSet: jest.fn().mockResolvedValue({
              data: { id: undefined }
            })
          }
        }
      );

    expect(nextState.payload).toEqual({
      changeId: undefined,
      workflowInstanceId: '1111',
      workflowSetupData: []
    });
  });

  it('thunk action > with workflowInstanceId & changeId', async () => {
    const state = {
      workflowSetup: {
        workflow: {
          id: undefined
        }
      },
      workflowFormSteps: {
        forms: {
          changeInformation: {
            changeSetId: '2222',
            workflowInstanceId: '1111',
            name: 'mock name',
            description: 'mock description',
            effectiveDate: new Date()
          },
          gettingStart: {
            returnedCheckCharges: true,
            lateCharges: true
          }
        }
      }
    };
    const nextState: any =
      await actionsWorkflowSetup.createManageCreditLimitWorkflowInstance({
        workflowSetupData: []
      })(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...state
            }
          } as RootState),
        {
          workflowService: {
            updateWorkflowInstance: jest.fn(),
            updateWorkflowInstanceData: jest.fn()
          }
        }
      );
    expect(nextState.payload).toBeUndefined();
  });

  it('thunk action > with existing changeId ', async () => {
    const state = {
      workflowSetup: {
        workflow: {
          id: undefined
        }
      },
      workflowFormSteps: {
        forms: {
          changeInformation: {
            changeSetId: '2222',
            name: 'mock name',
            description: 'mock description',
            effectiveDate: new Date()
          }
        }
      }
    };
    const nextState: any =
      await actionsWorkflowSetup.createManageCreditLimitWorkflowInstance({
        workflowSetupData: []
      })(
        store.dispatch,
        () =>
          ({
            ...store.getState(),
            workflowSetup: {
              ...state
            }
          } as RootState),
        {
          workflowService: {
            createWorkflowInstance: jest.fn().mockResolvedValue({
              data: { workflowInstanceDetails: { id: '1111' } }
            }),
            updateWorkflowInstanceData: jest
              .fn()
              .mockResolvedValue({ data: { workflowSetupData: [] } })
          }
        }
      );
    expect(nextState.payload).toBeUndefined();
  });
});
