import { createAsyncThunk } from '@reduxjs/toolkit';
import { APIMapping } from 'app/services';
import get from 'lodash.get';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';

export const getWorkflowEffectiveDateOptions = createAsyncThunk<
  any,
  any,
  ThunkAPIConfig
>('workflowSetup/getWorkflowEffectiveDateOptions', async (args, thunkAPI) => {
  const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

  const state = thunkAPI.getState();

  const changeForm = state.workflowSetup?.workflowFormSteps.forms[
    'changeInformation'
  ] as FormChangeInfo;

  const response = await workflowService.getWorkflowEffectiveDateOptions({
    selectFields: ['minDate', 'maxDate', 'invalidDates'],
    searchFields: {
      workflowInstanceId: changeForm.workflowInstanceId
    }
  });

  return response;
});
