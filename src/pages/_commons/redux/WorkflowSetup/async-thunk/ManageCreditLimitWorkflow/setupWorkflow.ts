import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { formatCommon } from 'app/helpers';
import { APIMapping } from 'app/services';
import { WorkflowSetupRootState } from 'app/types/workflowSetupState';
import get from 'lodash.get';
import set from 'lodash.set';
import { FormChangeInfo } from 'pages/_commons/WorkflowSetup/CommonSteps/ChangeInfoStep';

export const setupManageCreditLimitWorkflow = createAsyncThunk<
  any,
  WorkflowSetupThunkArg,
  ThunkAPIConfig
>(
  'workflowSetup/setupManageCreditLimitWorkflow',
  async ({ workflowSetupData }, thunkAPI) => {
    const { workflowService } = get(thunkAPI, 'extra') as APIMapping;

    const state = thunkAPI.getState();

    const changeForm = state.workflowSetup?.workflowFormSteps.forms[
      'changeInformation'
    ] as FormChangeInfo;

    const uploadFileForm = state.workflowSetup?.workflowFormSteps.forms[
      'uploadFile'
    ] as any;

    const workflowInstanceId = changeForm.workflowInstanceId?.toString();

    const { dateRange, checked, reasonCode } = state.workflowSetup
      ?.workflowFormSteps.forms['configureParameters'] as any;

    const updateRequest = {
      workflowInstanceId,
      status: 'COMPLETE',
      data: {
        workflowSetupData,
        configurations: {
          attachmentId: uploadFileForm.attachmentId,
          date: {
            minDate: formatCommon(dateRange?.start).time.date,
            maxDate: formatCommon(dateRange?.end).time.date
          },
          override: checked?.toString(),
          reason: reasonCode
        }
      }
    };

    // upload file

    await workflowService.updateWorkflowInstanceData(updateRequest);

    return true;
  }
);
export const setupManageCreditLimitWorkflowBuilder = (
  builder: ActionReducerMapBuilder<WorkflowSetupRootState>
) => {
  builder.addCase(setupManageCreditLimitWorkflow.pending, draftState => {
    set(draftState.workflowSetup, 'loading', true);
  });
  builder.addCase(setupManageCreditLimitWorkflow.fulfilled, draftState => {
    set(draftState.workflowSetup, 'loading', false);
    set(draftState.workflowSetup, 'error', false);
  });
  builder.addCase(setupManageCreditLimitWorkflow.rejected, draftState => {
    set(draftState.workflowSetup, 'loading', false);
    set(draftState.workflowSetup, 'error', true);
  });
};
