import { renderHook } from '@testing-library/react-hooks';
import { DMMTablesData, DMMTablesdMapping } from 'app/fixtures/dmm-tables-data';
import { mappingDataFromArray } from 'app/helpers';
import {
  defaultDataFilter,
  DMMTablesState,
  useSelectDMMTables
} from 'pages/_commons/redux/DMMTables';
import * as reactRedux from 'react-redux';

describe('redux-store > DMMTables > selector', () => {
  let mockDmmTablesData: any[] = [];
  let selectorMock: jest.SpyInstance;
  const mockSelectorDmmTables = (dmmTablesState: DMMTablesState) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector => selector({ dmmTables: dmmTablesState }));
  };
  beforeEach(() => {
    mockDmmTablesData = mappingDataFromArray(DMMTablesData, DMMTablesdMapping);
  });
  afterEach(() => {
    selectorMock && selectorMock.mockClear();
  });

  it('useSelectDMMTables ', () => {
    const pre = {
      changeDMMTables: {
        dmmTablesList: mockDmmTablesData,
        dataFilter: defaultDataFilter
      }
    } as any;

    mockSelectorDmmTables(pre);

    const { result } = renderHook(() => useSelectDMMTables());

    expect(result.current.totalItem).toEqual(2);
  });
});
