import { FIRST_PAGE, PAGE_SIZE } from 'app/constants/constants';
import { DMMTablesStateSortByFields, OrderBy } from 'app/constants/enums';

export interface DMMTablesState {
  changeDMMTables: ChangeDMMTables;
  downloadDecisionElement: DownloadAttachment;
}

export interface ChangeDMMTables {
  dmmTablesList: IDMMData[];
  loading: boolean;
  error?: boolean;
  dataFilter?: IDataFilter;
}

export const defaultSortBy: string[] = [DMMTablesStateSortByFields.TABLE_NAME];

export const defaultOrderBy: OrderByType[] = [OrderBy.ASC];

export const defaultDataFilter: IDataFilter = {
  page: FIRST_PAGE,
  pageSize: PAGE_SIZE[0],
  searchValue: '',
  sortBy: [...defaultSortBy],
  orderBy: [...defaultOrderBy]
};

export interface DownloadAttachment extends IFetching {
  attachment: IDownloadAttachment;
}
