import { createSelector } from '@reduxjs/toolkit';
import {
  MethodFieldParameterEnum,
  TableFieldParameterEnum
} from 'app/constants/enums';
import { orderBySelectedField } from 'app/helpers';
import { isEqual, uniqWith } from 'lodash';
import includes from 'lodash.includes';
import isEmpty from 'lodash.isempty';
import { useSelector } from 'react-redux';

const dmmTablesSelector = createSelector(
  (state: RootState) => state.dmmTables!.changeDMMTables,
  data => {
    const {
      dmmTablesList: rawDMMTablesList,
      loading,
      error = false,
      dataFilter = {}
    } = data;
    let totalItemFilter = 0;
    const { page, pageSize, sortBy, orderBy, searchValue, searchFields } =
      dataFilter;

    const dmmTablesList = orderBySelectedField(
      rawDMMTablesList.map((item, index) => {
        // Add tableTypeFilter field
        const tableTypes = item.dmmTables.map(t => t.tableType);
        const tableTypeFilter: ITableType[] = uniqWith(tableTypes, isEqual);

        // Handle search and filter
        let dmmTables = orderBySelectedField(
          item.dmmTables.map(dmm => ({ ...dmm })),
          sortBy!,
          orderBy!
        ).filter(
          s =>
            s.tableId
              ?.toLowerCase()
              .includes(searchValue?.trim().toLowerCase() || '') ||
            s.tableName
              ?.toLowerCase()
              .includes(searchValue?.trim().toLowerCase() || '') ||
            s.modeledFrom
              ?.toLowerCase()
              .includes(searchValue?.trim().toLowerCase() || '') ||
            s.newVersionOf
              ?.toLowerCase()
              .includes(searchValue?.trim().toLowerCase() || '')
        );

        if (!isEmpty(searchFields)) {
          const searchFieldsLowerCase = searchFields.map((i: any) =>
            i?.toLowerCase()
          );

          if (!includes(searchFieldsLowerCase, 'all')) {
            dmmTables = dmmTables.filter(i =>
              includes(searchFieldsLowerCase, i.tableType.value?.toLowerCase())
            );
          }
        }

        // Paging
        totalItemFilter = dmmTables.length;
        dmmTables = dmmTables.slice((page! - 1) * pageSize!, page! * pageSize!);

        return {
          ...item,
          id: `${index + 1}`,
          dmmTables: dmmTables,
          totalItemFilter: totalItemFilter,
          tableTypeFilter: tableTypeFilter
        };
      }),
      ['serviceName'],
      ['asc']
    );

    return {
      loading,
      error,
      totalItem: rawDMMTablesList.length,
      dmmList: dmmTablesList
    };
  }
);

export const useSelectDMMTables = () => {
  return useSelector<
    RootState,
    {
      dmmList: IDMMData[];
      loading: boolean;
      error: boolean;
      totalItem: number;
    }
  >(dmmTablesSelector);
};

const elementMetadataDMMTablesSelector = createSelector(
  (state: RootState) =>
    state.workflowSetup!.elementMetadataNonWorkflow.elements,
  data => {
    const elements = data.map(e => {
      if (e.name === MethodFieldParameterEnum.AllocationInterval) {
        return {
          name: e.name,
          options: e.options?.map(otp => {
            if (isEmpty(otp?.value)) {
              return {
                code: otp?.value,
                text: otp?.description
              };
            }
            return {
              code: otp?.value,
              text: otp?.value
            };
          })
        };
      }
      return {
        name: e.name,
        options: e.options?.map(otp => {
          if (isEmpty(otp?.value)) {
            return {
              code: otp?.value,
              text: otp?.description
            };
          }
          return {
            code: otp?.value,
            text: `${otp?.value} - ${otp?.description}`
          };
        })
      };
    });

    const searchCode =
      elements.find(e => e.name === MethodFieldParameterEnum.SearchCode)
        ?.options || [];
    const allocationFlag =
      elements.find(e => e.name === MethodFieldParameterEnum.AllocationFlag)
        ?.options || [];
    const allocationBeforeAfterCycle =
      elements.find(
        e => e.name === MethodFieldParameterEnum.AllocationBeforeAfterCycle
      )?.options || [];
    const changeInTermsMethodFlag =
      elements.find(
        e => e.name === MethodFieldParameterEnum.ChangeInTermsMethodFlag
      )?.options || [];
    const allocationInterval =
      elements.find(e => e.name === MethodFieldParameterEnum.AllocationInterval)
        ?.options || [];
    const caAllocationFlag =
      elements.find(e => e.name === TableFieldParameterEnum.CAAllocationFlag)
        ?.options || [];
    const aqAllocationFlag =
      elements.find(e => e.name === TableFieldParameterEnum.AQAllocationFlag)
        ?.options || [];
    const posPromoValidationOptions =
      elements.find(
        e => e.name === MethodFieldParameterEnum.PosPromotionValidation
      )?.options || [];
    const includeActivityOptions =
      elements.find(
        e => e.name === MethodFieldParameterEnum.IncludeActivityAction
      )?.options || [];

    return {
      searchCode,
      allocationFlag,
      allocationBeforeAfterCycle,
      allocationInterval,
      changeInTermsMethodFlag,
      caAllocationFlag,
      aqAllocationFlag,
      posPromoValidationOptions,
      includeActivityOptions
    };
  }
);

export const useSelectElementMetadataDMMTables = () => {
  return useSelector<
    RootState,
    {
      searchCode: any[];
      allocationFlag: any[];
      allocationBeforeAfterCycle: any[];
      allocationInterval: any[];
      changeInTermsMethodFlag: any[];
      caAllocationFlag: any[];
      aqAllocationFlag: any[];
      posPromoValidationOptions: any[];
      includeActivityOptions: any[];
    }
  >(elementMetadataDMMTablesSelector);
};

export const useSelectDMMTablesFilter = () => {
  const DMMTablesFilterSelector = createSelector(
    (state: RootState) => state.dmmTables!.changeDMMTables.dataFilter!,
    data => data
  );
  return useSelector<RootState, IDataFilter>(DMMTablesFilterSelector);
};
