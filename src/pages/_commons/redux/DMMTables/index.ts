import { createSlice } from '@reduxjs/toolkit';
import {
  downloadDecisionElement,
  downloadDecisionElementBuilder
} from './async-thunk/downloadDecisionElement';
import { getDMMTables, getDMMTablesBuilder } from './async-thunk/getDMMTables';
import reducers from './reducers';
import { defaultDataFilter, DMMTablesState } from './types';

const defaultState: DMMTablesState = {
  changeDMMTables: {
    dmmTablesList: [],
    loading: true,
    dataFilter: defaultDataFilter
  },
  downloadDecisionElement: {
    attachment: {} as IDownloadAttachment,
    error: false,
    loading: false
  }
};

const { actions, reducer } = createSlice({
  name: 'dmmTables',
  initialState: defaultState,
  reducers,
  extraReducers: builder => {
    getDMMTablesBuilder(builder);
    downloadDecisionElementBuilder(builder);
  }
});

const extraActions = {
  ...actions,
  getDMMTables,
  downloadDecisionElement
};

export * from './selector';
export * from './types';
export { extraActions as actionsDMMTables, reducer };
