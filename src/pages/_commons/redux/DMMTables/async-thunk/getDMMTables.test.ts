import store from 'app/_libraries/_dof/core/redux/createAppStore';
import { actionsDMMTables, reducer } from 'pages/_commons/redux/DMMTables';

const initReducer: any = {
  changeDMMTables: {
    dmmTablesList: [
      {
        id: '0111',
        serviceName: 'name',
        abbr: 'abbr',
        dmmTables: [
          {
            id: 'dmmTables001',
            name: 'dmmTablesName',
            comment: 'comment',
            status: 'status',
            accessible: true,
            tableType: {},
            decisionElementChanges: {}
          }
        ]
      }
    ],
    dataFilter: {},
    loading: false,
    error: false
  }
};

describe('redux-store > DMMTables > getDMMTables', () => {
  const mockChangeId = '123';

  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsDMMTables.getDMMTables.pending('getDMMTables', mockChangeId)
    );
    expect(nextState.changeDMMTables.loading).toEqual(true);
  });
  it('fulfilled -> isCancel', () => {
    const nextState = reducer(
      initReducer,
      actionsDMMTables.getDMMTables.fulfilled(
        { isCancel: true } as any,
        'getDMMTables',
        mockChangeId
      )
    );
    expect(nextState.changeDMMTables.loading).toEqual(false);
  });

  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsDMMTables.getDMMTables.rejected(null, 'getDMMTables', mockChangeId)
    );
    expect(nextState.changeDMMTables.loading).toEqual(false);
  });

  it('thunk action > DMMServiceList', async () => {
    const nextState: any = await actionsDMMTables.getDMMTables('123')(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: {
            data: {
              dmmList: {
                id: 'id',
                serviceName: 'serviceName',
                abbr: 'abbreviation',
                dmmTables: {
                  type: 'mappingDataFromArray',
                  containerProp: 'dmmTables',
                  dmmTables: {
                    id: 'id',
                    name: 'name',
                    comment: 'comment',
                    status: 'resultingAction',
                    accessible: 'downloadAccessible',
                    tableType: 'tableType',
                    decisionElementChanges: 'decisionElementMetaData',
                    allocationDate: {
                      type: 'dynamic',
                      containerProp: 'tableControlFields',
                      fieldProp: 'allocationDate',
                      mappingToType: 'string',
                      key: 'fieldName',
                      value: 'value'
                    },
                    allocationFlag: {
                      type: 'dynamic',
                      containerProp: 'tableControlFields',
                      fieldProp: 'allocationFlag',
                      mappingToType: 'string',
                      key: 'fieldName',
                      value: 'value'
                    },
                    allocationInterval: {
                      type: 'dynamic',
                      containerProp: 'tableControlFields',
                      fieldProp: 'allocationInterval',
                      mappingToType: 'string',
                      key: 'fieldName',
                      value: 'value'
                    },
                    allocationBACycle: {
                      type: 'dynamic',
                      containerProp: 'tableControlFields',
                      fieldProp: 'allocationBACycle',
                      mappingToType: 'string',
                      key: 'fieldName',
                      value: 'value'
                    },
                    changeCode: {
                      type: 'dynamic',
                      containerProp: 'tableControlFields',
                      fieldProp: 'changeCode',
                      mappingToType: 'string',
                      key: 'fieldName',
                      value: 'value'
                    },
                    citMethodFlag: {
                      type: 'dynamic',
                      containerProp: 'tableControlFields',
                      fieldProp: 'citMethodFlag',
                      mappingToType: 'string',
                      key: 'fieldName',
                      value: 'value'
                    }
                  }
                }
              }
            }
          }
        } as RootState),
      {
        changeService: {
          getDMMTables: jest.fn().mockResolvedValue({
            data: { DMMServiceList: initReducer }
          })
        }
      }
    );

    expect(nextState.meta.arg).toEqual('123');
  });

  it('thunk action > dmmServiceList', async () => {
    const nextState: any = await actionsDMMTables.getDMMTables('123')(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: {
            data: {
              dmmList: {
                id: 'id',
                serviceName: 'serviceName',
                abbr: 'abbreviation',
                dmmTables: {
                  type: 'mappingDataFromArray',
                  containerProp: 'dmmTables',
                  dmmTables: {
                    id: 'id',
                    name: 'name',
                    comment: 'comment',
                    status: 'resultingAction',
                    accessible: 'downloadAccessible',
                    tableType: 'tableType',
                    decisionElementChanges: 'decisionElementMetaData',
                    allocationDate: {
                      type: 'dynamic',
                      containerProp: 'tableControlFields',
                      fieldProp: 'allocationDate',
                      mappingToType: 'string',
                      key: 'fieldName',
                      value: 'value'
                    },
                    allocationFlag: {
                      type: 'dynamic',
                      containerProp: 'tableControlFields',
                      fieldProp: 'allocationFlag',
                      mappingToType: 'string',
                      key: 'fieldName',
                      value: 'value'
                    },
                    allocationInterval: {
                      type: 'dynamic',
                      containerProp: 'tableControlFields',
                      fieldProp: 'allocationInterval',
                      mappingToType: 'string',
                      key: 'fieldName',
                      value: 'value'
                    },
                    allocationBACycle: {
                      type: 'dynamic',
                      containerProp: 'tableControlFields',
                      fieldProp: 'allocationBACycle',
                      mappingToType: 'string',
                      key: 'fieldName',
                      value: 'value'
                    },
                    changeCode: {
                      type: 'dynamic',
                      containerProp: 'tableControlFields',
                      fieldProp: 'changeCode',
                      mappingToType: 'string',
                      key: 'fieldName',
                      value: 'value'
                    },
                    citMethodFlag: {
                      type: 'dynamic',
                      containerProp: 'tableControlFields',
                      fieldProp: 'citMethodFlag',
                      mappingToType: 'string',
                      key: 'fieldName',
                      value: 'value'
                    }
                  }
                }
              }
            }
          }
        } as RootState),
      {
        changeService: {
          getDMMTables: jest.fn().mockResolvedValue({
            data: { dmmServiceList: initReducer }
          })
        }
      }
    );

    expect(nextState.meta.arg).toEqual('123');
  });
});
