import store from 'app/_libraries/_dof/core/redux/createAppStore';
import { actionsDMMTables, reducer } from 'pages/_commons/redux/DMMTables';

const initReducer: any = {
  downloadDecisionElement: {
    attachment: {
      attachmentId: '001',
      filename: 'name',
      mimeType: 'type',
      fileSize: 100,
      fileStream: 'steam',
      createdDate: '2021-01-28T11:16:14.3536967+07:00'
    },
    loading: false,
    error: false
  }
};

describe('redux-store > DMMTables > downloadDecisionElement', () => {
  const mockChangeId = '123';

  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsDMMTables.downloadDecisionElement.pending(
        'downloadDecisionElement',
        mockChangeId
      )
    );
    expect(nextState.downloadDecisionElement.loading).toEqual(true);
  });
  it('fulfilled -> isCancel', () => {
    const nextState = reducer(
      initReducer,
      actionsDMMTables.downloadDecisionElement.fulfilled(
        { isCancel: true } as any,
        'downloadDecisionElement',
        mockChangeId
      )
    );
    expect(nextState.downloadDecisionElement.loading).toEqual(false);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsDMMTables.downloadDecisionElement.fulfilled(
        [] as any,
        'downloadDecisionElement',
        mockChangeId
      )
    );
    expect(nextState.downloadDecisionElement.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsDMMTables.downloadDecisionElement.rejected(
        null,
        'downloadDecisionElement',
        mockChangeId
      )
    );
    expect(nextState.downloadDecisionElement.loading).toEqual(false);
  });

  it('thunk action', async () => {
    const nextState: any = await actionsDMMTables.downloadDecisionElement(
      '123'
    )(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          mapping: {
            data: {
              attachment: {
                attachmentId: 'attachmentId',
                filename: 'filename',
                mimeType: 'mimeType',
                fileSize: 'fileSize',
                fileStream: 'fileStream',
                createdDate: 'createdDate'
              }
            }
          }
        } as RootState),
      {
        changeService: {
          downloadDecisionElement: jest.fn().mockResolvedValue({
            data: { attachment: initReducer }
          })
        }
      }
    );

    expect(nextState.meta.arg).toEqual('123');
  });

  // it('thunk action -> isCancel', async () => {
  //   const nextState: any = await actionsWorkflow.getWorkflows()(
  //     store.dispatch,
  //     () =>
  //       ({
  //         ...store.getState(),
  //         mapping: {
  //           data: { changeWorkflow: WorkflowMapping.Workflow }
  //         }
  //       } as RootState),
  //     {
  //       workflowService: {
  //         getInProgressWorkflow: jest.fn().mockResolvedValue({
  //           data: { workflowInstanceList: WorkflowData },
  //           isCancel: true
  //         })
  //       }
  //     }
  //   );

  //   expect(nextState.payload.isCancel).toEqual(true);
  // });
});
