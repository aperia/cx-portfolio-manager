import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { mappingDataFromObj } from 'app/helpers/mappingData';
import get from 'lodash.get';
import set from 'lodash.set';
import { DMMTablesState } from '../types';

export const downloadDecisionElement = createAsyncThunk<
  any,
  string,
  ThunkAPIConfig
>('mapping/downloadDecisionElement', async (dmmTableId, thunkAPI) => {
  const { changeService } = get(thunkAPI, 'extra');
  const state = thunkAPI.getState();
  // Take mapping model

  const downloadAttachmentMapping = state.mapping?.data?.downloadAttachment;

  const resp = await changeService.downloadDecisionElement(dmmTableId);
  const attachmentData = resp?.data?.attachment;

  // Mapping
  const mappedData = mappingDataFromObj(
    attachmentData,
    downloadAttachmentMapping!
  );

  return mappedData;
});

export const downloadDecisionElementBuilder = (
  builder: ActionReducerMapBuilder<DMMTablesState>
) => {
  builder.addCase(downloadDecisionElement.pending, draftState => {
    set(draftState.downloadDecisionElement, 'loading', true);
  });
  builder.addCase(downloadDecisionElement.fulfilled, draftState => {
    set(draftState.downloadDecisionElement, 'loading', false);
  });
  builder.addCase(downloadDecisionElement.rejected, draftState => {
    set(draftState.downloadDecisionElement, 'loading', false);
    set(draftState.downloadDecisionElement, 'error', true);
  });
};
