import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { mappingDataFromArray } from 'app/helpers';
import get from 'lodash.get';
import set from 'lodash.set';
import { DMMTablesState } from '../types';

export const getDMMTables = createAsyncThunk<any, string, ThunkAPIConfig>(
  'mapping/GetDMMTables',
  async (changeId: string, thunkAPI) => {
    const { changeService } = get(thunkAPI, 'extra');

    const state = thunkAPI.getState();
    // Take mapping model
    const dmmListMapping = state.mapping?.data?.dmmList;

    // Request API

    const res = await changeService.getDMMTables(changeId);
    const dmmListData = res?.data?.DMMServiceList || res?.data?.dmmServiceList;

    // Mapping
    const mappedData = mappingDataFromArray(dmmListData, dmmListMapping!).map(
      item => {
        let dmmTables: any[] = [];

        // merge versions into dmmTables
        item.dmmTables.map((tableItem: any) => {
          const { versions, ...rest } = tableItem;
          versions.map((version: any) =>
            dmmTables.push({ ...rest, ...version })
          );
        });

        // Add more field
        dmmTables = dmmTables.map((i: any) => {
          // create new Item
          const newItem: IDMMTable = {
            ...i,
            type: i.tableType.abbreviation,
            tableType: {
              code: `${item.abbr}${i.tableType.abbreviation}`,
              value: i.tableType.abbreviation,
              description: `${i.tableType.name} (${i.tableType.abbreviation})`
            },
            totalDecisionElements: i.decisionElements?.length,
            modeledFrom:
              i.tableModeled?.tableId !== i.tableId
                ? i.tableModeled?.tableId
                : undefined,
            newVersionOf:
              i.tableModeled?.tableId === i.tableId ? i.tableId : undefined,
            actionPayload: i.id
          };

          if (i.tableModeled && i.tableModeled.tableId === i.tableId) {
            delete newItem.modeledFrom;
          } else {
            delete newItem.newVersionOf;
          }

          return newItem;
        });

        return {
          ...item,
          dmmTables: dmmTables
        };
      }
    );
    return mappedData;
  }
);
export const getDMMTablesBuilder = (
  builder: ActionReducerMapBuilder<DMMTablesState>
) => {
  builder.addCase(getDMMTables.pending, (draftState, action) => {
    set(draftState.changeDMMTables, 'loading', true);
    set(draftState.changeDMMTables, 'error', false);
  });
  builder.addCase(getDMMTables.fulfilled, (draftState, action) => {
    return {
      ...draftState,
      changeDMMTables: {
        ...draftState.changeDMMTables,
        dmmTablesList: action.payload,
        loading: false
      }
    };
  });
  builder.addCase(getDMMTables.rejected, (draftState, action) => {
    set(draftState.changeDMMTables, 'loading', false);
    set(draftState.changeDMMTables, 'error', true);
  });
};
