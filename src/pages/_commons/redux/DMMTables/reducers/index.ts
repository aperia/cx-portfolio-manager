import { PayloadAction } from '@reduxjs/toolkit';
import {
  defaultDataFilter,
  defaultOrderBy,
  defaultSortBy,
  DMMTablesState
} from '../types';

const reducers = {
  updateFilter: (
    draftState: DMMTablesState,
    action: PayloadAction<IDataFilter>
  ) => {
    const { sortBy, orderBy, page, ...filter } = action.payload;
    const customFilter: IDataFilter = {};
    const hasSort = (sortBy?.length || 0) > 0;
    const hasOrder = (orderBy?.length || 0) > 0;

    if (hasSort) {
      customFilter.sortBy = [...(sortBy || []), ...defaultSortBy];
    }
    if (hasOrder) {
      customFilter.orderBy = [...(orderBy || []), ...defaultOrderBy];
    }

    if (page) {
      customFilter.page = page;
    } else {
      customFilter.page = 1;
    }

    draftState.changeDMMTables = {
      ...draftState.changeDMMTables,
      dataFilter: {
        ...draftState.changeDMMTables.dataFilter,
        ...filter,
        ...customFilter
      }
    };
  },
  clearAndReset: (draftState: DMMTablesState) => {
    draftState.changeDMMTables = {
      ...draftState.changeDMMTables,
      dataFilter: {
        ...defaultDataFilter,
        sortBy: draftState.changeDMMTables.dataFilter?.sortBy,
        orderBy: draftState.changeDMMTables.dataFilter?.orderBy
      }
    };
  },
  clearAndResetWhenSwitchTab: (draftState: DMMTablesState) => {
    draftState.changeDMMTables = {
      ...draftState.changeDMMTables,
      dataFilter: {
        ...defaultDataFilter,
        searchValue: draftState.changeDMMTables.dataFilter?.searchValue,
        sortBy: draftState.changeDMMTables.dataFilter?.sortBy,
        orderBy: draftState.changeDMMTables.dataFilter?.orderBy
      }
    };
  },
  setToDefault: (draftState: DMMTablesState) => {
    draftState.changeDMMTables = {
      ...draftState.changeDMMTables,
      dmmTablesList: [],
      loading: true,
      dataFilter: defaultDataFilter
    };
  }
};

export default reducers;
