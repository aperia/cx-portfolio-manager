import {
  actionsDMMTables,
  defaultDataFilter,
  DMMTablesState,
  reducer
} from 'pages/_commons/redux/DMMTables';

const initReducer = {
  changeDMMTables: {
    dmmTablesList: [] as IDMMData[],
    loading: false,
    error: false,
    dataFilter: defaultDataFilter
  }
} as DMMTablesState;

describe('redux-store > DMMTables > reducers', () => {
  it('setToDefault', () => {
    const nextState = reducer(initReducer, actionsDMMTables.setToDefault());
    expect(nextState.changeDMMTables.dmmTablesList.length).toEqual(0);
  });
});
