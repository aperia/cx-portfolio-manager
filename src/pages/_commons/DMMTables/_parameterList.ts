import {
  MethodFieldNameEnum,
  MethodFieldParameterEnum,
  TableFieldNameEnum,
  TableFieldParameterEnum
} from 'app/constants/enums';

export enum TableType {
  alpaq = 'ALPAQ',
  alpca = 'ALPCA',
  tlptq = 'TLPTQ',
  tlpaq = 'TLPAQ',
  tlpca = 'TLPCA',
  tlprq = 'TLPRQ',
  mlpca = 'MLPCA',
  mlpaq = 'MLPAQ',
  mlpst = 'MLPST'
}

enum BusinessName {
  nextDebitDate = 'Next Debit Date',
  includeActivityOptions = 'Include Activity Option',
  posPromotionValidation = 'POS Promotion Validation'
}

enum GreenScreenName {
  nextDebitDate = 'Next Debit Date',
  includeActivityOptions = 'INCL Activity Option',
  posPromoValidation = 'POS PROMO Validation'
}

export const getControlParameters = (type: string) => {
  switch (type) {
    case TableType.alpaq:
      return [
        {
          id: MethodFieldParameterEnum.AllocationDate,
          businessName: 'Allocation Date',
          greenScreenName: 'ALLOC Date',
          moreInfo:
            'The Allocation Date parameter defines the date you want the System to allocate accounts through the client allocation table.'
        },
        {
          id: MethodFieldParameterEnum.AllocationFlag,
          businessName: 'Allocation Flag',
          greenScreenName: 'ALLOC Flag',
          moreInfo:
            'The Allocation Flag parameter determines when you want the System to allocate accounts through the account qualification table.'
        },
        {
          id: MethodFieldParameterEnum.AllocationInterval,
          businessName: 'Allocation Interval',
          greenScreenName: 'ALLOC Interval',
          moreInfo:
            'The Allocation Interval parameter determines the number of months after the account’s Statement Cycle Allocation Date you want the System to allocate accounts through the account qualification table if you set the Allocation Flag field to 2.'
        },
        {
          id: MethodFieldParameterEnum.AllocationBeforeAfterCycle,
          businessName: 'Allocation Before/After Cycle',
          greenScreenName: 'ALLOC B/A Cycle',
          moreInfo:
            'The Allocation Before/After Cycle parameter determines whether the System allocates accounts before or after statement cycle processing.'
        },
        {
          id: MethodFieldParameterEnum.DefaultStrategy,
          businessName: 'Default Strategy',
          greenScreenName: 'Default Strategy',
          moreInfo:
            'The Default Strategy parameter defines the default pricing strategy to be assigned to a new or transferred cardholder account if the account receives the SAME result ID.'
        },
        {
          id: MethodFieldParameterEnum.ChangeInTermsMethodFlag,
          businessName: 'Change In Terms Method Flag',
          greenScreenName: 'CIT Method Flag',
          moreInfo:
            'The Change In Terms Method Flag parameter designates whether you want to associate CIT methods with the pricing strategies assigned during reallocation processing.'
        }
      ];
    case TableType.alpca:
      return [
        {
          id: MethodFieldParameterEnum.AllocationDate,
          businessName: MethodFieldNameEnum.AllocationDate,
          greenScreenName: 'ALLOC Date',
          moreInfo:
            'Code determining when you want the System to allocate accounts through the account qualification table'
        }
      ];
    case TableType.tlpaq:
      return [
        {
          id: MethodFieldParameterEnum.NextDebitDate,
          businessName: BusinessName.nextDebitDate,
          greenScreenName: GreenScreenName.nextDebitDate,
          moreInfo:
            'The Next Debit Date parameter defines the date (MM/DD/YYYY) when the next debit will post to the account.'
        },
        {
          id: MethodFieldParameterEnum.IncludeActivityAction,
          businessName: BusinessName.includeActivityOptions,
          greenScreenName: GreenScreenName.includeActivityOptions,
          moreInfo:
            'The Include Activity Option parameter indicates whether the System includes the cash advance transaction type, merchandise transaction type, or both transaction types in the table.'
        }
      ];
    case TableType.tlprq:
      return [
        {
          id: MethodFieldParameterEnum.PosPromotionValidation,
          businessName: BusinessName.posPromotionValidation,
          greenScreenName: GreenScreenName.posPromoValidation,
          moreInfo:
            'The POS Promotion Validation parameter designates whether you want to set the A/D/R IND and RESN CD fields on the Table Detail screen to indicate the result of the point-of-sale validation and provide a reason code.'
        }
      ];
    case TableType.mlpca:
      return [
        {
          id: TableFieldParameterEnum.AllocationDate,
          businessName: TableFieldNameEnum.AllocationDate,
          greenScreenName: 'ALLOC Date',
          moreInfoText:
            'The Allocation Date parameter defines the date you want the System to allocate accounts through the client allocation table.',
          moreInfo:
            'The Allocation Date parameter defines the date you want the System to allocate accounts through the client allocation table.'
        },
        {
          id: TableFieldParameterEnum.CAAllocationFlag,
          businessName: TableFieldNameEnum.CAAllocationFlag,
          greenScreenName: 'ALLOC Flag',
          moreInfoText:
            'The Allocation Flag parameter determines when you want the System to allocate accounts through the account qualification table.',
          moreInfo:
            'The Allocation Flag parameter determines when you want the System to allocate accounts through the account qualification table.'
        },
        {
          id: MethodFieldParameterEnum.AllocationFlag,
          businessName: 'Allocation Flag',
          greenScreenName: 'ALLOC Flag',
          moreInfo:
            'The Allocation Flag parameter determines when you want the System to allocate accounts through the account qualification table.'
        },
        {
          id: TableFieldParameterEnum.AllocationInterval,
          businessName: TableFieldNameEnum.AllocationInterval,
          greenScreenName: 'ALLOC Interval',
          moreInfoText:
            'The Allocation Interval parameter determines the number of months after the account’s Statement Cycle Allocation Date you want the System to allocate accounts through the account qualification table if you set the Allocation Flag field to 2.',
          moreInfo:
            'The Allocation Interval parameter determines the number of months after the account’s Statement Cycle Allocation Date you want the System to allocate accounts through the account qualification table if you set the Allocation Flag field to 2.'
        }
      ];
    default:
      return [
        {
          id: TableFieldParameterEnum.AllocationDate,
          businessName: TableFieldNameEnum.AllocationDate,
          greenScreenName: 'ALLOC Date',
          moreInfoText:
            'The Allocation Date parameter defines the date you want the System to allocate accounts through the client allocation table.',
          moreInfo:
            'The Allocation Date parameter defines the date you want the System to allocate accounts through the client allocation table.'
        },
        {
          id: TableFieldParameterEnum.AQAllocationFlag,
          businessName: TableFieldNameEnum.AQAllocationFlag,
          greenScreenName: 'ALLOC Flag',
          moreInfoText:
            'The Allocation Flag parameter determines when you want the System to allocate accounts through the account qualification table.',
          moreInfo:
            'The Allocation Flag parameter determines when you want the System to allocate accounts through the account qualification table.'
        },
        {
          id: MethodFieldParameterEnum.AllocationFlag,
          businessName: 'Allocation Flag',
          greenScreenName: 'ALLOC Flag',
          moreInfo:
            'The Allocation Flag parameter determines when you want the System to allocate accounts through the account qualification table.'
        },
        {
          id: TableFieldParameterEnum.AllocationInterval,
          businessName: TableFieldNameEnum.AllocationInterval,
          greenScreenName: 'ALLOC Interval',
          moreInfoText:
            'The Allocation Interval parameter determines the number of months after the account’s Statement Cycle Allocation Date you want the System to allocate accounts through the account qualification table if you set the Allocation Flag field to 2.',
          moreInfo:
            'The Allocation Interval parameter determines the number of months after the account’s Statement Cycle Allocation Date you want the System to allocate accounts through the account qualification table if you set the Allocation Flag field to 2.'
        },
        {
          id: TableFieldParameterEnum.AllocationBeforeAfterCycle,
          businessName: TableFieldNameEnum.AllocationBeforeAfterCycle,
          greenScreenName: 'ALLOC B/A Cycle',
          moreInfoText:
            'The Allocation Before/After Cycle parameter determines whether the System allocates accounts before or after statement cycle processing.',
          moreInfo:
            'The Allocation Before/After Cycle parameter determines whether the System allocates accounts before or after statement cycle processing.'
        },
        {
          id: TableFieldParameterEnum.ChangeCode,
          businessName: TableFieldNameEnum.ChangeCode,
          greenScreenName: 'Change Code',
          moreInfoText:
            'The Change Code parameter indicates the client-defined reason code for the current method override; variable-length, 2-position, optional field.',
          moreInfo:
            'The Change Code parameter indicates the client-defined reason code for the current method override; variable-length, 2-position, optional field.'
        },
        {
          id: TableFieldParameterEnum.ChangeInTermsMethodFlag,
          businessName: TableFieldNameEnum.ChangeInTermsMethodFlag,
          greenScreenName: 'CIT Method Flag',
          moreInfoText:
            'The Change In Terms Method Flag parameter designates whether you want to associate CIT methods with the pricing strategies assigned during reallocation processing.',
          moreInfo:
            'The Change In Terms Method Flag parameter designates whether you want to associate CIT methods with the pricing strategies assigned during reallocation processing.'
        },
        {
          id: TableFieldParameterEnum.ServiceSubjectSectionAreas,
          businessName: TableFieldNameEnum.ServiceSubjectSectionAreas,
          greenScreenName: 'SS/SS/SS Areas',
          moreInfoText:
            'The Service/Subject/Section Areas defines a list of Product Control File method overrides can be selected to include in your table.',
          moreInfo:
            'The Service/Subject/Section Areas defines a list of Product Control File method overrides can be selected to include in your table.'
        },
        {
          id: MethodFieldParameterEnum.NextDebitDate,
          businessName: BusinessName.nextDebitDate,
          greenScreenName: GreenScreenName.nextDebitDate,
          moreInfo:
            'The Next Debit Date parameter defines the date (MM/DD/YYYY) when the next debit will post to the account.'
        }
      ];
  }
};
