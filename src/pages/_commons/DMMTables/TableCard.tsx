import ButtonControl from 'app/components/DofControl/ButtonControl';
import { DMM_TABLES_SORT_BY_LIST } from 'app/constants/constants';
import { DropdownList, useTranslation } from 'app/_libraries/_dls';
import { DEFAULT_PAGE_SIZE } from 'app/_libraries/_dls/components/Pagination/constants';
import { View } from 'app/_libraries/_dof/core';
import includes from 'lodash.includes';
import isEmpty from 'lodash.isempty';
import isFunction from 'lodash.isfunction';
import { default as _orderBy } from 'lodash.orderby';
import React, { useCallback, useMemo } from 'react';
import { useDispatch } from 'react-redux';
import {
  actionsDMMTables,
  defaultDataFilter,
  useSelectDMMTablesFilter
} from '../redux/DMMTables';
import Paging from '../Utils/Paging';
import SortOrder from '../Utils/SortOrder';

interface ITableCardProps {
  title?: string;
  total?: number;
  data?: IDMMTable[];
  dataTableType: ITableType[];
  onShowDetails?: (data: IDMMTable) => void;
}
const TableCard: React.FC<ITableCardProps> = ({
  title,
  total,
  data = [],
  dataTableType = [],
  onShowDetails
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const dataFilter = useSelectDMMTablesFilter();
  const { page, pageSize, searchValue, searchFields } = dataFilter;

  const hasFilter = !isEmpty(searchFields) && !includes(searchFields, 'All');
  const dataTableTypeHasOrder = _orderBy(dataTableType, 'value');

  const tableTypeDymanic: ITableType[] = useMemo(
    () => [
      { code: '', value: 'All', description: 'All' },
      ...dataTableTypeHasOrder
    ],
    [dataTableTypeHasOrder]
  );

  const handleChangePage = useCallback(
    (pageNumber: number) => {
      dispatch(
        actionsDMMTables.updateFilter({
          page: pageNumber
        })
      );
    },
    [dispatch]
  );

  const handleChangePageSize = useCallback(
    (pageSizeNumber: number) => {
      dispatch(
        actionsDMMTables.updateFilter({
          pageSize: pageSizeNumber
        })
      );
    },
    [dispatch]
  );

  const handleSort = useCallback(
    (sortByValue: string) => {
      dispatch(
        actionsDMMTables.updateFilter({
          sortBy: [sortByValue]
        })
      );
    },
    [dispatch]
  );

  const handleOrder = useCallback(
    (orderByValue: string) => {
      dispatch(
        actionsDMMTables.updateFilter({
          orderBy: [orderByValue as OrderByType]
        })
      );
    },
    [dispatch]
  );

  const handleClearFilter = useCallback(() => {
    dispatch(actionsDMMTables.clearAndReset());
  }, [dispatch]);

  const handleFilter = useCallback(
    (e: any) => {
      const filterValue = e.target.value;
      dispatch(
        actionsDMMTables.updateFilter({
          searchFields: filterValue ? [filterValue] : ['']
        })
      );
    },
    [dispatch]
  );

  const titleAndFilter = useMemo(() => {
    return (
      <>
        {title && <h5>{title}</h5>}
        <div className={`d-flex align-items-center${title ? ' mt-16' : ''}`}>
          <div className="d-flex align-items-center mb-4">
            <strong className="fs-14 color-grey mr-4">
              {t('txt_dmm_tables_table_type')}:
            </strong>
            {dataTableType?.length > 1 && (
              <DropdownList
                name="tableType"
                value={searchFields ? searchFields[0] : 'All'}
                textFieldRender={e => {
                  const item = tableTypeDymanic.find(i => e === i.value);
                  return <span>{item?.description}</span>;
                }}
                onChange={handleFilter}
                variant="no-border"
              >
                {tableTypeDymanic.map((item, idx) => (
                  <DropdownList.Item
                    key={idx}
                    id={item.value}
                    label={item.description}
                    value={item.value}
                  />
                ))}
              </DropdownList>
            )}
          </div>
          {total! > 0 && (
            <SortOrder
              hasFilter={hasFilter || !!searchValue}
              dataFilter={dataFilter}
              defaultDataFilter={defaultDataFilter}
              sortByFields={DMM_TABLES_SORT_BY_LIST}
              onChangeSortBy={handleSort}
              onChangeOrderBy={handleOrder}
              onClearSearch={handleClearFilter}
            />
          )}
        </div>
      </>
    );
  }, [
    t,
    title,
    total,
    searchFields,
    dataTableType.length,
    tableTypeDymanic,
    searchValue,
    hasFilter,
    dataFilter,
    handleOrder,
    handleSort,
    handleClearFilter,
    handleFilter
  ]);

  const tableLists = useMemo(
    () =>
      data?.map((item: IDMMTable, index) => {
        const dataTable = {
          ...item,
          tableTypeDesc: item.tableType.description,
          status:
            item.tableId === item.tableModeled?.tableId
              ? t('txt_updated')
              : t('txt_created')
        };

        return (
          <div
            key={`dmm-tables-card-view__${index}`}
            className="position-relative"
          >
            <div
              className="list-view mt-12"
              onClick={() =>
                isFunction(onShowDetails) && onShowDetails(dataTable)
              }
            >
              <View
                id={`dmm-tables-card-view__${item.parentTableId}__${index}`}
                formKey={`dmm-tables-card-view__${item.parentTableId}__${index}`}
                descriptor="dmm-tables-card-view"
                value={dataTable}
              />
            </div>
            <div className="method-card-view-btn t-7" role="dof-field">
              <ButtonControl
                id="dmmTableExport-button"
                input={{ value: item.id } as any}
                meta={{} as any}
                label={t('txt_ddm_tables_btn_export')}
                options={{
                  actionName: 'downloadDMMTable'
                }}
              />
            </div>
          </div>
        );
      }),
    [t, data, onShowDetails]
  );

  return (
    <div className={`${title ? 'mt-24' : 'mt-16'}`}>
      {titleAndFilter}
      {tableLists}
      {total! > DEFAULT_PAGE_SIZE && (
        <Paging
          page={page}
          pageSize={pageSize}
          totalItem={total}
          onChangePage={handleChangePage}
          onChangePageSize={handleChangePageSize}
        />
      )}
    </div>
  );
};

export default TableCard;
