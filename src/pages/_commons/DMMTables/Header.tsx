import SimpleSearch from 'app/components/SimpleSearch';
import { useTranslation } from 'app/_libraries/_dls';
import isFunction from 'lodash.isfunction';
import React, { useCallback, useEffect, useRef } from 'react';

interface IProps {
  id: string;
  title?: string;
  searchValue?: string;
  showSearch?: boolean;
  onSearch?: (val: string) => void;
}
const DMMTablesHeader: React.FC<IProps> = ({
  id,
  title,
  searchValue,
  showSearch = true,
  onSearch
}) => {
  const { t } = useTranslation();

  const simpleSearchRef = useRef<any>(null);

  const handleSearch = useCallback(
    (val: string) => {
      isFunction(onSearch) && onSearch(val);
    },
    [onSearch]
  );

  useEffect(() => {
    if (!searchValue && simpleSearchRef) {
      showSearch && simpleSearchRef.current.clear();
    }
  }, [searchValue, showSearch]);

  return (
    <div
      id={`pricing-method-header-${id}`}
      className="d-flex align-self-center"
    >
      <h4>{title}</h4>
      <div className="ml-auto">
        {showSearch && (
          <SimpleSearch
            ref={simpleSearchRef}
            placeholder={t('txt_type_to_search')}
            clearTooltip={t('txt_clear_search_criteria')}
            onSearch={handleSearch}
            popperElement={
              <>
                <p className="color-grey">{t('txt_search_description')}</p>
                <ul className="search-field-item list-unstyled">
                  <li className="mt-16">Modeled From</li>
                  <li className="mt-16">New Version Of</li>
                  <li className="mt-16">Table ID</li>
                  <li className="mt-16">Table Name</li>
                </ul>
              </>
            }
          />
        )}
      </div>
    </div>
  );
};

export default DMMTablesHeader;
