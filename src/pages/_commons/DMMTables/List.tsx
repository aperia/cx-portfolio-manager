import FailedApiReload from 'app/components/FailedApiReload';
import NoDataFound from 'app/components/NoDataFound';
import { classnames } from 'app/helpers';
import { HorizontalTabs, useTranslation } from 'app/_libraries/_dls';
import isEmpty from 'lodash.isempty';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';
import {
  actionsDMMTables,
  useSelectDMMTables,
  useSelectDMMTablesFilter
} from '../redux/DMMTables';
import DMMTablesDetail from './Detail';
import DMMTablesHeader from './Header';
import TableCard from './TableCard';

interface IProps {
  changeId?: string;
  containerClassName?: string;
}
const DMMTablesList: React.FC<IProps> = ({ changeId }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const [activeKey, setActiveKey] = useState<string>('');
  const [detailsShow, setDetailsShow] = useState<boolean>(false);
  const [details, setDetails] = useState<IDMMTable>();

  const dmmTablesInfo = useSelectDMMTables();

  const dataFilter = useSelectDMMTablesFilter();
  const { loading, error, dmmList, totalItem } = dmmTablesInfo;
  const { searchValue, searchFields } = dataFilter;
  const hasFilterOnGroupBtn = !isEmpty(searchFields);
  const loaded = useRef<boolean>(true);

  // Fetch data
  useEffect(() => {
    changeId && dispatch(actionsDMMTables.getDMMTables(changeId));
    return () => {
      dispatch(actionsDMMTables.setToDefault());
    };
  }, [dispatch, changeId]);

  const handleApiReload = () => {
    changeId && dispatch(actionsDMMTables.getDMMTables(changeId));
  };

  // Handle set defaultTab
  useEffect(() => {
    if (isEmpty(dmmList) || totalItem === 1) return;
    loaded.current && setActiveKey(dmmList[0].abbr);
    loaded.current = false;
  }, [dmmList, totalItem]);

  // Handle get total
  const totalItemFilter = useMemo(() => {
    if (totalItem === 1) return dmmList[0]?.totalItemFilter;
    if (!activeKey) return 0;

    const newVal = dmmList.find(e => e.abbr === activeKey);

    return newVal?.totalItemFilter || 0;
  }, [activeKey, dmmList, totalItem]);

  useEffect(() => {
    if (!activeKey) return;
    dispatch(actionsDMMTables.clearAndResetWhenSwitchTab());
  }, [activeKey, dispatch]);

  // Handle switch tab
  const handleChangeActiveKey = (activeKey: string | null) => {
    setActiveKey(activeKey!);
  };

  // Handle search
  const handleSearch = useCallback(
    (searchInputValue: string) => {
      dispatch(
        actionsDMMTables.updateFilter({
          searchValue: searchInputValue
        })
      );
    },
    [dispatch]
  );
  // handleClearFilter
  const handleClearFilter = useCallback(() => {
    dispatch(actionsDMMTables.clearAndReset());
  }, [dispatch]);

  // Handle show flyout
  const handleShowDetail = (item: IDMMTable) => {
    setDetailsShow(true);
    setDetails(item);
  };

  const handleCloseDetail = () => {
    setDetailsShow(false);
  };

  return (
    <div
      id={`dmm-tables-${changeId}`}
      className={classnames({ 'loading mh-320': loading })}
    >
      {!loading && (
        <DMMTablesHeader
          id={changeId || 'dmm-tables-header'}
          title={t('txt_dmm_tables')}
          showSearch={totalItem > 0}
          searchValue={searchValue}
          onSearch={handleSearch}
        />
      )}

      {!loading && !error && (
        <>
          {totalItem > 0 && (
            <div className="mt-16">
              {totalItem > 1 ? (
                <HorizontalTabs
                  className="bg-inherit"
                  level2
                  activeKey={activeKey}
                  onSelect={handleChangeActiveKey}
                >
                  {!isEmpty(dmmList) &&
                    dmmList?.map((item: IDMMData) => {
                      const title = `${item.serviceName} (${item.abbr})`;

                      return (
                        <HorizontalTabs.Tab
                          key={`tab-${item.abbr}`}
                          eventKey={item.abbr}
                          title={title}
                        >
                          <TableCard
                            title={title}
                            data={item!.dmmTables}
                            total={item.totalItemFilter}
                            dataTableType={item.tableTypeFilter}
                            onShowDetails={(data: IDMMTable) =>
                              handleShowDetail(data)
                            }
                          />
                        </HorizontalTabs.Tab>
                      );
                    })}
                </HorizontalTabs>
              ) : (
                !isEmpty(dmmList) && (
                  <>
                    <div className="pt-8">
                      <h5>{`${dmmList[0]!.serviceName} (${
                        dmmList[0]!.abbr
                      })`}</h5>
                    </div>
                    <TableCard
                      data={dmmList[0]!.dmmTables}
                      total={dmmList[0]!.totalItemFilter}
                      dataTableType={dmmList[0]!.tableTypeFilter}
                      onShowDetails={(data: IDMMTable) =>
                        handleShowDetail(data)
                      }
                    />
                  </>
                )
              )}
            </div>
          )}

          {detailsShow && (
            <DMMTablesDetail
              show={detailsShow}
              details={details}
              onCloseDetails={handleCloseDetail}
            />
          )}

          {(totalItem === 0 || totalItemFilter === 0) && (
            <div className="d-flex flex-column justify-content-center mt-80 mb-8">
              <NoDataFound
                id="dmm-tables-no-data"
                title={t('txt_dmm_tables_no_data')}
                hasSearch={!!searchValue}
                hasFilter={hasFilterOnGroupBtn}
                linkTitle={
                  (hasFilterOnGroupBtn || !!searchValue) &&
                  t('txt_clear_and_reset')
                }
                onLinkClicked={handleClearFilter}
              />
            </div>
          )}
        </>
      )}

      {!loading && error && (
        <FailedApiReload
          id="change-details--dmm-tables-tab--error"
          onReload={handleApiReload}
          className="d-flex flex-column justify-content-center align-items-center mt-80 mb-32"
        />
      )}
    </div>
  );
};

export default DMMTablesList;
