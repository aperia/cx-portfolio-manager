import { PAGE_SIZE } from 'app/constants/constants';
import {
  ColumnType,
  Grid,
  Icon,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import {
  DEFAULT_PAGE_NUMBER,
  DEFAULT_PAGE_SIZE
} from 'app/_libraries/_dls/components/Pagination/constants';
import isEmpty from 'lodash.isempty';
import React, { useState } from 'react';
import { useSelectElementMetadataDMMTables } from '../redux/DMMTables';
import Paging from '../Utils/Paging';
import { mappingData } from './helper';

interface IGridDecisionProps {
  data: DecisionType[] | undefined;
}

const GridDecision: React.FC<IGridDecisionProps> = ({ data = [] }) => {
  const { t } = useTranslation();
  const [page, setPage] = useState<number>(DEFAULT_PAGE_NUMBER);
  const [pageSize, setPageSize] = useState<number>(PAGE_SIZE[0]);

  const metadata = useSelectElementMetadataDMMTables();
  const { searchCode } = metadata;
  const filterData = data || [];
  const total = filterData[0]?.value?.length;

  const dataPage = filterData?.map((item: DecisionType) => {
    return {
      ...item,
      value: item.value.slice((page - 1) * pageSize, page * pageSize)
    };
  });

  const handleChangePage = (page: number) => {
    setPage(page);
  };

  const handleChangePageSize = (pageSize: number) => {
    setPageSize(pageSize);
  };

  const columns = dataPage?.map((clm: any, index: number) => ({
    id: clm?.name + index,
    Header: (
      <div className="d-flex align-items-end">
        <p>{clm?.name}</p>
        {!!clm?.searchCode && (
          <Tooltip
            triggerClassName="ml-8 d-flex"
            element={
              <div>
                <p>
                  {t('txt_manage_account_level_table_decision_search')}:{' '}
                  {!isEmpty(searchCode)
                    ? searchCode.filter(val => val.code === clm?.searchCode)[0]
                        ?.text
                    : clm?.searchCode}
                </p>
                {!!clm?.immediateAllocation && (
                  <p>
                    {t(
                      'txt_manage_account_level_table_decision_immediate_allocation'
                    )}
                    : {clm?.immediateAllocation}
                  </p>
                )}
              </div>
            }
          >
            <Icon name="information" size="4x" className="color-grey-l16" />
          </Tooltip>
        )}
      </div>
    ),
    accessor: data => data![clm?.name],
    width: 180,
    minWidth: 180
  })) as ColumnType[];

  return (
    <>
      <Grid columns={columns} data={mappingData(dataPage)} />
      {total > DEFAULT_PAGE_SIZE && (
        <Paging
          page={page}
          pageSize={pageSize}
          totalItem={total}
          onChangePage={handleChangePage}
          onChangePageSize={handleChangePageSize}
        />
      )}
    </>
  );
};

export default GridDecision;
