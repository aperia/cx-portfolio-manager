import { fireEvent, RenderResult, screen } from '@testing-library/react';
import { DMMTablesData, DMMTablesdMapping } from 'app/fixtures/dmm-tables-data';
import { mappingDataFromArray } from 'app/helpers';
import { DMMTablesState } from 'app/types';
import { renderWithMockStore, spyOnTranslationHoook } from 'app/utils';
import 'app/utils/_mockComponent/mockButtonGroupFilter';
import 'app/utils/_mockComponent/mockDofView';
import 'app/utils/_mockComponent/mockFailedApiReload';
import 'app/utils/_mockComponent/mockNoDataFound';
import 'app/utils/_mockComponent/mockPaging';
import 'app/utils/_mockComponent/mockSortOrder';
import 'pages/_commons/PricingStrategy/mockPricingStrategyDetail';
import 'pages/_commons/PricingStrategy/mockPricingStrategyHeader';
import * as CommonRedux from 'pages/_commons/redux/Common/select-hooks';
import * as DMMTablesSelector from 'pages/_commons/redux/DMMTables/selector';
import React from 'react';
import { defaultDataFilter } from '../redux/PricingStrategy';
import DMMTablesList from './List';
jest.mock('./Detail', () => ({
  __esModule: true,
  default: ({ onCloseDetails }: any) => {
    return (
      <div>
        <p>Mock DMM Tables Detail</p>
        <button className="detail-close-btn" onClick={onCloseDetails}>
          Close Detail
        </button>
      </div>
    );
  }
}));

const mockUseSelectWindowDimension = jest.spyOn(
  CommonRedux,
  'useSelectWindowDimension'
);

const mockUseSelectWindowDimensionImplementation = (width: number) => {
  mockUseSelectWindowDimension.mockImplementation(() => ({ width } as any));
};

const mockuseSelectDMMTables = ({
  dmmList,
  loading,
  error,
  totalItem
}: any) => {
  jest.spyOn(DMMTablesSelector, 'useSelectDMMTables').mockReturnValue({
    dmmList,
    loading,
    error,
    totalItem
  });
};

const testId = 'list-test-id';

const mockDmmTablesData = mappingDataFromArray(
  DMMTablesData,
  DMMTablesdMapping
);
export const ListMock: any = {
  changeId: '123'
};

describe('DMMTablesList', () => {
  let renderResult: RenderResult;
  let storeCofig: any = {};
  beforeEach(() => {
    mockUseSelectWindowDimensionImplementation(1300);
    storeCofig = {
      initialState: {
        dmmTables: {
          changeDMMTables: {
            dmmTablesList: mockDmmTablesData,
            loading: false,
            dataFilter: defaultDataFilter,
            error: false
          }
        } as DMMTablesState
      }
    };
    spyOnTranslationHoook();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render dmm tables list has more a tab', async () => {
    mockuseSelectDMMTables({
      dmmList: mockDmmTablesData,
      loading: false,
      error: false,
      totalItem: 2
    });

    renderResult = await renderWithMockStore(
      <DMMTablesList {...ListMock} />,
      storeCofig,
      {},
      testId
    );
    await renderResult.findByTestId(testId);

    expect((await renderResult.findAllByRole('tab')).length).toEqual(2);

    expect(
      renderResult.getByText('Click on dmm-tables-card-view__undefined__0')
    ).toBeInTheDocument();
    fireEvent.click(
      renderResult.getByText('Click on dmm-tables-card-view__undefined__0')
    );
  });

  it('should render dmm tables list has only a tab', async () => {
    mockuseSelectDMMTables({
      dmmList: mockDmmTablesData,
      loading: false,
      error: false,
      totalItem: 1
    });

    renderResult = await renderWithMockStore(
      <DMMTablesList {...ListMock} />,
      storeCofig,
      {},
      testId
    );

    expect(renderResult.queryAllByRole('tab')).toEqual([]);

    expect(
      renderResult.getByText('Click on dmm-tables-card-view__undefined__0')
    ).toBeInTheDocument();
  });

  it('should render no data found', async () => {
    mockuseSelectDMMTables({
      dmmList: mockDmmTablesData,
      loading: false,
      error: false,
      totalItem: 0
    });

    renderResult = await renderWithMockStore(
      <DMMTablesList {...ListMock} />,
      storeCofig,
      {},
      testId
    );
    const { getByText } = renderResult;
    expect(getByText('No data found')).toBeInTheDocument();
  });

  it('should render data load unsuccessfully', async () => {
    mockuseSelectDMMTables({
      dmmList: mockDmmTablesData,
      loading: false,
      error: true,
      totalItem: 0
    });

    renderResult = await renderWithMockStore(
      <DMMTablesList {...ListMock} />,
      storeCofig,
      {},
      testId
    );
    const { getByText } = renderResult;
    fireEvent.click(screen.getByText('Reload'));
    expect(getByText('Data load unsuccessful')).toBeInTheDocument();
  });

  it('should show detail on list item clicked', async () => {
    mockuseSelectDMMTables({
      dmmList: mockDmmTablesData,
      loading: false,
      error: false,
      totalItem: 2
    });

    renderResult = await renderWithMockStore(
      <DMMTablesList {...ListMock} />,
      storeCofig,
      {},
      testId
    );
    const { getByText } = renderResult;

    fireEvent.click(
      renderResult.getByText('Click on dmm-tables-card-view__undefined__0')
    );
    expect(getByText(`Mock DMM Tables Detail`)).toBeInTheDocument();
    fireEvent.click(getByText('Close Detail'));
    expect(screen.queryByText(`Mock DMM Tables Detail`)).toBeNull();
  });
});
