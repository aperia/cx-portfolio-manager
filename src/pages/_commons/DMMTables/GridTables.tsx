import {
  MethodFieldParameterEnum,
  TableFieldParameterEnum
} from 'app/constants/enums';
import { formatCommon } from 'app/helpers';
import {
  ColumnType,
  Grid,
  Icon,
  Tooltip,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import isEmpty from 'lodash.isempty';
import React, { useMemo } from 'react';
import { useSelectElementMetadataDMMTables } from '../redux/DMMTables';
import { getControlParameters } from './_parameterList';

interface IGridTablesProps {
  data: Record<string, any | undefined>;
  type?: string;
}

const GridTables: React.FC<IGridTablesProps> = ({ data = [], type }) => {
  const { t } = useTranslation();
  const metaData = useSelectElementMetadataDMMTables();

  const {
    caAllocationFlag,
    aqAllocationFlag,
    allocationFlag,
    allocationBeforeAfterCycle,
    changeInTermsMethodFlag,
    allocationInterval,
    posPromoValidationOptions,
    includeActivityOptions
  } = metaData;

  const controlParameters = getControlParameters(type!) as [];

  const defaultControlParameters = useMemo(() => {
    return controlParameters.filter((item: any) =>
      data?.find((d: any) => d.name === item.id)
    );
  }, [data, controlParameters]);

  const valueAccessor = (item: Record<string, any>) => {
    let valueTruncate: any = '';

    const { id } = item;
    const { value } = data?.find((d: any) => d.name === id) || '';

    switch (id) {
      case MethodFieldParameterEnum.AllocationDate:
        valueTruncate = value ? formatCommon(value as any).time.date : '';
        break;

      case MethodFieldParameterEnum.AllocationFlag:
        const allocationFlagResult = allocationFlag.find(o => o.code === value);

        valueTruncate =
          allocationFlagResult?.code === ''
            ? allocationFlagResult?.code
            : allocationFlagResult?.text;
        break;

      case MethodFieldParameterEnum.NextDebitDate:
        valueTruncate = value ? formatCommon(value as any).time.date : '';
        break;

      case MethodFieldParameterEnum.IncludeActivityAction:
        if (isEmpty(includeActivityOptions)) {
          valueTruncate = value ? value : '';
        } else {
          const includeActivityOptionsResult = includeActivityOptions.find(
            o => o.code === value
          );

          valueTruncate =
            includeActivityOptionsResult?.code === ''
              ? includeActivityOptionsResult?.code
              : includeActivityOptionsResult?.text;
        }
        break;

      case MethodFieldParameterEnum.PosPromotionValidation:
        if (isEmpty(posPromoValidationOptions)) {
          valueTruncate = value ? value : '';
        } else {
          const posPromoValidationOptionsResult =
            posPromoValidationOptions.find(o => o.code === value);
          valueTruncate =
            posPromoValidationOptionsResult?.code === ''
              ? posPromoValidationOptionsResult?.code
              : posPromoValidationOptionsResult?.text;
        }
        break;

      case TableFieldParameterEnum.CAAllocationFlag:
        if (isEmpty(caAllocationFlag)) {
          valueTruncate = value ? value : '';
        } else {
          const caAllocationFlagResult = caAllocationFlag.find(
            o => o.code === value
          );
          valueTruncate =
            caAllocationFlagResult?.code === ''
              ? caAllocationFlagResult?.code
              : caAllocationFlagResult?.text;
        }
        break;

      case TableFieldParameterEnum.AQAllocationFlag:
        if (isEmpty(aqAllocationFlag)) {
          valueTruncate = value ? value : '';
        } else {
          const aqAllocationFlagResult = aqAllocationFlag.find(
            o => o.code === value
          );
          valueTruncate =
            aqAllocationFlagResult?.code === ''
              ? aqAllocationFlagResult?.code
              : aqAllocationFlagResult?.text;
        }
        break;

      case TableFieldParameterEnum.AllocationBeforeAfterCycle:
        if (isEmpty(allocationBeforeAfterCycle)) {
          valueTruncate = value ? value : '';
        } else {
          const allocationBeforeAfterCycleResult =
            allocationBeforeAfterCycle.find(o => o.code === value);
          valueTruncate =
            allocationBeforeAfterCycleResult?.code === ''
              ? allocationBeforeAfterCycleResult?.code
              : allocationBeforeAfterCycleResult?.text;
        }
        break;

      case TableFieldParameterEnum.AllocationInterval:
        if (isEmpty(allocationInterval)) {
          valueTruncate = value ? value : '';
        } else {
          const allocationIntervalResult = allocationInterval.find(
            o => o.code === value
          );
          valueTruncate =
            allocationIntervalResult?.code === ''
              ? allocationIntervalResult?.code
              : allocationIntervalResult?.text;
        }
        break;

      case TableFieldParameterEnum.ChangeInTermsMethodFlag:
        if (isEmpty(changeInTermsMethodFlag)) {
          valueTruncate = value ? value : '';
        } else {
          const ChangeInTermsMethodFlagResult = changeInTermsMethodFlag.find(
            o => o.code === value
          );
          valueTruncate =
            ChangeInTermsMethodFlagResult?.code === ''
              ? ChangeInTermsMethodFlagResult?.code
              : ChangeInTermsMethodFlagResult?.text;
        }
        break;

      default:
        valueTruncate = value ? value : '';
        break;
    }

    return (
      <TruncateText
        resizable
        lines={2}
        ellipsisLessText={t('txt_less')}
        ellipsisMoreText={t('txt_more')}
      >
        {valueTruncate}
      </TruncateText>
    );
  };

  const columns: ColumnType[] = [
    {
      id: 'businessName',
      Header: t('txt_manage_account_level_table_decision_business_name'),
      accessor: ({ businessName }) => t(businessName),
      width: 230
    },
    {
      id: 'greenScreenName',
      Header: t('txt_manage_account_level_table_decision_green_name'),
      accessor: ({ greenScreenName }) => t(greenScreenName),
      width: 200
    },
    {
      id: 'value',
      Header: t('txt_manage_account_level_table_decision_value'),
      accessor: valueAccessor,
      width: 260
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      cellBodyProps: { className: 'pt-12 pb-8' },
      width: 106,
      accessor: ({ moreInfo }) => (
        <Tooltip element={t(moreInfo)}>
          <Icon name="information" size="5x" className="color-grey-l16" />
        </Tooltip>
      )
    }
  ];

  return <Grid columns={columns} data={defaultControlParameters} />;
};

export default GridTables;
