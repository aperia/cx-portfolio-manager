import { RenderResult } from '@testing-library/react';
import { DMMTablesData, DMMTablesdMapping } from 'app/fixtures/dmm-tables-data';
import { mappingDataFromArray } from 'app/helpers';
import { renderWithMockStore, spyOnTranslationHoook } from 'app/utils';
import 'app/utils/_mockComponent/mockDofView';
import 'app/utils/_mockComponent/mockLoadMore';
import 'app/utils/_mockComponent/mockModalRegistry';
import React from 'react';
import DMMTablesDetail, { IDMMTablesDetailProps } from './Detail';

const testId = 'detail-test-id';

const mockDmmTablesData = mappingDataFromArray(
  DMMTablesData,
  DMMTablesdMapping
) as IDMMData[];

export const DetailMock: IDMMTablesDetailProps = {
  show: true,
  details: mockDmmTablesData[1].dmmTables[0],
  onCloseDetails: () => {}
};
describe('DmmTablesDetail', () => {
  let renderResult: RenderResult;

  beforeEach(() => {
    DetailMock.details = mockDmmTablesData[1].dmmTables[0];
    spyOnTranslationHoook();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render detail with status is created', async () => {
    renderResult = await renderWithMockStore(
      <DMMTablesDetail {...DetailMock} />,
      {},
      undefined,
      testId
    );
    await renderResult.findByTestId(testId);

    expect(
      renderResult.getByText('txt_dmm_tables_details')
    ).toBeInTheDocument();
    expect(
      renderResult.getByText('change-dmm-tables-details-snapshot__id2')
    ).toBeInTheDocument();
  });

  it('should render detail with status is updated', async () => {
    DetailMock.details = mockDmmTablesData[0].dmmTables[0];
    renderResult = await renderWithMockStore(
      <DMMTablesDetail {...DetailMock} />,
      {},
      undefined,
      testId
    );
    await renderResult.findByTestId(testId);

    expect(
      renderResult.getByText('txt_dmm_tables_details')
    ).toBeInTheDocument();
    expect(
      renderResult.getByText('change-dmm-tables-details-snapshot__id1')
    ).toBeInTheDocument();
  });

  it('should render empty detail', async () => {
    renderResult = await renderWithMockStore(
      <DMMTablesDetail {...DetailMock} show={false} details={undefined} />,
      {},
      undefined,
      testId
    );
    await renderResult.findByTestId(testId);
    expect(
      document.body.querySelectorAll('div.dls-modal-title').length
    ).toEqual(0);
  });

  it('load more data ', async () => {
    jest.useFakeTimers();

    renderResult = await renderWithMockStore(
      <DMMTablesDetail {...DetailMock} />,
      {},
      undefined,
      testId
    );
    await renderResult.findByTestId(testId);
  });
});
