import ButtonControl from 'app/components/DofControl/ButtonControl';
import ModalRegistry from 'app/components/ModalRegistry';
import {
  MethodFieldParameterEnum,
  TableFieldParameterEnum
} from 'app/constants/enums';
import {
  HorizontalTabs,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  SimpleBar,
  useTranslation
} from 'app/_libraries/_dls';
import { View } from 'app/_libraries/_dof/core';
import isEmpty from 'lodash.isempty';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { actionsWorkflowSetup } from '../redux/WorkflowSetup';
import GridDecision from './GridDecision';
import GridTables from './GridTables';

export interface IDMMTablesDetailProps {
  show: boolean;
  details?: IDMMTable;
  onCloseDetails: () => void;
}
const DMMTablesDetail: React.FC<IDMMTablesDetailProps> = ({
  show,
  details,
  onCloseDetails
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [activeTab, setActiveTab] = useState<string | null>('table');

  const {
    id,
    parentTableId,
    tableType,
    decisionElements,
    tableControlParameters
  } = details || {};

  useEffect(() => {
    dispatch(
      actionsWorkflowSetup.getNonWorkflowMetadata([
        MethodFieldParameterEnum.SearchCode,
        MethodFieldParameterEnum.IncludeActivityAction,
        MethodFieldParameterEnum.PosPromotionValidation,
        MethodFieldParameterEnum.AllocationFlag,
        TableFieldParameterEnum.AllocationBeforeAfterCycle,
        TableFieldParameterEnum.CAAllocationFlag,
        TableFieldParameterEnum.AQAllocationFlag,
        TableFieldParameterEnum.AllocationInterval,
        TableFieldParameterEnum.ChangeInTermsMethodFlag
      ])
    );
  }, [dispatch]);

  const handleSelectTab = (
    eventKey: string | null,
    e: React.SyntheticEvent<unknown>
  ) => {
    setActiveTab(eventKey);
  };

  const renderFlyoutContent = () => {
    return (
      <>
        {!isEmpty(tableControlParameters) && (
          <HorizontalTabs
            id="tab"
            activeKey={activeTab!}
            onSelect={handleSelectTab}
            defaultActiveKey="table"
            level2
          >
            <HorizontalTabs.Tab
              key="table"
              eventKey="table"
              title={t('txt_dmm_tables_level_table_control')}
            >
              <div className="bg-white p-24">
                <h5 className="mb-16">
                  {t('txt_dmm_tables_level_table_parameters')}
                </h5>
                <GridTables
                  data={tableControlParameters!}
                  type={tableType?.code}
                />
              </div>
            </HorizontalTabs.Tab>
            <HorizontalTabs.Tab
              key="decision"
              eventKey="decision"
              title={t('txt_dmm_tables_level_table_decision_elements')}
            >
              <div className="bg-white p-24">
                <h5 className="mb-16">
                  {t('txt_dmm_tables_level_table_decision_elements')}
                </h5>
                <GridDecision data={decisionElements} />
              </div>
            </HorizontalTabs.Tab>
          </HorizontalTabs>
        )}
        {isEmpty(tableControlParameters) && (
          <div className="bg-white p-24">
            <h5 className="mb-16">
              {t('txt_dmm_tables_level_table_decision_elements')}
            </h5>
            <GridDecision data={decisionElements} />
          </div>
        )}
      </>
    );
  };

  return (
    <ModalRegistry
      id={`dmm-tables-detail-${parentTableId}-${id}`}
      xs
      rt
      enforceFocus={false}
      scrollable={false}
      show={show}
      animationDuration={500}
      animationComponentProps={{ direction: 'right' }}
      classes={{
        dialog: 'window-sm'
      }}
      onAutoClosedAll={onCloseDetails}
    >
      <ModalHeader border closeButton onHide={onCloseDetails}>
        <ModalTitle>{t('txt_dmm_tables_details')}</ModalTitle>
      </ModalHeader>
      <ModalBody className="p-0">
        <SimpleBar>
          <div className="bg-light-l20">
            <div className="p-24">
              <View
                id={`change-dmm-tables-details-snapshot__${id}`}
                formKey={`change-dmm-tables-details-snapshot__${id}`}
                descriptor="change-dmm-tables-details-snapshot"
                value={details}
              />
            </div>
            {renderFlyoutContent()}
          </div>
        </SimpleBar>
      </ModalBody>
      <ModalFooter border cancelButtonText="Close" onCancel={onCloseDetails}>
        <ButtonControl
          id="dmmTableExport-button"
          className="mr-auto ml-n8"
          input={{ value: id } as any}
          meta={{} as any}
          label={t('txt_ddm_tables_btn_export')}
          options={{
            actionName: 'downloadDMMTable'
          }}
        />
      </ModalFooter>
    </ModalRegistry>
  );
};

export default DMMTablesDetail;
