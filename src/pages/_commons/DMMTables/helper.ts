export const mappingData = (data: DecisionType[]) => {
  const arrayValues = data[0]?.value || [];
  return arrayValues.reduce((current: any, _, index) => {
    const result = data!.reduce((currentMap: any, valueMap) => {
      return { ...currentMap, [valueMap.name]: valueMap.value[index] };
    }, {});
    return [...current, result];
  }, []);
};
