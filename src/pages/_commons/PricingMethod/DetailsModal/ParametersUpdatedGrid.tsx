import LoadMore from 'app/components/LoadMore';
import { LOAD_MORE_PAGE_SIZE } from 'app/constants/constants';
import { techNameToText } from 'app/helpers';
import { Grid, useTranslation } from 'app/_libraries/_dls';
import { isArray } from 'lodash';
import React, { useMemo, useState } from 'react';

export interface IParametersUpdatedGridProps {
  details?: IPricingMethod;
}
const ParametersUpdatedGrid: React.FC<IParametersUpdatedGridProps> = ({
  details
}) => {
  const { parameters: parametersRaw = [], parametersUpdated: total } =
    details || {};
  const [loadingMore, setLoadingMore] = useState<boolean>(false);
  const [page, setPage] = useState<number>(1);
  const { t } = useTranslation();

  const columns = useMemo(
    () => [
      {
        id: 'name',
        Header: t('txt_pricing_methods_parameter'),
        accessor: 'name',
        width: 300
      },
      {
        id: 'previousValue',
        Header: t('txt_pricing_methods_previous_value'),
        accessor: 'previousValue',
        width: 130
      },
      {
        id: 'newValue',
        Header: t('txt_pricing_methods_new_value'),
        accessor: 'newValue'
      }
    ],
    [t]
  );

  const parameters = useMemo(() => {
    return isArray(parametersRaw)
      ? parametersRaw.map(p => ({
          ...p,
          name: techNameToText(p.name)
        }))
      : [];
  }, [parametersRaw]);

  const handleOnChangePage = () => {
    setLoadingMore(true);
    setTimeout(() => {
      setPage(page + 1);
      setLoadingMore(false);
    }, 300);
  };

  return (
    <div className="p-24">
      <h5 className="pb-16">{t('txt_parameters_updated')}</h5>
      <Grid
        columns={columns}
        data={parameters.slice(0, page * LOAD_MORE_PAGE_SIZE)}
      />
      {total! > LOAD_MORE_PAGE_SIZE && (
        <LoadMore
          loading={loadingMore}
          onLoadMore={handleOnChangePage}
          isEnd={page * LOAD_MORE_PAGE_SIZE >= total!}
          className="mt-24"
          endLoadMoreText={t('txt_pricing_methods_end_of_methods_updated')}
        />
      )}
    </div>
  );
};

export default ParametersUpdatedGrid;
