import LoadMore from 'app/components/LoadMore';
import { LOAD_MORE_PAGE_SIZE } from 'app/constants/constants';
import { Grid, useTranslation } from 'app/_libraries/_dls';
import _orderBy from 'lodash.orderby';
import React, { useMemo, useState } from 'react';

interface IStrategiesImpactedGridProps {
  details?: IPricingMethod;
}
const StrategiesImpactedGrid: React.FC<IStrategiesImpactedGridProps> = ({
  details
}) => {
  const { strategiesImpacted = [], strategiesImpactedCount: total } =
    details || {};

  const strategiesImpactedOrdered = _orderBy(strategiesImpacted, ['name']);

  const [loadingMore, setLoadingMore] = useState<boolean>(false);
  const [page, setPage] = useState<number>(1);
  const { t } = useTranslation();

  const columns = useMemo(
    () => [
      {
        id: 'name',
        Header: t('txt_pricing_methods_strategyName'),
        accessor: 'name',
        width: 135
      },
      {
        id: 'description',
        Header: t('txt_description'),
        accessor: 'description'
      }
    ],
    [t]
  );

  const handleOnChangePage = () => {
    setLoadingMore(true);
    setTimeout(() => {
      setPage(page + 1);
      setLoadingMore(false);
    }, 300);
  };

  return (
    <div className="p-24">
      <h5 className="pb-16">
        {t('txt_pricing_methods_list_impacted_strategies')}
      </h5>
      <Grid
        columns={columns}
        data={strategiesImpactedOrdered.slice(0, page * LOAD_MORE_PAGE_SIZE)}
      />
      {total! > LOAD_MORE_PAGE_SIZE && (
        <LoadMore
          loading={loadingMore}
          onLoadMore={handleOnChangePage}
          isEnd={page * LOAD_MORE_PAGE_SIZE >= total!}
          className="mt-24"
          endLoadMoreText={t('txt_pricing_methods_end_of_methods_updated')}
        />
      )}
    </div>
  );
};

export default StrategiesImpactedGrid;
