import { RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import {
  PricingMethodData,
  PricingMethodMapping
} from 'app/fixtures/pricing-method-data';
import { mappingDataFromArray } from 'app/helpers';
import { renderComponent, spyOnTranslationHoook } from 'app/utils';
import 'app/utils/_mockComponent/mockDofView';
import 'app/utils/_mockComponent/mockLoadMore';
import 'app/utils/_mockComponent/mockModalRegistry';
import React from 'react';
import DetailModal, { IDetailProps } from '.';

const testId = 'detail-test-id';

let mockPricingMethodData: IPricingMethod[] = [];
mockPricingMethodData = mappingDataFromArray(
  PricingMethodData,
  PricingMethodMapping
)?.map(d => ({
  ...d,
  versionCreating: d.modeledFrom ? 'cloned' : 'new',
  parametersUpdated: d.parameters?.length || 0
}));

export const DetailMock: IDetailProps = {
  show: true,
  details: mockPricingMethodData[2],
  onCloseDetails: () => {}
};
describe('DetailModal', () => {
  let renderResult: RenderResult;
  beforeEach(() => {
    spyOnTranslationHoook();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('ParameterUpdate', () => {
    it('should render detail', async () => {
      const mockData = {
        show: true,
        onCloseDetails: () => {}
      };
      renderResult = await renderComponent(
        testId,
        <DetailModal {...mockData} />
      );
      await renderResult.findByTestId(testId);
      expect(
        document.body.querySelector('div.dls-modal-title')!.textContent
      ).toEqual('txt_pricing_methods_detail_title');
    });

    it('should render detail with parameters is not array', async () => {
      const mockData = {
        show: true,
        details: {
          parameters: {}
        },
        onCloseDetails: () => {}
      } as any;
      renderResult = await renderComponent(
        testId,
        <DetailModal {...mockData} />
      );
      await renderResult.findByTestId(testId);
      expect(
        document.body.querySelector('div.dls-modal-title')!.textContent
      ).toEqual('txt_pricing_methods_detail_title');
    });

    it('should render empty detail', async () => {
      renderResult = await renderComponent(
        testId,
        <DetailModal {...DetailMock} show={false} details={undefined} />
      );
      await renderResult.findByTestId(testId);
      expect(
        document.body.querySelectorAll('div.dls-modal-title').length
      ).toEqual(0);
    });

    it('load more data ', async () => {
      jest.useFakeTimers();
      DetailMock.details = {
        ...DetailMock.details,
        parametersUpdated: 50
      } as any;
      renderResult = await renderComponent(
        testId,
        <DetailModal {...DetailMock} />
      );
      await renderResult.findByTestId(testId);
      const loadMoreBtn = document.body.querySelector('button.btn-load-more');
      userEvent.click(loadMoreBtn!);
      expect(
        document.body.querySelector('.load-more-cont')!.className
      ).toContain('loading');
      jest.advanceTimersByTime(300);
      expect(
        document.body.querySelector('.load-more-cont')!.className
      ).not.toContain('loading');
    });
  });

  describe('StrategiesImpacted', () => {
    const strategiesImpactedMock = {
      ...DetailMock.details,
      strategiesImpactedCount: 10
    } as any;
    beforeEach(() => {
      spyOnTranslationHoook();
    });

    it('should render detail', async () => {
      renderResult = await renderComponent(
        testId,
        <DetailModal {...DetailMock} details={strategiesImpactedMock} />
      );
      await renderResult.findByTestId(testId);
      expect(
        document.body.querySelector('div.dls-modal-title')!.textContent
      ).toEqual('txt_pricing_methods_detail_title');
    });

    it('load more data ', async () => {
      jest.useFakeTimers();

      renderResult = await renderComponent(
        testId,
        <DetailModal
          {...DetailMock}
          details={{
            ...strategiesImpactedMock,
            parametersUpdated: 50,
            strategiesImpactedCount: 50
          }}
        />
      );
      await renderResult.findByTestId(testId);
      const loadMoreBtn = document.body.querySelector('button.btn-load-more');
      userEvent.click(loadMoreBtn!);
      expect(
        document.body.querySelector('.load-more-cont')!.className
      ).toContain('loading');
      jest.advanceTimersByTime(300);
      expect(
        document.body.querySelector('.load-more-cont')!.className
      ).not.toContain('loading');
    });
  });
});
