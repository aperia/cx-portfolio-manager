import ModalRegistry from 'app/components/ModalRegistry';
import {
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import React from 'react';
import ParametersUpdatedBody from './ParametersUpdatedBody';
import StrategiesImpactedBody from './StrategiesImpactedBody';

export interface IDetailProps {
  show: boolean;
  details?: IPricingMethod;
  onCloseDetails: () => void;
}
const DetailModal: React.FC<IDetailProps> = ({
  show,
  details,
  onCloseDetails
}) => {
  const { t } = useTranslation();
  const { id, strategiesImpactedCount } = details || {};
  const isVersioned = strategiesImpactedCount && strategiesImpactedCount > 0;

  return (
    <ModalRegistry
      id={`pricing-strategy-detail-${id}`}
      xs
      rt
      enforceFocus={false}
      scrollable={false}
      show={show}
      animationDuration={500}
      animationComponentProps={{ direction: 'right' }}
      classes={{
        dialog: 'window-sm'
      }}
      onAutoClosedAll={onCloseDetails}
    >
      <ModalHeader border closeButton onHide={onCloseDetails}>
        <ModalTitle>{t('txt_pricing_methods_detail_title')}</ModalTitle>
      </ModalHeader>
      <ModalBody className="p-0 overflow-auto">
        {isVersioned ? (
          <StrategiesImpactedBody details={details} />
        ) : (
          <ParametersUpdatedBody details={details} />
        )}
      </ModalBody>
      <ModalFooter border cancelButtonText="Close" onCancel={onCloseDetails} />
    </ModalRegistry>
  );
};

export default DetailModal;
