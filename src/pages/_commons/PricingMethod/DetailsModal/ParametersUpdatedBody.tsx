import { SimpleBar } from 'app/_libraries/_dls';
import { View } from 'app/_libraries/_dof/core';
import React from 'react';
import ParametersUpdatedGrid from './ParametersUpdatedGrid';

export interface IParametersUpdatedBodyProps {
  details?: IPricingMethod;
}
const ParametersUpdatedBody: React.FC<IParametersUpdatedBodyProps> = ({
  details
}) => {
  const { id } = details || {};

  return (
    <SimpleBar>
      <div className="bg-light-l20 px-24 pt-20 pb-24 border-bottom">
        <View
          id={`change-pricing-method-details__${id}`}
          formKey={`change-pricing-method-details__${id}`}
          descriptor="change-pricing-method-details"
          value={details}
        />
      </div>
      <ParametersUpdatedGrid details={details} />
    </SimpleBar>
  );
};

export default ParametersUpdatedBody;
