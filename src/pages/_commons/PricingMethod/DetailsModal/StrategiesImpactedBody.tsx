import { HorizontalTabs, SimpleBar } from 'app/_libraries/_dls';
import { View } from 'app/_libraries/_dof/core';
import React from 'react';
import ParametersUpdatedGrid from './ParametersUpdatedGrid';
import StrategiesImpactedGrid from './StrategiesImpactedGrid';

export interface IStrategiesImpactedBodyProps {
  details?: IPricingMethod;
}
const StrategiesImpactedBody: React.FC<IStrategiesImpactedBodyProps> = ({
  details
}) => {
  const { id } = details || {};

  return (
    <>
      <div className="bg-light-l20 px-24 py-24">
        <View
          id={`change-pricing-method-details-with-strategies__${id}`}
          formKey={`change-pricing-method-details-with-strategies__${id}`}
          descriptor="change-pricing-method-details-with-strategies"
          value={details}
        />
      </div>

      <HorizontalTabs className="bg-light-l20 bg-inherit" level2>
        <HorizontalTabs.Tab
          eventKey="pricingStrategiesImpacted"
          title="Pricing Strategies Impacted"
        >
          <div className="pricing-method-detail">
            <SimpleBar>
              <StrategiesImpactedGrid details={details} />
            </SimpleBar>
          </div>
        </HorizontalTabs.Tab>
        <HorizontalTabs.Tab
          eventKey="parametersUpdated"
          title="Parameters Updated"
        >
          <div className="pricing-method-detail">
            <SimpleBar>
              <ParametersUpdatedGrid details={details} />
            </SimpleBar>
          </div>
        </HorizontalTabs.Tab>
      </HorizontalTabs>
    </>
  );
};

export default StrategiesImpactedBody;
