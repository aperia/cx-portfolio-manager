import React from 'react';

jest.mock('pages/_commons/PricingMethod/DetailsModal', () => {
  return {
    __esModule: true,
    default: ({ onCloseDetails }: any) => {
      return (
        <div>
          <p>Mock Pricing Detail</p>
          <button className="detail-close-btn" onClick={onCloseDetails}>
            Close Detail
          </button>
        </div>
      );
    }
  };
});
