import { RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { renderComponent, spyOnTranslationHoook, spyOnUseRef } from 'app/utils';
import React from 'react';
import PricingMethodHeader from './Header';

const testId = 'header-test-id';

export const HeaderMock = {
  id: 'header-list',
  title: 'Mock title',
  searchValue: 'test'
};
// jest.spyOn(React, 'useRef').mockReturnValue({
//   current: {
//     clear: () => {}
//   }
// });
describe('PricingMethodHeader', () => {
  let renderResult: RenderResult;

  beforeEach(() => {
    spyOnUseRef({
      current: {
        clear: () => {}
      }
    });
    spyOnTranslationHoook();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render header', async () => {
    renderResult = await renderComponent(
      testId,
      <PricingMethodHeader {...HeaderMock} />
    );
    const wrapper = await renderResult.findByTestId(testId);
    expect(wrapper.querySelector('h4')!.textContent).toEqual('Mock title');
  });

  it('click on search icon', async () => {
    const onSearchFn = jest.fn();
    renderResult = await renderComponent(
      testId,
      <PricingMethodHeader
        {...HeaderMock}
        onSearch={onSearchFn}
        searchValue={''}
      />
    );
    const wrapper = await renderResult.findByTestId(testId);
    const iconBtn = wrapper.querySelector('i.icon.icon-search');
    userEvent.click(iconBtn!);
    expect(onSearchFn).toHaveBeenCalled();
  });
});
