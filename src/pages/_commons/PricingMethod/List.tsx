import FailedApiReload from 'app/components/FailedApiReload';
import NoDataFound from 'app/components/NoDataFound';
import { PRICING_METHODS_SORT_BY_LIST } from 'app/constants/constants';
import { classnames, mapVersionCreatingValue } from 'app/helpers';
import { useTranslation } from 'app/_libraries/_dls';
import { View } from 'app/_libraries/_dof/core';
import isEmpty from 'lodash.isempty';
import {
  actionsPricingMethods,
  defaultDataFilter,
  useSelectPricingMethods,
  useSelectPricingMethodsFilter
} from 'pages/_commons/redux/PricingMethod';
import ButtonGroupFilter from 'pages/_commons/Utils/ButtonGroupFilter';
import Paging from 'pages/_commons/Utils/Paging';
import SortOrder from 'pages/_commons/Utils/SortOrder';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useSelectWindowDimension } from '../redux/Common';
import DetailModal from './DetailsModal';
import PricingMethodHeader from './Header';

interface IProps {
  changeId?: string;
  containerClassName?: string;
  hideBreadCrumb?: boolean;
  hideHeader?: boolean;
  hideViewMode?: boolean;
  hideFilter?: boolean;
  title?: string;
  maxDataNumber?: number;
}
const PricingMethodList: React.FC<IProps> = ({
  changeId,
  containerClassName,
  hideHeader,
  hideFilter,
  title,
  maxDataNumber
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const buttons = useMemo(
    () => [
      {
        id: 'all',
        label: 'All',
        value: '',
        tooltip: ''
      },
      {
        id: 'pricingMethodsCreated',
        label: t('txt_created'),
        value: 'new',
        tooltip:
          'This refers to creating a new method in the 3270 screen after pressing PF2 while viewing a method.'
      },
      {
        id: 'pricingMethodsUpdated',
        label: t('txt_updated'),
        value: 'cloned',
        tooltip:
          'This refers to creating a new version of a method in the 3270 after pressing PF2 while viewing a method.'
      }
    ],
    [t]
  );

  const { width } = useSelectWindowDimension();
  const [detailsShow, setDetailsShow] = useState<boolean>(false);
  const [details, setDetails] = useState<IPricingMethod>();
  const pricingMethodsInfo = useSelectPricingMethods();
  const dataFilter = useSelectPricingMethodsFilter();

  const { loading, error, pricingMethods, totalItem, totalItemFilter } =
    pricingMethodsInfo;
  const mappedPricingMethods = pricingMethods.map(item => ({
    ...item,
    versionCreating: mapVersionCreatingValue(item.versionCreating)
  }));
  const { page, pageSize, searchValue, searchFields } = dataFilter;
  const hasFilterOnGroupBtn = !isEmpty(searchFields);

  const handleChangePage = useCallback(
    (pageNumber: number) => {
      dispatch(
        actionsPricingMethods.updateFilter({
          page: pageNumber
        })
      );
    },
    [dispatch]
  );
  const handleChangePageSize = useCallback(
    (pageSizeNumber: number) => {
      dispatch(
        actionsPricingMethods.updateFilter({
          pageSize: pageSizeNumber
        })
      );
    },
    [dispatch]
  );

  const handleSearch = useCallback(
    (searchInputValue: string) => {
      dispatch(
        actionsPricingMethods.updateFilter({
          searchValue: searchInputValue
        })
      );
    },
    [dispatch]
  );

  const handleSort = useCallback(
    (sortByValue: string) => {
      dispatch(
        actionsPricingMethods.updateFilter({
          sortBy: [sortByValue]
        })
      );
    },
    [dispatch]
  );
  const handleOrder = useCallback(
    (orderByValue: string) => {
      dispatch(
        actionsPricingMethods.updateFilter({
          orderBy: [orderByValue as OrderByType]
        })
      );
    },
    [dispatch]
  );

  const handleFilter = useCallback(
    (filterValue: string) => {
      dispatch(
        actionsPricingMethods.updateFilter({
          searchFields: filterValue ? [filterValue] : []
        })
      );
    },
    [dispatch]
  );

  const handleClearFilter = useCallback(() => {
    dispatch(actionsPricingMethods.clearAndReset());
  }, [dispatch]);

  const handleApiReload = () => {
    changeId && dispatch(actionsPricingMethods.getPricingMethods(changeId));
  };

  const onShowDetails = (item: IPricingMethod) => {
    setDetailsShow(true);
    setDetails(item);
  };

  const onCloseDetails = () => {
    setDetailsShow(false);
  };

  useEffect(() => {
    changeId && dispatch(actionsPricingMethods.getPricingMethods(changeId));
    if (maxDataNumber) {
      dispatch(
        actionsPricingMethods.updateFilter({
          pageSize: maxDataNumber
        })
      );
    }
    return () => {
      dispatch(actionsPricingMethods.setToDefault());
    };
  }, [dispatch, changeId, maxDataNumber]);

  const pricingMethodsList = useMemo(
    () => (
      <div className="mt-4">
        {mappedPricingMethods.map((item: IPricingMethod) => (
          <div
            key={`change-pricing-methods-card-view__${item.id || 'empty'}`}
            className="list-view mt-12"
            onClick={() => {
              onShowDetails(item);
            }}
          >
            <View
              id={`change-pricing-methods-card-view__${item.id || 'empty'}`}
              formKey={`change-pricing-methods-card-view__${
                item.id || 'empty'
              }`}
              descriptor="change-pricing-methods-card-view"
              value={item}
            />
          </div>
        ))}
      </div>
    ),
    [mappedPricingMethods]
  );

  return (
    <div
      id={`pricing-method-list-${changeId}`}
      className={classnames({ 'loading mh-320': loading }, containerClassName)}
    >
      {!hideHeader && (
        <PricingMethodHeader
          id={changeId || 'pricing-method-header'}
          title={title}
          showSearch={totalItem > 0}
          searchValue={searchValue}
          onSearch={handleSearch}
        />
      )}
      {!error && !loading && (
        <>
          {totalItem > 0 && (
            <>
              {!hideFilter && (
                <div className="d-flex align-items-center mt-16 pb-4">
                  <ButtonGroupFilter
                    id={changeId || 'pricing-method-filter'}
                    buttonList={buttons}
                    defaultSelectedValue={
                      dataFilter?.searchFields ? dataFilter.searchFields[0] : ''
                    }
                    onButtonSelectedChange={handleFilter}
                  />
                  {totalItemFilter > 0 && (
                    <SortOrder
                      small={
                        (hasFilterOnGroupBtn || !!searchValue) && width <= 1120
                      }
                      hasFilter={hasFilterOnGroupBtn || !!searchValue}
                      dataFilter={dataFilter}
                      defaultDataFilter={defaultDataFilter}
                      sortByFields={PRICING_METHODS_SORT_BY_LIST}
                      onChangeSortBy={handleSort}
                      onChangeOrderBy={handleOrder}
                      onClearSearch={handleClearFilter}
                    />
                  )}
                </div>
              )}
            </>
          )}

          {totalItemFilter > 0 && pricingMethodsList}
          {!hideFilter && totalItemFilter > 0 && (
            <Paging
              page={page}
              pageSize={pageSize}
              totalItem={totalItemFilter}
              onChangePage={handleChangePage}
              onChangePageSize={handleChangePageSize}
            />
          )}
          {detailsShow && (
            <DetailModal
              show={detailsShow}
              details={details}
              onCloseDetails={onCloseDetails}
            />
          )}
          {(totalItem === 0 || totalItemFilter === 0) && (
            <div className="d-flex flex-column justify-content-center mt-80 mb-32">
              <NoDataFound
                id="change-pricing-methods"
                title={t('txt_pricing_methods_no_data_to_display')}
                hasSearch={!!searchValue}
                hasFilter={hasFilterOnGroupBtn}
                linkTitle={
                  (hasFilterOnGroupBtn || !!searchValue) &&
                  t('txt_clear_and_reset')
                }
                onLinkClicked={handleClearFilter}
              />
            </div>
          )}
        </>
      )}

      {!loading && error && (
        <FailedApiReload
          id="change-details--pricing-methods-tab--error"
          onReload={handleApiReload}
          className="mt-40 pt-40 d-flex flex-column justify-content-center align-items-center"
        />
      )}
    </div>
  );
};

export default PricingMethodList;
