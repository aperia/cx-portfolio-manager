import { fireEvent, screen } from '@testing-library/dom';
import { RenderResult } from '@testing-library/react';
import { cleanup } from '@testing-library/react-hooks';
import {
  PricingMethodData,
  PricingMethodMapping
} from 'app/fixtures/pricing-method-data';
import { mappingDataFromArray } from 'app/helpers';
import { PricingMethodsState } from 'app/types';
import { renderWithMockStore, spyOnTranslationHoook } from 'app/utils';
import 'app/utils/_mockComponent/mockButtonGroupFilter';
import 'app/utils/_mockComponent/mockDofView';
import 'app/utils/_mockComponent/mockFailedApiReload';
import 'app/utils/_mockComponent/mockNoDataFound';
import 'app/utils/_mockComponent/mockPaging';
import 'app/utils/_mockComponent/mockSortOrder';
import 'pages/_commons/PricingMethod/mockPricingMethodDetail';
import 'pages/_commons/PricingMethod/mockPricingMethodHeader';
import * as CommonRedux from 'pages/_commons/redux/Common/select-hooks';
import * as PricingMethodSelector from 'pages/_commons/redux/PricingMethod/selector';
import React from 'react';
import { defaultDataFilter } from '../redux/PricingMethod';
import PricingMethodList from './List';

const mockUseSelectWindowDimension = jest.spyOn(
  CommonRedux,
  'useSelectWindowDimension'
);

const mockUseSelectWindowDimensionImplementation = (width: number) => {
  mockUseSelectWindowDimension.mockImplementation(() => ({ width } as any));
};

const mockUseSelectPricingMethods = ({
  pricingMethods,
  loading,
  error,
  totalItem,
  totalItemFilter
}: any) => {
  jest.spyOn(PricingMethodSelector, 'useSelectPricingMethods').mockReturnValue({
    pricingMethods,
    loading,
    error,
    totalItem,
    totalItemFilter
  });
};

const mockUseSelectPricingMethodsFilter = (dataFilter: any) => {
  jest
    .spyOn(PricingMethodSelector, 'useSelectPricingMethodsFilter')
    .mockReturnValue(dataFilter);
};

const testId = 'list-test-id';

let mockPricingMethodData: IPricingMethod[] = [];
mockPricingMethodData = mappingDataFromArray(
  PricingMethodData,
  PricingMethodMapping
)?.map(d => ({
  ...d,
  versionCreating: d.modeledFrom ? 'cloned' : 'new',
  parametersUpdated: d.parameters?.length || 0
}));

export const ListMock: any = {
  changeId: '123',
  containerClassName: 'container',
  title: 'Pricing method title'
};

describe('PricingMethodList', () => {
  let renderResult: RenderResult;
  let storeCofig: any = {};
  beforeEach(() => {
    mockUseSelectWindowDimensionImplementation(1300);
    storeCofig = {
      initialState: {
        pricingMethods: {
          changePricingMethods: {
            pricingMethods: mockPricingMethodData,
            loading: false,
            dataFilter: defaultDataFilter,
            error: false
          }
        } as PricingMethodsState
      }
    };
    spyOnTranslationHoook();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render pricing method list', async () => {
    mockUseSelectPricingMethods({
      pricingMethods: mockPricingMethodData,
      loading: false,
      error: false,
      totalItem: 30,
      totalItemFilter: 30
    });
    mockUseSelectPricingMethodsFilter(defaultDataFilter);
    renderResult = await renderWithMockStore(
      <PricingMethodList {...ListMock} />,
      storeCofig,
      {},
      testId
    );
    const wrapper = await renderResult.findByTestId(testId);
    expect(
      wrapper.querySelector('#pricing-method-list-' + ListMock.changeId)!
    ).toBeInTheDocument();
    fireEvent.click(screen.getByText('Filter 1'));
    fireEvent.click(screen.getByText('Filter Empty'));
    mockUseSelectPricingMethodsFilter({
      ...defaultDataFilter,
      searchFields: ['test search field']
    });
    cleanup();
    renderResult = await renderWithMockStore(
      <PricingMethodList {...ListMock} />,
      storeCofig,
      {},
      testId
    );
    expect(screen.getByText('test search field')).toBeInTheDocument();
  });

  it('should render pricing method list with empty id', async () => {
    mockUseSelectPricingMethods({
      pricingMethods: mockPricingMethodData.map((p: any) => ({
        ...p,
        id: undefined
      })),
      loading: false,
      error: false,
      totalItem: 30,
      totalItemFilter: 30
    });
    mockUseSelectPricingMethodsFilter({
      ...defaultDataFilter,
      searchFields: ['test search field']
    });
    renderResult = await renderWithMockStore(
      <PricingMethodList
        {...ListMock}
        changeId={undefined}
        maxDataNumber={5}
      />,
      storeCofig,
      {},
      testId
    );
    const wrapper = await renderResult.findByTestId(testId);

    expect(
      wrapper.querySelector('#pricing-method-list-undefined')!
    ).toBeInTheDocument();
  });

  it('should render no data found', async () => {
    mockUseSelectPricingMethods({
      pricingMethods: mockPricingMethodData,
      loading: false,
      error: false,
      totalItem: 0,
      totalItemFilter: 0
    });
    mockUseSelectPricingMethodsFilter(defaultDataFilter);
    renderResult = await renderWithMockStore(
      <PricingMethodList {...ListMock} />,
      storeCofig,
      {},
      testId
    );
    const { getByText } = renderResult;
    expect(getByText('No data found')).toBeInTheDocument();
  });

  it('should render no data found with search', async () => {
    mockUseSelectPricingMethods({
      pricingMethods: mockPricingMethodData,
      loading: false,
      error: false,
      totalItem: 0,
      totalItemFilter: 0
    });
    mockUseSelectPricingMethodsFilter({
      ...defaultDataFilter,
      searchValue: 'test value'
    });
    renderResult = await renderWithMockStore(
      <PricingMethodList {...ListMock} />,
      storeCofig,
      {},
      testId
    );
    const { getByText } = renderResult;
    expect(getByText('No data found')).toBeInTheDocument();
  });

  it('should render data load unsuccessfully', async () => {
    mockUseSelectPricingMethods({
      pricingMethods: mockPricingMethodData,
      loading: false,
      error: true,
      totalItem: 0,
      totalItemFilter: 0
    });
    mockUseSelectPricingMethodsFilter({
      ...defaultDataFilter
    });
    renderResult = await renderWithMockStore(
      <PricingMethodList {...ListMock} />,
      storeCofig,
      {},
      testId
    );
    const { getByText } = renderResult;
    fireEvent.click(screen.getByText('Reload'));
    expect(getByText('Data load unsuccessful')).toBeInTheDocument();
  });
  it('should show detail on list item clicked', async () => {
    mockUseSelectPricingMethods({
      pricingMethods: mockPricingMethodData,
      loading: false,
      error: false,
      totalItem: 40,
      totalItemFilter: 40
    });
    mockUseSelectPricingMethodsFilter({
      ...defaultDataFilter
    });
    renderResult = await renderWithMockStore(
      <PricingMethodList {...ListMock} />,
      storeCofig,
      {},
      testId
    );
    const { getByText } = renderResult;
    expect(
      getByText(
        `change-pricing-methods-card-view__${mockPricingMethodData[0].id}`
      )
    ).toBeInTheDocument();
    fireEvent.click(
      getByText(
        `change-pricing-methods-card-view__${mockPricingMethodData[0].id}`
      )
    );
    expect(getByText(`Mock Pricing Detail`)).toBeInTheDocument();
    fireEvent.click(getByText('Close Detail'));
    expect(screen.queryByText(`Mock Pricing Detail`)).toBeNull();
  });
  it('filter on pricing method list', async () => {
    mockUseSelectPricingMethods({
      pricingMethods: mockPricingMethodData,
      loading: false,
      error: false,
      totalItem: 30,
      totalItemFilter: 30
    });
    mockUseSelectPricingMethodsFilter(defaultDataFilter);
    renderResult = await renderWithMockStore(
      <PricingMethodList {...ListMock} />,
      storeCofig,
      {},
      testId
    );

    fireEvent.click(screen.getByText('List search'));
    fireEvent.click(screen.getByText('Sort By'));
    fireEvent.click(screen.getByText('Order By'));
    fireEvent.click(screen.getByText('Clear And Reset'));
    fireEvent.click(screen.getByText('Page 2'));
    fireEvent.click(screen.getByText('Page Size 25'));
    expect(screen.getByTestId(testId)!).toBeInTheDocument();
  });
});
