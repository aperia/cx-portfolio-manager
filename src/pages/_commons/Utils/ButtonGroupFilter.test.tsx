import { RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { renderComponent } from 'app/utils';
import React from 'react';
import ButtonGroupFilter, { IButtonGroupFilter } from './ButtonGroupFilter';

const testId = 'button-group-filter-test';

export const ButtonGroupFilterMock = {
  id: 'button-group-filter-1',
  buttonList: [
    {
      id: '',
      label: 'All',
      value: '',
      tooltip: ''
    },
    {
      id: 'btn1',
      label: 'Button 01',
      value: 'btn1',
      tooltip: 'Button 01 tooltip'
    },
    {
      id: 'btn2',
      label: 'Button 02',
      value: 'btn2',
      tooltip: 'Button 02 tooltip'
    }
  ] as IButtonGroupFilter[],
  defaultSelectedValue: 'btn1'
};

describe('ButtonGroupFilter', () => {
  let renderResult: RenderResult;

  const createWrapper = async (...props: any[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    renderResult = await renderComponent(
      testId,
      <ButtonGroupFilter {...mergeProps} />
    );
  };

  it('should render 3 buttons in group > default select on button #2 ', async () => {
    await createWrapper(ButtonGroupFilterMock);
    const wrapper = await renderResult.findByTestId(testId);
    expect(wrapper.querySelectorAll('button')!.length).toEqual(3);
    expect(wrapper.querySelectorAll('button')![1].className).toContain(
      'active'
    );
  });

  it('should use default ID and default selected button ', async () => {
    await createWrapper({
      buttonList: ButtonGroupFilterMock.buttonList
    });
    const wrapper = await renderResult.findByTestId(testId);
    expect(wrapper.querySelectorAll('button')![0].className).toContain(
      'active'
    );
  });

  it('click on second button', async () => {
    const onButtonSelectedChangeFn = jest.fn();
    await createWrapper({
      buttonList: ButtonGroupFilterMock.buttonList,
      onButtonSelectedChange: onButtonSelectedChangeFn
    });
    const wrapper = await renderResult.findByTestId(testId);

    const secondBtnElement = wrapper.querySelectorAll('button')![1];

    userEvent.click(secondBtnElement);

    expect(secondBtnElement.className).toContain('active');

    expect(onButtonSelectedChangeFn).toHaveBeenCalled();
  });
});
