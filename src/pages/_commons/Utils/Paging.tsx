import { PAGE_SIZE } from 'app/constants/constants';
import { pageSizeInfo, Pagination } from 'app/_libraries/_dls';
import { DEFAULT_PAGE_NUMBER } from 'app/_libraries/_dls/components/Pagination/constants';
import isFunction from 'lodash.isfunction';
import React, { useCallback } from 'react';

export interface IPagination {
  page?: number;
  pageSize?: number;
  totalItem?: number;
  onChangePage?: (page: number) => void;
  onChangePageSize?: (pageSize: number) => void;
}

const Paging: React.FC<IPagination> = ({
  page = DEFAULT_PAGE_NUMBER,
  pageSize = PAGE_SIZE[0],
  totalItem = 0,
  onChangePage,
  onChangePageSize
}) => {
  // pagination
  const handleOnChangePage = useCallback(
    (pageNumber: number) => {
      isFunction(onChangePage) && onChangePage(pageNumber);
    },
    [onChangePage]
  );
  const handleOnChangePageSize = useCallback(
    (pageInfo: pageSizeInfo) => {
      isFunction(onChangePageSize) && onChangePageSize(pageInfo.size);
    },
    [onChangePageSize]
  );

  return totalItem > PAGE_SIZE[0] ? (
    <div className="mt-16">
      <Pagination
        totalItem={totalItem}
        pageSize={PAGE_SIZE}
        pageNumber={page}
        pageSizeValue={pageSize}
        onChangePage={handleOnChangePage}
        onChangePageSize={handleOnChangePageSize}
      />
    </div>
  ) : (
    <React.Fragment></React.Fragment>
  );
};

export default Paging;
