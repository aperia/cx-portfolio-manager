import { RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { renderComponent } from 'app/utils';
import 'app/utils/_mockComponent/mockDlsPagination';
import React from 'react';
import Paging, { IPagination } from './Paging';

const testId = 'pagination-test';

export const PagingMock: IPagination = {
  page: 1,
  pageSize: 10,
  totalItem: 100
};
describe('Paging', () => {
  let renderResult: RenderResult;

  afterAll(() => {
    jest.clearAllMocks();
  });

  const createWrapper = async (...props: IPagination[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    renderResult = await renderComponent(testId, <Paging {...mergeProps} />);
  };

  it('should render paging with 3 page ', async () => {
    await createWrapper(PagingMock);
    const wrapper = await renderResult.findByTestId(testId);

    expect(wrapper.querySelector('.page-number')!.textContent).toEqual('1');
    expect(wrapper.querySelector('.total-item')!.textContent).toEqual('100');
  });

  it('should render empty component', async () => {
    await createWrapper({});
    const wrapper = await renderResult.findByTestId(testId);

    expect(wrapper.querySelectorAll('.page-number')!.length).toEqual(0);
  });

  it('change page & change page size', async () => {
    const onChangePageFn = jest.fn();
    const onChangePageSizeFn = jest.fn();
    await createWrapper({
      ...PagingMock,
      onChangePage: onChangePageFn,
      onChangePageSize: onChangePageSizeFn
    });
    const wrapper = await renderResult.findByTestId(testId);
    const changePageBtn = wrapper.querySelector('button.change-page-number');
    userEvent.click(changePageBtn!);
    expect(onChangePageFn).toHaveBeenCalled();

    const changePageSizeBtn = wrapper.querySelector('button.change-page-size');
    userEvent.click(changePageSizeBtn!);
    expect(onChangePageSizeFn).toHaveBeenCalled();
  });
});
