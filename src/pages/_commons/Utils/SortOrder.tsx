import SortAndOrder, {
  OrderByItemProps,
  SortByItemProps
} from 'app/components/SortAndOrder';
import { MIN_WIDTH_PC, ORDER_BY_LIST } from 'app/constants/constants';
import { classnames } from 'app/helpers';
import React, { useCallback, useMemo } from 'react';
import { isFunction } from 'util';
import { useSelectWindowDimension } from '../redux/Common';
import ClearAndResetButton from './ClearAndResetButton';

export interface ISortOrder {
  small?: boolean;
  hasFilter?: boolean;
  sortByFields: SortByItemProps[];
  dataFilter: IDataFilter;
  defaultDataFilter: IDataFilter;
  onChangeSortBy?: (sortBy: string) => void;
  onChangeOrderBy?: (orderBy: string) => void;
  onClearSearch?: () => void;
  isHideSort?: boolean;
}

const SortOrder: React.FC<ISortOrder> = ({
  hasFilter,
  sortByFields,
  dataFilter,
  defaultDataFilter,
  onChangeSortBy,
  onChangeOrderBy,
  onClearSearch,
  small,
  isHideSort = false
}) => {
  const { sortBy, orderBy } = dataFilter;
  const { width } = useSelectWindowDimension();
  const isSmall = useMemo(
    () => width <= MIN_WIDTH_PC || !!small,
    [small, width]
  );

  // sort by
  const handleChangeSortBy = useCallback(
    (sortByValue: SortByItemProps | undefined) => {
      isFunction(onChangeSortBy) && onChangeSortBy!(sortByValue!.value);
    },
    [onChangeSortBy]
  );

  // order by
  const handleChangeOrderBy = useCallback(
    (orderByValue: OrderByItemProps | undefined) => {
      isFunction(onChangeOrderBy) && onChangeOrderBy!(orderByValue!.value);
    },
    [onChangeOrderBy]
  );

  const handleClearAndReset = useCallback(() => {
    isFunction(onClearSearch) && onClearSearch!();
  }, [onClearSearch]);

  return (
    <div
      className={classnames('d-flex align-items-start ml-auto mr-n4', {
        'mr-n8': !isSmall && !hasFilter
      })}
    >
      <SortAndOrder
        small={small}
        sortByFields={sortByFields}
        sortByValueDefault={sortByFields.find(
          i => i.value === defaultDataFilter.sortBy![0]
        )}
        orderByValueDefault={ORDER_BY_LIST.find(
          i => i.value === defaultDataFilter.orderBy![0]
        )}
        sortByValue={sortByFields.find(i => i.value === sortBy![0])}
        orderByValue={ORDER_BY_LIST.find(i => i.value === orderBy![0])}
        onSortByChange={handleChangeSortBy}
        onOrderByChange={handleChangeOrderBy}
        isHideSort={isHideSort}
      />
      {hasFilter && (
        <div className="ml-16 mr-n4">
          <ClearAndResetButton small onClearAndReset={handleClearAndReset} />
        </div>
      )}
    </div>
  );
};

export const DEFAULT_SORT: {
  id: string;
  order: 'asc' | 'desc' | undefined;
} = { id: 'name', order: 'asc' };

export default SortOrder;
