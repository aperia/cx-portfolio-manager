import { RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { SortByItemProps } from 'app/components/SortAndOrder';
import { mockUseSelectWindowDimension, renderComponent } from 'app/utils';
import 'app/utils/_mockComponent/mockSortAndOrder';
import React from 'react';
import SortOrder, { ISortOrder } from './SortOrder';

const testId = 'sort-and-order-test';

export const SortOrderMock: ISortOrder = {
  sortByFields: [
    { description: 'Field 1', value: 'field1' },
    { description: 'Field 2', value: 'field2' },
    { description: 'Field 3', value: 'field3' }
  ] as SortByItemProps[],
  dataFilter: {
    sortBy: ['field2'],
    orderBy: ['desc'],
    searchValue: 'test'
  } as IDataFilter,
  defaultDataFilter: {
    sortBy: ['field1'],
    orderBy: ['desc']
  } as IDataFilter
};

describe('SortOrder', () => {
  let renderResult: RenderResult;

  beforeEach(() => {
    mockUseSelectWindowDimension(1025);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  const createWrapper = async (...props: ISortOrder[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {} as ISortOrder
    );
    renderResult = await renderComponent(testId, <SortOrder {...mergeProps} />);
  };

  it('should render sort order with 3 sortby value ', async () => {
    await createWrapper(SortOrderMock);
    const wrapper = await renderResult.findByTestId(testId);
    expect(wrapper.querySelector('.sort-by-fields')?.children!.length).toEqual(
      3
    );
    expect(
      wrapper.querySelectorAll('.sort-by-field-item')![1].className
    ).toContain('active');
  });

  it('change sort by & order by & clear search', async () => {
    const onChangeSortByFn = jest.fn();
    const onChangeOrderByFn = jest.fn();
    const onClearSearchFn = jest.fn();
    await createWrapper({
      ...SortOrderMock,
      hasFilter: true,
      onChangeSortBy: onChangeSortByFn,
      onChangeOrderBy: onChangeOrderByFn,
      onClearSearch: onClearSearchFn
    });
    const wrapper = await renderResult.findByTestId(testId);
    const changeSortBtn = wrapper.querySelector('button.change-sort-by');
    userEvent.click(changeSortBtn!);
    expect(onChangeSortByFn).toHaveBeenCalled();

    const changePageSizeBtn = wrapper.querySelector('button.change-order-by');
    userEvent.click(changePageSizeBtn!);
    expect(onChangeOrderByFn).toHaveBeenCalled();

    const clearSearchBtn = wrapper.querySelector('button#clear-button');
    userEvent.click(clearSearchBtn!);
    expect(onClearSearchFn).toHaveBeenCalled();
  });
});
