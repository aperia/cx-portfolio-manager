import { fireEvent, render, RenderResult } from '@testing-library/react';
import { BadgeColorType } from 'app/constants/enums';
import React from 'react';
import {
  formatBadge,
  formatBubble,
  formatText,
  formatTruncate
} from './formatGridField';

describe('Test formatGridField Fn Component', () => {
  it('should render GroupTextControl in formatText', async () => {
    let fieldRender = formatText(['dataCount'], {
      format: 'amount',
      formatType: 'quantity',
      truncate: false
    });
    let wrapper: RenderResult = render(
      <div>{fieldRender({ dataCount: 1234 }, 1)}</div>
    );
    expect(wrapper.getByText('1,234')).toBeInTheDocument();

    wrapper.unmount();

    fieldRender = formatText(['name']);
    wrapper = render(<div>{fieldRender({ name: 'Mock name' }, 1)}</div>);
    expect(wrapper.getByText('Mock name')).toBeInTheDocument();
  });

  it('should render GroupTextControl in formatTruncate', async () => {
    const mockLorem =
      'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Animi praesentium ut debitis hic modi ipsa, molestiae accusamus quo illo culpa adipisci temporibus aut nam incidunt quibusdam, error nesciunt officiis dolorem?';

    const fieldRender = formatTruncate(['description']);
    const wrapper: RenderResult = render(
      <div style={{ width: '100px' }}>
        {fieldRender({ description: mockLorem }, 1)}
      </div>
    );
    expect(wrapper.getByText('more')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('more'));
    expect(wrapper.getByText('less')).toBeInTheDocument();
  });

  it('should render BadgeGroupTextControl in formatBadge', async () => {
    const fieldRender = formatBadge(['methodType'], {
      colorType: BadgeColorType.MethodType
    });
    const wrapper: RenderResult = render(
      <div>{fieldRender({ methodType: 'Method Created' }, 1)}</div>
    );
    expect(wrapper.getByText('Method Created')).toBeInTheDocument();
    expect(
      wrapper.getByText('Method Created').parentElement?.className
    ).toContain('badge badge-purple');
  });

  it('should render formatBubble', async () => {
    const fieldRender = formatBubble(['methodType']);
    const wrapper: RenderResult = render(
      <div>{fieldRender({ methodType: 'Method Created' }, 1)}</div>
    );
    expect(wrapper.getByText('Method Created')).toBeInTheDocument();
    expect(
      wrapper.getByText('Method Created').parentElement?.className
    ).toContain('dls-truncate-text');
  });
});
