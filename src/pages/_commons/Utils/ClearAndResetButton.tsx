import { Button, useTranslation } from 'app/_libraries/_dls';
import React from 'react';

interface IClearAndResetButton {
  small?: boolean;
  onClearAndReset: () => void;
}

const ClearAndResetButton: React.FC<IClearAndResetButton> = ({
  small = false,
  onClearAndReset
}) => {
  const { t } = useTranslation();

  return (
    <div className="d-flex justify-content-center">
      <Button
        id="clear-button"
        size={small ? 'sm' : undefined}
        variant="outline-primary"
        onClick={onClearAndReset}
      >
        {t('txt_clear_and_reset')}
      </Button>
    </div>
  );
};

export default ClearAndResetButton;
