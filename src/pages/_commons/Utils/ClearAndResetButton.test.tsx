import { RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { renderComponent } from 'app/utils';
import React from 'react';
import ClearAndResetButton from './ClearAndResetButton';

const testId = 'clear-and-reset';

describe('ClearAndResetButton', () => {
  let renderResult: RenderResult;

  const createWrapper = async (...props: any[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    renderResult = await renderComponent(
      testId,
      <ClearAndResetButton {...mergeProps} />
    );
  };

  it('should render clear & reset button ', async () => {
    await createWrapper({});
    const wrapper = await renderResult.findByTestId(testId);
    expect(wrapper.querySelector('button')!.textContent).toEqual(
      'txt_clear_and_reset'
    );
  });

  it('click on clear & reset button', async () => {
    const onClearAndResetFn = jest.fn();
    await createWrapper({
      onClearAndReset: onClearAndResetFn
    });
    const wrapper = await renderResult.findByTestId(testId);

    userEvent.click(wrapper.querySelector('button')!);

    expect(onClearAndResetFn).toHaveBeenCalled();
  });
});
