import BadgeGroupTextControl from 'app/components/DofControl/BadgeGroupTextControl';
import BubbleGroupTextControl from 'app/components/DofControl/BubbleGroupTextControl';
import GroupTextControl from 'app/components/DofControl/GroupTextControl';
import { isValidDate } from 'app/helpers';
import { Icon, Tooltip } from 'app/_libraries/_dls';
import { isString } from 'lodash';
import get from 'lodash.get';
import React from 'react';

export const formatText = (path: string[], otps?: Record<string, any>) => {
  const fieldRender = (data: Record<string, any>, idx: number) => {
    let value = get(data, path);
    value = isValidDate(value) ? value : value?.toString().trim();

    return (
      <div className="mt-n4">
        <GroupTextControl
          id={`id_${idx}_${path.join('_')}`}
          input={{ value } as any}
          meta={{} as any}
          options={{ inline: true, lineTruncate: 2, ...(otps || {}) }}
        />
      </div>
    );
  };

  return fieldRender;
};

export const formatTruncate = (path: string[]) => {
  const fieldRender = (data: Record<string, any>, idx: number) => {
    let value = get(data, path);
    value = isValidDate(value) ? value : value?.toString().trim();

    return (
      <div className="mt-n4">
        <GroupTextControl
          id={`id_${idx}_${path.join('_')}`}
          input={{ value } as any}
          meta={{} as any}
          options={{
            valueClass: 'pre-line',
            inline: true,
            truncate: true,
            lineTruncate: 2,
            ellipsisMoreText: 'more',
            ellipsisLessText: 'less'
          }}
        />
      </div>
    );
  };

  return fieldRender;
};

export const formatBadge = (path: string[], otps?: Record<string, any>) => {
  const fieldRender = (data: Record<string, any>, idx: number) => {
    let value = get(data, path);
    value = isValidDate(value) ? value : value?.toString().trim();

    return (
      <div className="mt-n2">
        <BadgeGroupTextControl
          id={`id_${idx}_${path.join('_')}`}
          input={{ value } as any}
          meta={{} as any}
          options={otps}
        />
      </div>
    );
  };

  return fieldRender;
};

export const formatBubble = (path: string[]) => {
  const fieldRender = (data: Record<string, any>, idx: number) => {
    let value = get(data, path);
    value = isValidDate(value) ? value : value?.toString().trim();

    return (
      <div className="mt-n2">
        <BubbleGroupTextControl
          id={`id_${idx}_${path.join('_')}`}
          input={{ value } as any}
          meta={{} as any}
        />
      </div>
    );
  };

  return fieldRender;
};

export const viewMoreInfo = (path: string[], t: any) => {
  const fieldRender = (data: Record<string, any>) => {
    let value = get(data, path);
    value = isValidDate(value) ? value?.toString().trim() : value;

    return value ? (
      <Tooltip
        tooltipInnerClassName="pre-line"
        element={isString(value) && !value.includes(' ') ? t(value) : value}
      >
        <Icon name="information" size="5x" className="color-grey-l16" />
      </Tooltip>
    ) : (
      <span></span>
    );
  };

  return fieldRender;
};

export const valueTranslation = (path: string[], t: any) => {
  const fieldRender = (data: Record<string, any>) => {
    const text = get(data, path)?.trim();

    return isString(text) && !text.includes(' ') ? t(text) : text;
  };

  return fieldRender;
};
