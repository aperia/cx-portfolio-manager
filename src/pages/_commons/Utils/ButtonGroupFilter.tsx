import { isDevice } from 'app/helpers';
import { Button, GroupButton, Popover, Tooltip } from 'app/_libraries/_dls';
import isFunction from 'lodash.isfunction';
import React, { useEffect, useState } from 'react';

export interface IButtonGroupFilter {
  id: string;
  label: string;
  value: string;
  tooltip?: string;
}

interface IProps {
  id?: string;
  buttonList: IButtonGroupFilter[];
  defaultSelectedValue?: string;
  onButtonSelectedChange?: (value: string) => void;
}
const ButtonGroupFilter: React.FC<IProps> = ({
  id = 'btn-group-filter',
  buttonList,
  defaultSelectedValue,
  onButtonSelectedChange
}) => {
  const [buttonSelected, setButtonSelected] = useState<string>(
    buttonList[0]?.id
  );
  useEffect(() => {
    setButtonSelected(
      buttonList.find(b => b.value === defaultSelectedValue)?.id ||
        buttonList[0].id
    );
  }, [defaultSelectedValue, buttonList]);

  return (
    <div id={`button-group-filter-${id}`} className="d-flex align-items-center">
      <GroupButton>
        {buttonList.map(({ id: btnId, label, value, tooltip }) => {
          return (
            <Button
              key={btnId}
              id={btnId}
              selected={buttonSelected === btnId}
              onClick={() => {
                setButtonSelected(btnId);
                isFunction(onButtonSelectedChange) &&
                  onButtonSelectedChange(value);
              }}
              size="sm"
              variant="secondary"
            >
              {isDevice && tooltip ? (
                <Popover element={tooltip} placement="top" size="sm">
                  {label}
                </Popover>
              ) : (
                <Tooltip
                  key={btnId}
                  opened={!tooltip ? false : undefined}
                  element={tooltip}
                  placement="top"
                >
                  {label}
                </Tooltip>
              )}
            </Button>
          );
        })}
      </GroupButton>
    </div>
  );
};

export default ButtonGroupFilter;
