import { isString } from 'lodash';

export const newDate = (
  date: string | Date,
  options = {
    setMilliseconds: 0,
    setSeconds: 0,
    setMinutes: 0,
    setHours: 0
  } as MagicKeyValue
) => {
  let result = date;
  if (isString(date)) {
    result = new Date(date);
  }

  Object.keys(options).forEach(func => {
    const value = (options as any)[func];
    (result as any)[func](value);
  });

  return result as Date;
};

export const isEqualDate = (
  date1: string | Date,
  date2: string | Date,
  options?: MagicKeyValue
) => {
  return (
    newDate(date1, options).getTime() === newDate(date2, options).getTime()
  );
};
