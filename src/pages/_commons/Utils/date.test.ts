import { isEqualDate, newDate } from './date';

describe('date', () => {
  it('New Date with milliseconds & seconds & minutes & hours = 0', () => {
    const result = newDate('11/11/2222');

    expect(result.getMilliseconds()).toEqual(0);
    expect(result.getSeconds()).toEqual(0);
    expect(result.getMinutes()).toEqual(0);
    expect(result.getHours()).toEqual(0);
  });

  it('compare 2 string date', () => {
    const date1 = '1/1/2020';
    const date2 = '1/1/2020';
    const result = isEqualDate(date1, date2);

    expect(result).toBeTruthy();
  });
});
