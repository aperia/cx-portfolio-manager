import { fireEvent, render, RenderResult } from '@testing-library/react';
import 'app/utils/_mockComponent/mockDofView';
import 'app/utils/_mockComponent/mockFailedApiReload';
import * as workflowHook from 'pages/_commons/redux/Workflow/select-hooks';
import React from 'react';
import * as reactRedux from 'react-redux';
import WorkflowTemplateList from './index';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');

const mockSelectFavoriteWorkflowTemplate = jest.spyOn(
  workflowHook,
  'useSelectFavoriteWorkflowTemplate'
);
const mockSelectUnfavoriteWorkflowTemplate = jest.spyOn(
  workflowHook,
  'useSelectUnfavoriteWorkflowTemplate'
);
const mockSelectLastUpdatedFavoriteWorkflows = jest.spyOn(
  workflowHook,
  'useSelectLastUpdatedFavoriteWorkflows'
);

describe('Test WorkflowTemplateList Component', () => {
  const mockDispatch = jest.fn();

  const renderWorkflowTemplateList = (): RenderResult => {
    return render(
      <div>
        <WorkflowTemplateList />
      </div>
    );
  };

  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
    mockSelectFavoriteWorkflowTemplate.mockReturnValue({
      loading: false
    });
    mockSelectUnfavoriteWorkflowTemplate.mockReturnValue({
      loading: false
    });
    mockSelectLastUpdatedFavoriteWorkflows.mockReturnValue({
      loading: false,
      error: false,
      workflows: []
    });
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
    mockSelectFavoriteWorkflowTemplate.mockClear();
    mockSelectUnfavoriteWorkflowTemplate.mockClear();
    mockSelectLastUpdatedFavoriteWorkflows.mockClear();
  });

  it('should render WorkflowTemplateList > no data', () => {
    const wrapper = renderWorkflowTemplateList();

    fireEvent.click(wrapper.getByText('txt_add_favorite_workflows'));

    expect(
      wrapper.getByText('txt_no_favorite_workflows_to_display')
    ).toBeInTheDocument();
  });

  it('should render WorkflowTemplateList > has error', () => {
    mockSelectLastUpdatedFavoriteWorkflows.mockReturnValue({
      loading: false,
      error: true,
      workflows: []
    });

    const wrapper = renderWorkflowTemplateList();

    expect(wrapper.getByText('Data load unsuccessful')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('Reload'));
    expect(mockDispatch).toHaveBeenCalled();
  });

  it('should render WorkflowTemplateList > has data', () => {
    mockSelectLastUpdatedFavoriteWorkflows.mockReturnValue({
      loading: false,
      error: false,
      workflows: [
        {
          id: '1',
          name: 'Mock workflow 1'
        },
        {
          id: '2',
          name: 'Mock workflow 2'
        }
      ] as IWorkflowTemplateModel[]
    });

    const wrapper = renderWorkflowTemplateList();

    expect(
      wrapper.baseElement.querySelectorAll('.workflow-template-card-view')
        .length
    ).toEqual(2);
  });
});
