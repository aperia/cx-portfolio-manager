import FailedApi from 'app/components/FailedApiReload';
import NoDataFound from 'app/components/NoDataFound';
import WorkflowTemplateCardView from 'app/components/WorkflowTemplateCardView';
import { classnames } from 'app/helpers';
import { useTranslation } from 'app/_libraries/_dls';
import {
  actionsWorkflow,
  useSelectFavoriteWorkflowTemplate,
  useSelectLastUpdatedFavoriteWorkflows,
  useSelectUnfavoriteWorkflowTemplate
} from 'pages/_commons/redux/Workflow';
import React, { useCallback, useEffect } from 'react';
import { useDispatch } from 'react-redux';

interface IProps {
  containerClassName?: string;
}

const WorkflowTemplateList: React.FC<IProps> = ({ containerClassName }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const { workflows, loading, error } = useSelectLastUpdatedFavoriteWorkflows();
  const { loading: favoriting } = useSelectFavoriteWorkflowTemplate();
  const { loading: unfavoriting } = useSelectUnfavoriteWorkflowTemplate();

  useEffect(() => {
    dispatch(actionsWorkflow.getPageTemplateWorkflows());
  }, [dispatch]);

  const handleApiReload = useCallback(() => {
    dispatch(actionsWorkflow.getPageTemplateWorkflows());
  }, [dispatch]);

  const handleAddFavoriteWorkflow = useCallback(() => {}, []);

  return (
    <div
      className={classnames(
        {
          'loading mh-320': loading || favoriting || unfavoriting
        },
        containerClassName
      )}
    >
      {error ? (
        <FailedApi
          id="homepage-workflow-template-card-error"
          onReload={handleApiReload}
          className="mx-40 mt-40 pt-40 px-40 d-flex flex-column justify-content-center align-items-center"
        />
      ) : workflows?.length > 0 ? (
        workflows.map(item => (
          <div key={item.id} className="pt-4">
            <WorkflowTemplateCardView
              id={`template-${item.id}`}
              value={item}
              isHomePage={true}
            />
          </div>
        ))
      ) : (
        !loading && (
          <div className="mt-80 mb-24">
            <NoDataFound
              id="homepage-workflow-template-not-found"
              className="mx-40 px-40"
              title={t('txt_no_favorite_workflows_to_display')}
              linkTitle={t('txt_add_favorite_workflows')}
              onLinkClicked={handleAddFavoriteWorkflow}
            />
          </div>
        )
      )}
    </div>
  );
};

export default WorkflowTemplateList;
