import { CHANGE_TYPE } from 'app/constants/constants';
import { textToCamel } from 'app/helpers';
import {
  DropdownBaseChangeEvent,
  DropdownList,
  useTranslation
} from 'app/_libraries/_dls';
import includes from 'lodash.includes';
import isFunction from 'lodash.isfunction';
import React, { useCallback, useMemo } from 'react';

export interface IApprovalFilter {
  pendingApprovalType?: string;
  changeTypeList: string[];
  onFilterByType?: (value: string) => void;
}

const PendingApprovalFilter: React.FC<IApprovalFilter> = ({
  pendingApprovalType,
  onFilterByType,
  changeTypeList
}) => {
  const { t } = useTranslation();

  const handleFilterByType = useCallback(
    (e: DropdownBaseChangeEvent) => {
      isFunction(onFilterByType) && onFilterByType(e.target.value);
    },
    [onFilterByType]
  );

  const changeTypeListDynamic = useMemo(() => {
    const list = CHANGE_TYPE.filter(i =>
      includes(changeTypeList, textToCamel(i)?.toLowerCase())
    );
    return ['All', ...list];
  }, [changeTypeList]);

  return (
    <div className="d-flex align-items-center mr-24">
      <strong className="fs-14 color-grey mr-4">{t('txt_change_type')}:</strong>
      <DropdownList
        name="approval-type"
        value={pendingApprovalType}
        textField="description"
        onChange={handleFilterByType}
        variant="no-border"
      >
        {changeTypeListDynamic.map((item, idx) => (
          <DropdownList.Item key={idx} label={item} value={item} />
        ))}
      </DropdownList>
    </div>
  );
};

export default PendingApprovalFilter;
