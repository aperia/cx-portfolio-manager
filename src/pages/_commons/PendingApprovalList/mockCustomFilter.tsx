import React from 'react';

jest.mock('pages/_commons/PendingApprovalList/CustomFilter', () => {
  return {
    __esModule: true,
    default: ({ pendingApprovalType, onFilterByType }: any) => {
      return (
        <div>
          <div>{pendingApprovalType}</div>
          <button
            onClick={() => {
              onFilterByType('test type');
            }}
          >
            Change type
          </button>
        </div>
      );
    }
  };
});
