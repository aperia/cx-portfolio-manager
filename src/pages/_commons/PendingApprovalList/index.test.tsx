import { fireEvent, screen } from '@testing-library/dom';
import { RenderResult } from '@testing-library/react';
import { ChangeType } from 'app/constants/enums';
import { ChangeList, ChangeMapping } from 'app/fixtures/change-data';
import { mappingDataFromArray } from 'app/helpers';
import { renderWithMockStore, spyOnTranslationHoook } from 'app/utils';
import 'app/utils/_mockComponent/mockBreadCrumb';
import 'app/utils/_mockComponent/mockButtonGroupFilter';
import 'app/utils/_mockComponent/mockDofView';
import 'app/utils/_mockComponent/mockFailedApiReload';
import 'app/utils/_mockComponent/mockNoDataFound';
import 'app/utils/_mockComponent/mockPaging';
import 'app/utils/_mockComponent/mockSortOrder';
import 'pages/_commons/PendingApprovalList/mockCustomFilter';
import * as ChangeSelector from 'pages/_commons/redux/Change/select-hooks';
import React from 'react';
import { defaultApprovalQueuesDataFilter as defaultDataFilter } from '../redux/Change/reducers';
import PendingApprovalList from './';

jest.mock('react-router-dom', () => ({
  useHistory: () => ({
    push: jest.fn()
  })
}));

const mockUseSelectApprovalQueues = ({
  approvalQueues,
  total,
  changeTypeList
}: {
  approvalQueues: IChangeDetail[];
  total: number;
  changeTypeList?: string[];
}) => {
  jest.spyOn(ChangeSelector, 'useSelectApprovalQueues').mockReturnValue({
    approvalQueues,
    total,
    changeTypeList: changeTypeList || []
  });
};

const mockUseSelectApprovalQueuesStatus = ({ loading, error }: any) => {
  jest
    .spyOn(ChangeSelector, 'useSelectApprovalQueuesStatus')
    .mockReturnValue({ loading, error });
};

const mockUseSelectApprovalQueuesFilter = (dataFilter: any) => {
  jest
    .spyOn(ChangeSelector, 'useSelectApprovalQueuesFilter')
    .mockReturnValue(dataFilter);
};

const testId = 'list-test-id';

let mockPendingApprovalData: IChangeDetail[] = [];
mockPendingApprovalData = mappingDataFromArray(ChangeList, ChangeMapping)?.map(
  (p: IChangeDetail) => {
    if (p.changeType !== ChangeType.Pricing) {
      delete p.pricingStrategiesCreatedCount;
      delete p.pricingStrategiesUpdatedCount;
      delete p.pricingMethodsCreatedCount;
      delete p.pricingMethodsUpdatedCount;
      delete p.noOfDMMTablesCreated;
      delete p.noOfDMMTablesUpdated;
    } else {
      delete p.cardholdersAccountsImpacted;
      delete p.portfoliosImpacted;
    }
    return p;
  }
);

export const ListMock: any = {
  changeId: '123',
  containerClassName: 'container',
  title: 'Pending approval title'
};

describe('PendingApprovalList', () => {
  let renderResult: RenderResult;
  let storeCofig: any = {};
  beforeEach(() => {
    storeCofig = {
      initialState: {
        change: {
          approvalQueues: {
            data: mockPendingApprovalData,
            loading: false,
            dataFilter: defaultDataFilter,
            error: false
          }
        } as any
      }
    };
    spyOnTranslationHoook();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render pending approval list', async () => {
    mockUseSelectApprovalQueues({
      approvalQueues: mockPendingApprovalData,
      total: 30
    });
    mockUseSelectApprovalQueuesFilter(defaultDataFilter);
    mockUseSelectApprovalQueuesStatus({
      loading: true,
      error: false
    });
    renderResult = await renderWithMockStore(
      <PendingApprovalList {...ListMock} />,
      storeCofig,
      {},
      testId
    );
    const wrapper = await renderResult.findByTestId(testId);
    expect(wrapper).toBeInTheDocument();
    expect(screen.queryByText('Mock BreadCrumb')).toBeInTheDocument();
    expect(screen.queryByText('Pending approval title')).toBeInTheDocument();
    expect(screen.getAllByText('change-card-view-header').length).toEqual(3);
  });

  it('should render pending approval list without header, filter & breadcrumb', async () => {
    mockUseSelectApprovalQueues({
      approvalQueues: mockPendingApprovalData,
      total: 30
    });
    mockUseSelectApprovalQueuesStatus({
      loading: false,
      error: false
    });
    mockUseSelectApprovalQueuesFilter(defaultDataFilter);
    renderResult = await renderWithMockStore(
      <PendingApprovalList
        {...ListMock}
        hideBreadCrumb
        hideFilter
        hideHeader
        maxDataNumber={5}
      />,
      storeCofig,
      {},
      testId
    );
    const wrapper = await renderResult.findByTestId(testId);
    expect(wrapper).toBeInTheDocument();
    expect(screen.queryByText('Mock BreadCrumb')).toBeNull();
    expect(screen.queryByText('Pending approval title')).toBeNull();
    expect(screen.getAllByText('change-card-view-header').length).toEqual(3);
  });

  it('should render no data found', async () => {
    mockUseSelectApprovalQueues({
      approvalQueues: [],
      total: 0
    });
    mockUseSelectApprovalQueuesStatus({
      loading: false,
      error: false
    });
    mockUseSelectApprovalQueuesFilter(defaultDataFilter);
    renderResult = await renderWithMockStore(
      <PendingApprovalList {...ListMock} />,
      storeCofig,
      {},
      testId
    );
    const { getByText } = renderResult;
    expect(getByText('No data found')).toBeInTheDocument();
  });

  it('should render no data found with search', async () => {
    mockUseSelectApprovalQueues({
      approvalQueues: [],
      total: 0
    });
    mockUseSelectApprovalQueuesStatus({
      loading: false,
      error: false
    });
    mockUseSelectApprovalQueuesFilter({
      ...defaultDataFilter,
      searchValue: 'test value'
    });
    renderResult = await renderWithMockStore(
      <PendingApprovalList {...ListMock} />,
      storeCofig,
      {},
      testId
    );
    const { getByText } = renderResult;
    expect(getByText('No data found')).toBeInTheDocument();
  });

  it('should render data load unsuccessfully', async () => {
    mockUseSelectApprovalQueues({
      approvalQueues: [],
      total: 0
    });
    mockUseSelectApprovalQueuesStatus({
      loading: false,
      error: true
    });
    mockUseSelectApprovalQueuesFilter(defaultDataFilter);
    renderResult = await renderWithMockStore(
      <PendingApprovalList {...ListMock} />,
      storeCofig,
      {},
      testId
    );
    const { getByText } = renderResult;
    fireEvent.click(screen.getByText('Reload'));
    expect(getByText('Data load unsuccessful')).toBeInTheDocument();
  });

  it('change view mode on pending approval list', async () => {
    mockUseSelectApprovalQueues({
      approvalQueues: mockPendingApprovalData,
      total: 30
    });
    mockUseSelectApprovalQueuesStatus({
      loading: false,
      error: false
    });
    mockUseSelectApprovalQueuesFilter(defaultDataFilter);
    renderResult = await renderWithMockStore(
      <PendingApprovalList {...ListMock} />,
      storeCofig,
      {},
      testId
    );

    const wrapper = await renderResult.findByTestId(testId);

    fireEvent.click(wrapper.querySelector('.icon-view-grid')!);
    expect(wrapper.querySelector('.masonry-column')).toBeInTheDocument();
    fireEvent.click(wrapper.querySelector('.icon-view-list')!);
    expect(wrapper.querySelector('.masonry-column')).toBeNull();
  });

  it('filter on pending approval list', async () => {
    mockUseSelectApprovalQueues({
      approvalQueues: mockPendingApprovalData,
      total: 30
    });
    mockUseSelectApprovalQueuesStatus({
      loading: false,
      error: false
    });
    mockUseSelectApprovalQueuesFilter({
      ...defaultDataFilter,
      changeType: 'Pricing'
    });
    renderResult = await renderWithMockStore(
      <PendingApprovalList {...ListMock} />,
      storeCofig,
      {},
      testId
    );

    expect(screen.getByText('Pricing')).toBeInTheDocument();

    const wrapper = await renderResult.findByTestId(testId);

    fireEvent.click(wrapper.querySelector('.icon.icon-search')!);
    fireEvent.click(screen.getByText('Sort By'));
    fireEvent.click(screen.getByText('Order By'));
    fireEvent.click(screen.getByText('Clear And Reset'));
    fireEvent.click(screen.getByText('Page 2'));
    fireEvent.click(screen.getByText('Page Size 25'));
    fireEvent.click(screen.getByText('Change type'));

    fireEvent.click(
      screen.getByText(
        'Click on approval-queue-card-view-header__pendingApprovalCardView__' +
          mockPendingApprovalData[0].changeId
      )
    );
    expect(screen.getByTestId(testId)!).toBeInTheDocument();
    expect(screen.queryByText('Mock BreadCrumb')).toBeInTheDocument();
    expect(screen.queryByText('Pending approval title')).toBeInTheDocument();
    expect(screen.getAllByText('change-card-view-header').length).toEqual(3);
  });
});
