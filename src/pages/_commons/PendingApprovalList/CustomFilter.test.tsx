import { changeDlsDropdownMock, renderComponent } from 'app/utils';
import React from 'react';
import PendingApprovalFilter, { IApprovalFilter } from './CustomFilter';

describe('Custom Filter', () => {
  const testId = 'custom-filter';
  const onFilterByTypeMock = jest.fn();
  const defaultProps: IApprovalFilter = {
    pendingApprovalType: 'type1',
    changeTypeList: ['type1', 'type2'],
    onFilterByType: onFilterByTypeMock
  };
  const renderWraper = async (props?: IApprovalFilter) => {
    const result = await renderComponent(
      testId,
      <PendingApprovalFilter {...defaultProps} {...props} />
    );
    return result;
  };
  it('Render to UI', async () => {
    const { getByText, getByTestId } = await renderWraper();
    expect(getByText(/type1/)).toBeInTheDocument();
    changeDlsDropdownMock(0, 0, getByTestId(testId));
    expect(onFilterByTypeMock).toBeCalled();
  });
});
