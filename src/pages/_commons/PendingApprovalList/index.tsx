import ApprovalQueueCardView from 'app/components/ApprovalQueueCardView';
import FailedApiReload from 'app/components/FailedApiReload';
import NoDataFound from 'app/components/NoDataFound';
import { PENDING_APPROVAL_QUEUES_SORT_BY_LIST } from 'app/constants/constants';
import { changeDetailLink } from 'app/constants/links';
import { classnames } from 'app/helpers';
import { SimpleBar, useTranslation } from 'app/_libraries/_dls';
import isEmpty from 'lodash.isempty';
import {
  actionsChange,
  useSelectApprovalQueues,
  useSelectApprovalQueuesFilter,
  useSelectApprovalQueuesStatus,
  useSelectExpandingApproval
} from 'pages/_commons/redux/Change';
import { defaultApprovalQueuesDataFilter } from 'pages/_commons/redux/Change/reducers';
import Paging from 'pages/_commons/Utils/Paging';
import SortOrder from 'pages/_commons/Utils/SortOrder';
import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import PendingApprovalFilter from './CustomFilter';
import PendingApprovalHeader from './Header';

interface IProps {
  containerClassName?: string;
  hideBreadCrumb?: boolean;
  hideHeader?: boolean;
  hideViewMode?: boolean;
  hideFilter?: boolean;
  title?: string;
  maxDataNumber?: number;
  isFullPage?: boolean;
}
const PendingApprovalList: React.FC<IProps> = ({
  containerClassName,
  hideBreadCrumb,
  hideHeader,
  hideViewMode,
  hideFilter,
  title,
  maxDataNumber,
  isFullPage
}) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [isListView, setIsListView] = useState<boolean>(true);

  const { t } = useTranslation();
  const {
    approvalQueues: viewItems,
    total,
    changeTypeList
  } = useSelectApprovalQueues();
  const { loading, error } = useSelectApprovalQueuesStatus();

  const dataFilter = useSelectApprovalQueuesFilter();
  const expandingApproval = useSelectExpandingApproval();

  const {
    page,
    pageSize,
    searchValue,
    changeType: changeTypeFilter
  } = dataFilter;

  const hasDataFilter = changeTypeFilter?.toLowerCase() !== 'all';
  const hasSearchOrFilter = !!searchValue || hasDataFilter;

  const handleChangePage = useCallback(
    (pageNumber: number) => {
      dispatch(actionsChange.updateApprovalQueuesFilter({ page: pageNumber }));
    },
    [dispatch]
  );

  const handleChangePageSize = useCallback(
    (pageSizeNumber: number) => {
      dispatch(
        actionsChange.updateApprovalQueuesFilter({
          pageSize: pageSizeNumber
        })
      );
    },
    [dispatch]
  );

  const handleChangeSortBy = useCallback(
    (sortByValue: string) => {
      dispatch(
        actionsChange.updateApprovalQueuesFilter({
          sortBy: [sortByValue]
        })
      );
    },
    [dispatch]
  );

  const handleChangeOrderBy = useCallback(
    (orderByValue: string) => {
      dispatch(
        actionsChange.updateApprovalQueuesFilter({
          orderBy: [orderByValue as OrderByType]
        })
      );
    },
    [dispatch]
  );

  const handleSearch = useCallback(
    (val: string) => {
      dispatch(actionsChange.updateApprovalQueuesFilter({ searchValue: val }));
    },
    [dispatch]
  );

  const handleFilterByChangeType = useCallback(
    (typeValue: string) => {
      dispatch(
        actionsChange.updateApprovalQueuesFilter({ changeType: typeValue })
      );
    },
    [dispatch]
  );

  const handleClearAndReset = useCallback(() => {
    dispatch(actionsChange.clearAndResetApprovalQueuesFilter());
  }, [dispatch]);

  const handleApiReload = () => {
    dispatch(actionsChange.getApprovalQueueList());
  };

  const navigateToChangeDetail = useCallback(
    (changeId: string) => {
      history.push(changeDetailLink(changeId));
    },
    [history]
  );

  const handleToggle = useCallback(
    (changeId: string) => {
      dispatch(actionsChange.updateExpandingApproval(changeId));
    },
    [dispatch]
  );

  useEffect(() => {
    return () => {
      handleToggle('');
    };
  }, [handleToggle]);

  useEffect(() => {
    dispatch(actionsChange.getApprovalQueueList());
    if (maxDataNumber) {
      dispatch(
        actionsChange.updateApprovalQueuesFilter({
          pageSize: maxDataNumber
        })
      );
    }
    return () => {
      dispatch(actionsChange.setToDefaultApprovalQueues());
    };
  }, [dispatch, maxDataNumber]);

  return (
    <SimpleBar>
      <div
        className={classnames(
          {
            'loading h-100-main': loading && !hideBreadCrumb,
            'loading mh-320': loading && hideBreadCrumb
          },
          containerClassName
        )}
      >
        {!hideHeader && (
          <PendingApprovalHeader
            isListView={isListView}
            hideBreadCrumb={hideBreadCrumb}
            hideViewMode={hideViewMode}
            hideFilter={isEmpty(viewItems) && !hasSearchOrFilter}
            title={title}
            searchValue={searchValue}
            onChangeListView={setIsListView}
            onSearch={handleSearch}
          />
        )}
        {!hideFilter && (!isEmpty(viewItems) || hasSearchOrFilter) && (
          <div className="d-flex align-items-center mt-16 mb-4">
            <PendingApprovalFilter
              changeTypeList={changeTypeList}
              pendingApprovalType={changeTypeFilter}
              onFilterByType={handleFilterByChangeType}
            />
            {!isEmpty(viewItems) && (
              <SortOrder
                hasFilter={hasSearchOrFilter}
                dataFilter={dataFilter}
                defaultDataFilter={defaultApprovalQueuesDataFilter}
                sortByFields={PENDING_APPROVAL_QUEUES_SORT_BY_LIST}
                onChangeSortBy={handleChangeSortBy}
                onChangeOrderBy={handleChangeOrderBy}
                onClearSearch={handleClearAndReset}
              />
            )}
          </div>
        )}
        {!isEmpty(viewItems) ? (
          <>
            <div
              className={classnames('row', { 'masonry-column': !isListView })}
            >
              {viewItems.map(item => (
                <ApprovalQueueCardView
                  key={item.changeId}
                  id={`pendingApprovalCardView__${item.changeId}`}
                  value={item}
                  onClick={() => navigateToChangeDetail(item.changeId)}
                  isListViewMode={isListView}
                  isExpand={expandingApproval === item.changeId}
                  onToggle={handleToggle}
                />
              ))}
            </div>
            {!hideFilter && (
              <Paging
                page={page}
                pageSize={pageSize}
                totalItem={total}
                onChangePage={handleChangePage}
                onChangePageSize={handleChangePageSize}
              />
            )}
          </>
        ) : (
          !loading &&
          !error && (
            <>
              <div className="d-flex flex-column justify-content-center mt-80 mb-24">
                <NoDataFound
                  id="pendingApprovals-NoDataFound"
                  title={t('txt_no_pending_approvals_display')}
                  hasSearch={!!searchValue}
                  hasFilter={hasDataFilter}
                  linkTitle={hasSearchOrFilter ? t('txt_clear_and_reset') : ''}
                  onLinkClicked={handleClearAndReset}
                  isFullPage={isFullPage}
                />
              </div>
            </>
          )
        )}
        {error && (
          <FailedApiReload
            id="pendingApprovals-error"
            onReload={handleApiReload}
            className="mt-40 pt-40 d-flex flex-column justify-content-center align-items-center"
          />
        )}
      </div>
    </SimpleBar>
  );
};

export default PendingApprovalList;
