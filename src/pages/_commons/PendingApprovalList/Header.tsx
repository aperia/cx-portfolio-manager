import BreadCrumb from 'app/components/BreadCrumb';
import SimpleSearch from 'app/components/SimpleSearch';
import { BASE_URL } from 'app/constants/links';
import { classnames } from 'app/helpers';
import { Button, Icon, Tooltip, useTranslation } from 'app/_libraries/_dls';
import isFunction from 'lodash.isfunction';
import React, { Fragment, useCallback, useEffect, useRef } from 'react';
import { Link } from 'react-router-dom';

interface IPendingApprovalHeader {
  isListView?: boolean;
  hideBreadCrumb?: boolean;
  title?: string;
  hideViewMode?: boolean;
  hideFilter?: boolean;
  searchValue?: string;
  onChangeListView?: (isListView: boolean) => void;
  onSearch?: (val: string) => void;
}

const pendingApprovalSearchFields = ['Name', 'Owner', 'Status', 'Type'];

const PendingApprovalHeader: React.FC<IPendingApprovalHeader> = ({
  isListView,
  hideBreadCrumb,
  title,
  hideViewMode,
  hideFilter,
  searchValue,
  onChangeListView,
  onSearch
}) => {
  const { t } = useTranslation();

  const simpleSearchRef = useRef<any>(null);

  const handleSearch = useCallback(
    (val: string) => {
      isFunction(onSearch) && onSearch(val);
    },
    [onSearch]
  );

  const handleChangeListView = useCallback(
    (value: boolean) => {
      isFunction(onChangeListView) && onChangeListView(value);
    },
    [onChangeListView]
  );

  useEffect(() => {
    if (!searchValue && simpleSearchRef && simpleSearchRef.current) {
      simpleSearchRef.current.clear();
    }
  }, [searchValue]);

  return (
    <Fragment>
      {!hideBreadCrumb && (
        <div className="mb-16">
          <div className="section">
            <BreadCrumb
              id={'pendingApprovals__breadcrumb'}
              items={breadCrmbItems}
              width={992}
            />
          </div>
        </div>
      )}
      <div className="d-flex align-items-center">
        <h4>{title}</h4>
        {!hideFilter && (
          <div className="ml-auto d-flex ">
            <SimpleSearch
              ref={simpleSearchRef}
              placeholder={t('txt_type_to_search')}
              onSearch={handleSearch}
              popperElement={
                <>
                  <p className="color-grey">{t('txt_search_description')}</p>
                  <ul className="search-field-item list-unstyled">
                    {pendingApprovalSearchFields.map(i => (
                      <li key={i} className="mt-16">
                        {i}
                      </li>
                    ))}
                  </ul>
                </>
              }
            />

            {!hideViewMode && (
              <div className="d-flex mr-n4 only-pc">
                <div className="ml-24">
                  <Tooltip
                    element={t('txt_list_view')}
                    placement="top"
                    variant={'primary'}
                  >
                    <Button
                      id={'pendingApprovals-view-list-button'}
                      variant="icon-secondary"
                      className={classnames(isListView && 'active')}
                    >
                      <Icon
                        name="view-list"
                        color={'grey-l16' as any}
                        size="5x"
                        onClick={() => handleChangeListView(true)}
                      />
                    </Button>
                  </Tooltip>
                </div>
                <div className="ml-16">
                  <Tooltip
                    element={t('txt_grid_view')}
                    placement="top"
                    variant={'primary'}
                  >
                    <Button
                      id={'pendingApprovals-view-grid-button'}
                      variant="icon-secondary"
                      className={classnames(!isListView && 'active')}
                    >
                      <Icon
                        name="view-grid"
                        color={'grey-l16' as any}
                        size="5x"
                        onClick={() => handleChangeListView(false)}
                      />
                    </Button>
                  </Tooltip>
                </div>
              </div>
            )}
          </div>
        )}
      </div>
    </Fragment>
  );
};

export default PendingApprovalHeader;

const breadCrmbItems = [
  { id: 'home', title: <Link to={BASE_URL}>Home</Link> },
  { id: 'pendingApprovals', title: 'Pending Approval Queue' }
];
