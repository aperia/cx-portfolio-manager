// components
import { renderWithMockStore } from 'app/utils/renderWithMockStore';
import MappingProvider from 'pages/_commons/MappingProvider';
import React from 'react';

describe('Mapping Provider', () => {
  it('has mapping render component', async () => {
    const element = (
      <MappingProvider>
        <div>Test List Page</div>
      </MappingProvider>
    );
    const storeCofig = {
      initialState: {
        mapping: {
          data: {
            testList: {
              id: 'id',
              name: 'name'
            }
          }
        }
      }
    };
    const { getByText } = await renderWithMockStore(element, storeCofig);
    expect(getByText('Test List Page')).not.toBeEmptyDOMElement();
  });

  it('has NO mapping, render nothing', async () => {
    const element = (
      <MappingProvider>
        <div>Test List Page</div>
      </MappingProvider>
    );
    const storeCofig = {
      initialState: {}
    };
    const { queryByText } = await renderWithMockStore(element, storeCofig);
    expect(queryByText('Test List Page')).toBeNull();
  });
});
