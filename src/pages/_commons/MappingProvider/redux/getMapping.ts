import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { convertNameToCode } from 'app/helpers';
import { APIMapping } from 'app/services';
import { MappingState } from 'app/types';
import get from 'lodash.get';
import set from 'lodash.set';

/**
 * - getMapping thunk
 */
export const getMapping = createAsyncThunk<any, undefined, ThunkAPIConfig>(
  'mapping/getMapping',
  async (args: undefined, thunkAPI) => {
    const { systemService } = get(thunkAPI, 'extra') as APIMapping;
    const response = await Promise.all([
      systemService.getMapping(),
      systemService.getWorkflowMapping()
    ]);

    const appMapping = response[0]?.data;
    const _workflowMaping = response[1]?.data;

    const workflowMaping = Object.keys(_workflowMaping).reduce(
      (pre, curr) => ({
        ...pre,
        [convertNameToCode(curr)]: convertNameToCode(_workflowMaping[curr])
      }),
      {} as Record<string, string>
    );

    return { appMapping, workflowMaping };
  }
);

/**
 * - getMapping extra reducers builder
 */
export const getMappingBuilder = (
  builder: ActionReducerMapBuilder<MappingState>
) => {
  builder.addCase(getMapping.pending, (draftState, action) => {
    set(draftState, 'loading', true);
  });
  builder.addCase(getMapping.fulfilled, (draftState, action) => {
    draftState.data = action.payload.appMapping;
    draftState.workflowData = action.payload.workflowMaping;
    draftState.loading = false;
  });
  builder.addCase(getMapping.rejected, (draftState, action) => {
    set(draftState, 'error', true);
  });
};
