import { createSlice } from '@reduxjs/toolkit';
import { MappingState } from 'app/types';
import { getMapping, getMappingBuilder } from './getMapping';
import reducers from './reducers';

const { actions, reducer } = createSlice({
  name: 'mapping',
  initialState: {} as MappingState,
  reducers,
  extraReducers: builder => {
    getMappingBuilder(builder);
  }
});

const extraActions = {
  ...actions,
  getMapping
};

export { extraActions as actionsMapping, reducer };
