import { DataMaping } from 'app/fixtures/change-data';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import { actionsMapping, reducer } from 'pages/_commons/MappingProvider/redux';

const initReducer: any = {
  loading: false,
  error: false,
  data: []
};

describe('mapping > redux-store > getMapping', () => {
  it('pending', () => {
    const nextState = reducer(
      initReducer,
      actionsMapping.getMapping.pending('getMapping', undefined)
    );
    expect(nextState.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initReducer,
      actionsMapping.getMapping.fulfilled([], 'getMapping', undefined)
    );
    expect(nextState.loading).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initReducer,
      actionsMapping.getMapping.rejected(null, 'getMapping', undefined)
    ) as any;
    expect(nextState.loading).toEqual(false);
    expect(nextState.error).toEqual(true);
  });
  it('thunk action', async () => {
    const nextState = await actionsMapping.getMapping()(
      store.dispatch,
      store.getState(),
      {
        systemService: {
          getMapping: jest.fn().mockResolvedValue({
            data: DataMaping
          }),
          getWorkflowMapping: jest.fn().mockResolvedValue({
            data: { pricingMethods: '1123' }
          })
        }
      } as any
    );

    expect(Object.keys(nextState.payload).length).toEqual(2);
  });
});
