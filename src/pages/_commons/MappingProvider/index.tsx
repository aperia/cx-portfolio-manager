import { MappingStateData } from 'app/types';
import isEmpty from 'lodash.isempty';
import React, { ReactElement, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { actionsMapping } from './redux';

export interface MappingProviderProps {}

const MappingProvider: React.FC<MappingProviderProps> = ({ children }) => {
  const dispatch = useDispatch();
  const mapping = useSelector<RootState, MappingStateData>(
    state => state.mapping!.data
  );

  useEffect(() => {
    dispatch(actionsMapping.getMapping());
  }, [dispatch]);

  if (isEmpty(mapping)) return null;
  return children as ReactElement;
};

export default MappingProvider;
