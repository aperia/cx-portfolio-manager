import { createSelector } from '@reduxjs/toolkit';
import { useSelector } from 'react-redux';

const getCurrentUser = createSelector(
  (state: RootState) => state.auth?.currentUser,
  currentUser => {
    return currentUser;
  }
);

export const useSelectCurrentUser = () => {
  return useSelector<RootState, any>(getCurrentUser);
};

export const useSelectCurrentTimezone = () => {
  return useSelector<RootState, number>(
    state => state.auth?.currentUser.timeZone
  );
};
