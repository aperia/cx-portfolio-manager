import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AuthState } from 'app/types';
const initialState: AuthState = {
  currentUser: {}
};

const { actions, reducer } = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setCurrentUser: (draftState, action: PayloadAction<any>) => {
      draftState.currentUser = action.payload;
    }
  }
});

export { actions as authActions, reducer };
