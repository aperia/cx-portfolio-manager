import { authActions, reducer } from 'pages/_commons/Header/redux';

const initReducer = {
  currentUser: {}
};

describe('auth > reducers', () => {
  it('setCurrentUser', () => {
    const nextState = reducer(
      initReducer,
      authActions.setCurrentUser({
        essUserName: 'Test Name',
        userId: 'FDESMAST',
        userName: 'testname',
        groups: ['HLNSA'],
        timeZone: -5
      })
    );
    expect(nextState.currentUser.essUserName).toEqual('Test Name');
    expect(nextState.currentUser.timeZone).toEqual(-5);
  });
});
