import { AuthState } from 'app/types';
import {
  useSelectCurrentTimezone,
  useSelectCurrentUser
} from 'pages/_commons/Header/redux/selector';
import * as reactRedux from 'react-redux';

describe('select-hook > auth', () => {
  let selectorMock: jest.SpyInstance;
  const mockSelectorAuth = (authState: AuthState) => {
    selectorMock = jest
      .spyOn(reactRedux, 'useSelector')
      .mockImplementation(selector => selector({ auth: authState }));
  };

  afterEach(() => {
    selectorMock.mockClear();
  });

  it('useSelectCurrentUser', () => {
    mockSelectorAuth({
      currentUser: {
        essUserName: 'Test Name',
        userId: 'FDESMAST',
        userName: 'testname',
        groups: ['HLNSA'],
        timeZone: -5
      }
    });

    const result = useSelectCurrentUser();

    expect(result.essUserName).toEqual('Test Name');
    expect(result.timeZone).toEqual(-5);
  });

  it('useSelectCurrentTimezone', () => {
    mockSelectorAuth({
      currentUser: {
        timeZone: -5
      }
    });

    const result = useSelectCurrentTimezone();

    expect(result).toEqual(-5);
  });
});
