import { fireEvent } from '@testing-library/react';
import { BASE_URL } from 'app/constants/links';
import { renderWithMockStore } from 'app/utils';
import Header from 'pages/_commons/Header';
import { defaultDataFilter } from 'pages/_commons/redux/Notification/reducers';
import React from 'react';

jest.mock('react-router-dom', () => ({
  useHistory: () => ({
    push: jest.fn()
  })
}));
const mockDefault = {
  logo: 'images/remedy.svg',
  onClickLogo: jest.fn()
};

const mockHistoryPush = jest.fn();

const mockHistory = {
  push: mockHistoryPush
};
jest.mock('react-router-dom', () => ({
  useHistory: () => mockHistory,
  useLocation: () => ({}),
  useParams: () => ({})
}));

const testId = 'pages/_commons/Header';
describe('Test commons Header', () => {
  it('render to UI', async () => {
    const storeConfig = {
      initialState: {
        notification: {
          notificationList: {
            notifications: [
              {
                id: 'id',
                changeSetId: 'changeId',
                description: 'desc',
                date: '03/04/2020',
                isRead: false
              }
            ],
            loading: false,
            error: false,
            filter: defaultDataFilter
          },
          updateNotification: {
            loading: false,
            error: false,
            total: 0
          }
        }
      }
    };

    const renderResult = await renderWithMockStore(
      <Header {...mockDefault} />,
      storeConfig,
      {},
      testId
    );
    const wrapper = await renderResult.findByTestId(testId);

    const logoElement = wrapper.querySelector('.header-logo__link');

    expect(logoElement).toBeInTheDocument();

    fireEvent.click(logoElement!);

    expect(mockHistoryPush).toHaveBeenCalledWith(BASE_URL);
  });
});
