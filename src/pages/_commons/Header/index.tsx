import {
  default as HeaderRAC,
  HeaderProps as HeaderPropsRAC
} from 'app/components/Header';
import { appSetting } from 'app/constants/configurations';
import { BASE_URL } from 'app/constants/links';
import { jwtDecode } from 'app/helpers';
import { authActions } from 'pages/_commons/Header/redux';
import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';

export interface HeaderProps extends HeaderPropsRAC {}

const Header: React.FC<HeaderProps> = props => {
  const history = useHistory();

  const dispatch = useDispatch();

  const toHomepage = () => {
    history.push(BASE_URL);
  };

  useEffect(() => {
    const getLocalOffset = () => {
      return -new Date().getTimezoneOffset() / 60;
    };

    const userPayload = jwtDecode(appSetting.apiToken);

    dispatch(
      authActions.setCurrentUser({
        ...userPayload,
        timeZone: getLocalOffset()
      })
    );
  }, [dispatch]);

  return <HeaderRAC {...props} onClickLogo={toHomepage} />;
};

export default Header;
