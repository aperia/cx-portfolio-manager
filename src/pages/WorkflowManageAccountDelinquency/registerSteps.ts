import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { stepRegistry } from '../_commons/WorkflowSetup/registries';
import ConfigureParametersStep from './ConfigureParametersStep';
import GettingStartStep from './GettingStartStep';

stepRegistry.registerStep(
  'GetStartedManageAccountDelinquency',
  GettingStartStep
);
stepRegistry.registerStep(
  'ConfigureParametersManageAccountDelinquency',
  ConfigureParametersStep,
  [
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP_ACCOUNT_DELINQUENCY_CONFIGURE_PARAMETERS,
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP_ACCOUNT_DELINQUENCY_CONFIGURE_PARAMETERS_CONFIRM_EDIT
  ]
);
