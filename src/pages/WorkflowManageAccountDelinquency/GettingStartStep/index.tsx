import { WORKFLOW_SETUP } from 'app/constants/local-storage';
import {
  Button,
  Icon,
  InlineMessage,
  SimpleBar,
  TransDLS,
  useTranslation
} from 'app/_libraries/_dls';
import OverviewModal from 'pages/_commons/OverviewModal';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, { useRef, useState } from 'react';
import GettingStartSummary from './GettingStartSummary';

export interface GettingStartStepProps {
  isValid?: boolean;
  title?: any;

  alreadyShown?: boolean;
}

const GettingStartStep: React.FC<WorkflowSetupProps<GettingStartStepProps>> = ({
  title,
  formValues,
  setFormValues
}) => {
  const { t } = useTranslation();

  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;

  const [showOverview, setShowOverview] = useState(
    formValues.alreadyShown
      ? false
      : localStorage.getItem(WORKFLOW_SETUP.SHOW_OVERVIEW_AGAIN) !== 'false'
  );

  return (
    <React.Fragment>
      <SimpleBar>
        <div className="p-24">
          <InlineMessage variant="warning" withIcon className="mb-24">
            {t('txt_manage_account_delinquency_get_started_announcement')}
          </InlineMessage>
          <div className="d-flex align-items-center justify-content-between">
            <h4>{title}</h4>
            <Button
              className="mr-n8"
              variant="outline-primary"
              size="sm"
              onClick={() => setShowOverview(true)}
            >
              {t('txt_view_overview')}
            </Button>
          </div>
          <div className="row">
            <div className="col-12 col-md-6 mt-24">
              <div className="bg-light-l20 rounded-lg p-16">
                <div className="text-center">
                  <Icon name="request" size="12x" className="color-grey-l16" />
                </div>
                <p className="mt-8 color-grey">
                  {t('txt_manage_account_delinquency_get_started_desc')}
                </p>
                <p className="mt-16 fw-600">
                  {t(
                    'txt_manage_account_delinquency_get_started_desc_highlight_1'
                  )}
                </p>
                <p className="mt-8 color-grey">
                  {t('txt_manage_account_delinquency_get_started_desc_1_1')}
                </p>
                <ul className="list-dot color-grey">
                  <li>
                    {t(
                      'txt_manage_account_delinquency_get_started_desc_1_cd_051'
                    )}
                  </li>
                  <li>
                    {t(
                      'txt_manage_account_delinquency_get_started_desc_1_cd_052'
                    )}
                  </li>
                  <li>
                    {t(
                      'txt_manage_account_delinquency_get_started_desc_1_cd_011'
                    )}
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-12 col-md-6 mt-24 color-grey">
              <p>
                {t(
                  'txt_manage_account_delinquency_get_started_steps_guides_title'
                )}
              </p>
              <p className="mt-8">
                <TransDLS keyTranslation="txt_manage_account_delinquency_get_started_steps_guides_1">
                  <strong className="color-grey-d20" />
                </TransDLS>
              </p>
              <p className="mt-8">
                <TransDLS keyTranslation="txt_manage_account_delinquency_get_started_steps_guides_2">
                  <strong className="color-grey-d20" />
                </TransDLS>
              </p>
              <p className="mt-8">
                <TransDLS keyTranslation="txt_manage_account_delinquency_get_started_steps_guides_3">
                  <strong className="color-grey-d20" />
                </TransDLS>
              </p>
              <p className="mt-8">
                <TransDLS keyTranslation="txt_manage_account_delinquency_get_started_steps_guides_4">
                  <strong className="color-grey-d20" />
                </TransDLS>
              </p>
            </div>
          </div>
        </div>
      </SimpleBar>
      {showOverview && (
        <OverviewModal
          id="overviewWorkflowSetup"
          show
          onClose={() => {
            setShowOverview(false);
            keepRef.current.setFormValues({ alreadyShown: true });
          }}
        />
      )}
    </React.Fragment>
  );
};

const ExtraStaticGettingStartStep =
  GettingStartStep as WorkflowSetupStaticProp<GettingStartStepProps>;

ExtraStaticGettingStartStep.summaryComponent = GettingStartSummary;
ExtraStaticGettingStartStep.defaultValues = { isValid: true };

export default ExtraStaticGettingStartStep;
