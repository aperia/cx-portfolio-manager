import { ActionsValues, ConfigureParametersStepProps } from '.';

const stepValueFunc: WorkflowSetupStepValueFunc<ConfigureParametersStepProps> =
  ({ stepsForm: formValues, t }) => {
    const stepValues = {
      [ActionsValues.NM180Manual]: t(
        'txt_manage_account_delinquency_configure_parameters_step_values_NM180Manual'
      ),
      [ActionsValues.NM180Workout]: t(
        'txt_manage_account_delinquency_configure_parameters_step_values_NM180Workout'
      ),
      [ActionsValues.NM182]: t(
        'txt_manage_account_delinquency_configure_parameters_step_values_NM182'
      ),
      [ActionsValues.NM183]: t(
        'txt_manage_account_delinquency_configure_parameters_step_values_NM183'
      ),
      [ActionsValues.NM181]: t(
        'txt_manage_account_delinquency_configure_parameters_step_values_NM181'
      )
    };
    const { action } = formValues || {};
    return action ? stepValues[action as keyof typeof stepValues] : '';
  };

export default stepValueFunc;
