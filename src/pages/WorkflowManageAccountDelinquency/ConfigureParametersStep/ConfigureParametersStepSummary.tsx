import GroupTextControl from 'app/components/DofControl/GroupTextControl';
import { Button, useTranslation } from 'app/_libraries/_dls';
import { isFunction } from 'app/_libraries/_dls/lodash';
import { useSelectElementMetadataForManageAccountDelinquency } from 'pages/_commons/redux/WorkflowSetup';
import React, { useMemo } from 'react';
import {
  ActionsValues,
  ConfigureParametersStepProps,
  SubTransactionCode
} from '.';

const ConfigureParametersStepSummary: React.FC<
  WorkflowSetupSummaryProps<ConfigureParametersStepProps>
> = ({ formValues = {}, onEditStep, stepId, selectedStep }) => {
  const { t } = useTranslation();
  const {
    action: actions,
    nm180codes,
    nm180Mcsr,
    nm183codes,
    nm181csa
  } = useSelectElementMetadataForManageAccountDelinquency();
  const isShowText = stepId === selectedStep;
  const action = actions.find(a => a.code === formValues?.action)?.value;
  const actionText = actions.find(
    a => a.code === formValues?.action
  )?.description;
  const { parameters = [] } = formValues;
  const { transaction, subTransactionCode, code, customerServiceReage } =
    useMemo(() => {
      let transaction = '';
      let subTransactionCode: string | undefined = '';
      let code: string | undefined = '';
      let customerServiceReage: string | undefined = '';
      switch (action) {
        case ActionsValues.NM180Manual:
          transaction = t(
            'txt_manage_account_delinquency_configure_parameters_step_NM180M_title'
          );
          const parameter180M = parameters.find(
            p => p.name === SubTransactionCode.NM180Code
          );
          const subTransactionM = nm180codes.find(
            code => code.code === parameter180M?.value
          );
          subTransactionCode = subTransactionM?.text;
          const subTransactionValue = subTransactionM?.value;
          const parameter180Mcsr = parameters.find(
            p => p.name === SubTransactionCode.NM180Csr
          );
          customerServiceReage =
            subTransactionValue === '00'
              ? nm180Mcsr.find(code => code.code === parameter180Mcsr?.value)
                  ?.description
              : '';
          break;
        case ActionsValues.NM180Workout:
          transaction = t(
            'txt_manage_account_delinquency_configure_parameters_step_NM180K_title'
          );
          const parameter180W = parameters.find(
            p => p.name === SubTransactionCode.NM180Code
          );
          const subTransactionW = nm180codes.find(
            code => code.code === parameter180W?.value
          );
          subTransactionCode = subTransactionW?.text;
          const subTransactionWValue = subTransactionW?.value;
          const parameter180Wcsr = parameters.find(
            p => p.name === SubTransactionCode.NM180Csr
          );
          customerServiceReage =
            subTransactionWValue === '00'
              ? nm180Mcsr.find(code => code.code === parameter180Wcsr?.value)
                  ?.description
              : '';
          break;
        case ActionsValues.NM182:
          transaction = t(
            'txt_manage_account_delinquency_configure_parameters_step_NM182_desc_2'
          );
          break;
        case ActionsValues.NM183:
          transaction = t(
            'txt_manage_account_delinquency_configure_parameters_step_NM183_title'
          );
          const parameterNM183 = parameters.find(
            p => p.name === SubTransactionCode.NM183
          );
          code = nm183codes.find(
            code => code.code === parameterNM183?.value
          )?.text;
          break;
        case ActionsValues.NM181:
          transaction = t(
            'txt_manage_account_delinquency_configure_parameters_step_NM181_title'
          );
          const parameterNM181 = parameters.find(
            p => p.name === SubTransactionCode.NM181
          );
          customerServiceReage = nm181csa.find(
            code => code.code === parameterNM181?.value
          )?.description;
          break;
      }
      return { transaction, subTransactionCode, code, customerServiceReage };
    }, [action, nm180Mcsr, parameters, nm180codes, nm181csa, nm183codes, t]);

  const handleEdit = () => {
    isFunction(onEditStep) && onEditStep();
  };

  return (
    <div className="position-relative">
      <div className="absolute-top-right mt-n26 mr-n8">
        <Button variant="outline-primary" size="sm" onClick={handleEdit}>
          {t('txt_edit')}
        </Button>
      </div>
      <div className="row">
        <div className="col-12 col-lg-3">
          <GroupTextControl
            id="summary_action"
            input={{ value: actionText } as any}
            meta={{} as any}
            label={t('txt_action')}
            options={{
              inline: false,
              lineTruncate: 2,
              ellipsisLessText: t('txt_less'),
              ellipsisMoreText: t('txt_more')
            }}
          />
        </div>
        <div className="col-12 col-lg-3">
          {isShowText && (
            <GroupTextControl
              id="summary_transaction"
              input={{ value: transaction } as any}
              meta={{} as any}
              label={t('txt_transaction')}
              options={{
                inline: false,
                lineTruncate: 2,
                ellipsisLessText: t('txt_less'),
                ellipsisMoreText: t('txt_more')
              }}
            />
          )}
        </div>
        {subTransactionCode && isShowText && (
          <div className="col-12 col-lg-3">
            <GroupTextControl
              id="summary_sub_transaction_code"
              input={{ value: subTransactionCode } as any}
              meta={{} as any}
              label={t('txt_subtransaction_code')}
              options={{
                inline: false,
                lineTruncate: 2,
                ellipsisLessText: t('txt_less'),
                ellipsisMoreText: t('txt_more')
              }}
            />
          </div>
        )}
        {code && isShowText && (
          <div className="col-12 col-lg-3">
            <GroupTextControl
              id="summary_code"
              input={{ value: code } as any}
              meta={{} as any}
              label={t('txt_code')}
              options={{
                inline: false,
                lineTruncate: 2,
                ellipsisLessText: t('txt_less'),
                ellipsisMoreText: t('txt_more')
              }}
            />
          </div>
        )}
        {customerServiceReage && isShowText && (
          <div className="col-12 col-lg-3">
            <GroupTextControl
              id="summary_customer_service_reage"
              input={{ value: customerServiceReage } as any}
              meta={{} as any}
              label={t('txt_customer_service_reage')}
              options={{
                inline: false,
                lineTruncate: 2,
                ellipsisLessText: t('txt_less'),
                ellipsisMoreText: t('txt_more')
              }}
            />
          </div>
        )}
      </div>
    </div>
  );
};

export default ConfigureParametersStepSummary;
