import { fireEvent, render } from '@testing-library/react';
import React from 'react';
import { ActionsValues, SubTransactionCode } from '.';
import NM183Bottom, { IProps } from './NM183Bottom';

jest.mock('./ParameterList', () => {
  return {
    __esModule: true,
    default: () => <div>ParameterList</div>
  };
});
const setFormValuesMock = jest.fn();
const renderComponent = (props?: IProps) => {
  const defaultProps: IProps = {
    formValues: undefined,
    codes: undefined,
    setFormValues: setFormValuesMock
  };
  const wrapper = render(<NM183Bottom {...defaultProps} {...props} />);
  return wrapper;
};

describe('NM183Bottom', () => {
  it('Render without values', () => {
    const wrapper = renderComponent();
    expect(
      wrapper.getByText(
        'txt_manage_account_delinquency_configure_parameters_step_NM183_title'
      )
    ).toBeInTheDocument();
    expect(
      wrapper.queryByText(
        'txt_manage_account_delinquency_configure_parameters_step_parameter_list_description'
      )
    ).not.toBeInTheDocument();
  });

  it('Render with full values', () => {
    const wrapper = renderComponent({
      formValues: {
        action: ActionsValues.NM183,
        parameters: [{ name: SubTransactionCode.NM183, value: '01' }]
      },
      setFormValues: setFormValuesMock,
      codes: [
        { code: '01', text: '01' },
        { code: '02', text: '02' }
      ]
    });
    expect(
      wrapper.getByText(
        'txt_manage_account_delinquency_configure_parameters_step_NM183_title'
      )
    ).toBeInTheDocument();
    expect(wrapper.getByText('ParameterList')).toBeInTheDocument();
    const element = wrapper.baseElement
      .querySelectorAll('.dls-dropdown-list .text-field-container')
      .item(0);
    fireEvent.focus(element);
    const pageElement = document.querySelectorAll('.dls-popup .item').item(1);
    fireEvent.click(pageElement);
    expect(setFormValuesMock).toBeCalledWith({
      action: ActionsValues.NM183,
      isValid: true,
      parameters: [{ name: SubTransactionCode.NM183, value: '02' }]
    });
  });
});
