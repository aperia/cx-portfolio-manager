import { InlineMessage, useTranslation } from 'app/_libraries/_dls';
import React from 'react';

const NM180WorkoutRight = () => {
  const { t } = useTranslation();
  return (
    <div className="bg-light-l20 rounded-lg p-16 h-100 ">
      <InlineMessage variant="warning" withIcon className="mb-16">
        <p className="color-orange-d16">
          {t(
            'txt_manage_account_delinquency_configure_parameters_step_NM180K_warning'
          )}
        </p>
      </InlineMessage>
      <p className="fw-600 mb-8">
        {t(
          'txt_manage_account_delinquency_configure_parameters_step_NM180K_title'
        )}
      </p>
      <p className="color-grey mb-8">
        {t(
          'txt_manage_account_delinquency_configure_parameters_step_NM180K_desc'
        )}
      </p>
    </div>
  );
};
export default NM180WorkoutRight;
