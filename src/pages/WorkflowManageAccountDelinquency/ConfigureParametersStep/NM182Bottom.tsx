import React from 'react';
import { ConfigureParametersStepProps } from '.';
import ParameterList from './ParameterList';
export interface IProps {
  formValues: ConfigureParametersStepProps;
}
const NM182Bottom: React.FC<IProps> = ({ formValues }) => {
  return <ParameterList action={formValues.action!} />;
};
export default NM182Bottom;
