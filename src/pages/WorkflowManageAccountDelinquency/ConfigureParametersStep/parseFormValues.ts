import { getWorkflowSetupStepStatus } from 'app/helpers';
import {
  ActionsValues,
  ConfigureParametersStepProps,
  SubTransactionCode
} from '.';

export const isValid = (formValues: ConfigureParametersStepProps) => {
  const { action, parameters = [] } = formValues;

  switch (action) {
    case ActionsValues.NM180Manual:
      const nm180ManualCode = parameters.find(
        p => p.name === SubTransactionCode.NM180ManualCode
      )?.value;
      return !!nm180ManualCode;
    case ActionsValues.NM180Workout:
      const nm180WorkoutCode = parameters.find(
        p => p.name === SubTransactionCode.NM180WorkoutCode
      )?.value;
      return !!nm180WorkoutCode;
    case ActionsValues.NM181:
    case ActionsValues.NM182:
      return true;
    case ActionsValues.NM183:
      const nm183code = parameters.find(
        p => p.name === SubTransactionCode.NM183
      )?.value;
      return !!nm183code;

    default:
      return false;
  }
};

export const parseResponseToParameters = (
  formValues: ConfigureParametersStepProps
) => {
  const { action, parameters = [] } = formValues;

  switch (action) {
    case ActionsValues.NM180Manual:
      const nm180ManualCode = parameters.find(
        p => p.name === SubTransactionCode.NM180Code
      );
      const nm180ManualCsr = parameters.find(
        p => p.name === SubTransactionCode.NM180Csr
      );
      return [
        ...parameters,
        {
          name: SubTransactionCode.NM180ManualCode,
          value: nm180ManualCode?.value
        },
        {
          name: SubTransactionCode.NM180ManualCsr,
          value: nm180ManualCsr?.value
        }
      ];
    case ActionsValues.NM180Workout:
      const nm180WorkoutCode = parameters.find(
        p => p.name === SubTransactionCode.NM180Code
      );
      const nm180WorkoutCsr = parameters.find(
        p => p.name === SubTransactionCode.NM180Csr
      );
      return [
        ...parameters,
        {
          name: SubTransactionCode.NM180WorkoutCode,
          value: nm180WorkoutCode?.value
        },
        {
          name: SubTransactionCode.NM180WorkoutCsr,
          value: nm180WorkoutCsr?.value
        }
      ];

    default:
      return parameters;
  }
};

const parseFormValues: WorkflowSetupStepFormDataFunc<any> = (data, id) => {
  const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);
  if (!stepInfo) return;
  const configurations = data?.data?.configurations;
  const transactionId = configurations?.transactionId;
  const parameters = parseResponseToParameters({
    action: transactionId,
    parameters: configurations?.parameters
  });
  const valid = isValid({ action: transactionId, parameters: parameters });
  return {
    ...getWorkflowSetupStepStatus(stepInfo),
    action: transactionId,
    parameters: parameters,
    isValid: valid
  };
};

export default parseFormValues;
