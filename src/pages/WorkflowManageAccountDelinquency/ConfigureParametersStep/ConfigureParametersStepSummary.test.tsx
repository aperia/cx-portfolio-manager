import { fireEvent } from '@testing-library/react';
import { WorkflowMetadataList } from 'app/fixtures/workflow-metadata';
import { renderWithMockStore } from 'app/utils';
import 'app/utils/_mockComponent/mockUseTranslation';
import * as mockHook from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageAccountDelinquency';
import React from 'react';
import { ActionsValues, SubTransactionCode } from '.';
import ConfigureParametersStepSummary from './ConfigureParametersStepSummary';

const renderComponent = (props: any) => {
  return renderWithMockStore(<ConfigureParametersStepSummary {...props} />, {
    initialState: {
      workflowSetup: {
        elementMetadata: {
          elements: WorkflowMetadataList
        }
      }
    } as AppState
  });
};

const mockSelector = jest.spyOn(
  mockHook,
  'useSelectElementMetadataForManageAccountDelinquency'
);

describe('ManageAccountDelinquency > ConfigureParametersStepSummary', () => {
  beforeEach(() => {
    mockSelector.mockReturnValue({
      action: [
        {
          value: ActionsValues.NM180Manual,
          description: 'des',
          workFlow: 'wf',
          selected: true,
          code: ActionsValues.NM180Manual,
          text: '123'
        },
        {
          value: ActionsValues.NM180Workout,
          description: 'des',
          workFlow: 'wf',
          selected: true,
          code: ActionsValues.NM180Workout,
          text: '123'
        },
        {
          value: ActionsValues.NM182,
          description: 'des',
          workFlow: 'wf',
          selected: true,
          code: ActionsValues.NM182,
          text: '123'
        },
        {
          value: ActionsValues.NM183,
          description: 'des',
          workFlow: 'wf',
          selected: true,
          code: ActionsValues.NM183,
          text: '123'
        },
        {
          value: ActionsValues.NM181,
          description: 'des',
          workFlow: 'wf',
          selected: true,
          code: ActionsValues.NM181,
          text: '123'
        }
      ],
      nm180Mcsr: [
        {
          value: '123',
          description: 'des',
          workFlow: 'wf',
          selected: true,
          code: 'Y',
          text: '123'
        }
      ],
      nm180codes: [
        {
          value: '00',
          description: 'des',
          workFlow: 'wf',
          selected: true,
          code: '00',
          text: 'text'
        }
      ],
      nm181csa: [
        {
          value: '123',
          description: 'des',
          workFlow: 'wf',
          selected: true,
          code: '01',
          text: '123'
        }
      ],
      nm183codes: [
        {
          value: '123',
          description: 'des',
          workFlow: 'wf',
          selected: true,
          code: '01',
          text: '123'
        }
      ]
    });
  });

  it('should render NM180 Manual with subtransaction code is 00', async () => {
    const mockFunction = jest.fn();

    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        action: ActionsValues.NM180Manual,
        parameters: [
          { name: SubTransactionCode.NM180Code, value: '00' },
          { name: SubTransactionCode.NM180Csr, value: 'Y' },
          { name: SubTransactionCode.NM180ManualCode, value: 'Y' },
          { name: SubTransactionCode.NM180ManualCsr, value: '00' }
        ]
      },
      onEditStep: mockFunction
    };
    const wrapper = await renderComponent(props);

    const editButton = wrapper.getByText('txt_edit');
    expect(editButton).toBeInTheDocument;
    fireEvent.click(editButton);
    expect(mockFunction).toHaveBeenCalled();
  });

  it('should render NM180 Manual with Subtransaction code is NOT 00', async () => {
    const mockFunction = jest.fn();

    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        action: ActionsValues.NM180Manual,
        parameters: [
          { name: SubTransactionCode.NM180Code, value: '01' },
          { name: SubTransactionCode.NM180Csr, value: 'Y' }
        ]
      },
      onEditStep: mockFunction
    };
    const wrapper = await renderComponent(props);

    const editButton = wrapper.getByText('txt_edit');
    expect(editButton).toBeInTheDocument;
    fireEvent.click(editButton);
    expect(mockFunction).toHaveBeenCalled();
  });

  it('should render NM180 Workout with subtransaction code is 00', async () => {
    const mockFunction = jest.fn();

    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        action: ActionsValues.NM180Workout,
        parameters: [
          { name: SubTransactionCode.NM180Code, value: '00' },
          { name: SubTransactionCode.NM180Csr, value: 'Y' }
        ]
      },
      onEditStep: mockFunction
    };
    const wrapper = await renderComponent(props);

    const editButton = wrapper.getByText('txt_edit');
    expect(editButton).toBeInTheDocument;
    fireEvent.click(editButton);
    expect(mockFunction).toHaveBeenCalled();
  });

  it('should render NM180 Workout with subtransaction code is NOT 00', async () => {
    const mockFunction = jest.fn();

    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        action: ActionsValues.NM180Workout,
        parameters: [
          { name: SubTransactionCode.NM180Code, value: '01' },
          { name: SubTransactionCode.NM180Csr, value: 'Y' }
        ]
      },
      onEditStep: mockFunction
    };
    const wrapper = await renderComponent(props);

    const editButton = wrapper.getByText('txt_edit');
    expect(editButton).toBeInTheDocument;
    fireEvent.click(editButton);
    expect(mockFunction).toHaveBeenCalled();
  });

  it('should render NM181', async () => {
    const mockFunction = jest.fn();

    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        action: ActionsValues.NM181,
        parameters: [{ name: SubTransactionCode.NM181, value: 'Y' }]
      },
      onEditStep: mockFunction
    };
    const wrapper = await renderComponent(props);

    const editButton = wrapper.getByText('txt_edit');
    expect(editButton).toBeInTheDocument;
    fireEvent.click(editButton);
    expect(mockFunction).toHaveBeenCalled();
  });

  it('should render NM182', async () => {
    const mockFunction = jest.fn();

    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        action: ActionsValues.NM182
      },
      onEditStep: mockFunction
    };
    const wrapper = await renderComponent(props);

    const editButton = wrapper.getByText('txt_edit');
    expect(editButton).toBeInTheDocument;
    fireEvent.click(editButton);
    expect(mockFunction).toHaveBeenCalled();
  });

  it('should render NM183', async () => {
    const mockFunction = jest.fn();

    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        action: ActionsValues.NM183,
        parameters: [{ name: SubTransactionCode.NM183, value: '01' }]
      },
      onEditStep: mockFunction
    };
    const wrapper = await renderComponent(props);

    const editButton = wrapper.getByText('txt_edit');
    expect(editButton).toBeInTheDocument;
    fireEvent.click(editButton);
    expect(mockFunction).toHaveBeenCalled();
  });

  it('should render with formValues undefined', async () => {
    const mockFunction = jest.fn();

    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: undefined,
      onEditStep: mockFunction
    };
    const wrapper = await renderComponent(props);

    const editButton = wrapper.getByText('txt_edit');
    expect(editButton).toBeInTheDocument;
    fireEvent.click(editButton);
    expect(mockFunction).toHaveBeenCalled();
  });
});
