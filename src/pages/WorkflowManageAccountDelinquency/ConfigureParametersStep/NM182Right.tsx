import { InlineMessage, useTranslation } from 'app/_libraries/_dls';
import React from 'react';

const NM182Right = () => {
  const { t } = useTranslation();
  return (
    <div className="bg-light-l20 rounded-lg p-16 h-100 ">
      <InlineMessage variant="warning" withIcon className="mb-16">
        <p className="color-orange-d16">
          {t(
            'txt_manage_account_delinquency_configure_parameters_step_NM182_desc_1'
          )}
        </p>
      </InlineMessage>
      <p className="fw-600 mb-8">
        {t(
          'txt_manage_account_delinquency_configure_parameters_step_NM182_desc_2'
        )}
      </p>
      <p className="color-grey mb-8">
        {t(
          'txt_manage_account_delinquency_configure_parameters_step_NM182_desc_3'
        )}
      </p>
      <ul className="color-grey mb-12 pl-16">
        <li className="mb-8">
          {t(
            'txt_manage_account_delinquency_configure_parameters_step_NM182_rule_1'
          )}
        </li>
        <li className="mb-8">
          {' '}
          {t(
            'txt_manage_account_delinquency_configure_parameters_step_NM182_rule_2'
          )}
        </li>
        <li className="mb-8">
          {' '}
          {t(
            'txt_manage_account_delinquency_configure_parameters_step_NM182_rule_3'
          )}
        </li>
        <li className="mb-8">
          {' '}
          {t(
            'txt_manage_account_delinquency_configure_parameters_step_NM182_rule_4'
          )}
        </li>
        <li className="mb-8">
          {' '}
          {t(
            'txt_manage_account_delinquency_configure_parameters_step_NM182_rule_5'
          )}
        </li>
      </ul>
      <p className="color-grey">
        {t(
          'txt_manage_account_delinquency_configure_parameters_step_NM182_note'
        )}
      </p>
    </div>
  );
};
export default NM182Right;
