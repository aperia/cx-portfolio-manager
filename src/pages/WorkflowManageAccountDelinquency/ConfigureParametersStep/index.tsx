import {
  confirmEditWhenChangeEffectToUploadProps,
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { compare2Array } from 'app/helpers/array';
import { useUnsavedChangeRegistry } from 'app/hooks';
import { InlineMessage, Radio, useTranslation } from 'app/_libraries/_dls';
import { StateFile } from 'app/_libraries/_dls/components/Upload/File';
import { STATUS } from 'app/_libraries/_dls/components/Upload/helper';
import { isEmpty } from 'lodash';
import {
  actionsWorkflowSetup,
  useSelectElementMetadataForManageAccountDelinquency
} from 'pages/_commons/redux/WorkflowSetup';
import { parseParametersToRequest } from 'pages/_commons/redux/WorkflowSetup/async-thunk/ManageAccountDelinquencyWorkflow/helper';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, { useEffect, useMemo, useState } from 'react';
import { useDispatch } from 'react-redux';
import ConfigureParametersStepSummary from './ConfigureParametersStepSummary';
import NM180MBottom from './NM180Bottom';
import NM180ManualRight from './NM180ManualRight';
import NM180WorkoutRight from './NM180WorkoutRight';
import NM181Bottom from './NM181Bottom';
import NM181Right from './NM181Right';
import NM182Bottom from './NM182Bottom';
import NM182Right from './NM182Right';
import NM183Bottom from './NM183Bottom';
import NM183Right from './NM183Right';
import parseFormValues, { isValid } from './parseFormValues';
import stepValueFunc from './stepValueFunc';

export const ActionsValues = {
  NM180Manual: 'nm.180.manual',
  NM180Workout: 'nm.180.workout',
  NM182: 'nm.182',
  NM183: 'nm.183',
  NM181: 'nm.181'
};

export const SubTransactionCode = {
  NM180Code: 'nm.180.subtransaction.code',
  NM180ManualCode: 'nm.180.manual.subtransaction.code',
  NM180WorkoutCode: 'nm.180.workout.subtransaction.code',
  NM180Csr: 'nm.180.customer.service.reage',
  NM180ManualCsr: 'nm.180.manual.customer.service.reage',
  NM180WorkoutCsr: 'nm.180.workout.customer.service.reage',
  NM183: 'nm.183.code',
  NM181: 'nm.181.customer.service.adjustment',
  TranSactionID: 'transaction.id',
  NM182Transaction: 'nm.182.transaction',
  NM183Transaction: 'nm.183.transaction',
  NM181Transaction: 'nm.181.transaction',
  NM180_00Transaction: 'nm.180.00.transaction',
  NM180_03Transaction: 'nm.180.03.transaction',
  NM180_04Transaction: 'nm.180.04.transaction',
  NM180_05Transaction: 'nm.180.05.transaction',
  NM180_06Transaction: 'nm.180.06.transaction',
  NM180_07Transaction: 'nm.180.07.transaction',
  NM180_54Transaction: 'nm.180.54.transaction',
  NM180_50Transaction: 'nm.180.50.transaction',
  NM180_53Transaction: 'nm.180.53.transaction',
  NM180_60Transaction: 'nm.180.60.transaction',
  NM180_61Transaction: 'nm.180.61.transaction',
  NM180_62Transaction: 'nm.180.62.transaction',
  NM180_63Transaction: 'nm.180.63.transaction'
};

interface Parameter {
  name?: string;
  value?: string;
}

export interface ConfigureParametersStepProps {
  isValid?: boolean;
  action?: string;
  parameters?: Parameter[];
}
interface IDependencies {
  files?: StateFile[];
}
export interface ConfigureParametersProps
  extends WorkflowSetupProps<ConfigureParametersStepProps, IDependencies> {
  clearFormName: string;
}

const ConfigureParametersStep: React.FC<ConfigureParametersProps> = ({
  savedAt,
  isStuck,
  formValues,
  setFormValues,
  dependencies,
  selectedStep,
  stepId,
  clearFormValues,
  clearFormName
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const [initialValues, setInitialValues] = useState(formValues);
  useEffect(() => {
    setInitialValues(formValues);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [savedAt]);
  const formHasChanged = useMemo(() => {
    if (selectedStep !== stepId) return false;
    const isParametersChanged = !compare2Array(
      parseParametersToRequest(initialValues).map((p: any) => p.value),
      parseParametersToRequest(formValues).map((p: any) => p.value)
    );
    return initialValues.action !== formValues.action || isParametersChanged;
  }, [initialValues, formValues, selectedStep, stepId]);
  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP_ACCOUNT_DELINQUENCY_CONFIGURE_PARAMETERS,
      priority: 1
    },
    [formHasChanged]
  );

  const unsaveOptions = useMemo(
    () => ({
      ...confirmEditWhenChangeEffectToUploadProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__FORCE_EMBOSS_REPLACEMENT_CARDS_CONFIRM_EDIT,
      priority: 2
    }),
    []
  );
  const fileParametersChanged = useMemo(() => {
    if (
      selectedStep !== stepId ||
      (formValues.action !== ActionsValues.NM180Manual &&
        formValues.action !== ActionsValues.NM180Workout)
    )
      return false;
    const isParametersChanged = !compare2Array(
      (initialValues.parameters || [])
        .filter((p: any) =>
          initialValues?.action === ActionsValues.NM180Manual
            ? p.name === SubTransactionCode.NM180ManualCode
            : p.name === SubTransactionCode.NM180WorkoutCode
        )
        .map((p: any) => p.value),
      (formValues.parameters || [])
        .filter((p: any) =>
          formValues?.action === ActionsValues.NM180Manual
            ? p.name === SubTransactionCode.NM180ManualCode
            : p.name === SubTransactionCode.NM180WorkoutCode
        )
        .map((p: any) => p.value)
    );
    return isParametersChanged;
  }, [initialValues, formValues, selectedStep, stepId]);

  const handleResetUploadStep = () => {
    const file = dependencies?.files?.[0];
    const attachmentId = file?.idx;
    const isValid = file?.status === STATUS.valid;

    typeof clearFormValues === 'function' && clearFormValues(clearFormName);
    isValid &&
      dispatch(actionsWorkflowSetup.deleteTemplateFile({ attachmentId }));
  };
  useUnsavedChangeRegistry(
    unsaveOptions,
    [
      !isEmpty(dependencies?.files) &&
        (initialValues.action !== formValues.action || fileParametersChanged)
    ],
    handleResetUploadStep
  );

  // Selectors
  const {
    action: actions,
    nm180codes,
    nm180Mcsr,
    nm183codes,
    nm181csa
  } = useSelectElementMetadataForManageAccountDelinquency();

  // Handlers
  const handleOnSelectAction = (
    item: ManageAccountDelinquencyMetaDataOption
  ) => {
    const value = item.value;
    const valid = isValid({
      parameters: formValues.parameters,
      action: value
    });

    setFormValues({
      ...formValues,
      action: value,
      isValid: valid
    });
  };

  // Get element data
  useEffect(() => {
    dispatch(
      actionsWorkflowSetup.getWorkflowMetadata([
        'action',
        SubTransactionCode.NM183,
        SubTransactionCode.NM181,
        SubTransactionCode.NM180Code,
        SubTransactionCode.NM180Csr,
        SubTransactionCode.TranSactionID,
        SubTransactionCode.NM182Transaction,
        SubTransactionCode.NM183Transaction,
        SubTransactionCode.NM181Transaction,
        SubTransactionCode.NM180_00Transaction,
        SubTransactionCode.NM180_03Transaction,
        SubTransactionCode.NM180_04Transaction,
        SubTransactionCode.NM180_05Transaction,
        SubTransactionCode.NM180_06Transaction,
        SubTransactionCode.NM180_07Transaction,
        SubTransactionCode.NM180_54Transaction,
        SubTransactionCode.NM180_50Transaction,
        SubTransactionCode.NM180_53Transaction,
        SubTransactionCode.NM180_60Transaction,
        SubTransactionCode.NM180_61Transaction,
        SubTransactionCode.NM180_62Transaction,
        SubTransactionCode.NM180_63Transaction
      ])
    );
  }, [dispatch]);

  const renderRightContent = () => {
    const action = formValues?.action;
    switch (action) {
      case ActionsValues.NM180Manual:
        return <NM180ManualRight />;
      case ActionsValues.NM180Workout:
        return <NM180WorkoutRight />;
      case ActionsValues.NM182:
        return <NM182Right />;
      case ActionsValues.NM183:
        return <NM183Right />;
      case ActionsValues.NM181:
        return <NM181Right />;
      default:
        return '';
    }
  };

  const renderBottomContent = () => {
    const action = formValues?.action;
    switch (action) {
      case ActionsValues.NM180Manual:
        return (
          <NM180MBottom
            key={action}
            formValues={formValues}
            setFormValues={setFormValues}
            codes={nm180codes}
            csa={nm180Mcsr}
            subtransactionCodeName={SubTransactionCode.NM180ManualCode}
            csrName={SubTransactionCode.NM180ManualCsr}
          />
        );
      case ActionsValues.NM180Workout:
        return (
          <NM180MBottom
            key={action}
            formValues={formValues}
            setFormValues={setFormValues}
            codes={nm180codes}
            csa={nm180Mcsr}
            subtransactionCodeName={SubTransactionCode.NM180WorkoutCode}
            csrName={SubTransactionCode.NM180WorkoutCsr}
          />
        );
      case ActionsValues.NM182:
        return <NM182Bottom formValues={formValues} />;
      case ActionsValues.NM183:
        return (
          <NM183Bottom
            formValues={formValues}
            setFormValues={setFormValues}
            codes={nm183codes}
          />
        );
      case ActionsValues.NM181:
        return (
          <NM181Bottom
            formValues={formValues}
            setFormValues={setFormValues}
            csa={nm181csa}
          />
        );
      default:
        return '';
    }
  };

  return (
    <div className="position-relative">
      <p className="mt-8 color-grey">
        {t(
          'txt_manage_account_delinquency_configure_parameters_step_description'
        )}
      </p>
      <div className="pt-24 mt-24 border-top">
        {isStuck && (
          <InlineMessage className="mb-24" variant="danger" withIcon>
            {t('txt_step_stuck_move_forward_message')}
          </InlineMessage>
        )}
        <h5 className="mb-16">
          {t(
            'txt_manage_account_delinquency_configure_parameters_step_choose_an_action'
          )}
        </h5>
        <div className="row">
          <div className="col-md-6">
            <div className="group-card">
              {actions?.map(
                (
                  item: ManageAccountDelinquencyMetaDataOption,
                  index: number
                ) => {
                  return (
                    <div
                      key={index}
                      className="group-card-item d-flex align-items-center px-16 py-18 custom-control-root"
                      onClick={() => handleOnSelectAction(item)}
                    >
                      <span className="pr-8">{item.description}</span>
                      <Radio className="ml-auto mr-n4">
                        <Radio.Input
                          checked={item.value === formValues?.action}
                          className="checked-style"
                        />
                      </Radio>
                    </div>
                  );
                }
              )}
            </div>
          </div>
          <div className="col-md-6">{renderRightContent()}</div>
        </div>
      </div>
      <div>{renderBottomContent()}</div>
    </div>
  );
};

const ExtraStaticConfigureParametersStep =
  ConfigureParametersStep as WorkflowSetupStaticProp<ConfigureParametersStepProps>;

ExtraStaticConfigureParametersStep.summaryComponent =
  ConfigureParametersStepSummary;
ExtraStaticConfigureParametersStep.defaultValues = {
  action: undefined,
  isValid: false,
  parameters: []
};
ExtraStaticConfigureParametersStep.parseFormValues = parseFormValues;
ExtraStaticConfigureParametersStep.stepValue = stepValueFunc;

export default ExtraStaticConfigureParametersStep;
