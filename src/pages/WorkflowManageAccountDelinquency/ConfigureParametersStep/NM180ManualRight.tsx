import { useTranslation } from 'app/_libraries/_dls';
import React from 'react';

const NM180ManualRight = () => {
  const { t } = useTranslation();
  return (
    <div className="bg-light-l20 rounded-lg p-16 h-100 ">
      <p className="fw-600 mb-8">
        {t(
          'txt_manage_account_delinquency_configure_parameters_step_NM180M_desc_1'
        )}
      </p>
      <p className="color-grey mb-8">
        {t(
          'txt_manage_account_delinquency_configure_parameters_step_NM180M_desc_2'
        )}
      </p>
      <ul className="color-grey mb-12 pl-16">
        <li className="mb-8">
          {t(
            'txt_manage_account_delinquency_configure_parameters_step_NM180M_rule_1'
          )}
        </li>
        <li className="mb-8">
          {t(
            'txt_manage_account_delinquency_configure_parameters_step_NM180M_rule_2'
          )}
        </li>
      </ul>
      <p className="color-grey">
        {t(
          'txt_manage_account_delinquency_configure_parameters_step_NM180M_note'
        )}
      </p>
    </div>
  );
};

export default NM180ManualRight;
