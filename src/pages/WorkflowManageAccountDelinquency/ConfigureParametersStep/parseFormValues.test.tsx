import { ActionsValues, SubTransactionCode } from '.';
import parseFormValues from './parseFormValues';

describe('ManageAccountDelinquencyWorkFlow > ConfigureParametersStep > parseFormValues', () => {
  it('Should return no data', () => {
    const props = {
      data: {
        data: {}
      } as any,
      id: '1'
    };
    const response = parseFormValues(props.data, props.id);
    expect(response).toEqual(undefined);
  });

  it('Should return data with wrong action', () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '1'
            }
          ] as any,
          configurations: {
            transactionId: 'wrong transaction id'
          }
        }
      } as any,
      id: '1'
    };
    const response = parseFormValues(props.data, props.id);
    expect(response).toEqual({
      isPass: false,
      isSelected: false,
      action: 'wrong transaction id',
      parameters: [],
      isValid: false
    });
  });

  it('Should return data of NM180 Manual', () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '1'
            }
          ] as any,
          configurations: {
            transactionId: ActionsValues.NM180Manual,
            parameters: [
              { name: SubTransactionCode.NM180Code, value: '00' },
              { name: SubTransactionCode.NM180Csr, value: 'Y' }
            ]
          }
        }
      } as any,
      id: '1'
    };

    const response = parseFormValues(props.data, props.id);
    expect(response)?.toEqual({
      isPass: false,
      isSelected: false,
      action: ActionsValues.NM180Manual,
      parameters: [
        { name: SubTransactionCode.NM180Code, value: '00' },
        { name: SubTransactionCode.NM180Csr, value: 'Y' },
        { name: SubTransactionCode.NM180ManualCode, value: '00' },
        { name: SubTransactionCode.NM180ManualCsr, value: 'Y' }
      ],
      isValid: true
    });
  });

  it('Should return data of NM180 Workout', () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '1'
            }
          ] as any,
          configurations: {
            transactionId: ActionsValues.NM180Workout,
            parameters: [
              { name: SubTransactionCode.NM180Code, value: '00' },
              { name: SubTransactionCode.NM180Csr, value: 'Y' }
            ]
          }
        }
      } as any,
      id: '1'
    };

    const response = parseFormValues(props.data, props.id);
    expect(response)?.toEqual({
      isPass: false,
      isSelected: false,
      action: ActionsValues.NM180Workout,
      parameters: [
        { name: SubTransactionCode.NM180Code, value: '00' },
        { name: SubTransactionCode.NM180Csr, value: 'Y' },
        { name: SubTransactionCode.NM180WorkoutCode, value: '00' },
        { name: SubTransactionCode.NM180WorkoutCsr, value: 'Y' }
      ],
      isValid: true
    });
  });

  it('Should return data of NM181', () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '1'
            }
          ] as any,
          configurations: {
            transactionId: ActionsValues.NM181,
            parameters: [{ name: SubTransactionCode.NM181, value: 'Y' }]
          }
        }
      } as any,
      id: '1'
    };

    const response = parseFormValues(props.data, props.id);
    expect(response)?.toEqual({
      isPass: false,
      isSelected: false,
      action: ActionsValues.NM181,
      parameters: [{ name: SubTransactionCode.NM181, value: 'Y' }],
      isValid: true
    });
  });

  it('Should return data of NM182', () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '1'
            }
          ] as any,
          configurations: {
            transactionId: ActionsValues.NM182
          }
        }
      } as any,
      id: '1'
    };

    const response = parseFormValues(props.data, props.id);
    expect(response)?.toEqual({
      isPass: false,
      isSelected: false,
      action: ActionsValues.NM182,
      parameters: [],
      isValid: true
    });
  });

  it('Should return data of NM183', () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '1'
            }
          ] as any,
          configurations: {
            transactionId: ActionsValues.NM183,
            parameters: [{ name: SubTransactionCode.NM183, value: '00' }]
          }
        }
      } as any,
      id: '1'
    };

    const response = parseFormValues(props.data, props.id);
    expect(response)?.toEqual({
      isPass: false,
      isSelected: false,
      action: ActionsValues.NM183,
      parameters: [{ name: SubTransactionCode.NM183, value: '00' }],
      isValid: true
    });
  });
});
