import { fireEvent, render } from '@testing-library/react';
import React from 'react';
import { ActionsValues, SubTransactionCode } from '.';
import NM181Bottom, { IProps } from './NM181Bottom';

jest.mock('./ParameterList', () => {
  return {
    __esModule: true,
    default: () => <div>ParameterList</div>
  };
});
const setFormValuesMock = jest.fn();
const renderComponent = (props?: IProps) => {
  const defaultProps: IProps = {
    formValues: undefined,
    csa: undefined,
    setFormValues: setFormValuesMock
  };
  const wrapper = render(<NM181Bottom {...defaultProps} {...props} />);
  return wrapper;
};

describe('NM181Bottom', () => {
  it('Render without values and without csa', () => {
    const wrapper = renderComponent();
    expect(
      wrapper.getByText(
        'txt_manage_account_delinquency_configure_parameters_step_NM181_title'
      )
    ).toBeInTheDocument();
    expect(wrapper.getByText('ParameterList')).toBeInTheDocument();
  });

  it('Render without values and with csa', () => {
    const wrapper = renderComponent({
      formValues: {
        parameters: [{ name: SubTransactionCode.NM180Code, value: '00' }]
      },
      setFormValues: setFormValuesMock,
      csa: [
        { value: 'Y', description: 'Yes' },
        { value: 'N', description: 'No', selected: true }
      ] as any
    });
    expect(
      wrapper.getByText(
        'txt_manage_account_delinquency_configure_parameters_step_NM181_title'
      )
    ).toBeInTheDocument();
    expect(wrapper.getByText('ParameterList')).toBeInTheDocument();
  });

  it('Render with full values', () => {
    const wrapper = renderComponent({
      formValues: {
        action: ActionsValues.NM181,
        parameters: [{ name: SubTransactionCode.NM181, value: 'N' }]
      },
      setFormValues: setFormValuesMock,
      csa: [
        { value: 'Y', description: 'Yes' },
        { value: 'N', description: 'No', selected: true }
      ] as any
    });
    expect(
      wrapper.getByText(
        'txt_manage_account_delinquency_configure_parameters_step_NM181_csr'
      )
    ).toBeInTheDocument();
    expect(wrapper.getByText('ParameterList')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('Yes'));
    expect(setFormValuesMock).toBeCalledWith({
      action: ActionsValues.NM181,
      isValid: true,
      parameters: [{ name: SubTransactionCode.NM181, value: 'Y' }]
    });
  });
});
