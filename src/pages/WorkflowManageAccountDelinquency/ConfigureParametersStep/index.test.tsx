import { fireEvent } from '@testing-library/dom';
import { renderWithMockStore } from 'app/utils';
import { STATUS } from 'app/_libraries/_dls/components/Upload/helper';
import React from 'react';
import * as ReactRedux from 'react-redux';
import ConfigureParameters, { ActionsValues, SubTransactionCode } from '.';

jest.mock('./NM183Bottom', () => {
  return {
    __esModule: true,
    default: () => <div>NM183Bottom</div>
  };
});
jest.mock('./NM181Bottom', () => {
  return {
    __esModule: true,
    default: () => <div>NM181Bottom</div>
  };
});
jest.mock('./NM180Bottom', () => {
  return {
    __esModule: true,
    default: () => <div>NM180Bottom</div>
  };
});
jest.mock('app/hooks/useUnsavedChangeRegistry', () => {
  return {
    __esModule: true,
    useUnsavedChangeRegistry: (options: any, changes: any, onConfirm: any) => {
      onConfirm && onConfirm();
    }
  };
});

const mockUseDispatch = jest.spyOn(ReactRedux, 'useDispatch');

const mockElementData = [
  {
    id: '51',
    name: 'transaction.id',
    options: [
      {
        value: ActionsValues.NM180Manual,
        description: 'Perform Manual Reage',
        workFlow: 'accountDelinquency'
      },
      {
        value: ActionsValues.NM180Workout,
        description: 'Perform Workout Reage',
        workFlow: 'accountDelinquency'
      },
      {
        value: ActionsValues.NM182,
        description: 'Recreate Account Delinquency',
        workFlow: 'accountDelinquency'
      },
      {
        value: ActionsValues.NM183,
        description:
          'Increase or Descrease Delinquency on Already Delinquent Accounts',
        workFlow: 'accountDelinquency'
      },
      {
        value: ActionsValues.NM181,
        description: 'Clear Account Delinquency and Update History',
        workFlow: 'accountDelinquency'
      }
    ]
  },
  {
    id: '54',
    name: SubTransactionCode.NM183,
    options: [
      {
        value: '01',
        description: "Add an amount to the account's total delinquency."
      },
      {
        value: '02',
        description:
          "Add an amount only to the cash advance portion of the account's delinquency."
      },
      {
        value: '03',
        description: "Subtract an amount from the account's total delinquency."
      },
      {
        value: '04',
        description:
          "Subtract an amount only from the cash advance portion of the account's delinquency."
      }
    ]
  }
];

const renderComponent = (props: any) => {
  return renderWithMockStore(<ConfigureParameters {...props} />, {
    initialState: {
      workflowSetup: {
        elementMetadata: {
          elements: mockElementData
        },
        parameterData: {
          parameterList: [] as any
        }
      }
    } as AppState
  });
};

const setFormValuesMock = jest.fn();
const clearFormValuesMock = jest.fn();
const dispatchMock = jest.fn();

describe('ManageAccountDelinquencyWorkFlow > ConfigureParameters > Index', () => {
  beforeEach(() => {
    mockUseDispatch.mockReturnValue(dispatchMock);
  });
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Initial Values', async () => {
    const props = {
      formValues: {
        config: {
          parameters: {}
        }
      },
      dependencies: {
        files: [
          {
            status: STATUS.valid
          }
        ]
      },
      setFormValues: setFormValuesMock,
      clearFormValues: clearFormValuesMock
    } as any;

    const { getByText } = await renderComponent(props);
    expect(
      getByText(
        'txt_manage_account_delinquency_configure_parameters_step_description'
      )
    ).toBeInTheDocument();
  });

  it('Action NM182', async () => {
    const props = {
      formValues: {
        action: ActionsValues.NM182
      },
      setFormValues: setFormValuesMock
    } as any;

    const { getByText } = await renderComponent(props);
    expect(
      getByText(
        'txt_manage_account_delinquency_configure_parameters_step_description'
      )
    ).toBeInTheDocument();
    expect(
      getByText(
        'txt_manage_account_delinquency_configure_parameters_step_NM182_desc_1'
      )
    ).toBeInTheDocument();
    expect(
      getByText(
        'txt_manage_account_delinquency_configure_parameters_step_parameter_list_description'
      )
    ).toBeInTheDocument();
    expect(getByText('Recreate Account Delinquency')).toBeInTheDocument();
    fireEvent.click(getByText('Recreate Account Delinquency'));
    expect(setFormValuesMock).toBeCalled();
  });

  it('Action NM183', async () => {
    const props = {
      formValues: {
        action: ActionsValues.NM183,
        parameters: [{ name: SubTransactionCode.NM183, value: '01' }]
      },
      setFormValues: setFormValuesMock
    } as any;

    const { getByText } = await renderComponent(props);
    expect(
      getByText(
        'txt_manage_account_delinquency_configure_parameters_step_description'
      )
    ).toBeInTheDocument();
    expect(
      getByText(
        'txt_manage_account_delinquency_configure_parameters_step_NM183_warning'
      )
    ).toBeInTheDocument();
    expect(getByText('NM183Bottom')).toBeInTheDocument();
    fireEvent.click(
      getByText(
        'Increase or Descrease Delinquency on Already Delinquent Accounts'
      )
    );
    expect(setFormValuesMock).toBeCalledWith({
      action: ActionsValues.NM183,
      isValid: true,
      parameters: [{ name: SubTransactionCode.NM183, value: '01' }]
    });
  });

  it('Action NM181', async () => {
    const props = {
      formValues: {
        action: ActionsValues.NM181,
        parameters: [{ name: SubTransactionCode.NM181, value: 'Y' }]
      },
      setFormValues: setFormValuesMock
    } as any;

    const { getByText } = await renderComponent(props);
    expect(
      getByText(
        'txt_manage_account_delinquency_configure_parameters_step_NM181_warning'
      )
    ).toBeInTheDocument();
    expect(
      getByText(
        'txt_manage_account_delinquency_configure_parameters_step_NM181_title'
      )
    ).toBeInTheDocument();
    expect(getByText('NM181Bottom')).toBeInTheDocument();
    fireEvent.click(getByText('Clear Account Delinquency and Update History'));
    expect(setFormValuesMock).toBeCalledWith({
      action: ActionsValues.NM181,
      isValid: true,
      parameters: [{ name: SubTransactionCode.NM181, value: 'Y' }]
    });
  });

  it('Action NM180 Manual', async () => {
    const props = {
      formValues: {
        action: ActionsValues.NM180Manual,
        parameters: [{ name: SubTransactionCode.NM180ManualCode, value: '00' }]
      },
      setFormValues: setFormValuesMock
    } as any;

    const { getByText } = await renderComponent(props);
    expect(
      getByText(
        'txt_manage_account_delinquency_configure_parameters_step_NM180M_desc_1'
      )
    ).toBeInTheDocument();
    expect(
      getByText(
        'txt_manage_account_delinquency_configure_parameters_step_NM180M_desc_2'
      )
    ).toBeInTheDocument();
    expect(getByText('NM180Bottom')).toBeInTheDocument();
    fireEvent.click(getByText('Perform Manual Reage'));
    expect(setFormValuesMock).toBeCalledWith({
      action: ActionsValues.NM180Manual,
      isValid: true,
      parameters: [{ name: SubTransactionCode.NM180ManualCode, value: '00' }]
    });
  });

  it('Action NM180 Workout', async () => {
    const props = {
      formValues: {
        action: ActionsValues.NM180Workout,
        parameters: [{ name: SubTransactionCode.NM180WorkoutCode, value: '00' }]
      },
      setFormValues: setFormValuesMock
    } as any;

    const { getByText } = await renderComponent(props);
    expect(
      getByText(
        'txt_manage_account_delinquency_configure_parameters_step_NM180K_warning'
      )
    ).toBeInTheDocument();
    expect(
      getByText(
        'txt_manage_account_delinquency_configure_parameters_step_NM180K_title'
      )
    ).toBeInTheDocument();
    expect(getByText('NM180Bottom')).toBeInTheDocument();
    fireEvent.click(getByText('Perform Workout Reage'));
    expect(setFormValuesMock).toBeCalledWith({
      action: ActionsValues.NM180Workout,
      isValid: true,
      parameters: [{ name: SubTransactionCode.NM180WorkoutCode, value: '00' }]
    });
  });

  it('Check fileParametersChanged when different step', async () => {
    const props = {
      formValues: {
        action: ActionsValues.NM180Workout,
        parameters: [{ name: SubTransactionCode.NM180WorkoutCode, value: '00' }]
      },
      setFormValues: setFormValuesMock,
      selectedStep: 'step1',
      stepId: 'step2',
      dependencies: { attachmentId: 'attachmentId' }
    } as any;

    await renderComponent(props);
    expect(dispatchMock).toBeCalled();
  });

  it('Check fileParametersChanged when select current step', async () => {
    const props = {
      formValues: { action: ActionsValues.NM180Manual },
      setFormValues: setFormValuesMock,
      selectedStep: 'step1',
      stepId: 'step1',
      dependencies: { attachmentId: 'attachmentId' }
    } as any;

    await renderComponent(props);
    expect(dispatchMock).toBeCalled();
  });
});
