import EnhanceDropdownList from 'app/components/EnhanceDropdownList';
import { useFormValidations } from 'app/hooks';
import {
  DropdownBaseChangeEvent,
  Icon,
  Radio,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import React, { useEffect, useMemo } from 'react';
import { ConfigureParametersStepProps, SubTransactionCode } from '.';
import ParameterList from './ParameterList';

interface IFieldValidate {
  code: string;
}
export interface IProps {
  formValues?: ConfigureParametersStepProps;
  codes?: (ManageAccountDelinquencyMetaDataOption & RefData)[];
  csa?: (ManageAccountDelinquencyMetaDataOption & RefData)[];
  setFormValues: (value: ConfigureParametersStepProps) => void;
  subtransactionCodeName: string;
  csrName: string;
}
const NM180CodeanualBottom: React.FC<IProps> = ({
  formValues = {},
  setFormValues,
  csa = [],
  codes = [],
  subtransactionCodeName,
  csrName
}) => {
  const { t } = useTranslation();
  const { action, parameters = [] } = formValues;
  const code = parameters.find(p => p.name === subtransactionCodeName)?.value;
  const csaValue = parameters.find(p => p.name === csrName)?.value;
  const currentErrors = useMemo(
    () =>
      ({
        code: !code?.trim() && {
          status: true,
          message: t('txt_required_validation', {
            fieldName: t('txt_subtransaction_code')
          })
        }
      } as Record<keyof IFieldValidate, IFormError>),
    [code, t]
  );
  const [, errors, setTouched] =
    useFormValidations<keyof IFieldValidate>(currentErrors);

  const handleCodeChange = (e: DropdownBaseChangeEvent) => {
    const name = e.target.name;
    const value = e.target.value.code;
    const filterParameters = parameters.filter(
      p => p.name !== name && p.name !== SubTransactionCode.NM180Code
    );
    const updateParamters = [
      ...filterParameters,
      { name, value },
      { name: SubTransactionCode.NM180Code, value: value }
    ];
    setFormValues({
      ...formValues,
      parameters: updateParamters,
      isValid: !!value && !!csaValue
    });
  };

  const handleCsaChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const name = e.target.name;
    const value = e.target.value;
    const filterParameters = parameters.filter(
      p => p.name !== name && p.name !== SubTransactionCode.NM180Csr
    );
    const updateParamters = [
      ...filterParameters,
      { name, value },
      { name: SubTransactionCode.NM180Csr, value: value }
    ];
    setFormValues({
      ...formValues,
      parameters: updateParamters,
      isValid: !!code && !!value
    });
  };

  useEffect(() => {
    if (!csaValue) {
      const defaultValue = csa.find(c => c.selected)?.value || csa[0]?.value;
      if (defaultValue) {
        const filterParameters = parameters.filter(
          p => p.name !== csrName && p.name !== SubTransactionCode.NM180Csr
        );
        const updateParamters = [
          ...filterParameters,
          { name: csrName, value: defaultValue },
          { name: SubTransactionCode.NM180Csr, value: defaultValue }
        ];
        setFormValues({
          ...formValues,
          parameters: updateParamters,
          isValid: !!code && !!defaultValue
        });
      }
    }
  }, [csaValue, csa, code, formValues, parameters, setFormValues, csrName]);

  return (
    <React.Fragment>
      <div className="pt-24 mt-24 border-top">
        <h5 className="mb-16">
          {t(
            'txt_manage_account_delinquency_configure_parameters_step_NM180M_title'
          )}
        </h5>
        <div className="row">
          <div className="col-lg-4 col-md-6">
            <EnhanceDropdownList
              value={codes?.find(o => o.code === code)}
              onChange={handleCodeChange}
              label={t(
                'txt_manage_account_delinquency_configure_parameters_step_NM180M_subtransaction_code'
              )}
              name={subtransactionCodeName}
              required
              onFocus={setTouched('code')}
              onBlur={setTouched('code', true)}
              error={errors.code}
              options={codes}
              tooltipElement={
                <div>
                  <div className="text-uppercase">
                    {t(
                      'txt_manage_account_delinquency_configure_parameters_step_NM180M_subtran'
                    )}
                  </div>
                  <p className="mt-8">
                    {t(
                      'txt_manage_account_delinquency_configure_parameters_step_NM180M_code_tooltip'
                    )}
                  </p>
                </div>
              }
            />
          </div>
        </div>
        {code === '00' && (
          <div>
            <div className="d-flex align-items-center mt-16">
              <p className="color-grey mr-8 mb-8 fw-600">
                {t(
                  'txt_manage_account_delinquency_configure_parameters_step_NM181_csr'
                )}
              </p>
              <Tooltip
                element={
                  <div>
                    <div className="text-uppercase">{t('txt_code')}</div>
                    <p className="mt-8">
                      {t(
                        'txt_manage_account_delinquency_configure_parameters_step_NM181_tooltip'
                      )}
                    </p>
                  </div>
                }
                triggerClassName="d-flex"
              >
                <Icon name="information" size="5x" className="color-grey-l16" />
              </Tooltip>
            </div>
            <div className="d-flex align-items-center">
              {csa.map(i => {
                return (
                  <Radio key={i.value} className="mr-24">
                    <Radio.Input
                      onChange={handleCsaChange}
                      checked={csaValue === i.value}
                      value={i.value}
                      defaultChecked={false}
                      name={csrName}
                    />
                    <Radio.Label>{i.description}</Radio.Label>
                  </Radio>
                );
              })}
            </div>
          </div>
        )}
      </div>
      {code && <ParameterList action={action!} subtransactionCode={code} />}
    </React.Fragment>
  );
};

export default NM180CodeanualBottom;
