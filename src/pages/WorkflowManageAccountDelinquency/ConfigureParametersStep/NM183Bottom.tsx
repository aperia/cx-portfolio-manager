import EnhanceDropdownList from 'app/components/EnhanceDropdownList';
import { useFormValidations } from 'app/hooks';
import { DropdownBaseChangeEvent, useTranslation } from 'app/_libraries/_dls';
import React, { useMemo } from 'react';
import { ConfigureParametersStepProps, SubTransactionCode } from '.';
import ParameterList from './ParameterList';

interface IFieldValidate {
  code: string;
}
export interface IProps {
  formValues?: ConfigureParametersStepProps;
  codes?: (ManageAccountDelinquencyMetaDataOption & RefData)[];
  setFormValues: (value: ConfigureParametersStepProps) => void;
}
const NM183Bottom: React.FC<IProps> = ({
  formValues = {},
  setFormValues,
  codes = []
}) => {
  const { t } = useTranslation();
  const { action, parameters = [] } = formValues;
  const code = parameters.find(p => p.name === SubTransactionCode.NM183)?.value;
  const currentErrors = useMemo(
    () =>
      ({
        code: !code?.trim() && {
          status: true,
          message: t(
            'txt_manage_account_delinquency_configure_parameters_step_select_a_code'
          )
        }
      } as Record<keyof IFieldValidate, IFormError>),
    [code, t]
  );
  const handleCodeChange = (e: DropdownBaseChangeEvent) => {
    const name = e.target.name;
    const value = e.target.value.code;
    const filterParameters = parameters.filter(p => p.name !== name);
    const updateParamters = [...filterParameters, { name, value }];
    setFormValues({
      ...formValues,
      isValid: !!value,
      parameters: updateParamters
    });
  };
  const [, errors, setTouched] =
    useFormValidations<keyof IFieldValidate>(currentErrors);

  return (
    <React.Fragment>
      <div className="pt-24 mt-24 border-top">
        <h5 className="mb-16">
          {t(
            'txt_manage_account_delinquency_configure_parameters_step_NM183_title'
          )}
        </h5>
        <div className="row">
          <div className="col-lg-4 col-md-6">
            <EnhanceDropdownList
              value={codes?.find(o => o.code === code)}
              onChange={handleCodeChange}
              label={t('txt_code')}
              name={SubTransactionCode.NM183}
              required
              onFocus={setTouched('code')}
              onBlur={setTouched('code', true)}
              error={errors.code}
              options={codes}
              tooltipElement={
                <div>
                  <div className="text-uppercase">{t('txt_code')}</div>
                  <p className="mt-8">
                    {t(
                      'txt_manage_account_delinquency_configure_parameters_step_NM183_code_tooltip'
                    )}
                  </p>
                </div>
              }
            />
          </div>
        </div>
      </div>
      {code && <ParameterList action={action!} />}
    </React.Fragment>
  );
};
export default NM183Bottom;
