import { Icon, Radio, Tooltip, useTranslation } from 'app/_libraries/_dls';
import React, { useEffect } from 'react';
import { ConfigureParametersStepProps, SubTransactionCode } from '.';
import ParameterList from './ParameterList';

export interface IProps {
  formValues?: ConfigureParametersStepProps;
  csa?: (ManageAccountDelinquencyMetaDataOption & RefData)[];
  setFormValues: (value: ConfigureParametersStepProps) => void;
}
const NM181Bottom: React.FC<IProps> = ({
  formValues = {},
  setFormValues,
  csa = []
}) => {
  const { t } = useTranslation();
  const { action, parameters = [] } = formValues;
  const value = parameters.find(
    p => p.name === SubTransactionCode.NM181
  )?.value;
  const handleCsaChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const name = e.target.name;
    const value = e.target.value;
    const filterParameters = parameters.filter(p => p.name !== name);
    const updateParamters = [...filterParameters, { name, value }];
    setFormValues({
      ...formValues,
      isValid: !!value,
      parameters: updateParamters
    });
  };

  useEffect(() => {
    if (!value) {
      const defaultValue = csa.find(c => c.selected)?.value || csa[0]?.value;
      if (defaultValue) {
        const filterParameters = parameters.filter(
          p => p.name !== SubTransactionCode.NM181
        );
        const updateParamters = [
          ...filterParameters,
          { name: SubTransactionCode.NM181, value: defaultValue }
        ];
        setFormValues({
          ...formValues,
          parameters: updateParamters
        });
      }
    }
  }, [value, csa, formValues, , parameters, setFormValues]);

  return (
    <React.Fragment>
      <div className="pt-24 mt-24 border-top">
        <h5 className="mb-16">
          {t(
            'txt_manage_account_delinquency_configure_parameters_step_NM181_title'
          )}
        </h5>
        <div>
          <div className="d-flex align-items-center mb-8">
            <p className="color-grey mr-8 fw-600">
              {t(
                'txt_manage_account_delinquency_configure_parameters_step_NM181_csr'
              )}
            </p>
            <Tooltip
              element={
                <div>
                  <div className="text-uppercase">{t('txt_code')}</div>
                  <p className="mt-8">
                    {t(
                      'txt_manage_account_delinquency_configure_parameters_step_NM181_tooltip'
                    )}
                  </p>
                </div>
              }
              triggerClassName="d-flex"
            >
              <Icon name="information" size="5x" className="color-grey-l16" />
            </Tooltip>
          </div>
          <div className="d-flex align-items-center">
            {csa.map(i => {
              return (
                <Radio key={i.value} className="mr-24">
                  <Radio.Input
                    onChange={handleCsaChange}
                    checked={value === i.value}
                    value={i.value}
                    name={SubTransactionCode.NM181}
                  />
                  <Radio.Label>{i.description}</Radio.Label>
                </Radio>
              );
            })}
          </div>
        </div>
      </div>
      <ParameterList action={action!} />
    </React.Fragment>
  );
};
export default NM181Bottom;
