import { ActionsValues, ConfigureParametersStepProps } from '.';
import stepValueFunc from './stepValueFunc';

describe('WorkflowManageAccountDelinquency > ConfigureParametersStep > stepValueFunc', () => {
  it('Should return text of NM180 Manual', () => {
    const stepsForm = {
      action: ActionsValues.NM180Manual
    } as ConfigureParametersStepProps;
    const t = (text: string) => text;
    const response = stepValueFunc({ stepsForm, t });
    expect(response).toEqual(
      'txt_manage_account_delinquency_configure_parameters_step_values_NM180Manual'
    );
  });

  it('Should return text of NM180 Workout', () => {
    const stepsForm = {
      action: ActionsValues.NM180Workout
    } as ConfigureParametersStepProps;
    const t = (text: string) => text;
    const response = stepValueFunc({ stepsForm, t });
    expect(response).toEqual(
      'txt_manage_account_delinquency_configure_parameters_step_values_NM180Workout'
    );
  });

  it('Should return text of NM181', () => {
    const stepsForm = {
      action: ActionsValues.NM181
    } as ConfigureParametersStepProps;
    const t = (text: string) => text;
    const response = stepValueFunc({ stepsForm, t });
    expect(response).toEqual(
      'txt_manage_account_delinquency_configure_parameters_step_values_NM181'
    );
  });

  it('Should return text of NM182', () => {
    const stepsForm = {
      action: ActionsValues.NM182
    } as ConfigureParametersStepProps;
    const t = (text: string) => text;
    const response = stepValueFunc({ stepsForm, t });
    expect(response).toEqual(
      'txt_manage_account_delinquency_configure_parameters_step_values_NM182'
    );
  });

  it('Should return text of NM183', () => {
    const stepsForm = {
      action: ActionsValues.NM183
    } as ConfigureParametersStepProps;
    const t = (text: string) => text;
    const response = stepValueFunc({ stepsForm, t });
    expect(response).toEqual(
      'txt_manage_account_delinquency_configure_parameters_step_values_NM183'
    );
  });

  it('Has no formValues', () => {
    const t = (text: string) => text;
    const response = stepValueFunc({ stepsForm: undefined as any, t });
    expect(response).toEqual('');
  });
});
