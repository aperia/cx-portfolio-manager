import { fireEvent, render } from '@testing-library/react';
import React from 'react';
import { ActionsValues, SubTransactionCode } from '.';
import NM180Bottom, { IProps } from './NM180Bottom';

jest.mock('./ParameterList', () => {
  return {
    __esModule: true,
    default: () => <div>ParameterList</div>
  };
});
const setFormValuesMock = jest.fn();
const renderComponent = (props?: IProps) => {
  const defaultProps: IProps = {
    formValues: undefined,
    csa: undefined,
    subtransactionCodeName: SubTransactionCode.NM180ManualCode,
    csrName: SubTransactionCode.NM180ManualCsr,
    setFormValues: setFormValuesMock
  };
  const wrapper = render(<NM180Bottom {...defaultProps} {...props} />);
  return wrapper;
};

describe('NM180Bottom', () => {
  it('Render without values and without csa', () => {
    const wrapper = renderComponent();
    expect(
      wrapper.getByText(
        'txt_manage_account_delinquency_configure_parameters_step_NM180M_title'
      )
    ).toBeInTheDocument();
  });

  it('Render without values and with csa', () => {
    const wrapper = renderComponent({
      formValues: {
        parameters: [
          { name: SubTransactionCode.NM180ManualCode, value: '00' },
          { name: SubTransactionCode.NM180Code, value: '00' }
        ]
      },
      setFormValues: setFormValuesMock,
      subtransactionCodeName: SubTransactionCode.NM180ManualCode,
      csrName: SubTransactionCode.NM180ManualCsr,
      csa: [
        { value: 'Y', description: 'Yes' },
        { value: 'N', description: 'No', selected: true }
      ] as any
    });
    expect(
      wrapper.getByText(
        'txt_manage_account_delinquency_configure_parameters_step_NM180M_title'
      )
    ).toBeInTheDocument();
  });

  it('Render with full values', () => {
    const wrapper = renderComponent({
      formValues: {
        action: ActionsValues.NM180Manual,
        parameters: [
          { name: SubTransactionCode.NM180ManualCode, value: '00' },
          { name: SubTransactionCode.NM180Code, value: '00' },
          { name: SubTransactionCode.NM180ManualCsr, value: 'N' },
          { name: SubTransactionCode.NM180Csr, value: 'N' }
        ]
      },
      setFormValues: setFormValuesMock,
      subtransactionCodeName: SubTransactionCode.NM180ManualCode,
      csrName: SubTransactionCode.NM180ManualCsr,
      codes: [
        { code: '00', text: '00-text' },
        { code: '01', text: '01-text' }
      ],
      csa: [
        { value: 'Y', description: 'Yes' },
        { value: 'N', description: 'No', selected: true }
      ] as any
    });
    expect(
      wrapper.getByText(
        'txt_manage_account_delinquency_configure_parameters_step_NM180M_title'
      )
    ).toBeInTheDocument();

    // handle change subtransaction code
    const element = wrapper.baseElement
      .querySelectorAll('.dls-dropdown-list .text-field-container')
      .item(0);
    fireEvent.focus(element);
    const pageElement = document.querySelectorAll('.dls-popup .item').item(1);
    fireEvent.click(pageElement);
    expect(setFormValuesMock).toBeCalledWith({
      action: ActionsValues.NM180Manual,
      isValid: true,
      parameters: [
        { name: SubTransactionCode.NM180ManualCsr, value: 'N' },
        { name: SubTransactionCode.NM180Csr, value: 'N' },
        { name: SubTransactionCode.NM180ManualCode, value: '01' },
        { name: SubTransactionCode.NM180Code, value: '01' }
      ]
    });

    // handle change csr
    fireEvent.click(wrapper.getByText('Yes'));
    expect(setFormValuesMock).toBeCalledWith({
      action: ActionsValues.NM180Manual,
      isValid: true,
      parameters: [
        { name: SubTransactionCode.NM180ManualCode, value: '00' },
        { name: SubTransactionCode.NM180Code, value: '00' },
        { name: SubTransactionCode.NM180ManualCsr, value: 'Y' },
        { name: SubTransactionCode.NM180Csr, value: 'Y' }
      ]
    });
    expect(wrapper.getByText('ParameterList')).toBeInTheDocument();
  });
});
