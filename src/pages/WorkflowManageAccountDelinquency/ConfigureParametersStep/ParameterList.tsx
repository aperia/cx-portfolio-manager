import { ColumnType, Grid, useTranslation } from 'app/_libraries/_dls';
import { useSelectParameterData } from 'pages/_commons/redux/WorkflowSetup';
import React from 'react';
export interface IProps {
  action: string;
  subtransactionCode?: string;
}
const ParameterList: React.FC<IProps> = ({ action, subtransactionCode }) => {
  const { t } = useTranslation();
  const columns: ColumnType[] = [
    {
      id: 'fieldName',
      Header: t('txt_field_name'),
      accessor: 'name',
      width: 211
    },
    {
      id: 'fieldType',
      Header: t('txt_field_type'),
      accessor: 'type',
      width: 103
    },
    {
      id: 'description',
      Header: t('txt_description'),
      accessor: 'description'
    }
  ];
  const parameterList = useSelectParameterData({
    actionCode: action,
    subTransactionCode: subtransactionCode!
  });

  return (
    <div className="pt-24 mt-24 border-top">
      <h5 className="mb-12">{t('txt_parameter_list')}</h5>
      <p className="color-grey">
        {t(
          'txt_manage_account_delinquency_configure_parameters_step_parameter_list_description'
        )}
      </p>
      <div className="mt-16">
        <Grid columns={columns} data={parameterList} />
      </div>
    </div>
  );
};

export default ParameterList;
