import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { default as stepRegistry } from '../_commons/WorkflowSetup/registries/stepRegistry';

const registerFunc = jest.spyOn(stepRegistry, 'registerStep');

describe('ManageAccountDelinquencyWorkFlow > registerSteps', () => {
  it('Should register components', async () => {
    await import('./registerSteps');

    expect(registerFunc).toBeCalledWith(
      'GetStartedManageAccountDelinquency',
      expect.anything()
    );

    expect(registerFunc).toBeCalledWith(
      'ConfigureParametersManageAccountDelinquency',
      expect.anything(),
      [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP_ACCOUNT_DELINQUENCY_CONFIGURE_PARAMETERS,
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP_ACCOUNT_DELINQUENCY_CONFIGURE_PARAMETERS_CONFIRM_EDIT
      ]
    );
  });
});
