import { ColumnType } from 'app/_libraries/_dls';
import { formatText } from 'pages/_commons/Utils/formatGridField';
import { FilterOption } from '../type';

export const columns = (t: any, isSort = true, width: number): ColumnType[] => {
  const cols = [
    {
      id: 'textId',
      Header: t('txt_workflow_bulk_archive_pcf_text_id'),
      accessor: 'textId',
      isSort,
      width: width < 1280 ? 156 : undefined
    },
    {
      id: 'textType',
      Header: t('txt_workflow_bulk_archive_pcf_text_type'),
      accessor: 'textType',
      isSort,
      width: width < 1280 ? 157 : undefined
    },
    {
      id: 'languageCode',
      Header: t('txt_workflow_bulk_archive_pcf_language_code'),
      accessor: 'languageCode',
      isSort,
      width: width < 1280 ? 259 : undefined
    },
    {
      id: 'effectiveDate',
      Header: t('txt_workflow_bulk_archive_pcf_effective_date'),
      accessor: formatText(['effectiveDate'], { format: 'date' }),
      isSort,
      width: width < 1280 ? 290 : undefined
    }
  ];
  return cols;
};

export const textAreaFilterValues = {
  all: 'all',
  selected: 'selected',
  unSelected: 'Unselected'
};

export const textAreaFilterOptions = (t: any): FilterOption[] => {
  return [
    {
      value: textAreaFilterValues.all,
      text: t('txt_all')
    },
    {
      value: textAreaFilterValues.selected,
      text: t('txt_workflow_bulk_archive_pcf_selected_text_ids')
    },
    {
      value: textAreaFilterValues.unSelected,
      text: t('txt_workflow_bulk_archive_pcf_unselected_text_ids')
    }
  ];
};

export const pricingTextAreaTestId = {
  viewInstructionModal: 'viewInstructionModal',
  dropdownElement: 'methodDropdownElement'
};
