import { useFormValidations } from 'app/hooks';
import {
  ComboBox,
  DropdownBaseChangeEvent,
  useTranslation
} from 'app/_libraries/_dls';
import { isFunction, isUndefined } from 'lodash';
import { useSelectElementsBulkArchivePCF } from 'pages/_commons/redux/WorkflowSetup';
import React, { useEffect, useMemo, useState } from 'react';
import { pricingTextAreaTestId } from './constant';

interface IProps {
  isExistTextArea?: boolean;
  textArea?: RefData;
  onChangeTextArea?: (textArea: RefData) => void;
}

const AddTextArea: React.FC<IProps> = ({
  isExistTextArea = false,
  textArea,
  onChangeTextArea
}) => {
  const { t } = useTranslation();
  const { textAreas } = useSelectElementsBulkArchivePCF();

  const [selectedTextArea, setSelectedTextArea] =
    useState<RefData | undefined>(textArea);
  useEffect(() => setSelectedTextArea(textArea), [textArea]);

  const currentErrors = useMemo(
    () =>
      ({
        textArea:
          (!selectedTextArea?.code?.trim() && {
            status: true,
            message: t('txt_required_validation', {
              fieldName: t('txt_workflow_bulk_archive_pcf_text_area')
            })
          }) ||
          (isExistTextArea && {
            status: true,
            message: t(
              'txt_workflow_bulk_archive_pcf_validation_exist_select_text_area'
            )
          })
      } as Record<'textArea', IFormError>),
    [selectedTextArea?.code, t, isExistTextArea]
  );
  const [, errors, setTouched] = useFormValidations<'textArea'>(currentErrors);

  const handleSelectTextArea = (e: DropdownBaseChangeEvent) => {
    if (isUndefined(textArea)) {
      setSelectedTextArea(e.target.value);
    }

    isFunction(onChangeTextArea) && onChangeTextArea(e.target.value);
  };

  return (
    <ComboBox
      textField="text"
      required
      value={selectedTextArea}
      label={t('txt_workflow_bulk_archive_pcf_text_area')}
      onChange={handleSelectTextArea}
      noResult={t('txt_no_results_found_without_dot')}
      onFocus={setTouched('textArea')}
      onBlur={setTouched('textArea', true)}
      error={errors.textArea}
    >
      {textAreas.map(item => (
        <ComboBox.Item
          key={item.code}
          value={item}
          label={item.text}
          dataTestId={pricingTextAreaTestId.dropdownElement}
        />
      ))}
    </ComboBox>
  );
};

export default AddTextArea;
