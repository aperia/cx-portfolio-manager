import { fireEvent, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { mockUseModalRegistryFnc, renderWithMockStore } from 'app/utils';
import mockDevice from 'app/utils/_mockHelperConstant/mockDevice';
import React from 'react';
import TextList from './TextList';

const mockUseModalRegistry = mockUseModalRegistryFnc();

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

jest.mock('./TextAreaSection', () => ({
  __esModule: true,
  default: ({
    onClickExpand,
    setSortBy,
    setSelectedList
  }: {
    onClickExpand: () => void;
    setSortBy: () => void;
    setSelectedList: () => void;
  }) => (
    <div>
      <button
        data-testid="onClickExpand"
        onClick={() => {
          onClickExpand();
        }}
      >
        expand
      </button>
      <button
        data-testid="setSortBy"
        onClick={() => {
          setSortBy();
        }}
      >
        setSortBy
      </button>
      <button
        data-testid="setSelectedList"
        onClick={() => {
          setSelectedList();
        }}
      >
        setSelectedList
      </button>
      MethodSection
    </div>
  )
}));

const textAreas = [
  {
    code: 'A001',
    text: 'A001',
    textIdList: [
      {
        rowId: '1',
        effectiveDate: '2022-12-14T00:00:00',
        textId: 'textId'
      },
      {
        rowId: '2',
        effectiveDate: '2022-12-14T00:00:00',
        textId: 'textId'
      },
      {
        rowId: '3',
        effectiveDate: '2022-12-14T00:00:00',
        textId: 'textId'
      },
      {
        rowId: '4',
        effectiveDate: '2022-12-14T00:00:00',
        textId: 'textId'
      }
    ] as any[],
    filteredTransactions: [
      {
        id: '1',
        rowId: '1',
        effectiveDate: '2022-12-14T00:00:00',
        textId: 'textId'
      },
      {
        id: '2',
        rowId: '2',
        effectiveDate: '2022-12-14T00:00:00',
        textId: 'textId'
      },
      {
        id: '3',
        rowId: '3',
        effectiveDate: '2022-12-14T00:00:00',
        textId: 'textId'
      },
      {
        id: '4',
        rowId: '4',
        effectiveDate: '2022-12-14T00:00:00',
        textId: 'textId'
      }
    ]
  },
  {
    code: 'A002',
    text: 'A002',
    textIdList: [
      {
        rowId: '5',
        effectiveDate: '2022-12-14T00:00:00',
        textId: 'textId'
      }
    ] as any[]
  },
  {
    code: 'A003',
    text: 'A003',
    textIdList: [
      {
        rowId: '6',
        effectiveDate: '2022-12-14T00:00:00',
        textId: 'textId'
      }
    ] as any[]
  }
];

HTMLCanvasElement.prototype.getContext = jest.fn();

const renderComponent = async (
  props: any,
  workflowSetup?: any
): Promise<RenderResult> => {
  return await renderWithMockStore(<TextList {...props} />, {
    initialState: { workflowSetup }
  });
};

describe('WorkflowBulkArchivePCF > SelectTextAreaStep > TextList', () => {
  beforeEach(() => {
    mockDevice.isDevice = false;

    mockUseModalRegistry.mockImplementation(() => [true, false]);
  });

  afterEach(() => {
    mockUseModalRegistry.mockClear();
  });

  const defaultWorkflowSetup = {
    workflowMethods: { loading: true, error: false }
  };
  it('should render list', async () => {
    const props = {
      formValues: { selectedList: {} },
      setFormValues: jest.fn(),
      textAreas,
      keepRef: { current: { setFormValues: jest.fn() } }
    } as any;
    const { baseElement, getByText, queryByText, getByPlaceholderText } =
      await renderComponent(props, defaultWorkflowSetup);

    // expand all text list
    const expands = baseElement.querySelectorAll('.icon.icon-plus');
    expands.forEach(userEvent.click as any);

    const searchBox = getByPlaceholderText('txt_type_to_search');

    userEvent.type(searchBox, 't');
    userEvent.click(
      searchBox.parentElement!.querySelector(
        '.icon-simple-search .icon.icon-search'
      )!
    );
    userEvent.click(getByText('txt_clear_and_reset'));

    userEvent.type(searchBox, 'notfound');
    userEvent.click(
      searchBox.parentElement!.querySelector(
        '.icon-simple-search .icon.icon-search'
      )!
    );

    expect(
      getByText('txt_no_results_found txt_adjust_your_search_criteria')
    ).toBeInTheDocument();

    userEvent.click(getByText('txt_clear_and_reset'));
    expect(
      queryByText('txt_no_results_found txt_adjust_your_search_criteria')
    ).not.toBeInTheDocument();

    // handle filter
    userEvent.click(getByText('txt_all'));
    userEvent.click(
      getByText('txt_workflow_bulk_archive_pcf_unselected_text_ids')
    );
  });

  it('should render list', async () => {
    const props = {
      formValues: {
        selectedList: {}
      },
      setFormValues: jest.fn(),
      textAreas: [
        {
          code: 'A001',
          text: 'A001',
          transactions: [
            {
              rowId: '1',
              effectiveDate: '2022-12-14T00:00:00',
              textId: 'textId'
            },
            {
              rowId: '2',
              effectiveDate: '2022-12-14T00:00:00',
              textId: 'textId'
            },
            {
              rowId: '3',
              effectiveDate: '2022-12-14T00:00:00',
              textId: 'textId'
            },
            {
              rowId: '4',
              effectiveDate: '2022-12-14T00:00:00',
              textId: 'textId'
            }
          ] as any[],
          filteredTransactions: [
            {
              id: '1',
              rowId: '1',
              effectiveDate: '2022-12-14T00:00:00',
              textId: 'textId'
            },
            {
              id: '2',
              rowId: '2',
              effectiveDate: '2022-12-14T00:00:00',
              textId: 'textId'
            },
            {
              id: '3',
              rowId: '3',
              effectiveDate: '2022-12-14T00:00:00',
              textId: 'textId'
            },
            {
              id: '4',
              rowId: '4',
              effectiveDate: '2022-12-14T00:00:00',
              textId: 'textId'
            }
          ]
        }
      ],
      stepId: '3',
      selectedStep: '2',
      isValid: false,
      keepRef: { current: { setFormValues: jest.fn() } }
    } as any;
    const { rerender, getByTestId } = await renderComponent(
      props,
      defaultWorkflowSetup
    );
    fireEvent.click(getByTestId('onClickExpand'));
    fireEvent.click(getByTestId('setSortBy'));
    fireEvent.click(getByTestId('setSelectedList'));
    rerender(<TextList {...props} textAreas={textAreas} />);
  });
});
