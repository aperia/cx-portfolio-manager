import { RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import * as helper from 'app/helpers/isDevice';
import { mockUseModalRegistryFnc, renderWithMockStore } from 'app/utils';
import React from 'react';
import TextAreaSection from './TextAreaSection';

const textAreas = [
  {
    code: 'A001',
    text: 'A001',
    textAreaName: 'textAreaName',
    textIdList: [
      {
        rowId: '1',
        effectiveDate: '2022-12-14T00:00:00',
        textId: 'textId',
        textAreaName: 'textAreaName'
      },
      {
        rowId: '2',
        effectiveDate: '2022-12-14T00:00:00',
        textId: 'textId',
        textAreaName: 'textAreaName'
      },
      {
        rowId: '3',
        effectiveDate: '2022-12-14T00:00:00',
        textId: 'textId',
        textAreaName: 'textAreaName'
      },
      {
        rowId: '4',
        effectiveDate: '2022-12-14T00:00:00',
        textId: 'textId',
        textAreaName: 'textAreaName'
      }
    ],
    transactions: [
      {
        rowId: '1',
        effectiveDate: '2022-12-14T00:00:00',
        textId: 'textId',
        textAreaName: 'textAreaName'
      },
      {
        rowId: '2',
        effectiveDate: '2022-12-14T00:00:00',
        textId: 'textId',
        textAreaName: 'textAreaName'
      },
      {
        rowId: '3',
        effectiveDate: '2022-12-14T00:00:00',
        textId: 'textId',
        textAreaName: 'textAreaName'
      },
      {
        rowId: '4',
        effectiveDate: '2022-12-14T00:00:00',
        textId: 'textId',
        textAreaName: 'textAreaName'
      }
    ] as any[],
    filteredTransactions: [
      {
        id: '1',
        rowId: '1',
        effectiveDate: '2022-12-14T00:00:00',
        textId: 'textId',
        textAreaName: 'textAreaName'
      },
      {
        id: '2',
        rowId: '2',
        effectiveDate: '2022-12-14T00:00:00',
        textId: 'textId',
        textAreaName: 'textAreaName'
      },
      {
        id: '3',
        rowId: '3',
        effectiveDate: '2022-12-14T00:00:00',
        textId: 'textId',
        textAreaName: 'textAreaName'
      },
      {
        id: '4',
        rowId: '4',
        effectiveDate: '2022-12-14T00:00:00',
        textId: 'textId',
        textAreaName: 'textAreaName'
      }
    ]
  },
  {
    code: 'A002',
    text: 'A002',
    textAreaName: 'textAreaName',
    textIdList: [
      {
        rowId: '5',
        effectiveDate: '2022-12-14T00:00:00',
        textId: 'textId'
      }
    ] as any[],
    filteredTransactions: [
      {
        rowId: '5',
        effectiveDate: '2022-12-14T00:00:00',
        textId: 'textId'
      }
    ]
  },
  {
    code: 'A003',
    text: 'A003',
    transactions: [
      {
        rowId: '6',
        effectiveDate: '2022-12-14T00:00:00',
        textId: 'textId',
        text: 'text'
      }
    ] as any[]
  }
];
const mockUseModalRegistry = mockUseModalRegistryFnc();

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderComponent = async (
  props: any,
  workflowSetup?: any
): Promise<RenderResult> => {
  return await renderWithMockStore(<TextAreaSection {...props} />, {
    initialState: { workflowSetup }
  });
};
const mockHelper: any = helper;
describe('WorkflowBulkArchivePCF > SelectTextArea > TextAreaSection', () => {
  beforeEach(() => {
    mockHelper.isDevice = false;

    mockUseModalRegistry.mockImplementation(() => [true, false]);
  });

  afterEach(() => {
    mockUseModalRegistry.mockClear();
  });

  const defaultWorkflowSetup = {
    workflowStrategies: { loading: true, error: false }
  };

  it('should render list', async () => {
    const props = {
      formValues: { textIDs: [] },
      stepId: '1',
      selectedStep: '2',
      setFormValues: jest.fn(),
      clearSearchValue: jest.fn(),
      onClickExpand: jest.fn(),
      setSelectedList: jest.fn(),
      setSortBy: jest.fn(),
      selectedList: [],
      sortBy: { id: 'textId' },
      filterValue: 'txt_all',
      resetFilter: true,
      textArea: {
        ...textAreas[0],
        filteredTransactions: textAreas[0].textIdList
      }

      //textAreas: textAreas
    } as any;
    mockHelper.isDevice = true;

    const { getByText, getAllByRole, rerender } = await renderComponent(
      props,
      defaultWorkflowSetup
    );

    const checkboxAll = getAllByRole('checkbox')[0];
    userEvent.click(checkboxAll);

    userEvent.click(getByText('txt_workflow_bulk_archive_pcf_text_id'));

    rerender(
      <TextAreaSection
        {...props}
        selectedList={['1', '2']}
        isExpand={true}
        searchValue={'a'}
      />
    );
  });

  it('should render list without filteredTransactions', async () => {
    const props = {
      formValues: { textIDs: [] },
      stepId: '1',
      selectedStep: '2',
      setFormValues: jest.fn(),
      clearSearchValue: jest.fn(),
      onClickExpand: jest.fn(),
      setSelectedList: jest.fn(),
      setSortBy: jest.fn(),
      selectedList: [],
      sortBy: { id: 'textId' },
      resetFilter: true,
      filterValue: 'txt_all',
      textArea: {
        ...textAreas[0],
        filteredTransactions: textAreas[0].filteredTransactions
      },

      textAreas: textAreas
    } as any;
    mockHelper.isDevice = true;

    const { getByText, getAllByRole, rerender } = await renderComponent(
      props,
      defaultWorkflowSetup
    );

    const checkboxAll = getAllByRole('checkbox')[0];

    userEvent.click(checkboxAll);
    userEvent.click(checkboxAll);

    userEvent.click(getByText('txt_workflow_bulk_archive_pcf_text_id'));

    rerender(
      <TextAreaSection {...props} selectedList={['1', '2']} isExpand={true} />
    );
  });

  it('render Nodata', async () => {
    const props = {
      formValues: {
        textAreas: [{ ...textAreas[1] }] as any,
        textIDs: [{ code: '123' }]
      },
      setFormValues: jest.fn(),
      clearSearchValue: jest.fn(),
      onClickExpand: jest.fn(),
      setSelectedList: jest.fn(),
      setSortBy: jest.fn(),
      selectedList: [],
      textArea: {
        ...textAreas[1],
        textIdList: [],
        filteredTransactions: textAreas[1].filteredTransactions
      }

      //textArea: textAreas
    } as any;

    await renderComponent(props, defaultWorkflowSetup);
  });

  it('click hide textArea', async () => {
    const props = {
      formValues: {
        textIDs: [{ ...textAreas[1] }] as any
      },
      setFormValues: jest.fn(),
      clearSearchValue: jest.fn(),
      onClickExpand: jest.fn(),
      setSelectedList: jest.fn(),
      setSortBy: jest.fn(),
      selectedList: [],
      textArea: {
        ...textAreas[1],
        filteredTransactions: textAreas[1].textIdList,
        textIdList: [
          {
            rowId: '1',
            effectiveDate: '2022-12-14T00:00:00',
            textId: 'textId'
          },
          {
            rowId: '2',
            effectiveDate: '2022-12-14T00:00:00',
            textId: 'textId'
          },
          {
            rowId: '3',
            effectiveDate: '2022-12-14T00:00:00',
            textId: 'textId'
          },
          {
            rowId: '4',
            effectiveDate: '2022-12-14T00:00:00',
            textId: 'textId'
          }
        ]
      }

      //textArea: textAreas
    } as any;

    const { getByText } = await renderComponent(props, defaultWorkflowSetup);

    userEvent.click(
      getByText('txt_workflow_bulk_archive_pcf_hide_this_text_area')
    );
  });
});
