import parseFormValues from './_parseFormValues';

describe('pages > WorkFlowBulkArchivePCF > SelectTextArea > parseFormValues', () => {
  const mockDispatch = jest.fn();

  it('Should return undefined', () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '2'
            }
          ] as any,
          configurations: {
            textIDs: [
              {
                code: '1',
                value: '1',
                textIdList: [{ selected: true, rowId: 'id' }]
              }
            ]
          } as any
        }
      } as WorkflowSetupInstance,
      id: '1'
    };

    const response = parseFormValues(props.data, props.id, mockDispatch);
    expect(response).toEqual(undefined);
  });

  it('Should return data', () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '1'
            }
          ] as any,
          configurations: {
            textIDs: [
              {
                code: '1',
                value: '1',
                textIdList: [{ selected: true, rowId: 'id' }]
              }
            ]
          } as any
        }
      } as WorkflowSetupInstance,
      id: '1'
    };

    const response = parseFormValues(props.data, props.id, mockDispatch);
    expect(response?.isPass).toEqual(false);
  });
});
