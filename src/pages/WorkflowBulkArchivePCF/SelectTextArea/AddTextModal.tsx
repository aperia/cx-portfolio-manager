import ModalRegistry from 'app/components/ModalRegistry';
import {
  InlineMessage,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import { isFunction } from 'lodash';
import React, { useState } from 'react';
import AddTextArea from './AddText';

export interface AddTextAreaModalProps {
  existTextAreaIds: string[];
  onClose?: VoidFunction;
  onAdd?: (textArea: RefData) => void;
}

const AddTextAreaModal: React.FC<AddTextAreaModalProps> = ({
  existTextAreaIds,
  onClose,
  onAdd
}) => {
  const { t } = useTranslation();

  const [textArea, setTextArea] = useState<RefData>();
  const [isExistTextArea, setIsExistTextArea] = useState(false);

  const handleAdd = () => {
    const _isExistTextArea =
      !!textArea?.code && existTextAreaIds.includes(textArea.code);
    if (_isExistTextArea) {
      setIsExistTextArea(_isExistTextArea);
      return;
    }

    isFunction(onAdd) && onAdd(textArea!);
    isFunction(onClose) && onClose();
  };

  const handleChange = (t: RefData) => {
    setTextArea(t);
  };

  return (
    <ModalRegistry id="AddTextAreaModal" show onAutoClosedAll={onClose}>
      <ModalHeader border closeButton onHide={onClose}>
        <ModalTitle>
          {t('txt_workflow_bulk_archive_pcf_add_text_area')}
        </ModalTitle>
      </ModalHeader>
      <ModalBody>
        {isExistTextArea && (
          <InlineMessage withIcon variant="danger">
            {t(
              'txt_workflow_bulk_archive_pcf_validation_exist_select_text_area'
            )}
          </InlineMessage>
        )}
        <AddTextArea
          isExistTextArea={isExistTextArea}
          textArea={textArea}
          onChangeTextArea={handleChange}
        />
      </ModalBody>
      <ModalFooter
        cancelButtonText={t('txt_cancel')}
        okButtonText={t('txt_continue')}
        disabledOk={!textArea}
        onCancel={onClose}
        onOk={handleAdd}
      />
    </ModalRegistry>
  );
};

export default AddTextAreaModal;
