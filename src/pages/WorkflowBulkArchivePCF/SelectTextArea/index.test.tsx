import { RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import {
  mockThunkAction,
  mockUseModalRegistryFnc,
  renderWithMockStore
} from 'app/utils';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowBulkArchivePCF';
import React from 'react';
import { ParameterCode } from '../type';
import { pricingTextAreaTestId } from './constant';
import SelectTextAreaStep, { FormSelectTextArea } from './index';

const mockUseModalRegistry = mockUseModalRegistryFnc();

HTMLCanvasElement.prototype.getContext = jest.fn();

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const MockSelectElementMetadataForTextList = jest.spyOn(
  WorkflowSetup,
  'useTextAreaTransactionsStatus'
);

const MockSelectElementMetadataForBulkArchivePCF = jest.spyOn(
  WorkflowSetup,
  'useSelectElementsBulkArchivePCF'
);

const renderComponent = async (
  props: any,
  workflowSetup?: any
): Promise<RenderResult> => {
  return await renderWithMockStore(<SelectTextAreaStep {...props} />, {
    initialState: { workflowSetup }
  });
};
const mockWorkflowSetup = mockThunkAction(actionsWorkflowSetup);

export const mockElements = [
  {
    name: ParameterCode.TextAreaName,
    options: [{ value: 'TextAreaName' }]
  }
];

describe('ManageAdjustmentsSystemWorkflow > SelectTextAreaStep > Index', () => {
  beforeEach(() => {
    mockUseModalRegistry.mockReturnValue([true, false]);
    mockWorkflowSetup('getBulkArchivePCFWorkflowTextAreas', {
      match: true
    });
    MockSelectElementMetadataForTextList.mockReturnValue({
      loading: true,
      error: false
    });
    MockSelectElementMetadataForBulkArchivePCF.mockReturnValue({
      strategies: [],
      methods: [],
      tableAreas: [],
      textAreas: [
        { code: 'text1', text: 'text1' },
        { code: 'text2', text: 'text2' }
      ]
    });
  });

  const defaultWorkflowSetup = {
    elementMetadata: { elements: mockElements },
    workflowMethods: { loading: true, error: false }
  };

  it('should render with empty text area', async () => {
    mockWorkflowSetup('getBulkArchivePCFWorkflowTextAreas', {
      match: true,
      payload: { payload: { textAreas: [{}] } }
    });
    MockSelectElementMetadataForTextList.mockReturnValueOnce({
      loading: true
    });
    const setFormValues = jest.fn() as any;
    const props = {
      formValues: {
        textIDs: [] as any,
        selectedList: {}
      },
      setFormValues,
      isStuck: true
    } as WorkflowSetupProps<FormSelectTextArea>;

    const { getByText, getByTestId } = await renderComponent(
      props,
      defaultWorkflowSetup
    );

    userEvent.click(getByTestId(pricingTextAreaTestId.viewInstructionModal));
    userEvent.click(getByText('txt_workflow_bulk_archive_pcf_text_area'));
    userEvent.click(getByText('txt_workflow_bulk_archive_pcf_text_area'));
    userEvent.click(getByText('text1'));

    expect(
      getByText('txt_workflow_bulk_archive_pcf_text_area')
    ).toBeInTheDocument();
  });

  it('should add text area success', async () => {
    mockWorkflowSetup('getBulkArchivePCFWorkflowTextAreas', {
      match: true,
      payload: { payload: { textAreas: [] } }
    });
    const setFormValues = jest.fn() as any;
    const props = {
      formValues: {
        textIDs: [{ code: 'text1', text: 'text1', textIdList: [] as any[] }],
        selectedList: {}
      },
      setFormValues
    } as WorkflowSetupProps<FormSelectTextArea>;

    const {
      baseElement,
      getByText,
      getByTestId,
      getAllByTestId,
      getByPlaceholderText
    } = await renderComponent(props, defaultWorkflowSetup);
    expect(
      getByText('txt_workflow_bulk_archive_pcf_add_text_area')
    ).toBeInTheDocument();

    userEvent.click(getByTestId(pricingTextAreaTestId.viewInstructionModal));
    userEvent.click(getByText('txt_close'));
    userEvent.click(getByText('txt_workflow_bulk_archive_pcf_add_text_area'));
    expect(baseElement.querySelector('.dls-modal-root')).toBeInTheDocument();

    userEvent.click(getByText('txt_workflow_bulk_archive_pcf_text_area'));
    userEvent.click(
      getAllByTestId(
        `${pricingTextAreaTestId.dropdownElement}_dls-high-light-words`
      )[0]
    );
    userEvent.click(getByText('txt_workflow_bulk_archive_pcf_text_area'));
    userEvent.click(
      getAllByTestId(
        `${pricingTextAreaTestId.dropdownElement}_dls-high-light-words`
      )[0]
    );
    userEvent.click(getByText('txt_continue'));

    const searchBox = getByPlaceholderText('txt_type_to_search');
    userEvent.type(searchBox, 'a');
    userEvent.click(
      searchBox.parentElement!.querySelector('.icon.icon-search')!
    );
  });

  it('should add method fail', async () => {
    mockWorkflowSetup('getBulkArchivePCFWorkflowTextAreas', {
      match: false
    });
    const setFormValues = jest.fn() as any;
    const props = {
      formValues: {
        textIDs: [{ code: '001', text: '001', textIdList: [] as any[] }],
        selectedList: {}
      },
      setFormValues
    } as WorkflowSetupProps<FormSelectTextArea>;

    const { baseElement, getByText, getByTestId } = await renderComponent(
      props,
      defaultWorkflowSetup
    );
    expect(
      getByText('txt_workflow_bulk_archive_pcf_add_text_area')
    ).toBeInTheDocument();
    userEvent.click(getByTestId(pricingTextAreaTestId.viewInstructionModal));

    userEvent.click(getByText('txt_workflow_bulk_archive_pcf_add_text_area'));
    expect(baseElement.querySelector('.dls-modal-root')).toBeInTheDocument();
    userEvent.click(getByText('txt_cancel'));

    userEvent.click(getByText('txt_workflow_bulk_archive_pcf_add_text_area'));

    userEvent.click(getByText('txt_workflow_bulk_archive_pcf_text_area'));
    userEvent.click(getByText('text1'));

    userEvent.click(getByText('txt_continue'));
  });
});
