import { classnames, isDevice } from 'app/helpers';
import { usePagination } from 'app/hooks/usePagination';
import { TextAreaDetail } from 'app/types';
import {
  Button,
  Grid,
  Icon,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty, isFunction } from 'lodash';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import Paging from 'pages/_commons/Utils/Paging';
import React, { useEffect, useMemo, useState } from 'react';
import { FormSelectTextArea } from '.';
import { columns } from './constant';

const SelectPricingTextAreaSummary: React.FC<
  WorkflowSetupSummaryProps<FormSelectTextArea>
> = ({ onEditStep, formValues, stepId, selectedStep }) => {
  const { t } = useTranslation();

  const [expandList, setExpandList] = useState<Record<string, boolean>>({});

  const handleEdit = () => {
    isFunction(onEditStep) && onEditStep();
  };

  const resetFilter = useMemo(
    () => stepId === selectedStep,
    [stepId, selectedStep]
  );

  const textAreaSelected = useMemo(() => {
    if (isEmpty(formValues?.textIDs)) return [];
    const { selectedList = {}, textIDs } = formValues;
    return textIDs
      ?.filter(m => selectedList[m.code]?.length)
      ?.map(m => ({
        ...m,
        textIdList: m.textIdList.filter(t =>
          selectedList[m.code].includes(t.id)
        )
      }));
  }, [formValues]);

  useEffect(() => {
    if (!resetFilter) return;
    const expands = textAreaSelected?.reduce((returnValue, m) => {
      return { ...returnValue, [m?.code]: true };
    }, {});
    resetFilter && setExpandList(expands);
  }, [textAreaSelected, resetFilter]);

  const handleClickExpand = (id: string, isExpand: boolean) => () => {
    setExpandList(list => ({ ...list, [id]: isExpand }));
  };

  return (
    <div className="position-relative">
      <div className="absolute-top-right mt-n26 mr-n8">
        <Button variant="outline-primary" size="sm" onClick={handleEdit}>
          {t('txt_edit')}
        </Button>
      </div>
      <div className="pt-16">
        {textAreaSelected?.map((textArea, idx) => {
          const isExpand = !!expandList[textArea.code];

          return (
            <div
              key={textArea.code}
              className={classnames(idx !== 0 && 'mt-24')}
            >
              <TextAreaSection
                textArea={textArea}
                isExpand={isExpand}
                resetFilter={resetFilter}
                onClickExpand={handleClickExpand(textArea.code, !isExpand)}
              />
            </div>
          );
        })}
      </div>
    </div>
  );
};

interface TextAreaSectionProps {
  isExpand: boolean;
  textArea: TextAreaDetail;
  resetFilter: boolean;
  onClickExpand: VoidFunction;
}
const TextAreaSection: React.FC<TextAreaSectionProps> = ({
  isExpand,
  textArea,
  resetFilter,
  onClickExpand
}) => {
  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();

  const { text: textAreaName, textIdList } = textArea;

  const {
    sort,
    total,
    currentPage,
    currentPageSize,
    gridData: dataView,
    onSortChange,
    onPageChange,
    onPageSizeChange,
    onResetToDefaultFilter
  } = usePagination(textIdList, [], 'textId');

  useEffect(() => {
    if (!resetFilter) return;

    onResetToDefaultFilter();
  }, [resetFilter, onResetToDefaultFilter]);

  return (
    <>
      <div className="d-flex align-items-center justify-content-between">
        <div className="d-flex align-items-center">
          <Tooltip
            element={isExpand ? t('txt_collapse') : t('txt_expand')}
            opened={isDevice ? false : undefined}
            variant="primary"
            placement="top"
            triggerClassName="d-flex ml-n2"
          >
            <Button size="sm" variant="icon-secondary" onClick={onClickExpand}>
              <Icon name={isExpand ? 'minus' : 'plus'} size="4x" />
            </Button>
          </Tooltip>

          <strong className="ml-8">{textAreaName}</strong>
        </div>
      </div>
      <div className={classnames(!isExpand && 'd-none')}>
        <div className="mt-16">
          <Grid
            dataItemKey="rowId"
            data={dataView}
            columns={columns(t, false, width)}
            sortBy={[sort]}
            onSortChange={onSortChange}
          />
          {total > 10 && (
            <div className="mt-16">
              <Paging
                page={currentPage}
                pageSize={currentPageSize}
                totalItem={total}
                onChangePage={onPageChange}
                onChangePageSize={onPageSizeChange}
              />
            </div>
          )}
        </div>
      </div>
    </>
  );
};

export default SelectPricingTextAreaSummary;
