import {
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { classnames } from 'app/helpers';
import { useUnsavedChangeRegistry } from 'app/hooks';
import { TextAreaDetail } from 'app/types';
import { Button, InlineMessage, useTranslation } from 'app/_libraries/_dls';
import { isEqual } from 'app/_libraries/_dls/lodash';
import { orderBy } from 'lodash';
import {
  actionsWorkflowSetup,
  useTextAreaTransactionsStatus
} from 'pages/_commons/redux/WorkflowSetup';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, { useEffect, useMemo, useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import { ParameterCode } from '../type';
import AddText from './AddText';
import AddTextModal from './AddTextModal';
import { pricingTextAreaTestId } from './constant';
import InstructionModal from './InstructionModal';
import Summary from './Summary';
import TextList from './TextList';
import parseFormValues from './_parseFormValues';

export interface FormSelectTextArea {
  isValid?: boolean;
  selectedList: Record<string, string[]>;
  textIDs: TextAreaDetail[];
}

const SelectTextAreaStep: React.FC<WorkflowSetupProps<FormSelectTextArea>> =
  props => {
    const { setFormValues, formValues, savedAt, isStuck } = props;
    const [initialValues, setInitialValues] = useState(formValues);
    const { t } = useTranslation();
    const dispatch = useDispatch();

    const keepRef = useRef({ setFormValues });
    keepRef.current.setFormValues = setFormValues;

    const { loading } = useTextAreaTransactionsStatus();
    const [showAddTextModal, setShowAddTextModal] = useState(false);
    const [showInstructionModal, setShowInstructionModal] = useState(false);

    const existTextAreaIds = useMemo(
      () => formValues?.textIDs?.map(m => m.code) || [],
      [formValues?.textIDs]
    );

    const hasTextArea = useMemo(
      () => existTextAreaIds.length > 0,
      [existTextAreaIds.length]
    );

    useEffect(() => {
      dispatch(
        actionsWorkflowSetup.getWorkflowMetadata([ParameterCode.TextAreaName])
      );
    }, [dispatch]);

    useEffect(() => {
      setInitialValues(formValues);
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [savedAt]);

    const formChanged = useMemo(
      () => !isEqual(formValues.textIDs, initialValues.textIDs),
      [formValues.textIDs, initialValues.textIDs]
    );

    useUnsavedChangeRegistry(
      {
        ...unsavedExistedWorkflowSetupDataProps,
        formName:
          UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP_BULK_ARCHIVE_PCF_SELECT_TEXT_AREA__EXISTED_WORKFLOW,
        priority: 1
      },
      [formChanged]
    );

    const handleSelectTextArea = async (textArea: RefData) => {
      const transactionsResp = (await Promise.resolve(
        dispatch(
          actionsWorkflowSetup.getBulkArchivePCFWorkflowTextAreas({
            textArea: textArea.code
          })
        )
      )) as any;
      const isSuccess =
        actionsWorkflowSetup.getBulkArchivePCFWorkflowTextAreas.fulfilled.match(
          transactionsResp
        );

      if (isSuccess) {
        const _textArea: TextAreaDetail = {
          ...textArea,
          textIdList: transactionsResp?.payload?.textAreas?.map(
            (t: any, idx: number) => {
              const rowId = `${textArea.code}_${idx}_${Date.now()}`;
              const transaction = {
                ...t,
                rowId: rowId,
                id: rowId,
                'text.id': t?.textId,
                'text.type': t?.textType,
                'language.code': t?.languageCode,
                'effective.date': t?.effectiveDate
              };

              return transaction;
            }
          )
        };

        const _textAreas = [_textArea, ...formValues.textIDs];

        setFormValues({ textIDs: orderBy(_textAreas, ['code']) });
      }
    };

    const handleOpenAddTextModal = () => {
      setShowAddTextModal(true);
    };

    const handleCloseAddTextModal = () => {
      setShowAddTextModal(false);
    };

    const instructionView = useMemo(
      () => (
        <>
          <div className="mt-8 pb-24 border-bottom color-grey">
            <p>
              {t('txt_workflow_bulk_archive_pcf_text_area_instruction')}{' '}
              <span
                className="link text-decoration-none px-4"
                onClick={() => setShowInstructionModal(true)}
                data-testid={pricingTextAreaTestId.viewInstructionModal}
              >
                {t('txt_workflow_bulk_archive_pcf_view_instruction')}
              </span>
            </p>
          </div>
          {isStuck && (
            <InlineMessage className="mb-0 mt-24" variant="danger" withIcon>
              {t('txt_step_stuck_move_forward_message')}
            </InlineMessage>
          )}
        </>
      ),
      // eslint-disable-next-line react-hooks/exhaustive-deps
      [isStuck]
    );

    return (
      <>
        {!hasTextArea && (
          <>
            {instructionView}
            <div className={classnames('mt-24', loading && 'loading')}>
              <h5>{t('txt_workflow_bulk_archive_pcf_select_text_area')}</h5>
              <div className="row mt-16">
                <div className="col-6 col-lg-4">
                  <AddText onChangeTextArea={handleSelectTextArea} />
                </div>
              </div>
            </div>
          </>
        )}
        {hasTextArea && (
          <>
            <div
              className={classnames('position-relative', loading && 'loading')}
            >
              <div className="absolute-top-right mt-n32 mr-n8">
                <Button
                  variant="outline-primary"
                  size="sm"
                  onClick={handleOpenAddTextModal}
                >
                  {t('txt_workflow_bulk_archive_pcf_add_text_area')}
                </Button>
              </div>
              {instructionView}
            </div>
            <div className="mt-24">
              <TextList
                textAreas={formValues?.textIDs}
                isValid={formValues.isValid}
                keepRef={keepRef}
                {...props}
              />

              {showAddTextModal && (
                <AddTextModal
                  existTextAreaIds={existTextAreaIds}
                  onClose={handleCloseAddTextModal}
                  onAdd={handleSelectTextArea}
                />
              )}
            </div>
          </>
        )}
        {showInstructionModal && (
          <InstructionModal
            opened={showInstructionModal}
            onClose={() => setShowInstructionModal(false)}
          />
        )}
      </>
    );
  };

const ExtraStaticSelectTextAreaStep =
  SelectTextAreaStep as WorkflowSetupStaticProp<FormSelectTextArea>;

ExtraStaticSelectTextAreaStep.summaryComponent = Summary;
ExtraStaticSelectTextAreaStep.defaultValues = {
  isValid: false,
  selectedList: {},
  textIDs: []
};
ExtraStaticSelectTextAreaStep.parseFormValues = parseFormValues;

export default ExtraStaticSelectTextAreaStep;
