import NoDataFound from 'app/components/NoDataFound';
import { classnames, getSelectedRecord, isDevice } from 'app/helpers';
import { useCheckAllPagination } from 'app/hooks';
import { usePagination } from 'app/hooks/usePagination';
import {
  Button,
  Grid,
  Icon,
  SortType,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import { differenceWith, isEmpty, isEqual } from 'app/_libraries/_dls/lodash';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import Paging from 'pages/_commons/Utils/Paging';
import React, { useEffect, useMemo } from 'react';
import { FormSelectTextArea } from '.';
import { columns } from './constant';
import { FilterTextAreaDetail } from './TextList';

export interface TextAreaSectionProps {
  isExpand: boolean;
  textArea: FilterTextAreaDetail;
  resetFilter: boolean;
  searchValue?: string;
  filterValue?: string;
  selectedList: string[];
  sortBy: SortType;
  setSortBy: (id: string, sort: SortType) => void;
  onClickExpand: VoidFunction;
  setSelectedList: (id: string, list: string[]) => void;
}
const TextAreaSection: React.FC<
  WorkflowSetupProps<FormSelectTextArea> & TextAreaSectionProps
> = ({
  stepId,
  selectedStep,
  isExpand,
  textArea,
  resetFilter,
  searchValue,
  filterValue,
  selectedList,
  sortBy,
  setSortBy,
  onClickExpand,
  setSelectedList,
  setFormValues,
  formValues
}) => {
  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();
  const { text: textAreaName, textIdList, filteredTransactions } = textArea;
  const isDefaultFilter = filterValue === t('txt_all');
  const isUnselected =
    filterValue === t('txt_workflow_bulk_archive_pcf_unselected_text_ids');

  const readOnlyCheckbox = useMemo(() => [] as string[], []);

  const {
    sort,
    total,
    currentPage,
    currentPageSize,
    gridData: dataView,
    dataChecked: _checkedList,
    onSetDataChecked,
    onSortChange,
    onPageChange,
    onPageSizeChange,
    onResetToDefaultFilter
  } = usePagination(filteredTransactions, [], 'textId');

  const checkedList = useMemo(
    () => _checkedList.filter(r => !readOnlyCheckbox.includes(r)),
    [_checkedList, readOnlyCheckbox]
  );
  const allIds = textIdList
    .map(f => f.rowId!)
    .filter(r => !readOnlyCheckbox.includes(r));
  const { handleClickCheckAll, gridClassName } = useCheckAllPagination(
    {
      gridClassName: `ba-pcf-text-area-${textArea.text?.replace(/\s/g, '')}`,
      allIds,
      hasFilter: !!searchValue || !isDefaultFilter,
      filterIds: filteredTransactions
        .map(f => f.rowId!)
        .filter(r => !readOnlyCheckbox.includes(r)),
      checkedList,
      setCheckAll: _isCheckAll => onSetDataChecked(_isCheckAll ? allIds : []),
      setCheckList: onSetDataChecked
    },
    [searchValue, sort, currentPage, currentPageSize]
  );

  useEffect(() => {
    if (stepId !== selectedStep) {
      onResetToDefaultFilter();
    }
  }, [stepId, selectedStep, onSetDataChecked, onResetToDefaultFilter]);

  useEffect(() => {
    if (isEqual(checkedList, selectedList)) return;
    setSelectedList(textArea.code, checkedList);
  }, [checkedList, textArea.code, setSelectedList, selectedList]);

  useEffect(() => {
    if (isEqual(sort, sortBy)) return;
    setSortBy(textArea.code, sort);
  }, [sort, sortBy, textArea.code, setSortBy]);

  useEffect(() => {
    isEmpty(checkedList) && onSetDataChecked(selectedList);
    // rerender selectedList after onSetDataChecked
    isEmpty(checkedList) && setSelectedList(textArea.code, selectedList);

    if (!isEmpty(sortBy)) onSortChange(sortBy);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (!resetFilter) return;
    onPageChange(1);
  }, [
    selectedList,
    resetFilter,
    textArea.code,
    onSetDataChecked,
    onPageChange
  ]);

  const isNoData = useMemo(() => textIdList.length <= 0, [textIdList.length]);

  const showCountSelectedSection = useMemo(
    () => !searchValue || checkedList?.length > 1,
    [searchValue, checkedList?.length]
  );

  const disableHideTextArea = checkedList?.length > 0;

  const selectedText = useMemo(
    () =>
      getSelectedRecord({
        selected: filteredTransactions.filter(i => selectedList.includes(i.id))
          .length,
        total: differenceWith(
          textIdList.map(f => f.rowId!),
          readOnlyCheckbox
        ).length,
        text: t('txt_workflow_bulk_archive_pcf_text_ids_selected', {
          selected: filteredTransactions.filter(i =>
            selectedList.includes(i.id)
          ).length
        }),
        text1: t('txt_workflow_bulk_archive_pcf_1_text_id_selected'),
        textAll: t('txt_workflow_bulk_archive_pcf_all_text_ids_selected')
      }),
    [readOnlyCheckbox, textIdList, t, filteredTransactions, selectedList]
  );

  const onClickHide = () => {
    setFormValues({
      textIDs: formValues.textIDs.filter(f => f.code !== textArea.code)
    });
  };

  return (
    <>
      <div className="d-flex align-items-center justify-content-between">
        <div className="d-flex align-items-center">
          <Tooltip
            element={isExpand ? t('txt_collapse') : t('txt_expand')}
            opened={isDevice ? false : undefined}
            variant="primary"
            placement="top"
            triggerClassName="d-flex ml-n2"
          >
            <Button size="sm" variant="icon-secondary" onClick={onClickExpand}>
              <Icon name={isExpand ? 'minus' : 'plus'} size="4x" />
            </Button>
          </Tooltip>

          <strong className="ml-8">{textAreaName}</strong>
        </div>
        <div className="mr-n8">
          <Tooltip
            opened={disableHideTextArea ? undefined : false}
            element={t(
              'txt_workflow_bulk_archive_pcf_hide_this_text_area_tooltip'
            )}
          >
            <Button
              size="sm"
              variant="outline-primary"
              disabled={disableHideTextArea}
              onClick={onClickHide}
            >
              {t('txt_workflow_bulk_archive_pcf_hide_this_text_area')}
            </Button>
          </Tooltip>
        </div>
      </div>
      <div className={classnames(!isExpand && 'd-none')}>
        {showCountSelectedSection &&
          !isNoData &&
          !searchValue &&
          !isUnselected && <p className="mt-16">{selectedText}</p>}

        {!isNoData && (
          <div className={classnames('mt-16')}>
            <Grid
              className={gridClassName}
              dataItemKey="rowId"
              rowKey="rowId"
              data={dataView}
              columns={columns(t, true, width)}
              checkedList={checkedList}
              sortBy={[sort]}
              onSortChange={onSortChange}
              variant={{
                id: 'rowId',
                type: 'checkbox',
                cellHeaderProps: { onClick: handleClickCheckAll }
              }}
              readOnlyCheckbox={readOnlyCheckbox}
              onCheck={onSetDataChecked}
            />
            {total > 10 && (
              <div className="mt-16">
                <Paging
                  page={currentPage}
                  pageSize={currentPageSize}
                  totalItem={total}
                  onChangePage={onPageChange}
                  onChangePageSize={onPageSizeChange}
                />
              </div>
            )}
          </div>
        )}

        {isNoData && (
          <div className="d-flex flex-column justify-content-center mt-40 mb-24">
            <NoDataFound
              id="newTextArea_notfound"
              title={t(
                'txt_workflow_bulk_archive_pcf_text_id_list_no_results_found'
              )}
            />
          </div>
        )}
      </div>
    </>
  );
};

export default TextAreaSection;
