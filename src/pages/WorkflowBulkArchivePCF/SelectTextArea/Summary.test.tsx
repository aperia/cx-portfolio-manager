import { fireEvent, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { renderWithMockStore } from 'app/utils';
import mockDevice from 'app/utils/_mockHelperConstant/mockDevice';
import React from 'react';
import { FormSelectTextArea } from '.';
import Summary from './Summary';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderComponent = async (
  props: any,
  workflowSetup?: any
): Promise<RenderResult> => {
  return await renderWithMockStore(<Summary {...props} />, {
    initialState: { workflowSetup }
  });
};

describe('WorkflowBulkArchivePCF > SelectTextArea > Summary', () => {
  beforeEach(() => {
    mockDevice.isDevice = false;
  });

  const defaultWorkflowSetup = {
    profileTransactions: { loading: true, error: false }
  };
  it('should render summary', async () => {
    const props = {
      formValues: {
        textIDs: [
          {
            code: '001',
            text: '001',
            textIdList: [
              { id: '1' },
              { id: '2' },
              { id: '3' },
              { id: '4' },
              { id: '5' },
              { id: '6' },
              { id: '7' },
              { id: '8' },
              { id: '9' },
              { id: '10' },
              { id: '11' }
            ] as any[]
          },
          { code: '002', text: '002', textIdList: [] as any[] }
        ],
        selectedList: {
          ['001']: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11']
        }
      }
    } as unknown as WorkflowSetupSummaryProps<FormSelectTextArea>;
    const { baseElement, getByText, rerender } = await renderComponent(
      props,
      defaultWorkflowSetup
    );

    const colllapses = baseElement.querySelectorAll('.icon.icon-minus');
    colllapses.forEach(userEvent.click as any);
    // expand all methods list
    const expands = baseElement.querySelectorAll('.icon.icon-plus');
    expands.forEach(userEvent.click as any);

    rerender(<Summary {...props} stepId="step1" selectedStep="step2" />);

    expect(getByText('txt_edit')).toBeInTheDocument();
  });

  it('should render summary in device', async () => {
    mockDevice.isDevice = true;

    const props = {
      formValues: {
        textIDs: [
          {
            code: '001',
            text: '001',
            textIdList: [{ id: '1' }] as any[]
          },
          { code: '002', text: '002', textIdList: [{ id: '2' }] as any[] }
        ],
        selectedList: {
          ['001']: ['1'],
          ['002']: ['2']
        }
      }
    } as unknown as WorkflowSetupSummaryProps<FormSelectTextArea>;
    const { baseElement, getByText, rerender } = await renderComponent(
      props,
      defaultWorkflowSetup
    );

    // expand all methods list
    let expands = baseElement.querySelectorAll('.icon.icon-plus');
    expands.forEach(userEvent.click as any);

    // expand all transactions
    expands = baseElement.querySelectorAll('.icon.icon-plus');
    expands.forEach(userEvent.click as any);

    rerender(<Summary {...props} stepId="step1" selectedStep="step2" />);

    expect(getByText('txt_edit')).toBeInTheDocument();
  });

  it('Should click on edit', async () => {
    const mockFn = jest.fn();
    const { getByText } = await renderComponent(
      {
        formValues: {
          textIDs: [{ code: '001', text: '001', textIdList: [] }]
        },
        onEditStep: mockFn
      } as any,
      defaultWorkflowSetup
    );

    fireEvent.click(getByText('txt_edit'));
    expect(mockFn).toHaveBeenCalled();
  });

  it('Should render empty method', async () => {
    const mockFn = jest.fn();
    const { getByText } = await renderComponent(
      {
        formValues: {},
        onEditStep: mockFn
      } as any,
      defaultWorkflowSetup
    );

    expect(getByText('txt_edit')).toBeInTheDocument();
  });
});
