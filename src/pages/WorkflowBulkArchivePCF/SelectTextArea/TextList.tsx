import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import { classnames, matchSearchValue } from 'app/helpers';
import { TextAreaDetail, TextAreaTransactions } from 'app/types';
import {
  DropdownBaseChangeEvent,
  DropdownList,
  SortType,
  useTranslation
} from 'app/_libraries/_dls';
import { differenceWith, isEmpty } from 'app/_libraries/_dls/lodash';
import ClearAndResetButton from 'pages/_commons/Utils/ClearAndResetButton';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { FormSelectTextArea } from '.';
import { checkValidItem, matchFilterTextArea } from '../helper';
import { FilterOption } from '../type';
import { textAreaFilterOptions } from './constant';
import TextAreaSection from './TextAreaSection';

export interface FilterTextAreaDetail extends TextAreaDetail {
  filteredTransactions: TextAreaTransactions[];
}
export interface TextAreaListProps {
  textAreas: TextAreaDetail[];
  keepRef: any;
  isValid?: boolean;
}

const TextAreaList: React.FC<
  WorkflowSetupProps<FormSelectTextArea> & TextAreaListProps
> = props => {
  const { t } = useTranslation();
  const { textAreas, stepId, selectedStep, keepRef, isValid, formValues } =
    props;

  const prevRef = useRef({ textAreas });
  const searchRef = useRef<any>(null);

  const [searchValue, setSearchValue] = useState<string>('');
  const [filterValue, setFilterValue] = useState<string>(t('txt_all'));
  const [shouldResetFilter, setShouldResetFilter] = useState(false);

  const [expandList, setExpandList] = useState<Record<string, boolean>>({});
  const [sortBy, setSortBy] = useState<Record<string, SortType>>({});

  const textAreaOptions = textAreaFilterOptions(t);

  const filteredTextAreas = useMemo(
    () =>
      textAreas
        .map(p => {
          if (isEmpty(p.textIdList)) return { ...p, filteredTransactions: [] };

          return {
            ...p,
            filteredTransactions: p.textIdList.filter(
              w =>
                matchSearchValue(
                  [w.textId, w.textType, w.languageCode],
                  searchValue
                ) &&
                matchFilterTextArea({
                  filterValue,
                  selectedList: formValues?.selectedList,
                  textArea: w,
                  t
                })
            )
          } as FilterTextAreaDetail;
        })
        .filter(
          f =>
            f.filteredTransactions.length > 0 ||
            (!searchValue && filterValue === t('txt_all'))
        ),
    [textAreas, searchValue, filterValue, formValues?.selectedList, t]
  );

  const handleExpandByDefault = useCallback(() => {
    const list: Record<string, boolean> = {};
    textAreas.forEach(textArea => {
      list[textArea.code] = true;
    });
    setExpandList(list);
    setShouldResetFilter(true);
  }, [textAreas]);

  const handleClearFilter = useCallback(() => {
    searchRef?.current?.clear();
    setSearchValue('');
    setFilterValue(t('txt_all'));
    handleExpandByDefault();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [handleExpandByDefault]);

  const handleSearch = (val: string) => {
    setSearchValue(val);
    handleExpandByDefault();
  };

  const handleFilter = (e: DropdownBaseChangeEvent) => {
    setFilterValue(e.target?.value);
  };

  useEffect(() => {
    if (stepId !== selectedStep) {
      setShouldResetFilter(true);
      handleClearFilter();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [stepId, selectedStep, handleClearFilter]);

  useEffect(() => {
    if (shouldResetFilter) {
      setShouldResetFilter(false);
    }
  }, [shouldResetFilter]);

  useEffect(() => {
    const checkValid = checkValidItem(
      formValues?.selectedList,
      textAreas.map(s => s.code)
    );
    if (isValid === checkValid) return;
    keepRef.current.setFormValues({ isValid: checkValid });
  }, [isValid, textAreas, keepRef, formValues?.selectedList]);

  useEffect(() => {
    const _prevRef = prevRef.current;
    const prevTextArea = _prevRef.textAreas;
    // reset filter when add new method
    if (textAreas.length > prevTextArea.length) {
      handleClearFilter();
    }
  }, [handleClearFilter, textAreas.length]);

  useEffect(() => {
    const _prevRef = prevRef.current;
    const prevTextArea = _prevRef.textAreas;

    const prevLength = prevTextArea.length;
    const currLength = textAreas.length;

    const isFirstAdd = currLength === prevLength && currLength === 1;

    const diff = isFirstAdd
      ? textAreas[0].code
      : differenceWith(
          textAreas,
          prevTextArea,
          (o1, o2) => o1.code === o2.code
        )?.[0]?.code;

    if (!!diff) {
      setExpandList({ ...expandList, [diff]: true });
      _prevRef.textAreas = textAreas;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [textAreas]);

  const handleClickExpand = (id: string, isExpand: boolean) => () => {
    setExpandList(list => ({ ...list, [id]: isExpand }));
  };

  const handleClickSort = (id: string, sort: SortType) => {
    setSortBy(list => ({ ...list, [id]: sort }));
  };

  const handleSetSelectedList = (id: string, _list: string[]) => {
    // setSelectedList(list => ({ ...list, [id]: _list }));
    keepRef.current.setFormValues({
      selectedList: { ...formValues?.selectedList, [id]: _list }
    });
  };

  return (
    <>
      <div className="d-flex align-items-center justify-content-between mb-16">
        <h5>{t('txt_workflow_bulk_archive_pcf_text_id_list')}</h5>
        <SimpleSearch
          ref={searchRef}
          placeholder={t('txt_type_to_search')}
          onSearch={handleSearch}
          popperElement={
            <>
              <p className="color-grey">{t('txt_search_description')}</p>
              <ul className="search-field-item list-unstyled">
                <li className="mt-16">
                  {t('txt_workflow_bulk_archive_pcf_language_code')}
                </li>
                <li className="mt-16">
                  {t('txt_workflow_bulk_archive_pcf_text_id')}
                </li>
                <li className="mt-16">
                  {t('txt_workflow_bulk_archive_pcf_text_type')}
                </li>
              </ul>
            </>
          }
        />
      </div>

      <div className="d-flex align-items-center">
        <strong className="fs-14 color-grey mr-4">{t('txt_filter')}:</strong>
        <DropdownList
          name="status"
          value={filterValue}
          textField="description"
          onChange={handleFilter}
          variant="no-border"
        >
          {textAreaOptions.map((item: FilterOption) => (
            <DropdownList.Item
              key={item.value}
              label={item.text}
              value={item.text}
            />
          ))}
        </DropdownList>
      </div>

      {searchValue && !isEmpty(filteredTextAreas) && (
        <div className="d-flex justify-content-end mb-16 mr-n8">
          <ClearAndResetButton
            small
            onClearAndReset={() => handleClearFilter()}
          />
        </div>
      )}
      {isEmpty(filteredTextAreas) && (
        <div className="d-flex flex-column justify-content-center mt-40 mb-24">
          <NoDataFound
            id="newTextArea_notfound"
            hasSearch={!!searchValue}
            hasFilter={filterValue !== t('txt_all')}
            title={t(
              'txt_workflow_bulk_archive_pcf_text_id_list_no_results_found'
            )}
            linkTitle={t('txt_clear_and_reset')}
            onLinkClicked={() => handleClearFilter()}
          />
        </div>
      )}
      {!isEmpty(filteredTextAreas) && (
        <div className="mt-16">
          {filteredTextAreas.map((textArea, idx) => {
            const isExpand = !!expandList[textArea.code];

            return (
              <div
                key={textArea.code}
                className={classnames(idx !== 0 && 'mt-24 pt-24 border-top')}
              >
                <TextAreaSection
                  {...props}
                  textArea={textArea}
                  isExpand={isExpand}
                  resetFilter={shouldResetFilter}
                  filterValue={filterValue}
                  searchValue={searchValue}
                  selectedList={formValues.selectedList[textArea.code] || []}
                  sortBy={sortBy[textArea.code]}
                  setSortBy={handleClickSort}
                  onClickExpand={handleClickExpand(textArea.code, !isExpand)}
                  setSelectedList={handleSetSelectedList}
                />
              </div>
            );
          })}
        </div>
      )}
    </>
  );
};

export default TextAreaList;
