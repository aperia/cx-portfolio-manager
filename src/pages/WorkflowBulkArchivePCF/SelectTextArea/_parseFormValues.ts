import { getWorkflowSetupStepStatus } from 'app/helpers';
import { TextAreaDetail } from 'app/types';
import { FormSelectTextArea } from '.';
import { checkValidItem } from '../helper';

const parseFormValues: WorkflowSetupStepFormDataFunc<FormSelectTextArea> = (
  data,
  id
) => {
  const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);
  const config = (data?.data as any)?.configurations || {};

  if (!stepInfo || !config) return;

  const textIDs = (config?.textIDs as TextAreaDetail[]) || [];

  const selectedList = {} as Record<string, string[]>;
  textIDs?.forEach(element => {
    selectedList[element?.code] = element?.textIdList
      ?.filter(i => i?.selected)
      .map(item => item?.rowId as string);
  });

  const isValid = checkValidItem(
    selectedList,
    textIDs.map(m => m.code)
  );

  return {
    ...getWorkflowSetupStepStatus(stepInfo),
    isValid,
    textIDs,
    selectedList
  };
};

export default parseFormValues;
