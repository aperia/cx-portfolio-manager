import { fireEvent, render } from '@testing-library/react';
import 'app/utils/_mockComponent/mockModalRegistry';
import React from 'react';
import AddTextModal from './AddTextModal';

jest.mock('./AddText', () => ({
  __esModule: true,
  default: ({
    onChangeTextArea
  }: {
    onChangeTextArea: (ref: { code: string; text: string }) => void;
  }) => (
    <div data-testid="AddMethod">
      AddMethod
      <button
        data-testid="addMethod"
        onClick={() => onChangeTextArea({ code: 'method2', text: 'text2' })}
      >
        txt_add_method
      </button>
    </div>
  )
}));

const factory = (isExistedMethod?: boolean) => {
  const { getByText, getByTestId, queryByText } = render(
    <AddTextModal
      existTextAreaIds={isExistedMethod ? ['method2'] : []}
      onClose={jest.fn()}
      onAdd={jest.fn()}
    />
  );

  return {
    addMethod: () => {
      jest.useFakeTimers();
      fireEvent.click(getByTestId('addMethod'));
      jest.runAllTimers();
      fireEvent.click(getByText('txt_continue'));
    },
    getInlineMsg: () =>
      queryByText(
        'txt_workflow_bulk_archive_pcf_validation_exist_select_method'
      )
  };
};

describe('test AddMethodModal component', () => {
  it('should be render', () => {
    factory();
  });
  it('should add method with existed id', () => {
    const { addMethod } = factory(true);
    addMethod();
  });
  it('should add method with existed id', () => {
    const { addMethod, getInlineMsg } = factory(false);
    addMethod();
    expect(getInlineMsg()).not.toBeInTheDocument();
  });
});
