import { FormGettingStart } from '.';
import stepValueFunc from './_stepValueFunc';

describe('WorkflowBulkArchivePCF > GettingStartStep > stepValueFunc', () => {
  const t = jest.fn().mockImplementation(v => v);
  beforeEach(() => {
    t.mockImplementation(v => v);
  });

  it('should return prm text', () => {
    const stepsForm: FormGettingStart = { prm: true };

    const response = stepValueFunc({ stepsForm, t });
    expect(response).toEqual('txt_pricing_methods');
  });

  it('should return prs text', () => {
    const stepsForm: FormGettingStart = { prs: true };

    const response = stepValueFunc({ stepsForm, t });
    expect(response).toEqual('txt_pricing_strategies');
  });

  it('should return textIds text', () => {
    const stepsForm: FormGettingStart = { textIds: true };

    const response = stepValueFunc({ stepsForm, t });
    expect(response).toEqual(
      'txt_workflow_bulk_archive_pcf_getting_start_selection_text_ids'
    );
  });

  it('should return tableIds text', () => {
    const stepsForm: FormGettingStart = { tableIds: true };

    const response = stepValueFunc({ stepsForm, t });
    expect(response).toEqual(
      'txt_workflow_bulk_archive_pcf_getting_start_selection_table_ids'
    );
  });

  it('should return all transaction text', () => {
    const stepsForm: FormGettingStart = {
      prm: true,
      prs: true,
      textIds: true,
      tableIds: true
    };

    const response = stepValueFunc({ stepsForm, t });
    expect(response).toEqual('txt_all_transactions_selected');
  });

  it('should return empty', () => {
    const stepsForm: any = undefined;

    const response = stepValueFunc({ stepsForm, t });
    expect(response).toEqual('');
  });
});
