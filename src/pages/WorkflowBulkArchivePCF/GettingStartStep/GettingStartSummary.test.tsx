import { fireEvent, render, RenderResult } from '@testing-library/react';
import React from 'react';
import { FormGettingStart } from '.';
import GettingStartSummary from './GettingStartSummary';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <GettingStartSummary {...props} />
    </div>
  );
};

describe('WorkflowBulkArchivePCF > GettingStartStep > GettingStartSummary', () => {
  it('Should render summary contains txt_workflow_bulk_archive_pcf_getting_start_selection_table_ids', () => {
    const wrapper = renderComponent({
      formValues: {
        tableIds: true
      }
    } as WorkflowSetupSummaryProps<FormGettingStart>);

    expect(
      wrapper.getByText(
        'txt_workflow_bulk_archive_pcf_getting_start_selection_table_ids'
      )
    ).toBeInTheDocument();
  });

  it('Should click on edit', () => {
    const mockFn = jest.fn();
    const wrapper = renderComponent({
      formValues: {
        tableIds: true
      },
      onEditStep: mockFn
    } as WorkflowSetupSummaryProps<FormGettingStart>);

    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(mockFn).toHaveBeenCalled();
  });
});
