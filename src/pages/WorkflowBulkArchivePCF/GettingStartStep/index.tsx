import { WORKFLOW_SETUP } from 'app/constants/local-storage';
import {
  unsavedExistedWorkflowSetupDataOnStuckProps,
  unsavedExistedWorkflowSetupDataProps,
  unsavedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { useUnsavedChangeRegistry } from 'app/hooks';
import {
  Button,
  CheckBox,
  Icon,
  InlineMessage,
  TransDLS,
  useTranslation
} from 'app/_libraries/_dls';
import ContentExpand from 'pages/_commons/ContentExpand';
import OverviewModal from 'pages/_commons/OverviewModal';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import GettingStartSummary from './GettingStartSummary';
import parseFormValues from './_parseFormValues';
import stepValueFunc from './_stepValueFunc';

const gettingStartOptions: RefData[] = [
  { code: 'prm', text: 'txt_pricing_methods' },
  { code: 'prs', text: 'txt_pricing_strategies' },
  {
    code: 'textIds',
    text: 'txt_workflow_bulk_archive_pcf_getting_start_selection_text_ids'
  },
  {
    code: 'tableIds',
    text: 'txt_workflow_bulk_archive_pcf_getting_start_selection_table_ids'
  }
];
export interface FormGettingStart {
  isValid?: boolean;

  prm?: boolean;
  prs?: boolean;
  textIds?: boolean;
  tableIds?: boolean;

  alreadyShown?: boolean;
}
export interface GettingStartProps
  extends WorkflowSetupProps<FormGettingStart> {
  prmFormId?: string;
  prsFormId?: string;
  textIdsFormId?: string;
  tableIdsFormId?: string;
}
const GettingStartStep: React.FC<GettingStartProps> = ({
  title,
  stepId,
  selectedStep,
  savedAt,
  formValues,
  hasInstance,
  isStuck,

  prmFormId,
  prsFormId,
  textIdsFormId,
  tableIdsFormId,

  setFormValues,
  clearFormValues
}) => {
  const { t } = useTranslation();
  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;

  const [initialValues, setInitialValues] = useState(formValues);
  const [showOverview, setShowOverview] = useState(
    formValues.alreadyShown
      ? false
      : localStorage.getItem(WORKFLOW_SETUP.SHOW_OVERVIEW_AGAIN) !== 'false'
  );

  const hasSelectedAnOption = useMemo(
    () =>
      !!formValues.prm ||
      !!formValues.prs ||
      !!formValues.textIds ||
      !!formValues.tableIds,
    [formValues.prm, formValues.prs, formValues.textIds, formValues.tableIds]
  );
  useEffect(() => {
    const isValid = hasSelectedAnOption;
    if (formValues.isValid === isValid) return;

    keepRef.current.setFormValues({ isValid });
  }, [formValues, hasSelectedAnOption]);

  useEffect(() => {
    setInitialValues(formValues);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [savedAt]);

  useUnsavedChangeRegistry(
    {
      ...unsavedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP_BULK_ARCHIVE_PCF_GET_STARTED,
      priority: 1
    },
    [!hasInstance && hasSelectedAnOption]
  );

  const handleConfirmChangeDependencies = useCallback(() => {
    prmFormId && !formValues.prm && clearFormValues(prmFormId);

    prsFormId && !formValues.prs && clearFormValues(prsFormId);

    textIdsFormId && !formValues.textIds && clearFormValues(textIdsFormId);

    tableIdsFormId && !formValues.tableIds && clearFormValues(tableIdsFormId);
  }, [
    clearFormValues,
    formValues.prm,
    formValues.prs,
    formValues.textIds,
    formValues.tableIds,
    prmFormId,
    prsFormId,
    textIdsFormId,
    tableIdsFormId
  ]);

  const isUncheckAll = useMemo(
    () =>
      !formValues.prm &&
      !formValues.prs &&
      !formValues.textIds &&
      !formValues.tableIds,
    [formValues.prm, formValues.prs, formValues.textIds, formValues.tableIds]
  );
  const hasChanged = useMemo(
    () =>
      initialValues.prm !== formValues.prm ||
      initialValues.prs !== formValues.prs ||
      initialValues.textIds !== formValues.textIds ||
      initialValues.tableIds !== formValues.tableIds,
    [initialValues, formValues]
  );

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP_BULK_ARCHIVE_PCF_GET_STARTED_EXISTED_WORKFLOW,
      priority: 1,
      confirmFirst: true
    },
    [!isUncheckAll && !!hasInstance && hasChanged],
    handleConfirmChangeDependencies
  );
  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataOnStuckProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP_BULK_ARCHIVE_PCF_GET_STARTED_EXISTED_WORKFLOW__STUCK,
      priority: 1,
      confirmFirst: true
    },
    [isUncheckAll && !!hasInstance && hasChanged]
  );

  const handleChangeOption = (option: keyof FormGettingStart) => {
    const onChange = (e: any) => {
      setFormValues({ [option]: !formValues[option] });
    };

    return onChange;
  };

  return (
    <React.Fragment>
      <ContentExpand
        isSelectedStep={stepId === selectedStep}
        change={savedAt}
        instruction={
          <div className="pt-24 px-24">
            <div className="d-flex align-items-center justify-content-between">
              <h4>{title}</h4>
              <Button
                className="mr-n8"
                variant="outline-primary"
                size="sm"
                onClick={() => setShowOverview(true)}
              >
                {t('txt_view_overview')}
              </Button>
            </div>
            <div className="pb-24">
              {isStuck && (
                <InlineMessage className="mb-0 mt-24" variant="danger" withIcon>
                  {t('txt_step_stuck_move_forward_message')}
                </InlineMessage>
              )}
              <div className="row">
                <div className="col-6 mt-24">
                  <div className="bg-light-l20 rounded-lg p-16">
                    <div className="text-center">
                      <Icon
                        name="request"
                        size="12x"
                        className="color-grey-l16"
                      />
                    </div>
                    <p className="mt-8 color-grey">
                      {t(
                        'txt_workflow_bulk_archive_pcf_getting_start_top_left_desc_1'
                      )}
                    </p>
                    <p className="mt-16 fw-600">
                      {t(
                        'txt_workflow_bulk_archive_pcf_getting_start_top_left_title_1'
                      )}
                    </p>
                    <p className="mt-8 color-grey">
                      {t(
                        'txt_workflow_bulk_archive_pcf_getting_start_top_left_desc_2'
                      )}
                    </p>
                  </div>
                </div>
                <div className="col-6 mt-24 color-grey">
                  <p>
                    {t(
                      'txt_workflow_bulk_archive_pcf_getting_start_top_right_title_1'
                    )}
                  </p>
                  <p className="mt-8">
                    <TransDLS keyTranslation="txt_workflow_bulk_archive_pcf_getting_start_top_right_desc_1">
                      <strong className="color-grey-d20" />
                    </TransDLS>
                  </p>
                  <p className="mt-8">
                    <TransDLS keyTranslation="txt_workflow_bulk_archive_pcf_getting_start_top_right_desc_2">
                      <strong className="color-grey-d20" />
                    </TransDLS>
                  </p>
                  <p className="mt-8">
                    <TransDLS keyTranslation="txt_workflow_bulk_archive_pcf_getting_start_top_right_desc_3">
                      <strong className="color-grey-d20" />
                    </TransDLS>
                  </p>
                  <p className="mt-8">
                    <TransDLS keyTranslation="txt_workflow_bulk_archive_pcf_getting_start_top_right_desc_4">
                      <strong className="color-grey-d20" />
                    </TransDLS>
                  </p>
                </div>
              </div>
            </div>
          </div>
        }
      >
        <div className="p-24">
          <h5>
            {t('txt_workflow_bulk_archive_pcf_getting_start_selection_title')}
          </h5>
          <div className="list-cards list-cards--selectable list-cards--multi mt-16">
            {gettingStartOptions.map(o => (
              <label
                key={o.code}
                className="list-cards__item custom-control-root"
                htmlFor={o.code}
              >
                <span className="d-flex align-items-center">
                  <span className="pr-8">{t(o.text)}</span>
                </span>
                <CheckBox className="mr-n4">
                  <CheckBox.Input
                    id={o.code}
                    checked={formValues[o.code as keyof FormGettingStart]}
                    onChange={handleChangeOption(o.code as any)}
                  />
                </CheckBox>
              </label>
            ))}
          </div>
        </div>
      </ContentExpand>
      {showOverview && (
        <OverviewModal
          id="overviewWorkflowSetup"
          show
          onClose={() => {
            setShowOverview(false);
            keepRef.current.setFormValues({ alreadyShown: true });
          }}
        />
      )}
    </React.Fragment>
  );
};

const ExtraStaticGettingStartStep =
  GettingStartStep as WorkflowSetupStaticProp<FormGettingStart>;

ExtraStaticGettingStartStep.summaryComponent = GettingStartSummary;
ExtraStaticGettingStartStep.stepValue = stepValueFunc;
ExtraStaticGettingStartStep.defaultValues = {
  isValid: false,
  prm: false,
  prs: false,
  textIds: false,
  tableIds: false
};
ExtraStaticGettingStartStep.parseFormValues = parseFormValues;

export default ExtraStaticGettingStartStep;
