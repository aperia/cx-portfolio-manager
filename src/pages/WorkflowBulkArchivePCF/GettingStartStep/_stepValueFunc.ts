import { FormGettingStart } from '.';

const stepValueFunc: WorkflowSetupStepValueFunc<FormGettingStart> = ({
  stepsForm: formValues,
  t
}) => {
  const { prm, prs, textIds, tableIds } = formValues || {};
  const isSelectedAll = prm && prs && textIds && tableIds;

  const options = [];
  prm && options.push(t('txt_pricing_methods'));
  prs && options.push(t('txt_pricing_strategies'));
  textIds &&
    options.push(
      t('txt_workflow_bulk_archive_pcf_getting_start_selection_text_ids')
    );
  tableIds &&
    options.push(
      t('txt_workflow_bulk_archive_pcf_getting_start_selection_table_ids')
    );

  return isSelectedAll
    ? t('txt_all_transactions_selected')
    : options.join(', ');
};

export default stepValueFunc;
