import parseFormValues from './_parseFormValues';

describe('WorkflowBulkArchivePCF > GettingStartStep > parseFormValues', () => {
  const dispatch = jest.fn();

  it('Should return undefined', () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '2'
            }
          ] as any
        }
      } as WorkflowSetupInstance,
      id: '1'
    };

    const response = parseFormValues(props.data, props.id, dispatch);
    expect(response).toEqual(undefined);
  });

  it('Should return data', () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '1'
            }
          ] as any
        }
      } as WorkflowSetupInstance,
      id: '1'
    };

    const response = parseFormValues(props.data, props.id, dispatch);
    expect(response).toEqual({ isPass: false, isSelected: false });
  });
});
