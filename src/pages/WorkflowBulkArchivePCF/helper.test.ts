import {
  checkValidItem,
  matchFilterMethod,
  matchFilterStrategy,
  matchFilterTableArea,
  matchFilterTextArea
} from './helper';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const t = (s: string) => s;

const methodMockData = {
  id: 'methodId',
  name: 'methodName',
  effectiveDate: '2021-06-18T17:00:00Z',
  status: 'Not Updated' as WorkflowSetupRecordType
};

const strategyMockData = {
  id: 'strategyId',
  strategyName: 'strategyName',
  effectiveDate: '2021-06-18T17:00:00Z',
  status: 'Not Updated' as WorkflowSetupRecordType
};

const textAreaMockData = {
  id: 'textId',
  textId: 'textId',
  textType: 'textType',
  languageCode: 'languageCode',
  effectiveDate: '2021-06-18T17:00:00Z',
  status: 'Not Updated' as WorkflowSetupRecordType
};

const tableAreaMockData = {
  id: 'tableId',
  tableId: 'tableId',
  effectiveDate: '2021-06-18T17:00:00Z',
  status: 'Not Updated' as WorkflowSetupRecordType
};

const selectedList = {
  codeA: [],
  codeB: ['checkItem1', 'checkItem2']
};

describe('WorkflowBulkArchivePCF > helpers > matchFilterMethod', () => {
  it('All methods', async () => {
    const matchValue = matchFilterMethod({
      filterValue: 'txt_all',
      selectedList: { sectionId: ['methodId'] },
      method: methodMockData,
      t
    });
    expect(matchValue).toEqual(true);
  });

  it('Selected methods', async () => {
    const matchValue = matchFilterMethod({
      filterValue: 'txt_workflow_bulk_archive_pcf_selected_method',
      selectedList: { sectionId: ['methodId'] },
      method: methodMockData,
      t
    });
    expect(matchValue).toEqual(true);
  });

  it('Uselected methods', async () => {
    const matchValue = matchFilterMethod({
      filterValue: 'txt_workflow_bulk_archive_pcf_unselected_method',
      selectedList: { sectionId: ['methodId'] },
      method: methodMockData,
      t
    });
    expect(matchValue).toEqual(false);
  });

  it('Default Value', async () => {
    const matchValue = matchFilterMethod({
      filterValue: '',
      selectedList: { sectionId: ['methodId'] },
      method: methodMockData,
      t
    });
    expect(matchValue).toEqual(true);
  });
});

describe('WorkflowBulkArchivePCF > helpers > matchFilterStrategy', () => {
  it('All strategies', async () => {
    const matchValue = matchFilterStrategy({
      filterValue: 'txt_all',
      selectedList: { sectionId: ['strategyId'] },
      strategy: strategyMockData,
      t
    });
    expect(matchValue).toEqual(true);
  });

  it('Selected strategies', async () => {
    const matchValue = matchFilterStrategy({
      filterValue: 'txt_workflow_bulk_archive_pcf_selected_strategies',
      selectedList: { sectionId: ['strategyId'] },
      strategy: strategyMockData,
      t
    });
    expect(matchValue).toEqual(true);
  });

  it('Uselected strategies', async () => {
    const matchValue = matchFilterStrategy({
      filterValue: 'txt_workflow_bulk_archive_pcf_unselected_strategies',
      selectedList: { sectionId: ['strategyId'] },
      strategy: strategyMockData,
      t
    });
    expect(matchValue).toEqual(false);
  });

  it('Default Value', async () => {
    const matchValue = matchFilterStrategy({
      filterValue: '',
      selectedList: { sectionId: ['methodId'] },
      strategy: strategyMockData,
      t
    });
    expect(matchValue).toEqual(true);
  });
});

describe('WorkflowBulkArchivePCF > helpers > matchFilterTableArea', () => {
  it('All table IDs', async () => {
    const matchValue = matchFilterTableArea({
      filterValue: 'txt_all',
      selectedList: { sectionId: ['tableId'] },
      tableArea: tableAreaMockData,
      t
    });
    expect(matchValue).toEqual(true);
  });

  it('Selected table IDs', async () => {
    const matchValue = matchFilterTableArea({
      filterValue: 'txt_workflow_bulk_archive_pcf_selected_table_ids',
      selectedList: { sectionId: ['tableId'] },
      tableArea: tableAreaMockData,
      t
    });
    expect(matchValue).toEqual(true);
  });

  it('Uselected table IDs', async () => {
    const matchValue = matchFilterTableArea({
      filterValue: 'txt_workflow_bulk_archive_pcf_unselected_table_ids',
      selectedList: { sectionId: ['tableId'] },
      tableArea: tableAreaMockData,
      t
    });
    expect(matchValue).toEqual(false);
  });

  it('Default Value', async () => {
    const matchValue = matchFilterTableArea({
      filterValue: '',
      selectedList: { sectionId: ['tableId'] },
      tableArea: tableAreaMockData,
      t
    });
    expect(matchValue).toEqual(true);
  });
});

describe('WorkflowBulkArchivePCF > helpers > matchFilterTextArea', () => {
  it('All text IDs', async () => {
    const matchValue = matchFilterTextArea({
      filterValue: 'txt_all',
      selectedList: { sectionId: ['strategyId'] },
      textArea: textAreaMockData,
      t
    });
    expect(matchValue).toEqual(true);
  });

  it('Selected text IDs', async () => {
    const matchValue = matchFilterTextArea({
      filterValue: 'txt_workflow_bulk_archive_pcf_selected_text_ids',
      selectedList: { sectionId: ['textId'] },
      textArea: textAreaMockData,
      t
    });
    expect(matchValue).toEqual(true);
  });

  it('Uselected text IDs', async () => {
    const matchValue = matchFilterTextArea({
      filterValue: 'txt_workflow_bulk_archive_pcf_unselected_text_ids',
      selectedList: { sectionId: ['textId'] },
      textArea: textAreaMockData,
      t
    });
    expect(matchValue).toEqual(false);
  });

  it('Default Value', async () => {
    const matchValue = matchFilterTextArea({
      filterValue: '',
      selectedList: { sectionId: ['textId'] },
      textArea: textAreaMockData,
      t
    });
    expect(matchValue).toEqual(true);
  });
});

describe('WorkflowBulkArchivePCF > helpers > checkValidItem', () => {
  it('Check with selected Item', async () => {
    const checked = checkValidItem(selectedList, ['codeA', 'codeB']);
    expect(checked).toEqual(true);
  });

  it('Check without selected Item', async () => {
    const checked = checkValidItem(selectedList, []);
    expect(checked).toEqual(false);
  });
});
