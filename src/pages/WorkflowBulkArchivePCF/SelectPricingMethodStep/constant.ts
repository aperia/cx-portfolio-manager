import { ColumnType } from 'app/_libraries/_dls';
import { formatText } from 'pages/_commons/Utils/formatGridField';
import { FilterOption } from '../type';

export const columns = (t: any, isSort = true): ColumnType[] => {
  const cols = [
    {
      id: 'name',
      Header: t('txt_workflow_bulk_archive_pcf_method_name'),
      accessor: 'name',
      isSort
    },
    {
      id: 'effectiveDate',
      Header: t('txt_workflow_bulk_archive_pcf_effective_date'),
      accessor: formatText(['effectiveDate'], { format: 'date' }),
      isSort
    }
  ];
  return cols;
};

export const methodFilterValues = {
  all: 'all',
  selected: 'selected',
  unSelected: 'Unselected'
};

export const methodFilterOptions = (t: any): FilterOption[] => {
  return [
    {
      value: methodFilterValues.all,
      text: t('txt_all')
    },
    {
      value: methodFilterValues.selected,
      text: t('txt_workflow_bulk_archive_pcf_selected_method')
    },
    {
      value: methodFilterValues.unSelected,
      text: t('txt_workflow_bulk_archive_pcf_unselected_method')
    }
  ];
};

export const pricingMethodTestId = {
  viewInstructionModal: 'viewInstructionModal',
  dropdownElement: 'methodDropdownElement'
};
