import EnhanceDropdownList from 'app/components/EnhanceDropdownList';
import { useFormValidations } from 'app/hooks';
import {
  Button,
  ComboBox,
  DropdownBaseChangeEvent,
  useTranslation
} from 'app/_libraries/_dls';
import { isFunction, orderBy } from 'lodash';
import isEmpty from 'lodash.isempty';
import { useSelectServicesData } from 'pages/_commons/redux/WorkflowSetup';
import React, { useEffect, useMemo, useState } from 'react';

interface IProps {
  onChangeMethod?: (method: RefData) => void;
  isButton?: boolean;
  handleChangeForm?: (data: {
    service: {
      code: string;
      text: string;
    };
    subject: {
      code: string;
      text: string;
    };
    section: {
      code: string;
      text: string;
    };
  }) => void;
}

const defaultOption = { code: '', text: '' };

const initformValues = {
  service: defaultOption,
  subject: defaultOption,
  section: defaultOption
};

const AddMethod: React.FC<IProps> = ({
  onChangeMethod,
  isButton,
  handleChangeForm
}) => {
  const { t } = useTranslation();
  const service = useSelectServicesData();

  const [formValues, setFormValues] = useState(initformValues);

  const serviceCodeVal = formValues.service.code;
  const subjectCodeVal = formValues.subject.code;
  const sectionCodeVal = formValues.section.code;

  const [resetTouch, setResetTouch] = useState(0);
  useEffect(() => {
    process.nextTick(() => {
      setResetTouch(t => t + 1);
    });
  }, [serviceCodeVal, subjectCodeVal]);

  const currentErrors = useMemo(() => {
    return {
      serviceCode: !serviceCodeVal?.trim() && {
        status: true,
        message: t('txt_required_validation', {
          fieldName: t('txt_utilities_pcf_analyzer_service')
        })
      },
      subjectCode: !subjectCodeVal?.trim() && {
        status: true,
        message: t('txt_required_validation', {
          fieldName: t('txt_utilities_pcf_analyzer_subject')
        })
      },
      sectionCode: !sectionCodeVal?.trim() && {
        status: true,
        message: t('txt_required_validation', {
          fieldName: t('txt_utilities_pcf_analyzer_section')
        })
      }
    } as Record<any, IFormError>;
  }, [t, serviceCodeVal, subjectCodeVal, sectionCodeVal]);
  const [, errors, setTouched] = useFormValidations<any>(
    currentErrors,
    resetTouch
  );

  const serviceList = orderBy(service, ['name'], ['asc']).map(x => {
    return { code: x.name, text: x.name + ', ' + x.description };
  });

  const subjectList = useMemo(() => {
    const currentService = serviceCodeVal;
    const subjects =
      service.find(x => x.name === currentService)?.subjects || [];

    let data: RefData[] = [];
    data = subjects.map(y => {
      return { code: y.name, text: y.name + ', ' + y.description };
    });

    orderBy(data, ['code'], ['asc']);

    return data;
  }, [serviceCodeVal, service]);

  const sectionList = useMemo(() => {
    const currentService = serviceCodeVal;
    const currentSubject = subjectCodeVal;
    const subjects =
      service.find(x => x.name === currentService)?.subjects || [];

    const sections =
      subjects.find(x => x.name === currentSubject)?.sections || [];

    const data = sections.map(y => {
      return { code: y.name, text: y.name + ', ' + y.description };
    });
    const ordData = orderBy(data, ['code'], ['asc']);

    return ordData;
  }, [subjectCodeVal, serviceCodeVal, service]);

  const isValid = useMemo(() => {
    const props = ['serviceCode', 'subjectCode', 'sectionCode'];
    return props.every(p => !errors[p]?.status);
  }, [errors]);

  const hanldeOnchange =
    (action: string) => (event: DropdownBaseChangeEvent) => {
      const { text, code } = event?.target?.value || {};
      switch (action) {
        case 'service':
          if (formValues[action].code !== code)
            setFormValues({
              ...formValues,
              [action]: { code, text },
              subject: defaultOption,
              section: defaultOption
            });
          isFunction(handleChangeForm) &&
            handleChangeForm({
              ...formValues,
              [action]: { code, text },
              subject: defaultOption,
              section: defaultOption
            });
          break;

        case 'subject':
          if (formValues[action].code !== code)
            setFormValues({
              ...formValues,
              [action]: { code, text },
              section: defaultOption
            });
          isFunction(handleChangeForm) &&
            handleChangeForm({
              ...formValues,
              [action]: { code, text },
              section: defaultOption
            });
          break;

        default:
          setFormValues({
            ...formValues,
            [action]: {
              code,
              text
            }
          });
          isFunction(handleChangeForm) &&
            handleChangeForm({
              ...formValues,
              [action]: {
                code,
                text
              }
            });
          break;
      }
    };

  const handleValue = (value: RefData) => {
    return value.code !== '' ? value : undefined;
  };

  const handleValueCombobox = (value: RefData) => {
    return value.code === undefined || isEmpty(value.code)
      ? undefined
      : handleValue(value);
  };

  const handleOnBlur = () => {
    setTouched('sectionCode', true)();
    const { code, text } = formValues?.section;

    setFormValues({
      ...formValues,
      section: {
        code: code === undefined ? '' : code,
        text: text === undefined ? '' : text
      }
    });
  };

  const handleAddPricing = () => {
    isFunction(onChangeMethod) &&
      onChangeMethod({
        code: `${serviceCodeVal} ${subjectCodeVal} ${sectionCodeVal}`
      });
  };

  return (
    <>
      <div className="row mt-16">
        <div className="col-4">
          <EnhanceDropdownList
            required={true}
            label={t('txt_utilities_pcf_analyzer_service')}
            name="serviceCode"
            onFocus={setTouched('serviceCode')}
            onBlur={setTouched('serviceCode', true)}
            error={errors.serviceCode}
            options={serviceList}
            onChange={hanldeOnchange('service')}
            value={() => handleValue(formValues.service)}
          />
        </div>
        <div className="col-4">
          <EnhanceDropdownList
            required={true}
            label={t('txt_utilities_pcf_analyzer_subject')}
            name="subjectCode"
            onFocus={setTouched('subjectCode')}
            onBlur={setTouched('subjectCode', true)}
            error={errors.subjectCode}
            options={subjectList}
            onChange={hanldeOnchange('subject')}
            disabled={isEmpty(serviceCodeVal)}
            value={() => handleValue(formValues.subject)}
          />
        </div>
        <div className="col-4">
          <ComboBox
            required={true}
            label={t('txt_utilities_pcf_analyzer_section')}
            textField="text"
            onChange={hanldeOnchange('section')}
            noResult={t('txt_no_results_found_without_dot')}
            name="sectionCode"
            disabled={isEmpty(subjectCodeVal)}
            value={() => handleValueCombobox(formValues.section)}
            onBlur={() => handleOnBlur()}
            error={errors.sectionCode}
          >
            {sectionList.map((item: MagicKeyValue) => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        </div>
      </div>
      {isButton === true && (
        <div className="text-right mt-24">
          <Button
            size="sm"
            onClick={handleAddPricing}
            variant="primary"
            disabled={isEmpty(formValues?.section?.code) || !isValid}
          >
            {t('txt_continue')}
          </Button>
        </div>
      )}
    </>
  );
};

export default AddMethod;
