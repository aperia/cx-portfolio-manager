import { getWorkflowSetupStepStatus } from 'app/helpers';
import { MethodDetail } from 'app/types';
import { FormSelectMethod } from '.';
import { checkValidItem } from '../helper';

const parseFormValues: WorkflowSetupStepFormDataFunc<FormSelectMethod> = (
  data,
  id
) => {
  const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);
  const config = (data?.data as any)?.configurations || {};

  if (!stepInfo || !config) return;

  const pricingMethods = (config?.pricingMethods as MethodDetail[]) || [];

  const selectedList = {} as Record<string, string[]>;
  pricingMethods?.forEach(element => {
    selectedList[element?.code] = element?.methodList
      ?.filter(i => i?.selected)
      .map(item => item?.rowId as string);
  });

  const isValid = checkValidItem(
    selectedList,
    pricingMethods.map(m => m.code)
  );

  return {
    ...getWorkflowSetupStepStatus(stepInfo),
    isValid,
    pricingMethods,
    selectedList
  };
};

export default parseFormValues;
