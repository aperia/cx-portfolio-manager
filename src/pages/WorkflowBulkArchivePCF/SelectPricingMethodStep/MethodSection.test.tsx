import { RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import * as helper from 'app/helpers/isDevice';
import { mockUseModalRegistryFnc, renderWithMockStore } from 'app/utils';
import React from 'react';
import MethodSection from './MethodSection';

const methods = [
  {
    code: 'A001',
    text: 'A001',
    transactions: [
      {
        rowId: '1',
        effectiveDate: '2022-12-14T00:00:00',
        name: 'methodName'
      },
      {
        rowId: '2',
        effectiveDate: '2022-12-14T00:00:00',
        name: 'methodName'
      },
      {
        rowId: '3',
        effectiveDate: '2022-12-14T00:00:00',
        name: 'methodName'
      },
      {
        rowId: '4',
        effectiveDate: '2022-12-14T00:00:00',
        name: 'methodName'
      }
    ] as any[],
    filteredTransactions: [
      {
        id: '1',
        rowId: '1',
        effectiveDate: '2022-12-14T00:00:00',
        name: 'methodName'
      },
      {
        id: '2',
        rowId: '2',
        effectiveDate: '2022-12-14T00:00:00',
        name: 'methodName'
      },
      {
        id: '3',
        rowId: '3',
        effectiveDate: '2022-12-14T00:00:00',
        name: 'methodName'
      },
      {
        id: '4',
        rowId: '4',
        effectiveDate: '2022-12-14T00:00:00',
        name: 'methodName'
      }
    ],
    methodList: [{ rowId: '1' }, { rowId: '2' }, { rowId: '3' }, { rowId: '4' }]
  },
  {
    code: 'A002',
    text: 'A002',
    transactions: [
      {
        rowId: '5',
        effectiveDate: '2022-12-14T00:00:00',
        name: 'methodName'
      },
      {},
      {},
      {},
      {},
      {},
      {},
      {},
      {},
      {},
      {},
      {}
    ] as any[],
    filteredTransactions: [
      {
        id: '1',
        rowId: '1',
        effectiveDate: '2022-12-14T00:00:00',
        name: 'methodName'
      },
      {
        id: '2',
        rowId: '2',
        effectiveDate: '2022-12-14T00:00:00',
        name: 'methodName'
      },
      {
        id: '3',
        rowId: '3',
        effectiveDate: '2022-12-14T00:00:00',
        name: 'methodName'
      },
      {
        id: '4',
        rowId: '4',
        effectiveDate: '2022-12-14T00:00:00',
        name: 'methodName'
      },
      {
        id: '5',
        rowId: '5',
        effectiveDate: '2022-12-14T00:00:00',
        name: 'methodName'
      },
      {
        id: '6',
        rowId: '6',
        effectiveDate: '2022-12-14T00:00:00',
        name: 'methodName'
      },
      {
        id: '7',
        rowId: '7',
        effectiveDate: '2022-12-14T00:00:00',
        name: 'methodName'
      },
      {
        id: '8',
        rowId: '8',
        effectiveDate: '2022-12-14T00:00:00',
        name: 'methodName'
      },
      {
        id: '9',
        rowId: '9',
        effectiveDate: '2022-12-14T00:00:00',
        name: 'methodName'
      },
      {
        id: '10',
        rowId: '10',
        effectiveDate: '2022-12-14T00:00:00',
        name: 'methodName'
      },
      {
        id: '11',
        rowId: '11',
        effectiveDate: '2022-12-14T00:00:00',
        name: 'methodName'
      }
    ],
    methodList: []
  },
  {
    code: 'A003',
    text: 'A003',
    transactions: [
      {
        rowId: '6',
        effectiveDate: '2022-12-14T00:00:00',
        name: 'methodName'
      }
    ] as any[]
  }
];
const mockUseModalRegistry = mockUseModalRegistryFnc();

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderComponent = async (
  props: any,
  workflowSetup?: any
): Promise<RenderResult> => {
  return await renderWithMockStore(<MethodSection {...props} />, {
    initialState: { workflowSetup }
  });
};
const mockHelper: any = helper;
describe('WorkflowBulkArchivePCF > SelectPricingStrategyStep > MethodSection', () => {
  beforeEach(() => {
    mockHelper.isDevice = false;

    mockUseModalRegistry.mockImplementation(() => [true, false]);
  });

  afterEach(() => {
    mockUseModalRegistry.mockClear();
  });

  const defaultWorkflowSetup = {
    workflowStrategies: { loading: true, error: false }
  };

  it('should render list', async () => {
    jest.useFakeTimers();
    const props = {
      formValues: {},
      stepId: '1',
      selectedStep: '2',
      setFormValues: jest.fn(),
      clearSearchValue: jest.fn(),
      onClickExpand: jest.fn(),
      setSelectedList: jest.fn(),
      setSortBy: jest.fn(),
      selectedList: [],
      sortBy: { id: 'method.name' },
      filterValue: 'txt_all',
      resetFilter: true,
      method: {
        ...methods[0],
        filteredTransactions: methods[0].transactions
      },

      methods: methods
    } as any;
    mockHelper.isDevice = true;

    const { getByText, getAllByRole, rerender } = await renderComponent(
      props,
      defaultWorkflowSetup
    );

    const checkboxAll = getAllByRole('checkbox')[0];
    userEvent.click(checkboxAll);
    jest.runAllTimers();
    userEvent.click(checkboxAll);
    jest.runAllTimers();
    userEvent.click(checkboxAll);
    jest.runAllTimers();

    userEvent.click(getByText('txt_workflow_bulk_archive_pcf_method_name'));

    rerender(
      <MethodSection
        {...props}
        selectedList={['1', '2']}
        isExpand={true}
        searchValue={'a'}
      />
    );
  });

  it('should render list without filteredTransactions', async () => {
    const props = {
      formValues: {},
      stepId: '1',
      selectedStep: '2',
      setFormValues: jest.fn(),
      clearSearchValue: jest.fn(),
      onClickExpand: jest.fn(),
      setSelectedList: jest.fn(),
      setSortBy: jest.fn(),
      selectedList: [],
      sortBy: { id: 'strategyName' },
      resetFilter: true,
      filterValue: 'txt_all',
      method: {
        ...methods[0],
        filteredTransactions: methods[1].filteredTransactions
      },

      methods: methods
    } as any;
    mockHelper.isDevice = true;

    const { getByText, getAllByRole, rerender } = await renderComponent(
      props,
      defaultWorkflowSetup
    );

    const checkboxAll = getAllByRole('checkbox')[0];

    userEvent.click(checkboxAll);
    userEvent.click(checkboxAll);

    userEvent.click(getByText('txt_workflow_bulk_archive_pcf_method_name'));

    rerender(
      <MethodSection {...props} selectedList={['1', '2']} isExpand={true} />
    );
  });

  it('click hide method', async () => {
    const props = {
      formValues: {
        methods: [{ ...methods[1] }] as any,
        pricingMethods: [{ code: '001', text: '001' }]
      },
      setFormValues: jest.fn(),
      clearSearchValue: jest.fn(),
      onClickExpand: jest.fn(),
      setSelectedList: jest.fn(),
      setSortBy: jest.fn(),
      selectedList: [],
      method: {
        ...methods[1],
        filteredTransactions: methods[1].transactions
      },

      methods: methods
    } as any;

    const { getByText } = await renderComponent(props, defaultWorkflowSetup);

    userEvent.click(
      getByText(
        'txt_workflow_bulk_archive_pcf_hide_this_service_subject_section'
      )
    );
  });
});
