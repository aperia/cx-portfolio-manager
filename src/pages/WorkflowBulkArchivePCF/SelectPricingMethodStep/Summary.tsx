import { classnames, isDevice } from 'app/helpers';
import { usePagination } from 'app/hooks/usePagination';
import { MethodDetail } from 'app/types';
import {
  Button,
  Grid,
  Icon,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty, isFunction } from 'lodash';
import Paging from 'pages/_commons/Utils/Paging';
import React, { useEffect, useMemo, useState } from 'react';
import { FormSelectMethod } from '.';
import { columns } from './constant';

const SelectPricingMethodSummary: React.FC<
  WorkflowSetupSummaryProps<FormSelectMethod>
> = ({ onEditStep, formValues, stepId, selectedStep }) => {
  const { t } = useTranslation();

  const [expandList, setExpandList] = useState<Record<string, boolean>>({});

  const handleEdit = () => {
    isFunction(onEditStep) && onEditStep();
  };

  const resetFilter = useMemo(
    () => stepId === selectedStep,
    [stepId, selectedStep]
  );

  const methodSelected = useMemo(() => {
    if (isEmpty(formValues?.pricingMethods)) return [];
    const { selectedList = {}, pricingMethods } = formValues;
    return pricingMethods
      ?.filter(m => selectedList[m.code]?.length)
      ?.map(m => ({
        ...m,
        methodList: m.methodList.filter(t =>
          selectedList[m.code].includes(t.id)
        )
      }));
  }, [formValues]);

  useEffect(() => {
    if (!resetFilter) return;
    const expands = methodSelected?.reduce((returnValue, m) => {
      return { ...returnValue, [m?.code]: true };
    }, {});
    resetFilter && setExpandList(expands);
  }, [methodSelected, resetFilter]);

  const handleClickExpand = (id: string, isExpand: boolean) => () => {
    setExpandList(list => ({ ...list, [id]: isExpand }));
  };

  return (
    <div className="position-relative">
      <div className="absolute-top-right mt-n26 mr-n8">
        <Button variant="outline-primary" size="sm" onClick={handleEdit}>
          {t('txt_edit')}
        </Button>
      </div>
      <div className="pt-16">
        {methodSelected?.map((method, idx) => {
          const isExpand = !!expandList[method.code];

          return (
            <div key={method.code} className={classnames(idx !== 0 && 'mt-24')}>
              <MethodSection
                method={method}
                isExpand={isExpand}
                resetFilter={resetFilter}
                onClickExpand={handleClickExpand(method.code, !isExpand)}
              />
            </div>
          );
        })}
      </div>
    </div>
  );
};

interface MethodSectionProps {
  isExpand: boolean;
  method: MethodDetail;
  resetFilter: boolean;
  onClickExpand: VoidFunction;
}
const MethodSection: React.FC<MethodSectionProps> = ({
  isExpand,
  method,
  resetFilter,
  onClickExpand
}) => {
  const { t } = useTranslation();

  const { code: methodName, methodList } = method;

  const {
    sort,
    total,
    currentPage,
    currentPageSize,
    gridData: dataView,
    onSortChange,
    onPageChange,
    onPageSizeChange,
    onResetToDefaultFilter
  } = usePagination(methodList, [], 'name');

  useEffect(() => {
    if (!resetFilter) return;

    onResetToDefaultFilter();
  }, [resetFilter, onResetToDefaultFilter]);

  return (
    <>
      <div className="d-flex align-items-center justify-content-between">
        <div className="d-flex align-items-center">
          <Tooltip
            element={isExpand ? t('txt_collapse') : t('txt_expand')}
            opened={isDevice ? false : undefined}
            variant="primary"
            placement="top"
            triggerClassName="d-flex ml-n2"
          >
            <Button size="sm" variant="icon-secondary" onClick={onClickExpand}>
              <Icon name={isExpand ? 'minus' : 'plus'} size="4x" />
            </Button>
          </Tooltip>

          <strong className="ml-8">{methodName}</strong>
        </div>
      </div>
      <div className={classnames(!isExpand && 'd-none')}>
        <div className="mt-16">
          <Grid
            dataItemKey="rowId"
            data={dataView}
            columns={columns(t, false)}
            sortBy={[sort]}
            onSortChange={onSortChange}
          />
          {total > 10 && (
            <div className="mt-16">
              <Paging
                page={currentPage}
                pageSize={currentPageSize}
                totalItem={total}
                onChangePage={onPageChange}
                onChangePageSize={onPageSizeChange}
              />
            </div>
          )}
        </div>
      </div>
    </>
  );
};

export default SelectPricingMethodSummary;
