import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import 'app/utils/_mockComponent/mockModalRegistry';
import React from 'react';
import AddMethod from './AddMethod';

jest.mock('pages/_commons/redux/WorkflowSetup', () => ({
  actionsWorkflowSetup: { getWorkflowMetadata: jest.fn() },
  useSelectElementsBulkArchivePCF: () => ({
    methods: [
      {
        value: '',
        description: 'None',
        code: '',
        text: 'None'
      },
      {
        value: 'D',
        description:
          'Deny the item if it exceeds the amount in the LIMIT field.',
        code: 'D'
      },
      {
        value: 'Q',
        description:
          'Assign the item to the pending queue if it exceeds the amount in the LIMIT field.',
        code: 'Q',
        text: 'Q - Assign the item to the pending queue if it exceeds the amount in the LIMIT field.'
      }
    ]
  }),
  useSelectServicesData: () => [
    {
      name: 'CP',
      description: 'Cardholder Pricing',
      subjects: [
        {
          name: 'IC',
          description: 'Interest Charges',
          sections: [
            {
              name: 'IC',
              description: 'Interest Charges'
            },
            {
              name: 'IB',
              description: 'Incentive Pricing Break Points'
            },
            {
              name: 'ID',
              description: 'Interest Defaults'
            },
            {
              name: 'IF',
              description: 'Interest and Fees'
            },
            {
              name: 'II',
              description: 'Interest on Interest'
            },
            {
              name: 'IM',
              description: 'Interest Methods'
            },
            {
              name: 'IP',
              description: 'Incentive Pricing'
            },
            {
              name: 'IR',
              description: 'Index Rate'
            },
            {
              name: 'IV',
              description: 'Incentive Pricing Variable Interest'
            }
          ]
        },
        {
          name: 'IO',
          description: 'Income Options',
          sections: [
            {
              name: 'AC',
              description: 'Annual Charges'
            },
            {
              name: 'CI',
              description: 'Cash Advance Item Charges'
            },
            {
              name: 'CL',
              description: 'Credit Life'
            },
            {
              name: 'MC',
              description: 'Miscellaneous Charges'
            },
            {
              name: 'MI',
              description: 'Merchandise Item Charges'
            },
            {
              name: 'SC',
              description: 'Statement Charges'
            }
          ]
        },
        {
          name: 'OC',
          description: 'Other Controls',
          sections: [
            {
              name: 'AP',
              description: 'Adjustment Pricing'
            },
            {
              name: 'BD',
              description: 'Backdating'
            },
            {
              name: 'CL',
              description: 'Credit Line Management'
            },
            {
              name: 'CP',
              description: 'Current Pricing Notification'
            },
            {
              name: 'DR',
              description: 'Debit Ratification'
            }
          ]
        }
      ]
    },
    {
      name: 'AM',
      description: 'Account Marketing',
      subjects: [
        {
          name: 'AA',
          description: 'Alternate Accounts',
          sections: [
            {
              name: 'HE',
              description: 'Home Equity'
            }
          ]
        },
        {
          name: 'FD',
          description: 'Group Delimiters',
          sections: [
            {
              name: 'DC',
              description: 'Decision Controls'
            },
            {
              name: 'ER',
              description: 'Exception Report'
            },
            {
              name: 'KC',
              description: 'Key Controls'
            }
          ]
        },
        {
          name: 'PS',
          description: 'Payment Selection',
          sections: [
            {
              name: 'PA',
              description: 'Pay Ahead'
            },
            {
              name: 'PR',
              description: 'Payment Reversal'
            }
          ]
        }
      ]
    }
  ]
}));

const factory = (isExistMethod?: boolean, hasMethod?: boolean) => {
  const mockChangeMethod = jest.fn();
  const handleAddPricing = jest.fn();
  const { getByText } = render(
    <AddMethod
      handleChangeForm={mockChangeMethod}
      isButton
      onChangeMethod={handleAddPricing}
    />
  );

  const onSelectMethod = () => {
    const service = getByText('txt_utilities_pcf_analyzer_service');
    userEvent.click(service);
    userEvent.click(getByText('CP, Cardholder Pricing'));

    const subject = getByText('txt_utilities_pcf_analyzer_subject');
    userEvent.click(subject);
    userEvent.click(getByText('IC, Interest Charges'));

    const section = getByText('txt_utilities_pcf_analyzer_section');
    userEvent.click(section);
    userEvent.click(getByText('IB, Incentive Pricing Break Points'));
  };

  const clickContinue = () => {
    userEvent.click(getByText('txt_continue'));
  };

  return {
    onSelectMethod,
    clickContinue,
    mockChangeMethod,
    handleAddPricing
  };
};

describe('test AddMethod component', () => {
  it('should be render', () => {
    jest.useFakeTimers();
    const { onSelectMethod, mockChangeMethod } = factory();

    jest.runAllTicks();
    onSelectMethod();
    expect(mockChangeMethod).toBeCalled();

    jest.useRealTimers();
  });
  it('should be render with existed method', () => {
    const {
      onSelectMethod,
      clickContinue,
      mockChangeMethod,
      handleAddPricing
    } = factory(true, true);
    onSelectMethod();
    expect(mockChangeMethod).toBeCalled();

    clickContinue();
    expect(handleAddPricing).toBeCalled();
  });
});
