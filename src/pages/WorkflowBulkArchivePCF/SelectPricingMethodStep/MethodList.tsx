import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import { classnames, matchSearchValue } from 'app/helpers';
import { MethodDetail, MethodTransactions } from 'app/types';
import {
  DropdownBaseChangeEvent,
  DropdownList,
  SortType,
  useTranslation
} from 'app/_libraries/_dls';
import { differenceWith, isEmpty } from 'app/_libraries/_dls/lodash';
import ClearAndResetButton from 'pages/_commons/Utils/ClearAndResetButton';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { FormSelectMethod } from '.';
import { checkValidItem, matchFilterMethod } from '../helper';
import { FilterOption } from '../type';
import { methodFilterOptions } from './constant';
import MethodSection from './MethodSection';

export interface FilterMethodDetail extends MethodDetail {
  filteredTransactions: MethodTransactions[];
}
export interface MethodListProps {
  prm: MethodDetail[];
  keepRef: any;
  isValid?: boolean;
}

const MethodList: React.FC<
  WorkflowSetupProps<FormSelectMethod> & MethodListProps
> = props => {
  const { t } = useTranslation();
  const { prm, stepId, selectedStep, keepRef, isValid, formValues } = props;
  const prevRef = useRef({ prm });
  const searchRef = useRef<any>(null);

  const [searchValue, setSearchValue] = useState<string>('');
  const [filterValue, setFilterValue] = useState<string>(t('txt_all'));
  const [shouldResetFilter, setShouldResetFilter] = useState(false);

  const [expandList, setExpandList] = useState<Record<string, boolean>>({});

  const [sortBy, setSortBy] = useState<Record<string, SortType>>({});

  const methodOptions = methodFilterOptions(t);

  const filteredMethods = useMemo(
    () =>
      prm
        .map(p => {
          if (isEmpty(p.methodList)) return { ...p, filteredTransactions: [] };

          return {
            ...p,
            filteredTransactions: p.methodList.filter(
              w =>
                matchSearchValue([w?.name], searchValue) &&
                matchFilterMethod({
                  filterValue,
                  selectedList: formValues.selectedList,
                  method: w,
                  t
                })
            )
          } as FilterMethodDetail;
        })
        .filter(
          f =>
            f.filteredTransactions.length > 0 ||
            (!searchValue && filterValue === t('txt_all'))
        ),
    [filterValue, formValues?.selectedList, prm, searchValue, t]
  );

  const handleExpandByDefault = useCallback(() => {
    const list: Record<string, boolean> = {};
    prm.forEach(method => {
      list[method.code] = true;
    });
    setExpandList(list);
    setShouldResetFilter(true);
  }, [prm]);

  const handleClearFilter = useCallback(() => {
    searchRef?.current?.clear();
    setSearchValue('');
    setFilterValue(t('txt_all'));
    handleExpandByDefault();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [handleExpandByDefault]);

  const handleSearch = (val: string) => {
    setSearchValue(val);
    handleExpandByDefault();
  };

  const handleFilter = (e: DropdownBaseChangeEvent) => {
    setFilterValue(e.target?.value);
  };

  useEffect(() => {
    if (stepId !== selectedStep) {
      setShouldResetFilter(true);
      handleClearFilter();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [stepId, selectedStep, handleClearFilter]);

  useEffect(() => {
    if (shouldResetFilter) {
      setShouldResetFilter(false);
    }
  }, [shouldResetFilter]);

  useEffect(() => {
    const checkValid = checkValidItem(
      formValues?.selectedList,
      prm.map(s => s.code)
    );
    if (isValid === checkValid) return;
    keepRef.current.setFormValues({ isValid: checkValid });
  }, [isValid, prm, keepRef, formValues?.selectedList]);

  useEffect(() => {
    const _prevRef = prevRef.current;
    const prevMethod = _prevRef.prm;
    // reset filter when add new method
    if (prm.length > prevMethod.length) {
      handleClearFilter();
    }
  }, [handleClearFilter, prm.length]);

  useEffect(() => {
    const _prevRef = prevRef.current;
    const prevMethod = _prevRef.prm;

    const prevLength = prevMethod.length;
    const currLength = prm.length;

    const isFirstAdd = currLength === prevLength && currLength === 1;

    const diff = isFirstAdd
      ? prm[0].code
      : differenceWith(prm, prevMethod, (o1, o2) => o1.code === o2.code)?.[0]
          ?.code;

    if (!!diff) {
      setExpandList({ ...expandList, [diff]: true });
      _prevRef.prm = prm;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [prm]);

  const handleClickExpand = (id: string, isExpand: boolean) => () => {
    setExpandList(list => ({ ...list, [id]: isExpand }));
  };

  const handleClickSort = (id: string, sort: SortType) => {
    setSortBy(list => ({ ...list, [id]: sort }));
  };

  const handleSetSelectedList = (id: string, _list: string[]) => {
    //setSelectedList(list => ({ ...list, [id]: _list }));
    keepRef.current.setFormValues({
      selectedList: { ...formValues?.selectedList, [id]: _list }
    });
  };

  return (
    <>
      <div className="d-flex align-items-center justify-content-between mb-16">
        <h5>{t('txt_workflow_bulk_archive_pcf_method_list')}</h5>
        <SimpleSearch
          ref={searchRef}
          placeholder={t('txt_workflow_bulk_archive_pcf_search_by_method')}
          onSearch={handleSearch}
        />
      </div>

      <div className="d-flex align-items-center">
        <strong className="fs-14 color-grey mr-4">{t('txt_filter')}:</strong>
        <DropdownList
          name="status"
          value={filterValue}
          textField="description"
          onChange={handleFilter}
          variant="no-border"
        >
          {methodOptions.map((item: FilterOption) => (
            <DropdownList.Item
              key={item.value}
              label={item.text}
              value={item.text}
            />
          ))}
        </DropdownList>
      </div>

      {searchValue && !isEmpty(filteredMethods) && (
        <div className="d-flex justify-content-end mb-16 mr-n8">
          <ClearAndResetButton
            small
            onClearAndReset={() => handleClearFilter()}
          />
        </div>
      )}
      {isEmpty(filteredMethods) && (
        <div className="d-flex flex-column justify-content-center mt-40 mb-32">
          <NoDataFound
            id="newMethod_notfound"
            hasSearch={!!searchValue}
            hasFilter={filterValue !== t('txt_all')}
            title={t(
              'txt_workflow_bulk_archive_pcf_method_list_no_results_found'
            )}
            linkTitle={t('txt_clear_and_reset')}
            onLinkClicked={() => handleClearFilter()}
          />
        </div>
      )}
      {!isEmpty(filteredMethods) && (
        <div className="mt-16">
          {filteredMethods.map((method, idx) => {
            const isExpand = !!expandList[method.code];

            return (
              <div
                key={method.code}
                className={classnames(idx !== 0 && 'mt-24 pt-24 border-top')}
              >
                <MethodSection
                  {...props}
                  method={method}
                  isExpand={isExpand}
                  resetFilter={shouldResetFilter}
                  filterValue={filterValue}
                  searchValue={searchValue}
                  selectedList={formValues.selectedList[method.code] || []}
                  sortBy={sortBy[method.code]}
                  setSortBy={handleClickSort}
                  onClickExpand={handleClickExpand(method.code, !isExpand)}
                  setSelectedList={handleSetSelectedList}
                />
              </div>
            );
          })}
        </div>
      )}
    </>
  );
};

export default MethodList;
