import {
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { classnames } from 'app/helpers';
import { useUnsavedChangeRegistry } from 'app/hooks';
import { MethodDetail } from 'app/types';
import { Button, InlineMessage, useTranslation } from 'app/_libraries/_dls';
import { isEqual } from 'app/_libraries/_dls/lodash';
import { orderBy } from 'lodash';
import {
  actionsWorkflowSetup,
  useMethodTransactionsStatus
} from 'pages/_commons/redux/WorkflowSetup';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, { useEffect, useMemo, useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import { ParameterCode } from '../type';
import AddMethod from './AddMethod';
import AddMethodModal from './AddMethodModal';
import { pricingMethodTestId } from './constant';
import InstructionModal from './InstructionModal';
import MethodList from './MethodList';
import Summary from './Summary';
import parseFormValues from './_parseFormValues';

export interface FormSelectMethod {
  isValid?: boolean;
  selectedList: Record<string, string[]>;
  pricingMethods: MethodDetail[];
}

const SelectPricingMethodStep: React.FC<WorkflowSetupProps<FormSelectMethod>> =
  props => {
    const { setFormValues, formValues, savedAt, isStuck } = props;
    const [initialValues, setInitialValues] = useState(formValues);
    const { t } = useTranslation();
    const dispatch = useDispatch();

    const keepRef = useRef({ setFormValues });
    keepRef.current.setFormValues = setFormValues;

    const { loading } = useMethodTransactionsStatus();
    const [showAddMethodModal, setShowAddMethodModal] = useState(false);
    const [showInstructionModal, setShowInstructionModal] = useState(false);

    const existMethodIds = useMemo(
      () => formValues?.pricingMethods.map(m => m.code),
      [formValues?.pricingMethods]
    );

    const hasMethod = useMemo(
      () => existMethodIds.length > 0,
      [existMethodIds.length]
    );

    useEffect(() => {
      dispatch(
        actionsWorkflowSetup.getWorkflowMetadata([
          ParameterCode.MethodName,
          ParameterCode.StrategyName
        ])
      );
      dispatch(actionsWorkflowSetup.getServiceSubjectSectionOptions(''));
    }, [dispatch]);

    useEffect(() => {
      setInitialValues(formValues);
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [savedAt]);

    const formChanged = useMemo(
      () => !isEqual(formValues.pricingMethods, initialValues.pricingMethods),
      [formValues.pricingMethods, initialValues.pricingMethods]
    );

    useUnsavedChangeRegistry(
      {
        ...unsavedExistedWorkflowSetupDataProps,
        formName:
          UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP_BULK_ARCHIVE_PCF_SELECT_PRICING_METHOD__EXISTED_WORKFLOW,
        priority: 1
      },
      [formChanged]
    );

    const handleSelectMethod = async (method: MagicKeyValue) => {
      const transactionsResp = (await Promise.resolve(
        dispatch(
          actionsWorkflowSetup.getDesignLoanOffersWorkflowMethods({
            serviceSubjectSection: method.code
          })
        )
      )) as any;
      const isSuccess =
        actionsWorkflowSetup.getDesignLoanOffersWorkflowMethods.fulfilled.match(
          transactionsResp
        );

      if (isSuccess) {
        const _method: MethodDetail = {
          code: method?.code,
          value: method?.value,
          methodList: transactionsResp?.payload?.methods?.map(
            (t: any, idx: number) => {
              const rowId = `${method.code}_${idx}_${Date.now()}`;
              const methodI = {
                ...t,
                rowId: rowId,
                id: rowId,
                'method.name': t?.name,
                'effective.date': t?.effectiveDate,
                name: t?.name
              };
              return methodI;
            }
          )
        };
        const _methods = [_method, ...formValues.pricingMethods];

        setFormValues({ pricingMethods: orderBy(_methods, ['code']) });
      }
    };

    const handleOpenAddMethodModal = () => {
      setShowAddMethodModal(true);
    };

    const handleCloseAddMethodModal = () => {
      setShowAddMethodModal(false);
    };

    const instructionView = useMemo(
      () => (
        <>
          <div className="mt-8 pb-24 border-bottom color-grey">
            <p>
              {t('txt_workflow_bulk_archive_pcf_method_instruction')}{' '}
              <span
                className="link text-decoration-none px-4"
                onClick={() => setShowInstructionModal(true)}
                data-testid={pricingMethodTestId.viewInstructionModal}
              >
                {t('txt_workflow_bulk_archive_pcf_view_instruction')}
              </span>
            </p>
          </div>
          {isStuck && (
            <InlineMessage className="mb-0 mt-24" variant="danger" withIcon>
              {t('txt_step_stuck_move_forward_message')}
            </InlineMessage>
          )}
        </>
      ),
      // eslint-disable-next-line react-hooks/exhaustive-deps
      [isStuck]
    );

    return (
      <>
        {!hasMethod && (
          <>
            {instructionView}
            <div className={classnames('mt-24', loading && 'loading')}>
              <h5>
                {t(
                  'txt_workflow_bulk_archive_pcf_select_service_subject_section'
                )}
              </h5>
              <AddMethod onChangeMethod={handleSelectMethod} isButton />
            </div>
          </>
        )}
        {hasMethod && (
          <>
            <div
              className={classnames('position-relative', loading && 'loading')}
            >
              <div className="absolute-top-right mt-n32 mr-n8">
                <Button
                  variant="outline-primary"
                  size="sm"
                  onClick={handleOpenAddMethodModal}
                >
                  {t(
                    'txt_workflow_bulk_archive_pcf_add_service_subject_section'
                  )}
                </Button>
              </div>
              {instructionView}
            </div>
            <div className="mt-24">
              <MethodList
                prm={formValues?.pricingMethods}
                isValid={formValues.isValid}
                keepRef={keepRef}
                {...props}
              />

              {showAddMethodModal && (
                <AddMethodModal
                  existMethodIds={existMethodIds}
                  onClose={handleCloseAddMethodModal}
                  onAdd={handleSelectMethod}
                />
              )}
            </div>
          </>
        )}
        {showInstructionModal && (
          <InstructionModal
            opened={showInstructionModal}
            onClose={() => setShowInstructionModal(false)}
          />
        )}
      </>
    );
  };

const ExtraStaticSelectPricingMethodSteptep =
  SelectPricingMethodStep as WorkflowSetupStaticProp<FormSelectMethod>;

ExtraStaticSelectPricingMethodSteptep.summaryComponent = Summary;
ExtraStaticSelectPricingMethodSteptep.defaultValues = {
  isValid: false,
  selectedList: {},
  pricingMethods: []
};
ExtraStaticSelectPricingMethodSteptep.parseFormValues = parseFormValues;

export default ExtraStaticSelectPricingMethodSteptep;
