import ModalRegistry from 'app/components/ModalRegistry';
import {
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  TransDLS,
  useTranslation
} from 'app/_libraries/_dls';
import React, { useMemo } from 'react';

export interface InstructionModalProps {
  onClose?: VoidFunction;
  opened: boolean;
}

const InstructionModal: React.FC<InstructionModalProps> = ({
  onClose,
  opened
}) => {
  const { t } = useTranslation();

  const modalBody = useMemo(() => {
    return (
      <>
        <p className="fw-600">
          {t('txt_workflow_bulk_archive_pcf_method_instruction_title_1')}
        </p>
        <ul className="list-dot">
          <li>
            {t('txt_workflow_bulk_archive_pcf_method_instruction_desc_1_1')}
          </li>
          <li>
            {t('txt_workflow_bulk_archive_pcf_method_instruction_desc_1_2')}
          </li>
          <li>
            {t('txt_workflow_bulk_archive_pcf_method_instruction_desc_1_3')}
          </li>
          <li>
            {t('txt_workflow_bulk_archive_pcf_method_instruction_desc_1_4')}
          </li>
          <li>
            {t('txt_workflow_bulk_archive_pcf_method_instruction_desc_1_5')}
          </li>
        </ul>

        <hr className="mb-24 mt-24 border-dashed" />

        <p className="mt-16 fw-600">
          {t('txt_workflow_bulk_archive_pcf_method_instruction_title_2')}
        </p>
        <ul className="list-dot">
          <li>
            {t('txt_workflow_bulk_archive_pcf_method_instruction_desc_2_1')}
            <ul className="list-dot">
              <li>
                {t(
                  'txt_workflow_bulk_archive_pcf_method_instruction_sub_desc_2_1_1'
                )}
              </li>
              <li>
                <TransDLS keyTranslation="txt_workflow_bulk_archive_pcf_method_instruction_sub_desc_2_1_2">
                  <sup />
                </TransDLS>
              </li>
              <li>
                {t(
                  'txt_workflow_bulk_archive_pcf_method_instruction_sub_desc_2_1_3'
                )}
              </li>
              <li>
                {t(
                  'txt_workflow_bulk_archive_pcf_method_instruction_sub_desc_2_1_4'
                )}
              </li>
              <li>
                <TransDLS keyTranslation="txt_workflow_bulk_archive_pcf_method_instruction_sub_desc_2_1_5">
                  <sup />
                </TransDLS>
              </li>
            </ul>
          </li>
          <li>
            {t('txt_workflow_bulk_archive_pcf_method_instruction_desc_2_2')}
            <ul className="list-dot">
              <li>
                <TransDLS keyTranslation="txt_workflow_bulk_archive_pcf_method_instruction_sub_desc_2_2_1">
                  <sup />
                </TransDLS>
              </li>
              <li>
                <TransDLS keyTranslation="txt_workflow_bulk_archive_pcf_method_instruction_sub_desc_2_2_2">
                  <sup />
                </TransDLS>
              </li>
            </ul>
          </li>
          <li>
            <TransDLS keyTranslation="txt_workflow_bulk_archive_pcf_method_instruction_desc_2_3">
              <sup />
            </TransDLS>
          </li>
          <li>
            {t('txt_workflow_bulk_archive_pcf_method_instruction_desc_2_4')}
          </li>
          <li>
            {t('txt_workflow_bulk_archive_pcf_method_instruction_desc_2_5')}
          </li>
        </ul>
      </>
    );
  }, [t]);

  return (
    <ModalRegistry
      id="InstructionModal"
      show={opened}
      onAutoClosedAll={onClose}
      md
      animationComponent="picture"
    >
      <ModalHeader border closeButton onHide={onClose}>
        <ModalTitle>
          {t('txt_workflow_bulk_archive_pcf_instruction_detail')}
        </ModalTitle>
      </ModalHeader>
      <ModalBody>{modalBody}</ModalBody>
      <ModalFooter cancelButtonText={t('txt_close')} onCancel={onClose} />
    </ModalRegistry>
  );
};

export default InstructionModal;
