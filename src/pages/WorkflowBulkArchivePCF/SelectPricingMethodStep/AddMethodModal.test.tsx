import { fireEvent, render } from '@testing-library/react';
import 'app/utils/_mockComponent/mockModalRegistry';
import React from 'react';
import AddMethodModal from './AddMethodModal';

jest.mock('./AddMethod', () => ({
  __esModule: true,
  default: ({
    handleChangeForm
  }: {
    handleChangeForm: (ref: Record<string, RefData>) => void;
  }) => (
    <div data-testid="AddMethod">
      AddMethod
      <button
        data-testid="addMethod"
        onClick={() =>
          handleChangeForm({
            service: { code: 'LA', text: 'text2' },
            subject: { code: 'LC', text: 'text2' },
            section: { code: 'CC', text: 'text2' }
          })
        }
      >
        txt_add_method
      </button>
    </div>
  )
}));

const factory = (isExistedMethod?: boolean) => {
  const { getByText, getByTestId, queryByText } = render(
    <AddMethodModal
      existMethodIds={isExistedMethod ? ['LA LC CC'] : []}
      onClose={jest.fn()}
      onAdd={jest.fn()}
    />
  );

  return {
    getTitle: () =>
      getByText('txt_workflow_bulk_archive_pcf_add_service_subject_section'),

    addMethod: () => {
      jest.useFakeTimers();
      fireEvent.click(getByTestId('addMethod'));
      jest.runAllTimers();
      fireEvent.click(getByText('txt_continue'));
    },
    getInlineMsg: () =>
      queryByText(
        'txt_workflow_bulk_archive_pcf_validation_exist_select_method'
      )
  };
};

describe('test AddMethodModal component', () => {
  it('should be render', () => {
    const { getTitle } = factory();
    expect(getTitle()).toBeInTheDocument();
  });
  it('should add method with existed id', () => {
    const { addMethod, getInlineMsg } = factory(true);
    addMethod();
    expect(getInlineMsg()).toBeInTheDocument();
  });
  it('should add method with existed id', () => {
    const { addMethod, getInlineMsg } = factory(false);
    addMethod();
    expect(getInlineMsg()).not.toBeInTheDocument();
  });
});
