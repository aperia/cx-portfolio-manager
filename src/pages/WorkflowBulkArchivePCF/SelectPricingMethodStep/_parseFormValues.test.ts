import parseFormValues from './_parseFormValues';

describe('pages > WorkFlowBulkArchivePCF > SelectPricingMethodStep > parseFormValues', () => {
  const mockDispatch = jest.fn();

  it('Should return undefined', () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '2'
            }
          ] as any
        }
      } as WorkflowSetupInstance,
      id: '1'
    };

    const response = parseFormValues(props.data, props.id, mockDispatch);
    expect(response).toEqual(undefined);
  });

  it('Should return data', () => {
    const props = {
      data: {
        data: {
          configurations: {
            pricingMethods: [{ methodList: [{ selected: true, rowId: 'id' }] }]
          },
          workflowSetupData: [
            {
              id: '1'
            }
          ]
        }
      } as unknown as WorkflowSetupInstance,
      id: '1'
    };

    const response = parseFormValues(props.data, props.id, mockDispatch);
    expect(response).toEqual(
      expect.objectContaining({
        isPass: false,
        isSelected: false
      })
    );
  });
});
