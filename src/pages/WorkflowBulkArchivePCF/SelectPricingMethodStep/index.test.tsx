import { RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import {
  mockThunkAction,
  mockUseModalRegistryFnc,
  renderWithMockStore
} from 'app/utils';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowBulkArchivePCF';
import React from 'react';
import { ParameterCode } from '../type';
import { pricingMethodTestId } from './constant';
import SelectPricingMethodStep, { FormSelectMethod } from './index';

const mockUseModalRegistry = mockUseModalRegistryFnc();

HTMLCanvasElement.prototype.getContext = jest.fn();

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

jest.mock('./MethodList', () => ({
  __esModule: true,
  default: () => <div data-testid="MethodList">MethodList</div>
}));

jest.mock('./AddMethod', () => ({
  __esModule: true,
  default: ({
    onChangeMethod
  }: {
    onChangeMethod: (ref: { code: string; text: string }) => void;
  }) => (
    <div data-testid="AddMethod">
      AddMethod
      <button
        onClick={() => onChangeMethod({ code: 'method2', text: 'text2' })}
      >
        txt_add_method
      </button>
    </div>
  )
}));

jest.mock('./AddMethodModal', () => ({
  __esModule: true,
  default: ({
    onClose,
    onAdd
  }: {
    onClose: () => void;
    onAdd: (ref: { code: string; text: string }) => void;
  }) => (
    <div data-testid="AddMethodModal">
      <button
        onClick={() => {
          onClose();
        }}
      >
        txt_cancel
      </button>
      <button
        onClick={() => {
          onAdd({ code: 'method2', text: 'text2' });
        }}
      >
        txt_add_method
      </button>
      AddMethodModal
    </div>
  )
}));

jest.mock('./InstructionModal', () => ({
  __esModule: true,
  default: ({ onClose }: { onClose: () => void }) => (
    <div data-testid="InstructionModal">
      InstructionModal
      <button
        onClick={() => {
          onClose();
        }}
      >
        txt_close
      </button>
    </div>
  )
}));

jest.mock('./Summary', () => ({
  __esModule: true,
  default: () => <div data-testid="Summary">Summary</div>
}));

const MockSelectElementMetadataForMethodList = jest.spyOn(
  WorkflowSetup,
  'useMethodTransactionsStatus'
);

const MockSelectElementMetadataForBulkArchivePCF = jest.spyOn(
  WorkflowSetup,
  'useSelectElementsBulkArchivePCF'
);

const renderComponent = async (
  props: any,
  workflowSetup?: any
): Promise<RenderResult> => {
  return await renderWithMockStore(<SelectPricingMethodStep {...props} />, {
    initialState: { workflowSetup }
  });
};
const mockWorkflowSetup = mockThunkAction(actionsWorkflowSetup);

export const mockElements = [
  {
    name: ParameterCode.MethodName,
    options: [{ value: 'MethodName' }]
  },
  {
    name: ParameterCode.StrategyName,
    options: [{ value: 'StrategyName' }]
  }
];

describe('WorkflowBulkArchivePCF > SelectPricingMethodStep > Index', () => {
  beforeEach(() => {
    mockUseModalRegistry.mockReturnValue([true, false]);
    mockWorkflowSetup('getDesignLoanOffersWorkflowMethods', {
      match: false
    });
    MockSelectElementMetadataForMethodList.mockReturnValue({
      loading: true,
      error: false
    });
    MockSelectElementMetadataForBulkArchivePCF.mockReturnValue({
      strategies: [],
      methods: [
        { code: 'method1', text: 'text1' },
        { code: 'method2', text: 'text2' }
      ],
      tableAreas: [],
      textAreas: []
    });
  });

  const defaultWorkflowSetup = {
    elementMetadata: { elements: mockElements },
    workflowMethods: { loading: true, error: false }
  };

  it('should render with empty method', async () => {
    const spy = mockWorkflowSetup('getDesignLoanOffersWorkflowMethods', {
      match: true,
      payload: {
        payload: {
          methods: [
            { code: 'method1', text: 'text1' },
            { code: 'method2', text: 'text2' }
          ]
        }
      }
    });
    MockSelectElementMetadataForMethodList.mockReturnValueOnce({
      loading: true
    });
    const setFormValues = jest.fn() as any;
    const props = {
      formValues: {
        selectedList: {},
        methods: [],
        pricingMethods: []
      },
      setFormValues,
      isStuck: true
    } as unknown as WorkflowSetupProps<FormSelectMethod>;

    const { getByText, getByTestId } = await renderComponent(
      props,
      defaultWorkflowSetup
    );

    userEvent.click(getByTestId(pricingMethodTestId.viewInstructionModal));
    userEvent.click(
      getByText('txt_workflow_bulk_archive_pcf_select_service_subject_section')
    );
    userEvent.click(getByText('txt_add_method'));

    expect(
      getByText('txt_workflow_bulk_archive_pcf_select_service_subject_section')
    ).toBeInTheDocument();
    spy.mockClear();
    spy.mockRestore();
  });

  it('should add method success', async () => {
    const spy = mockWorkflowSetup('getDesignLoanOffersWorkflowMethods', {
      match: true,
      payload: {
        payload: {
          methods: [
            { code: 'method1', text: 'text1' },
            { code: 'method2', text: 'text2' }
          ]
        }
      }
    });
    const setFormValues = jest.fn() as any;
    const props = {
      formValues: {
        selectedList: {},
        pricingMethods: [{ code: '001', text: '001' }],
        methods: [{ code: 'method1', text: 'text1', transactions: [] as any[] }]
      },
      setFormValues
    } as unknown as WorkflowSetupProps<FormSelectMethod>;

    const { getByText, getByTestId } = await renderComponent(
      props,
      defaultWorkflowSetup
    );
    expect(
      getByText('txt_workflow_bulk_archive_pcf_add_service_subject_section')
    ).toBeInTheDocument();

    userEvent.click(getByTestId(pricingMethodTestId.viewInstructionModal));
    userEvent.click(getByText('txt_close'));
    userEvent.click(
      getByText('txt_workflow_bulk_archive_pcf_add_service_subject_section')
    );
    userEvent.click(getByText('txt_add_method'));
    spy.mockClear();
    spy.mockRestore();
  });

  it('should add method fail', async () => {
    const spy = mockWorkflowSetup('getDesignLoanOffersWorkflowMethods', {
      match: false
    });
    const setFormValues = jest.fn() as any;
    const props = {
      formValues: {
        pricingMethods: [{ code: '001', text: '001' }],
        methods: [{ code: '001', text: '001', transactions: [] as any[] }],
        selectedList: {}
      },
      setFormValues
    } as unknown as WorkflowSetupProps<FormSelectMethod>;

    const { getByText, getByTestId } = await renderComponent(
      props,
      defaultWorkflowSetup
    );
    expect(
      getByText('txt_workflow_bulk_archive_pcf_add_service_subject_section')
    ).toBeInTheDocument();
    userEvent.click(getByTestId(pricingMethodTestId.viewInstructionModal));

    userEvent.click(
      getByText('txt_workflow_bulk_archive_pcf_add_service_subject_section')
    );
    userEvent.click(getByText('txt_cancel'));

    userEvent.click(
      getByText('txt_workflow_bulk_archive_pcf_add_service_subject_section')
    );
    userEvent.click(getByText('txt_add_method'));
    spy.mockClear();
    spy.mockRestore();
  });
});
