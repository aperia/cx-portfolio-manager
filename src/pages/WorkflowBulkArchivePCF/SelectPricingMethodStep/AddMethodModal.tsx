import ModalRegistry from 'app/components/ModalRegistry';
import {
  InlineMessage,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty, isFunction } from 'lodash';
import React, { useState } from 'react';
import AddMethod from './AddMethod';

export interface AddMethodModalProps {
  existMethodIds: string[];
  onClose?: VoidFunction;
  onAdd?: (method: RefData) => void;
}

const defaultOption = { code: '', text: '' };

const initformValues = {
  service: defaultOption,
  subject: defaultOption,
  section: defaultOption
};

const AddMethodModal: React.FC<AddMethodModalProps> = ({
  existMethodIds,
  onClose,
  onAdd
}) => {
  const { t } = useTranslation();

  const [method, setMethod] = useState<RefData>();
  const [isExistMethod, setIsExistMethod] = useState(false);
  const [formValues, setFormValues] = useState(initformValues);

  const handleAdd = () => {
    const _isExistMethod =
      !!method?.code && existMethodIds.includes(method.code);
    if (_isExistMethod) {
      setIsExistMethod(_isExistMethod);
      return;
    }

    isFunction(onAdd) && onAdd(method!);
    isFunction(onClose) && onClose();
  };

  const handleChangeForm = (data: {
    service: {
      code: string;
      text: string;
    };
    subject: {
      code: string;
      text: string;
    };
    section: {
      code: string;
      text: string;
    };
  }) => {
    setFormValues(data);
    setMethod({
      code: `${data?.service?.code} ${data?.subject?.code} ${data?.section?.code}`
    });
  };

  return (
    <ModalRegistry id="AddMethodModal" show onAutoClosedAll={onClose} md>
      <ModalHeader border closeButton onHide={onClose}>
        <ModalTitle>
          {t('txt_workflow_bulk_archive_pcf_add_service_subject_section')}
        </ModalTitle>
      </ModalHeader>
      <ModalBody>
        {isExistMethod && (
          <InlineMessage withIcon variant="danger">
            {t('txt_workflow_bulk_archive_pcf_validation_exist_select_method')}
          </InlineMessage>
        )}
        <AddMethod handleChangeForm={handleChangeForm} />
      </ModalBody>
      <ModalFooter
        cancelButtonText={t('txt_cancel')}
        okButtonText={t('txt_continue')}
        disabledOk={isEmpty(formValues?.section.code)}
        onCancel={onClose}
        onOk={handleAdd}
      />
    </ModalRegistry>
  );
};

export default AddMethodModal;
