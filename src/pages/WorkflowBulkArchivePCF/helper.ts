import {
  MethodTransactions,
  StrategyTransactions,
  TableAreaTransactions,
  TextAreaTransactions
} from 'app/types';

export const matchFilterMethod = ({
  filterValue,
  selectedList,
  method,
  t
}: {
  filterValue?: string;
  selectedList: Record<string, string[]>;
  method: MethodTransactions;
  t: any;
}) => {
  let selectedValues = [] as string[];
  for (const key in selectedList) {
    selectedValues = [...selectedValues, ...selectedList[key]];
  }

  switch (filterValue) {
    case t('txt_all'):
      return true;
    case t('txt_workflow_bulk_archive_pcf_selected_method'):
      return selectedValues.includes(method.id);
    case t('txt_workflow_bulk_archive_pcf_unselected_method'):
      return !selectedValues.includes(method.id);
    default:
      return true;
  }
};

export const matchFilterStrategy = ({
  filterValue,
  selectedList,
  strategy,
  t
}: {
  filterValue?: string;
  selectedList: Record<string, string[]>;
  strategy: StrategyTransactions;
  t: any;
}) => {
  let selectedValues = [] as string[];

  for (const key in selectedList) {
    selectedValues = [...selectedValues, ...selectedList[key]];
  }

  switch (filterValue) {
    case t('txt_all'):
      return true;
    case t('txt_workflow_bulk_archive_pcf_selected_strategies'):
      return selectedValues.includes(strategy.id);
    case t('txt_workflow_bulk_archive_pcf_unselected_strategies'):
      return !selectedValues.includes(strategy.id);
    default:
      return true;
  }
};

export const matchFilterTextArea = ({
  filterValue,
  selectedList,
  textArea,
  t
}: {
  filterValue?: string;
  selectedList: Record<string, string[]>;
  textArea: TextAreaTransactions;
  t: any;
}) => {
  let selectedValues = [] as string[];

  for (const key in selectedList) {
    selectedValues = [...selectedValues, ...selectedList[key]];
  }

  switch (filterValue) {
    case t('txt_all'):
      return true;
    case t('txt_workflow_bulk_archive_pcf_selected_text_ids'):
      return selectedValues.includes(textArea.id);
    case t('txt_workflow_bulk_archive_pcf_unselected_text_ids'):
      return !selectedValues.includes(textArea.id);
    default:
      return true;
  }
};

export const matchFilterTableArea = ({
  filterValue,
  selectedList,
  tableArea,
  t
}: {
  filterValue?: string;
  selectedList: Record<string, string[]>;
  tableArea: TableAreaTransactions;
  t: any;
}) => {
  let selectedValues = [] as string[];

  for (const key in selectedList) {
    selectedValues = [...selectedValues, ...selectedList[key]];
  }

  switch (filterValue) {
    case t('txt_all'):
      return true;
    case t('txt_workflow_bulk_archive_pcf_selected_table_ids'):
      return selectedValues.includes(tableArea.id);
    case t('txt_workflow_bulk_archive_pcf_unselected_table_ids'):
      return !selectedValues.includes(tableArea.id);
    default:
      return true;
  }
};

export const checkValidItem = (
  selectedList: Record<string, string[]>,
  listCodes: string[]
) => {
  for (const key in selectedList) {
    if (!listCodes?.includes(key)) continue;
    if (selectedList[key]?.length !== 0) return true;
  }
  return false;
};
