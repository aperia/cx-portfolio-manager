import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { stepRegistry } from '../_commons/WorkflowSetup/registries';
import GettingStartStep from './GettingStartStep';
import SelectPricingMethodStep from './SelectPricingMethodStep';
import SelectPricingStrategyStep from './SelectPricingStrategyStep';
import SelectTableAreaStep from './SelectTableArea';
import SelectTextAreaStep from './SelectTextArea';

stepRegistry.registerStep(
  'GetStartedWorkflowBulkArchivePCF',
  GettingStartStep,
  [
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP_BULK_ARCHIVE_PCF_GET_STARTED_EXISTED_WORKFLOW,
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP_BULK_ARCHIVE_PCF_GET_STARTED_EXISTED_WORKFLOW__STUCK
  ]
);

stepRegistry.registerStep(
  'SlectPricingMethodBulkArchivePCF',
  SelectPricingMethodStep,
  [
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP_BULK_ARCHIVE_PCF_SELECT_PRICING_METHOD__EXISTED_WORKFLOW
  ]
);

stepRegistry.registerStep(
  'SlectPricingStrategyBulkArchivePCF',
  SelectPricingStrategyStep,
  [
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP_BULK_ARCHIVE_PCF_SELECT_PRICING_STRATEGY__EXISTED_WORKFLOW
  ]
);

stepRegistry.registerStep('SelectTextAreaBulkArchivePCF', SelectTextAreaStep, [
  UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP_BULK_ARCHIVE_PCF_SELECT_TEXT_AREA__EXISTED_WORKFLOW
]);

stepRegistry.registerStep(
  'SelectTableAreaBulkArchivePCF',
  SelectTableAreaStep,
  [
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP_BULK_ARCHIVE_PCF_SELECT_TABLE_AREA__EXISTED_WORKFLOW
  ]
);
