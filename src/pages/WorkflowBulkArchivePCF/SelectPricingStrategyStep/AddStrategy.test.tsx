import { fireEvent, render } from '@testing-library/react';
import 'app/utils/_mockComponent/mockComboBox';
import 'app/utils/_mockComponent/mockModalRegistry';
import React from 'react';
import AddStrategy from './AddStrategy';

jest.mock('pages/_commons/redux/WorkflowSetup', () => ({
  actionsWorkflowSetup: { getWorkflowMetadata: jest.fn() },
  useSelectElementsBulkArchivePCF: () => ({
    strategies: [],
    methods: [
      {
        value: '',
        description: 'None',
        code: '',
        text: 'None'
      },
      {
        value: 'D',
        description:
          'Deny the item if it exceeds the amount in the LIMIT field.',
        code: 'D'
      },
      {
        value: 'Q',
        description:
          'Assign the item to the pending queue if it exceeds the amount in the LIMIT field.',
        code: 'Q',
        text: 'Q - Assign the item to the pending queue if it exceeds the amount in the LIMIT field.'
      }
    ]
  })
}));

const factory = (isExistMethod?: boolean, hasMethod?: boolean) => {
  const mockChangeMethod = jest.fn();
  const { getByTestId } = render(
    <AddStrategy
      isExistStrategy={isExistMethod}
      strategy={hasMethod ? { code: '1', text: '1' } : undefined}
      onChangeStrategy={mockChangeMethod}
    />
  );

  const onSelectMethod = () => {
    const combobox = getByTestId('ComboBox.Input_onChange');
    fireEvent.change(combobox, { target: { value: '1' } });
  };

  return {
    onSelectMethod,
    mockChangeMethod
  };
};

describe('test AddMethod component', () => {
  it('should be render', () => {
    const { onSelectMethod, mockChangeMethod } = factory();
    onSelectMethod();
    expect(mockChangeMethod).toBeCalled();
  });
  it('should be render with existed method', () => {
    const { onSelectMethod, mockChangeMethod } = factory(true, true);
    onSelectMethod();
    expect(mockChangeMethod).toBeCalled();
  });
});
