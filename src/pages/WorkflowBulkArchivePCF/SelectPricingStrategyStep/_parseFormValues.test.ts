import parseFormValues from './_parseFormValues';

describe('pages > WorkFlowBulkArchivePCF > SelectPricingStrategyStep > parseFormValues', () => {
  const mockDispatch = jest.fn();

  const methodList = [
    {
      rowId: '1',
      effectiveDate: '2022-12-14T00:00:00',
      strategyName: 'strategyName',
      selected: '123'
    },
    {
      rowId: '2',
      effectiveDate: '2022-12-14T00:00:00',
      strategyName: 'strategyName',
      selected: '123'
    },
    {
      rowId: '3',
      effectiveDate: '2022-12-14T00:00:00',
      strategyName: 'strategyName',
      selected: '123'
    },
    {
      rowId: '4',
      effectiveDate: '2022-12-14T00:00:00',
      strategyName: 'strategyName',
      selected: '123'
    }
  ];

  it('Should return undefined', () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '2'
            }
          ] as any
        }
      } as WorkflowSetupInstance,
      id: '1'
    };

    parseFormValues(props.data, props.id, mockDispatch);
  });

  it('Should return data', () => {
    const props = {
      data: {
        data: {
          configurations: {},
          workflowSetupData: [
            {
              id: '1'
            }
          ] as any
        }
      } as unknown as WorkflowSetupInstance,
      id: '1'
    };

    parseFormValues(props.data, props.id, mockDispatch);
  });

  it('Should return data', () => {
    const props = {
      data: {
        data: {
          configurations: {
            pricingStrategies: [
              {
                rowId: '1',
                effectiveDate: '2022-12-14T00:00:00',
                strategyName: 'strategyName',
                selected: '123',
                code: '123',
                methodList
              },
              {
                rowId: '2',
                effectiveDate: '2022-12-14T00:00:00',
                strategyName: 'strategyName',
                selected: '123',
                code: '123',
                methodList
              },
              {
                rowId: '3',
                effectiveDate: '2022-12-14T00:00:00',
                strategyName: 'strategyName',
                selected: '123',
                code: '123',
                methodList
              },
              {
                rowId: '4',
                effectiveDate: '2022-12-14T00:00:00',
                strategyName: 'strategyName',
                selected: '123',
                code: '123',
                methodList
              }
            ]
          },
          workflowSetupData: [
            {
              id: '1'
            }
          ] as any
        }
      } as unknown as WorkflowSetupInstance,
      id: '1'
    };

    parseFormValues(props.data, props.id, mockDispatch);
  });
});
