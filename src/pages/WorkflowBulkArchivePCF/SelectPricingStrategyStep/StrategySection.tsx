import NoDataFound from 'app/components/NoDataFound';
import { classnames, getSelectedRecord, isDevice } from 'app/helpers';
import { useCheckAllPagination } from 'app/hooks';
import { usePagination } from 'app/hooks/usePagination';
import {
  Button,
  Grid,
  Icon,
  SortType,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import { differenceWith, isEmpty, isEqual } from 'app/_libraries/_dls/lodash';
import Paging from 'pages/_commons/Utils/Paging';
import React, { useEffect, useMemo } from 'react';
import { FormSelectStrategy } from '.';
import { columns } from './constant';
import { FilterStrategyDetail } from './StrategyList';

export interface StrategySectionProps {
  isExpand: boolean;
  strategy: FilterStrategyDetail;
  resetFilter: boolean;
  searchValue?: string;
  filterValue?: string;
  selectedList: string[];
  sortBy: SortType;
  setSortBy: (id: string, sort: SortType) => void;
  onClickExpand: VoidFunction;
  setSelectedList: (id: string, list: string[]) => void;
}
const StrategySection: React.FC<
  WorkflowSetupProps<FormSelectStrategy> & StrategySectionProps
> = ({
  stepId,
  selectedStep,
  isExpand,
  strategy,
  resetFilter,
  searchValue,
  filterValue,
  selectedList,
  sortBy,
  setSortBy,
  onClickExpand,
  setSelectedList,
  setFormValues,
  formValues
}) => {
  const { t } = useTranslation();
  const { text: strategyName, methodList, filteredTransactions } = strategy;
  const isDefaultFilter = filterValue === t('txt_all');
  const isUnselected =
    filterValue === t('txt_workflow_bulk_archive_pcf_unselected_strategies');

  const readOnlyCheckbox = useMemo(() => [] as string[], []);

  const {
    sort,
    total,
    currentPage,
    currentPageSize,
    gridData: dataView,
    dataChecked: _checkedList,
    onSetDataChecked,
    onSortChange,
    onPageChange,
    onPageSizeChange,
    onResetToDefaultFilter
  } = usePagination(filteredTransactions, [], 'strategyName');

  const checkedList = useMemo(
    () => _checkedList.filter(r => !readOnlyCheckbox.includes(r)),
    [_checkedList, readOnlyCheckbox]
  );
  const allIds = methodList
    .map(f => f.rowId!)
    .filter(r => !readOnlyCheckbox.includes(r));
  const { handleClickCheckAll, gridClassName } = useCheckAllPagination(
    {
      gridClassName: `ba-pcf-strategy-${strategy.text?.replace(/\s/g, '')}`,
      allIds,
      hasFilter: !!searchValue || !isDefaultFilter,
      filterIds: filteredTransactions
        .map(f => f.rowId!)
        .filter(r => !readOnlyCheckbox.includes(r)),
      checkedList,
      setCheckAll: _isCheckAll => onSetDataChecked(_isCheckAll ? allIds : []),
      setCheckList: onSetDataChecked
    },
    [searchValue, sort, currentPage, currentPageSize]
  );

  useEffect(() => {
    if (stepId !== selectedStep) {
      onResetToDefaultFilter();
    }
  }, [stepId, selectedStep, onSetDataChecked, onResetToDefaultFilter]);

  useEffect(() => {
    if (isEqual(checkedList, selectedList)) return;
    setSelectedList(strategy.code, checkedList);
  }, [checkedList, strategy.code, setSelectedList, selectedList]);

  useEffect(() => {
    if (isEqual(sort, sortBy)) return;
    setSortBy(strategy.code, sort);
  }, [sort, sortBy, strategy.code, setSortBy]);

  useEffect(() => {
    isEmpty(checkedList) && onSetDataChecked(selectedList);
    // rerender selectedList after onSetDataChecked
    isEmpty(checkedList) && setSelectedList(strategy.code, selectedList);
    if (!isEmpty(sortBy)) onSortChange(sortBy);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (!resetFilter) return;
    onPageChange(1);
  }, [
    selectedList,
    resetFilter,
    strategy.code,
    onSetDataChecked,
    onPageChange
  ]);

  const isNoData = useMemo(() => methodList.length <= 0, [methodList.length]);

  const showCountSelectedSection = useMemo(
    () => !searchValue || checkedList?.length > 1,
    [searchValue, checkedList?.length]
  );

  const disableHideStrategy = checkedList?.length > 0;

  const selectedText = useMemo(
    () =>
      getSelectedRecord({
        selected: filteredTransactions.filter(i => selectedList.includes(i.id))
          .length,
        total: differenceWith(
          methodList.map(f => f.rowId!),
          readOnlyCheckbox
        ).length,
        text: t('txt_workflow_bulk_archive_pcf_strategies_selected', {
          selected: filteredTransactions.filter(i =>
            selectedList.includes(i.id)
          ).length
        }),
        text1: t('txt_workflow_bulk_archive_pcf_1_strategy_selected'),
        textAll: t('txt_workflow_bulk_archive_pcf_all_strategies_selected')
      }),
    [readOnlyCheckbox, methodList, t, filteredTransactions, selectedList]
  );

  const onClickHide = () => {
    setFormValues({
      pricingStrategies: formValues.pricingStrategies.filter(
        f => f.code !== strategy.code
      )
    });
  };

  return (
    <>
      <div className="d-flex align-items-center justify-content-between">
        <div className="d-flex align-items-center">
          <Tooltip
            element={isExpand ? t('txt_collapse') : t('txt_expand')}
            opened={isDevice ? false : undefined}
            variant="primary"
            placement="top"
            triggerClassName="d-flex ml-n2"
          >
            <Button size="sm" variant="icon-secondary" onClick={onClickExpand}>
              <Icon name={isExpand ? 'minus' : 'plus'} size="4x" />
            </Button>
          </Tooltip>

          <strong className="ml-8">{strategyName}</strong>
        </div>
        <div className="d-flex align-items-center mr-n8">
          <Tooltip
            opened={disableHideStrategy ? undefined : false}
            element={t(
              'txt_workflow_bulk_archive_pcf_hide_this_strategy_area_tooltip'
            )}
          >
            <Button
              size="sm"
              variant="outline-primary"
              disabled={disableHideStrategy}
              onClick={onClickHide}
            >
              {t('txt_workflow_bulk_archive_pcf_hide_this_strategy_area')}
            </Button>
          </Tooltip>
        </div>
      </div>
      <div className={classnames(!isExpand && 'd-none')}>
        {isNoData && (
          <div className="d-flex flex-column justify-content-center mt-40 mb-24">
            <NoDataFound
              id="newMethod_notfound"
              title={t(
                'txt_workflow_bulk_archive_pcf_strategy_list_no_results_found'
              )}
            />
          </div>
        )}

        {showCountSelectedSection &&
          !isNoData &&
          !searchValue &&
          !isUnselected && <p className="mt-16">{selectedText}</p>}

        {!isNoData && (
          <div className={classnames('mt-16')}>
            <Grid
              className={gridClassName}
              dataItemKey="rowId"
              rowKey="rowId"
              data={dataView}
              columns={columns(t)}
              checkedList={checkedList}
              sortBy={[sort]}
              onSortChange={onSortChange}
              variant={{
                id: 'rowId',
                type: 'checkbox',
                cellHeaderProps: { onClick: handleClickCheckAll }
              }}
              readOnlyCheckbox={readOnlyCheckbox}
              onCheck={onSetDataChecked}
            />
            {total > 10 && (
              <div className="mt-16">
                <Paging
                  page={currentPage}
                  pageSize={currentPageSize}
                  totalItem={total}
                  onChangePage={onPageChange}
                  onChangePageSize={onPageSizeChange}
                />
              </div>
            )}
          </div>
        )}
      </div>
    </>
  );
};

export default StrategySection;
