import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import { classnames, matchSearchValue } from 'app/helpers';
import { StrategyDetail, StrategyTransactions } from 'app/types';
import {
  DropdownBaseChangeEvent,
  DropdownList,
  SortType,
  useTranslation
} from 'app/_libraries/_dls';
import { differenceWith, isEmpty } from 'app/_libraries/_dls/lodash';
import ClearAndResetButton from 'pages/_commons/Utils/ClearAndResetButton';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { FormSelectStrategy } from '.';
import { checkValidItem, matchFilterStrategy } from '../helper';
import { FilterOption } from '../type';
import { strategyFilterOptions } from './constant';
import StrategySection from './StrategySection';

export interface FilterStrategyDetail extends StrategyDetail {
  filteredTransactions: StrategyTransactions[];
}
export interface StrategyListProps {
  strategies: StrategyDetail[];
  keepRef: any;
  isValid?: boolean;
}

const StrategyList: React.FC<
  WorkflowSetupProps<FormSelectStrategy> & StrategyListProps
> = props => {
  const { t } = useTranslation();
  const { strategies, stepId, selectedStep, keepRef, isValid, formValues } =
    props;

  const prevRef = useRef({ strategies });
  const searchRef = useRef<any>(null);

  const [searchValue, setSearchValue] = useState<string>('');
  const [filterValue, setFilterValue] = useState<string>(t('txt_all'));
  const [shouldResetFilter, setShouldResetFilter] = useState(false);

  const [expandList, setExpandList] = useState<Record<string, boolean>>({});
  const [sortBy, setSortBy] = useState<Record<string, SortType>>({});

  const strategyOptions = useMemo(() => strategyFilterOptions(t), [t]);

  const filteredStrategies = useMemo(
    () =>
      strategies
        .map(p => {
          if (isEmpty(p.methodList)) return { ...p, filteredTransactions: [] };

          return {
            ...p,
            filteredTransactions: p.methodList.filter(
              w =>
                matchSearchValue([w.strategyName], searchValue) &&
                matchFilterStrategy({
                  filterValue,
                  selectedList: formValues?.selectedList as Record<
                    string,
                    string[]
                  >,
                  strategy: w,
                  t
                })
            )
          } as FilterStrategyDetail;
        })
        .filter(
          f =>
            f.filteredTransactions.length > 0 ||
            (!searchValue && filterValue === t('txt_all'))
        ),
    [strategies, searchValue, filterValue, formValues?.selectedList, t]
  );

  const handleExpandByDefault = useCallback(() => {
    const list: Record<string, boolean> = {};
    strategies.forEach(strategy => {
      list[strategy.code] = true;
    });
    setExpandList(list);
    setShouldResetFilter(true);
  }, [strategies]);

  const handleClearFilter = useCallback(() => {
    searchRef?.current?.clear();
    setSearchValue('');
    setFilterValue(t('txt_all'));
    handleExpandByDefault();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [handleExpandByDefault]);

  const handleSearch = (val: string) => {
    setSearchValue(val);
    handleExpandByDefault();
  };

  const handleFilter = (e: DropdownBaseChangeEvent) => {
    setFilterValue(e.target?.value);
  };

  useEffect(() => {
    if (stepId !== selectedStep) {
      setShouldResetFilter(true);
      handleClearFilter();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [stepId, selectedStep, handleClearFilter]);

  useEffect(() => {
    if (shouldResetFilter) {
      setShouldResetFilter(false);
    }
  }, [shouldResetFilter]);

  useEffect(() => {
    const checkValid = checkValidItem(
      formValues.selectedList as Record<string, string[]>,
      strategies.map(s => s.code)
    );
    if (isValid === checkValid) return;
    keepRef.current.setFormValues({ isValid: checkValid });
  }, [isValid, strategies, keepRef, formValues.selectedList]);

  useEffect(() => {
    const _prevRef = prevRef.current;
    const prevMethod = _prevRef.strategies;
    // reset filter when add new method
    if (strategies.length > prevMethod.length) {
      handleClearFilter();
    }
  }, [handleClearFilter, strategies.length]);

  useEffect(() => {
    const _prevRef = prevRef.current;
    const prevStrategy = _prevRef.strategies;

    const prevLength = prevStrategy.length;
    const currLength = strategies.length;

    const isFirstAdd = currLength === prevLength && currLength === 1;

    const diff = isFirstAdd
      ? strategies[0].code
      : differenceWith(
          strategies,
          prevStrategy,
          (o1, o2) => o1.code === o2.code
        )?.[0]?.code;

    if (!!diff) {
      setExpandList({ ...expandList, [diff]: true });
      _prevRef.strategies = strategies;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [strategies]);

  const handleClickExpand = (id: string, isExpand: boolean) => () => {
    setExpandList(list => ({ ...list, [id]: isExpand }));
  };

  const handleClickSort = (id: string, sort: SortType) => {
    setSortBy(list => ({ ...list, [id]: sort }));
  };

  const handleSetSelectedList = (id: string, _list: string[]) => {
    // setSelectedList(list => ({ ...list, [id]: _list }));
    keepRef.current.setFormValues({
      selectedList: { ...formValues?.selectedList, [id]: _list }
    });
  };
  return (
    <>
      <div className="d-flex align-items-center justify-content-between mb-16">
        <h5>{t('txt_workflow_bulk_archive_pcf_strategy_list')}</h5>
        <SimpleSearch
          ref={searchRef}
          placeholder={t('txt_workflow_bulk_archive_pcf_search_by_strategy')}
          onSearch={handleSearch}
        />
      </div>

      <div className="d-flex align-items-center">
        <strong className="fs-14 color-grey mr-4">{t('txt_filter')}:</strong>
        <DropdownList
          name="status"
          value={filterValue}
          textField="description"
          onChange={handleFilter}
          variant="no-border"
        >
          {strategyOptions.map((item: FilterOption) => (
            <DropdownList.Item
              key={item.value}
              label={item.text}
              value={item.text}
            />
          ))}
        </DropdownList>
      </div>

      {searchValue && !isEmpty(filteredStrategies) && (
        <div className="d-flex justify-content-end mb-16 mr-n8">
          <ClearAndResetButton
            small
            onClearAndReset={() => handleClearFilter()}
          />
        </div>
      )}
      {isEmpty(filteredStrategies) && (
        <div className="d-flex flex-column justify-content-center mt-40 mb-24">
          <NoDataFound
            id="newStrategy_notfound"
            hasSearch={!!searchValue}
            hasFilter={filterValue !== t('txt_all')}
            title={t(
              'txt_workflow_bulk_archive_pcf_strategy_list_no_results_found'
            )}
            linkTitle={t('txt_clear_and_reset')}
            onLinkClicked={() => handleClearFilter()}
          />
        </div>
      )}
      {!isEmpty(filteredStrategies) && (
        <div className="mt-16">
          {filteredStrategies.map((strategy, idx) => {
            const isExpand = !!expandList[strategy.code];

            return (
              <div
                key={strategy.code}
                className={classnames(idx !== 0 && 'mt-24 pt-24 border-top')}
              >
                <StrategySection
                  {...props}
                  strategy={strategy}
                  isExpand={isExpand}
                  resetFilter={shouldResetFilter}
                  filterValue={filterValue}
                  searchValue={searchValue}
                  selectedList={formValues.selectedList[strategy.code] || []}
                  sortBy={sortBy[strategy.code]}
                  setSortBy={handleClickSort}
                  onClickExpand={handleClickExpand(strategy.code, !isExpand)}
                  setSelectedList={handleSetSelectedList}
                />
              </div>
            );
          })}
        </div>
      )}
    </>
  );
};

export default StrategyList;
