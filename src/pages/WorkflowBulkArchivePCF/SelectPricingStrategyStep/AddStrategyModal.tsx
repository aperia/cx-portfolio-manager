import ModalRegistry from 'app/components/ModalRegistry';
import {
  InlineMessage,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import { isFunction } from 'lodash';
import React, { useState } from 'react';
import AddStrategy from './AddStrategy';

export interface AddStrategyModalProps {
  existStrategyIds: string[];
  onClose?: VoidFunction;
  onAdd?: (strategy: RefData) => void;
}

const AddStrategyModal: React.FC<AddStrategyModalProps> = ({
  existStrategyIds,
  onClose,
  onAdd
}) => {
  const { t } = useTranslation();

  const [strategy, setStrategy] = useState<RefData>();
  const [isExistStrategy, setisExistStrategy] = useState(false);

  const handleAdd = () => {
    const _isExistStrategy =
      !!strategy?.code && existStrategyIds.includes(strategy.code);
    if (_isExistStrategy) {
      setisExistStrategy(_isExistStrategy);
      return;
    }

    isFunction(onAdd) && onAdd(strategy!);
    isFunction(onClose) && onClose();
  };

  const handleChange = (s: RefData) => {
    setStrategy(s);
  };

  return (
    <ModalRegistry id="AddStrategyModal" show onAutoClosedAll={onClose}>
      <ModalHeader border closeButton onHide={onClose}>
        <ModalTitle>
          {t('txt_workflow_bulk_archive_pcf_add_strategy_area')}
        </ModalTitle>
      </ModalHeader>
      <ModalBody>
        {isExistStrategy && (
          <InlineMessage withIcon variant="danger">
            {t(
              'txt_workflow_bulk_archive_pcf_validation_exist_select_strategy'
            )}
          </InlineMessage>
        )}
        <AddStrategy
          isExistStrategy={isExistStrategy}
          strategy={strategy}
          onChangeStrategy={handleChange}
        />
      </ModalBody>
      <ModalFooter
        cancelButtonText={t('txt_cancel')}
        okButtonText={t('txt_continue')}
        disabledOk={!strategy}
        onCancel={onClose}
        onOk={handleAdd}
      />
    </ModalRegistry>
  );
};

export default AddStrategyModal;
