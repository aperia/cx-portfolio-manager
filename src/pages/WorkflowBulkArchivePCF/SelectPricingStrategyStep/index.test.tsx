import { RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import {
  mockThunkAction,
  mockUseModalRegistryFnc,
  renderWithMockStore
} from 'app/utils';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowBulkArchivePCF';
import React from 'react';
import { ParameterCode } from '../type';
import { pricingStrategyTestId } from './constant';
import SelectPricingStrategyStep, { FormSelectStrategy } from './index';

const mockUseModalRegistry = mockUseModalRegistryFnc();

HTMLCanvasElement.prototype.getContext = jest.fn();

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

jest.mock('./StrategyList', () => ({
  __esModule: true,
  default: () => <div data-testid="StrategyList">StrategyList</div>
}));

const MockSelectElementMetadataForStrategyList = jest.spyOn(
  WorkflowSetup,
  'useStrategyTransactionsStatus'
);

const MockSelectElementMetadataForBulkArchivePCF = jest.spyOn(
  WorkflowSetup,
  'useSelectElementsBulkArchivePCF'
);

const renderComponent = async (
  props: any,
  workflowSetup?: any
): Promise<RenderResult> => {
  return await renderWithMockStore(<SelectPricingStrategyStep {...props} />, {
    initialState: { workflowSetup }
  });
};
const mockWorkflowSetup = mockThunkAction(actionsWorkflowSetup);

export const mockElements = [
  {
    name: ParameterCode.MethodName,
    options: [{ value: 'MethodName' }]
  },
  {
    name: ParameterCode.StrategyName,
    options: [{ value: 'StrategyName' }]
  }
];

describe('WorkflowBulkArchivePCF > SelectPricingStrategyStep > Index', () => {
  beforeEach(() => {
    mockUseModalRegistry.mockReturnValue([true, false]);
    mockWorkflowSetup('getBulkArchivePCFWorkflowStrategies', {
      match: true
    });
    MockSelectElementMetadataForStrategyList.mockReturnValue({
      loading: true,
      error: false
    });
    MockSelectElementMetadataForBulkArchivePCF.mockReturnValue({
      strategies: [
        { code: 'strategy1', text: 'text1' },
        { code: 'strategy2', text: 'text2' }
      ],
      methods: [],
      textAreas: [],
      tableAreas: []
    });
  });

  const defaultWorkflowSetup = {
    elementMetadata: { elements: mockElements },
    workflowStrategies: { loading: true, error: false }
  };

  it('should render with empty strategy', async () => {
    const setFormValues = jest.fn() as any;
    const props = {
      formValues: {
        pricingStrategies: [] as any,
        methods: [{ code: '001', text: '001', transactions: [] as any[] }],
        selectedList: {}
      },
      setFormValues,
      isStuck: true
    } as unknown as WorkflowSetupProps<FormSelectStrategy>;

    mockWorkflowSetup('getBulkArchivePCFWorkflowStrategies', {
      match: true,
      payload: { payload: { strategies: [{}] } }
    });
    MockSelectElementMetadataForStrategyList.mockReturnValueOnce({
      loading: true
    });

    const { getByText, getByTestId } = await renderComponent(
      props,
      defaultWorkflowSetup
    );

    userEvent.click(getByTestId(pricingStrategyTestId.viewInstructionModal));
    userEvent.click(
      getByText('txt_workflow_bulk_archive_pcf_select_strategy_area')
    );
    userEvent.click(getByText('txt_workflow_bulk_archive_pcf_strategy_area'));
    userEvent.click(getByText('text1'));

    expect(
      getByText('txt_workflow_bulk_archive_pcf_select_strategy_area')
    ).toBeInTheDocument();
  });

  it('should add strategy success', async () => {
    mockWorkflowSetup('getBulkArchivePCFWorkflowStrategies', {
      match: true,
      payload: { payload: { strategies: [] } }
    });
    const setFormValues = jest.fn() as any;
    const props = {
      formValues: {
        pricingStrategies: [
          { code: 'strategy1', text: 'text1', transactions: [] as any[] }
        ],
        methods: [{ code: '001', text: '001', transactions: [] as any[] }],
        selectedList: {}
      },
      setFormValues
    } as unknown as WorkflowSetupProps<FormSelectStrategy>;

    const { baseElement, getByText, getByTestId, getAllByTestId } =
      await renderComponent(props, defaultWorkflowSetup);
    expect(
      getByText('txt_workflow_bulk_archive_pcf_add_strategy_area')
    ).toBeInTheDocument();

    userEvent.click(getByTestId(pricingStrategyTestId.viewInstructionModal));
    userEvent.click(getByText('txt_close'));
    userEvent.click(
      getByText('txt_workflow_bulk_archive_pcf_add_strategy_area')
    );
    expect(baseElement.querySelector('.dls-modal-root')).toBeInTheDocument();

    userEvent.click(getByText('txt_workflow_bulk_archive_pcf_strategy_area'));
    userEvent.click(
      getAllByTestId(
        `${pricingStrategyTestId.dropdownElement}_dls-high-light-words`
      )[0]
    );
    userEvent.click(getByText('txt_workflow_bulk_archive_pcf_strategy_area'));
    userEvent.click(
      getAllByTestId(
        `${pricingStrategyTestId.dropdownElement}_dls-high-light-words`
      )[0]
    );
    userEvent.click(getByText('txt_continue'));
  });

  it('should add strategy fail', async () => {
    mockWorkflowSetup('getBulkArchivePCFWorkflowStrategies', {
      match: false
    });
    const setFormValues = jest.fn() as any;
    const props = {
      formValues: {
        pricingStrategies: [
          { code: '001', text: '001', transactions: [] as any[] }
        ],
        methods: [{ code: '001', text: '001', transactions: [] as any[] }],
        selectedList: {}
      },
      setFormValues
    } as unknown as WorkflowSetupProps<FormSelectStrategy>;

    const { baseElement, getByText, getByTestId } = await renderComponent(
      props,
      defaultWorkflowSetup
    );
    userEvent.click(getByTestId(pricingStrategyTestId.viewInstructionModal));

    userEvent.click(
      getByText('txt_workflow_bulk_archive_pcf_add_strategy_area')
    );
    expect(baseElement.querySelector('.dls-modal-root')).toBeInTheDocument();
    userEvent.click(getByText('txt_cancel'));

    userEvent.click(
      getByText('txt_workflow_bulk_archive_pcf_add_strategy_area')
    );

    userEvent.click(getByText('txt_workflow_bulk_archive_pcf_strategy_area'));
    userEvent.click(getByText('text1'));

    userEvent.click(getByText('txt_continue'));
  });
});
