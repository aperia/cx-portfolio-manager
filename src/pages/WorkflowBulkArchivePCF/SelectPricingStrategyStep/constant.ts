import { ColumnType } from 'app/_libraries/_dls';
import { formatText } from 'pages/_commons/Utils/formatGridField';
import { FilterOption } from '../type';

export const columns = (t: any, isSort = true): ColumnType[] => {
  const cols = [
    {
      id: 'strategyName',
      Header: t('txt_workflow_bulk_archive_pcf_strategy_name'),
      accessor: 'strategyName',
      isSort
    },
    {
      id: 'effectiveDate',
      Header: t('txt_workflow_bulk_archive_pcf_effective_date'),
      accessor: formatText(['effectiveDate'], { format: 'date' }),
      isSort
    }
  ];
  return cols;
};

export const strategyFilterValues = {
  all: 'all',
  selected: 'selected',
  unSelected: 'Unselected'
};

export const strategyFilterOptions = (t: any): FilterOption[] => {
  return [
    {
      value: strategyFilterValues.all,
      text: t('txt_all')
    },
    {
      value: strategyFilterValues.selected,
      text: t('txt_workflow_bulk_archive_pcf_selected_strategies')
    },
    {
      value: strategyFilterValues.unSelected,
      text: t('txt_workflow_bulk_archive_pcf_unselected_strategies')
    }
  ];
};

export const pricingStrategyTestId = {
  viewInstructionModal: 'viewInstructionModal',
  dropdownElement: 'strategyDropdownElement'
};
