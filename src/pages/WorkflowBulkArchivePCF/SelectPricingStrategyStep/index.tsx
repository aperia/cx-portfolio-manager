import {
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { classnames } from 'app/helpers';
import { useUnsavedChangeRegistry } from 'app/hooks';
import { StrategyDetail } from 'app/types';
import { Button, InlineMessage, useTranslation } from 'app/_libraries/_dls';
import { isEqual } from 'app/_libraries/_dls/lodash';
import { orderBy } from 'lodash';
import {
  actionsWorkflowSetup,
  useStrategyTransactionsStatus
} from 'pages/_commons/redux/WorkflowSetup';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, { useEffect, useMemo, useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import { ParameterCode } from '../type';
import AddStrategy from './AddStrategy';
import AddStrategyModal from './AddStrategyModal';
import { pricingStrategyTestId } from './constant';
import InstructionModal from './InstructionModal';
import StrategyList from './StrategyList';
import Summary from './Summary';
import parseFormValues from './_parseFormValues';

export interface FormSelectStrategy {
  isValid?: boolean;
  selectedList: Record<string, string[]>;
  pricingStrategies: StrategyDetail[];
}

const SelectStrategyStrategyStep: React.FC<
  WorkflowSetupProps<FormSelectStrategy>
> = props => {
  const { setFormValues, formValues, savedAt, isStuck } = props;
  const [initialValues, setInitialValues] = useState(formValues);
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;

  const { loading } = useStrategyTransactionsStatus();
  const [showAddStrategyModal, setShowAddStrategyModal] = useState(false);
  const [showInstructionModal, setShowInstructionModal] = useState(false);

  const existStrategyIds = useMemo(
    () => formValues?.pricingStrategies?.map(m => m.code),
    [formValues?.pricingStrategies]
  );

  const hasStrategy = useMemo(
    () => existStrategyIds.length > 0,
    [existStrategyIds.length]
  );

  useEffect(() => {
    dispatch(
      actionsWorkflowSetup.getWorkflowMetadata([
        ParameterCode.MethodName,
        ParameterCode.StrategyName
      ])
    );
  }, [dispatch]);

  useEffect(() => {
    setInitialValues(formValues);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [savedAt]);

  const formChanged = useMemo(
    () =>
      !isEqual(formValues.pricingStrategies, initialValues.pricingStrategies),
    [formValues.pricingStrategies, initialValues.pricingStrategies]
  );

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP_BULK_ARCHIVE_PCF_SELECT_PRICING_STRATEGY__EXISTED_WORKFLOW,
      priority: 1
    },
    [formChanged]
  );

  const handleSelectStrategy = async (strategy: RefData) => {
    const transactionsResp = (await Promise.resolve(
      dispatch(
        actionsWorkflowSetup.getBulkArchivePCFWorkflowStrategies({
          strategyArea: strategy.code
        })
      )
    )) as any;
    const isSuccess =
      actionsWorkflowSetup.getBulkArchivePCFWorkflowStrategies.fulfilled.match(
        transactionsResp
      );

    if (isSuccess) {
      const _strategy: StrategyDetail = {
        ...strategy,
        methodList: transactionsResp?.payload?.strategies?.map(
          (t: any, idx: number) => {
            const rowId = `${strategy.code}_${idx}_${Date.now()}`;
            const methodList = {
              ...t,
              rowId: rowId,
              id: rowId,
              'strategy.name': t?.strategyName,
              'effective.date': t?.effectiveDate
            };

            return {
              ...methodList,
              originalStrategy: methodList
            };
          }
        )
      };

      const _strategies = [_strategy, ...formValues.pricingStrategies];

      setFormValues({ pricingStrategies: orderBy(_strategies, ['code']) });
    }
  };

  const handleOpenAddStrategyModal = () => {
    setShowAddStrategyModal(true);
  };

  const handleCloseAddStrategyModal = () => {
    setShowAddStrategyModal(false);
  };

  const instructionView = useMemo(
    () => (
      <>
        <div className="mt-8 pb-24 border-bottom color-grey">
          <p>
            {t('txt_workflow_bulk_archive_pcf_strategy_instruction')}{' '}
            <span
              className="link text-decoration-none px-4"
              onClick={() => setShowInstructionModal(true)}
              data-testid={pricingStrategyTestId.viewInstructionModal}
            >
              {t('txt_workflow_bulk_archive_pcf_view_instruction')}
            </span>
          </p>
        </div>
        {isStuck && (
          <InlineMessage className="mb-0 mt-24" variant="danger" withIcon>
            {t('txt_step_stuck_move_forward_message')}
          </InlineMessage>
        )}
      </>
    ),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [isStuck]
  );

  return (
    <>
      {!hasStrategy && (
        <>
          {instructionView}
          <div className={classnames('mt-24', loading && 'loading')}>
            <h5>{t('txt_workflow_bulk_archive_pcf_select_strategy_area')}</h5>
            <div className="row mt-16">
              <div className="col-6 col-lg-4">
                <AddStrategy onChangeStrategy={handleSelectStrategy} />
              </div>
            </div>
          </div>
        </>
      )}
      {hasStrategy && (
        <>
          <div
            className={classnames('position-relative', loading && 'loading')}
          >
            <div className="absolute-top-right mt-n32 mr-n8">
              <Button
                variant="outline-primary"
                size="sm"
                onClick={handleOpenAddStrategyModal}
              >
                {t('txt_workflow_bulk_archive_pcf_add_strategy_area')}
              </Button>
            </div>
            {instructionView}
          </div>
          <div className="mt-24">
            <StrategyList
              strategies={formValues?.pricingStrategies}
              isValid={formValues.isValid}
              keepRef={keepRef}
              {...props}
            />

            {showAddStrategyModal && (
              <AddStrategyModal
                existStrategyIds={existStrategyIds}
                onClose={handleCloseAddStrategyModal}
                onAdd={handleSelectStrategy}
              />
            )}
          </div>
        </>
      )}
      {showInstructionModal && (
        <InstructionModal
          opened={showInstructionModal}
          onClose={() => setShowInstructionModal(false)}
        />
      )}
    </>
  );
};

const ExtraStaticSelectPricingStrategySteptep =
  SelectStrategyStrategyStep as WorkflowSetupStaticProp<FormSelectStrategy>;

ExtraStaticSelectPricingStrategySteptep.summaryComponent = Summary;
ExtraStaticSelectPricingStrategySteptep.defaultValues = {
  isValid: false,
  selectedList: {},
  pricingStrategies: []
};
ExtraStaticSelectPricingStrategySteptep.parseFormValues = parseFormValues;

export default ExtraStaticSelectPricingStrategySteptep;
