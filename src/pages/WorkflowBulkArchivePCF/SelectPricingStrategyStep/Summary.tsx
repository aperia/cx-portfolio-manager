import { classnames, isDevice } from 'app/helpers';
import { usePagination } from 'app/hooks/usePagination';
import { StrategyDetail } from 'app/types';
import {
  Button,
  Grid,
  Icon,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty, isFunction } from 'lodash';
import Paging from 'pages/_commons/Utils/Paging';
import React, { useEffect, useMemo, useState } from 'react';
import { FormSelectStrategy } from '.';
import { columns } from './constant';

const SelectPricingStrategySummary: React.FC<
  WorkflowSetupSummaryProps<FormSelectStrategy>
> = ({ onEditStep, formValues, stepId, selectedStep }) => {
  const { t } = useTranslation();

  const [expandList, setExpandList] = useState<Record<string, boolean>>({});

  const handleEdit = () => {
    isFunction(onEditStep) && onEditStep();
  };

  const resetFilter = useMemo(
    () => stepId === selectedStep,
    [stepId, selectedStep]
  );

  const strategySelected = useMemo(() => {
    if (isEmpty(formValues?.pricingStrategies)) return [];
    const { selectedList = {}, pricingStrategies } = formValues;
    return pricingStrategies
      ?.filter(s => selectedList[s.code]?.length)
      ?.map(s => ({
        ...s,
        methodList: s.methodList.filter(t =>
          selectedList[s.code].includes(t.id)
        )
      }));
  }, [formValues]);

  useEffect(() => {
    if (!resetFilter) return;
    const expands = strategySelected?.reduce((returnValue, m) => {
      return { ...returnValue, [m?.code]: true };
    }, {});
    resetFilter && setExpandList(expands);
  }, [strategySelected, resetFilter]);

  const handleClickExpand = (id: string, isExpand: boolean) => () => {
    setExpandList(list => ({ ...list, [id]: isExpand }));
  };

  return (
    <div className="position-relative">
      <div className="absolute-top-right mt-n26 mr-n8">
        <Button variant="outline-primary" size="sm" onClick={handleEdit}>
          {t('txt_edit')}
        </Button>
      </div>
      <div className="pt-16">
        {strategySelected?.map((strategy, idx) => {
          const isExpand = !!expandList[strategy.code];

          return (
            <div
              key={strategy.code}
              className={classnames(idx !== 0 && 'mt-24')}
            >
              <StrategySection
                strategy={strategy}
                isExpand={isExpand}
                resetFilter={resetFilter}
                onClickExpand={handleClickExpand(strategy.code, !isExpand)}
              />
            </div>
          );
        })}
      </div>
    </div>
  );
};

interface StrategySectionProps {
  isExpand: boolean;
  strategy: StrategyDetail;
  resetFilter: boolean;
  onClickExpand: VoidFunction;
}
const StrategySection: React.FC<StrategySectionProps> = ({
  isExpand,
  strategy,
  resetFilter,
  onClickExpand
}) => {
  const { t } = useTranslation();

  const { text: strategyname, methodList } = strategy;

  const {
    sort,
    total,
    currentPage,
    currentPageSize,
    gridData: dataView,
    onSortChange,
    onPageChange,
    onPageSizeChange,
    onResetToDefaultFilter
  } = usePagination(methodList, [], 'strategyName');

  useEffect(() => {
    if (!resetFilter) return;

    onResetToDefaultFilter();
  }, [resetFilter, onResetToDefaultFilter]);

  return (
    <>
      <div className="d-flex align-items-center justify-content-between">
        <div className="d-flex align-items-center">
          <Tooltip
            element={isExpand ? t('txt_collapse') : t('txt_expand')}
            opened={isDevice ? false : undefined}
            variant="primary"
            placement="top"
            triggerClassName="d-flex ml-n2"
          >
            <Button size="sm" variant="icon-secondary" onClick={onClickExpand}>
              <Icon name={isExpand ? 'minus' : 'plus'} size="4x" />
            </Button>
          </Tooltip>

          <strong className="ml-8">{strategyname}</strong>
        </div>
      </div>
      <div className={classnames(!isExpand && 'd-none')}>
        <div className="mt-16">
          <Grid
            dataItemKey="rowId"
            data={dataView}
            columns={columns(t, false)}
            sortBy={[sort]}
            onSortChange={onSortChange}
          />
          {total > 10 && (
            <div className="mt-16">
              <Paging
                page={currentPage}
                pageSize={currentPageSize}
                totalItem={total}
                onChangePage={onPageChange}
                onChangePageSize={onPageSizeChange}
              />
            </div>
          )}
        </div>
      </div>
    </>
  );
};

export default SelectPricingStrategySummary;
