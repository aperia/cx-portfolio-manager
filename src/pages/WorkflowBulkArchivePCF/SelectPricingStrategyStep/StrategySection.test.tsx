import { RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import * as helper from 'app/helpers/isDevice';
import { mockUseModalRegistryFnc, renderWithMockStore } from 'app/utils';
import React from 'react';
import StrategySection from './StrategySection';

export const strategies = [
  {
    code: 'A001',
    text: 'A001',
    methodList: [
      {
        rowId: '1',
        effectiveDate: '2022-12-14T00:00:00',
        strategyName: 'strategyName'
      },
      {
        rowId: '2',
        effectiveDate: '2022-12-14T00:00:00',
        strategyName: 'strategyName'
      },
      {
        rowId: '3',
        effectiveDate: '2022-12-14T00:00:00',
        strategyName: 'strategyName'
      },
      {
        rowId: '4',
        effectiveDate: '2022-12-14T00:00:00',
        strategyName: 'strategyName'
      }
    ],
    transactions: [
      {
        rowId: '1',
        effectiveDate: '2022-12-14T00:00:00',
        strategyName: 'strategyName'
      },
      {
        rowId: '2',
        effectiveDate: '2022-12-14T00:00:00',
        strategyName: 'strategyName'
      },
      {
        rowId: '3',
        effectiveDate: '2022-12-14T00:00:00',
        strategyName: 'strategyName'
      },
      {
        rowId: '4',
        effectiveDate: '2022-12-14T00:00:00',
        strategyName: 'strategyName'
      }
    ] as any[],
    filteredTransactions: [
      {
        id: '1',
        rowId: '1',
        effectiveDate: '2022-12-14T00:00:00',
        strategyName: 'strategyName1'
      },
      {
        id: '2',
        rowId: '2',
        effectiveDate: '2022-12-14T00:00:00',
        strategyName: 'strategyName2'
      },
      {
        id: '3',
        rowId: '3',
        effectiveDate: '2022-12-14T00:00:00',
        strategyName: 'strategyName3'
      },
      {
        id: '4',
        rowId: '4',
        effectiveDate: '2022-12-14T00:00:00',
        strategyName: 'strategyName4'
      }
    ]
  },
  {
    code: 'A002',
    text: 'A002',
    transactions: [
      {
        rowId: '5',
        effectiveDate: '2022-12-14T00:00:00',
        strategyName: 'strategyName'
      },
      {},
      {},
      {},
      {},
      {},
      {},
      {},
      {},
      {},
      {},
      {}
    ] as any[]
  },
  {
    code: 'A003',
    text: 'A003',
    transactions: [
      {
        rowId: '6',
        effectiveDate: '2022-12-14T00:00:00',
        strategyName: 'strategyName'
      }
    ] as any[]
  }
];
const mockUseModalRegistry = mockUseModalRegistryFnc();

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderComponent = async (
  props: any,
  workflowSetup?: any
): Promise<RenderResult> => {
  return await renderWithMockStore(<StrategySection {...props} />, {
    initialState: { workflowSetup }
  });
};
const mockHelper: any = helper;
describe('WorkflowBulkArchivePCF > SelectPricingStrategyStep > StrategySection', () => {
  beforeEach(() => {
    mockHelper.isDevice = false;

    mockUseModalRegistry.mockImplementation(() => [true, false]);
  });

  afterEach(() => {
    mockUseModalRegistry.mockClear();
  });

  const defaultWorkflowSetup = {
    workflowStrategies: { loading: true, error: false }
  };

  it('should render list', async () => {
    const props = {
      formValues: {},
      stepId: '1',
      selectedStep: '2',
      setFormValues: jest.fn(),
      clearSearchValue: jest.fn(),
      onClickExpand: jest.fn(),
      setSelectedList: jest.fn(),
      setSortBy: jest.fn(),
      selectedList: [],
      filterValue: 'txt_all',
      sortBy: { id: 'strategyName' },
      resetFilter: true,
      strategy: {
        ...strategies[0],
        filteredTransactions: strategies[0].transactions
      },

      strategies: strategies
    } as any;
    mockHelper.isDevice = true;

    const { getByText, getAllByRole, rerender } = await renderComponent(
      props,
      defaultWorkflowSetup
    );

    const checkboxAll = getAllByRole('checkbox')[0];
    userEvent.click(checkboxAll);

    userEvent.click(getByText('txt_workflow_bulk_archive_pcf_strategy_name'));
    userEvent.click(checkboxAll);

    rerender(
      <StrategySection
        {...props}
        selectedList={['1', '2']}
        isExpand={true}
        searchValue={'a'}
      />
    );
  });

  it('should render list without filteredTransactions', async () => {
    const props = {
      formValues: {},
      stepId: '1',
      selectedStep: '2',
      setFormValues: jest.fn(),
      clearSearchValue: jest.fn(),
      onClickExpand: jest.fn(),
      setSelectedList: jest.fn(),
      setSortBy: jest.fn(),
      selectedList: [],
      sortBy: { id: 'strategyName' },
      resetFilter: true,
      strategy: {
        ...strategies[0],
        filteredTransactions: strategies[0].filteredTransactions
      },

      strategies: strategies
    } as any;
    mockHelper.isDevice = true;

    const { getByText, getAllByRole, rerender } = await renderComponent(
      props,
      defaultWorkflowSetup
    );

    const checkboxAll = getAllByRole('checkbox')[0];

    userEvent.click(checkboxAll);
    userEvent.click(checkboxAll);

    userEvent.click(getByText('txt_workflow_bulk_archive_pcf_strategy_name'));

    rerender(
      <StrategySection
        {...props}
        selectedList={['1', '2']}
        isExpand={true}
        searchValue={'a'}
      />
    );
  });

  it('render Nodata', async () => {
    const props = {
      formValues: {
        strategies: [{ ...strategies[1] }] as any,
        pricingStrategies: [{ code: '123' }]
      },
      setFormValues: jest.fn(),
      clearSearchValue: jest.fn(),
      onClickExpand: jest.fn(),
      setSelectedList: jest.fn(),
      setSortBy: jest.fn(),
      selectedList: [],
      strategy: {
        ...strategies[1],
        methodList: [],
        filteredTransactions: strategies[1].transactions
      },

      strategies: strategies
    } as any;

    await renderComponent(props, defaultWorkflowSetup);
  });

  it('click hide strategy', async () => {
    const props = {
      formValues: {
        strategies: [{ ...strategies[1] }] as any,
        pricingStrategies: [{ code: '123' }]
      },
      setFormValues: jest.fn(),
      clearSearchValue: jest.fn(),
      onClickExpand: jest.fn(),
      setSelectedList: jest.fn(),
      setSortBy: jest.fn(),
      selectedList: [],
      strategy: {
        ...strategies[1],
        methodList: [
          {
            rowId: '1',
            effectiveDate: '2022-12-14T00:00:00',
            strategyName: 'strategyName'
          },
          {
            rowId: '2',
            effectiveDate: '2022-12-14T00:00:00',
            strategyName: 'strategyName'
          },
          {
            rowId: '3',
            effectiveDate: '2022-12-14T00:00:00',
            strategyName: 'strategyName'
          },
          {
            rowId: '4',
            effectiveDate: '2022-12-14T00:00:00',
            strategyName: 'strategyName'
          }
        ],
        filteredTransactions: strategies[1].transactions
      },

      strategies: strategies
    } as any;

    const { getByText } = await renderComponent(props, defaultWorkflowSetup);

    userEvent.click(
      getByText('txt_workflow_bulk_archive_pcf_hide_this_strategy_area')
    );
  });
});
