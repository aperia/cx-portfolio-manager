import { getWorkflowSetupStepStatus } from 'app/helpers';
import { StrategyDetail } from 'app/types';
import { FormSelectStrategy } from '.';
import { checkValidItem } from '../helper';

const parseFormValues: WorkflowSetupStepFormDataFunc<FormSelectStrategy> = (
  data,
  id
) => {
  const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);

  const config = (data?.data as any)?.configurations || {};

  if (!stepInfo || !config) return;

  const pricingStrategies =
    (config?.pricingStrategies as StrategyDetail[]) || [];

  const selectedList = {} as Record<string, string[]>;
  pricingStrategies?.forEach(element => {
    selectedList[element?.code] = element?.methodList
      ?.filter(i => i?.selected)
      .map(item => item?.rowId as string);
  });

  const isValid = checkValidItem(
    selectedList,
    pricingStrategies.map(m => m.code)
  );

  return {
    ...getWorkflowSetupStepStatus(stepInfo),
    isValid,
    pricingStrategies,
    selectedList
  };
};

export default parseFormValues;
