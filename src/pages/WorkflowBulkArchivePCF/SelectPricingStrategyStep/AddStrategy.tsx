import { useFormValidations } from 'app/hooks';
import {
  ComboBox,
  DropdownBaseChangeEvent,
  useTranslation
} from 'app/_libraries/_dls';
import { isFunction, isUndefined } from 'lodash';
import { useSelectElementsBulkArchivePCF } from 'pages/_commons/redux/WorkflowSetup';
import React, { useEffect, useMemo, useState } from 'react';
import { pricingStrategyTestId } from './constant';

interface IProps {
  isExistStrategy?: boolean;
  strategy?: RefData;
  onChangeStrategy?: (strategy: RefData) => void;
}

const AddStrategy: React.FC<IProps> = ({
  isExistStrategy = false,
  strategy,
  onChangeStrategy
}) => {
  const { t } = useTranslation();
  const { strategies } = useSelectElementsBulkArchivePCF();

  const [selectedStrategy, setSelectedStrategy] =
    useState<RefData | undefined>(strategy);
  useEffect(() => setSelectedStrategy(strategy), [strategy]);

  const currentErrors = useMemo(
    () =>
      ({
        strategy:
          (!selectedStrategy?.code?.trim() && {
            status: true,
            message: t('txt_required_validation', {
              fieldName: t('txt_workflow_bulk_archive_pcf_strategy_area')
            })
          }) ||
          (isExistStrategy && {
            status: true,
            message: t(
              'txt_workflow_bulk_archive_pcf_validation_exist_select_strategy'
            )
          })
      } as Record<'strategy', IFormError>),
    [selectedStrategy?.code, t, isExistStrategy]
  );
  const [, errors, setTouched] = useFormValidations<'strategy'>(currentErrors);

  const handleSelectStrategy = (e: DropdownBaseChangeEvent) => {
    if (isUndefined(strategy)) {
      setSelectedStrategy(e.target.value);
    }

    isFunction(onChangeStrategy) && onChangeStrategy(e.target.value);
  };

  return (
    <ComboBox
      textField="text"
      required
      value={selectedStrategy}
      label={t('txt_workflow_bulk_archive_pcf_strategy_area')}
      onChange={handleSelectStrategy}
      noResult={t('txt_no_results_found_without_dot')}
      onFocus={setTouched('strategy')}
      onBlur={setTouched('strategy', true)}
      error={errors.strategy}
    >
      {strategies.map(item => (
        <ComboBox.Item
          key={item.code}
          value={item}
          label={item.text}
          dataTestId={pricingStrategyTestId.dropdownElement}
        />
      ))}
    </ComboBox>
  );
};

export default AddStrategy;
