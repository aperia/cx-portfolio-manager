import { fireEvent, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { renderWithMockStore } from 'app/utils';
import mockDevice from 'app/utils/_mockHelperConstant/mockDevice';
import React from 'react';
import Summary from './Summary';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderComponent = async (
  props: any,
  workflowSetup?: any
): Promise<RenderResult> => {
  return await renderWithMockStore(<Summary {...props} />, {
    initialState: { workflowSetup }
  });
};

describe('WorkflowBulkArchivePCF > SelectPricingMethodStep > Summary', () => {
  beforeEach(() => {
    mockDevice.isDevice = false;
  });

  const defaultWorkflowSetup = {
    profileTransactions: { loading: true, error: false }
  };
  it('should render summary', async () => {
    const props = {
      formValues: {
        pricingStrategies: [
          {
            code: '001',
            text: '001',
            methodList: [
              { id: '1' },
              { id: '2' },
              { id: '3' },
              { id: '4' },
              { id: '5' },
              { id: '6' },
              { id: '7' },
              { id: '8' },
              { id: '9' },
              { id: '10' },
              { id: '11' }
            ] as any[]
          },
          { code: '002', text: '002', methodList: [] as any[] }
        ],
        selectedList: {
          ['001']: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11']
        }
      }
    } as any;
    const { baseElement, getByText, rerender } = await renderComponent(
      props,
      defaultWorkflowSetup
    );

    const colllapses = baseElement.querySelectorAll('.icon.icon-minus');
    colllapses.forEach(userEvent.click as any);
    // expand all pricingMethods list
    const expands = baseElement.querySelectorAll('.icon.icon-plus');
    expands.forEach(userEvent.click as any);

    rerender(<Summary {...props} stepId="step1" selectedStep="step2" />);

    expect(getByText('txt_edit')).toBeInTheDocument();
  });

  it('should render summary in device', async () => {
    mockDevice.isDevice = true;

    const props = {
      formValues: {
        pricingStrategies: [
          {
            code: '001',
            text: '001',
            methodList: [{ id: '1' }] as any[]
          },
          { code: '002', text: '002', methodList: [{ id: '2' }] as any[] }
        ],
        selectedList: {
          ['001']: ['1'],
          ['002']: ['2']
        }
      }
    } as any;
    const { baseElement, getByText, rerender } = await renderComponent(
      props,
      defaultWorkflowSetup
    );

    // expand all pricingMethods list
    let expands = baseElement.querySelectorAll('.icon.icon-plus');
    expands.forEach(userEvent.click as any);

    // expand all methodList
    expands = baseElement.querySelectorAll('.icon.icon-plus');
    expands.forEach(userEvent.click as any);

    rerender(<Summary {...props} stepId="step1" selectedStep="step2" />);

    expect(getByText('txt_edit')).toBeInTheDocument();
  });

  it('Should click on edit', async () => {
    const mockFn = jest.fn();
    const { getByText } = await renderComponent(
      {
        formValues: {
          pricingStrategies: [
            {
              code: '001',
              text: '001',
              methodList: [{ code: '001', text: '001' }]
            }
          ]
        },
        onEditStep: mockFn
      } as any,
      defaultWorkflowSetup
    );

    fireEvent.click(getByText('txt_edit'));
    expect(mockFn).toHaveBeenCalled();
  });

  it('Should render empty method', async () => {
    const mockFn = jest.fn();
    const { getByText } = await renderComponent(
      {
        formValues: {},
        onEditStep: mockFn
      } as any,
      defaultWorkflowSetup
    );

    expect(getByText('txt_edit')).toBeInTheDocument();
  });
});
