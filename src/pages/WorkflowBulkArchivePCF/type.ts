export enum ParameterCode {
  MethodName = 'service.subject.section',
  StrategyName = 'strategy.area',
  TextAreaName = 'text.area',
  TableAreaName = 'table.area'
}

export interface FilterOption {
  value: string;
  text: string;
}
