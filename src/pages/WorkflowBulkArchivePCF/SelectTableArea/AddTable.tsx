import { useFormValidations } from 'app/hooks';
import {
  ComboBox,
  DropdownBaseChangeEvent,
  useTranslation
} from 'app/_libraries/_dls';
import { isFunction, isUndefined } from 'lodash';
import { useSelectElementsBulkArchivePCF } from 'pages/_commons/redux/WorkflowSetup';
import React, { useEffect, useMemo, useState } from 'react';
import { pricingTableAreaTestId } from './constant';

interface IProps {
  isExistTableArea?: boolean;
  tableArea?: RefData;
  onChangeTableArea?: (tableArea: RefData) => void;
}

const AddTableArea: React.FC<IProps> = ({
  isExistTableArea = false,
  tableArea,
  onChangeTableArea
}) => {
  const { t } = useTranslation();
  const { tableAreas } = useSelectElementsBulkArchivePCF();

  const [selectedTableArea, setSelectedTableArea] =
    useState<RefData | undefined>(tableArea);
  useEffect(() => setSelectedTableArea(tableArea), [tableArea]);

  const currentErrors = useMemo(
    () =>
      ({
        tableArea:
          (!selectedTableArea?.code?.trim() && {
            status: true,
            message: t('txt_required_validation', {
              fieldName: t('txt_workflow_bulk_archive_pcf_table_area')
            })
          }) ||
          (isExistTableArea && {
            status: true,
            message: t(
              'txt_workflow_bulk_archive_pcf_validation_exist_select_table_area'
            )
          })
      } as Record<'tableArea', IFormError>),
    [selectedTableArea?.code, t, isExistTableArea]
  );
  const [, errors, setTouched] = useFormValidations<'tableArea'>(currentErrors);

  const handleSelectTableArea = (e: DropdownBaseChangeEvent) => {
    if (isUndefined(tableArea)) {
      setSelectedTableArea(e.target.value);
    }

    isFunction(onChangeTableArea) && onChangeTableArea(e.target.value);
  };

  return (
    <ComboBox
      textField="text"
      required
      value={selectedTableArea}
      label={t('txt_workflow_bulk_archive_pcf_table_area')}
      onChange={handleSelectTableArea}
      noResult={t('txt_no_results_found_without_dot')}
      onFocus={setTouched('tableArea')}
      onBlur={setTouched('tableArea', true)}
      error={errors.tableArea}
    >
      {tableAreas.map(item => (
        <ComboBox.Item
          key={item.code}
          value={item}
          label={item.text}
          dataTestId={pricingTableAreaTestId.dropdownElement}
        />
      ))}
    </ComboBox>
  );
};

export default AddTableArea;
