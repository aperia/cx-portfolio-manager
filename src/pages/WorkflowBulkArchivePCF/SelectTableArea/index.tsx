import {
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { classnames } from 'app/helpers';
import { useUnsavedChangeRegistry } from 'app/hooks';
import { TableAreaDetail } from 'app/types';
import { Button, InlineMessage, useTranslation } from 'app/_libraries/_dls';
import { isEqual } from 'app/_libraries/_dls/lodash';
import { orderBy } from 'lodash';
import {
  actionsWorkflowSetup,
  useTableAreaTransactionsStatus
} from 'pages/_commons/redux/WorkflowSetup';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, { useEffect, useMemo, useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import { ParameterCode } from '../type';
import AddTable from './AddTable';
import AddTableModal from './AddTableModal';
import { pricingTableAreaTestId } from './constant';
import InstructionModal from './InstructionModal';
import Summary from './Summary';
import TableAreaList from './TableList';
import parseFormValues from './_parseFormValues';

export interface FormSelectTableArea {
  isValid?: boolean;
  selectedList: Record<string, string[]>;
  tableIDs: TableAreaDetail[];
}

const SelectTableAreaStep: React.FC<WorkflowSetupProps<FormSelectTableArea>> =
  props => {
    const { setFormValues, formValues, savedAt, isStuck } = props;
    const [initialValues, setInitialValues] = useState(formValues);
    const { t } = useTranslation();
    const dispatch = useDispatch();

    const keepRef = useRef({ setFormValues });
    keepRef.current.setFormValues = setFormValues;

    const { loading } = useTableAreaTransactionsStatus();
    const [showAddTableModal, setShowAddTableModal] = useState(false);
    const [showInstructionModal, setShowInstructionModal] = useState(false);

    const existTableAreaIds = useMemo(
      () => formValues?.tableIDs?.map(m => m.code),
      [formValues?.tableIDs]
    );

    const hasTableArea = useMemo(
      () => existTableAreaIds.length > 0,
      [existTableAreaIds.length]
    );

    useEffect(() => {
      dispatch(
        actionsWorkflowSetup.getWorkflowMetadata([ParameterCode.TableAreaName])
      );
    }, [dispatch]);

    useEffect(() => {
      setInitialValues(formValues);
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [savedAt]);

    const formChanged = useMemo(
      () => !isEqual(formValues.tableIDs, initialValues.tableIDs),
      [formValues.tableIDs, initialValues.tableIDs]
    );

    useUnsavedChangeRegistry(
      {
        ...unsavedExistedWorkflowSetupDataProps,
        formName:
          UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP_BULK_ARCHIVE_PCF_SELECT_TABLE_AREA__EXISTED_WORKFLOW,
        priority: 1
      },
      [formChanged]
    );

    const handleSelectTableArea = async (tableArea: RefData) => {
      const transactionsResp = (await Promise.resolve(
        dispatch(
          actionsWorkflowSetup.getBulkArchivePCFWorkflowTableAreas({
            tableArea: tableArea.code
          })
        )
      )) as any;
      const isSuccess =
        actionsWorkflowSetup.getBulkArchivePCFWorkflowTableAreas.fulfilled.match(
          transactionsResp
        );

      if (isSuccess) {
        const _tableArea: TableAreaDetail = {
          ...tableArea,
          methodList: transactionsResp?.payload?.tableAreas?.map(
            (t: any, idx: number) => {
              const rowId = `${tableArea.code}_${idx}_${Date.now()}`;
              const transaction = {
                ...t,
                rowId: rowId,
                id: rowId,
                'table.id': t?.tableId,
                'effective.date': t?.effectiveDate
              };

              return transaction;
            }
          )
        };

        const _tableAreas = [_tableArea, ...formValues.tableIDs];

        setFormValues({ tableIDs: orderBy(_tableAreas, ['code']) });
      }
    };

    const handleOpenAddTableModal = () => {
      setShowAddTableModal(true);
    };

    const handleCloseAddTableModal = () => {
      setShowAddTableModal(false);
    };

    const instructionView = useMemo(
      () => (
        <>
          <div className="mt-8 pb-24 border-bottom color-grey">
            <p>
              {t('txt_workflow_bulk_archive_pcf_table_area_instruction')}{' '}
              <span
                className="link text-decoration-none px-4"
                onClick={() => setShowInstructionModal(true)}
                data-testid={pricingTableAreaTestId.viewInstructionModal}
              >
                {t('txt_workflow_bulk_archive_pcf_view_instruction')}
              </span>
            </p>
          </div>
          {isStuck && (
            <InlineMessage className="mb-0 mt-24" variant="danger" withIcon>
              {t('txt_step_stuck_move_forward_message')}
            </InlineMessage>
          )}
        </>
      ),
      // eslint-disable-next-line react-hooks/exhaustive-deps
      [isStuck]
    );

    return (
      <>
        {!hasTableArea && (
          <>
            {instructionView}
            <div className={classnames('mt-24', loading && 'loading')}>
              <h5>{t('txt_workflow_bulk_archive_pcf_select_table_area')}</h5>
              <div className="row mt-16">
                <div className="col-6 col-lg-4">
                  <AddTable onChangeTableArea={handleSelectTableArea} />
                </div>
              </div>
            </div>
          </>
        )}
        {hasTableArea && (
          <>
            <div
              className={classnames('position-relative', loading && 'loading')}
            >
              <div className="absolute-top-right mt-n32 mr-n8">
                <Button
                  variant="outline-primary"
                  size="sm"
                  onClick={handleOpenAddTableModal}
                >
                  {t('txt_workflow_bulk_archive_pcf_add_table_area')}
                </Button>
              </div>
              {instructionView}
            </div>
            <div className="mt-24">
              <TableAreaList
                tableAreas={formValues?.tableIDs}
                isValid={formValues.isValid}
                keepRef={keepRef}
                {...props}
              />

              {showAddTableModal && (
                <AddTableModal
                  existTableAreaIds={existTableAreaIds}
                  onClose={handleCloseAddTableModal}
                  onAdd={handleSelectTableArea}
                />
              )}
            </div>
          </>
        )}
        {showInstructionModal && (
          <InstructionModal
            opened={showInstructionModal}
            onClose={() => setShowInstructionModal(false)}
          />
        )}
      </>
    );
  };

const ExtraStaticSelectTableAreaStep =
  SelectTableAreaStep as WorkflowSetupStaticProp<FormSelectTableArea>;

ExtraStaticSelectTableAreaStep.summaryComponent = Summary;
ExtraStaticSelectTableAreaStep.defaultValues = {
  isValid: false,
  selectedList: {},
  tableIDs: []
};
ExtraStaticSelectTableAreaStep.parseFormValues = parseFormValues;

export default ExtraStaticSelectTableAreaStep;
