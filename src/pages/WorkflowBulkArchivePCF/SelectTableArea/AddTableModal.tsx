import ModalRegistry from 'app/components/ModalRegistry';
import {
  InlineMessage,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import { isFunction } from 'lodash';
import React, { useState } from 'react';
import AddTableArea from './AddTable';

export interface AddTableAreaModalProps {
  existTableAreaIds: string[];
  onClose?: VoidFunction;
  onAdd?: (tableArea: RefData) => void;
}

const AddTableAreaModal: React.FC<AddTableAreaModalProps> = ({
  existTableAreaIds,
  onClose,
  onAdd
}) => {
  const { t } = useTranslation();

  const [tableArea, setTableArea] = useState<RefData>();
  const [isExistTableArea, setIsExistTableArea] = useState(false);

  const handleAdd = () => {
    const _isExistTableArea =
      !!tableArea?.code && existTableAreaIds.includes(tableArea.code);
    if (_isExistTableArea) {
      setIsExistTableArea(_isExistTableArea);
      return;
    }

    isFunction(onAdd) && onAdd(tableArea!);
    isFunction(onClose) && onClose();
  };

  const handleChange = (t: RefData) => {
    setTableArea(t);
  };

  return (
    <ModalRegistry id="AddTableAreaModal" show onAutoClosedAll={onClose}>
      <ModalHeader border closeButton onHide={onClose}>
        <ModalTitle>
          {t('txt_workflow_bulk_archive_pcf_add_table_area')}
        </ModalTitle>
      </ModalHeader>
      <ModalBody>
        {isExistTableArea && (
          <InlineMessage withIcon variant="danger">
            {t(
              'txt_workflow_bulk_archive_pcf_validation_exist_select_table_area'
            )}
          </InlineMessage>
        )}
        <AddTableArea
          isExistTableArea={isExistTableArea}
          tableArea={tableArea}
          onChangeTableArea={handleChange}
        />
      </ModalBody>
      <ModalFooter
        cancelButtonText={t('txt_cancel')}
        okButtonText={t('txt_continue')}
        disabledOk={!tableArea}
        onCancel={onClose}
        onOk={handleAdd}
      />
    </ModalRegistry>
  );
};

export default AddTableAreaModal;
