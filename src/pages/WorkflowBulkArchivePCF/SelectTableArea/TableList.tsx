import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import { classnames, matchSearchValue } from 'app/helpers';
import { TableAreaDetail, TableAreaTransactions } from 'app/types';
import {
  DropdownBaseChangeEvent,
  DropdownList,
  SortType,
  useTranslation
} from 'app/_libraries/_dls';
import { differenceWith, isEmpty } from 'app/_libraries/_dls/lodash';
import ClearAndResetButton from 'pages/_commons/Utils/ClearAndResetButton';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { FormSelectTableArea } from '.';
import { checkValidItem, matchFilterTableArea } from '../helper';
import { FilterOption } from '../type';
import { tableAreaFilterOptions } from './constant';
import TableAreaSection from './TableAreaSection';

export interface FilterTableAreaDetail extends TableAreaDetail {
  filteredTransactions: TableAreaTransactions[];
}
export interface TableAreaListProps {
  tableAreas: TableAreaDetail[];
  keepRef: any;
  isValid?: boolean;
}

const TableAreaList: React.FC<
  WorkflowSetupProps<FormSelectTableArea> & TableAreaListProps
> = props => {
  const { t } = useTranslation();
  const { tableAreas, stepId, selectedStep, keepRef, isValid, formValues } =
    props;

  const prevRef = useRef({ tableAreas });
  const searchRef = useRef<any>(null);

  const [searchValue, setSearchValue] = useState<string>('');
  const [filterValue, setFilterValue] = useState<string>(t('txt_all'));
  const [shouldResetFilter, setShouldResetFilter] = useState(false);

  const [expandList, setExpandList] = useState<Record<string, boolean>>({});

  const [sortBy, setSortBy] = useState<Record<string, SortType>>({});

  const tableAreaOptions = tableAreaFilterOptions(t);

  const filteredTableAreas = useMemo(
    () =>
      tableAreas
        .map(p => {
          if (isEmpty(p.methodList)) return { ...p, filteredTransactions: [] };

          return {
            ...p,
            filteredTransactions: p.methodList.filter(
              w =>
                matchSearchValue([w.tableId], searchValue) &&
                matchFilterTableArea({
                  filterValue,
                  selectedList: formValues?.selectedList,
                  tableArea: w,
                  t
                })
            )
          } as FilterTableAreaDetail;
        })
        .filter(
          f =>
            f.filteredTransactions.length > 0 ||
            (!searchValue && filterValue === t('txt_all'))
        ),
    [tableAreas, searchValue, filterValue, formValues?.selectedList, t]
  );

  const handleExpandByDefault = useCallback(() => {
    const list: Record<string, boolean> = {};
    tableAreas.forEach(tableArea => {
      list[tableArea.code] = true;
    });
    setExpandList(list);
    setShouldResetFilter(true);
  }, [tableAreas]);

  const handleClearFilter = useCallback(() => {
    searchRef?.current?.clear();
    setSearchValue('');
    setFilterValue(t('txt_all'));
    handleExpandByDefault();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [handleExpandByDefault]);

  const handleSearch = (val: string) => {
    setSearchValue(val);
    handleExpandByDefault();
  };

  const handleFilter = (e: DropdownBaseChangeEvent) => {
    setFilterValue(e.target?.value);
  };

  useEffect(() => {
    if (stepId !== selectedStep) {
      setShouldResetFilter(true);
      handleClearFilter();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [stepId, selectedStep, handleClearFilter]);

  useEffect(() => {
    if (shouldResetFilter) {
      setShouldResetFilter(false);
    }
  }, [shouldResetFilter]);

  useEffect(() => {
    const checkValid = checkValidItem(
      formValues?.selectedList,
      tableAreas.map(s => s.code)
    );
    if (isValid === checkValid) return;
    keepRef.current.setFormValues({ isValid: checkValid });
  }, [isValid, tableAreas, keepRef, formValues?.selectedList]);

  useEffect(() => {
    const _prevRef = prevRef.current;
    const prevTableArea = _prevRef.tableAreas;
    // reset filter when add new method
    if (tableAreas.length > prevTableArea.length) {
      handleClearFilter();
    }
  }, [handleClearFilter, tableAreas.length]);

  useEffect(() => {
    const _prevRef = prevRef.current;
    const prevTableArea = _prevRef.tableAreas;

    const prevLength = prevTableArea.length;
    const currLength = tableAreas.length;

    const isFirstAdd = currLength === prevLength && currLength === 1;

    const diff = isFirstAdd
      ? tableAreas[0].code
      : differenceWith(
          tableAreas,
          prevTableArea,
          (o1, o2) => o1.code === o2.code
        )?.[0]?.code;

    if (!!diff) {
      setExpandList({ ...expandList, [diff]: true });
      _prevRef.tableAreas = tableAreas;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tableAreas]);

  const handleClickExpand = (id: string, isExpand: boolean) => () => {
    setExpandList(list => ({ ...list, [id]: isExpand }));
  };

  const handleClickSort = (id: string, sort: SortType) => {
    setSortBy(list => ({ ...list, [id]: sort }));
  };

  const handleSetSelectedList = (id: string, _list: string[]) => {
    keepRef.current.setFormValues({
      selectedList: { ...formValues?.selectedList, [id]: _list }
    });
  };

  return (
    <>
      <div className="d-flex align-items-center justify-content-between mb-16">
        <h5>{t('txt_workflow_bulk_archive_pcf_table_id_list')}</h5>
        <SimpleSearch
          ref={searchRef}
          placeholder={t('txt_workflow_bulk_archive_pcf_search_by_table_id')}
          onSearch={handleSearch}
        />
      </div>

      <div className="d-flex align-items-center">
        <strong className="fs-14 color-grey mr-4">{t('txt_filter')}:</strong>
        <DropdownList
          name="status"
          value={filterValue}
          textField="description"
          onChange={handleFilter}
          variant="no-border"
        >
          {tableAreaOptions.map((item: FilterOption) => (
            <DropdownList.Item
              key={item.value}
              label={item.text}
              value={item.text}
            />
          ))}
        </DropdownList>
      </div>

      {searchValue && !isEmpty(filteredTableAreas) && (
        <div className="d-flex justify-content-end mb-16 mr-n8">
          <ClearAndResetButton
            small
            onClearAndReset={() => handleClearFilter()}
          />
        </div>
      )}
      {isEmpty(filteredTableAreas) && (
        <div className="d-flex flex-column justify-content-center mt-40 mb-24">
          <NoDataFound
            id="newTableArea_notfound"
            hasSearch={!!searchValue}
            hasFilter={filterValue !== t('txt_all')}
            title={t(
              'txt_workflow_bulk_archive_pcf_table_id_list_no_results_found'
            )}
            linkTitle={t('txt_clear_and_reset')}
            onLinkClicked={() => handleClearFilter()}
          />
        </div>
      )}
      {!isEmpty(filteredTableAreas) && (
        <div className="mt-16">
          {filteredTableAreas.map((tableArea, idx) => {
            const isExpand = !!expandList[tableArea.code];

            return (
              <div
                key={tableArea.code}
                className={classnames(idx !== 0 && 'mt-24 pt-24 border-top')}
              >
                <TableAreaSection
                  {...props}
                  tableArea={tableArea}
                  isExpand={isExpand}
                  resetFilter={shouldResetFilter}
                  filterValue={filterValue}
                  searchValue={searchValue}
                  selectedList={formValues.selectedList[tableArea.code] || []}
                  sortBy={sortBy[tableArea.code]}
                  setSortBy={handleClickSort}
                  onClickExpand={handleClickExpand(tableArea.code, !isExpand)}
                  setSelectedList={handleSetSelectedList}
                />
              </div>
            );
          })}
        </div>
      )}
    </>
  );
};

export default TableAreaList;
