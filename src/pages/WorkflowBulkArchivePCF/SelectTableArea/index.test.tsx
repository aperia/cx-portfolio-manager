import { RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import {
  mockThunkAction,
  mockUseModalRegistryFnc,
  renderWithMockStore
} from 'app/utils';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowBulkArchivePCF';
import React from 'react';
import { ParameterCode } from '../type';
import { pricingTableAreaTestId } from './constant';
import SelectTableAreaStep, { FormSelectTableArea } from './index';

const mockUseModalRegistry = mockUseModalRegistryFnc();

HTMLCanvasElement.prototype.getContext = jest.fn();

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

jest.mock('./TableList', () => ({
  __esModule: true,
  default: () => <div data-testid="TableList">TableList</div>
}));

const MockSelectElementMetadataForTableList = jest.spyOn(
  WorkflowSetup,
  'useTableAreaTransactionsStatus'
);

const MockSelectElementMetadataForBulkArchivePCF = jest.spyOn(
  WorkflowSetup,
  'useSelectElementsBulkArchivePCF'
);

const renderComponent = async (
  props: any,
  workflowSetup?: any
): Promise<RenderResult> => {
  return await renderWithMockStore(<SelectTableAreaStep {...props} />, {
    initialState: { workflowSetup }
  });
};
const mockWorkflowSetup = mockThunkAction(actionsWorkflowSetup);

export const mockElements = [
  {
    name: ParameterCode.TableAreaName,
    options: [{ value: 'TableAreaName' }]
  }
];

describe('WorkflowBulkArchivePCF > SelectTableArea > Index', () => {
  beforeEach(() => {
    mockUseModalRegistry.mockReturnValue([true, false]);
    mockWorkflowSetup('getBulkArchivePCFWorkflowTableAreas', {
      match: true
    });
    MockSelectElementMetadataForTableList.mockReturnValue({
      loading: true,
      error: false
    });
    MockSelectElementMetadataForBulkArchivePCF.mockReturnValue({
      strategies: [],
      methods: [],
      tableAreas: [
        { code: 'table1', text: 'text1' },
        { code: 'table2', text: 'text2' }
      ],
      textAreas: []
    });
  });

  const defaultWorkflowSetup = {
    elementMetadata: { elements: mockElements },
    workflowMethods: { loading: true, error: false }
  };

  it('should render with empty table area', async () => {
    mockWorkflowSetup('getBulkArchivePCFWorkflowTableAreas', {
      match: true,
      payload: { payload: { tableAreas: [{}] } }
    });
    MockSelectElementMetadataForTableList.mockReturnValueOnce({
      loading: true
    });
    const setFormValues = jest.fn() as any;
    const props = {
      formValues: {
        tableIDs: []
      },
      setFormValues,
      isStuck: true
    } as unknown as WorkflowSetupProps<FormSelectTableArea>;

    const { getByText, getByTestId } = await renderComponent(
      props,
      defaultWorkflowSetup
    );

    userEvent.click(getByTestId(pricingTableAreaTestId.viewInstructionModal));
    userEvent.click(
      getByText('txt_workflow_bulk_archive_pcf_select_table_area')
    );
    userEvent.click(getByText('txt_workflow_bulk_archive_pcf_table_area'));
    userEvent.click(getByText('text1'));

    expect(
      getByText('txt_workflow_bulk_archive_pcf_select_table_area')
    ).toBeInTheDocument();
  });

  it('should add table area success', async () => {
    mockWorkflowSetup('getBulkArchivePCFWorkflowTableAreas', {
      match: true,
      payload: { payload: { tableAreas: [] } }
    });
    const setFormValues = jest.fn() as any;
    const props = {
      formValues: {
        tableIDs: [{ code: 'table1', text: 'text1', transactions: [] as any[] }]
      },
      setFormValues
    } as unknown as WorkflowSetupProps<FormSelectTableArea>;

    const { baseElement, getByText, getByTestId, getAllByTestId } =
      await renderComponent(props, defaultWorkflowSetup);
    expect(
      getByText('txt_workflow_bulk_archive_pcf_add_table_area')
    ).toBeInTheDocument();

    userEvent.click(getByTestId(pricingTableAreaTestId.viewInstructionModal));
    userEvent.click(getByText('txt_close'));
    userEvent.click(getByText('txt_workflow_bulk_archive_pcf_add_table_area'));
    expect(baseElement.querySelector('.dls-modal-root')).toBeInTheDocument();

    userEvent.click(getByText('txt_workflow_bulk_archive_pcf_table_area'));
    userEvent.click(
      getAllByTestId(
        `${pricingTableAreaTestId.dropdownElement}_dls-high-light-words`
      )[0]
    );
    userEvent.click(getByText('txt_workflow_bulk_archive_pcf_table_area'));
    userEvent.click(
      getAllByTestId(
        `${pricingTableAreaTestId.dropdownElement}_dls-high-light-words`
      )[0]
    );
    userEvent.click(getByText('txt_continue'));
  });

  it('should add method fail', async () => {
    mockWorkflowSetup('getBulkArchivePCFWorkflowTableAreas', {
      match: false
    });
    const setFormValues = jest.fn() as any;
    const props = {
      formValues: {
        tableIDs: [{ code: '001', text: '001', transactions: [] as any[] }]
      },
      setFormValues
    } as unknown as WorkflowSetupProps<FormSelectTableArea>;

    const { baseElement, getByText, getByTestId } = await renderComponent(
      props,
      defaultWorkflowSetup
    );
    expect(
      getByText('txt_workflow_bulk_archive_pcf_add_table_area')
    ).toBeInTheDocument();
    userEvent.click(getByTestId(pricingTableAreaTestId.viewInstructionModal));

    userEvent.click(getByText('txt_workflow_bulk_archive_pcf_add_table_area'));
    expect(baseElement.querySelector('.dls-modal-root')).toBeInTheDocument();
    userEvent.click(getByText('txt_cancel'));

    userEvent.click(getByText('txt_workflow_bulk_archive_pcf_add_table_area'));

    userEvent.click(getByText('txt_workflow_bulk_archive_pcf_table_area'));
    userEvent.click(getByText('text1'));

    userEvent.click(getByText('txt_continue'));
  });
});
