import { RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import * as helper from 'app/helpers/isDevice';
import { mockUseModalRegistryFnc, renderWithMockStore } from 'app/utils';
import React from 'react';
import TableAreaSection from './TableAreaSection';

const tableAreas = [
  {
    code: 'A001',
    text: 'A001',
    methodList: [
      {
        rowId: '1',
        effectiveDate: '2022-12-14T00:00:00',
        tableId: 'tableId'
      },
      {
        rowId: '2',
        effectiveDate: '2022-12-14T00:00:00',
        tableId: 'tableId'
      },
      {
        rowId: '3',
        effectiveDate: '2022-12-14T00:00:00',
        tableId: 'tableId'
      },
      {
        rowId: '4',
        effectiveDate: '2022-12-14T00:00:00',
        tableId: 'tableId'
      }
    ],
    transactions: [
      {
        rowId: '1',
        effectiveDate: '2022-12-14T00:00:00',
        tableId: 'tableId'
      },
      {
        rowId: '2',
        effectiveDate: '2022-12-14T00:00:00',
        tableId: 'tableId'
      },
      {
        rowId: '3',
        effectiveDate: '2022-12-14T00:00:00',
        tableId: 'tableId'
      },
      {
        rowId: '4',
        effectiveDate: '2022-12-14T00:00:00',
        tableId: 'tableId'
      }
    ] as any[],
    filteredTransactions: [
      {
        id: '1',
        rowId: '1',
        effectiveDate: '2022-12-14T00:00:00',
        tableId: 'tableId'
      },
      {
        id: '2',
        rowId: '2',
        effectiveDate: '2022-12-14T00:00:00',
        tableId: 'tableId'
      },
      {
        id: '3',
        rowId: '3',
        effectiveDate: '2022-12-14T00:00:00',
        tableId: 'tableId'
      },
      {
        id: '4',
        rowId: '4',
        effectiveDate: '2022-12-14T00:00:00',
        tableId: 'tableId'
      }
    ]
  },
  {
    code: 'A002',
    text: 'A002',
    transactions: [
      {
        rowId: '5',
        effectiveDate: '2022-12-14T00:00:00',
        tableId: 'tableId'
      },
      {},
      {},
      {},
      {},
      {},
      {},
      {},
      {},
      {},
      {},
      {}
    ] as any[]
  },
  {
    code: 'A003',
    text: 'A003',
    transactions: [
      {
        rowId: '6',
        effectiveDate: '2022-12-14T00:00:00',
        tableId: 'tableId'
      }
    ] as any[]
  }
];
const mockUseModalRegistry = mockUseModalRegistryFnc();

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderComponent = async (
  props: any,
  workflowSetup?: any
): Promise<RenderResult> => {
  return await renderWithMockStore(<TableAreaSection {...props} />, {
    initialState: { workflowSetup }
  });
};
const mockHelper: any = helper;
describe('WorkflowBulkArchivePCF > SelectTableArea > TableAreaSection', () => {
  beforeEach(() => {
    mockHelper.isDevice = false;

    mockUseModalRegistry.mockImplementation(() => [true, false]);
  });

  afterEach(() => {
    mockUseModalRegistry.mockClear();
  });

  const defaultWorkflowSetup = {
    workflowStrategies: { loading: true, error: false }
  };

  it('should render list', async () => {
    const props = {
      formValues: {},
      stepId: '1',
      selectedStep: '2',
      setFormValues: jest.fn(),
      clearSearchValue: jest.fn(),
      onClickExpand: jest.fn(),
      setSelectedList: jest.fn(),
      setSortBy: jest.fn(),
      selectedList: [],
      sortBy: { id: 'tableId' },
      filterValue: 'txt_all',
      resetFilter: true,
      tableArea: {
        ...tableAreas[0],
        filteredTransactions: tableAreas[0].transactions
      },

      tableAreas: tableAreas
    } as any;
    mockHelper.isDevice = true;

    const { getByText, getAllByRole, rerender } = await renderComponent(
      props,
      defaultWorkflowSetup
    );

    const checkboxAll = getAllByRole('checkbox')[0];
    userEvent.click(checkboxAll);

    userEvent.click(getByText('txt_workflow_bulk_archive_pcf_table_id'));

    rerender(
      <TableAreaSection
        {...props}
        selectedList={['1', '2']}
        isExpand={true}
        searchValue={'a'}
      />
    );
  });

  it('should render list without filteredTransactions', async () => {
    const props = {
      formValues: {},
      stepId: '1',
      selectedStep: '2',
      setFormValues: jest.fn(),
      clearSearchValue: jest.fn(),
      onClickExpand: jest.fn(),
      setSelectedList: jest.fn(),
      setSortBy: jest.fn(),
      selectedList: [],
      sortBy: { id: 'tableId' },
      resetFilter: true,
      filterValue: 'txt_all',
      tableArea: {
        ...tableAreas[0],
        filteredTransactions: tableAreas[0].filteredTransactions
      },

      tableAreas: tableAreas
    } as any;
    mockHelper.isDevice = true;

    const { getByText, getAllByRole, rerender } = await renderComponent(
      props,
      defaultWorkflowSetup
    );

    const checkboxAll = getAllByRole('checkbox')[0];

    userEvent.click(checkboxAll);
    userEvent.click(checkboxAll);

    userEvent.click(getByText('txt_workflow_bulk_archive_pcf_table_id'));

    rerender(
      <TableAreaSection {...props} selectedList={['1', '2']} isExpand={true} />
    );
  });

  it('render noData', () => {
    const props = {
      formValues: {
        tableAreas: [{ ...tableAreas[1] }] as any,
        tableIDs: [{ code: '123' }]
      },
      setFormValues: jest.fn(),
      clearSearchValue: jest.fn(),
      onClickExpand: jest.fn(),
      setSelectedList: jest.fn(),
      setSortBy: jest.fn(),
      selectedList: [],
      tableArea: {
        ...tableAreas[1],
        methodList: [],
        filteredTransactions: tableAreas[1].transactions
      },
      tableAreas: tableAreas
    } as any;

    renderComponent(props, defaultWorkflowSetup);
  });

  it('click hide tableArea', async () => {
    const props = {
      formValues: {
        tableAreas: [{ ...tableAreas[1] }] as any,
        tableIDs: [{ code: '123' }]
      },
      setFormValues: jest.fn(),
      clearSearchValue: jest.fn(),
      onClickExpand: jest.fn(),
      setSelectedList: jest.fn(),
      setSortBy: jest.fn(),
      selectedList: [],
      tableArea: {
        ...tableAreas[1],
        methodList: [
          {
            rowId: '1',
            effectiveDate: '2022-12-14T00:00:00',
            tableId: 'tableId'
          },
          {
            rowId: '2',
            effectiveDate: '2022-12-14T00:00:00',
            tableId: 'tableId'
          },
          {
            rowId: '3',
            effectiveDate: '2022-12-14T00:00:00',
            tableId: 'tableId'
          },
          {
            rowId: '4',
            effectiveDate: '2022-12-14T00:00:00',
            tableId: 'tableId'
          }
        ],
        filteredTransactions: tableAreas[1].transactions
      },
      tableAreas: tableAreas
    } as any;

    const { getByText } = await renderComponent(props, defaultWorkflowSetup);

    userEvent.click(
      getByText('txt_workflow_bulk_archive_pcf_hide_this_table_area')
    );
  });
});
