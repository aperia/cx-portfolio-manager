import ModalRegistry from 'app/components/ModalRegistry';
import {
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import React, { useMemo } from 'react';

export interface InstructionModalProps {
  onClose?: VoidFunction;
  opened: boolean;
}

const InstructionModal: React.FC<InstructionModalProps> = ({
  onClose,
  opened
}) => {
  const { t } = useTranslation();

  const modalBody = useMemo(() => {
    return (
      <>
        <p className="fw-600">
          {t('txt_workflow_bulk_archive_pcf_table_area_instruction_title_1')}
        </p>
        <ul className="list-dot">
          <li>
            {t('txt_workflow_bulk_archive_pcf_table_area_instruction_desc_1_1')}
          </li>
          <li>
            {t('txt_workflow_bulk_archive_pcf_table_area_instruction_desc_1_2')}
          </li>
          <li>
            {t('txt_workflow_bulk_archive_pcf_table_area_instruction_desc_1_3')}
          </li>
          <li>
            {t('txt_workflow_bulk_archive_pcf_table_area_instruction_desc_1_4')}
          </li>
        </ul>
      </>
    );
  }, [t]);

  return (
    <ModalRegistry
      id="InstructionModal"
      show={opened}
      onAutoClosedAll={onClose}
      md
      animationComponent="picture"
    >
      <ModalHeader border closeButton onHide={onClose}>
        <ModalTitle>
          {t('txt_workflow_bulk_archive_pcf_instruction_detail')}
        </ModalTitle>
      </ModalHeader>
      <ModalBody>{modalBody}</ModalBody>
      <ModalFooter cancelButtonText={t('txt_close')} onCancel={onClose} />
    </ModalRegistry>
  );
};

export default InstructionModal;
