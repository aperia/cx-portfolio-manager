import { ColumnType } from 'app/_libraries/_dls';
import { formatText } from 'pages/_commons/Utils/formatGridField';
import { FilterOption } from '../type';

export const columns = (t: any, isSort = true): ColumnType[] => {
  const cols = [
    {
      id: 'tableId',
      Header: t('txt_workflow_bulk_archive_pcf_table_id'),
      accessor: 'tableId',
      isSort
    },
    {
      id: 'effectiveDate',
      Header: t('txt_workflow_bulk_archive_pcf_effective_date'),
      accessor: formatText(['effectiveDate'], { format: 'date' }),
      isSort
    }
  ];
  return cols;
};

export const tableAreaFilterValues = {
  all: 'all',
  selected: 'selected',
  unSelected: 'Unselected'
};

export const tableAreaFilterOptions = (t: any): FilterOption[] => {
  return [
    {
      value: tableAreaFilterValues.all,
      text: t('txt_all')
    },
    {
      value: tableAreaFilterValues.selected,
      text: t('txt_workflow_bulk_archive_pcf_selected_table_ids')
    },
    {
      value: tableAreaFilterValues.unSelected,
      text: t('txt_workflow_bulk_archive_pcf_unselected_table_ids')
    }
  ];
};

export const pricingTableAreaTestId = {
  viewInstructionModal: 'viewInstructionModal',
  dropdownElement: 'methodDropdownElement'
};
