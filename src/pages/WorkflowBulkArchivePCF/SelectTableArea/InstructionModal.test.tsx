import { render, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { mockUseModalRegistryFnc, renderWithMockStore } from 'app/utils';
import React from 'react';
import InstructionModal from './InstructionModal';


const mockUseModalRegistry = mockUseModalRegistryFnc();

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderModal = (props: any): RenderResult => {
  return render(
    <div>
      <InstructionModal {...props} />
    </div>
  );
};

describe('InstructionModal', () => {
  beforeEach(() => {
    mockUseModalRegistry.mockImplementation(() => [true, false]);
  });

  afterEach(() => {
    mockUseModalRegistry.mockClear();
  });

  it('Should render modal', () => {
    const props = {
      opened: true,
      onClose: jest.fn()
    };

const wrapper = renderModal(
  <InstructionModal {...props}>txt_workflow_bulk_archive_pcf_instruction_detail</InstructionModal>
);
expect(wrapper.getAllByText('txt_workflow_bulk_archive_pcf_instruction_detail')).toHaveLength(1);
  });

  it('Should render modal body text 1', () => {
    const props = {
      opened: true,
      onClose: jest.fn()
    };

const wrapper = renderModal(
  <InstructionModal {...props}>txt_workflow_bulk_archive_pcf_table_area_instruction_desc_1_1</InstructionModal>
);
expect(wrapper.getAllByText('txt_workflow_bulk_archive_pcf_table_area_instruction_desc_1_1')).toHaveLength(1);
  });

  it('Should render modal body text 2', () => {
    const props = {
      opened: true,
      onClose: jest.fn()
    };

const wrapper = renderModal(
  <InstructionModal {...props}>txt_workflow_bulk_archive_pcf_table_area_instruction_desc_1_2</InstructionModal>
);
expect(wrapper.getAllByText('txt_workflow_bulk_archive_pcf_table_area_instruction_desc_1_2')).toHaveLength(1);
  });

  
  it('Should close modal', async () => {
    const onClose = jest.fn();
    const props = {
      id:"InstructionModal",
      opened:true,
      onAutoClosedAll:{onClose},
      animationComponent:"picture"
    };

    const wrapper =  renderWithMockStore(
      <InstructionModal {...props} />,
    );

    const cancelButtonText = (await wrapper).getByText('txt_close');
    userEvent.click(cancelButtonText);

    expect(onClose).toBeCalledTimes(0);
  });

});
