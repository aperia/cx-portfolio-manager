import { getWorkflowSetupStepStatus } from 'app/helpers';
import { TableAreaDetail } from 'app/types';
import { FormSelectTableArea } from '.';
import { checkValidItem } from '../helper';

const parseFormValues: WorkflowSetupStepFormDataFunc<FormSelectTableArea> = (
  data,
  id
) => {
  const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);
  const config = (data?.data as any)?.configurations || {};

  if (!stepInfo || !config) return;

  const tableIDs = (config?.tableIDs as TableAreaDetail[]) || [];

  const selectedList = {} as Record<string, string[]>;
  tableIDs?.forEach(element => {
    selectedList[element?.code] = element?.methodList
      ?.filter(i => i?.selected)
      .map(item => item?.rowId as string);
  });

  const isValid = checkValidItem(
    selectedList,
    tableIDs.map(m => m.code)
  );

  return {
    ...getWorkflowSetupStepStatus(stepInfo),
    isValid,
    tableIDs,
    selectedList
  };
};

export default parseFormValues;
