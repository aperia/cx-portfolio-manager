import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { default as stepRegistry } from '../_commons/WorkflowSetup/registries/stepRegistry';

const registerFunc = jest.spyOn(stepRegistry, 'registerStep');

describe('WorkflowBulkArchivePCF > registerSteps', () => {
  it('Should register components', async () => {
    await import('./registerSteps');

    expect(registerFunc).toBeCalledWith(
      'GetStartedWorkflowBulkArchivePCF',
      expect.anything(),
      [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP_BULK_ARCHIVE_PCF_GET_STARTED_EXISTED_WORKFLOW,
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP_BULK_ARCHIVE_PCF_GET_STARTED_EXISTED_WORKFLOW__STUCK
      ]
    );
  });
});
