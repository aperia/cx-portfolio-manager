import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { stepRegistry } from '../_commons/WorkflowSetup/registries';
import GettingStartStep from './GettingStartStep';
import SelectMonetaryTransactionStep from './SelectMonetaryTransactionsStep';

stepRegistry.registerStep(
  'GetStartedFlexibleMonetaryTransactions',
  GettingStartStep
);

stepRegistry.registerStep(
  'SelectMonetaryTransactionsFlexbileMonenetaryTransctions',
  SelectMonetaryTransactionStep,
  [
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__FLEXIBLE_MONETARY_TRANSACTIONS__SELECT_MONETARY_TRANSACTIONS
  ]
);
