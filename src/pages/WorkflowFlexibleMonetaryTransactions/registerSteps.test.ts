import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { default as stepRegistry } from '../_commons/WorkflowSetup/registries/stepRegistry';

const registerFunc = jest.spyOn(stepRegistry, 'registerStep');

describe('FlexibleMonetaryTransactions > registerSteps', () => {
  it('Should register components', async () => {
    await import('./registerSteps');

    expect(registerFunc).toBeCalledWith(
      'GetStartedFlexibleMonetaryTransactions',
      expect.anything()
    );

    expect(registerFunc).toBeCalledWith(
      'SelectMonetaryTransactionsFlexbileMonenetaryTransctions',
      expect.anything(),
      [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__FLEXIBLE_MONETARY_TRANSACTIONS__SELECT_MONETARY_TRANSACTIONS
      ]
    );
  });
});
