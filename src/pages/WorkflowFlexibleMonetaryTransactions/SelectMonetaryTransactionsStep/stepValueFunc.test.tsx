import { SelectMonetaryTransactionsFormValues } from '.';
import { stepValueFunc } from './stepValueFunc';

describe('WorkflowSetup > CommonsStep > ChangeInfoStep > stepValueFunc', () => {
  it('should return with 1 items', () => {
    const stepsForm = {
      transactions: [{ id: '1', text: 'name1', description: 'description1' }]
    } as SelectMonetaryTransactionsFormValues;

    const result = stepValueFunc({ stepsForm, t: (text: string) => text });
    expect(result).toBeTruthy();
  });

  it('should return with empty', () => {
    const stepsForm = {} as SelectMonetaryTransactionsFormValues;

    const result = stepValueFunc({ stepsForm, t: (text: string) => text });
    expect(result).toEqual('');
  });

  it('should return changeName', () => {
    const stepsForm = {
      transactions: [
        { id: '1', text: 'name1', description: 'description1' },
        { id: '2', text: 'name2', description: 'description2' },
        { id: '3', text: 'name3', description: 'description3' },
        { id: '4', text: 'name4', description: 'description4' }
      ],
      listLength: 5
    } as SelectMonetaryTransactionsFormValues;

    const result = stepValueFunc({ stepsForm, t: (text: string) => text });
    expect(result).toBeTruthy();
  });

  it('should return changeName with all transaction selected', () => {
    const stepsForm = {
      transactions: [
        { id: '1', text: 'name1', description: 'description1' },
        { id: '2', text: 'name2', description: 'description2' },
        { id: '3', text: 'name3', description: 'description3' },
        { id: '4', text: 'name4', description: 'description4' }
      ],
      listLength: 4
    } as SelectMonetaryTransactionsFormValues;

    const result = stepValueFunc({ stepsForm, t: (text: string) => text });
    expect(result).toBeTruthy();
  });
});
