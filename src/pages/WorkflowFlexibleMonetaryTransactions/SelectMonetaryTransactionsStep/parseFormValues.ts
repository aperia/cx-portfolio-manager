import { getWorkflowSetupStepStatus } from 'app/helpers';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import { SelectMonetaryTransactionsFormValues } from '.';

const parseFormValues: WorkflowSetupStepFormDataFunc<SelectMonetaryTransactionsFormValues> =
  (data, id) => {
    const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);
    const { transactions } = (data?.data as any)?.configurations || {};

    if (isEmpty(transactions) || !stepInfo) return;

    const isValid = !isEmpty(transactions);

    return {
      ...getWorkflowSetupStepStatus(stepInfo),
      ...stepInfo.data,
      isValid,
      transactionsProp: transactions
    };
  };

export default parseFormValues;
