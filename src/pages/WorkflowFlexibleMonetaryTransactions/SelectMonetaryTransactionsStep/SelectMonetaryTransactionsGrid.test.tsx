import { fireEvent, render, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { mockThunkAction, renderWithMockStore } from 'app/utils';
import { STATUS } from 'app/_libraries/_dls/components/Upload/helper';
import { queryAllByClass } from 'app/_libraries/_dls/test-utils';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import * as selector from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowFlexibleMonetaryTransactions';
import React from 'react';
import { act } from 'react-dom/test-utils';
import SelectMonetaryTransactionsGrid, {
  Header,
  SubGrid
} from './SelectMonetaryTransactionsGrid';

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

const mockUseSelectElementsForMonetaryTransactions = jest.spyOn(
  selector,
  'useSelectElementsForMonetaryTransactions'
);

jest.mock('app/hooks/useUnsavedChangeRegistry', () => {
  return {
    __esModule: true,
    useUnsavedChangeRegistry: (options: any, changes: any, onConfirm: any) => {
      onConfirm && onConfirm();
    }
  };
});

const renderSubGridComponent = (props: any): RenderResult => {
  return render(
    <div>
      <SubGrid {...props} />
    </div>
  );
};

const renderHeaderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <Header {...props} />
    </div>
  );
};

const renderComponent = (props: any, initValues: any = {}) => {
  return renderWithMockStore(
    <SelectMonetaryTransactionsGrid {...props} />,
    initValues
  );
};

const mockActionsWorkflowSetup = mockThunkAction(actionsWorkflowSetup);

const get1MockData = (i: number | string) => ({
  id: `${i}`,
  name: `testName${i}`,
  description: `description${i}`
});

const generateMockData = (length: number) => {
  const result = [];
  for (let i = 0; i < length; i++) {
    result.push(get1MockData(i));
  }

  return result;
};

beforeEach(() => {
  mockActionsWorkflowSetup('getWorkflowMetadata');
  mockUseSelectElementsForMonetaryTransactions.mockReturnValue({
    transactions: []
  });
});
describe('SubGrid Component', () => {
  it('Should render SubGrid with data ', () => {
    const props = {
      data: {
        parameters: [
          { id: '1', value: 'name', description: 'description' },
          { id: '2', value: 'name2', description: 'description2' }
        ]
      }
    } as any;
    const wrapper = renderSubGridComponent(props);

    expect(wrapper.getByText('name')).toBeInTheDocument();
  });

  it('Should render SubGrid no data ', () => {
    const props = {
      data: {}
    } as any;
    const wrapper = renderSubGridComponent(props);

    expect(wrapper.getByText('txt_no_data_found_grid')).toBeInTheDocument();
  });
});

describe('Header Component', () => {
  const onSearchMock = jest.fn();
  const onResetMock = jest.fn();
  it('Should render Header with listNumber = 1 and enableResetBtn is false ', () => {
    const props = {
      listNumber: 1,
      searchValue: 'searchValue',
      onSearch: onSearchMock,
      onReset: onResetMock,
      enableResetBtn: false,
      children: <div>Test</div>
    } as any;
    const wrapper = renderHeaderComponent(props);

    expect(wrapper.getByText('Test')).toBeInTheDocument();
  });

  it('Should render Header with listNumber = 1 and enableResetBtn default ', () => {
    const props = {
      listNumber: 1,
      searchValue: 'searchValue',
      onSearch: onSearchMock,
      onReset: onResetMock,
      children: <div>Test</div>
    } as any;
    const wrapper = renderHeaderComponent(props);

    expect(wrapper.getByText('Test')).toBeInTheDocument();
  });

  it('Should render Header with listNumber = 2 and enableResetBtn is true ', () => {
    const props = {
      listNumber: 2,
      searchValue: 'searchValue',
      onSearch: onSearchMock,
      onReset: onResetMock,
      isCheckAll: false,
      enableResetBtn: true
    } as any;
    const wrapper = renderHeaderComponent(props);
    const resetButton = wrapper.getByText('txt_clear_and_reset');

    fireEvent.click(resetButton);
    expect(resetButton).toBeInTheDocument();
  });

  it('Should render Header with listNumber = 2 and enableResetBtn is true and isCheckAll is true', () => {
    const props = {
      listNumber: 2,
      searchValue: 'searchValue',
      onSearch: onSearchMock,
      onReset: onResetMock,
      isCheckAll: true,
      enableResetBtn: true
    } as any;
    const wrapper = renderHeaderComponent(props);
    const resetButton = wrapper.getByText('txt_clear_and_reset');

    fireEvent.click(resetButton);
    expect(resetButton).toBeInTheDocument();
  });
});
const mockSetFormValues = jest.fn();

describe('SelectMonetaryTransactionsGrid Component', () => {
  it('render with loading', async () => {
    const getWorkflowMetadata = mockActionsWorkflowSetup('getWorkflowMetadata');
    const props = {
      stepId: 'id',
      selectedStep: 'id1',
      savedAt: 1,
      formValues: { isValid: true },

      dependencies: {
        files: [
          {
            status: STATUS.valid
          }
        ]
      },
      setFormValues: mockSetFormValues,
      clearFormValues: mockSetFormValues
    };
    jest.useFakeTimers();
    await renderComponent(props, {});
    jest.runAllTimers();
    expect(getWorkflowMetadata).toHaveBeenCalled();
  });

  it('render with no data', async () => {
    const props = {
      stepId: 'id',
      selectedStep: 'id1',
      savedAt: 1,
      formValues: { isValid: true },

      dependencies: {},
      setFormValues: mockSetFormValues
    };
    jest.useFakeTimers();
    const wrapper = await renderComponent(props, {});
    jest.runAllTimers();

    expect(wrapper.getByText('txt_no_data_found')).toBeInTheDocument();

    act(() => {
      userEvent.click(wrapper.getByText('txt_no_data_found'));
    });
  });

  it('render with data with 10', async () => {
    const transactions = generateMockData(11) as any;
    mockUseSelectElementsForMonetaryTransactions.mockReturnValue({
      transactions
    });
    const props = {
      stepId: 'id',
      selectedStep: 'id',
      savedAt: 1,
      formValues: { isValid: true, transactions },

      dependencies: { attachmentId: '123123123' },
      setFormValues: mockSetFormValues
    };

    jest.useFakeTimers();
    const wrapper = await renderComponent(props, {});
    jest.runAllTimers();
    expect(
      wrapper.getByText('txt_number_transactions_selected')
    ).toBeInTheDocument();
  });

  it('render with data without click grid checkbox', async () => {
    const transactions = generateMockData(1) as any;
    mockUseSelectElementsForMonetaryTransactions.mockReturnValue({
      transactions
    });
    const props = {
      stepId: 'id',
      selectedStep: 'id',
      savedAt: 1,
      formValues: { isValid: true, transactions },

      dependencies: { attachmentId: '123123123' },
      setFormValues: mockSetFormValues
    };

    jest.useFakeTimers();
    const wrapper = await renderComponent(props, {});
    jest.runAllTimers();
    expect(
      wrapper.getByText('txt_number_transactions_selected')
    ).toBeInTheDocument();
  });

  it('render with data with click grid checkbox', async () => {
    const transactions = generateMockData(5) as any;
    mockUseSelectElementsForMonetaryTransactions.mockReturnValue({
      transactions
    });
    const props = {
      stepId: 'id',
      selectedStep: 'id',
      savedAt: 1,
      formValues: {
        isValid: true,
        transactions: [
          { id: '123', name: 'name1', description: 'description1' },
          { id: '345', name: 'name2', description: 'description2' }
        ]
      },

      dependencies: { attachmentId: '123123123' },
      setFormValues: mockSetFormValues
    };

    jest.useFakeTimers();
    const wrapper = await renderComponent(props, {});
    jest.runAllTimers();

    act(() => {
      userEvent.click(wrapper.getAllByRole('checkbox')[1]);
      userEvent.click(wrapper.getAllByRole('checkbox')[2]);
      userEvent.click(wrapper.getAllByRole('checkbox')[3]);
      userEvent.click(wrapper.getAllByRole('checkbox')[4]);
    });

    setTimeout(() => {
      expect(
        wrapper.getByText('txt_all_transactions_selected')
      ).toBeInTheDocument();
    }, 2);
  });

  it('render with data and search value', async () => {
    const transactions = generateMockData(1) as any;
    mockUseSelectElementsForMonetaryTransactions.mockReturnValue({
      transactions
    });
    const props = {
      stepId: 'id',
      selectedStep: 'id',
      savedAt: 1,
      formValues: { isValid: true, transactions },

      dependencies: { attachmentId: '123123123' },
      setFormValues: mockSetFormValues
    };

    const wrapper = await renderComponent(props, {});

    const search = wrapper.getByPlaceholderText(
      'txt_type_to_search_for_transaction'
    );
    fireEvent.change(search, { target: { value: 'asdasdasdasd' } });
    fireEvent.click(
      wrapper.baseElement.querySelector(
        'i[class="icon icon-search"]'
      ) as Element
    );

    expect(wrapper.getByText('txt_clear_and_reset')).toBeInTheDocument();
  });

  it('check all with user faketimer', async () => {
    const transactions = generateMockData(3) as any;
    mockUseSelectElementsForMonetaryTransactions.mockReturnValue({
      transactions
    });

    const props = {
      stepId: 'id',
      selectedStep: 'id',
      savedAt: 1,
      formValues: { isValid: true },

      dependencies: { attachmentId: '123123123' },
      setFormValues: mockSetFormValues
    };

    jest.useFakeTimers();
    const wrapper = await renderComponent(props, {});

    const checkboxAll = wrapper.getAllByRole('checkbox')[0];

    act(() => {
      userEvent.click(checkboxAll);
    });

    jest.runAllTimers();

    expect(
      wrapper.getByText('txt_all_transactions_selected')
    ).toBeInTheDocument();
  });

  it('check all without user faketimer', async () => {
    const transactions = generateMockData(3) as any;
    mockUseSelectElementsForMonetaryTransactions.mockReturnValue({
      transactions
    });

    const props = {
      stepId: 'id',
      selectedStep: 'id',
      savedAt: 1,
      formValues: { isValid: true },

      dependencies: { attachmentId: '123123123' },
      setFormValues: mockSetFormValues
    };

    const wrapper = await renderComponent(props, {});

    const checkboxAll = wrapper.getAllByRole('checkbox')[0];

    act(() => {
      userEvent.click(checkboxAll);
    });

    expect(
      wrapper.getByText('txt_all_transactions_selected')
    ).toBeInTheDocument();
  });

  it('expandbtn', async () => {
    const transactions = generateMockData(1) as any;
    mockUseSelectElementsForMonetaryTransactions.mockReturnValue({
      transactions
    });

    const props = {
      stepId: 'id',
      selectedStep: 'id',
      savedAt: 1,
      formValues: { isValid: true },

      dependencies: {},
      setFormValues: mockSetFormValues
    };

    jest.useFakeTimers();
    const wrapper = await renderComponent(props, {});
    jest.runAllTimers();
    const btnExpand = queryAllByClass(
      wrapper.container,
      /btn btn-icon-secondary btn-sm/
    )[0];
    act(() => {
      userEvent.click(btnExpand);
    });
    expect(
      wrapper.getByText('txt_number_transactions_selected')
    ).toBeInTheDocument();
  });

  it('parse transaction to selected item', async () => {
    const transactions = generateMockData(4) as any;
    mockUseSelectElementsForMonetaryTransactions.mockReturnValue({
      transactions
    });

    const props = {
      stepId: 'id',
      selectedStep: 'id',
      savedAt: 1,
      formValues: { isValid: true, transactionsProp: ['1', '2'] },

      dependencies: {},
      setFormValues: mockSetFormValues
    };

    jest.useFakeTimers();
    const wrapper = await renderComponent(props, {});
    jest.runAllTimers();
    expect(mockSetFormValues).toBeCalled();
    expect(
      wrapper.getByText('txt_number_transactions_selected')
    ).toBeInTheDocument();
  });
});
