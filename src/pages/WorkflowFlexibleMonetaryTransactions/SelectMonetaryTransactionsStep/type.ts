export enum ParameterCode {
  TransactionId = 'transaction.id',

  Transaction253 = '253.transaction',
  Transaction254 = '254.transaction',
  Transaction255 = '255.transaction',
  Transaction271 = '271.transaction'
}
