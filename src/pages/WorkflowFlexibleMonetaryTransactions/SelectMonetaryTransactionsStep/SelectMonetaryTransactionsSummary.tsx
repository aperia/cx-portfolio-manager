import { usePaginationGrid } from 'app/hooks';
import { Button, ColumnType, Grid, useTranslation } from 'app/_libraries/_dls';
import { isFunction, orderBy } from 'app/_libraries/_dls/lodash';
import Paging from 'pages/_commons/Utils/Paging';
import { DEFAULT_SORT } from 'pages/_commons/Utils/SortOrder';
import React, { useMemo } from 'react';
import { SelectMonetaryTransactionsFormValues } from '.';

const SelectMonetaryTransactionsSummary: React.FC<
  WorkflowSetupSummaryProps<SelectMonetaryTransactionsFormValues>
> = ({ formValues, onEditStep }) => {
  const { t } = useTranslation();

  const selectedData = useMemo(() => {
    return orderBy(
      formValues?.transactions || [],
      DEFAULT_SORT.id,
      DEFAULT_SORT.order
    );
  }, [formValues?.transactions]);

  const columnDefault: ColumnType[] = useMemo(
    () => [
      {
        id: 'text',
        Header: t('txt_flexible_monetary_trans_selected'),
        accessor: 'text',
        isSort: false
      }
    ],
    [t]
  );

  const {
    total,
    currentPage,
    currentPageSize,
    pageData,
    onPageChange,
    onPageSizeChange
  } = usePaginationGrid(selectedData);

  const handleEdit = () => {
    isFunction(onEditStep) && onEditStep();
  };

  return (
    <div className="position-relative">
      <div className="absolute-top-right mt-n26 mr-n8">
        <Button variant="outline-primary" size="sm" onClick={handleEdit}>
          {t('txt_edit')}
        </Button>
      </div>
      <div className="pt-16">
        <Grid columns={columnDefault} data={pageData} />
        {total > 10 && (
          <Paging
            totalItem={total}
            pageSize={currentPageSize}
            page={currentPage}
            onChangePage={onPageChange}
            onChangePageSize={onPageSizeChange}
          />
        )}
      </div>
    </div>
  );
};

export default SelectMonetaryTransactionsSummary;
