/* eslint-disable react/display-name */
import NoDataFound from 'app/components/NoDataFound';
import NoDataFoundGrid from 'app/components/NoDataFoundGrid';
import SimpleSearch from 'app/components/SimpleSearch';
import {
  confirmEditWhenChangeEffectToUploadProps,
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import {
  convertFilterArrayKeyToFilterArrayObject,
  mapGridExpandCollapse,
  mappingArrayKey
} from 'app/helpers';
import {
  useCheckAllPagination,
  useFunctionGrid,
  usePaginationGrid,
  useUnsavedChangeRegistry
} from 'app/hooks';
import { Button, ColumnType, Grid, useTranslation } from 'app/_libraries/_dls';
import { STATUS } from 'app/_libraries/_dls/components/Upload/helper';
import { differenceWith, uniq } from 'lodash';
import isEmpty from 'lodash.isempty';
import {
  actionsWorkflowSetup,
  useSelectElementsForMonetaryTransactions
} from 'pages/_commons/redux/WorkflowSetup';
import Paging from 'pages/_commons/Utils/Paging';
import React, { useEffect, useMemo, useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import { SelectMonetaryTransactionsProps } from '.';
import { ParameterCode } from './type';
export interface HeaderFMTType {
  listNumber: number;
  searchValue: string;
  onSearch: (value: string) => void;
  onReset?: () => void;
  enableResetBtn?: boolean;
  isCheckAll?: boolean;
}

export const SubGrid: React.FC<MagicKeyValue> = ({ data }) => {
  const { parameters = [] } = data;

  const { t } = useTranslation();
  const columnSubDefault: ColumnType[] = useMemo(
    () => [
      {
        id: 'value',
        Header: t('txt_field_name'),
        accessor: 'value',
        width: 213
      },
      {
        id: 'type',
        Header: t('txt_field_type'),
        accessor: 'type',
        width: 133
      },
      {
        id: 'description',
        Header: t('txt_description'),
        accessor: 'description'
      }
    ],
    [t]
  );

  return !isEmpty(parameters) ? (
    <div style={{ padding: '2rem' }}>
      <Grid columns={columnSubDefault} data={parameters} />
    </div>
  ) : (
    <NoDataFoundGrid />
  );
};

export const Header: React.FC<HeaderFMTType> = ({
  children,
  listNumber,
  searchValue,
  onSearch,
  onReset,
  isCheckAll = false,
  enableResetBtn = false
}) => {
  const { t } = useTranslation();
  return (
    <>
      <div className="d-flex align-items-center">
        <h5>{t('txt_monetary_transactions_list')}</h5>
        <div className="ml-auto">
          <SimpleSearch
            defaultValue={searchValue}
            placeholder={`${t('txt_type_to_search_for_transaction')}`}
            onSearch={onSearch}
          />
        </div>
      </div>

      <div className="d-flex align-items-center my-16">
        {!searchValue && (
          <div className="flex-1 py-4">
            {isCheckAll
              ? t('txt_all_transactions_selected')
              : listNumber !== 1
              ? t('txt_number_transactions_selected', { listNumber })
              : t('txt_number_transaction_selected', { listNumber })}
          </div>
        )}
        {searchValue && enableResetBtn && (
          <div className="ml-auto mr-n8">
            <Button
              id="flexible-monetary-transactions__btn"
              variant="outline-primary"
              size="sm"
              onClick={onReset}
            >
              {t('txt_clear_and_reset')}
            </Button>
          </div>
        )}
      </div>
      {children}
    </>
  );
};

const SelectMonetaryTransactionsGrid: React.FC<SelectMonetaryTransactionsProps> =
  ({
    savedAt,
    stepId,
    selectedStep,
    setFormValues,
    clearFormValues,
    clearFormName,
    formValues,
    dependencies
  }) => {
    const keepRef = useRef({ setFormValues, isLoadedAllTransaction: false });
    keepRef.current.setFormValues = setFormValues;

    const { t } = useTranslation();

    const dispatch = useDispatch();

    const { transactions } = useSelectElementsForMonetaryTransactions();

    const columnDefault: ColumnType[] = useMemo(
      () => [
        {
          id: 'text',
          Header: t('txt_monetary_transactions'),
          accessor: 'text',
          isSort: true
        }
      ],
      [t]
    );

    const {
      checkedList,
      expandedList,
      filterData,
      searchValue,
      gridData,
      sortBy,
      hasFilter,
      isCheckAll,
      pageCheckedList,
      onSetCheckedList,
      onSetPageCheckedList,
      onCheck,
      onSort,
      onResetSort,
      onSetGridData,
      onClearData,
      onExpand,
      onClearFilter,
      onResetFilter,
      onSearch,
      onSetFilterData,
      onSetIsCheckAll,
      onSetHasChange,
      onClearExpand
    } = useFunctionGrid({ defaultSortKey: 'text', defaultOrder: undefined });

    const {
      total,
      currentPage,
      currentPageSize,
      pageData,
      onPageChange,
      onPageSizeChange,
      onResetToDefaultFilter
    } = usePaginationGrid(hasFilter ? filterData : gridData);

    useEffect(() => {
      onSetGridData(transactions);
    }, [onSetGridData, transactions]);

    const listNumber = checkedList.length || 0;

    const hasSearchValue = !!searchValue;

    const [initialForm, setInitialForm] = useState(
      formValues.transactionsProp || []
    );
    const hasChange = !(
      isEmpty(differenceWith(initialForm, checkedList)) &&
      isEmpty(differenceWith(checkedList, initialForm!))
    );

    useEffect(() => {
      if (selectedStep === stepId) {
        onResetToDefaultFilter();
        onResetSort({ id: 'name', order: undefined });
        onClearData();
      }
    }, [
      selectedStep,
      stepId,
      onResetToDefaultFilter,
      onResetSort,
      onClearData
    ]);

    useEffect(() => {
      const transactionIds = (formValues?.transactions || []).map(
        item => item.id
      );
      setInitialForm(transactionIds);
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [savedAt]);

    useEffect(() => {
      if (stepId !== selectedStep) {
        onClearExpand();
      }
      onSetHasChange(false);
    }, [onClearExpand, onSetHasChange, stepId, selectedStep, savedAt]);

    useEffect(() => {
      const allPageIds = mappingArrayKey(pageData, 'id');
      onSetPageCheckedList(isCheckAll ? allPageIds : checkedList);
    }, [onSetPageCheckedList, isCheckAll, checkedList, pageData]);

    const { gridClassName, handleClickCheckAll } = useCheckAllPagination(
      {
        gridClassName: 'montery-transaction',
        allIds: transactions.map(t => t.code),
        hasFilter,
        filterIds: filterData.map(f => f.id),
        checkedList,
        setCheckAll: onSetIsCheckAll,
        setCheckList: onSetCheckedList
      },
      [searchValue, sortBy, currentPageSize, currentPageSize]
    );

    useUnsavedChangeRegistry(
      {
        ...unsavedExistedWorkflowSetupDataProps,
        formName:
          UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__FLEXIBLE_MONETARY_TRANSACTIONS__SELECT_MONETARY_TRANSACTIONS,
        priority: 1
      },
      [hasChange]
    );

    const unSaveOptions = useMemo(
      () => ({
        ...confirmEditWhenChangeEffectToUploadProps,
        formName:
          UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__FLEXIBLE_MONETARY_TRANSACTIONS__SELECT_MONETARY_TRANSACTIONS_CONFIRM_EDIT,
        priority: 1
      }),
      []
    );

    const handleResetUploadStep = () => {
      const file = dependencies?.files?.[0];
      const attachmentId = file?.idx;
      const isValid = file?.status === STATUS.valid;

      typeof clearFormValues === 'function' && clearFormValues(clearFormName);
      isValid &&
        dispatch(actionsWorkflowSetup.deleteTemplateFile({ attachmentId }));
    };
    useUnsavedChangeRegistry(
      unSaveOptions,
      [!isEmpty(dependencies?.files) && hasChange],
      handleResetUploadStep
    );

    useEffect(() => {
      dispatch(
        actionsWorkflowSetup.getWorkflowMetadata([ParameterCode.TransactionId])
      );
      onClearData();
    }, [dispatch, onClearData]);
    useEffect(() => {
      if (isEmpty(transactions) || keepRef.current.isLoadedAllTransaction) {
        return;
      }

      keepRef.current.isLoadedAllTransaction = true;
      dispatch(
        actionsWorkflowSetup.getWorkflowMetadata(
          uniq(transactions.map(t => `${t.code}.transaction`))
        )
      );
    }, [dispatch, transactions]);

    useEffect(() => {
      if (isEmpty(formValues?.transactionsProp) || isEmpty(gridData)) return;
      const newCheckedList = gridData.filter(item =>
        formValues.transactionsProp?.includes(item.id.toString())
      );
      const newCheckedListIds = newCheckedList.map(item => item.id);
      onSetCheckedList(newCheckedListIds);
      setInitialForm(newCheckedListIds);
      keepRef.current.setFormValues({ ...formValues, transactionsProp: [] });
    }, [formValues, gridData, onSetCheckedList]);

    useEffect(() => {
      const transactions = convertFilterArrayKeyToFilterArrayObject(gridData, {
        id: checkedList
      });
      keepRef.current.setFormValues({
        transactions
      });
    }, [checkedList, gridData]);

    useEffect(() => {
      if (selectedStep === stepId) {
        const isValid = checkedList.length > 0;
        if (isValid === formValues.isValid) return;

        keepRef.current.setFormValues({
          isValid,
          listLength: gridData.length
        });
      }
    }, [checkedList, gridData, formValues, selectedStep, stepId]);

    useEffect(() => {
      if (isEmpty(searchValue)) {
        onClearFilter();
      }
      onSetFilterData();
    }, [searchValue, onClearFilter, onSetFilterData]);

    if ((searchValue && isEmpty(filterData)) || gridData?.length <= 0) {
      return (
        <>
          <Header
            listNumber={listNumber}
            onSearch={onSearch}
            searchValue={searchValue}
            isCheckAll={isCheckAll}
          >
            <NoDataFound
              id="flexible-monetary-transactions-NoDataFound"
              title={t('txt_no_data_found')}
              linkTitle={t('txt_clear_and_reset')}
              hasSearch={hasSearchValue}
              onLinkClicked={onResetFilter}
              isFullPage={false}
              className="mt-40 mb-32"
            />
          </Header>
        </>
      );
    }

    return (
      <>
        <Header
          listNumber={listNumber}
          onSearch={onSearch}
          searchValue={searchValue}
          enableResetBtn={true}
          onReset={onResetFilter}
          isCheckAll={isCheckAll}
        >
          <Grid
            togglable
            columns={columnDefault}
            className={gridClassName}
            data={pageData}
            subRow={({ original }: MagicKeyValue) => (
              <SubGrid data={original} />
            )}
            sortBy={[sortBy]}
            onSortChange={onSort}
            dataItemKey="id"
            expandedItemKey="id"
            expandedList={expandedList}
            onExpand={onExpand}
            variant={{
              id: 'id',
              type: 'checkbox',
              cellHeaderProps: { onClick: handleClickCheckAll }
            }}
            checkedList={pageCheckedList}
            onCheck={el => onCheck(el, pageData)}
            toggleButtonConfigList={pageData.map(
              mapGridExpandCollapse('id', t)
            )}
          />
          {total > 10 && (
            <Paging
              totalItem={total}
              pageSize={currentPageSize}
              page={currentPage}
              onChangePage={onPageChange}
              onChangePageSize={onPageSizeChange}
            />
          )}
        </Header>
      </>
    );
  };

export default SelectMonetaryTransactionsGrid;
