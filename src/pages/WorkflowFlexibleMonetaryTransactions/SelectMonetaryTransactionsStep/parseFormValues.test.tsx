import parseFormValues from './parseFormValues';

describe('WorkflowFlexibleMonetaryTransactions > SelectMonetaryTransactionsStep > parseFormValues', () => {
  it('Should return undefined', () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '2'
            }
          ] as any,
          configurations: {
            transactions: []
          } as any
        }
      } as WorkflowSetupInstance,
      id: '1'
    };

    const response = parseFormValues(props.data, props.id);
    expect(response).toEqual(undefined);
  });

  it('Should return empty object', () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '2'
            }
          ] as any
        }
      } as WorkflowSetupInstance,
      id: '1'
    };

    const response = parseFormValues(props.data, props.id);
    expect(response).toEqual(undefined);
  });

  it('Should return data', () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '1'
            }
          ] as any,
          configurations: {
            transactions: ['1', '2']
          } as any
        }
      } as WorkflowSetupInstance,
      id: '1'
    };

    const response = parseFormValues(props.data, props.id);
    expect(response).toEqual({
      isPass: false,
      isSelected: false,
      isValid: true,
      transactionsProp: ['1', '2']
    });
  });
});
