import { fireEvent, render, RenderResult } from '@testing-library/react';
import React from 'react';
import SelectMonetaryTransactionsSummary from './SelectMonetaryTransactionsSummary';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const get1MockData = (i: number | string) => ({
  id: `${i}`,
  name: `testName${i}`,
  description: `description${i}`
});

const generateMockData = (length: number) => {
  const result = [];
  for (let i = 0; i < length; i++) {
    result.push(get1MockData(i));
  }

  return result;
};

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <SelectMonetaryTransactionsSummary {...props} />
    </div>
  );
};

describe('SelectMonetaryTransactionsSummary', () => {
  it('Should render summary ', () => {
    const mockData = generateMockData(2);
    const onEditStepMock = jest.fn();
    const props = {
      onEditStep: onEditStepMock,
      formValues: {
        transactions: mockData
      }
    } as any;
    const wrapper = renderComponent(props);

    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(onEditStepMock).toBeCalled();
  });

  it('Should render summary with paging ', () => {
    const mockData = generateMockData(11);
    const onEditStepMock = jest.fn();
    const props = {
      onEditStep: onEditStepMock,
      formValues: {
        transactions: mockData
      }
    } as any;
    const wrapper = renderComponent(props);

    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(onEditStepMock).toBeCalled();
  });

  it('Should render summary with transaction undefined ', () => {
    const onEditStepMock = jest.fn();
    const props = {
      onEditStep: onEditStepMock,
      formValues: {
        transactions: undefined
      }
    } as any;
    const wrapper = renderComponent(props);

    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(onEditStepMock).toBeCalled();
  });
});
