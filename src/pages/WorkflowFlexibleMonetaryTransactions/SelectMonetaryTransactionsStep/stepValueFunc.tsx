import { SelectMonetaryTransactionsFormValues } from '.';

export const stepValueFunc: WorkflowSetupStepValueFunc<SelectMonetaryTransactionsFormValues> =
  ({ stepsForm: formValues, t }) => {
    if (!formValues?.transactions) return '';
    const listNumber = formValues?.transactions?.length;
    const listLength = formValues?.listLength;
    let content = t('txt_number_transactions_selected', { listNumber });

    if (listNumber < 4) {
      content = formValues.transactions
        .map((item: any) => item.text)
        .join(', ');
    }

    if (listNumber === listLength) {
      content = t('txt_all_transactions_selected');
    }

    return content;
  };
