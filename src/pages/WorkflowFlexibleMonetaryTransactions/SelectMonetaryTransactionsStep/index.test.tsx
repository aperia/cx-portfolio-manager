import { render, RenderResult } from '@testing-library/react';
import React from 'react';
import SelectMonetaryTransactionsStep from '.';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

jest.mock('./SelectMonetaryTransactionsGrid', () => {
  return {
    __esModule: true,
    default: () => <div>SelectMonetaryTransactionsGrid</div>
  };
});

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <SelectMonetaryTransactionsStep {...props} />
    </div>
  );
};

describe('SelectMonetaryTransactionsStep', () => {
  it('Should render SelectMonetaryTransactionsStep contains Completed', () => {
    const wrapper = renderComponent({
      formValues: {}
    });

    expect(wrapper.getByText('txt_select_one_or_more_transaction'))
      .toBeInTheDocument;
  });
});
