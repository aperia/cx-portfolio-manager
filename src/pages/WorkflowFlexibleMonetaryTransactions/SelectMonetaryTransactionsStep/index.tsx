import { InlineMessage, useTranslation } from 'app/_libraries/_dls';
import { StateFile } from 'app/_libraries/_dls/components/Upload/File';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React from 'react';
import parseFormValues from './parseFormValues';
import SelectMonetaryTransactionsGrid from './SelectMonetaryTransactionsGrid';
import SelectMonetaryTransactionsSummary from './SelectMonetaryTransactionsSummary';
import { stepValueFunc } from './stepValueFunc';

export interface SelectMonetaryTransactionsFormValues {
  isValid?: boolean;
  listLength?: number;
  transactions?: any[];
  transactionsProp?: string[];
}
interface IDependencies {
  files?: StateFile[];
}
export interface SelectMonetaryTransactionsProps
  extends WorkflowSetupProps<
    SelectMonetaryTransactionsFormValues,
    IDependencies
  > {
  clearFormName: string;
}

const SelectMonetaryTransactions: React.FC<SelectMonetaryTransactionsProps> =
  props => {
    const { t } = useTranslation();
    const { isStuck } = props;

    return (
      <div>
        <p className="mt-12 color-grey">
          {t('txt_select_one_or_more_transaction')}
        </p>
        {isStuck && (
          <InlineMessage className="mb-0 mt-24" variant="danger" withIcon>
            {t('txt_step_stuck_move_forward_message')}
          </InlineMessage>
        )}
        <div className="pt-24 mt-24 border-top">
          <SelectMonetaryTransactionsGrid {...props} />
        </div>
      </div>
    );
  };

const ExtraStaticFlexibleMonetaryStep =
  SelectMonetaryTransactions as WorkflowSetupStaticProp;

ExtraStaticFlexibleMonetaryStep.summaryComponent =
  SelectMonetaryTransactionsSummary;
ExtraStaticFlexibleMonetaryStep.defaultValues = {
  isValid: false,
  transactions: []
};
ExtraStaticFlexibleMonetaryStep.stepValue = stepValueFunc;
ExtraStaticFlexibleMonetaryStep.parseFormValues = parseFormValues;

export default ExtraStaticFlexibleMonetaryStep;
