import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { stepRegistry } from '../_commons/WorkflowSetup/registries';
import ConfigureParametersStep from './ConfigureParametersStep';
import GettingStartStep from './GettingStartStep';

stepRegistry.registerStep(
  'GetStartedManageCreditLimitWorkflow',
  GettingStartStep
);
stepRegistry.registerStep(
  'ConfigureParametersManageCreditLimitWorkflow',
  ConfigureParametersStep,
  [
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_CREDIT_LIMIT_WORKFLOW__CONFIGURE_PARAMETERS
  ]
);
