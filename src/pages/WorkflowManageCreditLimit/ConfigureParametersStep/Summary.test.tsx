import { fireEvent } from '@testing-library/react';
import { WorkflowMetadataOverride } from 'app/fixtures/workflow-metadata';
import { renderWithMockStore } from 'app/utils';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageCreditLimit';
import React from 'react';
import Summary from './Summary';

const MockConfigureParametersOverride = jest.spyOn(
  WorkflowSetup,
  'useSelectConfigureParametersOverride'
);

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const getInitialState = ({
  elements = WorkflowMetadataOverride
}: any = {}) => ({
  workflowSetup: { elementMetadata: { elements } }
});

describe('ManagePenaltyFeeWorkflow > ConfigureParametersStep > Summary', () => {
  it('Should render summary contains Completed', async () => {
    MockConfigureParametersOverride.mockReturnValue({
      override: [
        {
          code: '1',
          text: 'Yes'
        },
        {
          code: '0',
          text: 'No'
        }
      ]
    });
    const props = {
      onEditStep: jest.fn(),
      formValues: {
        checked: '1',
        reasonCode: '123',
        dateRange: {
          start: new Date(),
          end: new Date()
        }
      }
    } as any;
    const wrapper = await renderWithMockStore(<Summary {...props} />, {
      initialState: getInitialState()
    });

    expect(wrapper.getByText('txt_date_range')).toBeInTheDocument;
    expect(wrapper.getByText('txt_temporary_credit_limit_override'))
      .toBeInTheDocument;
    expect(wrapper.getByText('txt_reason_code')).toBeInTheDocument;
  });

  it('Render with checked = r2', async () => {
    const mockFn = jest.fn();

    MockConfigureParametersOverride.mockReturnValue({
      override: [
        {
          code: '1',
          text: 'Yes'
        },
        {
          code: '0',
          text: 'No'
        }
      ]
    });
    const props = {
      formValues: {
        checked: '0',
        reasonCode: '123',
        dateRange: {
          start: new Date(),
          end: new Date()
        }
      },
      onEditStep: mockFn
    } as WorkflowSetupSummaryProps;

    const wrapper = await renderWithMockStore(<Summary {...props} />, {
      initialState: getInitialState()
    });

    expect(wrapper.getByText('No')).toBeInTheDocument;
    expect(wrapper.getByText('123')).toBeInTheDocument;

    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(mockFn).toHaveBeenCalled();
  });

  it('render with checked = empty string', async () => {
    const mockFn = jest.fn();

    MockConfigureParametersOverride.mockReturnValue({
      override: [
        {
          code: '1',
          text: 'Yes'
        },
        {
          code: '0',
          text: 'No'
        }
      ]
    });
    const props = {
      formValues: {
        checked: '',
        reasonCode: '123',
        dateRange: {
          start: new Date(),
          end: new Date()
        }
      },
      onEditStep: mockFn
    } as WorkflowSetupSummaryProps;

    const wrapper = await renderWithMockStore(<Summary {...props} />, {
      initialState: getInitialState()
    });

    expect(wrapper.getByText('txt_temporary_credit_limit_override'))
      .toBeInTheDocument;
    expect(wrapper.getByText('123')).toBeInTheDocument;

    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(mockFn).toHaveBeenCalled();
  });
});
