import { getWorkflowSetupStepStatus, isValidDate } from 'app/helpers';
import { isEmpty } from 'lodash';

const parseFormValues: WorkflowSetupStepFormDataFunc<any> = (data, id) => {
  const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);

  const {
    override,
    date,
    reason: reasonCode
  } = (data?.data as any)?.configurations || {};
  if ((isEmpty(date) && !reasonCode) || !stepInfo) return;

  const checked = override?.toString();
  const dateRange = {
    start: isValidDate(date.minDate) ? new Date(date.minDate) : undefined,
    end: isValidDate(date.maxDate) ? new Date(date.maxDate) : undefined
  };

  const isValid =
    !isEmpty(checked) &&
    !!dateRange?.end &&
    !!dateRange?.start &&
    !isEmpty(reasonCode);

  return {
    ...getWorkflowSetupStepStatus(stepInfo),
    isValid,
    checked,
    dateRange,
    reasonCode
  };
};

export default parseFormValues;
