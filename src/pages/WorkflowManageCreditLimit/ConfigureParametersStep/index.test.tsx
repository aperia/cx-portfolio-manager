import { render, RenderResult } from '@testing-library/react';
import React from 'react';
import ConfigureParametersStep from '.';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

jest.mock('./EditForm', () => {
  return {
    __esModule: true,
    default: () => <div>ConfigureParametersForm</div>
  };
});

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <ConfigureParametersStep {...props} />
    </div>
  );
};

describe('ManagePenaltyFeeWorkflow > ConfigureParametersStep', () => {
  it('Should render ConfigureParametersStep contains Completed', () => {
    const wrapper = renderComponent({
      formValues: {
        checked: 'r1'
      }
    });

    expect(
      wrapper.getByText(
        'txt_manage_credit_limit_workflow_configure_parameters_step'
      )
    ).toBeInTheDocument;
  });
  it('Should render stuck message', () => {
    const wrapper = renderComponent({
      formValues: {
        checked: 'r1'
      },
      isStuck: true
    });

    expect(wrapper.getByText('txt_step_stuck_move_forward_message'))
      .toBeInTheDocument;
  });
});
