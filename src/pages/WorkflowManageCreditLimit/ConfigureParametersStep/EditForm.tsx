import FieldTooltip from 'app/components/FieldTooltip';
import {
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { isDevice, stringValidate } from 'app/helpers';
import { useFormValidations, useUnsavedChangeRegistry } from 'app/hooks';
import {
  DateRangePicker,
  DateRangePickerValue,
  Radio,
  TextBox,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty, isEqual } from 'lodash';
import {
  actionsWorkflowSetup,
  useSelectConfigureParametersOverride
} from 'pages/_commons/redux/WorkflowSetup';
import { isEqualDate } from 'pages/_commons/Utils/date';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';
import { ConfigureParametersFormValues } from '.';
interface ChangeInfo {
  checked?: '1' | '0';
  reasonCode?: string;
  dateRange?: DateRangePickerValue;
}

const ConfigureParametersEditStep: React.FC<
  WorkflowSetupProps<ConfigureParametersFormValues>
> = ({ setFormValues, formValues, stepId, selectedStep, savedAt }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const isInitRef = useRef(true);

  const [initialValues, setInitialValues] = useState(formValues);
  const [disabledRange, setDisabledRange] = useState<any[]>([]);
  const { override } = useSelectConfigureParametersOverride();
  const [rangeActive, setRangeActive] = useState<{ min?: Date; max?: Date }>({
    min: undefined,
    max: undefined
  });
  const [invalidDates, setInvalidDates] = useState<any[]>([]);
  const [change, setChange] = useState<ChangeInfo>({
    dateRange: { start: undefined, end: undefined }
  });

  const handleFieldChange = (field: keyof ChangeInfo) => (e: any) => {
    const value = field === 'checked' ? e?.target?.id : e.target?.value;

    if (
      field === 'reasonCode' &&
      !stringValidate(value).isSpecialAlphanumeric()
    ) {
      return false;
    }

    setChange(change => {
      const newFormValue = {
        ...formValues,
        ...change,
        [field]: value
      };
      const isValid =
        !isEmpty(newFormValue.checked) &&
        !!newFormValue.dateRange?.end &&
        !!newFormValue.dateRange?.start &&
        !isEmpty(newFormValue.reasonCode);

      setFormValues({ ...newFormValue, isValid });
      return { ...change, [field]: value };
    });
  };

  const currentErrors = useMemo(() => {
    const inValidDateRange =
      !change?.dateRange || !change.dateRange.start || !change.dateRange.end;

    return {
      dateRange: inValidDateRange && {
        status: true,
        message: t('txt_required_validation', {
          fieldName: t('txt_date_range')
        })
      },
      reasonCode: isEmpty(change?.reasonCode?.trim()) && {
        status: true,
        message: t('txt_required_validation', {
          fieldName: t('txt_reason_code')
        })
      }
    } as Record<keyof ChangeInfo, IFormError>;
  }, [change, t]);
  const [, errors, setTouched] =
    useFormValidations<keyof ChangeInfo>(currentErrors);

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_CREDIT_LIMIT_WORKFLOW__CONFIGURE_PARAMETERS,
      priority: 1
    },
    [!isEqual(initialValues, formValues)]
  );

  const initFields = useCallback(async () => {
    const responseDateRange = await Promise.resolve(
      dispatch(actionsWorkflowSetup.getWorkflowEffectiveDateOptions({}))
    );
    dispatch(
      actionsWorkflowSetup.getWorkflowMetadata([
        'temporary.credit.limit.override'
      ])
    );

    const {
      maxDate,
      minDate,
      invalidDates = []
    } = (responseDateRange as any)?.payload?.data || {};

    setRangeActive({ max: new Date(maxDate), min: new Date(minDate) });
    const newDisabledRange: any[] = [];
    const newInvalidDates: any[] = [];
    invalidDates.forEach((invalidDate: any) => {
      const _date = new Date(invalidDate.date);
      newDisabledRange.push({ fromDate: _date, toDate: _date });
      newInvalidDates.push({ ...invalidDate, date: _date });
    });
    setInvalidDates(newInvalidDates);
    setDisabledRange(newDisabledRange);
    isInitRef.current = false;
  }, [dispatch]);

  const handleDateTooltip = (date: Date) => {
    const invalidDate = invalidDates.find(invalidDate =>
      isEqualDate(invalidDate.date, date)
    );

    if (!!invalidDate?.reason) {
      return <Tooltip element={<span>{invalidDate.reason}</span>} />;
    }

    if (
      (rangeActive?.min && rangeActive.min > date) ||
      (rangeActive?.max && rangeActive.max < date)
    ) {
      return <Tooltip element={<span>{t('txt_non_processing_day')}</span>} />;
    }
  };

  useEffect(() => {
    selectedStep === stepId && isInitRef.current && initFields();
  }, [initFields, selectedStep, stepId]);
  useEffect(() => {
    setInitialValues(formValues);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [savedAt]);

  useEffect(() => {
    const isUpdateChecked = isEqual(change.checked, formValues.checked);
    const isUpdateDateRange =
      isEqual(change.dateRange?.start, formValues.dateRange?.start) &&
      isEqual(change.dateRange?.end, formValues.dateRange?.end);
    const isUpdateReasonCode = isEqual(
      change.reasonCode,
      formValues.reasonCode
    );

    if (isUpdateChecked && isUpdateDateRange && isUpdateReasonCode) return;

    setChange({
      checked: formValues.checked,
      dateRange: {
        start: formValues.dateRange?.start,
        end: formValues.dateRange?.end
      },
      reasonCode: formValues.reasonCode
    });
  }, [
    change.checked,
    change.dateRange,
    change.reasonCode,
    formValues.checked,
    formValues.dateRange,
    formValues.reasonCode,
    stepId,
    selectedStep
  ]);

  return (
    <div className="row">
      <div className="col-6 col-xl-4">
        <FieldTooltip
          placement="right-end"
          element={
            <div>
              <div className="text-uppercase">{t('txt_start_end')}</div>
              <p className="mt-8">
                {t(
                  'txt_enter_the_date_range_when_the_temporary_credit_line_is_effective'
                )}
              </p>
            </div>
          }
        >
          <DateRangePicker
            error={errors.dateRange}
            onChange={handleFieldChange('dateRange')}
            onFocus={setTouched('dateRange')}
            onBlur={setTouched('dateRange', true)}
            label={t('txt_date_range')}
            dateTooltip={handleDateTooltip}
            disabledRange={disabledRange}
            maxDate={rangeActive.max}
            minDate={rangeActive.min}
            required
            value={change.dateRange}
            canInput={!isDevice}
          />
        </FieldTooltip>
      </div>

      <div className="col-12 mt-16">
        <p className="fw-600 color-grey">
          {t('txt_temporary_credit_limit_override')}
        </p>
        {override.map(({ code, text }) => {
          return (
            <div className="d-flex mt-8" key={code}>
              <Radio>
                <Radio.Input
                  id={code}
                  name="name"
                  checked={change.checked === code}
                  onChange={handleFieldChange('checked')}
                />
                <Radio.Label htmlFor={code}>{text}</Radio.Label>
              </Radio>
            </div>
          );
        })}
      </div>

      <div className="col-6 col-xl-4 mt-16">
        <FieldTooltip
          placement="right-start"
          element={
            <div>
              <div className="text-uppercase">{t('txt_reason')}</div>
              <p className="mt-8">
                {t(
                  'txt_enter_the_code_for_the_reason_for_assigning_the_temporary_credit_limit'
                )}
              </p>
            </div>
          }
        >
          <TextBox
            required
            maxLength={3}
            placeholder="000"
            value={change.reasonCode}
            error={errors.reasonCode}
            onChange={handleFieldChange('reasonCode')}
            onFocus={setTouched('reasonCode')}
            onBlur={setTouched('reasonCode', true)}
            label={t('txt_reason_code')}
          />
        </FieldTooltip>
      </div>
    </div>
  );
};

export default ConfigureParametersEditStep;
