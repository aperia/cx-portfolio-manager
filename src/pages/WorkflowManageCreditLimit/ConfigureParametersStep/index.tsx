import {
  DateRangePickerValue,
  InlineMessage,
  useTranslation
} from 'app/_libraries/_dls';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React from 'react';
import ConfigureParametersEditStep from './EditForm';
import parseFormValues from './parseFormValues';
import ConfigureParametersSummary from './Summary';

export interface ConfigureParametersFormValues {
  isValid?: boolean;
  checked?: '1' | '0';
  reasonCode?: string;
  dateRange?: DateRangePickerValue;
}

const ConfigureParametersStep: React.FC<
  WorkflowSetupProps<ConfigureParametersFormValues>
> = props => {
  const { t } = useTranslation();
  const { isStuck } = props;

  return (
    <div className="mt-24">
      {isStuck && (
        <InlineMessage className="mb-24" variant="danger" withIcon>
          {t('txt_step_stuck_move_forward_message')}
        </InlineMessage>
      )}

      <InlineMessage variant="warning" withIcon>
        {t('txt_manage_credit_limit_workflow_configure_parameters_step')}
      </InlineMessage>

      <ConfigureParametersEditStep {...props} />
    </div>
  );
};

const ExtraStaticConfigureParameters =
  ConfigureParametersStep as WorkflowSetupStaticProp;

ExtraStaticConfigureParameters.parseFormValues = parseFormValues;
ExtraStaticConfigureParameters.summaryComponent = ConfigureParametersSummary;
ExtraStaticConfigureParameters.defaultValues = {
  isValid: false,
  checked: '1',
  reasonCode: '',
  dateRange: {
    start: undefined,
    encodeURI: undefined
  }
};

export default ExtraStaticConfigureParameters;
