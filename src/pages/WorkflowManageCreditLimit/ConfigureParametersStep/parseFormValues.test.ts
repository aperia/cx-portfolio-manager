import parseFormValues from './parseFormValues';

describe('parseFormValues', () => {
  const data = {
    override: '1',
    date: {
      minDate: '1/1/2021',
      maxDate: '2/2/2021'
    },
    reason: 'reason'
  };
  const input: any = {
    data: {
      configurations: data,
      methodList: [],
      workflowSetupData: [
        {
          id: 'id',
          status: 'DONE'
        }
      ]
    }
  };

  it('selected step', () => {
    const id = 'id';
    const result = parseFormValues(input, id, jest.fn());

    expect(result.reasonCode).toEqual(data.reason);
    expect(result.checked).toEqual(data.override);
    expect(result.dateRange).toEqual({
      start: new Date(data.date.minDate),
      end: new Date(data.date.maxDate)
    });
  });

  it('select other step', () => {
    const id = 'id1';
    const result = parseFormValues(input, id, jest.fn());

    expect(result).toBeUndefined();
  });
  it('empty configs', () => {
    const id = 'id1';
    const result = parseFormValues(
      {
        data: {
          workflowSetupData: [
            {
              id: 'id',
              status: 'DONE'
            }
          ]
        }
      } as never,
      id,
      jest.fn()
    );
    expect(result).toBeUndefined();
  });
});
