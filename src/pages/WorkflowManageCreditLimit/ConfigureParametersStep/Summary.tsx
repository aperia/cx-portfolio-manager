import GroupTextControl from 'app/components/DofControl/GroupTextControl';
import { GroupTextFormat } from 'app/constants/enums';
import { Button, useTranslation } from 'app/_libraries/_dls';
import { isFunction } from 'lodash';
import {
  actionsWorkflowSetup,
  useSelectConfigureParametersOverride
} from 'pages/_commons/redux/WorkflowSetup';
import React, { useCallback, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { ConfigureParametersFormValues } from '.';

const ConfigureParametersSummary: React.FC<
  WorkflowSetupSummaryProps<ConfigureParametersFormValues>
> = ({ formValues, onEditStep }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { override } = useSelectConfigureParametersOverride();

  const handleEdit = () => {
    isFunction(onEditStep) && onEditStep();
  };

  const checked = override.find(item => item.code === formValues.checked);

  const initFields = useCallback(async () => {
    dispatch(
      actionsWorkflowSetup.getWorkflowMetadata([
        'temporary.credit.limit.override'
      ])
    );
  }, [dispatch]);

  useEffect(() => {
    initFields();
  }, [initFields]);

  return (
    <div className="position-relative">
      <div className="absolute-top-right mt-n26 mr-n8">
        <Button variant="outline-primary" size="sm" onClick={handleEdit}>
          {t('txt_edit')}
        </Button>
      </div>
      <div className="row">
        <div className="col-12 col-lg-3">
          <GroupTextControl
            id="date_range"
            input={{ value: formValues.dateRange } as any}
            meta={{} as any}
            label={t('txt_date_range')}
            options={{ inline: false, format: GroupTextFormat.DateRange }}
          />
        </div>
        <div className="col-12 col-lg-6">
          <GroupTextControl
            id="temporary_credit_limit_override"
            input={{ value: checked?.text } as any}
            meta={{} as any}
            label={t('txt_temporary_credit_limit_override')}
            options={{ inline: false }}
          />
        </div>
        <div className="col-12 col-lg-3">
          <GroupTextControl
            id="reason_code"
            input={{ value: formValues.reasonCode } as any}
            meta={{} as any}
            label={t('txt_reason_code')}
            options={{ inline: false }}
          />
        </div>
      </div>
    </div>
  );
};

export default ConfigureParametersSummary;
