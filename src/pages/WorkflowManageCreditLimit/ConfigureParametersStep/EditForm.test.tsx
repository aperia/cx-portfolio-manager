import { fireEvent, screen } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';
import { WorkflowMetadataOverride } from 'app/fixtures/workflow-metadata';
import { renderWithMockStore } from 'app/utils';
import { DatePickerChangeEvent } from 'app/_libraries/_dls';
import React from 'react';
import { act } from 'react-dom/test-utils';
import * as ReactRedux from 'react-redux';
import { ConfigureParametersFormValues } from '.';
import ConfigureParametersEditStep from './EditForm';
const mockUseDispatch = jest.spyOn(ReactRedux, 'useDispatch');

(function mockDOMMatrix() {
  class DOMMatrixMock {
    scale = jest.fn();
    translate = jest.fn();
  }
  global.DOMMatrix = DOMMatrixMock as any;
})();

const mockDateTooltipParams = jest.fn();
jest.mock('app/_libraries/_dls/components/DateRangePicker', () => {
  const actualModule = jest.requireActual(
    'app/_libraries/_dls/components/DateRangePicker'
  );
  return {
    ...actualModule,
    __esModule: true,
    default: ({ onChange, dateTooltip, label, onBlur, ...props }: any) => (
      <div>
        <span>{label}</span>
        <input
          data-testid="mockDatePicker"
          {...props}
          onClick={() => {
            const params = mockDateTooltipParams();

            !!dateTooltip && dateTooltip(...params);
          }}
          onChange={(e: any) => {
            onChange!({
              target: { value: new Date(e.target.value), name: e.target.name }
            } as DatePickerChangeEvent);
          }}
          onBlur={onBlur}
        />
      </div>
    )
  };
});

const queryInputFromLabel = (tierLabel: HTMLElement) => {
  return tierLabel
    .closest(
      '.dls-input-container, .dls-numreric-container, .text-field-container'
    )
    ?.querySelector('input, .input') as HTMLInputElement;
};
const renderComponent = (props: any) => {
  return renderWithMockStore(<ConfigureParametersEditStep {...props} />, {
    initialState: {
      workflowSetup: {
        elementMetadata: {
          elements: [WorkflowMetadataOverride] as any
        }
      }
    } as AppState
  });
};
describe('ManageCreditLimitWorkflow > ConfigureParametersSteps', () => {
  const testId = 'testId';
  beforeEach(() => {
    mockUseDispatch.mockReturnValue(jest.fn());
    mockDateTooltipParams.mockReturnValue([new Date('01/01/2021')]);
    mockUseDispatch.mockReturnValue(jest.fn());
  });

  afterEach(() => {
    jest.clearAllMocks();
  });
  it('Render component', async () => {
    mockDateTooltipParams.mockReturnValue([new Date('2021/02/03')]);
    mockUseDispatch.mockReturnValue(
      jest.fn().mockResolvedValue({
        payload: {
          data: {
            maxDate: '2021/02/04',
            minDate: '2021/02/01',
            invalidDates: [{ date: '2021/02/03', reason: 'no reason' }]
          }
        }
      })
    );
    const props: WorkflowSetupProps<ConfigureParametersFormValues> = {
      setFormValues: jest.fn(),
      formValues: {
        checked: '1',
        isValid: true,
        reasonCode: '123',
        dateRange: {
          start: new Date(),
          end: new Date()
        }
      },
      selectedStep: 'step',
      stepId: 'step',
      savedAt: 123
    };
    const { getByTestId } = await renderComponent({
      testId,
      ...props
    });
    const inputElement = getByTestId('mockDatePicker');
    expect(inputElement).toBeInTheDocument();
    fireEvent.click(inputElement);

    fireEvent.change(inputElement, {
      target: {
        value: 'undefined',
        name: 'dateValue',
        date: new Date('2021/02/03'),
        view: 'month'
      }
    });
    expect(inputElement).toBeInTheDocument();
  });
  it('Render component when selected', async () => {
    const props: WorkflowSetupProps<ConfigureParametersFormValues> = {
      setFormValues: jest.fn(),
      formValues: {
        checked: '1',
        isValid: true,
        reasonCode: '123',
        dateRange: {
          start: new Date(),
          end: new Date()
        }
      },
      selectedStep: 'step',
      stepId: 'step',
      savedAt: 123
    };
    const { getByTestId } = await renderComponent({
      testId,
      ...props
    });
    const inputElement = getByTestId('mockDatePicker');
    expect(inputElement).toBeInTheDocument();
    fireEvent.click(inputElement);
    fireEvent.change(inputElement, {
      target: {
        value: 'undefined',
        name: 'dateValue',
        date: new Date('2021/02/03'),
        view: 'month'
      }
    });
    expect(inputElement).toBeInTheDocument();
  });
  it('Render component when min-max valid', async () => {
    mockUseDispatch.mockReturnValue(
      jest.fn().mockReturnValue({
        payload: {
          data: {
            maxDate: '2021/02/02',
            minDate: '2021/02/01',
            invalidDates: [{ date: '2021/02/03', reason: 'no reason' }]
          }
        }
      })
    );
    const props: WorkflowSetupProps<ConfigureParametersFormValues> = {
      setFormValues: jest.fn(),
      formValues: {
        checked: '1',
        isValid: true,
        reasonCode: '123',
        dateRange: {
          start: new Date(),
          end: new Date()
        }
      },
      selectedStep: 'step',
      stepId: 'step',
      savedAt: 123
    };
    const { getByTestId } = await renderComponent({
      testId,
      ...props
    });
    const inputElement = getByTestId('mockDatePicker');
    expect(inputElement).toBeInTheDocument();
    fireEvent.click(inputElement);
    fireEvent.change(inputElement, {
      target: {
        value: 'undefined',
        name: 'dateValue',
        date: new Date('2021/02/04'),
        view: 'month'
      }
    });
    expect(inputElement).toBeInTheDocument();
  });
  it('handle change reason code', async () => {
    const props: WorkflowSetupProps<ConfigureParametersFormValues> = {
      setFormValues: jest.fn(),
      formValues: {
        checked: '1',
        isValid: true,
        reasonCode: '123',
        dateRange: {
          start: new Date(),
          end: new Date()
        }
      },
      selectedStep: 'step',
      stepId: 'step',
      savedAt: 123
    };
    const wrapper = await renderComponent({ testId, ...props });
    const checkInputs = screen.getAllByRole('radio')! as HTMLInputElement[];
    act(() => {
      userEvent.click(checkInputs[0]);
    });
    const reason = queryInputFromLabel(wrapper.getByText('txt_reason_code'));
    wrapper.rerender(
      <ConfigureParametersEditStep
        formValues={{
          checked: '1',
          isValid: true,
          reasonCode: '',
          dateRange: {
            start: new Date(),
            end: new Date()
          }
        }}
        {...({} as any)}
      />
    );
    fireEvent.change(reason, 'aa');
    wrapper.rerender(
      <ConfigureParametersEditStep
        formValues={{
          checked: '1',
          isValid: true,
          reasonCode: '',
          dateRange: {
            start: new Date(),
            end: new Date()
          }
        }}
        {...({} as any)}
      />
    );
    userEvent.type(reason, '#$');
  });
});
