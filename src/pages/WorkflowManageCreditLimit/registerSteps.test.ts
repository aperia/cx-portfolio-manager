import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { default as stepRegistry } from '../_commons/WorkflowSetup/registries/stepRegistry';

const registerFunc = jest.spyOn(stepRegistry, 'registerStep');

describe('ManageCreditLimitWorkflow > registerSteps', () => {
  it('Should register components', async () => {
    await import('./registerSteps');

    expect(registerFunc).toBeCalledWith(
      'GetStartedManageCreditLimitWorkflow',
      expect.anything()
    );
    expect(registerFunc).toBeCalledWith(
      'ConfigureParametersManageCreditLimitWorkflow',
      expect.anything(),
      [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_CREDIT_LIMIT_WORKFLOW__CONFIGURE_PARAMETERS
      ]
    );
  });
});
