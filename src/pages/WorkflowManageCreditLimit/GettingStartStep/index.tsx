import { WORKFLOW_SETUP } from 'app/constants/local-storage';
import { Button, Icon } from 'app/_libraries/_dls';
import OverviewModal from 'pages/_commons/OverviewModal';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, { useRef, useState } from 'react';
import GettingStartSummary from './GettingStartSummary';

export interface FormGettingStart {
  isValid?: boolean;

  returnedCheckCharges?: boolean;
  lateCharges?: boolean;

  alreadyShown?: boolean;
}

const GettingStartStep: React.FC<WorkflowSetupProps<FormGettingStart>> = ({
  stepId,
  selectedStep,
  title,
  formValues,
  setFormValues
}) => {
  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;

  const [showOverview, setShowOverview] = useState(
    formValues.alreadyShown
      ? false
      : localStorage.getItem(WORKFLOW_SETUP.SHOW_OVERVIEW_AGAIN) !== 'false'
  );

  return (
    <React.Fragment>
      <div className="position-relative">
        <div className="absolute-top-right mt-n28">
          <Button
            className="mr-n8"
            variant="outline-primary"
            size="sm"
            onClick={() => setShowOverview(true)}
          >
            View Overview
          </Button>
        </div>
        <div className="row">
          <div className="col-6 mt-24">
            <div className="bg-light-l20 rounded-lg p-16">
              <div className="text-center">
                <Icon name="request" size="12x" className="color-grey-l16" />
              </div>
              <p className="mt-8 color-grey">
                Use the Manage Temporary Credit Limit workflow to make a
                temporary increase or decrease in cardholders credit limit.
                Portfolio Manager will format and submit a batch NM*731,
                Temporary Credit Line Increase/Decrease transactions using the
                Cardholder Non-Monetary Transaction Tape Format (012) on your
                behalf for batch mainframe processing on the effective date of
                your change.
              </p>
              <p className="mt-16">
                <strong className="color-grey-d20">
                  Monitoring Non-Monetary Processing Results
                </strong>
              </p>
              <p className="mt-8 color-grey">
                You can use the following reports to monitor the processing
                results of the Manage Temporary Credit Limit workflow.
              </p>
              <ul className="list-dot color-grey">
                <li>
                  CD-051 Non-Monetary Journal report displays online, batch, and
                  System generated non-monetary transaction entries that posted
                  to the Cardholder Master File on the processing day.
                </li>
                <li>
                  CD-052 Unposted Non-Monetary Items report lists non-monetary
                  transaction entries that failed to post to the Cardholder
                  Master File on the processing day. The reason the transaction
                  failed to post also appears.
                </li>
                <li>
                  CD-011 Non-Monetary Entry List report displays rejects that
                  result from general edits such as the incorrect use of numeric
                  and nonnumeric data and the valid codes available. Rejects on
                  the CD-052 report result from System checks of information on
                  file and incorrect attempts to modify that information.
                </li>
              </ul>
            </div>
          </div>
          <div className="col-6 mt-24 color-grey">
            <p>Follow these steps to set up this workflow.</p>
            <p className="mt-8">
              1. <strong className="color-grey-d20">Select the change</strong>{' '}
              that will deploy this workflow or{' '}
              <strong className="color-grey-d20">create a new change</strong> to
              deploy it. To create a new change, you must enter the required
              information.
            </p>
            <p className="mt-8">
              2.{' '}
              <strong className="color-grey-d20">
                Upload a list of accounts
              </strong>{' '}
              and their related information.
            </p>
            <p className="mt-8">
              3.{' '}
              <strong className="color-grey-d20">
                Configure the parameters
              </strong>{' '}
              to set up the temporary credit limit.
            </p>
            <p className="mt-8">
              4. On the Review and Submit page, review the workflow information.
              If everything is correct, click{' '}
              <strong className="color-grey-d20">Submit</strong>.
            </p>
          </div>
        </div>
      </div>
      {showOverview && (
        <OverviewModal
          id="overviewWorkflowSetup"
          show
          onClose={() => {
            setShowOverview(false);
            keepRef.current.setFormValues({ alreadyShown: true });
          }}
        />
      )}
    </React.Fragment>
  );
};

const ExtraStaticGettingStartStep =
  GettingStartStep as WorkflowSetupStaticProp<FormGettingStart>;

ExtraStaticGettingStartStep.summaryComponent = GettingStartSummary;
ExtraStaticGettingStartStep.defaultValues = { isValid: true };

export default ExtraStaticGettingStartStep;
