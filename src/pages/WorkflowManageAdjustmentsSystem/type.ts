export enum ParameterCode {
  TransactionId = 'transaction.id',

  ProfileName = 'profile.name',
  BatchCode = 'batch.code',
  TransactionCode = 'transaction.code',
  MaximumAmount = 'maximum.amount',
  ActionCode = 'action.code',
  ShellName = 'shell.name'
}

export enum ParameterName {
  ProfileName = 'Profile Name',
  BatchCode = 'Batch Code',
  TransactionCode = 'Transaction Code',
  MaximumAmount = 'Maximum Amount',
  ActionCode = 'Action Code'
}

export enum ShellParameterName {
  TransactionProfile = 'Transaction Profile',
  Description = 'Description',
  OperatorCode = 'Operator Code',
  EmployeeID = 'Employee ID'
}

export type RefDataWithDesc = RefData & { description: string };

export interface GeneralInformationModel {
  managerId: string;
  queueOwner: string;
  queueOwnerId: string;
  terminalGroupId: string;
}

export interface ShellGeneralInformationModel {
  departmentId: string;
  managerOperatorId: string;
  operatorId: string;
  profile: string;
  supervisorOperatorId: string;
  terminalGroupId: string;
}

export interface AdjustmentLimitInfoModel {
  actionCode: string;
  adjustmentTypeId: string;
  batchCode: string;
  managerDepartmentMaximumAmount: number;
  shellMaximumAmount: number;
}

export interface GeneralInformationItem {
  fieldName: string;
  greenScreenName: string;
  value: string;
  moreInfo: string;
}

export const GeneralInformationFactory: Record<
  keyof GeneralInformationModel,
  Omit<GeneralInformationItem, 'value'>
> = {
  queueOwner: {
    fieldName: 'Queue Owner',
    greenScreenName: 'Queue Owner',
    moreInfo:
      'Shell name of the queue owner for whom you are assigning alternate reviewers.'
  },
  queueOwnerId: {
    fieldName: 'Queue Owner ID',
    greenScreenName: 'Oper ID',
    moreInfo: 'Identifier of the Queue Owner as assigned in Password System B.'
  },
  managerId: {
    fieldName: 'Manager ID',
    greenScreenName: 'Mgr OPID',
    moreInfo: 'Identifier of the manager to whom the Queue Owner reports.'
  },
  terminalGroupId: {
    fieldName: 'Terminal Group ID',
    greenScreenName: 'Group ID',
    moreInfo: 'Identifier of the terminal group.'
  }
};

export const ShellGeneralInformationFactory: Record<
  keyof ShellGeneralInformationModel,
  Omit<GeneralInformationItem, 'value'>
> = {
  departmentId: {
    fieldName: 'Department ID',
    greenScreenName: 'Dept',
    moreInfo:
      'The Department ID parameter defines the identifier of the department to which the Shell Name reports.'
  },
  managerOperatorId: {
    fieldName: 'Manager Operator ID',
    greenScreenName: 'Mgr OPID',
    moreInfo:
      'Operator identifier that the online security monitor establishes in Password System B for the manager responsible for reviewing automated adjustments the Shell Name makes.'
  },
  operatorId: {
    fieldName: 'Operator ID',
    greenScreenName: 'Oper ID',
    moreInfo:
      'Operator identifier the online security monitor establishes in Password System B for Shell Name of the operator initiating automated adjustments.'
  },
  profile: {
    fieldName: 'Profile',
    greenScreenName: 'Profile',
    moreInfo:
      'The Profile parameter defines the identifier of the access controls for the Shell Name per the Profile ID in Password System B.'
  },
  supervisorOperatorId: {
    fieldName: 'Supervisor Operator ID',
    greenScreenName: 'Supv OPID',
    moreInfo:
      'Operator identifier the online security monitor establishes in Password System B for the supervisor responsible for reviewing automated adjustments the Shell Name makes.'
  },
  terminalGroupId: {
    fieldName: 'Terminal Group ID',
    greenScreenName: 'Group',
    moreInfo:
      'The Terminal Group ID parameter defines the identifier of the terminal group.'
  }
};

export const AdjustmentLimitInformationFactory: Record<
  keyof AdjustmentLimitInfoModel,
  Omit<GeneralInformationItem, 'value'>
> = {
  batchCode: {
    fieldName: 'Batch Code',
    greenScreenName: 'BC',
    moreInfo:
      'The Batch Code paramter defines the identifier of the type of adjustments accessible to the operator.'
  },
  adjustmentTypeId: {
    fieldName: 'Adjustment Type ID',
    greenScreenName: 'TRN',
    moreInfo:
      'The Adjustment Type ID parameter defines the identifier of the adjustment type.'
  },
  shellMaximumAmount: {
    fieldName: 'Shell Maximum Amount',
    greenScreenName: 'Approve',
    moreInfo:
      'The Shell Maximum Amount parameter defines the maximum amount the Shell Name can adjust.'
  },
  managerDepartmentMaximumAmount: {
    fieldName: 'Manager/Department Maximum Amount',
    greenScreenName: 'Max',
    moreInfo:
      'The Manager/Department Maximum Amount parameter defines maximum amount the manager/department for the Shell Name can adjust.'
  },

  actionCode: {
    fieldName: 'Action Code',
    greenScreenName: 'AC',
    moreInfo:
      'The Action Code parameter determines the action to take when you attempt an adjustment and the amount exceeds the APPROVE limit for the Shell Name.'
  }
};
