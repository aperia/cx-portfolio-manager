import { getWorkflowSetupStepStatus } from 'app/helpers';
import { FormConfigJFS } from '.';

const parseFormValues: WorkflowSetupStepFormDataFunc<FormConfigJFS> = (
  data,
  id
) => {
  const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);
  if (!stepInfo) return;

  const shellProfile = data?.data?.configurations?.shellProfile;
  return {
    ...getWorkflowSetupStepStatus(stepInfo),
    ...shellProfile,
    isValid: !!shellProfile?.shellName
  };
};

export default parseFormValues;
