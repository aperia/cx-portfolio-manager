import { Button, useTranslation } from 'app/_libraries/_dls';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import { isFunction } from 'lodash';
import { useSelectElementsForManageAdjustmentSystem } from 'pages/_commons/redux/WorkflowSetup';
import React, { useMemo } from 'react';
import { FormConfigJFS } from '.';
import GeneralInformation, {
  useShellGeneralInformation
} from '../GeneralInformation';
import { RefDataWithDesc } from '../type';
import {
  AdjustmentLimitInfo,
  useAdjustmentLimitInfo
} from './AdjustmentLimitInfo';
const ConfigJFSSummary: React.FC<WorkflowSetupSummaryProps<FormConfigJFS>> = ({
  onEditStep,
  formValues
}) => {
  const { t } = useTranslation();

  const { shellNames } = useSelectElementsForManageAdjustmentSystem();
  const shellName = useMemo(
    () =>
      shellNames.find(s => s.code === formValues?.shellName) as RefDataWithDesc,
    [shellNames, formValues?.shellName]
  );

  const generalInformationProps = useShellGeneralInformation(
    formValues?.generalInfo
  );

  const adjustmentLimitInformationProps = useAdjustmentLimitInfo(
    formValues?.adjustmentLimitInfo
  );

  const handleEdit = () => {
    isFunction(onEditStep) && onEditStep();
  };

  return (
    <div className="position-relative">
      <div className="absolute-top-right mt-n26 mr-n8">
        <Button variant="outline-primary" size="sm" onClick={handleEdit}>
          {t('txt_edit')}
        </Button>
      </div>
      <div className="pt-16">
        <div className="form-group-static mt-0">
          <span className="form-group-static__label">
            {t('txt_manage_ocs_shells_step_3_shell_name')}
          </span>
          <span className="form-group-static__text">
            {shellName?.description}
          </span>
        </div>
        {!isEmpty(shellName) && (
          <GeneralInformation {...generalInformationProps} preview={true} />
        )}
        {!isEmpty(shellName) && (
          <AdjustmentLimitInfo
            {...adjustmentLimitInformationProps}
            preview={true}
          />
        )}
      </div>
    </div>
  );
};

export default ConfigJFSSummary;
