import { fireEvent, render, RenderResult } from '@testing-library/react';
import React from 'react';
import Summary from './Summary';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

jest.mock('pages/_commons/redux/WorkflowSetup', () => ({
  useSelectElementsForManageAdjustmentSystem: () => ({
    shellNames: [{ value: 'John', code: 'John', description: 'John' }]
  })
}));

jest.mock('../GeneralInformation', () => ({
  __esModule: true,
  default: () => <div>general Information</div>,
  useShellGeneralInformation: () => ({})
}));

jest.mock('./AdjustmentLimitInfo', () => ({
  __esModule: true,
  AdjustmentLimitInfo: () => <div>AdjustmentLimitInfo</div>,
  useAdjustmentLimitInfo: () => ({})
}));

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <Summary {...props} />
    </div>
  );
};

describe('ManageAdjustmentsSystemWorkflow > ConfigJFSStep > Summary', () => {
  it('Should render summary', () => {
    const wrapper = renderComponent({
      formValues: {}
    } as unknown as WorkflowSetupSummaryProps);

    expect(
      wrapper.getByText('txt_manage_ocs_shells_step_3_shell_name')
    ).toBeInTheDocument();
  });

  it('Should render summary with data', () => {
    const wrapper = renderComponent({
      formValues: { shellName: 'John' }
    } as unknown as WorkflowSetupSummaryProps);

    expect(wrapper.getByText('AdjustmentLimitInfo')).toBeInTheDocument();
  });

  it('Should click on edit', () => {
    const mockFn = jest.fn();
    const wrapper = renderComponent({
      formValues: {},
      onEditStep: mockFn
    } as unknown as WorkflowSetupSummaryProps);

    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(mockFn).toHaveBeenCalled();
  });
});
