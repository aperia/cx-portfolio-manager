import FieldTooltip from 'app/components/FieldTooltip';
import { ComboBox, useTranslation } from 'app/_libraries/_dls';
import { orderBy } from 'app/_libraries/_dls/lodash';
import {
  useSelectElementMetadataStatus,
  useSelectElementsForManageAdjustmentSystem
} from 'pages/_commons/redux/WorkflowSetup';
import React from 'react';
import { RefDataWithDesc } from '../type';

export interface SelectShellProps {
  shellName?: RefDataWithDesc;
  setShellName: (val: RefDataWithDesc) => void;
}

const SelectShell: React.FC<SelectShellProps> = ({
  shellName,
  setShellName
}) => {
  const { t } = useTranslation();
  const { shellNames } = useSelectElementsForManageAdjustmentSystem();
  const { loading: metadataLoading } = useSelectElementMetadataStatus();
  const shellNameOptions = orderBy(shellNames, ['description'], ['asc']);
  return (
    <>
      <h5 className="mt-24">{t('txt_select_shell')}</h5>
      <div className="row">
        <div className="col-6 col-xl-4 mt-16">
          <FieldTooltip
            keepShowOnDevice={false}
            placement="right-start"
            element={
              <div>
                <div className="text-uppercase">
                  {t('txt_shell_tooltip_header')}
                </div>
                <p className="mt-8">{t('txt_select_shell_tooltip_body')}</p>
              </div>
            }
          >
            <ComboBox
              required
              textField="description"
              label={t('txt_shell_name')}
              placeholder={t('txt_shell_name')}
              value={
                shellNames.find(s => s.code === shellName?.code) ?? shellName
              }
              onChange={e => setShellName(e.target.value)}
              loading={metadataLoading}
            >
              {(shellNameOptions as RefDataWithDesc[]).map(item => {
                return (
                  <ComboBox.Item
                    key={item.code}
                    value={item}
                    label={item.description}
                  />
                );
              })}
            </ComboBox>
          </FieldTooltip>
        </div>
      </div>
    </>
  );
};

export default SelectShell;
