import { HeadingWithSearch } from 'app/components/HeadingWithSearch';
import NoDataFound from 'app/components/NoDataFound';
import {
  formatCommon,
  mapGridExpandCollapse,
  matchSearchValue
} from 'app/helpers';
import { Grid, SortType, useTranslation } from 'app/_libraries/_dls';
import { isEmpty, orderBy } from 'app/_libraries/_dls/lodash';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import { useSelectElementsForManageAdjustmentSystem } from 'pages/_commons/redux/WorkflowSetup';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { GeneralInfoGrid } from '../GeneralInformation';
import {
  AdjustmentLimitInfoModel,
  AdjustmentLimitInformationFactory,
  GeneralInformationItem,
  RefDataWithDesc
} from '../type';

const adjustmentLimitInfoSearchDesc = [
  'txt_action_code',
  'txt_adjustment_type_id',
  'txt_batch_code'
];

export interface AdjustmentLimitInfoProps {
  data: Array<
    AdjustmentLimitInfoModel & { subRow: GeneralInformationItem[] } & {
      actionCodeCombined: string;
    }
  >;
  searchValue: string;
  onSearchChange: (s: string) => void;
  preview?: boolean;
  shellName?: RefDataWithDesc;
}

const defaultSort: SortType = { id: 'batchCode' };

const AdjustmentLimitInfo: React.FC<AdjustmentLimitInfoProps> = ({
  data,
  shellName,
  searchValue,
  onSearchChange,
  preview = false
}) => {
  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();
  const classNames = preview ? 'mt-24' : 'mt-24 pt-24 border-top';

  const [expandedList, setExpandedList] = useState<string[]>([]);
  const [sort, setSort] = useState<SortType>(defaultSort);

  const handleOnExpand = (dataKeyList: string[]) => {
    setExpandedList(isEmpty(dataKeyList) ? [] : dataKeyList);
  };

  const handleClearFilter = useCallback(() => {
    onSearchChange('');
  }, [onSearchChange]);

  const gridData = useMemo(() => {
    // sort by batchCode by default
    return orderBy(data, [sort.id], [sort.order || 'asc']).filter(item =>
      matchSearchValue(
        [
          item['actionCodeCombined'],
          item['adjustmentTypeId'],
          item['batchCode']
        ],
        searchValue
      )
    );
  }, [data, searchValue, sort.id, sort.order]);

  const columns = useMemo(
    () => [
      {
        id: 'batchCode',
        Header: t('txt_batch_code'),
        accessor: 'batchCode',
        isSort: !preview,
        width: 120
      },
      {
        id: 'adjustmentTypeId',
        Header: t('txt_adjustment_type_id'),
        accessor: 'adjustmentTypeId',
        isSort: !preview,
        width: 157
      },
      {
        id: 'shellMaximumAmount',
        Header: t('txt_shell_maximum_amount'),
        accessor: ({ shellMaximumAmount }: any) =>
          formatCommon(shellMaximumAmount).currency(2),
        cellBodyProps: { className: 'text-right' },
        cellHeaderProps: { className: 'text-right' },
        isSort: !preview,
        width: width < 1280 ? 200 : undefined
      },
      {
        id: 'managerDepartmentMaximumAmount',
        Header: t('txt_manager_department_maximum_amount'),
        accessor: ({ managerDepartmentMaximumAmount }: any) =>
          formatCommon(managerDepartmentMaximumAmount).currency(2),
        cellBodyProps: { className: 'text-right' },
        cellHeaderProps: { className: 'text-right' },
        isSort: !preview,
        width: width < 1280 ? 180 : undefined
      },
      {
        id: 'actionCode',
        Header: t('txt_action_code'),
        className: 'text-center',
        accessor: 'actionCode',
        width: 132
      }
    ],
    [t, preview, width]
  );

  useEffect(() => {
    // collapse all when searchValue and data changed
    handleOnExpand([]);
  }, [searchValue, data]);

  useEffect(() => {
    handleOnExpand([]);
    onSearchChange('');
    setSort(defaultSort);
  }, [onSearchChange, shellName]);

  const isNoRecordToDisplay =
    isEmpty(data) || (isEmpty(gridData) && !searchValue);

  return (
    <div className={classNames}>
      <HeadingWithSearch
        preview={preview}
        txtTitle="txt_adjustment_limit_information"
        searchValue={searchValue}
        onSearchChange={onSearchChange}
        searchDescription={adjustmentLimitInfoSearchDesc}
        onClearSearch={handleClearFilter}
        isDataEmpty={isEmpty(gridData)}
      />

      {isNoRecordToDisplay && (
        <NoDataFound
          id="newMethod_notfound"
          title={t('txt_no_results_found')}
          className="mt-40"
        />
      )}
      {!isEmpty(data) && isEmpty(gridData) && searchValue && (
        <div className="d-flex flex-column justify-content-center mt-40 mb-24">
          <NoDataFound
            id="newMethod_notfound"
            hasSearch
            title={t('txt_no_results_found')}
            linkTitle={t('txt_clear_and_reset')}
            onLinkClicked={handleClearFilter}
          />
        </div>
      )}
      {!isEmpty(gridData) && (
        <div className="mt-16">
          <Grid
            className="grid-has-tooltip center"
            togglable
            columns={columns}
            data={gridData}
            sortBy={[sort]}
            onSortChange={setSort}
            dataItemKey="rowId"
            expandedItemKey={'rowId'}
            expandedList={expandedList}
            onExpand={handleOnExpand}
            subRow={({ original }: any) => {
              return (
                <div className="p-20">
                  <GeneralInfoGrid data={original.subRow} />
                </div>
              );
            }}
            toggleButtonConfigList={gridData.map(
              mapGridExpandCollapse('rowId', t)
            )}
          />
        </div>
      )}
    </div>
  );
};

const useAdjustmentLimitInfo = (props?: AdjustmentLimitInfoModel[]) => {
  const [searchValue, setSearchValue] = useState('');
  const { actionCode } = useSelectElementsForManageAdjustmentSystem();
  const data = useMemo(
    () =>
      props
        ? props.map((row, rowId) => {
            const actionCodeCombined =
              actionCode.find(item => item.code === row.actionCode)?.text ??
              row.actionCode;
            const subRow = Object.entries(row).map(([ky, value]) => ({
              ...AdjustmentLimitInformationFactory[
                ky as keyof AdjustmentLimitInfoModel
              ],
              value: (function () {
                switch (ky) {
                  case 'actionCode':
                    return actionCodeCombined;
                  case 'shellMaximumAmount':
                  case 'managerDepartmentMaximumAmount':
                    return formatCommon(value).currency(2);
                  default:
                    return value;
                }
              })()
            }));
            return {
              ...row,
              actionCodeCombined,
              subRow,
              rowId: `${rowId}`
            };
          })
        : [],
    [actionCode, props]
  );

  return { searchValue, onSearchChange: setSearchValue, data };
};
export { AdjustmentLimitInfo, useAdjustmentLimitInfo };
