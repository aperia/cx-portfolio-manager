import {
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { classnames } from 'app/helpers';
import { useAsyncThunkDispatch, useUnsavedChangeRegistry } from 'app/hooks';
import { Button, InlineMessage, useTranslation } from 'app/_libraries/_dls';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import {
  actionsWorkflowSetup,
  useExportShellProfileStatus,
  useOcsShellProfileStatus,
  useSelectElementsForManageAdjustmentSystem
} from 'pages/_commons/redux/WorkflowSetup';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, { useEffect, useMemo, useRef, useState } from 'react';
import GeneralInformation, {
  useShellGeneralInformation
} from '../GeneralInformation';
import {
  ParameterCode,
  RefDataWithDesc,
  ShellGeneralInformationModel
} from '../type';
import {
  AdjustmentLimitInfo,
  useAdjustmentLimitInfo
} from './AdjustmentLimitInfo';
import SelectShell from './SelectShell';
import Summary from './Summary';
import parseFormValues from './_parseFormValues';

export interface FormConfigJFS {
  isValid?: boolean;
  shellName?: string;
  generalInfo?: any;
  adjustmentLimitInfo?: any;
}
const ConfigJFSStep: React.FC<WorkflowSetupProps<FormConfigJFS>> = ({
  setFormValues,
  formValues,
  savedAt,
  isStuck
}) => {
  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;

  const dispatch = useAsyncThunkDispatch();

  const { t } = useTranslation();

  const { shellNames } = useSelectElementsForManageAdjustmentSystem();
  const shellName = useMemo(
    () =>
      shellNames.find(s => s.code === formValues?.shellName) as RefDataWithDesc,
    [shellNames, formValues?.shellName]
  );

  const [prevShellName, setPrevShellName] = useState(formValues?.shellName);
  const [generalInformation, setGeneralInformation] =
    useState<ShellGeneralInformationModel>();

  const [adjustmentLimitInfo, setAdjustmentLimitInfo] = useState<any>();

  const generalInformationProps =
    useShellGeneralInformation(generalInformation);

  const adjustmentLimitInformationProps =
    useAdjustmentLimitInfo(adjustmentLimitInfo);

  const { loading } = useOcsShellProfileStatus();
  const { loading: exportShellLoading } = useExportShellProfileStatus();

  const handleShellNameChange = async (_shellName: RefDataWithDesc) => {
    setFormValues({ shellName: _shellName.code });
  };
  const handleExportShell = () => {
    dispatch(actionsWorkflowSetup.exportShellProfile(formValues!.shellName!));
  };

  const isFormValid = useMemo(() => {
    return !!formValues?.shellName;
  }, [formValues?.shellName]);

  useEffect(() => {
    keepRef.current.setFormValues({ isValid: isFormValid });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isFormValid]);

  useEffect(() => {
    setPrevShellName(formValues?.shellName);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [savedAt]);

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_AUTOMATED_ADJUSTMENTS_JFS,
      priority: 1
    },
    [prevShellName !== formValues?.shellName]
  );

  useEffect(() => {
    const handleGetOcsShellProfile = async () => {
      if (!formValues?.shellName) return;

      try {
        const ocsShellProfileResp = await dispatch(
          actionsWorkflowSetup.getManageAdjustmentsSystemOcsShellProfile(
            formValues?.shellName
          )
        ).unwrap();
        setGeneralInformation(ocsShellProfileResp.generalInformation);
        setAdjustmentLimitInfo(ocsShellProfileResp.adjustmentLimitInformation);

        keepRef.current.setFormValues({
          generalInfo: ocsShellProfileResp.generalInformation,
          adjustmentLimitInfo: ocsShellProfileResp.adjustmentLimitInformation
        });
      } catch (error) {
        setGeneralInformation(undefined);
        setAdjustmentLimitInfo(undefined);
      }
    };

    handleGetOcsShellProfile();
  }, [dispatch, formValues?.shellName]);

  useEffect(() => {
    dispatch(
      actionsWorkflowSetup.getWorkflowMetadata([
        ParameterCode.ShellName,
        ParameterCode.ActionCode
      ])
    );
  }, [dispatch]);

  return (
    <div
      className={classnames('mt-24', {
        loading: loading || exportShellLoading
      })}
    >
      {isStuck && (
        <InlineMessage className="mb-0" variant="danger" withIcon>
          {t('txt_step_stuck_move_forward_message')}
        </InlineMessage>
      )}
      {!isEmpty(formValues?.shellName) && (
        <div className="absolute-top-right mt-24 mr-16">
          <Button
            variant="outline-primary"
            size="sm"
            onClick={handleExportShell}
          >
            {t('txt_export_shell')}
          </Button>
        </div>
      )}
      <SelectShell shellName={shellName} setShellName={handleShellNameChange} />
      {!isEmpty(formValues?.shellName) && (
        <GeneralInformation {...generalInformationProps} />
      )}
      {!isEmpty(formValues?.shellName) && (
        <AdjustmentLimitInfo
          {...adjustmentLimitInformationProps}
          shellName={shellName}
        />
      )}
    </div>
  );
};

const ExtraStaticConfigJFSStep =
  ConfigJFSStep as WorkflowSetupStaticProp<FormConfigJFS>;

ExtraStaticConfigJFSStep.summaryComponent = Summary;
ExtraStaticConfigJFSStep.defaultValues = { isValid: false };
ExtraStaticConfigJFSStep.parseFormValues = parseFormValues;

export default ExtraStaticConfigJFSStep;
