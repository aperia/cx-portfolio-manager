import { fireEvent, render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { queryAllByClass } from 'app/_libraries/_dls/test-utils';
import * as CommonRedux from 'pages/_commons/redux/Common/select-hooks';
import React from 'react';
import { RefDataWithDesc } from '../type';
import {
  AdjustmentLimitInfo,
  useAdjustmentLimitInfo
} from './AdjustmentLimitInfo';

type Props = {
  shellName: RefDataWithDesc;
  adjustmentLimitInfo: any;
  preview?: boolean;
};

const mockData = [
  {
    batchCode: 'K2',
    adjustmentTypeId: '253',
    shellMaximumAmount: 7561.64,
    managerDepartmentMaximumAmount: 1568.46,
    actionCode: 'Q'
  },
  {
    batchCode: 'Q5',
    adjustmentTypeId: '253',
    shellMaximumAmount: 5305.86,
    managerDepartmentMaximumAmount: 3308.97,
    actionCode: 'D'
  },
  {
    batchCode: 'C3',
    adjustmentTypeId: '251',
    shellMaximumAmount: 3270.44,
    managerDepartmentMaximumAmount: 7978.83,
    actionCode: 'D'
  },
  {
    batchCode: 'C3',
    adjustmentTypeId: '253',
    shellMaximumAmount: 4145.32,
    managerDepartmentMaximumAmount: 4010.11,
    actionCode: 'D'
  }
];

const Wrapper = ({ shellName, adjustmentLimitInfo, preview }: Props) => {
  const adjustmentLimitInformationProps =
    useAdjustmentLimitInfo(adjustmentLimitInfo);

  return (
    <AdjustmentLimitInfo
      {...adjustmentLimitInformationProps}
      shellName={shellName}
      preview={preview}
    />
  );
};

const renderComponent = (props: Props) => {
  return render(<Wrapper {...props} />);
};

const propsGenerator = (data?: any, preview?: boolean) => {
  return {
    shellName: { value: 'John', code: 'John', description: 'John' },
    preview,
    adjustmentLimitInfo: data
  };
};

jest.mock('pages/_commons/redux/WorkflowSetup', () => ({
  useSelectElementsForManageAdjustmentSystem: () => ({
    actionCode: [
      {
        value: '',
        description: 'None',
        code: '',
        text: 'None'
      },
      {
        value: 'D',
        description:
          'Deny the item if it exceeds the amount in the LIMIT field.',
        code: 'D'
      },
      {
        value: 'Q',
        description:
          'Assign the item to the pending queue if it exceeds the amount in the LIMIT field.',
        code: 'Q',
        text: 'Q - Assign the item to the pending queue if it exceeds the amount in the LIMIT field.'
      }
    ]
  })
}));
const mockUseSelectWindowDimension = jest.spyOn(
  CommonRedux,
  'useSelectWindowDimension'
);
const mockUseSelectWindowDimensionImplementation = (width: number) => {
  mockUseSelectWindowDimension.mockImplementation(() => ({ width } as any));
};

describe('test AdjustmentLimitInfo', () => {
  beforeEach(() => {
    mockUseSelectWindowDimensionImplementation(1400);
  });
  afterEach(() => {
    mockUseSelectWindowDimension.mockClear();
  });

  it('should be render no data', () => {
    mockUseSelectWindowDimensionImplementation(900);
    const { getByText } = renderComponent(propsGenerator());

    expect(getByText('txt_no_results_found')).toBeInTheDocument();
  });

  it('should be render data', () => {
    const { getByText, container } = renderComponent(propsGenerator(mockData));
    const expandBtn = queryAllByClass(container, /icon icon-plus/)[0];
    userEvent.click(expandBtn);
    expect(getByText('txt_shell_maximum_amount')).toBeInTheDocument();
  });

  it('should be render data as preview', () => {
    const { getByText } = renderComponent(propsGenerator(mockData, true));
    expect(getByText('txt_shell_maximum_amount')).toBeInTheDocument();
  });

  it('should be render empty when search value not match', () => {
    const { getByRole, baseElement, getByText } = renderComponent(
      propsGenerator(mockData)
    );
    const searchBox = () => getByRole('textbox');
    fireEvent.change(searchBox()!, { target: { value: 'something wrong' } });
    userEvent.click(
      baseElement.querySelector('i[class="icon icon-search"]') as Element
    );

    const clearAndReset = getByText('txt_clear_and_reset');
    userEvent.click(clearAndReset);
    expect(getByText('txt_shell_maximum_amount')).toBeInTheDocument();
  });
});
