import { fireEvent, RenderResult, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { mockThunkAction, renderWithMockStore } from 'app/utils';
import 'app/_libraries/_dls/test-utils/mocks/mockCanvas.ts';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import React from 'react';
import ConfigJFSStep, { FormConfigJFS } from './index';

(function mockDOMMatrix() {
  class DOMMatrixMock {
    scale = jest.fn();
    translate = jest.fn();
  }
  global.DOMMatrix = DOMMatrixMock as any;
})();

jest.mock('app/hooks', () => ({
  __esModule: true,
  ...jest.requireActual('app/hooks'),
  useAsyncThunkDispatch: () => (fn: unknown) => {
    return typeof fn === 'function' ? fn() : fn;
  }
}));

const renderComponent = async (
  props: any,
  workflowSetup?: any
): Promise<RenderResult> => {
  return await renderWithMockStore(<ConfigJFSStep {...props} />, {
    initialState: { workflowSetup }
  });
};

const generalInformationFixture = {
  terminalGroupId: '0020',
  profile: 'PROFILE3',
  departmentId: '0203',
  managerOperatorId: 'MOID02',
  supervisorOperatorId: '0032',
  operatorId: 'OID03'
};

const adjustmentLimitInformationsFixture = [
  {
    batchCode: 'K2',
    adjustmentTypeId: '253',
    shellMaximumAmount: 7561.64,
    managerDepartmentMaximumAmount: 1568.46,
    actionCode: 'Q'
  },
  {
    batchCode: 'Q5',
    adjustmentTypeId: '253',
    shellMaximumAmount: 5305.86,
    managerDepartmentMaximumAmount: 3308.97,
    actionCode: 'D'
  },
  {
    batchCode: 'C3',
    adjustmentTypeId: '251',
    shellMaximumAmount: 3270.44,
    managerDepartmentMaximumAmount: 7978.83,
    actionCode: 'D'
  },
  {
    batchCode: 'C3',
    adjustmentTypeId: '253',
    shellMaximumAmount: 4145.32,
    managerDepartmentMaximumAmount: 4010.11,
    actionCode: 'D'
  }
];

describe('ManageAdjustmentsSystemWorkflow > ConfigJFSStep > Index', () => {
  const mockActionsWorkflowSetup = mockThunkAction(actionsWorkflowSetup);
  it('Should render ', async () => {
    mockActionsWorkflowSetup('getWorkflowMetadata');
    const mockFn = (values: any) => {};
    const props = {
      formValues: {},
      setFormValues: mockFn
    } as WorkflowSetupProps<FormConfigJFS>;

    const wrapper = await renderComponent(props);
    expect(wrapper.queryByText('txt_shell_name')).toBeInTheDocument();
  });

  it('select shell', async () => {
    mockActionsWorkflowSetup('getWorkflowMetadata');
    jest
      .spyOn(actionsWorkflowSetup, 'getManageAdjustmentsSystemOcsShellProfile')
      .mockImplementation(
        () =>
          ({
            type: 'workflowSetup/getManageAdjustmentsSystemOcsShellProfile',
            unwrap: () => ({
              id: 364262666,
              shellName: '5',
              generalInformation: generalInformationFixture,
              adjustmentLimitInformations: adjustmentLimitInformationsFixture
            })
          } as never)
      );

    jest.useFakeTimers();
    const mockFn = (values: any) => {};
    const props = {
      formValues: { shellName: 'John' },
      setFormValues: mockFn
    } as WorkflowSetupProps<FormConfigJFS>;
    const wrapper = await renderComponent(props, {
      elementMetadata: {
        loading: false,
        elements: [
          {
            id: '331',
            name: 'shell.name',
            options: [
              { value: 'John', code: 'John', description: 'John' },
              { value: 'Maxel', code: 'Maxel', description: 'Maxel' }
            ]
          }
        ]
      },
      ocsShellProfile: {},
      exportShellProfile: {}
    });
    // find combobox
    const cbbIcon = wrapper.baseElement.querySelector(
      '.icon.icon-chevron-down'
    );
    fireEvent.mouseDown(cbbIcon!);
    const item = wrapper.getByText(/John/i);

    await waitFor(() => {
      userEvent.click(item);
    });
    jest.runAllTimers();
    expect(wrapper.queryByText('txt_shell_name')).toBeInTheDocument();
  });

  it('select shell with error', async () => {
    mockActionsWorkflowSetup('getWorkflowMetadata');
    jest
      .spyOn(actionsWorkflowSetup, 'getManageAdjustmentsSystemOcsShellProfile')
      .mockReturnValue(
        () =>
          ({
            type: 'workflowSetup/getManageAdjustmentsSystemOcsShellProfile',
            error: {}
          } as never)
      );

    jest.useFakeTimers();
    const mockFn = (values: any) => {};
    const props = {
      formValues: {},
      setFormValues: mockFn
    } as WorkflowSetupProps<FormConfigJFS>;
    const wrapper = await renderComponent(props, {
      elementMetadata: {
        loading: false,
        elements: [
          {
            id: '331',
            name: 'shell.name',
            options: [
              { value: 'John', code: 'John', description: 'John' },
              { value: 'Maxel', code: 'Maxel', description: 'Maxel' }
            ]
          }
        ]
      },
      ocsShellProfile: {},
      exportShellProfile: {}
    });
    // find combobox
    const cbbIcon = wrapper.baseElement.querySelector(
      '.icon.icon-chevron-down'
    );
    fireEvent.mouseDown(cbbIcon!);
    const item = wrapper.getByText(/John/i);

    await waitFor(() => {
      userEvent.click(item);
    });
    jest.runAllTimers();
    expect(wrapper.queryByText('txt_shell_name')).toBeInTheDocument();
  });

  it('should export shell', async () => {
    mockActionsWorkflowSetup('getWorkflowMetadata');
    const mockExportShell = mockActionsWorkflowSetup('exportShellProfile');
    jest.useFakeTimers();
    const mockFn = (values: any) => {};
    const props = {
      formValues: {
        shellName: { value: 'John', code: 'John', description: 'John' },
        generalInfo: generalInformationFixture,
        adjustmentLimitInfo: adjustmentLimitInformationsFixture
      },
      setFormValues: mockFn
    } as unknown as WorkflowSetupProps<FormConfigJFS>;
    const wrapper = await renderComponent(props, {
      elementMetadata: {
        loading: false,
        elements: [
          {
            id: '331',
            name: 'shell.name',
            options: [
              { value: 'John', code: 'John', description: 'John' },
              { value: 'Maxel', code: 'John', description: 'Maxel' }
            ]
          }
        ]
      },
      ocsShellProfile: {},
      exportShellProfile: {}
    });
    const exportShell = wrapper.getByText('txt_export_shell');
    userEvent.click(exportShell);
    expect(mockExportShell).toBeCalled();
  });

  it('should be render stuck message', async () => {
    mockActionsWorkflowSetup('getWorkflowMetadata');
    jest.useFakeTimers();
    const mockFn = (values: any) => {};
    const props = {
      isStuck: true,
      formValues: {
        shellName: { value: 'John', code: 'John', description: 'John' },
        generalInfo: generalInformationFixture,
        adjustmentLimitInfo: adjustmentLimitInformationsFixture
      },
      setFormValues: mockFn
    } as unknown as WorkflowSetupProps<FormConfigJFS>;
    const wrapper = await renderComponent(props, {
      elementMetadata: {
        loading: false,
        elements: [
          {
            id: '331',
            name: 'shell.name',
            options: [
              { value: 'John', code: 'John', description: 'John' },
              { value: 'Maxel', code: 'John', description: 'Maxel' }
            ]
          }
        ]
      },
      ocsShellProfile: {},
      exportShellProfile: {}
    });
    const stuckMessage = wrapper.getByText(
      'txt_step_stuck_move_forward_message'
    );
    userEvent.click(stuckMessage);
  });
});
