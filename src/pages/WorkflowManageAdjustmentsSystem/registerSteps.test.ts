import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { default as stepRegistry } from '../_commons/WorkflowSetup/registries/stepRegistry';

const registerFunc = jest.spyOn(stepRegistry, 'registerStep');

describe('ManageAdjustmentsSystemWorkflow > registerSteps', () => {
  it('Should register components', async () => {
    await import('./registerSteps');

    expect(registerFunc).toBeCalledWith(
      'GetStartedManageAdjustmentsSystem',
      expect.anything(),
      [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_ADJUSTMENT_SYSTEM__GET_STARTED__EXISTED_WORKFLOW,
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_ADJUSTMENT_SYSTEM__GET_STARTED__EXISTED_WORKFLOW__STUCK
      ]
    );

    expect(registerFunc).toBeCalledWith(
      'ConfigJFSManageAdjustmentsSystem',
      expect.anything(),
      [UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_AUTOMATED_ADJUSTMENTS_JFS]
    );

    expect(registerFunc).toBeCalledWith(
      'ConfigFLSADJManageAdjustmentsSystem',
      expect.anything(),
      [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_AUTOMATED_ADJUSTMENTS_FLS_ADJ
      ]
    );

    expect(registerFunc).toBeCalled();

    expect(registerFunc).toBeCalledWith(
      'ConfigFDSAManageAdjustmentsSystem',
      expect.anything(),
      [UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_AUTOMATED_ADJUSTMENTS_FDSA]
    );
  });
});
