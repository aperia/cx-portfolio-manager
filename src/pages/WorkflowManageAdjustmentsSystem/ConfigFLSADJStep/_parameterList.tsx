import { ProfileTransactions } from 'app/types';
import React from 'react';
import { ParameterName } from '../type';

export interface Parameter {
  id: keyof ProfileTransactions;
  bussinessName: ParameterName;
  greenScreenName: string;
  moreInfoText?: string;
  moreInfo?: React.ReactNode;
}

export const MORE_INFO = {
  BatchCode: 'The parameter defines the code representing the batch.',
  TransactionCode:
    'The parameter defines the code representing the monetary transaction.',
  MaximumAmount:
    'The Maximum Amount parameter defines the maximum amount the operator can adjust for this transaction code.',
  ActionCode:
    'The Action Code parameter represents the action to take when the adjustment amount exceeds the amount in the Limit field.'
};

export const parameters: Parameter[] = [
  {
    id: 'batchCode',
    bussinessName: ParameterName.BatchCode,
    greenScreenName: 'BC',
    moreInfo: MORE_INFO.BatchCode
  },
  {
    id: 'transactionCode',
    bussinessName: ParameterName.TransactionCode,
    greenScreenName: 'TRN',
    moreInfo: MORE_INFO.TransactionCode
  },
  {
    id: 'maximumAmount',
    bussinessName: ParameterName.MaximumAmount,
    greenScreenName: 'Limit',
    moreInfo: MORE_INFO.MaximumAmount
  },
  {
    id: 'actionCode',
    bussinessName: ParameterName.ActionCode,
    greenScreenName: 'AC',
    moreInfo: MORE_INFO.ActionCode
  }
];
