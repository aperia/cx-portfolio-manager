import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import { classnames, matchSearchValue } from 'app/helpers';
import { ProfileDetail, ProfileTransactions } from 'app/types';
import { SortType, useTranslation } from 'app/_libraries/_dls';
import { isEmpty, orderBy } from 'app/_libraries/_dls/lodash';
import { differenceWith } from 'lodash';
import ClearAndResetButton from 'pages/_commons/Utils/ClearAndResetButton';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { FormConfigFLSADJ } from '.';
import ProfileSection from './ProfileSection';

export interface FilterProfileDetail extends ProfileDetail {
  filteredTransactions: ProfileTransactions[];
}
export interface ProfileListProps {
  profiles: ProfileDetail[];
}

const ProfileList: React.FC<
  WorkflowSetupProps<FormConfigFLSADJ> & ProfileListProps
> = props => {
  const { t } = useTranslation();
  const { setFormValues, profiles, stepId, selectedStep } = props;
  const prevRef = useRef({ profiles });
  const searchRef = useRef<any>(null);

  const [searchValue, setSearchValue] = useState<string>('');
  const [shouldResetFilter, setShouldResetFilter] = useState(false);

  const [expandList, setExpandList] = useState<Record<string, boolean>>({});
  const [selectedList, setSelectedList] = useState<Record<string, string[]>>(
    {}
  );
  const [sortBy, setSortBy] = useState<Record<string, SortType>>({});

  const filteredProfiles = useMemo(
    () =>
      profiles
        .map(p => {
          return {
            ...p,
            filteredTransactions: p.transactions
              .map(m => ({
                ...m,
                subSystemAdj: p.subSystemAdj,
                profileName: p.code
              }))
              .filter(w =>
                matchSearchValue(
                  [
                    w.subSystemAdj,
                    w.profileName,
                    w.batchCode,
                    w.transactionCode,
                    w.status
                  ],
                  searchValue
                )
              )
          } as FilterProfileDetail;
        })
        .filter(f => f.filteredTransactions.length > 0),
    [profiles, searchValue]
  );

  const handleExpandByDefault = useCallback(() => {
    const list: Record<string, boolean> = {};
    profiles.forEach(profile => {
      list[profile.code] = true;
    });
    setExpandList(list);
    setShouldResetFilter(true);
  }, [profiles]);

  const handleClearFilter = useCallback(() => {
    setSearchValue('');
    handleExpandByDefault();
  }, [handleExpandByDefault]);

  const handleSearch = (val: string) => {
    setSearchValue(val);
    handleExpandByDefault();
  };

  useEffect(() => {
    if (stepId !== selectedStep) {
      setShouldResetFilter(true);
      setSelectedList({});
      handleClearFilter();
    }
  }, [stepId, selectedStep, handleClearFilter]);

  useEffect(() => {
    if (shouldResetFilter) {
      setShouldResetFilter(false);
    }
  }, [shouldResetFilter]);

  useEffect(() => {
    !searchValue && searchRef?.current?.clear();
  }, [searchValue]);

  useEffect(() => {
    const _prevRef = prevRef.current;
    const prevProfile = _prevRef.profiles;

    const prevLength = prevProfile.length;
    const currLength = profiles.length;

    const isFirstAdd = currLength === prevLength && currLength === 1;

    const diff = isFirstAdd
      ? profiles[0].code
      : differenceWith(
          profiles,
          prevProfile,
          (o1, o2) => o1.code === o2.code
        )?.[0]?.code;

    if (!!diff) {
      setExpandList(list => ({ ...list, [diff]: true }));
    }

    return () => {
      _prevRef.profiles = profiles;
    };
  }, [profiles]);

  const handleClickExpand = (id: string, isExpand: boolean) => () => {
    setExpandList(list => ({ ...list, [id]: isExpand }));
  };

  const handleClickSort = (id: string, sort: SortType) => {
    setSortBy(list => ({ ...list, [id]: sort }));
  };

  const handleSetSelectedList = (id: string, _list: string[]) => {
    setSelectedList(list => ({ ...list, [id]: _list }));
  };
  const handleUpdateProfileDetails = (profileDetails?: ProfileDetail) => {
    if (isEmpty(profileDetails)) return;
    const _profiles = [...profiles];
    const idx = _profiles.findIndex(
      f => f.subSystemAdj === profileDetails?.subSystemAdj
    );
    _profiles[idx] = profileDetails!;

    setFormValues({ profiles: orderBy(_profiles, ['code']) });
  };

  return (
    <>
      <div className="d-flex align-items-center justify-content-between mb-16">
        <h5>{t('txt_config_manage_adjustments_system_profile_list')}</h5>
        <SimpleSearch
          ref={searchRef}
          placeholder={t('txt_type_to_search')}
          onSearch={handleSearch}
          popperElement={
            <>
              <p className="color-grey">{t('txt_search_description')}</p>
              <ul className="search-field-item list-unstyled">
                <li className="mt-16">
                  {t('txt_config_manage_adjustments_system_batchCode')}
                </li>
                <li className="mt-16">
                  {t('txt_config_manage_adjustments_system_profileName')}
                </li>
                <li className="mt-16">{t('txt_status')}</li>
                <li className="mt-16">
                  {t('txt_config_manage_adjustments_system_subsystemAdj')}
                </li>
                <li className="mt-16">
                  {t('txt_config_manage_adjustments_system_transactionCode')}
                </li>
              </ul>
            </>
          }
        />
      </div>
      {searchValue && !isEmpty(filteredProfiles) && (
        <div className="d-flex justify-content-end mb-16 mr-n8">
          <ClearAndResetButton
            small
            onClearAndReset={() => handleClearFilter()}
          />
        </div>
      )}
      {isEmpty(filteredProfiles) && (
        <div className="d-flex flex-column justify-content-center pt-40 mb-32">
          <NoDataFound
            id="newMethod_notfound"
            hasSearch
            title={t('txt_no_results_found')}
            linkTitle={t('txt_clear_and_reset')}
            onLinkClicked={() => handleClearFilter()}
          />
        </div>
      )}
      {!isEmpty(filteredProfiles) && (
        <div className="mt-16">
          {filteredProfiles.map((profile, idx) => {
            const isExpand = !!expandList[profile.code];

            return (
              <div
                key={profile.code}
                className={classnames(idx !== 0 && 'mt-24 pt-24 border-top')}
              >
                <ProfileSection
                  {...props}
                  profile={profile}
                  isExpand={isExpand}
                  resetFilter={shouldResetFilter}
                  searchValue={searchValue}
                  selectedList={selectedList[profile.code] || []}
                  sortBy={sortBy[profile.code]}
                  clearSearchValue={() => setSearchValue('')}
                  setSortBy={handleClickSort}
                  onClickExpand={handleClickExpand(profile.code, !isExpand)}
                  setSelectedList={handleSetSelectedList}
                  onUpdateProfileDetails={handleUpdateProfileDetails}
                />
              </div>
            );
          })}
        </div>
      )}
    </>
  );
};

export default ProfileList;
