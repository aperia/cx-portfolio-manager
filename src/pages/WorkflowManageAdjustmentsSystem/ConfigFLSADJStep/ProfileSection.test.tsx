import { act, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { TransactionStatus } from 'app/constants/enums';
import * as helper from 'app/helpers/isDevice';
import { mockUseModalRegistryFnc, renderWithMockStore } from 'app/utils';
import { queryAllByClass } from 'app/_libraries/_dls/test-utils';
import React from 'react';
import { mockElements } from './index.test';
import ProfileSection from './ProfileSection';

jest.mock(
  'pages/WorkflowManageAdjustmentsSystem/ConfigFLSADJStep/DeleteRecordsModal',
  () => ({
    __esModule: true,
    default: ({ onClose, onDelete }: any) => {
      return (
        <div>
          <div>DeleteRecordsModal</div>
          <button onClick={() => onClose()}>onClose</button>
          <button onClick={() => onDelete()}>onDelete</button>
        </div>
      );
    }
  })
);
jest.mock(
  'pages/WorkflowManageAdjustmentsSystem/ConfigFLSADJStep/CancelRecordsModal',
  () => ({
    __esModule: true,
    default: ({ onClose, onConfirm }: any) => {
      return (
        <div>
          <div>CancelRecordsModal</div>
          <button onClick={() => onClose()}>onClose</button>
          <button onClick={() => onConfirm()}>onConfirm</button>
        </div>
      );
    }
  })
);
jest.mock(
  'pages/WorkflowManageAdjustmentsSystem/ConfigFLSADJStep/RecordModal',
  () => ({
    __esModule: true,
    default: ({ onClose }: any) => {
      return (
        <div>
          <div>RecordModal</div>
          <button
            onClick={() =>
              onClose({
                rowId: '3',
                batchCode: 'B',
                actionCode: 'A'
              })
            }
          >
            edit Record
          </button>
          <button onClick={() => onClose()}>onClose</button>
        </div>
      );
    }
  })
);
const profiles = [
  {
    code: 'A001',
    text: 'A001',
    transactions: [
      {
        rowId: '1',
        status: TransactionStatus.NotUpdated,
        batchCode: 'AB',
        actionCode: 'A',
        maximumAmount: '123',
        originalProfile: {
          status: TransactionStatus.NotUpdated,
          batchCode: 'B',
          actionCode: 'A',
          maximumAmount: '123'
        }
      },
      {
        rowId: '2',
        status: TransactionStatus.New,
        batchCode: 'B',
        actionCode: 'A',
        originalProfile: {
          status: TransactionStatus.NotUpdated,
          batchCode: 'B',
          actionCode: 'A'
        }
      },
      {
        rowId: '3',
        status: TransactionStatus.Updated,
        batchCode: 'B',
        actionCode: 'A',
        originalProfile: {
          rowId: '3',
          status: TransactionStatus.Updated,
          batchCode: 'B',
          actionCode: 'A'
        }
      },
      {
        rowId: '4',
        status: TransactionStatus.MarkedForDeletion,
        batchCode: 'B',
        actionCode: 'A',
        originalProfile: {
          status: TransactionStatus.NotUpdated,
          batchCode: 'B',
          actionCode: 'A'
        }
      },
      {},
      {},
      {},
      {},
      {},
      {},
      {},
      {}
    ] as any[]
  },
  {
    code: 'A001',
    text: 'A001',
    transactions: [
      {
        status: TransactionStatus.NotUpdated,
        batchCode: 'AB',
        actionCode: 'A',
        originalProfile: {
          status: TransactionStatus.NotUpdated,
          batchCode: 'B',
          actionCode: 'A'
        }
      }
    ] as any[]
  },
  {
    code: 'A003',
    text: 'A003',
    transactions: [
      {
        rowId: 'A003_1',
        status: TransactionStatus.MarkedForDeletion,
        batchCode: 'AB',
        actionCode: 'A',
        originalProfile: {
          status: TransactionStatus.MarkedForDeletion,
          batchCode: 'B',
          actionCode: 'A'
        }
      }
    ] as any[]
  }
];
const mockUseModalRegistry = mockUseModalRegistryFnc();

const renderComponent = async (
  props: any,
  workflowSetup?: any
): Promise<RenderResult> => {
  return await renderWithMockStore(<ProfileSection {...props} />, {
    initialState: { workflowSetup }
  });
};
const mockHelper: any = helper;
describe('ManageAdjustmentsSystemWorkflow > ConfigFLSADJStep > ProfileSection', () => {
  beforeEach(() => {
    mockHelper.isDevice = false;

    mockUseModalRegistry.mockImplementation(() => [true, false]);
  });

  afterEach(() => {
    mockUseModalRegistry.mockClear();
  });

  const defaultWorkflowSetup = {
    elementMetadata: { elements: mockElements },
    profileTransactions: { loading: true, error: false }
  };

  it('should render list', async () => {
    const props = {
      formValues: {},
      stepId: '1',
      selectedStep: '2',
      setFormValues: jest.fn(),
      onUpdateProfileDetails: jest.fn(),
      clearSearchValue: jest.fn(),
      onClickExpand: jest.fn(),
      setSelectedList: jest.fn(),
      setSortBy: jest.fn(),
      selectedList: [],
      sortBy: { id: 'batchCode' },
      resetFilter: true,
      profile: {
        ...profiles[0],
        filteredTransactions: profiles[0].transactions
      },

      profiles: profiles
    } as any;
    mockHelper.isDevice = true;

    const { baseElement, getByText, getAllByRole, rerender } =
      await renderComponent(props, defaultWorkflowSetup);

    const checkboxAll = getAllByRole('checkbox')[0];
    act(() => {
      userEvent.click(checkboxAll);
    });

    const body = baseElement.querySelector('tbody[class="dls-grid-body"]');
    const expandBtn = queryAllByClass(body as HTMLElement, /icon icon-plus/)[0];
    act(() => {
      userEvent.click(expandBtn);
    });
    userEvent.click(
      getByText('txt_config_manage_adjustments_system_batchCode')
    );

    rerender(
      <ProfileSection
        {...props}
        selectedList={['1', '2']}
        isExpand={true}
        searchValue={'a'}
      />
    );
    expect(
      getByText('txt_manage_ocs_shells_step_3_bulk_delete')
    ).toBeInTheDocument();

    userEvent.click(getByText('txt_manage_ocs_shells_step_3_bulk_delete'));
  });

  it('should render list without filteredTransactions', async () => {
    const props = {
      formValues: {},
      stepId: '1',
      selectedStep: '2',
      setFormValues: jest.fn(),
      onUpdateProfileDetails: jest.fn(),
      clearSearchValue: jest.fn(),
      onClickExpand: jest.fn(),
      setSelectedList: jest.fn(),
      setSortBy: jest.fn(),
      selectedList: [],
      sortBy: { id: 'batchCode' },
      resetFilter: true,
      profile: {
        ...profiles[0],
        filteredTransactions: []
      },

      profiles: profiles
    } as any;
    mockHelper.isDevice = true;

    const { getByText, getAllByRole, rerender } = await renderComponent(
      props,
      defaultWorkflowSetup
    );

    const checkboxAll = getAllByRole('checkbox')[0];
    act(() => {
      userEvent.click(checkboxAll);
    });

    userEvent.click(
      getByText('txt_config_manage_adjustments_system_batchCode')
    );

    act(() => {
      userEvent.click(checkboxAll);
    });

    rerender(
      <ProfileSection
        {...props}
        selectedList={['1', '2']}
        isExpand={true}
        searchValue={'a'}
      />
    );
  });

  it('should render Delete record', async () => {
    const props = {
      formValues: {},
      setFormValues: jest.fn(),
      onUpdateProfileDetails: jest.fn(),
      clearSearchValue: jest.fn(),
      onClickExpand: jest.fn(),
      setSelectedList: jest.fn(),
      setSortBy: jest.fn(),
      selectedList: [],
      profile: {
        ...profiles[0],
        filteredTransactions: profiles[0].transactions
      },

      profiles: profiles
    } as any;

    const {
      getByText,
      getAllByText,

      getAllByRole,
      rerender
    } = await renderComponent(props, defaultWorkflowSetup);

    const checkboxAll = getAllByRole('checkbox')[0];
    act(() => {
      userEvent.click(checkboxAll);
    });

    userEvent.click(getAllByText(/txt_delete/)[0]);
    expect(getByText('DeleteRecordsModal')).toBeInTheDocument();

    userEvent.click(getByText('onClose'));
    expect(props.onUpdateProfileDetails).not.toHaveBeenCalled();

    rerender(
      <ProfileSection {...props} selectedList={['1', '2']} isExpand={true} />
    );
    expect(
      getByText('txt_manage_ocs_shells_step_3_bulk_delete')
    ).toBeInTheDocument();

    act(() => {
      userEvent.click(getByText('txt_manage_ocs_shells_step_3_bulk_delete'));
    });
    expect(getByText('DeleteRecordsModal')).toBeInTheDocument();

    act(() => {
      userEvent.click(getByText('onDelete'));
    });
    expect(props.onUpdateProfileDetails).toHaveBeenCalled();
  });
  it('should render Cancel record', async () => {
    const props = {
      formValues: {},
      setFormValues: jest.fn(),
      onUpdateProfileDetails: jest.fn(),
      clearSearchValue: jest.fn(),
      onClickExpand: jest.fn(),
      setSelectedList: jest.fn(),
      setSortBy: jest.fn(),
      selectedList: [],
      profile: {
        ...profiles[2],
        filteredTransactions: profiles[2].transactions
      },

      profiles: profiles
    } as any;

    const { getByText, getAllByText } = await renderComponent(
      props,
      defaultWorkflowSetup
    );

    userEvent.click(getAllByText(/txt_cancel/)[0]);
    expect(getByText('CancelRecordsModal')).toBeInTheDocument();

    userEvent.click(getByText('onClose'));
    expect(props.onUpdateProfileDetails).not.toHaveBeenCalled();

    userEvent.click(getAllByText(/txt_cancel/)[0]);
    expect(getByText('CancelRecordsModal')).toBeInTheDocument();
    userEvent.click(getByText('onConfirm'));
    expect(props.onUpdateProfileDetails).toHaveBeenCalled();
  });

  it('should render add record', async () => {
    const props = {
      formValues: {},
      setFormValues: jest.fn(),
      onUpdateProfileDetails: jest.fn(),
      clearSearchValue: jest.fn(),
      onClickExpand: jest.fn(),
      setSelectedList: jest.fn(),
      setSortBy: jest.fn(),
      selectedList: [],
      profile: {
        ...profiles[0],
        filteredTransactions: profiles[0].transactions
      },

      profiles: profiles
    } as any;

    const { getByText, getAllByText } = await renderComponent(
      props,
      defaultWorkflowSetup
    );

    userEvent.click(getAllByText(/txt_edit/)[0]);
    expect(getByText('RecordModal')).toBeInTheDocument();

    userEvent.click(getByText('onClose'));
    expect(props.onUpdateProfileDetails).not.toHaveBeenCalled();

    userEvent.click(getAllByText(/txt_edit/)[0]);
    expect(getByText('RecordModal')).toBeInTheDocument();
    userEvent.click(getByText('edit Record'));
    expect(props.onUpdateProfileDetails).toHaveBeenCalled();
  });

  it('should render edit record - edit record with status New', async () => {
    const props = {
      formValues: {},
      setFormValues: jest.fn(),
      onUpdateProfileDetails: jest.fn(),
      clearSearchValue: jest.fn(),
      onClickExpand: jest.fn(),
      setSelectedList: jest.fn(),
      setSortBy: jest.fn(),
      selectedList: [],
      profile: {
        ...profiles[0],
        filteredTransactions: profiles[0].transactions
      },

      profiles: profiles
    } as any;

    const { getByText, getAllByText } = await renderComponent(
      props,
      defaultWorkflowSetup
    );

    userEvent.click(getAllByText(/txt_edit/)[1]);
    expect(getByText('RecordModal')).toBeInTheDocument();
    userEvent.click(getByText('edit Record'));
    expect(props.onUpdateProfileDetails).toHaveBeenCalled();
  });

  it('should render edit record - edit record with status Updated', async () => {
    const props = {
      formValues: {},
      setFormValues: jest.fn(),
      onUpdateProfileDetails: jest.fn(),
      clearSearchValue: jest.fn(),
      onClickExpand: jest.fn(),
      setSelectedList: jest.fn(),
      setSortBy: jest.fn(),
      selectedList: [],
      profile: {
        ...profiles[0],
        filteredTransactions: profiles[0].transactions
      },

      profiles: profiles
    } as any;

    const { getByText, getAllByText } = await renderComponent(
      props,
      defaultWorkflowSetup
    );

    userEvent.click(getAllByText(/txt_edit/)[2]);
    expect(getByText('RecordModal')).toBeInTheDocument();
    userEvent.click(getByText('edit Record'));
    expect(props.onUpdateProfileDetails).toHaveBeenCalled();
  });

  it('should render add record', async () => {
    const props = {
      formValues: {},
      setFormValues: jest.fn(),
      onUpdateProfileDetails: jest.fn(),
      clearSearchValue: jest.fn(),
      onClickExpand: jest.fn(),
      setSelectedList: jest.fn(),
      setSortBy: jest.fn(),
      selectedList: [],
      profile: {
        ...profiles[0],
        filteredTransactions: profiles[0].transactions
      },

      profiles: profiles
    } as any;

    const { getByText } = await renderComponent(props, defaultWorkflowSetup);

    // userEvent.click(getAllByText(/txt_edit/)[2]);
    // expect(getByText('RecordModal')).toBeInTheDocument();
    userEvent.click(
      getByText('txt_config_manage_adjustments_system_add_record')
    );
    expect(getByText('RecordModal')).toBeInTheDocument();
    userEvent.click(getByText('edit Record'));
    // expect(props.onUpdateProfileDetails).toHaveBeenCalled();
  });

  it('click hide profile', async () => {
    const props = {
      formValues: {
        profiles: [{ ...profiles[1] }] as any
      },
      setFormValues: jest.fn(),
      onUpdateProfileDetails: jest.fn(),
      clearSearchValue: jest.fn(),
      onClickExpand: jest.fn(),
      setSelectedList: jest.fn(),
      setSortBy: jest.fn(),
      selectedList: [],
      profile: {
        ...profiles[1],
        filteredTransactions: profiles[1].transactions
      },

      profiles: profiles
    } as any;

    const { getByText, getAllByRole } = await renderComponent(
      props,
      defaultWorkflowSetup
    );

    const checkboxAll = getAllByRole('checkbox')[0];
    act(() => {
      userEvent.click(checkboxAll);
    });
    act(() => {
      userEvent.click(checkboxAll);
    });
    act(() => {
      userEvent.click(checkboxAll);
    });
    userEvent.click(
      getByText('txt_config_manage_adjustments_system_hide_profile')
    );
    expect(props.setFormValues).toHaveBeenCalled();
  });
});
