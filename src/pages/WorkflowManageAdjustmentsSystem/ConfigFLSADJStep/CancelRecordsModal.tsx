import ModalRegistry from 'app/components/ModalRegistry';
import {
  Button,
  ModalBody,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import React from 'react';

interface IProps {
  id: string;
  show: boolean;
  onClose: () => void;
  onConfirm: () => void;
}

const CancelRecordsModal: React.FC<IProps> = ({
  id,
  show,

  onClose,
  onConfirm
}) => {
  const { t } = useTranslation();

  return (
    <ModalRegistry
      id={id}
      show={show}
      onAutoClosedAll={onClose}
      classes={{ content: '' }}
    >
      <ModalHeader border closeButton onHide={onClose}>
        <ModalTitle>{t('txt_confirm_cancel')}</ModalTitle>
      </ModalHeader>
      <ModalBody>{t('txt_cancel_record')}</ModalBody>
      <div className="dls-modal-footer modal-footer">
        <Button variant="secondary" onClick={onClose}>
          {t('txt_cancel')}
        </Button>
        <Button variant="danger" onClick={onConfirm}>
          {t('txt_confirm')}
        </Button>
      </div>
    </ModalRegistry>
  );
};

export default CancelRecordsModal;
