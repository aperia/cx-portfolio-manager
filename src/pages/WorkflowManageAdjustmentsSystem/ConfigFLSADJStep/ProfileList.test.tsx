import { RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { TransactionStatus } from 'app/constants/enums';
import { mockUseModalRegistryFnc, renderWithMockStore } from 'app/utils';
import mockDevice from 'app/utils/_mockHelperConstant/mockDevice';
import React from 'react';
import { FormConfigFLSADJ } from '.';
import { mockElements } from './index.test';
import ProfileList, { ProfileListProps } from './ProfileList';
import { ProfileSectionProps } from './ProfileSection';

jest.mock('./ProfileSection', () => ({
  __esModule: true,
  default: ({
    onUpdateProfileDetails,
    clearSearchValue,
    onClickExpand,
    setSelectedList,
    setSortBy
  }: WorkflowSetupProps<FormConfigFLSADJ> & ProfileSectionProps) => {
    return (
      <div>
        <div>Mocked ProfileSection</div>
        <div
          onClick={() =>
            onUpdateProfileDetails &&
            onUpdateProfileDetails({
              code: '2',
              subSystemAdj: 's',
              transactions: []
            })
          }
        >
          Add Profile
        </div>
        <div
          onClick={() =>
            onUpdateProfileDetails && onUpdateProfileDetails(undefined)
          }
        >
          Add Profile 2
        </div>
        <div onClick={() => clearSearchValue && clearSearchValue()}>
          Clear Search
        </div>
        <div onClick={() => onClickExpand && onClickExpand()}>
          onClickExpand
        </div>
        <div onClick={() => setSortBy && setSortBy('', { id: '' })}>
          setSortBy
        </div>
        <div onClick={() => setSelectedList && setSelectedList('', [])}>
          setSelectedList
        </div>
      </div>
    );
  }
}));
const mockUseModalRegistry = mockUseModalRegistryFnc();

const renderComponent = async (
  props: any,
  workflowSetup?: any
): Promise<RenderResult> => {
  return await renderWithMockStore(<ProfileList {...props} />, {
    initialState: { workflowSetup }
  });
};

describe('ManageAdjustmentsSystemWorkflow > ConfigFLSADJStep > ProfileList', () => {
  beforeEach(() => {
    mockDevice.isDevice = false;

    mockUseModalRegistry.mockImplementation(() => [true, false]);
  });

  afterEach(() => {
    mockUseModalRegistry.mockClear();
  });

  const defaultWorkflowSetup = {
    elementMetadata: { elements: mockElements },
    profileTransactions: { loading: true, error: false }
  };
  it('should render list', async () => {
    const props = {
      formValues: {},
      setFormValues: jest.fn(),
      profiles: [
        {
          code: 'A001',
          text: 'A001',
          transactions: [
            {
              status: TransactionStatus.NotUpdated,
              batchCode: 'AB',
              actionCode: 'A',
              originalProfile: {
                status: TransactionStatus.NotUpdated,
                batchCode: 'B',
                actionCode: 'A'
              }
            },
            {
              status: TransactionStatus.New,
              batchCode: 'B',
              actionCode: 'A',
              originalProfile: {
                status: TransactionStatus.NotUpdated,
                batchCode: 'B',
                actionCode: 'A'
              }
            },
            {
              status: TransactionStatus.Updated,
              batchCode: 'B',
              actionCode: 'A',
              originalProfile: {
                status: TransactionStatus.NotUpdated,
                batchCode: 'B',
                actionCode: 'A'
              }
            },
            {
              status: TransactionStatus.MarkedForDeletion,
              batchCode: 'B',
              actionCode: 'A',
              originalProfile: {
                status: TransactionStatus.NotUpdated,
                batchCode: 'B',
                actionCode: 'A'
              }
            }
          ] as any[]
        },
        {
          code: 'A001',
          text: 'A001',
          transactions: [
            {
              status: TransactionStatus.NotUpdated,
              batchCode: 'AB',
              actionCode: 'A',
              originalProfile: {
                status: TransactionStatus.NotUpdated,
                batchCode: 'B',
                actionCode: 'A'
              }
            },
            {
              status: TransactionStatus.New,
              batchCode: 'B',
              actionCode: 'A',
              originalProfile: {
                status: TransactionStatus.NotUpdated,
                batchCode: 'B',
                actionCode: 'A'
              }
            },
            {
              status: TransactionStatus.Updated,
              batchCode: 'B',
              actionCode: 'A',
              originalProfile: {
                status: TransactionStatus.NotUpdated,
                batchCode: 'B',
                actionCode: 'A'
              }
            },
            {
              status: TransactionStatus.MarkedForDeletion,
              batchCode: 'B',
              actionCode: 'A',
              originalProfile: {
                status: TransactionStatus.NotUpdated,
                batchCode: 'B',
                actionCode: 'A'
              }
            }
          ] as any[]
        }
      ]
    } as any;
    const {
      baseElement,
      getByText,
      getAllByText,
      queryByText,
      getByPlaceholderText
    } = await renderComponent(props, defaultWorkflowSetup);

    // expand all profile list
    let expands = baseElement.querySelectorAll('.icon.icon-plus');
    expands.forEach(userEvent.click as any);

    // expand all transactions
    expands = baseElement.querySelectorAll('.icon.icon-plus');
    expands.forEach(userEvent.click as any);

    userEvent.click(getAllByText('Add Profile')[0]);
    userEvent.click(getAllByText('Add Profile 2')[0]);
    userEvent.click(getAllByText('Clear Search')[0]);
    userEvent.click(getAllByText('onClickExpand')[0]);
    userEvent.click(getAllByText('setSortBy')[0]);
    userEvent.click(getAllByText('setSelectedList')[0]);

    const searchBox = getByPlaceholderText('txt_type_to_search');

    userEvent.type(searchBox, 'A');
    userEvent.click(
      searchBox.parentElement!.querySelector(
        '.icon-simple-search .icon.icon-search'
      )!
    );
    userEvent.click(getByText('txt_clear_and_reset'));

    userEvent.type(searchBox, 'notfound');
    userEvent.click(
      searchBox.parentElement!.querySelector(
        '.icon-simple-search .icon.icon-search'
      )!
    );

    expect(
      getByText('txt_no_results_found txt_adjust_your_search_criteria')
    ).toBeInTheDocument();

    userEvent.click(getByText('txt_clear_and_reset'));
    expect(
      queryByText('txt_no_results_found txt_adjust_your_search_criteria')
    ).not.toBeInTheDocument();

    expect(getAllByText('Mocked ProfileSection')[0]).toBeInTheDocument();
  });

  it('should render summary in device', async () => {
    mockDevice.isDevice = true;

    const props = {
      stepId: '1',
      selectedStep: '2',
      profiles: [
        {
          code: '001',
          text: '001',
          transactions: [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}] as any[]
        },
        { code: '002', text: '002', transactions: [] as any[] }
      ]
    } as WorkflowSetupProps<FormConfigFLSADJ> & ProfileListProps;
    const { baseElement, getAllByText } = await renderComponent(
      props,
      defaultWorkflowSetup
    );

    // expand all profile list
    let expands = baseElement.querySelectorAll('.icon.icon-plus');
    expands.forEach(userEvent.click as any);

    // expand all transactions
    expands = baseElement.querySelectorAll('.icon.icon-plus');
    expands.forEach(userEvent.click as any);

    expect(getAllByText('Mocked ProfileSection')[0]).toBeInTheDocument();
  });
});
