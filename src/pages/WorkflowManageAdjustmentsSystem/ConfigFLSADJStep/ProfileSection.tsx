import { TransactionStatus } from 'app/constants/enums';
import { classnames, getSelectedRecord, isDevice } from 'app/helpers';
import { useCheckAllPagination } from 'app/hooks';
import { usePagination } from 'app/hooks/usePagination';
import { ProfileDetail, ProfileTransactions } from 'app/types';
import {
  Button,
  Grid,
  Icon,
  SortType,
  ToggleButtonConfig,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import {
  differenceWith,
  isEmpty,
  isEqual,
  isFunction
} from 'app/_libraries/_dls/lodash';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import { useSelectElementsForManageAdjustmentSystem } from 'pages/_commons/redux/WorkflowSetup';
import Paging from 'pages/_commons/Utils/Paging';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { FormConfigFLSADJ } from '.';
import CancelRecordsModal from './CancelRecordsModal';
import { columns } from './constant';
import DeleteRecordsModal from './DeleteRecordsModal';
import { FilterProfileDetail } from './ProfileList';
import ProfileListSubGrid from './ProfileListSubGrid';
import RecordModal from './RecordModal';

export interface ProfileSectionProps {
  isExpand: boolean;
  profile: FilterProfileDetail;
  resetFilter: boolean;
  searchValue?: string;
  selectedList: string[];
  sortBy: SortType;
  clearSearchValue: VoidFunction;
  setSortBy: (id: string, sort: SortType) => void;
  onClickExpand: VoidFunction;
  setSelectedList: (id: string, list: string[]) => void;
  onUpdateProfileDetails?: (profileDetail?: ProfileDetail) => void;
}
const ProfileSection: React.FC<
  WorkflowSetupProps<FormConfigFLSADJ> & ProfileSectionProps
> = ({
  stepId,
  selectedStep,
  isExpand,
  profile,
  resetFilter,
  searchValue,
  selectedList,
  sortBy,
  clearSearchValue,
  setSortBy,
  onClickExpand,
  setSelectedList,
  onUpdateProfileDetails,
  setFormValues,
  formValues
}) => {
  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();
  const {
    text: profileName,
    subSystemAdj,
    transactions,
    filteredTransactions
  } = profile;

  const profileElements = useSelectElementsForManageAdjustmentSystem();

  const [expandedList, setExpandedList] = useState<string[]>([]);

  const [showDeleteRecordModal, setShowDeleteRecordModal] = useState(false);
  const [cancelRowId, setCancelRowId] = useState<string | undefined>(undefined);
  const [deleteList, setDeleteList] = useState<string[]>([]);
  const [showRecordModal, setShowRecordModal] = useState(false);
  const [editingRecord, setEditingRecord] =
    useState<ProfileTransactions | undefined>(undefined);
  const [navigateToRowId, setNavigateToRowId] =
    useState<string | undefined>(undefined);

  const readOnlyCheckbox = useMemo(
    () =>
      transactions
        .filter(t =>
          ['updated', 'marked for deletion'].includes(t.status?.toLowerCase())
        )
        .map(t => t.rowId!),
    [transactions]
  );

  const isEditing = !isEmpty(editingRecord);
  const {
    sort,
    total,
    currentPage,
    currentPageSize,
    gridData: dataView,
    dataChecked: _checkedList,
    onSetDataChecked,
    onSortChange,
    onPageChange,
    onPageSizeChange,
    onResetToDefaultFilter,
    navigateToItem
  } = usePagination(filteredTransactions, [], 'batchCode');

  const checkedList = useMemo(
    () => _checkedList.filter(r => !readOnlyCheckbox.includes(r)),
    [_checkedList, readOnlyCheckbox]
  );
  const allIds = transactions
    .map(f => f.rowId!)
    .filter(r => !readOnlyCheckbox.includes(r));
  const { gridClassName, handleClickCheckAll } = useCheckAllPagination(
    {
      gridClassName: `grid-has-tooltip center profiles-transaction-${profile.code}`,
      allIds,
      hasFilter: !!searchValue,
      filterIds: filteredTransactions
        .map(f => f.rowId!)
        .filter(r => !readOnlyCheckbox.includes(r)),
      checkedList,
      setCheckAll: _isCheckAll => onSetDataChecked(_isCheckAll ? allIds : []),
      setCheckList: onSetDataChecked
    },
    [searchValue, sort, currentPage, currentPageSize]
  );

  useEffect(() => {
    if (stepId !== selectedStep) {
      onSetDataChecked([]);
      onResetToDefaultFilter();
    }
  }, [stepId, selectedStep, onSetDataChecked, onResetToDefaultFilter]);

  useEffect(() => {
    if (isEqual(checkedList, selectedList)) return;
    setSelectedList(profile.code, checkedList);
  }, [checkedList, profile.code, setSelectedList, selectedList]);

  useEffect(() => {
    if (isEqual(sort, sortBy)) return;
    setSortBy(profile.code, sort);
  }, [sort, sortBy, profile.code, setSortBy]);

  useEffect(() => {
    isEmpty(checkedList) && onSetDataChecked(selectedList);
    if (!isEmpty(sortBy)) onSortChange(sortBy);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (!resetFilter) return;

    setExpandedList([]);
    onPageChange(1);
  }, [selectedList, resetFilter, profile.code, onSetDataChecked, onPageChange]);

  useEffect(() => {
    if (!navigateToRowId) return;

    navigateToItem('rowId', navigateToRowId);
    setNavigateToRowId(undefined);
  }, [dataView, navigateToRowId, navigateToItem]);

  const isNoData = useMemo(
    () => transactions.length <= 0,
    [transactions.length]
  );

  const showCountSelectedSection = useMemo(
    () => !searchValue || checkedList?.length > 1,
    [searchValue, checkedList?.length]
  );
  const isBulkDelete = useMemo(() => checkedList.length > 1, [checkedList]);

  const disableHideProfile = useMemo(
    () => transactions.some(t => t.status !== TransactionStatus.NotUpdated),
    [transactions]
  );
  const mapGridExpandCollapse =
    (id: string, t: any) =>
    (item: any): ToggleButtonConfig =>
      ({
        id: item[id],
        collapsedTooltipProps: {
          element: t('txt_collapse'),
          opened: isDevice ? false : undefined
        },
        expandedTooltipProps: {
          element: t('txt_expand'),
          opened: isDevice ? false : undefined
        }
      } as ToggleButtonConfig);

  const handleOpenRecordModal = () => {
    setShowRecordModal(true);
  };

  const checkSameWithOriginal = (
    value1: ProfileTransactions,
    value2: ProfileTransactions
  ) => {
    return (
      value1.rowId === value2.rowId &&
      value1.actionCode === value2.actionCode &&
      value1.batchCode === value2.batchCode &&
      value1.maximumAmount === value2.maximumAmount &&
      value1.transactionCode === value2.transactionCode
    );
  };

  const handleCloseRecordModal = (
    profileTransactions?: ProfileTransactions
  ) => {
    if (!isEmpty(profileTransactions)) {
      if (isEditing) {
        const _transactions = [...profile?.transactions];
        const idx = _transactions.findIndex(
          f => f.rowId === editingRecord!.rowId
        );
        let status = TransactionStatus.Updated;

        if (_transactions[idx].status === TransactionStatus.New) {
          status = TransactionStatus.New;
        } else {
          if (
            checkSameWithOriginal(
              _transactions[idx].originalProfile!,
              profileTransactions!
            )
          ) {
            status = TransactionStatus.NotUpdated;
          }
        }

        _transactions[idx] = {
          ..._transactions[idx],
          ...profileTransactions,
          status: status
        };

        const profileDetail = {
          ...profile,
          transactions: _transactions
        };
        isFunction(onUpdateProfileDetails) &&
          onUpdateProfileDetails(profileDetail);

        if (status === TransactionStatus.Updated) {
          const f = differenceWith(checkedList, [editingRecord!.rowId]);
          onSetDataChecked(f);
        }
      } else {
        const rowId = `${profile.code}_${
          profile.transactions.length
        }_${Date.now()}`;
        const _profileTransactions = {
          ...profileTransactions!,
          status: TransactionStatus.New,
          rowId: rowId,
          id: rowId
        };
        const profileDetail = {
          ...profile,
          transactions: [...profile.transactions, _profileTransactions]
        };
        isFunction(onUpdateProfileDetails) &&
          onUpdateProfileDetails(profileDetail);

        setNavigateToRowId(_profileTransactions.rowId);
        onSetDataChecked([]);
        onSortChange({ id: 'batchCode' });
        setExpandedList([_profileTransactions.rowId]);
        clearSearchValue();
      }
    }

    setShowRecordModal(false);
    setEditingRecord(undefined);
  };

  const handleDeleteRecords = () => {
    const _transactions: ProfileTransactions[] = [];
    profile?.transactions?.forEach(element => {
      if (deleteList.includes(element.rowId!)) {
        if (element.status === TransactionStatus.New) return;
        _transactions.push({
          ...element,
          status: TransactionStatus.MarkedForDeletion
        });
        return;
      }
      _transactions.push(element);
    });
    const profileDetail = {
      ...profile,
      transactions: _transactions
    };
    isFunction(onUpdateProfileDetails) && onUpdateProfileDetails(profileDetail);
    setShowDeleteRecordModal(false);

    const diff = differenceWith(checkedList, deleteList);
    setSelectedList(profile.code, diff);
    onSetDataChecked(diff);
  };

  const handleCancelRecord = useCallback(() => {
    const idx = profile?.transactions.findIndex(f => f.rowId === cancelRowId);

    const _transactions = [...profile?.transactions];
    const originalProfile = _transactions[idx].originalProfile!;
    _transactions[idx] = {
      ...originalProfile,
      status: TransactionStatus.NotUpdated,
      originalProfile: originalProfile
    };

    const profileDetail = {
      ...profile,
      transactions: _transactions
    };
    isFunction(onUpdateProfileDetails) && onUpdateProfileDetails(profileDetail);
    setCancelRowId(undefined);
  }, [onUpdateProfileDetails, profile, cancelRowId]);

  const selectedText = useMemo(
    () =>
      getSelectedRecord({
        selected: differenceWith(checkedList, readOnlyCheckbox).length,
        total: differenceWith(
          transactions.map(f => f.rowId!),
          readOnlyCheckbox
        ).length,
        text: t('txt_config_manage_adjustments_system_records_selected', {
          selected: differenceWith(checkedList, readOnlyCheckbox).length
        }),
        text1: t('txt_config_manage_adjustments_system_1_record_selected'),
        textAll: t('txt_config_manage_adjustments_system_all_record_selected')
      }),
    [t, transactions, readOnlyCheckbox, checkedList]
  );

  const actionAccessor = useCallback(
    (data: Record<string, any>) => {
      const status = data.status;

      const showEdit = status !== TransactionStatus.MarkedForDeletion;
      const showDelete = [
        TransactionStatus.NotUpdated,
        TransactionStatus.New
      ].includes(status);
      const showCancel = [
        TransactionStatus.Updated,
        TransactionStatus.MarkedForDeletion
      ].includes(status);

      const onClickDelete = () => {
        setShowDeleteRecordModal(true);
        setDeleteList([data.rowId as string]);
      };

      const handleClickEdit = () => {
        const _profileTransactions = {
          ...data,
          maximumAmount: data?.maximumAmount
        } as ProfileTransactions;
        setEditingRecord(_profileTransactions);
      };

      const onClickCancel = () => {
        setCancelRowId(data.rowId as string);
      };

      return (
        <div className="d-flex justify-content-center">
          {showEdit && (
            <Button
              size="sm"
              variant="outline-primary"
              className={classnames(showEdit && 'mx-4')}
              onClick={handleClickEdit}
            >
              {t('txt_edit')}
            </Button>
          )}

          {showDelete && (
            <Button
              size="sm"
              variant="outline-danger"
              className={classnames(showEdit && 'mx-4')}
              onClick={onClickDelete}
            >
              {t('txt_delete')}
            </Button>
          )}
          {showCancel && (
            <Button
              size="sm"
              variant="outline-danger"
              className={classnames(showEdit && 'mx-4')}
              onClick={onClickCancel}
            >
              {t('txt_cancel')}
            </Button>
          )}
        </div>
      );
    },
    [t]
  );

  const onClickBulkDelete = () => {
    setShowDeleteRecordModal(true);
    setDeleteList(checkedList);
  };

  const onClickHide = () => {
    setFormValues({
      profiles: formValues.profiles.filter(f => f.code !== profile.code)
    });
  };

  const renderSubRow = useCallback(
    ({ original }: { original: ProfileTransactions }) => (
      <ProfileListSubGrid
        profileTransactions={original}
        metadata={profileElements}
      />
    ),
    [profileElements]
  );

  return (
    <>
      <div className="d-flex align-items-center justify-content-between">
        <div className="d-flex align-items-center">
          <Tooltip
            element={isExpand ? t('txt_collapse') : t('txt_expand')}
            opened={isDevice ? false : undefined}
            variant="primary"
            placement="top"
            triggerClassName="d-flex ml-n2"
          >
            <Button size="sm" variant="icon-secondary" onClick={onClickExpand}>
              <Icon name={isExpand ? 'minus' : 'plus'} size="4x" />
            </Button>
          </Tooltip>

          <strong className="ml-8">{profileName}</strong>
        </div>
        <div className="d-flex align-items-center mr-n8">
          <Tooltip
            opened={disableHideProfile ? undefined : false}
            element={t(
              'txt_config_manage_adjustments_system_hide_profile_tooltip'
            )}
          >
            <Button
              size="sm"
              variant="outline-primary"
              disabled={disableHideProfile}
              onClick={onClickHide}
            >
              {t('txt_config_manage_adjustments_system_hide_profile')}
            </Button>
          </Tooltip>

          <Button
            size="sm"
            variant="outline-primary"
            className="ml-8"
            onClick={handleOpenRecordModal}
          >
            {t('txt_config_manage_adjustments_system_add_record')}
          </Button>
        </div>
      </div>
      <div className={classnames(!isExpand && 'd-none')}>
        <div className="mt-8 color-grey">
          {t('txt_config_manage_adjustments_system_subsystemAdj')}:{' '}
          {subSystemAdj}
        </div>
        {showCountSelectedSection && (
          <div className="d-flex align-items-center my-16">
            {!searchValue && <p className="py-4"> {selectedText}</p>}
            {isBulkDelete && (
              <Button
                variant="outline-danger"
                size="sm"
                className="ml-auto mr-n8"
                onClick={onClickBulkDelete}
              >
                {t('txt_manage_ocs_shells_step_3_bulk_delete')}
              </Button>
            )}
          </div>
        )}

        {!isNoData && (
          <div
            className={classnames({
              'mt-16': !showCountSelectedSection
            })}
          >
            <Grid
              className={gridClassName}
              dataItemKey="rowId"
              expandedItemKey="rowId"
              data={dataView}
              columns={columns(t, actionAccessor, width)}
              togglable
              expandedList={expandedList}
              checkedList={checkedList}
              sortBy={[sort]}
              onSortChange={onSortChange}
              variant={{
                id: 'rowId',
                type: 'checkbox',
                cellHeaderProps: { onClick: handleClickCheckAll }
              }}
              readOnlyCheckbox={readOnlyCheckbox}
              checkboxTooltipPropsList={readOnlyCheckbox?.map(id => ({
                id: id,
                element: t(
                  'txt_config_manage_adjustments_system_checkbox_ready_only'
                )
              }))}
              subRow={renderSubRow}
              onExpand={setExpandedList}
              onCheck={onSetDataChecked}
              toggleButtonConfigList={dataView.map(
                mapGridExpandCollapse('rowId', t)
              )}
            />
            {total > 10 && (
              <div className="mt-16">
                <Paging
                  page={currentPage}
                  pageSize={currentPageSize}
                  totalItem={total}
                  onChangePage={onPageChange}
                  onChangePageSize={onPageSizeChange}
                />
              </div>
            )}
          </div>
        )}
      </div>
      {(showRecordModal || editingRecord) && (
        <RecordModal
          id="recordModal"
          show
          profileTransactions={editingRecord}
          onClose={handleCloseRecordModal}
        />
      )}
      {showDeleteRecordModal && (
        <DeleteRecordsModal
          id="removeRecordsModal"
          show={showDeleteRecordModal}
          onClose={() => setShowDeleteRecordModal(false)}
          onDelete={handleDeleteRecords}
          isBulk={deleteList.length > 1}
        />
      )}
      {cancelRowId !== undefined && (
        <CancelRecordsModal
          id="removeRecordsModal"
          show={cancelRowId !== undefined}
          onClose={() => setCancelRowId(undefined)}
          onConfirm={handleCancelRecord}
        />
      )}
    </>
  );
};

export default ProfileSection;
