import { getWorkflowSetupStepStatus } from 'app/helpers';
import { STATUS } from 'pages/WorkflowManageOCSShells/ConfigureParameters/constants';
import { FormConfigFLSADJ } from '.';
import { ParameterCode } from '../type';

const parseFormValues: WorkflowSetupStepFormDataFunc<FormConfigFLSADJ> = (
  data,
  id
) => {
  const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);
  if (!stepInfo) return;

  const profileList = data?.data?.configurations?.profileList || [];

  const now = Date.now();
  const profiles = profileList.map(p => ({
    code: p.profileName,
    text: p.profileName,
    subSystemAdj: p.subSystemAdj,
    transactions: p.ocsTransactionActions?.map((t: any, idx: string) => ({
      rowId: `${p.profileName}${now}${idx}`,
      status: t.status,

      batchCode: t.parameters?.find(
        (param: any) => param.name === ParameterCode.BatchCode
      )?.newValue,
      transactionCode: t.parameters?.find(
        (param: any) => param.name === ParameterCode.TransactionCode
      )?.newValue,
      maximumAmount: t.parameters?.find(
        (param: any) => param.name === ParameterCode.MaximumAmount
      )?.newValue,
      actionCode: t.parameters?.find(
        (param: any) => param.name === ParameterCode.ActionCode
      )?.newValue,

      originalProfile: {
        status: t.status,
        batchCode: t.parameters?.find(
          (param: any) => param.name === ParameterCode.BatchCode
        )?.previousValue,
        transactionCode: t.parameters?.find(
          (param: any) => param.name === ParameterCode.TransactionCode
        )?.previousValue,
        maximumAmount: t.parameters?.find(
          (param: any) => param.name === ParameterCode.MaximumAmount
        )?.previousValue,
        actionCode: t.parameters?.find(
          (param: any) => param.name === ParameterCode.ActionCode
        )?.previousValue
      }
    }))
  }));
  const isValid = profiles.some(p =>
    p.transactions?.some((s: any) => s.status !== STATUS.NotUpdated)
  );

  return {
    ...getWorkflowSetupStepStatus(stepInfo),
    profiles,
    isValid
  };
};

export default parseFormValues;
