import ModalRegistry from 'app/components/ModalRegistry';
import { ProfileRefData } from 'app/types';
import {
  InlineMessage,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import { isFunction } from 'lodash';
import React, { useState } from 'react';
import AddProfile from './AddProfile';

export interface AddProfileModalProps {
  existProfileIds: string[];
  onClose?: VoidFunction;
  onAdd?: (profile: ProfileRefData) => void;
}

const AddProfileModal: React.FC<AddProfileModalProps> = ({
  existProfileIds,
  onClose,
  onAdd
}) => {
  const { t } = useTranslation();

  const [profile, setProfile] = useState<ProfileRefData>();
  const [isExistProfile, setIsExistProfile] = useState(false);

  const handleAdd = () => {
    const _isExistProfile =
      !!profile?.code && existProfileIds.includes(profile.code);
    if (_isExistProfile) {
      setIsExistProfile(_isExistProfile);
      return;
    }

    isFunction(onAdd) && onAdd(profile!);
    isFunction(onClose) && onClose();
  };

  return (
    <ModalRegistry id="AddProfileModal" show onAutoClosedAll={onClose}>
      <ModalHeader border closeButton onHide={onClose}>
        <ModalTitle>
          {t('txt_config_manage_adjustments_system_select_profile')}
        </ModalTitle>
      </ModalHeader>
      <ModalBody>
        {isExistProfile && (
          <InlineMessage variant="danger">
            {t('txt_config_manage_adjustments_system_validation_exist_profile')}
          </InlineMessage>
        )}
        <AddProfile
          isExistProfile={isExistProfile}
          profile={profile}
          onChangeProfile={setProfile}
        />
      </ModalBody>
      <ModalFooter
        cancelButtonText={t('txt_cancel')}
        okButtonText={t('txt_continue')}
        disabledOk={!profile}
        onCancel={onClose}
        onOk={handleAdd}
      />
    </ModalRegistry>
  );
};

export default AddProfileModal;
