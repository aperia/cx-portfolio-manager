import { RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import {
  mockThunkAction,
  mockUseModalRegistryFnc,
  renderWithMockStore
} from 'app/utils';
import { STATUS } from 'pages/WorkflowManageOCSShells/ConfigureParameters/constants/enums';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import React from 'react';
import { ParameterCode } from '../type';
import ConfigFLSADJStep, { FormConfigFLSADJ } from './index';

const mockUseModalRegistry = mockUseModalRegistryFnc();

const renderComponent = async (
  props: any,
  workflowSetup?: any
): Promise<RenderResult> => {
  return await renderWithMockStore(<ConfigFLSADJStep {...props} />, {
    initialState: { workflowSetup }
  });
};
const mockWorkflowSetup = mockThunkAction(actionsWorkflowSetup);

export const mockElements = [
  {
    name: ParameterCode.TransactionCode,
    options: [{ value: 'AssessmentCode' }]
  },
  {
    name: ParameterCode.ProfileName,
    options: [{ value: 'ProfileName' }]
  },
  {
    name: ParameterCode.BatchCode,
    options: [{ value: 'BatchCode' }]
  },
  {
    name: ParameterCode.ActionCode,
    options: [{ value: 'ActionCode' }]
  }
];

describe('ManageAdjustmentsSystemWorkflow > ConfigFLSADJStep > Index', () => {
  beforeEach(() => {
    mockUseModalRegistry.mockReturnValue([true, false]);
    mockWorkflowSetup('getManageAdjustmentsSystemProfileTransactions', {
      match: true
    });
  });

  const defaultWorkflowSetup = {
    elementMetadata: { elements: mockElements },
    profileTransactions: { loading: true, error: false }
  };

  it('should render with empty profile', async () => {
    mockWorkflowSetup('getManageAdjustmentsSystemProfileTransactions', {
      match: true,
      payload: { payload: [{}] }
    });
    const setFormValues = jest.fn() as any;
    const props = {
      formValues: {
        profiles: [] as any
      },
      setFormValues
    } as WorkflowSetupProps<FormConfigFLSADJ>;

    const { getByText } = await renderComponent(props, defaultWorkflowSetup);

    userEvent.click(
      getByText('txt_config_manage_adjustments_system_select_profile')
    );
    userEvent.click(
      getByText('txt_config_manage_adjustments_system_profileName')
    );
    userEvent.click(getByText('ProfileName'));

    expect(
      getByText('txt_config_manage_adjustments_system_select_profile')
    ).toBeInTheDocument();
  });

  it('should render stuck message', async () => {
    mockWorkflowSetup('getManageAdjustmentsSystemProfileTransactions', {
      match: true,
      payload: { payload: [{}] }
    });
    const setFormValues = jest.fn() as any;
    const props = {
      isStuck: true,
      formValues: {
        profiles: [] as any
      },
      setFormValues
    } as WorkflowSetupProps<FormConfigFLSADJ>;

    const { getByText } = await renderComponent(props, defaultWorkflowSetup);

    expect(
      getByText('txt_step_stuck_move_forward_message')
    ).toBeInTheDocument();
  });

  it('should add profile success', async () => {
    mockWorkflowSetup('getManageAdjustmentsSystemProfileTransactions', {
      match: true,
      payload: { payload: [{}] }
    });
    const setFormValues = jest.fn() as any;
    const props = {
      formValues: {
        profiles: [
          {
            code: '001',
            text: '001',
            transactions: [
              { status: STATUS.NotUpdated },
              { status: STATUS.Updated }
            ] as any[]
          }
        ]
      },
      setFormValues
    } as WorkflowSetupProps<FormConfigFLSADJ>;

    const { baseElement, getByText, getByPlaceholderText } =
      await renderComponent(props, defaultWorkflowSetup);
    expect(
      getByText('txt_config_manage_adjustments_system_select_profile')
    ).toBeInTheDocument();

    userEvent.click(
      getByText('txt_config_manage_adjustments_system_select_profile')
    );
    expect(baseElement.querySelector('.dls-modal-root')).toBeInTheDocument();

    userEvent.click(
      getByText('txt_config_manage_adjustments_system_profileName')
    );
    userEvent.click(getByText('ProfileName'));
    userEvent.click(
      getByText('txt_config_manage_adjustments_system_profileName')
    );
    userEvent.click(getByText('ProfileName'));
    userEvent.click(getByText('txt_continue'));

    const searchBox = getByPlaceholderText('txt_type_to_search');
    userEvent.type(searchBox, 'a');
    userEvent.click(
      searchBox.parentElement!.querySelector('.icon.icon-search')!
    );

    // expect(setFormValues).toHaveBeenCalled();
  });

  it('should add profile fail', async () => {
    mockWorkflowSetup('getManageAdjustmentsSystemProfileTransactions', {
      match: false
    });
    const setFormValues = jest.fn() as any;
    const props = {
      formValues: {
        profiles: [{ code: '001', text: '001', transactions: [] as any[] }]
      },
      setFormValues
    } as WorkflowSetupProps<FormConfigFLSADJ>;

    const { baseElement, getByText } = await renderComponent(
      props,
      defaultWorkflowSetup
    );
    expect(
      getByText('txt_config_manage_adjustments_system_select_profile')
    ).toBeInTheDocument();

    userEvent.click(
      getByText('txt_config_manage_adjustments_system_select_profile')
    );
    expect(baseElement.querySelector('.dls-modal-root')).toBeInTheDocument();
    userEvent.click(getByText('txt_cancel'));

    userEvent.click(
      getByText('txt_config_manage_adjustments_system_select_profile')
    );

    userEvent.click(
      getByText('txt_config_manage_adjustments_system_profileName')
    );
    userEvent.click(getByText('ProfileName'));

    userEvent.click(getByText('txt_continue'));

    // expect(setFormValues).toHaveBeenCalled();
  });
});
