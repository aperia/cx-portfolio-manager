import { fireEvent, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { renderWithMockStore } from 'app/utils';
import mockDevice from 'app/utils/_mockHelperConstant/mockDevice';
import 'app/_libraries/_dls/test-utils/mocks/mockCanvas.ts';
import React from 'react';
import { FormConfigFLSADJ } from '.';
import { mockElements } from './index.test';
import Summary from './Summary';

const renderComponent = async (
  props: any,
  workflowSetup?: any
): Promise<RenderResult> => {
  return await renderWithMockStore(<Summary {...props} />, {
    initialState: { workflowSetup }
  });
};

describe('ManageAdjustmentsSystemWorkflow > ConfigFLSADJStep > Summary', () => {
  beforeEach(() => {
    mockDevice.isDevice = false;
  });

  const defaultWorkflowSetup = {
    elementMetadata: { elements: mockElements },
    profileTransactions: { loading: true, error: false }
  };
  it('should render summary', async () => {
    const props = {
      formValues: {
        profiles: [
          {
            code: '001',
            text: '001',
            transactions: [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}] as any[]
          },
          { code: '002', text: '002', transactions: [] as any[] }
        ]
      }
    } as WorkflowSetupSummaryProps<FormConfigFLSADJ>;
    const { baseElement, getByText, rerender } = await renderComponent(
      props,
      defaultWorkflowSetup
    );

    // expand all profile list
    let expands = baseElement.querySelectorAll('.icon.icon-plus');
    expands.forEach(userEvent.click as any);

    // expand all transactions
    expands = baseElement.querySelectorAll('.icon.icon-plus');
    expands.forEach(userEvent.click as any);

    rerender(<Summary {...props} stepId="step1" selectedStep="step2" />);

    const collapses = baseElement.querySelectorAll('.icon.icon-minus');
    userEvent.click(collapses[0]);

    expect(getByText('txt_edit')).toBeInTheDocument();
  });

  it('should render summary in device', async () => {
    mockDevice.isDevice = true;

    const props = {
      formValues: {
        profiles: [
          {
            code: '001',
            text: '001',
            transactions: [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}] as any[]
          },
          { code: '002', text: '002', transactions: [] as any[] }
        ]
      }
    } as WorkflowSetupSummaryProps<FormConfigFLSADJ>;
    const { baseElement, getByText, rerender } = await renderComponent(
      props,
      defaultWorkflowSetup
    );

    // expand all profile list
    let expands = baseElement.querySelectorAll('.icon.icon-plus');
    expands.forEach(userEvent.click as any);

    // expand all transactions
    expands = baseElement.querySelectorAll('.icon.icon-plus');
    expands.forEach(userEvent.click as any);

    rerender(<Summary {...props} stepId="step1" selectedStep="step2" />);

    expect(getByText('txt_edit')).toBeInTheDocument();
  });

  it('Should click on edit', async () => {
    const mockFn = jest.fn();
    const { getByText } = await renderComponent(
      {
        formValues: {},
        onEditStep: mockFn
      } as WorkflowSetupSummaryProps,
      defaultWorkflowSetup
    );

    fireEvent.click(getByText('txt_edit'));
    expect(mockFn).toHaveBeenCalled();
  });
});
