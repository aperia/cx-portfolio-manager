import { BadgeColorType, GroupTextFormat } from 'app/constants/enums';
import { ColumnType } from 'app/_libraries/_dls';
import {
  formatBadge,
  formatText,
  viewMoreInfo
} from 'pages/_commons/Utils/formatGridField';

export const columns = (
  t: any,
  actionAccessor?: any,
  width?: any
): ColumnType[] => {
  const cols = [
    {
      id: 'batchCode',
      Header: t('txt_config_manage_adjustments_system_batchCode'),
      accessor: 'batchCode',
      isSort: true,
      width: width < 1280 ? 110 : undefined
    },
    {
      id: 'transactionCode',
      Header: t('txt_config_manage_adjustments_system_transactionCode'),
      accessor: 'transactionCode',
      isSort: true,
      width: width < 1280 ? 120 : undefined
    },
    {
      id: 'maximumAmount',
      Header: t('txt_config_manage_adjustments_system_maximumAmount'),
      accessor: formatText(['maximumAmount'], {
        format: GroupTextFormat.Amount
      }),
      className: 'text-right',
      isSort: true,
      width: width < 1280 ? 110 : undefined
    },
    {
      id: 'actionCode',
      Header: t('txt_config_manage_adjustments_system_actionCode'),
      accessor: 'actionCode',
      isSort: true,
      width: width < 1280 ? 120 : undefined
    },
    {
      id: 'status',
      Header: t('txt_status'),
      accessor: formatBadge(['status'], {
        colorType: BadgeColorType.ConfigParameterStatus
      }),
      width: width < 1280 ? 150 : undefined
    },
    {
      id: 'code',
      Header: t('txt_actions'),
      className: 'text-center',
      accessor: actionAccessor,
      width: 135,
      cellBodyProps: { className: 'py-8' }
    }
  ];

  return cols;
};

export const recordModalColumns = (
  t: any,
  valueAccessor?: any
): ColumnType[] => [
  {
    id: 'bussinessName',
    Header: t('txt_business_name'),
    accessor: 'bussinessName'
  },
  {
    id: 'greenScreenName',
    Header: t('txt_green_screen_name'),
    accessor: 'greenScreenName'
  },
  {
    id: 'id',
    Header: t('txt_value'),
    cellBodyProps: { className: 'control' },
    accessor: valueAccessor
  },
  {
    id: 'moreInfo',
    Header: t('txt_more_info'),
    accessor: viewMoreInfo(['moreInfo'], t),
    width: 105
  }
];

export const profileListSubGridColumns = (
  t: any,
  preValueAccessor?: any,
  valueAccessor?: any
): ColumnType[] => [
  {
    id: 'bussinessName',
    Header: t('txt_business_name'),
    accessor: 'bussinessName'
  },
  {
    id: 'greenScreenName',
    Header: t('txt_green_screen_name'),
    accessor: 'greenScreenName'
  },
  {
    id: 'previousValue',
    Header: t('txt_previous_value'),
    accessor: preValueAccessor
  },
  {
    id: 'newValue',
    Header: t('txt_new_value'),
    accessor: valueAccessor
  },
  {
    id: 'value',
    Header: t('txt_value'),
    accessor: valueAccessor
  },
  {
    id: 'moreInfo',
    Header: t('txt_more_info'),
    className: 'text-center',
    accessor: viewMoreInfo(['moreInfo'], t),
    width: 105
  }
];
