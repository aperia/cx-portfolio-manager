import { fireEvent, render, RenderResult } from '@testing-library/react';
import { mockUseModalRegistryFnc } from 'app/utils';
import React from 'react';
import CancelRecordsModal from './CancelRecordsModal';

const mockUseModalRegistry = mockUseModalRegistryFnc();

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderModal = (props: any): RenderResult => {
  return render(
    <div>
      <CancelRecordsModal {...props} />
    </div>
  );
};

describe('WorkflowManageAdjustmentsSystem > ConfigFLSADJStep > CancelRecordsModal', () => {
  beforeEach(() => {
    mockUseModalRegistry.mockImplementation(() => [true, false]);
  });

  afterEach(() => {
    mockUseModalRegistry.mockClear();
  });

  it('Should close modal without delete', () => {
    const onClose = jest.fn();

    const props = {
      id: '1',
      show: true,
      onClose: onClose
    };

    const wrapper = renderModal(props);
    wrapper.debug();
    fireEvent.click(
      wrapper.baseElement.querySelector(
        'button[class="btn btn-secondary"]'
      ) as Element
    );
    expect(onClose).toHaveBeenCalled();
  });

  it('Should close modal with deleted', () => {
    const onConfirm = jest.fn();

    const props = {
      id: '1',
      show: true,
      onConfirm: onConfirm
    };

    const wrapper = renderModal(props);

    fireEvent.click(
      wrapper.baseElement.querySelector(
        'button[class="btn btn-danger"]'
      ) as Element
    );
    expect(onConfirm).toHaveBeenCalled();
  });
});
