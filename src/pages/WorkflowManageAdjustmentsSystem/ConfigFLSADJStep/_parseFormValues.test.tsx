import { ParameterCode } from '../type';
import parseFormValues from './_parseFormValues';

describe('ManageAdjustmentsSystemWorkflow > ConfigFLSADJStep > parseFormValues', () => {
  const mockDispatch = jest.fn();

  it('Should return undefined', () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '2'
            }
          ] as any
        }
      } as WorkflowSetupInstance,
      id: '1'
    };

    const response = parseFormValues(props.data, props.id, mockDispatch);
    expect(response).toEqual(undefined);
  });

  it('Should return data without parameters', () => {
    const props = {
      data: {
        data: {
          configurations: {},
          workflowSetupData: [
            {
              id: '1'
            }
          ] as any
        }
      } as unknown as WorkflowSetupInstance,
      id: '1'
    };

    const response = parseFormValues(props.data, props.id, mockDispatch);
    expect(response).toEqual(
      expect.objectContaining({ isPass: false, isSelected: false })
    );
  });

  it('Should return data', () => {
    const props = {
      data: {
        data: {
          configurations: {
            profileList: [
              {
                ocsTransactionActions: [
                  { parameters: [{ name: ParameterCode.BatchCode }] }
                ]
              }
            ]
          },
          workflowSetupData: [
            {
              id: '1'
            }
          ] as any
        }
      } as unknown as WorkflowSetupInstance,
      id: '1'
    };

    const response = parseFormValues(props.data, props.id, mockDispatch);
    expect(response).toEqual(
      expect.objectContaining({ isPass: false, isSelected: false })
    );
  });
});
