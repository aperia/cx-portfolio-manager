import ModalRegistry from 'app/components/ModalRegistry';
import {
  Button,
  ModalBody,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import React from 'react';

interface IProps {
  id: string;
  show: boolean;
  isBulk: boolean;
  onClose: () => void;
  onDelete: () => void;
}

const DeleteRecordsModal: React.FC<IProps> = ({
  id,
  show,
  isBulk,
  onClose,
  onDelete
}) => {
  const { t } = useTranslation();

  return (
    <ModalRegistry
      id={id}
      show={show}
      onAutoClosedAll={onClose}
      classes={{ content: '' }}
    >
      <ModalHeader border closeButton onHide={onClose}>
        <ModalTitle>{t('txt_confirm_deletion')}</ModalTitle>
      </ModalHeader>
      <ModalBody>
        {isBulk ? t('txt_bulk_delete_records') : t('txt_delete_record')}
      </ModalBody>
      <div className="dls-modal-footer modal-footer">
        <Button variant="secondary" onClick={onClose}>
          {t('txt_cancel')}
        </Button>
        <Button variant="danger" onClick={onDelete}>
          {t('txt_delete')}
        </Button>
      </div>
    </ModalRegistry>
  );
};

export default DeleteRecordsModal;
