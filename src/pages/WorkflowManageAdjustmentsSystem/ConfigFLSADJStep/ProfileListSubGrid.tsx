import { convertRefValue, formatCommon } from 'app/helpers';
import { ProfileTransactions } from 'app/types';
import {
  ColumnType,
  Grid,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import React, { useCallback, useMemo } from 'react';
import { profileListSubGridColumns } from './constant';
import { parameters } from './_parameterList';

export interface ProfileListSubGridProps {
  profileTransactions: ProfileTransactions;
  metadata: {
    transactions: RefData[];
    batchCode: RefData[];
    actionCode: RefData[];
  };
}
const ProfileListSubGrid: React.FC<ProfileListSubGridProps> = ({
  profileTransactions,
  metadata
}) => {
  const { t } = useTranslation();
  const { status } = profileTransactions;
  const { transactions, batchCode, actionCode } = metadata;

  const getValue = useCallback(
    (
      _profileTransactions: ProfileTransactions,
      field: keyof ProfileTransactions
    ) => {
      switch (field) {
        case 'maximumAmount':
          return _profileTransactions?.maximumAmount
            ? formatCommon(_profileTransactions.maximumAmount).currency(2)!
            : '';
        case 'transactionCode':
          return convertRefValue(
            transactions,
            _profileTransactions.transactionCode
          );
        case 'batchCode':
          return convertRefValue(batchCode, _profileTransactions.batchCode);
        case 'actionCode':
          return convertRefValue(actionCode, _profileTransactions.actionCode);
      }
    },
    [transactions, batchCode, actionCode]
  );

  const valueAccessor = useCallback(
    (data: Record<string, any>) => {
      return (
        <TruncateText
          lines={2}
          ellipsisLessText={t('txt_less')}
          ellipsisMoreText={t('txt_more')}
        >
          {getValue(profileTransactions, data.id)}
        </TruncateText>
      );
    },
    [t, getValue, profileTransactions]
  );

  const preValueAccessor = useCallback(
    (data: Record<string, any>) => {
      const originalProfile = profileTransactions.originalProfile!;
      return (
        <TruncateText
          lines={2}
          ellipsisLessText={t('txt_less')}
          ellipsisMoreText={t('txt_more')}
        >
          {getValue(originalProfile, data.id)}
        </TruncateText>
      );
    },
    [t, getValue, profileTransactions.originalProfile]
  );

  const columns: ColumnType[] = useMemo(
    () => profileListSubGridColumns(t, preValueAccessor, valueAccessor),
    [t, valueAccessor, preValueAccessor]
  );
  const filterColumns: ColumnType[] = useMemo(() => {
    switch (status) {
      case 'Updated':
        return columns.filter(f => f.id !== 'value');
      default:
        return columns.filter(
          f => f.id !== 'newValue' && f.id !== 'previousValue'
        );
    }
  }, [columns, status]);

  return (
    <div className="p-20 overflow-hidden">
      <Grid
        className="grid-has-tooltip center"
        columns={filterColumns}
        data={parameters}
      />
    </div>
  );
};

export default ProfileListSubGrid;
