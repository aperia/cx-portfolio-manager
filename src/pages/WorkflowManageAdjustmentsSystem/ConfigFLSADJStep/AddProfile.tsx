import FieldTooltip from 'app/components/FieldTooltip';
import { useFormValidations } from 'app/hooks';
import { ProfileRefData } from 'app/types';
import {
  ComboBox,
  DropdownBaseChangeEvent,
  useTranslation
} from 'app/_libraries/_dls';
import { isFunction, isUndefined } from 'lodash';
import { useSelectElementsForManageAdjustmentSystem } from 'pages/_commons/redux/WorkflowSetup';
import React, { useEffect, useMemo, useState } from 'react';

interface IProps {
  isExistProfile?: boolean;
  profile?: ProfileRefData;
  onChangeProfile?: (profile: ProfileRefData) => void;
}

const AddProfile: React.FC<IProps> = ({
  isExistProfile = false,
  profile,
  onChangeProfile
}) => {
  const { t } = useTranslation();
  const { profiles } = useSelectElementsForManageAdjustmentSystem();

  const [selectedProfile, setSelectedProfile] =
    useState<ProfileRefData | undefined>(profile);
  useEffect(() => setSelectedProfile(profile), [profile]);

  const currentErrors = useMemo(
    () =>
      ({
        profile:
          (!selectedProfile?.code?.trim() && {
            status: true,
            message: t('txt_required_validation', {
              fieldName: t('txt_config_manage_adjustments_system_profileName')
            })
          }) ||
          (isExistProfile && {
            status: true,
            message: t(
              'txt_config_manage_adjustments_system_validation_exist_profile'
            )
          })
      } as Record<'profile', IFormError>),
    [selectedProfile, t, isExistProfile]
  );
  const [, errors, setTouched] = useFormValidations<'profile'>(currentErrors);

  const handleSelectProfile = (e: DropdownBaseChangeEvent) => {
    if (isUndefined(profile)) {
      setSelectedProfile(e.target.value);
    }

    isFunction(onChangeProfile) && onChangeProfile(e.target.value);
  };

  return (
    <FieldTooltip
      placement="right-start"
      element={
        <div>
          <div className="text-uppercase">
            {t('txt_config_manage_adjustments_system_profile')}
          </div>
          <p className="mt-8">
            {t('txt_config_manage_adjustments_system_profile_tooltip_desc')}
          </p>
        </div>
      }
    >
      <ComboBox
        textField="text"
        required
        value={selectedProfile}
        label={t('txt_config_manage_adjustments_system_profileName')}
        onChange={handleSelectProfile}
        noResult={t('txt_no_results_found_without_dot')}
        onFocus={setTouched('profile')}
        onBlur={setTouched('profile', true)}
        error={errors.profile}
      >
        {profiles.map(item => (
          <ComboBox.Item key={item.code} value={item} label={item.text} />
        ))}
      </ComboBox>
    </FieldTooltip>
  );
};

export default AddProfile;
