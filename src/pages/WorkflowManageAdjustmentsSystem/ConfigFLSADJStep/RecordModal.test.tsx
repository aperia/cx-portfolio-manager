import {
  act,
  fireEvent,
  render,
  RenderResult,
  screen
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { TransactionStatus } from 'app/constants/enums';
import { mockUseModalRegistryFnc } from 'app/utils';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageAdjustmentsSystem';
import React from 'react';
import RecordModal from './RecordModal';

const mockUseModalRegistry = mockUseModalRegistryFnc();
const useSelectElementsForManageAdjustmentSystem = jest.spyOn(
  WorkflowSetup,
  'useSelectElementsForManageAdjustmentSystem'
);

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <RecordModal {...props} />
    </div>
  );
};

const then_change_dropdown = (wrapper: RenderResult) => {
  const dropdown = wrapper.getAllByPlaceholderText(
    'txt_workflow_manage_change_in_terms_select_an_option'
  )[0];
  act(() => {
    userEvent.click(dropdown);
  });

  act(() => {
    userEvent.click(screen.getByText('text'));
  });

  fireEvent.blur(dropdown);
};

describe('RecordModal ', () => {
  beforeEach(() => {
    mockUseModalRegistry.mockImplementation(() => [true, false]);

    useSelectElementsForManageAdjustmentSystem.mockReturnValue({
      transactions: [{ code: 'code', text: 'text' }],
      profiles: [{ subSystemAdj: 'a', code: 'code', text: 'text' }],
      batchCode: [{ code: 'code', text: 'text' }],
      actionCode: [{ code: 'code', text: 'text' }],
      queueOwner: [{ code: 'code', text: 'text' }],
      managerId: [{ code: 'code', text: 'text' }],
      shellNames: [{ code: 'code', text: 'text' }]
    });
  });

  afterEach(() => {
    mockUseModalRegistry.mockClear();
  });

  it('Should render Edit record', async () => {
    const props = {
      profileTransactions: {
        rowId: '1',
        status: TransactionStatus.NotUpdated,
        batchCode: 'AB',
        actionCode: 'A',
        maximumAmount: '123',
        originalProfile: {
          status: TransactionStatus.NotUpdated,
          batchCode: 'B',
          actionCode: 'A',
          maximumAmount: '123'
        }
      },
      show: true,
      onClose: jest.fn()
    };
    const wrapper = await renderComponent({
      ...props
    });

    expect(wrapper.getByText('txt_edit_record')).toBeInTheDocument;

    then_change_dropdown(wrapper);

    const maximumAmountInput = wrapper.getByTestId(
      'profile_maximumAmount_dls-numeric-textbox-input'
    ) as HTMLInputElement;
    userEvent.clear(maximumAmountInput);
    userEvent.type(maximumAmountInput, '12');

    const searchBox = wrapper.getByPlaceholderText('txt_type_to_search');
    userEvent.type(searchBox, 'a');
    userEvent.click(
      searchBox.parentElement!.querySelector(
        '.icon-simple-search .icon.icon-search'
      )!
    );

    userEvent.click(wrapper.getByText('txt_save'));

    expect(props.onClose).toHaveBeenCalledWith({
      ...props.profileTransactions,
      batchCode: 'AB',
      transactionCode: 'code',
      maximumAmount: 12
    });
  });

  it('Should render Edit record - edit with no change', async () => {
    const props = {
      profileTransactions: {
        rowId: '1',
        status: TransactionStatus.NotUpdated,
        batchCode: 'AB',
        actionCode: 'A',
        maximumAmount: '123',
        originalProfile: {
          status: TransactionStatus.NotUpdated,
          batchCode: 'B',
          actionCode: 'A',
          maximumAmount: '123'
        }
      },
      show: true,
      onClose: jest.fn()
    };
    const wrapper = await renderComponent({
      ...props
    });

    expect(wrapper.getByText('txt_edit_record')).toBeInTheDocument;

    const searchBox = wrapper.getByPlaceholderText('txt_type_to_search');

    userEvent.type(searchBox, 'notfound');
    userEvent.click(
      searchBox.parentElement!.querySelector(
        '.icon-simple-search .icon.icon-search'
      )!
    );

    expect(
      wrapper.getByText('txt_no_results_found txt_adjust_your_search_criteria')
    ).toBeInTheDocument();

    userEvent.click(wrapper.getByText('txt_clear_and_reset'));

    userEvent.click(wrapper.getByText('txt_save'));

    expect(props.onClose).toHaveBeenCalled();
  });

  it('Should render Edit record', async () => {
    const props = {
      profileTransactions: undefined,
      show: true,
      onClose: jest.fn()
    };
    const wrapper = await renderComponent({
      ...props
    });

    expect(wrapper.getByText('txt_config_manage_adjustments_system_add_record'))
      .toBeInTheDocument;

    userEvent.click(wrapper.getByText('txt_add'));

    expect(props.onClose).toHaveBeenCalledWith({
      actionCode: '',
      batchCode: '',
      maximumAmount: '',
      transactionCode: '',
      status: 'New'
    });
  });
});
