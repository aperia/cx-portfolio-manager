import NoDataFound from 'app/components/NoDataFound';
import {
  BadgeColorType,
  GroupTextFormat,
  TransactionStatus
} from 'app/constants/enums';
import { classnames, isDevice, mapGridExpandCollapse } from 'app/helpers';
import { usePagination } from 'app/hooks/usePagination';
import { ProfileDetail, ProfileTransactions } from 'app/types';
import {
  Button,
  ColumnType,
  Grid,
  Icon,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty, isFunction } from 'lodash';
import { useSelectElementsForManageAdjustmentSystem } from 'pages/_commons/redux/WorkflowSetup';
import { formatBadge, formatText } from 'pages/_commons/Utils/formatGridField';
import Paging from 'pages/_commons/Utils/Paging';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { FormConfigFLSADJ } from '.';
import ProfileListSubGrid from './ProfileListSubGrid';

const ConfigFLSADJSummary: React.FC<
  WorkflowSetupSummaryProps<FormConfigFLSADJ>
> = ({ onEditStep, formValues, stepId, selectedStep }) => {
  const { t } = useTranslation();

  const countProfile = useMemo(() => {
    return formValues?.profiles?.filter(el =>
      el?.transactions.some(k => k?.status !== TransactionStatus.NotUpdated)
    );
  }, [formValues]);

  const [expandList, setExpandList] = useState<Record<string, boolean>>({});

  const handleEdit = () => {
    isFunction(onEditStep) && onEditStep();
  };

  const resetFilter = useMemo(
    () => stepId === selectedStep,
    [stepId, selectedStep]
  );

  useEffect(() => {
    resetFilter && setExpandList({});
  }, [resetFilter]);

  const handleClickExpand = (id: string, isExpand: boolean) => () => {
    setExpandList(list => ({ ...list, [id]: isExpand }));
  };

  return (
    <div className="position-relative">
      <div className="absolute-top-right mt-n26 mr-n8">
        <Button variant="outline-primary" size="sm" onClick={handleEdit}>
          {t('txt_edit')}
        </Button>
      </div>

      {isEmpty(countProfile) && (
        <div className="d-flex flex-column justify-content-center pt-40 mb-32">
          <NoDataFound
            id="merchant-list-summary-NoDataFound"
            title={t('txt_config_processing_merchants_no_update_merchants')}
          />
        </div>
      )}
      {!isEmpty(countProfile) && (
        <div className="pt-16 list-summary-grid">
          {formValues?.profiles?.map((profile, idx) => {
            const isExpand = !expandList[profile.code];

            return (
              <div key={profile.code} className="summary-grid">
                <ProfileSection
                  profile={profile}
                  isExpand={isExpand}
                  resetFilter={resetFilter}
                  onClickExpand={handleClickExpand(profile.code, !!isExpand)}
                />
              </div>
            );
          })}
        </div>
      )}
    </div>
  );
};

interface ProfileSectionProps {
  isExpand: boolean;
  profile: ProfileDetail;
  resetFilter: boolean;
  onClickExpand: VoidFunction;
}
const ProfileSection: React.FC<ProfileSectionProps> = ({
  isExpand,
  profile,
  resetFilter,
  onClickExpand
}) => {
  const { t } = useTranslation();
  const { text: profileName, transactions } = profile;

  const profileElements = useSelectElementsForManageAdjustmentSystem();

  const [expandedList, setExpandedList] = useState<string[]>([]);

  const _transactions = useMemo(
    () => transactions?.filter(f => f.status !== TransactionStatus.NotUpdated),
    [transactions]
  );

  const {
    total,
    currentPage,
    currentPageSize,
    gridData: dataView,
    onPageChange,
    onPageSizeChange,
    onResetToDefaultFilter
  } = usePagination(_transactions, [], 'batchCode');

  useEffect(() => {
    if (!resetFilter) return;

    onResetToDefaultFilter();
    setExpandedList([]);
  }, [resetFilter, onResetToDefaultFilter]);

  const renderSubRow = useCallback(
    ({ original }: { original: ProfileTransactions }) => (
      <ProfileListSubGrid
        profileTransactions={original}
        metadata={profileElements}
      />
    ),
    [profileElements]
  );

  const columns = (): ColumnType[] => [
    {
      id: 'batchCode',
      Header: t('txt_config_manage_adjustments_system_batchCode'),
      accessor: 'batchCode',
      width: 110
    },
    {
      id: 'transactionCode',
      Header: t('txt_config_manage_adjustments_system_transactionCode'),
      accessor: 'transactionCode',
      width: 120
    },
    {
      id: 'maximumAmount',
      Header: t('txt_config_manage_adjustments_system_maximumAmount'),
      accessor: formatText(['maximumAmount'], {
        format: GroupTextFormat.Amount
      }),
      className: 'text-right',
      width: 110
    },
    {
      id: 'actionCode',
      Header: t('txt_config_manage_adjustments_system_actionCode'),
      accessor: 'actionCode',
      width: 120
    },
    {
      id: 'status',
      Header: t('txt_status'),
      accessor: formatBadge(['status'], {
        colorType: BadgeColorType.ConfigParameterStatus
      }),
      width: 135
    }
  ];

  if (total === 0) return null;

  return (
    <>
      <div className="d-flex align-items-center justify-content-between">
        <div className="d-flex align-items-center">
          <Tooltip
            element={isExpand ? t('txt_collapse') : t('txt_expand')}
            opened={isDevice ? false : undefined}
            variant="primary"
            placement="top"
            triggerClassName="d-flex ml-n2"
          >
            <Button size="sm" variant="icon-secondary" onClick={onClickExpand}>
              <Icon name={isExpand ? 'minus' : 'plus'} size="4x" />
            </Button>
          </Tooltip>

          <strong className="ml-8">{profileName}</strong>
        </div>
      </div>
      <div className={classnames(!isExpand && 'd-none')}>
        <div className="mt-16">
          <Grid
            className="grid-has-tooltip center"
            togglable
            dataItemKey="rowId"
            expandedItemKey="rowId"
            data={dataView}
            columns={columns()}
            expandedList={expandedList}
            toggleButtonConfigList={dataView.map(
              mapGridExpandCollapse('rowId', t)
            )}
            subRow={renderSubRow}
            onExpand={setExpandedList}
          />
          {total > 10 && (
            <div className="mt-16">
              <Paging
                page={currentPage}
                pageSize={currentPageSize}
                totalItem={total}
                onChangePage={onPageChange}
                onChangePageSize={onPageSizeChange}
              />
            </div>
          )}
        </div>
      </div>
    </>
  );
};

export default ConfigFLSADJSummary;
