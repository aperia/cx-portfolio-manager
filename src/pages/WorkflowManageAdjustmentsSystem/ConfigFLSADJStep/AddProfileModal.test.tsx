import { RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { mockUseModalRegistryFnc, renderWithMockStore } from 'app/utils';
import React from 'react';
import AddProfileModal, { AddProfileModalProps } from './AddProfileModal';
import { mockElements } from './index.test';

const mockUseModalRegistry = mockUseModalRegistryFnc();

const renderComponent = async (
  props: any,
  initialState?: any
): Promise<RenderResult> => {
  return await renderWithMockStore(<AddProfileModal {...props} />, {
    initialState
  });
};

describe('ManageAdjustmentsSystemWorkflow > ConfigFLSADJStep > AddProfileModal', () => {
  beforeEach(() => {
    mockUseModalRegistry.mockReturnValue([true, false]);
  });

  const defaultState = {
    workflowSetup: {
      elementMetadata: { elements: mockElements },
      merchantData: { merchants: [] }
    }
  };

  it('should render with exist profiles', async () => {
    const props = { existProfileIds: ['ProfileName'] } as AddProfileModalProps;

    const { getByText } = await renderComponent(props, defaultState);

    userEvent.click(
      getByText('txt_config_manage_adjustments_system_profileName')
    );
    userEvent.click(getByText('ProfileName'));
    userEvent.click(getByText('txt_continue'));

    expect(
      getByText('txt_config_manage_adjustments_system_select_profile')
    ).toBeInTheDocument();
  });
});
