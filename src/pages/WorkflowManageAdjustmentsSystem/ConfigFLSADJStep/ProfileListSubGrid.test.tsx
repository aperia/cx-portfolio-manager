import { render } from '@testing-library/react';
import { TransactionStatus } from 'app/constants/enums';
import { ProfileTransactions } from 'app/types';
import React from 'react';
import ProfileListSubGrid from './ProfileListSubGrid';

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

const metadata = {
  transactions: [{ code: 'code', text: 'text' }],
  batchCode: [{ code: 'code', text: 'text' }],
  actionCode: [{ code: 'code', text: 'text' }]
};

describe('WorkflowManageAdjustmentsSystem > ConfigFLSADJStep > ProfileListSubGrid', () => {
  it('render grid', () => {
    const profileTransactions = {
      rowId: '1',
      status: TransactionStatus.Updated,
      batchCode: 'AB',
      actionCode: 'A',
      maximumAmount: '123',
      originalProfile: {
        status: TransactionStatus.Updated,
        batchCode: 'B',
        actionCode: 'A',
        maximumAmount: '123'
      }
    } as ProfileTransactions;

    const wrapper = render(
      <ProfileListSubGrid
        profileTransactions={profileTransactions}
        metadata={metadata}
      />
    );
    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
  });
});
