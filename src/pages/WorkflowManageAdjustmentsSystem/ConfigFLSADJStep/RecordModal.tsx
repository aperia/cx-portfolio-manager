import EnhanceDropdownList from 'app/components/EnhanceDropdownList';
import ModalRegistry from 'app/components/ModalRegistry';
import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import { TransactionStatus } from 'app/constants/enums';
import { matchSearchValue } from 'app/helpers';
import { ProfileTransactions } from 'app/types';
import {
  ComboBox,
  Grid,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  NumericTextBox,
  TextBox,
  useTranslation
} from 'app/_libraries/_dls';
import { get, isEmpty, isEqual, isFunction } from 'app/_libraries/_dls/lodash';
import _orderBy from 'lodash.orderby';
import { useSelectElementsForManageAdjustmentSystem } from 'pages/_commons/redux/WorkflowSetup';
import ClearAndResetButton from 'pages/_commons/Utils/ClearAndResetButton';
import {
  default as React,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { recordModalColumns } from './constant';
import { parameters } from './_parameterList';

interface IProps {
  id: string;
  show: boolean;
  profileTransactions?: ProfileTransactions;

  onClose?: (profileTransactions?: ProfileTransactions) => void;
}

const RecordModal: React.FC<IProps> = ({
  id,
  show,
  profileTransactions,
  onClose
}) => {
  const { t } = useTranslation();
  const [profile, setProfile] = useState(profileTransactions);
  const profileElements = useSelectElementsForManageAdjustmentSystem();
  const { transactions, actionCode } = profileElements;
  const [searchValue, setSearchValue] = useState('');

  const simpleSearchRef = useRef<any>(null);
  useEffect(() => {
    if (!searchValue && simpleSearchRef?.current) {
      simpleSearchRef.current.clear();
    }
  }, [searchValue]);

  const handleClose = () => {
    isFunction(onClose) && onClose();
  };

  const handleAdd = () => {
    if (profile !== undefined && isEqual(profileTransactions, profile)) {
      handleClose();
      return;
    }

    isFunction(onClose) &&
      onClose(
        profile
          ? profile
          : {
              actionCode: '',
              transactionCode: '',
              maximumAmount: '',
              batchCode: '',
              status: TransactionStatus.New
            }
      );
  };

  const handleSearch = (val: string) => {
    setSearchValue(val);
  };

  const handleClearFilter = () => {
    setSearchValue('');
  };

  const handleFormChange = useCallback(
    (
        field: keyof ProfileTransactions,
        valuePath: string[] = ['value'],
        isNumber?: boolean
      ) =>
      (e: any) => {
        const _value = get(e.target, valuePath);
        const value = isNumber && _value ? Number(e.target?.value) : _value;

        setProfile({ ...profile, [field]: value } as ProfileTransactions);
      },
    [profile]
  );

  const renderDropdown = useCallback(
    (data: RefData[], field: keyof ProfileTransactions) => {
      const value = data.find(o => o.code === profile?.[field]);

      return (
        <EnhanceDropdownList
          placeholder={t(
            'txt_workflow_manage_change_in_terms_select_an_option'
          )}
          dataTestId={`addNewMethod__${field}`}
          size="small"
          value={value}
          onChange={handleFormChange(field, ['value', 'code'])}
          options={data}
        />
      );
    },
    [t, profile, handleFormChange]
  );

  const renderCombobox = useCallback(
    (data: RefData[], field: keyof ProfileTransactions) => {
      const value = data.find(o => o.code === profile?.[field]);

      return (
        <ComboBox
          placeholder={t(
            'txt_workflow_manage_change_in_terms_select_an_option'
          )}
          size="small"
          textField="text"
          value={value}
          onChange={handleFormChange(field, ['value', 'code'])}
          noResult={t('txt_no_results_found_without_dot')}
        >
          {_orderBy(data, ['text']).map(item => (
            <ComboBox.Item key={item.code} value={item} label={item.text} />
          ))}
        </ComboBox>
      );
    },
    [profile, t, handleFormChange]
  );
  const valueAccessor = useCallback(
    (data: Record<string, any>) => {
      const paramId = data.id as keyof ProfileTransactions;
      switch (paramId) {
        case 'batchCode': {
          const value = profile?.batchCode;
          return (
            <TextBox
              dataTestId="profile_batchCode"
              size="sm"
              maxLength={2}
              value={value}
              onChange={handleFormChange('batchCode')}
            />
          );
        }
        case 'actionCode': {
          return renderDropdown(actionCode, paramId);
        }
        case 'transactionCode': {
          return renderCombobox(transactions, paramId);
        }
        case 'maximumAmount': {
          const value = profile?.maximumAmount;
          return (
            <NumericTextBox
              dataTestId="profile_maximumAmount"
              size="sm"
              format="c2"
              showStep={false}
              maxLength={13}
              autoFocus={false}
              value={value}
              onChange={handleFormChange('maximumAmount', ['value'], true)}
            />
          );
        }
      }
    },
    [
      profile,
      renderDropdown,
      actionCode,
      transactions,
      renderCombobox,
      handleFormChange
    ]
  );

  const filteredParameters = useMemo(
    () =>
      parameters.filter(g =>
        matchSearchValue(
          [g.bussinessName, g.greenScreenName, g.moreInfo as string],
          searchValue
        )
      ),
    [searchValue]
  );

  const gridRecordProfile = useMemo(() => {
    return (
      <Grid
        className="grid-has-tooltip grid-has-control center"
        columns={recordModalColumns(t, valueAccessor)}
        data={filteredParameters}
      />
    );
  }, [valueAccessor, t, filteredParameters]);

  return (
    <ModalRegistry lg id={id} show={show} onAutoClosedAll={handleClose}>
      <ModalHeader border closeButton onHide={handleClose}>
        <ModalTitle>
          {isEmpty(profileTransactions)
            ? t('txt_config_manage_adjustments_system_add_record')
            : t('txt_edit_record')}
        </ModalTitle>
      </ModalHeader>
      <ModalBody>
        <div className="d-flex align-items-center justify-content-between mb-16">
          <h5>{t('txt_parameter_list')}</h5>
          <SimpleSearch
            ref={simpleSearchRef}
            placeholder={t('txt_type_to_search')}
            onSearch={handleSearch}
            popperElement={
              <>
                <p className="color-grey">{t('txt_search_description')}</p>
                <ul className="search-field-item list-unstyled">
                  <li className="mt-16">{t('txt_business_name')}</li>
                  <li className="mt-16">
                    {t('txt_bulk_external_status_management_green_screen_name')}
                  </li>
                  <li className="mt-16">{t('txt_more_info')}</li>
                </ul>
              </>
            }
          />
        </div>
        {searchValue && !isEmpty(filteredParameters) && (
          <div className="d-flex justify-content-end mb-16">
            <ClearAndResetButton small onClearAndReset={handleClearFilter} />
          </div>
        )}
        {isEmpty(filteredParameters) ? (
          <div className="d-flex flex-column justify-content-center mt-40">
            <NoDataFound
              id="configFLSADJStep_notfound"
              hasSearch
              title={t('txt_no_results_found')}
              linkTitle={t('txt_clear_and_reset')}
              onLinkClicked={handleClearFilter}
            />
          </div>
        ) : (
          gridRecordProfile
        )}
      </ModalBody>
      <ModalFooter
        cancelButtonText={t('txt_cancel')}
        okButtonText={
          isEmpty(profileTransactions) ? t('txt_add') : t('txt_save')
        }
        onCancel={handleClose}
        onOk={handleAdd}
      ></ModalFooter>
    </ModalRegistry>
  );
};

export default RecordModal;
