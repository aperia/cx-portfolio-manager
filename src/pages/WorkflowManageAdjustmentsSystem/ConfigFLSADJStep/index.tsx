import { TransactionStatus } from 'app/constants/enums';
import {
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { classnames } from 'app/helpers';
import { useUnsavedChangeRegistry } from 'app/hooks';
import { ProfileDetail, ProfileRefData } from 'app/types';
import { Button, InlineMessage, useTranslation } from 'app/_libraries/_dls';
import { isEqual } from 'app/_libraries/_dls/lodash';
import { orderBy } from 'lodash';
import { STATUS } from 'pages/WorkflowManageOCSShells/ConfigureParameters/constants';
import {
  actionsWorkflowSetup,
  useProfileTransactionsStatus
} from 'pages/_commons/redux/WorkflowSetup';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, { useEffect, useMemo, useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import { ParameterCode } from '../type';
import AddProfile from './AddProfile';
import AddProfileModal from './AddProfileModal';
import ProfileList from './ProfileList';
import Summary from './Summary';
import parseFormValues from './_parseFormValues';

export interface FormConfigFLSADJ {
  isValid?: boolean;

  profiles: ProfileDetail[];
}

const ConfigFLSADJStep: React.FC<WorkflowSetupProps<FormConfigFLSADJ>> =
  props => {
    const { setFormValues, formValues, savedAt, isStuck } = props;
    const [initialValues, setInitialValues] = useState(formValues);
    const { t } = useTranslation();
    const dispatch = useDispatch();

    const keepRef = useRef({ setFormValues });
    keepRef.current.setFormValues = setFormValues;

    const { loading } = useProfileTransactionsStatus();
    const [showAddProfileModal, setShowAddProfileModal] = useState(false);

    const existProfileIds = useMemo(
      () => formValues?.profiles.map(p => p.code),
      [formValues?.profiles]
    );
    const hasProfile = useMemo(
      () => existProfileIds.length > 0,
      [existProfileIds.length]
    );
    const isValid = useMemo(
      () =>
        formValues?.profiles?.some(p =>
          p.transactions?.some(s => s.status !== STATUS.NotUpdated)
        ),
      [formValues?.profiles]
    );

    useEffect(() => {
      dispatch(
        actionsWorkflowSetup.getWorkflowMetadata([
          ParameterCode.TransactionId,

          ParameterCode.ProfileName,
          ParameterCode.BatchCode,
          ParameterCode.TransactionCode,
          ParameterCode.ActionCode
        ])
      );
    }, [dispatch]);

    useEffect(() => {
      setInitialValues(formValues);
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [savedAt]);

    useEffect(() => {
      keepRef.current.setFormValues({ isValid });
    }, [isValid]);

    const formChanged = useMemo(
      () => !isEqual(formValues.profiles, initialValues.profiles),
      [formValues.profiles, initialValues.profiles]
    );

    useUnsavedChangeRegistry(
      {
        ...unsavedExistedWorkflowSetupDataProps,
        formName:
          UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_AUTOMATED_ADJUSTMENTS_FLS_ADJ,
        priority: 1
      },
      [formChanged]
    );

    const handleSelectProfile = async (profile: ProfileRefData) => {
      const transactionsResp = (await Promise.resolve(
        dispatch(
          actionsWorkflowSetup.getManageAdjustmentsSystemProfileTransactions(
            profile.code
          )
        )
      )) as any;
      const isSuccess =
        actionsWorkflowSetup.getManageAdjustmentsSystemProfileTransactions.fulfilled.match(
          transactionsResp
        );

      if (isSuccess) {
        const _profile: ProfileDetail = {
          ...profile,
          transactions: transactionsResp.payload.map((t: any, idx: number) => {
            const rowId = `${profile.code}_${idx}_${Date.now()}`;
            const transaction = {
              ...t,
              status: TransactionStatus.NotUpdated,
              rowId: rowId,
              id: rowId
            };
            return {
              ...transaction,
              originalProfile: transaction
            };
          })
        };

        const _profiles = [_profile, ...formValues.profiles];

        setFormValues({ profiles: orderBy(_profiles, ['code']) });
      }
    };

    const handleOpenAddProfileModal = () => {
      setShowAddProfileModal(true);
    };

    const handleCloseAddProfileModal = () => {
      setShowAddProfileModal(false);
    };

    return (
      <>
        {isStuck && (
          <InlineMessage className="mb-0 mt-24" variant="danger" withIcon>
            {t('txt_step_stuck_move_forward_message')}
          </InlineMessage>
        )}
        {!hasProfile && (
          <div className={classnames('mt-24', loading && 'loading')}>
            <h5>{t('txt_config_manage_adjustments_system_select_profile')}</h5>
            <div className="row mt-16">
              <div className="col-6 col-xl-4">
                <AddProfile onChangeProfile={handleSelectProfile} />
              </div>
            </div>
          </div>
        )}
        {hasProfile && (
          <>
            <div
              className={classnames('position-relative', loading && 'loading')}
            >
              <div className="absolute-top-right mt-n26 mr-n8">
                <Button
                  variant="outline-primary"
                  size="sm"
                  onClick={handleOpenAddProfileModal}
                >
                  {t('txt_config_manage_adjustments_system_select_profile')}
                </Button>
              </div>
            </div>
            <div className="mt-24">
              <ProfileList profiles={formValues?.profiles} {...props} />

              {showAddProfileModal && (
                <AddProfileModal
                  existProfileIds={existProfileIds}
                  onClose={handleCloseAddProfileModal}
                  onAdd={handleSelectProfile}
                />
              )}
            </div>
          </>
        )}
      </>
    );
  };

const ExtraStaticConfigFLSADJStep =
  ConfigFLSADJStep as WorkflowSetupStaticProp<FormConfigFLSADJ>;

ExtraStaticConfigFLSADJStep.summaryComponent = Summary;
ExtraStaticConfigFLSADJStep.defaultValues = { isValid: false, profiles: [] };
ExtraStaticConfigFLSADJStep.parseFormValues = parseFormValues;

export default ExtraStaticConfigFLSADJStep;
