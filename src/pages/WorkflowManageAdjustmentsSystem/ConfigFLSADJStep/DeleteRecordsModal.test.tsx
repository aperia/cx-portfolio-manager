import { fireEvent, render, RenderResult } from '@testing-library/react';
import { mockUseModalRegistryFnc } from 'app/utils';
import React from 'react';
import DeleteRecordsModal from './DeleteRecordsModal';

const mockUseModalRegistry = mockUseModalRegistryFnc();

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderModal = (props: any): RenderResult => {
  return render(
    <div>
      <DeleteRecordsModal {...props} />
    </div>
  );
};

describe('WorkflowManageAdjustmentsSystem > ConfigFLSADJStep > DeleteRecordsModal', () => {
  beforeEach(() => {
    mockUseModalRegistry.mockImplementation(() => [true, false]);
  });

  afterEach(() => {
    mockUseModalRegistry.mockClear();
  });

  it('Should close modal without delete', () => {
    const onClose = jest.fn();

    const props = {
      id: '1',
      show: true,
      isBulk: true,
      onClose: onClose
    };

    const wrapper = renderModal(props);
    wrapper.debug();
    fireEvent.click(
      wrapper.baseElement.querySelector(
        'button[class="btn btn-secondary"]'
      ) as Element
    );
    expect(onClose).toHaveBeenCalled();
  });

  it('Should close modal with deleted', () => {
    const onDelete = jest.fn();

    const props = {
      id: '1',
      show: true,
      onDelete: onDelete
    };

    const wrapper = renderModal(props);

    fireEvent.click(
      wrapper.baseElement.querySelector(
        'button[class="btn btn-danger"]'
      ) as Element
    );
    expect(onDelete).toHaveBeenCalled();
  });
});
