import { fireEvent, render } from '@testing-library/react';
import 'app/utils/_mockComponent/mockModalRegistry';
import 'app/utils/_mockComponent/mockUseTranslation';
import { STATUS } from 'pages/WorkflowManageOCSShells/ConfigureParameters/constants';
import React, { useState } from 'react';
import { DeleteShell, useDeleteShell } from './DeleteShell';

const mockDeleteCell = jest.fn();

const FakeComponent = ({ isBulkDelete }: { isBulkDelete: boolean }) => {
  const [, mockSetDataChecked] = useState(['1']);
  const { openDeleteForm, ...deleteCellProps } = useDeleteShell({
    onDelete: mockDeleteCell,
    onSetDataChecked: mockSetDataChecked
  });
  return (
    <>
      <button
        onClick={() => {
          openDeleteForm(
            isBulkDelete
              ? [
                  {
                    rowId: '1',
                    shellName: 'John',
                    newShellName: 'Snick',
                    status: STATUS.Updated
                  },
                  {
                    rowId: '2',
                    shellName: 'John',
                    newShellName: 'Snick',
                    status: STATUS.NotUpdated
                  }
                ]
              : [
                  {
                    rowId: '1',
                    shellName: 'John',
                    newShellName: 'Snick',
                    status: STATUS.Updated
                  }
                ]
          );
        }}
      >
        Open
      </button>
      <DeleteShell id="delete" {...deleteCellProps} />
    </>
  );
};

const factory = (isBulkDelete = false) => {
  const { getByText, queryByText } = render(
    <FakeComponent isBulkDelete={isBulkDelete} />
  );
  const openModal = () => {
    const openElement = getByText('Open');
    fireEvent.click(openElement);
  };

  const onClose = () => {
    const cancelButton = getByText('txt_cancel');
    fireEvent.click(cancelButton);
  };

  const onSubmit = () => {
    const submitButton = getByText('txt_remove');
    fireEvent.click(submitButton);
  };

  const title = () => queryByText('txt_confirm_remove');
  return {
    openModal,
    title,
    onClose,
    onSubmit
  };
};

describe('test cancel shell component', () => {
  it('should be render modal', async () => {
    const { openModal, title } = factory();
    openModal();
    expect(title()).toBeInTheDocument();
  });

  it('should be close', async () => {
    const { openModal, onClose, title } = factory();
    openModal();
    onClose();
    expect(title()).not.toBeInTheDocument();
  });

  it('should be submit', async () => {
    const { openModal, onSubmit } = factory();
    openModal();
    onSubmit();
    expect(mockDeleteCell).toBeCalled();
  });

  it('should be submit bulk delete', async () => {
    const { openModal, onSubmit } = factory(true);
    openModal();
    onSubmit();
    expect(mockDeleteCell).toBeCalled();
  });
});
