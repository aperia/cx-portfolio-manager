import {
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { classnames } from 'app/helpers';
import { useAsyncThunkDispatch, useUnsavedChangeRegistry } from 'app/hooks';
import { InlineMessage, useTranslation } from 'app/_libraries/_dls';
import { isEmpty, isEqual } from 'app/_libraries/_dls/lodash';
import { STATUS } from 'pages/WorkflowManageOCSShells/ConfigureParameters/constants';
import {
  actionsWorkflowSetup,
  useQueueProfileStatus,
  useSelectElementsForManageAdjustmentSystem
} from 'pages/_commons/redux/WorkflowSetup';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, { useEffect, useMemo, useRef } from 'react';
import GeneralInformation, {
  useGeneralInformation
} from '../GeneralInformation';
import { GeneralInformationModel, RefDataWithDesc } from '../type';
import AlternateAccessShellNames, {
  AlternateAccessCompRef,
  generateAccessShellNameGridData
} from './AlternateAccessShellNames';
import SelectProfile from './SelectProfile';
import Summary from './Summary';
import { Shell } from './types';
import { UnsavedChange, useUnsavedChange } from './UnsavedChange';
import parseFormValues from './_parseFormValues';

export interface FormConfigJQA {
  isValid?: boolean;
  queueOwner: string | null;
  managerId: RefData | null;
  generalInfo: GeneralInformationModel;
  alternateAccessShellNames: {
    rowId: string;
    shellName: string;
    newShellName: string;
    status: STATUS;
  }[];
}

const ConfigJQAStep: React.FC<WorkflowSetupProps<FormConfigJQA>> = ({
  setFormValues,
  formValues,
  stepId,
  selectedStep,
  savedAt,
  isStuck
}) => {
  const { t } = useTranslation();
  const dispatch = useAsyncThunkDispatch();
  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;
  const initialValues = useRef(formValues);

  const prevQueueOwnerSelected = useRef<RefDataWithDesc | null>(null);

  const alternateComponentRef = useRef<AlternateAccessCompRef>(null);

  const { loading } = useQueueProfileStatus();
  const { openUnsavedConfirm, ...unsavedChangeProps } = useUnsavedChange();
  const generalInformationProps = useGeneralInformation(formValues.generalInfo);
  const { queueOwner: queueOwnerOptions } =
    useSelectElementsForManageAdjustmentSystem();

  const selectedQueueOwner = useMemo(
    () =>
      queueOwnerOptions.find(
        q => q.code === formValues?.queueOwner
      ) as RefDataWithDesc,
    [queueOwnerOptions, formValues?.queueOwner]
  );

  const handleQueueOwnerRequestChange = (queueOwner?: RefDataWithDesc) => {
    if (prevQueueOwnerSelected.current?.code === queueOwner?.code) {
      return;
    }

    const alternateChanged = formValues.alternateAccessShellNames?.some(
      item => item.status !== STATUS.NotUpdated
    );
    if (alternateChanged) {
      openUnsavedConfirm(() => {
        handleQueueOwnerChange(queueOwner);
      }, handleDontChangeQueueOwner);
      return;
    }
    handleQueueOwnerChange(queueOwner);
  };

  const handleQueueOwnerChange = async (queueOwner?: RefDataWithDesc) => {
    try {
      if (!queueOwner) {
        prevQueueOwnerSelected.current = null;
        setFormValues({
          queueOwner: null,
          generalInfo: undefined,
          alternateAccessShellNames: []
        });
        return;
      }
      const queueProfileResp = await dispatch(
        actionsWorkflowSetup.getManageAdjustmentsSystemQueueProfile(
          queueOwner.code
        )
      ).unwrap();
      const shellNameGridData = generateAccessShellNameGridData(
        queueProfileResp.alternateAccessShellNames
      );
      prevQueueOwnerSelected.current = queueOwner;
      setFormValues({
        queueOwner: queueOwner.code,
        alternateAccessShellNames: shellNameGridData,
        generalInfo: queueProfileResp.generalInformation
      });
    } catch (error) {}
  };

  const handleDontChangeQueueOwner = () => {
    setFormValues({
      queueOwner: prevQueueOwnerSelected.current?.code ?? null
    });
  };

  const handleManagerIdChange = (managerId: RefData) => {
    setFormValues({ ...formValues, managerId });
  };

  const handleAddShell = (shell: RefDataWithDesc) => {
    const rowId = `${formValues.alternateAccessShellNames.length + 1}`;
    setFormValues({
      ...formValues,
      alternateAccessShellNames: [
        {
          rowId,
          shellName: shell.description,
          newShellName: shell.description,
          status: STATUS.New
        },
        ...formValues.alternateAccessShellNames
      ]
    });
    process.nextTick(() => {
      alternateComponentRef.current?.resetAndNavigateTo(rowId);
    });
  };

  const handleUpdateShell = (
    originalShell: Shell,
    newShell: RefDataWithDesc
  ) => {
    const newAlternateAccess = [...formValues.alternateAccessShellNames];
    const shellIndex = newAlternateAccess.findIndex(
      item => item.shellName === originalShell.shellName
    );
    const newShellName = newShell.description;
    const shellName =
      originalShell.status === STATUS.New
        ? newShell.description
        : originalShell.shellName;
    const status =
      originalShell.status === STATUS.New
        ? STATUS.New
        : shellName !== newShellName
        ? STATUS.Updated
        : STATUS.NotUpdated;
    newAlternateAccess[shellIndex] = {
      ...newAlternateAccess[shellIndex],
      newShellName,
      status,
      shellName
    };
    setFormValues({
      ...formValues,
      alternateAccessShellNames: newAlternateAccess
    });
    process.nextTick(() => {
      alternateComponentRef.current?.resetAndNavigateTo(originalShell?.rowId);
    });
  };

  const handleDeleteShell = (ids: string[]) => {
    let newAlternateAccess = [...formValues.alternateAccessShellNames];
    ids.forEach(id => {
      const rowIndex = newAlternateAccess.findIndex(f => f.rowId === id);

      // if current status equal new, then remove this record
      // else update status to mark for deletion
      if (newAlternateAccess[rowIndex].status === STATUS.New) {
        newAlternateAccess = newAlternateAccess.filter(
          item => item.rowId !== id
        );
      } else {
        newAlternateAccess[rowIndex] = {
          ...newAlternateAccess[rowIndex],
          status: STATUS.MarkedForRemoval
        };
      }
    });
    setFormValues({
      ...formValues,
      alternateAccessShellNames: newAlternateAccess
    });
  };

  const handleCancelShell = (shell: Shell) => {
    const newAlternateAccess = [...formValues.alternateAccessShellNames];
    const rowIndex = newAlternateAccess.findIndex(f => f.rowId === shell.rowId);
    newAlternateAccess[rowIndex] = {
      ...newAlternateAccess[rowIndex],
      status: STATUS.NotUpdated,
      newShellName: newAlternateAccess[rowIndex].shellName
    };
    setFormValues({
      ...formValues,
      alternateAccessShellNames: newAlternateAccess
    });
  };

  const isFormValid = useMemo(() => {
    return !!formValues?.queueOwner;
  }, [formValues?.queueOwner]);

  useEffect(() => {
    keepRef.current.setFormValues({ isValid: isFormValid });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isFormValid]);

  useEffect(() => {
    initialValues.current = formValues;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [savedAt]);

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_AUTOMATED_ADJUSTMENTS_JQA,
      priority: 1
    },
    [!isEqual(initialValues.current, formValues)]
  );

  return (
    <div className={classnames('mt-24', { loading })}>
      {isStuck && (
        <InlineMessage className="mb-0" variant="danger" withIcon>
          {t('txt_step_stuck_move_forward_message')}
        </InlineMessage>
      )}
      <SelectProfile
        loading={!!loading}
        queueOwner={selectedQueueOwner}
        setQueueOwner={handleQueueOwnerRequestChange}
        managerId={formValues.managerId}
        setManagerId={handleManagerIdChange}
      />
      {!isEmpty(formValues?.queueOwner) && (
        <GeneralInformation {...generalInformationProps} />
      )}
      {!isEmpty(formValues?.queueOwner) && (
        <AlternateAccessShellNames
          stepId={stepId}
          selectedStep={selectedStep}
          data={formValues.alternateAccessShellNames}
          onAdd={handleAddShell}
          onUpdate={handleUpdateShell}
          onDelete={handleDeleteShell}
          onCancel={handleCancelShell}
          ref={alternateComponentRef}
          queueOwner={selectedQueueOwner}
        />
      )}
      <UnsavedChange id="unsaved-change" {...unsavedChangeProps} />
    </div>
  );
};

const ExtraStaticConfigJQAStep =
  ConfigJQAStep as WorkflowSetupStaticProp<FormConfigJQA>;

ExtraStaticConfigJQAStep.summaryComponent = Summary;
ExtraStaticConfigJQAStep.defaultValues = {
  alternateAccessShellNames: undefined,
  generalInfo: undefined,
  isValid: false,
  managerId: undefined,
  queueOwner: undefined
};
ExtraStaticConfigJQAStep.parseFormValues = parseFormValues;

export default ExtraStaticConfigJQAStep;
