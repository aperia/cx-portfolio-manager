import { fireEvent, render, RenderResult } from '@testing-library/react';
import React from 'react';
import Summary from './Summary';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

jest.mock('pages/_commons/redux/WorkflowSetup', () => ({
  useSelectElementsForManageAdjustmentSystem: () => ({
    queueOwner: [
      {
        value: '',
        description: 'None',
        code: '',
        text: 'None'
      },
      {
        value: 'D',
        description:
          'Deny the item if it exceeds the amount in the LIMIT field.',
        code: 'D'
      },
      {
        value: 'Q',
        description:
          'Assign the item to the pending queue if it exceeds the amount in the LIMIT field.',
        code: 'Q',
        text: 'Q - Assign the item to the pending queue if it exceeds the amount in the LIMIT field.'
      }
    ]
  })
}));

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <Summary {...props} />
    </div>
  );
};

describe('ManageAdjustmentsSystemWorkflow > ConfigJQAStep > Summary', () => {
  it('Should render summary', () => {
    const wrapper = renderComponent({
      formValues: {
        queueOwner: { value: 'John', description: 'John', code: 'John' },
        alternateAccessShellNames: Array.from({ length: 12 }, (_, index) => ({
          rowId: `${index}`,
          shellName: 'John',
          newShellName: 'Maria',
          status: 'Updated'
        }))
      }
    } as unknown as WorkflowSetupSummaryProps);

    expect(wrapper.getByText('txt_queue_owner')).toBeInTheDocument();
  });

  it('Should render summary has resetFilter', () => {
    const wrapper = renderComponent({
      stepId: 'stepId',
      selectedStep: 'selectedStep',
      formValues: {
        queueOwner: { value: 'John', description: 'John', code: 'John' },
        alternateAccessShellNames: Array.from({ length: 12 }, (_, index) => ({
          rowId: `${index}`,
          shellName: 'John',
          newShellName: 'Maria',
          status: 'Updated'
        }))
      }
    } as unknown as WorkflowSetupSummaryProps);

    expect(wrapper.getByText('txt_queue_owner')).toBeInTheDocument();
  });

  it('Should click on edit', () => {
    const mockFn = jest.fn();
    const wrapper = renderComponent({
      formValues: {},
      onEditStep: mockFn
    } as unknown as WorkflowSetupSummaryProps);

    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(mockFn).toHaveBeenCalled();
  });
});
