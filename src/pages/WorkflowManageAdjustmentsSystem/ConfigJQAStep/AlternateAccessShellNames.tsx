import { HeadingWithSearch } from 'app/components/HeadingWithSearch';
import NoDataFound from 'app/components/NoDataFound';
import { BadgeColorType } from 'app/constants/enums';
import { getSelectedRecord } from 'app/helpers';
import { useCheckAllPagination } from 'app/hooks';
import { usePagination } from 'app/hooks/usePagination';
import {
  Button,
  ColumnType,
  Grid,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import { STATUS } from 'pages/WorkflowManageOCSShells/ConfigureParameters/constants';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import { formatBadge } from 'pages/_commons/Utils/formatGridField';
import Paging from 'pages/_commons/Utils/Paging';
import React, {
  useCallback,
  useEffect,
  useImperativeHandle,
  useMemo
} from 'react';
import { RefDataWithDesc } from '../type';
import { CancelShell, useCancelShell } from './CancelShell';
import { DeleteShell, useDeleteShell } from './DeleteShell';
import ShellForm, { useShellForm } from './ShellForm';
import { ParameterCode, Shell, ShellNameModel } from './types';

const DATA_MAX_LENGTH = 20;

export interface AlternateAccessShellNamesProps {
  queueOwner: RefDataWithDesc | null;
  data: Shell[];
  stepId: string;
  selectedStep: string;
  onAdd: (shell: RefDataWithDesc) => void;
  onUpdate: (originalShell: Shell, newShell: RefDataWithDesc) => void;
  onDelete: (ids: string[]) => void;
  onCancel: (shell: Shell) => void;
}

export type AlternateAccessCompRef = {
  resetAndNavigateTo: (rowId: string) => void;
};

const alternateAccessShellNamesDesc = [
  'txt_new_shell_name',
  'txt_shell_name',
  'txt_status'
];

const AlternateAccessShellNames = React.forwardRef<
  AlternateAccessCompRef,
  AlternateAccessShellNamesProps
>(
  (
    {
      queueOwner,
      data,
      stepId,
      selectedStep,
      onAdd,
      onUpdate,
      onDelete,
      onCancel
    },
    ref
  ) => {
    const { t } = useTranslation();
    const { width } = useSelectWindowDimension();
    const allIds = data.map(f => f.rowId!);
    const readOnlyCheckList = useMemo(() => {
      const lst: string[] = [];
      data.forEach(item => {
        if ([STATUS.Updated, STATUS.MarkedForRemoval].includes(item.status)) {
          lst.push(item.rowId);
        }
      });
      return lst;
    }, [data]);

    const checkableIds = allIds.filter(
      id => !readOnlyCheckList.includes(id)
    ) as string[];

    const {
      sort,
      total,
      currentPage,
      currentPageSize,
      gridData: dataView,
      allDataFilter,
      dataChecked: _checkedList,
      onSetDataChecked,
      onSortChange,
      onPageChange,
      onPageSizeChange,
      onResetToDefaultFilter,
      searchValue,
      onSearchChange,
      navigateToItemAndResetDefault
    } = usePagination(
      data,
      [
        ParameterCode.ShellName,
        ParameterCode.NewShellName,
        ParameterCode.Status
      ],
      ParameterCode.ShellName
    );
    const checkedList = useMemo<string[]>(
      () =>
        _checkedList.filter(
          checkedId => !readOnlyCheckList.includes(checkedId)
        ),
      [_checkedList, readOnlyCheckList]
    );
    const readOnlyGrid = useMemo(
      () =>
        dataView
          .filter(row => readOnlyCheckList.includes(row.rowId))
          .map(f => f.rowId!),
      [dataView, readOnlyCheckList]
    );
    const { gridClassName, handleClickCheckAll } = useCheckAllPagination(
      {
        gridClassName: `alternate-access-shell-names-grid`,
        allIds,
        hasFilter: !!searchValue,
        filterIds: allDataFilter.map(f => f.rowId!),
        checkedList,
        setCheckAll: _isCheckAll =>
          onSetDataChecked(_isCheckAll ? checkableIds : []),
        setCheckList: onSetDataChecked,
        checkableIds
      },
      [searchValue, sort, currentPage, currentPageSize]
    );

    const { openForm, ...shellFormProps } = useShellForm({ onAdd, onUpdate });
    const { openDeleteForm, ...deleteShellProps } = useDeleteShell({
      onDelete,
      onSetDataChecked
    });
    const { openCancelForm, ...cancelShellProps } = useCancelShell({
      onCancel
    });

    const handleAddShell = () => {
      openForm('ADD');
    };

    const actionAccessor = useCallback(
      (row: Shell) => {
        const status = row.status;
        const showCancel = [STATUS.Updated, STATUS.MarkedForRemoval].includes(
          status
        );
        const showDelete = [STATUS.NotUpdated, STATUS.New].includes(status);
        const showEdit = ![STATUS.MarkedForRemoval].includes(status);
        const handleClickEdit = () => {
          openForm('EDIT', row);
        };

        return (
          <div className="d-flex justify-content-center">
            {showEdit && (
              <Button
                size="sm"
                variant="outline-primary"
                className="mx-4"
                onClick={handleClickEdit}
              >
                {t('txt_edit')}
              </Button>
            )}

            {showCancel && (
              <Button
                size="sm"
                variant="outline-danger"
                className="mx-4"
                onClick={() => {
                  openCancelForm(row);
                }}
              >
                {t('txt_cancel')}
              </Button>
            )}
            {showDelete && (
              <Button
                size="sm"
                variant="outline-danger"
                className="mx-4"
                onClick={() => {
                  openDeleteForm([row]);
                }}
              >
                {t('txt_remove')}
              </Button>
            )}
          </div>
        );
      },
      [openCancelForm, openDeleteForm, openForm, t]
    );

    const columns: ColumnType<Shell>[] = useMemo(() => {
      return [
        {
          id: 'shellName',
          Header: t('txt_shell_name'),
          accessor: 'shellName',
          isSort: true,
          width: width < 1280 ? 279 : undefined
        },
        {
          id: 'newShellName',
          Header: t('txt_new_shell_name'),
          accessor: 'newShellName',
          isSort: true,
          width: width < 1280 ? 280 : undefined
        },
        {
          id: 'status',
          Header: t('txt_status'),
          accessor: formatBadge(['status'], {
            colorType: BadgeColorType.ConfigParameterStatus
          }),
          width: width < 1280 ? 169 : undefined
        },
        {
          id: 'code',
          Header: t('txt_actions'),
          className: 'text-center',
          accessor: actionAccessor,
          width: 133,
          cellBodyProps: { className: 'py-8' }
        }
      ];
    }, [actionAccessor, t, width]);

    const selectedText = useMemo(
      () =>
        getSelectedRecord({
          selected: checkedList.length,
          total: checkableIds.length,
          text: t('txt_config_manage_adjustments_system_shells_selected', {
            selected: checkedList.length
          }),
          text1: t('txt_config_manage_adjustments_system_1_shell_selected'),
          textAll: t('txt_config_manage_adjustments_system_all_shell_selected')
        }),
      [checkedList.length, checkableIds.length, t]
    );

    const handleClearFilter = useCallback(() => {
      onSearchChange('');
    }, [onSearchChange]);

    const resetAndNavigateTo = useCallback(
      (rowId: string) => {
        onSearchChange('');
        navigateToItemAndResetDefault('rowId', rowId);
      },
      [navigateToItemAndResetDefault, onSearchChange]
    );

    useImperativeHandle(ref, () => ({ resetAndNavigateTo }), [
      resetAndNavigateTo
    ]);

    useEffect(() => {
      onSearchChange(searchValue);
    }, [onSearchChange, searchValue]);

    useEffect(() => {
      if (stepId !== selectedStep) {
        onSetDataChecked([]);
        onResetToDefaultFilter();
        handleClearFilter();
      }
    }, [
      onSetDataChecked,
      onResetToDefaultFilter,
      handleClearFilter,
      stepId,
      selectedStep
    ]);

    useEffect(() => {
      // reset default when queueOwner changed
      onSetDataChecked([]);
      onResetToDefaultFilter();
      handleClearFilter();

      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [queueOwner?.code]);

    return (
      <>
        <div className="mt-24 pt-24 border-top">
          <HeadingWithSearch
            txtTitle="txt_alternate_access_shell_names"
            searchValue={searchValue}
            onSearchChange={onSearchChange}
            searchDescription={alternateAccessShellNamesDesc}
            onClearSearch={handleClearFilter}
            isDataEmpty={isEmpty(dataView)}
          />
          <div className="d-flex justify-content-end">
            <Tooltip
              element={t('txt_have_reached_the_maximum_shell_names')}
              opened={data.length >= DATA_MAX_LENGTH ? undefined : false}
              placement="top"
            >
              <Button
                className="ml-16 mr-n8"
                size="sm"
                variant="outline-primary"
                disabled={data.length >= DATA_MAX_LENGTH}
                onClick={handleAddShell}
              >
                {t('txt_add_shell')}
              </Button>
            </Tooltip>
          </div>
          {isEmpty(data) && (
            <NoDataFound
              id="newMethod_notfound"
              title={t('txt_no_shells_to_display')}
              className="mt-40"
            />
          )}
          {!isEmpty(data) && isEmpty(dataView) && searchValue && (
            <div className="d-flex flex-column justify-content-center mt-40 mb-24">
              <NoDataFound
                id="newMethod_notfound"
                hasSearch
                title={t('txt_no_results_found')}
                linkTitle={t('txt_clear_and_reset')}
                onLinkClicked={handleClearFilter}
              />
            </div>
          )}

          {!isEmpty(dataView) && (
            <div className="mt-16">
              {
                <div className="d-flex align-items-center">
                  {!searchValue && <p className="mb-12 py-4">{selectedText}</p>}
                  {checkedList.length > 1 && (
                    <Button
                      variant="outline-danger"
                      size="sm"
                      className="ml-auto mr-n8 mb-12"
                      onClick={() => {
                        openDeleteForm(
                          data.filter(_item =>
                            checkedList.includes(_item.rowId)
                          )
                        );
                      }}
                    >
                      {t('txt_bulk_remove')}
                    </Button>
                  )}
                </div>
              }

              <Grid
                className={gridClassName}
                columns={columns}
                dataItemKey="rowId"
                sortBy={[sort]}
                onSortChange={onSortChange}
                variant={{
                  id: 'rowId',
                  type: 'checkbox',
                  cellHeaderProps: {
                    onClick: handleClickCheckAll
                  }
                }}
                onCheck={onSetDataChecked}
                readOnlyCheckbox={readOnlyGrid}
                checkboxTooltipPropsList={readOnlyGrid?.map(id => ({
                  id: id,
                  element: t('txt_disabled_shell_tooltip')
                }))}
                checkedList={checkedList}
                data={dataView}
              />
              {total > 10 && (
                <div className="mt-16">
                  <Paging
                    page={currentPage}
                    pageSize={currentPageSize}
                    totalItem={total}
                    onChangePage={onPageChange}
                    onChangePageSize={onPageSizeChange}
                  />
                </div>
              )}
            </div>
          )}
        </div>
        {shellFormProps.show && (
          <ShellForm id="add-edit-shell" {...shellFormProps} />
        )}
        <DeleteShell id="delete-shell" {...deleteShellProps} />
        <CancelShell id="cancel-shell" {...cancelShellProps} />
      </>
    );
  }
);

const generateAccessShellNameGridData = (shellNames: ShellNameModel[]) => {
  return shellNames
    ? shellNames.map(({ shellName }, index) => ({
        rowId: `${index}`,
        shellName,
        newShellName: shellName,
        status: STATUS.NotUpdated
      }))
    : [];
};

export { generateAccessShellNameGridData };
export default AlternateAccessShellNames;
