import { fireEvent, render } from '@testing-library/react';
import 'app/utils/_mockComponent/mockModalRegistry';
import 'app/utils/_mockComponent/mockUseTranslation';
import { STATUS } from 'pages/WorkflowManageOCSShells/ConfigureParameters/constants';
import React from 'react';
import { CancelShell, useCancelShell } from './CancelShell';

const mockCancelCell = jest.fn();

const FakeComponent = () => {
  const { openCancelForm, ...cancelCellProps } = useCancelShell({
    onCancel: mockCancelCell
  });
  return (
    <>
      <button
        onClick={() => {
          openCancelForm({
            rowId: '1',
            shellName: 'John',
            newShellName: 'Snick',
            status: STATUS.Updated
          });
        }}
      >
        Open
      </button>
      <CancelShell id="cancel" {...cancelCellProps} />
    </>
  );
};

const factory = () => {
  const { getByText, queryByText } = render(<FakeComponent />);
  const openModal = () => {
    const openElement = getByText('Open');
    fireEvent.click(openElement);
  };

  const onClose = () => {
    const cancelButton = getByText('txt_cancel');
    fireEvent.click(cancelButton);
  };

  const onSubmit = () => {
    const submitButton = getByText('txt_confirm');
    fireEvent.click(submitButton);
  };

  const title = () => queryByText('txt_confirm_cancel');
  return {
    openModal,
    title,
    onClose,
    onSubmit
  };
};

describe('test cancel shell component', () => {
  it('should be render modal', async () => {
    const { openModal, title } = factory();
    openModal();
    expect(title()).toBeInTheDocument();
  });

  it('should be close', async () => {
    const { openModal, onClose, title } = factory();
    openModal();
    onClose();
    expect(title()).not.toBeInTheDocument();
  });

  it('should be submit', async () => {
    const { openModal, onSubmit } = factory();
    openModal();
    onSubmit();
    expect(mockCancelCell).toBeCalled();
  });
});
