import { act, fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { mockThunkAction, renderWithMockStore } from 'app/utils';
import 'app/utils/_mockComponent/mockModalRegistry';
import 'app/utils/_mockComponent/mockUseTranslation';
import 'app/_libraries/_dls/test-utils/mocks/mockCanvas.ts';
import { STATUS } from 'pages/WorkflowManageOCSShells/ConfigureParameters/constants';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import React from 'react';
import ShellForm, { useShellForm } from './ShellForm';
import { ShellFormType } from './types';

const mockOnAdd = jest.fn();
const mockOnUpdate = jest.fn();

const FakeComponent = ({ formType }: { formType: ShellFormType }) => {
  const { openForm, ...shellFormProps } = useShellForm({
    onAdd: mockOnAdd,
    onUpdate: mockOnUpdate
  });

  return (
    <>
      <button
        onClick={() => {
          openForm(formType, {
            rowId: '1',
            shellName: 'John',
            newShellName: 'Snick',
            status: STATUS.Updated
          });
        }}
      >
        Open
      </button>
      <ShellForm id="shell-form" {...shellFormProps} />
    </>
  );
};

const factory = async (formType: ShellFormType) => {
  const wrapper = await renderWithMockStore(
    <FakeComponent formType={formType} />,
    {
      initialState: {
        workflowSetup: {
          elementMetadata: {
            loading: false,
            elements: [
              {
                id: '331',
                name: 'shell.name',
                options: [
                  { value: 'John', code: 'John', description: 'John' },
                  { value: 'Maxel', code: 'Maxel', description: 'Maxel' }
                ]
              }
            ]
          }
        }
      }
    }
  );
  const { queryByText, getByText, baseElement } = wrapper;
  const openBtn = getByText('Open');
  const openForm = () => {
    act(() => {
      fireEvent.click(openBtn);
    });
  };

  const addFormTitle = () => queryByText('txt_add_shell');
  const updateFormTitle = () => queryByText('txt_edit_shell');

  const changeCombobox = () => {
    jest.useFakeTimers();
    const cbbIcon = baseElement.querySelector('.icon.icon-chevron-down');
    fireEvent.mouseDown(cbbIcon!);
    const shellItem = getByText(/John/i);

    act(() => {
      userEvent.click(shellItem);
    });
    jest.runAllTimers();
  };

  const closeForm = () => {
    act(() => {
      fireEvent.click(getByText('txt_cancel'));
    });
  };

  const submitForm = () => {
    act(() => {
      fireEvent.click(getByText(formType === 'ADD' ? 'txt_add' : 'txt_save'));
    });
  };

  return {
    openForm,
    addFormTitle,
    updateFormTitle,
    changeCombobox,
    closeForm,
    submitForm
  };
};

const mockActionsWorkflowSetup = mockThunkAction(actionsWorkflowSetup);

beforeEach(() => {
  mockActionsWorkflowSetup('getWorkflowMetadata');
});

describe('test ShellForm component', () => {
  it('should be render add form', async () => {
    const { openForm, addFormTitle, changeCombobox } = await factory('ADD');
    openForm();
    expect(addFormTitle()).toBeInTheDocument();

    await changeCombobox();
  });

  it('should be render edit form', async () => {
    const { openForm, updateFormTitle } = await factory('EDIT');
    openForm();
    expect(updateFormTitle()).toBeInTheDocument();
  });

  it('should be close the form', async () => {
    const { openForm, closeForm, addFormTitle } = await factory('ADD');
    openForm();
    closeForm();
    expect(addFormTitle()).not.toBeInTheDocument();
  });

  it('should be submit the add form', async () => {
    const { openForm, submitForm, changeCombobox } = await factory('ADD');
    openForm();
    changeCombobox();
    submitForm();
    expect(mockOnAdd).toBeCalled();
  });

  it('should be submit the edit form', async () => {
    const { openForm, submitForm } = await factory('EDIT');
    openForm();
    submitForm();
    expect(mockOnUpdate).toBeCalled();
  });
});
