import ModalRegistry from 'app/components/ModalRegistry';
import {
  Button,
  ModalBody,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import React, { useCallback, useRef, useState } from 'react';

export interface UnsavedChangeProps {
  id: string;
  show: boolean;
  onClose: () => void;
  onContinue: () => void;
  onSubmit: () => void;
}

const UnsavedChange: React.FC<UnsavedChangeProps> = ({
  id,
  show,
  onClose,
  onContinue,
  onSubmit
}) => {
  const { t } = useTranslation();

  return (
    <ModalRegistry id={id} show={show} onAutoClosedAll={onClose}>
      <ModalHeader border closeButton onHide={onClose}>
        <ModalTitle>{t('txt_unsaved_data')}</ModalTitle>
      </ModalHeader>
      <ModalBody>
        <span>{t('txt_queue_owner_changed_unsaved_msg')}</span>
      </ModalBody>
      <div className="dls-modal-footer modal-footer">
        <Button variant="secondary" onClick={onContinue}>
          {t('txt_continue_working')}
        </Button>
        <Button variant="danger" onClick={onSubmit}>
          {t('txt_discard_data')}
        </Button>
      </div>
    </ModalRegistry>
  );
};

const useUnsavedChange = () => {
  const [show, setShow] = useState(false);
  const callbackRef = useRef<() => void>();
  const cancelCallbackRef = useRef<() => void>();
  const openUnsavedConfirm = useCallback(
    (callback: () => void, cancelCb: () => void) => {
      setShow(true);
      callbackRef.current = callback;
      cancelCallbackRef.current = cancelCb;
    },
    []
  );

  const onClose = useCallback(() => {
    setShow(false);
    callbackRef.current = undefined;
    cancelCallbackRef.current = undefined;
  }, []);

  const onSubmit = useCallback(() => {
    callbackRef.current && callbackRef.current();
    onClose();
  }, [onClose]);

  const onContinue = useCallback(() => {
    cancelCallbackRef.current && cancelCallbackRef.current();
    onClose();
  }, [onClose]);
  return { show, onClose, onSubmit, openUnsavedConfirm, onContinue };
};

export { UnsavedChange, useUnsavedChange };
