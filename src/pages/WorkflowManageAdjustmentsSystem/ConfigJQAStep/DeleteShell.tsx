import ModalRegistry from 'app/components/ModalRegistry';
import {
  Button,
  ModalBody,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import React, { useCallback, useState } from 'react';
import { Shell } from './types';

export interface DeleteShellProps {
  id: string;
  show: boolean;
  isBulkDelete: boolean;
  onClose: () => void;
  onSubmit: () => void;
}

const DeleteShell: React.FC<DeleteShellProps> = ({
  id,
  show,
  isBulkDelete,
  onClose,
  onSubmit
}) => {
  const { t } = useTranslation();

  return (
    <ModalRegistry id={id} show={show} onAutoClosedAll={onClose}>
      <ModalHeader border closeButton onHide={onClose}>
        <ModalTitle>{t('txt_confirm_remove')}</ModalTitle>
      </ModalHeader>
      <ModalBody>
        <span>
          {isBulkDelete
            ? t('txt_bulk_remove_shell_description')
            : t('txt_remove_shell_description')}
        </span>
      </ModalBody>
      <div className="dls-modal-footer modal-footer">
        <Button variant="secondary" onClick={onClose}>
          {t('txt_cancel')}
        </Button>
        <Button variant="danger" onClick={onSubmit}>
          {t('txt_remove')}
        </Button>
      </div>
    </ModalRegistry>
  );
};

const useDeleteShell = ({
  onDelete,
  onSetDataChecked
}: {
  onDelete: (ids: string[]) => void;
  onSetDataChecked: (ids: any) => void;
}) => {
  const [isBulkDelete, setIsBulkDelete] = useState(false);
  const [show, setShow] = useState(false);
  const [shells, setShells] = useState<Shell[]>([]);
  const openDeleteForm = useCallback((_shells: Shell[]) => {
    setIsBulkDelete(_shells.length > 1);
    setShells(_shells);
    setShow(true);
  }, []);

  const onClose = useCallback(() => {
    setShow(false);
    setShells([]);
  }, []);

  const onSubmit = useCallback(() => {
    const idList = shells.map(item => item.rowId);
    onDelete(idList);
    // reset checkbox after delete
    onSetDataChecked((prev: string[]) =>
      prev.filter(id => !idList.includes(id))
    );
    onClose();
  }, [onClose, onDelete, onSetDataChecked, shells]);

  return { isBulkDelete, show, onClose, openDeleteForm, onSubmit };
};

export { DeleteShell, useDeleteShell };
