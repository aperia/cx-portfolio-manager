import ModalRegistry from 'app/components/ModalRegistry';
import {
  Button,
  ModalBody,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import React, { useCallback, useState } from 'react';
import { Shell } from './types';

export interface CancelShellProps {
  id: string;
  show: boolean;
  onClose: () => void;
  onSubmit: () => void;
}

const CancelShell: React.FC<CancelShellProps> = ({
  id,
  show,
  onClose,
  onSubmit
}) => {
  const { t } = useTranslation();

  return (
    <ModalRegistry id={id} show={show} onAutoClosedAll={onClose}>
      <ModalHeader border closeButton onHide={onClose}>
        <ModalTitle>{t('txt_confirm_cancel')}</ModalTitle>
      </ModalHeader>
      <ModalBody>
        <span>{t('txt_cancel_shell_description')}</span>
      </ModalBody>
      <div className="dls-modal-footer modal-footer">
        <Button variant="secondary" onClick={onClose}>
          {t('txt_cancel')}
        </Button>
        <Button variant="danger" onClick={onSubmit}>
          {t('txt_confirm')}
        </Button>
      </div>
    </ModalRegistry>
  );
};

const useCancelShell = ({ onCancel }: { onCancel: (shell: Shell) => void }) => {
  const [show, setShow] = useState(false);
  const [shell, setShell] = useState<Shell | null>(null);
  const openCancelForm = useCallback((_shell: Shell) => {
    setShell(_shell);
    setShow(true);
  }, []);

  const onClose = useCallback(() => {
    setShow(false);
    setShell(null);
  }, []);

  const onSubmit = useCallback(() => {
    onCancel(shell!);
    onClose();
  }, [onCancel, onClose, shell]);

  return { show, onClose, openCancelForm, onSubmit };
};

export { CancelShell, useCancelShell };
