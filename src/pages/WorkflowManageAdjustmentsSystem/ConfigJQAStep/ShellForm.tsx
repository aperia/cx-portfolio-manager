import ModalRegistry from 'app/components/ModalRegistry';
import { useAsyncThunkDispatch, useFormValidations } from 'app/hooks';
import {
  ComboBox,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import { orderBy } from 'app/_libraries/_dls/lodash';
import classNames from 'classnames';
import {
  actionsWorkflowSetup,
  useSelectElementMetadataStatus,
  useSelectElementsForManageAdjustmentSystem
} from 'pages/_commons/redux/WorkflowSetup';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { ParameterCode, RefDataWithDesc } from '../type';
import { Shell, ShellFormType } from './types';

export interface ShellFormProps {
  show: boolean;
  id: string;
  onClose: () => void;
  formType: ShellFormType;
  updateShell?: Shell;
  onSubmit: (shell: RefDataWithDesc) => void;
}

const ShellForm: React.FC<ShellFormProps> = props => {
  const { id, show, onClose, formType, updateShell, onSubmit } = props;
  const { t } = useTranslation();
  const dispatch = useAsyncThunkDispatch();
  const [selected, setSelected] = useState(shellToRefData(updateShell));
  const { shellNames } = useSelectElementsForManageAdjustmentSystem();
  const { loading: metadataLoading } = useSelectElementMetadataStatus();

  const shellNameOptions = useMemo(
    () => orderBy(shellNames, ['description'], ['asc']),
    [shellNames]
  );
  const currentErrors = useMemo(() => {
    return {
      shellName: {
        status: !selected,
        message: selected ? '' : t('txt_shell_name_is_required')
      }
    };
  }, [selected, t]);

  const [touches, errors, setTouched] = useFormValidations<any>(
    currentErrors,
    0
  );

  const isFormValid =
    (formType === 'EDIT' || touches.shellName) && !errors.shellName?.status;

  const handleSubmit = () => {
    onSubmit(selected!);
    onClose();
  };

  useEffect(() => {
    setSelected(shellToRefData(updateShell));
  }, [updateShell]);

  useEffect(() => {
    dispatch(
      actionsWorkflowSetup.getWorkflowMetadata([ParameterCode.ShellName])
    );
  }, [dispatch]);

  return (
    <ModalRegistry id={id} show={show} onAutoClosedAll={onClose} xs>
      <ModalHeader border closeButton onHide={onClose}>
        <ModalTitle>
          {formType === 'ADD' ? t('txt_add_shell') : t('txt_edit_shell')}
        </ModalTitle>
      </ModalHeader>
      <ModalBody>
        <ComboBox
          required
          textField="description"
          label={t('txt_shell_name')}
          placeholder={t('txt_shell_name')}
          value={
            (shellNameOptions as RefDataWithDesc[]).find(
              shell => shell.description === selected?.description
            ) ?? selected
          }
          onChange={e => setSelected(e.target.value)}
          onFocus={setTouched('shellName')}
          onBlur={setTouched('shellName', true)}
          error={errors.shellName}
          loading={metadataLoading}
        >
          {(shellNameOptions as RefDataWithDesc[]).map(item => (
            <ComboBox.Item
              key={item.code}
              value={item}
              label={item.description}
            />
          ))}
        </ComboBox>
      </ModalBody>
      <ModalFooter
        cancelButtonText={t('txt_cancel')}
        okButtonText={t(
          classNames({
            txt_add: formType === 'ADD',
            txt_save: formType === 'EDIT'
          })
        )}
        onCancel={onClose}
        onOk={handleSubmit}
        disabledOk={!isFormValid}
      />
    </ModalRegistry>
  );
};

const shellToRefData = (shell?: Shell) => {
  if (!shell) {
    return undefined;
  }
  return {
    code: shell.newShellName,
    description: shell.newShellName,
    text: shell.newShellName
  } as RefDataWithDesc;
};

const useShellForm = ({
  onAdd,
  onUpdate
}: {
  onAdd: (shell: RefDataWithDesc) => void;
  onUpdate: (originalShell: Shell, newShell: RefDataWithDesc) => void;
}) => {
  const [formType, setFormType] = useState<ShellFormType>();
  const [updateShell, setUpdateShell] = useState<Shell>();

  const openForm = useCallback((type: ShellFormType, updateShell?: Shell) => {
    setFormType(type);
    setUpdateShell(updateShell);
  }, []);

  const onClose = useCallback(() => {
    setFormType(undefined);
  }, []);

  const show = useMemo(() => !!formType, [formType]);

  const onSubmit = useCallback(
    (value: RefDataWithDesc) => {
      if (formType === 'EDIT') {
        onUpdate(updateShell!, value);
      } else {
        onAdd(value);
      }
    },
    [formType, updateShell, onAdd, onUpdate]
  );

  return {
    show,
    onClose,
    openForm,
    formType,
    updateShell,
    onSubmit
  };
};

export { useShellForm };
export default ShellForm;
