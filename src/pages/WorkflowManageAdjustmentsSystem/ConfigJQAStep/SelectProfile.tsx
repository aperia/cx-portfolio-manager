import FieldTooltip from 'app/components/FieldTooltip';
import { useFormValidations } from 'app/hooks';
import { ComboBox, useTranslation } from 'app/_libraries/_dls';
import orderBy from 'lodash.orderby';
import {
  actionsWorkflowSetup,
  useSelectElementMetadataStatus,
  useSelectElementsForManageAdjustmentSystem
} from 'pages/_commons/redux/WorkflowSetup';
import React, { useEffect, useMemo, useState } from 'react';
import { useDispatch } from 'react-redux';
import { RefDataWithDesc } from '../type';
import { MetaData } from './types';

export interface SelectProfileProps {
  queueOwner: RefDataWithDesc | null;
  setQueueOwner: (val: RefDataWithDesc) => void;
  managerId: RefData | null;
  setManagerId: (val: RefData) => void;
  loading: boolean;
}

const SelectProfile: React.FC<SelectProfileProps> = ({
  queueOwner: queueOwnerProp,
  setQueueOwner: setQueueOwnerProp,
  managerId,
  setManagerId,
  loading
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const [queueOwner, setQueueOwner] = useState(queueOwnerProp);

  const { queueOwner: queueOwnerOptions, managerId: managerIdOptions } =
    useSelectElementsForManageAdjustmentSystem();

  const { loading: metadataLoading } = useSelectElementMetadataStatus();

  const managerIdOptionsOrdered = orderBy(managerIdOptions, ['text'], ['asc']);

  const currentErrors = useMemo(() => {
    return {
      queueOwner: {
        status: !queueOwner && !loading,
        message: queueOwner ? '' : t('txt_queue_owner_is_required')
      }
    };
  }, [loading, queueOwner, t]);

  const [, errors, setTouched] = useFormValidations<any>(currentErrors, 0);

  useEffect(() => {
    dispatch(
      actionsWorkflowSetup.getWorkflowMetadata([
        MetaData.QueueOwner,
        MetaData.ManagerId
      ])
    );
  }, [dispatch]);

  useEffect(() => {
    setQueueOwner(queueOwnerProp);
  }, [queueOwnerProp]);

  return (
    <>
      <h5 className="mt-24">
        {t('txt_config_manage_adjustments_system_select_profile')}
      </h5>
      <div className="row">
        <div className="col-6 col-xl-4 mt-16">
          <FieldTooltip
            keepShowOnDevice={false}
            placement="right-start"
            element={
              <div>
                <div className="text-uppercase">
                  {t('txt_queue_owner_tooltip_header')}
                </div>
                <p className="mt-8">{t('txt_queue_owner_tooltip_body')}</p>
              </div>
            }
          >
            <ComboBox
              required
              textField="description"
              label={t('txt_queue_owner')}
              placeholder={t('txt_queue_owner')}
              value={
                queueOwnerOptions.find(q => q.code === queueOwner?.code) ??
                queueOwner
              }
              onChange={e => setQueueOwner(e.target.value)}
              onFocus={setTouched('queueOwner')}
              onBlur={e => {
                setTouched('queueOwner', true)();
                setQueueOwnerProp((e.target as any)?.value ?? null);
              }}
              error={errors.queueOwner}
              loading={metadataLoading}
            >
              {(queueOwnerOptions as RefDataWithDesc[]).map(item => (
                <ComboBox.Item
                  key={item.code}
                  value={item}
                  label={item.description}
                />
              ))}
            </ComboBox>
          </FieldTooltip>
        </div>
        <div className="col-6 col-xl-4 mt-16">
          <FieldTooltip
            keepShowOnDevice={false}
            placement="right-start"
            element={
              <div>
                <div className="text-uppercase">
                  {t('txt_manager_id_tooltip_header')}
                </div>
                <p className="mt-8">{t('txt_manager_id_tooltip_body')}</p>
              </div>
            }
          >
            <ComboBox
              textField="text"
              label={t('txt_manager_id')}
              placeholder={t('txt_manager_id')}
              value={managerId}
              onChange={e => setManagerId(e.target.value)}
              loading={metadataLoading}
            >
              {managerIdOptionsOrdered.map(item => (
                <ComboBox.Item key={item.code} value={item} label={item.text} />
              ))}
            </ComboBox>
          </FieldTooltip>
        </div>
      </div>
    </>
  );
};

export default SelectProfile;
