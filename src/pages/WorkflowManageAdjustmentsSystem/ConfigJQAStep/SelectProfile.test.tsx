import { fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { mockThunkAction, renderWithMockStore } from 'app/utils';
import 'app/_libraries/_dls/test-utils/mocks/mockCanvas.ts';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import React from 'react';
import SelectProfile from './SelectProfile';

// mock DOMMatrix

(function mockDOMMatrix() {
  class DOMMatrixMock {
    scale = jest.fn();
    translate = jest.fn();
  }
  global.DOMMatrix = DOMMatrixMock as any;
})();

const mSetQueueOwner = jest.fn();
const mSetManagerId = jest.fn();

const factory = async () => {
  const { getByText, baseElement } = await renderWithMockStore(
    <SelectProfile
      queueOwner={null}
      managerId={{ code: 'Joe', text: 'Joe' }}
      loading={false}
      setQueueOwner={mSetQueueOwner}
      setManagerId={mSetManagerId}
    />,
    {
      initialState: {
        workflowSetup: {
          elementMetadata: {
            loading: false,
            elements: [
              {
                id: '331',
                name: 'queue.owner',
                options: [
                  { value: 'John', code: 'John', description: 'John' },
                  { value: 'Maxel', code: 'Maxel', description: 'Maxel' }
                ]
              },
              {
                id: '331',
                name: 'manager.id',
                options: [
                  {
                    value: 'Joe',
                    code: 'Joe',
                    description: 'Joe'
                  },
                  { value: 'Anne', code: 'Anne', description: 'Anne' }
                ]
              }
            ]
          },
          queueProfile: {}
        }
      }
    }
  );

  const allCombobox = () =>
    baseElement.querySelectorAll('.icon.icon-chevron-down');
  const selectQueueOwner = () => {
    fireEvent.mouseDown(allCombobox()[0]);
    userEvent.click(getByText(/Maxel/i));
  };
  const blurQueueOwnerWithoutChange = () => {
    fireEvent.mouseDown(allCombobox()[0]);
    fireEvent.blur(allCombobox()[0]);
  };
  const selectManagerId = () => {
    fireEvent.mouseDown(allCombobox()[1]);
    userEvent.click(getByText(/Anne/i));
  };
  return {
    title: () =>
      getByText('txt_config_manage_adjustments_system_select_profile'),
    selectQueueOwner,
    selectManagerId,
    blurQueueOwnerWithoutChange
  };
};

const mockActionsWorkflowSetup = mockThunkAction(actionsWorkflowSetup);

beforeEach(() => {
  mockActionsWorkflowSetup('getWorkflowMetadata');
});

describe('Test SelectProfile component', () => {
  it('should be render', async () => {
    const { title } = await factory();
    expect(title()).toBeInTheDocument();
  });

  it('should be select queueOwner', async () => {
    jest.useFakeTimers();
    const { selectQueueOwner } = await factory();
    selectQueueOwner();
    jest.runAllTimers();
    expect(mSetQueueOwner).toBeCalled();
  });

  it('should be select queueOwner without any changes', async () => {
    jest.useFakeTimers();
    const { blurQueueOwnerWithoutChange } = await factory();
    blurQueueOwnerWithoutChange();
    jest.runAllTimers();
    expect(mSetQueueOwner).not.toBeCalled();
  });

  it('should be select managerId', async () => {
    jest.useFakeTimers();
    const { selectManagerId } = await factory();
    selectManagerId();
    jest.runAllTimers();
    expect(mSetManagerId).toBeCalled();
  });
});
