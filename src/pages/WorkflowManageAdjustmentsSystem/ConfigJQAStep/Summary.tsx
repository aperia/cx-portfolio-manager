import NoDataFound from 'app/components/NoDataFound';
import { BadgeColorType, TransactionStatus } from 'app/constants/enums';
import { usePagination } from 'app/hooks/usePagination';
import {
  Button,
  ColumnType,
  Grid,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import { isFunction } from 'app/_libraries/_dls/lodash';
import { useSelectElementsForManageAdjustmentSystem } from 'pages/_commons/redux/WorkflowSetup';
import { formatBadge } from 'pages/_commons/Utils/formatGridField';
import Paging from 'pages/_commons/Utils/Paging';
import React, { useEffect, useMemo } from 'react';
import { FormConfigJQA } from '.';
import { RefDataWithDesc } from '../type';
import { Shell } from './types';

const ConfigJQASummary: React.FC<WorkflowSetupSummaryProps<FormConfigJQA>> = ({
  onEditStep,
  formValues,
  stepId,
  selectedStep
}) => {
  const { t } = useTranslation();
  const { queueOwner: queueOwnerOptions } =
    useSelectElementsForManageAdjustmentSystem();

  const selectedQueueOwner = useMemo(
    () =>
      queueOwnerOptions.find(
        q => q.code === formValues?.queueOwner
      ) as RefDataWithDesc,
    [queueOwnerOptions, formValues?.queueOwner]
  );

  const managerId = formValues?.managerId?.code;
  const queueOwner = selectedQueueOwner?.description;

  const handleEdit = () => {
    isFunction(onEditStep) && onEditStep();
  };

  const resetFilter = useMemo(
    () => stepId === selectedStep,
    [stepId, selectedStep]
  );

  return (
    <div className="position-relative">
      <div className="absolute-top-right mt-n26 mr-n8">
        <Button variant="outline-primary" size="sm" onClick={handleEdit}>
          {t('txt_edit')}
        </Button>
      </div>
      <div className="pt-16">
        <ShellTable
          shell={formValues?.alternateAccessShellNames || []}
          resetFilter={resetFilter}
          queueOwner={queueOwner}
          managerId={managerId}
        />
      </div>
    </div>
  );
};

interface ShellSectionProps {
  shell: MagicKeyValue;
  resetFilter: boolean;
  queueOwner?: string;
  managerId?: string;
}
const ShellTable: React.FC<ShellSectionProps> = ({
  shell,
  resetFilter,
  queueOwner,
  managerId
}) => {
  const { t } = useTranslation();

  const renderTitle = (
    <div className="row">
      <div className="col-3">
        <div className="form-group-static mt-0">
          <span className="form-group-static__label">
            {t('txt_queue_owner')}
          </span>
          <span className="form-group-static__text">
            <TruncateText
              resizable
              lines={2}
              ellipsisLessText={t('txt_less')}
              ellipsisMoreText={t('txt_more')}
            >
              {queueOwner}
            </TruncateText>
          </span>
        </div>
      </div>
      <div className="col-3">
        <div className="form-group-static mt-0">
          <span className="form-group-static__label">
            {t('txt_manager_id')}
          </span>
          <span className="form-group-static__text">
            <TruncateText
              resizable
              lines={2}
              ellipsisLessText={t('txt_less')}
              ellipsisMoreText={t('txt_more')}
            >
              {managerId}
            </TruncateText>
          </span>
        </div>
      </div>
    </div>
  );

  const _shells = useMemo(
    () => shell?.filter((f: any) => f.status !== TransactionStatus.NotUpdated),
    [shell]
  );

  const {
    total,
    currentPage,
    currentPageSize,
    gridData: dataView,
    onPageChange,
    onPageSizeChange,
    onResetToDefaultFilter
  } = usePagination(_shells, [], 'shellName');

  useEffect(() => {
    if (!resetFilter) return;

    onResetToDefaultFilter();
  }, [resetFilter, onResetToDefaultFilter]);

  const columns: ColumnType<Shell>[] = useMemo(() => {
    return [
      {
        id: 'shellName',
        Header: t('txt_shell_name'),
        accessor: 'shellName',
        width: 279
      },
      {
        id: 'newShellName',
        Header: t('txt_new_shell_name'),
        accessor: 'newShellName',
        width: 280
      },
      {
        id: 'status',
        Header: t('txt_status'),
        accessor: formatBadge(['status'], {
          colorType: BadgeColorType.ConfigParameterStatus
        }),
        width: 169
      }
    ];
  }, [t]);

  return (
    <>
      {total === 0 && (
        <div className="d-flex flex-column justify-content-center pt-40 mb-32">
          <NoDataFound
            id="merchant-list-summary-NoDataFound"
            title={t('txt_config_processing_merchants_no_update_merchants')}
          />
        </div>
      )}
      {total > 0 && (
        <>
          {renderTitle}
          <div className="d-flex align-items-center justify-content-between"></div>
          <div>
            <div className="mt-16">
              <Grid data={dataView} columns={columns} />
              {total > 10 && (
                <div className="mt-16">
                  <Paging
                    page={currentPage}
                    pageSize={currentPageSize}
                    totalItem={total}
                    onChangePage={onPageChange}
                    onChangePageSize={onPageSizeChange}
                  />
                </div>
              )}
            </div>
          </div>
        </>
      )}
    </>
  );
};

export default ConfigJQASummary;
