import { getWorkflowSetupStepStatus } from 'app/helpers';
import { FormConfigJQA } from '.';
import { ParameterCode } from '../type';

const parseFormValues: WorkflowSetupStepFormDataFunc<FormConfigJQA> = (
  data,
  id
) => {
  const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);
  if (!stepInfo) return;

  const queueProfile = data?.data?.configurations?.queueProfile;
  const now = Date.now();

  return {
    ...getWorkflowSetupStepStatus(stepInfo),
    isValid: !!queueProfile?.queueOwner,
    queueOwner: queueProfile?.queueOwner,
    managerId: queueProfile?.managerId,
    generalInfo: queueProfile?.generalInformation,
    alternateAccessShellNames: queueProfile?.alternateAccessShellNames?.map(
      (shell: any, idx: number) => {
        const shellName = shell.parameters?.find(
          (p: any) => p.name === ParameterCode.ShellName
        );

        return {
          status: shell.status,
          rowId: `${now}${idx}`,
          shellName: shellName?.previousValue,
          newShellName: shellName?.newValue
        };
      }
    )
  };
};

export default parseFormValues;
