import { act, fireEvent, render, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import 'app/utils/_mockComponent/mockUseTranslation';
import { STATUS } from 'pages/WorkflowManageOCSShells/ConfigureParameters/constants';
import * as CommonRedux from 'pages/_commons/redux/Common/select-hooks';
import React, { useRef } from 'react';
import AlternateAccessShellNames, {
  AlternateAccessCompRef,
  generateAccessShellNameGridData
} from './AlternateAccessShellNames';
import { ShellNameModel } from './types';

jest.mock('app/components/HeadingWithSearch', () => ({
  ...jest.requireActual('app/components/HeadingWithSearch'),
  HeadingWithSearch: ({
    searchValue,
    onSearchChange
  }: {
    searchValue: string;
    onSearchChange: (s: string) => void;
  }) => (
    <div>
      <input
        data-testid="heading-with-search"
        value={searchValue}
        onChange={e => {
          onSearchChange(e.target.value);
        }}
      />
    </div>
  )
}));
jest.mock('./CancelShell', () => ({
  ...jest.requireActual('./CancelShell'),
  CancelShell: ({ show }: { show: boolean }) => (
    <div>CancelShell {show && <div data-testid="show-cancel"></div>}</div>
  )
}));
jest.mock('./DeleteShell', () => ({
  ...jest.requireActual('./DeleteShell'),
  DeleteShell: ({ show }: { show: boolean }) => (
    <div>DeleteShell {show && <div data-testid="show-delete"></div>}</div>
  )
}));
jest.mock('./ShellForm', () => ({
  __esModule: true,
  ...jest.requireActual('./ShellForm'),
  default: ({ show }: { show: boolean }) => (
    <>
      <div>ShellForm</div>
      {show && <div data-testid="show-shell-form"></div>}
    </>
  )
}));

const mockUseSelectWindowDimension = jest.spyOn(
  CommonRedux,
  'useSelectWindowDimension'
);
const mockShellNames = [{ shellName: 'John' }];

const mockUseSelectWindowDimensionImplementation = (width: number) => {
  mockUseSelectWindowDimension.mockImplementation(() => ({ width } as any));
};
const mockShellNamesMaxLen = () => {
  const DATA_MAX_LENGTH = 20;
  return Array.from({ length: DATA_MAX_LENGTH }, () => ({ shellName: 'John' }));
};

const FakeComponent = ({
  shellNames,
  stepId,
  selectedStep
}: {
  shellNames?: ShellNameModel[];
  stepId: string;
  selectedStep: string;
}) => {
  const shells = generateAccessShellNameGridData(shellNames!);
  if (shells.length > 4) {
    shells[2].status = STATUS.New;
    shells[3].status = STATUS.MarkedForRemoval;
    shells[4].status = STATUS.Updated;
  }
  const ref = useRef<AlternateAccessCompRef>(null);
  const generateProps = {
    queueOwner: { value: 'John', description: 'John', code: 'John' },
    data: shells,
    stepId,
    selectedStep,
    onAdd: jest.fn(),
    onUpdate: jest.fn(),
    onDelete: jest.fn(),
    onCancel: jest.fn()
  };
  return (
    <>
      <button
        onClick={() => {
          ref.current?.resetAndNavigateTo('1');
        }}
      >
        navigate
      </button>
      <AlternateAccessShellNames ref={ref} {...generateProps} />
    </>
  );
};

const factory = (
  shellNames?: ShellNameModel[],
  stepIdEqualSelected = false
) => {
  const { getByText, getByTestId, getAllByRole, getAllByText } = render(
    <FakeComponent
      shellNames={shellNames}
      stepId={stepIdEqualSelected ? 'step1' : 'step2'}
      selectedStep="step1"
    />
  );

  const openAddShell = () => {
    act(() => {
      fireEvent.click(getByText('txt_add_shell'));
    });
    act(() => {
      fireEvent.click(getByText('navigate'));
    });
  };

  const editShell = () => {
    act(() => {
      fireEvent.click(getAllByText('txt_edit')[0]);
    });
  };

  const cancelShell = () => {
    act(() => {
      fireEvent.click(getAllByText('txt_cancel')[0]);
    });
  };

  const deleteShell = () => {
    act(() => {
      fireEvent.click(getAllByText('txt_remove')[0]);
    });
  };
  const headingWithSearch = () => getByTestId('heading-with-search');
  const enterSearch = () => {
    act(() => {
      fireEvent.change(headingWithSearch(), { target: { value: 'not match' } });
    });
  };

  const navigateTo = () => {
    act(() => {
      fireEvent.click(getByText('navigate'));
    });
  };

  const checkAll = async () => {
    const checkbox = getAllByRole('checkbox');
    await waitFor(() => {
      userEvent.click(checkbox[0]);
    });
  };

  const bulkDelete = async () => {
    await checkAll();

    fireEvent.click(getByText('txt_bulk_remove'));
  };
  return {
    openAddShell,
    editShell,
    enterSearch,
    headingWithSearch,
    shellForm: () => getByText('ShellForm'),
    nodata: () => getByText('txt_clear_and_reset'),
    navigateTo,
    bulkDelete,
    showDeleteModal: () => getByTestId('show-delete'),
    showShellFormModal: () => getByTestId('show-shell-form'),
    showCancelModal: () => getByTestId('show-cancel'),
    cancelShell,
    deleteShell,
    checkAll
  };
};

describe('test AlternateAccessShellNames component', () => {
  beforeEach(() => {
    mockUseSelectWindowDimensionImplementation(1400);
  });
  afterEach(() => {
    mockUseSelectWindowDimension.mockClear();
  });

  it('should be render', () => {
    mockUseSelectWindowDimensionImplementation(900);
    const { headingWithSearch } = factory();
    expect(headingWithSearch()).toBeInTheDocument();
  });

  it('should be render with max data', () => {
    const { headingWithSearch } = factory(mockShellNamesMaxLen());
    expect(headingWithSearch()).toBeInTheDocument();
  });

  it('should be show add form', async () => {
    const { checkAll, openAddShell, shellForm } = factory(mockShellNames);
    await checkAll();
    await checkAll();
    openAddShell();
    expect(shellForm()).toBeInTheDocument();
  });

  it('should be render no data when search is not match', () => {
    const { enterSearch, nodata } = factory(mockShellNames, true);
    enterSearch();
    expect(nodata()).toBeInTheDocument();
  });

  it('should be bulk delete', async () => {
    const { bulkDelete, showDeleteModal } = factory(mockShellNamesMaxLen());
    await bulkDelete();
    expect(showDeleteModal()).toBeInTheDocument();
  });

  it('should be edit', () => {
    const { editShell, showShellFormModal } = factory(mockShellNamesMaxLen());
    editShell();
    expect(showShellFormModal()).toBeInTheDocument();
  });

  it('should be delete', () => {
    const { deleteShell, showDeleteModal } = factory(mockShellNamesMaxLen());
    deleteShell();
    expect(showDeleteModal()).toBeInTheDocument();
  });

  it('should be cancel', () => {
    const { cancelShell, showCancelModal } = factory(mockShellNamesMaxLen());
    cancelShell();
    expect(showCancelModal()).toBeInTheDocument();
  });
});
