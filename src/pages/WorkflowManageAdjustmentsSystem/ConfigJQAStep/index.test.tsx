import { act, fireEvent, RenderResult, waitFor } from '@testing-library/react';
import { mockThunkAction, renderWithMockStore } from 'app/utils';
import 'app/_libraries/_dls/test-utils/mocks/mockCanvas.ts';
import { STATUS } from 'pages/WorkflowManageOCSShells/ConfigureParameters/constants';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import React, { useState } from 'react';
import { RefDataWithDesc } from '../type';
import { AlternateAccessShellNamesProps } from './AlternateAccessShellNames';
import ConfigJQAStep, { FormConfigJQA } from './index';
import { SelectProfileProps } from './SelectProfile';

jest.mock('./SelectProfile', () => ({
  __esModule: true,
  ...jest.requireActual('./SelectProfile'),
  default: (props: SelectProfileProps) => {
    const handleSetQueueOwner = (queueOwner?: RefDataWithDesc) =>
      props.setQueueOwner(queueOwner!);
    const handleSetManagerId = () =>
      props.setManagerId({ text: 'Andy', code: 'Andy' });
    return (
      <>
        <div>{props.queueOwner?.code}</div>
        <div>txt_config_manage_adjustments_system_select_profile</div>
        <button
          onClick={() =>
            handleSetQueueOwner({
              text: 'Bob',
              code: 'Bob',
              description: 'Bob'
            })
          }
        >
          setQueueOwner
        </button>
        <button
          onClick={() =>
            handleSetQueueOwner({
              text: 'Joe',
              code: 'Joe',
              description: 'Joe'
            })
          }
        >
          setOtherQueueOwner
        </button>
        <button onClick={() => handleSetQueueOwner(undefined)}>
          clearQueueOwner
        </button>
        <button onClick={handleSetManagerId}>setManagerId</button>
      </>
    );
  }
}));

jest.mock('./AlternateAccessShellNames', () => ({
  __esModule: true,
  ...jest.requireActual('./AlternateAccessShellNames'),
  default: (props: AlternateAccessShellNamesProps) => {
    const mShell = {
      shellName: 'John',
      newShellName: 'Bob',
      rowId: '1',
      status: 'Updated' as STATUS
    };

    const mRefData = { value: 'John', code: 'John', description: 'John' };

    return (
      <>
        <div>AlternateAccessShellNames</div>
        <button data-testid="on-add" onClick={() => props.onAdd(mRefData)}>
          onAdd
        </button>
        <button
          data-testid="on-update"
          onClick={() => props.onUpdate(mShell, mRefData)}
        >
          onUpdate
        </button>
        <button
          data-testid="on-update-new"
          onClick={() =>
            props.onUpdate({ ...mShell, status: 'New' as STATUS }, mRefData)
          }
        >
          onUpdate
        </button>
        <button
          data-testid="on-not-updated"
          onClick={() =>
            props.onUpdate({ ...mShell, shellName: 'Bob' as STATUS }, mRefData)
          }
        >
          onUpdate
        </button>
        <button data-testid="on-delete" onClick={() => props.onDelete(['1'])}>
          onDelete
        </button>
        <button data-testid="on-cancel" onClick={() => props.onCancel(mShell)}>
          onCancel
        </button>
      </>
    );
  }
}));

jest.mock('../GeneralInformation', () => ({
  __esModule: true,
  ...jest.requireActual('../GeneralInformation'),
  default: () => <div>GeneralInformation</div>
}));

jest.mock('./UnsavedChange', () => ({
  __esModule: true,
  ...jest.requireActual('./UnsavedChange'),
  UnsavedChange: ({
    show,
    onContinue,
    onSubmit
  }: {
    show: boolean;
    onContinue: () => void;
    onSubmit: () => void;
  }) => (
    <div>
      {show && (
        <div data-testid="show-unsaved-change">
          <button onClick={onContinue} data-testid="on-continue"></button>
          <button onClick={onSubmit} data-testid="on-submit"></button>
        </div>
      )}
    </div>
  )
}));

const FakeComponent = (props: any) => {
  const [compProps, setComProps] = useState(props);
  const handleChangeProps = () => {
    setComProps((prev: any) => ({
      ...prev,
      formValues: {
        ...prev.formValues,
        alternateAccessShellNames: [
          {
            shellName: 'John',
            newShellName: 'Bob',
            rowId: '1',
            status: STATUS.Updated
          }
        ]
      }
    }));
  };
  return (
    <>
      <button data-testid="change-props" onClick={handleChangeProps}></button>
      <ConfigJQAStep {...compProps} />
    </>
  );
};

const renderComponent = async (
  props: any,
  workflowSetup?: any
): Promise<RenderResult> => {
  return await renderWithMockStore(<FakeComponent {...props} />, {
    initialState: { workflowSetup }
  });
};

describe('ManageAdjustmentsSystemWorkflow > ConfigJQAStep > Index', () => {
  const mockActionsWorkflowSetup = mockThunkAction(actionsWorkflowSetup);
  it('Should render ', async () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: {
        queueOwner: { value: 'John', code: 'John', description: 'John' }
      } as never,
      setFormValues: mockFn
    } as unknown as WorkflowSetupProps<FormConfigJQA>;

    const wrapper = await renderComponent(props);
    expect(
      wrapper.queryByText('txt_config_manage_adjustments_system_select_profile')
    ).toBeInTheDocument();
  });

  it('should be select Queue Owner', async () => {
    jest.useFakeTimers();
    mockActionsWorkflowSetup('getWorkflowMetadata');
    jest
      .spyOn(actionsWorkflowSetup, 'getManageAdjustmentsSystemQueueProfile')
      .mockImplementation(
        () =>
          ({
            type: 'workflowSetup/getManageAdjustmentsSystemQueueProfile',
            unwrap: () => ({
              generalInformation: {
                queueOwner: 'Fulloway',
                queueOwnerId: '23204775',
                managerId: '97960872',
                terminalGroupId: '98109891'
              },
              alternateAccessShellNames: [
                { shellName: 'Jones' },
                { shellName: 'Darela' },
                { shellName: 'Dude' },
                { shellName: 'Fulloway' },
                { shellName: 'Chandravadan' }
              ]
            })
          } as never)
      );
    const mockFn = (values: any) => {};
    const props = {
      formValues: {},
      setFormValues: mockFn
    } as WorkflowSetupProps<FormConfigJQA>;

    const wrapper = await renderComponent(props, {
      elementMetadata: {
        loading: false,
        elements: [
          {
            id: '331',
            name: 'queue.owner',
            options: [
              { value: 'John', code: 'John', description: 'John' },
              { value: 'Maxel', code: 'Maxel', description: 'Maxel' }
            ]
          },
          {
            id: '331',
            name: 'manager.id',
            options: [
              { value: 'Robinson', code: 'Robinson', description: 'Robinson' },
              { value: 'Anne', code: 'Anne', description: 'Anne' }
            ]
          }
        ]
      },
      queueProfile: {}
    });

    const queueOwner = wrapper.getByText('setQueueOwner');
    await waitFor(() => {
      fireEvent.click(queueOwner);
    });

    await waitFor(() => {
      fireEvent.click(wrapper.getByText('clearQueueOwner'));
    });

    jest.runAllTimers();

    const managerId = wrapper.getByText('setManagerId');
    fireEvent.click(managerId);
    jest.runAllTimers();
    expect(
      wrapper.queryByText('txt_config_manage_adjustments_system_select_profile')
    ).toBeInTheDocument();
  });

  it('should be select Queue Owner with error', async () => {
    jest.useFakeTimers();
    mockActionsWorkflowSetup('getWorkflowMetadata');
    jest
      .spyOn(actionsWorkflowSetup, 'getManageAdjustmentsSystemQueueProfile')
      .mockImplementation(
        () =>
          ({
            type: 'workflowSetup/getManageAdjustmentsSystemQueueProfile'
          } as never)
      );
    const mockFn = (values: any) => {};
    const props = {
      formValues: {},
      setFormValues: mockFn
    } as WorkflowSetupProps<FormConfigJQA>;

    const wrapper = await renderComponent(props, {
      elementMetadata: {
        loading: false,
        elements: [
          {
            id: '331',
            name: 'queue.owner',
            options: [
              { value: 'John', code: 'John', description: 'John' },
              { value: 'Maxel', code: 'Maxel', description: 'Maxel' }
            ]
          },
          {
            id: '331',
            name: 'manager.id',
            options: [
              { value: 'Robinson', code: 'Robinson', description: 'Robinson' },
              { value: 'Anne', code: 'Anne', description: 'Anne' }
            ]
          }
        ]
      },
      queueProfile: {}
    });

    const queueOwner = wrapper.getByText('setQueueOwner');
    await waitFor(() => {
      fireEvent.click(queueOwner);
    });
    jest.runAllTimers();

    expect(
      wrapper.queryByText('txt_config_manage_adjustments_system_select_profile')
    ).toBeInTheDocument();
  });

  it('should be set Queue Owner undefined', async () => {
    jest.useFakeTimers();
    mockActionsWorkflowSetup('getWorkflowMetadata');

    const mockFn = (values: any) => {};
    const props = {
      formValues: {},
      setFormValues: mockFn
    } as WorkflowSetupProps<FormConfigJQA>;

    const wrapper = await renderComponent(props, {
      elementMetadata: {
        loading: false,
        elements: [
          {
            id: '331',
            name: 'queue.owner',
            options: [
              { value: 'John', code: 'John', description: 'John' },
              { value: 'Maxel', code: 'Maxel', description: 'Maxel' }
            ]
          },
          {
            id: '331',
            name: 'manager.id',
            options: [
              { value: 'Robinson', code: 'Robinson', description: 'Robinson' },
              { value: 'Anne', code: 'Anne', description: 'Anne' }
            ]
          }
        ]
      },
      queueProfile: {}
    });

    const queueOwner = wrapper.getByText('clearQueueOwner');
    await waitFor(() => {
      fireEvent.click(queueOwner);
    });

    jest.runAllTimers();

    expect(
      wrapper.queryByText('txt_config_manage_adjustments_system_select_profile')
    ).toBeInTheDocument();
  });

  it('should be open unsaved change', async () => {
    jest.useFakeTimers();
    mockActionsWorkflowSetup('getWorkflowMetadata');
    jest
      .spyOn(actionsWorkflowSetup, 'getManageAdjustmentsSystemQueueProfile')
      .mockImplementation(
        () =>
          ({
            type: 'workflowSetup/getManageAdjustmentsSystemQueueProfile',
            unwrap: () => ({
              generalInformation: {
                queueOwner: 'Fulloway',
                queueOwnerId: '23204775',
                managerId: '97960872',
                terminalGroupId: '98109891'
              },
              alternateAccessShellNames: [
                { shellName: 'Jones' },
                { shellName: 'Darela' },
                { shellName: 'Dude' },
                { shellName: 'Fulloway' },
                { shellName: 'Chandravadan' }
              ]
            })
          } as never)
      );
    const mockFn = (values: any) => {};
    const props = {
      formValues: {
        queueOwner: { value: 'John', description: 'John', code: 'John' }
      } as never,
      setFormValues: mockFn
    } as unknown as WorkflowSetupProps<FormConfigJQA>;

    const wrapper = await renderComponent(props, {
      elementMetadata: {
        loading: false,
        elements: [
          {
            id: '331',
            name: 'queue.owner',
            options: [
              { value: 'John', code: 'John', description: 'John' },
              { value: 'Maxel', code: 'Maxel', description: 'Maxel' }
            ]
          },
          {
            id: '331',
            name: 'manager.id',
            options: [
              { value: 'Robinson', code: 'Robinson', description: 'Robinson' },
              { value: 'Anne', code: 'Anne', description: 'Anne' }
            ]
          }
        ]
      },
      queueProfile: {}
    });

    await waitFor(() => {
      fireEvent.click(wrapper.getByText('setQueueOwner'));
    });
    jest.runAllTimers();
    await waitFor(() => {
      fireEvent.click(wrapper.getByTestId('change-props'));
    });

    await waitFor(() => {
      fireEvent.click(wrapper.getByText('setOtherQueueOwner'));
    });
    jest.runAllTimers();

    act(() => {
      fireEvent.click(wrapper.getByTestId('on-continue'));
    });
    await waitFor(() => {
      fireEvent.click(wrapper.getByText('setOtherQueueOwner'));
    });
    fireEvent.click(wrapper.getByTestId('on-submit'));
    expect(
      wrapper.queryByText('txt_config_manage_adjustments_system_select_profile')
    ).toBeInTheDocument();
  });

  it('should be continue working', async () => {
    jest.useFakeTimers();
    mockActionsWorkflowSetup('getWorkflowMetadata');
    jest
      .spyOn(actionsWorkflowSetup, 'getManageAdjustmentsSystemQueueProfile')
      .mockImplementation(
        () =>
          ({
            type: 'workflowSetup/getManageAdjustmentsSystemQueueProfile',
            unwrap: () => ({
              generalInformation: {
                queueOwner: 'Fulloway',
                queueOwnerId: '23204775',
                managerId: '97960872',
                terminalGroupId: '98109891'
              },
              alternateAccessShellNames: [
                { shellName: 'Jones' },
                { shellName: 'Darela' },
                { shellName: 'Dude' },
                { shellName: 'Fulloway' },
                { shellName: 'Chandravadan' }
              ]
            })
          } as never)
      );
    const mockFn = (values: any) => {};
    const props = {
      formValues: {
        queueOwner: { value: 'John', description: 'John', code: 'John' },
        alternateAccessShellNames: [
          {
            shellName: 'John',
            newShellName: 'Bob',
            rowId: '1',
            status: STATUS.Updated
          }
        ]
      } as never,
      setFormValues: mockFn
    } as unknown as WorkflowSetupProps<FormConfigJQA>;

    const wrapper = await renderComponent(props, {
      elementMetadata: {
        loading: false,
        elements: [
          {
            id: '331',
            name: 'queue.owner',
            options: [
              { value: 'John', code: 'John', description: 'John' },
              { value: 'Maxel', code: 'Maxel', description: 'Maxel' }
            ]
          },
          {
            id: '331',
            name: 'manager.id',
            options: [
              { value: 'Robinson', code: 'Robinson', description: 'Robinson' },
              { value: 'Anne', code: 'Anne', description: 'Anne' }
            ]
          }
        ]
      },
      queueProfile: {}
    });

    await waitFor(() => {
      fireEvent.click(wrapper.getByText('setQueueOwner'));
    });

    act(() => {
      fireEvent.click(wrapper.getByTestId('on-continue'));
    });
    expect(
      wrapper.queryByText('txt_config_manage_adjustments_system_select_profile')
    ).toBeInTheDocument();
  });

  it('should be add shell', async () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: {
        queueOwner: { value: 'John', code: 'John', description: 'John' },
        alternateAccessShellNames: []
      } as never,
      setFormValues: mockFn
    } as unknown as WorkflowSetupProps<FormConfigJQA>;
    jest.useFakeTimers();
    const wrapper = await renderComponent(props);
    fireEvent.click(wrapper.getByTestId('on-add'));
    jest.runAllTimers();
    expect(
      wrapper.queryByText('txt_config_manage_adjustments_system_select_profile')
    ).toBeInTheDocument();
  });

  it('should be cancel shell', async () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: {
        queueOwner: { value: 'John', code: 'John', description: 'John' },
        alternateAccessShellNames: [
          {
            shellName: 'John',
            newShellName: 'Bob',
            rowId: '1',
            status: 'Updated' as STATUS
          }
        ]
      } as never,
      setFormValues: mockFn
    } as unknown as WorkflowSetupProps<FormConfigJQA>;
    jest.useFakeTimers();
    const wrapper = await renderComponent(props);
    fireEvent.click(wrapper.getByTestId('on-cancel'));
    jest.runAllTimers();
    expect(
      wrapper.queryByText('txt_config_manage_adjustments_system_select_profile')
    ).toBeInTheDocument();
  });

  it('should be delete shell new', async () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: {
        queueOwner: { value: 'John', code: 'John', description: 'John' },
        alternateAccessShellNames: [
          {
            shellName: 'John',
            newShellName: 'Bob',
            rowId: '1',
            status: 'New' as STATUS
          }
        ]
      } as never,
      setFormValues: mockFn
    } as unknown as WorkflowSetupProps<FormConfigJQA>;
    jest.useFakeTimers();
    const wrapper = await renderComponent(props);
    fireEvent.click(wrapper.getByTestId('on-delete'));
    jest.runAllTimers();
    expect(
      wrapper.queryByText('txt_config_manage_adjustments_system_select_profile')
    ).toBeInTheDocument();
  });

  it('should be delete shell not updated', async () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: {
        queueOwner: { value: 'John', code: 'John', description: 'John' },
        alternateAccessShellNames: [
          {
            shellName: 'John',
            newShellName: 'Bob',
            rowId: '1',
            status: 'Not Updated' as STATUS
          }
        ]
      } as never,
      setFormValues: mockFn
    } as unknown as WorkflowSetupProps<FormConfigJQA>;
    jest.useFakeTimers();
    const wrapper = await renderComponent(props);
    fireEvent.click(wrapper.getByTestId('on-delete'));
    jest.runAllTimers();
    expect(
      wrapper.queryByText('txt_config_manage_adjustments_system_select_profile')
    ).toBeInTheDocument();
  });

  it('should be update shell not updated', async () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: {
        queueOwner: { value: 'John', code: 'John', description: 'John' },
        alternateAccessShellNames: [
          {
            shellName: 'John',
            newShellName: 'Bob',
            rowId: '1',
            status: 'Not Updated' as STATUS
          }
        ]
      } as never,
      setFormValues: mockFn
    } as unknown as WorkflowSetupProps<FormConfigJQA>;
    jest.useFakeTimers();
    const wrapper = await renderComponent(props);
    fireEvent.click(wrapper.getByTestId('on-update'));
    jest.runAllTimers();
    expect(
      wrapper.queryByText('txt_config_manage_adjustments_system_select_profile')
    ).toBeInTheDocument();
  });

  it('should be update shell new', async () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: {
        queueOwner: { value: 'John', code: 'John', description: 'John' },
        alternateAccessShellNames: [
          {
            shellName: 'John',
            newShellName: 'Bob',
            rowId: '1',
            status: 'New' as STATUS
          }
        ]
      } as never,
      setFormValues: mockFn
    } as unknown as WorkflowSetupProps<FormConfigJQA>;
    jest.useFakeTimers();
    const wrapper = await renderComponent(props);
    fireEvent.click(wrapper.getByTestId('on-update-new'));
    jest.runAllTimers();
    expect(
      wrapper.queryByText('txt_config_manage_adjustments_system_select_profile')
    ).toBeInTheDocument();
  });

  it('should be update shell not updated', async () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: {
        queueOwner: { value: 'John', code: 'John', description: 'John' },
        alternateAccessShellNames: [
          {
            shellName: 'John',
            newShellName: 'Bob',
            rowId: '1',
            status: 'New' as STATUS
          }
        ]
      } as never,
      setFormValues: mockFn
    } as unknown as WorkflowSetupProps<FormConfigJQA>;
    jest.useFakeTimers();
    const wrapper = await renderComponent(props);
    fireEvent.click(wrapper.getByTestId('on-not-updated'));
    jest.runAllTimers();
    expect(
      wrapper.queryByText('txt_config_manage_adjustments_system_select_profile')
    ).toBeInTheDocument();
  });

  it('should render stuck message', async () => {
    const mockFn = (values: any) => {};
    const props = {
      isStuck: true,
      formValues: {
        queueOwner: { value: 'John', code: 'John', description: 'John' },
        alternateAccessShellNames: [
          {
            shellName: 'John',
            newShellName: 'Bob',
            rowId: '1',
            status: 'New' as STATUS
          }
        ]
      } as never,
      setFormValues: mockFn
    } as unknown as WorkflowSetupProps<FormConfigJQA>;
    jest.useFakeTimers();
    const wrapper = await renderComponent(props);
    expect(
      wrapper.queryByText('txt_step_stuck_move_forward_message')
    ).toBeInTheDocument();
  });
});
