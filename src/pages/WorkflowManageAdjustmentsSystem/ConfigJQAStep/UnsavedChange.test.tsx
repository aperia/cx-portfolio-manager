import { fireEvent, render } from '@testing-library/react';
import 'app/utils/_mockComponent/mockModalRegistry';
import React from 'react';
import { UnsavedChange, useUnsavedChange } from './UnsavedChange';

const mockCallback = jest.fn();
const mockCancelCallback = jest.fn();

const FakeComponent = () => {
  const { openUnsavedConfirm, ...unsavedChangeProps } = useUnsavedChange();
  return (
    <>
      <button
        onClick={() => {
          openUnsavedConfirm(mockCallback, mockCancelCallback);
        }}
      >
        Open
      </button>
      <UnsavedChange id="unsaved" {...unsavedChangeProps} />
    </>
  );
};

const factory = () => {
  const { getByText, queryByText } = render(<FakeComponent />);
  const openModal = () => {
    const openElement = getByText('Open');
    fireEvent.click(openElement);
  };

  const onClose = () => {
    const cancelButton = getByText('txt_continue_working');
    fireEvent.click(cancelButton);
  };

  const onSubmit = () => {
    const submitButton = getByText('txt_discard_data');
    fireEvent.click(submitButton);
  };

  const title = () => queryByText('txt_unsaved_data');
  return {
    openModal,
    title,
    onClose,
    onSubmit
  };
};

describe('test cancel shell component', () => {
  it('should be render modal', async () => {
    const { openModal, title } = factory();
    openModal();
    expect(title()).toBeInTheDocument();
  });

  it('should be close', async () => {
    const { openModal, onClose, title } = factory();
    openModal();
    onClose();
    expect(title()).not.toBeInTheDocument();
    expect(mockCancelCallback).toBeCalled();
  });

  it('should be submit', async () => {
    const { openModal, onSubmit } = factory();
    openModal();
    onSubmit();
    expect(mockCallback).toBeCalled();
  });
});
