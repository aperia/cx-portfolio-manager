import { STATUS } from 'pages/WorkflowManageOCSShells/ConfigureParameters/constants';

export enum ParameterCode {
  ShellName = 'shellName',
  NewShellName = 'newShellName',
  Status = 'status'
}

export enum MetaData {
  QueueOwner = 'queue.owner',
  ManagerId = 'manager.id'
}

export interface ShellNameModel {
  shellName: string;
}

export type Shell = {
  rowId: string;
  shellName: string;
  newShellName: string;
  status: STATUS;
};

export type ShellFormType = 'ADD' | 'EDIT' | undefined;
