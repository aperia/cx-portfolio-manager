import { HeadingWithSearch } from 'app/components/HeadingWithSearch';
import NoDataFound from 'app/components/NoDataFound';
import { matchSearchValue } from 'app/helpers';
import { Grid, useTranslation } from 'app/_libraries/_dls';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React, { useMemo, useState } from 'react';
import {
  GeneralInformationFactory,
  GeneralInformationItem,
  GeneralInformationModel,
  ShellGeneralInformationFactory,
  ShellGeneralInformationModel
} from './type';

export interface GeneralInformationProps {
  data: GeneralInformationItem[];
  searchValue: string;
  onSearchChange: (s: string) => void;
  preview?: boolean;
}

const generalInfoSearchDesc = [
  'txt_business_name',
  'txt_green_screen_name',
  'txt_more_info'
];

export interface GeneralInfoGridProps {
  data: GeneralInformationItem[];
}

const GeneralInfoGrid: React.FC<GeneralInfoGridProps> = ({ data }) => {
  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();

  const columns = useMemo(
    () => [
      {
        id: 'fieldName',
        Header: t('txt_business_name'),
        accessor: 'fieldName',
        width: width < 1280 ? 234 : undefined
      },
      {
        id: 'greenScreenName',
        Header: t('txt_green_screen_name'),
        accessor: 'greenScreenName',
        width: width < 1280 ? 236 : undefined
      },
      {
        id: 'value',
        Header: t('txt_value'),
        accessor: 'value',
        width: width < 1280 ? 260 : 460
      },
      {
        id: 'moreInfo',
        Header: t('txt_more_info'),
        className: 'text-center',
        accessor: viewMoreInfo(['moreInfo'], t),
        width: 106
      }
    ],
    [t, width]
  );

  return (
    <Grid className="grid-has-tooltip center" columns={columns} data={data} />
  );
};

const GeneralInformation: React.FC<GeneralInformationProps> = ({
  data,
  searchValue,
  onSearchChange,
  preview = false
}) => {
  const { t } = useTranslation();
  const classNames = preview ? 'mt-24' : 'mt-24 pt-24 border-top';

  const handleClearFilter = () => {
    onSearchChange('');
  };

  const gridData = useMemo(() => {
    return data.filter(item =>
      matchSearchValue(
        [item['fieldName'], item['greenScreenName'], item['moreInfo']],
        searchValue
      )
    );
  }, [data, searchValue]);

  return (
    <div className={classNames}>
      <HeadingWithSearch
        preview={preview}
        txtTitle="txt_general_information"
        searchValue={searchValue}
        onSearchChange={onSearchChange}
        searchDescription={generalInfoSearchDesc}
        onClearSearch={handleClearFilter}
        isDataEmpty={isEmpty(gridData)}
      />

      {isEmpty(data) && (
        <NoDataFound
          id="newMethod_notfound"
          title={t('txt_no_results_found')}
          className="mt-40"
        />
      )}
      {!isEmpty(data) && isEmpty(gridData) && searchValue && (
        <div className="d-flex flex-column justify-content-center mt-40 mb-24">
          <NoDataFound
            id="newMethod_notfound"
            hasSearch
            title={t('txt_no_results_found')}
            linkTitle={t('txt_clear_and_reset')}
            onLinkClicked={handleClearFilter}
          />
        </div>
      )}
      {!isEmpty(gridData) && (
        <div className="mt-16">
          <GeneralInfoGrid data={gridData} />
        </div>
      )}
    </div>
  );
};

const useGeneralInformation = (props?: GeneralInformationModel) => {
  const [searchValue, setSearchValue] = useState('');
  const data = useMemo(
    () =>
      props
        ? Object.entries(props).map(([ky, val]) => ({
            ...GeneralInformationFactory[ky as keyof GeneralInformationModel],
            value: val
          }))
        : [],
    [props]
  );

  return { searchValue, onSearchChange: setSearchValue, data };
};

const useShellGeneralInformation = (props?: ShellGeneralInformationModel) => {
  const [searchValue, setSearchValue] = useState('');
  const data = useMemo(
    () =>
      props
        ? Object.entries(props).map(([ky, val]) => ({
            ...ShellGeneralInformationFactory[
              ky as keyof ShellGeneralInformationModel
            ],
            value: val
          }))
        : [],
    [props]
  );

  return { searchValue, onSearchChange: setSearchValue, data };
};
export { useGeneralInformation, useShellGeneralInformation, GeneralInfoGrid };
export default GeneralInformation;
