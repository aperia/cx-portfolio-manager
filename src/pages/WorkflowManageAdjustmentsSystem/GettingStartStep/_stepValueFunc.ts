import { FormGettingStart } from '.';

const stepValueFunc: WorkflowSetupStepValueFunc<FormGettingStart> = ({
  stepsForm: formValues,
  t
}) => {
  const { jfs, flsAdj, jqa, fdsa } = formValues || {};
  const isSelectedAll = jfs && flsAdj && jqa && fdsa;

  const options = [];
  jfs && options.push(t('txt_config_manage_adjustments_system_jfs'));
  flsAdj && options.push(t('txt_config_manage_adjustments_system_flsAdj'));
  jqa && options.push(t('txt_config_manage_adjustments_system_jqa'));
  fdsa && options.push(t('txt_config_manage_adjustments_system_fdsa'));

  return isSelectedAll
    ? t('txt_all_transactions_selected')
    : options.join(', ');
};

export default stepValueFunc;
