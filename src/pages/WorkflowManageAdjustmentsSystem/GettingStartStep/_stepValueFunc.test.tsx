import { FormGettingStart } from '.';
import stepValueFunc from './_stepValueFunc';

describe('ManageAdjustmentsSystemWorkflow > GettingStartStep > stepValueFunc', () => {
  const t = jest.fn().mockImplementation(v => v);
  beforeEach(() => {
    t.mockImplementation(v => v);
  });

  it('should return jfs text', () => {
    const stepsForm: FormGettingStart = { jfs: true };

    const response = stepValueFunc({ stepsForm, t });
    expect(response).toEqual('txt_config_manage_adjustments_system_jfs');
  });

  it('should return flsAdj text', () => {
    const stepsForm: FormGettingStart = { flsAdj: true };

    const response = stepValueFunc({ stepsForm, t });
    expect(response).toEqual('txt_config_manage_adjustments_system_flsAdj');
  });

  it('should return jqa text', () => {
    const stepsForm: FormGettingStart = { jqa: true };

    const response = stepValueFunc({ stepsForm, t });
    expect(response).toEqual('txt_config_manage_adjustments_system_jqa');
  });

  it('should return fdsa text', () => {
    const stepsForm: FormGettingStart = { fdsa: true };

    const response = stepValueFunc({ stepsForm, t });
    expect(response).toEqual('txt_config_manage_adjustments_system_fdsa');
  });

  it('should return all transaction text', () => {
    const stepsForm: FormGettingStart = {
      jfs: true,
      flsAdj: true,
      jqa: true,
      fdsa: true
    };

    const response = stepValueFunc({ stepsForm, t });
    expect(response).toEqual('txt_all_transactions_selected');
  });

  it('should return empty', () => {
    const stepsForm: any = undefined;

    const response = stepValueFunc({ stepsForm, t });
    expect(response).toEqual('');
  });
});
