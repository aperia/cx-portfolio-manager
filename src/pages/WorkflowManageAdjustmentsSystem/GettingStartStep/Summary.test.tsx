import { fireEvent, render, RenderResult } from '@testing-library/react';
import React from 'react';
import { FormGettingStart } from '.';
import GettingStartSummary from './Summary';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <GettingStartSummary {...props} />
    </div>
  );
};

describe('ManageAdjustmentsSystemWorkflow > GettingStartStep > Summary', () => {
  it('Should render summary contains txt_config_manage_adjustments_system_fdsa', () => {
    const wrapper = renderComponent({
      formValues: {
        fdsa: true
      }
    } as WorkflowSetupSummaryProps<FormGettingStart>);

    expect(
      wrapper.getByText('txt_config_manage_adjustments_system_fdsa')
    ).toBeInTheDocument();
  });

  it('Should click on edit', () => {
    const mockFn = jest.fn();
    const wrapper = renderComponent({
      formValues: {
        fdsa: true
      },
      onEditStep: mockFn
    } as WorkflowSetupSummaryProps<FormGettingStart>);

    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(mockFn).toHaveBeenCalled();
  });
});
