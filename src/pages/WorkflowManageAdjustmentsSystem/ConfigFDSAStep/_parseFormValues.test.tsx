import { ParameterMetadata } from './type';
import parseFormValues from './_parseFormValues';

describe('ManageAdjustmentsSystemWorkflow > ConfigFDSAStep > parseFormValues', () => {
  const mockDispatch = jest.fn();

  it('Should return undefined', () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '2'
            }
          ] as any
        }
      } as WorkflowSetupInstance,
      id: '1'
    };

    const response = parseFormValues(props.data, props.id, mockDispatch);
    expect(response).toEqual(undefined);
  });

  it('Should return empty', () => {
    const props = {
      data: {
        data: {
          configurations: {},
          workflowSetupData: [
            {
              id: '1'
            }
          ] as any
        }
      } as unknown as WorkflowSetupInstance,
      id: '1'
    };

    const response = parseFormValues(props.data, props.id, mockDispatch);
    expect(response).toEqual({
      isPass: false,
      isSelected: false,
      isValid: false,
      shells: []
    });
  });

  it('Should return data', () => {
    const props = {
      data: {
        data: {
          configurations: {
            shellList: [
              {
                parameters: [
                  { name: ParameterMetadata.Level },
                  { name: ParameterMetadata.SupervisorShell }
                ]
              }
            ]
          },
          workflowSetupData: [
            {
              id: '1'
            }
          ] as any
        }
      } as unknown as WorkflowSetupInstance,
      id: '1'
    };

    const response = parseFormValues(props.data, props.id, mockDispatch);
    expect(response).toEqual(
      expect.objectContaining({
        isPass: false,
        isSelected: false,
        isValid: true
      })
    );
  });
});
