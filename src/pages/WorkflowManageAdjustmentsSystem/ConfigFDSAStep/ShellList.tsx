import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import { BadgeColorType } from 'app/constants/enums';
import { classnames, getSelectedRecord, isDevice } from 'app/helpers';
import { useCheckAllPagination } from 'app/hooks';
import { usePagination } from 'app/hooks/usePagination';
import { IShell } from 'app/types';
import {
  Button,
  ColumnType,
  Grid,
  Icon,
  ToggleButtonConfig,
  Tooltip,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty, isUndefined, orderBy } from 'app/_libraries/_dls/lodash';
import { STATUS } from 'pages/WorkflowManageOCSShells/ConfigureParameters/constants';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import { useSelectMetadataForManageOCSShells } from 'pages/_commons/redux/WorkflowSetup';
import ClearAndResetButton from 'pages/_commons/Utils/ClearAndResetButton';
import { formatBadge } from 'pages/_commons/Utils/formatGridField';
import Paging from 'pages/_commons/Utils/Paging';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { FormConfigFDSA } from '.';
import CancelRecordsModal from './CancelRecordsModal';
import EditShellModal from './EditShellModal';
import ShellSubGrid from './ShellSubGrid';
import { ParameterCode, ParameterMetadata } from './type';

export interface ShellListProps {}

const ShellList: React.FC<WorkflowSetupProps<FormConfigFDSA> & ShellListProps> =
  props => {
    const { t } = useTranslation();
    const { formValues, stepId, selectedStep, setFormValues } = props;
    const { width } = useSelectWindowDimension();

    const searchRef = useRef<any>(null);

    const [searchValue, setSearchValue] = useState<string>('');
    const [expandedList, setExpandedList] = useState<string[]>([]);

    const [cancelRowId, setCancelRowId] =
      useState<string | undefined>(undefined);

    const [editingRecord, setEditingRecord] =
      useState<IShell | undefined>(undefined);
    const [isBulkEdit, setIsBulkEdit] = useState(false);

    const shellList = useMemo(
      () => formValues?.shells || [],
      [formValues?.shells]
    );

    const metadata = useSelectMetadataForManageOCSShells();

    const levels = useMemo(() => metadata[ParameterMetadata.Level], [metadata]);

    const {
      sort,
      total,
      currentPage,
      currentPageSize,
      gridData: dataView,
      allDataFilter,
      dataChecked: _checkedList,
      onSetDataChecked,
      onSortChange,
      onPageChange,
      onPageSizeChange,
      onResetToDefaultFilter,
      onSearchChange
    } = usePagination(
      shellList,
      [
        ParameterCode.ShellName,
        ParameterCode.DepartmentId,
        ParameterCode.SupervisorShell
      ],
      ParameterCode.ShellName
    );

    const checkedList = useMemo<string[]>(() => _checkedList, [_checkedList]);

    const allIds = shellList.map(f => f.rowId!);
    const { gridClassName, handleClickCheckAll } = useCheckAllPagination(
      {
        gridClassName: `grid-has-tooltip center shell-grid`,
        allIds,
        hasFilter: !!searchValue,
        filterIds: allDataFilter.map(f => f.rowId!),
        checkedList,
        setCheckAll: _isCheckAll => onSetDataChecked(_isCheckAll ? allIds : []),
        setCheckList: onSetDataChecked
      },
      [searchValue, sort, currentPage, currentPageSize]
    );

    const showCountSelectedSection = useMemo(
      () => !searchValue || checkedList?.length > 1,
      [searchValue, checkedList?.length]
    );

    const mapGridExpandCollapse =
      (id: string, t: any) =>
      (item: any): ToggleButtonConfig =>
        ({
          id: item[id],
          collapsedTooltipProps: {
            element: t('txt_collapse'),
            opened: isDevice ? false : undefined
          },
          expandedTooltipProps: {
            element: t('txt_expand'),
            opened: isDevice ? false : undefined
          }
        } as ToggleButtonConfig);

    const checkSameWithOriginal = (value1: IShell, value2: IShell) => {
      return (
        (value1.level === value2.level || isUndefined(value2.level)) &&
        (value1.supervisorShell === value2.supervisorShell ||
          isUndefined(value2.supervisorShell))
      );
    };

    const handleClose = (isSave: boolean, shell?: IShell) => {
      let editingIds: string[] = [];
      if (!isEmpty(shell)) {
        if (isBulkEdit) {
          editingIds = checkedList;
        } else {
          editingIds = [shell!.rowId!];
        }

        const _shellList = shellList.map(i => {
          if (editingIds.includes(i.rowId!)) {
            const isSameOriginal = checkSameWithOriginal(i.original!, shell!);

            return {
              ...i,
              level: isUndefined(shell?.level) ? i.level : shell?.level,
              supervisorShell: isUndefined(shell?.supervisorShell)
                ? i.supervisorShell
                : shell?.supervisorShell,
              status: isSameOriginal ? STATUS.NotUpdated : STATUS.Updated
            };
          }
          return i;
        });
        onSetDataChecked(_checkedList.filter(f => !editingIds.includes(f)));
        setFormValues({
          shells: orderBy(_shellList, [ParameterCode.ShellName])
        });
      } else if (isSave) {
        onSetDataChecked(_checkedList.filter(f => f !== editingRecord?.rowId));
      }
      setEditingRecord(undefined);
      setIsBulkEdit(false);
    };

    const handleCancel = useCallback(() => {
      const idx = shellList?.findIndex(f => f.rowId === cancelRowId);

      const _shellList = [...shellList];
      _shellList[idx] = {
        ..._shellList[idx].original!,
        status: STATUS.NotUpdated,
        original: _shellList[idx].original
      };
      setFormValues({
        shells: orderBy(_shellList, [ParameterCode.ShellName])
      });
      setCancelRowId(undefined);
    }, [cancelRowId, shellList, setFormValues]);

    const selectedText = useMemo(
      () =>
        getSelectedRecord({
          selected: checkedList.length,
          total: shellList.map(f => f.rowId!).length,
          text: t('txt_config_manage_adjustments_system_shells_selected', {
            selected: checkedList.length
          }),
          text1: t('txt_config_manage_adjustments_system_1_shell_selected'),
          textAll: t('txt_config_manage_adjustments_system_all_shell_selected')
        }),
      [t, shellList, checkedList]
    );

    const actionAccessor = useCallback(
      (data: IShell) => {
        const status = data.status;

        const showCancel = [STATUS.Updated].includes(status);

        const handleClickEdit = () => {
          setEditingRecord(data);
        };

        const onClickCancel = () => {
          setCancelRowId(data.rowId as string);
        };

        return (
          <div className="d-flex justify-content-center">
            {
              <Button
                size="sm"
                variant="outline-primary"
                className="mx-4"
                onClick={handleClickEdit}
              >
                {t('txt_edit')}
              </Button>
            }

            {showCancel && (
              <Button
                size="sm"
                variant="outline-danger"
                className="mx-4"
                onClick={onClickCancel}
              >
                {t('txt_cancel')}
              </Button>
            )}
          </div>
        );
      },
      [t]
    );

    const renderSubRow = useCallback(
      ({ original }: { original: IShell }) => <ShellSubGrid shell={original} />,
      []
    );

    const columns = (t: any, actionAccessor?: any): ColumnType[] => [
      {
        id: 'shellName',
        Header: t('txt_shell_name'),
        accessor: 'shellName',
        isSort: true,
        width: width < 1280 ? 136 : undefined
      },
      {
        id: 'level',
        Header: t('txt_level'),
        accessor: (rowData: MagicKeyValue) => {
          const level = levels?.find(
            f => f.code?.toString() === rowData?.level?.toString()
          );
          return (
            <TruncateText
              resizable
              lines={2}
              ellipsisLessText={t('txt_less')}
              ellipsisMoreText={t('txt_more')}
            >
              {level?.text}
            </TruncateText>
          );
        },
        isSort: true,
        width: width < 1280 ? 157 : undefined
      },
      {
        id: 'departmentId',
        Header: (
          <div className="d-flex align-items-center">
            <p className="color-grey mr-8 fw-600">{t('txt_department_id')}</p>
            <Tooltip
              triggerClassName="d-flex"
              element={t(
                'txt_config_manage_adjustments_system_department_tooltip'
              )}
            >
              <Icon name="information" size="3x" className="color-grey-l16" />
            </Tooltip>
          </div>
        ),
        accessor: 'departmentId',
        isSort: true,
        width: width < 1280 ? 148 : undefined
      },
      {
        id: 'supervisorShell',
        Header: t('txt_supervisor'),
        accessor: 'supervisorShell',
        isSort: true,
        width: width < 1280 ? 115 : undefined
      },
      {
        id: 'status',
        Header: t('txt_status'),
        accessor: formatBadge(['status'], {
          colorType: BadgeColorType.ConfigParameterStatus
        }),
        width: 129
      },
      {
        id: 'code',
        Header: t('txt_actions'),
        className: 'text-center',
        accessor: actionAccessor,
        width: 133,
        cellBodyProps: { className: 'py-8' }
      }
    ];

    const handleClearFilter = useCallback(() => {
      setSearchValue('');
      setExpandedList([]);
    }, []);

    const handleSearch = (val: string) => {
      setSearchValue(val);
      setExpandedList([]);
    };

    useEffect(() => {
      !searchValue && searchRef?.current?.clear();
    }, [searchValue]);

    useEffect(() => {
      if (stepId !== selectedStep) {
        onSetDataChecked([]);
        onResetToDefaultFilter();
        setExpandedList([]);
        handleClearFilter();
      }
    }, [
      stepId,
      selectedStep,
      onSetDataChecked,
      onResetToDefaultFilter,
      handleClearFilter
    ]);

    useEffect(() => {
      onSearchChange(searchValue);
    }, [searchValue, onSearchChange]);

    const renderGrid = () => (
      <div>
        {showCountSelectedSection && (
          <div className="d-flex align-items-center my-16">
            {!searchValue && <p className="py-4"> {selectedText}</p>}
            {checkedList.length > 1 && (
              <Button
                variant="outline-primary"
                size="sm"
                className="ml-auto mr-n8"
                onClick={() => setIsBulkEdit(true)}
              >
                {t('txt_bulk_edit')}
              </Button>
            )}
          </div>
        )}

        {!isEmpty(dataView) && (
          <div
            className={classnames({
              'mt-16': !showCountSelectedSection
            })}
          >
            <Grid
              className={gridClassName}
              dataItemKey="rowId"
              expandedItemKey="rowId"
              data={dataView}
              columns={columns(t, actionAccessor)}
              togglable
              expandedList={expandedList}
              checkedList={checkedList}
              sortBy={[sort]}
              onSortChange={onSortChange}
              variant={{
                id: 'rowId',
                type: 'checkbox',
                cellHeaderProps: { onClick: handleClickCheckAll }
              }}
              subRow={renderSubRow}
              onExpand={setExpandedList}
              onCheck={onSetDataChecked}
              toggleButtonConfigList={dataView.map(
                mapGridExpandCollapse('rowId', t)
              )}
            />
            {total > 10 && (
              <div className="mt-16">
                <Paging
                  page={currentPage}
                  pageSize={currentPageSize}
                  totalItem={total}
                  onChangePage={onPageChange}
                  onChangePageSize={onPageSizeChange}
                />
              </div>
            )}
          </div>
        )}
        {(editingRecord || isBulkEdit) && (
          <EditShellModal
            id="editShellModal"
            show
            defaultShell={editingRecord}
            levels={levels}
            shells={shellList.filter(f => checkedList.includes(f.rowId!))}
            onClose={handleClose}
          />
        )}

        {cancelRowId !== undefined && (
          <CancelRecordsModal
            id="removeRecordsModal"
            show={cancelRowId !== undefined}
            onClose={() => setCancelRowId(undefined)}
            onConfirm={handleCancel}
          />
        )}
      </div>
    );

    return (
      <>
        <div className="d-flex align-items-center justify-content-between mb-16">
          <h5>{t('txt_shell_list')}</h5>
          <SimpleSearch
            ref={searchRef}
            placeholder={t('txt_type_to_search')}
            onSearch={handleSearch}
            popperElement={
              <>
                <p className="color-grey">{t('txt_search_description')}</p>
                <ul className="search-field-item list-unstyled">
                  <li className="mt-16">{t('txt_department_id')}</li>
                  <li className="mt-16">{t('txt_shell_name')}</li>
                  <li className="mt-16">{t('txt_supervisor')}</li>
                </ul>
              </>
            }
          />
        </div>
        {searchValue && !isEmpty(dataView) && (
          <div className="d-flex justify-content-end mb-16 mr-n8">
            <ClearAndResetButton
              small
              onClearAndReset={() => handleClearFilter()}
            />
          </div>
        )}
        {isEmpty(dataView) && (
          <div className="d-flex flex-column justify-content-center pt-40 mb-32">
            <NoDataFound
              id="shells_notfound"
              hasSearch
              title={t('txt_no_results_found')}
              linkTitle={t('txt_clear_and_reset')}
              onLinkClicked={() => handleClearFilter()}
            />
          </div>
        )}
        {!isEmpty(dataView) && <div className="mt-16">{renderGrid()}</div>}
      </>
    );
  };

export default ShellList;
