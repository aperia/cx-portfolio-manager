import NoDataFound from 'app/components/NoDataFound';
import { BadgeColorType, TransactionStatus } from 'app/constants/enums';
import { mapGridExpandCollapse } from 'app/helpers';
import { usePagination } from 'app/hooks/usePagination';
import { IShell } from 'app/types';
import {
  Button,
  ColumnType,
  Grid,
  Icon,
  Tooltip,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import { isFunction } from 'lodash';
import { useSelectMetadataForManageOCSShells } from 'pages/_commons/redux/WorkflowSetup';
import { formatBadge } from 'pages/_commons/Utils/formatGridField';
import Paging from 'pages/_commons/Utils/Paging';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { FormConfigFDSA } from '.';
import ShellSubGrid from './ShellSubGrid';
import { ParameterMetadata } from './type';

const ConfigFDSASummary: React.FC<WorkflowSetupSummaryProps<FormConfigFDSA>> =
  ({ onEditStep, formValues, stepId, selectedStep }) => {
    const { t } = useTranslation();

    const handleEdit = () => {
      isFunction(onEditStep) && onEditStep();
    };

    const resetFilter = useMemo(
      () => stepId === selectedStep,
      [stepId, selectedStep]
    );

    return (
      <div className="position-relative">
        <div className="absolute-top-right mt-n26 mr-n8">
          <Button variant="outline-primary" size="sm" onClick={handleEdit}>
            {t('txt_edit')}
          </Button>
        </div>
        <ProfileSection
          profile={formValues?.shells || []}
          resetFilter={resetFilter}
        />
      </div>
    );
  };

interface ProfileSectionProps {
  profile: IShell[];
  resetFilter: boolean;
}
const ProfileSection: React.FC<ProfileSectionProps> = ({
  profile,
  resetFilter
}) => {
  const { t } = useTranslation();

  const [expandedList, setExpandedList] = useState<string[]>([]);

  const _transactions = useMemo(
    () =>
      profile?.filter((f: any) => f.status !== TransactionStatus.NotUpdated),
    [profile]
  );

  const {
    total,
    currentPage,
    currentPageSize,
    gridData: dataView,
    onPageChange,
    onPageSizeChange,
    onResetToDefaultFilter
  } = usePagination(_transactions, [], 'shellName');

  const metadata = useSelectMetadataForManageOCSShells();

  const levels = useMemo(() => metadata[ParameterMetadata.Level], [metadata]);

  const columns = (t: any): ColumnType[] => [
    {
      id: 'shellName',
      Header: t('txt_shell_name'),
      accessor: 'shellName',
      width: 118
    },
    {
      id: 'currentLevel',
      Header: t('txt_current_level'),
      accessor: (rowData: MagicKeyValue) => {
        const level = levels?.find(
          f => f.code?.toString() === rowData?.original?.level?.toString()
        );
        return (
          <TruncateText
            resizable
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {level?.text}
          </TruncateText>
        );
      },
      width: 157
    },
    {
      id: 'newLevel',
      Header: t('txt_new_level'),
      accessor: (rowData: MagicKeyValue) => {
        const level = levels?.find(
          f => f.code?.toString() === rowData?.level?.toString()
        );
        return (
          <TruncateText
            resizable
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {level?.text}
          </TruncateText>
        );
      },
      width: 157
    },
    {
      id: 'departmentId',
      Header: (
        <div className="d-flex align-items-center">
          <p className="color-grey mr-8 fw-600">{t('txt_department_id')}</p>
          <Tooltip
            triggerClassName="d-flex"
            element={t(
              'txt_config_manage_adjustments_system_department_tooltip'
            )}
          >
            <Icon name="information" size="3x" className="color-grey-l16" />
          </Tooltip>
        </div>
      ),
      accessor: 'departmentId',
      width: 148
    },
    {
      id: 'prevShell',
      Header: t('txt_current_supervisor'),
      accessor: (rowData: MagicKeyValue) => {
        const prevShellName = rowData?.original?.supervisorShell;
        return (
          <TruncateText
            resizable
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {prevShellName}
          </TruncateText>
        );
      },
      width: 115
    },
    {
      id: 'supervisorShell',
      Header: t('txt_new_supervisor'),
      accessor: 'supervisorShell',
      width: 115
    },
    {
      id: 'status',
      Header: t('txt_status'),
      accessor: formatBadge(['status'], {
        colorType: BadgeColorType.ConfigParameterStatus
      }),
      width: 129
    }
  ];

  useEffect(() => {
    if (!resetFilter) return;

    onResetToDefaultFilter();
    setExpandedList([]);
  }, [resetFilter, onResetToDefaultFilter]);

  const renderSubRow = useCallback(
    ({ original }: { original: IShell }) => <ShellSubGrid shell={original} />,
    []
  );

  return (
    <>
      {total === 0 && (
        <div className="d-flex flex-column justify-content-center">
          <NoDataFound
            id="merchant-list-summary-NoDataFound"
            title={t('txt_config_processing_merchants_no_update_merchants')}
          />
        </div>
      )}
      {total > 0 && (
        <>
          <div className="pt-16">
            <Grid
              className="grid-has-tooltip center"
              togglable
              dataItemKey="rowId"
              expandedItemKey="rowId"
              data={dataView}
              columns={columns(t)}
              expandedList={expandedList}
              toggleButtonConfigList={dataView.map(
                mapGridExpandCollapse('rowId', t)
              )}
              subRow={renderSubRow}
              onExpand={setExpandedList}
            />
            {total > 10 && (
              <div className="mt-16">
                <Paging
                  page={currentPage}
                  pageSize={currentPageSize}
                  totalItem={total}
                  onChangePage={onPageChange}
                  onChangePageSize={onPageSizeChange}
                />
              </div>
            )}
          </div>
        </>
      )}
    </>
  );
};

export default ConfigFDSASummary;
