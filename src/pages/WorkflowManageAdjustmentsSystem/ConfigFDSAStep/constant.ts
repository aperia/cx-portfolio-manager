import { ColumnType } from 'app/_libraries/_dls';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';

export const shellListSubGridColumns = (
  t: any,
  valueAccessor?: any,
  width?: any
): ColumnType[] => [
  {
    id: 'businessName',
    Header: t('txt_business_name'),
    accessor: 'businessName'
  },
  {
    id: 'greenScreenName',
    Header: t('txt_green_screen_name'),
    accessor: 'greenScreenName'
  },
  {
    id: 'value',
    Header: t('txt_value'),
    accessor: valueAccessor,
    width: width < 1280 ? 200 : undefined
  },
  {
    id: 'moreInfo',
    Header: t('txt_more_info'),
    className: 'text-center',
    accessor: viewMoreInfo(['moreInfo'], t),
    width: 105
  }
];
