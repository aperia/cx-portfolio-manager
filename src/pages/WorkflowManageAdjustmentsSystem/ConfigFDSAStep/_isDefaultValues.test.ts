import isDefaultValues from './_isDefaultValues';

describe('ManageAdjustmentsSystemWorkflow > ConfigFDSAStep > isDefaultValues', () => {
  it('function isDefaultValues', () => {
    const formValues = {
      shells: [{ status: 'Not Updated' } as never],
      isValid: true
    };
    const defaultValue = {
      shells: [],
      isValid: true
    };

    const result = isDefaultValues(formValues, defaultValue);

    expect(result).toEqual(true);
  });
});
