import {
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { classnames } from 'app/helpers';
import { useUnsavedChangeRegistry } from 'app/hooks';
import { IShell } from 'app/types';
import { isEmpty, isEqual, orderBy } from 'app/_libraries/_dls/lodash';
import { STATUS } from 'pages/WorkflowManageOCSShells/ConfigureParameters/constants';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';
import ShellList from './ShellList';
import Summary from './Summary';
import { ParameterCode, ParameterMetadata, ShellLevel } from './type';
import isDefaultValues from './_isDefaultValues';
import parseFormValues from './_parseFormValues';

export interface FormConfigFDSA {
  isValid?: boolean;
  shells?: IShell[];
}
const ConfigFDSAStep: React.FC<WorkflowSetupProps<FormConfigFDSA>> = props => {
  const { setFormValues, formValues, savedAt } = props;
  const [initialValues, setInitialValues] = useState(formValues);
  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);

  const getInitialData = useCallback(async () => {
    if (!isEmpty(formValues.shells)) return;

    setLoading(true);
    const resp = (await Promise.resolve(
      dispatch(actionsWorkflowSetup.getShellList())
    )) as any;
    const isSuccess = actionsWorkflowSetup.getShellList.fulfilled.match(resp);
    if (isSuccess) {
      const shells = resp.payload.map((i: MagicKeyValue, idx: number) => {
        const rowId = `${i?.shellName}_${idx}_${Date.now()}`;
        const shell = {
          ...i,
          rowId: rowId,
          id: rowId,
          supervisorShell:
            i.level === ShellLevel.Manager ? '' : i.supervisorShell,
          status: STATUS.NotUpdated
        };

        return {
          ...shell,
          original: shell
        };
      });
      keepRef.current.setFormValues({
        shells: orderBy(shells, [ParameterCode.ShellName])
      });
    }
    setLoading(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch]);

  useEffect(() => {
    dispatch(
      actionsWorkflowSetup.getWorkflowMetadata(Object.values(ParameterMetadata))
    );
    dispatch(actionsWorkflowSetup.getSupervisorList());
    getInitialData();
  }, [getInitialData, dispatch]);

  useEffect(() => {
    if (isEmpty(formValues?.shells) || !isEmpty(initialValues.shells)) return;
    setInitialValues(formValues);
  }, [formValues, initialValues]);

  useEffect(() => {
    setInitialValues(formValues);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [savedAt]);

  useEffect(() => {
    const isValid = formValues?.shells?.some(
      s => s.status !== STATUS.NotUpdated
    );
    keepRef.current.setFormValues({ isValid });
  }, [formValues?.shells]);

  const formChanged = useMemo(
    () => !isEqual(formValues.shells, initialValues.shells),
    [formValues.shells, initialValues.shells]
  );

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_AUTOMATED_ADJUSTMENTS_FDSA,
      priority: 1
    },
    [formChanged]
  );

  return (
    <div className={classnames('mt-24', loading && 'loading')}>
      <ShellList {...props} />
    </div>
  );
};

const ExtraStaticConfigFDSAStep =
  ConfigFDSAStep as WorkflowSetupStaticProp<FormConfigFDSA>;

ExtraStaticConfigFDSAStep.summaryComponent = Summary;
ExtraStaticConfigFDSAStep.defaultValues = { isValid: false, shells: [] };
ExtraStaticConfigFDSAStep.parseFormValues = parseFormValues;
ExtraStaticConfigFDSAStep.isDefaultValues = isDefaultValues;

export default ExtraStaticConfigFDSAStep;
