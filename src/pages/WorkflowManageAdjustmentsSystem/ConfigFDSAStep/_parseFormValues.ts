import { getWorkflowSetupStepStatus } from 'app/helpers';
import { STATUS } from 'pages/WorkflowManageOCSShells/ConfigureParameters/constants';
import { FormConfigFDSA } from '.';
import { ParameterMetadata } from './type';

const parseFormValues: WorkflowSetupStepFormDataFunc<FormConfigFDSA> = (
  data,
  id
) => {
  const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);
  if (!stepInfo) return;

  const shellList = data?.data?.configurations?.shellList || [];

  const now = Date.now();
  const shells = shellList?.map((s, idx) => {
    const level = s.parameters?.find(
      (p: any) => p.name === ParameterMetadata.Level
    );
    const supervisorShell = s.parameters?.find(
      (p: any) => p.name === ParameterMetadata.SupervisorShell
    );

    const shell = {
      rowId: `${now}${idx}`,
      shellName: s.shellName,
      status: s.status,
      departmentId: s.departmentId,
      transactionProfile: s.transactionProfile,
      description: s.description,
      operatorCode: s.operatorCode,
      employeeId: s.employeeId,

      level: level?.newValue,
      supervisorShell: supervisorShell?.newValue
    };

    return {
      ...shell,
      original: {
        ...shell,
        level: level?.previousValue,
        supervisorShell: supervisorShell?.previousValue
      }
    };
  });

  const isValid = shells?.some(s => s.status !== STATUS.NotUpdated);

  return {
    ...getWorkflowSetupStepStatus(stepInfo),
    isValid,
    shells
  };
};

export default parseFormValues;
