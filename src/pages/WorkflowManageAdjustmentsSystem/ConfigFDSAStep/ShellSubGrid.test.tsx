import { render } from '@testing-library/react';
import { IShell } from 'app/types';
import { STATUS } from 'pages/WorkflowManageOCSShells/ConfigureParameters/constants';
import * as CommonRedux from 'pages/_commons/redux/Common/select-hooks';
import React from 'react';
import ShellSubGrid from './ShellSubGrid';

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

const mockUseSelectWindowDimension = jest.spyOn(
  CommonRedux,
  'useSelectWindowDimension'
);
const mockUseSelectWindowDimensionImplementation = (width: number) => {
  mockUseSelectWindowDimension.mockImplementation(() => ({ width } as any));
};

describe('WorkflowManageAdjustmentsSystem > ConfigFDSAStep > ShellSubGrid', () => {
  beforeEach(() => {
    mockUseSelectWindowDimensionImplementation(1400);
  });
  afterEach(() => {
    mockUseSelectWindowDimension.mockClear();
  });

  it('render grid', () => {
    mockUseSelectWindowDimensionImplementation(900);
    const shell = {
      rowId: '1',
      shellName: 'shellName',
      level: '01',
      departmentId: 'id1',
      supervisorShell: 'abc',
      status: STATUS.NotUpdated
    } as IShell;

    const wrapper = render(<ShellSubGrid shell={shell} />);
    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
  });
});
