import EnhanceDropdownList from 'app/components/EnhanceDropdownList';
import ModalRegistry from 'app/components/ModalRegistry';
import { isDevice } from 'app/helpers';
import { IShell } from 'app/types';
import {
  CheckBox,
  DropdownListProps,
  InlineMessage,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import {
  isEmpty,
  isEqual,
  isFunction,
  isUndefined
} from 'app/_libraries/_dls/lodash';
import { useSelectSupervisors } from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageAutomatedAdjustmentsSystem';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { ParameterCode, ShellLevel } from './type';

interface IProps {
  id: string;
  show: boolean;
  defaultShell?: IShell;
  shells?: IShell[];
  levels?: RefData[];
  onClose: (isSave: boolean, shell?: IShell) => void;
}

const EditShellModal: React.FC<IProps> = ({
  id,
  show,
  defaultShell,
  shells = [],
  levels = [],
  onClose
}) => {
  const { t } = useTranslation();
  const [shell, setShell] = useState(defaultShell);
  const [showWarningChanged, setShowWarning] = useState(false);
  const [warningText, setWarningText] = useState('');
  const isBulkEdit = isEmpty(defaultShell);
  const editingShellList = isEmpty(defaultShell) ? shells : [defaultShell];
  const [tooltipOpened, setTooltipOpened] = useState(false);

  const selectSupervisor = useSelectSupervisors();

  const isDiffLevelWithOriginal = useMemo(
    () => defaultShell && defaultShell?.original?.level !== defaultShell?.level,
    [defaultShell]
  );
  const isDiffSupervisorWithOriginal = useMemo(
    () =>
      defaultShell &&
      defaultShell?.original?.supervisorShell !== defaultShell?.supervisorShell,
    [defaultShell]
  );
  const [checkedLevel, setCheckedLevel] = useState(
    isDiffLevelWithOriginal ? true : false
  );
  const [checkedSupervisor, setCheckedSupervisor] = useState(
    isDiffSupervisorWithOriginal ? true : false
  );

  const supervisor = useMemo<RefData[]>(() => {
    return selectSupervisor.map(i => ({ code: i.name!, text: i.name! }));
  }, [selectSupervisor]);

  const supervisorDataByLevel = useMemo<RefData[]>(() => {
    const level = checkedLevel ? shell?.level : defaultShell?.level;
    const lv = parseInt(level || '00');
    return selectSupervisor
      .filter(f => parseInt(f.level!) >= lv && parseInt(f.level!) !== 0)
      .map(i => ({ code: i.name!, text: i.name! }));
  }, [selectSupervisor, shell?.level, checkedLevel, defaultShell]);

  const isManagerLevel = useMemo(
    () => shell?.level === ShellLevel.Manager,
    [shell]
  );

  const hasShellWithNonSupervisor = !!editingShellList.find(f =>
    isEmpty(f!.supervisorShell)
  );

  const hasShellWithSupervisor = !!editingShellList.find(
    f => !isEmpty(f!.supervisorShell)
  );
  const hasManagerLevel = !!editingShellList.find(
    f => f!.level === ShellLevel.Manager
  );

  const warningSelectSupervisor = useMemo(
    () =>
      [ShellLevel.Operator, ShellLevel.Supervisor].includes(
        shell?.level as ShellLevel
      ) &&
      (hasShellWithNonSupervisor ||
        defaultShell?.original?.level === ShellLevel.Manager),
    [shell, hasShellWithNonSupervisor, defaultShell]
  );

  const warningRemoveSupervisor = useMemo(
    () =>
      shell?.level === ShellLevel.Manager &&
      ((checkedSupervisor && isEmpty(shell?.supervisorShell)) ||
        (!checkedSupervisor && hasShellWithSupervisor)),
    [shell, checkedSupervisor, hasShellWithSupervisor]
  );

  const isDisableSupervisor = useMemo(
    () => isManagerLevel || (hasManagerLevel && isUndefined(shell?.level)),
    [isManagerLevel, shell?.level, hasManagerLevel]
  );
  const isRequiredSupervisor = useMemo(
    () =>
      (checkedLevel &&
        !isEmpty(shell?.level) &&
        [ShellLevel.Operator, ShellLevel.Supervisor].includes(
          shell!.level! as ShellLevel
        )) ||
      (!checkedLevel && hasShellWithSupervisor),
    [shell, checkedLevel, hasShellWithSupervisor]
  );

  const isInvalid = useMemo(
    () =>
      (warningSelectSupervisor &&
        (!checkedLevel ||
          !checkedSupervisor ||
          isEmpty(shell?.supervisorShell))) ||
      (!checkedLevel && (!checkedSupervisor || isDisableSupervisor)) ||
      (isRequiredSupervisor &&
        (defaultShell?.original?.level === ShellLevel.Manager ||
          defaultShell?.level === ShellLevel.Manager) &&
        !checkedSupervisor) ||
      (checkedSupervisor &&
        isRequiredSupervisor &&
        isEmpty(shell?.supervisorShell)) ||
      (checkedLevel && isUndefined(shell?.level)),
    [
      defaultShell,
      warningSelectSupervisor,
      shell,
      isRequiredSupervisor,
      checkedSupervisor,
      checkedLevel,
      isDisableSupervisor
    ]
  );
  const handleClose = () => {
    isFunction(onClose) && onClose();
  };

  const handleEdit = () => {
    if (shell !== undefined && isEqual(defaultShell, shell)) {
      onClose(true);
      return;
    }
    const _shell = { ...shell! };
    let supervisorShell = _shell.supervisorShell;
    if (!checkedSupervisor) {
      supervisorShell = undefined;
    }
    if (
      isManagerLevel ||
      (checkedSupervisor && isUndefined(_shell.supervisorShell))
    ) {
      supervisorShell = '';
    }

    let level = _shell.level;
    if (!checkedLevel) {
      level = undefined;
    }

    onClose(true, {
      ..._shell,
      supervisorShell: supervisorShell,
      level: level
    });
  };

  const handleFormChange = useCallback(
    (field: keyof IShell) => (e: any) => {
      const value = e.target?.value?.code;

      setShell({ ...shell, [field]: value } as IShell);
      if (field === 'level') {
        if (
          value === ShellLevel.None ||
          (warningText ===
            t('txt_config_manage_adjustments_system_warning_message_1') &&
            value !== ShellLevel.Manager) ||
          (warningText ===
            t('txt_config_manage_adjustments_system_warning_message_2') &&
            ![ShellLevel.Operator, ShellLevel.Supervisor].includes(
              shell?.level as ShellLevel
            ))
        ) {
          setWarningText('');
        }

        setShowWarning(true);
      }
    },
    [shell, t, warningText]
  );

  const renderDropdown = useCallback(
    (data: RefData[], field: keyof IShell, props?: DropdownListProps) => {
      const value = data.find(o => o.code === shell?.[field]);

      return (
        <EnhanceDropdownList
          placeholder={t(
            'txt_workflow_manage_change_in_terms_select_an_option'
          )}
          dataTestId={`editShell__${field}`}
          value={value}
          onChange={handleFormChange(field)}
          options={data}
          {...props}
        />
      );
    },
    [t, shell, handleFormChange]
  );

  const renderCurrentValueDropdown = useCallback(
    (data: RefData[], field: keyof IShell, props?: DropdownListProps) => {
      const value = data.find(
        o => o.code === defaultShell?.original?.[field]
      ) || { text: '' };

      return (
        <EnhanceDropdownList
          readOnly
          placeholder={t(
            'txt_workflow_manage_change_in_terms_select_an_option'
          )}
          dataTestId={`editShell__${field}_currentValue`}
          className="form-group-static__text"
          value={value}
          onChange={handleFormChange(field)}
          options={data}
          {...props}
        />
      );
    },
    [t, defaultShell, handleFormChange]
  );

  useEffect(() => {
    isManagerLevel && setCheckedSupervisor(false);
  }, [isManagerLevel, isBulkEdit]);

  useEffect(() => {
    if (warningRemoveSupervisor) {
      setWarningText(
        t('txt_config_manage_adjustments_system_warning_message_1')
      );
    }

    if (warningSelectSupervisor) {
      setWarningText(
        t('txt_config_manage_adjustments_system_warning_message_2')
      );
    }
  }, [
    warningRemoveSupervisor,
    warningSelectSupervisor,
    warningText,
    shell?.level,
    t
  ]);

  return (
    <ModalRegistry
      xs={isBulkEdit}
      sm={!isBulkEdit}
      id={id}
      show={show}
      onAutoClosedAll={handleClose}
    >
      <ModalHeader border closeButton onHide={handleClose}>
        <ModalTitle>
          {isBulkEdit
            ? t('txt_config_manage_adjustments_system_bulk_edit_shells_title')
            : t('txt_config_manage_adjustments_system_edit_shells_title')}
        </ModalTitle>
      </ModalHeader>
      <ModalBody>
        {showWarningChanged && warningText && (
          <InlineMessage variant="warning" withIcon className="mb-16">
            {warningText}
          </InlineMessage>
        )}
        <p>{t('txt_config_manage_adjustments_system_edit_description')}</p>
        <div className="d-flex my-16">
          <CheckBox>
            <CheckBox.Input
              id={'edit_level'}
              name={'edit_level'}
              checked={checkedLevel}
              onChange={() => {
                if (
                  !checkedLevel &&
                  defaultShell?.original?.level === shell?.level
                ) {
                  setShell({ ...shell, level: undefined } as IShell);
                }
                setShowWarning(!checkedLevel);
                setCheckedLevel(!checkedLevel);
              }}
            />
            <CheckBox.Label>{t('txt_level')}</CheckBox.Label>
          </CheckBox>
        </div>
        {checkedLevel && (
          <>
            <div className="row">
              {!isEmpty(defaultShell) && (
                <div className="col">
                  {renderCurrentValueDropdown(levels, ParameterCode.Level, {
                    label: t('txt_current_level')
                  } as DropdownListProps)}
                </div>
              )}
              <div className="col">
                {renderDropdown(levels, ParameterCode.Level, {
                  label: t('txt_new_level'),
                  required: true
                } as DropdownListProps)}
              </div>
            </div>
          </>
        )}
        <div className="d-flex mt-24 mb-16">
          <Tooltip
            opened={isDevice ? false : tooltipOpened && isDisableSupervisor}
            element={t(
              'txt_config_manage_adjustments_system_disable_supervisors_tooltip'
            )}
            variant="default"
            placement="top"
            onVisibilityChange={setTooltipOpened}
          >
            <CheckBox>
              <CheckBox.Input
                id={'edit_supervisor'}
                name={'edit_supervisor'}
                checked={checkedSupervisor}
                disabled={isDisableSupervisor}
                onChange={() => {
                  if (
                    !checkedSupervisor &&
                    defaultShell?.original?.supervisorShell ===
                      shell?.supervisorShell
                  ) {
                    setShell({
                      ...shell,
                      supervisorShell: undefined
                    } as IShell);
                  }
                  setCheckedSupervisor(!checkedSupervisor);
                }}
              />
              <CheckBox.Label>{t('txt_supervisor')}</CheckBox.Label>
            </CheckBox>
          </Tooltip>
        </div>
        {checkedSupervisor && (
          <>
            <div className="row">
              {!isEmpty(defaultShell) && (
                <div className="col">
                  {renderCurrentValueDropdown(
                    supervisor,
                    ParameterCode.SupervisorShell,
                    {
                      label: t('txt_current_supervisor'),
                      disabled: isDisableSupervisor
                    } as DropdownListProps
                  )}
                </div>
              )}
              <div className="col">
                {renderDropdown(
                  supervisorDataByLevel,
                  ParameterCode.SupervisorShell,
                  {
                    label: t('txt_new_supervisor'),
                    required: isRequiredSupervisor,
                    disabled:
                      isDisableSupervisor ||
                      (checkedLevel && isEmpty(shell?.level)) ||
                      (!checkedLevel &&
                        defaultShell?.level === ShellLevel.Manager)
                  } as DropdownListProps
                )}
              </div>
            </div>
          </>
        )}
      </ModalBody>
      <ModalFooter
        cancelButtonText={t('txt_cancel')}
        okButtonText={t('txt_save')}
        onCancel={handleClose}
        onOk={handleEdit}
        disabledOk={isInvalid}
      ></ModalFooter>
    </ModalRegistry>
  );
};

export default EditShellModal;
