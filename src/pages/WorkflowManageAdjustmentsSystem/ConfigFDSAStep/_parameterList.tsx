import { IShell } from 'app/types';
import React from 'react';
import { ShellParameterName } from '../type';

export interface Parameter {
  id: keyof IShell;
  businessName: ShellParameterName;
  greenScreenName: string;
  moreInfoText?: string;
  moreInfo?: React.ReactNode;
}

export const MORE_INFO = {
  TransactionProfile:
    'The Transaction Profile parameter identifies name of the transaction profile to assign to this shell.',
  Description:
    'The Description parameter defines the name of the operator assigned to this shell.',
  OperatorCode:
    'The Operator Code parameter defines the identifier of the operator for this shell.',
  ActionCode:
    'The Employee ID parameter defines the identifier of the employee.'
};

export const parameters: Parameter[] = [
  {
    id: 'transactionProfile',
    businessName: ShellParameterName.TransactionProfile,
    greenScreenName: 'Tran Pro',
    moreInfo: MORE_INFO.TransactionProfile
  },
  {
    id: 'description',
    businessName: ShellParameterName.Description,
    greenScreenName: 'Description',
    moreInfo: MORE_INFO.Description
  },
  {
    id: 'operatorCode',
    businessName: ShellParameterName.OperatorCode,
    greenScreenName: 'Oper Code',
    moreInfo: MORE_INFO.OperatorCode
  },
  {
    id: 'employeeId',
    businessName: ShellParameterName.EmployeeID,
    greenScreenName: 'Empl ID',
    moreInfo: MORE_INFO.ActionCode
  }
];
