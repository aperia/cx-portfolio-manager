export enum ParameterMetadata {
  SupervisorShell = 'supervisor.shell',
  Level = 'level'
}
export enum ParameterCode {
  ShellName = 'shellName',
  DepartmentId = 'departmentId',
  SupervisorShell = 'supervisorShell',
  Level = 'level'
}

export enum ShellLevel {
  None = '00',
  Operator = '01',
  Supervisor = '02',
  Manager = '03'
}
