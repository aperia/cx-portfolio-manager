import { act, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { IShell } from 'app/types';
import { mockUseModalRegistryFnc, renderWithMockStore } from 'app/utils';
import mockDevice from 'app/utils/_mockHelperConstant/mockDevice';
import { queryAllByClass } from 'app/_libraries/_dls/test-utils';
import { STATUS } from 'pages/WorkflowManageOCSShells/ConfigureParameters/constants';
import React from 'react';
import ShellList from './ShellList';

jest.mock(
  'pages/WorkflowManageAdjustmentsSystem/ConfigFDSAStep/EditShellModal',
  () => ({
    __esModule: true,
    default: ({ onClose }: any) => {
      return (
        <div>
          <div>EditShellModal</div>
          <button onClick={() => onClose(false)}>onClose</button>
          <button onClick={() => onClose(true)}>onClose 2</button>
          <button
            onClick={() =>
              onClose(true, {
                level: '02',
                supervisorShell: undefined
              } as IShell)
            }
          >
            edit level
          </button>
          <button
            onClick={() =>
              onClose(true, {
                level: '02',
                supervisorShell: undefined
              } as IShell)
            }
          >
            edit Shell
          </button>
          <button
            onClick={() =>
              onClose(true, {
                level: undefined,
                supervisorShell: 'supervisorShell'
              } as IShell)
            }
          >
            edit supervisorShell
          </button>
        </div>
      );
    }
  })
);
jest.mock(
  'pages/WorkflowManageAdjustmentsSystem/ConfigFDSAStep/CancelRecordsModal',
  () => ({
    __esModule: true,
    default: ({ onClose, onConfirm }: any) => {
      return (
        <div>
          <div>CancelRecordsModal</div>
          <button onClick={() => onClose()}>onClose</button>
          <button onClick={() => onConfirm()}>onConfirm</button>
        </div>
      );
    }
  })
);

const mockUseModalRegistry = mockUseModalRegistryFnc();

const renderComponent = async (
  props: any,
  workflowSetup?: any
): Promise<RenderResult> => {
  return await renderWithMockStore(<ShellList {...props} />, {
    initialState: { workflowSetup }
  });
};

describe('ManageAdjustmentsSystemWorkflow > ConfigFLSADJStep > ProfileList', () => {
  beforeEach(() => {
    mockDevice.isDevice = false;

    mockUseModalRegistry.mockImplementation(() => [true, false]);
  });

  afterEach(() => {
    mockUseModalRegistry.mockClear();
  });

  const defaultWorkflowSetup = {
    elementMetadata: {
      elements: [
        {
          id: '1',
          name: 'supervisor.shell',
          options: [
            {
              value: 'SHELL001',
              description: '',
              selected: null
            },
            {
              value: 'SHELL002',
              description: '',
              selected: null
            }
          ]
        },
        {
          id: '01',
          name: 'level',
          options: [
            {
              value: '00',
              description: 'Default',
              selected: null
            },
            {
              value: '01',
              description: 'Operator',
              selected: null
            },
            {
              value: '02',
              description: 'Supervisor',
              selected: null
            },
            {
              value: '03',
              description: 'Manager or department',
              selected: null
            }
          ]
        }
      ]
    },
    shellList: { loading: true, error: false }
  };
  it('should render list empty', async () => {
    const props = {
      stepId: '1',
      selectedStep: '2',
      formValues: {},
      setFormValues: jest.fn()
    } as any;
    jest.useFakeTimers();
    const wrapper = await renderComponent(props, defaultWorkflowSetup);
    jest.runAllTimers();

    expect(
      wrapper.getByText('txt_no_results_found txt_adjust_your_search_criteria')
    ).toBeInTheDocument();

    userEvent.click(wrapper.getByText('txt_clear_and_reset'));

    expect(
      wrapper.getByText('txt_no_results_found txt_adjust_your_search_criteria')
    ).toBeInTheDocument();
  });
  it('should render list', async () => {
    const props = {
      stepId: '2',
      selectedStep: '2',
      formValues: {
        shells: [
          {
            rowId: '1',
            id: '1',
            shellName: 'shellName',
            level: '01',
            departmentId: 'id1',
            supervisorShell: 'abc',
            status: STATUS.NotUpdated
          },
          {
            rowId: '2',
            id: '2',
            shellName: 'shellName',
            level: '01',
            departmentId: 'id1',
            supervisorShell: 'abc',
            status: STATUS.Updated
          },
          {},
          {},
          {},
          {},
          {},
          {},
          {},
          {},
          {},
          {}
        ]
      },
      setFormValues: jest.fn()
    } as any;
    jest.useFakeTimers();
    const wrapper = await renderComponent(props, defaultWorkflowSetup);
    jest.runAllTimers();

    const btnExpand = queryAllByClass(wrapper.container, /icon icon-plus/)[0];
    act(() => {
      userEvent.click(btnExpand);
    });
    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();

    const searchBox = wrapper.getByPlaceholderText('txt_type_to_search');

    userEvent.type(searchBox, 'a');
    userEvent.click(
      searchBox.parentElement!.querySelector(
        '.icon-simple-search .icon.icon-search'
      )!
    );
    userEvent.click(wrapper.getByText('txt_clear_and_reset'));

    userEvent.type(searchBox, 'notfound');
    userEvent.click(
      searchBox.parentElement!.querySelector(
        '.icon-simple-search .icon.icon-search'
      )!
    );

    expect(
      wrapper.getByText('txt_no_results_found txt_adjust_your_search_criteria')
    ).toBeInTheDocument();

    wrapper.rerender(<ShellList {...props} selectedStep="1" />);
  });

  it('edit shells', async () => {
    const props = {
      stepId: '1',
      selectedStep: '2',
      formValues: {
        shells: [
          {
            rowId: '1',
            id: '1',
            shellName: 'shellName',
            level: '01',
            departmentId: 'id1',
            supervisorShell: 'abc',
            status: STATUS.NotUpdated,
            original: { level: '02' }
          },
          {
            rowId: '2',
            id: '2',
            shellName: 'shellName',
            level: '02',
            departmentId: 'id1',
            supervisorShell: 'abc',
            status: STATUS.NotUpdated,
            original: { level: '01' }
          },
          {
            rowId: '3',
            id: '3',
            shellName: 'shellName',
            level: '01',
            departmentId: 'id1',
            supervisorShell: 'abc',
            status: STATUS.NotUpdated,
            original: { level: '01' }
          }
        ]
      },
      setFormValues: jest.fn()
    } as any;
    jest.useFakeTimers();
    const wrapper = await renderComponent(props, defaultWorkflowSetup);
    jest.runAllTimers();

    expect(wrapper.getAllByText('Not Updated')[0]).toBeInTheDocument();

    userEvent.click(wrapper.getAllByText('txt_edit')[0]);
    expect(wrapper.getByText('EditShellModal')).toBeInTheDocument();
    // userEvent.click(wrapper.getByText('onClose 2'));

    userEvent.click(wrapper.getByText('onClose'));
    expect(wrapper.queryByText('Updated')).not.toBeInTheDocument();

    userEvent.click(wrapper.getAllByText('txt_edit')[0]);
    expect(wrapper.getByText('EditShellModal')).toBeInTheDocument();
    userEvent.click(wrapper.getByText('edit level'));

    const checkboxAll = wrapper.getAllByRole('checkbox')[0];
    userEvent.click(checkboxAll);
    expect(
      wrapper.getByText(
        'txt_config_manage_adjustments_system_all_shell_selected'
      )
    ).toBeInTheDocument();

    userEvent.click(checkboxAll);
    const checkbox1 = wrapper.getAllByRole('checkbox')[1];
    userEvent.click(checkbox1);
    const checkbox2 = wrapper.getAllByRole('checkbox')[2];
    userEvent.click(checkbox2);
    expect(wrapper.getByText('txt_bulk_edit')).toBeInTheDocument();
    userEvent.click(wrapper.getByText('txt_bulk_edit'));
    expect(wrapper.getByText('EditShellModal')).toBeInTheDocument();
    userEvent.click(wrapper.getByText('edit Shell'));

    userEvent.click(checkboxAll);
    userEvent.click(wrapper.getByText('txt_bulk_edit'));
    expect(wrapper.getByText('EditShellModal')).toBeInTheDocument();
    userEvent.click(wrapper.getByText('edit supervisorShell'));
  });

  it('edit shells', async () => {
    const props = {
      stepId: '1',
      selectedStep: '2',
      formValues: {
        shells: [
          {
            rowId: '1',
            id: '1',
            shellName: 'shellName',
            level: '01',
            departmentId: 'id1',
            supervisorShell: 'abc',
            status: STATUS.NotUpdated,
            original: { level: '02' }
          },
          {
            rowId: '2',
            id: '2',
            shellName: 'shellName',
            level: '02',
            departmentId: 'id1',
            supervisorShell: 'abc',
            status: STATUS.NotUpdated,
            original: { level: '01' }
          },
          {
            rowId: '3',
            id: '3',
            shellName: 'shellName',
            level: '01',
            departmentId: 'id1',
            supervisorShell: 'abc',
            status: STATUS.NotUpdated,
            original: { level: '01' }
          }
        ]
      },
      setFormValues: jest.fn()
    } as any;
    jest.useFakeTimers();
    const wrapper = await renderComponent(props, defaultWorkflowSetup);
    jest.runAllTimers();
    const checkboxAll = wrapper.getAllByRole('checkbox')[0];
    userEvent.click(checkboxAll);
    expect(wrapper.getAllByText('Not Updated')[0]).toBeInTheDocument();

    userEvent.click(wrapper.getAllByText('txt_edit')[0]);
    expect(wrapper.getByText('EditShellModal')).toBeInTheDocument();
    userEvent.click(wrapper.getByText('onClose 2'));
  });
  it('cancel shells', async () => {
    const props = {
      stepId: '1',
      selectedStep: '2',
      formValues: {
        shells: [
          {
            rowId: '1',
            id: '1',
            shellName: 'shellName',
            level: '01',
            departmentId: 'id1',
            supervisorShell: 'abc',
            status: STATUS.Updated,
            original: { level: '02' }
          },
          {
            rowId: '2',
            id: '2',
            shellName: 'shellName',
            level: '02',
            departmentId: 'id1',
            supervisorShell: 'abc',
            status: STATUS.Updated,
            original: { level: '01' }
          }
        ]
      },
      setFormValues: jest.fn()
    } as any;
    jest.useFakeTimers();
    const wrapper = await renderComponent(props, defaultWorkflowSetup);
    jest.runAllTimers();

    userEvent.click(wrapper.getAllByText('txt_cancel')[0]);
    expect(wrapper.getByText('CancelRecordsModal')).toBeInTheDocument();
    userEvent.click(wrapper.getByText('onClose'));

    expect(
      wrapper.queryByText(STATUS.NotUpdated.toString())
    ).not.toBeInTheDocument();

    userEvent.click(wrapper.getAllByText('txt_cancel')[0]);
    expect(wrapper.getByText('CancelRecordsModal')).toBeInTheDocument();
    userEvent.click(wrapper.getByText('onConfirm'));
    // expect(
    //   wrapper.queryByText(STATUS.NotUpdated.toString())
    // ).toBeInTheDocument();
  });
  it('should render in device', async () => {
    mockDevice.isDevice = true;

    const props = {
      stepId: '1',
      selectedStep: '2',
      formValues: {
        shells: [
          {
            rowId: '1',
            id: '1',
            shellName: 'shellName',
            level: '01',
            departmentId: 'id1',
            supervisorShell: 'abc',
            status: STATUS.NotUpdated,
            original: { level: '02' }
          },
          {
            rowId: '2',
            id: '2',
            shellName: 'shellName',
            level: '02',
            departmentId: 'id1',
            supervisorShell: 'abc',
            status: STATUS.NotUpdated,
            original: { level: '01' }
          }
        ]
      },
      setFormValues: jest.fn()
    } as any;
    jest.useFakeTimers();
    const wrapper = await renderComponent(props, defaultWorkflowSetup);
    jest.runAllTimers();

    const expand = wrapper.baseElement.querySelectorAll('.icon.icon-plus')[0];
    userEvent.click(expand);
    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(wrapper.getByText('txt_green_screen_name')).toBeInTheDocument();
  });
});
