import { IShell } from 'app/types';
import {
  ColumnType,
  Grid,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import { get } from 'app/_libraries/_dls/lodash';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import React, { useCallback, useMemo } from 'react';
import { shellListSubGridColumns } from './constant';
import { parameters } from './_parameterList';

export interface ShellSubGridProps {
  shell: IShell;
}
const ShellSubGrid: React.FC<ShellSubGridProps> = ({ shell }) => {
  const { width } = useSelectWindowDimension();
  const { t } = useTranslation();

  const valueAccessor = useCallback(
    (data: Record<string, any>) => {
      return (
        <TruncateText
          lines={2}
          ellipsisLessText={t('txt_less')}
          ellipsisMoreText={t('txt_more')}
        >
          {get(shell, [data.id])}
        </TruncateText>
      );
    },
    [t, shell]
  );

  const columns: ColumnType[] = useMemo(
    () => shellListSubGridColumns(t, valueAccessor, width),
    [t, valueAccessor, width]
  );

  return (
    <div className="p-20 overflow-hidden">
      <Grid
        className="grid-has-tooltip center"
        columns={columns}
        data={parameters}
      />
    </div>
  );
};

export default ShellSubGrid;
