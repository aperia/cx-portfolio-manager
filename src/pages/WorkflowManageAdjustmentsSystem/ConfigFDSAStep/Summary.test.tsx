import { act, fireEvent, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { renderWithMockStore } from 'app/utils';
import 'app/utils/_mockComponent/mockPaging';
import { STATUS } from 'pages/WorkflowManageOCSShells/ConfigureParameters/constants';
import React from 'react';
import Summary from './Summary';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderComponent = async (
  props: any,
  workflowSetup?: any
): Promise<RenderResult> => {
  return await renderWithMockStore(<Summary {...props} />, {
    initialState: {
      workflowSetup: {
        elementMetadata: {
          loading: false,
          elements: [
            {
              id: '331',
              name: 'level',
              options: [
                {
                  value: '',
                  description: 'None'
                },
                {
                  value: '00',
                  description: 'Default value'
                },
                {
                  value: '01',
                  description: 'Operator'
                },
                {
                  value: '02',
                  description: 'Supervisor'
                },
                {
                  value: '03',
                  description: 'Manager'
                }
              ]
            }
          ]
        }
      }
    }
  });
};

describe('ManageAdjustmentsSystemWorkflow > ConfigFDSAStep > Summary', () => {
  it('Should render summary', async () => {
    const wrapper = await renderComponent({
      formValues: {}
    } as WorkflowSetupSummaryProps);

    expect(
      wrapper.getByText('txt_config_processing_merchants_no_update_merchants')
    ).toBeInTheDocument();
  });

  it('Should render summary with data', async () => {
    const wrapper = await renderComponent({
      formValues: {
        shells: [
          {
            shellName: 'string',
            transactionProfile: 'string',
            description: 'string',
            operatorCode: 'string',
            employeeId: 'string',
            supervisorShell: 'string',
            departmentId: 'string',
            level: '01',
            status: STATUS.Updated,
            rowId: 'string',
            original: {
              shellName: 'string',
              transactionProfile: 'string',
              description: 'string',
              operatorCode: 'string',
              employeeId: 'string',
              supervisorShell: 'string',
              departmentId: 'string',
              level: '01',
              status: STATUS.Updated,
              rowId: 'string'
            }
          }
        ]
      }
    } as WorkflowSetupSummaryProps);
    // expand
    const expands = wrapper.baseElement.querySelectorAll('.icon.icon-plus');
    act(() => {
      userEvent.click(expands[0]);
    });
    expect(wrapper.getByText('txt_green_screen_name')).toBeInTheDocument();
  });

  it('Should render summary with many data', async () => {
    const wrapper = await renderComponent({
      formValues: {
        shells: Array.from({ length: 12 }, (_, index) => ({
          shellName: 'string',
          transactionProfile: 'string',
          description: 'string',
          operatorCode: 'string',
          employeeId: 'string',
          supervisorShell: 'string',
          departmentId: 'string',
          level: '01',
          status: STATUS.Updated,
          rowId: `${index}`,
          original: {
            shellName: 'string',
            transactionProfile: 'string',
            description: 'string',
            operatorCode: 'string',
            employeeId: 'string',
            supervisorShell: 'string',
            departmentId: 'string',
            level: '01',
            status: STATUS.Updated,
            rowId: 'string'
          }
        }))
      }
    } as WorkflowSetupSummaryProps);

    expect(wrapper.getByText('Page 2')).toBeInTheDocument();
  });

  it('Should render summary without resetFilter', async () => {
    const wrapper = await renderComponent({
      formValues: {
        shells: [
          {
            shellName: 'string',
            transactionProfile: 'string',
            description: 'string',
            operatorCode: 'string',
            employeeId: 'string',
            supervisorShell: 'string',
            departmentId: 'string',
            level: '01',
            status: STATUS.Updated,
            rowId: 'string',
            original: {
              shellName: 'string',
              transactionProfile: 'string',
              description: 'string',
              operatorCode: 'string',
              employeeId: 'string',
              supervisorShell: 'string',
              departmentId: 'string',
              level: '01',
              status: STATUS.Updated,
              rowId: 'string'
            }
          }
        ]
      },
      stepId: 'step4',
      selectedStep: '!step4'
    } as WorkflowSetupSummaryProps);
    // expand

    expect(wrapper.getByText('txt_new_supervisor')).toBeInTheDocument();
  });

  it('Should click on edit', async () => {
    const mockFn = jest.fn();
    const wrapper = await renderComponent({
      formValues: {},
      onEditStep: mockFn
    } as WorkflowSetupSummaryProps);

    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(mockFn).toHaveBeenCalled();
  });
});
