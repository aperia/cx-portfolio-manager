import { isEmpty } from 'lodash';
import { STATUS } from 'pages/WorkflowManageOCSShells/ConfigureParameters/constants';
import { FormConfigFDSA } from '.';

const isDefaultValues: WorkflowSetupIsDefaultValueFunc<FormConfigFDSA> = (
  formValues,
  defaultValues
) => {
  return (
    isEmpty(formValues?.shells?.filter(s => s.status !== STATUS.NotUpdated)) &&
    formValues?.isValid === defaultValues?.isValid
  );
};

export default isDefaultValues;
