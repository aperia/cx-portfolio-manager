import { act, RenderResult, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import * as helper from 'app/helpers/isDevice';
import { SupervisorDataState } from 'app/types';
import { mockUseModalRegistryFnc, renderWithMockStore } from 'app/utils';
import { queryByClass } from 'app/_libraries/_dls/test-utils';
import React from 'react';
import EditShellModal from './EditShellModal';
import { ShellLevel } from './type';

const mockUseModalRegistry = mockUseModalRegistryFnc();

jest.mock('app/helpers/isDevice', () => ({
  __esModule: true,
  isDevice: null
}));

const mockHelper: any = helper;

const renderComponent = async (
  props: any,
  supervisorList?: any[]
): Promise<RenderResult> => {
  return await renderWithMockStore(<EditShellModal {...props} />, {
    initialState: {
      workflowSetup: {
        supervisorData: {
          supervisorList: supervisorList
        } as SupervisorDataState
      }
    } as AppState
  });
};
const levels = [
  { code: ShellLevel.None, text: 'None' },
  { code: ShellLevel.Operator, text: 'Operator' },
  { code: ShellLevel.Manager, text: 'Manager' }
];
describe('ManageAdjustmentsSystemWorkflow > ConfigFDSAStep > EditShellModal', () => {
  beforeEach(() => {
    mockUseModalRegistry.mockReturnValue([true, false]);
  });

  it('should render single edit ', async () => {
    mockHelper.isDevice = true;
    const props = {
      show: true,
      defaultShell: { level: '01', original: { level: '01' } },
      onClose: jest.fn()
    } as any;

    const wrapper = await renderComponent(props, []);

    expect(
      wrapper.queryByText(
        'txt_config_manage_adjustments_system_edit_shells_title'
      )
    ).toBeInTheDocument();

    const closeBtn = queryByClass(document.body, /icon icon-close/);
    userEvent.click(closeBtn!);
    expect(props.onClose).toHaveBeenCalled();
    mockHelper.isDevice = false;
  });

  it('should render single edit ', async () => {
    const props = {
      show: true,
      defaultShell: { level: '01', original: { level: '01' } },
      levels: levels,
      onClose: jest.fn()
    } as any;

    const wrapper = await renderComponent(props, [
      { name: 'abc', level: '01' }
    ]);

    expect(
      wrapper.queryByText(
        'txt_config_manage_adjustments_system_edit_shells_title'
      )
    ).toBeInTheDocument();

    const checkbox1 = wrapper.getAllByRole('checkbox')[0];
    userEvent.click(checkbox1);
    userEvent.click(wrapper.getAllByRole('checkbox')[1]);
    expect(wrapper.queryByText('txt_new_level')).toBeInTheDocument();

    userEvent.click(wrapper.getByText('txt_new_level'));
    expect(wrapper.queryByText('Manager')).toBeInTheDocument();
    act(() => {
      userEvent.click(screen.getByText('Manager'));
    });
    expect(
      wrapper.queryByText(
        'txt_config_manage_adjustments_system_warning_message_1'
      )
    ).toBeInTheDocument();

    userEvent.click(screen.getByText('txt_save'));
    expect(props.onClose).toHaveBeenCalledWith(true, {
      ...props.defaultShell,
      level: '03',
      supervisorShell: ''
    });
  });

  it('should show warning select supervisor  ', async () => {
    const props = {
      show: true,
      defaultShell: {
        level: '03',
        supervisorShell: '',
        original: { level: '01', supervisorShell: 'abc' }
      },
      levels: levels,
      onClose: jest.fn()
    } as any;

    const wrapper = await renderComponent(props, [
      { name: 'abc', level: '03' }
    ]);

    expect(
      wrapper.queryByText(
        'txt_config_manage_adjustments_system_edit_shells_title'
      )
    ).toBeInTheDocument();

    userEvent.click(wrapper.getByText('txt_new_level'));
    act(() => {
      userEvent.click(screen.getAllByText('Operator')[1]);
    });
    expect(
      wrapper.queryByText(
        'txt_config_manage_adjustments_system_warning_message_2'
      )
    ).toBeInTheDocument();
  });

  it('should show warning select supervisor  ', async () => {
    const props = {
      show: true,
      defaultShell: {
        level: '03',
        supervisorShell: '',
        original: { level: '01', supervisorShell: 'abc2' }
      },
      levels: levels,
      onClose: jest.fn()
    } as any;

    const wrapper = await renderComponent(props, [
      { name: 'abc', level: '03' },
      { name: 'abc2', level: '03' }
    ]);

    expect(
      wrapper.queryByText(
        'txt_config_manage_adjustments_system_edit_shells_title'
      )
    ).toBeInTheDocument();

    userEvent.click(wrapper.getByText('txt_new_level'));
    act(() => {
      userEvent.click(screen.getAllByText('Operator')[1]);
    });
    expect(
      wrapper.queryByText(
        'txt_config_manage_adjustments_system_warning_message_2'
      )
    ).toBeInTheDocument();

    userEvent.click(wrapper.getAllByRole('checkbox')[1]);
    userEvent.click(wrapper.getByText('txt_new_supervisor'));
    act(() => {
      userEvent.click(screen.getAllByText('abc2')[1]);
    });

    userEvent.click(screen.getByText('txt_save'));
    expect(props.onClose).toHaveBeenCalledWith(true, {
      ...props.defaultShell,
      level: '01',
      supervisorShell: 'abc2'
    });
  });

  it('should show warning select supervisor  ', async () => {
    const props = {
      show: true,
      defaultShell: {
        level: '03',
        supervisorShell: '',
        original: { level: '03', supervisorShell: '' }
      },
      levels: levels,
      onClose: jest.fn()
    } as any;

    const wrapper = await renderComponent(props, [
      { name: 'abc', level: '03' }
    ]);

    expect(
      wrapper.queryByText(
        'txt_config_manage_adjustments_system_edit_shells_title'
      )
    ).toBeInTheDocument();

    userEvent.click(wrapper.getAllByRole('checkbox')[0]);
    userEvent.click(wrapper.getAllByRole('checkbox')[0]);
    userEvent.click(wrapper.getAllByRole('checkbox')[0]);

    userEvent.click(wrapper.getByText('txt_new_level'));
    act(() => {
      userEvent.click(screen.getByText('Operator'));
    });
    expect(
      wrapper.queryByText(
        'txt_config_manage_adjustments_system_warning_message_2'
      )
    ).toBeInTheDocument();

    userEvent.click(wrapper.getAllByRole('checkbox')[1]);
    userEvent.click(wrapper.getAllByRole('checkbox')[1]);
    userEvent.click(wrapper.getAllByRole('checkbox')[1]);
    expect(wrapper.queryByText('txt_new_supervisor')).toBeInTheDocument();
  });

  it('should show warning select supervisor  ', async () => {
    const props = {
      show: true,
      defaultShell: {
        level: '00',
        supervisorShell: 'abc',
        original: { level: '03', supervisorShell: '' }
      },
      levels: levels,
      onClose: jest.fn()
    } as any;

    const wrapper = await renderComponent(props, [
      { name: 'abc', level: '00' }
    ]);

    expect(
      wrapper.queryByText(
        'txt_config_manage_adjustments_system_edit_shells_title'
      )
    ).toBeInTheDocument();

    userEvent.click(screen.getByText('txt_save'));
    expect(props.onClose).toHaveBeenCalled();

    userEvent.click(wrapper.getByText('txt_new_level'));
    act(() => {
      userEvent.click(screen.getByText('Operator'));
    });
    expect(
      wrapper.queryByText(
        'txt_config_manage_adjustments_system_warning_message_2'
      )
    ).toBeInTheDocument();
  });

  it('edit supervisor only ', async () => {
    const props = {
      show: true,
      defaultShell: {
        level: '00',
        supervisorShell: '',
        original: { level: '00', supervisorShell: '' }
      },
      levels: levels,
      onClose: jest.fn()
    } as any;

    const wrapper = await renderComponent(props, [
      { name: 'abc', level: '00' },
      { name: 'abc1', level: '01' }
    ]);

    expect(
      wrapper.queryByText(
        'txt_config_manage_adjustments_system_edit_shells_title'
      )
    ).toBeInTheDocument();

    userEvent.click(wrapper.getAllByRole('checkbox')[1]);
    userEvent.click(wrapper.getByText('txt_new_supervisor'));
    act(() => {
      userEvent.click(screen.getByText('abc1'));
    });
    userEvent.click(screen.getByText('txt_save'));
    expect(props.onClose).toHaveBeenCalledWith(true, {
      ...props.defaultShell,
      level: undefined,
      supervisorShell: 'abc1'
    });
  });

  it('should render bulk edit', async () => {
    const props = {
      show: true,
      shells: [{ level: '01', original: { level: '03' } }, { level: '02' }],
      levels: levels,
      onClose: jest.fn()
    } as any;

    const wrapper = await renderComponent(props, [
      { name: 'abc', level: '01' }
    ]);

    expect(
      wrapper.queryByText(
        'txt_config_manage_adjustments_system_bulk_edit_shells_title'
      )
    ).toBeInTheDocument();
    userEvent.click(wrapper.getAllByRole('checkbox')[0]);
    userEvent.click(wrapper.getAllByRole('checkbox')[1]);
  });
});
