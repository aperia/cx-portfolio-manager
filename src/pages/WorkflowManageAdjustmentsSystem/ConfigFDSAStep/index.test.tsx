import { RenderResult } from '@testing-library/react';
import { IShell } from 'app/types';
import {
  mockThunkAction,
  mockUseModalRegistryFnc,
  renderWithMockStore
} from 'app/utils';
import { STATUS } from 'pages/WorkflowManageOCSShells/ConfigureParameters/constants';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import React from 'react';
import ConfigFDSAStep, { FormConfigFDSA } from './index';
import { ShellLevel } from './type';

const mockUseModalRegistry = mockUseModalRegistryFnc();
const mockWorkflowSetup = mockThunkAction(actionsWorkflowSetup);

const renderComponent = async (
  props: any,
  workflowSetup?: any
): Promise<RenderResult> => {
  return await renderWithMockStore(<ConfigFDSAStep {...props} />, {
    initialState: { workflowSetup }
  });
};
describe('ManageAdjustmentsSystemWorkflow > ConfigFDSAStep > Index', () => {
  beforeEach(() => {
    mockUseModalRegistry.mockReturnValue([true, false]);
  });
  it('Should render ', async () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: {},
      setFormValues: mockFn
    } as WorkflowSetupProps<FormConfigFDSA>;
    mockWorkflowSetup('getShellList', {
      match: true,
      payload: { payload: [{}] }
    });
    const wrapper = await renderComponent(props);
    props.formValues = {
      shells: [
        {
          rowId: '1',
          shellName: 'shellName',
          level: '01',
          departmentId: 'id1',
          supervisorShell: 'abc',
          status: STATUS.NotUpdated
        } as IShell
      ]
    };
    wrapper.rerender(<ConfigFDSAStep {...props} />);
    expect(wrapper.queryByText('txt_shell_list')).toBeInTheDocument();
  });

  it('Should render with shells', async () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: {
        shells: [
          {
            rowId: '1',
            shellName: 'shellName',
            level: '01',
            departmentId: 'id1',
            supervisorShell: 'abc',
            status: STATUS.NotUpdated
          } as IShell
        ]
      },
      setFormValues: mockFn
    } as WorkflowSetupProps<FormConfigFDSA>;
    mockWorkflowSetup('getShellList', {
      match: true,
      payload: { payload: [{}] }
    });
    const wrapper = await renderComponent(props);
    props.formValues = {
      shells: [
        {
          rowId: '1',
          shellName: 'shellName',
          level: '01',
          departmentId: 'id1',
          supervisorShell: 'abc',
          status: STATUS.NotUpdated
        } as IShell
      ]
    };
    wrapper.rerender(<ConfigFDSAStep {...props} />);
    expect(wrapper.queryByText('txt_shell_list')).toBeInTheDocument();
  });

  it('Should render which is supervisorShell ', async () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: {},
      setFormValues: mockFn
    } as WorkflowSetupProps<FormConfigFDSA>;
    mockWorkflowSetup('getShellList', {
      match: true,
      payload: { payload: [{ level: ShellLevel.Manager }] }
    });
    const wrapper = await renderComponent(props);
    props.formValues = {
      shells: [
        {
          rowId: '1',
          shellName: 'shellName',
          level: '01',
          departmentId: 'id1',
          supervisorShell: 'abc',
          status: STATUS.NotUpdated
        } as IShell
      ]
    };
    wrapper.rerender(<ConfigFDSAStep {...props} />);
    expect(wrapper.queryByText('txt_shell_list')).toBeInTheDocument();
  });

  it('Should render without success ', async () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: {},
      setFormValues: mockFn
    } as WorkflowSetupProps<FormConfigFDSA>;
    mockWorkflowSetup('getShellList', {
      match: false,
      payload: { payload: [] }
    });
    const wrapper = await renderComponent(props);
    props.formValues = {
      shells: [
        {
          rowId: '1',
          shellName: 'shellName',
          level: '01',
          departmentId: 'id1',
          supervisorShell: 'abc',
          status: STATUS.NotUpdated
        } as IShell
      ]
    };
    wrapper.rerender(<ConfigFDSAStep {...props} />);
    expect(wrapper.queryByText('txt_shell_list')).toBeInTheDocument();
  });
});
