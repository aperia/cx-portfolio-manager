import { fireEvent } from '@testing-library/react';
import { renderWithMockStore } from 'app/utils';
import React from 'react';
import GeneralInformation, {
  useGeneralInformation,
  useShellGeneralInformation
} from './GeneralInformation';
import { GeneralInformationModel, ShellGeneralInformationModel } from './type';

type FactoryProps<T = 'queueOwner' | 'shell'> = {
  data?: T extends 'queueOwner'
    ? GeneralInformationModel
    : ShellGeneralInformationModel;
  preview?: boolean;
  type: T;
};

const Component = ({ data, preview, type }: FactoryProps) => {
  const hook =
    type === 'queueOwner' ? useGeneralInformation : useShellGeneralInformation;
  const props = hook(data as never);
  return <GeneralInformation {...props} preview={preview} />;
};

const factory = async ({ data, preview, type }: FactoryProps) => {
  const { getByText, queryByRole, baseElement } = await renderWithMockStore(
    <Component data={data} preview={preview} type={type} />
  );
  const searchBox = () => queryByRole('textbox');

  const searchSomeThing = (txt: string) => {
    fireEvent.change(searchBox()!, { target: { value: txt } });
    fireEvent.click(
      baseElement.querySelector('i[class="icon icon-search"]') as Element
    );
  };

  const clearAndReset = () => getByText('txt_clear_and_reset');

  const triggerClearAndReset = () => {
    fireEvent.click(clearAndReset());
  };
  return {
    noResultFound: () => getByText('txt_no_results_found'),
    grid: () => getByText('txt_green_screen_name'),
    searchSomeThing,
    clearAndReset: () => getByText('txt_clear_and_reset'),
    triggerClearAndReset,
    searchBox
  };
};

describe('test queue owner general information', () => {
  it('should be render queue owner no data', async () => {
    const { noResultFound }: any = await factory({
      type: 'queueOwner',
      data: undefined
    });
    expect(noResultFound()).toBeInTheDocument();
  });

  it('should be render shell no data', async () => {
    const { noResultFound }: any = await factory({
      type: 'shell',
      data: undefined
    });
    expect(noResultFound()).toBeInTheDocument();
  });

  it('should be render queueOwner grid', async () => {
    const { grid }: any = await factory({
      type: 'queueOwner',
      data: {
        managerId: '66666266',
        queueOwner: 'Aartsen',
        queueOwnerId: '67471013',
        terminalGroupId: '68398403'
      }
    });
    expect(grid()).toBeInTheDocument();
  });

  it('should be render shell grid', async () => {
    const { grid }: any = await factory({
      type: 'shell',
      data: {
        departmentId: '0020',
        managerOperatorId: 'MOID01',
        operatorId: 'OID04',
        profile: 'PROFILE2',
        supervisorOperatorId: '0032',
        terminalGroupId: '0032'
      }
    });
    expect(grid()).toBeInTheDocument();
  });

  it('should be search', async () => {
    const { clearAndReset, searchSomeThing, triggerClearAndReset, grid }: any =
      await factory({
        type: 'queueOwner',
        data: {
          managerId: '66666266',
          queueOwner: 'Aartsen',
          queueOwnerId: '67471013',
          terminalGroupId: '68398403'
        }
      });
    searchSomeThing('not match');
    expect(clearAndReset()).toBeInTheDocument();
    triggerClearAndReset();
    expect(grid()).toBeInTheDocument();
  });

  it('should be show as preview', async () => {
    const { searchBox }: any = await factory({
      type: 'queueOwner',
      preview: true,
      data: {
        managerId: '66666266',
        queueOwner: 'Aartsen',
        queueOwnerId: '67471013',
        terminalGroupId: '68398403'
      }
    });
    expect(searchBox()).not.toBeInTheDocument();
  });
});
