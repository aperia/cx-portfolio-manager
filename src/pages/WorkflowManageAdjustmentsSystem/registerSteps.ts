import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { stepRegistry } from '../_commons/WorkflowSetup/registries';
import ConfigFDSAStep from './ConfigFDSAStep';
import ConfigFLSADJStep from './ConfigFLSADJStep';
import ConfigJFSStep from './ConfigJFSStep';
import ConfigJQAStep from './ConfigJQAStep';
import GettingStartStep from './GettingStartStep';

stepRegistry.registerStep(
  'GetStartedManageAdjustmentsSystem',
  GettingStartStep,
  [
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_ADJUSTMENT_SYSTEM__GET_STARTED__EXISTED_WORKFLOW,
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_ADJUSTMENT_SYSTEM__GET_STARTED__EXISTED_WORKFLOW__STUCK
  ]
);
stepRegistry.registerStep('ConfigJFSManageAdjustmentsSystem', ConfigJFSStep, [
  UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_AUTOMATED_ADJUSTMENTS_JFS
]);
stepRegistry.registerStep(
  'ConfigFLSADJManageAdjustmentsSystem',
  ConfigFLSADJStep,
  [UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_AUTOMATED_ADJUSTMENTS_FLS_ADJ]
);
stepRegistry.registerStep('ConfigJQAManageAdjustmentsSystem', ConfigJQAStep, [
  UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_AUTOMATED_ADJUSTMENTS_JQA
]);
stepRegistry.registerStep('ConfigFDSAManageAdjustmentsSystem', ConfigFDSAStep, [
  UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_AUTOMATED_ADJUSTMENTS_FDSA
]);
