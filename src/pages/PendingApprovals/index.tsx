import { SimpleBar, useTranslation } from 'app/_libraries/_dls';
import PendingApprovalList from 'pages/_commons/PendingApprovalList';
import React from 'react';

interface IProps {}
const PendingApprovals: React.FC<IProps> = () => {
  const { t } = useTranslation();

  return (
    <SimpleBar>
      <PendingApprovalList
        containerClassName={'p-24'}
        title={t('txt_pending_approval_queue')}
        isFullPage={true}
      />
    </SimpleBar>
  );
};

export default PendingApprovals;
