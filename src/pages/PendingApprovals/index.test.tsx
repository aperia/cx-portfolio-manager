import { renderComponent } from 'app/utils';
import React from 'react';
import PendingApprovals from './';

jest.mock('app/_libraries/_dls', () => ({
  SimpleBar: ({ children }: any) => <div>{children}</div>,
  useTranslation: () => ({
    t: (key: string) => key
  })
}));
jest.mock('pages/_commons/PendingApprovalList', () => ({
  __esModule: true,
  default: ({ title }: any) => <div>{title}</div>
}));

const testId = 'pending-approvals';

describe('Page > Pending Approvals', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  it('Should render Pending Approvals UI', async () => {
    const { getByText } = await renderComponent(testId, <PendingApprovals />);

    expect(getByText('txt_pending_approval_queue')).toBeInTheDocument();
  });
});
