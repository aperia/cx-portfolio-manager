import { renderComponent } from 'app/utils';
import * as PortfolioSelector from 'pages/_commons/redux/Portfolio/select-hooks';
import React from 'react';
import * as ReactRedux from 'react-redux';
import MyPortfoliosDetail from './';

jest.mock('./StatisticDetails', () => ({
  __esModule: true,
  default: () => <div>Statistic Details</div>
}));

jest.mock('./Snapshot', () => ({
  __esModule: true,
  default: () => <div>Snapshot section</div>
}));

jest.mock('react-router-dom', () => ({
  useParams: () => '10000001'
}));

const mockSelectPortfoliosDetail = jest.spyOn(
  PortfolioSelector,
  'useSelectPortfoliosDetail'
);
const mockDispatch = jest.spyOn(ReactRedux, 'useDispatch');
const mockSelector = jest.spyOn(ReactRedux, 'useSelector');

const ChangeData: any = {
  id: '1',
  portfolioCriteria: [],
  PortfolioName: 'abc'
};

const testId = 'My Portfolios Detail';
describe('Page > My Portfolios detail', () => {
  const dispatchFn = jest.fn();

  beforeEach(() => {
    mockDispatch.mockReturnValue(dispatchFn);
    mockSelector.mockImplementation(selector =>
      selector({ portfolio: { portfoliosDetails: { loading: false } } })
    );
    mockSelectPortfoliosDetail.mockReturnValue({
      data: ChangeData,
      loading: false,
      error: false
    });
  });
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render Portfolios Detail UI, change type is pricing', async () => {
    const { getByText } = await renderComponent(testId, <MyPortfoliosDetail />);
    expect(getByText('Snapshot section')).toBeInTheDocument();
    expect(getByText('Statistic Details')).toBeInTheDocument();
  });
});
