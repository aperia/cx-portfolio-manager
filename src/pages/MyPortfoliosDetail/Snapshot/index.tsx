import FailedApiReload from 'app/components/FailedApiReload';
import {
  actionsPortfolio,
  useSelectPortfoliosDetail
} from 'pages/_commons/redux/Portfolio';
import React, { useCallback, useMemo } from 'react';
import { useDispatch } from 'react-redux';
import BreadCrumbs from '../BreadCrumbs';
import { MyPortfoliosDetailField } from '../MyPortfoliosDetailField';
import GroupTextPortfolios from './GroupTextPortfolios';

interface IMyPortfoliosSnapshotProps {
  portfolios: IPortfoliosDetail;
}

const MyPortfoliosSnapshot: React.FC<IMyPortfoliosSnapshotProps> = ({
  portfolios
}) => {
  const { error } = useSelectPortfoliosDetail();
  const dispatch = useDispatch();

  const portfoliosFieldView = useMemo(() => {
    const arrFieldView = [...(portfolios.portfolioCriteria || [])].sort(
      (a, b) => {
        if (a.fieldName.includes('Sys') && b.fieldName.includes('Sys')) {
          if (a.fieldName === MyPortfoliosDetailField.System_Prin_Agents) {
            return 1;
          }
          if (b.fieldName === MyPortfoliosDetailField.System_Prin_Agents) {
            return -1;
          }
          if (
            a.fieldName.toLocaleUpperCase() > b.fieldName.toLocaleUpperCase()
          ) {
            return 1;
          }
          if (
            a.fieldName.toLocaleUpperCase() < b.fieldName.toLocaleUpperCase()
          ) {
            return -1;
          }
        }
        if (a.fieldName.includes('Sys')) {
          return -1;
        }
        if (b.fieldName.includes('Sys')) {
          return 1;
        }

        if (
          a.fieldName.includes('Miscellaneous Fields') &&
          b.fieldName.includes('Miscellaneous Fields')
        ) {
          if (
            a.fieldName.toLocaleUpperCase() > b.fieldName.toLocaleUpperCase()
          ) {
            return 1;
          }
          if (
            a.fieldName.toLocaleUpperCase() < b.fieldName.toLocaleUpperCase()
          ) {
            return -1;
          }
        }
        if (a.fieldName.includes('Miscellaneous Fields')) {
          return 1;
        }
        if (b.fieldName.includes('Miscellaneous Fields')) {
          return -1;
        }

        if (a.fieldName.toLocaleUpperCase() > b.fieldName.toLocaleUpperCase()) {
          return 1;
        }

        if (a.fieldName.toLocaleUpperCase() < b.fieldName.toLocaleUpperCase()) {
          return -1;
        }

        return 0;
      }
    );

    return arrFieldView;
  }, [portfolios]);

  const renderTooltipView = useCallback(key => {
    switch (key) {
      case MyPortfoliosDetailField.System:
        return 'Include all Prin/Agents belonging to this System.';
      case MyPortfoliosDetailField.System_Prin:
        return 'Include all Agents belonging to this System/Prin.';
      default:
        return '';
    }
  }, []);

  const handleReload = useCallback(() => {
    dispatch(actionsPortfolio.getPortfolioDetail(portfolios.id!));
  }, [dispatch, portfolios]);

  return error ? (
    <FailedApiReload
      id="home-page-portfolio-detail-card-error"
      onReload={handleReload}
      className="mh-320 d-flex flex-column justify-content-center align-items-center w-100 bg-light-l20"
    />
  ) : (
    <div className="bg-light-l20 p-24">
      <div className="row">
        <div className="col-12">
          <BreadCrumbs portfolioName={portfolios.name} />
        </div>
        <div className="col-12">
          <div className="d-flex mt-14">
            {portfolios.icon && (
              <img
                id={`${portfolios.id}__img`}
                src={portfolios.icon}
                alt="visa"
                className="img-visa"
              />
            )}
            <h3 className="ml-16">{portfolios.name}</h3>
          </div>
        </div>
        <div className="col-12">
          <div className="row mt-8">
            {portfoliosFieldView.map(
              (item: IPortfolioCriteria, key: number) => {
                return (
                  <GroupTextPortfolios
                    key={`${item}-${key}-portfolios`}
                    id={`${item}-${key}-portfolios`}
                    value={item.values}
                    label={item.fieldName}
                    textInformationTooltip={renderTooltipView(item.fieldName)}
                  />
                );
              }
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default MyPortfoliosSnapshot;
