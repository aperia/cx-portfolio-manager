import { isDevice } from 'app/helpers';
import { Icon, Popover, Tooltip, TruncateText } from 'app/_libraries/_dls';
import classNames from 'classnames';
import React, { useMemo, useRef, useState } from 'react';

interface IGroupTextPortfoliosProps {
  label: string;
  id: string;
  value: string[];
  textInformationTooltip?: string;
  indexView?: number;
}

const GroupTextPortfolios: React.FC<IGroupTextPortfoliosProps> = ({
  label,
  id,
  value,
  textInformationTooltip,
  indexView = 1
}) => {
  const followRef = useRef<HTMLDivElement | null>(null);
  const [tooltipOpened, setTooltipOpened] = useState(false);

  const valueView = useMemo(() => {
    return {
      view: value.slice(0, indexView).join(' '),
      viewTooltip: value.slice(indexView)
    };
  }, [value, indexView]);

  return (
    <div
      className={classNames('form-group-static col-4 col-lg-3 w-xl-20')}
      id={id}
    >
      <span id={`${id}__label`} className="form-group-static__label">
        <TruncateText resizable title={label}>
          {label}
        </TruncateText>
      </span>
      <div
        id={`${id}__text`}
        className={classNames(
          'form-group-static__text flex-fill d-flex align-items-center'
        )}
      >
        <div className="flex-fill d-flex" ref={followRef}>
          <TruncateText resizable title={valueView.view} followRef={followRef}>
            {valueView.view.toString()}
          </TruncateText>
          {!!valueView.viewTooltip.length && (
            <Popover
              placement="top"
              size="auto"
              element={
                <div>
                  {valueView.viewTooltip.map(i => (
                    <>
                      <span>{i}</span>
                      <br />
                    </>
                  ))}
                </div>
              }
              opened={tooltipOpened}
              onVisibilityChange={setTooltipOpened}
            >
              <a
                className="dls-button-link-story color-blue-d12 ml-4 cursor-pointer link link-portfolio"
                onClick={() => setTooltipOpened(opened => !opened)}
                tabIndex={0}
                href="javascript:function() {return false;}"
              >{`+${valueView.viewTooltip.length}`}</a>
            </Popover>
          )}
          {!!textInformationTooltip && (
            <Tooltip
              element={textInformationTooltip}
              variant="default"
              opened={isDevice ? false : undefined}
              triggerClassName="d-flex ml-8"
            >
              <Icon name="information" size="5x" className="color-grey-l08" />
            </Tooltip>
          )}
        </div>
      </div>
    </div>
  );
};

export default GroupTextPortfolios;
