import userEvent from '@testing-library/user-event';
import { renderComponent } from 'app/utils';
import * as PortfolioSelector from 'pages/_commons/redux/Portfolio/select-hooks';
import React from 'react';
import * as ReactRedux from 'react-redux';
import Snapshot from './';

const mockSelectPortfoliosDetail = jest.spyOn(
  PortfolioSelector,
  'useSelectPortfoliosDetail'
);
const mockDispatch = jest.spyOn(ReactRedux, 'useDispatch');
const mockSelector = jest.spyOn(ReactRedux, 'useSelector');

jest.mock('../BreadCrumbs', () => ({
  __esModule: true,
  default: () => <div>BreadCrumbs_component</div>
}));

const PortfoliosData: any = {
  id: '1',
  portfolioCriteria: [
    {
      fieldName: 'Sys/Prin/Agents',
      values: ['1231 1231', '12312']
    },
    {
      fieldName: 'System/Prin',
      values: ['1231 1231', '12312']
    },
    {
      fieldName: 'System',
      values: ['1231 1231', '12312']
    },
    {
      fieldName: 'Sys/Prin/Agents',
      values: ['1231 1231', '12312']
    },
    {
      fieldName: 'System/Prin',
      values: ['1231 1231', '12312']
    },
    {
      fieldName: 'System',
      values: ['1231 1231', '12312']
    },
    {
      fieldName: 'BIN',
      values: []
    },
    {
      fieldName: 'BIN',
      values: []
    },
    {
      fieldName: 'Miscellaneous Fields 20-30-TX',
      values: ['12312']
    },
    {
      fieldName: 'System',
      values: ['1231 1231', '12312']
    },
    {
      fieldName: 'Miscellaneous Fields 1-10-TX',
      values: ['12312']
    },
    {
      fieldName: 'Miscellaneous Fields 20-30-TX',
      values: ['12312']
    },
    {
      fieldName: 'Miscellaneous Fields 10-15-TX',
      values: ['12312']
    },
    {
      fieldName: 'Company ID',
      values: []
    },
    {
      fieldName: 'BIN',
      values: []
    },
    {
      fieldName: 'Sys/Prin/Agents',
      values: ['1231 1231', '12312']
    }
  ],
  portfolioName: 'abc',
  portfolioIcon: 'abcs',
  icon: 'abc'
};

const testId = 'My Portfolios Detail Snapshot';
describe('Page > My Portfolios detail', () => {
  const dispatchFn = jest.fn();

  beforeEach(() => {
    mockDispatch.mockReturnValue(dispatchFn);
    mockSelector.mockImplementation(selector =>
      selector({ portfolio: { portfoliosDetails: { loading: false } } })
    );
    mockSelectPortfoliosDetail.mockReturnValue({
      data: PortfoliosData,
      loading: false,
      error: false
    });
  });
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render Portfolios Detail UI, change type is pricing', async () => {
    const { getByText } = await renderComponent(
      testId,
      <Snapshot portfolios={PortfoliosData} />
    );
    expect(getByText('BreadCrumbs_component')).toBeInTheDocument();
  });

  it('should render with failed api', async () => {
    mockSelectPortfoliosDetail.mockReturnValue({
      data: PortfoliosData,
      loading: false,
      error: true
    });

    const wrapper = await renderComponent(testId, <Snapshot portfolios={{}} />);
    expect(
      wrapper.getByText('Data load unsuccessful. Click Reload to try again.')
    ).toBeInTheDocument();
    userEvent.click(wrapper.getByText('Reload'));
  });
});
