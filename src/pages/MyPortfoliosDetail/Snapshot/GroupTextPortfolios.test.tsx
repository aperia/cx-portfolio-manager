import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { isDevice } from 'app/helpers';
import React from 'react';
import GroupTextPortfolios from './GroupTextPortfolios';

jest.mock('app/helpers', () => ({
  isDevice: jest.fn().mockReturnValue(true)
}));

describe('GroupTextPortfolios', () => {
  it('Should render GroupTextPortfolios is not textInformationTooltip', () => {
    const { getByText } = render(
      <GroupTextPortfolios
        id={'GroupTextPortfolios'}
        value={['abc']}
        label="bcd"
      />
    );
    expect(getByText('abc')).toBeInTheDocument();
  });
  it('Should render GroupTextPortfolios is textInformationTooltip', () => {
    const { getByText } = render(
      <GroupTextPortfolios
        id={'GroupTextPortfolios'}
        value={['abc', 'bc']}
        label="bcd"
        textInformationTooltip="abc"
      />
    );
    userEvent.click(getByText('+1'));
    expect(getByText('abc')).toBeInTheDocument();
  });
  it('Should render GroupTextPortfolios is textInformationTooltip and not device', () => {
    isDevice.mockReturnValue(false);
    const { getByText } = render(
      <GroupTextPortfolios
        id={'GroupTextPortfolios'}
        value={['abc', 'bc']}
        label="bcd"
        textInformationTooltip="abc"
      />
    );
    userEvent.click(getByText('+1'));
    expect(getByText('abc')).toBeInTheDocument();
  });
});
