import { render } from '@testing-library/react';
import React from 'react';
import BreadCrumbs from './index';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

jest.mock('app/components/BreadCrumb', () => ({
  __esModule: true,
  default: ({ items }: any) => {
    const renderItems = () => {
      return (
        <div>
          {items.map((x: any) => {
            return (
              <div
                key={x?.id}
                title={typeof x?.title == typeof '' ? x?.title : ''}
              >
                {x?.id}
              </div>
            );
          })}
        </div>
      );
    };
    return (
      <div>
        <div>BreadCrumb_component</div>
        {renderItems()}
      </div>
    );
  }
}));

describe('Test ChangeDetail > BreadCrumbs', () => {
  it('Should render BreadCrumbs with change name is not empty', () => {
    const wrapper = render(<BreadCrumbs portfolioName={'new_change_name'} />);
    expect(
      wrapper.baseElement.querySelector('div[title="new_change_name"]')
    ).toBeInTheDocument();
  });

  it('Should render BreadCrumbs with change name is empty', () => {
    const props = undefined as any;
    const wrapper = render(<BreadCrumbs {...props} />);
    expect(
      wrapper.baseElement.querySelector('div[title=""]')
    ).toBeInTheDocument();
  });
});
