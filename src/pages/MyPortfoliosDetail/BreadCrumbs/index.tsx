import BreadCrumb from 'app/components/BreadCrumb';
import { BASE_URL, MY_PORTFOLIOS_URL } from 'app/constants/links';
import { useTranslation } from 'app/_libraries/_dls';
import React, { useMemo } from 'react';
import { Link } from 'react-router-dom';

interface IProps {
  portfolioName?: string;
}
const BreadCrumbs: React.FC<IProps> = ({ portfolioName = '' }) => {
  const { t } = useTranslation();
  const breadCrmbItems = useMemo(
    () => [
      { id: 'home', title: <Link to={BASE_URL}>Home</Link> },
      {
        id: 'my-portfolios',
        title: <Link to={MY_PORTFOLIOS_URL}>{t('txt_my_portfolios')}</Link>
      },
      { id: 'my-portfolios-detail', title: `${portfolioName}` }
    ],
    [portfolioName, t]
  );

  return <BreadCrumb items={breadCrmbItems} width={992} />;
};

export default BreadCrumbs;
