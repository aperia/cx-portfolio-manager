import { SimpleBar } from 'app/_libraries/_dls';
import classNames from 'classnames';
import {
  actionsPortfolio,
  useSelectPortfoliosDetail
} from 'pages/_commons/redux/Portfolio';
import React, { useEffect, useMemo } from 'react';
import { useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';
import Snapshot from './Snapshot';
import StatisticDetails from './StatisticDetails';

export interface IURLParams {
  id: string;
}
interface IMyPortfoliosDetailProps {}

const MyPortfoliosDetail: React.FC<IMyPortfoliosDetailProps> = () => {
  const { id: portfolioId } = useParams<IURLParams>();
  const dispatch = useDispatch();
  const { data: dataPortfolio, loading } = useSelectPortfoliosDetail();

  useEffect(() => {
    dispatch(actionsPortfolio.getPortfolioDetail(portfolioId));
  }, [dispatch, portfolioId]);

  const portfoliosDetail = useMemo(() => {
    return {
      ...dataPortfolio
    };
  }, [dataPortfolio]);

  return (
    <SimpleBar
      className={classNames({
        loading: loading
      })}
    >
      <Snapshot portfolios={portfoliosDetail} />
      <div className="p-24 border-top">
        <StatisticDetails
          kpiList={portfoliosDetail?.portfolioStatistic?.kpiList || []}
        />
      </div>
    </SimpleBar>
  );
};

export default MyPortfoliosDetail;
