import { formatTime } from 'app/helpers';
import {
  Button,
  DropdownButton,
  DropdownButtonSelectEvent,
  Icon,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import LineChart from 'app/_libraries/_dls/components/charts/LineChart';
import { ChartConfiguration } from 'chart.js';
import _ from 'lodash';
import isEmpty from 'lodash.isempty';
import isUndefined from 'lodash.isundefined';
import {
  actionsPortfolio,
  selectKPIData,
  useKpiIdSelector
} from 'pages/_commons/redux/Portfolio';
import React, { useEffect, useMemo, useState } from 'react';
import { useDispatch } from 'react-redux';
import { ADD_BUTTONS, DAYS_PREVIOUSLY, PERIOD } from './constants';
import {
  mappingColorItem,
  mappingDaysPreviously,
  mappingPeriod,
  weeklyLabel
} from './mapping';
import RemovePopupModal from './removePopupModal';

export interface KpiChartProps {
  kpiId: string;
  onDeletedKpi: (kpiId: string) => void;
}

const KpiChart: React.FC<KpiChartProps> = ({ kpiId, onDeletedKpi }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [isOpenConfirmRemove, setIsOpenConfirmRemove] =
    useState<boolean>(false);

  const { data, loading } = useKpiIdSelector(selectKPIData, kpiId);

  useEffect(() => {
    dispatch(actionsPortfolio.getKPIDetails({ id: parseInt(kpiId) }));
  }, [kpiId, dispatch]);

  const kpiItem = useMemo(() => {
    if (!loading && !isEmpty(data) && !isUndefined(data)) {
      const dataItem = data?.items?.map((item: any, index: number) => {
        const values = item.values.map((x: any) => {
          return parseInt(x.value);
        });

        return {
          backgroundColor: mappingColorItem(index),
          borderColor: mappingColorItem(index),
          borderWidth: 2,
          data: values,
          label: item.name,
          pointBorderColor: '#fff',
          pointBorderWidth: 2,
          pointHoverBackgroundColor: mappingColorItem(index),
          pointHoverBorderColor: '#fff',
          pointHoverBorderWidth: 2,
          pointRadius: 4,
          pointStyle: 'circle'
          // tension: 0.4
        };
      });

      const values = !isUndefined(data?.items) ? data?.items[0]?.values : [];
      const { period, daysPreviously } = data;

      let label: string[] = [];
      if (values.length > 0) {
        switch (period.toUpperCase()) {
          case PERIOD.DAILY:
            label = values.map((x: any) => {
              return formatTime(x.label).dateForLineChart;
            });
            break;

          case PERIOD.WEEKLY:
            switch (daysPreviously.toUpperCase()) {
              case DAYS_PREVIOUSLY.LAST_WEEK:
              case DAYS_PREVIOUSLY.LAST_MONTH:
                label = values.map((x: any) => {
                  return weeklyLabel(x.label, daysPreviously);
                });
                break;

              case DAYS_PREVIOUSLY.LAST_3_MONTHS:
                label = values.map((x: any) => {
                  return weeklyLabel(x.label, daysPreviously);
                });
                break;
            }
            break;

          case PERIOD.MONTHLY:
            switch (daysPreviously.toUpperCase()) {
              case DAYS_PREVIOUSLY.LAST_WEEK:
              case DAYS_PREVIOUSLY.LAST_MONTH:
                label = values.map((x: any) => {
                  const date = formatTime(x.label.split('-')[0].trim());
                  //formatTime(x.label);
                  return date.monthShort;
                });
                break;

              case DAYS_PREVIOUSLY.LAST_3_MONTHS:
                label = values.map((x: any) => {
                  const date = formatTime(x.label.split('-')[0].trim());
                  return date.monthShortYear;
                });
                break;
            }
            break;
        }
      }

      const info = {
        name: data.name,
        kpi: data.kpi,
        subjectArea: data.subjectArea,
        submetric: data.submetric,
        period: mappingPeriod(data.period.toUpperCase()),
        daysPreviously: mappingDaysPreviously(data.daysPreviously.toUpperCase())
      };

      const dates = !isUndefined(data?.items)
        ? data.items[0].values.map((x: any) => {
            return x.label;
          })
        : [];

      return {
        dates,
        labels: label,
        datasets: _.uniq(dataItem),
        info: info
      };
    }
  }, [data, loading]);

  const [isZoom, setIsZoom] = useState<boolean>(false);
  const onZoom = () => {
    setIsZoom(!isZoom);
    document.body.classList.toggle('body-card-view', !isZoom);
  };

  const iconName = useMemo(() => {
    return !isZoom ? 'fullscreen' : 'exit-fullscreen';
  }, [isZoom]);

  const nameTooltip = useMemo(() => {
    return !isZoom ? 'Fullscreen' : 'Exit Fullscreen';
  }, [isZoom]);

  const placementTooltip = useMemo(() => {
    return !isZoom ? 'top' : 'left';
  }, [isZoom]);

  const onSelectAction = (event: DropdownButtonSelectEvent) => {
    const { value } = event.target.value;

    const datas = {
      name: kpiItem?.info?.name?.trim() || '',
      kpi: kpiItem?.info?.kpi?.trim() || '',
      kpiId: kpiId || '',
      period: mappingPeriod(kpiItem?.info.period || ''),
      daysPreviously: kpiItem?.info.daysPreviously || '',
      mode: value
    };

    switch (value) {
      case 'Edit':
        dispatch(
          actionsPortfolio.setOpenedChoosePopup({
            isChooseOpen: false,
            value: datas,
            isAddOpen: true
          })
        );
        break;

      case 'Remove':
        setIsOpenConfirmRemove(true);
        break;

      case 'Export':
        break;
    }
  };

  const dropdownListItem = useMemo(() => {
    return ADD_BUTTONS.map(item => (
      <DropdownButton.Item
        key={item.value}
        label={t(item.description)}
        value={item}
      />
    ));
  }, [t]);

  const handleOnRemoveKPI = () => {
    dispatch(actionsPortfolio.deletePortfolioKPI(kpiId));
    onDeletedKpi(kpiId);
    setIsOpenConfirmRemove(false);
  };

  const config: ChartConfiguration = useMemo(() => {
    const isDaily = kpiItem?.info?.period === 'Daily';

    return {
      type: 'line',
      data: kpiItem as any,
      options: {
        responsive: true,
        // maintainAspectRatio: false,
        interaction: {
          intersect: false,
          axis: 'xy',
          mode: 'x'
        },
        plugins: {
          legend: {
            display: false
          },
          title: {
            display: false,
            text: ''
          },
          tooltip: {
            padding: {
              top: 6,
              right: 8,
              bottom: 6,
              left: 8
            },
            bodySpacing: () => 5,
            caretSize: () => 8,
            cornerRadius: 8,
            // caretPadding: combineTooltip ? 0 : 4,
            caretPadding: 4,
            usePointStyle: () => true,
            //yAlign: 'bottom',
            callbacks: {
              footer: item => {
                return item.length === 1
                  ? `Total Revenue: $${item[0].formattedValue}`
                  : '';
              },
              title: item => {
                const dateOrigin = kpiItem?.dates?.[item[0]?.parsed?.x];

                let dateFormat = dateOrigin;
                if (dateOrigin?.includes('-')) {
                  // case week
                  dateFormat =
                    formatTime(dateOrigin.split('-')[0].trim())
                      .dateForLineChart +
                    ' - ' +
                    formatTime(dateOrigin.split('-')[1].trim())
                      .dateForLineChart;
                }

                return `${dateFormat}`;
              },
              label: item => {
                return kpiItem?.datasets.length === 1
                  ? ''
                  : `  ${item.dataset.label}: $${item.formattedValue}`;
              },
              labelColor: item => {
                return {
                  backgroundColor: item.dataset.backgroundColor as string,
                  borderColor: item.dataset.borderColor as string
                };
              },
              labelPointStyle: () => {
                return {
                  pointStyle: 'circle',
                  rotation: 0
                };
              }
            }
          }
        },
        layout: {
          padding: 0
        },
        scales: {
          x: {
            beginAtZero: false,
            grid: {
              display: true,
              borderDash: [1, 10],
              borderColor: '#CCCCD5',
              tickLength: 10,
              offset: !isDaily
            },
            ticks: {
              padding: 10,
              autoSkip: true,
              autoSkipPadding: 10
            },
            offset: true
          },
          y: {
            beginAtZero: true,
            grid: {
              display: true,
              borderDash: [1],
              borderColor: '#CCCCD5',
              tickColor: '#fff',
              drawBorder: false
            }
          }
        }
      },
      plugins: [
        {
          id: 'legend',
          beforeInit: chart => {
            if (chart.config.data.datasets.length < 2) return;

            /* istanbul ignore next */
            {
              const parent = chart.canvas.parentElement!;
              const anchor = parent.querySelector('.dls-line-chart-legend')!;
              document.body.classList.toggle('body-card-view-label', isZoom);

              chart.config.data.datasets.forEach(
                ({ label, backgroundColor }) => {
                  const itemExist: string[] = [];
                  anchor.querySelectorAll('.item').forEach((x: any) => {
                    itemExist.push(x.outerText);
                  });

                  if (!itemExist.includes(label || '')) {
                    const item = document.createElement('div');
                    item.classList.add('item');

                    const circle = document.createElement('span');
                    circle.classList.add('circle');
                    circle.style.backgroundColor = backgroundColor as string;
                    const textE = document.createElement('span');
                    textE.classList.add('text');
                    textE.innerText = label as string;
                    item.appendChild(circle);
                    item.appendChild(textE);

                    anchor.appendChild(item);
                  }
                }
              );
            }
          }
        }
      ]
    };
  }, [kpiItem, isZoom]);

  return (
    <>
      <RemovePopupModal
        id={kpiId}
        show={isOpenConfirmRemove}
        onClose={() => setIsOpenConfirmRemove(false)}
        onRemove={handleOnRemoveKPI}
      />

      <div className="card-view-box">
        <div
          className={`card-view  p-24 overflow-hidden ${
            isZoom ? 'card-view-zoom' : 'card-view-out'
          }`}
        >
          <div className="card-view-header">
            <div className="d-flex align-items-center">
              <h5>{kpiItem?.info.name}</h5>
              <div className="ml-8">
                <Tooltip
                  element={
                    <div>
                      <p>{`Subject Area: ${kpiItem?.info.subjectArea}`}</p>
                      <p>{`KPI: ${kpiItem?.info.kpi}`}</p>
                      <p>{`Period: ${kpiItem?.info.period}`}</p>
                      <p>{`Days Previously: ${kpiItem?.info.daysPreviously}`}</p>
                    </div>
                  }
                  triggerClassName="d-flex "
                >
                  <Icon
                    name="information"
                    className="color-grey-l16"
                    size="5x"
                  />
                </Tooltip>
              </div>
            </div>
            <div className="ml-auto d-flex">
              <Tooltip
                element={nameTooltip}
                placement={placementTooltip}
                triggerClassName="d-flex justify-content-center"
                variant="primary"
              >
                <Button variant="icon-secondary" size="sm" onClick={onZoom}>
                  <Icon name={iconName} />
                </Button>
              </Tooltip>

              <Tooltip
                element="Actions"
                placement={placementTooltip}
                triggerClassName="d-flex justify-content-center ml-8"
                variant="primary"
              >
                <DropdownButton
                  id={'id'}
                  dataTestId={'dataTestId'}
                  popupBaseProps={{
                    popupBaseClassName: 'inside-infobar',
                    placement: 'bottom-start'
                  }}
                  buttonProps={{
                    size: 'sm',
                    children: <Icon name="more-vertical" />,
                    variant: 'icon-secondary'
                  }}
                  onSelect={onSelectAction}
                >
                  {dropdownListItem}
                </DropdownButton>
              </Tooltip>
            </div>
          </div>
          <LineChart
            config={config}
            height={isZoom ? ('100%' as any) : undefined}
          />
        </div>
      </div>
    </>
  );
};

export default KpiChart;
