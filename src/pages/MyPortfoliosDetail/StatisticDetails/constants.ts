export const PERIOD = {
  DAILY: 'DAILY',
  WEEKLY: 'WEEKLY',
  MONTHLY: 'MONTHLY'
};

export const DAYS_PREVIOUSLY = {
  LAST_WEEK: 'LAST_WEEK',
  LAST_MONTH: 'LAST_MONTH',
  LAST_3_MONTHS: 'LAST_3_MONTHS'
};

export const ADD_BUTTONS = [
  {
    value: 'Edit',
    description: 'Edit'
  },
  {
    value: 'Export',
    description: 'Export'
  },
  {
    value: 'Remove',
    description: 'Remove'
  }
];

export const DROP_ITEM_PORTFOLIOS = {
  period: [
    {
      code: 'DAILY',
      text: 'Daily'
    },
    {
      code: 'MONTHLY',
      text: 'Monthly'
    },
    {
      code: 'WEEKLY',
      text: 'Weekly'
    }
  ],
  days_Previously: [
    { code: 'LAST_WEEK', text: 'Last week' },
    { code: 'LAST_MONTH', text: 'Last month' },
    { code: 'LAST_3_MONTHS', text: 'Last 3 months' }
  ]
};
