import { useFormValidations } from 'app/hooks';
import { DropdownList, TextBox, useTranslation } from 'app/_libraries/_dls';
import isEmpty from 'lodash.isempty';
import React, { useEffect, useMemo, useState } from 'react';
import { DROP_ITEM_PORTFOLIOS } from './constants';

export interface IFieldValidate {
  periodCode: string;
  daysPreviouslyCode: string;
}

const formInit = {
  kpiName: '',
  kpiId: '',
  period: { code: undefined, text: undefined },
  days_Previously: { code: undefined, text: undefined }
};

export interface AddKPIProps {
  onValid: (valid: boolean, value: {}, mode: string) => void;
  kpi?: any;
}

const AddKPI: React.FC<AddKPIProps> = ({ onValid, kpi }) => {
  const { t } = useTranslation();
  const [formValue, setFormValue] = useState(formInit);

  const periodItems = DROP_ITEM_PORTFOLIOS.period.map(r => (
    <DropdownList.Item key={r.code} label={r.text} value={r} />
  ));

  const daysPreviouslyItems = DROP_ITEM_PORTFOLIOS.days_Previously.map(r => (
    <DropdownList.Item key={r.code} label={r.text} value={r} />
  ));

  const currentErrors = useMemo(
    () =>
      ({
        periodCode: !formValue.period.code && {
          status: true,
          message: t('txt_required_validation', {
            fieldName: t('txt_field_period')
          })
        },
        daysPreviouslyCode: !formValue.days_Previously.code && {
          status: true,
          message: t('txt_required_validation', {
            fieldName: t('txt_field_days_previously')
          })
        }
      } as Record<keyof IFieldValidate, IFormError>),
    [formValue.period, formValue.days_Previously, t]
  );

  const [, errors, setTouched] =
    useFormValidations<keyof IFieldValidate>(currentErrors);
  const valid =
    !errors.daysPreviouslyCode &&
    !errors.periodCode &&
    formValue?.period?.code !== undefined &&
    formValue?.days_Previously?.code !== undefined;

  useEffect(() => {
    onValid(valid, formValue, kpi?.mode);
  }, [valid, onValid, formValue, kpi]);

  useEffect(() => {
    if (!isEmpty(kpi)) {
      setFormValue({
        kpiId: kpi?.kpiId || '',
        kpiName: kpi?.mode === 'Add' ? '' : kpi?.name,
        period: { code: kpi?.period, text: kpi?.period },
        days_Previously: {
          code: kpi?.daysPreviously,
          text: kpi?.daysPreviously
        }
      });
    }
  }, [kpi]);

  return (
    <>
      <p className="mb-16 color-grey">{`Enter details for ${
        kpi?.name || ''
      }`}</p>
      <div className="mb-16">
        <TextBox
          label={'KPI Name'}
          value={formValue.kpiName}
          onChange={e =>
            setFormValue({ ...formValue, kpiName: e.target.value })
          }
          maxLength={50}
        />
      </div>
      <div className="mb-16">
        <DropdownList
          tabIndex={1}
          id={`id__Period`}
          textField="text"
          label="Period"
          required
          value={formValue.period.text !== undefined && formValue.period}
          onChange={e => setFormValue({ ...formValue, period: e.target.value })}
          onFocus={setTouched('periodCode')}
          onBlur={setTouched('periodCode', true)}
          error={errors.periodCode}
        >
          {periodItems}
        </DropdownList>
      </div>
      <DropdownList
        tabIndex={2}
        id={`id__Days_Previously`}
        textField="text"
        label="Days Previously"
        required
        value={
          formValue.days_Previously.text !== undefined &&
          formValue.days_Previously
        }
        onChange={e =>
          setFormValue({ ...formValue, days_Previously: e.target.value })
        }
        onFocus={setTouched('daysPreviouslyCode')}
        onBlur={setTouched('daysPreviouslyCode', true)}
        error={errors.daysPreviouslyCode}
      >
        {daysPreviouslyItems}
      </DropdownList>
    </>
  );
};

export default AddKPI;
