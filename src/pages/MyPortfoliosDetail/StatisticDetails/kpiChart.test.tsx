import { fireEvent, render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import {
  mockUseDispatchFnc,
  mockUseModalRegistryFnc,
  mockUseSelectorFnc
} from 'app/utils';
import { DropdownButton } from 'app/_libraries/_dls';
import 'app/_libraries/_dls/test-utils/mocks/mockCanvas';
import * as StatisticDetailsSelector from 'pages/_commons/redux/Portfolio/select-hooks';
import React from 'react';
import { ADD_BUTTONS } from './constants';
import KpiChart, { KpiChartProps } from './kpiChart';

const mockUseModalRegistry = mockUseModalRegistryFnc();
const mockUseDispatch = mockUseDispatchFnc();
const mockUseSelector = mockUseSelectorFnc();

const mockUseKpiIdSelector = jest.spyOn(
  StatisticDetailsSelector,
  'useKpiIdSelector'
);

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

jest.mock('chart.js', () => ({
  Chart: {
    register: (list: any) => undefined
  },
  registerables: []
}));

jest.mock(
  'app/_libraries/_dls/components/charts/LineChart',
  () => (props: any) => {
    const { config } = props;

    // fake trigger event
    const tooltip = config.options.plugins.tooltip;

    tooltip.bodySpacing();
    tooltip.caretSize();
    tooltip.usePointStyle();
    const footer = tooltip.callbacks.footer([{ formattedValue: '10' }]);
    expect(footer).toEqual('Total Revenue: $10');

    tooltip.callbacks.footer([]);
    tooltip.callbacks.title([
      { formattedValue: '10', dataset: { data: [10] }, parsed: { x: 0 } }
    ]);
    tooltip.callbacks.label({ formattedValue: '10', dataset: {} });
    tooltip.callbacks.labelColor({ dataset: {} });
    tooltip.callbacks.labelPointStyle();

    config.plugins[0].beforeInit({
      config: {
        data: {
          datasets: [{ label: 'label', backgroundColor: 'red' }]
        }
      }
    });

    return <div>LineChart</div>;
  }
);

const mockOnDeleteKpi = jest.fn();
const RenderComponent = (props: KpiChartProps) => {
  // const { kpiId } = props;

  const dropdownListItem = () => {
    return ADD_BUTTONS.map(item => (
      <DropdownButton.Item
        key={item.value}
        label={item.description}
        value={item}
      />
    ));
  };

  return render(<KpiChart {...props}>{dropdownListItem}</KpiChart>);
};

describe('Test ChangeDetail > StatisticDetails', () => {
  const mockDispatch = jest.fn();

  beforeEach(() => {
    mockUseDispatch.mockReturnValue(mockDispatch);
    mockUseSelector.mockReturnValue(mockDispatch);
    mockUseDispatch.mockImplementation(() => mockDispatch);
    mockUseModalRegistry.mockImplementation(() => [true, false]);
    mockUseKpiIdSelector.mockReturnValue({
      data: {
        id: 137,
        name: 'G9HGCNCW',
        kpi: 'Total Accounts on File',
        subjectArea: 'Accounts',
        submetric: 'Count',
        period: 'Daily',
        daysPreviously: 'Last week',
        items: [
          {
            name: 'Garmin Pay',
            values: [
              {
                label: 'Sunday, Apr 10, 2022',
                value: 102619890
              },
              {
                label: 'Monday, Apr 11, 2022',
                value: 639502458
              },
              {
                label: 'Tuesday, Apr 12, 2022',
                value: 846474462
              },
              {
                label: 'Wednesday, Apr 13, 2022',
                value: 172362851
              },
              {
                label: 'Thursday, Apr 14, 2022',
                value: 932229261
              },
              {
                label: 'Friday, Apr 15, 2022',
                value: 705757230
              },
              {
                label: 'Saturday, Apr 16, 2022',
                value: 430566617
              }
            ]
          },
          {
            name: 'Samsung Pay',
            values: [
              {
                label: 'Sunday, Apr 10, 2022',
                value: 850220649
              },
              {
                label: 'Monday, Apr 11, 2022',
                value: 112885181
              },
              {
                label: 'Tuesday, Apr 12, 2022',
                value: 270508220
              },
              {
                label: 'Wednesday, Apr 13, 2022',
                value: 697580152
              },
              {
                label: 'Thursday, Apr 14, 2022',
                value: 898655570
              },
              {
                label: 'Friday, Apr 15, 2022',
                value: 26437800
              },
              {
                label: 'Saturday, Apr 16, 2022',
                value: 159777672
              }
            ]
          }
        ]
      },
      loading: false
    });
  });
  afterEach(() => {
    mockDispatch.mockClear();
    mockUseDispatch.mockClear();
    mockUseSelector.mockClear();
    mockUseModalRegistry.mockClear();
    jest.clearAllMocks();
  });

  it('Should render KpiChart daily', () => {
    mockUseKpiIdSelector.mockReturnValue({
      data: {
        id: 137,
        name: 'G9HGCNCW',
        kpi: 'Total Accounts on File',
        subjectArea: 'Accounts',
        submetric: 'Count',
        period: 'Daily',
        daysPreviously: 'Last week',
        items: [
          {
            name: 'Garmin Pay',
            values: [
              {
                label: 'Sunday, Apr 10, 2022',
                value: 102619890
              },
              {
                label: 'Monday, Apr 11, 2022',
                value: 639502458
              },
              {
                label: 'Tuesday, Apr 12, 2022',
                value: 846474462
              },
              {
                label: 'Wednesday, Apr 13, 2022',
                value: 172362851
              },
              {
                label: 'Thursday, Apr 14, 2022',
                value: 932229261
              },
              {
                label: 'Friday, Apr 15, 2022',
                value: 705757230
              },
              {
                label: 'Saturday, Apr 16, 2022',
                value: 430566617
              }
            ]
          },
          {
            name: 'Samsung Pay',
            values: [
              {
                label: 'Sunday, Apr 10, 2022',
                value: 850220649
              },
              {
                label: 'Monday, Apr 11, 2022',
                value: 112885181
              },
              {
                label: 'Tuesday, Apr 12, 2022',
                value: 270508220
              },
              {
                label: 'Wednesday, Apr 13, 2022',
                value: 697580152
              },
              {
                label: 'Thursday, Apr 14, 2022',
                value: 898655570
              },
              {
                label: 'Friday, Apr 15, 2022',
                value: 26437800
              },
              {
                label: 'Saturday, Apr 16, 2022',
                value: 159777672
              }
            ]
          }
        ]
      },
      loading: false
    });

    const { getAllByText } = render(
      <KpiChart onDeletedKpi={mockOnDeleteKpi} kpiId={'893070678'} />
    );
    expect(getAllByText('LineChart')).toBeTruthy();
  });

  it('Should render KpiChart Weekly > Last month', () => {
    mockUseKpiIdSelector.mockReturnValue({
      data: {
        id: 196,
        name: 'YOTOIEFN',
        kpi: 'Purchase Active Accounts',
        subjectArea: 'Purchases',
        submetric: '',
        period: 'Weekly',
        daysPreviously: 'Last month',
        items: [
          {
            name: 'Garmin Pay',
            values: [
              {
                label: 'Tuesday, Mar 01, 2022 - Saturday, Mar 05, 2022',
                value: 808344053
              },
              {
                label: 'Sunday, Mar 06, 2022 - Saturday, Mar 12, 2022',
                value: 6784928
              },
              {
                label: 'Sunday, Mar 13, 2022 - Saturday, Mar 19, 2022',
                value: 892835915
              },
              {
                label: 'Sunday, Mar 20, 2022 - Saturday, Mar 26, 2022',
                value: 981257
              },
              {
                label: 'Sunday, Mar 27, 2022 - Thursday, Mar 31, 2022',
                value: 764562898
              }
            ]
          }
        ]
      },
      loading: false
    });

    const { getAllByText } = render(
      <KpiChart onDeletedKpi={mockOnDeleteKpi} kpiId={'893070678'} />
    );
    expect(getAllByText('LineChart')).toBeTruthy();
  });

  it('Should render KpiChart Weekly > Last week', () => {
    mockUseKpiIdSelector.mockReturnValue({
      data: {
        id: 196,
        name: 'YOTOIEFN',
        kpi: 'Purchase Active Accounts',
        subjectArea: 'Purchases',
        submetric: '',
        period: 'Weekly',
        daysPreviously: 'Last week',
        items: [
          {
            name: 'Garmin Pay',
            values: [
              {
                label: 'Tuesday, Mar 01, 2022 - Saturday, Mar 05, 2022',
                value: 808344053
              },
              {
                label: 'Sunday, Mar 06, 2022 - Saturday, Mar 12, 2022',
                value: 6784928
              },
              {
                label: 'Sunday, Mar 13, 2022 - Saturday, Mar 19, 2022',
                value: 892835915
              },
              {
                label: 'Sunday, Mar 20, 2022 - Saturday, Mar 26, 2022',
                value: 981257
              },
              {
                label: 'Sunday, Mar 27, 2022 - Thursday, Mar 31, 2022',
                value: 764562898
              }
            ]
          }
        ]
      },
      loading: false
    });

    const { getAllByText } = render(
      <KpiChart onDeletedKpi={mockOnDeleteKpi} kpiId={'893070678'} />
    );
    expect(getAllByText('LineChart')).toBeTruthy();
  });

  it('Should render KpiChart Weekly > Last 3 months', () => {
    mockUseKpiIdSelector.mockReturnValue({
      data: {
        id: 95,
        name: 'A0DIQL92',
        kpi: 'Plastics',
        subjectArea: 'Accounts',
        submetric: 'Count',
        period: 'Weekly',
        daysPreviously: 'Last 3 months',
        items: [
          {
            name: 'Momo Pay',
            values: [
              {
                label: 'Saturday, Jan 01, 2022 - Saturday, Jan 01, 2022',
                value: 776209956
              },
              {
                label: 'Sunday, Jan 02, 2022 - Saturday, Jan 08, 2022',
                value: 162683297
              },
              {
                label: 'Sunday, Jan 09, 2022 - Saturday, Jan 15, 2022',
                value: 565862935
              },
              {
                label: 'Sunday, Jan 16, 2022 - Saturday, Jan 22, 2022',
                value: 63468425
              },
              {
                label: 'Sunday, Jan 23, 2022 - Saturday, Jan 29, 2022',
                value: 188267642
              },
              {
                label: 'Sunday, Jan 30, 2022 - Saturday, Feb 05, 2022',
                value: 169193815
              },
              {
                label: 'Sunday, Feb 06, 2022 - Saturday, Feb 12, 2022',
                value: 531214089
              },
              {
                label: 'Sunday, Feb 13, 2022 - Saturday, Feb 19, 2022',
                value: 735078534
              },
              {
                label: 'Sunday, Feb 20, 2022 - Saturday, Feb 26, 2022',
                value: 181663921
              },
              {
                label: 'Sunday, Feb 27, 2022 - Saturday, Mar 05, 2022',
                value: 545627525
              },
              {
                label: 'Sunday, Mar 06, 2022 - Saturday, Mar 12, 2022',
                value: 278977983
              },
              {
                label: 'Sunday, Mar 13, 2022 - Saturday, Mar 19, 2022',
                value: 306208713
              },
              {
                label: 'Sunday, Mar 20, 2022 - Saturday, Mar 26, 2022',
                value: 31234609
              },
              {
                label: 'Sunday, Mar 27, 2022 - Thursday, Mar 31, 2022',
                value: 926992298
              }
            ]
          },
          {
            name: 'Google Pay',
            values: [
              {
                label: 'Saturday, Jan 01, 2022 - Saturday, Jan 01, 2022',
                value: 749872747
              },
              {
                label: 'Sunday, Jan 02, 2022 - Saturday, Jan 08, 2022',
                value: 449694843
              },
              {
                label: 'Sunday, Jan 09, 2022 - Saturday, Jan 15, 2022',
                value: 529820937
              },
              {
                label: 'Sunday, Jan 16, 2022 - Saturday, Jan 22, 2022',
                value: 830882393
              },
              {
                label: 'Sunday, Jan 23, 2022 - Saturday, Jan 29, 2022',
                value: 989367297
              },
              {
                label: 'Sunday, Jan 30, 2022 - Saturday, Feb 05, 2022',
                value: 631919491
              },
              {
                label: 'Sunday, Feb 06, 2022 - Saturday, Feb 12, 2022',
                value: 483444585
              },
              {
                label: 'Sunday, Feb 13, 2022 - Saturday, Feb 19, 2022',
                value: 418525389
              },
              {
                label: 'Sunday, Feb 20, 2022 - Saturday, Feb 26, 2022',
                value: 896290507
              },
              {
                label: 'Sunday, Feb 27, 2022 - Saturday, Mar 05, 2022',
                value: 447733053
              },
              {
                label: 'Sunday, Mar 06, 2022 - Saturday, Mar 12, 2022',
                value: 845533839
              },
              {
                label: 'Sunday, Mar 13, 2022 - Saturday, Mar 19, 2022',
                value: 888043717
              },
              {
                label: 'Sunday, Mar 20, 2022 - Saturday, Mar 26, 2022',
                value: 411392379
              },
              {
                label: 'Sunday, Mar 27, 2022 - Thursday, Mar 31, 2022',
                value: 958085664
              }
            ]
          }
        ]
      },
      loading: false
    });

    const { getAllByText } = render(
      <KpiChart onDeletedKpi={mockOnDeleteKpi} kpiId={'893070678'} />
    );
    expect(getAllByText('LineChart')).toBeTruthy();
  });

  it('Should render KpiChart Monthly > Last 3 months', () => {
    mockUseKpiIdSelector.mockReturnValue({
      data: {
        id: 98,
        name: 'CCC05HNM',
        kpi: 'Finance Charge %',
        subjectArea: 'Revenue',
        submetric: 'Rate',
        period: 'Monthly',
        daysPreviously: 'Last 3 months',
        items: [
          {
            parsed: { x: 'x' },
            name: 'Fitbit Pay',
            values: [
              {
                label: 'Jan, 2022',
                value: 24293508
              },
              {
                label: 'Feb, 2022',
                value: 920681785
              },
              {
                label: 'Mar, 2022',
                value: 283577759
              }
            ]
          },
          {
            name: 'Apple Pay',
            values: [
              {
                label: 'Jan, 2022',
                value: 719244967
              },
              {
                label: 'Feb, 2022',
                value: 925656270
              },
              {
                label: 'Mar, 2022',
                value: 260977245
              }
            ]
          },
          {
            name: 'Garmin Pay',
            values: [
              {
                label: 'Jan, 2022',
                value: 514409959
              },
              {
                label: 'Feb, 2022',
                value: 840885771
              },
              {
                label: 'Mar, 2022',
                value: 812175946
              }
            ]
          },
          {
            name: 'Google Pay',
            values: [
              {
                label: 'Jan, 2022',
                value: 878708175
              },
              {
                label: 'Feb, 2022',
                value: 492063872
              },
              {
                label: 'Mar, 2022',
                value: 171873830
              }
            ]
          }
        ]
      },
      loading: false
    });

    const { getAllByText } = render(
      <KpiChart onDeletedKpi={mockOnDeleteKpi} kpiId={'893070678'} />
    );
    expect(getAllByText('LineChart')).toBeTruthy();
  });

  it('Should render KpiChart Monthly > Last months', () => {
    mockUseKpiIdSelector.mockReturnValue({
      data: {
        id: 33,
        name: 'ZH19T8JB',
        kpi: 'Utilization Rate (Calculation of Total utilization)',
        subjectArea: 'Balances',
        submetric: 'Rate',
        period: 'MONTHLY',
        daysPreviously: 'LAST_MONTH',
        items: [
          {
            parsed: { x: 'x' },
            name: 'Fitbit Pay',
            values: [
              {
                label: 'Mar - 2022',
                value: 250527517
              }
            ]
          },
          {
            name: 'Google Pay',
            values: [
              {
                label: 'Mar - 2022',
                value: 855294842
              }
            ]
          },
          {
            name: 'Momo Pay',
            values: [
              {
                label: 'Mar - 2022',
                value: 857893238
              }
            ]
          }
        ]
      },
      loading: false
    });

    const { getAllByText } = render(
      <KpiChart onDeletedKpi={mockOnDeleteKpi} kpiId={'893070678'} />
    );
    expect(getAllByText('LineChart')).toBeTruthy();
  });

  it('Should render KpiChart Monthly > Last week', () => {
    mockUseKpiIdSelector.mockReturnValue({
      data: {
        id: 33,
        name: 'ZH19T8JB',
        kpi: 'Utilization Rate (Calculation of Total utilization)',
        subjectArea: 'Balances',
        submetric: 'Rate',
        period: 'WEEKLY',
        daysPreviously: 'LAST_WEEK',
        items: [
          {
            parsed: { x: 'x' },
            name: 'Fitbit Pay',
            values: [
              {
                label: 'Mar - 2022',
                value: 250527517
              }
            ]
          },
          {
            name: 'Google Pay',
            values: [
              {
                label: 'Mar - 2022',
                value: 855294842
              }
            ]
          },
          {
            name: 'Momo Pay',
            values: [
              {
                label: 'Mar - 2022',
                value: 857893238
              }
            ]
          }
        ]
      },
      loading: false
    });

    const { getAllByText } = render(
      <KpiChart onDeletedKpi={mockOnDeleteKpi} kpiId={'893070678'} />
    );
    expect(getAllByText('LineChart')).toBeTruthy();
  });

  it('Should render action button', () => {
    mockUseKpiIdSelector.mockReturnValue({
      data: {
        id: 33,
        name: 'ZH19T8JB',
        kpi: 'Utilization Rate (Calculation of Total utilization)',
        subjectArea: 'Balances',
        submetric: 'Rate',
        period: 'DAILY',
        daysPreviously: 'LAST_3_MONTHS',
        items: [
          {
            parsed: { x: 'x' },
            name: 'Fitbit Pay',
            values: [
              {
                label: 'Mar - 2022',
                value: 250527517
              }
            ]
          },
          {
            name: 'Google Pay',
            values: [
              {
                label: 'Mar - 2022',
                value: 855294842
              }
            ]
          },
          {
            name: 'Momo Pay',
            values: [
              {
                label: 'Mar - 2022',
                value: 857893238
              }
            ]
          }
        ]
      },
      loading: false
    });

    const wrapper = RenderComponent({
      kpiId: '893070678',
      onDeletedKpi: mockOnDeleteKpi
    });
    const btn = wrapper.getByTestId('dataTestId_dls-button');
    userEvent.click(btn);

    const dropdownButton = wrapper.container.querySelector(
      'button.dls-dropdown-button'
    ) as Element;
    expect(dropdownButton).not.toBeNull();
    userEvent.click(dropdownButton);
    expect(wrapper.getByText('Edit')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('Edit'));

    expect(wrapper.getByText('Remove')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('Remove'));

    expect(wrapper.getByText('Export')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('Export'));

    expect(wrapper.getByText('txt_cancel')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('txt_cancel'));

    expect(wrapper.getByText('txt_remove')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('txt_remove'));
  });

  it('Should render zoom and last_week', () => {
    mockUseKpiIdSelector.mockReturnValue({
      data: {
        id: 33,
        name: 'ZH19T8JB',
        kpi: 'Utilization Rate (Calculation of Total utilization)',
        subjectArea: 'Balances',
        submetric: 'Rate',
        period: 'MONTHLY',
        daysPreviously: 'LAST_WEEK',
        items: [
          {
            name: 'Fitbit Pay',
            values: [
              {
                label: 'Mar - 2022',
                value: 250527517
              }
            ]
          },
          {
            name: 'Google Pay',
            values: [
              {
                label: 'Mar - 2022',
                value: 855294842
              }
            ]
          },
          {
            name: 'Momo Pay',
            values: [
              {
                label: 'Mar - 2022',
                value: 857893238
              }
            ]
          }
        ]
      },
      loading: false
    });
    const { getByText } = render(
      <KpiChart onDeletedKpi={mockOnDeleteKpi} kpiId="893070678" />
    );

    const element = document.querySelector('.icon.icon-fullscreen');
    userEvent.click(element!);

    expect(getByText('LineChart')).toBeInTheDocument();
  });
  it('Should render zoom and last 3 months', () => {
    mockUseKpiIdSelector.mockReturnValue({
      data: {
        id: 33,
        name: 'ZH19T8JB',
        kpi: 'Utilization Rate (Calculation of Total utilization)',
        subjectArea: 'Balances',
        submetric: 'Rate',
        period: 'MONTHLY',
        daysPreviously: 'LAST_3_MONTHS',
        items: [
          {
            name: 'Fitbit Pay',
            values: [
              {
                label: 'Mar - 2022',
                value: 250527517
              }
            ]
          },
          {
            name: 'Google Pay',
            values: [
              {
                label: 'Mar - 2022',
                value: 855294842
              }
            ]
          },
          {
            name: 'Momo Pay',
            values: [
              {
                label: 'Mar - 2022',
                value: 857893238
              }
            ]
          }
        ]
      },
      loading: false
    });
    const { getByText } = render(
      <KpiChart onDeletedKpi={mockOnDeleteKpi} kpiId="893070678" />
    );

    const element = document.querySelector('.icon.icon-fullscreen');
    userEvent.click(element!);

    expect(getByText('LineChart')).toBeInTheDocument();
  });
  it('Should render zoom and weekly', () => {
    mockUseKpiIdSelector.mockReturnValue({
      data: {
        id: 33,
        name: 'ZH19T8JB',
        kpi: 'Utilization Rate (Calculation of Total utilization)',
        subjectArea: 'Balances',
        submetric: 'Rate',
        period: 'WEEKLY',
        daysPreviously: 'LAST_3_MONTHS',
        items: [
          {
            name: 'Fitbit Pay',
            values: [
              {
                label: 'Mar - 2022',
                value: 250527517
              }
            ]
          },
          {
            name: 'Google Pay',
            values: [
              {
                label: 'Mar - 2022',
                value: 855294842
              }
            ]
          },
          {
            name: 'Momo Pay',
            values: [
              {
                label: 'Mar - 2022',
                value: 857893238
              }
            ]
          }
        ]
      },
      loading: false
    });
    const { getByText } = render(
      <KpiChart onDeletedKpi={mockOnDeleteKpi} kpiId="893070678" />
    );

    const element = document.querySelector('.icon.icon-fullscreen');
    userEvent.click(element!);

    expect(getByText('LineChart')).toBeInTheDocument();
  });
});
