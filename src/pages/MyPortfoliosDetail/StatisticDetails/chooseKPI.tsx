import KpiTemplateCardView from 'app/components/KpiTemplateCardView';
import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import SimpleSearchSmall from 'app/components/SimpleSearchSmall';
import { classnames } from 'app/helpers';
import { Button, SimpleBar, useTranslation } from 'app/_libraries/_dls';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import {
  actionsPortfolio,
  useSelectKPIListForSetup,
  useSelectKPIListForSetupMenu
} from 'pages/_commons/redux/Portfolio';
import { WorkflowTemplateType } from 'pages/_commons/redux/Workflow';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch } from 'react-redux';

export interface IkeepState {
  data: IKPIListForSetup[];
  searchKey: string;
  filter: string;
}

interface ChooseKPIProps {
  opened: boolean;
  handleKeepState: (value: any) => void;
  keepState: IkeepState;
}
const ChooseKPI: React.FC<ChooseKPIProps> = ({
  handleKeepState,
  opened,
  keepState = {}
}) => {
  const reState: any = useMemo(() => {
    return keepState;
  }, [keepState]);
  const { width } = useSelectWindowDimension();
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const [currentSection, setCurrentSection] = useState(
    reState?.filter || 'all'
  );
  const [searchValue, setSearchValue] = useState(reState?.searchKey || '');

  const hasFilterToClear = searchValue;
  const KPITypes = useSelectKPIListForSetupMenu();
  const { data: kpi, loading } = useSelectKPIListForSetup();

  const KPIList = useMemo(() => {
    let newData: any = [];

    const value = isEmpty(reState?.data) ? kpi : reState?.data;

    //filter
    if (isEmpty(searchValue)) {
      newData =
        currentSection === 'all'
          ? value
          : value?.filter(
              (x: any) => x.subjectArea.toLocaleLowerCase() === currentSection
            );
    }

    newData =
      currentSection !== 'all'
        ? value?.filter(
            (x: any) =>
              x.subjectArea.toLocaleLowerCase() === currentSection &&
              x.name
                .toLocaleLowerCase()
                .includes(searchValue.toLocaleLowerCase())
          )
        : value?.filter((x: any) =>
            x.name.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase())
          );

    return newData;
  }, [currentSection, searchValue, kpi, reState]);

  useEffect(() => {
    dispatch(actionsPortfolio.getKPIListForSetup(''));
  }, [dispatch]);

  const handleSearch = useCallback(
    (val: string) => {
      setSearchValue(val);
      handleKeepState({});
    },
    [handleKeepState]
  );

  const handleFilterWorkflowMenu = useCallback(
    item => {
      const { id } = item;
      setCurrentSection(id);
      handleKeepState({});
    },
    [handleKeepState]
  );

  useEffect(() => {
    if (!opened && isEmpty(reState)) {
      handleKeepState({
        data: KPIList,
        searchKey: searchValue,
        filter: currentSection
      });
    }
  }, [KPIList, handleKeepState, searchValue, currentSection, opened, reState]);

  // clear and reset
  const handleClearAndReset = useCallback(() => {
    setCurrentSection(currentSection);
    setSearchValue('');
    handleKeepState({});
  }, [currentSection, handleKeepState]);

  const menuItem = useCallback(
    (button: WorkflowTemplateType) => (
      <div className="workflow-menu-item py-4" key={button.id}>
        <Button
          variant={currentSection === button.id ? 'primary' : 'outline-primary'}
          className={classnames(
            'w-100 br-radius-8 text-left px-16 py-8 fs-14 fw-600',
            currentSection !== button.id && 'color-secondary'
          )}
          onClick={() => handleFilterWorkflowMenu(button)}
        >
          <div className="d-flex justify-content-between">
            <span>{button.label}</span>
            <span className="ml-4">{button.count}</span>
          </div>
        </Button>
      </div>
    ),
    [currentSection, handleFilterWorkflowMenu]
  );

  const sidebar = useMemo(
    () => (
      <div className="workflows-sidebar bg-light-l20 border-right px-8 py-24">
        <h5 className="px-16 mb-16">Subject Area</h5>
        <div>{KPITypes?.map(button => menuItem(button))}</div>
      </div>
    ),
    [KPITypes, menuItem]
  );

  const titleAndFilter = useMemo(
    () => (
      <div className="content-head">
        <div className="d-flex justify-content-between align-items-center mb-16">
          <h5>{t('txt_kpi_list')}</h5>
          {(KPIList.length > 0 || hasFilterToClear) && (
            <div>
              {width > 1040 ? (
                <SimpleSearch
                  placeholder={t('txt_type_to_search_kpi')}
                  onSearch={handleSearch}
                  defaultValue={searchValue}
                />
              ) : (
                <div className="mr-n12">
                  <SimpleSearchSmall
                    placeholder={t('txt_type_to_search_kpi')}
                    maxLength={200}
                    onSearch={handleSearch}
                    defaultValue={searchValue}
                  />
                </div>
              )}
            </div>
          )}
        </div>
        {KPIList.length > 0 && hasFilterToClear && (
          <div className="d-flex justify-content-end mb-16 mr-n6">
            <Button
              id="clear-button"
              size={'sm'}
              variant="outline-primary"
              onClick={handleClearAndReset}
            >
              {t('txt_clear_and_reset')}
            </Button>
          </div>
        )}
      </div>
    ),
    [
      t,
      handleSearch,
      searchValue,
      KPIList,
      width,
      hasFilterToClear,
      handleClearAndReset
    ]
  );

  const workflowTemplateCardList = useMemo(
    () =>
      KPIList.length > 0 && (
        <React.Fragment>
          <div className="row mt-n12">
            {KPIList.map((item: any) => (
              <div key={item.id} className="col-lg-12 col-xl-12">
                <KpiTemplateCardView
                  key={item.id}
                  id={`page_${item.id}`}
                  value={item}
                />
              </div>
            ))}
          </div>
        </React.Fragment>
      ),
    [KPIList]
  );

  const noDataFound = useMemo(
    () =>
      KPIList.length === 0 && (
        <div className="d-flex flex-column justify-content-center mt-40 mb-24">
          <NoDataFound
            id="all-kpi-NoDataFound"
            title={t('txt_no_kpi_to_display')}
            hasSearch={!!searchValue}
            linkTitle={hasFilterToClear && t('txt_clear_and_reset')}
            onLinkClicked={handleClearAndReset}
          />
        </div>
      ),
    [handleClearAndReset, t, KPIList, hasFilterToClear, searchValue]
  );

  return (
    <SimpleBar className="h-100 content-h-100">
      <div
        className={classnames('h-100 d-flex flex-column', loading && 'loading')}
      >
        <div className="d-flex flex-fill">
          {sidebar}
          <div className="workflow-content flex-fill px-24 pt-24 pb-24 h-100">
            {titleAndFilter}
            {workflowTemplateCardList}
            {!loading && noDataFound}
          </div>
        </div>
      </div>
    </SimpleBar>
  );
};

export default ChooseKPI;
