import { fireEvent, render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import {
  mockUseDispatchFnc,
  mockUseModalRegistryFnc,
  mockUseSelectorFnc
} from 'app/utils';
import 'app/_libraries/_dls/test-utils/mocks/mockCanvas';
import * as CommonRedux from 'pages/_commons/redux/Common/select-hooks';
import * as StatisticDetailsSelector from 'pages/_commons/redux/Portfolio/select-hooks';
import React from 'react';
import StatisticDetails from './';

const mockAddKpiMode = jest.fn();
jest.mock('./addKPI', () => ({
  __esModule: true,
  default: ({ onValid }: any) => {
    const handleOnValid = () => {
      onValid(
        true,
        {
          kpiName: 'kpiName',
          period: { code: 'code', text: 'text' },
          days_Previously: { code: 'code', text: 'text' }
        },
        mockAddKpiMode() || 'Edit'
      );
    };

    return <div onClick={handleOnValid}>AddKPI_Component</div>;
  }
}));

jest.mock('./kpiChart', () => ({
  __esModule: true,
  default: ({ onDeletedKpi }: any) => {
    const handleDelete = () => {
      onDeletedKpi('680887888');
    };

    return <div onClick={handleDelete}>kpiChart_Component</div>;
  }
}));

jest.mock('chart.js', () => ({
  Chart: {
    register: (list: any) => undefined
  },
  registerables: []
}));

jest.mock('app/_libraries/_dls/components/charts/LineChart', () => {
  const actualModule = jest.requireActual(
    'app/_libraries/_dls/components/charts/LineChart'
  );
  return {
    ...actualModule,
    LineChart: () => <div>LineChart</div>
  };
});

const mockSelectOpendChooseModal = jest.spyOn(
  StatisticDetailsSelector,
  'useSelectOpendChooseModal'
);

const mockSelectKPIListForSetup = jest.spyOn(
  StatisticDetailsSelector,
  'useSelectKPIListForSetup'
);

const mockSelectKPIListForSetupMenu = jest.spyOn(
  StatisticDetailsSelector,
  'useSelectKPIListForSetupMenu'
);

const mockUseModalRegistry = mockUseModalRegistryFnc();
const mockUseDispatch = mockUseDispatchFnc();
const mockUseSelector = mockUseSelectorFnc();
jest.mock('react-router-dom', () => ({
  useParams: () => '10000001'
}));

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const mockUseSelectWindowDimension = jest.spyOn(
  CommonRedux,
  'useSelectWindowDimension'
);

const mockUseSelectWindowDimensionImplementation = (width: number) => {
  mockUseSelectWindowDimension.mockImplementation(() => ({ width } as any));
};

describe('Test ChangeDetail > StatisticDetails', () => {
  const mockDispatch = jest.fn();
  const portfoliosDetail = {
    id: '1734306522',
    icon: '',
    name: '1JQBWFS1',
    portfolioCriteria: [
      {
        fieldName: 'Company Name',
        values: ['UNXMGZ93', 'NR0CDJ98', 'PW53PDAG', 'TCSIP58D']
      }
    ],
    portfolioStatistic: {
      kpiList: ['680887888']
    }
  };

  beforeEach(() => {
    mockUseDispatch.mockReturnValue(mockDispatch);
    mockUseSelector.mockReturnValue(mockDispatch);
    mockUseDispatch.mockImplementation(() => mockDispatch);
    mockUseSelectWindowDimensionImplementation(120);
    mockUseModalRegistry.mockImplementation(() => [true, false]);
    mockSelectOpendChooseModal.mockReturnValue({
      openedAddPopup: true,
      openedChoosePopup: true,
      dataChooseKPI: {
        id: '22',
        name: '30 Day Delinquent ',
        kpi: '30 Day Delinquent ',
        subjectArea: 'Delinquency',
        submetric: 'Count/Amount'
      }
    });

    mockSelectKPIListForSetup.mockReturnValue({
      loading: false,
      data: [
        {
          id: '30',
          name: '% of Purchase Volume',
          kpi: '30 Day Delinquent ',
          subjectArea: 'Digital',
          submetric: 'Rate'
        }
      ]
    });

    mockSelectKPIListForSetupMenu.mockReturnValue([
      {
        id: 'all',
        label: 'All',
        count: 39
      },
      {
        id: 'accounts',
        label: 'Accounts',
        count: 9
      },
      {
        id: 'balances',
        label: 'Balances',
        count: 5
      },
      {
        id: 'delinquency',
        label: 'Delinquency',
        count: 7
      },
      {
        id: 'digital',
        label: 'Digital',
        count: 3
      },
      {
        id: 'purchases',
        label: 'Purchases',
        count: 8
      },
      {
        id: 'revenue',
        label: 'Revenue',
        count: 7
      }
    ]);
  });
  afterEach(() => {
    mockUseDispatch.mockClear();
    mockUseSelector.mockClear();
    mockDispatch.mockClear();
    mockUseModalRegistry.mockClear();
    jest.clearAllMocks();
  });

  it('Should render StatisticDetails with portfolios empty', () => {
    const wrapper = render(<StatisticDetails kpiList={[]} />);

    const { getByText } = wrapper;
    userEvent.click(getByText('txt_add_kpi'));
    expect(getByText('txt_choose_KPI_to_add')).toBeInTheDocument();

    fireEvent.click(
      wrapper.baseElement.querySelectorAll(
        'button[class="btn btn-icon-secondary"]'
      )[2] as Element
    );
  });

  it('Should render StatisticDetails > ChooseKPI', async () => {
    const wrapper = render(<StatisticDetails kpiList={['680887888']} />);

    userEvent.click(wrapper.getAllByText('txt_add_kpi')[0]);
    userEvent.click(wrapper.getAllByText('txt_add_kpi')[1]);
    expect(wrapper.getByText('txt_choose_KPI_to_add')).toBeInTheDocument();

    then_header_close_modal();
    then_footer_close_modal();
  });

  it('Should render StatisticDetails > addKPI', () => {
    mockAddKpiMode.mockReturnValue('Add');
    mockSelectOpendChooseModal.mockReturnValue({
      openedAddPopup: true,
      openedChoosePopup: false,
      dataChooseKPI: {
        id: '22',
        name: '30 Day Delinquent ',
        kpi: '30 Day Delinquent ',
        subjectArea: 'Delinquency',
        submetric: 'Count/Amount'
      }
    });

    const { getAllByText, getByText } = render(
      <StatisticDetails kpiList={portfoliosDetail.portfolioStatistic.kpiList} />
    );

    userEvent.click(getAllByText('txt_add_kpi')[0]);
    expect(getByText('txt_choose_KPI_to_add')).toBeInTheDocument();

    //open add popup
    const selectBtn = document.body
      .querySelectorAll('button.btn-outline-primary')
      .item(0);

    userEvent.click(selectBtn!);

    const element = document.querySelector(
      '.dls-modal-root .dls-modal-header button.btn-icon-secondary'
    );
    userEvent.click(element!);

    expect(getByText('txt_title_modify')).toBeInTheDocument();
    userEvent.click(getByText('AddKPI_Component'));

    userEvent.click(getByText('txt_save'));
    userEvent.click(getByText('txt_cancel'));
    userEvent.click(getByText('txt_back'));

    then_header_close_modal();
    then_footer_close_modal();
    then_footer_ok_modal();

    expect(getByText('txt_choose_KPI_to_add')).toBeInTheDocument();
  });

  it('Should render StatisticDetails >  edit KPI', () => {
    mockSelectOpendChooseModal.mockReturnValue({
      openedAddPopup: true,
      openedChoosePopup: false,
      dataChooseKPI: {
        id: '22',
        name: '30 Day Delinquent ',
        kpi: '30 Day Delinquent ',
        subjectArea: 'Delinquency',
        submetric: 'Count/Amount'
      }
    });

    const { getAllByText, getByText } = render(
      <StatisticDetails kpiList={['680887888']} />
    );

    userEvent.click(getAllByText('txt_add_kpi')[0]);
    expect(getByText('txt_choose_KPI_to_add')).toBeInTheDocument();

    //open add popup
    const selectBtn = document.body
      .querySelectorAll('button.btn-outline-primary')
      .item(0);

    userEvent.click(selectBtn!);

    const element = document.querySelector(
      '.dls-modal-root .dls-modal-header button.btn-icon-secondary'
    );
    userEvent.click(element!);

    expect(getByText('txt_title_modify')).toBeInTheDocument();
    userEvent.click(getByText('AddKPI_Component'));

    userEvent.click(getByText('txt_save'));

    userEvent.click(getByText('kpiChart_Component'));

    then_header_close_modal();
    then_footer_close_modal();
    then_footer_ok_modal();

    expect(getByText('txt_choose_KPI_to_add')).toBeInTheDocument();
  });

  const then_header_close_modal = () => {
    const element = document.querySelector(
      '.dls-modal-root .dls-modal-header button.btn-icon-secondary'
    );
    userEvent.click(element!);
  };

  const then_footer_close_modal = () => {
    const element = document.querySelector(
      '.dls-modal-root .dls-modal-footer button.btn-secondary'
    );
    userEvent.click(element!);
  };

  const then_footer_ok_modal = () => {
    const element = document.querySelector(
      '.dls-modal-root .dls-modal-footer button.btn-primary'
    );
    userEvent.click(element!);
  };
});
