import ModalRegistry from 'app/components/ModalRegistry';
import {
  Button,
  ModalBody,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import isFunction from 'lodash.isfunction';
import React from 'react';

interface IProps {
  id: string;
  show: boolean;
  onClose: () => void;
  onRemove: () => void;
}

const RemovePopupModal: React.FC<IProps> = ({
  id,
  show,
  onClose,
  onRemove
}) => {
  const { t } = useTranslation();

  const handleRemove = () => {
    isFunction(onRemove) && onRemove();
  };

  const handleClose = () => {
    isFunction(onClose) && onClose();
  };

  return (
    <ModalRegistry
      id={id}
      show={show}
      onAutoClosedAll={handleClose}
      classes={{ content: '' }}
    >
      <ModalHeader border closeButton onHide={handleClose}>
        <ModalTitle>{t('txt_confirm_remove')}</ModalTitle>
      </ModalHeader>
      <ModalBody>
        <span>{t('txt_portfolio_confirm_msg_remove')}</span>
      </ModalBody>
      <div className="dls-modal-footer modal-footer">
        <Button variant="secondary" onClick={handleClose}>
          {t('txt_cancel')}
        </Button>
        <Button variant="danger" onClick={handleRemove}>
          {t('txt_remove')}
        </Button>
      </div>
    </ModalRegistry>
  );
};

export default RemovePopupModal;
