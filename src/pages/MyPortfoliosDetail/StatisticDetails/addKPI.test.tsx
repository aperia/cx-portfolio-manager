import { fireEvent, render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import {
  mockUseDispatchFnc,
  mockUseModalRegistryFnc,
  mockUseSelectorFnc
} from 'app/utils';
import 'app/_libraries/_dls/test-utils/mocks/mockCanvas';
import * as CommonRedux from 'pages/_commons/redux/Common/select-hooks';
import * as StatisticDetailsSelector from 'pages/_commons/redux/Portfolio/select-hooks';
import React from 'react';
import AddKPI from './addKPI';

const mockSelectOpendChooseModal = jest.spyOn(
  StatisticDetailsSelector,
  'useSelectOpendChooseModal'
);

const mockSelectKPIListForSetup = jest.spyOn(
  StatisticDetailsSelector,
  'useSelectKPIListForSetup'
);

const mockSelectKPIListForSetupMenu = jest.spyOn(
  StatisticDetailsSelector,
  'useSelectKPIListForSetupMenu'
);

const mockUseModalRegistry = mockUseModalRegistryFnc();
const mockUseDispatch = mockUseDispatchFnc();
const mockUseSelector = mockUseSelectorFnc();

jest.mock('react-router-dom', () => ({
  useParams: () => '10000001'
}));

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const mockUseSelectWindowDimension = jest.spyOn(
  CommonRedux,
  'useSelectWindowDimension'
);

const mockUseSelectWindowDimensionImplementation = (width: number) => {
  mockUseSelectWindowDimension.mockImplementation(() => ({ width } as any));
};

const allData = [
  {
    id: '30',
    name: '% of Purchase Volume',
    subjectArea: 'Digital',
    submetric: 'Rate'
  },
  {
    id: '22',
    name: '30 Day Delinquent ',
    subjectArea: 'Delinquency',
    submetric: 'Count/Amount'
  },
  {
    id: '23',
    name: '60 Days Delinquent ',
    subjectArea: 'Delinquency',
    submetric: 'Count/Amount'
  },
  {
    id: '24',
    name: '90+ Days Delinquent',
    subjectArea: 'Delinquency',
    submetric: 'Count/Amount'
  },
  {
    id: '21',
    name: 'Active Account Utilization',
    subjectArea: 'Balances',
    submetric: 'Rate'
  },
  {
    id: '34',
    name: 'Average Purchase Size',
    subjectArea: 'Purchases',
    submetric: 'Currency'
  },
  {
    id: '11',
    name: 'Balance Active Accounts',
    subjectArea: 'Accounts',
    submetric: 'Count'
  },
  {
    id: '10',
    name: 'Balance Active Accounts ',
    subjectArea: 'Accounts',
    submetric: 'Count'
  },
  {
    id: '15',
    name: 'Balance Active Rate',
    subjectArea: 'Accounts',
    submetric: 'Rate'
  },
  {
    id: '25',
    name: 'Charge Offs (total accounts in charge off status)',
    subjectArea: 'Delinquency',
    submetric: 'Count-Amount'
  },
  {
    id: '26',
    name: 'Charged Offs (accounts charged off today)',
    subjectArea: 'Delinquency',
    submetric: 'Count-Amount'
  },
  {
    id: '12',
    name: 'Closed Accounts',
    subjectArea: 'Accounts',
    submetric: 'Count'
  },
  {
    id: '28',
    name: 'Credit Loss ($)',
    subjectArea: 'Delinquency',
    submetric: 'Amount'
  },
  {
    id: '29',
    name: 'Digital Wallet Volume ',
    subjectArea: 'Digital',
    submetric: 'Amount/Count'
  },
  {
    id: '4',
    name: 'Fee Income',
    subjectArea: 'Revenue',
    submetric: 'Amount'
  },
  {
    id: '6',
    name: 'Fees%',
    subjectArea: 'Revenue',
    submetric: 'Rate'
  },
  {
    id: '5',
    name: 'Finance Charge %',
    subjectArea: 'Revenue',
    submetric: 'Rate'
  },
  {
    id: '3',
    name: 'Finance Charge Income',
    subjectArea: 'Revenue',
    submetric: 'Amount'
  },
  {
    id: '27',
    name: 'Fraud Loss ($)',
    subjectArea: 'Delinquency',
    submetric: 'Amount'
  },
  {
    id: '14',
    name: 'Gross Account Active Rate',
    subjectArea: 'Accounts',
    submetric: 'Rate'
  },
  {
    id: '8',
    name: 'Gross Active Accounts ',
    subjectArea: 'Accounts',
    submetric: 'Count'
  },
  {
    id: '2',
    name: 'Interchange Income',
    subjectArea: 'Revenue',
    submetric: 'Amount'
  },
  {
    id: '7',
    name: 'Interchange%',
    subjectArea: 'Revenue',
    submetric: 'Rate'
  },
  {
    id: '18',
    name: 'Outstanding Balances',
    subjectArea: 'Balances',
    submetric: 'Amount'
  },
  {
    id: '16',
    name: 'Plastics',
    subjectArea: 'Accounts',
    submetric: 'Count'
  },
  {
    id: '36',
    name: 'Purchase Active Accounts',
    subjectArea: 'Purchases',
    submetric: ''
  },
  {
    id: '33',
    name: 'Purchase Transactions (#) - Count',
    subjectArea: 'Purchases',
    submetric: 'Count'
  },
  {
    id: '37',
    name: 'Purchases By Location',
    subjectArea: 'Purchases',
    submetric: 'Geo'
  },
  {
    id: '38',
    name: 'Purchases Declined',
    subjectArea: 'Purchases',
    submetric: 'Amount/Count'
  },
  {
    id: '13',
    name: ' Revolvers vs. Transactors',
    subjectArea: 'Accounts',
    submetric: 'Count'
  },
  {
    id: '31',
    name: 'Split between POS Vs eCOmm',
    subjectArea: 'Digital',
    submetric: 'Count-Amount'
  },
  {
    id: '39',
    name: 'Top Decline Reason Code',
    subjectArea: 'Purchases',
    submetric: 'Amount/Count'
  },
  {
    id: '35',
    name: 'Top Merchant Categories',
    subjectArea: 'Purchases',
    submetric: 'Amount/Count'
  },
  {
    id: '9',
    name: 'Total Accounts on File',
    subjectArea: 'Accounts',
    submetric: 'Count'
  },
  {
    id: '17',
    name: 'Total Credit Lines ',
    subjectArea: 'Balances',
    submetric: 'Amount'
  },
  {
    id: '32',
    name: 'Total Purchases',
    subjectArea: 'Purchases',
    submetric: 'Amount'
  },
  {
    id: '1',
    name: 'Total Revenue',
    subjectArea: 'Revenue',
    submetric: 'Amount'
  },
  {
    id: '19',
    name: 'Total Revolving Balances',
    subjectArea: 'Balances',
    submetric: 'Amount'
  },
  {
    id: '20',
    name: 'Utilization Rate (Calculation of Total utilization)',
    subjectArea: 'Balances',
    submetric: 'Rate'
  }
];

describe('Test ChangeDetail > StatisticDetails', () => {
  const mockDispatch = jest.fn();

  beforeEach(() => {
    mockUseDispatch.mockReturnValue(mockDispatch);
    mockUseSelector.mockReturnValue(mockDispatch);
    mockUseDispatch.mockImplementation(() => mockDispatch);
    mockUseModalRegistry.mockImplementation(() => [true, false]);
    mockUseSelectWindowDimensionImplementation(1050);
    mockSelectOpendChooseModal.mockReturnValue({
      openedAddPopup: true,
      openedChoosePopup: true,
      dataChooseKPI: {
        id: '22',
        name: '30 Day Delinquent ',
        subjectArea: 'Delinquency',
        submetric: 'Count/Amount'
      }
    });

    mockSelectKPIListForSetup.mockReturnValue({
      loading: false,
      data: allData
    });

    mockSelectKPIListForSetupMenu.mockReturnValue([
      {
        id: 'all',
        label: 'All',
        count: 39
      },
      {
        id: 'accounts',
        label: 'Accounts',
        count: 9
      },
      {
        id: 'balances',
        label: 'Balances',
        count: 5
      },
      {
        id: 'delinquency',
        label: 'Delinquency',
        count: 7
      },
      {
        id: 'digital',
        label: 'Digital',
        count: 3
      },
      {
        id: 'purchases',
        label: 'Purchases',
        count: 8
      },
      {
        id: 'revenue',
        label: 'Revenue',
        count: 7
      }
    ]);
  });
  afterEach(() => {
    mockDispatch.mockClear();
    mockUseDispatch.mockClear();
    mockUseSelector.mockClear();
    mockUseModalRegistry.mockClear();
    jest.clearAllMocks();
  });

  it('Should render AddKPI', () => {
    const wrapper = render(
      <AddKPI kpi={{ name: '123', mode: 'Add' }} onValid={jest.fn()} />
    );
    expect(wrapper.getByText('Enter details for 123')).toBeInTheDocument();

    const textBox = wrapper.getByText('KPI Name');
    userEvent.type(textBox, 'qwe');
    fireEvent.blur(textBox);
    userEvent.type(textBox, 'mmm');
    fireEvent.blur(textBox);

    then_change_dropdown_item(0, 1);
    then_change_dropdown_item(0, 2);

    then_change_dropdown_item(1, 1);
    then_change_dropdown_item(1, 3);
  });

  const then_change_dropdown_item = (filterIdx: number, itemIdx: number) => {
    const element = document.body
      .querySelectorAll('.dls-dropdown-list .text-field-container')
      .item(filterIdx);
    fireEvent.focus(element);

    const pageElement = document
      .querySelectorAll('.dls-popup .item')
      .item(itemIdx);
    fireEvent.click(pageElement);
  };
});
