import 'app/_libraries/_dls/test-utils/mocks/mockCanvas';
import { DAYS_PREVIOUSLY, PERIOD } from './constants';
import {
  mappingColorItem,
  mappingDaysPreviously,
  mappingForEdit,
  mappingPeriod,
  weeklyLabel
} from './mapping';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

describe('Test ChangeDetail > StatisticDetails > mappingDaysPreviously', () => {
  it('should mappingDaysPreviously LAST_WEEK', () => {
    const data = mappingDaysPreviously(DAYS_PREVIOUSLY.LAST_WEEK);

    expect(data).toEqual('Last week');
  });
  it('should mappingDaysPreviously LAST_MONTH', () => {
    const data = mappingDaysPreviously(DAYS_PREVIOUSLY.LAST_MONTH);

    expect(data).toEqual('Last month');
  });
  it('should mappingDaysPreviously LAST_3_MONTHS', () => {
    const data = mappingDaysPreviously(DAYS_PREVIOUSLY.LAST_3_MONTHS);

    expect(data).toEqual('Last 3 months');
  });
});

describe('Test ChangeDetail > StatisticDetails > mappingForEdit', () => {
  it('should mappingForEdit LAST 3 MONTHS', () => {
    const data = mappingForEdit('LAST 3 MONTHS');

    expect(data).toEqual(DAYS_PREVIOUSLY.LAST_3_MONTHS);
  });
  it('should mappingForEdit LAST_WEEK', () => {
    const data = mappingForEdit('LAST WEEK');

    expect(data).toEqual(DAYS_PREVIOUSLY.LAST_WEEK);
  });
  it('should mappingForEdit LAST_MONTH', () => {
    const data = mappingForEdit('LAST MONTH');

    expect(data).toEqual(DAYS_PREVIOUSLY.LAST_MONTH);
  });
});

describe('Test ChangeDetail > StatisticDetails > mappingPeriod', () => {
  it('should mappingPeriod DAILY', () => {
    const data = mappingPeriod(PERIOD.DAILY);

    expect(data).toEqual('Daily');
  });
  it('should mappingPeriod WEEKLY', () => {
    const data = mappingPeriod(PERIOD.WEEKLY);

    expect(data).toEqual('Weekly');
  });
  it('should mappingPeriod MONTHLY', () => {
    const data = mappingPeriod(PERIOD.MONTHLY);

    expect(data).toEqual('Monthly');
  });
});

describe('Test ChangeDetail > StatisticDetails > mappingColorItem && weeklyLabel', () => {
  it('Should mappingColorItem', () => {
    const map = mappingColorItem(0);
    expect(map).toEqual('#5CBAE6');

    const map1 = mappingColorItem(1);
    expect(map1).toEqual('#BCE05A');

    const map2 = mappingColorItem(2);
    expect(map2).toEqual('#F66364');

    const map3 = mappingColorItem(3);
    expect(map3).toEqual('#FCB665');

    const map4 = mappingColorItem(4);
    expect(map4).toEqual('#D977C4');

    const mapDefault = mappingColorItem(5);
    expect(mapDefault).toEqual('#5CBAE6');
  });

  it('Should weeklyLabel LAST_WEEK', () => {
    const label = weeklyLabel(
      'Sunday, Apr 10, 2022 - Saturday, Apr 16, 2022',
      DAYS_PREVIOUSLY.LAST_WEEK
    );
    expect(label).toEqual('Apr 10 - 16');
  });

  it('Should weeklyLabel LAST_3_MONTHS', () => {
    const label = weeklyLabel(
      'Sunday, Apr 10, 2022 - Saturday, Apr 16, 2022',
      DAYS_PREVIOUSLY.LAST_3_MONTHS
    );
    expect(label).toEqual('Apr 10 - 16, 2022');
  });

  it('Should weeklyLabel LAST_3_MONTH && two year', () => {
    const label = weeklyLabel(
      'Friday, dec 10, 2021 - Monday, Jan 16, 2022',
      DAYS_PREVIOUSLY.LAST_3_MONTHS
    );
    expect(label).toEqual('Dec 10, 2001 - Jan 16, 2001');
  });
});
