import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { mockUseModalRegistryFnc } from 'app/utils';
import 'app/_libraries/_dls/test-utils/mocks/mockCanvas';
import React from 'react';
import RemovePopupModal from './removePopupModal';

const mockUseModalRegistry = mockUseModalRegistryFnc();

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

describe('Test ChangeDetail > StatisticDetails > removePopupModal', () => {
  beforeEach(() => {
    mockUseModalRegistry.mockImplementation(() => [true, false]);
  });
  afterEach(() => {
    mockUseModalRegistry.mockClear();
  });

  it('Should render removePopupModal > btn_delete', () => {
    const { getByText } = render(
      <RemovePopupModal
        id="123"
        onClose={() => {}}
        onRemove={() => {}}
        show={true}
      />
    );

    const btn_delete = getByText('txt_remove');
    userEvent.click(btn_delete);

    expect(btn_delete).toBeInTheDocument();
  });

  it('Should render removePopupModal > btn_cancel', () => {
    const { getByText } = render(
      <RemovePopupModal
        id="123"
        onClose={() => {}}
        onRemove={() => {}}
        show={true}
      />
    );

    const btn_cancel = getByText('txt_cancel');
    userEvent.click(btn_cancel);
    expect(btn_cancel).toBeInTheDocument();
  });
});
