import ModalRegistry from 'app/components/ModalRegistry';
import NoDataFound from 'app/components/NoDataFound';
import {
  Button,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import {
  actionsPortfolio,
  useSelectOpendChooseModal
} from 'pages/_commons/redux/Portfolio';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';
import { IURLParams } from '..';
import AddKPI from './addKPI';
import ChooseKPI, { IkeepState } from './chooseKPI';
import KpiChart from './kpiChart';
import { mappingForEdit } from './mapping';

interface IStatisticDetailsProps {
  kpiList: string[];
}

const StatisticDetails: React.FC<IStatisticDetailsProps> = ({ kpiList }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [keepState, setKeepState] = useState<any>({});
  const [kpiLst, setKpiLst] = useState<string[]>([]);

  const { id: portfolioId } = useParams<IURLParams>();
  const { openedAddPopup, openedChoosePopup, dataChooseKPI } =
    useSelectOpendChooseModal();

  const [valid, setValid] = useState<boolean>(false);
  const [addFormValue, setAddFormValue] = useState({
    kpiId: '',
    kpiName: '',
    period: { code: '', text: '' },
    days_Previously: { code: '', text: '' }
  });
  const [mode, setMode] = useState('Add');

  //init reset flat modal
  useEffect(() => {
    dispatch(
      actionsPortfolio.setOpenedChoosePopup({
        isChooseOpen: false,
        value: {},
        isAddOpen: false
      })
    );
  }, [dispatch]);

  useEffect(() => {
    if (isEmpty(kpiList)) return;
    setKpiLst(kpiList);
  }, [kpiList]);

  const kpis = useMemo(() => {
    if (isEmpty(kpiLst)) return;

    return kpiLst;
  }, [kpiLst]);

  const handleTooglePopup = (isChooseOpen: boolean, isAddOpen: boolean) => {
    dispatch(
      actionsPortfolio.setOpenedChoosePopup({
        isChooseOpen,
        value: {},
        isAddOpen
      })
    );
  };

  const handleFormAddKpi = (valid: boolean, value: any, mode: string) => {
    setValid(valid);
    setAddFormValue(value);
    setMode(mode);
  };

  const handleOnDeletedKpi = (kpiId: string) => {
    setKpiLst([...kpiLst.filter(x => x !== kpiId)]);
  };

  const handleSubmitAddKpi = async () => {
    switch (mode) {
      //Add PortfolioKPI & close popup

      case 'Add':
        const dataAdd = {
          portfolioId: portfolioId,
          kpi: dataChooseKPI?.name || '',
          subjectArea: dataChooseKPI?.subjectArea || '',
          submetric: dataChooseKPI?.submetric || '',
          name: !isEmpty(addFormValue.kpiName)
            ? addFormValue.kpiName
            : dataChooseKPI?.name || '',
          period: addFormValue.period.code,
          daysPreviously: addFormValue.days_Previously.code
        };

        const response = (await Promise.resolve(
          dispatch(actionsPortfolio.addPortfolioKPI(dataAdd))
        )) as any;

        const { id } = response?.payload?.data || {};
        !!id && setKpiLst([...kpiLst, id.toString()]);

        break;

      //Edit PortfolioKPI & close popup
      case 'Edit':
        const dataEdit = {
          id: parseInt(addFormValue.kpiId),
          name: !isEmpty(addFormValue.kpiName)
            ? addFormValue.kpiName
            : dataChooseKPI?.kpi || '',
          period: addFormValue.period.code?.toUpperCase(),
          daysPreviously: mappingForEdit(addFormValue.days_Previously.code)
        };

        dispatch(actionsPortfolio.updatePortfolioKPI(dataEdit));

        break;
    }

    handleTooglePopup(false, false);
  };

  const handleKeepState = useCallback(
    (value: IkeepState[]) => {
      setKeepState(value);
    },
    [setKeepState]
  );

  return (
    <>
      <div className="d-flex mb-16">
        <h4>{t('txt_statistic_details')}</h4>
        {kpis !== undefined && kpis?.length > 0 && (
          <div className="ml-auto">
            <Button
              variant="outline-primary"
              className="mr-n8"
              size="sm"
              onClick={() => {
                handleTooglePopup(true, false), setKeepState({});
              }}
            >
              {t('txt_add_kpi')}
            </Button>
          </div>
        )}
      </div>

      <div className="overflow-hidden ">
        {kpis !== undefined && kpis?.length > 0 && (
          <div className="row mb-m24">
            {kpis?.map((kpi: string, index: number) => {
              return (
                <div className="col-12 col-xl-6 mb-24" key={index}>
                  <KpiChart kpiId={kpi} onDeletedKpi={handleOnDeletedKpi} />
                </div>
              );
            })}
            <div className="col-12 col-xl-6 mb-24">
              <div className="card-view-empty d-flex align-items-center justify-content-center ">
                <NoDataFound
                  id="portfolio-detail-add-api"
                  title={t('txt_select_kpi_to_display')}
                  linkTitle={t('txt_add_kpi')}
                  onLinkClicked={() => {
                    handleTooglePopup(true, false), setKeepState({});
                  }}
                />
              </div>
            </div>
          </div>
        )}
        {(kpis === undefined || kpis?.length === 0) && (
          <NoDataFound
            id="portfolio-detail-add-api"
            title={t('txt_select_kpi_to_display')}
            linkTitle={t('txt_add_kpi')}
            onLinkClicked={() => {
              handleTooglePopup(true, false), setKeepState({});
            }}
            className="mt-80 mb-24"
          />
        )}
      </div>

      <ModalRegistry md id={'choose_KPI_to_add'} show={openedChoosePopup}>
        <ModalHeader
          border
          closeButton
          onHide={() => handleTooglePopup(false, false)}
        >
          <ModalTitle>{t('txt_choose_KPI_to_add')}</ModalTitle>
        </ModalHeader>
        <ModalBody noPadding>
          <ChooseKPI
            handleKeepState={handleKeepState}
            keepState={keepState}
            opened={openedChoosePopup}
          />
        </ModalBody>
        <ModalFooter
          border
          cancelButtonText={t('txt_close')}
          onCancel={() => handleTooglePopup(false, false)}
        ></ModalFooter>
      </ModalRegistry>

      <ModalRegistry id={'add_KPI'} show={openedAddPopup}>
        <ModalHeader
          border
          closeButton
          onHide={() => handleTooglePopup(false, false)}
        >
          <ModalTitle>{t('txt_title_modify')}</ModalTitle>
        </ModalHeader>
        <ModalBody>
          <AddKPI onValid={handleFormAddKpi} kpi={dataChooseKPI} />
        </ModalBody>
        <ModalFooter
          cancelButtonText={t('txt_cancel')}
          okButtonText={t('txt_save')}
          disabledOk={!valid}
          onCancel={() => handleTooglePopup(false, false)}
          onOk={handleSubmitAddKpi}
        >
          {mode === 'Add' && (
            <Button
              className="mr-auto ml-n8"
              variant="outline-primary"
              onClick={() => handleTooglePopup(true, false)}
            >
              {t('txt_back')}
            </Button>
          )}
        </ModalFooter>
      </ModalRegistry>
    </>
  );
};

export default StatisticDetails;
