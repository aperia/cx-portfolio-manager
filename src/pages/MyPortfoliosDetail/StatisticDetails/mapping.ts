import { formatTime } from 'app/helpers';
import { DAYS_PREVIOUSLY, PERIOD } from './constants';

const mappingColorItem = (item: number) => {
  switch (item) {
    case 0:
      return '#5CBAE6';

    case 1:
      return '#BCE05A';

    case 2:
      return '#F66364';

    case 3:
      return '#FCB665';

    case 4:
      return '#D977C4';

    default:
      return '#5CBAE6';
  }
};

const weeklyLabel = (value: string, daysPreviously: string) => {
  const firstDate = value.split('-')[0].trim();
  const secondDate = value.split('-')[1].trim();

  const dateFrom = formatTime(firstDate).monthShortDate;
  const dateTo = formatTime(secondDate).monthShortDate;
  const dateToShort = formatTime(secondDate).dateShort;

  let label = '';
  switch (daysPreviously.toUpperCase()) {
    case DAYS_PREVIOUSLY.LAST_WEEK:
    case DAYS_PREVIOUSLY.LAST_MONTH:
      label = dateFrom + ' - ' + dateToShort;
      break;

    case DAYS_PREVIOUSLY.LAST_3_MONTHS:
      const firstYear = formatTime(firstDate).year;
      const secondYear = formatTime(secondDate).year;

      const isTwoYear = firstYear != secondYear;

      label = isTwoYear
        ? formatTime(dateFrom).dateForLineChart +
          ' - ' +
          formatTime(dateTo).dateForLineChart
        : dateFrom + ' - ' + dateToShort + ', ' + firstYear;
      break;
  }

  return label;
};

const mappingDaysPreviously = (days: string) => {
  switch (days) {
    case DAYS_PREVIOUSLY.LAST_WEEK:
      return 'Last week';
    case DAYS_PREVIOUSLY.LAST_MONTH:
      return 'Last month';
    case DAYS_PREVIOUSLY.LAST_3_MONTHS:
      return 'Last 3 months';
    default:
      return days;
  }
};

const mappingPeriod = (period: string) => {
  switch (period) {
    case PERIOD.DAILY:
      return 'Daily';
    case PERIOD.WEEKLY:
      return 'Weekly';
    case PERIOD.MONTHLY:
      return 'Monthly';
    default:
      return period;
  }
};

const mappingForEdit = (value: string) => {
  switch (value?.toUpperCase()) {
    case 'LAST 3 MONTHS':
      return DAYS_PREVIOUSLY.LAST_3_MONTHS;
    case 'LAST MONTH':
      return DAYS_PREVIOUSLY.LAST_MONTH;
    case 'LAST WEEK':
      return DAYS_PREVIOUSLY.LAST_WEEK;
    default:
      return value;
  }
};

export {
  mappingColorItem,
  weeklyLabel,
  mappingDaysPreviously,
  mappingPeriod,
  mappingForEdit
};
