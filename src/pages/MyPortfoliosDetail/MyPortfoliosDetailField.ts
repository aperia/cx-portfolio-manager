export const MyPortfoliosDetailField = {
  PortfolioID: 'Portfolio ID',
  BIN: 'BIN',
  System: 'System',
  System_Prin: 'System/Prin',
  System_Prin_Agents: 'Sys/Prin/Agents',
  CompanyID: 'Company ID',
  CompanyName: 'Company Name',
  StateCode: 'State Code',
  DatalinkElements: 'Datalink Elements',
  PricingStrategy: 'Pricing Strategy',
  ProductTypeCode: 'Product Type Code',
  MiscellaneousFields110TX: 'Miscellaneous Fields 1-10-TX',
  MiscellaneousFields1115TX: 'Miscellaneous Fields 11-15-TX',
  MiscellaneousFields1620TX: 'Miscellaneous Fields 16-20-TX',
  MiscellaneousFields2125TX: 'Miscellaneous Fields 21-25-TX',
  MiscellaneousFields26TX: 'Miscellaneous Fields 26-TX',
  MiscellaneousFields27TX: 'Miscellaneous Fields 27-TX',
  MiscellaneousFields28TX: 'Miscellaneous Fields 28-TX',
  MiscellaneousFields29TX: 'Miscellaneous Fields 29-TX',
  MiscellaneousFields30TX: 'Miscellaneous Fields 30-TX',
  MiscellaneousFields31TX: 'Miscellaneous Fields 31-TX',
  MiscellaneousFields32TX: 'Miscellaneous Fields 32-TX',
  MiscellaneousFields33TX: 'Miscellaneous Fields 33-TX',
  MiscellaneousFields34TX: 'Miscellaneous Fields 34-TX',
  MiscellaneousFields35TX: 'Miscellaneous Fields 35-TX',
  MiscellaneousFields36TX: 'Miscellaneous Fields 36-TX',
  MiscellaneousFields37TX: 'Miscellaneous Fields 37-TX',
  MiscellaneousFields38TX: 'Miscellaneous Fields 38-TX',
  MiscellaneousFields39TX: 'Miscellaneous Fields 39-TX',
  MiscellaneousFields40TX: 'Miscellaneous Fields 40-TX',
  MiscellaneousFields41TX: 'Miscellaneous Fields 41-TX',
  MiscellaneousFields42TX: 'Miscellaneous Fields 42-TX',
  MiscellaneousFields43TX: 'Miscellaneous Fields 43-TX',
  MiscellaneousFields44TX: 'Miscellaneous Fields 44-TX',
  MiscellaneousFields45TX: 'Miscellaneous Fields 45-TX',
  MiscellaneousFields46TX: 'Miscellaneous Fields 46-TX',
  MiscellaneousFields47TX: 'Miscellaneous Fields 47-TX',
  MiscellaneousFields48TX: 'Miscellaneous Fields 48-TX',
  MiscellaneousFields49TX: 'Miscellaneous Fields 49-TX',
  MiscellaneousFields50TX: 'Miscellaneous Fields 50-TX'
};

export interface IMyPortfoliosDetailField {
  PortfolioID?: string;
  BIN?: string;
  System?: string;
  System_Prin?: string;
  System_Prin_Agents?: string;
  CompanyID?: string;
  CompanyName?: string;
  StateCode?: string;
  DatalinkElements?: string;
  PricingStrategy?: string;
  ProductTypeCode?: string;
  MiscellaneousFields110TX?: string;
  MiscellaneousFields1115TX?: string;
  MiscellaneousFields1620TX?: string;
  MiscellaneousFields2125TX?: string;
  MiscellaneousFields26TX?: string;
  MiscellaneousFields27TX?: string;
  MiscellaneousFields28TX?: string;
  MiscellaneousFields29TX?: string;
  MiscellaneousFields30TX?: string;
  MiscellaneousFields31TX?: string;
  MiscellaneousFields32TX?: string;
  MiscellaneousFields33TX?: string;
  MiscellaneousFields34TX?: string;
  MiscellaneousFields35TX?: string;
  MiscellaneousFields36TX?: string;
  MiscellaneousFields37TX?: string;
  MiscellaneousFields38TX?: string;
  MiscellaneousFields39TX?: string;
  MiscellaneousFields40TX?: string;
  MiscellaneousFields41TX?: string;
  MiscellaneousFields42TX?: string;
  MiscellaneousFields43TX?: string;
  MiscellaneousFields44TX?: string;
  MiscellaneousFields45TX?: string;
  MiscellaneousFields46TX?: string;
  MiscellaneousFields47TX?: string;
  MiscellaneousFields48TX?: string;
  MiscellaneousFields49TX?: string;
  MiscellaneousFields50TX?: string;
}
