import { MethodFieldParameterEnum } from 'app/constants/enums';
import { ColumnType } from 'app/_libraries/_dls';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import { GridParameter } from '.';

const columns = (
  t: any,
  valueAccessor: any,
  isSummary?: boolean
): ColumnType[] => [
  {
    id: 'fieldName',
    Header: t('txt_business_name'),
    accessor: 'fieldName'
  },
  {
    id: 'greenScreenName',
    Header: t('txt_bulk_external_status_management_green_screen_name'),
    accessor: 'greenScreenName'
  },
  {
    id: 'value',
    Header: t('txt_value'),
    cellBodyProps: { className: isSummary ? '' : 'action' },
    accessor: valueAccessor
  },
  {
    id: 'moreInfo',
    Header: t('txt_more_info'),
    className: 'text-center',
    accessor: viewMoreInfo(['moreInfo'], t),
    width: 105
  }
];

export const gridConfigureData = (t: any): GridParameter[] => [
  {
    id: MethodFieldParameterEnum.ManualAccountDeletionExternalStatusCode,
    fieldName: t('txt_manual_account_deletion_new_external_status_code'),
    greenScreenName: t('txt_status'),
    moreInfo: t(
      'txt_manual_account_deletion_new_external_status_code_more_info'
    )
  },
  {
    id: MethodFieldParameterEnum.ManualAccountDeletionPropagate,
    fieldName: t('txt_manual_account_deletion_propagate'),
    greenScreenName: t('txt_manual_account_deletion_propagate'),
    moreInfo: t('txt_manual_account_deletion_propagate_more_info')
  }
];
export const WorkflowManualAccountDeletionMetaParameters = [
  MethodFieldParameterEnum.ManualAccountDeletionExternalStatusCode,
  MethodFieldParameterEnum.ManualAccountDeletionMN0TransactionId,
  MethodFieldParameterEnum.ManualAccountDeletionTransactionId,
  MethodFieldParameterEnum.ManualAccountDeletionTransaction,
  MethodFieldParameterEnum.ManualAccountDeletionPropagate
];

export default columns;
