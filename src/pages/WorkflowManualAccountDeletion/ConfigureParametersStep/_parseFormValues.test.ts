import { MethodFieldParameterEnum } from 'app/constants/enums';
import parseFormValues from './_parseFormValues';

describe('WorkflowManualAccountDeletion > ConfigureParametersStep > parseFormValues', () => {
  it('Should return undefined', () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '2'
            }
          ] as any
        }
      } as WorkflowSetupInstance,
      id: '1'
    };

    const response = parseFormValues(props.data, props.id, jest.fn());
    expect(response).toEqual(undefined);
  });

  it('Should return data', () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '1'
            }
          ] as any,
          transactionId: '',
          configurations: {
            transactionId: 'test',
            [MethodFieldParameterEnum.ManualAccountDeletionExternalStatusCode]:
              'code',
            [MethodFieldParameterEnum.ManualAccountDeletionPropagate]: 'code',
            [MethodFieldParameterEnum.ManualAccountDeletionMN0TransactionId]:
              'nm.54',
            parameters: [
              {
                name: MethodFieldParameterEnum.ManualAccountDeletionMN0TransactionId,
                value: ''
              },
              {
                name: 'test',
                value: 'test'
              }
            ]
          } as any
        }
      } as any,
      id: '1'
    };
    const response = parseFormValues(props.data, props.id, jest.fn());

    expect(response)?.toEqual({
      transactionId: 'test',
      configureParameters: { 'nm.0.transaction.id': [''], test: 'test' },
      isPass: false,
      isSelected: false,
      isValid: true
    });
  });

  it('Should Run with default parameters data', () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '1'
            }
          ] as any,
          transactionId: '',
          configurations: {} as any
        }
      } as any,
      id: '1'
    };
    const response = parseFormValues(props.data, props.id, jest.fn());

    expect(response?.isPass)?.toEqual(false);
  });
});
