import {
  act,
  fireEvent,
  render,
  RenderResult,
  screen
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import { mockUseDispatchFnc } from 'app/utils';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManualAccountDeletion';
import React from 'react';
import ConfigureParametersStep from '.';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/utils/MockView')
);
const mockUseDispatch = mockUseDispatchFnc();

const useSelectElementMetadataForManualAccountDeletion = jest.spyOn(
  WorkflowSetup,
  'useSelectElementMetadataForManualAccountDeletion'
);

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});
jest.mock('app/hooks/useUnsavedChangeRegistry', () => {
  return {
    __esModule: true,
    useUnsavedChangeRegistry: (options: any, changes: any, onConfirm: any) => {
      onConfirm && onConfirm();
    }
  };
});
const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <ConfigureParametersStep {...props} />
    </div>
  );
};

const then_change_dropdown = (wrapper: RenderResult) => {
  const dropdown = wrapper.getAllByPlaceholderText(
    'txt_workflow_manage_change_in_terms_select_an_option'
  )[0];

  act(() => {
    userEvent.click(dropdown);
  });

  act(() => {
    userEvent.click(screen.getByText('text2'));
  });

  fireEvent.blur(dropdown);
};

describe('ConfigureParametersStep ', () => {
  const mockDispatch = jest.fn();
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
    useSelectElementMetadataForManualAccountDeletion.mockReturnValue({
      manualAccountDeletionExternalStatusCode: [
        { code: 'code', text: 'text1' }
      ],
      manualAccountDeletionPropagate: [{ code: 'code', text: 'text2' }],
      manualAccountDeletionTransaction: [{ code: 'code', text: 'text' }],
      manualAccountDeletionTransactionId: [
        { code: 'code', text: 'text', description: 'nm.0.accounts' },
        {
          code: 'code2',
          text: 'text2',
          description: 'nm.0.charged.off.accounts'
        }
      ],
      manualAccountDeletionMN0TransactionId: [
        { code: 'nm.670', text: 'NM*670' },
        { code: 'nm.54', text: 'NM*54' }
      ]
    });
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
  });

  it('Should have error', async () => {
    const props = {
      stepId: '1',
      selectedStep: '2',
      clearFormName: '',
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = await renderComponent({
      ...props,
      formValues: {
        isValid: false,
        configureParameters: {}
      }
    });

    expect(
      wrapper.getByText('txt_manual_account_deletion_configure_parameter_info')
    ).toBeInTheDocument;
  });

  it('Should render with nm.0.accounts', async () => {
    const props = {
      stepId: '1',
      selectedStep: '2',
      clearFormName: '',
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = await renderComponent({
      ...props,
      formValues: {
        isValid: false,
        transactionId: 'nm.0.accounts',
        configureParameters: {
          [MethodFieldParameterEnum.ManualAccountDeletionExternalStatusCode]:
            'code',
          [MethodFieldParameterEnum.ManualAccountDeletionPropagate]: 'code',
          [MethodFieldParameterEnum.ManualAccountDeletionMN0TransactionId]: []
        }
      },
      dependencies: {
        attachmentId: '123'
      }
    });

    expect(wrapper.getByText('txt_manual_account_deletion_nm0_title'))
      .toBeInTheDocument;
  });

  it('Should render with nm.0.charged.off.accounts', async () => {
    const props = {
      stepId: '2',
      selectedStep: '2',
      clearFormName: '',
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = await renderComponent({
      ...props,
      formValues: {
        isValid: false,
        transactionId: 'nm.0.charged.off.accounts',
        configureParameters: {
          [MethodFieldParameterEnum.ManualAccountDeletionExternalStatusCode]:
            'code',
          [MethodFieldParameterEnum.ManualAccountDeletionPropagate]: 'code',
          [MethodFieldParameterEnum.ManualAccountDeletionMN0TransactionId]: [
            'nm.54'
          ]
        }
      },
      dependencies: {
        attachmentId: '123'
      }
    });

    userEvent.click(screen.getByText('nm.0.charged.off.accounts'));
    userEvent.click(screen.getAllByRole('checkbox')[0]);
    userEvent.click(screen.getAllByRole('checkbox')[1]);

    expect(wrapper.getByText('txt_manual_account_deletion_transactions'))
      .toBeInTheDocument;

    then_change_dropdown(wrapper);

    const searchBox = wrapper.getByPlaceholderText('txt_type_to_search');
    userEvent.type(searchBox, 'a');
    userEvent.click(
      searchBox.parentElement!.querySelector(
        '.icon-simple-search .icon.icon-search'
      )!
    );

    userEvent.type(searchBox, 'notfound');
    userEvent.click(
      searchBox.parentElement!.querySelector(
        '.icon-simple-search .icon.icon-search'
      )!
    );

    expect(
      wrapper.getByText('txt_no_results_found txt_adjust_your_search_criteria')
    ).toBeInTheDocument();

    userEvent.click(wrapper.getByText('txt_clear_and_reset'));
    expect(
      wrapper.queryByText(
        'txt_no_results_found txt_adjust_your_search_criteria'
      )
    ).not.toBeInTheDocument();
  });

  it('Should render with Stuck message', async () => {
    const props = {
      stepId: '1',
      selectedStep: '2',
      clearFormName: '',
      isStuck: true,
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = await renderComponent({
      ...props,
      formValues: {
        isValid: false,
        transactionId: 'nm.0.accounts',
        configureParameters: {
          [MethodFieldParameterEnum.ManualAccountDeletionExternalStatusCode]:
            'code',
          [MethodFieldParameterEnum.ManualAccountDeletionPropagate]: 'code',
          [MethodFieldParameterEnum.ManualAccountDeletionMN0TransactionId]: []
        }
      },
      dependencies: {
        attachmentId: '123'
      }
    });

    expect(wrapper.getByText('txt_step_stuck_move_forward_message'))
      .toBeInTheDocument;
  });
});
