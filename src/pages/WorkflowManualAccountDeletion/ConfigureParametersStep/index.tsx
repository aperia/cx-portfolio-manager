import EnhanceDropdownList from 'app/components/EnhanceDropdownList';
import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import {
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { classnames, matchSearchValue } from 'app/helpers';
import { useUnsavedChangeRegistry } from 'app/hooks';
import {
  CheckBox,
  ComboBox,
  Grid,
  Icon,
  InlineMessage,
  Radio,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty, isEqual, orderBy } from 'app/_libraries/_dls/lodash';
import {
  actionsWorkflowSetup,
  useSelectElementMetadataForManualAccountDeletion
} from 'pages/_commons/redux/WorkflowSetup';
import ClearAndResetButton from 'pages/_commons/Utils/ClearAndResetButton';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';
import ConfigureParametersStepSummary from './ConfigureParametersStepSummary';
import columns, {
  gridConfigureData,
  WorkflowManualAccountDeletionMetaParameters
} from './constant';
import parseFormValues from './_parseFormValues';
import stepValueFunc from './_stepValueFunc';

export interface ConfigureParameter {
  [MethodFieldParameterEnum.ManualAccountDeletionExternalStatusCode]?: string;
  [MethodFieldParameterEnum.ManualAccountDeletionPropagate]?: string;
  [MethodFieldParameterEnum.ManualAccountDeletionMN0TransactionId]?: string[];
}

export type TransactionType = 'nm.0.accounts' | 'nm.0.charged.off.accounts';
export type MN0TransactionType = 'nm.670' | 'nm.54';

export interface GridParameter {
  id: MethodFieldParameterEnum;
  fieldName: string;
  greenScreenName: string;
  moreInfo: string;
}
export interface IConfigureParameters {}

export interface ConfigureParametersFormValues {
  isValid?: boolean;
  transactionId?: TransactionType;
  configureParameters?: ConfigureParameter;
}

const ConfigureParametersStep: React.FC<
  WorkflowSetupProps<ConfigureParametersFormValues>
> = ({ formValues, setFormValues, savedAt, stepId, selectedStep, isStuck }) => {
  const dispatch = useDispatch();
  const [initialValues, setInitialValues] = useState(formValues);
  const { configureParameters, transactionId } = formValues;
  const [configureParametersValue, setConfigureParametersValue] =
    useState<Record<string, ConfigureParameter> | undefined>(undefined);

  const [searchValue, setSearchValue] = useState('');
  const {
    manualAccountDeletionExternalStatusCode,
    manualAccountDeletionPropagate,
    manualAccountDeletionTransactionId,
    manualAccountDeletionMN0TransactionId
  } = useSelectElementMetadataForManualAccountDeletion();

  const { t } = useTranslation();
  const simpleSearchRef = useRef<any>(null);
  const showGrid = useMemo(
    () =>
      transactionId === 'nm.0.charged.off.accounts' &&
      configureParameters?.[
        MethodFieldParameterEnum.ManualAccountDeletionMN0TransactionId
      ]?.includes('nm.54'),
    [configureParameters, transactionId]
  );

  useEffect(() => {
    if (showGrid) {
      handleClearFilter();
    }
  }, [showGrid]);

  useEffect(() => {
    if (!searchValue && simpleSearchRef?.current) {
      simpleSearchRef.current.clear();
    }
  }, [searchValue]);
  useEffect(() => {
    if (configureParameters && !configureParametersValue) {
      setConfigureParametersValue({
        [`${transactionId}`]: configureParameters
      });
    }
  }, [configureParameters, transactionId, configureParametersValue]);

  useEffect(() => {
    if (stepId !== selectedStep) {
      setConfigureParametersValue(undefined);
      handleClearFilter();
    }
  }, [stepId, selectedStep, savedAt]);

  const handleSearch = (val: string) => {
    setSearchValue(val);
  };

  const gridData = useMemo(() => {
    return gridConfigureData(t).filter(p =>
      matchSearchValue(
        [p.fieldName, p.greenScreenName, p.moreInfo],
        searchValue
      )
    );
  }, [t, searchValue]);

  useEffect(() => {
    setInitialValues(formValues);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [savedAt]);

  const formChanged =
    initialValues?.transactionId !== transactionId ||
    initialValues?.configureParameters?.[
      MethodFieldParameterEnum.ManualAccountDeletionExternalStatusCode
    ] !==
      configureParameters?.[
        MethodFieldParameterEnum.ManualAccountDeletionExternalStatusCode
      ] ||
    initialValues?.configureParameters?.[
      MethodFieldParameterEnum.ManualAccountDeletionPropagate
    ] !==
      configureParameters?.[
        MethodFieldParameterEnum.ManualAccountDeletionPropagate
      ] ||
    !isEqual(
      orderBy(
        configureParameters?.[
          MethodFieldParameterEnum.ManualAccountDeletionMN0TransactionId
        ]
      ),
      orderBy(
        initialValues?.configureParameters?.[
          MethodFieldParameterEnum.ManualAccountDeletionMN0TransactionId
        ]
      )
    );

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANUAL_ACCOUNT_DELETION__CONFIGURE_PARAMETERS,
      priority: 1
    },
    [formChanged]
  );

  const isValid = useMemo(() => {
    return !(
      !transactionId ||
      (transactionId === 'nm.0.charged.off.accounts' &&
        isEmpty(
          configureParameters?.[
            MethodFieldParameterEnum.ManualAccountDeletionMN0TransactionId
          ]
        ))
    );
  }, [transactionId, configureParameters]);

  useEffect(() => {
    if (formValues.isValid === isValid) return;
    setFormValues({
      ...formValues,
      isValid
    });
  }, [isValid, setFormValues, formValues]);

  useEffect(() => {
    dispatch(
      actionsWorkflowSetup.getWorkflowMetadata(
        WorkflowManualAccountDeletionMetaParameters
      )
    );
  }, [dispatch]);

  const handleFormChange = useCallback(
    (field: keyof ConfigureParameter) => (e: any) => {
      setConfigureParametersValue({
        ...configureParametersValue,
        [`${transactionId}`]: {
          ...configureParametersValue?.[`${transactionId}`],
          [field]: e.target?.value?.code
        }
      });

      setFormValues({
        ...formValues,
        configureParameters: {
          ...formValues.configureParameters,
          [field]: e.target?.value?.code
        }
      });
    },
    [formValues, setFormValues, configureParametersValue, transactionId]
  );
  const renderDropdown = (data: RefData[], field: keyof ConfigureParameter) => {
    const value = data.find(
      o => o.code === configureParametersValue?.[`${transactionId}`]?.[field]
    );

    return (
      <EnhanceDropdownList
        placeholder={t('txt_workflow_manage_change_in_terms_select_an_option')}
        dataTestId={`configureParameters__${field}`}
        size="small"
        value={value}
        onChange={handleFormChange(field)}
        options={data}
      />
    );
  };
  const renderCombobox = (data: RefData[], field: keyof ConfigureParameter) => {
    const value = data.find(
      o => o.code === configureParametersValue?.[`${transactionId}`]?.[field]
    );

    return (
      <ComboBox
        placeholder={t('txt_workflow_manage_change_in_terms_select_an_option')}
        size="small"
        textField="text"
        value={value}
        onChange={handleFormChange(field)}
        noResult={t('txt_no_results_found_without_dot')}
      >
        {data.map(item => (
          <ComboBox.Item key={item.code} value={item} label={item.text} />
        ))}
      </ComboBox>
    );
  };

  const valueAccessor = (data: Record<string, any>) => {
    const paramId = data.id as MethodFieldParameterEnum;

    switch (paramId) {
      case MethodFieldParameterEnum.ManualAccountDeletionExternalStatusCode: {
        return renderDropdown(
          manualAccountDeletionExternalStatusCode,
          MethodFieldParameterEnum.ManualAccountDeletionExternalStatusCode
        );
      }
      case MethodFieldParameterEnum.ManualAccountDeletionPropagate: {
        return renderCombobox(
          manualAccountDeletionPropagate,
          MethodFieldParameterEnum.ManualAccountDeletionPropagate
        );
      }
    }
  };

  const handleChangeRadio = useCallback(
    (value: TransactionType) => {
      setFormValues({
        ...formValues,
        transactionId: value,
        configureParameters: configureParametersValue?.[`${value}`]
      });
    },
    [formValues, setFormValues, configureParametersValue]
  );

  const handleChangeCheckbox = useCallback(
    (e: any, transactionType: MN0TransactionType) => {
      let transactions =
        configureParameters?.[
          MethodFieldParameterEnum.ManualAccountDeletionMN0TransactionId
        ] || [];
      transactions = transactions.includes(transactionType)
        ? transactions.filter(f => f !== transactionType)
        : [...transactions, transactionType];

      setConfigureParametersValue({
        ...configureParametersValue,
        [`${transactionId}`]: {
          ...configureParametersValue?.[`${transactionId}`],
          [MethodFieldParameterEnum.ManualAccountDeletionMN0TransactionId]:
            transactions
        }
      });
      let newConfigureParameters = undefined;
      if (!isEmpty(transactions)) {
        newConfigureParameters = transactions.includes('nm.54')
          ? {
              ...configureParametersValue?.[`${transactionId}`],
              [MethodFieldParameterEnum.ManualAccountDeletionMN0TransactionId]:
                transactions
            }
          : {
              [MethodFieldParameterEnum.ManualAccountDeletionMN0TransactionId]:
                transactions
            };
      }
      setFormValues({
        ...formValues,
        configureParameters: newConfigureParameters
      });
    },
    [
      configureParametersValue,
      configureParameters,
      formValues,
      setFormValues,
      transactionId
    ]
  );

  const handleClearFilter = () => {
    setSearchValue('');
  };
  const renderRadioTransaction = useMemo(
    () => (
      <div className="group-card">
        {manualAccountDeletionTransactionId?.map((item, idx) => (
          <div
            key={item.code}
            className="group-card-item d-flex align-items-center px-16 py-18 custom-control-root"
            onClick={() => handleChangeRadio(item.code as TransactionType)}
          >
            <span className="pr-8">{item.description}</span>
            <Radio className="ml-auto mr-n4">
              <Radio.Input
                checked={transactionId === (item.code as TransactionType)}
                id={item.code}
                name={item.code}
                className="checked-style"
              />
            </Radio>
          </div>
        ))}
      </div>
    ),
    [transactionId, handleChangeRadio, manualAccountDeletionTransactionId]
  );

  const renderChargeOffAccountTransaction = useMemo(
    () => (
      <>
        <div className="divider-dashed my-8"></div>
        <p className="color-grey fw-600">
          {t('txt_manual_account_deletion_transactions')}
          <span className="ml-4 color-red">*</span>
        </p>
        {manualAccountDeletionMN0TransactionId?.map(item => (
          <div className="d-flex mt-8" key={item.code}>
            <CheckBox>
              <CheckBox.Input
                id={item.code}
                name={item.code}
                checked={configureParametersValue?.[`${transactionId}`]?.[
                  MethodFieldParameterEnum.ManualAccountDeletionMN0TransactionId
                ]?.includes(item.code)}
                onClick={e =>
                  handleChangeCheckbox(e, item.code as MN0TransactionType)
                }
              />
              <CheckBox.Label>
                {
                  manualAccountDeletionMN0TransactionId?.find(
                    f => f.code === item.code
                  )?.text
                }
              </CheckBox.Label>
            </CheckBox>
            <div className="ml-8">
              <Tooltip
                element={
                  item.code === 'nm.670'
                    ? t('txt_manual_account_deletion_nm_670_tooltip')
                    : t('txt_manual_account_deletion_nm_54_tooltip')
                }
              >
                <Icon name="information" size="5x" className="color-grey-l16" />
              </Tooltip>
            </div>
          </div>
        ))}
      </>
    ),
    [
      manualAccountDeletionMN0TransactionId,
      configureParametersValue,
      t,
      transactionId,
      handleChangeCheckbox
    ]
  );

  return (
    <>
      <div
        className={classnames({
          'pb-24 border-bottom': showGrid
        })}
      >
        {isStuck && (
          <InlineMessage className="mb-0 mt-24" variant="danger" withIcon>
            {t('txt_step_stuck_move_forward_message')}
          </InlineMessage>
        )}
        <InlineMessage variant="info" withIcon className="mt-24">
          {t('txt_manual_account_deletion_configure_parameter_info')}
        </InlineMessage>
        <h5>
          {t(
            'txt_bulk_external_status_management_configure_parameters_select_action'
          )}
        </h5>

        <div className="row mt-16">
          <div className="col-md-6">{renderRadioTransaction}</div>
          {!!transactionId && (
            <div className="col-md-6">
              <div className="bg-light-l20 rounded-lg p-16 h-100">
                <p className="color-grey-d20 fw-600">
                  {t('txt_manual_account_deletion_nm0_title')}
                </p>
                <p className="color-grey mt-8">
                  {transactionId === 'nm.0.accounts'
                    ? t('txt_manual_account_deletion_inactive_account_desc')
                    : t(
                        'txt_manual_account_deletion_inactive_charged_off_account_desc'
                      )}
                </p>
                {transactionId === 'nm.0.charged.off.accounts' &&
                  renderChargeOffAccountTransaction}
              </div>
            </div>
          )}
        </div>
      </div>
      {showGrid && (
        <div className="mt-24">
          <div className="d-flex align-items-center justify-content-between mb-16">
            <h5>
              {
                manualAccountDeletionMN0TransactionId?.find(
                  f => f.code === 'nm.54'
                )?.text
              }
            </h5>

            <SimpleSearch
              ref={simpleSearchRef}
              placeholder={t('txt_type_to_search')}
              onSearch={handleSearch}
              popperElement={
                <>
                  <p className="color-grey">{t('txt_search_description')}</p>
                  <ul className="search-field-item list-unstyled">
                    <li className="mt-16">{t('txt_business_name')}</li>
                    <li className="mt-16">
                      {t(
                        'txt_bulk_external_status_management_green_screen_name'
                      )}
                    </li>
                    <li className="mt-16">{t('txt_more_info')}</li>
                  </ul>
                </>
              }
            />
          </div>
          {searchValue && !isEmpty(gridData) && (
            <div className="d-flex justify-content-end mb-16 mr-n8">
              <ClearAndResetButton small onClearAndReset={handleClearFilter} />
            </div>
          )}
          {isEmpty(gridData) && (
            <div className="d-flex flex-column justify-content-center mt-40 mb-32">
              <NoDataFound
                id="newMethod_notfound"
                hasSearch
                title={t('txt_no_results_found')}
                linkTitle={t('txt_clear_and_reset')}
                onLinkClicked={handleClearFilter}
              />
            </div>
          )}
          {!isEmpty(gridData) && (
            <div className="mt-16">
              <Grid
                className="grid-has-control"
                columns={columns(t, valueAccessor)}
                data={gridData}
              />
            </div>
          )}
        </div>
      )}
    </>
  );
};

const ExtraStaticConfigureParameters =
  ConfigureParametersStep as WorkflowSetupStaticProp<ConfigureParametersFormValues>;
ExtraStaticConfigureParameters.summaryComponent =
  ConfigureParametersStepSummary;

ExtraStaticConfigureParameters.defaultValues = {
  isValid: false,
  configureParameters: {},
  transactionId: undefined
};
ExtraStaticConfigureParameters.parseFormValues = parseFormValues;
ExtraStaticConfigureParameters.stepValue = stepValueFunc;

export default ExtraStaticConfigureParameters;
