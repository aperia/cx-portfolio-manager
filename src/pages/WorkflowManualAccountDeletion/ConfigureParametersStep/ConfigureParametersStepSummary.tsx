import GroupTextControl from 'app/components/DofControl/GroupTextControl';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import {
  Button,
  Grid,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import { isFunction } from 'app/_libraries/_dls/lodash';
import { useSelectElementMetadataForManualAccountDeletion } from 'pages/_commons/redux/WorkflowSetup';
import React, { useMemo } from 'react';
import { ConfigureParameter, ConfigureParametersFormValues } from '.';
import columns, { gridConfigureData } from './constant';

const ConfigureParametersStepSummary: React.FC<
  WorkflowSetupSummaryProps<ConfigureParametersFormValues>
> = ({ formValues, onEditStep }) => {
  const { t } = useTranslation();
  const {
    manualAccountDeletionExternalStatusCode,
    manualAccountDeletionPropagate,
    manualAccountDeletionTransactionId,
    manualAccountDeletionMN0TransactionId
  } = useSelectElementMetadataForManualAccountDeletion();
  const { configureParameters, transactionId } = formValues;

  const selectedTransaction = useMemo(
    () =>
      manualAccountDeletionTransactionId.find(f => f.code === transactionId),
    [manualAccountDeletionTransactionId, transactionId]
  );

  const handleEdit = () => {
    isFunction(onEditStep) && onEditStep();
  };
  const gridData = useMemo(() => {
    return gridConfigureData(t);
  }, [t]);

  const valueAccessor = (records: Record<string, any>) => {
    const paramId = records.id as keyof ConfigureParameter;

    let valueTruncate = '';

    switch (paramId) {
      case MethodFieldParameterEnum.ManualAccountDeletionExternalStatusCode:
        valueTruncate =
          manualAccountDeletionExternalStatusCode.find(
            f => f.code === configureParameters?.[paramId]
          )?.text || '';
        break;
      case MethodFieldParameterEnum.ManualAccountDeletionPropagate:
        valueTruncate =
          manualAccountDeletionPropagate.find(
            f => f.code === configureParameters?.[paramId]
          )?.text || '';
        break;
    }
    if (valueTruncate.toLowerCase() === 'none') {
      valueTruncate = '';
    }

    return (
      <TruncateText
        resizable
        lines={2}
        ellipsisLessText={t('txt_less')}
        ellipsisMoreText={t('txt_more')}
      >
        {valueTruncate}
      </TruncateText>
    );
  };

  return (
    <div className="position-relative">
      <div className="absolute-top-right mt-n26 mr-n8">
        <Button variant="outline-primary" size="sm" onClick={handleEdit}>
          {t('txt_edit')}
        </Button>
      </div>
      <div className="row">
        <div className="col-6 col-lg-3">
          <GroupTextControl
            id="summary_actionText"
            input={{ value: selectedTransaction?.description } as any}
            meta={{} as any}
            label={t('txt_action')}
            options={{
              inline: false,
              lineTruncate: 2,
              ellipsisLessText: t('txt_less'),
              ellipsisMoreText: t('txt_more')
            }}
          />
        </div>
        <div className="col-6 col-lg-3">
          <GroupTextControl
            id="summary_transaction"
            input={{ value: t('txt_manual_account_deletion_nm0_title') } as any}
            meta={{} as any}
            label={t('txt_transaction')}
            options={{
              inline: false,
              lineTruncate: 2,
              ellipsisLessText: t('txt_less'),
              ellipsisMoreText: t('txt_more')
            }}
          />
        </div>
        {configureParameters?.[
          MethodFieldParameterEnum.ManualAccountDeletionMN0TransactionId
        ]?.map(item => (
          <div className="col-6 col-lg-3" key={item}>
            <GroupTextControl
              id="summary_transaction"
              input={
                {
                  value: manualAccountDeletionMN0TransactionId?.find(
                    f => f.code === item
                  )?.text
                } as any
              }
              meta={{} as any}
              label={t('txt_transaction')}
              options={{
                inline: false,
                lineTruncate: 2,
                ellipsisLessText: t('txt_less'),
                ellipsisMoreText: t('txt_more')
              }}
            />
          </div>
        ))}
      </div>
      {transactionId === 'nm.0.charged.off.accounts' &&
        configureParameters?.[
          MethodFieldParameterEnum.ManualAccountDeletionMN0TransactionId
        ]?.includes('nm.54') && (
          <div className="mt-24">
            <Grid columns={columns(t, valueAccessor, true)} data={gridData} />
          </div>
        )}
    </div>
  );
};

export default ConfigureParametersStepSummary;
