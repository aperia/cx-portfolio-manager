import { MethodFieldParameterEnum as MethodField } from 'app/constants/enums';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import { ConfigureParametersFormValues } from '.';

const { getState } = store;

const stepValueFunc: WorkflowSetupStepValueFunc<ConfigureParametersFormValues> =
  ({ stepsForm: formValues, t }) => {
    const { configureParameters } = formValues || {};

    const state = getState() as RootState;
    const elementMetaData = state?.workflowSetup?.elementMetadata.elements;
    const transactionMetaData: RefData[] =
      elementMetaData
        ?.find(
          f => f.name === MethodField.ManualAccountDeletionMN0TransactionId
        )
        ?.options?.map(item => ({
          code: item.value,
          text: `${item.description} (${item.name})`
        })) || [];

    const values = [t('txt_manual_account_deletion_nm0_step_value')];
    configureParameters?.[
      MethodField.ManualAccountDeletionMN0TransactionId
    ]?.forEach(item =>
      values.push(transactionMetaData.find(f => f.code === item)?.text)
    );

    return values.join(' - ');
  };

export default stepValueFunc;
