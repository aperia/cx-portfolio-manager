import { fireEvent, render, RenderResult } from '@testing-library/react';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManualAccountDeletion';
import React from 'react';
import ConfigureParametersStepSummary from './ConfigureParametersStepSummary';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const useSelectElementMetadataForManualAccountDeletion = jest.spyOn(
  WorkflowSetup,
  'useSelectElementMetadataForManualAccountDeletion'
);

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <ConfigureParametersStepSummary {...props} />
    </div>
  );
};

describe('WorkflowManualAccountDeletion > ConfigureParametersStepSummary', () => {
  beforeEach(() => {
    useSelectElementMetadataForManualAccountDeletion.mockReturnValue({
      manualAccountDeletionExternalStatusCode: [
        { code: 'code', text: 'text1' },
        { code: '', text: 'none' }
      ],
      manualAccountDeletionPropagate: [{ code: 'code', text: 'text2' }],
      manualAccountDeletionTransaction: [{ code: 'code', text: 'text' }],
      manualAccountDeletionTransactionId: [
        { code: 'code', text: 'text', description: 'nm.0.accounts' },
        {
          code: 'code2',
          text: 'text2',
          description: 'nm.0.charged.off.accounts'
        }
      ],
      manualAccountDeletionMN0TransactionId: [
        { code: 'nm.670', text: 'NM*670' },
        { code: 'nm.54', text: 'NM*54' }
      ]
    });
  });

  afterEach(() => {});

  it('Should render a grid with content', async () => {
    const props = {
      stepId: '1',
      selectedStep: '2',
      clearFormName: '',
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = await renderComponent({
      ...props,
      formValues: {
        isValid: false,
        transactionId: 'nm.0.charged.off.accounts',
        configureParameters: {
          [MethodFieldParameterEnum.ManualAccountDeletionExternalStatusCode]:
            'code',
          [MethodFieldParameterEnum.ManualAccountDeletionPropagate]: 'code',
          [MethodFieldParameterEnum.ManualAccountDeletionMN0TransactionId]: [
            'nm.54'
          ]
        }
      },
      dependencies: {
        attachmentId: '123'
      }
    });

    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    expect(body?.children.length).toEqual(2);
  });

  it('Should render a grid with content', async () => {
    const props = {
      stepId: '1',
      selectedStep: '2',
      clearFormName: '',
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = await renderComponent({
      ...props,
      formValues: {
        isValid: false,
        transactionId: 'nm.0.charged.off.accounts',
        configureParameters: {
          [MethodFieldParameterEnum.ManualAccountDeletionExternalStatusCode]:
            '321',
          [MethodFieldParameterEnum.ManualAccountDeletionPropagate]: 'dad',
          [MethodFieldParameterEnum.ManualAccountDeletionMN0TransactionId]: [
            'nm.54'
          ]
        }
      },
      dependencies: {
        attachmentId: '123'
      }
    });

    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    expect(body?.children.length).toEqual(2);
  });
  it('Should render a grid with content', async () => {
    const props = {
      stepId: '1',
      selectedStep: '2',
      clearFormName: '',
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = await renderComponent({
      ...props,
      formValues: {
        isValid: false,
        transactionId: 'nm.0.charged.off.accounts',
        configureParameters: {
          [MethodFieldParameterEnum.ManualAccountDeletionExternalStatusCode]:
            '',
          [MethodFieldParameterEnum.ManualAccountDeletionPropagate]: 'code',
          [MethodFieldParameterEnum.ManualAccountDeletionMN0TransactionId]: [
            'nm.54'
          ]
        }
      },
      dependencies: {
        attachmentId: '123'
      }
    });

    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    expect(body?.children.length).toEqual(2);
  });
  it('Should call edit function', () => {
    const mockFn = jest.fn();
    const props = {
      formValues: {},
      onEditStep: mockFn
    } as any;

    const wrapper = renderComponent(props);
    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(mockFn).toHaveBeenCalled();
  });
});
