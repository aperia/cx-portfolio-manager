import { MethodFieldParameterEnum as MethodField } from 'app/constants/enums';
import { ConfigureParametersFormValues } from '.';
import stepValueFunc from './_stepValueFunc';

const mockStateWithOptions = {
  workflowSetup: {
    elementMetadata: {
      elements: [
        {
          name: MethodField.ManualAccountDeletionMN0TransactionId,
          options: [{ value: '1', description: '1', name: '1' }]
        }
      ]
    }
  }
};

const mockStateWithNoOptions = {
  workflowSetup: {
    elementMetadata: {
      elements: [
        {
          name: MethodField.ManualAccountDeletionMN0TransactionId
        }
      ]
    }
  }
};
let mockState: any;

jest.mock('app/_libraries/_dof/core/redux/createAppStore', () => ({
  getState: () => mockState
}));

describe('WorkflowManualAccountDeletion > ConfigureParametersStep > stepValueFunc', () => {
  const t = jest.fn().mockImplementation(v => v);

  it('should Run with elementMeta data with options', () => {
    mockState = mockStateWithOptions;
    const stepsForm = {
      configureParameters: {
        [MethodField.ManualAccountDeletionMN0TransactionId]: ['1']
      }
    } as ConfigureParametersFormValues;

    stepValueFunc({ stepsForm, t });
  });

  it('should Run with elementMeta data without options', () => {
    mockState = mockStateWithNoOptions;
    const stepsForm = {
      configureParameters: {
        [MethodField.ManualAccountDeletionMN0TransactionId]: ['1']
      }
    } as ConfigureParametersFormValues;
    stepValueFunc({ stepsForm, t });
  });

  it('should Run with default stepForm', () => {
    const stepsForm = undefined as any;
    stepValueFunc({ stepsForm, t });
  });
});
