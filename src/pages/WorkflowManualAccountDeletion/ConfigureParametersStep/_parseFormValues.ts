import { MethodFieldParameterEnum as ParameterEnum } from 'app/constants/enums';
import { getWorkflowSetupStepStatus } from 'app/helpers';
import { ConfigureParameter, TransactionType } from '.';

const parseFormValues: WorkflowSetupStepFormDataFunc<any> = (data, id) => {
  const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);

  if (!stepInfo) return;

  const parameters = (data?.data?.configurations?.parameters || []) as any[];

  const configureParameters = parameters.reduce(
    (pre, curr) => ({
      ...pre,
      [curr.name]:
        curr.name === ParameterEnum.ManualAccountDeletionMN0TransactionId
          ? curr.value?.split(';')
          : curr.value
    }),
    {} as ConfigureParameter
  );

  const transactionId = (data?.data as any)?.configurations
    ?.transactionId as TransactionType;

  const isValid = !!transactionId;

  return {
    ...getWorkflowSetupStepStatus(stepInfo),
    ...stepInfo.data,
    isValid,
    transactionId,
    configureParameters
  };
};

export default parseFormValues;
