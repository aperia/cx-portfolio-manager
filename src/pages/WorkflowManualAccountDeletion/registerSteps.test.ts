import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { default as stepRegistry } from '../_commons/WorkflowSetup/registries/stepRegistry';

const registerFunc = jest.spyOn(stepRegistry, 'registerStep');

describe('WorkflowManualAccountDeletion > registerSteps', () => {
  it('Should register components', async () => {
    await import('./registerSteps');

    expect(registerFunc).toBeCalledWith(
      'GetStartedManualAccountDeletion',
      expect.anything()
    );

    expect(registerFunc).toBeCalledWith(
      'ConfigureParametersManualAccountDeletion',
      expect.anything(),
      [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANUAL_ACCOUNT_DELETION__CONFIGURE_PARAMETERS
      ]
    );
  });
});
