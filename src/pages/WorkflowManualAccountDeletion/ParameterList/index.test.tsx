import { render, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { queryAllByClass } from 'app/_libraries/_dls/test-utils/queryHelpers';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManualAccountDeletion';
import React from 'react';
import ParameterList from '.';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const useSelectElementMetadataForManualAccountDeletion = jest.spyOn(
  WorkflowSetup,
  'useSelectElementMetadataForManualAccountDeletion'
);

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <ParameterList {...props} />
    </div>
  );
};

describe('WorkflowBulkExternalStatusManagement > ParameterList', () => {
  beforeEach(() => {
    useSelectElementMetadataForManualAccountDeletion.mockReturnValue({
      manualAccountDeletionExternalStatusCode: [
        { code: 'code', text: 'text1' },
        { code: '', text: 'none' }
      ],
      manualAccountDeletionPropagate: [{ code: 'code', text: 'text2' }],
      manualAccountDeletionTransaction: [{ code: 'code', text: 'text' }],
      manualAccountDeletionTransactionId: [
        { code: 'code', text: 'text', description: 'nm.0.accounts' },
        {
          code: 'code2',
          text: 'text2',
          description: 'nm.0.charged.off.accounts'
        }
      ],
      manualAccountDeletionMN0TransactionId: [
        { code: 'nm.670', text: 'NM*670' },
        { code: 'nm.54', text: 'NM*54' }
      ]
    });
  });

  afterEach(() => {});

  it('Should render a grid with content', async () => {
    const props = {
      stepId: '1',
      selectedStep: '2',
      clearFormName: '',
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = await renderComponent({
      ...props,
      formValues: {
        isValid: false,
        transactionId: 'NM28',
        configureParameters: {}
      },
      dependencies: {
        attachmentId: '123'
      }
    });

    const expandBtn = queryAllByClass(wrapper.container, /icon icon-plus/)[0];
    userEvent.click(expandBtn);

    expect(wrapper.getByText('txt_field_name')).toBeInTheDocument();
    const minusBtn = queryAllByClass(wrapper.container, /icon icon-minus/)[0];
    userEvent.click(minusBtn);
  });
});
