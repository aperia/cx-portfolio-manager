import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import {
  stepRegistry,
  uploadFileStepRegistry
} from '../_commons/WorkflowSetup/registries';
import ConfigureParametersStep from './ConfigureParametersStep';
import GettingStartStep from './GettingStartStep';
import ParameterList from './ParameterList';

stepRegistry.registerStep('GetStartedManualAccountDeletion', GettingStartStep);

stepRegistry.registerStep(
  'ConfigureParametersManualAccountDeletion',
  ConfigureParametersStep,
  [
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANUAL_ACCOUNT_DELETION__CONFIGURE_PARAMETERS
  ]
);

stepRegistry.registerStep('GetStartedManualAccountDeletion', GettingStartStep);

uploadFileStepRegistry.registerComponent(
  'ManualAccountDeletionParameterList',
  ParameterList
);
