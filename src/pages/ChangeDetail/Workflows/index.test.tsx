import { fireEvent, render, RenderResult } from '@testing-library/react';
import 'app/utils/_mockComponent/mockFailedApiReload';
import 'app/utils/_mockComponent/mockNoDataFound';
import 'app/utils/_mockComponent/mockPaging';
import 'app/utils/_mockComponent/mockSortOrder';
import * as ChangeHook from 'pages/_commons/redux/Change/select-hooks';
import * as WorkflowHook from 'pages/_commons/redux/Workflow/select-hooks';
import React from 'react';
import * as reactRedux from 'react-redux';
import Workflows from './index';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    }),
    Pagination: ({
      page,
      pageSize,
      totalItem,
      onChangePage,
      onChangePageSize
    }: any) => {
      return (
        <div>
          <ul>
            <li className="page-number">{page}</li>
            <li className="page-size-value">{pageSize}</li>
            <li className="total-item">{totalItem}</li>
          </ul>
          <div>
            <button
              className="change-page-number"
              onClick={() => onChangePage(2)}
            >
              Page 2
            </button>
            <button
              className="change-page-size"
              onClick={() => onChangePageSize({ size: 25 })}
            >
              Page Size 25
            </button>
          </div>
        </div>
      );
    }
  };
});

jest.mock('app/_libraries/_dof/core', () => ({
  View: ({ formKey, descriptor }: any) => {
    return (
      <div id={formKey}>
        {formKey}
        <span>{descriptor}</span>
        <button id={`btn-${formKey}`}>BUTTON_ON_VIEW</button>
      </div>
    );
  }
}));

const mockReloadOnClose = jest.fn().mockReturnValue(true);
jest.mock('../Modals', () => {
  const actualModule = jest.requireActual('../Modals');
  return {
    ...actualModule,
    WorkflowDetailsModal: ({ workflowId, onClose }: any) => {
      return (
        <div
          onClick={() => {
            onClose(mockReloadOnClose());
          }}
          id={`WorkflowDetailsModal-${workflowId || 'not_selected'}`}
        >
          WorkflowDetailsModal_component
        </div>
      );
    }
  };
});

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');
const MockSelectCurrentChangeDetail = jest.spyOn(
  ChangeHook,
  'useSelectCurrentChangeDetail'
);

const MockSelectChangeWorkflowFilter = jest.spyOn(
  WorkflowHook,
  'useSelectChangeWorkflowFilter'
);
const MockSelectChangeWorkflows = jest.spyOn(
  WorkflowHook,
  'useSelectChangeWorkflows'
);

const renderComponent = (props: any): RenderResult => {
  return render(<Workflows {...props} />);
};

describe('Test ChangeDetail > Workflows', () => {
  const mockDispatch = jest.fn();
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
    mockReloadOnClose.mockReturnValue(true);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Should render with failed api', () => {
    MockSelectCurrentChangeDetail.mockReturnValue({} as any);

    MockSelectChangeWorkflows.mockReturnValue({
      error: true,
      loading: false,
      workflows: []
    } as any);

    MockSelectChangeWorkflowFilter.mockReturnValue({});
    const wrapper = renderComponent({ changeId: '1', editable: false });
    expect(wrapper.getByText('Data load unsuccessful')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('Reload'));
    expect(mockDispatch).toHaveBeenCalled();
  });

  it('Should render with no data found', () => {
    MockSelectCurrentChangeDetail.mockReturnValue({} as any);

    MockSelectChangeWorkflows.mockReturnValue({
      error: false,
      loading: false,
      workflows: [],
      totalItemFilter: 0
    } as any);

    MockSelectChangeWorkflowFilter.mockReturnValue({
      searchValue: '123'
    });
    const wrapper = renderComponent({ changeId: '1', editable: false });
    expect(wrapper.getByText('No data found')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('No data found'));
    expect(mockDispatch).toHaveBeenCalledWith({
      payload: undefined,
      type: 'workflow/clearAndResetChangeWorkflowsFilter'
    });
  });

  it('Should render data with editable is false', () => {
    MockSelectCurrentChangeDetail.mockReturnValue({
      loading: false,
      data: {
        changeStatus: '',
        changeId: '1',
        changeName: 'name 1'
      } as any
    });

    MockSelectChangeWorkflows.mockReturnValue({
      error: false,
      loading: false,
      workflows: [
        {
          name: 'wf-1',
          id: 'wf-id-1',
          validationMessages: [
            {
              severity: 'caution'
            }
          ],
          actions: ['edit']
        },
        {
          name: 'wf-2',
          id: 'wf-id-2',
          validationMessages: [
            {
              severity: 'error'
            }
          ]
        }
      ],
      totalItemFilter: 20,
      total: 20
    } as any);

    MockSelectChangeWorkflowFilter.mockReturnValue({
      searchValue: '123'
    });
    const wrapper = renderComponent({ changeId: '1', editable: false });
    expect(
      wrapper.baseElement.querySelectorAll('div.workflow-card-view').length
    ).toEqual(2);
  });

  it('Should render data with editable is true', () => {
    MockSelectCurrentChangeDetail.mockReturnValue({
      loading: false,
      data: {
        changeStatus: 'work',
        changeId: '1',
        changeName: 'name 1'
      } as any
    });

    MockSelectChangeWorkflows.mockReturnValue({
      error: false,
      loading: false,
      workflows: [
        {
          name: 'wf-1',
          id: 'wf-id-1',
          validationMessages: [
            {
              severity: 'caution'
            }
          ],
          actions: ['edit']
        },
        {
          name: 'wf-2',
          id: 'wf-id-2',
          validationMessages: [
            {
              severity: 'error'
            }
          ]
        }
      ],
      totalItemFilter: 20,
      total: 20
    } as any);

    MockSelectChangeWorkflowFilter.mockReturnValue({
      searchValue: '123'
    });
    const wrapper = renderComponent({ changeId: '1', editable: true });
    fireEvent.click(
      wrapper.baseElement.querySelector(
        'button#btn-change-workflow-card-view--right__changeWorkflowCardView__wf-id-1'
      ) as Element
    );

    fireEvent.click(
      wrapper.baseElement.querySelector(
        'div#change-workflow-card-view--right__changeWorkflowCardView__wf-id-1'
      ) as Element
    );

    expect(
      wrapper.baseElement.querySelector('div#WorkflowDetailsModal-wf-id-1')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.baseElement.querySelector(
        'div#WorkflowDetailsModal-wf-id-1'
      ) as Element
    );

    mockReloadOnClose.mockReturnValue(false);
    fireEvent.click(
      wrapper.baseElement.querySelector(
        'div#change-workflow-card-view--right__changeWorkflowCardView__wf-id-1'
      ) as Element
    );
    fireEvent.click(
      wrapper.baseElement.querySelector(
        'div#WorkflowDetailsModal-wf-id-1'
      ) as Element
    );
    expect(
      wrapper.baseElement.querySelector('div#WorkflowDetailsModal-wf-id-1')
    ).not.toBeInTheDocument();

    fireEvent.click(
      wrapper.baseElement.querySelector('i.icon.icon-search') as Element
    );
    expect(mockDispatch).toHaveBeenCalledWith({
      payload: { searchValue: '' },
      type: 'workflow/updateChangeWorkflowsFilter'
    });

    fireEvent.click(
      wrapper.container.querySelector('button.change-page-number') as Element
    );
    expect(mockDispatch).toHaveBeenCalledWith({
      payload: { page: 2 },
      type: 'workflow/updateChangeWorkflowsFilter'
    });

    fireEvent.click(
      wrapper.container.querySelector('button.change-page-size') as Element
    );
    expect(mockDispatch).toHaveBeenCalledWith({
      payload: { pageSize: 25 },
      type: 'workflow/updateChangeWorkflowsFilter'
    });

    fireEvent.click(
      wrapper.baseElement.querySelector(
        'button[class="sort-order--sort-btn"]'
      ) as Element
    );
    expect(mockDispatch).toHaveBeenCalled();

    fireEvent.click(
      wrapper.baseElement.querySelector(
        'button[class="sort-order--order-btn"]'
      ) as Element
    );
    expect(mockDispatch).toHaveBeenCalled();
    wrapper.debug();
  });
});
