import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React from 'react';
import CardView from './CardView';

jest.mock('app/_libraries/_dof/core', () => ({
  View: ({ formKey, descriptor }: any) => {
    return (
      <div id={formKey}>
        {formKey}
        <span>{descriptor}</span>
        <button id={`btn-${formKey}`}>BUTTON_ON_VIEW</button>
      </div>
    );
  }
}));

describe('Test ChangeDetail > Workflows > CardView', () => {
  it('render', () => {
    const onToggle = jest.fn();
    const props = {
      value: {},
      onToggle
    } as any;

    const wrapper = render(<CardView {...props} />);
    expect(
      wrapper.baseElement.querySelector('div#change-workflow-card-data-empty')
    ).toBeInTheDocument();

    userEvent.click(
      wrapper.baseElement.querySelector(
        '.btn.btn-icon-secondary .icon.icon-plus'
      )!
    );
    expect(onToggle).toBeCalledWith(undefined);
  });
  it('render with expaned', () => {
    const onToggle = jest.fn();
    const props = {
      value: { status: 'complete' },
      editable: true,
      isExpand: true,
      onToggle
    } as any;

    const wrapper = render(<CardView {...props} />);
    expect(
      wrapper.baseElement.querySelector(
        'div#change-workflow-card-view--expaned__empty'
      )
    ).toBeInTheDocument();

    userEvent.click(
      wrapper.baseElement.querySelector(
        '.btn.btn-icon-secondary .icon.icon-minus'
      )!
    );
    expect(onToggle).toBeCalledWith('');
  });
});
