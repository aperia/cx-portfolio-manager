import BadgeGroupTextControl from 'app/components/DofControl/BadgeGroupTextControl';
import { BadgeColorType } from 'app/constants/enums';
import { classnames } from 'app/helpers';
import {
  Button,
  Icon,
  Tooltip,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import { View } from 'app/_libraries/_dof/core';
import React from 'react';

export interface WorkflowCardViewProps {
  id: string;
  value: IWorkflowCardView;
  editable: boolean;
  isExpand?: boolean;
  onClick?: (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
  onToggle?: (workflowId: string) => void;
}
const CardView: React.FC<WorkflowCardViewProps> = ({
  id,
  value,
  editable,
  isExpand,
  onClick,
  onToggle
}) => {
  const { t } = useTranslation();
  const updateValue = { ...value, divWithDot: '' };
  if (editable) {
    const isCompletedWorkflow =
      updateValue.status?.toLowerCase() === 'complete';
    if (isCompletedWorkflow) {
      updateValue.actionPayload = value;
    } else {
      updateValue.continuePayload = value;
    }
  }

  const handleToggleCard = (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    e.stopPropagation();
    onToggle && onToggle(isExpand ? '' : updateValue.id);
  };

  return (
    <div
      id={`change-workflow-card-data-${id || 'empty'}`}
      className={classnames('list-view mt-12 workflow-card-view px-0 pb-0', {
        'border br-red': updateValue.workflowError
      })}
      onClick={onClick}
    >
      <div className="d-flex px-16 mb-8">
        <div>
          <Tooltip
            element={isExpand ? t('txt_collapse') : t('txt_expand')}
            variant="primary"
            placement="top"
          >
            <Button
              size="sm"
              variant="icon-secondary"
              onClick={handleToggleCard}
            >
              <Icon name={isExpand ? 'minus' : 'plus'} size="4x" />
            </Button>
          </Tooltip>
        </div>
        <div className="w-45">
          <div className="ml-8">
            <TruncateText
              id={id}
              title={updateValue?.name}
              className="h5"
              resizable
              suffix={{
                onlyDisplayOnTruncate: false,
                element: (
                  <>
                    {updateValue?.workflowError && (
                      <Icon
                        className="ml-8"
                        name="error"
                        size="5x"
                        color="red"
                      />
                    )}
                    {updateValue?.workflowWarning && (
                      <Icon
                        className="ml-8"
                        name="warning"
                        size="5x"
                        color="orange"
                      />
                    )}
                  </>
                )
              }}
            >
              {updateValue?.name}
            </TruncateText>
            <div className="mt-2">
              <BadgeGroupTextControl
                id={`change-workflow-card-view--left__${id || 'empty'}_status`}
                input={{ value: updateValue?.status } as any}
                meta={{} as any}
                options={{ colorType: BadgeColorType.WorkflowStatus }}
              />
            </div>
          </div>
        </div>
        <div className="w-45">
          <View
            id={`change-workflow-card-view--content__${id || 'empty'}`}
            formKey={`change-workflow-card-view--content__${id || 'empty'}`}
            descriptor="change-workflow-card-view--content"
            value={updateValue}
          />
        </div>
        <div className="w-10">
          <View
            id={`change-workflow-card-view--right__${id || 'empty'}`}
            formKey={`change-workflow-card-view--right__${id || 'empty'}`}
            descriptor="change-workflow-card-view--right"
            value={updateValue}
          />
        </div>
      </div>
      {isExpand && (
        <View
          id={`change-workflow-card-view--expaned__${id || 'empty'}`}
          formKey={`change-workflow-card-view--expaned__${id || 'empty'}`}
          descriptor="change-workflow-card-view--expaned"
          value={updateValue}
        />
      )}
    </div>
  );
};

export default CardView;
