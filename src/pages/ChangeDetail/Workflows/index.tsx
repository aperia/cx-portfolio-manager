import FailedApiReload from 'app/components/FailedApiReload';
import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import { PAGE_SIZE, WORKFLOWS_SORT_BY_LIST } from 'app/constants/constants';
import { classnames } from 'app/helpers';
import { pageSizeInfo, Pagination, useTranslation } from 'app/_libraries/_dls';
import { useSelectCurrentChangeDetail } from 'pages/_commons/redux/Change';
import {
  actionsWorkflow,
  useSelectChangeWorkflowFilter,
  useSelectChangeWorkflows
} from 'pages/_commons/redux/Workflow';
import { defaultDataFilter } from 'pages/_commons/redux/Workflow/reducers';
import SortOrder from 'pages/_commons/Utils/SortOrder';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';
import { WorkflowDetailsModal } from '../Modals';
import CardView from './CardView';

interface IProps {
  changeId: string;
  editable: boolean;
}
const Workflows: React.FC<IProps> = ({ changeId, editable }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const simpleSearchRef = useRef<any>(null);
  const [workflowExpanedId, setWorkflowExpanedId] = useState('');
  const [workflowDetailId, setWorkflowDetailId] = useState('');
  const { data: changeDetail = {} as any } = useSelectCurrentChangeDetail();

  const { workflows, total, totalItemFilter, loading, error } =
    useSelectChangeWorkflows();

  const dataFilter = useSelectChangeWorkflowFilter();
  const { page, pageSize, searchValue } = dataFilter;

  const showValidationFeedbackError = useMemo(
    () => changeDetail.changeStatus?.toLowerCase() === 'work',
    [changeDetail.changeStatus]
  );

  // pagination
  const handleOnChangePage = useCallback(
    (pageNumber: number) => {
      dispatch(
        actionsWorkflow.updateChangeWorkflowsFilter({ page: pageNumber })
      );
    },
    [dispatch]
  );
  const handleOnChangePageSize = useCallback(
    (pageInfo: pageSizeInfo) => {
      dispatch(
        actionsWorkflow.updateChangeWorkflowsFilter({ pageSize: pageInfo.size })
      );
    },
    [dispatch]
  );

  // sort by
  const handleChangeSortBy = (sortByValue: string) => {
    dispatch(
      actionsWorkflow.updateChangeWorkflowsFilter({
        sortBy: [sortByValue]
      })
    );
  };

  // order by
  const handleChangeOrderBy = (orderByValue: string) => {
    dispatch(
      actionsWorkflow.updateChangeWorkflowsFilter({
        orderBy: [orderByValue as OrderByType]
      })
    );
  };

  const handleSearch = (val: string) => {
    dispatch(
      actionsWorkflow.updateChangeWorkflowsFilter({
        searchValue: val
      })
    );
  };

  // clear and reset
  const handleClearAndReset = () => {
    dispatch(actionsWorkflow.clearAndResetChangeWorkflowsFilter());
    simpleSearchRef?.current?.clear();
  };

  useEffect(() => {
    dispatch(actionsWorkflow.getChangeWorkflows({ changeId }));

    return () => {
      dispatch(actionsWorkflow.setToDefault());
    };
  }, [dispatch, changeId]);

  const handleApiReload = useCallback(() => {
    dispatch(actionsWorkflow.getChangeWorkflows({ changeId }));
  }, [dispatch, changeId]);

  const rows = useMemo(() => {
    const onClickCard = (wfId: string) => {
      const handleClickCard = (
        e: React.MouseEvent<HTMLDivElement, MouseEvent>
      ) => {
        if ((e.target as Node)?.nodeName?.toLowerCase() === 'button') return;

        setWorkflowDetailId(wfId);
      };

      return handleClickCard;
    };

    return (
      <div className="mx-n24 px-24 mb-n24 pb-24">
        {workflows.map((item: IWorkflowModel) => {
          const value = { ...item } as IWorkflowCardView;
          const messages = value.validationMessages?.filter(
            m =>
              showValidationFeedbackError ||
              m.severity?.toLowerCase() === 'caution'
          );

          const hasError = messages?.some(
            e => e.severity?.toLowerCase() === 'error'
          );
          if (hasError) {
            value.workflowError = true;
          }

          const hasWarning = messages?.some(
            e => e.severity?.toLowerCase() === 'caution'
          );
          if (hasWarning) {
            value.workflowWarning = true;
          }

          return (
            <CardView
              key={item.id}
              id={`changeWorkflowCardView__${item.id}`}
              value={value}
              isExpand={workflowExpanedId === item.id}
              editable={
                editable &&
                item.actions?.some(i => i?.toLowerCase().includes('edit'))
              }
              onClick={onClickCard(item.id)}
              onToggle={setWorkflowExpanedId}
            />
          );
        })}
      </div>
    );
  }, [workflows, workflowExpanedId, showValidationFeedbackError, editable]);

  const pagination = useMemo(() => {
    if (totalItemFilter > PAGE_SIZE[0] && workflows.length > 0)
      return (
        <div className="mt-16">
          <Pagination
            totalItem={totalItemFilter}
            pageSize={PAGE_SIZE}
            pageNumber={page || 1}
            pageSizeValue={pageSize || PAGE_SIZE[0]}
            onChangePage={handleOnChangePage}
            onChangePageSize={handleOnChangePageSize}
          />
        </div>
      );
  }, [
    handleOnChangePage,
    handleOnChangePageSize,
    page,
    pageSize,
    totalItemFilter,
    workflows.length
  ]);

  return (
    <div className={classnames({ loading })} style={{ minHeight: '626px' }}>
      <div className="d-flex align-self-center">
        <h4>{t('txt_workflow_included_in_change')}</h4>
        <div className="ml-auto d-flex">
          {total > 0 && (
            <SimpleSearch
              ref={simpleSearchRef}
              placeholder={t('txt_type_to_search_by_workflow_name_and_desc')}
              onSearch={handleSearch}
            />
          )}
        </div>
      </div>
      {totalItemFilter > 0 && (
        <div className="mt-16 d-flex align-items-center ml-auto">
          <SortOrder
            hasFilter={!!searchValue}
            dataFilter={dataFilter}
            defaultDataFilter={defaultDataFilter}
            sortByFields={WORKFLOWS_SORT_BY_LIST}
            onChangeSortBy={handleChangeSortBy}
            onChangeOrderBy={handleChangeOrderBy}
            onClearSearch={handleClearAndReset}
          />
        </div>
      )}
      {error && (
        <FailedApiReload
          id="change-details--workflow-tab--error"
          onReload={handleApiReload}
          className="mt-40 pt-40 d-flex flex-column justify-content-center align-items-center"
        />
      )}
      {!error && totalItemFilter === 0 && !loading && (
        <div className="d-flex flex-column justify-content-center mt-80 mb-24">
          <NoDataFound
            id="change-detail-workflow"
            title={t('txt_no_workflows_to_display')}
            hasSearch={!!searchValue}
            linkTitle={searchValue && t('txt_clear_and_reset')}
            onLinkClicked={handleClearAndReset}
          />
        </div>
      )}
      {rows}
      {pagination}

      <WorkflowDetailsModal
        id={`${changeId}_workflowDetailModal`}
        show={!!workflowDetailId}
        workflowId={workflowDetailId}
        editable={editable}
        onClose={reload => {
          setWorkflowDetailId('');

          if (reload) {
            dispatch(actionsWorkflow.getChangeWorkflows({ changeId }));
          }
        }}
      />
    </div>
  );
};

export default Workflows;
