import { useTranslation } from 'app/_libraries/_dls';
import PricingStrategyList from 'pages/_commons/PricingStrategy/List';
import React from 'react';

interface IProps {
  changeId: string;
}

const PricingStrategies: React.FC<IProps> = ({ changeId }) => {
  const { t } = useTranslation();
  return (
    <PricingStrategyList
      containerClassName={''}
      title={t('txt_pricing_strategy_list')}
      changeId={changeId}
    />
  );
};

export default PricingStrategies;
