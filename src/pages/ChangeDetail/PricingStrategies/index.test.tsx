import { render } from '@testing-library/react';
import React from 'react';
import PricingStrategies from './index';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

jest.mock('pages/_commons/PricingStrategy/List', () => ({
  __esModule: true,
  default: () => {
    return <div>PricingStrategyList_component</div>;
  }
}));

describe('Test ChangeDetail > PricingStrategies', () => {
  it('Should render PricingStrategies', () => {
    const wrapper = render(<PricingStrategies changeId={'1'} />);
    expect(
      wrapper.getByText('PricingStrategyList_component')
    ).toBeInTheDocument();
  });
});
