import { fireEvent } from '@testing-library/react';
import { renderComponent } from 'app/utils';
import * as ChangeSelector from 'pages/_commons/redux/Change/select-hooks';
import React from 'react';
import * as ReactRedux from 'react-redux';
import ChangeDetail from './';

jest.mock('./DMMTables', () => ({
  __esModule: true,
  default: () => <div>DMM Tables tab</div>
}));
// jest.mock('./Portfolios', () => ({
//   __esModule: true,
//   default: () => <div>Portfolios tab</div>
// }));
jest.mock('./PricingMethods', () => ({
  __esModule: true,
  default: () => <div>Pricing Methods tab</div>
}));
jest.mock('./PricingStrategies', () => ({
  __esModule: true,
  default: () => <div>Pricing Strategies tab</div>
}));
jest.mock('./Workflows', () => ({
  __esModule: true,
  default: () => <div>Workflows tab</div>
}));
jest.mock('./Snapshot', () => ({
  __esModule: true,
  default: () => <div>Snapshot section</div>
}));
jest.mock('react-router-dom', () => ({
  useParams: () => '10000001'
}));

const mockSelectChangeDetail = jest.spyOn(
  ChangeSelector,
  'useSelectCurrentChangeDetail'
);

const mockSelectDataForExportLoading = jest.spyOn(
  ChangeSelector,
  'useSelectDataForExportLoading'
);

const mockDispatch = jest.spyOn(ReactRedux, 'useDispatch');
const mockSelector = jest.spyOn(ReactRedux, 'useSelector');

const ChangeData: any = {
  changeId: '1',
  changeType: 'pricing'
};

const testId = 'Change Detail';
describe('Page > Change detail', () => {
  const dispatchFn = jest.fn();

  beforeEach(() => {
    mockDispatch.mockReturnValue(dispatchFn);
    mockSelector.mockImplementation(selector =>
      selector({ change: { changeValidateStatus: { loading: false } } })
    );
    mockSelectChangeDetail.mockReturnValue({
      data: ChangeData,
      loading: false,
      error: false
    });
    mockSelectDataForExportLoading.mockReturnValue(false);
  });
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render Change Detail UI, change type is pricing', async () => {
    const { getByText } = await renderComponent(testId, <ChangeDetail />);
    expect(getByText('Snapshot section')).toBeInTheDocument();
    expect(getByText('Workflows')).toBeInTheDocument();
    // expect(getByText('Portfolios')).toBeInTheDocument();
    expect(getByText('Pricing Strategies')).toBeInTheDocument();
    expect(getByText('Pricing Methods')).toBeInTheDocument();
    expect(getByText('DMM Tables')).toBeInTheDocument();
  });
  it('should render Change Detail UI, change type is non-pricing', async () => {
    mockSelectChangeDetail.mockReturnValue({
      data: { ...ChangeData, changeType: 'accountManagement' },
      loading: false,
      error: false
    });
    const { getByText, queryByText } = await renderComponent(
      testId,
      <ChangeDetail />
    );
    expect(getByText('Snapshot section')).toBeInTheDocument();
    expect(getByText('Workflows')).toBeInTheDocument();
    // expect(getByText('Portfolios')).toBeInTheDocument();
    expect(queryByText('Pricing Strategies')).toBeNull();
    expect(queryByText('Pricing Methods')).toBeNull();
    expect(queryByText('DMM Tables')).toBeNull();
  });
  it('should show loading when waiting change detail data from API', async () => {
    mockSelectChangeDetail.mockReturnValue({
      data: {} as any,
      loading: true,
      error: false
    });
    const { getByTestId } = await renderComponent(testId, <ChangeDetail />);
    expect(
      getByTestId(testId).querySelector('.h-100.loading.full-loading')
    ).toBeInTheDocument();
  });

  it('should show loading when refreshing data from API', async () => {
    mockSelectChangeDetail.mockReturnValue({
      data: ChangeData,
      loading: true,
      error: false
    });
    mockSelector.mockImplementation(selector =>
      selector({ change: { changeValidateStatus: { loading: true } } })
    );
    const { getByTestId, getByText } = await renderComponent(
      testId,
      <ChangeDetail />
    );

    expect(getByTestId(testId).querySelector('.loading')).toBeInTheDocument();
    expect(getByText('Snapshot section')).toBeInTheDocument();
    fireEvent.click(
      getByTestId(testId).querySelector(
        'a[data-rb-event-key="changeDetail_pricingStrategies"]'
      ) as Element
    );
    expect(
      getByTestId(testId)
        .querySelector('a[data-rb-event-key="changeDetail_pricingStrategies"]')
        ?.getAttribute('aria-selected')
    ).toBe('true');
  });
});
