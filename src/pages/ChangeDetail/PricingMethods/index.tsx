import { useTranslation } from 'app/_libraries/_dls';
import PricingMethodList from 'pages/_commons/PricingMethod/List';
import React from 'react';

interface IProps {
  changeId?: string;
}
const PricingMethods: React.FC<IProps> = ({ changeId = '' }) => {
  const { t } = useTranslation();
  return (
    <PricingMethodList
      containerClassName={''}
      title={t('txt_pricing_method_list')}
      changeId={changeId}
    />
  );
};

export default PricingMethods;
