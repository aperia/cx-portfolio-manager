import { render } from '@testing-library/react';
import React from 'react';
import PricingMethods from './index';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

jest.mock('pages/_commons/PricingMethod/List', () => ({
  __esModule: true,
  default: ({ changeId }: any) => {
    return (
      <div>PricingMethodList_component{changeId ? `_${changeId}` : ''}</div>
    );
  }
}));

describe('Test ChangeDetail > PricingMethods', () => {
  it('Should render PricingMethods with change id is not empty', () => {
    const wrapper = render(<PricingMethods changeId={'1'} />);
    expect(
      wrapper.getByText('PricingMethodList_component_1')
    ).toBeInTheDocument();
  });

  it('Should render PricingMethods with change id is empty', () => {
    const props = {} as any;
    const wrapper = render(<PricingMethods {...props} />);
    expect(
      wrapper.getByText('PricingMethodList_component')
    ).toBeInTheDocument();
  });
});
