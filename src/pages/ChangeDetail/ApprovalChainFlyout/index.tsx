import ModalRegistry from 'app/components/ModalRegistry';
import { ChangeStatus } from 'app/constants/enums';
import { classnames, formatCommon } from 'app/helpers';
import {
  Bubble,
  Button,
  Icon,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  SimpleBar,
  useTranslation
} from 'app/_libraries/_dls';
import isArray from 'lodash.isarray';
import isEmpty from 'lodash.isempty';
import orderBy from 'lodash.orderby';
import React, { useCallback, useMemo, useState } from 'react';

export interface IApprovalChainFlyoutProps {
  id: string;
  title?: string;
  show: boolean;
  approvalByGroup?: IPendingApprovalBy;
  snapshot: IChangeDetail;
  onCloseModal: () => void;
}

const MAX_APPROVALGROUP = 100;

const ApprovalChainFlyout: React.FC<IApprovalChainFlyoutProps> = ({
  id,
  title,
  snapshot,
  approvalByGroup,
  show,
  onCloseModal
}) => {
  const { approvalRecords, changeStatus } = snapshot;

  const [selectedTeam, setSelectedTeam] =
    useState<undefined | string>(undefined);

  const pendingApprovalBy = useMemo(() => {
    let numOfCall = 0;
    const groups: IPendingApprovalBy[] = [];
    const getApprovalGroupBy = (group: IPendingApprovalBy | undefined) => {
      if (++numOfCall > MAX_APPROVALGROUP || isEmpty(group)) return;
      groups.push({ ...group!, nextApproverGroup: undefined });
      getApprovalGroupBy(group!.nextApproverGroup);
    };
    getApprovalGroupBy(approvalByGroup);
    return groups;
  }, [approvalByGroup]);

  const { t } = useTranslation();

  const getActionText = (action: string) => {
    switch (action?.toLowerCase()) {
      case 'approve':
        return t('txt_approval_chain_approved_by');
      case 'reject':
        return t('txt_approval_chain_rejected_by');
    }
    return '';
  };

  // modal close
  const handleModalClosed = useCallback(() => {
    onCloseModal && onCloseModal();
  }, [onCloseModal]);

  const renderApprovalRecords = () => {
    if (isEmpty(approvalRecords)) return;
    return (
      <>
        {orderBy(approvalRecords, ['actionTime'])?.map((approvalItem, idx) => {
          const { user, group, action, reason, actionTime } = approvalItem;
          return (
            <div
              key={idx}
              className={classnames(
                'list-item ',
                action?.toLowerCase() === 'reject' ? 'danger' : 'success'
              )}
            >
              <div className="d-flex">
                <p className="mr-4">{getActionText(action)}</p>
                <Bubble name={user?.name} small />
                <p className="px-4">
                  {user?.name} - {group?.name} {t('txt_approval_chain_team')}
                </p>
              </div>
              <p className="color-grey fs-12 mt-4 text-secondary">
                {formatCommon(actionTime).time.dayOfWeekDate}
              </p>
              {action?.toLowerCase() === 'reject' && reason && (
                <p className="mt-8 text-secondary">
                  • {t('txt_approval_chain_reason')}: {reason}
                </p>
              )}
            </div>
          );
        })}
      </>
    );
  };

  const renderPendingApproval = () => {
    if (
      isEmpty(pendingApprovalBy) ||
      !isArray(pendingApprovalBy) ||
      changeStatus != ChangeStatus.PendingApproval
    )
      return;

    return (
      <div className="list-item warning">
        <div className="d-flex">
          <p>{t('txt_approval_chain_waiting_for_approval')}</p>
        </div>
        {pendingApprovalBy?.map((team, idx) => (
          <div key={idx} className="list-content">
            <Button
              variant="outline-primary"
              className="align-items-center d-flex ml-n8 fs-14 my-8 text-secondary bg-transparent"
              size="sm"
              onClick={() => {
                setSelectedTeam(teamId =>
                  teamId === team.id ? undefined : team.id
                );
              }}
            >
              • {team.name}
              <Icon
                className="ml-4"
                name={selectedTeam === team.id ? 'chevron-up' : 'chevron-down'}
                size="3x"
              />
            </Button>

            {(team?.user?.length || 0) > 0 && selectedTeam === team.id && (
              <div className="rounded-md px-16 pt-16 pb-8 bg-light-l20">
                <ul className="nav">
                  {orderBy(team?.user, ['name']).map((u, uIdx) => (
                    <li key={uIdx} className="nav-item">
                      <Bubble name={u.name} small />
                      <p className="px-4">{u.name}</p>
                    </li>
                  ))}
                </ul>
              </div>
            )}
          </div>
        ))}
      </div>
    );
  };

  return (
    <ModalRegistry
      id={id}
      rt
      enforceFocus={false}
      scrollable={false}
      show={show}
      animationDuration={500}
      animationComponentProps={{ direction: 'right' }}
      classes={{
        content: 'custom-modal'
      }}
      onAutoClosedAll={handleModalClosed}
    >
      <ModalHeader border closeButton onHide={handleModalClosed}>
        <ModalTitle>{title}</ModalTitle>
      </ModalHeader>
      <ModalBody className={classnames('p-0 overflow-auto')}>
        <SimpleBar>
          <div className="p-24">
            <h5 className="mb-24 fw-700">{t('txt_approval_chain')}</h5>
            <div className="approval-change list">
              {renderApprovalRecords()}
              {renderPendingApproval()}
            </div>
          </div>
        </SimpleBar>
      </ModalBody>
      <ModalFooter
        border
        cancelButtonText="Close"
        onCancel={handleModalClosed}
      />
    </ModalRegistry>
  );
};

export default ApprovalChainFlyout;
