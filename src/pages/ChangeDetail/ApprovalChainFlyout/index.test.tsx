import { fireEvent } from '@testing-library/react';
import { ChangeStatus } from 'app/constants/enums';
import { mockUseModalRegistryFnc, renderComponent } from 'app/utils';
import React from 'react';
import ApprovalChainFlyout, { IApprovalChainFlyoutProps } from './';

const mockUseModalRegistry = mockUseModalRegistryFnc();

const mockProps: IApprovalChainFlyoutProps = {
  id: 'id01',
  title: 'Approval Chain',
  show: true,
  approvalByGroup: {
    user: [
      {
        id: 1834997615,
        name: 'Nate Nash'
      },
      {
        id: 1909125938,
        name: 'Luke Skywalker'
      }
    ],
    users: [
      {
        id: 1834997615,
        name: 'Nate Nash'
      },
      {
        id: 1909125938,
        name: 'Luke Skywalker'
      }
    ],
    id: '1971083673',
    name: 'consequat vulputate et invidunt autem eum dolore',
    changeType: 'no dolore eos ut',
    nextApproverGroup: {}
  } as IPendingApprovalBy,
  snapshot: {
    approvalRecords: [
      {
        action: 'approve',
        reason: 'abc',
        actionTime: new Date('2021-05-03T10:27:25.0191871+07:00'),
        user: {
          id: 599207098,
          name: 'Darth Vader'
        },
        group: {
          name: 'John Doe'
        }
      },
      {
        action: 'reject',
        reason: 'abc',
        actionTime: new Date('2021-05-03T10:27:25.0191871+07:00'),
        user: {
          id: 599207098,
          name: 'Darth Vader'
        },
        group: {
          name: 'John Doe'
        }
      },
      {
        action: 'pending',
        reason: 'abc',
        actionTime: new Date('2021-05-03T10:27:25.0191871+07:00'),
        user: {
          id: 599207098,
          name: 'Darth Vader'
        },
        group: {
          name: 'John Doe'
        }
      }
    ],
    changeStatus: ChangeStatus.PendingApproval
  } as IChangeDetail,
  onCloseModal: jest.fn()
};

const then_close_modal = () => {
  const element = document.querySelector(
    '.dls-modal-root .dls-modal-footer button.btn-secondary'
  );
  fireEvent.click(element!);
};

const testId = 'approvalChain';
describe('ApprovalChain ', () => {
  const createWrapper = async (...props: IApprovalChainFlyoutProps[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    ) as IApprovalChainFlyoutProps;
    await renderComponent(testId, <ApprovalChainFlyout {...mergeProps} />);
  };

  beforeEach(() => {
    mockUseModalRegistry.mockImplementation(() => [true, false]);
  });

  afterEach(() => {
    mockUseModalRegistry.mockClear();
  });

  it('should show approval chain modal with 3 approval records and 1 pending approval by team', async () => {
    await createWrapper(mockProps);

    const approvalTeamElement = document.body
      .querySelectorAll('.btn-outline-primary')
      .item(0);

    fireEvent.click(approvalTeamElement);
    expect(document.body.querySelectorAll('.list-item').length).toEqual(4);
    expect(document.body.querySelectorAll('.nav-item').length).toEqual(2);
    fireEvent.click(approvalTeamElement);
  });

  it('should show approval chain modal with 3 approval records and 0 pending approval by team', async () => {
    await createWrapper({
      ...mockProps,
      changeStatus: 'work',
      approvalByGroup: undefined,
      snapshot: { ...mockProps.snapshot }
    } as any);

    expect(document.body.querySelectorAll('.list-item').length).toEqual(3);
    expect(document.body.querySelectorAll('.nav-item').length).toEqual(0);
    then_close_modal();
  });

  it('should show approval chain modal with 3 approval records and 0 pending approval by team', async () => {
    await createWrapper({
      ...mockProps,
      changeStatus: 'work',
      approvalByGroup: undefined,
      snapshot: {
        ...mockProps.snapshot,
        approvalRecords: []
      }
    } as any);

    expect(document.body.querySelectorAll('.list-item').length).toEqual(0);
    expect(document.body.querySelectorAll('.nav-item').length).toEqual(0);
    then_close_modal();
  });
});
