import { ChangeType } from 'app/constants/enums';
import { classnames } from 'app/helpers';
import { HorizontalTabs, SimpleBar } from 'app/_libraries/_dls';
import isEqual from 'lodash.isequal';
import {
  actionsChange,
  useSelectCurrentChangeDetail,
  useSelectDataForExportLoading
} from 'pages/_commons/redux/Change';
import React, { useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import DMMTables from './DMMTables';
// import Portfolios from './Portfolios';
import PricingMethods from './PricingMethods';
import PricingStrategies from './PricingStrategies';
import Snapshot from './Snapshot';
import Workflows from './Workflows';

interface IURLParams {
  id: string;
}
interface IProps {}

const DEFAULT_TAB_ID = 'changeDetail_workflows';

const ChangeDetail: React.FC<IProps> = () => {
  const { id: changeId } = useParams<IURLParams>();
  const dispatch = useDispatch();
  const {
    data: changeSnapshot,
    loading,
    error
  } = useSelectCurrentChangeDetail();
  const exportLoading = useSelectDataForExportLoading();

  const validateLoading = useSelector<RootState, boolean>(state => {
    return state.change?.changeValidateStatus.loading || false;
  });

  useEffect(() => {
    dispatch(actionsChange.getChangeDetail(changeId));
    return () => setActiveTabKey(DEFAULT_TAB_ID);
  }, [dispatch, changeId]);

  const tabs = useMemo(() => {
    const defaultTabs = [
      {
        id: 'changeDetail_workflows',
        title: 'Workflows',
        component: <Workflows changeId={changeId} editable />
      }
      // {
      //   id: 'changeDetail_portfolios',
      //   title: 'Portfolios',
      //   component: <Portfolios changeId={changeId} />
      // }
    ];
    if (isEqual(changeSnapshot?.changeType, ChangeType.Pricing)) {
      defaultTabs.push({
        id: 'changeDetail_pricingStrategies',
        title: 'Pricing Strategies',
        component: <PricingStrategies changeId={changeId} />
      });
      defaultTabs.push({
        id: 'changeDetail_pricingMethods',
        title: 'Pricing Methods',
        component: <PricingMethods changeId={changeId} />
      });
      defaultTabs.push({
        id: 'changeDetail_dmmTables',
        title: 'DMM Tables',
        component: <DMMTables changeId={changeId} />
      });
    }
    return defaultTabs;
  }, [changeId, changeSnapshot]);

  const [activeTabKey, setActiveTabKey] = useState(tabs[0].id);

  if (loading && !changeSnapshot?.changeId) {
    return <div className="h-100 loading full-loading" />;
  }

  const handleTabSelected = (
    eventKey: string | null,
    e: React.SyntheticEvent<unknown>
  ) => {
    setActiveTabKey(eventKey!);
  };

  return (
    <SimpleBar
      className={classnames({
        loading:
          (loading || validateLoading || exportLoading) &&
          changeSnapshot?.changeId
      })}
    >
      <div>
        <Snapshot
          changeId={changeId}
          snapshot={changeSnapshot!}
          loading={loading}
          error={error}
        />
      </div>
      <div className="bg-light-l20">
        <HorizontalTabs
          activeKey={activeTabKey}
          mountOnEnter
          onSelect={handleTabSelected}
        >
          {tabs.map(({ id, title, component }) => (
            <HorizontalTabs.Tab key={id} eventKey={id} title={title}>
              <div className="p-24 bg-white">{component}</div>
            </HorizontalTabs.Tab>
          ))}
        </HorizontalTabs>
      </div>
    </SimpleBar>
  );
};

export default ChangeDetail;
