import EnhanceDropdownButton from 'app/components/EnhanceDropdownButton';
import { SnapshotAction } from 'app/constants/enums';
import { classnames } from 'app/helpers';
import { Button, DropdownButton, useTranslation } from 'app/_libraries/_dls';
import React, { useMemo } from 'react';

export interface IButtonProps {
  label: string;
  value?: SnapshotAction;
  variant?: 'outline-primary' | 'outline-danger';
}
interface IActionButtonListProps {
  actions: string[];
  onButtonClicked?: (btnValue: SnapshotAction) => void;
}

const ActionButtonList: React.FC<IActionButtonListProps> = ({
  actions,
  onButtonClicked
}) => {
  const { t } = useTranslation();

  const handleBtnClicked = (btnValue: SnapshotAction) => {
    onButtonClicked && onButtonClicked(btnValue);
  };

  const actionButtons = useMemo<Record<string, IButtonProps>>(
    () => ({
      validate: {
        label: t('txt_snapshot_validate'),
        value: SnapshotAction.VALIDATE
      },
      submit: {
        label: t('txt_snapshot_submit_for_approval'),
        value: SnapshotAction.SUBMIT_FOR_APPROVAL
      },
      delete: { label: t('txt_snapshot_delete'), value: SnapshotAction.DELETE },
      edit: { label: t('txt_snapshot_edit'), value: SnapshotAction.EDIT },
      export: { label: t('txt_snapshot_export'), value: SnapshotAction.EXPORT },
      viewactivities: {
        label: t('txt_snapshot_view_activities'),
        value: SnapshotAction.VIEW_ACTIVITIES
      },
      demote: { label: t('txt_snapshot_demote'), value: SnapshotAction.DEMOTE },
      approve: {
        label: t('txt_snapshot_approve'),
        value: SnapshotAction.APPROVE
      },
      reject: {
        label: t('txt_snapshot_reject'),
        value: SnapshotAction.REJECT,
        variant: 'outline-danger'
      }
    }),
    [t]
  );

  const buttons = useMemo<IButtonProps[]>(
    () =>
      [
        ...buttonOrders.filter(a => (actions || []).includes(a)),
        'export',
        'viewactivities'
      ]
        .map(a => actionButtons[a?.toLowerCase()])
        .filter(i => !!i),

    [actions, actionButtons]
  );

  return (
    <React.Fragment>
      {buttons.length <= 3 ? (
        <div className={classnames({ 'd-none': buttons.length > 3 })}>
          {buttons.map(buttonItem => {
            return (
              <Button
                key={buttonItem.value}
                variant={buttonItem.variant || 'outline-primary'}
                onClick={() => handleBtnClicked(buttonItem.value!)}
              >
                {buttonItem.label}
              </Button>
            );
          })}
        </div>
      ) : (
        <div className={classnames({ 'd-none': buttons.length <= 3 })}>
          {buttons.slice(0, 2).map(buttonItem => {
            return (
              <Button
                key={buttonItem.value}
                variant={buttonItem.variant || 'outline-primary'}
                onClick={() => handleBtnClicked(buttonItem.value!)}
              >
                {buttonItem.label}
              </Button>
            );
          })}
          <EnhanceDropdownButton
            buttonProps={{
              children: t('txt_actions'),
              variant: 'outline-primary'
            }}
            popupBaseProps={{
              placement: 'bottom-end'
            }}
            onSelect={event => {
              handleBtnClicked(event.target.value);
            }}
          >
            {buttons.slice(2).map(buttonItem => {
              return (
                <DropdownButton.Item
                  key={buttonItem.value}
                  label={buttonItem.label}
                  value={buttonItem.value}
                />
              );
            })}
          </EnhanceDropdownButton>
        </div>
      )}
    </React.Fragment>
  );
};

const buttonOrders = [
  'validate',
  'submit',
  'delete',
  'edit',
  'demote',
  'approve',
  'reject'
];

export default ActionButtonList;
