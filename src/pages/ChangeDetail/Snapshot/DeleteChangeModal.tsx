import ModalRegistry from 'app/components/ModalRegistry';
import {
  Button,
  ModalBody,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import isFunction from 'lodash.isfunction';
import React from 'react';

interface IProps {
  id: string;
  name: string;
  show: boolean;
  onClose: () => void;
  onRemove: () => void;
}

const DeleteChangeModal: React.FC<IProps> = ({
  id,
  name,
  show,
  onClose,
  onRemove
}) => {
  const { t } = useTranslation();

  const handleRemove = () => {
    isFunction(onRemove) && onRemove();
  };

  const handleClose = () => {
    isFunction(onClose) && onClose();
  };

  return (
    <ModalRegistry
      id={id}
      show={show}
      onAutoClosedAll={handleClose}
      classes={{ content: '' }}
    >
      <ModalHeader border closeButton onHide={handleClose}>
        <ModalTitle>{t('txt_confirm_deletion')}</ModalTitle>
      </ModalHeader>
      <ModalBody>
        <p>{t('txt_snapshot_confirm_msg_delete', { changeName: name })}</p>
      </ModalBody>
      <div className="dls-modal-footer modal-footer">
        <Button variant="secondary" onClick={handleClose}>
          {t('txt_cancel')}
        </Button>
        <Button variant="danger" onClick={handleRemove}>
          {t('txt_delete')}
        </Button>
      </div>
    </ModalRegistry>
  );
};

export default DeleteChangeModal;
