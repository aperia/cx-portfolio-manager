import FailedApi from 'app/components/FailedApiReload';
import { ChangeStatus, SnapshotAction } from 'app/constants/enums';
import { CHANGE_MANAGEMENT_URL } from 'app/constants/links';
import { canSetupWorkflow, convertNameToCode } from 'app/helpers';
import { InlineMessage, useTranslation } from 'app/_libraries/_dls';
import { View } from 'app/_libraries/_dof/core';
import isEmpty from 'lodash.isempty';
import {
  actionsChange,
  useSelectApprovalByGroup,
  useSelectCurrentChangeInvalidMessages,
  useSelectOpenWorkflowDetail
} from 'pages/_commons/redux/Change';
import {
  actionsWorkflow,
  useSelectChangeWorkflowsInvalidMessages
} from 'pages/_commons/redux/Workflow';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import { actionsToast } from 'pages/_commons/ToastNotifications/redux';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
import ActivityFlyout from '../ActivityFlyout';
import ApprovalChainFlyout from '../ApprovalChainFlyout';
import BreadCrumbs from '../BreadCrumbs';
import {
  ApproveChangeModal,
  DemoteChangeModal,
  EditChangeModal,
  RejectChangeModal,
  RejectionNoteModal,
  RescheduleChangeModal,
  SubmitChangeModal,
  ValidationFeedbackModal,
  WorkflowDetailsModal
} from '../Modals';
import ActionButtonList from './ActionButtonList';
import DeleteChangeModal from './DeleteChangeModal';

interface IProps {
  snapshot: IChangeDetail;
  loading?: boolean;
  error?: boolean;
  changeId: string;
}

const Snapshot: React.FC<IProps> = ({ changeId, snapshot, error, loading }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { t } = useTranslation();

  const [showActivity, setShowActivity] = useState<boolean>(false);
  const [showEditChange, setShowEditChange] = useState(false);
  const [showDeleteChange, setShowDeleteChange] = useState(false);
  const [showRescheduleChange, setShowRescheduleChange] = useState(false);
  const [showRejectionNote, setShowRejectionNote] = useState(false);
  const [showApproveChange, setShowApproveChange] = useState(false);
  const [showRejectChange, setShowRejectChange] = useState(false);
  const [showDemoteChange, setShowDemoteChange] = useState(false);
  const [showValidationFeedback, setShowValidationFeedback] = useState(false);
  const [showSubmitChange, setShowSubmitChange] = useState(false);
  const [showApprovalChain, setApprovalChain] = useState(false);

  const invalidMessagesWorkflows = useSelectChangeWorkflowsInvalidMessages();
  const invalidMessagesChange = useSelectCurrentChangeInvalidMessages();
  const workflowDetailId = useSelectOpenWorkflowDetail();
  const { loading: loadingApprovalBy, approvalByGroup } =
    useSelectApprovalByGroup();

  const isCautionValidationFeedback = useMemo(
    () =>
      [
        'validated',
        'clientpendinglevel',
        'pendingapproval',
        'rejected',
        'lapsed'
      ].some(a => convertNameToCode(snapshot.changeStatus || '').includes(a)),
    [snapshot.changeStatus]
  );
  const showValidationFeedbackMessage = useMemo(
    () =>
      isCautionValidationFeedback ||
      snapshot.changeStatus?.toLowerCase() === 'work',
    [isCautionValidationFeedback, snapshot.changeStatus]
  );
  const messages = useMemo(
    () =>
      [...invalidMessagesWorkflows, ...invalidMessagesChange].filter(
        m =>
          !isCautionValidationFeedback ||
          m.severity?.toLowerCase() === 'caution'
      ),
    [
      isCautionValidationFeedback,
      invalidMessagesChange,
      invalidMessagesWorkflows
    ]
  );

  const isAllCaution = useMemo(
    () =>
      showValidationFeedbackMessage &&
      messages.every(m => m.severity?.toLowerCase() === 'caution'),
    [showValidationFeedbackMessage, messages]
  );

  const showRescheduleChangeMessage = useMemo(
    () => snapshot.changeStatus?.toLowerCase() === 'lapsed',
    [snapshot.changeStatus]
  );
  const canRescheduleChange = useMemo(
    () =>
      showRescheduleChangeMessage &&
      snapshot.actions?.some(i => i?.toLowerCase().includes('reschedule')),
    [showRescheduleChangeMessage, snapshot.actions]
  );

  const showEditChangeMessageOnRejected = useMemo(
    () => snapshot.changeStatus?.toLowerCase() === 'rejected',
    [snapshot.changeStatus]
  );

  const canEditChange = useMemo(
    () => snapshot.actions?.some(i => i?.toLowerCase().includes('edit')),
    [snapshot.actions]
  );

  const validationFeedback = useMemo(
    () =>
      isAllCaution
        ? t('txt_change_validation_feedback_message_cautions')
        : t('txt_change_validation_feedback_message'),
    [t, isAllCaution]
  );

  const handeActivityClosed = useCallback(() => {
    setShowActivity(false);
  }, []);

  const handleShowApprovalChain = useCallback(() => {
    setApprovalChain(true);
  }, []);

  const handleApiReload = useCallback(() => {
    dispatch(actionsChange.getChangeDetail(changeId));
  }, [dispatch, changeId]);

  const handleReloadAfterEditWorkflow = useCallback(() => {
    dispatch(actionsChange.getChangeDetail(changeId));
    dispatch(actionsWorkflow.getChangeWorkflows({ changeId }));
  }, [dispatch, changeId]);

  useEffect(() => {
    const urlQuery = new URLSearchParams(history?.location?.search);
    const isFromImportExportWF =
      urlQuery.get('notifyFrom') === 'import-export-workflow';

    if (isFromImportExportWF && snapshot?.changeName) {
      dispatch(
        actionsToast.addToast({
          type: 'success',
          message: t(
            'txt_export_impor_change_details__notification_view_detail_success',
            {
              changeName: snapshot?.changeName
            }
          )
        })
      );
    }
  }, [dispatch, t, snapshot?.changeName, history]);

  useEffect(() => {
    return () => {
      dispatch(actionsChange.setToDefault());
      dispatch(actionsChange.clearExportData());
    };
  }, [dispatch]);

  useEffect(() => {
    snapshot.changeStatus === ChangeStatus.PendingApproval &&
      dispatch(actionsChange.getPendingApprovalGroup(changeId));
  }, [dispatch, changeId, snapshot.changeStatus]);

  const handleValidateChange = useCallback(async () => {
    const result = await (dispatch(
      actionsChange.validateChange({ changeId })
    ) as any);

    const isSuccess = actionsChange.validateChange.fulfilled.match(result);
    if (isSuccess) {
      await (dispatch(actionsChange.getChangeDetail(changeId)) as any);
      await (dispatch(actionsWorkflow.getChangeWorkflows({ changeId })) as any);

      dispatch(
        actionsToast.addToast({
          type: 'success',
          message: t('txt_validation_change_sent_for_validation_success')
        })
      );
    } else {
      dispatch(
        actionsToast.addToast({
          type: 'error',
          message: t('txt_validation_change_sent_for_validation_fail')
        })
      );
    }
  }, [t, dispatch, changeId]);

  const handleActionButtonClicked = useCallback(
    (value: SnapshotAction) => {
      switch (value) {
        case SnapshotAction.VIEW_ACTIVITIES:
          setShowActivity(true);
          break;
        case SnapshotAction.EXPORT:
          dispatch(actionsChange.exportChangeSet({ ...snapshot }));
          break;
        case SnapshotAction.EDIT:
          setShowEditChange(true);
          break;
        case SnapshotAction.DELETE:
          setShowDeleteChange(true);
          break;
        case SnapshotAction.APPROVE:
          setShowApproveChange(true);
          break;
        case SnapshotAction.REJECT:
          setShowRejectChange(true);
          break;
        case SnapshotAction.DEMOTE:
          setShowDemoteChange(true);
          break;
        case SnapshotAction.VALIDATE:
          handleValidateChange();
          break;
        case SnapshotAction.SUBMIT_FOR_APPROVAL:
          setShowSubmitChange(true);
          break;
      }
    },
    [dispatch, handleValidateChange, snapshot]
  );

  const hideApprovalChainIcon = useMemo(() => {
    if (
      loadingApprovalBy ||
      loading ||
      (isEmpty(approvalByGroup) && isEmpty(snapshot?.approvalRecords))
    )
      return true;

    if (
      !isEmpty(approvalByGroup) &&
      snapshot?.changeStatus === ChangeStatus.PendingApproval
    )
      return false;

    return isEmpty(snapshot?.approvalRecords);
  }, [snapshot, approvalByGroup, loadingApprovalBy, loading]);

  const handleOnDeleteChange = useCallback(async () => {
    const result = await (dispatch(
      actionsChange.deleteChange({ changeId })
    ) as any);

    const isSuccess = actionsChange.deleteChange.fulfilled.match(result);
    if (isSuccess) {
      //Direct to change managemen url
      history.push(CHANGE_MANAGEMENT_URL);
    }

    //Show toast deleted
    dispatch(
      actionsToast.addToast({
        type: isSuccess ? 'success' : 'error',
        message: isSuccess
          ? t('txt_snapshot_change_deleted_success')
          : t('txt_snapshot_change_failed_to_delete')
      })
    );
  }, [t, dispatch, changeId, history]);

  return error ? (
    <FailedApi
      id="home-page-portfolio-card-error"
      onReload={handleApiReload}
      className="mh-320 d-flex flex-column justify-content-center align-items-center w-100 bg-light-l20"
    />
  ) : (
    <>
      <div className="py-24 bg-light-l20">
        <div className="px-24">
          <div className="section">
            <BreadCrumbs changeName={snapshot?.changeName || ''} />
          </div>
          {(showRescheduleChangeMessage ||
            showEditChangeMessageOnRejected ||
            (showValidationFeedbackMessage && messages.length > 0)) && (
            <div className="ml-n8 mt-16">
              {showRescheduleChangeMessage && (
                <InlineMessage className="mt-8 ml-8 mb-0" variant="warning">
                  <span className="">
                    {t('txt_snapshot_reschedule_change_description')}
                  </span>
                  {canRescheduleChange && (
                    <span
                      className="link text-decoration-none pl-4"
                      onClick={() => setShowRescheduleChange(true)}
                    >
                      {t('txt_reschedule_change')}
                    </span>
                  )}
                </InlineMessage>
              )}
              {showEditChangeMessageOnRejected && (
                <InlineMessage className="mt-8 ml-8 mb-0" variant="danger">
                  <p>
                    <span className="">
                      {t('txt_snapshot_change_rejected')}
                    </span>
                    <span
                      className="link text-decoration-none px-4"
                      onClick={() => setShowRejectionNote(true)}
                    >
                      {t('txt_snapshot_view_reject_reason')}
                    </span>
                    <span>
                      {t('txt_snapshot_view_reject_reason_description')}
                    </span>
                    {canEditChange && (
                      <React.Fragment>
                        <span className="ml-4">
                          {t('txt_snapshot_change_rejected_description_1')}
                        </span>
                        <span
                          className="link text-decoration-none px-4"
                          onClick={() => setShowEditChange(true)}
                        >
                          {t('txt_snapshot_edit_change')}
                        </span>
                        <span className="">
                          {t('txt_snapshot_change_rejected_description_2')}
                        </span>
                      </React.Fragment>
                    )}
                  </p>
                </InlineMessage>
              )}
              {showValidationFeedbackMessage && messages.length > 0 && (
                <InlineMessage className="mt-8 ml-8 mb-0" variant="warning">
                  <span>{validationFeedback}</span>
                  <span
                    className="link text-decoration-none px-4"
                    onClick={() => setShowValidationFeedback(true)}
                  >
                    {t('txt_view_feedback')}
                  </span>
                  <span>{t('txt_for_more_information')}</span>
                </InlineMessage>
              )}
            </div>
          )}
          <div className="d-flex justify-content-between mt-24">
            <h3>{snapshot?.changeName}</h3>
            <div className="mr-n8">
              <ActionButtonList
                actions={snapshot?.actions || []}
                onButtonClicked={handleActionButtonClicked}
              />
            </div>
          </div>
          <div className="mt-8">
            <View
              id={`change-detail-snapshot__${snapshot?.changeId}`}
              formKey={`change-detail-snapshot__${snapshot?.changeId}`}
              descriptor="change-detail-snapshot"
              value={{
                ...snapshot,
                changeStatus: hideApprovalChainIcon
                  ? snapshot.changeStatus
                  : {
                      text: snapshot.changeStatus,
                      callbackAction: handleShowApprovalChain,
                      tooltip: t('txt_snapshot_view_approval_chain')
                    }
              }}
            />
          </div>
        </div>
      </div>
      {showActivity && (
        <ActivityFlyout
          id={`activity-in-change-modal-${snapshot.changeId}`}
          changeId={snapshot.changeId}
          title="Activities"
          show={showActivity}
          onCloseModal={handeActivityClosed}
        />
      )}
      {showEditChange && (
        <EditChangeModal
          id={`changeDetail_${snapshot?.changeId}__editChangeModal`}
          changeDetail={snapshot}
          show
          onClose={reload => {
            setShowEditChange(false);

            if (reload) {
              dispatch(actionsChange.getChangeDetail(snapshot.changeId));
            }
          }}
        />
      )}
      {showRescheduleChange && (
        <RescheduleChangeModal
          id={`changeDetail_${snapshot?.changeId}__rescheduleChangeModal`}
          show
          changeDetail={snapshot}
          onClose={reload => {
            setShowRescheduleChange(false);

            if (reload) {
              dispatch(actionsChange.getChangeDetail(snapshot.changeId));
            }
          }}
        />
      )}
      {showRejectionNote && (
        <RejectionNoteModal
          id={`changeDetail_${snapshot?.changeId}__rejectionNoteModal`}
          show
          rejectNote={{
            rejectedBy: snapshot?.lastChangedBy?.name || '',
            rejectedReason: snapshot?.rejectedReason || ''
          }}
          onClose={() => setShowRejectionNote(false)}
        />
      )}
      {showApproveChange && (
        <ApproveChangeModal
          id={`changeDetail_${snapshot?.changeId}__approveChangeModal`}
          changeId={snapshot.changeId}
          changeName={snapshot.changeName}
          show
          onClose={reload => {
            setShowApproveChange(false);

            if (reload) {
              dispatch(actionsChange.getChangeDetail(snapshot.changeId));
              dispatch(actionsChange.getPendingApprovalGroup(changeId));
            }
          }}
        />
      )}
      {showRejectChange && (
        <RejectChangeModal
          id={`changeDetail_${snapshot?.changeId}__rejectChangeModal`}
          changeId={snapshot.changeId}
          show
          onClose={reload => {
            setShowRejectChange(false);

            if (reload) {
              dispatch(actionsChange.getChangeDetail(snapshot.changeId));
            }
          }}
        />
      )}
      {showDemoteChange && (
        <DemoteChangeModal
          id={`changeDetail_${snapshot?.changeId}__rejectChangeModal`}
          changeId={snapshot.changeId}
          show
          onClose={reload => {
            setShowDemoteChange(false);
            if (reload) {
              dispatch(actionsChange.getChangeDetail(snapshot.changeId));
            }
          }}
        />
      )}
      {showValidationFeedbackMessage &&
        messages.length > 0 &&
        showValidationFeedback && (
          <ValidationFeedbackModal
            id={`changeDetail_${snapshot?.changeId}__changeValidationModal`}
            changeId={snapshot.changeId}
            messages={messages}
            show
            onClose={() => {
              setShowValidationFeedback(false);
            }}
            onEditChange={() => setShowEditChange(true)}
            onEditWorkflow={workflow => {
              if (canSetupWorkflow(workflow?.name)) {
                dispatch(
                  actionsWorkflowSetup.setWorkflowSetup({
                    isTemplate: false,
                    workflow,
                    onSubmitted: () => {
                      handleReloadAfterEditWorkflow();
                      return true;
                    }
                  })
                );
              }
            }}
          />
        )}
      {!!workflowDetailId && (
        <WorkflowDetailsModal
          id={`${workflowDetailId}_workflowDetailModal`}
          show={!!workflowDetailId}
          workflowId={workflowDetailId}
          editable={canEditChange}
          onClose={reload => {
            dispatch(actionsChange.closeWorkflowDetail());

            if (reload) {
              dispatch(actionsWorkflow.getChangeWorkflows({ changeId }));
            }
          }}
        />
      )}
      {showSubmitChange && (
        <SubmitChangeModal
          id={`changeDetail_${snapshot?.changeId}__submitChangeModal`}
          changeId={snapshot.changeId}
          changeName={snapshot.changeName}
          show
          onClose={reload => {
            setShowSubmitChange(false);

            if (reload) {
              dispatch(actionsChange.getChangeDetail(snapshot.changeId));
            }
          }}
        />
      )}
      {showApprovalChain && (
        <ApprovalChainFlyout
          id={`approval-chain-modal-${snapshot.changeId}`}
          title={t('txt_approval_chain')}
          show={showApprovalChain}
          snapshot={snapshot}
          approvalByGroup={approvalByGroup}
          onCloseModal={() => setApprovalChain(false)}
        />
      )}

      <DeleteChangeModal
        id={`delete-change-modal-${snapshot.changeId}`}
        name={snapshot.changeName}
        show={showDeleteChange}
        onClose={() => setShowDeleteChange(false)}
        onRemove={handleOnDeleteChange}
      />
    </>
  );
};

export default Snapshot;
