import { fireEvent } from '@testing-library/react';
import { SnapshotAction } from 'app/constants/enums';
import { renderComponent } from 'app/utils';
import React from 'react';
import ActionButtonList from './ActionButtonList';

jest.mock('app/_libraries/_dls', () => {
  const actualModules = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModules,
    useTranslation: () => ({
      t: (key: string) => key
    })
  };
});

const testId = 'action-button-list';

describe('Page > Change Detail > Snapshot > Action button', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render button list UI with 3 actions', async () => {
    const actions = ['edit', 'submit', 'validate'];

    const { getByText } = await renderComponent(
      testId,
      <ActionButtonList actions={actions} />
    );

    fireEvent.focus(getByText('txt_actions'));
    expect(getByText('txt_snapshot_edit')).toBeInTheDocument();
    expect(getByText('txt_snapshot_validate')).toBeInTheDocument();
    expect(getByText('txt_snapshot_submit_for_approval')).toBeInTheDocument();
    expect(getByText('txt_snapshot_view_activities')).toBeInTheDocument();
    expect(getByText('txt_snapshot_export')).toBeInTheDocument();
  });
});

it('should render button list UI without action', async () => {
  const actions: any = undefined;

  const { getByText } = await renderComponent(
    testId,
    <ActionButtonList actions={actions} />
  );

  expect(getByText('txt_snapshot_view_activities')).toBeInTheDocument();
  expect(getByText('txt_snapshot_export')).toBeInTheDocument();
});

it('handle button clicked on UI with 3 actions', async () => {
  const clickedBtnFn = jest.fn();
  const actions = ['edit', 'submit', 'validate'];

  const { getByText } = await renderComponent(
    testId,
    <ActionButtonList actions={actions} onButtonClicked={clickedBtnFn} />
  );

  expect(getByText('txt_snapshot_validate')).toBeInTheDocument();

  fireEvent.click(getByText('txt_snapshot_validate'));
  expect(clickedBtnFn).toHaveBeenCalledWith(SnapshotAction.VALIDATE);

  fireEvent.focus(getByText('txt_actions'));

  fireEvent.click(getByText('txt_snapshot_edit'));
  expect(clickedBtnFn).toHaveBeenCalledWith(SnapshotAction.EDIT);
});

it('handle button clicked on UI without actions', async () => {
  const clickedBtnFn = jest.fn();
  const actions: any[] = [];

  const { getByText } = await renderComponent(
    testId,
    <ActionButtonList actions={actions} onButtonClicked={clickedBtnFn} />
  );

  expect(getByText('txt_snapshot_view_activities')).toBeInTheDocument();

  fireEvent.click(getByText('txt_snapshot_view_activities'));
  expect(clickedBtnFn).toHaveBeenCalledWith(SnapshotAction.VIEW_ACTIVITIES);
});
