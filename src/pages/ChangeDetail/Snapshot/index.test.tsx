import { queryByText } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { ChangeStatus, SnapshotAction } from 'app/constants/enums';
import * as CanSetupWorkflowHelper from 'app/helpers/canSetupWorkflow';
import { mockThunkAction, renderWithMockStore, wait } from 'app/utils';
import 'app/utils/_mockComponent/mockFailedApiReload';
import 'app/utils/_mockComponent/mockUseTranslation';
import { actionsChange } from 'pages/_commons/redux/Change';
import { actionsWorkflow } from 'pages/_commons/redux/Workflow';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import React from 'react';
import Snapshot from './index';
const mockActionsChange = mockThunkAction(actionsChange);
const mockActionsWorkflow = mockThunkAction(actionsWorkflow);
const mockActionsWorkflowSetup = mockThunkAction(actionsWorkflowSetup);

jest.mock('../BreadCrumbs', () => ({
  __esModule: true,
  default: () => <div>BreadCrumbs_component</div>
}));

const mockOnClose = jest.fn();
const mockOnCloseWithReload = (reload = true) => {
  mockOnClose.mockImplementation(onClose => () => onClose && onClose(reload));
};

const mockEditWorkflowFunc = jest.fn();
const mockEditWorkflow = (
  workflow = { name: 'Manage Penalty Fees Workflow' }
) => {
  mockEditWorkflowFunc.mockImplementation(
    onEditWorkflow => () => onEditWorkflow && onEditWorkflow(workflow)
  );
};

jest.mock('../Modals', () => {
  const actualModule = jest.requireActual('../Modals');
  return {
    ...actualModule,
    ApproveChangeModal: ({ onClose }: any) => (
      <div onClick={mockOnClose(onClose)}>ApproveChangeModal</div>
    ),
    DemoteChangeModal: ({ onClose }: any) => (
      <div onClick={mockOnClose(onClose)}>DemoteChangeModal</div>
    ),
    EditChangeModal: ({ onClose }: any) => (
      <div onClick={mockOnClose(onClose)}>EditChangeModal</div>
    ),
    RejectChangeModal: ({ onClose }: any) => (
      <div onClick={mockOnClose(onClose)}>RejectChangeModal</div>
    ),
    RejectionNoteModal: ({ onClose }: any) => (
      <div onClick={onClose}>RejectionNoteModal</div>
    ),
    RescheduleChangeModal: ({ onClose }: any) => (
      <div onClick={mockOnClose(onClose)}>RescheduleChangeModal</div>
    ),
    SubmitChangeModal: ({ onClose }: any) => (
      <div onClick={mockOnClose(onClose)}>SubmitChangeModal</div>
    ),
    WorkflowDetailsModal: ({ onClose }: any) => (
      <div onClick={mockOnClose(onClose)}>WorkflowDetailsModal</div>
    ),
    ValidationFeedbackModal: ({
      onClose,
      onEditChange,
      onEditWorkflow
    }: any) => {
      return (
        <div>
          <div onClick={onClose}>ValidationFeedbackModal</div>
          <button onClick={onEditChange}>
            ValidationFeedbackModal_EditChange
          </button>
          <button onClick={mockEditWorkflowFunc(onEditWorkflow)}>
            ValidationFeedbackModal_EditWorkflow
          </button>
        </div>
      );
    }
  };
});

jest.mock('react-router', () => ({
  __esModule: true,
  useHistory: () => ({
    push: jest.fn(),
    location: { search: 'notifyFrom=import-export-workflow' }
  })
}));

jest.mock('app/_libraries/_dof/core/View', () => ({
  __esModule: true,
  default: ({
    formKey,
    descriptor,
    onClick,
    value: {
      changeStatus: { callbackAction }
    }
  }: any) => {
    return (
      <div id={formKey}>
        {formKey}
        <span>{descriptor}</span>
        <button onClick={onClick}>Click on {formKey}</button>
        <button onClick={callbackAction}>callbackAction_click</button>
      </div>
    );
  }
}));

const renderComponent = async (props: any, initialState: any) => {
  return await renderWithMockStore(<Snapshot {...props} />, {
    initialState
  });
};

const mockCanSetupWorkflow = jest.spyOn(
  CanSetupWorkflowHelper,
  'canSetupWorkflow'
);

const { location } = window;

describe('Test ChangeDetail > Snapshot', () => {
  beforeEach(() => {
    mockOnCloseWithReload();
    mockEditWorkflow();
    mockCanSetupWorkflow.mockReturnValue(true);
  });

  afterEach(() => {
    jest.clearAllMocks();
    window.location = location;
  });

  const initialState = {
    workflow: {
      changeWorkflows: {
        workflows: [
          {
            validationMessages: [
              {
                changeId: '123',
                workflowId: '123',
                severity: 'caution'
              }
            ]
          }
        ]
      }
    },
    change: {
      changeDetails: {
        data: {
          validationMessages: [
            {
              changeId: '456',
              workflowId: '456',
              severity: 'caution'
            }
          ]
        }
      },
      workflowDetailId: '123',
      pendingApprovalByGroup: {
        loading: false,
        approvalByGroup: {}
      }
    }
  };

  it('should render with failed api', async () => {
    const props = {
      changeId: '123',
      snapshot: {
        changeId: '123'
      },
      error: true
    };

    const getChangeDetailFn = mockActionsChange('getChangeDetail');
    const wrapper = await renderComponent(props, initialState);
    expect(wrapper.getByText('Data load unsuccessful')).toBeInTheDocument();
    userEvent.click(wrapper.getByText('Reload'));

    expect(getChangeDetailFn).toHaveBeenCalledWith('123');
  });

  it('should render UI only when no actions', async () => {
    const props = {
      changeId: '123',
      snapshot: {
        changeId: '123',
        changeName: 'name',
        changeStatus: ChangeStatus.Lapsed
      },
      error: false
    };

    const wrapper = await renderComponent(props, initialState);
    expect(wrapper.getByText('change-detail-snapshot')).toBeInTheDocument();

    expect(
      wrapper.queryByText('txt_reschedule_change')
    ).not.toBeInTheDocument();
    expect(
      wrapper.queryByText('txt_snapshot_validate')
    ).not.toBeInTheDocument();
    expect(
      wrapper.queryByText('txt_snapshot_submit_for_approval')
    ).not.toBeInTheDocument();
    expect(wrapper.queryByText('txt_snapshot_edit')).not.toBeInTheDocument();
    expect(wrapper.queryByText('txt_snapshot_demote')).not.toBeInTheDocument();
    expect(wrapper.queryByText('txt_snapshot_approve')).not.toBeInTheDocument();
    expect(wrapper.queryByText('txt_snapshot_reject')).not.toBeInTheDocument();
  });

  it('should render with data with change status is lapsed', async () => {
    mockCanSetupWorkflow.mockReturnValue(false);
    const props = {
      changeId: '123',
      snapshot: {
        changeId: '123',
        changeName: 'name',
        changeStatus: ChangeStatus.Lapsed,
        actions: [
          SnapshotAction.APPROVE,
          SnapshotAction.DEMOTE,
          SnapshotAction.EDIT,
          SnapshotAction.EXPORT,
          SnapshotAction.REJECT,
          SnapshotAction.SUBMIT_FOR_APPROVAL,
          SnapshotAction.VALIDATE,
          SnapshotAction.VIEW_ACTIVITIES,
          'reschedule'
        ]
      },
      error: false
    };

    mockEditWorkflow({ name: 'not found' });
    const setWorkflowSetup = mockActionsWorkflowSetup('setWorkflowSetup');
    const getChangeDetailFn = mockActionsChange('getChangeDetail');
    const wrapper = await renderComponent(props, initialState);
    expect(wrapper.getByText('change-detail-snapshot')).toBeInTheDocument();
    userEvent.click(wrapper.getByText('txt_reschedule_change'));
    expect(wrapper.getByText('RescheduleChangeModal')).toBeInTheDocument();
    userEvent.click(wrapper.getByText('RescheduleChangeModal'));
    expect(getChangeDetailFn).toHaveBeenCalledWith('123');

    userEvent.click(wrapper.getByText('txt_view_feedback'));
    userEvent.click(wrapper.getByText('ValidationFeedbackModal_EditWorkflow'));
    expect(setWorkflowSetup).not.toBeCalled();
  });

  it('should render with data with change status is rejected', async () => {
    const props = {
      changeId: '123',
      snapshot: {
        changeId: '123',
        changeName: 'CHANGE_NAME',
        changeStatus: ChangeStatus.Rejected,
        actions: [
          SnapshotAction.APPROVE,
          SnapshotAction.DEMOTE,
          SnapshotAction.EDIT,
          SnapshotAction.EXPORT,
          SnapshotAction.REJECT,
          SnapshotAction.SUBMIT_FOR_APPROVAL,
          SnapshotAction.VALIDATE,
          SnapshotAction.VIEW_ACTIVITIES,
          'reschedule'
        ]
      },
      error: false
    };

    const setWorkflowSetup = mockActionsWorkflowSetup('setWorkflowSetup', {
      callback: ({ onSubmitted }) => {
        onSubmitted && onSubmitted();
      }
    });
    let getChangeDetailFn = mockActionsChange('getChangeDetail');
    const wrapper = await renderComponent(props, initialState);
    userEvent.click(wrapper.getByText('txt_snapshot_view_reject_reason'));
    expect(wrapper.getByText('RejectionNoteModal')).toBeInTheDocument();
    userEvent.click(wrapper.getByText('RejectionNoteModal'));
    expect(
      queryByText(wrapper.container, /RejectionNoteModal/i)
    ).not.toBeInTheDocument();

    userEvent.click(wrapper.getByText('txt_snapshot_edit_change'));
    expect(wrapper.getByText('EditChangeModal')).toBeInTheDocument();
    userEvent.click(wrapper.getByText('EditChangeModal'));
    expect(getChangeDetailFn).toBeCalledWith('123');

    userEvent.click(wrapper.getByText('txt_view_feedback'));
    expect(wrapper.getByText('ValidationFeedbackModal')).toBeInTheDocument();
    userEvent.click(wrapper.getByText('ValidationFeedbackModal'));
    expect(
      queryByText(wrapper.container, /ValidationFeedbackModal/i)
    ).not.toBeInTheDocument();
    userEvent.click(wrapper.getByText('txt_view_feedback'));
    userEvent.click(wrapper.getByText('ValidationFeedbackModal_EditChange'));
    expect(wrapper.getByText('EditChangeModal')).toBeInTheDocument();
    getChangeDetailFn = mockActionsChange('getChangeDetail');
    userEvent.click(wrapper.getByText('EditChangeModal'));
    expect(getChangeDetailFn).toBeCalledWith('123');

    getChangeDetailFn = mockActionsChange('getChangeDetail');
    userEvent.click(wrapper.getByText('ValidationFeedbackModal_EditWorkflow'));
    expect(setWorkflowSetup).toBeCalled();
    expect(getChangeDetailFn).toBeCalledWith('123');
  });

  it('should show activity flyout', async () => {
    const props = {
      changeId: '123',

      snapshot: {
        changeId: '123',
        changeName: 'CHANGE_NAME',
        changeStatus: ChangeStatus.PendingApproval,
        actions: [SnapshotAction.VIEW_ACTIVITIES],
        loading: false,
        approvalRecords: [
          {
            actionTime: new Date('2021-09-15T14:57:07.1483351+07:00'),
            group: {
              id: 160626202,
              name: 'Darth Vader',
              changeType: 'PRODUCTSETUP',
              defaultgroup: false
            },
            user: {
              id: 493110309,
              name: 'Jesse Dunn'
            },
            action: 'APPROVE',
            reason: 'tincidunt et iriure'
          }
        ] as IApprovalRecord[]
      },
      error: false
    };

    const getPendingApprovalGroup = mockActionsChange(
      'getPendingApprovalGroup'
    );
    const wrapper = await renderComponent(props, {
      ...initialState,
      change: {
        changeDetails: {
          data: {
            validationMessages: [
              {
                changeId: '456',
                workflowId: '456',
                severity: 'caution'
              }
            ]
          }
        },
        workflowDetailId: '123',
        pendingApprovalByGroup: {
          loading: false,
          approvalByGroup: { user: {} }
        }
      }
    });

    expect(getPendingApprovalGroup).toHaveBeenCalledWith('123');

    userEvent.click(wrapper.getByText('callbackAction_click'));
    expect(wrapper.getAllByText('txt_approval_chain')[0]).toBeInTheDocument();
    userEvent.click(wrapper.getByText('Close'));
    expect(wrapper.queryByText('Activity List')).not.toBeInTheDocument();

    userEvent.click(wrapper.getByText('txt_snapshot_view_activities'));
    expect(wrapper.getByText('Activity List')).toBeInTheDocument();
    userEvent.click(wrapper.getByText('Close'));
    expect(wrapper.queryByText('Activity List')).not.toBeInTheDocument();
  });

  it('should not show activity flyout', async () => {
    const props = {
      changeId: '123',

      snapshot: {
        changeId: '123',
        changeName: 'CHANGE_NAME',
        changeStatus: ChangeStatus.Lapsed,
        actions: [SnapshotAction.VIEW_ACTIVITIES],
        loading: false,
        approvalRecords: [] as IApprovalRecord[]
      },
      error: false
    };

    await renderComponent(props, {
      ...initialState,
      change: {
        changeDetails: {
          data: {
            validationMessages: [
              {
                changeId: '456',
                workflowId: '456',
                severity: 'caution'
              }
            ]
          }
        },
        workflowDetailId: '123',
        pendingApprovalByGroup: {
          loading: false,
          approvalByGroup: { user: {} }
        }
      }
    });
  });

  it('should show edit modal', async () => {
    const props = {
      changeId: '123',
      snapshot: {
        changeId: '123',
        changeName: 'CHANGE_NAME',
        changeStatus: ChangeStatus.Work,
        actions: [SnapshotAction.EDIT]
      },
      error: false
    };

    const wrapper = await renderComponent(props, initialState);

    mockOnCloseWithReload(false);
    userEvent.click(wrapper.getByText('txt_snapshot_edit'));
    const modal = wrapper.getByText('EditChangeModal');
    expect(modal).toBeInTheDocument();
    userEvent.click(modal);
    expect(wrapper.queryByText('EditChangeModal')).not.toBeInTheDocument();
  });

  it('should show delete modal > delete', async () => {
    const props = {
      changeId: '123',
      snapshot: {
        changeId: '123',
        changeName: 'CHANGE_NAME',
        changeStatus: ChangeStatus.Work,
        actions: [SnapshotAction.DELETE]
      },
      error: false
    };

    const deleteChange = mockActionsChange('deleteChange');
    const wrapper = await renderComponent(props, initialState);

    mockOnCloseWithReload(false);
    userEvent.click(wrapper.getAllByText('txt_snapshot_delete')[0]);
    const modal = wrapper.getByText('txt_confirm_deletion');
    expect(modal).toBeInTheDocument();
    userEvent.click(modal);
    const buttonDelete = wrapper.getByText('txt_delete');
    expect(buttonDelete).toBeInTheDocument();
    userEvent.click(buttonDelete);

    await wait();

    expect(deleteChange).toBeCalledWith({ changeId: '123' });
  });

  it('should show delete modal > cancel', async () => {
    const props = {
      changeId: '123',
      snapshot: {
        changeId: '123',
        changeName: 'CHANGE_NAME',
        changeStatus: ChangeStatus.Work,
        actions: [SnapshotAction.DELETE]
      },
      error: false
    };

    const wrapper = await renderComponent(props, initialState);

    mockOnCloseWithReload(false);
    userEvent.click(wrapper.getAllByText('txt_snapshot_delete')[0]);
    const modal = wrapper.getByText('txt_confirm_deletion');
    expect(modal).toBeInTheDocument();
    userEvent.click(modal);
    const buttonDelete = wrapper.getByText('txt_cancel');
    expect(buttonDelete).toBeInTheDocument();
    userEvent.click(buttonDelete);
  });

  it('should show reschedule modal', async () => {
    const props = {
      changeId: '123',
      snapshot: {
        changeId: '123',
        changeName: 'CHANGE_NAME',
        changeStatus: ChangeStatus.Lapsed,
        actions: ['reschedule']
      },
      error: false
    };

    const wrapper = await renderComponent(props, initialState);

    mockOnCloseWithReload(false);
    userEvent.click(wrapper.getByText('txt_reschedule_change'));
    const modal = wrapper.getByText('RescheduleChangeModal');
    expect(modal).toBeInTheDocument();
    userEvent.click(modal);
    expect(
      wrapper.queryByText('RescheduleChangeModal')
    ).not.toBeInTheDocument();
  });

  it('should show approval change modal', async () => {
    const props = {
      changeId: '123',
      snapshot: {
        changeId: '123',
        changeName: 'CHANGE_NAME',
        changeStatus: ChangeStatus.Work,
        actions: [SnapshotAction.APPROVE]
      },
      error: false
    };

    const getChangeDetail = mockActionsChange('getChangeDetail');
    const getPendingApprovalGroup = mockActionsChange(
      'getPendingApprovalGroup'
    );
    const wrapper = await renderComponent(props, initialState);

    const actionBtn = wrapper.getByText('txt_snapshot_approve');
    userEvent.click(actionBtn);
    let modal = wrapper.getByText('ApproveChangeModal');
    expect(modal).toBeInTheDocument();
    userEvent.click(modal);
    expect(wrapper.queryByText('ApproveChangeModal')).not.toBeInTheDocument();
    expect(getChangeDetail).toBeCalledWith('123');
    expect(getPendingApprovalGroup).toBeCalledWith('123');

    mockOnCloseWithReload(false);
    userEvent.click(actionBtn);
    modal = wrapper.getByText('ApproveChangeModal');
    expect(modal).toBeInTheDocument();
    userEvent.click(modal);
    expect(wrapper.queryByText('ApproveChangeModal')).not.toBeInTheDocument();
  });

  it('should show rejected change modal', async () => {
    const props = {
      changeId: '123',
      snapshot: {
        changeId: '123',
        changeName: 'CHANGE_NAME',
        changeStatus: ChangeStatus.Work,
        actions: [SnapshotAction.REJECT]
      },
      error: false
    };

    const getChangeDetail = mockActionsChange('getChangeDetail');
    const wrapper = await renderComponent(props, initialState);

    const actionBtn = wrapper.getByText('txt_snapshot_reject');
    userEvent.click(actionBtn);
    let modal = wrapper.getByText('RejectChangeModal');
    expect(modal).toBeInTheDocument();
    userEvent.click(modal);
    expect(wrapper.queryByText('RejectChangeModal')).not.toBeInTheDocument();
    expect(getChangeDetail).toBeCalledWith('123');

    mockOnCloseWithReload(false);
    userEvent.click(actionBtn);
    modal = wrapper.getByText('RejectChangeModal');
    expect(modal).toBeInTheDocument();
    userEvent.click(modal);
    expect(wrapper.queryByText('RejectChangeModal')).not.toBeInTheDocument();
  });

  it('should show demote change modal', async () => {
    const props = {
      changeId: '123',
      snapshot: {
        changeId: '123',
        changeName: 'CHANGE_NAME',
        changeStatus: ChangeStatus.Work,
        actions: [SnapshotAction.DEMOTE]
      },
      error: false
    };

    const getChangeDetail = mockActionsChange('getChangeDetail');
    const wrapper = await renderComponent(props, initialState);

    const actionBtn = wrapper.getByText('txt_snapshot_demote');
    userEvent.click(actionBtn);
    let modal = wrapper.getByText('DemoteChangeModal');
    expect(modal).toBeInTheDocument();
    userEvent.click(modal);
    expect(wrapper.queryByText('DemoteChangeModal')).not.toBeInTheDocument();
    expect(getChangeDetail).toBeCalledWith('123');

    mockOnCloseWithReload(false);
    userEvent.click(actionBtn);
    modal = wrapper.getByText('DemoteChangeModal');
    expect(modal).toBeInTheDocument();
    userEvent.click(modal);
    expect(wrapper.queryByText('DemoteChangeModal')).not.toBeInTheDocument();
  });

  it('should show submit for aproval change modal', async () => {
    const props = {
      changeId: '123',
      snapshot: {
        changeId: '123',
        changeName: 'CHANGE_NAME',
        changeStatus: ChangeStatus.Work,
        actions: ['submit']
      },
      error: false
    };

    const getChangeDetail = mockActionsChange('getChangeDetail');
    const wrapper = await renderComponent(props, initialState);

    const actionBtn = wrapper.getByText('txt_snapshot_submit_for_approval');
    userEvent.click(actionBtn);
    let modal = wrapper.getByText('SubmitChangeModal');
    expect(modal).toBeInTheDocument();
    userEvent.click(modal);
    expect(wrapper.queryByText('SubmitChangeModal')).not.toBeInTheDocument();
    expect(getChangeDetail).toBeCalledWith('123');

    mockOnCloseWithReload(false);
    userEvent.click(actionBtn);
    modal = wrapper.getByText('SubmitChangeModal');
    expect(modal).toBeInTheDocument();
    userEvent.click(modal);
    expect(wrapper.queryByText('SubmitChangeModal')).not.toBeInTheDocument();
  });

  it('should run export', async () => {
    const props = {
      changeId: '123',
      snapshot: {
        changeId: '123',
        changeName: 'CHANGE_NAME',
        changeStatus: ChangeStatus.Work,
        actions: [SnapshotAction.EXPORT]
      },
      error: false
    };

    const wrapper = await renderComponent(props, initialState);

    const actionBtn = wrapper.getByText('txt_snapshot_export');
    userEvent.click(actionBtn);
    expect(wrapper.getByText('WorkflowDetailsModal')).toBeTruthy();
  });

  it('should run validate success', async () => {
    const props = {
      changeId: '123',
      snapshot: {
        changeId: '123',
        changeName: 'CHANGE_NAME',
        changeStatus: ChangeStatus.Work,
        actions: [SnapshotAction.VALIDATE]
      },
      error: false
    };

    const validateChange = mockActionsChange('validateChange');
    const getChangeDetail = mockActionsChange('getChangeDetail');
    const getChangeWorkflows = mockActionsWorkflow('getChangeWorkflows');
    const wrapper = await renderComponent(props, initialState);

    const actionBtn = wrapper.getByText('txt_snapshot_validate');
    userEvent.click(actionBtn);
    await wait();

    expect(validateChange).toBeCalledWith({ changeId: '123' });
    expect(getChangeDetail).toBeCalledWith('123');
    expect(getChangeWorkflows).toBeCalledWith({ changeId: '123' });
  });

  it('should run validate fail', async () => {
    const props = {
      changeId: '123',
      snapshot: {
        changeId: '123',
        changeName: 'CHANGE_NAME',
        changeStatus: ChangeStatus.Work,
        actions: [SnapshotAction.VALIDATE]
      },
      error: false
    };

    const validateChange = mockActionsChange('validateChange', {
      match: false
    });
    const getChangeDetail = mockActionsChange('getChangeDetail');
    const getChangeWorkflows = mockActionsWorkflow('getChangeWorkflows');
    const wrapper = await renderComponent(props, initialState);

    const actionBtn = wrapper.getByText('txt_snapshot_validate');
    userEvent.click(actionBtn);
    await wait();

    expect(validateChange).toBeCalledWith({ changeId: '123' });
    expect(getChangeDetail).not.toBeCalled();
    expect(getChangeWorkflows).not.toBeCalled();
  });

  it('should reload workflow after close workflow detail modal', async () => {
    const props = {
      changeId: '123',
      snapshot: {
        changeId: '123',
        changeName: 'CHANGE_NAME',
        changeStatus: ChangeStatus.Work
      },
      error: false
    };

    const getChangeWorkflows = mockActionsWorkflow('getChangeWorkflows');
    const wrapper = await renderComponent(props, initialState);

    const modal = wrapper.getByText('WorkflowDetailsModal');
    expect(modal).toBeInTheDocument();
    userEvent.click(modal);
    expect(wrapper.queryByText('WorkflowDetailsModal')).not.toBeInTheDocument();
    expect(getChangeWorkflows).toBeCalledWith({ changeId: '123' });
  });

  it('should not reload workflow after close workflow detail modal', async () => {
    const props = {
      changeId: '123',
      snapshot: {
        changeId: '123',
        changeName: 'CHANGE_NAME',
        changeStatus: ChangeStatus.Work
      },
      error: false
    };

    mockOnCloseWithReload(false);
    const getChangeWorkflows = mockActionsWorkflow('getChangeWorkflows');
    const wrapper = await renderComponent(props, initialState);

    const modal = wrapper.getByText('WorkflowDetailsModal');
    expect(modal).toBeInTheDocument();
    userEvent.click(modal);
    expect(wrapper.queryByText('WorkflowDetailsModal')).not.toBeInTheDocument();
    expect(getChangeWorkflows).not.toBeCalled();
  });

  it('should not reload workflow after close workflow detail modal with window.location.href', async () => {
    const props = {
      changeId: '123',
      snapshot: {
        changeId: '123',
        changeName: 'CHANGE_NAME',
        changeStatus: ChangeStatus.Work
      },
      error: false
    };

    mockOnCloseWithReload(false);
    const getChangeWorkflows = mockActionsWorkflow('getChangeWorkflows');
    const wrapper = await renderComponent(props, initialState);

    const modal = wrapper.getByText('WorkflowDetailsModal');
    expect(modal).toBeInTheDocument();
    userEvent.click(modal);
    expect(wrapper.queryByText('WorkflowDetailsModal')).not.toBeInTheDocument();
    expect(getChangeWorkflows).not.toBeCalled();
  });
});
