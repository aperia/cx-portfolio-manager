import DMMTablesList from 'pages/_commons/DMMTables/List';
import React from 'react';

interface IProps {
  changeId?: string;
}
const DMMTables: React.FC<IProps> = ({ changeId = '' }) => {
  return <DMMTablesList changeId={changeId} />;
};

export default DMMTables;
