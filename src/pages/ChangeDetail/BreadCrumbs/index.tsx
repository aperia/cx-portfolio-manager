import BreadCrumb from 'app/components/BreadCrumb';
import { BASE_URL, CHANGE_MANAGEMENT_URL } from 'app/constants/links';
import React, { useMemo } from 'react';
import { Link } from 'react-router-dom';

interface IProps {
  changeName: string;
}
const BreadCrumbs: React.FC<IProps> = ({ changeName = '' }) => {
  const breadCrmbItems = useMemo(
    () => [
      { id: 'home', title: <Link to={BASE_URL}>Home</Link> },
      {
        id: 'changeManagement',
        title: <Link to={CHANGE_MANAGEMENT_URL}>Change Management</Link>
      },
      { id: 'changeDetail', title: `${changeName}` }
    ],
    [changeName]
  );

  return <BreadCrumb items={breadCrmbItems} width={992} />;
};

export default BreadCrumbs;
