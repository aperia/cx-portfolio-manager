import { useTranslation } from 'app/_libraries/_dls';
import PortfolioList from 'pages/_commons/PortfolioList';
import React from 'react';

interface IProps {
  changeId: string;
}

const Portfolios: React.FC<IProps> = ({ changeId }) => {
  const { t } = useTranslation();
  return (
    <PortfolioList
      containerClassName={''}
      title={t('txt_portfolios_impacted')}
      changeId={changeId}
      hideBreadCrumb
      hideViewMode
    />
  );
};

export default Portfolios;
