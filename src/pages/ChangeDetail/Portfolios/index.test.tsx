import { render } from '@testing-library/react';
import React from 'react';
import Portfolios from './index';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

jest.mock('pages/_commons/PortfolioList', () => ({
  __esModule: true,
  default: () => {
    return <div>PortfolioList_component</div>;
  }
}));

describe('Test ChangeDetail > Portfolios', () => {
  it('Should render Portfolios', () => {
    const wrapper = render(<Portfolios changeId={'1'} />);
    expect(wrapper.getByText('PortfolioList_component')).toBeInTheDocument();
  });
});
