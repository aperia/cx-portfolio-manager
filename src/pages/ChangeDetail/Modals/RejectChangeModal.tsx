import ModalRegistry from 'app/components/ModalRegistry';
import TextAreaCountDown from 'app/components/TextAreaCountDown';
import { ChangeStatus } from 'app/constants/enums';
import { useFormValidations } from 'app/hooks';
import {
  Button,
  ModalBody,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import isFunction from 'lodash.isfunction';
import orderBy from 'lodash.orderby';
import {
  actionsChange,
  useSelectLoadingOnRejectChange
} from 'pages/_commons/redux/Change';
import { actionsToast } from 'pages/_commons/ToastNotifications/redux';
import React, { useMemo, useState } from 'react';
import { useDispatch } from 'react-redux';
import { MULTIAPPROVAL_MESSAGE } from './ApproveChangeModal';

interface IProps {
  id: string;
  changeId: string;
  show: boolean;

  onClose?: (reload?: boolean) => void;
}
interface IRejectChange {
  changeId: string;
  reason: string;
}

export const RejectChangeModal: React.FC<IProps> = ({
  id,
  changeId,
  show,
  onClose
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const [rejectDescription, setRejectDescription] = useState('');

  const loading = useSelectLoadingOnRejectChange();

  const currentErrors = useMemo(
    () =>
      ({
        reason: !rejectDescription?.trim() && {
          status: true,
          message: t('txt_validation_reason_required')
        }
      } as Record<keyof IRejectChange, IFormError>),
    [rejectDescription, t]
  );

  const [, errors, setTouched] =
    useFormValidations<keyof IRejectChange>(currentErrors);

  const isValid = useMemo(
    () => !Object.keys(errors).some(k => errors[k as keyof IRejectChange]),
    [errors]
  );

  const verifyMultiApprovals = async () => {
    const result: any = await Promise.resolve(
      dispatch(actionsChange.getChangeDetail(changeId))
    );
    const isSuccess = actionsChange.getChangeDetail.fulfilled.match(result);
    if (isSuccess) {
      const { changeStatus, approvalRecords } = result.payload.data || {};
      const record = orderBy(approvalRecords, ['actionTime'], ['desc'])[0];
      if (changeStatus !== ChangeStatus.PendingApproval) {
        if (['reject', 'approve'].includes(record?.action?.toLowerCase())) {
          dispatch(
            actionsToast.addToast({
              type: 'error',
              message: t(
                MULTIAPPROVAL_MESSAGE[record?.action?.toLowerCase()]
              ).replace('{0}', record?.user?.name || '')
            })
          );
        }
        isFunction(onClose) && onClose(isSuccess);
        return false;
      }
      return true;
    }
    return true;
  };

  const onSave = async () => {
    const verifyApprove = await verifyMultiApprovals();
    if (!verifyApprove) return;

    const result: any = await Promise.resolve(
      dispatch(
        actionsChange.rejectChange({
          changeId,
          reason: rejectDescription.trim()
        })
      )
    );
    const isSuccess = actionsChange.rejectChange.fulfilled.match(result);

    if (isSuccess) {
      dispatch(
        actionsToast.addToast({
          type: 'success',
          message: t('txt_change_rejected')
        })
      );
      isFunction(onClose) && onClose(isSuccess);
    } else {
      dispatch(
        actionsToast.addToast({
          type: 'error',
          message: t('txt_change_failed_to_reject')
        })
      );
    }
  };

  const handleCloseWithoutReload = () => {
    isFunction(onClose) && onClose();
  };

  const handleOnReasonChange = (field: keyof IRejectChange) => (e: any) => {
    setRejectDescription(e.target.value);
  };

  return (
    <ModalRegistry
      id={id}
      show={show}
      onAutoClosedAll={handleCloseWithoutReload}
      classes={{ content: loading ? 'loading' : '' }}
    >
      <ModalHeader border closeButton onHide={handleCloseWithoutReload}>
        <ModalTitle>{t('txt_reject_change')}</ModalTitle>
      </ModalHeader>
      <ModalBody>
        <p>{t('txt_reject_change_desc')} </p>
        <div>
          <div className="mt-16 mb-12 text-uppercase color-grey fw-500 fs-11">
            {t('txt_reason')} <span className="color-red">*</span>
          </div>
          <TextAreaCountDown
            id={`${id}__reason`}
            className="resize-none"
            value={rejectDescription}
            onChange={handleOnReasonChange('reason')}
            required
            maxLength={255}
            onFocus={setTouched('reason')}
            onBlur={setTouched('reason', true)}
            error={errors.reason}
          />
        </div>
      </ModalBody>
      <div className="dls-modal-footer modal-footer">
        <Button variant="secondary" onClick={handleCloseWithoutReload}>
          {t('txt_cancel')}
        </Button>
        <Button
          variant="danger"
          onClick={onSave}
          disabled={!isValid || Object.keys(errors).length === 0}
        >
          {t('txt_reject')}
        </Button>
      </div>
    </ModalRegistry>
  );
};
