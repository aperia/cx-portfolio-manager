import { act, fireEvent, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import {
  mockActionCreator,
  mockUseDispatchFnc,
  renderWithMockStore,
  wait
} from 'app/utils';
// import 'app/utils/_mockComponent/mockComboBox';
import 'app/utils/_mockComponent/mockDatePicker';
import 'app/utils/_mockComponent/mockEnhanceMultiSelect';
import 'app/utils/_mockComponent/mockModalRegistry';
import 'app/utils/_mockComponent/mockTimePicker';
import 'app/utils/_mockComponent/mockUseTranslation';
import mockDevice from 'app/utils/_mockHelperConstant/mockDevice';
import {
  ComboBoxProps,
  DatePickerChangeEvent,
  DropdownBaseChangeEvent,
  DropdownBaseGroupProps,
  DropdownBaseItemProps
} from 'app/_libraries/_dls';
import { actionsWorkflow } from 'pages/_commons/redux/Workflow';
import { actionsToast } from 'pages/_commons/ToastNotifications/redux';
import React from 'react';
import { SetReminderModal } from './SetReminderModal';

jest.mock('app/_libraries/_dls/components/ComboBox', () => {
  const actualModule = jest.requireActual(
    'app/_libraries/_dls/components/ComboBox'
  );
  const MockComboBox = (props: ComboBoxProps) => {
    const { onChange, label, onBlur, error, children } = props;

    return (
      <div className="dls-input-container">
        <input
          data-testid="ComboBox.Input_onChange"
          onChange={(e: any) => {
            const value: RefData = e.target.value;
            onChange!({
              target: { value: value }
            } as DropdownBaseChangeEvent);
          }}
          onBlur={onBlur}
        />
        {!!error && <p data-testid="ComboBox.Error">{error?.message}</p>}
        <span>{label}</span>
        <div>{children}</div>
      </div>
    );
  };

  MockComboBox.Item = ({ value, label }: DropdownBaseItemProps) => (
    <div key={`${value}-${label}`} data-testid={`ComboBox.Item-${label}`}>
      {label}
    </div>
  );

  MockComboBox.Group = ({ children, label }: DropdownBaseGroupProps) => (
    <div key={`${label}`} data-testid={`ComboBox.Group-${label}`}>
      {children}
    </div>
  );
  return {
    ...actualModule,
    __esModule: true,
    default: MockComboBox
  };
});

const mockDateTooltipParams = jest.fn();
jest.mock('app/_libraries/_dls/components/DatePicker', () => {
  const actualModule = jest.requireActual(
    'app/_libraries/_dls/components/DatePicker'
  );
  return {
    ...actualModule,
    __esModule: true,
    default: ({ onChange, dateTooltip, label, onBlur, ...props }: any) => (
      <div>
        <span>{label}</span>
        <input
          {...props}
          onClick={() => {
            const params = mockDateTooltipParams();

            !!dateTooltip && dateTooltip(...params);
          }}
          onChange={(e: any) => {
            onChange!({
              target: { value: new Date(e.target.value), name: e.target.name }
            } as DatePickerChangeEvent);
          }}
          onBlur={onBlur}
        />
      </div>
    )
  };
});

HTMLCanvasElement.prototype.getContext = jest.fn();
const defaultAuth = {
  currentUser: {
    essUserName: 'Test Name',
    userId: 'FDESMAST',
    userName: 'testname',
    groups: ['HLNSA'],
    timeZone: -5
  }
};
const defaultChangeOwner = {
  changeOwners: {
    data: [{ code: '001', text: 'John cena' }],
    loading: false
  }
} as any;

const defaultWorkflowState = {
  templateWorkflows: {
    workflows: [
      {
        id: '1',
        name: 'Prime Interest Rate Change'
      },
      { id: '2', name: 'Manage Penalty Fees Workflow' }
    ],
    pageFetching: {
      loading: false,
      error: false
    }
  },
  reminderSet: { loading: false }
} as any;

const mockProps: any = {
  id: 'id01',
  show: true,
  onClose: jest.fn()
};

const mockUseDispatch = mockUseDispatchFnc();
const actionsWorkflowMock: any = actionsWorkflow;

const queryInputFromLabel = (tierLabel: HTMLElement) => {
  return tierLabel
    .closest(
      '.dls-input-container, .dls-numreric-container, .text-field-container'
    )
    ?.querySelector('input, .input') as HTMLInputElement;
};

const then_change_recurrence_dropdown = (
  wrapper: RenderResult,
  value: string
) => {
  const setRecurrenceOptions = queryInputFromLabel(
    wrapper.getByText('Set Recurrence')
  );
  userEvent.click(setRecurrenceOptions);
  userEvent.click(wrapper.getByText(value));

  fireEvent.blur(setRecurrenceOptions);
};
const then_update_textarea = (wrapper: RenderResult, value: string) => {
  const inputElement = document.body.querySelector(
    `.text-field-container > textarea`
  );

  userEvent.type(inputElement!, value);
  fireEvent.blur(inputElement!);
};
const then_change_time_picker = (wrapper: RenderResult, value: string) => {
  const timepicker = wrapper
    .getByText('Reminder Time')
    .parentElement?.querySelector('input');

  userEvent.clear(timepicker!);
  act(() => {
    userEvent.type(timepicker!, value);
  });

  fireEvent.blur(timepicker!);
};

const then_change_date_picker = (wrapper: RenderResult, value: Date) => {
  const datepicker = wrapper
    .getByText('Reminder Date')
    .parentElement?.querySelector('input');
  userEvent.click(datepicker!);
  userEvent.clear(datepicker!);
  act(() => {
    // userEvent.type(datepicker!, value);
    fireEvent.change(datepicker!, {
      target: {
        value: value
      }
    });
  });

  fireEvent.blur(datepicker!);
};

const then_change_workflow_combobox = (
  wrapper: RenderResult,
  value: RefData
) => {
  const workflowInput = queryInputFromLabel(wrapper.getByText('Workflow'));
  act(() => {
    fireEvent.change(workflowInput, {
      target: {
        value: value
      }
    });
  });

  fireEvent.blur(workflowInput!);
};

const then_change_recipients_multiSelect = (
  wrapper: RenderResult,
  value: RefData
) => {
  const recipientsInput = queryInputFromLabel(wrapper.getByText('Recipients'));
  fireEvent.change(recipientsInput!, { target: { value: value } });
  fireEvent.blur(recipientsInput!);
};

const then_click_save_button = (wrapper: RenderResult) => {
  userEvent.click(wrapper.getByText('txt_submit'));
};

const then_click_notify_me_button = (wrapper: RenderResult) => {
  const notifyButton = wrapper.getByText('Notify Me');
  userEvent.click(notifyButton!);
};

describe('SetReminderModal ', () => {
  beforeEach(() => {
    mockDevice.isDevice = false;
    mockUseDispatch.mockImplementation(() => (() => {}) as any);
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
  });

  it('should Set Reminder success', async () => {
    const toastSpy = mockActionCreator(actionsToast);
    const addToastAction = toastSpy('addToast');
    actionsWorkflowMock.setReminderWorkflow = () => {
      actionsWorkflowMock.setReminderWorkflow = {
        fulfilled: {
          match: () => true
        }
      };
    };

    const wrapper = await renderWithMockStore(
      <SetReminderModal {...mockProps} />,
      {
        initialState: {
          workflow: { ...defaultWorkflowState, reminderSet: { loading: true } },
          change: defaultChangeOwner,
          auth: {
            ...defaultAuth,
            currentUser: { ...defaultAuth.currentUser, userId: 12 }
          }
        } as AppState
      }
    );
    then_change_workflow_combobox(wrapper, {
      code: '1',
      text: 'Prime Interest Rate Change'
    });
    then_change_recurrence_dropdown(wrapper, 'Every weekday (Mon - Fri)');

    then_change_recipients_multiSelect(wrapper, {
      code: '001',
      text: 'John cena'
    });

    then_change_date_picker(wrapper, new Date());

    then_change_time_picker(wrapper, '12:59 PM');

    then_update_textarea(wrapper, 'abc test');

    then_click_notify_me_button(wrapper);

    then_click_save_button(wrapper);

    await wait();
    expect(addToastAction).toBeCalledWith({
      type: 'success',
      message: 'Reminder set.'
    });
  });

  it('should Set Reminder success', async () => {
    const toastSpy = mockActionCreator(actionsToast);
    const addToastAction = toastSpy('addToast');
    actionsWorkflowMock.setReminderWorkflow = () => {
      actionsWorkflowMock.setReminderWorkflow = {
        fulfilled: {
          match: () => true
        }
      };
    };

    const wrapper = await renderWithMockStore(
      <SetReminderModal {...mockProps} />,
      {
        initialState: {
          workflow: { ...defaultWorkflowState, reminderSet: { loading: true } },
          change: defaultChangeOwner,
          auth: defaultAuth
        } as AppState
      }
    );
    then_change_workflow_combobox(wrapper, {
      code: '1',
      text: 'Prime Interest Rate Change'
    });
    then_change_recurrence_dropdown(wrapper, 'Every weekday (Mon - Fri)');

    then_change_recipients_multiSelect(wrapper, {
      code: '001',
      text: 'John cena'
    });

    then_change_date_picker(wrapper, new Date());

    then_change_time_picker(wrapper, '00:01 AM');

    then_update_textarea(wrapper, new Array(254).fill(0).join(''));

    then_click_notify_me_button(wrapper);

    then_click_save_button(wrapper);

    await wait();
    expect(addToastAction).toBeCalledWith({
      type: 'success',
      message: 'Reminder set.'
    });
  });

  it('should Set Reminder failed', async () => {
    const toastSpy = mockActionCreator(actionsToast);
    const addToastAction = toastSpy('addToast');
    actionsWorkflowMock.setReminderWorkflow = () => {
      actionsWorkflowMock.setReminderWorkflow = {
        fulfilled: {
          match: () => false
        }
      };
    };

    const wrapper = await renderWithMockStore(
      <SetReminderModal {...mockProps} />,
      {
        initialState: {
          workflow: defaultWorkflowState,
          change: defaultChangeOwner,
          auth: defaultAuth
        } as AppState
      }
    );
    then_change_workflow_combobox(wrapper, {
      code: '1',
      text: 'Prime Interest Rate Change'
    });
    then_change_recurrence_dropdown(wrapper, 'Every weekday (Mon - Fri)');

    then_change_recipients_multiSelect(wrapper, {
      code: '001',
      text: 'John cena'
    });

    then_change_time_picker(wrapper, '00:01 AM');

    then_change_date_picker(wrapper, new Date());

    then_update_textarea(wrapper, 'abc test');

    then_click_notify_me_button(wrapper);

    then_click_save_button(wrapper);

    await wait();
    expect(addToastAction).toBeCalledWith({
      type: 'error',
      message: 'Reminder failed to set.'
    });
  });

  it('should hide modal', async () => {
    await renderWithMockStore(<SetReminderModal {...mockProps} />, {
      initialState: {
        workflow: defaultWorkflowState,
        change: defaultChangeOwner,
        auth: defaultAuth
      } as AppState
    });

    const cancelButton = document.body.querySelector('.btn-secondary');
    fireEvent.click(cancelButton!);
    expect(mockProps.onClose).toHaveBeenCalled();
  });
});
