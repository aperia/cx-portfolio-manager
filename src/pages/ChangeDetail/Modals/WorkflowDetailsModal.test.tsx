import { act, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { ReportStatus } from 'app/constants/enums';
import * as CanSetupWorkflowHelper from 'app/helpers/canSetupWorkflow';
import * as FileUtils from 'app/helpers/fileUtilities';
import {
  mockActionCreator,
  mockThunkAction,
  renderWithMockStore,
  wait
} from 'app/utils';
import 'app/utils/_mockComponent/mockComboBox';
import 'app/utils/_mockComponent/mockDatePicker';
import 'app/utils/_mockComponent/mockModalRegistry';
import 'app/utils/_mockComponent/mockMultiSelect';
import 'app/utils/_mockComponent/mockTimePicker';
import 'app/utils/_mockComponent/mockUseTranslation';
import mockDevice from 'app/utils/_mockHelperConstant/mockDevice';
import { actionsWorkflow } from 'pages/_commons/redux/Workflow';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import { actionsToast } from 'pages/_commons/ToastNotifications/redux';
import React from 'react';
import { WorkflowDetailsModal } from './WorkflowDetailsModal';

(function mockDOMMatrix() {
  class DOMMatrixMock {
    scale = jest.fn();
    translate = jest.fn();
  }
  global.DOMMatrix = DOMMatrixMock as any;
})();
jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/utils/MockView')
);
const downloadBlobFileMock = jest.fn();
jest
  .spyOn(FileUtils, 'downloadBlobFile')
  .mockImplementation(downloadBlobFileMock);

//src\pages\ChangeDetail\Modals\WorkflowDetailsModal.test.tsx
jest.mock('./RemoveWorkflowModal', () => ({
  __esModule: true,
  RemoveWorkflowModal: ({ onClose }: any) => (
    <div>
      Mock RemoveWorkflowModal
      <button onClick={() => onClose(false)}>close</button>
      <button onClick={() => onClose(true)}>close with reload</button>
    </div>
  )
}));

const mockActionsWorkflow = mockThunkAction(actionsWorkflow);
const mockActionsWorkflowSetup = mockThunkAction(actionsWorkflowSetup);
const defaultChangeState = {
  changeDetails: {
    data: { changeId: '10000001', changeStatus: 'work' }
  }
} as any;

const defaultWorkflowState = {
  workflowRemoval: { loading: false },
  mapping: { data: { downloadTemplateFile: { filename: 'filename' } } },
  changeWorkflowDetail: {
    data: {
      uploadedAttachment: [
        {
          id: '1525660907',
          category: 'allocation',
          filename: 'Development guilde.docx',
          mimeType: 'application/pdf',
          status: 'UNAVAILABLE',
          fileSize: 2600273,
          recordCount: 10,
          uploadedDate: '2021-02-27T00:00:00',
          uploadedBy: 'Laura Bailey'
        },
        {
          id: '53484625',
          category: 'qualification',
          filename: 'Development guilde.docx',
          mimeType: 'image/jpg',
          status: 'AVAILABLE',
          fileSize: 1052974,
          recordCount: 96,
          uploadedDate: '2021-01-22T00:00:00',
          uploadedBy: 'Freddy Kruger'
        },
        {
          id: '53284625',
          filename: 'accessibleFile.pdf',
          mimeType: 'image/jpg',
          status: 'UNAVAILABLE',
          fileSize: 1052974,
          recordCount: 96,
          uploadedDate: '2021-01-22T00:00:00',
          uploadedBy: 'Freddy Kruger'
        }
      ] as IAttachment[],
      reports: [
        {
          id: '1646894358',
          reportFilename: 'Employment Agreement.pdf',
          mimeType: 'application/json',
          status: ReportStatus.READY,
          reportFileSize: 3125926,
          recordCount: 87,
          systemGeneratedDate: '2020-12-26T00:00:00'
        }
      ] as IReport[],
      id: '651239863',
      name: 'Manage Penalty Fees Workflow',
      description: 'ut gubergren nulla et',
      updatedDate: '2021-09-14T11:11:19.5816275+07:00',
      updatedBy: 'Luke Skywalker',
      workflowType: 'correspondence',
      status: 'AVAILABLE',
      firstRan: '2021-09-16T11:11:19.5816341+07:00',
      firstRanBy: 'Jonah Hill',
      workflowNote: 'eos elit vero suscipit nobis',
      validationMessages: [
        {
          field: 'returnedCheckComputerLetter',
          severity: 'error',
          message: 'sea ea sit '
        },
        {
          field: 'minimumPaymentDueOption',
          severity: 'caution',
          message: 'no eos ea'
        }
      ],
      lastExecuted: '2021-09-17T11:11:19.5814848+07:00',
      isFavorite: true,
      actions: ['edit', 'delete']
    },
    loading: false,
    error: false
  } as any
} as any;

const mockProps: any = {
  id: 'id01',
  workflowId: 'w01',
  show: true,
  editable: true,
  onClose: jest.fn()
};

const then_click_download_button = (
  wrapper: RenderResult,
  filename: string
) => {
  const button = wrapper
    .getByText(filename)
    ?.closest('.attachment-item')
    ?.querySelectorAll('button, .btn-icon-secondary');

  act(() => {
    userEvent.hover(button![0]);
  });
  act(() => {
    userEvent.click(button![0]);
    userEvent.click(button![1]);
  });
};

const then_click_delete_workflow_button = (wrapper: RenderResult) => {
  const button = wrapper.getByText('txt_delete');

  userEvent.click(button!);
  userEvent.click(wrapper.getByText('close'));
  userEvent.click(button!);
  userEvent.click(wrapper.getByText('close with reload'));
};

const then_click_edit_workflow_button = (wrapper: RenderResult) => {
  const button = wrapper.getByText('txt_edit');
  act(() => {
    userEvent.click(button!);
  });
};

const mockCanSetupWorkflow = jest.spyOn(
  CanSetupWorkflowHelper,
  'canSetupWorkflow'
);

describe('WorkflowDetailsModal ', () => {
  beforeEach(() => {
    mockActionsWorkflow('downloadAttachment');
    mockActionsWorkflow('getChangeWorkflowDetails');
    mockDevice.isDevice = false;
    mockCanSetupWorkflow.mockReturnValue(true);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render with state is undefined and show is false', async () => {
    const props: any = {
      id: 'id01',
      show: false,
      editable: true,
      onClose: jest.fn()
    };
    await renderWithMockStore(<WorkflowDetailsModal {...props} />, {
      initialState: {} as AppState
    });
  });

  it('should render WorkflowDetailsModal and download success', async () => {
    const toastSpy = mockActionCreator(actionsToast);
    const addToastAction = toastSpy('addToast');
    mockActionsWorkflow('downloadAttachment', {
      match: true,
      payload: {
        payload: {
          filename: 'abc.pdf',
          mimeType: 'pdf',
          fileStream: btoa('aa')
        }
      }
    });

    const wrapper = await renderWithMockStore(
      <WorkflowDetailsModal {...mockProps} />,
      {
        initialState: {
          workflow: defaultWorkflowState,
          change: defaultChangeState
        } as AppState
      }
    );

    expect(
      wrapper.getByText('txt_validate_message_caution_found (1)')
    ).toBeInTheDocument();

    expect(
      wrapper.getByText('txt_validate_message_error_found (1)')
    ).toBeInTheDocument();
    then_click_download_button(wrapper, 'Employment Agreement.pdf');

    expect(addToastAction).not.toBeCalled();
  });

  it('should render WorkflowDetailsModal and download success without file name', async () => {
    const toastSpy = mockActionCreator(actionsToast);
    const addToastAction = toastSpy('addToast');
    mockActionsWorkflow('downloadAttachment', {
      match: true,
      payload: {}
    });

    const wrapper = await renderWithMockStore(
      <WorkflowDetailsModal {...mockProps} />,
      {
        initialState: {
          workflow: defaultWorkflowState,
          change: defaultChangeState
        } as AppState
      }
    );

    expect(
      wrapper.getByText('txt_validate_message_caution_found (1)')
    ).toBeInTheDocument();

    expect(
      wrapper.getByText('txt_validate_message_error_found (1)')
    ).toBeInTheDocument();
    then_click_download_button(wrapper, 'Employment Agreement.pdf');

    expect(addToastAction).not.toBeCalled();
  });

  it('should render WorkflowDetailsModal and download failed', async () => {
    mockDevice.isDevice = true;
    const toastSpy = mockActionCreator(actionsToast);
    const addToastAction = toastSpy('addToast');
    const mockDownloadAttachment = mockActionsWorkflow('downloadAttachment', {
      match: false
    });

    const wrapper = await renderWithMockStore(
      <WorkflowDetailsModal {...mockProps} />,
      {
        initialState: {
          workflow: {
            ...defaultWorkflowState,
            changeWorkflowDetail: {
              ...defaultWorkflowState.changeWorkflowDetail,
              data: {
                ...defaultWorkflowState.changeWorkflowDetail.data,
                validationMessages: [
                  {
                    field: 'returnedCheckComputerLetter',
                    severity: 'caution',
                    message: 'sea ea sit '
                  },
                  {
                    field: 'minimumPaymentDueOption',
                    severity: 'caution',
                    message: 'no eos ea'
                  }
                ]
              }
            }
          },
          change: {
            changeDetails: {
              data: { changeId: '10000001' }
            }
          }
        } as AppState
      }
    );
    expect(
      wrapper.getByText('txt_validate_message_cautions_found (2)')
    ).toBeInTheDocument();

    const warningPopper = wrapper
      .getByText('txt_validate_message_cautions_found (2)')
      .parentElement?.querySelector('.dls-popper-trigger') as HTMLInputElement;
    userEvent.click(warningPopper);

    then_click_download_button(wrapper, 'accessibleFile.pdf');
    expect(mockDownloadAttachment).not.toHaveBeenCalled();

    then_click_download_button(wrapper, 'Employment Agreement.pdf');
    expect(mockDownloadAttachment).toHaveBeenCalled();
    await wait();
    expect(addToastAction).toBeCalledWith({
      type: 'error',
      message: 'txt_file_failed_to_download'
    });
  });

  it('should render validate message error then click to popper errors', async () => {
    mockActionsWorkflow('downloadAttachment');
    const wrapper = await renderWithMockStore(
      <WorkflowDetailsModal {...mockProps} />,
      {
        initialState: {
          workflow: {
            ...defaultWorkflowState,
            changeWorkflowDetail: {
              ...defaultWorkflowState.changeWorkflowDetail,
              data: {
                ...defaultWorkflowState.changeWorkflowDetail.data,
                validationMessages: [
                  {
                    field: 'returnedCheckComputerLetter',
                    severity: 'error',
                    message: 'sea ea sit '
                  },
                  {
                    field: 'minimumPaymentDueOption',
                    severity: 'error',
                    message: 'no eos ea'
                  }
                ],
                reports: [
                  {
                    id: '1646894358',
                    reportFilename: 'accessibleFile.pdf',
                    mimeType: 'application/json',
                    status: ReportStatus.GENERATING,
                    reportFileSize: 3125926,
                    recordCount: 87,
                    systemGeneratedDate: '2020-12-26T00:00:00'
                  }
                ] as IReport[],
                uploadedAttachment: [
                  {
                    id: '1525660907',
                    category: 'qualification',
                    filename: 'Development guilde.docx',
                    mimeType: 'application/pdf',
                    status: 'UNAVAILABLE',
                    fileSize: 2600273,
                    recordCount: 10,
                    uploadedDate: '2021-02-27T00:00:00',
                    uploadedBy: 'Laura Bailey'
                  },
                  {
                    id: '53484625',
                    category: 'qualification',
                    filename: 'Development guilde.docx',
                    mimeType: 'image/jpg',
                    status: 'AVAILABLE',
                    fileSize: 1052974,
                    recordCount: 96,
                    uploadedDate: '2021-01-22T00:00:00',
                    uploadedBy: 'Freddy Kruger'
                  }
                ] as IAttachment[],
                actions: ['delete']
              }
            }
          },
          change: defaultChangeState
        } as AppState
      }
    );
    expect(
      wrapper.getByText('txt_validate_message_errors_found (2)')
    ).toBeInTheDocument();

    const errorPopper = wrapper
      .getByText('txt_validate_message_errors_found (2)')
      .parentElement?.querySelector('.dls-popper-trigger') as HTMLInputElement;
    userEvent.click(errorPopper);
  });

  it('should show Edit Workflow', async () => {
    const wrapper = await renderWithMockStore(
      <WorkflowDetailsModal {...mockProps} />,
      {
        initialState: {
          workflow: defaultWorkflowState,
          change: defaultChangeState
        } as AppState
      }
    );
    mockActionsWorkflowSetup('setWorkflowSetup', {
      callback: ({ onSubmitted }) => {
        onSubmitted && onSubmitted({ isDeleted: true });
      }
    });
    then_click_delete_workflow_button(wrapper);
    then_click_edit_workflow_button(wrapper);
  });

  it('should show FailedApiReload ', async () => {
    const wrapper = await renderWithMockStore(
      <WorkflowDetailsModal {...mockProps} />,
      {
        initialState: {
          workflow: {
            ...defaultWorkflowState,
            changeWorkflowDetail: {
              ...defaultWorkflowState.changeWorkflowDetail,
              error: true
            }
          },
          change: defaultChangeState
        } as AppState
      }
    );

    await wait();
    userEvent.click(wrapper.getByText('Reload'));

    expect(
      wrapper.getByText('Data load unsuccessful. Click Reload to try again.')
    ).toBeInTheDocument();
  });

  it('should hide modal', async () => {
    const wrapper = await renderWithMockStore(
      <WorkflowDetailsModal {...mockProps} />,
      {
        initialState: {
          workflow: defaultWorkflowState,
          change: defaultChangeState
        } as AppState
      }
    );

    userEvent.click(wrapper.getByText('Close'));
    expect(mockProps.onClose).toHaveBeenCalled();
  });
});
