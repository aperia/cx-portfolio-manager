import ModalRegistry from 'app/components/ModalRegistry';
import TextAreaCountDown from 'app/components/TextAreaCountDown';
import { useFormValidations } from 'app/hooks';
import {
  Button,
  ModalBody,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import isFunction from 'lodash.isfunction';
import { actionsChange } from 'pages/_commons/redux/Change';
import { actionsToast } from 'pages/_commons/ToastNotifications/redux';
import React, { useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

interface IProps {
  id: string;
  changeId: string;
  show: boolean;
  onClose?: (reload?: boolean) => void;
}

interface IFieldValidate {
  reason: string;
}

export const DemoteChangeModal: React.FC<IProps> = ({
  id,
  changeId,
  show,
  onClose
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const [reason, setReason] = useState('');

  const loading = useSelector<RootState, boolean>(
    state => state.change?.changeDemoteStatus?.loading || false
  );

  const currentErrors = useMemo(
    () =>
      ({
        reason: !reason?.trim() && {
          status: true,
          message: t('txt_validation_reason_required')
        }
      } as Record<keyof IFieldValidate, IFormError>),
    [reason, t]
  );

  const [, errors, setTouched] =
    useFormValidations<keyof IFieldValidate>(currentErrors);

  const isValid = useMemo(
    () => !Object.keys(errors).some(k => errors[k as keyof IFieldValidate]),
    [errors]
  );

  const onSave = async () => {
    const result: any = await Promise.resolve(
      dispatch(
        actionsChange.demoteChange({
          changeId,
          reason: reason.trim()
        })
      )
    );
    const isSuccess = actionsChange.demoteChange.fulfilled.match(result);

    if (isSuccess) {
      dispatch(
        actionsToast.addToast({
          type: 'success',
          message: t('txt_change_demoted')
        })
      );
      isFunction(onClose) && onClose(isSuccess);
    } else {
      dispatch(
        actionsToast.addToast({
          type: 'error',
          message: t('txt_change_failed_to_demote')
        })
      );
    }
  };

  const handleCloseWithoutReload = () => {
    isFunction(onClose) && onClose();
  };

  const handleOnReasonChange = (e: any) => {
    setReason(e.target.value);
  };

  return (
    <ModalRegistry
      id={id}
      show={show}
      onAutoClosedAll={handleCloseWithoutReload}
      classes={{ content: loading ? 'loading' : '' }}
    >
      <ModalHeader border closeButton onHide={handleCloseWithoutReload}>
        <ModalTitle>{t('txt_demote_change_title')}</ModalTitle>
      </ModalHeader>
      <ModalBody>
        <p>{t('txt_demote_change_desc')} </p>
        <div>
          <div className="mt-16 mb-12 text-uppercase color-grey fw-500 fs-11">
            {t('txt_reason')} <span className="color-red">*</span>
          </div>
          <TextAreaCountDown
            id={`${id}__reason`}
            className="resize-none"
            value={reason}
            onChange={handleOnReasonChange}
            required
            maxLength={500}
            onFocus={setTouched('reason')}
            onBlur={setTouched('reason', true)}
            error={errors.reason}
          />
        </div>
      </ModalBody>
      <div className="dls-modal-footer modal-footer">
        <Button variant="secondary" onClick={handleCloseWithoutReload}>
          {t('txt_cancel')}
        </Button>
        <Button
          variant="danger"
          onClick={onSave}
          disabled={!isValid || Object.keys(errors).length === 0}
        >
          {t('txt_demote')}
        </Button>
      </div>
    </ModalRegistry>
  );
};
