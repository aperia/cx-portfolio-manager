import { fireEvent } from '@testing-library/react';
import {
  mockUseDispatchFnc,
  mockUseModalRegistryFnc,
  renderComponent
} from 'app/utils';
import { actionsChange, ChangeState } from 'pages/_commons/redux/Change';
import React from 'react';
import * as reactRedux from 'react-redux';
import { DemoteChangeModal } from './DemoteChangeModal';

jest.mock('app/_libraries/_dls', () => {
  const actualModules = jest.requireActual('app/_libraries/_dls');
  const translation = { t: (key: string) => key };
  return {
    ...actualModules,
    useTranslation: () => translation
  };
});

const mockUseModalRegistry = mockUseModalRegistryFnc();
const mockUseDispatch = mockUseDispatchFnc();

const actionsChangeMock: any = actionsChange;
let selectorMock: jest.SpyInstance;
const mockSelectorChanges = (changeState: ChangeState) => {
  selectorMock = jest
    .spyOn(reactRedux, 'useSelector')
    .mockImplementation(selector => selector({ change: changeState }));
};

const mockProps: any = {
  id: 'id01',
  changeId: 'changeId01',
  show: true,
  onClose: jest.fn()
};

const testId = 'demoteChange';
const then_update_textarea = (value: string) => {
  const inputElement = document.body.querySelector(
    `.text-field-container > textarea`
  );

  fireEvent.change(inputElement!, { target: { value: value } });
  fireEvent.blur(inputElement!);
};
const then_click_demote_button = () => {
  const rejectButton = document.body.querySelector('.btn-danger');
  fireEvent.click(rejectButton!);
};

const mock_show_loading = (show: boolean) => {
  const pre = {
    changeDemoteStatus: {
      loading: show
    }
  } as any;

  mockSelectorChanges(pre);
};

describe('Demote Change ', () => {
  const createWrapper = async (...props: any[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    await renderComponent(testId, <DemoteChangeModal {...mergeProps} />);
  };

  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => (() => {}) as any);
    mockUseModalRegistry.mockImplementation(() => [true, false]);
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
    mockUseModalRegistry.mockClear();
    selectorMock.mockClear();
  });

  it('should demote change success', async () => {
    mock_show_loading(true);
    actionsChangeMock.demoteChange = () => {
      actionsChangeMock.demoteChange = {
        fulfilled: {
          match: () => true
        }
      };
    };
    await createWrapper(mockProps);

    then_update_textarea('abc test');
    then_click_demote_button();
    expect(mockUseDispatch).toHaveBeenCalled();
  });

  it('should demote change failed', async () => {
    mock_show_loading(false);

    actionsChangeMock.demoteChange = () => {
      actionsChangeMock.demoteChange = {
        fulfilled: {
          match: () => false
        }
      };
    };
    await createWrapper(mockProps);

    then_update_textarea(
      '123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 1234 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 1234'
    );
    then_click_demote_button();
    expect(mockUseDispatch).toHaveBeenCalled();
  });

  it('should hide modal', async () => {
    await createWrapper(mockProps);
    const cancelButton = document.body.querySelector('.btn-secondary');
    fireEvent.click(cancelButton!);
    expect(mockProps.onClose).toHaveBeenCalled();
  });
});
