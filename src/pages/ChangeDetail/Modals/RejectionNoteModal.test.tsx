import { fireEvent, screen } from '@testing-library/react';
import { mockUseModalRegistryFnc, renderComponent } from 'app/utils';
import { App as DOFApp } from 'app/_libraries/_dof/core';
import React from 'react';
import { RejectionNoteModal } from './RejectionNoteModal';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/utils/MockView')
);

jest.mock('app/_libraries/_dls', () => {
  const actualModules = jest.requireActual('app/_libraries/_dls');
  const translation = { t: (key: string) => key };
  return {
    ...actualModules,
    useTranslation: () => translation
  };
});

const mockUseModalRegistry = mockUseModalRegistryFnc();

const mockProps: any = {
  id: 'id01',
  rejectNote: {
    rejectedBy: 'rejectedBy',
    rejectedReason: 'rejectedReason'
  },
  show: true,
  onClose: jest.fn()
};

const testId = 'rejectionNoteModal';

describe('Rejection Note Modal', () => {
  const createWrapper = async (...props: any[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    await renderComponent(
      testId,
      <DOFApp urlConfigs={[]}>
        <RejectionNoteModal {...(mergeProps as any)} />
      </DOFApp>
    );
  };

  beforeEach(() => {
    mockUseModalRegistry.mockImplementation(() => [true, false]);
  });

  afterEach(() => {
    mockUseModalRegistry.mockClear();
  });

  it('should show rejection note modal', async () => {
    await createWrapper(mockProps);
    expect(screen.getByTestId('change-rejection-note')).not.toBeEmpty();
  });

  it('should hide modal', async () => {
    await createWrapper(mockProps);
    const cancelButton = document.body.querySelector('.btn-secondary');
    fireEvent.click(cancelButton!);
    expect(mockProps.onClose).toHaveBeenCalled();
  });
});
