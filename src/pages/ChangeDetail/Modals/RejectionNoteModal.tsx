import ModalRegistry from 'app/components/ModalRegistry';
import {
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import { View } from 'app/_libraries/_dof/core';
import React from 'react';

interface IRejectNote {
  rejectedBy: string;
  rejectedReason: string;
}

interface IRejectionNoteModalProps {
  id: string;
  show: boolean;
  rejectNote: IRejectNote;
  onClose?: (reload?: boolean) => void;
}
export const RejectionNoteModal: React.FC<IRejectionNoteModalProps> = ({
  id,
  show,
  rejectNote,
  onClose
}) => {
  const { t } = useTranslation();
  return (
    <ModalRegistry id={id} show={show} onAutoClosedAll={onClose}>
      <ModalHeader border closeButton onHide={onClose}>
        <ModalTitle>{t('txt_snapshot_reject_reason')}</ModalTitle>
      </ModalHeader>
      <ModalBody className="px-24 pt-8 overflow-auto">
        <View
          formKey={`${id}__change-rejection-note-view`}
          descriptor="change-rejection-note"
          value={rejectNote}
        />
      </ModalBody>
      <ModalFooter cancelButtonText={t('txt_close')} onCancel={onClose} />
    </ModalRegistry>
  );
};
