import AttachmentItem from 'app/components/AttachmentItem';
import BubbleGroupTextControl from 'app/components/DofControl/BubbleGroupTextControl';
import GroupTextControl from 'app/components/DofControl/GroupTextControl';
import FailedApiReload from 'app/components/FailedApiReload';
import ModalRegistry from 'app/components/ModalRegistry';
import {
  FileStatus,
  GroupTextFormat,
  GroupTextFormatType,
  ReportStatus
} from 'app/constants/enums';
import { MIME_TYPE } from 'app/constants/mine-type';
import {
  camelToText,
  canSetupWorkflow,
  classnames,
  isDevice
} from 'app/helpers';
import {
  base64toBlob,
  bytesToFileSize,
  downloadBlobFile
} from 'app/helpers/fileUtilities';
import {
  Button,
  Icon,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  Popover,
  SimpleBar,
  Tooltip,
  TransDLS,
  useTranslation
} from 'app/_libraries/_dls';
import { View } from 'app/_libraries/_dof/core';
import isFunction from 'lodash.isfunction';
import orderBy from 'lodash.orderby';
import { useSelectCurrentChangeDetail } from 'pages/_commons/redux/Change';
import {
  actionsWorkflow,
  useSelectChangeWorkflowDetail
} from 'pages/_commons/redux/Workflow';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import { actionsToast } from 'pages/_commons/ToastNotifications/redux';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch } from 'react-redux';
import { RemoveWorkflowModal } from '.';

interface IProps {
  id: string;
  workflowId: string;
  show: boolean;

  editable: boolean;
  onClose?: (reload?: boolean) => void;
}
export const WorkflowDetailsModal: React.FC<IProps> = ({
  id,
  workflowId,
  show,

  editable,
  onClose
}) => {
  const { t } = useTranslation();

  const [showTooltipError, setShowTooltipError] = useState(true);
  const [showTooltipWarning, setShowTooltipWarning] = useState(true);
  const dispatch = useDispatch();
  const [showRemoveWorkflow, setShowRemoveWorkflow] = useState(false);

  const { data: changeDetail = {} as any } = useSelectCurrentChangeDetail();
  const {
    loading,
    error,
    data: {
      uploadedAttachment = [],
      reports: downloadableAttachment = [],
      ...detail
    } = {} as IWorkflowDetail
  } = useSelectChangeWorkflowDetail();

  const showValidationFeedbackError = useMemo(
    () => changeDetail.changeStatus?.toLowerCase() === 'work',
    [changeDetail.changeStatus]
  );

  useEffect(() => {
    if (!workflowId) return;

    dispatch(actionsWorkflow.getChangeWorkflowDetails(workflowId));

    return () => {
      dispatch(actionsWorkflow.resetWorkflowDetail());
    };
  }, [dispatch, workflowId]);

  const handleApiReload = useCallback(() => {
    dispatch(actionsWorkflow.getChangeWorkflowDetails(workflowId));
  }, [dispatch, workflowId]);

  const handleReloadAfterEditWorkflow = useCallback(() => {
    dispatch(actionsWorkflow.getChangeWorkflowDetails(workflowId));
    dispatch(actionsWorkflow.getChangeWorkflows(changeDetail.changeId));
  }, [dispatch, workflowId, changeDetail.changeId]);

  const canEditWorkflow = useMemo(
    () =>
      detail?.actions?.some((i: string) => i?.toLowerCase().includes('edit')),
    [detail.actions]
  );

  const canDeleteWorkflow = useMemo(
    () =>
      detail?.actions?.some((i: string) => i?.toLowerCase().includes('delete')),
    [detail.actions]
  );

  const uploadedAllocationAttachments = useMemo(
    () =>
      uploadedAttachment.filter(i =>
        (i.category || '').toLowerCase().includes('allocation')
      ),
    [uploadedAttachment]
  );

  const uploadedQualificationAttachments = useMemo(
    () =>
      uploadedAttachment.filter(i =>
        (i.category || '').toLowerCase().includes('qualification')
      ),
    [uploadedAttachment]
  );

  const unCategoryAttachments = useMemo(
    () => uploadedAttachment.filter(i => i.category == null),
    [uploadedAttachment]
  );

  const hasReport = useMemo(() => {
    return downloadableAttachment && downloadableAttachment.length > 0;
  }, [downloadableAttachment]);

  const hasAttachment = useMemo(() => {
    return (
      uploadedAllocationAttachments?.length > 0 ||
      uploadedQualificationAttachments?.length > 0 ||
      unCategoryAttachments?.length > 0
    );
  }, [
    uploadedAllocationAttachments,
    uploadedQualificationAttachments,
    unCategoryAttachments
  ]);

  const messages = useMemo(
    () =>
      detail.validationMessages?.filter(
        m =>
          showValidationFeedbackError || m.severity?.toLowerCase() === 'caution'
      ) || [],
    [detail.validationMessages, showValidationFeedbackError]
  );

  const errors = useMemo(
    () =>
      orderBy(
        messages?.filter(m => m.severity?.toLowerCase() === 'error'),
        ['field'],
        ['asc']
      ),
    [messages]
  );

  const warnings = useMemo(
    () =>
      orderBy(
        messages?.filter(m => m.severity?.toLowerCase() === 'caution'),
        ['field'],
        ['asc']
      ),
    [messages]
  );

  const handleCloseWithoutReload = useCallback(() => {
    isFunction(onClose) && onClose();
  }, [onClose]);

  const handleEditWorkflow = useCallback(() => {
    canSetupWorkflow(detail?.name) &&
      dispatch(
        actionsWorkflowSetup.setWorkflowSetup({
          isTemplate: false,
          workflow: detail as any,
          onSubmitted: ({ isDeleted }) => {
            handleReloadAfterEditWorkflow();

            isDeleted && handleCloseWithoutReload();
            return true;
          }
        })
      );
  }, [
    dispatch,
    handleReloadAfterEditWorkflow,
    handleCloseWithoutReload,
    detail
  ]);

  const getFeedbackPopupElement = useCallback(
    (mgs: IValidationMessage[]) => {
      const messagesLength = mgs.length;

      return (
        <div
          style={{ maxHeight: '360px' }}
          className="m-n16 p-16 overflow-auto"
        >
          {mgs.map((m, idx) => (
            <div
              key={idx}
              className={classnames('d-flex justify-content-between', {
                'border-top': idx !== 0,
                'py-16':
                  messagesLength > 1 && idx !== 0 && idx < messagesLength - 1,
                'pb-16': messagesLength > 1 && idx === 0,
                'pt-16': messagesLength > 1 && idx === messagesLength - 1
              })}
            >
              <div className="d-flex">
                <span className="pr-4">•</span>
                <span>
                  {!m.isChangeFeedback && m.field && (
                    <React.Fragment>
                      <span className="fw-500">{camelToText(m.field)}</span>
                      <span className="px-4">-</span>
                    </React.Fragment>
                  )}
                  <span>{m.message}</span>
                </span>
              </div>
              {detail.actions?.some(a => a.toLowerCase() === 'edit') && (
                <Button
                  className="ml-8 mr-n8"
                  size="sm"
                  variant="outline-primary"
                  onClick={handleEditWorkflow}
                >
                  {t('txt_edit')}
                </Button>
              )}
            </div>
          ))}
        </div>
      );
    },
    [detail.actions, t, handleEditWorkflow]
  );

  if (!show) return <React.Fragment />;

  return (
    <React.Fragment>
      <ModalRegistry
        rt
        id={id}
        show={true}
        animationComponentProps={{ direction: 'left' }}
        onAutoClosedAll={handleCloseWithoutReload}
      >
        <ModalHeader border closeButton onHide={handleCloseWithoutReload}>
          <ModalTitle>{t('txt_workflow_detail_title')}</ModalTitle>
        </ModalHeader>
        <ModalBody className={classnames('p-0 overflow-auto', { loading })}>
          {error && (
            <FailedApiReload
              id="home-page-change-card-error"
              onReload={handleApiReload}
              className="mt-40 pt-40 d-flex flex-column justify-content-center align-items-center"
            />
          )}
          {!loading && !error && (
            <SimpleBar>
              {(errors.length > 0 || warnings.length > 0) && (
                <div className="bg-orange-l40 p-24 border-bottom">
                  <p>{t('txt_validate_message_desc')}</p>
                  <div className="d-flex align-items-center mt-16">
                    {errors?.length > 0 && (
                      <div className="d-flex w-50 align-items-center">
                        <Icon name="error" color="red" size="4x" />
                        <span className="color-red-d16 fw-500 pl-4 pr-8">
                          {errors.length === 1
                            ? t('txt_validate_message_error_found')
                            : t('txt_validate_message_errors_found')}{' '}
                          ({errors.length})
                        </span>
                        <Popover
                          element={getFeedbackPopupElement(errors)}
                          size="md"
                          onVisibilityChange={opened =>
                            setShowTooltipError(!opened)
                          }
                        >
                          <Tooltip
                            element={t('txt_view_feedback')}
                            variant="primary"
                            opened={
                              isDevice || !showTooltipError ? false : undefined
                            }
                          >
                            <Button variant="icon-secondary" size="sm">
                              <Icon name="information" />
                            </Button>
                          </Tooltip>
                        </Popover>
                      </div>
                    )}
                    {warnings.length > 0 && (
                      <div className="d-flex w-50 align-items-center">
                        <Icon name="warning" color="orange" size="4x" />
                        <span className="color-orange-d16 fw-500 pl-4 pr-8">
                          {warnings.length === 1
                            ? t('txt_validate_message_caution_found')
                            : t('txt_validate_message_cautions_found')}{' '}
                          ({warnings.length})
                        </span>
                        <Popover
                          element={getFeedbackPopupElement(warnings)}
                          size="md"
                          onVisibilityChange={opened =>
                            setShowTooltipWarning(!opened)
                          }
                        >
                          <Tooltip
                            element={t('txt_view_feedback')}
                            variant="primary"
                            opened={
                              isDevice || !showTooltipWarning
                                ? false
                                : undefined
                            }
                          >
                            <Button variant="icon-secondary" size="sm">
                              <Icon name="information" />
                            </Button>
                          </Tooltip>
                        </Popover>
                      </div>
                    )}
                  </div>
                </div>
              )}
              <div className="bg-light-l20 p-24">
                <div className="d-flex align-items-center">
                  <h5>{detail?.name}</h5>

                  {editable && (canEditWorkflow || canDeleteWorkflow) && (
                    <div className="pl-16 mr-n8 flex-fill d-flex align-items-center justify-content-end">
                      {canEditWorkflow && (
                        <Button
                          variant="outline-primary"
                          size="sm"
                          onClick={handleEditWorkflow}
                        >
                          {t('txt_edit')}
                        </Button>
                      )}
                      {canDeleteWorkflow && (
                        <Button
                          className="ml-8"
                          variant="outline-danger"
                          size="sm"
                          onClick={() => setShowRemoveWorkflow(true)}
                        >
                          {t('txt_delete')}
                        </Button>
                      )}
                    </div>
                  )}
                </div>
                <View
                  id={`change-workflow-detail-snapshot-view__${id}`}
                  formKey={`change-workflow-detail-snapshot-view__${id}`}
                  descriptor="change-workflow-detail-snapshot-view"
                  value={detail}
                />
              </div>
              <div className="p-24 border-top">
                <div className="mb-16">
                  <h5>{t('txt_details')}</h5>
                  <View
                    id={`change-workflow-detail-details-tab-view__${id}`}
                    formKey={`change-workflow-detail-details-tab-view__${id}`}
                    descriptor="change-workflow-detail-details-tab-view"
                    value={detail}
                  />
                </div>
                {hasReport && (
                  <div className="py-16 border-top">
                    <h5 className="mb-16">
                      {t('txt_workflow_detail_reports')}
                    </h5>
                    {downloadableAttachment.every(
                      r => r.status?.toUpperCase() === ReportStatus.READY
                    ) ? (
                      <div className="attachment-group">
                        {downloadableAttachment.map(att => (
                          <Attachment
                            key={att.id}
                            item={{
                              ...att,
                              id: att.id,
                              filename: att.reportFilename,
                              fileSize: att.reportFileSize,
                              uploadedBy: 'System',
                              uploadedDate: att.systemGeneratedDate,
                              category: ''
                            }}
                            systemGenerated
                          />
                        ))}
                      </div>
                    ) : (
                      <div className="border rounded-lg py-8 px-16 bg-light-l20">
                        {t('txt_workflow_detail_reports_not_ready')}
                        <div className="mt-8">
                          <TransDLS
                            keyTranslation="txt_workflow_report_reload_message"
                            count={0}
                          >
                            <a className="link" onClick={handleApiReload} />
                          </TransDLS>
                        </div>
                      </div>
                    )}
                  </div>
                )}

                {hasAttachment && (
                  <div className="pt-16 border-top mb-n24">
                    <h5 className="mb-16">
                      {t('txt_workflow_detail_uploaded_files')}
                    </h5>

                    {uploadedAllocationAttachments?.length > 0 && (
                      <div className="mb-24">
                        <span className="mb-16 d-block fs-11 fw-500 text-uppercase color-grey">
                          {t('txt_workflow_client_allocation_table')}
                        </span>
                        <div className="attachment-group">
                          {uploadedAllocationAttachments.map(att => (
                            <Attachment key={att.id} item={att} />
                          ))}
                        </div>
                      </div>
                    )}

                    {uploadedQualificationAttachments?.length > 0 && (
                      <div className="mb-24">
                        <span className="mb-16 d-block fs-11 fw-500 text-uppercase color-grey">
                          {t('txt_workflow_account_qualification_table')}
                        </span>
                        <div className="attachment-group">
                          {uploadedQualificationAttachments.map(att => (
                            <Attachment key={att.id} item={att} />
                          ))}
                        </div>
                      </div>
                    )}

                    {unCategoryAttachments?.length > 0 && (
                      <div className="mb-24">
                        <div className="attachment-group">
                          {unCategoryAttachments.map(att => (
                            <Attachment key={att.id} item={att} />
                          ))}
                        </div>
                      </div>
                    )}
                  </div>
                )}
              </div>
            </SimpleBar>
          )}
        </ModalBody>
        <ModalFooter
          border
          cancelButtonText="Close"
          onCancel={handleCloseWithoutReload}
        />
      </ModalRegistry>

      {showRemoveWorkflow && (
        <RemoveWorkflowModal
          id={`${id}_removeWorlflowModal`}
          show={showRemoveWorkflow}
          workflowId={detail.id}
          workflowName={detail.name}
          onClose={reload => {
            setShowRemoveWorkflow(false);

            if (reload) {
              isFunction(onClose) && onClose(true);
            }
          }}
        />
      )}
    </React.Fragment>
  );
};

interface IAttachmentProps {
  item: IAttachment;
  systemGenerated?: boolean;
}
const Attachment: React.FC<IAttachmentProps> = ({
  item: {
    id,
    filename,
    status,
    uploadedBy,
    uploadedDate,
    recordCount,
    fileSize,
    mimeType
  },
  systemGenerated
}) => {
  const { t } = useTranslation();
  const [infoTooltipOpened, setInfoTooltipOpened] = useState(false);
  const [infoOpened, setInfoOpened] = useState(false);
  const [downloadProtectedOpened, setDownloadProtectedOpened] = useState(false);
  const dispatch = useDispatch();

  const accessible = useMemo(() => {
    return (
      status?.toUpperCase() === FileStatus.AVAILABLE ||
      status?.toUpperCase() === ReportStatus.READY
    );
  }, [status]);

  const downloadAttachment = async () => {
    if (!accessible) {
      setDownloadProtectedOpened(opened => !opened);
      return;
    }

    const result: any = await Promise.resolve(
      dispatch(actionsWorkflow.downloadAttachment(id?.toString()))
    );

    const isSuccess =
      actionsWorkflow.downloadAttachment.fulfilled.match(result);

    if (isSuccess) {
      const {
        fileStream = '',
        mimeType: mimeTypeDownload = '',
        filename: filenameDownload = ''
      } = (result?.payload || {}) as IDownloadAttachment;

      let fileName = filenameDownload;
      if (filenameDownload.split('.').length < 2) {
        const ext = MIME_TYPE[mimeTypeDownload]?.extensions[0] || '';
        fileName = `${filenameDownload}.${ext}`;
      }
      const blol = base64toBlob(fileStream, mimeTypeDownload);
      downloadBlobFile(blol, fileName);
    } else {
      dispatch(
        actionsToast.addToast({
          type: 'error',
          message: t('txt_file_failed_to_download')
        })
      );
    }
  };

  const detailElement = useMemo(
    () => (
      <table>
        <tbody>
          {!systemGenerated && (
            <tr>
              <td>
                <span className="fs-14 fw-500">
                  {t('txt_workflow_upload_by')}
                </span>
              </td>
              <td className="pl-24">
                <BubbleGroupTextControl
                  id={`workflowDetail_attachment_${id}__uploadedBy`}
                  input={{ value: uploadedBy } as any}
                  options={{ truncate: false }}
                  meta={{} as any}
                />
              </td>
            </tr>
          )}
          <tr>
            <td>
              <span className="fs-14 fw-500">
                {systemGenerated
                  ? t('txt_workflow_system_generated')
                  : t('txt_workflow_upload_date')}
              </span>
            </td>
            <td className="pl-24">
              <GroupTextControl
                id={`workflowDetail_attachment_${id}__uploadedDate`}
                input={{ value: uploadedDate } as any}
                options={{ format: GroupTextFormat.AllDatetime }}
                meta={{} as any}
              />
            </td>
          </tr>
          <tr>
            <td>
              <span className="fs-14 fw-500">
                {t('txt_workflow_upload_record_count')}
              </span>
            </td>
            <td className="pl-24">
              <GroupTextControl
                id={`workflowDetail_attachment_${id}__recordCount`}
                options={{
                  format: GroupTextFormat.Amount,
                  formatType: GroupTextFormatType.Quantity
                }}
                input={{ value: recordCount } as any}
                meta={{} as any}
              />
            </td>
          </tr>
          <tr>
            <td>
              <span className="fs-14 fw-500">
                {t('txt_workflow_upload_file_size')}
              </span>
            </td>
            <td className="pl-24">
              <GroupTextControl
                id={`workflowDetail_attachment_${id}__fileSize`}
                input={{ value: bytesToFileSize(fileSize, 0) } as any}
                meta={{} as any}
              />
            </td>
          </tr>
        </tbody>
      </table>
    ),
    [id, uploadedBy, uploadedDate, recordCount, fileSize, t, systemGenerated]
  );

  return (
    <AttachmentItem
      mimeType={mimeType}
      fileName={filename}
      suffix={
        <div className="mr-n4 d-flex">
          {!accessible && (
            <div className="mr-8 align-self-center color-grey-l16 fs-11">
              {t('txt_protected')}
            </div>
          )}

          <Tooltip
            element="Information"
            variant="primary"
            opened={isDevice ? false : infoTooltipOpened && !infoOpened}
            onVisibilityChange={setInfoTooltipOpened}
          >
            <Popover
              placement="top-end"
              size="md"
              element={detailElement}
              opened={infoOpened}
              onVisibilityChange={setInfoOpened}
              containerClassName="dls-tooltip-container"
            >
              <Button
                variant="icon-secondary"
                className={classnames('d-inline', { active: infoOpened })}
                size="sm"
                onClick={() => setInfoOpened(opened => !opened)}
              >
                <Icon name="information" color={'grey-l16' as any} size="4x" />
              </Button>
            </Popover>
          </Tooltip>
          <Tooltip
            element={
              !accessible
                ? t('txt_file_contains_sensitive_data')
                : t('txt_download')
            }
            variant={!accessible ? undefined : 'primary'}
            opened={isDevice ? false : undefined}
          >
            <Popover
              placement="top-end"
              size="auto"
              element={t('txt_file_contains_sensitive_data')}
              opened={isDevice && downloadProtectedOpened}
              onVisibilityChange={setDownloadProtectedOpened}
            >
              <Button
                variant="icon-secondary"
                className={classnames(
                  'd-inline ml-8',
                  !accessible && 'cursor-not-allowed'
                )}
                size="sm"
                onClick={downloadAttachment}
              >
                <Icon
                  name="download"
                  color={(!accessible ? 'grey-l40' : 'grey-l16') as any}
                  size="4x"
                />
              </Button>
            </Popover>
          </Tooltip>
        </div>
      }
    />
  );
};
