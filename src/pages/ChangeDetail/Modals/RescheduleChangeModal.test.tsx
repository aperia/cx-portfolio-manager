import { fireEvent, screen } from '@testing-library/react';
import {
  mockUseDispatchFnc,
  mockUseModalRegistryFnc,
  renderComponent
} from 'app/utils';
import { mockActionCreator } from 'app/utils/mockActionCreator';
import {
  DatePickerChangeEvent,
  DatePickerProps
} from 'app/_libraries/_dls/components/DatePicker';
import { actionsChange } from 'pages/_commons/redux/Change';
import * as ChangeSelector from 'pages/_commons/redux/Change/select-hooks';
import { actionsToast } from 'pages/_commons/ToastNotifications/redux';
import React from 'react';
import { RescheduleChangeModal } from './RescheduleChangeModal';

const mockUseSelectLoadingOnUpdateChange = (loading: boolean) => {
  jest
    .spyOn(ChangeSelector, 'useSelectLoadingOnUpdateChange')
    .mockReturnValue(loading);
};

const mockUseSelectChangeDateInfo = (value: any) => {
  jest.spyOn(ChangeSelector, 'useSelectChangeDateInfo').mockReturnValue(value);
};
jest.mock('app/_libraries/_dls', () => {
  const actualModules = jest.requireActual('app/_libraries/_dls');
  const translation = { t: (key: string) => key };
  return {
    ...actualModules,
    useTranslation: () => translation
  };
});

jest.mock('app/_libraries/_dls/components/DatePicker', () => {
  return {
    __esModule: true,
    default: ({ onChange, dateTooltip }: DatePickerProps) => (
      <>
        <input
          data-testid="DatePicker_onChange"
          onChange={(e: any) => {
            onChange!({
              target: { value: e.target.date, name: e.target.name }
            } as DatePickerChangeEvent);
            dateTooltip!(e.target.date as Date, e.target.view);
          }}
        />
      </>
    )
  };
});

const mockUseModalRegistry = mockUseModalRegistryFnc();
const mockUseDispatch = mockUseDispatchFnc();

const actionsChangeMock: any = actionsChange;

const mockProps: any = {
  id: 'id01',
  changeDetail: { changeId: 'changeId01', effectiveDate: '2021/05/04' },
  show: true,
  onClose: jest.fn()
};

const testId = 'rescheduleChange';

const then_click_save_button = () => {
  const rejectButton = document.body.querySelector('.btn-primary');
  fireEvent.click(rejectButton!);
};

const then_change_date_picker = (value: string, view: string | undefined) => {
  const inputElement = screen.getByTestId('DatePicker_onChange');
  fireEvent.change(inputElement, {
    target: {
      value: 'undefined',
      name: 'dateValue',
      date: new Date(value),
      view: view
    }
  });
  fireEvent.blur(inputElement!);
};

describe('Reschedule Change ', () => {
  const createWrapper = async (...props: any[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    await renderComponent(testId, <RescheduleChangeModal {...mergeProps} />);
  };

  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => (() => {}) as any);
    mockUseModalRegistry.mockImplementation(() => [true, false]);
    mockUseSelectChangeDateInfo({
      data: {
        minDate: '2021-02-02',
        maxDate: '2021-04-05',
        invalidDate: [
          { date: '2021-03-04', reason: 'reason' },
          { date: '2021-04-04', reason: 'reason' }
        ]
      }
    });
    mockUseSelectLoadingOnUpdateChange(false);
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
    mockUseModalRegistry.mockClear();
  });

  it('should reschedule change success', async () => {
    const toastSpy = mockActionCreator(actionsToast);
    const addToastAction = toastSpy('addToast');
    mockUseSelectLoadingOnUpdateChange(true);
    actionsChangeMock.rescheduleChange = () => {
      actionsChangeMock.rescheduleChange = {
        fulfilled: {
          match: () => true
        }
      };
    };
    await createWrapper(mockProps);
    then_change_date_picker('2021/02/03', undefined);
    then_click_save_button();
    await new Promise(resolve => setTimeout(() => resolve(0)));
    expect(addToastAction).toBeCalledWith({
      type: 'success',
      message: 'txt_notification_change_reschedule_success'
    });
  });

  it('should reschedule change failed when call api is reject', async () => {
    const toastSpy = mockActionCreator(actionsToast);
    const addToastAction = toastSpy('addToast');
    actionsChangeMock.rescheduleChange = () => {
      actionsChangeMock.rescheduleChange = {
        fulfilled: {
          match: () => false
        }
      };
    };
    await createWrapper(mockProps);
    then_change_date_picker('2021/02/03', 'year');
    then_click_save_button();
    await new Promise(resolve => setTimeout(() => resolve(0)));

    expect(addToastAction).toBeCalledWith({
      type: 'error',
      message: 'txt_notification_change_reschedule_fail'
    });
  });

  it('should show error when effectiveDate is invalid', async () => {
    const toastSpy = mockActionCreator(actionsToast);
    const addToastAction = toastSpy('addToast');
    await createWrapper({ ...mockProps, changeDetail: {} });

    then_change_date_picker('2021-04-04', 'month');
    then_click_save_button();
    expect(addToastAction).not.toHaveBeenCalled();
  });

  it('should show error when invalidDate is undefined', async () => {
    mockUseSelectChangeDateInfo({
      data: {
        invalidDate: undefined
      }
    });
    const toastSpy = mockActionCreator(actionsToast);
    const addToastAction = toastSpy('addToast');
    await createWrapper(mockProps);

    then_change_date_picker('2021-04-04', 'month');
    then_click_save_button();
    expect(addToastAction).not.toHaveBeenCalled();
  });

  it('should hide modal', async () => {
    await createWrapper(mockProps);
    const cancelButton = document.body.querySelector('.btn-secondary');
    fireEvent.click(cancelButton!);
    expect(mockProps.onClose).toHaveBeenCalled();
  });
});
