import EnhanceDatePicker from 'app/components/EnhanceDatePicker';
import ModalRegistry from 'app/components/ModalRegistry';
import { FormatTime } from 'app/constants/enums';
import {
  addCurrentTime,
  convertLocalDateToUTCDate,
  formatTimeDefault
} from 'app/helpers';
import { useFormValidations } from 'app/hooks';
import {
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import isFunction from 'lodash.isfunction';
import {
  actionsChange,
  useSelectChangeDateInfo,
  useSelectLoadingOnUpdateChange
} from 'pages/_commons/redux/Change';
import { actionsToast } from 'pages/_commons/ToastNotifications/redux';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { Detail } from 'react-calendar';
import { useDispatch } from 'react-redux';

interface IProps {
  id: string;
  show: boolean;
  changeDetail: IChangeDetail;

  onClose?: (reload?: boolean) => void;
}

type TFieldValidate = 'effectiveDate';
export const RescheduleChangeModal: React.FC<IProps> = ({
  id,
  show,
  changeDetail,

  onClose
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [effectiveDate, setEffectiveDate] = useState<Date>();
  const loading = useSelectLoadingOnUpdateChange();

  const {
    data: { minDate: minDateSetting, maxDate: maxDateSetting, invalidDate },
    loading: dateInfoLoading
  } = useSelectChangeDateInfo();

  const invalidDateTooltips = useMemo(
    () =>
      invalidDate?.reduceRight((pre, val) => {
        pre[formatTimeDefault(val.date, FormatTime.DateServer)] = val.reason;
        return pre;
      }, {} as Record<string, string>) || {},
    [invalidDate]
  );

  const disabledRange = useMemo(
    () =>
      invalidDate?.map(d => {
        const fromDate = new Date(
          formatTimeDefault(d.date, FormatTime.DateFrom)
        );
        const toDate = new Date(formatTimeDefault(d.date, FormatTime.DateTo));
        return { fromDate, toDate };
      }),
    [invalidDate]
  );

  useEffect(() => {
    if (!changeDetail?.changeId) return;

    const changeId = changeDetail.changeId;
    dispatch(actionsChange.getChangeDateInfo(changeId));
  }, [dispatch, changeDetail?.changeId]);

  useEffect(() => {
    if (!changeDetail?.effectiveDate) return;

    setEffectiveDate(
      convertLocalDateToUTCDate(new Date(changeDetail?.effectiveDate))
    );
  }, [changeDetail?.effectiveDate]);
  const minDate = useMemo(() => {
    return new Date(formatTimeDefault(minDateSetting!, FormatTime.DateServer));
  }, [minDateSetting]);
  const maxDate = useMemo(() => {
    return new Date(formatTimeDefault(maxDateSetting!, FormatTime.DateServer));
  }, [maxDateSetting]);

  const isEffectiveDateDisabled = useCallback(
    ({ date }: any) => {
      const dateTime = new Date(
        formatTimeDefault(date.toISOString(), FormatTime.DateServer)
      ).getTime();
      const maxDateTime = maxDate?.getTime();
      if (
        dateTime < minDate.getTime() ||
        (maxDateTime && dateTime > maxDateTime)
      ) {
        return true;
      }

      return !!invalidDateTooltips[
        formatTimeDefault(date.toISOString(), FormatTime.DateServer)
      ];
    },
    [invalidDateTooltips, maxDate, minDate]
  );

  const getDateTooltip = useCallback(
    (date: Date, view: Detail | undefined = 'month') => {
      if (view !== 'month') return;

      const tooltipContent =
        invalidDateTooltips[
          formatTimeDefault(date.toISOString(), FormatTime.DateServer)
        ];

      if (tooltipContent) return <Tooltip element={tooltipContent} />;
    },
    [invalidDateTooltips]
  );

  const currentErrors = useMemo(
    () =>
      ({
        effectiveDate:
          (!effectiveDate && {
            status: true,
            message: t('txt_required_validation', {
              fieldName: t('txt_effective_date')
            })
          }) ||
          (effectiveDate &&
            isEffectiveDateDisabled({ date: effectiveDate }) && {
              status: true,
              message: t('txt_validation_effective_date_invalid')
            })
      } as Record<TFieldValidate, IFormError>),
    [effectiveDate, isEffectiveDateDisabled, t]
  );

  const [, errors, setTouched] = useFormValidations<TFieldValidate>(
    currentErrors,
    0,
    'effectiveDate'
  );

  const isValid = useMemo(
    () => !Object.keys(errors).some(k => errors[k as TFieldValidate]),
    [errors]
  );

  const onSave = async () => {
    const result: any = await Promise.resolve(
      dispatch(
        actionsChange.rescheduleChange({
          changeId: changeDetail.changeId,
          effectiveDate: formatTimeDefault(
            addCurrentTime(effectiveDate!).toISOString(),
            FormatTime.DateServer
          )!
        })
      )
    );
    const isSuccess = actionsChange.rescheduleChange.fulfilled.match(result);
    if (isSuccess) {
      dispatch(
        actionsToast.addToast({
          type: 'success',
          message: t('txt_notification_change_reschedule_success')
        })
      );
      isFunction(onClose) && onClose(isSuccess);
    } else {
      dispatch(
        actionsToast.addToast({
          type: 'error',
          message: t('txt_notification_change_reschedule_fail')
        })
      );
    }
  };

  const handleCloseWithoutReload = () => {
    isFunction(onClose) && onClose();
  };

  return (
    <ModalRegistry
      id={id}
      show={show}
      onAutoClosedAll={handleCloseWithoutReload}
      classes={{ content: loading || dateInfoLoading ? 'loading' : '' }}
    >
      <ModalHeader border closeButton onHide={handleCloseWithoutReload}>
        <ModalTitle>{t('txt_reschedule_the_change')}</ModalTitle>
      </ModalHeader>
      <ModalBody>
        <EnhanceDatePicker
          id={`${id}__effectiveDate`}
          value={effectiveDate}
          minDate={minDate}
          maxDate={maxDate}
          disabledRange={disabledRange}
          dateTooltip={getDateTooltip}
          onChange={e => setEffectiveDate(e.target.value)}
          required
          label={t('txt_effective_date')}
          onFocus={setTouched('effectiveDate')}
          onBlur={setTouched('effectiveDate', true)}
          error={errors.effectiveDate}
        />
      </ModalBody>
      <ModalFooter
        cancelButtonText={t('txt_cancel')}
        okButtonText={t('txt_save')}
        onCancel={handleCloseWithoutReload}
        onOk={onSave}
        disabledOk={!isValid}
      />
    </ModalRegistry>
  );
};
