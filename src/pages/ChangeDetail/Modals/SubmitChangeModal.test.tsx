import { fireEvent } from '@testing-library/react';
import {
  mockUseDispatchFnc,
  mockUseModalRegistryFnc,
  renderComponent
} from 'app/utils';
import { actionsChange } from 'pages/_commons/redux/Change';
import * as ChangeSelector from 'pages/_commons/redux/Change/select-hooks';
import React from 'react';
import { SubmitChangeModal } from './SubmitChangeModal';

const mockUseSelectLoadingOnSubmitChange = (loading: boolean) => {
  jest
    .spyOn(ChangeSelector, 'useSelectLoadingOnSubmitChange')
    .mockReturnValue(loading);
};
jest.mock('app/_libraries/_dls', () => {
  const actualModules = jest.requireActual('app/_libraries/_dls');
  const translation = { t: (key: string) => key };
  return {
    ...actualModules,
    useTranslation: () => translation
  };
});

const mockUseModalRegistry = mockUseModalRegistryFnc();
const mockUseDispatch = mockUseDispatchFnc();

const actionsChangeMock: any = actionsChange;

const mockProps: any = {
  id: 'id01',
  changeId: 'changeId01',
  changeName: 'changeName',
  show: true,
  onClose: jest.fn()
};

const testId = 'submitChange';

const then_click_submit_button = () => {
  const rejectButton = document.body.querySelector('.btn-primary');
  fireEvent.click(rejectButton!);
};

describe('Submit change ', () => {
  const createWrapper = async (...props: any[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    await renderComponent(
      testId,
      <SubmitChangeModal {...(mergeProps as any)} />
    );
  };

  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => (() => {}) as any);
    mockUseModalRegistry.mockImplementation(() => [true, false]);
    mockUseSelectLoadingOnSubmitChange(false);
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
    mockUseModalRegistry.mockClear();
  });

  it('should submit change success', async () => {
    mockUseSelectLoadingOnSubmitChange(true);
    actionsChangeMock.submitChange = () => {
      actionsChangeMock.submitChange = {
        fulfilled: {
          match: () => true
        }
      };
    };
    await createWrapper(mockProps);

    then_click_submit_button();
    expect(mockUseDispatch).toHaveBeenCalled();
  });

  it('should submit change failed', async () => {
    actionsChangeMock.submitChange = () => {
      actionsChangeMock.submitChange = {
        fulfilled: {
          match: () => false
        }
      };
    };
    await createWrapper(mockProps);

    then_click_submit_button();
    expect(mockUseDispatch).toHaveBeenCalled();
  });

  it('should hide modal', async () => {
    await createWrapper(mockProps);
    const cancelButton = document.body.querySelector('.btn-secondary');
    fireEvent.click(cancelButton!);
    expect(mockProps.onClose).toHaveBeenCalled();
  });
});
