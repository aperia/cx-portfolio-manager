import EnhanceComboBox from 'app/components/EnhanceComboBox';
import EnhanceDatePicker from 'app/components/EnhanceDatePicker';
import ModalRegistry from 'app/components/ModalRegistry';
import TextAreaCountDown from 'app/components/TextAreaCountDown';
import { FormatTime } from 'app/constants/enums';
import {
  addCurrentTime,
  convertLocalDateToUTCDate,
  formatTimeDefault
} from 'app/helpers';
import { useFormValidations } from 'app/hooks';
import {
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  TextBox,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import isFunction from 'lodash.isfunction';
import {
  actionsChange,
  useSelectChangeDateInfo,
  useSelectChangeOwners,
  useSelectLoadingOnUpdateChange
} from 'pages/_commons/redux/Change';
import { actionsToast } from 'pages/_commons/ToastNotifications/redux';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { Detail } from 'react-calendar';
import { useDispatch } from 'react-redux';

interface IProps {
  id: string;
  show: boolean;
  changeDetail: IChangeDetail;

  onClose?: (reload?: boolean) => void;
}
export const EditChangeModal: React.FC<IProps> = ({
  id,
  show,
  changeDetail: changeProp,
  onClose
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const [changeDetail, setChangeDetail] = useState({ ...changeProp });
  const [changeOwner, setChangeOwner] = useState<RefData>();
  const [effectiveDate, setEffectiveDate] = useState<Date>();

  const { data: changeOwners, loading: changeOwnersLoading } =
    useSelectChangeOwners();
  const {
    data: { minDate: minDateSetting, maxDate: maxDateSetting, invalidDate },
    loading: dateInfoLoading
  } = useSelectChangeDateInfo();
  const updateChangeLoading = useSelectLoadingOnUpdateChange();

  const invalidDateTooltips = useMemo(
    () =>
      invalidDate?.reduceRight((pre, val) => {
        pre[formatTimeDefault(val.date, FormatTime.DateServer)] = val.reason;
        return pre;
      }, {} as Record<string, string>) || {},
    [invalidDate]
  );

  const disabledRange = useMemo(
    () =>
      invalidDate?.map(d => {
        const fromDate = new Date(
          formatTimeDefault(d.date, FormatTime.DateFrom)
        );
        const toDate = new Date(formatTimeDefault(d.date, FormatTime.DateTo));
        return { fromDate, toDate };
      }),
    [invalidDate]
  );

  const minDate = useMemo(() => {
    return new Date(formatTimeDefault(minDateSetting!, FormatTime.DateServer));
  }, [minDateSetting]);
  const maxDate = useMemo(() => {
    return new Date(formatTimeDefault(maxDateSetting!, FormatTime.DateServer));
  }, [maxDateSetting]);

  useEffect(() => {
    if (!changeProp?.changeId) return;

    const changeId = changeProp.changeId;
    dispatch(actionsChange.getChangeOwners(changeId));
    dispatch(actionsChange.getChangeDateInfo(changeId));
  }, [dispatch, changeProp?.changeId]);

  useEffect(() => {
    if (changeOwners.length <= 0) return;

    setChangeOwner(
      changeOwners.find(
        i =>
          i.code === changeDetail.changeOwner ||
          i.text === changeDetail.changeOwner
      )
    );
  }, [changeOwners, changeDetail.changeOwner]);

  useEffect(() => {
    if (!changeDetail?.effectiveDate) return;

    setEffectiveDate(
      convertLocalDateToUTCDate(new Date(changeDetail?.effectiveDate))
    );
  }, [changeDetail?.effectiveDate]);

  const isEffectiveDateDisabled = useCallback(
    ({ date }: any) => {
      const dateTime = new Date(
        formatTimeDefault(date.toISOString(), FormatTime.DateServer)
      ).getTime();
      const maxDateTime = maxDate?.getTime();
      if (
        dateTime < minDate.getTime() ||
        (maxDateTime && dateTime > maxDateTime)
      ) {
        return true;
      }

      return !!invalidDateTooltips[
        formatTimeDefault(date.toISOString(), FormatTime.DateServer)
      ];
    },
    [invalidDateTooltips, maxDate, minDate]
  );

  const getDateTooltip = useCallback(
    (date: Date, view: Detail | undefined = 'month') => {
      if (view !== 'month') return;

      const tooltipContent =
        invalidDateTooltips[
          formatTimeDefault(date.toISOString(), FormatTime.DateServer)
        ];

      if (tooltipContent) return <Tooltip element={tooltipContent} />;
    },
    [invalidDateTooltips]
  );

  const currentErrors = useMemo(
    () =>
      ({
        changeName: !changeDetail?.changeName?.trim() && {
          status: true,
          message: t('txt_required_validation', {
            fieldName: t('txt_name_of_change')
          })
        },
        changeOwner: !changeOwner?.code && {
          status: true,
          message: t('txt_required_validation', {
            fieldName: t('txt_change_owner')
          })
        },
        effectiveDate:
          (!effectiveDate && {
            status: true,
            message: t('txt_required_validation', {
              fieldName: t('txt_effective_date')
            })
          }) ||
          (effectiveDate &&
            isEffectiveDateDisabled({ date: effectiveDate }) && {
              status: true,
              message: t('txt_validation_effective_date_invalid')
            }),
        changeDescription: !changeDetail?.changeDescription?.trim() && {
          status: true,
          message: t('txt_required_validation', {
            fieldName: t('txt_change_description')
          })
        }
      } as Record<keyof IChangeDetail, IFormError>),
    [changeDetail, changeOwner, effectiveDate, isEffectiveDateDisabled, t]
  );

  const [, errors, setTouched] = useFormValidations<keyof IChangeDetail>(
    currentErrors,
    0,
    'effectiveDate'
  );

  const isValid = useMemo(
    () => !Object.keys(errors).some(k => errors[k as keyof IChangeDetail]),
    [errors]
  );

  const onSave = async () => {
    const result: any = await Promise.resolve(
      dispatch(
        actionsChange.updateChangeDetail({
          changeId: changeDetail.changeId,
          changeName: changeDetail.changeName.trim(),
          changeOwnerId: changeOwner!.code!,
          effectiveDate: formatTimeDefault(
            addCurrentTime(effectiveDate!).toISOString(),
            FormatTime.DateServer
          )!,
          description: changeDetail.changeDescription!.trim()
        })
      )
    );
    const isSuccess = actionsChange.updateChangeDetail.fulfilled.match(result);
    if (isSuccess) {
      dispatch(
        actionsToast.addToast({
          type: 'success',
          message: t('txt_notification_change_edited_success')
        })
      );
      isFunction(onClose) && onClose(isSuccess);
    } else {
      dispatch(
        actionsToast.addToast({
          type: 'error',
          message: t('txt_notification_change_edited_fail')
        })
      );
    }
  };

  const handleCloseWithoutReload = () => {
    isFunction(onClose) && onClose();
  };

  const handleChangeDetailEditation =
    (field: keyof IChangeDetail) => (e: any) => {
      const value = e.target?.value;
      setChangeDetail(change => {
        return { ...change, [field]: value };
      });
    };

  return (
    <ModalRegistry
      id={id}
      show={show}
      onAutoClosedAll={handleCloseWithoutReload}
      classes={{
        content:
          updateChangeLoading || changeOwnersLoading || dateInfoLoading
            ? 'loading'
            : ''
      }}
    >
      <ModalHeader border closeButton onHide={handleCloseWithoutReload}>
        <ModalTitle>{t('txt_edit_change')}</ModalTitle>
      </ModalHeader>
      <ModalBody>
        <div className="mb-16">
          <TextBox
            id={`${id}__changeName`}
            value={changeDetail?.changeName}
            onChange={handleChangeDetailEditation('changeName')}
            required
            maxLength={50}
            label={t('txt_change_name')}
            onFocus={setTouched('changeName')}
            onBlur={setTouched('changeName', true)}
            error={errors.changeName}
          />
        </div>
        <div className="mb-16">
          <EnhanceComboBox
            id={`${id}__changeOwner`}
            data={changeOwners}
            value={changeOwner}
            onChange={e => setChangeOwner(e.target.value)}
            required
            label={t('txt_change_owner')}
            onFocus={setTouched('changeOwner')}
            onBlur={setTouched('changeOwner', true)}
            textField="text"
            error={errors.changeOwner}
            noResult={t('txt_no_results_found')}
            loadingMoreText={t('txt_loading_more_change_owners')}
          />
        </div>
        <div className="mb-16">
          <EnhanceDatePicker
            id={`${id}__effectiveDate`}
            value={effectiveDate}
            minDate={minDate}
            maxDate={maxDate}
            disabledRange={disabledRange}
            dateTooltip={getDateTooltip}
            onChange={e => setEffectiveDate(e.target.value)}
            required
            label={t('txt_effective_date')}
            onFocus={setTouched('effectiveDate')}
            onBlur={setTouched('effectiveDate', true)}
            error={errors.effectiveDate}
          />
        </div>
        <div>
          <div className="mb-8 text-uppercase color-grey fw-500 fs-11">
            {t('txt_change_description')} <span className="color-red">*</span>
          </div>
          <TextAreaCountDown
            id={`${id}__changeDescription`}
            value={changeDetail?.changeDescription}
            onChange={handleChangeDetailEditation('changeDescription')}
            required
            rows={4}
            maxLength={255}
            onFocus={setTouched('changeDescription')}
            onBlur={setTouched('changeDescription', true)}
            error={errors.changeDescription}
          />
        </div>
      </ModalBody>
      <ModalFooter
        cancelButtonText={t('txt_cancel')}
        okButtonText={t('txt_save')}
        onCancel={handleCloseWithoutReload}
        onOk={onSave}
        disabledOk={!isValid}
      />
    </ModalRegistry>
  );
};
