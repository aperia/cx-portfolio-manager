import { fireEvent, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import {
  mockUseDispatchFnc,
  mockUseModalRegistryFnc,
  renderComponent
} from 'app/utils';
import { mockActionCreator } from 'app/utils/mockActionCreator';
import 'app/utils/_mockComponent/mockComboBox';
import {
  ComboBoxProps,
  DatePickerChangeEvent,
  DatePickerProps,
  DropdownBaseChangeEvent
} from 'app/_libraries/_dls';
import { actionsChange, ChangeState } from 'pages/_commons/redux/Change';
import { actionsToast } from 'pages/_commons/ToastNotifications/redux';
import React from 'react';
import * as reactRedux from 'react-redux';
import { EditChangeModal } from './EditChangeModal';

jest.mock('app/_libraries/_dls', () => {
  const actualModules = jest.requireActual('app/_libraries/_dls');
  const translation = { t: (key: string) => key };
  return {
    ...actualModules,
    useTranslation: () => translation
  };
});

jest.mock('app/_libraries/_dls/components/DatePicker', () => {
  return {
    __esModule: true,
    default: ({ onChange, dateTooltip }: DatePickerProps) => (
      <>
        <input
          data-testid="DatePicker_onChange"
          onChange={(e: any) => {
            onChange!({
              target: { value: e.target.date, name: e.target.name }
            } as DatePickerChangeEvent);
            dateTooltip!(e.target.date as Date, e.target.view);
          }}
        />
      </>
    )
  };
});

jest.mock('app/_libraries/_dls/components/ComboBox', () => {
  const actualModule = jest.requireActual(
    'app/_libraries/_dls/components/ComboBox'
  );
  return {
    ...actualModule,
    __esModule: true,
    default: ({ onChange }: ComboBoxProps) => (
      <>
        <input
          data-testid="ComboBox.Input_onChange"
          onChange={(e: any) =>
            onChange!({
              target: { value: e.target.value }
            } as DropdownBaseChangeEvent)
          }
        />
      </>
    )
  };
});

const mockUseModalRegistry = mockUseModalRegistryFnc();
const mockUseDispatch = mockUseDispatchFnc();

const actionsChangeMock: any = actionsChange;
let selectorMock: jest.SpyInstance;
const mockSelectorChanges = (changeState: ChangeState) => {
  selectorMock = jest
    .spyOn(reactRedux, 'useSelector')
    .mockImplementation(selector => selector({ change: changeState }));
};
const defaultState = {
  changeOwners: {
    data: [{ code: '001', text: 'John cena' }],
    loading: false
  },
  changeDateInfo: {
    data: {
      minDate: '2021-02-02',
      maxDate: '2021-04-05',
      invalidDate: [
        { date: '2021-03-04', reason: 'reason' },
        { date: '2021-04-04', reason: 'reason' }
      ]
    },
    loading: false
  }
} as any;
const mockProps: any = {
  id: 'id01',
  changeDetail: {
    changeId: 'changeId01',
    changeOwner: '001'
  },
  show: true,
  onClose: jest.fn()
};

const testId = 'demoteChange';
const then_update_textfield = (value: string) => {
  const inputElement = document.body.querySelector(
    `.dls-input-container > input`
  );

  fireEvent.change(inputElement!, { target: { value: value } });
  fireEvent.blur(inputElement!);
};
const then_update_textarea = (value: string) => {
  const inputElement = document.body.querySelector(
    `.text-field-container > textarea`
  );

  fireEvent.change(inputElement!, { target: { value: value } });
  fireEvent.blur(inputElement!);
};

const then_change_date_picker = (value: string, view: string | undefined) => {
  const inputElement = screen.getByTestId('DatePicker_onChange');
  fireEvent.change(inputElement, {
    target: {
      value: 'undefined',
      name: 'dateValue',
      date: new Date(value),
      view: view
    }
  });
  fireEvent.blur(inputElement!);
};

const then_change_combobox = (value: RefData) => {
  const inputElement = screen.getByTestId('ComboBox.Input_onChange');

  fireEvent.change(inputElement, {
    target: {
      value: value
    }
  });
};

const then_click_save_button = () => {
  const rejectButton = document.body.querySelector('.btn-primary');
  userEvent.click(rejectButton!);
};

describe('Edit Change ', () => {
  const createWrapper = async (...props: any[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    return renderComponent(testId, <EditChangeModal {...mergeProps} />);
  };

  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => (() => {}) as any);
    mockUseModalRegistry.mockImplementation(() => [true, false]);
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
    mockUseModalRegistry.mockClear();
    selectorMock.mockClear();
  });

  it('should Edit Change success', async () => {
    const toastSpy = mockActionCreator(actionsToast);
    const addToastAction = toastSpy('addToast');
    actionsChangeMock.updateChangeDetail = () => {
      actionsChangeMock.updateChangeDetail = {
        fulfilled: {
          match: () => true
        }
      };
    };

    mockSelectorChanges(defaultState);
    await createWrapper(mockProps);

    then_change_date_picker('2021/02/03', 'month');
    then_update_textfield('test');
    then_update_textarea('abc test');

    then_click_save_button();
    await new Promise(resolve => setTimeout(() => resolve(0)));
    expect(addToastAction).toBeCalledWith({
      type: 'success',
      message: 'txt_notification_change_edited_success'
    });
  });

  it('should Edit Change failed', async () => {
    const toastSpy = mockActionCreator(actionsToast);
    const addToastAction = toastSpy('addToast');
    actionsChangeMock.updateChangeDetail = () => {
      actionsChangeMock.updateChangeDetail = {
        fulfilled: {
          match: () => false
        }
      };
    };

    mockSelectorChanges(defaultState);
    await createWrapper({
      ...mockProps,
      changeDetail: {
        ...mockProps.changeDetail,
        effectiveDate: '2021/02/02',
        changeOwner: 'John cena'
      }
    });

    then_change_date_picker('2021/02/03', 'month');
    then_update_textfield('test');
    then_update_textarea('abc test');

    then_click_save_button();
    await new Promise(resolve => setTimeout(() => resolve(0)));
    expect(addToastAction).toBeCalledWith({
      type: 'error',
      message: 'txt_notification_change_edited_fail'
    });
  });

  it('should show error when changeId and changeOwners is empty', async () => {
    const toastSpy = mockActionCreator(actionsToast);
    const addToastAction = toastSpy('addToast');
    actionsChangeMock.updateChangeDetail = () => {
      actionsChangeMock.updateChangeDetail = {
        fulfilled: {
          match: () => false
        }
      };
    };

    mockSelectorChanges({
      ...defaultState,
      changeOwners: { data: [], loading: true }
    });
    await createWrapper({
      ...mockProps,
      changeDetail: {}
    });

    then_change_date_picker('2021/03/02', '');
    then_update_textfield('test');
    then_change_combobox({ code: '001', text: 'John cena' });
    then_update_textarea('abc test');

    then_click_save_button();
    await new Promise(resolve => setTimeout(() => resolve(0)));
    expect(addToastAction).toBeCalledWith({
      type: 'error',
      message: 'txt_notification_change_edited_fail'
    });
  });

  it('should show error dateTooltip', async () => {
    const toastSpy = mockActionCreator(actionsToast);
    const addToastAction = toastSpy('addToast');
    mockSelectorChanges(defaultState);
    await createWrapper(mockProps);

    then_change_date_picker('2021-04-04', 'month');
    then_update_textfield('test');
    then_update_textarea('abc test');
    then_change_combobox({ code: '001', text: 'John cena' });
    then_update_textarea('abc test');

    then_click_save_button();
    expect(addToastAction).not.toBeCalled();
  });

  it('should show error dateTooltip is undefined', async () => {
    const toastSpy = mockActionCreator(actionsToast);
    const addToastAction = toastSpy('addToast');
    mockSelectorChanges({
      ...defaultState,
      changeDateInfo: {
        ...defaultState.changeDateInfo,
        data: { invalidDate: undefined }
      }
    });
    await createWrapper(mockProps);

    then_change_date_picker('2021-04-04', undefined);
    then_update_textfield('test');
    then_update_textarea('abc test');
    then_change_combobox({ code: '001', text: 'John cena' });
    then_update_textarea('abc test');

    then_click_save_button();
    expect(addToastAction).not.toBeCalled();
  });

  it('should not show error dateTooltip when date picker view is year', async () => {
    const toastSpy = mockActionCreator(actionsToast);
    const addToastAction = toastSpy('addToast');

    mockSelectorChanges(defaultState);

    await createWrapper(mockProps);

    then_change_date_picker('2021-04-04', 'year');
    then_update_textfield('test');
    then_update_textarea('abc test');
    then_change_combobox({ code: '001', text: 'John cena' });
    then_update_textarea(
      '123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 1234'
    );

    then_click_save_button();

    expect(screen.getByText('1 txt_character_left')).toBeInTheDocument();
    expect(addToastAction).not.toBeCalled();
  });

  it('should hide modal', async () => {
    mockSelectorChanges(defaultState);
    await createWrapper(mockProps);

    const cancelButton = document.body.querySelector('.btn-secondary');
    fireEvent.click(cancelButton!);
    expect(mockProps.onClose).toHaveBeenCalled();
  });
});
