import EnhanceDatePicker from 'app/components/EnhanceDatePicker';
import EnhanceMultiSelect from 'app/components/EnhanceMultiSelect';
import ModalRegistry from 'app/components/ModalRegistry';
import TextAreaCountDown from 'app/components/TextAreaCountDown';
import {
  compareTime,
  getCronExpression,
  getCurrentTimePickerValue,
  isDevice,
  isToday
} from 'app/helpers';
import { useFormValidations } from 'app/hooks';
import {
  Button,
  ComboBox,
  DatePickerChangeEvent,
  DropdownList,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import TimePicker, {
  TimePickerValue
} from 'app/_libraries/_dls/components/TimePicker';
import { TimePickerChangeEvent } from 'app/_libraries/_dls/components/TimePicker/types';
import isEmpty from 'lodash.isempty';
import isFunction from 'lodash.isfunction';
import { useSelectCurrentUser } from 'pages/_commons/Header/redux/selector';
import {
  actionsChange,
  useSelectChangeOwners
} from 'pages/_commons/redux/Change';
import {
  actionsWorkflow,
  useSelectAllWorkflowRefDataSelector
} from 'pages/_commons/redux/Workflow';
import { actionsToast } from 'pages/_commons/ToastNotifications/redux';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

interface IProps {
  id: string;
  show: boolean;
  onClose?: () => void;
}
const recurrences = [
  { label: 'No recurrence', value: 'noRecurrence' },
  { label: 'Every weekday (Mon - Fri)', value: 'weekDay' },
  { label: 'Daily', value: 'daily' },
  { label: 'Weekly', value: 'weekly' },
  { label: 'Monthly', value: 'monthly' },
  { label: 'Yearly', value: 'yearly' }
];
const recurrenceItems = recurrences.map(r => (
  <DropdownList.Item key={r.value} label={r.label} value={r} />
));
export const SetReminderModal: React.FC<IProps> = ({ id, show, onClose }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const [workflow, setWorkflow] = useState<RefData | undefined>(undefined);
  const [reminderDate, setReminderDate] = useState<Date | undefined>(undefined);
  const [reminderTime, setReminderTime] =
    useState<TimePickerValue | undefined>(undefined);
  const [recurrence, setRecurrence] = useState(recurrences[0]);
  const [reminderRecipients, setReminderRecipients] = useState<RefData[]>([]);
  const [description, setDescription] = useState<string>('');

  const allWorkflowRefData = useSelectAllWorkflowRefDataSelector();

  const workflowItems = useMemo(
    () =>
      allWorkflowRefData.map(w => (
        <ComboBox.Item key={w.code} label={w.text} value={w} />
      )),
    [allWorkflowRefData]
  );

  const { data: recipients } = useSelectChangeOwners();

  const { essUserName, userId } = useSelectCurrentUser();
  const hasMe = useMemo(() => {
    const _userId = isNaN(Number.parseInt(userId)) ? '1' : userId;
    return reminderRecipients.findIndex(r => r.code === _userId) !== -1;
  }, [reminderRecipients, userId]);

  const handleCloseWithoutReload = () => {
    isFunction(onClose) && onClose();
  };

  const currentErrors = useMemo(
    () =>
      ({
        workflow: !workflow && {
          status: true,
          message: t('Select a workflow.')
        },
        reminderDate: !reminderDate && {
          status: true,
          message: t('Select a reminder date.')
        },
        reminderTime: (!reminderTime ||
          ['hour', 'minute', 'meridiem'].some(
            key => reminderTime && !reminderTime[key as keyof TimePickerValue]
          )) && {
          status: true,
          message: t('Select reminder time.')
        },
        reminderRecipients: isEmpty(reminderRecipients) && {
          status: true,
          message: t('Select at least one recipient.')
        },
        description: !description.trim() && {
          status: true,
          message: t('Enter a description.')
        }
      } as Record<any, IFormError>),
    [t, workflow, reminderDate, reminderTime, description, reminderRecipients]
  );

  const handleSetReminderDate = useCallback(
    (e: DatePickerChangeEvent) => {
      const date = e.target.value;
      setReminderDate(date);
      if (date && isToday(date.toDateString()) && !isEmpty(reminderTime)) {
        const currentTime = getCurrentTimePickerValue();
        const isSmaller = compareTime(reminderTime, currentTime);
        isSmaller === -1 && setReminderTime(currentTime);
      }
    },
    [reminderTime]
  );

  const handleSetReminderTime = (e: TimePickerChangeEvent) => {
    if (reminderDate && isToday(reminderDate.toDateString())) {
      const currentTime = getCurrentTimePickerValue();
      const isSmaller = compareTime(e.target.value, currentTime);
      isSmaller === -1
        ? setReminderTime(currentTime)
        : setReminderTime(e.target.value);
    } else {
      setReminderTime(e.target.value);
    }
  };

  const [touches, errors, setTouched] = useFormValidations<any>(
    currentErrors,
    0,
    'recurrence'
  );

  const handleNotifyMe = useCallback(() => {
    setTouched('reminderRecipients', true)();
    // API receive number, but currently userId is NOT a string
    const _userId = isNaN(Number.parseInt(userId)) ? '1' : userId;
    setReminderRecipients([
      ...reminderRecipients,
      { code: _userId, text: essUserName }
    ]);
  }, [essUserName, userId, reminderRecipients, setTouched]);

  const isTouchAll = useMemo(() => {
    const props = [
      'workflow',
      'reminderDate',
      'reminderTime',
      'reminderRecipients',
      'description'
    ];
    return props.every(p => touches[p]);
  }, [touches]);

  const isValid = useMemo(
    () => !Object.keys(errors).some(k => errors[k as any]),
    [errors]
  );

  const handleSubmit = async () => {
    const cronStartDate = reminderDate;
    const hours =
      reminderTime?.meridiem === 'AM'
        ? parseInt(reminderTime.hour!)
        : parseInt(reminderTime!.hour!) + 12;
    cronStartDate?.setHours(hours);
    cronStartDate?.setMinutes(parseInt(reminderTime!.minute!));

    const cronExpression = getCronExpression(cronStartDate, recurrence.value);

    const result: any = await Promise.resolve(
      dispatch(
        actionsWorkflow.setReminderWorkflow({
          workflowTemplateId: workflow!.code,
          cronStartDate: cronStartDate,
          cronExpression: cronExpression,
          type: 'WORKFLOW',
          recipients: reminderRecipients.map(r => r.code),
          description: description.trim()
        })
      )
    );
    const isSuccess =
      actionsWorkflow.setReminderWorkflow.fulfilled.match(result);
    if (isSuccess) {
      dispatch(
        actionsToast.addToast({
          type: 'success',
          message: t('Reminder set.')
        })
      );
      isFunction(onClose) && onClose();
    } else {
      dispatch(
        actionsToast.addToast({
          type: 'error',
          message: t('Reminder failed to set.')
        })
      );
    }
  };

  const submitting = useSelector<RootState, boolean | undefined>(
    state => state.workflow?.reminderSet?.loading
  );

  useEffect(() => {
    dispatch(actionsChange.getChangeOwners(''));
  }, [dispatch]);

  return (
    <ModalRegistry
      id={id}
      show={show}
      onAutoClosedAll={handleCloseWithoutReload}
      classes={{ content: submitting ? 'loading' : '' }}
    >
      <ModalHeader border closeButton onHide={handleCloseWithoutReload}>
        <ModalTitle>Set Reminder</ModalTitle>
      </ModalHeader>
      <ModalBody>
        <div className="mb-16">
          <ComboBox
            id={`${id}__workflow`}
            label={t('Workflow')}
            required
            textField="text"
            noResult={t('txt_no_results_found')}
            value={workflow}
            onChange={e => setWorkflow(e.target.value)}
            onFocus={setTouched('workflow')}
            onBlur={setTouched('workflow', true)}
            error={errors.workflow}
          >
            {workflowItems}
          </ComboBox>
        </div>
        <div className="mb-16">
          <EnhanceDatePicker
            id={`${id}__reminderDate`}
            label={t('Reminder Date')}
            required
            value={reminderDate}
            minDate={new Date()}
            onChange={handleSetReminderDate}
            onFocus={setTouched('reminderDate')}
            onBlur={setTouched('reminderDate', true)}
            error={errors.reminderDate}
          />
        </div>
        <div className="mb-16">
          <TimePicker
            id={`${id}__reminderTime`}
            label={t('Reminder Time')}
            required
            value={reminderTime}
            onChange={handleSetReminderTime}
            onFocus={setTouched('reminderTime')}
            onBlur={setTouched('reminderTime', true)}
            error={errors.reminderTime}
            canInput={!isDevice}
          />
        </div>
        <div className="mb-16">
          <DropdownList
            id={`${id}__recurrence`}
            textField="label"
            label="Set Recurrence"
            required
            value={recurrence}
            onChange={e => setRecurrence(e.target.value)}
          >
            {recurrenceItems}
          </DropdownList>
        </div>
        <div className="mb-8">
          <EnhanceMultiSelect
            id={`${id}__recipients`}
            label={t('Recipients')}
            placeholder={t('txt_type_at_least_2_characters')}
            textField="text"
            value={reminderRecipients}
            required
            data={recipients}
            onChange={e => {
              setReminderRecipients(e.target.value);
            }}
            noResult={t('txt_no_results_found')}
            onFocus={setTouched('reminderRecipients')}
            onBlur={setTouched('reminderRecipients', true)}
            error={errors.reminderRecipients}
          />
        </div>
        <div className="mb-16 ml-n8">
          <Button
            variant="outline-primary"
            size="sm"
            onClick={handleNotifyMe}
            disabled={hasMe}
          >
            Notify Me
          </Button>
        </div>
        <div>
          <div className="mb-12 text-uppercase color-grey fw-500 fs-11">
            {t('Description')} <span className="color-red">*</span>
          </div>
          <TextAreaCountDown
            id={`${id}__reminderDescription`}
            rows={4}
            maxLength={255}
            placeholder={t('txt_add_description')}
            value={description}
            onChange={e => setDescription(e.target.value)}
            onFocus={setTouched('description')}
            onBlur={setTouched('description', true)}
            error={errors.description}
          />
        </div>
      </ModalBody>
      <ModalFooter
        cancelButtonText={t('txt_cancel')}
        okButtonText={t('txt_submit')}
        onCancel={handleCloseWithoutReload}
        onOk={handleSubmit}
        disabledOk={!isTouchAll || !isValid}
      />
    </ModalRegistry>
  );
};
