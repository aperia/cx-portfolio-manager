import { fireEvent } from '@testing-library/react';
import {
  mockUseDispatchFnc,
  mockUseModalRegistryFnc,
  renderComponent
} from 'app/utils';
import { actionsChange } from 'pages/_commons/redux/Change';
import * as ChangeSelector from 'pages/_commons/redux/Change/select-hooks';
import React from 'react';
import { RejectChangeModal } from './RejectChangeModal';

jest.mock('app/_libraries/_dls', () => {
  const actualModules = jest.requireActual('app/_libraries/_dls');
  const translation = { t: (key: string) => key };
  return {
    ...actualModules,
    useTranslation: () => translation
  };
});
const mockUseSelectLoadingOnRejectChange = (loading: boolean) => {
  jest
    .spyOn(ChangeSelector, 'useSelectLoadingOnRejectChange')
    .mockReturnValue(loading);
};
const mockUseModalRegistry = mockUseModalRegistryFnc();
const mockUseDispatch = mockUseDispatchFnc();

const actionsChangeMock: any = actionsChange;

const mockProps: any = {
  id: 'id01',
  changeId: 'changeId01',
  show: true,
  onClose: jest.fn()
};

const testId = 'rejectChange';
const then_update_textarea = (value: string) => {
  const inputElement = document.body.querySelector(
    `.text-field-container > textarea`
  );

  fireEvent.change(inputElement!, { target: { value: value } });
  fireEvent.blur(inputElement!);
};
const then_click_reject_button = () => {
  const rejectButton = document.body.querySelector('.btn-danger');
  fireEvent.click(rejectButton!);
};

describe('Reject Change ', () => {
  const mockDispatch = jest.fn();
  const createWrapper = async (...props: any[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    await renderComponent(testId, <RejectChangeModal {...mergeProps} />);
  };

  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);

    mockUseModalRegistry.mockImplementation(() => [true, false]);
    mockUseSelectLoadingOnRejectChange(false);
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
    mockUseModalRegistry.mockClear();
  });

  it('should reject change not success when verify failed ', async () => {
    const mockDispatch = jest.fn().mockImplementation(() => ({
      payload: {}
    }));
    mockUseDispatch.mockImplementation(() => mockDispatch);
    mockUseSelectLoadingOnRejectChange(true);

    actionsChangeMock.getChangeDetail = () => {
      actionsChangeMock.getChangeDetail = {
        fulfilled: {
          match: () => false
        }
      };
    };
    await createWrapper(mockProps);
    then_update_textarea('abc test');
    then_click_reject_button();

    await new Promise(resolve => setTimeout(() => resolve(0)));
    expect(mockDispatch).toHaveBeenCalledTimes(3);
  });

  it('should reject change not success when verify not pass ', async () => {
    const mockDispatch = jest.fn().mockImplementation(() => ({
      payload: {
        data: {
          changeStatus: {},
          approvalRecords: [
            {
              action: 'reject',
              actionTime: '2021-05-04T10:27:25.0193714+07:00'
            }
          ]
        }
      }
    }));
    mockUseDispatch.mockImplementation(() => mockDispatch);
    mockUseSelectLoadingOnRejectChange(true);

    actionsChangeMock.getChangeDetail = () => {
      actionsChangeMock.getChangeDetail = {
        fulfilled: {
          match: () => true
        }
      };
    };
    await createWrapper(mockProps);
    then_update_textarea(
      '123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 1234'
    );
    then_click_reject_button();
    await new Promise(resolve => setTimeout(() => resolve(0)));
    expect(mockDispatch).toHaveBeenCalledTimes(2);
  });

  it('should reject change success', async () => {
    const mockDispatch = jest.fn().mockImplementation(() => ({
      payload: {
        data: {
          changeStatus: 'pendingApproval',
          approvalRecords: [
            {
              action: 'pending',
              actionTime: '2021-05-04T10:27:25.0193714+07:00'
            }
          ]
        }
      }
    }));
    mockUseDispatch.mockImplementation(() => mockDispatch);

    actionsChangeMock.getChangeDetail = () => {
      actionsChangeMock.getChangeDetail = {
        fulfilled: {
          match: () => true
        }
      };
    };
    actionsChangeMock.rejectChange = () => {
      actionsChangeMock.rejectChange = {
        fulfilled: {
          match: () => true
        }
      };
    };
    await createWrapper(mockProps);

    then_update_textarea('abc test');
    then_click_reject_button();

    await new Promise(resolve => setTimeout(() => resolve(0)));
    expect(mockDispatch).toHaveBeenCalledTimes(3);
  });

  it('should reject change failed', async () => {
    const mockDispatch = jest.fn().mockImplementation(() => ({
      payload: {
        data: {
          changeStatus: 'pendingApproval',
          approvalRecords: [
            {
              action: 'pending',
              actionTime: '2021-05-04T10:27:25.0193714+07:00'
            }
          ]
        }
      }
    }));
    mockUseDispatch.mockImplementation(() => mockDispatch);

    actionsChangeMock.getChangeDetail = () => {
      actionsChangeMock.getChangeDetail = {
        fulfilled: {
          match: () => true
        }
      };
    };
    actionsChangeMock.rejectChange = () => {
      actionsChangeMock.rejectChange = {
        fulfilled: {
          match: () => false
        }
      };
    };
    await createWrapper(mockProps);

    then_update_textarea('abc test');
    then_click_reject_button();
    await new Promise(resolve => setTimeout(() => resolve(0)));
    expect(mockDispatch).toHaveBeenCalledTimes(3);
  });

  it('should reject change not success when verify not success ', async () => {
    const mockDispatch = jest.fn().mockImplementation(() => ({
      payload: {}
    }));
    mockUseDispatch.mockImplementation(() => mockDispatch);
    mockUseSelectLoadingOnRejectChange(true);

    actionsChangeMock.getChangeDetail = () => {
      actionsChangeMock.getChangeDetail = {
        fulfilled: {
          match: () => true
        }
      };
    };
    await createWrapper(mockProps);
    then_update_textarea('abc test');
    then_click_reject_button();

    await new Promise(resolve => setTimeout(() => resolve(0)));
    expect(mockDispatch).toHaveBeenCalledTimes(1);
  });

  it('should hide modal', async () => {
    await createWrapper(mockProps);
    const cancelButton = document.body.querySelector('.btn-secondary');
    fireEvent.click(cancelButton!);
    expect(mockProps.onClose).toHaveBeenCalled();
  });
});
