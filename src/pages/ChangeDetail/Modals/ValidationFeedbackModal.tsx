import ModalRegistry from 'app/components/ModalRegistry';
import { camelToText, classnames, isDevice } from 'app/helpers';
import {
  Button,
  ColumnType,
  Grid,
  Icon,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import isFunction from 'lodash.isfunction';
import orderBy from 'lodash.orderby';
import React, { useCallback, useMemo, useRef } from 'react';

interface IProps {
  id: string;
  changeId: string;
  show: boolean;
  messages: IValidationMessage[];

  onClose?: () => void;
  onEditChange?: (changeId: string) => void;
  onEditWorkflow?: (workflow: IWorkflowModel) => void;
}
export const ValidationFeedbackModal: React.FC<IProps> = ({
  id,
  changeId,
  show,
  messages,

  onClose,
  onEditChange,
  onEditWorkflow
}) => {
  const { t } = useTranslation();

  const keepRef = useRef<{
    handleEditChange: typeof onEditChange;
    handleEditWorkflow: typeof onEditWorkflow;
  }>({
    handleEditChange: onEditChange,
    handleEditWorkflow: onEditWorkflow
  } as any);
  keepRef.current.handleEditChange = onEditChange;
  keepRef.current.handleEditWorkflow = onEditWorkflow;

  const showEditColumn = useMemo(
    () => messages?.some(m => m.actions?.includes('edit')),
    [messages]
  );

  const Severity = useCallback(({ severity }: IValidationMessage) => {
    const isError = severity?.toLowerCase() === 'error';
    const textClassName = isError ? 'color-red-d16' : 'color-orange-d16';
    const iconClassName = isError ? 'color-red' : 'color-orange';
    const icon = isError ? 'error' : 'warning';
    const text = isError ? 'Error' : 'Caution';
    return (
      <div className="d-flex align-items-center">
        <Icon name={icon} className={iconClassName} />
        <span className={classnames('ml-4', textClassName)}>{text}</span>
      </div>
    );
  }, []);

  const WorkflowText = useCallback(
    ({ source }: IValidationMessage) => (
      <span className="form-group-static">
        <span className="form-group-static__text">{source}</span>
      </span>
    ),
    []
  );

  const DescriptionText = useCallback(
    ({ message, field, isChangeFeedback }: IValidationMessage) => (
      <InlineMessage
        message={message}
        fieldName={!isChangeFeedback ? camelToText(field) : undefined}
      />
    ),
    []
  );

  const ActionButton = useCallback(
    ({ workflowId, actions, source }: IValidationMessage) => {
      const editable = actions?.includes('edit');

      const handleEdit = () => {
        const { handleEditWorkflow, handleEditChange } = keepRef.current;

        if (workflowId) {
          isFunction(handleEditWorkflow) &&
            handleEditWorkflow({
              id: workflowId,
              name: source
            } as IWorkflowModel);
        } else {
          isFunction(handleEditChange) && handleEditChange(changeId);
        }
      };

      if (!editable) return;

      return (
        <Button size="sm" variant="outline-primary" onClick={handleEdit}>
          {t('txt_edit')}
        </Button>
      );
    },
    [t, changeId]
  );

  const columns = useMemo<ColumnType[]>(() => {
    const result: any[] = [
      {
        id: 'severity',
        Header: t('txt_change_type_short'),
        accessor: Severity,
        width: 120
      },
      {
        id: 'source',
        Header: t('txt_source'),
        accessor: WorkflowText,
        width: 220
      },
      {
        id: 'message',
        Header: t('txt_message'),
        accessor: DescriptionText,
        width: 540
      }
    ];
    showEditColumn &&
      result.push({
        id: 'editAction',
        Header: t('txt_action'),
        accessor: ActionButton,
        cellBodyProps: { className: 'table-cell__action-view text-center' },
        cellHeaderProps: { className: 'text-center' },
        width: 90
      });

    return result;
  }, [
    t,
    showEditColumn,
    Severity,
    WorkflowText,
    DescriptionText,
    ActionButton
  ]);

  const data = useMemo<IValidationMessage[]>(
    () =>
      orderBy(
        messages,
        ['severity', 'isChangeFeedback', 'source', 'field'],
        ['desc', 'asc', 'asc', 'asc']
      ),
    [messages]
  );

  const modalContent = useMemo(
    () => (
      <React.Fragment>
        <ModalHeader border closeButton onHide={onClose}>
          <ModalTitle>{t('txt_change_validation_feedback_title')}</ModalTitle>
        </ModalHeader>
        <ModalBody className="overflow-auto">
          <div>
            <p>{t('txt_change_validation_feedback_description')}</p>
          </div>
          <div className="mt-24">
            <h5>{t('txt_validations')}</h5>
            <div className="mt-16">
              <Grid columns={columns} data={data} />
            </div>
          </div>
        </ModalBody>
        <ModalFooter cancelButtonText={t('txt_close')} onCancel={onClose} />
      </React.Fragment>
    ),
    [t, columns, data, onClose]
  );

  return (
    <ModalRegistry
      id={id}
      show={show}
      full={isDevice}
      lg={!isDevice}
      onAutoClosedAll={onClose}
    >
      {modalContent}
    </ModalRegistry>
  );
};

interface IInlineMessage {
  message: string;
  fieldName?: string;
}
const InlineMessage: React.FC<IInlineMessage> = ({ message, fieldName }) => {
  const fullMessage = `${(fieldName && ' - ') || ''}${message}`;

  return (
    <TruncateText
      resizable
      title={`${fieldName || ''}${fullMessage}`}
      lines={2}
      ellipsisMoreText="more"
      ellipsisLessText="less"
      prefix={{
        onlyDisplayOnTruncate: false,
        element: fieldName && <span className="fw-500 mr-4">{fieldName}</span>
      }}
    >
      {fullMessage}
    </TruncateText>
  );
};
