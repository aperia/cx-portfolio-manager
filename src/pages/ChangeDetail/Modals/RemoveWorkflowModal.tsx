import ModalRegistry from 'app/components/ModalRegistry';
import {
  Button,
  ModalBody,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import isFunction from 'lodash.isfunction';
import {
  actionsWorkflow,
  useSelectChangeWorkflowRemoval
} from 'pages/_commons/redux/Workflow';
import { actionsToast } from 'pages/_commons/ToastNotifications/redux';
import React from 'react';
import { useDispatch } from 'react-redux';

interface IProps {
  id: string;
  show: boolean;
  workflowId: string;
  workflowName: string;

  onClose?: (reload?: boolean) => void;
}

export const RemoveWorkflowModal: React.FC<IProps> = ({
  id,
  show,
  workflowId,
  workflowName,

  onClose
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const { loading } = useSelectChangeWorkflowRemoval();

  const onRemove = async () => {
    const result: any = await Promise.resolve(
      dispatch(actionsWorkflow.removeWorkflow(workflowId))
    );
    const isSuccess = actionsWorkflow.removeWorkflow.fulfilled.match(result);
    if (isSuccess) {
      dispatch(
        actionsToast.addToast({
          type: 'success',
          message: t('txt_notification_delete_workflow_success')
        })
      );
      isFunction(onClose) && onClose(isSuccess);
    } else {
      dispatch(
        actionsToast.addToast({
          type: 'error',
          message: t('txt_notification_delete_workflow_fail')
        })
      );
    }
  };

  const handleCloseWithoutReload = () => {
    isFunction(onClose) && onClose();
  };

  return (
    <ModalRegistry
      id={id}
      show={show}
      onAutoClosedAll={handleCloseWithoutReload}
      classes={{ content: loading ? 'loading' : '' }}
    >
      <ModalHeader border closeButton onHide={handleCloseWithoutReload}>
        <ModalTitle>{t('txt_confirm_deletion')}</ModalTitle>
      </ModalHeader>
      <ModalBody>
        <span>
          {t('txt_deletion_description').replace('{0}', workflowName)}
        </span>
      </ModalBody>
      <div className="dls-modal-footer modal-footer">
        <Button variant="secondary" onClick={handleCloseWithoutReload}>
          {t('txt_cancel')}
        </Button>
        <Button variant="danger" onClick={onRemove}>
          {t('txt_delete')}
        </Button>
      </div>
    </ModalRegistry>
  );
};
