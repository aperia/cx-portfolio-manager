import ModalRegistry from 'app/components/ModalRegistry';
import {
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import isFunction from 'lodash.isfunction';
import {
  actionsChange,
  useSelectLoadingOnSubmitChange
} from 'pages/_commons/redux/Change';
import { actionsToast } from 'pages/_commons/ToastNotifications/redux';
import React from 'react';
import { useDispatch } from 'react-redux';

interface IProps {
  id: string;
  changeId: string;
  changeName: string;
  show: boolean;
  onClose?: (reload?: boolean) => void;
}

export const SubmitChangeModal: React.FC<IProps> = ({
  id,
  changeId,
  changeName,
  show,
  onClose
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const loading = useSelectLoadingOnSubmitChange();

  const handleOnOk = async () => {
    const result: any = await Promise.resolve(
      dispatch(
        actionsChange.submitChange({
          changeId
        })
      )
    );
    const isSuccess = actionsChange.submitChange.fulfilled.match(result);

    if (isSuccess) {
      dispatch(
        actionsToast.addToast({
          type: 'success',
          message: t('txt_validation_change_sent_for_validation_success')
        })
      );
      isFunction(onClose) && onClose(isSuccess);
    } else {
      dispatch(
        actionsToast.addToast({
          type: 'error',
          message: t('txt_validation_change_sent_for_validation_fail')
        })
      );
    }
  };

  const handleCloseWithoutReload = () => {
    isFunction(onClose) && onClose();
  };

  return (
    <ModalRegistry
      id={id}
      show={show}
      onAutoClosedAll={handleCloseWithoutReload}
      classes={{ content: loading ? 'loading' : '' }}
    >
      <ModalHeader border closeButton onHide={handleCloseWithoutReload}>
        <ModalTitle>{t('txt_change_submit_title')}</ModalTitle>
      </ModalHeader>
      <ModalBody>
        {t('txt_change_submit_desc').replace('{0}', changeName)}
      </ModalBody>
      <ModalFooter
        cancelButtonText={t('txt_cancel')}
        okButtonText={t('txt_submit')}
        onCancel={handleCloseWithoutReload}
        onOk={handleOnOk}
      />
    </ModalRegistry>
  );
};
