import { fireEvent } from '@testing-library/react';
import {
  mockUseDispatchFnc,
  mockUseModalRegistryFnc,
  renderComponent
} from 'app/utils';
import { actionsWorkflow } from 'pages/_commons/redux/Workflow';
import * as WorkflowSelector from 'pages/_commons/redux/Workflow/select-hooks';
import React from 'react';
import { RemoveWorkflowModal } from './RemoveWorkflowModal';

const mockUseSelectChangeWorkflowRemoval = (loading: boolean) => {
  jest
    .spyOn(WorkflowSelector, 'useSelectChangeWorkflowRemoval')
    .mockReturnValue({ loading: loading } as any);
};

jest.mock('app/_libraries/_dls', () => {
  const actualModules = jest.requireActual('app/_libraries/_dls');
  const translation = { t: (key: string) => key };
  return {
    ...actualModules,
    useTranslation: () => translation
  };
});

const mockUseModalRegistry = mockUseModalRegistryFnc();
const mockUseDispatch = mockUseDispatchFnc();

const actionsWorkflowMock: any = actionsWorkflow;

const mockProps: any = {
  id: 'id01',
  changeId: 'changeId01',
  workflowId: 'workflowId',
  workflowName: 'workflowName',
  show: true,
  onClose: jest.fn()
};

const testId = 'removeWorkflow';

const then_click_remove_button = () => {
  const rejectButton = document.body.querySelector('.btn-danger');
  fireEvent.click(rejectButton!);
};

describe('Remove workflow ', () => {
  const createWrapper = async (...props: any[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    await renderComponent(
      testId,
      <RemoveWorkflowModal {...(mergeProps as any)} />
    );
  };

  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => (() => {}) as any);
    mockUseModalRegistry.mockImplementation(() => [true, false]);
    mockUseSelectChangeWorkflowRemoval(false);
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
    mockUseModalRegistry.mockClear();
  });

  it('should remove workflow success', async () => {
    mockUseSelectChangeWorkflowRemoval(true);
    actionsWorkflowMock.removeWorkflow = () => {
      actionsWorkflowMock.removeWorkflow = {
        fulfilled: {
          match: () => true
        }
      };
    };
    await createWrapper(mockProps);

    then_click_remove_button();
    expect(mockUseDispatch).toHaveBeenCalled();
  });

  it('should remove workflow failed', async () => {
    actionsWorkflowMock.removeWorkflow = () => {
      actionsWorkflowMock.removeWorkflow = {
        fulfilled: {
          match: () => false
        }
      };
    };
    await createWrapper(mockProps);

    then_click_remove_button();
    expect(mockUseDispatch).toHaveBeenCalled();
  });

  it('should hide modal', async () => {
    await createWrapper(mockProps);
    const cancelButton = document.body.querySelector('.btn-secondary');
    fireEvent.click(cancelButton!);
    expect(mockProps.onClose).toHaveBeenCalled();
  });
});
