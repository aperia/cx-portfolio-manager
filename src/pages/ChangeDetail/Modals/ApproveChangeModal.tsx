import ModalRegistry from 'app/components/ModalRegistry';
import { ChangeStatus } from 'app/constants/enums';
import {
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import isFunction from 'lodash.isfunction';
import orderBy from 'lodash.orderby';
import {
  actionsChange,
  useSelectLoadingOnApproveChange
} from 'pages/_commons/redux/Change';
import { actionsToast } from 'pages/_commons/ToastNotifications/redux';
import React from 'react';
import { useDispatch } from 'react-redux';

interface IProps {
  id: string;
  changeId: string;
  changeName: string;
  show: boolean;
  onClose?: (reload?: boolean) => void;
}

export const MULTIAPPROVAL_MESSAGE: Record<string, string> = {
  reject: 'txt_cannot_process_reject',
  approve: 'txt_cannot_process_approve'
};

export const ApproveChangeModal: React.FC<IProps> = ({
  id,
  changeId,
  changeName,
  show,
  onClose
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const loading = useSelectLoadingOnApproveChange();

  const verifyMultiApprovals = async () => {
    const result: any = await Promise.resolve(
      dispatch(actionsChange.getChangeDetail(changeId))
    );
    const isSuccess = actionsChange.getChangeDetail.fulfilled.match(result);
    if (isSuccess) {
      const { changeStatus, approvalRecords } = result.payload.data || {};
      const record = orderBy(approvalRecords, ['actionTime'], ['desc'])[0];
      if (changeStatus !== ChangeStatus.PendingApproval) {
        if (['reject', 'approve'].includes(record?.action?.toLowerCase())) {
          dispatch(
            actionsToast.addToast({
              type: 'error',
              message: t(
                MULTIAPPROVAL_MESSAGE[record?.action?.toLowerCase()]
              ).replace('{0}', record?.user?.name || '')
            })
          );
        }
        isFunction(onClose) && onClose(isSuccess);
        return false;
      }
      return true;
    }
    return true;
  };

  const handleOnOk = async () => {
    const verifyApprove = await verifyMultiApprovals();
    if (!verifyApprove) return;
    const result: any = await Promise.resolve(
      dispatch(
        actionsChange.approveChange({
          changeId
        })
      )
    );
    const isSuccess = actionsChange.approveChange.fulfilled.match(result);

    if (isSuccess) {
      dispatch(
        actionsToast.addToast({
          type: 'success',
          message: t('txt_change_approved')
        })
      );
      isFunction(onClose) && onClose(isSuccess);
    } else {
      dispatch(
        actionsToast.addToast({
          type: 'error',
          message: t('txt_change_failed_to_approve')
        })
      );
    }
  };

  const handleCloseWithoutReload = () => {
    isFunction(onClose) && onClose();
  };

  return (
    <ModalRegistry
      id={id}
      show={show}
      onAutoClosedAll={handleCloseWithoutReload}
      classes={{ content: loading ? 'loading' : '' }}
    >
      <ModalHeader border closeButton onHide={handleCloseWithoutReload}>
        <ModalTitle>{t('txt_approval_confirmation')}</ModalTitle>
      </ModalHeader>
      <ModalBody>
        {t('txt_approval_confirmation_desc').replace('{0}', changeName)}
      </ModalBody>
      <ModalFooter
        cancelButtonText={t('txt_cancel')}
        okButtonText={t('txt_approve')}
        onCancel={handleCloseWithoutReload}
        onOk={handleOnOk}
      />
    </ModalRegistry>
  );
};
