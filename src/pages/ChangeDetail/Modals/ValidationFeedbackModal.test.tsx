import { fireEvent } from '@testing-library/react';
import {
  mockUseDispatchFnc,
  mockUseModalRegistryFnc,
  renderComponent
} from 'app/utils';
import * as ChangeSelector from 'pages/_commons/redux/Change/select-hooks';
import React from 'react';
import { ValidationFeedbackModal } from './ValidationFeedbackModal';

const mockUseSelectLoadingOnSubmitChange = (loading: boolean) => {
  jest
    .spyOn(ChangeSelector, 'useSelectLoadingOnSubmitChange')
    .mockReturnValue(loading);
};
jest.mock('app/_libraries/_dls', () => {
  const actualModules = jest.requireActual('app/_libraries/_dls');
  const translation = { t: (key: string) => key };
  return {
    ...actualModules,
    useTranslation: () => translation
  };
});

const mockUseModalRegistry = mockUseModalRegistryFnc();
const mockUseDispatch = mockUseDispatchFnc();

const mockProps: any = {
  id: 'id01',
  changeId: 'changeId01',
  messages: [
    {
      field: 'returnedCheckChargeOption',
      severity: 'caution',
      actions: 'edit',
      workflowId: 'workflowId',
      message: 'eos aliquip sed vero blandit facilisi nonumy takimata et'
    },
    {
      field: 'returnedCheckChargeOption',
      severity: 'caution',
      actions: 'edit',
      message: 'eos aliquip sed vero blandit facilisi nonumy takimata et'
    },
    {
      field: 'returnedCheckChargeOption',
      isChangeFeedback: true,
      severity: 'error',
      actions: '',
      workflowId: 'workflowId',
      message: 'eos aliquip sed vero blandit facilisi nonumy takimata et'
    }
  ],
  show: true,
  onClose: jest.fn(),
  onEditChange: jest.fn(),
  onEditWorkflow: jest.fn()
};

const testId = 'submitChange';

const then_click_edit_button = (idx: number) => {
  const editButtons = document.body.querySelectorAll('.btn-outline-primary');
  fireEvent.click(editButtons[idx]);
};

describe('Submit change ', () => {
  const createWrapper = async (...props: any[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    await renderComponent(
      testId,
      <ValidationFeedbackModal {...(mergeProps as any)} />
    );
  };

  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => (() => {}) as any);
    mockUseModalRegistry.mockImplementation(() => [true, false]);
    mockUseSelectLoadingOnSubmitChange(false);
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
    mockUseModalRegistry.mockClear();
  });

  it('should call onEditWorkFlow when click edit on row which having workflowId ', async () => {
    await createWrapper(mockProps);
    then_click_edit_button(0);
    expect(mockProps.onEditWorkflow).toHaveBeenCalled();
  });

  it('should call onEditChange when click edit on row which have no workflowId ', async () => {
    await createWrapper(mockProps);
    then_click_edit_button(1);
    expect(mockProps.onEditChange).toHaveBeenCalled();
  });

  it('should hide modal', async () => {
    await createWrapper(mockProps);
    const cancelButton = document.body.querySelector('.btn-secondary');
    fireEvent.click(cancelButton!);
    expect(mockProps.onClose).toHaveBeenCalled();
  });
});
