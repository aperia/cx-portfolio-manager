import { fireEvent } from '@testing-library/react';
import {
  mockUseDispatchFnc,
  mockUseModalRegistryFnc,
  renderComponent
} from 'app/utils';
import { actionsChange } from 'pages/_commons/redux/Change';
import * as ChangeSelector from 'pages/_commons/redux/Change/select-hooks';
import React from 'react';
import { ApproveChangeModal } from './ApproveChangeModal';

const mockUseSelectApprovalQueues = (loading: boolean) => {
  jest
    .spyOn(ChangeSelector, 'useSelectLoadingOnApproveChange')
    .mockReturnValue(loading);
};
const mockUseModalRegistry = mockUseModalRegistryFnc();
const mockUseDispatch = mockUseDispatchFnc();

const actionsChangeMock: any = actionsChange;

const mockProps: any = {
  id: 'id01',
  changeId: 'changeId01',
  changeName: 'changeName',
  show: true,
  onClose: jest.fn()
};

const testId = 'approvalChange';

describe('Approval Change ', () => {
  const mockDispatch = jest.fn();
  const createWrapper = async (...props: any[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    await renderComponent(testId, <ApproveChangeModal {...mergeProps} />);
  };

  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);

    mockUseModalRegistry.mockImplementation(() => [true, false]);
    mockUseSelectApprovalQueues(false);
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
    mockUseModalRegistry.mockClear();
  });

  it('should approve change not success when verify not pass ', async () => {
    const mockDispatch = jest.fn().mockImplementation(() => ({
      payload: {}
    }));
    mockUseDispatch.mockImplementation(() => mockDispatch);
    mockUseSelectApprovalQueues(true);

    actionsChangeMock.getChangeDetail = () => {
      actionsChangeMock.getChangeDetail = {
        fulfilled: {
          match: () => true
        }
      };
    };
    await createWrapper(mockProps);

    const approvalButton = document.body.querySelector('.btn-primary');
    fireEvent.click(approvalButton!);
    await new Promise(resolve => setTimeout(() => resolve(true)));
    expect(mockDispatch).toHaveBeenCalledTimes(1);
  });

  it('should approve change not success when verify not pass ', async () => {
    const mockDispatch = jest.fn().mockImplementation(() => ({
      payload: {
        data: {
          changeStatus: {},
          approvalRecords: [
            {
              action: 'reject',
              actionTime: '2021-05-04T10:27:25.0193714+07:00'
            }
          ]
        }
      }
    }));
    mockUseDispatch.mockImplementation(() => mockDispatch);
    mockUseSelectApprovalQueues(true);

    actionsChangeMock.getChangeDetail = () => {
      actionsChangeMock.getChangeDetail = {
        fulfilled: {
          match: () => true
        }
      };
    };
    await createWrapper(mockProps);

    const approvalButton = document.body.querySelector('.btn-primary');
    fireEvent.click(approvalButton!);
    await new Promise(resolve => setTimeout(() => resolve(true)));
    expect(mockDispatch).toHaveBeenCalledTimes(2);
  });

  it('should approve change not success when verify not pass', async () => {
    const mockDispatch = jest.fn().mockImplementation(() => ({
      payload: {
        data: {
          changeStatus: {},
          approvalRecords: [
            {
              action: 'pending',
              actionTime: '2021-05-04T10:27:25.0193714+07:00'
            }
          ]
        }
      }
    }));
    mockUseDispatch.mockImplementation(() => mockDispatch);

    actionsChangeMock.getChangeDetail = () => {
      actionsChangeMock.getChangeDetail = {
        fulfilled: {
          match: () => true
        }
      };
    };
    await createWrapper(mockProps);

    const approvalButton = document.body.querySelector('.btn-primary');
    fireEvent.click(approvalButton!);
    await new Promise(resolve => setTimeout(() => resolve(true)));
    expect(mockDispatch).toHaveBeenCalledTimes(1);
  });

  it('should approve change not success when verify not pass', async () => {
    const mockDispatch = jest.fn().mockImplementation(() => ({
      payload: {
        data: {
          changeStatus: 'pendingApproval',
          approvalRecords: []
        }
      }
    }));
    mockUseDispatch.mockImplementation(() => mockDispatch);

    actionsChangeMock.getChangeDetail = () => {
      actionsChangeMock.getChangeDetail = {
        fulfilled: {
          match: () => true
        }
      };
    };
    actionsChangeMock.approveChange = () => {
      actionsChangeMock.approveChange = {
        fulfilled: {
          match: () => true
        }
      };
    };
    await createWrapper(mockProps);

    const approvalButton = document.body.querySelector('.btn-primary');
    fireEvent.click(approvalButton!);
    await new Promise(resolve => setTimeout(() => resolve(true)));
    expect(mockDispatch).toHaveBeenCalledTimes(3);
  });

  it('should approve change success', async () => {
    mockUseDispatch.mockImplementation(() => mockDispatch);

    actionsChangeMock.getChangeDetail = () => {
      actionsChangeMock.getChangeDetail = {
        fulfilled: {
          match: () => false
        }
      };
    };
    actionsChangeMock.approveChange = () => {
      actionsChangeMock.approveChange = {
        fulfilled: {
          match: () => true
        }
      };
    };
    await createWrapper(mockProps);

    const approvalButton = document.body.querySelector('.btn-primary');
    fireEvent.click(approvalButton!);
    await new Promise(resolve => setTimeout(() => resolve(true)));
    expect(mockDispatch).toHaveBeenCalledTimes(3);
  });

  it('should approve change fail', async () => {
    mockUseDispatch.mockImplementation(() => mockDispatch);

    actionsChangeMock.getChangeDetail = () => {
      actionsChangeMock.getChangeDetail = {
        fulfilled: {
          match: () => false
        }
      };
    };
    actionsChangeMock.approveChange = () => {
      actionsChangeMock.approveChange = {
        fulfilled: {
          match: () => false
        }
      };
    };
    await createWrapper(mockProps);

    const approvalButton = document.body.querySelector('.btn-primary');
    fireEvent.click(approvalButton!);
    await new Promise(resolve => setTimeout(() => resolve(true)));
    expect(mockDispatch).toHaveBeenCalledTimes(3);
  });

  it('should hide modal', async () => {
    await createWrapper(mockProps);
    const cancelButton = document.body.querySelector('.btn-secondary');
    fireEvent.click(cancelButton!);
    expect(mockProps.onClose).toHaveBeenCalled();
  });
});
