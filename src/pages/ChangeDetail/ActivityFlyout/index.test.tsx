import { fireEvent, render, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { mockUseModalRegistryFnc } from 'app/utils';
import 'app/utils/_mockComponent/mockFailedApiReload';
import 'app/utils/_mockComponent/mockLoadMore';
import 'app/utils/_mockComponent/mockNoDataFound';
import 'app/utils/_mockComponent/mockSortOrder';
import * as ActivityHook from 'pages/_commons/redux/Activity/select-hooks';
import * as WorkflowHook from 'pages/_commons/redux/Workflow/select-hooks';
import React from 'react';
import * as reactRedux from 'react-redux';
import ActivityFlyout from './index';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderModal = (props: any): RenderResult => {
  return render(
    <div>
      <ActivityFlyout {...props} />
    </div>
  );
};

const MockSelectActivitiesInChange = jest.spyOn(
  ActivityHook,
  'useSelectActivitiesInChange'
);

const MockSelectActivitiesInChangeFilter = jest.spyOn(
  ActivityHook,
  'useSelectActivitiesInChangeFilter'
);

const MockSelectActivitiesInChangeSearchValue = jest.spyOn(
  ActivityHook,
  'useSelectActivitiesInChangeSearchValue'
);

const MockSelectDeletedChangeWorkflowIds = jest.spyOn(
  WorkflowHook,
  'useSelectDeletedChangeWorkflowIds'
);

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');
const mockUseModalRegistry = mockUseModalRegistryFnc();

describe('Test ChangeDetail > ActivityFlyout', () => {
  const mockDispatch = jest.fn();
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
    mockUseModalRegistry.mockImplementation(() => [true, false]);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Should render with failed api', () => {
    MockSelectActivitiesInChange.mockReturnValue({
      activityList: [
        {
          id: 'activity 1',
          name: 'activity 1',
          action: 'NEW',
          dateChanged: '01/01/2020'
        }
      ] as any,
      loading: false,
      total: 0,
      error: true
    });

    MockSelectActivitiesInChangeFilter.mockReturnValue({
      page: 0,
      pageSize: 10,
      sortBy: [''],
      orderBy: ['asc']
    });

    MockSelectActivitiesInChangeSearchValue.mockReturnValue('1');
    MockSelectDeletedChangeWorkflowIds.mockReturnValue(['2', '3']);

    const props = {
      id: '1',
      changeId: 'change-1',
      title: 'title 1',
      show: true,
      onCloseModal: jest.fn()
    };

    const wrapper = renderModal(props);
    expect(wrapper.getByText('Data load unsuccessful')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('Reload'));
    expect(mockDispatch).toHaveBeenCalled();
  });

  it('Should render with no data found', () => {
    MockSelectActivitiesInChange.mockReturnValue({
      activityList: [] as any,
      loading: false,
      total: 0,
      error: false
    });

    MockSelectActivitiesInChangeFilter.mockReturnValue({
      page: 0,
      pageSize: 10,
      sortBy: [''],
      orderBy: ['asc']
    });

    MockSelectActivitiesInChangeSearchValue.mockReturnValue('1');
    MockSelectDeletedChangeWorkflowIds.mockReturnValue(['2', '3']);

    const props = {
      id: '1',
      changeId: 'change-1',
      title: 'title 1',
      show: true,
      onCloseModal: jest.fn()
    };

    const wrapper = renderModal(props);
    expect(wrapper.getByText('No data found')).toBeInTheDocument();
  });

  it('Should render with data', () => {
    MockSelectActivitiesInChange.mockReturnValue({
      activityList: [
        {
          id: 'activity 1',
          name: 'activity 1',
          action: 'NEW',
          dateChanged: '01/01/2020'
        },
        {
          id: 'activity 2',
          name: 'activity 2',
          action: 'NEW',
          dateChanged: '02/02/2020'
        }
      ] as any,
      loading: false,
      total: 2,
      error: false
    });

    MockSelectActivitiesInChangeFilter.mockReturnValue({
      page: 0,
      pageSize: 1,
      sortBy: ['dateChanged'],
      orderBy: ['asc']
    });

    MockSelectActivitiesInChangeSearchValue.mockReturnValue('search value');
    MockSelectDeletedChangeWorkflowIds.mockReturnValue(['2', '3']);

    const props = {
      id: '1',
      changeId: 'change-1',
      title: 'title 1',
      show: true,
      onCloseModal: jest.fn()
    };

    const wrapper = renderModal(props);
    expect(wrapper.getByText('activity 1')).toBeInTheDocument();

    jest.useFakeTimers();
    const loadMoreBtn = wrapper.baseElement.querySelector(
      'button.btn-load-more'
    );
    fireEvent.click(loadMoreBtn!);
    expect(document.body.querySelector('.load-more-cont')!.className).toContain(
      'loading'
    );
    jest.advanceTimersByTime(300);
    expect(
      document.body.querySelector('.load-more-cont')!.className
    ).not.toContain('loading');

    fireEvent.click(wrapper.getByText('txt_clear_and_reset'));
    expect(mockDispatch).toHaveBeenCalledWith({
      payload: undefined,
      type: 'activity/clearAndResetActivitiesInChangeFilter'
    });

    fireEvent.click(
      wrapper.baseElement.querySelector('i.icon.icon-search') as Element
    );
    expect(wrapper.getByText('txt_apply')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('txt_apply'));
    expect(mockDispatch).toHaveBeenCalledWith({
      payload: undefined,
      type: 'activity/clearAndResetActivitiesInChangeFilter'
    });

    fireEvent.click(
      wrapper.baseElement.querySelector('i.icon.icon-sort-by') as Element
    );
    expect(wrapper.getByText('Sort and Order')).toBeInTheDocument();
    userEvent.click(wrapper.getByText('Descending') as Element);
    fireEvent.click(wrapper.getByText('Apply'));
    expect(mockDispatch).toHaveBeenCalledWith({
      payload: undefined,
      type: 'activity/clearAndResetActivitiesInChangeFilter'
    });

    fireEvent.click(
      wrapper.baseElement.querySelector('i.icon.icon-close') as Element
    );

    expect(mockDispatch).toHaveBeenCalledWith({
      payload: undefined,
      type: 'activity/clearAndResetActivitiesInChangeFilter'
    });
  });
});
