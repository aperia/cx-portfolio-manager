import Activities from 'app/components/Activities';
import FailedApi from 'app/components/FailedApiReload';
import LoadMore from 'app/components/LoadMore';
import ModalRegistry from 'app/components/ModalRegistry';
import NoDataFound from 'app/components/NoDataFound';
import SimpleSearchSmall from 'app/components/SimpleSearchSmall';
import SortAndOrderTablet from 'app/components/SortAndOrder/OnTablet';
import {
  ACTIVITIES_SORT_BY_LIST,
  ORDER_BY_LIST
} from 'app/constants/constants';
import { classnames } from 'app/helpers';
import {
  Button,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  SimpleBar,
  useTranslation
} from 'app/_libraries/_dls';
import {
  actionsActivity,
  useSelectActivitiesInChange,
  useSelectActivitiesInChangeFilter,
  useSelectActivitiesInChangeSearchValue
} from 'pages/_commons/redux/Activity';
import { defaultDataFilter } from 'pages/_commons/redux/Activity/reducers';
import { useSelectDeletedChangeWorkflowIds } from 'pages/_commons/redux/Workflow';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';

export interface IActivityFlyoutProps {
  id: string;
  changeId: string;
  title?: string;
  show: boolean;
  onCloseModal: () => void;
}
const ActivityFlyout: React.FC<IActivityFlyoutProps> = ({
  id,
  changeId,
  title,
  show,
  onCloseModal
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const contentRef = useRef<HTMLDivElement>(null);

  const { activityList, loading, error, total } = useSelectActivitiesInChange();
  const {
    // searchValue,
    page,
    pageSize,
    sortBy,
    orderBy
  } = useSelectActivitiesInChangeFilter();

  const searchValue = useSelectActivitiesInChangeSearchValue();

  const deletedWorkflowIds = useSelectDeletedChangeWorkflowIds();

  const haveLoadMore = useMemo(() => {
    return pageSize && pageSize < total;
  }, [pageSize, total]);

  const [loadingMore, setLoadingMore] = useState(false);

  // pagination
  const handleOnChangePage = useCallback(() => {
    setLoadingMore(true);
    setTimeout(() => {
      dispatch(
        actionsActivity.updateActivitiesInChangeFilter({ page: page! + 1 })
      );
      setLoadingMore(false);
    }, 300);
  }, [dispatch, page]);

  // sort by
  const handleChangeSortBy = useCallback(
    (sortByValue: any) => {
      dispatch(
        actionsActivity.updateActivitiesInChangeFilter({
          sortBy: [sortByValue.value]
        })
      );
    },
    [dispatch]
  );

  // order by
  const handleChangeOrderBy = useCallback(
    (orderByValue: any) => {
      dispatch(
        actionsActivity.updateActivitiesInChangeFilter({
          orderBy: [orderByValue.value]
        })
      );
    },
    [dispatch]
  );

  // search
  const handleSearch = useCallback(
    (val: string) => {
      dispatch(
        actionsActivity.updateActivitiesInChangeFilter({
          searchValue: val?.trim()
        })
      );
    },
    [dispatch]
  );

  // clear and reset
  const handleClearAndReset = useCallback(() => {
    dispatch(actionsActivity.clearAndResetActivitiesInChangeFilter());
  }, [dispatch]);

  // modal close
  const handleModalClosed = useCallback(() => {
    dispatch(actionsActivity.setToDefault());
    onCloseModal && onCloseModal();
  }, [dispatch, onCloseModal]);

  // dispatch action fetch data activity
  useEffect(() => {
    dispatch(actionsActivity.getActivitiesInChange(changeId));

    return () => {
      dispatch(actionsActivity.setToDefault());
      dispatch(actionsActivity.clearAndResetActivitiesInChangeFilter());
    };
  }, [dispatch, changeId]);

  const handleApiReload = useCallback(() => {
    dispatch(actionsActivity.getActivitiesInChange(changeId));
  }, [dispatch, changeId]);

  useEffect(() => {
    const el = contentRef.current?.querySelector('.check-intersection');
    if (el) {
      const observer = new IntersectionObserver(
        ([e]) => {
          const flyoutHead = contentRef.current?.querySelector('.flyout-head');
          flyoutHead &&
            flyoutHead.classList.toggle('has-scroll', e.intersectionRatio < 1);
        },
        {
          threshold: [0.1]
        }
      );
      observer.observe(el);
      return () => observer.unobserve(el);
    }
  });

  const activityCardView = useMemo(() => {
    return (
      <Activities
        activities={activityList}
        deletedWorkflowIds={deletedWorkflowIds}
      />
    );
  }, [activityList, deletedWorkflowIds]);

  const sortOrderClear = useMemo(
    () => (
      <React.Fragment>
        <SortAndOrderTablet
          sortByFields={ACTIVITIES_SORT_BY_LIST}
          sortByValueDefault={ACTIVITIES_SORT_BY_LIST.find(
            i => i.value === defaultDataFilter.sortBy![0]
          )}
          orderByValueDefault={ORDER_BY_LIST.find(
            i => i.value === defaultDataFilter.orderBy![0]
          )}
          sortByValue={ACTIVITIES_SORT_BY_LIST.find(
            i => i.value === sortBy![0]
          )}
          orderByValue={ORDER_BY_LIST.find(i => i.value === orderBy![0])}
          onOrderByChange={handleChangeOrderBy}
          onSortByChange={handleChangeSortBy}
        />
      </React.Fragment>
    ),
    [orderBy, sortBy, handleChangeOrderBy, handleChangeSortBy]
  );

  return (
    <ModalRegistry
      id={id}
      rt
      enforceFocus={false}
      scrollable={false}
      show={show}
      animationDuration={500}
      animationComponentProps={{ direction: 'right' }}
      classes={{
        content: 'activity-modal'
      }}
      onAutoClosedAll={handleModalClosed}
    >
      <ModalHeader border closeButton onHide={handleModalClosed}>
        <ModalTitle>{title}</ModalTitle>
      </ModalHeader>
      <ModalBody className={classnames('p-0 overflow-auto', { loading })}>
        <SimpleBar>
          <div className={classnames('d-flex flex-column')} ref={contentRef}>
            <div className="check-intersection" />
            <div
              id="activity-fly-out-head"
              className={classnames({
                'flyout-head sticky pb-0 pt-24 px-24': true,
                'no-data': !activityList || activityList.length === 0
              })}
            >
              <div className="d-flex align-items-center justify-content-between mb-16">
                <h5>Activity List</h5>
                {(searchValue || activityList?.length > 0) && (
                  <div className="mr-n4">
                    <SimpleSearchSmall
                      placeholder="Type to search by activities"
                      maxLength={200}
                      onSearch={handleSearch}
                      defaultValue={searchValue}
                    />
                    {sortOrderClear}
                  </div>
                )}
              </div>
              {searchValue && activityList?.length > 0 && (
                <div className="d-flex align-items-center justify-content-end mb-16 mr-n8">
                  <Button
                    id="ContactList-Button-Clear"
                    variant="outline-primary"
                    size="sm"
                    onClick={handleClearAndReset}
                  >
                    {t('txt_clear_and_reset')}
                  </Button>
                </div>
              )}
            </div>
            <div className="flyout-body pt-0 pb-24 px-24">
              {error ? (
                <FailedApi
                  id="home-page-change-card-error"
                  onReload={handleApiReload}
                  className="mt-40 pt-40 d-flex flex-column justify-content-center align-items-center"
                />
              ) : activityList?.length > 0 ? (
                <>
                  {activityCardView}
                  {haveLoadMore && (
                    <LoadMore
                      loading={loadingMore}
                      onLoadMore={handleOnChangePage}
                      isEnd={page! * pageSize! > total}
                      className="mt-24"
                      endLoadMoreText="End of Activity Log."
                    />
                  )}
                </>
              ) : (
                !loading && (
                  <>
                    <NoDataFound
                      id="homepage-change-not-found"
                      className="mt-40 pt-40"
                      title={t('txt_activities_no_data_to_display')}
                      hasSearch={!!searchValue}
                      linkTitle={searchValue && t('txt_clear_and_reset')}
                      onLinkClicked={handleClearAndReset}
                    />
                  </>
                )
              )}
            </div>
          </div>
        </SimpleBar>
      </ModalBody>
      <ModalFooter
        border
        cancelButtonText="Close"
        onCancel={handleModalClosed}
      />
    </ModalRegistry>
  );
};

export default ActivityFlyout;
