import { fireEvent } from '@testing-library/dom';
import { WorkflowInProgressCardViewProps } from 'app/components/WorkflowInProgressCardView';
import {
  changeDlsDropdownMock,
  mockUseDispatchFnc,
  mockUseSelectWindowDimension,
  renderComponent
} from 'app/utils';
import 'app/utils/_mockComponent/mockBreadCrumb';
import 'app/utils/_mockComponent/mockCanvas';
import 'app/utils/_mockComponent/mockFailedApiReload';
import 'app/utils/_mockComponent/mockNoDataFound';
import 'app/utils/_mockComponent/mockPaging';
import 'app/utils/_mockComponent/mockSimpleSearch';
import 'app/utils/_mockComponent/mockSortAndOrder';
import * as selectHooks from 'pages/_commons/redux/Workflow/select-hooks';
import React from 'react';
import { HashRouter, Route } from 'react-router-dom';
import MyInProgressWorkflows from './index';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    __esModule: true,
    ...actualModule,
    useTranslation: () => ({ t: (key: string) => key })
  };
});
jest.mock('app/components/WorkflowInProgressCardView', () => {
  return {
    __esModule: true,
    default: ({ id, onToggle, isExpand }: WorkflowInProgressCardViewProps) => (
      <div>
        <div
          onClick={() => onToggle && onToggle('testId1')}
        >{`Workflow Card View ${id}`}</div>
        <div>{isExpand ? 'expand' : 'collapse'}</div>
      </div>
    )
  };
});
// mock common hooks implementation
const useDispatchMock = mockUseDispatchFnc();
const dispatchMock = jest.fn();
// mock selector hooks implementation
const useSelectInProgressWorkflowsSelectorMock = jest.spyOn(
  selectHooks,
  'useSelectInProgressWorkflowsSelector'
);
const useSelectInProgressWorkflowsFilterSelectorMock = jest.spyOn(
  selectHooks,
  'useSelectInProgressWorkflowsFilterSelector'
);
const useSelectInProgressWorkflowTypesSelectorMock = jest.spyOn(
  selectHooks,
  'useSelectInProgressWorkflowTypesSelector'
);

beforeEach(() => {
  useDispatchMock.mockImplementation(() => dispatchMock);
  mockUseSelectWindowDimension(1920);
});
afterEach(() => {
  jest.clearAllMocks();
});

describe('My Inprogress Workflows', () => {
  const testId = 'testId';
  const WorkflowInprogressData = [
    {
      id: 'testId1',
      name: 'Test Name 1'
    },
    {
      id: 'testId2',
      name: 'Test Name 2'
    }
  ] as IWorkflowModel[];
  const renderWrapper = async () => {
    const component = (
      <HashRouter>
        <Route>
          <MyInProgressWorkflows />;
        </Route>
      </HashRouter>
    );
    const renderResult = await renderComponent(testId, component);
    return renderResult;
  };
  it('Render Component has data', async () => {
    // mock selector return value
    useSelectInProgressWorkflowsSelectorMock.mockReturnValue({
      workflows: WorkflowInprogressData,
      loading: false,
      error: false,
      total: 2
    });
    useSelectInProgressWorkflowsFilterSelectorMock.mockReturnValue({
      sortBy: ['updateDate', 'name'],
      orderBy: ['desc', 'asc'],
      searchValue: '',
      filterValue: ''
    });
    useSelectInProgressWorkflowTypesSelectorMock.mockReturnValue([
      { code: '', text: 'All' },
      { code: 'type1', text: 'Type 1' },
      { code: 'type2', text: 'Type 2' },
      { code: 'type3', text: 'Type 3' }
    ]);

    const { getByText, getAllByText } = await renderWrapper();
    expect(getByText(/Mock BreadCrumb/)).toBeInTheDocument();
    expect(getAllByText(/Workflow Card View/)).toHaveLength(2);
    fireEvent.click(getByText(/Workflow Card View testId1/));
    expect(getByText('expand')).toBeInTheDocument();
  });

  it('Render Component > Search and Filter', async () => {
    // mock selector return value
    useSelectInProgressWorkflowsSelectorMock.mockReturnValue({
      workflows: WorkflowInprogressData,
      loading: false,
      error: false,
      total: 2
    });
    useSelectInProgressWorkflowsFilterSelectorMock.mockReturnValue({
      sortBy: ['updateDate', 'name'],
      orderBy: ['desc', 'asc'],
      searchValue: '',
      filterValue: 'xxx'
    });
    useSelectInProgressWorkflowTypesSelectorMock.mockReturnValue([
      { code: '', text: 'All' },
      { code: 'type1', text: 'Type 1' },
      { code: 'type2', text: 'Type 2' },
      { code: 'type3', text: 'Type 3' }
    ]);

    const { getByText, getAllByText, getByTestId } = await renderWrapper();
    expect(getByText(/Mock BreadCrumb/)).toBeInTheDocument();
    expect(getAllByText(/Workflow Card View/)).toHaveLength(2);
    fireEvent.click(getByText(/Simple Search/));
    expect(dispatchMock).toBeCalled();
    changeDlsDropdownMock(0, 2, getByTestId(testId));
    expect(dispatchMock).toBeCalled();
  });

  it('Render Component has error', async () => {
    // mock selector return value
    useSelectInProgressWorkflowsSelectorMock.mockReturnValue({
      workflows: [],
      loading: false,
      error: true,
      total: 0
    });
    useSelectInProgressWorkflowsFilterSelectorMock.mockReturnValue({
      sortBy: ['updateDate', 'name'],
      orderBy: ['desc', 'asc'],
      searchValue: '',
      filterValue: ''
    });
    useSelectInProgressWorkflowTypesSelectorMock.mockReturnValue([
      { code: '', text: 'All' },
      { code: 'type1', text: 'Type 1' },
      { code: 'type2', text: 'Type 2' },
      { code: 'type3', text: 'Type 3' }
    ]);

    const { getByText } = await renderWrapper();
    expect(getByText(/Data load unsuccessful/)).toBeInTheDocument();
    fireEvent.click(getByText(/Reload/));
    expect(dispatchMock).toBeCalled();
  });

  it('Render Component has no data', async () => {
    // mock selector return value
    useSelectInProgressWorkflowsSelectorMock.mockReturnValue({
      workflows: [],
      loading: false,
      error: false,
      total: 0
    });
    useSelectInProgressWorkflowsFilterSelectorMock.mockReturnValue({
      sortBy: ['updateDate', 'name'],
      orderBy: ['desc', 'asc'],
      searchValue: '',
      filterValue: ''
    });
    useSelectInProgressWorkflowTypesSelectorMock.mockReturnValue([
      { code: '', text: 'All' },
      { code: 'type1', text: 'Type 1' },
      { code: 'type2', text: 'Type 2' },
      { code: 'type3', text: 'Type 3' }
    ]);

    const { getByText } = await renderWrapper();
    expect(getByText(/No data found/)).toBeInTheDocument();
    fireEvent.click(getByText(/No data found/));
    expect(dispatchMock).toBeCalled();
  });

  it('Render Component > Sort and Order', async () => {
    // mock selector return value
    useSelectInProgressWorkflowsSelectorMock.mockReturnValue({
      workflows: WorkflowInprogressData,
      loading: false,
      error: false,
      total: 2
    });
    useSelectInProgressWorkflowsFilterSelectorMock.mockReturnValue({
      sortBy: ['updateDate', 'name'],
      orderBy: ['desc', 'asc'],
      searchValue: '',
      filterValue: ''
    });
    useSelectInProgressWorkflowTypesSelectorMock.mockReturnValue([
      { code: '', text: 'All' },
      { code: 'type1', text: 'Type 1' },
      { code: 'type2', text: 'Type 2' },
      { code: 'type3', text: 'Type 3' }
    ]);

    const { getByText } = await renderWrapper();
    fireEvent.click(getByText(/Change to field1/));
    expect(dispatchMock).toBeCalled();
    fireEvent.click(getByText(/Change to asc/));
    expect(dispatchMock).toBeCalled();
  });

  it('Render Component > Pagination', async () => {
    // mock selector return value
    useSelectInProgressWorkflowsSelectorMock.mockReturnValue({
      workflows: WorkflowInprogressData,
      loading: false,
      error: false,
      total: 2
    });
    useSelectInProgressWorkflowsFilterSelectorMock.mockReturnValue({
      sortBy: ['updateDate', 'name'],
      orderBy: ['desc', 'asc'],
      searchValue: '',
      filterValue: ''
    });
    useSelectInProgressWorkflowTypesSelectorMock.mockReturnValue([
      { code: '', text: 'All' },
      { code: 'type1', text: 'Type 1' },
      { code: 'type2', text: 'Type 2' },
      { code: 'type3', text: 'Type 3' }
    ]);

    const { getByText } = await renderWrapper();
    fireEvent.click(getByText(/Page 2/));
    expect(dispatchMock).toBeCalled();
    fireEvent.click(getByText(/Page Size 25/));
    expect(dispatchMock).toBeCalled();
  });
});
