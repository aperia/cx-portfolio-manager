import BreadCrumb from 'app/components/BreadCrumb';
import FailedApiReload from 'app/components/FailedApiReload';
import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import WorkflowInProgressCardView from 'app/components/WorkflowInProgressCardView';
import { INPROGRESS_WORKFLOWS_SORT_BY_LIST } from 'app/constants/constants';
import { BASE_URL } from 'app/constants/links';
import { classnames } from 'app/helpers';
import {
  DropdownBaseChangeEvent,
  DropdownList,
  SimpleBar,
  useTranslation
} from 'app/_libraries/_dls';
import {
  actionsWorkflow,
  useSelectInProgressWorkflowsFilterSelector,
  useSelectInProgressWorkflowsSelector,
  useSelectInProgressWorkflowTypesSelector
} from 'pages/_commons/redux/Workflow';
import { defaultInprogressDataFilter } from 'pages/_commons/redux/Workflow/reducers';
import Paging from 'pages/_commons/Utils/Paging';
import SortOrder from 'pages/_commons/Utils/SortOrder';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';

const breadCrmbItems = [
  { id: 'home', title: <Link to={BASE_URL}>Home</Link> },
  { id: 'myInProgressWorkflows', title: 'My In Progress Workflows' }
];

interface IMyInProgressWorkflowsProps {}
const MyInProgressWorkflows: React.FC<IMyInProgressWorkflowsProps> = () => {
  const { workflows, total, loading, error } =
    useSelectInProgressWorkflowsSelector();
  const dataFilter = useSelectInProgressWorkflowsFilterSelector();
  const { page = 1, pageSize = 10, searchValue, filterValue } = dataFilter;

  const [expandIds, setExpandIds] = useState<string[]>([]);

  const dynamicWorkflowTypes = useSelectInProgressWorkflowTypesSelector();
  const hasFilterToClear = searchValue || filterValue;
  const dispatch = useDispatch();
  const { t } = useTranslation();

  // sort by
  const handleChangeSortBy = useCallback(
    (sortByValue: string) => {
      sortByValue &&
        dispatch(
          actionsWorkflow.updateInProgressWorkflowsFilter({
            sortBy: [sortByValue]
          })
        );
    },
    [dispatch]
  );

  // order by
  const handleChangeOrderBy = useCallback(
    (orderByValue: string) => {
      orderByValue &&
        dispatch(
          actionsWorkflow.updateInProgressWorkflowsFilter({
            orderBy: [orderByValue as OrderByType]
          })
        );
    },
    [dispatch]
  );

  // pagination
  const handleChangePage = useCallback(
    (pageNumber: number) => {
      dispatch(
        actionsWorkflow.updateInProgressWorkflowsFilter({
          page: pageNumber
        })
      );
    },
    [dispatch]
  );

  const handleChangePageSize = useCallback(
    (pageSizeNumber: number) => {
      dispatch(
        actionsWorkflow.updateInProgressWorkflowsFilter({
          pageSize: pageSizeNumber,
          page: 1
        })
      );
    },
    [dispatch]
  );

  const handleSearch = useCallback(
    (val: string) => {
      dispatch(
        actionsWorkflow.updateInProgressWorkflowsFilter({
          page: 1,
          searchValue: val
        })
      );
    },
    [dispatch]
  );

  const handleFilterWorkflowType = useCallback(
    (e: DropdownBaseChangeEvent) => {
      dispatch(
        actionsWorkflow.updateInProgressWorkflowsFilter({
          page: 1,
          filterValue: e.target.value.code
        })
      );
    },
    [dispatch]
  );

  // clear and reset
  const handleClearAndReset = useCallback(() => {
    dispatch(actionsWorkflow.clearAndResetInProgressWorkflowsFilter());
  }, [dispatch]);

  const handleApiReload = useCallback(() => {
    dispatch(actionsWorkflow.getWorkflows());
  }, [dispatch]);

  useEffect(() => {
    dispatch(actionsWorkflow.getWorkflows());
    return () => {
      dispatch(actionsWorkflow.resetInProgressWorkflowsFilterToDefault());
    };
  }, [dispatch]);

  const handleToggle = useCallback((workflowId: string) => {
    setExpandIds([workflowId]);
  }, []);

  const titleAndFilter = useMemo(
    () => (
      <div>
        <div className="mb-16">
          <BreadCrumb
            id={'workflows__breadcrumb'}
            items={breadCrmbItems}
            width={992}
          />
        </div>
        <div className="d-flex justify-content-between align-items-center mb-16">
          <h4>My In Progress Workflows</h4>
          {(total > 0 || hasFilterToClear) && (
            <SimpleSearch
              placeholder={t('txt_type_to_search_by_workflow_name_and_desc')}
              onSearch={handleSearch}
              defaultValue={searchValue}
            />
          )}
        </div>
        <div className="d-flex justify-content-between align-items-center mb-16">
          {(total > 0 || hasFilterToClear) && (
            <div className="d-flex align-items-center">
              <strong className="fs-14 color-grey mr-4">Type:</strong>
              <DropdownList
                name="type"
                textField="text"
                value={
                  dynamicWorkflowTypes.find(
                    item => item.code === filterValue
                  ) || dynamicWorkflowTypes[0]
                }
                onChange={handleFilterWorkflowType}
                variant="no-border"
              >
                {dynamicWorkflowTypes.map((item, idx) => (
                  <DropdownList.Item key={idx} label={item.text} value={item} />
                ))}
              </DropdownList>
            </div>
          )}
          {total > 0 && (
            <div className="d-flex align-self-end">
              <SortOrder
                hasFilter={!!searchValue || hasFilterToClear}
                dataFilter={dataFilter}
                sortByFields={INPROGRESS_WORKFLOWS_SORT_BY_LIST}
                defaultDataFilter={defaultInprogressDataFilter}
                onChangeOrderBy={handleChangeOrderBy}
                onChangeSortBy={handleChangeSortBy}
                onClearSearch={handleClearAndReset}
              />
            </div>
          )}
        </div>
      </div>
    ),
    [
      t,
      handleChangeOrderBy,
      handleChangeSortBy,
      handleSearch,
      dataFilter,
      searchValue,
      handleClearAndReset,
      total,
      handleFilterWorkflowType,
      hasFilterToClear,
      filterValue,
      dynamicWorkflowTypes
    ]
  );

  const workflowCardList = useMemo(
    () => (
      <div
        className={classnames({
          'loading mh-320': loading
        })}
      >
        {error ? (
          <FailedApiReload
            id="workflow-in-progress-page-error"
            onReload={handleApiReload}
            className="mt-40 pt-40 d-flex flex-column justify-content-center align-items-center"
          />
        ) : workflows?.length > 0 ? (
          workflows.map(item => (
            <WorkflowInProgressCardView
              key={item.id}
              id={item.id}
              value={item}
              isExpand={expandIds.includes(item.id)}
              onToggle={handleToggle}
            />
          ))
        ) : (
          !loading && (
            <NoDataFound
              id="workflow-in-progress-page-not-found"
              className="mt-40 pt-40"
              title={t('txt_no_in_progress_workflows_to_display')}
              hasSearch={!!searchValue}
              hasFilter={!!filterValue}
              isFullPage={true}
              linkTitle={t('txt_clear_and_reset')}
              onLinkClicked={handleClearAndReset}
            />
          )
        )}
      </div>
    ),
    [
      workflows,
      error,
      filterValue,
      handleApiReload,
      loading,
      searchValue,
      t,
      handleClearAndReset,
      expandIds,
      handleToggle
    ]
  );

  const paging = useMemo(
    () => (
      <Paging
        page={page}
        pageSize={pageSize}
        totalItem={total}
        onChangePage={handleChangePage}
        onChangePageSize={handleChangePageSize}
      />
    ),
    [page, pageSize, total, handleChangePage, handleChangePageSize]
  );

  return (
    <SimpleBar>
      <div className={classnames('p-24')}>
        {titleAndFilter}
        {workflowCardList}
        {paging}
      </div>
    </SimpleBar>
  );
};

export default MyInProgressWorkflows;
