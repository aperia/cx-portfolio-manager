import { fireEvent } from '@testing-library/dom';
import { renderWithMockStore } from 'app/utils';
import React from 'react';
import CancelOCSShellModal from '.';
describe('Manage OCS Shell > Step 3 > Cancel OCS Shell Modal', () => {
  const props = {
    id: 'id',
    show: true,
    cancelShell: {},
    onClose: jest.fn(),
    onCancelShell: jest.fn()
  };
  it('Should render component', async () => {
    const { getByText } = await renderWithMockStore(
      <CancelOCSShellModal {...props} />,
      {}
    );
    expect(getByText('txt_confirm_cancel'));
    expect(getByText('txt_manage_ocs_shells_step_4_confirm_cancel_title'));
    fireEvent.click(getByText('txt_close'));
    expect(props.onClose).toBeCalled();
    fireEvent.click(getByText('txt_confirm'));
    expect(props.onCancelShell).toBeCalled();
  });
});
