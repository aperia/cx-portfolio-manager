import ModalRegistry from 'app/components/ModalRegistry';
import {
  Button,
  ModalBody,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import React from 'react';

interface IProps {
  id: string;
  show: boolean;
  cancelShell: IOCSShell;
  onClose: () => void;
  onCancelShell: (shell: IOCSShell) => void;
}

const CancelOCSShellModal: React.FC<IProps> = ({
  id,
  show,
  cancelShell,
  onClose,
  onCancelShell
}) => {
  const { t } = useTranslation();

  return (
    <ModalRegistry
      id={id}
      show={show}
      onAutoClosedAll={onClose}
      classes={{ content: '' }}
    >
      <ModalHeader border closeButton onHide={onClose}>
        <ModalTitle>{t('txt_confirm_cancel')}</ModalTitle>
      </ModalHeader>
      <ModalBody>
        {t('txt_manage_ocs_shells_step_4_confirm_cancel_title')}
      </ModalBody>
      <div className="dls-modal-footer modal-footer">
        <Button variant="secondary" onClick={() => onClose()}>
          {t('txt_close')}
        </Button>
        <Button
          variant="danger"
          onClick={() => {
            onCancelShell(cancelShell);
          }}
        >
          {t('txt_confirm')}
        </Button>
      </div>
    </ModalRegistry>
  );
};

export default CancelOCSShellModal;
