import AttachmentItem from 'app/components/AttachmentItem';
import { MIME_TYPE } from 'app/constants/mine-type';
import { isDevice, mapGridExpandCollapse } from 'app/helpers';
import { base64toBlob, downloadBlobFile } from 'app/helpers/fileUtilities';
import { usePagination } from 'app/hooks/usePagination';
import {
  Badge,
  Button,
  ColumnType,
  Grid,
  Icon,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import { DEFAULT_PAGE_SIZE } from 'app/_libraries/_dls/components/Pagination/constants';
import { isFunction } from 'app/_libraries/_dls/lodash';
import { actionsWorkflow } from 'pages/_commons/redux/Workflow';
import { actionsToast } from 'pages/_commons/ToastNotifications/redux';
import Paging from 'pages/_commons/Utils/Paging';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { ManageOCSShellsFormValue } from '.';
import { STATUS } from './constants';
import ExpandedGrid from './ExpandedGrid';
import { mapColorBadge } from './helpers';

const Summary: React.FC<WorkflowSetupSummaryProps<ManageOCSShellsFormValue>> =
  ({ formValues, onEditStep }) => {
    const { t } = useTranslation();
    const dispatch = useDispatch();
    const { shells, originalShells, files } = formValues;
    const file = files && files[0];
    const { idx, name = '', type = '' } = file || {};
    const [updatedShells, setUpdatedShells] = useState<IOCSShell[]>([]);
    const [expandedIds, setExpandedIds] = useState<string[]>([]);
    const {
      gridData,
      sort,
      currentPage,
      currentPageSize,
      total,
      onSortChange,
      onPageChange,
      onPageSizeChange
    } = usePagination(updatedShells, undefined, 'shellName');

    const handleEdit = () => {
      isFunction(onEditStep) && onEditStep();
    };

    const handleDownloadFile = async (attachmentId: string) => {
      const result: any = await Promise.resolve(
        dispatch(actionsWorkflow.downloadAttachment(attachmentId?.toString()))
      );
      const isSuccess =
        actionsWorkflow.downloadAttachment.fulfilled.match(result);

      if (isSuccess) {
        const {
          fileStream = '',
          mimeType: mimeTypeDownload = '',
          filename: filenameDownload = ''
        } = (result?.payload || {}) as IDownloadAttachment;

        let fileName = filenameDownload;
        if (filenameDownload.split('.').length < 2) {
          const ext = MIME_TYPE[mimeTypeDownload]?.extensions[0] || '';
          fileName = `${filenameDownload}.${ext}`;
        }
        const blol = base64toBlob(fileStream, mimeTypeDownload);
        downloadBlobFile(blol, fileName);
      } else {
        dispatch(
          actionsToast.addToast({
            type: 'error',
            message: t('txt_file_failed_to_download')
          })
        );
      }
    };

    useEffect(() => {
      const _updatedShells = (shells || []).filter(
        shell => shell.status !== STATUS.NotUpdated
      );
      setUpdatedShells(_updatedShells);
    }, [shells]);

    const columns: ColumnType[] = [
      {
        id: 'shellName',
        isSort: true,
        Header: t('txt_manage_ocs_shells_step_3_shell_name'),
        accessor: 'shellName'
      },
      {
        id: 'transactionProfile',
        isSort: true,
        Header: t('txt_manage_ocs_shells_step_3_transaction_profile'),
        accessor: 'transactionProfile'
      },
      {
        id: 'employeeId',
        isSort: true,
        Header: t('txt_manage_ocs_shells_step_3_employee_id'),
        accessor: 'employeeId'
      },
      {
        id: 'shellStatus',
        isSort: true,
        Header: t('txt_manage_ocs_shells_step_3_shell_status'),
        accessor: 'shellStatus'
      },
      {
        id: 'status',
        isSort: false,
        Header: t('txt_manage_ocs_shells_step_3_status'),
        accessor: (row: MagicKeyValue) => {
          return (
            <Badge textTransformNone color={mapColorBadge(row?.status)}>
              {row?.status}
            </Badge>
          );
        }
      }
    ];

    return (
      <div className="position-relative">
        <div className="absolute-top-right mt-n26 mr-n8">
          <Button variant="outline-primary" size="sm" onClick={handleEdit}>
            {t('txt_edit')}
          </Button>
        </div>
        {total > 0 && (
          <div className="pt-16">
            <Grid
              togglable
              expandedItemKey="id"
              expandedList={expandedIds}
              onExpand={setExpandedIds}
              subRow={(rowData: any) => (
                <ExpandedGrid
                  row={rowData.original}
                  originalData={originalShells?.find(
                    (shell: any) =>
                      shell.shellName === rowData.original?.modelFromName
                  )}
                />
              )}
              data={gridData}
              columns={columns}
              toggleButtonConfigList={gridData.map(
                mapGridExpandCollapse('id', t)
              )}
              sortBy={[sort]}
              onSortChange={onSortChange}
            />
            {total > DEFAULT_PAGE_SIZE && (
              <div className="mt-16">
                <Paging
                  page={currentPage}
                  pageSize={currentPageSize}
                  totalItem={total}
                  onChangePage={onPageChange}
                  onChangePageSize={onPageSizeChange}
                />
              </div>
            )}
          </div>
        )}
        {files && files.length > 0 && (
          <div className="mt-24">
            <div className="fs-14 fw-600">
              {t('txt_manage_ocs_shells_step_4_ocs_shell_file')}
            </div>
            {idx && (
              <div className="mt-16 w-100">
                <AttachmentItem
                  id={idx}
                  mimeType={type}
                  fileName={name}
                  suffix={
                    <div className="mr-n4 d-flex">
                      <Tooltip
                        element={t('txt_download')}
                        variant={'primary'}
                        opened={isDevice ? false : undefined}
                      >
                        <Button
                          variant="icon-secondary"
                          className={'d-inline ml-8'}
                          size="sm"
                          onClick={() => handleDownloadFile(idx)}
                        >
                          <Icon
                            name="download"
                            color={'grey-l16' as any}
                            size="4x"
                          />
                        </Button>
                      </Tooltip>
                    </div>
                  }
                />
              </div>
            )}
          </div>
        )}
      </div>
    );
  };

export default Summary;
