import { STATUS } from './constants';
import isDefaultValues from './isDefaultValues';

describe('Manage OCS Shells > Configure Parameters > isDefaultValues', () => {
  it('isDefaultValues', () => {
    const defaultValues = { shells: [] };
    const formValues = {
      shells: [{ status: STATUS.NotUpdated }]
    };
    const result = isDefaultValues(formValues, defaultValues);

    expect(result).toBeTruthy();
  });
  it('isDefaultValues with undefined shells', () => {
    const defaultValues = { shells: [] };
    const formValues = {};
    const result = isDefaultValues(formValues, defaultValues);

    expect(result).toBeTruthy();
  });
});
