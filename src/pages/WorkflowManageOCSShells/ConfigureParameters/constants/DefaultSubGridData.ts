import { MetadataKey, OCSShellKey } from './enums';

export enum InputControlType {
  TextBox,
  Combobox,
  DropdownList,
  DatePicker,
  Password,
  MultiSelect
}
export interface ISubGridData {
  id: string;
  fieldName?: string;
  greenScreenName?: string;
  previousValue?: string | string[];
  value?: string | string[];
  moreInfo?: string;
  inputControlType?: InputControlType;
  inputOptions?: any;
  keyOptions?: MetadataKey;
  onChange?: (value: any) => void;
}

const DefaultSubGridData = (
  rowData?: IOCSShell,
  t?: any,
  originalData?: IOCSShell
) => {
  const row = rowData || {};
  const original = originalData || {};

  const data: ISubGridData[] = [
    {
      id: OCSShellKey.ShellName,
      fieldName: t('txt_manage_ocs_shells_step_3_shell_name'),
      greenScreenName: t('txt_manage_ocs_shells_step_3_shell_name'),
      previousValue: original[OCSShellKey.ShellName],
      value: row[OCSShellKey.ShellName],
      moreInfo: t('txt_manage_ocs_shells_step_3_shell_name_more_info'),
      inputControlType: InputControlType.TextBox,
      inputOptions: {
        maxLength: 8,
        upperCase: true,
        trimStart: true
      }
    },
    {
      id: OCSShellKey.TransactionProfile,
      fieldName: t('txt_manage_ocs_shells_step_3_transaction_profile'),
      greenScreenName: t(
        'txt_manage_ocs_shells_step_3_transaction_profile_green_name'
      ),
      previousValue: original[OCSShellKey.TransactionProfile],
      value: row[OCSShellKey.TransactionProfile],
      moreInfo: t('txt_manage_ocs_shells_step_3_transaction_profile_more_info'),
      inputControlType: InputControlType.Combobox,
      keyOptions: MetadataKey.TransactionProfile
    },
    {
      id: OCSShellKey.FieldProfile,
      fieldName: t('txt_manage_ocs_shells_step_3_field_profile'),
      greenScreenName: t(
        'txt_manage_ocs_shells_step_3_field_profile_green_name'
      ),
      previousValue: original[OCSShellKey.FieldProfile],
      value: row[OCSShellKey.FieldProfile],
      moreInfo: t('txt_manage_ocs_shells_step_3_field_profile_more_info'),
      inputControlType: InputControlType.Combobox,
      keyOptions: MetadataKey.FieldProfile
    },
    {
      id: OCSShellKey.SysPrinProfile,
      fieldName: t('txt_manage_ocs_shells_step_3_sys_prin_profile'),
      greenScreenName: t(
        'txt_manage_ocs_shells_step_3_sys_prin_profile_green_name'
      ),
      previousValue: original[OCSShellKey.SysPrinProfile],
      value: row[OCSShellKey.SysPrinProfile],
      moreInfo: t('txt_manage_ocs_shells_step_3_sys_prin_profile_more_info'),
      inputControlType: InputControlType.Combobox,
      keyOptions: MetadataKey.SysPrinProfile
    },
    {
      id: OCSShellKey.Description,
      fieldName: t('txt_manage_ocs_shells_step_3_description'),
      greenScreenName: t('txt_manage_ocs_shells_step_3_description'),
      previousValue: original[OCSShellKey.Description],
      value: row[OCSShellKey.Description],
      moreInfo: t('txt_manage_ocs_shells_step_3_description_more_info'),
      inputControlType: InputControlType.TextBox,
      inputOptions: {
        maxLength: 30
      }
    },
    {
      id: OCSShellKey.AccountProfile,
      fieldName: t('txt_manage_ocs_shells_step_3_account_profile'),
      greenScreenName: t(
        'txt_manage_ocs_shells_step_3_account_profile_screen_name'
      ),
      previousValue: original[OCSShellKey.AccountProfile],
      value: row[OCSShellKey.AccountProfile],
      moreInfo: t('txt_manage_ocs_shells_step_3_account_profile_more_info'),
      inputControlType: InputControlType.Combobox,
      keyOptions: MetadataKey.AccountProfile
    },
    {
      id: OCSShellKey.OperatorCode,
      fieldName: t('txt_manage_ocs_shells_step_3_operator_code'),
      greenScreenName: t(
        'txt_manage_ocs_shells_step_3_operator_code_green_name'
      ),
      previousValue: original[OCSShellKey.OperatorCode],
      value: row[OCSShellKey.OperatorCode],
      moreInfo: t('txt_manage_ocs_shells_step_3_operator_code_more_info'),
      inputControlType: InputControlType.Combobox,
      keyOptions: MetadataKey.OperatorCode
    },
    {
      id: OCSShellKey.SingleSession,
      fieldName: t('txt_manage_ocs_shells_step_3_single_session'),
      greenScreenName: t('txt_manage_ocs_shells_step_3_single_session'),
      previousValue: original[OCSShellKey.SingleSession],
      value: row[OCSShellKey.SingleSession],
      moreInfo: t('txt_manage_ocs_shells_step_3_single_session_more_info'),
      inputControlType: InputControlType.DropdownList,
      keyOptions: MetadataKey.SingleSession
    },
    {
      id: OCSShellKey.EmployeeId,
      fieldName: t('txt_manage_ocs_shells_step_3_employee_id'),
      greenScreenName: t('txt_manage_ocs_shells_step_3_employee_id_green_name'),
      previousValue: original[OCSShellKey.EmployeeId],
      value: row[OCSShellKey.EmployeeId],
      moreInfo: t('txt_manage_ocs_shells_step_3_employee_id_more_info'),
      inputControlType: InputControlType.Combobox,
      keyOptions: MetadataKey.EmployeeId
    },
    {
      id: OCSShellKey.SupervisorShell,
      fieldName: t('txt_manage_ocs_shells_step_3_supervisor_shell'),
      greenScreenName: t('txt_manage_ocs_shells_step_3_supervisor_shell_green'),
      previousValue: original[OCSShellKey.SupervisorShell],
      value: row[OCSShellKey.SupervisorShell],
      moreInfo: t('txt_manage_ocs_shells_step_3_supervisor_shell_more_info'),
      inputControlType: InputControlType.Combobox,
      keyOptions: MetadataKey.SupervisorShell
    },
    {
      id: OCSShellKey.ReportManagementSystemId,
      fieldName: t('txt_manage_ocs_shells_step_3_report_management_system_id'),
      greenScreenName: t(
        'txt_manage_ocs_shells_step_3_report_management_system_id_green_name'
      ),
      previousValue: original[OCSShellKey.ReportManagementSystemId],
      value: row[OCSShellKey.ReportManagementSystemId],
      moreInfo: t(
        'txt_manage_ocs_shells_step_3_report_management_system_id_more_info'
      ),
      inputControlType: InputControlType.Combobox,
      keyOptions: MetadataKey.RMSId
    },
    {
      id: OCSShellKey.DepartmentId,
      fieldName: t('txt_manage_ocs_shells_step_3_department_id'),
      greenScreenName: t(
        'txt_manage_ocs_shells_step_3_department_id_green_name'
      ),
      previousValue: original[OCSShellKey.DepartmentId],
      value: row[OCSShellKey.DepartmentId],
      moreInfo: t('txt_manage_ocs_shells_step_3_department_id_more_info'),
      inputControlType: InputControlType.Combobox,
      keyOptions: MetadataKey.Department
    },
    {
      id: OCSShellKey.Level,
      fieldName: t('txt_manage_ocs_shells_step_3_level'),
      greenScreenName: t('txt_manage_ocs_shells_step_3_level'),
      previousValue: original[OCSShellKey.Level],
      value: row[OCSShellKey.Level],
      moreInfo: t('txt_manage_ocs_shells_step_3_level_more_info'),
      inputControlType: InputControlType.DropdownList,
      keyOptions: MetadataKey.Level
    },
    {
      id: OCSShellKey.MasterSignOnOption,
      fieldName: t('txt_manage_ocs_shells_step_3_master_sign_on_option'),
      greenScreenName: t(
        'txt_manage_ocs_shells_step_3_master_sign_on_option_screen_name'
      ),
      previousValue: original[OCSShellKey.MasterSignOnOption],
      value: row[OCSShellKey.MasterSignOnOption],
      moreInfo: t(
        'txt_manage_ocs_shells_step_3_master_sign_on_option_more_info'
      ),
      inputControlType: InputControlType.DropdownList,
      keyOptions: MetadataKey.MasterSignOnOption
    },
    {
      id: OCSShellKey.PwdExpirationOption,
      fieldName: t('txt_manage_ocs_shells_step_3_password_expiration_option'),
      greenScreenName: t(
        'txt_manage_ocs_shells_step_3_password_expiration_option_green_name'
      ),
      previousValue: original[OCSShellKey.PwdExpirationOption],
      value: row[OCSShellKey.PwdExpirationOption],
      moreInfo: t(
        'txt_manage_ocs_shells_step_3_password_expiration_option_more_info'
      ),
      inputControlType: InputControlType.DropdownList,
      keyOptions: MetadataKey.PasswordExpirationOption
    },
    {
      id: OCSShellKey.ExpireDate,
      fieldName: t('txt_manage_ocs_shells_step_3_expire_date'),
      greenScreenName: t('txt_manage_ocs_shells_step_3_expire_date'),
      previousValue: original[OCSShellKey.ExpireDate],
      value: row[OCSShellKey.ExpireDate],
      moreInfo: t('txt_manage_ocs_shells_step_3_expire_date_more_info'),
      inputControlType: InputControlType.DatePicker
    },
    {
      id: OCSShellKey.NeverDisable,
      fieldName: t('txt_manage_ocs_shells_step_3_never_disable'),
      greenScreenName: t('txt_manage_ocs_shells_step_3_never_disable'),
      previousValue: original[OCSShellKey.NeverDisable],
      value: row[OCSShellKey.NeverDisable],
      moreInfo: t('txt_manage_ocs_shells_step_3_never_disable_more_info'),
      inputControlType: InputControlType.DropdownList,
      keyOptions: MetadataKey.NeverDisable
    },
    {
      id: OCSShellKey.Comment,
      fieldName: t('txt_manage_ocs_shells_step_3_comment'),
      greenScreenName: t('txt_manage_ocs_shells_step_3_comment'),
      previousValue: original[OCSShellKey.Comment],
      value: row[OCSShellKey.Comment],
      moreInfo: t('txt_manage_ocs_shells_step_3_comment_more_info'),
      inputControlType: InputControlType.TextBox,
      inputOptions: {
        maxLength: 30
      }
    },
    {
      id: OCSShellKey.InitialPwd,
      fieldName: t('txt_manage_ocs_shells_step_3_initial_password'),
      greenScreenName: t('txt_manage_ocs_shells_step_3_initial_password'),
      previousValue: original[OCSShellKey.InitialPwd],
      value: row[OCSShellKey.InitialPwd],
      moreInfo: t('txt_manage_ocs_shells_step_3_initial_password_more_info'),
      inputControlType: InputControlType.Password,
      inputOptions: {
        maxLength: 8
      }
    },
    {
      id: OCSShellKey.ConfirmPwd,
      fieldName: t('txt_manage_ocs_shells_step_3_confirm_password'),
      greenScreenName: t('txt_manage_ocs_shells_step_3_confirm_password'),
      previousValue: original[OCSShellKey.ConfirmPwd],
      value: row[OCSShellKey.ConfirmPwd],
      moreInfo: t('txt_manage_ocs_shells_step_3_confirm_password_more_info'),
      inputControlType: InputControlType.Password,
      inputOptions: {
        maxLength: 8,
        disabled: !row['initialPwd']
      }
    },
    {
      id: OCSShellKey.ShellStatus,
      fieldName: t('txt_manage_ocs_shells_step_3_shell_status'),
      greenScreenName: t(
        'txt_manage_ocs_shells_step_3_shell_status_green_name'
      ),
      previousValue: original[OCSShellKey.ShellStatus],
      value: row[OCSShellKey.ShellStatus],
      moreInfo: t('txt_manage_ocs_shells_step_3_shell_status_more_info'),
      inputControlType: InputControlType.DropdownList,
      keyOptions: MetadataKey.Status,
      inputOptions: {
        disabled: true
      }
    },
    {
      id: OCSShellKey.DaysAllowed,
      fieldName: t('txt_manage_ocs_shells_step_3_days_allowed'),
      greenScreenName: t('txt_manage_ocs_shells_step_3_days_allowed'),
      previousValue: original[OCSShellKey.DaysAllowed],
      value: row[OCSShellKey.DaysAllowed],
      moreInfo: t('txt_manage_ocs_shells_step_3_days_allowed_more_info'),
      inputControlType: InputControlType.MultiSelect,
      keyOptions: MetadataKey.DaysAllowed
    },
    {
      id: OCSShellKey.TimesAllowed,
      fieldName: t('txt_manage_ocs_shells_step_3_times_allowed'),
      greenScreenName: t('txt_manage_ocs_shells_step_3_times_allowed'),
      previousValue: original[OCSShellKey.TimesAllowed],
      value: row[OCSShellKey.TimesAllowed],
      moreInfo: t('txt_manage_ocs_shells_step_3_times_allowed_more_info'),
      inputControlType: InputControlType.MultiSelect,
      keyOptions: MetadataKey.TimesAllowed
    }
  ];

  return data;
};

export default DefaultSubGridData;
