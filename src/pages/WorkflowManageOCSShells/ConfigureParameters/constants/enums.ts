export enum STATUS {
  New = 'New',
  Updated = 'Updated',
  NotUpdated = 'Not Updated',
  MarkedForDeletion = 'Marked for Deletion',
  MarkedForRemoval = 'Marked for Removal'
}

export enum MetadataKey {
  TransactionProfile = 'transaction.profile',
  FieldProfile = 'field.profile',
  SysPrinProfile = 'sys.prin.profile',
  AccountProfile = 'account.profile',
  OperatorCode = 'operator.code',
  SingleSession = 'single.session',
  EmployeeId = 'employee.id',
  SupervisorShell = 'supervisor.shell',
  RMSId = 'report.management.system.id',
  Department = 'department.id',
  Level = 'level',
  MasterSignOnOption = 'master.sign.on.option',
  PasswordExpirationOption = 'password.expiration.option',
  NeverDisable = 'never.disable',
  Status = 'shell.status',
  DaysAllowed = 'days.allowed',
  TimesAllowed = 'times.allowed'
}

export enum OCSShellKey {
  ShellName = 'shellName',
  TransactionProfile = 'transactionProfile',
  FieldProfile = 'fieldProfile',
  SysPrinProfile = 'sysPrinProfile',
  Description = 'description',
  AccountProfile = 'accountProfile',
  OperatorCode = 'operatorCode',
  SingleSession = 'singleSession',
  EmployeeId = 'employeeId',
  SupervisorShell = 'supervisorShell',
  ReportManagementSystemId = 'reportManagementSystemId',
  DepartmentId = 'departmentId',
  Level = 'level',
  MasterSignOnOption = 'masterSignOnOption',
  PwdExpirationOption = 'pwdExpirationOption',
  ExpireDate = 'expireDate',
  NeverDisable = 'neverDisable',
  Comment = 'comment',
  InitialPwd = 'initialPwd',
  ConfirmPwd = 'confirmPwd',
  ShellStatus = 'shellStatus',
  DaysAllowed = 'daysAllowed',
  TimesAllowed = 'timesAllowed'
}

export const OCSShellKeyToParameter: Record<string, string> = {
  shellName: 'shell.name',
  transactionProfile: 'transaction.profile',
  fieldProfile: 'field.profile',
  sysPrinProfile: 'sys.prin.profile',
  description: 'description',
  accountProfile: 'account.profile',
  operatorCode: 'operator.code',
  singleSession: 'single.session',
  employeeId: 'employee.id',
  supervisorShell: 'supervisor.shell',
  reportManagementSystemId: 'report.management.system.id',
  departmentId: 'department.id',
  level: 'level',
  masterSignOnOption: 'master.sign.on.option',
  pwdExpirationOption: 'password.expiration.option',
  expireDate: 'expire.date',
  neverDisable: 'never.disable',
  comment: 'comment',
  initialPwd: 'initial.password',
  confirmPwd: 'confirm.password',
  shellStatus: 'shell.status',
  daysAllowed: 'days.allowed',
  timesAllowed: 'times.allowed'
};
