import { TruncateText } from 'app/_libraries/_dls';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React from 'react';
import { ISubGridData } from './DefaultSubGridData';

export const truncateAccessor = (
  rowData: MagicKeyValue,
  field: keyof ISubGridData | string,
  t: any
) => {
  const value = rowData[field];
  return (
    <TruncateText
      resizable
      lines={2}
      ellipsisLessText={t('txt_less')}
      ellipsisMoreText={t('txt_more')}
    >
      {value}
    </TruncateText>
  );
};
const DefaultSubGridColumns = (
  accessor: (
    rowData: ISubGridData,
    field: keyof ISubGridData,
    t: any
  ) => JSX.Element | undefined,
  t: any,
  width: number,
  hasControl: boolean
) => {
  return [
    {
      id: 'fieldName',
      Header: t('txt_manage_ocs_shells_step_3_business_name'),
      accessor: (rowData: ISubGridData) =>
        truncateAccessor(rowData, 'fieldName', t),
      isSort: false,
      cellBodyProps: { className: hasControl ? 'pt-12 pb-8' : '' }
    },
    {
      id: 'greenScreenName',
      Header: t('txt_manage_ocs_shells_step_3_green_screen_name'),
      accessor: (rowData: ISubGridData) =>
        truncateAccessor(rowData, 'greenScreenName', t),
      isSort: false,
      cellBodyProps: { className: hasControl ? 'pt-12 pb-8' : '' }
    },
    {
      id: 'previousValue',
      Header: t('txt_manage_ocs_shells_step_3_previous_value'),
      accessor: (rowData: ISubGridData) =>
        accessor(rowData, 'previousValue', t),
      isSort: false,
      cellBodyProps: { className: hasControl ? 'pt-12 pb-8' : '' }
    },
    {
      id: 'newValue',
      Header: t('txt_manage_ocs_shells_step_3_new_value'),
      accessor: (rowData: ISubGridData) => accessor(rowData, 'value', t),
      isSort: false,
      cellBodyProps: { className: hasControl ? 'pt-12 pb-8' : '' }
    },
    {
      id: 'value',
      Header: t('txt_manage_ocs_shells_step_3_value'),
      accessor: (rowData: ISubGridData) => accessor(rowData, 'value', t),
      isSort: false,
      width: width < 1280 ? 280 : undefined,
      cellBodyProps: { className: hasControl ? 'py-8' : '' }
    },
    {
      id: 'moreInfo',
      Header: t('txt_manage_ocs_shells_step_3_more_info'),
      className: 'text-center',
      accessor: viewMoreInfo(['moreInfo'], t),
      width: 105,
      isSort: false,
      cellBodyProps: { className: hasControl ? 'pt-12 pb-8' : '' }
    }
  ];
};

export default DefaultSubGridColumns;
