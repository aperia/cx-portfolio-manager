import { InlineMessage, useTranslation } from 'app/_libraries/_dls';
import { useSelectOCSShells } from 'pages/_commons/redux/WorkflowSetup';
import { StateFileProp } from 'pages/_commons/WorkflowSetup/CommonSteps/_components/UploadFile';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, { useEffect } from 'react';
import { STATUS } from './constants';
import isDefaultValues from './isDefaultValues';
import OCSShellList from './OCSShellList';
import parseFormValues from './parseFormValues';
import Summary from './Summary';
import UploadSection from './UploadSection';

export interface ManageOCSShellsFormValue {
  isValid?: boolean;
  shells?: IOCSShell[];
  attachmentId?: string;
  originalShells?: IOCSShell[];
  files?: StateFileProp[];
}

const ManageOCSShells: React.FC<WorkflowSetupProps<ManageOCSShellsFormValue>> =
  props => {
    const { t } = useTranslation();
    const { formValues, setFormValues, stepId, selectedStep, isStuck } = props;
    const { attachmentId, shells } = formValues;
    const ocsShellsData = useSelectOCSShells();
    const { loading, ocsShells = [] } = ocsShellsData || {};

    useEffect(() => {
      if (stepId !== selectedStep || loading) {
        return;
      }

      const updatedShells = (shells || []).filter(
        shell => shell.status !== STATUS.NotUpdated
      );
      if (!attachmentId && updatedShells.length === 0) {
        setFormValues({ isValid: false });
      } else {
        setFormValues({ isValid: true, originalShells: ocsShells });
      }
    }, [
      loading,
      selectedStep,
      stepId,
      attachmentId,
      shells,
      ocsShells,
      setFormValues
    ]);

    return (
      <>
        {isStuck && (
          <InlineMessage className="mb-0 mt-24" variant="danger" withIcon>
            {t('txt_step_stuck_move_forward_message')}
          </InlineMessage>
        )}
        <div className="my-24">
          <OCSShellList {...props} />
        </div>
        <div className="border-top">
          <UploadSection {...props} />
        </div>
      </>
    );
  };

const ExtraStaticManageOCSShellsStep =
  ManageOCSShells as WorkflowSetupStaticProp<ManageOCSShellsFormValue>;

ExtraStaticManageOCSShellsStep.summaryComponent = Summary;
ExtraStaticManageOCSShellsStep.isDefaultValues = isDefaultValues;
ExtraStaticManageOCSShellsStep.defaultValues = {
  isValid: false,
  shells: [],
  originalShells: [],
  files: [],
  attachmentId: undefined
};

ExtraStaticManageOCSShellsStep.parseFormValues = parseFormValues;

export default ExtraStaticManageOCSShellsStep;
