import ModalRegistry from 'app/components/ModalRegistry';
import {
  Button,
  ModalBody,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import React from 'react';

interface IProps {
  id: string;
  show: boolean;
  isBulk: boolean;
  deleteShell?: IOCSShell;
  onClose: () => void;
  onDelete: (shell?: IOCSShell) => void;
}

const RemoveOCSShellModal: React.FC<IProps> = ({
  id,
  show,
  isBulk,
  onClose,
  onDelete,
  deleteShell
}) => {
  const { t } = useTranslation();

  return (
    <ModalRegistry
      id={id}
      show={show}
      onAutoClosedAll={onClose}
      classes={{ content: '' }}
    >
      <ModalHeader border closeButton onHide={onClose}>
        <ModalTitle>{t('txt_confirm_deletion')}</ModalTitle>
      </ModalHeader>
      <ModalBody>
        {isBulk
          ? t('txt_manage_ocs_shells_step_4_delete_shells_title')
          : t('txt_manage_ocs_shells_step_4_delete_shell_title')}
      </ModalBody>
      <div className="dls-modal-footer modal-footer">
        <Button variant="secondary" onClick={() => onClose()}>
          {t('txt_cancel')}
        </Button>
        <Button
          variant="danger"
          onClick={() => {
            deleteShell === undefined ? onDelete() : onDelete(deleteShell);
          }}
        >
          {t('txt_delete')}
        </Button>
      </div>
    </ModalRegistry>
  );
};

export default RemoveOCSShellModal;
