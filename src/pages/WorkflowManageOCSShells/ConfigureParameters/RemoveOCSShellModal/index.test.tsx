import { fireEvent } from '@testing-library/dom';
import { renderWithMockStore } from 'app/utils';
import React from 'react';
import RemoveOCSShellModal from '.';
describe('Manage OCS Shell > Step 3 > Remove OCS Shell Modal', () => {
  it('Should render component Bulk delete', async () => {
    const props = {
      id: 'id',
      show: true,
      isBulk: true,
      onClose: jest.fn(),
      onDelete: jest.fn(),
      deleteShell: undefined
    };
    const { getByText } = await renderWithMockStore(
      <RemoveOCSShellModal {...props} />,
      {}
    );
    expect(getByText('txt_confirm_deletion'));
    expect(getByText('txt_manage_ocs_shells_step_4_delete_shells_title'));
    fireEvent.click(getByText('txt_cancel'));
    expect(props.onClose).toBeCalled();
    fireEvent.click(getByText('txt_delete'));
    expect(props.onDelete).toBeCalled();
  });

  it('Should render component Single delete', async () => {
    const props = {
      id: 'id',
      show: true,
      isBulk: false,
      onClose: jest.fn(),
      onDelete: jest.fn(),
      deleteShell: {}
    };
    const { getByText } = await renderWithMockStore(
      <RemoveOCSShellModal {...props} />,
      {}
    );
    expect(getByText('txt_confirm_deletion'));
    expect(getByText('txt_manage_ocs_shells_step_4_delete_shell_title'));
    fireEvent.click(getByText('txt_cancel'));
    expect(props.onClose).toBeCalled();
    fireEvent.click(getByText('txt_delete'));
    expect(props.onDelete).toBeCalled();
  });
});
