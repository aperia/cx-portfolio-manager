import { render } from '@testing-library/react';
import React from 'react';
import UploadSection from '.';
import { ManageOCSShellsFormValue } from '..';

jest.mock(
  'pages/_commons/WorkflowSetup/CommonSteps/_components/DownloadFile',
  () => ({
    __esModule: true,
    default: () => <div>Download File Component</div>
  })
);

jest.mock(
  'pages/_commons/WorkflowSetup/CommonSteps/_components/UploadFile',
  () => ({
    __esModule: true,
    default: () => <div>Upload File Component</div>
  })
);

describe('Manage OCS Shells > ConfigureParameters > Upload Section', () => {
  const renderComponent = (
    props: WorkflowSetupProps<ManageOCSShellsFormValue>
  ) => {
    return render(<UploadSection {...props} />);
  };

  it('Should render component', () => {
    const props = {} as any;
    const { getByText } = renderComponent(props);
    expect(
      getByText('txt_manage_ocs_shells_step_3_upload_ocs_shell_file')
    ).toBeInTheDocument();
    expect(getByText('Download File Component')).toBeInTheDocument();
    expect(getByText('Upload File Component')).toBeInTheDocument();
  });
});
