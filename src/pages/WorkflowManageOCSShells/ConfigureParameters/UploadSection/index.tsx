import { useTranslation } from 'app/_libraries/_dls';
import DownloadFile from 'pages/_commons/WorkflowSetup/CommonSteps/_components/DownloadFile';
import UploadFile from 'pages/_commons/WorkflowSetup/CommonSteps/_components/UploadFile';
import React from 'react';
import { ManageOCSShellsFormValue } from '..';

const UploadSection: React.FC<WorkflowSetupProps<ManageOCSShellsFormValue>> = ({
  stepId,
  selectedStep,
  formValues,
  dependencies,
  setFormValues,
  savedAt,
  ready,
  handleSave
}) => {
  const { t } = useTranslation();
  return (
    <div className="pt-24">
      <h5 className="mb-16">
        {t('txt_manage_ocs_shells_step_3_upload_ocs_shell_file')}
      </h5>
      <DownloadFile
        downloadTemplateType={'manageOCSShells'}
        className="border-bottom-dashed"
      />
      <UploadFile
        stepId={stepId}
        selectedStep={selectedStep}
        formValues={formValues}
        dependencies={dependencies}
        setFormValues={setFormValues}
        savedAt={savedAt}
        ready={ready}
        handleSave={handleSave}
      />
    </div>
  );
};

export default UploadSection;
