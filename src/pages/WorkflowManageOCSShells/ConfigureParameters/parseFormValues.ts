import { getWorkflowSetupStepStatus } from 'app/helpers';
import { workflowTemplateService } from 'app/services';
import { get } from 'lodash';
import { VERIFIED_STATUSES_RESPONSE } from 'pages/_commons/WorkflowSetup/CommonSteps/_components/UploadFile/constants';
import { getStatusFileFromValidationMessages } from 'pages/_commons/WorkflowSetup/CommonSteps/_components/UploadFile/helpers';
import { convertShellsParamToProp } from './helpers';

const parseFormValues: WorkflowSetupStepFormDataFunc<any> = async (
  data,
  id
) => {
  const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);
  if (!stepInfo) return;
  const configuration = data?.data?.configurations || {};
  const { attachmentId, ocsShells = [] } = configuration as any;

  let file, status;
  if (attachmentId) {
    const attachmentIds = [attachmentId + ''];
    const response = await workflowTemplateService.getFileDetails({
      searchFields: { attachmentIds }
    });
    const { validationMessages, ...attachment } = get(
      response,
      'data.attachments.0',
      {}
    );
    const successResponse = VERIFIED_STATUSES_RESPONSE.includes(
      attachment?.status?.toLowerCase()
    );

    status = getStatusFileFromValidationMessages(validationMessages);
    file = attachment && {
      ...attachment,
      idx: attachment.id,
      name: attachment.filename,
      size: attachment.fileSize,
      type: attachment.mineType,
      percentage: successResponse ? 100 : 98,
      status
    };
  }

  const shells = convertShellsParamToProp(ocsShells);
  const isValid = !!attachmentId || ocsShells.length > 0;

  return {
    ...getWorkflowSetupStepStatus(stepInfo),
    attachmentId,
    files: !!file ? [file] : [],
    shells,
    isValid
  };
};

export default parseFormValues;
