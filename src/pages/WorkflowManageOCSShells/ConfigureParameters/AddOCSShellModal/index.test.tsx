import { fireEvent } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';
import { ISimpleSearchProps } from 'app/components/SimpleSearch';
import { renderWithMockStore } from 'app/utils';
import React from 'react';
import { act } from 'react-dom/test-utils';
import AddOCSShellModal, { IProps } from '.';

jest.mock('app/components/SimpleSearch', () => {
  return {
    __esModule: true,
    default: ({
      defaultValue = '',
      placeholder = '',
      maxLength = 50,
      className = 'simple-search',
      clearTooltip = 'Clear Search Criteria',
      popperElement,
      onSearch
    }: ISimpleSearchProps) => {
      return (
        <div>
          <div>Simple Search</div>
          <input />
          <div onClick={() => onSearch('sadio')}>Search Sadio</div>
          <div onClick={() => onSearch('shell')}>Search Shell</div>
        </div>
      );
    }
  };
});

describe('Manage OCS Shells > Configure Parameters > Add OCS Shell Modal', () => {
  const mockMetadata = [
    {
      name: 'transaction.profile',
      options: [
        {
          value: 'PROFILE1',
          description: '',
          selected: null
        },
        {
          value: 'PROFILE2',
          description: '',
          selected: null
        },
        {
          value: 'PROFILE3',
          description: '',
          selected: null
        },
        {
          value: 'PROFILE4',
          description: '',
          selected: null
        }
      ]
    },
    {
      name: 'field.profile',
      options: [
        {
          value: 'FLDPRO01',
          description: '',
          selected: null
        },
        {
          value: 'FLDPRO02',
          description: '',
          selected: null
        },
        {
          value: 'FLDPRO03',
          description: '',
          selected: null
        },
        {
          value: 'FLDPRO04',
          description: '',
          selected: null
        }
      ]
    },
    {
      name: 'sys.prin.profile',
      options: [
        {
          value: '00200001',
          description: '',
          selected: null
        },
        {
          value: '00200002',
          description: '',
          selected: null
        },
        {
          value: '00200003',
          description: '',
          selected: null
        },
        {
          value: '00200004',
          description: '',
          selected: null
        }
      ]
    },
    {
      name: 'account.profile',
      options: [
        {
          value: '*DEFAULT',
          description: '',
          selected: 'true'
        },
        {
          value: 'ACCTPRO1',
          description: '',
          selected: null
        },
        {
          value: 'ACCTPRO2',
          description: '',
          selected: null
        },
        {
          value: 'ACCTPRO3',
          description: '',
          selected: null
        },
        {
          value: 'ACCTPRO4',
          description: '',
          selected: null
        }
      ]
    },
    {
      name: 'operator.code',
      options: [
        {
          value: '214',
          description: '',
          selected: null
        },
        {
          value: '418',
          description: '',
          selected: null
        },
        {
          value: '357',
          description: '',
          selected: null
        },
        {
          value: '573',
          description: '',
          selected: null
        }
      ]
    },
    {
      name: 'single.session',
      options: [
        {
          value: '',
          description: 'None',
          selected: null
        },
        {
          value: 'N',
          description:
            'The shell can sign on to more than one terminal at a time.',
          selected: null
        },
        {
          value: 'Y',
          description: 'The shell can only sign on to one terminal at a time.',
          selected: 'true'
        }
      ]
    },
    {
      name: 'employee.id',
      options: [
        {
          value: 'EMPLOYEE1',
          description: '',
          selected: null
        },
        {
          value: 'EMPLOYEE2',
          description: '',
          selected: null
        },
        {
          value: 'EMPLOYEE3',
          description: '',
          selected: null
        },
        {
          value: 'EMPLOYEE4',
          description: '',
          selected: null
        }
      ]
    },
    {
      name: 'supervisor.shell',
      options: [
        {
          value: 'SHELL001',
          description: '',
          selected: null
        },
        {
          value: 'SHELL002',
          description: '',
          selected: null
        },
        {
          value: 'SHELL003',
          description: '',
          selected: null
        },
        {
          value: 'SHELL004',
          description: '',
          selected: null
        }
      ]
    },
    {
      name: 'report.management.system.id',
      options: [
        {
          value: 'RMSID001',
          description: '',
          selected: null
        },
        {
          value: 'RMSID002',
          description: '',
          selected: null
        },
        {
          value: 'RMSID003',
          description: '',
          selected: null
        },
        {
          value: 'RMSID004',
          description: '',
          selected: null
        }
      ]
    },
    {
      name: 'department.id',
      options: [
        {
          value: 'DEPTID01',
          description: '',
          selected: null
        },
        {
          value: 'DEPTID02',
          description: '',
          selected: null
        },
        {
          value: 'DEPTID03',
          description: '',
          selected: null
        },
        {
          value: 'DEPTID04',
          description: '',
          selected: null
        }
      ]
    },
    {
      name: 'level',
      options: [
        {
          value: '',
          description: 'None',
          selected: null
        },
        {
          value: '01',
          description: 'Operator',
          selected: null
        },
        {
          value: '02',
          description: 'Supervisor',
          selected: null
        },
        {
          value: '03',
          description: 'Manager or department',
          selected: null
        }
      ]
    },
    {
      name: 'master.sign.on.option',
      options: [
        {
          value: '',
          description: 'None',
          selected: null
        },
        {
          value: 'N',
          description:
            'No, the shell does not have master sign-on capabilities.',
          selected: 'true'
        },
        {
          value: 'Y',
          description: 'Yes, the shell has master sign-on capabilities.',
          selected: null
        }
      ]
    },
    {
      name: 'password.expiration.option',
      options: [
        {
          value: '',
          description: 'None',
          selected: null
        },
        {
          value: 'N',
          description: 'No, the password expires.',
          selected: 'true'
        },
        {
          value: 'Y',
          description: 'Yes, the password never expires.',
          selected: null
        }
      ]
    },
    {
      name: 'never.disable',
      options: [
        {
          value: '',
          description: 'None',
          selected: null
        },
        {
          value: 'N',
          description:
            'No, the System disables the shell after three invalid sign-on attempts.',
          selected: 'true'
        },
        {
          value: 'Y',
          description:
            'Yes, the System does not disable shell after three invalid sign-on attempts.',
          selected: null
        }
      ]
    },
    {
      name: 'never.disable',
      options: [
        {
          value: '',
          description: 'None',
          selected: null
        },
        {
          value: 'N',
          description:
            'No, the System disables the shell after three invalid sign-on attempts.',
          selected: 'true'
        },
        {
          value: 'Y',
          description:
            'Yes, the System does not disable shell after three invalid sign-on attempts.',
          selected: null
        }
      ]
    },
    {
      name: 'days.allowed',
      options: [
        {
          value: 'Sunday',
          description: '',
          selected: null
        },
        {
          value: 'Monday',
          description: '',
          selected: null
        },
        {
          value: 'Tuesday',
          description: '',
          selected: null
        },
        {
          value: 'Wednesday',
          description: '',
          selected: null
        },
        {
          value: 'Thursday',
          description: '',
          selected: null
        },
        {
          value: 'Friday',
          description: '',
          selected: null
        },
        {
          value: 'Saturday',
          description: '',
          selected: null
        }
      ]
    },
    {
      name: 'times.allowed',
      options: [
        {
          value: '12:00 AM - 12:30 AM',
          description: '',
          selected: null
        },
        {
          value: '12:30 AM - 1:00 AM',
          description: '',
          selected: null
        },
        {
          value: '1:00 AM - 1:30 AM',
          description: '',
          selected: null
        },
        {
          value: '1:30 AM - 2:00 AM',
          description: '',
          selected: null
        },
        {
          value: '2:00 AM - 2:30 AM',
          description: '',
          selected: null
        },
        {
          value: '2:30 AM - 3:00 AM',
          description: '',
          selected: null
        },
        {
          value: '3:00 AM - 3:30 AM',
          description: '',
          selected: null
        },
        {
          value: '3:30 AM - 4:00 AM',
          description: '',
          selected: null
        },
        {
          value: '4:00 AM - 4:30 AM',
          description: '',
          selected: null
        },
        {
          value: '4:30 AM - 5:00 AM',
          description: '',
          selected: null
        },
        {
          value: '5:00 AM - 5:30 AM',
          description: '',
          selected: null
        },
        {
          value: '5:30 AM - 6:00 AM',
          description: '',
          selected: null
        },
        {
          value: '6:00 AM - 6:30 AM',
          description: '',
          selected: null
        },
        {
          value: '6:30 AM - 7:00 AM',
          description: '',
          selected: null
        },
        {
          value: '7:00 AM - 7:30 AM',
          description: '',
          selected: null
        },
        {
          value: '7:30 AM - 8:00 AM',
          description: '',
          selected: null
        },
        {
          value: '8:00 AM - 8:30 AM',
          description: '',
          selected: null
        },
        {
          value: '8:30 AM - 9:00 AM',
          description: '',
          selected: null
        },
        {
          value: '9:00 AM - 9:30 AM',
          description: '',
          selected: null
        },
        {
          value: '9:30 AM - 10:00 AM',
          description: '',
          selected: null
        },
        {
          value: '10:00 AM - 10:30 AM',
          description: '',
          selected: null
        },
        {
          value: '10:30 AM - 11:00 AM',
          description: '',
          selected: null
        },
        {
          value: '11:00 AM - 11:30 AM',
          description: '',
          selected: null
        },
        {
          value: '11:30 AM - 12:00 PM',
          description: '',
          selected: null
        },
        {
          value: '12:00 PM - 12:30 PM',
          description: '',
          selected: null
        },
        {
          value: '12:30 PM - 1:00 PM',
          description: '',
          selected: null
        },
        {
          value: '1:00 PM - 1:30 PM',
          description: '',
          selected: null
        },
        {
          value: '1:30 PM - 2:00 PM',
          description: '',
          selected: null
        },
        {
          value: '2:00 PM - 2:30 PM',
          description: '',
          selected: null
        },
        {
          value: '2:30 PM - 3:00 PM',
          description: '',
          selected: null
        },
        {
          value: '3:00 PM - 3:30 PM',
          description: '',
          selected: null
        },
        {
          value: '3:30 PM - 4:00 PM',
          description: '',
          selected: null
        },
        {
          value: '4:00 PM - 4:30 PM',
          description: '',
          selected: null
        },
        {
          value: '4:30 PM - 5:00 PM',
          description: '',
          selected: null
        },
        {
          value: '5:00 PM - 5:30 PM',
          description: '',
          selected: null
        },
        {
          value: '5:30 PM - 6:00 PM',
          description: '',
          selected: null
        },
        {
          value: '6:00 PM - 6:30 PM',
          description: '',
          selected: null
        },
        {
          value: '6:30 PM - 7:00 PM',
          description: '',
          selected: null
        },
        {
          value: '7:00 PM - 7:30 PM',
          description: '',
          selected: null
        },
        {
          value: '7:30 PM - 8:00 PM',
          description: '',
          selected: null
        },
        {
          value: '8:00 PM - 8:30 PM',
          description: '',
          selected: null
        },
        {
          value: '8:30 PM - 9:00 PM',
          description: '',
          selected: null
        },
        {
          value: '9:00 PM - 9:30 PM',
          description: '',
          selected: null
        },
        {
          value: '9:30 PM - 10:00 PM',
          description: '',
          selected: null
        },
        {
          value: '10:00 PM - 10:30 PM',
          description: '',
          selected: null
        },
        {
          value: '10:30 PM - 11:00 PM',
          description: '',
          selected: null
        },
        {
          value: '11:00 PM - 11:30 PM',
          description: '',
          selected: null
        },
        {
          value: '11:30 PM - 12:00 AM',
          description: '',
          selected: null
        }
      ]
    },
    {
      name: 'shell.status',
      options: [
        {
          value: 'ENABLED',
          description: 'The shell has access to the FDR� System.',
          selected: 'true'
        },
        {
          value: 'DISABLED',
          description:
            'The shell does not have access to the FDR� System. Shells in this status can be re-enabled using any existing reset methods.',
          selected: null
        },
        {
          value: 'DISABLED*',
          description:
            'The shell does not have access to the FDR� System. The shell was manually suspended by the security administrator (master or sub-master). Shells in this status can only be reenabled by the security administrator setting this field to ENABLED.',
          selected: null
        }
      ]
    }
  ];
  const mockShell = {
    shellName: 'MANE',
    transactionProfile: 'PROFILE3',
    fieldProfile: 'FLDPRO01',
    sysPrinProfile: '00200004',
    description: 'sit nonumy ex',
    accountProfile: '*DEFAULT',
    operatorCode: '357',
    singleSession: 'N',
    employeeId: 'EMPLOYEE2',
    supervisorShell: 'SHELL003',
    reportManagementSystemId: 'RMSID001',
    departmentId: 'DEPTID02',
    level: '02',
    masterSignOnOption: 'N',
    pwdExpirationOption: 'N',
    expireDate: '2022-06-25T00:00:00',
    neverDisable: 'Y',
    comment: 'sadipscing no stet volutpat sanctus in gubergren',
    initialPwd: '12345678',
    confirmPwd: '12345678',
    shellStatus: 'DISABLED',
    daysAllowed: ['Thursday', 'Monday', 'Tuesday'],
    timesAllowed: []
  };

  const storeCofig = {
    initialState: {
      workflowSetup: {
        elementMetadata: {
          elements: mockMetadata
        }
      }
    }
  };
  beforeEach(() => {});

  it('Should render component Add shell', async () => {
    const props = {
      show: true,
      shells: [{ id: 1, shellName: 'MANE' }],
      title: 'Add Shell',
      onSetShow: jest.fn(),
      onAddOCSShell: jest.fn()
    } as IProps;
    jest.useFakeTimers();
    const { getByText, getByTestId } = await renderWithMockStore(
      <AddOCSShellModal {...props} />,
      storeCofig
    );
    expect(getByText('Add Shell')).toBeInTheDocument();
    const shellNameInput = getByTestId(
      'addOCSShell_shellName_dls-text-box_input'
    );
    expect(shellNameInput).toBeInTheDocument();
    act(() => {
      userEvent.type(shellNameInput, 'MANE');
    });
    const initialPassInput = getByTestId(
      'addOCSShell_initialPwd_dls-text-box_input'
    );
    expect(initialPassInput).toBeInTheDocument();
    act(() => {
      userEvent.type(initialPassInput, 'Mane');
    });
    const confirmPwdInput = getByTestId(
      'addOCSShell_confirmPwd_dls-text-box_input'
    );
    expect(confirmPwdInput).toBeInTheDocument();
    act(() => {
      userEvent.type(confirmPwdInput, 'Mani');
    });
    fireEvent.click(getByText('txt_add'));
    expect(props.onAddOCSShell).not.toBeCalled();
    userEvent.clear(initialPassInput);
    userEvent.type(shellNameInput, 'MANI');

    // change description
    const descriptionInput = getByTestId(
      'addOCSShell_description_dls-text-box_input'
    );
    expect(descriptionInput).toBeInTheDocument();
    act(() => {
      userEvent.type(descriptionInput, 'Liverpool');
    });

    // change transaction profile combobox
    const transactionProfileDiv = getByTestId(
      'addOCSShell_transactionProfile_dls-combobox'
    );
    const transactionProfileDivInput =
      transactionProfileDiv.querySelector('input');
    act(() => {
      fireEvent.focus(transactionProfileDivInput!);
    });
    const transactionProfileItem = document.querySelector(
      '.dls-dropdown-base-container .item'
    );

    act(() => {
      fireEvent.click(transactionProfileItem!);
    });
    jest.runAllTimers();

    // change single session dropdown list
    const singleSessionDropdown = getByTestId(
      'addOCSShell_singleSession_dls-dropdown-list'
    );
    const singleSessionInput = singleSessionDropdown.querySelector('.input');
    fireEvent.focus(singleSessionInput!);
    const singleSessionItem = document.querySelector(
      '.dls-dropdown-base-scroll-container .item'
    );
    act(() => {
      fireEvent.click(singleSessionItem!);
    });
    jest.runAllTimers();

    // change expired date picker
    const expireDatePicker = getByTestId(
      'addOCSShell_expireDate_dls-date-picker_input_dls-masked-text-box_input'
    );
    userEvent.type(expireDatePicker!, '01042022');
    fireEvent.blur(expireDatePicker);
    jest.runAllTimers();

    // change daysAllowed multi select
    const daysAllowedMulti = getByTestId(
      'addOCSShell_daysAllowed_dls-multiselect'
    );
    const daysAllowedInput = daysAllowedMulti.querySelector('input');
    fireEvent.focus(daysAllowedInput!);
    const daysAllowedItem = document.querySelector(
      '.dls-dropdown-base-container .item.item-checkbox'
    );
    fireEvent.click(daysAllowedItem!);
    jest.runAllTimers();

    fireEvent.click(getByText('txt_add'));
    expect(props.onAddOCSShell).toBeCalled();
  });

  it('Should render component Edit shell', async () => {
    const props = {
      show: true,
      shell: mockShell,
      shells: [],
      title: 'Edit Shell',
      isEdit: true,
      onSetShow: jest.fn(),
      onAddOCSShell: jest.fn()
    } as IProps;
    const { getByText } = await renderWithMockStore(
      <AddOCSShellModal {...props} />,
      storeCofig
    );
    expect(getByText('Edit Shell')).toBeInTheDocument();
    expect(getByText('txt_save')).toBeInTheDocument();
    fireEvent.click(getByText('txt_save'));
    expect(props.onAddOCSShell).toBeCalled();
  });

  it('Should render grid with search parameter', async () => {
    const props = {
      show: true,
      shells: [],
      title: 'Add Shell',
      onSetShow: jest.fn(),
      onAddOCSShell: jest.fn()
    } as IProps;
    const { getByText, queryByText } = await renderWithMockStore(
      <AddOCSShellModal {...props} />,
      storeCofig
    );
    expect(getByText('Add Shell')).toBeInTheDocument();
    expect(getByText('Simple Search')).toBeInTheDocument();

    // search no data
    fireEvent.click(getByText('Search Sadio'));
    expect(getByText(/txt_no_results_found/)).toBeInTheDocument();
    fireEvent.click(getByText('txt_clear_and_reset'));
    expect(queryByText(/txt_no_results_found/)).not.toBeInTheDocument();

    // search has data
    fireEvent.click(getByText('Search Shell'));
    expect(getByText(/txt_clear_and_reset/)).toBeInTheDocument();
    fireEvent.click(getByText('txt_clear_and_reset'));
    expect(queryByText(/txt_clear_and_reset/)).not.toBeInTheDocument();
  });

  it('Should render modal and cancel', async () => {
    const props = {
      show: true,
      shells: [],
      title: 'Add Shell',
      onSetShow: jest.fn(),
      onAddOCSShell: jest.fn()
    } as IProps;
    const { getByText } = await renderWithMockStore(
      <AddOCSShellModal {...props} />,
      storeCofig
    );
    expect(getByText('Add Shell')).toBeInTheDocument();
    expect(getByText('txt_cancel')).toBeInTheDocument();
    fireEvent.click(getByText('txt_cancel'));
    expect(props.onSetShow).toBeCalled();
  });
});
