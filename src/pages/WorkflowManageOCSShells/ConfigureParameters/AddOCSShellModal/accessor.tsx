import EnhanceDatePicker from 'app/components/EnhanceDatePicker';
import EnhanceDropdownList from 'app/components/EnhanceDropdownList';
import SentitiveDataInput from 'app/components/SentitiveDataInput';
import { isValidDate } from 'app/helpers';
import { ComboBox, TextBox } from 'app/_libraries/_dls';
import MultiSelect from 'app/_libraries/_dls/components/MultiSelect';
import { isString } from 'lodash';
import React from 'react';
import {
  InputControlType,
  ISubGridData
} from '../constants/DefaultSubGridData';

const accessor =
  (metadata: { [key: string]: RefData[] }) =>
  (
    rowData: ISubGridData,
    field: keyof ISubGridData,
    t: any
  ): JSX.Element | undefined => {
    const {
      id,
      inputControlType,
      onChange,
      keyOptions,
      inputOptions = {}
    } = rowData;
    const options = metadata && keyOptions ? metadata[keyOptions] : [];

    const value = rowData[field] as any;
    switch (inputControlType) {
      case InputControlType.TextBox:
        const { trimStart, upperCase, ...rest } = inputOptions;
        return (
          <TextBox
            dataTestId={`addOCSShell_${id}`}
            size="sm"
            value={value}
            onChange={e => {
              let value = e.target.value;
              if (trimStart) {
                value = value.trimStart();
              }
              if (upperCase) {
                value = value.toUpperCase();
              }
              onChange && onChange(value);
            }}
            placeholder={t('txt_enter_a_value')}
            {...rest}
          />
        );
      case InputControlType.Combobox:
        const comboboxValue = isString(value)
          ? options.find(o => o.code === value)
          : value;
        return (
          <ComboBox
            size="small"
            value={comboboxValue}
            placeholder={t('txt_select_an_option')}
            dataTestId={`addOCSShell_${id}`}
            textField="text"
            onChange={e => onChange && onChange(e.target.value)}
            noResult={t('txt_no_results_found_without_dot')}
            searchBarPlaceholder={t('txt_select_an_option')}
          >
            {options.map(option => (
              <ComboBox.Item
                key={option.code}
                value={option}
                label={option.text}
              />
            ))}
          </ComboBox>
        );
      case InputControlType.DropdownList:
        const dropdownValue = isString(value)
          ? options.find(o => o.code === value)
          : value;
        return (
          <EnhanceDropdownList
            placeholder={t('txt_select_an_option')}
            dataTestId={`addOCSShell_${id}`}
            size="small"
            value={dropdownValue}
            onChange={e => onChange && onChange(e.target.value?.code)}
            options={options}
            {...inputOptions}
          />
        );
      case InputControlType.DatePicker:
        return (
          <EnhanceDatePicker
            size="small"
            dataTestId={`addOCSShell_${id}`}
            value={isValidDate(value) ? new Date(value) : undefined}
            onChange={e => onChange && onChange(e.target.value)}
            label={t('Todo Label')}
            {...inputOptions}
          />
        );
      case InputControlType.Password:
        return (
          <SentitiveDataInput
            value={value}
            onChange={e => onChange && onChange(e.target.value)}
            placeholder={t('txt_enter_a_value')}
            dataTestId={`addOCSShell_${id}`}
            {...inputOptions}
          />
        );
      case InputControlType.MultiSelect:
        const multiSelectValue = value?.every((v: any) => typeof v === 'string')
          ? options.filter(o => value?.includes(o.code))
          : value;
        return (
          <MultiSelect
            dataTestId={`addOCSShell_${id}`}
            value={multiSelectValue}
            onChange={e => onChange && onChange(e.target.value)}
            textField="text"
            placeholder={t('txt_select_options')}
          >
            {options.map(option => (
              <MultiSelect.Item
                key={option.code}
                label={option.text}
                value={option}
                variant="checkbox"
              />
            ))}
          </MultiSelect>
        );
    }
  };

export default accessor;
