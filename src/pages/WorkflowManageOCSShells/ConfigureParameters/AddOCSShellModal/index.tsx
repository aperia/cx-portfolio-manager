import ModalRegistry from 'app/components/ModalRegistry';
import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import { FormatTime } from 'app/constants/enums';
import { unsavedChangesProps } from 'app/constants/unsave-changes-form-names';
import { formatTimeDefault, matchSearchValue } from 'app/helpers';
import { useUnsavedChangeRegistry, useUnsavedChangesRedirect } from 'app/hooks';
import {
  Grid,
  InlineMessage,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import { get, isEmpty } from 'lodash';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import {
  actionsWorkflowSetup,
  useSelectMetadataForManageOCSShells
} from 'pages/_commons/redux/WorkflowSetup';
import ClearAndResetButton from 'pages/_commons/Utils/ClearAndResetButton';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';
import { MetadataKey } from '../constants';
import DefaultSubGridColumns from '../constants/DefaultSubGridColumns';
import DefaultSubGridData, {
  ISubGridData
} from '../constants/DefaultSubGridData';
import { convertFormDataToShell, isDiff } from '../helpers';
import accessor from './accessor';

export interface IProps {
  show: boolean;
  isEdit?: boolean;
  shell?: IOCSShell;
  shells?: IOCSShell[];
  title: string;
  onSetShow: (show: boolean) => void;
  onAddOCSShell: (shell: IOCSShell) => void;
}
const searchFields = ['fieldName', 'greenScreenName', 'moreInfo'];

const AddOCSShellModal: React.FC<IProps> = ({
  show,
  isEdit = false,
  shell: shellProps,
  shells,
  title,
  onSetShow,
  onAddOCSShell
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const metadata = useSelectMetadataForManageOCSShells();
  const redirect = useUnsavedChangesRedirect();
  const { width } = useSelectWindowDimension();

  const [searchValue, setSearchValue] = useState<string>();
  const [shell, setShell] = useState<IOCSShell>({});
  const [minDateSetting, setMinDateSetting] = useState();
  const [maxDateSetting, setMaxDateSetting] = useState();
  const [errors, setErrors] = useState<MagicKeyValue>({});
  const initialShellRef = useRef<IOCSShell>();

  const errorMessages = new Set(
    Object.values(errors).map(error => error.message)
  );
  const inlineErrors = Array.from(errorMessages);

  const minDate = useMemo(() => {
    return new Date(formatTimeDefault(minDateSetting!, FormatTime.DateServer));
  }, [minDateSetting]);

  const maxDate = useMemo(() => {
    return new Date(formatTimeDefault(maxDateSetting!, FormatTime.DateServer));
  }, [maxDateSetting]);

  useUnsavedChangeRegistry(
    {
      ...unsavedChangesProps,
      formName: 'unsaveOCSShell',
      priority: 1
    },
    [
      !!initialShellRef.current &&
        isDiff(initialShellRef.current, convertFormDataToShell(shell))
    ]
  );

  const handleChangeField = (field: string) => (value: any) => {
    if (field === 'initialPwd' && !value) {
      setShell({ ...shell, [field]: value, confirmPwd: value });
      return;
    }
    setShell({ ...shell, [field]: value });
  };
  const defaultColumns = DefaultSubGridColumns(
    accessor(metadata),
    t,
    width,
    true
  );
  const columns = defaultColumns.filter(
    column => column.id !== 'previousValue' && column.id !== 'newValue'
  );
  const defaultData = useMemo(() => {
    return DefaultSubGridData(shell, t);
  }, [shell, t]);

  const data = defaultData.map((item: ISubGridData) => {
    const { id, inputOptions } = item;
    const _inputOptions = { ...inputOptions, error: errors?.[id] };
    return {
      ...item,
      onChange: handleChangeField(id),
      inputOptions:
        id === 'expireDate'
          ? { ..._inputOptions, minDate, maxDate }
          : { ..._inputOptions }
    };
  });

  const filteredData = useMemo(() => {
    return data.filter((item: any) =>
      matchSearchValue(
        searchFields.map((field: string) => get(item, field.split('.'))),
        searchValue
      )
    );
  }, [searchValue, data]);

  const validate = () => {
    let errors = {};
    const { id, shellName, initialPwd, confirmPwd } = shell;

    // shell name is unique
    const hasShellName = shells?.some(
      x => x.id !== id && x.shellName === shellName
    );
    if (hasShellName) {
      errors = {
        ...errors,
        shellName: {
          status: true,
          message: 'Shell Name must be unique.'
        }
      };
    }

    // password miss match
    if (!!initialPwd && !!confirmPwd && initialPwd !== confirmPwd) {
      const error = {
        status: true,
        message: 'Initial Password and Confirm Password do not match.'
      };
      errors = { ...errors, initialPwd: error, confirmPwd: error };
    }

    return errors;
  };

  const handleAddRecord = () => {
    const errors = validate();
    setErrors(errors);
    const isInvalid = Object.values(errors).some((error: any) => error.status);
    if (!isInvalid) {
      const _shell = convertFormDataToShell(shell);
      onAddOCSShell(_shell);
    }
  };

  const handleClose = () => {
    redirect({
      onConfirm: () => onSetShow(false),
      formsWatcher: ['unsaveOCSShell']
    });
  };

  const handleClearFilter = () => {
    setSearchValue('');
  };

  useEffect(() => {
    if (shellProps) {
      setShell(shellProps);
      initialShellRef.current = shellProps;
    } else {
      const accountProfile = metadata[MetadataKey.AccountProfile]?.find(
        option => option.selected
      )?.code;
      const singleSession = metadata[MetadataKey.SingleSession]?.find(
        option => option.selected
      )?.code;
      const masterSignOnOption = metadata[MetadataKey.MasterSignOnOption]?.find(
        option => option.selected
      )?.code;
      const pwdExpirationOption = metadata[
        MetadataKey.PasswordExpirationOption
      ]?.find(option => option.selected)?.code;
      const neverDisable = metadata[MetadataKey.NeverDisable]?.find(
        option => option.selected
      )?.code;
      const shellStatus = metadata[MetadataKey.Status]?.find(
        option => option.selected
      )?.code;
      const initialShell = {
        accountProfile,
        singleSession,
        masterSignOnOption,
        pwdExpirationOption,
        neverDisable,
        shellStatus
      };
      setShell(initialShell);
      initialShellRef.current = initialShell;
    }
  }, [shellProps, metadata]);

  // get date options
  const getDateOptions = useCallback(async () => {
    const responseDateRange = await Promise.resolve(
      dispatch(actionsWorkflowSetup.getWorkflowEffectiveDateOptions({}))
    );

    const { maxDate: maxDateRes, minDate: minDateRes } =
      (responseDateRange as any)?.payload?.data || {};

    setMinDateSetting(minDateRes);
    setMaxDateSetting(maxDateRes);
  }, [dispatch]);

  useEffect(() => {
    getDateOptions();
  }, [getDateOptions]);

  return (
    <ModalRegistry
      lg
      id={'add-ocs-shell-modal'}
      show={show}
      onAutoClosedAll={handleClose}
    >
      <ModalHeader border closeButton onHide={handleClose}>
        <ModalTitle>{title}</ModalTitle>
      </ModalHeader>
      <ModalBody>
        <>
          <div className="d-flex align-items-center justify-content-between">
            <h5>{t('txt_parameter_list')}</h5>
            <SimpleSearch
              defaultValue={searchValue}
              placeholder={t('txt_type_to_search')}
              onSearch={setSearchValue}
              popperElement={
                <>
                  <p className="color-grey">{t('txt_search_description')}</p>
                  <ul className="search-field-item list-unstyled">
                    <li className="mt-16">
                      {t('txt_manage_ocs_shells_step_3_business_name')}
                    </li>
                    <li className="mt-16">
                      {t('txt_manage_ocs_shells_step_3_green_screen_name')}
                    </li>
                    <li className="mt-16">
                      {t('txt_manage_ocs_shells_step_3_more_info')}
                    </li>
                  </ul>
                </>
              }
            />
          </div>
          {searchValue && filteredData.length > 0 && (
            <div className="d-flex justify-content-end mt-16 mr-n8">
              <ClearAndResetButton small onClearAndReset={handleClearFilter} />
            </div>
          )}
          {!isEmpty([inlineErrors]) && (
            <div className="mt-8">
              {inlineErrors.map(message => (
                <InlineMessage
                  key={message}
                  variant="danger"
                  className="mb-0 mt-8 mr-8"
                >
                  {message}
                </InlineMessage>
              ))}
            </div>
          )}
          {filteredData.length > 0 ? (
            <div className="mt-16">
              <Grid columns={columns} data={filteredData} />
            </div>
          ) : (
            <div className="d-flex flex-column justify-content-center mt-40">
              <NoDataFound
                id="newRecord_notfound"
                hasSearch
                title={t('txt_no_results_found')}
                linkTitle={t('txt_clear_and_reset')}
                onLinkClicked={handleClearFilter}
              />
            </div>
          )}
        </>
      </ModalBody>
      <ModalFooter
        cancelButtonText={t('txt_cancel')}
        okButtonText={!isEdit ? t('txt_add') : t('txt_save')}
        onCancel={handleClose}
        onOk={handleAddRecord}
      ></ModalFooter>
    </ModalRegistry>
  );
};

export default AddOCSShellModal;
