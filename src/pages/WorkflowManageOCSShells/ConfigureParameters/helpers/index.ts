import { FormatTime } from 'app/constants/enums';
import { formatTime } from 'app/helpers';
import { format } from 'date-and-time';
import { isArray, isDate, isEqual } from 'lodash';
import { OCSShellKey, OCSShellKeyToParameter, STATUS } from '../constants';

export const mapColorBadge = (status: string) => {
  switch (status) {
    case STATUS.New:
      return 'purple';
    case STATUS.Updated:
      return 'green';
    case STATUS.MarkedForDeletion:
      return 'red';
    case STATUS.NotUpdated:
    default:
      return 'grey';
  }
};

export const isDiff = (
  shell1: IOCSShell,
  shell2: IOCSShell | undefined,
  compareFields?: (keyof IOCSShell)[]
): boolean => {
  if (!shell1 || !shell2) return false;

  const _compareFields = compareFields
    ? compareFields
    : Object.values(OCSShellKey);
  return _compareFields.some(key => {
    if (key === OCSShellKey.DaysAllowed || key === OCSShellKey.TimesAllowed) {
      const diff =
        shell1[key]?.length !== shell2[key]?.length ||
        !isEqual(
          [...(shell1[key] || [])].sort(),
          [...(shell2[key] || [])].sort()
        );
      return diff;
    } else if (key === OCSShellKey.ExpireDate) {
      const shell1Date = formatTime(shell1[key]).date;
      const shell2Date = formatTime(shell2[key]).date;
      return (shell1Date || shell2Date) && shell1Date !== shell2Date;
    } else {
      return (shell1[key] || shell2[key]) && shell1[key] !== shell2[key];
    }
  });
};

export const convertFormDataToShell = (shellFormData: MagicKeyValue) => {
  let _shell = { ...shellFormData };

  Object.entries(shellFormData).forEach(keyValue => {
    const [key, value] = keyValue;
    if (isArray(value)) {
      _shell = {
        ..._shell,
        [key]: value.map(value =>
          typeof value === 'string' ? value : value?.code
        )
      };
    } else if (isDate(value)) {
      _shell = { ..._shell, [key]: format(value, FormatTime.Date) };
    } else if (typeof value !== 'string' && typeof value !== 'number') {
      _shell = { ..._shell, [key]: value?.code };
    }
  });

  return _shell;
};

export const convertShellsToRequestModel = (
  shells?: IOCSShell[],
  originalShells?: IOCSShell[]
) => {
  const ocsShells = shells?.map(shell => {
    const { status, id, modelFromName } = shell;
    let parameters: any[] = [];
    switch (status) {
      case STATUS.MarkedForDeletion:
      case STATUS.NotUpdated:
        parameters = Object.keys(OCSShellKeyToParameter).map(field => ({
          name: OCSShellKeyToParameter[field],
          previousValue: shell[field as keyof IOCSShell],
          newValue: ''
        }));
        break;
      case STATUS.Updated:
        const originalShell =
          originalShells?.find(shell => shell.shellName === modelFromName) ||
          {};
        parameters = Object.keys(OCSShellKeyToParameter).map(field => ({
          name: OCSShellKeyToParameter[field],
          previousValue: originalShell[field as keyof IOCSShell],
          newValue: shell[field as keyof IOCSShell]
        }));
        break;
      case STATUS.New:
        parameters = Object.keys(OCSShellKeyToParameter).map(field => ({
          name: OCSShellKeyToParameter[field],
          previousValue: '',
          newValue: shell[field as keyof IOCSShell]
        }));
        break;
    }

    return {
      id,
      modelFromName,
      status,
      parameters
    };
  });

  return ocsShells;
};

const convertShellParamToProp = (shell: any): IOCSShell => ({
  id: shell.id,
  modelFromName: shell.modelFromName,
  status: shell.status,
  ...Object.keys(OCSShellKeyToParameter).reduce((pre, curr) => {
    const parameter = shell.parameters?.find(
      (p: any) => p.name === OCSShellKeyToParameter[curr]
    );

    const isNewOrUpdated = [STATUS.Updated, STATUS.New].includes(shell.status);

    return {
      ...pre,
      [curr]: isNewOrUpdated ? parameter?.newValue : parameter?.previousValue
    };
  }, {} as any)
});

export const convertShellsParamToProp = (shells: any[]): IOCSShell[] => {
  const ocsShells = shells.map(convertShellParamToProp);

  return ocsShells;
};
