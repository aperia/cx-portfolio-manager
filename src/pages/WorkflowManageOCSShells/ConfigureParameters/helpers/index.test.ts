import {
  convertFormDataToShell,
  convertShellsParamToProp,
  convertShellsToRequestModel,
  isDiff,
  mapColorBadge
} from '.';
import { STATUS } from '../constants';

describe('Manage OCS Shells > Step 3 > helpers', () => {
  it('mapColorBadge', () => {
    const statusColors = {
      [STATUS.New]: 'purple',
      [STATUS.Updated]: 'green',
      [STATUS.MarkedForDeletion]: 'red',
      [STATUS.NotUpdated]: 'grey'
    };
    Object.entries(statusColors).map(([key, value]) => {
      const color = mapColorBadge(key);
      expect(color).toEqual(value);
    });
  });

  it('isDiff with undefined shell', () => {
    const shell: IOCSShell = {
      shellName: 'name',
      daysAllowed: ['Monday'],
      timesAllowed: ['01:00AM - 01:30AM'],
      expireDate: '02/02/2020'
    };
    const otherShell = undefined as any;

    const diff = isDiff(shell, otherShell);
    expect(diff).toBe(false);
  });

  it('isDiff default', () => {
    const shell: IOCSShell = {
      shellName: 'name',
      daysAllowed: ['Monday'],
      timesAllowed: ['01:00AM - 01:30AM'],
      expireDate: '02/02/2020'
    };
    const otherShell: IOCSShell = {
      shellName: 'name',
      daysAllowed: ['Monday'],
      timesAllowed: ['01:00AM - 01:30AM'],
      expireDate: '02/02/2020'
    };

    const diff = isDiff(shell, otherShell);
    expect(diff).toBe(false);
  });

  it('isDiff compare custom fields', () => {
    const shell: IOCSShell = {
      shellName: 'name',
      daysAllowed: undefined,
      timesAllowed: ['01:00AM - 01:30AM'],
      expireDate: undefined
    };
    const otherShell: IOCSShell = {
      shellName: 'name',
      daysAllowed: undefined,
      timesAllowed: ['01:00AM - 01:30AM'],
      expireDate: undefined
    };

    const diff = isDiff(shell, otherShell, ['daysAllowed', 'expireDate']);
    expect(diff).toBe(false);
  });

  it('convertFormDataToShell', () => {
    const shellFormData: MagicKeyValue = {
      shellName: 'name',
      daysAllowed: [{ code: 'monday', text: 'Monday' }, 'tuesday'],
      expireDate: new Date('12/23/2021'),
      accountProfile: { code: 'profile', text: 'Profile' }
    };
    const shell = convertFormDataToShell(shellFormData);
    expect(shell).toEqual({
      shellName: 'name',
      daysAllowed: ['monday', 'tuesday'],
      expireDate: '12/23/2021',
      accountProfile: 'profile'
    });
  });

  it('convertShellsToRequestModel', () => {
    const shells: IOCSShell[] = [
      { id: '1', shellName: 'name', status: STATUS.New },
      { id: '2', shellName: 'name', status: STATUS.Updated },
      { id: '3', shellName: 'name', status: STATUS.NotUpdated },
      { id: '4', shellName: 'name', status: STATUS.MarkedForDeletion },
      { id: '5', shellName: 'name', status: STATUS.Updated }
    ];
    const originalShells: IOCSShell[] = [
      { id: '1', shellName: 'name', status: STATUS.NotUpdated },
      { id: '2', shellName: 'name', status: STATUS.NotUpdated },
      { id: '3', shellName: 'name', status: STATUS.NotUpdated },
      { id: '4', shellName: 'name', status: STATUS.NotUpdated }
    ];
    const requestModel = convertShellsToRequestModel(shells, originalShells);
    expect(requestModel).toHaveLength(5);
  });

  it('convertShellsParamToProp', () => {
    const shells: IOCSShell[] = [
      {
        id: '1',
        shellName: 'name',
        status: STATUS.New,
        parameters: [{ name: 'name' }]
      } as any,
      { id: '2', shellName: 'name', status: STATUS.Updated },
      { id: '3', shellName: 'name', status: STATUS.NotUpdated },
      { id: '4', shellName: 'name', status: STATUS.MarkedForDeletion },
      { id: '5', shellName: 'name', status: STATUS.Updated }
    ];
    const requestModel = convertShellsParamToProp(shells);
    expect(requestModel).toHaveLength(5);
  });
});
