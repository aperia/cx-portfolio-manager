import { fireEvent } from '@testing-library/dom';
import * as FileUtils from 'app/helpers/fileUtilities';
import * as helper from 'app/helpers/isDevice';
import { mockThunkAction, renderWithMockStore } from 'app/utils';
import { actionsWorkflow } from 'pages/_commons/redux/Workflow';
import React from 'react';
import { ManageOCSShellsFormValue } from '.';
import { STATUS as SHELL_STATUS } from './constants';
import Summary from './Summary';

const mockHelper: any = helper;

const base64toBlobMock = jest.spyOn(FileUtils, 'base64toBlob');
const downloadBlobFileMock = jest.spyOn(FileUtils, 'downloadBlobFile');

const storeCofig = {
  initialState: {}
};
const mockActionsWorkflow = mockThunkAction(actionsWorkflow);

describe('Manage OCS Shells > Configure Parameters > Summary', () => {
  const generateUpdatedShells = (count: number) => {
    const shells = [];
    for (let i = 0; i < count; i++) {
      shells.push({ id: 1, shellName: 'name1', status: SHELL_STATUS.Updated });
    }
    return shells;
  };
  beforeEach(() => {
    base64toBlobMock.mockReturnValue(new Blob());
    downloadBlobFileMock.mockImplementation(() => {});
  });

  it('Should render Grid and Upload File', async () => {
    const props = {
      formValues: {
        shells: generateUpdatedShells(15),
        originalShells: [{ id: 1, shellName: 'name' }],
        files: [
          {
            idx: '7678',
            name: 'test.xlsx',
            size: 8289,
            percentage: 100,
            type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            status: 0,
            length: 1,
            item: jest.fn()
          }
        ]
      },
      onEditStep: jest.fn()
    } as WorkflowSetupSummaryProps<ManageOCSShellsFormValue>;
    const wrapper = await renderWithMockStore(<Summary {...props} />, {});
    expect(wrapper.getByText('txt_edit')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(props.onEditStep).toBeCalled();
    const expandIcon = wrapper.container.querySelector('i.icon.icon-plus');
    fireEvent.click(expandIcon!);
  });

  it('render with empty ocs shell list', async () => {
    mockHelper.isDevice = false;
    const props = {
      formValues: {
        shells: undefined,
        originalShells: undefined,
        files: [
          {
            idx: '7678',
            name: 'test.xlsx',
            size: 8289,
            percentage: 100,
            type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            status: 0,
            length: 1,
            item: jest.fn()
          }
        ]
      },
      onEditStep: jest.fn()
    } as WorkflowSetupSummaryProps<ManageOCSShellsFormValue>;
    const wrapper = await renderWithMockStore(
      <Summary {...props} />,
      storeCofig
    );

    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(props.onEditStep).toBeCalled();
  });

  it('render with empty file', async () => {
    mockHelper.isDevice = true;
    const props = {
      formValues: {
        shells: [],
        originalShells: [],
        files: undefined
      },
      onEditStep: jest.fn()
    } as WorkflowSetupSummaryProps<ManageOCSShellsFormValue>;
    const wrapper = await renderWithMockStore(
      <Summary {...props} />,
      storeCofig
    );

    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(props.onEditStep).toBeCalled();
  });

  it('Download file failed', async () => {
    const props = {
      formValues: {
        shells: generateUpdatedShells(15),
        originalShells: [{ id: 1, shellName: 'name' }],
        files: [
          {
            idx: '7678',
            name: 'test.xlsx',
            size: 8289,
            percentage: 100,
            type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            status: 0,
            length: 1,
            item: jest.fn()
          }
        ]
      },
      onEditStep: jest.fn()
    } as WorkflowSetupSummaryProps<ManageOCSShellsFormValue>;
    mockActionsWorkflow('downloadAttachment', {
      match: false,
      payload: {}
    });
    const wrapper = await renderWithMockStore(
      <Summary {...props} />,
      storeCofig
    );
    const downloadIcon = wrapper.container.querySelector(
      'i.icon.icon-download'
    );
    fireEvent.click(downloadIcon!);
  });

  it('Download file successfully with full file name', async () => {
    const props = {
      formValues: {
        shells: generateUpdatedShells(15),
        originalShells: [{ id: 1, shellName: 'name' }],
        files: [
          {
            idx: '7678',
            name: 'test.xlsx',
            size: 8289,
            percentage: 100,
            type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            status: 0,
            length: 1,
            item: jest.fn()
          }
        ]
      },
      onEditStep: jest.fn()
    } as WorkflowSetupSummaryProps<ManageOCSShellsFormValue>;
    mockActionsWorkflow('downloadAttachment', {
      match: true,
      payload: {
        payload: {
          fileStream: '',
          mimeType: 'text/csv',
          filename: 'mane.csv'
        }
      }
    });
    const wrapper = await renderWithMockStore(
      <Summary {...props} />,
      storeCofig
    );
    const downloadIcon = wrapper.container.querySelector(
      'i.icon.icon-download'
    );
    fireEvent.click(downloadIcon!);
    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(props.onEditStep).toBeCalled();
    expect(
      wrapper.getByText('txt_manage_ocs_shells_step_4_ocs_shell_file')
    ).toBeInTheDocument();
  });

  it('Download file successfully with file name not full', async () => {
    const props = {
      formValues: {
        shells: generateUpdatedShells(15),
        originalShells: [{ id: 1, shellName: 'name' }],
        files: [
          {
            idx: '7678',
            name: 'test.xlsx',
            size: 8289,
            percentage: 100,
            type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            status: 0,
            length: 1,
            item: jest.fn()
          }
        ]
      },
      onEditStep: jest.fn()
    } as WorkflowSetupSummaryProps<ManageOCSShellsFormValue>;
    mockActionsWorkflow('downloadAttachment', {
      match: true,
      payload: {
        payload: {
          attachments: [
            {
              validationMessages: [],
              id: 'id',
              fileName: 'mane',
              fileSize: 123,
              mineType: 'text/csv'
            }
          ]
        }
      }
    });
    const wrapper = await renderWithMockStore(
      <Summary {...props} />,
      storeCofig
    );
    const downloadIcon = wrapper.container.querySelector(
      'i.icon.icon-download'
    );
    fireEvent.click(downloadIcon!);
    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(props.onEditStep).toBeCalled();
  });

  it('Download file successfully with file name without payload', async () => {
    const props = {
      formValues: {
        shells: generateUpdatedShells(15),
        originalShells: [{ id: 1, shellName: 'name' }],
        files: [
          {
            idx: '7678',
            name: 'test.xlsx',
            size: 8289,
            percentage: 100,
            type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            status: 0,
            length: 1,
            item: jest.fn()
          }
        ]
      },
      onEditStep: jest.fn()
    } as WorkflowSetupSummaryProps<ManageOCSShellsFormValue>;
    mockActionsWorkflow('downloadAttachment', {
      match: true,
      payload: {}
    });
    const wrapper = await renderWithMockStore(
      <Summary {...props} />,
      storeCofig
    );
    const downloadIcon = wrapper.container.querySelector(
      'i.icon.icon-download'
    );
    fireEvent.click(downloadIcon!);
    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(props.onEditStep).toBeCalled();
  });
});
