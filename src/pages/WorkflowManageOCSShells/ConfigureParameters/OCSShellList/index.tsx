import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import {
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { classnames, mapGridExpandCollapse } from 'app/helpers';
import { useCheckAllPagination, useUnsavedChangeRegistry } from 'app/hooks';
import { usePagination } from 'app/hooks/usePagination';
import {
  Badge,
  Button,
  ColumnType,
  Grid,
  useTranslation
} from 'app/_libraries/_dls';
import { DEFAULT_PAGE_SIZE } from 'app/_libraries/_dls/components/Pagination/constants';
import { mapColorBadge } from 'pages/WorkflowManageAccountSerialization/ManageAccountSerializationStep/helper';
import {
  MetadataKey,
  OCSShellKey,
  STATUS
} from 'pages/WorkflowManageOCSShells/ConfigureParameters/constants';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import {
  actionsWorkflowSetup,
  useSelectOCSShells
} from 'pages/_commons/redux/WorkflowSetup';
import Paging from 'pages/_commons/Utils/Paging';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';
import { ManageOCSShellsFormValue } from '..';
import AddOCSShellModal from '../AddOCSShellModal';
import CancelOCSShellModal from '../CancelOCSShellModal';
import { truncateAccessor } from '../constants/DefaultSubGridColumns';
import ExpandedGrid from '../ExpandedGrid';
import { isDiff } from '../helpers';
import RemoveOCSShellModal from '../RemoveOCSShellModal';
import Actions from './Actions';

const OCSShellList: React.FC<WorkflowSetupProps<ManageOCSShellsFormValue>> =
  props => {
    const { t } = useTranslation();
    const { width } = useSelectWindowDimension();
    const dispatch = useDispatch();

    const columns: ColumnType[] = [
      {
        id: 'shellName',
        isSort: true,
        Header: t('txt_manage_ocs_shells_step_3_shell_name'),
        accessor: (rowData: MagicKeyValue) =>
          truncateAccessor(rowData, 'shellName', t),
        width: width < 1280 ? 160 : undefined
      },
      {
        id: 'transactionProfile',
        isSort: true,
        Header: t('txt_manage_ocs_shells_step_3_transaction_profile'),
        accessor: (rowData: MagicKeyValue) =>
          truncateAccessor(rowData, 'transactionProfile', t),
        width: width < 1280 ? 200 : undefined
      },
      {
        id: 'employeeId',
        isSort: true,
        Header: t('txt_manage_ocs_shells_step_3_employee_id'),
        accessor: (rowData: MagicKeyValue) =>
          truncateAccessor(rowData, 'employeeId', t),
        width: width < 1280 ? 126 : undefined
      },
      {
        id: 'shellStatus',
        isSort: true,
        Header: t('txt_manage_ocs_shells_step_3_shell_status'),
        accessor: (rowData: MagicKeyValue) =>
          truncateAccessor(rowData, 'shellStatus', t),
        width: width < 1280 ? 126 : undefined
      },
      {
        id: 'status',
        isSort: false,
        Header: t('txt_manage_ocs_shells_step_3_status'),
        width: width < 1280 ? 200 : undefined,
        accessor: (row: MagicKeyValue) => {
          return (
            <Badge textTransformNone color={mapColorBadge(row?.status)}>
              {row?.status}
            </Badge>
          );
        }
      },
      {
        id: 'action',
        isSort: false,
        Header: t('txt_manage_ocs_shells_step_3_action'),
        width: 133,
        cellHeaderProps: {
          className: 'text-center'
        },
        cellBodyProps: {
          className: 'py-8'
        },
        accessor: (row: MagicKeyValue) => {
          return (
            <Actions
              data={row}
              onCancelRecord={handleShowCancelModal}
              onDeleteRecord={handleShowDeleteRecordModal}
              onEditRecord={handleShowEditRecordModal}
            />
          );
        }
      }
    ];

    const { stepId, selectedStep, setFormValues, formValues, savedAt } = props;
    const { shells = [] } = formValues;
    const keepRef = useRef({ setFormValues, initialShells: [] as IOCSShell[] });
    keepRef.current.setFormValues = setFormValues;
    const hasMetadata = useRef<boolean>(false);

    const [expandedIds, setExpandedIds] = useState<string[]>([]);
    const [showAddOCSShellModal, setShowAddOCSShellModal] =
      useState<boolean>(false);
    const [isEdit, setIsEdit] = useState<boolean>(false);
    const [editShell, setEditShell] = useState<MagicKeyValue>();
    const [showDeleteRecordModal, setShowDeleteRecordModal] =
      useState<boolean>(false);
    const [showBulkDeleteRecordModal, setShowBulkDeleteRecordModal] =
      useState<boolean>(false);
    const [deleteShell, setDeleteShell] = useState<IOCSShell>();
    const [showConfirmCancelModal, setShowConfirmCancelModal] =
      useState<boolean>(false);
    const [cancelShell, setCancelShell] = useState<IOCSShell>();

    const ocsShellsData = useSelectOCSShells();
    const { loading, ocsShells = [] } = ocsShellsData || {};

    const {
      gridData,
      dataChecked: _checkedList,
      searchValue,
      allDataFilter,
      sort,
      currentPage,
      currentPageSize,
      total,
      onSearchChange,
      onSortChange,
      onPageChange,
      onPageSizeChange,
      onSetDataChecked,
      onResetToDefaultFilter
    } = usePagination(
      shells,
      [
        'shellName',
        'transactionProfile',
        'description',
        'operatorCode',
        'employeeId'
      ],
      'shellName'
    );

    const readOnlyCheckbox = useMemo(() => {
      const disableShells = shells.filter(
        shell =>
          shell.status === STATUS.Updated ||
          shell.status === STATUS.MarkedForDeletion
      );
      return disableShells.map(shell => shell.id!);
    }, [shells]);
    const allIds = shells
      .map(f => f.id!)
      .filter(r => !readOnlyCheckbox.includes(r));
    const checkedList = useMemo(
      () =>
        _checkedList
          .filter(i => allIds.includes(i))
          .filter(r => !readOnlyCheckbox.includes(r)),
      [allIds, _checkedList, readOnlyCheckbox]
    );
    const { gridClassName, handleClickCheckAll } = useCheckAllPagination(
      {
        gridClassName: 'ocs-shells-list',
        allIds,
        hasFilter: !!searchValue,
        filterIds: allDataFilter
          .map(f => f.id!)
          .filter(r => !readOnlyCheckbox.includes(r)),
        checkedList,
        setCheckAll: _isCheckAll => onSetDataChecked(_isCheckAll ? allIds : []),
        setCheckList: onSetDataChecked
      },
      [searchValue, sort, currentPage, currentPageSize]
    );

    const recordSelected = useMemo(() => {
      const count = checkedList?.length;
      const text =
        count === shells.length
          ? t('txt_manage_ocs_shells_step_3_all_ocs_shells_types_selected')
          : count !== 1
          ? t('txt_manage_ocs_shells_step_3_ocs_shells_selected', {
              count
            })
          : t('txt_manage_ocs_shells_step_3_ocs_shell_selected', {
              count
            });

      return text;
    }, [checkedList.length, shells.length, t]);

    const hasChange = () => {
      const initialShells = keepRef.current.initialShells;
      const hasChange =
        initialShells.length !== shells.length ||
        shells.some(shell =>
          isDiff(
            shell,
            initialShells?.find(s => s.id === shell.id),
            [...Object.values(OCSShellKey), 'status']
          )
        );
      return hasChange;
    };

    useUnsavedChangeRegistry(
      {
        ...unsavedExistedWorkflowSetupDataProps,
        formName: UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_OCS_SHELLS,
        priority: 1
      },
      [stepId === selectedStep && hasChange()]
    );

    const handleSearch = (value: string) => {
      setExpandedIds([]);
      onSearchChange(value);
    };

    const handleClearAndReset = () => {
      onSearchChange('');
      setExpandedIds([]);
    };

    const toggleAddOCSShellModal = () => {
      setShowAddOCSShellModal(state => !state);
    };

    const handleShowAddRecordModal = () => {
      setShowAddOCSShellModal(true);
      setIsEdit(false);
      setEditShell(undefined);
    };

    const handleAddRecordToList = (shell: IOCSShell) => {
      const { id, status, modelFromName } = shell;
      if (!id) {
        const _id = `${Date.now()}`;
        keepRef.current.setFormValues({
          shells: [...shells, { ...shell, id: _id, status: STATUS.New }]
        });
        onResetToDefaultFilter();
        setExpandedIds([_id] as any);
        setShowAddOCSShellModal(false);
      } else {
        const originalShell = ocsShells.find(
          (shell: any) => shell.modelFromName === modelFromName
        );
        const _shells = [...shells];
        const index = shells.findIndex(shell => shell.id === id);
        const _isDiff = originalShell && isDiff(originalShell, shell);
        _shells.splice(index, 1, {
          ...shell,
          status:
            status === STATUS.New
              ? STATUS.New
              : _isDiff
              ? STATUS.Updated
              : STATUS.NotUpdated
        });
        keepRef.current.setFormValues({
          shells: _shells
        });
      }
      setShowAddOCSShellModal(false);
    };

    const handleShowEditRecordModal = (row: MagicKeyValue) => {
      setShowAddOCSShellModal(true);
      setIsEdit(true);
      setEditShell(row);
    };

    const handleShowDeleteRecordModal = (row: IOCSShell) => {
      setShowDeleteRecordModal(true);
      setDeleteShell(row);
    };

    const handleCloseDeleteModal = () => {
      setShowDeleteRecordModal(false);
      setShowBulkDeleteRecordModal(false);
      setDeleteShell(undefined);
    };

    const handleCloseCancelModal = () => {
      setShowConfirmCancelModal(false);
      setCancelShell(undefined);
    };

    const handleDeleteRecords = (shell?: IOCSShell) => {
      if (shell) {
        if (shell.status === STATUS.New) {
          const _shells = shells.filter(x => x.id !== shell.id);
          keepRef.current.setFormValues({ shells: _shells });
        } else {
          const _shells = shells.map(x =>
            x.id === shell.id ? { ...x, status: STATUS.MarkedForDeletion } : x
          );
          keepRef.current.setFormValues({ shells: _shells });
        }
      } else {
        const filterShells = shells.filter(
          x =>
            !checkedList.includes(x.id) ||
            (checkedList.includes(x.id) && x.status !== STATUS.New)
        );
        const _shells = filterShells.map(x =>
          checkedList.includes(x.id)
            ? { ...x, status: STATUS.MarkedForDeletion }
            : x
        );
        keepRef.current.setFormValues({ shells: _shells });
        onSetDataChecked([]);
      }
      handleCloseDeleteModal();
    };

    const handleShowCancelModal = useCallback((shell: IOCSShell) => {
      setShowConfirmCancelModal(true);
      setCancelShell(shell);
    }, []);

    const handleCancelRecord = useCallback(
      (shell: IOCSShell) => {
        const { modelFromName } = shell;
        const _shell = ocsShells?.find(
          (x: IOCSShell) => x.shellName === modelFromName
        );
        if (_shell) {
          const _shells = shells.filter(x => x.modelFromName !== modelFromName);
          keepRef.current.setFormValues({ shells: [..._shells, _shell] });
        }
        handleCloseCancelModal();
      },
      [shells, ocsShells]
    );

    const getInitialDatas = useCallback(() => {
      dispatch(actionsWorkflowSetup.getOCSShellList());
      dispatch(
        actionsWorkflowSetup.getWorkflowMetadata(Object.values(MetadataKey))
      );
      hasMetadata.current = true;
    }, [dispatch]);

    useEffect(() => {
      if (stepId === selectedStep) {
        keepRef.current.initialShells = shells;
      }
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [savedAt, selectedStep, stepId]);

    useEffect(() => {
      keepRef.current.setFormValues({ shells: ocsShells });
    }, [ocsShells]);

    useEffect(() => {
      !hasMetadata.current && getInitialDatas();
    }, [dispatch, getInitialDatas]);

    useEffect(() => {
      if (stepId === selectedStep) {
        onResetToDefaultFilter();
        setExpandedIds([]);
      }
    }, [stepId, selectedStep, onResetToDefaultFilter]);

    return (
      <div className={classnames({ loading })}>
        <div className="d-flex align-items-center">
          <h5>{t('txt_manage_ocs_shells_step_3_ocs_shell_list')}</h5>
          <div className="ml-auto">
            <SimpleSearch
              defaultValue={searchValue}
              onSearch={handleSearch}
              placeholder={t('txt_manage_ocs_shells_step_3_search_placeholder')}
              popperElement={
                <>
                  <p className="color-grey">{t('txt_search_description')}</p>
                  <ul className="search-field-item list-unstyled">
                    <li className="mt-16">
                      {t('txt_manage_ocs_shells_step_3_description')}
                    </li>
                    <li className="mt-16">
                      {t('txt_manage_ocs_shells_step_3_employee_id')}
                    </li>
                    <li className="mt-16">
                      {t('txt_manage_ocs_shells_step_3_operator_code')}
                    </li>
                    <li className="mt-16">
                      {t('txt_manage_ocs_shells_step_3_shell_name')}
                    </li>
                    <li className="mt-16">
                      {t('txt_manage_ocs_shells_step_3_transaction_profile')}
                    </li>
                  </ul>
                </>
              }
            />
          </div>
        </div>
        <div className="d-flex align-items-center justify-content-end mt-16 mr-n8">
          <Button
            variant="outline-primary"
            size="sm"
            onClick={handleShowAddRecordModal}
          >
            {t('txt_manage_ocs_shells_step_3_add_ocs_shell')}
          </Button>
          {!!searchValue && total > 0 && (
            <div className="text-right">
              <Button
                className="ml-8"
                variant="outline-primary"
                size="sm"
                onClick={handleClearAndReset}
              >
                {t('txt_clear_and_reset')}
              </Button>
            </div>
          )}
        </div>
        {total > 0 ? (
          <>
            <div className="d-flex align-items-center my-12">
              {!searchValue && <p className="py-4">{recordSelected}</p>}
              {checkedList.length > 1 && (
                <Button
                  variant="outline-danger"
                  size="sm"
                  className="ml-auto mr-n8"
                  onClick={() => setShowBulkDeleteRecordModal(true)}
                >
                  {t('txt_manage_ocs_shells_step_3_bulk_delete')}
                </Button>
              )}
            </div>

            <Grid
              className={gridClassName}
              variant={{
                id: 'id',
                type: 'checkbox',
                cellHeaderProps: { onClick: handleClickCheckAll }
              }}
              togglable
              expandedItemKey="id"
              expandedList={expandedIds}
              readOnlyCheckbox={readOnlyCheckbox}
              checkboxTooltipPropsList={readOnlyCheckbox?.map(id => ({
                id,
                element: t(
                  'txt_manage_ocs_shells_step_3_checkbox_readonly_tooltip'
                )
              }))}
              onExpand={setExpandedIds}
              subRow={(rowData: any) => (
                <ExpandedGrid
                  row={rowData.original}
                  originalData={ocsShells.find(
                    (shell: any) =>
                      shell.shellName === rowData.original?.modelFromName
                  )}
                />
              )}
              data={gridData}
              checkedList={checkedList}
              columns={columns}
              toggleButtonConfigList={gridData.map(
                mapGridExpandCollapse('id', t)
              )}
              sortBy={[sort]}
              onSortChange={onSortChange}
              onCheck={onSetDataChecked}
            />
            {total > DEFAULT_PAGE_SIZE && (
              <div className="mt-16">
                <Paging
                  page={currentPage}
                  pageSize={currentPageSize}
                  totalItem={total}
                  onChangePage={onPageChange}
                  onChangePageSize={onPageSizeChange}
                />
              </div>
            )}
          </>
        ) : (
          <div className="mt-40 mb-24">
            <NoDataFound
              id="no_ocs_shells_found"
              title={t(
                'txt_manage_ocs_shells_step_3_no_ocs_shells_to_displayed'
              )}
              hasSearch={!!searchValue}
              linkTitle={searchValue && t('txt_clear_and_reset')}
              onLinkClicked={handleClearAndReset}
            />
          </div>
        )}

        {showAddOCSShellModal && (
          <AddOCSShellModal
            show={showAddOCSShellModal}
            isEdit={isEdit}
            shell={editShell}
            title={
              isEdit
                ? t('txt_manage_ocs_shells_step_3_edit_ocs_shell')
                : t('txt_manage_ocs_shells_step_3_add_ocs_shell')
            }
            onSetShow={toggleAddOCSShellModal}
            onAddOCSShell={handleAddRecordToList}
            shells={shells}
          />
        )}

        {(showDeleteRecordModal || showBulkDeleteRecordModal) && (
          <RemoveOCSShellModal
            id="deleteOCSShellModal"
            show={showDeleteRecordModal || showBulkDeleteRecordModal}
            onClose={handleCloseDeleteModal}
            onDelete={handleDeleteRecords}
            isBulk={showBulkDeleteRecordModal}
            deleteShell={deleteShell}
          />
        )}

        {showConfirmCancelModal && (
          <CancelOCSShellModal
            id="cancelOCSShellModal"
            show={showConfirmCancelModal}
            onClose={handleCloseCancelModal}
            onCancelShell={handleCancelRecord}
            cancelShell={cancelShell!}
          />
        )}
      </div>
    );
  };

export default OCSShellList;
