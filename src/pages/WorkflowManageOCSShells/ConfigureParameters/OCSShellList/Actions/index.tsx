import { Button, useTranslation } from 'app/_libraries/_dls';
import { STATUS } from 'pages/WorkflowManageOCSShells/ConfigureParameters/constants';
import React from 'react';

export interface IActionsProps {
  data: IOCSShell;
  onEditRecord: (row: IOCSShell) => void;
  onDeleteRecord: (row: IOCSShell) => void;
  onCancelRecord: (row: IOCSShell) => void;
}
const Actions: React.FC<IActionsProps> = ({
  data,
  onCancelRecord,
  onDeleteRecord,
  onEditRecord
}) => {
  const { t } = useTranslation();

  return (
    <div className="d-flex justify-content-center">
      {data.status === STATUS.MarkedForDeletion ? (
        <>
          <Button
            variant="outline-danger"
            size="sm"
            onClick={() => onCancelRecord(data)}
          >
            {t('txt_cancel')}
          </Button>
        </>
      ) : (
        <>
          <Button
            variant="outline-primary"
            size="sm"
            onClick={() => onEditRecord(data)}
          >
            {t('txt_edit')}
          </Button>

          {data.status === STATUS.Updated ? (
            <Button
              variant="outline-danger"
              size="sm"
              onClick={() => onCancelRecord(data)}
            >
              {t('txt_cancel')}
            </Button>
          ) : (
            <Button
              variant="outline-danger"
              size="sm"
              onClick={() => onDeleteRecord(data)}
              className="ml-8"
            >
              {t('txt_delete')}
            </Button>
          )}
        </>
      )}
    </div>
  );
};

export default Actions;
