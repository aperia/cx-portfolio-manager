import { fireEvent, render } from '@testing-library/react';
import React from 'react';
import Actions, { IActionsProps } from '.';
import { STATUS } from '../../constants';

describe('Manage OCS shells > step 3 > OCSShellList > Action', () => {
  const renderComponent = (props: IActionsProps) => {
    return render(<Actions {...props} />);
  };

  it('Should render component with Status MarkedForDeletion', () => {
    const props: IActionsProps = {
      data: { status: STATUS.MarkedForDeletion },
      onEditRecord: jest.fn(),
      onDeleteRecord: jest.fn(),
      onCancelRecord: jest.fn()
    };
    const { getByText } = renderComponent(props);
    expect(getByText('txt_cancel')).toBeInTheDocument();
    fireEvent.click(getByText('txt_cancel'));
    expect(props.onCancelRecord).toBeCalled();
  });

  it('Should render component with Status Updated', () => {
    const props: IActionsProps = {
      data: { status: STATUS.Updated },
      onEditRecord: jest.fn(),
      onDeleteRecord: jest.fn(),
      onCancelRecord: jest.fn()
    };
    const { getByText } = renderComponent(props);
    expect(getByText('txt_edit')).toBeInTheDocument();
    fireEvent.click(getByText('txt_edit'));
    expect(props.onEditRecord).toBeCalled();

    expect(getByText('txt_cancel')).toBeInTheDocument();
    fireEvent.click(getByText('txt_cancel'));
    expect(props.onCancelRecord).toBeCalled();
  });

  it('Should render component with Status New or NotUpdated', () => {
    const props: IActionsProps = {
      data: { status: STATUS.New },
      onEditRecord: jest.fn(),
      onDeleteRecord: jest.fn(),
      onCancelRecord: jest.fn()
    };
    const { getByText } = renderComponent(props);
    expect(getByText('txt_edit')).toBeInTheDocument();
    fireEvent.click(getByText('txt_edit'));
    expect(props.onEditRecord).toBeCalled();

    expect(getByText('txt_delete')).toBeInTheDocument();
    fireEvent.click(getByText('txt_delete'));
    expect(props.onDeleteRecord).toBeCalled();
  });
});
