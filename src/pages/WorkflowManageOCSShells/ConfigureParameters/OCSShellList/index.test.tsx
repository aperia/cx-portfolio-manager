import { fireEvent, render } from '@testing-library/react';
import { ISimpleSearchProps } from 'app/components/SimpleSearch';
import * as CommonRedux from 'pages/_commons/redux/Common/select-hooks';
import * as SelectHooks from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageOCSShells';
import React from 'react';
import { act } from 'react-dom/test-utils';
import * as ReactRedux from 'react-redux';
import OCSShellList from '.';
import { STATUS } from '../constants';

jest.mock('app/components/SimpleSearch', () => {
  return {
    __esModule: true,
    default: ({
      defaultValue = '',
      placeholder = '',
      maxLength = 50,
      className = 'simple-search',
      clearTooltip = 'Clear Search Criteria',
      popperElement,
      onSearch
    }: ISimpleSearchProps) => {
      return (
        <div>
          <div>Simple Search</div>
          <input />
          <div onClick={() => onSearch('sadio')}>Search Sadio</div>
          <div onClick={() => onSearch('mane')}>Search Mane</div>
        </div>
      );
    }
  };
});

jest.mock('../AddOCSShellModal', () => {
  return {
    __esModule: true,
    default: ({ onSetShow, onAddOCSShell, shell }: any) => {
      const newShell: IOCSShell = {
        shellName: 'name'
      };
      const editShell: IOCSShell = {
        id: '1',
        shellName: 'ManeSadio'
      };
      return (
        <div>
          <div>AddOCSShellModal</div>
          <div onClick={() => onAddOCSShell(newShell)}>
            AddOCSShellModal Add Button
          </div>
          <div
            onClick={() => onAddOCSShell(shell.id === 1 ? editShell : shell)}
          >
            AddOCSShellModal Save Button
          </div>
          <div onClick={() => onSetShow()}>AddOCSShellModal Cancel Button</div>
        </div>
      );
    }
  };
});

jest.mock('../CancelOCSShellModal', () => {
  return {
    __esModule: true,
    default: ({ onClose, onCancelShell, cancelShell }: any) => (
      <div>
        <div>CancelOCSShellModal</div>
        <div onClick={() => onCancelShell(cancelShell)}>
          CancelOCSShellModal Confirm
        </div>
        <div onClick={() => onClose()}>CancelOCSShellModal Cancel</div>
      </div>
    )
  };
});

jest.mock('../RemoveOCSShellModal', () => {
  return {
    __esModule: true,
    default: ({ onClose, onDelete, deleteShell }: any) => (
      <div>
        <div>RemoveOCSShellModal</div>
        <div
          onClick={() => {
            onDelete(deleteShell);
          }}
        >
          RemoveOCSShellModal Delete
        </div>
        <div
          onClick={() => {
            onClose();
          }}
        >
          RemoveOCSShellModal Cancel
        </div>
      </div>
    )
  };
});

jest.mock('./Actions', () => {
  return {
    __esModule: true,
    default: ({ data, onCancelRecord, onDeleteRecord, onEditRecord }: any) => (
      <div>
        <div data-testid={`${data.id}_edit`} onClick={() => onEditRecord(data)}>
          Edit
        </div>
        <div
          data-testid={`${data.id}_delete`}
          onClick={() => onDeleteRecord(data)}
        >
          Delete
        </div>
        <div
          data-testid={`${data.id}_cancel`}
          onClick={() => onCancelRecord(data)}
        >
          Cancel
        </div>
      </div>
    )
  };
});

jest.mock('../ExpandedGrid', () => {
  return {
    __esModule: true,
    default: () => <div>ExpandedGrid</div>
  };
});

const mockUseSelectWindowDimension = jest.spyOn(
  CommonRedux,
  'useSelectWindowDimension'
);
const mockDispatch = jest.spyOn(ReactRedux, 'useDispatch');
const useSelectOCSShells = jest.spyOn(SelectHooks, 'useSelectOCSShells');

const mockUseSelectWindowDimensionImplementation = (width: number) => {
  mockUseSelectWindowDimension.mockImplementation(() => ({ width } as any));
};
describe('Manage OCS Shells > ConfigureParameters > OCS Shell List', () => {
  const ocsShellDataMockData = {
    ocsShells: [
      {
        shellName: 'MANE',
        transactionProfile: 'PROFILE3',
        fieldProfile: 'FLDPRO01',
        sysPrinProfile: '00200004',
        description: 'sit nonumy ex',
        accountProfile: '*DEFAULT',
        operatorCode: '357',
        singleSession: 'N',
        employeeId: 'EMPLOYEE2',
        supervisorShell: 'SHELL003',
        reportManagementSystemId: 'RMSID001',
        departmentId: 'DEPTID02',
        level: '02',
        masterSignOnOption: 'N',
        pwdExpirationOption: 'N',
        expireDate: '2022-06-25T00:00:00',
        neverDisable: 'Y',
        comment: 'sadipscing no stet volutpat sanctus in gubergren',
        initialPwd: '12345678',
        confirmPwd: '12345678',
        shellStatus: 'DISABLED',
        daysAllowed: ['Thursday', 'Monday', 'Tuesday'],
        timesAllowed: [],
        modelFromName: 'MANE'
      },
      {
        shellName: 'GJ841TLX',
        transactionProfile: 'PROFILE4',
        fieldProfile: 'FLDPRO04',
        sysPrinProfile: '00200002',
        description: 'vulputate gubergren no est sed',
        accountProfile: 'ACCTPRO3',
        operatorCode: '214',
        singleSession: 'N',
        employeeId: 'EMPLOYEE3',
        supervisorShell: 'SHELL004',
        reportManagementSystemId: 'RMSID002',
        departmentId: 'DEPTID04',
        level: '02',
        masterSignOnOption: 'Y',
        pwdExpirationOption: 'Y',
        expireDate: '2022-10-15T00:00:00',
        neverDisable: 'N',
        comment: 'et labore dolor sed lorem at kasd',
        initialPwd: '87654321',
        confirmPwd: '87654321',
        shellStatus: 'ENABLED',
        daysAllowed: ['Tuesday', 'Sunday', 'Thursday', 'Friday'],
        timesAllowed: ['12:30 AM - 1:00 AM'],
        check: true,
        modelFromName: 'GJ841TLX'
      },
      {
        shellName: 'KQOWBW89',
        transactionProfile: 'PROFILE3',
        fieldProfile: 'FLDPRO04',
        sysPrinProfile: '00200002',
        description: 'elitr ea vero delenit et magna',
        accountProfile: 'ACCTPRO3',
        operatorCode: '573',
        singleSession: 'Y',
        employeeId: 'EMPLOYEE1',
        supervisorShell: 'SHELL002',
        reportManagementSystemId: 'RMSID001',
        departmentId: 'DEPTID02',
        level: '02',
        masterSignOnOption: 'N',
        pwdExpirationOption: 'Y',
        expireDate: '2022-08-09T00:00:00',
        neverDisable: 'Y',
        comment: 'diam enim',
        initialPwd: '87654321',
        confirmPwd: '87654321',
        shellStatus: 'DISABLED',
        daysAllowed: ['Sunday', 'Thursday', 'Wednesday'],
        timesAllowed: ['12:00 AM - 12:30 AM']
      },
      {
        shellName: '7U7FLJMI',
        transactionProfile: 'PROFILE1',
        fieldProfile: 'FLDPRO03',
        sysPrinProfile: '00200003',
        description: 'voluptua amet tincidunt et lorem',
        accountProfile: '*DEFAULT',
        operatorCode: '418',
        singleSession: 'N',
        employeeId: 'EMPLOYEE4',
        supervisorShell: 'SHELL004',
        reportManagementSystemId: 'RMSID004',
        departmentId: 'DEPTID01',
        level: '01',
        masterSignOnOption: 'N',
        pwdExpirationOption: 'N',
        expireDate: '2022-09-19T00:00:00',
        neverDisable: 'Y',
        comment: 'sea ut dolore labore clita',
        initialPwd: '87654321',
        confirmPwd: '87654321',
        shellStatus: 'ENABLED',
        daysAllowed: ['Thursday', 'Wednesday'],
        timesAllowed: [],
        check: false
      },
      {
        shellName: '21WTDEV7',
        transactionProfile: 'PROFILE2',
        fieldProfile: 'FLDPRO04',
        sysPrinProfile: '00200003',
        description: 'dolor imperdiet gubergren et dolor',
        accountProfile: 'ACCTPRO2',
        operatorCode: '357',
        singleSession: 'Y',
        employeeId: 'EMPLOYEE3',
        supervisorShell: 'SHELL001',
        reportManagementSystemId: 'RMSID002',
        departmentId: 'DEPTID04',
        level: '03',
        masterSignOnOption: 'N',
        pwdExpirationOption: 'N',
        expireDate: '2022-09-16T00:00:00',
        neverDisable: 'N',
        comment: 'duo aliquyam feugiat',
        initialPwd: '87654321',
        confirmPwd: '87654321',
        shellStatus: 'ENABLED',
        daysAllowed: ['Saturday'],
        timesAllowed: ['12:30 AM - 1:00 AM'],
        check: false
      },
      {
        shellName: 'L1CPACWD',
        transactionProfile: 'PROFILE3',
        fieldProfile: 'FLDPRO02',
        sysPrinProfile: '00200003',
        description: 'sea liber et',
        accountProfile: 'ACCTPRO1',
        operatorCode: '418',
        singleSession: 'N',
        employeeId: 'EMPLOYEE4',
        supervisorShell: 'SHELL003',
        reportManagementSystemId: 'RMSID003',
        departmentId: 'DEPTID03',
        level: '03',
        masterSignOnOption: 'N',
        pwdExpirationOption: 'N',
        expireDate: '2022-05-04T00:00:00',
        neverDisable: 'N',
        comment: 'vel gubergren aliquyam in',
        initialPwd: '87654321',
        confirmPwd: '87654321',
        shellStatus: 'DISABLED',
        daysAllowed: ['Monday', 'Saturday'],
        timesAllowed: ['5:00 PM - 5:30 PM', '5:30 PM - 6:00 PM'],
        check: false
      },
      {
        shellName: 'C24ESNP1',
        transactionProfile: 'PROFILE2',
        fieldProfile: 'FLDPRO04',
        sysPrinProfile: '00200002',
        description: 'sadipscing iriure in duis',
        accountProfile: '*DEFAULT',
        operatorCode: '418',
        singleSession: 'Y',
        employeeId: 'EMPLOYEE1',
        supervisorShell: 'SHELL004',
        reportManagementSystemId: 'RMSID002',
        departmentId: 'DEPTID03',
        level: '01',
        masterSignOnOption: 'N',
        pwdExpirationOption: 'N',
        expireDate: '2022-12-07T00:00:00',
        neverDisable: 'Y',
        comment: 'diam lorem gubergren aliquyam',
        initialPwd: '12345678',
        confirmPwd: '12345678',
        shellStatus: 'ENABLED',
        daysAllowed: ['Tuesday', 'Thursday', 'Sunday', 'Friday'],
        timesAllowed: ['12:00 AM - 12:30 AM', '5:00 PM - 5:30 PM'],
        check: false
      },
      {
        shellName: 'L938OHPL',
        transactionProfile: 'PROFILE3',
        fieldProfile: 'FLDPRO04',
        sysPrinProfile: '00200002',
        description: 'ut elitr',
        accountProfile: 'ACCTPRO2',
        operatorCode: '357',
        singleSession: 'Y',
        employeeId: 'EMPLOYEE1',
        supervisorShell: 'SHELL002',
        reportManagementSystemId: 'RMSID003',
        departmentId: 'DEPTID04',
        level: '01',
        masterSignOnOption: 'Y',
        pwdExpirationOption: 'Y',
        expireDate: '2022-04-18T00:00:00',
        neverDisable: 'Y',
        comment: 'consequat ut et voluptua possim',
        initialPwd: '87654321',
        confirmPwd: '87654321',
        shellStatus: 'ENABLED',
        daysAllowed: ['Tuesday', 'Wednesday', 'Sunday', 'Thursday', 'Monday'],
        timesAllowed: [],
        check: false
      },
      {
        shellName: 'E4Y4SF4H',
        transactionProfile: 'PROFILE4',
        fieldProfile: 'FLDPRO02',
        sysPrinProfile: '00200004',
        description: 'ipsum nibh in clita nonumy',
        accountProfile: 'ACCTPRO2',
        operatorCode: '214',
        singleSession: 'Y',
        employeeId: 'EMPLOYEE3',
        supervisorShell: 'SHELL001',
        reportManagementSystemId: 'RMSID004',
        departmentId: 'DEPTID01',
        level: '01',
        masterSignOnOption: 'Y',
        pwdExpirationOption: 'Y',
        expireDate: '2022-11-13T00:00:00',
        neverDisable: 'N',
        comment: 'sit ut duis takimata commodo no',
        initialPwd: '12345678',
        confirmPwd: '12345678',
        shellStatus: 'ENABLED',
        daysAllowed: ['Friday', 'Saturday', 'Monday', 'Wednesday'],
        timesAllowed: ['5:30 PM - 6:00 PM', '5:00 PM - 5:30 PM'],
        check: false
      },
      {
        shellName: 'S56FJR9D',
        transactionProfile: 'PROFILE4',
        fieldProfile: 'FLDPRO03',
        sysPrinProfile: '00200002',
        description: 'lorem tempor labore lorem',
        accountProfile: 'ACCTPRO2',
        operatorCode: '214',
        singleSession: 'N',
        employeeId: 'EMPLOYEE1',
        supervisorShell: 'SHELL002',
        reportManagementSystemId: 'RMSID001',
        departmentId: 'DEPTID03',
        level: '01',
        masterSignOnOption: 'Y',
        pwdExpirationOption: 'Y',
        expireDate: '2022-07-23T00:00:00',
        neverDisable: 'N',
        comment: 'et stet kasd dolores',
        initialPwd: '87654321',
        confirmPwd: '87654321',
        shellStatus: 'ENABLED',
        daysAllowed: ['Saturday', 'Tuesday', 'Sunday', 'Thursday', 'Wednesday'],
        timesAllowed: ['12:30 AM - 1:00 AM', '5:30 PM - 6:00 PM'],
        check: false
      },
      {
        shellName: 'M62PJB64',
        transactionProfile: 'PROFILE1',
        fieldProfile: 'FLDPRO02',
        sysPrinProfile: '00200001',
        description: 'iusto magna sed',
        accountProfile: 'ACCTPRO2',
        operatorCode: '357',
        singleSession: 'Y',
        employeeId: 'EMPLOYEE2',
        supervisorShell: 'SHELL003',
        reportManagementSystemId: 'RMSID004',
        departmentId: 'DEPTID02',
        level: '02',
        masterSignOnOption: 'Y',
        pwdExpirationOption: 'N',
        expireDate: '2022-03-14T00:00:00',
        neverDisable: 'Y',
        comment: 'aliquyam eirmod veniam in est nulla sadipscing',
        initialPwd: '87654321',
        confirmPwd: '87654321',
        shellStatus: 'DISABLED*',
        daysAllowed: ['Monday', 'Sunday', 'Friday'],
        timesAllowed: ['12:30 AM - 1:00 AM'],
        check: false
      }
    ]
  };

  beforeEach(() => {
    mockDispatch.mockReturnValue(jest.fn());
    useSelectOCSShells.mockReturnValue(ocsShellDataMockData);
    mockUseSelectWindowDimensionImplementation(1400);
  });

  afterEach(() => {
    mockUseSelectWindowDimension.mockClear();
  });

  const renderComponent = (props: any) => {
    return render(<OCSShellList {...props} />);
  };

  it('Should render component', async () => {
    const _shells = ocsShellDataMockData.ocsShells.map((s, index) => ({
      ...s,
      status: STATUS.NotUpdated,
      id: index
    }));
    const props = {
      stepId: 'step',
      selectedStep: 'step',
      setFormValues: jest.fn(),
      formValues: {
        shells: _shells,
        originalShells: _shells
      },
      savedAt: 123
    } as any;
    const wrapper = renderComponent(props);
    const { getByText, getByTestId } = wrapper;

    expect(
      getByText('txt_manage_ocs_shells_step_3_ocs_shell_list')
    ).toBeInTheDocument();
    expect(getByText('Simple Search')).toBeInTheDocument();
    fireEvent.click(getByText('txt_manage_ocs_shells_step_3_add_ocs_shell'));
    expect(getByText('AddOCSShellModal')).toBeInTheDocument();
    fireEvent.click(getByTestId('1_delete'));
    expect(getByText('RemoveOCSShellModal')).toBeInTheDocument();
    fireEvent.click(getByTestId('1_cancel'));
    expect(getByText('CancelOCSShellModal')).toBeInTheDocument();
    fireEvent.click(getByTestId('1_edit'));
    expect(getByText('AddOCSShellModal')).toBeInTheDocument();

    const expandIcon = wrapper.baseElement.querySelector('i.icon.icon-plus');
    fireEvent.click(expandIcon!);
    expect(getByText('ExpandedGrid')).toBeInTheDocument();
  });

  it('Add / Edit an OCS Shell', async () => {
    mockUseSelectWindowDimensionImplementation(900);
    const _shells = ocsShellDataMockData.ocsShells.map((s, index) => ({
      ...s,
      status: index === 0 ? STATUS.New : STATUS.NotUpdated, // mock first element has status New
      id: index
    }));
    const props = {
      stepId: 'step',
      selectedStep: 'step',
      setFormValues: jest.fn(),
      formValues: {
        shells: _shells,
        originalShells: _shells
      },
      savedAt: 123
    } as any;
    const wrapper = renderComponent(props);
    const { getByText, getByTestId } = wrapper;

    expect(
      getByText('txt_manage_ocs_shells_step_3_ocs_shell_list')
    ).toBeInTheDocument();

    // add ocs shell
    fireEvent.click(getByText('txt_manage_ocs_shells_step_3_add_ocs_shell'));
    expect(getByText('AddOCSShellModal')).toBeInTheDocument();
    fireEvent.click(getByText('AddOCSShellModal Add Button'));
    // close add ocs shell
    fireEvent.click(getByText('txt_manage_ocs_shells_step_3_add_ocs_shell'));
    fireEvent.click(getByText('AddOCSShellModal Cancel Button'));

    // edit ocs shell with id 0 is new shell
    fireEvent.click(getByTestId('0_edit'));
    expect(getByText('AddOCSShellModal')).toBeInTheDocument();
    fireEvent.click(getByText('AddOCSShellModal Save Button'));

    // edit ocs shell with id 1 the will be updated
    fireEvent.click(getByTestId('1_edit'));
    expect(getByText('AddOCSShellModal')).toBeInTheDocument();
    fireEvent.click(getByText('AddOCSShellModal Save Button'));

    // edit ocs shell with id 2 the will be not updated
    fireEvent.click(getByTestId('2_edit'));
    expect(getByText('AddOCSShellModal')).toBeInTheDocument();
    fireEvent.click(getByText('AddOCSShellModal Save Button'));
    expect(props.setFormValues).toBeCalled();

    // fireEvent.click(getByTestId('1_delete'));
    // expect(getByText('RemoveOCSShellModal')).toBeInTheDocument();
    // fireEvent.click(getByTestId('1_cancel'));
    // expect(getByText('CancelOCSShellModal')).toBeInTheDocument();
  });

  it('Delete an OCS Shell', async () => {
    const _shells = ocsShellDataMockData.ocsShells.map((s, index) => ({
      ...s,
      status: index === 0 ? STATUS.New : STATUS.NotUpdated, // mock first element has status New
      id: index
    }));
    const props = {
      stepId: 'step',
      selectedStep: 'step',
      setFormValues: jest.fn(),
      formValues: {
        shells: _shells,
        originalShells: _shells
      },
      savedAt: 123
    } as any;
    const wrapper = renderComponent(props);
    const { getByText, getByTestId } = wrapper;

    expect(
      getByText('txt_manage_ocs_shells_step_3_ocs_shell_list')
    ).toBeInTheDocument();

    // delete a new shell
    fireEvent.click(getByTestId('0_delete'));
    expect(getByText('RemoveOCSShellModal')).toBeInTheDocument();
    fireEvent.click(getByText('RemoveOCSShellModal Cancel'));
    fireEvent.click(getByTestId('0_delete'));
    fireEvent.click(getByText('RemoveOCSShellModal Delete'));

    // delete a not update shell
    fireEvent.click(getByTestId('1_delete'));
    expect(getByText('RemoveOCSShellModal')).toBeInTheDocument();
    fireEvent.click(getByText('RemoveOCSShellModal Cancel'));
    fireEvent.click(getByTestId('1_delete'));
    fireEvent.click(getByText('RemoveOCSShellModal Delete'));

    // bulk delete
    const headerCheckbox = document.querySelector('th .dls-checkbox-input');
    fireEvent.click(headerCheckbox!);
    fireEvent.click(getByText('txt_manage_ocs_shells_step_3_bulk_delete'));
    expect(getByText('RemoveOCSShellModal')).toBeInTheDocument();
    fireEvent.click(getByText('RemoveOCSShellModal Delete'));

    // fireEvent.click(getByTestId('1_cancel'));
    // expect(getByText('CancelOCSShellModal')).toBeInTheDocument();
  });

  it('Cancel an OCS Shell', async () => {
    const _shells = ocsShellDataMockData.ocsShells
      .slice(0, 3)
      .map((s, index) => ({
        ...s,
        status: index === 0 ? STATUS.MarkedForDeletion : STATUS.Updated,
        id: index
      }));
    const props = {
      stepId: 'step',
      selectedStep: 'step',
      setFormValues: jest.fn(),
      formValues: {
        shells: _shells,
        originalShells: _shells.slice(0, 2)
      },
      savedAt: 123
    } as any;
    const wrapper = renderComponent(props);
    const { getByText, getByTestId } = wrapper;

    expect(
      getByText('txt_manage_ocs_shells_step_3_ocs_shell_list')
    ).toBeInTheDocument();

    // Open and close CancelOCSShellModal
    fireEvent.click(getByTestId('0_cancel'));
    expect(getByText('CancelOCSShellModal')).toBeInTheDocument();
    fireEvent.click(getByText('CancelOCSShellModal Cancel'));

    // Cancel a MarkedForDeletion shell
    fireEvent.click(getByTestId('0_cancel'));
    expect(getByText('CancelOCSShellModal')).toBeInTheDocument();
    fireEvent.click(getByText('CancelOCSShellModal Confirm'));

    // Cancel a Updated shell
    fireEvent.click(getByTestId('1_cancel'));
    expect(getByText('CancelOCSShellModal')).toBeInTheDocument();
    fireEvent.click(getByText('CancelOCSShellModal Confirm'));

    // Cancel a Updated shell not have original shell
    fireEvent.click(getByTestId('2_cancel'));
    expect(getByText('CancelOCSShellModal')).toBeInTheDocument();
    fireEvent.click(getByText('CancelOCSShellModal Confirm'));
  });

  it('Search an ocs shell', async () => {
    const _shells = ocsShellDataMockData.ocsShells
      .slice(0, 3)
      .map((s, index) => ({
        ...s,
        status: STATUS.MarkedForDeletion,
        id: index
      }));
    const props = {
      stepId: 'step',
      selectedStep: 'step',
      setFormValues: jest.fn(),
      formValues: {
        shells: _shells,
        originalShells: _shells
      },
      savedAt: 123
    } as any;
    const wrapper = renderComponent(props);
    const { getByText } = wrapper;

    expect(
      getByText('txt_manage_ocs_shells_step_3_ocs_shell_list')
    ).toBeInTheDocument();
    expect(getByText('Simple Search')).toBeInTheDocument();
    fireEvent.click(getByText('Search Sadio'));
    expect(getByText('txt_clear_and_reset')).toBeInTheDocument();
    fireEvent.click(getByText('Search Mane'));
    const headerCheckbox = document.querySelector('th .dls-checkbox-input');
    act(() => {
      fireEvent.click(headerCheckbox!);
    });

    expect(getByText('txt_clear_and_reset')).toBeInTheDocument();
    fireEvent.click(getByText('txt_clear_and_reset'));
  });

  it('Should render compoent when select other step', async () => {
    useSelectOCSShells.mockReturnValue(undefined);
    const _shells = ocsShellDataMockData.ocsShells
      .slice(0, 3)
      .map((s, index) => ({
        ...s,
        status: STATUS.MarkedForDeletion,
        id: index
      }));
    const props = {
      stepId: 'step1',
      selectedStep: 'step2',
      setFormValues: jest.fn(),
      formValues: {
        shells: _shells,
        originalShells: _shells
      },
      savedAt: 123
    } as any;
    const wrapper = renderComponent(props);
    const { getByText } = wrapper;

    expect(
      getByText('txt_manage_ocs_shells_step_3_ocs_shell_list')
    ).toBeInTheDocument();
  });

  it('Should render record selected text', async () => {
    useSelectOCSShells.mockReturnValue(undefined);
    const _shells = ocsShellDataMockData.ocsShells
      .slice(0, 2)
      .map((s, index) => ({
        ...s,
        status: STATUS.NotUpdated,
        id: index
      }));
    const props = {
      stepId: 'step1',
      selectedStep: 'step1',
      setFormValues: jest.fn(),
      formValues: {
        shells: _shells,
        originalShells: _shells
      },
      savedAt: 123
    } as any;
    const wrapper = renderComponent(props);
    const { getByText } = wrapper;

    expect(
      getByText('txt_manage_ocs_shells_step_3_ocs_shell_list')
    ).toBeInTheDocument();
    expect(
      getByText('txt_manage_ocs_shells_step_3_ocs_shells_selected')
    ).toBeInTheDocument();

    const headerCheckbox = document.querySelector('th .dls-checkbox-input');
    fireEvent.click(headerCheckbox!);
    expect(
      getByText('txt_manage_ocs_shells_step_3_all_ocs_shells_types_selected')
    ).toBeInTheDocument();
    const firstRowCheckbox = document.querySelector(
      'tbody .dls-checkbox-input'
    );
    fireEvent.click(firstRowCheckbox!);
    expect(
      getByText('txt_manage_ocs_shells_step_3_ocs_shell_selected')
    ).toBeInTheDocument();
  });

  it('Should render check box all when has search value', async () => {
    useSelectOCSShells.mockReturnValue(undefined);
    const _shells = ocsShellDataMockData.ocsShells
      .slice(0, 3)
      .map((s, index) => ({
        ...s,
        status: STATUS.NotUpdated,
        id: index,
        shellName: 'Mane'
      }));
    const props = {
      stepId: 'step1',
      selectedStep: 'step1',
      setFormValues: jest.fn(),
      formValues: {
        shells: _shells,
        originalShells: _shells
      },
      savedAt: 123
    } as any;
    const wrapper = renderComponent(props);
    const { getByText } = wrapper;

    expect(
      getByText('txt_manage_ocs_shells_step_3_ocs_shell_list')
    ).toBeInTheDocument();

    fireEvent.click(getByText('Search Mane'));
    const headerCheckbox = document.querySelector('th .dls-checkbox-input');
    fireEvent.click(headerCheckbox!);
    fireEvent.click(headerCheckbox!);
    const firstRowCheckbox = document.querySelector(
      'tbody .dls-checkbox-input'
    );
    fireEvent.click(firstRowCheckbox!);
  });
});
