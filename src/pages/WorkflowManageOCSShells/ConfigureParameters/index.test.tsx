import { render } from '@testing-library/react';
import * as WorkflowSetupSelectHooks from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageOCSShells';
import React from 'react';
import ExtraStaticManageOCSShellsStep, { ManageOCSShellsFormValue } from '.';
import { STATUS } from './constants';

jest.mock('./OCSShellList', () => ({
  __esModule: true,
  default: () => <div>OCSShellList</div>
}));

jest.mock('./UploadSection', () => ({
  __esModule: true,
  default: () => <div>UploadSection</div>
}));
const useSelectOCSShellsMock = jest.spyOn(
  WorkflowSetupSelectHooks,
  'useSelectOCSShells'
);

describe('Manage OCS Shells > ConfigureParameters', () => {
  const renderComponent = (
    props: WorkflowSetupProps<ManageOCSShellsFormValue>
  ) => {
    return render(<ExtraStaticManageOCSShellsStep {...props} />);
  };
  beforeEach(() => {
    useSelectOCSShellsMock.mockReturnValue(undefined as any);
  });

  it('Should render component is selected step and valid', () => {
    const props = {
      formValues: {
        attachmentId: 'id',
        shells: ''
      },
      setFormValues: jest.fn(),
      stepId: 'stepId',
      selectedStep: 'stepId'
    } as any;
    const { getByText } = renderComponent(props);

    expect(getByText('OCSShellList')).toBeInTheDocument();
    expect(getByText('UploadSection')).toBeInTheDocument();
  });

  it('Should render component is selected step and invalid', () => {
    const props = {
      formValues: {
        attachmentId: '',
        shells: [{ status: STATUS.NotUpdated }]
      },
      setFormValues: jest.fn(),
      stepId: 'stepId',
      selectedStep: 'stepId'
    } as any;
    const { getByText } = renderComponent(props);

    expect(getByText('OCSShellList')).toBeInTheDocument();
    expect(getByText('UploadSection')).toBeInTheDocument();
  });

  it('Should render component is NOT selected step', () => {
    const props = {
      formValues: {
        attachmentId: '',
        shells: [{ status: STATUS.NotUpdated }]
      },
      setFormValues: jest.fn(),
      stepId: 'stepId',
      selectedStep: 'stopId'
    } as any;
    const { getByText } = renderComponent(props);

    expect(getByText('OCSShellList')).toBeInTheDocument();
    expect(getByText('UploadSection')).toBeInTheDocument();
  });
});
