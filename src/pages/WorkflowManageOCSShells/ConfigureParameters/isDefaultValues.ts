import { isEqual } from 'lodash';
import { ManageOCSShellsFormValue } from '.';
import { STATUS } from './constants';

const isDefaultValues: WorkflowSetupIsDefaultValueFunc<ManageOCSShellsFormValue> =
  (formValues, defaultValues) => {
    const newDefault: ManageOCSShellsFormValue = {
      ...formValues,
      shells:
        formValues.shells?.filter(s => s.status !== STATUS.NotUpdated) || []
    };

    return isEqual(newDefault, defaultValues);
  };

export default isDefaultValues;
