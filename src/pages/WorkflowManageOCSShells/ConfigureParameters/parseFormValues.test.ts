import { workflowTemplateService } from 'app/services';
import parseFormValues from './parseFormValues';

describe('Manage OCS Shells > Configure Parameters > parseFormValues', () => {
  it('has attachmentId', async () => {
    const fileResponse = {
      id: 'id',
      filename: 'filename',
      fileSize: 'fileSize',
      mineType: 'mineType'
    };
    jest.spyOn(workflowTemplateService, 'getFileDetails').mockResolvedValue({
      data: { attachments: [fileResponse] }
    } as any);
    const id = 'step3';
    const workflowInstanceData = {
      data: {
        configurations: { attachmentId: 'attachmentId' },
        workflowSetupData: [
          {
            id: 'step3',
            status: 'DONE'
          }
        ]
      }
    } as any;
    const result = await parseFormValues(workflowInstanceData, id, jest.fn());
    expect(result.attachmentId).toEqual('attachmentId');
  });

  it('has attachmentId with successResponse', async () => {
    const fileResponse = {
      id: 'id',
      filename: 'filename',
      fileSize: 'fileSize',
      mineType: 'mineType',
      status: 'ready'
    };
    jest.spyOn(workflowTemplateService, 'getFileDetails').mockResolvedValue({
      data: { attachments: [fileResponse] }
    } as any);
    const workflowInstanceData: any = {
      data: {
        configurations: { attachmentId: 'attachmentId' },
        workflowSetupData: [{ id: 'id', data: { file: { id: 'file' } } }]
      }
    };
    const id = 'id';
    const result = await parseFormValues(workflowInstanceData, id, jest.fn());

    expect(result.attachmentId).toEqual('attachmentId');
  });

  it('should parse Data has step values', async () => {
    const id = 'step3';
    const workflowInstanceData = {
      data: {
        workflowSetupData: [
          {
            id: 'step3',
            status: 'DONE'
          }
        ]
      }
    } as any;
    const result = await parseFormValues(workflowInstanceData, id, jest.fn());
    expect(result).toEqual({
      attachmentId: undefined,
      files: [],
      isPass: true,
      isSelected: false,
      isValid: false,
      shells: []
    });
  });

  it('should parse data Not have step values', async () => {
    const id = 'step3';
    const workflowInstanceData = {
      data: {
        workflowSetupData: [
          {
            id: 'step2',
            status: 'DONE'
          }
        ]
      }
    } as any;
    const result = await parseFormValues(workflowInstanceData, id, jest.fn());
    expect(result).toBeUndefined();
  });
});
