import { render } from '@testing-library/react';
import {
  MetadataKey,
  STATUS
} from 'pages/WorkflowManageOCSShells/ConfigureParameters/constants';
import * as CommonSelectHooks from 'pages/_commons/redux/Common/select-hooks';
import * as WorkflowSetupSelectHooks from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageOCSShells';
import React from 'react';
import ExpandedGrid, { IExpanedGridProps } from '.';

const useSelectWindowDimensionMock = jest.spyOn(
  CommonSelectHooks,
  'useSelectWindowDimension'
);
const useSelectMetadataForManageOCSShellsMock = jest.spyOn(
  WorkflowSetupSelectHooks,
  'useSelectMetadataForManageOCSShells'
);

describe('Manage OCS Shells > ConfigureParameters > ExpandedGrid', () => {
  const renderComponent = (props: IExpanedGridProps) => {
    return render(<ExpandedGrid {...props} />);
  };
  beforeEach(() => {
    useSelectWindowDimensionMock.mockReturnValue({ width: 1024, height: 1080 });
    useSelectMetadataForManageOCSShellsMock.mockReturnValue({
      [MetadataKey.SingleSession]: [{ code: 'code', text: 'text' }]
    });
  });
  it('Should render component with status is Updated', () => {
    const row: IOCSShell = {
      shellName: 'Shell Name',
      status: STATUS.Updated
    };
    const { getByText, queryByText } = renderComponent({
      row,
      originalData: {
        [MetadataKey.SingleSession]: [{ code: 'text', text: 'text' }]
      }
    });
    expect(
      getByText('txt_manage_ocs_shells_step_3_previous_value')
    ).toBeInTheDocument();
    expect(
      getByText('txt_manage_ocs_shells_step_3_new_value')
    ).toBeInTheDocument();
    expect(
      queryByText('txt_manage_ocs_shells_step_3_value')
    ).not.toBeInTheDocument();
  });

  it('Should render component with status is Not Updated', () => {
    const row: IOCSShell = {
      shellName: 'Shell Name',
      status: STATUS.NotUpdated
    };
    const { getByText, queryByText } = renderComponent({ row });
    expect(
      queryByText('txt_manage_ocs_shells_step_3_previous_value')
    ).not.toBeInTheDocument();
    expect(
      queryByText('txt_manage_ocs_shells_step_3_new_value')
    ).not.toBeInTheDocument();
    expect(getByText('txt_manage_ocs_shells_step_3_value')).toBeInTheDocument();
  });
});
