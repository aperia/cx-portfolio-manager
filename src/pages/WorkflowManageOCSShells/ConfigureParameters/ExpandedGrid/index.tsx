import { Grid, useTranslation } from 'app/_libraries/_dls';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import { useSelectMetadataForManageOCSShells } from 'pages/_commons/redux/WorkflowSetup';
import React from 'react';
import { STATUS } from '../constants';
import DefaultSubGridColumns from '../constants/DefaultSubGridColumns';
import DefaultSubGridData from '../constants/DefaultSubGridData';
import accessor from './accessor';

export interface IExpanedGridProps {
  row: IOCSShell;
  originalData?: IOCSShell;
}

const ExpandedGrid: React.FC<IExpanedGridProps> = ({ row, originalData }) => {
  const { status } = row;
  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();
  const metadata = useSelectMetadataForManageOCSShells();

  const defaultColumns = DefaultSubGridColumns(
    accessor(metadata),
    t,
    width,
    false
  );
  const columns =
    status === STATUS.Updated
      ? defaultColumns.filter(column => column.id !== 'value')
      : defaultColumns.filter(
          column => column.id !== 'previousValue' && column.id !== 'newValue'
        );

  const data = DefaultSubGridData(row, t, originalData);

  return (
    <div className="p-20">
      <Grid columns={columns} data={data} />
    </div>
  );
};

export default ExpandedGrid;
