import MaskedPassword from 'app/components/MaskedPassword';
import { formatTime } from 'app/helpers';
import React from 'react';
import { truncateAccessor } from '../constants/DefaultSubGridColumns';
import {
  InputControlType,
  ISubGridData
} from '../constants/DefaultSubGridData';

const accessor =
  (metadata: { [key: string]: RefData[] }) =>
  (rowData: ISubGridData, field: keyof ISubGridData, t: any): JSX.Element => {
    const { id, inputControlType, keyOptions } = rowData;
    const options = metadata && keyOptions ? metadata[keyOptions] : [];
    let value = rowData[field];
    if (id === 'expireDate') {
      value = formatTime(value).date;
    } else if (id === 'daysAllowed' || id === 'timesAllowed') {
      value = value?.join(', ');
    }

    if (
      inputControlType === InputControlType.Combobox ||
      inputControlType === InputControlType.DropdownList
    ) {
      value = options?.find(i => i.code === value)?.text;
    }

    if (inputControlType === InputControlType.Password) {
      return <MaskedPassword value={value?.toString()} />;
    } else {
      return truncateAccessor({ ...rowData, [field]: value }, field, t);
    }
  };

export default accessor;
