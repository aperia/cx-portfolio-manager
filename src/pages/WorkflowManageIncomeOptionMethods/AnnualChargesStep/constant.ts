import {
  MethodFieldNameEnum,
  MethodFieldParameterEnum
} from 'app/constants/enums';
import {
  ParameterIncomeOptionMethodsGroup,
  ParameterIncomeOptionMethodsList,
  ParameterIncomeOptionMethodsListIds
} from './types';

export const ONLINE_PCF = {
  //Ac - Statement and Notification Display
  ManageIncomeOptionMethodsAcAnnualChargeStatementDisplayControl:
    'Statement Display Control',
  ManageIncomeOptionMethodsAcPreAnnualChargeNotifications:
    'Pre-Annual Charge Notification',
  ManageIncomeOptionMethodsAcRegulationZNotificationMediaMethod:
    'Notification Media Method',
  ManageIncomeOptionMethodsAcRegulationZnotificationGenerationPeriod:
    'Notification Generation Period',
  ManageIncomeOptionMethodsAcPreNotificationMessageId: 'Statement Message',
  ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine1: 'Line 1',
  ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine2Before:
    'Line 2 (Preceding)',
  ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine2After:
    'Line 2',
  ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine3: 'Line 3',
  ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine4: 'Line 4',
  ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine5: 'Line 5',
  ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine6: 'Line 6',
  ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine7: 'Line 7',
  ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine8: 'Line 8',
  ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine9: 'Line 9',
  ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine10: 'Line 10',
  ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine11: 'Line 11',
  ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine12: 'Line 12',
  ManageIncomeOptionMethodsAcAnnualChargeStatementDescription:
    'Statement Description',
  //Ac - Assessment Calculations
  ManageIncomeOptionMethodsAcBatchTypeCode: 'Batch Type',
  ManageIncomeOptionMethodsAcIncludeAnnualChargeInInterestCalculations:
    'Interest Bearing',
  ManageIncomeOptionMethodsAcPreventDelinquencyFromAnnualFeeBalance:
    'Block Delinquency for Fee Only',
  ManageIncomeOptionMethodsAcAnnualChargeOption: 'Annual Charge Option',
  ManageIncomeOptionMethodsAcFirstYearMaximumFeeManagementOption:
    'AC 1st Year Max Fee Mgmt',
  ManageIncomeOptionMethodsAcPrenotificationAmountChangeOptions:
    'Pre-Note Amount Change Code',
  ManageIncomeOptionMethodsAcAmount1: 'Amount 1',
  ManageIncomeOptionMethodsAcAmount2: 'Amount 2',
  ManageIncomeOptionMethodsAcAmount3: 'Amount 3',
  ManageIncomeOptionMethodsAcAmount4: 'Amount 4',
  ManageIncomeOptionMethodsAcAmount5: 'Amount 5',
  ManageIncomeOptionMethodsAcAmount6: 'Amount 6',
  ManageIncomeOptionMethodsAcAdditionalPlasticCharge:
    'Additional Plastic Charge',
  //AC - Account Eligibility
  ManageIncomeOptionMethodsAcPlasticsRequirementCode: 'Plastics Requirement',
  ManageIncomeOptionMethodsAcSpecialConditionsForExternalStatus:
    'External Status',
  ManageIncomeOptionMethodsAcSpecialConditionsForExpiredAccounts:
    'Expired Accounts',
  ManageIncomeOptionMethodsAcAnnualChargeIncludeTable: 'Status Reason Table',
  ManageIncomeOptionMethodsAcFinanceChargeSuppressionCode:
    'Finance Charge Suppression Code',
  ManageIncomeOptionMethodsAcNoPrenotificationAssessmentOptions:
    'No Pre-Notification Code',
  //AC - Assessment Timing
  ManageIncomeOptionMethodsAcExistingAccountStartMonthOption:
    'Existing Account Start Month',
  ManageIncomeOptionMethodsAcNumberOfMonthsToFirstCharge: 'Number of Months',
  ManageIncomeOptionMethodsAcPostingCycleOptions: 'Posting Cycle',
  ManageIncomeOptionMethodsAcFeeTimingOptions: 'Fee Generation Timing',
  //AC - Advanced Parameters - Assessment Timing
  ManageIncomeOptionMethodsAcSpecifiedMonthForAnnualCharge: 'Charge Month',
  ManageIncomeOptionMethodsAcFeeWaiverMonths: 'Fee Waive Option Months',
  ManageIncomeOptionMethodsAcSecondAnnualFeeNumberOfDays:
    'Second Annual Fee No of Days',
  //AC - Advanced Parameters - Assessment Calculations
  ManageIncomeOptionMethodsAcDebitActivityOption: 'Debit Activity',
  ManageIncomeOptionMethodsAcWaiverTotalDollarCalculationOption:
    'Fee Waive Total Dollars Option',
  ManageIncomeOptionMethodsAcRateChangePeriod: 'Rate Change Period',
  //AC - Advanced Parameters - Account Eligibility
  ManageIncomeOptionMethodsAcFeeWaiverOption: 'Fee Waiver Option',
  ManageIncomeOptionMethodsAcMinimumBalanceAndExternalStatusOption:
    'Minimum Balance/Ext Status Flag',
  ManageIncomeOptionMethodsAcFeeWaiverAmount: 'Fee Waive Option Amount',
  ManageIncomeOptionMethodsAcStatusAMinimumBalance:
    'External Status A (Auth Prohibited) Minimum Amount',
  ManageIncomeOptionMethodsAcStatusCMinimumBalance:
    'External Status C (Closed) Minimum Amount',
  ManageIncomeOptionMethodsAcStatusEMinimumBalance:
    'External Status E (Revoked) Minimum Amount',
  ManageIncomeOptionMethodsAcStatusFMinimumBalance:
    'External Status F (Frozen) Minimum Amount',
  ManageIncomeOptionMethodsAcStatusBlankMinimumBalance:
    'External Status Blank (Normal) Minimum Amount',
  //AC - Advanced Parameters -  Reversal Options
  ManageIncomeOptionMethodsAcReversalStatementMessageWarningTextID:
    'Annual Fee Reversal Msg Text ID',
  ManageIncomeOptionMethodsAcReversalNumberOfCycles:
    'Annual Fee Reversal Number of Cycles',
  ManageIncomeOptionMethodsAcReversalReasonCode:
    'Annual Fee Reversal Reason Code',
  ManageIncomeOptionMethodsAcReversalStatementMessageWarningOption:
    'Annual Fee Reversal Statement',
  ManageIncomeOptionMethodsAcAnnualFeeReversalBatchID:
    'Annual Fee Reversal Batch ID'
};

export const MORE_INFO = {
  //Ac - Statement and Notification Display
  ManageIncomeOptionMethodsAcAnnualChargeStatementDisplayControl:
    'The Annual Charge Statement Display Control parameter identifies the source of description text for the annual charge detail on cardholder statements.',
  ManageIncomeOptionMethodsAcPreAnnualChargeNotifications:
    'The Pre-annual Charge Notifications parameter determines which cardholders receive prenotification of annual charges.',
  ManageIncomeOptionMethodsAcRegulationZNotificationMediaMethod:
    'The Regulation Z Notification Media Method parameter determines the method of notification sent to your cardholders for Regulation Z Annual Charge Notification.',
  ManageIncomeOptionMethodsAcRegulationZnotificationGenerationPeriod:
    'The Regulation Z Notification Generation Period parameter determines when Federal Regulation Z annual charge notifications post to cardholder accounts.',
  ManageIncomeOptionMethodsAcPreNotificationMessageId:
    'The Pre-Notification Message ID parameter controls prenotification statement messages for annual charges applied to cardholder accounts. If you set the Notification Media Method parameter to 02 or 05, the System includes the message identified by the text identifier on the cardholder statement. If you set this parameter to 03 or 06, the System includes the free form text message.',
  ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine1:
    'The Statement Pre-Notification Message - Line 1 parameter contains the text for the first line of the freeform message used to disclose annual charge information on cardholder statements.',
  ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine2Before:
    'The Statement Pre-Notification Message - Line 2 (Before Hard-Coded Dollar Amount) parameter contains the text preceding the annual charge amount on the second line of the freeform message used to disclose annual charge information on cardholder statements.',
  ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine2After:
    'The Statement Pre-Notification Message - Line 2 (After Hard-Coded Dollar Amount) parameter contains the text following the annual charge amount on the second line of the freeform message used to disclose annual charge information on cardholder statements.',
  ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine3:
    'The Line 3 through Line 12 parameters contain the text for the various lines of freeform messages used to disclose annual charge information on cardholder statements.',
  ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine4:
    'The Line 3 through Line 12 parameters contain the text for the various lines of freeform messages used to disclose annual charge information on cardholder statements.',
  ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine5:
    'The Line 3 through Line 12 parameters contain the text for the various lines of freeform messages used to disclose annual charge information on cardholder statements.',
  ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine6:
    'The Line 3 through Line 12 parameters contain the text for the various lines of freeform messages used to disclose annual charge information on cardholder statements.',
  ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine7:
    'The Line 3 through Line 12 parameters contain the text for the various lines of freeform messages used to disclose annual charge information on cardholder statements.',
  ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine8:
    'The Line 3 through Line 12 parameters contain the text for the various lines of freeform messages used to disclose annual charge information on cardholder statements.',
  ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine9:
    'The Line 3 through Line 12 parameters contain the text for the various lines of freeform messages used to disclose annual charge information on cardholder statements.',
  ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine10:
    'The Line 3 through Line 12 parameters contain the text for the various lines of freeform messages used to disclose annual charge information on cardholder statements.',
  ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine11:
    'The Line 3 through Line 12 parameters contain the text for the various lines of freeform messages used to disclose annual charge information on cardholder statements.',
  ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine12:
    'The Line 3 through Line 12 parameters contain the text for the various lines of freeform messages used to disclose annual charge information on cardholder statements.',
  ManageIncomeOptionMethodsAcAnnualChargeStatementDescription:
    'The Annual Charge Statement Description parameter contains the message that appears before the date in the annual charge message on the cardholder statement if you set the Statement Display Control parameter in this section to C.',
  //Ac - Assessment Calculations
  ManageIncomeOptionMethodsAcBatchTypeCode:
    'The Batch Type Code parameter indicates how annual charge transactions are entered into the System. You can enter batch types as essentially Sales (1) or Adjustments (4, or 5). Choosing value 4 means the adjustment will "hit" both the cardholder account and the merchant account, whereas value 5 will only "hit" the merchant account.',
  ManageIncomeOptionMethodsAcIncludeAnnualChargeInInterestCalculations:
    'The Include Annual Charge in Interest Calculations parameter determines whether or not annual charges are interest-bearing. In some situations, issuers will exclude the annual charge from interest as part of a grace period (option 2).',
  ManageIncomeOptionMethodsAcPreventDelinquencyFromAnnualFeeBalance:
    'The Prevent Delinquency from Annual Fee Balance parameter allows you to prevent accounts from becoming delinquent if the annual charge is not paid.',
  ManageIncomeOptionMethodsAcAnnualChargeOption:
    'The Annual Charge Option parameter determines whether cardholders are assessed an annual charge on a monthly or a yearly basis.',
  ManageIncomeOptionMethodsAcFirstYearMaximumFeeManagementOption:
    'The First Year Maximum Fee Management Option parameter determines whether annual charges should be included in first year maximum fee management.',
  ManageIncomeOptionMethodsAcPrenotificationAmountChangeOptions:
    'The Prenotification Amount Change Options parameter controls the action the System takes when the annual charge amount disclosed in the advance notice does not match the statement amount.',
  ManageIncomeOptionMethodsAcAmount1:
    'The Amount 1 through Amount 6 parameters contain the amounts that correspond to annual charge fields on the customer account record. For example, the Amount 4 parameter contains the amount to charge if the annual charge field on the customer account record is set to 4.',
  ManageIncomeOptionMethodsAcAmount2:
    'The Amount 1 through Amount 6 parameters contain the amounts that correspond to annual charge fields on the customer account record. For example, the Amount 4 parameter contains the amount to charge if the annual charge field on the customer account record is set to 4.',
  ManageIncomeOptionMethodsAcAmount3:
    'The Amount 1 through Amount 6 parameters contain the amounts that correspond to annual charge fields on the customer account record. For example, the Amount 4 parameter contains the amount to charge if the annual charge field on the customer account record is set to 4.',
  ManageIncomeOptionMethodsAcAmount4:
    'The Amount 1 through Amount 6 parameters contain the amounts that correspond to annual charge fields on the customer account record. For example, the Amount 4 parameter contains the amount to charge if the annual charge field on the customer account record is set to 4.',
  ManageIncomeOptionMethodsAcAmount5:
    'The Amount 1 through Amount 6 parameters contain the amounts that correspond to annual charge fields on the customer account record. For example, the Amount 4 parameter contains the amount to charge if the annual charge field on the customer account record is set to 4.',
  ManageIncomeOptionMethodsAcAmount6:
    'The Amount 1 through Amount 6 parameters contain the amounts that correspond to annual charge fields on the customer account record. For example, the Amount 4 parameter contains the amount to charge if the annual charge field on the customer account record is set to 4.',
  ManageIncomeOptionMethodsAcAdditionalPlasticCharge:
    'The Additional Plastic Charge parameter specifies the annual charge for more than one plastic issued on an account. This amount is combined with the amount parameters in this section.',
  //AC - Account Eligibility
  ManageIncomeOptionMethodsAcPlasticsRequirementCode:
    'The Plastics Requirement Code parameter determines whether accounts must have plastics to be assessed an annual charge.',
  ManageIncomeOptionMethodsAcSpecialConditionsForExternalStatus:
    'The Special Conditions for External Status parameter determines whether customer accounts with an external status code receive notification of the annual charge.',
  ManageIncomeOptionMethodsAcSpecialConditionsForExpiredAccounts:
    'The Special Conditions for Expired Accounts parameter determines whether expired customer accounts receive notification of the annual charge and whether the System assesses the annual charge.',
  ManageIncomeOptionMethodsAcAnnualChargeIncludeTable:
    'The Annual Charge "Include" Table parameter determines whether to waive the annual charge for cardholder accounts according to your settings on a status/reason code table.',
  ManageIncomeOptionMethodsAcFinanceChargeSuppressionCode:
    'The Finance Charge Suppression Code parameter determines whether the System suppresses finance charges on inactive accounts.',
  ManageIncomeOptionMethodsAcNoPrenotificationAssessmentOptions:
    'The "No-Prenotification" Assessment Options parameter controls the action the System takes when no annual charge notification was provided to the cardholder in advance of the statement.',
  //AC - Assessment Timing
  ManageIncomeOptionMethodsAcExistingAccountStartMonthOption:
    'The Existing Account Start Month Option parameter determines whether a new annual charge assessed on an existing account includes the month the charge is levied.',
  ManageIncomeOptionMethodsAcNumberOfMonthsToFirstCharge:
    'The Number of Months to First Charge parameter establishes the number of months after an account’s opening date that the System assesses the first annual charge.',
  ManageIncomeOptionMethodsAcPostingCycleOptions:
    'The Posting Cycle Options parameter determines on which day you want annual charges to post to cardholder accounts, if they are calculated on the last processing day of the month.',
  ManageIncomeOptionMethodsAcFeeTimingOptions:
    'The Fee Timing Options parameter determines whether annual charges are posted at month end, when an account is added to the System, or when an account cycles.',
  //AC - Advanced Parameters - Assessment Timing
  ManageIncomeOptionMethodsAcSpecifiedMonthForAnnualCharge:
    'The Specified Month for Annual Charge parameter determines the month that annual charges are assessed when you set the Annual Charge Option parameter in this section to 3, 7, A, or C.',
  ManageIncomeOptionMethodsAcFeeWaiverMonths:
    'The Fee Waive Option Months parameter identifies the number of months from the open date or the last annual charge date during which the new annual charge can be waived.',
  ManageIncomeOptionMethodsAcSecondAnnualFeeNumberOfDays:
    'The Second Annual Fee No of Days parameter determines the count of days, plus 366, after which the System will generate the next annual fee after the original account open date.',
  //AC - Advanced Parameters - Assessment Calculations
  ManageIncomeOptionMethodsAcDebitActivityOption:
    'The Debit Activity Option parameter controls whether the System waives the annual charge for accounts that post a debit transaction during the year. For this parameter, debit transactions include only merchandise sales and cash advances.',
  ManageIncomeOptionMethodsAcWaiverTotalDollarCalculationOption:
    'The Waiver Total Dollar Calculation Option parameter determines the calculation for waiving annual charges.',
  ManageIncomeOptionMethodsAcRateChangePeriod:
    'The Rate Change Period parameter determines whether the System automatically changes the annual charge rate on cardholder account records. If the annual charge rate changes, this parameter indicates when the change takes place after the open date. If you set this parameter to change the annual charge rate, the delay periods indicated include the open date month as month one.',
  //AC - Advanced Parameters - Account Eligibility
  ManageIncomeOptionMethodsAcFeeWaiverOption:
    'The Fee Waive Option parameter allows you to delay or prevent annual charges from being assessed for cardholder accounts that meet criteria you specify.',
  ManageIncomeOptionMethodsAcMinimumBalanceAndExternalStatusOption:
    'The Minimum Balance and External Status Option parameter indicates whether you want to use an account’s external status, balance, and number of plastics as criteria for assessing the annual charge.',
  ManageIncomeOptionMethodsAcFeeWaiverAmount:
    'The Fee Waiver Amount parameter identifies the total amount that must post to the cardholder account before the annual charge is waived. The calculation is based on your setting in the Fee Waive Total Dollars Option parameter in this section.',
  ManageIncomeOptionMethodsAcStatusAMinimumBalance:
    'The External Status Minimum Amount parameters identify the minimum balance an account with the specified external status must have to be assessed an annual charge.',
  ManageIncomeOptionMethodsAcStatusCMinimumBalance:
    'The External Status Minimum Amount parameters identify the minimum balance an account with the specified external status must have to be assessed an annual charge.',
  ManageIncomeOptionMethodsAcStatusEMinimumBalance:
    'The External Status Minimum Amount parameters identify the minimum balance an account with the specified external status must have to be assessed an annual charge.',
  ManageIncomeOptionMethodsAcStatusFMinimumBalance:
    'The External Status Minimum Amount parameters identify the minimum balance an account with the specified external status must have to be assessed an annual charge.',
  ManageIncomeOptionMethodsAcStatusBlankMinimumBalance:
    'The External Status Minimum Amount parameters identify the minimum balance an account with the specified external status must have to be assessed an annual charge.',
  //AC - Advanced Parameters -  Reversal Options
  ManageIncomeOptionMethodsAcReversalStatementMessageWarningTextID:
    'The Reversal Statement Message Warning Text ID parameter identifies the text identification notice that is printed on the cardholder statement when the account’s annual fee is reversed because the account has been closed due to monetary inactivity.',
  ManageIncomeOptionMethodsAcReversalNumberOfCycles:
    'The Reversal Number of Cycles parameter determines the number of cycles an account must be inactive after the annual fee is charged before the System reverses the annual fee and related interest and closes the account.',
  ManageIncomeOptionMethodsAcReversalReasonCode:
    'The Reversal Reason Code parameter determines the client-defined reason code that is used when an account is closed due to inactivity other than the annual fee and any related interest.',
  ManageIncomeOptionMethodsAcReversalStatementMessageWarningOption:
    'The Reversal Statement Message Warning Option parameter determines when a message is printed on the statement for an account that will be closed due to monetary inactivity.',
  ManageIncomeOptionMethodsAcAnnualFeeReversalBatchID:
    'The Annual Fee Reversal Batch ID parameter contains the alpha reference code you want the System to use for each batch of membership fee or annual charge reversals if you want the System to also report information from the original transaction with each reversal on the CD-072, Report of Adjustments. If you leave this parameter blank, the System does not report this information.'
};

export const parameterIncomeOptionMethodsList: ParameterIncomeOptionMethodsList[] =
  [
    {
      id: 'Info_StatementAndNotificationDisplay',
      section: 'txt_manage_penalty_fee_standard_parameters',
      parameterGroup: 'Statement and Notification Display'
    },
    {
      id: 'Info_AssessmentCalculations',
      section: 'txt_manage_penalty_fee_standard_parameters',
      parameterGroup: 'Assessment Calculations'
    },
    {
      id: 'Info_AccountEligibility',
      section: 'txt_manage_penalty_fee_standard_parameters',
      parameterGroup: 'Account Eligibility'
    },
    {
      id: 'Info_AssessmentTiming',
      section: 'txt_manage_penalty_fee_standard_parameters',
      parameterGroup: 'Assessment Timing'
    },
    {
      id: 'Info_advanced_AssessmentTiming',
      section: 'txt_manage_penalty_fee_advanced_parameters',
      parameterGroup: 'Assessment Timing'
    },
    {
      id: 'Info_advanced_AssessmentCalculations',
      section: 'txt_manage_penalty_fee_advanced_parameters',
      parameterGroup: 'Assessment Calculations'
    },
    {
      id: 'Info_advanced_AccountEligibility',
      section: 'txt_manage_penalty_fee_advanced_parameters',
      parameterGroup: 'Account Eligibility'
    },
    {
      id: 'Info_advanced_ReversalOptions',
      section: 'txt_manage_penalty_fee_advanced_parameters',
      parameterGroup: 'Reversal Options'
    }
  ];

export const parameterIncomeOptionMethodsGroup: Record<
  ParameterIncomeOptionMethodsListIds,
  ParameterIncomeOptionMethodsGroup[]
> = {
  Info_StatementAndNotificationDisplay: [
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualChargeStatementDisplayControl,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcAnnualChargeStatementDisplayControl,
      onlinePCF:
        ONLINE_PCF.ManageIncomeOptionMethodsAcAnnualChargeStatementDisplayControl,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcAnnualChargeStatementDisplayControl,
      moreInfo:
        MORE_INFO.ManageIncomeOptionMethodsAcAnnualChargeStatementDisplayControl
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPreAnnualChargeNotifications,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcPreAnnualChargeNotifications,
      onlinePCF:
        ONLINE_PCF.ManageIncomeOptionMethodsAcPreAnnualChargeNotifications,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcPreAnnualChargeNotifications,
      moreInfo:
        MORE_INFO.ManageIncomeOptionMethodsAcPreAnnualChargeNotifications
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcRegulationZNotificationMediaMethod,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcRegulationZNotificationMediaMethod,
      onlinePCF:
        ONLINE_PCF.ManageIncomeOptionMethodsAcRegulationZNotificationMediaMethod,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcRegulationZNotificationMediaMethod,
      moreInfo:
        MORE_INFO.ManageIncomeOptionMethodsAcRegulationZNotificationMediaMethod
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcRegulationZnotificationGenerationPeriod,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcRegulationZnotificationGenerationPeriod,
      onlinePCF:
        ONLINE_PCF.ManageIncomeOptionMethodsAcRegulationZnotificationGenerationPeriod,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcRegulationZnotificationGenerationPeriod,
      moreInfo:
        MORE_INFO.ManageIncomeOptionMethodsAcRegulationZnotificationGenerationPeriod
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine1,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine1,
      onlinePCF:
        ONLINE_PCF.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine1,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine1,
      moreInfo:
        MORE_INFO.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine1
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine2Before,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine2Before,
      onlinePCF:
        ONLINE_PCF.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine2Before,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine2Before,
      moreInfo:
        MORE_INFO.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine2Before
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine2After,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine2After,
      onlinePCF:
        ONLINE_PCF.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine2After,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine2After,
      moreInfo:
        MORE_INFO.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine2After
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine3,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine3,
      onlinePCF:
        ONLINE_PCF.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine3,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine3,
      moreInfo:
        MORE_INFO.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine3
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine4,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine4,
      onlinePCF:
        ONLINE_PCF.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine4,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine4,
      moreInfo:
        MORE_INFO.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine4
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine5,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine5,
      onlinePCF:
        ONLINE_PCF.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine5,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine5,
      moreInfo:
        MORE_INFO.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine5
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine6,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine6,
      onlinePCF:
        ONLINE_PCF.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine6,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine6,
      moreInfo:
        MORE_INFO.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine6
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine7,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine7,
      onlinePCF:
        ONLINE_PCF.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine7,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine7,
      moreInfo:
        MORE_INFO.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine7
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine8,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine8,
      onlinePCF:
        ONLINE_PCF.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine8,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine8,
      moreInfo:
        MORE_INFO.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine8
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine9,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine9,
      onlinePCF:
        ONLINE_PCF.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine9,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine9,
      moreInfo:
        MORE_INFO.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine9
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine10,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine10,
      onlinePCF:
        ONLINE_PCF.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine10,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine10,
      moreInfo:
        MORE_INFO.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine10
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine11,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine11,
      onlinePCF:
        ONLINE_PCF.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine11,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine11,
      moreInfo:
        MORE_INFO.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine11
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine12,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine12,
      onlinePCF:
        ONLINE_PCF.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine12,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine12,
      moreInfo:
        MORE_INFO.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine12
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualChargeStatementDescription,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcAnnualChargeStatementDescription,
      onlinePCF:
        ONLINE_PCF.ManageIncomeOptionMethodsAcAnnualChargeStatementDescription,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcAnnualChargeStatementDescription,
      moreInfo:
        MORE_INFO.ManageIncomeOptionMethodsAcAnnualChargeStatementDescription
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPreNotificationMessageId,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcPreNotificationMessageId,
      onlinePCF: ONLINE_PCF.ManageIncomeOptionMethodsAcPreNotificationMessageId,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcPreNotificationMessageId,
      moreInfo: MORE_INFO.ManageIncomeOptionMethodsAcPreNotificationMessageId
    }
  ],
  Info_AssessmentCalculations: [
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcBatchTypeCode,
      fieldName: MethodFieldNameEnum.ManageIncomeOptionMethodsAcBatchTypeCode,
      onlinePCF: ONLINE_PCF.ManageIncomeOptionMethodsAcBatchTypeCode,
      moreInfoText: MORE_INFO.ManageIncomeOptionMethodsAcBatchTypeCode,
      moreInfo: MORE_INFO.ManageIncomeOptionMethodsAcBatchTypeCode
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcIncludeAnnualChargeInInterestCalculations,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcIncludeAnnualChargeInInterestCalculations,
      onlinePCF:
        ONLINE_PCF.ManageIncomeOptionMethodsAcIncludeAnnualChargeInInterestCalculations,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcIncludeAnnualChargeInInterestCalculations,
      moreInfo:
        MORE_INFO.ManageIncomeOptionMethodsAcIncludeAnnualChargeInInterestCalculations
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPreventDelinquencyFromAnnualFeeBalance,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcPreventDelinquencyFromAnnualFeeBalance,
      onlinePCF:
        ONLINE_PCF.ManageIncomeOptionMethodsAcPreventDelinquencyFromAnnualFeeBalance,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcPreventDelinquencyFromAnnualFeeBalance,
      moreInfo:
        MORE_INFO.ManageIncomeOptionMethodsAcPreventDelinquencyFromAnnualFeeBalance
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualChargeOption,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcAnnualChargeOption,
      onlinePCF: ONLINE_PCF.ManageIncomeOptionMethodsAcAnnualChargeOption,
      moreInfoText: MORE_INFO.ManageIncomeOptionMethodsAcAnnualChargeOption,
      moreInfo: MORE_INFO.ManageIncomeOptionMethodsAcAnnualChargeOption
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount1,
      fieldName: MethodFieldNameEnum.ManageIncomeOptionMethodsAcAmount1,
      onlinePCF: ONLINE_PCF.ManageIncomeOptionMethodsAcAmount1,
      moreInfoText: MORE_INFO.ManageIncomeOptionMethodsAcAmount1,
      moreInfo: MORE_INFO.ManageIncomeOptionMethodsAcAmount1
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount2,
      fieldName: MethodFieldNameEnum.ManageIncomeOptionMethodsAcAmount2,
      onlinePCF: ONLINE_PCF.ManageIncomeOptionMethodsAcAmount2,
      moreInfoText: MORE_INFO.ManageIncomeOptionMethodsAcAmount2,
      moreInfo: MORE_INFO.ManageIncomeOptionMethodsAcAmount2
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount3,
      fieldName: MethodFieldNameEnum.ManageIncomeOptionMethodsAcAmount3,
      onlinePCF: ONLINE_PCF.ManageIncomeOptionMethodsAcAmount3,
      moreInfoText: MORE_INFO.ManageIncomeOptionMethodsAcAmount3,
      moreInfo: MORE_INFO.ManageIncomeOptionMethodsAcAmount3
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount4,
      fieldName: MethodFieldNameEnum.ManageIncomeOptionMethodsAcAmount4,
      onlinePCF: ONLINE_PCF.ManageIncomeOptionMethodsAcAmount4,
      moreInfoText: MORE_INFO.ManageIncomeOptionMethodsAcAmount4,
      moreInfo: MORE_INFO.ManageIncomeOptionMethodsAcAmount4
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount5,
      fieldName: MethodFieldNameEnum.ManageIncomeOptionMethodsAcAmount5,
      onlinePCF: ONLINE_PCF.ManageIncomeOptionMethodsAcAmount5,
      moreInfoText: MORE_INFO.ManageIncomeOptionMethodsAcAmount5,
      moreInfo: MORE_INFO.ManageIncomeOptionMethodsAcAmount5
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount6,
      fieldName: MethodFieldNameEnum.ManageIncomeOptionMethodsAcAmount6,
      onlinePCF: ONLINE_PCF.ManageIncomeOptionMethodsAcAmount6,
      moreInfoText: MORE_INFO.ManageIncomeOptionMethodsAcAmount6,
      moreInfo: MORE_INFO.ManageIncomeOptionMethodsAcAmount6
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAdditionalPlasticCharge,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcAdditionalPlasticCharge,
      onlinePCF: ONLINE_PCF.ManageIncomeOptionMethodsAcAdditionalPlasticCharge,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcAdditionalPlasticCharge,
      moreInfo: MORE_INFO.ManageIncomeOptionMethodsAcAdditionalPlasticCharge
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFirstYearMaximumFeeManagementOption,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcFirstYearMaximumFeeManagementOption,
      onlinePCF:
        ONLINE_PCF.ManageIncomeOptionMethodsAcFirstYearMaximumFeeManagementOption,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcFirstYearMaximumFeeManagementOption,
      moreInfo:
        MORE_INFO.ManageIncomeOptionMethodsAcFirstYearMaximumFeeManagementOption
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPrenotificationAmountChangeOptions,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcPrenotificationAmountChangeOptions,
      onlinePCF:
        ONLINE_PCF.ManageIncomeOptionMethodsAcPrenotificationAmountChangeOptions,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcPrenotificationAmountChangeOptions,
      moreInfo:
        MORE_INFO.ManageIncomeOptionMethodsAcPrenotificationAmountChangeOptions
    }
  ],
  Info_AccountEligibility: [
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPlasticsRequirementCode,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcPlasticsRequirementCode,
      onlinePCF: ONLINE_PCF.ManageIncomeOptionMethodsAcPlasticsRequirementCode,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcPlasticsRequirementCode,
      moreInfo: MORE_INFO.ManageIncomeOptionMethodsAcPlasticsRequirementCode
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcSpecialConditionsForExternalStatus,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcSpecialConditionsForExternalStatus,
      onlinePCF:
        ONLINE_PCF.ManageIncomeOptionMethodsAcSpecialConditionsForExternalStatus,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcSpecialConditionsForExternalStatus,
      moreInfo:
        MORE_INFO.ManageIncomeOptionMethodsAcSpecialConditionsForExternalStatus
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcSpecialConditionsForExpiredAccounts,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcSpecialConditionsForExpiredAccounts,
      onlinePCF:
        ONLINE_PCF.ManageIncomeOptionMethodsAcSpecialConditionsForExpiredAccounts,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcSpecialConditionsForExpiredAccounts,
      moreInfo:
        MORE_INFO.ManageIncomeOptionMethodsAcSpecialConditionsForExpiredAccounts
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualChargeIncludeTable,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcAnnualChargeIncludeTable,
      onlinePCF: ONLINE_PCF.ManageIncomeOptionMethodsAcAnnualChargeIncludeTable,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcAnnualChargeIncludeTable,
      moreInfo: MORE_INFO.ManageIncomeOptionMethodsAcAnnualChargeIncludeTable
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFinanceChargeSuppressionCode,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcFinanceChargeSuppressionCode,
      onlinePCF:
        ONLINE_PCF.ManageIncomeOptionMethodsAcFinanceChargeSuppressionCode,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcFinanceChargeSuppressionCode,
      moreInfo:
        MORE_INFO.ManageIncomeOptionMethodsAcFinanceChargeSuppressionCode
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcNoPrenotificationAssessmentOptions,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcNoPrenotificationAssessmentOptions,
      onlinePCF:
        ONLINE_PCF.ManageIncomeOptionMethodsAcNoPrenotificationAssessmentOptions,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcNoPrenotificationAssessmentOptions,
      moreInfo:
        MORE_INFO.ManageIncomeOptionMethodsAcNoPrenotificationAssessmentOptions
    }
  ],
  Info_AssessmentTiming: [
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcExistingAccountStartMonthOption,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcExistingAccountStartMonthOption,
      onlinePCF:
        ONLINE_PCF.ManageIncomeOptionMethodsAcExistingAccountStartMonthOption,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcExistingAccountStartMonthOption,
      moreInfo:
        MORE_INFO.ManageIncomeOptionMethodsAcExistingAccountStartMonthOption
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcNumberOfMonthsToFirstCharge,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcNumberOfMonthsToFirstCharge,
      onlinePCF:
        ONLINE_PCF.ManageIncomeOptionMethodsAcNumberOfMonthsToFirstCharge,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcNumberOfMonthsToFirstCharge,
      moreInfo: MORE_INFO.ManageIncomeOptionMethodsAcNumberOfMonthsToFirstCharge
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPostingCycleOptions,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcPostingCycleOptions,
      onlinePCF: ONLINE_PCF.ManageIncomeOptionMethodsAcPostingCycleOptions,
      moreInfoText: MORE_INFO.ManageIncomeOptionMethodsAcPostingCycleOptions,
      moreInfo: MORE_INFO.ManageIncomeOptionMethodsAcPostingCycleOptions
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeTimingOptions,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcFeeTimingOptions,
      onlinePCF: ONLINE_PCF.ManageIncomeOptionMethodsAcFeeTimingOptions,
      moreInfoText: MORE_INFO.ManageIncomeOptionMethodsAcFeeTimingOptions,
      moreInfo: MORE_INFO.ManageIncomeOptionMethodsAcFeeTimingOptions
    }
  ],
  Info_advanced_AssessmentTiming: [
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcSpecifiedMonthForAnnualCharge,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcSpecifiedMonthForAnnualCharge,
      onlinePCF:
        ONLINE_PCF.ManageIncomeOptionMethodsAcSpecifiedMonthForAnnualCharge,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcSpecifiedMonthForAnnualCharge,
      moreInfo:
        MORE_INFO.ManageIncomeOptionMethodsAcSpecifiedMonthForAnnualCharge
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeWaiverMonths,
      fieldName: MethodFieldNameEnum.ManageIncomeOptionMethodsAcFeeWaiverMonths,
      onlinePCF: ONLINE_PCF.ManageIncomeOptionMethodsAcFeeWaiverMonths,
      moreInfoText: MORE_INFO.ManageIncomeOptionMethodsAcFeeWaiverMonths,
      moreInfo: MORE_INFO.ManageIncomeOptionMethodsAcFeeWaiverMonths
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcSecondAnnualFeeNumberOfDays,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcSecondAnnualFeeNumberOfDays,
      onlinePCF:
        ONLINE_PCF.ManageIncomeOptionMethodsAcSecondAnnualFeeNumberOfDays,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcSecondAnnualFeeNumberOfDays,
      moreInfo: MORE_INFO.ManageIncomeOptionMethodsAcSecondAnnualFeeNumberOfDays
    }
  ],
  Info_advanced_AssessmentCalculations: [
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcDebitActivityOption,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcDebitActivityOption,
      onlinePCF: ONLINE_PCF.ManageIncomeOptionMethodsAcDebitActivityOption,
      moreInfoText: MORE_INFO.ManageIncomeOptionMethodsAcDebitActivityOption,
      moreInfo: MORE_INFO.ManageIncomeOptionMethodsAcDebitActivityOption
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcWaiverTotalDollarCalculationOption,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcWaiverTotalDollarCalculationOption,
      onlinePCF:
        ONLINE_PCF.ManageIncomeOptionMethodsAcWaiverTotalDollarCalculationOption,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcWaiverTotalDollarCalculationOption,
      moreInfo:
        MORE_INFO.ManageIncomeOptionMethodsAcWaiverTotalDollarCalculationOption
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcRateChangePeriod,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcRateChangePeriod,
      onlinePCF: ONLINE_PCF.ManageIncomeOptionMethodsAcRateChangePeriod,
      moreInfoText: MORE_INFO.ManageIncomeOptionMethodsAcRateChangePeriod,
      moreInfo: MORE_INFO.ManageIncomeOptionMethodsAcRateChangePeriod
    }
  ],
  Info_advanced_AccountEligibility: [
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeWaiverOption,
      fieldName: MethodFieldNameEnum.ManageIncomeOptionMethodsAcFeeWaiverOption,
      onlinePCF: ONLINE_PCF.ManageIncomeOptionMethodsAcFeeWaiverOption,
      moreInfoText: MORE_INFO.ManageIncomeOptionMethodsAcFeeWaiverOption,
      moreInfo: MORE_INFO.ManageIncomeOptionMethodsAcFeeWaiverOption
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeWaiverAmount,
      fieldName: MethodFieldNameEnum.ManageIncomeOptionMethodsAcFeeWaiverAmount,
      onlinePCF: ONLINE_PCF.ManageIncomeOptionMethodsAcFeeWaiverAmount,
      moreInfoText: MORE_INFO.ManageIncomeOptionMethodsAcFeeWaiverAmount,
      moreInfo: MORE_INFO.ManageIncomeOptionMethodsAcFeeWaiverAmount
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcMinimumBalanceAndExternalStatusOption,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcMinimumBalanceAndExternalStatusOption,
      onlinePCF:
        ONLINE_PCF.ManageIncomeOptionMethodsAcMinimumBalanceAndExternalStatusOption,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcMinimumBalanceAndExternalStatusOption,
      moreInfo:
        MORE_INFO.ManageIncomeOptionMethodsAcMinimumBalanceAndExternalStatusOption
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatusAMinimumBalance,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcStatusAMinimumBalance,
      onlinePCF: ONLINE_PCF.ManageIncomeOptionMethodsAcStatusAMinimumBalance,
      moreInfoText: MORE_INFO.ManageIncomeOptionMethodsAcStatusAMinimumBalance,
      moreInfo: MORE_INFO.ManageIncomeOptionMethodsAcStatusAMinimumBalance
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatusCMinimumBalance,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcStatusCMinimumBalance,
      onlinePCF: ONLINE_PCF.ManageIncomeOptionMethodsAcStatusCMinimumBalance,
      moreInfoText: MORE_INFO.ManageIncomeOptionMethodsAcStatusCMinimumBalance,
      moreInfo: MORE_INFO.ManageIncomeOptionMethodsAcStatusCMinimumBalance
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatusEMinimumBalance,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcStatusEMinimumBalance,
      onlinePCF: ONLINE_PCF.ManageIncomeOptionMethodsAcStatusEMinimumBalance,
      moreInfoText: MORE_INFO.ManageIncomeOptionMethodsAcStatusEMinimumBalance,
      moreInfo: MORE_INFO.ManageIncomeOptionMethodsAcStatusEMinimumBalance
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatusFMinimumBalance,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcStatusFMinimumBalance,
      onlinePCF: ONLINE_PCF.ManageIncomeOptionMethodsAcStatusFMinimumBalance,
      moreInfoText: MORE_INFO.ManageIncomeOptionMethodsAcStatusFMinimumBalance,
      moreInfo: MORE_INFO.ManageIncomeOptionMethodsAcStatusFMinimumBalance
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatusBlankMinimumBalance,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcStatusBlankMinimumBalance,
      onlinePCF:
        ONLINE_PCF.ManageIncomeOptionMethodsAcStatusBlankMinimumBalance,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcStatusBlankMinimumBalance,
      moreInfo: MORE_INFO.ManageIncomeOptionMethodsAcStatusBlankMinimumBalance
    }
  ],
  Info_advanced_ReversalOptions: [
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcReversalNumberOfCycles,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcReversalNumberOfCycles,
      onlinePCF: ONLINE_PCF.ManageIncomeOptionMethodsAcReversalNumberOfCycles,
      moreInfoText: MORE_INFO.ManageIncomeOptionMethodsAcReversalNumberOfCycles,
      moreInfo: MORE_INFO.ManageIncomeOptionMethodsAcReversalNumberOfCycles
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcReversalReasonCode,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcReversalReasonCode,
      onlinePCF: ONLINE_PCF.ManageIncomeOptionMethodsAcReversalReasonCode,
      moreInfoText: MORE_INFO.ManageIncomeOptionMethodsAcReversalReasonCode,
      moreInfo: MORE_INFO.ManageIncomeOptionMethodsAcReversalReasonCode
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcReversalStatementMessageWarningOption,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcReversalStatementMessageWarningOption,
      onlinePCF:
        ONLINE_PCF.ManageIncomeOptionMethodsAcReversalStatementMessageWarningOption,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcReversalStatementMessageWarningOption,
      moreInfo:
        MORE_INFO.ManageIncomeOptionMethodsAcReversalStatementMessageWarningOption
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcReversalStatementMessageWarningTextID,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcReversalStatementMessageWarningTextID,
      onlinePCF:
        ONLINE_PCF.ManageIncomeOptionMethodsAcReversalStatementMessageWarningTextID,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcReversalStatementMessageWarningTextID,
      moreInfo:
        MORE_INFO.ManageIncomeOptionMethodsAcReversalStatementMessageWarningTextID
    },
    {
      id: MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualFeeReversalBatchID,
      fieldName:
        MethodFieldNameEnum.ManageIncomeOptionMethodsAcAnnualFeeReversalBatchID,
      onlinePCF: ONLINE_PCF.ManageIncomeOptionMethodsAcAnnualFeeReversalBatchID,
      moreInfoText:
        MORE_INFO.ManageIncomeOptionMethodsAcAnnualFeeReversalBatchID,
      moreInfo: MORE_INFO.ManageIncomeOptionMethodsAcAnnualFeeReversalBatchID
    }
  ]
};

export const META_PARAMETER_ANNUAL_CHARGES = [
  //Ac - Assessment Calculations
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcBatchTypeCode,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcIncludeAnnualChargeInInterestCalculations,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPreventDelinquencyFromAnnualFeeBalance,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFirstYearMaximumFeeManagementOption,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPrenotificationAmountChangeOptions,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount1,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount2,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount3,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount4,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount5,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount6,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAdditionalPlasticCharge,
  //Ac - Statement and Notification Display
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualChargeStatementDisplayControl,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPreAnnualChargeNotifications,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcRegulationZNotificationMediaMethod,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcRegulationZnotificationGenerationPeriod,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPreNotificationMessageId,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine1,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine2Before,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine2After,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine3,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine4,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine5,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine6,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine7,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine8,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine9,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine10,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine11,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine12,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualChargeStatementDescription,
  //AC - Account Eligibility
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPlasticsRequirementCode,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcSpecialConditionsForExternalStatus,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcSpecialConditionsForExpiredAccounts,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualChargeIncludeTable,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFinanceChargeSuppressionCode,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcNoPrenotificationAssessmentOptions,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeWaiverOption,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcMinimumBalanceAndExternalStatusOption,
  //AC - Assessment Timing
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcExistingAccountStartMonthOption,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcNumberOfMonthsToFirstCharge,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPostingCycleOptions,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeTimingOptions,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcSpecifiedMonthForAnnualCharge,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeWaiverMonths,
  //AC - Advanced Parameters - Assessment Calculations
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcDebitActivityOption,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcWaiverTotalDollarCalculationOption,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcRateChangePeriod,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualChargeOption,
  //AC - Advanced Parameters -  Reversal Options
  MethodFieldParameterEnum.ManageIncomeOptionMethodsAcReversalStatementMessageWarningTextID
];
