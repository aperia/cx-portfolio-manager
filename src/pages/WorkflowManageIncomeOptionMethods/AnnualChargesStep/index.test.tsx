import { render, RenderResult } from '@testing-library/react';
import React from 'react';
import { ConfigureParametersFormValue } from '../ConfigureParameters';
import AnnualChargesStep from './index';

jest.mock('../ConfigureParameters', () => ({
  __esModule: true,
  default: () => {
    return (
      <div>
        <div>ConfigureParameters</div>
      </div>
    );
  }
}));

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <AnnualChargesStep {...props} />
    </div>
  );
};

describe('WorkflowManageIncomeOptionMethods > AnnualChargesStep > Index', () => {
  it('Should render Overview modal > Close overview modal', () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: {
        isValid: true
      },
      setFormValues: mockFn
    } as WorkflowSetupProps<ConfigureParametersFormValue>;

    const wrapper = renderComponent(props);
    expect(wrapper.getByText('ConfigureParameters')).toBeInTheDocument();
  });
});
