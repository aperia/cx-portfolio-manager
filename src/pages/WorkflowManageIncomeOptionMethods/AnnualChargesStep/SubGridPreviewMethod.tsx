import { MethodFieldParameterEnum } from 'app/constants/enums';
import { matchSearchValue } from 'app/helpers';
import {
  ColumnType,
  Grid,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import {
  formatTruncate,
  viewMoreInfo
} from 'pages/_commons/Utils/formatGridField';
import React, { useMemo } from 'react';
import { parameterIncomeOptionMethodsGroup } from './constant';
import { ParameterIncomeOptionMethodsList } from './types';

interface IProps {
  original: ParameterIncomeOptionMethodsList;
  metadata: Record<MethodFieldParameterEnum, any>;
  methodType: string;
}
const SubGridPreviewMethod: React.FC<IProps> = ({ original, metadata }) => {
  const { t } = useTranslation();

  const data = useMemo(
    () =>
      parameterIncomeOptionMethodsGroup[original.id].filter(p =>
        matchSearchValue([p.fieldName, p.moreInfoText, p.onlinePCF], undefined)
      ),
    [original.id]
  );

  const valueAccessor = (records: Record<string, any>) => {
    const paramId = records.id as MethodFieldParameterEnum;
    const valueTruncate =
      metadata[paramId]?.toString().toLowerCase() !== 'none'
        ? metadata[paramId]
        : '';

    return (
      <TruncateText
        resizable
        lines={2}
        ellipsisLessText={t('txt_less')}
        ellipsisMoreText={t('txt_more')}
      >
        {valueTruncate}
      </TruncateText>
    );
  };

  const columns: ColumnType[] = [
    {
      id: 'fieldName',
      Header: t('txt_business_name'),
      accessor: formatTruncate(['fieldName'])
    },
    {
      id: 'onlinePCF',
      Header: t('txt_manage_penalty_fee_online_PCF'),
      accessor: formatTruncate(['onlinePCF'])
    },
    {
      id: 'value',
      Header: t('txt_value'),
      accessor: valueAccessor
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      accessor: viewMoreInfo(['moreInfo'], t),
      width: 105
    }
  ];

  return (
    <div className="p-20">
      <Grid columns={columns} data={data} />
    </div>
  );
};

export default SubGridPreviewMethod;
