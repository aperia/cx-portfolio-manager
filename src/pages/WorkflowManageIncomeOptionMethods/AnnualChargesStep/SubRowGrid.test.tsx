import { renderComponent } from 'app/utils';
import React from 'react';
import { parameterIncomeOptionMethodsList } from './constant';
import SubRowGrid from './SubRowGrid';

const t = value => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

jest.mock('./SubGridPreviewMethod', () => ({
  __esModule: true,
  default: () => <div>SubGridPreviewMethod</div>
}));

describe('pages > WorkflowManageIncomeOptionMethods > AnnualChargesStep > SubRowGrid', () => {
  it('render', async () => {
    const original = [
      { newValue: '0', name: 'annual.charge.statement.display.control' },
      { newValue: '0', name: 'pre.annual.charge.notifications' },
      { newValue: '0', name: 'regulation.z.notification.media.method' },
      { newValue: '0', name: 'regulation.z.notification.generation.period' },
      { newValue: '0', name: 'pre.notification.message.id' },
      { newValue: '0', name: 'statement.pre.notification.message.line.1' },
      {
        newValue: '0',
        name: 'statement.pre.notification.message.line.2.before'
      },
      {
        newValue: '0',
        name: 'statement.pre.notification.message.line.2.after'
      },
      { newValue: '0', name: 'statement.pre.notification.message.line.3' },
      { newValue: '0', name: 'statement.pre.notification.message.line.4' },
      { newValue: '0', name: 'statement.pre.notification.message.line.5' },
      { newValue: '0', name: 'statement.pre.notification.message.line.6' },
      { newValue: '0', name: 'statement.pre.notification.message.line.7' },
      { newValue: '0', name: 'statement.pre.notification.message.line.8' },
      { newValue: '0', name: 'statement.pre.notification.message.line.9' },
      { newValue: '0', name: 'statement.pre.notification.message.line.10' },
      { newValue: '0', name: 'statement.pre.notification.message.line.11' },
      { newValue: '0', name: 'statement.pre.notification.message.line.12' },
      { newValue: '0', name: 'annual.charge.statement.description' }
    ];

    const metadata = {
      manageIncomeOptionMethodsAcAnnualChargeStatementDisplayControlOptions: [
        { code: 'code', text: 'text' }
      ],
      manageIncomeOptionMethodsAcPreAnnualChargeNotificationsOptions: [
        { code: 'code', text: 'text' }
      ],
      manageIncomeOptionMethodsAcRegulationZnotificationGenerationPeriodOptions:
        [{ code: 'code', text: 'text' }],
      manageIncomeOptionMethodsAcRegulationZNotificationMediaMethodOptions: [
        { code: 'code', text: 'text' }
      ],
      manageIncomeOptionMethodsAcPreNotificationMessageIdOptions: [
        { code: 'code', text: 'text' }
      ],
      changeInTermsCommunicationLetterIdForInActice: [
        { code: 'code', text: 'text' }
      ],
      manageIncomeOptionMethodsAcBatchTypeCodeOptions: [
        { code: 'code', text: 'text' }
      ],
      manageIncomeOptionMethodsAcIncludeAnnualChargeInInterestCalculationsOptions:
        [{ code: 'code', text: 'text' }],
      manageIncomeOptionMethodsAcPreventDelinquencyFromAnnualFeeBalanceOptions:
        [{ code: 'code', text: 'text' }],
      manageIncomeOptionMethodsAcAnnualChargeOptionOptions: [
        { code: 'code', text: 'text' }
      ],
      manageIncomeOptionMethodsAcFirstYearMaximumFeeManagementOptionOptions: [
        { code: 'code', text: 'text' }
      ],
      manageIncomeOptionMethodsAcPrenotificationAmountChangeOptionsOptions: [
        { code: 'code', text: 'text' }
      ],
      manageIncomeOptionMethodsAcPlasticsRequirementCodeOptions: [
        { code: 'code', text: 'text' }
      ],
      manageIncomeOptionMethodsAcSpecialConditionsForExternalStatusOptions: [
        { code: 'code', text: 'text' }
      ],
      manageIncomeOptionMethodsAcSpecialConditionsForExpiredAccountsOptions: [
        { code: 'code', text: 'text' }
      ],
      manageIncomeOptionMethodsAcAnnualChargeIncludeTableOptions: [
        { code: 'code', text: 'text' }
      ],
      manageIncomeOptionMethodsAcFinanceChargeSuppressionCodeOptions: [
        { code: 'code', text: 'text' }
      ],
      manageIncomeOptionMethodsAcNoPrenotificationAssessmentOptionsOptions: [
        { code: 'code', text: 'text' }
      ],
      manageIncomeOptionMethodsAcExistingAccountStartMonthOptionOptions: [
        { code: 'code', text: 'text' }
      ],
      manageIncomeOptionMethodsAcNumberOfMonthsToFirstChargeOptions: [
        { code: 'code', text: 'text' }
      ],
      manageIncomeOptionMethodsAcPostingCycleOptionsOptions: [
        { code: 'code', text: 'text' }
      ],
      manageIncomeOptionMethodsAcFeeTimingOptionsOptions: [
        { code: 'code', text: 'text' }
      ],
      manageIncomeOptionMethodsAcSpecifiedMonthForAnnualChargeOptions: [
        { code: 'code', text: 'text' }
      ],
      manageIncomeOptionMethodsAcFeeWaiverMonthsOptions: [
        { code: 'code', text: 'text' }
      ],
      manageIncomeOptionMethodsAcDebitActivityOptionOptions: [
        { code: 'code', text: 'text' }
      ],
      manageIncomeOptionMethodsAcWaiverTotalDollarCalculationOptionOptions: [
        { code: 'code', text: 'text' }
      ],
      manageIncomeOptionMethodsAcRateChangePeriodOptions: [
        { code: 'code', text: 'text' }
      ],
      manageIncomeOptionMethodsAcFeeWaiverOptionOptions: [
        { code: 'code', text: 'text' }
      ],
      manageIncomeOptionMethodsAcMinimumBalanceAndExternalStatusOptionOptions: [
        { code: 'code', text: 'text' }
      ],
      manageIncomeOptionMethodsAcReversalStatementMessageWarningTextIDOptions: [
        { code: 'code', text: 'text' }
      ]
    };
    const wrapper = await renderComponent(
      'testId',
      <SubRowGrid metadata={metadata} original={original} />
    );

    expect(
      wrapper.getByText(parameterIncomeOptionMethodsList[0].parameterGroup)
    ).toBeInTheDocument();
  });
});
