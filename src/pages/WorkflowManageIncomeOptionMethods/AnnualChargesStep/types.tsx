import {
  MethodFieldNameEnum,
  MethodFieldParameterEnum
} from 'app/constants/enums';
import React from 'react';

export type ParameterIncomeOptionMethodsListIds =
  | 'Info_StatementAndNotificationDisplay'
  | 'Info_AssessmentCalculations'
  | 'Info_AccountEligibility'
  | 'Info_AssessmentTiming'
  | 'Info_advanced_AssessmentTiming'
  | 'Info_advanced_AssessmentCalculations'
  | 'Info_advanced_AccountEligibility'
  | 'Info_advanced_ReversalOptions';

export interface ParameterIncomeOptionMethodsList {
  id: ParameterIncomeOptionMethodsListIds;
  section:
    | 'txt_manage_penalty_fee_standard_parameters'
    | 'txt_manage_penalty_fee_advanced_parameters';
  parameterGroup:
    | 'Statement and Notification Display'
    | 'Assessment Calculations'
    | 'Account Eligibility'
    | 'Assessment Timing'
    | 'Miscellaneous Parameters'
    | 'Reversal Options';
  moreInfo?: React.ReactNode;
}

export interface ParameterIncomeOptionMethodsGroup {
  id: MethodFieldParameterEnum;
  fieldName: MethodFieldNameEnum;
  onlinePCF: string;
  moreInfoText: string;
  moreInfo: React.ReactNode;
}

export interface IncomeOptionMethodsMeta {
  //Ac - Statement and Notification Display
  manageIncomeOptionMethodsAcAnnualChargeStatementDisplayControlOptions: RefData[];
  manageIncomeOptionMethodsAcPreAnnualChargeNotificationsOptions: RefData[];
  manageIncomeOptionMethodsAcRegulationZnotificationGenerationPeriodOptions: RefData[];
  manageIncomeOptionMethodsAcRegulationZNotificationMediaMethodOptions: RefData[];
  manageIncomeOptionMethodsAcPreNotificationMessageIdOptions: RefData[];
  //Ac - Assessment Calculations
  manageIncomeOptionMethodsAcBatchTypeCodeOptions: RefData[];
  manageIncomeOptionMethodsAcIncludeAnnualChargeInInterestCalculationsOptions: RefData[];
  manageIncomeOptionMethodsAcPreventDelinquencyFromAnnualFeeBalanceOptions: RefData[];
  manageIncomeOptionMethodsAcAnnualChargeOptionOptions: RefData[];
  manageIncomeOptionMethodsAcFirstYearMaximumFeeManagementOptionOptions: RefData[];
  manageIncomeOptionMethodsAcPrenotificationAmountChangeOptionsOptions: RefData[];
  //AC - Account Eligibility
  manageIncomeOptionMethodsAcPlasticsRequirementCodeOptions: RefData[];
  manageIncomeOptionMethodsAcSpecialConditionsForExternalStatusOptions: RefData[];
  manageIncomeOptionMethodsAcSpecialConditionsForExpiredAccountsOptions: RefData[];
  manageIncomeOptionMethodsAcAnnualChargeIncludeTableOptions: RefData[];
  manageIncomeOptionMethodsAcFinanceChargeSuppressionCodeOptions: RefData[];
  manageIncomeOptionMethodsAcNoPrenotificationAssessmentOptionsOptions: RefData[];
  //AC - Assessment Timing
  manageIncomeOptionMethodsAcExistingAccountStartMonthOptionOptions: RefData[];
  manageIncomeOptionMethodsAcNumberOfMonthsToFirstChargeOptions: RefData[];
  manageIncomeOptionMethodsAcPostingCycleOptionsOptions: RefData[];
  manageIncomeOptionMethodsAcFeeTimingOptionsOptions: RefData[];
  //AC - Advanced Parameters - Assessment Timing
  manageIncomeOptionMethodsAcSpecifiedMonthForAnnualChargeOptions: RefData[];
  manageIncomeOptionMethodsAcFeeWaiverMonthsOptions: RefData[];
  //AC - Advanced Parameters - Assessment Calculations
  manageIncomeOptionMethodsAcDebitActivityOptionOptions: RefData[];
  manageIncomeOptionMethodsAcWaiverTotalDollarCalculationOptionOptions: RefData[];
  manageIncomeOptionMethodsAcRateChangePeriodOptions: RefData[];
  //AC - Advanced Parameters - Account Eligibility
  manageIncomeOptionMethodsAcFeeWaiverOptionOptions: RefData[];
  manageIncomeOptionMethodsAcMinimumBalanceAndExternalStatusOptionOptions: RefData[];
  //AC - Advanced Parameters -  Reversal Options
  manageIncomeOptionMethodsAcReversalStatementMessageWarningTextIDOptions: RefData[];
}
