import { ServiceSubjectSection } from 'app/constants/enums';
import {
  getWorkflowSetupMethodType,
  getWorkflowSetupStepStatus
} from 'app/helpers';
import { ConfigureParametersFormValue } from '../ConfigureParameters';

const parseFormValues: WorkflowSetupStepFormDataFunc<ConfigureParametersFormValue> =
  (data, id) => {
    const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);
    if (!stepInfo) return;
    const now = Date.now();

    const methods = (data.data.configurations?.methodList || [])
      .filter(
        (method: any) =>
          method.serviceSubjectSection === ServiceSubjectSection.IAC
      )
      .map(
        (method: any, idx: number) =>
          ({
            ...method,
            id: '', // Remove Id due to error 500 when update with methodId
            methodType: getWorkflowSetupMethodType(method),
            rowId: now + idx
          } as WorkflowSetupMethod & { rowId?: number })
      );

    return {
      ...getWorkflowSetupStepStatus(stepInfo),
      isValid: methods.length > 0,
      methods,
      isAnnualChargesStep: true
    };
  };

export default parseFormValues;
