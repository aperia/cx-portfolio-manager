import { render } from '@testing-library/react';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import React from 'react';
import SubGridPreviewMethod from './SubGridPreviewMethod';

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

const metadata = {
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualChargeStatementDisplayControl]:
    'none',
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPreAnnualChargeNotifications]:
    'test',
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcRegulationZNotificationMediaMethod]:
    'test2',
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcRegulationZnotificationGenerationPeriod]:
    'test2',
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPreNotificationMessageId]:
    'test2',
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcBatchTypeCode]: 'test2',
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcIncludeAnnualChargeInInterestCalculations]:
    'test2',
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPreventDelinquencyFromAnnualFeeBalance]:
    'test2'
};

describe('pages > WorkflowManageIncomeOptionMethods > AnnualChargesStep > SubGridPreviewMethod', () => {
  it('Info_Basic', () => {
    const original = { id: 'Info_StatementAndNotificationDisplay' };
    const props = {
      metadata,
      original
    };
    const wrapper = render(<SubGridPreviewMethod {...props} />);
    expect(wrapper.getByText('test')).toBeInTheDocument();
  });
});
