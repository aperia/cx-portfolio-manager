import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React from 'react';
import ConfigureParameters, {
  ConfigureParametersFormValue
} from '../ConfigureParameters';
import Summary from '../ConfigureParameters/Summary';
import parseFormValues from './parseFormValues';

const AnnualChargesStep: React.FC<
  WorkflowSetupProps<ConfigureParametersFormValue>
> = prop => {
  return <ConfigureParameters {...prop} />;
};

const ExtraStaticAnnualChargesStep =
  AnnualChargesStep as WorkflowSetupStaticProp;

ExtraStaticAnnualChargesStep.summaryComponent = Summary;
ExtraStaticAnnualChargesStep.defaultValues = {
  isValid: false,
  isAnnualChargesStep: true,
  methods: []
};
ExtraStaticAnnualChargesStep.parseFormValues = parseFormValues;

export default ExtraStaticAnnualChargesStep;
