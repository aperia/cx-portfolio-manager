import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React from 'react';
import SubGridNewMethod from './SubGridNewMethod';

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

const metadata = {
  manageIncomeOptionMethodsAcAnnualChargeStatementDisplayControlOptions: [
    { code: 'code', text: 'text' }
  ],
  manageIncomeOptionMethodsAcPreAnnualChargeNotificationsOptions: [
    { code: 'code', text: 'text' }
  ],
  manageIncomeOptionMethodsAcRegulationZnotificationGenerationPeriodOptions: [
    { code: 'code', text: 'text' }
  ],
  manageIncomeOptionMethodsAcRegulationZNotificationMediaMethodOptions: [
    { code: 'code', text: 'text' }
  ],
  manageIncomeOptionMethodsAcPreNotificationMessageIdOptions: [
    { code: 'code', text: 'text' }
  ],
  changeInTermsCommunicationLetterIdForInActice: [
    { code: 'code', text: 'text' }
  ],
  manageIncomeOptionMethodsAcBatchTypeCodeOptions: [
    { code: 'code', text: 'text' }
  ],
  manageIncomeOptionMethodsAcIncludeAnnualChargeInInterestCalculationsOptions: [
    { code: 'code', text: 'text' }
  ],
  manageIncomeOptionMethodsAcPreventDelinquencyFromAnnualFeeBalanceOptions: [
    { code: 'code', text: 'text' }
  ],
  manageIncomeOptionMethodsAcAnnualChargeOptionOptions: [
    { code: 'code', text: 'text' }
  ],
  manageIncomeOptionMethodsAcFirstYearMaximumFeeManagementOptionOptions: [
    { code: 'code', text: 'text' }
  ],
  manageIncomeOptionMethodsAcPrenotificationAmountChangeOptionsOptions: [
    { code: 'code', text: 'text' }
  ],
  manageIncomeOptionMethodsAcPlasticsRequirementCodeOptions: [
    { code: 'code', text: 'text' }
  ],
  manageIncomeOptionMethodsAcSpecialConditionsForExternalStatusOptions: [
    { code: 'code', text: 'text' }
  ],
  manageIncomeOptionMethodsAcSpecialConditionsForExpiredAccountsOptions: [
    { code: 'code', text: 'text' }
  ],
  manageIncomeOptionMethodsAcAnnualChargeIncludeTableOptions: [
    { code: 'code', text: 'text' }
  ],
  manageIncomeOptionMethodsAcFinanceChargeSuppressionCodeOptions: [
    { code: 'code', text: 'text' }
  ],
  manageIncomeOptionMethodsAcNoPrenotificationAssessmentOptionsOptions: [
    { code: 'code', text: 'text' }
  ],
  manageIncomeOptionMethodsAcExistingAccountStartMonthOptionOptions: [
    { code: 'code', text: 'text' }
  ],
  manageIncomeOptionMethodsAcNumberOfMonthsToFirstChargeOptions: [
    { code: 'code', text: 'text' }
  ],
  manageIncomeOptionMethodsAcPostingCycleOptionsOptions: [
    { code: 'code', text: 'text' }
  ],
  manageIncomeOptionMethodsAcFeeTimingOptionsOptions: [
    { code: 'code', text: 'text' }
  ],
  manageIncomeOptionMethodsAcSpecifiedMonthForAnnualChargeOptions: [
    { code: 'code', text: 'text' }
  ],
  manageIncomeOptionMethodsAcFeeWaiverMonthsOptions: [
    { code: 'code', text: 'text' }
  ],
  manageIncomeOptionMethodsAcDebitActivityOptionOptions: [
    { code: 'code', text: 'text' }
  ],
  manageIncomeOptionMethodsAcWaiverTotalDollarCalculationOptionOptions: [
    { code: 'code', text: 'text' }
  ],
  manageIncomeOptionMethodsAcRateChangePeriodOptions: [
    { code: 'code', text: 'text' }
  ],
  manageIncomeOptionMethodsAcFeeWaiverOptionOptions: [
    { code: 'code', text: 'text' }
  ],
  manageIncomeOptionMethodsAcMinimumBalanceAndExternalStatusOptionOptions: [
    { code: 'code', text: 'text' }
  ],
  manageIncomeOptionMethodsAcReversalStatementMessageWarningTextIDOptions: [
    { code: 'code', text: 'text' }
  ]
};
const mockOnChange =
  (field: keyof WorkflowSetupMethodObjectParams, isRefData?: boolean) =>
  (e: any) =>
    jest.fn();
const _props = {
  metadata,
  onChange: mockOnChange,
  searchValue: ''
};

describe('pages > WorkflowManageIncomeOptionMethods > AnnualChargesStep > SubGridNewMethod', () => {
  it('Info_Basic ', () => {
    const props = {
      ..._props,
      original: { id: 'Info_StatementAndNotificationDisplay' }
    };
    jest.useFakeTimers();
    const wrapper = render(
      <SubGridNewMethod initialMethod={'NEWVERSION'} {...props} />
    );
    jest.runAllTimers();
    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
    const citPriorityOrderInput = wrapper.getByTestId(
      'addNewMethod__manageIncomeOptionMethodsAcStatementPreNotificationMessageLine1_dls-text-box_input'
    ) as HTMLInputElement;
    userEvent.type(citPriorityOrderInput, '12');

    userEvent.clear(citPriorityOrderInput);
    userEvent.type(citPriorityOrderInput, 'cc');

    //
  });
  it('Info_AssessmentCalculations', () => {
    const props = {
      ..._props,
      original: { id: 'Info_AssessmentCalculations' }
    };
    const wrapper = render(
      <SubGridNewMethod initialMethod={'NEWVERSION'} {...props} />
    );
    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
  });
  it('Info_AccountEligibility', () => {
    const props = { ..._props, original: { id: 'Info_AccountEligibility' } };
    const wrapper = render(
      <SubGridNewMethod initialMethod={'NEWVERSION'} {...props} />
    );
    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
  });
  it('Info_AssessmentTiming', () => {
    const props = { ..._props, original: { id: 'Info_AssessmentTiming' } };
    const wrapper = render(
      <SubGridNewMethod initialMethod={'NEWVERSION'} {...props} />
    );
    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
  });
  it('Info_advanced_AssessmentTiming', () => {
    const props = {
      ..._props,
      original: { id: 'Info_advanced_AssessmentTiming' }
    };
    const wrapper = render(
      <SubGridNewMethod initialMethod={'NEWVERSION'} {...props} />
    );
    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
  });
  it('Info_advanced_AssessmentCalculations', () => {
    const props = {
      ..._props,
      original: { id: 'Info_advanced_AssessmentCalculations' }
    };
    const wrapper = render(
      <SubGridNewMethod initialMethod={'NEWVERSION'} {...props} />
    );
    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
  });
  it('Info_advanced_AccountEligibility', () => {
    const props = {
      ..._props,
      original: { id: 'Info_advanced_AccountEligibility' }
    };
    const wrapper = render(
      <SubGridNewMethod initialMethod={'NEWVERSION'} {...props} />
    );
    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
  });
  it('Info_advanced_ReversalOptions', () => {
    const props = {
      ..._props,
      original: { id: 'Info_advanced_ReversalOptions' }
    };
    const wrapper = render(
      <SubGridNewMethod initialMethod={'NEWVERSION'} {...props} />
    );
    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
  });
});
