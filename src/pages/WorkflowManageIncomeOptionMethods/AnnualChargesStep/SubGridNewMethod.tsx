import EnhanceDropdownList from 'app/components/EnhanceDropdownList';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import { matchSearchValue } from 'app/helpers';
import {
  ColumnType,
  ComboBox,
  Grid,
  NumericTextBox,
  TextBox,
  useTranslation
} from 'app/_libraries/_dls';
import {
  formatTruncate,
  viewMoreInfo
} from 'pages/_commons/Utils/formatGridField';
import React, { useMemo } from 'react';
import { parameterIncomeOptionMethodsGroup } from './constant';
import {
  IncomeOptionMethodsMeta,
  ParameterIncomeOptionMethodsList
} from './types';

interface IProps {
  initialMethod: string;
  searchValue: string;
  method: WorkflowSetupMethodObjectParams;
  original: ParameterIncomeOptionMethodsList;
  minDate?: Date;
  maxDate?: Date;
  metadata: IncomeOptionMethodsMeta;
  onChange: (
    field: keyof WorkflowSetupMethodObjectParams,
    isRefData?: boolean
  ) => (e: any) => void;
}
const SubGridNewMethod: React.FC<IProps> = ({
  searchValue,
  method,
  original,
  metadata,
  onChange,
  initialMethod,
  minDate,
  maxDate
}) => {
  const { t } = useTranslation();

  const {
    //Ac - Statement and Notification Display
    manageIncomeOptionMethodsAcAnnualChargeStatementDisplayControlOptions,
    manageIncomeOptionMethodsAcPreAnnualChargeNotificationsOptions,
    manageIncomeOptionMethodsAcRegulationZnotificationGenerationPeriodOptions,
    manageIncomeOptionMethodsAcRegulationZNotificationMediaMethodOptions,
    manageIncomeOptionMethodsAcPreNotificationMessageIdOptions,
    //Ac - Assessment Calculations
    manageIncomeOptionMethodsAcBatchTypeCodeOptions,
    manageIncomeOptionMethodsAcIncludeAnnualChargeInInterestCalculationsOptions,
    manageIncomeOptionMethodsAcPreventDelinquencyFromAnnualFeeBalanceOptions,
    manageIncomeOptionMethodsAcAnnualChargeOptionOptions,
    manageIncomeOptionMethodsAcFirstYearMaximumFeeManagementOptionOptions,
    manageIncomeOptionMethodsAcPrenotificationAmountChangeOptionsOptions,
    //AC - Account Eligibility
    manageIncomeOptionMethodsAcPlasticsRequirementCodeOptions,
    manageIncomeOptionMethodsAcSpecialConditionsForExternalStatusOptions,
    manageIncomeOptionMethodsAcSpecialConditionsForExpiredAccountsOptions,
    manageIncomeOptionMethodsAcAnnualChargeIncludeTableOptions,
    manageIncomeOptionMethodsAcFinanceChargeSuppressionCodeOptions,
    manageIncomeOptionMethodsAcNoPrenotificationAssessmentOptionsOptions,
    //AC - Assessment Timing
    manageIncomeOptionMethodsAcExistingAccountStartMonthOptionOptions,
    manageIncomeOptionMethodsAcNumberOfMonthsToFirstChargeOptions,
    manageIncomeOptionMethodsAcPostingCycleOptionsOptions,
    manageIncomeOptionMethodsAcFeeTimingOptionsOptions,
    //AC - Advanced Parameters - Assessment Timing
    manageIncomeOptionMethodsAcSpecifiedMonthForAnnualChargeOptions,
    manageIncomeOptionMethodsAcFeeWaiverMonthsOptions,
    //AC - Advanced Parameters - Assessment Calculations
    manageIncomeOptionMethodsAcDebitActivityOptionOptions,
    manageIncomeOptionMethodsAcWaiverTotalDollarCalculationOptionOptions,
    manageIncomeOptionMethodsAcRateChangePeriodOptions,
    //AC - Advanced Parameters - Account Eligibility
    manageIncomeOptionMethodsAcFeeWaiverOptionOptions,
    manageIncomeOptionMethodsAcMinimumBalanceAndExternalStatusOptionOptions,
    //AC - Advanced Parameters -  Reversal Options
    manageIncomeOptionMethodsAcReversalStatementMessageWarningTextIDOptions
  } = metadata;

  const data = useMemo(
    () =>
      parameterIncomeOptionMethodsGroup[original.id].filter(p =>
        matchSearchValue(
          [p.fieldName, p.moreInfoText, p.onlinePCF],
          searchValue
        )
      ),
    [original.id, searchValue]
  );

  const renderCombobox = (
    data: RefData[],
    field: keyof WorkflowSetupMethodObjectParams
  ) => {
    const value = data.find(o => o.code === method?.[field]);
    const defaultValue = data.find(o => o.selected);

    return (
      <ComboBox
        placeholder={t('txt_workflow_manage_change_in_terms_select_an_option')}
        size="small"
        textField="text"
        value={value || defaultValue}
        onChange={onChange(field, true)}
        noResult={t('txt_no_results_found_without_dot')}
      >
        {data.map(item => (
          <ComboBox.Item key={item.code} value={item} label={item.text} />
        ))}
      </ComboBox>
    );
  };

  const renderDropdown = (
    data: RefData[],
    field: keyof WorkflowSetupMethodObjectParams
  ) => {
    const value = data.find(o => o.code === method?.[field]);

    return (
      <EnhanceDropdownList
        placeholder={t('txt_workflow_manage_change_in_terms_select_an_option')}
        dataTestId={`addNewMethod__${field}`}
        size="small"
        value={value}
        onChange={onChange(field, true)}
        options={data}
        disabled={
          field ===
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcIncludeAnnualChargeInInterestCalculations
        }
      />
    );
  };

  const valueAccessor = (data: Record<string, any>) => {
    const paramId = data.id as MethodFieldParameterEnum;

    switch (paramId) {
      //Ac - Statement and Notification Display
      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualChargeStatementDisplayControl: {
        return renderDropdown(
          manageIncomeOptionMethodsAcAnnualChargeStatementDisplayControlOptions,
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualChargeStatementDisplayControl
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPreAnnualChargeNotifications: {
        return renderDropdown(
          manageIncomeOptionMethodsAcPreAnnualChargeNotificationsOptions,
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPreAnnualChargeNotifications
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcRegulationZNotificationMediaMethod: {
        return renderDropdown(
          manageIncomeOptionMethodsAcRegulationZNotificationMediaMethodOptions,
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcRegulationZNotificationMediaMethod
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcRegulationZnotificationGenerationPeriod: {
        return renderDropdown(
          manageIncomeOptionMethodsAcRegulationZnotificationGenerationPeriodOptions,
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcRegulationZnotificationGenerationPeriod
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine1: {
        return (
          <TextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__manageIncomeOptionMethodsAcStatementPreNotificationMessageLine1"
            value={
              method?.[
                MethodFieldParameterEnum
                  .ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine1
              ] || ''
            }
            size="sm"
            maxLength={61}
            onChange={onChange(
              MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine1
            )}
          />
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine2Before: {
        return (
          <TextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__manageIncomeOptionMethodsAcStatementPreNotificationMessageLine2Before"
            value={
              method?.[
                MethodFieldParameterEnum
                  .ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine2Before
              ] || ''
            }
            size="sm"
            maxLength={45}
            onChange={onChange(
              MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine2Before
            )}
          />
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine2After: {
        return (
          <TextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__manageIncomeOptionMethodsAcStatementPreNotificationMessageLine2After"
            value={
              method?.[
                MethodFieldParameterEnum
                  .ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine2After
              ] || ''
            }
            size="sm"
            maxLength={7}
            onChange={onChange(
              MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine2After
            )}
          />
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine3: {
        return (
          <TextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__manageIncomeOptionMethodsAcStatementPreNotificationMessageLine3"
            value={
              method?.[
                MethodFieldParameterEnum
                  .ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine3
              ] || ''
            }
            size="sm"
            maxLength={61}
            onChange={onChange(
              MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine3
            )}
          />
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine4: {
        return (
          <TextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__manageIncomeOptionMethodsAcStatementPreNotificationMessageLine4"
            value={
              method?.[
                MethodFieldParameterEnum
                  .ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine4
              ] || ''
            }
            size="sm"
            maxLength={61}
            onChange={onChange(
              MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine4
            )}
          />
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine5: {
        return (
          <TextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__manageIncomeOptionMethodsAcStatementPreNotificationMessageLine5"
            value={
              method?.[
                MethodFieldParameterEnum
                  .ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine5
              ] || ''
            }
            size="sm"
            maxLength={61}
            onChange={onChange(
              MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine5
            )}
          />
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine6: {
        return (
          <TextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__manageIncomeOptionMethodsAcStatementPreNotificationMessageLine6"
            value={
              method?.[
                MethodFieldParameterEnum
                  .ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine6
              ] || ''
            }
            size="sm"
            maxLength={61}
            onChange={onChange(
              MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine6
            )}
          />
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine7: {
        return (
          <TextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__manageIncomeOptionMethodsAcStatementPreNotificationMessageLine7"
            value={
              method?.[
                MethodFieldParameterEnum
                  .ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine7
              ] || ''
            }
            size="sm"
            maxLength={61}
            onChange={onChange(
              MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine7
            )}
          />
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine8: {
        return (
          <TextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__manageIncomeOptionMethodsAcStatementPreNotificationMessageLine8"
            value={
              method?.[
                MethodFieldParameterEnum
                  .ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine8
              ] || ''
            }
            size="sm"
            maxLength={61}
            onChange={onChange(
              MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine8
            )}
          />
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine9: {
        return (
          <TextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__manageIncomeOptionMethodsAcStatementPreNotificationMessageLine9"
            value={
              method?.[
                MethodFieldParameterEnum
                  .ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine9
              ] || ''
            }
            size="sm"
            maxLength={61}
            onChange={onChange(
              MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine9
            )}
          />
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine10: {
        return (
          <TextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__manageIncomeOptionMethodsAcStatementPreNotificationMessageLine10"
            value={
              method?.[
                MethodFieldParameterEnum
                  .ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine10
              ] || ''
            }
            size="sm"
            maxLength={61}
            onChange={onChange(
              MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine10
            )}
          />
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine11: {
        return (
          <TextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__manageIncomeOptionMethodsAcStatementPreNotificationMessageLine11"
            value={
              method?.[
                MethodFieldParameterEnum
                  .ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine11
              ] || ''
            }
            size="sm"
            maxLength={61}
            onChange={onChange(
              MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine11
            )}
          />
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine12: {
        return (
          <TextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__manageIncomeOptionMethodsAcStatementPreNotificationMessageLine12"
            value={
              method?.[
                MethodFieldParameterEnum
                  .ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine12
              ] || ''
            }
            size="sm"
            maxLength={61}
            onChange={onChange(
              MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine12
            )}
          />
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualChargeStatementDescription: {
        return (
          <TextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__manageIncomeOptionMethodsAcAnnualChargeStatementDescription"
            value={
              method?.[
                MethodFieldParameterEnum
                  .ManageIncomeOptionMethodsAcAnnualChargeStatementDescription
              ] || ''
            }
            size="sm"
            maxLength={14}
            onChange={onChange(
              MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualChargeStatementDescription
            )}
          />
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPreNotificationMessageId: {
        return renderCombobox(
          manageIncomeOptionMethodsAcPreNotificationMessageIdOptions,
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPreNotificationMessageId
        );
      }

      //Ac - Assessment Calculations
      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcBatchTypeCode: {
        return renderDropdown(
          manageIncomeOptionMethodsAcBatchTypeCodeOptions,
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcBatchTypeCode
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcIncludeAnnualChargeInInterestCalculations: {
        return renderDropdown(
          manageIncomeOptionMethodsAcIncludeAnnualChargeInInterestCalculationsOptions,
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcIncludeAnnualChargeInInterestCalculations
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPreventDelinquencyFromAnnualFeeBalance: {
        return renderDropdown(
          manageIncomeOptionMethodsAcPreventDelinquencyFromAnnualFeeBalanceOptions,
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPreventDelinquencyFromAnnualFeeBalance
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualChargeOption: {
        return renderDropdown(
          manageIncomeOptionMethodsAcAnnualChargeOptionOptions,
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualChargeOption
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount1: {
        return (
          <NumericTextBox
            dataTestId="addNewMethod__manageIncomeOptionMethodsAcAmount1"
            size="sm"
            format="c2"
            showStep={false}
            maxLength={13}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount1
              ] || ''
            }
            onChange={onChange(
              MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount1
            )}
          />
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount2: {
        return (
          <NumericTextBox
            dataTestId="addNewMethod__manageIncomeOptionMethodsAcAmount2"
            size="sm"
            format="c2"
            showStep={false}
            maxLength={13}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount2
              ] || ''
            }
            onChange={onChange(
              MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount2
            )}
          />
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount3: {
        return (
          <NumericTextBox
            dataTestId="addNewMethod__manageIncomeOptionMethodsAcAmount3"
            size="sm"
            format="c2"
            showStep={false}
            maxLength={13}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount3
              ] || ''
            }
            onChange={onChange(
              MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount3
            )}
          />
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount4: {
        return (
          <NumericTextBox
            dataTestId="addNewMethod__manageIncomeOptionMethodsAcAmount4"
            size="sm"
            format="c2"
            showStep={false}
            maxLength={13}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount4
              ] || ''
            }
            onChange={onChange(
              MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount4
            )}
          />
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount5: {
        return (
          <NumericTextBox
            dataTestId="addNewMethod__manageIncomeOptionMethodsAcAmount5"
            size="sm"
            format="c2"
            showStep={false}
            maxLength={13}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount5
              ] || ''
            }
            onChange={onChange(
              MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount5
            )}
          />
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount6: {
        return (
          <NumericTextBox
            dataTestId="addNewMethod__manageIncomeOptionMethodsAcAmount6"
            size="sm"
            format="c2"
            showStep={false}
            maxLength={13}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount6
              ] || ''
            }
            onChange={onChange(
              MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount6
            )}
          />
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAdditionalPlasticCharge: {
        return (
          <NumericTextBox
            dataTestId="addNewMethod__manageIncomeOptionMethodsAcAdditionalPlasticCharge"
            size="sm"
            format="c2"
            showStep={false}
            maxLength={13}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum
                  .ManageIncomeOptionMethodsAcAdditionalPlasticCharge
              ] || ''
            }
            onChange={onChange(
              MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAdditionalPlasticCharge
            )}
          />
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFirstYearMaximumFeeManagementOption: {
        return renderDropdown(
          manageIncomeOptionMethodsAcFirstYearMaximumFeeManagementOptionOptions,
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFirstYearMaximumFeeManagementOption
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPrenotificationAmountChangeOptions: {
        return renderDropdown(
          manageIncomeOptionMethodsAcPrenotificationAmountChangeOptionsOptions,
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPrenotificationAmountChangeOptions
        );
      }

      //AC - Account Eligibility
      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPlasticsRequirementCode: {
        return renderDropdown(
          manageIncomeOptionMethodsAcPlasticsRequirementCodeOptions,
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPlasticsRequirementCode
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcSpecialConditionsForExternalStatus: {
        return renderDropdown(
          manageIncomeOptionMethodsAcSpecialConditionsForExternalStatusOptions,
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcSpecialConditionsForExternalStatus
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcSpecialConditionsForExpiredAccounts: {
        return renderDropdown(
          manageIncomeOptionMethodsAcSpecialConditionsForExpiredAccountsOptions,
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcSpecialConditionsForExpiredAccounts
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualChargeIncludeTable: {
        return renderCombobox(
          manageIncomeOptionMethodsAcAnnualChargeIncludeTableOptions,
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualChargeIncludeTable
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFinanceChargeSuppressionCode: {
        return renderDropdown(
          manageIncomeOptionMethodsAcFinanceChargeSuppressionCodeOptions,
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFinanceChargeSuppressionCode
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcNoPrenotificationAssessmentOptions: {
        return renderDropdown(
          manageIncomeOptionMethodsAcNoPrenotificationAssessmentOptionsOptions,
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcNoPrenotificationAssessmentOptions
        );
      }

      //AC - Assessment Timing
      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcExistingAccountStartMonthOption: {
        return renderDropdown(
          manageIncomeOptionMethodsAcExistingAccountStartMonthOptionOptions,
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcExistingAccountStartMonthOption
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcNumberOfMonthsToFirstCharge: {
        return renderDropdown(
          manageIncomeOptionMethodsAcNumberOfMonthsToFirstChargeOptions,
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcNumberOfMonthsToFirstCharge
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPostingCycleOptions: {
        return renderDropdown(
          manageIncomeOptionMethodsAcPostingCycleOptionsOptions,
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPostingCycleOptions
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeTimingOptions: {
        return renderDropdown(
          manageIncomeOptionMethodsAcFeeTimingOptionsOptions,
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeTimingOptions
        );
      }

      //AC - Advanced Parameters - Assessment Timing
      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcSpecifiedMonthForAnnualCharge: {
        return renderDropdown(
          manageIncomeOptionMethodsAcSpecifiedMonthForAnnualChargeOptions,
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcSpecifiedMonthForAnnualCharge
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeWaiverMonths: {
        return renderDropdown(
          manageIncomeOptionMethodsAcFeeWaiverMonthsOptions,
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeWaiverMonths
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcSecondAnnualFeeNumberOfDays: {
        return (
          <NumericTextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__manageIncomeOptionMethodsAcSecondAnnualFeeNumberOfDays"
            size="sm"
            format="n"
            maxLength={2}
            autoFocus={false}
            min={0}
            max={99}
            value={
              method?.[
                MethodFieldParameterEnum
                  .ManageIncomeOptionMethodsAcSecondAnnualFeeNumberOfDays
              ]
            }
            onChange={onChange(
              MethodFieldParameterEnum.ManageIncomeOptionMethodsAcSecondAnnualFeeNumberOfDays
            )}
          />
        );
      }

      //AC - Advanced Parameters - Assessment Calculations
      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcDebitActivityOption: {
        return renderDropdown(
          manageIncomeOptionMethodsAcDebitActivityOptionOptions,
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcDebitActivityOption
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcWaiverTotalDollarCalculationOption: {
        return renderDropdown(
          manageIncomeOptionMethodsAcWaiverTotalDollarCalculationOptionOptions,
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcWaiverTotalDollarCalculationOption
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcRateChangePeriod: {
        return renderDropdown(
          manageIncomeOptionMethodsAcRateChangePeriodOptions,
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcRateChangePeriod
        );
      }

      //AC - Advanced Parameters - Account Eligibility
      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeWaiverOption: {
        return renderDropdown(
          manageIncomeOptionMethodsAcFeeWaiverOptionOptions,
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeWaiverOption
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeWaiverAmount: {
        return (
          <NumericTextBox
            dataTestId="addNewMethod__manageIncomeOptionMethodsAcFeeWaiverAmount"
            size="sm"
            format="c0"
            showStep={false}
            maxLength={11}
            min={0}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum
                  .ManageIncomeOptionMethodsAcFeeWaiverAmount
              ] || ''
            }
            onChange={onChange(
              MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeWaiverAmount
            )}
          />
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcMinimumBalanceAndExternalStatusOption: {
        return renderDropdown(
          manageIncomeOptionMethodsAcMinimumBalanceAndExternalStatusOptionOptions,
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcMinimumBalanceAndExternalStatusOption
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatusAMinimumBalance: {
        return (
          <NumericTextBox
            dataTestId="addNewMethod__manageIncomeOptionMethodsAcStatusAMinimumBalance"
            size="sm"
            format="c2"
            showStep={false}
            maxLength={17}
            min={0}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum
                  .ManageIncomeOptionMethodsAcStatusAMinimumBalance
              ] || ''
            }
            onChange={onChange(
              MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatusAMinimumBalance
            )}
          />
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatusCMinimumBalance: {
        return (
          <NumericTextBox
            dataTestId="addNewMethod__manageIncomeOptionMethodsAcStatusCMinimumBalance"
            size="sm"
            format="c2"
            showStep={false}
            maxLength={17}
            min={0}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum
                  .ManageIncomeOptionMethodsAcStatusCMinimumBalance
              ] || ''
            }
            onChange={onChange(
              MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatusCMinimumBalance
            )}
          />
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatusEMinimumBalance: {
        return (
          <NumericTextBox
            dataTestId="addNewMethod__manageIncomeOptionMethodsAcStatusEMinimumBalance"
            size="sm"
            format="c2"
            showStep={false}
            maxLength={17}
            min={0}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum
                  .ManageIncomeOptionMethodsAcStatusEMinimumBalance
              ] || ''
            }
            onChange={onChange(
              MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatusEMinimumBalance
            )}
          />
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatusFMinimumBalance: {
        return (
          <NumericTextBox
            dataTestId="addNewMethod__manageIncomeOptionMethodsAcStatusFMinimumBalance"
            size="sm"
            format="c2"
            showStep={false}
            maxLength={17}
            min={0}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum
                  .ManageIncomeOptionMethodsAcStatusFMinimumBalance
              ] || ''
            }
            onChange={onChange(
              MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatusFMinimumBalance
            )}
          />
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatusBlankMinimumBalance: {
        return (
          <NumericTextBox
            dataTestId="addNewMethod__manageIncomeOptionMethodsAcStatusBlankMinimumBalance"
            size="sm"
            format="c2"
            showStep={false}
            maxLength={17}
            min={0}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum
                  .ManageIncomeOptionMethodsAcStatusBlankMinimumBalance
              ] || ''
            }
            onChange={onChange(
              MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatusBlankMinimumBalance
            )}
          />
        );
      }

      //AC - Advanced Parameters -  Reversal Options
      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcReversalNumberOfCycles: {
        return (
          <NumericTextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__manageIncomeOptionMethodsAcReversalNumberOfCycles"
            size="sm"
            format="n"
            maxLength={1}
            min={0}
            max={9}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum
                  .ManageIncomeOptionMethodsAcReversalNumberOfCycles
              ]
            }
            onChange={onChange(
              MethodFieldParameterEnum.ManageIncomeOptionMethodsAcReversalNumberOfCycles
            )}
          />
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcReversalReasonCode: {
        return (
          <TextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__manageIncomeOptionMethodsAcReversalReasonCode"
            value={
              method?.[
                MethodFieldParameterEnum
                  .ManageIncomeOptionMethodsAcReversalReasonCode
              ] || ''
            }
            size="sm"
            maxLength={2}
            onChange={onChange(
              MethodFieldParameterEnum.ManageIncomeOptionMethodsAcReversalReasonCode
            )}
          />
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcReversalStatementMessageWarningOption: {
        return (
          <NumericTextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__manageIncomeOptionMethodsAcReversalStatementMessageWarningOption"
            size="sm"
            format="n"
            defaultValue={0}
            maxLength={1}
            min={0}
            max={9}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum.ManageIncomeOptionMethodsAcReversalStatementMessageWarningOption ||
                  0
              ]
            }
            onChange={onChange(
              MethodFieldParameterEnum.ManageIncomeOptionMethodsAcReversalStatementMessageWarningOption ||
                0
            )}
          />
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcReversalStatementMessageWarningTextID: {
        return renderCombobox(
          manageIncomeOptionMethodsAcReversalStatementMessageWarningTextIDOptions,
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcReversalStatementMessageWarningTextID
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualFeeReversalBatchID: {
        return (
          <TextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__manageIncomeOptionMethodsAcAnnualFeeReversalBatchID"
            value={
              method?.[
                MethodFieldParameterEnum
                  .ManageIncomeOptionMethodsAcAnnualFeeReversalBatchID
              ] || ''
            }
            size="sm"
            maxLength={2}
            onChange={onChange(
              MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualFeeReversalBatchID
            )}
          />
        );
      }
    }
  };

  const columns: ColumnType[] = [
    {
      id: 'fieldName',
      Header: t('txt_business_name'),
      accessor: formatTruncate(['fieldName']),
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'onlinePCF',
      Header: t('txt_manage_penalty_fee_online_PCF'),
      accessor: formatTruncate(['onlinePCF']),
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'value',
      Header: t('txt_value'),
      cellBodyProps: { className: 'py-8' },
      accessor: valueAccessor
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      accessor: viewMoreInfo(['moreInfo'], t),
      width: 105,
      cellBodyProps: { className: 'pt-12 pb-8' }
    }
  ];

  return (
    <div className="p-20">
      <Grid columns={columns} data={data} />
    </div>
  );
};

export default SubGridNewMethod;
