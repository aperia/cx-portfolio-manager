import { MethodFieldParameterEnum } from 'app/constants/enums';
import {
  formatCommon,
  mapGridExpandCollapse,
  methodParamsToObject
} from 'app/helpers';
import { Grid, useTranslation } from 'app/_libraries/_dls';
import React, { useState } from 'react';
import newMethodColumns from '../ConfigureParameters/newMethodColumns';
import { parameterIncomeOptionMethodsList } from './constant';
import SubGridPreviewMethod from './SubGridPreviewMethod';
import {
  IncomeOptionMethodsMeta,
  ParameterIncomeOptionMethodsListIds
} from './types';

export interface SubGridProps {
  original: WorkflowSetupMethod;
  metadata: IncomeOptionMethodsMeta;
}
const SubRowGrid: React.FC<SubGridProps> = ({ original, metadata }) => {
  const {
    //Ac - Statement and Notification Display
    manageIncomeOptionMethodsAcAnnualChargeStatementDisplayControlOptions,
    manageIncomeOptionMethodsAcPreAnnualChargeNotificationsOptions,
    manageIncomeOptionMethodsAcRegulationZnotificationGenerationPeriodOptions,
    manageIncomeOptionMethodsAcRegulationZNotificationMediaMethodOptions,
    manageIncomeOptionMethodsAcPreNotificationMessageIdOptions,
    //Ac - Assessment Calculations
    manageIncomeOptionMethodsAcBatchTypeCodeOptions,
    manageIncomeOptionMethodsAcIncludeAnnualChargeInInterestCalculationsOptions,
    manageIncomeOptionMethodsAcPreventDelinquencyFromAnnualFeeBalanceOptions,
    manageIncomeOptionMethodsAcAnnualChargeOptionOptions,
    manageIncomeOptionMethodsAcFirstYearMaximumFeeManagementOptionOptions,
    manageIncomeOptionMethodsAcPrenotificationAmountChangeOptionsOptions,
    //AC - Account Eligibility
    manageIncomeOptionMethodsAcPlasticsRequirementCodeOptions,
    manageIncomeOptionMethodsAcSpecialConditionsForExternalStatusOptions,
    manageIncomeOptionMethodsAcSpecialConditionsForExpiredAccountsOptions,
    manageIncomeOptionMethodsAcAnnualChargeIncludeTableOptions,
    manageIncomeOptionMethodsAcFinanceChargeSuppressionCodeOptions,
    manageIncomeOptionMethodsAcNoPrenotificationAssessmentOptionsOptions,
    //AC - Assessment Timing
    manageIncomeOptionMethodsAcExistingAccountStartMonthOptionOptions,
    manageIncomeOptionMethodsAcNumberOfMonthsToFirstChargeOptions,
    manageIncomeOptionMethodsAcPostingCycleOptionsOptions,
    manageIncomeOptionMethodsAcFeeTimingOptionsOptions,
    //AC - Advanced Parameters - Assessment Timing
    manageIncomeOptionMethodsAcSpecifiedMonthForAnnualChargeOptions,
    manageIncomeOptionMethodsAcFeeWaiverMonthsOptions,
    //AC - Advanced Parameters - Assessment Calculations
    manageIncomeOptionMethodsAcDebitActivityOptionOptions,
    manageIncomeOptionMethodsAcWaiverTotalDollarCalculationOptionOptions,
    manageIncomeOptionMethodsAcRateChangePeriodOptions,
    //AC - Advanced Parameters - Account Eligibility
    manageIncomeOptionMethodsAcFeeWaiverOptionOptions,
    manageIncomeOptionMethodsAcMinimumBalanceAndExternalStatusOptionOptions,
    //AC - Advanced Parameters -  Reversal Options
    manageIncomeOptionMethodsAcReversalStatementMessageWarningTextIDOptions
  } = metadata;

  const convertedObject = methodParamsToObject(original);

  const { t } = useTranslation();

  const data: Record<MethodFieldParameterEnum, any> = {
    //Ac - Statement and Notification Display
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualChargeStatementDisplayControl]:
      manageIncomeOptionMethodsAcAnnualChargeStatementDisplayControlOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ManageIncomeOptionMethodsAcAnnualChargeStatementDisplayControl
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPreAnnualChargeNotifications]:
      manageIncomeOptionMethodsAcPreAnnualChargeNotificationsOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ManageIncomeOptionMethodsAcPreAnnualChargeNotifications
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcRegulationZnotificationGenerationPeriod]:
      manageIncomeOptionMethodsAcRegulationZnotificationGenerationPeriodOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ManageIncomeOptionMethodsAcRegulationZnotificationGenerationPeriod
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcRegulationZNotificationMediaMethod]:
      manageIncomeOptionMethodsAcRegulationZNotificationMediaMethodOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ManageIncomeOptionMethodsAcRegulationZNotificationMediaMethod
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine1]:
      convertedObject[
        MethodFieldParameterEnum
          .ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine1
      ],
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine2Before]:
      convertedObject[
        MethodFieldParameterEnum
          .ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine2Before
      ],
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine2After]:
      convertedObject[
        MethodFieldParameterEnum
          .ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine2After
      ],
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine3]:
      convertedObject[
        MethodFieldParameterEnum
          .ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine3
      ],
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine4]:
      convertedObject[
        MethodFieldParameterEnum
          .ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine4
      ],
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine5]:
      convertedObject[
        MethodFieldParameterEnum
          .ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine5
      ],
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine6]:
      convertedObject[
        MethodFieldParameterEnum
          .ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine6
      ],
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine7]:
      convertedObject[
        MethodFieldParameterEnum
          .ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine7
      ],
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine8]:
      convertedObject[
        MethodFieldParameterEnum
          .ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine8
      ],
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine9]:
      convertedObject[
        MethodFieldParameterEnum
          .ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine9
      ],
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine10]:
      convertedObject[
        MethodFieldParameterEnum
          .ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine10
      ],
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine11]:
      convertedObject[
        MethodFieldParameterEnum
          .ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine11
      ],
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine12]:
      convertedObject[
        MethodFieldParameterEnum
          .ManageIncomeOptionMethodsAcStatementPreNotificationMessageLine12
      ],
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualChargeStatementDescription]:
      convertedObject[
        MethodFieldParameterEnum
          .ManageIncomeOptionMethodsAcAnnualChargeStatementDescription
      ],
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPreNotificationMessageId]:
      manageIncomeOptionMethodsAcPreNotificationMessageIdOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ManageIncomeOptionMethodsAcPreNotificationMessageId
          ]
      )?.text,

    //Ac - Assessment Calculations
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcBatchTypeCode]:
      manageIncomeOptionMethodsAcBatchTypeCodeOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.ManageIncomeOptionMethodsAcBatchTypeCode
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcIncludeAnnualChargeInInterestCalculations]:
      manageIncomeOptionMethodsAcIncludeAnnualChargeInInterestCalculationsOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ManageIncomeOptionMethodsAcIncludeAnnualChargeInInterestCalculations
          ]
      )?.text,

    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPreventDelinquencyFromAnnualFeeBalance]:
      manageIncomeOptionMethodsAcPreventDelinquencyFromAnnualFeeBalanceOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ManageIncomeOptionMethodsAcPreventDelinquencyFromAnnualFeeBalance
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualChargeOption]:
      manageIncomeOptionMethodsAcAnnualChargeOptionOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ManageIncomeOptionMethodsAcAnnualChargeOption
          ]
      )?.text,

    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount1]: formatCommon(
      convertedObject[
        MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount1
      ]!
    ).currency(2),

    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount2]: formatCommon(
      convertedObject[
        MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount2
      ]!
    ).currency(2),

    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount3]: formatCommon(
      convertedObject[
        MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount3
      ]!
    ).currency(2),

    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount4]: formatCommon(
      convertedObject[
        MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount4
      ]!
    ).currency(2),

    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount5]: formatCommon(
      convertedObject[
        MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount5
      ]!
    ).currency(2),

    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount6]: formatCommon(
      convertedObject[
        MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAmount6
      ]!
    ).currency(2),

    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAdditionalPlasticCharge]:
      formatCommon(
        convertedObject[
          MethodFieldParameterEnum
            .ManageIncomeOptionMethodsAcAdditionalPlasticCharge
        ]!
      ).currency(2),

    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFirstYearMaximumFeeManagementOption]:
      manageIncomeOptionMethodsAcFirstYearMaximumFeeManagementOptionOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ManageIncomeOptionMethodsAcFirstYearMaximumFeeManagementOption
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPrenotificationAmountChangeOptions]:
      manageIncomeOptionMethodsAcPrenotificationAmountChangeOptionsOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ManageIncomeOptionMethodsAcPrenotificationAmountChangeOptions
          ]
      )?.text,
    //AC - Account Eligibility
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPlasticsRequirementCode]:
      manageIncomeOptionMethodsAcPlasticsRequirementCodeOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ManageIncomeOptionMethodsAcPlasticsRequirementCode
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcSpecialConditionsForExternalStatus]:
      manageIncomeOptionMethodsAcSpecialConditionsForExternalStatusOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ManageIncomeOptionMethodsAcSpecialConditionsForExternalStatus
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcSpecialConditionsForExpiredAccounts]:
      manageIncomeOptionMethodsAcSpecialConditionsForExpiredAccountsOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ManageIncomeOptionMethodsAcSpecialConditionsForExpiredAccounts
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualChargeIncludeTable]:
      manageIncomeOptionMethodsAcAnnualChargeIncludeTableOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ManageIncomeOptionMethodsAcAnnualChargeIncludeTable
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFinanceChargeSuppressionCode]:
      manageIncomeOptionMethodsAcFinanceChargeSuppressionCodeOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ManageIncomeOptionMethodsAcFinanceChargeSuppressionCode
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcNoPrenotificationAssessmentOptions]:
      manageIncomeOptionMethodsAcNoPrenotificationAssessmentOptionsOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ManageIncomeOptionMethodsAcNoPrenotificationAssessmentOptions
          ]
      )?.text,
    //AC - Assessment Timing
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcExistingAccountStartMonthOption]:
      manageIncomeOptionMethodsAcExistingAccountStartMonthOptionOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ManageIncomeOptionMethodsAcExistingAccountStartMonthOption
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcNumberOfMonthsToFirstCharge]:
      manageIncomeOptionMethodsAcNumberOfMonthsToFirstChargeOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ManageIncomeOptionMethodsAcNumberOfMonthsToFirstCharge
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcPostingCycleOptions]:
      manageIncomeOptionMethodsAcPostingCycleOptionsOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ManageIncomeOptionMethodsAcPostingCycleOptions
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeTimingOptions]:
      manageIncomeOptionMethodsAcFeeTimingOptionsOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeTimingOptions
          ]
      )?.text,
    //AC - Advanced Parameters - Assessment Timing
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcSpecifiedMonthForAnnualCharge]:
      manageIncomeOptionMethodsAcSpecifiedMonthForAnnualChargeOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ManageIncomeOptionMethodsAcSpecifiedMonthForAnnualCharge
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeWaiverMonths]:
      manageIncomeOptionMethodsAcFeeWaiverMonthsOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeWaiverMonths
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcSecondAnnualFeeNumberOfDays]:
      convertedObject[
        MethodFieldParameterEnum
          .ManageIncomeOptionMethodsAcSecondAnnualFeeNumberOfDays
      ],
    //AC - Advanced Parameters - Assessment Calculations
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcDebitActivityOption]:
      manageIncomeOptionMethodsAcDebitActivityOptionOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ManageIncomeOptionMethodsAcDebitActivityOption
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcWaiverTotalDollarCalculationOption]:
      manageIncomeOptionMethodsAcWaiverTotalDollarCalculationOptionOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ManageIncomeOptionMethodsAcWaiverTotalDollarCalculationOption
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcRateChangePeriod]:
      manageIncomeOptionMethodsAcRateChangePeriodOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.ManageIncomeOptionMethodsAcRateChangePeriod
          ]
      )?.text,
    //AC - Advanced Parameters - Account Eligibility
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeWaiverOption]:
      manageIncomeOptionMethodsAcFeeWaiverOptionOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeWaiverOption
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeWaiverAmount]:
      formatCommon(
        convertedObject[
          MethodFieldParameterEnum.ManageIncomeOptionMethodsAcFeeWaiverAmount
        ]!
      ).currency(0),
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcMinimumBalanceAndExternalStatusOption]:
      manageIncomeOptionMethodsAcMinimumBalanceAndExternalStatusOptionOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ManageIncomeOptionMethodsAcMinimumBalanceAndExternalStatusOption
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatusAMinimumBalance]:
      formatCommon(
        convertedObject[
          MethodFieldParameterEnum
            .ManageIncomeOptionMethodsAcStatusAMinimumBalance
        ]!
      ).currency(2),
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatusCMinimumBalance]:
      formatCommon(
        convertedObject[
          MethodFieldParameterEnum
            .ManageIncomeOptionMethodsAcStatusCMinimumBalance
        ]!
      ).currency(2),
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatusEMinimumBalance]:
      formatCommon(
        convertedObject[
          MethodFieldParameterEnum
            .ManageIncomeOptionMethodsAcStatusEMinimumBalance
        ]!
      ).currency(2),
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatusFMinimumBalance]:
      formatCommon(
        convertedObject[
          MethodFieldParameterEnum
            .ManageIncomeOptionMethodsAcStatusFMinimumBalance
        ]!
      ).currency(2),
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcStatusBlankMinimumBalance]:
      formatCommon(
        convertedObject[
          MethodFieldParameterEnum
            .ManageIncomeOptionMethodsAcStatusBlankMinimumBalance
        ]!
      ).currency(2),
    //AC - Advanced Parameters -  Reversal Options
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcReversalNumberOfCycles]:
      convertedObject[
        MethodFieldParameterEnum
          .ManageIncomeOptionMethodsAcReversalNumberOfCycles
      ],
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcReversalReasonCode]:
      convertedObject[
        MethodFieldParameterEnum.ManageIncomeOptionMethodsAcReversalReasonCode
      ],
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcReversalStatementMessageWarningOption]:
      convertedObject[
        MethodFieldParameterEnum
          .ManageIncomeOptionMethodsAcReversalStatementMessageWarningOption
      ],
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcReversalStatementMessageWarningTextID]:
      manageIncomeOptionMethodsAcReversalStatementMessageWarningTextIDOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ManageIncomeOptionMethodsAcReversalStatementMessageWarningTextID
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsAcAnnualFeeReversalBatchID]:
      convertedObject[
        MethodFieldParameterEnum
          .ManageIncomeOptionMethodsAcAnnualFeeReversalBatchID
      ]
  } as Record<MethodFieldParameterEnum, any>;

  const [expandedList, setExpandedList] = useState<
    ParameterIncomeOptionMethodsListIds[]
  >(['Info_StatementAndNotificationDisplay']);

  return (
    <div className="p-20 overflow-hidden">
      <Grid
        columns={newMethodColumns(t)}
        data={parameterIncomeOptionMethodsList}
        subRow={({ original: _original }: MagicKeyValue) => (
          <SubGridPreviewMethod
            original={_original}
            metadata={data}
            methodType={original.methodType}
          />
        )}
        dataItemKey="id"
        togglable
        expandedItemKey="id"
        expandedList={expandedList}
        onExpand={setExpandedList as any}
        toggleButtonConfigList={parameterIncomeOptionMethodsList.map(
          mapGridExpandCollapse('id', t)
        )}
      />
    </div>
  );
};

export default SubRowGrid;
