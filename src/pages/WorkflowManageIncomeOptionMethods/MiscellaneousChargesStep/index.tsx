import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React from 'react';
import ConfigureParameters, {
  ConfigureParametersFormValue
} from '../ConfigureParameters';
import Summary from '../ConfigureParameters/Summary';
import parseFormValues from './parseFormValues';

const MiscellaneousChargesStep: React.FC<
  WorkflowSetupProps<ConfigureParametersFormValue>
> = prop => {
  return <ConfigureParameters {...prop} />;
};

const ExtraStaticMiscellaneousChargesStep =
  MiscellaneousChargesStep as WorkflowSetupStaticProp;

ExtraStaticMiscellaneousChargesStep.summaryComponent = Summary;
ExtraStaticMiscellaneousChargesStep.defaultValues = {
  isValid: false,
  isAnnualChargesStep: false,
  methods: []
};
ExtraStaticMiscellaneousChargesStep.parseFormValues = parseFormValues;

export default ExtraStaticMiscellaneousChargesStep;
