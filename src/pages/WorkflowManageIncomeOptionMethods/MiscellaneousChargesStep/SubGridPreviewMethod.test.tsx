import { render } from '@testing-library/react';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import React from 'react';
import SubGridPreviewMethod from './SubGridPreviewMethod';

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

const metadata = {
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsMcJoiningFeeAmount]:
    'none',
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsMcJoiningFeeStatementDescription]:
    'test',
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsMcJoiningFeeBatchId]:
    'test',
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsMcCardReplacementFeeAmount]:
    'test',
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsMcCardReplacementFeeBatchId]:
    'test',
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsMcCardReplacementFeeStatementDescription]:
    'test',
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsMcMiscellaneousFeeChargeId]:
    'test',
  [MethodFieldParameterEnum.ManageIncomeOptionMethodsMcFirstYearMaximumFeeManagement]:
    'test_fee'
};

describe('pages > WorkflowManageIncomeOptionMethods > MiscellaneousChargesStep > SubGridPreviewMethod', () => {
  it('Info_Basic > empty designPromotionBasicInformation5', () => {
    const original = { id: 'Info_Settings' };
    const props = {
      metadata,
      original
    };
    const wrapper = render(<SubGridPreviewMethod {...props} />);
    expect(wrapper.getByText('test_fee')).toBeInTheDocument();
  });
});
