import { MethodFieldParameterEnum } from 'app/constants/enums';
import { formatCommon, methodParamsToObject } from 'app/helpers';
import React from 'react';
import SubGridPreviewMethod from './SubGridPreviewMethod';
import { MiscellaneousChargesMeta } from './types';

export interface SubGridProps {
  original: WorkflowSetupMethod;
  metadata: MiscellaneousChargesMeta;
}
const SubRowGrid: React.FC<SubGridProps> = ({ original, metadata }) => {
  const {
    manageIncomeOptionMethodsMcJoiningFeeStatementDescriptionOptions,
    manageIncomeOptionMethodsMcJoiningFeeBatchIdOptions,
    manageIncomeOptionMethodsMcCardReplacementFeeBatchIdOptions,
    manageIncomeOptionMethodsMcCardReplacementFeeStatementDescriptionOptions,
    manageIncomeOptionMethodsMcFirstYearMaximumFeeManagementOptions
  } = metadata;

  const convertedObject = methodParamsToObject(original);

  const data: Record<MethodFieldParameterEnum, any> = {
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsMcJoiningFeeAmount]:
      formatCommon(
        convertedObject[
          MethodFieldParameterEnum.ManageIncomeOptionMethodsMcJoiningFeeAmount
        ]!
      ).currency(2),
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsMcJoiningFeeStatementDescription]:
      manageIncomeOptionMethodsMcJoiningFeeStatementDescriptionOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ManageIncomeOptionMethodsMcJoiningFeeStatementDescription
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsMcJoiningFeeBatchId]:
      manageIncomeOptionMethodsMcJoiningFeeBatchIdOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ManageIncomeOptionMethodsMcJoiningFeeBatchId
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsMcCardReplacementFeeAmount]:
      formatCommon(
        convertedObject[
          MethodFieldParameterEnum
            .ManageIncomeOptionMethodsMcCardReplacementFeeAmount
        ]!
      ).currency(2),
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsMcCardReplacementFeeBatchId]:
      manageIncomeOptionMethodsMcCardReplacementFeeBatchIdOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ManageIncomeOptionMethodsMcCardReplacementFeeBatchId
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsMcCardReplacementFeeStatementDescription]:
      manageIncomeOptionMethodsMcCardReplacementFeeStatementDescriptionOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ManageIncomeOptionMethodsMcCardReplacementFeeStatementDescription
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsMcMiscellaneousFeeChargeId]:
      convertedObject[
        MethodFieldParameterEnum
          .ManageIncomeOptionMethodsMcMiscellaneousFeeChargeId
      ],
    [MethodFieldParameterEnum.ManageIncomeOptionMethodsMcFirstYearMaximumFeeManagement]:
      manageIncomeOptionMethodsMcFirstYearMaximumFeeManagementOptions.find(
        o =>
          o.code ===
          convertedObject[
            MethodFieldParameterEnum
              .ManageIncomeOptionMethodsMcFirstYearMaximumFeeManagement
          ]
      )?.text
  } as Record<MethodFieldParameterEnum, any>;

  return (
    <SubGridPreviewMethod metadata={data} methodType={original.methodType} />
  );
};

export default SubRowGrid;
