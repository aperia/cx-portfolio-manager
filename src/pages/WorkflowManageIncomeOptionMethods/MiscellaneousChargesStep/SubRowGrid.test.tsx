import { renderComponent } from 'app/utils';
import React from 'react';
import SubRowGrid from './SubRowGrid';

const t = value => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

jest.mock('./SubGridPreviewMethod', () => ({
  __esModule: true,
  default: () => <div>SubGridPreviewMethod</div>
}));

describe('pages > WorkflowManageIncomeOptionMethods > MiscellaneousChargesStep > SubRowGrid', () => {
  it('render', async () => {
    const original = [
      { newValue: '0', name: 'joining.fee.amount' },
      { newValue: '0', name: 'joining.fee.statement.description' },
      { newValue: '0', name: 'joining.fee.batch.id' },
      { newValue: '0', name: 'card.replacement.fee.amount' },
      { newValue: '0', name: 'card.replacement.fee.batch.id' },
      { newValue: '0', name: 'card.replacement.fee.statement.description' },
      { newValue: '0', name: 'miscellaneous.fee.charge.id' },
      { newValue: '0', name: 'first.year.maximum.fee.management' }
    ];

    const metadata = {
      manageIncomeOptionMethodsMcJoiningFeeStatementDescriptionOptions: [
        { code: 'code', text: 'text' }
      ],
      manageIncomeOptionMethodsMcJoiningFeeBatchIdOptions: [
        { code: 'code', text: 'text' }
      ],
      manageIncomeOptionMethodsMcCardReplacementFeeBatchIdOptions: [
        { code: 'code', text: 'text' }
      ],
      manageIncomeOptionMethodsMcCardReplacementFeeStatementDescriptionOptions:
        [{ code: 'code', text: 'text' }],
      manageIncomeOptionMethodsMcFirstYearMaximumFeeManagementOptions: [
        { code: 'code', text: 'text' }
      ]
    };
    const wrapper = await renderComponent(
      'testId',
      <SubRowGrid metadata={metadata} original={original} />
    );

    expect(wrapper.getByText('SubGridPreviewMethod')).toBeInTheDocument();
  });
});
