import {
  MethodFieldNameEnum,
  MethodFieldParameterEnum
} from 'app/constants/enums';

export const ONLINE_PCF = {
  ManageIncomeOptionMethodsMcJoiningFeeAmount: 'Joining Fee',
  ManageIncomeOptionMethodsMcJoiningFeeStatementDescription:
    'Joining Fee Description',
  ManageIncomeOptionMethodsMcJoiningFeeBatchId:
    'Joining Fee Batch Identification',
  ManageIncomeOptionMethodsMcCardReplacementFeeAmount: 'Card Replacement Fees',
  ManageIncomeOptionMethodsMcCardReplacementFeeBatchId:
    'Card Replacement Batch Identification',
  ManageIncomeOptionMethodsMcCardReplacementFeeStatementDescription:
    'Card Replacement Description',
  ManageIncomeOptionMethodsMcMiscellaneousFeeChargeId:
    'Miscellaneous Custom Fee Charge ID',
  ManageIncomeOptionMethodsMcFirstYearMaximumFeeManagement:
    'MC 1st Year Max Fee Mgmt'
};

export const MORE_INFO = {
  ManageIncomeOptionMethodsMcJoiningFeeAmount:
    'The Joining Fee Amount parameter identifies the amount you want to assess as a joining fee.',
  ManageIncomeOptionMethodsMcJoiningFeeStatementDescription:
    'The Joining Fee Statement Description parameter identifies, on the cardholder statement, the type of fee that is being charged.',
  ManageIncomeOptionMethodsMcJoiningFeeBatchId:
    'The Joining Fee Batch ID parameter identifies the alpha code you want the System to use to identify your joining fees. Joining fees post as sales in batch type 05 adjustments. Set the first position of the alpha code to Q to identify a special charge. You can set the second position to fit your needs.',
  ManageIncomeOptionMethodsMcCardReplacementFeeAmount:
    'The Card Replacement Fee Amount parameter determines the amount a cardholder is charged when a replacement plastic is issued.',
  ManageIncomeOptionMethodsMcCardReplacementFeeBatchId:
    'The Card Replacement Fee Batch ID parameter identifies the alpha code you designate to identify a plastic replacement fee. Card replacement fees post as sales in batch type 05 adjustments.',
  ManageIncomeOptionMethodsMcCardReplacementFeeStatementDescription:
    'The Card Replacement Description parameter identifies the text identification of the message that prints on the cardholder statement when a plastic replacement fee is assessed.',
  ManageIncomeOptionMethodsMcMiscellaneousFeeChargeId:
    'The Miscellaneous Fee Charge ID parameter determines whether a custom fee transaction should be treated as a miscellaneous charge.',
  ManageIncomeOptionMethodsMcFirstYearMaximumFeeManagement:
    'The MC 1st Year Max Fee Mgmt parameter determines whether joining fees, card replacement fees, and custom fees should be included in first year maximum fee management.'
};

export const parameterMiscellaneousChargesGroup = [
  {
    id: MethodFieldParameterEnum.ManageIncomeOptionMethodsMcJoiningFeeAmount,
    fieldName: MethodFieldNameEnum.ManageIncomeOptionMethodsMcJoiningFeeAmount,
    onlinePCF: ONLINE_PCF.ManageIncomeOptionMethodsMcJoiningFeeAmount,
    moreInfoText: MORE_INFO.ManageIncomeOptionMethodsMcJoiningFeeAmount,
    moreInfo: MORE_INFO.ManageIncomeOptionMethodsMcJoiningFeeAmount
  },
  {
    id: MethodFieldParameterEnum.ManageIncomeOptionMethodsMcJoiningFeeStatementDescription,
    fieldName:
      MethodFieldNameEnum.ManageIncomeOptionMethodsMcJoiningFeeStatementDescription,
    onlinePCF:
      ONLINE_PCF.ManageIncomeOptionMethodsMcJoiningFeeStatementDescription,
    moreInfoText:
      MORE_INFO.ManageIncomeOptionMethodsMcJoiningFeeStatementDescription,
    moreInfo:
      MORE_INFO.ManageIncomeOptionMethodsMcJoiningFeeStatementDescription
  },
  {
    id: MethodFieldParameterEnum.ManageIncomeOptionMethodsMcJoiningFeeBatchId,
    fieldName: MethodFieldNameEnum.ManageIncomeOptionMethodsMcJoiningFeeBatchId,
    onlinePCF: ONLINE_PCF.ManageIncomeOptionMethodsMcJoiningFeeBatchId,
    moreInfoText: MORE_INFO.ManageIncomeOptionMethodsMcJoiningFeeBatchId,
    moreInfo: MORE_INFO.ManageIncomeOptionMethodsMcJoiningFeeBatchId
  },
  {
    id: MethodFieldParameterEnum.ManageIncomeOptionMethodsMcCardReplacementFeeAmount,
    fieldName:
      MethodFieldNameEnum.ManageIncomeOptionMethodsMcCardReplacementFeeAmount,
    onlinePCF: ONLINE_PCF.ManageIncomeOptionMethodsMcCardReplacementFeeAmount,
    moreInfoText: MORE_INFO.ManageIncomeOptionMethodsMcCardReplacementFeeAmount,
    moreInfo: MORE_INFO.ManageIncomeOptionMethodsMcCardReplacementFeeAmount
  },
  {
    id: MethodFieldParameterEnum.ManageIncomeOptionMethodsMcCardReplacementFeeBatchId,
    fieldName:
      MethodFieldNameEnum.ManageIncomeOptionMethodsMcCardReplacementFeeBatchId,
    onlinePCF: ONLINE_PCF.ManageIncomeOptionMethodsMcCardReplacementFeeBatchId,
    moreInfoText:
      MORE_INFO.ManageIncomeOptionMethodsMcCardReplacementFeeBatchId,
    moreInfo: MORE_INFO.ManageIncomeOptionMethodsMcCardReplacementFeeBatchId
  },
  {
    id: MethodFieldParameterEnum.ManageIncomeOptionMethodsMcCardReplacementFeeStatementDescription,
    fieldName:
      MethodFieldNameEnum.ManageIncomeOptionMethodsMcCardReplacementFeeStatementDescription,
    onlinePCF:
      ONLINE_PCF.ManageIncomeOptionMethodsMcCardReplacementFeeStatementDescription,
    moreInfoText:
      MORE_INFO.ManageIncomeOptionMethodsMcCardReplacementFeeStatementDescription,
    moreInfo:
      MORE_INFO.ManageIncomeOptionMethodsMcCardReplacementFeeStatementDescription
  },
  {
    id: MethodFieldParameterEnum.ManageIncomeOptionMethodsMcMiscellaneousFeeChargeId,
    fieldName:
      MethodFieldNameEnum.ManageIncomeOptionMethodsMcMiscellaneousFeeChargeId,
    onlinePCF: ONLINE_PCF.ManageIncomeOptionMethodsMcMiscellaneousFeeChargeId,
    moreInfoText: MORE_INFO.ManageIncomeOptionMethodsMcMiscellaneousFeeChargeId,
    moreInfo: MORE_INFO.ManageIncomeOptionMethodsMcMiscellaneousFeeChargeId
  },
  {
    id: MethodFieldParameterEnum.ManageIncomeOptionMethodsMcFirstYearMaximumFeeManagement,
    fieldName:
      MethodFieldNameEnum.ManageIncomeOptionMethodsMcFirstYearMaximumFeeManagement,
    onlinePCF:
      ONLINE_PCF.ManageIncomeOptionMethodsMcFirstYearMaximumFeeManagement,
    moreInfoText:
      MORE_INFO.ManageIncomeOptionMethodsMcFirstYearMaximumFeeManagement,
    moreInfo: MORE_INFO.ManageIncomeOptionMethodsMcFirstYearMaximumFeeManagement
  }
];

export const META_PARAMETER_MISCELLANEOUS_CHARGES = [
  MethodFieldParameterEnum.ManageIncomeOptionMethodsMcJoiningFeeAmount,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsMcJoiningFeeStatementDescription,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsMcJoiningFeeBatchId,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsMcCardReplacementFeeAmount,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsMcCardReplacementFeeBatchId,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsMcCardReplacementFeeStatementDescription,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsMcMiscellaneousFeeChargeId,
  MethodFieldParameterEnum.ManageIncomeOptionMethodsMcFirstYearMaximumFeeManagement
];
