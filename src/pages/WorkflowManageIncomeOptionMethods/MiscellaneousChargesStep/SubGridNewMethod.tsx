import EnhanceDropdownList from 'app/components/EnhanceDropdownList';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import { matchSearchValue, stringValidate } from 'app/helpers';
import {
  ColumnType,
  ComboBox,
  Grid,
  NumericTextBox,
  TextBox,
  useTranslation
} from 'app/_libraries/_dls';
import {} from 'app/_libraries/_dls/lodash';
import {
  formatTruncate,
  viewMoreInfo
} from 'pages/_commons/Utils/formatGridField';
import React, { useMemo } from 'react';
import { parameterMiscellaneousChargesGroup } from './constant';
import { MiscellaneousChargesMeta } from './types';

interface IProps {
  initialMethod: string;
  searchValue: string;
  method: WorkflowSetupMethodObjectParams;
  minDate?: Date;
  maxDate?: Date;
  metadata: MiscellaneousChargesMeta;
  onChange: (
    field: keyof WorkflowSetupMethodObjectParams,
    isRefData?: boolean
  ) => (e: any) => void;
}
const SubGridNewMethod: React.FC<IProps> = ({
  searchValue,
  method,
  metadata,
  onChange,
  initialMethod
}) => {
  const { t } = useTranslation();

  const {
    manageIncomeOptionMethodsMcJoiningFeeStatementDescriptionOptions,
    manageIncomeOptionMethodsMcJoiningFeeBatchIdOptions,
    manageIncomeOptionMethodsMcCardReplacementFeeBatchIdOptions,
    manageIncomeOptionMethodsMcCardReplacementFeeStatementDescriptionOptions,
    manageIncomeOptionMethodsMcFirstYearMaximumFeeManagementOptions
  } = metadata;
  const dataColumn =
    initialMethod === 'NEWVERSION'
      ? parameterMiscellaneousChargesGroup
      : parameterMiscellaneousChargesGroup;

  const data = useMemo(
    () =>
      dataColumn.filter(p =>
        matchSearchValue(
          [p.fieldName, p.moreInfoText, p.onlinePCF],
          searchValue
        )
      ),
    [dataColumn, searchValue]
  );

  const renderDropdown = (
    data: RefData[],
    field: keyof WorkflowSetupMethodObjectParams
  ) => {
    const value = data.find(o => o.code === method?.[field]);

    return (
      <EnhanceDropdownList
        placeholder={t('txt_workflow_manage_change_in_terms_select_an_option')}
        dataTestId={`addNewMethod__${field}`}
        size="small"
        value={value}
        onChange={onChange(field, true)}
        options={data}
      />
    );
  };

  const renderCombobox = (
    data: RefData[],
    field: keyof WorkflowSetupMethodObjectParams
  ) => {
    const value = data.find(o => o.code === method?.[field]);

    return (
      <ComboBox
        placeholder={t('txt_workflow_manage_change_in_terms_select_an_option')}
        size="small"
        textField="text"
        value={value}
        onChange={onChange(field, true)}
        noResult={t('txt_no_results_found_without_dot')}
      >
        {data.map(item => (
          <ComboBox.Item key={item.code} value={item} label={item.text} />
        ))}
      </ComboBox>
    );
  };

  const valueAccessor = (data: Record<string, any>) => {
    const paramId = data.id as MethodFieldParameterEnum;

    switch (paramId) {
      case MethodFieldParameterEnum.ManageIncomeOptionMethodsMcJoiningFeeAmount: {
        return (
          <NumericTextBox
            dataTestId="addNewMethod__manageIncomeOptionMethodsMcJoiningFeeAmount"
            size="sm"
            format="c2"
            showStep={false}
            maxLength={13}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum
                  .ManageIncomeOptionMethodsMcJoiningFeeAmount
              ] || ''
            }
            onChange={onChange(
              MethodFieldParameterEnum.ManageIncomeOptionMethodsMcJoiningFeeAmount
            )}
          />
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsMcJoiningFeeStatementDescription: {
        return renderCombobox(
          manageIncomeOptionMethodsMcJoiningFeeStatementDescriptionOptions,
          MethodFieldParameterEnum.ManageIncomeOptionMethodsMcJoiningFeeStatementDescription
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsMcJoiningFeeBatchId: {
        return renderCombobox(
          manageIncomeOptionMethodsMcJoiningFeeBatchIdOptions,
          MethodFieldParameterEnum.ManageIncomeOptionMethodsMcJoiningFeeBatchId
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsMcCardReplacementFeeAmount: {
        return (
          <NumericTextBox
            dataTestId="addNewMethod__manageIncomeOptionMethodsMcCardReplacementFeeAmount"
            size="sm"
            format="c2"
            showStep={false}
            maxLength={13}
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum
                  .ManageIncomeOptionMethodsMcCardReplacementFeeAmount
              ] || ''
            }
            onChange={onChange(
              MethodFieldParameterEnum.ManageIncomeOptionMethodsMcCardReplacementFeeAmount
            )}
          />
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsMcCardReplacementFeeBatchId: {
        return renderCombobox(
          manageIncomeOptionMethodsMcCardReplacementFeeBatchIdOptions,
          MethodFieldParameterEnum.ManageIncomeOptionMethodsMcCardReplacementFeeBatchId
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsMcCardReplacementFeeStatementDescription: {
        return renderCombobox(
          manageIncomeOptionMethodsMcCardReplacementFeeStatementDescriptionOptions,
          MethodFieldParameterEnum.ManageIncomeOptionMethodsMcCardReplacementFeeStatementDescription
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsMcMiscellaneousFeeChargeId: {
        return (
          <TextBox
            placeholder={t('txt_enter_a_value')}
            dataTestId="addNewMethod__manageIncomeOptionMethodsMcMiscellaneousFeeChargeId"
            size="sm"
            autoFocus={false}
            value={
              method?.[
                MethodFieldParameterEnum
                  .ManageIncomeOptionMethodsMcMiscellaneousFeeChargeId
              ] || ''
            }
            onChange={e => {
              const value = e.target.value;
              if (!stringValidate(value).isAlphanumeric()) {
                return false;
              }

              onChange &&
                onChange(
                  MethodFieldParameterEnum.ManageIncomeOptionMethodsMcMiscellaneousFeeChargeId
                )(e);
            }}
          />
        );
      }

      case MethodFieldParameterEnum.ManageIncomeOptionMethodsMcFirstYearMaximumFeeManagement: {
        return renderDropdown(
          manageIncomeOptionMethodsMcFirstYearMaximumFeeManagementOptions,
          MethodFieldParameterEnum.ManageIncomeOptionMethodsMcFirstYearMaximumFeeManagement
        );
      }
    }
  };

  const columns: ColumnType[] = [
    {
      id: 'fieldName',
      Header: t('txt_business_name'),
      accessor: formatTruncate(['fieldName']),
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'onlinePCF',
      Header: t('txt_manage_penalty_fee_online_PCF'),
      accessor: formatTruncate(['onlinePCF']),
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'value',
      Header: t('txt_value'),
      cellBodyProps: { className: 'py-8' },
      accessor: valueAccessor
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      accessor: viewMoreInfo(['moreInfo'], t),
      width: 105,
      cellBodyProps: { className: 'pt-12 pb-8' }
    }
  ];

  return (
    <div className="p-20">
      <Grid columns={columns} data={data} />
    </div>
  );
};

export default SubGridNewMethod;
