export interface MiscellaneousChargesMeta {
  manageIncomeOptionMethodsMcJoiningFeeStatementDescriptionOptions: RefData[];
  manageIncomeOptionMethodsMcJoiningFeeBatchIdOptions: RefData[];
  manageIncomeOptionMethodsMcCardReplacementFeeBatchIdOptions: RefData[];
  manageIncomeOptionMethodsMcCardReplacementFeeStatementDescriptionOptions: RefData[];
  manageIncomeOptionMethodsMcFirstYearMaximumFeeManagementOptions: RefData[];
}
