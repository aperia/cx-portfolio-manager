import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import 'app/utils/_mockComponent/mockCanvas';
import React from 'react';
import SubGridNewMethod from './SubGridNewMethod';
const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

const metadata = {
  manageIncomeOptionMethodsMcJoiningFeeStatementDescriptionOptions: [
    { code: 'code', text: 'text' }
  ],
  manageIncomeOptionMethodsMcJoiningFeeBatchIdOptions: [
    { code: 'code', text: 'text' }
  ],
  manageIncomeOptionMethodsMcCardReplacementFeeBatchIdOptions: [
    { code: 'code', text: 'text' }
  ],
  manageIncomeOptionMethodsMcCardReplacementFeeStatementDescriptionOptions: [
    { code: 'code', text: 'text' }
  ],
  manageIncomeOptionMethodsMcFirstYearMaximumFeeManagementOptions: [
    { code: 'code', text: 'text' }
  ],
  ManageIncomeOptionMethodsMcMiscellaneousFeeChargeId: [
    { code: 'code', text: 'text' }
  ]
};
const mockOnChange =
  (field: keyof WorkflowSetupMethodObjectParams, isRefData?: boolean) =>
  (e: any) =>
    jest.fn();
const _props = {
  metadata,
  onChange: mockOnChange,
  searchValue: ''
};

describe('pages > WorkflowManageIncomeOptionMethods > IncomeOptionMethodsStep > SubGridNewMethod', () => {
  it('Info_Basic ', () => {
    const props = { ..._props, original: { id: 'Info_Settings' } };
    jest.useFakeTimers();
    const wrapper = render(
      <SubGridNewMethod initialMethod={'NEWVERSION'} {...props} />
    );
    jest.runAllTimers();
    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();

    const citPriorityOrderInput = wrapper.getByTestId(
      'addNewMethod__manageIncomeOptionMethodsMcMiscellaneousFeeChargeId_dls-text-box_input'
    ) as HTMLInputElement;
    userEvent.type(citPriorityOrderInput, '12');

    userEvent.clear(citPriorityOrderInput);
    userEvent.type(citPriorityOrderInput, '%');
    //
  });
  it('Info_StatementDisplay  ', () => {
    const props = { ..._props, original: { id: 'Info_StatementDisplay' } };
    const wrapper = render(
      <SubGridNewMethod initialMethod={'NEWVERSION'} {...props} />
    );
    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
  });
  it('Info_MethodsToProtect  ', () => {
    const props = { ..._props, original: { id: 'Info_MethodsToProtect' } };
    const wrapper = render(
      <SubGridNewMethod initialMethod={'NEWVERSION'} {...props} />
    );
    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
  });
  it('Info_PenaltyPricing  ', () => {
    const props = { ..._props, original: { id: 'Info_PenaltyPricing' } };
    const wrapper = render(
      <SubGridNewMethod initialMethod={'NEWVERSION'} {...props} />
    );
    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
  });
  it('Info_MiscellaneousParameters  ', () => {
    const props = {
      ..._props,
      original: { id: 'Info_MiscellaneousParameters' }
    };
    const wrapper = render(
      <SubGridNewMethod initialMethod={'NEWVERSION'} {...props} />
    );
    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
  });
});
