import ModalRegistry from 'app/components/ModalRegistry';
import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import TextAreaCountDown from 'app/components/TextAreaCountDown';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import {
  METHOD_AC_FIELDS,
  METHOD_INCOME_DEFAULT,
  METHOD_MC_FIELDS
} from 'app/constants/mapping';
import {
  unsavedChangesProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import {
  mapGridExpandCollapse,
  mappingArrayKey,
  matchSearchValue,
  methodParamsToObject,
  objectToMethodParams
} from 'app/helpers';
import {
  useFormValidations,
  useUnsavedChangeRegistry,
  useUnsavedChangesRedirect
} from 'app/hooks';
import {
  Button,
  Grid,
  InlineMessage,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  TextBox,
  TransDLS,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty } from 'lodash';
import isEqual from 'lodash.isequal';
import isFunction from 'lodash.isfunction';
import { useSelectElementMetadataForManageIncomeOptionMethods } from 'pages/_commons/redux/WorkflowSetup';
import ClearAndResetButton from 'pages/_commons/Utils/ClearAndResetButton';
import React, { useEffect, useMemo, useRef, useState } from 'react';
import {
  parameterIncomeOptionMethodsGroup,
  parameterIncomeOptionMethodsList
} from '../AnnualChargesStep/constant';
import SubGridNewMethodAnnualCharges from '../AnnualChargesStep/SubGridNewMethod';
import { parameterMiscellaneousChargesGroup } from '../MiscellaneousChargesStep/constant';
import SubGridNewMethodMiscellaneousCharges from '../MiscellaneousChargesStep/SubGridNewMethod';
import newMethodColumns from './newMethodColumns';
import { ParameterListIds } from './types';

interface IProps {
  id: string;
  show: boolean;
  isAnnualChargesStep?: boolean;

  isCreate?: boolean;
  method?: WorkflowSetupMethod;
  draftMethod?: WorkflowSetupMethod;
  methodNames?: string[];
  minDate?: Date;
  maxDate?: Date;
  onClose?: (method?: WorkflowSetupMethod, isBack?: boolean) => void;
}

const AddNewMethodModal: React.FC<IProps> = ({
  id,
  show,
  isAnnualChargesStep,
  isCreate = true,
  method: methodProp = { methodType: 'NEWMETHOD' } as WorkflowSetupMethod,
  draftMethod,
  methodNames = [],
  onClose,
  minDate,
  maxDate
}) => {
  const metadata = useSelectElementMetadataForManageIncomeOptionMethods();

  const { t } = useTranslation();
  const redirect = useUnsavedChangesRedirect();

  const isChangeFieldName = useRef(false);

  const [initialMethod, setInitialMethod] = useState<
    WorkflowSetupMethodObjectParams & { rowId?: number }
  >(methodParamsToObject(methodProp));
  const [method, setMethod] = useState(
    draftMethod ? methodParamsToObject(draftMethod) : initialMethod
  );
  const [searchValue, setSearchValue] = useState('');
  const [expandedList, setExpandedList] = useState<ParameterListIds[]>([
    'Info_StatementAndNotificationDisplay'
  ]);

  const [isDuplicateName, setIsDuplicateName] = useState(false);

  const isNewVersion = useMemo(
    () => method.methodType === 'NEWVERSION',
    [method.methodType]
  );

  const isNewMethod = useMemo(
    () => method.methodType === 'NEWMETHOD',
    [method.methodType]
  );

  const parameters = useMemo(
    () =>
      isAnnualChargesStep
        ? parameterIncomeOptionMethodsList.filter(p =>
            parameterIncomeOptionMethodsGroup[p.id].some(g =>
              matchSearchValue(
                [g.fieldName, g.moreInfoText, g.onlinePCF],
                searchValue
              )
            )
          )
        : parameterMiscellaneousChargesGroup.filter(g =>
            matchSearchValue(
              [g.fieldName, g.moreInfoText, g.onlinePCF],
              searchValue
            )
          ),
    [searchValue, isAnnualChargesStep]
  );

  const errorDuplicateName = useMemo(
    () => ({
      status: true,
      message: t('txt_manage_penalty_fee_validation_method_name_must_be_unique')
    }),
    [t]
  );

  useEffect(() => {
    searchValue &&
      parameters &&
      setExpandedList(mappingArrayKey(parameters, 'id'));
  }, [searchValue, parameters]);

  useUnsavedChangeRegistry(
    {
      ...unsavedChangesProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__INCOME_OPTION_METHODS__METHOD_DETAILS,
      priority: 1
    },
    [
      !isEqual(initialMethod.name?.trim() || '', method.name?.trim() || ''),
      !isEqual(
        initialMethod.comment?.trim() || '',
        method.comment?.trim() || ''
      ),
      !isEqual(initialMethod, method)
    ]
  );

  const currentErrors = useMemo(
    () =>
      ({
        name: !method?.name?.trim() && {
          status: true,
          message: t('txt_required_validation', {
            fieldName: t('txt_method_name')
          })
        }
      } as Record<keyof WorkflowSetupMethodObjectParams, IFormError>),
    [method?.name, t]
  );
  const [touches, errors, setTouched] =
    useFormValidations<keyof WorkflowSetupMethodObjectParams>(currentErrors);

  const isValid = useMemo(
    () =>
      !errors.name?.status &&
      !isDuplicateName &&
      (!currentErrors.name?.status || touches.name?.touched),
    [
      errors.name?.status,
      isDuplicateName,
      currentErrors.name?.status,
      touches.name?.touched
    ]
  );

  useEffect(() => {
    let defaultParamObj: Record<string, string> = {};
    if (
      initialMethod.methodType === 'NEWMETHOD' &&
      initialMethod.rowId === undefined
    ) {
      const paramMapping = Object.keys(METHOD_INCOME_DEFAULT).reduce(
        (pre, next) => {
          pre[next] = METHOD_INCOME_DEFAULT[next];
          return pre;
        },
        {} as Record<string, string>
      );

      defaultParamObj = Object.assign({}, paramMapping);

      Object.assign(defaultParamObj, {
        [MethodFieldParameterEnum.ManageIncomeOptionMethodsMcJoiningFeeStatementDescription]:
          metadata.manageIncomeOptionMethodsMcJoiningFeeStatementDescriptionOptions.find(
            o => o.selected?.toString().toLowerCase() === 'true'
          )?.code || ''
      });

      defaultParamObj.methodType = 'NEWMETHOD';
      defaultParamObj.comment = '';

      setMethod(defaultParamObj as any);
      setInitialMethod(defaultParamObj as any);
    }
    setMethod(method => ({ ...method, ['comment']: method.comment || '' }));
  }, [
    initialMethod.methodType,
    initialMethod.rowId,
    metadata.manageIncomeOptionMethodsMcJoiningFeeStatementDescriptionOptions
  ]);

  const simpleSearchRef = useRef<any>(null);
  useEffect(() => {
    if (!searchValue && simpleSearchRef?.current) {
      simpleSearchRef.current.clear();
    }
  }, [searchValue]);

  const handleBack = () => {
    const methodForm = objectToMethodParams(
      isAnnualChargesStep ? METHOD_AC_FIELDS : METHOD_MC_FIELDS,
      method
    );
    methodForm.versionParameters = !!methodForm.versionParameters
      ? methodForm.versionParameters
      : methodForm.parameters;

    isFunction(onClose) && onClose(methodForm, true);
  };

  const handleClose = () => {
    redirect({
      onConfirm: () => isFunction(onClose) && onClose(),
      formsWatcher: [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__INCOME_OPTION_METHODS__METHOD_DETAILS
      ]
    });
  };

  const handleAddMethod = () => {
    const isEdit = !isCreate;
    const isChangeMethodName = initialMethod.name !== method.name;
    const isCheckName =
      (isNewVersion && isCreate) || isCreate || (isEdit && isChangeMethodName);

    const isExistNameInCurrentMethods = methodNames.includes(
      method?.name?.trim()
    );
    const isChooseMethod = !isNewMethod && !isNewVersion;

    const isDuplicateName =
      (isCheckName && isExistNameInCurrentMethods) ||
      (isChooseMethod && method?.name?.trim() === method.modeledFrom?.name);

    setIsDuplicateName(true);
    if (isDuplicateName) return;

    const methodForm = objectToMethodParams(
      isAnnualChargesStep ? METHOD_AC_FIELDS : METHOD_MC_FIELDS,
      method
    );
    methodForm.versionParameters = !!methodForm.versionParameters
      ? methodForm.versionParameters
      : methodForm.parameters;

    isFunction(onClose) && onClose(methodForm);
  };

  const handleFormChange =
    (
      field: keyof WorkflowSetupMethodObjectParams & { optionalTiers?: string },
      isRefData?: boolean
    ) =>
    (e: any) => {
      let value = isRefData ? e.target?.value?.code : e.target?.value;

      if (field === 'name') {
        // flag to check is change for Event onBlur
        isChangeFieldName.current = true;

        value = value?.toUpperCase();
        const isValidValue = /[^a-zA-Z1-9]/.test(value);
        if (isValidValue) return;
      }

      setMethod(method => ({ ...method, [field]: value }));
    };

  const handleBlurName = () => {
    if (isChangeFieldName.current) {
      setIsDuplicateName(false);
    }
    isChangeFieldName.current = false;

    setTouched('name', true)();
  };

  const handleSearch = (val: string) => {
    setSearchValue(val);
  };

  const handleClearFilter = () => {
    setSearchValue('');
    setExpandedList(['Info_StatementAndNotificationDisplay']);
  };

  return (
    <ModalRegistry lg id={id} show={show} onAutoClosedAll={handleClose}>
      <ModalHeader border closeButton onHide={handleClose}>
        <ModalTitle>{t('txt_manage_penalty_fee_method_details')}</ModalTitle>
      </ModalHeader>
      <ModalBody>
        <div className="color-grey">
          {isAnnualChargesStep ? (
            <>
              <p>
                <TransDLS keyTranslation="txt_manage_income_option_methods_cp_io_ac_method_details_desc_1">
                  <strong className="color-grey-d20" />
                </TransDLS>
              </p>
              {/* <p className="pt-8">
                <TransDLS keyTranslation="txt_manage_income_option_methods_cp_io_ac_method_details_desc_2">
                  <strong className="color-grey-d20" />
                </TransDLS>
              </p> */}
            </>
          ) : (
            <>
              <p>
                {t(
                  'txt_manage_income_option_methods_cp_io_mc_method_details_desc_1'
                )}
              </p>
              <p className="pt-8">
                <TransDLS keyTranslation="txt_manage_income_option_methods_cp_io_mc_method_details_desc_2">
                  <strong className="color-grey-d20" />
                </TransDLS>
              </p>
            </>
          )}
        </div>
        <div>
          {isDuplicateName && (
            <InlineMessage variant="danger" withIcon className="mb-8 mt-16">
              {t(
                'txt_manage_penalty_fee_validation_method_name_must_be_unique'
              )}
            </InlineMessage>
          )}
          <div className="row mt-16">
            {!isNewMethod && (
              <div className="col-6 col-lg-4">
                <TextBox
                  id="addNewMethod__selectedMethod"
                  readOnly
                  value={method?.modeledFrom?.name || ''}
                  maxLength={8}
                  label={t('txt_manage_penalty_fee_selected_method')}
                />
              </div>
            )}
            <div className="col-6 col-lg-4 mr-lg-4">
              <TextBox
                id="addNewMethod__methodName"
                readOnly={isNewVersion}
                value={method?.name || ''}
                onChange={handleFormChange('name')}
                required
                maxLength={8}
                label={t('txt_method_name')}
                onFocus={setTouched('name')}
                onBlur={handleBlurName}
                error={errors.name || (isDuplicateName && errorDuplicateName)}
              />
            </div>
            <div className="mt-16 col-12 col-lg-8">
              <TextAreaCountDown
                id="addNewMethod__comment"
                label={t('txt_comment_area')}
                value={method?.comment || ''}
                onChange={handleFormChange('comment')}
                rows={4}
                maxLength={240}
              />
            </div>
          </div>
          <div className="mt-24">
            <div className="d-flex align-items-center justify-content-between mb-16">
              <h5>{t('txt_parameter_list')}</h5>
              <SimpleSearch
                ref={simpleSearchRef}
                placeholder={t('txt_type_to_search')}
                onSearch={handleSearch}
                popperElement={
                  <>
                    <p className="color-grey">{t('txt_search_description')}</p>
                    <ul className="search-field-item list-unstyled">
                      <li className="mt-16">{t('txt_business_name')}</li>
                      <li className="mt-16">{t('txt_more_info')}</li>
                      <li className="mt-16">
                        {t('txt_manage_penalty_fee_online_PCF')}
                      </li>
                    </ul>
                  </>
                }
              />
            </div>
            {searchValue && !isEmpty(parameters) && (
              <div className="d-flex justify-content-end mb-16 mr-n8">
                <ClearAndResetButton
                  small
                  onClearAndReset={handleClearFilter}
                />
              </div>
            )}
            {isEmpty(parameters) && (
              <div className="d-flex flex-column justify-content-center mt-40">
                <NoDataFound
                  id="newMethod_notfound"
                  hasSearch
                  title={t('txt_no_results_found')}
                  linkTitle={t('txt_clear_and_reset')}
                  onLinkClicked={handleClearFilter}
                />
              </div>
            )}
            {!isEmpty(parameters) &&
              (!isAnnualChargesStep ? (
                <div className="m-n20">
                  <SubGridNewMethodMiscellaneousCharges
                    method={method}
                    metadata={metadata}
                    minDate={minDate}
                    maxDate={maxDate}
                    searchValue={searchValue}
                    onChange={handleFormChange}
                    initialMethod={initialMethod.methodType || 'NEWMETHOD'}
                  />
                </div>
              ) : (
                <Grid
                  columns={newMethodColumns(t)}
                  data={parameters}
                  subRow={({ original }: MagicKeyValue) => (
                    <SubGridNewMethodAnnualCharges
                      original={original}
                      method={method}
                      metadata={metadata}
                      minDate={minDate}
                      maxDate={maxDate}
                      searchValue={searchValue}
                      onChange={handleFormChange}
                      initialMethod={initialMethod.methodType || 'NEWMETHOD'}
                    />
                  )}
                  dataItemKey="id"
                  togglable
                  expandedItemKey="id"
                  expandedList={expandedList}
                  onExpand={setExpandedList as any}
                  toggleButtonConfigList={parameters.map(
                    mapGridExpandCollapse('id', t)
                  )}
                />
              ))}
          </div>
        </div>
      </ModalBody>
      <ModalFooter
        cancelButtonText={t('txt_cancel')}
        okButtonText={t('txt_save')}
        onCancel={handleClose}
        onOk={handleAddMethod}
        disabledOk={!isValid}
      >
        {!isNewMethod && (
          <Button
            className="mr-auto ml-n8"
            variant="outline-primary"
            onClick={handleBack}
          >
            {t('txt_back')}
          </Button>
        )}
      </ModalFooter>
    </ModalRegistry>
  );
};

export default AddNewMethodModal;
