import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { stepRegistry } from '../_commons/WorkflowSetup/registries';
import AnnualChargesStep from './AnnualChargesStep';
import GettingStartStep from './GettingStartStep';
import MiscellaneousChargesStep from './MiscellaneousChargesStep';

stepRegistry.registerStep(
  'GetStartedManageIncomeOptionMethods',
  GettingStartStep,
  [
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__INCOME_OPTION_METHODS__GET_STARTED__EXISTED_WORKFLOW,
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__INCOME_OPTION_METHODS__GET_STARTED__EXISTED_WORKFLOW__STUCK
  ]
);

stepRegistry.registerStep(
  'ConfigureParametersAnnualChargesStep',
  AnnualChargesStep,
  [UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__INCOME_OPTION_METHODS__CIT]
);

stepRegistry.registerStep(
  'ConfigureParametersMiscellaneousChargesStep',
  MiscellaneousChargesStep,
  [UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__INCOME_OPTION_METHODS__CIT]
);
