import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { default as stepRegistry } from '../_commons/WorkflowSetup/registries/stepRegistry';

const registerFunc = jest.spyOn(stepRegistry, 'registerStep');

describe('WorkflowManageIncomeOptionMethods > registerSteps', () => {
  it('Should register components', async () => {
    await import('./registerSteps');

    expect(registerFunc).toBeCalledWith(
      'ConfigureParametersMiscellaneousChargesStep',
      expect.anything(),
      [UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__INCOME_OPTION_METHODS__CIT]
    );
    expect(registerFunc).toBeCalledWith(
      'ConfigureParametersAnnualChargesStep',
      expect.anything(),
      [UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__INCOME_OPTION_METHODS__CIT]
    );
  });
});
