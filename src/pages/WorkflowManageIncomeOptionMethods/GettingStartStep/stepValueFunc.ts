import { FormGettingStart } from '.';

const stepValueFunc: WorkflowSetupStepValueFunc<FormGettingStart> = ({
  stepsForm: formValues
}) => {
  const { annualCharges, miscellaneousCharges } = formValues || {};

  if (annualCharges && miscellaneousCharges) {
    return 'Annual Charges (CP IO AC) and Miscellaneous Charges (CP IO MC)';
  }

  return annualCharges
    ? 'Annual Charges (CP IO AC)'
    : miscellaneousCharges
    ? 'Miscellaneous Charges (CP IO MC)'
    : '';
};

export default stepValueFunc;
