import parseFormValues from './parseFormValues';

describe('AssignPricingStrategiesWorkFlow > ConfigureParametersStep > parseFormValues', () => {
  it('Should return undefined', () => {
    const props = {
      data: {
        data: { workflowSetupData: [{ id: '2' }] as any }
      } as WorkflowSetupInstance,
      id: '1'
    };

    const response = parseFormValues(props.data, props.id);
    expect(response).toEqual(undefined);
  });

  it('Should return data', () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '1',
              data: { configureParametersProtectedBalancesAttributes: {} }
            }
          ] as any
        }
      } as any,
      id: '1'
    };

    const response = parseFormValues(props.data, props.id);

    expect(response)?.toEqual({
      configureParametersProtectedBalancesAttributes: {},
      isPass: false,
      isSelected: false
    });
  });
});
