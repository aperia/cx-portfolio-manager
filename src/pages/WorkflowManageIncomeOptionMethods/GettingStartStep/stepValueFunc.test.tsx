import { FormGettingStart } from '.';
import stepValueFunc from './stepValueFunc';

describe('ManagePenaltyFeeWorkflow > GettingStartStep > stepValueFunc', () => {
  it('Should Return Check Charges and Late Charges', () => {
    const stepsForm = {
      miscellaneousCharges: true,
      annualCharges: true
    } as FormGettingStart;

    const response = stepValueFunc({ stepsForm });
    expect(response).toEqual(
      'Annual Charges (CP IO AC) and Miscellaneous Charges (CP IO MC)'
    );
  });

  it('Should Return Check Charges', () => {
    const stepsForm = {
      annualCharges: true
    } as FormGettingStart;

    const response = stepValueFunc({ stepsForm });
    expect(response).toEqual('Annual Charges (CP IO AC)');
  });

  it('Should Return Late Charges', () => {
    const stepsForm = {
      miscellaneousCharges: true
    } as FormGettingStart;

    const response = stepValueFunc({ stepsForm });
    expect(response).toEqual('Miscellaneous Charges (CP IO MC)');
  });

  it('Should Return empty', () => {
    const stepsForm: any = undefined;

    const response = stepValueFunc({ stepsForm });
    expect(response).toEqual('');
  });
});
