import { getWorkflowSetupStepStatus } from 'app/helpers';
import { FormGettingStart } from '.';

const parseFormValues: WorkflowSetupStepFormDataFunc<FormGettingStart> = (
  data,
  id
) => {
  const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);
  if (!stepInfo) return;

  return {
    ...getWorkflowSetupStepStatus(stepInfo),
    ...stepInfo.data
  };
};

export default parseFormValues;
