import { WORKFLOW_SETUP } from 'app/constants/local-storage';
import {
  unsavedExistedWorkflowSetupDataOnStuckProps,
  unsavedExistedWorkflowSetupDataProps,
  unsavedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { useUnsavedChangeRegistry } from 'app/hooks';
import {
  Button,
  CheckBox,
  Icon,
  InlineMessage,
  TransDLS,
  useTranslation
} from 'app/_libraries/_dls';
import ContentExpand from 'pages/_commons/ContentExpand';
import OverviewModal from 'pages/_commons/OverviewModal';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import GettingStartSummary from './GettingStartSummary';
import parseFormValues from './parseFormValues';
import stepValueFunc from './stepValueFunc';

export interface FormGettingStart {
  isValid?: boolean;

  annualCharges?: boolean;
  miscellaneousCharges?: boolean;

  alreadyShown?: boolean;
}

export interface GettingStartProps
  extends WorkflowSetupProps<FormGettingStart> {
  acFormId?: string;
  mcFormId?: string;
}

const GettingStartStep: React.FC<GettingStartProps> = ({
  title,
  stepId,
  selectedStep,
  savedAt,
  formValues,
  hasInstance,
  isStuck,
  acFormId,
  mcFormId,
  setFormValues,
  clearFormValues
}) => {
  const { t } = useTranslation();
  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;

  const [initialValues, setInitialValues] = useState(formValues);
  const [showOverview, setShowOverview] = useState(
    formValues.alreadyShown
      ? false
      : localStorage.getItem(WORKFLOW_SETUP.SHOW_OVERVIEW_AGAIN) !== 'false'
  );

  useEffect(() => {
    const isValid =
      !!formValues.annualCharges || !!formValues.miscellaneousCharges;
    if (formValues.isValid === isValid) return;

    keepRef.current.setFormValues({ isValid });
  }, [formValues]);

  useEffect(() => {
    setInitialValues(formValues);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [savedAt]);

  useUnsavedChangeRegistry(
    {
      ...unsavedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_PENALTY_FEES__GET_STARTED,
      priority: 1
    },
    [
      !hasInstance &&
        (!!formValues.miscellaneousCharges || !!formValues.annualCharges)
    ]
  );

  const handleConfirmChangeDependencies = useCallback(() => {
    acFormId && !formValues.annualCharges && clearFormValues(acFormId);

    mcFormId && !formValues.miscellaneousCharges && clearFormValues(mcFormId);
  }, [
    clearFormValues,
    formValues.annualCharges,
    formValues.miscellaneousCharges,
    acFormId,
    mcFormId
  ]);

  const isUncheckAll = useMemo(
    () => !formValues.miscellaneousCharges && !formValues.annualCharges,
    [formValues.miscellaneousCharges, formValues.annualCharges]
  );
  const hasChanged = useMemo(
    () =>
      initialValues.miscellaneousCharges !== formValues.miscellaneousCharges ||
      initialValues.annualCharges !== formValues.annualCharges,
    [initialValues, formValues]
  );

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__INCOME_OPTION_METHODS__GET_STARTED__EXISTED_WORKFLOW,
      priority: 1,
      confirmFirst: true
    },
    [!isUncheckAll && !!hasInstance && hasChanged],
    handleConfirmChangeDependencies
  );

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataOnStuckProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__INCOME_OPTION_METHODS__GET_STARTED__EXISTED_WORKFLOW__STUCK,
      priority: 1,
      confirmFirst: true
    },
    [isUncheckAll && !!hasInstance && hasChanged]
  );

  const handleChangeOption = (option: keyof FormGettingStart) => {
    const onChange = (e: any) => {
      setFormValues({ [option]: !formValues[option] });
    };

    return onChange;
  };

  return (
    <React.Fragment>
      <ContentExpand
        isSelectedStep={stepId === selectedStep}
        change={savedAt}
        instruction={
          <div className="pt-24 px-24">
            <div className="d-flex align-items-center justify-content-between">
              <h4>{title}</h4>
              <Button
                className="mr-n8"
                variant="outline-primary"
                size="sm"
                onClick={() => setShowOverview(true)}
              >
                {t('txt_view_overview')}
              </Button>
            </div>
            <div className="pb-24">
              {isStuck && (
                <InlineMessage className="mb-0 mt-24" variant="danger" withIcon>
                  {t('txt_step_stuck_move_forward_message')}
                </InlineMessage>
              )}

              <div className="row">
                <div className="col-6 mt-24">
                  <div className="bg-light-l20 rounded-lg p-16">
                    <div className="text-center">
                      <Icon
                        name="request"
                        size="12x"
                        className="color-grey-l16"
                      />
                    </div>
                    <p className="mt-8 color-grey">
                      {t('txt_manage_income_option_methods_get_started_desc_1')}
                    </p>
                    <p className="mt-16 fw-600">
                      {t(
                        'txt_manage_income_option_methods_get_started_desc_title_1'
                      )}
                    </p>
                    <p className="mt-8 fw-600">
                      {t(
                        'txt_manage_income_option_methods_get_started_desc_title_2'
                      )}
                    </p>
                    <p className="mt-8 color-grey">
                      {t('txt_manage_income_option_methods_get_started_desc_2')}
                    </p>
                    <p className="mt-8 color-grey">
                      {t('txt_manage_income_option_methods_get_started_desc_3')}
                    </p>
                    <p className="mt-16 fw-600">
                      {t(
                        'txt_manage_income_option_methods_get_started_desc_title_3'
                      )}
                    </p>
                    <p className="mt-8 color-grey">
                      {t('txt_manage_income_option_methods_get_started_desc_4')}
                    </p>
                    <p className="mt-8 color-grey">
                      {t('txt_manage_income_option_methods_get_started_desc_5')}
                    </p>
                    <p className="mt-8 color-grey">
                      {t('txt_manage_income_option_methods_get_started_desc_6')}
                    </p>
                    <p className="mt-8 color-grey">
                      {t('txt_manage_income_option_methods_get_started_desc_7')}
                    </p>
                    <p className="mt-8 color-grey">
                      {t('txt_manage_income_option_methods_get_started_desc_8')}
                    </p>
                    <p className="mt-8 color-grey">
                      {t('txt_manage_income_option_methods_get_started_desc_9')}
                    </p>
                  </div>
                </div>
                <div className="col-6 mt-24 color-grey">
                  <p>
                    {t(
                      'txt_manage_income_option_methods_get_started_steps_guides_title'
                    )}
                  </p>
                  <p className="mt-8">
                    <TransDLS keyTranslation="txt_manage_income_option_methods_get_started_steps_guides_1">
                      <strong className="color-grey-d20" />
                    </TransDLS>
                  </p>
                  <p className="mt-8">
                    <TransDLS keyTranslation="txt_manage_income_option_methods_get_started_steps_guides_2">
                      <strong className="color-grey-d20" />
                    </TransDLS>
                  </p>
                  <p className="mt-8">
                    <TransDLS keyTranslation="txt_manage_income_option_methods_get_started_steps_guides_3">
                      <strong className="color-grey-d20" />
                    </TransDLS>
                  </p>
                  <p className="mt-8">
                    <TransDLS keyTranslation="txt_manage_income_option_methods_get_started_steps_guides_4">
                      <strong className="color-grey-d20" />
                    </TransDLS>
                  </p>
                  <p className="mt-8">
                    <TransDLS keyTranslation="txt_manage_income_option_methods_get_started_steps_guides_desc">
                      <strong className="color-grey-d20" />
                    </TransDLS>
                  </p>
                </div>
              </div>
            </div>
          </div>
        }
      >
        <div className="p-24">
          <h5>
            <TransDLS keyTranslation="txt_manage_income_option_methods_get_started_select_the_fee">
              <strong className="color-red" />
            </TransDLS>
          </h5>
          <div className="row">
            <div className="col-6 col-xxl-4 mt-16">
              <div className="list-cards list-cards--selectable list-cards--single">
                <label
                  className="list-cards__item custom-control-root"
                  htmlFor="annualCharges"
                >
                  <span className="d-flex align-items-center">
                    <span className="pr-8">
                      {t('txt_manage_income_option_methods_annual_charges')}
                    </span>
                  </span>
                  <CheckBox className="mr-n4">
                    <CheckBox.Input
                      id="annualCharges"
                      onChange={handleChangeOption('annualCharges')}
                      checked={formValues.annualCharges}
                    />
                  </CheckBox>
                </label>
              </div>
            </div>
            <div className="col-6 col-xxl-4 mt-16">
              <div className="list-cards list-cards--selectable list-cards--single">
                <label
                  className="list-cards__item custom-control-root"
                  htmlFor="miscellaneousCharges"
                >
                  <span className="d-flex align-items-center">
                    <span className="pr-8">
                      {t(
                        'txt_manage_income_option_methods_miscellaneous_charges'
                      )}
                    </span>
                  </span>
                  <CheckBox className="mr-n4">
                    <CheckBox.Input
                      id="miscellaneousCharges"
                      onChange={handleChangeOption('miscellaneousCharges')}
                      checked={formValues.miscellaneousCharges}
                    />
                  </CheckBox>
                </label>
              </div>
            </div>
          </div>
        </div>
      </ContentExpand>

      {showOverview && (
        <OverviewModal
          id="overviewWorkflowSetup"
          show
          onClose={() => {
            setShowOverview(false);
            keepRef.current.setFormValues({ alreadyShown: true });
          }}
        />
      )}
    </React.Fragment>
  );
};

const ExtraStaticGettingStartStep =
  GettingStartStep as WorkflowSetupStaticProp<FormGettingStart>;

ExtraStaticGettingStartStep.summaryComponent = GettingStartSummary;
ExtraStaticGettingStartStep.stepValue = stepValueFunc;
ExtraStaticGettingStartStep.defaultValues = {
  isValid: false,
  miscellaneousCharges: false,
  annualCharges: false
};
ExtraStaticGettingStartStep.parseFormValues = parseFormValues;

export default ExtraStaticGettingStartStep;
