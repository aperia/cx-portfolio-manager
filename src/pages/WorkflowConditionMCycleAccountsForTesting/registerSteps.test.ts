import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { default as stepRegistry } from '../_commons/WorkflowSetup/registries/stepRegistry';

const registerFunc = jest.spyOn(stepRegistry, 'registerStep');

describe('WorkflowFlexibleNonMonetaryTransactions > registerSteps', () => {
  it('Should register components', async () => {
    await import('./registerSteps');

    expect(registerFunc).toBeCalledWith(
      'GetStartedConditionMCycleAccountsForTestingWorkflow',
      expect.anything()
    );

    expect(registerFunc).toBeCalledWith(
      'SelectTransactionsConditionMCycleAccountsForTestingWorkflow',
      expect.anything(),
      [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CONDITION_M_CYCLE_ACCOUNTS_FOR_TESTING
      ]
    );
  });
});
