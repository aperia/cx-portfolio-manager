import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { stepRegistry } from '../_commons/WorkflowSetup/registries';
import GettingStartStep from './GettingStartStep';
import SelectTransactionsStep from './SelectTransactionsStep';

stepRegistry.registerStep(
  'GetStartedConditionMCycleAccountsForTestingWorkflow',
  GettingStartStep
);

stepRegistry.registerStep(
  'SelectTransactionsConditionMCycleAccountsForTestingWorkflow',
  SelectTransactionsStep,
  [UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CONDITION_M_CYCLE_ACCOUNTS_FOR_TESTING]
);
