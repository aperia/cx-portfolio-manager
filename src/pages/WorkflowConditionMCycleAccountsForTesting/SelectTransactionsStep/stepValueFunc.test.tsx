import { render } from '@testing-library/react';
import { Form } from '.';
import stepValueFunc from './stepValueFunc';

const t = (value: string) => {
  return value;
};

describe('WorkflowConditionMCycleAccountsForTesting > SelectTransactionsStep > stepValueFunc', () => {
  it('Should Return with XXX Transaction Selected', () => {
    const stepsForm = {
      transactionsData: [
        { name: 'a1', id: 1 },
        { name: 'a2', id: 2 },
        { name: 'a3', id: 3 },
        { name: 'a4', id: 4 },
        { name: 'a5', id: 5 }
      ]
    } as Form;

    const response: any = stepValueFunc({ stepsForm, t });
    const wrapper = render(response);
    expect(wrapper.queryByText(/txt_number_transactions_selected/))
      .toBeInTheDocument;
  });

  it('Should Return with content', () => {
    const stepsForm = {
      transactionsData: [
        { name: 'a1', id: 1 },
        { name: 'a2', id: 2 },
        { name: 'a3', id: 3 }
      ]
    } as Form;

    const response: any = stepValueFunc({ stepsForm, t });
    const wrapper = render(response);
    expect(wrapper.queryByText(/a1,a2,a3/)).toBeInTheDocument;
  });

  it('Should Return with empty string', () => {
    const stepsForm = {} as Form;

    const response = stepValueFunc({ stepsForm, t });
    expect(response).toEqual('');
  });

  it('should return changeName with all transaction selected', () => {
    const stepsForm = {
      transactionsData: [
        { name: 'a1', id: 1 },
        { name: 'a2', id: 2 },
        { name: 'a3', id: 3 }
      ],
      listLength: 3
    };

    const result = stepValueFunc({ stepsForm, t });
    expect(result).toBeTruthy();
  });
});
