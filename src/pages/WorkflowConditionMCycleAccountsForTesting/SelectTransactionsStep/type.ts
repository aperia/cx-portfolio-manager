export enum ParameterCode {
  TransactionId = 'transaction.id',

  Transaction253 = '253.transaction',
  Transaction254 = '254.transaction',
  Transaction255 = '255.transaction',
  Transaction271 = '271.transaction',

  TransactionCU956 = 'CU.956.transaction',
  TransactionNM102 = 'NM.102.transaction',
  TransactionNM146 = 'NM.146.transaction',
  TransactionNM150 = 'NM.150.transaction'
}
