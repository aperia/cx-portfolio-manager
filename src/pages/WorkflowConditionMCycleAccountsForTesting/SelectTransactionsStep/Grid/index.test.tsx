import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { renderWithMockStore } from 'app/utils';
import 'app/utils/_mockComponent/mockNoDataFound';
import { STATUS } from 'app/_libraries/_dls/components/Upload/helper';
import { queryAllByClass } from 'app/_libraries/_dls/test-utils';
import * as selector from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowConditionMCycleAccountsForTesting';
import React from 'react';
import { act } from 'react-dom/test-utils';
import Grid from '.';

const setFormValues = jest.fn();
const clearFormValues = jest.fn();

const get1MockData = (i: number | string) => ({
  id: `${i}`,
  code: `${i}`,
  name: `testName${i}`,
  text: `testText${i}`
});

const generateMockData = (length: number) => {
  const result = [];
  for (let i = 0; i < length; i++) {
    result.push(get1MockData(i));
  }

  return result;
};

const t = (value: string) => value;
const useTranslation = () => ({ t });
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { __esModule: true, ...actualModule, useTranslation };
});

jest.mock('app/hooks/useUnsavedChangeRegistry', () => {
  return {
    __esModule: true,
    useUnsavedChangeRegistry: (options: any, changes: any, onConfirm: any) => {
      onConfirm && onConfirm();
    }
  };
});
const mockUseSelectElementsForConditionMCycleAccountsForTesting = jest.spyOn(
  selector,
  'useSelectElementsForConditionMCycleAccountsForTesting'
);

describe('WorkflowConditionMCycleAccountsForTesting > SelectTransactionsStep > Grid', () => {
  beforeEach(() => {
    mockUseSelectElementsForConditionMCycleAccountsForTesting.mockReturnValue({
      transactions: []
    });
  });

  it('render with loading', async () => {
    const props = {
      stepId: 'id',
      selectedStep: 'id1',
      savedAt: 1,
      formValues: { isValid: true },

      dependencies: {
        files: [
          {
            status: STATUS.valid
          }
        ]
      },
      setFormValues,
      clearFormValues
    } as any;
    await renderWithMockStore(<Grid {...props} />, {});
  });

  it('render with no data', async () => {
    const props = {
      stepId: 'id',
      selectedStep: 'id',
      savedAt: 1,
      formValues: { isValid: true },

      dependencies: {},
      setFormValues
    } as any;

    jest.useFakeTimers();
    const wrapper = await renderWithMockStore(<Grid isEdit {...props} />, {});
    jest.runAllTimers();
    expect(wrapper.getByText('No data found')).toBeInTheDocument();

    act(() => {
      userEvent.click(wrapper.getByText('No data found'));
    });
  });

  describe('with data', () => {
    it('render', async () => {
      const transactions = generateMockData(11) as any;
      mockUseSelectElementsForConditionMCycleAccountsForTesting.mockReturnValue(
        { transactions }
      );

      const props = {
        stepId: 'id',
        selectedStep: 'id',
        savedAt: 1,
        formValues: { isValid: true },

        dependencies: { attachmentId: 'id' },
        setFormValues
      } as any;

      jest.useFakeTimers();
      const wrapper = await renderWithMockStore(<Grid isEdit {...props} />, {});
      jest.runAllTimers();

      const searchBox = wrapper.getByPlaceholderText(
        'txt_type_to_search_for_transaction'
      );

      userEvent.type(searchBox, 'test');
      userEvent.click(
        searchBox.parentElement!.querySelector(
          '.icon-simple-search .icon.icon-search'
        )!
      );

      const btnExpand = queryAllByClass(wrapper.container, /icon icon-plus/)[0];
      act(() => {
        userEvent.click(btnExpand);
      });

      const allCollapseBtn = queryAllByClass(
        wrapper.container,
        /icon icon-minus/
      );
      act(() => {
        allCollapseBtn.forEach(e => userEvent.click(e));
      });

      expect(
        wrapper.queryByText(
          'txt_workflow_condition_m_cycle_accounts_for_testing__number_transactions_selected'
        )
      ).not.toBeInTheDocument();
    });

    it('check all', async () => {
      const transactions = generateMockData(3) as any;
      mockUseSelectElementsForConditionMCycleAccountsForTesting.mockReturnValue(
        { transactions }
      );

      const props = {
        stepId: 'id',
        selectedStep: 'id',
        savedAt: 1,
        formValues: { isValid: true },

        dependencies: { attachmentId: 'id' },
        setFormValues
      } as any;

      jest.useFakeTimers();
      await renderWithMockStore(<Grid isEdit {...props} />, {});
      jest.runAllTimers();

      const checkboxAll = screen.getAllByRole('checkbox')[0];
      act(() => {
        userEvent.click(checkboxAll);
      });
    });

    it('check one row', async () => {
      const transactions = generateMockData(3) as any;
      mockUseSelectElementsForConditionMCycleAccountsForTesting.mockReturnValue(
        { transactions }
      );

      const props = {
        stepId: 'id',
        selectedStep: 'id',
        savedAt: 1,
        formValues: { isValid: true },

        dependencies: {},
        setFormValues
      } as any;

      jest.useFakeTimers();
      const wrapper = await renderWithMockStore(<Grid isEdit {...props} />, {});
      jest.runAllTimers();

      const checkbox = wrapper.getAllByRole('checkbox')[1];
      act(() => {
        userEvent.click(checkbox);
      });
      expect(
        wrapper.getByText(
          `txt_workflow_condition_m_cycle_accounts_for_testing__number_transactions_selected`
        )
      ).toBeInTheDocument();
    });

    it('with formValue transaction', async () => {
      const transactions = generateMockData(5) as any;
      mockUseSelectElementsForConditionMCycleAccountsForTesting.mockReturnValue(
        { transactions }
      );

      const props = {
        stepId: 'id',
        selectedStep: 'id',
        savedAt: 1,
        formValues: { isValid: true, transactions: ['1', '2'] },

        dependencies: {},
        setFormValues
      } as any;

      jest.useFakeTimers();
      const wrapper = await renderWithMockStore(<Grid isEdit {...props} />, {});
      jest.runAllTimers();

      act(() => {
        userEvent.click(wrapper.getAllByRole('checkbox')[1]);
        userEvent.click(wrapper.getAllByRole('checkbox')[2]);
        userEvent.click(wrapper.getAllByRole('checkbox')[3]);
        userEvent.click(wrapper.getAllByRole('checkbox')[4]);
      });

      expect(
        wrapper.getByText(
          'txt_workflow_condition_m_cycle_accounts_for_testing__number_transactions_selected'
        )
      ).toBeInTheDocument();
    });
  });
});
