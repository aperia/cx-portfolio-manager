import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import { PAGE_SIZE } from 'app/constants/constants';
import {
  confirmEditWhenChangeEffectToUploadProps,
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { mapGridExpandCollapse } from 'app/helpers';
import { compare2Array } from 'app/helpers/array';
import { useCheckAllPagination, useUnsavedChangeRegistry } from 'app/hooks';
import { usePagination } from 'app/hooks/usePagination';
import {
  Button,
  Grid,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import { STATUS } from 'app/_libraries/_dls/components/Upload/helper';
import { differenceWith, isEmpty } from 'app/_libraries/_dls/lodash';
import {
  actionsWorkflowSetup,
  useSelectElementsForConditionMCycleAccountsForTesting
} from 'pages/_commons/redux/WorkflowSetup';
import Paging from 'pages/_commons/Utils/Paging';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';
import { SelectNonMonetaryTransactionsStepProps } from '..';
import { ParameterCode } from '../type';
import { SubGrid } from './SubGrid';

const GridStep: React.FC<SelectNonMonetaryTransactionsStepProps> = props => {
  const {
    stepId,
    selectedStep,
    savedAt,
    formValues,
    dependencies,
    setFormValues,
    clearFormValues,
    clearFormName
  } = props;

  const setFormValuesRef = useRef(setFormValues);
  setFormValuesRef.current = setFormValues;

  const dispatch = useDispatch();
  const { t } = useTranslation();

  const { transactions } =
    useSelectElementsForConditionMCycleAccountsForTesting();

  const [expandedList, setExpandedList] = useState<string[]>([]);
  const [initialValues, setInitialValues] = useState(formValues?.transactions);

  const {
    sort,
    total,
    currentPage,
    currentPageSize,
    searchValue,
    gridData: dataView,
    dataChecked,
    isCheckAll,
    onSetDataChecked,
    onCheckInPage,
    onSetIsCheckAll,
    onSortChange,
    onPageChange,
    onPageSizeChange,
    onSearchChange,
    onResetToDefaultFilter
  } = usePagination(transactions, ['text'], 'text');

  const { gridClassName, handleClickCheckAll } = useCheckAllPagination(
    {
      gridClassName: 'condition-m-cycle-accounts-for-testing',
      allIds: transactions.map(t => t.code),
      hasFilter: !!searchValue,
      filterIds: dataView.map(f => f.code),
      checkedList: dataChecked,
      setCheckAll: onSetIsCheckAll,
      setCheckList: onSetDataChecked
    },
    [searchValue, sort, currentPageSize, currentPageSize]
  );

  useEffect(() => {
    if (selectedStep === stepId) {
      onResetToDefaultFilter();
      setExpandedList([]);
    }
  }, [selectedStep, stepId, onResetToDefaultFilter]);

  const renderSubRow = (row: any) => <SubGrid row={row} />;
  const columns = useMemo(
    () => [
      {
        id: 'text',
        Header: t('txt_transaction_column_name'),
        accessor: 'text',
        isSort: true
      }
    ],
    [t]
  );

  const handleClearAndReset = useCallback(() => {
    onPageSizeChange(PAGE_SIZE[0]);
    onPageChange(1);
    onSearchChange('');
    setExpandedList([]);
  }, [onPageChange, onPageSizeChange, onSearchChange]);

  const handleCheck = (dataKeyList: string[]) => {
    onCheckInPage(dataKeyList);
  };

  useEffect(() => {
    setFormValuesRef.current({
      transactions: dataChecked,
      transactionsData: transactions.filter(item =>
        dataChecked.includes(item.code)
      )
    });
  }, [dataChecked, transactions]);

  useEffect(() => {
    dispatch(
      actionsWorkflowSetup.getWorkflowMetadata([
        ParameterCode.TransactionId,
        ParameterCode.Transaction253,
        ParameterCode.Transaction254,
        ParameterCode.Transaction255,
        ParameterCode.Transaction271,
        ParameterCode.TransactionCU956,
        ParameterCode.TransactionNM102,
        ParameterCode.TransactionNM146,
        ParameterCode.TransactionNM150
      ])
    );
  }, [dispatch]);

  const handleExpand = (dataKeyList: string[]) => {
    setExpandedList(dataKeyList);
  };

  const hasChange = !(
    isEmpty(differenceWith(initialValues, dataChecked)) &&
    isEmpty(differenceWith(dataChecked, initialValues!))
  );
  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CONDITION_M_CYCLE_ACCOUNTS_FOR_TESTING,
      priority: 1
    },
    [hasChange]
  );

  const unsaveOptions = useMemo(
    () => ({
      ...confirmEditWhenChangeEffectToUploadProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CONDITION_M_CYCLE_ACCOUNTS_FOR_TESTING_CONFIRM_EDIT,
      priority: 1
    }),
    []
  );

  const handleResetUploadStep = () => {
    const file = dependencies?.files?.[0];
    const attachmentId = file?.idx;
    const isValid = file?.status === STATUS.valid;

    typeof clearFormValues === 'function' && clearFormValues(clearFormName);
    isValid &&
      dispatch(actionsWorkflowSetup.deleteTemplateFile({ attachmentId }));
  };
  useUnsavedChangeRegistry(
    unsaveOptions,
    [
      !isEmpty(dependencies?.files) &&
        !compare2Array(initialValues, dataChecked)
    ],
    handleResetUploadStep
  );

  const transactionsSelected = useMemo(() => {
    const listNumber = dataChecked?.length;
    const content = isCheckAll
      ? t('txt_all_transactions_selected')
      : listNumber !== 1
      ? t(
          'txt_workflow_condition_m_cycle_accounts_for_testing__number_transactions_selected',
          { listNumber }
        )
      : t(
          'txt_workflow_condition_m_cycle_accounts_for_testing__number_transaction_selected',
          { listNumber }
        );

    return (
      <TruncateText title={content} resizable>
        {content}
      </TruncateText>
    );
  }, [dataChecked, t, isCheckAll]);

  const viewNoData = useMemo(() => {
    return (
      <>
        <div className="pt-24 mt-24 border-top">
          <div className="d-flex align-items-center">
            <h5>
              {t(
                'txt_workflow_condition_m_cycle_accounts_for_testing__transaction_list'
              )}
            </h5>
            <div className="ml-auto">
              {(total > 0 || !isEmpty(searchValue)) && (
                <div className="ml-auto">
                  <SimpleSearch
                    defaultValue={searchValue}
                    onSearch={onSearchChange}
                    placeholder={t('txt_type_to_search_for_transaction')}
                  />
                </div>
              )}
            </div>
          </div>
        </div>
        {!searchValue && <div className="mt-16">{transactionsSelected}</div>}
        <div className="d-flex flex-column justify-content-center mt-40 mb-32">
          <NoDataFound
            hasSearch={!!searchValue}
            id="search-workflow-condition-m-cycle-not-found"
            title={t('txt_no_transactions_to_display')}
            linkTitle={t('txt_clear_and_reset')}
            onLinkClicked={handleClearAndReset}
          />
        </div>
      </>
    );
  }, [
    handleClearAndReset,
    onSearchChange,
    searchValue,
    t,
    total,
    transactionsSelected
  ]);

  // Bind formValue transactions to checkedList
  useEffect(() => {
    const newData = transactions.filter(item =>
      formValues.transactions?.includes(item.code)
    );
    const newCheckedList = newData.map(item => item.code);

    if (differenceWith(newCheckedList, dataChecked).length === 0) return;

    setFormValuesRef.current({ transactionsData: newData });
    onSetDataChecked(newCheckedList);
    onSetIsCheckAll(formValues.transactions?.length === transactions.length);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [formValues.transactions]);

  // update isValid
  useEffect(() => {
    const newIsValid = (formValues.transactions?.length || 0) > 0;
    if (newIsValid === formValues.isValid) return;
    setFormValuesRef.current({
      isValid: newIsValid,
      listLength: transactions.length
    });
  }, [transactions.length, formValues]);

  // set initialValues
  useEffect(() => {
    setInitialValues(formValues.transactions);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [savedAt]);

  if (dataView.length === 0) return viewNoData;

  return (
    <div className="pt-24 mt-24 border-top">
      <div className="d-flex align-items-center">
        <h5>
          {t(
            'txt_workflow_condition_m_cycle_accounts_for_testing__transaction_list'
          )}
        </h5>
        {(!isEmpty(searchValue) || total > 0) && (
          <div className="ml-auto">
            <SimpleSearch
              defaultValue={searchValue}
              onSearch={onSearchChange}
              placeholder={t('txt_type_to_search_for_transaction')}
            />
          </div>
        )}
      </div>

      <div className="d-flex align-items-center my-16">
        {!searchValue && (
          <div className="flex-1 py-4">{transactionsSelected}</div>
        )}
        {!!searchValue && (
          <div className="ml-auto mr-n8">
            <Button
              size="sm"
              variant="outline-primary"
              onClick={handleClearAndReset}
            >
              {t('txt_clear_and_reset')}
            </Button>
          </div>
        )}
      </div>

      <Grid
        togglable
        className={gridClassName}
        data={dataView}
        columns={columns}
        expandedList={expandedList}
        checkedList={formValues.transactions}
        onSortChange={onSortChange}
        sortBy={[sort]}
        toggleButtonConfigList={dataView.map(mapGridExpandCollapse('id', t))}
        dataItemKey="id"
        expandedItemKey="id"
        variant={{
          id: 'id',
          type: 'checkbox',
          cellHeaderProps: { onClick: handleClickCheckAll }
        }}
        subRow={renderSubRow}
        onExpand={handleExpand}
        onCheck={handleCheck}
      />
      {total > 10 && (
        <div className="mt-16">
          <Paging
            page={currentPage}
            pageSize={currentPageSize}
            totalItem={total}
            onChangePage={onPageChange}
            onChangePageSize={onPageSizeChange}
          />
        </div>
      )}
    </div>
  );
};

export default GridStep;
