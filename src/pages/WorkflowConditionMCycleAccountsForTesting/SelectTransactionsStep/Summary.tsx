import { usePagination } from 'app/hooks/usePagination';
import { Button, ColumnType, Grid, useTranslation } from 'app/_libraries/_dls';
import { isFunction, orderBy } from 'app/_libraries/_dls/lodash';
import Paging from 'pages/_commons/Utils/Paging';
import React, { useMemo } from 'react';
import { Form } from '.';

const Summary: React.FC<WorkflowSetupSummaryProps<Form>> = props => {
  const { formValues, onEditStep } = props;

  const { t } = useTranslation();

  const handleEdit = () => {
    isFunction(onEditStep) && onEditStep();
  };

  const columnDefault: ColumnType[] = useMemo(
    () => [
      {
        id: 'text',
        Header: t(
          'txt_workflow_condition_m_cycle_accounts_for_testing__transactions_selected'
        ),
        accessor: 'text'
      }
    ],
    [t]
  );

  const selectedData = useMemo(() => {
    return orderBy(formValues?.transactionsData || [], 'text', 'asc');
  }, [formValues?.transactionsData]);

  const {
    total,
    currentPage,
    currentPageSize,
    gridData: dataView,
    onPageSizeChange,
    onPageChange
  } = usePagination(selectedData, ['text'], 'text');

  return (
    <div className="position-relative">
      <div className="absolute-top-right mt-n26 mr-n8">
        <Button variant="outline-primary" size="sm" onClick={handleEdit}>
          {t('txt_edit')}
        </Button>
      </div>
      <div className="pt-16">
        <Grid columns={columnDefault} data={dataView} />
      </div>
      <div className="mt-16">
        <Paging
          page={currentPage}
          pageSize={currentPageSize}
          totalItem={total}
          onChangePage={onPageChange}
          onChangePageSize={onPageSizeChange}
        />
      </div>
    </div>
  );
};

export default Summary;
