import { Form } from '.';

const stepValueFunc: WorkflowSetupStepValueFunc<Form> = ({
  stepsForm: formValues,
  t
}) => {
  if (!formValues?.transactionsData) return '';
  const listNumber = formValues?.transactionsData.length;
  const listLength = formValues?.listLength;

  let content = t('txt_number_transactions_selected', { listNumber });

  if (listNumber < 4) {
    content = formValues?.transactionsData?.map(item => item.name)?.join(', ');
  }

  if (listNumber === listLength) {
    content = t('txt_all_transactions_selected');
  }

  return content;
};

export default stepValueFunc;
