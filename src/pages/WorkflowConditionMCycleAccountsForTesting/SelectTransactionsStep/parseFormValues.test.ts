import parseFormValues from './parseFormValues';

describe('WorkflowConditionMCycleAccountsForTesting > SelectTransactionsStep > parseFormValues', () => {
  const dispatchMock = jest.fn();
  const data = {
    transactions: ['1', '2']
  };
  const input: any = {
    data: {
      configurations: data,
      methodList: [],
      workflowSetupData: [
        {
          id: 'id',
          status: 'DONE'
        }
      ]
    }
  };

  it('selected step', () => {
    const id = 'id';
    const result = parseFormValues(input, id, dispatchMock);

    expect(result.transactions).toEqual(data.transactions);
  });

  it('select other step', () => {
    const id = 'id1';
    const result = parseFormValues(input, id, dispatchMock);

    expect(result).toBeUndefined();
  });

  it('empty config', () => {
    const id = 'id';
    const result = parseFormValues({} as any, id, dispatchMock);

    expect(result).toBeUndefined();
  });
});
