import { getWorkflowSetupStepStatus } from 'app/helpers';
import { isEmpty } from 'lodash';

const parseFormValues: WorkflowSetupStepFormDataFunc<any> = (data, id) => {
  const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);

  const { transactions } = (data?.data as any)?.configurations || {};
  if (isEmpty(transactions) || !stepInfo) return;

  const isValid = !isEmpty(transactions);

  return {
    ...getWorkflowSetupStepStatus(stepInfo),
    isValid,
    transactions
  };
};

export default parseFormValues;
