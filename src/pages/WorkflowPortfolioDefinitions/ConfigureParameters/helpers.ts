import { matchSearchValue } from 'app/helpers';
import { StateFile } from 'app/_libraries/_dls/components/Upload/File';
import { Options, STATUS } from 'app/_libraries/_dls/components/Upload/helper';
import get from 'lodash.get';
import isArray from 'lodash.isarray';
import isEmpty from 'lodash.isempty';

export const searchAttribute = (attributes: any, searchValue: string) => {
  return attributes.some((item: any) => {
    if (!isEmpty(item['attributes'])) {
      return searchAttribute(item['attributes'], searchValue);
    }
    const attribute = get(item, 'item', {});
    if (
      matchSearchValue(
        [
          attribute['filterBy'],
          attribute['value'],
          get(attribute, ['value', 'text'])
        ],
        searchValue
      )
    )
      return true;
  });
};

export const getStatusFileFromValidationMessages = (
  validationMessages: any[]
) => {
  let newStatus: 0 | 1 = STATUS.valid;
  /* istanbul ignore else */
  if (!isEmpty(validationMessages)) {
    validationMessages?.forEach(
      (validation: { field: string; message: string }) => {
        if (validation.field === 'Extension') {
          newStatus = STATUS.invalid;
        }
      }
    );
  }

  return newStatus;
};

export const getValid = (file: StateFile, options?: Options) => {
  const { name = '', size = 0 } = file;
  const { maxFile, typeFiles, minFile } = options || {};
  const extension = (name.split('.').pop() || '').toLocaleLowerCase();

  const isTypeSupport = Boolean(
    typeFiles && isArray(typeFiles)
      ? typeFiles.map(type => type.toLocaleLowerCase()).includes(extension)
      : true
  );

  const isOverSize = Boolean(maxFile ? maxFile < size : false);
  const isUnderSize = Boolean(minFile ? minFile > size : false);

  return {
    isInvalid: Boolean(
      !isTypeSupport ||
        isOverSize ||
        isUnderSize ||
        file.status === STATUS.invalid
    ),
    isTypeSupport,
    isOverSize,
    isUnderSize
  };
};

export const checkIsValidAttribute = (attributes: any) => {
  return attributes.some((attributes: any) => {
    if (get(attributes, ['item', 'filterBy'])) {
      return true;
    }
    if (get(attributes, ['attributes'])) {
      return checkIsValidAttribute(get(attributes, ['attributes']));
    }
  });
};

export const checkDisableItemFilterBy = (
  filterOptions: string[],
  filterIgnore: string[][]
) => {
  return filterIgnore.some(item => {
    return item.every(filters => filterOptions.includes(filters));
  });
};

export const findFilterValue = (attributes: any) => {
  return attributes.reduce((previousValue: any, newValue: any) => {
    const filter = get(newValue, ['item', 'filterBy']);
    if (filter && !previousValue.includes(filter)) {
      return [...previousValue, filter];
    }
    if (get(newValue, ['attributes'])) {
      return [
        ...previousValue,
        ...findFilterValue(get(newValue, ['attributes']))
      ];
    }
    return [...previousValue];
  }, []);
};

export const countTotalAttribute = (attributes: any) => {
  return attributes.reduce((previousValue: any, newValue: any) => {
    if (get(newValue, ['attributes'])) {
      return previousValue + countTotalAttribute(get(newValue, ['attributes']));
    }
    return previousValue + 1;
  }, 0);
};

export const upperFirstCharacter = (text: string) =>
  text.replace(/^\w/, (letter: string) => letter.toUpperCase());

export const convertBase64ToFile = (base64: string, name: string) => {
  const arr = base64.split(',');
  if (arr.length < 2) return null;
  const mimeArr = arr[0].match(/:(.*?);/);
  if (!mimeArr || mimeArr.length < 2) return null;
  const mime = mimeArr[1];
  const buff = Buffer.from(arr[1], 'base64');
  return new File([buff], name, {
    type: mime
  });
};
