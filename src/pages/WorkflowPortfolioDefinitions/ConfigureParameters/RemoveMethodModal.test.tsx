import { fireEvent } from '@testing-library/react';
import { renderWithMockStore } from 'app/utils';
import React from 'react';
import RemoveMethodModal from './RemoveMethodModal';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderComponent = (props: any) => {
  return renderWithMockStore(<RemoveMethodModal {...props} />, {});
};

describe('RemoveMethodModal', () => {
  it('render component', async () => {
    const wrapper = await renderComponent({
      id: 'test',
      methodName: 'new',
      show: true,
      onClose: jest.fn()
    });

    expect(wrapper.getByText('txt_confirm_deletion')).toBeInTheDocument();

    //onRemove
    fireEvent.click(wrapper.getByText('txt_delete'));

    //handleCloseWithoutReload
    fireEvent.click(wrapper.getByText('txt_cancel'));
  });
});
