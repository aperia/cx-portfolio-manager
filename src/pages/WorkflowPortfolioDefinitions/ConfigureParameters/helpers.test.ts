import {
  checkDisableItemFilterBy,
  checkIsValidAttribute,
  convertBase64ToFile,
  findFilterValue,
  getStatusFileFromValidationMessages,
  getValid,
  searchAttribute
} from './helpers';

describe('test helper', () => {
  it('searchAttribute', () => {
    searchAttribute(
      [{ id: '1', attributes: [{ id: '1' }], filterBy: ['1'], value: '1' }],
      '123'
    );
  });

  it('searchAttribute', () => {
    searchAttribute(
      [{ id: '1', attributes: [], filterBy: ['1'], value: '1' }],
      '1'
    );
  });

  it('getStatusFileFromValidationMessages', () => {
    getStatusFileFromValidationMessages([
      { field: '123', message: '123' },
      { field: 'Extension', message: '123' }
    ]);
  });

  it('findFilterValue', () => {
    findFilterValue([
      { id: '1', attributes: [{ id: '1' }], filterBy: ['1'], value: '1' }
    ]);
  });

  it('findFilterValue', () => {
    findFilterValue([{ id: '1', attributes: [{ id: '1' }], value: '1' }]);
  });

  it('checkDisableItemFilterBy', () => {
    checkDisableItemFilterBy(['123', '124'], [['123', '12']]);
  });

  it('checkIsValidAttribute', () => {
    checkIsValidAttribute([{ id: '1', attributes: [{ id: '1' }], value: '1' }]);
  });

  it('checkIsValidAttribute', () => {
    checkIsValidAttribute([
      { id: '1', attributes: [{ id: '1' }], filterBy: ['1'], value: '1' }
    ]);
  });

  it('getValid', () => {
    getValid(
      {
        idx: '1',
        percentage: 1,
        status: 1,
        txtName: '1',
        txtDescription: 'des',
        name: '1',
        size: 2,
        lastModified: '1'
      },
      {
        typeFiles: ['jpg'],
        maxFile: 1
      }
    );
  });

  it('getValid', () => {
    getValid(
      {
        idx: '1',
        percentage: 1,
        status: 1,
        txtName: '1',
        txtDescription: 'des',
        name: '1',
        size: 1,
        lastModified: '1'
      },
      {
        typeFiles: ['jpg'],
        minFile: 2
      }
    );
  });

  it('getValid', () => {
    getValid({});
  });

  it('convertBase64ToFile', () => {
    convertBase64ToFile(':123;:123;123,123,123:123', 'acb');
  });

  it('convertBase64ToFile', () => {
    convertBase64ToFile('123;123;123,123,123:123', 'acb');
  });

  it('convertBase64ToFile', () => {
    convertBase64ToFile('123;123', 'acb');
  });
});
