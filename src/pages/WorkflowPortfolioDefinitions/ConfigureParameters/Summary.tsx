import { MethodFieldParameterEnum } from 'app/constants/enums';
import { mapGridExpandCollapse } from 'app/helpers';
import { usePaginationGrid } from 'app/hooks';
import {
  Button,
  ColumnType,
  Grid,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty, isFunction, orderBy } from 'app/_libraries/_dls/lodash';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import { formatText } from 'pages/_commons/Utils/formatGridField';
import Paging from 'pages/_commons/Utils/Paging';
import React, { useEffect, useMemo, useState } from 'react';
import { ICON_DEFAULT_PATH } from './constant';
import SubRowGrid from './SubRowGrid';

const Summary: React.FC<WorkflowSetupSummaryProps<any>> = ({
  formValues,
  onEditStep,
  stepId,
  selectedStep
}) => {
  const { t } = useTranslation();
  const [expandedList, setExpandedList] = useState<string[]>([]);

  const { width } = useSelectWindowDimension();
  const methods = useMemo(
    () => orderBy(formValues?.methods || [], ['name'], ['asc']),
    [formValues?.methods]
  );

  useEffect(() => {
    if (stepId !== selectedStep) return;

    setExpandedList([]);
  }, [stepId, selectedStep]);

  const handleEdit = () => {
    isFunction(onEditStep) && onEditStep();
  };

  const {
    total,
    currentPage,
    currentPageSize,
    pageData,
    onPageChange,
    onPageSizeChange
  } = usePaginationGrid(methods);

  const handleOnExpand = (dataKeyList: string[]) => {
    setExpandedList(isEmpty(dataKeyList) ? [] : dataKeyList);
  };

  const columns: ColumnType[] = useMemo(
    () => [
      {
        id: 'methodName',
        Header: t('txt_portfolio_definitions_name_label'),
        accessor: formatText(['name']),
        width: width < 1280 ? 250 : undefined
      },
      {
        id: 'icon',
        Header: t('txt_portfolio_definitions_icon_label'),
        width: width < 1280 ? 180 : undefined,
        accessor: data => {
          const file = data[MethodFieldParameterEnum.PortfolioDefinitionsIcon];
          if (!file) {
            return (
              <Tooltip
                placement="top"
                element={t('txt_portfolio_definitions_default_icon')}
              >
                <div className="box-portfolio-icon">
                  <img
                    className="rounded-sm"
                    src={ICON_DEFAULT_PATH}
                    alt="icon-default"
                  />
                </div>
              </Tooltip>
            );
          }
          return (
            <div className="box-portfolio-icon">
              <img
                className="rounded-sm"
                height={24}
                width={48}
                src={URL.createObjectURL(file)}
              />
            </div>
          );
        }
      }
    ],
    [t, width]
  );

  const renderGrid = useMemo(() => {
    if (isEmpty(pageData)) return;
    return (
      <div className="pt-16">
        <Grid
          togglable
          columns={columns}
          data={pageData}
          subRow={({ original }: MagicKeyValue) => (
            <SubRowGrid original={original} />
          )}
          dataItemKey="rowId"
          toggleButtonConfigList={pageData.map(
            mapGridExpandCollapse('rowId', t)
          )}
          expandedItemKey={'rowId'}
          expandedList={expandedList}
          onExpand={handleOnExpand}
          scrollable
          className="grid-has-control grid-has-tooltip center"
        />
        {total > 10 && (
          <Paging
            totalItem={total}
            pageSize={currentPageSize}
            page={currentPage}
            onChangePage={onPageChange}
            onChangePageSize={onPageSizeChange}
          />
        )}
      </div>
    );
  }, [
    columns,
    expandedList,
    t,
    total,
    currentPageSize,
    currentPage,
    pageData,
    onPageChange,
    onPageSizeChange
  ]);

  return (
    <div className="position-relative">
      <div className="absolute-top-right mt-n26 mr-n8">
        <Button variant="outline-primary" size="sm" onClick={handleEdit}>
          {t('txt_edit')}
        </Button>
      </div>
      <>{renderGrid}</>
    </div>
  );
};

export default Summary;
