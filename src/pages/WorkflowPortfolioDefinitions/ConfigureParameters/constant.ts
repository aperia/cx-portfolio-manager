import { MethodFieldParameterEnum } from 'app/constants/enums';

export const ATTRIBUTE_VALUE_FIELDS: MagicKeyValue = {
  Sys: {
    metadataKey: 'sys',
    moreInfo: {
      title: 'CHD-SYSTEM-NO',
      description: 'Select a system identifier.'
    },
    typeControl: 'combobox',
    props: {
      required: true
    }
  },
  Prin: {
    metadataKey: 'prin',
    moreInfo: {
      title: 'CHD-PRIN-BANK',
      description: 'Select a principal identifier.'
    },
    typeControl: 'combobox',
    props: {
      required: true
    }
  },
  Agent: {
    metadataKey: 'agent',
    moreInfo: {
      title: 'CHD-AGENT-BANK',
      description: 'Select an agent identifier.'
    },
    typeControl: 'combobox',
    props: {
      required: true
    }
  },
  BIN: {
    metadataKey: MethodFieldParameterEnum.PortfolioDefinitionsAttributeBin,
    moreInfo: {
      title: 'CHD-CLIENT-NUMBER',
      description: 'Select a bank’s four-digit client number.'
    },
    typeControl: 'combobox',
    props: {
      required: true
    }
  },
  ['Portfolio ID']: {
    metadataKey:
      MethodFieldParameterEnum.PortfolioDefinitionsAttributePortfolioId,
    moreInfo: {
      title: 'CHD-ACS-CURR-PORT',
      description:
        'Select a value indicating a bank defined like group of accounts for use in Adaptive Control System processing. The current Adaptive Control portfolio assigned to the cardholder account. This can be assigned via the Portfolio ID parameter in the Behavior Score Adaptive Control section (RF AM BS) of the PCF or Non Mon Transaction 174.'
    },
    typeControl: 'format-textbox',
    props: {
      required: true,
      pattern: '^\\d{0,4}$',
      maxLength: 4
    }
  },
  ['State Code']: {
    metadataKey:
      MethodFieldParameterEnum.PortfolioDefinitionsAttributeStateCode,
    moreInfo: {
      title: 'CHD-STATE',
      description:
        'Select an abbreviation for the name of a specific state and certain foreign countries.'
    },
    typeControl: 'combobox',
    props: {
      required: true
    }
  },
  ['Pricing Strategy']: {
    metadataKey: 'pricing.stratergies',
    moreInfo: {
      title: 'CHD-CURR-PRICING-STRATEGY',
      description:
        'Select an issuer-defined code identifying the pricing strategy under which the system processes the account.'
    },
    typeControl: 'combobox',
    props: {
      required: true
    }
  },
  ['Product Type Code']: {
    props: {
      min: 0,
      maxLength: 3,
      format: 'n',
      required: true,
      showStep: false
    },
    moreInfo: {
      title: 'CHD-UD-PRODUCT-TYPE',
      description:
        'Enter an user-defined field on the Cardholder Master File that is updated via the NM CC screen or by using unformatted NM*203.'
    },
    typeControl: 'numeric'
  },
  ['Company ID']: {
    props: {
      maxLength: 8
    },
    moreInfo: {
      title: 'CHD-CMPN-ID',
      description:
        'Select an issuer-defined company an account belongs to. All commercial card accounts must have a value in this field.'
    },
    typeControl: 'textbox'
  },
  ['Company Name']: {
    props: {
      maxLength: 26
    },
    moreInfo: {
      title: 'XXXX-NAME',
      description:
        'Enter a name of the commercial organization for this company card account.'
    },
    typeControl: 'textbox'
  },
  ['Datalink Elements']: {
    props: {
      maxLength: 8
    },
    moreInfo: {
      title: '',
      description: 'Select a name describing the element.'
    },
    typeControl: 'textbox'
  },
  ['Miscellaneous Fields 1-10-TX']: {
    props: {
      maxLength: 10
    },
    moreInfo: {
      title: 'IF4-MISC-13-PSTN-1-10-TX',
      description:
        'Select a client defined text. Characters 1 through 10 of miscellaneous thirteenth text.'
    },
    typeControl: 'textbox'
  },
  ['Miscellaneous Fields 11-15-TX']: {
    props: {
      maxLength: 5
    },
    typeControl: 'textbox',
    moreInfo: {
      title: 'IF4-MISC-13-PSTN-11-15-TX',
      description:
        'Select a client defined text. Characters 11 through 15 of miscellaneous thirteenth text.'
    }
  },
  ['Miscellaneous Fields 16-20-TX']: {
    props: {
      maxLength: 5
    },
    typeControl: 'textbox',
    moreInfo: {
      title: 'IF4-MISC-13-PSTN-16-20-TX',
      description:
        'Select a client defined text. Characters 16 through 20 of miscellaneous thirteenth text.'
    }
  },
  ['Miscellaneous Fields 21-25-TX']: {
    props: {
      maxLength: 5
    },
    typeControl: 'textbox',
    moreInfo: {
      title: 'IF4-MISC-13-PSTN-21-25-TX',
      description:
        'Select a client defined text. Characters 21 through 25 of miscellaneous thirteenth text.'
    }
  },
  ['Miscellaneous Fields 26-TX']: {
    props: {
      maxLength: 1
    },
    typeControl: 'textbox',
    moreInfo: {
      title: 'IF4-MISC-13-PSTN-26-TX',
      description:
        'Select a client defined text. Character 26 of miscellaneous thirteenth text.'
    }
  },
  ['Miscellaneous Fields 27-TX']: {
    props: {
      maxLength: 1
    },
    moreInfo: {
      title: 'IF4-MISC-13-PSTN-27-TX',
      description:
        'Select a client defined text. Character 27 of miscellaneous thirteenth text.'
    },
    typeControl: 'textbox'
  },
  ['Miscellaneous Fields 28-TX']: {
    props: {
      maxLength: 1
    },
    moreInfo: {
      title: 'IF4-MISC-13-PSTN-28-TX',
      description:
        'Select a client defined text. Character 28 of miscellaneous thirteenth text.'
    },
    typeControl: 'textbox'
  },
  ['Miscellaneous Fields 29-TX']: {
    props: {
      maxLength: 1
    },
    moreInfo: {
      title: 'IF4-MISC-13-PSTN-29-TX',
      description:
        'Select a client defined text. Character 29 of miscellaneous thirteenth text.'
    },
    typeControl: 'textbox'
  },
  ['Miscellaneous Fields 30-TX']: {
    props: {
      maxLength: 1
    },
    moreInfo: {
      title: 'IF4-MISC-13-PSTN-30-TX',
      description:
        'Select a client defined text. Character 30 of miscellaneous thirteenth text.'
    },
    typeControl: 'textbox'
  },
  ['Miscellaneous Fields 31-TX']: {
    props: {
      maxLength: 1
    },
    moreInfo: {
      title: 'IF4-MISC-13-PSTN-31-TX',
      description:
        'Select a client defined text. Character 31 of miscellaneous thirteenth text.'
    },
    typeControl: 'textbox'
  },
  ['Miscellaneous Fields 32-TX']: {
    props: {
      maxLength: 1
    },
    moreInfo: {
      title: 'IF4-MISC-13-PSTN-32-TX',
      description:
        'Select a client defined text. Character 32 of miscellaneous thirteenth text.'
    },
    typeControl: 'textbox'
  },
  ['Miscellaneous Fields 33-TX']: {
    props: {
      maxLength: 1
    },
    moreInfo: {
      title: 'IF4-MISC-13-PSTN-33-TX',
      description:
        'Select a client defined text. Character 33 of miscellaneous thirteenth text.'
    },
    typeControl: 'textbox'
  },
  ['Miscellaneous Fields 34-TX']: {
    props: {
      maxLength: 1
    },
    moreInfo: {
      title: 'IF4-MISC-13-PSTN-34-TX',
      description:
        'Select a client defined text. Character 34 of miscellaneous thirteenth text.'
    },
    typeControl: 'textbox'
  },
  ['Miscellaneous Fields 35-TX']: {
    props: {
      maxLength: 1
    },
    moreInfo: {
      title: 'IF4-MISC-13-PSTN-35-TX',
      description:
        'Select a client defined text. Character 35 of miscellaneous thirteenth text.'
    },
    typeControl: 'textbox'
  },
  ['Miscellaneous Fields 36-TX']: {
    props: {
      maxLength: 1
    },
    moreInfo: {
      title: 'IF4-MISC-13-PSTN-36-TX',
      description:
        'Select a client defined text. Character 36 of miscellaneous thirteenth text.'
    },
    typeControl: 'textbox'
  },
  ['Miscellaneous Fields 37-TX']: {
    props: {
      maxLength: 1
    },
    moreInfo: {
      title: 'IF4-MISC-13-PSTN-37-TX',
      description:
        'Select a client defined text. Character 37 of miscellaneous thirteenth text.'
    },
    typeControl: 'textbox'
  },
  ['Miscellaneous Fields 38-TX']: {
    props: {
      maxLength: 1
    },
    moreInfo: {
      title: 'IF4-MISC-13-PSTN-38-TX',
      description:
        'Select a client defined text. Character 38 of miscellaneous thirteenth text.'
    },
    typeControl: 'textbox'
  },
  ['Miscellaneous Fields 39-TX']: {
    props: {
      maxLength: 1
    },
    moreInfo: {
      title: 'IF4-MISC-13-PSTN-39-TX',
      description:
        'Select a client defined text. Character 39 of miscellaneous thirteenth text.'
    },
    typeControl: 'textbox'
  },
  ['Miscellaneous Fields 40-TX']: {
    props: {
      maxLength: 1
    },
    moreInfo: {
      title: 'IF4-MISC-13-PSTN-40-TX',
      description:
        'Select a client defined text. Character 40 of miscellaneous thirteenth text.'
    },
    typeControl: 'textbox'
  },
  ['Miscellaneous Fields 41-TX']: {
    props: {
      maxLength: 1
    },
    moreInfo: {
      title: 'IF4-MISC-13-PSTN-41-TX',
      description:
        'Select a client defined text. Character 41 of miscellaneous thirteenth text.'
    },
    typeControl: 'textbox'
  },
  ['Miscellaneous Fields 42-TX']: {
    props: {
      maxLength: 1
    },
    moreInfo: {
      title: 'IF4-MISC-13-PSTN-42-TX',
      description:
        'Select a client defined text. Character 42 of miscellaneous thirteenth text.'
    },
    typeControl: 'textbox'
  },
  ['Miscellaneous Fields 43-TX']: {
    props: {
      maxLength: 1
    },
    moreInfo: {
      title: 'IF4-MISC-13-PSTN-43-TX',
      description:
        'Select a client defined text. Character 43 of miscellaneous thirteenth text.'
    },
    typeControl: 'textbox'
  },
  ['Miscellaneous Fields 44-TX']: {
    props: {
      maxLength: 1
    },
    moreInfo: {
      title: 'IF4-MISC-13-PSTN-44-TX',
      description:
        'Select a client defined text. Character 44 of miscellaneous thirteenth text.'
    },
    typeControl: 'textbox'
  },
  ['Miscellaneous Fields 45-TX']: {
    props: {
      maxLength: 1
    },
    moreInfo: {
      title: 'IF4-MISC-13-PSTN-45-TX',
      description:
        'Select a client defined text. Character 45 of miscellaneous thirteenth text.'
    },
    typeControl: 'textbox'
  },
  ['Miscellaneous Fields 46-TX']: {
    props: {
      maxLength: 1
    },
    moreInfo: {
      title: 'IF4-MISC-13-PSTN-46-TX',
      description:
        'Select a client defined text. Character 46 of miscellaneous thirteenth text.'
    },
    typeControl: 'textbox'
  },
  ['Miscellaneous Fields 47-TX']: {
    props: {
      maxLength: 1
    },
    moreInfo: {
      title: 'IF4-MISC-13-PSTN-47-TX',
      description:
        'Select a client defined text. Character 47 of miscellaneous thirteenth text.'
    },
    typeControl: 'textbox'
  },
  ['Miscellaneous Fields 48-TX']: {
    props: {
      maxLength: 1
    },
    moreInfo: {
      title: 'IF4-MISC-13-PSTN-48-TX',
      description:
        'Select a client defined text. Character 48 of miscellaneous thirteenth text.'
    },
    typeControl: 'textbox'
  },
  ['Miscellaneous Fields 49-TX']: {
    props: {
      maxLength: 1
    },
    moreInfo: {
      title: 'IF4-MISC-13-PSTN-49-TX',
      description:
        'Select a client defined text. Character 49 of miscellaneous thirteenth text.'
    },
    typeControl: 'textbox'
  },
  ['Miscellaneous Fields 50-TX']: {
    props: {
      maxLength: 1
    },
    moreInfo: {
      title: 'IF4-MISC-13-PSTN-50-TX',
      description:
        'Select a client defined text. Character 50 of miscellaneous thirteenth text.'
    },
    typeControl: 'textbox'
  }
};

export const FILTER_SPA_OPTIONS: string[] = ['Sys', 'Prin', 'Agent'];

export const FILE_TYPES: string[] = ['jpg', 'jpeg', 'png'];
export const MAX_SIZE = 128 * 1024;
export const PORTFOLIO_NAME_MAX_LENGTH = 50;
export const MAX_FILTER_OPTIONS_DEFAULT = 8;
export const FILE_TYPES_INPUT = 'image/jpg, image/jpeg, image/png';
export const DEFAULT_FIRST_ATTRIBUTE = Object.freeze({
  operator: 'where',
  type: 'attribute',
  item: {
    filterBy: '',
    operator: '',
    value: ''
  }
});

export const DEFAULT_ATTRIBUTE = Object.freeze({
  operator: 'and',
  type: 'attribute',
  item: {
    filterBy: '',
    operator: '',
    value: ''
  }
});

export const DEFAULT_FIRST_ATTRIBUTE_GROUP = Object.freeze({
  operator: 'where',
  type: 'group',
  attributes: [
    {
      operator: '',
      type: 'attribute',
      item: {
        filterBy: '',
        operator: '',
        value: ''
      }
    }
  ]
});

export const DEFAULT_ATTRIBUTE_GROUP = Object.freeze({
  operator: 'and',
  type: 'group',
  attributes: [
    {
      operator: '',
      type: 'attribute',
      item: {
        filterBy: '',
        operator: '',
        value: ''
      }
    }
  ]
});

export const ICON_DEFAULT_PATH = './images/icons/icon-default.png';
