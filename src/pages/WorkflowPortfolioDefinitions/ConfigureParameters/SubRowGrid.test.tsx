import { MethodFieldParameterEnum } from 'app/constants/enums';
import { renderComponent } from 'app/utils';
import * as WorkflowPortfolioDefinitions from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowPortfolioDefinitions';
import React from 'react';
import SubRowGrid from './SubRowGrid';

const MockSelectWorkflowMethodsSelector = jest.spyOn(
  WorkflowPortfolioDefinitions,
  'useSelectElementMetadataForPortfolioDefinitionsSelector'
);
const t = (value: any) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

const file = new File(['test'], 'test.txt', {
  type: 'text/plain'
});

describe('pages > Portfolio Definitions > SubRowGrid', () => {
  beforeEach(() => {
    MockSelectWorkflowMethodsSelector.mockReturnValue({
      'filter.by': [
        {
          text: 'Sys',
          code: 'Sys'
        },
        {
          text: 'Prin',
          code: 'Prin'
        }
      ],
      'sys.operators': [{ text: 'equal', code: 'equal' }]
    });
  });
  it('render', async () => {
    const methods: any = {
      id: 'id',
      name: 'MTHMTCI',
      modeledFrom: {
        id: 'id1',
        name: 'MTHMTCI',
        versions: [{ id: 'id' }]
      },
      methodType: 'NEWMETHOD',
      parameters: [],
      comment: '',
      tableControlParameters: [],
      decisionElements: [],
      [MethodFieldParameterEnum.PortfolioDefinitionsAttributes]: [
        {
          operator: 'where',
          item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
          type: 'attribute'
        },
        {
          operator: 'and',
          item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
          type: 'attribute'
        },
        {
          operator: 'and',
          attributes: [
            {
              operator: '',
              type: 'group',
              attributes: [
                {
                  item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
                  operator: '',
                  type: 'attribute'
                }
              ]
            }
          ],
          type: 'group'
        }
      ],
      [MethodFieldParameterEnum.PortfolioDefinitionsIcon]: file,
      rowId: 1,
      serviceSubjectSection: 'CP PF LC',
      versionParameters: []
    };
    const wrapper = await renderComponent(
      'testId',
      <SubRowGrid original={methods} />
    );

    expect(wrapper.queryAllByText('txt_value')).toHaveLength(3);
    expect(wrapper.queryAllByText('txt_filter_by')).toHaveLength(3);
    expect(
      wrapper.queryAllByText('txt_manage_account_memos_operator')
    ).toHaveLength(3);
  });
});
