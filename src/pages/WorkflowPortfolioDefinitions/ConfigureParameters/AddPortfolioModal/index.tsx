import ModalRegistry from 'app/components/ModalRegistry';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import {
  unsavedChangesProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { methodParamsToObject, objectToMethodParams } from 'app/helpers';
import {
  useFormValidations,
  useUnsavedChangeRegistry,
  useUnsavedChangesRedirect
} from 'app/hooks';
import {
  InlineMessage,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  TextBox,
  useTranslation
} from 'app/_libraries/_dls';
import { StateFile } from 'app/_libraries/_dls/components/Upload/File';
import { cloneDeep, flattenDeep, isEmpty, isEqual, set } from 'lodash';
import get from 'lodash.get';
import isFunction from 'lodash.isfunction';
import React, { useEffect, useMemo, useRef, useState } from 'react';
import { PORTFOLIO_NAME_MAX_LENGTH } from '../constant';
import { checkIsValidAttribute, countTotalAttribute } from '../helpers';
import AttributeSection, { AttributeProps } from './Attributes';
import { AttributeItemGroupProps } from './Attributes/AttributeGroup';
import UploadIcon from './UploadIcon';

interface IProps extends WorkflowSetupProps {
  id: string;
  show: boolean;
  isCreate?: boolean;
  method?: WorkflowSetupMethod;
  draftMethod?: WorkflowSetupMethod;
  methodNames?: string[];
  onClose?: (method?: WorkflowSetupMethod, isBack?: boolean) => void;
}

export type NestedArray<T> = Array<T> | Array<NestedArray<T>>;

const AddNewMethodModal: React.FC<IProps> = props => {
  const {
    id,
    show,
    isCreate = true,
    method: methodProp = { methodType: 'NEWMETHOD' } as WorkflowSetupMethod,
    draftMethod,
    methodNames = [],
    onClose
  } = props;

  const { t } = useTranslation();
  const redirect = useUnsavedChangesRedirect();

  const isChangeFieldName = useRef(false);

  const [initialMethod, setInitialMethod] = useState<
    WorkflowSetupMethodObjectParams & { rowId?: number }
  >(methodParamsToObject(methodProp));
  const [method, setMethod] = useState(
    draftMethod ? methodParamsToObject(draftMethod) : initialMethod
  );
  const [attributesValid, setValid] = useState<NestedArray<boolean>>([]);
  const [fileValid, setValidUpload] = useState(true);
  const [numberOfAttribute, setNumberOfAttribute] = useState<number>(
    countTotalAttribute(
      method[MethodFieldParameterEnum.PortfolioDefinitionsAttributes] || []
    )
  );

  const [isDuplicateName, setIsDuplicateName] = useState(false);

  const isNewVersion = useMemo(
    () => method.methodType === 'NEWVERSION',
    [method.methodType]
  );

  const isNewMethod = useMemo(
    () => method.methodType === 'NEWMETHOD',
    [method.methodType]
  );

  const checkIsConfirmAttributes = () => {
    const attributes = get(
      method,
      [MethodFieldParameterEnum.PortfolioDefinitionsAttributes],
      []
    ) as (AttributeProps | AttributeItemGroupProps)[];

    if (!isCreate) {
      return !isEqual(
        {
          name: initialMethod.name?.trim() || '',
          icon: get(
            initialMethod,
            [MethodFieldParameterEnum.PortfolioDefinitionsIcon],
            ''
          ),
          attributes: JSON.stringify(
            get(
              initialMethod,
              [MethodFieldParameterEnum.PortfolioDefinitionsAttributes],
              ''
            )
          )
        },
        {
          name: method.name?.trim() || '',
          icon: get(
            method,
            [MethodFieldParameterEnum.PortfolioDefinitionsIcon],
            ''
          ),
          attributes: JSON.stringify(attributes)
        }
      );
    }

    if (numberOfAttribute < 3) return false;

    return (
      checkIsValidAttribute(attributes) ||
      !isEmpty(
        get(method, [MethodFieldParameterEnum.PortfolioDefinitionsIcon], '')
      ) ||
      !isEmpty(method.name?.trim())
    );
  };

  useUnsavedChangeRegistry(
    {
      ...unsavedChangesProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__PORTFOLIO__DEFINITIONS__CONFIGURE_PARAMETERS,
      priority: 1
    },
    [checkIsConfirmAttributes()]
  );

  const currentErrors = useMemo(
    () =>
      ({
        name: !method?.name?.trim() && {
          status: true,
          message: t('txt_required_validation', {
            fieldName: t('txt_portfolio_definitions_name_label')
          })
        }
      } as Record<keyof WorkflowSetupMethodObjectParams, IFormError>),
    [method?.name, t]
  );
  const [touches, errors, setTouched] = useFormValidations(currentErrors);

  const isValid = useMemo(
    () =>
      !errors.name?.status &&
      !isDuplicateName &&
      (!currentErrors.name?.status || touches.name?.touched),
    [
      errors.name?.status,
      isDuplicateName,
      currentErrors.name?.status,
      touches.name?.touched
    ]
  );

  useEffect(() => {
    let defaultParamObj: Record<string, string> = {};
    if (
      initialMethod.methodType === 'NEWMETHOD' &&
      initialMethod.rowId === undefined
    ) {
      const paramMapping = {};
      defaultParamObj = Object.assign({}, paramMapping);
      defaultParamObj.methodType = 'NEWMETHOD';
      setMethod(defaultParamObj as any);
      setInitialMethod(defaultParamObj as any);
    }
  }, [initialMethod.methodType, initialMethod.rowId]);

  const handleClose = () => {
    redirect({
      onConfirm: () => isFunction(onClose) && onClose(),
      formsWatcher: [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__PORTFOLIO__DEFINITIONS__CONFIGURE_PARAMETERS
      ]
    });
  };

  const handleAddMethod = () => {
    const isEdit = !isCreate;
    const isChangeMethodName = initialMethod.name !== method.name;
    const isCheckName =
      (isNewVersion && isCreate) || isCreate || (isEdit && isChangeMethodName);

    const isExistNameInCurrentMethods = methodNames.includes(
      method?.name?.trim()
    );
    const isChooseMethod = !isNewMethod && !isNewVersion;

    const isDuplicateName =
      (isCheckName && isExistNameInCurrentMethods) ||
      (isChooseMethod && method?.name?.trim() === method.modeledFrom?.name);

    setIsDuplicateName(true);
    if (isDuplicateName) return;

    const methodForm = objectToMethodParams([], method);
    methodForm.versionParameters = !!methodForm.versionParameters
      ? methodForm.versionParameters
      : methodForm.parameters;

    isFunction(onClose) && onClose(methodForm);
  };

  const handleFormChange =
    (
      field: keyof WorkflowSetupMethodObjectParams & { optionalTiers?: string },
      isRefData?: boolean
    ) =>
    (e: any) => {
      let value = e;

      if (field === 'name') {
        // flag to check is change for Event onBlur
        isChangeFieldName.current = true;
        value = e.target?.value;
      }
      setMethod(method => ({ ...method, [field]: value }));
    };

  const handleBlurName = () => {
    if (isChangeFieldName.current) {
      setIsDuplicateName(false);
    }
    isChangeFieldName.current = false;

    setTouched('name', true)();
  };

  const checkAttributeValid = (path: string, isValid: boolean) => {
    setValid((valids: NestedArray<boolean>) => {
      const newValids = cloneDeep(valids);
      set(newValids, path, isValid);
      return newValids;
    });
  };

  const checkAttributeGroupValid = (levels: string[], node?: string) => {
    if (!node) {
      setValid((valids: NestedArray<boolean>) => {
        valids.splice(get(levels, '0'), 1);
        return valids;
      });
      return;
    }

    setValid((valids: NestedArray<boolean>) => {
      const cloneValids = cloneDeep(valids);
      const validParent = get(cloneValids, levels, []);
      validParent.splice(get(node, '0'), 1);
      return cloneValids;
    });
  };

  const [titleModal, confirmBtn] = useMemo(() => {
    if (isCreate) {
      return [t('txt_portfolio_definitions_add_portfolion'), t('txt_add')];
    }
    return [t('txt_portfolio_definitions_edit_portfolion'), t('txt_save')];
  }, [isCreate, t]);

  const validAttribute = flattenDeep(attributesValid);

  const isValidAttribute = useMemo(() => {
    if (isEmpty(validAttribute)) return false;
    return !validAttribute.some(isValid => isValid === false);
  }, [validAttribute]);

  return (
    <ModalRegistry lg id={id} show={show} onAutoClosedAll={handleClose}>
      <ModalHeader border closeButton onHide={handleClose}>
        <ModalTitle>{titleModal}</ModalTitle>
      </ModalHeader>
      <ModalBody>
        <h5>{t('txt_portfolio_definitions_add_title_1')}</h5>
        {isDuplicateName && (
          <InlineMessage variant="danger" withIcon className="mb-8 mt-16">
            {t('txt_portfolio_definitions_validation_name_must_be_unique')}
          </InlineMessage>
        )}
        <div className="row mt-16">
          <div className="col-6 col-lg-4">
            <TextBox
              id="addPortfolio"
              value={method?.name || ''}
              onChange={handleFormChange('name')}
              required
              maxLength={PORTFOLIO_NAME_MAX_LENGTH}
              label={t('txt_portfolio_definitions_name_label')}
              onFocus={setTouched('name')}
              onBlur={handleBlurName}
              error={errors.name}
            />
          </div>
        </div>
        <div className="mt-16">
          <p className="fw-600">{t('txt_portfolio_definitions_icon_label')}</p>
          <p className="mt-8 text-secondary">
            {t('txt_portfolio_definitions_text_upload')}
          </p>
          <UploadIcon
            onChange={handleFormChange(
              MethodFieldParameterEnum.PortfolioDefinitionsIcon
            )}
            icon={
              method[MethodFieldParameterEnum.PortfolioDefinitionsIcon] as
                | StateFile
                | undefined
            }
            setValidUpload={setValidUpload}
          />
          <AttributeSection
            onChange={handleFormChange(
              MethodFieldParameterEnum.PortfolioDefinitionsAttributes
            )}
            checkAttributeGroupValid={checkAttributeGroupValid}
            checkAttributeValid={checkAttributeValid}
            attributes={
              (method[
                MethodFieldParameterEnum.PortfolioDefinitionsAttributes
              ] || []) as (AttributeProps | AttributeItemGroupProps)[]
            }
            setNumberOfAttribute={setNumberOfAttribute}
            totalAttribute={numberOfAttribute}
            attributesValid={attributesValid}
            {...props}
          />
        </div>
      </ModalBody>
      <ModalFooter
        cancelButtonText={t('txt_cancel')}
        okButtonText={confirmBtn}
        onCancel={handleClose}
        onOk={handleAddMethod}
        disabledOk={!isValid || !isValidAttribute || !fileValid}
      ></ModalFooter>
    </ModalRegistry>
  );
};

export default AddNewMethodModal;
