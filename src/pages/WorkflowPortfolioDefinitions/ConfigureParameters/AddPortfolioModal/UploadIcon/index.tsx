import { useTranslation } from 'app/_libraries/_dls';
import Upload, { UploadRef } from 'app/_libraries/_dls/components/Upload';
import {
  Props as FileProps,
  StateFile
} from 'app/_libraries/_dls/components/Upload/File';
import { getValid, STATUS } from 'app/_libraries/_dls/components/Upload/helper';
import React, { FC, useRef } from 'react';
import { FILE_TYPES, FILE_TYPES_INPUT, MAX_SIZE } from '../../constant';
import CustomFile from './CustomFile';
export interface UploadIconProps {
  onChange: (file: StateFile | undefined) => void;
  icon: StateFile | undefined;
  setValidUpload: (isValid: boolean) => void;
}

const UploadIcon: FC<UploadIconProps> = ({
  onChange,
  icon,
  setValidUpload
}) => {
  const uploadRef = useRef<UploadRef | null>(null);
  const { t } = useTranslation();

  const handleChangeFile = (data: { files: StateFile[] }) => {
    onChange(data.files[0]);
    process.nextTick(() => {
      data.files[0]?.status === STATUS.valid &&
        uploadRef.current?.uploadFiles();
    });
  };

  const handleRemove = async () => {
    onChange(undefined);
    setValidUpload(true);
  };

  return (
    <div className="mt-16 custom-dls w-100">
      <Upload
        ref={uploadRef}
        files={icon ? [icon] : []}
        saveUrl={() => {}}
        id="automation-test-id"
        title={t('txt_choose_file')}
        inputConfig={{
          accept: FILE_TYPES_INPUT
        }}
        multiple={false}
        typeFiles={FILE_TYPES}
        maxFile={MAX_SIZE}
        onChangeFile={handleChangeFile}
        onRemove={handleRemove}
        renderFile={(props: FileProps) => {
          return (
            <CustomFile
              {...props}
              getValid={getValid}
              setValidUpload={setValidUpload}
            />
          );
        }}
      />
    </div>
  );
};

export default UploadIcon;
