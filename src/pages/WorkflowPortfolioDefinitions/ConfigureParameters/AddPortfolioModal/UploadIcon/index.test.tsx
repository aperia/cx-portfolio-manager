import userEvent from '@testing-library/user-event';
import { workflowTemplateService } from 'app/services/workflowTemplate.service';
import { renderWithMockStore } from 'app/utils';
import React from 'react';
import UploadFile from './index';

describe('UploadFile', () => {
  it('should render empty props', async () => {
    const props = {
      onChange: jest.fn(),
      setValidUpload: jest.fn(),
      icon: undefined
    };

    const { getByText } = await renderWithMockStore(<UploadFile {...props} />);
    expect(getByText('Choose File')).toBeInTheDocument();
  });

  it('should remove offline', async () => {
    const props = {
      icon: {
        idx: 'id',
        name: 'file.png',
        size: 1024,
        length: 1024,
        percentage: 100,
        item: () => null
      },
      onChange: jest.fn(),
      setValidUpload: jest.fn()
    };

    const { container, getByText } = await renderWithMockStore(
      <UploadFile {...props} />
    );
    expect(getByText('file.png')).toBeInTheDocument();

    userEvent.click(container.querySelector('.file-item .icon.icon-close')!);
  });

  it('should check valid when change file', async () => {
    const file = new File(['test'], 'file.png', { type: 'png' } as any);
    jest
      .spyOn(workflowTemplateService, 'uploadWorkflowTemplate')
      .mockImplementation(((formData: any, { onUploadProgress }: any) => {
        onUploadProgress({ loaded: 100, total: 100 });
        return Promise.resolve({
          idx: 'id',
          data: { attachment: [file] }
        });
      }) as any);
    jest.useFakeTimers();
    const props = {
      icon: {
        idx: 'id',
        name: 'file.png',
        size: 1024,
        length: 1024,
        percentage: 100,
        item: () => null
      },
      onChange: jest.fn(),
      setValidUpload: jest.fn()
    };

    const { container, queryByText, rerender } = await renderWithMockStore(
      <UploadFile {...props} />
    );

    const input = container.querySelector('input');
    userEvent.upload(input!, file);
    rerender(<UploadFile {...props} icon={file as any} />);
    jest.runAllTicks();
    jest.runAllTimers();

    expect(queryByText('file.png')).toBeInTheDocument();

    jest.useRealTimers();
  });

  it('should invalid when change file but data responce is empty attachment', async () => {
    const file = new File(['test'], 'file.png', { type: 'png' });
    jest
      .spyOn(workflowTemplateService, 'uploadWorkflowTemplate')
      .mockImplementation(((formData: any, { onUploadProgress }: any) => {
        onUploadProgress({ loaded: 100, total: 100 });
        return Promise.resolve({
          idx: 'id',
          data: { attachment: [] }
        });
      }) as any);
    jest.useFakeTimers();
    const props = {
      icon: {
        idx: 'id',
        name: 'file.png',
        size: 1024,
        length: 1024,
        percentage: 100,
        item: () => null
      },
      onChange: jest.fn(),
      setValidUpload: jest.fn()
    };

    const { container, queryByText, rerender } = await renderWithMockStore(
      <UploadFile {...props} />
    );
    const input = container.querySelector('input');
    userEvent.upload(input!, file);
    rerender(<UploadFile {...props} icon={file as any} />);
    jest.runAllTicks();
    jest.runAllTimers();

    expect(queryByText('file.png')).toBeInTheDocument();

    jest.useRealTimers();
  });

  it('should wait for next validation check', async () => {
    const file = new File(['test'], 'file.png', { type: 'png' });
    jest
      .spyOn(workflowTemplateService, 'uploadWorkflowTemplate')
      .mockImplementationOnce(((formData: any, { onUploadProgress }: any) => {
        onUploadProgress({ loaded: 100, total: 100 });
        return Promise.resolve({
          idx: 'id',
          data: {
            attachment: [
              {
                ...file,
                validationMessages: [
                  { field: 'field1', message: 'message1' },
                  { field: 'Extension', message: 'invalid Extension' }
                ]
              }
            ]
          }
        });
      }) as any);
    jest.spyOn(workflowTemplateService, 'getFileDetails').mockResolvedValue({
      data: { attachments: [{ status: 'REVIEW' }] }
    } as any);
    jest.useFakeTimers();
    const props = {
      icon: {
        idx: 'id',
        name: 'file.png',
        size: 1024,
        length: 1024,
        percentage: 100,
        item: () => null
      },
      onChange: jest.fn(),
      setValidUpload: jest.fn()
    };

    const { container, queryByText, rerender } = await renderWithMockStore(
      <UploadFile {...props} />
    );

    const input = container.querySelector('input');
    userEvent.upload(input!, file);
    rerender(<UploadFile {...props} icon={file as any} />);
    jest.runAllTicks();
    jest.runTimersToTime(2);

    expect(queryByText('file.png')).toBeInTheDocument();

    jest.useRealTimers();
  });

  it('should check valid when change file with validation success', async () => {
    const file = new File(['test'], 'file.png', { type: 'png' });
    jest
      .spyOn(workflowTemplateService, 'uploadWorkflowTemplate')
      .mockImplementationOnce(((formData: any, { onUploadProgress }: any) => {
        onUploadProgress({ loaded: 100, total: 100 });
        return Promise.resolve({
          idx: 'id',
          data: {
            attachment: [
              {
                ...file,
                validationMessages: [
                  { field: 'field1', message: 'message1' },
                  { field: 'Extension', message: 'invalid Extension' }
                ]
              }
            ]
          }
        });
      }) as any);
    jest.spyOn(workflowTemplateService, 'getFileDetails').mockResolvedValue({
      data: { attachments: [{ status: 'VALID' }] }
    } as any);
    jest.useFakeTimers();
    const props = {
      icon: {
        idx: 'id',
        name: 'file.png',
        size: 1024,
        length: 1024,
        percentage: 100,
        item: () => null
      },
      onChange: jest.fn(),
      setValidUpload: jest.fn()
    };

    const { container, queryByText, rerender } = await renderWithMockStore(
      <UploadFile {...props} />
    );
    const input = container.querySelector('input');
    userEvent.upload(input!, file);
    rerender(<UploadFile {...props} icon={file as any} />);
    jest.runAllTicks();
    jest.runTimersToTime(2);

    expect(queryByText('file.png')).toBeInTheDocument();
    rerender(<UploadFile {...props} icon={file as any} />);
    jest.useRealTimers();
  });

  it('should check valid when change file with validation message', async () => {
    const file = new File(['test'], 'file.png', { type: 'png' });
    jest
      .spyOn(workflowTemplateService, 'uploadWorkflowTemplate')
      .mockImplementationOnce(((formData: any, { onUploadProgress }: any) => {
        onUploadProgress({ loaded: 100, total: 100 });
        return Promise.resolve({
          idx: 'id',
          data: {
            attachment: [
              {
                ...file,
                validationMessages: [
                  { field: 'field1', message: 'message1' },
                  { field: 'Extension', message: 'invalid Extension' }
                ]
              }
            ]
          }
        });
      }) as any);
    jest.spyOn(workflowTemplateService, 'getFileDetails').mockResolvedValue({
      data: {
        attachments: [
          { status: 'INVALID', validationMessages: [{ message: 'message' }] }
        ]
      }
    } as any);
    jest.useFakeTimers();
    const props = {
      icon: {
        idx: 'id',
        name: 'file.png',
        size: 1024,
        length: 1024,
        percentage: 100,
        item: () => null
      },
      onChange: jest.fn(),
      setValidUpload: jest.fn()
    };

    const { container, queryByText, rerender } = await renderWithMockStore(
      <UploadFile {...props} />
    );

    const input = container.querySelector('input');
    userEvent.upload(input!, file);
    rerender(<UploadFile {...props} icon={file as any} />);
    jest.runAllTicks();
    jest.runTimersToTime(2);

    expect(queryByText('file.png')).toBeInTheDocument();

    jest.useRealTimers();
  });

  it('should check invalid when change file with fail to upload', async () => {
    const file = new File(['test'], 'file.png', { type: 'png' });
    jest
      .spyOn(workflowTemplateService, 'uploadWorkflowTemplate')
      .mockRejectedValueOnce({});
    jest.useFakeTimers();
    const props = {
      icon: {
        idx: 'id',
        name: 'file.png',
        size: 1024,
        length: 1024,
        percentage: 100,
        item: () => null
      },
      onChange: jest.fn(),
      setValidUpload: jest.fn()
    };

    const { container, queryByText, rerender } = await renderWithMockStore(
      <UploadFile {...props} />
    );
    const input = container.querySelector('input');
    userEvent.upload(input!, file);

    rerender(<UploadFile {...props} icon={file as any} />);
    jest.runAllTicks();
    jest.runAllTimers();

    expect(queryByText('file.png')).toBeInTheDocument();
    jest.useRealTimers();
  });

  it('should get file detail when edit wf', async () => {
    jest.spyOn(workflowTemplateService, 'getFileDetails').mockResolvedValue({
      data: {
        attachments: [
          { id: 'id', status: 'Valid', filename: 'file.png', fileSize: 123 }
        ]
      }
    } as any);
    jest.useFakeTimers();
    const props = {
      icon: {
        idx: 'id',
        name: 'file.png',
        size: 1024,
        length: 1024,
        percentage: 100,
        item: () => null
      },
      onChange: jest.fn(),
      setValidUpload: jest.fn()
    };

    await renderWithMockStore(<UploadFile {...props} />);
    jest.useRealTimers();
  });
});
