import '@testing-library/jest-dom';
import {
  act,
  cleanup,
  fireEvent,
  render,
  screen
} from '@testing-library/react';
import * as helper from 'app/_libraries/_dls/components/Upload/helper';
import { queryByClass } from 'app/_libraries/_dls/test-utils';
import 'app/_libraries/_dls/test-utils/mocks/mockCanvas';
import * as canvasTextWidth from 'app/_libraries/_dls/utils/canvasTextWidth';
import * as getElementAttribute from 'app/_libraries/_dls/utils/getElementAttribute';
import React from 'react';
import File from '.';

afterEach(cleanup);

const STATUS = helper.STATUS;

jest.mock('resize-observer-polyfill', () =>
  jest.requireActual('app/_libraries/_dls/test-utils/mocks/MockResizeObserver')
);

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => () => ({
  t: (text: string) => text
}));

const renderWrapper = (props: any) => {
  let wrapper;

  jest.useFakeTimers();
  act(() => {
    wrapper = render(<File {...props} />);
    jest.runAllTimers();
  });

  return wrapper as any;
};

const index = 1;

describe('File', () => {
  it('Delete item', () => {
    const handleRemoveFile = jest.fn();
    const file = {
      idx: '1',
      name: 'name.png',
      length: 1024,
      pngDescription: '',
      pngName: '',
      status: STATUS.valid,
      percentage: 20,
      item: () => null
    };
    renderWrapper({
      onRemoveFile: handleRemoveFile,
      file,
      typeFiles: ['png'],
      setValidUpload: jest.fn(),
      isEditFileName: true,
      index
    });
    act(() => {
      screen
        .getByTestId('undefined-namepng_dls-upload-file-remove-button')
        .click();
    });

    expect(handleRemoveFile).toHaveBeenCalledWith(index, file.idx);
  });

  it('Render error', () => {
    const handleRemoveFile = jest.fn();
    const file = {
      idx: '1',
      name: 'name.png',
      length: 1024,
      pngName: '',
      pngDescription: 'description',
      status: STATUS.error,
      percentage: 20,
      item: () => null
    };
    const { container } = renderWrapper({
      onRemoveFile: handleRemoveFile,
      file,
      typeFiles: ['txt'],
      setValidUpload: jest.fn(),
      isEditFileName: true,
      index,
      getValid: jest.fn(props => ({
        isTypeSupport: false
      }))
    });

    expect(queryByClass(container, /icon icon-error/)).toBeInTheDocument();
  });

  it('Render valid file', () => {
    const handleRemoveFile = jest.fn();
    const file = {
      idx: '1',
      name: 'name.png',
      length: 1024,
      pngName: '',
      pngDescription: 'description',
      status: STATUS.valid,
      percentage: 20,
      item: () => null
    };
    const { container } = renderWrapper({
      onRemoveFile: handleRemoveFile,
      file,
      typeFiles: ['png'],
      setValidUpload: jest.fn(),
      isEditFileName: true,
      index
    });

    expect(queryByClass(container, /icon icon-success/)).toBeInTheDocument();
  });

  it('Render one line & text truncate', () => {
    const handleRemoveFile = jest.fn();
    const file = {
      idx: '1',
      name: 'name.png',
      length: 1024,
      status: STATUS.success,
      item: () => null
    };
    act(() => {
      Object.defineProperty(Element.prototype, 'clientWidth', {
        value: 123
      });
      jest
        .spyOn(getElementAttribute, 'getAvailableWidth')
        .mockImplementation(() => 10);
      jest.spyOn(helper, 'getAllWidthChildren').mockImplementation(() => 1000);
    });
    renderWrapper({
      onRemoveFile: handleRemoveFile,
      file,
      typeFiles: ['png'],
      setValidUpload: jest.fn(),
      isEditFileName: true,
      index
    });

    expect(queryByClass(document.body, /text-truncate/)).toBeInTheDocument();
  });

  it('Render 2 line', () => {
    const handleRemoveFile = jest.fn();
    const file = {
      idx: '1',
      name: 'name.txt',
      length: 1024,
      status: STATUS.success,
      item: () => null
    };
    const wrapper = renderWrapper({
      onRemoveFile: handleRemoveFile,
      file: file,
      typeFiles: ['png'],
      index: index,
      setValidUpload: jest.fn()
    });

    jest.spyOn(canvasTextWidth, 'default').mockImplementation(() => null);

    act(() => {
      let fake = 0;
      Object.defineProperty(Element.prototype, 'clientWidth', {
        get: () => {
          fake++;
          return fake % 2 === 0 ? 1 : 2;
        }
      });
      // trigger resize
      window.dispatchEvent(new Event('resize'));
    });
    expect(queryByClass(document.body, /default/)).toBeInTheDocument();

    act(() => {
      wrapper.rerender(
        <File
          onRemoveFile={handleRemoveFile}
          file={{ ...file, name: 'a.png' }}
          typeFiles={['png']}
          setValidUpload={jest.fn()}
          index={index}
        />
      );
      jest.runAllTimers();
    });
    expect(queryByClass(document.body, /default/)).not.toBeInTheDocument();
  });

  it('Status updating', () => {
    const handleRemoveFile = jest.fn();
    const file = {
      idx: '1',
      name: 'name.png',
      length: 1024,
      txtDescription: '',
      txtName: '',
      status: STATUS.uploading,
      item: () => null
    };
    const wrapper = renderWrapper({
      onRemoveFile: handleRemoveFile,
      file: file,
      typeFiles: ['png'],
      index: index,
      setValidUpload: jest.fn(),
      isEditFileName: true,
      getValid: jest.fn(props => ({
        isTypeSupport: true
      }))
    });

    expect(queryByClass(wrapper.baseElement, /uploading/)).toBeInTheDocument();
  });

  it('Change name & description', () => {
    const handleRemoveFile = jest.fn();
    const file = {
      idx: '1',
      name: 'name.png',
      length: 1024,
      txtDescription: '',
      txtName: '',
      status: STATUS.valid,
      item: () => null
    };
    renderWrapper({
      onRemoveFile: handleRemoveFile,
      file: file,
      typeFiles: ['png'],
      index: index,
      setValidUpload: jest.fn(),
      isEditFileName: true,
      getValid: jest.fn(props => ({
        isTypeSupport: true
      }))
    });
    const cpmName = screen.getByPlaceholderText(
      'txt_type_to_add_name'
    ) as HTMLInputElement;
    fireEvent.change(cpmName, { target: { value: '123' } });
    expect(cpmName.value).toBe('123');
  });
});
