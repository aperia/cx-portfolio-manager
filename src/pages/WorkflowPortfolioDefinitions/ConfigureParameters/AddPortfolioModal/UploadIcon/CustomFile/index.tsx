import { Button, Icon, Tooltip, useTranslation } from 'app/_libraries/_dls';
import {
  Props as FileProps,
  StateFile
} from 'app/_libraries/_dls/components/Upload/File';
import {
  formatBytes,
  getAllWidthChildren,
  getFileExtension,
  getValid,
  MAX_FILE,
  MIN_FILE,
  Options,
  STATUS
} from 'app/_libraries/_dls/components/Upload/helper';
import {
  canvasTextWidth,
  genAmtId,
  getAvailableWidth
} from 'app/_libraries/_dls/utils';
import classNames from 'classnames';
import React, {
  useEffect,
  useLayoutEffect,
  useMemo,
  useRef,
  useState
} from 'react';
// helpers
import ResizeObserver from 'resize-observer-polyfill';
import CustomMessage from './CustomMessage';

export interface CustomFileProps extends FileProps {
  getValid?: (
    file: StateFile,
    options: Options
  ) => {
    isInvalid?: boolean;
    isTypeSupport?: boolean;
    isOverSize?: boolean;
    isUnderSize?: boolean;
  };
  setValidUpload: (isNotValid: boolean) => void;
}

const CustomFile: React.FC<CustomFileProps> = ({
  className,
  file,
  index,
  onRemoveFile,
  typeFiles,
  setValidUpload,
  isEditFileName,
  maxFile = MAX_FILE,
  minFile = MIN_FILE,

  getValid: getValidCustom,

  dataTestId
}) => {
  const { idx, name, size, lastModified, status, txtName } = file;
  const { t } = useTranslation();

  const fileRef = useRef<HTMLDivElement | null>(null);
  const containerRef = useRef<HTMLDivElement | null>(null);
  const iconRef = useRef<HTMLDivElement | null>(null);
  const fileMessageRef = useRef<HTMLDivElement | null>(null);
  const fileSizeRef = useRef<HTMLDivElement | null>(null);

  const [fileNameRef, setFileNameRef] = useState<HTMLDivElement | null>(null);
  const [isTruncate, setIsTruncate] = useState<boolean>();
  const [isOneLine, setIsOneLine] = useState<boolean>();

  const fileExtension = getFileExtension(name);
  const valid = getValidCustom
    ? getValidCustom(file, { typeFiles, maxFile, minFile })
    : getValid(file, { typeFiles, maxFile, minFile });

  useLayoutEffect(() => {
    const resizeObserver = new ResizeObserver(() => {
      process.nextTick(() => {
        const widthParent = fileRef.current?.parentElement?.clientWidth;
        const widthFile = fileRef.current?.clientWidth;
        const isOneLine = widthFile === widthParent;

        setIsOneLine(isOneLine);
        const isDefault =
          fileRef.current?.parentElement?.classList.contains('default');

        if (isOneLine && !isDefault) {
          fileRef.current?.parentElement?.classList.add('default');
        }
        // ~ else if
        if (!(isOneLine && !isDefault) && !isOneLine) {
          fileRef.current?.parentElement?.classList.remove('default');
        }

        if (!fileNameRef) return;

        const widthContainer = getAvailableWidth(fileNameRef);
        const widthMessage = getAllWidthChildren(fileMessageRef.current);

        const computedStyles = window.getComputedStyle(fileNameRef);
        const textMetrics = canvasTextWidth(name!, computedStyles.font);

        const widthName = textMetrics?.width || 0;

        const isTruncate = widthContainer - widthName - widthMessage - 32 < 0;

        setIsTruncate(isTruncate);
      });
    });
    resizeObserver.observe(containerRef.current!);

    // clear ResizeObserver
    return () => resizeObserver.disconnect();
  }, [fileNameRef, name]);

  const isUpload: boolean = status === STATUS.uploading;
  const isDone: boolean = status === STATUS.success;
  const isUploadFailed: boolean = status === STATUS.error;
  const isError: boolean = isUploadFailed || (valid.isInvalid as boolean);

  useEffect(() => {
    if (isUpload) {
      setValidUpload(!isUpload);
      return;
    }
    setValidUpload(!isError);
  }, [isError, isUpload, setValidUpload]);

  const tooltipBtnRemoveFile = t('txt_remove');
  const { isTypeSupport } = valid;
  const hasName =
    isEditFileName && isTypeSupport && !isUpload && !isDone && !isError;

  const placeholderName = t('txt_type_to_add_name');

  const messageCpm = useMemo(() => {
    const statusMsg = {
      isDone,
      isUpload,
      isUploadFailed,
      maxFile,
      minFile
    };

    return (
      <div
        className={classNames('file-message', {
          error: isError,
          uploading: isUpload
        })}
        ref={fileMessageRef}
      >
        <CustomMessage valid={valid} status={statusMsg} isError={isError} />
      </div>
    );
  }, [isDone, isError, isUpload, isUploadFailed, maxFile, minFile, valid]);

  return (
    <div
      ref={fileRef}
      className={classNames(
        'file-item',
        { truncate: isOneLine && isTruncate },
        className
      )}
      key={`${name}-${lastModified}`}
      data-testid={genAmtId(
        `${dataTestId}-${name}`,
        'dls-upload-file',
        'Upload.File'
      )}
    >
      <div ref={containerRef} className="file-item-content">
        <i ref={iconRef} className={`bg-file-type ${fileExtension}`} />
        <div ref={setFileNameRef} className="file-name">
          {hasName ? (
            <input
              className="text-truncate"
              placeholder={placeholderName}
              defaultValue={txtName || name}
              onChange={event => {
                file.txtName = event.target.value;
              }}
              data-testid={genAmtId(
                `${dataTestId}-${name}`,
                'dls-upload-file-input',
                'Upload.File.input'
              )}
            />
          ) : (
            <div
              className="text-truncate"
              title={name}
              data-testid={genAmtId(
                `${dataTestId}-${name}`,
                'dls-upload-file-name',
                'Upload.File.name'
              )}
            >
              {name}
            </div>
          )}
          {!isTruncate && messageCpm}
        </div>
        {isTruncate && messageCpm}

        <div
          className="file-size"
          ref={fileSizeRef}
          data-testid={genAmtId(
            `${dataTestId}-${name}`,
            'dls-upload-file-info',
            'Upload.File.info'
          )}
        >
          <>
            {!isUpload && (
              <span className="file-size-info">{formatBytes(size)}</span>
            )}
            {!isUpload && (
              <Tooltip
                triggerClassName="delete-file-btn"
                placement="top"
                variant="primary"
                element={tooltipBtnRemoveFile}
                data-testid={genAmtId(
                  `${dataTestId}-${name}`,
                  'dls-upload-file-name',
                  'Upload.File.name'
                )}
              >
                <Button
                  size="sm"
                  variant="icon-secondary"
                  onClick={() => onRemoveFile(index, idx)}
                  data-testid={genAmtId(
                    `${dataTestId}-${name}`,
                    'dls-upload-file-remove-button',
                    'Upload.File.remove'
                  )}
                >
                  <Icon name="close" />
                </Button>
              </Tooltip>
            )}
          </>
        </div>
      </div>
    </div>
  );
};

export default CustomFile;
