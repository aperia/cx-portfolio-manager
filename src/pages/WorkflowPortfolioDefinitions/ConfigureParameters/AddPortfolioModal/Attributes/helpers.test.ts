import { getKeyOperator } from './helpers';

describe('test helper', () => {
  it('gen keyOperator', () => {
    getKeyOperator('Sys');
  });

  it('when parameter isn`t string', () => {
    getKeyOperator(123);
  });
});
