import {
  Button,
  DropdownList,
  Icon,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import { find } from 'app/_libraries/_dls/lodash';
import classNames from 'classnames';
import { useSelectElementMetadataForPortfolioDefinitionsSelector } from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowPortfolioDefinitions';
import React, { FC } from 'react';
import { AttributeProps } from '.';
import Attribute, { AttributeTemplateProps } from './Attribute';
import SelectMethodAddButtons from './SelectMethodAddButtons';

export interface AttributeItemGroupProps {
  operator: string;
  attributes?: AttributeProps[];
  type: string;
  item?: AttributeProps;
}
export interface AttributeGroupProps
  extends Omit<AttributeTemplateProps, 'attribute'> {
  data: AttributeItemGroupProps;
  onSelectAddMethod: (e: any, path: string) => void;
  addAttribute: (path: string) => void;
  onChange: (field: string, path: string) => (e: any) => void;
  filterOptionsValid: string[];
  checkAttributeValid: (path: string, isValid: boolean) => void;
}

const AttributeGroup: FC<AttributeGroupProps> = ({
  removeAttribute,
  data,
  onSelectAddMethod,
  addAttribute,
  onChange,
  path,
  filterOptionsValid,
  checkAttributeValid
}) => {
  const { t } = useTranslation();
  const { operator: operators = [] } =
    useSelectElementMetadataForPortfolioDefinitionsSelector();
  const { attributes = [], type, operator, item = {} } = data;
  const isAttribute = type === 'attribute';

  const operatorRelatedValue = find(operators, { code: operator });

  const level = path.split('.').length;

  const classNameSub =
    level === 2
      ? 'condition-sub-item bg-light-l16'
      : 'condition-sub-sub-item bg-light-l20';

  const Operator = () => {
    if (!operator) return null;

    if (operator === 'where') return t('txt_portfolio_definitions_where');

    return (
      <DropdownList
        variant="no-border"
        value={operatorRelatedValue}
        className="pl-0 ml-n8"
        textField="text"
        name="operatorRelated"
        onChange={onChange('operatorRelated', path)}
      >
        {operators?.map(item => (
          <DropdownList.Item
            key={item?.code}
            value={item?.code}
            label={item?.text}
          />
        ))}
      </DropdownList>
    );
  };

  if (isAttribute) {
    return (
      <div className="condition-item">
        <div className="flex-shrink-0 w-10 w-lg-8 w-xxl-5 pt-8">
          <Operator />
        </div>

        <Attribute
          path={path}
          removeAttribute={removeAttribute}
          attribute={item}
          onChange={onChange}
          filterOptionsValid={filterOptionsValid}
          checkAttributeValid={checkAttributeValid}
        />
      </div>
    );
  }

  return (
    <div className="condition-item condition-item-group">
      <div className="flex-shrink-0 w-10 w-lg-8 w-xxl-5 pt-24">
        <Operator />
      </div>

      <div
        className={classNames(`${classNameSub} p-16 rounded-lg flex-1 mx-16`)}
      >
        {attributes.map(
          (attributes: Omit<AttributeItemGroupProps, 'item'>, idx: number) => {
            return (
              <AttributeGroup
                key={idx}
                path={`${path}.${idx}`}
                removeAttribute={removeAttribute}
                data={attributes}
                onSelectAddMethod={onSelectAddMethod}
                addAttribute={addAttribute}
                onChange={onChange}
                filterOptionsValid={filterOptionsValid}
                checkAttributeValid={checkAttributeValid}
              />
            );
          }
        )}
        <div className="d-flex align-items-center">
          <div className="d-flex ml-auto">
            {level === 2 ? (
              <div className="d-flex justify-content-end mt-16 mr-n8">
                <Button
                  size="sm"
                  variant="outline-primary"
                  onClick={() => addAttribute(path)}
                >
                  {t('txt_portfolio_definitions_add_attribute')}
                </Button>
              </div>
            ) : (
              <SelectMethodAddButtons
                onSelectAddMethod={onSelectAddMethod}
                path={path}
                isGroup
              />
            )}
          </div>
        </div>
      </div>
      <div className="flex-shrink-0 pt-28 mr-n2">
        <Tooltip placement="top" variant="primary" element={t('txt_remove')}>
          <Button
            size="sm"
            variant="icon-secondary"
            onClick={() => removeAttribute(path)}
          >
            <Icon name="close" size="4x" className="color-red" />
          </Button>
        </Tooltip>
      </div>
    </div>
  );
};

export default AttributeGroup;
