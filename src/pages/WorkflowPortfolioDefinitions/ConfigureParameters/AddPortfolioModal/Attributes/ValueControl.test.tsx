import { fireEvent, render } from '@testing-library/react';
import * as WorkflowPortfolioDefinitions from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowPortfolioDefinitions';
import React from 'react';
import ValueControl from './ValueControl';

const MockSelectWorkflowMethodsSelector = jest.spyOn(
  WorkflowPortfolioDefinitions,
  'useSelectElementMetadataForPortfolioDefinitionsSelector'
);

const props = {
  filterBy: 'Sys',
  path: '',
  errors: {},
  onChange: jest.fn(),
  setTouched: jest.fn()
};

describe('CreatePortfolioDefinitionsWorkflow > ConfigureParameters > AttributeSection > AttributeGroup', () => {
  beforeEach(() => {
    MockSelectWorkflowMethodsSelector.mockReturnValue({
      'filter.by': [
        {
          text: 'Sys',
          code: 'Sys'
        },
        {
          text: 'Prin',
          code: 'Prin'
        }
      ],
      sys: [
        {
          text: 'test',
          code: 'test'
        }
      ],
      ['Portfolio ID']: [
        {
          text: 'Sys',
          code: 'Sys'
        },
        {
          text: 'Prin',
          code: 'Prin'
        }
      ]
    });
  });

  it('Should render with ValueControl > Combobox', async () => {
    const wrapper = render(<ValueControl {...props} />);
    const cbbIcon = wrapper.baseElement.querySelector(
      '.icon.icon-chevron-down'
    );
    fireEvent.mouseDown(cbbIcon!);
    expect(cbbIcon).not.toBeNull();
  });

  it('Should render with ValueControl > Textbox', () => {
    const wrapper = render(
      <ValueControl {...props} filterBy="Miscellaneous Fields 16-20-TX" />
    );
    expect(
      wrapper.getByTestId('textbox_value_dls-text-box_input')
    ).toBeInTheDocument();
  });

  it('Should render with ValueControl > Textbox disable', () => {
    const wrapper = render(<ValueControl {...props} filterBy="" />);
    expect(
      wrapper.getByTestId('textbox_value__disable_dls-text-box_input')
    ).toBeDisabled();
  });

  it('Should render with ValueControl > Numeric', () => {
    const wrapper = render(
      <ValueControl {...props} filterBy="Product Type Code" />
    );

    expect(
      wrapper.getByTestId('numeric_value_dls-numeric-textbox-input')
    ).toBeInTheDocument();
  });

  it('Should render with ValueControl > Numeric', () => {
    const wrapper = render(<ValueControl {...props} filterBy="Portfolio ID" />);

    expect(
      wrapper.getByTestId('format_textbox_value_dls-text-box')
    ).toBeInTheDocument();
  });
});
