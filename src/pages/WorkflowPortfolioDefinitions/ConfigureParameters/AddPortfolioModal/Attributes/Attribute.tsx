import { useFormValidations } from 'app/hooks';
import {
  Button,
  ComboBox,
  DropdownList,
  Icon,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import { find, get, isEmpty } from 'lodash';
import { useSelectElementMetadataForPortfolioDefinitionsSelector } from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowPortfolioDefinitions';
import React, { FC, useEffect, useMemo, useState } from 'react';
import { AttributeItemProps } from '.';
import {
  ATTRIBUTE_VALUE_FIELDS,
  FILTER_SPA_OPTIONS,
  MAX_FILTER_OPTIONS_DEFAULT
} from '../../constant';
import { getKeyOperator } from './helpers';
import ValueControl from './ValueControl';

export interface AttributeTemplateProps {
  path: string;
  removeAttribute: (path: string) => void;
  attribute: AttributeItemProps;
  onChange: (field: string, path: string) => (e: any) => void;
  filterOptionsValid: string[];
  checkAttributeValid: (path: string, isValid: boolean) => void;
}

const Attribute: FC<AttributeTemplateProps> = ({
  path,
  removeAttribute,
  attribute,
  onChange,
  filterOptionsValid,
  checkAttributeValid
}) => {
  const { t } = useTranslation();
  const [resetTouch, setResetTouch] = useState(0);

  const metadata = useSelectElementMetadataForPortfolioDefinitionsSelector();
  const { ['filter.by']: filterByOptions } = metadata;

  const { filterBy = '', operator, value } = attribute;

  const currentErrors = useMemo(() => {
    const { props: { required = false } = {} } =
      ATTRIBUTE_VALUE_FIELDS[filterBy] || {};

    const errorObj = {
      filterBy: isEmpty(filterBy) && {
        status: true,
        message: t('txt_required_validation', {
          fieldName: 'Filter By'
        })
      },
      operator: !isEmpty(filterBy) &&
        isEmpty(operator) && {
          status: true,
          message: t('txt_required_validation', {
            fieldName: 'Operator'
          })
        }
    } as Record<any, IFormError>;
    if (required) {
      Object.assign(errorObj, {
        value: !isEmpty(filterBy) &&
          isEmpty(value) && {
            status: true,
            message: t('txt_required_validation', {
              fieldName: 'Value'
            })
          }
      });
    }

    return errorObj;
  }, [filterBy, operator, value, t]);

  const [, errors, setTouched] = useFormValidations<any>(
    currentErrors,
    resetTouch
  );

  useEffect(() => {
    const isValid = Object.keys(currentErrors).every(
      (field: string) => (get(currentErrors, field, false) as boolean) === false
    );

    checkAttributeValid(path, isValid);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentErrors, path]);

  useEffect(() => {
    process.nextTick(() => {
      setResetTouch(t => t + 1);
    });
  }, [filterBy]);

  const filterByItem = find(filterByOptions, { code: filterBy })?.code;

  const keyOperator: string = getKeyOperator(filterBy);

  const operatorOptions: RefData[] = get(metadata, keyOperator, []);
  const operatorValue = find(operatorOptions, { code: operator });

  let maxFilters = MAX_FILTER_OPTIONS_DEFAULT;
  let hasSPAFilter = false;

  if (filterOptionsValid.length >= MAX_FILTER_OPTIONS_DEFAULT) {
    const numberSPAOptions: number = filterOptionsValid.filter(
      (option: string) => FILTER_SPA_OPTIONS.includes(option)
    ).length;
    hasSPAFilter = numberSPAOptions > 0;
    maxFilters = maxFilters + (numberSPAOptions - 1);
  }

  const isLimitOptions = filterOptionsValid.length >= maxFilters;

  return (
    <>
      <div className="row flex-1 mx-4">
        <div className="col-6 col-lg-3">
          <ComboBox
            value={filterByItem}
            label={t('txt_filter_by')}
            name="filterBy"
            textField="text"
            onChange={onChange('filterBy', path)}
            onFilterChange={(e: any) => onChange('filterBy', path)}
            onFocus={setTouched('filterBy')}
            onBlur={setTouched('filterBy', true)}
            error={errors?.filterBy}
            required
          >
            {filterByOptions?.map(item => {
              let isDisable = false;
              if (!hasSPAFilter || !FILTER_SPA_OPTIONS.includes(item?.code)) {
                isDisable =
                  !filterOptionsValid.includes(item?.code) && isLimitOptions;
              }

              const itemProps = {
                value: item?.code,
                label: item?.text,
                disabled: isDisable
              };

              if (isDisable) {
                Object.assign(itemProps, {
                  tooltipProps: {
                    element: (
                      <div className="custom-tooltip">
                        {t('txt_portfolio_definitions__filter_tooltip')}
                      </div>
                    )
                  }
                });
              }

              return <ComboBox.Item key={item?.code} {...itemProps} />;
            })}
          </ComboBox>
        </div>
        <div className="col-6 col-lg-3">
          <DropdownList
            value={operatorValue}
            label={t('txt_manage_account_memos_operator')}
            name="findMemos_operator"
            textField="text"
            required
            disabled={isEmpty(filterBy)}
            onChange={onChange('operator', path)}
            readOnly={operatorOptions.length === 1}
            onFocus={setTouched('operator')}
            onBlur={setTouched('operator', true)}
            error={errors?.operator}
          >
            {operatorOptions.map(item => (
              <DropdownList.Item
                key={item.code}
                value={item.code}
                label={item.text}
              />
            ))}
          </DropdownList>
        </div>
        <div className="col-12 col-lg-6 mt-16 mt-lg-0">
          <ValueControl
            filterBy={filterBy}
            value={value}
            path={path}
            errors={errors}
            onChange={onChange}
            setTouched={setTouched}
          />
        </div>
      </div>
      <div className="flex-shrink-0 pt-12 mr-n2">
        <Tooltip
          placement="top"
          variant="primary"
          triggerClassName="dls-upload-file-btn-remove"
          element={t('txt_remove')}
        >
          <Button
            size="sm"
            variant="icon-secondary"
            onClick={() => {
              removeAttribute(path);
            }}
          >
            <Icon name="close" size="4x" className="color-red" />
          </Button>
        </Tooltip>
      </div>
    </>
  );
};

export default Attribute;
