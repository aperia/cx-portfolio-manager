import EnhanceDropdownButton from 'app/components/EnhanceDropdownButton';
import { ADD_ATTRIBUTE_BUTTONS } from 'app/constants/constants';
import {
  DropdownButton,
  DropdownButtonSelectEvent,
  useTranslation
} from 'app/_libraries/_dls';
import React, { FC, useMemo } from 'react';

export interface SelectMethodAddButtonsProps {
  onSelectAddMethod: (
    e: DropdownButtonSelectEvent,
    path: string,
    isGroup?: boolean
  ) => void;
  path: string;
  isGroup?: boolean;
}

const SelectMethodAddButtons: FC<SelectMethodAddButtonsProps> = ({
  onSelectAddMethod,
  path,
  isGroup
}) => {
  const { t } = useTranslation();

  const dropdownListItem = useMemo(() => {
    return ADD_ATTRIBUTE_BUTTONS.map(item => (
      <DropdownButton.Item
        key={item.value}
        label={t(item.description)}
        value={item}
      />
    ));
  }, [t]);

  return (
    <div className="d-flex justify-content-end mt-16 mr-n6">
      <EnhanceDropdownButton
        buttonProps={{
          size: 'sm',
          children: t('txt_add'),
          variant: 'outline-primary'
        }}
        onSelect={e => onSelectAddMethod(e, path, isGroup)}
      >
        {dropdownListItem}
      </EnhanceDropdownButton>
    </div>
  );
};

export default SelectMethodAddButtons;
