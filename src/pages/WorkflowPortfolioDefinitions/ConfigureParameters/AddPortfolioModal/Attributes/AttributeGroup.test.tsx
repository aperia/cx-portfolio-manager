import { render, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import 'app/utils/_mockComponent/mockCanvas';
import * as WorkflowPortfolioDefinitions from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowPortfolioDefinitions';
import React from 'react';
import AttributeGroup from './AttributeGroup';
const MockSelectWorkflowMethodsSelector = jest.spyOn(
  WorkflowPortfolioDefinitions,
  'useSelectElementMetadataForPortfolioDefinitionsSelector'
);

jest.mock('./Attribute', () => ({
  __esModule: true,
  default: () => {
    return (
      <div>
        <div>Attribute</div>
      </div>
    );
  }
}));

jest.mock('./SelectMethodAddButtons', () => ({
  __esModule: true,
  default: () => {
    return (
      <div>
        <div>SelectMethodAddButtons</div>
      </div>
    );
  }
}));

const props = {
  removeAttribute: jest.fn(),
  onSelectAddMethod: jest.fn(),
  addAttribute: jest.fn(),
  onChange: jest.fn(),
  path: '',
  checkAttributeValid: jest.fn(),
  filterOptionsValid: [],
  data: {
    operator: 'where',
    type: 'attribute'
  }
};

describe('CreatePortfolioDefinitionsWorkflow > ConfigureParameters > AttributeSection > AttributeGroup', () => {
  beforeEach(() => {
    MockSelectWorkflowMethodsSelector.mockReturnValue({
      'filter.by': [
        {
          text: 'Sys',
          code: 'Sys'
        },
        {
          text: 'Prin',
          code: 'Prin'
        }
      ],
      operator: [
        {
          text: 'and',
          code: 'and'
        }
      ]
    });
  });

  afterEach(() => {
    MockSelectWorkflowMethodsSelector.mockClear();
  });

  it('Should render with Attribute', () => {
    const wrapper = render(<AttributeGroup {...props} />);
    expect(wrapper.getByText('Attribute')).toBeInTheDocument();
  });

  it('Should render with SelectMethodAddButtons', () => {
    const wrapper = render(
      <AttributeGroup
        {...props}
        data={{
          operator: 'where',
          attributes: [
            {
              operator: '',
              item: { filterBy: 'Sys', operator: 'and', value: 'AA' },
              type: 'attribute'
            },
            {
              operator: 'and',
              item: { filterBy: 'Sys', operator: 'and', value: 'AA' },
              type: 'attribute'
            }
          ],
          type: 'group'
        }}
      />
    );
    expect(wrapper.getByText('SelectMethodAddButtons')).toBeInTheDocument();
  });

  it('Should render with Dropdown Operator', () => {
    const wrapper = render(
      <AttributeGroup
        {...props}
        data={{
          operator: 'where',
          attributes: [
            {
              operator: '',
              item: { filterBy: 'Sys', operator: 'and', value: 'AA' },
              type: 'attribute'
            },
            {
              operator: 'and',
              item: { filterBy: 'Sys', operator: 'and', value: 'AA' },
              type: 'attribute'
            }
          ],
          type: 'group'
        }}
      />
    );
    const ddl = wrapper.container.querySelector(
      '.dls-dropdown-list .text-field-container'
    )!;
    expect(ddl).toBeInTheDocument();
  });

  it('Should render with AttributeGroup > render Where Operator', () => {
    const wrapper = render(
      <AttributeGroup
        {...props}
        data={{
          operator: 'where',
          attributes: [
            {
              operator: '',
              item: { filterBy: 'Sys', operator: 'and', value: 'AA' },
              type: 'attribute'
            }
          ],
          type: 'group'
        }}
      />
    );
    expect(
      wrapper.getByText('txt_portfolio_definitions_where')
    ).toBeInTheDocument();
  });

  it('Should render with AttributeGroup > is first item attribute group', () => {
    const wrapper = render(
      <AttributeGroup
        {...props}
        data={{
          operator: '',
          attributes: [
            {
              operator: '',
              item: { filterBy: 'Sys', operator: 'and', value: 'AA' },
              type: 'attribute'
            }
          ],
          type: 'group'
        }}
      />
    );
    expect(
      wrapper.baseElement.querySelector('.flex-shrink-0.w-10')
        ?.childElementCount
    ).toEqual(0);
  });

  it('Should render with AttributeGroup > add attribute', async () => {
    const addAttribute = jest.fn();
    const wrapper = render(
      <AttributeGroup
        {...props}
        path="0.1"
        addAttribute={addAttribute}
        data={{
          operator: 'where',
          attributes: [
            {
              operator: '',
              item: { filterBy: 'Sys', operator: 'and', value: 'AA' },
              type: 'attribute'
            }
          ],
          type: 'group'
        }}
      />
    );

    const addAttributeBtn = wrapper.baseElement.querySelector(
      '.btn.btn-outline-primary'
    );

    expect(addAttributeBtn).toBeInTheDocument();

    await waitFor(() => {
      addAttributeBtn && userEvent.click(addAttributeBtn);
    });
    jest.runAllTimers();
    expect(addAttribute).toHaveBeenCalled();
  });

  it('Should render with AttributeGroup > remove attribute', async () => {
    const removeAttribute = jest.fn();

    const wrapper = render(
      <AttributeGroup
        {...props}
        path="0.1"
        removeAttribute={removeAttribute}
        data={{
          operator: 'where',
          attributes: [
            {
              operator: '',
              item: { filterBy: 'Sys', operator: 'and', value: 'AA' },
              type: 'attribute'
            }
          ],
          type: 'group'
        }}
      />
    );

    const removeBtn = wrapper.baseElement.querySelector(
      '.icon.icon-close.color-red'
    );

    await waitFor(() => {
      removeBtn && userEvent.click(removeBtn);
    });
    jest.runAllTimers();
    expect(removeAttribute).toHaveBeenCalled();
  });
});
