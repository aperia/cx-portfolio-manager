import { fireEvent, render, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import 'app/utils/_mockComponent/mockCanvas';
import * as WorkflowPortfolioDefinitions from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowPortfolioDefinitions';
import { default as React } from 'react';
import Attribute from './Attribute';

const MockSelectWorkflowMethodsSelector = jest.spyOn(
  WorkflowPortfolioDefinitions,
  'useSelectElementMetadataForPortfolioDefinitionsSelector'
);

jest.mock('./ValueControl', () => ({
  __esModule: true,
  default: () => {
    return (
      <div>
        <div>ValueControl</div>
      </div>
    );
  }
}));

const props = {
  path: '',
  removeAttribute: jest.fn(),
  isGroup: false,
  attribute: {},
  onChange: jest.fn(),
  checkAttributeValid: jest.fn(),
  filterOptionsValid: []
};

describe('CreatePortfolioDefinitionsWorkflow > ConfigureParameters > AttributeSection > Attribute', () => {
  beforeEach(() => {
    MockSelectWorkflowMethodsSelector.mockReturnValue({
      'filter.by': [
        {
          text: 'Sys',
          code: 'Sys'
        },
        {
          text: 'Prin',
          code: 'Prin'
        },
        {
          text: 'BIN',
          code: 'BIN'
        },
        {
          text: 'Portfolio ID',
          code: 'Portfolio ID'
        },
        {
          text: 'State Code',
          code: 'State Code'
        },
        {
          text: 'Pricing Strategy',
          code: 'Pricing Strategy'
        },
        {
          text: 'Product Type Code',
          code: 'Product Type Code'
        },
        {
          text: 'Company ID',
          code: 'Company ID'
        },
        {
          text: 'Company Name',
          code: 'Company Name'
        },
        {
          text: 'Datalink Elements',
          code: 'Datalink Elements'
        },
        {
          text: 'Miscellaneous Fields 1-10-TX',
          code: 'Miscellaneous Fields 1-10-TX'
        }
      ],
      'sys.operators': [
        {
          text: 'equal',
          code: 'equal'
        }
      ]
    });
  });

  afterEach(() => {
    MockSelectWorkflowMethodsSelector.mockClear();
  });

  it('Should render width 3 fields > Filter Combobox Dropdown Operator and ValueControl', () => {
    const wrapper = render(<Attribute {...props} />);

    expect(wrapper.getByText('ValueControl')).toBeInTheDocument();
    const cbbIcon = wrapper.baseElement.querySelector(
      '.icon.icon-chevron-down'
    );
    expect(cbbIcon).toBeInTheDocument();
    const ddl = wrapper.container.querySelector(
      '.dls-dropdown-list .text-field-container'
    )!;
    expect(ddl).toBeInTheDocument();
  });

  it('Should render > Filter Item is disable when having max filter', () => {
    const newProps = {
      filterOptionsValid: [
        'Prin',
        'Sys',
        'prin2',
        'prin3',
        'prin4',
        'prin5',
        'prin6',
        'prin7',
        'prin8',
        'prin9',
        'prin10',
        'prin11'
      ],
      attribute: {
        filterBy: 'Sys',
        operator: 'equal',
        value: ''
      },
      path: '',
      removeAttribute: jest.fn(),
      isGroup: false,
      onChange: jest.fn(),
      checkAttributeValid: jest.fn()
    };
    const wrapper = render(<Attribute {...newProps} />);

    const cbbIcon = wrapper.baseElement.querySelector(
      '.icon.icon-chevron-down'
    );
    fireEvent.mouseDown(cbbIcon!);
    expect(
      wrapper.baseElement.querySelector('.item.dls-disabled')
    ).toBeInTheDocument();
  });

  it('Should render > Change Operator', () => {
    const onChange = jest.fn();

    const newProps = {
      ...props,
      filterOptionsValid: [
        'Prin',
        'Sys',
        'Agent',
        'prin3',
        'prin4',
        'prin5',
        'prin6',
        'prin7',
        'prin8'
      ],
      onChange,
      checkAttributeValid: jest.fn()
    };
    const wrapper = render(<Attribute {...newProps} />);
    const ddl = wrapper.container.querySelector(
      '.dls-dropdown-list .text-field-container'
    )!;
    expect(ddl).toBeInTheDocument();
  });

  it('Should render > Change Filter By', async () => {
    const onChange = jest.fn();
    const newProps = {
      ...props,
      attribute: {
        filterBy: 'Sys',
        operator: 'equal',
        value: ''
      },
      onChange,
      checkAttributeValid: jest.fn()
    };
    const wrapper = render(<Attribute {...newProps} />);
    jest.useFakeTimers();

    const cbbIcon = wrapper.baseElement.querySelector(
      '.icon.icon-chevron-down'
    );
    fireEvent.mouseDown(cbbIcon!);
    const sysItem = wrapper.getByText(/Sys/i);

    await waitFor(() => {
      userEvent.click(sysItem);
    });
    jest.runAllTimers();
    expect(onChange).toHaveBeenCalled();
  });

  it('Should render > Have Error Field', async () => {
    const onChange = jest.fn();
    const newProps = {
      ...props,
      attribute: {
        filterBy: 'Sys',
        operator: 'equal',
        value: ''
      },
      onChange,
      checkAttributeValid: jest.fn()
    };
    const wrapper = render(<Attribute {...newProps} />);
    jest.useFakeTimers();

    const cbbIcon = wrapper.baseElement.querySelector(
      '.icon.icon-chevron-down'
    );
    fireEvent.mouseDown(cbbIcon!);

    await waitFor(() => {
      fireEvent.blur(cbbIcon!);
    });
    jest.runAllTimers();

    expect(jest.spyOn(newProps, 'checkAttributeValid')).toBeCalled();
  });

  it('Should render > remove attribute', async () => {
    const removeAttribute = jest.fn();
    const newProps = {
      ...props,
      removeAttribute,
      attribute: {
        filterBy: 'Sys',
        operator: 'equal',
        value: ''
      },
      onChange: jest.fn(),
      checkAttributeValid: jest.fn()
    };
    const wrapper = render(<Attribute {...newProps} />);

    const buttonRemove = wrapper.baseElement.querySelector(
      '.btn.btn-icon-secondary'
    );

    await waitFor(() => {
      buttonRemove && userEvent.click(buttonRemove);
    });
    jest.runAllTimers();
    expect(removeAttribute).toHaveBeenCalled();
  });
});
