import { fireEvent, render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import * as WorkflowPortfolioDefinitions from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowPortfolioDefinitions';
import React from 'react';
import AttributeSection from './index';

const MockSelectWorkflowMethodsSelector = jest.spyOn(
  WorkflowPortfolioDefinitions,
  'useSelectElementMetadataForPortfolioDefinitionsSelector'
);

const props = {
  onChange: jest.fn(),
  setValid: jest.fn(),
  stepId: 'configureParameters diff',
  title: 'Configure Parameters',
  selectedStep: 'configureParameters',
  hasInstance: true,
  isEdit: false,
  isStuck: false,
  ready: true,
  savedAt: 1647669005153,
  totalAttribute: 0,
  handleSave: jest.fn(),
  setFormValues: jest.fn(),
  clearFormValues: jest.fn(),
  setNumberOfAttribute: jest.fn(),
  attributesValid: jest.fn(),
  checkAttributeValid: jest.fn(),
  checkAttributeGroupValid: jest.fn(),
  formValues: {
    isValid: true
  }
};

jest.mock('./AttributeGroup', () => {
  const AttributeGroup = ({ removeAttribute, onChange, ...props }: any) => {
    return (
      <>
        <div onClick={() => removeAttribute('1')}>remove</div>
        <div onClick={() => removeAttribute('1.0')}>removeFirstItem</div>
        <div onClick={() => removeAttribute('1.1.0')}>removeChildLv3</div>
        <div onClick={() => removeAttribute('2.0.0')}>removeChildAndParent</div>

        <div
          onClick={() =>
            onChange('operatorRelated', '1')({ target: { value: 'or' } })
          }
        >
          onchange
        </div>
        <div
          onClick={() =>
            onChange('filterBy', '1.1')({ target: { value: 'abc' } })
          }
        >
          onchangeFilter
        </div>
        <div
          onClick={() =>
            onChange('filterBy', '1.1.0')({ target: { value: 'Sys' } })
          }
        >
          onchangeChildAttribute
        </div>
      </>
    );
  };

  return {
    __esModule: true,
    default: AttributeGroup
  };
});

jest.mock('./SelectMethodAddButtons', () => {
  const MockDropDown = ({ onSelectAddMethod }: any) => {
    return (
      <>
        <div
          onClick={() =>
            onSelectAddMethod(
              {
                target: {
                  value: {
                    value: 'addAttribute',
                    code: 'addAttribute'
                  }
                }
              },
              '3'
            )
          }
        >
          addAttribute
        </div>
        <div
          onClick={() =>
            onSelectAddMethod(
              {
                target: {
                  value: {
                    value: 'addAttribute',
                    code: 'addAttribute'
                  }
                }
              },
              '1.1'
            )
          }
        >
          addAttributeWithPath
        </div>
        <div
          onClick={() =>
            onSelectAddMethod(
              {
                target: {
                  value: {
                    value: 'addAttributeGroup',
                    code: 'addAttributeGroup'
                  }
                }
              },
              '1.1'
            )
          }
        >
          addAttributeGroup
        </div>
        <div
          onClick={() =>
            onSelectAddMethod(
              {
                target: {
                  value: {
                    value: 'addAttributeGroup',
                    code: 'addAttributeGroup'
                  }
                }
              },
              '3'
            )
          }
        >
          addAttributeGroupInParentNode
        </div>
      </>
    );
  };

  MockDropDown.Item = () => <div>Mock Drop down item</div>;
  return {
    __esModule: true,
    default: MockDropDown
  };
});

const attributes = [
  {
    type: 'attribute',
    operator: '',
    item: {
      filterBy: 'Sys',
      operator: 'equal'
    }
  },
  {
    type: 'group',
    operator: 'and',
    attributes: [
      {
        type: 'attribute',
        operator: '',
        item: {
          filterBy: 'Sys',
          operator: 'equal'
        }
      },
      {
        type: 'group',
        operator: 'and',
        attributes: [
          {
            type: 'attribute',
            operator: '',
            item: {
              filterBy: 'Sys',
              operator: 'equal'
            }
          },
          {
            type: 'attribute',
            operator: '',
            item: {
              filterBy: 'Prin',
              operator: 'equal'
            }
          }
        ]
      }
    ]
  },
  {
    type: 'group',
    operator: 'and',
    attributes: [
      {
        type: 'group',
        operator: '',
        attributes: [
          {
            type: 'attribute',
            operator: 'and',
            item: {
              filterBy: 'Prin',
              operator: 'equal'
            }
          }
        ]
      }
    ]
  }
];

describe('CreatePortfolioDefinitionsWorkflow > ConfigureParameters > AttributeSection', () => {
  beforeEach(() => {
    MockSelectWorkflowMethodsSelector.mockReturnValue({
      'filter.by': [
        {
          text: 'Sys',
          code: 'Sys'
        },
        {
          text: 'Prin',
          code: 'Prin'
        }
      ],
      'sys.operators': [{ text: 'equal', code: 'equal' }]
    });
  });

  it('Should render with Add Attribute/Attribute Group Buttons', () => {
    const wrapper = render(<AttributeSection {...props} />);
    expect(
      wrapper.getByText('txt_portfolio_definitions_add_attribute')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_portfolio_definitions_add_attribute_group')
    ).toBeInTheDocument();
  });

  it('Add Attribute', () => {
    const wrapper = render(<AttributeSection {...props} />);

    fireEvent.click(
      wrapper.getByText('txt_portfolio_definitions_add_attribute')
    );
    expect(wrapper.getByText('addAttribute')).toBeInTheDocument();
  });

  it('Add Attribute second item', () => {
    const wrapper = render(
      <AttributeSection
        {...props}
        attributes={[
          {
            operator: '',
            type: 'attribute',
            item: {
              filterBy: 'Sys',
              operator: 'and',
              value: 'AA'
            }
          }
        ]}
      />
    );

    fireEvent.click(wrapper.getByText('addAttribute'));
    expect(wrapper.getByText('addAttribute')).toBeInTheDocument();
  });

  it('Add Attribute Group', () => {
    const wrapper = render(<AttributeSection {...props} />);
    fireEvent.click(
      wrapper.getByText('txt_portfolio_definitions_add_attribute_group')
    );
    expect(wrapper.getByText('addAttributeGroup')).toBeInTheDocument();
  });

  it('Should render with Attribute List > add attribute group with path', () => {
    const wrapper = render(
      <AttributeSection {...props} attributes={attributes} />
    );

    const mockDropDown = wrapper.getByText('addAttributeGroup');
    expect(mockDropDown).toBeInTheDocument();
    userEvent.click(mockDropDown);
  });
  it('Should render with Attribute List > add attribute group in parent node', () => {
    const wrapper = render(
      <AttributeSection {...props} attributes={attributes} />
    );

    const mockDropDown = wrapper.getByText('addAttributeGroupInParentNode');
    expect(mockDropDown).toBeInTheDocument();
    userEvent.click(mockDropDown);
  });

  it('Should render with Attribute List > add attribute with path', () => {
    const wrapper = render(
      <AttributeSection {...props} attributes={attributes} />
    );

    const mockDropDown = wrapper.getByText('addAttributeWithPath');
    expect(mockDropDown).toBeInTheDocument();
    userEvent.click(mockDropDown);
  });
  it('Should render with Attribute List > remove', () => {
    const wrapper = render(
      <AttributeSection {...props} attributes={attributes} />
    );

    const removeBtn = wrapper.queryAllByText('remove')[0];
    expect(removeBtn).toBeInTheDocument();
    userEvent.click(removeBtn);
  });

  it('Should render with Attribute List > remove first item in group', () => {
    const wrapper = render(
      <AttributeSection {...props} attributes={attributes} />
    );

    const removeBtn = wrapper.queryAllByText('removeFirstItem')[0];
    expect(removeBtn).toBeInTheDocument();
    userEvent.click(removeBtn);
  });

  it('Should render with Attribute List > remove Child Lv3 in group', () => {
    const wrapper = render(
      <AttributeSection {...props} attributes={attributes} />
    );

    const removeBtn = wrapper.queryAllByText('removeChildLv3')[0];
    expect(removeBtn).toBeInTheDocument();
    userEvent.click(removeBtn);
  });

  it('Should render with Attribute List > remove Child And Parent', () => {
    const wrapper = render(
      <AttributeSection {...props} attributes={attributes} />
    );

    const removeBtn = wrapper.queryAllByText('removeChildAndParent')[0];
    expect(removeBtn).toBeInTheDocument();
    userEvent.click(removeBtn);
  });

  it('Should render with Attribute List > onChange', () => {
    const wrapper = render(
      <AttributeSection {...props} attributes={attributes} />
    );

    const onChange = wrapper.queryAllByText('onchange')[0];
    expect(onChange).toBeInTheDocument();

    userEvent.click(onChange);
  });

  it('Should render with Attribute List > onChange filterBy', () => {
    const wrapper = render(
      <AttributeSection {...props} attributes={attributes} />
    );

    const onChange = wrapper.queryAllByText('onchangeFilter')[0];
    userEvent.click(onChange);
  });
  it('Should render with Attribute List > child attribute change', () => {
    const wrapper = render(
      <AttributeSection {...props} attributes={attributes} />
    );

    const onChange = wrapper.queryAllByText('onchangeChildAttribute')[0];
    expect(onChange).toBeInTheDocument();
    userEvent.click(onChange);
  });
});
