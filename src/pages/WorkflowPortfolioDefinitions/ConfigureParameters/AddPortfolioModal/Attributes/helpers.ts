import { isString } from 'lodash';

export const getKeyOperator = (text: string) => {
  if (!isString(text)) return '';
  return text
    .toLowerCase()
    .replace(/[^a-zA-Z0-9]/g, '.')
    .concat('.operators');
};
