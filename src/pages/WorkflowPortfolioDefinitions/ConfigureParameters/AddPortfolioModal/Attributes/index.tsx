import { AddAttributeButtons } from 'app/constants/enums';
import {
  DropdownButtonSelectEvent,
  Icon,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import { get, set, unset } from 'app/_libraries/_dls/lodash';
import { cloneDeep, isEmpty } from 'lodash';
import { useSelectElementMetadataForPortfolioDefinitionsSelector } from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowPortfolioDefinitions';
import React, { useState } from 'react';
import {
  DEFAULT_ATTRIBUTE,
  DEFAULT_ATTRIBUTE_GROUP,
  DEFAULT_FIRST_ATTRIBUTE,
  DEFAULT_FIRST_ATTRIBUTE_GROUP
} from '../../constant';
import { countTotalAttribute, findFilterValue } from '../../helpers';
import AttributeGroup, { AttributeItemGroupProps } from './AttributeGroup';
import { getKeyOperator } from './helpers';
import SelectMethodAddButtons from './SelectMethodAddButtons';

export interface AttributeItemProps {
  operator?: string;
  filterBy?: string;
  value?: number | string | MagicKeyValue;
}
export interface AttributeProps {
  operator: string;
  item?: AttributeItemProps;
  type: string;
}

export interface AttributeSectionProps extends WorkflowSetupProps {
  onChange: (e: any) => void;
  attributes?: (AttributeProps | AttributeItemGroupProps)[] | [];
  setNumberOfAttribute: (numberAttribute: number) => void;
  totalAttribute: number;
  attributesValid: {};
  checkAttributeValid: (path: string, isValid: boolean) => void;
  checkAttributeGroupValid: (leves: string[], node?: string) => void;
}
const AttributeSection: React.FC<AttributeSectionProps> = ({
  attributes: attributesProps = [],
  setNumberOfAttribute,
  totalAttribute,
  attributesValid,
  checkAttributeValid,
  checkAttributeGroupValid,
  ...props
}) => {
  const { t } = useTranslation();
  const metadata = useSelectElementMetadataForPortfolioDefinitionsSelector();

  const [attributes, setAttributes] = useState<
    (AttributeProps | AttributeItemGroupProps)[]
  >(cloneDeep(attributesProps));
  const [filterOptionsValid, setFilterOptionsValid] = useState<string[]>([]);

  const addAttribute = (path: string, isGroup?: boolean) => {
    const levels = path.split('.');
    const cloneAttributes = [...attributes];
    setNumberOfAttribute(totalAttribute + 1);

    if (!isGroup && levels.length === 1) {
      setAttributes(attributes => [
        ...attributes,
        cloneDeep(DEFAULT_ATTRIBUTE)
      ]);
      props.onChange([...cloneAttributes, cloneDeep(DEFAULT_ATTRIBUTE)]);
      return;
    }

    let pathGetDataArr = '';
    levels.forEach((level: string, index: number) => {
      if (index === 0) {
        pathGetDataArr = `${level}.attributes`;
        return;
      }

      pathGetDataArr = `${pathGetDataArr}.${level}.attributes`;
    });
    const attribute = get(cloneAttributes, pathGetDataArr);
    attribute.push(cloneDeep(DEFAULT_ATTRIBUTE));
    setAttributes(cloneAttributes);
    props.onChange(cloneAttributes);
  };

  const addAttributeGroup = (path: string, isGroup?: boolean) => {
    const levels = path.split('.');
    const cloneAttributes = [...attributes];
    setNumberOfAttribute(totalAttribute + 1);

    if (!isGroup && levels.length === 1) {
      setAttributes(attributes => [
        ...attributes,
        cloneDeep(DEFAULT_ATTRIBUTE_GROUP)
      ]);
      props.onChange([...cloneAttributes, cloneDeep(DEFAULT_ATTRIBUTE_GROUP)]);
      return;
    }

    let pathGetDataArr = '';
    levels.forEach((level: string, index: number) => {
      if (index === 0) {
        pathGetDataArr = `${level}.attributes`;
        return;
      }

      pathGetDataArr = `${pathGetDataArr}.${level}.attributes`;
    });

    const attribute = get(cloneAttributes, pathGetDataArr);
    attribute.push(cloneDeep(DEFAULT_ATTRIBUTE_GROUP));
    setAttributes(cloneAttributes);
    props.onChange(cloneAttributes);
  };

  const removeAttribute = (path: string) => {
    const cloneAttributes = cloneDeep(attributes);
    const levels: string[] = path.split('.');

    // count total item in attribute

    if (levels.length === 1) {
      cloneAttributes.splice(get(levels, '0'), 1);
      const totalAttribute = countTotalAttribute(cloneAttributes);
      setNumberOfAttribute(totalAttribute);
      if (!isEmpty(cloneAttributes)) {
        set(cloneAttributes, '0.operator', 'where');
      }
      setFilterOptionsValid(findFilterValue(cloneAttributes));
      checkAttributeGroupValid(levels);
      setAttributes([...cloneAttributes]);
      props.onChange([...cloneAttributes]);

      return;
    }

    const attributeTargetIdx: string = levels.pop() || '';
    const attributeTargetParent = levels.join('.attributes.');
    const parentNode = get(cloneAttributes, attributeTargetParent);
    parentNode['attributes'].splice(attributeTargetIdx, 1);
    checkAttributeGroupValid(levels, attributeTargetIdx);

    if (isEmpty(parentNode['attributes'])) {
      //loop when remove child item
      const parentPath: string = levels.join('.');
      removeAttribute(parentPath);
      return;
    }

    // change operator new first attribute
    set(parentNode, 'attributes.0.operator', '');
    set(cloneAttributes, attributeTargetParent, parentNode);
    const totalAttribute = countTotalAttribute(cloneAttributes);
    setNumberOfAttribute(totalAttribute);
    setAttributes(cloneAttributes);
    props.onChange(cloneAttributes);
  };

  const handleFormChange =
    (field: string, path: string, isRefData?: boolean) => (e: any) => {
      const value = e.target?.value;
      const cloneAttributes = JSON.parse(JSON.stringify([...attributes]));

      const levels = path.split('.');
      let attributeTarget = levels[0];

      for (let i = 0; i < levels.length; i++) {
        const attribute = get(cloneAttributes, `${attributeTarget}.attributes`);
        if (isEmpty(attribute) || i === levels.length - 1) break;

        attributeTarget = `${attributeTarget}.attributes.${levels[i + 1]}`;
      }

      if (field === 'operatorRelated') {
        set(cloneAttributes, `${attributeTarget}.operator`, value);
      } else {
        set(cloneAttributes, `${attributeTarget}.item.${field}`, value);
      }

      if (field === 'filterBy') {
        const keyOperator: string = getKeyOperator(value);
        const operatorOptions: RefData[] | [] = get(metadata, keyOperator, []);
        if (operatorOptions.length === 1) {
          const operatorValue = operatorOptions[0].code;
          set(
            cloneAttributes,
            `${attributeTarget}.item.operator`,
            operatorValue
          );
        }

        unset(cloneAttributes, `${attributeTarget}.item.value`);
        setFilterOptionsValid(findFilterValue(cloneAttributes));
      }

      setAttributes([...cloneAttributes]);
      props.onChange([...cloneAttributes]);
    };

  const AttributeButtons = () => {
    return (
      <div className="row mt-16">
        <div className="col-6 col-xl-4">
          <div
            className="rcc-btn d-flex justify-content-between border rounded-lg py-10"
            onClick={() => {
              setNumberOfAttribute(totalAttribute + 1);
              setAttributes([cloneDeep(DEFAULT_FIRST_ATTRIBUTE)]);
              props.onChange([cloneDeep(DEFAULT_FIRST_ATTRIBUTE)]);
            }}
          >
            <span className="d-flex align-items-center ml-16">
              <Icon name="method-add" size="9x" className="color-grey-l16" />
              <span className="ml-12 mr-8">
                {t('txt_portfolio_definitions_add_attribute')}
              </span>
              <Tooltip
                element={t('txt_portfolio_definitions_add_attribute_example')}
                triggerClassName="d-flex"
              >
                <Icon name="information" className="color-grey-l16" size="5x" />
              </Tooltip>
            </span>
          </div>
        </div>
        <div className="col-6 col-xl-4">
          <div
            className="rcc-btn d-flex justify-content-between border rounded-lg py-10"
            onClick={() => {
              setNumberOfAttribute(totalAttribute + 1);
              setAttributes([cloneDeep(DEFAULT_FIRST_ATTRIBUTE_GROUP)]);
              props.onChange([cloneDeep(DEFAULT_FIRST_ATTRIBUTE_GROUP)]);
            }}
          >
            <span className="d-flex align-items-center ml-16">
              <Icon
                name="knowledge-base"
                size="9x"
                className="color-grey-l16"
              />
              <span className="ml-12 mr-8">
                {t('txt_portfolio_definitions_add_attribute_group')}
              </span>
              <Tooltip
                element={t(
                  'txt_portfolio_definitions_add_attribute_group_example'
                )}
                triggerClassName="d-flex"
                containerClassName="pre-line"
              >
                <Icon name="information" className="color-grey-l16" size="5x" />
              </Tooltip>
            </span>
          </div>
        </div>
      </div>
    );
  };

  const onSelectAddMethod = (
    event: DropdownButtonSelectEvent,
    path: string,
    isGroup?: boolean
  ) => {
    const { value } = event.target.value;
    switch (value) {
      case AddAttributeButtons.ADD_ATTRIBUTE:
        addAttribute(path, isGroup);
        break;
      case AddAttributeButtons.ADD_ATTRIBUTE_GROUP:
        addAttributeGroup(path, isGroup);
        break;
    }
  };

  const renderAttributes = () => {
    return (
      <div className="mt-8">
        {attributes.map((item: any, idx) => {
          return (
            <AttributeGroup
              path={`${idx}`}
              key={idx}
              removeAttribute={removeAttribute}
              data={item}
              onSelectAddMethod={onSelectAddMethod}
              addAttribute={addAttribute}
              onChange={handleFormChange}
              filterOptionsValid={filterOptionsValid}
              checkAttributeValid={checkAttributeValid}
            />
          );
        })}
        <SelectMethodAddButtons
          onSelectAddMethod={onSelectAddMethod}
          path={`${attributes.length}`}
        />
      </div>
    );
  };

  return (
    <div className="mt-24">
      <h5>
        {t('txt_portfolio_definitions_attributes')}{' '}
        <span className="text-asterisk">*</span>
      </h5>
      {isEmpty(attributes) ? <AttributeButtons /> : renderAttributes()}
    </div>
  );
};

export default AttributeSection;
