import { fireEvent, render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React from 'react';
import SelectMethodAddButtons from './SelectMethodAddButtons';

describe('CreatePortfolioDefinitionsWorkflow > ConfigureParameters > AttributeSection > SelectMethodAddButtons', () => {
  it('Should render', () => {
    const props = {
      onSelectAddMethod: jest.fn(),
      path: ''
    };
    const wrapper = render(<SelectMethodAddButtons {...props} />);
    const dropdownButton = wrapper.container.querySelector(
      'button.dls-dropdown-button'
    ) as Element;
    expect(dropdownButton).not.toBeNull();

    userEvent.click(dropdownButton);
    expect(wrapper.getByText('txt_add')).toBeInTheDocument();
  });
  it('Should render > select method', () => {
    const props = {
      onSelectAddMethod: jest.fn(),
      path: ''
    };
    const wrapper = render(<SelectMethodAddButtons {...props} />);

    const dropdownButton = wrapper.container.querySelector(
      'button.dls-dropdown-button'
    ) as Element;
    expect(dropdownButton).not.toBeNull();

    userEvent.click(dropdownButton);

    expect(
      wrapper.getByText('txt_portfolio_definitions_add_attribute')
    ).toBeInTheDocument();
    fireEvent.click(
      wrapper.getByText('txt_portfolio_definitions_add_attribute')
    );
    expect(props.onSelectAddMethod).toHaveBeenCalled();
  });
});
