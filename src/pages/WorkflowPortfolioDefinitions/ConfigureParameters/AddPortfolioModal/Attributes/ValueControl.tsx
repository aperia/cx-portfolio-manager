import FieldTooltip from 'app/components/FieldTooltip';
import FormatTextBox from 'app/components/FormatTextBox';
import {
  ComboBox,
  NumericTextBox,
  TextBox,
  useTranslation
} from 'app/_libraries/_dls';
import get from 'lodash.get';
import { useSelectElementMetadataForPortfolioDefinitionsSelector } from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowPortfolioDefinitions';
import React, { FC } from 'react';
import { ATTRIBUTE_VALUE_FIELDS } from '../../constant';

export interface ValueControlProps {
  filterBy: string;
  value?: number | string | MagicKeyValue;
  path: string;
  errors: Record<string, IFormError>;
  onChange: (field: string, path: string) => (e: any) => void;
  setTouched: (field: string, isTouch?: boolean) => (e: MagicKeyValue) => void;
}

const ValueControl: FC<ValueControlProps> = ({
  filterBy,
  value = '',
  path,
  errors,
  onChange,
  setTouched
}) => {
  const { t } = useTranslation();
  const metadata = useSelectElementMetadataForPortfolioDefinitionsSelector();
  const {
    metadataKey,
    moreInfo: { title = '', description = '' } = {},
    typeControl,
    props
  } = ATTRIBUTE_VALUE_FIELDS[filterBy] || {};
  const options = get(metadata, metadataKey, []);

  switch (typeControl) {
    case 'combobox':
      return (
        <FieldTooltip
          placement="left-start"
          element={
            <div>
              {title && <div className="text-uppercase">{title}</div>}
              <p className="mt-8">{description}</p>
            </div>
          }
        >
          <ComboBox
            value={value}
            label={t('txt_value')}
            name="value"
            textField="text"
            onChange={onChange('value', path)}
            onFocus={setTouched('value')}
            onBlur={setTouched('value', true)}
            error={errors?.value}
            required
          >
            {options?.map((item: RefData) => (
              <ComboBox.Item key={item?.code} value={item} label={item?.text} />
            ))}
          </ComboBox>
        </FieldTooltip>
      );
    case 'textbox':
      return (
        <FieldTooltip
          keepShowOnDevice={false}
          placement="left-start"
          element={
            <div>
              {title && <div className="text-uppercase">{title}</div>}
              <p className="mt-8">{description}</p>
            </div>
          }
        >
          <TextBox
            {...props}
            label={t('txt_value')}
            onChange={onChange('value', path)}
            onFocus={setTouched('value')}
            onBlur={setTouched('value', true)}
            error={errors?.value}
            value={value}
            dataTestId="textbox_value"
          />
        </FieldTooltip>
      );
    case 'numeric':
      return (
        <FieldTooltip
          keepShowOnDevice={false}
          placement="left-start"
          element={
            <div>
              {title && <div className="text-uppercase">{title}</div>}
              <p className="mt-8">{description}</p>
            </div>
          }
        >
          <NumericTextBox
            {...props}
            label={t('txt_value')}
            onChange={onChange('value', path)}
            onFocus={setTouched('value')}
            onBlur={setTouched('value', true)}
            error={errors?.value}
            value={value}
            dataTestId="numeric_value"
          />
        </FieldTooltip>
      );
    case 'format-textbox':
      return (
        <FieldTooltip
          keepShowOnDevice={false}
          placement="left-start"
          element={
            <div>
              {title && <div className="text-uppercase">{title}</div>}
              <p className="mt-8">{description}</p>
            </div>
          }
        >
          <FormatTextBox
            {...props}
            label={t('txt_value')}
            onChange={onChange('value', path)}
            onFocus={setTouched('value')}
            onBlur={setTouched('value', true)}
            error={errors?.value}
            value={value}
            dataTestId="format_textbox_value"
          />
        </FieldTooltip>
      );

    default:
      return (
        <TextBox
          disabled
          label={t('txt_value')}
          dataTestId="textbox_value__disable"
        />
      );
  }
};

export default ValueControl;
