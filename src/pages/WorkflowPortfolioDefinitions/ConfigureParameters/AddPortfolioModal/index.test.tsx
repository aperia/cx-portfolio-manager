import userEvent from '@testing-library/user-event';
import { renderWithMockStore } from 'app/utils';
import 'app/utils/_mockComponent/mockModalRegistry';
import 'app/utils/_mockComponent/mockUseTranslation';
import mockDevice from 'app/utils/_mockHelperConstant/mockDevice';
import * as WorkflowPortfolioDefinitions from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowPortfolioDefinitions';
import React from 'react';
import * as reactRedux from 'react-redux';
import AddNewMethodModal from './index';

const mockOnConfirmParams = jest.fn();
jest.mock('app/hooks/useUnsavedChangesRedirect', () => {
  return {
    __esModule: true,
    useUnsavedChangesRedirect:
      () =>
      ({ onConfirm, onDiscard, onCancel }: any) => {
        onDiscard && onDiscard();
        onCancel && onCancel();
        onConfirm && onConfirm(mockOnConfirmParams());
      }
  };
});

jest.mock('./UploadIcon', () => ({
  __esModule: true,
  default: () => {
    return (
      <div>
        <div>UploadIcon</div>
      </div>
    );
  }
}));

jest.mock('./Attributes', () => {
  const AttributeSection = ({
    onChange,
    checkAttributeGroupValid,
    checkAttributeValid,
    setNumberOfAttribute,
    ...props
  }: any) => {
    return (
      <>
        <div onClick={() => onChange('1')}>changeAttribute</div>
        <div onClick={() => checkAttributeGroupValid('1')}>
          checkAttributeGroupValid
        </div>
        <div onClick={() => checkAttributeGroupValid(['1'], '1')}>
          checkAttributeGroupValidHavingNode
        </div>
        <div onClick={() => checkAttributeValid('1.0', true)}>
          checkAttributeValid
        </div>
        <div onClick={() => setNumberOfAttribute(4)}>setNumberOfAttribute</div>
      </>
    );
  };

  return {
    __esModule: true,
    default: AttributeSection
  };
});

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

const MockSelectWorkflowMethodsSelector = jest.spyOn(
  WorkflowPortfolioDefinitions,
  'useSelectElementMetadataForPortfolioDefinitionsSelector'
);

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');

describe('PortfolioDefinitionWorkflow > AddNewMethodModal', () => {
  const onClose = jest.fn();
  const mockDispatch = jest.fn();

  const defaultProps = {
    id: 'AddNewMethodModal',
    show: true,
    onClose,
    stepId: 'configureParameters diff',
    title: 'Configure Parameters',
    selectedStep: 'configureParameters',
    hasInstance: true,
    isEdit: false,
    isStuck: false,
    ready: true,
    savedAt: 1647669005153,
    handleSave: jest.fn(),
    setFormValues: jest.fn(),
    clearFormValues: jest.fn(),
    formValues: {
      isValid: true
    }
  };

  const method: any = {
    id: 'id',
    name: 'MTHMTCI',
    modeledFrom: {
      id: 'id1',
      name: 'MTHMTCI',
      versions: [{ id: 'id' }],
      index: 10
    },
    'portfolio.definitions.attributes': [
      {
        type: 'attribute',
        operator: '',
        item: {
          filterBy: 'Sys',
          operator: 'equal'
        }
      },
      {
        type: 'group',
        operator: 'and',
        attributes: [
          {
            type: 'attribute',
            operator: '',
            item: {
              filterBy: 'Sys',
              operator: 'equal'
            }
          },
          {
            type: 'attribute',
            operator: '',
            item: {
              filterBy: 'Prin',
              operator: 'equal'
            }
          }
        ]
      }
    ],
    methodType: 'NEWVERSION',
    comment: ''
  };

  const queryInputFromLabel = (tierLabel: HTMLElement) => {
    return tierLabel
      .closest(
        '.dls-input-container, .dls-numreric-container, .text-field-container'
      )
      ?.querySelector('input, .input, textarea') as HTMLInputElement;
  };

  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);

    mockDevice.isDevice = false;
    mockOnConfirmParams.mockReturnValue('');
    MockSelectWorkflowMethodsSelector.mockReturnValue({
      'filter.by': [
        {
          text: 'Sys',
          code: 'Sys'
        },
        {
          text: 'Prin',
          code: 'Prin'
        }
      ],
      'sys.operators': [{ text: 'equal', code: 'equal' }]
    });
  });

  afterEach(() => {
    onClose.mockClear();
  });

  it('should render AddPortfolioModal', async () => {
    mockDevice.isDevice = true;
    const wrapper = await renderWithMockStore(
      <AddNewMethodModal {...defaultProps} />
    );
    expect(
      wrapper.getByText('txt_portfolio_definitions_add_portfolion')
    ).toBeInTheDocument();

    const description = queryInputFromLabel(
      wrapper.getByText('txt_portfolio_definitions_name_label')
    );
    expect(description).toBeInTheDocument();

    userEvent.click(wrapper.getByText('txt_cancel'));
    expect(onClose).toBeCalled();
  });

  it('should render AddNewMethodModal form modeled method > check Duplicate Method', async () => {
    const wrapper = await renderWithMockStore(
      <AddNewMethodModal
        {...defaultProps}
        methodNames={['MTHMTCI1']}
        draftMethod={{
          ...method,
          modeledFrom: { ...method.modeledFrom, name: '' },
          methodType: 'MODELEDMETHOD',
          parameters: undefined as any
        }}
        method={{
          ...method,
          modeledFrom: { ...method.modeledFrom, name: '' },
          methodType: 'MODELEDMETHOD',
          parameters: undefined as any
        }}
        isCreate={true}
      />
    );

    const methodName = queryInputFromLabel(
      wrapper.getByText('txt_portfolio_definitions_name_label')
    );
    userEvent.clear(methodName);
    userEvent.type(methodName, 'MTHMTCI1');
    expect(methodName?.value).toEqual('MTHMTCI1');
    const checkAttributeValidAction = wrapper.getByText('checkAttributeValid');

    userEvent.click(checkAttributeValidAction);

    const checkAttributeGroupValidAction = wrapper.getByText(
      'checkAttributeGroupValid'
    );
    userEvent.click(checkAttributeGroupValidAction);

    const addBtn = wrapper.getByText('txt_add');

    userEvent.click(addBtn);
    expect(
      wrapper.getByText(
        'txt_portfolio_definitions_validation_name_must_be_unique'
      )
    ).toBeInTheDocument();
  });

  it('should render AddNewMethodModal form modeled method > check Attribute Valid', async () => {
    const wrapper = await renderWithMockStore(
      <AddNewMethodModal
        {...defaultProps}
        methodNames={['MTHMTCI1']}
        draftMethod={{
          ...method,
          modeledFrom: { ...method.modeledFrom, name: '' },
          methodType: 'MODELEDMETHOD',
          parameters: undefined as any,
          name: 'MTHMTCI1',
          'portfolio.definitions.attributes': [
            {
              operator: 'where',
              item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
              type: 'attribute'
            },
            {
              operator: 'and',
              item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
              type: 'attribute'
            },
            {
              operator: 'and',
              item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
              type: 'attribute'
            }
          ]
        }}
        method={{
          ...method,
          modeledFrom: { ...method.modeledFrom, name: '' },
          methodType: 'MODELEDMETHOD',
          parameters: undefined as any,
          name: 'MTHMTCI',
          'portfolio.definitions.attributes': [
            {
              operator: 'where',
              item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
              type: 'attribute'
            },
            {
              operator: 'and',
              item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
              type: 'attribute'
            },
            {
              operator: 'and',
              item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
              type: 'attribute'
            }
          ]
        }}
        isCreate={false}
      />
    );

    const methodName = queryInputFromLabel(
      wrapper.getByText('txt_portfolio_definitions_name_label')
    );
    userEvent.clear(methodName);
    userEvent.type(methodName, 'MTHMTCI2');
    expect(methodName?.value).toEqual('MTHMTCI2');
    const checkAttributeValidAction = wrapper.getByText('checkAttributeValid');

    userEvent.click(checkAttributeValidAction);

    const checkAttributeGroupValidAction = wrapper.getByText(
      'checkAttributeGroupValidHavingNode'
    );
    userEvent.click(checkAttributeGroupValidAction);
    const addBtn = wrapper.baseElement.querySelector('.btn.btn-primary')!;

    userEvent.click(addBtn);
  });
});
