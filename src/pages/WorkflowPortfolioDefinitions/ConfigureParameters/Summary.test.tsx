import { render, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import * as CommonRedux from 'pages/_commons/redux/Common/select-hooks';
import * as selectorMock from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowPortfolioDefinitions';
import React from 'react';
import Summary from './Summary';

jest.mock('./SubRowGrid', () => ({
  __esModule: true,
  default: () => <div>SubRowGrid_component</div>
}));

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const mockUseSelectWindowDimension = jest.spyOn(
  CommonRedux,
  'useSelectWindowDimension'
);

const file = new File(['test'], 'test.png', {
  type: 'png'
});

global.URL.createObjectURL = jest.fn();

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <Summary {...props} />
    </div>
  );
};

jest.spyOn(
  selectorMock,
  'useSelectElementMetadataForPortfolioDefinitionsSelector'
);

describe('pages > Create Portfolio Definitions > ConfigureParameters > Summary', () => {
  beforeEach(() => {
    const mockUseSelectWindowDimensionImplementation = (width: number) => {
      mockUseSelectWindowDimension.mockImplementation(() => ({ width } as any));
    };
    mockUseSelectWindowDimensionImplementation(900);
  });

  afterEach(() => {
    mockUseSelectWindowDimension.mockClear();
  });

  it('Should render a grid with no content', () => {
    const props = {
      formValues: {
        isValid: true
      },
      onEditStep: jest.fn(),
      stepId: 1,
      selectedStep: 2
    };

    renderComponent(props);
  });

  it('Should render a grid with no content', () => {
    const props = {
      formValues: {
        isValid: true,
        methods: [
          {
            methodType: 'NEWMETHOD',
            name: '121233',
            parameters: [],
            [MethodFieldParameterEnum.PortfolioDefinitionsAttributes]: [
              {
                item: {
                  filterBy: 'acb',
                  operator: 'contains',
                  value: '121233'
                },
                type: 'attribute',
                operator: 'and'
              }
            ],
            [MethodFieldParameterEnum.PortfolioDefinitionsIcon]: file,
            rowId: 2,
            serviceSubjectSection: 'CP PF LC',
            versionParameters: []
          },
          {
            methodType: 'NEWMETHOD',
            name: '121233',
            parameters: [],
            [MethodFieldParameterEnum.PortfolioDefinitionsAttributes]: [
              {
                item: {
                  filterBy: 'acb',
                  operator: 'contains',
                  value: '121233'
                },
                type: 'attribute',
                operator: 'and'
              }
            ],
            [MethodFieldParameterEnum.PortfolioDefinitionsIcon]: undefined,
            rowId: 2,
            serviceSubjectSection: 'CP PF LC',
            versionParameters: []
          },
          {
            rowId: 2,
            name: '121233'
          },
          {
            rowId: 2,
            name: '121233'
          },
          {
            rowId: 2,
            name: '121233'
          },
          {
            rowId: 2,
            name: '121233'
          },
          {
            rowId: 2,
            name: '121233'
          },
          {
            rowId: 2,
            name: '121233'
          },
          {
            rowId: 2,
            name: '121233'
          },
          {
            rowId: 2,
            name: '121233'
          },
          {
            rowId: 2,
            name: '121233'
          }
        ]
      },
      onEditStep: jest.fn(),
      stepId: 1,
      selectedStep: 1
    };

    const wrapper = renderComponent(props);

    //Click expand
    const icon = wrapper.container.querySelector('.icon-plus') as HTMLElement;
    userEvent.click(icon);
    userEvent.click(icon);

    //Click to onEdit
    const edit = wrapper.queryAllByText('txt_edit');
    userEvent.click(edit[0]);
  });
});
