import { methodParamsToObject } from 'app/helpers';
import { useTranslation } from 'app/_libraries/_dls';
import classNames from 'classnames';
import find from 'lodash.find';
import get from 'lodash.get';
import { useSelectElementMetadataForPortfolioDefinitionsSelector } from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowPortfolioDefinitions';
import React from 'react';
import { getKeyOperator } from './AddPortfolioModal/Attributes/helpers';
import { upperFirstCharacter } from './helpers';

export interface ParameterValueType {}

export interface SubGridProps {
  original: WorkflowSetupMethod;
}

const AttributeReview = ({ attribute }: any) => {
  const { t } = useTranslation();
  const metadata = useSelectElementMetadataForPortfolioDefinitionsSelector();
  const {
    operator: operatorRelated = '',
    item: { filterBy = '', operator = '', value = '' } = {}
  } = attribute;

  const operatorRelatedText = upperFirstCharacter(operatorRelated);

  const keyOperator: string = getKeyOperator(filterBy);
  const operatorOptions: any[] = get(metadata, keyOperator, []);
  const operatorValue = find(operatorOptions, { code: operator });
  const operatorText = get(operatorValue, 'text', operator);

  const valueText = value?.text || value;

  const classNameAttribute = ['where', ''].includes(operatorRelated)
    ? ''
    : 'border-top mt-8 pt-8';

  return (
    <div className={classNames(`condition ${classNameAttribute}`)}>
      <div className="row">
        <div className="form-group-static col flex-100 pr-8 mt-0">
          <p className="pt-20">{operatorRelatedText}</p>
        </div>
        <div className="col pl-16">
          <div className="row mti-16">
            <div className="form-group-static col">
              <div className="form-group-static__label">
                {t('txt_filter_by')}
              </div>
              <div className="form-group-static__text">{filterBy}</div>
            </div>
            <div className="form-group-static col">
              <div className="form-group-static__label">
                {t('txt_manage_account_memos_operator')}
              </div>
              <div className="form-group-static__text">{operatorText}</div>
            </div>
            <div className="form-group-static col-12 col-xl-6">
              <div className="form-group-static__label">{t('txt_value')}</div>
              <div className="form-group-static__text">{valueText}</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
const AttributeGroupReview = ({ attributeGroup, path }: any) => {
  const { operator = '', attributes, type = '' } = attributeGroup;

  if (type === 'attribute') {
    return <AttributeReview path={path} attribute={attributeGroup} />;
  }

  const level = path.split('.').length;

  const classNameSub =
    level === 2
      ? 'condition-sub-item bg-light-l16 ml-m22'
      : 'condition-sub-sub-item bg-light-l20 ml-m20';

  const operatorRelatedText = upperFirstCharacter(operator);

  return (
    <div
      className={classNames('condition-item condition-item-group', {
        'border-top mt-8 pt-8': !['where', ''].includes(operator)
      })}
    >
      <div className="flex-shrink-0 w-110 mt-36">
        <p>{operatorRelatedText}</p>
      </div>
      <div className={classNames(`${classNameSub} p-16 rounded-lg flex-1`)}>
        {attributes.map((attributes: any, idx: number) => {
          return (
            <AttributeGroupReview
              key={idx}
              index={idx}
              path={`${path}.${idx}`}
              attributeGroup={attributes}
            />
          );
        })}
      </div>
    </div>
  );
};

const SubRowGrid: React.FC<SubGridProps> = ({ original }) => {
  const { t } = useTranslation();
  const convertedObject = methodParamsToObject(original);
  return (
    <div className="p-20 overflow-hidden summary-content">
      <h5 className="mb-16">{t('txt_portfolio_definitions_attributes')}</h5>
      {get(convertedObject, ['portfolio.definitions.attributes']).map(
        (attribute: any, idx: number) => {
          return (
            <AttributeGroupReview
              key={idx}
              attributeGroup={attribute}
              path={`${idx}`}
            />
          );
        }
      )}
    </div>
  );
};

export default SubRowGrid;
