import { MethodFieldParameterEnum } from 'app/constants/enums';
import {
  getWorkflowSetupMethodType,
  getWorkflowSetupStepStatus
} from 'app/helpers';
import { PortfolioDefinitionsFormValue } from '.';
import { convertBase64ToFile } from './helpers';

const parseFormValues: WorkflowSetupStepFormDataFunc<PortfolioDefinitionsFormValue> =
  (data, id) => {
    const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);

    if (!stepInfo) return;

    const methods = (data.data.configurations?.porfolioList || []).map(
      (method: any, idx: number) =>
        ({
          ...method,
          id: '', // Remove Id due to error 500 when update with methodId
          methodType: getWorkflowSetupMethodType(method),
          rowId: idx,
          name: method.portfolioName,
          [MethodFieldParameterEnum.PortfolioDefinitionsAttributes]:
            method.portfolioAtributes,
          portfolioIcon:
            method.portfolioIcon &&
            convertBase64ToFile(
              method.portfolioIcon?.value,
              method.portfolioIcon?.name
            )
        } as WorkflowSetupMethod & { rowId?: number })
    );
    return {
      ...getWorkflowSetupStepStatus(stepInfo),
      isValid: methods.length > 0,
      methods
    };
  };

export default parseFormValues;
