import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import {
  MethodFieldParameterEnum,
  ServiceSubjectSection
} from 'app/constants/enums';
import { PORTFOLIO_ATTRIBUTE_FIELDS } from 'app/constants/mapping';
import {
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { mapGridExpandCollapse, matchSearchValue } from 'app/helpers';
import { usePaginationGrid, useUnsavedChangeRegistry } from 'app/hooks';
import {
  Button,
  ColumnType,
  Grid,
  InlineMessage,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import { DEFAULT_PAGE_SIZE } from 'app/_libraries/_dls/components/Pagination/constants';
import { isEmpty, orderBy } from 'lodash';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import ClearAndResetButton from 'pages/_commons/Utils/ClearAndResetButton';
import { formatText } from 'pages/_commons/Utils/formatGridField';
import Paging from 'pages/_commons/Utils/Paging';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';
import ActionButtons from './ActionButtons';
import AddPortfolioModal from './AddPortfolioModal';
import { ICON_DEFAULT_PATH } from './constant';
import { searchAttribute } from './helpers';
import parseFormValues from './parseFormValues';
import RemoveMethodModal from './RemoveMethodModal';
import SubRowGrid from './SubRowGrid';
import Summary from './Summary';

export interface PortfolioDefinitionsFormValue {
  isValid?: boolean;

  methods?: (WorkflowSetupMethod & { rowId?: number })[];
}
/* istanbul ignore next */
const ConfigurateParameterStep: React.FC<
  WorkflowSetupProps<PortfolioDefinitionsFormValue>
> = props => {
  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();
  const { stepId, selectedStep, savedAt, formValues, isStuck, setFormValues } =
    props;
  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;
  const simpleSearchRef = useRef<any>(null);
  const [searchValue, setSearchValue] = useState('');

  const dispatch = useDispatch();
  const [expandedList, setExpandedList] = useState<string[]>([]);
  const [showAddModal, setShowAddModal] = useState(false);

  const [removeMethodRowId, setRemoveMethodRowId] =
    useState<undefined | number>(undefined);
  const [editMethodRowId, setEditMethodRowId] =
    useState<undefined | number>(undefined);

  const [hasChange, setHasChange] = useState(false);

  const methods = useMemo(
    () => orderBy(formValues?.methods || [], ['name'], ['asc']),
    [formValues?.methods]
  );
  const methodNames = useMemo(() => methods.map(i => i.name), [methods]);

  const prevExpandedList = useRef<string[]>();

  useEffect(() => {
    if (!searchValue && simpleSearchRef?.current) {
      simpleSearchRef.current.clear();
    }
  }, [searchValue]);

  useEffect(() => {
    if (stepId !== selectedStep) {
      setSearchValue('');
      simpleSearchRef?.current && simpleSearchRef.current.clear();
      setExpandedList([]);
      onPageChange(1);
      onPageSizeChange(DEFAULT_PAGE_SIZE);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [stepId, selectedStep]);

  useEffect(() => {
    prevExpandedList.current = expandedList;
  }, [expandedList]);

  const handleClearFilter = () => setSearchValue('');
  const handleSearch = (val: string) => setSearchValue(val);

  const onRemoveMethod = useCallback((idx: number) => {
    setRemoveMethodRowId(idx);
  }, []);

  const onEditMethod = useCallback((idx: number) => {
    setEditMethodRowId(idx);
  }, []);

  const onClickAddPortfolio = () => {
    setShowAddModal(true);
  };

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__PORTFOLIO__DEFINITIONS__CONFIGURE_PARAMETERS,
      priority: 1
    },
    [hasChange]
  );

  const columns: ColumnType[] = useMemo(
    () => [
      {
        id: 'methodName',
        Header: t('txt_portfolio_definitions_name_label'),
        accessor: formatText(['name']),
        width: width < 1280 ? 250 : undefined
      },
      {
        id: 'icon',
        Header: t('txt_portfolio_definitions_icon_label'),

        width: width < 1280 ? 180 : undefined,
        cellBodyProps: { className: 'pt-10' },
        accessor: data => {
          const file = data[MethodFieldParameterEnum.PortfolioDefinitionsIcon];
          if (!file) {
            return (
              <Tooltip
                placement="top"
                element={t('txt_portfolio_definitions_default_icon')}
              >
                <div className="box-portfolio-icon">
                  <img
                    className="rounded-sm"
                    src={ICON_DEFAULT_PATH}
                    alt="icon-default"
                  />
                </div>
              </Tooltip>
            );
          }

          return (
            <div className="box-portfolio-icon">
              <img
                className="rounded-sm"
                height={24}
                width={48}
                src={URL.createObjectURL(file)}
              />
            </div>
          );
        }
      },
      {
        id: 'actions',
        Header: t('txt_actions'),
        accessor: data => (
          <div className="d-flex justify-content-center">
            <Button
              size="sm"
              variant="outline-primary"
              onClick={() => onEditMethod(data.rowId)}
            >
              {t('txt_edit')}
            </Button>
            <Button
              size="sm"
              variant="outline-danger"
              className="ml-8"
              onClick={() => onRemoveMethod(data.rowId)}
            >
              {t('txt_delete')}
            </Button>
          </div>
        ),
        className: 'text-center',
        cellBodyProps: { className: 'control' },
        width: 142
      }
    ],
    [t, onEditMethod, onRemoveMethod, width]
  );

  useEffect(() => {
    dispatch(
      actionsWorkflowSetup.getWorkflowMetadata(PORTFOLIO_ATTRIBUTE_FIELDS)
    );
    dispatch(
      actionsWorkflowSetup.getBulkArchivePCFWorkflowStrategies({
        strategyArea: 'CP'
      })
    );
  }, [dispatch]);

  const data: any = useMemo(() => {
    /* istanbul ignore else */
    if (searchValue) {
      setExpandedList([]);
    }
    return methods.filter((method: any) => {
      if (matchSearchValue([method.name], searchValue)) return true;
      return searchAttribute(
        method[MethodFieldParameterEnum.PortfolioDefinitionsAttributes],
        searchValue
      );
    });
  }, [searchValue, methods]);

  const {
    total,
    currentPage,
    currentPageSize,
    pageData,
    onPageChange,
    onPageSizeChange
  } = usePaginationGrid(data);

  useEffect(() => {
    const isValid = (formValues?.methods?.length || 0) > 0;
    if (formValues.isValid === isValid) return;

    keepRef.current.setFormValues({ isValid });
  }, [formValues]);

  useEffect(() => {
    setHasChange(false);
  }, [stepId, selectedStep, savedAt]);

  const handleOnExpand = (dataKeyList: string[]) => {
    setExpandedList(isEmpty(dataKeyList) ? [] : dataKeyList);
  };

  const renderGrid = useMemo(() => {
    if (isEmpty(pageData) && !searchValue) return null;
    if (isEmpty(pageData) && searchValue)
      return (
        <div className="d-flex flex-column justify-content-center mt-24 mb-16">
          <h5>{t('txt_portfolio_definitions_list_title')}</h5>
          <ActionButtons
            small={!isEmpty(methods)}
            onClickAddPortfolio={onClickAddPortfolio}
          />
          <NoDataFound
            id="newRecord_notfound"
            hasSearch
            title={t('txt_no_results_found')}
            linkTitle={t('txt_clear_and_reset')}
            onLinkClicked={handleClearFilter}
          />
        </div>
      );

    return (
      <>
        <div className="d-flex align-items-center justify-content-between mt-24 mb-16">
          <h5>{t('txt_portfolio_definitions_list_title')}</h5>
          <SimpleSearch
            ref={simpleSearchRef}
            placeholder={t('txt_type_to_search')}
            onSearch={handleSearch}
            popperElement={
              <>
                <p className="color-grey">{t('txt_search_description')}</p>
                <ul className="search-field-item list-unstyled">
                  <li className="mt-16">{t('txt_filter_by')}</li>
                  <li className="mt-16">
                    {t('txt_portfolio_definitions_name_label')}
                  </li>
                  <li className="mt-16">{t('txt_value')}</li>
                </ul>
              </>
            }
          />
        </div>
        <div className="d-flex justify-content-end">
          <ActionButtons
            small={!isEmpty(methods)}
            onClickAddPortfolio={onClickAddPortfolio}
          />
          {searchValue && !isEmpty(pageData) && (
            <div className="ml-16 mr-n8">
              <ClearAndResetButton small onClearAndReset={handleClearFilter} />
            </div>
          )}
        </div>

        <div className="mt-16">
          <Grid
            togglable
            columns={columns}
            data={pageData}
            subRow={({ original }: any) => <SubRowGrid original={original} />}
            dataItemKey="rowId"
            toggleButtonConfigList={pageData.map(
              mapGridExpandCollapse('rowId', t)
            )}
            expandedItemKey={'rowId'}
            expandedList={expandedList}
            onExpand={handleOnExpand}
            scrollable
            className="grid-has-control grid-has-tooltip center"
          />
          {total > 10 && (
            <Paging
              totalItem={total}
              pageSize={currentPageSize}
              page={currentPage}
              onChangePage={onPageChange}
              onChangePageSize={onPageSizeChange}
            />
          )}
        </div>
      </>
    );
  }, [
    columns,
    currentPage,
    currentPageSize,
    expandedList,
    methods,
    onPageChange,
    onPageSizeChange,
    pageData,
    searchValue,
    t,
    total
  ]);

  return (
    <div>
      <div className="mt-8 color-grey">
        <p>{t('txt_portfolio_definitions_parameters_1')}</p>
      </div>
      {isStuck && (
        <InlineMessage className="mb-8 mt-24" variant="danger" withIcon>
          {t('txt_step_stuck_move_forward_message')}
        </InlineMessage>
      )}

      {isEmpty(pageData) && !searchValue && (
        <ActionButtons
          small={!isEmpty(methods)}
          onClickAddPortfolio={onClickAddPortfolio}
        />
      )}
      {renderGrid}
      {removeMethodRowId !== undefined && (
        <RemoveMethodModal
          id="removeMethod"
          show
          methodName={methods.find(i => i.rowId === removeMethodRowId)?.name}
          onClose={method => {
            if (method) {
              setFormValues({
                methods: methods.filter(i => i.rowId !== removeMethodRowId)
              });
              setHasChange(true);
            }
            setRemoveMethodRowId(undefined);
          }}
        />
      )}
      {editMethodRowId !== undefined && (
        <AddPortfolioModal
          id="addNewMethod"
          show
          isCreate={false}
          method={methods.find(i => i.rowId === editMethodRowId)}
          methodNames={methodNames}
          onClose={(method, isBack) => {
            if (method) {
              method.serviceSubjectSection = ServiceSubjectSection.LC;
              const newMethds = methods.map(item => {
                if (item.rowId === editMethodRowId) return method;
                return item;
              });
              setFormValues({ methods: newMethds });
              setHasChange(true);
              handleClearFilter();
            }
            setEditMethodRowId(undefined);
          }}
          {...props}
        />
      )}

      {showAddModal && (
        <AddPortfolioModal
          id="addNewMethod"
          show
          methodNames={methodNames}
          onClose={method => {
            if (method) {
              const rowId = Date.now();
              const newMethods = orderBy(
                [
                  {
                    ...method,
                    rowId
                  },
                  ...methods
                ],
                ['name'],
                ['asc']
              );
              setFormValues({
                methods: newMethods
              });
              setExpandedList([rowId as any]);
              setHasChange(true);
              const newMethodIdx = newMethods.findIndex(m => m.rowId === rowId);
              onPageChange(Math.ceil((newMethodIdx + 1) / currentPageSize));
              handleClearFilter();
            }
            setShowAddModal(false);
          }}
          {...props}
        />
      )}
    </div>
  );
};

const ExtraStaticConfigurateParameterStep =
  ConfigurateParameterStep as WorkflowSetupStaticProp;

ExtraStaticConfigurateParameterStep.defaultValues = {
  isValid: false,
  methods: []
};
ExtraStaticConfigurateParameterStep.summaryComponent = Summary;

ExtraStaticConfigurateParameterStep.parseFormValues = parseFormValues;

export default ExtraStaticConfigurateParameterStep;
