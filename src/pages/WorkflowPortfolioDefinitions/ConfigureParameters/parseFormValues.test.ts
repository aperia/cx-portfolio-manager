import {
  MethodFieldParameterEnum,
  ServiceSubjectSection
} from 'app/constants/enums';
import parseFormValues from './parseFormValues';

describe('AssignPricingStrategiesWorkFlow > ConfigureParametersStep > parseFormValues', () => {
  it('Should return undefined', () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '2'
            }
          ],
          configurations: {
            porfolioList: [] as any
          }
        }
      } as WorkflowSetupInstance,
      id: '1'
    };

    parseFormValues(props.data, props.id, jest.fn());
  });

  it('Should return undefined', () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '2'
            }
          ],
          configurations: {
            porfolioList: undefined as any
          }
        }
      } as WorkflowSetupInstance,
      id: '2'
    };

    parseFormValues(props.data, props.id, jest.fn());
  });

  it('Should return with data', () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '2'
            }
          ],
          configurations: {
            porfolioList: [
              {
                methodType: 'NEWMETHOD',
                name: '121233',
                parameters: [],
                [MethodFieldParameterEnum.PortfolioDefinitionsAttributes]: [
                  {
                    filterBy: 'acb',
                    operator: 'contains',
                    operatorRelated: 'as',
                    value: '121233',
                    attributes: [
                      {
                        id: '1',
                        operatorRelated: 'as',
                        filterBy: '',
                        operator: ''
                      },
                      { id: '2' }
                    ]
                  }
                ],
                [MethodFieldParameterEnum.PortfolioDefinitionsIcon]: {
                  value: '123',
                  name: '123'
                },
                rowId: 2,
                serviceSubjectSection: ServiceSubjectSection.DLO,
                versionParameters: []
              }
            ] as any
          }
        }
      } as WorkflowSetupInstance,
      id: '2'
    };

    parseFormValues(props.data, props.id, jest.fn());
  });
});
