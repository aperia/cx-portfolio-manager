import { waitFor } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import { mockUseDispatchFnc, renderWithMockStore } from 'app/utils';
import * as CommonRedux from 'pages/_commons/redux/Common/select-hooks';
import React from 'react';
import ConfigureParametersStep from '.';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/utils/MockView')
);
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});
jest.mock('app/hooks/useUnsavedChangeRegistry', () => {
  return {
    __esModule: true,
    useUnsavedChangeRegistry: (options: any, changes: any, onConfirm: any) => {
      onConfirm && onConfirm();
    }
  };
});
const mockOnCloseWithoutMethod = jest.fn();

jest.mock('./AddPortfolioModal', () => ({
  __esModule: true,
  default: ({ onClose }: any) => {
    const handleOnClose = () => {
      if (mockOnCloseWithoutMethod()) {
        onClose();
        return;
      }

      onClose(
        {
          id: '2',
          name: 'method-2',
          rowId: 1,
          versions: [
            {
              id: 'version 1.0'
            }
          ]
        },
        true
      );
    };
    return <div onClick={handleOnClose}>AddNewMethodModal_component</div>;
  }
}));

jest.mock('./RemoveMethodModal', () => ({
  __esModule: true,
  default: ({ onClose }: any) => {
    const handleOnClose = () => {
      if (mockOnCloseWithoutMethod()) {
        onClose();
        return;
      }

      onClose(
        {
          id: '1',
          name: 'method-1',
          rowId: 1,
          versions: [
            {
              id: 'version 1.0'
            }
          ]
        },
        true
      );
    };
    return <div onClick={handleOnClose}>RemoveMethodModal_component</div>;
  }
}));
const mockUseSelectWindowDimension = jest.spyOn(
  CommonRedux,
  'useSelectWindowDimension'
);
const mockUseDispatch = mockUseDispatchFnc();
const renderComponent = (props: any) => {
  return renderWithMockStore(<ConfigureParametersStep {...props} />, {
    initialState: {
      workflowSetup: {
        elementMetadata: {
          elements: [
            {
              id: '1',
              name: 'minimum.pay.due.option',
              options: [
                {
                  description: 'Do not use reasonable amount fee processing.',
                  value: '0'
                },
                {
                  description: 'Do not use reasonable amount fee processing.',
                  value: '1'
                }
              ],
              workflowTemplateId: '36'
            },
            {
              id: '2',
              name: 'returned.check.charge.option',
              options: [
                {
                  description: 'Do not use reasonable amount fee processing.',
                  value: '0'
                },
                {
                  description: 'Do not use reasonable amount fee processing.',
                  value: '1'
                }
              ],
              workflowTemplateId: '36'
            }
          ]
        },
        workflowStrategies: {
          strategies: [
            {
              effectiveDate: '2022-10-24T00:00:00',
              numberOfVersions: 0,
              relatedPricingStrategies: 0,
              strategyName: 'ZO8Q6J4C',
              versions: ['effectiveDate']
            },
            {
              effectiveDate: '2022-01-28T00:00:00',
              numberOfVersions: 0,
              relatedPricingStrategies: 0,
              strategyName: 'BOG3JQFG',
              versions: ['effectiveDate']
            }
          ]
        }
      } as any
    } as AppState
  });
};

const file = new File(['test'], 'test.txt', {
  type: 'text/plain'
});

global.URL.createObjectURL = jest.fn();

const mockUseSelectWindowDimensionImplementation = (width: number) => {
  mockUseSelectWindowDimension.mockImplementation(() => ({ width } as any));
};

const methods = [
  {
    methodType: 'NEWMETHOD',
    name: '123',
    parameters: [],
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributes]: [
      {
        operator: 'where',
        item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
        type: 'attribute'
      },
      {
        operator: 'and',
        item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
        type: 'attribute'
      },
      {
        operator: 'and',
        item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
        type: 'attribute'
      }
    ],
    [MethodFieldParameterEnum.PortfolioDefinitionsIcon]: file,
    rowId: 1,
    serviceSubjectSection: 'CP PF LC',
    versionParameters: []
  },
  {
    methodType: 'NEWMETHOD',
    name: '121233',
    parameters: [],
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributes]: [
      {
        operator: 'where',
        item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
        type: 'attribute'
      },
      {
        operator: 'and',
        item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
        type: 'attribute'
      },
      {
        operator: 'and',
        item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
        type: 'attribute'
      }
    ],
    [MethodFieldParameterEnum.PortfolioDefinitionsIcon]: file,
    rowId: 2,
    serviceSubjectSection: 'CP PF LC',
    versionParameters: []
  },
  {
    methodType: 'NEWMETHOD',
    name: '123',
    parameters: [],
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributes]: [
      {
        operator: 'where',
        item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
        type: 'attribute'
      },
      {
        operator: 'and',
        item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
        type: 'attribute'
      },
      {
        operator: 'and',
        item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
        type: 'attribute'
      }
    ],
    [MethodFieldParameterEnum.PortfolioDefinitionsIcon]: file,
    rowId: 1,
    serviceSubjectSection: 'CP PF LC',
    versionParameters: []
  },
  {
    methodType: 'NEWMETHOD',
    name: '121233',
    parameters: [],
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributes]: [
      {
        operator: 'where',
        item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
        type: 'attribute'
      },
      {
        operator: 'and',
        item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
        type: 'attribute'
      },
      {
        operator: 'and',
        item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
        type: 'attribute'
      }
    ],
    [MethodFieldParameterEnum.PortfolioDefinitionsIcon]: file,
    rowId: 2,
    serviceSubjectSection: 'CP PF LC',
    versionParameters: []
  },
  {
    methodType: 'NEWMETHOD',
    name: '123',
    parameters: [],
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributes]: [
      {
        operator: 'where',
        item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
        type: 'attribute'
      },
      {
        operator: 'and',
        item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
        type: 'attribute'
      },
      {
        operator: 'and',
        item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
        type: 'attribute'
      }
    ],
    [MethodFieldParameterEnum.PortfolioDefinitionsIcon]: file,
    rowId: 1,
    serviceSubjectSection: 'CP PF LC',
    versionParameters: []
  },
  {
    methodType: 'NEWMETHOD',
    name: '121233',
    parameters: [],
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributes]: [
      {
        operator: 'where',
        item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
        type: 'attribute'
      },
      {
        operator: 'and',
        item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
        type: 'attribute'
      },
      {
        operator: 'and',
        item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
        type: 'attribute'
      }
    ],
    [MethodFieldParameterEnum.PortfolioDefinitionsIcon]: file,
    rowId: 2,
    serviceSubjectSection: 'CP PF LC',
    versionParameters: []
  },
  {
    methodType: 'NEWMETHOD',
    name: '123',
    parameters: [],
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributes]: [
      {
        operator: 'where',
        item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
        type: 'attribute'
      },
      {
        operator: 'and',
        item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
        type: 'attribute'
      },
      {
        operator: 'and',
        attributes: [
          {
            operator: '',
            type: 'group',
            attributes: [
              {
                operator: '',
                type: 'attribute',
                item: { filterBy: 'Sys', operator: 'equal', value: 'AA' }
              }
            ]
          }
        ],
        type: 'group'
      }
    ],
    [MethodFieldParameterEnum.PortfolioDefinitionsIcon]: file,
    rowId: 1,
    serviceSubjectSection: 'CP PF LC',
    versionParameters: []
  },
  {
    methodType: 'NEWMETHOD',
    name: '121233',
    parameters: [],
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributes]: [
      {
        operator: 'where',
        item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
        type: 'attribute'
      },
      {
        operator: 'and',
        item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
        type: 'attribute'
      },
      {
        operator: 'and',
        item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
        type: 'attribute'
      }
    ],
    [MethodFieldParameterEnum.PortfolioDefinitionsIcon]: file,
    rowId: 2,
    serviceSubjectSection: 'CP PF LC',
    versionParameters: []
  },
  {
    methodType: 'NEWMETHOD',
    name: '123',
    parameters: [],
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributes]: [
      {
        operator: 'where',
        item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
        type: 'attribute'
      },
      {
        operator: 'and',
        item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
        type: 'attribute'
      },
      {
        operator: 'and',
        item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
        type: 'attribute'
      }
    ],
    [MethodFieldParameterEnum.PortfolioDefinitionsIcon]: file,
    rowId: 1,
    serviceSubjectSection: 'CP PF LC',
    versionParameters: []
  },
  {
    methodType: 'NEWMETHOD',
    name: '121233',
    parameters: [],
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributes]: [
      {
        operator: 'where',
        item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
        type: 'attribute'
      },
      {
        operator: 'and',
        item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
        type: 'attribute'
      },
      {
        operator: 'and',
        item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
        type: 'attribute'
      }
    ],
    [MethodFieldParameterEnum.PortfolioDefinitionsIcon]: file,
    rowId: 2,
    serviceSubjectSection: 'CP PF LC',
    versionParameters: []
  },
  {
    methodType: 'NEWMETHOD',
    name: '123',
    parameters: [],
    [MethodFieldParameterEnum.PortfolioDefinitionsAttributes]: [
      {
        operator: 'where',
        item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
        type: 'attribute'
      },
      {
        operator: 'and',
        item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
        type: 'attribute'
      },
      {
        operator: 'and',
        item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
        type: 'attribute'
      }
    ],
    [MethodFieldParameterEnum.PortfolioDefinitionsIcon]: file,
    rowId: 1,
    serviceSubjectSection: 'CP PF LC',
    versionParameters: []
  }
];

describe('ConfigureParametersStep > ConfigureParametersStep', () => {
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => (() => {}) as any);
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
    mockUseSelectWindowDimension.mockClear();
  });

  it('Should render ConfigureParametersStep', async () => {
    mockUseSelectWindowDimensionImplementation(1300);
    const props = {
      stepId: 'configureParameters diff',
      title: 'Configure Parameters',
      selectedStep: 'configureParameters',
      hasInstance: true,
      isEdit: false,
      isStuck: false,
      ready: true,
      savedAt: 1647669005153,
      handleSave: jest.fn(),
      setFormValues: jest.fn(),
      clearFormValues: jest.fn(),
      formValues: {
        isValid: true
      }
    };

    const wrapper = await renderComponent({
      ...props
    });

    expect(
      wrapper.getByText('txt_portfolio_definitions_add_portfolion')
    ).toBeInTheDocument();

    //Click to add
    userEvent.click(
      wrapper.getByText('txt_portfolio_definitions_add_portfolion')
    );

    //Click to close modal add
    userEvent.click(wrapper.getByText('AddNewMethodModal_component'));
  });

  it('Should render ConfigureParametersStep contains methods', async () => {
    mockUseSelectWindowDimensionImplementation(900);
    const props = {
      stepId: 'configureParameters',
      title: 'Configure Parameters',
      selectedStep: 'configureParameters',
      hasInstance: true,
      isEdit: false,
      isStuck: false,
      ready: true,
      savedAt: 1647669005153,
      handleSave: jest.fn(),
      setFormValues: jest.fn(),
      clearFormValues: jest.fn(),
      formValues: {
        isValid: true,
        methods
      }
    };

    const wrapper = await renderComponent({
      ...props
    });

    expect(
      wrapper.getByText('txt_portfolio_definitions_add_portfolion')
    ).toBeInTheDocument();

    //Click to add
    userEvent.click(
      wrapper.getByText('txt_portfolio_definitions_add_portfolion')
    );

    //Click to close modal add
    userEvent.click(wrapper.getByText('AddNewMethodModal_component'));

    //Click to search
    const input = wrapper.getByPlaceholderText('txt_type_to_search');
    userEvent.click(input);
    userEvent.type(input, 'acb');
    userEvent.type(input, '{enter}');

    //Click reset search
    const search = wrapper.getByText('txt_clear_and_reset');
    userEvent.click(search);

    //Click expand
    const icon = wrapper.container.querySelector('.icon-plus') as HTMLElement;
    userEvent.click(icon);
    userEvent.click(icon);

    //Click to onEdit
    const edit = wrapper.queryAllByText('txt_edit');
    userEvent.click(edit[0]);
    userEvent.click(wrapper.getByText('AddNewMethodModal_component'));

    //Click to onRemove
    const remove = wrapper.queryAllByText('txt_delete');
    userEvent.click(remove[0]);
    userEvent.click(wrapper.getByText('RemoveMethodModal_component'));

    wrapper.rerender(
      <ConfigureParametersStep {...props} stepId="configureParameters 1" />
    );
  });

  it('Should render ConfigureParametersStep contains methods with subgrid', async () => {
    mockUseSelectWindowDimensionImplementation(900);
    const props = {
      stepId: 'configureParameters',
      title: 'Configure Parameters',
      selectedStep: 'configureParameters',
      hasInstance: true,
      isEdit: false,
      isStuck: true,
      ready: true,
      savedAt: 1647669005153,
      handleSave: jest.fn(),
      setFormValues: jest.fn(),
      clearFormValues: jest.fn(),
      formValues: {
        isValid: true,
        methods: [
          {
            methodType: 'NEWMETHOD',
            name: '121233',
            parameters: [],
            [MethodFieldParameterEnum.PortfolioDefinitionsAttributes]: [
              {
                operator: 'where',
                item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
                type: 'attribute'
              },
              {
                operator: 'and',
                item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
                type: 'attribute'
              },
              {
                operator: 'and',
                item: { filterBy: 'Sys', operator: 'equal', value: 'AA' },
                type: 'attribute'
              }
            ],
            rowId: 2,
            serviceSubjectSection: 'CP PF LC',
            versionParameters: []
          }
        ]
      }
    };

    const wrapper = await renderComponent({
      ...props
    });

    expect(
      wrapper.getByText('txt_portfolio_definitions_add_portfolion')
    ).toBeInTheDocument();

    //Click expand
    const icon = wrapper.container.querySelector('.icon-plus') as HTMLElement;
    userEvent.click(icon);
    userEvent.click(icon);
  });

  it('Should render ConfigureParametersStep reset search value', async () => {
    mockUseSelectWindowDimensionImplementation(900);
    const props = {
      stepId: 'configureParameters',
      title: 'Configure Parameters',
      selectedStep: 'configureParameters',
      hasInstance: true,
      isEdit: false,
      isStuck: false,
      ready: true,
      savedAt: 1647669005153,
      handleSave: jest.fn(),
      setFormValues: jest.fn(),
      clearFormValues: jest.fn(),
      formValues: {
        isValid: true,
        methods
      }
    };

    const wrapper = await renderComponent({
      ...props
    });

    //Click to search
    const input = wrapper.getByPlaceholderText('txt_type_to_search');
    userEvent.click(input);
    userEvent.type(input, 'Sys');
    userEvent.type(input, '{enter}');

    await waitFor(() => {
      //Click reset search
      const resetSearch = wrapper.getByText('txt_clear_and_reset');
      userEvent.click(resetSearch);
    });
  });
});
