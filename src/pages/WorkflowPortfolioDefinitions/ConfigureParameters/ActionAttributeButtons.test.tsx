import { renderWithMockStore } from 'app/utils';
import React from 'react';
import ActionAttributeButtons from './ActionAttributeButtons';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderComponent = (props: any) => {
  return renderWithMockStore(<ActionAttributeButtons {...props} />, {});
};

describe('ActionAttributeButtons', () => {
  it('render component', async () => {
    const wrapper = await renderComponent({
      small: true,
      title: 'tess',
      onClickAddPortfolio: jest.fn()
    });

    expect(
      wrapper.getByText('txt_portfolio_definitions_add_portfolion')
    ).toBeInTheDocument();
  });

  it('render component', async () => {
    const wrapper = await renderComponent({
      onClickAddPortfolio: jest.fn()
    });

    expect(
      wrapper.getByText('txt_portfolio_definitions_add_portfolion')
    ).toBeInTheDocument();
  });
});
