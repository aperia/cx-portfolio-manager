import { useTranslation } from 'app/_libraries/_dls';
import { Icon } from 'app/_libraries/_dls/components';
import React from 'react';
import { Button } from 'react-bootstrap';

interface ActionButtonsProps {
  small?: boolean;
  title?: string;
  onClickAddPortfolio?: () => void;
}

const ActionButtons: React.FC<ActionButtonsProps> = ({
  small = false,
  title = '',
  onClickAddPortfolio
}) => {
  const { t } = useTranslation();

  if (small) {
    return (
      <div className="d-flex align-items-center">
        <h5>{t(title)}</h5>
        <div className="d-flex ml-auto mr-n8">
          <Button onClick={onClickAddPortfolio}>
            {t('txt_portfolio_definitions_add_portfolion')}
          </Button>
        </div>
      </div>
    );
  }

  return (
    <div className="row mt-16">
      <div className="col-4">
        <div
          className="rcc-btn d-flex justify-content-between border rounded-lg py-10"
          onClick={onClickAddPortfolio}
        >
          <span className="d-flex align-items-center ml-16">
            <Icon name="method-add" size="9x" className="color-grey-l16" />
            <span className="ml-12 mr-8">
              {t('txt_portfolio_definitions_add_portfolion')}
            </span>
          </span>
        </div>
      </div>
    </div>
  );
};

export default ActionButtons;
