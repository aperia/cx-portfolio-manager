import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { stepRegistry } from '../_commons/WorkflowSetup/registries';
import ConfigureParameters from './ConfigureParameters';
import GettingStartStep from './GettingStartStep';

stepRegistry.registerStep(
  'GetStartedPortfolioDefinitionsMethods',
  GettingStartStep
);

stepRegistry.registerStep(
  'ConfigureParametersPortfolioDefinitions',
  ConfigureParameters,
  [
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__PORTFOLIO__DEFINITIONS__CONFIGURE_PARAMETERS
  ]
);
