import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { uploadFileStepRegistry } from 'pages/_commons/WorkflowSetup/registries';
import { default as stepRegistry } from '../_commons/WorkflowSetup/registries/stepRegistry';

const registerFunc = jest.spyOn(stepRegistry, 'registerStep');
const registerCom = jest.spyOn(uploadFileStepRegistry, 'registerComponent');

describe('WorkflowCycleAccountsTestEnvironment > registerSteps', () => {
  it('Should register components', async () => {
    await import('./registerSteps');

    expect(registerFunc).toBeCalledWith(
      'GetStartedCycleAccountsTestEnvironment',
      expect.anything()
    );

    expect(registerCom).toBeCalledWith(
      'SubContentCycleAccountsTestEnvironment',
      expect.anything()
    );

    expect(registerFunc).toBeCalledWith(
      'ConfigureParametersCycleAccountsTextEnvironment',
      expect.anything(),
      [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CYCLE_ACCOUNTS_TEST_ENVIRONMENT__CONFIGURE_PARAMETERS
      ]
    );
  });
});
