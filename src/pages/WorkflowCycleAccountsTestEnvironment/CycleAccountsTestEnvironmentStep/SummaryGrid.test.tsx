import { RenderResult } from '@testing-library/react';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import { renderWithMockStore } from 'app/utils';
import * as CommonRedux from 'pages/_commons/redux/Common/select-hooks';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowCycleAccountsTestEnvironment';
import React from 'react';
import SummaryGrid from './SummaryGrid';
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const mockUseSelectWindowDimension = jest.spyOn(
  CommonRedux,
  'useSelectWindowDimension'
);

const mockUseSelectElementMetadataCycleAccount = jest.spyOn(
  WorkflowSetup,
  'useSelectElementMetadataCycleAccount'
);

const renderComponent = (props: any): RenderResult => {
  return renderWithMockStore(<SummaryGrid {...props} />);
};
const mockUseSelectWindowDimensionImplementation = (width: number) => {
  mockUseSelectWindowDimension.mockImplementation(() => ({ width } as any));
};

describe('WorkflowCycleAccountsTestEnvironment > Summary', () => {
  beforeEach(() => {
    mockUseSelectElementMetadataCycleAccount.mockReturnValue({
      cycleAccountsTestEnvironmentCode: [{ code: 'code', text: 'text' }],
      cycleAccountsTestEnvironmentTransaction: [{ code: 'code', text: 'text' }]
    });
  });

  afterEach(() => {
    mockUseSelectWindowDimension.mockClear();
  });

  it('Should render a grid with content', async () => {
    mockUseSelectWindowDimensionImplementation(900);
    const props = {
      stepId: '1',
      selectedStep: '2',
      clearFormName: '',
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = await renderComponent({
      ...props,
      formValues: {
        isValid: false,
        configureParameters: {
          [MethodFieldParameterEnum.CycleAccountsTestEnvironmentCode]: 'code'
        }
      },
      dependencies: {
        attachmentId: '123'
      }
    });

    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    expect(body?.children.length).toEqual(1);
  });
  it('Should render a grid with content', async () => {
    mockUseSelectWindowDimensionImplementation(1400);
    const props = {
      stepId: '1',
      selectedStep: '2',
      clearFormName: '',
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = await renderComponent({
      ...props,
      formValues: {
        isValid: false,
        configureParameters: {}
      },
      dependencies: {
        attachmentId: '123'
      }
    });

    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    expect(body?.children.length).toEqual(1);
  });
});
