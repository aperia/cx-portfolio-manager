import { getWorkflowSetupStepStatus } from 'app/helpers';
import { ConfigureParameter, ConfigureParametersFormValues } from '.';

const parseFormValues: WorkflowSetupStepFormDataFunc<ConfigureParametersFormValues> =
  (data, id) => {
    const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);
    if (!stepInfo) return;

    const parameters = (data?.data?.configurations?.parameters || []) as any[];

    const configureParameters = parameters.reduce(
      (pre, curr) => ({
        ...pre,
        [curr.name]: curr.value
      }),
      {} as ConfigureParameter
    );

    const isValid = true;

    return {
      ...getWorkflowSetupStepStatus(stepInfo),
      configureParameters,
      isValid
    };
  };

export default parseFormValues;
