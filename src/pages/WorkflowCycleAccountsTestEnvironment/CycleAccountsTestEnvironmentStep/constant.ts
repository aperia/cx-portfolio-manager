import {
  CycleAccountFieldNameEnum,
  CycleAccountFieldParameterEnum
} from 'app/constants/enums';
import { GridParameter } from './ParameterGrid';

export const gridConfigureData = (t: any): GridParameter[] => [
  {
    id: CycleAccountFieldParameterEnum.Code,
    fieldName: CycleAccountFieldNameEnum.Code,
    greenScreenName: CycleAccountFieldNameEnum.Code,
    moreInfoText: t('txt_cycle_accounts_test_environment_code_more_info'),
    moreInfo: t('txt_cycle_accounts_test_environment_code_more_info')
  }
];
