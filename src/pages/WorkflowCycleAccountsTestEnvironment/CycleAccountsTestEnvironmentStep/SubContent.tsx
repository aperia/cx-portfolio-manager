import { METHOD_CYCLE_ACCOUNTS_FIELDS } from 'app/constants/mapping';
import {
  Button,
  ColumnType,
  Grid,
  Icon,
  Tooltip,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import {
  actionsWorkflowSetup,
  useSelectElementMetadataCycleAccount
} from 'pages/_commons/redux/WorkflowSetup';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';

const SubContent: React.FC<WorkflowSetupProps> = ({ stepId, selectedStep }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const [isExpand, setIsExpand] = useState<boolean>(false);

  const { cycleAccountsTestEnvironmentTransaction } =
    useSelectElementMetadataCycleAccount();

  useEffect(() => {
    dispatch(
      actionsWorkflowSetup.getWorkflowMetadata(METHOD_CYCLE_ACCOUNTS_FIELDS)
    );
  }, [dispatch]);

  useEffect(() => {
    stepId !== selectedStep && setIsExpand(false);
  }, [stepId, selectedStep]);

  const columns: ColumnType[] = [
    {
      id: 'fieldName',
      Header: t('txt_cycle_accounts_test_environment_field_name'),
      width: 211,
      accessor: 'code'
    },
    {
      id: 'value',
      Header: t('txt_cycle_accounts_test_environment_field_type'),
      width: 103,
      accessor: 'type'
    },
    {
      id: 'moreInfo',
      Header: t('txt_cycle_accounts_test_environment_description'),
      accessor: data => {
        return (
          <TruncateText
            resizable
            lines={2}
            ellipsisLessText={t('txt_less')}
            ellipsisMoreText={t('txt_more')}
          >
            {data?.text}
          </TruncateText>
        );
      }
    }
  ];

  return (
    <div className="py-24 border-bottom">
      <div className="d-flex align-items-center">
        <Tooltip
          element={isExpand ? t('txt_collapse') : t('txt_expand')}
          variant="primary"
          placement="top"
          triggerClassName="d-flex ml-n2"
        >
          <Button
            size="sm"
            variant="icon-secondary"
            onClick={() => setIsExpand(!isExpand)}
          >
            <Icon name={isExpand ? 'minus' : 'plus'} size="4x" />
          </Button>
        </Tooltip>
        <p className="fw-600 ml-8">{t('txt_parameter_list')}</p>
      </div>

      {isExpand && (
        <>
          <p className="mt-8 mb-16">
            {t('txt_cycle_accounts_test_environment_upload_title')}
          </p>
          <Grid
            columns={columns}
            data={cycleAccountsTestEnvironmentTransaction}
          />
        </>
      )}
    </div>
  );
};

const ExtraStaticSubContentStep = SubContent as WorkflowSetupStaticProp;
ExtraStaticSubContentStep.defaultValues = { isValid: true };

export default ExtraStaticSubContentStep;
