import { act, fireEvent, RenderResult, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import { WorkflowMetadataOverride } from 'app/fixtures/workflow-metadata';
import { mockUseDispatchFnc, renderWithMockStore } from 'app/utils';
import * as CommonRedux from 'pages/_commons/redux/Common/select-hooks';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowCycleAccountsTestEnvironment';
import React from 'react';
import ParameterGrid from './ParameterGrid';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/utils/MockView')
);
const mockUseSelectWindowDimension = jest.spyOn(
  CommonRedux,
  'useSelectWindowDimension'
);
const mockUseSelectWindowDimensionImplementation = (width: number) => {
  mockUseSelectWindowDimension.mockImplementation(() => ({ width } as any));
};

const mockUseSelectElementMetadataCycleAccount = jest.spyOn(
  WorkflowSetup,
  'useSelectElementMetadataCycleAccount'
);
const mockUseDispatch = mockUseDispatchFnc();

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

jest.mock('app/hooks/useUnsavedChangeRegistry', () => {
  return {
    __esModule: true,
    useUnsavedChangeRegistry: (options: any, changes: any, onConfirm: any) => {
      onConfirm && onConfirm();
    }
  };
});

const getInitialState = ({
  elements = WorkflowMetadataOverride
}: any = {}) => ({
  workflowSetup: { elementMetadata: { elements } }
});

const renderComponent = (props: any) => {
  return renderWithMockStore(<ParameterGrid {...props} />, {
    initialState: getInitialState()
  });
};

const queryInputFromLabel = (tierLabel: HTMLElement) => {
  return tierLabel
    .closest('.text-field-container')
    ?.querySelector('input, .input') as HTMLInputElement;
};

const then_change_letter_number_dropdown = (wrapper: RenderResult) => {
  const setRecurrenceOptions = queryInputFromLabel(
    wrapper.getByText('txt_cycle_accounts_test_environment_select_an_option')
  );

  act(() => {
    userEvent.click(setRecurrenceOptions);
  });

  act(() => {
    userEvent.click(screen.getByText('text'));
  });

  fireEvent.blur(setRecurrenceOptions);
};

describe('WorkflowCycleAccountsTestEnvironment > ConfigureParametersStep ', () => {
  const mockDispatch = jest.fn();
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
    mockUseSelectElementMetadataCycleAccount.mockReturnValue({
      cycleAccountsTestEnvironmentCode: [{ code: 'code', text: 'text' }],
      cycleAccountsTestEnvironmentTransaction: [{ code: 'code', text: 'text' }]
    });
  });

  afterEach(() => {
    mockUseSelectWindowDimension.mockClear();
    mockUseDispatch.mockClear();
  });

  it('Should have error', async () => {
    mockUseSelectWindowDimensionImplementation(900);
    const props = {
      stepId: '1',
      selectedStep: '2',
      clearFormName: '',
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = await renderComponent({
      ...props,
      formValues: {
        isValid: false,
        configureParameters: {}
      }
    });

    expect(
      wrapper.getByText('txt_cycle_accounts_test_environment_search_title')
    ).toBeInTheDocument;
  });

  it('Should render a grid with content', async () => {
    mockUseSelectWindowDimensionImplementation(1400);
    const props = {
      stepId: '2',
      selectedStep: '2',
      clearFormName: '',
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = await renderComponent({
      ...props,
      formValues: {
        isValid: false,
        configureParameters: {
          [MethodFieldParameterEnum.CycleAccountsTestEnvironmentCode]: 'I'
        }
      },
      dependencies: {
        attachmentId: '123'
      }
    });

    then_change_letter_number_dropdown(wrapper);

    expect(
      wrapper.getByText('txt_cycle_accounts_test_environment_search_title')
    ).toBeInTheDocument;
  });

  it('Should render search bar', async () => {
    mockUseSelectWindowDimensionImplementation(1400);
    const wrapper = await renderComponent({
      formValues: {}
    });

    expect(
      wrapper.getByText('txt_cycle_accounts_test_environment_search_title')
    ).toBeInTheDocument;
  });
});
