import { MethodFieldParameterEnum } from 'app/constants/enums';
import { Grid, TruncateText, useTranslation } from 'app/_libraries/_dls';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import { useSelectElementMetadataCycleAccount } from 'pages/_commons/redux/WorkflowSetup';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React from 'react';
import { ConfigureParameter, ConfigureParametersFormValues } from '.';
import { gridConfigureData } from './constant';

const SummaryGrid: React.FC<
  WorkflowSetupSummaryProps<ConfigureParametersFormValues>
> = ({ formValues }) => {
  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();
  const { configureParameters } = formValues;
  const { cycleAccountsTestEnvironmentCode } =
    useSelectElementMetadataCycleAccount();

  const valueAccessor = (row: MagicKeyValue) => {
    const paramId = row.id as keyof ConfigureParameter;
    let valueTruncate = configureParameters?.[paramId] || '';

    if (
      !!valueTruncate &&
      cycleAccountsTestEnvironmentCode &&
      paramId === MethodFieldParameterEnum.CycleAccountsTestEnvironmentCode
    ) {
      valueTruncate = cycleAccountsTestEnvironmentCode.find(
        f => f.code === configureParameters?.[paramId]
      )!.text!;
    }
    return (
      <TruncateText
        resizable
        lines={2}
        ellipsisLessText={t('txt_less')}
        ellipsisMoreText={t('txt_more')}
      >
        {valueTruncate}
      </TruncateText>
    );
  };

  const columns = [
    {
      id: 'fieldName',
      Header: t('txt_business_name'),
      accessor: 'fieldName'
    },
    {
      id: 'greenScreenName',
      Header: t('txt_green_screen_name'),
      accessor: 'greenScreenName'
    },
    {
      id: 'value',
      Header: t('txt_value'),
      accessor: valueAccessor,
      width: width < 1280 ? 280 : undefined
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      accessor: viewMoreInfo(['moreInfo'], t),
      width: 105
    }
  ];

  return (
    <div>
      <Grid columns={columns} data={gridConfigureData(t)} />
    </div>
  );
};

export default SummaryGrid;
