import parseFormValues from './parseFormValues';

describe('pages > WorkflowDesignLoanOffers > DesignLoanOffersStep > parseFormValues', () => {
  it('parseFormValues', () => {
    const configurations: any = {
      parameters: [{ id: 'id', name: 'name', value: 'value' }]
    };
    const id = 'id';
    const input: any = {
      data: {
        configurations,
        workflowSetupData: [{ id, status: 'INPROGRESS' }]
      }
    };

    // isvalid
    let result = parseFormValues(input, id, jest.fn);
    expect(result?.isValid).toBeTruthy();

    // other step
    result = parseFormValues(input, 'id1', jest.fn);
    expect(result).toBeUndefined();
  });
  it('parseFormValues with empty parameters', () => {
    const configurations: any = {};
    const id = 'id';
    const input: any = {
      data: {
        configurations,
        workflowSetupData: [{ id, status: 'INPROGRESS' }]
      }
    };

    // isvalid
    let result = parseFormValues(input, id, jest.fn);
    expect(result?.isValid).toBeTruthy();

    // other step
    result = parseFormValues(input, 'id1', jest.fn);
    expect(result).toBeUndefined();
  });
});
