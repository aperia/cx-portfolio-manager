import { MethodFieldParameterEnum } from 'app/constants/enums';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React from 'react';
import ParameterGrid from './ParameterGrid';
import parseFormValues from './parseFormValues';
import ConfigureParametersSummary from './Summary';

export interface ConfigureParameter {
  [MethodFieldParameterEnum.CycleAccountsTestEnvironmentCode]?: string;
}

export interface ConfigureParametersFormValues {
  isValid?: boolean;
  configureParameters?: ConfigureParameter;
}

const ConfigureParametersStep: React.FC<
  WorkflowSetupProps<ConfigureParametersFormValues>
> = props => {
  return (
    <>
      <ParameterGrid {...props} />
    </>
  );
};

const ExtraStaticConfigureParametersStep =
  ConfigureParametersStep as WorkflowSetupStaticProp;
ExtraStaticConfigureParametersStep.summaryComponent =
  ConfigureParametersSummary;
ExtraStaticConfigureParametersStep.defaultValues = { isValid: true };
ExtraStaticConfigureParametersStep.parseFormValues = parseFormValues;

export default ExtraStaticConfigureParametersStep;
