import { fireEvent, render, RenderResult } from '@testing-library/react';
import { mockUseDispatchFnc } from 'app/utils';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowCycleAccountsTestEnvironment';
import React from 'react';
import SubContent from './SubContent';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});
const mockUseDispatch = mockUseDispatchFnc();
const mockUseSelectElementMetadataCycleAccount = jest.spyOn(
  WorkflowSetup,
  'useSelectElementMetadataCycleAccount'
);

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <SubContent {...props} />
    </div>
  );
};

describe('WorkflowCycleAccountsTestEnvironment > ParameterList', () => {
  const mockDispatch = jest.fn();
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
    mockUseSelectElementMetadataCycleAccount.mockReturnValue({
      cycleAccountsTestEnvironmentCode: [{ code: 'code', text: 'text' }],
      cycleAccountsTestEnvironmentTransaction: [{ code: 'code', text: 'text' }]
    });
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
    mockUseSelectElementMetadataCycleAccount.mockReset();
  });

  it('Should render a grid with content', async () => {
    const props = {
      stepId: '1',
      selectedStep: '2',
      clearFormName: '',
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = await renderComponent({
      ...props,
      formValues: {},
      dependencies: {
        attachmentId: '123'
      }
    });

    const buttonEx = wrapper.baseElement.querySelector(
      'button[class="btn btn-icon-secondary btn-sm"]'
    ) as Element;
    fireEvent.click(buttonEx);

    expect(wrapper.getByText('txt_parameter_list')).toBeInTheDocument();
  });
});
