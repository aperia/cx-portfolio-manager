import { render, RenderResult } from '@testing-library/react';
import ConfigureParametersStep from '.';
import React from 'react';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

jest.mock('./ParameterGrid', () => {
  return {
    __esModule: true,
    default: () => <div>ConfigureParametersForm</div>
  };
});

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <ConfigureParametersStep {...props} />
    </div>
  );
};

describe('WorkflowCycleAccountsTestEnvironment > ConfigureParametersStep', () => {
  it('Should render ConfigureParametersStep contains Completed', () => {
    const wrapper = renderComponent({
      formValues: {
        checked: 'r1'
      }
    });

    expect(wrapper.getByText('ConfigureParametersForm')).toBeInTheDocument;
  });
});
