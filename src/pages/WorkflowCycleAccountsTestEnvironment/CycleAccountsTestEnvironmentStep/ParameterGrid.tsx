import EnhanceDropdownList from 'app/components/EnhanceDropdownList';
import {
  CycleAccountFieldParameterEnum,
  MethodFieldParameterEnum
} from 'app/constants/enums';
import {
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { matchSearchValue } from 'app/helpers';
import { useUnsavedChangeRegistry } from 'app/hooks';
import { Grid, useTranslation } from 'app/_libraries/_dls';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import { useSelectElementMetadataCycleAccount } from 'pages/_commons/redux/WorkflowSetup';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { ConfigureParameter, ConfigureParametersFormValues } from '.';
import { gridConfigureData } from './constant';

export interface GridParameter {
  id: CycleAccountFieldParameterEnum;
  fieldName: string;
  greenScreenName: string;
  moreInfoText: string;
  moreInfo: React.ReactNode;
}

const ParameterGrid: React.FC<
  WorkflowSetupProps<ConfigureParametersFormValues>
> = ({ setFormValues, formValues, savedAt, selectedStep, stepId }) => {
  const [initialValues, setInitialValues] = useState(formValues);
  const { configureParameters } = formValues;
  const { width } = useSelectWindowDimension();
  const [searchValue, setSearchValue] = useState('');
  const { cycleAccountsTestEnvironmentCode } =
    useSelectElementMetadataCycleAccount();

  const { t } = useTranslation();

  useEffect(() => {
    setInitialValues(formValues);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [savedAt]);

  useEffect(() => {
    if (stepId !== selectedStep) {
      setSearchValue('');
    }
  }, [stepId, selectedStep]);

  const formChanged =
    initialValues?.configureParameters?.[
      MethodFieldParameterEnum.CycleAccountsTestEnvironmentCode
    ] !==
    configureParameters?.[
      MethodFieldParameterEnum.CycleAccountsTestEnvironmentCode
    ];
  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CYCLE_ACCOUNTS_TEST_ENVIRONMENT__CONFIGURE_PARAMETERS,
      priority: 1
    },
    [formChanged]
  );

  const handleFormChange = useCallback(
    (field: keyof ConfigureParameter) => (e: any) => {
      setFormValues({
        configureParameters: {
          ...configureParameters,
          [field]: e?.target?.value.code
        }
      });
    },
    [configureParameters, setFormValues]
  );

  const gridData = useMemo(
    () =>
      gridConfigureData(t).filter(f =>
        matchSearchValue(
          [f.fieldName, f.moreInfoText, f.greenScreenName],
          searchValue
        )
      ),
    [t, searchValue]
  );

  const renderDropdown = (data: RefData[], field: keyof ConfigureParameter) => {
    const value = data.find(o => o.code === configureParameters?.[field]);

    return (
      <EnhanceDropdownList
        placeholder={t('txt_cycle_accounts_test_environment_select_an_option')}
        dataTestId={`configureParameters__${field}`}
        size="small"
        value={value}
        onChange={handleFormChange(field)}
        options={data}
      />
    );
  };

  const valueAccessor = (data: Record<string, any>) => {
    const paramId = data.id as MethodFieldParameterEnum;
    switch (paramId) {
      case MethodFieldParameterEnum.CycleAccountsTestEnvironmentCode: {
        return renderDropdown(
          cycleAccountsTestEnvironmentCode,
          MethodFieldParameterEnum.CycleAccountsTestEnvironmentCode
        );
      }
    }
  };

  const columns = [
    {
      id: 'fieldName',
      Header: t('txt_business_name'),
      accessor: 'fieldName',
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'greenScreenName',
      Header: t('txt_green_screen_name'),
      accessor: 'greenScreenName',
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'value',
      Header: t('txt_value'),
      accessor: valueAccessor,
      cellBodyProps: { className: 'py-8' },
      width: width < 1280 ? 280 : undefined
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      accessor: viewMoreInfo(['moreInfo'], t),
      width: 105,
      cellBodyProps: { className: 'pt-12 pb-8' }
    }
  ];

  return (
    <>
      <div className="d-flex align-items-center justify-content-between mt-24 mb-16">
        <h5>{t('txt_cycle_accounts_test_environment_search_title')}</h5>
      </div>

      <Grid columns={columns} data={gridData} />
    </>
  );
};

export default ParameterGrid;
