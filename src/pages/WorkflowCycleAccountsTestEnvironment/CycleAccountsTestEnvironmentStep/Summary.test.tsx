import { fireEvent } from '@testing-library/react';
import { WorkflowMetadataOverride } from 'app/fixtures/workflow-metadata';
import { renderWithMockStore } from 'app/utils';
import Summary from './Summary';
import React from 'react';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

jest.mock('./SummaryGrid', () => ({
  __esModule: true,
  default: () => <div>SubRowGrid_component</div>
}));

const getInitialState = ({
  elements = WorkflowMetadataOverride
}: any = {}) => ({
  workflowSetup: { elementMetadata: { elements } }
});

describe('WorkflowCycleAccountsTestEnvironment  > Summary', () => {
  it('Should render summary contains Completed', async () => {
    const mockFn = jest.fn();
    const props = {
      onEditStep: mockFn,
      formValues: {}
    } as any;
    const wrapper = await renderWithMockStore(<Summary {...props} />, {
      initialState: getInitialState()
    });

    expect(wrapper.getByText('txt_edit')).toBeInTheDocument;
    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(mockFn).toHaveBeenCalled();
  });
});
