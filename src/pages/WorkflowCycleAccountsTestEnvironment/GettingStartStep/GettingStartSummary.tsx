import { Icon, useTranslation } from 'app/_libraries/_dls';
import React from 'react';

const GettingStartSummary: React.FC<WorkflowSetupSummaryProps> = () => {
  const { t } = useTranslation();
  return (
    <div className="position-relative">
      <div className="absolute-top-right mt-n22 d-flex align-items-center">
        <Icon className="mr-4" name="success" size="4x" color="green" />
        <span className="color-green-d16 fs-14">{t('txt_completed')}</span>
      </div>
    </div>
  );
};

export default GettingStartSummary;
