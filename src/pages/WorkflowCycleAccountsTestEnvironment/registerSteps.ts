import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import {
  stepRegistry,
  uploadFileStepRegistry
} from '../_commons/WorkflowSetup/registries';
import ConfigureParametersStep from './CycleAccountsTestEnvironmentStep';
import SubContent from './CycleAccountsTestEnvironmentStep/SubContent';
import GettingStartStep from './GettingStartStep';

stepRegistry.registerStep(
  'GetStartedCycleAccountsTestEnvironment',
  GettingStartStep
);

uploadFileStepRegistry.registerComponent(
  'SubContentCycleAccountsTestEnvironment',
  SubContent
);

stepRegistry.registerStep(
  'ConfigureParametersCycleAccountsTextEnvironment',
  ConfigureParametersStep,
  [
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CYCLE_ACCOUNTS_TEST_ENVIRONMENT__CONFIGURE_PARAMETERS
  ]
);
