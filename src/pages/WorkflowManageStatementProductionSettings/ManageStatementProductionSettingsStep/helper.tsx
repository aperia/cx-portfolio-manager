import { MethodFieldParameterEnum } from 'app/constants/enums';

export const LIST_DROPDOWN_FORM: any[] = [
  MethodFieldParameterEnum.ManageStatementProductionSettingsCreditBalanceHoldOptions,
  MethodFieldParameterEnum.ManageStatementProductionSettingsWhereToSendRequestPayments,
  MethodFieldParameterEnum.ManageStatementProductionSettingsSameDayLostStatusOption,
  MethodFieldParameterEnum.ManageStatementProductionSettingsUseFixedMinimumPaymentDueDate,
  MethodFieldParameterEnum.ManageStatementProductionSettingsAnnualInterestDisplay,
  MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeLateChargesInAnnualInterestDisplay,
  MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeCashItemInAnnualInterestDisplay,
  MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeMerchandiseItemInAnnualInterestDisplay,
  MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeOverLimitFeesInAnnualInterestDisplay,
  MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeStatementProductionFeesInAnnualInterestDisplay,
  MethodFieldParameterEnum.ManageStatementProductionSettingsProduceStatementsForInactiveDeletedAccounts,
  MethodFieldParameterEnum.ManageStatementProductionSettingsUSInactiveAccountExternalStatusControl,
  MethodFieldParameterEnum.ManageStatementProductionSettingsNonUSInactiveAccountExternalStatusControl,
  MethodFieldParameterEnum.ManageStatementProductionSettingsPricingStrategyChangeStatementProduction,
  MethodFieldParameterEnum.ManageStatementProductionSettingsStatusAAuthorizationProhibited,
  MethodFieldParameterEnum.ManageStatementProductionSettingsStatusBBankrupt,
  MethodFieldParameterEnum.ManageStatementProductionSettingsStatusCClosed,
  MethodFieldParameterEnum.ManageStatementProductionSettingsStatusERevoked,
  MethodFieldParameterEnum.ManageStatementProductionSettingsStatusFFrozen,
  MethodFieldParameterEnum.ManageStatementProductionSettingsStatusIInterestProhibited,
  MethodFieldParameterEnum.ManageStatementProductionSettingsStatusLLost,
  MethodFieldParameterEnum.ManageStatementProductionSettingsStatusUStolen,
  MethodFieldParameterEnum.ManageStatementProductionSettingsStatusBlank,
  MethodFieldParameterEnum.ManageStatementProductionSettingsEbillLanguageOptions,
  MethodFieldParameterEnum.ManageStatementProductionSettingsFinanceChargeStatementDisplayOptions,
  MethodFieldParameterEnum.ManageStatementProductionSettingsDisplayAirlineTransactionDetailsOnStatement,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCardholderNamesOnStatement,
  MethodFieldParameterEnum.ManageStatementProductionSettingsReversalDisplayOption,
  MethodFieldParameterEnum.ManageStatementProductionSettingsLateChargeDateDisplay,
  MethodFieldParameterEnum.ManageStatementProductionSettingsOverlimitChargeDateDisplay,
  MethodFieldParameterEnum.ManageStatementProductionSettingsForeignCurrencyDisplayOptions,
  MethodFieldParameterEnum.ManageStatementProductionSettingsFeeRecordIndicatorOnCISScreens,
  MethodFieldParameterEnum.ManageStatementProductionSettingsOptionalIssuerFeeStatementDisplayCode,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCreditBalanceHoldCode,
  MethodFieldParameterEnum.ManageStatementProductionSettingsPrintHardCopySecurityStatements,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCrossCycleFeeDisplayOptions,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCrossCycleInterestDisplayOptions,
  MethodFieldParameterEnum.ManageStatementProductionSettings280TransactionsSeriesDisplayOptions
];

export const LIST_NUMERIC_PERCENT_FORM: any[] = [
  MethodFieldParameterEnum.ManageStatementProductionSettingsMinimumAmountForInactiveDeletedAccounts
];

export const LIST_CIT_DROP_DRAG_FORM: any[] = [
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPICBP,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPICID,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPICII,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIM,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIP,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIR,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPICMF,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPICVI,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOAC,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOCI,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOMC,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOMI,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFLC,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFOC,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFRC,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPPOMP,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPICME
];

export const LIST_COMBOBOX_FORM: any[] = [
  MethodFieldParameterEnum.ManageStatementProductionSettingsUseFixedMinimumPaymentDueDateTableID,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance1,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance2,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance3,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance4,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance5,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance6,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance7,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance8,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance9,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCashAdvanceFinanceCharges,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCashItemCharges,
  MethodFieldParameterEnum.ManageStatementProductionSettingsMerchandiseFinanceCharges,
  MethodFieldParameterEnum.ManageStatementProductionSettingsMerchandiseItemCharges,
  MethodFieldParameterEnum.ManageStatementProductionSettingsLateCharge,
  MethodFieldParameterEnum.ManageStatementProductionSettingsOverlimitCharge,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsuranceRefunds,
  MethodFieldParameterEnum.ManageStatementProductionSettingsOptionalIssuerFeeMessageText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsSaleAmountAdjustmentText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCashAdvanceAmountAdjustmentText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsReturnAmountAdjustmentText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsPaymentAmountAdjustmentText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffPrincipalAdjustmentTextLine1,
  MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffFinanceChargesAdjustmentTextLine2,
  MethodFieldParameterEnum.ManageStatementProductionSettingsMiscellaneousSpecificCreditAdjustmentText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffSmallBalanceAdjustmentText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffCreditLifeInsuranceAdjustmentText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffCashInterestRefundText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffLateChargeRefundText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffMerchandiseInterestRefundText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffCashItemRefundText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffMerchandiseItemRefundText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffOverlimitFeeRefundText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsSaleReversalText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCashAdvanceReversalText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsReturnReversalText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsPaymentReversalText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsSCSStatementOverrideTableID,
  MethodFieldParameterEnum.ManageStatementProductionSettingsLateFeeWaiverText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsGenericMessageAboveRevolvingBalance,
  MethodFieldParameterEnum.ManageStatementProductionSettingsGenericMessageBelowRevolvingBalance,
  MethodFieldParameterEnum.ManageStatementProductionSettingsNormalTermsDateID,
  MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycCashAdvance,
  MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycCashItemCharge,
  MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycMerchandise,
  MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycMdseItemCharge,
  MethodFieldParameterEnum.ManageStatementProductionSettingsMinimumFinanceCharges,
  MethodFieldParameterEnum.ManageStatementProductionSettingsActivityFee,
  MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycLateCharge,
  MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycOverlimitCharge,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsuranceCharge,
  MethodFieldParameterEnum.ManageStatementProductionSettingsOtherDebitAdjustmentText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsOtherCreditAdjustmentText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCashAdvanceFinanceChargesText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsMerchandiseItemChargeText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCashItemChargesText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsMerchandiseFinanceChargesText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsLateChargesText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsOverlimitChargesText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsMinimumFinanceChargesText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsActivityFeesText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsuranceText
];

export const METHOD_MSPS_FIELDS = [
  MethodFieldParameterEnum.ManageStatementProductionSettingsCreditBalanceHoldOptions,
  MethodFieldParameterEnum.ManageStatementProductionSettingsWhereToSendRequestPayments,
  MethodFieldParameterEnum.ManageStatementProductionSettingsPaymentDueDays,
  MethodFieldParameterEnum.ManageStatementProductionSettingsSameDayLostStatusOption,
  MethodFieldParameterEnum.ManageStatementProductionSettingsUseFixedMinimumPaymentDueDate,
  MethodFieldParameterEnum.ManageStatementProductionSettingsUseFixedMinimumPaymentDueDateTableID,
  MethodFieldParameterEnum.ManageStatementProductionSettingsAnnualInterestDisplay,
  MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeLateChargesInAnnualInterestDisplay,
  MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeCashItemInAnnualInterestDisplay,
  MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeMerchandiseItemInAnnualInterestDisplay,
  MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeOverLimitFeesInAnnualInterestDisplay,
  MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeStatementProductionFeesInAnnualInterestDisplay,
  MethodFieldParameterEnum.ManageStatementProductionSettingsProduceStatementsForInactiveDeletedAccounts,
  MethodFieldParameterEnum.ManageStatementProductionSettingsUSInactiveAccountExternalStatusControl,
  MethodFieldParameterEnum.ManageStatementProductionSettingsNonUSInactiveAccountExternalStatusControl,
  MethodFieldParameterEnum.ManageStatementProductionSettingsPricingStrategyChangeStatementProduction,
  MethodFieldParameterEnum.ManageStatementProductionSettingsMinimumAmountForInactiveDeletedAccounts,
  MethodFieldParameterEnum.ManageStatementProductionSettingsStatusAAuthorizationProhibited,
  MethodFieldParameterEnum.ManageStatementProductionSettingsStatusBBankrupt,
  MethodFieldParameterEnum.ManageStatementProductionSettingsStatusCClosed,
  MethodFieldParameterEnum.ManageStatementProductionSettingsStatusERevoked,
  MethodFieldParameterEnum.ManageStatementProductionSettingsStatusFFrozen,
  MethodFieldParameterEnum.ManageStatementProductionSettingsStatusIInterestProhibited,
  MethodFieldParameterEnum.ManageStatementProductionSettingsStatusLLost,
  MethodFieldParameterEnum.ManageStatementProductionSettingsStatusUStolen,
  MethodFieldParameterEnum.ManageStatementProductionSettingsStatusBlank,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance1,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance2,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance3,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance4,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance5,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance6,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance7,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance8,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance9,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCashAdvanceFinanceCharges,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCashItemCharges,
  MethodFieldParameterEnum.ManageStatementProductionSettingsMerchandiseFinanceCharges,
  MethodFieldParameterEnum.ManageStatementProductionSettingsMerchandiseItemCharges,
  MethodFieldParameterEnum.ManageStatementProductionSettingsLateCharge,
  MethodFieldParameterEnum.ManageStatementProductionSettingsOverlimitCharge,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsuranceRefunds,
  MethodFieldParameterEnum.ManageStatementProductionSettingsTurnOffPaper,
  MethodFieldParameterEnum.ManageStatementProductionSettingsTurnOnPaper,
  MethodFieldParameterEnum.ManageStatementProductionSettingsReturnToEDelivery,
  MethodFieldParameterEnum.ManageStatementProductionSettingsDeliquentAccountUnEnrollment,
  MethodFieldParameterEnum.ManageStatementProductionSettingsEbillLanguageOptions,
  MethodFieldParameterEnum.ManageStatementProductionSettingsFinanceChargeStatementDisplayOptions,
  MethodFieldParameterEnum.ManageStatementProductionSettingsDisplayAirlineTransactionDetailsOnStatement,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCardholderNamesOnStatement,
  MethodFieldParameterEnum.ManageStatementProductionSettingsReversalDisplayOption,
  MethodFieldParameterEnum.ManageStatementProductionSettingsLateChargeDateDisplay,
  MethodFieldParameterEnum.ManageStatementProductionSettingsOverlimitChargeDateDisplay,
  MethodFieldParameterEnum.ManageStatementProductionSettingsForeignCurrencyDisplayOptions,
  MethodFieldParameterEnum.ManageStatementProductionSettingsFeeRecordIndicatorOnCISScreens,
  MethodFieldParameterEnum.ManageStatementProductionSettingsOptionalIssuerFeeStatementDisplayCode,
  MethodFieldParameterEnum.ManageStatementProductionSettingsOptionalIssuerFeeMessageText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsSaleAmountAdjustmentText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCashAdvanceAmountAdjustmentText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsReturnAmountAdjustmentText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsPaymentAmountAdjustmentText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffPrincipalAdjustmentTextLine1,
  MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffFinanceChargesAdjustmentTextLine2,
  MethodFieldParameterEnum.ManageStatementProductionSettingsMiscellaneousSpecificCreditAdjustmentText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffSmallBalanceAdjustmentText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffCreditLifeInsuranceAdjustmentText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffCashInterestRefundText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffLateChargeRefundText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffMerchandiseInterestRefundText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffCashItemRefundText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffMerchandiseItemRefundText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffOverlimitFeeRefundText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsSaleReversalText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCashAdvanceReversalText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsReturnReversalText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsPaymentReversalText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPICBP,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPICID,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPICII,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIM,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIP,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIR,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPICMF,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPICVI,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOAC,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOCI,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOMC,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOMI,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFLC,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFOC,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFRC,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPPOMP,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCPICME,
  MethodFieldParameterEnum.ManageStatementProductionSettingsSaleAmountAdjustmentText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCreditBalanceHoldCode,
  MethodFieldParameterEnum.ManageStatementProductionSettingsPrintHardCopySecurityStatements,
  MethodFieldParameterEnum.ManageStatementProductionSettingsSCSStatementOverrideTableID,
  MethodFieldParameterEnum.ManageStatementProductionSettingsLateFeeWaiverText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsGenericMessageAboveRevolvingBalance,
  MethodFieldParameterEnum.ManageStatementProductionSettingsGenericMessageBelowRevolvingBalance,
  MethodFieldParameterEnum.ManageStatementProductionSettingsNormalTermsDateID,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCrossCycleFeeDisplayOptions,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCrossCycleInterestDisplayOptions,
  MethodFieldParameterEnum.ManageStatementProductionSettings280TransactionsSeriesDisplayOptions,
  MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycCashAdvance,
  MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycCashItemCharge,
  MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycMerchandise,
  MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycMdseItemCharge,
  MethodFieldParameterEnum.ManageStatementProductionSettingsMinimumFinanceCharges,
  MethodFieldParameterEnum.ManageStatementProductionSettingsActivityFee,
  MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycLateCharge,
  MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycOverlimitCharge,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsuranceCharge,
  MethodFieldParameterEnum.ManageStatementProductionSettingsOtherDebitAdjustmentText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsOtherCreditAdjustmentText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCashAdvanceFinanceChargesText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsMerchandiseItemChargeText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCashItemChargesText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsMerchandiseFinanceChargesText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsLateChargesText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsOverlimitChargesText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsMinimumFinanceChargesText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsActivityFeesText,
  MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsuranceText
];

export const METHOD_MSPS_DEFAULT: Record<string, string> = {
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditBalanceHoldOptions]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsWhereToSendRequestPayments]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsPaymentDueDays]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsSameDayLostStatusOption]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsUseFixedMinimumPaymentDueDate]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsUseFixedMinimumPaymentDueDateTableID]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsAnnualInterestDisplay]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeLateChargesInAnnualInterestDisplay]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeCashItemInAnnualInterestDisplay]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeMerchandiseItemInAnnualInterestDisplay]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeOverLimitFeesInAnnualInterestDisplay]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeStatementProductionFeesInAnnualInterestDisplay]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsProduceStatementsForInactiveDeletedAccounts]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsUSInactiveAccountExternalStatusControl]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsNonUSInactiveAccountExternalStatusControl]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsPricingStrategyChangeStatementProduction]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsMinimumAmountForInactiveDeletedAccounts]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsStatusAAuthorizationProhibited]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsStatusBBankrupt]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsStatusCClosed]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsStatusERevoked]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsStatusFFrozen]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsStatusIInterestProhibited]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsStatusLLost]: ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsStatusUStolen]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsStatusBlank]: ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance1]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance2]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance3]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance4]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance5]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance6]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance7]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance8]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance9]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCashAdvanceFinanceCharges]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCashItemCharges]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsMerchandiseFinanceCharges]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsMerchandiseItemCharges]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsLateCharge]: '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsOverlimitCharge]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsuranceRefunds]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsTurnOffPaper]: '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsTurnOnPaper]: '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsReturnToEDelivery]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsDeliquentAccountUnEnrollment]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsEbillLanguageOptions]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsFinanceChargeStatementDisplayOptions]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsDisplayAirlineTransactionDetailsOnStatement]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCardholderNamesOnStatement]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsReversalDisplayOption]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsLateChargeDateDisplay]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsOverlimitChargeDateDisplay]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsForeignCurrencyDisplayOptions]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsFeeRecordIndicatorOnCISScreens]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsOptionalIssuerFeeStatementDisplayCode]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsOptionalIssuerFeeMessageText]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsSaleAmountAdjustmentText]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCashAdvanceAmountAdjustmentText]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsReturnAmountAdjustmentText]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsPaymentAmountAdjustmentText]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffPrincipalAdjustmentTextLine1]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffFinanceChargesAdjustmentTextLine2]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsMiscellaneousSpecificCreditAdjustmentText]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffSmallBalanceAdjustmentText]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffCreditLifeInsuranceAdjustmentText]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffCashInterestRefundText]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffLateChargeRefundText]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffMerchandiseInterestRefundText]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffCashItemRefundText]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffMerchandiseItemRefundText]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffOverlimitFeeRefundText]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsSaleReversalText]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCashAdvanceReversalText]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsReturnReversalText]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsPaymentReversalText]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICBP]: '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICID]: '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICII]: '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIM]: '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIP]: '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIR]: '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICMF]: '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICVI]: '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOAC]: '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOCI]: '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOMC]: '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOMI]: '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFLC]: '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFOC]: '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFRC]: '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCPPOMP]: '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICME]: '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditBalanceHoldCode]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsPrintHardCopySecurityStatements]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsSCSStatementOverrideTableID]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsLateFeeWaiverText]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsGenericMessageAboveRevolvingBalance]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsGenericMessageBelowRevolvingBalance]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsNormalTermsDateID]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCrossCycleFeeDisplayOptions]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCrossCycleInterestDisplayOptions]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettings280TransactionsSeriesDisplayOptions]:
    ' ',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycCashAdvance]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycCashItemCharge]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycMerchandise]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycMdseItemCharge]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsMinimumFinanceCharges]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsActivityFee]: '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycLateCharge]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycOverlimitCharge]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsuranceCharge]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsOtherDebitAdjustmentText]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsOtherCreditAdjustmentText]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCashAdvanceFinanceChargesText]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsMerchandiseItemChargeText]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCashItemChargesText]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsMerchandiseFinanceChargesText]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsLateChargesText]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsOverlimitChargesText]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsMinimumFinanceChargesText]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsActivityFeesText]:
    '',
  [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsuranceText]:
    ''
};

export const mappingPreviewData = (
  metadata: MagicKeyValue,
  data: MagicKeyValue
) => {
  const {
    creditBalanceHoldOptions,
    whereToSendRequestPayments,
    sameDayLostStatusOption,
    useFixedMinimumPaymentDueDate,
    useFixedMinimumPaymentDueDateTableId,
    annualInterestDisplay,
    includeLateChargesInAnnualInterestDisplay,
    includeCashItemInAnnualInterestDisplay,
    includeMerchandiseItemInAnnualInterestDisplay,
    includeOverLimitFeesInAnnualInterestDisplay,
    includesStatementProductionFeesInAnnualInterestDisplay,
    produceStatementsForInactiveDeletedAccounts,
    usInactiveAccountExternalStatusControl,
    nonUsInactiveAccountExternalStatusControl,
    pricingStrategyChangeStatementProduction,
    statusAAuthorizationProhibited,
    statusBBankrupt,
    statusCClosed,
    statusERevoked,
    statusFFrozen,
    statusIInterestProhibited,
    statusLLost,
    statusUStolen,
    statusBlank,
    creditLifeInsurancePlan1,
    creditLifeInsurancePlan2,
    creditLifeInsurancePlan3,
    creditLifeInsurancePlan4,
    creditLifeInsurancePlan5,
    creditLifeInsurancePlan6,
    creditLifeInsurancePlan7,
    creditLifeInsurancePlan8,
    creditLifeInsurancePlan9,
    cashAdvanceFinanceCharges,
    cashItemCharges,
    merchandiseFinanceCharges,
    merchandiseItemCharges,
    lateCharges,
    overlimitCharge,
    creditLifeInsuranceRefunds,
    eBillLanguageOptions,
    financeChargeStatementDisplayOptions,
    displayAirlineTransaction,
    cardHolderNamesOnStatement,
    reversalDisplayOption,
    lateChargeDateDisplay,
    overlimitChargeDateDisplay,
    foreignCurrencyDisplayOptions,
    feeRecordIndicator,
    optionalIssuerFeeStatementDisplayCode,
    optionalIssuerFeeMessageText,
    saleAmountAdjText,
    cashAdvanceAmountAdjText,
    returnAmountAdjText,
    paymentAmountAdjText,
    chargeOffPrincipalAdjText,
    chargeOffFinanceChargesAdjText,
    miscSpecificCreditAdjText,
    chargeOffSmallBalanceAdjText,
    chargeOffCreditLifeInsuranceAdjText,
    chargeOffCashInterestRefundText,
    chargeOffLateChargeRefundText,
    chargeOffMerchandiseInterestRefundText,
    chargeOffCashItemRefundText,
    chargeOffMerchandiseItemRefundText,
    chargeOffOverlimitFeeRefundText,
    saleReversalText,
    cashAdvanceReversalText,
    returnReversalText,
    paymentReversalText,
    creditBalanceHoldCode,
    printHardCopySec,
    cssStatementOverrideTableId,
    lateFeeWaiverText,
    genericMessageAbove,
    genericMessageBelow,
    normalTermsDateId,
    crossCycleFeeDisplayOptions,
    crossCycleInterestDisplayOptions,
    transactionSeriesDisplayOptions,
    prevCycCashAdv,
    prevCycMerchandise,
    prevCycCashItemCharge,
    prevCycMdsItemCharge,
    minimumFinanceCharges,
    activityFee,
    prevCycLateCharge,
    prevCycOverLimitCharge,
    creditLifeInsuranceCharge,
    otherCreditAdjText,
    otherDebitAdjText,
    cashAdvFinanceChargesText,
    merchandiseItemChargeText,
    cashItemChargesText,
    merchandiseFinanceChargesText,
    lateChargesText,
    overlimitChargesText,
    minimumFinanceChargesText,
    activityFeesText,
    creditLifeInsuranceText
  } = metadata;
  const mappingData: MagicKeyValue = {
    [MethodFieldParameterEnum.ManageStatementProductionSettingsTurnOnPaper]:
      data[
        MethodFieldParameterEnum.ManageStatementProductionSettingsTurnOnPaper
      ],
    [MethodFieldParameterEnum.ManageStatementProductionSettingsTurnOffPaper]:
      data[
        MethodFieldParameterEnum.ManageStatementProductionSettingsTurnOffPaper
      ],
    [MethodFieldParameterEnum.ManageStatementProductionSettingsReturnToEDelivery]:
      data[
        MethodFieldParameterEnum
          .ManageStatementProductionSettingsReturnToEDelivery
      ],
    [MethodFieldParameterEnum.ManageStatementProductionSettingsDeliquentAccountUnEnrollment]:
      data[
        MethodFieldParameterEnum
          .ManageStatementProductionSettingsDeliquentAccountUnEnrollment
      ],
    [MethodFieldParameterEnum.ManageStatementProductionSettingsMinimumAmountForInactiveDeletedAccounts]:
      data[
        MethodFieldParameterEnum
          .ManageStatementProductionSettingsMinimumAmountForInactiveDeletedAccounts
      ],

    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditBalanceHoldOptions]:
      creditBalanceHoldOptions.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsCreditBalanceHoldOptions
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsWhereToSendRequestPayments]:
      whereToSendRequestPayments.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsWhereToSendRequestPayments
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPaymentDueDays]:
      data[
        MethodFieldParameterEnum.ManageStatementProductionSettingsPaymentDueDays
      ],
    [MethodFieldParameterEnum.ManageStatementProductionSettingsSameDayLostStatusOption]:
      sameDayLostStatusOption.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsSameDayLostStatusOption
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsUseFixedMinimumPaymentDueDate]:
      useFixedMinimumPaymentDueDate.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsUseFixedMinimumPaymentDueDate
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsUseFixedMinimumPaymentDueDateTableID]:
      useFixedMinimumPaymentDueDateTableId.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsUseFixedMinimumPaymentDueDateTableID
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsAnnualInterestDisplay]:
      annualInterestDisplay.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsAnnualInterestDisplay
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeLateChargesInAnnualInterestDisplay]:
      includeLateChargesInAnnualInterestDisplay.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsIncludeLateChargesInAnnualInterestDisplay
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeCashItemInAnnualInterestDisplay]:
      includeCashItemInAnnualInterestDisplay.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsIncludeCashItemInAnnualInterestDisplay
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeMerchandiseItemInAnnualInterestDisplay]:
      includeMerchandiseItemInAnnualInterestDisplay.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsIncludeMerchandiseItemInAnnualInterestDisplay
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeOverLimitFeesInAnnualInterestDisplay]:
      includeOverLimitFeesInAnnualInterestDisplay.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsIncludeOverLimitFeesInAnnualInterestDisplay
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeStatementProductionFeesInAnnualInterestDisplay]:
      includesStatementProductionFeesInAnnualInterestDisplay.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsIncludeStatementProductionFeesInAnnualInterestDisplay
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsProduceStatementsForInactiveDeletedAccounts]:
      produceStatementsForInactiveDeletedAccounts.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsProduceStatementsForInactiveDeletedAccounts
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsUSInactiveAccountExternalStatusControl]:
      usInactiveAccountExternalStatusControl.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsUSInactiveAccountExternalStatusControl
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsNonUSInactiveAccountExternalStatusControl]:
      nonUsInactiveAccountExternalStatusControl.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsNonUSInactiveAccountExternalStatusControl
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPricingStrategyChangeStatementProduction]:
      pricingStrategyChangeStatementProduction.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsPricingStrategyChangeStatementProduction
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsStatusAAuthorizationProhibited]:
      statusAAuthorizationProhibited.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsStatusAAuthorizationProhibited
          ]
      )?.text,

    [MethodFieldParameterEnum.ManageStatementProductionSettingsStatusBBankrupt]:
      statusBBankrupt.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsStatusBBankrupt
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsStatusCClosed]:
      statusCClosed.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsStatusCClosed
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsStatusERevoked]:
      statusERevoked.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsStatusERevoked
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsStatusFFrozen]:
      statusFFrozen.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsStatusFFrozen
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsStatusIInterestProhibited]:
      statusIInterestProhibited.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsStatusIInterestProhibited
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsStatusLLost]:
      statusLLost.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsStatusLLost
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsStatusUStolen]:
      statusUStolen.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsStatusUStolen
          ]
      )?.text,

    [MethodFieldParameterEnum.ManageStatementProductionSettingsStatusBlank]:
      statusBlank.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsStatusBlank
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance1]:
      creditLifeInsurancePlan1.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsCreditLifeInsurance1
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance2]:
      creditLifeInsurancePlan2.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsCreditLifeInsurance2
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance3]:
      creditLifeInsurancePlan3.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsCreditLifeInsurance3
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance4]:
      creditLifeInsurancePlan4.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsCreditLifeInsurance4
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance5]:
      creditLifeInsurancePlan5.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsCreditLifeInsurance5
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance6]:
      creditLifeInsurancePlan6.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsCreditLifeInsurance6
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance7]:
      creditLifeInsurancePlan7.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsCreditLifeInsurance7
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance8]:
      creditLifeInsurancePlan8.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsCreditLifeInsurance8
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance9]:
      creditLifeInsurancePlan9.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsCreditLifeInsurance9
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCashAdvanceFinanceCharges]:
      cashAdvanceFinanceCharges.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsCashAdvanceFinanceCharges
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCashItemCharges]:
      cashItemCharges.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsCashItemCharges
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsMerchandiseFinanceCharges]:
      merchandiseFinanceCharges.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsMerchandiseFinanceCharges
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsMerchandiseItemCharges]:
      merchandiseItemCharges.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsMerchandiseItemCharges
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsLateCharge]:
      lateCharges.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum.ManageStatementProductionSettingsLateCharge
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsOverlimitCharge]:
      overlimitCharge.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsOverlimitCharge
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsuranceRefunds]:
      creditLifeInsuranceRefunds.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsCreditLifeInsuranceRefunds
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsEbillLanguageOptions]:
      eBillLanguageOptions.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsEbillLanguageOptions
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsFinanceChargeStatementDisplayOptions]:
      financeChargeStatementDisplayOptions.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsFinanceChargeStatementDisplayOptions
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsDisplayAirlineTransactionDetailsOnStatement]:
      displayAirlineTransaction.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsDisplayAirlineTransactionDetailsOnStatement
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCardholderNamesOnStatement]:
      cardHolderNamesOnStatement.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsCardholderNamesOnStatement
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsReversalDisplayOption]:
      reversalDisplayOption.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsReversalDisplayOption
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsLateChargeDateDisplay]:
      lateChargeDateDisplay.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsLateChargeDateDisplay
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsOverlimitChargeDateDisplay]:
      overlimitChargeDateDisplay.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsOverlimitChargeDateDisplay
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsForeignCurrencyDisplayOptions]:
      foreignCurrencyDisplayOptions.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsForeignCurrencyDisplayOptions
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsFeeRecordIndicatorOnCISScreens]:
      feeRecordIndicator.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsFeeRecordIndicatorOnCISScreens
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsOptionalIssuerFeeStatementDisplayCode]:
      optionalIssuerFeeStatementDisplayCode.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsOptionalIssuerFeeStatementDisplayCode
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsOptionalIssuerFeeMessageText]:
      optionalIssuerFeeMessageText.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsOptionalIssuerFeeMessageText
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsSaleAmountAdjustmentText]:
      saleAmountAdjText.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsSaleAmountAdjustmentText
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCashAdvanceAmountAdjustmentText]:
      cashAdvanceAmountAdjText.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsCashAdvanceAmountAdjustmentText
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsReturnAmountAdjustmentText]:
      returnAmountAdjText.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsReturnAmountAdjustmentText
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffFinanceChargesAdjustmentTextLine2]:
      chargeOffFinanceChargesAdjText.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsChargeOffFinanceChargesAdjustmentTextLine2
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPaymentAmountAdjustmentText]:
      paymentAmountAdjText.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsPaymentAmountAdjustmentText
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffPrincipalAdjustmentTextLine1]:
      chargeOffPrincipalAdjText.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsChargeOffPrincipalAdjustmentTextLine1
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsMiscellaneousSpecificCreditAdjustmentText]:
      miscSpecificCreditAdjText.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsMiscellaneousSpecificCreditAdjustmentText
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffSmallBalanceAdjustmentText]:
      chargeOffSmallBalanceAdjText.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsChargeOffSmallBalanceAdjustmentText
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffCreditLifeInsuranceAdjustmentText]:
      chargeOffCreditLifeInsuranceAdjText.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsChargeOffCreditLifeInsuranceAdjustmentText
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffCashInterestRefundText]:
      chargeOffCashInterestRefundText.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsChargeOffCashInterestRefundText
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffLateChargeRefundText]:
      chargeOffLateChargeRefundText.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsChargeOffLateChargeRefundText
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffMerchandiseItemRefundText]:
      chargeOffMerchandiseItemRefundText.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsChargeOffMerchandiseItemRefundText
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffCashItemRefundText]:
      chargeOffCashItemRefundText.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsChargeOffCashItemRefundText
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffMerchandiseInterestRefundText]:
      chargeOffMerchandiseInterestRefundText.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsChargeOffMerchandiseInterestRefundText
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffOverlimitFeeRefundText]:
      chargeOffOverlimitFeeRefundText.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsChargeOffOverlimitFeeRefundText
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsSaleReversalText]:
      saleReversalText.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsSaleReversalText
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCashAdvanceReversalText]:
      cashAdvanceReversalText.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsCashAdvanceReversalText
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsReturnReversalText]:
      returnReversalText.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsReturnReversalText
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPaymentReversalText]:
      paymentReversalText.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsPaymentReversalText
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICBP]:
      data[MethodFieldParameterEnum.ManageStatementProductionSettingsCPICBP],
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICID]:
      data[MethodFieldParameterEnum.ManageStatementProductionSettingsCPICID],
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICII]:
      data[MethodFieldParameterEnum.ManageStatementProductionSettingsCPICII],
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIM]:
      data[MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIM],
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIP]:
      data[MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIP],
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIR]:
      data[MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIR],
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICMF]:
      data[MethodFieldParameterEnum.ManageStatementProductionSettingsCPICMF],
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICVI]:
      data[MethodFieldParameterEnum.ManageStatementProductionSettingsCPICVI],
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOAC]:
      data[MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOAC],
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOCI]:
      data[MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOCI],
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOMC]:
      data[MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOMC],
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOMI]:
      data[MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOMI],
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFLC]:
      data[MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFLC],
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFOC]:
      data[MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFOC],
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFRC]:
      data[MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFRC],
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPPOMP]:
      data[MethodFieldParameterEnum.ManageStatementProductionSettingsCPPOMP],
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICME]:
      data[MethodFieldParameterEnum.ManageStatementProductionSettingsCPICME],
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditBalanceHoldCode]:
      creditBalanceHoldCode.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsCreditBalanceHoldCode
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPrintHardCopySecurityStatements]:
      printHardCopySec.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsPrintHardCopySecurityStatements
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsSCSStatementOverrideTableID]:
      cssStatementOverrideTableId.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsSCSStatementOverrideTableID
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsLateFeeWaiverText]:
      lateFeeWaiverText.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsLateFeeWaiverText
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsGenericMessageAboveRevolvingBalance]:
      genericMessageAbove.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsGenericMessageAboveRevolvingBalance
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsGenericMessageBelowRevolvingBalance]:
      genericMessageBelow.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsGenericMessageBelowRevolvingBalance
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsNormalTermsDateID]:
      normalTermsDateId.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsNormalTermsDateID
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCrossCycleFeeDisplayOptions]:
      crossCycleFeeDisplayOptions.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsCrossCycleFeeDisplayOptions
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCrossCycleInterestDisplayOptions]:
      crossCycleInterestDisplayOptions.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsCrossCycleInterestDisplayOptions
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettings280TransactionsSeriesDisplayOptions]:
      transactionSeriesDisplayOptions.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettings280TransactionsSeriesDisplayOptions
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycCashAdvance]:
      prevCycCashAdv.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsPrevCycCashAdvance
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycCashItemCharge]:
      prevCycCashItemCharge.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsPrevCycCashItemCharge
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycMerchandise]:
      prevCycMerchandise.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsPrevCycMerchandise
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycMdseItemCharge]:
      prevCycMdsItemCharge.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsPrevCycMdseItemCharge
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsMinimumFinanceCharges]:
      minimumFinanceCharges.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsMinimumFinanceCharges
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsActivityFee]:
      activityFee.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsActivityFee
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycLateCharge]:
      prevCycLateCharge.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsPrevCycLateCharge
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycOverlimitCharge]:
      prevCycOverLimitCharge.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsPrevCycOverlimitCharge
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsuranceCharge]:
      creditLifeInsuranceCharge.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsCreditLifeInsuranceCharge
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsOtherDebitAdjustmentText]:
      otherDebitAdjText.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsOtherDebitAdjustmentText
          ]
      )?.text,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsOtherCreditAdjustmentText]:
      otherCreditAdjText.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsOtherCreditAdjustmentText
          ]
      )?.text,

    [MethodFieldParameterEnum.ManageStatementProductionSettingsCashAdvanceFinanceChargesText]:
      cashAdvFinanceChargesText.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsCashAdvanceFinanceChargesText
          ]
      )?.text,

    [MethodFieldParameterEnum.ManageStatementProductionSettingsMerchandiseItemChargeText]:
      merchandiseItemChargeText.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsMerchandiseItemChargeText
          ]
      )?.text,

    [MethodFieldParameterEnum.ManageStatementProductionSettingsCashItemChargesText]:
      cashItemChargesText.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsCashItemChargesText
          ]
      )?.text,

    [MethodFieldParameterEnum.ManageStatementProductionSettingsMerchandiseFinanceChargesText]:
      merchandiseFinanceChargesText.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsMerchandiseFinanceChargesText
          ]
      )?.text,

    [MethodFieldParameterEnum.ManageStatementProductionSettingsLateChargesText]:
      lateChargesText.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsLateChargesText
          ]
      )?.text,

    [MethodFieldParameterEnum.ManageStatementProductionSettingsOverlimitChargesText]:
      overlimitChargesText.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsOverlimitChargesText
          ]
      )?.text,

    [MethodFieldParameterEnum.ManageStatementProductionSettingsMinimumFinanceChargesText]:
      minimumFinanceChargesText.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsMinimumFinanceChargesText
          ]
      )?.text,

    [MethodFieldParameterEnum.ManageStatementProductionSettingsActivityFeesText]:
      activityFeesText.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsActivityFeesText
          ]
      )?.text,

    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsuranceText]:
      creditLifeInsuranceText.find(
        (o: MagicKeyValue) =>
          o.code ===
          data[
            MethodFieldParameterEnum
              .ManageStatementProductionSettingsCreditLifeInsuranceText
          ]
      )?.text
  };
  return mappingData;
};

export const defaultValue = (metadata: any) => {
  const {
    creditLifeInsurancePlan1,
    creditLifeInsurancePlan2,
    creditLifeInsurancePlan3,
    creditLifeInsurancePlan4,
    creditLifeInsurancePlan5,
    creditLifeInsurancePlan6,
    creditLifeInsurancePlan7,
    creditLifeInsurancePlan8,
    creditLifeInsurancePlan9,
    cashAdvanceFinanceCharges,
    cashItemCharges,
    merchandiseFinanceCharges,
    merchandiseItemCharges,
    lateCharges,
    overlimitCharge,
    creditLifeInsuranceRefunds,
    optionalIssuerFeeMessageText,
    saleAmountAdjText,
    cashAdvanceAmountAdjText,
    returnAmountAdjText,
    paymentAmountAdjText,
    chargeOffPrincipalAdjText,
    chargeOffFinanceChargesAdjText,
    miscSpecificCreditAdjText,
    chargeOffSmallBalanceAdjText,
    chargeOffCreditLifeInsuranceAdjText,
    chargeOffCashInterestRefundText,
    chargeOffLateChargeRefundText,
    chargeOffMerchandiseInterestRefundText,
    chargeOffCashItemRefundText,
    chargeOffMerchandiseItemRefundText,
    chargeOffOverlimitFeeRefundText,
    saleReversalText,
    cashAdvanceReversalText,
    returnReversalText,
    paymentReversalText,
    lateFeeWaiverText,
    normalTermsDateId,
    cpIcBp,
    cpIcId,
    cpIcIi,
    cpIcIm,
    cpIcIp,
    cpIcIr,
    cpIcMf,
    cpIcVi,
    cpIoAc,
    cpAoCi,
    cpIoMc,
    cpIoMi,
    cpPfLc,
    cpPfOc,
    cpPfRc,
    cpPoMp,
    cpIcMe,
    prevCycCashAdv,
    prevCycMerchandise,
    prevCycCashItemCharge,
    prevCycMdsItemCharge,
    minimumFinanceCharges,
    activityFee,
    prevCycLateCharge,
    prevCycOverLimitCharge,
    creditLifeInsuranceCharge,
    otherCreditAdjText,
    otherDebitAdjText,
    cashAdvFinanceChargesText,
    merchandiseItemChargeText,
    cashItemChargesText,
    merchandiseFinanceChargesText,
    lateChargesText,
    overlimitChargesText,
    minimumFinanceChargesText,
    activityFeesText,
    creditLifeInsuranceText
  } = metadata;
  const data = {
    ...METHOD_MSPS_DEFAULT,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance1]:
      creditLifeInsurancePlan1?.find((option: MagicKeyValue) => option.selected)
        ?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance2]:
      creditLifeInsurancePlan2?.find((option: MagicKeyValue) => option.selected)
        ?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance3]:
      creditLifeInsurancePlan3?.find((option: MagicKeyValue) => option.selected)
        ?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance4]:
      creditLifeInsurancePlan4?.find((option: MagicKeyValue) => option.selected)
        ?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance5]:
      creditLifeInsurancePlan5?.find((option: MagicKeyValue) => option.selected)
        ?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance6]:
      creditLifeInsurancePlan6?.find((option: MagicKeyValue) => option.selected)
        ?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance7]:
      creditLifeInsurancePlan7?.find((option: MagicKeyValue) => option.selected)
        ?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance8]:
      creditLifeInsurancePlan8?.find((option: MagicKeyValue) => option.selected)
        ?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance9]:
      creditLifeInsurancePlan9?.find((option: MagicKeyValue) => option.selected)
        ?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCashAdvanceFinanceCharges]:
      cashAdvanceFinanceCharges?.find(
        (option: MagicKeyValue) => option.selected
      )?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCashItemCharges]:
      cashItemCharges?.find((option: MagicKeyValue) => option.selected)?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsMerchandiseFinanceCharges]:
      merchandiseFinanceCharges?.find(
        (option: MagicKeyValue) => option.selected
      )?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsMerchandiseItemCharges]:
      merchandiseItemCharges?.find((option: MagicKeyValue) => option.selected)
        ?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsLateCharge]:
      lateCharges?.find((option: MagicKeyValue) => option.selected)?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsOverlimitCharge]:
      overlimitCharge?.find((option: MagicKeyValue) => option.selected)?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsuranceRefunds]:
      creditLifeInsuranceRefunds?.find(
        (option: MagicKeyValue) => option.selected
      )?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsOptionalIssuerFeeMessageText]:
      optionalIssuerFeeMessageText?.find(
        (option: MagicKeyValue) => option.selected
      )?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsSaleAmountAdjustmentText]:
      saleAmountAdjText?.find((option: MagicKeyValue) => option.selected)?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCashAdvanceAmountAdjustmentText]:
      cashAdvanceAmountAdjText?.find((option: MagicKeyValue) => option.selected)
        ?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsReturnAmountAdjustmentText]:
      returnAmountAdjText?.find((option: MagicKeyValue) => option.selected)
        ?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPaymentAmountAdjustmentText]:
      paymentAmountAdjText?.find((option: MagicKeyValue) => option.selected)
        ?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffPrincipalAdjustmentTextLine1]:
      chargeOffPrincipalAdjText?.find(
        (option: MagicKeyValue) => option.selected
      )?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffFinanceChargesAdjustmentTextLine2]:
      chargeOffFinanceChargesAdjText?.find(
        (option: MagicKeyValue) => option.selected
      )?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsMiscellaneousSpecificCreditAdjustmentText]:
      miscSpecificCreditAdjText?.find(
        (option: MagicKeyValue) => option.selected
      )?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffSmallBalanceAdjustmentText]:
      chargeOffSmallBalanceAdjText?.find(
        (option: MagicKeyValue) => option.selected
      )?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffCreditLifeInsuranceAdjustmentText]:
      chargeOffCreditLifeInsuranceAdjText?.find(
        (option: MagicKeyValue) => option.selected
      )?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffCashInterestRefundText]:
      chargeOffCashInterestRefundText?.find(
        (option: MagicKeyValue) => option.selected
      )?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffLateChargeRefundText]:
      chargeOffLateChargeRefundText?.find(
        (option: MagicKeyValue) => option.selected
      )?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffMerchandiseInterestRefundText]:
      chargeOffMerchandiseInterestRefundText?.find(
        (option: MagicKeyValue) => option.selected
      )?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffCashItemRefundText]:
      chargeOffCashItemRefundText?.find(
        (option: MagicKeyValue) => option.selected
      )?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffMerchandiseItemRefundText]:
      chargeOffMerchandiseItemRefundText?.find(
        (option: MagicKeyValue) => option.selected
      )?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffOverlimitFeeRefundText]:
      chargeOffOverlimitFeeRefundText?.find(
        (option: MagicKeyValue) => option.selected
      )?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsSaleReversalText]:
      saleReversalText?.find((option: MagicKeyValue) => option.selected)?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCashAdvanceReversalText]:
      cashAdvanceReversalText?.find((option: MagicKeyValue) => option.selected)
        ?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsReturnReversalText]:
      returnReversalText?.find((option: MagicKeyValue) => option.selected)
        ?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPaymentReversalText]:
      paymentReversalText?.find((option: MagicKeyValue) => option.selected)
        ?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsLateFeeWaiverText]:
      lateFeeWaiverText?.find((option: MagicKeyValue) => option.selected)?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsNormalTermsDateID]:
      normalTermsDateId?.find((option: MagicKeyValue) => option.selected)?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICBP]:
      cpIcBp[0].code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICID]:
      cpIcId[0].code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICII]:
      cpIcIi[0].code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIM]:
      cpIcIm[0].code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIP]:
      cpIcIp[0].code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIR]:
      cpIcIr[0].code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICMF]:
      cpIcMf[0].code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICVI]:
      cpIcVi[0].code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOAC]:
      cpIoAc[0].code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOCI]:
      cpAoCi[0].code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOMC]:
      cpIoMc[0].code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOMI]:
      cpIoMi[0].code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFLC]:
      cpPfLc[0].code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFOC]:
      cpPfOc[0].code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFRC]:
      cpPfRc[0].code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPPOMP]:
      cpPoMp[0].code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICME]:
      cpIcMe[0].code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycCashAdvance]:
      prevCycCashAdv?.find((option: MagicKeyValue) => option.selected)?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycMerchandise]:
      prevCycMerchandise?.find((option: MagicKeyValue) => option.selected)
        ?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycCashItemCharge]:
      prevCycCashItemCharge?.find((option: MagicKeyValue) => option.selected)
        ?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycMdseItemCharge]:
      prevCycMdsItemCharge?.find((option: MagicKeyValue) => option.selected)
        ?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsMinimumFinanceCharges]:
      minimumFinanceCharges?.find((option: MagicKeyValue) => option.selected)
        ?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsActivityFee]:
      activityFee?.find((option: MagicKeyValue) => option.selected)?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycLateCharge]:
      prevCycLateCharge?.find((option: MagicKeyValue) => option.selected)?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycOverlimitCharge]:
      prevCycOverLimitCharge?.find((option: MagicKeyValue) => option.selected)
        ?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsuranceCharge]:
      creditLifeInsuranceCharge?.find(
        (option: MagicKeyValue) => option.selected
      )?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsOtherCreditAdjustmentText]:
      otherCreditAdjText?.find((option: MagicKeyValue) => option.selected)
        ?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsOtherDebitAdjustmentText]:
      otherDebitAdjText?.find((option: MagicKeyValue) => option.selected)?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCashAdvanceFinanceChargesText]:
      cashAdvFinanceChargesText?.find(
        (option: MagicKeyValue) => option.selected
      )?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsMerchandiseItemChargeText]:
      merchandiseItemChargeText?.find(
        (option: MagicKeyValue) => option.selected
      )?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCashItemChargesText]:
      cashItemChargesText?.find((option: MagicKeyValue) => option.selected)
        ?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsMerchandiseFinanceChargesText]:
      merchandiseFinanceChargesText?.find(
        (option: MagicKeyValue) => option.selected
      )?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsLateChargesText]:
      lateChargesText?.find((option: MagicKeyValue) => option.selected)?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsOverlimitChargesText]:
      overlimitChargesText?.find((option: MagicKeyValue) => option.selected)
        ?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsMinimumFinanceChargesText]:
      minimumFinanceChargesText?.find(
        (option: MagicKeyValue) => option.selected
      )?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsActivityFeesText]:
      activityFeesText?.find((option: MagicKeyValue) => option.selected)?.code,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsuranceText]:
      creditLifeInsuranceText?.find((option: MagicKeyValue) => option.selected)
        ?.code
  };
  return data;
};
