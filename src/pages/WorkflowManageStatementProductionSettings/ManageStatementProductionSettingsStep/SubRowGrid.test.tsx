import { renderComponent } from 'app/utils';
import React from 'react';
import { parameterGroup, parameterList } from './newMethodParameterList';
import SubRowGrid from './SubRowGrid';

const t = (value: any) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

jest.mock('./SubGridPreviewMethod', () => ({
  __esModule: true,
  default: () => <div>SubGridPreviewMethod</div>
}));

describe('pages > WorkflowManageStatementProductionSettings > ManageStatementProductionSettingsStep > SubRowGrid', () => {
  it('render', async () => {
    const original = {
      id: '',
      name: 'aaaa',
      comment: '',
      methodType: 'MODELEDMETHOD',
      modeledFrom: {
        id: '312689',
        name: 'at consetetur',
        versions: [{ id: '1032937' }]
      },
      versionParameters: [
        {
          name: 'credit.balance.hold.options',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'where.to.send.request.payments',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'use.fixed.minimum.payment.due.date',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'use.fixed.minimum.payment.due.date.table.id',
          newValue: '',
          previousValue: ''
        },
        { name: 'annual.interest.display', newValue: '', previousValue: '' },
        {
          name: 'include.late.charges.in.annual.interest.display',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'include.cash.item.in.annual.interest.display',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'include.merchandise.items.in.annual.interest.display',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'include.overlimit.fees.in.annual.interest.display',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'includes.statement.production.fees.in.annual.interest.display',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'produce.statements.for.inactive.deleted.accounts',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'us.inactive.account.external.status.control',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'non.us.inactive.account.external.status.control',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'pricing.strategy.change.statement.production',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'minimum.amount.for.inactive.deleted.account.statements',
          newValue: '',
          previousValue: ''
        },
        { name: 'status.b.bankrupt', newValue: '', previousValue: '' },
        {
          name: 'status.a.authorization.prohibited',
          newValue: '',
          previousValue: ''
        },
        { name: 'status.c.closed', newValue: '', previousValue: '' },
        { name: 'status.e.revoked', newValue: '', previousValue: '' },
        { name: 'status.f.frozen', newValue: '', previousValue: '' },
        {
          name: 'status.i.interest.prohibited',
          newValue: '',
          previousValue: ''
        },
        { name: 'status.l.lost', newValue: '', previousValue: '' },
        { name: 'status.u.stolen', newValue: '', previousValue: '' },
        { name: 'status.blank', newValue: '', previousValue: '' },
        {
          name: 'credit.life.insurance.text.id.plan.1',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'credit.life.insurance.text.id.plan.2',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'credit.life.insurance.text.id.plan.3',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'credit.life.insurance.text.id.plan.4',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'credit.life.insurance.text.id.plan.5',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'credit.life.insurance.text.id.plan.6',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'credit.life.insurance.text.id.plan.7',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'credit.life.insurance.text.id.plan.8',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'credit.life.insurance.text.id.plan.9',
          newValue: '',
          previousValue: ''
        },
        { name: 'refund.adj.cash.advance', newValue: '', previousValue: '' },
        {
          name: 'refund.adj.cash.item.charge',
          newValue: '',
          previousValue: ''
        },
        { name: 'refund.adj.merchandise', newValue: '', previousValue: '' },
        {
          name: 'refund.adj.mdse.item.charge',
          newValue: '',
          previousValue: ''
        },
        { name: 'refund.adj.late.charge', newValue: '', previousValue: '' },
        {
          name: 'refund.adj.overlimit.charge',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'credit.life.insurance.refunds',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'turn.off.paper.after.edelivery.enrollment.number.of.months',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'turn.on.paper.for.delinquent.accounts.number.of.months',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'return.to.edelivery.after.curing.delinquency.number.of.months',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'delinquent.account.unenrollment.number.of.months',
          newValue: '',
          previousValue: ''
        },
        { name: 'ebill.language.options', newValue: '', previousValue: '' },
        {
          name: 'finance.charge.statement.display.options',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'display.airline.transaction.details.on.statement',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'cardholder.names.on.statement',
          newValue: '',
          previousValue: ''
        },
        { name: 'reversal.display.option', newValue: '', previousValue: '' },
        { name: 'late.charge.date.display', newValue: '', previousValue: '' },
        {
          name: 'overlimit.charge.date.display',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'foreign.currency.display.options',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'fee.record.indicator.on.cis.screens',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'optional.issuer.fee.statement.display.code',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'optional.issuer.fee.message.text',
          newValue: '',
          previousValue: ''
        },
        { name: 'cp.ic.bp', newValue: '', previousValue: '' },
        { name: 'cp.ic.id', newValue: '', previousValue: '' },
        { name: 'cp.ic.ii', newValue: '', previousValue: '' },
        { name: 'cp.ic.im', newValue: '', previousValue: '' },
        { name: 'cp.ic.ip', newValue: '', previousValue: '' },
        { name: 'cp.ic.ir', newValue: '', previousValue: '' },
        { name: 'cp.ic.mf', newValue: '', previousValue: '' },
        { name: 'cp.ic.vi', newValue: '', previousValue: '' },
        { name: 'cp.io.ac', newValue: '', previousValue: '' },
        { name: 'cp.io.ci', newValue: '', previousValue: '' },
        { name: 'cp.io.mc', newValue: '', previousValue: '' },
        { name: 'cp.io.mi', newValue: '', previousValue: '' },
        { name: 'cp.pf.lc', newValue: '', previousValue: '' },
        { name: 'cp.pf.oc', newValue: '', previousValue: '' },
        { name: 'cp.pf.rc', newValue: '', previousValue: '' },
        { name: 'cp.po.mp', newValue: '', previousValue: '' },
        { name: 'cp.ic.me', newValue: '', previousValue: '' },
        {
          name: 'sale.amount.adjustment.text',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'cash.advance.amount.adjustment.text',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'return.amount.adjustment.text',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'payment.amount.adjustment.text',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'charge.off.principal.adjustment.text.line.1',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'charge.off.finance.charges.adjustment.text.line.2',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'miscellaneous.specific.credit.adjustment.text',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'charge.off.small.balance.adjustment.text',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'charge.off.credit.life.insurance.adjustment.text',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'charge.off.cash.interest.refund.text',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'charge.off.late.charge.refund.text',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'charge.off.merchandise.interest.refund.text',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'charge.off.cash.item.refund.text',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'charge.off.merchandise.item.refund.text',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'charge.off.overlimit.fee.refund.text',
          newValue: '',
          previousValue: ''
        },
        { name: 'sale.reversal.text', newValue: '', previousValue: '' },
        { name: 'cash.advance.reversal.text', newValue: '', previousValue: '' },
        { name: 'return.reversal.text', newValue: '', previousValue: '' },
        { name: 'payment.reversal.text', newValue: '', previousValue: '' },
        { name: 'credit.balance.hold.code', newValue: '', previousValue: '' },
        {
          name: 'print.hard.copy.security.statements',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'scs.statement.override.table.id',
          newValue: '',
          previousValue: ''
        },
        { name: 'late.fee.waiver.text', newValue: '', previousValue: '' },
        {
          name: 'generic.message.above.revolving.balance',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'generic.message.below.revolving.balance',
          newValue: '',
          previousValue: ''
        },
        { name: 'normal.terms.date.id', newValue: '', previousValue: '' },
        {
          name: 'cross.cycle.fee.display.options',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'cross.cycle.interest.display.options',
          newValue: '',
          previousValue: ''
        },
        {
          name: '280.transactions.series.display.options',
          newValue: '',
          previousValue: ''
        },
        { name: 'prev.cyc.cash.advance', newValue: '', previousValue: '' },
        { name: 'prev.cyc.cash.item.charge', newValue: '', previousValue: '' },
        { name: 'prev.cyc.merchandise', newValue: '', previousValue: '' },
        { name: 'prev.cyc.mdse.item.charge', newValue: '', previousValue: '' },
        { name: 'minimum.finance.charges', newValue: '', previousValue: '' },
        { name: 'activity.fee', newValue: '', previousValue: '' },
        { name: 'prev.cyc.late.charge', newValue: '', previousValue: '' },
        { name: 'prev.cyc.overlimit.charge', newValue: '', previousValue: '' },
        {
          name: 'credit.life.insurance.charge',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'other.debit.adjustment.text',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'other.credit.adjustment.text',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'cash.advance.finance.charges.text',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'merchandise.item.charges.text',
          newValue: '',
          previousValue: ''
        },
        { name: 'cash.item.charges.text', newValue: '', previousValue: '' },
        {
          name: 'merchandise.finance.charges.text',
          newValue: '',
          previousValue: ''
        },
        { name: 'late.charges.text', newValue: '', previousValue: '' },
        { name: 'overlimit.charges.text', newValue: '', previousValue: '' },
        {
          name: 'minimum.finance.charges.text',
          newValue: '',
          previousValue: ''
        },
        { name: 'activity.fees.text', newValue: '', previousValue: '' },
        { name: 'credit.life.insurance.text', newValue: '', previousValue: '' }
      ],
      parameters: [
        {
          name: 'credit.balance.hold.options',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'where.to.send.request.payments',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'use.fixed.minimum.payment.due.date',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'use.fixed.minimum.payment.due.date.table.id',
          newValue: '',
          previousValue: ''
        },
        { name: 'annual.interest.display', newValue: '', previousValue: '' },
        {
          name: 'include.late.charges.in.annual.interest.display',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'include.cash.item.in.annual.interest.display',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'include.merchandise.items.in.annual.interest.display',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'include.overlimit.fees.in.annual.interest.display',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'includes.statement.production.fees.in.annual.interest.display',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'produce.statements.for.inactive.deleted.accounts',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'us.inactive.account.external.status.control',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'non.us.inactive.account.external.status.control',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'pricing.strategy.change.statement.production',
          newValue: '',
          previousValue: ''
        },
        {
          name: 'minimum.amount.for.inactive.deleted.account.statements',
          newValue: '',
          previousValue: ''
        },
        { name: 'status.b.bankrupt', newValue: '', previousValue: '' },
        {
          name: 'status.a.authorization.prohibited',
          newValue: '',
          previousValue: ''
        },
        { name: 'status.c.closed', newValue: '', previousValue: '' },
        { name: 'status.e.revoked', newValue: '', previousValue: '' },
        { name: 'status.f.frozen', newValue: '', previousValue: '' },
        {
          name: 'status.i.interest.prohibited',
          newValue: '',
          previousValue: ''
        },
        { name: 'status.l.lost', newValue: '', previousValue: '' },
        { name: 'status.u.stolen', newValue: '', previousValue: '' },
        { name: 'status.blank', newValue: '', previousValue: '' }
      ],
      rowId: 1636346098104,
      serviceSubjectSection: 'PL RT PC'
    };

    const metadata: any = {
      creditBalanceHoldOptions: [
        { code: 'TEXTDFAD', text: 'TEXTDFAD - ' },
        { code: 'TEXTDFAJ', text: 'TEXTDFAJ - ' },
        { code: 'TEXTDFAP', text: 'TEXTDFAP - ' },
        { code: 'TEXTDFAR', text: 'TEXTDFAR - ' },
        { code: 'TEXTDFCA', text: 'TEXTDFCA - ' }
      ],
      whereToSendRequestPayments: [
        { code: 'TEXTDFAD', text: 'TEXTDFAD - ' },
        { code: 'TEXTDFAJ', text: 'TEXTDFAJ - ' },
        { code: 'TEXTDFAP', text: 'TEXTDFAP - ' },
        { code: 'TEXTDFAR', text: 'TEXTDFAR - ' },
        { code: 'TEXTDFCA', text: 'TEXTDFCA - ' }
      ],
      sameDayLostStatusOption: [
        { code: '', text: ' - None' },
        { code: '0', text: '0 - Do not display the promotion description.' },
        {
          code: '1',
          text: '1 - Display the description from the Description parameter.'
        },
        {
          code: '2',
          text: '2 - Display the description from the Alternate Description parameter.'
        },
        {
          code: '3',
          text: '3 - Display the description from the DD, Description Display, text type in the DT, Detail Descriptions, text area of the PCF Text Maintenance feature.'
        },
        {
          code: '4',
          text: '4 - Display the description from the DX, Description Extended, text type in the DT, Detail Descriptions, text area of the PCF Text Maintenance feature.'
        }
      ],
      useFixedMinimumPaymentDueDate: [
        { code: 'TEXTDFAD', text: 'TEXTDFAD - ' },
        { code: 'TEXTDFAJ', text: 'TEXTDFAJ - ' },
        { code: 'TEXTDFAP', text: 'TEXTDFAP - ' },
        { code: 'TEXTDFAR', text: 'TEXTDFAR - ' },
        { code: 'TEXTDFCA', text: 'TEXTDFCA - ' },
        { code: 'TEXTDFCC', text: 'TEXTDFCC - ' },
        { code: 'TEXTDFCF', text: 'TEXTDFCF - ' },
        { code: 'TEXTDFCI', text: 'TEXTDFCI - ' },
        { code: 'TEXTDFCL', text: 'TEXTDFCL - ' },
        { code: 'TEXTDFCP', text: 'TEXTDFCP - ' }
      ],
      useFixedMinimumPaymentDueDateTableId: [
        { code: 'TEXTDFAD', text: 'TEXTDFAD - ' },
        { code: 'TEXTDFAJ', text: 'TEXTDFAJ - ' },
        { code: 'TEXTDFAP', text: 'TEXTDFAP - ' },
        { code: 'TEXTDFAR', text: 'TEXTDFAR - ' },
        { code: 'TEXTDFCA', text: 'TEXTDFCA - ' },
        { code: 'TEXTDFCC', text: 'TEXTDFCC - ' },
        { code: 'TEXTDFCF', text: 'TEXTDFCF - ' },
        { code: 'TEXTDFCI', text: 'TEXTDFCI - ' },
        { code: 'TEXTDFCL', text: 'TEXTDFCL - ' },
        { code: 'TEXTDFCP', text: 'TEXTDFCP - ' }
      ],
      annualInterestDisplay: [
        { code: 'TEXTDFAD', text: 'TEXTDFAD - ' },
        { code: 'TEXTDFAJ', text: 'TEXTDFAJ - ' },
        { code: 'TEXTDFAP', text: 'TEXTDFAP - ' },
        { code: 'TEXTDFAR', text: 'TEXTDFAR - ' },
        { code: 'TEXTDFCA', text: 'TEXTDFCA - ' },
        { code: 'TEXTDFCC', text: 'TEXTDFCC - ' },
        { code: 'TEXTDFCF', text: 'TEXTDFCF - ' },
        { code: 'TEXTDFCI', text: 'TEXTDFCI - ' },
        { code: 'TEXTDFCL', text: 'TEXTDFCL - ' },
        { code: 'TEXTDFCP', text: 'TEXTDFCP - ' },
        { code: 'TEXTDFAD', text: 'TEXTDFAD - ' },
        { code: 'TEXTDFAE', text: 'TEXTDFAE - ' },
        { code: 'TEXTDFAS', text: 'TEXTDFAS - ' },
        { code: 'TEXTDFAR', text: 'TEXTDFAR - ' }
      ],
      includeLateChargesInAnnualInterestDisplay: [
        { code: '', text: ' - None' },
        { code: '0', text: '0 - Do not display the promotion description.' },
        {
          code: '1',
          text: '1 - Display text on statement and service screens.'
        },
        { code: '3', text: '3 - Display text only on service screens.' }
      ],
      includeCashItemInAnnualInterestDisplay: [
        { code: '', text: ' - None' },
        { code: '0', text: '0 - Option not used.' },
        {
          code: '1',
          text: '1 - Apply returns to the oldest balance ID of the promotion ID assigned by your TLP decision tables.'
        },
        {
          code: '2',
          text: '2 - Apply returns to the newest balance ID of the promotion ID assigned by your TLP decision tables.'
        },
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Option not used. Returns follow the credit application process you have established with your payment application tables and Product Control File settings.'
        },
        {
          code: '1',
          text: '1 - Apply returns to the oldest balance ID of the promotion ID assigned by your TLP decision tables.'
        }
      ],
      includeMerchandiseItemInAnnualInterestDisplay: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Exclude this promotional balance from the amount required to meet payoff exception processing.'
        },
        {
          code: '1',
          text: '1 - Include this promotional balance in the amount required to meet payoff exception processing.'
        },
        {
          code: '2',
          text: '2 - Exclude this promotional balance from the amount required to meet payoff exception processing when the delay period end date is greater than the last statement date.'
        },
        {
          code: '3',
          text: '3 - Exclude this promotional balance from the amount required to meet payoff exception processing when the delay period end date is greater than the payoff exception date.'
        }
      ],
      includeOverLimitFeesInAnnualInterestDisplay: [
        { code: 'THDI1031', text: 'THDI1031 - ' },
        { code: 'THDI1032', text: 'THDI1032 - ' },
        { code: 'MTHD2921', text: 'MTHD2921 - ' },
        { code: 'MTHD1921', text: 'MTHD1921 - ' }
      ],
      includesStatementProductionFeesInAnnualInterestDisplay: [
        { code: 'THDI1031', text: 'THDI1031 - ' },
        { code: 'THDI1032', text: 'THDI1032 - ' },
        { code: 'METHOD2', text: 'METHOD2 - ' },
        { code: 'MTHD1921', text: 'MTHD1921 - ' }
      ],
      produceStatementsForInactiveDeletedAccounts: [
        { code: 'THDI1031', text: 'THDI1031 - ' },
        { code: 'THDI1032', text: 'THDI1032 - ' },
        { code: 'METHOD2', text: 'METHOD2 - ' },
        { code: 'MTHD1921', text: 'MTHD1921 - ' }
      ],
      usInactiveAccountExternalStatusControl: [
        { code: '', text: ' - None' },
        { code: '1', text: '1 - 01' },
        { code: '2', text: '2 - 02' },
        { code: '3', text: '3 - 03' },
        { code: '4', text: '4 - 04' },
        { code: '5', text: '5 - 05' },
        { code: '6', text: '6 - 06' },
        { code: '7', text: '7 - 07' },
        { code: '8', text: '8 - 08' },
        { code: '9', text: '9 - 09' },
        { code: '10', text: '10 - 10' }
      ],
      nonUsInactiveAccountExternalStatusControl: [
        { code: 'THDI1031', text: 'THDI1031 - ' },
        { code: 'THDI1032', text: 'THDI1032 - ' },
        { code: 'MTHD2921', text: 'MTHD2921 - ' },
        { code: 'MTHD1921', text: 'MTHD1921 - ' }
      ],
      pricingStrategyChangeStatementProduction: [
        { code: '', text: ' - None' },
        {
          code: 'D',
          text: 'D - Reamortize the minimum payment due based on the current balance amount of the promotion.'
        },
        {
          code: 'F',
          text: 'F - Reamortize the minimum payment due for the remaining payment term.'
        },
        {
          code: 'H',
          text: 'H - Reamortize the minimum payment due amount based on the high balance amount of the promotion.'
        },
        {
          code: '0',
          text: '0 - Do not use this parameter to calculate minimum payment.'
        },
        {
          code: '1',
          text: '1 - Calculate fixed minimum payment by dividing the amount at posting by the setting in the Payout Period parameter in this section. Round up to the next whole dollar.'
        },
        {
          code: '2',
          text: '2 - Calculate fixed minimum payment by multiplying the amount at posting by the setting in the Standard Minimum Payments Rate parameter in this section. Round up to the next whole dollar.'
        },
        {
          code: '3',
          text: '3 - Calculate decreasing minimum payment by multiplying the outstanding principal by the setting in the Standard Minimum Payments Rate parameter at cycle time. Round according to the setting in the Rounding parameter in the Minimum Payment Due section (CP PO MP) of the Product Control File.'
        },
        {
          code: '4',
          text: '4 - Calculate decreasing minimum payment by multiplying the outstanding principal and interest by the setting in the Standard Minimum Payments Rate parameter at cycle time. Round according to the setting in the Rounding parameter in the Minimum Payment Due section (CP PO MP).'
        },
        {
          code: '5',
          text: '5 - Calculate fixed minimum payment by amortizing the principal and interest over the life of the promotion. Use the monthly installment as the minimum payment due.'
        },
        {
          code: '6',
          text: '6 - Calculate the minimum payment due (MPD) by using current total-amount-owed (TAO), the annual interest rate (INT), and the remaining payment period (N).'
        },
        { code: '7', text: '7 - Reserved for future use.' },
        {
          code: '8',
          text: '8 - Combine all promotional balances assigned to this calculation method. Calculate the fixed minimum payment due by using the charge parameter in the Fixed Minimum Payment section (CP PO FM) of the Product Control File that corresponds to the first amount field greater than the combined promotional balance for the designated method to arrive at an amount.'
        },
        {
          code: '9',
          text: '9 - Calculate fixed minimum payment due (MPD) by amortizing the principal, fees, and interest over the life of the promotion. Use the monthly installment as the minimum payment due.'
        }
      ],
      statusAAuthorizationProhibited: [
        { code: 'THDI1031', text: 'THDI1031 - ' },
        { code: 'THDI1032', text: 'THDI1032 - ' },
        { code: 'MTHD2921', text: 'MTHD2921 - ' },
        { code: 'MTHD1921', text: 'MTHD1921 - ' }
      ],
      statusBBankrupt: [{ code: 'loanNewValue', text: 'THDI1031 - ' }],
      statusCClosed: [{ code: '21', text: '21' }],
      statusERevoked: [
        { code: '', text: ' - None' },
        { code: 'B', text: 'B - Round the calculation to the higher penny.' },
        { code: '5', text: '5 - Round to nearest penny.' }
      ],
      statusFFrozen: [
        { code: '873', text: '873' },
        { code: '21', text: '21' }
      ],
      statusIInterestProhibited: [{ code: '873', text: '873' }],
      statusLLost: [{ code: '77', text: '77' }],
      statusUStolen: [
        { code: '', text: ' - None' },
        { code: '0', text: '0 - Do not use this option.' },
        {
          code: '1',
          text: '1 - Calculate discount based on the promotional discount rate only.'
        },
        {
          code: '2',
          text: '2 - Calculate discount based on adding the promotional discount rate to the merchant qualifying rate.'
        },
        { code: '77', text: '77' }
      ],
      statusBlank: [
        { code: '', text: ' - None' },
        { code: '0', text: '0 - Do not use this option.' },
        {
          code: '1',
          text: '1 - If the promotional balance returns to revolving, use the interest rate at the next level up, the plan rate if one exists, or the revolving rate. Combine with other balances into a single average daily balance at the plan or revolving level. Maintain the promotion on the account record for the length of time specified in the Number of Retention Months parameter in this section.'
        }
      ],
      minimumAmountForInactiveDeletedAccounts: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      creditLifeInsurancePlan1: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      creditLifeInsurancePlan2: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      creditLifeInsurancePlan3: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      creditLifeInsurancePlan4: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      creditLifeInsurancePlan5: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      creditLifeInsurancePlan6: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      creditLifeInsurancePlan7: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      creditLifeInsurancePlan8: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      creditLifeInsurancePlan9: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      cashAdvanceFinanceCharges: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      cashItemCharges: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      merchandiseFinanceCharges: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      merchandiseItemCharges: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      lateCharges: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      overlimitCharge: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      creditLifeInsuranceRefunds: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      eBillLanguageOptions: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      financeChargeStatementDisplayOptions: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      displayAirlineTransaction: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      cardHolderNamesOnStatement: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      reversalDisplayOption: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      lateChargeDateDisplay: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      overlimitChargeDateDisplay: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      foreignCurrencyDisplayOptions: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      feeRecordIndicator: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      optionalIssuerFeeStatementDisplayCode: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      optionalIssuerFeeMessageText: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      saleAmountAdjText: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      cashAdvanceAmountAdjText: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      returnAmountAdjText: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      paymentAmountAdjText: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      chargeOffPrincipalAdjText: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      chargeOffFinanceChargesAdjText: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      miscSpecificCreditAdjText: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      chargeOffSmallBalanceAdjText: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      chargeOffCreditLifeInsuranceAdjText: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      chargeOffCashInterestRefundText: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      chargeOffLateChargeRefundText: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      chargeOffMerchandiseInterestRefundText: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      chargeOffCashItemRefundText: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      chargeOffMerchandiseItemRefundText: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      chargeOffOverlimitFeeRefundText: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      saleReversalText: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      cashAdvanceReversalText: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      returnReversalText: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      paymentReversalText: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      creditBalanceHoldCode: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      printHardCopySec: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      cssStatementOverrideTableId: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      lateFeeWaiverText: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      genericMessageAbove: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      genericMessageBelow: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      normalTermsDateId: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      crossCycleFeeDisplayOptions: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      crossCycleInterestDisplayOptions: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      transactionSeriesDisplayOptions: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      prevCycCashAdv: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      prevCycMerchandise: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      prevCycCashItemCharge: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      prevCycMdsItemCharge: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      minimumFinanceCharges: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      activityFee: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      prevCycLateCharge: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      prevCycOverLimitCharge: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      creditLifeInsuranceCharge: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      otherCreditAdjText: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      otherDebitAdjText: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      cashAdvFinanceChargesText: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      merchandiseItemChargeText: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      cashItemChargesText: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      merchandiseFinanceChargesText: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      lateChargesText: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      overlimitChargesText: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      minimumFinanceChargesText: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      activityFeesText: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ],
      creditLifeInsuranceText: [
        { code: '', text: ' - None' },
        {
          code: '0',
          text: '0 - Do not apply penalty pricing at the promotion level.'
        },
        { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
        {
          code: '2',
          text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
        }
      ]
    };

    const wrapper = await renderComponent(
      'testId',
      <SubRowGrid
        metadata={metadata}
        original={original}
        localParameterGroup={parameterGroup}
      />
    );

    expect(
      wrapper.getByText(parameterList[0]?.parameterGroup as any)
    ).toBeInTheDocument();
    expect(
      wrapper.getByText(parameterList[1]?.parameterGroup as any)
    ).toBeInTheDocument();
    expect(
      wrapper.getByText(parameterList[2]?.parameterGroup as any)
    ).toBeInTheDocument();
  });
});
