import { InlineMessage, useTranslation } from 'app/_libraries/_dls';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React from 'react';
import ConfigureParameters from './ConfigureParameters';
import ManageStatementProductionSettingsSummary from './Summary';
import parseFormValues from './_parseFormValues';

export interface ManageStatementProductionSettingsFormValue {
  isValid?: boolean;
  methods?: any[];
}

const ManageStatementProductionSettings: React.FC<
  WorkflowSetupProps<ManageStatementProductionSettingsFormValue>
> = props => {
  const { t } = useTranslation();
  const { isStuck } = props;

  return (
    <div>
      <p className="mt-12 color-grey">
        {t('txt_manage_statement_production_settings_description_step_3')}
      </p>
      <div className="pt-24 mt-24 border-top">
        {isStuck && (
          <InlineMessage className="mb-24" variant="danger" withIcon>
            {t('txt_step_stuck_move_forward_message')}
          </InlineMessage>
        )}
        <ConfigureParameters {...props} />
      </div>
    </div>
  );
};

const ExtraStaticManageStatementProductionSettingsStep =
  ManageStatementProductionSettings as WorkflowSetupStaticProp;

ExtraStaticManageStatementProductionSettingsStep.summaryComponent =
  ManageStatementProductionSettingsSummary;
ExtraStaticManageStatementProductionSettingsStep.defaultValues = {
  isValid: false,
  methods: []
};

ExtraStaticManageStatementProductionSettingsStep.parseFormValues =
  parseFormValues;

export default ExtraStaticManageStatementProductionSettingsStep;
