import EnhanceDropdownList from 'app/components/EnhanceDropdownList';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import { matchSearchValue } from 'app/helpers';
import {
  ColumnType,
  ComboBox,
  Grid,
  NumericTextBox,
  useTranslation
} from 'app/_libraries/_dls';
import { orderBy } from 'app/_libraries/_dls/lodash';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import {
  formatTruncate,
  viewMoreInfo
} from 'pages/_commons/Utils/formatGridField';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { LIST_NUMERIC_FORM } from './constant';
import {
  LIST_CIT_DROP_DRAG_FORM,
  LIST_COMBOBOX_FORM,
  LIST_DROPDOWN_FORM,
  LIST_NUMERIC_PERCENT_FORM
} from './helper';
import {
  ParameterGroup,
  ParameterList,
  ParameterListIds
} from './newMethodParameterList';

export interface ManageStatementElementData {
  creditBalanceHoldOptions: RefData[];
  whereToSendRequestPayments: RefData[];
  sameDayLostStatusOption: RefData[];
  useFixedMinimumPaymentDueDate: RefData[];
  useFixedMinimumPaymentDueDateTableId: RefData[];
  annualInterestDisplay: RefData[];
  includeLateChargesInAnnualInterestDisplay: RefData[];
  includeCashItemInAnnualInterestDisplay: RefData[];
  includeMerchandiseItemInAnnualInterestDisplay: RefData[];
  includeOverLimitFeesInAnnualInterestDisplay: RefData[];
  includesStatementProductionFeesInAnnualInterestDisplay: RefData[];
  produceStatementsForInactiveDeletedAccounts: RefData[];
  usInactiveAccountExternalStatusControl: RefData[];
  nonUsInactiveAccountExternalStatusControl: RefData[];
  pricingStrategyChangeStatementProduction: RefData[];
  statusAAuthorizationProhibited: RefData[];
  statusBBankrupt: RefData[];
  statusCClosed: RefData[];
  statusERevoked: RefData[];
  statusFFrozen: RefData[];
  statusIInterestProhibited: RefData[];
  statusLLost: RefData[];
  statusUStolen: RefData[];
  statusBlank: RefData[];
  creditLifeInsurancePlan1: RefData[];
  creditLifeInsurancePlan2: RefData[];
  creditLifeInsurancePlan3: RefData[];
  creditLifeInsurancePlan4: RefData[];
  creditLifeInsurancePlan5: RefData[];
  creditLifeInsurancePlan6: RefData[];
  creditLifeInsurancePlan7: RefData[];
  creditLifeInsurancePlan8: RefData[];
  creditLifeInsurancePlan9: RefData[];
  cashAdvanceFinanceCharges: RefData[];
  cashItemCharges: RefData[];
  merchandiseFinanceCharges: RefData[];
  merchandiseItemCharges: RefData[];
  lateCharges: RefData[];
  overlimitCharge: RefData[];
  creditLifeInsuranceRefunds: RefData[];
  eBillLanguageOptions: RefData[];
  financeChargeStatementDisplayOptions: RefData[];
  displayAirlineTransaction: RefData[];
  cardHolderNamesOnStatement: RefData[];
  reversalDisplayOption: RefData[];
  lateChargeDateDisplay: RefData[];
  overlimitChargeDateDisplay: RefData[];
  foreignCurrencyDisplayOptions: RefData[];
  feeRecordIndicator: RefData[];
  optionalIssuerFeeStatementDisplayCode: RefData[];
  optionalIssuerFeeMessageText: RefData[];
  saleAmountAdjText: RefData[];
  cashAdvanceAmountAdjText: RefData[];
  returnAmountAdjText: RefData[];
  paymentAmountAdjText: RefData[];
  chargeOffPrincipalAdjText: RefData[];
  chargeOffFinanceChargesAdjText: RefData[];
  miscSpecificCreditAdjText: RefData[];
  chargeOffSmallBalanceAdjText: RefData[];
  chargeOffCreditLifeInsuranceAdjText: RefData[];
  chargeOffCashInterestRefundText: RefData[];
  chargeOffLateChargeRefundText: RefData[];
  chargeOffMerchandiseInterestRefundText: RefData[];
  chargeOffCashItemRefundText: RefData[];
  chargeOffMerchandiseItemRefundText: RefData[];
  chargeOffOverlimitFeeRefundText: RefData[];
  saleReversalText: RefData[];
  cashAdvanceReversalText: RefData[];
  returnReversalText: RefData[];
  paymentReversalText: RefData[];
  cpIcBp: RefData[];
  cpIcId: RefData[];
  cpIcIi: RefData[];
  cpIcIm: RefData[];
  cpIcIp: RefData[];
  cpIcIr: RefData[];
  cpIcMf: RefData[];
  cpIcVi: RefData[];
  cpIoAc: RefData[];
  cpAoCi: RefData[];
  cpIoMc: RefData[];
  cpIoMi: RefData[];
  cpPfLc: RefData[];
  cpPfOc: RefData[];
  cpPfRc: RefData[];
  cpPoMp: RefData[];
  cpIcMe: RefData[];
  creditBalanceHoldCode: RefData[];
  printHardCopySec: RefData[];
  cssStatementOverrideTableId: RefData[];
  lateFeeWaiverText: RefData[];
  genericMessageAbove: RefData[];
  genericMessageBelow: RefData[];
  normalTermsDateId: RefData[];
  crossCycleFeeDisplayOptions: RefData[];
  crossCycleInterestDisplayOptions: RefData[];
  transactionSeriesDisplayOptions: RefData[];
  prevCycCashAdv: RefData[];
  prevCycMerchandise: RefData[];
  prevCycCashItemCharge: RefData[];
  prevCycMdsItemCharge: RefData[];
  minimumFinanceCharges: RefData[];
  activityFee: RefData[];
  prevCycLateCharge: RefData[];
  prevCycOverLimitCharge: RefData[];
  creditLifeInsuranceCharge: RefData[];
  otherCreditAdjText: RefData[];
  otherDebitAdjText: RefData[];
  cashAdvFinanceChargesText: RefData[];
  merchandiseItemChargeText: RefData[];
  cashItemChargesText: RefData[];
  merchandiseFinanceChargesText: RefData[];
  lateChargesText: RefData[];
  overlimitChargesText: RefData[];
  minimumFinanceChargesText: RefData[];
  activityFeesText: RefData[];
  creditLifeInsuranceText: RefData[];
}

interface IProps {
  setDragData: (data: any[]) => void;
  searchValue: string;
  method: WorkflowSetupMethodObjectParams;
  original: ParameterList;
  minDate?: Date;
  maxDate?: Date;
  metadata: ManageStatementElementData;
  localParameterGroup: Record<ParameterListIds, ParameterGroup[]>;
  onChange: (
    field: keyof WorkflowSetupMethodObjectParams,
    isRefData?: boolean
  ) => (e: any) => void;
  handleChangeCITForm: (data: any) => void;
}

const SubGridNewMethod: React.FC<IProps> = ({
  searchValue,
  method,
  original,
  metadata,
  onChange,
  setDragData,
  handleChangeCITForm,
  localParameterGroup
}) => {
  const { t } = useTranslation();

  const { width } = useSelectWindowDimension();

  const {
    creditBalanceHoldOptions,
    whereToSendRequestPayments,
    sameDayLostStatusOption,
    useFixedMinimumPaymentDueDate,
    useFixedMinimumPaymentDueDateTableId,
    annualInterestDisplay,
    includeLateChargesInAnnualInterestDisplay,
    includeCashItemInAnnualInterestDisplay,
    includeMerchandiseItemInAnnualInterestDisplay,
    includeOverLimitFeesInAnnualInterestDisplay,
    includesStatementProductionFeesInAnnualInterestDisplay,
    produceStatementsForInactiveDeletedAccounts,
    usInactiveAccountExternalStatusControl,
    nonUsInactiveAccountExternalStatusControl,
    pricingStrategyChangeStatementProduction,
    statusAAuthorizationProhibited,
    statusBBankrupt,
    statusCClosed,
    statusERevoked,
    statusFFrozen,
    statusIInterestProhibited,
    statusLLost,
    statusUStolen,
    statusBlank,
    creditLifeInsurancePlan1,
    creditLifeInsurancePlan2,
    creditLifeInsurancePlan3,
    creditLifeInsurancePlan4,
    creditLifeInsurancePlan5,
    creditLifeInsurancePlan6,
    creditLifeInsurancePlan7,
    creditLifeInsurancePlan8,
    creditLifeInsurancePlan9,
    cashAdvanceFinanceCharges,
    cashItemCharges,
    merchandiseFinanceCharges,
    merchandiseItemCharges,
    lateCharges,
    overlimitCharge,
    creditLifeInsuranceRefunds,
    eBillLanguageOptions,
    financeChargeStatementDisplayOptions,
    displayAirlineTransaction,
    cardHolderNamesOnStatement,
    reversalDisplayOption,
    lateChargeDateDisplay,
    overlimitChargeDateDisplay,
    foreignCurrencyDisplayOptions,
    feeRecordIndicator,
    optionalIssuerFeeStatementDisplayCode,
    optionalIssuerFeeMessageText,
    saleAmountAdjText,
    cashAdvanceAmountAdjText,
    returnAmountAdjText,
    paymentAmountAdjText,
    chargeOffPrincipalAdjText,
    chargeOffFinanceChargesAdjText,
    miscSpecificCreditAdjText,
    chargeOffSmallBalanceAdjText,
    chargeOffCreditLifeInsuranceAdjText,
    chargeOffCashInterestRefundText,
    chargeOffLateChargeRefundText,
    chargeOffMerchandiseInterestRefundText,
    chargeOffCashItemRefundText,
    chargeOffMerchandiseItemRefundText,
    chargeOffOverlimitFeeRefundText,
    saleReversalText,
    cashAdvanceReversalText,
    returnReversalText,
    paymentReversalText,
    creditBalanceHoldCode,
    printHardCopySec,
    cssStatementOverrideTableId,
    lateFeeWaiverText,
    genericMessageAbove,
    genericMessageBelow,
    normalTermsDateId,
    crossCycleFeeDisplayOptions,
    crossCycleInterestDisplayOptions,
    transactionSeriesDisplayOptions,
    prevCycCashAdv,
    prevCycMerchandise,
    prevCycCashItemCharge,
    prevCycMdsItemCharge,
    minimumFinanceCharges,
    activityFee,
    prevCycLateCharge,
    prevCycOverLimitCharge,
    creditLifeInsuranceCharge,
    otherCreditAdjText,
    otherDebitAdjText,
    cashAdvFinanceChargesText,
    merchandiseItemChargeText,
    cashItemChargesText,
    merchandiseFinanceChargesText,
    lateChargesText,
    overlimitChargesText,
    minimumFinanceChargesText,
    activityFeesText,
    creditLifeInsuranceText
  } = metadata;

  const remappingData: MagicKeyValue = {
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditBalanceHoldOptions]:
      creditBalanceHoldOptions,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsWhereToSendRequestPayments]:
      whereToSendRequestPayments,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsSameDayLostStatusOption]:
      sameDayLostStatusOption,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsUseFixedMinimumPaymentDueDate]:
      useFixedMinimumPaymentDueDate,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsUseFixedMinimumPaymentDueDateTableID]:
      useFixedMinimumPaymentDueDateTableId,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsAnnualInterestDisplay]:
      annualInterestDisplay,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeLateChargesInAnnualInterestDisplay]:
      includeLateChargesInAnnualInterestDisplay,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeCashItemInAnnualInterestDisplay]:
      includeCashItemInAnnualInterestDisplay,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeMerchandiseItemInAnnualInterestDisplay]:
      includeMerchandiseItemInAnnualInterestDisplay,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeOverLimitFeesInAnnualInterestDisplay]:
      includeOverLimitFeesInAnnualInterestDisplay,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeStatementProductionFeesInAnnualInterestDisplay]:
      includesStatementProductionFeesInAnnualInterestDisplay,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsProduceStatementsForInactiveDeletedAccounts]:
      produceStatementsForInactiveDeletedAccounts,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsUSInactiveAccountExternalStatusControl]:
      usInactiveAccountExternalStatusControl,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsNonUSInactiveAccountExternalStatusControl]:
      nonUsInactiveAccountExternalStatusControl,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPricingStrategyChangeStatementProduction]:
      pricingStrategyChangeStatementProduction,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsStatusAAuthorizationProhibited]:
      statusAAuthorizationProhibited,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsStatusBBankrupt]:
      statusBBankrupt,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsStatusCClosed]:
      statusCClosed,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsStatusERevoked]:
      statusERevoked,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsStatusFFrozen]:
      statusFFrozen,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsStatusIInterestProhibited]:
      statusIInterestProhibited,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsStatusLLost]:
      statusLLost,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsStatusUStolen]:
      statusUStolen,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsStatusBlank]:
      statusBlank,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance1]:
      creditLifeInsurancePlan1,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance2]:
      creditLifeInsurancePlan2,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance3]:
      creditLifeInsurancePlan3,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance4]:
      creditLifeInsurancePlan4,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance5]:
      creditLifeInsurancePlan5,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance6]:
      creditLifeInsurancePlan6,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance7]:
      creditLifeInsurancePlan7,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance8]:
      creditLifeInsurancePlan8,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance9]:
      creditLifeInsurancePlan9,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCashAdvanceFinanceCharges]:
      cashAdvanceFinanceCharges,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCashItemCharges]:
      cashItemCharges,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsMerchandiseFinanceCharges]:
      merchandiseFinanceCharges,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsMerchandiseItemCharges]:
      merchandiseItemCharges,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsLateCharge]:
      lateCharges,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsOverlimitCharge]:
      overlimitCharge,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsuranceRefunds]:
      creditLifeInsuranceRefunds,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsEbillLanguageOptions]:
      eBillLanguageOptions,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsFinanceChargeStatementDisplayOptions]:
      financeChargeStatementDisplayOptions,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsDisplayAirlineTransactionDetailsOnStatement]:
      displayAirlineTransaction,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCardholderNamesOnStatement]:
      cardHolderNamesOnStatement,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsReversalDisplayOption]:
      reversalDisplayOption,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsLateChargeDateDisplay]:
      lateChargeDateDisplay,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsOverlimitChargeDateDisplay]:
      overlimitChargeDateDisplay,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsForeignCurrencyDisplayOptions]:
      foreignCurrencyDisplayOptions,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsFeeRecordIndicatorOnCISScreens]:
      feeRecordIndicator,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsOptionalIssuerFeeStatementDisplayCode]:
      optionalIssuerFeeStatementDisplayCode,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsOptionalIssuerFeeMessageText]:
      optionalIssuerFeeMessageText,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsSaleAmountAdjustmentText]:
      saleAmountAdjText,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCashAdvanceAmountAdjustmentText]:
      cashAdvanceAmountAdjText,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsReturnAmountAdjustmentText]:
      returnAmountAdjText,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPaymentAmountAdjustmentText]:
      paymentAmountAdjText,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffPrincipalAdjustmentTextLine1]:
      chargeOffPrincipalAdjText,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffFinanceChargesAdjustmentTextLine2]:
      chargeOffFinanceChargesAdjText,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsMiscellaneousSpecificCreditAdjustmentText]:
      miscSpecificCreditAdjText,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffSmallBalanceAdjustmentText]:
      chargeOffSmallBalanceAdjText,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffCreditLifeInsuranceAdjustmentText]:
      chargeOffCreditLifeInsuranceAdjText,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffCashInterestRefundText]:
      chargeOffCashInterestRefundText,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffLateChargeRefundText]:
      chargeOffLateChargeRefundText,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffMerchandiseInterestRefundText]:
      chargeOffMerchandiseInterestRefundText,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffCashItemRefundText]:
      chargeOffCashItemRefundText,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffMerchandiseItemRefundText]:
      chargeOffMerchandiseItemRefundText,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffOverlimitFeeRefundText]:
      chargeOffOverlimitFeeRefundText,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsSaleReversalText]:
      saleReversalText,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCashAdvanceReversalText]:
      cashAdvanceReversalText,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsReturnReversalText]:
      returnReversalText,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPaymentReversalText]:
      paymentReversalText,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditBalanceHoldCode]:
      creditBalanceHoldCode,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPrintHardCopySecurityStatements]:
      printHardCopySec,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsSCSStatementOverrideTableID]:
      cssStatementOverrideTableId,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsLateFeeWaiverText]:
      lateFeeWaiverText,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsGenericMessageAboveRevolvingBalance]:
      genericMessageAbove,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsGenericMessageBelowRevolvingBalance]:
      genericMessageBelow,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsNormalTermsDateID]:
      normalTermsDateId,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCrossCycleFeeDisplayOptions]:
      crossCycleFeeDisplayOptions,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCrossCycleInterestDisplayOptions]:
      crossCycleInterestDisplayOptions,
    [MethodFieldParameterEnum.ManageStatementProductionSettings280TransactionsSeriesDisplayOptions]:
      transactionSeriesDisplayOptions,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycCashAdvance]:
      prevCycCashAdv,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycCashItemCharge]:
      prevCycCashItemCharge,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycMerchandise]:
      prevCycMerchandise,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycMdseItemCharge]:
      prevCycMdsItemCharge,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsMinimumFinanceCharges]:
      minimumFinanceCharges,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsActivityFee]:
      activityFee,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycLateCharge]:
      prevCycLateCharge,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycOverlimitCharge]:
      prevCycOverLimitCharge,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsuranceCharge]:
      creditLifeInsuranceCharge,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsOtherDebitAdjustmentText]:
      otherDebitAdjText,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsOtherCreditAdjustmentText]:
      otherCreditAdjText,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCashAdvanceFinanceChargesText]:
      cashAdvFinanceChargesText,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsMerchandiseItemChargeText]:
      merchandiseItemChargeText,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCashItemChargesText]:
      cashItemChargesText,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsMerchandiseFinanceChargesText]:
      merchandiseFinanceChargesText,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsLateChargesText]:
      lateChargesText,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsOverlimitChargesText]:
      overlimitChargesText,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsMinimumFinanceChargesText]:
      minimumFinanceChargesText,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsActivityFeesText]:
      activityFeesText,
    [MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsuranceText]:
      creditLifeInsuranceText
  };

  const data = useMemo(
    () =>
      localParameterGroup[original.id].filter(p =>
        matchSearchValue(
          [p.fieldName, p.moreInfoText, p.onlinePCF],
          searchValue
        )
      ),
    [searchValue, original.id, localParameterGroup]
  );

  const [gridData, setGridData] = useState<any[]>(data);

  const valueAccessor = (data: Record<string, any>, index: number) => {
    const paramId = data.id as MethodFieldParameterEnum;

    if (LIST_CIT_DROP_DRAG_FORM.includes(paramId)) {
      return <div>{method?.[paramId]?.padStart(2, '0')}</div>;
    }

    if (LIST_DROPDOWN_FORM.includes(paramId)) {
      const value = remappingData[paramId].find(
        (o: MagicKeyValue) => o.code === method?.[paramId]
      );
      return (
        <EnhanceDropdownList
          placeholder={t('txt_select_an_option')}
          dataTestId={`addNewMethod__${paramId}`}
          size="small"
          value={value}
          onChange={onChange(paramId, true)}
          options={remappingData[paramId]}
          tooltipItemCustom={remappingData[paramId].map((o: MagicKeyValue) => ({
            code: o.code,
            tooltipItem: (
              <div
                className="custom-tooltip"
                dangerouslySetInnerHTML={{
                  __html: o.text
                    ?.replace(/\n/g, '</br>')
                    ?.replace(/\t/g, '&emsp;') as any
                }}
              ></div>
            )
          }))}
        />
      );
    }

    if (LIST_COMBOBOX_FORM.includes(paramId)) {
      const value = remappingData?.[paramId]?.find(
        (o: MagicKeyValue) => o.code === method?.[paramId]
      );

      return (
        <ComboBox
          placeholder={t('txt_select_an_option')}
          size="small"
          textField="text"
          value={value}
          onChange={onChange(paramId, true)}
          noResult={t('txt_no_results_found_without_dot')}
        >
          {remappingData?.[paramId].map((item: MagicKeyValue) => {
            return (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            );
          })}
        </ComboBox>
      );
    }

    if (LIST_NUMERIC_FORM.includes(paramId)) {
      return (
        <NumericTextBox
          placeholder={t('txt_enter_a_value')}
          dataTestId={`addNewMethod__${paramId}`}
          size="sm"
          format="n"
          maxLength={2}
          autoFocus={false}
          value={method?.[paramId] as string}
          onChange={onChange(paramId)}
        />
      );
    }

    if (LIST_NUMERIC_PERCENT_FORM.includes(paramId)) {
      return (
        <NumericTextBox
          size="sm"
          dataTestId={`addNewMethod__${paramId}`}
          format="c2"
          showStep={false}
          maxLength={13}
          autoFocus={false}
          value={method?.[paramId] as string}
          onChange={onChange(paramId)}
        />
      );
    }
  };

  const columns: ColumnType[] = [
    {
      id: 'fieldName',
      Header: t('txt_business_name'),
      accessor: formatTruncate(['fieldName']),
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'onlinePCF',
      Header: t('txt_manage_penalty_fee_online_PCF'),
      accessor: formatTruncate(['onlinePCF']),
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'value',
      Header: t('txt_value'),
      accessor: valueAccessor,
      cellBodyProps: { className: 'py-8' },
      width: width < 1280 ? 240 : undefined
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      accessor: viewMoreInfo(['moreInfo'], t),
      width: 105,
      cellBodyProps: { className: 'pt-12 pb-8' }
    }
  ];

  const extendColumns: ColumnType[] = [
    {
      id: 'printOrder',
      Header: t('Print Order'),
      accessor: valueAccessor,
      width: 118,
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'fieldName',
      Header: t('txt_business_name'),
      accessor: formatTruncate(['fieldName']),
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'onlinePCF',
      Header: t('txt_manage_penalty_fee_online_PCF'),
      accessor: formatTruncate(['onlinePCF']),
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      accessor: viewMoreInfo(['moreInfo'], t),
      width: 105,
      cellBodyProps: { className: 'pt-12 pb-8' }
    }
  ];

  const isCITDisclosuresPrintOrderNumber =
    original.id === 'Info_CITDisclosuresPrintOrderNumbers';

  const dynamicColumns = isCITDisclosuresPrintOrderNumber
    ? extendColumns
    : columns;

  useEffect(() => {
    if (!isCITDisclosuresPrintOrderNumber) return;
    setGridData(data);
  }, [setGridData, isCITDisclosuresPrintOrderNumber, data]);

  const dynamicData = isCITDisclosuresPrintOrderNumber
    ? orderBy(
        gridData.map(el => ({
          ...el,
          orderBy: Number((method as any)?.[el.id]?.padStart(1, '0'))
        })),
        ['orderBy'],
        ['asc']
      )
    : data;

  const handleOnChange = useCallback(
    (nextData: any[]) => {
      if (isCITDisclosuresPrintOrderNumber) {
        handleChangeCITForm(nextData);

        setGridData(nextData);
        setDragData(nextData);
      }
    },
    [setDragData, isCITDisclosuresPrintOrderNumber, handleChangeCITForm]
  );
  const tooltipDragList = useMemo(() => {
    if (!searchValue) return [];
    return dynamicData?.map(item => ({
      id: item?.id,
      tooltipProps: {
        element: t(
          'txt_manage_statement_production_settings_disable_dnd_tooltips'
        )
      }
    }));
  }, [searchValue, dynamicData, t]);

  return (
    <div className="p-20">
      {isCITDisclosuresPrintOrderNumber && (
        <p className="mb-16 color-grey">
          {t('txt_manage_statement_production_settings_cit_form_header')}
        </p>
      )}
      <Grid
        variant={
          isCITDisclosuresPrintOrderNumber
            ? {
                id: 'id',
                type: 'dnd',
                isFixedLeft: true,
                cellBodyProps: {
                  className: 'text-center'
                }
              }
            : undefined
        }
        onDataChange={handleOnChange}
        dnd={isCITDisclosuresPrintOrderNumber ? true : false}
        columns={dynamicColumns}
        data={dynamicData}
        dataItemKey={'id'}
        dndReadOnly={Boolean(searchValue)}
        dndButtonConfigList={tooltipDragList}
      />
    </div>
  );
};

export default SubGridNewMethod;
