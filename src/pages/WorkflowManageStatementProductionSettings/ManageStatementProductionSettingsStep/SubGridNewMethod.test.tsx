import { fireEvent } from '@testing-library/dom';
import { renderWithMockStore } from 'app/utils';
import 'app/utils/_mockComponent/mockCanvas';
import * as CommonRedux from 'pages/_commons/redux/Common/select-hooks';
import React from 'react';
import { parameterGroup } from './newMethodParameterList';
import SubGridNewMethod from './SubGridNewMethod';
const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

const mockUseSelectWindowDimension = jest.spyOn(
  CommonRedux,
  'useSelectWindowDimension'
);

const metadata: any = {
  creditBalanceHoldOptions: [
    { code: 'TEXTDFAD', text: 'TEXTDFAD - ' },
    { code: 'TEXTDFAJ', text: 'TEXTDFAJ - ' },
    { code: 'TEXTDFAP', text: 'TEXTDFAP - ' },
    { code: 'TEXTDFAR', text: 'TEXTDFAR - ' },
    { code: 'TEXTDFCA', text: 'TEXTDFCA - ' }
  ],
  whereToSendRequestPayments: [
    { code: 'TEXTDFAD', text: 'TEXTDFAD - ' },
    { code: 'TEXTDFAJ', text: 'TEXTDFAJ - ' },
    { code: 'TEXTDFAP', text: 'TEXTDFAP - ' },
    { code: 'TEXTDFAR', text: 'TEXTDFAR - ' },
    { code: 'TEXTDFCA', text: 'TEXTDFCA - ' }
  ],
  sameDayLostStatusOption: [
    { code: '', text: ' - None' },
    { code: '0', text: '0 - Do not display the promotion description.' },
    {
      code: '1',
      text: '1 - Display the description from the Description parameter.'
    },
    {
      code: '2',
      text: '2 - Display the description from the Alternate Description parameter.'
    },
    {
      code: '3',
      text: '3 - Display the description from the DD, Description Display, text type in the DT, Detail Descriptions, text area of the PCF Text Maintenance feature.'
    },
    {
      code: '4',
      text: '4 - Display the description from the DX, Description Extended, text type in the DT, Detail Descriptions, text area of the PCF Text Maintenance feature.'
    }
  ],
  useFixedMinimumPaymentDueDate: [
    { code: 'TEXTDFAD', text: 'TEXTDFAD - ' },
    { code: 'TEXTDFAJ', text: 'TEXTDFAJ - ' },
    { code: 'TEXTDFAP', text: 'TEXTDFAP - ' },
    { code: 'TEXTDFAR', text: 'TEXTDFAR - ' },
    { code: 'TEXTDFCA', text: 'TEXTDFCA - ' },
    { code: 'TEXTDFCC', text: 'TEXTDFCC - ' },
    { code: 'TEXTDFCF', text: 'TEXTDFCF - ' },
    { code: 'TEXTDFCI', text: 'TEXTDFCI - ' },
    { code: 'TEXTDFCL', text: 'TEXTDFCL - ' },
    { code: 'TEXTDFCP', text: 'TEXTDFCP - ' }
  ],
  useFixedMinimumPaymentDueDateTableId: [
    { code: 'TEXTDFAD', text: 'TEXTDFAD - ' },
    { code: 'TEXTDFAJ', text: 'TEXTDFAJ - ' },
    { code: 'TEXTDFAP', text: 'TEXTDFAP - ' },
    { code: 'TEXTDFAR', text: 'TEXTDFAR - ' },
    { code: 'TEXTDFCA', text: 'TEXTDFCA - ' },
    { code: 'TEXTDFCC', text: 'TEXTDFCC - ' },
    { code: 'TEXTDFCF', text: 'TEXTDFCF - ' },
    { code: 'TEXTDFCI', text: 'TEXTDFCI - ' },
    { code: 'TEXTDFCL', text: 'TEXTDFCL - ' },
    { code: 'TEXTDFCP', text: 'TEXTDFCP - ' }
  ],
  annualInterestDisplay: [
    { code: 'TEXTDFAD', text: 'TEXTDFAD - ' },
    { code: 'TEXTDFAJ', text: 'TEXTDFAJ - ' },
    { code: 'TEXTDFAP', text: 'TEXTDFAP - ' },
    { code: 'TEXTDFAR', text: 'TEXTDFAR - ' },
    { code: 'TEXTDFCA', text: 'TEXTDFCA - ' },
    { code: 'TEXTDFCC', text: 'TEXTDFCC - ' },
    { code: 'TEXTDFCF', text: 'TEXTDFCF - ' },
    { code: 'TEXTDFCI', text: 'TEXTDFCI - ' },
    { code: 'TEXTDFCL', text: 'TEXTDFCL - ' },
    { code: 'TEXTDFCP', text: 'TEXTDFCP - ' },
    { code: 'TEXTDFAD', text: 'TEXTDFAD - ' },
    { code: 'TEXTDFAE', text: 'TEXTDFAE - ' },
    { code: 'TEXTDFAS', text: 'TEXTDFAS - ' },
    { code: 'TEXTDFAR', text: 'TEXTDFAR - ' }
  ],
  includeLateChargesInAnnualInterestDisplay: [
    { code: '', text: ' - None' },
    { code: '0', text: '0 - Do not display the promotion description.' },
    {
      code: '1',
      text: '1 - Display text on statement and service screens.'
    },
    { code: '3', text: '3 - Display text only on service screens.' }
  ],
  includeCashItemInAnnualInterestDisplay: [
    { code: '', text: ' - None' },
    { code: '0', text: '0 - Option not used.' },
    {
      code: '1',
      text: '1 - Apply returns to the oldest balance ID of the promotion ID assigned by your TLP decision tables.'
    },
    {
      code: '2',
      text: '2 - Apply returns to the newest balance ID of the promotion ID assigned by your TLP decision tables.'
    },
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Option not used. Returns follow the credit application process you have established with your payment application tables and Product Control File settings.'
    },
    {
      code: '1',
      text: '1 - Apply returns to the oldest balance ID of the promotion ID assigned by your TLP decision tables.'
    }
  ],
  includeMerchandiseItemInAnnualInterestDisplay: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Exclude this promotional balance from the amount required to meet payoff exception processing.'
    },
    {
      code: '1',
      text: '1 - Include this promotional balance in the amount required to meet payoff exception processing.'
    },
    {
      code: '2',
      text: '2 - Exclude this promotional balance from the amount required to meet payoff exception processing when the delay period end date is greater than the last statement date.'
    },
    {
      code: '3',
      text: '3 - Exclude this promotional balance from the amount required to meet payoff exception processing when the delay period end date is greater than the payoff exception date.'
    }
  ],
  includeOverLimitFeesInAnnualInterestDisplay: [
    { code: 'THDI1031', text: 'THDI1031 - ' },
    { code: 'THDI1032', text: 'THDI1032 - ' },
    { code: 'MTHD2921', text: 'MTHD2921 - ' },
    { code: 'MTHD1921', text: 'MTHD1921 - ' }
  ],
  includesStatementProductionFeesInAnnualInterestDisplay: [
    { code: 'THDI1031', text: 'THDI1031 - ' },
    { code: 'THDI1032', text: 'THDI1032 - ' },
    { code: 'METHOD2', text: 'METHOD2 - ' },
    { code: 'MTHD1921', text: 'MTHD1921 - ' }
  ],
  produceStatementsForInactiveDeletedAccounts: [
    { code: 'THDI1031', text: 'THDI1031 - ' },
    { code: 'THDI1032', text: 'THDI1032 - ' },
    { code: 'METHOD2', text: 'METHOD2 - ' },
    { code: 'MTHD1921', text: 'MTHD1921 - ' }
  ],
  usInactiveAccountExternalStatusControl: [
    { code: '', text: ' - None' },
    { code: '1', text: '1 - 01' },
    { code: '2', text: '2 - 02' },
    { code: '3', text: '3 - 03' },
    { code: '4', text: '4 - 04' },
    { code: '5', text: '5 - 05' },
    { code: '6', text: '6 - 06' },
    { code: '7', text: '7 - 07' },
    { code: '8', text: '8 - 08' },
    { code: '9', text: '9 - 09' },
    { code: '10', text: '10 - 10' }
  ],
  nonUsInactiveAccountExternalStatusControl: [
    { code: 'THDI1031', text: 'THDI1031 - ' },
    { code: 'THDI1032', text: 'THDI1032 - ' },
    { code: 'MTHD2921', text: 'MTHD2921 - ' },
    { code: 'MTHD1921', text: 'MTHD1921 - ' }
  ],
  pricingStrategyChangeStatementProduction: [
    { code: '', text: ' - None' },
    {
      code: 'D',
      text: 'D - Reamortize the minimum payment due based on the current balance amount of the promotion.'
    },
    {
      code: 'F',
      text: 'F - Reamortize the minimum payment due for the remaining payment term.'
    },
    {
      code: 'H',
      text: 'H - Reamortize the minimum payment due amount based on the high balance amount of the promotion.'
    },
    {
      code: '0',
      text: '0 - Do not use this parameter to calculate minimum payment.'
    },
    {
      code: '1',
      text: '1 - Calculate fixed minimum payment by dividing the amount at posting by the setting in the Payout Period parameter in this section. Round up to the next whole dollar.'
    },
    {
      code: '2',
      text: '2 - Calculate fixed minimum payment by multiplying the amount at posting by the setting in the Standard Minimum Payments Rate parameter in this section. Round up to the next whole dollar.'
    },
    {
      code: '3',
      text: '3 - Calculate decreasing minimum payment by multiplying the outstanding principal by the setting in the Standard Minimum Payments Rate parameter at cycle time. Round according to the setting in the Rounding parameter in the Minimum Payment Due section (CP PO MP) of the Product Control File.'
    },
    {
      code: '4',
      text: '4 - Calculate decreasing minimum payment by multiplying the outstanding principal and interest by the setting in the Standard Minimum Payments Rate parameter at cycle time. Round according to the setting in the Rounding parameter in the Minimum Payment Due section (CP PO MP).'
    },
    {
      code: '5',
      text: '5 - Calculate fixed minimum payment by amortizing the principal and interest over the life of the promotion. Use the monthly installment as the minimum payment due.'
    },
    {
      code: '6',
      text: '6 - Calculate the minimum payment due (MPD) by using current total-amount-owed (TAO), the annual interest rate (INT), and the remaining payment period (N).'
    },
    { code: '7', text: '7 - Reserved for future use.' },
    {
      code: '8',
      text: '8 - Combine all promotional balances assigned to this calculation method. Calculate the fixed minimum payment due by using the charge parameter in the Fixed Minimum Payment section (CP PO FM) of the Product Control File that corresponds to the first amount field greater than the combined promotional balance for the designated method to arrive at an amount.'
    },
    {
      code: '9',
      text: '9 - Calculate fixed minimum payment due (MPD) by amortizing the principal, fees, and interest over the life of the promotion. Use the monthly installment as the minimum payment due.'
    }
  ],
  statusAAuthorizationProhibited: [
    { code: 'THDI1031', text: 'THDI1031 - ' },
    { code: 'THDI1032', text: 'THDI1032 - ' },
    { code: 'MTHD2921', text: 'MTHD2921 - ' },
    { code: 'MTHD1921', text: 'MTHD1921 - ' }
  ],
  statusBBankrupt: [{ code: 'loanNewValue', text: 'THDI1031 - ' }],
  statusCClosed: [{ code: '21', text: '21' }],
  statusERevoked: [
    { code: '', text: ' - None' },
    { code: 'B', text: 'B - Round the calculation to the higher penny.' },
    { code: '5', text: '5 - Round to nearest penny.' }
  ],
  statusFFrozen: [
    { code: '873', text: '873' },
    { code: '21', text: '21' }
  ],
  statusIInterestProhibited: [{ code: '873', text: '873' }],
  statusLLost: [{ code: '77', text: '77' }],
  statusUStolen: [
    { code: '', text: ' - None' },
    { code: '0', text: '0 - Do not use this option.' },
    {
      code: '1',
      text: '1 - Calculate discount based on the promotional discount rate only.'
    },
    {
      code: '2',
      text: '2 - Calculate discount based on adding the promotional discount rate to the merchant qualifying rate.'
    },
    { code: '77', text: '77' }
  ],
  statusBlank: [
    { code: '', text: ' - None' },
    { code: '0', text: '0 - Do not use this option.' },
    {
      code: '1',
      text: '1 - If the promotional balance returns to revolving, use the interest rate at the next level up, the plan rate if one exists, or the revolving rate. Combine with other balances into a single average daily balance at the plan or revolving level. Maintain the promotion on the account record for the length of time specified in the Number of Retention Months parameter in this section.'
    }
  ],
  minimumAmountForInactiveDeletedAccounts: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  creditLifeInsurancePlan1: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  creditLifeInsurancePlan2: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  creditLifeInsurancePlan3: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  creditLifeInsurancePlan4: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  creditLifeInsurancePlan5: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  creditLifeInsurancePlan6: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  creditLifeInsurancePlan7: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  creditLifeInsurancePlan8: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  creditLifeInsurancePlan9: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  cashAdvanceFinanceCharges: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  cashItemCharges: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  merchandiseFinanceCharges: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  merchandiseItemCharges: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  lateCharges: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  overlimitCharge: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  creditLifeInsuranceRefunds: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  eBillLanguageOptions: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  financeChargeStatementDisplayOptions: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  displayAirlineTransaction: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  cardHolderNamesOnStatement: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  reversalDisplayOption: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  lateChargeDateDisplay: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  overlimitChargeDateDisplay: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  foreignCurrencyDisplayOptions: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  feeRecordIndicator: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  optionalIssuerFeeStatementDisplayCode: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  optionalIssuerFeeMessageText: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  saleAmountAdjText: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  cashAdvanceAmountAdjText: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  returnAmountAdjText: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  paymentAmountAdjText: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  chargeOffPrincipalAdjText: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  chargeOffFinanceChargesAdjText: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  miscSpecificCreditAdjText: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  chargeOffSmallBalanceAdjText: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  chargeOffCreditLifeInsuranceAdjText: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  chargeOffCashInterestRefundText: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  chargeOffLateChargeRefundText: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  chargeOffMerchandiseInterestRefundText: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  chargeOffCashItemRefundText: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  chargeOffMerchandiseItemRefundText: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  chargeOffOverlimitFeeRefundText: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  saleReversalText: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  cashAdvanceReversalText: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  returnReversalText: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  paymentReversalText: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  creditBalanceHoldCode: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  printHardCopySec: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  cssStatementOverrideTableId: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  lateFeeWaiverText: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  genericMessageAbove: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  genericMessageBelow: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  normalTermsDateId: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  crossCycleFeeDisplayOptions: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  crossCycleInterestDisplayOptions: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  transactionSeriesDisplayOptions: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  prevCycCashAdv: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  prevCycMerchandise: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  prevCycCashItemCharge: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  prevCycMdsItemCharge: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  minimumFinanceCharges: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  activityFee: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  prevCycLateCharge: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  prevCycOverLimitCharge: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  creditLifeInsuranceCharge: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  otherCreditAdjText: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  otherDebitAdjText: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  cashAdvFinanceChargesText: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  merchandiseItemChargeText: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  cashItemChargesText: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  merchandiseFinanceChargesText: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  lateChargesText: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  overlimitChargesText: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  minimumFinanceChargesText: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  activityFeesText: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ],
  creditLifeInsuranceText: [
    { code: '', text: ' - None' },
    {
      code: '0',
      text: '0 - Do not apply penalty pricing at the promotion level.'
    },
    { code: '1', text: '1 - Apply penalty pricing to the promotion.' },
    {
      code: '2',
      text: '2 - Apply penalty pricing to the promotion if all revolving switches within the promotion are set to revolve, or if the promotion is a grandfathered balance.'
    }
  ]
};

const method: any = {
  'credit.balance.hold.options': 'code',
  'where.to.send.requested.statements': 'code',
  'payment.due.days': 'code',
  'same.day.lost.status.option': 'code',
  'use.fixed.minimum.payment.due.date': 'code',
  'use.fixed.minimum.payment.due.date.table.id': 'code',
  'annual.interest.display': 'code',
  'include.late.charges.in.annual.interest.display': 'code',
  'include.cash.item.in.annual.interest.display': 'code',
  'include.merchandise.item.in.annual.interest.display': 'code',
  'include.overlimit.fees.in.annual.interest.display': 'code',
  'include.statement.production.fees.in.annual.interest.display': 'code',
  'produce.statements.for.inactive.deleted.accounts': 'code',
  'us.inactive.account.external.status.control': 'code',
  'non.us.inactive.account.external.status.control': 'code',
  'pricing.strategy.change.statement.production': 'code',
  'minimum.amount.for.inactive.deleted.account.statements': 'code',
  'status.a.authorization.prohibited': 'code',
  'status.b.bankrupt': 'code',
  'status.c.closed': 'code',
  'status.e.revoked': 'code',
  'status.f.frozen': 'code',
  'status.i.interest.prohibited': 'code',
  'status.l.lost': 'code',
  'status.u.stolen': 'code',
  'status.blank': 'code'
};

const _props = {
  metadata,
  onChange: jest.fn(),
  searchValue: '',
  method,
  localParameterGroup: parameterGroup,
  handleChangeCITForm: jest.fn(),
  setDragData: jest.fn()
};

describe('pages > WorkflowManageStatementProductionSettings > ManageStatementProductionSettingsStep > SubGridNewMethod', () => {
  const mockUseSelectWindowDimensionImplementation = (width: number) => {
    mockUseSelectWindowDimension.mockImplementation(() => ({ width } as any));
  };

  beforeEach(() => {
    mockUseSelectWindowDimensionImplementation(900);
  });

  afterEach(() => {
    mockUseSelectWindowDimension.mockClear();
  });

  it('Info_StatementProductionOptions', async () => {
    const props = {
      ..._props,
      original: {
        id: 'Info_StatementProductionOptions',
        section:
          'txt_manage_statement_production_settings_method_standard_parameters',
        parameterGroup:
          'txt_manage_statement_production_settings_statement_production_options'
      } as any
    };
    const wrapper = await renderWithMockStore(
      <SubGridNewMethod {...props} />,
      {}
    );

    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
    expect(wrapper.getByText('txt_value')).toBeInTheDocument();
    expect(wrapper.getByText('txt_more_info')).toBeInTheDocument();
  });

  it('Info_AnnualInterestDisplay', async () => {
    const props = {
      ..._props,
      original: {
        id: 'Info_AnnualInterestDisplay',
        section:
          'txt_manage_statement_production_settings_method_standard_parameters',
        parameterGroup:
          'txt_manage_statement_production_settings_annual_interest_display'
      } as any
    };
    const wrapper = await renderWithMockStore(
      <SubGridNewMethod {...props} />,
      {}
    );

    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
    expect(wrapper.getByText('txt_value')).toBeInTheDocument();
    expect(wrapper.getByText('txt_more_info')).toBeInTheDocument();
  });

  it('Info_ExternalStatusPrintControl', async () => {
    const props = {
      ..._props,
      original: {
        id: 'Info_ExternalStatusPrintControl',
        section:
          'txt_manage_statement_production_settings_method_standard_parameters',
        parameterGroup:
          'txt_manage_statement_production_settings_external_status_print_control'
      } as any
    };
    const wrapper = await renderWithMockStore(
      <SubGridNewMethod {...props} />,
      {}
    );

    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
    expect(wrapper.getByText('txt_value')).toBeInTheDocument();
    expect(wrapper.getByText('txt_more_info')).toBeInTheDocument();
  });

  it('Info_InactiveDeletedAccount', async () => {
    const props = {
      ..._props,
      original: {
        id: 'Info_InactiveDeletedAccount',
        section:
          'txt_manage_statement_production_settings_method_standard_parameters',
        parameterGroup:
          'txt_manage_statement_production_settings_inactive_deleted_account'
      } as any
    };
    const wrapper = await renderWithMockStore(
      <SubGridNewMethod {...props} />,
      {}
    );
    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();
    expect(wrapper.getByText('txt_value')).toBeInTheDocument();
    expect(wrapper.getByText('txt_more_info')).toBeInTheDocument();
  });

  it('Info_CITDisclosuresPrintOrderNumbers', async () => {
    const props = {
      ..._props,
      original: {
        id: 'Info_CITDisclosuresPrintOrderNumbers',
        section:
          'txt_manage_statement_production_settings_method_standard_parameters',
        parameterGroup:
          'txt_manage_statement_production_settings_cit_disclosures_print_order_numbers'
      } as any
    };
    const wrapper = await renderWithMockStore(
      <SubGridNewMethod {...props} />,
      {}
    );

    const dragElement1 = wrapper.container.querySelector(
      'tbody tr:first-child .draggable'
    )!;
    const dragElement2 = dragElement1.nextSibling!;

    fireEvent.dragStart(dragElement1);
    fireEvent.drop(dragElement2);

    expect(
      wrapper.baseElement.querySelector('.dls-drag-layer')
    ).toBeInTheDocument();

    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();

    expect(wrapper.container.querySelector('i[class="icon icon-drag"]'));

    fireEvent.drag(
      wrapper.container.querySelector('i[class="icon icon-drag"]') as any
    );
    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();

    expect(wrapper.getByText('txt_more_info')).toBeInTheDocument();
  });

  it('Info_CITDisclosuresPrintOrderNumbers', async () => {
    const props = {
      ..._props,
      original: {
        id: 'Info_CITDisclosuresPrintOrderNumbers',
        section:
          'txt_manage_statement_production_settings_method_standard_parameters',
        parameterGroup:
          'txt_manage_statement_production_settings_cit_disclosures_print_order_numbers'
      } as any,
      searchValue: 'CP IC BP'
    };
    const wrapper = await renderWithMockStore(
      <SubGridNewMethod {...props} />,
      {}
    );

    expect(wrapper.getByText('txt_business_name')).toBeInTheDocument();

    expect(wrapper.container.querySelector('i[class="icon icon-drag"]'));

    expect(
      wrapper.getByText('txt_manage_penalty_fee_online_PCF')
    ).toBeInTheDocument();

    expect(wrapper.getByText('txt_more_info')).toBeInTheDocument();
  });
});
