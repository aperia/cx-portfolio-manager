import {
  BadgeColorType,
  FormatTime,
  MethodFieldParameterEnum,
  ServiceSubjectSection
} from 'app/constants/enums';
import {
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import {
  formatTimeDefault,
  mapGridExpandCollapse,
  mappingArrayKey
} from 'app/helpers';
import { useUnsavedChangeRegistry } from 'app/hooks';
import { usePagination } from 'app/hooks/usePagination';
import { Button, ColumnType, Grid, useTranslation } from 'app/_libraries/_dls';
import { isEmpty, orderBy } from 'app/_libraries/_dls/lodash';
import ActionButtons from 'pages/WorkflowManagePenaltyFee/LateChargesStep/ActionButtons';
import RemoveMethodModal from 'pages/WorkflowManagePenaltyFee/LateChargesStep/RemoveMethodModal';
import {
  actionsWorkflowSetup,
  useSelectElementMetadataForMSPS
} from 'pages/_commons/redux/WorkflowSetup';
import {
  formatBadge,
  formatText,
  formatTruncate
} from 'pages/_commons/Utils/formatGridField';
import Paging from 'pages/_commons/Utils/Paging';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';
import { ManageStatementProductionSettingsFormValue } from '.';
import AddNewMethodModal from './AddNewMethodModal';
import { METHOD_MSPS_FIELDS } from './helper';
import MethodListModal from './MethodListModal';
import {
  ParameterGroup,
  parameterGroup,
  ParameterListIds
} from './newMethodParameterList';
import SubRowGrid from './SubRowGrid';

const ConfigureParameters: React.FC<
  WorkflowSetupProps<ManageStatementProductionSettingsFormValue>
> = ({ setFormValues, formValues, stepId, selectedStep, savedAt }) => {
  const { t } = useTranslation();

  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;

  const dispatch = useDispatch();

  const [localParameterGroup, setLocalParameterGroup] =
    useState<Record<ParameterListIds, ParameterGroup[]>>(parameterGroup);

  const [expandedList, setExpandedList] = useState<any[]>([]);
  const [openAddNewMethod, setOpenAddNewMethod] = useState<boolean>(false);
  const [openMethodToModel, setOpenMethodToModel] = useState<boolean>(false);
  const [openCreateNewVersion, setOpenCreateNewVersion] =
    useState<boolean>(false);
  const [removeMethodRowId, setRemoveMethodRowId] =
    useState<undefined | number>(undefined);
  const [editMethodRowId, setEditMethodRowId] =
    useState<undefined | number>(undefined);
  const [editMethodBackId, setEditMethodBackId] =
    useState<undefined | number>(undefined);
  const [hasChange, setHasChange] = useState(false);
  const [keepCreateMethodState, setKeepCreateMethodState] =
    useState<any | undefined>(undefined);

  const metadata = useSelectElementMetadataForMSPS();

  const {
    cpIcBp,
    cpIcId,
    cpIcIi,
    cpIcIm,
    cpIcIp,
    cpIcIr,
    cpIcMf,
    cpIcVi,
    cpIoAc,
    cpAoCi,
    cpIoMc,
    cpIoMi,
    cpPfLc,
    cpPfOc,
    cpPfRc,
    cpPoMp,
    cpIcMe
  } = metadata;

  const mappingData: any = useMemo(
    () => ({
      [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICBP]:
        cpIcBp?.[0]?.code,
      [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICID]:
        cpIcId?.[0]?.code,
      [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICII]:
        cpIcIi?.[0]?.code,
      [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIM]:
        cpIcIm?.[0]?.code,
      [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIP]:
        cpIcIp?.[0]?.code,
      [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIR]:
        cpIcIr?.[0]?.code,
      [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICMF]:
        cpIcMf?.[0]?.code,
      [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICVI]:
        cpIcVi?.[0]?.code,
      [MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOAC]:
        cpIoAc?.[0]?.code,
      [MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOCI]:
        cpAoCi?.[0]?.code,
      [MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOMC]:
        cpIoMc?.[0]?.code,
      [MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOMI]:
        cpIoMi?.[0]?.code,
      [MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFLC]:
        cpPfLc?.[0]?.code,
      [MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFOC]:
        cpPfOc?.[0]?.code,
      [MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFRC]:
        cpPfRc?.[0]?.code,
      [MethodFieldParameterEnum.ManageStatementProductionSettingsCPPOMP]:
        cpPoMp?.[0]?.code,
      [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICME]:
        cpIcMe?.[0]?.code
    }),
    [
      cpIcBp,
      cpIcId,
      cpIcIi,
      cpIcIm,
      cpIcIp,
      cpIcIr,
      cpIcMf,
      cpIcVi,
      cpIoAc,
      cpAoCi,
      cpIoMc,
      cpIoMi,
      cpPfLc,
      cpPfOc,
      cpPfRc,
      cpPoMp,
      cpIcMe
    ]
  );

  const methods = useMemo(
    () => orderBy(formValues?.methods || [], ['name'], ['asc']),
    [formValues?.methods]
  );

  const methodNames = useMemo(
    () => mappingArrayKey(methods, 'name'),
    [methods]
  );

  const onRemoveMethod = useCallback((idx: number) => {
    setRemoveMethodRowId(idx);
  }, []);

  const onEditMethod = useCallback((idx: number) => {
    setEditMethodRowId(idx);
  }, []);

  const onClickAddNewMethod = () => {
    setOpenAddNewMethod(true);
  };

  const onClickMethodToModel = () => {
    setOpenMethodToModel(true);
  };

  const onClickCreateNewVersion = () => {
    setOpenCreateNewVersion(true);
  };

  const closeMethodListModalModal = () => {
    setOpenCreateNewVersion(false);
    setOpenMethodToModel(false);
  };

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_STATEMENT_PRODUCTION_SETTINGS,
      priority: 1
    },
    [hasChange]
  );

  const methodTypeToText = useCallback(
    (type: WorkflowSetupMethodType) => {
      switch (type) {
        case 'NEWVERSION':
          return t('txt_manage_penalty_fee_version_created');
        case 'MODELEDMETHOD':
          return t('txt_manage_penalty_fee_method_modeled');
      }
      return t('txt_manage_penalty_fee_method_created');
    },
    [t]
  );

  const columns: ColumnType[] = useMemo(
    () => [
      {
        id: 'methodName',
        Header: t('txt_method_name'),
        accessor: formatText(['name']),
        width: 105
      },
      {
        id: 'modelOrCreateFrom',
        Header: t('txt_manage_penalty_fee_modeled_or_create_from'),
        accessor: formatText(['modeledFrom', 'name']),
        width: 120
      },
      {
        id: 'comment',
        Header: t('txt_comment_area'),
        accessor: formatTruncate(['comment']),
        width: 312
      },
      {
        id: 'methodType',
        Header: t('txt_manage_penalty_fee_action_taken'),
        accessor: (data, idx) =>
          formatBadge(['methodType'], {
            colorType: BadgeColorType.MethodType,
            noBorder: true
          })(
            {
              ...data,
              methodType: methodTypeToText(data.methodType)
            },
            idx
          ),
        width: 153
      },
      {
        id: 'actions',
        Header: t('txt_actions'),
        accessor: (data: any) => (
          <div className="d-flex justify-content-center">
            <Button
              size="sm"
              variant="outline-primary"
              onClick={() => onEditMethod(data.rowId)}
            >
              {t('txt_edit')}
            </Button>
            <Button
              size="sm"
              variant="outline-danger"
              className="ml-8"
              onClick={() => onRemoveMethod(data.rowId)}
            >
              {t('txt_delete')}
            </Button>
          </div>
        ),
        className: 'text-center',
        cellBodyProps: { className: 'py-8' },
        width: 142
      }
    ],
    [t, methodTypeToText, onEditMethod, onRemoveMethod]
  );

  const [minDateSetting, setMinDateSetting] = useState();
  const [maxDateSetting, setMaxDateSetting] = useState();

  const minDate = useMemo(() => {
    return new Date(formatTimeDefault(minDateSetting!, FormatTime.Date));
  }, [minDateSetting]);

  const maxDate = useMemo(() => {
    return new Date(formatTimeDefault(maxDateSetting!, FormatTime.Date));
  }, [maxDateSetting]);

  const newData = useMemo(() => {
    return {
      ...localParameterGroup,
      ['Info_CITDisclosuresPrintOrderNumbers']: orderBy(
        localParameterGroup?.Info_CITDisclosuresPrintOrderNumbers?.map(el => ({
          ...el,
          orderBy: Number(mappingData?.[el.id]?.padStart(1, '0'))
        })),
        ['orderBy'],
        ['asc']
      )
    };
  }, [localParameterGroup, mappingData]);

  const initFields = useCallback(async () => {
    const responseDateRange = await Promise.resolve(
      dispatch(actionsWorkflowSetup.getWorkflowEffectiveDateOptions({}))
    );

    const { maxDate: maxDateRes, minDate: minDateRes } =
      (responseDateRange as any)?.payload?.data || {};

    setMinDateSetting(minDateRes);
    setMaxDateSetting(maxDateRes);
  }, [dispatch]);

  useEffect(() => {
    initFields();
  }, [initFields]);

  useEffect(() => {
    dispatch(actionsWorkflowSetup.getWorkflowMetadata(METHOD_MSPS_FIELDS));
  }, [dispatch]);

  const {
    total,
    currentPage,
    currentPageSize,
    gridData,
    pageSize,
    onResetToDefaultFilter,
    onPageChange,
    onPageSizeChange
  } = usePagination(methods);

  useEffect(() => {
    const isValid = (formValues?.methods?.length || 0) > 0;
    if (formValues?.isValid === isValid) return;

    keepRef?.current?.setFormValues({ isValid });
  }, [formValues]);

  useEffect(() => {
    if (stepId !== selectedStep) {
      setExpandedList([]);
      onResetToDefaultFilter();
    }
    setHasChange(false);
  }, [stepId, selectedStep, savedAt, onResetToDefaultFilter]);

  const handleOnExpand = (dataKeyList: string[]) => {
    setExpandedList(isEmpty(dataKeyList) ? [] : dataKeyList);
  };

  const updateDragData = (data: any[]) => {
    setLocalParameterGroup(prevData => ({
      ...prevData,
      ['Info_CITDisclosuresPrintOrderNumbers']: data
    }));
  };

  const renderGrid = useMemo(() => {
    if (isEmpty(gridData)) return;
    return (
      <div className="pt-16">
        <Grid
          togglable
          columns={columns}
          data={gridData}
          subRow={({ original }: any) => (
            <SubRowGrid
              original={original}
              metadata={metadata}
              localParameterGroup={newData}
            />
          )}
          dataItemKey="rowId"
          toggleButtonConfigList={gridData.map(
            mapGridExpandCollapse('rowId', t)
          )}
          expandedItemKey={'rowId'}
          expandedList={expandedList}
          onExpand={handleOnExpand}
          scrollable
        />
        {total > 10 && (
          <Paging
            totalItem={total}
            pageSize={currentPageSize}
            page={currentPage}
            onChangePage={onPageChange}
            onChangePageSize={onPageSizeChange}
          />
        )}
      </div>
    );
  }, [
    columns,
    metadata,
    expandedList,
    t,
    total,
    currentPageSize,
    currentPage,
    gridData,
    onPageChange,
    onPageSizeChange,
    newData
  ]);

  return (
    <>
      <ActionButtons
        small={!isEmpty(methods)}
        title="txt_method_list"
        onClickAddNewMethod={onClickAddNewMethod}
        onClickMethodToModel={onClickMethodToModel}
        onClickCreateNewVersion={onClickCreateNewVersion}
      />
      {renderGrid}
      {removeMethodRowId !== undefined && (
        <RemoveMethodModal
          id="removeMethod"
          show
          methodName={methods.find(i => i.rowId === removeMethodRowId)?.name}
          onClose={method => {
            if (method) {
              setFormValues({
                methods: methods.filter(i => i.rowId !== removeMethodRowId)
              });
              setHasChange(true);
            }
            setRemoveMethodRowId(undefined);
          }}
        />
      )}
      {editMethodRowId !== undefined && (
        <AddNewMethodModal
          id="addNewMethod"
          show
          isCreate={false}
          method={methods.find(i => i.rowId === editMethodRowId)}
          methodNames={methodNames}
          minDate={minDate}
          maxDate={maxDate}
          onClose={(method, dragData, isBack, keepState) => {
            if (method) {
              dragData && updateDragData(dragData);
              method.serviceSubjectSection = ServiceSubjectSection.MSPS;
              const newMethds = methods.map(item => {
                if (item.rowId === editMethodRowId) return method;
                return item;
              });
              setFormValues({ methods: newMethds });
              setHasChange(true);

              if (isBack) {
                setEditMethodBackId(editMethodRowId);
                setKeepCreateMethodState(keepState);
              }
            }
            setEditMethodRowId(undefined);
          }}
          localParameterGroup={localParameterGroup}
        />
      )}
      {editMethodBackId && (
        <MethodListModal
          localParameterGroup={localParameterGroup}
          id="editMethodFromModel"
          show
          keepState={keepCreateMethodState}
          methodNames={methodNames}
          isCreateNewVersion={
            methods.find(i => i.rowId === editMethodBackId)!.methodType ===
            'NEWVERSION'
          }
          defaultMethod={methods.find(i => i.rowId === editMethodBackId)}
          onClose={(method, dragData) => {
            if (method) {
              dragData && updateDragData(dragData);
              method.serviceSubjectSection = ServiceSubjectSection.MSPS;
              const newMethds = methods.map(item => {
                if (item.rowId === editMethodBackId) return method;
                return item;
              });
              setFormValues({ methods: newMethds });
              setHasChange(true);
            }
            setEditMethodBackId(undefined);
            setKeepCreateMethodState(undefined);
          }}
        />
      )}
      {(openCreateNewVersion || openMethodToModel) && (
        <MethodListModal
          localParameterGroup={localParameterGroup}
          id="addMethodFromModel"
          show
          methodNames={methodNames}
          isCreateNewVersion={openCreateNewVersion}
          onClose={(newMethod, dragData) => {
            if (newMethod) {
              dragData && updateDragData(dragData);
              const rowId: any = Date.now();
              setFormValues({
                methods: [
                  {
                    ...newMethod,
                    rowId,
                    serviceSubjectSection: ServiceSubjectSection.MSPS
                  },
                  ...methods
                ]
              });
              setExpandedList([rowId]);
              setHasChange(true);
            }
            closeMethodListModalModal();
          }}
        />
      )}
      {openAddNewMethod && (
        <AddNewMethodModal
          id="addNewMethod"
          show
          methodNames={methodNames}
          minDate={minDate}
          maxDate={maxDate}
          onClose={(method, dragData) => {
            if (method) {
              dragData && updateDragData(dragData);
              const rowId: any = Date.now();
              const newMethodIndex = orderBy(
                [...methods, method],
                ['name'],
                ['asc']
              ).findIndex(el => el.name === method.name);

              onPageChange(
                total > 10 ? Math.ceil(newMethodIndex / pageSize) : 1
              );

              setFormValues({
                methods: [
                  {
                    ...method,
                    rowId,
                    serviceSubjectSection: ServiceSubjectSection.MSPS
                  },
                  ...methods
                ]
              });

              setExpandedList([rowId]);
              setHasChange(true);
            }
            setOpenAddNewMethod(false);
          }}
          localParameterGroup={localParameterGroup}
        />
      )}
    </>
  );
};

export default ConfigureParameters;
