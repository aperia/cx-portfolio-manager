import userEvent from '@testing-library/user-event';
import { WorkflowMetadataList } from 'app/fixtures/workflow-metadata';
import { renderWithMockStore } from 'app/utils';
import 'app/utils/_mockComponent/mockModalRegistry';
import 'app/utils/_mockComponent/mockUseTranslation';
import mockDevice from 'app/utils/_mockHelperConstant/mockDevice';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageStatementProductionSettings';
import React from 'react';
import AddNewMethodModal from './AddNewMethodModal';
import { parameterGroup } from './newMethodParameterList';
const mockOnConfirmParams = jest.fn();
jest.mock('app/hooks/useUnsavedChangesRedirect', () => {
  return {
    __esModule: true,
    useUnsavedChangesRedirect:
      () =>
      ({ onConfirm, onDiscard, onCancel }: any) => {
        onDiscard && onDiscard();
        onCancel && onCancel();
        onConfirm && onConfirm(mockOnConfirmParams());
      }
  };
});

jest.mock('./SubGridNewMethod', () => ({
  __esModule: true,
  default: ({ handleChangeCITForm, onChange }: any) => {
    const onFormChangeTextbox = () => {
      onChange('payment.due.days', false)({ target: { value: -123 } });
    };
    const onFormChangeNumeric = () => {
      onChange('number.of.months', false)({ target: { value: -123 } });
    };
    const onChangeCITForm = () => {
      handleChangeCITForm([
        {
          id: '1',
          name: 'method-1',
          versions: [
            {
              id: 'version 1.0'
            }
          ]
        }
      ]);
    };

    return (
      <div>
        <div onClick={onChangeCITForm}>SubGridNewMethod_component</div>;
        <div onClick={onFormChangeTextbox}>
          SubGridNewMethod_Textbox_component
        </div>
        <div onClick={onFormChangeNumeric}>
          SubGridNewMethod_Numeric_component
        </div>
      </div>
    );
  }
}));

const MockSelectElementMetadataForMSPS = jest.spyOn(
  WorkflowSetup,
  'useSelectElementMetadataForMSPS'
);

describe('pages > WorkflowManageStatementProductionSettings > ManageStatementProductionSettingsStep > AddNewMethodModal', () => {
  const onClose = jest.fn();
  const defaultProps = {
    id: 'AddNewMethodModal',
    show: true,
    onClose,
    localParameterGroup: parameterGroup
  };

  const method: any = {
    id: 'id',
    name: 'MTHMTCI',
    modeledFrom: {
      id: 'id1',
      name: 'MTHMTCI',
      versions: [{ id: 'id' }]
    },
    methodType: 'NEWVERSION',
    comment: '',
    parameters: [
      {
        name: 'credit.balance.hold.options',
        previousValue: '0',
        newValue: 'T'
      },
      {
        name: 'where.to.send.requested.statements',
        previousValue: '0',
        newValue: '0'
      },
      {
        name: 'payment.due.days',
        previousValue: '4',
        newValue: '-1'
      },
      {
        name: 'same.day.lost.status.option',
        previousValue: '0',
        newValue: '3'
      },
      {
        name: 'use.fixed.minimum.payment.due.date',
        previousValue: '0',
        newValue: '8'
      },
      {
        name: 'use.fixed.minimum.payment.due.date.table.id',
        previousValue: '0',
        newValue: '1000'
      },
      {
        name: 'annual.interest.display',
        previousValue: '0',
        newValue: '10'
      },
      {
        name: 'include.late.charges.in.annual.interest.display',
        previousValue: '0',
        newValue: '8'
      },
      {
        name: 'include.cash.item.in.annual.interest.display',
        previousValue: '4',
        newValue: '8'
      },
      {
        name: 'include.merchandise.item.in.annual.interest.display',
        previousValue: '0',
        newValue: '1'
      },
      {
        name: 'include.overlimit.fees.in.annual.interest.display',
        previousValue: '8',
        newValue: '8'
      },
      {
        name: 'include.statement.production.fees.in.annual.interest.display',
        previousValue: '1',
        newValue: '1'
      },
      {
        name: 'produce.statements.for.inactive.deleted.accounts',
        previousValue: '0',
        newValue: '0'
      },
      {
        name: 'us.inactive.account.external.status.control',
        previousValue: '0',
        newValue: '1000'
      },
      {
        name: 'non.us.inactive.account.external.status.control',
        previousValue: '2',
        newValue: '3'
      },
      {
        name: 'pricing.strategy.change.statement.production',
        previousValue: '1',
        newValue: '0'
      },
      {
        name: 'minimum.amount.for.inactive.deleted.account.statements',
        previousValue: 'E',
        newValue: 'I'
      },
      {
        name: 'status.a.authorization.prohibited',
        previousValue: 'B',
        newValue: 'E'
      },
      {
        name: 'status.b.bankrupt',
        previousValue: 'U',
        newValue: 'I'
      },
      {
        name: 'status.c.closed',
        previousValue: 'F',
        newValue: 'L'
      },
      {
        name: 'status.e.revoked',
        previousValue: 'F',
        newValue: 'Z'
      },
      {
        name: 'status.f.frozen',
        previousValue: 'B',
        newValue: 'F'
      },
      {
        name: 'status.i.interest.prohibited',
        previousValue: 'B',
        newValue: 'F'
      },
      {
        name: 'status.l.lost',
        previousValue: 'B',
        newValue: 'F'
      },
      {
        name: 'status.u.stolen',
        previousValue: 'B',
        newValue: 'F'
      },
      {
        name: 'status.blank',
        previousValue: 'B',
        newValue: 'F'
      }
    ],
    versionParameters: [
      {
        name: 'credit.balance.hold.options',
        previousValue: '0',
        newValue: 'T'
      },
      {
        name: 'where.to.send.requested.statements',
        previousValue: '0',
        newValue: '0'
      },
      {
        name: 'payment.due.days',
        previousValue: '4',
        newValue: '-1'
      },
      {
        name: 'same.day.lost.status.option',
        previousValue: '0',
        newValue: '3'
      },
      {
        name: 'use.fixed.minimum.payment.due.date',
        previousValue: '0',
        newValue: '8'
      },
      {
        name: 'use.fixed.minimum.payment.due.date.table.id',
        previousValue: '0',
        newValue: '1000'
      },
      {
        name: 'annual.interest.display',
        previousValue: '0',
        newValue: '10'
      },
      {
        name: 'include.late.charges.in.annual.interest.display',
        previousValue: '0',
        newValue: '8'
      },
      {
        name: 'include.cash.item.in.annual.interest.display',
        previousValue: '4',
        newValue: '8'
      },
      {
        name: 'include.merchandise.item.in.annual.interest.display',
        previousValue: '0',
        newValue: '1'
      },
      {
        name: 'include.overlimit.fees.in.annual.interest.display',
        previousValue: '8',
        newValue: '8'
      },
      {
        name: 'include.statement.production.fees.in.annual.interest.display',
        previousValue: '1',
        newValue: '1'
      },
      {
        name: 'produce.statements.for.inactive.deleted.accounts',
        previousValue: '0',
        newValue: '0'
      },
      {
        name: 'us.inactive.account.external.status.control',
        previousValue: '0',
        newValue: '1000'
      },
      {
        name: 'non.us.inactive.account.external.status.control',
        previousValue: '2',
        newValue: '3'
      },
      {
        name: 'pricing.strategy.change.statement.production',
        previousValue: '1',
        newValue: '0'
      },
      {
        name: 'minimum.amount.for.inactive.deleted.account.statements',
        previousValue: 'E',
        newValue: 'I'
      },
      {
        name: 'status.a.authorization.prohibited',
        previousValue: 'B',
        newValue: 'E'
      },
      {
        name: 'status.b.bankrupt',
        previousValue: 'U',
        newValue: 'I'
      },
      {
        name: 'status.c.closed',
        previousValue: 'F',
        newValue: 'L'
      },
      {
        name: 'status.e.revoked',
        previousValue: 'F',
        newValue: 'Z'
      },
      {
        name: 'status.f.frozen',
        previousValue: 'B',
        newValue: 'F'
      },
      {
        name: 'status.i.interest.prohibited',
        previousValue: 'B',
        newValue: 'F'
      },
      {
        name: 'status.l.lost',
        previousValue: 'B',
        newValue: 'F'
      },
      {
        name: 'status.u.stolen',
        previousValue: 'B',
        newValue: 'F'
      },
      {
        name: 'status.blank',
        previousValue: 'B',
        newValue: 'F'
      }
    ]
  };

  const getInitialState = ({ elements = WorkflowMetadataList }: any = {}) => ({
    workflowSetup: { elementMetadata: { elements } }
  });
  const queryInputFromLabel = (tierLabel: HTMLElement) => {
    return tierLabel
      .closest(
        '.dls-input-container, .dls-numreric-container, .text-field-container'
      )
      ?.querySelector('input, .input, textarea') as HTMLInputElement;
  };

  beforeEach(() => {
    mockDevice.isDevice = false;
    mockOnConfirmParams.mockReturnValue('');
    MockSelectElementMetadataForMSPS.mockReturnValue({
      creditBalanceHoldOptions: [{ code: 'code', text: 'text' }],
      whereToSendRequestPayments: [{ code: 'code', text: 'text' }],
      sameDayLostStatusOption: [{ code: 'code', text: 'text' }],
      useFixedMinimumPaymentDueDate: [{ code: 'code', text: 'text' }],
      useFixedMinimumPaymentDueDateTableId: [{ code: 'code', text: 'text' }],
      annualInterestDisplay: [{ code: 'code', text: 'text' }],
      includeLateChargesInAnnualInterestDisplay: [
        { code: 'code', text: 'text' }
      ],
      includeCashItemInAnnualInterestDisplay: [{ code: 'code', text: 'text' }],
      includeMerchandiseItemInAnnualInterestDisplay: [
        { code: 'code', text: 'text' }
      ],
      includeOverLimitFeesInAnnualInterestDisplay: [
        { code: 'code', text: 'text' }
      ],
      includesStatementProductionFeesInAnnualInterestDisplay: [
        { code: 'code', text: 'text' }
      ],
      produceStatementsForInactiveDeletedAccounts: [
        { code: 'code', text: 'text' }
      ],
      usInactiveAccountExternalStatusControl: [{ code: 'code', text: 'text' }],
      nonUsInactiveAccountExternalStatusControl: [
        { code: 'code', text: 'text' }
      ],
      pricingStrategyChangeStatementProduction: [
        { code: 'code', text: 'text' }
      ],
      statusAAuthorizationProhibited: [{ code: 'code', text: 'text' }],
      statusBBankrupt: [{ code: 'code', text: 'text' }],
      statusCClosed: [{ code: 'code', text: 'text' }],
      statusERevoked: [{ code: 'code', text: 'text' }],
      statusFFrozen: [{ code: 'code', text: 'text' }],
      statusIInterestProhibited: [{ code: 'code', text: 'text' }],
      statusLLost: [{ code: 'code', text: 'text' }],
      statusUStolen: [{ code: 'code', text: 'text' }],
      statusBlank: [{ code: 'code', text: 'text' }],
      creditLifeInsurancePlan1: [{ code: 'code', text: 'text' }],
      creditLifeInsurancePlan2: [{ code: 'code', text: 'text' }],
      creditLifeInsurancePlan3: [{ code: 'code', text: 'text' }],
      creditLifeInsurancePlan4: [{ code: 'code', text: 'text' }],
      creditLifeInsurancePlan5: [{ code: 'code', text: 'text' }],
      creditLifeInsurancePlan6: [{ code: 'code', text: 'text' }],
      creditLifeInsurancePlan7: [{ code: 'code', text: 'text' }],
      creditLifeInsurancePlan8: [{ code: 'code', text: 'text' }],
      creditLifeInsurancePlan9: [{ code: 'code', text: 'text' }],
      cashAdvanceFinanceCharges: [{ code: 'code', text: 'text' }],
      cashItemCharges: [{ code: 'code', text: 'text' }],
      merchandiseFinanceCharges: [{ code: 'code', text: 'text' }],
      merchandiseItemCharges: [{ code: 'code', text: 'text' }],
      lateCharges: [{ code: 'code', text: 'text' }],
      overlimitCharge: [{ code: 'code', text: 'text' }],
      creditLifeInsuranceRefunds: [{ code: 'code', text: 'text' }],
      eBillLanguageOptions: [{ code: 'code', text: 'text' }],
      financeChargeStatementDisplayOptions: [{ code: 'code', text: 'text' }],
      displayAirlineTransaction: [{ code: 'code', text: 'text' }],
      cardHolderNamesOnStatement: [{ code: 'code', text: 'text' }],
      reversalDisplayOption: [{ code: 'code', text: 'text' }],
      lateChargeDateDisplay: [{ code: 'code', text: 'text' }],
      overlimitChargeDateDisplay: [{ code: 'code', text: 'text' }],
      foreignCurrencyDisplayOptions: [{ code: 'code', text: 'text' }],
      feeRecordIndicator: [{ code: 'code', text: 'text' }],
      optionalIssuerFeeStatementDisplayCode: [{ code: 'code', text: 'text' }],
      optionalIssuerFeeMessageText: [{ code: 'code', text: 'text' }],
      saleAmountAdjText: [{ code: 'code', text: 'text' }],
      cashAdvanceAmountAdjText: [{ code: 'code', text: 'text' }],
      returnAmountAdjText: [{ code: 'code', text: 'text' }],
      paymentAmountAdjText: [{ code: 'code', text: 'text' }],
      chargeOffPrincipalAdjText: [{ code: 'code', text: 'text' }],
      chargeOffFinanceChargesAdjText: [{ code: 'code', text: 'text' }],
      miscSpecificCreditAdjText: [{ code: 'code', text: 'text' }],
      chargeOffSmallBalanceAdjText: [{ code: 'code', text: 'text' }],
      chargeOffCreditLifeInsuranceAdjText: [{ code: 'code', text: 'text' }],
      chargeOffCashInterestRefundText: [{ code: 'code', text: 'text' }],
      chargeOffLateChargeRefundText: [{ code: 'code', text: 'text' }],
      chargeOffMerchandiseInterestRefundText: [{ code: 'code', text: 'text' }],
      chargeOffCashItemRefundText: [{ code: 'code', text: 'text' }],
      chargeOffMerchandiseItemRefundText: [{ code: 'code', text: 'text' }],
      chargeOffOverlimitFeeRefundText: [{ code: 'code', text: 'text' }],
      saleReversalText: [{ code: 'code', text: 'text' }],
      cashAdvanceReversalText: [{ code: 'code', text: 'text' }],
      returnReversalText: [{ code: 'code', text: 'text' }],
      paymentReversalText: [{ code: 'code', text: 'text' }],
      cpIcBp: [{ code: 'code', text: 'text' }],
      cpIcId: [{ code: 'code', text: 'text' }],
      cpIcIi: [{ code: 'code', text: 'text' }],
      cpIcIm: [{ code: 'code', text: 'text' }],
      cpIcIp: [{ code: 'code', text: 'text' }],
      cpIcIr: [{ code: 'code', text: 'text' }],
      cpIcMf: [{ code: 'code', text: 'text' }],
      cpIcVi: [{ code: 'code', text: 'text' }],
      cpIoAc: [{ code: 'code', text: 'text' }],
      cpAoCi: [{ code: 'code', text: 'text' }],
      cpIoMc: [{ code: 'code', text: 'text' }],
      cpIoMi: [{ code: 'code', text: 'text' }],
      cpPfLc: [{ code: 'code', text: 'text' }],
      cpPfOc: [{ code: 'code', text: 'text' }],
      cpPfRc: [{ code: 'code', text: 'text' }],
      cpPoMp: [{ code: 'code', text: 'text' }],
      cpIcMe: [{ code: 'code', text: 'text' }],
      creditBalanceHoldCode: [{ code: 'code', text: 'text' }],
      printHardCopySec: [{ code: 'code', text: 'text' }],
      cssStatementOverrideTableId: [{ code: 'code', text: 'text' }],
      lateFeeWaiverText: [{ code: 'code', text: 'text' }],
      genericMessageAbove: [{ code: 'code', text: 'text' }],
      genericMessageBelow: [{ code: 'code', text: 'text' }],
      normalTermsDateId: [{ code: 'code', text: 'text' }],
      crossCycleFeeDisplayOptions: [{ code: 'code', text: 'text' }],
      crossCycleInterestDisplayOptions: [{ code: 'code', text: 'text' }],
      transactionSeriesDisplayOptions: [{ code: 'code', text: 'text' }],
      prevCycCashAdv: [{ code: 'code', text: 'text' }],
      prevCycMerchandise: [{ code: 'code', text: 'text' }],
      prevCycCashItemCharge: [{ code: 'code', text: 'text' }],
      prevCycMdsItemCharge: [{ code: 'code', text: 'text' }],
      minimumFinanceCharges: [{ code: 'code', text: 'text' }],
      activityFee: [{ code: 'code', text: 'text' }],
      prevCycLateCharge: [{ code: 'code', text: 'text' }],
      prevCycOverLimitCharge: [{ code: 'code', text: 'text' }],
      creditLifeInsuranceCharge: [{ code: 'code', text: 'text' }],
      otherCreditAdjText: [{ code: 'code', text: 'text' }],
      otherDebitAdjText: [{ code: 'code', text: 'text' }],
      cashAdvFinanceChargesText: [{ code: 'code', text: 'text' }],
      merchandiseItemChargeText: [{ code: 'code', text: 'text' }],
      cashItemChargesText: [{ code: 'code', text: 'text' }],
      merchandiseFinanceChargesText: [{ code: 'code', text: 'text' }],
      lateChargesText: [{ code: 'code', text: 'text' }],
      overlimitChargesText: [{ code: 'code', text: 'text' }],
      minimumFinanceChargesText: [{ code: 'code', text: 'text' }],
      activityFeesText: [{ code: 'code', text: 'text' }],
      creditLifeInsuranceText: [{ code: 'code', text: 'text' }]
    });
  });

  afterEach(() => {
    onClose.mockClear();
  });

  it('should render AddNewMethodModal', async () => {
    mockDevice.isDevice = true;
    const wrapper = await renderWithMockStore(
      <AddNewMethodModal {...defaultProps} />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByText(
        'txt_manage_statement_production_settings_method_details'
      )
    ).toBeInTheDocument();

    const description = queryInputFromLabel(
      wrapper.getByText('txt_comment_area')
    );
    userEvent.type(description, new Array(239).fill(0).join(''));
    expect(description.textContent?.length).toEqual(239);

    userEvent.click(wrapper.getByText('txt_cancel'));
    expect(onClose).toBeCalled();
  });

  it('should render AddNewMethodModal form new version', async () => {
    const wrapper = await renderWithMockStore(
      <AddNewMethodModal {...defaultProps} method={method} />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByText(
        'txt_manage_statement_production_settings_method_details'
      )
    ).toBeInTheDocument();

    userEvent.click(wrapper.getByText('txt_back'));
    expect(onClose).toBeCalled();
  });

  it('should render AddNewMethodModal form new version', async () => {
    const wrapper = await renderWithMockStore(
      <AddNewMethodModal {...defaultProps} method={method} isCreate={true} />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByText(
        'txt_manage_statement_production_settings_method_details'
      )
    ).toBeInTheDocument();

    userEvent.click(wrapper.getByText('txt_back'));
    expect(onClose).toBeCalled();
  });

  it('should render AddNewMethodModal form new version', async () => {
    const wrapper = await renderWithMockStore(
      <AddNewMethodModal {...defaultProps} method={method} isCreate={false} />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByText(
        'txt_manage_statement_production_settings_method_details'
      )
    ).toBeInTheDocument();

    userEvent.click(wrapper.getByText('txt_back'));
    expect(onClose).toBeCalled();
  });

  it('should render AddNewMethodModal form modeled method', async () => {
    const wrapper = await renderWithMockStore(
      <AddNewMethodModal
        {...defaultProps}
        methodNames={['MTHMTCI1']}
        draftMethod={{
          ...method,
          modeledFrom: { ...method.modeledFrom, name: '' },
          methodType: 'MODELEDMETHOD',
          parameters: undefined as any
        }}
        method={{
          ...method,
          modeledFrom: { ...method.modeledFrom, name: '' },
          methodType: 'MODELEDMETHOD',
          parameters: undefined as any
        }}
      />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByText(
        'txt_manage_statement_production_settings_method_details'
      )
    ).toBeInTheDocument();

    userEvent.click(wrapper.getByText('txt_back'));
    expect(onClose).toBeCalled();
  });

  it('should save method form modeled method', async () => {
    const wrapper = await renderWithMockStore(
      <AddNewMethodModal
        methodNames={['MTHMTCI']}
        {...defaultProps}
        method={{ ...method, methodType: 'MODELEDMETHOD' }}
      />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByText(
        'txt_manage_statement_production_settings_method_details'
      )
    ).toBeInTheDocument();

    userEvent.click(wrapper.getByText('SubGridNewMethod_Textbox_component'));
    userEvent.click(wrapper.getByText('SubGridNewMethod_Numeric_component'));
    userEvent.click(wrapper.getByText('SubGridNewMethod_component'));
    userEvent.click(wrapper.getByText('txt_save'));
  });

  it('should render no data filter', async () => {
    const wrapper = await renderWithMockStore(
      <AddNewMethodModal {...defaultProps} method={undefined} />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByText(
        'txt_manage_statement_production_settings_method_details'
      )
    ).toBeInTheDocument();

    const searchBox = wrapper.getByPlaceholderText('txt_type_to_search');

    userEvent.type(searchBox, 'a');
    userEvent.click(
      searchBox.parentElement!.querySelector(
        '.icon-simple-search .icon.icon-search'
      )!
    );

    userEvent.type(searchBox, 'notfound');
    userEvent.click(
      searchBox.parentElement!.querySelector(
        '.icon-simple-search .icon.icon-search'
      )!
    );

    expect(
      wrapper.getByText('txt_no_results_found txt_adjust_your_search_criteria')
    ).toBeInTheDocument();

    userEvent.click(wrapper.getByText('txt_clear_and_reset'));
    expect(
      wrapper.queryByText(
        'txt_no_results_found txt_adjust_your_search_criteria'
      )
    ).not.toBeInTheDocument();
  });

  it('Stop Add new method when name duplicate', async () => {
    const wrapper = await renderWithMockStore(
      <AddNewMethodModal
        {...defaultProps}
        isCreate
        methodNames={['MTHMTCI']}
        draftMethod={{
          ...method,
          modeledFrom: { ...method.modeledFrom, name: 'MTHMTCI' },
          methodType: 'MODELEDMETHOD',
          parameters: undefined as any
        }}
        method={{
          ...method,
          modeledFrom: { ...method.modeledFrom, name: '' },
          methodType: 'MODELEDMETHOD',
          parameters: undefined as any
        }}
      />,
      { initialState: getInitialState() }
    );
    const methodName = queryInputFromLabel(
      wrapper.getByText('txt_method_name')
    );

    userEvent.clear(methodName);
    userEvent.type(methodName, '@');
    userEvent.type(methodName, 'MTHMTCI1');
    expect(methodName?.value).toEqual('MTHMTCI1');
    userEvent.click(document.body);
    userEvent.click(methodName);

    userEvent.click(wrapper.getByText('txt_save'));

    expect(onClose).toBeCalled();
  });
});
