import { MethodFieldParameterEnum } from 'app/constants/enums';

export const LIST_NUMERIC_FORM: any[] = [
  MethodFieldParameterEnum.ManageStatementProductionSettingsPaymentDueDays,
  MethodFieldParameterEnum.ManageStatementProductionSettingsTurnOffPaper,
  MethodFieldParameterEnum.ManageStatementProductionSettingsTurnOnPaper,
  MethodFieldParameterEnum.ManageStatementProductionSettingsReturnToEDelivery,
  MethodFieldParameterEnum.ManageStatementProductionSettingsDeliquentAccountUnEnrollment
];
