import { renderWithMockStore } from 'app/utils';
import React from 'react';
import ManageStatementProductionSettingsStep from '.';

jest.mock('./ConfigureParameters', () => ({
  __esModule: true,
  default: () => <div>ConfigureParameters</div>
}));

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

const renderComponent = (props: any, initValues: any = {}) => {
  return renderWithMockStore(
    <ManageStatementProductionSettingsStep {...props} />,
    initValues
  );
};

describe('ManageStatementProductionSettingsStep', () => {
  it('Should render ManageStatementProductionSettingsStep contains Completed', async () => {
    const props = { isStuck: false };
    const wrapper = await renderComponent(props, {});

    expect(
      wrapper.getByText(
        'txt_manage_statement_production_settings_description_step_3'
      )
    ).toBeInTheDocument;
  });
  it('Should render ManageStatementProductionSettingsStep contains Completed', async () => {
    const props = { isStuck: true };
    const wrapper = await renderComponent(props, {});

    expect(wrapper.getByText('txt_step_stuck_move_forward_message'))
      .toBeInTheDocument;
  });
});
