import { ServiceSubjectSection } from 'app/constants/enums';
import {
  getWorkflowSetupMethodType,
  getWorkflowSetupStepStatus
} from 'app/helpers';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import { ManageStatementProductionSettingsFormValue } from '.';

const parseFormValues: WorkflowSetupStepFormDataFunc<ManageStatementProductionSettingsFormValue> =
  (data, id) => {
    const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);
    if (!stepInfo) return;

    const now = Date.now();
    const methods = (data.data.configurations?.methodList || [])
      .filter(
        (method: any) =>
          method.serviceSubjectSection === ServiceSubjectSection.MSPS
      )
      .map(
        (method: any, idx: number) =>
          ({
            ...method,
            id: '', // Remove Id due to error 500 when update with methodId
            methodType: getWorkflowSetupMethodType(method),
            rowId: now + idx
          } as WorkflowSetupMethod & { rowId?: number })
      );

    const isValid = !isEmpty(methods);

    return {
      ...getWorkflowSetupStepStatus(stepInfo),
      isValid,
      methods
    };
  };

export default parseFormValues;
