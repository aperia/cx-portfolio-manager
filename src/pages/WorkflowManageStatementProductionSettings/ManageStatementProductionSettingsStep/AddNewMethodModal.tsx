import ModalRegistry from 'app/components/ModalRegistry';
import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import TextAreaCountDown from 'app/components/TextAreaCountDown';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import {
  unsavedChangesProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import {
  mapGridExpandCollapse,
  mappingArrayKey,
  matchSearchValue,
  methodParamsToObject,
  objectToMethodParams
} from 'app/helpers';
import {
  useFormValidations,
  useUnsavedChangeRegistry,
  useUnsavedChangesRedirect
} from 'app/hooks';
import {
  Button,
  Grid,
  InlineMessage,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  TextBox,
  TransDLS,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty } from 'lodash';
import isEqual from 'lodash.isequal';
import isFunction from 'lodash.isfunction';
import { useSelectElementMetadataForMSPS } from 'pages/_commons/redux/WorkflowSetup';
import ClearAndResetButton from 'pages/_commons/Utils/ClearAndResetButton';
import React, { useEffect, useMemo, useRef, useState } from 'react';
import { LIST_NUMERIC_FORM } from './constant';
import { defaultValue, METHOD_MSPS_FIELDS } from './helper';
import newMethodColumns from './newMethodColumns';
import {
  parameterGroup,
  ParameterGroup,
  parameterList,
  ParameterListIds
} from './newMethodParameterList';
import SubGridNewMethod from './SubGridNewMethod';

interface IKeepState {
  searchValue: string;
  expandedList: ParameterListIds[];
}
interface IProps {
  id: string;
  show: boolean;
  isCreate?: boolean;
  method?: WorkflowSetupMethod;
  draftMethod?: WorkflowSetupMethod;
  keepState?: IKeepState;
  methodNames?: string[];
  minDate?: Date;
  maxDate?: Date;
  onClose?: (
    method?: WorkflowSetupMethod,
    dragData?: any[],
    isBack?: boolean,
    keepState?: IKeepState
  ) => void;
  localParameterGroup: Record<ParameterListIds, ParameterGroup[]>;
}

const AddNewMethodModal: React.FC<IProps> = ({
  id,
  show,
  isCreate = true,
  method: methodProp = { methodType: 'NEWMETHOD' } as WorkflowSetupMethod,
  draftMethod,
  keepState,
  methodNames = [],
  minDate,
  maxDate,
  onClose,
  localParameterGroup
}) => {
  const metadata = useSelectElementMetadataForMSPS();

  const [dragData, setDragData] = useState<any[]>();

  const { t } = useTranslation();
  const redirect = useUnsavedChangesRedirect();

  const isChangeFieldName = useRef(false);

  const [initialMethod, setInitialMethod] = useState<
    WorkflowSetupMethodObjectParams & { rowId?: number }
  >(methodParamsToObject(methodProp));
  const [method, setMethod] = useState(
    draftMethod ? methodParamsToObject(draftMethod) : initialMethod
  );
  const [searchValue, setSearchValue] = useState(keepState?.searchValue || '');
  const [expandedList, setExpandedList] = useState<ParameterListIds[]>(
    keepState?.expandedList || ['Info_StatementProductionOptions']
  );
  const [isDuplicateName, setIsDuplicateName] = useState(false);

  const isNewVersion = useMemo(
    () => method.methodType === 'NEWVERSION',
    [method.methodType]
  );

  const isNewMethod = useMemo(
    () => method.methodType === 'NEWMETHOD',
    [method.methodType]
  );

  const dynamicLocalParameterGroup = useMemo(() => {
    if (isCreate) return parameterGroup;
    return localParameterGroup;
  }, [isCreate, localParameterGroup]);

  const parameters = useMemo(
    () =>
      parameterList.filter(p =>
        dynamicLocalParameterGroup[p.id].some(g =>
          matchSearchValue(
            [g.fieldName, g.moreInfoText, g.onlinePCF],
            searchValue
          )
        )
      ),
    [searchValue, dynamicLocalParameterGroup]
  );
  useEffect(() => {
    searchValue &&
      parameters &&
      setExpandedList(mappingArrayKey(parameters, 'id'));
  }, [searchValue, parameters]);

  const errorDuplicateName = useMemo(
    () => ({
      status: true,
      message: t('txt_manage_penalty_fee_validation_method_name_must_be_unique')
    }),
    [t]
  );

  useUnsavedChangeRegistry(
    {
      ...unsavedChangesProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_STATEMENT_PRODUCTION_SETTINGS,
      priority: 1
    },
    [
      !isEqual(initialMethod.name?.trim() || '', method.name?.trim() || ''),
      !isEqual(
        initialMethod.comment?.trim() || '',
        method.comment?.trim() || ''
      ),
      !isEqual(initialMethod, method)
    ]
  );

  const currentErrors = useMemo(
    () =>
      ({
        name: !method?.name?.trim() && {
          status: true,
          message: t('txt_required_validation', {
            fieldName: t('txt_method_name')
          })
        }
      } as Record<keyof WorkflowSetupMethodObjectParams, IFormError>),
    [method?.name, t]
  );
  const [touches, errors, setTouched] =
    useFormValidations<keyof WorkflowSetupMethodObjectParams>(currentErrors);

  const isValid = useMemo(
    () =>
      !errors.name?.status &&
      !isDuplicateName &&
      (!currentErrors.name?.status || touches.name?.touched),
    [
      errors.name?.status,
      isDuplicateName,
      currentErrors.name?.status,
      touches.name?.touched
    ]
  );

  useEffect(() => {
    let defaultParamObj: Record<string, string> = {};

    if (metadata) {
      const DEFAULT_VALUE: any = defaultValue(metadata);

      if (
        initialMethod.methodType === 'NEWMETHOD' &&
        initialMethod.rowId === undefined
      ) {
        const paramMapping = Object.keys(DEFAULT_VALUE).reduce((pre, next) => {
          pre[next] = DEFAULT_VALUE[next];
          return pre;
        }, {} as Record<string, string>);
        defaultParamObj = Object.assign({}, paramMapping);
        defaultParamObj.methodType = 'NEWMETHOD';
        setMethod(defaultParamObj as any);
        setInitialMethod(defaultParamObj as any);
      }
    }
  }, [initialMethod.methodType, initialMethod.rowId, metadata]);

  const simpleSearchRef = useRef<any>(null);
  useEffect(() => {
    if (!searchValue && simpleSearchRef?.current) {
      simpleSearchRef.current.clear();
    }
  }, [searchValue]);

  const handleBack = () => {
    const methodForm = objectToMethodParams([...METHOD_MSPS_FIELDS], method);
    methodForm.versionParameters = !!methodForm.versionParameters
      ? methodForm.versionParameters
      : methodForm.parameters;

    isFunction(onClose) &&
      onClose(methodForm, dragData, true, { searchValue, expandedList });
  };

  const handleClose = () => {
    redirect({
      onConfirm: () => isFunction(onClose) && onClose(),
      formsWatcher: [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_STATEMENT_PRODUCTION_SETTINGS
      ]
    });
  };

  const handleAddMethod = () => {
    const isEdit = !isCreate;
    const isChangeMethodName = initialMethod.name !== method.name;
    const isCheckName =
      (isNewVersion && isCreate) || isCreate || (isEdit && isChangeMethodName);

    const isExistNameInCurrentMethods = methodNames.includes(
      method?.name?.trim()
    );
    const isChooseMethod = !isNewMethod && !isNewVersion;

    const isDuplicateName =
      (isCheckName && isExistNameInCurrentMethods) ||
      (isChooseMethod && method?.name?.trim() === method.modeledFrom?.name);
    setIsDuplicateName(isDuplicateName);
    if (isDuplicateName) return;

    const methodForm = objectToMethodParams([...METHOD_MSPS_FIELDS], method);
    methodForm.versionParameters = !!methodForm.versionParameters
      ? methodForm.versionParameters
      : methodForm.parameters;

    isFunction(onClose) && onClose(methodForm, dragData);
  };

  const handleFormChange =
    (
      field: keyof WorkflowSetupMethodObjectParams & { optionalTiers?: string },
      isRefData?: boolean
    ) =>
    (e: any) => {
      let value = isRefData ? e.target?.value?.code : e.target?.value;

      if (LIST_NUMERIC_FORM.includes(field)) {
        const negativeNum = /^-\d+$/.test(value);
        if (negativeNum) return;
      }

      if (field === 'name') {
        // flag to check is change for Event onBlur
        isChangeFieldName.current = true;

        value = value?.toUpperCase();
        const isValidValue = /[^a-zA-Z0-9]/.test(value);
        if (isValidValue) return;
      }
      setMethod(method => ({ ...method, [field]: value }));
    };

  const handleChangeCITForm = (data: any) => {
    setMethod(method => ({
      ...method,
      [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICBP]: (
        data.findIndex(
          (el: any) =>
            el.id ===
            MethodFieldParameterEnum.ManageStatementProductionSettingsCPICBP
        ) + 1
      )
        .toString()
        .padStart(2, '0'),
      [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICID]: (
        data.findIndex(
          (el: any) =>
            el.id ===
            MethodFieldParameterEnum.ManageStatementProductionSettingsCPICID
        ) + 1
      )
        .toString()
        .padStart(2, '0'),
      [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICII]: (
        data.findIndex(
          (el: any) =>
            el.id ===
            MethodFieldParameterEnum.ManageStatementProductionSettingsCPICII
        ) + 1
      )
        .toString()
        .padStart(2, '0'),
      [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIM]: (
        data.findIndex(
          (el: any) =>
            el.id ===
            MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIM
        ) + 1
      )
        .toString()
        .padStart(2, '0'),
      [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIP]: (
        data.findIndex(
          (el: any) =>
            el.id ===
            MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIP
        ) + 1
      )
        .toString()
        .padStart(2, '0'),
      [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIR]: (
        data.findIndex(
          (el: any) =>
            el.id ===
            MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIR
        ) + 1
      )
        .toString()
        .padStart(2, '0'),
      [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICMF]: (
        data.findIndex(
          (el: any) =>
            el.id ===
            MethodFieldParameterEnum.ManageStatementProductionSettingsCPICMF
        ) + 1
      )
        .toString()
        .padStart(2, '0'),
      [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICVI]: (
        data.findIndex(
          (el: any) =>
            el.id ===
            MethodFieldParameterEnum.ManageStatementProductionSettingsCPICVI
        ) + 1
      )
        .toString()
        .padStart(2, '0'),
      [MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOAC]: (
        data.findIndex(
          (el: any) =>
            el.id ===
            MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOAC
        ) + 1
      )
        .toString()
        .padStart(2, '0'),
      [MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOCI]: (
        data.findIndex(
          (el: any) =>
            el.id ===
            MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOCI
        ) + 1
      )
        .toString()
        .padStart(2, '0'),
      [MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOMC]: (
        data.findIndex(
          (el: any) =>
            el.id ===
            MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOMC
        ) + 1
      )
        .toString()
        .padStart(2, '0'),
      [MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOMI]: (
        data.findIndex(
          (el: any) =>
            el.id ===
            MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOMI
        ) + 1
      )
        .toString()
        .padStart(2, '0'),
      [MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFLC]: (
        data.findIndex(
          (el: any) =>
            el.id ===
            MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFLC
        ) + 1
      )
        .toString()
        .padStart(2, '0'),
      [MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFOC]: (
        data.findIndex(
          (el: any) =>
            el.id ===
            MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFOC
        ) + 1
      )
        .toString()
        .padStart(2, '0'),
      [MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFRC]: (
        data.findIndex(
          (el: any) =>
            el.id ===
            MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFRC
        ) + 1
      )
        .toString()
        .padStart(2, '0'),
      [MethodFieldParameterEnum.ManageStatementProductionSettingsCPPOMP]: (
        data.findIndex(
          (el: any) =>
            el.id ===
            MethodFieldParameterEnum.ManageStatementProductionSettingsCPPOMP
        ) + 1
      )
        .toString()
        .padStart(2, '0'),
      [MethodFieldParameterEnum.ManageStatementProductionSettingsCPICME]: (
        data.findIndex(
          (el: any) =>
            el.id ===
            MethodFieldParameterEnum.ManageStatementProductionSettingsCPICME
        ) + 1
      )
        .toString()
        .padStart(2, '0')
    }));
  };

  const handleBlurName = () => {
    if (isChangeFieldName.current) {
      setIsDuplicateName(false);
    }
    isChangeFieldName.current = false;

    setTouched('name', true)();
  };

  const handleSearch = (val: string) => {
    setSearchValue(val);
  };

  const handleClearFilter = () => {
    setSearchValue('');
    setExpandedList(['Info_StatementProductionOptions']);
  };

  return (
    <ModalRegistry
      lg
      id={id}
      show={show}
      onAutoClosedAll={handleClose}
      onAutoClosed={handleClearFilter}
    >
      <ModalHeader border closeButton onHide={handleClose}>
        <ModalTitle>
          {t('txt_manage_statement_production_settings_method_details')}
        </ModalTitle>
      </ModalHeader>
      <ModalBody>
        <p className="color-grey">
          <TransDLS keyTranslation="txt_manage_statement_production_settings_create_methods_desc">
            <strong className="color-grey-d20" />
          </TransDLS>
        </p>
        <div>
          {isDuplicateName && (
            <InlineMessage variant="danger" withIcon className="mb-8 mt-16">
              {t(
                'txt_manage_penalty_fee_validation_method_name_must_be_unique'
              )}
            </InlineMessage>
          )}
          <div className="row mt-16">
            {!isNewMethod && (
              <div className="col-6 col-lg-4">
                <TextBox
                  id="addNewMethod__selectedMethod"
                  readOnly
                  value={method?.modeledFrom?.name || ''}
                  maxLength={8}
                  label={t('txt_design_loan_offers_selected_method')}
                />
              </div>
            )}
            <div className="col-6 col-lg-4 mr-lg-4">
              <TextBox
                id="addNewMethod__methodName"
                readOnly={isNewVersion}
                value={method?.name || ''}
                onChange={handleFormChange('name')}
                required
                maxLength={8}
                label={t('txt_method_name')}
                onFocus={setTouched('name')}
                onBlur={handleBlurName}
                error={errors.name || (isDuplicateName && errorDuplicateName)}
              />
            </div>
            <div className="mt-16 col-12 col-lg-8">
              <TextAreaCountDown
                id="addNewMethod__comment"
                label={t('txt_comment_area')}
                value={method?.comment || ''}
                onChange={handleFormChange('comment')}
                rows={4}
                maxLength={240}
              />
            </div>
          </div>
          <div className="mt-24">
            <div className="d-flex align-items-center justify-content-between mb-16">
              <h5>{t('txt_parameter_list')}</h5>
              <SimpleSearch
                ref={simpleSearchRef}
                placeholder={t('txt_type_to_search')}
                onSearch={handleSearch}
                defaultValue={searchValue}
                popperElement={
                  <>
                    <p className="color-grey">{t('txt_search_description')}</p>
                    <ul className="search-field-item list-unstyled">
                      <li className="mt-16">{t('txt_business_name')}</li>
                      <li className="mt-16">{t('txt_more_info')}</li>
                      <li className="mt-16">{t('txt_online_pcf')}</li>
                    </ul>
                  </>
                }
              />
            </div>
            {searchValue && !isEmpty(parameters) && (
              <div className="d-flex justify-content-end mb-16 mr-n8">
                <ClearAndResetButton
                  small
                  onClearAndReset={handleClearFilter}
                />
              </div>
            )}
            {isEmpty(parameters) && (
              <div className="d-flex flex-column justify-content-center mt-40 mb-32">
                <NoDataFound
                  id="newMethod_notfound"
                  hasSearch
                  title={t('txt_no_results_found')}
                  linkTitle={t('txt_clear_and_reset')}
                  onLinkClicked={handleClearFilter}
                />
              </div>
            )}
            {!isEmpty(parameters) && (
              <Grid
                togglable
                columns={newMethodColumns(t)}
                data={parameters}
                subRow={({ original }: MagicKeyValue) => (
                  <SubGridNewMethod
                    localParameterGroup={dynamicLocalParameterGroup}
                    setDragData={setDragData}
                    minDate={minDate}
                    maxDate={maxDate}
                    original={original}
                    method={method}
                    metadata={metadata}
                    searchValue={searchValue}
                    onChange={handleFormChange}
                    handleChangeCITForm={handleChangeCITForm}
                  />
                )}
                dataItemKey="id"
                expandedItemKey="id"
                expandedList={expandedList}
                onExpand={setExpandedList as any}
                toggleButtonConfigList={parameters.map(
                  mapGridExpandCollapse('id', t)
                )}
              />
            )}
          </div>
        </div>
      </ModalBody>
      <ModalFooter
        cancelButtonText={t('txt_cancel')}
        okButtonText={t('txt_save')}
        onCancel={handleClose}
        onOk={handleAddMethod}
        disabledOk={!isValid}
      >
        {!isNewMethod && (
          <Button
            className="mr-auto ml-n8"
            variant="outline-primary"
            onClick={handleBack}
          >
            {t('txt_back')}
          </Button>
        )}
      </ModalFooter>
    </ModalRegistry>
  );
};

export default AddNewMethodModal;
