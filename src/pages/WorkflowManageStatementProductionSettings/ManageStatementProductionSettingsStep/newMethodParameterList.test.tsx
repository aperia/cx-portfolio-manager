import { parameterGroup, parameterList } from './newMethodParameterList';

describe('pages > WorkflowManageStatementProductionSettings > ManageStatementProductionSettingsStep > newMethodParameterList', () => {
  it('parameterList', () => {
    expect(parameterList).toEqual([
      {
        id: 'Info_StatementProductionOptions',
        section:
          'txt_manage_statement_production_settings_method_standard_parameters',
        parameterGroup:
          'txt_manage_statement_production_settings_statement_production_options'
      },
      {
        id: 'Info_AnnualInterestDisplay',
        section:
          'txt_manage_statement_production_settings_method_standard_parameters',
        parameterGroup:
          'txt_manage_statement_production_settings_annual_interest_display'
      },
      {
        id: 'Info_InactiveDeletedAccount',
        section:
          'txt_manage_statement_production_settings_method_standard_parameters',
        parameterGroup:
          'txt_manage_statement_production_settings_inactive_deleted_account'
      },
      {
        id: 'Info_ExternalStatusPrintControl',
        section:
          'txt_manage_statement_production_settings_method_standard_parameters',
        parameterGroup:
          'txt_manage_statement_production_settings_external_status_print_control'
      },
      {
        id: 'Info_CreditLifeInsuranceTextIds',
        section:
          'txt_manage_statement_production_settings_method_standard_parameters',
        parameterGroup:
          'txt_manage_statement_production_settings_credit_life_insurance_text_ids'
      },
      {
        id: 'Info_RefundMessages',
        section:
          'txt_manage_statement_production_settings_method_standard_parameters',
        parameterGroup:
          'txt_manage_statement_production_settings_refund_messages'
      },
      {
        id: 'Info_StatementDisplayOptions',
        section:
          'txt_manage_statement_production_settings_method_standard_parameters',
        parameterGroup:
          'txt_manage_statement_production_settings_statement_display_options'
      },
      {
        id: 'Info_EMessengerSettings',
        section:
          'txt_manage_statement_production_settings_method_standard_parameters',
        parameterGroup:
          'txt_manage_statement_production_settings_e_messenger_settings'
      },
      {
        id: 'Info_CITDisclosuresPrintOrderNumbers',
        section:
          'txt_manage_statement_production_settings_method_standard_parameters',
        parameterGroup:
          'txt_manage_statement_production_settings_cit_disclosures_print_order_numbers'
      },
      {
        id: 'Info_PreviousCycleMessageText',
        section:
          'txt_manage_statement_production_settings_method_advanced_parameters',
        parameterGroup:
          'txt_manage_statement_production_settings_previous_cycle_message_text'
      },
      {
        id: 'Info_StatementFunctionality',
        section:
          'txt_manage_statement_production_settings_method_advanced_parameters',
        parameterGroup:
          'txt_manage_statement_production_settings_statement_functionality'
      },
      {
        id: 'Info_OtherDisplayOptions',
        section:
          'txt_manage_statement_production_settings_method_advanced_parameters',
        parameterGroup:
          'txt_manage_statement_production_settings_other_display_options'
      },
      {
        id: 'Info_MiscellaneousAdjustmentText',
        section:
          'txt_manage_statement_production_settings_method_advanced_parameters',
        parameterGroup:
          'txt_manage_statement_production_settings_miscellaneous_adjustment_text'
      },
      {
        id: 'Info_DisputesMessageText',
        section:
          'txt_manage_statement_production_settings_method_advanced_parameters',
        parameterGroup:
          'txt_manage_statement_production_settings_disputes_message_text'
      }
    ]);
  });
  it('parameterGroup', () => {
    const result = parameterGroup;

    expect(Object.keys(result).length).toEqual(14);
  });
});
