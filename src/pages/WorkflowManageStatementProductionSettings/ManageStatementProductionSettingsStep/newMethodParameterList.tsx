import {
  MethodFieldNameEnum,
  MethodFieldParameterEnum
} from 'app/constants/enums';
import React from 'react';

export type ParameterListIds =
  | 'Info_StatementProductionOptions'
  | 'Info_AnnualInterestDisplay'
  | 'Info_InactiveDeletedAccount'
  | 'Info_ExternalStatusPrintControl'
  | 'Info_CreditLifeInsuranceTextIds'
  | 'Info_RefundMessages'
  | 'Info_StatementDisplayOptions'
  | 'Info_EMessengerSettings'
  | 'Info_CITDisclosuresPrintOrderNumbers'
  | 'Info_PreviousCycleMessageText'
  | 'Info_StatementFunctionality'
  | 'Info_OtherDisplayOptions'
  | 'Info_MiscellaneousAdjustmentText'
  | 'Info_DisputesMessageText';

export interface ParameterList {
  id: ParameterListIds;
  section:
    | 'txt_manage_statement_production_settings_method_standard_parameters'
    | 'txt_manage_statement_production_settings_method_advanced_parameters';
  parameterGroup?:
    | 'txt_manage_statement_production_settings_statement_production_options'
    | 'txt_manage_statement_production_settings_annual_interest_display'
    | 'txt_manage_statement_production_settings_inactive_deleted_account'
    | 'txt_manage_statement_production_settings_external_status_print_control'
    | 'txt_manage_statement_production_settings_credit_life_insurance_text_ids'
    | 'txt_manage_statement_production_settings_refund_messages'
    | 'txt_manage_statement_production_settings_statement_display_options'
    | 'txt_manage_statement_production_settings_e_messenger_settings'
    | 'txt_manage_statement_production_settings_cit_disclosures_print_order_numbers'
    | 'txt_manage_statement_production_settings_previous_cycle_message_text'
    | 'txt_manage_statement_production_settings_statement_functionality'
    | 'txt_manage_statement_production_settings_other_display_options'
    | 'txt_manage_statement_production_settings_miscellaneous_adjustment_text'
    | 'txt_manage_statement_production_settings_disputes_message_text';

  moreInfo?: React.ReactNode;
}

export const parameterList: ParameterList[] = [
  {
    id: 'Info_StatementProductionOptions',
    section:
      'txt_manage_statement_production_settings_method_standard_parameters',
    parameterGroup:
      'txt_manage_statement_production_settings_statement_production_options'
  },
  {
    id: 'Info_AnnualInterestDisplay',
    section:
      'txt_manage_statement_production_settings_method_standard_parameters',
    parameterGroup:
      'txt_manage_statement_production_settings_annual_interest_display'
  },
  {
    id: 'Info_InactiveDeletedAccount',
    section:
      'txt_manage_statement_production_settings_method_standard_parameters',
    parameterGroup:
      'txt_manage_statement_production_settings_inactive_deleted_account'
  },
  {
    id: 'Info_ExternalStatusPrintControl',
    section:
      'txt_manage_statement_production_settings_method_standard_parameters',
    parameterGroup:
      'txt_manage_statement_production_settings_external_status_print_control'
  },
  {
    id: 'Info_CreditLifeInsuranceTextIds',
    section:
      'txt_manage_statement_production_settings_method_standard_parameters',
    parameterGroup:
      'txt_manage_statement_production_settings_credit_life_insurance_text_ids'
  },
  {
    id: 'Info_RefundMessages',
    section:
      'txt_manage_statement_production_settings_method_standard_parameters',
    parameterGroup: 'txt_manage_statement_production_settings_refund_messages'
  },
  {
    id: 'Info_StatementDisplayOptions',
    section:
      'txt_manage_statement_production_settings_method_standard_parameters',
    parameterGroup:
      'txt_manage_statement_production_settings_statement_display_options'
  },
  {
    id: 'Info_EMessengerSettings',
    section:
      'txt_manage_statement_production_settings_method_standard_parameters',
    parameterGroup:
      'txt_manage_statement_production_settings_e_messenger_settings'
  },
  {
    id: 'Info_CITDisclosuresPrintOrderNumbers',
    section:
      'txt_manage_statement_production_settings_method_standard_parameters',
    parameterGroup:
      'txt_manage_statement_production_settings_cit_disclosures_print_order_numbers'
  },
  {
    id: 'Info_PreviousCycleMessageText',
    section:
      'txt_manage_statement_production_settings_method_advanced_parameters',
    parameterGroup:
      'txt_manage_statement_production_settings_previous_cycle_message_text'
  },
  {
    id: 'Info_StatementFunctionality',
    section:
      'txt_manage_statement_production_settings_method_advanced_parameters',
    parameterGroup:
      'txt_manage_statement_production_settings_statement_functionality'
  },
  {
    id: 'Info_OtherDisplayOptions',
    section:
      'txt_manage_statement_production_settings_method_advanced_parameters',
    parameterGroup:
      'txt_manage_statement_production_settings_other_display_options'
  },
  {
    id: 'Info_MiscellaneousAdjustmentText',
    section:
      'txt_manage_statement_production_settings_method_advanced_parameters',
    parameterGroup:
      'txt_manage_statement_production_settings_miscellaneous_adjustment_text'
  },
  {
    id: 'Info_DisputesMessageText',
    section:
      'txt_manage_statement_production_settings_method_advanced_parameters',
    parameterGroup:
      'txt_manage_statement_production_settings_disputes_message_text'
  }
];

export interface ParameterGroup {
  id: MethodFieldParameterEnum;
  fieldName: MethodFieldNameEnum;
  onlinePCF: string;
  moreInfoText: string;
  moreInfo: React.ReactNode;
}
export const parameterGroup: Record<ParameterListIds, ParameterGroup[]> = {
  Info_StatementProductionOptions: [
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCreditBalanceHoldOptions,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsCreditBalanceHoldOptions,
      onlinePCF: 'Credit Balance Hold Code',
      moreInfoText:
        'The Credit Balance Hold Options parameter controls the automatic setting of statement hold codes on accounts cycling with credit balances. You can set this parameter to have the System set the hold code on an account that cycles with a credit balance and do one of the following. Send the account’s statement directly to the issuer regardless of current account activity. Send the account’s statement to the issuer unless current account activity exits, then send to the customer.',
      moreInfo: (
        <div>
          The Credit Balance Hold Options parameter controls the automatic
          setting of statement hold codes on accounts cycling with credit
          balances. You can set this parameter to have the System set the hold
          code on an account that cycles with a credit balance and do one of the
          following.
          <ul className="list-dot">
            <li>
              Send the account’s statement directly to the issuer regardless of
              current account activity.
            </li>
            <li>
              Send the account’s statement to the issuer unless current account
              activity exits, then send to the customer.
            </li>
          </ul>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsWhereToSendRequestPayments,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsWhereToSendRequestPayments,
      onlinePCF: 'Demand Statement',
      moreInfoText:
        'If you enter valid code 1 and the account has a hold code, the System sends the statement to the issuer.',
      moreInfo:
        'If you enter valid code 1 and the account has a hold code, the System sends the statement to the issuer.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsPaymentDueDays,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsPaymentDueDays,
      onlinePCF: 'Payment Due Days',
      moreInfoText:
        'The Payment Due Days parameter indicates the number of days following the cycling of the account that the payment due date is set for all accounts. If the payment due date is on a Saturday, Sunday, or federal bank holiday, the System calculates the payment due date according to your setting in the Pymt Due to Non-Bank Holiday parameter in this section.',
      moreInfo: (
        <div>
          <p>
            The Payment Due Days parameter indicates the number of days
            following the cycling of the account that the payment due date is
            set for all accounts.
          </p>
          <p>
            If the payment due date is on a Saturday, Sunday, or federal bank
            holiday, the System calculates the payment due date according to
            your setting in the Pymt Due to Non-Bank Holiday parameter in this
            section.
          </p>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsSameDayLostStatusOption,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsSameDayLostStatusOption,
      onlinePCF:
        MethodFieldNameEnum.ManageStatementProductionSettingsSameDayLostStatusOption,
      moreInfoText:
        'The Same Day Lost Status Option parameter determines whether an account that is reported lost on the day a statement is created will include the L status or the account’s previous status.',
      moreInfo:
        'The Same Day Lost Status Option parameter determines whether an account that is reported lost on the day a statement is created will include the L status or the account’s previous status.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsUseFixedMinimumPaymentDueDate,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsUseFixedMinimumPaymentDueDate,
      onlinePCF: 'Fixed MPD Date Code',
      moreInfoText:
        'Determines whether or not to use a fixed minimum payment due date. Before setting the Fixed MPD Date Code and Fixed MPDD Table ID parameters, you must first use the PCF Table Maintenance feature to build a table and corresponding identifier using the MD, Fixed MPD Date Day table area.Please be aware that the fixed MPD date feature can be overriden by your setting in the Pymt Due to Non-Bank Holiday parameter also in the Statement Production section (CP IC SP) of the PCF. To avoid this, set the Pymt Due to Non-Bank Holiday parameter to zero. Warning! Your setting in the Pymt Due to Non-Bank Holiday parameter could result in non-compliance with federal regulations regarding fixed MPD dates. Consult with your legal counsel and compliance officer about use of the Pymt Due to Non-Bank Holiday parameter in conjunction with the fixed MPD date feature.',
      moreInfo: (
        <div>
          <p>
            Determines whether or not to use a fixed minimum payment due date.
            Before setting the Fixed MPD Date Code and Fixed MPDD Table ID
            parameters, you must first use the PCF Table Maintenance feature to
            build a table and corresponding identifier using the MD, Fixed MPD
            Date Day table area.
          </p>
          <p>
            Please be aware that the fixed MPD date feature can be overriden by
            your setting in the Pymt Due to Non-Bank Holiday parameter also in
            the Statement Production section (CP IC SP) of the PCF. To avoid
            this, set the Pymt Due to Non-Bank Holiday parameter to zero.
            Warning! Your setting in the Pymt Due to Non-Bank Holiday parameter
            could result in non-compliance with federal regulations regarding
            fixed MPD dates. Consult with your legal counsel and compliance
            officer about use of the Pymt Due to Non-Bank Holiday parameter in
            conjunction with the fixed MPD date feature.
          </p>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsUseFixedMinimumPaymentDueDateTableID,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsUseFixedMinimumPaymentDueDateTableID,
      onlinePCF: 'Fixed MPDD Table ID',
      moreInfoText:
        'The Use Fixed Minimum Payment Due Date Table ID parameter contains the identifier of the table you created using the MD, Fixed MPD Date Day table area of the Table Maintenance feature which the System uses if you set the Fixed MPD Date Code parameter to Y. You can create multiple Fixed MPD Date Day tables.',
      moreInfo:
        'The Use Fixed Minimum Payment Due Date Table ID parameter contains the identifier of the table you created using the MD, Fixed MPD Date Day table area of the Table Maintenance feature which the System uses if you set the Fixed MPD Date Code parameter to Y. You can create multiple Fixed MPD Date Day tables.'
    }
  ],

  Info_AnnualInterestDisplay: [
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsAnnualInterestDisplay,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsAnnualInterestDisplay,
      onlinePCF:
        MethodFieldNameEnum.ManageStatementProductionSettingsAnnualInterestDisplay,
      moreInfoText:
        'The Annual Interest Display parameter also controls the display of billed or paid interest in the following fields. Year-to-date interest field on the BS, Balance and Status transaction screen. &PRVYTDINT variable code in the Cardholder Letters System showing the previous year-to-date interest, either billed or paid.',
      moreInfo: (
        <div>
          The Annual Interest Display parameter also controls the display of
          billed or paid interest in the following fields.
          <ul className="list-dot">
            <li>
              Year-to-date interest field on the BS, Balance and Status
              transaction screen.
            </li>
            <li>
              &PRVYTDINT variable code in the Cardholder Letters System showing
              the previous year-to-date interest, either billed or paid.
            </li>
          </ul>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeLateChargesInAnnualInterestDisplay,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsIncludeLateChargesInAnnualInterestDisplay,
      onlinePCF: 'Late Charges',
      moreInfoText:
        'The following charges parameters determine whether to include the specified charges in the annual interest displayed on cardholder statements. These parameters work in conjunction with the Annual Interest Display parameter in this section.',
      moreInfo:
        'The following charges parameters determine whether to include the specified charges in the annual interest displayed on cardholder statements. These parameters work in conjunction with the Annual Interest Display parameter in this section.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeCashItemInAnnualInterestDisplay,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsIncludeCashItemInAnnualInterestDisplay,
      onlinePCF: 'Cash Item Charges',
      moreInfoText:
        'The following charges parameters determine whether to include the specified charges in the annual interest displayed on cardholder statements. These parameters work in conjunction with the Annual Interest Display parameter in this section.',
      moreInfo:
        'The following charges parameters determine whether to include the specified charges in the annual interest displayed on cardholder statements. These parameters work in conjunction with the Annual Interest Display parameter in this section.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeMerchandiseItemInAnnualInterestDisplay,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsIncludeMerchandiseItemInAnnualInterestDisplay,
      onlinePCF: 'Merchandise Item Charges',
      moreInfoText:
        'The following charges parameters determine whether to include the specified charges in the annual interest displayed on cardholder statements. These parameters work in conjunction with the Annual Interest Display parameter in this section.',
      moreInfo:
        'The following charges parameters determine whether to include the specified charges in the annual interest displayed on cardholder statements. These parameters work in conjunction with the Annual Interest Display parameter in this section.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeOverLimitFeesInAnnualInterestDisplay,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsIncludeOverLimitFeesInAnnualInterestDisplay,
      onlinePCF: 'Overlimit Charges',
      moreInfoText:
        'The following charges parameters determine whether to include the specified charges in the annual interest displayed on cardholder statements. These parameters work in conjunction with the Annual Interest Display parameter in this section.',
      moreInfo:
        'The following charges parameters determine whether to include the specified charges in the annual interest displayed on cardholder statements. These parameters work in conjunction with the Annual Interest Display parameter in this section.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsIncludeStatementProductionFeesInAnnualInterestDisplay,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsIncludeStatementProductionFeesInAnnualInterestDisplay,
      onlinePCF: 'Statement Charges',
      moreInfoText:
        'The following charges parameters determine whether to include the specified charges in the annual interest displayed on cardholder statements. These parameters work in conjunction with the Annual Interest Display parameter in this section.',
      moreInfo:
        'The following charges parameters determine whether to include the specified charges in the annual interest displayed on cardholder statements. These parameters work in conjunction with the Annual Interest Display parameter in this section.'
    }
  ],
  Info_InactiveDeletedAccount: [
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsProduceStatementsForInactiveDeletedAccounts,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsProduceStatementsForInactiveDeletedAccounts,
      onlinePCF: 'Inactive/Deleted Account Statement',
      moreInfoText:
        'The Produce Statements for Inactive/Deleted Accounts parameter controls the production of statements for inactive and/or deleted cardholder accounts. If these statements are created for the purpose of displaying annual interest, the Annual Interest Display parameter in this section determines whether to print billed interest or paid interest on these statements. The actual amount printed is controlled by the following parameters under the Annual Interest Display heading in this section.',
      moreInfo:
        'The Produce Statements for Inactive/Deleted Accounts parameter controls the production of statements for inactive and/or deleted cardholder accounts. If these statements are created for the purpose of displaying annual interest, the Annual Interest Display parameter in this section determines whether to print billed interest or paid interest on these statements. The actual amount printed is controlled by the following parameters under the Annual Interest Display heading in this section.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsUSInactiveAccountExternalStatusControl,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsUSInactiveAccountExternalStatusControl,
      onlinePCF: 'External Status Inclusion',
      moreInfoText:
        'The US Inactive Account External Status controls statements sent to inactive accounts with an external status and a US address. Determine whether or not to produce a statement for inactive accounts with an external status other than "blank."',
      moreInfo:
        'The US Inactive Account External Status controls statements sent to inactive accounts with an external status and a US address. Determine whether or not to produce a statement for inactive accounts with an external status other than "blank."'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsNonUSInactiveAccountExternalStatusControl,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsNonUSInactiveAccountExternalStatusControl,
      onlinePCF: 'Non-U.S. Inclusion',
      moreInfoText:
        'The Non-US Inactive Account External Status controls statements sent to inactive accounts with an external status and a non-US address. Determine whether or not to produce a statement for inactive accounts with an external status other than "blank."',
      moreInfo:
        'The Non-US Inactive Account External Status controls statements sent to inactive accounts with an external status and a non-US address. Determine whether or not to produce a statement for inactive accounts with an external status other than "blank."'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsMinimumAmountForInactiveDeletedAccounts,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsMinimumAmountForInactiveDeletedAccounts,
      onlinePCF: 'Special Statement Minimum Amount',
      moreInfoText:
        'The Minimum Amount for Inactive/Deleted Account Statements parameter identifies the minimum amount of interest needed to generate statements for deleted or inactive cardholder accounts. To use this field, set the Inactive/Deleted Account Statement parameter in this section to 2, 3, 4, or 5.',
      moreInfo:
        'The Minimum Amount for Inactive/Deleted Account Statements parameter identifies the minimum amount of interest needed to generate statements for deleted or inactive cardholder accounts. To use this field, set the Inactive/Deleted Account Statement parameter in this section to 2, 3, 4, or 5.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsPricingStrategyChangeStatementProduction,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsPricingStrategyChangeStatementProduction,
      onlinePCF: 'Pricing Strategy Change',
      moreInfoText:
        'When a pricing strategy changes on an inactive account, use this setting to determine if a statement is produced or not.',
      moreInfo:
        'When a pricing strategy changes on an inactive account, use this setting to determine if a statement is produced or not.'
    }
  ],
  Info_ExternalStatusPrintControl: [
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsStatusAAuthorizationProhibited,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsStatusAAuthorizationProhibited,
      onlinePCF: 'A - Authorization Prohibited',
      moreInfoText:
        'Use the following external status statement print control parameters to control the printing of statements based on account balance and/or external status other than external status Z for charged-off accounts.',
      moreInfo:
        'Use the following external status statement print control parameters to control the printing of statements based on account balance and/or external status other than external status Z for charged-off accounts.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsStatusBBankrupt,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsStatusBBankrupt,
      onlinePCF: 'B - Bankrupt',
      moreInfoText:
        'Use the following external status statement print control parameters to control the printing of statements based on account balance and/or external status other than external status Z for charged-off accounts.',
      moreInfo:
        'Use the following external status statement print control parameters to control the printing of statements based on account balance and/or external status other than external status Z for charged-off accounts.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsStatusCClosed,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsStatusCClosed,
      onlinePCF: 'C - Closed',
      moreInfoText:
        'Use the following external status statement print control parameters to control the printing of statements based on account balance and/or external status other than external status Z for charged-off accounts.',
      moreInfo:
        'Use the following external status statement print control parameters to control the printing of statements based on account balance and/or external status other than external status Z for charged-off accounts.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsStatusERevoked,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsStatusERevoked,
      onlinePCF: 'E - Revoked',
      moreInfoText:
        'Use the following external status statement print control parameters to control the printing of statements based on account balance and/or external status other than external status Z for charged-off accounts.',
      moreInfo:
        'Use the following external status statement print control parameters to control the printing of statements based on account balance and/or external status other than external status Z for charged-off accounts.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsStatusFFrozen,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsStatusFFrozen,
      onlinePCF: 'F - Frozen',
      moreInfoText:
        'Use the following external status statement print control parameters to control the printing of statements based on account balance and/or external status other than external status Z for charged-off accounts.',
      moreInfo:
        'Use the following external status statement print control parameters to control the printing of statements based on account balance and/or external status other than external status Z for charged-off accounts.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsStatusIInterestProhibited,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsStatusIInterestProhibited,
      onlinePCF: 'I - Interest Prohibited',
      moreInfoText:
        'Use the following external status statement print control parameters to control the printing of statements based on account balance and/or external status other than external status Z for charged-off accounts.',
      moreInfo:
        'Use the following external status statement print control parameters to control the printing of statements based on account balance and/or external status other than external status Z for charged-off accounts.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsStatusLLost,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsStatusLLost,
      onlinePCF: 'L- Lost',
      moreInfoText:
        'Use the following external status statement print control parameters to control the printing of statements based on account balance and/or external status other than external status Z for charged-off accounts.',
      moreInfo:
        'Use the following external status statement print control parameters to control the printing of statements based on account balance and/or external status other than external status Z for charged-off accounts.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsStatusUStolen,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsStatusUStolen,
      onlinePCF: 'U - Stolen',
      moreInfoText:
        'Use the following external status statement print control parameters to control the printing of statements based on account balance and/or external status other than external status Z for charged-off accounts.',
      moreInfo:
        'Use the following external status statement print control parameters to control the printing of statements based on account balance and/or external status other than external status Z for charged-off accounts.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsStatusBlank,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsStatusBlank,
      onlinePCF: 'Normal',
      moreInfoText:
        'Use the following external status statement print control parameters to control the printing of statements based on account balance and/or external status other than external status Z for charged-off accounts.',
      moreInfo:
        'Use the following external status statement print control parameters to control the printing of statements based on account balance and/or external status other than external status Z for charged-off accounts.'
    }
  ],
  Info_CreditLifeInsuranceTextIds: [
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance1,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsCreditLifeInsurance1,
      onlinePCF: 'Plan 1',
      moreInfoText:
        'Text Area = DT, Text Type = ON The Credit Life Insurance Text ID - Plan 1 through Credit Life Insurance Text ID - Plan 9 parameters identify description text for credit insurance premiums on cardholder statements. These text identification codes work with text area code DT and text type code ON. If you want one plan to use the default wording for another plan, enter the default text identification code listed for the description you want. For example, if you want plan 3 or 5 to use an amount per hundred dollars, enter DEFON002 in the parameter to print CREDIT INSURANCE AT X.XXXX PER 100.00 as the description.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = ON</p>
          <p>
            The Credit Life Insurance Text ID - Plan 1 through Credit Life
            Insurance Text ID - Plan 9 parameters identify description text for
            credit insurance premiums on cardholder statements.
          </p>
          <p>
            These text identification codes work with text area code DT and text
            type code ON. If you want one plan to use the default wording for
            another plan, enter the default text identification code listed for
            the description you want. For example, if you want plan 3 or 5 to
            use an amount per hundred dollars, enter DEFON002 in the parameter
            to print CREDIT INSURANCE AT X.XXXX PER 100.00 as the description.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance2,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsCreditLifeInsurance2,
      onlinePCF: 'Plan 2',
      moreInfoText:
        'Text Area = DT, Text Type = ON The Credit Life Insurance Text ID - Plan 1 through Credit Life Insurance Text ID - Plan 9 parameters identify description text for credit insurance premiums on cardholder statements. These text identification codes work with text area code DT and text type code ON. If you want one plan to use the default wording for another plan, enter the default text identification code listed for the description you want. For example, if you want plan 3 or 5 to use an amount per hundred dollars, enter DEFON002 in the parameter to print CREDIT INSURANCE AT X.XXXX PER 100.00 as the description.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = ON</p>
          <p>
            The Credit Life Insurance Text ID - Plan 1 through Credit Life
            Insurance Text ID - Plan 9 parameters identify description text for
            credit insurance premiums on cardholder statements.
            <p>
              These text identification codes work with text area code DT and
              text type code ON. If you want one plan to use the default wording
              for another plan, enter the default text identification code
              listed for the description you want. For example, if you want plan
              3 or 5 to use an amount per hundred dollars, enter DEFON002 in the
              parameter to print CREDIT INSURANCE AT X.XXXX PER 100.00 as the
              description.
            </p>
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance3,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsCreditLifeInsurance3,
      onlinePCF: 'Plan 3',
      moreInfoText:
        'Text Area = DT, Text Type = ON The Credit Life Insurance Text ID - Plan 1 through Credit Life Insurance Text ID - Plan 9 parameters identify description text for credit insurance premiums on cardholder statements. These text identification codes work with text area code DT and text type code ON. If you want one plan to use the default wording for another plan, enter the default text identification code listed for the description you want. For example, if you want plan 3 or 5 to use an amount per hundred dollars, enter DEFON002 in the parameter to print CREDIT INSURANCE AT X.XXXX PER 100.00 as the description.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = ON</p>
          <p>
            The Credit Life Insurance Text ID - Plan 1 through Credit Life
            Insurance Text ID - Plan 9 parameters identify description text for
            credit insurance premiums on cardholder statements.
            <p>
              These text identification codes work with text area code DT and
              text type code ON. If you want one plan to use the default wording
              for another plan, enter the default text identification code
              listed for the description you want. For example, if you want plan
              3 or 5 to use an amount per hundred dollars, enter DEFON002 in the
              parameter to print CREDIT INSURANCE AT X.XXXX PER 100.00 as the
              description.
            </p>
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance4,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsCreditLifeInsurance4,
      onlinePCF: 'Plan 4',
      moreInfoText:
        'Text Area = DT, Text Type = ON The Credit Life Insurance Text ID - Plan 1 through Credit Life Insurance Text ID - Plan 9 parameters identify description text for credit insurance premiums on cardholder statements. These text identification codes work with text area code DT and text type code ON. If you want one plan to use the default wording for another plan, enter the default text identification code listed for the description you want. For example, if you want plan 3 or 5 to use an amount per hundred dollars, enter DEFON002 in the parameter to print CREDIT INSURANCE AT X.XXXX PER 100.00 as the description.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = ON</p>
          <p>
            The Credit Life Insurance Text ID - Plan 1 through Credit Life
            Insurance Text ID - Plan 9 parameters identify description text for
            credit insurance premiums on cardholder statements.
            <p>
              These text identification codes work with text area code DT and
              text type code ON. If you want one plan to use the default wording
              for another plan, enter the default text identification code
              listed for the description you want. For example, if you want plan
              3 or 5 to use an amount per hundred dollars, enter DEFON002 in the
              parameter to print CREDIT INSURANCE AT X.XXXX PER 100.00 as the
              description.
            </p>
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance5,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsCreditLifeInsurance5,
      onlinePCF: 'Plan 5',
      moreInfoText:
        'Text Area = DT, Text Type = ON The Credit Life Insurance Text ID - Plan 1 through Credit Life Insurance Text ID - Plan 9 parameters identify description text for credit insurance premiums on cardholder statements. These text identification codes work with text area code DT and text type code ON. If you want one plan to use the default wording for another plan, enter the default text identification code listed for the description you want. For example, if you want plan 3 or 5 to use an amount per hundred dollars, enter DEFON002 in the parameter to print CREDIT INSURANCE AT X.XXXX PER 100.00 as the description.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = ON</p>
          <p>
            The Credit Life Insurance Text ID - Plan 1 through Credit Life
            Insurance Text ID - Plan 9 parameters identify description text for
            credit insurance premiums on cardholder statements.
            <p>
              These text identification codes work with text area code DT and
              text type code ON. If you want one plan to use the default wording
              for another plan, enter the default text identification code
              listed for the description you want. For example, if you want plan
              3 or 5 to use an amount per hundred dollars, enter DEFON002 in the
              parameter to print CREDIT INSURANCE AT X.XXXX PER 100.00 as the
              description.
            </p>
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance6,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsCreditLifeInsurance6,
      onlinePCF: 'Plan 6',
      moreInfoText:
        'Text Area = DT, Text Type = ON The Credit Life Insurance Text ID - Plan 1 through Credit Life Insurance Text ID - Plan 9 parameters identify description text for credit insurance premiums on cardholder statements.These text identification codes work with text area code DT and text type code ON. If you want one plan to use the default wording for another plan, enter the default text identification code listed for the description you want. For example, if you want plan 3 or 5 to use an amount per hundred dollars, enter DEFON002 in the parameter to print CREDIT INSURANCE AT X.XXXX PER 100.00 as the description.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = ON</p>
          <p>
            The Credit Life Insurance Text ID - Plan 1 through Credit Life
            Insurance Text ID - Plan 9 parameters identify description text for
            credit insurance premiums on cardholder statements.
            <p>
              These text identification codes work with text area code DT and
              text type code ON. If you want one plan to use the default wording
              for another plan, enter the default text identification code
              listed for the description you want. For example, if you want plan
              3 or 5 to use an amount per hundred dollars, enter DEFON002 in the
              parameter to print CREDIT INSURANCE AT X.XXXX PER 100.00 as the
              description.
            </p>
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance7,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsCreditLifeInsurance7,
      onlinePCF: 'Plan 7',
      moreInfoText:
        'Text Area = DT, Text Type = ON The Credit Life Insurance Text ID - Plan 1 through Credit Life Insurance Text ID - Plan 9 parameters identify description text for credit insurance premiums on cardholder statements.These text identification codes work with text area code DT and text type code ON. If you want one plan to use the default wording for another plan, enter the default text identification code listed for the description you want. For example, if you want plan 3 or 5 to use an amount per hundred dollars, enter DEFON002 in the parameter to print CREDIT INSURANCE AT X.XXXX PER 100.00 as the description.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = ON</p>
          <p>
            The Credit Life Insurance Text ID - Plan 1 through Credit Life
            Insurance Text ID - Plan 9 parameters identify description text for
            credit insurance premiums on cardholder statements.
            <p>
              These text identification codes work with text area code DT and
              text type code ON. If you want one plan to use the default wording
              for another plan, enter the default text identification code
              listed for the description you want. For example, if you want plan
              3 or 5 to use an amount per hundred dollars, enter DEFON002 in the
              parameter to print CREDIT INSURANCE AT X.XXXX PER 100.00 as the
              description.
            </p>
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance8,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsCreditLifeInsurance8,
      onlinePCF: 'Plan 8',
      moreInfoText:
        'Text Area = DT, Text Type = ON The Credit Life Insurance Text ID - Plan 1 through Credit Life Insurance Text ID - Plan 9 parameters identify description text for credit insurance premiums on cardholder statements.These text identification codes work with text area code DT and text type code ON. If you want one plan to use the default wording for another plan, enter the default text identification code listed for the description you want. For example, if you want plan 3 or 5 to use an amount per hundred dollars, enter DEFON002 in the parameter to print CREDIT INSURANCE AT X.XXXX PER 100.00 as the description.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = ON</p>
          <p>
            The Credit Life Insurance Text ID - Plan 1 through Credit Life
            Insurance Text ID - Plan 9 parameters identify description text for
            credit insurance premiums on cardholder statements.
            <p>
              These text identification codes work with text area code DT and
              text type code ON. If you want one plan to use the default wording
              for another plan, enter the default text identification code
              listed for the description you want. For example, if you want plan
              3 or 5 to use an amount per hundred dollars, enter DEFON002 in the
              parameter to print CREDIT INSURANCE AT X.XXXX PER 100.00 as the
              description.
            </p>
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsurance9,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsCreditLifeInsurance9,
      onlinePCF: 'Plan 9',
      moreInfoText:
        'Text Area = DT, Text Type = ON The Credit Life Insurance Text ID - Plan 1 through Credit Life Insurance Text ID - Plan 9 parameters identify description text for credit insurance premiums on cardholder statements.These text identification codes work with text area code DT and text type code ON. If you want one plan to use the default wording for another plan, enter the default text identification code listed for the description you want. For example, if you want plan 3 or 5 to use an amount per hundred dollars, enter DEFON002 in the parameter to print CREDIT INSURANCE AT X.XXXX PER 100.00 as the description.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = ON</p>
          <p>
            The Credit Life Insurance Text ID - Plan 1 through Credit Life
            Insurance Text ID - Plan 9 parameters identify description text for
            credit insurance premiums on cardholder statements.
            <p>
              These text identification codes work with text area code DT and
              text type code ON. If you want one plan to use the default wording
              for another plan, enter the default text identification code
              listed for the description you want. For example, if you want plan
              3 or 5 to use an amount per hundred dollars, enter DEFON002 in the
              parameter to print CREDIT INSURANCE AT X.XXXX PER 100.00 as the
              description.
            </p>
          </p>
        </>
      )
    }
  ],
  Info_RefundMessages: [
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCashAdvanceFinanceCharges,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsCashAdvanceFinanceCharges,
      onlinePCF: 'Refund Adj Cash Advance',
      moreInfoText:
        'Text Area = DT, Text Type = RC Use these codes to change description text for a refund adjustment via the Text Maintenance feature. The default text identification code identifies wording that Fiserv has created. You can build your own text using the Text Maintenance feature. To specify your own text, select a Text ID for each parameter. Only use a code for text that is either scheduled or in production.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = RC.</p>
          <p>
            Use these codes to change description text for a refund adjustment
            via the Text Maintenance feature. The default text identification
            code identifies wording that Fiserv has created. You can build your
            own text using the Text Maintenance feature. To specify your own
            text, select a Text ID for each parameter. Only use a code for text
            that is either scheduled or in production.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCashItemCharges,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsCashItemCharges,
      onlinePCF: 'Refund Adj Cash Item Charge',
      moreInfoText:
        'Text Area = DT, Text Type = RI Use these codes to change description text for a refund adjustment via the Text Maintenance feature. The default text identification code identifies wording that Fiserv has created. You can build your own text using the Text Maintenance feature. To specify your own text, select a Text ID for each parameter. Only use a code for text that is either scheduled or in production.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = RI.</p>
          <p>
            Use these codes to change description text for a refund adjustment
            via the Text Maintenance feature. The default text identification
            code identifies wording that Fiserv has created. You can build your
            own text using the Text Maintenance feature. To specify your own
            text, select a Text ID for each parameter. Only use a code for text
            that is either scheduled or in production.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsMerchandiseFinanceCharges,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsMerchandiseFinanceCharges,
      onlinePCF: 'Refund Adj Merchandise',
      moreInfoText:
        'Text Area = DT, Text Type = RS Use these codes to change description text for a refund adjustment via the Text Maintenance feature. The default text identification code identifies wording that Fiserv has created. You can build your own text using the Text Maintenance feature. To specify your own text, select a Text ID for each parameter. Only use a code for text that is either scheduled or in production.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = RS.</p>
          <p>
            Use these codes to change description text for a refund adjustment
            via the Text Maintenance feature. The default text identification
            code identifies wording that Fiserv has created. You can build your
            own text using the Text Maintenance feature. To specify your own
            text, select a Text ID for each parameter. Only use a code for text
            that is either scheduled or in production.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsMerchandiseItemCharges,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsMerchandiseItemCharges,
      onlinePCF: 'Refund Adj Mdse Item Charge',
      moreInfoText:
        'Text Area = DT, Text Type = RM Use these codes to change description text for a refund adjustment via the Text Maintenance feature. The default text identification code identifies wording that Fiserv has created. You can build your own text using the Text Maintenance feature. To specify your own text, select a Text ID for each parameter. Only use a code for text that is either scheduled or in production.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = RM.</p>
          <p>
            Use these codes to change description text for a refund adjustment
            via the Text Maintenance feature. The default text identification
            code identifies wording that Fiserv has created. You can build your
            own text using the Text Maintenance feature. To specify your own
            text, select a Text ID for each parameter. Only use a code for text
            that is either scheduled or in production.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsLateCharge,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsLateCharge,
      onlinePCF: 'Refund Adj Late Charge',
      moreInfoText:
        'Text Area = DT, Text Type = RL Use these codes to change description text for a refund adjustment via the Text Maintenance feature. The default text identification code identifies wording that Fiserv has created. You can build your own text using the Text Maintenance feature. To specify your own text, select a Text ID for each parameter. Only use a code for text that is either scheduled or in production.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = RL.</p>
          <p>
            Use these codes to change description text for a refund adjustment
            via the Text Maintenance feature. The default text identification
            code identifies wording that Fiserv has created. You can build your
            own text using the Text Maintenance feature. To specify your own
            text, select a Text ID for each parameter. Only use a code for text
            that is either scheduled or in production.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsOverlimitCharge,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsOverlimitCharge,
      onlinePCF: 'Refund Adj Overlimit Charge',
      moreInfoText:
        'Text Area = DT, Text Type = RO Use these codes to change description text for a refund adjustment via the Text Maintenance feature. The default text identification code identifies wording that Fiserv has created. You can build your own text using the Text Maintenance feature. To specify your own text, select a Text ID for each parameter. Only use a code for text that is either scheduled or in production.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = RO.</p>
          <p>
            Use these codes to change description text for a refund adjustment
            via the Text Maintenance feature. The default text identification
            code identifies wording that Fiserv has created. You can build your
            own text using the Text Maintenance feature. To specify your own
            text, select a Text ID for each parameter. Only use a code for text
            that is either scheduled or in production.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsuranceRefunds,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsCreditLifeInsuranceRefunds,
      onlinePCF: 'Refund Adj Credit Life Ins',
      moreInfoText:
        'Text Area = DT, Text Type = RN Use these codes to change description text for a refund adjustment via the Text Maintenance feature. The default text identification code identifies wording that Fiserv has created. You can build your own text using the Text Maintenance feature. To specify your own text, select a Text ID for each parameter. Only use a code for text that is either scheduled or in production.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = RN.</p>
          <p>
            Use these codes to change description text for a refund adjustment
            via the Text Maintenance feature. The default text identification
            code identifies wording that Fiserv has created. You can build your
            own text using the Text Maintenance feature. To specify your own
            text, select a Text ID for each parameter. Only use a code for text
            that is either scheduled or in production.
          </p>
        </>
      )
    }
  ],
  Info_StatementDisplayOptions: [
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsFinanceChargeStatementDisplayOptions,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsFinanceChargeStatementDisplayOptions,
      onlinePCF: 'Finance Charge Display',
      moreInfoText:
        'The Finance Charge Statement Display Options parameter determines how the System displays cash advance, merchandise, and total periodic rate finance charges on cardholder statements.',
      moreInfo:
        'The Finance Charge Statement Display Options parameter determines how the System displays cash advance, merchandise, and total periodic rate finance charges on cardholder statements.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsDisplayAirlineTransactionDetailsOnStatement,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsDisplayAirlineTransactionDetailsOnStatement,
      onlinePCF: 'Passenger Transport Display',
      moreInfoText:
        'The Display Airline Transaction Details on Statement parameter controls passenger itinerary data printed on reports, cardholder statements, and the CSS, Display Statement transaction screen. You can set this parameter to 3 only if you print Enterprise Presentation or Correspondence Director statements. With this option, the System also automatically prints the currency name and amount for non-U.S. transactions. For Mastercard® World Card T&E transactions, the System automatically prints the following information, if supplied by the merchant, on Mastercard World Card cardholder billing statements. The System prints this information regardless of the setting of the Passenger Transport Display parameter. See your Optis User Documentation for more information regarding Mastercard® World Card and Mastercard® Commercial Card corporate T&E statements.',
      moreInfo: (
        <div>
          <p>
            The Display Airline Transaction Details on Statement parameter
            controls passenger itinerary data printed on reports, cardholder
            statements, and the CSS, Display Statement transaction screen.
          </p>
          <p>
            You can set this parameter to 3 only if you print Enterprise
            Presentation or Correspondence Director statements. With this
            option, the System also automatically prints the currency name and
            amount for non-U.S. transactions. For Mastercard® World Card T&E
            transactions, the System automatically prints the following
            information, if supplied by the merchant, on Mastercard World Card
            cardholder billing statements. The System prints this information
            regardless of the setting of the Passenger Transport Display
            parameter. See your Optis User Documentation for more information
            regarding Mastercard® World Card and Mastercard® Commercial Card
            corporate T&E statements.
          </p>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCardholderNamesOnStatement,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsCardholderNamesOnStatement,
      onlinePCF: 'Name on Statement',
      moreInfoText:
        'The Cardholder Names on Statement parameter determines which cardholder name you want to appear on cardholder statements.',
      moreInfo:
        'The Cardholder Names on Statement parameter determines which cardholder name you want to appear on cardholder statements.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsReversalDisplayOption,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsReversalDisplayOption,
      onlinePCF: 'Reversal Display',
      moreInfoText:
        'The Reversal Display Option parameter controls the printing of reversals and their original postings on cardholder statements when the reversal enters the System in the same billing cycle as the original posting. During nightly processing, the System uses this parameter to determine whether to keep a reversal and its original posting until the account cycles. If a reversal enters the System in the same billing cycle as the original posting, and any of the following data for the reversal does not match the data from the original transaction, the System prints each reversal and its original posting as a separate line item on the cardholder statement regardless of your setting in this parameter. Merchant number Reference number Amount Date Due to System processing requirements for merchant numbers, merchant number data for a reversal on a transaction that originated with an interchange merchant may not match the merchant number data on the original transaction. The System prints the reversal and its original posting as a separate line item on the cardholder statement.',
      moreInfo: (
        <>
          <p>
            The Reversal Display Option parameter controls the printing of
            reversals and their original postings on cardholder statements when
            the reversal enters the System in the same billing cycle as the
            original posting. During nightly processing, the System uses this
            parameter to determine whether to keep a reversal and its original
            posting until the account cycles.
          </p>
          <p>
            If a reversal enters the System in the same billing cycle as the
            original posting, and any of the following data for the reversal
            does not match the data from the original transaction, the System
            prints each reversal and its original posting as a separate line
            item on the cardholder statement regardless of your setting in this
            parameter.
          </p>
          <div>
            <ul>
              <li>Merchant number</li>
              <li>Reference number</li>
              <li>Amount</li>
              <li>Date</li>
              Due to System processing requirements for merchant numbers,
              merchant number data for a reversal on a transaction that
              originated with an interchange merchant may not match the merchant
              number data on the original transaction. The System prints the
              reversal and its original posting as a separate line item on the
              cardholder statement.
            </ul>
          </div>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsLateChargeDateDisplay,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsLateChargeDateDisplay,
      onlinePCF: 'Late Charge Date Display',
      moreInfoText:
        'The Late Charge Date Display parameter determines whether the date the late charge is assessed is printed on the cardholder statement for the transaction and posting date of the late charge transaction detail.',
      moreInfo:
        'The Late Charge Date Display parameter determines whether the date the late charge is assessed is printed on the cardholder statement for the transaction and posting date of the late charge transaction detail.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsOverlimitChargeDateDisplay,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsOverlimitChargeDateDisplay,
      onlinePCF: 'Overlimit Fees Date Display',
      moreInfoText:
        'The Overlimit Charge Date Display parameter controls which date should be used in the statement detail section on customer statements. If you set this parameter to 1, the overlimit fee assessed date appears in the overlimit fee detail on the customer statement.',
      moreInfo:
        'The Overlimit Charge Date Display parameter controls which date should be used in the statement detail section on customer statements. If you set this parameter to 1, the overlimit fee assessed date appears in the overlimit fee detail on the customer statement.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsForeignCurrencyDisplayOptions,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsForeignCurrencyDisplayOptions,
      onlinePCF: 'Foreign Display',
      moreInfoText:
        'The Foreign Currency Display Options parameter determines whether the currency name or the currency code is displayed in the foreign currency display on paper customer statements and the CSS, Display Statement screen. If you set this parameter to 1, the System displays foreign currency information on two separate detail lines on both paper statements and the CSS screen. The foreign currency name appears on the first detail line, and the foreign currency amount and conversion rate appear on a second detail line. If you set this parameter to 2 or 3, the System displays the optional issuer fee as part of the currency conversion rate on paper statements, on the CSS screen, and on the CDS, Display Cycle-to-Date Activity screen. Be aware, the conversion rate displayed on both paper statements and the CSS screen isn’t the actual conversion rate that is used or in effect at the time the transaction occurred. Rather, the rate that appears is calculated by the System and is determined by dividing the transaction amount by the posting amount. Consult with your compliance department or legal counsel to determine if this treatment of optional issuer fees is consistent with applicable law and how you assess and disclose these fees.',
      moreInfo: (
        <>
          <p>
            The Foreign Currency Display Options parameter determines whether
            the currency name or the currency code is displayed in the foreign
            currency display on paper customer statements and the CSS, Display
            Statement screen.
          </p>
          <p>
            If you set this parameter to 1, the System displays foreign currency
            information on two separate detail lines on both paper statements
            and the CSS screen. The foreign currency name appears on the first
            detail line, and the foreign currency amount and conversion rate
            appear on a second detail line. If you set this parameter to 2 or 3,
            the System displays the optional issuer fee as part of the currency
            conversion rate on paper statements, on the CSS screen, and on the
            CDS, Display Cycle-to-Date Activity screen. Be aware, the conversion
            rate displayed on both paper statements and the CSS screen isn’t the
            actual conversion rate that is used or in effect at the time the
            transaction occurred. Rather, the rate that appears is calculated by
            the System and is determined by dividing the transaction amount by
            the posting amount. Consult with your compliance department or legal
            counsel to determine if this treatment of optional issuer fees is
            consistent with applicable law and how you assess and disclose these
            fees.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsFeeRecordIndicatorOnCISScreens,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsFeeRecordIndicatorOnCISScreens,
      onlinePCF: 'Fee Record Ind',
      moreInfoText:
        'The Fee Record Indicator on CIS Screens parameter controls whether the System creates and displays detail items on the following CIS screens for late, overlimit, or promotional mid-cycle interest charges only after the statement cycles or also at the time the transaction posts to the customer account. CDA, Display Daily Authorizations CDE, Display Detail Item CDS, Display Cycle-to-Date Activity',
      moreInfo: (
        <>
          <p>
            The Fee Record Indicator on CIS Screens parameter controls whether
            the System creates and displays detail items on the following CIS
            screens for late, overlimit, or promotional mid-cycle interest
            charges only after the statement cycles or also at the time the
            transaction posts to the customer account.
          </p>

          <ul>
            <li>CDA, Display Daily Authorizations</li>
            <li>CDE, Display Detail Item</li>
            <li>CDS, Display Cycle-to-Date Activity</li>
          </ul>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsOptionalIssuerFeeStatementDisplayCode,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsOptionalIssuerFeeStatementDisplayCode,
      onlinePCF: 'OIF Statement Display',
      moreInfoText:
        'The Optional Issuer Fee - Statement Display Code parameter controls how you want optional issuer fees to appear on cardholder statements.  Optional issuer fees will be included in the foreign transaction amount when viewing cycle-to-date transactions. The amounts are separated during statement cycle processing if you select option 2. If you set this parameter to 1 or 2, the second detail line does not appear when you are viewing cycle-to-date transactions.  If you set this parameter to 1 or 2, enter the identifier of the detail text that you want to appear on cardholder statements in the Optional Finance Charge Statement Messages Optional Issuer Fee parameter in this section. ',
      moreInfo:
        'The Optional Issuer Fee - Statement Display Code parameter controls how you want optional issuer fees to appear on cardholder statements.  Optional issuer fees will be included in the foreign transaction amount when viewing cycle-to-date transactions. The amounts are separated during statement cycle processing if you select option 2. If you set this parameter to 1 or 2, the second detail line does not appear when you are viewing cycle-to-date transactions.  If you set this parameter to 1 or 2, enter the identifier of the detail text that you want to appear on cardholder statements in the Optional Finance Charge Statement Messages Optional Issuer Fee parameter in this section. '
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsOptionalIssuerFeeMessageText,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsOptionalIssuerFeeMessageText,
      onlinePCF:
        'Optional Finance Charge Statement Messages Optional Issuer Fee',
      moreInfoText:
        'Text Area = DT, Text Type = ON. Identify the text ID of the message text for the Optional Issuer Fee to be displayed on statements. This is only used if you set the Optional Issuer Fee Statement Display Code to a 1 or a 2. To print a custom detail text, you must first create an optional issuer fee detail text via the Text Maintenance feature. Use the OI, Optional issuer fee text type in the DT, Detail descriptions text area, to create custom text. You can also use the statement text variable &OIAM, Optional Issuer Fee amount, in this detail text.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = ON.</p>
          <p>
            Identify the text ID of the message text for the Optional Issuer Fee
            to be displayed on statements. This is only used if you set the
            Optional Issuer Fee Statement Display Code to a 1 or a 2. To print a
            custom detail text, you must first create an optional issuer fee
            detail text via the Text Maintenance feature. Use the OI, Optional
            issuer fee text type in the DT, Detail descriptions text area, to
            create custom text. You can also use the statement text variable
            &OIAM, Optional Issuer Fee amount, in this detail text.
          </p>
        </>
      )
    }
  ],
  Info_EMessengerSettings: [
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsTurnOffPaper,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsTurnOffPaper,
      onlinePCF: 'Paper Turn Off Months Number',
      moreInfoText:
        'The Turn Off Paper after eDelivery Enrollment - Number of Months parameter determines the number of months a customer receives both paper and electronic statements after the account becomes enrolled in the eMessengerSM service.For example, you set this parameter to 03. Three months after the date contained in the electronic bill presentment (EBP) start date field on the Cardholder Master File, the System automatically generates a NM*763, Participation Indicator transaction, to change the account’s EBP participation indicator setting from a B, designating both paper and electronic statements, to an E, designating electronic statements only. ',
      moreInfo: (
        <div>
          The Turn Off Paper after eDelivery Enrollment - Number of Months
          parameter determines the number of months a customer receives both
          paper and electronic statements after the account becomes enrolled in
          the eMessenger<sup>SM</sup> service.For example, you set this
          parameter to 03. Three months after the date contained in the
          electronic bill presentment (EBP) start date field on the Cardholder
          Master File, the System automatically generates a NM*763,
          Participation Indicator transaction, to change the account’s EBP
          participation indicator setting from a B, designating both paper and
          electronic statements, to an E, designating electronic statements
          only.
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsTurnOnPaper,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsTurnOnPaper,
      onlinePCF: 'Delinquent Turn On Months Number',
      moreInfoText:
        'The Turn On Paper for Delinquent Accounts - Number of Months parameter controls statement delivery for eMessenger accounts that are set to receive only electronic statements and that become delinquent. If an account remains delinquent longer than the number of cycles you enter in this parameter, the System will begin sending both electronic and paper statements to the account. For example, you set this parameter to 02. If an eMessenger account maintains a delinquent status for two cycles, the System automatically generates a NM*763, Participation Indicator transaction, to change the account’s EBP participation indicator setting from an E, designating electronic statements only, to a D, designating delinquency.',
      moreInfo:
        'The Turn On Paper for Delinquent Accounts - Number of Months parameter controls statement delivery for eMessenger accounts that are set to receive only electronic statements and that become delinquent. If an account remains delinquent longer than the number of cycles you enter in this parameter, the System will begin sending both electronic and paper statements to the account. For example, you set this parameter to 02. If an eMessenger account maintains a delinquent status for two cycles, the System automatically generates a NM*763, Participation Indicator transaction, to change the account’s EBP participation indicator setting from an E, designating electronic statements only, to a D, designating delinquency.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsReturnToEDelivery,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsReturnToEDelivery,
      onlinePCF: 'Cured Turn Off Months Number',
      moreInfoText:
        'The Return to eDelivery after Curing Delinquency - Number of Months parameter controls statement delivery for eMessenger accounts that return to good standing from a delinquent status. If an account cures its delinquency for the number of cycles you enter in this parameter, the System returns the customer to electronic statement delivery. For example, you set this parameter to 02. If an eMessenger account cures its delinquent status for two cycles, the System automatically generates a NM*763, Participation Indicator transaction, to change the account’s EBP participation indicator setting from a D, designating delinquency, back to an E, designating electronic statements only.',
      moreInfo:
        'The Return to eDelivery after Curing Delinquency - Number of Months parameter controls statement delivery for eMessenger accounts that return to good standing from a delinquent status. If an account cures its delinquency for the number of cycles you enter in this parameter, the System returns the customer to electronic statement delivery. For example, you set this parameter to 02. If an eMessenger account cures its delinquent status for two cycles, the System automatically generates a NM*763, Participation Indicator transaction, to change the account’s EBP participation indicator setting from a D, designating delinquency, back to an E, designating electronic statements only.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsDeliquentAccountUnEnrollment,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsDeliquentAccountUnEnrollment,
      onlinePCF: 'eBILL Unenroll Delq Number Cycles',
      moreInfoText:
        'The Delinquent Account Unenrollment - Number of Cycles parameter determines the number of cycles an eMessenger account can remain delinquent before becoming deactivated from participation in the eMessengerSM service. For example, you set this parameter to 02. After an account stays delinquent for two cycles, the System automatically generates a NM*763, Participation Indicator transaction, to change the account’s EBP participation indicator setting from a D, designating delinquency, to a blank, deactivating the account from the eMessenger service. If an account cures its delinquent status sooner than the number of cycles you set in this parameter and remains cured for the number of cycles you set in the Cured Turn Off Months Number parameter, the System automatically generates a NM* 63, Participation Indicator transaction, to change the account’s EBP participation indicator setting from a D back to an E, designating electronic statements only.',
      moreInfo: (
        <div>
          The Delinquent Account Unenrollment - Number of Cycles parameter
          determines the number of cycles an eMessenger account can remain
          delinquent before becoming deactivated from participation in the
          eMessenger<sup>SM</sup> service. For example, you set this parameter
          to 02. After an account stays delinquent for two cycles, the System
          automatically generates a NM*763, Participation Indicator transaction,
          to change the account’s EBP participation indicator setting from a D,
          designating delinquency, to a blank, deactivating the account from the
          eMessenger service. If an account cures its delinquent status sooner
          than the number of cycles you set in this parameter and remains cured
          for the number of cycles you set in the Cured Turn Off Months Number
          parameter, the System automatically generates a NM* 63, Participation
          Indicator transaction, to change the account’s EBP participation
          indicator setting from a D back to an E, designating electronic
          statements only.
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsEbillLanguageOptions,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsEbillLanguageOptions,
      onlinePCF: 'eBILL Language Code',
      moreInfoText:
        'The eBILL Language Options parameter determines whether languages other than English can be used for eBILL processing. If you set this parameter to 1, eBILL accounts that have a language code set to a language other than English will receive paper statements. The System resets the eBILL participation indicator for these accounts to blank, deactivating eBILL participation.',
      moreInfo:
        'The eBILL Language Options parameter determines whether languages other than English can be used for eBILL processing. If you set this parameter to 1, eBILL accounts that have a language code set to a language other than English will receive paper statements. The System resets the eBILL participation indicator for these accounts to blank, deactivating eBILL participation.'
    }
  ],
  Info_CITDisclosuresPrintOrderNumbers: [
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCPICBP,
      fieldName: MethodFieldNameEnum.ManageStatementProductionSettingsCPICBP,
      onlinePCF: 'CP/IC/BP',
      moreInfoText:
        'The Print Order 1 through 17 parameters determine the order in which the System places messages that disclose changes to revolving balance terms on a customer notification letter or statement. Your entries in these parameters control non-penalty disclosures. If you have set the Penalty Priority parameter in the Change in Terms section to a Y, the System will place the penalty disclosure message first, followed by the revolving balance messages placed in the order you specify in these print order parameters. Disclosures for plans, promotions, and protected balances follow the revolving balance disclosures.',
      moreInfo:
        'The Print Order 1 through 17 parameters determine the order in which the System places messages that disclose changes to revolving balance terms on a customer notification letter or statement. Your entries in these parameters control non-penalty disclosures. If you have set the Penalty Priority parameter in the Change in Terms section to a Y, the System will place the penalty disclosure message first, followed by the revolving balance messages placed in the order you specify in these print order parameters. Disclosures for plans, promotions, and protected balances follow the revolving balance disclosures.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCPICID,
      fieldName: MethodFieldNameEnum.ManageStatementProductionSettingsCPICID,
      onlinePCF: 'CP/IC/ID',
      moreInfoText:
        'The Print Order 1 through 17 parameters determine the order in which the System places messages that disclose changes to revolving balance terms on a customer notification letter or statement. Your entries in these parameters control non-penalty disclosures. If you have set the Penalty Priority parameter in the Change in Terms section to a Y, the System will place the penalty disclosure message first, followed by the revolving balance messages placed in the order you specify in these print order parameters. Disclosures for plans, promotions, and protected balances follow the revolving balance disclosures.',
      moreInfo:
        'The Print Order 1 through 17 parameters determine the order in which the System places messages that disclose changes to revolving balance terms on a customer notification letter or statement. Your entries in these parameters control non-penalty disclosures. If you have set the Penalty Priority parameter in the Change in Terms section to a Y, the System will place the penalty disclosure message first, followed by the revolving balance messages placed in the order you specify in these print order parameters. Disclosures for plans, promotions, and protected balances follow the revolving balance disclosures.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCPICII,
      fieldName: MethodFieldNameEnum.ManageStatementProductionSettingsCPICII,
      onlinePCF: 'CP/IC/II',
      moreInfoText:
        'The Print Order 1 through 17 parameters determine the order in which the System places messages that disclose changes to revolving balance terms on a customer notification letter or statement. Your entries in these parameters control non-penalty disclosures. If you have set the Penalty Priority parameter in the Change in Terms section to a Y, the System will place the penalty disclosure message first, followed by the revolving balance messages placed in the order you specify in these print order parameters. Disclosures for plans, promotions, and protected balances follow the revolving balance disclosures.',
      moreInfo:
        'The Print Order 1 through 17 parameters determine the order in which the System places messages that disclose changes to revolving balance terms on a customer notification letter or statement. Your entries in these parameters control non-penalty disclosures. If you have set the Penalty Priority parameter in the Change in Terms section to a Y, the System will place the penalty disclosure message first, followed by the revolving balance messages placed in the order you specify in these print order parameters. Disclosures for plans, promotions, and protected balances follow the revolving balance disclosures.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIM,
      fieldName: MethodFieldNameEnum.ManageStatementProductionSettingsCPICIM,
      onlinePCF: 'CP/IC/IM',
      moreInfoText:
        'The Print Order 1 through 17 parameters determine the order in which the System places messages that disclose changes to revolving balance terms on a customer notification letter or statement. Your entries in these parameters control non-penalty disclosures. If you have set the Penalty Priority parameter in the Change in Terms section to a Y, the System will place the penalty disclosure message first, followed by the revolving balance messages placed in the order you specify in these print order parameters. Disclosures for plans, promotions, and protected balances follow the revolving balance disclosures.',
      moreInfo:
        'The Print Order 1 through 17 parameters determine the order in which the System places messages that disclose changes to revolving balance terms on a customer notification letter or statement. Your entries in these parameters control non-penalty disclosures. If you have set the Penalty Priority parameter in the Change in Terms section to a Y, the System will place the penalty disclosure message first, followed by the revolving balance messages placed in the order you specify in these print order parameters. Disclosures for plans, promotions, and protected balances follow the revolving balance disclosures.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIP,
      fieldName: MethodFieldNameEnum.ManageStatementProductionSettingsCPICIP,
      onlinePCF: 'CP/IC/IP',
      moreInfoText:
        'The Print Order 1 through 17 parameters determine the order in which the System places messages that disclose changes to revolving balance terms on a customer notification letter or statement. Your entries in these parameters control non-penalty disclosures. If you have set the Penalty Priority parameter in the Change in Terms section to a Y, the System will place the penalty disclosure message first, followed by the revolving balance messages placed in the order you specify in these print order parameters. Disclosures for plans, promotions, and protected balances follow the revolving balance disclosures.',
      moreInfo:
        'The Print Order 1 through 17 parameters determine the order in which the System places messages that disclose changes to revolving balance terms on a customer notification letter or statement. Your entries in these parameters control non-penalty disclosures. If you have set the Penalty Priority parameter in the Change in Terms section to a Y, the System will place the penalty disclosure message first, followed by the revolving balance messages placed in the order you specify in these print order parameters. Disclosures for plans, promotions, and protected balances follow the revolving balance disclosures.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCPICIR,
      fieldName: MethodFieldNameEnum.ManageStatementProductionSettingsCPICIR,
      onlinePCF: 'CP/IC/IR',
      moreInfoText:
        'The Print Order 1 through 17 parameters determine the order in which the System places messages that disclose changes to revolving balance terms on a customer notification letter or statement. Your entries in these parameters control non-penalty disclosures. If you have set the Penalty Priority parameter in the Change in Terms section to a Y, the System will place the penalty disclosure message first, followed by the revolving balance messages placed in the order you specify in these print order parameters. Disclosures for plans, promotions, and protected balances follow the revolving balance disclosures.',
      moreInfo:
        'The Print Order 1 through 17 parameters determine the order in which the System places messages that disclose changes to revolving balance terms on a customer notification letter or statement. Your entries in these parameters control non-penalty disclosures. If you have set the Penalty Priority parameter in the Change in Terms section to a Y, the System will place the penalty disclosure message first, followed by the revolving balance messages placed in the order you specify in these print order parameters. Disclosures for plans, promotions, and protected balances follow the revolving balance disclosures.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCPICMF,
      fieldName: MethodFieldNameEnum.ManageStatementProductionSettingsCPICMF,
      onlinePCF: 'CP/IC/MF',
      moreInfoText:
        'The Print Order 1 through 17 parameters determine the order in which the System places messages that disclose changes to revolving balance terms on a customer notification letter or statement. Your entries in these parameters control non-penalty disclosures. If you have set the Penalty Priority parameter in the Change in Terms section to a Y, the System will place the penalty disclosure message first, followed by the revolving balance messages placed in the order you specify in these print order parameters. Disclosures for plans, promotions, and protected balances follow the revolving balance disclosures.',
      moreInfo:
        'The Print Order 1 through 17 parameters determine the order in which the System places messages that disclose changes to revolving balance terms on a customer notification letter or statement. Your entries in these parameters control non-penalty disclosures. If you have set the Penalty Priority parameter in the Change in Terms section to a Y, the System will place the penalty disclosure message first, followed by the revolving balance messages placed in the order you specify in these print order parameters. Disclosures for plans, promotions, and protected balances follow the revolving balance disclosures.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCPICVI,
      fieldName: MethodFieldNameEnum.ManageStatementProductionSettingsCPICVI,
      onlinePCF: 'CP/IC/VI',
      moreInfoText:
        'The Print Order 1 through 17 parameters determine the order in which the System places messages that disclose changes to revolving balance terms on a customer notification letter or statement. Your entries in these parameters control non-penalty disclosures. If you have set the Penalty Priority parameter in the Change in Terms section to a Y, the System will place the penalty disclosure message first, followed by the revolving balance messages placed in the order you specify in these print order parameters. Disclosures for plans, promotions, and protected balances follow the revolving balance disclosures.',
      moreInfo:
        'The Print Order 1 through 17 parameters determine the order in which the System places messages that disclose changes to revolving balance terms on a customer notification letter or statement. Your entries in these parameters control non-penalty disclosures. If you have set the Penalty Priority parameter in the Change in Terms section to a Y, the System will place the penalty disclosure message first, followed by the revolving balance messages placed in the order you specify in these print order parameters. Disclosures for plans, promotions, and protected balances follow the revolving balance disclosures.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOAC,
      fieldName: MethodFieldNameEnum.ManageStatementProductionSettingsCPIOAC,
      onlinePCF: 'CP/IO/AC',
      moreInfoText:
        'The Print Order 1 through 17 parameters determine the order in which the System places messages that disclose changes to revolving balance terms on a customer notification letter or statement. Your entries in these parameters control non-penalty disclosures. If you have set the Penalty Priority parameter in the Change in Terms section to a Y, the System will place the penalty disclosure message first, followed by the revolving balance messages placed in the order you specify in these print order parameters. Disclosures for plans, promotions, and protected balances follow the revolving balance disclosures.',
      moreInfo:
        'The Print Order 1 through 17 parameters determine the order in which the System places messages that disclose changes to revolving balance terms on a customer notification letter or statement. Your entries in these parameters control non-penalty disclosures. If you have set the Penalty Priority parameter in the Change in Terms section to a Y, the System will place the penalty disclosure message first, followed by the revolving balance messages placed in the order you specify in these print order parameters. Disclosures for plans, promotions, and protected balances follow the revolving balance disclosures.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOCI,
      fieldName: MethodFieldNameEnum.ManageStatementProductionSettingsCPIOCI,
      onlinePCF: 'CP/IO/CI',
      moreInfoText:
        'The Print Order 1 through 17 parameters determine the order in which the System places messages that disclose changes to revolving balance terms on a customer notification letter or statement. Your entries in these parameters control non-penalty disclosures. If you have set the Penalty Priority parameter in the Change in Terms section to a Y, the System will place the penalty disclosure message first, followed by the revolving balance messages placed in the order you specify in these print order parameters. Disclosures for plans, promotions, and protected balances follow the revolving balance disclosures.',
      moreInfo:
        'The Print Order 1 through 17 parameters determine the order in which the System places messages that disclose changes to revolving balance terms on a customer notification letter or statement. Your entries in these parameters control non-penalty disclosures. If you have set the Penalty Priority parameter in the Change in Terms section to a Y, the System will place the penalty disclosure message first, followed by the revolving balance messages placed in the order you specify in these print order parameters. Disclosures for plans, promotions, and protected balances follow the revolving balance disclosures.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOMC,
      fieldName: MethodFieldNameEnum.ManageStatementProductionSettingsCPIOMC,
      onlinePCF: 'CP/IO/MC',
      moreInfoText:
        'The Print Order 1 through 17 parameters determine the order in which the System places messages that disclose changes to revolving balance terms on a customer notification letter or statement. Your entries in these parameters control non-penalty disclosures. If you have set the Penalty Priority parameter in the Change in Terms section to a Y, the System will place the penalty disclosure message first, followed by the revolving balance messages placed in the order you specify in these print order parameters. Disclosures for plans, promotions, and protected balances follow the revolving balance disclosures.',
      moreInfo:
        'The Print Order 1 through 17 parameters determine the order in which the System places messages that disclose changes to revolving balance terms on a customer notification letter or statement. Your entries in these parameters control non-penalty disclosures. If you have set the Penalty Priority parameter in the Change in Terms section to a Y, the System will place the penalty disclosure message first, followed by the revolving balance messages placed in the order you specify in these print order parameters. Disclosures for plans, promotions, and protected balances follow the revolving balance disclosures.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCPIOMI,
      fieldName: MethodFieldNameEnum.ManageStatementProductionSettingsCPIOMI,
      onlinePCF: 'CP/IO/MI',
      moreInfoText:
        'The Print Order 1 through 17 parameters determine the order in which the System places messages that disclose changes to revolving balance terms on a customer notification letter or statement. Your entries in these parameters control non-penalty disclosures. If you have set the Penalty Priority parameter in the Change in Terms section to a Y, the System will place the penalty disclosure message first, followed by the revolving balance messages placed in the order you specify in these print order parameters. Disclosures for plans, promotions, and protected balances follow the revolving balance disclosures.',
      moreInfo:
        'The Print Order 1 through 17 parameters determine the order in which the System places messages that disclose changes to revolving balance terms on a customer notification letter or statement. Your entries in these parameters control non-penalty disclosures. If you have set the Penalty Priority parameter in the Change in Terms section to a Y, the System will place the penalty disclosure message first, followed by the revolving balance messages placed in the order you specify in these print order parameters. Disclosures for plans, promotions, and protected balances follow the revolving balance disclosures.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFLC,
      fieldName: MethodFieldNameEnum.ManageStatementProductionSettingsCPPFLC,
      onlinePCF: 'CP/PF/LC',
      moreInfoText:
        'The Print Order 1 through 17 parameters determine the order in which the System places messages that disclose changes to revolving balance terms on a customer notification letter or statement. Your entries in these parameters control non-penalty disclosures. If you have set the Penalty Priority parameter in the Change in Terms section to a Y, the System will place the penalty disclosure message first, followed by the revolving balance messages placed in the order you specify in these print order parameters. Disclosures for plans, promotions, and protected balances follow the revolving balance disclosures.',
      moreInfo:
        'The Print Order 1 through 17 parameters determine the order in which the System places messages that disclose changes to revolving balance terms on a customer notification letter or statement. Your entries in these parameters control non-penalty disclosures. If you have set the Penalty Priority parameter in the Change in Terms section to a Y, the System will place the penalty disclosure message first, followed by the revolving balance messages placed in the order you specify in these print order parameters. Disclosures for plans, promotions, and protected balances follow the revolving balance disclosures.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFOC,
      fieldName: MethodFieldNameEnum.ManageStatementProductionSettingsCPPFOC,
      onlinePCF: 'CP/PF/OC',
      moreInfoText:
        'The Print Order 1 through 17 parameters determine the order in which the System places messages that disclose changes to revolving balance terms on a customer notification letter or statement. Your entries in these parameters control non-penalty disclosures. If you have set the Penalty Priority parameter in the Change in Terms section to a Y, the System will place the penalty disclosure message first, followed by the revolving balance messages placed in the order you specify in these print order parameters. Disclosures for plans, promotions, and protected balances follow the revolving balance disclosures.',
      moreInfo:
        'The Print Order 1 through 17 parameters determine the order in which the System places messages that disclose changes to revolving balance terms on a customer notification letter or statement. Your entries in these parameters control non-penalty disclosures. If you have set the Penalty Priority parameter in the Change in Terms section to a Y, the System will place the penalty disclosure message first, followed by the revolving balance messages placed in the order you specify in these print order parameters. Disclosures for plans, promotions, and protected balances follow the revolving balance disclosures.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCPPFRC,
      fieldName: MethodFieldNameEnum.ManageStatementProductionSettingsCPPFRC,
      onlinePCF: 'CP/PF/RC',
      moreInfoText:
        'The Print Order 1 through 17 parameters determine the order in which the System places messages that disclose changes to revolving balance terms on a customer notification letter or statement. Your entries in these parameters control non-penalty disclosures. If you have set the Penalty Priority parameter in the Change in Terms section to a Y, the System will place the penalty disclosure message first, followed by the revolving balance messages placed in the order you specify in these print order parameters. Disclosures for plans, promotions, and protected balances follow the revolving balance disclosures.',
      moreInfo:
        'The Print Order 1 through 17 parameters determine the order in which the System places messages that disclose changes to revolving balance terms on a customer notification letter or statement. Your entries in these parameters control non-penalty disclosures. If you have set the Penalty Priority parameter in the Change in Terms section to a Y, the System will place the penalty disclosure message first, followed by the revolving balance messages placed in the order you specify in these print order parameters. Disclosures for plans, promotions, and protected balances follow the revolving balance disclosures.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCPPOMP,
      fieldName: MethodFieldNameEnum.ManageStatementProductionSettingsCPPOMP,
      onlinePCF: 'CP/PO/MP',
      moreInfoText:
        'The Print Order 1 through 17 parameters determine the order in which the System places messages that disclose changes to revolving balance terms on a customer notification letter or statement. Your entries in these parameters control non-penalty disclosures. If you have set the Penalty Priority parameter in the Change in Terms section to a Y, the System will place the penalty disclosure message first, followed by the revolving balance messages placed in the order you specify in these print order parameters. Disclosures for plans, promotions, and protected balances follow the revolving balance disclosures.',
      moreInfo:
        'The Print Order 1 through 17 parameters determine the order in which the System places messages that disclose changes to revolving balance terms on a customer notification letter or statement. Your entries in these parameters control non-penalty disclosures. If you have set the Penalty Priority parameter in the Change in Terms section to a Y, the System will place the penalty disclosure message first, followed by the revolving balance messages placed in the order you specify in these print order parameters. Disclosures for plans, promotions, and protected balances follow the revolving balance disclosures.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCPICME,
      fieldName: MethodFieldNameEnum.ManageStatementProductionSettingsCPICME,
      onlinePCF: 'CP/IC/ME',
      moreInfoText:
        'The Print Order 1 through 17 parameters determine the order in which the System places messages that disclose changes to revolving balance terms on a customer notification letter or statement. Your entries in these parameters control non-penalty disclosures. If you have set the Penalty Priority parameter in the Change in Terms section to a Y, the System will place the penalty disclosure message first, followed by the revolving balance messages placed in the order you specify in these print order parameters. Disclosures for plans, promotions, and protected balances follow the revolving balance disclosures.',
      moreInfo:
        'The Print Order 1 through 17 parameters determine the order in which the System places messages that disclose changes to revolving balance terms on a customer notification letter or statement. Your entries in these parameters control non-penalty disclosures. If you have set the Penalty Priority parameter in the Change in Terms section to a Y, the System will place the penalty disclosure message first, followed by the revolving balance messages placed in the order you specify in these print order parameters. Disclosures for plans, promotions, and protected balances follow the revolving balance disclosures.'
    }
  ],
  Info_PreviousCycleMessageText: [
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycCashAdvance,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsCashAdvanceFinanceCharges,
      onlinePCF:
        MethodFieldNameEnum.ManageStatementProductionSettingsPrevCycCashAdvance,
      moreInfoText:
        'The following parameters identify description text for the finance charges, item charges, penalty fees, credit insurance premiums, etc. that are displayed when the transaction is back-dated to the previous cycle.',
      moreInfo:
        'The following parameters identify description text for the finance charges, item charges, penalty fees, credit insurance premiums, etc. that are displayed when the transaction is back-dated to the previous cycle.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycCashItemCharge,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsCashItemCharges,
      onlinePCF:
        MethodFieldNameEnum.ManageStatementProductionSettingsPrevCycCashItemCharge,
      moreInfoText:
        'The following parameters identify description text for the finance charges, item charges, penalty fees, credit insurance premiums, etc. that are displayed when the transaction is back-dated to the previous cycle.',
      moreInfo:
        'The following parameters identify description text for the finance charges, item charges, penalty fees, credit insurance premiums, etc. that are displayed when the transaction is back-dated to the previous cycle.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycMerchandise,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsMerchandiseFinanceCharges,
      onlinePCF:
        MethodFieldNameEnum.ManageStatementProductionSettingsPrevCycMerchandise,
      moreInfoText:
        'The following parameters identify description text for the finance charges, item charges, penalty fees, credit insurance premiums, etc. that are displayed when the transaction is back-dated to the previous cycle.',
      moreInfo:
        'The following parameters identify description text for the finance charges, item charges, penalty fees, credit insurance premiums, etc. that are displayed when the transaction is back-dated to the previous cycle.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycMdseItemCharge,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsMerchandiseItemCharges,
      onlinePCF:
        MethodFieldNameEnum.ManageStatementProductionSettingsPrevCycMdseItemCharge,
      moreInfoText:
        'The following parameters identify description text for the finance charges, item charges, penalty fees, credit insurance premiums, etc. that are displayed when the transaction is back-dated to the previous cycle.',
      moreInfo:
        'The following parameters identify description text for the finance charges, item charges, penalty fees, credit insurance premiums, etc. that are displayed when the transaction is back-dated to the previous cycle.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsMinimumFinanceCharges,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsMinimumFinanceCharges,
      onlinePCF: 'Prev Cyc Minimum Finance Chg',
      moreInfoText:
        'The following parameters identify description text for the finance charges, item charges, penalty fees, credit insurance premiums, etc. that are displayed when the transaction is back-dated to the previous cycle.',
      moreInfo:
        'The following parameters identify description text for the finance charges, item charges, penalty fees, credit insurance premiums, etc. that are displayed when the transaction is back-dated to the previous cycle.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsActivityFee,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsActivityFee,
      onlinePCF: 'Prev Cyc Activity Fee',
      moreInfoText:
        'The following parameters identify description text for the finance charges, item charges, penalty fees, credit insurance premiums, etc. that are displayed when the transaction is back-dated to the previous cycle.',
      moreInfo:
        'The following parameters identify description text for the finance charges, item charges, penalty fees, credit insurance premiums, etc. that are displayed when the transaction is back-dated to the previous cycle.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycLateCharge,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsLateCharge,
      onlinePCF:
        MethodFieldNameEnum.ManageStatementProductionSettingsPrevCycLateCharge,
      moreInfoText:
        'The following parameters identify description text for the finance charges, item charges, penalty fees, credit insurance premiums, etc. that are displayed when the transaction is back-dated to the previous cycle.',
      moreInfo:
        'The following parameters identify description text for the finance charges, item charges, penalty fees, credit insurance premiums, etc. that are displayed when the transaction is back-dated to the previous cycle.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsPrevCycOverlimitCharge,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsOverlimitCharge,
      onlinePCF:
        MethodFieldNameEnum.ManageStatementProductionSettingsPrevCycOverlimitCharge,
      moreInfoText:
        'The following parameters identify description text for the finance charges, item charges, penalty fees, credit insurance premiums, etc. that are displayed when the transaction is back-dated to the previous cycle.',
      moreInfo:
        'The following parameters identify description text for the finance charges, item charges, penalty fees, credit insurance premiums, etc. that are displayed when the transaction is back-dated to the previous cycle.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsuranceCharge,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsCreditLifeInsuranceCharge,
      onlinePCF: 'Prev Cyc Credit Life Ins',
      moreInfoText:
        'The following parameters identify description text for the finance charges, item charges, penalty fees, credit insurance premiums, etc. that are displayed when the transaction is back-dated to the previous cycle.',
      moreInfo:
        'The following parameters identify description text for the finance charges, item charges, penalty fees, credit insurance premiums, etc. that are displayed when the transaction is back-dated to the previous cycle.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsOtherDebitAdjustmentText,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsOtherDebitAdjustmentText,
      onlinePCF: 'Prev Cyc Other Debits',
      moreInfoText:
        'The following parameters identify description text for the finance charges, item charges, penalty fees, credit insurance premiums, etc. that are displayed when the transaction is back-dated to the previous cycle.',
      moreInfo:
        'The following parameters identify description text for the finance charges, item charges, penalty fees, credit insurance premiums, etc. that are displayed when the transaction is back-dated to the previous cycle.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsOtherCreditAdjustmentText,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsOtherCreditAdjustmentText,
      onlinePCF: 'Prev Cyc Other Credits',
      moreInfoText:
        'The following parameters identify description text for the finance charges, item charges, penalty fees, credit insurance premiums, etc. that are displayed when the transaction is back-dated to the previous cycle.',
      moreInfo:
        'The following parameters identify description text for the finance charges, item charges, penalty fees, credit insurance premiums, etc. that are displayed when the transaction is back-dated to the previous cycle.'
    }
  ],
  Info_StatementFunctionality: [
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCreditBalanceHoldCode,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsCreditBalanceHoldCode,
      onlinePCF:
        MethodFieldNameEnum.ManageStatementProductionSettingsCreditBalanceHoldCode,
      moreInfoText:
        'The Credit Balance Hold Code parameter controls the automatic setting of statement hold codes on accounts cycling with credit balances.',
      moreInfo:
        'The Credit Balance Hold Code parameter controls the automatic setting of statement hold codes on accounts cycling with credit balances.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsPrintHardCopySecurityStatements,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsPrintHardCopySecurityStatements,
      onlinePCF: 'Type 6 Statement Print Control',
      moreInfoText:
        'The Type 6 Statement Print Control parameter controls whether the System prints hardcopy security statements.',
      moreInfo:
        'The Type 6 Statement Print Control parameter controls whether the System prints hardcopy security statements.'
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsSCSStatementOverrideTableID,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsSCSStatementOverrideTableID,
      onlinePCF: 'Override Table ID',
      moreInfoText:
        'If you use the Strategic Communications Solution (SCS) for statement production, you can use the Override Table ID parameter to identify the table you created using the SO, State Override table area of the Table Maintenance feature which allows you to control the printing of zero balance cardholder statements at the state level and by external status. You can create multiple State Override tables. You can use this parameter only if the Decision Processing Flag parameter in the Activation Controls section (PF PC AC) of the Product Control File is set to 5, 6, or 7.',
      moreInfo:
        'If you use the Strategic Communications Solution (SCS) for statement production, you can use the Override Table ID parameter to identify the table you created using the SO, State Override table area of the Table Maintenance feature which allows you to control the printing of zero balance cardholder statements at the state level and by external status. You can create multiple State Override tables. You can use this parameter only if the Decision Processing Flag parameter in the Activation Controls section (PF PC AC) of the Product Control File is set to 5, 6, or 7.'
    }
  ],
  Info_OtherDisplayOptions: [
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsLateFeeWaiverText,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsLateFeeWaiverText,
      onlinePCF: 'Optional Finance Charge Statement Messages Late Fee Waiver',
      moreInfoText:
        'Text Area = DT, Text Type = LW. This is the text of the statement message in the event the issuer waives the late charge.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = LW.</p>
          <p>
            This is the text of the statement message in the event the issuer
            waives the late charge.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsGenericMessageAboveRevolvingBalance,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsGenericMessageAboveRevolvingBalance,
      onlinePCF: 'Revolv Above Box Mssg ID',
      moreInfoText:
        'Text Area = TC, Text Type = AB. The Generic Message Above Revolving Balance parameter contains the identifier of the generic message that appears above the delineated disclosure box for revolving balances. You build and assign an identifier to this message using the AB, Above the Box Message text type, within the TC, Terms and Conditions message text area. The System edits this parameter for a valid message identifier.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = TC, Text Type = AB.</p>
          <p>
            The Generic Message Above Revolving Balance parameter contains the
            identifier of the generic message that appears above the delineated
            disclosure box for revolving balances. You build and assign an
            identifier to this message using the AB, Above the Box Message text
            type, within the TC, Terms and Conditions message text area. The
            System edits this parameter for a valid message identifier.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsGenericMessageBelowRevolvingBalance,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsGenericMessageBelowRevolvingBalance,
      onlinePCF: 'Revolv Below Box Mssg ID',
      moreInfoText:
        'Text Area = TC, Text Type = UB. The Generic Message Below Revolving Balance parameter contains the identifier of the generic message that appears below the delineated disclosure box for revolving balances. You build and assign an identifier to this message using the UB, Below the Box Message text type, within the TC, Terms and Conditions message text area. The System edits this parameter for a valid message identifier.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = TC, Text Type = UB.</p>
          <p>
            The Generic Message Below Revolving Balance parameter contains the
            identifier of the generic message that appears below the delineated
            disclosure box for revolving balances. You build and assign an
            identifier to this message using the UB, Below the Box Message text
            type, within the TC, Terms and Conditions message text area. The
            System edits this parameter for a valid message identifier.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsNormalTermsDateID,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsNormalTermsDateID,
      onlinePCF: 'Normal CIT Disclosure Header PCF TC/TD',
      moreInfoText:
        'Text Area = TC, Text Type = TD. The Normal Terms Date ID parameter contains the text ID of the terms date header message in normal CIT and future terms disclosures.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = TC, Text Type = TD.</p>
          <p>
            The Normal Terms Date ID parameter contains the text ID of the terms
            date header message in normal CIT and future terms disclosures.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCrossCycleFeeDisplayOptions,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsCrossCycleFeeDisplayOptions,
      onlinePCF: 'Cross Cycle Fee Display Code',
      moreInfoText:
        'The Cross Cycle Fee Display Code parameter determines whether a fee transaction that is a cross-cycle or backdated adjustment appears in the fee group on the cardholder statement. Be aware that this parameter affects only statements with the sorting and grouping functionality implemented to help you comply with the CARD (Credit Card Accountability Responsibility and Disclosure) Act of 2009 and Regulation Z - Truth in Lending Act. If your statements are not produced with this functionality and you want to use this feature for your statements, you must submit a program request. Contact your Fiserv Service partner for more information.',
      moreInfo: (
        <>
          <p>
            The Cross Cycle Fee Display Code parameter determines whether a fee
            transaction that is a cross-cycle or backdated adjustment appears in
            the fee group on the cardholder statement. Be aware that this
            parameter affects only statements with the sorting and grouping
            functionality implemented to help you comply with the CARD (Credit
            Card Accountability Responsibility and Disclosure) Act of 2009 and
            Regulation Z - Truth in Lending Act. If your statements are not
            produced with this functionality and you want to use this feature
            for your statements, you must submit a program request. Contact your
            Fiserv Service partner for more information.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCrossCycleInterestDisplayOptions,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsCrossCycleInterestDisplayOptions,
      onlinePCF: 'Cross Cycle Interest Display Code',
      moreInfoText:
        'The Cross-Cycle Interest Display Options parameter determines whether an interest transaction that is a cross-cycle or backdated adjustment appears in the interest group on the cardholder statement. Be aware that this parameter affects only statements with the sorting and grouping functionality implemented to help you comply with the CARD (Credit Card Accountability Responsibility and Disclosure) Act of 2009 and Regulation Z - Truth in Lending Act. If your statements are not produced with this functionality and you want to use this feature for your statements, you must submit a program request. Contact your Fiserv Service partner for more information.',
      moreInfo: (
        <>
          <p>
            The Cross-Cycle Interest Display Options parameter determines
            whether an interest transaction that is a cross-cycle or backdated
            adjustment appears in the interest group on the cardholder
            statement. Be aware that this parameter affects only statements with
            the sorting and grouping functionality implemented to help you
            comply with the CARD (Credit Card Accountability Responsibility and
            Disclosure) Act of 2009 and Regulation Z - Truth in Lending Act. If
            your statements are not produced with this functionality and you
            want to use this feature for your statements, you must submit a
            program request. Contact your Fiserv Service partner for more
            information.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettings280TransactionsSeriesDisplayOptions,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettings280TransactionsSeriesDisplayOptions,
      onlinePCF: 'Tran 280 Series Display Code',
      moreInfoText:
        'The 280-Transactions Series Display Options parameter determines whether 280-series refund interest or fee transactions are included in the interest group, the fee group, or both groups. Be aware that this parameter affects only statements with the sorting and grouping functionality implemented to help you comply with the CARD (Credit Card Accountability Responsibility and Disclosure) Act of 2009 and Regulation Z - Truth in Lending Act.',
      moreInfo: (
        <>
          <p>
            The 280-Transactions Series Display Options parameter determines
            whether 280-series refund interest or fee transactions are included
            in the interest group, the fee group, or both groups. Be aware that
            this parameter affects only statements with the sorting and grouping
            functionality implemented to help you comply with the CARD (Credit
            Card Accountability Responsibility and Disclosure) Act of 2009 and
            Regulation Z - Truth in Lending Act.
          </p>
        </>
      )
    }
  ],
  Info_MiscellaneousAdjustmentText: [
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsSaleAmountAdjustmentText,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsSaleAmountAdjustmentText,
      onlinePCF: 'Misc Adjs Sale Amount Adj',
      moreInfoText:
        'Text Area = DT, Text Type = SA.Each miscellaneous adjustment parameter consists of a text identification code and text type code. Use these codes to change description text for a particular adjustment via the Text Maintenance feature. This Sale Amount Adjustment Text identifies the text displayed on the statement for a change to a sale amount using tran code 259. To specify your own text, type its text identification code over the code displayed. Use a code for text that is either scheduled or in production.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = SA.</p>
          <p>
            Each miscellaneous adjustment parameter consists of a text
            identification code and text type code. Use these codes to change
            description text for a particular adjustment via the Text
            Maintenance feature. This Sale Amount Adjustment Text identifies the
            text displayed on the statement for a change to a sale amount using
            tran code 259. To specify your own text, type its text
            identification code over the code displayed. Use a code for text
            that is either scheduled or in production.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCashAdvanceAmountAdjustmentText,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsCashAdvanceAmountAdjustmentText,
      onlinePCF: 'Misc Adjs Cash Amount Adj',
      moreInfoText:
        'Text Area = DT, Text Type = CA. Each miscellaneous adjustment parameter consists of a text identification code and text type code. Use these codes to change description text for a particular adjustment via the Text Maintenance feature. This Cash Advance Adjustment Text identifies the text displayed on the statement for a change to a cash advance amount using tran code 260. To specify your own text, type its text identification code over the code displayed. Use a code for text that is either scheduled or in production.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = CA.</p>
          <p>
            Each miscellaneous adjustment parameter consists of a text
            identification code and text type code. Use these codes to change
            description text for a particular adjustment via the Text
            Maintenance feature. This Cash Advance Adjustment Text identifies
            the text displayed on the statement for a change to a cash advance
            amount using tran code 260. To specify your own text, type its text
            identification code over the code displayed. Use a code for text
            that is either scheduled or in production.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsReturnAmountAdjustmentText,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsReturnAmountAdjustmentText,
      onlinePCF: 'Misc Adjs Return Amt Adj',
      moreInfoText:
        'Text Area = DT, Text Type = RA.Each miscellaneous adjustment parameter consists of a text identification code and text type code. Use these codes to change description text for a particular adjustment via the Text Maintenance feature. This Return Adjustment Text identifies the text displayed on the statement for a change to a return amount using tran code 261. To specify your own text, type its text identification code over the code displayed. Use a code for text that is either scheduled or in production.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = RA.</p>
          <p>
            Each miscellaneous adjustment parameter consists of a text
            identification code and text type code. Use these codes to change
            description text for a particular adjustment via the Text
            Maintenance feature. This Return Adjustment Text identifies the text
            displayed on the statement for a change to a return amount using
            tran code 261. To specify your own text, type its text
            identification code over the code displayed. Use a code for text
            that is either scheduled or in production.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsPaymentAmountAdjustmentText,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsPaymentAmountAdjustmentText,
      onlinePCF: 'Misc Adjs Payment Amt Adj	',
      moreInfoText:
        'Text Area = DT, Text Type = PJ. Each miscellaneous adjustment parameter consists of a text identification code and text type code. Use these codes to change description text for a particular adjustment via the Text Maintenance feature. This Payment Adjustment Text identifies the text displayed on the statement for a change to a payment amount using tran code 273. To specify your own text, type its text identification code over the code displayed. Use a code for text that is either scheduled or in production.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = PJ.</p>
          <p>
            Each miscellaneous adjustment parameter consists of a text
            identification code and text type code. Use these codes to change
            description text for a particular adjustment via the Text
            Maintenance feature. This Payment Adjustment Text identifies the
            text displayed on the statement for a change to a payment amount
            using tran code 273. To specify your own text, type its text
            identification code over the code displayed. Use a code for text
            that is either scheduled or in production.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffPrincipalAdjustmentTextLine1,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsChargeOffPrincipalAdjustmentTextLine1,
      onlinePCF: 'Chargeoff Adjs Total Acct Line-1',
      moreInfoText:
        'Text Area = DT, Text Type = CP. Each miscellaneous adjustment parameter consists of a text identification code and text type code. Use these codes to change description text for a particular adjustment via the Text Maintenance feature. This Charge-off Principal Adjustment Text identifies the text displayed on the statement for a charged-off account principal using tran code 371. To specify your own text, type its text identification code over the code displayed. Use a code for text that is either scheduled or in production.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = CP.</p>
          <p>
            Each miscellaneous adjustment parameter consists of a text
            identification code and text type code. Use these codes to change
            description text for a particular adjustment via the Text
            Maintenance feature. This Charge-off Principal Adjustment Text
            identifies the text displayed on the statement for a charged-off
            account principal using tran code 371. To specify your own text,
            type its text identification code over the code displayed. Use a
            code for text that is either scheduled or in production.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffFinanceChargesAdjustmentTextLine2,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsChargeOffFinanceChargesAdjustmentTextLine2,
      onlinePCF: 'Chargeoff Adjs Total Acct Line-2',
      moreInfoText:
        'Text Area = DT, Text Type = CF. Each miscellaneous adjustment parameter consists of a text identification code and text type code. Use these codes to change description text for a particular adjustment via the Text Maintenance feature. This Charge-off Principal Adjustment Text identifies the text displayed on the statement for a charged-off account finance charges using tran code 371/376. To specify your own text, type its text identification code over the code displayed. Use a code for text that is either scheduled or in production.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = CF.</p>
          <p>
            Each miscellaneous adjustment parameter consists of a text
            identification code and text type code. Use these codes to change
            description text for a particular adjustment via the Text
            Maintenance feature. This Charge-off Principal Adjustment Text
            identifies the text displayed on the statement for a charged-off
            account finance charges using tran code 371/376. To specify your own
            text, type its text identification code over the code displayed. Use
            a code for text that is either scheduled or in production.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsMiscellaneousSpecificCreditAdjustmentText,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsMiscellaneousSpecificCreditAdjustmentText,
      onlinePCF: 'Misc Adjs Specific Credit',
      moreInfoText:
        'Text Area = DT, Text Type = AJ. Each miscellaneous adjustment parameter consists of a text identification code and text type code. Use these codes to change description text for a particular adjustment via the Text Maintenance feature. This Specific Credit Adjustment Text identifies the text displayed on the statement for a change to a specific credit using tran code 280. To specify your own text, type its text identification code over the code displayed. Use a code for text that is either scheduled or in production.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = AJ.</p>
          <p>
            Each miscellaneous adjustment parameter consists of a text
            identification code and text type code. Use these codes to change
            description text for a particular adjustment via the Text
            Maintenance feature. This Specific Credit Adjustment Text identifies
            the text displayed on the statement for a change to a specific
            credit using tran code 280. To specify your own text, type its text
            identification code over the code displayed. Use a code for text
            that is either scheduled or in production.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffSmallBalanceAdjustmentText,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsChargeOffSmallBalanceAdjustmentText,
      onlinePCF: 'Chargeoff Adjs Small Balance',
      moreInfoText:
        'Text Area = DT, Text Type = SB. Each miscellaneous adjustment parameter consists of a text identification code and text type code. Use these codes to change description text for a particular adjustment via the Text Maintenance feature. This Charge-off "Small Balance" Adjustment Text identifies the text displayed on the statement for a charged-off "small balance" using tran code 370. To specify your own text, type its text identification code over the code displayed. Use a code for text that is either scheduled or in production.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = SB.</p>
          <p>
            {` Each miscellaneous adjustment parameter consists of a text
            identification code and text type code. Use these codes to change
            description text for a particular adjustment via the Text
            Maintenance feature. This Charge-off "Small Balance" Adjustment Text
            identifies the text displayed on the statement for a charged-off
            "small balance" using tran code 370. To specify your own text, type
            its text identification code over the code displayed. Use a code for
            text that is either scheduled or in production.`}
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffCreditLifeInsuranceAdjustmentText,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsChargeOffCreditLifeInsuranceAdjustmentText,
      onlinePCF: 'Chargeoff Adjs Credit Life Ins',
      moreInfoText:
        'Text Area = DT, Text Type = CL. Each miscellaneous adjustment parameter consists of a text identification code and text type code. Use these codes to change description text for a particular adjustment via the Text Maintenance feature. This Charge-off Credit Life Insurance Adjustment Text identifies the text displayed on the statement for a charged-off credit life insurance value using tran code 375. To specify your own text, type its text identification code over the code displayed. Use a code for text that is either scheduled or in production.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = CL.</p>
          <p>
            Each miscellaneous adjustment parameter consists of a text
            identification code and text type code. Use these codes to change
            description text for a particular adjustment via the Text
            Maintenance feature. This Charge-off Credit Life Insurance
            Adjustment Text identifies the text displayed on the statement for a
            charged-off credit life insurance value using tran code 375. To
            specify your own text, type its text identification code over the
            code displayed. Use a code for text that is either scheduled or in
            production.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffCashInterestRefundText,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsChargeOffCashInterestRefundText,
      onlinePCF: 'Chargeoff Adjs Cash Adv Interest',
      moreInfoText:
        'Text Area = DT, Text Type = RE. Each miscellaneous adjustment parameter consists of a text identification code and text type code. Use these codes to change description text for a particular adjustment via the Text Maintenance feature. This Charge-off Cash Interest Refund Text identifies the text displayed on the statement for a charged-off cash interest amount using tran code 372. To specify your own text, type its text identification code over the code displayed. Use a code for text that is either scheduled or in production.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = RE.</p>
          <p>
            Each miscellaneous adjustment parameter consists of a text
            identification code and text type code. Use these codes to change
            description text for a particular adjustment via the Text
            Maintenance feature. This Charge-off Cash Interest Refund Text
            identifies the text displayed on the statement for a charged-off
            cash interest amount using tran code 372. To specify your own text,
            type its text identification code over the code displayed. Use a
            code for text that is either scheduled or in production.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffLateChargeRefundText,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsChargeOffLateChargeRefundText,
      onlinePCF: 'Chargeoff Adjs Billed Late Chgs',
      moreInfoText:
        'Text Area = DT, Text Type = RD. Each miscellaneous adjustment parameter consists of a text identification code and text type code. Use these codes to change description text for a particular adjustment via the Text Maintenance feature. This Charge-off Late Charge Refund Text identifies the text displayed on the statement for a charged-off late charge amount using tran code 373. To specify your own text, type its text identification code over the code displayed. Use a code for text that is either scheduled or in production.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = RD.</p>
          <p>
            Each miscellaneous adjustment parameter consists of a text
            identification code and text type code. Use these codes to change
            description text for a particular adjustment via the Text
            Maintenance feature. This Charge-off Late Charge Refund Text
            identifies the text displayed on the statement for a charged-off
            late charge amount using tran code 373. To specify your own text,
            type its text identification code over the code displayed. Use a
            code for text that is either scheduled or in production.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffMerchandiseInterestRefundText,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsChargeOffMerchandiseInterestRefundText,
      onlinePCF: 'Chargeoff Adjs Mdse Int and Svc Chg',
      moreInfoText:
        'Text Area = DT, Text Type = RG. Each miscellaneous adjustment parameter consists of a text identification code and text type code. Use these codes to change description text for a particular adjustment via the Text Maintenance feature. This Charge-off Merchandise Interest Refund Text identifies the text displayed on the statement for a charged-off merchandise interest amount using tran code 374. To specify your own text, type its text identification code over the code displayed. Use a code for text that is either scheduled or in production.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = RG.</p>
          <p>
            Each miscellaneous adjustment parameter consists of a text
            identification code and text type code. Use these codes to change
            description text for a particular adjustment via the Text
            Maintenance feature. This Charge-off Merchandise Interest Refund
            Text identifies the text displayed on the statement for a
            charged-off merchandise interest amount using tran code 374. To
            specify your own text, type its text identification code over the
            code displayed. Use a code for text that is either scheduled or in
            production.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffCashItemRefundText,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsChargeOffCashItemRefundText,
      onlinePCF: 'Chargeoff Adjs Billed Cash Item Chg',
      moreInfoText:
        'Text Area = DT, Text Type = RH. Each miscellaneous adjustment parameter consists of a text identification code and text type code. Use these codes to change description text for a particular adjustment via the Text Maintenance feature. This Charge-off Cash Item Refund Text identifies the text displayed on the statement for a charged-off cash item amount using tran code 380. To specify your own text, type its text identification code over the code displayed. Use a code for text that is either scheduled or in production.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = RH.</p>
          <p>
            Each miscellaneous adjustment parameter consists of a text
            identification code and text type code. Use these codes to change
            description text for a particular adjustment via the Text
            Maintenance feature. This Charge-off Cash Item Refund Text
            identifies the text displayed on the statement for a charged-off
            cash item amount using tran code 380. To specify your own text, type
            its text identification code over the code displayed. Use a code for
            text that is either scheduled or in production.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffMerchandiseItemRefundText,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsChargeOffMerchandiseItemRefundText,
      onlinePCF: 'Chargeoff Adjs Billed Mdse Item Chg',
      moreInfoText:
        'Text Area = DT, Text Type = RJ. Each miscellaneous adjustment parameter consists of a text identification code and text type code. Use these codes to change description text for a particular adjustment via the Text Maintenance feature. This Charge-off Merchandise Item Refund Text identifies the text displayed on the statement for a charged-off merchandise item amount using tran code 381. To specify your own text, type its text identification code over the code displayed. Use a code for text that is either scheduled or in production.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = RJ.</p>
          <p>
            Each miscellaneous adjustment parameter consists of a text
            identification code and text type code. Use these codes to change
            description text for a particular adjustment via the Text
            Maintenance feature. This Charge-off Merchandise Item Refund Text
            identifies the text displayed on the statement for a charged-off
            merchandise item amount using tran code 381. To specify your own
            text, type its text identification code over the code displayed. Use
            a code for text that is either scheduled or in production.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsChargeOffOverlimitFeeRefundText,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsChargeOffOverlimitFeeRefundText,
      onlinePCF: 'Chargeoff Adjs Billed Overlmt Fees',
      moreInfoText:
        'Text Area = DT, Text Type = RK. Each miscellaneous adjustment parameter consists of a text identification code and text type code. Use these codes to change description text for a particular adjustment via the Text Maintenance feature. This Charge-off Overlimit Fee Refund Text identifies the text displayed on the statement for a charged-off overlimit fee amount using tran code 381. To specify your own text, type its text identification code over the code displayed. Use a code for text that is either scheduled or in production.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = RK.</p>
          <p>
            Each miscellaneous adjustment parameter consists of a text
            identification code and text type code. Use these codes to change
            description text for a particular adjustment via the Text
            Maintenance feature. This Charge-off Overlimit Fee Refund Text
            identifies the text displayed on the statement for a charged-off
            overlimit fee amount using tran code 381. To specify your own text,
            type its text identification code over the code displayed. Use a
            code for text that is either scheduled or in production.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsSaleReversalText,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsSaleReversalText,
      onlinePCF: 'Misc Adjs Sale Reversal',
      moreInfoText:
        'Text Area = DT, Text Type = MR. Each miscellaneous adjustment parameter consists of a text identification code and text type code. Use these codes to change description text for a particular adjustment via the Text Maintenance feature. This Sale Reversal Text identifies the text displayed on the statement for a reversal of a sale using tran code 256. To specify your own text, type its text identification code over the code displayed. Use a code for text that is either scheduled or in production.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = MR.</p>
          <p>
            Each miscellaneous adjustment parameter consists of a text
            identification code and text type code. Use these codes to change
            description text for a particular adjustment via the Text
            Maintenance feature. This Sale Reversal Text identifies the text
            displayed on the statement for a reversal of a sale using tran code
            256. To specify your own text, type its text identification code
            over the code displayed. Use a code for text that is either
            scheduled or in production.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCashAdvanceReversalText,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsCashAdvanceReversalText,
      onlinePCF: 'Misc Adjs Cash Reversal',
      moreInfoText:
        'Text Area = DT, Text Type = AR. Each miscellaneous adjustment parameter consists of a text identification code and text type code. Use these codes to change description text for a particular adjustment via the Text Maintenance feature. This Cash Advance Reversal Text identifies the text displayed on the statement for a reversal of a cash advance using tran code 257. To specify your own text, type its text identification code over the code displayed. Use a code for text that is either scheduled or in production.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = AR.</p>
          <p>
            Each miscellaneous adjustment parameter consists of a text
            identification code and text type code. Use these codes to change
            description text for a particular adjustment via the Text
            Maintenance feature. This Cash Advance Reversal Text identifies the
            text displayed on the statement for a reversal of a cash advance
            using tran code 257. To specify your own text, type its text
            identification code over the code displayed. Use a code for text
            that is either scheduled or in production.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsReturnReversalText,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsReturnReversalText,
      onlinePCF: 'Misc Adjs Return Reversal',
      moreInfoText:
        'Text Area = DT, Text Type = RQ. Each miscellaneous adjustment parameter consists of a text identification code and text type code. Use these codes to change description text for a particular adjustment via the Text Maintenance feature. This Return Reversal Text identifies the text displayed on the statement for a reversal of a return using tran code 258. To specify your own text, type its text identification code over the code displayed. Use a code for text that is either scheduled or in production.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = RQ.</p>
          <p>
            Each miscellaneous adjustment parameter consists of a text
            identification code and text type code. Use these codes to change
            description text for a particular adjustment via the Text
            Maintenance feature. This Return Reversal Text identifies the text
            displayed on the statement for a reversal of a return using tran
            code 258. To specify your own text, type its text identification
            code over the code displayed. Use a code for text that is either
            scheduled or in production.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsPaymentReversalText,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsPaymentReversalText,
      onlinePCF: 'Misc Adjs Payment Reversal',
      moreInfoText:
        'Text Area = DT, Text Type = PV. Each miscellaneous adjustment parameter consists of a text identification code and text type code. Use these codes to change description text for a particular adjustment via the Text Maintenance feature. This Payment Reversal Text identifies the text displayed on the statement for a reversal of a payment using tran code 272. To specify your own text, type its text identification code over the code displayed. Use a code for text that is either scheduled or in production.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = PV.</p>
          <p>
            Each miscellaneous adjustment parameter consists of a text
            identification code and text type code. Use these codes to change
            description text for a particular adjustment via the Text
            Maintenance feature. This Payment Reversal Text identifies the text
            displayed on the statement for a reversal of a payment using tran
            code 272. To specify your own text, type its text identification
            code over the code displayed. Use a code for text that is either
            scheduled or in production.
          </p>
        </>
      )
    }
  ],
  Info_DisputesMessageText: [
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCashAdvanceFinanceChargesText,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsCashAdvanceFinanceChargesText,
      onlinePCF: 'Dispute Cash Advance',
      moreInfoText:
        'Text Area = DT, Text Type = DH. The following parameters identify description text for dispute adjustments that are displayed on the statement.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = DH.</p>
          <p>
            The following parameters identify description text for dispute
            adjustments that are displayed on the statement.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsMerchandiseItemChargeText,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsMerchandiseItemChargeText,
      onlinePCF: 'Dispute Mdse Item Charge',
      moreInfoText:
        'Text Area = DT, Text Type = DB. The following parameters identify description text for dispute adjustments that are displayed on the statement.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = DB.</p>
          <p>
            The following parameters identify description text for dispute
            adjustments that are displayed on the statement.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCashItemChargesText,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsCashItemChargesText,
      onlinePCF: 'Dispute Cash Item Charge',
      moreInfoText:
        'Text Area = DT, Text Type = DE. The following parameters identify description text for dispute adjustments that are displayed on the statement.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = DE.</p>
          <p>
            The following parameters identify description text for dispute
            adjustments that are displayed on the statement.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsMerchandiseFinanceChargesText,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsMerchandiseFinanceChargesText,
      onlinePCF: 'Dispute Merchandise',
      moreInfoText:
        'Text Area = DT, Text Type = DG. The following parameters identify description text for dispute adjustments that are displayed on the statement.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = DG.</p>
          <p>
            The following parameters identify description text for dispute
            adjustments that are displayed on the statement.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsLateChargesText,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsLateChargesText,
      onlinePCF: 'Dispute Late Charge',
      moreInfoText:
        'Text Area = DT, Text Type = DI. The following parameters identify description text for dispute adjustments that are displayed on the statement.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = DI.</p>
          <p>
            The following parameters identify description text for dispute
            adjustments that are displayed on the statement.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsOverlimitChargesText,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsOverlimitChargesText,
      onlinePCF: 'Dispute Overlimit Charge',
      moreInfoText:
        'Text Area = DT, Text Type = OD. The following parameters identify description text for dispute adjustments that are displayed on the statement.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = OD.</p>
          <p>
            The following parameters identify description text for dispute
            adjustments that are displayed on the statement.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsMinimumFinanceChargesText,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsMinimumFinanceChargesText,
      onlinePCF: 'Dispute Min Finance Chg',
      moreInfoText:
        'Text Area = DT, Text Type = DF. The following parameters identify description text for dispute adjustments that are displayed on the statement.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = DF.</p>
          <p>
            The following parameters identify description text for dispute
            adjustments that are displayed on the statement.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsActivityFeesText,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsActivityFeesText,
      onlinePCF: 'Dispute Activity Fee',
      moreInfoText:
        'Text Area = DT, Text Type = DA. The following parameters identify description text for dispute adjustments that are displayed on the statement.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = DA.</p>
          <p>
            The following parameters identify description text for dispute
            adjustments that are displayed on the statement.
          </p>
        </>
      )
    },
    {
      id: MethodFieldParameterEnum.ManageStatementProductionSettingsCreditLifeInsuranceText,
      fieldName:
        MethodFieldNameEnum.ManageStatementProductionSettingsCreditLifeInsuranceText,
      onlinePCF: 'Dispute Credit Life Ins',
      moreInfoText:
        'Text Area = DT, Text Type = DL. The following parameters identify description text for dispute adjustments that are displayed on the statement.',
      moreInfo: (
        <>
          <p className="mb-8">Text Area = DT, Text Type = DL.</p>
          <p>
            The following parameters identify description text for dispute
            adjustments that are displayed on the statement.
          </p>
        </>
      )
    }
  ]
};
