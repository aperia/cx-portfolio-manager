import { render } from '@testing-library/react';
import React from 'react';
import { parameterGroup, ParameterList } from './newMethodParameterList';
import SubGridPreviewMethod from './SubGridPreviewMethod';

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

const metadata = {
  creditBalanceHoldOptions: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  whereToSendRequestPayments: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  sameDayLostStatusOption: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  useFixedMinimumPaymentDueDate: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  useFixedMinimumPaymentDueDateTableId: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  annualInterestDisplay: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  includeLateChargesInAnnualInterestDisplay: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  includeCashItemInAnnualInterestDisplay: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  includeMerchandiseItemInAnnualInterestDisplay: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  includeOverLimitFeesInAnnualInterestDisplay: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  includesStatementProductionFeesInAnnualInterestDisplay: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  produceStatementsForInactiveDeletedAccounts: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  usInactiveAccountExternalStatusControl: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  nonUsInactiveAccountExternalStatusControl: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  pricingStrategyChangeStatementProduction: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  statusAAuthorizationProhibited: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  statusBBankrupt: [{ code: 'None', description: 'None', text: 'None' }],
  statusCClosed: [{ code: 'None', description: 'None', text: 'None' }],
  statusERevoked: [{ code: 'code', description: 'description', text: 'text' }],
  statusFFrozen: [{ code: 'code', description: 'description', text: 'text' }],
  statusIInterestProhibited: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  statusLLost: [{ code: 'code', description: 'description', text: 'text' }],
  statusUStolen: [{ code: 'code', description: 'description', text: 'text' }],
  statusBlank: [{ code: 'code', description: 'description', text: 'text' }],
  minimumAmountForInactiveDeletedAccounts: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  creditLifeInsurancePlan1: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  creditLifeInsurancePlan2: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  creditLifeInsurancePlan3: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  creditLifeInsurancePlan4: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  creditLifeInsurancePlan5: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  creditLifeInsurancePlan6: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  creditLifeInsurancePlan7: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  creditLifeInsurancePlan8: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  creditLifeInsurancePlan9: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  cashAdvanceFinanceCharges: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  cashItemCharges: [{ code: 'code', description: 'description', text: 'text' }],
  merchandiseFinanceCharges: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  merchandiseItemCharges: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  lateCharges: [{ code: 'code', description: 'description', text: 'text' }],
  overlimitCharge: [{ code: 'code', description: 'description', text: 'text' }],
  creditLifeInsuranceRefunds: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  eBillLanguageOptions: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  financeChargeStatementDisplayOptions: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  displayAirlineTransaction: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  cardHolderNamesOnStatement: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  reversalDisplayOption: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  lateChargeDateDisplay: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  overlimitChargeDateDisplay: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  foreignCurrencyDisplayOptions: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  feeRecordIndicator: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  optionalIssuerFeeStatementDisplayCode: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  optionalIssuerFeeMessageText: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  saleAmountAdjText: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  cashAdvanceAmountAdjText: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  returnAmountAdjText: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  paymentAmountAdjText: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  chargeOffPrincipalAdjText: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  chargeOffFinanceChargesAdjText: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  miscSpecificCreditAdjText: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  chargeOffSmallBalanceAdjText: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  chargeOffCreditLifeInsuranceAdjText: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  chargeOffCashInterestRefundText: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  chargeOffLateChargeRefundText: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  chargeOffMerchandiseInterestRefundText: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  chargeOffCashItemRefundText: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  chargeOffMerchandiseItemRefundText: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  chargeOffOverlimitFeeRefundText: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  saleReversalText: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  cashAdvanceReversalText: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  returnReversalText: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  paymentReversalText: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  creditBalanceHoldCode: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  printHardCopySec: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  cssStatementOverrideTableId: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  lateFeeWaiverText: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  genericMessageAbove: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  genericMessageBelow: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  normalTermsDateId: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  crossCycleFeeDisplayOptions: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  crossCycleInterestDisplayOptions: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  transactionSeriesDisplayOptions: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  prevCycCashAdv: [{ code: 'code', description: 'description', text: 'text' }],
  prevCycMerchandise: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  prevCycCashItemCharge: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  prevCycMdsItemCharge: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  minimumFinanceCharges: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  activityFee: [{ code: 'code', description: 'description', text: 'text' }],
  prevCycLateCharge: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  prevCycOverLimitCharge: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  creditLifeInsuranceCharge: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  otherCreditAdjText: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  otherDebitAdjText: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  cashAdvFinanceChargesText: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  merchandiseItemChargeText: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  cashItemChargesText: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  merchandiseFinanceChargesText: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  lateChargesText: [{ code: 'code', description: 'description', text: 'text' }],
  overlimitChargesText: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  minimumFinanceChargesText: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  activityFeesText: [
    { code: 'code', description: 'description', text: 'text' }
  ],
  creditLifeInsuranceText: [
    { code: 'code', description: 'description', text: 'text' }
  ]
};

const method = {
  'credit.balance.hold.options': 'code',
  'where.to.send.requested.statements': 'code',
  'payment.due.days': 'code',
  'same.day.lost.status.option': 'code',
  'use.fixed.minimum.payment.due.date': 'code',
  'use.fixed.minimum.payment.due.date.table.id': 'code',
  'annual.interest.display': 'code',
  'include.late.charges.in.annual.interest.display': 'code',
  'include.cash.item.in.annual.interest.display': 'code',
  'include.merchandise.item.in.annual.interest.display': 'code',
  'include.overlimit.fees.in.annual.interest.display': 'code',
  'include.statement.production.fees.in.annual.interest.display': 'code',
  'produce.statements.for.inactive.deleted.accounts': 'code',
  'us.inactive.account.external.status.control': 'code',
  'non.us.inactive.account.external.status.control': 'None',
  'pricing.strategy.change.statement.production': 'code',
  'minimum.amount.for.inactive.deleted.account.statements': 'code',
  'status.a.authorization.prohibited': 'code',
  'status.b.bankrupt': 'code',
  'status.c.closed': 'code',
  'status.e.revoked': 'code',
  'status.f.frozen': 'code',
  'status.i.interest.prohibited': 'code',
  'status.l.lost': 'code',
  'status.u.stolen': 'code',
  'status.blank': 'code'
};

const props = {
  metadata,
  method,
  original: {},
  localParameterGroup: parameterGroup
};

describe('Info_ExternalStatusPrintControl with None selected', () => {
  it('Info_ExternalStatusPrintControl', () => {
    const original: ParameterList = {
      id: 'Info_ExternalStatusPrintControl',
      section:
        'txt_manage_statement_production_settings_method_standard_parameters',
      parameterGroup:
        'txt_manage_statement_production_settings_external_status_print_control'
    };

    const props = {
      original,
      localParameterGroup: parameterGroup,
      metadata: {
        'status.a.authorization.prohibited': 'None',
        'status.b.bankrupt': 'None',
        statusCClosed: 'None',
        statusERevoked: [{ code: ' ', description: 'None', text: 'None' }],
        statusFFrozen: [{ code: ' ', description: 'None', text: 'None' }],
        statusIInterestProhibited: [
          { code: 'code', description: 'description', text: 'text' }
        ],
        statusLLost: [
          { code: 'code', description: 'description', text: 'text' }
        ],
        statusUStolen: [
          { code: 'code', description: 'description', text: 'text' }
        ],
        statusBlank: [
          { code: 'code', description: 'description', text: 'text' }
        ],
        minimumAmountForInactiveDeletedAccounts: [
          { code: 'code', description: 'description', text: 'text' }
        ]
      }
    };
    const wrapper = render(<SubGridPreviewMethod {...props} />);

    expect(
      wrapper.getByText('A - Authorization Prohibited')
    ).toBeInTheDocument();
  });
});

describe('pages > WorkflowDesignPromotions > DesignPromotionsStep > SubGridPreviewMethod', () => {
  it('Info_StatementProductionOptions', () => {
    const original: ParameterList = {
      id: 'Info_StatementProductionOptions',
      section:
        'txt_manage_statement_production_settings_method_standard_parameters',
      parameterGroup:
        'txt_manage_statement_production_settings_statement_production_options'
    };
    const newProps = { ...props, original };
    const wrapper = render(<SubGridPreviewMethod {...newProps} />);

    expect(wrapper.getByText('Credit Balance Hold Code')).toBeInTheDocument();
  });
});

describe('Info_InactiveDeletedAccount', () => {
  it('Info_InactiveDeletedAccount', () => {
    const original: ParameterList = {
      id: 'Info_InactiveDeletedAccount',
      section:
        'txt_manage_statement_production_settings_method_standard_parameters',
      parameterGroup:
        'txt_manage_statement_production_settings_inactive_deleted_account'
    };
    const newProps = { ...props, original };
    const wrapper = render(<SubGridPreviewMethod {...newProps} />);

    expect(
      wrapper.getByText('Inactive/Deleted Account Statement')
    ).toBeInTheDocument();
  });
});

describe('Info_AnnualInterestDisplay', () => {
  it('Info_AnnualInterestDisplay', () => {
    const original: ParameterList = {
      id: 'Info_AnnualInterestDisplay',
      section:
        'txt_manage_statement_production_settings_method_standard_parameters',
      parameterGroup:
        'txt_manage_statement_production_settings_annual_interest_display'
    };
    const newProps = { ...props, original };
    const wrapper = render(<SubGridPreviewMethod {...newProps} />);

    expect(wrapper.getByText('Statement Charges')).toBeInTheDocument();
  });
});

describe('Info_ExternalStatusPrintControl', () => {
  it('Info_ExternalStatusPrintControl', () => {
    const original: ParameterList = {
      id: 'Info_ExternalStatusPrintControl',
      section:
        'txt_manage_statement_production_settings_method_standard_parameters',
      parameterGroup:
        'txt_manage_statement_production_settings_external_status_print_control'
    };

    const newProps = { ...props, original };
    const wrapper = render(<SubGridPreviewMethod {...newProps} />);

    expect(
      wrapper.getByText('A - Authorization Prohibited')
    ).toBeInTheDocument();
  });
});

describe('Info_CITDisclosuresPrintOrderNumbers', () => {
  it('Info_CITDisclosuresPrintOrderNumbers', () => {
    const original: ParameterList = {
      id: 'Info_CITDisclosuresPrintOrderNumbers',
      section:
        'txt_manage_statement_production_settings_method_standard_parameters',
      parameterGroup:
        'txt_manage_statement_production_settings_cit_disclosures_print_order_numbers'
    };

    const newProps = { ...props, original };
    const wrapper = render(<SubGridPreviewMethod {...newProps} />);

    expect(wrapper.getByText('CP/IC/BP')).toBeInTheDocument();
  });
});
