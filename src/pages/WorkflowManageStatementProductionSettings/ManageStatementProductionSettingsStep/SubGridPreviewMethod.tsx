import { MethodFieldParameterEnum } from 'app/constants/enums';
import { formatCommon, matchSearchValue } from 'app/helpers';
import {
  ColumnType,
  Grid,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import { orderBy } from 'app/_libraries/_dls/lodash';
import {
  formatTruncate,
  viewMoreInfo
} from 'pages/_commons/Utils/formatGridField';
import React, { useMemo } from 'react';
import { LIST_CIT_DROP_DRAG_FORM } from './helper';
import {
  ParameterGroup,
  ParameterList,
  ParameterListIds
} from './newMethodParameterList';

interface IProps {
  original: ParameterList;
  metadata: MagicKeyValue;
  localParameterGroup: Record<ParameterListIds, ParameterGroup[]>;
}
const SubGridPreviewMethod: React.FC<IProps> = ({
  original,
  metadata,
  localParameterGroup
}) => {
  const { t } = useTranslation();

  const data = useMemo(
    () =>
      localParameterGroup[original.id]?.filter(p =>
        matchSearchValue([p.fieldName, p.moreInfoText, p.onlinePCF], undefined)
      ),
    [original.id, localParameterGroup]
  );

  const valueAccessor = (data: Record<string, any>, index: number) => {
    const paramId = data.id as MethodFieldParameterEnum;

    if (LIST_CIT_DROP_DRAG_FORM.includes(paramId)) {
      return <span>{metadata[paramId]?.padStart(2, '0')}</span>;
    }

    if (
      paramId ===
      MethodFieldParameterEnum.ManageStatementProductionSettingsMinimumAmountForInactiveDeletedAccounts
    ) {
      return (
        <TruncateText
          resizable={true}
          lines={2}
          ellipsisLessText={t('txt_less')}
          ellipsisMoreText={t('txt_more')}
        >
          {formatCommon(metadata[paramId]).currency(2)}
        </TruncateText>
      );
    }

    return (
      <TruncateText
        resizable={true}
        lines={2}
        ellipsisLessText={t('txt_less')}
        ellipsisMoreText={t('txt_more')}
      >
        {metadata[paramId] === 'None' ? '' : metadata[paramId]}
      </TruncateText>
    );
  };

  const isCITDisclosuresPrintOrderNumber =
    original.id === 'Info_CITDisclosuresPrintOrderNumbers';

  const extendColumns: ColumnType[] = [
    {
      id: 'printOrder',
      Header: t('Print Order'),
      accessor: valueAccessor,
      width: 118,
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'fieldName',
      Header: t('txt_business_name'),
      accessor: formatTruncate(['fieldName']),
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'onlinePCF',
      Header: t('txt_manage_penalty_fee_online_PCF'),
      accessor: formatTruncate(['onlinePCF']),
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      accessor: viewMoreInfo(['moreInfo'], t),
      width: 105,
      cellBodyProps: { className: 'pt-12 pb-8' }
    }
  ];

  const columns: ColumnType[] = [
    {
      id: 'fieldName',
      Header: t('txt_business_name'),
      accessor: formatTruncate(['fieldName'])
    },
    {
      id: 'onlinePCF',
      Header: t('txt_manage_penalty_fee_online_PCF'),
      accessor: formatTruncate(['onlinePCF'])
    },
    {
      id: 'value',
      Header: t('txt_value'),
      accessor: valueAccessor
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      accessor: viewMoreInfo(['moreInfo'], t),
      width: 105
    }
  ];

  const dynamicColumns = isCITDisclosuresPrintOrderNumber
    ? extendColumns
    : columns;

  const dynamicData = isCITDisclosuresPrintOrderNumber
    ? orderBy(
        data.map(el => ({
          ...el,
          orderBy: Number(metadata?.[el.id]?.padStart(1, '0'))
        })),
        ['orderBy'],
        ['asc']
      )
    : data;

  return (
    <div className="p-20">
      <Grid columns={dynamicColumns} data={dynamicData} />
    </div>
  );
};

export default SubGridPreviewMethod;
