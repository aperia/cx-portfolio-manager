import { mapGridExpandCollapse, methodParamsToObject } from 'app/helpers';
import { Grid, useTranslation } from 'app/_libraries/_dls';
import React, { useState } from 'react';
import { mappingPreviewData } from './helper';
import newMethodColumns from './newMethodColumns';
import {
  ParameterGroup,
  parameterList,
  ParameterListIds
} from './newMethodParameterList';
import { ManageStatementElementData } from './SubGridNewMethod';
import SubGridPreviewMethod from './SubGridPreviewMethod';

export interface SubGridProps {
  original: any;
  metadata: ManageStatementElementData;
  localParameterGroup: Record<ParameterListIds, ParameterGroup[]>;
}
const SubRowGrid: React.FC<SubGridProps> = ({
  original,
  metadata,
  localParameterGroup
}) => {
  const convertedObject = methodParamsToObject(original);
  const data = mappingPreviewData(metadata, convertedObject);
  const { t } = useTranslation();

  const [expandedList, setExpandedList] = useState<ParameterListIds[]>([
    'Info_StatementProductionOptions'
  ]);

  return (
    <div className="p-20 overflow-hidden">
      <Grid
        togglable
        columns={newMethodColumns(t)}
        data={parameterList}
        subRow={({ original }: MagicKeyValue) => (
          <SubGridPreviewMethod
            original={original}
            metadata={data}
            localParameterGroup={localParameterGroup}
          />
        )}
        dataItemKey="id"
        expandedItemKey="id"
        expandedList={expandedList}
        onExpand={setExpandedList as any}
        toggleButtonConfigList={parameterList.map(
          mapGridExpandCollapse('id', t)
        )}
      />
    </div>
  );
};

export default SubRowGrid;
