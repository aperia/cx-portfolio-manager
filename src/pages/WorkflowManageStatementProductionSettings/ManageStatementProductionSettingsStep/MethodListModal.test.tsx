import {
  fireEvent,
  queryByText,
  render,
  RenderResult
} from '@testing-library/react';
import { mockUseModalRegistryFnc } from 'app/utils';
import 'app/utils/_mockComponent/mockFailedApiReload';
import 'app/utils/_mockComponent/mockNoDataFound';
import 'app/utils/_mockComponent/mockPaging';
import * as WorkflowSetupManagePenaltyFee from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManagePenaltyFee';
import * as WorkflowSetupManageStatementProductionSettings from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageStatementProductionSettings';
import React from 'react';
import * as reactRedux from 'react-redux';
import MethodListModal from './MethodListModal';

const selectedMethod = {
  id: '1',
  name: 'method-1',
  versions: [{ id: 'version 1.0' }]
};

const draftMethod = {
  modeledFrom: {
    id: '1',
    name: 'name-1',
    versions: [{ id: 'version 1.0' }]
  }
};

const selectedVersionId = 'version 1.0';

jest.mock('pages/_commons/Utils/SortOrder', () => {
  return {
    __esModule: true,
    default: function SortOrder({
      dataFilter,
      onChangeOrderBy,
      onClearSearch
    }: any) {
      return (
        <div>
          <p>Mock Sort Order</p>
          <button
            className="sort-order--order-btn"
            onClick={() => onChangeOrderBy('test sort value')}
          >
            Order By
          </button>
          <button className="sort-order--clear-btn" onClick={onClearSearch}>
            Clear And Reset
          </button>
        </div>
      );
    }
  };
});

jest.mock('app/components/MethodCardView', () => ({
  __esModule: true,
  default: ({ onToggle, onSelectVersion }: any) => {
    const handleSelectVersion = () => {
      onSelectVersion(selectedMethod, selectedVersionId);
    };
    return (
      <div>
        <div>MethodCardView_component</div>
        <button onClick={handleSelectVersion}>SELECT_VERSION</button>
        <button onClick={onToggle}>ON_TOGGLE</button>
      </div>
    );
  }
}));

jest.mock('app/components/WorkflowStrategyFlyout', () => ({
  __esModule: true,
  default: ({ onClose }: any) => {
    return (
      <div>
        <div onClick={onClose}>WorkflowStrategyFlyout_component</div>
      </div>
    );
  }
}));

jest.mock('./AddNewMethodModal', () => ({
  __esModule: true,
  default: ({ onClose }: any) => {
    const handleOnClose = () => {
      onClose(draftMethod, false);
    };

    const handleOnCloseWithBack = () => {
      onClose(draftMethod, false, true);
    };

    const handleOnBack = () => {
      onClose(draftMethod, true);
    };

    const handleOnCloseWithoutSelected = () => {
      onClose();
    };
    return (
      <div>
        <div>AddNewMethodModal_component</div>
        <button onClick={handleOnCloseWithBack}>
          CLOSE_AddNewMethodModalWithBack
        </button>
        <button onClick={handleOnClose}>CLOSE_AddNewMethodModal</button>
        <button onClick={handleOnBack}>BACK_AddNewMethodModal</button>
        <button onClick={handleOnCloseWithoutSelected}>
          CLOSEWITHOUTSELECTED_AddNewMethodModal
        </button>
      </div>
    );
  }
}));

const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');
const mockUseSelector = jest.spyOn(reactRedux, 'useSelector');

const mockUseModalRegistry = mockUseModalRegistryFnc();
const MockSelectStrategyFlyoutSelector = jest.spyOn(
  WorkflowSetupManagePenaltyFee,
  'useSelectStrategyFlyoutSelector'
);
const MockSelectWorkflowMethodsFilterSelector = jest.spyOn(
  WorkflowSetupManagePenaltyFee,
  'useSelectWorkflowMethodsFilterSelector'
);

const MockSelectWorkflowMethodsSelector = jest.spyOn(
  WorkflowSetupManageStatementProductionSettings,
  'useSelectWorkflowMethodsMSPSSelector'
);

const renderModal = (props: any): RenderResult => {
  return render(
    <div>
      <MethodListModal {...props} />
    </div>
  );
};

describe('pages > WorkflowManageStatementProductionSettings > ManageStatementProductionSettingsStep > MethodListModal', () => {
  const mockDispatch = jest.fn();

  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
    mockUseSelector.mockImplementation(selector =>
      selector({
        caches: {}
      })
    );
    mockUseModalRegistry.mockImplementation(() => [true, false]);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Should render modal with body has loading', () => {
    MockSelectWorkflowMethodsSelector.mockReturnValue({
      methods: [] as any,
      loading: true,
      error: false,
      total: 0,
      totalMethods: []
    });

    MockSelectWorkflowMethodsFilterSelector.mockReturnValue({
      searchValue: '',
      page: 0,
      pageSize: 10
    });

    const props = {
      id: 'modal_id',
      defaultMethod: undefined,
      onClose: jest.fn()
    };

    const wrapper = renderModal(props);
    const body = wrapper.baseElement.querySelector('div.loading');
    expect(body).not.toEqual(null);
  });

  it('Should render modal with api call failed', () => {
    MockSelectWorkflowMethodsSelector.mockReturnValue({
      methods: [] as any,
      totalMethods: [],
      loading: false,
      error: true,
      total: 0
    });

    MockSelectWorkflowMethodsFilterSelector.mockReturnValue({
      searchValue: '',
      page: 0,
      pageSize: 10
    });

    const props = {
      id: 'modal_id',
      defaultMethod: undefined,
      onClose: jest.fn()
    };

    const wrapper = renderModal(props);
    expect(wrapper.getByText('Data load unsuccessful')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('Reload'));
    expect(mockDispatch).toHaveBeenCalled();
  });

  it('Should render modal with No data found', () => {
    MockSelectWorkflowMethodsSelector.mockReturnValue({
      methods: [] as any,
      totalMethods: [],
      loading: false,
      error: false,
      total: 0
    });

    MockSelectWorkflowMethodsFilterSelector.mockReturnValue({
      searchValue: '123',
      page: 1,
      pageSize: 10
    });

    const props = {
      id: 'modal_id',
      defaultMethod: undefined,
      onClose: jest.fn()
    };

    const wrapper = renderModal(props);
    const noDataFound = wrapper.getByText('No data found');
    expect(noDataFound).toBeInTheDocument();
    fireEvent.click(noDataFound);
    expect(mockDispatch).toHaveBeenCalledWith({
      payload: undefined,
      type: 'workflowSetup/clearAndResetWorkflowMethodFilter'
    });
  });

  it('Should render without defaultMethod', () => {
    MockSelectWorkflowMethodsSelector.mockReturnValue({
      methods: [
        {
          id: '1',
          name: 'method-1',
          versions: [{ id: 'version 1.0' }]
        },
        {
          id: '11',
          name: 'method-11',
          versions: [{ id: 'version 1.0' }]
        }
      ] as any,
      totalMethods: [
        {
          id: '1',
          name: 'method-1',
          versions: [{ id: 'version 1.0' }]
        },
        {
          id: '11',
          name: 'method-11',
          versions: [{ id: 'version 1.0' }]
        }
      ] as any,
      loading: false,
      error: false,
      total: 2
    });

    MockSelectWorkflowMethodsFilterSelector.mockReturnValue({
      searchValue: '',
      page: 1,
      pageSize: 10
    });

    MockSelectStrategyFlyoutSelector.mockReturnValue({
      id: 'Flyout_id'
    } as any);

    const props = {
      id: 'modal_id',
      onClose: jest.fn()
    };

    const wrapper = renderModal(props);
    expect(wrapper.getAllByText('MethodCardView_component')).toHaveLength(2);

    fireEvent.click(wrapper.getAllByText('SELECT_VERSION')[0]);
    fireEvent.click(wrapper.getByText('txt_continue'));
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('CLOSE_AddNewMethodModal'));

    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();

    fireEvent.click(wrapper.getAllByText('SELECT_VERSION')[0]);
    fireEvent.click(wrapper.getByText('txt_continue'));
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('CLOSE_AddNewMethodModal'));

    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();

    fireEvent.click(wrapper.getAllByText('SELECT_VERSION')[0]);
    fireEvent.click(wrapper.getByText('txt_continue'));
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('BACK_AddNewMethodModal'));

    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();

    fireEvent.click(wrapper.getAllByText('SELECT_VERSION')[0]);
    fireEvent.click(wrapper.getByText('txt_continue'));
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('BACK_AddNewMethodModal'));

    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render modal with data', () => {
    MockSelectWorkflowMethodsSelector.mockReturnValue({
      methods: [
        {
          id: '1',
          name: 'method-1',
          versions: [
            {
              id: 'version 1.0'
            }
          ]
        },
        {
          id: '11',
          name: 'method-11',
          versions: [
            {
              id: 'version 1.0'
            }
          ]
        }
      ] as any,
      totalMethods: [
        {
          id: '1',
          name: 'method-1',
          versions: [{ id: 'version 1.0' }]
        },
        {
          id: '11',
          name: 'method-11',
          versions: [{ id: 'version 1.0' }]
        }
      ] as any,
      loading: false,
      error: false,
      total: 2
    });

    MockSelectWorkflowMethodsFilterSelector.mockReturnValue({
      searchValue: '',
      page: 1,
      pageSize: 10
    });

    MockSelectStrategyFlyoutSelector.mockReturnValue({
      id: 'Flyout_id'
    } as any);

    const props = {
      id: 'modal_id',
      defaultMethod: {
        modeledFrom: {
          id: '1',
          name: 'name-1',
          versions: [{ id: 'version 1.0' }]
        }
      },
      isCreateNewVersion: true,
      onClose: jest.fn()
    };

    const wrapper = renderModal(props);
    expect(wrapper.getAllByText('MethodCardView_component')).toHaveLength(2);

    fireEvent.click(
      wrapper.baseElement.querySelector(
        'button[class="sort-order--order-btn"]'
      ) as Element
    );

    fireEvent.click(
      wrapper.baseElement.querySelector(
        'i[class="icon icon-search"]'
      ) as Element
    );
    expect(mockDispatch).toHaveBeenCalledWith({
      payload: { page: 1, searchValue: '' },
      type: 'workflowSetup/updateWorkflowMethodFilter'
    });

    fireEvent.click(
      wrapper.baseElement.querySelector(
        'button[class="change-page-number"]'
      ) as Element
    );
    expect(mockDispatch).toHaveBeenCalledWith({
      payload: { page: 2 },
      type: 'workflowSetup/updateWorkflowMethodFilter'
    });

    fireEvent.click(
      wrapper.baseElement.querySelector(
        'button[class="change-page-size"]'
      ) as Element
    );
    expect(mockDispatch).toHaveBeenCalledWith({
      payload: { pageSize: 25 },
      type: 'workflowSetup/updateWorkflowMethodFilter'
    });

    fireEvent.click(wrapper.getAllByText('ON_TOGGLE')[0]);
    expect(mockDispatch).toHaveBeenCalledWith(
      expect.objectContaining({ type: 'workflowSetup/updateExpandingMethod' })
    );

    fireEvent.click(wrapper.getAllByText('SELECT_VERSION')[0]);
    fireEvent.click(wrapper.getByText('txt_continue'));
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('CLOSE_AddNewMethodModalWithBack'));

    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();

    fireEvent.click(wrapper.getAllByText('SELECT_VERSION')[0]);
    fireEvent.click(wrapper.getByText('txt_continue'));
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('CLOSE_AddNewMethodModal'));

    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();

    fireEvent.click(wrapper.getAllByText('SELECT_VERSION')[0]);
    fireEvent.click(wrapper.getByText('txt_continue'));
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('BACK_AddNewMethodModal'));

    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();

    fireEvent.click(wrapper.getAllByText('SELECT_VERSION')[0]);
    fireEvent.click(wrapper.getByText('txt_continue'));
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('BACK_AddNewMethodModal'));

    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();

    fireEvent.click(wrapper.getAllByText('SELECT_VERSION')[0]);
    fireEvent.click(wrapper.getByText('txt_continue'));
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('CLOSEWITHOUTSELECTED_AddNewMethodModal')
    );

    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();

    fireEvent.click(wrapper.getByText('WorkflowStrategyFlyout_component'));
    expect(mockDispatch).toHaveBeenCalledWith({
      payload: null,
      type: 'workflowSetup/setStrategyFlyout'
    });
  });
});
