import {
  fireEvent,
  queryByText,
  render,
  RenderResult
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { ServiceSubjectSection } from 'app/constants/enums';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManageStatementProductionSettings';
import React from 'react';
import * as reactRedux from 'react-redux';
import ConfigureParameters from './ConfigureParameters';

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');
const MockSelectElementMetadataForMSPS = jest.spyOn(
  WorkflowSetup,
  'useSelectElementMetadataForMSPS'
);

const mockOnCloseWithoutMethod = jest.fn();
jest.mock('./MethodListModal', () => ({
  __esModule: true,
  default: ({ onClose }: any) => {
    const handleOnClose = () => {
      if (mockOnCloseWithoutMethod()) {
        onClose();
        return;
      }

      onClose({
        id: '1',
        name: 'method-1',
        versions: [
          {
            id: 'version 1.0'
          }
        ]
      });
    };

    return <div onClick={handleOnClose}>MethodListModal_component</div>;
  }
}));

jest.mock('./AddNewMethodModal', () => ({
  __esModule: true,
  default: ({ onClose }: any) => {
    const handleOnClose = () => {
      if (mockOnCloseWithoutMethod()) {
        onClose(undefined, [], false);
        return;
      }

      onClose(
        {
          id: '1',
          name: 'method-1',
          versions: [
            {
              id: 'version 1.0'
            }
          ]
        },
        [],
        true
      );
    };
    return <div onClick={handleOnClose}>AddNewMethodModal_component</div>;
  }
}));

jest.mock(
  'pages/WorkflowManagePenaltyFee/LateChargesStep/RemoveMethodModal',
  () => ({
    __esModule: true,
    default: ({ onClose }: any) => {
      const handleOnClose = () => {
        if (mockOnCloseWithoutMethod()) {
          onClose();
          return;
        }

        onClose({
          id: '1',
          name: 'method-1',
          versions: [
            {
              id: 'version 1.0'
            }
          ]
        });
      };
      return <div onClick={handleOnClose}>RemoveMethodModal_component</div>;
    }
  })
);

jest.mock('./SubRowGrid', () => ({
  __esModule: true,
  default: () => <div>SubRowGrid_component</div>
}));

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <ConfigureParameters {...props} />
    </div>
  );
};
const t = (value: string) => value;
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { ...actualModule, useTranslation: () => ({ t }) };
});

describe('pages > WorkflowManageStatementProductionSettings > ManageStatementProductionSettingsStep > ConfigureParameters', () => {
  const mockDispatch = jest.fn();
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
    MockSelectElementMetadataForMSPS.mockReturnValue({
      creditBalanceHoldOptions: [{ code: 'code', text: 'text' }],
      whereToSendRequestPayments: [{ code: 'code', text: 'text' }],
      sameDayLostStatusOption: [{ code: 'code', text: 'text' }],
      useFixedMinimumPaymentDueDate: [{ code: 'code', text: 'text' }],
      useFixedMinimumPaymentDueDateTableId: [{ code: 'code', text: 'text' }],
      annualInterestDisplay: [{ code: 'code', text: 'text' }],
      includeLateChargesInAnnualInterestDisplay: [
        { code: 'code', text: 'text' }
      ],
      includeCashItemInAnnualInterestDisplay: [{ code: 'code', text: 'text' }],
      includeMerchandiseItemInAnnualInterestDisplay: [
        { code: 'code', text: 'text' }
      ],
      includeOverLimitFeesInAnnualInterestDisplay: [
        { code: 'code', text: 'text' }
      ],
      includesStatementProductionFeesInAnnualInterestDisplay: [
        { code: 'code', text: 'text' }
      ],
      produceStatementsForInactiveDeletedAccounts: [
        { code: 'code', text: 'text' }
      ],
      usInactiveAccountExternalStatusControl: [{ code: 'code', text: 'text' }],
      nonUsInactiveAccountExternalStatusControl: [
        { code: 'code', text: 'text' }
      ],
      pricingStrategyChangeStatementProduction: [
        { code: 'code', text: 'text' }
      ],
      statusAAuthorizationProhibited: [{ code: 'code', text: 'text' }],
      statusBBankrupt: [{ code: 'code', text: 'text' }],
      statusCClosed: [{ code: 'code', text: 'text' }],
      statusERevoked: [{ code: 'code', text: 'text' }],
      statusFFrozen: [{ code: 'code', text: 'text' }],
      statusIInterestProhibited: [{ code: 'code', text: 'text' }],
      statusLLost: [{ code: 'code', text: 'text' }],
      statusUStolen: [{ code: 'code', text: 'text' }],
      statusBlank: [{ code: 'code', text: 'text' }],
      creditLifeInsurancePlan1: [{ code: 'code', text: 'text' }],
      creditLifeInsurancePlan2: [{ code: 'code', text: 'text' }],
      creditLifeInsurancePlan3: [{ code: 'code', text: 'text' }],
      creditLifeInsurancePlan4: [{ code: 'code', text: 'text' }],
      creditLifeInsurancePlan5: [{ code: 'code', text: 'text' }],
      creditLifeInsurancePlan6: [{ code: 'code', text: 'text' }],
      creditLifeInsurancePlan7: [{ code: 'code', text: 'text' }],
      creditLifeInsurancePlan8: [{ code: 'code', text: 'text' }],
      creditLifeInsurancePlan9: [{ code: 'code', text: 'text' }],
      cashAdvanceFinanceCharges: [{ code: 'code', text: 'text' }],
      cashItemCharges: [{ code: 'code', text: 'text' }],
      merchandiseFinanceCharges: [{ code: 'code', text: 'text' }],
      merchandiseItemCharges: [{ code: 'code', text: 'text' }],
      lateCharges: [{ code: 'code', text: 'text' }],
      overlimitCharge: [{ code: 'code', text: 'text' }],
      creditLifeInsuranceRefunds: [{ code: 'code', text: 'text' }],
      eBillLanguageOptions: [{ code: 'code', text: 'text' }],
      financeChargeStatementDisplayOptions: [{ code: 'code', text: 'text' }],
      displayAirlineTransaction: [{ code: 'code', text: 'text' }],
      cardHolderNamesOnStatement: [{ code: 'code', text: 'text' }],
      reversalDisplayOption: [{ code: 'code', text: 'text' }],
      lateChargeDateDisplay: [{ code: 'code', text: 'text' }],
      overlimitChargeDateDisplay: [{ code: 'code', text: 'text' }],
      foreignCurrencyDisplayOptions: [{ code: 'code', text: 'text' }],
      feeRecordIndicator: [{ code: 'code', text: 'text' }],
      optionalIssuerFeeStatementDisplayCode: [{ code: 'code', text: 'text' }],
      optionalIssuerFeeMessageText: [{ code: 'code', text: 'text' }],
      saleAmountAdjText: [{ code: 'code', text: 'text' }],
      cashAdvanceAmountAdjText: [{ code: 'code', text: 'text' }],
      returnAmountAdjText: [{ code: 'code', text: 'text' }],
      paymentAmountAdjText: [{ code: 'code', text: 'text' }],
      chargeOffPrincipalAdjText: [{ code: 'code', text: 'text' }],
      chargeOffFinanceChargesAdjText: [{ code: 'code', text: 'text' }],
      miscSpecificCreditAdjText: [{ code: 'code', text: 'text' }],
      chargeOffSmallBalanceAdjText: [{ code: 'code', text: 'text' }],
      chargeOffCreditLifeInsuranceAdjText: [{ code: 'code', text: 'text' }],
      chargeOffCashInterestRefundText: [{ code: 'code', text: 'text' }],
      chargeOffLateChargeRefundText: [{ code: 'code', text: 'text' }],
      chargeOffMerchandiseInterestRefundText: [{ code: 'code', text: 'text' }],
      chargeOffCashItemRefundText: [{ code: 'code', text: 'text' }],
      chargeOffMerchandiseItemRefundText: [{ code: 'code', text: 'text' }],
      chargeOffOverlimitFeeRefundText: [{ code: 'code', text: 'text' }],
      saleReversalText: [{ code: 'code', text: 'text' }],
      cashAdvanceReversalText: [{ code: 'code', text: 'text' }],
      returnReversalText: [{ code: 'code', text: 'text' }],
      paymentReversalText: [{ code: 'code', text: 'text' }],
      cpIcBp: [{ code: 'code', text: 'text' }],
      cpIcId: [{ code: 'code', text: 'text' }],
      cpIcIi: [{ code: 'code', text: 'text' }],
      cpIcIm: [{ code: 'code', text: 'text' }],
      cpIcIp: [{ code: 'code', text: 'text' }],
      cpIcIr: [{ code: 'code', text: 'text' }],
      cpIcMf: [{ code: 'code', text: 'text' }],
      cpIcVi: [{ code: 'code', text: 'text' }],
      cpIoAc: [{ code: 'code', text: 'text' }],
      cpAoCi: [{ code: 'code', text: 'text' }],
      cpIoMc: [{ code: 'code', text: 'text' }],
      cpIoMi: [{ code: 'code', text: 'text' }],
      cpPfLc: [{ code: 'code', text: 'text' }],
      cpPfOc: [{ code: 'code', text: 'text' }],
      cpPfRc: [{ code: 'code', text: 'text' }],
      cpPoMp: [{ code: 'code', text: 'text' }],
      cpIcMe: [{ code: 'code', text: 'text' }],
      creditBalanceHoldCode: [{ code: 'code', text: 'text' }],
      printHardCopySec: [{ code: 'code', text: 'text' }],
      cssStatementOverrideTableId: [{ code: 'code', text: 'text' }],
      lateFeeWaiverText: [{ code: 'code', text: 'text' }],
      genericMessageAbove: [{ code: 'code', text: 'text' }],
      genericMessageBelow: [{ code: 'code', text: 'text' }],
      normalTermsDateId: [{ code: 'code', text: 'text' }],
      crossCycleFeeDisplayOptions: [{ code: 'code', text: 'text' }],
      crossCycleInterestDisplayOptions: [{ code: 'code', text: 'text' }],
      transactionSeriesDisplayOptions: [{ code: 'code', text: 'text' }],
      prevCycCashAdv: [{ code: 'code', text: 'text' }],
      prevCycMerchandise: [{ code: 'code', text: 'text' }],
      prevCycCashItemCharge: [{ code: 'code', text: 'text' }],
      prevCycMdsItemCharge: [{ code: 'code', text: 'text' }],
      minimumFinanceCharges: [{ code: 'code', text: 'text' }],
      activityFee: [{ code: 'code', text: 'text' }],
      prevCycLateCharge: [{ code: 'code', text: 'text' }],
      prevCycOverLimitCharge: [{ code: 'code', text: 'text' }],
      creditLifeInsuranceCharge: [{ code: 'code', text: 'text' }],
      otherCreditAdjText: [{ code: 'code', text: 'text' }],
      otherDebitAdjText: [{ code: 'code', text: 'text' }],
      cashAdvFinanceChargesText: [{ code: 'code', text: 'text' }],
      merchandiseItemChargeText: [{ code: 'code', text: 'text' }],
      cashItemChargesText: [{ code: 'code', text: 'text' }],
      merchandiseFinanceChargesText: [{ code: 'code', text: 'text' }],
      lateChargesText: [{ code: 'code', text: 'text' }],
      overlimitChargesText: [{ code: 'code', text: 'text' }],
      minimumFinanceChargesText: [{ code: 'code', text: 'text' }],
      activityFeesText: [{ code: 'code', text: 'text' }],
      creditLifeInsuranceText: [{ code: 'code', text: 'text' }]
    });

    mockOnCloseWithoutMethod.mockReturnValue(false);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Should render with no method > Create new version', () => {
    const props = {
      stepId: '1',
      selectedStep: 'selectedStep',
      formValues: {},
      saveAt: 0,
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_select_an_option')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    );
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('MethodListModal_component'));
    expect(
      queryByText(wrapper.container, /MethodListModal_component/i)
    ).not.toBeInTheDocument();

    mockOnCloseWithoutMethod.mockReturnValue(true);
    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    );
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('MethodListModal_component'));
    expect(
      queryByText(wrapper.container, /MethodListModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with no method > Choose Method to Model', () => {
    const props = {
      stepId: '1',
      selectedStep: 'selectedStep',
      formValues: {},
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_select_an_option')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_choose_method_to_model')
    );
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('MethodListModal_component'));
    expect(
      queryByText(wrapper.container, /MethodListModal_component/i)
    ).not.toBeInTheDocument();

    mockOnCloseWithoutMethod.mockReturnValue(true);
    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_choose_method_to_model')
    );

    fireEvent.click(wrapper.getByText('MethodListModal_component'));
    expect(
      queryByText(wrapper.container, /MethodListModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with no method > Add New Method', () => {
    const props = {
      stepId: '1',
      selectedStep: 'selectedStep',
      formValues: {},
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_select_an_option')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_method')
    );
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();

    mockOnCloseWithoutMethod.mockReturnValue(true);
    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_method')
    );
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with MSPS Method > Edit method', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    expect(body?.children.length).toEqual(3);

    //SHOW Add new method modal
    const editButton = body?.children[1].querySelector(
      'button[class="btn btn-outline-primary btn-sm"]'
    ) as Element;
    fireEvent.click(editButton);
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
    //End

    mockOnCloseWithoutMethod.mockReturnValue(true);
    fireEvent.click(editButton);
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with MSPS Method > Add new method', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );

    //SHOW AddNewMethodModal
    const editButton = body?.children[0].querySelector(
      'button[class="btn btn-outline-primary btn-sm"]'
    ) as Element;
    fireEvent.click(editButton);
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
    //End

    mockOnCloseWithoutMethod.mockReturnValue(true);
    fireEvent.click(editButton);
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with MSPS Method > Remove method', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );

    //SHOW RemoveMethodModal
    const removeButton = body?.children[0].querySelector(
      'button[class="btn btn-outline-danger btn-sm ml-8"]'
    ) as Element;

    fireEvent.click(removeButton);
    expect(
      wrapper.getByText('RemoveMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('RemoveMethodModal_component'));
    expect(
      queryByText(wrapper.container, /RemoveMethodModal_component/i)
    ).not.toBeInTheDocument();
    //End

    mockOnCloseWithoutMethod.mockReturnValue(true);
    fireEvent.click(removeButton);
    expect(
      wrapper.getByText('RemoveMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('RemoveMethodModal_component'));
    expect(
      queryByText(wrapper.container, /RemoveMethodModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with MSPS Method > click Add New Method on dropdown button > show paging', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'NEWVERSION',
            rowId: 3
          },
          {
            id: '4',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '5',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '6',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'NEWVERSION',
            rowId: 3
          },
          {
            id: '7',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '8',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '9',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'NEWVERSION',
            rowId: 3
          },
          {
            id: '10',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '11',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '12',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const dropdownButton = wrapper.container.querySelector(
      'button.dls-dropdown-button'
    ) as Element;
    userEvent.click(dropdownButton);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_create_new_method')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_method')
    );
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with MSPS Method > click Add New Method on dropdown button', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const dropdownButton = wrapper.container.querySelector(
      'button.dls-dropdown-button'
    ) as Element;
    userEvent.click(dropdownButton);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_create_new_method')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_method')
    );
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with MSPS Method > click Create New Version on dropdown button', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const dropdownButton = wrapper.container.querySelector(
      'button.dls-dropdown-button'
    ) as Element;
    userEvent.click(dropdownButton);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    );
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('MethodListModal_component'));
    expect(
      queryByText(wrapper.container, /MethodListModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with MSPS Method > click Choose Method to Model on dropdown button', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const dropdownButton = wrapper.container.querySelector(
      'button.dls-dropdown-button'
    ) as Element;
    userEvent.click(dropdownButton);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_choose_method_to_model')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_choose_method_to_model')
    );
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('MethodListModal_component'));
    expect(
      queryByText(wrapper.container, /MethodListModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with MSPS Method > Expand method', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    const firstChild = body?.children[0].querySelector(
      'button[class="btn btn-icon-secondary btn-sm"]'
    );
    fireEvent.click(firstChild as Element);
    expect(
      queryByText(wrapper.container, /SubRowGrid_component/i)
    ).toBeInTheDocument();
    fireEvent.click(firstChild as Element);
    expect(
      queryByText(wrapper.container, /SubRowGrid_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with MSPS Method > Edit method to list with method', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    expect(body?.children.length).toEqual(3);

    //SHOW Add new method modal
    const editButton = body?.children[1].querySelector(
      'button[class="btn btn-outline-primary btn-sm"]'
    ) as Element;

    //WITHOUT METHOD
    fireEvent.click(editButton);
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
    //End
    mockOnCloseWithoutMethod.mockReturnValue(true);
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('MethodListModal_component'));
  });

  it('Should render with MSPS Method > Edit method to list without method', () => {
    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.MSPS,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    expect(body?.children.length).toEqual(3);

    //SHOW Add new method modal
    const editButton = body?.children[1].querySelector(
      'button[class="btn btn-outline-primary btn-sm"]'
    ) as Element;

    //WITH METHOD
    fireEvent.click(editButton);
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
    //End
    mockOnCloseWithoutMethod.mockReturnValue(false);
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('MethodListModal_component'));
  });
});
