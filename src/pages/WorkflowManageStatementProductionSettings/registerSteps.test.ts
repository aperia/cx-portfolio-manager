import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { default as stepRegistry } from '../_commons/WorkflowSetup/registries/stepRegistry';

const registerFunc = jest.spyOn(stepRegistry, 'registerStep');

describe('ManageStatementProductionSettings > registerSteps', () => {
  it('Should register components', async () => {
    await import('./registerSteps');

    expect(registerFunc).toBeCalledWith(
      'GetStartedManageStatementProductionSettings',
      expect.anything()
    );

    expect(registerFunc).toBeCalledWith(
      'ManageStatementProductionSettings',
      expect.anything(),
      [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_STATEMENT_PRODUCTION_SETTINGS
      ]
    );
  });
});
