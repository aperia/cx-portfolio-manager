import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { stepRegistry } from '../_commons/WorkflowSetup/registries';
import GettingStartStep from './GettingStartStep';
import ManageStatementProductionSettingsStep from './ManageStatementProductionSettingsStep';

stepRegistry.registerStep(
  'GetStartedManageStatementProductionSettings',
  GettingStartStep
);

stepRegistry.registerStep(
  'ManageStatementProductionSettings',
  ManageStatementProductionSettingsStep,
  [UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_STATEMENT_PRODUCTION_SETTINGS]
);
