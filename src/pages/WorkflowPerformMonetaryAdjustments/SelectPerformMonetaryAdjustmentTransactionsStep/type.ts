export enum ParameterCode {
  TransactionId = 'transaction.id',

  Transaction256 = '256.transaction',
  Transaction272 = '272.transaction',
  Transaction280 = '280.transaction',
  Transaction281 = '281.transaction',
  Transaction282 = '282.transaction',
  Transaction283 = '283.transaction',
  Transaction284 = '284.transaction'
}
