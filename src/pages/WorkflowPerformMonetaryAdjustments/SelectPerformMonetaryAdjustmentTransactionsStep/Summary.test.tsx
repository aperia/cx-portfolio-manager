import { fireEvent } from '@testing-library/react';
import { WorkflowMetadataOverride } from 'app/fixtures/workflow-metadata';
import { renderWithMockStore } from 'app/utils';
import React from 'react';
import { SEARCH_FILED } from './Grid/helper';
import Summary from './Summary';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const getInitialState = ({
  elements = WorkflowMetadataOverride
}: any = {}) => ({
  workflowSetup: { elementMetadata: { elements } }
});

describe('WorkflowPerformMonetaryAdjustmentTransactions > SelectPerformMonetaryAdjustmentTransactionsStep > Summary', () => {
  it('Should render summary contains Completed', async () => {
    const mockFn = jest.fn();
    const props = {
      onEditStep: mockFn,
      formValues: {
        transactionsData: [
          {
            id: 1,
            [SEARCH_FILED]: '1'
          },
          {
            id: 1,
            [SEARCH_FILED]: '1'
          }
        ]
      }
    } as any;
    const wrapper = await renderWithMockStore(<Summary {...props} />, {
      initialState: getInitialState()
    });

    expect(wrapper.getByText('txt_edit')).toBeInTheDocument;
    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(mockFn).toHaveBeenCalled();
  });

  it('Should render summary contains Completed without transaction data', async () => {
    const mockFn = jest.fn();
    const props = {
      onEditStep: mockFn,
      formValues: {
        transactionsData: undefined
      }
    } as any;
    const wrapper = await renderWithMockStore(<Summary {...props} />, {
      initialState: getInitialState()
    });

    expect(wrapper.getByText('txt_edit')).toBeInTheDocument;
    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(mockFn).toHaveBeenCalled();
  });
});
