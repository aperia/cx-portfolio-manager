import { render, RenderResult } from '@testing-library/react';
import React from 'react';
import ConfigureParametersStep from '.';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

jest.mock('./Grid', () => {
  return {
    __esModule: true,
    default: () => <div>EditForm</div>
  };
});

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <ConfigureParametersStep {...props} />
    </div>
  );
};

describe('WorkflowPerformMonetaryAdjustmentTransactions > SelectPerformMonetaryAdjustmentTransactionsStep', () => {
  it('Should render ConfigureParametersStep contains Completed', () => {
    const { getByText } = renderComponent({ formValues: { checked: 'r1' } });

    expect(getByText('txt_select_one_or_more_transactions')).toBeInTheDocument;
  });
});
