import parseFormValues from './parseFormValues';

describe('WorkflowPerformMonetaryAdjustmentTransactions > SelectPerformMonetaryAdjustmentTransactionsStep > parseFormValues', () => {
  const data = {
    transactions: ['1', '2']
  };
  const input: any = {
    data: {
      configurations: data,
      methodList: [],
      workflowSetupData: [
        {
          id: 'id',
          status: 'DONE'
        }
      ]
    }
  };

  const input1: any = {
    data: {
      methodList: [],
      workflowSetupData: [
        {
          id: 'id',
          status: 'DONE'
        }
      ]
    }
  };

  it('selected step', () => {
    const id = 'id';
    const result = parseFormValues(input, id);

    expect(result.transactionsProp).toEqual(data.transactions);
  });

  it('select other step', () => {
    const id = 'id1';
    const result = parseFormValues(input1, id);

    expect(result).toBeUndefined();
  });
});
