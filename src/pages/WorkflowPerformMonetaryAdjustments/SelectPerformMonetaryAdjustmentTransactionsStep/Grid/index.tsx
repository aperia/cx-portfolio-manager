import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import { PAGE_SIZE } from 'app/constants/constants';
import {
  confirmEditWhenChangeEffectToUploadProps,
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { mapGridExpandCollapse } from 'app/helpers';
import { compare2Array } from 'app/helpers/array';
import { useCheckAllPagination, useUnsavedChangeRegistry } from 'app/hooks';
import { usePagination } from 'app/hooks/usePagination';
import {
  Button,
  ColumnType,
  Grid,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import { STATUS } from 'app/_libraries/_dls/components/Upload/helper';
import { isEmpty, uniq } from 'lodash';
import {
  actionsWorkflowSetup,
  useSelectElementsForPerformMonetaryAdjustment
} from 'pages/_commons/redux/WorkflowSetup';
import Paging from 'pages/_commons/Utils/Paging';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';
import { SelectPerformMonetaryAdjustmentTransactionsProps } from '..';
import { ParameterCode } from '../type';
import { SubGrid } from './SubGrid';

const GridStep: React.FC<SelectPerformMonetaryAdjustmentTransactionsProps> =
  props => {
    const {
      selectedStep,
      stepId,
      formValues,
      setFormValues,
      dependencies,
      savedAt,
      clearFormValues,
      clearFormName
    } = props;

    const setFormValuesRef = useRef(setFormValues);
    setFormValuesRef.current = setFormValues;

    const dispatch = useDispatch();
    const { t } = useTranslation();
    const { transactions } = useSelectElementsForPerformMonetaryAdjustment();

    const [expandedList, setExpandedList] = useState<string[]>([]);
    const [initialValues, setInitialValues] = useState(
      formValues?.transactions
    );

    const columns = useMemo<ColumnType[]>(
      () => [
        {
          id: 'text',
          Header: t('txt_perform_adjustment_transaction'),
          accessor: 'text',
          isSort: true
        }
      ],
      [t]
    );

    const {
      sort,
      total,
      currentPage,
      currentPageSize,
      searchValue,
      gridData: dataView,
      allDataFilter,
      dataChecked,
      isCheckAll,
      dataCheckedView,
      onCheckInPage,
      onSetDataChecked,
      onSortChange,
      onSetIsCheckAll,
      onPageChange,
      onPageSizeChange,
      onSearchChange,
      onResetToDefaultFilter
    } = usePagination(transactions, ['text'], 'text');

    useEffect(() => {
      if (selectedStep === stepId) {
        onResetToDefaultFilter();
        setExpandedList([]);
      }
    }, [selectedStep, stepId, onResetToDefaultFilter]);

    const renderSubRow = (row: any) => <SubGrid row={row} />;

    const handleCheck = (dataKeyList: string[]) => {
      onCheckInPage(dataKeyList);
      setFormValuesRef.current({
        transactions: dataKeyList,
        transactionsData: transactions.filter(item =>
          dataKeyList.includes(item.code)
        )
      });
    };

    const handleClearAndReset = useCallback(() => {
      onPageSizeChange(PAGE_SIZE[0]);
      onPageChange(1);
      onSearchChange('');
      setExpandedList([]);
    }, [onPageChange, onPageSizeChange, onSearchChange]);

    const handleExpand = (dataKeyList: string[]) => {
      setExpandedList(dataKeyList);
    };

    const handleFetchNoData = useCallback(() => {
      handleClearAndReset();
    }, [handleClearAndReset]);

    useUnsavedChangeRegistry(
      {
        ...unsavedExistedWorkflowSetupDataProps,
        formName:
          UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__PERFORM_MONETARY_ADJUSTMENT_TRANSACTIONS__SELECT_MONETARY_ADJUSTMENT_TRANSACTIONS,
        priority: 1
      },
      [!compare2Array(initialValues, dataChecked)]
    );

    const unsaveOptions = useMemo(
      () => ({
        ...confirmEditWhenChangeEffectToUploadProps,
        formName:
          UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__PERFORM_MONETARY_ADJUSTMENT_TRANSACTIONS__SELECT_MONETARY_ADJUSTMENT_TRANSACTIONS_CONFIRM_EDIT,
        priority: 1
      }),
      []
    );

    const handleResetUploadStep = () => {
      const file = dependencies?.files?.[0];
      const attachmentId = file?.idx;
      const isValid = file?.status === STATUS.valid;

      typeof clearFormValues === 'function' && clearFormValues(clearFormName);
      isValid &&
        dispatch(actionsWorkflowSetup.deleteTemplateFile({ attachmentId }));
    };
    useUnsavedChangeRegistry(
      unsaveOptions,
      [
        !isEmpty(dependencies?.files) &&
          !compare2Array(initialValues, dataChecked)
      ],
      handleResetUploadStep
    );

    const transactionsSelected = useMemo(() => {
      const listNumber = dataChecked?.length;
      const content = isCheckAll
        ? t('txt_all_transactions_selected')
        : listNumber !== 1
        ? t('txt_number_transactions_selected', { listNumber })
        : t('txt_number_transaction_selected', { listNumber });

      return (
        <TruncateText title={content} resizable>
          {content}
        </TruncateText>
      );
    }, [dataChecked, t, isCheckAll]);

    const viewNoData = useMemo(() => {
      return (
        <>
          <div className="pt-24 mt-24 border-top">
            <div className="d-flex align-items-center">
              <h5>{t('txt_perform_monetary_transaction_list')}</h5>
              <div className="ml-auto">
                {(total > 0 || !isEmpty(searchValue)) && (
                  <div className="ml-auto">
                    <SimpleSearch
                      defaultValue={searchValue}
                      onSearch={onSearchChange}
                      placeholder={t('txt_type_to_search_for_transaction')}
                    />
                  </div>
                )}
              </div>
            </div>
          </div>
          {!searchValue && <div className="mt-16">{transactionsSelected}</div>}
          <div className="d-flex flex-column justify-content-center mt-40 mb-32">
            <NoDataFound
              hasSearch={!!searchValue}
              id="search-workflow-monetary-not-found"
              title={t('txt_no_transactions_to_display')}
              linkTitle={!!searchValue && t('txt_clear_and_reset')}
              onLinkClicked={handleFetchNoData}
            />
          </div>
        </>
      );
    }, [
      handleFetchNoData,
      onSearchChange,
      searchValue,
      t,
      total,
      transactionsSelected
    ]);

    const { gridClassName, handleClickCheckAll } = useCheckAllPagination(
      {
        allIds: transactions.map(t => t.code),
        hasFilter: !!searchValue,
        filterIds: allDataFilter.map(f => f.code),
        checkedList: dataChecked,
        setCheckAll: onSetIsCheckAll,
        setCheckList: onSetDataChecked
      },
      [searchValue, sort, currentPageSize, currentPageSize]
    );

    // by formValue transactions to checkedList
    useEffect(() => {
      if (isEmpty(formValues?.transactionsProp) || isEmpty(transactions))
        return;

      const newCheckedList = transactions
        .filter(item => formValues.transactionsProp?.includes(item.code))
        .map(item => item.code);
      onSetDataChecked(newCheckedList);
      setFormValuesRef.current({
        ...formValues,
        transactionsProp: []
      });
      setInitialValues(newCheckedList);
    }, [transactions, formValues, onSetDataChecked]);

    // set new transactions & transactionsData to FormValues
    useEffect(() => {
      if (!compare2Array(formValues.transactions, dataChecked)) {
        setFormValuesRef.current({
          transactions: dataChecked,
          transactionsData: transactions?.filter(item =>
            dataChecked.includes(item.code)
          )
        });
      }
    }, [transactions, dataChecked, formValues.transactions]);

    // update isValid
    useEffect(() => {
      if (selectedStep === stepId) {
        const newIsValid = dataChecked.length > 0;
        if (newIsValid === formValues.isValid) return;
        setFormValuesRef.current({
          ...formValues,
          isValid: newIsValid,
          listLength: transactions.length
        });
      }
    }, [
      dataChecked.length,
      transactions.length,
      formValues,
      stepId,
      selectedStep
    ]);

    useEffect(() => {
      dispatch(
        actionsWorkflowSetup.getWorkflowMetadata([ParameterCode.TransactionId])
      );
    }, [dispatch]);
    const keepRef = useRef({ isLoadedAllTransaction: false });
    useEffect(() => {
      if (isEmpty(transactions) || keepRef.current.isLoadedAllTransaction) {
        return;
      }

      keepRef.current.isLoadedAllTransaction = true;
      dispatch(
        actionsWorkflowSetup.getWorkflowMetadata(
          uniq(transactions.map(t => `${t.code}.transaction`))
        )
      );
    }, [dispatch, transactions]);

    // set initialValues
    useEffect(() => {
      setInitialValues(formValues.transactions);
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [savedAt]);

    if (dataView.length === 0) return viewNoData;

    return (
      <div className="pt-24 mt-24 border-top">
        <div className="d-flex align-items-center">
          <h5>{t('txt_perform_monetary_transaction_list')}</h5>
          {(!isEmpty(searchValue) || total > 0) && (
            <div className="ml-auto">
              <SimpleSearch
                defaultValue={searchValue}
                onSearch={onSearchChange}
                placeholder={t('txt_type_to_search_for_transaction')}
              />
            </div>
          )}
        </div>

        <div className="d-flex justify-content-between my-16">
          {!searchValue && (
            <div className="flex-1 py-4">{transactionsSelected}</div>
          )}

          {!isEmpty(searchValue) && (
            <div className="ml-auto mr-n8">
              <Button
                size="sm"
                variant="outline-primary"
                onClick={handleClearAndReset}
              >
                {t('txt_clear_and_reset')}
              </Button>
            </div>
          )}
        </div>

        <Grid
          className={`grid-has-tooltip center ${gridClassName}`}
          data={dataView}
          columns={columns}
          expandedList={expandedList}
          checkedList={dataCheckedView}
          onSortChange={onSortChange}
          dataItemKey="id"
          togglable
          toggleButtonConfigList={dataView.map(mapGridExpandCollapse('id', t))}
          expandedItemKey="id"
          variant={{
            id: 'id',
            type: 'checkbox',
            cellHeaderProps: { onClick: handleClickCheckAll }
          }}
          sortBy={[sort]}
          subRow={renderSubRow}
          onExpand={handleExpand}
          onCheck={handleCheck}
        />
        {total > 10 && (
          <div className="mt-16">
            <Paging
              page={currentPage}
              pageSize={currentPageSize}
              totalItem={total}
              onChangePage={onPageChange}
              onChangePageSize={onPageSizeChange}
            />
          </div>
        )}
      </div>
    );
  };

export default GridStep;
