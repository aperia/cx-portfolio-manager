import { fireEvent } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';
import { renderWithMockStore } from 'app/utils';
import 'app/utils/_mockComponent/mockNoDataFound';
import { STATUS } from 'app/_libraries/_dls/components/Upload/helper';
import { queryAllByClass } from 'app/_libraries/_dls/test-utils';
import React from 'react';
import { act } from 'react-dom/test-utils';
import Grid from '.';
import { ParameterCode } from '../type';

const setFormValues = jest.fn();
const clearFormValues = jest.fn();

const get1MockData = (i: number | string) => ({
  value: `${i}`,
  description: `description${i}`
});

const generateMockData = (length: number) => {
  const result = [];
  for (let i = 0; i < length; i++) {
    result.push(get1MockData(i + 1));
  }

  return result;
};

const t = (value: string) => value;
const useTranslation = () => ({ t });
jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return { __esModule: true, ...actualModule, useTranslation };
});

jest.mock('app/hooks/useUnsavedChangeRegistry', () => {
  return {
    __esModule: true,
    useUnsavedChangeRegistry: (options: any, changes: any, onConfirm: any) => {
      onConfirm && onConfirm();
    }
  };
});

describe('WorkflowPerformMonetaryAdjustmentTransactions > SelectPerformMonetaryAdjustmentTransactionsStep > Grid', () => {
  const getInitialState = (transactions?: any[]) => ({
    workflowSetup: {
      elementMetadata: {
        elements: [
          {
            name: ParameterCode.TransactionId,
            options: transactions ?? generateMockData(2)
          },
          {
            name: ParameterCode.Transaction256,
            options: [{ value: 'Transaction256' }]
          }
        ]
      }
    }
  });

  it('render with loading', async () => {
    const props = {
      stepId: 'id',
      selectedStep: 'id1',
      savedAt: 1,
      formValues: { isValid: true },

      dependencies: {
        files: [
          {
            status: STATUS.valid
          }
        ]
      },
      setFormValues,
      clearFormValues
    } as any;
    await renderWithMockStore(<Grid {...props} />, {});
  });

  it('render with no data', async () => {
    const props = {
      stepId: 'id',
      selectedStep: 'id',
      savedAt: 1,
      formValues: { isValid: true },

      dependencies: {},
      setFormValues
    } as any;

    const initialState = getInitialState([]);

    jest.useFakeTimers();
    const wrapper = await renderWithMockStore(<Grid {...props} />, {
      initialState
    });
    jest.runAllTimers();
    expect(wrapper.getByText('No data found')).toBeInTheDocument();

    act(() => {
      userEvent.click(wrapper.getByText('No data found'));
    });
  });

  describe('with data', () => {
    it('render', async () => {
      const initialState = getInitialState();

      const props = {
        stepId: 'id',
        selectedStep: 'id',
        savedAt: 1,
        formValues: { isValid: true },

        dependencies: { attachmentId: 'id' },
        setFormValues
      } as any;

      jest.useFakeTimers();
      const wrapper = await renderWithMockStore(<Grid {...props} />, {
        initialState
      });
      jest.runAllTimers();

      const searchBox = wrapper.getByPlaceholderText(
        'txt_type_to_search_for_transaction'
      );

      userEvent.type(searchBox, '1');
      userEvent.click(
        searchBox.parentElement!.querySelector(
          '.icon-simple-search .icon.icon-search'
        )!
      );

      const btnExpand = queryAllByClass(wrapper.container, /icon icon-plus/)[0];
      act(() => {
        userEvent.click(btnExpand);
      });

      const allCollapseBtn = queryAllByClass(
        wrapper.container,
        /icon icon-minus/
      );
      act(() => {
        allCollapseBtn.forEach(e => userEvent.click(e));
      });

      expect(
        wrapper.queryByText('txt_number_transactions_selected')
      ).not.toBeInTheDocument();
    });

    it('check all', async () => {
      const initialState = getInitialState();

      const props = {
        stepId: 'id',
        selectedStep: 'id',
        savedAt: 1,
        formValues: { isValid: true },

        dependencies: {},
        setFormValues
      } as any;

      jest.useFakeTimers();
      const wrapper = await renderWithMockStore(<Grid {...props} />, {
        initialState
      });
      jest.runAllTimers();

      const checkboxAll = wrapper.getAllByRole('checkbox')[0];
      act(() => {
        fireEvent.change(checkboxAll, { target: { checked: true } });
      });

      expect(
        wrapper.getByText('txt_number_transactions_selected')
      ).toBeInTheDocument();
    });

    it('check all condition', async () => {
      const initialState = getInitialState();

      const props = {
        stepId: 'id',
        selectedStep: 'id',
        savedAt: 1,
        formValues: { isValid: true },

        dependencies: {},
        setFormValues
      } as any;

      jest.useFakeTimers();
      const wrapper = await renderWithMockStore(<Grid {...props} />, {
        initialState
      });
      jest.runAllTimers();

      const checkboxAll = wrapper.getAllByRole('checkbox')[0];
      act(() => {
        userEvent.click(checkboxAll);
      });

      expect(
        wrapper.getByText('txt_number_transactions_selected')
      ).toBeInTheDocument();
    });

    it('check one row', async () => {
      const initialState = getInitialState();

      const props = {
        stepId: 'id',
        selectedStep: 'id',
        savedAt: 1,
        formValues: { isValid: true },

        dependencies: {},
        setFormValues
      } as any;

      jest.useFakeTimers();
      const wrapper = await renderWithMockStore(<Grid {...props} />, {
        initialState
      });
      jest.runAllTimers();

      act(() => {
        userEvent.click(wrapper.getAllByRole('checkbox')[1]);
      });
      expect(
        wrapper.getByText(`txt_number_transactions_selected`)
      ).toBeInTheDocument();
    });

    it('check two row', async () => {
      const initialState = getInitialState();

      const props = {
        stepId: 'id',
        selectedStep: 'id',
        savedAt: 1,
        formValues: { isValid: true },

        dependencies: {},
        setFormValues
      } as any;

      jest.useFakeTimers();
      const wrapper = await renderWithMockStore(<Grid {...props} />, {
        initialState
      });
      jest.runAllTimers();

      act(() => {
        userEvent.click(wrapper.getAllByRole('checkbox')[1]);
      });
      expect(
        wrapper.getByText(`txt_number_transactions_selected`)
      ).toBeInTheDocument();
    });

    it('with formValue transaction', async () => {
      const initialState = getInitialState(generateMockData(11));

      const props = {
        stepId: 'id',
        selectedStep: 'id',
        savedAt: 1,
        formValues: {
          isValid: true,
          transactions: undefined,
          transactionsProp: ['1', '2']
        },

        dependencies: {},
        setFormValues
      } as any;

      jest.useFakeTimers();
      const wrapper = await renderWithMockStore(<Grid {...props} />, {
        initialState
      });
      jest.runAllTimers();

      act(() => {
        userEvent.click(wrapper.getAllByRole('checkbox')[1]);
      });

      wrapper.debug();
      setTimeout(() => {
        expect(
          wrapper.getByText('txt_number_transactions_selected')
        ).toBeInTheDocument();
      }, 1);
    });

    it('with formValue transaction', async () => {
      const initialState = getInitialState();

      const props = {
        stepId: 'id',
        selectedStep: 'id',
        savedAt: 1,
        formValues: { isValid: true, transactions: ['1', '2'] },

        dependencies: {},
        setFormValues
      } as any;

      jest.useFakeTimers();
      const wrapper = await renderWithMockStore(<Grid {...props} />, {
        initialState
      });
      jest.runAllTimers();

      act(() => {
        userEvent.click(wrapper.getAllByRole('checkbox')[1]);
      });

      wrapper.debug();
      setTimeout(() => {
        expect(
          wrapper.getByText('txt_number_transactions_selected')
        ).toBeInTheDocument();
      }, 1);
    });
  });
});
