import NoDataFoundGrid from 'app/components/NoDataFoundGrid';
import { ColumnType, Grid, useTranslation } from 'app/_libraries/_dls';
import get from 'lodash.get';
import isEmpty from 'lodash.isempty';
import React, { useEffect, useState } from 'react';

export const SubGrid = (props: any) => {
  const data = get(props, 'row.original.parameters', []);

  const { t } = useTranslation();

  const [columns, setColumns] = useState<ColumnType[]>([]);

  useEffect(() => {
    setColumns([
      {
        id: 'value',
        Header: t('txt_field_name'),
        accessor: 'value',
        width: 213
      },
      {
        id: 'type',
        Header: t('txt_field_type'),
        accessor: 'type',
        width: 113
      },
      {
        id: 'description',
        Header: t('txt_description'),
        accessor: 'description'
      }
    ]);
  }, [t]);

  return isEmpty(data) ? (
    <NoDataFoundGrid />
  ) : (
    <div style={{ padding: '2rem' }}>
      <Grid columns={columns} data={data} />
    </div>
  );
};
