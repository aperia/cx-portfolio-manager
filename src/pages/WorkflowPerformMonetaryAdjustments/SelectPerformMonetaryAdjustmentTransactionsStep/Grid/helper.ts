export const DEFAULT_SORT: {
  id: string;
  order: 'asc' | 'desc' | undefined;
} = { id: 'id', order: 'asc' };
