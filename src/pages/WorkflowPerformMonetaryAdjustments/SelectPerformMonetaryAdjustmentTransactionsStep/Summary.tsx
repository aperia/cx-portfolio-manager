import { usePagination } from 'app/hooks/usePagination';
import { Button, ColumnType, Grid, useTranslation } from 'app/_libraries/_dls';
import { isFunction, orderBy } from 'app/_libraries/_dls/lodash';
import Paging from 'pages/_commons/Utils/Paging';
import React, { useMemo } from 'react';
import { Form } from '.';
import { DEFAULT_SORT } from './Grid/helper';

const Summary: React.FC<WorkflowSetupSummaryProps<Form>> = props => {
  const { formValues, onEditStep } = props;

  const { t } = useTranslation();

  const handleEdit = () => {
    isFunction(onEditStep) && onEditStep();
  };

  const columnDefault: ColumnType[] = useMemo(
    () => [
      {
        id: 'text',
        Header: t('txt_perform_monetary_transactions_selected'),
        accessor: 'text'
      }
    ],
    [t]
  );

  const selectedData = useMemo(() => {
    return orderBy(
      formValues?.transactionsData || [],
      DEFAULT_SORT.id,
      DEFAULT_SORT.order
    );
  }, [formValues?.transactionsData]);

  const {
    total,
    currentPage,
    currentPageSize,
    gridData: dataView,
    onPageSizeChange,
    onPageChange
  } = usePagination(selectedData, ['text'], 'text');

  return (
    <div className="position-relative">
      <div className="absolute-top-right mt-n26 mr-n8">
        <Button variant="outline-primary" size="sm" onClick={handleEdit}>
          {t('txt_edit')}
        </Button>
      </div>
      <div className="pt-16">
        <Grid columns={columnDefault} data={dataView} />
      </div>
      <div className="mt-16">
        <Paging
          page={currentPage}
          pageSize={currentPageSize}
          totalItem={total}
          onChangePage={onPageChange}
          onChangePageSize={onPageSizeChange}
        />
      </div>
    </div>
  );
};

export default Summary;
