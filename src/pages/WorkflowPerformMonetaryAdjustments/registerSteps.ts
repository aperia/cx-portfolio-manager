import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { stepRegistry } from '../_commons/WorkflowSetup/registries';
import GettingStartStep from './GettingStartStep';
import SelectPerformMonetaryAdjustmentTransactions from './SelectPerformMonetaryAdjustmentTransactionsStep/index';

stepRegistry.registerStep(
  'GetStartedPerformMonetaryAdjustmentsWorkflow',
  GettingStartStep
);

stepRegistry.registerStep(
  'SelectPerformMonetaryAdjustmentsWorkflow',
  SelectPerformMonetaryAdjustmentTransactions,
  [
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__PERFORM_MONETARY_ADJUSTMENT_TRANSACTIONS__SELECT_MONETARY_ADJUSTMENT_TRANSACTIONS
  ]
);
