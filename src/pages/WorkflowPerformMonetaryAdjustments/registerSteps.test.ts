import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { default as stepRegistry } from '../_commons/WorkflowSetup/registries/stepRegistry';

const registerFunc = jest.spyOn(stepRegistry, 'registerStep');

describe('PerformMonetaryAdjustmentsWorkflow > registerSteps', () => {
  it('Should register components', async () => {
    await import('./registerSteps');

    expect(registerFunc).toBeCalledWith(
      'GetStartedPerformMonetaryAdjustmentsWorkflow',
      expect.anything()
    );
    expect(registerFunc).toBeCalledWith(
      'SelectPerformMonetaryAdjustmentsWorkflow',
      expect.anything(),
      [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__PERFORM_MONETARY_ADJUSTMENT_TRANSACTIONS__SELECT_MONETARY_ADJUSTMENT_TRANSACTIONS
      ]
    );
  });
});
