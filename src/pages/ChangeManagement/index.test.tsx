import { renderComponent } from 'app/utils';
import React from 'react';
import ChangeManagement from './';

jest.mock('app/_libraries/_dls', () => ({
  SimpleBar: ({ children }: any) => <div>{children}</div>,
  useTranslation: () => ({
    t: (key: string) => key
  })
}));
jest.mock('pages/_commons/ChangeList', () => ({
  __esModule: true,
  default: ({ title }: any) => <div>{title}</div>
}));

const testId = 'change-management';

describe('Page > Change Management', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  it('Should render Change Management UI', async () => {
    const { getByText } = await renderComponent(testId, <ChangeManagement />);

    expect(getByText('txt_change_management')).toBeInTheDocument();
  });
});
