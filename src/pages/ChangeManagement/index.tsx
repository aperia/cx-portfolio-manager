import { SimpleBar, useTranslation } from 'app/_libraries/_dls';
import ChangeList from 'pages/_commons/ChangeList';
import React from 'react';

interface IMyChangeManagementProps {}
const ChangeManagement: React.FC<IMyChangeManagementProps> = () => {
  const { t } = useTranslation();

  return (
    <SimpleBar>
      <ChangeList
        containerClassName={'p-24'}
        title={t('txt_change_management')}
        isFullPage={true}
      />
    </SimpleBar>
  );
};

export default ChangeManagement;
