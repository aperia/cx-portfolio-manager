import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { default as stepRegistry } from '../_commons/WorkflowSetup/registries/stepRegistry';

const registerFunc = jest.spyOn(stepRegistry, 'registerStep');

describe('WorkflowBulkExternalStatusManagement > registerSteps', () => {
  it('Should register components', async () => {
    await import('./registerSteps');

    expect(registerFunc).toBeCalledWith(
      'GetStartedBulkExternalStatusManagement',
      expect.anything()
    );

    expect(registerFunc).toBeCalledWith(
      'ConfigureParametersBulkExternalStatusManagement',
      expect.anything(),
      [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__SEND_BULK_EXTERNAL_STATUS_MANAGEMENT__CONFIGURE_PARAMETERS
      ]
    );
  });
});
