import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import {
  stepRegistry,
  uploadFileStepRegistry
} from '../_commons/WorkflowSetup/registries';
import ConfigureParametersStep from './ConfigureParametersStep';
import GettingStartStep from './GettingStartStep';
import ParameterList from './ParameterList';

stepRegistry.registerStep(
  'GetStartedBulkExternalStatusManagement',
  GettingStartStep
);

stepRegistry.registerStep(
  'ConfigureParametersBulkExternalStatusManagement',
  ConfigureParametersStep,
  [
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__SEND_BULK_EXTERNAL_STATUS_MANAGEMENT__CONFIGURE_PARAMETERS
  ]
);

stepRegistry.registerStep(
  'GetStartedBulkExternalStatusManagement',
  GettingStartStep
);

uploadFileStepRegistry.registerComponent('ParameterList', ParameterList);
