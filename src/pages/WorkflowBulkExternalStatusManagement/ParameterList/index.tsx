import {
  Button,
  ColumnType,
  Grid,
  Icon,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import { useSelectElementMetadataForBulkExternalStatus } from 'pages/_commons/redux/WorkflowSetup';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, { useEffect, useMemo, useState } from 'react';

export interface ParameterListProp {
  isValid?: boolean;
}

const ParameterList: React.FC<WorkflowSetupProps<ParameterListProp>> = ({
  stepId,
  selectedStep,
  title,
  formValues,
  setFormValues
}) => {
  const { t } = useTranslation();
  const [isExpand, setIsExpand] = useState(false);

  const { bulkExternalStatusManagementTransaction } =
    useSelectElementMetadataForBulkExternalStatus();

  const columns: ColumnType[] = useMemo(
    () => [
      {
        id: 'name',
        Header: t('txt_field_name'),
        accessor: 'code',
        width: 211
      },
      {
        id: 'type',
        Header: t('txt_field_type'),
        accessor: 'type',
        width: 103
      },
      {
        id: 'description',
        Header: t('txt_description'),
        accessor: 'text'
      }
    ],
    [t]
  );

  useEffect(() => {
    if (stepId !== selectedStep) {
      setIsExpand(false);
    }
  }, [stepId, selectedStep]);

  return (
    <div className="border-bottom py-24">
      <div className="d-flex align-items-center ">
        <div className="flex-shrink-0 ml-n2 mr-8">
          <Tooltip
            element={isExpand ? t('txt_collapse') : t('txt_expand')}
            variant="primary"
            placement="top"
          >
            <Button
              variant="icon-secondary"
              size="sm"
              onClick={() => setIsExpand(!isExpand)}
            >
              <Icon name={isExpand ? 'minus' : 'plus'} />
            </Button>
          </Tooltip>
        </div>
        <h5>{t('txt_parameter_list')}</h5>
      </div>
      {isExpand && (
        <div className="pt-8">
          <p className="color-grey">
            {t('txt_bulk_external_status_management_parameter_list_desc')}
          </p>
          {!isEmpty(bulkExternalStatusManagementTransaction) && (
            <div className="mt-16">
              <Grid
                columns={columns}
                data={bulkExternalStatusManagementTransaction}
              />
            </div>
          )}
        </div>
      )}
    </div>
  );
};

const ExtraStaticParameterList =
  ParameterList as WorkflowSetupStaticProp<ParameterListProp>;

ExtraStaticParameterList.defaultValues = { isValid: true };

export default ExtraStaticParameterList;
