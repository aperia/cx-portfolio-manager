import { render, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { queryAllByClass } from 'app/_libraries/_dls/test-utils/queryHelpers';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowBulkExternalStatusManagement';
import React from 'react';
import ParameterList from '.';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const useSelectElementMetadataForBulkExternalStatus = jest.spyOn(
  WorkflowSetup,
  'useSelectElementMetadataForBulkExternalStatus'
);

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <ParameterList {...props} />
    </div>
  );
};

describe('WorkflowBulkExternalStatusManagement > ParameterList', () => {
  beforeEach(() => {
    useSelectElementMetadataForBulkExternalStatus.mockReturnValue({
      bulkExternalStatusManagementStatusCode: [{ code: 'code', text: 'text' }],
      bulkExternalStatusManagementReasonCode: [{ code: 'code', text: 'text' }],
      bulkExternalStatusManagementTransactionId: [
        { code: 'code', text: 'text' }
      ],
      bulkExternalStatusManagementTransaction: [{ code: 'code', text: 'text' }]
    });
  });

  afterEach(() => {});

  it('Should render a grid with content', async () => {
    const props = {
      stepId: '1',
      selectedStep: '2',
      clearFormName: '',
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = await renderComponent({
      ...props,
      formValues: {
        isValid: false,
        transactionId: 'NM28',
        configureParameters: {}
      },
      dependencies: {
        attachmentId: '123'
      }
    });

    const expandBtn = queryAllByClass(wrapper.container, /icon icon-plus/)[0];
    userEvent.click(expandBtn);

    expect(wrapper.getByText('txt_field_name')).toBeInTheDocument();
    const minusBtn = queryAllByClass(wrapper.container, /icon icon-minus/)[0];
    userEvent.click(minusBtn);
  });
});
