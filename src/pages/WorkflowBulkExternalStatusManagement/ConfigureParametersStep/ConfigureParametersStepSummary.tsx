import GroupTextControl from 'app/components/DofControl/GroupTextControl';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import {
  Button,
  Grid,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import { isFunction } from 'app/_libraries/_dls/lodash';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import { useSelectElementMetadataForBulkExternalStatus } from 'pages/_commons/redux/WorkflowSetup';
import React, { useMemo } from 'react';
import { ConfigureParameter, ConfigureParametersFormValues } from '.';
import columns, { gridConfigureData } from './constant';

const ConfigureParametersStepSummary: React.FC<
  WorkflowSetupSummaryProps<ConfigureParametersFormValues>
> = ({ formValues, onEditStep }) => {
  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();
  const {
    bulkExternalStatusManagementTransactionId,
    bulkExternalStatusManagementStatusCode
  } = useSelectElementMetadataForBulkExternalStatus();
  const { configureParameters, transactionId } = formValues;

  const selectedTransaction = useMemo(
    () =>
      bulkExternalStatusManagementTransactionId.find(
        f => f.code === transactionId
      ),
    [bulkExternalStatusManagementTransactionId, transactionId]
  );

  const handleEdit = () => {
    isFunction(onEditStep) && onEditStep();
  };
  const gridData = useMemo(() => {
    if (transactionId === 'nm.28')
      return [
        gridConfigureData(t).find(
          f =>
            f.id ===
            MethodFieldParameterEnum.BulkExternalStatusManagementReasonCode
        )
      ];
    return gridConfigureData(t);
  }, [t, transactionId]);

  const valueAccessor = (records: Record<string, any>) => {
    const paramId = records.id as keyof ConfigureParameter;

    let valueTruncate = configureParameters?.[paramId] || '';
    if (
      !!valueTruncate &&
      bulkExternalStatusManagementStatusCode &&
      paramId ===
        MethodFieldParameterEnum.BulkExternalStatusManagementStatusCode
    ) {
      valueTruncate = bulkExternalStatusManagementStatusCode.find(
        f => f.code === configureParameters?.[paramId]
      )!.text!;
    }
    return (
      <TruncateText
        resizable
        lines={2}
        ellipsisLessText={t('txt_less')}
        ellipsisMoreText={t('txt_more')}
      >
        {valueTruncate}
      </TruncateText>
    );
  };

  return (
    <div className="position-relative">
      <div className="absolute-top-right mt-n26 mr-n8">
        <Button variant="outline-primary" size="sm" onClick={handleEdit}>
          {t('txt_edit')}
        </Button>
      </div>
      <div className="row">
        <div className="col-6 col-lg-3">
          <GroupTextControl
            id="summary_actionText"
            input={{ value: selectedTransaction?.description } as any}
            meta={{} as any}
            label={t('txt_action')}
            options={{
              inline: false,
              lineTruncate: 2,
              ellipsisLessText: t('txt_less'),
              ellipsisMoreText: t('txt_more')
            }}
          />
        </div>
        <div className="col-6 col-lg-3">
          <GroupTextControl
            id="summary_transaction"
            input={{ value: selectedTransaction?.text } as any}
            meta={{} as any}
            label={t('txt_transaction')}
            options={{
              inline: false,
              lineTruncate: 2,
              ellipsisLessText: t('txt_less'),
              ellipsisMoreText: t('txt_more')
            }}
          />
        </div>
      </div>
      <div className="mt-24">
        <Grid columns={columns(t, valueAccessor, width)} data={gridData} />
      </div>
    </div>
  );
};

export default ConfigureParametersStepSummary;
