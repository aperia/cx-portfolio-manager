import { fireEvent, RenderResult } from '@testing-library/react';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import { renderWithMockStore } from 'app/utils';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowBulkExternalStatusManagement';
import React from 'react';
import ConfigureParametersStepSummary from './ConfigureParametersStepSummary';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const useSelectElementMetadataForBulkExternalStatus = jest.spyOn(
  WorkflowSetup,
  'useSelectElementMetadataForBulkExternalStatus'
);

const renderComponent = (props: any): RenderResult => {
  return renderWithMockStore(
    <div>
      <ConfigureParametersStepSummary {...props} />
    </div>
  );
};

describe('WorkflowBulkExternalStatusManagement > ConfigureParametersStepSummary', () => {
  beforeEach(() => {
    useSelectElementMetadataForBulkExternalStatus.mockReturnValue({
      bulkExternalStatusManagementStatusCode: [
        { code: 'code', text: 'text' },
        { code: 'code1', text: 'text1' }
      ],
      bulkExternalStatusManagementReasonCode: [{ code: 'code', text: 'text' }],
      bulkExternalStatusManagementTransactionId: [
        { code: 'code', text: 'text' }
      ],
      bulkExternalStatusManagementTransaction: [{ code: 'code', text: 'text' }]
    });
  });

  afterEach(() => {});

  it('Should render a grid with content', async () => {
    const props = {
      stepId: '1',
      selectedStep: '2',
      clearFormName: '',
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = await renderComponent({
      ...props,
      formValues: {
        isValid: false,
        transactionId: 'nm.28',
        configureParameters: {
          [MethodFieldParameterEnum.BulkExternalStatusManagementStatusCode]:
            'code',
          [MethodFieldParameterEnum.BulkExternalStatusManagementReasonCode]:
            'code'
        }
      },
      dependencies: {
        attachmentId: '123'
      }
    });

    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    expect(body?.children.length).toEqual(1);
  });

  it('Should render a grid with content', async () => {
    const props = {
      stepId: '1',
      selectedStep: '2',
      clearFormName: '',
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = await renderComponent({
      ...props,
      formValues: {
        isValid: false,
        transactionId: 'nm.28',
        configureParameters: {
          [MethodFieldParameterEnum.BulkExternalStatusManagementStatusCode]:
            'code',
          [MethodFieldParameterEnum.BulkExternalStatusManagementReasonCode]:
            'code'
        }
      },
      dependencies: {
        attachmentId: '123'
      }
    });

    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    expect(body?.children.length).toEqual(1);
  });

  it('Should call edit function', async () => {
    const mockFn = jest.fn();
    const props = {
      formValues: {
        configureParameters: {
          [MethodFieldParameterEnum.BulkExternalStatusManagementStatusCode]:
            'code',
          [MethodFieldParameterEnum.BulkExternalStatusManagementReasonCode]:
            'code'
        }
      },
      onEditStep: mockFn
    } as any;

    const wrapper = await renderComponent(props);
    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(mockFn).toHaveBeenCalled();
  });

  it('Should call edit function plus', async () => {
    const mockFn = jest.fn();

    useSelectElementMetadataForBulkExternalStatus.mockReturnValue({
      bulkExternalStatusManagementStatusCode: [{ code: 'code', text: 'text' }],
      bulkExternalStatusManagementReasonCode: [{ code: 'code', text: 'text' }],
      bulkExternalStatusManagementTransactionId: [
        { code: 'code', text: 'text' }
      ],
      bulkExternalStatusManagementTransaction: [{ code: 'code', text: 'text' }]
    });

    const props = {
      formValues: {
        configureParameters: {
          [MethodFieldParameterEnum.BulkExternalStatusManagementStatusCode]:
            'code',
          [MethodFieldParameterEnum.BulkExternalStatusManagementReasonCode]:
            'code'
        }
      },
      onEditStep: mockFn
    } as any;

    const wrapper = await renderComponent(props);
    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(mockFn).toHaveBeenCalled();
  });
});
