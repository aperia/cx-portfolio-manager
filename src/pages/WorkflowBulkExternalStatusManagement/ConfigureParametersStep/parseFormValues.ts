import { getWorkflowSetupStepStatus } from 'app/helpers';
import { ConfigureParameter, TransactionType } from '.';

const parseFormValues: WorkflowSetupStepFormDataFunc<any> = (data, id) => {
  const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);

  const parameters = ((data?.data as any)?.configurations?.parameters ||
    []) as any[];
  const configureParameters = parameters.reduce(
    (pre, curr) => ({ ...pre, [curr.name]: curr.value }),
    {} as ConfigureParameter
  );

  const transactionId = (data?.data as any)?.configurations
    ?.transactionId as TransactionType;

  if (!stepInfo) return;

  const isValid = !!transactionId;

  return {
    ...getWorkflowSetupStepStatus(stepInfo),
    ...stepInfo.data,
    isValid,
    transactionId,
    configureParameters
  };
};

export default parseFormValues;
