import EnhanceDropdownList from 'app/components/EnhanceDropdownList';
import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import {
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { classnames, matchSearchValue } from 'app/helpers';
import { useUnsavedChangeRegistry } from 'app/hooks';
import { ComboBox, Grid, Radio, useTranslation } from 'app/_libraries/_dls';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import {
  actionsWorkflowSetup,
  useSelectElementMetadataForBulkExternalStatus
} from 'pages/_commons/redux/WorkflowSetup';
import ClearAndResetButton from 'pages/_commons/Utils/ClearAndResetButton';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';
import ConfigureParametersStepSummary from './ConfigureParametersStepSummary';
import columns, { gridConfigureData } from './constant';
import parseFormValues from './parseFormValues';
import stepValueFunc from './stepValueFunc';

export interface ConfigureParameter {
  [MethodFieldParameterEnum.BulkExternalStatusManagementStatusCode]?: string;
  [MethodFieldParameterEnum.BulkExternalStatusManagementReasonCode]?: string;
}
export type TransactionType = 'nm.16' | 'nm.28';

export interface GridParameter {
  id: MethodFieldParameterEnum;
  fieldName: string;
  greenScreenName: string;
  moreInfo: string;
}
export interface IConfigureParameters {}

export interface ConfigureParametersFormValues {
  isValid?: boolean;
  transactionId?: TransactionType;
  configureParameters?: ConfigureParameter;
}

const ConfigureParametersStep: React.FC<
  WorkflowSetupProps<ConfigureParametersFormValues>
> = ({ formValues, setFormValues, savedAt, stepId, selectedStep }) => {
  const dispatch = useDispatch();
  const { width } = useSelectWindowDimension();
  const [initialValues, setInitialValues] = useState(formValues);
  const { configureParameters, transactionId } = formValues;
  const [configureParametersValue, setConfigureParametersValue] =
    useState<Record<string, ConfigureParameter> | undefined>(undefined);

  const [searchValue, setSearchValue] = useState('');
  const {
    bulkExternalStatusManagementStatusCode,
    bulkExternalStatusManagementReasonCode,
    bulkExternalStatusManagementTransactionId
  } = useSelectElementMetadataForBulkExternalStatus();

  const { t } = useTranslation();
  const simpleSearchRef = useRef<any>(null);

  useEffect(() => {
    if (!searchValue && simpleSearchRef?.current) {
      simpleSearchRef.current.clear();
    }
  }, [searchValue]);
  useEffect(() => {
    if (configureParameters && !configureParametersValue) {
      setConfigureParametersValue({
        [`${transactionId}`]: configureParameters
      });
    }
  }, [configureParameters, transactionId, configureParametersValue]);

  useEffect(() => {
    if (stepId !== selectedStep) {
      setConfigureParametersValue(undefined);
      setSearchValue('');
    }
  }, [stepId, selectedStep, savedAt]);

  const handleSearch = (val: string) => {
    setSearchValue(val);
  };

  const gridData = useMemo(() => {
    if (transactionId === 'nm.28')
      return [
        gridConfigureData(t).find(
          f =>
            f.id ===
            MethodFieldParameterEnum.BulkExternalStatusManagementReasonCode
        )
      ];
    return gridConfigureData(t).filter(p =>
      matchSearchValue(
        [p.fieldName, p.greenScreenName, p.moreInfo],
        searchValue
      )
    );
  }, [t, searchValue, transactionId]);

  useEffect(() => {
    setInitialValues(formValues);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [savedAt]);

  const formChanged =
    initialValues?.transactionId !== transactionId ||
    initialValues?.configureParameters?.[
      MethodFieldParameterEnum.BulkExternalStatusManagementStatusCode
    ] !==
      configureParameters?.[
        MethodFieldParameterEnum.BulkExternalStatusManagementStatusCode
      ] ||
    initialValues?.configureParameters?.[
      MethodFieldParameterEnum.BulkExternalStatusManagementReasonCode
    ] !==
      configureParameters?.[
        MethodFieldParameterEnum.BulkExternalStatusManagementReasonCode
      ];

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__SEND_BULK_EXTERNAL_STATUS_MANAGEMENT__CONFIGURE_PARAMETERS,
      priority: 1
    },
    [formChanged]
  );

  const selectedTransaction = useMemo(
    () =>
      bulkExternalStatusManagementTransactionId.find(
        f => f.code === transactionId
      ),
    [bulkExternalStatusManagementTransactionId, transactionId]
  );

  const isValid = useMemo(() => !!transactionId, [transactionId]);

  useEffect(() => {
    if (formValues.isValid === isValid) return;
    setFormValues({
      ...formValues,
      isValid
    });
  }, [isValid, setFormValues, formValues]);

  useEffect(() => {
    dispatch(
      actionsWorkflowSetup.getWorkflowMetadata([
        MethodFieldParameterEnum.BulkExternalStatusManagementStatusCode,
        MethodFieldParameterEnum.BulkExternalStatusManagementReasonCode,
        MethodFieldParameterEnum.BulkExternalStatusManagementTransactionId,
        MethodFieldParameterEnum.BulkExternalStatusManagementTransaction
      ])
    );
  }, [dispatch]);

  const handleFormChange = useCallback(
    (field: keyof ConfigureParameter) => (e: any) => {
      setConfigureParametersValue({
        ...configureParametersValue,
        [`${transactionId}`]: {
          ...configureParametersValue?.[`${transactionId}`],
          [field]: e.target?.value?.code
        }
      });

      setFormValues({
        ...formValues,
        configureParameters: {
          ...formValues.configureParameters,
          [field]: e.target?.value?.code
        }
      });
    },
    [formValues, setFormValues, configureParametersValue, transactionId]
  );
  const renderDropdown = (data: RefData[], field: keyof ConfigureParameter) => {
    const value = data.find(
      o => o.code === configureParametersValue?.[`${transactionId}`]?.[field]
    );

    return (
      <EnhanceDropdownList
        placeholder={t('txt_workflow_manage_change_in_terms_select_an_option')}
        dataTestId={`configureParameters__${field}`}
        size="small"
        value={value}
        onChange={handleFormChange(field)}
        options={data}
      />
    );
  };
  const renderCombobox = (data: RefData[], field: keyof ConfigureParameter) => {
    const value = data.find(
      o => o.code === configureParametersValue?.[`${transactionId}`]?.[field]
    );

    return (
      <ComboBox
        placeholder={t('txt_workflow_manage_change_in_terms_select_an_option')}
        textField="text"
        size="small"
        value={value}
        onChange={handleFormChange(field)}
        noResult={t('txt_no_results_found_without_dot')}
      >
        {data.map(item => (
          <ComboBox.Item key={item.code} value={item} label={item.text} />
        ))}
      </ComboBox>
    );
  };

  const valueAccessor = (data: Record<string, any>) => {
    const paramId = data.id as MethodFieldParameterEnum;

    switch (paramId) {
      case MethodFieldParameterEnum.BulkExternalStatusManagementStatusCode: {
        return renderDropdown(
          bulkExternalStatusManagementStatusCode,
          MethodFieldParameterEnum.BulkExternalStatusManagementStatusCode
        );
      }
      case MethodFieldParameterEnum.BulkExternalStatusManagementReasonCode: {
        return renderCombobox(
          bulkExternalStatusManagementReasonCode,
          MethodFieldParameterEnum.BulkExternalStatusManagementReasonCode
        );
      }
    }
  };

  const handleChangeRadio = useCallback(
    (value: TransactionType) => {
      setFormValues({
        ...formValues,
        transactionId: value,
        configureParameters: configureParametersValue?.[`${value}`]
      });
      setSearchValue('');
    },
    [formValues, setFormValues, configureParametersValue]
  );

  const handleClearFilter = () => {
    setSearchValue('');
  };
  const renderRadioTransaction = useMemo(
    () => (
      <div className="group-card">
        {bulkExternalStatusManagementTransactionId?.map((item, idx) => (
          <div
            key={item.code}
            className="group-card-item d-flex align-items-center px-16 py-18 custom-control-root"
            onClick={() => handleChangeRadio(item.code as TransactionType)}
          >
            <span className="pr-8">{item.description}</span>
            <Radio className="ml-auto mr-n4">
              <Radio.Input
                checked={transactionId === (item.code as TransactionType)}
                id={item.code}
                name={item.code}
                className="checked-style"
              />
            </Radio>
          </div>
        ))}
      </div>
    ),
    [
      transactionId,
      handleChangeRadio,
      bulkExternalStatusManagementTransactionId
    ]
  );

  return (
    <>
      <div
        className={classnames({
          'pb-24 border-bottom': !!transactionId
        })}
      >
        <h5 className="mt-24">
          {t(
            'txt_bulk_external_status_management_configure_parameters_select_action'
          )}
        </h5>

        <div className="row mt-16">
          <div className="col-md-6">{renderRadioTransaction}</div>
          {!!transactionId && (
            <div className="col-md-6">
              <div className="bg-light-l20 rounded-lg p-16 h-100">
                <p className="color-grey-d20 fw-600">
                  {selectedTransaction?.text}
                </p>
              </div>
            </div>
          )}
        </div>
      </div>
      {!!transactionId && (
        <div className="mt-24">
          <div className="d-flex align-items-center justify-content-between mb-16">
            <h5>{selectedTransaction?.text}</h5>
            {transactionId === 'nm.16' && (
              <SimpleSearch
                ref={simpleSearchRef}
                placeholder={t('txt_type_to_search')}
                onSearch={handleSearch}
                popperElement={
                  <>
                    <p className="color-grey">{t('txt_search_description')}</p>
                    <ul className="search-field-item list-unstyled">
                      <li className="mt-16">{t('txt_business_name')}</li>
                      <li className="mt-16">
                        {t(
                          'txt_bulk_external_status_management_green_screen_name'
                        )}
                      </li>
                      <li className="mt-16">{t('txt_more_info')}</li>
                    </ul>
                  </>
                }
              />
            )}
          </div>
          {searchValue && !isEmpty(gridData) && (
            <div className="d-flex justify-content-end mb-16 mr-n8">
              <ClearAndResetButton small onClearAndReset={handleClearFilter} />
            </div>
          )}
          {isEmpty(gridData) && (
            <div className="d-flex flex-column justify-content-center mt-40 mb-32">
              <NoDataFound
                id="newMethod_notfound"
                hasSearch
                title={t('txt_no_results_found')}
                linkTitle={t('txt_clear_and_reset')}
                onLinkClicked={handleClearFilter}
              />
            </div>
          )}
          {!isEmpty(gridData) && (
            <Grid columns={columns(t, valueAccessor, width)} data={gridData} />
          )}
        </div>
      )}
    </>
  );
};

const ExtraStaticConfigureParameters =
  ConfigureParametersStep as WorkflowSetupStaticProp<ConfigureParametersFormValues>;
ExtraStaticConfigureParameters.summaryComponent =
  ConfigureParametersStepSummary;

ExtraStaticConfigureParameters.defaultValues = {
  isValid: false,
  configureParameters: {},
  transactionId: undefined
};
ExtraStaticConfigureParameters.parseFormValues = parseFormValues;
ExtraStaticConfigureParameters.stepValue = stepValueFunc;

export default ExtraStaticConfigureParameters;
