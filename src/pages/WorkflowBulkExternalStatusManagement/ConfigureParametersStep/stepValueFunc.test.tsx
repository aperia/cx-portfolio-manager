import { ConfigureParametersFormValues } from '.';
import stepValueFunc from './stepValueFunc';

describe('WorkflowBulkExternalStatusManagement > ConfigureParametersStep > stepValueFunc', () => {
  const t = (v: string) => v;

  it('Should Return NM16 title', () => {
    const stepsForm = {
      transactionId: 'nm.16'
    } as ConfigureParametersFormValues;

    const response = stepValueFunc({ stepsForm, t });
    expect(response).toEqual('txt_bulk_external_status_management_nm16_title');
  });
  it('Should Return NM28 title', () => {
    const stepsForm = {
      transactionId: 'nm.28'
    } as ConfigureParametersFormValues;

    const response = stepValueFunc({ stepsForm, t });
    expect(response).toEqual('txt_bulk_external_status_management_nm28_title');
  });

  it('Should Return Check param', () => {
    const response = stepValueFunc({ stepsForm: undefined as any, t });
    expect(response).toEqual('');
  });
});
