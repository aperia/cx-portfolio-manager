import { act, fireEvent, RenderResult, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import { mockUseDispatchFnc, renderWithMockStore } from 'app/utils';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowBulkExternalStatusManagement';
import React from 'react';
import ConfigureParametersStep from '.';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/utils/MockView')
);
const mockUseDispatch = mockUseDispatchFnc();

const useSelectElementMetadataForBulkExternalStatus = jest.spyOn(
  WorkflowSetup,
  'useSelectElementMetadataForBulkExternalStatus'
);

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});
jest.mock('app/hooks/useUnsavedChangeRegistry', () => {
  return {
    __esModule: true,
    useUnsavedChangeRegistry: (options: any, changes: any, onConfirm: any) => {
      onConfirm && onConfirm();
    }
  };
});
const renderComponent = (props: any): RenderResult => {
  return renderWithMockStore(
    <div>
      <ConfigureParametersStep {...props} />
    </div>
  );
};
const queryInputFromLabel = (tierLabel: HTMLElement) => {
  return tierLabel
    .closest(
      '.dls-input-container, .dls-numreric-container, .text-field-container'
    )
    ?.querySelector('input, .input') as HTMLInputElement;
};
const then_change_letter_number_dropdown = (wrapper: RenderResult) => {
  const setRecurrenceOptions = queryInputFromLabel(
    wrapper.getByText('txt_workflow_manage_change_in_terms_select_an_option')
  );
  act(() => {
    userEvent.click(setRecurrenceOptions);
  });

  act(() => {
    userEvent.click(screen.getByText('text'));
  });

  fireEvent.blur(setRecurrenceOptions);
};

describe('ConfigureParametersStep ', () => {
  const mockDispatch = jest.fn();
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
    useSelectElementMetadataForBulkExternalStatus.mockReturnValue({
      bulkExternalStatusManagementStatusCode: [{ code: 'code', text: 'text' }],
      bulkExternalStatusManagementReasonCode: [{ code: 'code', text: 'text' }],
      bulkExternalStatusManagementTransactionId: [
        { code: 'code', text: 'text' }
      ],
      bulkExternalStatusManagementTransaction: [{ code: 'code', text: 'text' }]
    });
  });

  afterEach(() => {
    mockUseDispatch.mockClear();
  });

  it('Should have error', async () => {
    const props = {
      stepId: '1',
      selectedStep: '2',
      clearFormName: '',
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = await renderComponent({
      ...props,
      formValues: {
        isValid: false,
        configureParameters: {}
      }
    });

    expect(
      wrapper.getByText(
        'txt_bulk_external_status_management_configure_parameters_select_action'
      )
    ).toBeInTheDocument;
  });

  it('Should render with NM28', async () => {
    const props = {
      stepId: '1',
      selectedStep: '2',
      clearFormName: '',
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = await renderComponent({
      ...props,
      formValues: {
        isValid: false,
        transactionId: 'nm.28',
        configureParameters: {
          configureParameters: {
            [MethodFieldParameterEnum.BulkExternalStatusManagementStatusCode]:
              'I',
            [MethodFieldParameterEnum.BulkExternalStatusManagementReasonCode]:
              '0'
          }
        }
      },
      dependencies: {
        attachmentId: '123'
      }
    });

    expect(wrapper.getByText('txt_reason_code')).toBeInTheDocument;
  });

  it('Should render with NM16', async () => {
    const props = {
      stepId: '2',
      selectedStep: '2',
      clearFormName: '',
      setFormValues: jest.fn(),
      clearFormValues: jest.fn()
    };
    const wrapper = await renderComponent({
      ...props,
      formValues: {
        isValid: false,
        transactionId: 'nm.16',
        configureParameters: {
          configureParameters: {
            [MethodFieldParameterEnum.BulkExternalStatusManagementStatusCode]:
              'I',
            [MethodFieldParameterEnum.BulkExternalStatusManagementReasonCode]:
              '0'
          }
        }
      },
      dependencies: {
        attachmentId: '123'
      }
    });

    userEvent.click(screen.queryAllByRole('radio')[0]);
    then_change_letter_number_dropdown(wrapper);

    expect(
      wrapper.getByText(
        'txt_bulk_external_status_management_configure_parameters_select_action'
      )
    ).toBeInTheDocument;

    const searchBox = wrapper.getByPlaceholderText('txt_type_to_search');
    userEvent.type(searchBox, 'a');
    userEvent.click(
      searchBox.parentElement!.querySelector(
        '.icon-simple-search .icon.icon-search'
      )!
    );

    userEvent.type(searchBox, 'notfound');
    userEvent.click(
      searchBox.parentElement!.querySelector(
        '.icon-simple-search .icon.icon-search'
      )!
    );

    expect(
      wrapper.getByText('txt_no_results_found txt_adjust_your_search_criteria')
    ).toBeInTheDocument();

    userEvent.click(wrapper.getByText('txt_clear_and_reset'));
    expect(
      wrapper.queryByText(
        'txt_no_results_found txt_adjust_your_search_criteria'
      )
    ).not.toBeInTheDocument();
  });
});
