import { MethodFieldParameterEnum } from 'app/constants/enums';
import parseFormValues from './parseFormValues';

describe('WorkflowBulkExternalStatusManagementWorkFlow > ConfigureParametersStep > parseFormValues', () => {
  it('Should return undefined', () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '2'
            }
          ] as any
        }
      } as WorkflowSetupInstance,
      id: '1'
    };

    const response = parseFormValues(props.data, props.id);
    expect(response).toEqual(undefined);
  });

  it('Should return data', () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '1'
            }
          ] as any,
          transactionId: '',
          configurations: {
            [MethodFieldParameterEnum.BulkExternalStatusManagementStatusCode]:
              '2',
            [MethodFieldParameterEnum.BulkExternalStatusManagementReasonCode]:
              '1',

            parameters: [
              {
                name: 'name1',
                value: 'value1'
              },
              {
                name: 'name2',
                value: 'value2'
              }
            ]
          } as any
        }
      } as any,
      id: '1'
    };

    const response = parseFormValues(props.data, props.id);
    expect(response)?.toEqual({
      configureParameters: {
        name1: 'value1',
        name2: 'value2'
      },
      isPass: false,
      isSelected: false,
      isValid: false,
      transactionId: undefined
    });
  });
});
