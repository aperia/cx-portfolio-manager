import { MethodFieldParameterEnum } from 'app/constants/enums';
import { ColumnType } from 'app/_libraries/_dls';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import { GridParameter } from '.';

const columns = (t: any, valueAccessor: any, width: number): ColumnType[] => [
  {
    id: 'fieldName',
    Header: t('txt_business_name'),
    accessor: 'fieldName',
    cellBodyProps: { className: 'pt-12 pb-8' }
  },
  {
    id: 'greenScreenName',
    Header: t('txt_bulk_external_status_management_green_screen_name'),
    accessor: 'greenScreenName',
    cellBodyProps: { className: 'pt-12 pb-8' }
  },
  {
    id: 'value',
    Header: t('txt_value'),
    cellBodyProps: { className: 'py-8' },
    accessor: valueAccessor,
    width: width < 1280 ? 220 : undefined
  },
  {
    id: 'moreInfo',
    Header: t('txt_more_info'),
    className: 'text-center',
    accessor: viewMoreInfo(['moreInfo'], t),
    width: 105,
    cellBodyProps: { className: 'pt-12 pb-8' }
  }
];

export const gridConfigureData = (t: any): GridParameter[] => [
  {
    id: MethodFieldParameterEnum.BulkExternalStatusManagementStatusCode,
    fieldName: t('txt_bulk_external_status_management_external_status_code'),
    greenScreenName: t('txt_status'),
    moreInfo: t(
      'txt_bulk_external_status_management_external_status_code_more_info'
    )
  },
  {
    id: MethodFieldParameterEnum.BulkExternalStatusManagementReasonCode,
    fieldName: t('txt_reason_code'),
    greenScreenName: t('txt_reason'),
    moreInfo: t('txt_bulk_external_status_management_reason_code_more_info')
  }
];

export default columns;
