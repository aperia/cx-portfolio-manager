import { ConfigureParametersFormValues } from '.';

const stepValueFunc: WorkflowSetupStepValueFunc<ConfigureParametersFormValues> =
  ({ stepsForm: formValues, t }) => {
    const { transactionId } = formValues || {};

    switch (transactionId) {
      case 'nm.16':
        return t('txt_bulk_external_status_management_nm16_title');
      case 'nm.28':
        return t('txt_bulk_external_status_management_nm28_title');
      default:
        return '';
    }
  };

export default stepValueFunc;
