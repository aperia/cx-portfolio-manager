import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { stepRegistry } from '../_commons/WorkflowSetup/registries';
import ConfigParametersStep from './ConfigParametersStep';
import GettingStartStep from './GettingStartStep';

stepRegistry.registerStep(
  'GetStartedConfigureProcessingMerchants',
  GettingStartStep
);
stepRegistry.registerStep(
  'ConfigParametersConfigureProcessingMerchants',
  ConfigParametersStep,
  [
    UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CONFIGURE_PROCESSING_MERCHANTS__CONFIGURE_PARAMETERS
  ]
);
