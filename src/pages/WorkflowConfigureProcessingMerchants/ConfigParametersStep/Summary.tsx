import NoDataFound from 'app/components/NoDataFound';
import { GroupTextFormat } from 'app/constants/enums';
import { mapGridExpandCollapse } from 'app/helpers';
import { usePagination } from 'app/hooks/usePagination';
import { Button, ColumnType, Grid, useTranslation } from 'app/_libraries/_dls';
import { isFunction } from 'lodash';
import { useSelectElementsForConfigProcessingMerchants } from 'pages/_commons/redux/WorkflowSetup';
import { formatText } from 'pages/_commons/Utils/formatGridField';
import Paging from 'pages/_commons/Utils/Paging';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { FormConfigParameters } from '.';
import MerchantListSubGrid from './MerchantListSubGrid';

const ConfigParametersSummary: React.FC<
  WorkflowSetupSummaryProps<FormConfigParameters>
> = ({ formValues, onEditStep, stepId, selectedStep }) => {
  const { t } = useTranslation();
  const merchantElements = useSelectElementsForConfigProcessingMerchants();

  const [expandedList, setExpandedList] = useState<string[]>([]);

  const {
    total,
    sort,
    currentPage,
    currentPageSize,
    gridData: dataView,
    onSortChange,
    onPageChange,
    onPageSizeChange,
    onResetToDefaultFilter
  } = usePagination(formValues.merchants, [], 'merchantName');

  useEffect(() => {
    if (stepId !== selectedStep) return;

    setExpandedList([]);
    onResetToDefaultFilter();
  }, [onResetToDefaultFilter, stepId, selectedStep]);

  const columns = useMemo<ColumnType[]>(
    () => [
      {
        id: 'sysPrin',
        Header: t('txt_config_processing_merchants_sysPrin'),
        accessor: 'sysPrin',
        isSort: true
      },
      {
        id: 'merchantName',
        Header: t('txt_config_processing_merchants_merchantName'),
        accessor: 'merchantName',
        isSort: true
      },
      {
        id: 'modeledFrom.merchantName',
        Header: t('txt_modeled_from'),
        accessor: 'modeledFrom.merchantName',
        isSort: true
      },
      {
        id: 'phoneNumber',
        Header: t('txt_config_processing_merchants_phoneNumber'),
        accessor: formatText(['phoneNumber'], {
          format: GroupTextFormat.PhoneNumber
        })
      },
      {
        id: 'taxpayerId',
        Header: t('txt_config_processing_merchants_taxpayerId'),
        accessor: formatText(['taxpayerId'], {
          format: GroupTextFormat.TaxpayerId
        })
      }
    ],
    [t]
  );

  const renderSubRow = useCallback(
    ({ original }: { original: MerchantItemCreated }) => (
      <MerchantListSubGrid original={original} metadata={merchantElements} />
    ),
    [merchantElements]
  );

  const handleEdit = () => {
    isFunction(onEditStep) && onEditStep();
  };

  return (
    <div className="position-relative">
      <div className="absolute-top-right mt-n26 mr-n8">
        <Button variant="outline-primary" size="sm" onClick={handleEdit}>
          {t('txt_edit')}
        </Button>
      </div>
      <div className="pt-16">
        {total === 0 && (
          <div className="d-flex flex-column justify-content-center mt-40 mb-32">
            <NoDataFound
              id="merchant-list-summary-NoDataFound"
              title={t('txt_config_processing_merchants_no_update_merchants')}
            />
          </div>
        )}
        {total > 0 && (
          <Grid
            togglable
            dataItemKey="rowId"
            expandedItemKey="rowId"
            data={dataView}
            columns={columns}
            sortBy={[sort]}
            onSortChange={onSortChange}
            expandedList={expandedList}
            toggleButtonConfigList={dataView.map(
              mapGridExpandCollapse('rowId', t)
            )}
            subRow={renderSubRow}
            onExpand={setExpandedList}
          />
        )}
        {total > 10 && (
          <div className="mt-16">
            <Paging
              page={currentPage}
              pageSize={currentPageSize}
              totalItem={total}
              onChangePage={onPageChange}
              onChangePageSize={onPageSizeChange}
            />
          </div>
        )}
      </div>
    </div>
  );
};

export default ConfigParametersSummary;
