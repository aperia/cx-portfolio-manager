import { RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import {
  mockThunkAction,
  mockUseModalRegistryFnc,
  renderWithMockStore
} from 'app/utils';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowConfigureProcessingMerchants';
import React from 'react';
import SelectMerchantModal, {
  SelectMerchantModalProps
} from './SelectMerchantModal';
import { ParameterCode } from './type';

const useSelectElementMetadata = jest.spyOn(
  WorkflowSetup,
  'useSelectElementsForConfigProcessingMerchants'
);
const mockUseModalRegistry = mockUseModalRegistryFnc();

const mockWorkflowSetup = mockThunkAction(actionsWorkflowSetup);

const renderComponent = async (
  props: any,
  initialState?: any
): Promise<RenderResult> => {
  return await renderWithMockStore(<SelectMerchantModal {...props} />, {
    initialState
  });
};

export const mockElements = [
  {
    name: ParameterCode.SystemPrinciple,
    options: [{ value: '0000-0001' }, { value: '0000-0002' }]
  },
  {
    name: ParameterCode.AssessmentCode,
    options: [{ value: 'AssessmentCode' }]
  },
  {
    name: ParameterCode.InformationIndicator,
    options: [{ value: 'InformationIndicator' }]
  },
  {
    name: ParameterCode.AggregatorType,
    options: [{ value: 'AggregatorType' }]
  },
  {
    name: ParameterCode.TINType,
    options: [{ value: 'TINType' }]
  },
  {
    name: ParameterCode.SparkExclusionIndicator,
    options: [{ value: 'SparkExclusionIndicator' }]
  },
  {
    name: ParameterCode.TransactionDecisionTableId,
    options: [{ value: 'TransactionDecisionTableId' }]
  },
  {
    name: ParameterCode.InStorePayment,
    options: [{ value: 'InStorePayment' }]
  },
  {
    name: ParameterCode.MerchantPaymentDescription,
    options: [{ value: 'MerchantPaymentDescription' }]
  },
  {
    name: ParameterCode.ProcessFlag,
    options: [{ value: 'ProcessFlag' }]
  },
  {
    name: ParameterCode.DiscountMethodParticipation,
    options: [{ value: 'DiscountMethodParticipation' }]
  }
];

describe('ConfigureProcessingMerchantsWorkflow > ConfigParametersStep > SelectMerchantModal', () => {
  beforeEach(() => {
    mockUseModalRegistry.mockReturnValue([true, false]);
    mockWorkflowSetup('getConfigureProcessingMerchantsList');
  });

  const defaultState = {
    workflowSetup: {
      elementMetadata: { elements: mockElements },
      merchantData: { merchants: [] }
    }
  };

  it('should render', async () => {
    useSelectElementMetadata.mockReturnValue({
      aggregatorType: [{ code: 'name', description: 'type' }],
      assessmentCode: [{ code: 'name', description: 'type' }],
      discountMethodParticipation: [{ code: 'name', description: 'type' }],
      inStorePayment: [{ code: 'name', description: 'type' }],
      informationIndicator: [{ code: 'name', description: 'type' }],
      merchantPaymentDescription: [{ code: 'name', description: 'type' }],
      processFlag: [{ code: 'name', description: 'type' }],
      sparkExclusionIndicator: [{ code: 'name', description: 'type' }],
      transactionDecisionTableId: [{ code: 'name', description: 'type' }],
      promotionDiscountRateTableId: [{ code: 'name', description: 'type' }],
      TINType: [{ code: 'name', description: 'type' }]
    } as any);
    const props = { systemPrinciple: '000 001' } as SelectMerchantModalProps;

    const { getByText, getByPlaceholderText, rerender } = await renderComponent(
      props,
      defaultState
    );

    userEvent.click(
      getByText('txt_config_processing_merchants_export_merchants')
    );
    rerender(<SelectMerchantModal {...props} show={false} />);

    const searchBox = getByPlaceholderText('txt_type_to_search');
    userEvent.type(searchBox, 'a');
    userEvent.click(
      searchBox.parentElement!.querySelector('.icon.icon-search')!
    );

    expect(
      getByText('txt_config_processing_merchants_merchant_list')
    ).toBeInTheDocument();
  });

  it('should select merchant in the list', async () => {
    useSelectElementMetadata.mockReturnValue({
      aggregatorType: [{ code: 'name', description: 'type' }],
      assessmentCode: [{ code: 'name', description: 'type' }],
      discountMethodParticipation: [{ code: 'name', description: 'type' }],
      inStorePayment: [{ code: 'name', description: 'type' }],
      informationIndicator: [{ code: 'name', description: 'type' }],
      merchantPaymentDescription: [{ code: 'name', description: 'type' }],
      processFlag: [{ code: 'name', description: 'type' }],
      sparkExclusionIndicator: [{ code: 'name', description: 'type' }],
      transactionDecisionTableId: [{ code: 'name', description: 'type' }],
      promotionDiscountRateTableId: [{ code: 'name', description: 'type' }],
      TINType: [{ code: 'name', description: 'type' }]
    } as any);
    const onContinue = jest.fn() as any;
    const props = {
      systemPrinciple: '000 001',
      onContinue
    } as SelectMerchantModalProps;

    const { baseElement, getByPlaceholderText, getByText } =
      await renderComponent(props, {
        workflowSetup: {
          ...defaultState.workflowSetup,
          merchantData: { merchants: [{ id: '1', merchantName: 'a' }] }
        }
      });

    userEvent.click(baseElement.querySelector('i.icon.icon-plus')!);

    const searchBox = getByPlaceholderText('txt_type_to_search');
    userEvent.type(searchBox, 'a');
    userEvent.click(
      searchBox.parentElement!.querySelector('.icon.icon-search')!
    );

    userEvent.click(getByText('txt_clear_and_reset'));
    userEvent.click(baseElement.querySelector('.dls-radio .dls-radio-label')!);

    userEvent.click(getByText('txt_continue'));

    expect(onContinue).toBeCalled();
  });

  it('should run onBack', async () => {
    const onBack = jest.fn() as any;
    const props = {
      systemPrinciple: '000 001',
      onBack
    } as SelectMerchantModalProps;

    const { getByText } = await renderComponent(props, {
      workflowSetup: {
        ...defaultState.workflowSetup,
        merchantData: {
          merchants: [
            { id: '1' },
            { id: '2' },
            { id: '3' },
            { id: '4' },
            { id: '5' },
            { id: '6' },
            { id: '7' },
            { id: '8' },
            { id: '9' },
            { id: '10' },
            { id: '11' }
          ]
        }
      }
    });

    userEvent.click(getByText('txt_back'));

    expect(onBack).toBeCalled();
  });
});
