import { RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { mockUseModalRegistryFnc, renderWithMockStore } from 'app/utils';
import React from 'react';
import SelectSPModal, { SelectSPModalProps } from './SelectSPModal';

const mockUseModalRegistry = mockUseModalRegistryFnc();

const renderComponent = async (props: any): Promise<RenderResult> => {
  return await renderWithMockStore(<SelectSPModal {...props} />);
};

describe('ConfigureProcessingMerchantsWorkflow > ConfigParametersStep > SelectSPModal', () => {
  beforeEach(() => {
    mockUseModalRegistry.mockReturnValue([true, false]);
  });

  it('should render', async () => {
    const props = {
      systems: [{ code: '000', text: '000' }],
      principles: { '000': [{ code: '001', text: '001' }] }
    } as SelectSPModalProps;

    const { getByText, rerender } = await renderComponent(props);

    rerender(<SelectSPModal {...props} resetValue="000" />);

    expect(
      getByText('txt_config_processing_merchants_select_sys_prin')
    ).toBeInTheDocument();
  });

  it('should select merchant and continue', async () => {
    const onContinue = jest.fn();
    const props = {
      systems: [{ code: '000', text: '000' }],
      principles: { '000': [{ code: '001', text: '001' }] },
      onContinue
    } as SelectSPModalProps;

    const { getByText } = await renderComponent(props);

    const sysOptions = getByText('txt_spa_sys');
    userEvent.click(sysOptions);
    userEvent.click(getByText('000'));

    userEvent.click(getByText('txt_continue'));
    expect(onContinue).toBeCalled();

    const prinOptions = getByText('txt_spa_prin');
    userEvent.click(prinOptions);
    userEvent.click(getByText('001'));

    userEvent.click(getByText('txt_continue'));

    expect(onContinue).toBeCalled();
  });
});
