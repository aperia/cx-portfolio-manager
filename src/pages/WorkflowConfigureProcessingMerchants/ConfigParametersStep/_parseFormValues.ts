import { getWorkflowSetupStepStatus } from 'app/helpers';
import { FormConfigParameters } from '.';
import { parseMerchantsParameterToProp } from './helper';

const parseFormValues: WorkflowSetupStepFormDataFunc<FormConfigParameters> = (
  data,
  id
) => {
  const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);
  if (!stepInfo) return;

  const merchants = parseMerchantsParameterToProp(
    data?.data?.configurations?.merchants || []
  );

  return {
    ...getWorkflowSetupStepStatus(stepInfo),
    merchants,
    isValid: merchants.length > 0
  };
};

export default parseFormValues;
