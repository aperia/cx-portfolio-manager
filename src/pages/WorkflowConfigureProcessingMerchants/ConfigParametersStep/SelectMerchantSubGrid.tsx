import {
  convertRefValue,
  formatContactPhone,
  formatTaxpayerId
} from 'app/helpers';
import {
  ColumnType,
  Grid,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React, { useCallback, useMemo } from 'react';
import { selectMerchantParameters } from './_parameterList';

export interface SelectMerchantSubGridProps {
  original: MerchantItem;
  metadata: {
    assessmentCode: RefData[];
    informationIndicator: RefData[];
    aggregatorType: RefData[];
    TINType: RefData[];
    sparkExclusionIndicator: RefData[];
    transactionDecisionTableId: RefData[];
    promotionDiscountRateTableId: RefData[];
    inStorePayment: RefData[];
    merchantPaymentDescription: RefData[];
    processFlag: RefData[];
    discountMethodParticipation: RefData[];
  };
}
const SelectMerchantSubGrid: React.FC<SelectMerchantSubGridProps> = ({
  original,
  metadata
}) => {
  const { t } = useTranslation();

  const values = useMemo<Record<keyof MerchantItem, string | undefined>>(() => {
    const {
      aggregatorType,
      assessmentCode,
      discountMethodParticipation,
      inStorePayment,
      informationIndicator,
      merchantPaymentDescription,
      processFlag,
      sparkExclusionIndicator,
      transactionDecisionTableId,
      promotionDiscountRateTableId,
      TINType
    } = metadata;

    return {
      ...original,
      phoneNumber: formatContactPhone(original.phoneNumber),
      merchantCustomerServicePhone: formatContactPhone(
        original.merchantCustomerServicePhone
      ),
      taxpayerId: formatTaxpayerId(original.taxpayerId),
      aggregatorType: convertRefValue(aggregatorType, original.aggregatorType),
      assessmentCode: convertRefValue(assessmentCode, original.assessmentCode),
      discountMethodParticipation: convertRefValue(
        discountMethodParticipation,
        original.discountMethodParticipation
      ),
      inStorePayment: convertRefValue(inStorePayment, original.inStorePayment),
      informationIndicator: convertRefValue(
        informationIndicator,
        original.informationIndicator
      ),
      merchantPaymentDescription: convertRefValue(
        merchantPaymentDescription,
        original.merchantPaymentDescription
      ),
      processFlag: convertRefValue(processFlag, original.processFlag),
      sparkExclusionIndicator: convertRefValue(
        sparkExclusionIndicator,
        original.sparkExclusionIndicator
      ),
      transactionDecisionTableId: convertRefValue(
        transactionDecisionTableId,
        original.transactionDecisionTableId
      ),
      promotionDiscountRateTableId: convertRefValue(
        promotionDiscountRateTableId,
        original.promotionDiscountRateTableId
      ),
      TINType: convertRefValue(TINType, original.TINType)
    };
  }, [metadata, original]);

  const valueAccessor = useCallback(
    (data: Record<string, any>) => (
      <TruncateText
        lines={2}
        ellipsisLessText={t('txt_less')}
        ellipsisMoreText={t('txt_more')}
      >
        {values[data.id as keyof MerchantItem]}
      </TruncateText>
    ),
    [t, values]
  );

  const columns: ColumnType[] = useMemo(
    () => [
      {
        id: 'bussinessName',
        Header: t('txt_business_name'),
        accessor: 'bussinessName',
        width: 240
      },
      {
        id: 'greenScreenName',
        Header: t('txt_config_processing_merchants_green_screen_name'),
        accessor: 'greenScreenName',
        width: 240
      },
      {
        id: 'id',
        Header: t('txt_value'),
        accessor: valueAccessor
      },
      {
        id: 'moreInfo',
        Header: t('txt_more_info'),
        className: 'text-center',
        accessor: viewMoreInfo(['moreInfo'], t),
        width: 105
      }
    ],
    [t, valueAccessor]
  );

  return (
    <div className="p-20 overflow-hidden">
      <Grid columns={columns} data={selectMerchantParameters} />
    </div>
  );
};

export default SelectMerchantSubGrid;
