import {
  parseMerchantsParameterToProp,
  parseMerchantsPropToParameter
} from './helper';

describe('test ConfigParametersStep > helper', () => {
  it('test parseMerchantsPropToParameter function', () => {
    const result = parseMerchantsPropToParameter([
      {
        rowId: 'rowId1',
        sysPrin: 'sysPrin1',
        modeledFrom: {
          merchantName: 'name1',
          phoneNumber: 'phone1'
        },
        merchantName: 'name0',
        phoneNumber: 'phone0'
      }
    ]);
    expect(result[0].id).toEqual('rowId1');
  });

  it('test parseMerchantsParameterToProp function', () => {
    const result = parseMerchantsParameterToProp([
      {
        id: 'rowId1',
        sysPrin: 'sysPrin1',
        parameters: [
          {
            name: 'merchantName',
            previousValue: 'name1',
            newValue: 'name0'
          },
          {
            name: 'phoneNumber',
            previousValue: 'phone1',
            newValue: 'phone0'
          }
        ]
      }
    ]);
    expect(result[0].rowId).toEqual('rowId1');
  });
});
