import EnhanceDropdownList from 'app/components/EnhanceDropdownList';
import FormatTextBox from 'app/components/FormatTextBox';
import ModalRegistry from 'app/components/ModalRegistry';
import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch, { SimpleSearchPublic } from 'app/components/SimpleSearch';
import {
  unsavedChangesProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import {
  convertStringToNumberOnly,
  formatContactPhone,
  formatTaxpayerId
} from 'app/helpers';
import { useUnsavedChangeRegistry } from 'app/hooks';
import { usePagination } from 'app/hooks/usePagination';
import {
  Button,
  ColumnType,
  ComboBox,
  Grid,
  InlineMessage,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  TextBox,
  useTranslation
} from 'app/_libraries/_dls';
import { isFunction } from 'lodash';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import { useSelectElementsForConfigProcessingMerchants } from 'pages/_commons/redux/WorkflowSetup';
import ClearAndResetButton from 'pages/_commons/Utils/ClearAndResetButton';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';
import { parameters } from './_parameterList';

export interface MerchantDetailsModalProps {
  show?: boolean;
  merchant: MerchantItem;
  systemPrinciple: string;

  onCancel?: VoidFunction;
  onBack?: () => void;
  onSave?: (merchant: MerchantItemCreated) => void;
}

const searchFields = ['bussinessName', 'greenScreenName', 'moreInfoText'];

const getMerchantCreated = (
  merchant: MerchantItem,
  sysPrin: string
): MerchantItemCreated => {
  if ((merchant as MerchantItemCreated).modeledFrom) {
    return { ...merchant };
  }

  return {
    ...merchant,
    modeledFrom: { ...merchant },
    sysPrin,
    rowId: `${Date.now()}`
  };
};

const MerchantDetailsModal: React.FC<MerchantDetailsModalProps> = ({
  show = true,
  merchant: merchantProp,
  systemPrinciple,

  onCancel,
  onSave,
  onBack
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const searchRef = useRef<SimpleSearchPublic>(null);
  const { width } = useSelectWindowDimension();
  const [merchant, setMerchant] = useState<MerchantItemCreated>(
    getMerchantCreated(merchantProp, systemPrinciple)
  );
  const [initialMerchant] = useState<MerchantItemCreated>(
    getMerchantCreated(merchantProp, systemPrinciple)
  );

  useEffect(() => {
    setMerchant(_merchant => {
      if (
        merchantProp.merchantAccountNumber ===
        _merchant?.modeledFrom?.merchantAccountNumber
      )
        return _merchant;

      return getMerchantCreated(merchantProp, systemPrinciple);
    });
  }, [merchantProp, systemPrinciple]);

  const {
    aggregatorType,
    assessmentCode,
    discountMethodParticipation,
    inStorePayment,
    informationIndicator,
    merchantPaymentDescription,
    processFlag,
    sparkExclusionIndicator,
    transactionDecisionTableId,
    promotionDiscountRateTableId,
    TINType
  } = useSelectElementsForConfigProcessingMerchants();

  const {
    total,
    searchValue,
    gridData: dataView,
    onSearchChange,
    onResetToDefaultFilter
  } = usePagination(parameters, searchFields, undefined, 30);

  useEffect(() => {
    if (!show) return;

    onResetToDefaultFilter();
  }, [dispatch, onResetToDefaultFilter, show]);

  useEffect(() => {
    !searchValue && searchRef.current?.clear();
  }, [searchValue]);

  const handleChange = (field: keyof MerchantItem, isRefData?: boolean) => {
    const onChange = (e: any) => {
      const value: string = isRefData ? e.target?.value?.code : e.target?.value;

      setMerchant(_merchant => ({ ..._merchant, [field]: value }));
    };

    return onChange;
  };

  const formatPhone = useCallback(
    (value: string) => formatContactPhone(value)!,
    []
  );

  const formatTaxpayer = useCallback(
    (value: string) => formatTaxpayerId(value)!,
    []
  );

  useUnsavedChangeRegistry(
    {
      ...unsavedChangesProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CONFIGURE_PROCESSING_MERCHANTS__CONFIGURE_PARAMETERS__MERCHANT_DETAILS,
      priority: 1
    },
    [
      Object.keys(initialMerchant)
        .map(k => k as keyof MerchantItemCreated)
        .some(k => {
          if (['id', 'rowId', 'modeledFrom', 'sysPrin'].includes(k)) {
            return false;
          }

          return (initialMerchant as MerchantItemCreated)[k] !== merchant[k];
        })
    ]
  );

  const valueAccessor = useCallback(
    (data: Record<string, any>) => {
      const paramId = data.id as keyof MerchantItem;

      switch (paramId) {
        case 'merchantName': {
          return (
            <TextBox
              dataTestId="merchantDetails_merchantName"
              size="sm"
              placeholder={t('txt_enter_a_value')}
              value={merchant.merchantName || ''}
              onChange={handleChange('merchantName')}
              maxLength={30}
            />
          );
        }
        case 'phoneNumber': {
          return (
            <FormatTextBox
              dataTestId="merchantDetails_phoneNumber"
              pattern="^\d{0,13}$"
              size="sm"
              format={formatPhone}
              parse={convertStringToNumberOnly}
              placeholder={t('txt_enter_a_value')}
              value={merchant.phoneNumber || ''}
              onChange={handleChange('phoneNumber')}
            />
          );
        }
        case 'address': {
          return (
            <TextBox
              dataTestId="merchantDetails_address"
              size="sm"
              placeholder={t('txt_enter_a_value')}
              value={merchant.address || ''}
              onChange={handleChange('address')}
              maxLength={38}
            />
          );
        }
        case 'city': {
          return (
            <TextBox
              dataTestId="merchantDetails_city"
              size="sm"
              placeholder={t('txt_enter_a_value')}
              value={merchant.city || ''}
              onChange={handleChange('city')}
              maxLength={32}
            />
          );
        }
        case 'stateProvinceTerritory': {
          return (
            <TextBox
              dataTestId="merchantDetails_stateProvinceTerritory"
              size="sm"
              placeholder={t('txt_enter_a_value')}
              value={merchant.stateProvinceTerritory || ''}
              onChange={handleChange('stateProvinceTerritory')}
              maxLength={25}
            />
          );
        }
        case 'zipPostalCode': {
          return (
            <TextBox
              dataTestId="merchantDetails_zipPostalCode"
              size="sm"
              placeholder={t('txt_enter_a_value')}
              value={merchant.zipPostalCode || ''}
              onChange={handleChange('zipPostalCode')}
              maxLength={14}
            />
          );
        }
        case 'doingBusinessAs': {
          return (
            <FormatTextBox
              dataTestId="merchantDetails_doingBusinessAs"
              size="sm"
              pattern="^[0-9a-zA-Z ]+$"
              placeholder={t('txt_enter_a_value')}
              value={merchant.doingBusinessAs || ''}
              onChange={handleChange('doingBusinessAs')}
              maxLength={40}
            />
          );
        }
        case 'IRSName': {
          return (
            <TextBox
              dataTestId="merchantDetails_IRSName"
              size="sm"
              placeholder={t('txt_enter_a_value')}
              value={merchant.IRSName || ''}
              onChange={handleChange('IRSName')}
              maxLength={40}
            />
          );
        }
        case 'imprinterNumber': {
          return (
            <FormatTextBox
              dataTestId="merchantDetails_imprinterNumber"
              maxLength={3}
              size="sm"
              pattern="^\d{0,3}$"
              placeholder={t('txt_enter_a_value')}
              value={merchant.imprinterNumber}
              onChange={handleChange('imprinterNumber')}
            />
          );
        }
        case 'assessmentCode': {
          const value = assessmentCode.find(
            o => o.code === merchant.assessmentCode
          );

          return (
            <EnhanceDropdownList
              dataTestId="merchantDetails_assessmentCode"
              size="small"
              value={value}
              placeholder={t('txt_select_an_option')}
              onChange={handleChange('assessmentCode', true)}
              options={assessmentCode}
            />
          );
        }
        case 'informationIndicator': {
          const value = informationIndicator.find(
            o => o.code === merchant.informationIndicator
          );

          return (
            <EnhanceDropdownList
              dataTestId="merchantDetails_informationIndicator"
              size="small"
              value={value}
              placeholder={t('txt_select_an_option')}
              onChange={handleChange('informationIndicator', true)}
              options={informationIndicator}
            />
          );
        }
        case 'merchantCustomerServicePhone': {
          return (
            <FormatTextBox
              dataTestId="merchantDetails_merchantCustomerServicePhone"
              pattern="^\d{0,14}$"
              size="sm"
              format={formatPhone}
              parse={convertStringToNumberOnly}
              placeholder={t('txt_enter_a_value')}
              value={merchant.merchantCustomerServicePhone || ''}
              onChange={handleChange('merchantCustomerServicePhone')}
            />
          );
        }
        case 'aggregatorType': {
          const value = aggregatorType.find(
            o => o.code === merchant.aggregatorType
          );
          return (
            <TextBox
              dataTestId="merchantDetails_aggregatorType"
              size="sm"
              readOnly
              value={value?.text}
              onChange={handleChange('aggregatorType')}
            />
          );
        }
        case 'TINType': {
          const value = TINType.find(o => o.code === merchant.TINType);

          return (
            <EnhanceDropdownList
              dataTestId="merchantDetails_TINType"
              size="small"
              value={value}
              placeholder={t('txt_select_an_option')}
              onChange={handleChange('TINType', true)}
              options={TINType}
            />
          );
        }
        case 'taxpayerId': {
          return (
            <FormatTextBox
              dataTestId="merchantDetails_taxpayerId"
              size="sm"
              pattern="^\d{0,10}$"
              format={formatTaxpayer}
              parse={convertStringToNumberOnly}
              placeholder={t('txt_enter_a_value')}
              value={merchant.taxpayerId}
              onChange={handleChange('taxpayerId')}
            />
          );
        }
        case 'sparkExclusionIndicator': {
          const value = sparkExclusionIndicator.find(
            o => o.code === merchant.sparkExclusionIndicator
          );

          return (
            <EnhanceDropdownList
              dataTestId="merchantDetails_sparkExclusionIndicator"
              size="small"
              value={value}
              placeholder={t('txt_select_an_option')}
              onChange={handleChange('sparkExclusionIndicator', true)}
              options={sparkExclusionIndicator}
            />
          );
        }
        case 'transactionDecisionTableId': {
          const value = transactionDecisionTableId.find(
            o => o.code === merchant.transactionDecisionTableId
          );

          return (
            <ComboBox
              textField="text"
              value={value}
              size="small"
              placeholder={t('txt_select_an_option')}
              onChange={handleChange('transactionDecisionTableId', true)}
              noResult={t('txt_no_results_found_without_dot')}
            >
              {transactionDecisionTableId.map(item => (
                <ComboBox.Item key={item.code} value={item} label={item.text} />
              ))}
            </ComboBox>
          );
        }
        case 'promotionDiscountRateTableId': {
          const value = promotionDiscountRateTableId.find(
            o => o.code === merchant.promotionDiscountRateTableId
          );

          return (
            <ComboBox
              textField="text"
              value={value}
              size="small"
              placeholder={t('txt_select_an_option')}
              onChange={handleChange('promotionDiscountRateTableId', true)}
              noResult={t('txt_no_results_found_without_dot')}
            >
              {promotionDiscountRateTableId.map(item => (
                <ComboBox.Item key={item.code} value={item} label={item.text} />
              ))}
            </ComboBox>
          );
        }
        case 'inStorePayment': {
          const value = inStorePayment.find(
            o => o.code === merchant.inStorePayment
          );

          return (
            <EnhanceDropdownList
              dataTestId="merchantDetails_inStorePayment"
              size="small"
              value={value}
              placeholder={t('txt_select_an_option')}
              onChange={handleChange('inStorePayment', true)}
              options={inStorePayment}
            />
          );
        }
        case 'merchantPaymentDescription': {
          const value = merchantPaymentDescription.find(
            o => o.code === merchant.merchantPaymentDescription
          );

          return (
            <EnhanceDropdownList
              dataTestId="merchantDetails_merchantPaymentDescription"
              size="small"
              value={value}
              placeholder={t('txt_select_an_option')}
              onChange={handleChange('merchantPaymentDescription', true)}
              options={merchantPaymentDescription}
            />
          );
        }
        case 'businessCategoryCode': {
          return (
            <FormatTextBox
              dataTestId="merchantDetails_businessCategoryCode"
              size="sm"
              pattern="^\d{0,4}$"
              placeholder={t('txt_enter_a_value')}
              value={merchant.businessCategoryCode || ''}
              onChange={handleChange('businessCategoryCode')}
              maxLength={4}
            />
          );
        }
        case 'processFlag': {
          const value = processFlag.find(o => o.code === merchant.processFlag);

          return (
            <EnhanceDropdownList
              dataTestId="merchantDetails_processFlag"
              size="small"
              value={value}
              placeholder={t('txt_select_an_option')}
              onChange={handleChange('processFlag', true)}
              options={processFlag}
            />
          );
        }
        case 'discountMethodParticipation': {
          const value = discountMethodParticipation.find(
            o => o.code === merchant.discountMethodParticipation
          );

          return (
            <EnhanceDropdownList
              dataTestId="merchantDetails_discountMethodParticipation"
              size="small"
              value={value}
              placeholder={t('txt_select_an_option')}
              onChange={handleChange('discountMethodParticipation', true)}
              options={discountMethodParticipation}
            />
          );
        }
      }
    },
    [
      formatPhone,
      formatTaxpayer,
      t,
      merchant,
      aggregatorType,
      assessmentCode,
      discountMethodParticipation,
      inStorePayment,
      informationIndicator,
      merchantPaymentDescription,
      processFlag,
      sparkExclusionIndicator,
      transactionDecisionTableId,
      promotionDiscountRateTableId,
      TINType
    ]
  );

  const columns: ColumnType[] = useMemo(
    () => [
      {
        id: 'bussinessName',
        Header: t('txt_business_name'),
        accessor: 'bussinessName',
        cellBodyProps: { className: 'pt-12 pb-8' }
      },
      {
        id: 'greenScreenName',
        Header: t('txt_config_processing_merchants_green_screen_name'),
        accessor: 'greenScreenName',
        cellBodyProps: { className: 'pt-12 pb-8' }
      },
      {
        id: 'id',
        Header: t('txt_value'),
        accessor: valueAccessor,
        cellBodyProps: { className: 'py-8' },
        width: width < 1280 ? 240 : undefined
      },
      {
        id: 'moreInfo',
        Header: t('txt_more_info'),
        className: 'text-center',
        accessor: viewMoreInfo(['moreInfo'], t),
        width: 105,
        cellBodyProps: { className: 'pt-12 pb-8' }
      }
    ],
    [t, valueAccessor, width]
  );

  const handleBack = () => {
    isFunction(onBack) && onBack();
  };

  const handleSave = () => {
    isFunction(onSave) && onSave(merchant!);
  };

  return (
    <ModalRegistry
      id="WorkflowSetup_ConfigureProcessingMerchants_MerchantDetailsModal"
      lg
      show={show}
      onAutoClosedAll={onCancel}
    >
      <ModalHeader border closeButton onHide={onCancel}>
        <ModalTitle>
          {t('txt_config_processing_merchants_merchant_details')}
        </ModalTitle>
      </ModalHeader>
      <ModalBody>
        <div className="d-flex justify-content-between align-items-center">
          <h5>{t('txt_parameter_list')}</h5>
          <SimpleSearch
            ref={searchRef}
            placeholder={t('txt_type_to_search')}
            onSearch={onSearchChange}
            popperElement={
              <>
                <p className="color-grey">{t('txt_search_description')}</p>
                <ul className="search-field-item list-unstyled">
                  <li className="mt-16">{t('txt_business_name')}</li>
                  <li className="mt-16">
                    {t('txt_config_processing_merchants_green_screen_name')}
                  </li>
                  <li className="mt-16">{t('txt_more_info')}</li>
                </ul>
              </>
            }
          />
        </div>
        {searchValue && total > 0 && (
          <div className="mt-16 d-flex justify-content-end">
            <ClearAndResetButton
              small
              onClearAndReset={onResetToDefaultFilter}
            />
          </div>
        )}
        <InlineMessage className="mt-16" variant="warning" withIcon>
          {t('txt_config_processing_merchants_merchant_detail_warning')}
        </InlineMessage>
        {total === 0 && (
          <div className="d-flex flex-column justify-content-center mt-40">
            <NoDataFound
              id="merchant-detail-NoDataFound"
              title={t('txt_config_processing_merchants_no_merchants')}
              hasSearch={!!searchValue}
              linkTitle={!!searchValue && t('txt_clear_and_reset')}
              onLinkClicked={onResetToDefaultFilter}
            />
          </div>
        )}
        {total > 0 && <Grid data={dataView} columns={columns} />}
      </ModalBody>
      <ModalFooter
        cancelButtonText={t('txt_cancel')}
        okButtonText={t('txt_save')}
        onCancel={onCancel}
        onOk={handleSave}
      >
        <Button
          variant="outline-primary"
          className="mr-auto ml-n8"
          onClick={handleBack}
        >
          {t('txt_back')}
        </Button>
      </ModalFooter>
    </ModalRegistry>
  );
};

export default MerchantDetailsModal;
