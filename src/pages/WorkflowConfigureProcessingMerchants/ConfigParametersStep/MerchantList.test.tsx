import { RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { mockUseModalRegistryFnc, renderWithMockStore } from 'app/utils';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowConfigureProcessingMerchants';
import React from 'react';
import MerchantList, { MerchantListProps } from './MerchantList';
import { mockElements } from './SelectMerchantModal.test';

const useSelectElementMetadata = jest.spyOn(
  WorkflowSetup,
  'useSelectElementsForConfigProcessingMerchants'
);
jest.mock('app/_libraries/_dls/components/i18next/Trans', () => {
  return {
    __esModule: true,
    default: ({ keyTranslation }: any) => <div>{keyTranslation}</div>
  };
});
const mockUseModalRegistry = mockUseModalRegistryFnc();

const renderComponent = async (
  props: any,
  initialState?: any
): Promise<RenderResult> => {
  return await renderWithMockStore(<MerchantList {...props} />, {
    initialState
  });
};

describe('ConfigureProcessingMerchantsWorkflow > ConfigParametersStep > SelectMerchantModal', () => {
  beforeEach(() => {
    mockUseModalRegistry.mockReturnValue([true, false]);
  });

  const defaultState = {
    workflowSetup: {
      elementMetadata: { elements: mockElements },
      merchantData: { merchants: [] }
    }
  };

  it('should render no merchants', async () => {
    const props = {} as MerchantListProps;

    const { getByText } = await renderComponent(props, defaultState);

    expect(
      getByText('txt_config_processing_merchants_configure_merchants')
    ).toBeInTheDocument();
  });

  it('should render merchants list', async () => {
    useSelectElementMetadata.mockReturnValue({
      aggregatorType: [{ code: 'name', description: 'type' }],
      assessmentCode: [{ code: 'name', description: 'type' }],
      discountMethodParticipation: [{ code: 'name', description: 'type' }],
      inStorePayment: [{ code: 'name', description: 'type' }],
      informationIndicator: [{ code: 'name', description: 'type' }],
      merchantPaymentDescription: [{ code: 'name', description: 'type' }],
      processFlag: [{ code: 'name', description: 'type' }],
      sparkExclusionIndicator: [{ code: 'name', description: 'type' }],
      transactionDecisionTableId: [{ code: 'name', description: 'type' }],
      promotionDiscountRateTableId: [{ code: 'name', description: 'type' }],
      TINType: [{ code: 'name', description: 'type' }]
    } as any);
    const onDelete = jest.fn() as any;
    const onClickEdit = jest.fn() as any;
    const props = {
      total: 11,
      merchants: [{ rowId: '1', modeledFrom: { id: '1' } } as any],
      sort: { id: 'id' },
      expandedList: ['1'],
      onDelete,
      onClickEdit
    } as MerchantListProps;

    const { getByText, getAllByText } = await renderComponent(
      props,
      defaultState
    );

    userEvent.click(getAllByText('txt_edit')[0]);
    expect(onClickEdit).toBeCalled();

    userEvent.click(getAllByText('txt_delete')[0]);

    userEvent.click(
      document.body.querySelector('.modal-footer .btn.btn-danger')!
    );
    expect(onDelete).toBeCalled();

    userEvent.click(getAllByText('txt_delete')[0]);
    userEvent.click(getByText('txt_cancel'));

    expect(
      getByText('txt_config_processing_merchants_configure_merchants')
    ).toBeInTheDocument();
  });
});
