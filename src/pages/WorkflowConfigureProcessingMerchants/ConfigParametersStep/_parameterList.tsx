import React from 'react';
import { ParameterCode, ParameterName } from './type';

export interface Parameter {
  id: keyof MerchantItem;
  bussinessName: ParameterName;
  greenScreenName: string;
  moreInfoText?: string;
  moreInfo?: React.ReactNode;
}
export const parameters: Parameter[] = [
  {
    id: 'merchantName',
    bussinessName: ParameterName.MerchantName,
    greenScreenName: 'Mrch Name',
    moreInfoText:
      'The Merchant Name parameter provides the merchant’s name as it is to appear on the merchant plastic and cardholder statements. Only 25 of the 30 positions entered in the MRCH NAME field will appear in the DOING BUSINESS AS: BANKCARD field, which would then appear on the customer’s statement.',
    moreInfo:
      'The Merchant Name parameter provides the merchant’s name as it is to appear on the merchant plastic and cardholder statements. Only 25 of the 30 positions entered in the MRCH NAME field will appear in the DOING BUSINESS AS: BANKCARD field, which would then appear on the customer’s statement.'
  },
  {
    id: 'phoneNumber',
    bussinessName: ParameterName.PhoneNumber,
    greenScreenName: ParameterName.PhoneNumber,
    moreInfoText:
      'The Phone Number parameter identifies the merchant’s area code and telephone number',
    moreInfo:
      'The Phone Number parameter identifies the merchant’s area code and telephone number'
  },
  {
    id: 'address',
    bussinessName: ParameterName.Address,
    greenScreenName: 'Address 1',
    moreInfoText:
      'The Address parameter contains the first line of merchant address.',
    moreInfo:
      'The Address parameter contains the first line of merchant address.'
  },
  {
    id: 'city',
    bussinessName: ParameterName.City,
    greenScreenName: ParameterName.City,
    moreInfoText:
      'The City parameter contains the city of merchant mailing address.',
    moreInfo:
      'The City parameter contains the city of merchant mailing address.'
  },
  {
    id: 'stateProvinceTerritory',
    bussinessName: ParameterName.StateProvinceTerritory,
    greenScreenName: 'St/Prov/Trry',
    moreInfoText:
      'The State/Province/Territory parameter defines the merchant’s state, province or territory. The information you enter in this field determines the subdivision that appears on the @MNA screen.',
    moreInfo:
      'The State/Province/Territory parameter defines the merchant’s state, province or territory. The information you enter in this field determines the subdivision that appears on the @MNA screen.'
  },
  {
    id: 'zipPostalCode',
    bussinessName: ParameterName.ZipPostalCode,
    greenScreenName: 'ZIP/Postal',
    moreInfoText:
      'The ZIP/Postal Code parameter defines the ZIP code or postal code of the merchant’s mailing',
    moreInfo:
      'The ZIP/Postal Code parameter defines the ZIP code or postal code of the merchant’s mailing'
  },
  {
    id: 'doingBusinessAs',
    bussinessName: ParameterName.DBA,
    greenScreenName: 'Doing Business As: Bankcard',
    moreInfoText:
      'The Doing Business As (DBA) parameter defines the merchant’s description that is sent to an issuer. This field is divided into three parts. The first 25 positions identify the merchant name. The next 13 positions identify the merchant’s city The last 2 positions identify the merchant state or country code.',
    moreInfo: (
      <div>
        The Doing Business As (DBA) parameter defines the merchant’s description
        that is sent to an issuer. This field is divided into three parts.
        <ul>
          <li>The first 25 positions identify the merchant name.</li>
          <li>The next 13 positions identify the merchant’s city</li>
          <li>
            The last 2 positions identify the merchant state or country code.
          </li>
        </ul>
      </div>
    )
  },
  {
    id: 'IRSName',
    bussinessName: ParameterName.IRSName,
    greenScreenName: ParameterName.IRSName,
    moreInfoText:
      'The IRS Name parameter defines the name merchant uses when filing federal income tax returns.',
    moreInfo:
      'The IRS Name parameter defines the name merchant uses when filing federal income tax returns.'
  },
  {
    id: 'imprinterNumber',
    bussinessName: ParameterName.ImprinterNumber,
    greenScreenName: 'Imprntr Num',
    moreInfoText:
      'The Imprinter Number parameter defines the count of imprinters furnished to the merchant. Enter zeros in this field if you do not furnish an imprinter to the merchant.',
    moreInfo:
      'The Imprinter Number parameter defines the count of imprinters furnished to the merchant. Enter zeros in this field if you do not furnish an imprinter to the merchant.'
  },
  {
    id: 'assessmentCode',
    bussinessName: ParameterName.AssessmentCode,
    greenScreenName: 'Assmt Code',
    moreInfoText:
      'The Assessment Code parameter defines the type of merchant classification.',
    moreInfo:
      'The Assessment Code parameter defines the type of merchant classification.'
  },
  {
    id: 'informationIndicator',
    bussinessName: ParameterName.InformationIndicator,
    greenScreenName: 'Inf Ind',
    moreInfoText:
      'The Information Indicator parameter defines whether you can give the merchant’s telephone number to the customer.',
    moreInfo:
      'The Information Indicator parameter defines whether you can give the merchant’s telephone number to the customer.'
  },
  {
    id: 'merchantCustomerServicePhone',
    bussinessName: ParameterName.MerchantCustomerServicePhone,
    greenScreenName: 'Mrch CS Phone',
    moreInfoText:
      'The Merchant Customer Service Phone parameter defines the merchant customer service telephone number. You cannot enter all zeros, all ones, or all nines in this field.',
    moreInfo:
      'The Merchant Customer Service Phone parameter defines the merchant customer service telephone number. You cannot enter all zeros, all ones, or all nines in this field.'
  },
  {
    id: 'aggregatorType',
    bussinessName: ParameterName.AggregatorType,
    greenScreenName: 'Aggr',
    moreInfoText:
      'The Aggregator Type parameter defines the merchant’s aggregator type. Visa defines an aggregator as an Internet Payment Service Provider (IPSPs) and Payment Service Providers (PSPs).',
    moreInfo:
      'The Aggregator Type parameter defines the merchant’s aggregator type. Visa defines an aggregator as an Internet Payment Service Provider (IPSPs) and Payment Service Providers (PSPs).'
  },
  {
    id: 'TINType',
    bussinessName: ParameterName.TINType,
    greenScreenName: ParameterName.TINType,
    moreInfoText:
      'The TIN Type parameter defines the type of merchant tax reporting number entered in the TAXPAYER ID field',
    moreInfo:
      'The TIN Type parameter defines the type of merchant tax reporting number entered in the TAXPAYER ID field'
  },
  {
    id: 'taxpayerId',
    bussinessName: ParameterName.TaxpayerId,
    greenScreenName: ParameterName.TaxpayerId,
    moreInfoText:
      'The Taxpayer ID parameter define the identifier of the federal taxpayer identification number of the merchant',
    moreInfo:
      'The Taxpayer ID parameter define the identifier of the federal taxpayer identification number of the merchant'
  },
  {
    id: 'sparkExclusionIndicator',
    bussinessName: ParameterName.SparkExclusionIndicator,
    greenScreenName: 'Spark Exclusion Ind',
    moreInfoText:
      'The Spark Exclusion Indicator parameter indicates the factor that excludes the account from, or includes it in, the selected features of the merchant Internal Revenue Service (IRS) reporting process that are represented by the TIN Processing parameter setting in the Merchant Miscellaneous section (MP OC MM) of the Product Control File.',
    moreInfo:
      'The Spark Exclusion Indicator parameter indicates the factor that excludes the account from, or includes it in, the selected features of the merchant Internal Revenue Service (IRS) reporting process that are represented by the TIN Processing parameter setting in the Merchant Miscellaneous section (MP OC MM) of the Product Control File.'
  },
  {
    id: 'transactionDecisionTableId',
    bussinessName: ParameterName.TransactionDecisionTableId,
    greenScreenName: 'Tran Decision Tbl ID',
    moreInfoText:
      'Processing Merchants to be used in Transaction Level Processing decisioning may set the Transaction Decision Table ID to indicate the Retail Qualification (RQ) table to be used to assign promotional pricing (PL RT PC).',
    moreInfo:
      'Processing Merchants to be used in Transaction Level Processing decisioning may set the Transaction Decision Table ID to indicate the Retail Qualification (RQ) table to be used to assign promotional pricing (PL RT PC).'
  },
  {
    id: 'promotionDiscountRateTableId',
    bussinessName: ParameterName.PromotionDiscountRateTableId,
    greenScreenName: 'Promo Disc Rate Tbl ID',
    moreInfoText:
      'The Promotion Discount Rate Table ID parameter defines the identifier of the transaction table containing the alternate discount rates you want to use for a merchant.',
    moreInfo:
      'The Promotion Discount Rate Table ID parameter defines the identifier of the transaction table containing the alternate discount rates you want to use for a merchant.'
  },
  {
    id: 'inStorePayment',
    bussinessName: ParameterName.InStorePayment,
    greenScreenName: ParameterName.InStorePayment,
    moreInfoText:
      'Processing Merchants to be used with a Single-Message Right Time Payment should set the IN-STORE PAYMENT field to a S - Merchant accepts in-store payments. You can submit Right Time Payments using real-time methods such as the Right Time Payment Open Data Streams transactions, or APIs Warning: If you set the IN-STORE PAYMENT field to S - Processing Merchant accepts in-store payments, you are using the Single-Message Right Time Payment option and you do not need to submit a Monetary Tape Deposit Format (038) to post the Right-Time payment for a list of accounts. If you do submit Monetary Tape Deposit Format (038) when using this field, your accounts will contain duplicate Right Time Payments.',
    moreInfo: (
      <div>
        <p>
          Processing Merchants to be used with a Single-Message Right Time
          Payment should set the IN-STORE PAYMENT field to a S - Merchant
          accepts in-store payments. You can submit Right Time Payments using
          real-time methods such as the Right Time Payment Open Data Streams
          transactions, or APIs
        </p>
        <p className="mt-8">
          Warning: If you set the IN-STORE PAYMENT field to S - Processing
          Merchant accepts in-store payments, you are using the Single-Message
          Right Time Payment option and you do not need to submit a Monetary
          Tape Deposit Format (038) to post the Right-Time payment for a list of
          accounts. If you do submit Monetary Tape Deposit Format (038) when
          using this field, your accounts will contain duplicate Right Time
          Payments.
        </p>
      </div>
    )
  },
  {
    id: 'merchantPaymentDescription',
    bussinessName: ParameterName.MerchantPaymentDescription,
    greenScreenName: 'Merchant Payment Desc',
    moreInfoText:
      'The Merchant Payment Description parameter defines the message printed on cardholder statements for payments.',
    moreInfo:
      'The Merchant Payment Description parameter defines the message printed on cardholder statements for payments.'
  },
  {
    id: 'businessCategoryCode',
    bussinessName: ParameterName.BusinessCategoryCode,
    greenScreenName: 'Bus/Cat Code',
    moreInfoText:
      'The Business/Category Code parameter defines the merchant’s line of business.',
    moreInfo:
      'The Business/Category Code parameter defines the merchant’s line of business.'
  },
  {
    id: 'processFlag',
    bussinessName: ParameterName.ProcessFlag,
    greenScreenName: 'Process Flg',
    moreInfoText:
      'Code representing whether the merchant can deposit tickets and/or request authorizations for transactions for a card type',
    moreInfo:
      'Code representing whether the merchant can deposit tickets and/or request authorizations for transactions for a card type'
  },
  {
    id: 'discountMethodParticipation',
    bussinessName: ParameterName.DiscountMethodParticipation,
    greenScreenName: 'Dis Method/Participation',
    moreInfoText:
      'The Discount Method/Participation parameter represents the merchant discount method followed by the code representing whether the merchant is a participation merchant. The Discount Method portion of this field indicates the method for determining the merchant’s discount.',
    moreInfo:
      'The Discount Method/Participation parameter represents the merchant discount method followed by the code representing whether the merchant is a participation merchant. The Discount Method portion of this field indicates the method for determining the merchant’s discount.'
  }
];
export const selectMerchantParameters: Parameter[] = [
  {
    id: 'merchantAccountNumber',
    bussinessName: ParameterName.MerchantAccountNumber,
    greenScreenName: 'Mrch Num',
    moreInfoText: "Number of merchant's account.",
    moreInfo: "Number of merchant's account."
  },
  ...parameters
];

export const merchantPropToParameter: Record<string, string> = {
  merchantName: ParameterCode.MerchantName,
  phoneNumber: ParameterCode.PhoneNumber,
  address: ParameterCode.Address,
  city: ParameterCode.City,
  stateProvinceTerritory: ParameterCode.StateProvinceTerritory,
  zipPostalCode: ParameterCode.ZipPostalCode,
  doingBusinessAs: ParameterCode.DBA,
  IRSName: ParameterCode.IRSName,
  imprinterNumber: ParameterCode.ImprinterNumber,
  assessmentCode: ParameterCode.AssessmentCode,
  informationIndicator: ParameterCode.InformationIndicator,
  merchantCustomerServicePhone: ParameterCode.MerchantCustomerServicePhone,
  aggregatorType: ParameterCode.AggregatorType,
  TINType: ParameterCode.TINType,
  taxpayerId: ParameterCode.TaxpayerId,
  sparkExclusionIndicator: ParameterCode.SparkExclusionIndicator,
  transactionDecisionTableId: ParameterCode.TransactionDecisionTableId,
  promotionDiscountRateTableId: ParameterCode.PromotionDiscountRateTableId,
  inStorePayment: ParameterCode.InStorePayment,
  merchantPaymentDescription: ParameterCode.MerchantPaymentDescription,
  businessCategoryCode: ParameterCode.BusinessCategoryCode,
  processFlag: ParameterCode.ProcessFlag,
  discountMethodParticipation: ParameterCode.DiscountMethodParticipation
};
