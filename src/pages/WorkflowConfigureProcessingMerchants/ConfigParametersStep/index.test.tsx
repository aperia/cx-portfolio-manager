import { RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { renderWithMockStore } from 'app/utils';
import React from 'react';
import ConfigParametersStep, { FormConfigParameters } from './index';

jest.mock('./MerchantList', () => ({
  __esModule: true,
  default: ({
    onChangeExpandedList,
    onClickConfigureMerchant,
    onDelete,
    onClickEdit,
    onSortChange,
    onPageChange,
    onPageSizeChange
  }: any) => {
    return (
      <div>
        <div>MerchantList</div>
        <button onClick={onChangeExpandedList}>
          MerchantList__onChangeExpandedList
        </button>
        <button onClick={onClickConfigureMerchant}>
          MerchantList__onClickConfigureMerchant
        </button>
        <button onClick={onDelete}>MerchantList__onDelete</button>
        <button onClick={onClickEdit}>MerchantList__onClickEdit</button>
        <button onClick={onSortChange}>MerchantList__onSortChange</button>
        <button onClick={onPageChange}>MerchantList__onPageChange</button>
        <button onClick={onPageSizeChange}>
          MerchantList__onPageSizeChange
        </button>
      </div>
    );
  }
}));
jest.mock('./SelectSPModal', () => ({
  __esModule: true,
  default: ({ onCancel, onContinue }: any) => {
    return (
      <div>
        <div>SelectSPModal</div>
        <button onClick={onCancel}>SelectSPModal__onCancel</button>
        <button onClick={onContinue}>SelectSPModal__onContinue</button>
      </div>
    );
  }
}));
const mockSelectedMerchant = jest.fn();
jest.mock('./SelectMerchantModal', () => ({
  __esModule: true,
  default: ({ onCancel, onBack, onContinue }: any) => {
    return (
      <div>
        <div>SelectMerchantModal</div>
        <button onClick={() => onCancel()}>
          SelectMerchantModal__onCancel
        </button>
        <button onClick={onBack}>SelectMerchantModal__onBack</button>
        <button onClick={() => onContinue(mockSelectedMerchant())}>
          SelectMerchantModal__onContinue
        </button>
      </div>
    );
  }
}));
const mockMerchantDetail = jest.fn();
jest.mock('./MerchantDetailsModal', () => ({
  __esModule: true,
  default: ({ onCancel, onBack, onSave }: any) => {
    return (
      <div>
        <div>MerchantDetailsModal</div>
        <button onClick={() => onCancel({ shouldConfirm: true })}>
          MerchantDetailsModal__onCancel
        </button>
        <button onClick={onBack}>MerchantDetailsModal__onBack</button>
        <button onClick={() => onSave(mockMerchantDetail())}>
          MerchantDetailsModal__onSave
        </button>
      </div>
    );
  }
}));

const mockOnConfirmParams = jest.fn();
jest.mock('app/hooks/useUnsavedChangesRedirect', () => {
  return {
    __esModule: true,
    useUnsavedChangesRedirect:
      () =>
      ({ onConfirm, onDiscard, onCancel }: any) => {
        onDiscard && onDiscard();
        onCancel && onCancel();
        onConfirm && onConfirm(mockOnConfirmParams());
      }
  };
});

const renderComponent = async (props: any): Promise<RenderResult> => {
  return await renderWithMockStore(<ConfigParametersStep {...props} />);
};

describe('ConfigureProcessingMerchantsWorkflow > ConfigParametersStep > Index', () => {
  beforeEach(() => {
    mockSelectedMerchant.mockReturnValue({});
    mockMerchantDetail.mockReturnValue({});
  });

  it('should render', async () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: { merchants: [] as MerchantItemCreated[] },
      stepId: 'step1',
      selectedStep: 'step2',
      setFormValues: mockFn
    } as WorkflowSetupProps<FormConfigParameters>;

    const { getByText } = await renderComponent(props);

    userEvent.click(getByText('MerchantList__onClickConfigureMerchant'));
    userEvent.click(getByText('MerchantList__onDelete'));
    userEvent.click(getByText('MerchantList__onClickEdit'));

    userEvent.click(getByText('SelectSPModal__onContinue'));

    userEvent.click(getByText('SelectMerchantModal__onContinue'));
    userEvent.click(getByText('SelectMerchantModal__onBack'));

    userEvent.click(getByText('MerchantDetailsModal__onBack'));
    userEvent.click(getByText('SelectMerchantModal__onContinue'));
    userEvent.click(getByText('MerchantDetailsModal__onSave'));
    userEvent.click(getByText('SelectMerchantModal__onContinue'));
    userEvent.click(getByText('MerchantDetailsModal__onCancel'));

    userEvent.click(getByText('SelectMerchantModal__onCancel'));

    expect(
      getByText('txt_config_processing_merchants_step_desc')
    ).toBeInTheDocument();
  });

  it('should save', async () => {
    const mockFn = (values: any) => {};
    const props = {
      formValues: { isValid: true, merchants: [{}] as MerchantItemCreated[] },
      setFormValues: mockFn
    } as WorkflowSetupProps<FormConfigParameters>;

    const { getByText } = await renderComponent(props);

    userEvent.click(getByText('MerchantList__onClickConfigureMerchant'));
    userEvent.click(getByText('MerchantList__onDelete'));
    userEvent.click(getByText('MerchantList__onClickEdit'));

    userEvent.click(getByText('SelectSPModal__onContinue'));

    userEvent.click(getByText('SelectMerchantModal__onContinue'));
    userEvent.click(getByText('SelectMerchantModal__onBack'));

    userEvent.click(getByText('MerchantDetailsModal__onBack'));
    userEvent.click(getByText('MerchantDetailsModal__onSave'));

    expect(
      getByText('txt_config_processing_merchants_step_desc')
    ).toBeInTheDocument();
  });
});
