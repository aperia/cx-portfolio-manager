import FieldTooltip from 'app/components/FieldTooltip';
import ModalRegistry from 'app/components/ModalRegistry';
import { useFormValidations } from 'app/hooks';
import {
  ComboBox,
  DropdownBaseChangeEvent,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import { isFunction, orderBy } from 'lodash';
import React, { useEffect, useMemo, useState } from 'react';

type ValidateField = 'sys' | 'prin';
const validateField: Record<ValidateField, ValidateField> = {
  sys: 'sys',
  prin: 'prin'
};
export interface SelectSPModalProps {
  show?: boolean;
  resetValue?: string;
  systems: RefData[];
  principles: Record<string, RefData[]>;
  onCancel?: VoidFunction;
  onContinue?: (systemPrinciple: string) => void;
}

const SelectSPModal: React.FC<SelectSPModalProps> = ({
  show = true,
  resetValue,
  systems,
  principles,
  onCancel,
  onContinue
}) => {
  const { t } = useTranslation();

  const [selectedSystem, setSelectedSystem] = useState<RefData | undefined>();
  const [selectedPrinciple, setSelectedPrinciple] =
    useState<RefData | undefined>();

  useEffect(() => {
    if (resetValue) {
      const sysPrin = resetValue.split(' ');
      setSelectedSystem(systems.find(s => s.code === sysPrin[0]));
      setSelectedPrinciple(
        principles[sysPrin[0]]?.find(p => p.code === sysPrin?.[1])
      );
      return;
    }

    setSelectedSystem(undefined);
    setSelectedPrinciple(undefined);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [show, resetValue]);

  const systemWithPrinciples = useMemo(
    () =>
      orderBy(
        principles[selectedSystem?.code as string] || [],
        ['text'],
        ['asc']
      ),
    [principles, selectedSystem?.code]
  );

  const [resetTouch, setResetTouch] = useState(0);
  useEffect(() => {
    process.nextTick(() => {
      setResetTouch(t => t + 1);
    });
  }, [selectedSystem, selectedPrinciple]);

  const currentErrors = useMemo(
    () =>
      ({
        sys: !selectedSystem && {
          status: true,
          message: t('txt_required_validation', {
            fieldName: 'Sys'
          })
        }
      } as Record<ValidateField, IFormError>),
    [selectedSystem, t]
  );

  const [, errors, setTouched] = useFormValidations<ValidateField>(
    currentErrors,
    resetTouch
  );

  const handleContinue = () => {
    const system = selectedSystem!.code;
    const principle = selectedPrinciple?.code;

    const sp = `${system}${principle ? ` ${principle}` : ''}`;

    isFunction(onContinue) && onContinue(sp);
  };

  const handleChangeSystem = (e: DropdownBaseChangeEvent) => {
    setSelectedSystem(e.target.value);

    e.target.value !== selectedSystem && setSelectedPrinciple(undefined);
  };

  const handleChangePrinciple = (e: DropdownBaseChangeEvent) => {
    setSelectedPrinciple(e.target.value);
  };

  return (
    <ModalRegistry
      id="WorkflowSetup_ConfigureProcessingMerchants_SelectSPModal"
      sm
      show={show}
      onAutoClosedAll={onCancel}
    >
      <ModalHeader border closeButton onHide={onCancel}>
        <ModalTitle>
          {t('txt_config_processing_merchants_select_sys_prin')}
        </ModalTitle>
      </ModalHeader>
      <ModalBody>
        <div className="row">
          <div className="col-6">
            <FieldTooltip
              placement="right-start"
              element={
                <div>
                  <p className="pb-8">{t('txt_tooltip_sys')}</p>
                  <p>{t('txt_tooltip_sys_body')}</p>
                </div>
              }
            >
              <ComboBox
                id="SelectSPModal__workflow_sys"
                label={t('txt_spa_sys')}
                required
                textField="text"
                noResult={t('txt_no_results_found')}
                value={selectedSystem}
                onChange={handleChangeSystem}
                onFocus={setTouched(validateField.sys)}
                onBlur={setTouched(validateField.sys, true)}
                error={errors[validateField.sys]}
              >
                {systems.map(s => (
                  <ComboBox.Item key={s.code} value={s} label={s.text} />
                ))}
              </ComboBox>
            </FieldTooltip>
          </div>
          <div className="col-6">
            <FieldTooltip
              placement="right-start"
              element={
                <div>
                  <p className="pb-8">{t('txt_tooltip_prin')}</p>
                  <p>
                    {t('txt_config_processing_merchants_tooltip_prin_body')}
                  </p>
                </div>
              }
            >
              <ComboBox
                id="SelectSPModal__workflow_prin"
                label={t('txt_spa_prin')}
                textField="text"
                noResult={t('txt_no_results_found')}
                value={selectedPrinciple}
                onChange={handleChangePrinciple}
                disabled={!selectedSystem}
              >
                {systemWithPrinciples.map(p => (
                  <ComboBox.Item key={p.code} value={p} label={p.text} />
                ))}
              </ComboBox>
            </FieldTooltip>
          </div>
        </div>
      </ModalBody>
      <ModalFooter
        cancelButtonText={t('txt_cancel')}
        okButtonText={t('txt_continue')}
        onCancel={onCancel}
        onOk={handleContinue}
        disabledOk={!selectedSystem}
      />
    </ModalRegistry>
  );
};

export default SelectSPModal;
