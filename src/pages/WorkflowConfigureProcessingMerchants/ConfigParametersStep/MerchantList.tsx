import ConfirmationModal from 'app/components/ConfirmationModal';
import { GroupTextFormat } from 'app/constants/enums';
import { mapGridExpandCollapse } from 'app/helpers';
import {
  Button,
  ColumnType,
  Grid,
  Icon,
  SortType,
  TransDLS,
  useTranslation
} from 'app/_libraries/_dls';
import { isFunction } from 'lodash';
import { useSelectWindowDimension } from 'pages/_commons/redux/Common';
import { useSelectElementsForConfigProcessingMerchants } from 'pages/_commons/redux/WorkflowSetup';
import { formatText } from 'pages/_commons/Utils/formatGridField';
import Paging from 'pages/_commons/Utils/Paging';
import React, { useCallback, useMemo, useState } from 'react';
import MerchantListSubGrid from './MerchantListSubGrid';

export interface MerchantListProps {
  total: number;
  sort: SortType;
  currentPage: number;
  currentPageSize: number;
  expandedList: string[];
  merchants?: MerchantItem[];
  onSortChange?: (sort: SortType) => void;
  onPageChange?: (pageSelected: number) => void;
  onPageSizeChange?: (size: number) => void;
  onChangeExpandedList: (expandList: string[]) => void;
  onClickConfigureMerchant?: VoidFunction;
  onClickEdit?: (item: MerchantItem) => void;
  onDelete?: (item: MerchantItem) => void;
}

const MerchantList: React.FC<MerchantListProps> = ({
  total,
  sort,
  currentPage,
  currentPageSize,
  expandedList,
  merchants = [],
  onSortChange,
  onPageChange,
  onPageSizeChange,
  onChangeExpandedList,
  onClickConfigureMerchant,
  onClickEdit,
  onDelete
}) => {
  const { t } = useTranslation();
  const { width } = useSelectWindowDimension();

  const merchantElements = useSelectElementsForConfigProcessingMerchants();

  const [merchantDeletion, setMerchantDeletion] = useState<MerchantItem>();

  const actionAccessor = useCallback(
    (data: Record<string, any>) => {
      const handleClickDelete = () => {
        setMerchantDeletion(data as MerchantItem);
      };

      const handleClickEdit = () => {
        isFunction(onClickEdit) && onClickEdit(data as MerchantItem);
      };

      return (
        <div className="d-flex justify-content-center">
          <Button size="sm" variant="outline-primary" onClick={handleClickEdit}>
            {t('txt_edit')}
          </Button>
          <Button
            size="sm"
            variant="outline-danger"
            className="ml-8"
            onClick={handleClickDelete}
          >
            {t('txt_delete')}
          </Button>
        </div>
      );
    },
    [t, onClickEdit]
  );

  const columns = useMemo<ColumnType[]>(
    () => [
      {
        id: 'sysPrin',
        Header: t('txt_config_processing_merchants_sysPrin'),
        accessor: 'sysPrin',
        isSort: true,
        width: width < 1280 ? 152 : undefined
      },
      {
        id: 'merchantName',
        Header: t('txt_config_processing_merchants_merchantName'),
        accessor: 'merchantName',
        isSort: true,
        width: width < 1280 ? 157 : undefined
      },
      {
        id: 'modeledFrom.merchantName',
        Header: t('txt_modeled_from'),
        accessor: 'modeledFrom.merchantName',
        isSort: true,
        width: width < 1280 ? 157 : undefined
      },
      {
        id: 'phoneNumber',
        Header: t('txt_config_processing_merchants_phoneNumber'),
        accessor: formatText(['phoneNumber'], {
          format: GroupTextFormat.PhoneNumber
        }),
        width: width < 1280 ? 142 : undefined
      },
      {
        id: 'taxpayerId',
        Header: t('txt_config_processing_merchants_taxpayerId'),
        accessor: formatText(['taxpayerId'], {
          format: GroupTextFormat.TaxpayerId
        }),
        width: width < 1280 ? 120 : undefined
      },
      {
        id: 'actions',
        Header: t('txt_actions'),
        accessor: actionAccessor,
        className: 'text-center',
        cellBodyProps: { className: 'py-8' },
        width: 133
      }
    ],
    [t, actionAccessor, width]
  );

  const renderSubRow = useCallback(
    ({ original }: { original: MerchantItemCreated }) => (
      <MerchantListSubGrid original={original} metadata={merchantElements} />
    ),
    [merchantElements]
  );

  const handleRemoveMerchant = () => {
    isFunction(onDelete) && onDelete(merchantDeletion!);
    setMerchantDeletion(undefined);
  };

  if (merchants.length <= 0)
    return (
      <div className="row mt-16">
        <div className="col-6 col-xl-4">
          <div
            className="rcc-btn d-flex justify-content-between border rounded-lg py-10"
            onClick={onClickConfigureMerchant}
          >
            <span className="d-flex align-items-center ml-16">
              <Icon name="services" size="9x" className="color-grey-l16" />
              <span className="ml-12 mr-8">
                {t('txt_config_processing_merchants_configure_merchants')}
              </span>
            </span>
          </div>
        </div>
      </div>
    );

  return (
    <div className="mt-24 pt-24 border-top">
      <div className="d-flex justify-content-between align-items-center mb-16">
        <h5>{t('txt_config_processing_merchants_merchant_list')}</h5>
        <Button
          size="sm"
          variant="outline-primary"
          className="mr-n8"
          onClick={onClickConfigureMerchant}
        >
          {t('txt_config_processing_merchants_configure_merchants')}
        </Button>
      </div>
      <div>
        <Grid
          togglable
          dataItemKey="rowId"
          expandedItemKey="rowId"
          data={merchants}
          sortBy={[sort]}
          columns={columns}
          expandedList={expandedList}
          toggleButtonConfigList={merchants.map(
            mapGridExpandCollapse('rowId', t)
          )}
          onSortChange={onSortChange}
          subRow={renderSubRow}
          onExpand={onChangeExpandedList}
        />
        {total > 10 && (
          <div className="mt-16">
            <Paging
              page={currentPage}
              pageSize={currentPageSize}
              totalItem={total}
              onChangePage={onPageChange}
              onChangePageSize={onPageSizeChange}
            />
          </div>
        )}
      </div>
      {merchantDeletion && (
        <ConfirmationModal
          id="WorkflowSetup_ConfigureProcessingMerchants_DeleteMerchantModal"
          show
          modalTitle={t('txt_confirm_deletion')}
          modalContent={
            <TransDLS keyTranslation="txt_config_processing_merchants_confirm_delete_merchant_message">
              <strong>{{ merchantName: merchantDeletion.merchantName }}</strong>
            </TransDLS>
          }
          modalButtons={{
            cancelText: t('txt_cancel'),
            cancelVariant: 'secondary',
            confirmText: t('txt_delete'),
            confirmVariant: 'danger'
          }}
          onCancel={() => setMerchantDeletion(undefined)}
          onConfirm={handleRemoveMerchant}
        />
      )}
    </div>
  );
};

export default MerchantList;
