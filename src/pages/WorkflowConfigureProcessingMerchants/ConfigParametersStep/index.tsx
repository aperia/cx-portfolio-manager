import {
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { useUnsavedChangeRegistry, useUnsavedChangesRedirect } from 'app/hooks';
import { usePagination } from 'app/hooks/usePagination';
import { InlineMessage, useTranslation } from 'app/_libraries/_dls';
import { differenceWith } from 'lodash';
import {
  actionsWorkflowSetup,
  useSelectElementsForConfigProcessingMerchants
} from 'pages/_commons/redux/WorkflowSetup';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, { useEffect, useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import MerchantDetailsModal from './MerchantDetailsModal';
import MerchantList from './MerchantList';
import SelectMerchantModal from './SelectMerchantModal';
import SelectSPModal from './SelectSPModal';
import Summary from './Summary';
import { ParameterCode } from './type';
import parseFormValues from './_parseFormValues';

export interface FormConfigParameters {
  isValid?: boolean;
  merchants: MerchantItemCreated[];
}
const ConfigParametersStep: React.FC<WorkflowSetupProps<FormConfigParameters>> =
  ({ formValues, setFormValues, stepId, selectedStep, savedAt, isStuck }) => {
    const { t } = useTranslation();
    const dispatch = useDispatch();
    const redirect = useUnsavedChangesRedirect();

    const keepRef = useRef({ setFormValues });
    keepRef.current.setFormValues = setFormValues;

    const [initialValues, setInitialValues] = useState(formValues);
    useEffect(() => {
      setInitialValues(formValues);
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [savedAt]);

    const [isEdit, setIsEdit] = useState(false);
    const [showSelectSPModal, setShowSelectSPModal] = useState(false);
    const [showSelectMerchantModal, setShowSelectMerchantModal] =
      useState(false);
    const [showMerchantDetailModal, setShowMerchantDetailModal] =
      useState(false);
    const [selectedSysPrin, setSelectedSysPrin] = useState('');
    const [selectedMerchant, setSelectedMerchant] = useState<MerchantItem>();

    const [expandedList, setExpandedList] = useState<string[]>([]);

    const { systems, principles } =
      useSelectElementsForConfigProcessingMerchants();

    const {
      total,
      sort,
      currentPage,
      currentPageSize,
      gridData: merchants,
      onSortChange,
      onPageChange,
      onPageSizeChange,
      onResetToDefaultFilter
    } = usePagination(formValues.merchants, [], 'merchantName');

    useEffect(() => {
      if (stepId !== selectedStep) return;

      setExpandedList([]);
      onResetToDefaultFilter();
    }, [onResetToDefaultFilter, stepId, selectedStep]);

    useEffect(() => {
      dispatch(
        actionsWorkflowSetup.getWorkflowMetadata([
          ParameterCode.SystemPrinciple,

          ParameterCode.MerchantName,
          ParameterCode.PhoneNumber,
          ParameterCode.Address,
          ParameterCode.City,
          ParameterCode.StateProvinceTerritory,
          ParameterCode.ZipPostalCode,
          ParameterCode.DBA,
          ParameterCode.IRSName,
          ParameterCode.AssessmentCode,
          ParameterCode.InformationIndicator,
          ParameterCode.MerchantCustomerServicePhone,
          ParameterCode.AggregatorType,
          ParameterCode.TINType,
          ParameterCode.TaxpayerId,
          ParameterCode.SparkExclusionIndicator,
          ParameterCode.TransactionDecisionTableId,
          ParameterCode.PromotionDiscountRateTableId,
          ParameterCode.InStorePayment,
          ParameterCode.MerchantPaymentDescription,
          ParameterCode.ProcessFlag,
          ParameterCode.DiscountMethodParticipation
        ])
      );
    }, [dispatch]);

    useEffect(() => {
      const isValid = (formValues?.merchants?.length || 0) > 0;
      if (formValues.isValid === isValid) return;

      keepRef.current.setFormValues({ isValid });
    }, [formValues]);

    useUnsavedChangeRegistry(
      {
        ...unsavedExistedWorkflowSetupDataProps,
        formName:
          UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CONFIGURE_PROCESSING_MERCHANTS__CONFIGURE_PARAMETERS,
        priority: 1
      },
      [
        differenceWith(initialValues?.merchants, formValues?.merchants).length >
          0,
        differenceWith(formValues?.merchants, initialValues?.merchants).length >
          0
      ]
    );

    const handleCloseModal = ({ shouldConfirm = true } = {}) => {
      const onConfirm = () => {
        setShowSelectSPModal(false);

        setSelectedSysPrin('');
        setShowSelectMerchantModal(false);

        setSelectedMerchant(undefined);
        setShowMerchantDetailModal(false);
      };

      if (!shouldConfirm) {
        onConfirm();
        return;
      }

      redirect({
        onConfirm,
        formsWatcher: [
          UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CONFIGURE_PROCESSING_MERCHANTS__CONFIGURE_PARAMETERS__MERCHANT_DETAILS
        ]
      });
    };

    const handleClickConfigureMerchant = () => {
      setIsEdit(false);
      setShowSelectSPModal(true);
    };
    const handleSelectSP = (systemPrinciple: string) => {
      setShowSelectMerchantModal(true);
      setSelectedSysPrin(systemPrinciple);
    };

    const handleSelectMerchant = (merchant: MerchantItem) => {
      setShowMerchantDetailModal(true);
      setSelectedMerchant(merchant);
    };
    const handleBackFromSelectMerchantModal = () => {
      setShowSelectMerchantModal(false);

      setShowSelectSPModal(true);
    };

    const handleBackFromMerchantDetailModal = () => {
      setShowMerchantDetailModal(false);

      setShowSelectMerchantModal(true);

      setShowSelectSPModal(false);
    };
    const handleSaveMerchant = (_merchant: MerchantItemCreated) => {
      const editIndex = formValues.merchants.findIndex(
        m => m.rowId === _merchant.rowId
      );

      if (editIndex !== -1) {
        const newMerchants = [...formValues.merchants];
        newMerchants.splice(editIndex, 1, _merchant);
        setFormValues({ merchants: newMerchants });

        !expandedList.includes(_merchant.rowId!) &&
          setExpandedList(list => [...list, _merchant.rowId!]);
      } else {
        setFormValues({ merchants: [...formValues.merchants, _merchant] });
        setExpandedList([_merchant.rowId!]);
        onResetToDefaultFilter();
      }

      handleCloseModal({ shouldConfirm: false });
    };

    const handeDeleteMerchant = (_merchant: MerchantItem) => {
      const merchants = formValues.merchants.filter(
        m => m.rowId !== _merchant.rowId
      );

      setFormValues({ merchants });
    };

    const handleEditMerchant = (_merchant: MerchantItemCreated) => {
      setIsEdit(true);

      setShowSelectSPModal(true);
      setSelectedSysPrin(_merchant.sysPrin!);
      setShowSelectMerchantModal(true);

      setShowMerchantDetailModal(true);
      setSelectedMerchant(_merchant);
    };

    return (
      <>
        {isStuck && (
          <InlineMessage className="mb-16 mt-24" variant="danger" withIcon>
            {t('txt_step_stuck_move_forward_message')}
          </InlineMessage>
        )}
        <div className="mt-8">
          {t('txt_config_processing_merchants_step_desc')}
        </div>
        <MerchantList
          total={total}
          sort={sort}
          currentPage={currentPage}
          currentPageSize={currentPageSize}
          merchants={merchants}
          expandedList={expandedList}
          onChangeExpandedList={setExpandedList}
          onClickConfigureMerchant={handleClickConfigureMerchant}
          onDelete={handeDeleteMerchant}
          onClickEdit={handleEditMerchant}
          onSortChange={onSortChange}
          onPageChange={onPageChange}
          onPageSizeChange={onPageSizeChange}
        />

        <SelectSPModal
          show={showSelectSPModal}
          resetValue={selectedSysPrin}
          systems={systems}
          principles={principles}
          onCancel={handleCloseModal}
          onContinue={handleSelectSP}
        />

        <SelectMerchantModal
          show={showSelectMerchantModal}
          isEdit={isEdit}
          systemPrinciple={selectedSysPrin}
          merchant={selectedMerchant}
          onCancel={handleCloseModal}
          onBack={handleBackFromSelectMerchantModal}
          onContinue={handleSelectMerchant}
        />

        {selectedMerchant && (
          <MerchantDetailsModal
            show={showMerchantDetailModal}
            merchant={selectedMerchant}
            systemPrinciple={selectedSysPrin}
            onCancel={handleCloseModal}
            onBack={handleBackFromMerchantDetailModal}
            onSave={handleSaveMerchant}
          />
        )}
      </>
    );
  };

const ExtraStaticConfigParametersStep =
  ConfigParametersStep as WorkflowSetupStaticProp<FormConfigParameters>;

ExtraStaticConfigParametersStep.summaryComponent = Summary;
ExtraStaticConfigParametersStep.defaultValues = {
  isValid: false,
  merchants: []
};
ExtraStaticConfigParametersStep.parseFormValues = parseFormValues;

export default ExtraStaticConfigParametersStep;
