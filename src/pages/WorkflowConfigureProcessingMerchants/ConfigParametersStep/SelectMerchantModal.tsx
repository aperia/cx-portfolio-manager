import ModalRegistry from 'app/components/ModalRegistry';
import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch, { SimpleSearchPublic } from 'app/components/SimpleSearch';
import { GroupTextFormat } from 'app/constants/enums';
import { mapGridExpandCollapse } from 'app/helpers';
import { usePagination } from 'app/hooks/usePagination';
import {
  Button,
  ColumnType,
  Grid,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  TransDLS,
  useTranslation
} from 'app/_libraries/_dls';
import { isFunction } from 'lodash';
import {
  actionsWorkflowSetup,
  useExportMerchantsStatus,
  useMerchantList,
  useMerchantListStatus,
  useSelectElementsForConfigProcessingMerchants
} from 'pages/_commons/redux/WorkflowSetup';
import ClearAndResetButton from 'pages/_commons/Utils/ClearAndResetButton';
import { formatText } from 'pages/_commons/Utils/formatGridField';
import Paging from 'pages/_commons/Utils/Paging';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';
import SelectMerchantSubGrid from './SelectMerchantSubGrid';

export interface SelectMerchantModalProps {
  show?: boolean;
  isEdit: boolean;
  systemPrinciple: string;
  merchant?: MerchantItem;
  onCancel?: VoidFunction;
  onBack?: () => void;
  onContinue?: (merchant: MerchantItem) => void;
}

const searchFields = [
  'IRSName',
  'merchantAccountNumber',
  'merchantName',
  'phoneNumber',
  'transactionDecisionTableId'
];

const SelectMerchantModal: React.FC<SelectMerchantModalProps> = ({
  show = true,
  systemPrinciple,
  merchant,
  isEdit,
  onCancel,
  onContinue,
  onBack
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const searchRef = useRef<SimpleSearchPublic>(null);

  const merchantElements = useSelectElementsForConfigProcessingMerchants();
  const data = useMerchantList();
  const { loading, error } = useMerchantListStatus();
  const { loading: exportLoading } = useExportMerchantsStatus();
  const [selectedMerchant, setSelectedMerchant] = useState(
    merchant?.merchantAccountNumber || ''
  );
  const [expandedList, setExpandedList] = useState<string[]>([]);

  const {
    sort,
    total,
    currentPage,
    currentPageSize,
    searchValue,
    gridData: dataView,
    onSortChange,
    onPageChange,
    onPageSizeChange,
    onSearchChange,
    onResetToDefaultFilter
  } = usePagination(data, searchFields, 'merchantName');

  const handleClearAndReset = useCallback(
    ({ keepSort = true }: any = {}) => {
      onResetToDefaultFilter({ keepSort });
      setExpandedList([]);
      searchRef?.current?.clear();
    },
    [onResetToDefaultFilter]
  );

  // reset sort/filter and expand/collapse to default
  useEffect(() => {
    if (!show) return;

    handleClearAndReset({ keepSort: false });
  }, [handleClearAndReset, show]);

  useEffect(() => {
    setSelectedMerchant('');

    dispatch(
      actionsWorkflowSetup.getConfigureProcessingMerchantsList(systemPrinciple)
    );
  }, [dispatch, systemPrinciple]);

  useEffect(
    () => setSelectedMerchant(merchant?.merchantAccountNumber || ''),
    [merchant]
  );

  useEffect(() => {
    !searchValue && searchRef.current?.clear();
  }, [searchValue]);

  const columns = useMemo<ColumnType[]>(
    () => [
      {
        id: 'merchantAccountNumber',
        Header: t('txt_config_processing_merchants_merchantAccountNumber'),
        accessor: 'merchantAccountNumber',
        isSort: true
      },
      {
        id: 'merchantName',
        Header: t('txt_config_processing_merchants_merchantName'),
        accessor: 'merchantName',
        isSort: true
      },
      {
        id: 'phoneNumber',
        Header: t('txt_config_processing_merchants_phoneNumber'),
        accessor: formatText(['phoneNumber'], {
          format: GroupTextFormat.PhoneNumber
        })
      },
      {
        id: 'taxpayerId',
        Header: t('txt_config_processing_merchants_taxpayerId'),
        accessor: formatText(['taxpayerId'], {
          format: GroupTextFormat.TaxpayerId
        })
      }
    ],
    [t]
  );

  const handleAutoCloseModal = () => {
    handleClearAndReset({ keepSort: false });
  };

  const handleBack = () => {
    isFunction(onBack) && onBack();
  };

  const handleContinue = () => {
    const merchant = data.find(
      i => i.merchantAccountNumber === selectedMerchant
    );

    isFunction(onContinue) && onContinue(merchant!);
  };

  const handleSelectMerchant = (dataKeyList: string[]) => {
    setSelectedMerchant(dataKeyList[0]);
  };

  const handleExportMerchants = () => {
    dispatch(
      actionsWorkflowSetup.exportConfigureProcessingMerchants(systemPrinciple)
    );
  };

  const handleSearch = (value: string) => {
    onSearchChange(value);
    setExpandedList([]);
  };

  const renderSubRow = useCallback(
    ({ original }: { original: MerchantItem }) => (
      <SelectMerchantSubGrid original={original} metadata={merchantElements} />
    ),
    [merchantElements]
  );

  return (
    <ModalRegistry
      id="WorkflowSetup_ConfigureProcessingMerchants_SelectMerchantModal"
      lg
      show={show}
      loading={loading || exportLoading}
      onAutoClosedAll={onCancel}
      onAutoClosed={handleAutoCloseModal}
    >
      <ModalHeader border closeButton onHide={onCancel}>
        <ModalTitle>
          {t('txt_config_processing_merchants_merchant_list')}
        </ModalTitle>
      </ModalHeader>
      <ModalBody>
        <TransDLS keyTranslation="txt_config_processing_merchants_select_merchant_desc">
          <strong className="color-grey-d20" />
        </TransDLS>
        <div className="mt-24 d-flex justify-content-between align-items-center">
          <h5>{systemPrinciple}</h5>
          <SimpleSearch
            ref={searchRef}
            placeholder={t('txt_type_to_search')}
            onSearch={handleSearch}
            popperElement={
              <>
                <p className="color-grey">{t('txt_search_description')}</p>
                <ul className="search-field-item list-unstyled">
                  <li className="mt-16">
                    {t('txt_config_processing_merchants_irsName')}
                  </li>
                  <li className="mt-16">
                    {t('txt_config_processing_merchants_merchantAccountNumber')}
                  </li>
                  <li className="mt-16">
                    {t('txt_config_processing_merchants_merchantName')}
                  </li>
                  <li className="mt-16">
                    {t('txt_config_processing_merchants_phoneNumber')}
                  </li>
                  <li className="mt-16">
                    {t(
                      'txt_config_processing_merchants_transactionDecisionTableId'
                    )}
                  </li>
                </ul>
              </>
            }
          />
        </div>
        <div className="d-flex justify-content-end my-16 mr-n8">
          <Button
            variant="outline-primary"
            size="sm"
            onClick={handleExportMerchants}
          >
            {t('txt_config_processing_merchants_export_merchants')}
          </Button>
          {searchValue && total > 0 && (
            <div className="ml-8">
              <ClearAndResetButton
                small
                onClearAndReset={() => handleClearAndReset()}
              />
            </div>
          )}
        </div>
        {!loading && !error && total === 0 && (
          <div className="d-flex flex-column justify-content-center mt-40">
            <NoDataFound
              id="merchant-list-NoDataFound"
              title={t('txt_config_processing_merchants_no_merchants')}
              hasSearch={!!searchValue}
              linkTitle={!!searchValue && t('txt_clear_and_reset')}
              onLinkClicked={handleClearAndReset}
            />
          </div>
        )}
        {!loading && !error && total > 0 && (
          <Grid
            togglable
            dataItemKey="id"
            expandedItemKey="id"
            data={dataView}
            columns={columns}
            expandedList={expandedList}
            checkedList={[selectedMerchant]}
            sortBy={[sort]}
            onSortChange={onSortChange}
            toggleButtonConfigList={dataView.map(
              mapGridExpandCollapse('id', t)
            )}
            variant={{ id: 'id', type: 'radio' }}
            subRow={renderSubRow}
            onExpand={setExpandedList}
            onCheck={handleSelectMerchant}
          />
        )}
        {total > 10 && (
          <div className="mt-16">
            <Paging
              page={currentPage}
              pageSize={currentPageSize}
              totalItem={total}
              onChangePage={onPageChange}
              onChangePageSize={onPageSizeChange}
            />
          </div>
        )}
      </ModalBody>
      <ModalFooter
        cancelButtonText={t('txt_cancel')}
        okButtonText={t('txt_continue')}
        onCancel={onCancel}
        onOk={handleContinue}
        disabledOk={!selectedMerchant}
      >
        {!isEdit && (
          <Button
            variant="outline-primary"
            className="mr-auto ml-n8"
            onClick={handleBack}
          >
            {t('txt_back')}
          </Button>
        )}
      </ModalFooter>
    </ModalRegistry>
  );
};

export default SelectMerchantModal;
