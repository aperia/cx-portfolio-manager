import { RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import {
  mockThunkAction,
  mockUseModalRegistryFnc,
  renderWithMockStore
} from 'app/utils';
import * as CommonRedux from 'pages/_commons/redux/Common/select-hooks';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowConfigureProcessingMerchants';
import React from 'react';
import MerchantDetailsModal, {
  MerchantDetailsModalProps
} from './MerchantDetailsModal';
import { mockElements } from './SelectMerchantModal.test';
const mockUseModalRegistry = mockUseModalRegistryFnc();

const useSelectElementMetadata = jest.spyOn(
  WorkflowSetup,
  'useSelectElementsForConfigProcessingMerchants'
);

const mockWorkflowSetup = mockThunkAction(actionsWorkflowSetup);

const mockUseSelectWindowDimension = jest.spyOn(
  CommonRedux,
  'useSelectWindowDimension'
);
const mockUseSelectWindowDimensionImplementation = (width: number) => {
  mockUseSelectWindowDimension.mockImplementation(() => ({ width } as any));
};

const renderComponent = async (
  props: any,
  initialState?: any
): Promise<RenderResult> => {
  return await renderWithMockStore(<MerchantDetailsModal {...props} />, {
    initialState
  });
};

describe('ConfigureProcessingMerchantsWorkflow > ConfigParametersStep > SelectMerchantModal', () => {
  beforeEach(() => {
    mockUseModalRegistry.mockReturnValue([true, false]);
    mockWorkflowSetup('getConfigureProcessingMerchantsList');
  });

  afterEach(() => {
    mockUseSelectWindowDimension.mockClear();
  });
  const defaultState = {
    workflowSetup: {
      elementMetadata: { elements: mockElements },
      merchantData: { merchants: [] }
    }
  };

  it('should render', async () => {
    useSelectElementMetadata.mockReturnValue({
      aggregatorType: [{ code: 'name', description: 'type' }],
      assessmentCode: [{ code: 'name', description: 'type' }],
      discountMethodParticipation: [{ code: 'name', description: 'type' }],
      inStorePayment: [{ code: 'name', description: 'type' }],
      informationIndicator: [{ code: 'name', description: 'type' }],
      merchantPaymentDescription: [{ code: 'name', description: 'type' }],
      processFlag: [{ code: 'name', description: 'type' }],
      sparkExclusionIndicator: [{ code: 'name', description: 'type' }],
      transactionDecisionTableId: [{ code: 'name', description: 'type' }],
      promotionDiscountRateTableId: [{ code: 'name', description: 'type' }],
      TINType: [{ code: 'name', description: 'type' }]
    } as any);
    mockUseSelectWindowDimensionImplementation(1400);
    const onBack = jest.fn() as any;
    const onSave = jest.fn() as any;
    const props = {
      systemPrinciple: '000 001',
      merchant: {
        address: 'a',
        merchantAccountNumber: 'merchantAccountNumber'
      },
      onBack,
      onSave
    } as MerchantDetailsModalProps;

    const {
      getByText,
      getByPlaceholderText,
      getAllByPlaceholderText,
      getByTestId,
      rerender
    } = await renderComponent(props, defaultState);

    const phoneInput = getByTestId(
      'merchantDetails_phoneNumber_dls-text-box_input'
    );
    userEvent.type(phoneInput, '1');
    userEvent.type(phoneInput, '{delete}');

    const taxpayerIdInput = getByTestId(
      'merchantDetails_taxpayerId_dls-text-box_input'
    );
    userEvent.type(taxpayerIdInput, '1');
    userEvent.type(taxpayerIdInput, '{delete}');

    userEvent.type(getAllByPlaceholderText('txt_enter_a_value')[0], 'aa');
    userEvent.click(getAllByPlaceholderText('txt_select_an_option')[0]);

    // userEvent.click(getByText('TransactionDecisionTableId'));

    rerender(
      <MerchantDetailsModal
        {...props}
        merchant={{ modeledFrom: {} } as any}
        show={false}
      />
    );

    const searchBox = getByPlaceholderText('txt_type_to_search');
    userEvent.type(searchBox, 'aa');
    userEvent.click(
      searchBox.parentElement!.querySelector('.icon.icon-search')!
    );

    userEvent.click(getByText('txt_back'));
    expect(onBack).toHaveBeenCalled();
    userEvent.click(getByText('txt_save'));
    expect(onSave).toHaveBeenCalled();

    expect(
      getByText('txt_config_processing_merchants_merchant_details')
    ).toBeInTheDocument();
  });
  it('should render with different dimension', async () => {
    mockUseSelectWindowDimensionImplementation(900);
    useSelectElementMetadata.mockReturnValue({
      aggregatorType: [{ code: 'name', description: 'type' }],
      assessmentCode: [{ code: 'name', description: 'type' }],
      discountMethodParticipation: [{ code: 'name', description: 'type' }],
      inStorePayment: [{ code: 'name', description: 'type' }],
      informationIndicator: [{ code: 'name', description: 'type' }],
      merchantPaymentDescription: [{ code: 'name', description: 'type' }],
      processFlag: [{ code: 'name', description: 'type' }],
      sparkExclusionIndicator: [{ code: 'name', description: 'type' }],
      transactionDecisionTableId: [{ code: 'name', description: 'type' }],
      promotionDiscountRateTableId: [{ code: 'name', description: 'type' }],
      TINType: [{ code: 'name', description: 'type' }]
    } as any);
    const onBack = jest.fn() as any;
    const onSave = jest.fn() as any;
    const props = {
      systemPrinciple: '000 001',
      merchant: {
        address: 'a',
        merchantAccountNumber: 'merchantAccountNumber'
      },
      onBack,
      onSave
    } as MerchantDetailsModalProps;

    const {
      getByText,
      getByPlaceholderText,
      getAllByPlaceholderText,
      getByTestId,
      rerender
    } = await renderComponent(props, defaultState);

    const phoneInput = getByTestId(
      'merchantDetails_phoneNumber_dls-text-box_input'
    );
    userEvent.type(phoneInput, '1');
    userEvent.type(phoneInput, '{delete}');

    const taxpayerIdInput = getByTestId(
      'merchantDetails_taxpayerId_dls-text-box_input'
    );
    userEvent.type(taxpayerIdInput, '1');
    userEvent.type(taxpayerIdInput, '{delete}');

    userEvent.type(getAllByPlaceholderText('txt_enter_a_value')[0], 'aa');
    userEvent.click(getAllByPlaceholderText('txt_select_an_option')[0]);

    rerender(
      <MerchantDetailsModal
        {...props}
        merchant={{ modeledFrom: {} } as any}
        show={false}
      />
    );

    const searchBox = getByPlaceholderText('txt_type_to_search');
    userEvent.type(searchBox, 'aa');
    userEvent.click(
      searchBox.parentElement!.querySelector('.icon.icon-search')!
    );

    userEvent.click(getByText('txt_back'));
    expect(onBack).toHaveBeenCalled();
    userEvent.click(getByText('txt_save'));
    expect(onSave).toHaveBeenCalled();

    expect(
      getByText('txt_config_processing_merchants_merchant_details')
    ).toBeInTheDocument();
  });
});
