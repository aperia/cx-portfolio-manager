import { merchantPropToParameter } from './_parameterList';

export const parseMerchantsPropToParameter = (
  merchants: MerchantItemCreated[]
) => {
  return merchants.map(merchant => ({
    id: merchant.rowId,
    sysPrin: merchant.sysPrin,
    parameters: Object.keys(merchantPropToParameter).map(prop => ({
      name: merchantPropToParameter[prop],
      previousValue: merchant?.modeledFrom?.[prop as keyof MerchantItem],
      newValue: merchant?.[prop as keyof MerchantItem]
    }))
  }));
};

const parseMerchantParameterToProp = (
  parameters: MagicKeyValue[],
  parseK = 'newValue'
) => {
  return Object.keys(merchantPropToParameter).reduce(
    (pre, curr) => ({
      ...pre,
      [curr]: parameters?.find(p => p.name === merchantPropToParameter[curr])?.[
        parseK
      ]
    }),
    {} as MagicKeyValue
  );
};

export const parseMerchantsParameterToProp = (merchants: MagicKeyValue[]) => {
  return merchants.map(
    merchant =>
      ({
        ...parseMerchantParameterToProp(merchant.parameters),
        sysPrin: merchant.sysPrin,
        rowId: merchant.id,
        modeledFrom: {
          ...parseMerchantParameterToProp(merchant.parameters, 'previousValue')
        }
      } as MerchantItemCreated)
  );
};
