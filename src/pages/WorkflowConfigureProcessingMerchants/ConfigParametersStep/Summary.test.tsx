import { fireEvent, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { renderWithMockStore } from 'app/utils';
import React from 'react';
import Summary from './Summary';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});
jest.mock('./MerchantListSubGrid', () => ({
  __esModule: true,
  default: ({}: any) => {
    return (
      <div>
        <div>MerchantListSubGrid</div>
      </div>
    );
  }
}));

const renderComponent = async (props: any): Promise<RenderResult> => {
  return await renderWithMockStore(<Summary {...props} />);
};

describe('ConfigureProcessingMerchantsWorkflow > ConfigParametersStep > Summary', () => {
  it('should render summary with empty merchants', async () => {
    const { queryByText } = await renderComponent({
      formValues: {
        merchants: [] as MerchantItemCreated[]
      },
      stepId: 'step1',
      selectedStep: 'step2'
    } as WorkflowSetupSummaryProps);

    expect(
      queryByText('txt_config_processing_merchants_no_update_merchants')
    ).toBeInTheDocument();
  });
  it('should render summary with list of merchant', async () => {
    const { queryByText, container } = await renderComponent({
      formValues: {
        merchants: [{}] as MerchantItemCreated[]
      }
    } as WorkflowSetupSummaryProps);

    userEvent.click(
      container.querySelector('.btn.btn-icon-secondary .icon.icon-plus')!
    );

    expect(
      queryByText('txt_config_processing_merchants_no_update_merchants')
    ).not.toBeInTheDocument();
  });
  it('should render summary with pagination', async () => {
    const { queryByText } = await renderComponent({
      formValues: {
        merchants: [
          {},
          {},
          {},
          {},
          {},
          {},
          {},
          {},
          {},
          {},
          {}
        ] as MerchantItemCreated[]
      }
    } as WorkflowSetupSummaryProps);

    expect(
      queryByText('txt_config_processing_merchants_no_update_merchants')
    ).not.toBeInTheDocument();
  });

  it('should click on edit', async () => {
    const mockFn = jest.fn();
    const wrapper = await renderComponent({
      formValues: { merchants: [] as MerchantItemCreated[] },
      onEditStep: mockFn
    } as WorkflowSetupSummaryProps);

    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(mockFn).toHaveBeenCalled();
  });
});
