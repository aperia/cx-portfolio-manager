import {
  convertRefValue,
  formatContactPhone,
  formatTaxpayerId
} from 'app/helpers';
import {
  ColumnType,
  Grid,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import { formatText, viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React, { useCallback, useMemo } from 'react';
import { parameters } from './_parameterList';

export interface MerchantListSubGridProps {
  original: MerchantItemCreated;
  metadata: {
    assessmentCode: RefData[];
    informationIndicator: RefData[];
    aggregatorType: RefData[];
    TINType: RefData[];
    sparkExclusionIndicator: RefData[];
    transactionDecisionTableId: RefData[];
    promotionDiscountRateTableId: RefData[];
    inStorePayment: RefData[];
    merchantPaymentDescription: RefData[];
    processFlag: RefData[];
    discountMethodParticipation: RefData[];
  };
}
const MerchantListSubGrid: React.FC<MerchantListSubGridProps> = ({
  original,
  metadata
}) => {
  const { t } = useTranslation();

  const getValue = useCallback(
    (item: MerchantItem): Record<keyof MerchantItem, string | undefined> => {
      const {
        aggregatorType,
        assessmentCode,
        discountMethodParticipation,
        inStorePayment,
        informationIndicator,
        merchantPaymentDescription,
        processFlag,
        sparkExclusionIndicator,
        transactionDecisionTableId,
        promotionDiscountRateTableId,
        TINType
      } = metadata;

      return {
        ...item,
        phoneNumber: formatContactPhone(item.phoneNumber),
        merchantCustomerServicePhone: formatContactPhone(
          item.merchantCustomerServicePhone
        ),
        taxpayerId: formatTaxpayerId(item.taxpayerId),
        aggregatorType: convertRefValue(aggregatorType, item.aggregatorType),
        assessmentCode: convertRefValue(assessmentCode, item.assessmentCode),
        discountMethodParticipation: convertRefValue(
          discountMethodParticipation,
          item.discountMethodParticipation
        ),
        inStorePayment: convertRefValue(inStorePayment, item.inStorePayment),
        informationIndicator: convertRefValue(
          informationIndicator,
          item.informationIndicator
        ),
        merchantPaymentDescription: convertRefValue(
          merchantPaymentDescription,
          item.merchantPaymentDescription
        ),
        processFlag: convertRefValue(processFlag, item.processFlag),
        sparkExclusionIndicator: convertRefValue(
          sparkExclusionIndicator,
          item.sparkExclusionIndicator
        ),
        transactionDecisionTableId: convertRefValue(
          transactionDecisionTableId,
          item.transactionDecisionTableId
        ),
        promotionDiscountRateTableId: convertRefValue(
          promotionDiscountRateTableId,
          item.promotionDiscountRateTableId
        ),
        TINType: convertRefValue(TINType, item.TINType)
      };
    },
    [metadata]
  );

  const previousValue = useMemo(
    () => getValue(original.modeledFrom!),
    [getValue, original.modeledFrom]
  );
  const newValue = useMemo(() => getValue(original), [getValue, original]);

  const previousValueAccessor = useCallback(
    (data: Record<string, any>) => (
      <TruncateText
        lines={2}
        ellipsisLessText={t('txt_less')}
        ellipsisMoreText={t('txt_more')}
      >
        {previousValue[data.id as keyof MerchantItem]}
      </TruncateText>
    ),
    [t, previousValue]
  );

  const newValueAccessor = useCallback(
    (data: Record<string, any>) => (
      <TruncateText
        lines={2}
        ellipsisLessText={t('txt_less')}
        ellipsisMoreText={t('txt_more')}
      >
        {newValue[data.id as keyof MerchantItem]}
      </TruncateText>
    ),
    [t, newValue]
  );

  const columns: ColumnType[] = useMemo(
    () => [
      {
        id: 'bussinessName',
        Header: t('txt_business_name'),
        accessor: formatText(['bussinessName']),
        width: 180
      },
      {
        id: 'greenScreenName',
        Header: t('txt_config_processing_merchants_green_screen_name'),
        accessor: 'greenScreenName',
        width: 180
      },
      {
        id: 'modeledFrom.id',
        Header: t('txt_previous_value'),
        accessor: previousValueAccessor
      },
      {
        id: 'id',
        Header: t('txt_new_value'),
        accessor: newValueAccessor
      },
      {
        id: 'moreInfo',
        Header: t('txt_more_info'),
        className: 'text-center',
        accessor: viewMoreInfo(['moreInfo'], t),
        width: 105
      }
    ],
    [t, previousValueAccessor, newValueAccessor]
  );

  return (
    <div className="p-20 overflow-hidden">
      <Grid columns={columns} data={parameters} />
    </div>
  );
};

export default MerchantListSubGrid;
