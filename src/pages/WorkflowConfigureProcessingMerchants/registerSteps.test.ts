import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { default as stepRegistry } from '../_commons/WorkflowSetup/registries/stepRegistry';

const registerFunc = jest.spyOn(stepRegistry, 'registerStep');

describe('ConfigureProcessingMerchantsWorkflow > registerSteps', () => {
  it('Should register components', async () => {
    await import('./registerSteps');
    expect(registerFunc).toBeCalledWith(
      'ConfigParametersConfigureProcessingMerchants',
      expect.anything(),
      [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__CONFIGURE_PROCESSING_MERCHANTS__CONFIGURE_PARAMETERS
      ]
    );
  });
});
