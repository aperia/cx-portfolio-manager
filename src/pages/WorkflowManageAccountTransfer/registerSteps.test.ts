import { default as stepRegistry } from '../_commons/WorkflowSetup/registries/stepRegistry';

const registerFunc = jest.spyOn(stepRegistry, 'registerStep');

describe('ManageAccountTransferWorkflow > registerSteps', () => {
  it('Should register components', async () => {
    await import('./registerSteps');

    expect(registerFunc).toBeCalledWith(
      'GetStartedManageAccountTransferWorkflow',
      expect.anything()
    );
  });
});
