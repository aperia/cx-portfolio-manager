import {
  stepRegistry,
  uploadFileStepRegistry
} from '../_commons/WorkflowSetup/registries';
import GettingStartStep from './GettingStartStep';
import ParameterList from './UploadParameterList';

stepRegistry.registerStep(
  'GetStartedManageAccountTransferWorkflow',
  GettingStartStep
);

uploadFileStepRegistry.registerComponent(
  'ManageAccountTransferParameterList',
  ParameterList
);
