import { fireEvent, render } from '@testing-library/react';
import { mockThunkAction, mockUseDispatchFnc } from 'app/utils';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import React from 'react';
import ParameterList from './index';

const mockUseDispatch = mockUseDispatchFnc();
const mockActionsWorkflowSetup = mockThunkAction(actionsWorkflowSetup);
beforeEach(() => {
  mockUseDispatch.mockImplementation(() => jest.fn());
  mockActionsWorkflowSetup('getWorkflowMetadata');
});

afterEach(() => {
  mockUseDispatch.mockClear();
});

jest.mock('pages/_commons/redux/WorkflowSetup', () => ({
  actionsWorkflowSetup: { getWorkflowMetadata: jest.fn() },
  useSelectElementsBulkArchivePCF: () => ({
    methods: [
      {
        value: '',
        description: 'None',
        code: '',
        text: 'None'
      },
      {
        value: 'D',
        description:
          'Deny the item if it exceeds the amount in the LIMIT field.',
        code: 'D'
      },
      {
        value: 'Q',
        description:
          'Assign the item to the pending queue if it exceeds the amount in the LIMIT field.',
        code: 'Q',
        text: 'Q - Assign the item to the pending queue if it exceeds the amount in the LIMIT field.'
      }
    ]
  })
}));

jest.mock('pages/_commons/redux/WorkflowSetup/select-hooks', () => ({
  useSelectElementMetadataForManageAccounTransfer: () => ({
    transactions: [
      {
        value: '',
        description: 'None',
        code: '',
        text: 'None'
      },
      {
        value: 'D',
        description:
          'Deny the item if it exceeds the amount in the LIMIT field.',
        code: 'D'
      },
      {
        value: 'Q',
        description:
          'Assign the item to the pending queue if it exceeds the amount in the LIMIT field.',
        code: 'Q',
        text: 'Q - Assign the item to the pending queue if it exceeds the amount in the LIMIT field.'
      }
    ]
  })
}));

const factory = ({
  stepId,
  selectedStep
}: {
  stepId: string;
  selectedStep: string;
}) => {
  const { getByText, getByRole } = render(
    <ParameterList {...({ stepId, selectedStep } as never)} />
  );

  const triggerExpand = () => {
    fireEvent.click(getByRole('button'));
  };

  return {
    getTitle: () => getByText('txt_parameter_list'),
    triggerExpand,
    getGrid: () => getByText('txt_upload_file_parameter_list_desc')
  };
};

describe('test ParameterList component', () => {
  it('should be render', () => {
    const { getTitle } = factory({ stepId: '!id', selectedStep: 'id' });
    expect(getTitle()).toBeInTheDocument();
  });
  it('should be expand', () => {
    const { getGrid, triggerExpand } = factory({
      stepId: 'id',
      selectedStep: 'id'
    });
    triggerExpand();
    expect(getGrid()).toBeInTheDocument();
  });
});
