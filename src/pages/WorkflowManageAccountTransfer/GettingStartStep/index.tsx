import { WORKFLOW_SETUP } from 'app/constants/local-storage';
import {
  Button,
  Icon,
  InlineMessage,
  SimpleBar,
  TransDLS,
  useTranslation
} from 'app/_libraries/_dls';
import OverviewModal from 'pages/_commons/OverviewModal';
import GettingStartSummary from 'pages/_commons/WorkflowSetup/CommonSteps/GettingStartStep/GettingStartSummary';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, { useMemo, useRef, useState } from 'react';

export interface FormGettingStart {
  isValid?: boolean;
  alreadyShown?: boolean;
}

const PROCESSING_LIST = [
  'txt_manage_account_transfer_workflow__step1_left_CD-051',
  'txt_manage_account_transfer_workflow__step1_left_CD-051_1',
  'txt_manage_account_transfer_workflow__step1_left_CD-052',
  'txt_manage_account_transfer_workflow__step1_left_CD-052_1',
  'txt_manage_account_transfer_workflow__step1_left_CD-061',
  'txt_manage_account_transfer_workflow__step1_left_CD-061_1',
  'txt_manage_account_transfer_workflow__step1_left_CD-071',
  'txt_manage_account_transfer_workflow__step1_left_CD-071_1',
  'txt_manage_account_transfer_workflow__step1_left_CD-072',
  'txt_manage_account_transfer_workflow__step1_left_CD-072_1',
  'txt_manage_account_transfer_workflow__step1_left_CD-111-115',
  'txt_manage_account_transfer_workflow__step1_left_CD-111-115_1',
  'txt_manage_account_transfer_workflow__step1_left_CD-121',
  'txt_manage_account_transfer_workflow__step1_left_CD-121_1',
  'txt_manage_account_transfer_workflow__step1_left_CD-272',
  'txt_manage_account_transfer_workflow__step1_left_CD-272_1',
  'txt_manage_account_transfer_workflow__step1_left_CD-334',
  'txt_manage_account_transfer_workflow__step1_left_CD-334_1',
  'txt_manage_account_transfer_workflow__step1_left_CD-643',
  'txt_manage_account_transfer_workflow__step1_left_CD-643_1',
  'txt_manage_account_transfer_workflow__step1_left_CD-688',
  'txt_manage_account_transfer_workflow__step1_left_CD-688_1',
  'txt_manage_account_transfer_workflow__step1_left_CD-691',
  'txt_manage_account_transfer_workflow__step1_left_CD-691_1',
  'txt_manage_account_transfer_workflow__step1_left_CD-8484',
  'txt_manage_account_transfer_workflow__step1_left_CD-8484_1'
];

const GettingStartStep: React.FC<WorkflowSetupProps<FormGettingStart>> = ({
  title,
  formValues,
  setFormValues
}) => {
  const keepRef = useRef({ setFormValues });
  const { t } = useTranslation();
  keepRef.current.setFormValues = setFormValues;

  const [showOverview, setShowOverview] = useState(
    formValues.alreadyShown
      ? false
      : localStorage.getItem(WORKFLOW_SETUP.SHOW_OVERVIEW_AGAIN) !== 'false'
  );

  const viewListProcessing = useMemo(() => {
    const result = [];

    for (let i = 0; i < PROCESSING_LIST.length; i += 2) {
      const txt1 = PROCESSING_LIST[i];
      const txt2 = PROCESSING_LIST[i + 1];

      result.push(
        <li key={txt1} style={{ wordBreak: 'break-word' }} className="mt-8">
          <span>{t(txt1)}</span>
          <span className="d-block">{t(txt2)}</span>
        </li>
      );
    }

    return result;
  }, [t]);

  const headerView = useMemo(
    () => (
      <React.Fragment>
        <InlineMessage variant="warning" withIcon className="mb-24">
          {t('txt_manage_account_transfer_workflow__step1_warning')}
        </InlineMessage>
        <div className="d-flex align-items-center justify-content-between">
          <h4>{t(title)}</h4>
          <Button
            className="mr-n8"
            variant="outline-primary"
            size="sm"
            onClick={() => setShowOverview(true)}
          >
            {t('txt_view_overview')}
          </Button>
        </div>
      </React.Fragment>
    ),
    [t, title]
  );
  const leftContent = useMemo(
    () => (
      <div className="col-6 mt-24">
        <div className="bg-light-l20 rounded-lg p-16">
          <div className="text-center">
            <Icon name="request" size="12x" className="color-grey-l16" />
          </div>
          <p className="mt-8 color-grey">
            {t('txt_manage_account_transfer_workflow__step1_left_1')}
          </p>
          <p className="mt-16 fw-600">
            {t('txt_manage_account_transfer_workflow__step1_left_2')}
          </p>
          <p className="mt-8 color-grey">
            {t('txt_manage_account_transfer_workflow__step1_left_3')}{' '}
          </p>
          <ul className="list-dot color-grey">{viewListProcessing}</ul>
        </div>
      </div>
    ),
    [t, viewListProcessing]
  );

  const rightContent = useMemo(() => {
    return (
      <div className="col-6 mt-24 color-grey">
        <p>{t('txt_follow_these_steps_to_set_up_this_workflow')}</p>
        {[
          'txt_manage_account_transfer_workflow__step1_right_1',
          'txt_manage_account_transfer_workflow__step1_right_2',
          'txt_manage_account_transfer_workflow__step1_right_3',
          'txt_manage_account_transfer_workflow__step1_right_4',
          'txt_manage_account_transfer_workflow__step1_right_5'
        ].map(i => (
          <p key={i} className="mt-8">
            <TransDLS keyTranslation={i}>
              <strong className="color-grey-d20" />
            </TransDLS>
          </p>
        ))}
      </div>
    );
  }, [t]);

  return (
    <SimpleBar>
      <div className="p-24">
        {headerView}
        <div className="row">
          {leftContent}
          {rightContent}
        </div>
      </div>
      {showOverview && (
        <OverviewModal
          id="overviewWorkflowSetup"
          show
          onClose={() => {
            setShowOverview(false);
            keepRef.current.setFormValues({ alreadyShown: true });
          }}
        />
      )}
    </SimpleBar>
  );
};

const ExtraStaticGettingStartStep =
  GettingStartStep as WorkflowSetupStaticProp<FormGettingStart>;

ExtraStaticGettingStartStep.summaryComponent = GettingStartSummary;
ExtraStaticGettingStartStep.defaultValues = { isValid: true };

export default ExtraStaticGettingStartStep;
