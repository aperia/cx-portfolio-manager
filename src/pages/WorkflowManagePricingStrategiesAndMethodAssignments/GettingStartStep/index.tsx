import { WORKFLOW_SETUP } from 'app/constants/local-storage';
import {
  Button,
  Icon,
  InlineMessage,
  TransDLS,
  useTranslation
} from 'app/_libraries/_dls';
import OverviewModal from 'pages/_commons/OverviewModal';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, { useRef, useState } from 'react';
import GettingStartSummary from './GettingStartSummary';

export interface FormGettingStart {
  isValid?: boolean;
  alreadyShown?: boolean;
}

const GettingStartStep: React.FC<WorkflowSetupProps<FormGettingStart>> = ({
  formValues,
  setFormValues,
  isStuck
}) => {
  const { t } = useTranslation();
  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;

  const [showOverview, setShowOverview] = useState(
    formValues.alreadyShown
      ? false
      : localStorage.getItem(WORKFLOW_SETUP.SHOW_OVERVIEW_AGAIN) !== 'false'
  );

  return (
    <React.Fragment>
      <div className="position-relative">
        <div className="absolute-top-right mt-n28">
          <Button
            className="mr-n8"
            variant="outline-primary"
            size="sm"
            onClick={() => setShowOverview(true)}
          >
            {t('txt_view_overview')}
          </Button>
        </div>
        {isStuck && (
          <InlineMessage className="mb-0 mt-24" variant="danger" withIcon>
            {t('txt_step_stuck_move_forward_message')}
          </InlineMessage>
        )}
        <div className="row">
          <div className="col-12 col-md-6 mt-24">
            <div className="bg-light-l20 rounded-lg p-16">
              <div className="text-center">
                <Icon name="request" size="12x" className="color-grey-l16" />
              </div>
              <p className="mt-16 color-grey">
                {t('txt_pricing_strategies_and_assignments_get_started_line1')}
              </p>
              <p className="mt-16">
                <strong>
                  {t(
                    'txt_pricing_strategies_and_assignments_get_started_line2'
                  )}
                </strong>
              </p>
              <p className="mt-8">
                <strong>
                  {t(
                    'txt_pricing_strategies_and_assignments_get_started_line3'
                  )}
                </strong>
              </p>
              <p className="mt-8 color-grey">
                {t('txt_pricing_strategies_and_assignments_get_started_line4')}
              </p>
              <p className="mt-8 color-grey">
                {t('txt_pricing_strategies_and_assignments_get_started_line5')}
              </p>
              <p className="mt-16">
                <strong>
                  {t(
                    'txt_pricing_strategies_and_assignments_get_started_line6'
                  )}
                </strong>
              </p>
              <p className="mt-8 color-grey">
                {t('txt_pricing_strategies_and_assignments_get_started_line7')}
              </p>
              <p className="mt-8 color-grey">
                {t('txt_pricing_strategies_and_assignments_get_started_line8')}
              </p>
              <p className="mt-8 color-grey">
                {t('txt_pricing_strategies_and_assignments_get_started_line9')}
              </p>
              <p className="mt-8 color-grey">
                {t('txt_pricing_strategies_and_assignments_get_started_line10')}
              </p>
              <p className="mt-8 color-grey">
                {t('txt_pricing_strategies_and_assignments_get_started_line11')}
              </p>
              <p className="mt-16">
                <strong>
                  {t(
                    'txt_pricing_strategies_and_assignments_get_started_line12'
                  )}
                </strong>
              </p>
              <p className="mt-8 color-grey">
                {t('txt_pricing_strategies_and_assignments_get_started_line13')}
              </p>
            </div>
          </div>
          <div className="col-12 col-md-6 mt-24 color-grey">
            <p>
              {t('txt_pricing_strategies_and_assignments_get_started_line14')}
            </p>
            <p className="mt-8">
              <TransDLS keyTranslation="txt_pricing_strategies_and_assignments_get_started_line15">
                <strong className="color-grey-d20" />
              </TransDLS>
            </p>
            <p className="mt-8">
              <TransDLS keyTranslation="txt_pricing_strategies_and_assignments_get_started_line16">
                <strong className="color-grey-d20" />
              </TransDLS>
            </p>
            <p className="mt-8">
              <TransDLS keyTranslation="txt_pricing_strategies_and_assignments_get_started_line17">
                <strong className="color-grey-d20" />
              </TransDLS>
            </p>
          </div>
        </div>
      </div>
      {showOverview && (
        <OverviewModal
          id="overviewWorkflowSetup"
          show
          onClose={() => {
            setShowOverview(false);
            keepRef.current.setFormValues({ alreadyShown: true });
          }}
        />
      )}
    </React.Fragment>
  );
};

const ExtraStaticGettingStartStep =
  GettingStartStep as WorkflowSetupStaticProp<FormGettingStart>;

ExtraStaticGettingStartStep.summaryComponent = GettingStartSummary;
ExtraStaticGettingStartStep.defaultValues = { isValid: true };

export default ExtraStaticGettingStartStep;
