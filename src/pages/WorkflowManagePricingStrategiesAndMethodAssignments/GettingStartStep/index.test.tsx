import { act, fireEvent, render, RenderResult } from '@testing-library/react';
import React from 'react';
import GettingStartStep, { FormGettingStart } from './index';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({ t: (value: string) => value })
  };
});

jest.mock('pages/_commons/OverviewModal', () => ({
  __esModule: true,
  default: ({ onClose }: any) => {
    return (
      <div>
        <div>OverviewModal</div>
        <button onClick={onClose}>OverviewButtonCloseModal</button>
      </div>
    );
  }
}));

jest.mock('pages/_commons/ContentExpand', () => ({
  __esModule: true,
  default: ({ instruction, children }: any) => (
    <div>
      <div>ContentExpand</div>
      <div>{instruction}</div>
      <div>{children}</div>
    </div>
  )
}));

const renderComponent = (props: any): RenderResult => {
  return render(<GettingStartStep {...props} />);
};

describe('WorkflowManagePricingStrategiesAndMethodAssignments > GettingStartStep > Index', () => {
  it('Should render without Overview modal > Open overview modal', () => {
    const props = {
      setFormValues: (values: any) => {},
      formValues: { alreadyShown: true }
    } as WorkflowSetupProps<FormGettingStart>;

    const wrapper = renderComponent(props);
    expect(wrapper.queryByText(/OverviewModal/)).not.toBeInTheDocument;

    fireEvent.click(wrapper.getByText('txt_view_overview'));
    expect(wrapper.queryByText(/OverviewModal/)).toBeInTheDocument;
  });

  it('Should render Overview modal > Close overview modal', () => {
    const props = {
      setFormValues: (values: any) => {},
      formValues: { alreadyShown: false }
    } as WorkflowSetupProps<FormGettingStart>;

    const wrapper = renderComponent(props);
    expect(wrapper.queryByText(/OverviewModal/)).toBeInTheDocument;
    act(() => {
      fireEvent.click(wrapper.getByText('txt_view_overview'));
    });

    act(() => {
      fireEvent.click(wrapper.getByText('OverviewButtonCloseModal'));
    });

    expect(wrapper.queryByText(/OverviewModal/)).not.toBeInTheDocument;
  });

  it('Should render stuck message', () => {
    const props = {
      setFormValues: (values: any) => {},
      formValues: {},
      isStuck: true
    } as unknown as WorkflowSetupProps<FormGettingStart>;

    const wrapper = renderComponent(props);

    expect(wrapper.queryByText('txt_step_stuck_move_forward_message'))
      .toBeInTheDocument;
  });
});
