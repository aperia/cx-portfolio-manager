import { default as stepRegistry } from '../_commons/WorkflowSetup/registries/stepRegistry';

const registerFunc = jest.spyOn(stepRegistry, 'registerStep');

describe('WorkflowManagePricingStrategiesAndMethodAssignments > registerSteps', () => {
  it('Should register components', async () => {
    await import('./registerSteps');

    expect(registerFunc).toBeCalledWith(
      'GettingStartManagePricingStrategiesAndMethodAssignments',
      expect.anything()
    );
  });
});
