import { useCallback, useMemo, useState } from 'react';
import { OptionType } from './types';

const useOptionModal = () => {
  const [optionSelected, setOptionSelected] = useState<OptionType | null>(null);
  const [versionSelected, setVersionSelected] = useState<string>();
  const [uniqueId, setUniqueId] = useState<string>();

  const onOptionSelect = useCallback(
    (opt: OptionType, _versionSelected?: string, _uniqueId?: string) => {
      setOptionSelected(opt);
      setVersionSelected(_versionSelected);
      setUniqueId(_uniqueId);
    },
    []
  );
  const showCreateModal = useMemo(
    () => optionSelected === 'CREATE' || optionSelected === 'EDIT_CREATE',
    [optionSelected]
  );

  const showChooseStrategyModal = useMemo(
    () => optionSelected === 'CHOOSE' || optionSelected === 'EDIT_CHOOSE',
    [optionSelected]
  );

  const resetToDefault = () => {
    setOptionSelected(null);
    setVersionSelected(undefined);
    setUniqueId(undefined);
  };

  return {
    onOptionSelect,
    showCreateModal,
    showChooseStrategyModal,
    resetToDefault,
    versionSelected,
    setVersionSelected,
    uniqueId
  };
};

export { useOptionModal };
