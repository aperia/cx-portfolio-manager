import ModalRegistry from 'app/components/ModalRegistry';
import {
  unsavedChangesProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { useUnsavedChangeRegistry, useUnsavedChangesRedirect } from 'app/hooks';
import {
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  TransDLS,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import { nanoid } from 'nanoid';
import {
  useSelectStrategyData,
  useSelectStrategyDataFetchingStatus
} from 'pages/_commons/redux/WorkflowSetup';
import React, { useImperativeHandle, useMemo } from 'react';
import { flattenMethodList, generateMethodListFromMethodData } from './helpers';
import {
  SelectStrategyArea,
  STRATEGY_AREA_LIST,
  useSelectStrategyArea
} from './SelectStrategyArea';
import { StrategyList, useStrategyList } from './StrategyList';
import { MethodItem, OptionType, StrategyDetailsFormValues } from './types';

export interface ChooseOrCreateModalProps {
  showChoose: boolean;
  showCreate: boolean;
  onClose: () => void;
  openStrategyDetails: (props: {
    from: OptionType;
    informationValues: StrategyDetailsFormValues;
    methodList: MethodItem[];
    uniqueId: string;
    version?: string;
  }) => void;
  versionDirty?: string;
  versionSelected?: string;
  uniqueId?: string;
}

const ChooseOrCreateModal = React.forwardRef<
  { resetList: () => void },
  ChooseOrCreateModalProps
>(
  (
    {
      showChoose,
      showCreate,
      onClose,
      openStrategyDetails,
      versionDirty,
      versionSelected,
      uniqueId
    },
    ref
  ) => {
    const { t } = useTranslation();
    const data = useSelectStrategyData();
    const { error, loading } = useSelectStrategyDataFetchingStatus();
    const fromOption = useMemo(() => {
      if (showChoose) return ['CHOOSE', 'EDIT_CHOOSE'];
      if (showCreate) return ['CREATE', 'EDIT_CREATE'];
      return [undefined, undefined];
    }, [showChoose, showCreate]);
    const strategyListProps = useStrategyList({
      data,
      show: showChoose || showCreate,
      error,
      loading,
      versionSelected
    });
    const [selectedStrategyArea] = useSelectStrategyArea(STRATEGY_AREA_LIST[0]);
    const redirect = useUnsavedChangesRedirect();

    const handleClose = () => {
      redirect({
        onConfirm: onClose,
        formsWatcher: [
          UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_PRICING_STRATEGIES___CREATE_OR_MODELED
        ]
      });
    };

    const onSubmit = () => {
      const assignment = flattenMethodList(
        strategyListProps.versionSelectedData?.methods
      );
      const _methodList = generateMethodListFromMethodData(
        assignment,
        assignment
      );
      openStrategyDetails({
        from: uniqueId
          ? (fromOption[1] as OptionType)
          : (fromOption[0] as OptionType),
        informationValues: {
          selectedStrategy: strategyListProps.selectedStrategy,
          strategyName: showCreate
            ? strategyListProps.selectedStrategy.name
            : '',
          cisDescription: '',
          comment: ''
        },
        methodList: _methodList,
        version: strategyListProps.versionSelected,
        uniqueId: uniqueId ?? nanoid()
      });
    };

    useImperativeHandle(ref, () => ({
      resetList: () => strategyListProps.onResetSearch(true)
    }));

    const dirty =
      !!versionDirty && versionDirty === strategyListProps.versionSelected;

    useUnsavedChangeRegistry(
      {
        ...unsavedChangesProps,
        formName:
          UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_PRICING_STRATEGIES___CREATE_OR_MODELED,
        priority: 1
      },
      [dirty]
    );

    return (
      <ModalRegistry
        id="manage-pricing-strategy-choose-strategy-to-model"
        show={showCreate || showChoose}
        lg
        onAutoClosedAll={handleClose}
      >
        <ModalHeader border closeButton onHide={handleClose}>
          <ModalTitle>
            {t(
              showChoose
                ? 'txt_choose_strategies_to_model'
                : 'txt_create_new_version'
            )}
          </ModalTitle>
        </ModalHeader>
        <ModalBody className="px-24 pt-24 pb-24 pb-lg-0">
          {showChoose ? (
            <TransDLS keyTranslation="txt_pricing_strategies_and_assignments_choose_strategies_to_model_help_text">
              <strong className="color-grey-d20"></strong>
            </TransDLS>
          ) : (
            <TransDLS keyTranslation="txt_pricing_strategies_and_assignments_create_new_version_help_text">
              <strong className="color-grey-d20"></strong>
            </TransDLS>
          )}
          <SelectStrategyArea value={selectedStrategyArea} />
          <StrategyList {...strategyListProps} />
        </ModalBody>

        <ModalFooter
          cancelButtonText={t('txt_cancel')}
          okButtonText={t('txt_continue')}
          onCancel={handleClose}
          onOk={onSubmit}
          disabledOk={isEmpty(strategyListProps.versionSelectedData)}
        />
      </ModalRegistry>
    );
  }
);

export { ChooseOrCreateModal };
