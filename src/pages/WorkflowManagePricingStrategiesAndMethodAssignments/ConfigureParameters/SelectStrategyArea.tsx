import { useAsyncThunkDispatch } from 'app/hooks';
import { DropdownList, useTranslation } from 'app/_libraries/_dls';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import React, { useEffect, useState } from 'react';

export const STRATEGY_AREA_LIST = [
  { code: 'CP', text: 'CP, Cardholder Pricing' }
];

export interface SelectStrategyAreaProps {
  value: RefData;
}

const SelectStrategyArea: React.FC<SelectStrategyAreaProps> = ({ value }) => {
  const { t } = useTranslation();

  return (
    <>
      <h5 className="mt-24">
        {t('txt_pricing_strategies_and_assignments_select_strategy_area')}
      </h5>
      <div className="row">
        <div className="col-5 mt-16">
          <DropdownList
            name="status"
            textField="text"
            value={value}
            readOnly
            label={t('txt_pricing_strategies_and_assignments_strategy_area')}
          >
            {STRATEGY_AREA_LIST.map(item => (
              <DropdownList.Item
                label={item.text}
                key={item.code}
                value={item.code}
              />
            ))}
          </DropdownList>
        </div>
      </div>
    </>
  );
};

const useSelectStrategyArea = (init: RefData) => {
  const [selected, setSelected] = useState(init);
  const thunkDispatch = useAsyncThunkDispatch();

  useEffect(() => {
    thunkDispatch(actionsWorkflowSetup.getPricingStrategyList(selected.code));
  }, [thunkDispatch, selected]);

  useEffect(() => {
    setSelected(init);
  }, [init]);

  return [selected];
};

export { SelectStrategyArea, useSelectStrategyArea };
