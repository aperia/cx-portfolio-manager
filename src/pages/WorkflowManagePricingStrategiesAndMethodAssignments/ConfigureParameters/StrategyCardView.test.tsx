import { fireEvent, render } from '@testing-library/react';
import React from 'react';
import { StrategyCardView } from './StrategyCardView';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/utils/MockView')
);

const mOnVersionSelected = jest.fn();
const mOnToggle = jest.fn();

const versionFixture = [
  {
    effectiveDate: '2022-02-02',
    accountsProcessedAA: '1234',
    accountsProcessedALP: '123',
    status: 'Production',
    cisDescription: '',
    commentArea: '',
    id: '1111',
    methods: []
  }
];

const factory = ({
  isExpand = false,
  versionSelected = '',
  versions = versionFixture
}: {
  isExpand?: boolean;
  versionSelected?: string;
  versions?: StrategyVersion[];
}) => {
  const { getByTestId } = render(
    <StrategyCardView
      id=""
      name="STT"
      isExpand={isExpand}
      versions={versions}
      versionSelected={versionSelected}
      onVersionSelect={mOnVersionSelected}
      onToggle={mOnToggle}
    />
  );

  const getView = () => getByTestId('strategy-card-view');
  const getChildView = () => getByTestId('strategy-card-view-expanded');

  const triggerSelectVersion = () => {
    fireEvent.click(getChildView().parentElement!);
  };

  return { getView, getChildView, triggerSelectVersion };
};

describe('test StrategyCardView component', () => {
  it('should be render', () => {
    const { getView } = factory({});
    expect(getView()).toBeInTheDocument();
  });

  it('should be render with expand', () => {
    const { getChildView, triggerSelectVersion } = factory({ isExpand: true });
    expect(getChildView()).toBeInTheDocument();
    triggerSelectVersion();
    expect(mOnVersionSelected).toBeCalled();
  });
});
