import { act, fireEvent, render } from '@testing-library/react';
import React from 'react';
import { MethodList } from './MethodList';
import { MethodItem } from './types';

jest.mock('app/components/HeadingWithSearch', () => ({
  ...jest.requireActual('app/components/HeadingWithSearch'),
  HeadingWithSearch: ({
    searchValue,
    onSearchChange,
    onClearSearch
  }: {
    searchValue: string;
    onSearchChange: (s: string) => void;
    onClearSearch: () => void;
  }) => (
    <div>
      <input
        data-testid="heading-with-search"
        value={searchValue}
        onChange={e => {
          onSearchChange(e.target.value);
        }}
      />
      {searchValue && (
        <button data-testid="clear-search" onClick={onClearSearch}></button>
      )}
    </div>
  )
}));

jest.mock('./MethodListGrid', () => ({
  MethodListGrid: () => <div data-testid="MethodListGrid"></div>
}));

jest.mock('app/components/NoDataFound', () => ({
  __esModule: true,
  default: () => <div data-testid="NoDataFound"></div>
}));

const fixture = [
  {
    rowId: 'IC',
    service: 'CP',
    subject: 'IC',
    subData: [
      {
        id: 'CP_IC_IR',
        serviceSubjectSection: '',
        currentAssignment: 'METHOD2',
        newAssignment: 'METHOD2',
        moreInfoText: ''
      },
      {
        id: 'CP_IC_ID',
        serviceSubjectSection: '',
        currentAssignment: 'METHOD2',
        newAssignment: 'METHOD2',
        moreInfoText: ''
      }
    ]
  }
];

const factory = (list: MethodItem[]) => {
  const { getByTestId, queryByTestId } = render(<MethodList list={list} />);

  const showNodata = () => {
    const searchInput = getByTestId('heading-with-search');
    act(() => {
      fireEvent.change(searchInput, { target: { value: 'not match' } });
    });
  };
  const getNodata = () => queryByTestId('NoDataFound');

  const resetSearch = () => {
    fireEvent.click(getByTestId('clear-search'));
  };
  return {
    getMethodListGrid: () => getByTestId('MethodListGrid'),
    showNodata,
    resetSearch,
    getNodata
  };
};

describe('test MethodList component', () => {
  it('should be render', () => {
    const { getMethodListGrid } = factory(fixture);
    expect(getMethodListGrid()).toBeInTheDocument();
  });

  it('should be search and reset', () => {
    const { showNodata, getNodata, resetSearch } = factory(fixture);
    showNodata();
    expect(getNodata()).toBeInTheDocument();
    resetSearch();
    expect(getNodata()).not.toBeInTheDocument();
  });
});
