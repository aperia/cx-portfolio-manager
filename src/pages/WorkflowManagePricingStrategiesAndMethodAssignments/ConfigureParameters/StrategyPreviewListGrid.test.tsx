import { act, fireEvent, render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React, { useRef, useState } from 'react';
import { StrategyPreviewListGrid } from './StrategyPreviewListGrid';
import { StrategyListFormValues, StrategyPreviewListGridRef } from './types';

jest.mock('./RemoveStrategyConfirm', () => ({
  __esModule: true,
  RemoveStrategyConfirm: () => <div data-testid="RemoveStrategyConfirm"></div>,
  useRemoveStrategy: () => ({
    openRemoveStrategyConfirm: (fn: Function) => fn()
  })
}));

jest.mock('./MethodListGrid', () => ({
  MethodListGrid: () => <div data-testid="MethodListGrid"></div>
}));

jest.mock('pages/_commons/Utils/Paging', () => ({
  __esModule: true,
  default: () => <div data-testid="Paging"></div>
}));

const mOnRemove = jest.fn();
const mOnUpdate = jest.fn();

const fixture = [
  {
    unique: '12',
    name: 'STT',
    modeledFrom: { name: 'STT1', id: '2' },
    fromVersion: '222',
    cisDescription: '',
    commentArea: '',
    methods: [
      {
        serviceSubjectSection: 'CP IC ID',
        previousValue: 'Method1',
        newValue: 'Method2'
      }
    ]
  },
  {
    unique: '13',
    name: 'STT',
    modeledFrom: { name: 'STT', id: '2' },
    fromVersion: '222',
    cisDescription: '',
    commentArea: '',
    methods: [
      {
        serviceSubjectSection: 'CP IC ID',
        previousValue: 'Method1',
        newValue: 'Method2'
      }
    ]
  }
];

const fixture10 = Array.from({ length: 12 }, (_, index) => ({
  unique: `${index}`,
  name: 'STT',
  modeledFrom: { name: 'STT1', id: '2' },
  fromVersion: '222',
  cisDescription: '',
  commentArea: '',
  methods: [
    {
      serviceSubjectSection: 'CP IC ID',
      previousValue: 'Method1',
      newValue: 'Method2'
    }
  ]
}));

const FakeComponent = ({
  isPreview = false,
  list = fixture
}: {
  isPreview?: boolean;
  list?: StrategyListFormValues[];
}) => {
  const [expandedList, setExpandedList] = useState<string[]>([]);
  const ref = useRef<StrategyPreviewListGridRef>(null);

  return (
    <>
      <StrategyPreviewListGrid
        strategyList={list}
        isPreview={isPreview}
        onRemove={mOnRemove}
        onUpdate={mOnUpdate}
        expanded={expandedList}
        onExpand={setExpandedList}
        ref={ref}
      />
      <button
        data-testid="navigateToItem"
        onClick={() => {
          ref.current?.navigateToItem('12');
        }}
      >
        trigger navigateToItem
      </button>
      <button
        data-testid="resetToPage1"
        onClick={() => {
          ref.current?.resetToPage1();
        }}
      >
        trigger navigateToItem
      </button>
    </>
  );
};

const factory = (list?: StrategyListFormValues[], isPreview?: boolean) => {
  const { getAllByText, getByTestId, baseElement } = render(
    <FakeComponent isPreview={isPreview} list={list} />
  );

  const triggerNavigateToItem = () => {
    fireEvent.click(getByTestId('navigateToItem'));
  };
  const triggerResetPage = () => {
    fireEvent.click(getByTestId('resetToPage1'));
  };

  const triggerEdit = () => {
    fireEvent.click(getAllByText('txt_edit')[0]);
  };

  const triggerDelete = () => {
    fireEvent.click(getAllByText('txt_delete')[0]);
  };

  const triggerExpand = () => {
    act(() => {
      userEvent.click(baseElement.getElementsByClassName('icon icon-plus')[0]);
    });
  };

  const getMethodListGrid = () => getByTestId('MethodListGrid');

  const getPaging = () => getByTestId('Paging');
  const getRemoveStrategyConfirm = () => getByTestId('RemoveStrategyConfirm');
  return {
    triggerNavigateToItem,
    triggerResetPage,
    getRemoveStrategyConfirm,
    getPaging,
    triggerEdit,
    triggerDelete,
    triggerExpand,
    getMethodListGrid
  };
};

describe('test StrategyPreviewListGrid component', () => {
  it('should be render StrategyPreviewListGrid', () => {
    const { getRemoveStrategyConfirm } = factory();
    expect(getRemoveStrategyConfirm()).toBeInTheDocument();
  });

  it('should be show Pagination', () => {
    const { getPaging } = factory(fixture10, true);
    expect(getPaging()).toBeInTheDocument();
  });

  it('should be trigger edit', () => {
    const { triggerEdit, triggerResetPage, triggerNavigateToItem } = factory();
    triggerResetPage();
    triggerNavigateToItem();
    triggerEdit();
    expect(mOnUpdate).toBeCalled();
  });

  it('should be trigger delete', () => {
    const { triggerDelete } = factory();
    triggerDelete();
    expect(mOnRemove).toBeCalled();
  });

  it('should be expand', () => {
    const { triggerExpand, getMethodListGrid } = factory();
    triggerExpand();
    expect(getMethodListGrid()).toBeInTheDocument();
  });
});
