import {
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { useUnsavedChangeRegistry } from 'app/hooks';
import { InlineMessage, useTranslation } from 'app/_libraries/_dls';
import { isEqual } from 'app/_libraries/_dls/lodash';
import { nanoid } from 'nanoid';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, {
  useCallback,
  useEffect,
  useReducer,
  useRef,
  useState
} from 'react';
import { ChooseOrCreateModal } from './ChooseOrCreateModal';
import { MethodProvider } from './Context';
import { convertToOpenStrategyDetailsValues } from './helpers';
import parseFormValues from './parseFormValues';
import { SelectOption } from './SelectOption';
import { StrategyDetails, useStrategyDetails } from './StrategyDetails';
import { StrategyPreviewList } from './StrategyPreviewList';
import Summary from './Summary';
import {
  StrategyListFormValues,
  StrategyPreviewListGridRef,
  StrategyPreviewListItem
} from './types';
import { useOptionModal } from './useOptionModal';

export interface ConfigureParametersFormValues {
  isValid?: boolean;
  strategyList?: StrategyListFormValues[];
}

export type FormValuesAction =
  | { type: 'ADD_STRATEGY_TO_LIST'; payload: StrategyListFormValues }
  | {
      type: 'UPDATE_STRATEGY';
      payload: StrategyListFormValues;
    }
  | { type: 'REMOVE_STRATEGY'; payload: string }
  | { type: 'SET_IS_VALID'; payload: boolean }
  | { type: 'SET_STRATEGY_LIST_EXPANDED'; payload: string[] }
  | { type: 'RESET_TRUNCATE_LIST' }
  | { type: 'SAVE_STRATEGY_LIST_INITIAL' };

const getPureStrategyList = (list: StrategyListFormValues[]) =>
  list.map(({ unique, ...rest }) => rest);

const ConfigureParametersStep: React.FC<
  WorkflowSetupProps<ConfigureParametersFormValues>
> = props => {
  const { formValues, setFormValues, savedAt, stepId, selectedStep, isStuck } =
    props;
  const { t } = useTranslation();
  const strategyListRef = useRef<StrategyPreviewListGridRef>(null);
  const [strategyListInitial, setStrategyListInitial] = useState<
    StrategyListFormValues[]
  >([]);

  const formValuesReducer = (
    state: Required<ConfigureParametersFormValues> & {
      strategyListExpanded: string[];
    },
    action: FormValuesAction
  ) => {
    const getPureFormValue = ({
      strategyListExpanded: _,
      ...rest
    }: Required<ConfigureParametersFormValues> & {
      strategyListExpanded: string[];
    }) => rest;
    let nextState;
    switch (action.type) {
      case 'ADD_STRATEGY_TO_LIST':
        nextState = {
          ...state,
          // regenerate unique id to collapse all truncate
          strategyList: [
            ...state.strategyList!.map(item => ({ ...item, unique: nanoid() })),
            action.payload
          ],
          strategyListExpanded: [action.payload.unique]
        };
        setFormValues(getPureFormValue(nextState));
        // wait for re-render
        setTimeout(() => {
          strategyListRef?.current?.navigateToItem(action.payload.unique);
        }, 100);
        return nextState;
      case 'UPDATE_STRATEGY':
        nextState = { ...state, strategyList: [...state.strategyList] };
        const currentStrategyIndex = nextState.strategyList.findIndex(
          item => item.unique === action.payload.unique
        );
        nextState.strategyList[currentStrategyIndex] = action.payload;
        setFormValues(getPureFormValue(nextState));
        return nextState;
      case 'REMOVE_STRATEGY':
        nextState = {
          ...state,
          strategyList: state.strategyList.filter(
            st => st.unique !== action.payload
          )
        };
        setFormValues(getPureFormValue(nextState));
        return nextState;
      case 'SET_IS_VALID':
        nextState = { ...state, isValid: action.payload };
        setFormValues(getPureFormValue(nextState));
        return nextState;
      case 'SET_STRATEGY_LIST_EXPANDED':
        return { ...state, strategyListExpanded: action.payload };
      case 'SAVE_STRATEGY_LIST_INITIAL':
        setStrategyListInitial(formValues.strategyList!);
        return { ...state, strategyList: formValues.strategyList! };
      default:
        // case 'RESET_TRUNCATE_LIST':
        nextState = {
          ...state,
          strategyList: state.strategyList.map(st => ({
            ...st,
            unique: nanoid()
          }))
        };
        setFormValues(getPureFormValue(nextState));
        return nextState;
    }
  };

  const [{ strategyListExpanded, strategyList }, dispatch] = useReducer(
    formValuesReducer,
    {
      ...(formValues as Required<ConfigureParametersFormValues>),
      strategyListExpanded: []
    }
  );

  const chooseOrCreateRef = useRef<{ resetList: () => void }>(null);
  const [versionDirty, setVersionDirty] = useState<string>();

  const {
    showCreateModal,
    showChooseStrategyModal,
    resetToDefault,
    onOptionSelect,
    versionSelected,
    uniqueId
  } = useOptionModal();

  const onCloseCb = useCallback(() => {
    resetToDefault();
    setVersionDirty(undefined);
  }, [resetToDefault]);

  const onBackCb = useCallback((dirty?: string) => {
    setVersionDirty(dirty);
    chooseOrCreateRef.current?.resetList();
  }, []);

  const { openStrategyDetails, ...strategyDetailsProps } = useStrategyDetails({
    onCloseCb,
    onBackCb,
    dispatch,
    versionSelected
  });

  const handleRemoveStrategy = (name: string) => {
    dispatch({ type: 'REMOVE_STRATEGY', payload: name });
  };

  const handleUpdateStrategy = (values: StrategyPreviewListItem) => {
    const { informationValues, methodList, from } =
      convertToOpenStrategyDetailsValues(values);
    onOptionSelect(from, values.fromVersion, values.uniqueId);
    openStrategyDetails({
      from,
      informationValues,
      methodList,
      uniqueId: values.uniqueId
    });
  };

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_PRICING_STRATEGIES___CONFIGURE_PARAMETERS,
      priority: 1
    },
    [
      !isEqual(
        getPureStrategyList(strategyListInitial),
        getPureStrategyList(formValues.strategyList!)
      )
    ]
  );

  useEffect(() => {
    // save initValues to change recognized
    dispatch({ type: 'SAVE_STRATEGY_LIST_INITIAL' });
  }, [savedAt]);

  useEffect(() => {
    dispatch({ type: 'SET_IS_VALID', payload: !!strategyList.length });
  }, [strategyList.length]);

  useEffect(() => {
    // reset expanded list when next step
    if (stepId !== selectedStep) {
      dispatch({ type: 'SET_STRATEGY_LIST_EXPANDED', payload: [] });
      dispatch({ type: 'RESET_TRUNCATE_LIST' });
      strategyListRef.current?.resetToPage1();
    }
  }, [stepId, selectedStep]);

  const showButtonSelect = !formValues.strategyList?.length;

  return (
    <>
      <div className="mt-8 pb-24 border-bottom color-grey">
        <p>
          {t(
            'txt_pricing_strategies_and_assignments_configure_parameter_help_text'
          )}
        </p>
      </div>
      {isStuck && (
        <InlineMessage className="mb-0 mt-24" variant="danger" withIcon>
          {t('txt_step_stuck_move_forward_message')}
        </InlineMessage>
      )}
      {showButtonSelect && <SelectOption onSelect={onOptionSelect} />}
      {!showButtonSelect && (
        <StrategyPreviewList
          onOptionSelect={onOptionSelect}
          strategyList={formValues.strategyList!}
          onRemove={handleRemoveStrategy}
          onUpdate={handleUpdateStrategy}
          expanded={strategyListExpanded}
          onExpand={(list: string[]) => {
            dispatch({ type: 'SET_STRATEGY_LIST_EXPANDED', payload: list });
          }}
          ref={strategyListRef}
        />
      )}
      <ChooseOrCreateModal
        showCreate={showCreateModal}
        showChoose={showChooseStrategyModal}
        onClose={onCloseCb}
        openStrategyDetails={openStrategyDetails}
        versionDirty={versionDirty}
        versionSelected={versionSelected}
        uniqueId={uniqueId}
        ref={chooseOrCreateRef}
      />
      <MethodProvider methodList={strategyDetailsProps.methodList}>
        <StrategyDetails
          {...strategyDetailsProps}
          tempStrategyList={formValues.strategyList!}
        />
      </MethodProvider>
    </>
  );
};

const ExtraStaticSelectStrategies =
  ConfigureParametersStep as WorkflowSetupStaticProp;
ExtraStaticSelectStrategies.summaryComponent = Summary;
ExtraStaticSelectStrategies.parseFormValues = parseFormValues;
ExtraStaticSelectStrategies.defaultValues = {
  isValid: false,
  strategyList: []
};

export default ExtraStaticSelectStrategies;
