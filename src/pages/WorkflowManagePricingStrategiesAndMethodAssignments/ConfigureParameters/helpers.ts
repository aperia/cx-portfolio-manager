import { orderBy } from 'app/_libraries/_dls/lodash';
import {
  METHOD_SUB_DATA,
  SERVICE_SUBJECT_SECTION_KY_MAPPING,
  SUBJECT
} from './constants';
import {
  OptionType,
  StrategyListFormValues,
  StrategyPreviewListItem,
  Subject
} from './types';

export const getMethodListFromStrategyList = (
  strategyList: Strategy[],
  strategyName?: string,
  version?: string
) => {
  if (!version || !strategyName) return [];
  const strategyFinding = strategyList.find(
    ({ name }) => name === strategyName
  );
  if (!strategyFinding) return [];
  const versionFinding = strategyFinding.versions.find(
    ({ id }) => id === version
  );
  if (!versionFinding) return [];
  return versionFinding.methods;
};

/**
 *
 * @param list [{currentAssignment: "ABC", serviceSubjectSection: "CP IC PO"}]
 * @returns {CP_IC_PO: "ABC"}
 */
export const flattenMethodList = (list?: StrategyMethod[]) => {
  const initial: Record<string, string> = {};
  if (!list) return initial;
  return list.reduce((flatten, method) => {
    const pair = Object.entries(SERVICE_SUBJECT_SECTION_KY_MAPPING).find(
      ([, v]) => v === method.serviceSubjectSection
    );
    if (pair) {
      flatten[pair[0]] = method.currentAssignment;
    }
    return flatten;
  }, initial);
};

export const groupByServiceSubjectSection = <
  T extends { serviceSubjectSection: string }
>(
  list: T[]
) => {
  return list.reduce((acc, curr) => {
    Object.entries(SERVICE_SUBJECT_SECTION_KY_MAPPING).forEach(([k, v]) => {
      if (curr.serviceSubjectSection === v) {
        acc[k] = curr;
      }
    });
    return acc;
  }, {} as Record<string, T>);
};

export const generateMethodListFromMethodData = (
  currentAssignment: Record<string, string>,
  newAssignment: Record<string, string>
) =>
  Object.entries(SUBJECT).map(([id, subject]) => ({
    rowId: `${id}`,
    service: 'CP, Cardholder Pricing',
    subject,
    subData: METHOD_SUB_DATA[id as Subject].map(sub => ({
      ...sub,
      newAssignment: newAssignment[sub.id],
      currentAssignment: currentAssignment[sub.id]
    }))
  }));

export const generateMethodsNewAssignmentInit = () => {
  const results: Record<string, string> = {};
  Object.keys(SERVICE_SUBJECT_SECTION_KY_MAPPING).forEach(
    k => (results[k] = '')
  );
  return results;
};

export const formatStrategyPreviewListData = (
  list: StrategyListFormValues[]
) => {
  return list.reduce((acc, curr) => {
    const methodGrouped = groupByServiceSubjectSection(curr.methods);
    const serviceSubject = Object.entries(SUBJECT).map(([id, subject]) => {
      return {
        rowId: `${id}`,
        service: 'CP, Cardholder Pricing',
        subject,
        subData: METHOD_SUB_DATA[id as Subject].map(sub => {
          return {
            ...sub,
            newAssignment: methodGrouped[sub.id]?.newValue,
            currentAssignment: methodGrouped[sub.id]?.previousValue
          };
        })
      };
    });
    const formatted = {
      uniqueId: curr.unique,
      strategyName: curr.name,
      modeled: curr.modeledFrom.name,
      modeledId: curr.modeledFrom.id,
      cisDescription: curr.cisDescription,
      commentArea: curr.commentArea,
      fromVersion: curr.fromVersion,
      actionTaken:
        curr.name === curr.modeledFrom.name
          ? 'Version Created'
          : 'Strategy Modeled',
      serviceSubject
    };
    acc.push(formatted);
    return orderBy(acc, ['strategyName'], ['asc']);
  }, [] as StrategyPreviewListItem[]);
};

export const convertToOpenStrategyDetailsValues = (
  source: StrategyPreviewListItem
) => {
  const informationValues = {
    strategyName: source.strategyName,
    cisDescription: source.cisDescription,
    comment: source.commentArea,
    selectedStrategy: { name: source.modeled, id: source.modeledId }
  };

  const methodList = source.serviceSubject;
  return {
    informationValues,
    methodList,
    from:
      source.strategyName !== source.modeled
        ? ('EDIT_CHOOSE' as OptionType)
        : ('EDIT_CREATE' as OptionType)
  };
};
