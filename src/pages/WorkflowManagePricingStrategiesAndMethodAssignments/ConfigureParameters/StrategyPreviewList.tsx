import EnhanceDropdownButton from 'app/components/EnhanceDropdownButton';
import { DropdownButton, useTranslation } from 'app/_libraries/_dls';
import React from 'react';
import { OPTIONS } from './SelectOption';
import { StrategyPreviewListGrid } from './StrategyPreviewListGrid';
import {
  OptionType,
  StrategyListFormValues,
  StrategyPreviewListGridRef,
  StrategyPreviewListItem
} from './types';

interface StrategyPreviewListProps {
  onOptionSelect: (name: OptionType) => void;
  strategyList: StrategyListFormValues[];
  onRemove: (name: string) => void;
  onUpdate: (values: StrategyPreviewListItem) => void;
  expanded: string[];
  onExpand: (list: string[]) => void;
}

const StrategyPreviewList = React.forwardRef<
  StrategyPreviewListGridRef,
  StrategyPreviewListProps
>(
  (
    { onOptionSelect, strategyList, onRemove, onUpdate, expanded, onExpand },
    ref
  ) => {
    const { t } = useTranslation();
    return (
      <>
        <div className="mt-24">
          <div className="d-flex align-items-center">
            <h5>{t('txt_strategy_list')}</h5>
            <div className="d-flex ml-auto mr-n8">
              <EnhanceDropdownButton
                buttonProps={{
                  size: 'sm',
                  children: t('txt_add_strategy'),
                  variant: 'outline-primary'
                }}
                onSelect={e => {
                  onOptionSelect(e.target.value);
                }}
              >
                {OPTIONS.map(item => (
                  <DropdownButton.Item
                    key={item.name}
                    label={t(item.title)}
                    value={item.name}
                  />
                ))}
              </EnhanceDropdownButton>
            </div>
          </div>
        </div>
        <div className="mt-16">
          <StrategyPreviewListGrid
            strategyList={strategyList}
            onRemove={onRemove}
            onUpdate={onUpdate}
            expanded={expanded}
            onExpand={onExpand}
            ref={ref}
          />
        </div>
      </>
    );
  }
);

export { StrategyPreviewList };
