import { mapGridExpandCollapse } from 'app/helpers';
import { Grid, useTranslation } from 'app/_libraries/_dls';
import React, { useState } from 'react';
import { MethodListSubGrid } from './MethodListSubGrid';
import { MethodItem, Subject } from './types';

interface MethodListGridProps {
  data: MethodItem[];
  isPreview?: boolean;
}

const MethodListGrid: React.FC<MethodListGridProps> = ({ data, isPreview }) => {
  const { t } = useTranslation();
  // IC Subject expand by default
  const [expandedList, onExpand] = useState<string[]>([Subject.IC]);

  const columns = [
    {
      id: 'service',
      Header: t('txt_service'),
      accessor: 'service',
      width: 356
    },
    {
      id: 'subject',
      Header: t('txt_subject'),
      accessor: 'subject',
      width: 559
    }
  ];
  return (
    <Grid
      togglable
      expandedList={expandedList}
      onExpand={onExpand}
      dataItemKey="rowId"
      expandedItemKey="rowId"
      toggleButtonConfigList={data.map(mapGridExpandCollapse('rowId', t))}
      subRow={({ original }: { original: MethodItem }) => (
        <div className="p-20">
          <MethodListSubGrid isPreview={isPreview} data={original.subData} />
        </div>
      )}
      columns={columns}
      data={data}
    />
  );
};

export type { MethodListGridProps };
export { MethodListGrid };
