import { fireEvent, render } from '@testing-library/react';
import 'app/utils/_mockComponent/mockModalRegistry';
import React, { useRef } from 'react';
import { ChooseOrCreateModal } from './ChooseOrCreateModal';

const mOpenStrategyDetails = jest.fn();
const mOnClose = jest.fn();

jest.mock('./SelectStrategyArea', () => ({
  STRATEGY_AREA_LIST: [{ code: 'CP', text: 'CP, Cardholder Pricing' }],
  useSelectStrategyArea: () => [],
  SelectStrategyArea: () => <div data-testid="SelectStrategyArea"></div>
}));

jest.mock('./StrategyList', () => ({
  useStrategyList: () => ({
    onResetSearch: jest.fn(),
    versionSelectedData: { id: '' },
    selectedStrategy: { name: 'STT1' }
  }),
  StrategyList: () => <div data-testid="StrategyList"></div>
}));

jest.mock('pages/_commons/redux/WorkflowSetup', () => ({
  useSelectStrategyData: () => [],
  useSelectStrategyDataFetchingStatus: () => ({ loading: false, error: false })
}));

jest.mock('app/hooks', () => ({
  useUnsavedChangeRegistry: () => ({}),
  useUnsavedChangesRedirect:
    () =>
    ({ onConfirm }: { onConfirm: () => void }) => {
      onConfirm();
    }
}));

const FakeComponent = ({
  show,
  versionDirty,
  uniqueId
}: {
  show: 'create' | 'choose' | '';
  versionDirty?: string;
  uniqueId?: string;
}) => {
  const ref = useRef<{ resetList: () => void }>(null);
  return (
    <>
      <ChooseOrCreateModal
        showCreate={show === 'create'}
        showChoose={show === 'choose'}
        onClose={mOnClose}
        openStrategyDetails={mOpenStrategyDetails}
        versionDirty={versionDirty}
        uniqueId={uniqueId}
        ref={ref}
      />
      <button
        data-testid="trigger-ref"
        onClick={() => ref.current?.resetList()}
      >
        trigger Ref
      </button>
    </>
  );
};

const factory = ({
  show,
  versionDirty,
  uniqueId
}: {
  show: 'create' | 'choose' | '';
  versionDirty?: string;
  uniqueId?: string;
}) => {
  const { queryByText, getByText, getByTestId } = render(
    <FakeComponent
      show={show}
      versionDirty={versionDirty}
      uniqueId={uniqueId}
    />
  );

  const getTitleCreate = () => queryByText('txt_create_new_version');
  const getTitleChoose = () => getByText('txt_choose_strategies_to_model');

  const triggerClose = () => {
    fireEvent.click(getByText('txt_cancel'));
  };

  const triggerSubmit = () => {
    fireEvent.click(getByText('txt_continue'));
  };

  const triggerRef = () => {
    fireEvent.click(getByTestId('trigger-ref'));
  };

  return {
    getTitleCreate,
    getTitleChoose,
    triggerRef,
    triggerClose,
    triggerSubmit
  };
};

describe('test ChooseOrCreateModal component', () => {
  it('should be render create modal', () => {
    const { getTitleCreate, triggerRef } = factory({ show: 'create' });
    triggerRef();
    expect(getTitleCreate()).toBeInTheDocument();
  });

  it('should be render choose modal', () => {
    const { getTitleChoose, triggerRef } = factory({ show: 'choose' });
    triggerRef();
    expect(getTitleChoose()).toBeInTheDocument();
  });

  it('should be hide modal', () => {
    const { getTitleCreate, triggerRef } = factory({ show: '' });
    triggerRef();
    expect(getTitleCreate()).not.toBeInTheDocument();
  });

  it('should be close', () => {
    const { triggerClose } = factory({ show: 'create', versionDirty: 'v1' });
    triggerClose();
    expect(mOnClose).toBeCalled();
  });

  it('should be submit create', () => {
    const { triggerSubmit } = factory({ show: 'create', versionDirty: 'v1' });
    triggerSubmit();
    expect(mOpenStrategyDetails).toBeCalled();
  });

  it('should be submit choose', () => {
    const { triggerSubmit } = factory({ show: 'choose', versionDirty: 'v1' });
    triggerSubmit();
    expect(mOpenStrategyDetails).toBeCalled();
  });

  it('should be open from back on details', () => {
    const { triggerSubmit } = factory({
      show: 'create',
      versionDirty: 'v1',
      uniqueId: 'unique'
    });
    triggerSubmit();
    expect(mOpenStrategyDetails).toBeCalled();
  });
});
