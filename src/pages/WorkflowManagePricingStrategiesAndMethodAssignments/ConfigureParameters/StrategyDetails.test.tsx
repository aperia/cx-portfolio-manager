import { act, fireEvent, render } from '@testing-library/react';
import 'app/utils/_mockComponent/mockModalRegistry';
import 'app/_libraries/_dls/test-utils/mocks/mockCanvas';
import * as workflowSetup from 'pages/_commons/redux/WorkflowSetup';
import React from 'react';
import { StrategyDetails, useStrategyDetails } from './StrategyDetails';
import { OptionType, StrategyListFormValues } from './types';

const mOnCloseCb = jest.fn();
const mOnBackCb = jest.fn();
const mDispatch = jest.fn();
const mockOnConfirmParams = jest.fn();

jest.mock('pages/_commons/redux/WorkflowSetup');

jest.mock('app/hooks', () => ({
  useAsyncThunkDispatch: () => (fn: Function) => {
    fn();
  },
  useUnsavedChangeRegistry: (options: any, changes: any, onConfirm: any) => {
    onConfirm && onConfirm();
  },
  useUnsavedChangesRedirect:
    () =>
    ({ onConfirm, onDiscard, onCancel }: any) => {
      onDiscard && onDiscard();
      onCancel && onCancel();
      onConfirm && onConfirm(mockOnConfirmParams());
    }
}));

jest.mock('app/_libraries/_dls/components/i18next/Trans', () => ({
  __esModule: true,
  default: ({ keyTranslation }: { keyTranslation: string }) => (
    <div>{keyTranslation}</div>
  )
}));

jest.mock('./StrategyDetailsForm', () => ({
  useStrategyDetailsForm: () => ({
    handleSetDuplicated: jest.fn(),
    handleSetVersionDuplicated: jest.fn(),
    values: {
      selectedStrategy: { name: 'STT', id: '1' },
      strategyName: 'STT1',
      cisDescription: '',
      comment: ''
    }
  }),
  StrategyDetailsForm: () => <div data-testid="StrategyDetailsForm"></div>
}));

jest.mock('./Context', () => ({
  useMethodContext: () => ({
    isMethodListDirty: false,
    methodList: [
      {
        rowId: '1',
        service: 'CP',
        subject: 'IC',
        subData: [
          { id: 'CP_IC_ID', currentAssignment: 'ABC', newAssignment: 'ABC' }
        ]
      }
    ]
  })
}));

jest.mock('./MethodList', () => ({
  MethodList: () => <div data-testid="MethodList"></div>
}));

const mGetPricingStrategyMethodList = jest.fn();

const generateOpenDetailParams = ({
  from,
  strategyName = 'STT',
  version
}: {
  from: OptionType;
  strategyName?: string;
  version?: string;
}) => ({
  from,
  informationValues: {
    selectedStrategy: { name: 'STT', id: '1' },
    strategyName,
    cisDescription: '',
    comment: ''
  },
  methodList: [],
  uniqueId: 'unique',
  version
});

const strategyListFixture = (strategyName = 'STT') => [
  {
    unique: 'unique',
    name: strategyName,
    modeledFrom: { name: 'STT1', id: 'id' },
    fromVersion: 'vid1',
    cisDescription: '',
    commentArea: '',
    methods: [
      {
        serviceSubjectSection: 'CP IC ID',
        previousValue: 'METHOD2',
        newValue: 'METHOD1'
      }
    ]
  }
];

beforeEach(() => {
  jest
    .spyOn(workflowSetup.actionsWorkflowSetup, 'getPricingStrategyMethodList')
    .mockImplementation(() => mGetPricingStrategyMethodList);

  jest
    .spyOn(workflowSetup, 'useSelectStrategyData')
    .mockImplementation(() => [{ id: 'STT2', name: 'STT2', versions: [] }]);
});

const FakeComponent = ({
  tempStrategyList,
  versionSelected
}: {
  tempStrategyList: StrategyListFormValues[];
  versionSelected?: string;
}) => {
  const { openStrategyDetails, ...strategyDetailsProps } = useStrategyDetails({
    onCloseCb: mOnCloseCb,
    onBackCb: mOnBackCb,
    dispatch: mDispatch,
    versionSelected
  });

  return (
    <>
      <button
        data-testid="openStrategyDetails_from_edit_choose"
        onClick={() => {
          openStrategyDetails(
            generateOpenDetailParams({ from: 'EDIT_CHOOSE', version: '1' })
          );
        }}
      ></button>

      <button
        data-testid="openStrategyDetails_from_choose"
        onClick={() => {
          openStrategyDetails(
            generateOpenDetailParams({
              from: 'CHOOSE',
              strategyName: 'STT1',
              version: '1'
            })
          );
        }}
      ></button>
      <button
        data-testid="openStrategyDetails_from_create"
        onClick={() => {
          openStrategyDetails(generateOpenDetailParams({ from: 'CREATE' }));
        }}
      ></button>
      <button
        data-testid="openStrategyDetails_from_edit_create"
        onClick={() => {
          openStrategyDetails(
            generateOpenDetailParams({ from: 'EDIT_CREATE' })
          );
        }}
      ></button>
      <StrategyDetails
        {...strategyDetailsProps}
        tempStrategyList={tempStrategyList}
      />
    </>
  );
};

const factory = (
  tempStrategyList: StrategyListFormValues[],
  versionSelected?: string
) => {
  const { queryByTestId, getByTestId, getByText } = render(
    <FakeComponent
      tempStrategyList={tempStrategyList}
      versionSelected={versionSelected}
    />
  );

  const openModalFromChoose = () => {
    act(() => {
      fireEvent.click(getByTestId('openStrategyDetails_from_choose'));
    });
  };

  const openModalFromEditChoose = () => {
    act(() => {
      fireEvent.click(getByTestId('openStrategyDetails_from_edit_choose'));
    });
  };

  const openModalFromCreate = () => {
    act(() => {
      fireEvent.click(getByTestId('openStrategyDetails_from_create'));
    });
  };

  const openModalFromEditCreate = () => {
    act(() => {
      fireEvent.click(getByTestId('openStrategyDetails_from_edit_create'));
    });
  };

  const getStrategyDetailForm = () => queryByTestId('StrategyDetailsForm');

  const triggerBack = () => {
    act(() => {
      fireEvent.click(getByText('txt_back'));
    });
  };

  const triggerSubmit = () => {
    act(() => {
      fireEvent.click(getByText('txt_save'));
    });
  };

  const triggerClose = () => {
    act(() => {
      fireEvent.click(getByText('txt_cancel'));
    });
  };

  return {
    openModalFromChoose,
    openModalFromEditCreate,
    openModalFromCreate,
    openModalFromEditChoose,
    getStrategyDetailForm,
    triggerBack,
    triggerClose,
    triggerSubmit
  };
};

describe('test StrategyDetails component', () => {
  it('should be render from edit choose modal', () => {
    const { openModalFromEditChoose, getStrategyDetailForm, triggerBack } =
      factory(strategyListFixture());
    openModalFromEditChoose();
    expect(getStrategyDetailForm()).toBeInTheDocument();
    triggerBack();
  });

  it('should be return back from choose modal', () => {
    const {
      openModalFromChoose,
      openModalFromEditChoose,
      getStrategyDetailForm,
      triggerBack
    } = factory(strategyListFixture());
    openModalFromChoose();
    expect(getStrategyDetailForm()).toBeInTheDocument();
    triggerBack();
    openModalFromEditChoose();
    triggerBack();
    openModalFromEditChoose();
    triggerBack();
    expect(getStrategyDetailForm()).not.toBeInTheDocument();
  });

  it('should be render from create new version', () => {
    const { openModalFromEditCreate, getStrategyDetailForm, triggerClose } =
      factory(strategyListFixture());
    openModalFromEditCreate();
    expect(getStrategyDetailForm()).toBeInTheDocument();
    triggerClose();
  });

  it('should be submit', () => {
    const { openModalFromEditChoose, triggerSubmit, getStrategyDetailForm } =
      factory(strategyListFixture());
    openModalFromEditChoose();
    triggerSubmit();
    expect(getStrategyDetailForm()).not.toBeInTheDocument();
  });

  it('should be submit and duplicated', () => {
    jest
      .spyOn(workflowSetup, 'useSelectStrategyData')
      .mockImplementation(() => [{ id: 'STT1', name: 'STT1', versions: [] }]);
    const { openModalFromEditChoose, triggerSubmit, getStrategyDetailForm } =
      factory(strategyListFixture());
    openModalFromEditChoose();
    triggerSubmit();
    expect(getStrategyDetailForm()).toBeInTheDocument();
  });

  it('should be submit from CREATE', () => {
    const { openModalFromCreate, triggerSubmit, getStrategyDetailForm } =
      factory(strategyListFixture());
    openModalFromCreate();
    triggerSubmit();
    expect(getStrategyDetailForm()).not.toBeInTheDocument();
  });

  it('should be submit from CREATE and duplicated', () => {
    const {
      openModalFromCreate,
      triggerSubmit,
      getStrategyDetailForm,
      triggerBack
    } = factory(strategyListFixture('STT1'));
    openModalFromCreate();
    triggerSubmit();
    expect(getStrategyDetailForm()).toBeInTheDocument();
    triggerBack();
  });
});
