import {
  Button,
  Icon,
  Radio,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import { orderBy } from 'app/_libraries/_dls/lodash';
import { View } from 'app/_libraries/_dof/core';
import React, { useMemo } from 'react';

export interface StrategyCardViewProps {
  id: string;
  name: string;
  versions: StrategyVersion[];
  isExpand: boolean;
  onToggle: () => void;
  versionSelected?: string;
  onVersionSelect: (
    selectedStrategy: { name: string; id: string },
    version: StrategyVersion
  ) => void;
}

const StrategyCardView: React.FC<StrategyCardViewProps> = ({
  id,
  name,
  isExpand,
  versions,
  versionSelected,
  onVersionSelect,
  onToggle
}) => {
  const { t } = useTranslation();
  const strategyCardViewValue = useMemo(
    () => ({
      name,
      numberOfVersions: t('txt_version', { count: versions.length })
    }),
    [name, t, versions.length]
  );

  const versionOrdered = orderBy(versions, ['effectiveDate'], ['desc']);
  return (
    <div
      id={`method-card-data-${id || 'empty'}`}
      className="mt-12 position-relative"
    >
      <div className="list-view method-card-view py-8">
        <View
          id={`strategy-card-view-header__${id}`}
          formKey={`strategy-card-view-header__${id}`}
          descriptor={`strategy-card-view`}
          value={strategyCardViewValue}
        />
        {isExpand && (
          <div className="bg-light-l16 rounded-bottom-8 py-8 mx-n16 px-16 mb-n8 mt-10">
            <h6 className="mb-16">
              {t('txt_pricing_strategies_and_assignments_strategy_versions')}
            </h6>
            {versionOrdered.map((v, index) => (
              <div
                key={`${v.id}-${index}`}
                className="list-view py-12 mt-8 bg-white position-relative bg-checkbox-hover-light-l20 custom-control-root"
                onClick={() => onVersionSelect({ name, id }, v)}
              >
                <View
                  id={`strategy-card-view-expanded__${id}${v.id}`}
                  formKey={`strategy-card-view-expanded__${id}${v.id}`}
                  descriptor={`strategy-card-view-expanded`}
                  value={v}
                />
                <Radio className="select-version-radio">
                  <Radio.Input
                    className="checked-style"
                    checked={versionSelected === v.id}
                  />
                </Radio>
              </div>
            ))}
          </div>
        )}
        <div className="strategy-card-expand">
          <Tooltip
            element={isExpand ? t('txt_collapse') : t('txt_expand')}
            variant="primary"
            placement="top"
          >
            <Button
              variant="outline-primary"
              className="strategy-card-expand-btn"
              onClick={onToggle}
            >
              <Icon
                name={isExpand ? 'minus' : 'plus'}
                className="mr-0 color-grey-l16"
                size="4x"
              />
            </Button>
          </Tooltip>
        </div>
      </div>
    </div>
  );
};

export { StrategyCardView };
