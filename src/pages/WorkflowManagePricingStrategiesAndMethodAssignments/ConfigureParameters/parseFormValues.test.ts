import parseFormValues from './parseFormValues';

const fixture = (hasStrategy?: boolean) => ({
  data: {
    workflowSetupData: [{ id: 'configureParameters' }],
    configurations: hasStrategy ? { strategies: [{}] } : {},
    workflowInstanceDetails: {}
  }
});

describe('test parseFormValues helper', () => {
  it('should be working correctly', () => {
    const actual = parseFormValues(
      fixture(true) as never,
      'configureParameters',
      jest.fn()
    );
    expect(actual?.isValid).toEqual(true);
  });

  it('should be return isValid as false', () => {
    const actual = parseFormValues(
      fixture(false) as never,
      'configureParameters',
      jest.fn()
    );
    expect(actual?.isValid).toEqual(false);
  });

  it('should be return undefined', () => {
    const actual = parseFormValues(fixture(true) as never, 'any id', jest.fn());
    expect(actual).toEqual(undefined);
  });
});
