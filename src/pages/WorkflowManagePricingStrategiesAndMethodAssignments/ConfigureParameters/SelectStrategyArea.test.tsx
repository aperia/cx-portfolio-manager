import { render } from '@testing-library/react';
import 'app/_libraries/_dls/test-utils/mocks/mockCanvas';
import { actionsWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import React from 'react';
import {
  SelectStrategyArea,
  STRATEGY_AREA_LIST,
  useSelectStrategyArea
} from './SelectStrategyArea';

jest.mock('app/hooks', () => ({
  useAsyncThunkDispatch: () => (fn: Function) => {
    fn();
  }
}));

const mGetPricingStrategyList = jest.fn();

beforeEach(() => {
  jest
    .spyOn(actionsWorkflowSetup, 'getPricingStrategyList')
    .mockImplementation(() => mGetPricingStrategyList);
});

const FakeComponent = () => {
  const [selected] = useSelectStrategyArea(STRATEGY_AREA_LIST[0]);
  return <SelectStrategyArea value={selected} />;
};

const factory = () => {
  const { getByText } = render(<FakeComponent />);

  const getTitle = () =>
    getByText('txt_pricing_strategies_and_assignments_select_strategy_area');
  return { getTitle };
};

describe('test SelectStrategyArea component', () => {
  it('should be working', () => {
    const { getTitle } = factory();
    expect(getTitle()).toBeInTheDocument();
  });
});
