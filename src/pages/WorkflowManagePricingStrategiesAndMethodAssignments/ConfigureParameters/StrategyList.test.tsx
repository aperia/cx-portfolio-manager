import { act, fireEvent, render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { OrderByItemProps } from 'app/components/SortAndOrder';
import { OrderBy } from 'app/constants/enums';
import 'app/_libraries/_dls/test-utils/mocks/mockCanvas';
import React, { useState } from 'react';
import { StrategyList, useStrategyList } from './StrategyList';

jest.mock('app/components/HeadingWithSearch', () => ({
  ...jest.requireActual('app/components/HeadingWithSearch'),
  HeadingWithSearch: ({
    searchValue,
    onSearchChange,
    onClearSearch
  }: {
    searchValue: string;
    onSearchChange: (s: string) => void;
    onClearSearch: (state?: unknown) => void;
  }) => (
    <div>
      <input
        data-testid="heading-with-search"
        value={searchValue}
        onChange={e => {
          onSearchChange(e.target.value);
        }}
      />
      {searchValue && (
        <>
          <button data-testid="clear-search" onClick={onClearSearch}></button>
          <button
            data-testid="clear-search-with-true"
            onClick={() => onClearSearch(true)}
          ></button>
        </>
      )}
    </div>
  )
}));

jest.mock('pages/_commons/Utils/Paging', () => ({
  __esModule: true,
  default: () => <div data-testid="Paging"></div>
}));

jest.mock('app/components/SortAndOrder/OnPC', () => ({
  __esModule: true,
  default: ({
    onOrderByChange,
    orderByValue
  }: {
    onOrderByChange: (o: OrderByItemProps) => void;
    orderByValue: { value: OrderBy; description: string };
  }) => (
    <div data-testid="SortOrDer">
      <button
        onClick={() => onOrderByChange({ description: '', value: 'desc' })}
        data-testid="SortOrDer_onOrderByChange"
      >
        onOrderByChange
      </button>
      <div data-testid="SortOrDer_orderBy">{orderByValue.value}</div>
    </div>
  )
}));

jest.mock('./StrategyCardView', () => ({
  StrategyCardView: ({
    onToggle,
    onVersionSelect
  }: {
    onToggle: () => void;
    onVersionSelect: (
      selectedStrategy: { name: string; id: string },
      versionData?: StrategyVersion
    ) => void;
  }) => (
    <div data-testid="StrategyCardView">
      <button data-testid="toggleCardView" onClick={onToggle}>
        ToggleCardView
      </button>
      <button
        data-testid="selectVersion"
        onClick={() => {
          onVersionSelect({ name: 'STT1', id: '1' });
        }}
      >
        selectVersion
      </button>
    </div>
  )
}));

jest.mock('app/components/NoDataFound', () => ({
  __esModule: true,
  default: () => <div data-testid="NoDataFound"></div>
}));

const FakeComponent = ({
  data,
  loading,
  versionSelected
}: {
  data: Strategy[];
  loading?: boolean;
  versionSelected?: string;
}) => {
  const [show, setShow] = useState(true);
  const strategyListProps = useStrategyList({
    data,
    show,
    error: false,
    loading,
    versionSelected
  });
  return (
    <>
      <button
        data-testid="toggleShow"
        onClick={() => {
          setShow(s => !s);
        }}
      >
        ToggleShow
      </button>
      <StrategyList {...strategyListProps} />
    </>
  );
};

const factory = (
  data: Strategy[],
  versionSelected?: string,
  loading?: boolean
) => {
  const { getByTestId, queryByTestId, getAllByTestId } = render(
    <FakeComponent
      data={data}
      loading={loading}
      versionSelected={versionSelected}
    />
  );

  const searchValueNotMatch = () => {
    const searchInput = getByTestId('heading-with-search');
    act(() => {
      fireEvent.change(searchInput, { target: { value: 'not match' } });
    });
  };

  const clearSearch = (withState?: unknown) => {
    if (withState) {
      act(() => {
        fireEvent.click(getByTestId('clear-search-with-true'));
      });
    } else {
      act(() => {
        fireEvent.click(getByTestId('clear-search'));
      });
    }
  };

  const selectVersion = () => {
    act(() => {
      fireEvent.click(getAllByTestId('selectVersion')[0]);
    });
  };

  const changeOrder = () => {
    act(() => {
      fireEvent.click(getByTestId('SortOrDer_onOrderByChange'));
    });
  };

  const getOrderBy = () => getByTestId('SortOrDer_orderBy');

  const getNodata = () => queryByTestId('NoDataFound');

  const getPaging = () => queryByTestId('Paging');

  const toggleShow = () => {
    fireEvent.click(getByTestId('toggleShow'));
  };

  const expandView = () => {
    act(() => {
      userEvent.click(getAllByTestId('toggleCardView')[0]);
    });
  };

  const collapseView = () => {
    act(() => {
      userEvent.click(getAllByTestId('toggleCardView')[0]);
    });
  };

  return {
    searchValueNotMatch,
    getNodata,
    getPaging,
    toggleShow,
    expandView,
    collapseView,
    changeOrder,
    getOrderBy,
    selectVersion,
    clearSearch
  };
};

const generateData = (length: number) => {
  return Array.from({ length }, (_, i) => ({
    id: `${i}`,
    name: `STT${i}`,
    versions: [
      {
        effectiveDate: '2022-02-02',
        accountsProcessedAA: '123',
        accountsProcessedALP: '1234',
        status: 'Production',
        cisDescription: '',
        commentArea: '',
        id: `vid-${i}`,
        methods: []
      }
    ]
  }));
};

describe('test StrategyList component', () => {
  it('should be render', () => {
    const { getNodata, expandView, collapseView } = factory(generateData(2));
    expandView();
    collapseView();
    expect(getNodata()).not.toBeInTheDocument();
  });

  it('should be render empty data', () => {
    const { getNodata } = factory([]);
    expect(getNodata()).toBeInTheDocument();
  });

  it('should be search data which is not match', () => {
    const { searchValueNotMatch, getNodata, clearSearch } = factory(
      generateData(2)
    );
    searchValueNotMatch();
    expect(getNodata()).toBeInTheDocument();
    clearSearch();
    expect(getNodata()).not.toBeInTheDocument();
    searchValueNotMatch();
    clearSearch(true);
    expect(getNodata()).not.toBeInTheDocument();
  });

  it('should be render pagination', () => {
    const { getPaging, toggleShow, selectVersion } = factory(generateData(12));
    toggleShow();
    selectVersion();
    expect(getPaging()).toBeInTheDocument();
  });

  it('should be change order', () => {
    const { changeOrder, getOrderBy } = factory(generateData(12), 'vid-2');
    changeOrder();
    expect(getOrderBy()).toHaveTextContent('desc');
  });
});
