import { fireEvent, render } from '@testing-library/react';
import 'app/utils/_mockComponent/mockComboBox';
import React from 'react';
import { MethodProvider } from './Context';
import { MethodListSubGrid, MethodListSubGridProps } from './MethodListSubGrid';
import { MethodItem } from './types';

const methodsFixture = { CP_IC_ID: ['METHOD3', 'METHOD2'] };

const dataFixture = [
  {
    id: 'CP_IC_IR',
    serviceSubjectSection: '',
    currentAssignment: 'METHOD2',
    newAssignment: 'METHOD2',
    moreInfoText: ''
  },
  {
    id: 'CP_IC_ID',
    serviceSubjectSection: '',
    currentAssignment: 'METHOD2',
    newAssignment: 'METHOD3',
    moreInfoText: ''
  }
];

jest.mock(
  'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManagePricingStrategiesAndMethodAssignments',
  () => ({
    useSelectMethods: () => methodsFixture
  })
);

const FakeComponent = ({
  methodList,
  data,
  isPreview
}: MethodListSubGridProps & { methodList: MethodItem[] }) => {
  return (
    <MethodProvider methodList={methodList}>
      <MethodListSubGrid data={data} isPreview={isPreview} />
    </MethodProvider>
  );
};

const factory = (
  methodList: MethodItem[],
  data: MethodListSubGridProps['data'],
  isPreview?: MethodListSubGridProps['isPreview']
) => {
  const { getByText, getAllByTestId } = render(
    <FakeComponent methodList={methodList} data={data} isPreview={isPreview} />
  );
  const onChangeAssignment = () => {
    const comboboxes = getAllByTestId('ComboBox.Input_onChange');
    fireEvent.change(comboboxes[0], { target: { value: 'METHOD3' } });
  };
  return {
    onChangeAssignment,
    getGrid: () => getByText('txt_service_subject_section')
  };
};

describe('test MethodListSubGrid component', () => {
  it('should be render', () => {
    const { getGrid } = factory([], dataFixture, false);
    expect(getGrid()).toBeInTheDocument();
  });

  it('should be render in review mode', () => {
    const { getGrid } = factory([], dataFixture, true);
    expect(getGrid()).toBeInTheDocument();
  });

  it('should be change assignment', () => {
    const { getGrid, onChangeAssignment } = factory([], dataFixture, false);
    onChangeAssignment();
    expect(getGrid()).toBeInTheDocument();
  });
});
