import { fireEvent, render } from '@testing-library/react';
import React from 'react';
import { SelectOption } from './SelectOption';

const mOnSelect = jest.fn();

const factory = () => {
  const { container } = render(<SelectOption onSelect={mOnSelect} />);
  const triggerSelect = () => {
    const optionButtons = container.getElementsByClassName('rcc-btn');
    fireEvent.click(optionButtons[0]);
  };
  return { triggerSelect };
};

describe('test SelectOption component', () => {
  it('should be working', () => {
    const { triggerSelect } = factory();
    triggerSelect();
    expect(mOnSelect).toBeCalled();
  });
});
