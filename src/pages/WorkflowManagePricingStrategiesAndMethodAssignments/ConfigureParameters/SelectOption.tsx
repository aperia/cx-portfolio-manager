import { Icon, IconProps, Tooltip, useTranslation } from 'app/_libraries/_dls';
import React from 'react';
import { OptionType } from './types';

interface Option {
  iconName: IconProps['name'];
  title: string;
  tooltip: string;
  name: OptionType;
}
export interface OptionButtonProps extends Option {
  onClick: (name: OptionType) => void;
}

export interface SelectOption {
  onSelect: (name: OptionType) => void;
}

const OptionButton: React.FC<OptionButtonProps> = ({
  onClick,
  iconName,
  title,
  tooltip,
  name
}) => {
  const { t } = useTranslation();
  return (
    <div
      className="rcc-btn d-flex justify-content-between border rounded-lg py-10"
      onClick={() => onClick(name)}
    >
      <span className="d-flex align-items-center ml-16">
        <Icon name={iconName} size="9x" className="color-grey-l16" />
        <span className="ml-12 mr-8">{t(title)}</span>
        <Tooltip element={t(tooltip)} triggerClassName="d-flex">
          <Icon name="information" className="color-grey-l16" size="5x" />
        </Tooltip>
      </span>
    </div>
  );
};

export const OPTIONS: Option[] = [
  {
    iconName: 'method-create',
    title: 'txt_create_new_version',
    tooltip:
      'txt_pricing_strategies_and_assignments_create_new_version_tooltip',
    name: 'CREATE'
  },
  {
    iconName: 'method-clone',
    title: 'txt_choose_strategies_to_model',
    tooltip: 'txt_choose_strategies_to_model_tooltip',
    name: 'CHOOSE'
  }
];

export const SelectOption: React.FC<SelectOption> = ({ onSelect }) => {
  const { t } = useTranslation();
  return (
    <div className="mt-24">
      <h5>{t('txt_pricing_strategies_and_assignments_select_an_option')}</h5>
      <div className="row mt-16">
        {OPTIONS.map(opt => (
          <div key={opt.name} className="col-4">
            <OptionButton {...opt} onClick={onSelect} />
          </div>
        ))}
      </div>
    </div>
  );
};
