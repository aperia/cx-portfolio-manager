import { HeadingWithSearch } from 'app/components/HeadingWithSearch';
import NoDataFound from 'app/components/NoDataFound';
import { OrderByItemProps } from 'app/components/SortAndOrder';
import SortOrder from 'app/components/SortAndOrder/OnPC';
import { ORDER_BY_LIST } from 'app/constants/constants';
import { OrderBy } from 'app/constants/enums';
import { classnames } from 'app/helpers';
import { usePagination } from 'app/hooks/usePagination';
import { useTranslation } from 'app/_libraries/_dls';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import Paging from 'pages/_commons/Utils/Paging';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { StrategyCardView } from './StrategyCardView';

export type StrategyListProps = ReturnType<typeof useStrategyList>;

const StrategyList: React.FC<StrategyListProps> = props => {
  const { t } = useTranslation();
  const {
    onToggle,
    isExpanded,
    versionSelected,
    onVersionSelect,
    total,
    pagingProps,
    showPagination,
    orderBy,
    onOrderByChange,
    searchValue,
    data,
    gridData,
    onSearchChange,
    onResetSearch,
    loading
  } = props;

  return (
    <div className={classnames('mt-24 pt-24 border-top', { loading })}>
      <HeadingWithSearch
        txtTitle="txt_strategy_list"
        searchDescription={[]}
        searchDescriptionString="txt_strategy_search_description"
        searchValue={searchValue}
        onSearchChange={onSearchChange}
        onClearSearch={onResetSearch}
        isDataEmpty={!gridData.length}
      />
      {total > 0 && (
        <div className="d-flex justify-content-end align-items-center my-16">
          <SortOrder
            sortByFields={[]}
            orderByValue={orderBy}
            onOrderByChange={_o => onOrderByChange(_o!)}
            isHideSort={true}
          />
        </div>
      )}
      {isEmpty(data) && !loading && (
        <NoDataFound
          id="newMethod_notfound"
          title={t('txt_no_data_to_display')}
          className="mt-40"
        />
      )}
      {!isEmpty(data) && isEmpty(gridData) && searchValue && !loading && (
        <div className="d-flex flex-column justify-content-center mt-40 mb-24">
          <NoDataFound
            id="newMethod_notfound"
            hasSearch
            title={t('txt_no_results_found')}
            linkTitle={t('txt_clear_and_reset')}
            onLinkClicked={onResetSearch}
          />
        </div>
      )}

      {gridData.map(strategy => (
        <StrategyCardView
          id={strategy.id}
          versionSelected={versionSelected}
          onVersionSelect={onVersionSelect}
          onToggle={() => onToggle(strategy.name)}
          isExpand={isExpanded(strategy.name)}
          key={strategy.id}
          versions={strategy.versions}
          name={strategy.name}
        />
      ))}
      {showPagination && (
        <div className="mt-16">
          <Paging {...pagingProps} />
        </div>
      )}
    </div>
  );
};

const ORDER_FIELD = 'name';
const FILTER_FIELD = ['name'];

const useStrategyList = ({
  data,
  show,
  error,
  loading = false,
  versionSelected: versionSelectedProp
}: {
  data: Strategy[];
  show: boolean;
  error?: boolean;
  loading?: boolean;
  versionSelected?: string;
}) => {
  const [expandedList, setExpandedList] = useState<string[]>([]);
  const [versionSelectedData, setVersionSelectedData] =
    useState<StrategyVersion>();
  const [selectedStrategy, setSelectedStrategy] = useState({
    name: '',
    id: ''
  });

  const {
    sort,
    total,
    currentPage,
    currentPageSize,
    pageSize,
    searchValue,
    gridData,
    onSortChange,
    onPageChange,
    onPageSizeChange,
    onSearchChange,
    setCurrentPage,
    navigateToItemAndResetDefault
  } = usePagination(data, FILTER_FIELD, ORDER_FIELD);

  const onToggle = useCallback((strategyName: string) => {
    setExpandedList(prev =>
      prev.includes(strategyName)
        ? prev.filter(st => !st.includes(strategyName))
        : [...prev, strategyName]
    );
  }, []);

  const onResetSearch = useCallback(
    (fromBack?: unknown) => {
      /**
       * Order remains the same.
       * All the others (Search, Truncated state) is reset to default
       * Auto navigate to the page a version is selected.
       * If no version is selected, auto navigate to the 1st page.
       * Auto expand the strategy whose version is currently selected and collapse all other strategies if any
       */
      const strategyHasSelected = selectedStrategy.name;
      navigateToItemAndResetDefault(
        'name',
        strategyHasSelected,
        fromBack === true ? false : true
        // fromBack be able to an event
        // don't refactor to !fromBack
      );
      setExpandedList([selectedStrategy.name]);
    },
    [navigateToItemAndResetDefault, selectedStrategy.name]
  );

  const onVersionSelect = useCallback(
    (
      selectedStrategy: { name: string; id: string },
      versionData?: StrategyVersion
    ) => {
      setVersionSelectedData(versionData);
      setSelectedStrategy(selectedStrategy);
    },
    []
  );

  const isExpanded = useCallback(
    (strategyName: string) => expandedList.includes(strategyName),
    [expandedList]
  );

  const pagingProps = useMemo(
    () => ({
      page: currentPage,
      pageSize: currentPageSize,
      totalItem: total,
      onChangePage: onPageChange,
      onChangePageSize: onPageSizeChange
    }),
    [currentPage, currentPageSize, onPageChange, onPageSizeChange, total]
  );

  const showPagination = useMemo(() => total > pageSize, [pageSize, total]);

  const orderBy = useMemo(() => {
    const orderValue = sort.order ?? OrderBy.ASC;
    return {
      value: orderValue as OrderBy,
      description: ORDER_BY_LIST.find(l => l.value === (orderValue as OrderBy))!
        .description
    };
  }, [sort.order]);
  const onOrderByChange = useCallback(
    (_order: OrderByItemProps) => {
      onSortChange({ id: ORDER_FIELD, order: _order.value });
    },
    [onSortChange]
  );

  useEffect(() => {
    // reset to default after close
    setExpandedList([]);
    setVersionSelectedData(undefined);
    setSelectedStrategy({ name: '', id: '' });
    onSortChange({ id: ORDER_FIELD, order: OrderBy.ASC });
    !show && setCurrentPage(1);
  }, [onSortChange, setCurrentPage, show]);

  useEffect(() => {
    const _strategySelectedData = data.find(item =>
      item.versions.some(v => v.id === versionSelectedProp)
    );
    if (!_strategySelectedData) {
      return;
    }
    const { name, id, versions } = _strategySelectedData;
    setExpandedList([name]);
    setSelectedStrategy({ name, id });
    setVersionSelectedData(versions.find(v => v.id === versionSelectedProp));
  }, [data, versionSelectedProp]);

  return {
    onToggle,
    isExpanded,
    versionSelected: versionSelectedData?.id,
    selectedStrategy,
    versionSelectedData,
    onVersionSelect,
    total,
    pagingProps,
    showPagination,
    orderBy,
    searchValue,
    data,
    gridData,
    onOrderByChange,
    onSearchChange,
    onResetSearch,
    loading
  };
};

export { StrategyList, useStrategyList };
