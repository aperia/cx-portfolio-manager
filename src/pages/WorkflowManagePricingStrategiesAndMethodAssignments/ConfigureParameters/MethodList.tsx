import { HeadingWithSearch } from 'app/components/HeadingWithSearch';
import NoDataFound from 'app/components/NoDataFound';
import { matchSearchValue } from 'app/helpers';
import { useTranslation } from 'app/_libraries/_dls';
import React, { useMemo, useState } from 'react';
import { MethodListGrid } from './MethodListGrid';
import { MethodItem } from './types';

interface MethodListProps {
  list: MethodItem[];
}

const MethodList: React.FC<MethodListProps> = ({ list }) => {
  const { t } = useTranslation();
  const [searchValue, setSearchValue] = useState('');

  const onClearSearch = () => {
    setSearchValue('');
  };

  const dataFiltered = useMemo(() => {
    return list.reduce((acc, curr) => {
      const newCurr = { ...curr };
      newCurr.subData = curr.subData.filter(s =>
        matchSearchValue(
          [
            s.currentAssignment,
            s.newAssignment,
            s.serviceSubjectSection,
            s.moreInfoText
          ],
          searchValue
        )
      );
      if (newCurr.subData.length) {
        acc.push(newCurr);
      }
      return acc;
    }, [] as MethodItem[]);
  }, [list, searchValue]);

  const showNodata = searchValue && !dataFiltered.length;

  return (
    <div className="mt-24">
      <HeadingWithSearch
        txtTitle="txt_method_list"
        searchDescription={[
          'txt_current_assignment',
          'txt_more_info',
          'txt_new_assignment',
          'txt_service_subject_section'
        ]}
        searchValue={searchValue}
        onSearchChange={setSearchValue}
        isDataEmpty={!dataFiltered.length}
        onClearSearch={onClearSearch}
      />
      {!showNodata && <MethodListGrid data={dataFiltered} />}
      {showNodata && (
        <NoDataFound
          id="strategy_details_not_found"
          title={t('txt_no_results_found')}
          hasSearch
          linkTitle={t('txt_clear_and_reset')}
          onLinkClicked={onClearSearch}
        />
      )}
    </div>
  );
};

export type { MethodListProps };
export { MethodList };
