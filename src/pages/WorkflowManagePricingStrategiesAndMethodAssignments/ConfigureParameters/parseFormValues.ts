import { getWorkflowSetupStepStatus } from 'app/helpers';
import { nanoid } from 'nanoid';
import { ConfigureParametersFormValues } from '.';

const parseFormValues: WorkflowSetupStepFormDataFunc<ConfigureParametersFormValues> =
  (data, id) => {
    const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);
    if (!stepInfo) return;

    const strategyList = data.data.configurations?.strategies || [];

    return {
      ...getWorkflowSetupStepStatus(stepInfo),
      isValid: strategyList.length > 0,
      strategyList: strategyList.map(item => ({ ...item, unique: nanoid() }))
    };
  };

export default parseFormValues;
