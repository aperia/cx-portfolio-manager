import { BadgeColorType } from 'app/constants/enums';
import { mapGridExpandCollapse } from 'app/helpers';
import { usePagination } from 'app/hooks/usePagination';
import { Button, ColumnType, Grid, useTranslation } from 'app/_libraries/_dls';
import { DEFAULT_PAGE_SIZE } from 'app/_libraries/_dls/components/Pagination/constants';
import {
  formatBadge,
  formatTruncate
} from 'pages/_commons/Utils/formatGridField';
import Paging from 'pages/_commons/Utils/Paging';
import React, { useImperativeHandle, useMemo } from 'react';
import { formatStrategyPreviewListData } from './helpers';
import { MethodListGrid } from './MethodListGrid';
import {
  RemoveStrategyConfirm,
  useRemoveStrategy
} from './RemoveStrategyConfirm';
import {
  StrategyListFormValues,
  StrategyPreviewListGridRef,
  StrategyPreviewListItem
} from './types';

interface StrategyPreviewListGridProps {
  strategyList: StrategyListFormValues[];
  isPreview?: boolean;
  onRemove?: (name: string) => void;
  onUpdate?: (values: StrategyPreviewListItem) => void;
  expanded: string[];
  onExpand: (list: string[]) => void;
}

const StrategyPreviewListGrid = React.forwardRef<
  StrategyPreviewListGridRef,
  StrategyPreviewListGridProps
>((props, ref) => {
  const { strategyList, isPreview, onRemove, onUpdate, expanded, onExpand } =
    props;
  const { t } = useTranslation();

  const { openRemoveStrategyConfirm, ...removeStrategyConfirmProps } =
    useRemoveStrategy();

  const handleEdit = (row: StrategyPreviewListItem) => {
    onUpdate && onUpdate(row);
  };

  const handleRemove = (uniqueId: string) => {
    openRemoveStrategyConfirm(() => {
      handleInvokeRemove(uniqueId);
    });
  };
  const handleInvokeRemove = (uniqueId: string) => {
    onRemove && onRemove(uniqueId);
  };
  const originalData = useMemo(
    () => formatStrategyPreviewListData(strategyList),
    [strategyList]
  );

  const {
    total,
    currentPage,
    currentPageSize,
    gridData,
    onPageChange,
    onPageSizeChange,
    navigateToItem,
    setCurrentPage
  } = usePagination(originalData, []);

  const columns = [
    {
      id: 'strategyName',
      Header: t('txt_strategy_name'),
      accessor: 'strategyName',
      width: 120
    },
    {
      id: 'modeled',
      Header: t('txt_modeled_created_from'),
      accessor: 'modeled',
      width: 128
    },
    {
      id: 'cisDescription',
      Header: t('txt_cis_description'),
      accessor: formatTruncate(['cisDescription']),
      width: 162
    },
    {
      id: 'commentArea',
      Header: t('txt_comment_area'),
      accessor: formatTruncate(['commentArea']),
      width: 163
    },
    {
      id: 'actionTaken',
      Header: t('txt_action_taken'),
      accessor: formatBadge(['actionTaken'], {
        colorType: BadgeColorType.ActionTaken,
        noBorder: true
      }),
      width: 153
    },
    !isPreview && {
      id: 'actions',
      Header: t('txt_actions'),
      accessor: (row: StrategyPreviewListItem) => (
        <div className="d-flex justify-content-center">
          <Button
            size="sm"
            variant="outline-primary"
            onClick={() => handleEdit(row)}
          >
            {t('txt_edit')}
          </Button>
          <Button
            size="sm"
            variant="outline-danger"
            className="ml-8"
            onClick={() => handleRemove(row.uniqueId)}
          >
            {t('txt_delete')}
          </Button>
        </div>
      ),
      className: 'text-center',
      cellBodyProps: { className: 'py-8' },
      width: 132
    }
  ].filter(Boolean) as ColumnType[];

  useImperativeHandle(
    ref,
    () => ({
      navigateToItem: (uniqueId: string) =>
        navigateToItem('uniqueId', uniqueId),
      resetToPage1: () => {
        setCurrentPage(1);
      }
    }),
    [navigateToItem, setCurrentPage]
  );

  return (
    <>
      <Grid
        togglable
        expandedList={expanded}
        onExpand={onExpand}
        dataItemKey="uniqueId"
        rowKey="uniqueId"
        expandedItemKey="uniqueId"
        toggleButtonConfigList={gridData.map(
          mapGridExpandCollapse('uniqueId', t)
        )}
        subRow={({ original }: { original: StrategyPreviewListItem }) => (
          <div className="p-20">
            <MethodListGrid data={original.serviceSubject} isPreview />
          </div>
        )}
        columns={columns}
        data={gridData}
      />
      {total > DEFAULT_PAGE_SIZE && (
        <Paging
          totalItem={total}
          pageSize={currentPageSize}
          page={currentPage}
          onChangePage={onPageChange}
          onChangePageSize={onPageSizeChange}
        />
      )}
      {!isPreview && <RemoveStrategyConfirm {...removeStrategyConfirmProps} />}
    </>
  );
});

export type { StrategyPreviewListGridProps };
export { StrategyPreviewListGrid };
