import { renderHook } from '@testing-library/react-hooks';
import { act } from 'react-dom/test-utils';
import { OptionType } from './types';
import { useOptionModal } from './useOptionModal';

const factory = () => {
  const { result } = renderHook(() => useOptionModal());

  const selectOption = (opt: OptionType) => {
    act(() => {
      result.current.onOptionSelect(opt);
    });
  };

  const resetToDefault = () => {
    act(() => {
      result.current.resetToDefault();
    });
  };

  const getShowCreateModal = () => result.current.showCreateModal;
  const getChooseStrategyModal = () => result.current.showChooseStrategyModal;

  return {
    selectOption,
    resetToDefault,
    getShowCreateModal,
    getChooseStrategyModal
  };
};

describe('test useOptionModal hook', () => {
  it('should be working', () => {
    const {
      selectOption,
      getChooseStrategyModal,
      getShowCreateModal,
      resetToDefault
    } = factory();
    selectOption('CHOOSE');
    expect(getChooseStrategyModal()).toEqual(true);
    resetToDefault();
    expect(getChooseStrategyModal()).toEqual(false);
    selectOption('CREATE');
    expect(getShowCreateModal()).toEqual(true);
  });
});
