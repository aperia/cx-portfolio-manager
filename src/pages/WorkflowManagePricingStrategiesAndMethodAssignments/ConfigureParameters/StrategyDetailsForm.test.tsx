import { act, fireEvent, render } from '@testing-library/react';
import 'app/_libraries/_dls/test-utils/mocks/mockCanvas';
import React from 'react';
import {
  StrategyDetailsForm,
  useStrategyDetailsForm
} from './StrategyDetailsForm';
import { OptionType, StrategyDetailsFormValues } from './types';

const FakeComponent = ({
  initValues,
  openedFrom
}: {
  initValues: StrategyDetailsFormValues;
  openedFrom?: OptionType;
}) => {
  const {
    formError,
    handleSetDuplicated,
    handleSetVersionDuplicated,
    ...strategyDetailsFormProps
  } = useStrategyDetailsForm({
    openedFrom,
    initValues
  });

  return (
    <>
      {formError && <div data-testid="form-error" />}
      <button
        data-testid="setDuplicated"
        onClick={() => handleSetDuplicated(true)}
      >
        set-duplicated
      </button>
      <button
        data-testid="setVersionDuplicated"
        onClick={() => handleSetVersionDuplicated(true)}
      ></button>
      <StrategyDetailsForm {...strategyDetailsFormProps} />
    </>
  );
};

const factory = (
  initValues: StrategyDetailsFormValues,
  openedFrom?: OptionType
) => {
  const { queryByTestId, getByTestId, queryByText } = render(
    <FakeComponent initValues={initValues} openedFrom={openedFrom} />
  );

  const getFormError = () => queryByTestId('form-error');

  const getStrategyNameField = () =>
    queryByTestId('strategyDetails__strategyName');

  const onChangeStrategyName = (text: string) => {
    const control = getStrategyNameField();
    act(() => {
      fireEvent.change(control!, { target: { value: text } });
    });
    act(() => {
      fireEvent.blur(control!);
    });
  };

  const onChangeCISDescription = (text: string) => {
    const control = queryByTestId(
      'strategyDetails__cisDescription_dls-text-area_input'
    );
    act(() => {
      fireEvent.change(control!, { target: { value: text } });
    });
    act(() => {
      fireEvent.blur(control!);
    });
  };

  const triggerSetDuplicated = () => {
    fireEvent.click(getByTestId('setDuplicated'));
  };

  const getDuplicateErrMsg = () =>
    queryByText('txt_strategy_name_duplicated_error_msg');

  const triggerSetVersionDuplicated = () => {
    fireEvent.click(getByTestId('setVersionDuplicated'));
  };

  const getVersionDuplicateErrMsg = () =>
    queryByText('txt_create_new_version_duplicated_error_msg');
  return {
    getFormError,
    onChangeStrategyName,
    onChangeCISDescription,
    getStrategyNameField,
    triggerSetDuplicated,
    getDuplicateErrMsg,
    triggerSetVersionDuplicated,
    getVersionDuplicateErrMsg
  };
};

const generateInitValue = (
  selectedStrategy: { name: string; id: string },
  strategyName = ''
) => ({
  selectedStrategy,
  strategyName: strategyName,
  cisDescription: '',
  comment: ''
});

describe('test StrategyDetailsForm component', () => {
  it('should be render create form', () => {
    const { getStrategyNameField } = factory(
      generateInitValue({ name: 'STT', id: '1' }),
      'CREATE'
    );
    expect(getStrategyNameField()).not.toBeInTheDocument();
  });

  it('should be render choose form', () => {
    const { getFormError, onChangeStrategyName, onChangeCISDescription } =
      factory(generateInitValue({ name: 'STT', id: '1' }), 'CHOOSE');
    onChangeStrategyName('STT2');
    onChangeCISDescription('some thing description');
    expect(getFormError()).not.toBeInTheDocument();
  });

  it('should be render edit choose form', () => {
    const { getFormError, onChangeStrategyName } = factory(
      generateInitValue({ name: 'STT', id: '1' }, 'STT3'),
      'EDIT_CHOOSE'
    );
    onChangeStrategyName('STT2');
    expect(getFormError()).not.toBeInTheDocument();
    onChangeStrategyName('');
    expect(getFormError()).toBeInTheDocument();
  });

  it('should be duplicate Name', () => {
    const { triggerSetDuplicated, getDuplicateErrMsg } = factory(
      generateInitValue({ name: 'STT', id: '1' }, 'STT3'),
      'EDIT_CHOOSE'
    );
    triggerSetDuplicated();
    expect(getDuplicateErrMsg()).toBeInTheDocument();
  });

  it('should be version duplicated', () => {
    const { triggerSetVersionDuplicated, getVersionDuplicateErrMsg } = factory(
      generateInitValue({ name: 'STT', id: '1' }, 'STT3'),
      'EDIT_CHOOSE'
    );
    triggerSetVersionDuplicated();
    expect(getVersionDuplicateErrMsg()).toBeInTheDocument();
  });
});
