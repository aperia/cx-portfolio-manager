import { render } from '@testing-library/react';
import React from 'react';
import { MethodListGrid } from './MethodListGrid';
import { MethodItem } from './types';

jest.mock('./MethodListSubGrid', () => ({
  MethodListSubGrid: () => <div data-testid="MethodListSubGrid"></div>
}));

const fixture = [
  {
    rowId: 'IC',
    service: 'CP',
    subject: 'IC',
    subData: [
      {
        id: 'CP_IC_IR',
        serviceSubjectSection: '',
        currentAssignment: 'METHOD2',
        newAssignment: 'METHOD2',
        moreInfoText: ''
      },
      {
        id: 'CP_IC_ID',
        serviceSubjectSection: '',
        currentAssignment: 'METHOD2',
        newAssignment: 'METHOD2',
        moreInfoText: ''
      }
    ]
  }
];
const factory = (data: MethodItem[]) => {
  const { getByTestId } = render(<MethodListGrid data={data} />);
  return { getMethodListSubGrid: () => getByTestId('MethodListSubGrid') };
};

describe('test MethodListGrid component', () => {
  it('should be render', () => {
    const { getMethodListSubGrid } = factory(fixture);
    expect(getMethodListSubGrid()).toBeInTheDocument();
  });
});
