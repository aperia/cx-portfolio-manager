import { fireEvent, render } from '@testing-library/react';
import React from 'react';
import { emptyFunction, MethodProvider, useMethodContext } from './Context';
import { MethodItem } from './types';

const FakeChildComponent = () => {
  const { onChangeAssignment } = useMethodContext();
  return (
    <button
      data-testid="trigger-change-assignment"
      onClick={() => {
        onChangeAssignment('CP_IC_ID', 'METHOD1');
      }}
    >
      change assignment
    </button>
  );
};

const FakeComponent = ({ methodList }: { methodList: MethodItem[] }) => {
  return (
    <MethodProvider methodList={methodList}>
      <FakeChildComponent />
    </MethodProvider>
  );
};

const fixture1 = [
  {
    rowId: 'IC',
    service: 'CP',
    subject: 'IC',
    subData: [
      {
        id: 'CP_IC_IR',
        serviceSubjectSection: '',
        currentAssignment: 'METHOD2',
        newAssignment: 'METHOD2',
        moreInfoText: ''
      },
      {
        id: 'CP_IC_ID',
        serviceSubjectSection: '',
        currentAssignment: 'METHOD2',
        newAssignment: 'METHOD2',
        moreInfoText: ''
      }
    ]
  }
];

const fixture2 = [
  {
    rowId: 'IC',
    service: 'CP',
    subject: 'IC',
    subData: [
      {
        id: 'CP_IC_IR',
        serviceSubjectSection: '',
        currentAssignment: 'METHOD2',
        newAssignment: 'METHOD2',
        moreInfoText: ''
      }
    ]
  }
];

const factory = (methodList: MethodItem[]) => {
  const { getByTestId } = render(<FakeComponent methodList={methodList} />);
  const getChildComponent = () => getByTestId('trigger-change-assignment');
  return {
    triggerChangeAssignment: () => {
      fireEvent.click(getChildComponent());
    },
    getChildComponent
  };
};

describe('test Context component', () => {
  it('should be render', () => {
    const { triggerChangeAssignment, getChildComponent } = factory([]);
    triggerChangeAssignment();
    expect(getChildComponent()).toBeInTheDocument();
  });

  it('should be render with data', () => {
    const { triggerChangeAssignment, getChildComponent } = factory(fixture1);
    triggerChangeAssignment();
    expect(getChildComponent()).toBeInTheDocument();
  });

  it('should be render with no data', () => {
    emptyFunction();
    const { triggerChangeAssignment, getChildComponent } = factory(fixture2);
    triggerChangeAssignment();
    expect(getChildComponent()).toBeInTheDocument();
  });
});
