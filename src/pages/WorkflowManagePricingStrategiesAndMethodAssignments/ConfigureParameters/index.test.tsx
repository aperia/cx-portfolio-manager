import { fireEvent, render } from '@testing-library/react';
import { act } from '@testing-library/react-hooks';
import 'app/_libraries/_dls/test-utils/mocks/mockCanvas.ts';
import React from 'react';
import ConfigureParametersStep, {
  ConfigureParametersFormValues,
  FormValuesAction
} from '.';
import { StrategyPreviewListItem } from './types';

jest.mock('./ChooseOrCreateModal', () => ({
  ChooseOrCreateModal: () => <div data-testid="ChooseOrCreateModal"></div>
}));

jest.mock('./Context', () => ({
  MethodProvider: ({ children }: { children: React.ReactNode }) => (
    <div data-testid="MethodProvider">{children}</div>
  )
}));

jest.mock('./SelectOption', () => ({
  SelectOption: () => <div data-testid="SelectOption"></div>
}));

jest.mock('./StrategyDetails', () => ({
  useStrategyDetails: ({
    onCloseCb,
    onBackCb,
    dispatch
  }: {
    onCloseCb: () => void;
    onBackCb: (version?: string) => void;
    dispatch: React.Dispatch<FormValuesAction>;
    versionSelected?: string;
  }) => {
    return {
      openStrategyDetails: jest.fn(),
      onClose: onCloseCb,
      onBack: onBackCb,
      dispatch
    };
  },
  StrategyDetails: ({
    onClose,
    onBack,
    dispatch
  }: {
    onClose: () => void;
    onBack: (version?: string) => void;
    dispatch: React.Dispatch<FormValuesAction>;
  }) => (
    <div data-testid="StrategyDetails">
      <button
        data-testid="StrategyDetails_onClose"
        onClick={() => {
          onClose();
        }}
      ></button>
      <button
        data-testid="StrategyDetails_onBack"
        onClick={() => {
          onBack('vid');
        }}
      ></button>
      <button
        data-testid="StrategyDetails_triggerUpdate"
        onClick={() => {
          dispatch({
            type: 'UPDATE_STRATEGY',
            payload: {
              unique: '12',
              name: 'STT2',
              modeledFrom: { name: 'STT1', id: '2' },
              fromVersion: '222',
              cisDescription: '',
              commentArea: '',
              methods: [
                {
                  serviceSubjectSection: 'CP IC ID',
                  previousValue: 'Method1',
                  newValue: 'Method2'
                }
              ]
            }
          });
        }}
      ></button>
      <button
        data-testid="StrategyDetails_triggerAddNew"
        onClick={() => {
          dispatch({
            type: 'ADD_STRATEGY_TO_LIST',
            payload: {
              unique: '13',
              name: 'STT4',
              modeledFrom: { name: 'STT1', id: '2' },
              fromVersion: '222',
              cisDescription: '',
              commentArea: '',
              methods: [
                {
                  serviceSubjectSection: 'CP IC ID',
                  previousValue: 'Method1',
                  newValue: 'Method2'
                }
              ]
            }
          });
        }}
      ></button>
      <button
        data-testid="StrategyDetails_triggerResetTruncate"
        onClick={() => {
          dispatch({
            type: 'RESET_TRUNCATE_LIST'
          });
        }}
      ></button>
    </div>
  )
}));

jest.mock('./StrategyPreviewList', () => ({
  StrategyPreviewList: ({
    onExpand,
    onRemove,
    onUpdate
  }: {
    onExpand: (list: string[]) => void;
    onRemove: (name: string) => void;
    onUpdate: (values: StrategyPreviewListItem) => void;
  }) => (
    <div data-testid="StrategyPreviewList">
      <button
        data-testid="StrategyPreviewList_onExpand"
        onClick={() => onExpand([])}
      ></button>
      <button
        data-testid="StrategyPreviewList_onRemove"
        onClick={() => onRemove('STT')}
      ></button>
      <button
        data-testid="StrategyPreviewList_onUpdate"
        onClick={() =>
          onUpdate({
            uniqueId: '12',
            strategyName: 'STT',
            fromVersion: '222',
            cisDescription: '',
            commentArea: ''
          } as never)
        }
      ></button>
    </div>
  )
}));

jest.mock('./Summary', () => ({
  __esModule: true,
  default: () => <div data-testid="Summary"></div>
}));

jest.mock('./useOptionModal', () => ({
  useOptionModal: () => ({
    onOptionSelect: jest.fn(),
    resetToDefault: jest.fn()
  })
}));

jest.mock('app/hooks/useUnsavedChangeRegistry', () => {
  return {
    __esModule: true,
    useUnsavedChangeRegistry: (options: any, changes: any, onConfirm: any) => {
      onConfirm && onConfirm();
    }
  };
});

const factory = (props: WorkflowSetupProps<ConfigureParametersFormValues>) => {
  const { getByTestId, queryByTestId, getByText } = render(
    <ConfigureParametersStep {...props} />
  );

  const getChooseOrCreate = () => getByTestId('ChooseOrCreateModal');

  const getStrategyPreviewList = () => queryByTestId('StrategyPreviewList');

  const getSelectOption = () => queryByTestId('SelectOption');

  const getStuckErrorMsg = () =>
    getByText('txt_step_stuck_move_forward_message');

  const triggerExpand = () => {
    act(() => {
      fireEvent.click(getByTestId('StrategyPreviewList_onExpand'));
    });
  };

  const triggerRemove = () => {
    act(() => {
      fireEvent.click(getByTestId('StrategyPreviewList_onRemove'));
    });
  };

  const triggerAddNewStrategy = () => {
    jest.useFakeTimers();
    act(() => {
      fireEvent.click(getByTestId('StrategyDetails_triggerAddNew'));
    });
    jest.runAllTimers();
  };

  const triggerResetTruncateList = () => {
    act(() => {
      fireEvent.click(getByTestId('StrategyDetails_triggerResetTruncate'));
    });
  };

  const triggerUpdateRequest = () => {
    act(() => {
      fireEvent.click(getByTestId('StrategyPreviewList_onUpdate'));
    });
  };

  const triggerUpdate = () => {
    act(() => {
      fireEvent.click(getByTestId('StrategyDetails_triggerUpdate'));
    });
  };

  const triggerOnClose = () => {
    act(() => {
      fireEvent.click(getByTestId('StrategyDetails_onClose'));
    });
  };

  const triggerOnBack = () => {
    act(() => {
      fireEvent.click(getByTestId('StrategyDetails_onBack'));
    });
  };

  return {
    getChooseOrCreate,
    getSelectOption,
    getStrategyPreviewList,
    getStuckErrorMsg,
    triggerExpand,
    triggerRemove,
    triggerUpdateRequest,
    triggerOnClose,
    triggerOnBack,
    triggerUpdate,
    triggerAddNewStrategy,
    triggerResetTruncateList
  };
};

const strategyListFixture = [
  {
    unique: '12',
    name: 'STT',
    modeledFrom: { name: 'STT1', id: '2' },
    fromVersion: '222',
    cisDescription: '',
    commentArea: '',
    methods: [
      {
        serviceSubjectSection: 'CP IC ID',
        previousValue: 'Method1',
        newValue: 'Method2'
      }
    ]
  }
];

describe('test ConfigureParametersStep component', () => {
  it('should be render', () => {
    const { getSelectOption, getStrategyPreviewList, triggerOnClose } = factory(
      {
        stepId: 'step1',
        selectedStep: 'step1',
        isEdit: false,
        savedAt: 1,
        formValues: { strategyList: [], isValid: true },
        setFormValues: jest.fn(),
        clearFormValues: jest.fn,
        handleSave: jest.fn()
      }
    );
    expect(getSelectOption()).toBeInTheDocument();
    expect(getStrategyPreviewList()).not.toBeInTheDocument();
    triggerOnClose();
  });

  it('should be render expand', () => {
    const { triggerExpand, getStrategyPreviewList, triggerOnBack } = factory({
      stepId: 'step1',
      selectedStep: 'step1',
      isEdit: false,
      savedAt: 1,
      formValues: { strategyList: strategyListFixture, isValid: true },
      setFormValues: jest.fn(),
      clearFormValues: jest.fn,
      handleSave: jest.fn()
    });
    triggerExpand();
    expect(getStrategyPreviewList()).toBeInTheDocument();
    triggerOnBack();
  });

  it('should be render and trigger remove', () => {
    const { triggerRemove, getStrategyPreviewList, triggerAddNewStrategy } =
      factory({
        stepId: 'step1',
        selectedStep: 'step1',
        isEdit: false,
        savedAt: 1,
        formValues: { strategyList: strategyListFixture, isValid: true },
        setFormValues: jest.fn(),
        clearFormValues: jest.fn,
        handleSave: jest.fn()
      });
    triggerRemove();
    triggerAddNewStrategy();
    expect(getStrategyPreviewList()).toBeInTheDocument();
  });

  it('should be render and trigger update', () => {
    const {
      triggerUpdateRequest,
      triggerUpdate,
      getStrategyPreviewList,
      triggerResetTruncateList
    } = factory({
      stepId: 'step1',
      selectedStep: 'step1',
      isEdit: false,
      savedAt: 1,
      formValues: { strategyList: strategyListFixture, isValid: true },
      setFormValues: jest.fn(),
      clearFormValues: jest.fn,
      handleSave: jest.fn()
    });
    triggerUpdateRequest();
    triggerUpdate();
    triggerResetTruncateList();
    expect(getStrategyPreviewList()).toBeInTheDocument();
  });

  it('should be render with stuck', () => {
    const { getStuckErrorMsg } = factory({
      stepId: 'step2',
      selectedStep: 'step1',
      isEdit: false,
      savedAt: 1,
      formValues: { strategyList: [], isValid: true },
      setFormValues: jest.fn(),
      clearFormValues: jest.fn,
      handleSave: jest.fn(),
      isStuck: true
    });
    expect(getStuckErrorMsg()).toBeInTheDocument();
  });
});
