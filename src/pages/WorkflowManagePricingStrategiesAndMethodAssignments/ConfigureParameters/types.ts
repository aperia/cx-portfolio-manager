export type OptionType = 'CREATE' | 'CHOOSE' | 'EDIT_CREATE' | 'EDIT_CHOOSE';

export type StrategyDetailsFormValues = {
  strategyName: string;
  cisDescription?: string;
  comment?: string;
  selectedStrategy: { name: string; id: string };
};

type ValidationError = { status: boolean; message: string };

export type StrategyDetailsFormErrors = {
  strategyName?: ValidationError;
};

export type StrategyDetailsFormState = {
  values: StrategyDetailsFormValues;
  isStrategyNameDuplicated: boolean;
  isCreateNewVersionDuplicated: boolean;
};

export type StrategyDetailsFormAction =
  | {
      type: 'SET_VALUES';
      payload: { fieldName: keyof StrategyDetailsFormValues; value: string };
    }
  | {
      type: 'RESET';
      payload: StrategyDetailsFormState;
    }
  | { type: 'SET_DUPLICATED'; payload: boolean }
  | { type: 'SET_VERSION_DUPLICATED'; payload: boolean };

export enum Subject {
  IC = 'IC',
  IO = 'IO',
  OC = 'OC',
  PB = 'PB',
  PF = 'PF',
  PO = 'PO'
}

export interface MethodItem {
  rowId: string;
  service: string;
  subject: string;
  subData: MethodSubItem[];
}

export interface MethodSubItem {
  id: string;
  serviceSubjectSection: string;
  currentAssignment: string;
  newAssignment: string;
  moreInfoText: string;
}

export interface MethodItem {
  rowId: string;
  service: string;
  subject: string;
  subData: {
    newAssignment: string;
    currentAssignment: string;
    id: string;
    serviceSubjectSection: string;
    moreInfoText: string;
  }[];
}

export interface StrategyListFormValues {
  unique: string;
  name: string;
  modeledFrom: { name: string; id: string };
  fromVersion: string;
  cisDescription?: string;
  commentArea?: string;
  methods: {
    serviceSubjectSection: string;
    previousValue: string;
    newValue: string;
  }[];
}

export interface StrategyPreviewListItem {
  uniqueId: string;
  strategyName: string;
  modeled: string;
  modeledId: string;
  cisDescription?: string;
  commentArea?: string;
  actionTaken: string;
  fromVersion: string;
  serviceSubject: {
    rowId: string;
    service: string;
    subject: string;
    subData: {
      newAssignment: string;
      currentAssignment: string;
      id: string;
      serviceSubjectSection: string;
      moreInfoText: string;
    }[];
  }[];
}

export type StrategyPreviewListGridRef = {
  navigateToItem: (uniqueId: string) => void;
  resetToPage1: () => void;
};
