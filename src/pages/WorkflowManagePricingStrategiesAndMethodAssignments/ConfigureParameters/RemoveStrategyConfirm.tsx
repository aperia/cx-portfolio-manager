import ModalRegistry from 'app/components/ModalRegistry';
import {
  Button,
  ModalBody,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import React, { useCallback, useRef, useState } from 'react';

interface RemoveStrategyConfirmProps {
  show: boolean;
  onClose: () => void;
  onSubmit: () => void;
}

const RemoveStrategyConfirm: React.FC<RemoveStrategyConfirmProps> = ({
  show,
  onClose,
  onSubmit
}) => {
  const { t } = useTranslation();
  const handleDelete = () => {
    onSubmit();
    onClose;
  };

  return (
    <ModalRegistry
      id="remove-strategy-confirm-modal"
      show={show}
      onAutoClosedAll={onClose}
      classes={{ content: '' }}
    >
      <ModalHeader border closeButton onHide={onClose}>
        <ModalTitle>{t('txt_confirm_deletion')}</ModalTitle>
      </ModalHeader>
      <ModalBody>
        <span>{t('txt_confirm_delete_strategy_body')}</span>
      </ModalBody>
      <div className="dls-modal-footer modal-footer">
        <Button variant="secondary" onClick={onClose}>
          {t('txt_cancel')}
        </Button>
        <Button variant="danger" onClick={handleDelete}>
          {t('txt_delete')}
        </Button>
      </div>
    </ModalRegistry>
  );
};

const useRemoveStrategy = () => {
  const submitRef = useRef<() => void>();
  const [show, setShow] = useState(false);

  const onClose = () => {
    setShow(false);
  };

  const onSubmit = useCallback(() => {
    submitRef.current && submitRef.current();
    onClose();
  }, []);

  const openRemoveStrategyConfirm = useCallback((submitCb: () => void) => {
    setShow(true);
    submitRef.current = submitCb;
  }, []);

  return { show, onSubmit, onClose, openRemoveStrategyConfirm };
};

export type { RemoveStrategyConfirmProps };
export { RemoveStrategyConfirm, useRemoveStrategy };
