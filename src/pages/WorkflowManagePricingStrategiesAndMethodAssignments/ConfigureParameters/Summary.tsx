import { Button, useTranslation } from 'app/_libraries/_dls';
import { isFunction } from 'app/_libraries/_dls/lodash';
import React, { useEffect, useRef, useState } from 'react';
import { ConfigureParametersFormValues } from '.';
import { StrategyPreviewListGrid } from './StrategyPreviewListGrid';
import { StrategyPreviewListGridRef } from './types';

const Summary: React.FC<
  WorkflowSetupSummaryProps<ConfigureParametersFormValues>
> = ({ onEditStep, formValues, stepId, selectedStep }) => {
  const { t } = useTranslation();

  const strategyListRef = useRef<StrategyPreviewListGridRef>(null);

  const handleEdit = () => {
    isFunction(onEditStep) && onEditStep();
  };

  const [expandedList, setExpandedList] = useState<string[]>([]);

  useEffect(() => {
    // reset to page 1 when next step
    if (stepId !== selectedStep) {
      strategyListRef.current?.resetToPage1();
    }
  }, [stepId, selectedStep]);

  return (
    <div className="position-relative">
      <div className="absolute-top-right mt-n26 mr-n8">
        <Button variant="outline-primary" size="sm" onClick={handleEdit}>
          {t('txt_edit')}
        </Button>
      </div>
      <div className="pt-16">
        <StrategyPreviewListGrid
          strategyList={formValues.strategyList!}
          isPreview
          expanded={expandedList}
          onExpand={setExpandedList}
          ref={strategyListRef}
        />
      </div>
    </div>
  );
};

export default Summary;
