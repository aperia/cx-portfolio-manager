import TextAreaCountDown from 'app/components/TextAreaCountDown';
import { useFormValidations } from 'app/hooks';
import { InlineMessage, TextBox, useTranslation } from 'app/_libraries/_dls';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import React, {
  memo,
  useCallback,
  useEffect,
  useMemo,
  useReducer,
  useState
} from 'react';
import {
  OptionType,
  StrategyDetailsFormAction,
  StrategyDetailsFormErrors,
  StrategyDetailsFormState,
  StrategyDetailsFormValues
} from './types';

interface StrategyDetailsFormProps {
  isStrategyNameDuplicated: boolean;
  isCreateNewVersionDuplicated: boolean;
  selectedStrategy: string;
  openedFrom?: OptionType;
  values: StrategyDetailsFormValues;
  errors: StrategyDetailsFormErrors;
  handleChangeFieldValue: (
    fieldName: keyof StrategyDetailsFormValues
  ) => (e: any) => void;
  handleBlurField: (fieldName: keyof StrategyDetailsFormValues) => () => void;
}

const StrategyDetailsForm: React.FC<StrategyDetailsFormProps> = memo(props => {
  const { t } = useTranslation();

  const {
    isStrategyNameDuplicated,
    isCreateNewVersionDuplicated,
    openedFrom,
    values,
    errors,
    handleChangeFieldValue,
    handleBlurField,
    selectedStrategy
  } = props;
  const { strategyName, cisDescription, comment } = values;

  return (
    <>
      <div>
        {isStrategyNameDuplicated && (
          <InlineMessage variant="danger" withIcon className="mb-8 mt-16">
            {t('txt_strategy_name_duplicated_error_msg')}
          </InlineMessage>
        )}
        {isCreateNewVersionDuplicated && (
          <InlineMessage variant="danger" withIcon className="mb-8 mt-16">
            {t('txt_create_new_version_duplicated_error_msg')}
          </InlineMessage>
        )}
      </div>
      <div className="row mt-16">
        {(openedFrom === 'CREATE' || openedFrom === 'EDIT_CREATE') && (
          <div className="col-6">
            <TextBox
              id="strategyDetails__selectedStrategy"
              readOnly
              value={selectedStrategy}
              label={t('txt_selected_strategy')}
            />
          </div>
        )}
        {(openedFrom === 'CHOOSE' || openedFrom === 'EDIT_CHOOSE') && (
          <div className="col-6">
            <TextBox
              data-testid="strategyDetails__strategyName"
              id="strategyDetails__strategyName"
              value={strategyName}
              onChange={handleChangeFieldValue('strategyName')}
              required
              onBlur={handleBlurField('strategyName')}
              maxLength={4}
              label={t('txt_strategy_name')}
              error={errors.strategyName}
            />
          </div>
        )}
      </div>
      <div className="row mt-16">
        <div className="col-6">
          <TextAreaCountDown
            id="strategyDetails__cisDescription"
            dataTestId="strategyDetails__cisDescription"
            value={cisDescription}
            onChange={handleChangeFieldValue('cisDescription')}
            rows={4}
            maxLength={180}
            label={t('txt_cis_description')}
          />
        </div>
        <div className="col-6">
          <TextAreaCountDown
            id="strategyDetails__commentArea"
            label={t('txt_comment_area')}
            value={comment}
            onChange={handleChangeFieldValue('comment')}
            rows={4}
            maxLength={240}
          />
        </div>
      </div>
    </>
  );
});

const formReducer = (
  state: StrategyDetailsFormState,
  action: StrategyDetailsFormAction
) => {
  switch (action.type) {
    case 'SET_VALUES':
      const { fieldName, value } = action.payload;
      return {
        ...state,
        values: {
          ...state.values,
          [fieldName]:
            fieldName === 'strategyName' ? value.toUpperCase() : value
        }
      };

    case 'SET_DUPLICATED':
      return { ...state, isStrategyNameDuplicated: action.payload };

    case 'SET_VERSION_DUPLICATED':
      return { ...state, isCreateNewVersionDuplicated: action.payload };

    default:
      // 'RESET'
      return action.payload;
  }
};

const initialFormState = (values: StrategyDetailsFormValues) => ({
  values,
  isStrategyNameDuplicated: false,
  isCreateNewVersionDuplicated: false
});
const useStrategyDetailsForm = ({
  openedFrom,
  initValues
}: {
  openedFrom?: OptionType;
  initValues: StrategyDetailsFormValues;
}) => {
  const { t } = useTranslation();
  const [formState, formDispatch] = useReducer(
    formReducer,
    initialFormState(initValues)
  );
  const [resetError, setResetError] = useState(0);
  const { values, isStrategyNameDuplicated, isCreateNewVersionDuplicated } =
    formState;
  const validation = useMemo(
    () =>
      ({
        strategyName: !values.strategyName
          ? {
              status: true,
              message: t('txt_required_validation', {
                fieldName: 'Strategy Name'
              }) as string
            }
          : undefined
      } as Record<string, IFormError>),
    [values.strategyName, t]
  );

  const [touches, errors, setTouched] = useFormValidations(
    validation,
    resetError
  );

  const handleBlurField = useCallback(
    (fieldName: keyof StrategyDetailsFormValues) => () => {
      setTouched(fieldName, true)();
    },
    [setTouched]
  );

  const handleChangeFieldValue = useCallback(
    (fieldName: keyof StrategyDetailsFormValues) => (e: any) => {
      const value = e.target?.value;
      formDispatch({
        type: 'SET_VALUES',
        payload: { fieldName, value }
      });
    },
    []
  );

  useEffect(() => {
    formDispatch({
      type: 'RESET',
      payload: initialFormState(initValues)
    });
    setResetError(prev => prev + 1);
  }, [initValues]);

  const formError = useMemo(
    () =>
      (openedFrom === 'CHOOSE' &&
        (!!errors.strategyName || isEmpty(touches.strategyName))) ||
      (openedFrom === 'EDIT_CHOOSE' && !!errors.strategyName),
    [errors.strategyName, openedFrom, touches.strategyName]
  );

  const handleSetDuplicated = useCallback((isDuplicated: boolean) => {
    formDispatch({ type: 'SET_DUPLICATED', payload: isDuplicated });
  }, []);

  const handleSetVersionDuplicated = useCallback((isDuplicated: boolean) => {
    formDispatch({ type: 'SET_VERSION_DUPLICATED', payload: isDuplicated });
  }, []);

  return {
    values,
    errors,
    handleBlurField,
    handleChangeFieldValue,
    isStrategyNameDuplicated,
    isCreateNewVersionDuplicated,
    openedFrom,
    formError,
    selectedStrategy: formState.values.selectedStrategy.name,
    handleSetDuplicated,
    handleSetVersionDuplicated
  };
};

export type { StrategyDetailsFormProps };
export { StrategyDetailsForm, useStrategyDetailsForm };
