import ModalRegistry from 'app/components/ModalRegistry';
import {
  unsavedChangesProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { classnames } from 'app/helpers';
import {
  useAsyncThunkDispatch,
  useUnsavedChangeRegistry,
  useUnsavedChangesRedirect
} from 'app/hooks';
import {
  Button,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  TransDLS,
  useTranslation
} from 'app/_libraries/_dls';
import { isEqual } from 'app/_libraries/_dls/lodash';
import {
  actionsWorkflowSetup,
  useSelectStrategyData
} from 'pages/_commons/redux/WorkflowSetup';
import React, { memo, useCallback, useEffect, useMemo, useState } from 'react';
import { FormValuesAction } from '.';
import {
  DEFAULT_FORM_VALUES,
  SERVICE_SUBJECT_SECTION_KY_MAPPING
} from './constants';
import { useMethodContext } from './Context';
import { MethodList } from './MethodList';
import {
  StrategyDetailsForm,
  useStrategyDetailsForm
} from './StrategyDetailsForm';
import {
  MethodItem,
  OptionType,
  StrategyDetailsFormValues,
  StrategyListFormValues
} from './types';

export interface StrategyDetailsProps {
  show: boolean;
  onClose: () => void;
  onBack: (dirty: boolean) => void;
  onSubmit: (
    openedFrom: OptionType,
    values: StrategyDetailsFormValues,
    list: MethodItem[],
    prevStrategyName: string
  ) => void;
  openedFrom?: OptionType;
  formValues: StrategyDetailsFormValues;
  methodList: MethodItem[];
  tempStrategyList: StrategyListFormValues[];
}

const StrategyDetails: React.FC<StrategyDetailsProps> = memo(
  ({
    show,
    onClose,
    onBack,
    onSubmit,
    openedFrom,
    formValues,
    tempStrategyList
  }) => {
    const { t } = useTranslation();
    const { isMethodListDirty, methodList } = useMethodContext();
    const strategyList = useSelectStrategyData();
    const strategyNameList = useMemo(
      () => strategyList.map(strategy => strategy.name),
      [strategyList]
    );
    const tempStrategyNameList = tempStrategyList.map(lst => lst.name);

    const {
      formError,
      handleSetDuplicated,
      handleSetVersionDuplicated,
      ...strategyDetailsFormProps
    } = useStrategyDetailsForm({
      openedFrom,
      initValues: formValues
    });
    const { values } = strategyDetailsFormProps;
    const handleSubmit = () => {
      if (openedFrom === 'CHOOSE' || openedFrom === 'EDIT_CHOOSE') {
        const isDuplicated =
          strategyNameList.includes(values.strategyName) ||
          (formValues.strategyName !== values.strategyName && // for edit_choose
            tempStrategyNameList.includes(values.strategyName));
        handleSetDuplicated(isDuplicated);
        if (isDuplicated) return;
      }
      if (openedFrom === 'CREATE') {
        const isDuplicated = tempStrategyNameList.includes(values.strategyName);
        handleSetVersionDuplicated(
          tempStrategyNameList.includes(values.strategyName)
        );
        if (isDuplicated) return;
      }
      onSubmit(openedFrom!, values, methodList, formValues.strategyName);
    };

    useUnsavedChangeRegistry(
      {
        ...unsavedChangesProps,
        formName:
          UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_PRICING_STRATEGIES___CONFIGURE_PARAMETERS_DETAILS,
        priority: 1
      },
      [!isEqual(formValues, values), isMethodListDirty]
    );

    return (
      <ModalRegistry
        id="manage-pricing-strategy-strategy-details"
        show={show}
        lg
        onAutoClosedAll={onClose}
      >
        <ModalHeader border closeButton onHide={onClose}>
          <ModalTitle>{t('txt_strategy_details')}</ModalTitle>
        </ModalHeader>
        <ModalBody className="px-24 pt-24 pb-24 pb-lg-0">
          <TransDLS
            keyTranslation={classnames(
              (openedFrom === 'CREATE' || openedFrom === 'EDIT_CREATE') &&
                'txt_pricing_strategies_and_assignments_strategy_details_help_text_from_create',
              (openedFrom === 'CHOOSE' || openedFrom === 'EDIT_CHOOSE') &&
                'txt_pricing_strategies_and_assignments_strategy_details_help_text_from_choose'
            )}
          >
            <strong className="color-grey-d20">
              {{ strategyName: values.selectedStrategy.name }}
            </strong>
            <strong className="color-grey-d20"></strong>
          </TransDLS>
          <StrategyDetailsForm {...strategyDetailsFormProps} />
          <MethodList list={methodList} />
        </ModalBody>

        <ModalFooter
          cancelButtonText={t('txt_cancel')}
          okButtonText={t('txt_save')}
          onCancel={onClose}
          onOk={handleSubmit}
          disabledOk={formError}
        >
          <Button
            className="mr-auto ml-n8"
            variant="outline-primary"
            onClick={() => {
              onBack(!isEqual(formValues, values) || isMethodListDirty);
            }}
          >
            {t('txt_back')}
          </Button>
        </ModalFooter>
      </ModalRegistry>
    );
  }
);

const useStrategyDetails = ({
  onCloseCb,
  onBackCb,
  dispatch,
  versionSelected
}: {
  onCloseCb: () => void;
  onBackCb: (version?: string) => void;
  dispatch: React.Dispatch<FormValuesAction>;
  versionSelected?: string;
}) => {
  const thunkDispatch = useAsyncThunkDispatch();
  const redirect = useUnsavedChangesRedirect();

  const [openedFrom, setOpenedFrom] = useState<OptionType>();
  const [show, setShow] = useState(!!openedFrom);
  const [initialFormValues, setInitialFormValues] =
    useState<StrategyDetailsFormValues>(DEFAULT_FORM_VALUES);
  const [methodList, setMethodList] = useState<MethodItem[]>([]);
  const [currentUniqueId, setCurrentUniqueId] = useState<string>();
  const [prevVersion, setPrevVersion] =
    useState<string | undefined>(versionSelected);

  const openStrategyDetails = useCallback(
    ({
      from,
      informationValues: values,
      methodList: list,
      version,
      uniqueId
    }: {
      from: OptionType;
      informationValues: StrategyDetailsFormValues;
      methodList: MethodItem[];
      uniqueId: string;
      version?: string;
    }) => {
      setOpenedFrom(from);
      setCurrentUniqueId(uniqueId);
      // for handle back button
      if (!version || prevVersion !== version || openedFrom !== from) {
        setInitialFormValues(values);
        setMethodList(list);
      }
      setPrevVersion(version);
      setShow(true);
    },
    [openedFrom, prevVersion]
  );

  const onBack = useCallback(
    (dirty: boolean) => {
      setShow(false);
      onBackCb(dirty ? prevVersion : undefined);
    },
    [onBackCb, prevVersion]
  );

  const invokeOnClose = useCallback(() => {
    setOpenedFrom(undefined);
    setInitialFormValues(DEFAULT_FORM_VALUES);
    setMethodList([]);
    setShow(false);
    onCloseCb();
  }, [onCloseCb]);

  const onClose = () => {
    redirect({
      onConfirm: invokeOnClose,
      formsWatcher: [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_PRICING_STRATEGIES___CONFIGURE_PARAMETERS_DETAILS
      ]
    });
  };

  const onSubmit = (
    openedFrom: OptionType,
    values: StrategyDetailsFormValues,
    list: MethodItem[]
  ) => {
    const newStrategy: StrategyListFormValues = {
      unique: currentUniqueId!,
      name: values.strategyName,
      modeledFrom: values.selectedStrategy,
      cisDescription: values.cisDescription,
      commentArea: values.comment,
      fromVersion: prevVersion!,
      methods: list.reduce(
        (acc, method) => {
          return acc.concat(
            method.subData.map(item => ({
              serviceSubjectSection:
                SERVICE_SUBJECT_SECTION_KY_MAPPING[
                  item.id as keyof typeof SERVICE_SUBJECT_SECTION_KY_MAPPING
                ],
              previousValue: item.currentAssignment,
              newValue: item.newAssignment
            }))
          );
        },
        [] as {
          serviceSubjectSection: string;
          previousValue: string;
          newValue: string;
        }[]
      )
    };
    if (openedFrom === 'CHOOSE' || openedFrom === 'CREATE') {
      dispatch({ type: 'ADD_STRATEGY_TO_LIST', payload: newStrategy });
    } else {
      dispatch({
        type: 'UPDATE_STRATEGY',
        payload: newStrategy
      });
    }
    invokeOnClose();
  };

  useEffect(() => {
    thunkDispatch(
      actionsWorkflowSetup.getPricingStrategyMethodList(
        Object.values(SERVICE_SUBJECT_SECTION_KY_MAPPING)
      )
    );
  }, [thunkDispatch]);

  useEffect(() => {
    setPrevVersion(versionSelected);
  }, [versionSelected]);

  return {
    show,
    openedFrom,
    onBack,
    onClose,
    openStrategyDetails,
    onSubmit,
    formValues: initialFormValues,
    methodList
  };
};

export { StrategyDetails, useStrategyDetails };
