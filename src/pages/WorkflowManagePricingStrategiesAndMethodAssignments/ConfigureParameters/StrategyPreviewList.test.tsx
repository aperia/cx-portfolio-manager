import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React from 'react';
import { act } from 'react-dom/test-utils';
import { StrategyPreviewList } from './StrategyPreviewList';

jest.mock('./StrategyPreviewListGrid', () => ({
  StrategyPreviewListGrid: () => (
    <div data-testid="StrategyPreviewListGrid"></div>
  )
}));

const mockOnOptionSelect = jest.fn();

const factory = () => {
  const { getByTestId, getByText } = render(
    <StrategyPreviewList
      onOptionSelect={mockOnOptionSelect}
      strategyList={[]}
      onRemove={jest.fn()}
      onUpdate={jest.fn()}
      expanded={[]}
      onExpand={jest.fn()}
    />
  );

  const getStrategyPreviewListGrid = () =>
    getByTestId('StrategyPreviewListGrid');

  const triggerOptionSelect = () => {
    jest.useFakeTimers();
    act(() => {
      userEvent.click(getByText('txt_add_strategy'));
    });
    act(() => {
      userEvent.click(getByText('txt_create_new_version'));
    });
    jest.runAllTimers();
  };

  return { getStrategyPreviewListGrid, triggerOptionSelect };
};

describe('test StrategyPreviewList component', () => {
  it('should be render', () => {
    const { getStrategyPreviewListGrid } = factory();
    expect(getStrategyPreviewListGrid()).toBeInTheDocument();
  });

  it('should be select option', () => {
    const { triggerOptionSelect } = factory();
    triggerOptionSelect();
    expect(mockOnOptionSelect).toBeCalled();
  });
});
