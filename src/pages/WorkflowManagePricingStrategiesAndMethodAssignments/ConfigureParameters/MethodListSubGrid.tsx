import BadgeGroupTextControl from 'app/components/DofControl/BadgeGroupTextControl';
import { BadgeColorType } from 'app/constants/enums';
import {
  ColumnType,
  ComboBox,
  Grid,
  useTranslation
} from 'app/_libraries/_dls';
import { useSelectMethods } from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManagePricingStrategiesAndMethodAssignments';
import { formatText, viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React, { useCallback } from 'react';
import { MORE_INFO, SERVICE_SUBJECT_SECTION_KY_MAPPING } from './constants';
import { useMethodContext } from './Context';
import { MethodSubItem } from './types';

interface MethodListSubGridProps {
  data: MethodSubItem[];
  isPreview?: boolean;
}

const EMPTY_ARRAY: string[] = [];

const MethodListSubGrid: React.FC<MethodListSubGridProps> = ({
  data,
  isPreview
}) => {
  const { t } = useTranslation();
  const { onChangeAssignment } = useMethodContext();
  const methodList = useSelectMethods();

  const newAssignmentAccessor = useCallback(
    (data: MethodSubItem) => {
      const paramId = data.id;
      const currentMethodList = methodList[paramId] ?? EMPTY_ARRAY;
      return (
        <ComboBox
          textField="text"
          size="small"
          placeholder={t('txt_select_an_option')}
          noResult={t('txt_no_results_found_without_dot')}
          value={data.newAssignment}
          onChange={e => {
            onChangeAssignment(data.id, e.target.value);
          }}
        >
          {currentMethodList.map(item => (
            <ComboBox.Item key={item} value={item} label={item} />
          ))}
        </ComboBox>
      );
    },
    [methodList, onChangeAssignment, t]
  );

  const columns = [
    {
      id: 'serviceSubjectSection',
      Header: t('txt_service_subject_section'),
      accessor: formatText(['serviceSubjectSection']),
      width: isPreview ? 270 : 313
    },
    {
      id: 'currentAssignment',
      Header: t('txt_current_assignment'),
      accessor: formatText(['currentAssignment']),
      width: isPreview ? 166 : 225
    },
    {
      id: 'newAssignment',
      Header: t('txt_new_assignment'),
      accessor: isPreview
        ? formatText(['newAssignment'])
        : newAssignmentAccessor,
      cellBodyProps: { className: 'py-8' },
      width: isPreview ? 166 : 288
    },
    isPreview && {
      id: 'status',
      Header: t('txt_status'),
      accessor: ({ currentAssignment, newAssignment }: MethodSubItem) => (
        <div className="mt-n2">
          <BadgeGroupTextControl
            id={`id_status_${currentAssignment}-${newAssignment}`}
            input={
              {
                value:
                  currentAssignment === newAssignment
                    ? 'Not Updated'
                    : 'Updated'
              } as never
            }
            meta={{} as never}
            options={{
              colorType: BadgeColorType.ConfigParameterStatus
            }}
          />
        </div>
      ),
      cellBodyProps: { className: 'py-8' },
      width: 129
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      accessor: ({ id }: MethodSubItem) => {
        const data =
          MORE_INFO[id as keyof typeof SERVICE_SUBJECT_SECTION_KY_MAPPING];
        return viewMoreInfo(['node'], t)(data);
      },
      width: 106
    }
  ].filter(Boolean) as ColumnType[];
  return <Grid data={data} columns={columns} />;
};

export type { MethodListSubGridProps };
export { MethodListSubGrid };
