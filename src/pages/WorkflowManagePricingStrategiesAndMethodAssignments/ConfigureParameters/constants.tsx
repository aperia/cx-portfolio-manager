import React from 'react';
import { MethodSubItem, Subject } from './types';

export const DEFAULT_FORM_VALUES = {
  selectedStrategy: { name: '', id: '' },
  strategyName: '',
  cisDescription: '',
  comment: ''
};

export const SUBJECT: Record<Subject, string> = {
  [Subject.IC]: 'IC, Interest Charges ',
  [Subject.IO]: 'IO, Income Options',
  [Subject.OC]: 'OC, Other Controls',
  [Subject.PB]: 'PB, Protected Balance',
  [Subject.PF]: 'PF, Penalty Fees',
  [Subject.PO]: 'PO, Payment Options'
};

export const SERVICE_SUBJECT_SECTION_KY_MAPPING = {
  CP_IC_BP: 'CP IC BP',
  CP_IC_IB: 'CP IC IB',
  CP_IC_ID: 'CP IC ID',
  CP_IC_IF: 'CP IC IF',
  CP_IC_II: 'CP IC II',
  CP_IC_IM: 'CP IC IM',
  CP_IC_IP: 'CP IC IP',
  CP_IC_IR: 'CP IC IR',
  CP_IC_IV: 'CP IC IV',
  CP_IC_ME: 'CP IC ME',
  CP_IC_MF: 'CP IC MF',
  CP_IC_ML: 'CP IC ML',
  CP_IC_MS: 'CP IC MS',
  CP_IC_PE: 'CP IC PE',
  CP_IC_SC: 'CP IC SC',
  CP_IC_SP: 'CP IC SP',
  CP_IC_SZ: 'CP IC SZ',
  CP_IC_TI: 'CP IC TI',
  CP_IC_VI: 'CP IC VI',
  CP_IO_AC: 'CP IO AC',
  CP_IO_CI: 'CP IO CI',
  CP_IO_CL: 'CP IO CL',
  CP_IO_MC: 'CP IO MC',
  CP_IO_MI: 'CP IO MI',
  CP_IO_SC: 'CP IO SC',
  CP_OC_AP: 'CP OC AP',
  CP_OC_BD: 'CP OC BD',
  CP_OC_CL: 'CP OC CL',
  CP_OC_CP: 'CP OC CP',
  CP_OC_DR: 'CP OC DR',
  CP_OC_FP: 'CP OC FP',
  CP_OC_GS: 'CP OC GS',
  CP_OC_MO: 'CP OC MO',
  CP_OC_MP: 'CP OC MP',
  CP_OC_PP: 'CP OC PP',
  CP_OC_RB: 'CP OC RB',
  CP_OC_SA: 'CP OC SA',
  CP_OC_SD: 'CP OC SD',
  CP_OC_TI: 'CP OC TI',
  CP_PB_CT: 'CP PB CT',
  CP_PF_DA: 'CP PF DA',
  CP_PF_LC: 'CP PF LC',
  CP_PF_OC: 'CP PF OC',
  CP_PF_RC: 'CP PF RC',
  CP_PO_CA: 'CP PO CA',
  CP_PO_FM: 'CP PO FM',
  CP_PO_MP: 'CP PO MP',
  CP_PO_RM: 'CP PO RM',
  CP_PO_SP: 'CP PO SP'
};

const SERVICE_SUBJECT_SECTION = {
  CP_IC_BP: 'CP IC BP, Break Points',
  CP_IC_IB: 'CP IC IB, Incentive Pricing Break Points',
  CP_IC_ID: 'CP IC ID, Interest Defaults',
  CP_IC_IF: 'CP IC IF, Interest and Fees',
  CP_IC_II: 'CP IC II, Interest on Interest',
  CP_IC_IM: 'CP IC IM, Interest Methods',
  CP_IC_IP: 'CP IC IP, Incentive Pricing',
  CP_IC_IR: 'CP IC IR, Index Rate',
  CP_IC_IV: 'CP IC IV, Incentive Pricing Variable Interest',
  CP_IC_ME: 'CP IC ME, Max Cap EAPR',
  CP_IC_MF: 'CP IC MF, Minimum Finance Charges',
  CP_IC_ML: 'CP IC ML, Method Level',
  CP_IC_MS: 'CP IC MS, Money Sales',
  CP_IC_PE: 'CP IC PE, Payoff Exceptions',
  CP_IC_SC: 'CP IC SC, State Controls',
  CP_IC_SP: 'CP IC SP, Statement Production',
  CP_IC_SZ: 'CP IC SZ, Securitization Controls',
  CP_IC_TI: 'CP IC TI, Tiered Interest',
  CP_IC_VI: 'CP IC VI, Variable Interest',
  CP_IO_AC: 'CP IO AC, Annual Charges',
  CP_IO_CI: 'CP IO CI, Cash Advance Item Charges',
  CP_IO_CL: 'CP IO CL, Credit Life',
  CP_IO_MC: 'CP IO MC, Miscellaneous Charges',
  CP_IO_MI: 'CP IO MI, Merchandise Item Charges',
  CP_IO_SC: 'CP IO SC, Statement Charges',
  CP_OC_AP: 'CP OC AP, Adjustment Pricing',
  CP_OC_BD: 'CP OC BD, Backdating',
  CP_OC_CL: 'CP OC CL, Credit Line Management',
  CP_OC_CP: 'CP OC CP, Current Pricing Notification',
  CP_OC_DR: 'CP OC DR, Debit Ratification',
  CP_OC_FP: 'CP OC FP, Full Pricing Notification',
  CP_OC_GS: 'CP OC GS, Group Statement Design',
  CP_OC_MO: 'CP OC MO, Method Overrides',
  CP_OC_MP: 'CP OC MP, MULTRAN Processing',
  CP_OC_PP: 'CP OC PP, Promotional Purchases',
  CP_OC_RB: 'CP OC RB, Rebate Option/Bonus Program',
  CP_OC_SA: 'CP OC SA, Special Accounts',
  CP_OC_SD: 'CP OC SD, Statement Design',
  CP_OC_TI: 'CP OC TI, Travel Insurance',
  CP_PB_CT: 'CP PB CT, Change in Terms',
  CP_PF_DA: 'CP PF DA, Declined Authorization Charges',
  CP_PF_LC: 'CP PF LC, Late Charges',
  CP_PF_OC: 'CP PF OC, Overlimit Charges',
  CP_PF_RC: 'CP PF RC, Returned Check Charges',
  CP_PO_CA: 'CP PO CA, Credit Application',
  CP_PO_FM: 'CP PO FM, Fixed Minimum Payment',
  CP_PO_MP: 'CP PO MP, Minimum Payment Due',
  CP_PO_RM: 'CP PO RM, Rules Minimum Payment',
  CP_PO_SP: 'CP PO SP, Skip Payment'
};

export const MORE_INFO: Record<
  keyof typeof SERVICE_SUBJECT_SECTION,
  { raw: string; node: React.ReactNode }
> = {
  CP_IC_BP: {
    raw: 'If you choose to use interest break points, set the parameters described in this section. The break point feature enables you to charge a standard interest rate for cash advances and merchandise, plus two break point rates based on the cardholder account balance. Set controls for cash advance and merchandise balances separately. When the balance reaches your predetermined break points, new interest rates take effect. Please refer to Online Product Control File Parameters manual for more information.',
    node: 'If you choose to use interest break points, set the parameters described in this section. The break point feature enables you to charge a standard interest rate for cash advances and merchandise, plus two break point rates based on the cardholder account balance. Set controls for cash advance and merchandise balances separately. When the balance reaches your predetermined break points, new interest rates take effect. Please refer to Online Product Control File Parameters manual for more information.'
  },
  CP_IC_IB: {
    raw: 'The incentive pricing break point feature gives you the option of a standard interest rate for cash advances and merchandise plus two break point rates based on the cardholder account balance. Set controls for cash advance and merchandise balances separately. When the balance reaches your predetermined incentive pricing break points, new interest rates take effect. Please refer to Online Product Control File Parameters manual for more information.',
    node: 'The incentive pricing break point feature gives you the option of a standard interest rate for cash advances and merchandise plus two break point rates based on the cardholder account balance. Set controls for cash advance and merchandise balances separately. When the balance reaches your predetermined incentive pricing break points, new interest rates take effect. Please refer to Online Product Control File Parameters manual for more information.'
  },
  CP_IC_ID: {
    raw: 'The parameters in this section identify the annual percentage rate used to establish base interest rates and limits on the cardholder account record. Interest calculations are compared to minimum and maximum limits on the individual account record. You can set separate interest rates and limits for merchandise and for cash advances. All rates allow for decimal percentages. For example, 12500 equals 12.500%. These limits do not affect any other charges, fees, or credit interest which may be included in the effective annual percentage rate. This section also contains parameters which are used in conjunction with the Index Rate section (CP IC IR) of the Product Control File. Please refer to Online Product Control File Parameters manual for more information.',
    node: (
      <div>
        <p>
          The parameters in this section identify the annual percentage rate
          used to establish base interest rates and limits on the cardholder
          account record. Interest calculations are compared to minimum and
          maximum limits on the individual account record. You can set separate
          interest rates and limits for merchandise and for cash advances. All
          rates allow for decimal percentages. For example, 12500 equals
          12.500%.
        </p>
        <p>
          These limits do not affect any other charges, fees, or credit interest
          which may be included in the effective annual percentage rate.
        </p>
        <p>
          This section also contains parameters which are used in conjunction
          with the Index Rate section (CP IC IR) of the Product Control File.
          Please refer to Online Product Control File Parameters manual for more
          information.
        </p>
      </div>
    )
  },
  CP_IC_IF: {
    raw: 'The parameters in this section control the accumulation and/or display of charges as part of total interest or total fees on cardholder statements. Please refer to Online Product Control File Parameters manual for more information.',
    node: 'The parameters in this section control the accumulation and/or display of charges as part of total interest or total fees on cardholder statements. Please refer to Online Product Control File Parameters manual for more information.'
  },
  CP_IC_II: {
    raw: 'Warning! When establishing or changing parameter settings in the CP IC II, Interest on Interest section, be aware that you may also need to establish or change corresponding parameter settings in the CP IC SZ, Securitization Controls section. Parameter settings in both of these PCF sections may have significant implications for Reg AB Securitization Compliance. Please consult appropriate legal counsel or compliance personnel regarding any selections or changes you make. The parameters in this section allow you to select interest-bearing options for the following fees. Annual charge Cash item fee Credit insurance premium Late fee Merchandise item fee Miscellaneous charge Overlimit fee. Please refer to Online Product Control File Parameters manual for more information.',
    node: (
      <div>
        <p>
          <strong>Warning!</strong>
        </p>
        <p>
          When establishing or changing parameter settings in the CP IC II,
          Interest on Interest section, be aware that you may also need to
          establish or change corresponding parameter settings in the CP IC SZ,
          Securitization Controls section. Parameter settings in both of these
          PCF sections may have significant implications for Reg AB
          Securitization Compliance. Please consult appropriate legal counsel or
          compliance personnel regarding any selections or changes you make.
        </p>
        <p>
          The parameters in this section allow you to select interest-bearing
          options for the following fees.
        </p>
        <ul className="pl-16">
          <li>Annual charge</li>
          <li>Cash item fee</li>
          <li>Credit insurance premium</li>
          <li>Late fee</li>
          <li>Merchandise item fee</li>
          <li>Miscellaneous charge</li>
          <li>Overlimit fee</li>
        </ul>
        <p>
          Please refer to Online Product Control File Parameters manual for more
          information.
        </p>
      </div>
    )
  },
  CP_IC_IM: {
    raw: 'The parameters in this section control the calculation, percentage rate, and assessment of interest on cardholder account balances. Please refer to Online Product Control File Parameters manual for more information.',
    node: 'The parameters in this section control the calculation, percentage rate, and assessment of interest on cardholder account balances. Please refer to Online Product Control File Parameters manual for more information.'
  },
  CP_IC_IP: {
    raw: 'You can use the incentive pricing feature to establish introductory and promotional interest rates for a fixed period of time for both new and existing accounts. The parameters in this section allow you to control how you want to use the incentive pricing feature. This section also contains parameters which are used in conjunction with the Index Rate section (CP IC IR) of the Product Control File. Please refer to Online Product Control File Parameters manual for more information.',
    node: 'You can use the incentive pricing feature to establish introductory and promotional interest rates for a fixed period of time for both new and existing accounts. The parameters in this section allow you to control how you want to use the incentive pricing feature. This section also contains parameters which are used in conjunction with the Index Rate section (CP IC IR) of the Product Control File. Please refer to Online Product Control File Parameters manual for more information.'
  },
  CP_IC_IR: {
    raw: 'The parameters in this section establish index rates that you can use in conjunction with other sections of the Product Control File to determine interest rates. The annual percentage rates (APRs) become effective once the Product Control File methods are in production. Please refer to Online Product Control File Parameters manual for more information.',
    node: 'The parameters in this section establish index rates that you can use in conjunction with other sections of the Product Control File to determine interest rates. The annual percentage rates (APRs) become effective once the Product Control File methods are in production. Please refer to Online Product Control File Parameters manual for more information.'
  },
  CP_IC_IV: {
    raw: 'The parameters in this section allow you to establish three different sets of incentive pricing interest rates, each set controlled by an effective date. This option allows for a smooth transition from one interest rate to another. Warning! Regulation Z requires the disclosure of each periodic rate that is used to compute the finance charge and the range of balances to which it is applicable. If you use more than one interest rate during the cardholder’s cycle, you may need a special statement format to properly disclose each rate and the balance to which it is applicable. Please refer to Online Product Control File Parameters manual for more information.',
    node: (
      <div>
        <p>
          The parameters in this section allow you to establish three different
          sets of incentive pricing interest rates, each set controlled by an
          effective date. This option allows for a smooth transition from one
          interest rate to another.
        </p>
        <p>
          <strong>Warning!</strong>
        </p>
        <p>
          Regulation Z requires the disclosure of each periodic rate that is
          used to compute the finance charge and the range of balances to which
          it is applicable. If you use more than one interest rate during the
          cardholder’s cycle, you may need a special statement format to
          properly disclose each rate and the balance to which it is applicable.
          Please refer to Online Product Control File Parameters manual for more
          information.
        </p>
      </div>
    )
  },
  CP_IC_ME: {
    raw: 'The parameters in this section control maximum effective annual percentage rate (EAPR) processing.  You use the parameters in this section to establish the maximum EAPR for customer accounts, determine which types of fees and interest the System includes when calculating the EAPR, and control how the System refunds fees and interest if the maximum EAPR is exceeded. You use the parameters on the third screen in this section to control how EAPR information is displayed on customer statements. Please refer to Online Product Control File Parameters manual for more information. Warning! Selecting or changing the online Product Control File settings contained within this service/subject/section can have significant regulatory compliance implications. Altering these settings may require changes in the programming or design of your periodic statement or in the disclosures you have provided to your cardholders. Please consult your legal counsel or compliance personnel regarding selection or change of Product Control File settings.',
    node: (
      <div>
        <p>
          The parameters in this section control maximum effective annual
          percentage rate (EAPR) processing.
        </p>
        <p>
          You use the parameters in this section to establish the maximum EAPR
          for customer accounts, determine which types of fees and interest the
          System includes when calculating the EAPR, and control how the System
          refunds fees and interest if the maximum EAPR is exceeded.
        </p>
        <p>
          You use the parameters on the third screen in this section to control
          how EAPR information is displayed on customer statements. Please refer
          to Online Product Control File Parameters manual for more information.
        </p>
        <p>
          <strong>Warning!</strong>
        </p>
        <p>
          Selecting or changing the online Product Control File settings
          contained within this service/subject/section can have significant
          regulatory compliance implications. Altering these settings may
          require changes in the programming or design of your periodic
          statement or in the disclosures you have provided to your cardholders.
          Please consult your legal counsel or compliance personnel regarding
          selection or change of Product Control File settings.
        </p>
      </div>
    )
  },
  CP_IC_MF: {
    raw: 'The parameters in this section control minimum finance charges that you want to assess accounts. Please refer to Online Product Control File Parameters manual for more information. Warning! Selecting or changing the online Product Control File settings contained within this service/subject/section can have significant regulatory compliance implications. Altering these settings may require changes in the programming or design of your periodic statement or in the disclosures you have provided to your cardholders. Please consult your legal counsel or compliance personnel regarding selection or change of Product Control File settings.',
    node: (
      <div>
        <p>
          The parameters in this section control minimum finance charges that
          you want to assess accounts. Please refer to Online Product Control
          File Parameters manual for more information.
        </p>
        <p>
          <strong>Warning!</strong>
        </p>
        <p>
          Selecting or changing the online Product Control File settings
          contained within this service/subject/section can have significant
          regulatory compliance implications. Altering these settings may
          require changes in the programming or design of your periodic
          statement or in the disclosures you have provided to your cardholders.
          Please consult your legal counsel or compliance personnel regarding
          selection or change of Product Control File settings.
        </p>
      </div>
    )
  },
  CP_IC_ML: {
    raw: 'The parameters in this section enable you to use method set processing to control the assignment of interest rates and pricing terms to promotional, plan, and protected balances. Please refer to Online Product Control File Parameters manual for more information.',
    node: 'The parameters in this section enable you to use method set processing to control the assignment of interest rates and pricing terms to promotional, plan, and protected balances. Please refer to Online Product Control File Parameters manual for more information.'
  },
  CP_IC_MS: {
    raw: 'The money sales parameters specify the annual percentage rate, start date, and end date used with a money sale. To use these parameters, you must also set the Promotion Cash Advance parameter in the Merchant Numbers section (AO AC MN) of the Product Control File. Please refer to Online Product Control File Parameters manual for more information.',
    node: 'The money sales parameters specify the annual percentage rate, start date, and end date used with a money sale. To use these parameters, you must also set the Promotion Cash Advance parameter in the Merchant Numbers section (AO AC MN) of the Product Control File. Please refer to Online Product Control File Parameters manual for more information.'
  },
  CP_IC_PE: {
    raw: 'Payoff exceptions allow cardholders to pay off the balance on their statements without incurring additional finance charges. These exceptions include only the noninstallment portion of the account balance when you use the 90/180 day processing option by setting the 90-Day Processing parameter in the Promotional Purchases section (CP OC PP) of the Product Control File. You can set separate payoff exceptions for promotional balances that post through the Transaction Level ProcessingSM service. To do this, create a promotion only method and attach it to the Payoff Exceptions section (CP IC PE) and the Promotion Controls section (PL RT PC) of the Product Control File. Warning! Selecting or changing the online Product Control File settings contained within this service/subject/section can have significant regulatory compliance implications. Altering these settings may require changes in the programming or design of your periodic statement or in the disclosures you have provided to your cardholders. Please consult your legal counsel or compliance personnel regarding selection or change of Product Control File settings. Please refer to Online Product Control File Parameters manual for more information.',
    node: (
      <div>
        <p>
          Payoff exceptions allow cardholders to pay off the balance on their
          statements without incurring additional finance charges. These
          exceptions include only the noninstallment portion of the account
          balance when you use the 90/180 day processing option by setting the
          90-Day Processing parameter in the Promotional Purchases section (CP
          OC PP) of the Product Control File.
        </p>
        <p>
          You can set separate payoff exceptions for promotional balances that
          post through the Transaction Level ProcessingSM service. To do this,
          create a promotion only method and attach it to the Payoff Exceptions
          section (CP IC PE) and the Promotion Controls section (PL RT PC) of
          the Product Control File.
        </p>
        <p>
          <strong>Warning!</strong>
        </p>
        <p>
          Selecting or changing the online Product Control File settings
          contained within this service/subject/section can have significant
          regulatory compliance implications. Altering these settings may
          require changes in the programming or design of your periodic
          statement or in the disclosures you have provided to your cardholders.
          Please consult your legal counsel or compliance personnel regarding
          selection or change of Product Control File settings. Please refer to
          Online Product Control File Parameters manual for more information.
        </p>
      </div>
    )
  },
  CP_IC_SC: {
    raw: 'The parameters in this section control processing based on cardholder state of residence. Please refer to Online Product Control File Parameters manual for more information.',
    node: 'The parameters in this section control processing based on cardholder state of residence. Please refer to Online Product Control File Parameters manual for more information.'
  },
  CP_IC_SP: {
    raw: 'The parameters in this section control statement production. Statement production involves both the appearance of the statement and the production. Warning! Selecting or changing the online Product Control File settings contained within this service/subject/section can have significant regulatory compliance implications. Altering these settings may require changes in the programming or design of your periodic statement or in the disclosures you have provided to your cardholders. Please consult your legal counsel or compliance personnel regarding selection or change of Product Control File settings. Be aware that Fiserv will not override existing statement suppression controls to produce change-in-terms notifications that are initiated from a CIT method. Refer to the CIT Processing and Statement Suppression heading in the Change-in-Terms Methods chapter in the Cardholder Terms and Disclosures manual for more information. Please refer to Online Product Control File Parameters manual for more information.',
    node: (
      <div>
        <p>
          The parameters in this section control statement production. Statement
          production involves both the appearance of the statement and the
          production.
        </p>
        <p>
          <strong>Warning!</strong>
        </p>
        <p>
          Selecting or changing the online Product Control File settings
          contained within this service/subject/section can have significant
          regulatory compliance implications. Altering these settings may
          require changes in the programming or design of your periodic
          statement or in the disclosures you have provided to your cardholders.
          Please consult your legal counsel or compliance personnel regarding
          selection or change of Product Control File settings.
        </p>
        <p>
          Be aware that Fiserv will not override existing statement suppression
          controls to produce change-in-terms notifications that are initiated
          from a CIT method. Refer to the CIT Processing and Statement
          Suppression heading in the Change-in-Terms Methods chapter in the
          Cardholder Terms and Disclosures manual for more information. Please
          refer to Online Product Control File Parameters manual for more
          information.
        </p>
      </div>
    )
  },
  CP_IC_SZ: {
    raw: 'Warning! When establishing or changing parameter settings in the CP IC SZ, Securitization Controls section, be aware that you may also need to establish or change corresponding parameter settings in the CP IC II, Interest on Interest section. Parameter settings in both of these PCF sections may have significant implications for Reg AB Securitization Compliance. Please consult appropriate legal counsel or compliance personnel regarding any selections or changes you make. If you are involved in receivable-based financing, use the parameters in this section to determine whether the following fees will be reported as a finance charge or as a principal amount within securitization reporting. Annual charge Cash item fee Credit insurance premium Late charge Merchandise item fee Miscellaneous charge Overlimit charge Non-interest bearing debits The following reports display these fees in the interest or principal columns, based on how you set the parameters in this section. CD-124/CD-128/CD-1500, Portfolio Activity Summary reports CD-6753, Portfolio Securitization Detail Report CD-6754, Customer Level Portfolio Securitization Report CM-884, Stratification Report Refer to the Cardholder Account Maintenance manual for more information about these reports. Please refer to Online Product Control File Parameters manual for more information.',
    node: (
      <div>
        <p>
          <strong>Warning!</strong>
        </p>
        <p>
          When establishing or changing parameter settings in the CP IC SZ,
          Securitization Controls section, be aware that you may also need to
          establish or change corresponding parameter settings in the CP IC II,
          Interest on Interest section. Parameter settings in both of these PCF
          sections may have significant implications for Reg AB Securitization
          Compliance. Please consult appropriate legal counsel or compliance
          personnel regarding any selections or changes you make.
        </p>
        <p>
          If you are involved in receivable-based financing, use the parameters
          in this section to determine whether the following fees will be
          reported as a finance charge or as a principal amount within
          securitization reporting.
        </p>
        <ul className="pl-16">
          <li>Annual charge</li>
          <li>Cash item fee</li>
          <li>Credit insurance premium</li>
          <li>Late charge</li>
          <li>Merchandise item fee</li>
          <li>Miscellaneous charge</li>
          <li>Overlimit charge</li>
          <li>Non-interest bearing debits</li>
        </ul>
        <p>
          The following reports display these fees in the interest or principal
          columns, based on how you set the parameters in this section.
        </p>
        <ul className="pl-16">
          <li>CD-124/CD-128/CD-1500, Portfolio Activity Summary reports</li>
          <li>CD-6753, Portfolio Securitization Detail Report</li>
          <li>CD-6754, Customer Level Portfolio Securitization Report</li>
          <li>CM-884, Stratification Report</li>
        </ul>
        <p>
          Refer to the Cardholder Account Maintenance manual for more
          information about these reports. Please refer to Online Product
          Control File Parameters manual for more information.
        </p>
      </div>
    )
  },
  CP_IC_TI: {
    raw: 'You can control the interest rate that applies to an account according to the balance the account carries. The tiered interest feature links interest rates to dollar amounts called break points. When an account balance exceeds a break point, the System begins using the interest rate corresponding the next break point. For example, accounts with balances less than or equal to break point 1 are assessed the interest rate linked to break point 1. You can use tiered interest when using either the average daily balance or daily accrual method of interest calculation. The tiered interest feature offers two break-point tables. Table 1 is activated by an effective date. If you enter values in table 2, the System will use those values until the effective date on table 1 goes into effect. When table 1 takes effect, control from table 2 stops. Please refer to Online Product Control File Parameters manual for more information. Warning! Selecting or changing the online Product Control File settings contained within this service/subject/section can have significant regulatory compliance implications. Altering these settings may require changes in the programming or design of your periodic statement or in the disclosures you have provided to your cardholders. Please consult your legal counsel or compliance personnel regarding selection or change of Product Control File settings.',
    node: (
      <div>
        <p>
          You can control the interest rate that applies to an account according
          to the balance the account carries. The tiered interest feature links
          interest rates to dollar amounts called break points. When an account
          balance exceeds a break point, the System begins using the interest
          rate corresponding the next break point. For example, accounts with
          balances less than or equal to break point 1 are assessed the interest
          rate linked to break point 1.
        </p>
        <p>
          You can use tiered interest when using either the average daily
          balance or daily accrual method of interest calculation.
        </p>
        <p>
          The tiered interest feature offers two break-point tables. Table 1 is
          activated by an effective date. If you enter values in table 2, the
          System will use those values until the effective date on table 1 goes
          into effect. When table 1 takes effect, control from table 2 stops.
          Please refer to Online Product Control File Parameters manual for more
          information.
        </p>
        <p>
          <strong>Warning!</strong>
        </p>
        <p>
          Selecting or changing the online Product Control File settings
          contained within this service/subject/section can have significant
          regulatory compliance implications. Altering these settings may
          require changes in the programming or design of your periodic
          statement or in the disclosures you have provided to your cardholders.
          Please consult your legal counsel or compliance personnel regarding
          selection or change of Product Control File settings.
        </p>
      </div>
    )
  },
  CP_IC_VI: {
    raw: 'The following parameters allow you to establish three different sets of interest rates, each set controlled by an effective date. Variable Interest Option Calculation Add Break Point Rates Prenotification Date Standard Prenotification Message ID Parameters under the Cash Set 1-3 heading Parameters under the Merchandise Set -3 heading Parameters under the Effective Date Set 1-3 heading This option allows for a smooth transition from one interest rate to another. Regulation Z requires the disclosure of each periodic rate that is used to compute the finance charge and the range of balances to which it is applicable. If you use more than one interest rate during the cardholder’s cycle, you may need a special statement format to properly disclose each rate and the balance to which it is applicable. Warning! Selecting or changing the online Product Control File settings contained within this service/subject/section can have significant regulatory compliance implications. Altering these settings may require changes in the programming or design of your periodic statement or in the disclosures you have provided to your cardholders. Please consult your legal counsel or compliance personnel regarding selection or change of Product Control File settings. Please refer to Online Product Control File Parameters manual for more information.',
    node: (
      <div>
        <p>
          The following parameters allow you to establish three different sets
          of interest rates, each set controlled by an effective date.
        </p>
        <ul className="pl-16">
          <li>Variable Interest Option</li>
          <li>Calculation</li>
          <li>Add Break Point Rates</li>
          <li>Prenotification Date</li>
          <li>Standard Prenotification Message ID</li>
          <li>Parameters under the Cash Set 1-3 heading</li>
          <li>Parameters under the Merchandise Set -3 heading</li>
          <li>Parameters under the Effective Date Set 1-3 heading</li>
        </ul>
        <p>
          This option allows for a smooth transition from one interest rate to
          another. Regulation Z requires the disclosure of each periodic rate
          that is used to compute the finance charge and the range of balances
          to which it is applicable.
        </p>
        <p>
          If you use more than one interest rate during the cardholder’s cycle,
          you may need a special statement format to properly disclose each rate
          and the balance to which it is applicable.
        </p>
        <p>
          <strong>Warning!</strong>
        </p>
        <p>
          Selecting or changing the online Product Control File settings
          contained within this service/subject/section can have significant
          regulatory compliance implications. Altering these settings may
          require changes in the programming or design of your periodic
          statement or in the disclosures you have provided to your cardholders.
          Please consult your legal counsel or compliance personnel regarding
          selection or change of Product Control File settings. Please refer to
          Online Product Control File Parameters manual for more information.
        </p>
      </div>
    )
  },
  CP_IO_AC: {
    raw: 'The parameters in this section control the calculation and assessment of annual charges. Use these parameters to set annual charge processing parameters in the following areas. Refer to the Annual Charges chapter in the Cardholder Billing manual for important information about how several parameters in this section work together to control annual charge processing. Warning! Selecting or changing the online Product Control File settings contained within this service/subject/section can have significant regulatory compliance implications. Altering these settings may require changes in the programming or design of your periodic statement or in the disclosures you have provided to your cardholders. Please consult your legal counsel or compliance personnel regarding selection or change of Product Control File settings. Please refer to Online Product Control File Parameters manual for more information.',
    node: (
      <div>
        <p>
          The parameters in this section control the calculation and assessment
          of annual charges. Use these parameters to set annual charge
          processing parameters in the following areas. Refer to the Annual
          Charges chapter in the Cardholder Billing manual for important
          information about how several parameters in this section work together
          to control annual charge processing.
        </p>
        <p>
          <strong>Warning!</strong>
        </p>
        <p>
          Selecting or changing the online Product Control File settings
          contained within this service/subject/section can have significant
          regulatory compliance implications. Altering these settings may
          require changes in the programming or design of your periodic
          statement or in the disclosures you have provided to your cardholders.
          Please consult your legal counsel or compliance personnel regarding
          selection or change of Product Control File settings. Please refer to
          Online Product Control File Parameters manual for more information.
        </p>
      </div>
    )
  },
  CP_IO_CI: {
    raw: 'The entries you make in the following parameters control the assessment and notification of cash advance item charges. Cash advance item charges are charges assessed to an account for a cash advance. The charge can be either a fixed amount or a percentage of the cash advance. There are four different sets of controls for cash advance item charges. Sets 1 through 3 are based on the assessment code of the merchant through which the cash advance was received. Set 4 controls item charges for ATM cash advances. Each different set allows you to control the percentage rate, minimum and maximum amounts, break point, high percent, and high amount. The corresponding parameters are included in this section. Warning! Selecting or changing the online Product Control File settings contained within this service/subject/section can have significant regulatory compliance implications. Altering these settings may require changes in the programming or design of your periodic statement or in the disclosures you have provided to your cardholders. Please consult your legal counsel or compliance personnel regarding selection or change of Product Control File settings. Please refer to Online Product Control File Parameters manual for more information.',
    node: (
      <div>
        <p>
          The entries you make in the following parameters control the
          assessment and notification of cash advance item charges. Cash advance
          item charges are charges assessed to an account for a cash advance.
          The charge can be either a fixed amount or a percentage of the cash
          advance. There are four different sets of controls for cash advance
          item charges. Sets 1 through 3 are based on the assessment code of the
          merchant through which the cash advance was received. Set 4 controls
          item charges for ATM cash advances. Each different set allows you to
          control the percentage rate, minimum and maximum amounts, break point,
          high percent, and high amount. The corresponding parameters are
          included in this section.
        </p>
        <p>
          <strong>Warning!</strong>
        </p>
        <p>
          Selecting or changing the online Product Control File settings
          contained within this service/subject/section can have significant
          regulatory compliance implications. Altering these settings may
          require changes in the programming or design of your periodic
          statement or in the disclosures you have provided to your cardholders.
          Please consult your legal counsel or compliance personnel regarding
          selection or change of Product Control File settings. Please refer to
          Online Product Control File Parameters manual for more information.
        </p>
      </div>
    )
  },
  CP_IO_CL: {
    raw: 'Credit life is a feature that allows you to offer customers insurance to cover the monthly payment due amount if a customer becomes unable to make payments under covered circumstances. Refer to the Credit Insurance manual for more information about processing a credit insurance product. Please refer to Online Product Control File Parameters manual for more information.',
    node: 'Credit life is a feature that allows you to offer customers insurance to cover the monthly payment due amount if a customer becomes unable to make payments under covered circumstances. Refer to the Credit Insurance manual for more information about processing a credit insurance product. Please refer to Online Product Control File Parameters manual for more information.'
  },
  CP_IO_MC: {
    raw: 'The entries you make in the following parameters control the assessment of miscellaneous charges. Miscellaneous charges include joining fees and card replacement fees. Please refer to Online Product Control File Parameters manual for more information.',
    node: 'The entries you make in the following parameters control the assessment of miscellaneous charges. Miscellaneous charges include joining fees and card replacement fees. Please refer to Online Product Control File Parameters manual for more information.'
  },
  CP_IO_MI: {
    raw: 'The entries you make in the following parameters control the assessment of merchandise item charges. Merchandise item charges are charges on sales posting to a cardholder’s account. Charges can be per item, or a single monthly charge. Warning! Selecting or changing the Product Control File settings contained within this service/subject/section can have significant regulatory compliance implications. Altering these settings may require changes in the programming or design of your periodic statement or in the disclosures you have provided to your cardholders. Please consult your legal counsel or compliance personnel regarding selection or change of these settings. Please refer to Online Product Control File Parameters manual for more information.',
    node: (
      <div>
        <p>
          The entries you make in the following parameters control the
          assessment of merchandise item charges. Merchandise item charges are
          charges on sales posting to a cardholder’s account. Charges can be per
          item, or a single monthly charge.
        </p>
        <p>
          <strong>Warning!</strong>
        </p>
        <p>
          Selecting or changing the Product Control File settings contained
          within this service/subject/section can have significant regulatory
          compliance implications. Altering these settings may require changes
          in the programming or design of your periodic statement or in the
          disclosures you have provided to your cardholders. Please consult your
          legal counsel or compliance personnel regarding selection or change of
          these settings. Please refer to Online Product Control File Parameters
          manual for more information.
        </p>
      </div>
    )
  },
  CP_IO_SC: {
    raw: 'The following parameters control statement charges. Statement charges are amounts you can assess accounts when statements are produced. Warning! Selecting or changing the online Product Control File settings contained within this service/subject/section can have significant regulatory compliance implications. Altering these settings may require changes in the programming or design of your periodic statement or in the disclosures you have provided to your cardholders. Please consult your legal counsel or compliance personnel regarding selection or change of Product Control File settings. Please refer to Online Product Control File Parameters manual for more information.',
    node: (
      <div>
        <p>
          The following parameters control statement charges. Statement charges
          are amounts you can assess accounts when statements are produced.
        </p>
        <p>
          <strong>Warning!</strong>
        </p>
        <p>
          Selecting or changing the online Product Control File settings
          contained within this service/subject/section can have significant
          regulatory compliance implications. Altering these settings may
          require changes in the programming or design of your periodic
          statement or in the disclosures you have provided to your cardholders.
          Please consult your legal counsel or compliance personnel regarding
          selection or change of Product Control File settings. Please refer to
          Online Product Control File Parameters manual for more information.
        </p>
      </div>
    )
  },
  CP_OC_AP: {
    raw: 'The parameters in this section control the impact of adjustments that post across an allocation cycle, also referred to as cross-cycle adjustments. An allocation cycle is when the System processes an account through client allocation and account qualification tables in the Account Level ProcessingSM service (ALPSM service) and the Method Level ProcessingSM service (MLPSM service) to determine the appropriate pricing strategies and method overrides for an account. This section applies only to accounts that use the ALP service or MLP service. Refer to the ALP Processing Considerations and MLP Processing Considerations chapters in the Issuer Marketing Products manual for more information about the ALP service and MLP service and cross-cycle adjustments. Please refer to Online Product Control File Parameters manual for more information.',
    node: (
      <div>
        <p>
          The parameters in this section control the impact of adjustments that
          post across an allocation cycle, also referred to as cross-cycle
          adjustments. An allocation cycle is when the System processes an
          account through client allocation and account qualification tables in
          the Account Level Processing<sup>SM</sup> service (ALP<sup>SM</sup>{' '}
          service) and the Method Level Processing<sup>SM</sup> service (MLP
          <sup>SM</sup> service) to determine the appropriate pricing strategies
          and method overrides for an account. This section applies only to
          accounts that use the ALP service or MLP service.
        </p>
        <p>
          Refer to the ALP Processing Considerations and MLP Processing
          Considerations chapters in the Issuer Marketing Products manual for
          more information about the ALP service and MLP service and cross-cycle
          adjustments. Please refer to Online Product Control File Parameters
          manual for more information.
        </p>
      </div>
    )
  },
  CP_OC_BD: {
    raw: 'Parameters in this section control the backdating of sales, returns, cash advances, convenience checks, and payments. These parameters enable you to select the number of days each type of monetary item can be backdated, whether it can be backdated across cycle, and the method of handling inactive and never active accounts. In addition to the backdating options, the Adj Date Display Override parameter in this section enables you to determine whether the following appears on cardholder statements and certain Customer Inquiry System screens. Original date of a transaction that was not backdated to the transaction date Processing date of a reversal or adjustment for the original transaction that posted to the account Use of the Adj Date Display Override parameter has no impact on any backdating processing controls that you establish. Warning! Selecting or changing the online Product Control File settings contained within this service/subject/section can have significant regulatory compliance implications. Altering these settings may require changes in the programming or design of your periodic statement or in the disclosures you have provided to your cardholders. Please consult your legal counsel or compliance personnel regarding selection or change of Product Control File settings. Please refer to Online Product Control File Parameters manual for more information.',
    node: (
      <div>
        <p>
          Parameters in this section control the backdating of sales, returns,
          cash advances, convenience checks, and payments. These parameters
          enable you to select the number of days each type of monetary item can
          be backdated, whether it can be backdated across cycle, and the method
          of handling inactive and never active accounts.
        </p>
        <p>
          In addition to the backdating options, the Adj Date Display Override
          parameter in this section enables you to determine whether the
          following appears on cardholder statements and certain Customer
          Inquiry System screens.
        </p>
        <ul className="pl-16">
          <li>
            Original date of a transaction that was not backdated to the
            transaction date
          </li>
          <li>
            Processing date of a reversal or adjustment for the original
            transaction that posted to the account
          </li>
        </ul>
        <p>
          Use of the Adj Date Display Override parameter has no impact on any
          backdating processing controls that you establish.
        </p>
        <p>
          <strong>Warning!</strong>
        </p>
        <p>
          Selecting or changing the online Product Control File settings
          contained within this service/subject/section can have significant
          regulatory compliance implications. Altering these settings may
          require changes in the programming or design of your periodic
          statement or in the disclosures you have provided to your cardholders.
          Please consult your legal counsel or compliance personnel regarding
          selection or change of Product Control File settings. Please refer to
          Online Product Control File Parameters manual for more information.
        </p>
      </div>
    )
  },
  CP_OC_CL: {
    raw: 'The parameters in this section control the calculation and adjustment of account credit lines. The New Accounts and Cardholder Exceptions sections of the online Product Control File include additional parameters for credit line processing. Please refer to Online Product Control File Parameters manual for more information.',
    node: 'The parameters in this section control the calculation and adjustment of account credit lines. The New Accounts and Cardholder Exceptions sections of the online Product Control File include additional parameters for credit line processing. Please refer to Online Product Control File Parameters manual for more information.'
  },
  CP_OC_CP: {
    raw: 'This section of the Product Control File is not available for use. Use the manual disclosure parameters in the CP OC FP, Full Pricing Notification section, to control on-demand current pricing notifications. Please refer to Online Product Control File Parameters manual for more information.',
    node: 'This section of the Product Control File is not available for use. Use the manual disclosure parameters in the CP OC FP, Full Pricing Notification section, to control on-demand current pricing notifications. Please refer to Online Product Control File Parameters manual for more information.'
  },
  CP_OC_DR: {
    raw: 'Parameters in this section work in conjunction with the Account Level ProcessingSM service (ALPSM service) to control debit ratification. Debit ratification enables you to allow cardholders to accept the change to this pricing strategy by using their plastic or line of credit on or after a given date. These parameters affect accounts that currently process under this pricing strategy. This section applies only to strategies used with the ALP service. Warning! Selecting or changing the settings of parameters in the Debit Ratification section of the online Product Control File can have significant regulatory compliance implications. Altering these settings may require changes in the design of disclosures you provide to your cardholders. Consult your legal counsel or compliance personnel regarding selection or change of these settings. Debit ratification for the ALP service is not designed to work in conjunction with change in terms (CIT) methods. If you set parameters in the Debit Ratification section (CP OC DR) of the Product Control File (PCF) and use the CIT methods, the results may not meet your business expectations and may not meet regulatory guidelines. Fiserv strongly recommends you do not use the Debit Ratification section of the PCF to establish debit ratification for the ALP service if you use CIT methods in your processing. Refer to the AO AC DR, Debit Ratification chapter in the Agent Only Parameters manual for information about debit ratification for strategies used with agent assignment. Refer to the Debit Ratification chapter in the Cardholder Terms and Disclosures manual for more information about debit ratification. Refer to the Issuer Marketing Products manual for more information about the ALP service. Please refer to Online Product Control File Parameters manual for more information.',
    node: (
      <div>
        <p>
          Parameters in this section work in conjunction with the Account Level
          Processing<sup>SM</sup> service (ALP<sup>SM</sup> service) to control
          debit ratification. Debit ratification enables you to allow
          cardholders to accept the change to this pricing strategy by using
          their plastic or line of credit on or after a given date. These
          parameters affect accounts that currently process under this pricing
          strategy. This section applies only to strategies used with the ALP
          service.
        </p>
        <p>
          <strong>Warning!</strong>
        </p>
        <p>
          Selecting or changing the settings of parameters in the Debit
          Ratification section of the online Product Control File can have
          significant regulatory compliance implications. Altering these
          settings may require changes in the design of disclosures you provide
          to your cardholders. Consult your legal counsel or compliance
          personnel regarding selection or change of these settings.
        </p>
        <p>
          Debit ratification for the ALP service is not designed to work in
          conjunction with change in terms (CIT) methods. If you set parameters
          in the Debit Ratification section (CP OC DR) of the Product Control
          File (PCF) and use the CIT methods, the results may not meet your
          business expectations and may not meet regulatory guidelines. Fiserv
          strongly recommends you do not use the Debit Ratification section of
          the PCF to establish debit ratification for the ALP service if you use
          CIT methods in your processing.
        </p>
        <p>
          Refer to the AO AC DR, Debit Ratification chapter in the Agent Only
          Parameters manual for information about debit ratification for
          strategies used with agent assignment. Refer to the Debit Ratification
          chapter in the Cardholder Terms and Disclosures manual for more
          information about debit ratification. Refer to the Issuer Marketing
          Products manual for more information about the ALP service. Please
          refer to Online Product Control File Parameters manual for more
          information.
        </p>
      </div>
    )
  },
  CP_OC_FP: {
    raw: 'The parameters in this section control disclosures of current or future terms for cardholder accounts. The parameters are grouped according to their function under three headings. MANUAL DISCLOSURE ANNUAL DISCLOSURE DELAYED CHANGE  DISCLOSURE Please refer to Online Product Control File Parameters manual for more information.',
    node: (
      <div>
        <p>
          The parameters in this section control disclosures of current or
          future terms for cardholder accounts. The parameters are grouped
          according to their function under three headings.
        </p>
        <ul className="pl-16">
          <li>MANUAL DISCLOSURE</li>
          <li>ANNUAL DISCLOSURE</li>
          <li>DELAYED CHANGE DISCLOSURE</li>
        </ul>
        <p>
          Please refer to Online Product Control File Parameters manual for more
          information.
        </p>
      </div>
    )
  },
  CP_OC_GS: {
    raw: 'The entries you make in the following parameters control the information that prints on Relationship Processing® group master statements. Please refer to Online Product Control File Parameters manual for more information.',
    node: (
      <div>
        <p>
          The entries you make in the following parameters control the
          information that prints on Relationship Processing® group master
          statements.
        </p>
        <p>
          Please refer to Online Product Control File Parameters manual for more
          information.
        </p>
      </div>
    )
  },
  CP_OC_MO: {
    raw: 'The parameters in this section determine the default method overrides for new Account Level ProcessingSM accounts. If you use the ALPSM service, you can use these parameters to implement new pricing methods for individual cardholders without creating new strategies. Once you assign the default method overrides to a strategy, the System will automatically assign those method overrides to each new account within that strategy. Please refer to Online Product Control File Parameters manual for the list of method overrides you can enter.',
    node: (
      <div>
        <p>
          The parameters in this section determine the default method overrides
          for new Account Level Processing<sup>SM</sup> accounts. If you use the
          ALP<sup>SM</sup> service, you can use these parameters to implement
          new pricing methods for individual cardholders without creating new
          strategies.
        </p>
        <p>
          Once you assign the default method overrides to a strategy, the System
          will automatically assign those method overrides to each new account
          within that strategy. Please refer to Online Product Control File
          Parameters manual for the list of method overrides you can enter.
        </p>
      </div>
    )
  },
  CP_OC_MP: {
    raw: 'The parameters in this section control interest-on-credit (MULTRAN) processing and the assessment of various types of service charges based on an account’s balance. Basic interest on credit processing involves calculating and paying interest to cardholders on their credit balances, as well as charging them interest on their debit balances. This type of processing enables you to create the type of account you wish to offer. You can offer a traditional credit card account paying interest on credit balances, or a debit card account with an overdraft or credit reserve. You can also offer a secured account that pairs a security deposit account with a credit card account and allows you to offer credit to high risk applicants. A security deposit account is an interest-on-credit account, but does not accrue interest. A secured account eliminates the risk by requiring that a minimum balance be maintained in the security deposit account. You control interest on credit processing and determine participation in this type of processing by setting the Multran Processing Option parameter in the Miscellaneous section (AO AC MC) of the Product Control File. Please refer to Online Product Control File Parameters manual for more information.',
    node: (
      <div>
        <p>
          The parameters in this section control interest-on-credit (MULTRAN)
          processing and the assessment of various types of service charges
          based on an account’s balance.
        </p>
        <p>
          Basic interest on credit processing involves calculating and paying
          interest to cardholders on their credit balances, as well as charging
          them interest on their debit balances. This type of processing enables
          you to create the type of account you wish to offer. You can offer a
          traditional credit card account paying interest on credit balances, or
          a debit card account with an overdraft or credit reserve. You can also
          offer a secured account that pairs a security deposit account with a
          credit card account and allows you to offer credit to high risk
          applicants. A security deposit account is an interest-on-credit
          account, but does not accrue interest. A secured account eliminates
          the risk by requiring that a minimum balance be maintained in the
          security deposit account.
        </p>
        <p>
          You control interest on credit processing and determine participation
          in this type of processing by setting the Multran Processing Option
          parameter in the Miscellaneous section (AO AC MC) of the Product
          Control File. Please refer to Online Product Control File Parameters
          manual for more information.
        </p>
      </div>
    )
  },
  CP_OC_PP: {
    raw: 'The parameters in this section control the following special promotional features. Transaction Level ProcessingSM service Deferred Payments Monthly Unique Balances Please refer to Online Product Control File Parameters manual for more information.',
    node: (
      <div>
        <p>
          The parameters in this section control the following special
          promotional features.
        </p>
        <ul className="pl-16">
          <li>
            Transaction Level Processing<sup>SM</sup> service
          </li>
          <li>Deferred Payments</li>
          <li>Monthly Unique Balances</li>
        </ul>
        <p>
          Please refer to Online Product Control File Parameters manual for more
          information.
        </p>
      </div>
    )
  },
  CP_OC_RB: {
    raw: 'This section of the Product Control File is not available for use. Use the Rewards platform to establish rewards rebate and/or bonus programs. Please refer to Online Product Control File Parameters manual for more information.',
    node: 'This section of the Product Control File is not available for use. Use the Rewards platform to establish rewards rebate and/or bonus programs. Please refer to Online Product Control File Parameters manual for more information.'
  },
  CP_OC_SA: {
    raw: 'The parameters in this section are either reserved for restricted processing or are used by Australian processors only. Please refer to Online Product Control File Parameters manual for more information.',
    node: 'The parameters in this section are either reserved for restricted processing or are used by Australian processors only. Please refer to Online Product Control File Parameters manual for more information.'
  },
  CP_OC_SD: {
    raw: 'The entries you make in the following parameters control which information prints on customer statements. Up to six screens are available. Screens 4 and 5 mirror screen 3. Please refer to Online Product Control File Parameters manual for more information.',
    node: 'The entries you make in the following parameters control which information prints on customer statements. Up to six screens are available. Screens 4 and 5 mirror screen 3. Please refer to Online Product Control File Parameters manual for more information.'
  },
  CP_OC_TI: {
    raw: 'The parameters in this section allow you to provide Basic Travel Insurance automatically whenever a cardholder makes a qualifying airline ticket purchase. Please refer to Online Product Control File Parameters manual for more information.',
    node: 'The parameters in this section allow you to provide Basic Travel Insurance automatically whenever a cardholder makes a qualifying airline ticket purchase. Please refer to Online Product Control File Parameters manual for more information.'
  },
  CP_PB_CT: {
    raw: 'The parameters in this section control implementation of a change in pricing terms for your customers. By associating a change-in-terms (CIT) method with a pricing strategy, you can determine the following aspects of changes in terms. The date on which the new terms are applied to the customer account The date on which the System generated terms disclosure notification customers so that you can provide your customers with advance notice of the change Balance protection, which sets aside certain balances so that they continue to be processed under existing terms and not subject to the new terms The messages used for disclosure notifications Be aware that Fiserv will not override existing statement suppression controls to produce change-in-terms notifications that are initiated from a CIT method. Refer to the CIT Processing and Statement Suppression heading in the Change-in-Terms Methods chapter in the Cardholder Terms and Disclosures manual for more information.',
    node: (
      <div>
        <p>
          The parameters in this section control implementation of a change in
          pricing terms for your customers. By associating a change-in-terms
          (CIT) method with a pricing strategy, you can determine the following
          aspects of changes in terms.
        </p>
        <ul className="pl-16">
          <li>
            The date on which the new terms are applied to the customer account
          </li>
          <li>
            The date on which the System generated terms disclosure notification
            customers so that you can provide your customers with advance notice
            of the change
          </li>
          <li>
            {' '}
            Balance protection, which sets aside certain balances so that they
            continue to be processed under existing terms and not subject to the
            new terms
          </li>
          <li>The messages used for disclosure notifications</li>
        </ul>
        <p>
          <strong>Warning!</strong>
        </p>
        <p>
          Be aware that Fiserv will not override existing statement suppression
          controls to produce change-in-terms notifications that are initiated
          from a CIT method.
        </p>
        <p>
          Refer to the CIT Processing and Statement Suppression heading in the
          Change-in-Terms Methods chapter in the Cardholder Terms and
          Disclosures manual for more information.
        </p>
      </div>
    )
  },
  CP_PF_DA: {
    raw: 'The parameters in this section allow you to automatically assess a charge when the System declines a convenience check authorization because the transaction exceeded the customer account’s available credit. You can also set criteria to limit these charges by using the parameters in this section. The System does not assess charges for declined merchandise authorizations. Charges for declined batch or online authorizations appear as miscellaneous charges on the CD-121, Ledger Activity Report. They also appear under account fees and late charges on the CM-051/CM-052, Cardholder Management Report/ Cardholder Management Chain Report. Please refer to Online Product Control File Parameters manual for more information.',
    node: (
      <div>
        <p>
          The parameters in this section allow you to automatically assess a
          charge when the System declines a convenience check authorization
          because the transaction exceeded the customer account’s available
          credit. You can also set criteria to limit these charges by using the
          parameters in this section.
        </p>
        <p>
          The System does not assess charges for declined merchandise
          authorizations.
        </p>
        <p>
          Charges for declined batch or online authorizations appear as
          miscellaneous charges on the CD-121, Ledger Activity Report. They also
          appear under account fees and late charges on the CM-051/CM-052,
          Cardholder Management Report/ Cardholder Management Chain Report.
          Please refer to Online Product Control File Parameters manual for more
          information.
        </p>
      </div>
    )
  },
  CP_PF_LC: {
    raw: 'You can assess a customer a charge at account cycle time when the payment due is late. The parameters in this section provide you with the option of setting the maximum late charge amount, late charge calculations, and late charges based on an account’s external status. You can add late charges into the minimum payment due calculation for an account by setting the Late Charge parameter in the Minimum Payment Due section (CP PO MP). Please refer to Online Product Control File Parameters manual for more information.',
    node: (
      <div>
        <p>
          You can assess a customer a charge at account cycle time when the
          payment due is late. The parameters in this section provide you with
          the option of setting the maximum late charge amount, late charge
          calculations, and late charges based on an account’s external status.
        </p>
        <p>
          You can add late charges into the minimum payment due calculation for
          an account by setting the Late Charge parameter in the Minimum Payment
          Due section (CP PO MP). Please refer to Online Product Control File
          Parameters manual for more information.
        </p>
      </div>
    )
  },
  CP_PF_OC: {
    raw: 'You can assess customers a charge when their account balances become greater than the established credit line. You can find general information about overlimit processing in the Collections and Debt Management manual. Use the parameters in this section to determine whether an account is overlimit and to establish the calculation and assessment of overlimit charges. These parameters calculate overlimit charge amounts, set maximum and minimum limits, control other charge calculation options, and control assessment options. You can add overlimit charges into the minimum payment due calculation for an account by setting the Overlimit Charge parameter in the Minimum Payment Due section. Please refer to Online Product Control File Parameters manual for more information.',
    node: (
      <div>
        <p>
          You can assess customers a charge when their account balances become
          greater than the established credit line. You can find general
          information about overlimit processing in the Collections and Debt
          Management manual.
        </p>
        <p>
          Use the parameters in this section to determine whether an account is
          overlimit and to establish the calculation and assessment of overlimit
          charges. These parameters calculate overlimit charge amounts, set
          maximum and minimum limits, control other charge calculation options,
          and control assessment options.
        </p>
        <p>
          You can add overlimit charges into the minimum payment due calculation
          for an account by setting the Overlimit Charge parameter in the
          Minimum Payment Due section. Please refer to Online Product Control
          File Parameters manual for more information.
        </p>
      </div>
    )
  },
  CP_PF_RC: {
    raw: 'You can assess a charge to the customer when a payment check is returned to you for insufficient funds. Use the parameters in this section to set returned check minimum and maximum amounts, statement messages, and other controls. Please refer to Online Product Control File Parameters manual for more information.',
    node: 'You can assess a charge to the customer when a payment check is returned to you for insufficient funds. Use the parameters in this section to set returned check minimum and maximum amounts, statement messages, and other controls. Please refer to Online Product Control File Parameters manual for more information.'
  },
  CP_PO_CA: {
    raw: '',
    node: (
      <div>
        <p>
          The parameters in this section control the following credit
          application features.
        </p>
        <ul className="pl-16">
          <li>Cash advance payments</li>
          <li>Maximizer control</li>
          <li>Payment and return application sequences</li>
          <li>Payment reversals</li>
          <li>Percentage of payment to principal</li>
          <li>Returns</li>
          <li>Special options</li>
          <li>Split minimum payment due payment application</li>
        </ul>
        <p>
          Please refer to Online Product Control File Parameters manual for more
          information.
        </p>
      </div>
    )
  },
  CP_PO_FM: {
    raw: 'The parameters in this section control the calculation and assessment of fixed minimum payments. These parameters control the manner in which the System calculates the fixed minimum payment, as well as the amounts the System assesses as fixed minimum payments. Please refer to Online Product Control File Parameters manual for more information.',
    node: 'The parameters in this section control the calculation and assessment of fixed minimum payments. These parameters control the manner in which the System calculates the fixed minimum payment, as well as the amounts the System assesses as fixed minimum payments. Please refer to Online Product Control File Parameters manual for more information.'
  },
  CP_PO_MP: {
    raw: 'The parameters in this section allow you to control the calculation and assessment of the minimum payment due for the standard balance. You can set unique minimum payment due controls for promotional balances posted through the Transaction Level ProcessingSM service using the parameters in the Promotion Controls section (PL RT PC) of the Product Control File. Please refer to Online Product Control File Parameters manual for more information.',
    node: (
      <div>
        <p>
          The parameters in this section allow you to control the calculation
          and assessment of the minimum payment due for the standard balance.
        </p>
        <p>
          You can set unique minimum payment due controls for promotional
          balances posted through the Transaction Level ProcessingSM service
          using the parameters in the Promotion Controls section (PL RT PC) of
          the Product Control File. Please refer to Online Product Control File
          Parameters manual for more information.
        </p>
      </div>
    )
  },
  CP_PO_RM: {
    raw: 'The parameters in this section allow you to control minimum payment due processing in the Pricing service of Rules Management. Please refer to Online Product Control File Parameters manual for more information.',
    node: 'The parameters in this section allow you to control minimum payment due processing in the Pricing service of Rules Management. Please refer to Online Product Control File Parameters manual for more information.'
  },
  CP_PO_SP: {
    raw: 'The parameters in this section control the skip payment option. Cardholders avoid becoming delinquent if a payment is not received during the skip payment month. The skip payment feature gives you the following options. You can offer cardholders the option of skipping a payment in a specified month you determine. You can send cardholders skip payment coupons. Cardholders initiate the skip payment in any month by returning a coupon with their remittance slip. You can also offer a combination of both options. Warning! Do not use the Skip Payment parameters if you set the Standard Minimum Payments Calculation Method parameter in the Promotion Controls section (PL RT PC) of the Product Control File to a value other than zero. Please refer to Online Product Control File Parameters manual for more information.',
    node: (
      <div>
        <p>
          The parameters in this section control the skip payment option.
          Cardholders avoid becoming delinquent if a payment is not received
          during the skip payment month.
        </p>
        <p>The skip payment feature gives you the following options.</p>
        <ul className="pl-16">
          <li>
            You can offer cardholders the option of skipping a payment in a
            specified month you determine.
          </li>
          <li>
            You can send cardholders skip payment coupons. Cardholders initiate
            the skip payment in any month by returning a coupon with their
            remittance slip.
          </li>
          <li>You can also offer a combination of both options.</li>
        </ul>
        <p>
          <strong>Warning!</strong>
        </p>
        <p>
          Do not use the Skip Payment parameters if you set the Standard Minimum
          Payments Calculation Method parameter in the Promotion Controls
          section (PL RT PC) of the Product Control File to a value other than
          zero. Please refer to Online Product Control File Parameters manual
          for more information.
        </p>
      </div>
    )
  }
};

export const METHOD_SUB_DATA = (function () {
  const results = {} as Record<
    Subject,
    Array<Omit<MethodSubItem, 'newAssignment' | 'currentAssignment'>>
  >;
  Object.keys(SUBJECT).forEach(subject => {
    const subData: Array<
      Omit<MethodSubItem, 'newAssignment' | 'currentAssignment'>
    > = [];
    Object.entries(MORE_INFO).forEach(([serviceSubjectSection, moreInfo]) => {
      if (serviceSubjectSection.split('_')[1] === subject) {
        subData.push({
          id: serviceSubjectSection,
          serviceSubjectSection:
            SERVICE_SUBJECT_SECTION[
              serviceSubjectSection as keyof typeof SERVICE_SUBJECT_SECTION
            ],
          moreInfoText: moreInfo.raw
        });
      }
    });
    results[subject as Subject] = subData;
  });
  return results;
})();
