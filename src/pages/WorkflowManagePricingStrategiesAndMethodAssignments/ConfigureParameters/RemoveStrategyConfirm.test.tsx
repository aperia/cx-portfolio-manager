import { fireEvent, render } from '@testing-library/react';
import 'app/utils/_mockComponent/mockModalRegistry';
import React from 'react';
import {
  RemoveStrategyConfirm,
  useRemoveStrategy
} from './RemoveStrategyConfirm';

const mSubmitCb = jest.fn();

const FakeComponent = () => {
  const { openRemoveStrategyConfirm, ...props } = useRemoveStrategy();
  return (
    <>
      <button
        data-testid="open-modal"
        onClick={() => {
          openRemoveStrategyConfirm(mSubmitCb);
        }}
      >
        open
      </button>
      <RemoveStrategyConfirm {...props} />
    </>
  );
};

const factory = () => {
  const { getByText, queryByText, getByTestId } = render(<FakeComponent />);

  const openModal = () => {
    fireEvent.click(getByTestId('open-modal'));
  };

  const triggerClose = () => {
    fireEvent.click(getByText('txt_cancel'));
  };

  const triggerSubmit = () => {
    fireEvent.click(getByText('txt_delete'));
  };

  const getModalTitle = () => queryByText('txt_confirm_deletion');

  return { openModal, getModalTitle, triggerClose, triggerSubmit };
};

describe('test RemoveStrategyConfirm component', () => {
  it('should be close', () => {
    const { openModal, triggerClose, getModalTitle } = factory();
    openModal();
    expect(getModalTitle()).toBeInTheDocument();
    triggerClose();
    expect(getModalTitle()).not.toBeInTheDocument();
  });

  it('should be submit', () => {
    const { openModal, triggerSubmit, getModalTitle } = factory();
    openModal();
    expect(getModalTitle()).toBeInTheDocument();
    triggerSubmit();
    expect(mSubmitCb).toBeCalled();
  });
});
