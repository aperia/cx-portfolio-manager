import {
  convertToOpenStrategyDetailsValues,
  flattenMethodList,
  formatStrategyPreviewListData,
  generateMethodListFromMethodData,
  generateMethodsNewAssignmentInit,
  getMethodListFromStrategyList,
  groupByServiceSubjectSection
} from './helpers';

describe('test getMethodListFromStrategyList helper', () => {
  const strategyListFixture: Strategy[] = [
    {
      id: 'ID',
      name: 'STT1',
      versions: [
        {
          effectiveDate: '2022-02-02',
          accountsProcessedAA: '1234',
          accountsProcessedALP: '123',
          status: 'Production',
          cisDescription: '',
          commentArea: '',
          id: '123',
          methods: [
            { currentAssignment: 'ABC', serviceSubjectSection: 'CP IC ID' }
          ]
        }
      ]
    }
  ];
  it('strategyName does not existed in params', () => {
    const actual = getMethodListFromStrategyList(strategyListFixture);
    expect(actual).toEqual([]);
  });

  it('strategyName does not existed in strategyList', () => {
    const actual = getMethodListFromStrategyList(
      strategyListFixture,
      'OTHER',
      '123'
    );
    expect(actual).toEqual([]);
  });

  it('version does not existed in strategyList', () => {
    const actual = getMethodListFromStrategyList(
      strategyListFixture,
      'STT1',
      '1234'
    );
    expect(actual).toEqual([]);
  });

  it('should be working correctly', () => {
    const actual = getMethodListFromStrategyList(
      strategyListFixture,
      'STT1',
      '123'
    );
    expect(actual).toEqual(strategyListFixture[0].versions[0].methods);
  });
});

describe('test flattenMethodList helper', () => {
  const strategyListMethodFixture = [
    { currentAssignment: 'ABC', serviceSubjectSection: 'CP IC ID' },
    { currentAssignment: 'ABC', serviceSubjectSection: 'CP IC IE' }
  ];

  it('should be return empty if not pass list data', () => {
    const actual = flattenMethodList();
    expect(actual).toEqual({});
  });

  it('should be working correctly', () => {
    const actual = flattenMethodList(strategyListMethodFixture);
    expect(actual).toEqual({ CP_IC_ID: 'ABC' });
  });
});

describe('test groupByServiceSubjectSection helper', () => {
  const fixture = [
    { currentAssignment: 'ABC', serviceSubjectSection: 'CP IC ID' },
    { currentAssignment: 'ABC', serviceSubjectSection: 'CP IC IE' }
  ];

  it('should be working correctly', () => {
    const actual = groupByServiceSubjectSection(fixture);
    expect(actual).toEqual({
      CP_IC_ID: { currentAssignment: 'ABC', serviceSubjectSection: 'CP IC ID' }
    });
  });
});

describe('test generateMethodListFromMethodData helper', () => {
  const fixture = { CP_IC_ID: 'ABC' };

  it('should be working correctly', () => {
    const actual = generateMethodListFromMethodData(fixture, fixture);
    expect(actual.length).toEqual(6);
  });
});

describe('test generateMethodsNewAssignmentInit helper', () => {
  it('should be working correctly', () => {
    const actual = generateMethodsNewAssignmentInit();
    expect(actual).toBeTruthy();
  });
});

describe('test formatStrategyPreviewListData helper', () => {
  const fixture = [
    {
      unique: '12',
      name: 'STT',
      modeledFrom: { name: 'STT1', id: '2' },
      fromVersion: '222',
      cisDescription: '',
      commentArea: '',
      methods: [
        {
          serviceSubjectSection: 'CP IC ID',
          previousValue: 'Method1',
          newValue: 'Method2'
        }
      ]
    },
    {
      unique: '12',
      name: 'STT',
      modeledFrom: { name: 'STT', id: '2' },
      fromVersion: '222',
      cisDescription: '',
      commentArea: '',
      methods: [
        {
          serviceSubjectSection: 'CP IC ID',
          previousValue: 'Method1',
          newValue: 'Method2'
        }
      ]
    }
  ];
  it('should be working correctly', () => {
    const actual = formatStrategyPreviewListData(fixture);
    expect(actual.length).toEqual(2);
  });
});

describe('test convertToOpenStrategyDetailsValues helper', () => {
  const fixture = {
    uniqueId: '123',
    strategyName: 'STT',
    modeled: 'STT',
    modeledId: '12',
    cisDescription: '',
    commentArea: '',
    actionTaken: '',
    fromVersion: '',
    serviceSubject: [
      {
        rowId: '123',
        service: 'IC',
        subject: 'CP',
        subData: [
          {
            newAssignment: 'MT',
            currentAssignment: 'MT',
            id: '123',
            serviceSubjectSection: 'CP IC ID',
            moreInfoText: ''
          }
        ]
      }
    ]
  };
  it('should be working correctly', () => {
    const actual = convertToOpenStrategyDetailsValues(fixture);
    expect(actual.informationValues).toEqual({
      cisDescription: '',
      comment: '',
      selectedStrategy: { id: '12', name: 'STT' },
      strategyName: 'STT'
    });
  });
  it('should be working correctly', () => {
    fixture.modeled = 'STT2';
    const actual = convertToOpenStrategyDetailsValues(fixture);
    expect(actual.informationValues).toEqual({
      cisDescription: '',
      comment: '',
      selectedStrategy: { id: '12', name: 'STT2' },
      strategyName: 'STT'
    });
  });
});
