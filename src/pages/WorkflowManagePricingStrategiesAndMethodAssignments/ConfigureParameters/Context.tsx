import { isEqual } from 'app/_libraries/_dls/lodash';
import React, {
  createContext,
  PropsWithChildren,
  useCallback,
  useContext,
  useEffect,
  useRef,
  useState
} from 'react';
import { MethodItem } from './types';

export const emptyFunction = () => {};

const Context = createContext<{
  methodList: MethodItem[];
  onChangeAssignment: (
    serviceSubjectSection: string,
    newAssignment: string
  ) => void;
  isMethodListDirty: boolean;
}>({
  methodList: [],
  onChangeAssignment: emptyFunction,
  isMethodListDirty: false
});

const MethodProvider: React.FC<
  PropsWithChildren<{ methodList: MethodItem[] }>
> = ({ methodList: methodListProp, children }) => {
  const prevMethodList = useRef<MethodItem[]>([]);
  const [methodList, setMethodList] = useState(methodListProp);

  const onChangeAssignment = useCallback(
    (serviceSubjectSection: string, newAssignment: string) => {
      setMethodList(_prev => {
        const prev: MethodItem[] = JSON.parse(JSON.stringify(_prev));
        const [, subject] = serviceSubjectSection.split('_');
        const findBySubjectIndex = prev.findIndex(
          item => item.rowId === subject
        );
        if (findBySubjectIndex === -1) return prev;
        const findByServiceSubjectSectionIndex = prev[
          findBySubjectIndex
        ].subData.findIndex(item => item.id === serviceSubjectSection);
        if (findByServiceSubjectSectionIndex === -1) return prev;
        prev[findBySubjectIndex].subData[findByServiceSubjectSectionIndex] = {
          ...prev[findBySubjectIndex].subData[findByServiceSubjectSectionIndex],
          newAssignment
        };
        return prev;
      });
    },
    []
  );

  useEffect(() => {
    setMethodList(methodListProp);
    prevMethodList.current = methodListProp;
  }, [methodListProp]);

  return (
    <Context.Provider
      value={{
        methodList,
        onChangeAssignment,
        isMethodListDirty: !isEqual(prevMethodList.current, methodList)
      }}
    >
      {children}
    </Context.Provider>
  );
};

const useMethodContext = () => useContext(Context);

export { MethodProvider, useMethodContext };
