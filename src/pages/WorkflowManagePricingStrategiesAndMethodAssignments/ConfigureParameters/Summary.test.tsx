import { fireEvent, render } from '@testing-library/react';
import React from 'react';
import Summary from './Summary';

jest.mock('./StrategyPreviewListGrid', () => ({
  StrategyPreviewListGrid: () => (
    <div data-testid="StrategyPreviewListGrid">StrategyPreviewListGrid</div>
  )
}));

const mOnEditStep = jest.fn();

const factory = (isStepDiff?: boolean) => {
  const { getByTestId, getByText } = render(
    <Summary
      originalStepConfig=""
      onEditStep={mOnEditStep}
      formValues={{}}
      stepId="step1"
      selectedStep={isStepDiff ? 'step2' : 'step1'}
    />
  );

  const getStrategyPreviewListGrid = () =>
    getByTestId('StrategyPreviewListGrid');

  const triggerEditStep = () => {
    fireEvent.click(getByText('txt_edit'));
  };
  return { getStrategyPreviewListGrid, triggerEditStep };
};

describe('test Summary component', () => {
  it('should be render', () => {
    const { getStrategyPreviewListGrid } = factory();
    expect(getStrategyPreviewListGrid()).toBeInTheDocument();
  });

  it('should be render when stepId not equal selectedStep', () => {
    const { getStrategyPreviewListGrid } = factory(true);
    expect(getStrategyPreviewListGrid()).toBeInTheDocument();
  });

  it('should be edit step', () => {
    const { triggerEditStep } = factory();
    triggerEditStep();
    expect(mOnEditStep).toBeCalled();
  });
});
