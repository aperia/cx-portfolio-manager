import { FormGettingStart } from '.';
import stepValueFunc from './stepValueFunc';

describe('ManagePenaltyFeeWorkflow > GettingStartStep > stepValueFunc', () => {
  it('Should Return Check Charges and Late Charges', () => {
    const stepsForm = {
      lateCharges: true,
      returnedCheckCharges: true
    } as FormGettingStart;

    const response = stepValueFunc({ stepsForm });
    expect(response).toEqual('Returned Check Charges and Late Charges');
  });

  it('Should Return Check Charges', () => {
    const stepsForm = {
      returnedCheckCharges: true
    } as FormGettingStart;

    const response = stepValueFunc({ stepsForm });
    expect(response).toEqual('Returned Check Charges');
  });

  it('Should Return Late Charges', () => {
    const stepsForm = {
      lateCharges: true
    } as FormGettingStart;

    const response = stepValueFunc({ stepsForm });
    expect(response).toEqual('Late Charges');
  });

  it('Should Return empty', () => {
    const stepsForm: any = undefined;

    const response = stepValueFunc({ stepsForm });
    expect(response).toEqual('');
  });
});
