import { FormGettingStart } from '.';

const stepValueFunc: WorkflowSetupStepValueFunc<FormGettingStart> = ({
  stepsForm: formValues
}) => {
  const { returnedCheckCharges, lateCharges } = formValues || {};

  if (returnedCheckCharges && lateCharges) {
    return 'Returned Check Charges and Late Charges';
  }

  return returnedCheckCharges
    ? 'Returned Check Charges'
    : lateCharges
    ? 'Late Charges'
    : '';
};

export default stepValueFunc;
