import { fireEvent, render, RenderResult } from '@testing-library/react';
import React from 'react';
import GettingStartSummary from './GettingStartSummary';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <GettingStartSummary {...props} />
    </div>
  );
};

describe('ManagePenaltyFeeWorkflow > GettingStartStep > GettingStartSummary', () => {
  it('Should render summary contains Returned Check Charges', () => {
    const wrapper = renderComponent({
      formValues: {
        returnedCheckCharges: true
      }
    } as WorkflowSetupSummaryProps);

    expect(wrapper.getByText('Returned Check Charges')).toBeInTheDocument;
  });

  it('Should click on edit', () => {
    const mockFn = jest.fn();
    const wrapper = renderComponent({
      formValues: {
        returnedCheckCharges: true
      },
      onEditStep: mockFn
    } as WorkflowSetupSummaryProps);

    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(mockFn).toHaveBeenCalled();
  });
});
