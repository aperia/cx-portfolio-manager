import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { default as stepRegistry } from '../_commons/WorkflowSetup/registries/stepRegistry';

const registerFunc = jest.spyOn(stepRegistry, 'registerStep');

describe('ManagePenaltyFeeWorkflow > registerSteps', () => {
  it('Should register components', async () => {
    await import('./registerSteps');

    expect(registerFunc).toBeCalledWith(
      'GetStartedManagePenaltyFee',
      expect.anything(),
      [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_PENALTY_FEES__GET_STARTED__EXISTED_WORKFLOW,
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_PENALTY_FEES__GET_STARTED__EXISTED_WORKFLOW__STUCK
      ]
    );

    expect(registerFunc).toBeCalledWith(
      'LateChargesManagePenaltyFee',
      expect.anything(),
      [UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_PENALTY_FEES__LC]
    );

    expect(registerFunc).toBeCalledWith(
      'ReturnedCheckChargesManagePenaltyFee',
      expect.anything(),
      [UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_PENALTY_FEES__RC]
    );
  });
});
