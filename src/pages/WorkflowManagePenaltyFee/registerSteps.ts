import { UNSAVED_CHANGE_NAMES } from 'app/constants/unsave-changes-form-names';
import { stepRegistry } from '../_commons/WorkflowSetup/registries';
import GettingStartStep from './GettingStartStep';
import LateChargesStep from './LateChargesStep';
import ReturnedCheckChargesStep from './ReturnedCheckChargesStep';

stepRegistry.registerStep('GetStartedManagePenaltyFee', GettingStartStep, [
  UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_PENALTY_FEES__GET_STARTED__EXISTED_WORKFLOW,
  UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_PENALTY_FEES__GET_STARTED__EXISTED_WORKFLOW__STUCK
]);
stepRegistry.registerStep('LateChargesManagePenaltyFee', LateChargesStep, [
  UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_PENALTY_FEES__LC
]);
stepRegistry.registerStep(
  'ReturnedCheckChargesManagePenaltyFee',
  ReturnedCheckChargesStep,
  [UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_PENALTY_FEES__RC]
);
