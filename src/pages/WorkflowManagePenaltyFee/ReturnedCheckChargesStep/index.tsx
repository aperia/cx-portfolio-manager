import EnhanceDropdownButton from 'app/components/EnhanceDropdownButton';
import { ADD_METHOD_BUTTONS } from 'app/constants/constants';
import {
  AddMethodButtons,
  BadgeColorType,
  MethodFieldParameterEnum,
  ServiceSubjectSection
} from 'app/constants/enums';
import {
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { mapGridExpandCollapse } from 'app/helpers';
import { useUnsavedChangeRegistry } from 'app/hooks';
import {
  Button,
  ColumnType,
  DropdownButton,
  DropdownButtonSelectEvent,
  Grid,
  Icon,
  InlineMessage,
  Tooltip,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty, orderBy } from 'lodash';
import {
  actionsWorkflowSetup,
  useSelectElementMetadataForRC,
  useSelectElementMetadataStatus
} from 'pages/_commons/redux/WorkflowSetup';
import {
  formatBadge,
  formatText,
  formatTruncate
} from 'pages/_commons/Utils/formatGridField';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, {
  Fragment,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';
import AddNewMethodModal from './AddNewMethodModal';
import MethodListModal from './MethodListModal';
import parseFormValues from './parseFormValues';
import RemoveMethodModal from './RemoveMethodModal';
import ReturnedCheckChargesSummary from './ReturnedCheckChargesSummary';
import SubRowGrid from './SubRowGrid';

export interface ReturnedCheckChargesFormValues {
  isValid?: boolean;

  methods?: (WorkflowSetupMethod & { rowId?: number })[];
}
const ReturnedCheckChargesStep: React.FC<
  WorkflowSetupProps<ReturnedCheckChargesFormValues>
> = ({ stepId, selectedStep, savedAt, formValues, isStuck, setFormValues }) => {
  const { t } = useTranslation();

  const { loading } = useSelectElementMetadataStatus();
  const metadata = useSelectElementMetadataForRC();

  const dispatch = useDispatch();

  const [isCreateNewVersion, setIsCreateNewVersion] = useState(false);
  const [isMethodToModel, setIsMethodToModel] = useState(false);
  const [expandedList, setExpandedList] = useState<string[]>([]);
  const methods = useMemo(
    () => orderBy(formValues?.methods || [], ['name'], ['asc']),
    [formValues?.methods]
  );
  const methodNames = useMemo(() => methods.map(i => i.name), [methods]);

  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;

  const [showAddNewMethodModal, setShowAddNewMethodModal] = useState(false);
  const [removeMethodRowId, setRemoveMethodRowId] =
    useState<undefined | number>(undefined);
  const [editMethodRowId, setEditMethodRowId] =
    useState<undefined | number>(undefined);
  const [editMethodBackId, setEditMethodBackId] =
    useState<undefined | number>(undefined);
  const [hasChange, setHasChange] = useState(false);
  const [keepCreateMethodState, setKeepCreateMethodState] =
    useState<any | undefined>(undefined);

  const onRemoveMethod = useCallback((idx: number) => {
    setRemoveMethodRowId(idx);
  }, []);

  const onEditMethod = useCallback((idx: number) => {
    setEditMethodRowId(idx);
  }, []);

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName: UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_PENALTY_FEES__RC,
      priority: 1
    },
    [hasChange]
  );

  const methodTypeToText = useCallback(
    (type: WorkflowSetupMethodType) => {
      switch (type) {
        case 'NEWVERSION':
          return t('txt_manage_penalty_fee_version_created');
        case 'MODELEDMETHOD':
          return t('txt_manage_penalty_fee_method_modeled');
      }
      return t('txt_manage_penalty_fee_method_created');
    },
    [t]
  );

  const columns: ColumnType[] = useMemo(
    () => [
      {
        id: 'methodName',
        Header: t('txt_method_name'),
        accessor: formatText(['name']),
        width: 105
      },
      {
        id: 'modelOrCreateFrom',
        Header: t('txt_manage_penalty_fee_modeled_or_create_from'),
        accessor: formatText(['modeledFrom', 'name']),
        width: 120
      },
      {
        id: 'comment',
        Header: t('txt_comment'),
        accessor: formatTruncate(['comment']),
        width: 312
      },
      {
        id: 'methodType',
        Header: t('txt_manage_penalty_fee_action_taken'),
        accessor: (data, idx) =>
          formatBadge(['methodType'], {
            colorType: BadgeColorType.MethodType,
            noBorder: true
          })(
            {
              ...data,
              methodType: methodTypeToText(data.methodType)
            },
            idx
          ),
        width: 153
      },
      {
        id: 'actions',
        Header: t('txt_actions'),
        accessor: (data: any) => (
          <div className="d-flex justify-content-center">
            <Button
              size="sm"
              variant="outline-primary"
              onClick={() => onEditMethod(data.rowId)}
            >
              {t('txt_edit')}
            </Button>
            <Button
              size="sm"
              variant="outline-danger"
              className="ml-8"
              onClick={() => onRemoveMethod(data.rowId)}
            >
              {t('txt_delete')}
            </Button>
          </div>
        ),
        cellProps: { className: 'text-center py-8' },
        width: 142
      }
    ],
    [t, onRemoveMethod, onEditMethod, methodTypeToText]
  );

  useEffect(() => {
    const isValid = (formValues?.methods?.length || 0) > 0;
    if (formValues.isValid === isValid) return;

    keepRef.current.setFormValues({ isValid });
  }, [methods, formValues]);

  useEffect(() => {
    dispatch(
      actionsWorkflowSetup.getWorkflowMetadata([
        MethodFieldParameterEnum.ReturnedCheckChargesOption,
        MethodFieldParameterEnum.MinimumPayDueOptions,
        MethodFieldParameterEnum.OneFeeParticipationCode,
        MethodFieldParameterEnum.PostingTextID,
        MethodFieldParameterEnum.ReversalTextID,
        MethodFieldParameterEnum.FirstYrMaxManagement,
        MethodFieldParameterEnum.OneFeePriorityCode,

        MethodFieldParameterEnum.LateChargesOption,
        MethodFieldParameterEnum.CalculationBase,
        MethodFieldParameterEnum.AssessmentControl,
        MethodFieldParameterEnum.BalanceIndicator,
        MethodFieldParameterEnum.AssessedAccounts,
        MethodFieldParameterEnum.CurrentBalanceAssessment,
        MethodFieldParameterEnum.CalculationDayControl,
        MethodFieldParameterEnum.NonProcessingLateCharge,
        MethodFieldParameterEnum.IncludeExcludeControl,
        MethodFieldParameterEnum.Status1,
        MethodFieldParameterEnum.LateChargeResetCounter
      ])
    );
  }, [dispatch]);

  useEffect(() => {
    if (stepId !== selectedStep) {
      setExpandedList([]);
    }
    setHasChange(false);
  }, [stepId, selectedStep, savedAt]);

  const onClickAddNewMethod = () => {
    setShowAddNewMethodModal(true);
  };
  const onClickMethodToModel = () => {
    setIsMethodToModel(true);
  };
  const onClickCreateNewVersion = () => {
    setIsCreateNewVersion(true);
  };
  const closeMethodListModalModal = () => {
    setIsCreateNewVersion(false);
    setIsMethodToModel(false);
  };

  const onSelectAddMethod = useCallback((event: DropdownButtonSelectEvent) => {
    const { value } = event.target.value;
    switch (value) {
      case AddMethodButtons.CREATE_NEW_VERSION:
        onClickCreateNewVersion();
        break;
      case AddMethodButtons.ADD_NEW_METHOD:
        onClickAddNewMethod();
        break;
      case AddMethodButtons.CHOOSE_METHOD_TO_MODEL:
        onClickMethodToModel();
        break;
    }
  }, []);

  const renderNoMethod = useMemo(() => {
    return (
      <Fragment>
        <h5>{t('txt_manage_penalty_fee_select_an_option')}</h5>
        <div className="row mt-16">
          <div className="col-xl col-6">
            <div
              className="rcc-btn d-flex justify-content-between border rounded-lg py-10"
              onClick={onClickCreateNewVersion}
            >
              <span className="d-flex align-items-center ml-16">
                <Icon
                  name="method-create"
                  size="9x"
                  className="color-grey-l16"
                />
                <span className="ml-12 mr-8">
                  {t('txt_manage_penalty_fee_create_new_version')}
                </span>
                <Tooltip
                  element={t('txt_manage_penalty_fee_create_new_version_desc')}
                  triggerClassName="d-flex"
                >
                  <Icon
                    name="information"
                    className="color-grey-l16"
                    size="5x"
                  />
                </Tooltip>
              </span>
            </div>
          </div>
          <div className="col-xl col-6">
            <div
              className="rcc-btn d-flex justify-content-between border rounded-lg py-10"
              onClick={onClickMethodToModel}
            >
              <span className="d-flex align-items-center ml-16">
                <Icon
                  name="method-clone"
                  size="9x"
                  className="color-grey-l16"
                />
                <span className="ml-12 mr-8">
                  {t('txt_manage_penalty_fee_choose_method_to_model')}
                </span>
                <Tooltip
                  element={t(
                    'txt_manage_penalty_fee_choose_method_to_model_desc'
                  )}
                  triggerClassName="d-flex"
                >
                  <Icon
                    name="information"
                    className="color-grey-l16"
                    size="5x"
                  />
                </Tooltip>
              </span>
            </div>
          </div>
          <div className="col-xl col-6 mt-xl-0 mt-16">
            <div
              className="rcc-btn d-flex justify-content-between border rounded-lg py-10"
              onClick={onClickAddNewMethod}
            >
              <span className="d-flex align-items-center ml-16">
                <Icon name="method-add" size="9x" className="color-grey-l16" />
                <span className="ml-12 mr-8">
                  {t('txt_manage_penalty_fee_create_new_method')}
                </span>
                <Tooltip
                  element={t('txt_manage_penalty_fee_create_new_method_desc')}
                  triggerClassName="d-flex"
                >
                  <Icon
                    name="information"
                    className="color-grey-l16"
                    size="5x"
                  />
                </Tooltip>
              </span>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }, [t]);

  const dropdownListItem = useMemo(() => {
    return ADD_METHOD_BUTTONS.map(item => (
      <DropdownButton.Item
        key={item.value}
        label={t(item.description)}
        value={item}
      />
    ));
  }, [t]);

  const handleOnExpand = (dataKeyList: string[]) => {
    setExpandedList(
      isEmpty(dataKeyList) ? [] : [dataKeyList[dataKeyList.length - 1]]
    );
  };

  const renderRCCMethod = useMemo(() => {
    return (
      <div>
        <div className="d-flex align-items-center">
          <h5>
            {t(
              'txt_manage_penalty_fee_returned_check_charges_method_list_title'
            )}
          </h5>
          <div className="d-flex ml-auto mr-n8">
            <EnhanceDropdownButton
              buttonProps={{
                size: 'sm',
                children: t('txt_manage_penalty_fee_add_method'),
                variant: 'outline-primary'
              }}
              onSelect={onSelectAddMethod}
            >
              {dropdownListItem}
            </EnhanceDropdownButton>
          </div>
        </div>
        <div className="pt-16 ">
          <Grid
            columns={columns}
            data={methods}
            subRow={({ original }: any) => (
              <SubRowGrid original={original} metadata={metadata} />
            )}
            dataItemKey="rowId"
            togglable
            toggleButtonConfigList={methods.map(
              mapGridExpandCollapse('rowId', t)
            )}
            expandedItemKey="rowId"
            expandedList={expandedList}
            onExpand={handleOnExpand}
            scrollable
          />
        </div>
      </div>
    );
  }, [
    dropdownListItem,
    columns,
    onSelectAddMethod,
    methods,
    metadata,
    expandedList,
    t
  ]);

  return (
    <div className={`${loading && 'loading'}`}>
      <div className="mt-8 pb-24 border-bottom color-grey">
        <p>{t('txt_manage_penalty_fee_returned_check_charges_desc_1')}</p>
      </div>
      {isStuck && (
        <InlineMessage className="mb-0 mt-24" variant="danger" withIcon>
          {t('txt_step_stuck_move_forward_message')}
        </InlineMessage>
      )}
      <div className="mt-24">
        {isEmpty(methods) ? renderNoMethod : renderRCCMethod}
      </div>
      {(isCreateNewVersion || isMethodToModel) && (
        <MethodListModal
          id="addMethodFromModel"
          show
          methodNames={methodNames}
          isCreateNewVersion={isCreateNewVersion}
          onClose={newMethod => {
            if (newMethod) {
              const rowId = Date.now();
              setFormValues({
                methods: [
                  {
                    ...newMethod,
                    rowId,
                    serviceSubjectSection: ServiceSubjectSection.RC
                  },
                  ...methods
                ]
              });
              setExpandedList([rowId] as any);
              setHasChange(true);
            }
            closeMethodListModalModal();
          }}
        />
      )}
      {showAddNewMethodModal && (
        <AddNewMethodModal
          id="addNewMethod"
          show
          methodNames={methodNames}
          onClose={method => {
            if (method) {
              const rowId = Date.now();
              setFormValues({
                methods: [
                  {
                    ...method,
                    rowId,
                    serviceSubjectSection: ServiceSubjectSection.RC
                  },
                  ...methods
                ]
              });
              setExpandedList([rowId] as any);
              setHasChange(true);
            }
            setShowAddNewMethodModal(false);
          }}
        />
      )}
      {editMethodRowId !== undefined && (
        <AddNewMethodModal
          id="addNewMethod"
          show
          method={methods.find(i => i.rowId === editMethodRowId)}
          methodNames={methodNames}
          onClose={(method, isBack, keepState) => {
            if (method) {
              method.serviceSubjectSection = ServiceSubjectSection.RC;
              const newMethds = methods.map(item => {
                if (item.rowId === editMethodRowId) return method;
                return item;
              });
              setFormValues({ methods: newMethds });
              setHasChange(true);

              if (isBack) {
                setEditMethodBackId(editMethodRowId);
                setKeepCreateMethodState(keepState);
              }
            }
            setEditMethodRowId(undefined);
          }}
        />
      )}
      {editMethodBackId && (
        <MethodListModal
          id="editMethodFromModel"
          show
          isCreateNewVersion={
            methods.find(i => i.rowId === editMethodBackId)!.methodType ===
            'NEWVERSION'
          }
          methodNames={methodNames}
          defaultMethod={methods.find(i => i.rowId === editMethodBackId)}
          keepState={keepCreateMethodState}
          onClose={method => {
            if (method) {
              method.serviceSubjectSection = ServiceSubjectSection.RC;
              const newMethds = methods.map(item => {
                if (item.rowId === editMethodRowId) return method;
                return item;
              });
              setFormValues({ methods: newMethds });
              setHasChange(true);
            }
            setEditMethodBackId(undefined);
            setKeepCreateMethodState(undefined);
          }}
        />
      )}
      {removeMethodRowId !== undefined && (
        <RemoveMethodModal
          id="removeMethod"
          show
          methodName={methods.find(i => i.rowId === removeMethodRowId)?.name}
          onClose={method => {
            if (method) {
              setFormValues({
                methods: methods.filter(i => i.rowId !== removeMethodRowId)
              });
              setHasChange(true);
            }
            setRemoveMethodRowId(undefined);
          }}
        />
      )}
    </div>
  );
};
const ExtraStaticReturnedCheckChargesStep =
  ReturnedCheckChargesStep as WorkflowSetupStaticProp<ReturnedCheckChargesFormValues>;

ExtraStaticReturnedCheckChargesStep.summaryComponent =
  ReturnedCheckChargesSummary;
ExtraStaticReturnedCheckChargesStep.defaultValues = {
  isValid: false,
  methods: []
};
ExtraStaticReturnedCheckChargesStep.parseFormValues = parseFormValues;

export default ExtraStaticReturnedCheckChargesStep;
