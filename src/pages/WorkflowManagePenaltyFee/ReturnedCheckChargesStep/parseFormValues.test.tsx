import { ServiceSubjectSection } from 'app/constants/enums';
import parseFormValues from './parseFormValues';

describe('ManagePenaltyFeeWorkflow > ReturnedCheckChargesStep > parseFormValues', () => {
  it('Should return undefined with methodList is not array', () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '1',
              status: 'DONE'
            }
          ],
          configurations: {}
        }
      } as WorkflowSetupInstance,
      id: '1'
    };

    const response = parseFormValues(props.data, props.id);
    expect(response).toEqual({
      isPass: true,
      isSelected: false,
      isValid: false,
      methods: []
    });
  });

  it('Should return undefined with not exist step', () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '2',
              status: 'DONE'
            }
          ],
          configurations: {
            methodList: [{}]
          }
        }
      } as WorkflowSetupInstance,
      id: '1'
    };

    const response = parseFormValues(props.data, props.id);
    expect(response).toEqual(undefined);
  });

  it('Should return undefined with methodList not any ServiceSubjectSection is RC', () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '1',
              status: 'DONE'
            }
          ],
          configurations: {
            methodList: [{}]
          }
        }
      } as WorkflowSetupInstance,
      id: '1'
    };

    const response = parseFormValues(props.data, props.id);
    expect(response).toEqual({
      isPass: true,
      isSelected: false,
      isValid: false,
      methods: []
    });
  });

  it('Should return data is not undefined', () => {
    const props = {
      data: {
        data: {
          workflowSetupData: [
            {
              id: '1',
              status: 'DONE'
            }
          ],
          configurations: {
            methodList: [
              {
                id: '1',
                name: 'name 1',
                serviceSubjectSection: ServiceSubjectSection.RC
              }
            ]
          }
        }
      } as WorkflowSetupInstance,
      id: '1'
    };

    const response = parseFormValues(props.data, props.id);
    expect(response).not.toEqual(undefined);
  });
});
