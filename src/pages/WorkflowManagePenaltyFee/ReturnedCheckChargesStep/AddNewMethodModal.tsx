import ModalRegistry from 'app/components/ModalRegistry';
import NoDataFound from 'app/components/NoDataFound';
import SimpleSearch from 'app/components/SimpleSearch';
import TextAreaCountDown from 'app/components/TextAreaCountDown';
import { MethodFieldParameterEnum } from 'app/constants/enums';
import { METHOD_RC_DEFAULT, METHOD_RC_FIELDS } from 'app/constants/mapping';
import {
  unsavedChangesProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import {
  mapGridExpandCollapse,
  matchSearchValue,
  methodParamsToObject,
  objectToMethodParams
} from 'app/helpers';
import {
  useFormValidations,
  useUnsavedChangeRegistry,
  useUnsavedChangesRedirect
} from 'app/hooks';
import {
  Button,
  Grid,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  TextBox,
  TransDLS,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty, isEqual } from 'lodash';
import isFunction from 'lodash.isfunction';
import { useSelectElementMetadataForRC } from 'pages/_commons/redux/WorkflowSetup';
import ClearAndResetButton from 'pages/_commons/Utils/ClearAndResetButton';
import React, { useEffect, useMemo, useRef, useState } from 'react';
import newMethodColumns from './newMethodColumns';
import {
  parameterGroup,
  parameterList,
  ParameterListIds
} from './newMethodParameterList';
import SubGridNewMethod from './SubGridNewMethod';

interface IKeepState {
  searchValue: string;
  expandedList: ParameterListIds[];
}
interface IProps {
  id: string;
  show: boolean;
  method?: WorkflowSetupMethod & { rowId?: number };
  draftMethod?: WorkflowSetupMethod & { rowId?: number };
  keepState?: IKeepState;
  methodNames?: string[];
  onClose?: (
    method?: WorkflowSetupMethod,
    isBack?: boolean,
    keepState?: IKeepState
  ) => void;
}
const AddNewMethodModal: React.FC<IProps> = ({
  id,
  show,
  method: methodProp = { methodType: 'NEWMETHOD' } as WorkflowSetupMethod,
  draftMethod,
  methodNames = [],
  keepState,
  onClose
}) => {
  const metadata = useSelectElementMetadataForRC();

  const { t } = useTranslation();
  const redirect = useUnsavedChangesRedirect();

  const [initialMethod, setInitialMethod] = useState<
    WorkflowSetupMethodObjectParams & { rowId?: number }
  >(methodParamsToObject(methodProp));
  const [method, setMethod] = useState(
    draftMethod ? methodParamsToObject(draftMethod) : initialMethod
  );

  const [searchValue, setSearchValue] = useState(keepState?.searchValue || '');
  const [expandedList, setExpandedList] = useState<ParameterListIds[]>(
    keepState?.expandedList || ['Info_AssessmentCalculation']
  );

  const isNewVersion = useMemo(
    () => method.methodType === 'NEWVERSION',
    [method.methodType]
  );
  const isNewMethod = useMemo(
    () => method.methodType === 'NEWMETHOD',
    [method.methodType]
  );

  const parameters = useMemo(
    () =>
      parameterList.filter(p =>
        parameterGroup[p.id].some(g =>
          matchSearchValue(
            [g.fieldName, g.moreInfoText, g.onlinePCF],
            searchValue
          )
        )
      ),
    [searchValue]
  );

  useUnsavedChangeRegistry(
    {
      ...unsavedChangesProps,
      formName:
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_PENALTY_FEES__RC_METHOD_DETAILS,
      priority: 1
    },
    [
      !isEqual(initialMethod.name?.trim() || '', method.name?.trim() || ''),
      !isEqual(
        initialMethod.comment?.trim() || '',
        method.comment?.trim() || ''
      ),
      !isEqual(
        initialMethod?.[MethodFieldParameterEnum.ComputerLetter]?.trim() ||
          '0000',
        method?.[MethodFieldParameterEnum.ComputerLetter]?.trim() || '0000'
      ),
      METHOD_RC_FIELDS.some(f => {
        if (f === MethodFieldParameterEnum.ComputerLetter) {
          return false;
        }
        return !isEqual(
          initialMethod[f]?.trim() || '',
          method[f]?.trim() || ''
        );
      })
    ]
  );

  const currentErrors = useMemo(
    () =>
      ({
        name:
          (!method?.name?.trim() && {
            status: true,
            message: t('txt_required_validation', {
              fieldName: t('txt_method_name')
            })
          }) ||
          (initialMethod.name?.trim() !== method?.name?.trim() &&
            methodNames.includes(method?.name?.trim()) && {
              status: true,
              message: t(
                'txt_manage_penalty_fee_validation_method_name_must_be_unique'
              )
            }) ||
          (!isNewMethod &&
            !isNewVersion &&
            method?.name?.trim() === method.modeledFrom?.name && {
              status: true,
              message: t(
                'txt_manage_penalty_fee_validation_method_name_must_be_unique'
              )
            }),
        [MethodFieldParameterEnum.ComputerLetter]: method?.[
          MethodFieldParameterEnum.ComputerLetter
        ]?.includes('_') && {
          status: true,
          message: t(
            'txt_manage_penalty_fee_validation_invalid_computer_letter'
          )
        }
      } as Record<keyof WorkflowSetupMethodObjectParams, IFormError>),
    [method, t, methodNames, initialMethod, isNewMethod, isNewVersion]
  );
  const [touches, errors, setTouched] =
    useFormValidations<keyof WorkflowSetupMethodObjectParams>(currentErrors);

  const isValid = useMemo(
    () =>
      !Object.keys(errors).some(
        k => errors[k as keyof WorkflowSetupMethodObjectParams]
      ) &&
      (!currentErrors.name?.status || touches.name?.touched),
    [errors, touches, currentErrors]
  );

  useEffect(() => {
    let defaultParamObj: Record<string, string> = {};
    if (
      initialMethod.methodType === 'NEWMETHOD' &&
      initialMethod.rowId === undefined
    ) {
      const paramMapping = Object.keys(METHOD_RC_DEFAULT).reduce(
        (pre, next) => {
          pre[next] = METHOD_RC_DEFAULT[next];
          return pre;
        },
        {} as Record<string, string>
      );
      defaultParamObj = Object.assign({}, paramMapping);
      defaultParamObj.methodType = 'NEWMETHOD';
      setMethod(defaultParamObj as any);
      setInitialMethod(defaultParamObj as any);
    }
  }, [initialMethod.methodType, initialMethod.rowId]);

  const simpleSearchRef = useRef<any>(null);
  useEffect(() => {
    if (!searchValue && simpleSearchRef?.current) {
      simpleSearchRef.current.clear();
    }
  }, [searchValue]);

  const handleBack = () => {
    const methodForm = objectToMethodParams(METHOD_RC_FIELDS, method);
    methodForm.versionParameters = !!methodForm.versionParameters
      ? methodForm.versionParameters
      : methodForm.parameters;
    isFunction(onClose) &&
      onClose(methodForm, true, { searchValue, expandedList });
  };

  const handleClose = () => {
    redirect({
      onConfirm: () => isFunction(onClose) && onClose(),
      formsWatcher: [
        UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_PENALTY_FEES__RC_METHOD_DETAILS
      ]
    });
  };

  const handleAddMethod = () => {
    const methodForm = objectToMethodParams(METHOD_RC_FIELDS, method);
    methodForm.versionParameters = !!methodForm.versionParameters
      ? methodForm.versionParameters
      : methodForm.parameters;

    isFunction(onClose) && onClose(methodForm);
  };

  const handleFormChange =
    (field: keyof WorkflowSetupMethodObjectParams, isRefData?: boolean) =>
    (e: any) => {
      let value: string = isRefData ? e.target?.value?.code : e.target?.value;
      value = field === 'name' ? value?.toUpperCase() : value;

      setMethod(method => ({ ...method, [field]: value }));
    };

  const handleSearch = (val: string) => {
    setSearchValue(val);
    setExpandedList(parameters.map(p => p.id));
  };

  const handleClearFilter = () => {
    setSearchValue('');
    setExpandedList(['Info_AssessmentCalculation']);
  };

  return (
    <ModalRegistry
      lg
      id={id}
      show={show}
      onAutoClosedAll={handleClose}
      onAutoClosed={handleClearFilter}
    >
      <ModalHeader border closeButton onHide={handleClose}>
        <ModalTitle>{t('txt_manage_penalty_fee_method_details')}</ModalTitle>
      </ModalHeader>
      <ModalBody>
        <div className="color-grey">
          {isNewMethod && (
            <p>
              {t(
                'txt_manage_penalty_fee_returned_check_charges_new_method_desc'
              )}
            </p>
          )}
          {isNewVersion && (
            <p>
              {t(
                'txt_manage_penalty_fee_returned_check_charges_new_version_desc'
              )}
            </p>
          )}
          {!isNewMethod && !isNewVersion && (
            <p>
              {t(
                'txt_manage_penalty_fee_returned_check_charges_model_from_desc'
              )}
            </p>
          )}
          <p className="pt-8">
            <TransDLS keyTranslation="txt_manage_penalty_fee_returned_check_charges_new_method_desc_save">
              <strong className="color-grey-d20" />
            </TransDLS>
          </p>
        </div>

        <div>
          <div className="row mt-16">
            {!isNewMethod && (
              <div className="col-6 col-lg-4">
                <TextBox
                  id="addNewMethod__selectedMethod"
                  readOnly
                  value={method?.modeledFrom?.name || ''}
                  maxLength={8}
                  label={t('Selected Method')}
                />
              </div>
            )}
            <div className="col-6 col-lg-4 mr-lg-4">
              <TextBox
                id="addNewMethod__methodName"
                readOnly={isNewVersion}
                value={method?.name || ''}
                onChange={handleFormChange('name')}
                required
                maxLength={8}
                label={t('txt_method_name')}
                onFocus={setTouched('name')}
                onBlur={setTouched('name', true)}
                error={errors.name}
              />
            </div>
            <div className="mt-16 col-12 col-lg-8">
              <TextAreaCountDown
                id="addNewMethod__comment"
                label={t('txt_comment_area')}
                value={method?.comment || ''}
                onChange={handleFormChange('comment')}
                rows={4}
                maxLength={240}
              />
            </div>
          </div>
          <div className="mt-24">
            <div className="d-flex align-items-center justify-content-between mb-16">
              <h5>{t('txt_parameter_list')}</h5>
              <SimpleSearch
                ref={simpleSearchRef}
                placeholder={t('txt_type_to_search')}
                defaultValue={searchValue}
                onSearch={handleSearch}
                popperElement={
                  <>
                    <p className="color-grey">{t('txt_search_description')}</p>
                    <ul className="search-field-item list-unstyled">
                      <li className="mt-16">{t('txt_business_name')}</li>
                      <li className="mt-16">{t('txt_more_info')}</li>
                      <li className="mt-16">
                        {t('txt_manage_penalty_fee_online_PCF')}
                      </li>
                    </ul>
                  </>
                }
              />
            </div>
            {searchValue && !isEmpty(parameters) && (
              <div className="d-flex justify-content-end mb-16 mr-n8">
                <ClearAndResetButton
                  small
                  onClearAndReset={handleClearFilter}
                />
              </div>
            )}
            {isEmpty(parameters) && (
              <div className="d-flex flex-column justify-content-center mt-40">
                <NoDataFound
                  id="newMethod_notfound"
                  hasSearch
                  title={t('txt_no_results_found')}
                  linkTitle={t('txt_clear_and_reset')}
                  onLinkClicked={handleClearFilter}
                />
              </div>
            )}
            {!isEmpty(parameters) && (
              <Grid
                columns={newMethodColumns(t)}
                data={parameters}
                subRow={({ original }: MagicKeyValue) => (
                  <SubGridNewMethod
                    original={original}
                    method={method}
                    metadata={metadata}
                    searchValue={searchValue}
                    onChange={handleFormChange}
                    errors={errors}
                    setTouched={setTouched}
                  />
                )}
                dataItemKey="id"
                togglable
                expandedItemKey="id"
                expandedList={expandedList}
                onExpand={setExpandedList as any}
                toggleButtonConfigList={parameters.map(
                  mapGridExpandCollapse('id', t)
                )}
              />
            )}
          </div>
        </div>
      </ModalBody>
      <ModalFooter
        cancelButtonText={t('txt_cancel')}
        okButtonText={t('txt_save')}
        onCancel={handleClose}
        onOk={handleAddMethod}
        disabledOk={!isValid}
      >
        {!isNewMethod && (
          <Button
            className="mr-auto ml-n8"
            variant="outline-primary"
            onClick={handleBack}
          >
            {t('txt_back')}
          </Button>
        )}
      </ModalFooter>
    </ModalRegistry>
  );
};

export default AddNewMethodModal;
