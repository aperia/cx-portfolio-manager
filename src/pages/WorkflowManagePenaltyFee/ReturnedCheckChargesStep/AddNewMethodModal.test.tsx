import userEvent from '@testing-library/user-event';
import { WorkflowMetadataList } from 'app/fixtures/workflow-metadata';
import { renderWithMockStore } from 'app/utils';
import 'app/utils/_mockComponent/mockModalRegistry';
import 'app/utils/_mockComponent/mockUseTranslation';
import mockDevice from 'app/utils/_mockHelperConstant/mockDevice';
import React from 'react';
import AddNewMethodModal from './AddNewMethodModal';

describe('ManagePenaltyFeeWorkflow > ReturnedCheckChargesStep > AddNewMethodModal', () => {
  const onClose = jest.fn();
  const defaultProps = {
    id: 'AddNewMethodModal',
    show: true,
    onClose
  };

  const method: WorkflowSetupMethod = {
    id: 'id',
    name: '',
    comment: 'comment',
    methodType: 'NEWVERSION',
    modeledFrom: {
      id: 'id',
      name: 'MTHMTCI',
      versions: [
        {
          id: 'id version'
        }
      ]
    },
    parameters: [
      {
        name: 'minimum.pay.due.option',
        previousValue: '3',
        newValue: '2'
      },
      {
        name: 'amount.assessed',
        previousValue: '104',
        newValue: '123'
      },
      {
        name: 'returned.check.charge.option',
        previousValue: '0',
        newValue: '2'
      },
      {
        name: 'one.fee.participation.code',
        previousValue: '0',
        newValue: '0'
      }
    ]
  };

  const getInitialState = ({ elements = WorkflowMetadataList }: any = {}) => ({
    workflowSetup: { elementMetadata: { elements } }
  });

  const queryInputFromLabel = (tierLabel: HTMLElement) => {
    return tierLabel
      .closest(
        '.dls-input-container, .dls-numreric-container, .text-field-container'
      )
      ?.querySelector('input, .input, textarea') as HTMLInputElement;
  };

  beforeEach(() => {
    mockDevice.isDevice = false;
  });

  afterEach(() => {
    onClose.mockClear();
  });

  it('should render AddNewMethodModal', async () => {
    mockDevice.isDevice = true;
    const wrapper = await renderWithMockStore(
      <AddNewMethodModal {...defaultProps} />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByText('txt_manage_penalty_fee_method_details')
    ).toBeInTheDocument();

    userEvent.click(wrapper.getByText('txt_cancel'));
    expect(onClose).toBeCalled();

    const description = queryInputFromLabel(
      wrapper.getByText('txt_comment_area')
    );
    userEvent.type(description, new Array(239).fill(0).join(''));
    expect(description.textContent?.length).toEqual(239);
  });

  it('should render AddNewMethodModal form new version', async () => {
    const wrapper = await renderWithMockStore(
      <AddNewMethodModal {...defaultProps} method={method} />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByText('txt_manage_penalty_fee_method_details')
    ).toBeInTheDocument();

    userEvent.click(wrapper.getByText('txt_back'));
    expect(onClose).toBeCalled();
  });

  it('should render AddNewMethodModal form modeled method', async () => {
    const wrapper = await renderWithMockStore(
      <AddNewMethodModal
        {...defaultProps}
        methodNames={['MTHMTCI1']}
        draftMethod={{
          ...method,
          modeledFrom: { ...method.modeledFrom, name: '' },
          methodType: 'MODELEDMETHOD',
          parameters: undefined as any
        }}
        method={{
          ...method,
          modeledFrom: { ...method.modeledFrom, name: '' },
          methodType: 'MODELEDMETHOD',
          parameters: undefined as any
        }}
      />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByText('txt_manage_penalty_fee_method_details')
    ).toBeInTheDocument();

    const expandedButtons = wrapper.container.querySelectorAll(
      'table tr td .btn.btn-icon-secondary.btn-sm .icon.icon-plus'
    );
    expandedButtons.forEach(element => userEvent.click(element));

    const methodName = queryInputFromLabel(
      wrapper.getByText('txt_method_name')
    );
    userEvent.clear(methodName);
    userEvent.type(methodName, 'MTHMTCI1');
    expect(methodName?.value).toEqual('MTHMTCI1');
    userEvent.click(document.body);
    userEvent.click(methodName);

    expect(
      wrapper.getByText(
        'txt_manage_penalty_fee_validation_method_name_must_be_unique'
      )
    ).toBeInTheDocument();

    const computerLetter = wrapper.getByTestId(
      'addNewMethod__computerLetter_dls-masked-text-box_input'
    ) as HTMLInputElement;

    userEvent.type(computerLetter, '1');
    expect(computerLetter?.value).toEqual('1___');
    userEvent.click(document.body);
    userEvent.click(computerLetter);

    expect(
      wrapper.getByText(
        'txt_manage_penalty_fee_validation_invalid_computer_letter'
      )
    ).toBeInTheDocument();

    userEvent.click(wrapper.getByText('txt_back'));
    expect(onClose).toBeCalled();
  });

  it('should save method form modeled method', async () => {
    const wrapper = await renderWithMockStore(
      <AddNewMethodModal
        methodNames={['MTHMTCI']}
        {...defaultProps}
        method={{ ...method, name: 'MTHMTCI', methodType: 'MODELEDMETHOD' }}
      />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByText('txt_manage_penalty_fee_method_details')
    ).toBeInTheDocument();

    const methodName = queryInputFromLabel(
      wrapper.getByText('txt_method_name')
    );
    userEvent.clear(methodName);
    userEvent.type(methodName, 'MTHMTCI1');
    expect(methodName.value).toEqual('MTHMTCI1');

    const minimumPayDueOptions = wrapper
      .getByTestId('addNewMethod__minimumPayDue_dls-dropdown-list')
      .querySelector('.input') as HTMLInputElement;
    userEvent.click(minimumPayDueOptions);
    userEvent.click(wrapper.getByText('1 - Use the current MPD amount.'));

    userEvent.click(wrapper.getByText('txt_save'));
    expect(onClose).toBeCalled();
  });

  it('should save new method', async () => {
    const wrapper = await renderWithMockStore(
      <AddNewMethodModal {...defaultProps} method={undefined} />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByText('txt_manage_penalty_fee_method_details')
    ).toBeInTheDocument();

    const methodName = queryInputFromLabel(
      wrapper.getByText('txt_method_name')
    );
    userEvent.type(methodName, 'MTHMTCI1');

    userEvent.click(wrapper.getByText('txt_save'));
    expect(onClose).toBeCalled();
  });

  it('should render no data filter', async () => {
    const wrapper = await renderWithMockStore(
      <AddNewMethodModal {...defaultProps} method={undefined} />,
      { initialState: getInitialState() }
    );
    expect(
      wrapper.getByText('txt_manage_penalty_fee_method_details')
    ).toBeInTheDocument();

    const searchBox = wrapper.getByPlaceholderText('txt_type_to_search');

    userEvent.type(searchBox, 'a');
    userEvent.click(
      searchBox.parentElement!.querySelector(
        '.icon-simple-search .icon.icon-search'
      )!
    );

    userEvent.type(searchBox, 'notfound');
    userEvent.click(
      searchBox.parentElement!.querySelector(
        '.icon-simple-search .icon.icon-search'
      )!
    );

    expect(
      wrapper.getByText('txt_no_results_found txt_adjust_your_search_criteria')
    ).toBeInTheDocument();

    userEvent.click(wrapper.getByText('txt_clear_and_reset'));
    expect(
      wrapper.queryByText(
        'txt_no_results_found txt_adjust_your_search_criteria'
      )
    ).not.toBeInTheDocument();
  });
});
