import { fireEvent, render, RenderResult } from '@testing-library/react';
import { mockUseModalRegistryFnc } from 'app/utils';
import React from 'react';
import RemoveMethodModal from './RemoveMethodModal';

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const mockUseModalRegistry = mockUseModalRegistryFnc();

const renderModal = (props: any): RenderResult => {
  return render(
    <div>
      <RemoveMethodModal {...props} />
    </div>
  );
};

describe('ManagePenaltyFeeWorkflow > ReturnedCheckChargesStep > RemoveMethodModal', () => {
  beforeEach(() => {
    mockUseModalRegistry.mockImplementation(() => [true, false]);
  });

  afterEach(() => {
    mockUseModalRegistry.mockClear();
  });

  it('should render modal', () => {
    const wrapper = renderModal({
      id: 'modal_id',
      methodName: 'method name',
      onClose: jest.fn()
    });

    expect(wrapper.getByText('Confirm Delete')).toBeInTheDocument();
  });

  it('should call onRemove function', () => {
    const props = {
      id: 'modal_id',
      methodName: 'method name',
      onClose: jest.fn()
    };
    const wrapper = renderModal(props);

    fireEvent.click(wrapper.getByText('Delete'));
    expect(props.onClose).toHaveBeenCalled();
  });

  it('should call handleCloseWithoutReload function', () => {
    const props = {
      id: 'modal_id',
      methodName: 'method name',
      onClose: jest.fn()
    };
    const wrapper = renderModal(props);

    fireEvent.click(wrapper.getByText('txt_cancel'));
    expect(props.onClose).toHaveBeenCalled();
  });
});
