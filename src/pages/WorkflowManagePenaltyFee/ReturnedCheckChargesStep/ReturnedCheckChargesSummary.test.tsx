import {
  fireEvent,
  queryByText,
  render,
  RenderResult
} from '@testing-library/react';
import { ServiceSubjectSection } from 'app/constants/enums';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManagePenaltyFee';
import React from 'react';
import ReturnedCheckChargesSummary from './ReturnedCheckChargesSummary';

jest.mock('./SubRowGrid', () => ({
  __esModule: true,
  default: () => <div>SubRowGrid_component</div>
}));

const MockSelectElementMetadataForRC = jest.spyOn(
  WorkflowSetup,
  'useSelectElementMetadataForRC'
);

jest.mock('app/_libraries/_dls', () => {
  const actualModule = jest.requireActual('app/_libraries/_dls');
  return {
    ...actualModule,
    useTranslation: () => ({
      t: (value: string) => {
        return value;
      }
    })
  };
});

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <ReturnedCheckChargesSummary {...props} />
    </div>
  );
};

describe('ManagePenaltyFeeWorkflow > ReturnedCheckChargesStep > ReturnedCheckChargesSummary', () => {
  beforeEach(() => {
    MockSelectElementMetadataForRC.mockReturnValue({} as any);
  });

  afterEach(() => {
    MockSelectElementMetadataForRC.mockReset();
  });

  it('Should render a grid with no content', () => {
    const props = {
      stepId: '',
      formValues: {}
    } as any;

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    expect(body?.children.length).toEqual(0);
  });

  it('Should render a grid with content', () => {
    const props = {
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.RC,
            methodType: 'NEWMETHOD'
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.RC,
            methodType: 'MODELEDMETHOD'
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.RC,
            methodType: 'NEWVERSION'
          }
        ]
      }
    } as any;

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    expect(body?.children.length).toEqual(3);
  });

  it('Should call edit function', () => {
    const mockFn = jest.fn();
    const props = {
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.RC,
            methodType: 'NEWMETHOD'
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.RC,
            methodType: 'MODELEDMETHOD'
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.RC,
            methodType: 'NEWVERSION'
          }
        ]
      },
      onEditStep: mockFn
    } as any;

    const wrapper = renderComponent(props);
    fireEvent.click(wrapper.getByText('txt_edit'));
    expect(mockFn).toHaveBeenCalled();
  });

  it('Should show sub row', () => {
    const mockFn = jest.fn();
    const props = {
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.RC,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.RC,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.RC,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ]
      },
      onEditStep: mockFn
    } as any;

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    const firstChild = body?.children[0].querySelector(
      'button[class="btn btn-icon-secondary btn-sm"]'
    );
    fireEvent.click(firstChild as Element);
    expect(
      queryByText(wrapper.container, /SubRowGrid_component/i)
    ).toBeInTheDocument();
    fireEvent.click(firstChild as Element);
    expect(
      queryByText(wrapper.container, /SubRowGrid_component/i)
    ).not.toBeInTheDocument();
  });
});
