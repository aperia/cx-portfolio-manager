import {
  fireEvent,
  queryByText,
  render,
  RenderResult
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { ServiceSubjectSection } from 'app/constants/enums';
import 'app/utils/_mockComponent/mockUseTranslation';
import * as WorkflowSetup from 'pages/_commons/redux/WorkflowSetup/select-hooks/workflowManagePenaltyFee';
import * as WorkflowCommon from 'pages/_commons/redux/WorkflowSetup/select-hooks/_common';
import React from 'react';
import * as reactRedux from 'react-redux';
import ReturnedCheckChargesStep from './index';

const MockSelectElementMetadataForRC = jest.spyOn(
  WorkflowSetup,
  'useSelectElementMetadataForRC'
);

const MockSelectElementMetadataStatus = jest.spyOn(
  WorkflowCommon,
  'useSelectElementMetadataStatus'
);

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');
const mockOnCloseWithoutMethod = jest.fn();

jest.mock('./MethodListModal', () => ({
  __esModule: true,
  default: ({ onClose }: any) => {
    const handleOnClose = () => {
      if (mockOnCloseWithoutMethod()) {
        onClose();
        return;
      }

      onClose({
        id: '1',
        name: 'method-1',
        versions: [
          {
            id: 'version 1.0'
          }
        ]
      });
    };
    return <div onClick={handleOnClose}>MethodListModal_component</div>;
  }
}));

jest.mock('./AddNewMethodModal', () => ({
  __esModule: true,
  default: ({ onClose }: any) => {
    const handleOnClose = () => {
      if (mockOnCloseWithoutMethod()) {
        onClose();
        return;
      }

      onClose(
        {
          id: '1',
          name: 'method-1',
          versions: [
            {
              id: 'version 1.0'
            }
          ]
        },
        true
      );
    };
    return <div onClick={handleOnClose}>AddNewMethodModal_component</div>;
  }
}));

jest.mock('./RemoveMethodModal', () => ({
  __esModule: true,
  default: ({ onClose }: any) => {
    const handleOnClose = () => {
      if (mockOnCloseWithoutMethod()) {
        onClose();
        return;
      }

      onClose({
        id: '1',
        name: 'method-1',
        versions: [
          {
            id: 'version 1.0'
          }
        ]
      });
    };
    return <div onClick={handleOnClose}>RemoveMethodModal_component</div>;
  }
}));

jest.mock('./SubRowGrid', () => ({
  __esModule: true,
  default: () => <div>SubRowGrid_component</div>
}));

const renderComponent = (props: any): RenderResult => {
  return render(
    <div>
      <ReturnedCheckChargesStep {...props} />
    </div>
  );
};

describe('ManagePenaltyFeeWorkflow > ReturnedCheckChargesStep > index', () => {
  const mockDispatch = jest.fn();
  beforeEach(() => {
    mockUseDispatch.mockImplementation(() => mockDispatch);
    MockSelectElementMetadataForRC.mockReturnValue({
      rccOptions: [
        {
          code: 'code',
          text: 'text'
        }
      ],
      minimumPayDueOptions: [
        {
          code: 'code',
          text: 'text'
        }
      ],
      oneFeeParticipationCodeOptions: [
        {
          code: 'code',
          text: 'text'
        }
      ],
      postingTextIDOptions: [
        {
          code: 'code',
          text: 'text'
        }
      ],
      reversalTextIDOptions: [
        {
          code: 'code',
          text: 'text'
        }
      ],
      firstYearMaxManagementOptions: [
        {
          code: 'code',
          text: 'text'
        }
      ],
      oneFeePriorityCodeOptions: [
        {
          code: 'code',
          text: 'text'
        }
      ]
    });

    mockOnCloseWithoutMethod.mockReturnValue(false);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Should render with loading class', () => {
    MockSelectElementMetadataStatus.mockReturnValue({
      loading: true
    });

    const props = {
      stepId: '1',
      selectedStep: 'selectedStep',
      formValues: {},
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    expect(
      wrapper.baseElement.querySelector('div[class="loading"]')
    ).toBeInTheDocument();
  });

  it('Should render with no method > Create new version', () => {
    MockSelectElementMetadataStatus.mockReturnValue({
      loading: false
    });

    const props = {
      stepId: '1',
      selectedStep: 'selectedStep',
      formValues: {},
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_select_an_option')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    );
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('MethodListModal_component'));
    expect(
      queryByText(wrapper.container, /MethodListModal_component/i)
    ).not.toBeInTheDocument();

    mockOnCloseWithoutMethod.mockReturnValue(true);
    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    );
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('MethodListModal_component'));
    expect(
      queryByText(wrapper.container, /MethodListModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with no method > Choose Method to Model', () => {
    MockSelectElementMetadataStatus.mockReturnValue({
      loading: false
    });

    const props = {
      stepId: '1',
      selectedStep: 'selectedStep',
      formValues: {},
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_select_an_option')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_choose_method_to_model')
    );
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('MethodListModal_component'));
    expect(
      queryByText(wrapper.container, /MethodListModal_component/i)
    ).not.toBeInTheDocument();

    mockOnCloseWithoutMethod.mockReturnValue(true);
    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_choose_method_to_model')
    );
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('MethodListModal_component'));
    expect(
      queryByText(wrapper.container, /MethodListModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with no method > Add New Method', () => {
    MockSelectElementMetadataStatus.mockReturnValue({
      loading: false
    });

    const props = {
      stepId: '1',
      selectedStep: 'selectedStep',
      formValues: {},
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_select_an_option')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_method')
    );
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();

    mockOnCloseWithoutMethod.mockReturnValue(true);
    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_method')
    );
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with RCC Method > Edit method', () => {
    MockSelectElementMetadataStatus.mockReturnValue({
      loading: false
    });

    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.RC,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.RC,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.RC,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    expect(body?.children.length).toEqual(3);

    //SHOW Add new method modal
    const editButton = body?.children[1].querySelector(
      'button[class="btn btn-outline-primary btn-sm"]'
    ) as Element;
    fireEvent.click(editButton);
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
    //End

    mockOnCloseWithoutMethod.mockReturnValue(true);
    fireEvent.click(editButton);
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with RCC Method > Add new method', () => {
    MockSelectElementMetadataStatus.mockReturnValue({
      loading: false
    });

    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.RC,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.RC,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.RC,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );

    //SHOW AddNewMethodModal
    const editButton = body?.children[0].querySelector(
      'button[class="btn btn-outline-primary btn-sm"]'
    ) as Element;
    fireEvent.click(editButton);
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
    //End

    mockOnCloseWithoutMethod.mockReturnValue(true);
    fireEvent.click(editButton);
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with RCC Method > Remove method', () => {
    MockSelectElementMetadataStatus.mockReturnValue({
      loading: false
    });

    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.RC,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.RC,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.RC,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );

    //SHOW RemoveMethodModal
    const removeButton = body?.children[0].querySelector(
      'button[class="btn btn-outline-danger btn-sm ml-8"]'
    ) as Element;

    fireEvent.click(removeButton);
    expect(
      wrapper.getByText('RemoveMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('RemoveMethodModal_component'));
    expect(
      queryByText(wrapper.container, /RemoveMethodModal_component/i)
    ).not.toBeInTheDocument();
    //End

    mockOnCloseWithoutMethod.mockReturnValue(true);
    fireEvent.click(removeButton);
    expect(
      wrapper.getByText('RemoveMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('RemoveMethodModal_component'));
    expect(
      queryByText(wrapper.container, /RemoveMethodModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with RCC Method > click Add New Method on dropdown button', () => {
    MockSelectElementMetadataStatus.mockReturnValue({
      loading: false
    });

    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.RC,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.RC,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.RC,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const dropdownButton = wrapper.container.querySelector(
      'button.dls-dropdown-button'
    ) as Element;
    userEvent.click(dropdownButton);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_create_new_method')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_method')
    );
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with RCC Method > click Create New Version on dropdown button', () => {
    MockSelectElementMetadataStatus.mockReturnValue({
      loading: false
    });

    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.RC,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.RC,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.RC,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const dropdownButton = wrapper.container.querySelector(
      'button.dls-dropdown-button'
    ) as Element;
    userEvent.click(dropdownButton);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_create_new_version')
    );
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('MethodListModal_component'));
    expect(
      queryByText(wrapper.container, /MethodListModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with RCC Method > click Choose Method to Model on dropdown button', () => {
    MockSelectElementMetadataStatus.mockReturnValue({
      loading: false
    });

    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.RC,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.RC,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.RC,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const dropdownButton = wrapper.container.querySelector(
      'button.dls-dropdown-button'
    ) as Element;
    userEvent.click(dropdownButton);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_choose_method_to_model')
    ).toBeInTheDocument();

    fireEvent.click(
      wrapper.getByText('txt_manage_penalty_fee_choose_method_to_model')
    );
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('MethodListModal_component'));
    expect(
      queryByText(wrapper.container, /MethodListModal_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with RCC Method > Expand method', () => {
    MockSelectElementMetadataStatus.mockReturnValue({
      loading: false
    });

    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.RC,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.RC,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.RC,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    const firstChild = body?.children[0].querySelector(
      'button[class="btn btn-icon-secondary btn-sm"]'
    );
    fireEvent.click(firstChild as Element);
    expect(
      queryByText(wrapper.container, /SubRowGrid_component/i)
    ).toBeInTheDocument();
    fireEvent.click(firstChild as Element);
    expect(
      queryByText(wrapper.container, /SubRowGrid_component/i)
    ).not.toBeInTheDocument();
  });

  it('Should render with RCC Method > Edit method to list with method', () => {
    MockSelectElementMetadataStatus.mockReturnValue({
      loading: false
    });

    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.RC,
            methodType: 'NEWMETHOD',
            rowId: 1
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.RC,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.RC,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    expect(body?.children.length).toEqual(3);

    //SHOW Add new method modal
    const editButton = body?.children[1].querySelector(
      'button[class="btn btn-outline-primary btn-sm"]'
    ) as Element;
    fireEvent.click(editButton);
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
    //End

    mockOnCloseWithoutMethod.mockReturnValue(true);
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('MethodListModal_component'));
  });

  it('Should render with RCC Method > Edit method to list without method', () => {
    MockSelectElementMetadataStatus.mockReturnValue({
      loading: false
    });

    const props = {
      stepId: '1',
      selectedStep: '1',
      formValues: {
        methods: [
          {
            id: '1',
            name: 'name 1',
            serviceSubjectSection: ServiceSubjectSection.RC,
            methodType: 'NEWMETHOD',
            rowId: undefined
          },
          {
            id: '2',
            name: 'name 2',
            serviceSubjectSection: ServiceSubjectSection.RC,
            methodType: 'MODELEDMETHOD',
            rowId: 2
          },
          {
            id: '3',
            name: 'name 3',
            serviceSubjectSection: ServiceSubjectSection.RC,
            methodType: 'NEWVERSION',
            rowId: 3
          }
        ],
        isValid: true
      },
      setFormValues: jest.fn()
    };

    const wrapper = renderComponent(props);
    const body = wrapper.baseElement.querySelector(
      'tbody[class="dls-grid-body"]'
    );
    expect(body?.children.length).toEqual(3);

    //SHOW Add new method modal
    const editButton = body?.children[1].querySelector(
      'button[class="btn btn-outline-primary btn-sm"]'
    ) as Element;
    fireEvent.click(editButton);
    expect(
      wrapper.getByText('AddNewMethodModal_component')
    ).toBeInTheDocument();

    fireEvent.click(wrapper.getByText('AddNewMethodModal_component'));
    expect(
      queryByText(wrapper.container, /AddNewMethodModal_component/i)
    ).not.toBeInTheDocument();
    //End

    mockOnCloseWithoutMethod.mockReturnValue(false);
    expect(wrapper.getByText('MethodListModal_component')).toBeInTheDocument();
    fireEvent.click(wrapper.getByText('MethodListModal_component'));
  });
});
