import { MethodFieldParameterEnum } from 'app/constants/enums';
import { formatCommon, methodParamsToObject } from 'app/helpers';
import {
  ColumnType,
  Grid,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React, { useCallback, useMemo } from 'react';
import { parameterGroup, ParameterList } from './newMethodParameterList';

export interface SubRowGridInfoProps {
  original: ParameterList;
  method: WorkflowSetupMethod;
  metadata: {
    rccOptions: RefData[];
    minimumPayDueOptions: RefData[];
    oneFeeParticipationCodeOptions: RefData[];
    postingTextIDOptions: RefData[];
    reversalTextIDOptions: RefData[];
    firstYearMaxManagementOptions: RefData[];
    oneFeePriorityCodeOptions: RefData[];
  };
}
const SubRowGridInfo: React.FC<SubRowGridInfoProps> = ({
  original,
  method: methodProp,
  metadata
}) => {
  const { t } = useTranslation();

  const method = useMemo(() => methodParamsToObject(methodProp), [methodProp]);
  const data = useMemo(() => parameterGroup[original.id], [original.id]);

  const values = useMemo<Record<string, string | undefined>>(() => {
    const {
      rccOptions,
      minimumPayDueOptions,
      oneFeeParticipationCodeOptions,
      postingTextIDOptions,
      reversalTextIDOptions,
      firstYearMaxManagementOptions,
      oneFeePriorityCodeOptions
    } = metadata;

    return {
      [MethodFieldParameterEnum.OneFeeParticipationCode]:
        oneFeeParticipationCodeOptions.find(
          o =>
            o.code === method[MethodFieldParameterEnum.OneFeeParticipationCode]
        )?.text,
      [MethodFieldParameterEnum.PostingTextID]: postingTextIDOptions.find(
        o => o.code === method[MethodFieldParameterEnum.PostingTextID]
      )?.text,
      [MethodFieldParameterEnum.OneFeePriorityCode]:
        oneFeePriorityCodeOptions.find(
          o => o.code === method[MethodFieldParameterEnum.OneFeePriorityCode]
        )?.text,
      [MethodFieldParameterEnum.ReversalTextID]: reversalTextIDOptions.find(
        o => o.code === method[MethodFieldParameterEnum.ReversalTextID]
      )?.text,
      [MethodFieldParameterEnum.FirstYrMaxManagement]:
        firstYearMaxManagementOptions.find(
          o => o.code === method[MethodFieldParameterEnum.FirstYrMaxManagement]
        )?.text,
      [MethodFieldParameterEnum.MinimumPayDueOptions]:
        minimumPayDueOptions.find(
          o => o.code === method[MethodFieldParameterEnum.MinimumPayDueOptions]
        )?.text,
      [MethodFieldParameterEnum.ReturnedCheckChargesOption]: rccOptions.find(
        o =>
          o.code === method[MethodFieldParameterEnum.ReturnedCheckChargesOption]
      )?.text,
      [MethodFieldParameterEnum.AmountAssessed]: formatCommon(
        method[MethodFieldParameterEnum.AmountAssessed]!
      ).currency(2),
      [MethodFieldParameterEnum.ComputerLetter]:
        method[MethodFieldParameterEnum.ComputerLetter],
      [MethodFieldParameterEnum.BatchIdentification]:
        method[MethodFieldParameterEnum.BatchIdentification]
    };
  }, [metadata, method]);

  const valueAccessor = useCallback(
    (data: Record<string, any>) => (
      <TruncateText
        lines={2}
        ellipsisLessText={t('txt_less')}
        ellipsisMoreText={t('txt_more')}
      >
        {values[data.id]}
      </TruncateText>
    ),
    [t, values]
  );

  const columns: ColumnType[] = [
    {
      id: 'fieldName',
      Header: t('txt_business_name'),
      accessor: 'fieldName',
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'onlinePCF',
      Header: t('txt_manage_penalty_fee_online_PCF'),
      accessor: 'onlinePCF',
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'id',
      Header: t('txt_value'),
      accessor: valueAccessor,
      cellBodyProps: { className: 'py-8' }
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      accessor: viewMoreInfo(['moreInfo'], t),
      width: 105,
      cellBodyProps: { className: 'pt-12 pb-8' }
    }
  ];

  return (
    <div className="p-20 overflow-hidden">
      <Grid columns={columns} data={data} />
    </div>
  );
};

export default SubRowGridInfo;
