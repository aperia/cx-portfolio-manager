import EnhanceDropdownList from 'app/components/EnhanceDropdownList';
import {
  MethodFieldNameEnum,
  MethodFieldParameterEnum
} from 'app/constants/enums';
import { matchSearchValue } from 'app/helpers';
import {
  ColumnType,
  ComboBox,
  Grid,
  MaskedTextBox,
  NumericTextBox,
  TextBox,
  useTranslation
} from 'app/_libraries/_dls';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React, { useMemo } from 'react';
import { parameterGroup, ParameterList } from './newMethodParameterList';

interface IProps {
  searchValue: string;
  method: WorkflowSetupMethodObjectParams;
  original: ParameterList;
  metadata: {
    rccOptions: RefData[];
    minimumPayDueOptions: RefData[];
    oneFeeParticipationCodeOptions: RefData[];
    postingTextIDOptions: RefData[];
    reversalTextIDOptions: RefData[];
    firstYearMaxManagementOptions: RefData[];
    oneFeePriorityCodeOptions: RefData[];
  };
  onChange: (
    field: keyof WorkflowSetupMethodObjectParams,
    isRefData?: boolean
  ) => (e: any) => void;
  setTouched: (
    field: keyof WorkflowSetupMethodObjectParams,
    blur?: boolean | undefined
  ) => () => void;
  errors: Record<keyof WorkflowSetupMethodObjectParams, IFormError>;
}
const SubGridNewMethod: React.FC<IProps> = ({
  searchValue,
  method,
  original,
  metadata,
  errors,
  onChange,
  setTouched
}) => {
  const { t } = useTranslation();

  const {
    rccOptions,
    minimumPayDueOptions,
    oneFeeParticipationCodeOptions,
    postingTextIDOptions,
    reversalTextIDOptions,
    firstYearMaxManagementOptions,
    oneFeePriorityCodeOptions
  } = metadata;

  const data = useMemo(
    () =>
      parameterGroup[original.id].filter(p =>
        matchSearchValue(
          [p.fieldName, p.moreInfoText, p.onlinePCF],
          searchValue
        )
      ),
    [searchValue, original.id]
  );

  const valueAccessor = (data: Record<string, any>) => {
    const paramId = data.id as MethodFieldParameterEnum;

    switch (paramId) {
      case MethodFieldParameterEnum.ReturnedCheckChargesOption: {
        const value = rccOptions.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.ReturnedCheckChargesOption]
        );

        return (
          <EnhanceDropdownList
            dataTestId="addNewMethod__rccOption"
            size="small"
            value={value}
            placeholder={MethodFieldNameEnum.ReturnedCheckChargesOption}
            onChange={onChange(
              MethodFieldParameterEnum.ReturnedCheckChargesOption,
              true
            )}
            options={rccOptions}
          />
        );
      }
      case MethodFieldParameterEnum.AmountAssessed: {
        return (
          <NumericTextBox
            dataTestId="addNewMethod__amountAssessed"
            format="c2"
            showStep={false}
            maxLength={13}
            autoFocus={false}
            size="sm"
            placeholder={MethodFieldNameEnum.AmountAssessed}
            value={method?.[MethodFieldParameterEnum.AmountAssessed] || ''}
            onChange={onChange(MethodFieldParameterEnum.AmountAssessed)}
          />
        );
      }
      case MethodFieldParameterEnum.MinimumPayDueOptions: {
        const value = minimumPayDueOptions.find(
          o =>
            o.code === method?.[MethodFieldParameterEnum.MinimumPayDueOptions]
        );

        return (
          <EnhanceDropdownList
            dataTestId="addNewMethod__minimumPayDue"
            size="small"
            value={value}
            placeholder={MethodFieldNameEnum.MinimumPayDueOptions}
            onChange={onChange(
              MethodFieldParameterEnum.MinimumPayDueOptions,
              true
            )}
            options={minimumPayDueOptions}
          />
        );
      }
      case MethodFieldParameterEnum.OneFeeParticipationCode: {
        const value = oneFeeParticipationCodeOptions.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.OneFeeParticipationCode]
        );

        return (
          <EnhanceDropdownList
            dataTestId="addNewMethod__oneFeeParticipationCode"
            size="small"
            value={value}
            placeholder={MethodFieldNameEnum.OneFeeParticipationCode}
            onChange={onChange(
              MethodFieldParameterEnum.OneFeeParticipationCode,
              true
            )}
            options={oneFeeParticipationCodeOptions}
          />
        );
      }
      case MethodFieldParameterEnum.BatchIdentification: {
        return (
          <TextBox
            dataTestId="addNewMethod__batchIdentification"
            size="sm"
            placeholder={MethodFieldNameEnum.BatchIdentification}
            value={method?.[MethodFieldParameterEnum.BatchIdentification]}
            onChange={onChange(MethodFieldParameterEnum.BatchIdentification)}
            maxLength={2}
          />
        );
      }
      case MethodFieldParameterEnum.ComputerLetter: {
        return (
          <div className="masked-textbox-size-small">
            <MaskedTextBox
              dataTestId="addNewMethod__computerLetter"
              mask={[
                /[a-zA-Z0-9]/,
                /[a-zA-Z0-9]/,
                /[a-zA-Z0-9]/,
                /[a-zA-Z0-9]/
              ]}
              placeholder={MethodFieldNameEnum.ComputerLetter}
              value={method?.[MethodFieldParameterEnum.ComputerLetter]}
              onChange={onChange(MethodFieldParameterEnum.ComputerLetter)}
              onFocus={setTouched(MethodFieldParameterEnum.ComputerLetter)}
              onBlur={setTouched(MethodFieldParameterEnum.ComputerLetter, true)}
              error={errors[MethodFieldParameterEnum.ComputerLetter]}
            />
          </div>
        );
      }
      case MethodFieldParameterEnum.PostingTextID: {
        const value = postingTextIDOptions.find(
          o => o.code === method?.[MethodFieldParameterEnum.PostingTextID]
        );

        return (
          <ComboBox
            textField="text"
            value={value}
            size="small"
            placeholder={MethodFieldNameEnum.PostingTextID}
            onChange={onChange(MethodFieldParameterEnum.PostingTextID, true)}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {postingTextIDOptions.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.ReversalTextID: {
        const value = reversalTextIDOptions.find(
          o => o.code === method?.[MethodFieldParameterEnum.ReversalTextID]
        );
        return (
          <ComboBox
            textField="text"
            value={value}
            size="small"
            placeholder={MethodFieldNameEnum.ReversalTextID}
            onChange={onChange(MethodFieldParameterEnum.ReversalTextID, true)}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {reversalTextIDOptions.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.FirstYrMaxManagement: {
        const value = firstYearMaxManagementOptions.find(
          o =>
            o.code === method?.[MethodFieldParameterEnum.FirstYrMaxManagement]
        );

        return (
          <EnhanceDropdownList
            dataTestId="addNewMethod__firstYearMaxManagement"
            size="small"
            value={value}
            placeholder={MethodFieldNameEnum.FirstYrMaxManagement}
            onChange={onChange(
              MethodFieldParameterEnum.FirstYrMaxManagement,
              true
            )}
            options={firstYearMaxManagementOptions}
          />
        );
      }
      case MethodFieldParameterEnum.OneFeePriorityCode: {
        const value = oneFeePriorityCodeOptions.find(
          o => o.code === method?.[MethodFieldParameterEnum.OneFeePriorityCode]
        );

        return (
          <EnhanceDropdownList
            dataTestId="addNewMethod__oneFeePriorityCode"
            size="small"
            value={value}
            placeholder={MethodFieldNameEnum.OneFeePriorityCode}
            onChange={onChange(
              MethodFieldParameterEnum.OneFeePriorityCode,
              true
            )}
            options={oneFeePriorityCodeOptions}
          />
        );
      }
    }
  };

  const columns: ColumnType[] = [
    {
      id: 'fieldName',
      Header: t('txt_business_name'),
      accessor: 'fieldName',
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'onlinePCF',
      Header: t('txt_manage_penalty_fee_online_PCF'),
      accessor: 'onlinePCF',
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'value',
      Header: t('txt_value'),
      accessor: valueAccessor,
      cellBodyProps: { className: 'py-8' }
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      accessor: viewMoreInfo(['moreInfo'], t),
      width: 105,
      cellBodyProps: { className: 'pt-12 pb-8' }
    }
  ];

  return (
    <div className="p-20">
      <Grid columns={columns} data={data} />
    </div>
  );
};

export default SubGridNewMethod;
