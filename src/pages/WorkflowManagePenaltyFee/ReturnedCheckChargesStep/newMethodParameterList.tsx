import {
  MethodFieldNameEnum,
  MethodFieldParameterEnum
} from 'app/constants/enums';
import React from 'react';

export type ParameterListIds = 'Info_AssessmentCalculation' | 'Adv';

export interface ParameterList {
  id: ParameterListIds;
  section:
    | 'txt_manage_penalty_fee_standard_parameters'
    | 'txt_manage_penalty_fee_advanced_parameters';
  parameterGroup?: 'txt_manage_penalty_fee_assessment_calculation';
  moreInfo?: React.ReactNode;
}

export const parameterList: ParameterList[] = [
  {
    id: 'Info_AssessmentCalculation',
    section: 'txt_manage_penalty_fee_standard_parameters',
    parameterGroup: 'txt_manage_penalty_fee_assessment_calculation',
    moreInfo:
      'txt_manage_penalty_fee_returned_check_charges_advanced_parameters'
  },
  {
    id: 'Adv',
    section: 'txt_manage_penalty_fee_advanced_parameters'
  }
];

export interface ParameterGroup {
  id: MethodFieldParameterEnum;
  fieldName: MethodFieldNameEnum;
  onlinePCF: string;
  moreInfoText: string;
  moreInfo: React.ReactNode;
}
export const parameterGroup: Record<ParameterListIds, ParameterGroup[]> = {
  Info_AssessmentCalculation: [
    {
      id: MethodFieldParameterEnum.ReturnedCheckChargesOption,
      fieldName: MethodFieldNameEnum.ReturnedCheckChargesOption,
      onlinePCF: MethodFieldNameEnum.ReturnedCheckChargesOption,
      moreInfoText:
        'Determine whether or not a Return Check Charge will be assessed when a payment results in an insufficient-funds decline.',
      moreInfo:
        'Determine whether or not a Return Check Charge will be assessed when a payment results in an insufficient-funds decline.'
    },
    {
      id: MethodFieldParameterEnum.AmountAssessed,
      fieldName: MethodFieldNameEnum.AmountAssessed,
      onlinePCF: 'Returned Check Minimum or Fixed Amount',
      moreInfoText:
        'This is the amount assessed to the cardholder when a payment results in an insufficient-funds decline. You can enter up to two decimal places.',
      moreInfo:
        'This is the amount assessed to the cardholder when a payment results in an insufficient-funds decline. You can enter up to two decimal places.'
    },
    {
      id: MethodFieldParameterEnum.MinimumPayDueOptions,
      fieldName: MethodFieldNameEnum.MinimumPayDueOptions,
      onlinePCF: 'RC MPD CD',
      moreInfoText:
        'Consult your legal and compliance partners before changing the value of this field. Use this parameter to determine how the Reasonable Amount fee is calculated when cardholders incur an insufficient-funds penalty fee. This ensures that fees are not higher than the minimum payment due.',
      moreInfo: (
        <div>
          <p>
            Consult your legal and compliance partners before changing the value
            of this field.
          </p>
          <p className="mt-8">
            Use this parameter to determine how the Reasonable Amount fee is
            calculated when cardholders incur an insufficient-funds penalty fee.
            This ensures that fees are not higher than the minimum payment due.
          </p>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.OneFeeParticipationCode,
      fieldName: MethodFieldNameEnum.OneFeeParticipationCode,
      onlinePCF: 'One Fee Part CD',
      moreInfoText:
        'The One-Fee Participation Code parameter indicates whether you use One-Fee Processing for penalty fees. The One-Fee feature enables you to assess no more than one penalty fee per penalty-qualifying infraction in a single cycle. Eligible fees include late charges, overlimit charges, and returned check charges including those returned check charges that were externally generated when the batch ID matches the setting in the Batch Identification parameter in this section, and those charges that were the result of cycle-to-date processing or a cross-cycle adjustment. In addition, an increase to a late charge or overlimit charge from a previous statement cycle are included in the evaluation. The system will permit either one penalty fee or one increase to a previous penalty fee to post to the account per statement cycle. Consult your legal and compliance partners before changing the value of this field.',
      moreInfo:
        'The One-Fee Participation Code parameter indicates whether you use One-Fee Processing for penalty fees. The One-Fee feature enables you to assess no more than one penalty fee per penalty-qualifying infraction in a single cycle. Eligible fees include late charges, overlimit charges, and returned check charges including those returned check charges that were externally generated when the batch ID matches the setting in the Batch Identification parameter in this section, and those charges that were the result of cycle-to-date processing or a cross-cycle adjustment. In addition, an increase to a late charge or overlimit charge from a previous statement cycle are included in the evaluation. The system will permit either one penalty fee or one increase to a previous penalty fee to post to the account per statement cycle. Consult your legal and compliance partners before changing the value of this field.'
    }
  ],
  Adv: [
    {
      id: MethodFieldParameterEnum.BatchIdentification,
      fieldName: MethodFieldNameEnum.BatchIdentification,
      onlinePCF: 'Returned Check Batch Identification',
      moreInfoText:
        'The Batch Identification parameter sets the alpha reference code used to identify a batch of payment reversals, insufficient check charges, and reversals of a returned check charge.',
      moreInfo:
        'The Batch Identification parameter sets the alpha reference code used to identify a batch of payment reversals, insufficient check charges, and reversals of a returned check charge.'
    },
    {
      id: MethodFieldParameterEnum.ComputerLetter,
      fieldName: MethodFieldNameEnum.ComputerLetter,
      onlinePCF: 'Returned Check Computer Letter',
      moreInfoText:
        'If you set the Returned Check Charges Option to 1 or 2 and you want to generate a letter, enter a valid 4-character letter identifier here. The identifier may be any combination of letters or numbers. Enter leading zeros for a 1-, 2-, or 3-character identifier. If you do not generate a letter, leave this field blank or enter 0000.',
      moreInfo:
        'If you set the Returned Check Charges Option to 1 or 2 and you want to generate a letter, enter a valid 4-character letter identifier here. The identifier may be any combination of letters or numbers. Enter leading zeros for a 1-, 2-, or 3-character identifier. If you do not generate a letter, leave this field blank or enter 0000.'
    },
    {
      id: MethodFieldParameterEnum.PostingTextID,
      fieldName: MethodFieldNameEnum.PostingTextID,
      onlinePCF: 'Returned Check Charge Statement Descr Posting Text Id',
      moreInfoText:
        'Identify the Return Check Charge text that will appear on statements when this fee posts.',
      moreInfo: (
        <div>
          <p>Text Area = DT, Text Type = RP.</p>
          <br />
          Identify the Return Check Charge text that will appear on statements
          when this fee posts.
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.ReversalTextID,
      fieldName: MethodFieldNameEnum.ReversalTextID,
      onlinePCF: 'Statement Description Reversal Text Id',
      moreInfoText:
        'Identify the Return Check Charge text that will appear on statements when this fee is reversed.',
      moreInfo: (
        <div>
          <p>Text Area = DT, Text Type = RR.</p>
          <br />
          Identify the Return Check Charge text that will appear on statements
          when this fee is reversed.
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.FirstYrMaxManagement,
      fieldName: MethodFieldNameEnum.FirstYrMaxManagement,
      onlinePCF: 'RC 1st Yr Max Mgmt CD',
      moreInfoText:
        'Consult your legal and compliance partners before changing the value of this field. The First Year Maximum Fee Management concept is related to Regulation Z and Card Act compliance that prevents issuers from charging a first-year sum of total fees that is greater than 25% of the total credit line.',
      moreInfo:
        'Consult your legal and compliance partners before changing the value of this field. The First Year Maximum Fee Management concept is related to Regulation Z and Card Act compliance that prevents issuers from charging a first-year sum of total fees that is greater than 25% of the total credit line.'
    },
    {
      id: MethodFieldParameterEnum.OneFeePriorityCode,
      fieldName: MethodFieldNameEnum.OneFeePriorityCode,
      onlinePCF: 'One Fee Priorization Code',
      moreInfoText:
        'Use this parameter only when One Fee Processing is applied. Identify whether to charge a Return Check Charge or Late Charge when One Fee Processing is applied and a cardholder incurs both fees in the same cycle.',
      moreInfo:
        'Use this parameter only when One Fee Processing is applied. Identify whether to charge a Return Check Charge or Late Charge when One Fee Processing is applied and a cardholder incurs both fees in the same cycle.'
    }
  ]
};
