import { render } from '@testing-library/react';
import React from 'react';
import SubRowGrid from './SubRowGrid';

jest.mock('app/_libraries/_dof/core', () => ({
  View: ({ formKey, descriptor, onClick }: any) => {
    return (
      <div id={formKey}>
        {formKey}
        <span>{descriptor}</span>
      </div>
    );
  }
}));

describe('ManagePenaltyFeeWorkflow > ReturnedCheckChargesStep > SubRowGrid', () => {
  it('Should render view with row_id', () => {
    const props = {
      original: {
        rowId: 'row_id'
      },
      metadata: {
        rccOptions: [
          {
            code: 'code',
            text: 'text'
          }
        ],
        minimumPayDueOptions: [
          {
            code: 'code',
            text: 'text'
          }
        ],
        oneFeeParticipationCodeOptions: [
          {
            code: 'code',
            text: 'text'
          }
        ],
        postingTextIDOptions: [
          {
            code: 'code',
            text: 'text'
          }
        ],
        reversalTextIDOptions: [
          {
            code: 'code',
            text: 'text'
          }
        ],
        firstYearMaxManagementOptions: [
          {
            code: 'code',
            text: 'text'
          }
        ],
        oneFeePriorityCodeOptions: [
          {
            code: 'code',
            text: 'text'
          }
        ]
      }
    };

    const wrapper = render(<SubRowGrid {...props} />);
    expect(
      wrapper.getByText('txt_manage_penalty_fee_advanced_parameters')
    ).toBeInTheDocument();
  });
});
