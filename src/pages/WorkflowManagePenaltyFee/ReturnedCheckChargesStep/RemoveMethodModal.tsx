import ModalRegistry from 'app/components/ModalRegistry';
import {
  Button,
  ModalBody,
  ModalHeader,
  ModalTitle,
  useTranslation
} from 'app/_libraries/_dls';
import isFunction from 'lodash.isfunction';
import React from 'react';

interface IProps {
  id: string;
  show: boolean;
  methodName?: string;
  onClose?: (reload?: boolean) => void;
}

const RemoveMethodModal: React.FC<IProps> = ({
  id,
  methodName,
  show,
  onClose
}) => {
  const { t } = useTranslation();
  const onRemove = async () => {
    isFunction(onClose) && onClose(true);
  };

  const handleCloseWithoutReload = () => {
    isFunction(onClose) && onClose();
  };

  return (
    <ModalRegistry
      id={id}
      show={show}
      onAutoClosedAll={handleCloseWithoutReload}
      classes={{
        content: ''
      }}
    >
      <ModalHeader border closeButton onHide={handleCloseWithoutReload}>
        <ModalTitle>Confirm Delete</ModalTitle>
      </ModalHeader>
      <ModalBody>
        <span>
          Are you sure you want to permanently delete the method{' '}
          <span className="fw-600">{methodName} </span> from this workflow?
        </span>
      </ModalBody>
      <div className="dls-modal-footer modal-footer">
        <Button variant="secondary" onClick={handleCloseWithoutReload}>
          {t('txt_cancel')}
        </Button>
        <Button variant="danger" onClick={onRemove}>
          Delete
        </Button>
      </div>
    </ModalRegistry>
  );
};

export default RemoveMethodModal;
