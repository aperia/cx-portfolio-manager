import { ServiceSubjectSection } from 'app/constants/enums';
import {
  getWorkflowSetupMethodType,
  getWorkflowSetupStepStatus
} from 'app/helpers';
import { ReturnedCheckChargesFormValues } from '.';

const parseFormValues: WorkflowSetupStepFormDataFunc<ReturnedCheckChargesFormValues> =
  (data, id) => {
    const stepInfo = data?.data?.workflowSetupData?.find(s => s.id === id);
    if (!stepInfo) return;

    const now = Date.now();
    const methods = (data.data.configurations?.methodList || [])
      .filter(
        (method: any) =>
          method.serviceSubjectSection === ServiceSubjectSection.RC
      )
      .map(
        (method: any, idx: number) =>
          ({
            ...method,
            methodType: getWorkflowSetupMethodType(method),
            rowId: now + idx
          } as WorkflowSetupMethod & { rowId?: number })
      );

    return {
      ...getWorkflowSetupStepStatus(stepInfo),
      isValid: methods.length > 0,
      methods
    };
  };

export default parseFormValues;
