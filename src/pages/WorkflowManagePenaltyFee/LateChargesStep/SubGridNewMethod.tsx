import EnhanceDropdownList from 'app/components/EnhanceDropdownList';
import {
  MethodFieldNameEnum,
  MethodFieldParameterEnum
} from 'app/constants/enums';
import { matchSearchValue } from 'app/helpers';
import {
  ColumnType,
  ComboBox,
  Grid,
  NumericTextBox,
  useTranslation
} from 'app/_libraries/_dls';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React, { useMemo } from 'react';
import { parameterGroup, ParameterList } from './newMethodParameterList';

interface IProps {
  searchValue: string;
  method: WorkflowSetupMethodObjectParams;
  original: ParameterList;
  metadata: {
    lateChargesOption: RefData[];
    calculationBase: RefData[];
    assessmentControl: RefData[];
    balanceIndicator: RefData[];
    assessedAccounts: RefData[];
    currentBalanceAssessment: RefData[];
    calculationDayControl: RefData[];
    nonProcessingLateCharge: RefData[];
    includeExcludeControl: RefData[];
    status1: RefData[];
    status2: RefData[];
    status3: RefData[];
    status4: RefData[];
    status5: RefData[];
    lateChargeResetCounter: RefData[];
    minimumPayDueOptions: RefData[];
    reversalDetailsTextID: RefData[];
    waivedMessageTextID: RefData[];
    latePaymentWarningMessage: RefData[];
  };
  onChange: (
    field: keyof WorkflowSetupMethodObjectParams,
    isRefData?: boolean
  ) => (e: any) => void;
}
const SubGridNewMethod: React.FC<IProps> = ({
  searchValue,
  method,
  original,
  metadata,
  onChange
}) => {
  const { t } = useTranslation();

  const {
    lateChargesOption,
    calculationBase,
    assessmentControl,
    balanceIndicator,
    assessedAccounts,
    currentBalanceAssessment,
    calculationDayControl,
    nonProcessingLateCharge,
    includeExcludeControl,
    status1,
    status2,
    status3,
    status4,
    status5,
    lateChargeResetCounter,
    minimumPayDueOptions,
    reversalDetailsTextID,
    waivedMessageTextID,
    latePaymentWarningMessage
  } = metadata;

  const data = useMemo(
    () =>
      parameterGroup[original.id].filter(p =>
        matchSearchValue(
          [p.fieldName, p.moreInfoText, p.onlinePCF],
          searchValue
        )
      ),
    [searchValue, original.id]
  );

  const valueAccessor = (data: Record<string, any>) => {
    const paramId = data.id as MethodFieldParameterEnum;

    switch (paramId) {
      case MethodFieldParameterEnum.LateChargesOption: {
        const value = lateChargesOption.find(
          o => o.code === method?.[MethodFieldParameterEnum.LateChargesOption]
        );

        return (
          <EnhanceDropdownList
            dataTestId="addNewMethod__lcOption"
            size="small"
            value={value}
            placeholder={MethodFieldNameEnum.LateChargesOption}
            onChange={onChange(
              MethodFieldParameterEnum.LateChargesOption,
              true
            )}
            options={lateChargesOption}
          />
        );
      }
      case MethodFieldParameterEnum.CalculationBase: {
        const value = calculationBase.find(
          o => o.code === method?.[MethodFieldParameterEnum.CalculationBase]
        );

        return (
          <EnhanceDropdownList
            dataTestId="addNewMethod__calculationBaseOption"
            size="small"
            value={value}
            placeholder={MethodFieldNameEnum.CalculationBase}
            onChange={onChange(MethodFieldParameterEnum.CalculationBase, true)}
            options={calculationBase}
          />
        );
      }
      case MethodFieldParameterEnum.MinimumPayDueOptions: {
        const value = minimumPayDueOptions.find(
          o =>
            o.code === method?.[MethodFieldParameterEnum.MinimumPayDueOptions]
        );

        return (
          <EnhanceDropdownList
            dataTestId="addNewMethod__minimumPayDueOptions"
            size="small"
            value={value}
            placeholder={MethodFieldNameEnum.MinimumPayDueOptions}
            onChange={onChange(
              MethodFieldParameterEnum.MinimumPayDueOptions,
              true
            )}
            options={minimumPayDueOptions}
          />
        );
      }
      case MethodFieldParameterEnum.FixedAmount: {
        return (
          <NumericTextBox
            dataTestId="addNewMethod__minimumFixedAmount"
            size="sm"
            placeholder={MethodFieldNameEnum.FixedAmount}
            value={method?.[MethodFieldParameterEnum.FixedAmount] || ''}
            onChange={onChange(MethodFieldParameterEnum.FixedAmount)}
            format="c2"
            showStep={false}
            maxLength={13}
            autoFocus={false}
          />
        );
      }
      case MethodFieldParameterEnum.Percent: {
        return (
          <NumericTextBox
            dataTestId="addNewMethod__percent"
            size="sm"
            format="p3"
            showStep={false}
            maxLength={5}
            autoFocus={false}
            placeholder={MethodFieldNameEnum.Percent}
            value={method?.[MethodFieldParameterEnum.Percent] || ''}
            onChange={onChange(MethodFieldParameterEnum.Percent)}
          />
        );
      }
      case MethodFieldParameterEnum.MaximumAmount: {
        return (
          <NumericTextBox
            dataTestId="addNewMethod__maximumAmount"
            size="sm"
            format="c2"
            showStep={false}
            maxLength={13}
            autoFocus={false}
            placeholder={MethodFieldNameEnum.MaximumAmount}
            value={method?.[MethodFieldParameterEnum.MaximumAmount] || ''}
            onChange={onChange(MethodFieldParameterEnum.MaximumAmount)}
          />
        );
      }
      case MethodFieldParameterEnum.MaximumYearlyAmount: {
        return (
          <NumericTextBox
            dataTestId="addNewMethod__maximumYearlyAmount"
            size="sm"
            format="c2"
            showStep={false}
            maxLength={13}
            autoFocus={false}
            placeholder={MethodFieldNameEnum.MaximumYearlyAmount}
            value={method?.[MethodFieldParameterEnum.MaximumYearlyAmount] || ''}
            onChange={onChange(MethodFieldParameterEnum.MaximumYearlyAmount)}
          />
        );
      }
      case MethodFieldParameterEnum.AssessmentControl: {
        const value = assessmentControl.find(
          o => o.code === method?.[MethodFieldParameterEnum.AssessmentControl]
        );

        return (
          <EnhanceDropdownList
            dataTestId="addNewMethod__assessmentControl"
            size="small"
            value={value}
            placeholder={MethodFieldNameEnum.AssessmentControl}
            onChange={onChange(
              MethodFieldParameterEnum.AssessmentControl,
              true
            )}
            options={assessmentControl}
          />
        );
      }
      case MethodFieldParameterEnum.CyclesOfConsecutiveDelinquency: {
        return (
          <NumericTextBox
            dataTestId="addNewMethod__cyclesOfConsecutiveDelinquency"
            size="sm"
            format="i0"
            maxLength={1}
            min={0}
            max={9}
            autoFocus={false}
            placeholder={MethodFieldNameEnum.CyclesOfConsecutiveDelinquency}
            value={
              method?.[MethodFieldParameterEnum.CyclesOfConsecutiveDelinquency]
            }
            onChange={onChange(
              MethodFieldParameterEnum.CyclesOfConsecutiveDelinquency
            )}
          />
        );
      }
      case MethodFieldParameterEnum.BalanceIndicator: {
        const value = balanceIndicator.find(
          o => o.code === method?.[MethodFieldParameterEnum.BalanceIndicator]
        );

        return (
          <EnhanceDropdownList
            dataTestId="addNewMethod__balanceIndicator"
            size="small"
            value={value}
            placeholder={MethodFieldNameEnum.BalanceIndicator}
            onChange={onChange(MethodFieldParameterEnum.BalanceIndicator, true)}
            options={balanceIndicator}
          />
        );
      }
      case MethodFieldParameterEnum.AssessedAccounts: {
        const value = assessedAccounts.find(
          o => o.code === method?.[MethodFieldParameterEnum.AssessedAccounts]
        );

        return (
          <EnhanceDropdownList
            dataTestId="addNewMethod__assessedAccounts"
            size="small"
            value={value}
            placeholder={MethodFieldNameEnum.AssessedAccounts}
            onChange={onChange(MethodFieldParameterEnum.AssessedAccounts, true)}
            options={assessedAccounts}
            tooltipItemCustom={[
              {
                code: '7',
                tooltipItem: (
                  <div>
                    <p>
                      7 - Do not assess late charges if the account meets one of
                      the following conditions:
                    </p>
                    <p className="mt-8">
                      • The customer’s cycle-to-date payments have reduced the
                      minimum payment due to an amount less than that in the
                      Minimum Delq Amount parameter.
                    </p>
                    <p className="mt-8">
                      • The beginning balance is less than that in the Minimum
                      or Fixed Amount parameter in this section. If you use this
                      option, you must also set the Exclusion Balance and
                      Minimum or Fixed Amount parameters to values greater than
                      zero.
                    </p>
                  </div>
                )
              },
              {
                code: '8',
                tooltipItem: (
                  <div>
                    <p>
                      8 - Do not assess late charges if the account meets one of
                      the following conditions:
                    </p>
                    <p className="mt-8">
                      • The unpaid portion of the PCF-calculated MPD is less
                      than or equal to the amount in the Threshold Waive Amount
                      parameter in this section.
                    </p>
                    <p className="mt-8">
                      • The balance is less than that in the Exclusion Balance
                      parameter. If you use this option, you must also set the
                      Threshold Waive Amount parameter to a value greater than
                      zero.
                    </p>
                  </div>
                )
              }
            ]}
          />
        );
      }
      case MethodFieldParameterEnum.ExclusionBalance: {
        return (
          <NumericTextBox
            dataTestId="addNewMethod__exclusionBalance"
            size="sm"
            format="c0"
            showStep={false}
            maxLength={13}
            autoFocus={false}
            placeholder={MethodFieldNameEnum.ExclusionBalance}
            value={method?.[MethodFieldParameterEnum.ExclusionBalance]}
            onChange={onChange(MethodFieldParameterEnum.ExclusionBalance)}
          />
        );
      }
      case MethodFieldParameterEnum.CurrentBalanceAssessment: {
        const value = currentBalanceAssessment.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.CurrentBalanceAssessment]
        );

        return (
          <EnhanceDropdownList
            dataTestId="addNewMethod__currentBalanceAssessment"
            size="small"
            value={value}
            placeholder={MethodFieldNameEnum.CurrentBalanceAssessment}
            onChange={onChange(
              MethodFieldParameterEnum.CurrentBalanceAssessment,
              true
            )}
            options={currentBalanceAssessment}
          />
        );
      }
      case MethodFieldParameterEnum.CalculationDayControl: {
        const value = calculationDayControl.find(
          o =>
            o.code === method?.[MethodFieldParameterEnum.CalculationDayControl]
        );

        return (
          <EnhanceDropdownList
            dataTestId="addNewMethod__calculationDayControl"
            size="small"
            value={value}
            placeholder={MethodFieldNameEnum.CalculationDayControl}
            onChange={onChange(
              MethodFieldParameterEnum.CalculationDayControl,
              true
            )}
            options={calculationDayControl}
          />
        );
      }
      case MethodFieldParameterEnum.NumberOfDays: {
        return (
          <NumericTextBox
            dataTestId="addNewMethod__numberOfDays"
            size="sm"
            format="i0"
            maxLength={2}
            min={0}
            max={30}
            autoFocus={false}
            placeholder={MethodFieldNameEnum.NumberOfDays}
            value={method?.[MethodFieldParameterEnum.NumberOfDays]}
            onChange={onChange(MethodFieldParameterEnum.NumberOfDays)}
          />
        );
      }
      case MethodFieldParameterEnum.NonProcessingLateCharge: {
        const value = nonProcessingLateCharge.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.NonProcessingLateCharge]
        );

        return (
          <EnhanceDropdownList
            dataTestId="addNewMethod__nonProcessingLateCharge"
            size="small"
            value={value}
            placeholder={MethodFieldNameEnum.NonProcessingLateCharge}
            onChange={onChange(
              MethodFieldParameterEnum.NonProcessingLateCharge,
              true
            )}
            options={nonProcessingLateCharge}
          />
        );
      }
      case MethodFieldParameterEnum.IncludeExcludeControl: {
        const value = includeExcludeControl.find(
          o =>
            o.code === method?.[MethodFieldParameterEnum.IncludeExcludeControl]
        );

        return (
          <EnhanceDropdownList
            dataTestId="addNewMethod__includeOrExcludeControl"
            size="small"
            value={value}
            placeholder={MethodFieldNameEnum.IncludeExcludeControl}
            onChange={onChange(
              MethodFieldParameterEnum.IncludeExcludeControl,
              true
            )}
            options={includeExcludeControl}
          />
        );
      }
      case MethodFieldParameterEnum.Status1: {
        const value = status1.find(
          o => o.code === method?.[MethodFieldParameterEnum.Status1]
        );

        return (
          <EnhanceDropdownList
            dataTestId="addNewMethod__status1Option"
            size="small"
            value={value}
            placeholder={MethodFieldNameEnum.Status1}
            onChange={onChange(MethodFieldParameterEnum.Status1, true)}
            options={status1}
          />
        );
      }
      case MethodFieldParameterEnum.Status2: {
        const value = status2.find(
          o => o.code === method?.[MethodFieldParameterEnum.Status2]
        );

        return (
          <EnhanceDropdownList
            dataTestId="addNewMethod__status2Option"
            size="small"
            value={value}
            placeholder={MethodFieldNameEnum.Status2}
            onChange={onChange(MethodFieldParameterEnum.Status2, true)}
            options={status2}
          />
        );
      }
      case MethodFieldParameterEnum.Status3: {
        const value = status3.find(
          o => o.code === method?.[MethodFieldParameterEnum.Status3]
        );

        return (
          <EnhanceDropdownList
            dataTestId="addNewMethod__status3Option"
            size="small"
            value={value}
            placeholder={MethodFieldNameEnum.Status3}
            onChange={onChange(MethodFieldParameterEnum.Status3, true)}
            options={status3}
          />
        );
      }
      case MethodFieldParameterEnum.Status4: {
        const value = status4.find(
          o => o.code === method?.[MethodFieldParameterEnum.Status4]
        );

        return (
          <EnhanceDropdownList
            dataTestId="addNewMethod__status4Option"
            size="small"
            value={value}
            placeholder={MethodFieldNameEnum.Status4}
            onChange={onChange(MethodFieldParameterEnum.Status4, true)}
            options={status4}
          />
        );
      }
      case MethodFieldParameterEnum.Status5: {
        const value = status5.find(
          o => o.code === method?.[MethodFieldParameterEnum.Status5]
        );

        return (
          <EnhanceDropdownList
            dataTestId="addNewMethod__status5Option"
            size="small"
            value={value}
            placeholder={MethodFieldNameEnum.Status5}
            onChange={onChange(MethodFieldParameterEnum.Status5, true)}
            options={status5}
          />
        );
      }
      case MethodFieldParameterEnum.Tier2Balance: {
        return (
          <NumericTextBox
            dataTestId="addNewMethod__tier2Balance"
            size="sm"
            format="c2"
            showStep={false}
            maxLength={17}
            autoFocus={false}
            placeholder={MethodFieldNameEnum.Tier2Balance}
            value={method?.[MethodFieldParameterEnum.Tier2Balance]}
            onChange={onChange(MethodFieldParameterEnum.Tier2Balance)}
          />
        );
      }
      case MethodFieldParameterEnum.Tier2Amount: {
        return (
          <NumericTextBox
            dataTestId="addNewMethod__tier2Amount"
            size="sm"
            format="c2"
            showStep={false}
            maxLength={17}
            autoFocus={false}
            placeholder={MethodFieldNameEnum.Tier2Amount}
            value={method?.[MethodFieldParameterEnum.Tier2Amount]}
            onChange={onChange(MethodFieldParameterEnum.Tier2Amount)}
          />
        );
      }
      case MethodFieldParameterEnum.Tier3Balance: {
        return (
          <NumericTextBox
            dataTestId="addNewMethod__tier3Balance"
            size="sm"
            format="c2"
            showStep={false}
            maxLength={17}
            autoFocus={false}
            placeholder={MethodFieldNameEnum.Tier3Balance}
            value={method?.[MethodFieldParameterEnum.Tier3Balance]}
            onChange={onChange(MethodFieldParameterEnum.Tier3Balance)}
          />
        );
      }
      case MethodFieldParameterEnum.Tier3Amount: {
        return (
          <NumericTextBox
            dataTestId="addNewMethod__tier3Amount"
            size="sm"
            format="c2"
            showStep={false}
            maxLength={17}
            autoFocus={false}
            placeholder={MethodFieldNameEnum.Tier3Amount}
            value={method?.[MethodFieldParameterEnum.Tier3Amount]}
            onChange={onChange(MethodFieldParameterEnum.Tier3Amount)}
          />
        );
      }
      case MethodFieldParameterEnum.Tier4Balance: {
        return (
          <NumericTextBox
            dataTestId="addNewMethod__tier4Balance"
            size="sm"
            format="c2"
            showStep={false}
            maxLength={17}
            autoFocus={false}
            placeholder={MethodFieldNameEnum.Tier4Balance}
            value={method?.[MethodFieldParameterEnum.Tier4Balance]}
            onChange={onChange(MethodFieldParameterEnum.Tier4Balance)}
          />
        );
      }
      case MethodFieldParameterEnum.Tier4Amount: {
        return (
          <NumericTextBox
            dataTestId="addNewMethod__tier4Amount"
            size="sm"
            format="c2"
            showStep={false}
            maxLength={17}
            autoFocus={false}
            placeholder={MethodFieldNameEnum.Tier4Amount}
            value={method?.[MethodFieldParameterEnum.Tier4Amount]}
            onChange={onChange(MethodFieldParameterEnum.Tier4Amount)}
          />
        );
      }
      case MethodFieldParameterEnum.Tier5Balance: {
        return (
          <NumericTextBox
            dataTestId="addNewMethod__tier5Balance"
            size="sm"
            format="c2"
            showStep={false}
            maxLength={17}
            autoFocus={false}
            placeholder={MethodFieldNameEnum.Tier5Balance}
            value={method?.[MethodFieldParameterEnum.Tier5Balance]}
            onChange={onChange(MethodFieldParameterEnum.Tier5Balance)}
          />
        );
      }
      case MethodFieldParameterEnum.Tier5Amount: {
        return (
          <NumericTextBox
            dataTestId="addNewMethod__tier25Amount"
            size="sm"
            format="c2"
            showStep={false}
            maxLength={17}
            autoFocus={false}
            placeholder={MethodFieldNameEnum.Tier5Amount}
            value={method?.[MethodFieldParameterEnum.Tier5Amount]}
            onChange={onChange(MethodFieldParameterEnum.Tier5Amount)}
          />
        );
      }
      case MethodFieldParameterEnum.ThresholdToWaveLateCharge: {
        return (
          <NumericTextBox
            dataTestId="addNewMethod__thresholdToWaveLateCharge"
            format="c2"
            showStep={false}
            maxLength={17}
            size="sm"
            autoFocus={false}
            placeholder={MethodFieldNameEnum.ThresholdToWaveLateCharge}
            value={method?.[MethodFieldParameterEnum.ThresholdToWaveLateCharge]}
            onChange={onChange(
              MethodFieldParameterEnum.ThresholdToWaveLateCharge
            )}
          />
        );
      }
      case MethodFieldParameterEnum.LateChargeResetCounter: {
        const value = lateChargeResetCounter.find(
          o =>
            o.code === method?.[MethodFieldParameterEnum.LateChargeResetCounter]
        );

        return (
          <EnhanceDropdownList
            dataTestId="addNewMethod__lateChargeResetCounter"
            size="small"
            value={value}
            placeholder={MethodFieldNameEnum.LateChargeResetCounter}
            onChange={onChange(
              MethodFieldParameterEnum.LateChargeResetCounter,
              true
            )}
            options={lateChargeResetCounter}
          />
        );
      }
      case MethodFieldParameterEnum.ReversalDetailsTextID: {
        const value = reversalDetailsTextID.find(
          o =>
            o.code === method?.[MethodFieldParameterEnum.ReversalDetailsTextID]
        );

        return (
          <ComboBox
            textField="text"
            value={value}
            size="small"
            placeholder={MethodFieldNameEnum.ReversalDetailsTextID}
            onChange={onChange(
              MethodFieldParameterEnum.ReversalDetailsTextID,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {reversalDetailsTextID.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.WaivedMessageTextID: {
        const value = waivedMessageTextID.find(
          o => o.code === method?.[MethodFieldParameterEnum.WaivedMessageTextID]
        );

        return (
          <ComboBox
            textField="text"
            value={value}
            placeholder={MethodFieldNameEnum.WaivedMessageTextID}
            size="small"
            onChange={onChange(
              MethodFieldParameterEnum.WaivedMessageTextID,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {waivedMessageTextID.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
      case MethodFieldParameterEnum.LatePaymentWarningMessage: {
        const value = latePaymentWarningMessage.find(
          o =>
            o.code ===
            method?.[MethodFieldParameterEnum.LatePaymentWarningMessage]
        );

        return (
          <ComboBox
            textField="text"
            value={value}
            placeholder={MethodFieldNameEnum.LatePaymentWarningMessage}
            size="small"
            onChange={onChange(
              MethodFieldParameterEnum.LatePaymentWarningMessage,
              true
            )}
            noResult={t('txt_no_results_found_without_dot')}
          >
            {latePaymentWarningMessage.map(item => (
              <ComboBox.Item key={item.code} value={item} label={item.text} />
            ))}
          </ComboBox>
        );
      }
    }
  };

  const columns: ColumnType[] = [
    {
      id: 'fieldName',
      Header: t('txt_business_name'),
      accessor: 'fieldName',
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'onlinePCF',
      Header: t('txt_manage_penalty_fee_online_PCF'),
      accessor: 'onlinePCF',
      cellBodyProps: { className: 'pt-12 pb-8' }
    },
    {
      id: 'value',
      Header: t('txt_value'),
      accessor: valueAccessor,
      cellBodyProps: { className: 'py-8' }
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      accessor: viewMoreInfo(['moreInfo'], t),
      width: 105,
      cellBodyProps: { className: 'pt-12 pb-8' }
    }
  ];

  return (
    <div className="p-20">
      <Grid columns={columns} data={data} />
    </div>
  );
};

export default SubGridNewMethod;
