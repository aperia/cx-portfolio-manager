import { mapGridExpandCollapse } from 'app/helpers';
import { Grid, useTranslation } from 'app/_libraries/_dls';
import React, { useState } from 'react';
import newMethodColumns from './newMethodColumns';
import { parameterList, ParameterListIds } from './newMethodParameterList';
import SubRowGridInfo from './SubRowGridInfo';

export interface SubGridProps {
  original: any;
  metadata: {
    lateChargesOption: RefData[];
    calculationBase: RefData[];
    assessmentControl: RefData[];
    balanceIndicator: RefData[];
    assessedAccounts: RefData[];
    currentBalanceAssessment: RefData[];
    calculationDayControl: RefData[];
    nonProcessingLateCharge: RefData[];
    includeExcludeControl: RefData[];
    status1: RefData[];
    status2: RefData[];
    status3: RefData[];
    status4: RefData[];
    status5: RefData[];
    lateChargeResetCounter: RefData[];
    minimumPayDueOptions: RefData[];
    reversalDetailsTextID: RefData[];
    waivedMessageTextID: RefData[];
    latePaymentWarningMessage: RefData[];
  };
}
const SubRowGrid: React.FC<SubGridProps> = ({ original: method, metadata }) => {
  const { t } = useTranslation();
  const [expandedList, setExpandedList] = useState<ParameterListIds[]>([
    'Info_AssessmentCalculation'
  ]);

  return (
    <div className="p-20 overflow-hidden">
      <Grid
        columns={newMethodColumns(t)}
        data={parameterList}
        subRow={({ original }: MagicKeyValue) => (
          <SubRowGridInfo
            method={method}
            original={original}
            metadata={metadata}
          />
        )}
        dataItemKey="id"
        togglable
        expandedItemKey="id"
        expandedList={expandedList}
        onExpand={setExpandedList as any}
        toggleButtonConfigList={parameterList.map(
          mapGridExpandCollapse('id', t)
        )}
      />
    </div>
  );
};

export default SubRowGrid;
