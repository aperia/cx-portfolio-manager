import { MethodFieldParameterEnum } from 'app/constants/enums';
import { formatCommon, methodParamsToObject } from 'app/helpers';
import {
  ColumnType,
  Grid,
  TruncateText,
  useTranslation
} from 'app/_libraries/_dls';
import { viewMoreInfo } from 'pages/_commons/Utils/formatGridField';
import React, { useCallback, useMemo } from 'react';
import { parameterGroup, ParameterList } from './newMethodParameterList';

export interface SubRowGridInfoProps {
  original: ParameterList;
  method: WorkflowSetupMethod;
  metadata: {
    lateChargesOption: RefData[];
    calculationBase: RefData[];
    assessmentControl: RefData[];
    balanceIndicator: RefData[];
    assessedAccounts: RefData[];
    currentBalanceAssessment: RefData[];
    calculationDayControl: RefData[];
    nonProcessingLateCharge: RefData[];
    includeExcludeControl: RefData[];
    status1: RefData[];
    status2: RefData[];
    status3: RefData[];
    status4: RefData[];
    status5: RefData[];
    lateChargeResetCounter: RefData[];
    minimumPayDueOptions: RefData[];
    reversalDetailsTextID: RefData[];
    waivedMessageTextID: RefData[];
    latePaymentWarningMessage: RefData[];
  };
}
const SubRowGridInfo: React.FC<SubRowGridInfoProps> = ({
  original,
  method: methodProp,
  metadata
}) => {
  const { t } = useTranslation();

  const method = useMemo(() => methodParamsToObject(methodProp), [methodProp]);
  const data = useMemo(() => parameterGroup[original.id], [original.id]);

  const values = useMemo<Record<string, string | undefined>>(() => {
    const {
      lateChargesOption,
      calculationBase,
      assessmentControl,
      balanceIndicator,
      assessedAccounts,
      currentBalanceAssessment,
      calculationDayControl,
      nonProcessingLateCharge,
      includeExcludeControl,
      status1,
      status2,
      status3,
      status4,
      status5,
      lateChargeResetCounter,
      minimumPayDueOptions,
      reversalDetailsTextID,
      waivedMessageTextID,
      latePaymentWarningMessage
    } = metadata;

    return {
      [MethodFieldParameterEnum.LateChargesOption]: lateChargesOption.find(
        o => o.code === method[MethodFieldParameterEnum.LateChargesOption]
      )?.text,
      [MethodFieldParameterEnum.CalculationBase]: calculationBase.find(
        o => o.code === method[MethodFieldParameterEnum.CalculationBase]
      )?.text,
      [MethodFieldParameterEnum.AssessmentControl]: assessmentControl.find(
        o => o.code === method[MethodFieldParameterEnum.AssessmentControl]
      )?.text,
      [MethodFieldParameterEnum.CyclesOfConsecutiveDelinquency]:
        method[MethodFieldParameterEnum.CyclesOfConsecutiveDelinquency]!,
      [MethodFieldParameterEnum.BalanceIndicator]: balanceIndicator.find(
        o => o.code === method[MethodFieldParameterEnum.BalanceIndicator]
      )?.text,
      [MethodFieldParameterEnum.AssessedAccounts]: assessedAccounts.find(
        o => o.code === method[MethodFieldParameterEnum.AssessedAccounts]
      )?.text,
      [MethodFieldParameterEnum.CurrentBalanceAssessment]:
        currentBalanceAssessment.find(
          o =>
            o.code === method[MethodFieldParameterEnum.CurrentBalanceAssessment]
        )?.text,
      [MethodFieldParameterEnum.CalculationDayControl]:
        calculationDayControl.find(
          o => o.code === method[MethodFieldParameterEnum.CalculationDayControl]
        )?.text,
      [MethodFieldParameterEnum.NonProcessingLateCharge]:
        nonProcessingLateCharge.find(
          o =>
            o.code === method[MethodFieldParameterEnum.NonProcessingLateCharge]
        )?.text,
      [MethodFieldParameterEnum.IncludeExcludeControl]:
        includeExcludeControl.find(
          o => o.code === method[MethodFieldParameterEnum.IncludeExcludeControl]
        )?.text,
      [MethodFieldParameterEnum.Status1]: status1.find(
        o => o.code === method[MethodFieldParameterEnum.Status1]
      )?.text,
      [MethodFieldParameterEnum.Status2]: status2.find(
        o => o.code === method[MethodFieldParameterEnum.Status2]
      )?.text,
      [MethodFieldParameterEnum.Status3]: status3.find(
        o => o.code === method[MethodFieldParameterEnum.Status3]
      )?.text,
      [MethodFieldParameterEnum.Status4]: status4.find(
        o => o.code === method[MethodFieldParameterEnum.Status4]
      )?.text,
      [MethodFieldParameterEnum.Status5]: status5.find(
        o => o.code === method[MethodFieldParameterEnum.Status5]
      )?.text,
      [MethodFieldParameterEnum.LateChargeResetCounter]:
        lateChargeResetCounter.find(
          o =>
            o.code === method[MethodFieldParameterEnum.LateChargeResetCounter]
        )?.text,
      [MethodFieldParameterEnum.MinimumPayDueOptions]:
        minimumPayDueOptions.find(
          o => o.code === method[MethodFieldParameterEnum.MinimumPayDueOptions]
        )?.text,

      [MethodFieldParameterEnum.MaximumAmount]: formatCommon(
        method[MethodFieldParameterEnum.MaximumAmount]!
      ).currency(2),
      [MethodFieldParameterEnum.MaximumYearlyAmount]: formatCommon(
        method[MethodFieldParameterEnum.MaximumYearlyAmount]!
      ).currency(2),
      [MethodFieldParameterEnum.ExclusionBalance]: formatCommon(
        method[MethodFieldParameterEnum.ExclusionBalance]!
      ).currency(0),
      [MethodFieldParameterEnum.NumberOfDays]:
        method[MethodFieldParameterEnum.NumberOfDays],
      [MethodFieldParameterEnum.Tier2Balance]: formatCommon(
        method[MethodFieldParameterEnum.Tier2Balance]!
      ).currency(2),
      [MethodFieldParameterEnum.Tier3Balance]: formatCommon(
        method[MethodFieldParameterEnum.Tier3Balance]!
      ).currency(2),
      [MethodFieldParameterEnum.Tier4Balance]: formatCommon(
        method[MethodFieldParameterEnum.Tier4Balance]!
      ).currency(2),
      [MethodFieldParameterEnum.Tier5Balance]: formatCommon(
        method[MethodFieldParameterEnum.Tier5Balance]!
      ).currency(2),
      [MethodFieldParameterEnum.Tier2Amount]: formatCommon(
        method[MethodFieldParameterEnum.Tier2Amount]!
      ).currency(2),
      [MethodFieldParameterEnum.Tier3Amount]: formatCommon(
        method[MethodFieldParameterEnum.Tier3Amount]!
      ).currency(2),
      [MethodFieldParameterEnum.Tier4Amount]: formatCommon(
        method[MethodFieldParameterEnum.Tier4Amount]!
      ).currency(2),
      [MethodFieldParameterEnum.Tier5Amount]: formatCommon(
        method[MethodFieldParameterEnum.Tier5Amount]!
      ).currency(2),
      [MethodFieldParameterEnum.FixedAmount]: formatCommon(
        method[MethodFieldParameterEnum.FixedAmount]!
      ).currency(2),
      [MethodFieldParameterEnum.Percent]: formatCommon(
        method[MethodFieldParameterEnum.Percent]!
      ).percent(3),
      [MethodFieldParameterEnum.ThresholdToWaveLateCharge]: formatCommon(
        method[MethodFieldParameterEnum.ThresholdToWaveLateCharge]!
      ).currency(2),

      [MethodFieldParameterEnum.ReversalDetailsTextID]:
        reversalDetailsTextID.find(
          o => o.code === method[MethodFieldParameterEnum.ReversalDetailsTextID]
        )?.text,
      [MethodFieldParameterEnum.WaivedMessageTextID]: waivedMessageTextID.find(
        o => o.code === method[MethodFieldParameterEnum.WaivedMessageTextID]
      )?.text,
      [MethodFieldParameterEnum.LatePaymentWarningMessage]:
        latePaymentWarningMessage.find(
          o =>
            o.code ===
            method[MethodFieldParameterEnum.LatePaymentWarningMessage]
        )?.text
    };
  }, [metadata, method]);

  const valueAccessor = useCallback(
    (data: Record<string, any>) => (
      <TruncateText
        lines={2}
        ellipsisLessText={t('txt_less')}
        ellipsisMoreText={t('txt_more')}
      >
        {values[data.id]}
      </TruncateText>
    ),
    [t, values]
  );

  const columns: ColumnType[] = [
    {
      id: 'fieldName',
      Header: t('txt_business_name'),
      accessor: 'fieldName'
    },
    {
      id: 'onlinePCF',
      Header: t('txt_manage_penalty_fee_online_PCF'),
      accessor: 'onlinePCF'
    },
    {
      id: 'id',
      Header: t('txt_value'),
      accessor: valueAccessor
    },
    {
      id: 'moreInfo',
      Header: t('txt_more_info'),
      className: 'text-center',
      accessor: viewMoreInfo(['moreInfo'], t),
      width: 105
    }
  ];

  return (
    <div className="p-20 overflow-hidden">
      <Grid columns={columns} data={data} />
    </div>
  );
};

export default SubRowGridInfo;
