import {
  BadgeColorType,
  MethodFieldParameterEnum,
  ServiceSubjectSection
} from 'app/constants/enums';
import {
  unsavedExistedWorkflowSetupDataProps,
  UNSAVED_CHANGE_NAMES
} from 'app/constants/unsave-changes-form-names';
import { mapGridExpandCollapse } from 'app/helpers';
import { useUnsavedChangeRegistry } from 'app/hooks';
import {
  Button,
  ColumnType,
  Grid,
  InlineMessage,
  useTranslation
} from 'app/_libraries/_dls';
import { isEmpty, orderBy } from 'lodash';
import {
  actionsWorkflowSetup,
  useSelectElementMetadataForLC
} from 'pages/_commons/redux/WorkflowSetup';
import {
  formatBadge,
  formatText,
  formatTruncate
} from 'pages/_commons/Utils/formatGridField';
import { WorkflowSetupStaticProp } from 'pages/_commons/WorkflowSetup/types';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';
import ActionButtons from './ActionButtons';
import AddNewMethodModal from './AddNewMethodModal';
import LateChargesSummary from './LateChargesSummary';
import MethodListModal from './MethodListModal';
import parseFormValues from './parseFormValues';
import RemoveMethodModal from './RemoveMethodModal';
import SubRowGrid from './SubRowGrid';

export interface LateChargesFormValues {
  isValid?: boolean;

  methods?: (WorkflowSetupMethod & { rowId?: number })[];
}
const LateChargesStep: React.FC<WorkflowSetupProps<LateChargesFormValues>> = ({
  stepId,
  selectedStep,
  savedAt,
  isStuck,

  formValues,
  setFormValues
}) => {
  const { t } = useTranslation();

  const keepRef = useRef({ setFormValues });
  keepRef.current.setFormValues = setFormValues;

  const dispatch = useDispatch();
  const [expandedList, setExpandedList] = useState<string[]>([]);
  const [showAddNewMethodModal, setShowAddNewMethodModal] = useState(false);
  const [isCreateNewVersion, setIsCreateNewVersion] = useState(false);
  const [isMethodToModel, setIsMethodToModel] = useState(false);
  const [removeMethodRowId, setRemoveMethodRowId] =
    useState<undefined | number>(undefined);
  const [editMethodRowId, setEditMethodRowId] =
    useState<undefined | number>(undefined);
  const [editMethodBackId, setEditMethodBackId] =
    useState<undefined | number>(undefined);
  const [hasChange, setHasChange] = useState(false);
  const [keepCreateMethodState, setKeepCreateMethodState] =
    useState<any | undefined>(undefined);

  const metadata = useSelectElementMetadataForLC();

  const methods = useMemo(
    () => orderBy(formValues?.methods || [], ['name'], ['asc']),
    [formValues?.methods]
  );
  const methodNames = useMemo(() => methods.map(i => i.name), [methods]);

  const onRemoveMethod = useCallback((idx: number) => {
    setRemoveMethodRowId(idx);
  }, []);

  const onEditMethod = useCallback((idx: number) => {
    setEditMethodRowId(idx);
  }, []);

  const onClickAddNewMethod = () => {
    setShowAddNewMethodModal(true);
  };
  const onClickMethodToModel = () => {
    setIsMethodToModel(true);
  };
  const onClickCreateNewVersion = () => {
    setIsCreateNewVersion(true);
  };
  const closeMethodListModalModal = () => {
    setIsCreateNewVersion(false);
    setIsMethodToModel(false);
  };

  useUnsavedChangeRegistry(
    {
      ...unsavedExistedWorkflowSetupDataProps,
      formName: UNSAVED_CHANGE_NAMES.WORKFLOW_SETUP__MANAGE_PENALTY_FEES__LC,
      priority: 1
    },
    [hasChange]
  );

  const methodTypeToText = useCallback(
    (type: WorkflowSetupMethodType) => {
      switch (type) {
        case 'NEWVERSION':
          return t('txt_manage_penalty_fee_version_created');
        case 'MODELEDMETHOD':
          return t('txt_manage_penalty_fee_method_modeled');
      }
      return t('txt_manage_penalty_fee_method_created');
    },
    [t]
  );

  const columns: ColumnType[] = useMemo(
    () => [
      {
        id: 'methodName',
        Header: t('txt_method_name'),
        accessor: formatText(['name']),
        width: 105
      },
      {
        id: 'modelOrCreateFrom',
        Header: t('txt_manage_penalty_fee_modeled_or_create_from'),
        accessor: formatText(['modeledFrom', 'name']),
        width: 120
      },
      {
        id: 'comment',
        Header: t('txt_comment'),
        accessor: formatTruncate(['comment']),
        width: 312
      },
      {
        id: 'methodType',
        Header: t('txt_manage_penalty_fee_action_taken'),
        accessor: (data, idx) =>
          formatBadge(['methodType'], {
            colorType: BadgeColorType.MethodType,
            noBorder: true
          })(
            {
              ...data,
              methodType: methodTypeToText(data.methodType)
            },
            idx
          ),
        width: 153
      },
      {
        id: 'actions',
        Header: t('txt_actions'),
        accessor: (data: any) => (
          <div className="d-flex justify-content-center">
            <Button
              size="sm"
              variant="outline-primary"
              onClick={() => onEditMethod(data.rowId)}
            >
              {t('txt_edit')}
            </Button>
            <Button
              size="sm"
              variant="outline-danger"
              className="ml-8"
              onClick={() => onRemoveMethod(data.rowId)}
            >
              {t('txt_delete')}
            </Button>
          </div>
        ),
        className: 'text-center',
        cellBodyProps: { className: 'py-8' },
        width: 142
      }
    ],
    [t, methodTypeToText, onEditMethod, onRemoveMethod]
  );

  useEffect(() => {
    dispatch(
      actionsWorkflowSetup.getWorkflowMetadata([
        MethodFieldParameterEnum.LateChargesOption,
        MethodFieldParameterEnum.CalculationBase,
        MethodFieldParameterEnum.MinimumPayDueOptions,
        MethodFieldParameterEnum.AssessmentControl,
        MethodFieldParameterEnum.BalanceIndicator,
        MethodFieldParameterEnum.AssessedAccounts,
        MethodFieldParameterEnum.CurrentBalanceAssessment,
        MethodFieldParameterEnum.CalculationDayControl,
        MethodFieldParameterEnum.NonProcessingLateCharge,
        MethodFieldParameterEnum.IncludeExcludeControl,
        MethodFieldParameterEnum.Status1,
        MethodFieldParameterEnum.Status2,
        MethodFieldParameterEnum.Status3,
        MethodFieldParameterEnum.Status4,
        MethodFieldParameterEnum.Status5,
        MethodFieldParameterEnum.LateChargeResetCounter,
        MethodFieldParameterEnum.ReversalDetailsTextID,
        MethodFieldParameterEnum.WaivedMessageTextID,
        MethodFieldParameterEnum.LatePaymentWarningMessage
      ])
    );
  }, [dispatch]);
  useEffect(() => {
    const isValid = (formValues?.methods?.length || 0) > 0;
    if (formValues.isValid === isValid) return;

    keepRef.current.setFormValues({ isValid });
  }, [formValues]);

  useEffect(() => {
    if (stepId !== selectedStep) {
      setExpandedList([]);
    }
    setHasChange(false);
  }, [stepId, selectedStep, savedAt]);

  const handleOnExpand = (dataKeyList: string[]) => {
    setExpandedList(
      isEmpty(dataKeyList) ? [] : [dataKeyList[dataKeyList.length - 1]]
    );
  };

  const renderGrid = useMemo(() => {
    if (isEmpty(methods)) return;
    return (
      <div className="pt-16">
        <Grid
          columns={columns}
          data={methods}
          subRow={({ original }: any) => (
            <SubRowGrid original={original} metadata={metadata} />
          )}
          togglable
          dataItemKey="rowId"
          toggleButtonConfigList={methods.map(
            mapGridExpandCollapse('rowId', t)
          )}
          expandedItemKey={'rowId'}
          expandedList={expandedList}
          onExpand={handleOnExpand}
          scrollable
        />
      </div>
    );
  }, [columns, expandedList, metadata, methods, t]);

  return (
    <div>
      <div className="mt-8 pb-24 border-bottom color-grey">
        <p>{t('txt_manage_penalty_fee_late_charge_desc_1')}</p>
      </div>
      {isStuck && (
        <InlineMessage className="mb-0 mt-24" variant="danger" withIcon>
          {t('txt_step_stuck_move_forward_message')}
        </InlineMessage>
      )}
      <div className="mt-24">
        <ActionButtons
          small={!isEmpty(methods)}
          title="txt_manage_penalty_fee_late_charge_method_list_title"
          onClickAddNewMethod={onClickAddNewMethod}
          onClickMethodToModel={onClickMethodToModel}
          onClickCreateNewVersion={onClickCreateNewVersion}
        />
      </div>
      {renderGrid}
      {removeMethodRowId !== undefined && (
        <RemoveMethodModal
          id="removeMethod"
          show
          methodName={methods.find(i => i.rowId === removeMethodRowId)?.name}
          onClose={method => {
            if (method) {
              setFormValues({
                methods: methods.filter(i => i.rowId !== removeMethodRowId)
              });
              setHasChange(true);
            }
            setRemoveMethodRowId(undefined);
          }}
        />
      )}
      {editMethodRowId !== undefined && (
        <AddNewMethodModal
          id="addNewMethod"
          show
          method={methods.find(i => i.rowId === editMethodRowId)}
          methodNames={methodNames}
          onClose={(method, isBack, keepState) => {
            if (method) {
              method.serviceSubjectSection = ServiceSubjectSection.LC;
              const newMethds = methods.map(item => {
                if (item.rowId === editMethodRowId) return method;
                return item;
              });
              setFormValues({ methods: newMethds });
              setHasChange(true);

              if (isBack) {
                setEditMethodBackId(editMethodRowId);
                setKeepCreateMethodState(keepState);
              }
            }
            setEditMethodRowId(undefined);
          }}
        />
      )}
      {editMethodBackId && (
        <MethodListModal
          id="editMethodFromModel"
          show
          methodNames={methodNames}
          isCreateNewVersion={
            methods.find(i => i.rowId === editMethodBackId)!.methodType ===
            'NEWVERSION'
          }
          defaultMethod={methods.find(i => i.rowId === editMethodBackId)}
          keepState={keepCreateMethodState}
          onClose={method => {
            if (method) {
              method.serviceSubjectSection = ServiceSubjectSection.LC;
              const newMethds = methods.map(item => {
                if (item.rowId === editMethodBackId) return method;
                return item;
              });
              setFormValues({ methods: newMethds });
              setHasChange(true);
            }
            setEditMethodBackId(undefined);
            setKeepCreateMethodState(undefined);
          }}
        />
      )}
      {(isCreateNewVersion || isMethodToModel) && (
        <MethodListModal
          id="addMethodFromModel"
          show
          methodNames={methodNames}
          isCreateNewVersion={isCreateNewVersion}
          onClose={newMethod => {
            if (newMethod) {
              const rowId = Date.now();
              setFormValues({
                methods: [
                  {
                    ...newMethod,
                    rowId,
                    serviceSubjectSection: ServiceSubjectSection.LC
                  },
                  ...methods
                ]
              });
              setExpandedList([rowId] as any);
              setHasChange(true);
            }
            closeMethodListModalModal();
          }}
        />
      )}
      {showAddNewMethodModal && (
        <AddNewMethodModal
          id="addNewMethod"
          show
          methodNames={methodNames}
          onClose={method => {
            if (method) {
              const rowId = Date.now();
              setFormValues({
                methods: [
                  {
                    ...method,
                    rowId,
                    serviceSubjectSection: ServiceSubjectSection.LC
                  },
                  ...methods
                ]
              });
              setExpandedList([rowId] as any);
              setHasChange(true);
            }
            setShowAddNewMethodModal(false);
          }}
        />
      )}
    </div>
  );
};

const ExtraStaticLateChargesStep = LateChargesStep as WorkflowSetupStaticProp;

ExtraStaticLateChargesStep.summaryComponent = LateChargesSummary;
ExtraStaticLateChargesStep.defaultValues = { isValid: false, methods: [] };
ExtraStaticLateChargesStep.parseFormValues = parseFormValues;

export default ExtraStaticLateChargesStep;
