import {
  MethodFieldNameEnum,
  MethodFieldParameterEnum
} from 'app/constants/enums';
import React from 'react';

export type ParameterListIds =
  | 'Info_AssessmentCalculation'
  | 'Info_AssessmentControls'
  | 'Info_ExternalStatusQualification'
  | 'Adv_AssessmentCalculation'
  | 'Adv_AssessmentControls'
  | 'Adv_StatementMessaging';

export interface ParameterList {
  id: ParameterListIds;
  section:
    | 'txt_manage_penalty_fee_standard_parameters'
    | 'txt_manage_penalty_fee_advanced_parameters';
  parameterGroup:
    | 'txt_manage_penalty_fee_assessment_calculation'
    | 'txt_manage_penalty_fee_assessment_controls'
    | 'txt_manage_penalty_fee_external_status_qualification'
    | 'txt_manage_penalty_fee_statement_messaging';
  moreInfo?: React.ReactNode;
}

export const parameterList: ParameterList[] = [
  {
    id: 'Info_AssessmentCalculation',
    section: 'txt_manage_penalty_fee_standard_parameters',
    parameterGroup: 'txt_manage_penalty_fee_assessment_calculation',
    moreInfo: 'txt_manage_penalty_fee_late_charges_advanced_parameters'
  },
  {
    id: 'Info_AssessmentControls',
    section: 'txt_manage_penalty_fee_standard_parameters',
    parameterGroup: 'txt_manage_penalty_fee_assessment_controls',
    moreInfo: 'txt_manage_penalty_fee_assessment_controls_info_desc'
  },
  {
    id: 'Info_ExternalStatusQualification',
    section: 'txt_manage_penalty_fee_standard_parameters',
    parameterGroup: 'txt_manage_penalty_fee_external_status_qualification',
    moreInfo: 'txt_manage_penalty_fee_external_status_qualification_info_desc'
  },
  {
    id: 'Adv_AssessmentCalculation',
    section: 'txt_manage_penalty_fee_advanced_parameters',
    parameterGroup: 'txt_manage_penalty_fee_assessment_calculation'
  },
  {
    id: 'Adv_AssessmentControls',
    section: 'txt_manage_penalty_fee_advanced_parameters',
    parameterGroup: 'txt_manage_penalty_fee_assessment_controls'
  },
  {
    id: 'Adv_StatementMessaging',
    section: 'txt_manage_penalty_fee_advanced_parameters',
    parameterGroup: 'txt_manage_penalty_fee_statement_messaging'
  }
];

export interface ParameterGroup {
  id: MethodFieldParameterEnum;
  fieldName: MethodFieldNameEnum;
  onlinePCF: string;
  moreInfoText: string;
  moreInfo: React.ReactNode;
}
export const parameterGroup: Record<ParameterListIds, ParameterGroup[]> = {
  Info_AssessmentCalculation: [
    {
      id: MethodFieldParameterEnum.LateChargesOption,
      fieldName: MethodFieldNameEnum.LateChargesOption,
      onlinePCF: MethodFieldNameEnum.LateChargesOption,
      moreInfoText:
        "Choose how to assess late charges for cardholders. If you select 'T', use the Advanced Settings to enter values for each tier.",
      moreInfo:
        "Choose how to assess late charges for cardholders. If you select 'T', use the Advanced Settings to enter values for each tier."
    },
    {
      id: MethodFieldParameterEnum.CalculationBase,
      fieldName: MethodFieldNameEnum.CalculationBase,
      onlinePCF: 'Group Identifier',
      moreInfoText:
        'Select an option for how to calculate a percentage-based late charge, and what to multiply the fee percentage by.',
      moreInfo:
        'Select an option for how to calculate a percentage-based late charge, and what to multiply the fee percentage by.'
    },
    {
      id: MethodFieldParameterEnum.MinimumPayDueOptions,
      fieldName: MethodFieldNameEnum.MinimumPayDueOptions,
      onlinePCF: 'LC MPD CD',
      moreInfoText:
        'The Late Charge MinPayDue Code determines the minimum payment due value used to calculate the late charge amount. Consult your legal counsel and compliance partners before changing this setting.',
      moreInfo:
        'The Late Charge MinPayDue Code determines the minimum payment due value used to calculate the late charge amount. Consult your legal counsel and compliance partners before changing this setting.'
    },
    {
      id: MethodFieldParameterEnum.FixedAmount,
      fieldName: MethodFieldNameEnum.MinimumOrFixedAmount,
      onlinePCF: MethodFieldNameEnum.MinimumOrFixedAmount,
      moreInfoText:
        'Enter the fixed amount billed to the cardholder when a late charge is assessed. Up to two decimal places can be entered.',
      moreInfo:
        'Enter the fixed amount billed to the cardholder when a late charge is assessed. Up to two decimal places can be entered.'
    },
    {
      id: MethodFieldParameterEnum.Percent,
      fieldName: MethodFieldNameEnum.Percent,
      onlinePCF: MethodFieldNameEnum.Percent,
      moreInfoText:
        'Enter a percentage determined in the “Calculation Base” field which will be charged to the customer when a late charge is assessed to the cardholder. Enter up to three decimal places.',
      moreInfo:
        'Enter a percentage determined in the “Calculation Base” field which will be charged to the customer when a late charge is assessed to the cardholder. Enter up to three decimal places.'
    },
    {
      id: MethodFieldParameterEnum.MaximumAmount,
      fieldName: MethodFieldNameEnum.MaximumAmount,
      onlinePCF: MethodFieldNameEnum.MaximumAmount,
      moreInfoText:
        'If you calculate charges using a percentage-based value, enter the maximum amount that can be assessed for the late charge.',
      moreInfo:
        'If you calculate charges using a percentage-based value, enter the maximum amount that can be assessed for the late charge.'
    },
    {
      id: MethodFieldParameterEnum.MaximumYearlyAmount,
      fieldName: MethodFieldNameEnum.MaximumYearlyAmount,
      onlinePCF: MethodFieldNameEnum.MaximumYearlyAmount,
      moreInfoText:
        'Enter the maximum value of all late charges that can be assessed on an account in a calendar year. This field does not apply to accounts whose late charges are a fixed amount. If using a tiered structure for late charges, ensure this amount is equal to or greater than the amount entered for the last tier. Tier amounts are entered under Advanced Settings.',
      moreInfo:
        'Enter the maximum value of all late charges that can be assessed on an account in a calendar year. This field does not apply to accounts whose late charges are a fixed amount. If using a tiered structure for late charges, ensure this amount is equal to or greater than the amount entered for the last tier. Tier amounts are entered under Advanced Settings.'
    }
  ],
  Info_AssessmentControls: [
    {
      id: MethodFieldParameterEnum.AssessmentControl,
      fieldName: MethodFieldNameEnum.AssessmentControl,
      onlinePCF: MethodFieldNameEnum.AssessmentControl,
      moreInfoText: 'Select how late charges are assessed.',
      moreInfo: 'Select how late charges are assessed.'
    },
    {
      id: MethodFieldParameterEnum.CyclesOfConsecutiveDelinquency,
      fieldName: MethodFieldNameEnum.CyclesOfConsecutiveDelinquency,
      onlinePCF: MethodFieldNameEnum.CyclesOfConsecutiveDelinquency,
      moreInfoText:
        'Select whether late charges are assessed when the balance is entirely late charges.',
      moreInfo:
        'Select whether late charges are assessed when the balance is entirely late charges.'
    },
    {
      id: MethodFieldParameterEnum.BalanceIndicator,
      fieldName: MethodFieldNameEnum.BalanceIndicator,
      onlinePCF: 'Late Charge Balance Indicator',
      moreInfoText:
        'Select whether late charges are assessed when the balance is entirely late charges.',
      moreInfo:
        'Select whether late charges are assessed when the balance is entirely late charges.'
    },
    {
      id: MethodFieldParameterEnum.AssessedAccounts,
      fieldName: MethodFieldNameEnum.AssessedAccounts,
      onlinePCF: MethodFieldNameEnum.AssessedAccounts,
      moreInfoText: 'Select when late charges are assessed.',
      moreInfo: 'Select when late charges are assessed.'
    },
    {
      id: MethodFieldParameterEnum.ExclusionBalance,
      fieldName: MethodFieldNameEnum.ExclusionBalance,
      onlinePCF: MethodFieldNameEnum.ExclusionBalance,
      moreInfoText:
        'An account balance must be less than the Exclusion Balance amount to avoid a late charge assessment. You can use this parameter only if you set the Assessed Accounts parameter to 3. You must set the Assessment Control parameter if you set this parameter to an amount great than 0. This parameter works in conjunction with the Current Balance Assessment parameter. If the balance you select using the Current Balance Assessment parameter is greater than the amount set in this parameter, the system assesses late charges.',
      moreInfo: (
        <div>
          <p>
            An account balance must be less than the Exclusion Balance amount to
            avoid a late charge assessment. You can use this parameter only if
            you set the Assessed Accounts parameter to 3.
          </p>
          <p className="mt-8">
            You must set the Assessment Control parameter if you set this
            parameter to an amount great than 0. This parameter works in
            conjunction with the Current Balance Assessment parameter. If the
            balance you select using the Current Balance Assessment parameter is
            greater than the amount set in this parameter, the system assesses
            late charges.
          </p>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.CurrentBalanceAssessment,
      fieldName: MethodFieldNameEnum.CurrentBalanceAssessment,
      onlinePCF: MethodFieldNameEnum.CurrentBalanceAssessment,
      moreInfoText:
        'Select whether to assess late charges based on the current balance or the previous statement balance. This parameter is available only when the Assessed Accounts parameter is 3. You must also enter a value in the Exclusion Balance parameter.',
      moreInfo:
        'Select whether to assess late charges based on the current balance or the previous statement balance. This parameter is available only when the Assessed Accounts parameter is 3. You must also enter a value in the Exclusion Balance parameter.'
    },
    {
      id: MethodFieldParameterEnum.CalculationDayControl,
      fieldName: MethodFieldNameEnum.CalculationDayControl,
      onlinePCF: MethodFieldNameEnum.CalculationDayControl,
      moreInfoText:
        'The Calculation Day Control parameter determines the point at which the System calculates late charges on accounts if the minimum payment due has not been met. You can have the System calculate late charges a set number of days after the billing cycle, or a set number of days after the payment due date. When you set this parameter to zero, the System adds late charge days to the billing cycle. When you set this parameter to a number other than zero, the System adds late charge days to the payment due date.',
      moreInfo:
        'The Calculation Day Control parameter determines the point at which the System calculates late charges on accounts if the minimum payment due has not been met. You can have the System calculate late charges a set number of days after the billing cycle, or a set number of days after the payment due date. When you set this parameter to zero, the System adds late charge days to the billing cycle. When you set this parameter to a number other than zero, the System adds late charge days to the payment due date.'
    },
    {
      id: MethodFieldParameterEnum.NumberOfDays,
      fieldName: MethodFieldNameEnum.NumberOfDays,
      onlinePCF: MethodFieldNameEnum.NumberOfDays,
      moreInfoText:
        'Warning! Be aware that changing the setting in the Number Of Days parameter to shorten the grace period may result in a loss of revenue. Enter the number of days after the billing cycle or payment due date that late charges are generated. The Calculation Day Control parameter setting determines whether late charges are generated after the billing cycle or after the payment date. For example, assume the Calculation Day Control parameter is 0 and the Number of Days parameter is 10. If an account cycles with a delinquent status, then the account’s delinquency status is checked the 10th day after the billing cycle. A late charge is generated if the account is still delinquent but not if the delinquency has been paid off. This gives the customer a grace period in which to pay off a delinquency and avoid being assessed a late charge. A value of 00 generates late charges on the payment due date calculated after the system changes the Payment Due to Non-Bank Holiday parameter in the Statement Production section or when the account cycles, depending on the option you are using. Valid values are 00 through 30.',
      moreInfo: (
        <div>
          <p className="fw-600">Warning! </p>
          <p className="fw-600 pb-8">
            Be aware that changing the setting in the Number Of Days parameter
            to shorten the grace period may result in a loss of revenue.
          </p>
          <p className="pb-8">
            Enter the number of days after the billing cycle or payment due date
            that late charges are generated. The Calculation Day Control
            parameter setting determines whether late charges are generated
            after the billing cycle or after the payment date.
          </p>
          <p className="pb-8">
            For example, assume the Calculation Day Control parameter is 0 and
            the Number of Days parameter is 10. If an account cycles with a
            delinquent status, then the account’s delinquency status is checked
            the 10th day after the billing cycle. A late charge is generated if
            the account is still delinquent but not if the delinquency has been
            paid off. This gives the customer a grace period in which to pay off
            a delinquency and avoid being assessed a late charge.
          </p>
          <p>
            A value of 00 generates late charges on the payment due date
            calculated after the system changes the Payment Due to Non-Bank
            Holiday parameter in the Statement Production section or when the
            account cycles, depending on the option you are using. Valid values
            are 00 through 30.
          </p>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.NonProcessingLateCharge,
      fieldName: MethodFieldNameEnum.NonProcessingLateCharge,
      onlinePCF: MethodFieldNameEnum.NonProcessingLateCharge,
      moreInfoText:
        'The Non-Processing Late Charge field determines whether to assess a late fee on the previous processing day or the next processing day when the late fee assessment date falls on a non-processing day.',
      moreInfo:
        'The Non-Processing Late Charge field determines whether to assess a late fee on the previous processing day or the next processing day when the late fee assessment date falls on a non-processing day.'
    }
  ],
  Info_ExternalStatusQualification: [
    {
      id: MethodFieldParameterEnum.IncludeExcludeControl,
      fieldName: MethodFieldNameEnum.IncludeExcludeControl,
      onlinePCF: MethodFieldNameEnum.IncludeExcludeControl,
      moreInfoText:
        'This field determines how accounts are qualified for late charges based on the external status code assigned to the account at the time of the statement cycle, after any non-monetary or monetary transactions have executed for the account. Select Include to only assess late charges against accounts with the entered External Status Code values. Select Exclude to not assess late charges against accounts with the entered External Status Code values.',
      moreInfo: (
        <div>
          <p>
            This field determines how accounts are qualified for late charges
            based on the external status code assigned to the account at the
            time of the statement cycle, after any non-monetary or monetary
            transactions have executed for the account.
          </p>
          <p className="mt-8">
            Select Include to only assess late charges against accounts with the
            entered External Status Code values. Select Exclude to not assess
            late charges against accounts with the entered External Status Code
            values.
          </p>
        </div>
      )
    },
    {
      id: MethodFieldParameterEnum.Status1,
      fieldName: MethodFieldNameEnum.Status1,
      onlinePCF: 'Include/Exclude Status Code 1',
      moreInfoText:
        'Select up to five External Status Codes to include or exclude accounts.',
      moreInfo:
        'Select up to five External Status Codes to include or exclude accounts.'
    },
    {
      id: MethodFieldParameterEnum.Status2,
      fieldName: MethodFieldNameEnum.Status2,
      onlinePCF: 'Include/Exclude Status Code 2',
      moreInfoText:
        'Select up to five External Status Codes to include or exclude accounts.',
      moreInfo:
        'Select up to five External Status Codes to include or exclude accounts.'
    },
    {
      id: MethodFieldParameterEnum.Status3,
      fieldName: MethodFieldNameEnum.Status3,
      onlinePCF: 'Include/Exclude Status Code 3',
      moreInfoText:
        'Select up to five External Status Codes to include or exclude accounts.',
      moreInfo:
        'Select up to five External Status Codes to include or exclude accounts.'
    },
    {
      id: MethodFieldParameterEnum.Status4,
      fieldName: MethodFieldNameEnum.Status4,
      onlinePCF: 'Include/Exclude Status Code 4',
      moreInfoText:
        'Select up to five External Status Codes to include or exclude accounts.',
      moreInfo:
        'Select up to five External Status Codes to include or exclude accounts.'
    },
    {
      id: MethodFieldParameterEnum.Status5,
      fieldName: MethodFieldNameEnum.Status5,
      onlinePCF: 'Include/Exclude Status Code 5',
      moreInfoText:
        'Select up to five External Status Codes to include or exclude accounts.',
      moreInfo:
        'Select up to five External Status Codes to include or exclude accounts.'
    }
  ],
  Adv_AssessmentCalculation: [
    {
      id: MethodFieldParameterEnum.Tier2Balance,
      fieldName: MethodFieldNameEnum.Tier2Balance,
      onlinePCF: MethodFieldNameEnum.Tier2Balance,
      moreInfoText:
        'The Tier 2 Balance through Tier 5 Balance fields contain the account balance amounts used to determine the late fee for each tier.',
      moreInfo:
        'The Tier 2 Balance through Tier 5 Balance fields contain the account balance amounts used to determine the late fee for each tier.'
    },
    {
      id: MethodFieldParameterEnum.Tier2Amount,
      fieldName: MethodFieldNameEnum.Tier2Amount,
      onlinePCF: MethodFieldNameEnum.Tier2Amount,
      moreInfoText:
        'The Tier 2 Amount through Tier 5 Amount fields contain the late fee amounts you assign to each balance tier.',
      moreInfo:
        'The Tier 2 Amount through Tier 5 Amount fields contain the late fee amounts you assign to each balance tier.'
    },
    {
      id: MethodFieldParameterEnum.Tier3Balance,
      fieldName: MethodFieldNameEnum.Tier3Balance,
      onlinePCF: MethodFieldNameEnum.Tier3Balance,
      moreInfoText:
        'The Tier 2 Balance through Tier 5 Balance fields contain the account balance amounts used to determine the late fee for each tier.',
      moreInfo:
        'The Tier 2 Balance through Tier 5 Balance fields contain the account balance amounts used to determine the late fee for each tier.'
    },
    {
      id: MethodFieldParameterEnum.Tier3Amount,
      fieldName: MethodFieldNameEnum.Tier3Amount,
      onlinePCF: MethodFieldNameEnum.Tier3Amount,
      moreInfoText:
        'The Tier 2 Amount through Tier 5 Amount fields contain the late fee amounts you assign to each balance tier.',
      moreInfo:
        'The Tier 2 Amount through Tier 5 Amount fields contain the late fee amounts you assign to each balance tier.'
    },
    {
      id: MethodFieldParameterEnum.Tier4Balance,
      fieldName: MethodFieldNameEnum.Tier4Balance,
      onlinePCF: MethodFieldNameEnum.Tier4Balance,
      moreInfoText:
        'The Tier 2 Balance through Tier 5 Balance fields contain the account balance amounts used to determine the late fee for each tier.',
      moreInfo:
        'The Tier 2 Balance through Tier 5 Balance fields contain the account balance amounts used to determine the late fee for each tier.'
    },
    {
      id: MethodFieldParameterEnum.Tier4Amount,
      fieldName: MethodFieldNameEnum.Tier4Amount,
      onlinePCF: MethodFieldNameEnum.Tier4Amount,
      moreInfoText:
        'The Tier 2 Amount through Tier 5 Amount fields contain the late fee amounts you assign to each balance tier.',
      moreInfo:
        'The Tier 2 Amount through Tier 5 Amount fields contain the late fee amounts you assign to each balance tier.'
    },
    {
      id: MethodFieldParameterEnum.Tier5Balance,
      fieldName: MethodFieldNameEnum.Tier5Balance,
      onlinePCF: MethodFieldNameEnum.Tier5Balance,
      moreInfoText:
        'The Tier 2 Balance through Tier 5 Balance fields contain the account balance amounts used to determine the late fee for each tier.',
      moreInfo:
        'The Tier 2 Balance through Tier 5 Balance fields contain the account balance amounts used to determine the late fee for each tier.'
    },
    {
      id: MethodFieldParameterEnum.Tier5Amount,
      fieldName: MethodFieldNameEnum.Tier5Amount,
      onlinePCF: MethodFieldNameEnum.Tier5Amount,
      moreInfoText:
        'The Tier 2 Amount through Tier 5 Amount fields contain the late fee amounts you assign to each balance tier.',
      moreInfo:
        'The Tier 2 Amount through Tier 5 Amount fields contain the late fee amounts you assign to each balance tier.'
    }
  ],
  Adv_AssessmentControls: [
    {
      id: MethodFieldParameterEnum.ThresholdToWaveLateCharge,
      fieldName: MethodFieldNameEnum.ThresholdToWaveLateCharge,
      onlinePCF: 'Threshold Waive Amount',
      moreInfoText:
        "Enter the amount that the sum of payments must reach to waive the assessment of a late charge. The Threshold Waive Fixed Amount parameter must be less than the minimum payment due (MPD) amount. The system applies the threshold amount to the account's MPD calculated by your settings in the PCF. To set Threshold Waive Fixed Amount parameter to a value greater than zero, you must set the Assessed Accounts parameter to 8.",
      moreInfo:
        "Enter the amount that the sum of payments must reach to waive the assessment of a late charge. The Threshold Waive Fixed Amount parameter must be less than the minimum payment due (MPD) amount. The system applies the threshold amount to the account's MPD calculated by your settings in the PCF. To set Threshold Waive Fixed Amount parameter to a value greater than zero, you must set the Assessed Accounts parameter to 8."
    },
    {
      id: MethodFieldParameterEnum.LateChargeResetCounter,
      fieldName: MethodFieldNameEnum.LateChargeResetCounter,
      onlinePCF: 'Late Fee Rest CD',
      moreInfoText:
        'Determines when the consecutive late fee count is set back to zero. There is a dependency on what the issuer entered into the LATE CHARGE CYCLES OF CONSECUTIVE DELINQUENCY.',
      moreInfo:
        'Determines when the consecutive late fee count is set back to zero. There is a dependency on what the issuer entered into the LATE CHARGE CYCLES OF CONSECUTIVE DELINQUENCY.'
    }
  ],
  Adv_StatementMessaging: [
    {
      id: MethodFieldParameterEnum.ReversalDetailsTextID,
      fieldName: MethodFieldNameEnum.ReversalDetailsTextID,
      onlinePCF: 'Reversal Detail',
      moreInfoText:
        'When you have a late fee reversed, this is the text that appears on the Statement message. Default text ID is LATE CHG REVERSAL DEFFR001.',
      moreInfo:
        'When you have a late fee reversed, this is the text that appears on the Statement message. Default text ID is LATE CHG REVERSAL DEFFR001.'
    },
    {
      id: MethodFieldParameterEnum.WaivedMessageTextID,
      fieldName: MethodFieldNameEnum.WaivedMessageTextID,
      onlinePCF: 'Waived Message',
      moreInfoText:
        'When you have a late fee reversed, this is the text that appears on the Statement message.',
      moreInfo:
        'When you have a late fee reversed, this is the text that appears on the Statement message.'
    },
    {
      id: MethodFieldParameterEnum.LatePaymentWarningMessage,
      fieldName: MethodFieldNameEnum.LatePaymentWarningMessage,
      onlinePCF: MethodFieldNameEnum.LatePaymentWarningMessage,
      moreInfoText:
        'You can use the default text ID TEXTDFLW of the LW, Late payment warning messages text area, to create your late payment warning messages up to 15 lines in length.',
      moreInfo:
        'You can use the default text ID TEXTDFLW of the LW, Late payment warning messages text area, to create your late payment warning messages up to 15 lines in length.'
    }
  ]
};
