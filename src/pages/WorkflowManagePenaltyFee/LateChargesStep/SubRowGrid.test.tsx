import { render } from '@testing-library/react';
import React from 'react';
import SubRowGrid from './SubRowGrid';

jest.mock('app/_libraries/_dof/core', () => ({
  View: ({ formKey, descriptor, onClick }: any) => {
    return (
      <div id={formKey}>
        {formKey}
        <span>{descriptor}</span>
      </div>
    );
  }
}));

describe('ManagePenaltyFeeWorkflow > LateChargesStep > SubRowGrid', () => {
  const props = {
    metadata: {
      lateChargesOption: [
        {
          code: 'code',
          text: 'text'
        }
      ],
      calculationBase: [
        {
          code: 'code',
          text: 'text'
        }
      ],
      assessmentControl: [
        {
          code: 'code',
          text: 'text'
        }
      ],
      cyclesOfConsecutiveDelinquency: [
        {
          code: 'code',
          text: 'text'
        }
      ],
      balanceIndicator: [
        {
          code: 'code',
          text: 'text'
        }
      ],
      assessedAccounts: [
        {
          code: 'code',
          text: 'text'
        }
      ],
      currentBalanceAssessment: [
        {
          code: 'code',
          text: 'text'
        }
      ],
      calculationDayControl: [
        {
          code: 'code',
          text: 'text'
        }
      ],
      nonProcessingLateCharge: [
        {
          code: 'code',
          text: 'text'
        }
      ],
      includeExcludeControl: [
        {
          code: 'code',
          text: 'text'
        }
      ],
      status1: [
        {
          code: 'code',
          text: 'text'
        }
      ],
      status2: [
        {
          code: 'code',
          text: 'text'
        }
      ],
      status3: [
        {
          code: 'code',
          text: 'text'
        }
      ],
      status4: [
        {
          code: 'code',
          text: 'text'
        }
      ],
      status5: [
        {
          code: 'code',
          text: 'text'
        }
      ],
      lateChargeResetCounter: [
        {
          code: 'code',
          text: 'text'
        }
      ],
      minimumPayDueOptions: [
        {
          code: 'code',
          text: 'text'
        }
      ],
      reversalDetailsTextID: [
        {
          code: 'code',
          text: 'text'
        }
      ],
      waivedMessageTextID: [
        {
          code: 'code',
          text: 'text'
        }
      ],
      latePaymentWarningMessage: [
        {
          code: 'code',
          text: 'text'
        }
      ]
    },
    original: {
      rowId: 1
    }
  };

  it('should render ', () => {
    const wrapper = render(<SubRowGrid {...props} />);
    expect(
      wrapper.getAllByText('txt_manage_penalty_fee_advanced_parameters')[0]
    ).toBeInTheDocument();
  });

  it('should render within tier', () => {
    const wrapper = render(
      <SubRowGrid
        {...props}
        original={
          {
            'tier.3.balance': '1',
            'tier.3.amount': '10',
            'tier.4.balance': '1',
            'tier.4.amount': '10',
            'tier.5.balance': '1',
            'tier.5.amount': '10',
            rowId: 1
          } as any
        }
      />
    );
    expect(
      wrapper.getAllByText('txt_manage_penalty_fee_advanced_parameters')[0]
    ).toBeInTheDocument();
  });
});
