import React from 'react';
import ReactDOM from 'react-dom';

jest.mock('react-dom', () => ({
  render: jest.fn()
}));

jest.mock('./App', () => ({ __esModule: true, default: () => <div>App</div> }));

describe('Application root', () => {
  it('should render UI without crashing', async () => {
    const div = document.createElement('div');
    div.id = 'root';
    document.body.appendChild(div);
    require('./index');
    expect(ReactDOM.render).toBeCalled();
  });
});
