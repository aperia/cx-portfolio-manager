import { AlertMessageControlProps } from 'app/components/DofControl/AlertMessageControl';
import { BadgeGroupTextControlProps } from 'app/components/DofControl/BadgeGroupTextControl';
import { BubbleGroupTextControlProps } from 'app/components/DofControl/BubbleGroupTextControl';
import { ButtonControlProps } from 'app/components/DofControl/ButtonControl';
import { CheckBoxControlProps } from 'app/components/DofControl/CheckBoxControl';
import { ComboBoxControlProps } from 'app/components/DofControl/ComboBoxControl';
import { DatePickerControlProps } from 'app/components/DofControl/DatePickerControl';
import { DivControlProps } from 'app/components/DofControl/DivControl';
import { DividerControlProps } from 'app/components/DofControl/DividerControl';
import { DropDownControlProps } from 'app/components/DofControl/DropDownControl';
import { EmptyControlProps } from 'app/components/DofControl/EmptyControl';
import { GroupMethodCardTextButtonControlProps } from 'app/components/DofControl/GroupMethodCardTextButtonControl';
import { GroupTextControlProps } from 'app/components/DofControl/GroupTextControl';
import { GroupTextMoreControlProps } from 'app/components/DofControl/GroupTextMoreControl';
import { HeadingControlProps } from 'app/components/DofControl/HeadingControl';
import { IconControlProps } from 'app/components/DofControl/IconControl';
import { IconWithValControlProps } from 'app/components/DofControl/IconWithValControl';
import { ImageControlProps } from 'app/components/DofControl/ImageControl';
import { MaskedTextBoxControlProps } from 'app/components/DofControl/MaskedTextBoxControl';
import { NumericControlProps } from 'app/components/DofControl/NumericControl';
import { TextAreaControlProps } from 'app/components/DofControl/TextAreaControl';
import { TextBoxControlProps } from 'app/components/DofControl/TextBoxControl';
import { controlRegistry, layoutRegistry } from 'app/_libraries/_dof/core';
/**
 * Onchange funcs registries
 */
// import 'dof/custom-functions';

/**
 * Layout Registries
 */
layoutRegistry.registerLayout(
  'DynamicBootstrapGridLayout',
  import(
    'app/components/DofControl/DynamicBootstrapGridLayout' /* webpackChunkName: 'custom-controls' */
  ).then(imports => imports.default)
);

/**
 * Control registries
 */
controlRegistry.registerControl<HeadingControlProps>('HeadingControl', {
  promise: () =>
    import(
      'app/components/DofControl/HeadingControl' /* webpackChunkName: 'custom-controls' */
    ).then(imports => imports.default)
});

controlRegistry.registerControl<GroupTextControlProps>('GroupTextControl', {
  promise: () =>
    import(
      'app/components/DofControl/GroupTextControl' /* webpackChunkName: 'custom-controls' */
    ).then(imports => imports.default)
});

controlRegistry.registerControl<DivControlProps>('DivControl', {
  promise: () =>
    import(
      'app/components/DofControl/DivControl' /* webpackChunkName: 'custom-controls' */
    ).then(imports => imports.default)
});

controlRegistry.registerControl<DividerControlProps>('DividerControl', {
  promise: () =>
    import(
      'app/components/DofControl/DividerControl' /* webpackChunkName: 'custom-controls' */
    ).then(imports => imports.default)
});

controlRegistry.registerControl<DatePickerControlProps>('DatePickerControl', {
  promise: () =>
    import(
      'app/components/DofControl/DatePickerControl' /* webpackChunkName: 'custom-controls' */
    ).then(imports => imports.default)
});

controlRegistry.registerControl<ComboBoxControlProps>('ComboBoxControl', {
  promise: () =>
    import(
      'app/components/DofControl/ComboBoxControl' /* webpackChunkName: 'custom-controls' */
    ).then(imports => imports.default)
});

controlRegistry.registerControl<DropDownControlProps>('DropDownControl', {
  promise: () =>
    import(
      'app/components/DofControl/DropDownControl' /* webpackChunkName: 'custom-controls' */
    ).then(imports => imports.default)
});

controlRegistry.registerControl<CheckBoxControlProps>('CheckBoxControl', {
  promise: () =>
    import(
      'app/components/DofControl/CheckBoxControl' /* webpackChunkName: 'custom-controls' */
    ).then(imports => imports.default)
});

controlRegistry.registerControl<TextBoxControlProps>('TextBoxControl', {
  promise: () =>
    import(
      'app/components/DofControl/TextBoxControl' /* webpackChunkName: 'custom-controls' */
    ).then(imports => imports.default)
});

controlRegistry.registerControl<NumericControlProps>('NumericControl', {
  promise: () =>
    import(
      'app/components/DofControl/NumericControl' /* webpackChunkName: 'custom-controls' */
    ).then(imports => imports.default)
});

controlRegistry.registerControl<MaskedTextBoxControlProps>(
  'MaskedTextBoxControl',
  {
    promise: () =>
      import(
        'app/components/DofControl/MaskedTextBoxControl' /* webpackChunkName: 'custom-controls' */
      ).then(imports => imports.default)
  }
);

controlRegistry.registerControl<TextAreaControlProps>('TextAreaControl', {
  promise: () =>
    import(
      'app/components/DofControl/TextAreaControl' /* webpackChunkName: 'custom-controls' */
    ).then(imports => imports.default)
});

controlRegistry.registerControl<AlertMessageControlProps>(
  'AlertMessageControl',
  {
    promise: () =>
      import(
        'app/components/DofControl/AlertMessageControl' /* webpackChunkName: 'custom-controls' */
      ).then(imports => imports.default)
  }
);

controlRegistry.registerControl<ImageControlProps>('ImageControl', {
  promise: () =>
    import(
      'app/components/DofControl/ImageControl' /* webpackChunkName: 'custom-controls' */
    ).then(imports => imports.default)
});

controlRegistry.registerControl<ButtonControlProps>('ButtonControl', {
  promise: () =>
    import(
      'app/components/DofControl/ButtonControl' /* webpackChunkName: 'custom-controls' */
    ).then(imports => imports.default)
});

controlRegistry.registerControl<EmptyControlProps>('EmptyControl', {
  promise: () =>
    import(
      'app/components/DofControl/EmptyControl' /* webpackChunkName: 'custom-controls' */
    ).then(imports => imports.default)
});

controlRegistry.registerControl<BadgeGroupTextControlProps>(
  'BadgeGroupTextControl',
  {
    promise: () =>
      import(
        'app/components/DofControl/BadgeGroupTextControl' /* webpackChunkName: 'custom-controls' */
      ).then(imports => imports.default)
  }
);

controlRegistry.registerControl<BubbleGroupTextControlProps>(
  'BubbleGroupTextControl',
  {
    promise: () =>
      import(
        'app/components/DofControl/BubbleGroupTextControl' /* webpackChunkName: 'custom-controls' */
      ).then(imports => imports.default)
  }
);

controlRegistry.registerControl<GroupTextMoreControlProps>(
  'GroupTextMoreControl',
  {
    promise: () =>
      import(
        'app/components/DofControl/GroupTextMoreControl' /* webpackChunkName: 'custom-controls' */
      ).then(imports => imports.default)
  }
);

controlRegistry.registerControl<IconControlProps>('IconControl', {
  promise: () =>
    import(
      'app/components/DofControl/IconControl' /* webpackChunkName: 'custom-controls' */
    ).then(imports => imports.default)
});

controlRegistry.registerControl<IconWithValControlProps>('IconWithValControl', {
  promise: () =>
    import(
      'app/components/DofControl/IconWithValControl' /* webpackChunkName: 'custom-controls' */
    ).then(imports => imports.default)
});

controlRegistry.registerControl<IconControlProps>('WorkflowCardIconControl', {
  promise: () =>
    import(
      'app/components/DofControl/WorkflowCardIconControl' /* webpackChunkName: 'custom-controls' */
    ).then(imports => imports.default)
});

controlRegistry.registerControl<GroupMethodCardTextButtonControlProps>(
  'GroupMethodCardTextButtonControl',
  {
    promise: () =>
      import(
        'app/components/DofControl/GroupMethodCardTextButtonControl' /* webpackChunkName: 'custom-controls' */
      ).then(imports => imports.default)
  }
);
