import {
  BASE_URL,
  CHANGE_DETAIL_URL,
  CHANGE_MANAGEMENT_URL,
  MY_IN_PROGRESS_WORKFLOWS_URL,
  MY_PORTFOLIOS_DETAIL_URL,
  MY_PORTFOLIOS_URL,
  PENDING_APPROVALS_URL,
  WORKFLOWS_URL
} from 'app/constants/links';
import ChangeDetail from 'pages/ChangeDetail';
import ChangeManagement from 'pages/ChangeManagement';
// components
import Home from 'pages/Home';
import MyInProgressWorkflow from 'pages/MyInProgressWorkflows';
import MyPortfolios from 'pages/MyPortfolios';
import MyPortfoliosDetail from 'pages/MyPortfoliosDetail';
import PendingApprovals from 'pages/PendingApprovals';
import TemplateWorkflows from 'pages/TemplateWorkflows';
import ChangeNotFoundModal from 'pages/_commons/ChangeNotFoundModal';
import Header from 'pages/_commons/Header';
import InfoBar from 'pages/_commons/InfoBar';
import Logger from 'pages/_commons/Logger';
import NotificationProvider from 'pages/_commons/NotiticationProvider';
import { useWorkflowSetup } from 'pages/_commons/redux/WorkflowSetup';
import ToastNotifications from 'pages/_commons/ToastNotifications';
import UnsavedConfirmModal from 'pages/_commons/UnsavedConfirmModal';
import WorkflowSetup from 'pages/_commons/WorkflowSetup';
import React from 'react';
import { HashRouter, Redirect, Route, Switch } from 'react-router-dom';

export interface AppContainerProps {}
const AppContainer: React.FC<AppContainerProps> = props => {
  const { isTemplate, workflow, onSubmitted } = useWorkflowSetup();

  return (
    <HashRouter basename={BASE_URL}>
      <NotificationProvider>
        <Header logo={`./images/remedy.svg`} />
        <div className="info-bar-wrapper home">
          <div className="info-bar-main">
            <Switch>
              <Route path={BASE_URL} exact={true} component={Home} />
              <Route
                path={MY_PORTFOLIOS_URL}
                exact={true}
                component={MyPortfolios}
              />
              <Route
                path={MY_PORTFOLIOS_DETAIL_URL}
                exact={true}
                component={MyPortfoliosDetail}
              />
              <Route
                path={CHANGE_MANAGEMENT_URL}
                exact={true}
                component={ChangeManagement}
              />
              <Route
                path={CHANGE_DETAIL_URL}
                exact={true}
                component={ChangeDetail}
              />
              <Route
                path={PENDING_APPROVALS_URL}
                exact={true}
                component={PendingApprovals}
              />
              <Route
                path={WORKFLOWS_URL}
                exact={true}
                component={TemplateWorkflows}
              />
              <Route
                path={MY_IN_PROGRESS_WORKFLOWS_URL}
                exact={true}
                component={MyInProgressWorkflow}
              />
              <Redirect from={`${BASE_URL}*`} to={BASE_URL} />
            </Switch>
          </div>
          <div className="infobar-container">
            <InfoBar />
          </div>
        </div>
        <Logger />
        <ToastNotifications />
        {workflow && (
          <WorkflowSetup
            workflow={workflow}
            isEdit={!isTemplate}
            onSubmitted={onSubmitted}
          />
        )}
        <UnsavedConfirmModal />
        <ChangeNotFoundModal />
      </NotificationProvider>
    </HashRouter>
  );
};

export default AppContainer;
