## START REACT APP IN DEVELOPMENT:

1. Unzip `node_modules.7z`

2. Update dls package by run `npm run dls`

3. Start React App in HTTPS protocol `npm run start:https`

4. Open Chrome browser in --disable-web-security mode (to avoid CORS request when call api from localhost:3000 to api mock server) by

   - Open Run dialog (CTRL + R) and enter `chrome.exe --disable-web-security --user-data-dir="C:/Chrome dev session"`
   - Click OK to Run

5. Open [https://localhost:3000](https://localhost:3000) to view it in the browser at step 4.

## START MOCK API SERVER

1. Get .Net core Mock API at `$/Fiserv/CXPM1.0/CXPM.Sprint01`

2. Open `CCT.CXPM.WebApp.sln` solution in Visual studio 2019

3. At Solution Explorer window on the right, right click on CCT.CXPM.WebApp project and click on `Set as Startup Project`

4. Start Mock server by clicking on `IIS Express` button at center top

5. Refresh React web to get data from Mock API

## BUILD & DEPLOY REACT APP

1. config environment variables in `.env.production`

   `PUBLIC_URL`: where your react app domain/url.

   `REACT_APP_API`: where your API Server domain/url

2. npm run build

3. copy all files in build folder to server

## BUILD & DEPLOY MOCK API SERVER

1. Get .Net core Mock API at `$/Fiserv/CXPM1.0/CXPM.Sprint01`

2. Open `CCT.CXPM.WebApp.sln` solution in Visual studio 2019

3. At Solution Explorer window on the right, right click on CCT.CXPM.WebApp project and click on `Set as Startup Project`

4. Next, to publish mock api package, right click on CCT.CXPM.WebApp project and click on `Publish...`

5. Package will be publish to `<Solution_Folder>\CCT.CXPM.WebApp\bin\Release\publish\`, then deploy package to server in sub-application of Website, based on `REACT_APP_API` config in `.env.production`

For example, if `REACT_APP_API` = https://cxpm-qa.local/sprint1/api-mock/api, then create sub-application in website name `api-mock` and deploy mock api package there


## CODE FORMATTING

In the Visual Code, press Ctr + P run the below command:

### `ext install esbenp.prettier-vscode`

Install plugin Prettier for formatting source code

# GIT PRE COMMIT

npm run precommit
