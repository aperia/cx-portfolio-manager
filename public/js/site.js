﻿var excludeClass = ["k-grid-header", "form-group-static__label", "bubble", "k-tabstrip-items", "k-grid-pager", "ds-attr-search-field", "k-label", "form-check-label", "ds-pager", "k-window-title", "btn-secondary", "btn-primary"];
var excludeElement = ["h1", "h2", "h3", "h4", "h5", "h6", "h7", "strong", "label"];
var excludeText = ["txt:", "NotFound:"];

var runScanPage = function () {
    var body = document.getElementsByTagName("body");
    for (var i = 0; i < body.length; i++) {
        if (scanId) {
            scanElement(body[i]);
        }
        if (scanDof) {
            scanDOF(body[i]);
        }
    }
    console.log("Scan done!!!!");
}

var runScanSection = function (id) {
    var body = document.getElementById(id);
    if (scanId) {
        scanElement(body);
    }
    if (scanDof) {
        scanDOF(body);
    }
    console.log("Scan done!!!!");
}

var scanDOF = function (element) {

    if (element.getAttribute("role")) {
        element.style.padding = '2px';
        if (element.getAttribute("role").indexOf("dof-field") != -1) {
            element.style.border = '2px solid blue';
        } else if (element.getAttribute("role").indexOf("dof-view") != -1) {
            element.style.border = '2px solid red';
        }
    }

    if (element.childElementCount > 0) {
        for (var i = 0; i < element.childElementCount; i++) {
            scanDOF(element.children[i]);
        }
    }
}

var scanElement = function (element) {
    if (element.childElementCount > 0) {
        for (var i = 0; i < element.childElementCount; i++) {

            if (!checkValidElement(element)) continue;

            scanElement(element.children[i]);
        }
    }
    else {
        if (checkValidElement(element) && element.textContent.trim() != "" && (!element.getAttribute("id") || element.getAttribute("id").indexOf("undefined") != -1)) {

            var validElement = true;
            for (var i = 0; i < excludeText.length; i++) {
                if (element.textContent.trim().indexOf(excludeText[i]) >= 0) {
                    validElement = false;
                    break;
                }
            }

            if (validElement) {
                if (element.getAttribute("id") && element.getAttribute("id").indexOf("undefined") != -1) {
                    element.style.background = 'pink';
                } else {
                    element.style.background = 'yellow';
                }
                element.style.border = '1px solid black';
                //console.log(element);
            }
        }


        // duplicate id on element
        if (element.getAttribute("id") && idList.includes(element.getAttribute("id"))) {
            element.style.background = 'red';
        }

        if (element.getAttribute("id") && !idList.includes(element.getAttribute("id"))) {
            idList.push(element.getAttribute("id"));
        }
    }
}
var checkValidElement = function (element) {
    for (var i = 0; i < excludeClass.length; i++) {
        if (element.className || element.className.indexOf(excludeClass[i]) == -1) continue;
        return false;
    }
    for (var i = 0; i < excludeElement.length; i++) {
        if (element.localName.indexOf(excludeElement[i]) == -1) continue;
        return false;
    }
    return true;
}

var idList = [];
var myTimeout;

const url = new URLSearchParams(window.location.search);
const scanId = url.get('scanid') == "on";
const scanDof = url.get('scandof') == "on";
const isScan = scanId || scanDof;

if (isScan) {
    window.addEventListener('DOMSubtreeModified', function () {
        idList = [];
        clearTimeout(myTimeout);
        myTimeout = setTimeout(function () { runScanPage() }, 500)
    }, false);
}